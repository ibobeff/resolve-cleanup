/******************************************************************************
* (C) Copyright 2014
*
* Resolve Systems, LLC
*
* All rights reserved
* This software is distributed under license and may not be used, copied,
* modified, or distributed without the express written permission of
* Resolve Systems, LLC.
*
******************************************************************************/

package com.resolve.rsview.controller;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.fileupload.FileItem;
import org.apache.commons.fileupload.disk.DiskFileItemFactory;
import org.apache.commons.fileupload.servlet.ServletFileUpload;
import org.apache.commons.lang3.tuple.Pair;
import org.apache.http.HttpHeaders;
import org.codehaus.jackson.map.ObjectMapper;
import org.owasp.esapi.ESAPI;
import org.owasp.esapi.errors.IntrusionException;
import org.owasp.esapi.errors.ValidationException;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import com.resolve.rsbase.MInfo;
import com.resolve.rsbase.MainBase;
import com.resolve.service.License;
import com.resolve.services.ImpexService;
import com.resolve.services.ServiceHibernate;
import com.resolve.services.constants.ConstantValues;
import com.resolve.services.hibernate.util.ExportUtils;
import com.resolve.services.hibernate.util.ImpexUtil;
import com.resolve.services.hibernate.util.ImportUtils;
import com.resolve.services.hibernate.vo.ResolveEventVO;
import com.resolve.services.hibernate.vo.ResolveImpexManifestVO;
import com.resolve.services.hibernate.vo.ResolveImpexModuleVO;
import com.resolve.services.hibernate.vo.ResolveParserVO;
import com.resolve.services.impex.importmodule.ModuleValidationUtil;
import com.resolve.services.impex.util.ImpexEnum;
import com.resolve.services.impex.vo.ImpexStatusDTO;
import com.resolve.services.interfaces.ImpexComponentDTO;
import com.resolve.services.util.ServletUtils;
import com.resolve.services.vo.QueryDTO;
import com.resolve.services.vo.QuerySort;
import com.resolve.services.vo.ResponseDTO;
import com.resolve.util.Constants;
import com.resolve.util.ERR;
import com.resolve.util.Guid;
import com.resolve.util.JspUtils;
import com.resolve.util.LicenseEnum;
import com.resolve.util.Log;
import com.resolve.util.StringUtils;

import net.sf.json.JSONObject;

/**
 * This controller serve the requests for Resolve Import and Export
 * <p>
 * The Resolve Import Export (Impex) Module is used to archive or transfer any
 * resolve component like Wiki documents, Action Tasks, Forms and more. Use
 * Import/Export to:
 * <li>Create a backup of your Runbooks, Action Tasks, documentation, and other
 * wiki documents or files.
 * <li>Copy your wiki documents, ActionTasks, and Runbooks to another Resolve
 * installation, such as a backup machine or a new installation.
 * <li>Add modules to Resolve, such as new adaptors or other new modules or
 * code.
 * <p>
 * DB Table:
 * {@code resolve_impex_module, resolve_impex_glide, resolve_impex_wiki, resolve_impex_manifest, resolve_impex_log}
 * <p>
 * Url : {@code http://localhost:8080/resolve/jsp/rsclient.jsp#RS.impex.Main/}
 * 
 * @author Mangesh Shimpi
 *
 */
@Controller
public class AjaxImpex extends GenericController {
	/**
	 * Returns list of Import/Export difinitions based on filters, sorts,
	 * pagination.
	 * <p>
	 * <u><b> Example </b></u>:
	 * <p>
	 * <code><pre>
	 * filter:[{"field":"uname","type":"auto","condition":"contains","value":"gateway"}]
	 * page:1
	 * start:0
	 * limit:50
	 * sort:[{"property":"sysUpdatedOn","direction":"DESC"}]
	 * </pre></code> Above example fires a query which checkes for definition name
	 * containing 'gateway'. By default, <code>filter</code> is empty.
	 * <p>
	 * 
	 * @param query
	 *            {@link QueryDTO}, represents the query, pagination, sorting,
	 *            grouping for the UI
	 *            <p>
	 * @param request
	 *            </br>
	 *            <code> HttpServletRequest </code> containing <code>USERNAME</code>
	 *            - username of the user who is logged in to Resolve
	 *            <p>
	 * @param response
	 * 
	 * @return {@link ResponseDTO} - Reponse Data Transfer Object having a List and
	 *         total # of records </br>
	 *         <code>'records'</code> : List of type {@link ResolveParserVO} </br>
	 *         <code>'total'</code> : # of total records in the table/db
	 *         <p>
	 * @throws ServletException
	 * @throws IOException
	 */
    
	@RequestMapping(value = {"/impex/v2","/impex/list"}, method = {RequestMethod.GET }, headers = "Accept=application/json; charset=utf-8")
    @ResponseBody
    public ResponseDTO<ResolveImpexModuleVO> listV2(@ModelAttribute QueryDTO query, HttpServletRequest request,
            HttpServletResponse response) throws ServletException, Exception {
        ResponseDTO<ResolveImpexModuleVO> result = new ResponseDTO<>();
        String username = (String) request.getAttribute(Constants.HTTP_REQUEST_USERNAME);
        query.setModelName(ImpexEnum.IMPEX_MODEL.getValue());
        query.setSelectColumns(ImpexEnum.DEFINITION_LIST_COLUMNS.getValue() + ImpexEnum.COMMA.getValue() + ImpexEnum.DEFINITION_LIST_NEW_COLUMNS.getValue());
        
        try {
            List<ResolveImpexModuleVO> list = ImpexService.getResolveImpexModulesV2(query, username);
            populateLocationCSRFToken(list, request);
            int total = ServiceHibernate.getTotalHqlSysIdCount(query);
            result.setSuccess(true).setRecords(list);
            result.setTotal(total);
        } catch (Exception e) {
            Log.log.error(ImpexEnum.ERROR_LISTING_IMPEX_DEFINITIONS.getValue(), e);
            result.setSuccess(false).setMessage(ImpexEnum.ERROR_LISTING_IMPEX_DEFINITIONS.getValue());
        }

        return result;
    }
	
	private void populateLocationCSRFToken(List<ResolveImpexModuleVO> list, HttpServletRequest request)
	{
	    if (CollectionUtils.isNotEmpty(list))
	    {
    	    for (ResolveImpexModuleVO vo : list) {
                if (StringUtils.isNotEmpty(vo.getULocation())) {
                    String[] params = vo.getULocation().split("\\?");
                    String[] token = JspUtils.getCSRFTokenForPage(request, params[0]);
                    vo.setULocation(String.join("", params[0], "?", params[1], ImpexEnum.AMPERCENT.getValue(), 
                    							token[0], "=", token[1], ImpexEnum.AMPERCENT.getValue(),
                    							HttpHeaders.REFERER, ImpexEnum.EQUAL.getValue(), request.getRequestURL()));
                }
            }
	    }
	}

	/**
	 * Deletes Impex difinition(s).
	 * 
	 * @param ids
	 *            </br>
	 *            <code>List</code> of impex definition ids to be deleted. <br>
	 * @param request
	 *            </br>
	 *            <code> HttpServletRequest </code> containing <code>USERNAME</code>
	 *            - username of the user who is logged in to Resolve
	 *            <p>
	 * @param response
	 *            </br>
	 * 
	 * @return {@link ResponseDTO} - Reponse Data Transfer Object containing the
	 *         result of the opeation </br>
	 *         <code>{"success":true,"message":null,"data":null,"records":null,"total":0}</code>
	 * 
	 * @throws ServletException
	 * @throws IOException
	 */
	@RequestMapping(value = { "/impex/delete", "/impex/delete/v2" }, method = {
			RequestMethod.POST }, headers = "Accept=application/json; charset=utf-8")
	@ResponseBody
	public ResponseDTO<Object> delete(@RequestBody List<String> ids, HttpServletRequest request,
			HttpServletResponse response) throws ServletException, IOException {
		ResponseDTO<Object> result = new ResponseDTO<Object>();
		String userName = (String) request.getAttribute(Constants.HTTP_REQUEST_USERNAME);
		Pair<ResolveEventVO, Boolean> rsEventVOPair = null;
		
		try {
			if (CollectionUtils.isNotEmpty(ids)) {
				if (Log.log.isDebugEnabled()) {
			        Log.log.debug(String.format("AjaxImpex.delete(%s,%s) calling " +
												"ServiceHibernate.getAndSetImpexEventValue...", 
												StringUtils.collectionToString(ids, ", "), userName));
		    	}
				
				try {
					rsEventVOPair = ServiceHibernate.getAndSetImpexEventValue("DELETE", 5, "Sys Ids", userName);
				} catch (Exception e) {
					if (StringUtils.isBlank(e.getMessage()) || 
						!(e.getMessage().startsWith("Operation timed out after waiting for"))) {
			    		throw e;
			    	} else {
			    		if (Log.log.isDebugEnabled()) {
			    			Log.log.debug(String.format("AjaxImpex.delete(%s,%s) " +
														"ServiceHibernate.getAndSetImpexEventValue timed out after " +
														"waiting for 5 seconds", 
														StringUtils.collectionToString(ids, ", "), userName));
			    		}
			    	}
				}
		        
		        if (rsEventVOPair != null && Log.log.isDebugEnabled()) {
			        Log.log.debug(String.format("AjaxImpex.delete(%s,%s) " +
												"ServiceHibernate.getAndSetImpexEventValue returned Resolve Event %s", 
												StringUtils.collectionToString(ids, ", "), userName,
												(rsEventVOPair.getLeft() != null ? 
												 "[" + rsEventVOPair.getLeft() + "] which is " + 
												 (rsEventVOPair.getRight().booleanValue() ? "NEW" : "OLD") : "null")));
		        }
		        
		        if (rsEventVOPair != null && rsEventVOPair.getRight().booleanValue()) {
		        	ImpexUtil.initImpexOperationStage("DELETE", "Impex Model By Ids", 1);
					ImpexService.deleteImpexModelByIDS(ids, userName);
					ImpexUtil.updateImpexCount("DELETE");
					result.setSuccess(true);
		        } else {
		        	String message = null;
		        	
		        	if (rsEventVOPair != null) {
		        		message = String.format("%s IMPEX operation by user %s is in progress, please try later", 
							   					 rsEventVOPair.getLeft().getModuleNameAndHasStage(null).getLeft(),
							   					 rsEventVOPair.getLeft().getSysCreatedBy());
		        	} else {
		        		message = "Another IMPEX operation is in progress, please try later";
		        	}
		        	
		        	Log.log.info(message);
		        	result.setSuccess(false).setMessage(message).setErrorCode(ERR.IMPEX104.getCode());
		        }
			} else {
				result.setSuccess(true);
			}
		} catch (Throwable e) {
			Log.log.error(ImpexEnum.ERROR_DELETING_IMPEX_DEFINITIONS.getValue(), e);
			result.setSuccess(false).setMessage(ImpexEnum.ERROR_DELETING_IMPEX_DEFINITIONS.getValue());
		} finally {
			if (rsEventVOPair != null && rsEventVOPair.getRight().booleanValue()) {
				try {
					ResolveEventVO latestImpexEventVO = ServiceHibernate.getLatestImpexEventByValue();
					
					if (latestImpexEventVO != null) {
						Pair<String, Boolean> moduleNameAndHasStagePair = 
													latestImpexEventVO.getModuleNameAndHasStage("DELETE");
	                	
	                	if (moduleNameAndHasStagePair == null || StringUtils.isBlank(moduleNameAndHasStagePair.getLeft())) {
	                		throw new Exception (
	                						String.format("Failed to get module name from latest Impex Rsolve Event [%s]",
	                									  latestImpexEventVO));
	                	}
                	
	                	if (latestImpexEventVO.getUValue().equals("IMPEX-DELETE Impex Model By Ids:Sys Ids,1,1")) {	                	
		                	latestImpexEventVO.setUValue("IMPEX-DELETE:Sys Ids,-2,Done");
							ServiceHibernate.insertResolveEvent(latestImpexEventVO);
				            
				            if (Log.log.isTraceEnabled()) {
				            	Log.log.trace(String.format("AjaxImpex.delete(%s,%s) inserted/updated Resolve Event [%s]", 
				            								StringUtils.collectionToString(ids, ", "), userName, 
				            								latestImpexEventVO));
				            }
	                	} else {
	                		Log.log.error(String.format("Latest Impex Event Value [%s] is not same as expected " +
	                									"[IMPEX-DELETE:Sys Ids,1,1]", latestImpexEventVO));
	                	}
					}
				} catch (Exception e) {
					Log.log.error(String.format("Error%sin getting latest Impex Resolve Event", 
												(StringUtils.isNotBlank(e.getMessage()) ? " [" + e.getMessage() + "] " : " ")));
				}
			}
		}
		return result;
	}

	/**
	 * Saves Impex definition.
	 * <p>
	 * 
	 * @param container
	 *            {@link JSONObject} Entire payload of Impex definition. A sample of
	 *            this payload is given below:
	 *            <p>
	 *            <code><pre>
	 * {
	 *     "uname": "Save",
	 *     "uforwardWikiDocument": "",
	 *     "uscriptName": "",
	 *     "udescription": "Test Definition",
	 *     "sysCreatedBy": "admin",
	 *     "sysUpdatedBy": "admin",
	 *     "sysOrganizationName": "UNDEFINED",
	 *     "id": "8a9482f0474bcb2a01475b6933090018",
	 *     "sysCreatedOn": "2014-07-21T17:12:06",
	 *     "sysUpdatedOn": "2014-07-21T17:17:13",
	 *     "impexDefinition": [
	 *      {
	 *          "sys_id": "8a9482f0474bcb2a01475b6cc7300019",
	 *          "type": "actionTask",
	 *          "name": "Cache_test",
	 *          "namespace": "rsqa",
	 *          "description": "{\"uname\":\"Cache_test\",\"unamespace\":\"rsqa\",\"usummary\":null,\"sysUpdatedBy\":\"admin\",\"sysUpdatedOn\":\"2014-07-21T17:16:01\",\"sys_id\":\"8a9482f0474bcb2a01475b6cc7300019\",\"id\":\"rsqaCache_test\"} ",
	 *          "options": {
	 *             "atProperties": true,
	 *             "atPost": false,
	 *             "excludeRefAssessor": false,
	 *              ...
	 *          }
	 *       }
	 *    ]
	 * }
	 * </pre></code> The id's and sys_ids will be blank for the brand new
	 *            definition. It will be populated after the first save and will be
	 *            used for rest of the subsequent saves
	 * @param request
	 *            {@link HttpServletRequest} </code> containing
	 *            <code>USERNAME</code>: a username of the user who is logged in to
	 *            Resolve
	 * @param response
	 *            {@link HttpServletResponse}
	 * @return {@link ResponseDTO<Object>} Entire definition payload along with the
	 *         success information. Here's the sample: <code><pre>
	    {
	       "success": true,
	       "message": null,
	       "records": null,
	       "total": 0,
	       "data": {
	          "id": "8a9482f0474bcb2a01475b6933090018",
	          "sys_id": "8a9482f0474bcb2a01475b6933090018",
	          "sysCreatedOn": 1405987926793,
	          "sysCreatedBy": "admin",
	          "sysUpdatedOn": 1406046146711,
	          "sysUpdatedBy": "admin",
	          "sysModCount": 0,
	          "sysPerm": "UNDEFINED",
	          "sysOrg": null,
	          "sysIsDeleted": null,
	          "sysOrganizationName": "UNDEFINED",
	          "impexDefinition": [
	             {
	                "sys_id": "8a9482f0474bcb2a01475b6cc7300019",
	                "type": "actionTask",
	                "name": "Cache_test",
	                "namespace": "rsqa",
	                "description": "{\"uname\":\"Cache_test\",\"unamespace\":\"rsqa\",\"usummary\":null,\"sysUpdatedBy\":\"admin\",\"sysUpdatedOn\":\"2014-07-21T17:16:01\",\"sys_id\":\"8a9482f0474bcb2a01475b6cc7300019\",\"id\":\"rsqaCache_test\"} ",
	                "sysUpdatedBy": "admin",
	                "sysUpdatedOn": 1405988161328,
	                "options": {
	                   "atProperties": true,
	                   "atPost": false,
	                   "excludeRefAssessor": false,
	                   "excludeRefParser": false,
	                   ...
	                }
	             }
	          ],
	          "resolveImpexWikis": null,
	          "resolveImpexGlides": null,
	          "uname": "Save",
	          "ulocation": null,
	          "uzipFileContent": null,
	          "udescription": "Test Definition",
	          "uforwardWikiDocument": "",
	          "uscriptName": "",
	          "uzipFileName": null,
	          "udirty": null
	       }
	    }
	 * </pre></code>
	 * @throws ServletException
	 * @throws IOException
	 */
	@RequestMapping(value = { "/impex/save"}, method = { RequestMethod.POST,
			RequestMethod.PUT }, headers = "Accept=application/json; charset=utf-8")
	@ResponseBody
	public ResponseDTO<Object> save(@RequestBody JSONObject container, HttpServletRequest request,
			HttpServletResponse response) throws ServletException, IOException {
		ResponseDTO<Object> result = new ResponseDTO<Object>();
		String userName = (String) request.getAttribute(Constants.HTTP_REQUEST_USERNAME);
		String jsonContainer = container.toString();
		try {
			ResolveImpexModuleVO entity = new ObjectMapper().readValue(jsonContainer, ResolveImpexModuleVO.class);
			ResolveImpexModuleVO savedVO = ImpexService.saveResolveImpexModule(entity, userName, true, "SAVE");
			result.setSuccess(true).setData(savedVO);
		} catch (Throwable e) {
			Log.log.error("Error saving Impex definition(s).\n" + e);
			result.setSuccess(false).setMessage("Error saving Impex definition(s).");
		}
		return result;
	}

	/**
	 * List the components present in the definition. Sample returned json:
	 * 
	 * <pre>
	 * <code>
	    {
	       "success": true,
	       "message": null,
	       "records": null,
	       "total": 0,
	       "data": {
	          "id": "8a9482f0474bcb2a01475b6933090018",
	          "sys_id": "8a9482f0474bcb2a01475b6933090018",
	          "sysCreatedOn": 1405987926793,
	          "sysCreatedBy": "admin",
	          "sysUpdatedOn": 1406046146711,
	          "sysUpdatedBy": "admin",
	          "sysModCount": 0,
	          "sysPerm": "UNDEFINED",
	          "sysOrg": null,
	          "sysIsDeleted": null,
	          "sysOrganizationName": "UNDEFINED",
	          "impexDefinition": [
	             {
	                "sys_id": "8a9482f0474bcb2a01475b6cc7300019",
	                "type": "actionTask",
	                "name": "Cache_test",
	                "namespace": "rsqa",
	                "description": "{\"uname\":\"Cache_test\",\"unamespace\":\"rsqa\",\"usummary\":null,\"sysUpdatedBy\":\"admin\",\"sysUpdatedOn\":\"2014-07-21T17:16:01\",\"sys_id\":\"8a9482f0474bcb2a01475b6cc7300019\",\"id\":\"rsqaCache_test\"} ",
	                "sysUpdatedBy": "admin",
	                "sysUpdatedOn": 1405988161328,
	                "options": {
	                   "atProperties": true,
	                   "atPost": false,
	                   "excludeRefAssessor": false,
	                   "excludeRefParser": false,
	                   ...
	                }
	             }
	          ],
	          "resolveImpexWikis": null,
	          "resolveImpexGlides": null,
	          "uname": "Save",
	          "ulocation": null,
	          "uzipFileContent": null,
	          "udescription": "Test Definition",
	          "uforwardWikiDocument": "",
	          "uscriptName": "",
	          "uzipFileName": null,
	          "udirty": null
	       }
	    }
	 * </code>
	 * </pre>
	 * 
	 * @param moduleId
	 *            <code>String</code> moduleId whoes definition needs to be read.
	 * @param request
	 *            {@link HttpServletRequest} </code> containing
	 *            <code>USERNAME</code>: a username of the user who is logged in to
	 *            Resolve
	 * @param response
	 *            {@link HttpServletResponse}
	 * @return {@link ResponseDTO<Object>} Entire definition payload along with the
	 *         success information.
	 * @throws ServletException
	 * @throws IOException
	 */
	@RequestMapping(value = "/impex/get", method = {
			RequestMethod.GET }, headers = "Accept=application/json; charset=utf-8")
	@ResponseBody
	public ResponseDTO<Object> getDefinitionDetail(@RequestParam String moduleId, HttpServletRequest request,
			HttpServletResponse response) throws ServletException, IOException {
		// moduleId = request.getParameter("moduleId");
		ResponseDTO<Object> result = new ResponseDTO<Object>();
		String userName = (String) request.getAttribute(Constants.HTTP_REQUEST_USERNAME);
		try {
			ResolveImpexModuleVO module = ImpexUtil.getImpexModelWithDefinitions(moduleId, userName);
			module.setUZipFileContent(null);
			result.setSuccess(true).setData(module);
		} catch (Exception e) {
			Log.log.error(ImpexEnum.ERROR_GETTING_IMPEX_DEFINITION.getValue(), e);
			result.setSuccess(false).setMessage(ImpexEnum.ERROR_GETTING_IMPEX_DEFINITION.getValue());
		}

		return result;
	}

	@RequestMapping(value = "/impex/definition/{moduleId}/v2", method = {
			RequestMethod.GET }, headers = "Accept=application/json; charset=utf-8")
	@ResponseBody
	public ResponseDTO<Object> getDefinitionDetailV2(@PathVariable String moduleId, HttpServletRequest request,
			HttpServletResponse response) throws ServletException, IOException {
		return getDefinitionDetail(moduleId, request, response);
	}

	/**
	 * Method to get components to be added in the export module.
	 * 
	 * @param queryDTO
	 *            {@link QueryDTO} For pagination and filering. The sample below
	 *            filters components whoes uname contains a string Wait: <code><pre>
	 * 
	 *   filter:[{"field":"uname","type":"auto","condition":"contains","value":"Wait"}]
	    page:1
	    start:0
	    limit:50
	    sort:[{"property":"uname","direction":"ASC"}]
	 * </pre></code>
	 * @param compType
	 *            A <code>String</code> specifying the type of the component whoes
	 *            list to be displayed.
	 * @param request
	 *            {@link HttpServletRequest} </code> containing
	 *            <code>USERNAME</code>: a username of the user who is logged in to
	 *            Resolve
	 * @param response
	 *            {@link HttpServletResponse}
	 * @return {@link ResponseDTO} Contains success responce along with the list of
	 *         components searched for to be displayed in the UI.
	 * @throws ServletException
	 * @throws IOException
	 */
	@SuppressWarnings("unchecked")
	@RequestMapping(value = "/impex/addcomponent/list", method = { RequestMethod.GET,
			RequestMethod.POST }, headers = "Accept=application/json; charset=utf-8")
	@ResponseBody
	public ResponseDTO<Object> getExportCompListToBeAdded(@ModelAttribute QueryDTO queryDTO,
			@RequestParam String compType, HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		ResponseDTO<Object> result = new ResponseDTO<Object>();
		String userName = (String) request.getAttribute(Constants.HTTP_REQUEST_USERNAME);

		try {
			result = ImpexUtil.getExportCompListToBeAdded(queryDTO, compType, userName);
			result.setSuccess(true);
		} catch (Exception e) {
			Log.log.error("Error listing Impex definition(s).", e);
			result.setSuccess(false).setMessage("Error listing Impex definition(s).");
		}

		return result;
	}

	@RequestMapping(value = "impex/components/{componentType}/v2", method = {
			RequestMethod.GET }, headers = "Accept=application/json; charset=utf-8")
	@ResponseBody
	public ResponseDTO<Object> getComponentListByType(@ModelAttribute QueryDTO queryDTO,
			@PathVariable String componentType, HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		return getExportCompListToBeAdded(queryDTO, componentType, request, response);
	}

	@RequestMapping(value = "/impex/checkRefCustomTable", method = { RequestMethod.POST,
			RequestMethod.GET }, headers = "Accept=application/json; charset=utf-8")
	@ResponseBody
	public ResponseDTO<Object> checkRefCustomTable(@RequestParam("moduleName") String moduleName,
			HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		ResponseDTO<Object> result = new ResponseDTO<Object>();
		String userName = (String) request.getAttribute(Constants.HTTP_REQUEST_USERNAME);
		try {
			boolean refTablePresent = ImpexUtil.checkRefCustomTables(moduleName, userName);
			result.setData(refTablePresent);
			result.setSuccess(true);
		} catch (Exception e) {
			Log.log.error("Error while checking for reference custom tables.", e);
			result.setSuccess(false).setMessage("Error while checking for reference custom tables.");
		}
		return result;
	}

	/**
	 * Exports a module
	 * 
	 * @param moduleName
	 *            A <code>String</code> represending module name to be exported.
	 * @param excludeIds
	 *            A <code>List</code> of <code>String</code>s represending component
	 *            ids to be excluded from the export.
	 * @param request
	 *            {@link HttpServletRequest} </code> containing
	 *            <code>USERNAME</code> of the user who is logged in to Resolve
	 * @param response
	 *            {@link HttpServletResponse}
	 * @return {@link ResponseDTO} Contains success responce for the request.
	 * @throws ServletException
	 * @throws IOException
	 */
	@RequestMapping(value = { "/impex/export", "/impex/export/v2" }, method = {
			RequestMethod.POST }, headers = "Accept=application/json; charset=utf-8")
	@ResponseBody
	public ResponseDTO<Object> export(@RequestParam("moduleName") String moduleName,
			@RequestParam("excludeIds") List<String> excludeIds, HttpServletRequest request,
			HttpServletResponse response) throws ServletException, IOException {
		ResponseDTO<Object> result = new ResponseDTO<Object>();
		String userName = (String) request.getAttribute(Constants.HTTP_REQUEST_USERNAME);
		try {
			String message = ImpexUtil.exportModule(moduleName, excludeIds, false, userName);
			result.setSuccess(true).setData(true);
			if (StringUtils.isNotBlank(message)) {
			    result.setErrorCode(ERR.IMPEX104.getCode());
				result.setMessage(message).setData(false);
			}
		} catch (Exception e) {
			Log.log.error("Error exporting Impex definition.", e);
			result.setSuccess(false).setMessage("Error exporting Impex definition.");
		}
		return result;
	}

	/**
	 * Uploads file content present in request payload synchronously for import.
	 * 
	 * @param request
	 *            {@link HttpServletRequest} </code> containing
	 *            <code>USERNAME</code> of the user who is logged in to Resolve
	 * @param response
	 *            {@link HttpServletResponse}
	 * @return
	 * @throws ServletException
	 * @throws IOException
	 */
	@SuppressWarnings("unchecked")
	@RequestMapping(value = {"/impex/uploadFile", "/impex/upload/v2"}, method = { RequestMethod.GET, RequestMethod.POST })
	@ResponseBody
	public ResponseDTO<Map<String, String>> uploadModule(@RequestParam (required = false, defaultValue = "false") Boolean isNew,
	                HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
	    ResponseDTO<Map<String, String>> result = new ResponseDTO<>();
		String userName = (String) request.getAttribute(Constants.HTTP_REQUEST_USERNAME);
		List<FileItem> fileItems = null;
		Map<String, String> messageMap = null;
		
		Pair<ResolveEventVO, Boolean> rsEventVOPair = null;
		
		try {
			if (Log.log.isDebugEnabled()) {
		        Log.log.debug(String.format("AjaxImpex.uploadModule(%s) calling ServiceHibernate.getAndSetImpexEventValue...", 
											userName));
	    	}
			
			try {
				rsEventVOPair = ServiceHibernate.getAndSetImpexEventValue("UPLOAD", 5, "Upload File", userName);
			} catch (Exception e) {
				if (StringUtils.isBlank(e.getMessage()) || 
					!(e.getMessage().startsWith("Operation timed out after waiting for"))) {
		    		throw e;
		    	} else {
		    		if (Log.log.isDebugEnabled()) {
		    			Log.log.debug(String.format("AjaxImpex.uploadModule(%s) " +
													"ServiceHibernate.getAndSetImpexEventValue timed  out after " +
													"waiting for 5 seconds", userName));
		    		}
		    	}
			}
			
			if (rsEventVOPair != null && Log.log.isDebugEnabled()) {
		        Log.log.debug(String.format("AjaxImpex.uploadModule(%s) " +
											"ServiceHibernate.getAndSetImpexEventValue returned Resolve Event %s", 
											userName,
											(rsEventVOPair.getLeft() != null ? 
											 "[" + rsEventVOPair.getLeft() + "] which is " + 
											 (rsEventVOPair.getRight().booleanValue() ? "NEW" : "OLD") : "null")));
	        }
			
			if (rsEventVOPair != null && rsEventVOPair.getRight().booleanValue()) {
				fileItems = new ServletFileUpload(new DiskFileItemFactory()).parseRequest(request);
				
				if (CollectionUtils.isNotEmpty(fileItems)) {
					for (FileItem fileItem : fileItems) {
						if (!fileItem.isFormField()) {
							ImpexUtil.initImpexOperationStage("UPLOAD", fileItem.getName(), 1);
						    messageMap = ImpexService.uploadFileForImport(fileItem, isNew, userName);
						    ImpexUtil.updateImpexCount("UPLOAD");
							break;
						}
					}
				}
				
				result.setMessage("File uploaded successfully.");
				result.setSuccess(true);
				result.setData(messageMap);
			} else {
				String message = null;
	        	
	        	if (rsEventVOPair != null) {
	        		message = String.format("%s IMPEX operation by user %s is in progress, please try later", 
						   					 rsEventVOPair.getLeft().getModuleNameAndHasStage(null).getLeft(),
						   					 rsEventVOPair.getLeft().getSysCreatedBy());
	        	} else {
	        		message = "Another IMPEX operation is in progress, please try later";
	        	}
	        	
	        	Log.log.info(message);
	        	result.setSuccess(false).setMessage(message).setErrorCode(ERR.IMPEX104.getCode());
			}
		} catch (Throwable e) {
		    String errorMsg = e.getMessage();
		    if (StringUtils.isNotBlank(errorMsg) && errorMsg.startsWith(ImpexEnum.CONTENT_ERROR.getValue())) {
		        Log.log.error(errorMsg, e);
		        result.setSuccess(false).setMessage(ERR.IMPEX106.getMessage()).setErrorCode(ERR.IMPEX106.getCode());
		    } else {
    			Log.log.error(e.getMessage(), e);
    			result.setSuccess(false).setMessage("Error uploading file.");
    			response.sendError(500, "Error uploading file.");
		    }
		} finally {
			if (rsEventVOPair != null && rsEventVOPair.getRight().booleanValue()) {
				try {
					ResolveEventVO latestImpexEventVO = ServiceHibernate.getLatestImpexEventByValue();
					
					if (latestImpexEventVO != null) {
						if (latestImpexEventVO.getUValue().startsWith("IMPEX-UPLOAD") && 
							latestImpexEventVO.getUValue().endsWith(",1,1")) {
							latestImpexEventVO.setUValue(latestImpexEventVO.getUValue().replace(",1,1", ",-2,Done"));
							ServiceHibernate.insertResolveEvent(latestImpexEventVO);
				            
				            if (Log.log.isTraceEnabled()) {
				            	Log.log.trace(String.format("AjaxImpex.uploadModule(%s) inserted/updated Resolve Event [%s]", 
				            								userName, latestImpexEventVO));
				            }
						} else {
							Log.log.error(String.format("Latest Impex Event Value [%s] does not starts with expected " +
														"IMPEX-UPLOAD and does not ends with expected ,1,1", 
														latestImpexEventVO));
						}
					}
				} catch (Exception e) {
					Log.log.error(String.format("Error%sin getting latest Impex Resolve Event", 
												(StringUtils.isNotBlank(e.getMessage()) ? " [" + e.getMessage() + "] " : " ")));
				}
			}
		}
		// current version of plupload cannot handle request header properly..there must
		// be sth wrong with their flash plugin it ignores the header configuration in
		// js file so we have to set the content type to their predefined accpet type.
		response.setContentType("text/html; charset=utf-8");
		response.getWriter().print(new ObjectMapper().writeValueAsString(result));
		response.getWriter().flush();

		return null;
	}

	/**
	 * Uploads and installs a module from RSConsole given a local URL path.
	 * 
	 * @param request
	 *            {@link HttpServletRequest} </code> containing
	 *            <code>USERNAME</code> of the user who is logged in to Resolve
	 * @param response
	 *            {@link HttpServletResponse}
	 * @return
	 * @throws ServletException
	 * @throws IOException
	 */
	@SuppressWarnings("unchecked")
	@RequestMapping(value = "/impex/uploadAndInstallModule", method = { RequestMethod.GET, RequestMethod.POST })
	@ResponseBody
	public ResponseDTO<Object> uploadAndInstallModule(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		ResponseDTO<Object> result = new ResponseDTO<Object>();
		String userName = (String) request.getParameter("_username_");
		// String path = (String) request.getParameter("path");
		Pair<ResolveEventVO, Boolean> rsEventVOPair = null;
		String licenseBreachMsg = null;
		ERR err = null;
		
		try {
			/*
			 * License is V2 and instance is NOT PROD and 
			 * license is breached for named user count or HA or has expired 
			 * then do not allow import operation.
			 * 
			 * License V1 and instance is configured and is NON PROD and
			 * license is breached for named user count or HA or has expired 
			 * then do not allow import operation.
			 */
			
			License lic = MainBase.main.getLicenseService().getLicense();
			
			if ((lic.isV2AndNonProd() || 
				 (!lic.isV2() && 
				  !MainBase.main.configGeneral.getEnv()
				   .equalsIgnoreCase(LicenseEnum.LICENSE_ENV_NON_PROD.getKeygenName()) &&
				  !MainBase.main.configGeneral.getEnv()
				   .equalsIgnoreCase(LicenseEnum.LICENSE_ENV_PRODUCTION.getKeygenName()))) &&
				(lic.isExpired() || MainBase.main.getLicenseService().getIsUserCountViolated() || 
				 MainBase.main.getLicenseService().getIsHAViolated())) {
				
				licenseBreachMsg = "Expired";
				
				if (MainBase.main.getLicenseService().getIsUserCountViolated()) {
					licenseBreachMsg = "Named User Count";
				}
				
				if (MainBase.main.getLicenseService().getIsHAViolated()) {
					licenseBreachMsg = "HA";
				}
				
				String errMsg = String.format("License violation : %s. Upload and Install operation is prohibited.",
											  licenseBreachMsg);
				
				err = ERR.IMPEX108;
				throw new Exception(errMsg);
			}
			
			List<File> fileUploads = ESAPI.httpUtilities().getFileUploads(request);
			// Dako: According to the behavior I have explored, it is supposed only one file
			// to be here
			// Traverse the list in case we need to process more than one file here
			if (CollectionUtils.isNotEmpty(fileUploads)) {
				File srcFile = fileUploads.get(0);
				// get the file location
				// File srcFile = new File(path);
				
				if (Log.log.isDebugEnabled()) {
			        Log.log.debug(String.format("AjaxImpex.uploadAndInstallModule(%s,%s) calling " +
			        							"ServiceHibernate.getAndSetImpexEventValue...", 
			        							srcFile.getName(), userName));
		    	}
				
				try {
					rsEventVOPair = ServiceHibernate.getAndSetImpexEventValue("UPLOADANDINSTALL", 5, srcFile.getName(), 
																			  userName);
				} catch (Exception e) {
					if (StringUtils.isBlank(e.getMessage()) || 
						!(e.getMessage().startsWith("Operation timed out after waiting for"))) {
			    		throw e;
			    	} else {
			    		if (Log.log.isDebugEnabled()) {
			    			Log.log.debug(String.format("AjaxImpex.uploadAndInstallModule(%s,%s) " +
														"ServiceHibernate.getAndSetImpexEventValue timed  out after " +
														"waiting for 5 seconds", srcFile.getName(), userName));
			    		}
			    	}
				}
				
				if (rsEventVOPair != null && Log.log.isDebugEnabled()) {
			        Log.log.debug(String.format("AjaxImpex.uploadAndInstallModule(%s,%s) " +
												"ServiceHibernate.getAndSetImpexEventValue returned Resolve Event %s", 
												srcFile.getName(), userName,
												(rsEventVOPair.getLeft() != null ? 
												 "[" + rsEventVOPair.getLeft() + "] which is " + 
												 (rsEventVOPair.getRight().booleanValue() ? "NEW" : "OLD") : "null")));
		        }
				
				if (rsEventVOPair != null && rsEventVOPair.getRight().booleanValue()) {
					ImpexUtil.initImpexOperationStage("UPLOADANDINSTALL", "Upload", 1);
					// copy it to the rsexpert directory
					String moduleName = ImpexService.uploadFileForImport(srcFile, userName, true);
					ImpexUtil.updateImpexCount("UPLOADANDINSTALL");
					
					try {
						ResolveEventVO latestUploadEventVO = ServiceHibernate.getLatestEventByValue("IMPEX-UPLOADANDINSTALL");
						
						if (latestUploadEventVO != null) {
							Pair<String, Boolean> moduleNameAndHasStagePair = 
														latestUploadEventVO.getModuleNameAndHasStage("UPLOADANDINSTALL");
		                	
		                	if (moduleNameAndHasStagePair == null || StringUtils.isBlank(moduleNameAndHasStagePair.getLeft())) {
		                		throw new Exception (
		                						String.format(
		                							"Failed to get file name from latest UPLOADANDINSTALL Rsolve Event [%s]",
		                							latestUploadEventVO));
		                	}
	                	
		                	String fileName = moduleNameAndHasStagePair.getLeft();
		                	
							latestUploadEventVO.setUValue(String.format("IMPEX-UPLOADANDINSTALL:%s,-2,Done", fileName));
							ServiceHibernate.insertResolveEvent(latestUploadEventVO);
				            
				            if (Log.log.isTraceEnabled()) {
				            	Log.log.trace(String.format("AjaxImpex.uploadAndInstallModule(%s,%s) inserted/updated " +
				            							    "Resolve Event [%s]", 
				            							    srcFile.getName(), userName, latestUploadEventVO));
				            }
				            
				            if (moduleName.contains(".zip")) {
								moduleName = moduleName.substring(0, moduleName.lastIndexOf(".zip"));
							}
				            
							String message = ImpexService.importModule(moduleName, null, userName, false);
							
							if (StringUtils.isNotBlank(message)) {
								result.setSuccess(false).setMessage(message).setData(false);
								result.setErrorCode(ERR.IMPEX104.getCode());
							} else {
								// create the JSON success output
								result.setSuccess(true)
								.setMessage(String.format("Uploaded %s and asynchronous installation of %s module " +
														  "has stared...", srcFile.getName(), moduleName));
								result.setData(String.format("Module: %s", moduleName));
							}
						}
					} catch (Exception e) {
						String errMsg = String.format("Error%sin getting latest IMPEX-UPLOADANDINSTALL Resolve Event", 
													  (StringUtils.isNotBlank(e.getMessage()) ? 
													   " [" + e.getMessage() + "] " : " "));
						Log.log.error(errMsg);
						result.setSuccess(false).setMessage(errMsg);
					}
				} else {
					String message = null;
		        	
		        	if (rsEventVOPair != null) {
		        		message = String.format("%s IMPEX operation by user %s is in progress, please try later", 
							   					 rsEventVOPair.getLeft().getModuleNameAndHasStage(null).getLeft(),
							   					 rsEventVOPair.getLeft().getSysCreatedBy());
		        	} else {
		        		message = "Another IMPEX operation is in progress, please try later";
		        	}
		        	
		        	Log.log.info(message);
		        	result.setSuccess(false).setMessage(message).setErrorCode(ERR.IMPEX104.getCode());
				}
			} else {
				result.setSuccess(false).setMessage("No file selected for upload");
			}
		} catch (Throwable e) {
			// create the JSON fail output
			String errMsg = "Error uploading and installing module.";
			if (StringUtils.isNotEmpty(e.getMessage()) && e.getMessage().contains("License violation : ")) {
				errMsg = e.getMessage();
				Log.log.warn(errMsg);
			} else {
				errMsg = String.format("Error%sin uploading and installing module.", 
									   (StringUtils.isNotEmpty(e.getMessage()) ? " [" + e.getMessage() + "] " : " "));
				Log.log.error(errMsg, e);
			}
			
			result.setSuccess(false);
			
			if (err != null) {
				result.setErrorCode(err.getCode()).setMessage(String.format(err.getMessage(), licenseBreachMsg));
			} else {
				result.setMessage(errMsg);
			}
		}

		return result;
	}

	/**
	 * Imports module represented by <code>moduleName</code>.
	 * 
	 * @param moduleName:
	 *            <code>String</code> represending module name to be exported.
	 * @param excludeIds:
	 *            List of String containing sysIds of the import components to be
	 *            excluded from the import operation.
	 * @param request
	 *            {@link HttpServletRequest} </code> containing
	 *            <code>USERNAME</code> of the user who is logged in to Resolve
	 * @param response
	 *            {@link HttpServletResponse}
	 * @return
	 * @throws ServletException
	 * @throws IOException
	 */
	@RequestMapping(value = "/impex/import", method = { RequestMethod.GET, RequestMethod.POST })
	@ResponseBody
	public ResponseDTO<Object> importModule(@RequestParam("moduleName") String moduleName,
			@RequestParam("excludeIds") List<String> excludeIds, HttpServletRequest request,
			HttpServletResponse response) throws ServletException, IOException {
		ResponseDTO<Object> result = new ResponseDTO<Object>();
		String userName = (String) request.getAttribute(Constants.HTTP_REQUEST_USERNAME);
		String licenseBreachMsg = null;
		ERR err = null;
		
		try {
			/*
			 * License is V2 and instance is NOT PROD and 
			 * license is breached for named user count or HA or has expired 
			 * then do not allow import operation.
			 * 
			 * License V1 and instance is configured and is NON PROD and
			 * license is breached for named user count or HA or has expired 
			 * then do not allow import operation.
			 */
			
			License lic = MainBase.main.getLicenseService().getLicense();
			
			if ((lic.isV2AndNonProd() || 
				 (!lic.isV2() && 
				  !MainBase.main.configGeneral.getEnv()
				   .equalsIgnoreCase(LicenseEnum.LICENSE_ENV_NON_PROD.getKeygenName()) &&
				  !MainBase.main.configGeneral.getEnv()
				   .equalsIgnoreCase(LicenseEnum.LICENSE_ENV_PRODUCTION.getKeygenName()))) &&
				(lic.isExpired() || MainBase.main.getLicenseService().getIsUserCountViolated() || 
				 MainBase.main.getLicenseService().getIsHAViolated())) {
				
				licenseBreachMsg = "Expired";
				
				if (MainBase.main.getLicenseService().getIsUserCountViolated()) {
					licenseBreachMsg = "Named User Count";
				}
				
				if (MainBase.main.getLicenseService().getIsHAViolated()) {
					licenseBreachMsg = "HA";
				}
				
				String errMsg = String.format("License violation : %s. Import module operation is prohibited.",
											  licenseBreachMsg);
				err = ERR.IMPEX107;
				throw new Exception(errMsg);
			}
			
			if (excludeIds.size() == 0) {
				excludeIds = null;
			}
			if (moduleName.contains(".zip")) {
				moduleName = moduleName.substring(0, moduleName.lastIndexOf(".zip"));
			}
			String message = ImpexService.importModule(moduleName, excludeIds, userName, false);
			result.setSuccess(true).setData(true);
			if (StringUtils.isNotBlank(message)) {
				result.setSuccess(false).setMessage(message).setData(false);
				result.setErrorCode(ERR.IMPEX104.getCode());
			}
		} catch (Exception e) {
			String errMsg = "Error importing module.";
			if (StringUtils.isNotEmpty(e.getMessage()) && e.getMessage().contains("License violation : ")) {
				errMsg = e.getMessage();
				Log.log.warn(errMsg);
			} else {
				errMsg = String.format("Error%sin importing impex module.", 
									   (StringUtils.isNotEmpty(e.getMessage()) ? " [" + e.getMessage().substring(e.getMessage().indexOf(":") + 1, e.getMessage().length()) + "] " : " "));
				Log.log.error(errMsg, e);
			}
			
			result.setSuccess(false);
			
			if (err != null) {
				result.setErrorCode(err.getCode()).setMessage(String.format(err.getMessage(), licenseBreachMsg));
			} else {
				result.setMessage(errMsg);
			}
		}

		return result;
	}

	/**
	 * Check whether the module represented by <code>moduleName</code> contains
	 * custom table.
	 * 
	 * @param moduleName:
	 *            String representing module name.
	 * @param request
	 *            {@link HttpServletRequest}
	 * @param response
	 *            {@link HttpServletResponse}
	 * @return Returns boolean true if module contains a custom table, false
	 *         otherwise.
	 * 
	 * @throws ServletException
	 * @throws IOException
	 */
	@RequestMapping(value = "/impex/checkForCustomTables", method = { RequestMethod.GET, RequestMethod.POST })
	@ResponseBody
	public ResponseDTO<Object> checkForCustomTables(@RequestParam("moduleName") String moduleName,
			HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		ResponseDTO<Object> result = new ResponseDTO<Object>();
		try {
			boolean data = ImpexService.checkForCustomTables(moduleName);
			result.setSuccess(true).setData(data);
		} catch (Exception e) {
			String error = "Error while checking whether custon table exist in the module.";
			Log.log.error(error, e);
			result.setSuccess(false).setMessage(error);
		}

		return result;
	}

	/**
	 * Captures the status of Impex operation. This method is called repeatedly
	 * until and <code>percent</code> is 100 and <code>finished</code> is true.
	 * 
	 * @param operation
	 *            A <code>String</code> represending the current operation. It could
	 *            either be <code>import</code> or <code>export</code>
	 * @param request
	 *            {@link HttpServletRequest}
	 * @param response
	 *            {@link HttpServletResponse}
	 * @return {@link ResponseDTO} Contains status of the operation. Sample is shown
	 *         below: <code><pre>
	    {
	       "success": true,
	       "message": null,
	       "data": {
	          "percent": 11,
	          "module": "Save",
	          "operation": "export",
	          "wikiName": "",
	          "finished": false
	       },
	       "records": null,
	       "total": 0
	    }
	 * </pre></code>
	 * @throws ServletException
	 * @throws IOException
	 */
	@RequestMapping(value = "/impex/impexStatus", method = { RequestMethod.GET,
			RequestMethod.POST }, headers = "Accept=application/json; charset=utf-8")
	@ResponseBody
	public ResponseDTO<Object> impexStatus(@RequestParam("operation") String operation, HttpServletRequest request,
			HttpServletResponse response) throws ServletException, IOException {
		ResponseDTO<Object> result = new ResponseDTO<Object>();
		try
		{
		    ImpexStatusDTO statusDTO = new ImpexStatusDTO();
			Map<String, String> resultMap = null;
			
			if (Log.log.isTraceEnabled()) {
				Log.log.trace(String.format("AjaxImpex.impexStatus(%s) B4 DBPoolUsage [%s]", 
											operation,
											StringUtils.mapToString(
												new MInfo().dbpoolUsage(new HashMap<String, Object>()), "=", ", ")));
			}
			
			if (Log.log.isTraceEnabled()) {
				Log.log.trace(String.format("Calling getImportStatus(%s)...", operation));
			}
			resultMap = ImpexUtil.getImportStatus(operation.toUpperCase());
			if (Log.log.isTraceEnabled()) {
				Log.log.trace(String.format("getImportStatus(%s) returned %s", 
											operation, StringUtils.mapToString(resultMap, "=", ",")));
			}
			
			if (resultMap.get("SUCCESS") != null)
			{
				String moduleName = resultMap.get("MODULE");
				String newModuleName = resultMap.containsKey("NEWMODULE") ? resultMap.get("NEWMODULE") : moduleName;
				String percent = resultMap.get("SUCCESS");
				String status = resultMap.containsKey("STATUS") ? resultMap.get("STATUS") : "";
				String stage = resultMap.get("STAGE");
				String stageSuccess = resultMap.get("STAGESUCCESS");
				String forwardWiki = resultMap.get("FORWARD_WIKI");
				String impexLog = null;
				try
				{
					Integer.parseInt(percent);
					{
					    statusDTO.setPercent(percent);
					    statusDTO.setModule(moduleName);
					    statusDTO.setOperation(operation);
					    
					    if (percent.equals("100")) {
					        statusDTO.setFinished(true);
					        statusDTO.setWiki(forwardWiki);
					        impexLog = resultMap.get("LOG");
					        if (StringUtils.isNotBlank(impexLog)) {
					            statusDTO.setImpexLog(impexLog);
					        }
					        if ((StringUtils.isNotBlank(stage) && stage.toLowerCase().contains("export")) ||
					        	StringUtils.isBlank(stage)) {
						        String[] token = JspUtils.getCSRFTokenForPage(request, ImpexEnum.IMPEX_DOWNLOAD_URL.getValue());
			                    ResolveImpexModuleVO modelVO = ImpexUtil.getImpexModelVO(null, newModuleName, false);
			                    String url = "";
			                    if (modelVO != null)
			                    {
			                        url = ImpexEnum.IMPEX_DOWNLOAD_URL.getValue() + ImpexEnum.QUESTION.getValue() + 
			                        	  ImpexEnum.FILENAME.getValue() + ImpexEnum.EQUAL.getValue() + 
			                        	  modelVO.getUZipFileName() + ImpexEnum.AMPERCENT.getValue() + 
			                        	  ImpexEnum.MODULEID.getValue() + ImpexEnum.EQUAL.getValue() + 
			                        	  modelVO.getSys_id() + ImpexEnum.AMPERCENT.getValue() +
			                              token[0] + ImpexEnum.EQUAL.getValue() + token[1] + ImpexEnum.AMPERCENT.getValue() +
			                              HttpHeaders.REFERER + ImpexEnum.EQUAL.getValue() + request.getRequestURL();
			                        statusDTO.setUrl(url);
			                    }
					        }
					    }
					    else
					        statusDTO.setFinished(false);
					    
					    if (StringUtils.isNotBlank(stage)) {
					    	statusDTO.setStage(stage);
					    }
					    
					    if (StringUtils.isNotBlank(stageSuccess)) {
					    	statusDTO.setStageSuccess(stageSuccess);
					    }
					}
				}
				catch (NumberFormatException nfe)
				{
                    String[] token = JspUtils.getCSRFTokenForPage(request, ImpexEnum.IMPEX_DOWNLOAD_URL.getValue());
                    ResolveImpexModuleVO modelVO = ImpexUtil.getImpexModelVO(null, moduleName, false);
                    String url = "";
                    if (modelVO != null)
                    {
                        url = ImpexEnum.IMPEX_DOWNLOAD_URL.getValue() + ImpexEnum.QUESTION.getValue() + ImpexEnum.FILENAME.getValue() + ImpexEnum.EQUAL.getValue() + modelVO.getUZipFileName() +
                                        ImpexEnum.AMPERCENT.getValue() + ImpexEnum.MODULEID.getValue() + ImpexEnum.EQUAL.getValue() + modelVO.getSys_id() + ImpexEnum.AMPERCENT.getValue() +
                                        token[0] + ImpexEnum.EQUAL.getValue() + token[1] + ImpexEnum.AMPERCENT.getValue() +
			                              HttpHeaders.REFERER + ImpexEnum.EQUAL.getValue() + request.getRequestURL();
                        statusDTO.setPercent("100");
                        statusDTO.setModule(moduleName);
                        statusDTO.setOperation(operation);
                        statusDTO.setFinished(true);
                        statusDTO.setStatus(status);
                        statusDTO.setUrl(url);
                    }
                }
				if (StringUtils.isNotBlank(impexLog) && 
				                impexLog.contains(ImpexEnum.IMPEX_LOG_ERROR_MARKER.getValue())) {
				    result.setSuccess(false).setData(statusDTO);
				    result.setErrorCode(ERR.IMPEX105.getCode());
				    result.setMessage(ERR.IMPEX105.getMessage());
                } else {
                    result.setSuccess(true).setData(statusDTO);
                }
			}
			else if (resultMap.get("ERROR") != null)
			{
    			String error = resultMap.get("ERROR");
    			result.setSuccess(false).setMessage(error);
			} else if (resultMap.get("STATUS") != null && resultMap.get("STATUS").equalsIgnoreCase("BUSY")) {
				statusDTO.setStatus(resultMap.get("STATUS"));
				result.setSuccess(true).setData(statusDTO);
			} else {
				statusDTO.setStatus(resultMap.get("UNKNOWN"));
				result.setSuccess(true).setData(statusDTO);
			}
		}
		catch (Exception e)
		{
			Log.log.error("Error importing impex definition", e);
			result.setSuccess(false).setMessage("Error importing impex definition");
		}
		
		if (Log.log.isTraceEnabled()) {
			Log.log.trace(String.format("AjaxImpex.impexStatus(%s) After DBPoolUsage [%s]", 
										operation,
										StringUtils.mapToString(
											new MInfo().dbpoolUsage(new HashMap<String, Object>()), "=", ", ")));
		}
		
		if (Log.log.isTraceEnabled()) {
			Log.log.trace(String.format("AjaxImpex.impexStatus(%s) returning Success=%b, Data/Error Msg=%s",
										operation, result.isSuccess(), 
										(result.isSuccess() ? result.getData() : result.getMessage())));
		}
		
		return result;
	}

	/**
	 * Upload a module present on different Resolve system by URL synchronously, for Import. The
	 * URL takes following parameters:
	 * <code>http://[IP_ADDRESS]:[PORT]/resolve/service/wiki/impex/download?filename=[MODULE_ZIP_NAME]&moduleId=&_username_=[USER_NAME]&_password_=[PASSWORD]</code>
	 * where <code>MODULE_ZIP_NAME</code> is modile zip file name with extension.
	 * For example, <code>test_module.zip</code>
	 * <p>
	 * 
	 * From 6.3 onwards this is synchronous call.
	 * 
	 * @param moduelURL
	 *            : String representing URL to upload a module for import.
	 * @param request
	 *            {@link HttpServletRequest} </code> containing
	 *            <code>USERNAME</code> of the user who is logged in to Resolve
	 * @param response
	 *            {@link HttpServletResponse}
	 * @return
	 * @throws ServletException
	 * @throws IOException
	 */
	@RequestMapping(value = "/impex/urlUpload", method = { RequestMethod.GET,
			RequestMethod.POST }, headers = "Accept=application/json; charset=utf-8")
	@ResponseBody
	public ResponseDTO<Object> urlUpload(@RequestParam("moduleURL") String moduleURL, HttpServletRequest request,
			HttpServletResponse response) throws ServletException, IOException {
		ResponseDTO<Object> result = new ResponseDTO<Object>();
		String userName = (String) request.getAttribute(Constants.HTTP_REQUEST_USERNAME);
		Pair<ResolveEventVO, Boolean> rsEventVOPair = null;
		
		try {
			if (Log.log.isDebugEnabled()) {
		        Log.log.debug(String.format("AjaxImpex.urlUpload(%s,%s) calling ServiceHibernate.getAndSetImpexEventValue...", 
		        							moduleURL, userName));
	    	}
			
			try {
				rsEventVOPair = ServiceHibernate.getAndSetImpexEventValue("URLUPLOAD", 5, "URL Upload File", userName);
			} catch (Exception e) {
				if (StringUtils.isBlank(e.getMessage()) || 
					!(e.getMessage().startsWith("Operation timed out after waiting for"))) {
		    		throw e;
		    	} else {
		    		if (Log.log.isDebugEnabled()) {
		    			Log.log.debug(String.format("AjaxImpex.urlUpload(%s,%s) " +
													"ServiceHibernate.getAndSetImpexEventValue timed  out after " +
													"waiting for 5 seconds", moduleURL, userName));
		    		}
		    	}
			}
			
			if (rsEventVOPair != null && Log.log.isDebugEnabled()) {
		        Log.log.debug(String.format("AjaxImpex.urlUpload(%s,%s) " +
											"ServiceHibernate.getAndSetImpexEventValue returned Resolve Event %s", 
											moduleURL, userName,
											(rsEventVOPair.getLeft() != null ? 
											 "[" + rsEventVOPair.getLeft() + "] which is " + 
											 (rsEventVOPair.getRight().booleanValue() ? "NEW" : "OLD") : "null")));
	        }
			
			if (rsEventVOPair != null && rsEventVOPair.getRight().booleanValue()) {
				ImpexUtil.initImpexOperationStage("URLUPLOAD", 
												  moduleURL.replaceAll(":", "<COLON>").replaceAll("-", "<DASH>")
												  .replaceAll("/", "<FORWARDSLASH>"), 1);
				String moduleName = ImpexUtil.uploadFileByURL(moduleURL, userName);
				ImpexUtil.updateImpexCount("URLUPLOAD");
				result.setSuccess(true).setData(moduleName);
			} else {
				String message = null;
	        	
	        	if (rsEventVOPair != null) {
	        		message = String.format("%s IMPEX operation by user %s is in progress, please try later", 
						   					 rsEventVOPair.getLeft().getModuleNameAndHasStage(null).getLeft(),
						   					 rsEventVOPair.getLeft().getSysCreatedBy());
	        	} else {
	        		message = "Another IMPEX operation is in progress, please try later";
	        	}
	        	
	        	Log.log.info(message);
	        	result.setSuccess(false).setMessage(message).setErrorCode(ERR.IMPEX104.getCode());
			}
		} catch (Exception e) {
			Log.log.error("Error upload impex definition by url", e);
			result.setSuccess(false).setMessage("Error upload impex definition by url");
		} finally {
			if (rsEventVOPair != null && rsEventVOPair.getRight().booleanValue()) {
				try {
					ResolveEventVO latestImpexEventVO = ServiceHibernate.getLatestImpexEventByValue();
					
					if (latestImpexEventVO != null) {
						Pair<String, Boolean> moduleNameAndHasStagePair = 
													latestImpexEventVO.getModuleNameAndHasStage("URLUPLOAD");
	                	
	                	if (moduleNameAndHasStagePair == null || StringUtils.isBlank(moduleNameAndHasStagePair.getLeft())) {
	                		throw new Exception (
	                						String.format("Failed to get URL from latest URLUPLOAD Rsolve Event [%s]",
	                									  latestImpexEventVO));
	                	}
	                	
	                	if (latestImpexEventVO.getUValue().equals(String.format("IMPEX-URLUPLOAD %s:URL Upload File,1,1", 
	                															moduleURL.replaceAll(":", "<COLON>")
	                															.replaceAll("-", "<DASH>")
	                															.replaceAll("/", "<FORWARDSLASH>")))) {
	                		latestImpexEventVO.setUValue(latestImpexEventVO.getUValue().replace(",1,1", ",-2,Done"));
	                		ServiceHibernate.insertResolveEvent(latestImpexEventVO);
			            
	                		if (Log.log.isTraceEnabled()) {
	                			Log.log.trace(String.format("AjaxImpex.urlUpload(%s,%s) inserted/updated Resolve Event [%s]", 
			            									moduleURL, userName, latestImpexEventVO));
	                		}
	                	} else {
	                		Log.log.error(String.format("Latest Impex Event Value [%s] is not same as expected " +
														"IMPEX-URLUPLOAD %s:URL Upload File,1,1", latestImpexEventVO,
														moduleURL.replaceAll(":", "<COLON>").replaceAll("-", "<DASH>")
														.replaceAll("/", "<FORWARDSLASH>")));
	                	}
					}
				} catch (Exception e) {
					Log.log.error(String.format("Error%sin getting latest Impex Resolve Event", 
												(StringUtils.isNotBlank(e.getMessage()) ? " [" + e.getMessage() + "] " : " ")));
				}
			}
		}

		return result;
	}

	/**
	 * Status of the module which is getting uploaded by URL can be checked here.
	 * This API is called from UI right after the <code>urlUpload</code> is called
	 * as that operation is async.
	 * <p>
	 * 
	 * @param request
	 *            {@link HttpServletRequest} </code> containing
	 *            <code>USERNAME</code> of the user who is logged in to Resolve
	 * @param response
	 *            {@link HttpServletResponse}
	 * @return
	 * @throws ServletException
	 * @throws IOException
	 */
	@RequestMapping(value = "/impex/urlUploadStatus", method = { RequestMethod.GET,
			RequestMethod.POST }, headers = "Accept=application/json; charset=utf-8")
	@ResponseBody
	public ResponseDTO<Object> urlUploadStatus(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		ResponseDTO<Object> result = new ResponseDTO<Object>();
		String userName = (String) request.getAttribute(Constants.HTTP_REQUEST_USERNAME);
		try {
			Map<String, String> resultMap = ImpexUtil.urlUploadStatus(userName);
			if (resultMap.get("SUCCESS") != null) {
				String percent = resultMap.get("SUCCESS");
				try {
					Integer.parseInt(percent);
					{
						result.setSuccess(true).setData(percent);
					}
				} catch (NumberFormatException nfe) {
					result.setSuccess(true).setData("100").setMessage(percent);
				}
			} else if (resultMap.get("ERROR") != null) {
				String error = resultMap.get("ERROR");
				result.setSuccess(true).setData("-1").setMessage(error);
			}
		} catch (Exception e) {
			Log.log.error("Error importing impex definition", e);
			result.setSuccess(false).setMessage("Error importing impex definition");
		}

		return result;
	}

	/**
	 * Prepares manifest for the module represented by <code>moduleName</code>
	 * <p>
	 * 
	 * @param moduleName
	 *            : String representing module name.
	 * @param operation
	 *            : String representing operation. It could either be import or
	 *            export.
	 * @param request
	 *            {@link HttpServletRequest} </code> containing
	 *            <code>USERNAME</code> of the user who is logged in to Resolve
	 * @param response
	 *            {@link HttpServletResponse}
	 * @return
	 * @throws ServletException
	 * @throws IOException
	 */
	@RequestMapping(value = "/impex/prepareManifest", method = { RequestMethod.GET,
			RequestMethod.POST }, headers = "Accept=application/json; charset=utf-8")
	@ResponseBody
	public ResponseDTO<Object> prepareManifest(@RequestParam("moduleName") String moduleName,
			@RequestParam("operation") String operation, HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		String userName = (String) request.getAttribute(Constants.HTTP_REQUEST_USERNAME);
		ResponseDTO<Object> result = new ResponseDTO<Object>();
		
		if (moduleName.contains(".zip")) {
			moduleName = moduleName.substring(0, moduleName.lastIndexOf(".zip"));
		}
		
		Pair<ResolveEventVO, Boolean> rsEventVOPair = null;
		
		try {
			if (Log.log.isDebugEnabled()) {
		        Log.log.debug(String.format("AjaxImpex.prepareManifest(%s,%s) calling " +
											"ServiceHibernate.getAndSetImpexEventValue...", 
											moduleName, userName));
	    	}
			
			try {
				rsEventVOPair = ServiceHibernate.getAndSetImpexEventValue("MANIFEST", 5, moduleName, userName);
			} catch (Exception e) {
				if (StringUtils.isBlank(e.getMessage()) || 
					!(e.getMessage().startsWith("Operation timed out after waiting for"))) {
		    		throw e;
		    	} else {
		    		if (Log.log.isDebugEnabled()) {
		    			Log.log.debug(String.format("AjaxImpex.prepareManifest(%s,%s) " +
													"ServiceHibernate.getAndSetImpexEventValue timed  out after " +
													"waiting for 5 seconds", 
													moduleName, userName));
		    		}
		    	}
			}
	        
	        if (rsEventVOPair != null && Log.log.isDebugEnabled()) {
		        Log.log.debug(String.format("AjaxImpex.prepareManifest(%s,%s) " +
											"ServiceHibernate.getAndSetImpexEventValue returned Resolve Event %s", 
											moduleName, userName,
											(rsEventVOPair.getLeft() != null ? 
											 "[" + rsEventVOPair.getLeft() + "] which is " + 
											 (rsEventVOPair.getRight().booleanValue() ? "NEW" : "OLD") : "null")));
	        }
	        
	        if (rsEventVOPair != null && rsEventVOPair.getRight().booleanValue()) {
				if (moduleName.contains(".zip")) {
					moduleName = moduleName.substring(0, moduleName.lastIndexOf(".zip"));
				}
				
				ImpexUtil.initImpexOperationStage("MANIFEST", "Prepare", 1);
				boolean status = ImpexService.prepareManifest(moduleName, operation.toUpperCase(), userName, true);
				// manifest generation is complete. Update resolve event as 'done'
				ImpexUtil.updateImpexCount("MANIFEST");
				
				result.setSuccess(status);
				
				if (status) {
					result.setData(status);
				}
				
				if (!status) {
					result.setMessage(String.format("Failed to prepare manifest for module %s and operation %s", 
													moduleName, operation));
				}
	        } else {
	        	String message = null;
	        	
	        	if (rsEventVOPair != null) {
	        		message = String.format("%s IMPEX operation by user %s is in progress, please try later", 
						   					 rsEventVOPair.getLeft().getModuleNameAndHasStage(null).getLeft(),
						   					 rsEventVOPair.getLeft().getSysCreatedBy());
	        	} else {
	        		message = "Another IMPEX operation is in progress, please try later";
	        	}
	        	
	        	Log.log.info(message);
	        	result.setSuccess(false).setMessage(message).setErrorCode(ERR.IMPEX104.getCode());
	        }
		} catch (Throwable e) {
			Log.log.error("Error executing prepareManifest.", e);
			result.setSuccess(false).setMessage("Error executing prepareManifest.");
		} finally {
			if (rsEventVOPair != null && rsEventVOPair.getRight().booleanValue()) {
				try {
					ResolveEventVO latestImpexEventVO = ServiceHibernate.getLatestImpexEventByValue();
					
					if (latestImpexEventVO != null) {
						Pair<String, Boolean> moduleNameAndHasStagePair = 
													latestImpexEventVO.getModuleNameAndHasStage("MANIFEST");
	                	
	                	if (moduleNameAndHasStagePair == null || StringUtils.isBlank(moduleNameAndHasStagePair.getLeft())) {
	                		throw new Exception (
	                						String.format("Failed to get module name from latest Impex Rsolve Event [%s]",
	                									  latestImpexEventVO));
	                	}
                	
	                	if (latestImpexEventVO.getUValue().equals(String.format("IMPEX-MANIFEST Prepare:%s,1,1", moduleName))) {	                	
		                	latestImpexEventVO.setUValue(String.format("IMPEX-MANIFEST:%s,-2,Done", moduleName));
							ServiceHibernate.insertResolveEvent(latestImpexEventVO);
				            
				            if (Log.log.isTraceEnabled()) {
				            	Log.log.trace(String.format("AjaxImpex.urlUpload(%s,%s) inserted/updated Resolve Event [%s]", 
				            								moduleName, userName, latestImpexEventVO));
				            }
	                	} else {
	                		Log.log.error(String.format("Latest Impex Event Value [%s] is not same as expected " +
	                									"[IMPEX-MANIFEST:%s,1,1]", latestImpexEventVO, moduleName));
	                	}
					}
				} catch (Exception e) {
					Log.log.error(String.format("Error%sin getting latest Impex Resolve Event", 
												(StringUtils.isNotBlank(e.getMessage()) ? " [" + e.getMessage() + "] " : " ")));
				}
			}
		}
		
		return result;
	}

	/**
	 * Get the status of already executing manifest operation.
	 * <p>
	 * 
	 * @param moduleName
	 *            : String representing mmodule name.
	 * @param operation
	 *            : String representing operation. It could either be import or
	 *            export.
	 * @param request
	 *            {@link HttpServletRequest} </code> containing
	 *            <code>USERNAME</code> of the user who is logged in to Resolve
	 * @param response
	 *            {@link HttpServletResponse}
	 * @return
	 * @throws ServletException
	 * @throws IOException
	 */
	@RequestMapping(value = "/impex/manifestStatus", method = { RequestMethod.GET,
			RequestMethod.POST }, headers = "Accept=application/json; charset=utf-8")
	@ResponseBody
	public ResponseDTO<Object> manifestStatus(@RequestParam("moduleName") String moduleName,
			@RequestParam("operation") String operation, HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		String userName = (String) request.getAttribute(Constants.HTTP_REQUEST_USERNAME);
		ResponseDTO<Object> result = new ResponseDTO<Object>();
		try {
			if (moduleName.contains(".zip")) {
				moduleName = moduleName.substring(0, moduleName.lastIndexOf(".zip"));
			}
			Object status = ImpexService.manifestStatus(moduleName, operation.toUpperCase(), userName);
			result.setData(status);
			result.setSuccess(true);
		} catch (Exception e) {
			Log.log.error("Error executing manifestStatus", e);
			result.setSuccess(false).setMessage("Error executing manifestStatus");
		}
		return result;
	}

	/**
	 * Retrieves the manifest information of the module specified by
	 * <code>moduleName</code>. Manifest lets user check whether the package which
	 * is getting imported already present on the system. If yes, then whether the
	 * package is the same or different.
	 * 
	 * @param query
	 *            {@link QueryDTO} <code>query</code> contains search, filter,
	 *            sorting and pagination information for the grid. Refer
	 *            <code>list</code> method above for more details.
	 * @param moduleName
	 *            A <code>String</code> containing the name of the module whoes
	 *            manifest information needs to be viewed.
	 * @param operation
	 *            A <code>String</code> containing impex operation. It could be
	 *            <code>import</code> or <code>export</code>.
	 * @param request
	 *            {@link HttpServletRequest} </code> containing
	 *            <code>USERNAME</code> of the user who is logged in to Resolve
	 * @param response
	 *            {@link HttpServletResponse}
	 * @return {@link ResponseDTO}
	 * @throws ServletException
	 * @throws IOException
	 */
	@RequestMapping(value = "/impex/getManifest", method = { RequestMethod.GET,
			RequestMethod.POST }, headers = "Accept=application/json; charset=utf-8")
	@ResponseBody
	public ResponseDTO<ResolveImpexManifestVO> getManifest(@ModelAttribute QueryDTO query,
			@RequestParam("moduleName") String moduleName, @RequestParam("operation") String operation,
			HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		ResponseDTO<ResolveImpexManifestVO> result = new ResponseDTO<ResolveImpexManifestVO>();
		String userName = (String) request.getAttribute(Constants.HTTP_REQUEST_USERNAME);
		try {
			// massage the query for the properties grid
			manipuateQueryDTOForPropertiesGrid(query);
			// List<ImpexComponentDTO> impexCompDTOList =
			// ServiceHibernate.getManifest(moduleName, userName);
			if (moduleName.contains(".zip")) {
				moduleName = moduleName.substring(0, moduleName.lastIndexOf(".zip"));
			}
			List<ResolveImpexManifestVO> impexCompDTOList = ImpexService.getManifest(query, moduleName, operation,
					userName);

			query.setModelName("ResolveImpexManifest");
			query.setWhereClause(
					"UOperationType = '" + operation.toUpperCase() + "' and UModuleName = '" + moduleName + "'");
			int total = ServiceHibernate.getTotalHqlCount(query);
			result.setSuccess(true).setRecords(impexCompDTOList).setTotal(total);
		} catch (Exception e) {
			Log.log.error("Error importing impex definition", e);
			result.setSuccess(false).setMessage("Error importing impex definition");
		}

		return result;
	}

	/**
	 * Retrieve the result of last impex operation.
	 * 
	 * @param moduleName
	 *            A <code>String</code> represending module name whoes impex result
	 *            is needed.
	 * @param request
	 *            {@link HttpServletRequest}
	 * @param response
	 *            {@link HttpServletResponse}
	 * @return {@link ResponseDTO} A success response containing <code>data</code>
	 *         as actual result as a <code>String</code>.
	 * @throws ServletException
	 * @throws IOException
	 */
	@RequestMapping(value = "/impex/getImpexResult", method = { RequestMethod.GET,
			RequestMethod.POST }, headers = "Accept=application/json; charset=utf-8")
	@ResponseBody
	public ResponseDTO<Object> getImpexResult(@RequestParam("moduleName") String moduleName, HttpServletRequest request,
			HttpServletResponse response) throws ServletException, IOException {
		ResponseDTO<Object> result = new ResponseDTO<Object>();
		String userName = (String) request.getAttribute(Constants.HTTP_REQUEST_USERNAME);
		result.setSuccess(true).setData("");

		try {
			String impexResult = ImpexUtil.getImpexResult(moduleName, userName);
			if (StringUtils.isNotBlank(impexResult)) {
				if (impexResult.contains("color=\"red\"")) {
					result.setSuccess(true).setData(impexResult).setMessage("ERROR");
				} else {
					result.setSuccess(true).setData(impexResult).setMessage("SUCCESS");
				}
			}
		} catch (Exception e) {
			Log.log.error("Error Getting Impex Status.", e);
			result.setSuccess(false).setMessage("Error Getting Impex Status.");
		}

		return result;
	}

	/**
	 * Validate a module represented by <code>moduleName</code>
	 * <p>
	 * 
	 * @param moduleName
	 *            A <code>String</code> represending module name whoes validation
	 *            needs to be performed.
	 * @param request
	 *            {@link HttpServletRequest}
	 * @param response
	 *            {@link HttpServletResponse}
	 * @return
	 * @throws ServletException
	 * @throws IOException
	 */
	@RequestMapping(value = "/impex/validateModule", method = { RequestMethod.GET,
			RequestMethod.POST }, headers = "Accept=application/json; charset=utf-8")
	@ResponseBody
	public ResponseDTO<ImpexComponentDTO> validateModule(@RequestParam("moduleName") String moduleName,
			HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		ResponseDTO<ImpexComponentDTO> result = new ResponseDTO<ImpexComponentDTO>();
		String userName = (String) request.getAttribute(Constants.HTTP_REQUEST_USERNAME);
		try {
			if (moduleName.contains(".zip")) {
				moduleName = moduleName.substring(0, moduleName.lastIndexOf(".zip"));
			}
			new ModuleValidationUtil(moduleName, userName).startValidation();

			result.setSuccess(true);
		} catch (Exception e) {
			Log.log.error("Error importing impex definition", e);
			result.setSuccess(false).setMessage("Error importing impex definition");
		}

		return result;
	}

	/**
	 * Download the module.
	 * <p>
	 * 
	 * @param fileName
	 *            A <code>String</code> representing file name of the module.
	 * @param moduleId
	 *            A <code>String</code> representing module id of the module.
	 * @param request
	 *            {@link HttpServletRequest} </code> containing
	 *            <code>USERNAME</code> of the user who is logged in to Resolve
	 * @param response
	 *            {@link HttpServletResponse}
	 * @return {@link ModelAndView}
	 * @throws ServletException
	 * @throws IOException
	 * @throws IntrusionException
	 * @throws ValidationException
	 */
	@RequestMapping(value = ConstantValues.URL_IMPEX_DOWNLOAD, method = { RequestMethod.GET })
	public ModelAndView downloadModule(@RequestParam("filename") String fileName,
			@RequestParam("moduleId") String moduleId, HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException, ValidationException, IntrusionException {
		String userName = (String) request.getAttribute(Constants.HTTP_REQUEST_USERNAME);
		try {
			// get the module name
			String moduleName = null;
			if (StringUtils.isNotEmpty(fileName) && fileName.indexOf(".zip") > -1) {
				moduleName = fileName.substring(0, fileName.lastIndexOf('.'));
			}

			// get the module
			ResolveImpexModuleVO module = ImpexUtil.getImpexModelZipContent(moduleId, moduleName, userName);
			if (module != null && module.getUZipFileContent() != null) {
				byte[] uZipFileContent = ESAPI.validator().getValidFileContent("safeContent",
						module.getUZipFileContent(), Integer.MAX_VALUE, false);
				ServletUtils.sendHeaders(fileName, uZipFileContent.length,
						Arrays.copyOf(uZipFileContent, uZipFileContent.length > 128 ? 128 : uZipFileContent.length),
						request, response);
				response.getOutputStream().write(uZipFileContent);
				// ServletUtils.sendHeaders(fileName, module.getUZipFileContent().length,
				// request, response);
				// response.getOutputStream().write(module.getUZipFileContent());
			} else {
				response.sendError(499, String.format("File %s not found.", fileName));
			}
		} catch (Exception e) {
			Log.log.error(e.getMessage(), e);
		}

		return null;

	}// importModule

	// private apis
	private void manipuateQueryDTOForPropertiesGrid(QueryDTO query) {

		// filter out the UI fields which do not map to database column.
		List<QuerySort> sorts = query.getSortItems();
		if (sorts != null) {
			for (QuerySort sort : sorts) {
				if (sort.getProperty().equalsIgnoreCase("group")) {
					sorts.remove(sort);
				}
			}

			String jsonSort = QuerySort.encodeJson(sorts);
			query.setSort(jsonSort);
		}
	}

	////////////////////////////
	/** Impex Version 2 (v2) controller API's **/
	////////////////////////////

	/**
	 * API to retrieve component details.
	 * 
	 * @param component
	 *            : String representing the type of the component.
	 *            <p>
	 *            For phase 1, only "runbook" is supported. In later implementation,
	 *            tasks will be implemented.
	 * @param id
	 *            : String representing sys_id of the component.
	 * @param parent: (Optional) String representing parent of the component. This is applicable
	 *                to action task only, when it's getting exported as part of an automation/DT
	 * @param subType: (Optional) String, introduced for gateway export,
	 *             representing gateway type. In case of SDK2 gateways, this field distinguishes
	 *             filters from one type to other.
	 *
	 * @param request
	 * @return A Map of String and an Object, where key could either be 'task',
	 *         'wiki' or 'ERROR' and the object is a list of Strings representing
	 *         the name of the components or an error message if the key is 'ERROR'.
	 * @throws ServletException
	 * @throws Exception
	 */
	@RequestMapping(value = "/impex/detail/{component}/{id}", method = {
			RequestMethod.GET }, headers = "Accept=application/json; charset=utf-8")
	@ResponseBody
	public ResponseDTO<Object> getCompDetails(@PathVariable String component,
	                @PathVariable String id,
	                @RequestParam (required = false) String parent,
	                @RequestParam (required = false) String subType,
	                HttpServletRequest request) throws ServletException, Exception {
		ResponseDTO<Object> result = new ResponseDTO<>();
		String username = (String) request.getAttribute(Constants.HTTP_REQUEST_USERNAME);
		try {
			Map<String, Object> map = ExportUtils.getCompDetails(component, id, parent, subType, username);
			if (map.containsKey(ImpexEnum.ERROR.getValue())) {
				result.setSuccess(false);
				result.setMessage((String) map.get(ImpexEnum.ERROR.getValue()));
			} else {
				result.setData(map);
				result.setSuccess(true);
			}
		} catch (Exception e) {
			result.setSuccess(false);
			result.setMessage(ImpexEnum.ERROR_RETRIEVING_COMPONENT_DETAILS.getValue());
			Log.log.error(ImpexEnum.ERROR_RETRIEVING_COMPONENT_DETAILS.getValue(), e);
		}

		return result;
	}
	
	/**
	 * API to export CEF Artifact Types. The playload will not contain a module name
	 * and it's description. The back end will assign one at runtime. These exported packages
	 * will not be visible from UI.
	 * 
	 * @param container
	 * <pre>{@code
	 *     {@link JSONObject} Entire payload of Impex definition. E.g.
	 *     {
                &quot;id&quot;: &quot;&quot;,
                &quot;uname&quot;: &quot;&quot;,
                &quot;udescription&quot;: &quot;&quot;,
                &quot;uforwardWikiDocument&quot;: &quot;&quot;,
                &quot;uscriptName&quot;: &quot;&quot;,
                &quot;impexDefinition&quot;: [
                    {
                        &quot;type&quot;: &quot;artifactType&quot;,
                        &quot;name&quot;: &quot;Host Name&quot;,
                        &quot;namespace&quot;: &quot;&quot;,
                        &quot;options&quot; : {&quot;includeCustomKeys&quot; : &quot;true&quot;}
                    }
                ]
            }
        }</pre>
	 * @param request
	 * @param response
	 * @return
	 * @throws Exception
	 */
	@RequestMapping(value="/impex/exportArtifact", method=RequestMethod.POST)
	@ResponseBody
	public ResponseDTO<Object> exportArtifact (@RequestBody JSONObject container, HttpServletRequest request,
	                HttpServletResponse response) throws Exception {
	    String moduleName = String.format("ResolveArtifactExport__%s", Guid.getGuid(true));
	    container.put("uname", moduleName);
	    container.put("udescription", "Resolve Artifact Export");
	    return saveAndExport(container, request, response);
	}

	@RequestMapping(value = { "/impex/saveAndExport" }, method = { RequestMethod.POST,
			RequestMethod.PUT }, headers = "Accept=application/json; charset=utf-8")
	@ResponseBody
	public ResponseDTO<Object> saveAndExport(@RequestBody JSONObject container, HttpServletRequest request,
			HttpServletResponse response) throws ServletException, IOException {
		ResponseDTO<Object> result = new ResponseDTO<Object>();
		String userName = (String) request.getAttribute(Constants.HTTP_REQUEST_USERNAME);
		String message = null;
		ResolveImpexModuleVO entity = null;
		String errMessage = null;
		
		try {
			entity = new ObjectMapper().readValue(container.toString(),
					ResolveImpexModuleVO.class);			

			if (Log.log.isDebugEnabled()) {
		        Log.log.debug(String.format("AjaxImpex.saveAndExport(%s,%s) calling " +
											"ServiceHibernate.getAndSetImpexEventValue...", 
											entity.getUName(), userName));
	    	}
			
			Pair<ResolveEventVO, Boolean> rsEventVOPair = null;
			
			try {
				rsEventVOPair = ServiceHibernate.getAndSetImpexEventValue("EXPORT", 5, entity.getUName(), userName);
			} catch (Exception e) {
				if (StringUtils.isBlank(e.getMessage()) || 
					!(e.getMessage().startsWith("Operation timed out after waiting for"))) {
	    			throw e;
	    		} else {
	    			if (Log.log.isDebugEnabled()) {
	    				Log.log.debug(String.format("AjaxImpex.saveAndExport(%s,%s) " +
													"ServiceHibernate.getAndSetImpexEventValue timed out after " +
													"waiting for 5 seconds", 
													entity.getUName(), userName));
	    			}
	    		}
			}
			
	        if (rsEventVOPair != null && Log.log.isDebugEnabled()) {
		        Log.log.debug(String.format("AjaxImpex.saveAndExport(%s,%s) " +
											"ServiceHibernate.getAndSetImpexEventValue returned Resolve Event %s", 
											entity.getUName(), userName,
											(rsEventVOPair.getLeft() != null ? 
											 "[" + rsEventVOPair.getLeft() + "] which is " + 
											 (rsEventVOPair.getRight().booleanValue() ? "NEW" : "OLD") : "null")));
	        }
	        
	        if (rsEventVOPair != null && rsEventVOPair.getRight().booleanValue()) {
	        	ResolveImpexModuleVO savedVO = ImpexUtil.saveResolveImpexModule(entity, userName, true, "SAVE");
	        	
				List<String> excludeList = new ArrayList<>();
				if (StringUtils.isNotBlank(entity.getExcludeList())) {
				    excludeList.addAll(Arrays.asList(entity.getExcludeList().split(",")));
				}
				message = ImpexUtil.exportModule(entity.getUName(), excludeList, true, userName);
				result.setSuccess(true).setData(true);
				if (StringUtils.isNotBlank(message)) {
					result.setMessage(message).setData(false).setErrorCode(ERR.IMPEX104.getCode());
				}
	
				result.setSuccess(true).setData(savedVO);
	        } else {
	        	if (rsEventVOPair != null) {
		        	message = String.format("%s IMPEX operation by user %s is in progress, please try later", 
											rsEventVOPair.getLeft().getModuleNameAndHasStage(null).getLeft(),
											rsEventVOPair.getLeft().getSysCreatedBy());
	        	} else {
	        		message = "Another IMPEX operation is in progress, please try later";
	        	}
	        	
	        	Log.log.info(message);
	        	result.setSuccess(false).setMessage(message);
	        }
		} catch (Throwable e) {
			Log.log.error("Error while save and export Impex definition(s)." + e);
			errMessage = e.getMessage();
			if (StringUtils.isBlank(errMessage))
			{
			    errMessage = "Error while save and export Impex definition.";
			}
			result.setSuccess(false).setMessage(errMessage);
		} finally {
			// If error is module already exists then set the Resolve Event to export failed.
			
			if (StringUtils.isNotBlank(errMessage) && errMessage.contains(ImpexUtil.MODULE_ALREADY_EXISTS_MARKER)) {
				try {
					ResolveEventVO eventVO = ServiceHibernate.getLatestImpexEventByValue();
					
			        if (Log.log.isDebugEnabled()) {
				        Log.log.debug(String.format("AjaxImpex.saveAndExport(%s,%s) calling " +
				        							"ServiceHibernate.getLatestImpexEventByValue returned " +
				        							"Resolve Event [%s]", 
				        							(entity != null && StringUtils.isNotBlank(entity.getUName()) ? 
				        							 entity.getUName() : "null"), userName, 
				        							(eventVO != null ? eventVO : "null")));
			        }
			        
			        if (eventVO != null)
			        {
			            eventVO.setUValue(StringUtils.replace(eventVO.getUValue(), ",1,0", ",-1,0"));
			            
			            try {
			            	ServiceHibernate.insertResolveEvent(eventVO);
			            } catch (Exception e) {
			            	// Ignore nothing can be done here, user has to wait max 5 minutes to start next Impex operation
			            }
			            
			            if (Log.log.isDebugEnabled()) {
			            	Log.log.debug(String.format("AjaxImpex.saveAndExport(%s,%s) inserted/updated Resolve Event [%s]", 
			            								(entity != null && StringUtils.isNotBlank(entity.getUName()) ? 
			            								 entity.getUName() : "null"), userName, eventVO));
			            }
			        }
				} catch (Exception e) {
					Log.log.error(String.format("Error%sin getting latest Impex Resolve Event", 
												(StringUtils.isNotBlank(e.getMessage()) ? " [" + e.getMessage() + "] " : " ")));
				}
			}
		}
		
		return result;
	}
	
    @RequestMapping(value = {"/impex/manifestGraph/{id}"}, method = { RequestMethod.GET }, headers = "Accept=application/json; charset=utf-8")
    @ResponseBody
    public ResponseDTO<Object> getManifestGraph(@PathVariable String id, @RequestParam boolean isDirty, HttpServletRequest request) throws ServletException, IOException
    {
        ResponseDTO<Object> result = new ResponseDTO<Object>();
        String userName = (String) request.getAttribute(Constants.HTTP_REQUEST_USERNAME);
        Map<String, String> resultMap = new HashMap<>();
        try
        {
            resultMap = ImportUtils.getManifestGraph(id, isDirty, userName);
            result.setSuccess(true);
        }
        catch(Exception e)
        {
            result.setSuccess(false);
            resultMap.put(ImpexEnum.MESSAGE.getValue(), String.format(ERR.IMPEX101.getMessage(), id));
            resultMap.put(ImpexEnum.ERROR_CODE.getValue(), ERR.IMPEX101.getCode());
            Log.log.error(String.format(ERR.IMPEX101.getMessage(), id), e);
        }
        result.setData(resultMap);
        return result;
    }
}
