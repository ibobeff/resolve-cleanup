package com.resolve.rsview.controller;

import java.util.List;

import org.apache.commons.collections.CollectionUtils;
import org.springframework.http.HttpStatus;
import org.springframework.validation.BindException;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.ResponseStatus;

import com.resolve.search.model.UserInfo;
import com.resolve.services.hibernate.vo.UsersVO;
import com.resolve.services.hibernate.wiki.SaveHelper;
import com.resolve.services.util.UserUtils;
import com.resolve.services.vo.ResponseDTO;
import com.resolve.util.Log;
import com.resolve.util.StringUtils;

public class GenericController
{
    @SuppressWarnings("rawtypes")
    @ExceptionHandler(BindException.class)
    @ResponseStatus(HttpStatus.BAD_REQUEST)
    @ResponseBody
    public ResponseDTO handle(BindException e) throws Throwable {
        ResponseDTO result = new ResponseDTO();
        Log.log.error("Error parsing incoming request", e);
        result.setSuccess(false).setMessage("Error parsing incoming request");
        
        return result;
    }
    
    protected final UserInfo getUserInfo(UsersVO user) throws Exception
    {
        if (user == null || StringUtils.isBlank(user.getSys_id()) || StringUtils.isBlank(user.getUUserName()))
        {
            throw new Exception("Invalid user object.");
        }

        UserInfo result = new UserInfo(user.getSys_id(), user.getSysOrg(), user.getUUserName(), user.getRoles(), UserUtils.isAdminUser(user));
        return result;
    }

    protected final String checkAndSetErrorMessage(Exception e, String defaultErrMsg) {
    	String errMsg = defaultErrMsg;
    	
    	if (e != null && StringUtils.isNotBlank(e.getMessage()) && 
    		e.getMessage().startsWith(SaveHelper.FAILED_TO_SAVE_OBJECT_PREFIX) &&
            (e.getMessage().endsWith(SaveHelper.FAILED_TO_SAVE_OBJECT_SUFFIX) ||
             e.getMessage().endsWith(SaveHelper.FAILED_TO_SAVE_OBJECT_HA_VIOLATED_SUFFIX) ||
             e.getMessage().endsWith(SaveHelper.FAILED_TO_SAVE_OBJECT_USER_COUNT_VIOLATED_SUFFIX) ||
             e.getMessage().contains(SaveHelper.DIRECTLY_LINKED_TO_ITSELF))) {
        	errMsg = e.getMessage();
        } else {
        	errMsg = e.getMessage();
        	if (errMsg.equals(defaultErrMsg)) {
        		Log.log.error(errMsg, e);
        	} else {
        		Log.log.warn(errMsg);
        	}
        }
    	
    	return errMsg;
    }
    
    protected final String checkAndSetErrorMessage(Throwable e, String defaultErrMsg, List<String> startsWith, 
    											   List<String> contains, List<String> endsWith) {
    	String errMsg = defaultErrMsg;
    	
    	if (e != null) {
    		if (StringUtils.isNotBlank(e.getMessage())) {
		    	if ((e.getMessage().startsWith(SaveHelper.FAILED_TO_SAVE_OBJECT_PREFIX) &&
		             (e.getMessage().endsWith(SaveHelper.FAILED_TO_SAVE_OBJECT_SUFFIX) ||
		              e.getMessage().endsWith(SaveHelper.FAILED_TO_SAVE_OBJECT_HA_VIOLATED_SUFFIX) ||
		              e.getMessage().endsWith(SaveHelper.FAILED_TO_SAVE_OBJECT_USER_COUNT_VIOLATED_SUFFIX))) ||
		    		(CollectionUtils.isNotEmpty(startsWith) && 
			         startsWith.stream().filter(s -> e.getMessage().startsWith(s)).findAny().orElse(null) != null) ||
		    		(CollectionUtils.isNotEmpty(contains) && 
		    		 contains.stream().filter(s -> e.getMessage().contains(s)).findAny().orElse(null) != null) ||
		    		(CollectionUtils.isNotEmpty(endsWith) && 
		    		 endsWith.stream().filter(s -> e.getMessage().endsWith(s)).findAny().orElse(null) != null)) {
		        	errMsg = e.getMessage();
		        	
		        	if (!e.getMessage().startsWith(SaveHelper.FAILED_TO_SAVE_OBJECT_PREFIX)) {
		        		Log.log.warn(errMsg);
		        	}
		    	}
    		} else if (e.getCause() != null && StringUtils.isNotBlank(e.getCause().getMessage()) ) {
    			if ((e.getCause().getMessage().startsWith(SaveHelper.FAILED_TO_SAVE_OBJECT_PREFIX) &&
   		             (e.getCause().getMessage().endsWith(SaveHelper.FAILED_TO_SAVE_OBJECT_SUFFIX) ||
   		              e.getCause().getMessage().endsWith(SaveHelper.FAILED_TO_SAVE_OBJECT_HA_VIOLATED_SUFFIX) ||
   		              e.getCause().getMessage().endsWith(SaveHelper.FAILED_TO_SAVE_OBJECT_USER_COUNT_VIOLATED_SUFFIX))) ||
   		    		(CollectionUtils.isNotEmpty(startsWith) && 
   			         startsWith.stream().filter(s -> e.getCause().getMessage().startsWith(s)).findAny().orElse(null) != null) ||
   		    		(CollectionUtils.isNotEmpty(contains) && 
   		    		 contains.stream().filter(s -> e.getCause().getMessage().contains(s)).findAny().orElse(null) != null) ||
   		    		(CollectionUtils.isNotEmpty(endsWith) && 
   		    		 endsWith.stream().filter(s -> e.getCause().getMessage().endsWith(s)).findAny().orElse(null) != null)) {
   		        	errMsg = e.getCause().getMessage();
   		        	
   		        	if (!e.getCause().getMessage().startsWith(SaveHelper.FAILED_TO_SAVE_OBJECT_PREFIX)) {
   		        		Log.log.warn(errMsg);
   		        	}
   		    	}
	    	} else {
	    		Log.log.error(errMsg, e);
	    	}
    	} else {
            Log.log.error(errMsg, e);
        }
    	
    	return errMsg;
    }
}
