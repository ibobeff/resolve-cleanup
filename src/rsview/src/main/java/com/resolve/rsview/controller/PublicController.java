/******************************************************************************
* (C) Copyright 2014
*
* Resolve Systems, LLC
*
* All rights reserved
* This software is distributed under license and may not be used, copied,
* modified, or distributed without the express written permission of
* Resolve Systems, LLC.
*
******************************************************************************/

package com.resolve.rsview.controller;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.owasp.esapi.ESAPI;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import com.resolve.services.constants.ConstantValues;

/**
 * 
 * Public Controller calls all the corresponding API's from {@link ViewController}. It's main purpose is to have backward compatibility. 
 * <br>
 * Please refer to View Controller documentation for more details.
 *
 */
@Controller
public class PublicController extends GenericController
{
    /**
     * Refer to {@link ViewController#view(String, String, HttpServletRequest, HttpServletResponse) ViewController.view}
     */
	@RequestMapping(value = ConstantValues.URL_PUBLIC_VIEWDOC, method = {RequestMethod.GET, RequestMethod.POST})
	public ModelAndView view(@PathVariable(ConstantValues.WIKI_NAMESPACE_KEY) String namespace, @PathVariable(ConstantValues.WIKI_DOCNAME_KEY) String docname, HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException
	{
	    return new ViewController().view(namespace, docname, request, response);
	}//view
	
	/**
     * Refer to {@link ViewController#view(HttpServletRequest, HttpServletResponse) ViewController.view}
     */
	@RequestMapping(value = ConstantValues.URL_PUBLIC_VIEW, method = RequestMethod.GET)
	public ModelAndView view(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException
	{
	    return new ViewController().view(request, response);
	}//view
	
//	@RequestMapping(value = ConstantValues.URL_PUBLIC_VIEWMODELC, method = RequestMethod.GET)
//	public ModelAndView viewmodelc(@PathVariable(ConstantValues.WIKI_NAMESPACE_KEY) String namespace, @PathVariable(ConstantValues.WIKI_DOCNAME_KEY) String docname, HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException
//	{
//		return new ViewController().viewmodelc(namespace, docname, request, response);
//	} // viewmodelc
	
//	@RequestMapping(value = ConstantValues.URL_PUBLIC_VIEWEXCPC, method = RequestMethod.GET)
//	public ModelAndView viewexcpc(@PathVariable(ConstantValues.WIKI_NAMESPACE_KEY) String namespace, @PathVariable(ConstantValues.WIKI_DOCNAME_KEY) String docname, HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException
//	{
//	    return new ViewController().viewexcpc(namespace, docname, request, response);
//	} // viewexcpc
	
	/**
     * Refer to {@link ViewController#viewattach(String, String, HttpServletRequest, HttpServletResponse) ViewController.viewattach}
     */
	@RequestMapping(value = ConstantValues.URL_PUBLIC_VIEWATTACH, method = RequestMethod.GET)
	public ModelAndView viewattach(@PathVariable(ConstantValues.WIKI_NAMESPACE_KEY) String namespace, @PathVariable(ConstantValues.WIKI_DOCNAME_KEY) String docname, HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException
	{
	    return new ViewController().viewattach(namespace, docname, request, response);
	} // viewattach
	
	/**
     * Refer to {@link ViewController#download(String, String, HttpServletRequest, HttpServletResponse) ViewController.download}
     */
	@RequestMapping(value = ConstantValues.URL_PUBLIC_DOWNLOAD, method = RequestMethod.GET)
	public ModelAndView download(@PathVariable(ConstantValues.WIKI_NAMESPACE_KEY) String namespace, @PathVariable(ConstantValues.WIKI_DOCNAME_KEY) String docname, HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException
	{
	    return new ViewController().download(namespace, docname, request, response);
	} // download
	
	/**
     * Refer to {@link ViewController#lookup(HttpServletRequest, HttpServletResponse) ViewController.lookup}
     */
	@RequestMapping(value = ConstantValues.URL_PUBLIC_LOOKUP, method = {RequestMethod.GET, RequestMethod.POST})
	public ModelAndView lookup(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException
	{
	    return new ViewController().lookup(request, response);
	} // lookup

//	@RequestMapping(value = ConstantValues.URL_PUBLIC_CURRENT, method = {RequestMethod.GET, RequestMethod.POST})
//	public ModelAndView current(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException
//	{
//	    return new ViewController().current(request, response);
//	} // current

	/**
     * Refer to {@link ViewController#renderWikiDocumentContent(HttpServletRequest, HttpServletResponse) ViewController.renderWikiDocumentContent}
     */
	@RequestMapping(value = ConstantValues.URL_PUBLIC_INCLUDE_DOC_CONTENT, method = {RequestMethod.POST, RequestMethod.GET} )
    @ResponseBody
    public String renderWikiDocumentContent(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException
    {
	    return ESAPI.encoder().encodeForHTML(new ViewController().renderWikiDocumentContent(request, response));
    } // renderWikiDocumentContent
	
} // PublicController
