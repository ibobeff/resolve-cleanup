/******************************************************************************
* (C) Copyright 2014
*
* Resolve Systems, LLC
*
* All rights reserved
* This software is distributed under license and may not be used, copied,
* modified, or distributed without the express written permission of
* Resolve Systems, LLC.
*
******************************************************************************/

package com.resolve.rsview.controller;

import java.util.Collections;
import java.util.Comparator;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.TreeMap;
import java.util.function.Function;

import javax.servlet.http.HttpServletRequest;

import org.elasticsearch.ElasticsearchException;
import org.springframework.stereotype.Controller;
import org.springframework.stereotype.Service;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.resolve.search.content.ContentSearchAPI;
import com.resolve.services.hibernate.vo.UsersVO;
import com.resolve.services.util.UserUtils;
import com.resolve.services.vo.NamespaceResponseDTO;
import com.resolve.services.vo.ResponseDTO;
import com.resolve.services.vo.SearchAttributesDTO;
import com.resolve.services.vo.SearchRecordDTO;
import com.resolve.services.vo.SearchResponseDTO;
import com.resolve.util.ConstantMessages;
import com.resolve.util.Constants;
import com.resolve.util.Log;

/**
 * 
 * Controller for namespaces.
 * 
 */
@Controller
@Service
@RequestMapping("/search/aggregation")
public class AjaxSearchAggregation extends GenericController {

	/**
	 * Provides aggregated list of namespaces for actions tasks, wiki documents and
	 * Resolve properties with the possibility to filter them based on different
	 * criteria.
	 * 
	 * @throws Exception
	 */
	@RequestMapping(value = "/namespaces", method = { RequestMethod.POST, RequestMethod.GET })
	@ResponseBody
	public NamespaceResponseDTO namespaces(@ModelAttribute SearchAttributesDTO searchDTO,
			@RequestParam(defaultValue = "") String problemId,
			@RequestParam(defaultValue = "", value = "RESOLVE.ORG_ID") String orgId,
			@RequestParam(defaultValue = "", value = "RESOLVE.ORG_NAME") String orgName, HttpServletRequest request) {
		String username = (String) request.getAttribute(Constants.HTTP_REQUEST_USERNAME);

		NamespaceResponseDTO result = new NamespaceResponseDTO();
		try {
			UsersVO user = UserUtils.getUser(username);
			SearchResponseDTO searchResult = ContentSearchAPI.searchAllDocuments(searchDTO, getUserInfo(user),
					ContentSearchAPI.AGGREGATION_FIELD_NAMESPACE);
			// building response tree
			result = getRecordTree(searchResult, x -> {
				return x.getNamespace();
			}, "\\.");
		} catch (ElasticsearchException e) {
			Log.log.error(ConstantMessages.ERROR_SEARCHING_WITH_TERM, e);
			result.setSuccess(false).setMessage(ConstantMessages.ERROR_SEARCHING_WITH_TERM);
		} catch (Exception e) {
			Log.log.error(ConstantMessages.ERROR_PERFORMING_SEARCH, e);
			result.setSuccess(false).setMessage(ConstantMessages.ERROR_PERFORMING_SEARCH);
		}

		return result;
	}

	/**
	 * Provides aggregated list of menupaths for actions tasks, wiki documents and
	 * Resolve properties with the possibility to filter them based on different
	 * criteria.
	 * 
	 * @throws Exception
	 */
	@RequestMapping(value = "/menupaths", method = { RequestMethod.POST, RequestMethod.GET })
	@ResponseBody
	public NamespaceResponseDTO menupaths(@ModelAttribute SearchAttributesDTO searchDTO,
			@RequestParam(defaultValue = "") String problemId,
			@RequestParam(defaultValue = "", value = "RESOLVE.ORG_ID") String orgId,
			@RequestParam(defaultValue = "", value = "RESOLVE.ORG_NAME") String orgName, HttpServletRequest request) {
		String username = (String) request.getAttribute(Constants.HTTP_REQUEST_USERNAME);
		NamespaceResponseDTO result = new NamespaceResponseDTO();
		try {
			UsersVO user = UserUtils.getUser(username);
			SearchResponseDTO searchResult = ContentSearchAPI.searchAllDocuments(searchDTO, getUserInfo(user),
					ContentSearchAPI.AGGREGATION_FIELD_MENUPATH);

			// building response tree
			result = getRecordTree(searchResult, x -> {
				return x.getMenupath();
			}, "/");

		} catch (ElasticsearchException e) {
			Log.log.error(ConstantMessages.ERROR_SEARCHING_WITH_TERM, e);
			result.setSuccess(false).setMessage(ConstantMessages.ERROR_SEARCHING_WITH_TERM);
		} catch (Exception e) {
			Log.log.error(ConstantMessages.ERROR_PERFORMING_SEARCH, e);
			result.setSuccess(false).setMessage(ConstantMessages.ERROR_PERFORMING_SEARCH);
		}
		return result;
	}

	@RequestMapping(value = "/authors", method = { RequestMethod.POST, RequestMethod.GET })
	@ResponseBody
	public ResponseDTO<String> authors(@RequestParam(defaultValue = "") String problemId,
			@RequestParam(defaultValue = "", value = "RESOLVE.ORG_ID") String orgId,
			@RequestParam(defaultValue = "", value = "RESOLVE.ORG_NAME") String orgName, HttpServletRequest request) {
		String username = (String) request.getAttribute(Constants.HTTP_REQUEST_USERNAME);

		ResponseDTO<String> response = new ResponseDTO<>();
		try {
			UsersVO user = UserUtils.getUser(username);
			List<String> authors = ContentSearchAPI.getAuthors(getUserInfo(user));
			Collections.sort(authors, String.CASE_INSENSITIVE_ORDER);
			
			response.setRecords(authors);
		} catch (ElasticsearchException e) {
			Log.log.error(ConstantMessages.ERROR_SEARCHING_WITH_TERM, e);
			response.setSuccess(false).setMessage(ConstantMessages.ERROR_SEARCHING_WITH_TERM);
		} catch (Exception e) {
			Log.log.error(ConstantMessages.ERROR_PERFORMING_SEARCH, e);
			response.setSuccess(false).setMessage(ConstantMessages.ERROR_PERFORMING_SEARCH);
		}

		return response;
	}

	private NamespaceResponseDTO getRecordTree(SearchResponseDTO response, Function<SearchRecordDTO, String> valueFun,
			String delimiter) {

		NamespaceResponseDTO allNamespaces = new NamespaceResponseDTO();
		allNamespaces.setName("all");

		Map<String, NamespaceResponseDTO> namespaceToItem = new TreeMap<>();
		for (SearchRecordDTO record : response.getRecords()) {
			String value = valueFun.apply(record);
			// removing delimiter from the beginning of the value
			value = value.replaceAll("^" + delimiter + "+", "");
			if (!value.isEmpty()) {
				NamespaceResponseDTO parent = allNamespaces;
				String[] parts = value.split(delimiter);
				String currentNamespace = "";
				String tempDelim = "";
				for (String part : parts) {
					currentNamespace = currentNamespace + tempDelim + part;
					tempDelim = delimiter;
					NamespaceResponseDTO item = namespaceToItem.get(currentNamespace);
					if (item == null) {
						item = new NamespaceResponseDTO();
						item.setName(part);
						item.setLeaf(true);
						namespaceToItem.put(currentNamespace, item);
						List<NamespaceResponseDTO> records = parent.getRecords();
						if (records == null) {
							records = new LinkedList<>();
							parent.setRecords(records);
						}
						records.add(item);
						sort(records);
					}

					parent.setLeaf(false);
					parent = item;
				}
			}

		}
		sort(allNamespaces.getRecords());
		return allNamespaces;
	}

	private static void sort(List<NamespaceResponseDTO> list) {
		if (list != null && !list.isEmpty()) {
			Collections.sort(list, new Comparator<NamespaceResponseDTO>() {

				@Override
				public int compare(NamespaceResponseDTO o1, NamespaceResponseDTO o2) {
					return o1.getName().compareToIgnoreCase(o2.getName());
				}

			});
		}
	}
}
