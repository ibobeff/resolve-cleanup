/******************************************************************************
* (C) Copyright 2014
*
* Resolve Systems, LLC
*
* All rights reserved
* This software is distributed under license and may not be used, copied,
* modified, or distributed without the express written permission of
* Resolve Systems, LLC.
*
******************************************************************************/

package com.resolve.rsview.main;

import java.util.Map;

import com.resolve.rsbase.BaseMetric;
import com.resolve.util.MetricThresholdType;

/**
 * @author alokika.dash
 *
 */

/**
 * This class is used for receiving message in RSVIEW to update thresholds used in metrics.
 */
public class MMetric
{
    public static void updateMetricThresholds(Map<String, MetricThresholdType> params)
    {
        // update thresholds
        BaseMetric.updateMetricThresholds(params);
    } //updateMetricThresholds
    
} // MMetric