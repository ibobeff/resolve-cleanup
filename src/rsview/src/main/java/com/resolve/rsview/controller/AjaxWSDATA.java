/******************************************************************************
* (C) Copyright 2014
*
* Resolve Systems, LLC
*
* All rights reserved
* This software is distributed under license and may not be used, copied,
* modified, or distributed without the express written permission of
* Resolve Systems, LLC.
*
******************************************************************************/

package com.resolve.rsview.controller;

import java.io.IOException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.stereotype.Controller;
import org.springframework.stereotype.Service;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.resolve.services.ServiceHibernate;
//import com.resolve.search.worksheet.WorksheetSearchAPI;
//import com.resolve.services.ServiceCassandra;
import com.resolve.services.ServiceWorksheet;
import com.resolve.services.vo.ResponseDTO;
import com.resolve.util.Constants;
import com.resolve.util.Log;
import com.resolve.util.StringUtils;

import net.sf.json.JSON;
import net.sf.json.JSONSerializer;
import net.sf.json.JsonConfig;

/**
 * 
 * This controller has the end points to store data in Worksheet (WSDATA). 
 * <p> 
 * The idea is to share data between Runbook executions. For eg, if there is a scenario where data needs to be communicated between Runbook execution, one of the runbook </br>
 * will 'set' a value and the other runbook can 'get' that value. 
 * <p> 
 * In simple terms, its like storing a Map in a Worksheet.
 * <p> 
 * For more information about WSDATA, please refer to <a href="http://community.gen-e.com/resolve/jsp/rsclient.jsp">Resolve documentation</a>. 
 * <p>
 * DB Table: This data is stored in Cassandra and not in a SQL database </br>
 * Url : {@code http://localhost:8080/resolve/jsp/rsclient.jsp#RS.worksheet.Worksheets/} (Click on 'Edit' of WS, than goto 'Data' tab to get the WSDATA info) 
 * 
 * 
 * @author jeet.marwah
 *
 */

@Controller
@Service
public class AjaxWSDATA extends GenericController
{
    /**
     * This is a 'get' api to fetch the data/values set in a Worksheet.
     *  
     * <p> 
     * <u><b>Example</b></u>:</br> 
     * <code><pre>
     *  id:07715c659d8f444aa0e3447ab551c9ed -- (worksheet Id)
     * </pre></code>
     * 
     * <p>
     * Return object in response:
     * <code><pre>
     *  'data' : Map<String, Object>
     * </pre></code>
     * 
     * @param problemid sysId of a Worksheet 
     * @param keyList  List of Keys to be retreived from WorksheetWSDATA. If null or empty, entire map is returned.
     * @param request  HttpServletRequest
     * @param response  HttpServletResponse
     * @return {@link ResponseDTO} - Reponse Data Transfer Object having a Map<String, Object> with key-value
     * @throws ServletException
     * @throws IOException
     */
    @RequestMapping(value = "/wsdata/getMap", method = { RequestMethod.GET, RequestMethod.POST }, headers = "Accept=application/json; charset=utf-8")
    @ResponseBody
    public ResponseDTO getWorksheetWSDATA(@RequestParam String problemId, @RequestParam(required = false) List<String> keyList, HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException
    {
        ResponseDTO result = new ResponseDTO();

        try
        {
            if (StringUtils.isNotBlank(problemId))
            {
                String username = (String) request.getAttribute(Constants.HTTP_REQUEST_USERNAME);
                Map<String, Object> map = ServiceWorksheet.getWorksheetWSDATA(problemId, keyList, username);
                if (map != null)
                {
                    result.setData(map);
                    result.setSuccess(true);
                }
                else
                {
                    result.setSuccess(false).setMessage("WSDATA get: worksheet doesn't exist: " + problemId);
                }
            }
            else
            {
                //send success=true to avoid the pop up on the screen 
                Log.log.warn("Problem Id is null");
                result.setSuccess(true);
            }
        }
        catch (Exception e)
        {
            Log.log.error("Error getting WSDATA.", e);
            result.setSuccess(false).setMessage("Error getting WSDATA.");
        }

        return result;
    }

    /**
     * Get a specific value from worksheet map for a key.
     * 
     * <p> 
     * <u><b>Example</b></u>:</br> 
     * <code><pre>
     *  problemId: 07715c659d8f444aa0e3447ab551c9ed -- (worksheet Id)
     * </pre></code>
     * 
     * <p>
     * Return object in response:
     * <code><pre>
     *  'data' : Object, which is the value of the 'propertyName' 
     * </pre></code>
     *  
     * @param problemid sysId of a Worksheet 
     * @param propertyName Key name whose value is to be retrieved.
     * @param request  HttpServletRequest
     * @param response  HttpServletResponse
     * @return {@link ResponseDTO} - Reponse Data Transfer Object having a 'Object' value for the key
     * @throws ServletException
     * @throws IOException
     */
    @RequestMapping(value = "/wsdata/getValue", method = { RequestMethod.GET, RequestMethod.POST }, headers = "Accept=application/json; charset=utf-8")
    @ResponseBody
    public ResponseDTO getWorksheetWSDATAProperty(@RequestParam String problemId, @RequestParam String propertyName, HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException
    {
        ResponseDTO result = new ResponseDTO();

        try
        {
            String username = (String) request.getAttribute(Constants.HTTP_REQUEST_USERNAME);
            Object property = ServiceWorksheet.getWorksheetWSDATAProperty(problemId, propertyName, username);
            result.setData(property);
            result.setSuccess(true);
        }
        catch (Exception e)
        {
            Log.log.error("Error getting WSDATA property.", e);
            result.setSuccess(false).setMessage("Error getting WSDATA property.");
        }

        return result;
    }

    /**
     * Put a key-value pair in worksheet map. To add a value manually, goto Worksheet -- Edit -- Data Tab -- Click Add
     * 
     * <p> 
     * <u><b>Example</b></u>:</br> 
     * <code><pre>
     *  propertyName:key
     *  propertyValue:value
     *  problemId: 07715c659d8f444aa0e3447ab551c9ed -- (worksheet Id)
     * </pre></code>
     * 
     * @param problemid sysId of a Worksheet
     * @param propertyName Key name 
     * @param propertyValue Value to be set
     * @param request  HttpServletRequest
     * @param response  HttpServletResponse
     * @return {@link ResponseDTO} - Reponse Data Transfer Object with success value as 'true' or 'false'
     * @throws ServletException
     * @throws IOException
     */

    @RequestMapping(value = "/wsdata/put", method = { RequestMethod.GET, RequestMethod.POST }, headers = "Accept=application/json; charset=utf-8")
    @ResponseBody
    public ResponseDTO setWorksheetWSDATAProperty(@RequestParam String problemId, @RequestParam String propertyName, @RequestParam Object propertyValue, HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException
    {
        ResponseDTO result = new ResponseDTO();
        try
        {
            String username = (String) request.getAttribute(Constants.HTTP_REQUEST_USERNAME);
            ServiceWorksheet.setWorksheetWSDATAProperty(problemId, propertyName, propertyValue, username);
            result.setSuccess(true);
        }
        catch (Exception e)
        {
            Log.log.error("Error setting WSDATA property.", e);
            result.setSuccess(false).setMessage("Error setting WSDATA property.");
        }

        return result;
    }


    /**
     * THIS IS NOT USED FOR NOW. PLEASE IGNORE IT AND DO NOT CREATE TEST CASE FOR THIS AS IT MAY CHANGE.
     * 
     * Update whole WSDATA map with all its keys and values. 
     * 
     * @param problemId sysId of a Worksheet
     * @param jsonMap Json string of type Map<String, Object>
     * @param request  HttpServletRequest
     * @param response  HttpServletResponse
     * @return {@link ResponseDTO} - Reponse Data Transfer Object with success value as 'true' or 'false'
     * @throws ServletException
     * @throws IOException
     */
    @SuppressWarnings({ "unchecked" })
    @RequestMapping(value = "/wsdata/updateMap", method = { RequestMethod.GET, RequestMethod.POST }, headers = "Accept=application/json; charset=utf-8")
    @ResponseBody
    public ResponseDTO updateWorksheetWSDATA(@RequestParam String problemId, @RequestParam String jsonMap, HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException
    {
        ResponseDTO result = new ResponseDTO();

        JsonConfig jsonConfig = new JsonConfig();
        jsonConfig.setRootClass(HashMap.class);
        JSON json = JSONSerializer.toJSON(jsonMap, jsonConfig);
        Map<String, Object> map = (HashMap<String, Object>) JSONSerializer.toJava(json, jsonConfig);

        try
        {
            String username = (String) request.getAttribute(Constants.HTTP_REQUEST_USERNAME);
            ServiceWorksheet.updateWorksheetWSDATA(problemId, map, username);
            result.setSuccess(true);
        }
        catch (Exception e)
        {
            Log.log.error("Error updating WSDATA property.", e);
            result.setSuccess(false).setMessage("Error updating WSDATA property.");
        }

        return result;
    }

    /**
     * Deletes the 'keys' from WsDataMap for a Worksheet/ProblemId
     * 
     * <p> 
     * <u><b>Example</b></u>:</br> 
     * <code><pre>
     *  ids: test1
     *  ids: test2
     *  problemId: 07715c659d8f444aa0e3447ab551c9ed -- (worksheet Id)
     * </pre></code>
     * 
     *       
     * @param problemId  sysId of requested worksheet row
     * @param ids Array of keys/properties to be deleted.
     * @param request  HttpServletRequest
     * @param response  HttpServletResponse
     * @return {@link ResponseDTO} - Reponse Data Transfer Object with success value as 'true' or 'false'
     * @throws ServletException
     * @throws IOException
     */
    
    @SuppressWarnings("rawtypes")
    @RequestMapping(value = "/wsdata/delete", method = { RequestMethod.GET, RequestMethod.POST }, headers = "Accept=application/json; charset=utf-8")
    @ResponseBody
    public ResponseDTO deleteWorksheetWSDATAProperties(@RequestParam String problemId, @RequestParam String[] ids, HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException
    {
        ResponseDTO result = new ResponseDTO();

        String username = (String) request.getAttribute(Constants.HTTP_REQUEST_USERNAME);
        ServiceWorksheet.deleteWorksheetWSDATA(problemId, ids, username);
        result.setSuccess(true);

        return result;
    }
    
    /**
     * This API will be invoked from Javascript to save key-value pair in WSDATA.
     * 
     * @param propertyName : String representing name of the property
     * @param propertyValue : String representing value of the property
     * @param request : HttpServletRequest
     * @return
     * 	{@link ResponseDTO} - Reponse Data Transfer Object with success value as 'true' or 'false'
     * @throws ServletException
     * @throws IOException
     */
    @SuppressWarnings("rawtypes")
	@RequestMapping(value = "/wsdata/save", method = { RequestMethod.GET, RequestMethod.POST }, headers = "Accept=application/json; charset=utf-8")
    @ResponseBody
    public ResponseDTO saveProperty(@RequestParam String propertyName, @RequestParam Object propertyValue, HttpServletRequest request) throws ServletException, IOException
    {
        ResponseDTO result = new ResponseDTO();
        String username = (String) request.getAttribute(Constants.HTTP_REQUEST_USERNAME);
        String orgName = (String) request.getAttribute(Constants.EXECUTE_ORG_NAME);
        
        try
        {
	        String problemId = ServiceHibernate.getActiveWorksheet(username, null, orgName);
	        ServiceWorksheet.setWorksheetWSDATAProperty(problemId, propertyName, propertyValue, username);
	        result.setSuccess(true);
        }
        catch(Exception e)
        {
            Log.log.error("Error saving WSDATA property.", e);
            result.setSuccess(false).setMessage("Error saving WSDATA property.");
        }

        return result;
    }
    
    /**
     * This API in intended to be used from Javascript to get the value of given property stored in WSDATA
     * 
     * @param propertyName : String representing name of the property whoes value is needed.
     * @param request : HttpServletRequest
     * @return
     * 	{@link ResponseDTO} - Reponse Data Transfer Object with success value as 'true' or 'false' with data populated with the value.
     * @throws ServletException
     * @throws IOException
     */
    @RequestMapping(value = "/wsdata/get", method = { RequestMethod.GET, RequestMethod.POST }, headers = "Accept=application/json; charset=utf-8")
    @ResponseBody
    public ResponseDTO<Object> get(@RequestParam String propertyName, HttpServletRequest request) throws ServletException, IOException
    {
        ResponseDTO<Object> result = new ResponseDTO<Object>();
        String username = (String) request.getAttribute(Constants.HTTP_REQUEST_USERNAME);
        String orgName = (String) request.getAttribute(Constants.EXECUTE_ORG_NAME);
        
        try
        {
	        String problemId = ServiceHibernate.getActiveWorksheet(username, null, orgName);
	        Object property = ServiceWorksheet.getWorksheetWSDATAProperty(problemId, propertyName, username);
            result.setData(StringUtils.isBlank((String)property)?"":property);
            result.setSuccess(true);
        }
        catch(Exception e)
        {
            Log.log.error("Error getting WSDATA property.", e);
            result.setSuccess(false).setMessage("Error getting WSDATA property.");
        }

        return result;
    }
}
