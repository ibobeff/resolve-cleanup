/******************************************************************************
* (C) Copyright 2014
*
* Resolve Systems, LLC
*
* All rights reserved
* This software is distributed under license and may not be used, copied,
* modified, or distributed without the express written permission of
* Resolve Systems, LLC.
*
******************************************************************************/

package com.resolve.rsview.main;

import com.resolve.util.XDoc;
import com.resolve.util.ConfigMap;

public class ConfigWindows extends ConfigMap
{
    private static final long serialVersionUID = -4113113906418709751L;
	boolean active          = false;
    boolean allowBasic      = true;
    boolean allowDelegation = true;
    boolean allowLocalhost  = true;
    boolean allowUnsecure   = true;
    boolean promptIfNtlm    = true;
    boolean useKeytab       = false;
    String clientModuleName = "spnego-client";
    String serverModuleName = "spnego-server";
    String domainUser       = "";
    String domainPass       = "";
    
    public ConfigWindows(XDoc config)
    {
        super(config);
        
        define("active", BOOLEAN, "./AUTH/WINDOWS/@ACTIVE");
        define("allowBasic", BOOLEAN, "./AUTH/WINDOWS/@ALLOW_BASIC");
        define("allowDelegation", BOOLEAN, "./AUTH/WINDOWS/@ALLOW_DELEGATION");
        define("allowLocalhost", BOOLEAN, "./AUTH/WINDOWS/@ALLOW_LOCALHOST");
        define("allowUnsecure", BOOLEAN, "./AUTH/WINDOWS/@ALLOW_UNSECURE");
        define("promptIfNtlm", BOOLEAN, "./AUTH/WINDOWS/@PROMPT_NTLM");
        define("useKeytab", BOOLEAN, "./AUTH/WINDOWS/@USE_KEYTAB");
        define("clientModuleName", STRING, "./AUTH/WINDOWS/@CLIENTMODULENAME");
        define("serverModuleName", STRING, "./AUTH/WINDOWS/@SERVERMODULENAME");
        define("domainUser", STRING, "./AUTH/WINDOWS/@DOMAINUSER");
        define("domainPass", ENCRYPT, "./AUTH/WINDOWS/@DOMAINPASS");
    } // ConfigId
    
    public void load() throws Exception
    {
        loadAttributes();
    } // load

    public void save()
    {
        saveAttributes();
    } // save

    public boolean isActive()
    {
        return active;
    }

    public void setActive(boolean active)
    {
        this.active = active;
    }

    public boolean isAllowBasic()
    {
        return allowBasic;
    }

    public void setAllowBasic(boolean allowBasic)
    {
        this.allowBasic = allowBasic;
    }

    public boolean isAllowDelegation()
    {
        return allowDelegation;
    }

    public void setAllowDelegation(boolean allowDelegation)
    {
        this.allowDelegation = allowDelegation;
    }

    public boolean isAllowLocalhost()
    {
        return allowLocalhost;
    }

    public void setAllowLocalhost(boolean allowLocalhost)
    {
        this.allowLocalhost = allowLocalhost;
    }

    public boolean isAllowUnsecure()
    {
        return allowUnsecure;
    }

    public void setAllowUnsecure(boolean allowUnsecure)
    {
        this.allowUnsecure = allowUnsecure;
    }

    public boolean isPromptIfNtlm()
    {
        return promptIfNtlm;
    }

    public void setPromptIfNtlm(boolean promptIfNtlm)
    {
        this.promptIfNtlm = promptIfNtlm;
    }

    public String getClientModuleName()
    {
        return clientModuleName;
    }

    public void setClientModuleName(String clientModuleName)
    {
        this.clientModuleName = clientModuleName;
    }

    public String getDomainUser()
    {
        return domainUser;
    }

    public void setDomainUser(String domainUser)
    {
        this.domainUser = domainUser;
    }

    public String getDomainPass()
    {
        return domainPass;
    }

    public void setDomainPass(String domainPass)
    {
        this.domainPass = domainPass;
    }

    public boolean isUseKeytab()
    {
        return useKeytab;
    }

    public void setUseKeytab(boolean useKeytab)
    {
        this.useKeytab = useKeytab;
    }

    public String getServerModuleName()
    {
        return serverModuleName;
    }

    public void setServerModuleName(String serverModuleName)
    {
        this.serverModuleName = serverModuleName;
    }

} // ConfigWindows
