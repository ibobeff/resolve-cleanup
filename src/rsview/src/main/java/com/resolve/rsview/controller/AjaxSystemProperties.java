/******************************************************************************
* (C) Copyright 2014
*
* Resolve Systems, LLC
*
* All rights reserved
* This software is distributed under license and may not be used, copied,
* modified, or distributed without the express written permission of
* Resolve Systems, LLC.
*
******************************************************************************/

package com.resolve.rsview.controller;

import java.io.IOException;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.httpclient.HttpStatus;
import org.owasp.esapi.ESAPI;
import org.owasp.esapi.errors.IntrusionException;
import org.owasp.esapi.errors.ValidationException;
import org.springframework.stereotype.Controller;
import org.springframework.stereotype.Service;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.resolve.rsbase.MainBase;
import com.resolve.rsview.auth.Authentication;
import com.resolve.rsview.main.Main;
import com.resolve.services.ServiceHibernate;
import com.resolve.services.hibernate.vo.PropertiesVO;
import com.resolve.services.jdbc.util.JdbcCrudUtil;
import com.resolve.services.vo.QueryDTO;
import com.resolve.services.vo.QuerySort;
import com.resolve.services.vo.ResponseDTO;
import com.resolve.sql.SQL;
import com.resolve.sql.SQLConnection;
import com.resolve.util.Constants;
import com.resolve.util.Log;
import com.resolve.util.SyslogFormatter;

import net.sf.json.JSONObject;
import net.sf.json.JSONSerializer;
import net.sf.json.JsonConfig;


/**
 * 
 * This controller serves the request(CRUD operations) for System Properties. 
 * <p>
 * These properties affect the behaviour of a feature/functionality of Resolve. For eg. there are properties that set the Default Roles of a 
 * Wiki document when they are created. 
 * <p>
 * For more information about the System Properties, please refer to <a href="http://community.gen-e.com/resolve/jsp/rsclient.jsp">Resolve documentation</a> or the Description of the Properties. 
 * <p>
 * 
 * DB Table: {@code properties}</br>
 * Url : {@code http://localhost:8080/resolve/jsp/rsclient.jsp#RS.sysadmin.SystemProperties/}
 * 
 * @author jeet.marwah
 *
 */

@Controller
@Service
public class AjaxSystemProperties extends GenericController
{
	private static final String ERROR_DELETING_PROPERTIES = "Error deleting Properties";
	private static final String ERROR_RETRIEVING_PROPERTIES = "Error retrieving Properties";
	private static final String ERROR_RETRIEVING_SYSTEM_PROPERTY = "Error retrieving system property";
    private static final String ERROR_SAVING_PROPERTIES = "Error saving Properties";

	/**
     * Returns/Retrieves list of Properties for the grid based on filters, sorts, pagination. 
     * <p>Presently, for System Properties, pagination is turned off and hence you see that the limit=-1.
     *   
     * <u><b>Example</b></u>:</br> 
     * <code><pre>
     *  filter: [{"field":"uname","type":"auto","condition":"contains","value":"rs"}]
     *  page: 1
     *  start: 0
     *  limit: -1
     *  group: [{"property":"group","direction":"ASC"}]
     *  sort: [{"property":"group","direction":"ASC"},{"property":"uname","direction":"ASC"}]
     * </pre></code>
     * 
     * <p>
     * Parameters in the Request object: 
     * <code><pre>
     *  USERNAME - username of the user who is logged in to Resolve
     * </pre></code>
     *  
     * <p>
     * Return object in response:
     * <code><pre>
     *  'records' : List of type {@link PropertiesVO}
     *  'total' : # of total records in the table/db 
     * </pre></code>
     * 
     * @param query  {@link QueryDTO}, represents the query, pagination, sorting, grouping for the UI
     * @param request  HttpServletRequest
     * @param response  HttpServletResponse
     * @return {@link ResponseDTO} - Reponse Data Transfer Object having a List  and total # of records 
     * @throws ServletException
     * @throws IOException
     */
    @RequestMapping(value = "/sysproperties/getAllProperties", method = { RequestMethod.GET, RequestMethod.POST }, headers = "Accept=application/json; charset=utf-8")
    @ResponseBody
    public ResponseDTO list(@ModelAttribute QueryDTO query, HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException
    {
        ResponseDTO result = new ResponseDTO();
        String username = (String) request.getAttribute(Constants.HTTP_REQUEST_USERNAME);
        try
        {   
            //massage the query for the properties grid
            manipuateQueryDTOForPropertiesGrid(query);
            
//            QueryFilter test = new QueryFilter("string", "social_admin", "UValue", "equals");
//            query.addFilterItem(test);

            //get the data
            List<PropertiesVO> data = ServiceHibernate.getProperties(query, username);
            int total = ServiceHibernate.getTotalHqlCount(query);

            result.setSuccess(true).setRecords(data);
            result.setTotal(total);
        }
        catch (Exception e)
        {
            Log.log.error(ERROR_RETRIEVING_PROPERTIES, e);
            result.setSuccess(false).setMessage(ERROR_RETRIEVING_PROPERTIES);
        }

        return result;
    }

    /**
     * Get system properties by sysId
     * 
     * <p>
     * Return object in response 
     * <code><pre>
     *  'data' : {@link PropertiesVO}
     * </pre></code>
     * 
     * 
     * @param id sysId of the System Property
     * @param request HttpServletRequest
     * @param response HttpServletResponse
     * @return {@link ResponseDTO} - Reponse Data Transfer Object with 'data' of type {@link PropertiesVO}
     * @throws ServletException
     * @throws IOException
     */
    @RequestMapping(value = "/sysproperties/getSystemProperty", method = { RequestMethod.POST }, headers = "Accept=application/json; charset=utf-8")
    @ResponseBody
    public ResponseDTO get(@RequestParam String id, HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException
    {
        ResponseDTO result = new ResponseDTO();

        try
        {
            PropertiesVO vo = ServiceHibernate.findPropertyById(id);
            result.setData(vo);
            result.setSuccess(true);

        }
        catch (Exception e)
        {
            Log.log.error(ERROR_RETRIEVING_PROPERTIES, e);
            result.setSuccess(false).setMessage(ERROR_RETRIEVING_PROPERTIES);
        }

        return result;
    }
    
    /**
     * Get system properties by name
     * <p>
     * Return object in response 
     * <code><pre>
     *  'data' : {@link PropertiesVO}
     * </pre></code>
     * 
     * 
     * @param name System Property name
     * @param request HttpServletRequest
     * @param response HttpServletResponse
     * @return {@link ResponseDTO} - Reponse Data Transfer Object with 'data' of type {@link PropertiesVO}
     * @throws ServletException
     * @throws IOException
     */
    @RequestMapping(value = "/sysproperties/getSystemPropertyByName", method = { RequestMethod.POST }, headers = "Accept=application/json; charset=utf-8")
    @ResponseBody
    public ResponseDTO getByName(@RequestParam String name, HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException
    {
        ResponseDTO result = new ResponseDTO();

        try
        {
            ESAPI.validator().getValidInput("Get System Property By Name", name, "ResolveTextNoPercent", 255, false);
            PropertiesVO vo = ServiceHibernate.findPropertyByName(name);
            result.setData(vo);
            result.setSuccess(true);

        }
        catch (ValidationException|IntrusionException vorie)
        {
            String logMessage = "Error " + vorie.getMessage() + " retrieving specified system property " + 
                                (name.length() <= 255 ? name : name.substring(0, 254) + "...");
            
            Log.log.info(logMessage);
            Log.syslog.info(SyslogFormatter.INSTANCE.formatAudit(Authentication.getRemoteAddrForSysLog(request), 
                                                                (String) request.getAttribute(Constants.HTTP_REQUEST_USERNAME),
                                                                request.getLocalAddr(), logMessage));
            response.setStatus(HttpStatus.SC_BAD_REQUEST);
            result.setSuccess(false).setMessage(HttpStatus.getStatusText(HttpStatus.SC_BAD_REQUEST));
        }
        catch (Exception e)
        {
            
            Log.log.error(ERROR_RETRIEVING_SYSTEM_PROPERTY, e);
            result.setSuccess(false).setMessage(ERROR_RETRIEVING_SYSTEM_PROPERTY);
        }

        return result;
    }
    
    /**
     * Get clob column name
     * <p>
     * Return object in response 
     * <code><pre>
     *  'data' : {@link String}
     * </pre></code>
     * 
     * 
     * @param request HttpServletRequest
     * @param response HttpServletResponse
     * @return {@link String} - Reponse Data Transfer Object with 'data' of type {@link String}
     * @throws ServletException
     * @throws IOException
     */
    @RequestMapping(value = "/sysproperties/getClobColumnNames", method = { RequestMethod.GET, RequestMethod.POST }, headers = "Accept=application/json; charset=utf-8")
    @ResponseBody
    public ResponseDTO<List<String>> getClobColummNames(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException
    {
        ResponseDTO<List<String>> result = new ResponseDTO<List<String>>();
        List<String> columnNames = new ArrayList<String>();
        
        try
        {
        	String dbType = ((Main) MainBase.main).getConfigSQL().getDbtype().toUpperCase();
            if(dbType == null)
                throw new Exception("No database type is available.");
            
            if(dbType.equalsIgnoreCase("oracle")) {
                columnNames = getClobColumnNameForOracle("PROPERTIES", dbType); 
            }
   
            result.setData(columnNames);
            result.setSuccess(true);
        }
        catch (Exception e)
        {
            Log.log.error(ERROR_RETRIEVING_PROPERTIES, e);
            result.setSuccess(false).setMessage(ERROR_RETRIEVING_PROPERTIES);
        }

        return result;
    }

    /**
     * Delete the properties based on array of sysIds
     * <p>
     * Parameters in the Request object: 
     * <code><pre>
     *  USERNAME - username of the user who is logged in to Resolve
     * </pre></code>
     * 
     * @param ids  array of sysIds to be deleted
     * @param request HttpServletRequest
     * @param response HttpServletResponse
     * @return {@link ResponseDTO} - Reponse Data Transfer Object with status as 'true' or 'false'
     * @throws ServletException
     * @throws IOException
     */
    @RequestMapping(value = "/sysproperties/deleteProperties", method = { RequestMethod.POST }, headers = "Accept=application/json; charset=utf-8")
    @ResponseBody
    public ResponseDTO delete(@RequestBody String[] ids, HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException
    {
        ResponseDTO result = new ResponseDTO();
        String username = (String) request.getAttribute(Constants.HTTP_REQUEST_USERNAME);

        try
        {
            ServiceHibernate.deletePropertiesById(ids, username);
            result.setSuccess(true);
        }
        catch (Exception e)
        {
            Log.log.error(ERROR_DELETING_PROPERTIES, e);
            result.setSuccess(false).setMessage(ERROR_DELETING_PROPERTIES);
        }

        return result;
    }

    
    /**
     *  Saves/Update a property. If 'sysId' is set, it will be update else it will create it. 
     * 
     * <p>
     * Parameters in the Request object: 
     * <code><pre>
     *  USERNAME - username of the user who is logged in to Resolve
     * </pre></code>
     * <p>
     * Return object in response 
     * <code><pre>
     *  'data' : {@link PropertiesVO}
     * </pre></code>
     *  
     * @param jsonProperty JSONObject/JSON having data of type {@link PropertiesVO}
     * @param request HttpServletRequest
     * @param response HttpServletResponse
     * @return {@link ResponseDTO} - Reponse Data Transfer Object with 'data' having the created/updated System Property
     * @throws ServletException
     * @throws IOException
     */
    @RequestMapping(value = "/sysproperties/saveSystemProperty", method = { RequestMethod.POST }, headers = "Accept=application/json; charset=utf-8")
    @ResponseBody
    public ResponseDTO save(@RequestBody JSONObject jsonProperty, HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException
    {
      //using jackson to deserialize the json
//        String json = jsonProperty.toString();
//        PropertiesVO property = new ObjectMapper().readValue(json, PropertiesVO.class);
        
        ResponseDTO result = new ResponseDTO();
        JsonConfig jsonConfig = new JsonConfig();
        jsonConfig.setRootClass(PropertiesVO.class);
        PropertiesVO property = (PropertiesVO) JSONSerializer.toJava(jsonProperty, jsonConfig);
        String username = (String) request.getAttribute(Constants.HTTP_REQUEST_USERNAME);
        try
        {
            PropertiesVO prop = ServiceHibernate.saveProperties(property, username);
            result.setSuccess(true).setData(prop);
        }
        catch (Exception e)
        {
            Log.log.error(ERROR_SAVING_PROPERTIES, e);
            result.setSuccess(false).setMessage(ERROR_SAVING_PROPERTIES);
        }

        return result;
    }
    
    //private apis
    private void manipuateQueryDTOForPropertiesGrid(QueryDTO query)
    {
        query.setModelName("Properties");
        
        //filter out the UI fields which do not map to database column.
        List<QuerySort> sorts = query.getSortItems();
        if(sorts != null)
        {
            for(QuerySort sort : sorts)
            {
                if(sort.getProperty().equalsIgnoreCase("group"))
                {
                    sorts.remove(sort);
                }
            }
            
            String jsonSort = QuerySort.encodeJson(sorts);
            query.setSort(jsonSort);
        }
    }
    
    private static List<String> getClobColumnNameForOracle(String tableName, final String dbType) {
        List<String> columnNames = new ArrayList<>();
        
        if(dbType == null || !dbType.equalsIgnoreCase("Oracle")) {
            return columnNames;
        }
        
        SQLConnection conn = null;
        PreparedStatement ps = null;
        ResultSet rs = null;

        String sql = "SELECT column_name FROM user_tab_cols WHERE data_type='CLOB' AND table_name = ?";
        
        try
        {
            conn = SQL.getConnection();
            
            ps = conn.prepareStatement(sql);
            ps.setString(1, tableName);
            rs = ps.executeQuery();

            while(rs.next()) {
                String column = rs.getString("column_name");

                if(column != null && column.length() != 0)
                    columnNames.add(column);
            }
        } catch(Exception e) {
            Log.log.error(e.getMessage());
        } finally {
            try {
                if(ps != null)
                    ps.close();
                if(rs != null)
                    rs.close();
                if(conn != null)
                    conn.close();
            } catch(Exception ee) {
                Log.log.error(ee.getMessage());
            }
        }
        
        return columnNames;
    }
}