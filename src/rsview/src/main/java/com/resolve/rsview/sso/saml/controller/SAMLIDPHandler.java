package com.resolve.rsview.sso.saml.controller;

import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.net.URLDecoder;
import java.nio.charset.StandardCharsets;
import java.util.HashMap;
import java.util.Map;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.opensaml.DefaultBootstrap;
import org.opensaml.xml.ConfigurationException;
import org.owasp.esapi.ESAPI;

import com.resolve.rsview.auth.Authentication;
import com.resolve.rsview.main.Main;
import com.resolve.rsview.sso.authenticator.SSOAuthenticationDTO;
import com.resolve.rsview.sso.authenticator.SSOAuthenticator;
import com.resolve.rsview.sso.saml.SAMLAuthenticatorFactory;
import com.resolve.rsview.sso.util.SAMLEnum;
import com.resolve.services.constants.ConstantValues;
import com.resolve.services.util.PropertiesUtil;
import com.resolve.util.Constants;
import com.resolve.util.CryptUtils;
import com.resolve.util.Log;
import com.resolve.util.StringUtils;

public class SAMLIDPHandler extends HttpServlet
{
    private static final long serialVersionUID = 2760347665403094646L;
    
	/**
	 * This method is required to enable the default SAML behavior 
	 * @throws Exception 
	 */
	public SAMLIDPHandler() throws Exception {
	    try {
	    	DefaultBootstrap.bootstrap();
	    } catch (ConfigurationException e) {
	        Log.log.error(e.getMessage(), e);
	        throw new Exception("Bootstrap failed");
	    }
	    // Bootstrap enables SAML default security configuration we need to override
	    // it with our Resolve security configuration
	    ESAPI.initialize("org.owasp.esapi.reference.DefaultSecurityConfiguration");
	    //
	    Main.startupLatch.countDown();
	}
	
	@Override
	public void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
	    handleSamlRequest(request, response);
	}

	@Override
	public void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
	    handleSamlRequest(request, response);
	}
	  
	@SuppressWarnings({ "unchecked", "rawtypes" })
	public void handleSamlRequest(HttpServletRequest req, HttpServletResponse res) throws ServletException {
		
		String redirectURL = "";
		Map<String, Object> sessionMap = new HashMap<>();
		Log.log.debug("With valid user session SAML user");
		try {
		    SSOAuthenticator samlAuthenticator = SAMLAuthenticatorFactory.INSTANCE.build(req, res);
		    SSOAuthenticationDTO authentication = samlAuthenticator.authenticate(req, res);
		    
		    // if user us authenticated but not authorized, we populate username as a session attribute as authentication (SSOAuthenticationDTO) will be returned null.
		    String userName = req.getSession().getAttribute(SAMLEnum.USERNAME.getValue()) == null ? null : req.getSession().getAttribute(SAMLEnum.USERNAME.getValue()).toString();
		    //If the authentication succeeds 
		    if (authentication != null && authentication.getErrCode() == null)
		    {
		    	userName = authentication.getUsername();
		    } else {
		    	if (authentication != null && authentication.getErrCode() != null) {
		    		throw new Exception(authentication.getErrCode().getMessage());
		    	}
		    }
		    
	    	/*
	    	 * Compare if the authenticated userName is same as that of found in the
	    	 * original URL params, if at all present.
	    	 */
	    	String sessionId = req.getSession().getId();
	    	if (SAMLAuthHandler.getAdfsSessionsMap().containsKey(sessionId))
	    	{
	    	    sessionMap = (Map)SAMLAuthHandler.getAdfsSessionsMap().get(sessionId);
	    	    req.setAttribute(Constants.HTTP_ATTRIBUTE_CREATETIME, sessionMap.get(Constants.HTTP_ATTRIBUTE_CREATETIME));
	    	    redirectURL = (String)sessionMap.get(SAMLEnum.REQUEST_URI.getValue()) + (StringUtils.isNotBlank((String)sessionMap.get(SAMLEnum.QUERY_STRING.getValue())) ? "?" + (String)sessionMap.get(SAMLEnum.QUERY_STRING.getValue()) : "");
	    	}
	    	else
	    	{
	    	    req.setAttribute(Constants.HTTP_ATTRIBUTE_CREATETIME, System.currentTimeMillis());
	    	    redirectURL = SAMLEnum.LANDING_URI.getValue();
	    	}
	    	
	    	sessionMap.put(SAMLEnum.USERNAME.getValue(), userName);
	    	sessionMap.put(SAMLEnum.AUTHENTICATED.getValue(), true);
	    	SAMLAuthHandler.sendMapToSync(sessionId, sessionMap, true);
	    	long DEFAULT_SESSION_TIMEOUT = (PropertiesUtil.getPropertyLong(ConstantValues.PROPERTIES_SESSION_TIMEOUT) < 720) ? 720000 : PropertiesUtil.getPropertyLong(ConstantValues.PROPERTIES_SESSION_TIMEOUT) * 1000;
            String token = Authentication.setAuthToken(req, res, userName, req.getRemoteAddr(), null, DEFAULT_SESSION_TIMEOUT, null, null);
            req.setAttribute(Constants.HTTP_REQUEST_TOKEN, CryptUtils.encryptToken(token));
            req.getSession().setAttribute(SAMLEnum.USERNAME.getValue(), ESAPI.validator().getValidInput("Invalid session attribute ", userName, "HTTPURI", 100, true));
            ESAPI.httpUtilities().sendRedirect(res, decodedQryURI(redirectURL));
		} catch(Exception ex) {
		    Log.log.error("Error while authenticating user: ", ex);
			throw new ServletException(ex);
		}
	}
	private String decodedQryURI(String queryString) {
		String decodedQueryString = "";
		if (StringUtils.isNotBlank(queryString))
        {
            try
            {
                decodedQueryString = URLDecoder.decode(queryString, StandardCharsets.UTF_8.name());
            }
            catch (UnsupportedEncodingException e)
            {
                Log.auth.warn("Error " + e.getLocalizedMessage() + " occured while decoding query string " +
                              queryString + " using " + StandardCharsets.UTF_8.name() + " character set," +
                              " returning encoded query string!!!", e);
                decodedQueryString = queryString;
            }
        }
        else
        {
            decodedQueryString = "";
        }
		return decodedQueryString;
	}
}
