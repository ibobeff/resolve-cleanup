package com.resolve.rsview.controller;

import java.util.ArrayList;
import java.util.List;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.resolve.services.hibernate.util.CEFUtil;
import com.resolve.services.hibernate.util.ConverterUtil;
import com.resolve.services.hibernate.vo.CEFDictionaryItemVO;
import com.resolve.services.vo.CEFDictionaryItemDTO;
import com.resolve.services.vo.ResponseDTO;
import com.resolve.util.Log;

@Controller
@RequestMapping("/cef")
public class AjaxCEF extends GenericController {
	private static final String ERROR_RETRIEVING_CEF_DATA = "Error retrieving CEF dictionary data";
	private static final String ERROR_NOT_FOUND_CEF_DATA = "CEF dictionary data not found";
	
	@RequestMapping(method = RequestMethod.GET)
	@ResponseBody
	public ResponseDTO<List<CEFDictionaryItemDTO>> list() {
		ResponseDTO<List<CEFDictionaryItemDTO>> result = new ResponseDTO<>();
		try {
			List<CEFDictionaryItemVO> data = CEFUtil.getAllCEFDictionary();
			List<CEFDictionaryItemDTO> dtoData = new ArrayList<>(data.size());
			for (CEFDictionaryItemVO vo : data) {
				dtoData.add(ConverterUtil.convertCEFItemVOToDTO(vo));
			}

			result.setSuccess(true).setData(dtoData);
		} catch (Exception e) {
			Log.log.error(ERROR_RETRIEVING_CEF_DATA, e);
			result.setSuccess(false).setMessage(ERROR_RETRIEVING_CEF_DATA);
		}

		return result;
	}

	@RequestMapping(value = "/{shortName}", method = RequestMethod.GET)
	@ResponseBody
	public ResponseDTO<CEFDictionaryItemDTO> getByShortName(@PathVariable String shortName) {
		ResponseDTO<CEFDictionaryItemDTO> result = new ResponseDTO<>();
		try {
			CEFDictionaryItemVO vo = CEFUtil.getCEFDictionaryByShortName(shortName);
			if (vo != null) {
				result.setSuccess(true).setData(ConverterUtil.convertCEFItemVOToDTO(vo));
			} else {
				result.setSuccess(false).setMessage(ERROR_NOT_FOUND_CEF_DATA);
			}
		} catch (Exception e) {
			Log.log.error(ERROR_RETRIEVING_CEF_DATA, e);
			result.setSuccess(false).setMessage(ERROR_RETRIEVING_CEF_DATA);
		}

		return result;
	}
}
