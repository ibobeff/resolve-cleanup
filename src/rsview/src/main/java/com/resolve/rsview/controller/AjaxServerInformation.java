/******************************************************************************
* (C) Copyright 2014
*
* Resolve Systems, LLC
*
* All rights reserved
* This software is distributed under license and may not be used, copied,
* modified, or distributed without the express written permission of
* Resolve Systems, LLC.
*
******************************************************************************/

package com.resolve.rsview.controller;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.stereotype.Controller;
import org.springframework.stereotype.Service;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.resolve.rsview.util.ServerInformationUtil;
import com.resolve.services.vo.ResponseDTO;
import com.resolve.services.vo.ServerInfoDTO;
import com.resolve.util.Log;

@Controller
@Service
public class AjaxServerInformation extends GenericController
{
    private static final String ERROR_RETRIEVING_SERVER_INFO = "Error retrieving server info";

	@RequestMapping(value = "/server/get", method = { RequestMethod.POST }, headers = "Accept=application/json; charset=utf-8")
    @ResponseBody
    public ResponseDTO get(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException
    {
        ResponseDTO result = new ResponseDTO();

        try
        {
            ServerInfoDTO dto = ServerInformationUtil.getServerInformation(request);
            result.setData(dto);
            result.setSuccess(true);
        }
        catch (Exception e)
        {
            Log.log.error(ERROR_RETRIEVING_SERVER_INFO, e);
            result.setSuccess(false).setMessage(ERROR_RETRIEVING_SERVER_INFO);
        }

        return result;
    }

}
