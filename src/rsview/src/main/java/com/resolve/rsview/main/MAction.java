/******************************************************************************
* (C) Copyright 2014
*
* Resolve Systems, LLC
*
* All rights reserved
* This software is distributed under license and may not be used, copied,
* modified, or distributed without the express written permission of
* Resolve Systems, LLC.
*
******************************************************************************/

package com.resolve.rsview.main;

import java.io.File;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.ArrayList;import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;

import org.apache.commons.lang3.tuple.Pair;

import com.resolve.esb.MMsgContent;
import com.resolve.esb.MMsgHeader;
import com.resolve.esb.MMsgOptions;
import com.resolve.execute.ExecuteMain;
import com.resolve.persistence.util.ExportUtil;
import com.resolve.rsbase.MainBase;
import com.resolve.rsview.auth.Authentication;
import com.resolve.rsview.cr.ContentRequestUtil;
import com.resolve.rsview.impex.ImpexAPI;
import com.resolve.rsview.sso.saml.controller.SAMLAuthHandler;
import com.resolve.services.ImpexService;
import com.resolve.services.ServiceAuthentication;
import com.resolve.services.ServiceGateway;
import com.resolve.services.ServiceHibernate;
import com.resolve.services.ServiceMigration;
import com.resolve.services.ServiceWiki;
import com.resolve.services.archive.ArchiveSIRData;
import com.resolve.services.constants.ConstantValues;
import com.resolve.services.hibernate.customtable.CustomTableUtil;
import com.resolve.services.hibernate.customtable.ExtSaveMetaFormView;
import com.resolve.services.hibernate.util.ActionTaskPropertiesUtil;
import com.resolve.services.hibernate.util.ActionTaskUtil;
import com.resolve.services.hibernate.util.AppsUtil;
import com.resolve.services.hibernate.util.ArtifactConfigurationUtil;
import com.resolve.services.hibernate.util.ArtifactTypeUtil;
import com.resolve.services.hibernate.util.AssessorUtil;
import com.resolve.services.hibernate.util.CronUtil;
import com.resolve.services.hibernate.util.ExecuteRunbook;
import com.resolve.services.hibernate.util.ImpexUtil;
import com.resolve.services.hibernate.util.PropertiesUtil;
import com.resolve.services.hibernate.util.SystemScriptUtil;
import com.resolve.services.hibernate.util.TagUtil;
import com.resolve.services.hibernate.util.WikiLookupUtil;
import com.resolve.services.hibernate.util.WikiTemplateUtil;
import com.resolve.services.hibernate.util.WikiUtils;
import com.resolve.services.hibernate.vo.ConfigActiveDirectoryVO;
import com.resolve.services.hibernate.vo.ConfigLDAPVO;
import com.resolve.services.hibernate.vo.ConfigRADIUSVO;
import com.resolve.services.hibernate.vo.ResolveEventVO;
import com.resolve.services.hibernate.vo.ResolveImpexModuleVO;
import com.resolve.services.hibernate.wiki.DeleteHelper;
import com.resolve.services.impex.importmodule.ImportReports;
import com.resolve.services.util.PlaybookUtils;
import com.resolve.services.util.UserUtils;
import com.resolve.thread.TaskExecutor;
import com.resolve.util.Constants;
import com.resolve.util.FileUtils;
import com.resolve.util.Log;
import com.resolve.util.StringUtils;
import com.resolve.util.constants.HibernateConstants;

public class MAction
{
    public static final String RESOLVETAG_OLD_TAGNAME = "RESOLVETAG_OLD_TAGNAME";
    public static final String RESOLVETAG_NEW_TAGNAME = "RESOLVETAG_NEW_TAGNAME";

    public void heartbeat(Map params)
    {
        // ignore
    } // heartbeat
    
    public Map<String, Object> postInstalledOperations(Map params)
    {
        ServiceMigration.postInstalledOperations(params);
        
        return new HashMap<String, Object>();
    }

    public Map<String,Object> migrateNeo4jToSQL(Map params)
    {
        //Neo4j to SQL migration
        Log.log.debug("STARTING THE NEO4J MIGRATION");
        try
        {
            ServiceMigration.migrateNeo4jToSQL();
        }
        catch (Throwable t)
        {
            Log.log.error("Error in Neo4j Migration", t);
        }
        Log.log.debug("NEO4J MIGRATION COMPLETED ");
        
        return new HashMap<String, Object>();
    }
    
    public Map<String,Object> migrateActionTask(Map params)
    {
        //Neo4j to SQL migration
        Log.log.debug("Start Action Task Migration");
        try
        {
            ServiceMigration.migrateActionTask(5);
        }
        catch (Throwable t)
        {
            Log.log.error("Error in Action Task Migration", t);
        }
        Log.log.debug("ACTIONTASK MIGRATION COMPLETED ");
        
        return new HashMap<String, Object>();
    }
    
    public Map<String,Object> migrateLegacyGateways(Map params)
    {
        //Neo4j to SQL migration
        Log.log.debug("Start migration of Legacy gateways");
        try
        {
            ServiceMigration.migrateLegacyGateways(params);
        }
        catch (Throwable t)
        {
            Log.log.error("Error in migration of Legacy gateways", t);
        }
        Log.log.debug("LEGEACY GATEWAY MIGRATION COMPLETED ");
        
        return new HashMap<String, Object>();
    }
    
    //called when an Resolve is Updated
    public Map<String,Object> updateCleanup(Map params)
    {
        return new HashMap<String, Object>();
    }
    
    //called when an Resolve is Upgraded
    public Map<String, Object> upgradeCleanup(Map params)
    {
        Log.log.debug("*****        START OF upgradeCleanup          *****");
        long startTime = System.currentTimeMillis();
        
        //update the link flag to true in the groups table
        ServiceMigration.updateExternalFlags();
        
        //move tags from sql db to graph db
//        try
//        {
//            ServiceTag.addAllResolveTagFromDBToGraph();
//        }
//        catch (Exception e)
//        {
//            Log.log.error("error in migrating tags from DB to graph", e);
//        }
        
        //cleanup the actiontask table to have the 'menupath' correct
        //make sure that all the access rights records are there for all the actiontask. 
        // - find the AT list that does not have accessrights rec and than get and save each one to make sure that they are sync
        ServiceMigration.cleanupActiontaskTable();
        
        //sync the DB and graphDB recs
//        ServiceHibernate.syncDBToGraph();
        
        //migrate the relationship of Doc-Tag to the graph, at-tag, users, etc 
//        ServiceHibernate.migrate();
        
        Log.log.debug("Time taken(ms): " + (System.currentTimeMillis() - startTime));
        Log.log.debug("*****        END OF upgradeCleanup          *****");

        //return - required for the rsmgmt to know that its done.
        return new HashMap<String, Object>();
        
    }
    
//    public Map<String, Object> syncTagsESAndGraph(Map params)
//    {
//        ServiceHibernate.syncTagsESAndGraph();
//        
//        return new HashMap<String, Object>();
//    }

    /**
     * Expected values: All are mandatory
     * 
     * DESTINATION - folder location . eg. C:/Jeet/testing/
     * MODULENAME
     * USERNAME
     * 
     * @param params
     */
    public void downloadModule(Map params)
    {
        String destination = (String) params.get("DESTINATION");//"C:/Jeet/testing/";
        String moduleNames = (String) params.get("MODULENAME");//"Cr";
        String username = (String) params.get("USERNAME");//"admin";
       
        if (StringUtils.isNotBlank(destination) && StringUtils.isNotBlank(moduleNames) && StringUtils.isNotBlank(username))
        {
            if(!destination.endsWith("/"))
            {
                destination = destination + "/";
            }
            String[] modules = moduleNames.split(",");
            for (int i=0; i < modules.length; i++)
            {
                String moduleName = modules[i].trim();
                try
                {
                    ResolveImpexModuleVO module = ImpexUtil.getImpexModelVOWithReferences(null, moduleName, username);
                    if (module != null && module.getUZipFileContent() != null)
                    {
                        FileUtils.writeByteArrayToFile(new File(destination + module.getUZipFileName()), module.getUZipFileContent());
                    }
                }
                catch (Exception e)
                {
                    Log.log.error(e.getMessage(), e);
                }
            }
        }
    }
    
    @SuppressWarnings("rawtypes")
    public void importModule(final Map params)
    {
        Log.log.info(Main.main.configId.guid + " RSView Received importModule (" + StringUtils.mapToLogString(params) + ") message!!!");
        Log.log.info("Import-Module-Original thread: " + Thread.currentThread().getName());
        
        try
        {
            Log.log.info("Waiting for the SAML Dispatcher to start up...");
            System.out.println(new Date() + " - Waiting for the SAML to start up...");
            Main.startupLatch.await();
            System.out.println(new Date() + " - Startup latch is released.");
            Log.log.info("SAML Dispatcher started!!! Starting import of received modules.");
        }
        catch(InterruptedException ie)
        {
            ie.printStackTrace();
        }
        
        new Thread()
        {
            public void run()
            {
                String name = Thread.currentThread().getName();
                Thread.currentThread().setName("Import-Module-" + name);
                importModuleFromUpdate(params);
            }
        }.start();
        
    } // importModule
    
    @SuppressWarnings({ "rawtypes", "static-access" })
    public void importModuleFromUpdate(Map params)
    {
        String modulenames = (String)params.get(Constants.IMPEX_PARAM_MODULENAME);
        Log.log.debug("modulenames: " + modulenames);
        String username = (String)params.get(Constants.HTTP_REQUEST_USERNAME);
        Object blockIndexObj = params.get(Constants.IMPEX_PARAM_BLOCK_INDEX);
        Map<String, Object> indexParams = new HashMap<String, Object>();
        boolean blockIndex = false;
        if (blockIndexObj != null)
        {
            blockIndex = blockIndexObj.toString().equalsIgnoreCase("true");
        }
        if (StringUtils.isNotEmpty(modulenames))
        {
            String[] modules = modulenames.split(",");
            for (int i=0; i < modules.length; i++)
            {
                String modulename = modules[i].trim();
                
                try
                {
                    if (StringUtils.isNotEmpty(modulename))
                    {
                        Log.log.info("Importing module: " + modulename + "...");
                        
                        initImportForUpdate(modulename, username);
                        
                        if (StringUtils.isNotBlank(username))
                        {
                            ImpexAPI.importModule(modulename, username, blockIndex);
                        }
                        else
                        {
                            ImpexAPI.importModule(modulename, blockIndex);
                        }
                        
                        Log.log.info("Imported module: " + modulename);
                    }
                }
                catch (Throwable e)
                {
                    e.printStackTrace();
                    Log.log.warn(e.getMessage(), e);
                }
            }
            if (blockIndex)
            {
                indexParams.put("INDEX_ALL_ACTIONTASK", true);
                indexParams.put("INDEX_ALL_WIKIDOCUMENTS", true);
                indexParams.put("INDEX_ALL_ATTACHMENTS", true);
                indexParams.put("PURGE_ALL_INDEXES", true);
            }
        } 
    }
    
    /**
     * This API is deprecated in Resolve 5.2
     * @param params
     */
    @Deprecated
    public static void importReports(Map<String, String> params)
    {
        /*
        String csvModuleNames = (String)params.get(Constants.IMPEX_PARAM_MODULENAMES);

        ImportReports.importReportsOfModules(csvModuleNames);
        */
    }

    public void exportModule(Map params)
    {
        String modulenames = (String)params.get(Constants.IMPEX_PARAM_MODULENAME);
        if (modulenames.equalsIgnoreCase("ALL"))
        {
            modulenames = ImpexUtil.getAllModuleNames();
        }
        
        if (StringUtils.isNotEmpty(modulenames))
        {
            String[] modules = modulenames.split(",");
            for (int i=0; i < modules.length; i++)
            {
                String modulename = modules[i].trim();
                
		        try
		        {
		            if (StringUtils.isNotEmpty(modulename))
		            {
			            Log.log.info("Export module: "+modulename);
			            
			            Map<String, Object> exportParams = new HashMap<String, Object>();
			            exportParams.put("MODULE_NAME", modulename);
			            exportParams.put("USER", "admin");
			            exportParams.put("CLEAN_MODULE", false);
			            
			            ImpexService.exportSync(exportParams);
			            
			            //download the file in the rsexpert folder
			            
		            }
		        }
		        catch (Throwable e)
		        {
		            Log.log.warn(e.getMessage(), e);
		        }
            }
        }
    } // exportModule
    
    public static void createExport(Map params)
    {
    	 String modulename = (String) params.get(Constants.IMPEX_PARAM_MODULENAME);
         String description = (String) params.get(Constants.IMPEX_PARAM_DESCRIPTION);
         if (modulename.equalsIgnoreCase("ALL"))
         {
             Log.log.error("Impex Module name 'ALL' is a reserved impex name");
         }
         else if (StringUtils.isEmpty(modulename))
         {
             Log.log.error("A Module Name must be given to be created");
         }
         else
         {
        	 ImpexUtil.createExport(modulename, description);
         }
    }//createExport
    
    public static void addRunbooksToExport(Map params)
    {
    	com.resolve.services.util.ImpexUtilService.addRunbooksToExport(params);
    }//addRunbooksToExport
    
    public static void addComponentsToExport(Map params)
    {
    	com.resolve.services.util.ImpexUtilService.addComponentsToExport(params);
    }//addComponentsToExport
    
    public void importUpdates(Map params)
    {
        try
        {
        	String fileName = (String) params.get("FILE");
        	String fileContent = (String) params.get("CONTENT");
            String dir = RSContext.getWebHome()+"/WEB-INF/updates";
        	if(!StringUtils.isEmpty(fileName) && !StringUtils.isEmpty(fileContent))
        	{
        		Log.log.info("Create the rel file");
        		//create the file in the 'update' folder	
        		File relFile = FileUtils.getFile(dir, fileName);
        		FileUtils.writeStringToFile(relFile, fileContent);
        		
        		//import that file now
        		Log.log.info("Importing the relationship file");
        		ExportUtil.importFile(relFile);
        	}
        	else
        	{
	            Log.log.info("Export updates from dir: "+dir);
	            ExportUtil.importDirectory(dir);
        	}
        }
        catch (Throwable e)
        {
            Log.log.warn(e.getMessage(), e);
        }
    } // importUpdates
    
    public void setEventData(Map params)
    {
        // redirect to rscontrol
        
        // set EVENT_TYPE
        params.put(Constants.ESB_PARAM_EVENTTYPE, Constants.GATEWAY_EVENT_TYPE_RSVIEW); 
        Main.main.esb.sendMessage("RSCONTROL", "MAction.executeProcess", params); 
    } // setEventData
    
    public synchronized void addNameSpaceToGraphDB()
    {
        //TODO: migrate all the namespaces in DB to graph DB
        Log.log.info("--- finished add all namespace to graphdb");
    }
    
    /**
     * archive 
     * 
     * @param params
     */
    public void archive(Map<String, Object> params)
    {
        String clazz = (String) params.get(HibernateConstants.ARCHIVE_CLASS_NAME);
        String attributeName = (String) params.get(HibernateConstants.ARCHIVE_ATTRIBUTE_NAME);
        String sysId = (String) params.get(HibernateConstants.ARCHIVE_SYS_ID);
        String username = params.get(HibernateConstants.USERNAME) != null ? (String) params.get(HibernateConstants.USERNAME) : "system";

        try
        {
            if(clazz != null && StringUtils.isNotBlank(attributeName) && StringUtils.isNotEmpty(sysId))
            {
                ServiceHibernate.archive(clazz, attributeName, sysId, username);
            }
        }
        catch(Throwable t)
        {
            Log.log.error("Error archiving:", t);
        }
    }//archive
    
    
    public void purgeArchive(Map<String, Object> params)
    {
        String clazz = (String) params.get(HibernateConstants.ARCHIVE_CLASS_NAME);
        String sysIds = (String) params.get(HibernateConstants.ARCHIVE_SYS_ID);//csv of sysIds 
        String username = params.get(HibernateConstants.USERNAME) != null ? (String) params.get(HibernateConstants.USERNAME) : "system";

        try
        {
            if(clazz != null && StringUtils.isNotEmpty(sysIds))
            {
                ServiceHibernate.purgeArchive(clazz, sysIds, username);
            }
        }
        catch(Throwable t)
        {
            Log.log.error("Error purging archive:", t);
        }
    }//archive


    /**
     * cleanup wiki archive based on the configuration set
     * 
     * @param params
     */
// TODO - add to service thread
    public void cleanupWikiArchive(Map<String, Object> params)
    {
        String docId = (String)params.get(ConstantValues.WIKI_DOC_SYS_ID_KEY);
        
        try
        {
            ServiceWiki.cleanupWikiArchive(docId);
        }
        catch(Throwable t)
        {
            Log.log.error("Error in cleanupWikiArchive:", t);
        }
        
    }//cleanupWikiArchive
    
    public String updatePassword(Map<String, String> params)
    {
        String result = null;
        String username = params.get(Constants.USER_ADMIN);
        String oldPassword = params.get(Constants.OLD_PASSWORD);
        String newPassword = params.get(Constants.NEW_PASSWORD);
        
        if(StringUtils.isEmpty(oldPassword) || StringUtils.isEmpty(newPassword) || StringUtils.isEmpty(username))
        {
            result = "Missing the username, old password, or new password cannot set password";
        }
        else if(!UserUtils.checkPassword(username, oldPassword))
        {
            result = "username and password are incorrect";
        }
        else{
            if(!newPassword.startsWith("_"))
            {
                result = UserUtils.passwordIsValid(newPassword);
            }else {
                newPassword = newPassword.substring(1, newPassword.length());
            }
            if(result == null)
            {
                try 
                {
                    UserUtils.updatePassword(username, newPassword);
                    result = username + " password updated";
                }catch (Exception e)
                {
                    Log.log.error(e);
                    result = "failed to update "+username+" password";
                }
            }
        }
        return result;
    }
    public String updateMaintPassword(Map<String, String> params)
    {
        String result = null;
        String oldPassword = params.get(Constants.OLD_PASSWORD);
        String newPassword = params.get(Constants.NEW_PASSWORD);
        
        if (StringUtils.isEmpty(oldPassword) || StringUtils.isEmpty(newPassword))
        {
            result = "Missing old or new password, cannot set resolve.maint password";
        }
        else
        {
            File file = FileUtils.getFile(RSContext.getWebHome() + "/WEB-INF/users/resolve.maint");
            if (UserUtils.setMaintPassword(Constants.USER_RESOLVE_MAINT, oldPassword, newPassword, file))
            {
                result = "resolve.maint password updated";
            }
            else
            {
                result = "failed to update resolve.maint password";
            }
        }
        return result;
    }// updateMaintPassword

    @Deprecated
    public void importReport(Map params)
    {
        String reportNames = (String)params.get(Constants.ESB_PARAM_REPORTNAME);
        if (StringUtils.isNotEmpty(reportNames))
        {
            String[] reports = reportNames.split(",");
            for (int i=0; i < reports.length; i++)
            {
                String reportName = reports[i].trim();
                
		        try
		        {
		            if (StringUtils.isNotEmpty(reportName))
		            {
			            Log.log.info("Import report: "+reportName);
			            ImportReports.startReportImport(reportName);
		            }
		        }
		        catch (Throwable e)
		        {
		            Log.log.warn(e.getMessage(), e);
		        }
            }
        }
    } // importReport

    public void exportReport(Map params)
    {
        String reportNames = (String)params.get(Constants.ESB_PARAM_REPORTNAME);
        if (StringUtils.isNotEmpty(reportNames))
        {
            String[] reports = reportNames.split(",");
            for (int i=0; i < reports.length; i++)
            {
                String reportName = reports[i].trim();
                
		        try
		        {
		            if (StringUtils.isNotEmpty(reportName))
		            {
			            Log.log.info("Import report: "+reportName);
			            ImpexAPI.exportReport(reportName);
		            }
		        }
		        catch (Throwable e)
		        {
		            Log.log.warn(e.getMessage(), e);
		        }
            }
        }
    } // exportReport
    
    public String displayAuthCache(Map params)
    {
        return Authentication.printAuthTokenCache();
    } // displayAuthCache
    
    public static void backup(Map params)
    {
        //Backup.backup();
    	Log.log.error("Neo4j is removed. This feature is not available anymore.");
    } // backup

    public static void checkIndex(Map params)
    {
    	Log.log.error("Neo4j is removed. This feature is not available anymore.");
        //Backup.checkIndex();
    } // checkIndex
    
    /**
     * api called after the reviewed flag is set
     * 
     * @param params
     */
    public void submitRequest(Map params)
    {
        ContentRequestUtil.submitRequest(params);
    } // submitRequest
    
    /**
     * 
     * Mandatory values are
     * 
     * BUTTON_NAME
     * FORM_VIEW_NAME
     * USERNAME
     * LIST OF NAME VALUE PAIRS THAT MAPS TO <colname>:<value> for the custom table mapped to this form
     * 
     * @param params
     */
    public Map<String, Object> submitForm(Map params)
    {
        return ServiceHibernate.submitForm(params);
    } // submitForm
    
    /*
     * executeRequest
     */
    public void executeRequest(MMsgHeader header, Map msg) throws Exception
    {

        // init options
        MMsgOptions options = header.getOptions();
        MMsgContent content = new MMsgContent(msg);

        // pass-through options
        String actionname = options.getActionName();
        String actionnamespace = options.getActionNamespace();
        String actionsummary = options.getActionSummary();
        String actionhidden = options.getActionHidden();
        String actiontags = options.getActionTags();
        String actionid = options.getActionID();
        String processid = options.getProcessID();
        String wiki = options.getWiki();
        String executeid = options.getExecuteID();
        String parserid = options.getParserID();
        String assessid = options.getAssessID();
        String logResult = options.getLogResult();
        String userid = options.getUserID();
        String problemid = options.getProblemID();
        String reference = options.getReference();
        String address = options.getAddress();
        String target = options.getTarget();
        String affinity = options.getAffinity();
        String resultAddr = options.getResultAddr();

        // cron job params
        String taskname = options.getTaskname();
        String type = options.getType();
        String command = options.getCommand();
        String args = options.getArgs();
        String input = options.getInput();
        String delay = options.getDelay();
        String timeout = options.getTimeout();
        String processTimeout = options.getProcessTimeout();
        String metricId = options.getMetricId();

        // content
        String properties = content.getProperties();
        String debug = content.getDebug();
        String params = content.getParams();
        String flows = content.getFlows();
        String inputs = content.getInputs();

        try
        {
            // init thread processid
            Log.setProcessId(processid);

            Log.log.debug("executeRequest processid: " + processid + " executeid: " + executeid);
            // Log.log.debug("  options: "+properties);

            // get properties parameters
            Map props = (Map) StringUtils.stringToObj(properties);
            type = type.toUpperCase();

            // init execute main
            Map execParams = new HashMap();
            execParams.put("JOBNAME", taskname.toUpperCase());
            execParams.put("TYPE", type);

            if (type.equalsIgnoreCase(Constants.ACTION_INVOCATION_TYPE_OS))
            {
                String cmdline = getCmdline(command, args);
                String execpath = (String) props.get(Constants.EXECUTE_EXECPATH);
                if (execpath == null)
                {
                    execpath = Main.main.configGeneral.getHome();
                }

                execParams.put(Constants.EXECUTE_CMDLINE, cmdline);
                execParams.put(Constants.EXECUTE_INPUT, input);
                execParams.put(Constants.EXECUTE_EXECPATH, execpath);
            }
            else if (type.equalsIgnoreCase(Constants.ACTION_INVOCATION_TYPE_JAVA))
            {
                String classpath = (String) props.get(Constants.EXECUTE_CLASSPATH);
                if (classpath == null)
                {
                    classpath = "";
                }

                String javaoptions = (String) props.get(Constants.EXECUTE_JAVAOPTIONS);
                if (javaoptions == null)
                {
                    javaoptions = "";
                }

                String execpath = (String) props.get(Constants.EXECUTE_EXECPATH);
                if (execpath == null)
                {
                    execpath = Main.main.configGeneral.getHome();
                }

                execParams.put(Constants.EXECUTE_FILENAME, command);
                execParams.put(Constants.EXECUTE_ARGS, args);
                execParams.put(Constants.EXECUTE_INPUT, input);
                execParams.put(Constants.EXECUTE_EXECPATH, execpath);
                execParams.put(Constants.EXECUTE_JREPATH, Main.main.configGeneral.getHome() + "/jre");
                execParams.put(Constants.EXECUTE_CLASSPATH, classpath);
                execParams.put(Constants.EXECUTE_JAVAOPTIONS, javaoptions);
            }
            else if (type.equalsIgnoreCase(Constants.ACTION_INVOCATION_TYPE_REMOTE))
            {
                execParams.put(Constants.EXECUTE_FILENAME, command);
                execParams.put(Constants.EXECUTE_ARGS, args);
            }
            else if (type.equalsIgnoreCase(Constants.ACTION_INVOCATION_TYPE_BASH))
            {
                String cmdline = StringUtils.escapeQuotesForBash(getCmdline(command, args));
                String execpath = (String) props.get(Constants.EXECUTE_EXECPATH);
                if (execpath == null)
                {
                    execpath = Main.main.configGeneral.getHome();
                }

                execParams.put(Constants.EXECUTE_CMDLINE, "/bin/bash -c \"" + cmdline + "\"");
                execParams.put(Constants.EXECUTE_INPUT, input);
                execParams.put(Constants.EXECUTE_EXECPATH, execpath);
            }
            else if (type.equalsIgnoreCase(Constants.ACTION_INVOCATION_TYPE_CMD))
            {
                String cmdline = getCmdline(command, args);
                String execpath = (String) props.get(Constants.EXECUTE_EXECPATH);
                if (execpath == null)
                {
                    execpath = Main.main.configGeneral.getHome();
                }

                execParams.put(Constants.EXECUTE_CMDLINE, "cmd /c " + cmdline);
                execParams.put(Constants.EXECUTE_INPUT, input);
                execParams.put(Constants.EXECUTE_EXECPATH, execpath);
            }
            else if (type.equalsIgnoreCase(Constants.ACTION_INVOCATION_TYPE_CSCRIPT) || type.equalsIgnoreCase(Constants.ACTION_INVOCATION_TYPE_POWERSHELL) || type.equalsIgnoreCase(Constants.ACTION_INVOCATION_TYPE_WINEXEC) || type.equalsIgnoreCase(Constants.ACTION_INVOCATION_TYPE_WINTASK))
            {
                String cmdline = getCmdline(command, args);
                String execpath = (String) props.get(Constants.EXECUTE_EXECPATH);
                if (execpath == null)
                {
                    execpath = Main.main.configGeneral.getHome();
                }

                execParams.put(Constants.EXECUTE_CMDLINE, cmdline);
                execParams.put(Constants.EXECUTE_INPUT, input);
                execParams.put(Constants.EXECUTE_EXECPATH, execpath);
            }
            else if (type.equalsIgnoreCase(Constants.ACTION_INVOCATION_TYPE_EXTERNAL))
            {
                String cmdline = getCmdline(command, args);
                execParams.put(Constants.EXECUTE_CMDLINE, cmdline);
                execParams.put(Constants.EXECUTE_INPUT, input);
                execParams.put(Constants.EXECUTE_EXECPATH, (String) props.get(Constants.EXECUTE_EXECPATH));
            }

            // initialize parameters
            execParams.put(Constants.EXECUTE_ACTIONNAME, actionname);
            execParams.put(Constants.EXECUTE_ACTIONNAMESPACE, actionnamespace);
            execParams.put(Constants.EXECUTE_ACTIONSUMMARY, actionsummary);
            execParams.put(Constants.EXECUTE_ACTIONHIDDEN, actionhidden);
            execParams.put(Constants.EXECUTE_ACTIONTAGS, actiontags);
            execParams.put(Constants.EXECUTE_ACTIONID, actionid);
            execParams.put(Constants.EXECUTE_PROCESSID, processid);
            execParams.put(Constants.EXECUTE_WIKI, wiki);
            execParams.put(Constants.EXECUTE_EXECUTEID, executeid);
            execParams.put(Constants.EXECUTE_PARSERID, parserid);
            execParams.put(Constants.EXECUTE_ASSESSID, assessid);
            execParams.put(Constants.EXECUTE_LOGRESULT, logResult);
            execParams.put(Constants.EXECUTE_USERID, userid);
            execParams.put(Constants.EXECUTE_PROBLEMID, problemid);
            execParams.put(Constants.EXECUTE_REFERENCE, reference);
            execParams.put(Constants.EXECUTE_DELAY, delay);
            execParams.put(Constants.EXECUTE_ADDRESS, address);
            execParams.put(Constants.EXECUTE_TARGET, target);
            execParams.put(Constants.EXECUTE_AFFINITY, affinity);
            execParams.put(Constants.EXECUTE_PARAMS, params);
            execParams.put(Constants.EXECUTE_FLOWS, flows);
            execParams.put(Constants.EXECUTE_INPUTS, inputs);
            execParams.put(Constants.EXECUTE_TIMEOUT, timeout);
            execParams.put(Constants.EXECUTE_PROCESS_TIMEOUT, processTimeout);
            execParams.put(Constants.EXECUTE_METRICID, metricId);
            execParams.put(Constants.EXECUTE_JOBNAME, taskname);
            execParams.put(Constants.EXECUTE_DEBUG, debug);
            execParams.put(Constants.EXECUTE_RESULTADDR, resultAddr);

            // set properties
            setProperties(execParams, props);

            // immediate actiontask (delayed is performed at the msg delivery
            // (MListener)
            Log.log.info("Executing ActionTask name: " + taskname);
            TaskExecutor.execute(ExecuteMain.class, "execute", execParams);

        }
        finally
        {
            Log.clearProcessId();
        }

    } // executeRequest

    public void abortRequest(Map params) throws Exception
    {
        String taskname = (String) params.get(Constants.EXECUTE_TASKNAME);

        // kill process job
        Process process = ExecuteMain.getJobProcess(taskname);
        if (process != null)
        {
            Map killParams = new HashMap();
            killParams.put(Constants.EXECUTE_TYPE, "PROCESS");
            killParams.put(Constants.EXECUTE_JOBNAME, taskname.toUpperCase());
            TaskExecutor.execute(ExecuteMain.class, "kill", killParams);
        }

        // kill thread job
        else
        {
            Thread thread = ExecuteMain.getJobThread(taskname);
            if (thread != null)
            {
                Map killParams = new HashMap();
                killParams.put(Constants.EXECUTE_TYPE, "THREAD");
                killParams.put(Constants.EXECUTE_JOBNAME, taskname.toUpperCase());
                TaskExecutor.execute(ExecuteMain.class, "kill", killParams);
            }
        }
    } // abortRequest
    
    public Map<String, Object> executeRunbook(Map<String, Object> params)
    {
        String resultStr = ExecuteRunbook.start(params);
        
        Map<String, Object> result = new HashMap<String, Object>();
        result.put("RESULT", resultStr);
        
        return result;
    }
    
    public void invalidateLDAP(Map<String, Object> params)
    {
        Log.log.debug("Got message to Invalidate LDAP cache");
        ConfigLDAPVO configLDAPVO = (ConfigLDAPVO) params.get(Constants.AUTH_DOMAIN);
        ServiceAuthentication.invalidateLDAPCache(configLDAPVO, "system");
    }
    
    public void invalidateAD(Map<String, Object> params)
    {
        Log.log.debug("Got message to Invalidate AD cache");
        ConfigActiveDirectoryVO configADVO = (ConfigActiveDirectoryVO) params.get(Constants.AUTH_DOMAIN);
        ServiceAuthentication.invalidateADCache(configADVO, "system");
    }

    void setProperties(Map cronParams, Map props)
    {
        // only insert properties if not defined in cronParams
        for (Iterator i = props.entrySet().iterator(); i.hasNext();)
        {
            Map.Entry entry = (Map.Entry) i.next();
            String key = (String) entry.getKey();
            String value = (String) entry.getValue();

            if (!cronParams.containsKey(key))
            {
                cronParams.put(key, value);
            }
        }
    } // setProperties

    String getCmdline(String command, String args)
    {
        String result;

        if (StringUtils.isEmpty(args))
        {
            result = command;
        }
        else
        {
            result = command + " " + args;
        }

        return result;
    } // getCmdline
    
    public String indexAllWikiDocuments(Map<String, String> params) {
        HashMap<String, Object> p = new HashMap<String, Object>();
        MainBase.esb.sendInternalMessage(Constants.ESB_NAME_RSVIEW, "MIndex.indexAllWikiDocuments", p);
        return "Sending message to RSVIEW. Indexing all wiki documents...";
    }
    
    public String indexAllActionTasks(Map<String, String> params) {
        HashMap<String, Object> p = new HashMap<String, Object>();
        MainBase.esb.sendInternalMessage(Constants.ESB_NAME_RSVIEW, "MIndex.indexAllActionTasks", p);
        return "Sending message to RSVIEW. Indexing all action tasks...";
    }
    
    // Activates the Resolve cluster
    public Map<String, String> activateCluster(Map<String, String> params)
    {
        Log.log.info("***** Cluster activation message received *****");
        Main main = (Main) MainBase.main;
        main.activateCluster(params);
        

        // index all wikis and actiontasks
        //HashMap<String, Object> p = new HashMap<String, Object>();
        //MainBase.esb.sendInternalMessage(Constants.ESB_NAME_RSVIEW, "MIndex.indexAllWikiDocuments", p);
        //MainBase.esb.sendInternalMessage(Constants.ESB_NAME_RSVIEW, "MIndex.indexAllActionTasks", p);
        
        return getClusterStatus(params);
    }

    // Deactivates the Resolve cluster
    public Map<String, String> deactivateCluster(Map<String, String> params)
    {
        Log.log.info("***** Cluster deactivation message received *****");
        Main main = (Main) MainBase.main;
        main.deactivateCluster(params);
        return getClusterStatus(params);
    }
    
    // Gets the status of the Resolve Cluster, whether it's been activated. 
    // Returns a Map<String, String> with a key "STATUS" and a value of 
    // either "ACTIVE" and "INACTIVE"
    public Map<String, String> getClusterStatus(Map<String, String> params)
    {
        Log.log.info("***** Cluster status message received (RSVIEW). Status: " + MainBase.main.isClusterModeActive() + " *****");
        Map<String, String> status = new HashMap<String, String>();
        if(MainBase.main.isClusterModeActive()) {
            status.put("STATUS", "ACTIVE");
        } else {
            status.put("STATUS", "INACTIVE");
        }
        
        return status;
    }
    
    public void invalidateControllerAndRolesCache(Map<String, String> params)
    {
        Log.log.info("***** Invalidate Controller And Roles Cache message received *****");
        
        if(params != null && params.containsKey(Constants.MSG_SOURCE_ID) && params.get(Constants.MSG_SOURCE_ID).equalsIgnoreCase(MainBase.main.configId.getGuid()))
        {
            Log.log.info("***** Ignoring received self generated Invalidate Controller And Roles Cache message *****");
            return;
        }
        
        AppsUtil.invalidateControllerAndRolesCache();
    }
    
    /**
     * API to be called from update script to clean all bad (dangling) references present in Wiki content and in main, abort and DT models.
     * 
     */
    public void cleanWikidocBadReferences(Map<String, String> params)
    {
        ServiceWiki.cleanWikidocBadReferences();
    }
    
	public Map<String, String> esbPing(Map<String, String> params)
    {
        params.put("RESPONSE-FROM", MainBase.main.configId.getGuid());
        return params;
        //MainBase.esb.sendInternalMessage(componet, "MAction.esbPingResponse", params);
    }
    
    /**
     * Broadcasts to all RSVIEWS to call removeOldESLogs
     * <p>
     *
     * @param  params  a HashMap<String, String> object with a "fileDateThreshold" key mapped to a String value of an integer. This value represents a day count from today with
     * which to delete old files. 
     * <p>
     * <p>
     * Example: 30 = delete files older than 30 days from today.
     * @return      void
     */
    public void broadcastRemoveOldESLogs(Map<String, String> params) {
        MainBase.esb.sendInternalMessage(Constants.ESB_NAME_RSVIEWS, "MAction.removeOldESLogs", params);
    }
    
    
    /**
     * Removes all old elastic search logs in the /resolve/elasticsearch/logs directory.
     * <p>
     * 
     * @param  properties  a HashMap<String, String> object with a "fileDateThreshold" key mapped to a String value of an integer. This value represents a day count from today with
     * which to delete old files. 
     * <p>
     * <p>
     * Example: 30 = delete files older than 30 days from today.
     * @return      void
     */
    public void removeOldESLogs(Map<String, String> properties) {
        
        
        if(properties != null && properties.size() > 0 && properties.get("fileDateThreshold") != null) {
            
            Integer dateOffset = Integer.parseInt(properties.get("fileDateThreshold"));
            
            try
            {
            
                Log.log.debug("Checking for out-of-date elastic search logs.");
                
                File elasticSearchLogDirectory  = FileUtils.getFile(MainBase.main.getResolveHome() + "/elasticsearch/logs/");
                File[] logFiles                 = elasticSearchLogDirectory.listFiles();
                Calendar cal                    = Calendar.getInstance();
                Date date                       = new Date();
                cal.setTime(date);
                cal.add(Calendar.DATE, -dateOffset);
                Date dateWithOffset             = cal.getTime();
                
                if(logFiles != null) {
                    for(File logFile : logFiles) {
                        if(FileUtils.isFileOlder(logFile, dateWithOffset)) {
                            logFile.delete();
                        }
                    }
                }
            
            } catch(NumberFormatException nfe) 
            {
            
                Log.log.error("An error occured while trying to purge old elastic search log files: The offset value given in the blueprint.properties must be a an integer value.");

            } catch(Exception e)
            {
            
                Log.log.error("An error occured while trying to purge old elastic search log files.");
                Log.log.error(e.getMessage(), e);
            
            }
            
        } else {
            
            Log.log.error("An error occured while trying to purge old elastic search log files. Properties did not include a valid file date threshold.");
            
        }
    }
    
    public void invalidateRADIUS(Map<String, Object> params)
    {
        Log.log.debug("Got message to Invalidate RADIUS cache");
        ConfigRADIUSVO configRADIUSVO = (ConfigRADIUSVO) params.get(Constants.AUTH_DOMAIN);
        ServiceAuthentication.invalidateRADIUSCache(configRADIUSVO, "system");
    }
    
    @SuppressWarnings("rawtypes")
	public Map<String,Object> updateResultMacro(Map params)
    {
        Log.log.debug("STARTING RESULT MACRO UPDATE");
        try
        {
            WikiUtils.updateResultMacro(null);
        }
        catch (Throwable t)
        {
            Log.log.error("Error during result macro update", t);
        }
        Log.log.debug("FINISHED RESULT MACRO UPDATE");
        
        return new HashMap<String, Object>();
    }
    
    @SuppressWarnings("rawtypes")
	public Map<String,Object> updateCarriageReturn(Map params)
    {
        Log.log.debug("STARTING CARRIAGE RETURN UPDATE");
        try
        {
            WikiUtils.updateCarriageReturn(null);
        }
        catch (Throwable t)
        {
            Log.log.error("Error during carriage return update", t);
        }
        Log.log.debug("FINISHED CARRIAGE RETURN UPDATE");
        
        return new HashMap<String, Object>();
    }
    
    @SuppressWarnings("rawtypes")
	public Map<String,Object> fixWikiABAssociation(Map params)
    {
        Log.log.debug("STARTING FIX WIKI AUTOMATION BUILDER ASSOCOATION");
        try
        {
            WikiUtils.fixWikiABAssociation();
        }
        catch (Throwable t)
        {
            Log.log.error("Error while fixing Wiki AB Association", t);
        }
        Log.log.debug("FINISHED FIX WIKI AUTOMATION BUILDER ASSOCOATION");
        
        return new HashMap<String, Object>();
    }
    
    @SuppressWarnings("rawtypes")
	public Map<String, Object> updateSummaryAndDetailFields(Map params)
    {
    	Log.log.debug("Start migrating resolve_assess to encorporate conditional summary and detail.");
    	
    	try
    	{
    		AssessorUtil.updateSummaryAndDetailFields();
    	}
    	catch(Throwable t)
    	{
    		Log.log.error("Error while migrating resolve_assess to encorporate conditional summary and detail.", t);
    	}
    	
    	Log.log.debug("Finish migrating resolve_assess to encorporate conditional summary and detail.");
    	return new HashMap<String, Object>();
    }
    
    @SuppressWarnings("rawtypes")
    public Map<String, Object> addDefaultSecGroupsToSysProperty(Map map)
    {
        Log.log.info("Adding default security groups to system property, app.security.groups.");
        try
        {
            PlaybookUtils.addDefaultSecGroupsToSysProperty();
        }
        catch(Throwable t)
        {
            Log.log.error("Error while Adding default security groups to system property, app.security.groups.", t);
        }
        Log.log.info("Finished Adding default security groups to system property, app.security.groups.");
        return new HashMap<String, Object>();
    }
    
    @SuppressWarnings("rawtypes")
    public void terminate(Map params)
    {
        ((Main)MainBase.main).exit(null);
        // ignore
    }

    @SuppressWarnings("rawtypes")
    public Map<String, Object> updateTaskWikiDefaultRoles(Map map)
    {
        Log.log.info("Updating default roles of Actiontask, Wikidocs and Custom tables.");
        try
        {
            PropertiesUtil.updateTaskWikiDefaultRoles(null);
        }
        catch(Throwable t)
        {
            Log.log.error("Error updating default roles of Actiontask, Wikidocs and Custom tables.", t);
        }
        Log.log.info("Finished updating default roles of Actiontask, Wikidocs and Custom tables.");
        return new HashMap<String, Object>();
    }
    
    @SuppressWarnings("rawtypes")
    public Map<String, Object> reindexSirComponents(Map params)
    {
        Log.log.debug("Starting to update SIR components as modified.");
        
        try
        {
            ArchiveSIRData.reindexSirComponents();
        }
        catch(Throwable t)
        {
            Log.log.error("Error while updating SIR components as modified.", t);
        }
        
        Log.log.debug("Finish updating SIR components as modified.");
        return new HashMap<String, Object>();
    }
    
    @SuppressWarnings("rawtypes")
    public Map<String, Object> populateCaseTypeFromProperty(Map params)
    {
        Log.log.debug("Starting to populate default case types from system property.");
        
        try
        {
            PropertiesUtil.populateCaseTypeFromProperty();
        }
        catch(Throwable t)
        {
            Log.log.error("Error while populating default case types from system property.", t);
        }
        
        Log.log.debug("Finish populating default case types from system property.");
        return new HashMap<String, Object>();
    }
    
    @SuppressWarnings("rawtypes")
    public Map<String, Object> migrateSirTemplate(Map params)
    {
        Log.log.debug("Starting migration of SIR 1.0 template to SIR 2.0.");
        
        try
        {
            WikiUtils.migrateSirTemplate();
        }
        catch(Throwable t)
        {
            Log.log.error("Error while migration of SIR 1.0 template to SIR 2.0.", t);
        }
        
        Log.log.debug("Finish migration of SIR 1.0 template to SIR 2.0.");
        return new HashMap<String, Object>();
    }
    
    public void syncADFSSessionsMap(Map<String, Object> params)
    {
        Log.log.debug("Starting sync of ADFS Sessions Map.");
        
        try
        {
            SAMLAuthHandler.syncADFSSessionsMap(params);
        }
        catch(Throwable t)
        {
            Log.log.error("Error while sync of ADFS Sessions Map.", t);
        }
        
        Log.log.debug("Finish sync of ADFS Sessions Map.");
    }
    
    public Map<String, Object> populateComponentChecksum(Map<String, Object> params)
    {
        Log.log.debug("Start population of component checksum.");
        
        try
        {
            WikiUtils.updateAllWikisForChecksum();
            ActionTaskUtil.updateAllTasksForChecksum();
            ExtSaveMetaFormView.updateAllFormsForChecksum();
            ActionTaskPropertiesUtil.updateAllTaskPropertiessForChecksum();
            CustomTableUtil.updateAllCTWithChecksum();
            SystemScriptUtil.updateAllSystemScriptsForChecksum();
            WikiTemplateUtil.updateAllWikiTemplatesForChecksum();
            WikiLookupUtil.updateAllWikiLookupsForChecksum();
            TagUtil.updateAllTagsForChecksum();
            com.resolve.services.util.PropertiesUtil.updateAllPropertiesForChecksum();
            CronUtil.updateAllCronsForChecksum();
            ArtifactTypeUtil.updateAllCefKeysForChecksum();
            ArtifactTypeUtil.updateAllCustomKeysForChecksum();
            ArtifactConfigurationUtil.updateAllArtifactConfigsForChecksum();
            ArtifactTypeUtil.updateAllArtifactTypesForChecksum();
            ServiceGateway.updateGatewayFilterChecksum();
        }
        catch(Throwable t)
        {
            Log.log.error("Error while populating component checksum.", t);
        }
        
        Log.log.debug("Finish population of component checksum.");
        return new HashMap<String, Object>();
    }
    
    public static void initImportForUpdate(String moduleName, String userName) {
    	
    	while (true) {
	    	if (Log.log.isTraceEnabled()) {
		        Log.log.trace(String.format("MAction.initImportForUpdate(%s,%s) calling " +
											"ServiceHibernate.getAndSetImpexEventValue...", 
											moduleName, userName));
	    	}
	    	
	    	Pair<ResolveEventVO, Boolean> rsEventVOPair = null;
	    	
	    	try {
	    		rsEventVOPair = ServiceHibernate.getAndSetImpexEventValue("IMPORT", 0, moduleName, userName);
	    	} catch (Exception e) {
	    		if (StringUtils.isBlank(e.getMessage()) || !(e.getMessage().startsWith("Operation timed out after waiting for"))) {
	    			Log.log.warn(String.format("Failed to get and set import of module %s by user %s init event for update operation.", 
	    										moduleName, userName));
	    		}
	    	}
	    	
	        if (rsEventVOPair != null && Log.log.isTraceEnabled()) {
		        Log.log.trace(String.format("MAction.importModule(%s,%s) " +
											"ServiceHibernate.getAndSetImpexEventValue returned Resolve Event %s", 
											moduleName, userName, 
											(rsEventVOPair.getLeft() != null ? 
											 "[" + rsEventVOPair.getLeft() + "] which is " + 
											 (rsEventVOPair.getRight().booleanValue() ? "NEW" : "OLD") : "null")));
	        }

	        if (rsEventVOPair == null || !rsEventVOPair.getRight().booleanValue()) {
	        	Log.log.warn(String.format("Failed to initialize import of module %s by user %s for update operation. " +
	        							   "Will try after sleeping for 10 sec...",
	        							   moduleName, userName));
	        	try {
	        		Thread.sleep(10000);
	        	} catch (InterruptedException e) {
	        		// Ignore the exception
	        	}
	        } else {
	        	break;
	        }
    	}
    }
    
    public Map<String, Object> cleanWikiContentRelations(Map<String, Object> params)
    {
        Log.log.info("Start clean content wiki wiki rel table.");
        
        try {
            WikiUtils.cleanWikiContentRelations();
        } catch (Exception e) {
            Log.log.error("Failed to clean content wiki wiki rel table.", e);
        }
        Log.log.info("Finished clean content wiki wiki rel table.");
        return new HashMap<String, Object>();
    }
    
    public Map<String, Object> deleteWikiDocument(Map<String, Object> params)
    {
        Log.log.info("Start delete Reports.Resolution Record Reports");
        
        try {
            DeleteHelper.deleteWikiDocument(null, "Reports.Resolution Record Reports", "system");
        } catch (Exception e) {
            Log.log.error("Unable to delete Reports.Resolution Record Reports.", e);
        }
        Log.log.info("Finished delete Reports.Resolution Record Reports");
        return new HashMap<String, Object>();
    }
} // MAction
