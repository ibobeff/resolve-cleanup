package com.resolve.rsview.sso.saml.controller;

import java.io.IOException;
import java.net.URLEncoder;
import java.nio.charset.StandardCharsets;
import java.util.HashMap;
import java.util.Map;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.opensaml.DefaultBootstrap;
import org.opensaml.xml.ConfigurationException;
import org.owasp.esapi.ESAPI;
import org.owasp.esapi.errors.AccessControlException;
import com.resolve.rsview.auth.Authentication;
import com.resolve.rsview.main.Main;
import com.resolve.rsview.sso.authenticator.SSOAuthenticationDTO;
import com.resolve.rsview.sso.authenticator.SSOAuthenticator;
import com.resolve.rsview.sso.saml.SAMLAuthenticatorFactory;
import com.resolve.services.util.PropertiesUtil;
import com.resolve.rsview.sso.util.SAMLEnum;
import com.resolve.util.Constants;
import com.resolve.util.Log;
import com.resolve.util.StringUtils;

public class SAMLDispatcher extends HttpServlet {

  private static final long serialVersionUID = -5162175903196571855L;

  private static final String RESOLVE_LANDING_PATH = "/resolve/jsp/rsclient.jsp";

  private static final String DEFAULT_LOGIN_ENDPOINT = "/resolve/service/login";

  private static final String CUSTOM_LOGIN_ENDPOINT_KEY = "sso.saml.loginEndpoint";

  public SAMLDispatcher() {
    try {
      DefaultBootstrap.bootstrap();
    } catch (ConfigurationException e) {
      Log.log.error(e.getMessage(), e);
    }
    // Because the above bootstrap executes this:
    // ESAPI.initialize("org.opensaml.ESAPISecurityConfig");
    // we need to bring it back to normal
    ESAPI.initialize("org.owasp.esapi.reference.DefaultSecurityConfiguration");

    Main.startupLatch.countDown();
  }

  @Override
  public void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
    consumeSAMLResponse(request, response);
  }

  @Override
  public void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
    consumeSAMLResponse(request, response);
  }

  private void consumeSAMLResponse(HttpServletRequest request, HttpServletResponse response) throws IOException {

    SSOAuthenticator samlAuthenticator = SAMLAuthenticatorFactory.INSTANCE.build(request, response);
    SSOAuthenticationDTO authentication = samlAuthenticator.authenticate(request, response);

    String redirect = DEFAULT_LOGIN_ENDPOINT;

    if (authentication != null && authentication.getErrCode() == null) {
      // set token
      request.setAttribute(Constants.HTTP_ATTRIBUTE_CREATETIME, Long.toString(authentication.getCreationTime()));
      // SUP-18 (RS-26672) : Guardian hot-fix required for SAML 2.0 integration with PING Identity -
      // removed the SSO type for the RSTOKEN
      String token = Authentication.setAuthToken(request, response, authentication.getUsername(), request.getRemoteAddr(), null, authentication.getSessionTimeout(), null, null);
      Log.auth.trace("Setting the " + Constants.HTTP_REQUEST_TOKEN + " in the request to : " + token);

      redirect = RESOLVE_LANDING_PATH;
      
      if (PropertiesUtil.getPropertyBoolean(SAMLEnum.ADFS_ENABLED_KEY.getValue()))
	  {
        Map<String, Object> sessionMap = new HashMap<String, Object>();
        String sessionId = request.getSession().getId();
        if (SAMLAuthHandler.getAdfsSessionsMap().containsKey(sessionId))
        {
          sessionMap = (Map)SAMLAuthHandler.getAdfsSessionsMap().get(sessionId);
        }
          	
        Log.auth.trace("Setting " + authentication.getUsername() + " as Authenticated for SessionID " + sessionId);
        sessionMap.put(SAMLEnum.USERNAME.getValue(), authentication.getUsername());
        sessionMap.put(SAMLEnum.AUTHENTICATED.getValue(), true);
        SAMLAuthHandler.sendMapToSync(sessionId, sessionMap, true); 
	  }
    } else {
      if (StringUtils.isNotBlank(PropertiesUtil.getPropertyString(CUSTOM_LOGIN_ENDPOINT_KEY))) {
        redirect = PropertiesUtil.getPropertyString(CUSTOM_LOGIN_ENDPOINT_KEY);
      }
      
      if (authentication != null && authentication.getErrCode() != null) {
        if (redirect.contains("?")) {
        	redirect = redirect + "&";
        } else {
        	redirect = redirect + "?";
        }
        
        redirect = redirect + Constants.HTTP_REQUEST_SSOSERVICEERRORMSG + "=" + 
				   URLEncoder.encode(authentication.getErrCode().getMessage(), StandardCharsets.UTF_8.name());
      }
    }

    try {
      ESAPI.httpUtilities().sendRedirect(response, StringUtils.removeNewLineAndCarriageReturn(redirect));
    } catch (AccessControlException ace) {
      Log.log.error("Access denied when redirecting to " + StringUtils.removeNewLineAndCarriageReturn(redirect), ace);
      throw new IOException(ace);
    } finally {
      response.flushBuffer();
      response.getWriter().flush();
    }

  }

}
