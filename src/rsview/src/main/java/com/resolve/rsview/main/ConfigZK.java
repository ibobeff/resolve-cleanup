/******************************************************************************
* (C) Copyright 2014
*
* Resolve Systems, LLC
*
* All rights reserved
* This software is distributed under license and may not be used, copied,
* modified, or distributed without the express written permission of
* Resolve Systems, LLC.
*
******************************************************************************/

package com.resolve.rsview.main;

import com.resolve.util.ConfigMap;
import com.resolve.util.XDoc;

public class ConfigZK extends ConfigMap
{
    private static final long serialVersionUID = 4942337860042242371L;
	boolean active = true;
    
    public ConfigZK(XDoc config) throws Exception
    {
        super(config);
        
        define("active", BOOLEAN, "./ZK/@ACTIVE");
    } // ConfigMonitor
    
    public void load() throws Exception
    {
        loadAttributes();
    } // load

    public void save() throws Exception
    {
        saveAttributes();
    } // save

    public boolean isActive()
    {
        return active;
    } //isActive

    public void setActive(boolean active)
    {
        this.active = active;
    } //setActive

} // ConfigZK