package com.resolve.rsview.controller;

import java.util.List;
import java.util.Map;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.resolve.services.hibernate.util.UserUtils;
import com.resolve.services.vo.ResponseDTO;
import com.resolve.util.BlueprintUtils;
import com.resolve.util.Constants;
import com.resolve.util.Log;

@Controller
public class AjaxBlueprint extends GenericController {
    /**
     * API to read a blueprint file, either complete or just gateway configuration depending upon the parameters.
     * 
     * @param guids : List of strings representing GUID of the RSRemote(s) whoes blueprint needs to be read.
     * @param gatewayTypes : (Optional) List of strings representing gateway type(s).<br>
     *        if 'all' is sent as a part of this list, all the gateway configurations will be returned.<br>
     *        If a blank list is sent, entire blueprint file will be sent.
     * @param request : HttpServletRequest
     * @return: A String representing Json object where key will be a guid and it's value will be hirarchical<br>
     *          blueprint properties in json format.
     * @throws ServletException
     */
    @RequestMapping(value = {"/blueprint/read"}, method = {RequestMethod.GET }, headers = "Accept=application/json; charset=utf-8")
    @ResponseBody
    public ResponseDTO<Map<String, Object>> readBlueprint(@RequestParam List<String> guids,
                    @RequestParam (required = false) List<String> gatewayTypes,
                    HttpServletRequest request) throws ServletException {
        ResponseDTO<Map<String, Object>> result = new ResponseDTO<>();
        String username = (String) request.getAttribute(Constants.HTTP_REQUEST_USERNAME);
        if (UserUtils.isAdminUser(username)) {
            try {
                Map<String, Object> resultMap = BlueprintUtils.readBlueprint(guids, gatewayTypes, username);
                result.setData(resultMap);
                result.setSuccess(true);
            } catch (Exception e) {
                String errMsg = String.format("Error while reading blueprint file. guids: %s, gatewayTypes: %s", guids, gatewayTypes);
                Log.log.error(errMsg, e);
                result.setSuccess(false);
                result.setMessage(errMsg);
            }
        } else {
            result.setSuccess(false);
            result.setMessage("User does not have permission for this operation.");
        }
        return result;
    }
    
    /**
     * API to get a blueprint (BP) lock. If the lock is acquired, username and the time stamp will be returned.<br>
     * If the lock is already acquired by another user, along with the username (who has a lock) and time stamp, a warning message is returned.
     * If API is called with force (true), the caller will acquire a lock.
     * 
     * @param guid : String representing guid of the component whose blueprint (edit) lock is needed.
     * @param force : boolean if true, a lock will be forced acquired.
     * @param request : HttpServletRequest
     * @return: A Map with the information about a user who has a lock and the time when the lock is acquired.<br>
     *          E.g.<pre>
     *          {@code
     *                  {"TIME": 1555113296000, "USER": "admin"}
     *          </pre>
     *          If the lock is already arequired by someone else a warning message is returned<br>
     *          E.g. if a lock is acquired by 'admin' and is asked by another user, say, 'user'
     *          <pre> {@code
     *                  {"TIME": 1555113296000, "USER": "admin", "WARNING": "This lock is acquired by another user, admin"}
     *          </pre>
     * @throws ServletException
     */
    @RequestMapping(value = {"/blueprint/getBpLock"}, method = {RequestMethod.GET }, headers = "Accept=application/json; charset=utf-8")
    @ResponseBody
    public ResponseDTO<Map<String, Object>> getBPLock(@RequestParam String guid,
                                                      @RequestParam Boolean force,
                                                      HttpServletRequest request) throws ServletException {
        ResponseDTO<Map<String, Object>> result = new ResponseDTO<>();
        String username = (String) request.getAttribute(Constants.HTTP_REQUEST_USERNAME);
        if (UserUtils.isAdminUser(username)) {
            try {
                Map<String, Object> resultMap = BlueprintUtils.getBPLock(guid, force, username);
                result.setData(resultMap);
                result.setSuccess(true);
            } catch (Exception e) {
                String errMsg = String.format("Error while getting/acquiring BP lock. Params: guid: %s, force: %s", guid, force);
                Log.log.error(errMsg, e);
                result.setSuccess(false);
                result.setMessage(errMsg);
            }
        } else {
            result.setSuccess(false);
            result.setMessage("User does not have permission for this operation.");
        }
        return result;
    }
    
    /**
     * API to release (relinquish) the acquired lock
     * 
     * @param guid: String representing guid of the component whose blueprint (edit) lock is need to be released.
     * @param request: HttpServletRequest
     * @return: 
     * @throws ServletException
     */
    @RequestMapping(value = {"/blueprint/releaseBpLock"}, method = {RequestMethod.POST }, headers = "Accept=application/json; charset=utf-8")
    @ResponseBody
    public ResponseDTO<Map<String, Object>> releaseBpLock(@RequestParam String guid,
                                                      HttpServletRequest request) throws ServletException {
        ResponseDTO<Map<String, Object>> result = new ResponseDTO<>();
        String username = (String) request.getAttribute(Constants.HTTP_REQUEST_USERNAME);
        if (UserUtils.isAdminUser(username)) {
            try {
                BlueprintUtils.releaseBPLock(guid, username);
                result.setSuccess(true);
            } catch (Exception e) {
                String errMsg = String.format("Error while releasing BP lock. Params: guid: %s, username: %s", guid, username);
                Log.log.error(errMsg, e);
                result.setSuccess(false);
                result.setMessage(errMsg);
            }
        } else {
            result.setSuccess(false);
            result.setMessage("User does not have permission for this operation.");
        }
        return result;
    }
}
