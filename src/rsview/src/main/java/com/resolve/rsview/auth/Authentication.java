/******************************************************************************
 * (C) Copyright 2014
 *
 * Resolve Systems, LLC
 *
 * All rights reserved
 * This software is distributed under license and may not be used, copied,
 * modified, or distributed without the express written permission of
 * Resolve Systems, LLC.
 *
 ******************************************************************************/

package com.resolve.rsview.auth;

import java.io.File;
import java.io.IOException;
import java.io.PrintWriter;
import java.io.UnsupportedEncodingException;
import java.net.InetAddress;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLDecoder;
import java.net.URLEncoder;
import java.nio.charset.StandardCharsets;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.Date;
import java.util.Enumeration;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Properties;
import java.util.Set;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ConcurrentMap;
import java.util.concurrent.atomic.AtomicLong;
import java.util.stream.Collectors;

import javax.security.auth.kerberos.KerberosPrincipal;
import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang3.exception.ExceptionUtils;
import org.apache.commons.lang3.tuple.Pair;
import org.apache.http.HttpHeaders;
import org.apache.http.client.methods.HttpGet;
import org.ietf.jgss.GSSException;
import org.owasp.csrfguard.CsrfGuard;
import org.owasp.esapi.ESAPI;
import org.owasp.esapi.errors.AccessControlException;
import org.owasp.esapi.errors.IntrusionException;
import org.owasp.esapi.errors.ValidationException;

import com.google.common.collect.ImmutableList;
import com.resolve.auth.ActiveDirectory;
import com.resolve.auth.LDAP;
import com.resolve.auth.RADIUS;
import com.resolve.rsview.auth.spnego.SpnegoAuthenticator;
import com.resolve.rsview.auth.spnego.SpnegoConstants;
import com.resolve.rsview.auth.spnego.SpnegoHttpServletRequest;
import com.resolve.rsview.auth.spnego.SpnegoHttpServletResponse;
import com.resolve.rsview.auth.spnego.SpnegoPrincipal;
import com.resolve.rsview.main.Main;
import com.resolve.rsview.main.RSContext;
import com.resolve.rsview.sso.authenticator.SSOAuthenticationDTO;
import com.resolve.rsview.sso.authenticator.SSOAuthenticator;
import com.resolve.rsview.sso.authenticator.impl.SplunkAuthenticator;
import com.resolve.rsview.sso.util.SAMLEnum;
import com.resolve.rsview.sso.util.SSOUtil;
import com.resolve.services.ServiceAuthentication;
import com.resolve.services.ServiceHibernate;
import com.resolve.services.constants.ConstantValues;
import com.resolve.services.hibernate.util.RBACUtils;
import com.resolve.services.hibernate.vo.ConfigActiveDirectoryVO;
import com.resolve.services.hibernate.vo.ConfigLDAPVO;
import com.resolve.services.hibernate.vo.ConfigRADIUSVO;
import com.resolve.services.hibernate.vo.OrganizationVO;
import com.resolve.services.hibernate.vo.PropertiesVO;
import com.resolve.services.hibernate.vo.UsersVO;
import com.resolve.services.util.PropertiesUtil;
import com.resolve.services.util.UserUtils;
import com.resolve.util.Constants;
import com.resolve.util.CryptUtils;
import com.resolve.util.ERR;
import com.resolve.util.FileUtils;
import com.resolve.util.HttpUtil;
import com.resolve.util.JspUtils;
import com.resolve.util.Log;
import com.resolve.util.StringUtils;
import com.resolve.util.SyslogFormatter;
import com.resolve.util.restclient.RestCaller;

import net.sf.json.JSONObject;

public class Authentication implements Filter
{
    private static final String TIMEZONE_STR = "TIMEZONE_STR";
    
    public static final String AUTHENTICATION_SYSLOG_RSVIEW_LOGIN_SUCCESS_MESAGE_PREFIX = "Successful ";
    public static final String AUTHENTICATION_SYSLOG_RSVIEW_LOGIN_FAILED_MESAGE_PREFIX = "Failed ";
    public static final String AUTHENTICATION_SYSLOG_RSVIEW_LOGIN_MESSAGE_SUFFIX = " login to RSView";
    public static final String AUTHENTICATION_SYSLOG_RSVIEW_LOGOUT_MESSAGE_SUFFIX = " logout from RSView";
    public static final String CONTENT_SECURITY_POLICY_FRAME_ANCESTOR_ATTRIBUTE = "frame-ancestors";
    public static final String CONTENT_SECURITY_POLICY_FRAME_ANCESTOR_SELF = "'self'";
    public static final String SSOTYPE_HOST_IDENTIFIER_PREFIX = "<" + Constants.HTTP_REQUEST_SSO_TYPE.toLowerCase() + "=";
    public static final String IFRAME_HOST_IDENTIFIER_PREFIX = "<" + Constants.HTTP_REQUEST_IFRAMEID.toLowerCase() + "=";
    public static final String HOST_IDENTIFIER_SUFFIX = ">";
    public static final String ALLOWED_DOMAIN_WILD_CARD_CHARACTER = "*";
    
    public static final String AUTHENTICATION_SYSLOG_RSVIEW_LOGIN_MOCK_AUTH_TYPE_PREFIX = "MOCK";
    
    public static final String SITEMINDER_PROPERTY_PREFIX = "siteminder.";
    public static final String SITEMINDER_USERID_PROPERTY_SUFFIX = "userid.http.header";
    public static final String SITEMINDER_GROUP_PROPERTY_SUFFIX = "group.http.header";
    public static final String SITEMINDER_MOCK_PROPERTY_SUFFIX = "mock";
    public static final String SITEMINDER_GROUP_REQUIRED_PROPERTY_SUFFIX = "group.required";
    public static final String SITEMINDER_DEFAULTDOMAIN_PROPERTY_SUFFIX = "defaultdomain";
    public static final String SITEMINDER_FALLBACK_PROPERTY_SUFFIX = "fallback";
    public static final String SITEMINDER_RELOGIN_BUTTON_LABEL_PROPERTY_SUFFIX = "relogin.button.label";
    
    public static final String SERVICENOW_AUTH_TYPE = "ServiceNow";
    public static final String SITEMINDER_AUTH_TYPE = "SiteMinder";
    
    public static final String MULTIPLE_PARAM_VALUES_DELIMITER_REGEX = "\\|";
    
    public static final String USER_AGENT_MSIE_REV11_IDENTIFIER = "rv:11.0";
    public static final String USER_AGENT_MSIE_MSIE_IDENTIFIER = "msie ";
    public static final String CACHE_CONTROL_NO_CACHE_OPTION = "no-cache";
    public static final String CACHE_CONTROL_MUST_REVALIDATE_OPTION = "must-revalidate";
    public static final String CACHE_CONTROL_NO_STORE_OPTION = "no-store";
    public static final String RESOLVE_CACHE_CONTROL_OPTIONS = CACHE_CONTROL_NO_CACHE_OPTION + ", " + 
    														   CACHE_CONTROL_NO_STORE_OPTION + ", " + 
    														   CACHE_CONTROL_MUST_REVALIDATE_OPTION;
    public static final String RESOLVE_CACHE_CONTROL_MSIE_OPTIONS = CACHE_CONTROL_NO_CACHE_OPTION + ", " + 
			   														CACHE_CONTROL_MUST_REVALIDATE_OPTION;
    private static final String RESOLVE_AUTO_REFRESH_HEADER = "RESOLVE_AUTO_REFRESH";
    
	static String domain = "";

    static ConcurrentMap<String, AuthSession> authTokenCache = null;
    static AtomicLong sessionActive = new AtomicLong();
    static AtomicLong sessionMax = new AtomicLong();

    static SpnegoAuthenticator authenticator = null;
    
    static ConcurrentMap<String, IFramePreLoginSession> iFramePreLoginSessionCache = null;
    
    static ConcurrentMap<String, Map<String, String[]>> mockSSOSrvcDataCache = null;
    private static final String LOGIN_URL = "/service/login";
    private static final String PASS_LC = "pass";
    private static final String SANITIZED_PAS_SWORD = "*****";
    
    private static final List<String> uriList = ImmutableList.of ("/service/login",
                    "/service/logout",
                    "SystemService.rpc",
                    "/jsp/accessdenied.jsp",
                    "/service/passwordRecovery",
                    "/service/client/login",
                    "/client/module-loader.jsp",
                    "/jsp/sessionTimedOut",
                    "/jsp/ssoUserLockedInResolve",
                    "/jsp/ssoUserGetFromOrCreateInResolve",
                    "/jsp/ssoConfigMissingOrIncompleteInResolve",
                    "/jsp/ssoConfiguredHostAccessException",
                    "/service/client/getDefaultColorTheme",
                    "/service/client/getLoginScreenDisplayInfo",
                    "/service/client/colorThemes",
                    "/jsp/ssoAdfsUnauthorized",
                    StringUtils.substringBetween(AuthConstants
                    							 .SSO_USER_CREATE_IN_RESOLVE_DUE_TO_LICENSING_ISSUES_JSP_URL_PREFIX
                    							 .getValue(), "/resolve", ".jsp"),
                    "/sir/index.html");
    
    public static String getRemoteAddrForSysLog(HttpServletRequest request)
    {
        String remoteIPAddress = request.getHeader("X-FORWARDED-FOR"); // proxy or load balancer
        if (remoteIPAddress == null)
        {
            remoteIPAddress = request.getRemoteAddr();
        }

        return remoteIPAddress;
    }

    public static ConcurrentMap<String, AuthSession> getAuthTokenCache()
    {
        if (authTokenCache == null)
        {
            authTokenCache = new AuthTokenMap();
        }

        return authTokenCache;
    } // getAuthTokenCache

    public void init(FilterConfig filterConfig) throws ServletException
    {
        try
        {
            // pre-authenticate
            if (RSContext.getMain().getConfigWindows().isActive())
            {
                System.setProperty("java.security.krb5.conf", RSContext.getWebHome() + "/WEB-INF/krb5.conf");
                System.setProperty("java.security.auth.login.config", RSContext.getWebHome() + "/WEB-INF/login.conf");
                authenticator = new SpnegoAuthenticator(RSContext.getMain().getConfigWindows());
            }
			
			domain = RSContext.getMain().getConfigAuth().getDomain();
        }
        catch (Throwable t)
        {
            Log.log.error(t.getMessage(), t);
        }
    } // init

    public void destroy()
    {
        if (null != authenticator)
        {
            authenticator.dispose();
            authenticator = null;
        }
    } // destroy
    
    public static String getIFrameId(HttpServletRequest request)
    {
        String iframeid = null;
        
        String url = getURL(request);
        
        iframeid = request.getParameter(Constants.HTTP_REQUEST_IFRAMEID);
        
        if (StringUtils.isBlank(iframeid))
        {
            iframeid = request.getParameter(Constants.HTTP_REQUEST_IFRAMEID.toLowerCase());
        }
        
        if (StringUtils.isBlank(iframeid) && url.contains(Constants.HTTP_REQUEST_IFRAMEID.toLowerCase() + "%3D"))
        {
            iframeid = StringUtils.substringBetween(url, Constants.HTTP_REQUEST_IFRAMEID.toLowerCase() + "%3D", "%26");
            
            if (StringUtils.isBlank(iframeid))
            {
                iframeid = StringUtils.substringBetween(url, Constants.HTTP_REQUEST_IFRAMEID.toLowerCase() + "%3D", "&");

                if (StringUtils.isBlank(iframeid))
                {
                    iframeid = StringUtils.substringAfter(url, Constants.HTTP_REQUEST_IFRAMEID.toLowerCase() + "%3D");
                }
            }
        }
        
        if (StringUtils.isBlank(iframeid) && url.contains(Constants.HTTP_REQUEST_IFRAMEID + "%3D"))
        {
            iframeid = StringUtils.substringBetween(url, Constants.HTTP_REQUEST_IFRAMEID + "%3D", "%26");
            
            if (StringUtils.isBlank(iframeid))
            {
                iframeid = StringUtils.substringBetween(url, Constants.HTTP_REQUEST_IFRAMEID + "%3D", "&");
                
                if (StringUtils.isBlank(iframeid))
                {
                    iframeid = StringUtils.substringAfter(url, Constants.HTTP_REQUEST_IFRAMEID + "%3D");
                }
            }
        }
        
        if (StringUtils.isBlank(iframeid) && url.contains(Constants.HTTP_REQUEST_IFRAMEID.toLowerCase() + "%252F"))
        {
            iframeid = StringUtils.substringBetween(url, Constants.HTTP_REQUEST_IFRAMEID.toLowerCase() + "%252F", "%26");
            
            if (StringUtils.isBlank(iframeid))
            {
                iframeid = StringUtils.substringBetween(url, Constants.HTTP_REQUEST_IFRAMEID.toLowerCase() + "%252F", "&");
                
                if (StringUtils.isBlank(iframeid))
                {
                    iframeid = StringUtils.substringAfter(url, Constants.HTTP_REQUEST_IFRAMEID.toLowerCase() + "%252F");
                }
            }
        }
        
        if (StringUtils.isBlank(iframeid) && url.contains(Constants.HTTP_REQUEST_IFRAMEID + "%252F"))
        {
            iframeid = StringUtils.substringBetween(url, Constants.HTTP_REQUEST_IFRAMEID + "%252F", "%26");
            
            if (StringUtils.isBlank(iframeid))
            {
                iframeid = StringUtils.substringBetween(url, Constants.HTTP_REQUEST_IFRAMEID + "%252F", "&");
                
                if (StringUtils.isBlank(iframeid))
                {
                    iframeid = StringUtils.substringAfter(url, Constants.HTTP_REQUEST_IFRAMEID + "%252F");
                }
            }
        }
        
        if (StringUtils.isBlank(iframeid) && url.contains(Constants.HTTP_REQUEST_IFRAMEID.toLowerCase() + "%253D"))
        {
            iframeid = StringUtils.substringBetween(url, Constants.HTTP_REQUEST_IFRAMEID.toLowerCase() + "%253D", "%26");
            
            if (StringUtils.isBlank(iframeid))
            {
                iframeid = StringUtils.substringBetween(url, Constants.HTTP_REQUEST_IFRAMEID.toLowerCase() + "%253D", "&");
                
                if (StringUtils.isBlank(iframeid))
                {
                    iframeid = StringUtils.substringAfter(url, Constants.HTTP_REQUEST_IFRAMEID.toLowerCase() + "%253D");
                }
            }
        }
        
        if (StringUtils.isBlank(iframeid) && url.contains(Constants.HTTP_REQUEST_IFRAMEID + "%253D"))
        {
            iframeid = StringUtils.substringBetween(url, Constants.HTTP_REQUEST_IFRAMEID + "%253D", "%26");
            
            if (StringUtils.isBlank(iframeid))
            {
                iframeid = StringUtils.substringBetween(url, Constants.HTTP_REQUEST_IFRAMEID + "%253D", "&");
                
                if (StringUtils.isBlank(iframeid))
                {
                    iframeid = StringUtils.substringAfter(url, Constants.HTTP_REQUEST_IFRAMEID + "%253D");
                }
            }
        }
        
        if (StringUtils.isBlank(iframeid))
        {
            iframeid = request.getParameter(Constants.HTTP_REQUEST_SSO_TYPE);
            
            if (StringUtils.isBlank(iframeid) && url.contains(Constants.HTTP_REQUEST_SSO_TYPE + "%3D"))
            {
                iframeid = StringUtils.substringBetween(url, Constants.HTTP_REQUEST_SSO_TYPE + "%3D", "%26");
                
                if (StringUtils.isBlank(iframeid))
                {
                    iframeid = StringUtils.substringBetween(url, Constants.HTTP_REQUEST_SSO_TYPE + "%3D", "&");
                    
                    if (StringUtils.isBlank(iframeid))
                    {
                        iframeid = StringUtils.substringAfter(url, Constants.HTTP_REQUEST_SSO_TYPE + "%3D");
                    }
                }
            }
            
            if (StringUtils.isBlank(iframeid) && url.contains(Constants.HTTP_REQUEST_SSO_TYPE + "%253D"))
            {
                iframeid = StringUtils.substringBetween(url, Constants.HTTP_REQUEST_SSO_TYPE + "%253D", "%2526");
                
                if (StringUtils.isBlank(iframeid))
                {
//                    iframeid = StringUtils.substringBetween(url, Constants.HTTP_REQUEST_SSO_TYPE + "%253D", "%2526");
//                    
//                    if (StringUtils.isBlank(iframeid))
//                    {
                        iframeid = StringUtils.substringBetween(url, Constants.HTTP_REQUEST_SSO_TYPE + "%253D", "&");
                        
                        if (StringUtils.isBlank(iframeid))
                        {
                            iframeid = StringUtils.substringAfter(url, Constants.HTTP_REQUEST_SSO_TYPE + "%253D");
                        }
//                    }
                }
            }
        }
        
        if (StringUtils.isBlank(iframeid))
        {
            iframeid = getIFrameIdFromIFramePreLoginSessionCache(request);
        }
        
        if (StringUtils.isNotBlank(iframeid))
        {
            iframeid = iframeid.toLowerCase();
        }
        
        return iframeid;
    }
    
    public static Map<String, String[]> getSecurityHeaderSuffixes(String iFrameId, HttpServletRequest request)
    {
        Map<String, String[]> securityHeaderSuffixes = new HashMap<String, String[]>();
        
        if (PropertiesUtil.getPropertyBoolean(Constants.DISABLE_HTTP_IFRAME_INTEGRATION_SECUIRTY))
        {
            return securityHeaderSuffixes;
        }
        
        Set<String> xFrameOptionsHeaderVals = new HashSet<String>();
        
        if (StringUtils.isBlank(iFrameId))
        {
            xFrameOptionsHeaderVals.add(AuthConstants.X_FRAME_OPTIONS_SAMEORIGIN.getValue());
            securityHeaderSuffixes.put(AuthConstants.CONTENT_SECURITY_POLICY_HEADER.getValue(), 
                                       new String[] {CONTENT_SECURITY_POLICY_FRAME_ANCESTOR_ATTRIBUTE + " " + 
                                       CONTENT_SECURITY_POLICY_FRAME_ANCESTOR_SELF});
        }
        else
        {
            List<String> allowedDomainURLs = PropertiesUtil.getPropertyList(
            													Constants.SECURITY_HTTP_IFRAME_INTEGRATION_ALLOWEDURLS);
            String allowedURLsSuffix = AuthConstants.X_FRAME_OPTIONS_SAMEORIGIN.getValue();
            String frameAncestorsSuffix = null;
            
            if (allowedDomainURLs != null && !allowedDomainURLs.isEmpty())
            {
                StringBuilder sbFA = new StringBuilder();
                String faPrefix = " ";
                
                for (String allowedDomainURL : allowedDomainURLs)
                {
                    if (StringUtils.isNotBlank(allowedDomainURL))
                    {
                        String allowedDomainURLTmp = allowedDomainURL.trim().toLowerCase();
                        Map<String, String> whiteListDomainURLMap = new HashMap<String, String>();
                        
                        whiteListDomainURLMap.put(allowedDomainURLTmp, allowedDomainURLTmp);
                        
                        if ((allowedDomainURLTmp.startsWith(SSOTYPE_HOST_IDENTIFIER_PREFIX) ||
                             allowedDomainURLTmp.startsWith(IFRAME_HOST_IDENTIFIER_PREFIX)) &&
                            allowedDomainURLTmp.contains(HOST_IDENTIFIER_SUFFIX))
                        {
                            String hostIdentifierPrefix = allowedDomainURLTmp.startsWith(SSOTYPE_HOST_IDENTIFIER_PREFIX) ?
                                                          SSOTYPE_HOST_IDENTIFIER_PREFIX : IFRAME_HOST_IDENTIFIER_PREFIX;
                            
                            String ssoType = StringUtils.substringBetween(allowedDomainURLTmp, 
                                                                          hostIdentifierPrefix, 
                                                                          HOST_IDENTIFIER_SUFFIX);
                            
                            String ssoDomain = StringUtils.substringAfter(allowedDomainURLTmp, 
                                                                          HOST_IDENTIFIER_SUFFIX);
                            
                            whiteListDomainURLMap.put(ssoType, ssoDomain);
                        }
                        
                        // Check if iframeid (REFERRER/SSOTYPE parameter) is in white list of allowed domain URLs
                        
                        if (allowedURLsSuffix.equals(AuthConstants.X_FRAME_OPTIONS_SAMEORIGIN.getValue()) &&
                            whiteListDomainURLMap.containsKey(iFrameId.toLowerCase()))
                        {
                            /*
                             *  If allowed domain starts with wild card character * then check whether the request's referer 
                             *  contains allowed domain part after wild card character
                             */
                            
                            if (whiteListDomainURLMap.get(iFrameId.toLowerCase()).startsWith(ALLOWED_DOMAIN_WILD_CARD_CHARACTER) &&
                                whiteListDomainURLMap.get(iFrameId.toLowerCase()).length() >= 2)
                            {
                                String whiteListDomainURLToComp = whiteListDomainURLMap.get(iFrameId.toLowerCase()).substring(1);
                                Log.auth.trace("White listed domain URL to find in requested referer: " + whiteListDomainURLToComp);
                                
                                String requestingHost = null;
                                
                                if (getIFramePreLoginSessionCache().containsKey(request.getSession().getId()))
                                {
                                    IFramePreLoginSession ifprlsess = getIFramePreLoginSessionCache().get(request.getSession().getId());
                                    
                                    Log.auth.trace("iframe pre-login cached session: " + ifprlsess);
                                    
                                    if (StringUtils.isNotBlank(ifprlsess.getRefererHost().toLowerCase()) &&
                                        ifprlsess.getRefererHost().toLowerCase().contains(whiteListDomainURLToComp))
                                    {
                                        allowedURLsSuffix = AuthConstants.X_FRAME_OPTIONS_ALLOW_FROM.getValue() + " " + 
                                        					ifprlsess.getRefererHost();
                                        sbFA.append(faPrefix).append(ifprlsess.getRefererHost());
                                    }
                                }
                                else
                                {
                                    // Not adding check to make sure that there is no non i-frame integrated Auth session already there
                                    // as depending on # of active unique user ids i.e. auth sessions (and potetntially each auth session associated with one 
                                    // or more than one client browser sessions) the check could get really expensive.
                                    // Added code in isAllowedReferer() to not get into that situation for case where common part of 
                                    // regular URL and i-frame integration URL is same and request is made with from . If that turns out inadequate
                                    // then will have to add this check. (HP)
                                    
                                    requestingHost = getProtocolAndAuthority(request.getHeader(HttpHeaders.REFERER));
                                    
                                    Log.auth.info("Failed to find original request referer for request with session id=" + 
                                    			  request.getSession().getId() + ", iframeId=" + iFrameId + ", url=" + 
                                    			  request.getRequestURL().toString() + ", query string=" + 
                                    			  request.getQueryString() + ". Response " + 
                                    			  AuthConstants.CONTENT_SECURITY_POLICY_HEADER.getValue() + " " + 
                                    			  CONTENT_SECURITY_POLICY_FRAME_ANCESTOR_ATTRIBUTE + ", " +
                                                  AuthConstants.X_FRAME_OPTIONS_HEADER.getValue() + " " + 
                                    			  AuthConstants.X_FRAME_OPTIONS_ALLOW_FROM.getValue() + 
                                    			  " headers set to " + requestingHost + ".");
                                    
                                    if (StringUtils.isNotBlank(requestingHost) && requestingHost.toLowerCase().contains(whiteListDomainURLToComp))
                                    {
                                        allowedURLsSuffix = AuthConstants.X_FRAME_OPTIONS_ALLOW_FROM.getValue() + " " + 
                                        					requestingHost;
                                        sbFA.append(faPrefix).append(requestingHost);
                                    }
                                }
                            }
                            else
                            {
                                allowedURLsSuffix = AuthConstants.X_FRAME_OPTIONS_ALLOW_FROM.getValue() + " " + 
                                					whiteListDomainURLMap.get(iFrameId.toLowerCase());
                                sbFA.append(faPrefix).append(whiteListDomainURLMap.get(iFrameId.toLowerCase()));
                            }
                            break;
                        }
                    }
                }
                
                frameAncestorsSuffix = sbFA.toString();
            }
            
            xFrameOptionsHeaderVals.add(allowedURLsSuffix);
            securityHeaderSuffixes.put(AuthConstants.CONTENT_SECURITY_POLICY_HEADER.getValue(), 
                                       new String[] {CONTENT_SECURITY_POLICY_FRAME_ANCESTOR_ATTRIBUTE + " " + 
                                                     CONTENT_SECURITY_POLICY_FRAME_ANCESTOR_SELF + frameAncestorsSuffix + ";"});
        }
        
        String[] xFrameOptionsHeaderValsArray = new String[xFrameOptionsHeaderVals.size()];
        xFrameOptionsHeaderValsArray = xFrameOptionsHeaderVals.toArray(xFrameOptionsHeaderValsArray);
        
        securityHeaderSuffixes.put(AuthConstants.X_FRAME_OPTIONS_HEADER.getValue(), xFrameOptionsHeaderValsArray);
        
        return securityHeaderSuffixes;
    }
    private static String sanitizeUrl(String url)
    {
    	String sanitizedUrl = url;
    	if(sanitizedUrl.contains("password="))
    	{
    		sanitizedUrl = url.replaceAll("password=[^&]*","password=******");
    	}
    	if(sanitizedUrl.contains("_password_="))
    	{
    		sanitizedUrl = url.replaceAll("_password_=[^&]*", "_password_=******");
    	}
    	if(sanitizedUrl.contains("password%3D"))
    	{
    		sanitizedUrl = url.replaceAll("password%3D[^%]*", "password%3D******");
    	}
    	if(sanitizedUrl.contains("_password_%3D"))
    	{
    		sanitizedUrl = url.replaceAll("_password_%3D[^%]*", "_password_%3D******");
    	}
    	return sanitizedUrl;
    	
    }
    public void doFilter(ServletRequest req, ServletResponse resp, FilterChain chain) throws IOException, ServletException
    {
        boolean isRedirect = false;
        boolean internal = false;
        
        HttpServletRequest request = new MultiReadHttpServletRequest((HttpServletRequest) req);
        HttpServletResponse response = new XFrameOptionsHttpServletResponse((HttpServletResponse)resp);
        
        // http://www.javafaq.nu/java-example-code-235.html - we can use this filter also if required
        // I did not added it as our app is UTF8 and we don't need any other type presently.
        request.setCharacterEncoding(StandardCharsets.UTF_8.name());

        if (!isAllowedReferer(request)) {
            Log.auth.warn("Forbidden referer: " + request.getHeader(HttpHeaders.REFERER) + 
                          (StringUtils.isNotBlank(getIFrameId(request)) ? 
                           " for i-frame integration with i-frame integration id " + getIFrameId(request) : ""));
            response.sendError(HttpServletResponse.SC_FORBIDDEN);
            return;
        }

        /*
         * For IE, Platform For Privacy Preference (P3P) header is must to accept cross domain cookies.
         * With Resolve iframe integrated into third party applications such as ServcieNow
         * results in cross domain cookies getting generated.
         */

        if (StringUtils.isBlank(response.getHeader("p3p")))
        {
            response./*addHeader*/setHeader("p3p", "CP=\"JUSTFORIE\"");
        }
        
        String ua = request.getHeader(HttpHeaders.USER_AGENT).toLowerCase();
        boolean isIE = ((ua.indexOf(USER_AGENT_MSIE_REV11_IDENTIFIER) != -1) || 
        				(ua.indexOf("msie ") != -1)) ? true : false;
        response.setDateHeader(HttpHeaders.EXPIRES, 0);
        if (isIE) {
            // RBA-16405
            // IE doesn't render awesomefont icons if no-cache is set for Pragma
            // and no-store is set for Cache-control. W/o these 2 fields set,
            // ZAP scanning will report as a low-risk level which should be OK
            response.setHeader(HttpHeaders.CACHE_CONTROL, RESOLVE_CACHE_CONTROL_MSIE_OPTIONS);
        } else {
            response.setHeader(HttpHeaders.PRAGMA, CACHE_CONTROL_NO_CACHE_OPTION);
            response.setHeader(HttpHeaders.CACHE_CONTROL, RESOLVE_CACHE_CONTROL_OPTIONS);
        }
        
        String iframeid = getIFrameId(request);
        
        Log.auth.trace("doFilter iframeid: " + iframeid);
        
        Map<String, String[]> securityHeaderSuffixes = getSecurityHeaderSuffixes(iframeid, request);
        
        Log.auth.trace("doFilter securityHeaderSuffixes: " + StringUtils.mapToString(securityHeaderSuffixes, "=", ", "));
        
        Map<String, String[]> xHeaders = new HashMap<String, String[]>();
        
        String cspHeader = request.isSecure() ? 
        				   AuthConstants.CONTENT_SECURITY_POLICY_UPGRADE_INSECURE_REQUESTS.getValue() : null;
        
        if (securityHeaderSuffixes != null && 
        	securityHeaderSuffixes.containsKey(AuthConstants.CONTENT_SECURITY_POLICY_HEADER.getValue()))
        {
            cspHeader = StringUtils.isNotBlank(cspHeader) ? cspHeader + "; " + 
            			securityHeaderSuffixes.get(AuthConstants.CONTENT_SECURITY_POLICY_HEADER.getValue())[0] :
                        securityHeaderSuffixes.get(AuthConstants.CONTENT_SECURITY_POLICY_HEADER.getValue())[0];
            //xHeaders.put(CONTENT_SECURITY_POLICY_HEADER, securityHeaderSuffixes.get(CONTENT_SECURITY_POLICY_HEADER));
        }
        
        if (StringUtils.isNotBlank(cspHeader))
        {
            xHeaders.put(AuthConstants.CONTENT_SECURITY_POLICY_HEADER.getValue(), new String[] {cspHeader});
        }
        
        if (securityHeaderSuffixes != null && 
        	securityHeaderSuffixes.containsKey(AuthConstants.X_FRAME_OPTIONS_HEADER.getValue()))
        {
            xHeaders.put(AuthConstants.X_FRAME_OPTIONS_HEADER.getValue(), 
            			 securityHeaderSuffixes.get(AuthConstants.X_FRAME_OPTIONS_HEADER.getValue()));
        }
        
        xHeaders.put(AuthConstants.X_XSS_PROTECTTION_HEADER.getValue(), 
        			 new String[] {AuthConstants.X_XSS_PROTECTTION_ENABLED_MODE_BLOCKED.getValue()});
        xHeaders.put(AuthConstants.X_CONTENT_TYPE_OPTIONS_HEADER.getValue(), 
        			 new String[] {AuthConstants.X_CONTENT_TYPE_OPTIONS_NOSNIFF.getValue()});
        
        SpnegoPrincipal principal = null;
        SpnegoHttpServletResponse spnegoResponse = null;

        // check if internal
        if (StringUtils.isNotBlank(request.getParameter(Constants.HTTP_REQUEST_INTERNAL_USERNAME)))
        {
            internal = true;

            String ssoType = request.getParameter(Constants.HTTP_REQUEST_SSO_TYPE);

            if (StringUtils.isBlank(ssoType))
            {
                String ssoInfo = request.getParameter("USource");

                if (StringUtils.isNotBlank(ssoInfo))
                {
                    Map<String, String> ssoInfoMap = StringUtils.jsonObjectToMap((StringUtils.stringToJSONObject(ssoInfo)));

                    ssoType = ssoInfoMap.get(Constants.HTTP_REQUEST_SSO_TYPE);
                }
            }
            
            if (StringUtils.isBlank(ssoType))
            {
                ssoType = iframeid;
            }
            
            // For public user do not create cookie
            if (request.getParameter(Constants.HTTP_REQUEST_INTERNAL_USERNAME).equalsIgnoreCase(Constants.USER_PUBLIC))
            {
                invalidate(request, response, ssoType);
            }
        }

        // init request time
        request.setAttribute(Constants.HTTP_ATTRIBUTE_CREATETIME, "" + System.currentTimeMillis());
        
        String decodedURL = getDecodedURL(request);
        Log.auth.trace("doFilter url: " + sanitizeUrl(decodedURL));

        // check that url doesnt start with /service/login or /service/logout
        if (!uriList.stream().anyMatch(s -> decodedURL.startsWith(s)) ||
                        (decodedURL.startsWith(LOGIN_URL) && StringUtils.isNotBlank(iframeid) && 
                         request.getMethod().equals(HttpGet.METHOD_NAME) &&
                         StringUtils.isBlank(request.getParameter(Constants.HTTP_REQUEST_SSOSERVICEID)) &&
                         requestHasSiteMinderHeaders(request)))
        {
            try
            {
                // Windows SSO and NOT INTERNAL
                if (!internal && RSContext.getMain().getConfigWindows().isActive())
                {
                    spnegoResponse = new SpnegoHttpServletResponse((HttpServletResponse) response);
                    principal = validate(request, spnegoResponse, xHeaders);
                }
                else if (!internal && PropertiesUtil.getPropertyBoolean(SAMLEnum.ADFS_ENABLED_KEY.getValue())
                                && StringUtils.isNotBlank(PropertiesUtil.getPropertyString(SAMLEnum.ADFS_ENDPOINT_KEY.getValue())))
                {
                    /*
                     * Every time a request is made to Resolve and if sso.adfs.enabled is true,
                     * Request is going to come here.
                     */
                    SSOUtil.INSTANCE.handleSsoAdfsRequest(request, response);
                }

                // INTERNAL, AD, LDAP
                else
                {
                    principal = validate(request, response, xHeaders);
                }
            }
            catch (RSAccessException e)
            {
                String error = StringUtils.isNotBlank(e.getMessage()) ? e.getMessage() : 
                			   "User does not have application rights for this operation.";
                
                if (requestHasSiteMinderHeaders(request))
                {
                    String redirectPrefix = AuthConstants.RESOLVE_SERVICE_LOGIN_URL_QUESTION_PREFIX.getValue() + 
                    						Constants.HTTP_REQUEST_SSOSERVICEID + "=" + 
                    						URLEncoder.encode(Authentication.SITEMINDER_AUTH_TYPE, 
                    										  StandardCharsets.UTF_8.name());
                    
                    Map<String, String[]> siteMinderHeaders = Authentication.getSiteMinderHeaders(request);
                    
                    if (siteMinderHeaders != null && !siteMinderHeaders.isEmpty())
                    {
                        StringBuilder redirectStrngBldr = new StringBuilder(redirectPrefix);
                        
                        for (String siteMinderHeaderName : siteMinderHeaders.keySet())
                        {
                            String[] siteMinderHeaderValues = siteMinderHeaders.get(siteMinderHeaderName);
                            
                            for (int i = 0; i < siteMinderHeaderValues.length; i++)
                            {
                                redirectStrngBldr.append("&").append(siteMinderHeaderName).append("=").append(URLEncoder.encode(siteMinderHeaderValues[i], StandardCharsets.UTF_8.name()));
                            }
                        }
                        
                        redirectStrngBldr.append("&").append(Constants.HTTP_REQUEST_SSOSERVICEERRORMSG).append("=").append(URLEncoder.encode(error + " Please contact Resolve system administrator.", StandardCharsets.UTF_8.name()));
                        
                        redirectPrefix = redirectStrngBldr.toString();
                    }
                    
                    try {
						ESAPI.httpUtilities().sendRedirect(response, 
														   ESAPI.validator().getValidInput("SiteMinder Access Exception", 
																   						   redirectPrefix, "Redirect", 
																   						   8192, false));
                    	} catch (AccessControlException | IOException | ValidationException | IntrusionException ace) {
                    		Log.auth.error("Error " + ace.getLocalizedMessage() + " occurred while redirecting to " +
                    					   redirectPrefix, ace);
                    		throw new ServletException("Error " + ace.getLocalizedMessage() + " occurred in redirect.", ace);
                    	}
                }
                else
                {
                	Log.log.warn(String.format("Error [%s] in doFilter()", error));
                    PrintWriter out = response.getWriter();
                    out.print("{\"success\":false,\"message\":\"Error processing request\"}");
                    // response.sendRedirect("/resolve/jsp/accessdenied.jsp");
                }
                isRedirect = true;
            }
            catch (RSAuthException e)
            {
                Log.auth.trace("validate resulted in error " + e.getLocalizedMessage() + ".", e);
                
                // check if the url is public
                if (decodedURL.contains("/public/"))
                {
                	Authentication.regenerateSession(request);
                    AuthenticateResult authResult = authenticate(Constants.USER_PUBLIC, "");
                    if (authResult.valid)
                    {
                        long DEFAULT_SESSION_TIMEOUT = (PropertiesUtil.getPropertyLong(ConstantValues.PROPERTIES_SESSION_TIMEOUT) < 720) ? 720000 : PropertiesUtil.getPropertyLong(ConstantValues.PROPERTIES_SESSION_TIMEOUT) * 1000;

                        String token = setAuthToken(request, response, Constants.USER_PUBLIC, request.getRemoteAddr(), null, DEFAULT_SESSION_TIMEOUT, null, iframeid);
                        request.setAttribute(Constants.HTTP_REQUEST_USERNAME, Constants.USER_PUBLIC);
                        request.setAttribute(Constants.HTTP_REQUEST_TOKEN, CryptUtils.encryptToken(token));
                        Collection<String> tokens = new ArrayList<String>(1);
                        tokens.add(token);
                        ValidateTokenResult valTokenResult = validateToken(tokens, request, response);
                        String iframeId = request.getParameter(Constants.HTTP_REQUEST_IFRAMEID);
                        boolean valid = valTokenResult.isValid();
                        if (!valid)
                        {
                            Log.auth.error("Unable to validate token user \"public\".");
                            Log.log.error("Unable to validate token user \"public\".");
                            
                            String syslogMessage = AUTHENTICATION_SYSLOG_RSVIEW_LOGIN_FAILED_MESAGE_PREFIX + authResult.authType + AUTHENTICATION_SYSLOG_RSVIEW_LOGIN_MESSAGE_SUFFIX;
                            Log.syslog.warn(SyslogFormatter.INSTANCE.formatAudit(getRemoteAddrForSysLog(request), "Token user " + Constants.USER_PUBLIC, request.getLocalAddr(), syslogMessage));
                            
                            Log.auth.trace("Decoded URL: " + sanitizeUrl(decodedURL));
                            
                            if (!decodedURL.contains(JspUtils.getCSRFTokenName()))
                            {
                                String[] csrfPageTokenDetails = 
                                			JspUtils.getCSRFTokenForPage(
                                					request, AuthConstants.RESOLVE_SERVICE_LOGIN_URL.getValue());
                            
                                try {
									ESAPI.httpUtilities().sendRedirect(
										response, 
										AuthConstants.RESOLVE_SERVICE_LOGIN_URL_QUESTION_URL_PARAMETER_PREFIX.getValue() + 
										URLEncoder.encode(decodedURL, StandardCharsets.UTF_8.name()) + 
										"&" + csrfPageTokenDetails[0] + "=" + csrfPageTokenDetails[1]);
								} catch (AccessControlException | IOException ace) {
									Log.auth.error(
										"Error " + ace.getLocalizedMessage() + " occurred while redirecting to " +
										AuthConstants.RESOLVE_SERVICE_LOGIN_URL_QUESTION_URL_PARAMETER_PREFIX.getValue() + 
										URLEncoder.encode(decodedURL, StandardCharsets.UTF_8.name()) + 
										"&" + csrfPageTokenDetails[0] + "=" + csrfPageTokenDetails[1], ace);
									throw new ServletException("Error " + ace.getLocalizedMessage() + " occurred in redirect.", ace);
								}
                            }
                            else
                            {
                            	try {
									ESAPI.httpUtilities().sendRedirect(
										response,
										AuthConstants.RESOLVE_SERVICE_LOGIN_URL_QUESTION_URL_PARAMETER_PREFIX.getValue() + 
										URLEncoder.encode(decodedURL, StandardCharsets.UTF_8.name()));
								} catch (AccessControlException | IOException ace) {
									Log.auth.error(
										"Error " + ace.getLocalizedMessage() + " occurred while redirecting to " +
										AuthConstants.RESOLVE_SERVICE_LOGIN_URL_QUESTION_URL_PARAMETER_PREFIX.getValue() +
										URLEncoder.encode(decodedURL, StandardCharsets.UTF_8.name()), ace);
									throw new ServletException("Error " + ace.getLocalizedMessage() + 
															   " occurred in redirect.", ace);
								}
                            }
                            isRedirect = true;
                        } else if (iframeId != null) {
                            cacheIFramePreLoginSession(
                                new IFramePreLoginSession(
                                    request.getSession().getId(),
                                    iframeId.toLowerCase(), 
                                    getProtocolAndAuthority(request.getHeader(HttpHeaders.REFERER))
                               )
                            );
                        }
                    }
                }
                else
                {
                    if (!response.isCommitted())
                    {
                        Log.auth.trace("Decoded URL: " + sanitizeUrl(decodedURL));
                        
                        if (StringUtils.isNotBlank(request.getParameter(Constants.HTTP_REQUEST_INTERNAL_USERNAME)) &&
                            StringUtils.isNotBlank(request.getParameter(Constants.HTTP_REQUEST_INTERNALP_ASSWORD)))
                        {
                            String intUserName = request.getParameter(Constants.HTTP_REQUEST_INTERNAL_USERNAME);
                            String intP_assword = request.getParameter(Constants.HTTP_REQUEST_INTERNALP_ASSWORD);
                            
                            AuthenticateResult authResult = authenticate(intUserName, intP_assword);
                            
                            if (authResult.valid)
                            {
                                try {
									ESAPI.httpUtilities().sendRedirect(response, decodedURL);
								} catch (AccessControlException | IOException ace) {
									Log.auth.error("Error " + ace.getLocalizedMessage() + 
												   " occurred while redirecting to " + decodedURL + ".", ace);
									throw new ServletException("Error " + 
															   ace.getLocalizedMessage() + " in redirect.", ace);
								}
                                isRedirect = false;
                            }
                            else
                            {
                                /*
                                 *  MultiReadHttpServletRequest wrapper filters out CSRFToken, _token_ and 
                                 *  duplicate values for parameter (if any) from query string
                                 */
                                
                                if (!decodedURL.contains(JspUtils.getCSRFTokenName()))
                                {
                                	try {
										ESAPI.httpUtilities().sendRedirect(
											response, 
											AuthConstants.RESOLVE_SERVICE_LOGIN_URL_QUESTION_URL_PARAMETER_PREFIX.
											getValue() + 
											URLEncoder.encode(decodedURL, StandardCharsets.UTF_8.name()));
									} catch (AccessControlException | IOException ace) {
										Log.auth.error(
											"Error " + ace.getLocalizedMessage() + 
											AuthConstants.RESOLVE_SERVICE_LOGIN_URL_QUESTION_URL_PARAMETER_PREFIX.
											getValue() + 
											 URLEncoder.encode(decodedURL, 
															   StandardCharsets.UTF_8.name()) + ".", ace);
										throw new ServletException("Error " + 
																   ace.getLocalizedMessage() + " in redirect.", ace);
									}
                                }
                                isRedirect = true;
                            }
                        }
                        else
                        {
                            String redirectPrefix = AuthConstants.RESOLVE_SERVICE_LOGIN_URL_QUESTION_PREFIX.getValue();
                            
                            if (requestHasSiteMinderHeaders(request))
                            {                                
                                redirectPrefix += Constants.HTTP_REQUEST_SSOSERVICEID + "=" + URLEncoder.encode(Authentication.SITEMINDER_AUTH_TYPE, StandardCharsets.UTF_8.name());
                                
                                Map<String, String[]> siteMinderHeaders = Authentication.getSiteMinderHeaders(request);
                                
                                if (siteMinderHeaders != null && !siteMinderHeaders.isEmpty())
                                {
                                    StringBuilder redirectStrngBldr = new StringBuilder(redirectPrefix);
                                    
                                    for (String siteMinderHeaderName : siteMinderHeaders.keySet())
                                    {
                                        String[] siteMinderHeaderValues = siteMinderHeaders.get(siteMinderHeaderName);
                                        
                                        for (int i = 0; i < siteMinderHeaderValues.length; i++)
                                        {
                                            redirectStrngBldr.append("&").append(siteMinderHeaderName).append("=").append(URLEncoder.encode(siteMinderHeaderValues[i], StandardCharsets.UTF_8.name()));
                                        }
                                    }
                                    
                                    redirectStrngBldr.append("&").append(Constants.HTTP_REQUEST_SSOSERVICEERRORMSG).append("=").append(URLEncoder.encode(e.getLocalizedMessage() + " Please contact Resolve system administrator.", StandardCharsets.UTF_8.name()));
                                    
                                    redirectPrefix = redirectStrngBldr.toString();
                                }
                                
                                redirectPrefix += "&url=";
                            }
                            else
                            {
                                redirectPrefix += "url=";
                            }
                            
                            try {
								ESAPI.httpUtilities().sendRedirect(response, redirectPrefix + 
																   URLEncoder.encode(decodedURL, 
																		   			 StandardCharsets.UTF_8.name()));
							} catch (AccessControlException | IOException ace) {
								Log.auth.error("Error " + ace.getLocalizedMessage() + " occurred while redirecting to " +
											   redirectPrefix + URLEncoder.encode(decodedURL, 
													   							  StandardCharsets.UTF_8.name()) + ".", 
											   ace);
								throw new ServletException("Error " + 
										   					ace.getLocalizedMessage() + " in redirect.", ace);
							}
                            isRedirect = true;
                        }
                    }
                    else
                    {
                        Log.log.info("Response already commited, skipping login redirect");
                        Log.auth.info("Response already commited, skipping login redirect");
                        isRedirect = true;
                    }
                    //isRedirect = true;
                }
            }
            catch (ServletException se)
            {
                throw se;
            }
            catch (IOException ioe)
            {
                throw ioe;
            }
            catch (SSOSessTimedOutException ssotoe)
            {
                Log.auth.trace("doFilter validate threw " + ssotoe);

                if (!ssotoe.getUrl().endsWith("poll"))
                {
                	try {
						ESAPI.httpUtilities().sendRedirect(
												response, 
												AuthConstants.RESOLVE_SESSION_TIMED_OUT_JSP_URL_PREFIX.getValue() + 
												ssotoe.getExternalSystem() + "&" + 
												Constants.HTTP_REQUEST_SSO_TYPE + "=" + 
												ssotoe.getExternalSystem());
					} catch (AccessControlException | IOException ace) {
						Log.auth.error("Error " + ace.getLocalizedMessage() + " occurred while redirecting to " +
									   AuthConstants.RESOLVE_SESSION_TIMED_OUT_JSP_URL_PREFIX.getValue() + 
									   ssotoe.getExternalSystem() + "&" + Constants.HTTP_REQUEST_SSO_TYPE + "=" + 
									   ssotoe.getExternalSystem(), ace);
						throw new ServletException("Error " + 
								   					ace.getLocalizedMessage() + " in redirect.", ace);
					}
                    isRedirect = true;
                }
                else
                {
                    // In case of client/poll request continue to Ajax call after setting Redirect URL attribute
                    request.setAttribute(Constants.HTTP_ATTRIBUTE_SSO_REDIRECT_URL, 
                    					 AuthConstants.RESOLVE_SESSION_TIMED_OUT_JSP_URL_PREFIX.getValue() + 
                                         ssotoe.getExternalSystem() + "&" + Constants.HTTP_REQUEST_SSO_TYPE + 
                                         "=" + ssotoe.getExternalSystem());
                }
            }
            catch (SSOUserLockedInResolveException ssoule)
            {
                Log.auth.trace("doFilter validate threw " + ssoule);

                if (!ssoule.getUrl().endsWith("poll"))
                {
                	try {
						ESAPI.httpUtilities().sendRedirect(
												response,
												AuthConstants.SSO_USER_LOCKED_IN_RESOLVE_JSP_URL_PREFIX.getValue() +
												ssoule.getExternalSystem() + "&" + Constants.HTTP_REQUEST_SSO_TYPE + "=" + 
												ssoule.getExternalSystem());
					} catch (AccessControlException | IOException ace) {
						Log.auth.error("Error " + ace.getLocalizedMessage() + " occurred while redirecting to " +
									   AuthConstants.SSO_USER_LOCKED_IN_RESOLVE_JSP_URL_PREFIX.getValue() + 
									   ssoule.getExternalSystem() + "&" + Constants.HTTP_REQUEST_SSO_TYPE + "=" + 
									   ssoule.getExternalSystem() + ".", ace);
						throw new ServletException("Error " + 
								   					ace.getLocalizedMessage() + " in redirect.", ace);
					}
                    isRedirect = true;
                }
                else
                {
                    // In case of client/poll request continue to Ajax call after setting Redirect URL attribute
                    request.setAttribute(Constants.HTTP_ATTRIBUTE_SSO_REDIRECT_URL, 
                    					 AuthConstants.SSO_USER_LOCKED_IN_RESOLVE_JSP_URL_PREFIX.getValue() + 
                                         ssoule.getExternalSystem() + "&" + Constants.HTTP_REQUEST_SSO_TYPE + 
                                         "=" + ssoule.getExternalSystem());
                }
            }
            catch (SSOUserGetFromOrCreateInResolveException ssougoce)
            {
                Log.auth.trace("doFilter validate threw " + ssougoce);

                if (!ssougoce.getUrl().endsWith("poll"))
                {
                	try {
						ESAPI.httpUtilities().sendRedirect(
							response,
							AuthConstants.SSO_USER_GET_FROM_OR_CREATE_IN_RESOLVE_JSP_URL_PREFIX.getValue() +
							ssougoce.getExternalSystem() + "&" + Constants.HTTP_REQUEST_SSO_TYPE + 
							"=" + ssougoce.getExternalSystem());
					} catch (AccessControlException | IOException ace) {
						Log.auth.error("Error " + ace.getLocalizedMessage() + " occurred while redirecting to " +
									   AuthConstants.SSO_USER_GET_FROM_OR_CREATE_IN_RESOLVE_JSP_URL_PREFIX.getValue() +
								       ssougoce.getExternalSystem() + "&" + Constants.HTTP_REQUEST_SSO_TYPE + 
									   "=" + ssougoce.getExternalSystem() + ".", ace);
						throw new ServletException("Error " + 
								   					ace.getLocalizedMessage() + " in redirect.", ace);
					}
                    isRedirect = true;
                }
                else
                {
                    // In case of client/poll request continue to Ajax call after setting Redirect URL attribute
                    request.setAttribute(Constants.HTTP_ATTRIBUTE_SSO_REDIRECT_URL, 
                    					 AuthConstants.SSO_USER_GET_FROM_OR_CREATE_IN_RESOLVE_JSP_URL_PREFIX.getValue() + 
                                         ssougoce.getExternalSystem() + "&" + Constants.HTTP_REQUEST_SSO_TYPE + 
                                         "=" + ssougoce.getExternalSystem());
                }
            }
            catch (SSOConfigMissingOrIncompleteInResolveException ssocmorice)
            {
                Log.auth.trace("doFilter validate threw " + ssocmorice);

                if (!ssocmorice.getUrl().endsWith("poll"))
                {
                	try {
						ESAPI.httpUtilities().sendRedirect(
									response,
									AuthConstants.SSO_CONFIG_MISSING_OR_INCOMPLETE_IN_RESOLVE_JSP_URL_PREFIX.getValue() + 
									ssocmorice.getExternalSystem() + "&" + Constants.HTTP_REQUEST_SSO_TYPE + 
									"=" + ssocmorice.getExternalSystem());
					} catch (AccessControlException | IOException ace) {
						Log.auth.error(
								"Error " + ace.getLocalizedMessage() + " occurred while redirecting to " +
								AuthConstants.SSO_CONFIG_MISSING_OR_INCOMPLETE_IN_RESOLVE_JSP_URL_PREFIX.getValue() + 
								ssocmorice.getExternalSystem() + "&" + Constants.HTTP_REQUEST_SSO_TYPE + 
								"=" + ssocmorice.getExternalSystem() + ".", ace);
						throw new ServletException("Error " + ace.getLocalizedMessage() + " in redirect.", ace);
					}
                    isRedirect = true;
                }
                else
                {
                    // In case of client/poll request continue to Ajax call after setting Redirect URL attribute
                    request.setAttribute(
                    				Constants.HTTP_ATTRIBUTE_SSO_REDIRECT_URL, 
                    				AuthConstants.SSO_CONFIG_MISSING_OR_INCOMPLETE_IN_RESOLVE_JSP_URL_PREFIX.getValue() + 
                                    ssocmorice.getExternalSystem() + "&" + Constants.HTTP_REQUEST_SSO_TYPE + 
                                    "=" + ssocmorice.getExternalSystem());
                }
            }
            catch (SSOConfiguredHostAccessException ssochae)
            {
                Log.auth.trace("doFilter validate threw " + ssochae);

                if (!ssochae.getUrl().endsWith("poll"))
                {
                	try {
						ESAPI.httpUtilities().sendRedirect(
									response,
									AuthConstants.SSO_CONFIGURED_HOST_ACCESS_EXCEPTION_JSP_URL_PREFIX.getValue() + 
									ssochae.getExternalSystem() + "&" + Constants.HTTP_REQUEST_SSO_TYPE + 
									"=" + ssochae.getExternalSystem());
					} catch (AccessControlException | IOException ace) {
						Log.auth.error("Error " + ace.getLocalizedMessage() + " occurred while redirecting to " +
									   AuthConstants.SSO_CONFIGURED_HOST_ACCESS_EXCEPTION_JSP_URL_PREFIX.getValue() + 
									   ssochae.getExternalSystem() + "&" + Constants.HTTP_REQUEST_SSO_TYPE + 
									   "=" + ssochae.getExternalSystem() + ".", ace);
						throw new ServletException("Error " + 
								   					ace.getLocalizedMessage() + " in redirection.", ace);
					}
                    isRedirect = true;
                }
                else
                {
                    // In case of client/poll request continue to Ajax call after setting Redirect URL attribute
                    request.setAttribute(Constants.HTTP_ATTRIBUTE_SSO_REDIRECT_URL, 
                    					 AuthConstants.SSO_CONFIGURED_HOST_ACCESS_EXCEPTION_JSP_URL_PREFIX.getValue() + 
                                         ssochae.getExternalSystem() + "&" + Constants.HTTP_REQUEST_SSO_TYPE + 
                                         "=" + ssochae.getExternalSystem());
                }
            } catch (SSOUserCreateInResolveDueToLicensingIssuesException ssoucle) {
            	Log.auth.trace("doFilter validate threw " + ssoucle);
            	
                if (!ssoucle.getUrl().endsWith("poll"))
                {
                	try {
						ESAPI.httpUtilities().sendRedirect(
							response,
							AuthConstants.SSO_USER_CREATE_IN_RESOLVE_DUE_TO_LICENSING_ISSUES_JSP_URL_PREFIX.getValue() +
							ssoucle.getExternalSystem() + "&" + Constants.HTTP_REQUEST_SSO_TYPE + 
							"=" + ssoucle.getExternalSystem());
					} catch (AccessControlException | IOException ace) {
						Log.auth.error("Error " + ace.getLocalizedMessage() + " occurred while redirecting to " +
									   AuthConstants.SSO_USER_CREATE_IN_RESOLVE_DUE_TO_LICENSING_ISSUES_JSP_URL_PREFIX
									   .getValue() +ssoucle.getExternalSystem() + "&" + Constants.HTTP_REQUEST_SSO_TYPE + 
									   "=" + ssoucle.getExternalSystem() + ".", ace);
						throw new ServletException("Error " + 
								   					ace.getLocalizedMessage() + " in redirect.", ace);
					}
                    isRedirect = true;
                }
                else
                {
                    // In case of client/poll request continue to Ajax call after setting Redirect URL attribute
                    request.setAttribute(Constants.HTTP_ATTRIBUTE_SSO_REDIRECT_URL, 
                    					 AuthConstants.SSO_USER_CREATE_IN_RESOLVE_DUE_TO_LICENSING_ISSUES_JSP_URL_PREFIX
                    					 .getValue() + ssoucle.getExternalSystem() + "&" + Constants.HTTP_REQUEST_SSO_TYPE + 
                                         "=" + ssoucle.getExternalSystem());
                }
			}
        }
        else
        {
            for (String header : xHeaders.keySet())
            {
                String[] headerVals = xHeaders.get(header);
                
                for (int i = 0; i < headerVals.length; i++)
                {
                    response.addHeader(header, headerVals[i]);
                }
            }
        }

        // check and set caching
        Date now = new Date();
        if (decodedURL.contains(".nocache."))
        {
            response.setDateHeader("Date", now.getTime());
            response.setDateHeader("Expires", -1);
            response.setHeader("Pragma", "no-cache");
            response.setHeader("Cache-control", "no-cache, no-store, must-revalidate");
        }
        else if (decodedURL.contains(".cache."))
        {
            response.setDateHeader("Date", now.getTime());
            response.setDateHeader("Expires", now.getTime() + 2592000000L); // 30days
        }

        // do remaining filter chains
        if (!isRedirect)
        {
            if (!internal && spnegoResponse != null)
            {
                // context/auth loop not yet complete
                if (spnegoResponse.isStatusSet())
                {
                    return;
                }

                // assert
                if (principal == null)
                {
                    try
                    {
                        Log.log.warn("Principal was null.");
                        Log.auth.warn("Principal was null.");
                        spnegoResponse.setStatus(HttpServletResponse.SC_INTERNAL_SERVER_ERROR, true);
                        return;
                    }
                    catch (IOException e)
                    {
                        Log.log.warn(e.getMessage(), e);
                        Log.auth.warn(e.getMessage(), e);
                    }
                }

                chain.doFilter(new SpnegoHttpServletRequest(request, principal), spnegoResponse);
            }
            else
            {
                chain.doFilter(request, response);
            }
        }
    } // doFilter

    static class ServiceNowAuthResult
    {
        public int authCode;
        public long sessionTimeout = 30 * 60 * 1000;
        public String snAuthenticSessionId = null;

        public ServiceNowAuthResult(int authCode, long sessionTimeout, String snAuthenticSessionId)
        {
            this.authCode = authCode;
            this.sessionTimeout = sessionTimeout;
            this.snAuthenticSessionId = snAuthenticSessionId;
        }

        public ServiceNowAuthResult(int authCode)
        {
            this.authCode = authCode;
        }
    }
    
    /**
     * Check for valid cookie token for authenticated logins.
     * 
     * @param request
     * @throws RSAuthException
     *             - if session has not been authenticated
     * @throws SSOSessTimedOutException
     *             - if SSO'ed users host session has timed out
     * @throws SSOUserLockedInResolveException
     * @throws SSOUserGetFromOrCreateInResolveException
     * @throws SSOConfigMissingOrIncompleteInResolveException
     * @throws SSOConfiguredHostAccessException
     * @throws SSOUserCreateInResolveDueToLicensingIssuesException 
     */
    public static SpnegoPrincipal validate(HttpServletRequest request, HttpServletResponse response) 
    									throws RSAuthException, RSAccessException, IOException, 
    										   ServletException, SSOSessTimedOutException, 
    										   SSOUserLockedInResolveException, 
    										   SSOUserGetFromOrCreateInResolveException, 
    										   SSOConfigMissingOrIncompleteInResolveException, 
    										   SSOConfiguredHostAccessException, 
    										   SSOUserCreateInResolveDueToLicensingIssuesException
    {
        Map<String, String[]> xHeaders = new HashMap<String, String[]>();
        return validate(request, response, xHeaders);
    }
    
    public static Map<String, String> getSiteMinderProperties()
    {
        Map<String, String> siteMinderPropsToVal = new HashMap<String, String>();
        
        List<PropertiesVO> siteMinderPropVOs = PropertiesUtil.getPropertiesLikeName(SITEMINDER_PROPERTY_PREFIX);
        
        for (PropertiesVO siteMinderPropVO : siteMinderPropVOs)
        {
            if ((siteMinderPropVO.getUName().equals(SITEMINDER_PROPERTY_PREFIX + SITEMINDER_USERID_PROPERTY_SUFFIX) &&
                 StringUtils.isNotBlank(siteMinderPropVO.getUValue())) ||
                (siteMinderPropVO.getUName().equals(SITEMINDER_PROPERTY_PREFIX + SITEMINDER_GROUP_PROPERTY_SUFFIX) &&
                 StringUtils.isNotBlank(siteMinderPropVO.getUValue())))
            {
                siteMinderPropsToVal.put(siteMinderPropVO.getUName(), siteMinderPropVO.getUValue());
            }
        }
        
        return siteMinderPropsToVal;
    }
    
    public static boolean requestHasSiteMinderHeaders(HttpServletRequest request)
    {
//        MultiReadHttpServletRequest multiReadReq = (MultiReadHttpServletRequest)request;
        
        boolean retval = false;
        
        Map<String, String> siteMinderPropsToVal = getSiteMinderProperties();
        
        if (siteMinderPropsToVal != null && !siteMinderPropsToVal.isEmpty() &&
            siteMinderPropsToVal.containsKey(SITEMINDER_PROPERTY_PREFIX + SITEMINDER_USERID_PROPERTY_SUFFIX) &&
            ((PropertiesUtil.getPropertyBoolean(SITEMINDER_PROPERTY_PREFIX + SITEMINDER_GROUP_REQUIRED_PROPERTY_SUFFIX) &&
              siteMinderPropsToVal.containsKey(SITEMINDER_PROPERTY_PREFIX + SITEMINDER_GROUP_PROPERTY_SUFFIX)) ||
             !PropertiesUtil.getPropertyBoolean(SITEMINDER_PROPERTY_PREFIX + SITEMINDER_GROUP_REQUIRED_PROPERTY_SUFFIX)))
        {
            String siteMinderUserIdProp = PropertiesUtil.getPropertyBoolean(SITEMINDER_PROPERTY_PREFIX + SITEMINDER_MOCK_PROPERTY_SUFFIX) ?
                                          AuthConstants.RESOLVE_PARAM_AS_HEADER_HEADER_NAME_PREFIX.getValue() + 
                                          siteMinderPropsToVal.get(SITEMINDER_PROPERTY_PREFIX + SITEMINDER_USERID_PROPERTY_SUFFIX) :
                                          siteMinderPropsToVal.get(SITEMINDER_PROPERTY_PREFIX + SITEMINDER_USERID_PROPERTY_SUFFIX);
            
            String siteMinderGroupsProp = PropertiesUtil.getPropertyBoolean(SITEMINDER_PROPERTY_PREFIX + SITEMINDER_GROUP_REQUIRED_PROPERTY_SUFFIX) ?
                                          (PropertiesUtil.getPropertyBoolean(SITEMINDER_PROPERTY_PREFIX + SITEMINDER_MOCK_PROPERTY_SUFFIX) ?
                                           AuthConstants.RESOLVE_PARAM_AS_HEADER_HEADER_NAME_PREFIX.getValue() + 
                                           siteMinderPropsToVal.get(SITEMINDER_PROPERTY_PREFIX + SITEMINDER_GROUP_PROPERTY_SUFFIX) :
                                           siteMinderPropsToVal.get(SITEMINDER_PROPERTY_PREFIX + SITEMINDER_GROUP_PROPERTY_SUFFIX)) : 
                                          null;
            
            if (StringUtils.isNotBlank(request.getHeader(siteMinderUserIdProp)) &&
                ((StringUtils.isNotBlank(siteMinderGroupsProp) && StringUtils.isNotBlank(request.getHeader(siteMinderGroupsProp))) ||
                 StringUtils.isBlank(siteMinderGroupsProp)))
            {
                retval = true;
            }
            
            if (!retval && getMockSSOSrvcDataCache().containsKey(request.getSession().getId()) &&
                PropertiesUtil.getPropertyBoolean(SITEMINDER_PROPERTY_PREFIX + SITEMINDER_MOCK_PROPERTY_SUFFIX) &&
                (getMockSSOSrvcDataCache().get(request.getSession().getId()).get(siteMinderUserIdProp).length >= 1 &&
                 ((StringUtils.isNotBlank(siteMinderGroupsProp) &&
                   StringUtils.isNotBlank(getMockSSOSrvcDataCache().get(request.getSession().getId()).get(siteMinderGroupsProp)[0])) || 
                  StringUtils.isBlank(siteMinderGroupsProp))))
            {
                retval = true;
            }
        }
        
        return retval;
    }
    
    /**
     * Check for valid cookie token for authenticated logins.
     * 
     * @param request
     * @throws RSAuthException
     *             - if session has not been authenticated
     * @throws SSOSessTimedOutException
     *             - if SSO'ed users host session has timed out
     * @throws SSOUserLockedInResolveException
     * @throws SSOUserGetFromOrCreateInResolveException
     * @throws SSOConfigMissingOrIncompleteInResolveException
     * @throws SSOConfiguredHostAccessException
     * @throws SSOUserCreateInResolveDueToLicensingIssuesException 
     */
    public static SpnegoPrincipal validate(
    									HttpServletRequest request, 
    									HttpServletResponse response, 
    									Map<String, String[]> xHeaders) 
    											throws RSAuthException, 
    												   RSAccessException, 
    												   IOException, 
    												   ServletException, 
    												   SSOSessTimedOutException, 
    												   SSOUserLockedInResolveException, 
    												   SSOUserGetFromOrCreateInResolveException, 
    												   SSOConfigMissingOrIncompleteInResolveException, 
    												   SSOConfiguredHostAccessException, 
    												   SSOUserCreateInResolveDueToLicensingIssuesException
    {
        SpnegoPrincipal result = null;
        String username = null;
        String pollUserName = null;
        String password = null;
        boolean valid = false;
        boolean isServiceNowButSSODisabled = false;
        String iFrameId = null;

        String url = getURL(request);

        Log.auth.trace("validate URL: " + sanitizeUrl(url));

        try
        {
            if (Log.auth.isTraceEnabled())
            {
                Log.auth.trace("Request Content Type : " + request.getContentType());
    
                if (StringUtils.isNotBlank(request.getContentType()) && request.getContentType().toLowerCase().contains("application/json"))
                {
                    Log.auth.trace("Request Method : " + request.getMethod());
    
                    if (request.getMethod().equalsIgnoreCase("POST"))
                    {
                        Log.auth.trace("Request Content Length : " + request.getContentLength());
    
                        if (request.getContentLength() > 0)
                        {
                            Log.auth.trace("Payload : " + StringUtils.toString(request.getInputStream(), StandardCharsets.UTF_8.name()));
                        }
                    }
                }
            }
    
            String ssoType = request.getParameter(Constants.HTTP_REQUEST_SSO_TYPE);
            
            if (StringUtils.isBlank(ssoType))
            {
                ssoType = request.getParameter(Constants.HTTP_REQUEST_SSO_TYPE.toLowerCase());
            }
    
            Log.auth.trace("ssoType from request parameter: " + ssoType);
    
            Log.auth.trace("ssoType=" + (StringUtils.isNotBlank(ssoType) ? ssoType : "null") + 
                           ", Source userName = " + 
                           (StringUtils.isNotBlank(pollUserName) ? pollUserName : "null") + 
                           ", request " + Constants.HTTP_REQUEST_USERNAME + " attribute:" + 
                           (StringUtils.isNotBlank((String) request.getAttribute(Constants.HTTP_REQUEST_USERNAME)) ? 
                            (String) request.getAttribute(Constants.HTTP_REQUEST_USERNAME) : "null"));
            
            // Check if ServiceNow SSO is disabled
            if ("servicenow".equalsIgnoreCase(ssoType) && PropertiesUtil.getPropertyBoolean("servicenow.sso.disable"))
            {
                Log.auth.trace("Resetting ssoType [" + ssoType + "] from request parameter as servicenow.sso.disable property is set to true.");
                
                if (StringUtils.isBlank(iFrameId))
                {
                    iFrameId = ssoType;
                }
                
                ssoType = null;
                isServiceNowButSSODisabled = true;
            }
                        
            Log.auth.trace("ssoType=" + (StringUtils.isNotBlank(ssoType) ? ssoType : "null") + ", Source userName = " + 
                           (StringUtils.isNotBlank(pollUserName) ? pollUserName : "null") + 
                           ", request " + Constants.HTTP_REQUEST_USERNAME + " attribute:" + 
                           (StringUtils.isNotBlank((String)request.getAttribute(Constants.HTTP_REQUEST_USERNAME)) ? 
                           (String)request.getAttribute(Constants.HTTP_REQUEST_USERNAME) : "null"));
            
            if (StringUtils.isBlank(ssoType) && StringUtils.isBlank(iFrameId))
            {
                iFrameId = request.getParameter(Constants.HTTP_REQUEST_IFRAMEID);
                
                if (StringUtils.isBlank(iFrameId))
                {
                    iFrameId = request.getParameter(Constants.HTTP_REQUEST_IFRAMEID.toLowerCase());
                }
                
                Log.auth.trace(StringUtils.isNotBlank(iFrameId) ? Constants.HTTP_REQUEST_IFRAMEID + " in request is set to " + iFrameId : "");
            }
            
            // get token
            String token = StringUtils.isNotBlank(request.getParameter(Constants.HTTP_REQUEST_INTERNAL_TOKEN)) ? 
                            CryptUtils.decryptToken(request.getParameter(Constants.HTTP_REQUEST_INTERNAL_TOKEN)) : 
                           request.getParameter(Constants.HTTP_REQUEST_INTERNAL_TOKEN);
            Log.auth.trace("Token from the Request Parameter (" + Constants.HTTP_REQUEST_INTERNAL_TOKEN + "): " + token);
    
            Collection<String> tokens = null;
    
            if (token == null || token.trim().length() == 0)
            {
                token = request.getAttribute(Constants.HTTP_REQUEST_TOKEN) != null ? CryptUtils.decryptToken(request.getAttribute(Constants.HTTP_REQUEST_TOKEN).toString()) : null;
                Log.auth.trace("Token from Request Attribute (" + Constants.HTTP_REQUEST_TOKEN + "): " + token);
                if (token == null || token.trim().length() == 0)
                {
                    String iframeIntegrationId = ssoType;
                    
                    if (StringUtils.isBlank(iframeIntegrationId) && StringUtils.isNotBlank(iFrameId))
                    {
                        iframeIntegrationId = iFrameId;
                    }
                    
                    // Check for Session id in iframe pre-login cache
                    if (StringUtils.isBlank(iframeIntegrationId) && 
                        getIFramePreLoginSessionCache().containsKey(request.getSession().getId()))
                    {
                        iframeIntegrationId = getIFramePreLoginSessionCache().get(request.getSession().getId()).getIFrameId();
                        iFrameId = iframeIntegrationId;
                    }
                                    
                    Collection<Cookie> authTokenCookies = getAuthTokenCookies(request, iframeIntegrationId, response);
    
                    if (authTokenCookies != null && !authTokenCookies.isEmpty())
                    {
                        tokens = new ArrayList<String>(authTokenCookies.size());
    
                        for (Cookie authTokenCookie : authTokenCookies)
                        {
                            token = authTokenCookie.getValue();
                            tokens.add(CryptUtils.decryptToken(token));
                            Log.auth.trace("Token from Cookie (" + authTokenCookie.getName() + "): " + token);
                        }
                    }
                }
                else
                {
                    tokens = new ArrayList<String>(1);
                    tokens.add(token);
                }
            }
            else
            {
                tokens = new ArrayList<String>(1);
                tokens.add(token);
            }
    
            // check tokens
            if (tokens != null && !tokens.isEmpty())
            {
                Log.auth.trace("Validating " + tokens.size() + " tokens: " + tokens);
    
                ValidateTokenResult valTokenResult = validateToken(tokens, request, response);
    
                Log.auth.trace(valTokenResult);
    
                boolean setSSOType = false;
                boolean setIFrameId = false;
                
                if (StringUtils.isBlank(ssoType))
                {
                    ssoType = valTokenResult.getSsoType();
                    
                    if (StringUtils.isNotBlank(ssoType))
                    {
                        setSSOType = true;
                    }
                }
    
                if (StringUtils.isBlank(pollUserName))
                {
                    pollUserName = valTokenResult.getUser();
                }
                
                if (StringUtils.isBlank(iFrameId))
                {
                    iFrameId = valTokenResult.getiFrameId();
                    
                    if (StringUtils.isNotBlank(iFrameId))
                    {
                        setIFrameId = true;
                    }
                }
                
                if ((setSSOType || setIFrameId) /*&& request.getMethod().equalsIgnoreCase("GET")*/)
                {
                    if (setSSOType)
                    {
                        ((MultiReadHttpServletRequest)request).setQueryStringSuffix(Constants.HTTP_REQUEST_SSO_TYPE + "=" + ssoType);
                        Log.auth.trace("Added SSOTYPE=" + ssoType + " to URL query string. New URL is " + sanitizeUrl(getURL(request)));
                    }
                    else
                    {
                        ((MultiReadHttpServletRequest)request).setQueryStringSuffix(Constants.HTTP_REQUEST_IFRAMEID + "=" + iFrameId);
                        Log.auth.trace("Added IFRAMEID=" + iFrameId + " to URL query string. New URL is " + sanitizeUrl(getURL(request)));
                    }
                }
                
                // Determine the security headers based on the SSO/iFrameId in token.
                
                String cspHeader = request.isSecure() ? 
                				   AuthConstants.CONTENT_SECURITY_POLICY_UPGRADE_INSECURE_REQUESTS.getValue() : null;
                
                if (StringUtils.isNotBlank(ssoType) || StringUtils.isNotBlank(iFrameId))
                {
                    String iframeid = StringUtils.isNotBlank(ssoType) ? ssoType : iFrameId;
                    
                    Map<String, String[]> securityHeaderSuffixes = getSecurityHeaderSuffixes(iframeid, request);
                    
                    Log.auth.trace("validate securityHeaderSuffixes: " + StringUtils.mapToString(securityHeaderSuffixes, "=", ", "));
                    
                    if (securityHeaderSuffixes != null && 
                    	securityHeaderSuffixes.containsKey(AuthConstants.CONTENT_SECURITY_POLICY_HEADER.getValue()))
                    {
                        cspHeader = StringUtils.isNotBlank(cspHeader) ? 
                        			cspHeader + "; " + 
                        			securityHeaderSuffixes.get(
                        					AuthConstants.CONTENT_SECURITY_POLICY_HEADER.getValue())[0] :
                                    securityHeaderSuffixes.get(AuthConstants.CONTENT_SECURITY_POLICY_HEADER.getValue())[0];
                        //xHeaders.put(CONTENT_SECURITY_POLICY_HEADER, securityHeaderSuffixes.get(CONTENT_SECURITY_POLICY_HEADER));
                    }
                    
                    Log.auth.trace("validate cspHeader=" + cspHeader);
                    
                    if (securityHeaderSuffixes != null && 
                    	securityHeaderSuffixes.containsKey(AuthConstants.X_FRAME_OPTIONS_HEADER.getValue()))
                    {
                        xHeaders.put(AuthConstants.X_FRAME_OPTIONS_HEADER.getValue(), 
                        			 securityHeaderSuffixes.get(AuthConstants.X_FRAME_OPTIONS_HEADER.getValue()));
                    }
                }
                
                if (StringUtils.isNotBlank(cspHeader))
                {
                    xHeaders.put(AuthConstants.CONTENT_SECURITY_POLICY_HEADER.getValue(), new String[] {cspHeader});
                }
                
                Log.auth.trace("validate " + AuthConstants.CONTENT_SECURITY_POLICY_HEADER.getValue() + "=" + 
                			   xHeaders.get(AuthConstants.CONTENT_SECURITY_POLICY_HEADER.getValue()) + ", " +
                               AuthConstants.X_FRAME_OPTIONS_HEADER.getValue() + "=" + 
                			   xHeaders.get(AuthConstants.X_FRAME_OPTIONS_HEADER.getValue()));
                
                token = valTokenResult.getValidToken();
    
                valid = valTokenResult.isValid();
    
                username = (String) request.getAttribute(Constants.HTTP_REQUEST_USERNAME);
    
                /*
                 * In scenario where requested URL is from ServiceNow-Resolve iframe integration,
                 * request has USERNAME parameter, and ServiceNow SSO is disabled then disable check 
                 * whether user name from token is same as the USERNAME in request parameter. 
                 */
                String usernameParam = (String) request.getParameter(Constants.HTTP_REQUEST_USERNAME);
    
                if (StringUtils.isNotBlank(usernameParam) && StringUtils.isNotBlank(username) && 
                    isServiceNowButSSODisabled)
                {
                    /*
                     * If SSO type is servicenow and SSO is disabled then skip the user name change check
                     */
                    
                    boolean skip = false;
                    
                    if ("servicenow".equalsIgnoreCase(ssoType) && PropertiesUtil.getPropertyBoolean("servicenow.sso.disable"))
                    {
                        skip = true;
                    }
                    
                    if (!skip)
                    {
                        // if the usernames are not same and ssoType is not blank, it's an invalid token
                        if (!usernameParam.equalsIgnoreCase(username))
                        {
                            if (StringUtils.isNotBlank(ssoType))
                            {
                                valid = false;
                            }
                        }
        
                        if (!valid)
                        {
                            Log.log.debug("Token user " + username + " and incoming user " + usernameParam + " are different, invalidating token.");
                            Log.auth.debug("Token user " + username + " and incoming user " + usernameParam + " are different, invalidating token.");
                            invalidate(request, response, iFrameId);
                        }
                    }
                }
    
                Log.auth.trace("Is this token valid :" + valid);
    
                if (!valid)
                {
                    token = null;
                    
                    if ("servicenow".equalsIgnoreCase(ssoType) && PropertiesUtil.getPropertyBoolean("servicenow.sso.disable"))
                    {
                        iFrameId = ssoType;
                        ssoType = null;
                    }
                    
                    if (StringUtils.isNotBlank(iFrameId))
                    {
                        cacheIFramePreLoginSession(
                            new IFramePreLoginSession(request.getSession().getId(), iFrameId,
                                                      getProtocolAndAuthority(request.getHeader(HttpHeaders.REFERER))));
                    }
                }
                else
                {
                    if (RSContext.getMain().getConfigWindows().isActive())
                    {
                        result = new SpnegoPrincipal(username, KerberosPrincipal.KRB_NT_PRINCIPAL, null);
                    }
    
                    // check permissions
                    valid = validateApp(username, token, request, response);
                    if (!valid)
                    {
                        String syslogMessage = "Token user does not have access to requested URI " + request.getRequestURI();
                        Log.syslog.warn(SyslogFormatter.INSTANCE.formatAudit(getRemoteAddrForSysLog(request), username, request.getLocalAddr(), syslogMessage));
    
                        throw new RSAccessException();
                    }
                }
            }
            
            // authenticate only if does not have a single token
            if (StringUtils.isEmpty(token))
            {
                Log.auth.trace("Token is empty/null here.");
                boolean internal = false;
    
                long DEFAULT_SESSION_TIMEOUT = (PropertiesUtil.getPropertyLong(ConstantValues.PROPERTIES_SESSION_TIMEOUT) < 720) ? 720000 : PropertiesUtil.getPropertyLong(ConstantValues.PROPERTIES_SESSION_TIMEOUT) * 1000;
    
                // check internal username / password
                if (StringUtils.isNotBlank(request.getParameter(Constants.HTTP_REQUEST_INTERNAL_USERNAME)))
                {
                    internal = true;
                }
    
                // windows authentication
                if (!internal && RSContext.getMain().getConfigWindows().isActive())
                {
                    ssoType = null;
                    Log.auth.trace("Into the Windows Authentication");
                    final SpnegoHttpServletResponse spnegoResponse = (SpnegoHttpServletResponse) response;
                    SpnegoPrincipal principal = checkWindowsPassword(request, spnegoResponse);
                    Log.auth.trace(String.format("Checking the Windows password and got value of the principal: %s", 
                    							 principal));
    
                    if (principal == null)
                    {
                        Log.syslog.warn(SyslogFormatter.INSTANCE.formatAudit(getRemoteAddrForSysLog(request), username, request.getLocalAddr(), "Failed to authenticate Windows user"));
                        throw new RSAuthException(getURL(request));
                    }
    
                    // authenticate
                    username = principal.getName();
    
                    // strip @domain from username if using AD defaultDomain
                    if ((RSContext.getMain().getActiveDirectory() != null) && (StringUtils.isNotBlank(RSContext.getMain().getActiveDirectory().getDefaultDomain())))
                    {
                        username = username.substring(0, username.indexOf('@'));
                    }
    
                    // validate against AD
                    Log.auth.trace(String.format("Calling the auth search api with the username:%s",username));
                    AuthenticateResult windowsAuthResult = Authentication.search(username);
                    valid = windowsAuthResult.valid;
                    Log.auth.trace(String.format("Is this Windows user %s valid? %b", username, valid));
    
                    if (!valid)
                    {
                        Log.syslog.warn(
                        		SyslogFormatter.INSTANCE.formatAudit(getRemoteAddrForSysLog(request), username, 
                        											 request.getLocalAddr(), 
                        											 (windowsAuthResult.errCode != null ? 
                        											  windowsAuthResult.errCode.getMessage() :
                        											  String.format(
                        													 "Failed to authenticate Windows user %s in AD", 
                        													 username))));
                        
                        if (windowsAuthResult.errCode != null) {
                        	throw new RSAuthException(windowsAuthResult.errCode.getMessage());
                        } else {
                        	throw new RSAuthException(getURL(request));
                        }
                    }
                    else
                    {
                        username = username.toLowerCase();
                    }
    
                    // check permissions
                    valid = validateApp(username, token, request, response);
                    Log.auth.trace("Validating the app for this user : " + valid);
                    if (!valid)
                    {
                        Log.syslog.warn(SyslogFormatter.INSTANCE.formatAudit(getRemoteAddrForSysLog(request), username, request.getLocalAddr(), "Windows SSO user does not have access to requested URI " + request.getRequestURI()));
                        throw new RSAccessException();
                    }
                    else
                    {
                        String syslogMessage = AUTHENTICATION_SYSLOG_RSVIEW_LOGIN_SUCCESS_MESAGE_PREFIX + "Windows SSO" + AUTHENTICATION_SYSLOG_RSVIEW_LOGIN_MESSAGE_SUFFIX;
                        Log.syslog.warn(SyslogFormatter.INSTANCE.formatAudit(getRemoteAddrForSysLog(request), username, request.getLocalAddr(), syslogMessage));
                    }
    
                    // set token
                    token = setAuthToken(request, response, username, request.getRemoteAddr(), null, DEFAULT_SESSION_TIMEOUT, null, iFrameId);
                    Log.auth.trace("Setting the " + Constants.HTTP_REQUEST_TOKEN + " in the request to : " + token);
                    request.setAttribute(Constants.HTTP_REQUEST_USERNAME, username);
                    request.setAttribute(Constants.HTTP_REQUEST_TOKEN, CryptUtils.encryptToken(token));
    
                    result = principal;
                }
                // ServiceNow SSO
                else if ("servicenow".equalsIgnoreCase(ssoType))
                {
                    Log.auth.trace("Into the ServiceNow authentication");
                    
                    if (PropertiesUtil.getPropertyBoolean("servicenow.sso.disable"))
                    {
                        throw new RSAuthException(getURL(request));
                    }
                    
                    username = request.getParameter(Constants.HTTP_REQUEST_USERNAME);
    
                    if (StringUtils.isBlank(username))
                    {
                        username = pollUserName;
                    }
    
                    String sessionId = request.getParameter(Constants.HTTP_REQUEST_SESSION_ID);
    
                    // check username defined
                    if (StringUtils.isEmpty(username))
                    {
                        throw new RSAuthException(getURL(request));
                    }
    
                    /*
                     * authenticate
                     * 0 - valid
                     * 1 - valid user but ServiceNow Session Timed Out
                     * -1 - user locked in Resolve
                     * -2 - Error getting/creating user in resolve
                     * -3 - Missing or incomplete ServiceNow SSO configuration
                     * -4 - Error in accessing ServiceNow server using configured values
                     */
    
                    ServiceNowAuthResult snAuthResult = Authentication.authenticateServiceNow(username, sessionId);
    
                    int authCode = snAuthResult.authCode;
    
                    valid = authCode >= 0 ? true : false;
    
                    Log.auth.trace("Is this user valid : " + valid + ", authCode = " + authCode);
    
                    if (!valid)
                    {
                        if (authCode == -1)
                        {
                            Log.syslog.warn(SyslogFormatter.INSTANCE.formatAudit(getRemoteAddrForSysLog(request), username, request.getLocalAddr(), "ServiceNow SSO user locked in Resolve"));
    
                            throw new SSOUserLockedInResolveException("ServiceNow", url);
                        }
                        else if (authCode == -2)
                        {
                            Log.syslog.warn(SyslogFormatter.INSTANCE.formatAudit(getRemoteAddrForSysLog(request), username, request.getLocalAddr(), "Failed to get or create ServiceNow SSO user in Resolve"));
    
                            throw new SSOUserGetFromOrCreateInResolveException("ServiceNow", url);
                        }
                        else if (authCode == -3)
                        {
                            Log.syslog.warn(SyslogFormatter.INSTANCE.formatAudit(getRemoteAddrForSysLog(request), username, request.getLocalAddr(), "Missing or incomplete ServiceNow SSO configuration in Resolve"));
    
                            throw new SSOConfigMissingOrIncompleteInResolveException("ServiceNow", url);
                        }
                        else if (authCode == -4)
                        {
                            Log.syslog.warn(SyslogFormatter.INSTANCE.formatAudit(getRemoteAddrForSysLog(request), username, request.getLocalAddr(), "Error accessing configured ServiceNow SSO server"));
                            
                            throw new SSOConfiguredHostAccessException("ServiceNow", url);
                        }
                        else if (authCode == -5)
                        {
                            Log.syslog.warn(SyslogFormatter.INSTANCE.formatAudit(getRemoteAddrForSysLog(request), username, request.getLocalAddr(), "Failed to create ServiceNow SSO user in Resolve due to licensing issues."));
                            
                            throw new SSOUserCreateInResolveDueToLicensingIssuesException("ServiceNow", url);
                        }
                        else
                        {
                            Log.syslog.warn(SyslogFormatter.INSTANCE.formatAudit(getRemoteAddrForSysLog(request), username, request.getLocalAddr(), "Error authenticating ServiceNow SSO user"));
    
                            throw new RSAuthException(url);
                        }
                    }
                    else
                    {
                        if (authCode == 1)
                        {
                            Log.syslog.warn(SyslogFormatter.INSTANCE.formatAudit(getRemoteAddrForSysLog(request), username, request.getLocalAddr(), "ServiceNow SSO users session timed out"));
    
                            throw new SSOSessTimedOutException("ServiceNow", url);
                        }
    
                        username = username.toLowerCase();
                    }
    
                    // check permissions
                    valid = validateApp(username, token, request, response);
                    Log.auth.trace("Does this user has rights for the app : " + valid);
    
                    if (!valid)
                    {
                        Log.syslog.warn(SyslogFormatter.INSTANCE.formatAudit(getRemoteAddrForSysLog(request), username, request.getLocalAddr(), "ServiceNow SSO user does not have access to requested URI " + request.getRequestURI()));
    
                        throw new RSAccessException();
                    }
                    else
                    {
                        String syslogMessage = AUTHENTICATION_SYSLOG_RSVIEW_LOGIN_SUCCESS_MESAGE_PREFIX + "ServiceNow SSO" + AUTHENTICATION_SYSLOG_RSVIEW_LOGIN_MESSAGE_SUFFIX;
                        Log.syslog.warn(SyslogFormatter.INSTANCE.formatAudit(getRemoteAddrForSysLog(request), username, request.getLocalAddr(), syslogMessage));
                        
                        if (StringUtils.isNotBlank(ssoType))
                        {
                            cacheIFramePreLoginSession(
                                new IFramePreLoginSession(request.getSession().getId(), ssoType.toLowerCase(),
                                                          getProtocolAndAuthority(request.getHeader(HttpHeaders.REFERER))));
                        }
                    }
    
                    // set token
                    token = setAuthToken(request, response, username, request.getRemoteAddr(), ssoType, snAuthResult.sessionTimeout, snAuthResult.snAuthenticSessionId, null, ssoType, null, null);
                    Log.auth.trace("Setting the " + Constants.HTTP_REQUEST_TOKEN + " in the request to : " + token);
                    request.setAttribute(Constants.HTTP_REQUEST_USERNAME, username);
                    request.setAttribute(Constants.HTTP_REQUEST_TOKEN, CryptUtils.encryptToken(token));
                }
                else if ("adfs".equalsIgnoreCase(ssoType))
                {
                    Log.auth.trace("Performing ADFS SSO authentication...");
                    // It is already been performed by the new framework - SAML | ADFSAuthenticator
                }
                else if ("splunk".equalsIgnoreCase(ssoType))
                {
                    Log.auth.trace("Performing Splunk SSO authentication...");
                    SSOAuthenticator splunkSSOAuthenticator = new SplunkAuthenticator();
                    SSOAuthenticationDTO splunkAuthentication = splunkSSOAuthenticator.authenticate(request, response);
                    if (splunkAuthentication != null && splunkAuthentication.getErrCode() == null)
                    {
                        Log.log.debug(String.format("Splunk SSO Authentication DTO: %s", splunkAuthentication));
                        Log.auth.debug(String.format("Splunk SSO Authentication DTO: %s", splunkAuthentication));
                        request.setAttribute(Constants.HTTP_ATTRIBUTE_CREATETIME, Long.toString(splunkAuthentication.getCreationTime()));
                        username = splunkAuthentication.getUsername();
                        token = Authentication.setAuthToken(request, response, splunkAuthentication.getUsername(), request.getRemoteAddr(), splunkSSOAuthenticator.getSSOType(), splunkAuthentication.getSessionTimeout(), null, splunkSSOAuthenticator.getSSOType());
                        request.setAttribute(Constants.HTTP_REQUEST_TOKEN, CryptUtils.encryptToken(token));
                        Log.auth.trace("Setting the " + Constants.HTTP_REQUEST_TOKEN + " in the request to : " + token);
                        Log.auth.info(String.format("Splunk SSO succeeded, username: %s, referrer: %s", username, request.getHeader("Referer")));
                        String syslogMessage = AUTHENTICATION_SYSLOG_RSVIEW_LOGIN_SUCCESS_MESAGE_PREFIX + "Splunk SSO" + AUTHENTICATION_SYSLOG_RSVIEW_LOGIN_MESSAGE_SUFFIX;
                        Log.syslog.info(SyslogFormatter.INSTANCE.formatAudit(getRemoteAddrForSysLog(request), username, request.getLocalAddr(), syslogMessage));
                        
                        if (StringUtils.isNotBlank(ssoType))
                        {
                            cacheIFramePreLoginSession(
                                new IFramePreLoginSession(request.getSession().getId(), ssoType.toLowerCase(), 
                                                          getProtocolAndAuthority(request.getHeader(HttpHeaders.REFERER))));
                        }
                    }
                    else
                    {
                    	if (splunkAuthentication != null && splunkAuthentication.getErrCode() != null) {
                    		throw new RSAccessException(splunkAuthentication.getErrCode().getMessage());
                    	} else {
                    		throw new RSAccessException();
                    	}
                    }
                }
                else if (requestHasSiteMinderHeaders(request))// CA SiteMinder SSO Service
                {
                    if (StringUtils.isNotBlank(ssoType) && StringUtils.isBlank(iFrameId))
                    {
                        iFrameId = ssoType;
                    }
                    
                    ssoType = null;
                    Log.auth.trace("SiteMinder SSO Service");
                    
                    Map<String, String> siteMinderPropVals = getSiteMinderProperties();
                    
                    Map<String, String[]> siteMinderHeaders = getSiteMinderHeaders(request);
                    
                    Log.auth.trace("requests " + SITEMINDER_AUTH_TYPE + " HTTP Headers: " + 
                                   StringUtils.mapToString(siteMinderHeaders, "=", ","));
                    
                    String[] groups = new String[0];
                    
                    for (String siteMinderHeaderName : siteMinderHeaders.keySet())
                    {
                        if (siteMinderPropVals.containsKey(SITEMINDER_PROPERTY_PREFIX + SITEMINDER_USERID_PROPERTY_SUFFIX) && 
                            siteMinderHeaderName.endsWith(siteMinderPropVals.get(SITEMINDER_PROPERTY_PREFIX + SITEMINDER_USERID_PROPERTY_SUFFIX)))
                        {
                            username = siteMinderHeaders.get(siteMinderHeaderName)[0];
                        }
                        else if (siteMinderPropVals.containsKey(SITEMINDER_PROPERTY_PREFIX + SITEMINDER_GROUP_PROPERTY_SUFFIX) && 
                                 siteMinderHeaderName.endsWith(siteMinderPropVals.get(SITEMINDER_PROPERTY_PREFIX + SITEMINDER_GROUP_PROPERTY_SUFFIX)))
                        {
                            groups = siteMinderHeaders.get(siteMinderHeaderName);
                        }
                    }
                    
                    Pair<String, String> siteMinderResultPair = Authentication.authenticateSiteMinder(username, groups);
                    
                    if (StringUtils.isBlank(siteMinderResultPair.getLeft()))
                    {
                        String syslogMessage = AUTHENTICATION_SYSLOG_RSVIEW_LOGIN_FAILED_MESAGE_PREFIX + 
                                               (PropertiesUtil.getPropertyBoolean(SITEMINDER_PROPERTY_PREFIX + SITEMINDER_MOCK_PROPERTY_SUFFIX) ?
                                                AUTHENTICATION_SYSLOG_RSVIEW_LOGIN_MOCK_AUTH_TYPE_PREFIX : "") + 
                                               SITEMINDER_AUTH_TYPE + AUTHENTICATION_SYSLOG_RSVIEW_LOGIN_MESSAGE_SUFFIX;
                        Log.syslog.warn(SyslogFormatter.INSTANCE.formatAudit(getRemoteAddrForSysLog(request), username, request.getLocalAddr(), syslogMessage));
    
                        throw new RSAuthException(getURL(request), siteMinderResultPair.getRight());
                    }
                    else
                    {
                        username = siteMinderResultPair.getLeft().toLowerCase();
                    }
                    
                    // check permissions
                    valid = validateApp(username, null, request, response);
                    Log.auth.trace("Does this user has rights for the app : " + valid);
    
                    if (!valid)
                    {
                        String syslogMessage = (PropertiesUtil.getPropertyBoolean(SITEMINDER_PROPERTY_PREFIX + SITEMINDER_MOCK_PROPERTY_SUFFIX) ?
                                                AUTHENTICATION_SYSLOG_RSVIEW_LOGIN_MOCK_AUTH_TYPE_PREFIX : "") +
                                               SITEMINDER_AUTH_TYPE + " user " + username + " does not have access to requested URI " + request.getRequestURI();
                        Log.syslog.warn(SyslogFormatter.INSTANCE.formatAudit(getRemoteAddrForSysLog(request), username, request.getLocalAddr(), syslogMessage));
    
                        throw new RSAccessException();
                    }
                    
                    // Check if user is not locked out
                    
                    boolean isLockedUser = UserUtils.isLockedOut(username);
                    
                    if (isLockedUser)
                    {
                        String syslogMessage = AUTHENTICATION_SYSLOG_RSVIEW_LOGIN_FAILED_MESAGE_PREFIX + 
                                               (PropertiesUtil.getPropertyBoolean(SITEMINDER_PROPERTY_PREFIX + SITEMINDER_MOCK_PROPERTY_SUFFIX) ?
                                                AUTHENTICATION_SYSLOG_RSVIEW_LOGIN_MOCK_AUTH_TYPE_PREFIX : "") + 
                                               SITEMINDER_AUTH_TYPE + ", user " + username + " has been locked out.";
                        Log.syslog.warn(SyslogFormatter.INSTANCE.formatAudit(getRemoteAddrForSysLog(request), username, request.getLocalAddr(), syslogMessage));

                        throw new RSAuthException(getURL(request), "User " + username + " has been locked out.");
                    }
                    
                    
                    String syslogMessage = AUTHENTICATION_SYSLOG_RSVIEW_LOGIN_SUCCESS_MESAGE_PREFIX + 
                                           (PropertiesUtil.getPropertyBoolean(SITEMINDER_PROPERTY_PREFIX + SITEMINDER_MOCK_PROPERTY_SUFFIX) ?
                                            AUTHENTICATION_SYSLOG_RSVIEW_LOGIN_MOCK_AUTH_TYPE_PREFIX : "") +
                                           SITEMINDER_AUTH_TYPE + AUTHENTICATION_SYSLOG_RSVIEW_LOGIN_MESSAGE_SUFFIX;
                    Log.syslog.info(SyslogFormatter.INSTANCE.formatAudit(getRemoteAddrForSysLog(request), username, request.getLocalAddr(), syslogMessage));
    
                    // update user login details
                    ServiceAuthentication.updateAndCheckLogin(username, request.getRemoteAddr());
                    
                    // set token
                    
                    Map<String, String[]> mockSSOSrvcData = null;
                    
                    if (PropertiesUtil.getPropertyBoolean(SITEMINDER_PROPERTY_PREFIX + SITEMINDER_MOCK_PROPERTY_SUFFIX))
                    {
                        mockSSOSrvcData = siteMinderHeaders;
                    }
                    
                    token = setSSOServiceAuthToken(request, response, username, request.getRemoteAddr(), null, DEFAULT_SESSION_TIMEOUT, password, iFrameId, SITEMINDER_AUTH_TYPE, mockSSOSrvcData);
                    Log.auth.trace("Setting the " + Constants.HTTP_REQUEST_TOKEN + " in the request to : " + token);
                    request.setAttribute(Constants.HTTP_REQUEST_USERNAME, username);
                    request.setAttribute(Constants.HTTP_REQUEST_TOKEN, CryptUtils.encryptToken(token));
                    
                    if (StringUtils.isNotBlank(iFrameId))
                    {
                        String ssoReferer = getProtocolAndAuthority(request.getHeader(HttpHeaders.REFERER));
                        
                        cacheIFramePreLoginSession(
                            new IFramePreLoginSession(request.getSession().getId(), iFrameId.toLowerCase(), ssoReferer));
                        
                        if (url.startsWith("%2Fservice%2Flogin") && StringUtils.isNotBlank(iFrameId) && 
                            request.getMethod().equals(HttpGet.METHOD_NAME) &&
                            StringUtils.isBlank(request.getParameter(Constants.HTTP_REQUEST_SSOSERVICEID)) &&
                            requestHasSiteMinderHeaders(request))
                        {
                        	try {
        						ESAPI.httpUtilities().sendRedirect(
        									response,
        									AuthConstants.RESOLVE_RSCLIENT_JSP_URL_QUESTION_PREFIX.getValue() + 
        									HttpHeaders.REFERER + "=" + 
        									ESAPI.validator().getValidInput("SiteMinder Redirect", ssoReferer, "Redirect",
        																	4096, false));
        					} catch (AccessControlException | IOException | ValidationException | IntrusionException ace) {
        						Log.auth.error("Error " + ace.getLocalizedMessage() + " occurred while redirecting to " +
        									   AuthConstants.RESOLVE_RSCLIENT_JSP_URL_QUESTION_PREFIX.getValue() + 
        									   HttpHeaders.REFERER + "=" + ssoReferer + ".", ace);
        						throw new ServletException("Error " + 
        								   					ace.getLocalizedMessage() + " in redirection.", ace);
        					}
                        }
                    }
                }
                else // internal / LDAP / AD / RADIUS authentication
                {
                    if (StringUtils.isNotBlank(ssoType) && StringUtils.isBlank(iFrameId))
                    {
                        iFrameId = ssoType;
                    }
                    
                    ssoType = null;
                    Log.auth.trace("Into the LDAP/AD/RADIUS authentication");
    
                    username = request.getParameter(Constants.HTTP_REQUEST_INTERNAL_USERNAME);
                    password = request.getParameter(Constants.HTTP_REQUEST_INTERNALP_ASSWORD);
    
                    // check username defined
                    if (StringUtils.isEmpty(username))
                    {
                        throw new RSAuthException(getURL(request));
                    }
    
                    // authenticate
                    AuthenticateResult authResult = Authentication.authenticate(username, password);
                    Log.auth.trace("Is this user a valid : " + authResult);
    
                    if (!authResult.valid)
                    {
                        String syslogMessage = AUTHENTICATION_SYSLOG_RSVIEW_LOGIN_FAILED_MESAGE_PREFIX + 
                        					   authResult.authType + AUTHENTICATION_SYSLOG_RSVIEW_LOGIN_MESSAGE_SUFFIX +
                        					   (authResult.errCode != null ? ". " + authResult.errCode.getMessage() : "");
                        Log.syslog.warn(SyslogFormatter.INSTANCE.formatAudit(getRemoteAddrForSysLog(request), username, request.getLocalAddr(), syslogMessage));
    
                        throw new RSAuthException(getURL(request));
                    }
                    else
                    {
                        username = username.toLowerCase();
                        request.setAttribute(Constants.HTTP_REQUEST_USERNAME, username);
                    }
    
                    // check permissions
                    valid = validateApp(username, token, request, response);
                    Log.auth.trace("Does this user has rights for the app : " + valid);
    
                    if (!valid)
                    {
                        String syslogMessage = authResult.authType + " user does not have access to requested URI " + request.getRequestURI();
                        Log.syslog.warn(SyslogFormatter.INSTANCE.formatAudit(getRemoteAddrForSysLog(request), username, request.getLocalAddr(), syslogMessage));
    
                        throw new RSAccessException();
                    }
                    else
                    {
                        String syslogMessage = AUTHENTICATION_SYSLOG_RSVIEW_LOGIN_SUCCESS_MESAGE_PREFIX + authResult.authType + AUTHENTICATION_SYSLOG_RSVIEW_LOGIN_MESSAGE_SUFFIX;
                        Log.syslog.info(SyslogFormatter.INSTANCE.formatAudit(getRemoteAddrForSysLog(request), username, request.getLocalAddr(), syslogMessage));
                    }
    
                    // set token
                    token = setAuthToken(request, response, username, request.getRemoteAddr(), (isServiceNowButSSODisabled ? "servicenow" : null), DEFAULT_SESSION_TIMEOUT, password, iFrameId);
                    Log.auth.trace("Setting the " + Constants.HTTP_REQUEST_TOKEN + " in the request to : " + token);
                    request.setAttribute(Constants.HTTP_REQUEST_USERNAME, username);
                    request.setAttribute(Constants.HTTP_REQUEST_TOKEN, CryptUtils.encryptToken(token));
                    
                    if (StringUtils.isNotBlank(iFrameId))
                    {
                        cacheIFramePreLoginSession(
                            new IFramePreLoginSession(request.getSession().getId(), iFrameId.toLowerCase(), 
                                                      getProtocolAndAuthority(request.getHeader(HttpHeaders.REFERER))));
                    }
                }
            }
    
            // check if auto-logout
            String logout = request.getParameter(Constants.HTTP_REQUEST_INTERNAL_LOGOUT);
            if (StringUtils.isNotEmpty(logout))
            {
                invalidate(request, response, token, ssoType);
            }
            else
            {
                if (StringUtils.isNotBlank(token))
                {
                    if (request.getAttribute(Constants.HTTP_REQUEST_TOKEN) == null)
                    {
                        request.setAttribute(Constants.HTTP_REQUEST_TOKEN, CryptUtils.encryptToken(token));
                    }
    
                    if (request.getAttribute(Constants.HTTP_ATTRIBUTE_RSVIEW_ID_GUID) == null)
                    {
                        request.setAttribute(Constants.HTTP_ATTRIBUTE_RSVIEW_ID_GUID, RSContext.main.configId.guid);
                    }
                }
            }
        }
        finally
        {
            if (xHeaders != null && !xHeaders.isEmpty())
            {
                for (String header : xHeaders.keySet())
                {
                    String[] headerVals = xHeaders.get(header);
                    
                    for (int i = 0; i < headerVals.length; i++)
                    {
                    	String safeHeaderName = HttpUtil.getSafeHeaderName(header);
                    	String safeHeaderValue = HttpUtil.getSafeHeaderValue(headerVals[i]);
                    	
                    	if (StringUtils.isNotBlank(safeHeaderName) && StringUtils.isNotBlank(safeHeaderValue)) {
                    		ESAPI.httpUtilities().addHeader(response, safeHeaderName, safeHeaderValue);
                    	}
                    }
                }
            }
        }
        
        return result;
    } // validate

    public static ValidateTokenResult validateToken(Collection<String> tokens, HttpServletRequest request, HttpServletResponse response)
    {
        boolean valid = false;
        String validToken = null;
        String user = null;
        String ssoType = null;
        String iFrameId = null;
        
		String reqURLAuthority = getAuthority(request.getRequestURL().toString());
		String hostName = getHost(request.getRequestURL().toString());

        // logging
        Log.auth.debug("Validating url: " + request.getQueryString() + " with Request URL Authority [" + reqURLAuthority + "] from [" + hostName + "]");

        if (tokens != null && !tokens.isEmpty())
        {
            for (String token : tokens)
            {
                String url = getURL(request);
                Log.auth.trace("Validating token: " + token);
                AuthSession authSession = ((AuthTokenMap)getAuthTokenCache()).getForSessionId(request.getSession().getId(), token);
                if (authSession != null && (StringUtils.isBlank(hostName) || authSession.getClientReqURLHostName().equals(hostName)))
                {
                	if(Log.auth.isTraceEnabled())
                		Log.auth.trace("Found AuthSession: " + authSession + " for token: " + token);
                    
                    // check if the url is rsclient, remove public user token
                    //String url = getURL(request);

                    // Ignore cookie with public user if cookie with non-public user has been identified previously in loop
                    String urlPlusQueryString = ((MultiReadHttpServletRequest)request).getURL();
                    if (StringUtils.isBlank(validToken) && urlPlusQueryString != null && urlPlusQueryString.contains("%2Frsclient") && authSession.getUsername().equals("public"))
                    {
                        validToken = null;
                    }
                    else
                    {
                    	String authToken = authSession.getToken();
                        long tokenCreateTime = Long.parseLong(authToken.split("/")[1]);
                        /*final*/ long SESSION_TIMEOUT = authSession.getSessionTimeout();
                        
                        if (StringUtils.isBlank(authSession.getSSOType()))
                        {
                            SESSION_TIMEOUT = (PropertiesUtil.getPropertyBoolean("session.fixed.timeout") ? 
                                               PropertiesUtil.getPropertyLong(Constants.SESSION_MAXALLOWED) :
                                               PropertiesUtil.getPropertyLong(Constants.SESSION_TIMEOUT)) * 1000;
                            
                            if (SESSION_TIMEOUT < 720000)
                            {
                                if (SESSION_TIMEOUT <= 0)
                                {
                                    if (PropertiesUtil.getPropertyBoolean("session.fixed.timeout"))
                                    {
                                        if (PropertiesUtil.getPropertyLong(Constants.SESSION_TIMEOUT) >
                                            PropertiesUtil.getPropertyLong(Constants.SESSION_MAXALLOWED))
                                        {
                                            SESSION_TIMEOUT = PropertiesUtil.getPropertyLong(Constants.SESSION_TIMEOUT) * 1000;
                                        }
                                    }
                                }
                                
                                if (SESSION_TIMEOUT < 720000)
                                {
                                    SESSION_TIMEOUT = 720000;
                                }
                            }
                        }
                        
                        if (SESSION_TIMEOUT > ((Integer.MAX_VALUE - 1) / 2))
                        {
                            SESSION_TIMEOUT = (Integer.MAX_VALUE - 1) / 2;
                        }
                        
                        Log.auth.trace("SESSION_TIMEOUT: " + SESSION_TIMEOUT + " msec");
                        
                        // check if token has expired
                        long currTime = System.currentTimeMillis();
                        long duration = currTime - authSession.getUpdateTime().longValue();
                        Log.auth.trace("Time elapsed since AuthSession was last updated: " + duration + " msec");
                        
                        final long AUTH_TOKEN_REFRESHUPDATETIME = 5 * 60 * 1000; // 5 min updates has to be more than session refresh window
                        
                        if (duration < SESSION_TIMEOUT)
                        {
                            Log.auth.trace("Time elapsed since AuthSession was last updated: " + duration + " msec < Session Timeout of " + SESSION_TIMEOUT + " msec");
                            
                            // Note AUTH_TOKEN_REFRESHWINDOW is guaranteed to be more than AUTH_TOKEN_REFRESHUPDATETIME by at least 1 minute
                            
                            boolean updatedTokenTimeout = false;
                            
                            if (duration > AUTH_TOKEN_REFRESHUPDATETIME)
                            {
                                Log.auth.trace("Time elapsed since AuthSession was last updated: " + duration + " msec > AuthSession Token Refresh Update Time of " + AUTH_TOKEN_REFRESHUPDATETIME + " msec for URL [" + url + "]");
                                
                                if (!(url.contains("%2Fclient%2Fpoll") || (StringUtils.isNotBlank(request.getHeader(RESOLVE_AUTO_REFRESH_HEADER)))))
                                {
                                    // update token lookup
                                    long authSessUpdTimeB4Upd = authSession.getUpdateTime().longValue();
                                    updateTokenTimeout(token, authSession, request);
                                    
                                    updatedTokenTimeout = authSession.getUpdateTime().longValue() > authSessUpdTimeB4Upd ? true : false;
                                    
                                    if (updatedTokenTimeout && Log.auth.isInfoEnabled())
                                    {
                                        Log.auth.info("Updated authSession: " + authSession + 
                                        			  " updateTime for non */client/poll/auto-refresh URL " + url);
                                    }
                                }
                                else
                                {
                                	if (url.contains("%2Fclient%2Fpoll"))
                                	{
                                		if(Log.auth.isTraceEnabled())
                                			Log.auth.trace("AuthSession: " + authSession + " updateTime was not updated as URL is " + url);
                                	}
                                	else if (StringUtils.isNotBlank(request.getHeader(RESOLVE_AUTO_REFRESH_HEADER)))
                                	{
                                		if(Log.auth.isTraceEnabled())
                                			Log.auth.trace("AuthSession: " + authSession + 
                                					   " updateTime was not updated as request to URL " + url +
                                					   " is the result of UI auto-refresh.");
                                	}
                                }
                            }

                            // update token if it is older than one half of session timeout
                            // NOTE: assumes cache was initialized from above by getAuthTokenCache()
                            
                            if (updatedTokenTimeout)
                            {
                                Log.auth.info("Updating cookie as AuthSession update time was updated. Time elapsed since cookie was " + 
                                              (PropertiesUtil.getPropertyBoolean("session.fixed.timeout") ? "created" : "last updated") + 
                                              ": " + (currTime - (PropertiesUtil.getPropertyBoolean("session.fixed.timeout") ? 
                                                                  tokenCreateTime : Long.parseLong(token.split("/")[1]))) + 
                                              "msec");

                                boolean sessionStillValid = true;

                                if (StringUtils.isNotBlank(authSession.getSSOType()))
                                {
                                    if ((currTime - authSession.getLastSSOValidationTime()) >= AUTH_TOKEN_REFRESHUPDATETIME)
                                    {
                                    	if(Log.auth.isTraceEnabled())
                                    		Log.auth.trace("Time elapsed since SSO Session was last validated: " + (currTime - authSession.getLastSSOValidationTime()) + " msec > " + AUTH_TOKEN_REFRESHUPDATETIME + " msec, validating " + authSession.getSSOType() + " session id " + authSession.getSSOSessionId());

                                        authSession.setLastSSOValidationTime(currTime);

                                        switch (authSession.getSSOType().toLowerCase())
                                        {
                                            case "servicenow":
                                                sessionStillValid = Authentication.hasServiceNowSession(authSession.getUsername(), authSession.getSSOSessionId());
                                                break;
                                        }
                                    }
                                }
                                else
                                {
                                    // Revalidate if authSession with token still exists in session cache
                                    
                                    AuthSession reValidatedAuthSession = ((AuthTokenMap)getAuthTokenCache()).getForSessionId(request.getSession().getId(), authSession.getToken());
                                    
                                    if (reValidatedAuthSession == null)
                                    {
                                        sessionStillValid = false;
                                        Log.auth.info("AuthSession for token " + authSession.getToken() + " missing from auth session cache. Invalidating token " + authSession.getToken());
                                    }
                                }

                                if (sessionStillValid)
                                {
                                	if(Log.auth.isTraceEnabled())
                                		Log.auth.trace("Auth Session : " + authSession + " associated with token : " + 
                                    			   token + "/" + authSession.getToken() + " is still valid.");
                                    
                                    boolean doStraightTimout = PropertiesUtil.getPropertyBoolean("session.fixed.timeout");

                                    if (!doStraightTimout)
                                    {
                                        Log.auth.info("Associating updated AuthSession : " + authSession + 
                                        			  " with token and creating/updating cookie...");

                                        if (!(url.contains("%2Fclient%2Fpoll") || 
                                        	  (StringUtils.isNotBlank(request.getHeader(RESOLVE_AUTO_REFRESH_HEADER)))))
                                        {
                                            // update cookie
                                            updateAuthToken(request, response, authSession);
                                            Log.auth.info("Updated authSession: " + authSession + 
                                            			  " updateTime for non */client/poll/auto-refresh URL " + url);
                                        }
                                        else
                                        {
                                        	if (url.contains("%2Fclient%2Fpoll"))
                                        	{
                                        		if(Log.auth.isTraceEnabled())
                                        			Log.auth.trace("AuthSession: " + authSession + 
                                        					   " updateTime was not updated as URL is " + url);
                                        	}
                                        	else if (StringUtils.isNotBlank(request.getHeader(RESOLVE_AUTO_REFRESH_HEADER)))
                                        	{
                                        		if(Log.auth.isTraceEnabled())
                                        			Log.auth.trace("AuthSession: " + authSession + 
                                        					   " updateTime was not updated as request to URL " + url +
                                        					   " is the result of UI auto-refresh.");
                                        	}
                                        }
                                    }
                                    else
                                    {
                                        if (StringUtils.isNotBlank(authSession.getSSOType()))
                                        {
                                            Log.auth.info("Extending age of cookie associated with AuthSession : " + authSession + " by " + (authSession.getSessionTimeout() / 1000) + "seconds");
                                            // For fixed timeout extend the SSO Cookie age
                                            extendSSOCookie(request, response, /*token*/authSession.getToken(), ssoType, authSession.getSessionTimeout());
                                        }
                                    }
                                }
                                else
                                {
                                	if(Log.auth.isTraceEnabled())
                                		Log.auth.trace("Auth Session : " + authSession + " associated with token : " + token + "/" + authSession.getToken() + " is invalid.");

                                    String[] tokenParts = token.split("/");

                                    user = tokenParts[0];

                                    if (tokenParts.length >= 5)
                                    {
                                        if (StringUtils.isNotBlank(authSession.getSSOServiceProvider()))
                                        {
                                            iFrameId = tokenParts[4];
                                            ssoType = null;
                                        }
                                        else
                                        {
                                            ssoType = tokenParts[4];
                                            iFrameId = null;
                                        }
                                    }

                                    if (StringUtils.isNotBlank(ssoType))
                                    {
                                    	if(Log.auth.isTraceEnabled())
                                    		Log.auth.trace("Removing " + ssoType + " Auth Session : " + authSession + " associated with token : " + token + " as it is invalid and extending the cookie age.");

                                        removeAuthToken(token);
                                        extendSSOCookie(request, response, /*token*/authSession.getToken(), ssoType, authSession.getSessionTimeout());
                                    }
                                }
                            }

                            // set username
                            // username = authSession.getUsername();

                            // Return the first valid token and set the user name token of returned token into request
                            if (StringUtils.isBlank(validToken))
                            {
                                // set username and token in request obj
                                request.setAttribute(Constants.HTTP_REQUEST_USERNAME, authSession.getUsername());
                                request.setAttribute(Constants.HTTP_REQUEST_TOKEN, CryptUtils.encryptToken(authSession.getToken()));

                                validToken = /*token*/authSession.getToken();
                                valid = true;

                                String[] tokenParts = token.split("/");

                                user = tokenParts[0];
                                // user = authSession.getUsername();

                                if (tokenParts.length >= 5)
                                {
                                    if (StringUtils.isNotBlank(authSession.getSSOServiceProvider()))
                                    {
                                        iFrameId = tokenParts[4];
                                        ssoType = null;
                                    }
                                    else
                                    {
                                        ssoType = tokenParts[4];
                                        iFrameId = null;
                                    }
                                }
                            }
                        }

                        // remove token
                        else
                        {
                            Log.auth.info("Invalidating authenticated session " + authSession + 
                                          " corresponding to token: " + token + " auth session token: " + authSession.getToken() + 
                                          (StringUtils.isNotBlank(authSession.getSSOType()) ? " and SSO Type " + authSession.getSSOType(): "") + 
                                          (StringUtils.isNotBlank(authSession.getIFrameId()) ? " and i-frame Integration Id " + authSession.getIFrameId(): "") + 
                                          (StringUtils.isNotBlank(authSession.getSSOServiceProvider()) ? " and SSO Service Provider " + authSession.getSSOServiceProvider(): "") + 
                                          " as time elapsed since it was last updated: " + duration + 
                                          " msec > Session Timeout of " +  SESSION_TIMEOUT + " msec");
                            
                            if (StringUtils.isNotBlank(authSession.getSSOType()))
                            {
                                removeAuthToken(/*token*/authSession.getToken());
                                extendSSOCookie(request, response, /*token*/authSession.getToken(), authSession.getSSOType(), authSession.getSessionTimeout());
                            }
                            else
                            {
                                // removeAuthToken(token);
                                invalidate(request, response, /*token*/authSession.getToken(), authSession.getIFrameId());
                            }

                            String[] tokenParts = token.split("/");

                            user = tokenParts[0];

                            if (tokenParts.length >= 5)
                            {
                                if (StringUtils.isNotBlank(authSession.getSSOServiceProvider()))
                                {
                                    iFrameId = tokenParts[4];
                                    ssoType = null;
                                }
                                else
                                {
                                    ssoType = tokenParts[4];
                                    iFrameId = null;
                                }
                            }
                        }
                    }
                }
                else
                {
                    Log.auth.info("Invalidating token " + token + " as no authenticated session found corresponding to it");

                    String[] tokenParts = token.split("/");

                    user = tokenParts[0];

                    if (tokenParts.length >= 5)
                    {
                        ssoType = tokenParts[4];
                    }

                    if (StringUtils.isNotBlank(ssoType) &&
                        (ssoType.equals("servicenow") || (ssoType.equals("splunk"))))
                    {
                        long DEFAULT_SESSION_TIMEOUT = (PropertiesUtil.getPropertyLong(ConstantValues.PROPERTIES_SESSION_TIMEOUT) < 720) ? 720000 : PropertiesUtil.getPropertyLong(ConstantValues.PROPERTIES_SESSION_TIMEOUT) * 1000;

                        extendSSOCookie(request, response, token, ssoType, DEFAULT_SESSION_TIMEOUT);
                    }
                    else
                    {
                        // removeAuthToken(token);
                        invalidate(request, response, token, ssoType);
                        user = null;
                        ssoType = null;
                    }
                }
            }
        }
        
        return new ValidateTokenResult(valid, user, ssoType, validToken, ssoType);
    } // validateToken

    public static boolean validateApp(String username, String token, HttpServletRequest request, HttpServletResponse response) throws RSAuthException
    {
        return canUserAccessThisUrl(request);

    } // validateApp

    public static void invalidate(HttpServletRequest request, HttpServletResponse response, String ssoType)
    {
        invalidate(request, response, null, ssoType);
    } // invalidate

    public static void invalidateForSession(HttpServletRequest request, HttpServletResponse response, String ssoType)
    {
        invalidateForSession(request, response, null, ssoType);
    } // invalidate
    
    private static void invalidate(HttpServletRequest request, HttpServletResponse response, String token, String ssoType)
    {
        Collection<Cookie> cookies = getAuthCookies(request, ssoType);
        boolean clearIFrameIdFromCache = true;
        if(StringUtils.isNotEmpty(token))
        {
        	AuthSession removedAuthSess = removeAuthToken(token);
        	if(removedAuthSess != null)
        	{
        		String cookieNameSuffix = null;
                
                if (StringUtils.isNotBlank(removedAuthSess.getSSOType()) || StringUtils.isNotBlank(removedAuthSess.getIFrameId()))
                {
                    cookieNameSuffix = StringUtils.isNotBlank(removedAuthSess.getSSOType()) ? 
                    		removedAuthSess.getSSOType() : removedAuthSess.getIFrameId();
                }
                
        		String cookieName = Constants.AUTH_TOKEN + (StringUtils.isNotBlank(cookieNameSuffix) ? "-" + cookieNameSuffix.toUpperCase() : "");

                Cookie cookie = new Cookie(cookieName, "tobedelete");
                cookie.setPath("/");
                cookie.setHttpOnly(true);
                cookie.setMaxAge(0);
                ESAPI.httpUtilities().addCookie(response, cookie);
        	}
        }
        if (cookies != null && !cookies.isEmpty())
        {
            for (Cookie cookie : cookies)
            {
                if (cookie != null)
                {
                    String decryptedCookieToken = CryptUtils.decryptToken(cookie.getValue());
                    
                    // Check if two tokens same except time
                    
                    String[] authSessTokenParts = StringUtils.isNotBlank(token) ? token.split("/") : new String[] {};
                    String[] cookieTokenParts = decryptedCookieToken.split("/");
                    
                    boolean sameTokens = false;
                    
                    if (authSessTokenParts != null && cookieTokenParts != null && authSessTokenParts.length == cookieTokenParts.length)
                    {
                        sameTokens = true;
                        
                        for (int i = 0; i < authSessTokenParts.length && sameTokens; i++)
                        {
                            // skip time which is at index 1 in token
                            
                            if (i != 1)
                            {
                                Log.auth.trace("sameTokens = " + Boolean.toString(sameTokens) + ", authSessTokenParts[" + i + "] = " + 
                                               authSessTokenParts[i] + ", cookieTokenParts[" + i + "] = " + cookieTokenParts[i]);
                                sameTokens &= authSessTokenParts[i].equals(cookieTokenParts[i]);
                            }
                        }
                    }
                    
                    if (StringUtils.isBlank(token) || (StringUtils.isNotBlank(token) && sameTokens))
                    {
                        Log.auth.trace("Removing cookie " + cookie.getName() + " containing " + decryptedCookieToken + " with MaxAge = " + cookie.getMaxAge());
                        // remove cookie
                        // cookie.setPath("/"); //Don't care for the path
                        cookie.setMaxAge(0);

                        String ctoken = decryptedCookieToken;

                        // remove token from autheticated session cache if it exists
                        if (StringUtils.isNotEmpty(ctoken))
                        {
                            AuthSession removedAuthSess = removeAuthToken(ctoken);
                            
                            if (removedAuthSess != null && removedAuthSess.hasMockSSOSrvcData() &&
                                removedAuthSess.getMockSSOSrvcDataForSession(request.getSession().getId()) != null &&
                                !removedAuthSess.getMockSSOSrvcDataForSession(request.getSession().getId()).isEmpty())
                            {
                                getMockSSOSrvcDataCache().put(request.getSession().getId(), 
                                                              removedAuthSess.getMockSSOSrvcDataForSession(request.getSession().getId()));
                                Log.auth.trace("Added Mock SSO Service Data " + 
                                               StringUtils.mapToString(removedAuthSess.getMockSSOSrvcDataForSession(request.getSession().getId()), 
                                                                       "=", ", ") +
                                               " for Session Id : " + request.getSession().getId() + " to Mock SSO Service Cache!!!");
                                
                                /*
                                 * Do not remove cached iframeId from i-frame pre-login session cache 
                                 * if i-frame id is set and SSO Provider is set.
                                 * Auto re-login needs to have i-frame Id to log back in and set original i-frame id.
                                 */
                                
                                if (StringUtils.isNotBlank(removedAuthSess.getIFrameId()) && 
                                    StringUtils.isNotBlank(removedAuthSess.getSSOServiceProvider()))
                                {
                                    clearIFrameIdFromCache = false;
                                }
                            }
                        }

                        // DefaultHTTPUtilities dhttp = new DefaultHTTPUtilities();
                        ESAPI.httpUtilities().addCookie(response, cookie);
                        // dhttp.addCookie(response, cookie);
                        // response.addCookie(cookie);
                    }
                    else
                    {
                        if (StringUtils.isNotBlank(token) && !decryptedCookieToken.equals(token))
                        Log.auth.warn("Invalidate: AuthSesison token: [" + token + "] and de-crypted token from cookie [" + decryptedCookieToken + "] do not match, FAILED TO INVALIDATE!!!");
                    }
                }
            }
        }
                
        if (clearIFrameIdFromCache)
        {
            // Remove iframe login session from cache
            removeIFrameIdFromIFramePreLoginSessionCache(request);
        }
    } // invalidate

    private static void invalidateForSession(HttpServletRequest request, HttpServletResponse response, String token, String ssoType)
    {
        Collection<Cookie> cookies = getAuthCookies(request, ssoType);
        boolean clearIFrameIdFromCache = true;

        if (cookies != null && !cookies.isEmpty())
        {
            for (Cookie cookie : cookies)
            {
                if (cookie != null)
                {
                    String decryptedCookieToken = CryptUtils.decryptToken(cookie.getValue());
                    
                    // Check if two tokens same except time
                    
                    String[] authSessTokenParts = StringUtils.isNotBlank(token) ? token.split("/") : new String[] {};
                    String[] cookieTokenParts = decryptedCookieToken.split("/");
                    
                    boolean sameTokens = false;
                    
                    if (authSessTokenParts != null && cookieTokenParts != null && authSessTokenParts.length == cookieTokenParts.length)
                    {
                        sameTokens = true;
                        
                        for (int i = 0; i < authSessTokenParts.length && sameTokens; i++)
                        {
                            // skip time which is at index 1 in token
                            
                            if (i != 1)
                            {
                                Log.auth.trace("sameTokens = " + Boolean.toString(sameTokens) + ", authSessTokenParts[" + i + "] = " + 
                                               authSessTokenParts[i] + ", cookieTokenParts[" + i + "] = " + cookieTokenParts[i]);
                                sameTokens &= authSessTokenParts[i].equals(cookieTokenParts[i]);
                            }
                        }
                    }
                    
                    if (StringUtils.isBlank(token) || (StringUtils.isNotBlank(token) && sameTokens))
                    {
                        Log.auth.trace("Removing cookie " + cookie.getName() + " containing " + decryptedCookieToken + " with MaxAge = " + cookie.getMaxAge());
                        // remove cookie
                        // cookie.setPath("/"); //Don't care for the path
                        cookie.setMaxAge(0);

                        String ctoken = decryptedCookieToken;

                        // remove token from autheticated session cache if it exists
                        if (StringUtils.isNotEmpty(ctoken))
                        {
                            AuthSession removedAuthSess = removeAuthTokenForSession(ctoken, request);
                            
                            if (removedAuthSess == null)
                            {
                                removedAuthSess = ((AuthTokenMap)getAuthTokenCache()).getForSessionId(request.getSession().getId(), ctoken);
                            }
                            
                            if (removedAuthSess != null && removedAuthSess.hasMockSSOSrvcData() &&
                                removedAuthSess.getMockSSOSrvcDataForSession(request.getSession().getId()) != null &&
                                !removedAuthSess.getMockSSOSrvcDataForSession(request.getSession().getId()).isEmpty())
                            {
                                getMockSSOSrvcDataCache().put(request.getSession().getId(), 
                                                              removedAuthSess.getMockSSOSrvcDataForSession(request.getSession().getId()));
                                
                                Log.auth.trace("Added Mock SSO Service Data " + 
                                                StringUtils.mapToString(removedAuthSess.getMockSSOSrvcDataForSession(request.getSession().getId()), 
                                                                        "=", ", ") +
                                                " for Session Id : " + request.getSession().getId() + " to Mock SSO Service Cache!!!");
                                
                                /*
                                 * Do not remove cached iframeId from i-frame pre-login session cache 
                                 * if i-frame id is set and SSO Provider is set.
                                 * Auto re-login needs to have i-frame Id to log back in and set original i-frame id.
                                 */
                                
                                if (StringUtils.isNotBlank(removedAuthSess.getIFrameId()) && 
                                    StringUtils.isNotBlank(removedAuthSess.getSSOServiceProvider()))
                                {
                                    clearIFrameIdFromCache = false;
                                }
                            }
                        }

                        // DefaultHTTPUtilities dhttp = new DefaultHTTPUtilities();
                        ESAPI.httpUtilities().addCookie(response, cookie);
                        // dhttp.addCookie(response, cookie);
                        // response.addCookie(cookie);
                    }
                    else
                    {
                        if (StringUtils.isNotBlank(token) && !decryptedCookieToken.equals(token))
                        Log.auth.warn("Invalidate: AuthSesison token: [" + token + "] and de-crypted token from cookie [" + decryptedCookieToken + "] do not match, FAILED TO INVALIDATE!!!");
                    }
                }
            }
        }
        
        if (clearIFrameIdFromCache)
        {
            // Remove iframe login session from cache
            removeIFrameIdFromIFramePreLoginSessionCache(request);
        }
    } // invalidateForSession
    
    private static void extendSSOCookie(HttpServletRequest request, HttpServletResponse response, String token, String ssoType, long sessionTimeout)
    {
        Collection<Cookie> cookies = getAuthCookies(request, ssoType);

        if (cookies != null && !cookies.isEmpty())
        {
            for (Cookie cookie : cookies)
            {
                if (cookie != null)
                {
                    if (StringUtils.isBlank(token) || (StringUtils.isNotBlank(token) && cookie.getValue().equals(token)))
                    {
                        Log.auth.trace("Extending cookie " + cookie.getName() + " containing " + cookie.getValue() + " with MaxAge = " + cookie.getMaxAge());

                        String oldCookieName = cookie.getName();
                        String oldValue = cookie.getValue();
                        boolean isSecure = cookie.getSecure();

                        cookie.setMaxAge(0);
                        // DefaultHTTPUtilities dhttp = new DefaultHTTPUtilities();
                        // dhttp.addCookie(response, cookie);
                        ESAPI.httpUtilities().addCookie(response, cookie);
                        // response.addCookie(cookie);

                        Cookie newCookie = new Cookie(oldCookieName, oldValue);
                        newCookie.setPath("/");
                        newCookie.setValue(oldValue);

                        if (isSecure)
                        {
                            newCookie.setSecure(true);
                        }

                        newCookie.setMaxAge(((int) (sessionTimeout / 1000)) * 2);

                        try
                        {
                            new URL(request.getRequestURL().toString());
                        }
                        catch (MalformedURLException e)
                        {
                            Log.auth.error(e.getMessage(), e);
                        }

                        if (StringUtils.isNotBlank(domain))
                        {
                            if (domain.startsWith("."))
							{
								newCookie.setDomain(domain);
							}
							else
                            {
                                newCookie.setDomain("." + domain);
                            }
                        }

                        // response.addCookie(newCookie);
                        // dhttp.addCookie(response, newCookie);
                        ESAPI.httpUtilities().addCookie(response, newCookie);
                        break;
                    }
                }
            }
        }
    }

    private static void updateTokenTimeout(String token, AuthSession authSession, HttpServletRequest request)
    {
        // get the flag from the database
        boolean doStraightTimout = PropertiesUtil.getPropertyBoolean("session.fixed.timeout");

        if (!doStraightTimout)
        {
        	if(Log.auth.isTraceEnabled())
        		Log.auth.trace("AuthSession Before Update: " + authSession + " for token: " + token);
            authSession.setUpdateTimeForSession(request.getSession().getId(), System.currentTimeMillis());
            if(Log.auth.isTraceEnabled())
            	Log.auth.trace("AuthSession After Update: " + authSession + " for token: " + token);
            // NOTE: need to PUT again to serialize to other cluster members
            authTokenCache.put(token, authSession);
        }

    } // updateTokenTimeout

    public static class AuthenticateResult
    {
        public boolean valid;
        public String authType;
        public ERR errCode;

        public AuthenticateResult(boolean valid, String authType)
        {
            this.valid = valid;
            this.authType = authType;      
        }
        
        public AuthenticateResult(boolean valid, String authType, ERR errCode)
        {
            this.valid = valid;
            this.authType = authType;
            this.errCode = errCode;
        }
        
        public String toString()
        {
            return "AuthenticateResult: valid=" + valid + ", authType=" + authType + 
            	   (errCode != null ? errCode.getMessage() : "") + ".";
        }
    }

    /*
     * Sanitize attributes map returned by AD/LDAP/RADIUS local/remote 
     * by replacing value for any lower cased attribute name containing 
     * "pass" with *****.
     */
    private static void sanitizeAttributes(Map<String, Object> attributes) {
    	if (attributes != null && !attributes.isEmpty()) {
    		List<Map.Entry<String, Object>> attributesEntryList = (attributes.entrySet())
	    													  	  .stream()
	    													  	  .collect(
	    													  	      Collectors.toCollection(
	    													              ArrayList<Map.Entry<String, Object>>::new));
	    	
    		attributesEntryList.parallelStream().forEach(me -> {
	    			       								           if (me.getKey().toLowerCase().contains(PASS_LC)) {
	    			       								    	       me.setValue(SANITIZED_PAS_SWORD);
	    			       								           }	    									    		    	   
	    												       });
    	}
    }
    
    @SuppressWarnings("unchecked")
    public static AuthenticateResult authenticate(String username, String password)
    {
        boolean result = false;
        boolean grouprequired = false;
        String authType = "local";
        ERR errCode = null;

        Log.auth.trace("Into authenticate for user: " + username);

        if (StringUtils.isNotEmpty(username))
        {
            // public user
            if (username.equals(Constants.USER_PUBLIC))
            {
                result = true;
            }

            // resolve.maint user
            else if (username.equals(Constants.USER_RESOLVE_MAINT))
            {
                File file = FileUtils.getFile(RSContext.getWebHome() + "/WEB-INF/users/resolve.maint");
                if (UserUtils.checkMaintPassword(username, password, file))
                {
                    result = true;
                }
            }

            // admin user
            else if (username.equals(Constants.USER_ADMIN))
            {
                // resolve db user
                if (UserUtils.checkPassword(username, password) && UserUtils.isLockedOut(username) == false)
                {
                    result = true;
                }
            }

            // normal user
            else if (UserUtils.isLockedOut(username) == false && UserUtils.isOrgEnabled(username) == true)
            {
                Log.auth.trace("Into the condition for the normal user");
                boolean fallback = true;

                Map<String, Object> attributes = null;
                Properties syncProperties = null;

                // check rsremote RADIUS/LDAP/AD in specific order
                String org_id = "";
                String domainId = getDomain(username);
                Log.auth.trace("Getting the domainId for this user :" + (StringUtils.isNotBlank(domainId) ? domainId : " No domain id found in user name"));

                if (StringUtils.isBlank(domainId) && StringUtils.isNotBlank(PropertiesUtil.getPropertyString("authentication.defaultdomain")))
                {
                    /*
                     * Set default domain if not specified by user and
                     * system property authentication.defaultdomain is set.
                     */
                    domainId = PropertiesUtil.getPropertyString("authentication.defaultdomain");
                }

                if (StringUtils.isNotEmpty(domainId))
                {
                    if (ServiceAuthentication.isRADIUSDomainExist(domainId))
                    {
                        authType = "RADIUS";
                        Log.auth.trace("This is an RADIUS domain");
                        ConfigRADIUSVO vo = ServiceAuthentication.getRADIUSFor(domainId);
                        Log.auth.trace("RADIUS domain [" + domainId + "] Configuration: " + vo);

                        if (vo != null)
                        {
                            if (vo.getBelongsToOrganization() != null)
                            {
                                org_id = vo.getBelongsToOrganization().getSys_id();
                            }

                            if (ServiceAuthentication.isOrgEnabled(org_id))
                            {
                                attributes = authenticateRemoteRADIUS(username, password, vo);
                                // syncProperties = StringUtils.stringToProperties(vo.getUProperties());
                                // Set the "memberof" key to group name received from RADIUS

                                if (attributes != null && !attributes.isEmpty() && !attributes.containsKey(Constants.MEMBEROF) && !attributes.containsKey(Constants.MEMBEROF_DEFAULT) && StringUtils.isNotBlank(PropertiesUtil.getPropertyString("authentication.defaultmemberof")))
                                {
                                    attributes.put(Constants.MEMBEROF_DEFAULT, PropertiesUtil.getPropertyString("authentication.defaultmemberof"));
                                }

                                fallback = vo.getUFallback();

                                // For RADIUS grouprequired is true by default

                                grouprequired = true;
                            }
                            else
                            {
                                Log.log.warn("Organization is disabled. org_id: " + org_id);
                                Log.auth.warn("Organization is disabled. org_id: " + org_id);
                            }
                        }
                    }
                    else if (ServiceAuthentication.isLDAPDomainExist(domainId))
                    {
                        authType = "Remote LDAP";
                        Log.auth.trace("This is a remote LDAP user");
                        ConfigLDAPVO vo = ServiceAuthentication.getLDAPFor(domainId);
                        Log.auth.trace("Value for the LDAP domain:" + vo);

                        if (vo != null)
                        {
                            if (vo.getBelongsToOrganization() != null)
                            {
                                org_id = vo.getBelongsToOrganization().getSys_id();
                            }

                            if (ServiceAuthentication.isOrgEnabled(org_id))
                            {
                                attributes = authenticateRemoteLDAP(username, password, vo);
                                syncProperties = StringUtils.stringToProperties(vo.getUProperties());
                                fallback = vo.getUFallback();
                            }
                            else
                            {
                                Log.log.warn("Organization is disabled. org_id: " + org_id);
                                Log.auth.warn("Organization is disabled. org_id: " + org_id);
                            }

                            grouprequired = vo.getGroupRequired();
                        }
                    }
                    else if (ServiceAuthentication.isActiveDirectoryDomainExist(domainId))
                    {
                        authType = "Remote AD";
                        Log.auth.trace("This is a remote AD user");
                        ConfigActiveDirectoryVO vo = ServiceAuthentication.getActiveDirectoryFor(domainId);
                        Log.auth.trace("Value for the AD domain:" + vo);

                        if (vo != null)
                        {
                            if (vo.getBelongsToOrganization() != null)
                            {
                                org_id = vo.getBelongsToOrganization().getSys_id();
                            }

                            if (ServiceAuthentication.isOrgEnabled(org_id))
                            {
                                attributes = authenticateRemoteActiveDirectory(username, password, vo);
                                syncProperties = StringUtils.stringToProperties(vo.getUProperties());
                                fallback = vo.getUFallback();
                            }
                            else
                            {
                                Log.log.warn("Organization is disabled. org_id: " + org_id);
                                Log.auth.warn("Organization is disabled. org_id: " + org_id);
                            }

                            grouprequired = vo.getGroupRequired(); //RSContext.getMain().getConfigActiveDirectory().isGrouprequired();
                        }
                    }
                }
                
                sanitizeAttributes(attributes);
                
                // check if local rsview LDAP/AD if domain is empty or has domain but not remote (i.e. not in config_ldap/ad table)
                Log.auth.trace("Checking the attributes : " + attributes);
                if (attributes == null)
                {
                    LDAP clonedLDAP = RSContext.getMain().getLDAP();
                    ActiveDirectory clonedAD = RSContext.getMain().getActiveDirectory();
                    RADIUS clonedRADIUS =  RSContext.getMain().getRADIUS();
                    
                    // ldap user
                    if (clonedLDAP != null)
                    {
                        authType = "Local LDAP";
                        Log.auth.trace("This is a local LDAP user");
                        grouprequired = RSContext.getMain().getConfigLDAP().isGrouprequired();
                        attributes = clonedLDAP.authenticate(username, password);
                        syncProperties = clonedLDAP.getSyncProperties();
                        fallback = RSContext.getMain().getConfigLDAP().isFallback();
                    }

                    // activedirectory user
                    else if (clonedAD != null)
                    {
                        authType = "Local AD";
                        Log.auth.trace("This is a local AD user");
                        grouprequired = RSContext.getMain().getConfigActiveDirectory().isGrouprequired();
                        attributes = clonedAD.authenticate(username, password, null);
                        syncProperties = clonedAD.getSyncProperties();
                        fallback = RSContext.getMain().getConfigActiveDirectory().isFallback();
                    }
					//CISCO ACS-Internal User RADIUS Authentication
					else if(clonedRADIUS!=null) {
                        authType="Local RADIUS";
                        Log.auth.trace("This is a CISCO ACS user");
                        grouprequired = RSContext.getMain().getConfigRADIUS().isGrouprequired();
                        attributes = clonedRADIUS.authenticateUser(username, password);
                        fallback = RSContext.getMain().getConfigRADIUS().isFallback();
                    }
                }

                Log.auth.trace("authType : " + authType);
                Log.auth.trace("attributes : " + attributes);
                Log.auth.trace("syncProperties : " + syncProperties);
                Log.auth.trace("fallback : " + fallback);
                Log.auth.trace("grouprequired : " + grouprequired);

                // String userNameWithoutDomain = getUserNameWithoutDomain(username);

                try
                {
                    if (attributes != null && attributes.size() > 0)
                    {
                        Log.auth.trace("attributes are not null. Calling the initializeUser");
                        
                        try {
                        	String initedUserName = initializeUser(username, org_id, domainId, attributes, syncProperties, grouprequired, authType);
                        	if (StringUtils.isNotBlank(initedUserName)) {
                        		username = initedUserName;
                        	}
                        	result = true;
                        } catch (Exception e) {
                        	String msg = String.format("Error in initializing %s authenticated user: %s in Resolve. %s", 
                        							   authType, username, (StringUtils.isNotBlank(e.getMessage()) ? 
                        									   	  			e.getMessage() : ""));
     
					     	if (StringUtils.isNotBlank(e.getMessage()) && 
					     		(ERR.E10022.getMessage().equals(e.getMessage()) || 
					     		 ERR.E10023.getMessage().equals(e.getMessage()))) {
					     		Log.auth.info(msg);
					     		errCode = ERR.getERRFromMessage(e.getMessage());
					     	} else {
					     		Log.auth.error(msg, e);
					     	}
                        }                        
                    }
                    else
                    {
                        Log.log.warn("LDAP/AD/RADIUS Authentication failed.");
                        Log.auth.warn("LDAP/AD/RADIUS Authentication failed.");

                        if (fallback)
                        {
                            Log.log.warn("Falling back to internal Resolve authentication.");
                            Log.auth.warn("Falling back to internal Resolve authentication.");
                        }

                        result = false;
                    }

                    // set fallback to true if resolve.maint or admin
                    if (username.equalsIgnoreCase("resolve.maint") || username.equalsIgnoreCase("admin"))
                    {
                        fallback = true;
                    }

                    Log.auth.trace("result : " + result);
                    Log.auth.trace("fallback : " + fallback);

                    // check resolve db user
                    if (!result && fallback)
                    {
                        // check if fallback to resolve auth allowed
                        if (UserUtils.checkPassword(username, password))
                        {
                            result = true;
                        }
                        else
                        {
                            Log.log.info("Resolve authentication failed.");
                            Log.auth.info("Resolve authentication failed.");
                        }
                    }
                }
                catch (Exception e)
                {
                    Log.auth.warn("Error in User login for user:" + username, e);
                    result = false;
                }

                Log.auth.trace("After checking the password, result : " + result);

            }

            // update login failure for user
            if (result == false && errCode == null)
            {
                ServiceAuthentication.updateLoginFailure(username);
            }
        }

        return new AuthenticateResult(result, authType, errCode);
    } // authenticate

    private static ServiceNowAuthResult authenticateServiceNow(String username, String sessionId)
    {
        int result = -2;
        
        long sessionTimeout = 30 * 60 * 1000;
        String authentiServiceNowSessionId = StringUtils.isNotBlank(sessionId) ? sessionId : null;

        Log.auth.debug("Into authenticate for user in ServiceNow: " + username + ", " + sessionId);

        if (StringUtils.isNotBlank(username))
        {
            String serviceNowUrl = PropertiesUtil.getPropertyString("servicenow.rest.url");
            
            String httpbasicauthusername = RSContext.getMain().getConfigAuth().getServicenowSSOUsername();
            if(httpbasicauthusername.isEmpty()) httpbasicauthusername = PropertiesUtil.getPropertyString("servicenow.rest.username");	
            
            String httpbasicauthpassword = RSContext.getMain().getConfigAuth().getServicenowSSOPassword();
            if(httpbasicauthpassword.isEmpty()) httpbasicauthpassword = PropertiesUtil.getPropertyString("servicenow.rest.password");
            
            Boolean isGroupRequired = PropertiesUtil.getPropertyBoolean("servicenow.grouprequired");

            //don't log the password, even if it is useful
            Log.auth.trace("ServiceNow: url=" + serviceNowUrl + ", username=" + httpbasicauthusername);

            if (StringUtils.isBlank(serviceNowUrl) || StringUtils.isBlank(httpbasicauthusername) || StringUtils.isBlank(httpbasicauthpassword))
            {
                result = -3;
                Log.auth.error("Check all servicenow SSO related system properties like servicenow.rest.url etc.");
            }
            else
            {
                try
                {
                    // let's validate user is valid loggedin user in ServiceNow
                    // first.
                    // call ServiceNow REST API to validate/get the loged in
                    // users session id

                    RestCaller restCaller = new RestCaller(serviceNowUrl, httpbasicauthusername, httpbasicauthpassword, Main.main.configProxy);

                    Map<String, String> sysPropParams = new HashMap<String, String>();
                    sysPropParams.put("sysparm_query", "name=glide.ui.session_timeout");
                    sysPropParams.put("sysparm_fields", "value");
                    sysPropParams.put("sysparm_limit", "1");
                    sysPropParams.put("sysparm_offset", "0");

                    Log.auth.trace("ServiceNow sys_properties request parameters: " + StringUtils.mapToString(sysPropParams));

                    restCaller.setUrlSuffix("sys_properties");

                    int retryCount = 0;
//** Fetch user record from the system_propties table. Retry 5 times. If failed to get record even after 5 times then doesn't fecth the record from v_user_session table and returna resulcode 1
                    while (result < 0)
                    {
                        if(retryCount == 5) 
                        {
                            Log.log.debug("Error retriving record from sys_properties table of ServiceNow");
                            result = 1;
                            break;
                        }
                        if (retryCount > 0)
                        {
                            Thread.sleep(100);
                        }
                        String sysPropResponse = restCaller.getMethod(null, sysPropParams, null);

                        Log.auth.trace("ServiceNow sys_properties response: " + sysPropResponse);
                        List<Map<String, String>> sysProps = new ArrayList<Map<String, String>>();
                        try
                        {
                            sysProps = StringUtils.jsonArrayStringToList("result", sysPropResponse);
                        }
                        catch (Exception e)
                        {

                        }

                        if (sysProps != null && !sysProps.isEmpty())
                        {
                            Map<String, String> sysProp = sysProps.get(0);

                            // Log.auth.trace("glide.ui.session_timeout
                            // sys_property: " +
                            // StringUtils.mapToString(sysProp));

                            if (sysProp.containsKey("value"))
                            {
                                if (sysProp.get("value") != null && !sysProp.get("value").isEmpty())
                                {
                                    try
                                    {
                                        sessionTimeout = Integer.parseInt(sysProp.get("value")) * 60 * 1000;
                                        result = 0;
                                        Log.auth.trace("ServiceNow Session Timeout is " + sessionTimeout + "ms");
                                    }
                                    catch (NumberFormatException nfe)
                                    {
                                        Log.log.warn("System property glide.ui.session_timeout returned non integer value of " + sysProp.get("value"));
                                    }

                                }

                            }
                            
                           
                        }
                  
                        if (result < 0)
                        {
                            retryCount++;
                        }
                    }
                    
                    //** If you find vaild record in sys_properties table then only go and fetch the v_user_session table.                  
                    if (result == 0)
                    {
                        //reset retryCount and result for v_user_session table record fetch
                        retryCount = 0;
                        result = -2;
                        
                        Map<String, String> sessionReqParams = new HashMap<String, String>();
                        sessionReqParams.put("sysparm_query", "locked=false"
                                        /*
                                         * + (StringUtils.isBlank(sessionId) ?
                                         * "^active=true" : "")
                                         */ + "^user=" + username + (StringUtils.isNotBlank(sessionId) ? "^session_id=" + sessionId : ""));
                        sessionReqParams.put("sysparm_limit", "1");
                        sessionReqParams.put("sysparm_offset", "0");

                        Log.auth.trace("ServiceNow session request parameters: " + StringUtils.mapToString(sessionReqParams));

                        restCaller.setUrlSuffix("v_user_session");

// fetch the v_user_session table. If fail to get record retry for 5 times.else return result code 1                 
                        while (result < 0)
                        {
                            if(retryCount == 5) 
                            {
                                Log.log.debug(username + " is not logged in user.");
                                result = 1;
                                break;
                            }
                            
                            if (retryCount > 0)
                            {
                                Thread.sleep(100);
                            }
                            

                            String response = restCaller.getMethod(null, sessionReqParams, null);
                            List<Map<String, String>> sessionData = new ArrayList<Map<String, String>>();
                            Log.auth.trace("ServiceNow session response [" + (retryCount + 1) + "]: " + response);
                            try
                            {
                                sessionData = StringUtils.jsonArrayStringToList("result", response);
                            }
                            catch (Exception e)
                            {

                            }
                            
                            if (sessionData != null && sessionData.size() > 0)
                            {
                                if (StringUtils.isBlank(authentiServiceNowSessionId))
                                {
                                    Map<String, String> userSessionData = sessionData.get(0);
                                    authentiServiceNowSessionId = userSessionData.get("session_id");
                                }

                                Log.log.debug(username + " is logged in user with authentic ServieNow Session Id " + authentiServiceNowSessionId + ".");
                                result = 0;
                            }
                           
                            if (result < 0)
                            {
                                retryCount++;
                            }
                        }

                        if (result >= 0) // Even in case of Session Timed Out
                                         // check
                                         // if user is present in Resolve
                        {
                           
                            UsersVO userVO = UserUtils.getUser(username);
                            if (userVO == null)
                            {
                                Log.log.debug("ServiceNow user not found in Resolve, creating it: " + username);
                                username = initializeServiceNowUser(restCaller, username, isGroupRequired);
                            }
                            else
                            {
                                if (UserUtils.isLockedOut(username))
                                {
                                    Log.log.warn("ServiceNow user found in Resolve, but is locked: " + username);
                                    result = -1;
                                }
                                else
                                {
                                    Log.log.debug("ServiceNow user found in Resolve, proceed with login: " + username);
                                }
                            }
                        }
                    }
                }
                catch (Exception e)
                {
                	String msg = String.format("Error in ServiceNow SSO user login for user: %s. %s", username, 
                							   (StringUtils.isNotBlank(e.getMessage()) ? e.getMessage() : ""));
                
                	if (StringUtils.isNotBlank(e.getMessage()) && 
                		(ERR.E10022.getMessage().equals(e.getMessage()) || 
                		 ERR.E10023.getMessage().equals(e.getMessage()))) {
                		result = -5;
                		Log.auth.info(msg);
                	} else {
                		result = -4;
                		Log.auth.error(msg, e);
                	}
                }
            }

            /*
             * SSO update failure could be for 2 reasons Missing or incomplete
             * configuration on Resolve (-3) Inability to reach ServiceNow
             * server using configured values (-4)
             * 
             * In either case this should not be counted towards unsuccessful
             * login attempts for Resolve user.
             *
             * if (result < 0) {
             * ServiceAuthentication.updateLoginFailure(username); }
             */
        }

        return new ServiceNowAuthResult(result, sessionTimeout, authentiServiceNowSessionId);
    } // authenticate

    private static boolean hasServiceNowSession(String username, String ssoSessionId)
    {
        boolean hasServiceNowSession = false;

        Log.auth.debug("Checking if " + username + (StringUtils.isNotBlank(ssoSessionId) ? " with ServiceNow Session Id " + ssoSessionId : "") + " is still valid user in ServiceNow");

        String serviceNowUrl = PropertiesUtil.getPropertyString("servicenow.rest.url");
        String httpbasicauthusername = PropertiesUtil.getPropertyString("servicenow.rest.username");
        String httpbasicauthpassword = PropertiesUtil.getPropertyString("servicenow.rest.password");

        Log.auth.trace("ServiceNow: url=" + serviceNowUrl + ", username=" + httpbasicauthusername + ", password=" + httpbasicauthpassword);

        if (StringUtils.isBlank(serviceNowUrl) || StringUtils.isBlank(httpbasicauthusername) || StringUtils.isBlank(httpbasicauthpassword))
        {
            Log.auth.error("Check all servicenow SSO related system properties like servicenow.rest.url etc.");
        }
        else
        {
            try
            {
                RestCaller restCaller = new RestCaller(serviceNowUrl, httpbasicauthusername, 
                                                       httpbasicauthpassword, Main.main.configProxy);

                Map<String, String> sessionReqParams = new HashMap<String, String>();
                sessionReqParams.put("sysparm_query", "locked=false" /* + "^active=true" */ + "^user=" + username + (StringUtils.isNotBlank(ssoSessionId) ? "^session_id=" + ssoSessionId : ""));
                sessionReqParams.put("sysparm_limit", "1");
                sessionReqParams.put("sysparm_offset", "0");

                Log.auth.trace("ServiceNow session request parameters: " + StringUtils.mapToString(sessionReqParams));

                restCaller.setUrlSuffix("v_user_session");

                int retryCount = 0;

                while (!hasServiceNowSession && retryCount < 5)
                {
                    if (retryCount > 0)
                    {
                        Thread.sleep(100);
                    }

                    String response = restCaller.getMethod(null, sessionReqParams, null);

                    Log.auth.trace("ServiceNow session response [" + (retryCount + 1) + "]: " + response);

                    List<Map<String, String>> sessionData = StringUtils.jsonArrayStringToList("result", response);

                    if (sessionData != null && sessionData.size() > 0)
                    {
                        Log.log.debug(username + " has valid unlocked ServiceNow session.");
                        hasServiceNowSession = true;
                    }

                    if (retryCount < 5)
                    {
                        retryCount++;
                    }
                }
            }
            catch (Exception e)
            {
                Log.auth.error("Error in autheticating session in ServiceNow for user " + username, e);
            }
        }

        return hasServiceNowSession;
    }

    private static String initializeUser(String username, String org_id, String domainId, Map<String, Object> attributes, Properties syncAttributes, boolean grouprequired, String authType) throws Exception
    {
        Log.auth.trace("Into initializeUser");
        Log.auth.trace("REQUEST COMING FROM :" + ExceptionUtils.getStackTrace(new Exception("LOGGING PURPOSE ONLY. PLEASE IGNORE THIS.")));

        // set username to lowercase
        username = username.toLowerCase();

        // update assigned user groups from LDAP/AD/RADIUS/SiteMinder
        List<String> memberOf = getMemberOf(attributes, syncAttributes);

        // if this flag is true, than make sure that Resolve has atleast one of the LDAP/AD/RADIUS/SiteMinder group
        if (grouprequired)
        {
            Log.auth.trace("Validate the " + authType + " groups if they exist in Resolve for user:" + username);
            ServiceAuthentication.validateIfGroupRequiredForUser(username, memberOf, authType);
        }

        // copy details from template "user" if does not exist
        Log.auth.trace("Cloning user with template");
        boolean cloned = ServiceHibernate.copyTemplateUser(username, domainId);

        if (cloned)
        {
            String syslogMessage = "Cloned " + authType + " user " + username + " into Resolve";
            Log.syslog.info(SyslogFormatter.INSTANCE.formatAudit(InetAddress.getLocalHost().getHostAddress(), username, InetAddress.getLocalHost().getHostAddress(), syslogMessage));
        }

        Log.auth.trace("Assigning User Groups:" + memberOf + " of Authorization Type " + authType);
        ServiceAuthentication.assignUserGroups(username, memberOf, authType);

        // sync authType user mapping
        if (syncAttributes != null)
        {
            Log.auth.trace("Syncing " + username + " attributes from Authentication system of type " + authType);
            syncUserAttributes(username, attributes, syncAttributes, domainId, org_id);
        }

        return username;
    } // initializeUser

    private static String initializeServiceNowUser(RestCaller restCaller, String username, boolean grouprequired) throws Exception
    {
        Log.auth.trace("Into initializeServiceNowUser");
        Log.auth.trace("REQUEST COMING FROM :" + ExceptionUtils.getStackTrace(new Exception("LOGGING PURPOSE ONLY. PLEASE IGNORE THIS.")));

        // set username to lowercase
        username = username.toLowerCase();

        // create the user in Resolve
        // get user's information from ServiceNow
        Map<String, String> userReqParams = new HashMap<String, String>();
        userReqParams.put("user_name", username);
        restCaller.setUrlSuffix("sys_user");
        String userResponse = restCaller.getMethod(null, userReqParams, null);
        Properties syncAttributes = new Properties();
        Map<String, Object> attributes = new HashMap<String, Object>();
        String userSysId = null;
        if (StringUtils.isNotBlank(userResponse))
        {
            List<Map<String, String>> userList = StringUtils.jsonArrayStringToList("result", userResponse);
            if (userList != null && userList.size() > 0)
            {
                // interested in only first record. actually expect one record only
                Map<String, String> userMap = userList.get(0);

                /*
                 * Not using sys_user.source anymore to save user and sso type on server side
                 * if(!userMap.isEmpty())
                 * {
                 * Map<String, String> sourceProps = new HashMap<String, String>();
                 * 
                 * sourceProps.put(Constants.HTTP_REQUEST_SSO_TYPE, "ServiceNow");
                 * sourceProps.put(Constants.HTTP_REQUEST_USERNAME, username);
                 * 
                 * userMap.put("source", StringUtils.mapToJson(sourceProps));
                 * }
                 */

                for (String key : userMap.keySet())
                {
                    if ("sys_id".equalsIgnoreCase(key)) userSysId = userMap.get(key);
                    if ("first_name".equalsIgnoreCase(key)) syncAttributes.setProperty("firstname", "first_name");
                    if ("last_name".equalsIgnoreCase(key)) syncAttributes.setProperty("lastname", "last_name");
                    if ("user_password".equalsIgnoreCase(key)) syncAttributes.setProperty("password", "user_password");
                    if ("title".equalsIgnoreCase(key)) syncAttributes.setProperty("title", "title");
                    if ("email".equalsIgnoreCase(key)) syncAttributes.setProperty("email", "email");
                    if ("phone".equalsIgnoreCase(key)) syncAttributes.setProperty("phone", "phone");
                    if ("mobile_phone".equalsIgnoreCase(key)) syncAttributes.setProperty("mobile", "mobile_phone");
                    // if("source".equalsIgnoreCase(key)) syncAttributes.setProperty("source", "source");

                    attributes.put(key, userMap.get(key));
                }
            }
        }

        // update assigned user groups from ServiceNow
        List<String> memberOf = getMemberOfServiceNow(restCaller, userSysId);

        // if this flag is true, than make sure that Resolve has atleast one of the LDAP/AD group
        if (grouprequired)
        {
            Log.auth.trace("Validate the ServiceNow groups if they exist in Resolve for user:" + username);
            try
            {
                ServiceAuthentication.validateIfGroupRequiredForUser(username, memberOf, SERVICENOW_AUTH_TYPE);
            }
            catch (Exception e)
            {
                Log.log.warn(e.getMessage(), e);
                throw e;
            }
        }

        // copy details from template "user" if does not exist
        Log.auth.trace("Cloning user with template");
        boolean cloned = ServiceHibernate.copyTemplateUser(username, null);

        if (cloned)
        {
            String syslogMessage = "Cloned ServiceNow SSO user " + username + " into Resolve";
            Log.syslog.info(SyslogFormatter.INSTANCE.formatAudit(InetAddress.getLocalHost().getHostAddress(), username, InetAddress.getLocalHost().getHostAddress(), syslogMessage));
        }

        Log.auth.trace("Assigning User Groups:" + memberOf);
        ServiceAuthentication.assignUserGroups(username, memberOf, SERVICENOW_AUTH_TYPE);

        // sync ldap user mapping
        if (syncAttributes != null)
        {
            Log.auth.trace("Syncing " + username + " attributes for Authentication system of type " + SERVICENOW_AUTH_TYPE);
            syncUserAttributes(username, attributes, syncAttributes, null, null);
        }

        return username;
    } // initializeUser

    private static List<String> getMemberOfServiceNow(RestCaller restCaller, String userSysId) throws Exception
    {
        List<String> result = new ArrayList<String>();

        try
        {
            // get all the groups from ServiceNow
            Map<String, String> userGroupReqParams = new HashMap<String, String>();
            userGroupReqParams.put("user.sys_id", userSysId);
            userGroupReqParams.put("sysparm_display_value", "true");
            restCaller.setUrlSuffix("sys_user_grmember");
            String userGroupResponse = restCaller.getMethod(null, userGroupReqParams, null);

            List<Map<String, String>> userGroupData = StringUtils.jsonArrayStringToList("result", userGroupResponse);
            if (userGroupData != null && userGroupData.size() > 0)
            {
                for (Map<String, String> userGroupMap : userGroupData)
                {
                    for (String key : userGroupMap.keySet())
                    {
                        if ("group".equalsIgnoreCase(key))
                        {
                            JSONObject rootJObj = StringUtils.stringToJSONObject(userGroupMap.get(key));
                            result.add((String) rootJObj.get("display_value"));
                            if (Log.log.isTraceEnabled())
                            {
                                Log.log.trace("Group from ServiceNow :" + (String) rootJObj.get("display_value"));
                            }
                        }
                    }
                }
            }
        }
        catch (Exception e)
        {
            Log.log.error(e.getMessage(), e);
            throw e;
        }

        return result;
    }

    @SuppressWarnings("unchecked")
    private static AuthenticateResult search(String username)
    {
        boolean result = false;
        boolean grouprequired = false;
        ERR errCode = null;

        Log.auth.trace("Start of Authentication.search for user:" + username);
        if (StringUtils.isNotEmpty(username))
        {
            // normal user
            if (UserUtils.isLockedOut(username) == false)
            {
                if (RSContext.getMain().getActiveDirectory() != null)
                {
                    Map<String, Object> attributes = null;
                    Properties syncProperties = null;

                    String org_id = "";
                    grouprequired = RSContext.getMain().getConfigActiveDirectory().isGrouprequired();
                    String domainId = getDomain(username);
                    if (StringUtils.isNotEmpty(domainId))
                    {
                        if (ServiceAuthentication.isActiveDirectoryDomainExist(domainId))
                        {
                            ConfigActiveDirectoryVO vo = ServiceAuthentication.getActiveDirectoryFor(domainId);
                            if (vo != null)
                            {
                                if (vo.getBelongsToOrganization() != null)
                                {
                                    org_id = vo.getBelongsToOrganization().getSys_id();
                                }
                                attributes = authenticateRemoteActiveDirectoryUsingBindDN(username, vo);
                                syncProperties = StringUtils.stringToProperties(vo.getUProperties());
                            }
                        }
                    }

                    if (attributes == null)
                    {
                        // activedirectory user
                        attributes = RSContext.getMain().getActiveDirectory().authenticateUsingBindDN(username);
                        syncProperties = RSContext.getMain().getActiveDirectory().getSyncProperties();
                    }

                    if (attributes != null)
                    {
                        Log.auth.trace("initializeUser user with attributes:" + StringUtils.mapToString(attributes));
                        try
                        {
                            username = initializeUser(username, org_id, domainId, attributes, syncProperties, grouprequired, "AD");
                            result = true;
                        }
                        catch (Exception e)
                        {
                        	String msg = String.format("Error in initializing Windows authenticated user: %s in Resolve. %s", 
     							   					   username, (StringUtils.isNotBlank(e.getMessage()) ? 
     							   							   							 e.getMessage() : ""));

                        	if (StringUtils.isNotBlank(e.getMessage()) && 
                        	    (ERR.E10022.getMessage().equals(e.getMessage()) || 
                        	     ERR.E10023.getMessage().equals(e.getMessage()))) {
                        		Log.auth.info(msg);
                        		errCode = ERR.getERRFromMessage(e.getMessage());
                        	} else {
                        		Log.auth.error(msg, e);
                        	}
                        }
                    }
                    else
                    {
                        Log.log.warn("AD Authentication for Windows user " + username + " in domain " + domainId + " failed.");
                        Log.auth.warn("AD Authentication for Windows user " + username + " in domain " + domainId + " failed.");
                        result = false;
                    }
                }

                if (!result && errCode == null)
                {
                    Log.log.info("Search for Windows user " + username + " in AD failed.");
                    Log.auth.info("Search for Windows user " + username + " in AD failed.");
                }
            }

            if (result == false && errCode == null)
            {
                ServiceAuthentication.updateLoginFailure(username);
            }
        }

        return new AuthenticateResult(result, "Windows", errCode);
    } // search

    private static void syncUserAttributes(String username, Map<String, Object> attributes, Properties properties, String domain, String org_id)
    {
        try
        {
            UsersVO userVO = ServiceHibernate.getUserNoCache(username);
            Log.auth.trace("Roles for user in Auth:" + userVO.getUserRoles());
            Log.auth.trace("Groups for user in Auth:" + userVO.getUserGroups());

            if (userVO != null)
            {
                // set organziation
                if (StringUtils.isNotEmpty(org_id))
                {
                    userVO.setBelongsToOrganization(new OrganizationVO(org_id));
                }

                if (properties != null)
                {
                    for (String key : properties.stringPropertyNames())
                    {
                        // TODO - need to change this to reflection rather than
                        String propertyKey = properties.getProperty(key).toLowerCase();
                        Object objValue = attributes.get(propertyKey);
                        if (!(objValue instanceof String)) // only allowing strings currently
                            continue;

                        String value = (String) objValue;

                        if (key.equalsIgnoreCase("firstname"))
                        {
                            userVO.setUFirstName(value);
                        }
                        else if (key.equalsIgnoreCase("lastname"))
                        {
                            userVO.setULastName(value);
                        }
                        else if (key.equalsIgnoreCase("password"))
                        {
                            userVO.setUUserP_assword(value);
                        }
                        else if (key.equalsIgnoreCase("title"))
                        {
                            userVO.setUTitle(value);
                        }
                        else if (key.equalsIgnoreCase("email"))
                        {
                            // set value from LDAP/AD
                            if (StringUtils.isNotEmpty(value))
                            {
                                userVO.setUEmail(value);
                            }
                            else
                            {
                                // generate email if not currently defined
                                if (StringUtils.isEmpty(userVO.getUEmail()))
                                {
                                    // username had domain alread
                                    if (username.indexOf('@') > 0)
                                    {
                                        userVO.setUEmail(username);
                                    }

                                    // generate username from domain username@domain
                                    else if (StringUtils.isNotEmpty(domain))
                                    {
                                        userVO.setUEmail(username + "@" + domain);
                                    }
                                }
                            }
                        }
                        else if (key.equalsIgnoreCase("phone"))
                        {
                            userVO.setUHomePhone(value);
                        }
                        else if (key.equalsIgnoreCase("mobile"))
                        {
                            userVO.setUMobilePhone(value);
                        }
                        else if (key.equalsIgnoreCase("source"))
                        {
                            userVO.setUSource(value);
                        }
                        else
                        {
                            Log.log.warn("Unknown LDAP map property: " + key);
                        }
                    } // end of for loop
                }

                // update the user
                UserUtils.saveUser(userVO, userVO.getUUserName());
            }
        }
        catch (Throwable e)
        {
            Log.log.error("Unable to sync LDAP/AD user attributes. " + e.getMessage(), e);
        }

    } // syncUserAttributes

    public static String getURL(HttpServletRequest request)
    {
        String result = null;

        String uri = request.getRequestURI();
        String queryString = request.getQueryString();

        if (uri.startsWith("/resolve"))
        {
            uri = uri.substring(8);
        }

        if (queryString == null)
        {
            try
            {
                result = URLEncoder.encode(uri, StandardCharsets.UTF_8.name());
            }
            catch (UnsupportedEncodingException e)
            {
                Log.auth.error(e.getMessage(), e);
            }
        }
        else
        {
            try
            {
                result = URLEncoder.encode(uri + "?", StandardCharsets.UTF_8.name()) + queryString;
            }
            catch (UnsupportedEncodingException e)
            {
                Log.auth.error(e.getMessage(), e);
            }
        }

        // HttpServletRequest.getQueryStting() returns encoded query string. No need to double encode it.
//        try
//        {
//            result = URLEncoder.encode(result, StandardCharsets.UTF_8.name());
//        }
//        catch (UnsupportedEncodingException e)
//        {
//            Log.log.error(e.getMessage(), e);
//        }
        
        Log.auth.trace("getURL() returning : " + sanitizeUrl(result));
        
        return result;
    } // getURL

    public static String getDecodedURL(HttpServletRequest request)
    {
        String result = null;

        String uri = request.getRequestURI();
        String queryString = request.getQueryString();

        String decodedQueryString = null;
        
        if (StringUtils.isNotBlank(queryString))
        {
            try
            {
                decodedQueryString = URLDecoder.decode(queryString, StandardCharsets.UTF_8.name());
            }
            catch (UnsupportedEncodingException e)
            {
                Log.auth.warn("Error " + e.getLocalizedMessage() + " occured while decoding query string " +
                              queryString + " using " + StandardCharsets.UTF_8.name() + " character set," +
                              " returning encoded query string!!!", e);
                decodedQueryString = queryString;
            }
        }
        else
        {
            decodedQueryString = "";
        }
        
        if (uri.startsWith("/resolve"))
        {
            uri = uri.substring(8);
        }

        if (StringUtils.isBlank(decodedQueryString))
        {
            result = uri;
        }
        else
        {
            result = uri + "?" + decodedQueryString;
        }
        
        return result;
    } // getURL
    
    public static String getAuthToken(HttpServletRequest request, String ssoType)
    {
        String result = null;

        Collection<Cookie> cookies = getCookies(request, Constants.AUTH_TOKEN, ssoType);

        if (cookies != null && !cookies.isEmpty())
        {
            Cookie cookie = cookies.iterator().next();

            if (cookie != null)
            {
                result = CryptUtils.decryptToken(cookie.getValue());
            }
        }

        return result;
    } // getAuthToken

    private static Collection<Cookie> getAuthCookies(HttpServletRequest request, String ssoType)
    {
        return getCookies(request, Constants.AUTH_TOKEN, ssoType);
    } // getAuthCookie

    public static Collection<Cookie> getCookies(HttpServletRequest request, String name, String ssoType)
    {
        Collection<Cookie> result = new ArrayList<Cookie>();
        // boolean found = false;

        String cookieName = name + (StringUtils.isNotBlank(ssoType) ? "-" + ssoType.toUpperCase() : "");

        Cookie[] cookies = request.getCookies();
        if (cookies != null)
        {
            for (int i = 0; /* !found && */ i < cookies.length; i++)
            {
                if (cookies[i].getName().equals(cookieName))
                {
                    cookies[i].setHttpOnly(true);
                    result.add(cookies[i]);
                }
            }
        }
        return result;
    } // getCookie

    public static String setAuthToken(HttpServletRequest request, HttpServletResponse response, String username, String host, String ssoType, long sessionTimeout, String ssoSessionId, String password, String iFrameId, String sSOServiceProvider, Map<String, String[]> mockSSOSrvcData)
    {
        AuthSession authSession = new AuthSession();
        String tz = request.getParameter(TIMEZONE_STR);// coming in from the login page.
        if (StringUtils.isBlank(tz))
        {
            tz = (String) request.getAttribute(TIMEZONE_STR);
        }

        authSession.setUsername(username);
        if (host.equalsIgnoreCase("0:0:0:0:0:0:0:1"))
        {
            authSession.setHost(".app.localhost");
        }
        else
        {
            authSession.setHost(host);
        }
        authSession.setTimezone(tz);
        authSession.setSSOType(ssoType);
        authSession.setSessionTimeout(sessionTimeout);
        authSession.setSessionId(request.getSession().getId());

        if (StringUtils.isNotBlank(ssoSessionId))
        {
            Log.auth.trace("Setting SSO Session Id " + ssoSessionId + " into AuthSession");
            authSession.setSSOSessionId(ssoSessionId);
        }

        String reqURLHostName = "";
        String reqURLAuthority = "";

        try
        {
            URL reqURL = new URL(request.getRequestURL().toString());
            reqURLHostName = reqURL.getHost();
            reqURLAuthority = reqURL.getAuthority();
        }
        catch (MalformedURLException e)
        {
            Log.log.error(e.getMessage(), e);
        }

        authSession.setClientReqURLHostName(reqURLHostName);
        authSession.setReqURLAuthority(StringUtils.substringBefore(reqURLAuthority, ":"));

        if (StringUtils.isNotBlank(password) && ((com.resolve.rsview.main.ConfigGeneral) RSContext.getMain().configGeneral).isSaveUserCredentials())
        {
            try
            {
                authSession.setUserCredentials(password);
            }
            catch (Exception e)
            {
                Log.log.error("Error " + e.getMessage() + " while saving user credentials in session info.", e);
            }
        }
        
        if (StringUtils.isNotBlank(iFrameId))
        {
            Log.auth.trace("Setting iframe Id " + iFrameId + " into AuthSession");
            authSession.setIFrameId(iFrameId);
        }
        
        authSession.setUpdateTimeForSession(request.getSession().getId(), System.currentTimeMillis());
        
        if (StringUtils.isNotBlank(sSOServiceProvider))
        {
            authSession.setSSOServiceProvider(sSOServiceProvider);
            
            if (mockSSOSrvcData != null && !mockSSOSrvcData.isEmpty())
            {
                authSession.setMockSSOSrvcDataForSession(request.getSession().getId(), mockSSOSrvcData);
            }
        }
        
        return updateAuthToken(request, response, authSession);
    }

    public static String setAuthToken(HttpServletRequest request, HttpServletResponse response, String username, String host, String ssoType, long sessionTimeout, String password, String iFrameId)
    {
        return setAuthToken(request, response, username, host, ssoType, sessionTimeout, "", password, iFrameId, null, null);
    } // setAuthToken

    public static String setSSOServiceAuthToken(HttpServletRequest request, HttpServletResponse response, String username, String host, String ssoType, long sessionTimeout, String password, String iFrameId, String sSOServiceProvider, Map<String, String[]> mockSSOSrvcData)
    {
        return setAuthToken(request, response, username, host, ssoType, sessionTimeout, "", password, iFrameId, sSOServiceProvider, mockSSOSrvcData);
    } // setAuthToken
    
    private static String updateAuthToken(HttpServletRequest request, HttpServletResponse response, AuthSession authSession)
    {
        final String createTime = (String) request.getAttribute(Constants.HTTP_ATTRIBUTE_CREATETIME);

        // generate token value
        String token = generateToken(authSession.getUsername(), authSession.getHost(), authSession.getReqURLAuthority(), createTime, authSession.getSSOType(), authSession.getIFrameId());

        String oldToken = null;
        // String oldTokenSuffix = null;

        Collection<Cookie> cookies = getAuthCookies(request, authSession.getSSOType());

        boolean isNew = true;

        if (cookies != null && !cookies.isEmpty())
        {
            for (Cookie cookie : cookies)
            {
                if (cookie != null)
                {
                    // Get the token of existing cookie which will be old and needs to be removed from auth session cache
                    String oldEncryptedToken = cookie.getValue();
                    oldToken = CryptUtils.decryptToken(oldEncryptedToken);

                    Log.auth.trace("Found old cookie " + cookie.getName() + " with value " + oldToken + " with MaxAge = " + cookie.getMaxAge());
                    
                    if (oldToken.contains(authSession.getReqURLAuthority()))
                    {
                    	if(Log.auth.isTraceEnabled())
                    		Log.auth.trace("Old cookie " + cookie.getName() + " with value " + oldToken + " and MaxAge = " + cookie.getMaxAge() + " will be removed as it contains Request URL Authority [" + authSession.getReqURLAuthority() + "]");

                        // Delete the cookie by setting MaxAge to 0

                        cookie.setMaxAge(0);
                        // DefaultHTTPUtilities dhttp = new DefaultHTTPUtilities();
                        // ESAPI.httpUtilities().addCookie(response, cookie);
                        ESAPI.httpUtilities().addCookie(response, cookie);
                        // response.addCookie(cookie);
                        
                        isNew = false;
                    }
                }
            }
        }

        String cookieNameSuffix = null;
        
        if (StringUtils.isNotBlank(authSession.getSSOType()) || StringUtils.isNotBlank(authSession.getIFrameId()))
        {
            cookieNameSuffix = StringUtils.isNotBlank(authSession.getSSOType()) ? 
                               authSession.getSSOType() : authSession.getIFrameId();
        }
        
        String cookieName = Constants.AUTH_TOKEN + (StringUtils.isNotBlank(cookieNameSuffix) ? "-" + cookieNameSuffix.toUpperCase() : "");

        Cookie cookie = new Cookie(cookieName, CryptUtils.encryptToken(token));
        cookie.setPath("/");
        cookie.setValue(CryptUtils.encryptToken(token));
        cookie.setHttpOnly(true);

        Log.auth.trace((isNew ? "New" : "Updated") + " " + cookieName + " : " + token);

        // If we're using ssl, then we want to make sure that the session cookie is only sent over an ssl connection
        if (request.isSecure())
        {
            cookie.setSecure(true);
        }

        // maxAge with properties

        long effectiveSessionTimeOut = (PropertiesUtil.getPropertyBoolean("session.fixed.timeout") ?
                                        PropertiesUtil.getPropertyLong(Constants.SESSION_MAXALLOWED) :
                                        PropertiesUtil.getPropertyLong(Constants.SESSION_TIMEOUT)) * 1000;
        
        if (StringUtils.isNotBlank(authSession.getSSOType()))
        {
            effectiveSessionTimeOut = authSession.getSessionTimeout();
        }
        
        Log.auth.trace("Effective Session Time Out for setting Cookie Max Age is " + ((effectiveSessionTimeOut / 1000) * 2) + " seconds");
        
        if (effectiveSessionTimeOut < 720000)
        {
            if (effectiveSessionTimeOut <= 0)
            {
                if (PropertiesUtil.getPropertyBoolean("session.fixed.timeout"))
                {
                    if (PropertiesUtil.getPropertyLong(Constants.SESSION_TIMEOUT) >
                        PropertiesUtil.getPropertyLong(Constants.SESSION_MAXALLOWED))
                    {
                        effectiveSessionTimeOut = PropertiesUtil.getPropertyLong(Constants.SESSION_TIMEOUT) * 1000;
                    }
                }
            }
            
            if (effectiveSessionTimeOut < 720000)
            {
                effectiveSessionTimeOut = 720000;
            }
        }
        
        if (effectiveSessionTimeOut > ((Integer.MAX_VALUE - 1) / 2))
        {
            effectiveSessionTimeOut = (Integer.MAX_VALUE - 1) / 2;
        }
        
        Log.auth.trace("Cookie " + cookieName + " is set to expire (i.e MaxAge) after " + ((effectiveSessionTimeOut / 1000) * 2) + " seconds.");
        
        cookie.setMaxAge(/*-1*/((int) (effectiveSessionTimeOut / 1000)) * 2);
        
        // set Domain excluding first www or SSOType in request URL Authority

        if (StringUtils.isNotBlank(domain))
        {
            if (domain.startsWith("."))
            {
				cookie.setDomain(domain);
			}
			else
			{
				cookie.setDomain("." + domain);
			}
            Log.auth.trace((isNew ? "New" : "Updated") + " " + cookieName + " : domain is set to [" + domain + "]");
        }

        // DefaultHTTPUtilities dhttp = new DefaultHTTPUtilities();
        // dhttp.addCookie(response, cookie);
        ESAPI.httpUtilities().addCookie(response, cookie);
        // response.addCookie(cookie);

        // update token in cache
        if(Log.auth.isTraceEnabled())
        	Log.auth.trace("Setting token: " + token + " into AuthSession: " + authSession);
        authSession.setToken(token);

        getAuthTokenCache().put(token, authSession);
        if(Log.auth.isDebugEnabled())
        	Log.auth.debug("Added AuthSession: " + authSession + " into autheticated session cache");

        // Remove it at the time of logout
        //removeIFrameIdFromIFramePreLoginSessionCache(request);
        
        updateStats();

        return token;
    } // updateAuthToken

    private static void clearIFramePreLoginSessionCache(AuthSession authSess)
    {
        if (StringUtils.isBlank(authSess.getIFrameId()) || 
            StringUtils.isBlank(authSess.getSSOServiceProvider()))
        {
            Map<String, Long> clntSessToUpdTime = authSess.getClientSessionIdToUpdateTime();
            
            if (clntSessToUpdTime != null && !clntSessToUpdTime.isEmpty())
            {
                for(String sessId : clntSessToUpdTime.keySet())
                {
                    getIFramePreLoginSessionCache().remove(sessId);
                }
            }
            
            if (StringUtils.isNotBlank(authSess.getSessionId()))
            {
                getIFramePreLoginSessionCache().remove(authSess.getSessionId());
            }
        }
    }
    
    public static AuthSession removeAuthToken(String token)
    {
        AuthSession removedAuthSess = null;
        
        if (getAuthTokenCache().containsKey(token))
        {
            AuthSession authSess = getAuthTokenCache().get(token);
            
            Log.auth.debug("Removing Auth Session: " + authSess + " for Auth Token: " + token + " from authenticated session cache.");
            
            clearIFramePreLoginSessionCache(authSess);
            
            removedAuthSess = getAuthTokenCache().remove(token);
            
            if (removedAuthSess != null && removedAuthSess.getClntSessIdToMockSSOSrvcData() != null &&
                !removedAuthSess.getClntSessIdToMockSSOSrvcData().isEmpty())
            {
                for (String sessionId : removedAuthSess.getClntSessIdToMockSSOSrvcData().keySet())
                {
                    getMockSSOSrvcDataCache().put(sessionId, 
                                                  removedAuthSess.getMockSSOSrvcDataForSession(sessionId));
                    Log.auth.trace("Added Mock SSO Service Data " + 
                                   StringUtils.mapToString(removedAuthSess.getMockSSOSrvcDataForSession(sessionId), 
                                   "=", ", ") +
                                   " for Session Id : " + sessionId + " to Mock SSO Service Cache for Removed Auth Token!!!");
                }
            }
            
            // remove wiki user history
            // WikiSessionManager.getInstance().removeSessionContextFor(token);

            // update stats
            updateStats();
        }
        
        return removedAuthSess;
    } // removeAuthToken
    
    public static AuthSession removeAuthTokenForSession(String token, HttpServletRequest request)
    {
        AuthSession removedAuthSess = null;
        
        if (((AuthTokenMap)getAuthTokenCache()).containsKeyForSession(request.getSession().getId(), token))
        {
            Log.auth.debug("Removing client auth session for session id " + request.getSession().getId() + " from Auth Session: " + 
                           ((AuthTokenMap) getAuthTokenCache()).getForSessionId(request.getSession().getId(), token) + 
                           " for Auth Token: " + token + " from authenticated session cache.");
            
            removedAuthSess = ((AuthTokenMap) getAuthTokenCache()).removeForSession(request.getSession().getId(), token);

            if (removedAuthSess != null &&
                (StringUtils.isBlank(removedAuthSess.getIFrameId()) || 
                 StringUtils.isBlank(removedAuthSess.getSSOServiceProvider())))
            {
                getIFramePreLoginSessionCache().remove(request.getSession().getId());
            }
            // remove wiki user history
            // WikiSessionManager.getInstance().removeSessionContextFor(token);

            // update stats
            updateStats();
        }
        
        return removedAuthSess;
    } // removeAuthTokenForSession
    
    private static void updateStats()
    {
        // update max
        long size = getAuthTokenCache().size();
        if (size > sessionMax.get())
        {
            sessionMax.set(size);
        }

        // update active
        sessionActive.set(size);

        Log.auth.debug("# of Active Sessions: " + sessionActive.get() + ", Max Sessions: " + sessionMax.get());
    } // updateStats

    private static String generateToken(String username, String host, String reqURLAuthority, String time, String ssoType, String iFrameId)
    {
        final String TOKEN_SEPARATOR = "/";

        return username + TOKEN_SEPARATOR + time + TOKEN_SEPARATOR + host + TOKEN_SEPARATOR + reqURLAuthority + 
               (StringUtils.isNotBlank(ssoType) ? TOKEN_SEPARATOR + ssoType : "") + 
               (StringUtils.isBlank(ssoType) && StringUtils.isNotBlank(iFrameId) ? TOKEN_SEPARATOR + iFrameId : "");
    } // generateToken

    public static String printAuthTokenCache()
    {
        String result = "";

        int count = 0;
        Set<String> unique = new HashSet<String>();

        for (AuthSession session : authTokenCache.values())
        {
            result += session.toString() + "\n";
            unique.add(session.getUsername() + "/" + session.getHost());
            count++;
        }
        result += "\n";
        result += "count: " + count + " unique: " + unique.size() + "\n";

        System.out.println(result);
        return result;
    } // printAuthTokenCache

    public static String getSessionTable(boolean isCheckbox)
    {
        String result = "";

        if (authTokenCache != null)
        {
            // display sessions
            List<String> removeTokenList = new ArrayList<String>();
            for (AuthSession authSession : authTokenCache.values())
            {
                long effectiveSessionTimeout = (PropertiesUtil.getPropertyBoolean("session.fixed.timeout") ?
                                                PropertiesUtil.getPropertyLong(Constants.SESSION_MAXALLOWED) :
                                                PropertiesUtil.getPropertyLong(Constants.SESSION_TIMEOUT)) * 1000; 
   
                if (effectiveSessionTimeout < 720000)
                {
                    effectiveSessionTimeout = 720000;
                }
                
                long duration = System.currentTimeMillis() - 
                                (PropertiesUtil.getPropertyBoolean("session.fixed.timeout") ? 
                                 authSession.getCreateTime() : authSession.getUpdateTime());
                
                if (duration > effectiveSessionTimeout)
                {
                    removeTokenList.add(authSession.getToken());
                }
                else
                {
                    if (isCheckbox)
                    {
                        result += "<input type=\"checkbox\" name=\"id\" value=\"" + authSession.getToken() + "\" /> |";
                    }
                    result += authSession.getUsername() + "|" + authSession.getHost() + "|" + new Date(authSession.getCreateTime()) + "|" + new Date(authSession.getUpdateTime()) + "|" + duration + "\n";
                }
            }

            // remove expired tokens
            for (String token : removeTokenList)
            {
                removeAuthToken(token);
            }
        }

        return result;
    } // getSessionTable

    public static List<AuthSession> getActiveUserSessions()
    {
        List<AuthSession> sessions = new ArrayList<AuthSession>();

        if (authTokenCache != null)
        {
            List<String> removeTokenList = new ArrayList<String>();
            
            long effectiveSessionTimeout = (PropertiesUtil.getPropertyBoolean("session.fixed.timeout") ?
                                            PropertiesUtil.getPropertyLong(Constants.SESSION_MAXALLOWED) :
                                            PropertiesUtil.getPropertyLong(Constants.SESSION_TIMEOUT)) * 1000; 
            
            if (effectiveSessionTimeout < 720000)
            {
                effectiveSessionTimeout = 720000;
            }
            
            for (AuthSession authSession : authTokenCache.values())
            {
                long duration = System.currentTimeMillis() - 
                                (PropertiesUtil.getPropertyBoolean("session.fixed.timeout") ? 
                                 authSession.getCreateTime() : authSession.getUpdateTime());
                
                if (duration > effectiveSessionTimeout)
                {
                    removeTokenList.add(authSession.getToken());
                }
                else
                {
                    sessions.add(authSession);
                }
            }

            // remove expired tokens
            for (String token : removeTokenList)
            {
                removeAuthToken(token);
            }
        }

        return sessions;
    }

    /**
     * Removes all inactive user sessions from authentication token cache
     */
    public static void removeInactiveUserSessions()
    {
        if (authTokenCache != null)
        {
            long sessMaxAllowed = PropertiesUtil.getPropertyLong(Constants.SESSION_MAXALLOWED);

            /*
            if (PropertiesUtil.getPropertyBoolean("session.fixed.timeout") && 
                sessMaxAllowed < 720 )
            {
                sessMaxAllowed = 720;
            }*/
            
            /*if (sessMaxAllowed > 0)
            {*/
                List<String> removeTokenList = new ArrayList<String>();
                Set<String> removeIFrameSessions = new HashSet<String>();
                
                for (AuthSession authSession : authTokenCache.values())
                {
                    long duration = (System.currentTimeMillis() - 
                                     (PropertiesUtil.getPropertyBoolean("session.fixed.timeout") ? 
                                      authSession.getCreateTime() : authSession.getUpdateTime()));
                    
                    //if (duration > authSession.getSessionTimeout())
                    /*
                     * As we used to read a cached session.timeout value, any update to the actual system property
                     * wasn't coming into effect until use logs out and logs back in.
                     * Now reading the session.timeout property at runtime.
                     */
                    long sessionTimeout = PropertiesUtil.getPropertyLong(Constants.SESSION_TIMEOUT);
                    /*
                    if (sessionTimeout < 720)
                    {
                        sessionTimeout = 720;
                    }*/
                    
                    long effectiveMaxSessionTimeOut = authSession.getSessionTimeout() / 1000;
                    
                    if (StringUtils.isBlank(authSession.getSSOType()))
                    {
                        effectiveMaxSessionTimeOut = PropertiesUtil.getPropertyBoolean("session.fixed.timeout") ? 
                                                     sessMaxAllowed : sessionTimeout;
                    }
                    
                    if (effectiveMaxSessionTimeOut < 720)
                    {
                        if (effectiveMaxSessionTimeOut <= 0)
                        {
                            if (PropertiesUtil.getPropertyBoolean("session.fixed.timeout"))
                            {
                                if (PropertiesUtil.getPropertyLong(Constants.SESSION_TIMEOUT) >
                                    PropertiesUtil.getPropertyLong(Constants.SESSION_MAXALLOWED))
                                {
                                    effectiveMaxSessionTimeOut = PropertiesUtil.getPropertyLong(Constants.SESSION_TIMEOUT);
                                }
                            }
                        }
                        
                        if (effectiveMaxSessionTimeOut < 720)
                        {
                            effectiveMaxSessionTimeOut = 720;
                        }
                    }
                    
                    if (effectiveMaxSessionTimeOut > (((Integer.MAX_VALUE - 1) / 2) / 1000))
                    {
                        effectiveMaxSessionTimeOut = ((Integer.MAX_VALUE - 1) / 2) / 1000;
                    }
                    
                    if (duration > /*(sessMaxAllowed >= sessionTimeout ? sessMaxAllowed : sessionTimeout)*/effectiveMaxSessionTimeOut * 1000)
                    {
                    	if(Log.auth.isTraceEnabled())
                    		Log.auth.trace("Adding AuthSession " + authSession + " to list of auth sessions to be cleaned up");
                        removeTokenList.add(authSession.getToken());
                        
                        if (StringUtils.isNotBlank(authSession.getSessionId()))
                        {
                            removeIFrameSessions.add(authSession.getSessionId());
                        }
                    }
                }

                // remove expired tokens
                for (String token : removeTokenList)
                {
                    removeAuthToken(token);
                }
                
                // remove the iframe sessions which do not have corresponding auth session
                
                Set<String> iFrameSessionIdWithAuthSession = new HashSet<String>();
                
                for (AuthSession authSession : authTokenCache.values())
                {
                    if (StringUtils.isNotBlank(authSession.getSessionId()))
                    {
                        iFrameSessionIdWithAuthSession.add(authSession.getSessionId());
                    }
                }
                
                for (String iFrameSessionId : getIFramePreLoginSessionCache().keySet())
                {
                    if (!removeIFrameSessions.contains(iFrameSessionId) && 
                        !iFrameSessionIdWithAuthSession.contains(iFrameSessionId))
                    {
                        IFramePreLoginSession iFramePreLoginSess = getIFramePreLoginSessionCache().get(iFrameSessionId);
                        
                        if (iFramePreLoginSess != null && 
                            ((System.currentTimeMillis() - iFramePreLoginSess.getCreateTime()) > 
                             (Long.parseLong(PropertiesUtil.getPropertyString(Constants.PROPERTY_SESSION_TIMEOUT)) * 1000)))
                        {
                            Log.auth.trace("Adding iFrame Pre-Login Session with session Id " + iFrameSessionId + 
                                           " to list of iFrame Pre-Login Sessions to be cleaned up");
                            
                            removeIFrameSessions.add(iFrameSessionId);
                        }
                    }
                }
                
                for (String iFrameSessionId : removeIFrameSessions)
                {
                    getIFramePreLoginSessionCache().remove(iFrameSessionId);
                }
            /*}
            else
            {
                Log.auth.trace("Session cleanup disabled (" + Constants.SESSION_MAXALLOWED + " = " + sessMaxAllowed + ")");
            }*/
        }
    }

    public static long getSessionActive()
    {
        return sessionActive.get();
    } // getSessionActive

    public static long getSessionMaxActive()
    {
        return sessionMax.get();
    } // getSessionMaxActive

    private static SpnegoPrincipal checkWindowsPassword(HttpServletRequest request, SpnegoHttpServletResponse spnegoResponse) throws ServletException, IOException
    {
        SpnegoPrincipal result = null;

        final HttpServletRequest httpRequest = (HttpServletRequest) request;
        // final SpnegoHttpServletResponse spnegoResponse = new SpnegoHttpServletResponse((HttpServletResponse) response);

        // client/caller principal
        try
        {
            result = authenticator.authenticate(httpRequest, spnegoResponse);
        }
        catch (GSSException e)
        {
            Log.log.warn("HTTP Authorization Header: " + httpRequest.getHeader(SpnegoConstants.AUTHZ_HEADER), e);
            throw new ServletException(e);
        }
        catch (IOException e)
        {
            Log.log.warn(e.getMessage(), e);
            throw e;
        }

        return result;
    } // checkWindowsPassword

    public static String getDomain(String loginName)
    {
        String domain = null;

        int pos = loginName.indexOf('\\');
        if (pos > 0)
        {
            domain = loginName.substring(0, pos);
        }
        else
        {
            pos = loginName.indexOf('@');
            if (pos > 0)
            {
                domain = loginName.substring(pos + 1, loginName.length());
            }
        }

        return domain;
    } // getDomain

    public static String getUserNameWithoutDomain(String loginName)
    {
        String userNameWithoutDomain = loginName;

        int pos = loginName.indexOf('\\');
        if (pos > 0)
        {
            userNameWithoutDomain = loginName.substring(pos + 1, loginName.length()); // loginName.substring(0, pos);
        }
        else
        {
            pos = loginName.indexOf('@');
            if (pos > 0)
            {
                userNameWithoutDomain = loginName.substring(0, pos); // loginName.substring(pos + 1, loginName.length());
            }
        }

        return userNameWithoutDomain;
    } // getUserNameWithoutDomain

    @SuppressWarnings({ "unchecked", "rawtypes", "static-access" })
    private static Map authenticateRemoteLDAP(String username, String password, ConfigLDAPVO vo)
    {
        Map result = null;

        long timeout = 30000;
        String gateway = vo.getUGateway();

        // convert vo to params
        Map params = new HashMap();
        params.put(Constants.AUTH_USERNAME, username);
        params.put(Constants.AUTH_P_ASSWORD, password);
        params.put(Constants.AUTH_DOMAIN, vo.getUDomain());
        params.put(Constants.AUTH_LDAP_MODE, vo.getUMode());
        params.put(Constants.AUTH_LDAP_HOST, vo.getUIpAddress());
        params.put(Constants.AUTH_LDAP_PORT, vo.getUPort());
        params.put(Constants.AUTH_LDAP_BINDDN, vo.getUBindDn());
        params.put(Constants.AUTH_LDAP_BINDP_ASSWORD, vo.getUBindPassword());
        params.put(Constants.AUTH_LDAP_BASEDNLIST, vo.getUBaseDNList());
        params.put(Constants.AUTH_LDAP_SSL, vo.getUSsl());
        params.put(Constants.AUTH_LDAP_CRYPTTYPE, vo.getUCryptType());
        params.put(Constants.AUTH_LDAP_CRYPTPREFIX, vo.getUCryptPrefix());
        params.put(Constants.AUTH_LDAP_PROPERTIES, vo.getUProperties());

        params.put(Constants.AUTH_LDAP_VERSION, vo.getUVersion());
        params.put(Constants.AUTH_LDAP_DEFAULT_DOMAIN, vo.getUDefaultDomain());
        params.put(Constants.AUTH_LDAP_UID_ATTRIBUTE, vo.getUUidAttribute());
        params.put(Constants.AUTH_LDAP_P_ASSWORD_ATTRIBUTE, vo.getUPasswordAttribute());
        params.put(Constants.AUTH_LDAP_GROUPREQUIRED, vo.getGroupRequired());

        try
        {
            result = RSContext.getMain().getESB().call(gateway, "MAuth.authenticateLDAP", params, timeout);
        }
        catch (Exception e)
        {
            Log.log.error(e.getMessage(), e);
        }

        return result;
    } // authenticateRemoteLDAP

    @SuppressWarnings({ "unchecked", "rawtypes", "static-access" })
    private static Map authenticateRemoteActiveDirectory(String username, String password, ConfigActiveDirectoryVO vo)
    {
        Map result = null;

        long timeout = 30000;
        String gateway = vo.getUGateway();

        // convert vo to params
        Map params = new HashMap();
        params.put(Constants.AUTH_USERNAME, username);
        params.put(Constants.AUTH_P_ASSWORD, password);
        params.put(Constants.AUTH_DOMAIN, vo.getUDomain());
        params.put(Constants.AUTH_AD_MODE, vo.getUMode());
        params.put(Constants.AUTH_AD_HOST, vo.getUIpAddress());
        params.put(Constants.AUTH_AD_PORT, vo.getUPort());
        params.put(Constants.AUTH_AD_BINDDN, vo.getUBindDn());
        params.put(Constants.AUTH_AD_BINDP_ASSWORD, vo.getUBindDn());
        params.put(Constants.AUTH_AD_BASEDNLIST, vo.getUBaseDNList());
        params.put(Constants.AUTH_AD_SSL, vo.getUSsl());
        params.put(Constants.AUTH_AD_PROPERTIES, vo.getUProperties());

        params.put(Constants.AUTH_AD_VERSION, vo.getUVersion());
        params.put(Constants.AUTH_AD_DEFAULT_DOMAIN, vo.getUDefaultDomain());
        params.put(Constants.AUTH_AD_UID_ATTRIBUTE, vo.getUUidAttribute());
        params.put(Constants.AUTH_AD_GROUPREQUIRED, vo.getGroupRequired());
        try
        {
            result = RSContext.getMain().getESB().call(gateway, "MAuth.authenticateActiveDirectory", params, timeout);
        }
        catch (Exception e)
        {
            Log.log.error(e.getMessage(), e);
        }

        return result;
    } // authenticateRemoteActiveDirectory

    @SuppressWarnings({ "unchecked", "rawtypes", "static-access" })
    private static Map authenticateRemoteActiveDirectoryUsingBindDN(String username, ConfigActiveDirectoryVO vo)
    {
        Map result = null;

        long timeout = 30000;
        String gateway = vo.getUGateway();

        // convert vo to params
        Map params = new HashMap();
        params.put(Constants.AUTH_USERNAME, username);
        params.put(Constants.AUTH_P_ASSWORD, "");
        params.put(Constants.AUTH_DOMAIN, vo.getUDomain());
        params.put(Constants.AUTH_AD_MODE, ActiveDirectory.AUTH_MODE_USERNAME);
        params.put(Constants.AUTH_AD_HOST, vo.getUIpAddress());
        params.put(Constants.AUTH_AD_PORT, vo.getUPort());
        params.put(Constants.AUTH_AD_BINDDN, vo.getUBindDn());
        params.put(Constants.AUTH_AD_BINDP_ASSWORD, vo.getUBindDn());
        params.put(Constants.AUTH_AD_BASEDNLIST, vo.getUBaseDNList());
        params.put(Constants.AUTH_AD_SSL, vo.getUSsl());
        params.put(Constants.AUTH_AD_PROPERTIES, vo.getUProperties());

        params.put(Constants.AUTH_AD_VERSION, vo.getUVersion());
        params.put(Constants.AUTH_AD_DEFAULT_DOMAIN, vo.getUDefaultDomain());
        params.put(Constants.AUTH_AD_UID_ATTRIBUTE, vo.getUUidAttribute());
       
        try
        {
            result = RSContext.getMain().getESB().call(gateway, "MAuth.authenticateActiveDirectory", params, timeout);
        }
        catch (Exception e)
        {
            Log.log.error(e.getMessage(), e);
        }

        return result;
    } // authenticateRemoteActiveDirectoryUsingBindDN

    /**
     * boolean hasAccess = canUserAccessThisUrl(request);
     * 
     * @param request
     * @return
     */
    private static boolean canUserAccessThisUrl(HttpServletRequest request)
    {
        boolean hasAccess = true;

        String uri = request.getRequestURI(); // /resolve/service/sysproperties/getAllProperties
        String username = (String) request.getAttribute(Constants.HTTP_REQUEST_USERNAME);

        Log.auth.trace("canUserAccessThisUrl(" + uri + ", " + username + ")");
        
        try
        {
        	// First check A-RBAC 
        	
        	String arbac = RBACUtils.getApplicationRBACForURI(uri);
        	
        	if (StringUtils.isNotBlank(arbac)) {
        		hasAccess = com.resolve.services.hibernate.util.UserUtils.getUserfunctionPermission(username, arbac, null);
        		Log.auth.trace("Access to URI " + uri + " is controlled by A-RBAC " + arbac + 
        					   " which for user " + username + " is set to " + hasAccess + ".");
        	} else {
        		// Check legacy application (URL) security
	            String controller = getControllerName(uri); // sysproperties
	            if (StringUtils.isNotBlank(controller) && StringUtils.isNotBlank(username))
	            {
	                hasAccess = ServiceHibernate.doesUserHaveRightsForController(controller, username);
	            }
        	}
        }
        catch (Exception e)
        {
            Log.log.error("Error " + e.getMessage() + " in checking if user " + username + " has access to URI " + uri, e);
            Log.auth.error("Error " + e.getMessage() + " in checking if user " + username + " has access to URI " + uri, e);
        }
        return hasAccess;
    }

    /**
     * 
     * @param uri
     *            --> /resolve/service/sysproperties/getAllProperties , /resolve/service/login
     * @return
     */
    private static String getControllerName(String uri)
    {
        String result = "";

        if (StringUtils.isNotEmpty(uri))
        {
            if (uri.indexOf("service") > -1)
            {
                result = uri.substring(uri.indexOf("service") + "service".length()); /// /sysproperties/getAllProperties
            }
            else if (uri.startsWith("/resolve"))/// resolve/kibana/app/dashboards/guided.json
            {
                result = uri.substring("/resolve".length()); /// /kibana/app/dashboards/guided.json
            }
        }

        return result;
    }

    @SuppressWarnings("unchecked")
    private static List<String> getMemberOf(Map<String, Object> attributes, Properties syncAttributes)
    {
        List<String> result = null;
        if (attributes != null)
        {
            String memberofKey = syncAttributes != null ? syncAttributes.getProperty(Constants.MEMBEROF) : null;
            if (StringUtils.isBlank(memberofKey))
            {
                memberofKey = Constants.MEMBEROF_DEFAULT;
            }

            // anything coming in from LDAP is in lowercase
            memberofKey = memberofKey.toLowerCase();

            Object obj = attributes.get(memberofKey);
            if (obj instanceof String)
            {
                result = StringUtils.stringToList((String) obj, AuthConstants.COMMA_DELIMITER.getValue());
            }
            else
            {
                result = (List<String>) obj;
            }
        }

        Log.log.debug("User groups (memberOf): " + result);
        if (result == null)
        {
            // this indicates that it will remove all the external groups assigned to this user
            result = new ArrayList<String>();
        }

        return result;
    }

    public static void test_initializeUser()
    {
        String username = "jeet.ldap@resolvetest-2.com";
        String org_id = "";
        String domainId = "resolvetest-2.com";

        Map<String, Object> attributes = new HashMap<String, Object>();
        attributes.put("uid", "jeet.ldap");
        attributes.put("mail", "test@gmail.com");
        attributes.put("title", "");
        attributes.put("sn", "Marwah");
        attributes.put("userpassword", "{md5}eF4lT09DIn3wKUXARVnbrw==");
        attributes.put("cn", "jeet.ldap");
        attributes.put("givenname", "Jeet M");
        attributes.put("dn", "uid=jeet.ldap,ou=users,dc=resolvetest-2,dc=com");
        attributes.put("telephonenumber", "0987654321");
        attributes.put("memberof", "Dev Group,Dev Group");
        attributes.put("mobile", "1111111121");
        attributes.put("objectclass", "inetOrgPerson");

        Properties syncAttributes = new Properties();
        syncAttributes.put("phone", "telephonenumber");
        syncAttributes.put("mobile", "mobile");
        syncAttributes.put("email", "mail");
        syncAttributes.put("lastname", "sn");
        syncAttributes.put("firstname", "givenname");
        syncAttributes.put("title", "title");

        String result;
        try
        {
            result = initializeUser(username, org_id, domainId, attributes, syncAttributes, false, "local");
            Log.log.debug("result -->" + result);
        }
        catch (Exception e)
        {
            Log.log.warn("error in initializing the user:" + username, e);
        }
    }

    public static Collection<Cookie> getAuthTokenCookies(HttpServletRequest request, String ssoTypeSuffix, HttpServletResponse response)
    {
        Collection<Cookie> result = new ArrayList<Cookie>();

        Cookie[] cookies = request.getCookies();

        String reqURLAuthority = "";

        try
        {
            reqURLAuthority = new URL(request.getRequestURL().toString()).getAuthority();
            reqURLAuthority = StringUtils.substringBefore(reqURLAuthority, ":");
            Log.auth.trace("getAuthTokenCookies for Request URL Authority [" + reqURLAuthority + "], request is secure " + request.isSecure());
        }
        catch (MalformedURLException e)
        {
            Log.log.error(e.getMessage(), e);
        }

        if (cookies != null)
        {
            for (int i = 0; i < cookies.length; i++)
            {
                if (Log.auth.isTraceEnabled() && cookies[i].getName().startsWith(Constants.AUTH_TOKEN))
                {
                    Log.auth.trace(cookies[i].getName() + ": decypted value [" + CryptUtils.decryptToken(cookies[i].getValue())+ "]");
                }
                
                if (StringUtils.isNotBlank(ssoTypeSuffix) && cookies[i].getName().equals(Constants.AUTH_TOKEN + "-" + ssoTypeSuffix.toUpperCase()))
                {
                    Log.auth.trace("Auth Token Cookie " + cookies[i].getName() + " for " + ssoTypeSuffix + " contains " + CryptUtils.decryptToken(cookies[i].getValue()));
                    result.add(cookies[i]);
                }
                else if (StringUtils.isBlank(ssoTypeSuffix) && cookies[i].getName().equals(Constants.AUTH_TOKEN))
                {
                	String token = CryptUtils.decryptToken(cookies[i].getValue());
                	
                	if (StringUtils.isNotBlank(reqURLAuthority) && token.contains(reqURLAuthority)) {
                        Log.auth.trace("Auth Token Cookie " + cookies[i].getName() + " contains " + CryptUtils.decryptToken(cookies[i].getValue()));
                        result.add(cookies[i]);
                	}
                }
            }
        }

        return result;
    } // getAuthTokenCookies

    @SuppressWarnings({ "unchecked", "rawtypes", "static-access" })
    private static Map authenticateRemoteRADIUS(String username, String password, ConfigRADIUSVO vo)
    {
        Map result = null;

        long timeout = 30000;
        String gateway = vo.getUGateway();

        // convert vo to params
        Map params = new HashMap();

        params.put(Constants.AUTH_USERNAME, username);
        params.put(Constants.AUTH_P_ASSWORD, password);
        params.put(Constants.AUTH_DOMAIN, vo.getUDomain());
        params.put(Constants.AUTH_RADIUS_PRIMARYHOST, vo.getUPrimaryHost());

        if (StringUtils.isNotBlank(vo.getUSecondaryHost()))
        {
            params.put(Constants.AUTH_RADIUS_SECONDARYHOST, vo.getUSecondaryHost());
        }

        params.put(Constants.AUTH_RADIUS_AUTHPORT, vo.getUAuthPort());
        params.put(Constants.AUTH_RADIUS_ACCTPORT, vo.getUAcctPort());
        params.put(Constants.AUTH_RADIUS_AUTHPROTOCOL, vo.getUAuthProtocol());
        params.put(Constants.AUTH_RADIUS_SHAREDSECRET, vo.getUSharedSecret());

        try
        {
            result = RSContext.getMain().getESB().call(gateway, "MRADIUS.authenticateUser", params, timeout);
        }
        catch (Exception e)
        {
            Log.log.error("Error " + e.getMessage() + " in authenticating " + username + " with RADIUS authentication.", e);
        }

        return result;
    } // authenticateRemoteRADIUS
    
    static class AuthTokenMap implements ConcurrentMap<String, AuthSession>
    {
        ConcurrentMap<String, AuthSession> partialToken2Session;
        ConcurrentMap<String, Long> partialTokenToTokenTimeStamp;
        
        public AuthTokenMap()
        {
            partialToken2Session = new ConcurrentHashMap<String, AuthSession>();
            partialTokenToTokenTimeStamp = new ConcurrentHashMap<String, Long>();
        }

        @Override
        public int size()
        {
            return partialToken2Session.size();
        }

        @Override
        public boolean isEmpty()
        {
            return partialToken2Session.isEmpty();
        }

        private String getPartialToken(String fullToken)
        {
            String partialToken = fullToken;
            String[] tokenParts = fullToken.trim().split("/");
            
            if (tokenParts != null && tokenParts.length >= 3)
            {
                String tokenTS = tokenParts[1].trim();
                
                try
                {
                    Long.parseLong(tokenTS);
                    
                    partialToken = tokenParts[0];
                    
                    for (int i = 2; i < tokenParts.length; i++)
                    {
                        partialToken += "/" + tokenParts[i];
                    }
                }
                catch(NumberFormatException nfe)
                {
                    //
                }
            }
            
            return partialToken;
        }
        
        private Long getTokenTimeStamp(String fullToken)
        {
            Long tokenTimeStamp = 0L;
            String[] tokenParts = fullToken.trim().split("/");
            
            if (tokenParts != null && tokenParts.length >= 3)
            {
                String tokenTS = tokenParts[1].trim();
                
                try
                {
                    tokenTimeStamp = Long.parseLong(tokenTS);
                }
                catch(NumberFormatException nfe)
                {
                    //
                }
            }
            
            return tokenTimeStamp;
        }
        
        @Override
        public boolean containsKey(Object key)
        {
            if ((key == null) || (!(key instanceof String)) || ((String)key).trim().isEmpty())
            {
                return false;
            }
            
            if (partialToken2Session.containsKey(key))
            {
                return true;
            }
            
            return partialToken2Session.containsKey(getPartialToken((String)key)) && 
                   partialTokenToTokenTimeStamp.containsKey(getPartialToken((String)key));
        }
        
        public boolean containsKeyForSession(String sessionId, String key)
        {
            if (StringUtils.isBlank(sessionId) || StringUtils.isBlank(key))
            {
                return false;
            }
                        
            return partialToken2Session.containsKey(getPartialToken(key)) && 
                   partialTokenToTokenTimeStamp.containsKey(getPartialToken(key)) &&
                   ((AuthSession)partialToken2Session.get(getPartialToken(key))).getUpdateTimeForSession(sessionId) > 0l;
        }
        
        @Override
        public boolean containsValue(Object value)
        {
            return partialToken2Session.containsValue(value);
        }

        @Override
        public AuthSession get(Object key)
        {
            if ((key == null) || (!(key instanceof String)) || ((String)key).trim().isEmpty())
            {
                return null;
            }
            
            if (partialToken2Session.get(getPartialToken((String)key)) != null &&
                partialTokenToTokenTimeStamp.containsKey(getPartialToken((String)key)) &&
                partialTokenToTokenTimeStamp.get(getPartialToken((String)key)).compareTo(getTokenTimeStamp((String)key)) >= 0)
            {
                return partialToken2Session.get(getPartialToken((String)key));
            }
            else
            {
                return null;
            }
        }

        public AuthSession getForSessionId(String sessionId, String key)
        {
            if (StringUtils.isBlank(sessionId) || StringUtils.isBlank(key))
            {
                return null;
            }
                        
            if (partialToken2Session.get(getPartialToken(key)) != null &&
                partialTokenToTokenTimeStamp.containsKey(getPartialToken(key)) &&
                partialTokenToTokenTimeStamp.get(getPartialToken(key)).compareTo(getTokenTimeStamp(key)) >= 0 &&
                ((AuthSession)partialToken2Session.get(getPartialToken(key))).getUpdateTimeForSession(sessionId) > 0l)
            {
                Log.auth.trace("AuthTokenMap.getForSessionId(" + sessionId + ", " + key + ") returning " + (AuthSession)partialToken2Session.get(getPartialToken(key)));
                return partialToken2Session.get(getPartialToken(key));
            }
            else
            {
                Log.auth.trace("AuthTokenMap.getForSessionId(" + sessionId + ", " + key + ") returning null");
                return null;
            }
        }
        
        @Override
        public AuthSession put(String key, AuthSession value)
        {
            if ((key == null) || key.trim().isEmpty())
            {
                return null;
            }
            
            if (value == null || !(value instanceof AuthSession))
            {
                return null;
            }
            
            if (partialToken2Session.get(getPartialToken(key)) != null &&
                partialTokenToTokenTimeStamp.containsKey(getPartialToken(key)))
            {
                Log.auth.trace("Updating partialToken2Session Map value for key " + getPartialToken(key) + " from " + 
                                partialToken2Session.get(getPartialToken(key)) + " with " + value + ".");
                if (partialTokenToTokenTimeStamp.get(getPartialToken(key)).compareTo(getTokenTimeStamp(key)) < 0)
                {
                    Log.auth.trace("Updating partialTokenToTokenTimeStamp Map value for key " + getPartialToken(key) + " from " + 
                                   partialTokenToTokenTimeStamp.get(getPartialToken(key)) + " to " + getTokenTimeStamp(key) + ".");
                    partialTokenToTokenTimeStamp.put(getPartialToken(key), getTokenTimeStamp(key));
                }
                
                AuthSession updatedAuthSession = (AuthSession)partialToken2Session.get(getPartialToken(key));
                updatedAuthSession.updateClientSessionIdToUpdateTime(value);
                
                Log.auth.trace("Updating partialToken2Session Map value for key " + getPartialToken(key) + " to updated Auth Session " + updatedAuthSession);
                
                return partialToken2Session.put(getPartialToken(key), updatedAuthSession);
            }
            else
            {
                partialTokenToTokenTimeStamp.put(getPartialToken(key), getTokenTimeStamp(key));
                Log.auth.trace("Adding entry into partialToken2Session Map for key " + getPartialToken(key) + " and value " + value + ".");
                return partialToken2Session.put(getPartialToken(key), value);
            }
        }

        @Override
        public AuthSession remove(Object key)
        {
            if ((key == null) || (!(key instanceof String)) || ((String)key).trim().isEmpty())
            {
                return null;
            }
            
            if (partialToken2Session.get(getPartialToken((String)key)) != null &&
                partialTokenToTokenTimeStamp.containsKey(getPartialToken((String)key)) &&
                partialTokenToTokenTimeStamp.get(getPartialToken((String)key)).compareTo(getTokenTimeStamp((String)key)) == 0)
            {
                partialTokenToTokenTimeStamp.remove(getPartialToken((String)key));
                return partialToken2Session.remove(getPartialToken((String)key));
            }
            else
            {
                return null;
            }
        }
        
        public AuthSession removeForSession(String sessionId, String key)
        {
            if ((key == null) || (!(key instanceof String)) || ((String)key).trim().isEmpty() ||
                StringUtils.isBlank(sessionId))
            {
                return null;
            }
            
            Log.auth.trace("Remove entries for key " + key + " (" + getPartialToken(key) + 
                           ") for Session with id " + sessionId + " from partialToken2Session and partialTokenToTokenTimeStamp maps" + sessionId);
            
            if (partialToken2Session.get(getPartialToken(key)) != null &&
                partialTokenToTokenTimeStamp.containsKey(getPartialToken(key)))
            {
                AuthSession authSess = partialToken2Session.get(getPartialToken(key));
                
                authSess.removeClientSession(sessionId);
                
                if (!authSess.hasClientSessions())
                {
                    Log.auth.trace("Map entries from partialToken2Session and partialTokenToTokenTimeStamp maps for key " +
                                   key + " and partial key " + getPartialToken(key) + 
                                   " removed as AuthSession has no associated client sessions.");
                    partialTokenToTokenTimeStamp.remove(getPartialToken(key));
                    return partialToken2Session.remove(getPartialToken(key));
                }
                else
                {
                    Log.auth.trace("Map entries from partialToken2Session and partialTokenToTokenTimeStamp maps for key " +
                                   key + " and partial key " + getPartialToken(key) + 
                                   " not removed as AuthSession still has associated client sessions.");
                    return null;
                }
            }
            else
            {
                return null;
            }
        }
        
        @SuppressWarnings("rawtypes")
        @Override
        public void putAll(Map m)
        {
            throw new RuntimeException("Not supported.");
        }

        @Override
        public void clear()
        {
            partialToken2Session.clear();
            partialTokenToTokenTimeStamp.clear();
        }

        @SuppressWarnings({ "unchecked", "rawtypes" })
        @Override
        public Set keySet()
        {
            return partialToken2Session.keySet();
        }

        @Override
        public Collection<AuthSession> values()
        {
            return partialToken2Session.values();
        }

        @SuppressWarnings({ "unchecked", "rawtypes" })
        @Override
        public Set entrySet()
        {
            return partialToken2Session.entrySet();
        }

        @Override
        public AuthSession putIfAbsent(String key, AuthSession value)
        {
            throw new RuntimeException("Not supported.");
        }

        @Override
        public boolean remove(Object key, Object value)
        {
            throw new RuntimeException("Not supported.");
        }

        @Override
        public boolean replace(String key, AuthSession oldValue, AuthSession newValue)
        {
            throw new RuntimeException("Not supported.");
        }

        @Override
        public AuthSession replace(String key, AuthSession value)
        {
            throw new RuntimeException("Not supported.");
        }
    }
    
    public static ConcurrentMap<String, IFramePreLoginSession> getIFramePreLoginSessionCache()
    {
        if (iFramePreLoginSessionCache == null)
        {
            iFramePreLoginSessionCache = new ConcurrentHashMap<String, IFramePreLoginSession>();
        }

        return iFramePreLoginSessionCache;
    } // getIFramePreLoginSessionCache
    
    public static void cacheIFramePreLoginSession(IFramePreLoginSession iFramePreLoginSess)
    {
        if (iFramePreLoginSess != null && 
            StringUtils.isNotBlank(iFramePreLoginSess.getSessionId()) &&
            StringUtils.isNotBlank(iFramePreLoginSess.getIFrameId()))
            {
                getIFramePreLoginSessionCache().put(iFramePreLoginSess.getSessionId(), 
                                                    iFramePreLoginSess);
                Log.auth.trace("Added " + iFramePreLoginSess.getIFrameId() + " for " + 
                               iFramePreLoginSess.getSessionId() + " with referer host as " + 
                               iFramePreLoginSess.getRefererHost() + 
                               " in IFramePreLoginSessionCache, Cache Count is " + 
                               getIFramePreLoginSessionCache().size());
            }
    }
    
    public static String getIFrameIdFromIFramePreLoginSessionCache(HttpServletRequest request)
    {
        String iFrameId = null;
        
        if (getIFramePreLoginSessionCache().containsKey(request.getSession().getId()))
        {
            IFramePreLoginSession iFramePreLoginSess = getIFramePreLoginSessionCache().get(request.getSession().getId());
            iFrameId = iFramePreLoginSess.getIFrameId();
            Log.auth.trace("Found " + iFrameId + " for " + request.getSession().getId() +
                           " from IFramePreLoginSessionCache, Cache Count is " + 
                           getIFramePreLoginSessionCache().size());
        }
        
        return iFrameId;
    }
    
    public static void removeIFrameIdFromIFramePreLoginSessionCache(HttpServletRequest request)
    {
        if (getIFramePreLoginSessionCache().containsKey(request.getSession().getId()))
        {
            getIFramePreLoginSessionCache().remove(request.getSession().getId());
            Log.auth.trace("Removed " + request.getSession().getId() +
                            " from IFramePreLoginSessionCache, Cache Count is " + 
                            getIFramePreLoginSessionCache().size());
        }
    }

    public static void regenerateSession(HttpServletRequest request) {
        HttpSession session = request.getSession(false);
        if (session!=null && !session.isNew()) {
            String cachedPreLoginIFrameId = null;
            String cachedRefererHost = null;
            
            // Check for session in pre-login iframe sessions
            if (getIFramePreLoginSessionCache().containsKey(session.getId()))
            {
                IFramePreLoginSession iframePreLogSess = getIFramePreLoginSessionCache().get(session.getId());
                
                if (iframePreLogSess != null && iframePreLogSess.getSessionId().equals(session.getId()))
                {
                    Log.auth.trace("regenerateSession(): Found cached pre login iframeId " + iframePreLogSess.getIFrameId() +
                                   " for session id " + iframePreLogSess.getSessionId());
                    
                    cachedPreLoginIFrameId = iframePreLogSess.getIFrameId();
                    cachedRefererHost = iframePreLogSess.getRefererHost();
                    
                    getIFramePreLoginSessionCache().remove(session.getId(), iframePreLogSess);
                }
            }
            
            session.invalidate();
            request.getSession(true);
            
            if (StringUtils.isNotBlank(cachedPreLoginIFrameId))
            {
                Log.auth.trace("regenerateSession(): Adding cached pre login iframeId " + cachedPreLoginIFrameId +
                                " for newly generated session id " + request.getSession().getId());
                
                cacheIFramePreLoginSession(new IFramePreLoginSession(request.getSession().getId(), cachedPreLoginIFrameId, cachedRefererHost));
            }
        }
    }
    
    public static String getHost(String urlString) {
        URL url = getURL(urlString);
        return url == null ? "" : url.getHost(); 
    }
    
    public static String getProtocolAndAuthority(String urlString) {
        URL url = getURL(urlString);
        return url == null ? "" : url.getProtocol() + "://" +  url.getAuthority(); 
    }
    
    private static String getAuthority(String urlString) {
        URL url = getURL(urlString);
        return url == null ? "" : url.getAuthority(); 
    }

    private static URL getURL(String urlString) {
        if (StringUtils.isNotEmpty(urlString)) {
            try {
                return new URL(urlString);
            } catch (MalformedURLException e) {
                Log.auth.error(String.format("Error: %s, %s", e.getMessage(), urlString), e);
            }
        }

        return null;
    }

    private static boolean isAllowedReferer(HttpServletRequest request)
    {
        if (PropertiesUtil.getPropertyBoolean(Constants.DISABLE_HTTP_IFRAME_INTEGRATION_SECUIRTY))
        {
            return true;
        }
        
        Log.auth.trace(HttpHeaders.REFERER + " in Request [" + (StringUtils.isNotBlank(request.getHeader(HttpHeaders.REFERER)) ? request.getHeader(HttpHeaders.REFERER) : "NO " + HttpHeaders.REFERER) + "]");
        String referer = getHost(request.getHeader(HttpHeaders.REFERER));

        if (StringUtils.isNotBlank(request.getHeader(HttpHeaders.REFERER)))
        {
            Log.auth.trace(HttpHeaders.HOST + " in " + HttpHeaders.REFERER + " [" + request.getHeader(HttpHeaders.REFERER) + "] is [" + (StringUtils.isNotBlank(referer) ? referer : "NO " + HttpHeaders.HOST) + "]");
        }

        Log.auth.trace(HttpHeaders.HOST + " in Request [" + (StringUtils.isNotBlank(getHost(request.getRequestURL().toString())) ? getHost(request.getRequestURL().toString()) : "NO " + HttpHeaders.HOST) + "]");

        String hostName = getHost(request.getRequestURL().toString());
        // boolean sameReferer = StringUtils.isBlank(referer) ||
        // referer.equals(hostName);
        
        boolean ssoReferer = false;
        
        String iframeid = getIFrameId(request);
        
        if (StringUtils.isNotBlank(iframeid))
        {
            String ssoDomain = getSSODomain(iframeid);
            
            String ssoHost = "NO SSO " + HttpHeaders.HOST;
            
            if (StringUtils.isNotBlank(ssoDomain))
            {
                ssoHost = ssoDomain.startsWith(ALLOWED_DOMAIN_WILD_CARD_CHARACTER) ? ssoDomain : getHost(ssoDomain);
                
                Log.auth.trace("SSO Host: [" + ssoHost + "]");
                
                ssoReferer = StringUtils.isNotBlank(referer) && 
                             
                             /* host in first/initial request should be different than host in referer 
                              * with IFRAMEID set, spcifically so with wild card in allowed URLs 
                              * Example allowed URL entry is <MYIFRAMEID>=*.mysite.com
                              * with Resolve site URL myresolve.mysite.com and external sites 
                              * integrating Resolve using i-frame from myresolveiframe.mysite.com
                              * Requests with host name in Referer and URL same and IFRAMEID set to MYIFRAMEID
                              * should be rejected as IFRAMEID configured host and Referer host do not match
                              * even though Referer host and request host matches.
                              */
                                            
                             (getIFramePreLoginSessionCache().containsKey(request.getSession().getId()) ? 
                              (StringUtils.isNotBlank(referer) && referer.equals(hostName) &&
                               getIFrameIdFromIFramePreLoginSessionCache(request).equalsIgnoreCase(iframeid)) :
                              (StringUtils.isNotBlank(referer) && !hostName.equals(referer) &&
                               (ssoHost.startsWith(ALLOWED_DOMAIN_WILD_CARD_CHARACTER) ? 
                                referer.contains(ssoHost.substring(1)) : referer.equals(ssoHost))));
            }
            
            Log.auth.trace("IFRAMEID:[" + iframeid + "], " + HttpHeaders.REFERER + ":[" +
                           referer + "], [" + ssoHost + "], ssoReferer = " + ssoReferer);
        }
        
        
        boolean sameReferer = StringUtils.isBlank(iframeid) ? StringUtils.isNotBlank(referer) && referer.equals(hostName) : ssoReferer;
        
        if (Log.auth.isTraceEnabled() && StringUtils.isBlank(iframeid))
        {
            Log.auth.trace(HttpHeaders.REFERER + ":[" + referer + "], [" + hostName + "], sameReferer = " + sameReferer);
        }

        boolean csrfGuardWhiteListed = false;
        
        // URIs white listed in CSRF Guard properties can be landing pages without Referer
        
        if (!sameReferer)
        {
            CsrfGuard csrfGuard = CsrfGuard.getInstance();
            csrfGuardWhiteListed = !csrfGuard.isProtectedPageAndMethod(request);
            Log.auth.trace("CSRF Guard isProtectedPageAndMethod(" + request.getRequestURI() + 
                           ", " + request.getMethod() + ") returned " + csrfGuardWhiteListed);
            
            if (csrfGuardWhiteListed && StringUtils.isNotBlank(iframeid) && StringUtils.isBlank(referer))
            {
                Log.auth.warn("Page " + request.getRequestURI() + " and method " + request.getMethod() + 
                              " is protected but i-frame id is present and first/initial request is missing HTTP header " + 
                              HttpHeaders.REFERER + ", re-setting page and method protected!!!");
                 csrfGuardWhiteListed = false;
            }
            else if (csrfGuardWhiteListed && StringUtils.isNotBlank(iframeid) && 
                     StringUtils.isNotBlank(referer) &&
                     getIFramePreLoginSessionCache().containsKey(request.getSession().getId()) &&
                     !getIFrameIdFromIFramePreLoginSessionCache(request).equalsIgnoreCase(iframeid))
            {
                Log.auth.warn("Page " + request.getRequestURI() + " and method " + request.getMethod() + 
                              " is protected but i-frame id in i-frame Pre-Login cached i-frame id " + 
                              getIFrameIdFromIFramePreLoginSessionCache(request) +
                              " does not match i-frame id " + iframeid + "from request, re-setting page and method protected!!!");
                csrfGuardWhiteListed = false;
            }
            else if (csrfGuardWhiteListed && StringUtils.isBlank(iframeid) &&
                     StringUtils.isNotBlank(getIFrameIdFromIFramePreLoginSessionCache(request)))
            {
                Log.auth.warn("Page " + request.getRequestURI() + " and method " + request.getMethod() + 
                              " is protected but i-frame Pre-Login cache already contains i-frame id " +
                              getIFrameIdFromIFramePreLoginSessionCache(request) + " for session id " +
                              request.getSession().getId() + "!!!");
                csrfGuardWhiteListed = false;
            }
        }
        
        boolean serviceNoReferer = false; // Browsers which do not set referer
                                          // on redirect request (FF and IE)
                                          // and URIs which are not white listed 
                                          // in CSRF Guard properties file

        if (!sameReferer && !csrfGuardWhiteListed && 
            (request.getRequestURI().equals(AuthConstants.RESOLVE_SERVICE_LOGIN_URL.getValue())))
        {
            Log.auth.trace("Request URI is [" + request.getRequestURI() + "]");
            serviceNoReferer = true;
            Log.auth.trace("serviceLoginNoReferer is " + serviceNoReferer);
            
            if (serviceNoReferer && StringUtils.isNotBlank(iframeid) && StringUtils.isBlank(referer))
            {
                Log.auth.warn("Page URI " + request.getRequestURI() + " does not require " + HttpHeaders.REFERER + 
                              " but i-frame id is present and first/initial request is missing HTTP header " + 
                              HttpHeaders.REFERER + ", re-setting does not require HTTP header " + HttpHeaders.REFERER + "!!!");
                serviceNoReferer = false;
            }
            else if (serviceNoReferer && StringUtils.isNotBlank(iframeid) && 
                     StringUtils.isNotBlank(referer) &&
                     getIFramePreLoginSessionCache().containsKey(request.getSession().getId()) &&
                     !getIFrameIdFromIFramePreLoginSessionCache(request).equalsIgnoreCase(iframeid))
            {
                Log.auth.warn("Page URI " + request.getRequestURI() + "  does not require " + HttpHeaders.REFERER +  
                              " but i-frame id in i-frame Pre-Login cached i-frame id " + 
                              getIFrameIdFromIFramePreLoginSessionCache(request) +
                              " does not match i-frame id " + iframeid + "from request, re-setting does not require HTTP header " + 
                              HttpHeaders.REFERER + "!!!");
                serviceNoReferer = false;
            }
            else if (serviceNoReferer && StringUtils.isBlank(iframeid) &&
                     StringUtils.isNotBlank(getIFrameIdFromIFramePreLoginSessionCache(request)))
            {
                Log.auth.warn("Page URI " + request.getRequestURI() + "  does not require " + HttpHeaders.REFERER + 
                              " but i-frame Pre-Login cache already contains i-frame id " +
                              getIFrameIdFromIFramePreLoginSessionCache(request) + " for session id " +
                              request.getSession().getId() + "!!!");
                serviceNoReferer = false;
            }
        }

        Log.auth.trace("isAllowedReferer() returning " + (sameReferer || csrfGuardWhiteListed || serviceNoReferer));

        return sameReferer  || csrfGuardWhiteListed || serviceNoReferer;
    }

    private static String getSSODomain(String ssoName) {
        List<String> allowedDomainURLs = PropertiesUtil.getPropertyList(Constants.SECURITY_HTTP_IFRAME_INTEGRATION_ALLOWEDURLS);

		if (CollectionUtils.isNotEmpty(allowedDomainURLs)) {
			for (String allowedDomainURL : allowedDomainURLs) {
				allowedDomainURL = allowedDomainURL.trim().toLowerCase();
				if ((allowedDomainURL.startsWith(SSOTYPE_HOST_IDENTIFIER_PREFIX)
						|| allowedDomainURL.startsWith(IFRAME_HOST_IDENTIFIER_PREFIX))
						&& allowedDomainURL.contains(HOST_IDENTIFIER_SUFFIX)) {
					String hostIdentifierPrefix = allowedDomainURL.startsWith(SSOTYPE_HOST_IDENTIFIER_PREFIX)
							? SSOTYPE_HOST_IDENTIFIER_PREFIX : IFRAME_HOST_IDENTIFIER_PREFIX;

					String sso = StringUtils.substringBetween(allowedDomainURL, hostIdentifierPrefix,
							HOST_IDENTIFIER_SUFFIX);

					String ssoDomain = StringUtils.substringAfter(allowedDomainURL, HOST_IDENTIFIER_SUFFIX);
					
					if(sso.equals(ssoName)) {
						return ssoDomain;
					}
				}
			}
		}

		return "";
    }
    
    public static Pair<String, String> authenticateSiteMinder(String username, String[] groups)
    {
        String resolveUserName = null;
        
        // Phase II
        // UI to configure all SiteMinder HTTP headers (currently configured using system properties), user attributes to add/update from customers User Directory
        // into Resolve user. Also Org this user would belong to once Authentication & Authorization is tied to Org.
        // With Org all SiteMinder HTTP headers should be configurable per Org (if Org has one setup) or its parent Org and so on.
        
        Log.auth.trace("Into authenticate SiteMinder for user: " + username + ", groups: " + (groups.length == 1 ? groups[0] : ""));
        
        boolean groupRequired = PropertiesUtil.getPropertyBoolean(SITEMINDER_PROPERTY_PREFIX + SITEMINDER_GROUP_REQUIRED_PROPERTY_SUFFIX);
         
        Log.auth.trace("SiteMinder Group Required System Property " + SITEMINDER_PROPERTY_PREFIX + SITEMINDER_GROUP_REQUIRED_PROPERTY_SUFFIX + " is set to: " + groupRequired);
        
        String org_id = "";
        String domainId = getDomain(username);
        
        Log.auth.trace("Domain Id for " + username + (StringUtils.isNotBlank(domainId) ? "is " + domainId : " can not be found") + ".");

        if (StringUtils.isBlank(domainId) && StringUtils.isNotBlank(PropertiesUtil.getPropertyString("authentication.defaultdomain")))
        {
            /*
             * Set default domain if not specified by user and
             * system property siteminder.defaultdomain is set.
             */
            domainId = PropertiesUtil.getPropertyString(SITEMINDER_PROPERTY_PREFIX + SITEMINDER_DEFAULTDOMAIN_PROPERTY_SUFFIX);
        }
        
        boolean fallback = PropertiesUtil.getPropertyBoolean(SITEMINDER_PROPERTY_PREFIX + SITEMINDER_FALLBACK_PROPERTY_SUFFIX);
        
        Log.auth.trace("SiteMinder Fall Back to Resolve Authentication System Property " + SITEMINDER_PROPERTY_PREFIX + SITEMINDER_GROUP_REQUIRED_PROPERTY_SUFFIX + " is set to: " + fallback);
        
        Map<String, Object> attributes = new HashMap<String, Object>();
        
        if (groupRequired)
        {
            List<String> groupsList = Arrays.asList(groups);
            attributes.put(Constants.MEMBEROF_DEFAULT.toLowerCase(), /*StringUtils.arrayToString(*/groupsList/*, COMMA_DELIMITER)*/);
        }
        
        // Currently not syncing any user attributes received from SiteMinder into Resolve
        
        String errorMsg = null;
        
        try
        {
            resolveUserName = initializeUser(username, org_id, domainId, attributes, null, groupRequired, SITEMINDER_AUTH_TYPE);
            
        }
        catch(Exception e)
        {
        	String msg = String.format("Failed to create/update %s authenticated and authroized with external " +
        							   "Authentication & Authorization system of %s type. %s", 
        							   username, SITEMINDER_AUTH_TYPE, (StringUtils.isNotBlank(e.getMessage()) ? 
        									   							e.getMessage() : ""));

        	if (StringUtils.isNotBlank(e.getMessage()) && 
        		(ERR.E10022.getMessage().equals(e.getMessage()) || 
        		 ERR.E10023.getMessage().equals(e.getMessage()))) {
        		Log.auth.info(e.getMessage());
        		errorMsg = e.getMessage();
        	} else {
        		Log.auth.error(msg, e);
        		errorMsg = msg;
        	}
        }
        
        return Pair.of(resolveUserName, errorMsg);
    }
    
    public static ConcurrentMap<String, Map<String, String[]>> getMockSSOSrvcDataCache()
    {
        if (mockSSOSrvcDataCache == null)
        {
            mockSSOSrvcDataCache = new ConcurrentHashMap<String, Map<String, String[]>>();
        }

        return mockSSOSrvcDataCache;
    } // getMockSSOSrvcDataCache
    
    public static Map<String, String[]> getSiteMinderHeaders(HttpServletRequest request)
    {
        Map<String, String[]> siteMinderHeaders = new HashMap<String, String[]>();
        
        Map<String, String> siteMinderPropsToVal = getSiteMinderProperties();
        
        String siteMinderUserIdProp = PropertiesUtil.getPropertyBoolean(SITEMINDER_PROPERTY_PREFIX + 
        																SITEMINDER_MOCK_PROPERTY_SUFFIX) ?
                                      AuthConstants.RESOLVE_PARAM_AS_HEADER_HEADER_NAME_PREFIX.getValue() + 
                                      siteMinderPropsToVal.get(
                                    		  SITEMINDER_PROPERTY_PREFIX + SITEMINDER_USERID_PROPERTY_SUFFIX) :
                                      siteMinderPropsToVal.get(
                                    		  SITEMINDER_PROPERTY_PREFIX + SITEMINDER_USERID_PROPERTY_SUFFIX);

        String siteMinderGroupsProp = PropertiesUtil.getPropertyBoolean(SITEMINDER_PROPERTY_PREFIX + 
        																SITEMINDER_GROUP_REQUIRED_PROPERTY_SUFFIX) ?
                                      (PropertiesUtil.getPropertyBoolean(SITEMINDER_PROPERTY_PREFIX + 
                                    		  							 SITEMINDER_MOCK_PROPERTY_SUFFIX) ?
                                       AuthConstants.RESOLVE_PARAM_AS_HEADER_HEADER_NAME_PREFIX.getValue() + 
                                       siteMinderPropsToVal.get(
                                    		   SITEMINDER_PROPERTY_PREFIX + SITEMINDER_GROUP_PROPERTY_SUFFIX) :
                                       siteMinderPropsToVal.get(
                                    		   SITEMINDER_PROPERTY_PREFIX + SITEMINDER_GROUP_PROPERTY_SUFFIX)) : 
                                      null;
                                       
        String username = request.getHeader(siteMinderUserIdProp);
        
        if (StringUtils.isBlank(username) && getMockSSOSrvcDataCache().containsKey(request.getSession().getId()) &&
            PropertiesUtil.getPropertyBoolean(SITEMINDER_PROPERTY_PREFIX + SITEMINDER_MOCK_PROPERTY_SUFFIX) &&
            getMockSSOSrvcDataCache().get(request.getSession().getId()).get(siteMinderUserIdProp).length == 1)
        {
            username = getMockSSOSrvcDataCache().get(request.getSession().getId()).get(siteMinderUserIdProp)[0];
            Log.auth.trace("Retrieved SiteMinder Header " + siteMinderUserIdProp + " value " + username + " from Mock SSO Service Data Cache!!!");
        }
        
        Enumeration<String> groupsEnum = StringUtils.isNotBlank(siteMinderGroupsProp) ? request.getHeaders(siteMinderGroupsProp) : null;
        
        List<String> groupsArray = new ArrayList<String>();
        
        if (groupsEnum != null)
        {
            while(groupsEnum.hasMoreElements())
            {
                groupsArray.add(groupsEnum.nextElement());
            }
        }
        
        String[] groups = new String[groupsArray.size()];
        groups =  groupsArray.toArray(groups);
        
        if (StringUtils.isNotBlank(siteMinderGroupsProp) && (groups.length == 0) &&
            getMockSSOSrvcDataCache().containsKey(request.getSession().getId()) &&
            PropertiesUtil.getPropertyBoolean(SITEMINDER_PROPERTY_PREFIX + SITEMINDER_MOCK_PROPERTY_SUFFIX) &&
            getMockSSOSrvcDataCache().get(request.getSession().getId()).get(siteMinderGroupsProp).length >= 1)
        {
            groups = getMockSSOSrvcDataCache().get(request.getSession().getId()).get(siteMinderGroupsProp);
            Log.auth.trace("Retrieved SiteMinder Header " + siteMinderGroupsProp + " value " + groups + " from Mock SSO Service Data Cache!!!");
        }
        
        siteMinderHeaders.put(siteMinderUserIdProp, new String[]{username});
        
        if (groups.length >= 1)
        {
            siteMinderHeaders.put(siteMinderGroupsProp, groups);
        }
        
        if (PropertiesUtil.getPropertyBoolean(SITEMINDER_PROPERTY_PREFIX + SITEMINDER_MOCK_PROPERTY_SUFFIX))
        {
            Map<String, String[]> removedMockSSOSrvcData = getMockSSOSrvcDataCache().remove(request.getSession().getId());
            
            if (removedMockSSOSrvcData != null && !removedMockSSOSrvcData.isEmpty())
            {
                Log.auth.trace("Removed Mock SSO Service Data " + 
                                StringUtils.mapToString(removedMockSSOSrvcData, "=", ", ") +
                                " for Session Id : " + request.getSession().getId() + " from Mock SSO Service Cache!!!");
            }
        }
        
        return siteMinderHeaders;
    }
} // Authentication