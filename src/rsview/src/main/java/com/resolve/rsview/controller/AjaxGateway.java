/******************************************************************************
* (C) Copyright 2014
*
* Resolve Systems, LLC
*
* All rights reserved
* This software is distributed under license and may not be used, copied,
* modified, or distributed without the express written permission of
* Resolve Systems, LLC.
*
******************************************************************************/

package com.resolve.rsview.controller;

import java.io.IOException;
import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Properties;
import java.util.Set;
import java.util.StringTokenizer;
import java.util.TreeMap;
import java.util.TreeSet;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ConcurrentMap;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.collections.MapUtils;
import org.apache.commons.lang3.tuple.Pair;
import org.codehaus.jackson.map.DeserializationConfig.Feature;
import org.codehaus.jackson.map.ObjectMapper;
import org.owasp.esapi.ESAPI;
import org.springframework.stereotype.Controller;
import org.springframework.stereotype.Service;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.resolve.gateway.dto.GatewayDTO;
import com.resolve.persistence.model.EmailFilter;
import com.resolve.persistence.util.HibernateProxy;
import com.resolve.persistence.util.HibernateUtil;
import com.resolve.rsview.main.Main;
import com.resolve.rsview.main.RSContext;
import com.resolve.services.GatewayUtil;
import com.resolve.services.ServiceGateway;
import com.resolve.services.ServiceHibernate;
import com.resolve.services.hibernate.util.GatewayFilterUtil;
import com.resolve.services.hibernate.util.GatewayType;
import com.resolve.services.hibernate.util.MSGGatewayFilterUtil;
import com.resolve.services.hibernate.util.PullGatewayFilterUtil;
import com.resolve.services.hibernate.util.PushGatewayFilterUtil;
import com.resolve.services.hibernate.util.UserUtils;
import com.resolve.services.hibernate.vo.GatewayFilterVO;
import com.resolve.services.hibernate.vo.GatewayHealthCheckVO;
import com.resolve.services.hibernate.vo.GatewayVO;
import com.resolve.services.hibernate.vo.MSGGatewayFilterVO;
import com.resolve.services.hibernate.vo.PullGatewayFilterVO;
import com.resolve.services.hibernate.vo.PushGatewayFilterVO;
import com.resolve.services.hibernate.vo.ResolveBlueprintVO;
import com.resolve.services.interfaces.VO;
import com.resolve.services.vo.QueryDTO;
import com.resolve.services.vo.ResponseDTO;
import com.resolve.util.BeanUtil;
import com.resolve.util.Constants;
import com.resolve.util.Log;
import com.resolve.util.MappingAnnotation;
import com.resolve.util.StringUtils;

import net.sf.json.JSONException;
import net.sf.json.JSONObject;

@Controller
@Service
public class AjaxGateway extends GenericController
{
	private static final Pattern PASSWORD_KEY_VALUE = Pattern.compile("(\\w+)(PASSWORD)=([^\\s]+)");
	private static final String STARS = "*****";
	private static final String GATEWAY_NAME_PARAMETER = "gatewayName";
	private static final String ESAPI_HTTP_URL_FILTER = "HTTPURL";
	private static final String GATEWAY_NAME_INVALID_MSG = "Invalid " + GATEWAY_NAME_PARAMETER + " input";
	private static final String FILTER_OPERATION_BY_USER_FIELD_NAME = "RESOLVE_USERNAME";
	private static final String FILTER_SUFFIX = "Filter";
	private static final String FILTER_MAP_KEY_GATEWAY_CLASS = "GATEWAYCLASS";
	private static final String PUSH_GATEWAY_CLASS_NAME = "MPushGateway";
	private static final String PULL_GATEWAY_CLASS_NAME = "MPullGateway";
	private static final String MSG_GATEWAY_CLASS_NAME = "MMSGGateway";
	private static final String FILTER_MAP_KEY_QUEUE = "QUEUE";
	// SDK2 gateways support only deploy/undeploy filter operations
	private static final String GATEWAY_DEPLOY_FILTERS_OPERATION = "deployFilters";
	private static final String GATEWAY_UNDEPLOY_FILTERS_OPERATION = "undeployFilters";
	// Legacy and SDK1 gateways support only clear i.e. undeploy all and set i.e. deploy specific filters
	private static final String GATEWAY_CLEAR_AND_SET_FILTERS_OPERATION = "clearAndSetFilters";
	private static final String FILTER_MAP_KEY_IS_SDK2 = "FILTER_MAP_KEY_IS_SDK2";
	private static final String FILTER_MAP_KEY_COMPANION_MODEL_ROOT_MESSAGE_HANDLER = "COMPANIONMODELROOTMESSAGEHANDLER";
	private static final String FILTER_MAP_KEY_COMPANION_MODEL_CLEARANDSET_METHOD = "COMPANIONMODELCLEARANDSETMETHOD";
	private static final String FILTER_MAP_KEY_COMPANION_MODEL_DEPLOY_METHOD = "COMPANIONMODELDEPLOYMETHOD";
	private static final String FILTER_MAP_KEY_COMPANION_MODEL_UNDEPLOY_METHOD = "COMPANIONMODELUNDEPLOYMETHOD";
	private static final String FILTER_MAP_KEY_GATEWAY_COMPANION_MODEL_CLASS_NAME = "COMPANIONMODELCLASSNAME";
	
    protected boolean timerCanceled = false;
    private final int RETRIES = 10;
    private final long SLEEPTIME = 500L; //milliseconds
    
    /*
     * Map of gateway names to whether it is SDK2 developed gateways or not.
     * SDK2 developed gateways use common filter models by category Push, Pull, JMS versus 
     * legacy and SDK1 gateways use gateway specific filter models.
     */
    private static ConcurrentMap<String, Boolean> isNewSDK = new ConcurrentHashMap<String, Boolean>();

    /**
     * This method return a list of filters available for a gateway. For executing this method for a gateway refer
     * to the corresponding hibernate entity (e.g., EmailFilter or DatabaseFilter classes) for its fields to
     * query on.
     * Note: to ensure backward compatibility.
     *    Even both gatewayType and modelName are optional but at least on of them must exist.
     *    If modelName is passed, gatewayType will be ignored if exists.
     *
     * @param gatewayType name of the gateway type. For example, HTTP, EWS
     * @param modelName hibernate entity name, (e.g., "EmailFilter"). For example, check {@link EmailFilter} in the "persistence" project.
     * @param query in general contains hibernate (HQL) query related information. For example query.setWhereClause("lower(UName) like '%hello%'") can
     * be used to query the entities for name.
     * @param request
     * @param response
     * @return
     * @throws ServletException
     * @throws IOException
     */
    @RequestMapping(value = "/gateway/listFilters", method = { RequestMethod.GET }, headers = "Accept=application/json; charset=utf-8")
    @ResponseBody
    public ResponseDTO<GatewayVO> listFilters(
            @RequestParam(name="gatewayType", required=false) String gatewayType,
            @RequestParam(name="modelName", required=false) String modelName,
            @ModelAttribute QueryDTO query,
            HttpServletRequest request,
            HttpServletResponse response
        ) throws ServletException, IOException
    {

        ResponseDTO<GatewayVO> result = new ResponseDTO<GatewayVO>();

        if (StringUtils.isBlank(gatewayType) && StringUtils.isBlank(modelName)) {
            String warningMsg = "Either gatewayType and modelName must exist. Can't be both empty";
            Log.log.warn(warningMsg);
            return result.setSuccess(false).setMessage(warningMsg);
        }

        String username = (String) request.getAttribute(Constants.HTTP_REQUEST_USERNAME);
        
        String whereClause = query.getWhereClause();
        String type = null;

        if (StringUtils.isBlank(modelName)) {
            modelName = GatewayUtil.getGatewayFilterModelName(gatewayType, false);
        }

        try
        {
            if (StringUtils.isBlank(modelName))
                throw new Exception("modelName must be provided");
            
            if(isNewSDK.get(modelName) == null)
                isNewSDK.put(modelName, ServiceGateway.isNewSDKGateway(modelName));
            
            if(isNewSDK.get(modelName)) {
                int index = modelName.indexOf("Filter");
                
                if(index != -1) {
                    String gatewayName = modelName.substring(0, index);

                    if (StringUtils.isBlank(whereClause))
                        whereClause = "UGatewayName='" + gatewayName + "'";
                    
                    type = ServiceGateway.getGatewayType(gatewayName);
                    
                    if(type != null) {
                        if(type.equals("Push")) {
                            query.setModelName("PushGatewayFilter");
                        } else if(type.equals("Pull")) {
                            query.setModelName("PullGatewayFilter");
                        } else if(type.equals("MSG")) {
                            query.setModelName("MSGGatewayFilter");
                        }
                    } else {
                        Log.log.warn("Cannot find gateway filters.");
                        result.setSuccess(false);
                        return result;
                    }
                } else {
                    Log.log.warn("modelName does not contain 'Filter'.");
                    result.setSuccess(false);
                    return result;
                }
            }else {
            	query.setModelName(modelName);
            }
            
            if (StringUtils.isBlank(whereClause))
                whereClause = "UQueue='" + Constants.DEFAULT_GATEWAY_QUEUE_NAME + "'";
            else
                whereClause = whereClause + " and UQueue='" + Constants.DEFAULT_GATEWAY_QUEUE_NAME + "'";

            query.setWhereClause(whereClause);
            
            List<GatewayVO> records = ServiceGateway.getAllFilter(query, username, true);
            List<Throwable> getAttributeErrors = new ArrayList<Throwable>();
            
            if(CollectionUtils.isNotEmpty(records) && isNewSDK.get(modelName)) {                
                final String fType = type;
                records.parallelStream().forEach(vo -> {
                    String id = vo.getSys_id();
                    
                    if(fType.equals("Push")) {
                    	Map<String, String> attrs;
                    	try {
                    		attrs = PushGatewayFilterUtil.getPushGatewayFilterAttributes(id);
                    		((PushGatewayFilterVO)vo).setAttrs(attrs);
                    	}  catch (Throwable t) {
							Log.log.error(String.format("Error %sin getting Push Gateway Filter Attributes for filter " +
														"Id %s", 
														(StringUtils.isNotBlank(t.getMessage()) ? 
														 "[" + t.getMessage() + "] " : ""), id));
							getAttributeErrors.add(t);
                    	}
                    } else if(fType.equals("Pull")) {
                    	Map<String, String> attrs;
						try {
							attrs = PullGatewayFilterUtil.getPullGatewayFilterAttributes(id);
							((PullGatewayFilterVO)vo).setAttrs(attrs);
						} catch (Throwable t) {
							Log.log.error(String.format("Error %sin getting Pull Gateway Filter Attributes for filter " +
														"Id %s", 
														(StringUtils.isNotBlank(t.getMessage()) ? 
														 "[" + t.getMessage() + "] " : ""), id));
							getAttributeErrors.add(t);
						} 
                    } else if(fType.equals("MSG")) {
                    	Map<String, String> attrs;
						try {
							attrs = MSGGatewayFilterUtil.getMSGGatewayFilterAttributes(id);
							((MSGGatewayFilterVO)vo).setAttrs(attrs);
						} catch (Throwable t) {
							Log.log.error(String.format("Error %sin getting MSG Gateway Filter Attributes for filter " +
														"Id %s", 
														(StringUtils.isNotBlank(t.getMessage()) ? 
														 "[" + t.getMessage() + "] " : ""), id));
							getAttributeErrors.add(t);
						}
                    }
                });
            }
            
            if (CollectionUtils.isEmpty(getAttributeErrors)) { 
            	result.setRecords(records);
            	result.setTotal(records.size());
            } else {
            	throw new Exception(getAttributeErrors.get(0));
            }
        } catch (Exception e) {
            Log.log.error("Error retrieving filters", e);
            result.setSuccess(false).setMessage("Error retrieving filters");
        }
        
        return result;
    }

    /**
     * This method returns a list of available gateways (a.k.a queues) for a type of gateway. For example Email gateway
     * could have multiple instances with diferent queue name. This method return all those queue names.
     *
     * @param gatewayName identifies what gateway we need queues for. Check the blueprint for the proper name. The blueprint
     * has properties with the pattern rsremote.receive.xxxx.active. Whatever is in place of "xxxx" is the gateway name we
     * want to use.
     *
     * @param request
     * @param response
     * @return
     * @throws ServletException
     * @throws IOException
     */
    @RequestMapping(value = "/gateway/listGateways", method = { RequestMethod.GET }, headers = "Accept=application/json; charset=utf-8")
    @ResponseBody
    public ResponseDTO<String> listGateways(@RequestParam("gatewayName") String gatewayName, HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException
    {
        ResponseDTO<String> result = new ResponseDTO<String>();
        
        String username = (String) request.getAttribute(Constants.HTTP_REQUEST_USERNAME);
        
        boolean isAdminUser = UserUtils.isAdminUser(username);
        boolean isNoOrgAccessible = UserUtils.isNoOrgAccessible(username);
        
        try
        {
            if (StringUtils.isBlank(gatewayName))
            {
                throw new Exception("gatewayName must be provided");
            }
            else
            {
                //LinkedHashSet is serializable
                Set<String> gateways = new TreeSet<String>();
                List<ResolveBlueprintVO> blueprints = ServiceHibernate.findAllResolveBlueprint();
                for (ResolveBlueprintVO blueprintVO : blueprints)
                {
                    Properties properties = new Properties();
                    properties.load(StringUtils.toInputStream(blueprintVO.getUBlueprint()));
                    gatewayName = gatewayName.toLowerCase();
                    String tmpModelname = gatewayName.toLowerCase();
                    if (GatewayUtil.GATEWAY_EXCEPTION_MAPPING.containsKey(gatewayName))
                    {
                        tmpModelname = GatewayUtil.GATEWAY_EXCEPTION_MAPPING.get(gatewayName);
                    }
                    
                    // Get rsremote instance count
                    
                    if (properties.containsKey("rsremote.instance.count"))
                    {
                        int rmtInstCnt = 0;
                        
                        try
                        {
                            rmtInstCnt = Integer.parseInt(properties.getProperty("rsremote.instance.count", "0"));
                        }
                        catch(NumberFormatException nfe)
                        {
                            Log.log.warn("Blueprint property rsremote.instance.count value " + 
                                         properties.getProperty("rsremote.instance.count") + 
                                         " is not a number!!!");
                        }
                        
                        if (rmtInstCnt > 0)
                        {
                            for (int i = 1; i <= rmtInstCnt; i++)
                            {
                                if (properties.containsKey("rsremote.instance" + i + ".name"))
                                {
                                    String rmtInstName = properties.getProperty("rsremote.instance" + i + ".name");
                                    
                                    String orgSuffix = "";
                                    
                                    boolean hasOrgAccess = false;
                                    
                                    String orgNamePropVal = properties.getProperty(rmtInstName + ".general.org");
                                    if (StringUtils.isNotBlank(orgNamePropVal))
                                    {   
                                        if (isAdminUser || UserUtils.isOrgAccessibleByOrgName(orgNamePropVal, username, false, false))
                                        {
                                            hasOrgAccess = true;
                                            
                                            if (StringUtils.isNotBlank(orgNamePropVal))
                                            {
                                                orgSuffix = orgNamePropVal.replaceAll("[^0-9A-Za-z_]", "").toUpperCase();
                                            }
                                        }
                                    }
                                    else
                                    {
                                        if (isAdminUser || isNoOrgAccessible)
                                        {
                                            hasOrgAccess = true;
                                        }
                                    }
                                    
                                    if (hasOrgAccess && properties.containsKey(rmtInstName + ".receive." + tmpModelname + ".active") &&
                                        Boolean.parseBoolean(properties.getProperty(rmtInstName + ".receive." + tmpModelname + ".active")) &&
                                        properties.containsKey(rmtInstName + ".receive." + tmpModelname + ".queue") &&
                                        ((Boolean.parseBoolean(properties.getProperty(rmtInstName + ".receive." + tmpModelname + ".primary"))) || 
                                         (Boolean.parseBoolean(properties.getProperty(rmtInstName + ".receive." + tmpModelname + ".secondary")))))
                                    {
                                        gateways.add(properties.getProperty(rmtInstName + ".receive." + tmpModelname + ".queue") + orgSuffix);
                                    }                                    
                                }
                                else
                                {
                                    Log.log.warn("RSRemote instance name property rsremote.instance" + i + 
                                                 ".name is missing from blueprint!!!");
                                }
                            }
                        }
                    }
                }
                
                List<String> gatewayList = new ArrayList<String>();
                gatewayList.addAll(gateways);
                result.setRecords(gatewayList);
                result.setTotal(gateways.size());
            }
        }
        catch (Exception e)
        {
            Log.log.error("Error retrieving gateways (queue) from resolve_blueprint table", e);
            result.setSuccess(false).setMessage("Error retrieving gateways");
        }
        return result;
    }
    
    /**
     * This method returns a list of available gateways (a.k.a queues) for a type of gateway. For example Email gateway
     * could have multiple instances with diferent queue name. This method return all those queue names.
     *
     * @param gatewayName identifies what gateway we need queues for. Check the blueprint for the proper name. The blueprint
     * has properties with the pattern rsremote.receive.xxxx.active. Whatever is in place of "xxxx" is the gateway name we
     * want to use.
     *
     * @param request
     * @param response
     * @return
     * @throws ServletException
     * @throws IOException
     */
    @RequestMapping(value = "/gateway/listQueues", method = { RequestMethod.GET }, headers = "Accept=application/json; charset=utf-8")
    @ResponseBody
    public ResponseDTO<Map<String,Set<String>>> listQueues( HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException
    {
        ResponseDTO<Map<String,Set<String>>> result = new ResponseDTO<Map<String,Set<String>>>();
        try
        {
            //LinkedHashSet is serializable
            TreeMap<String,Set<String>> gateways = GatewayUtil.getActiveQueueListMap();
            
            result.setData(gateways);
            result.setTotal(gateways.size());
        }
        catch (Exception e)
        {
            Log.log.error("Error retrieving gateway queue(s) from resolve_blueprint table", e);
            result.setSuccess(false).setMessage("Error retrieving gateway queue(s) from resolve_blueprint table");
        }
        
        return result;
    }

    /**
     * This method return details of a filter. Refer to the corresponding VO for the JSON object's key. For example check EmailFilterVO
     * for probable JSON data.
     * Note: to ensure backward compatibility.
     *     Even both gatewayType and modelName are optional but at least on of them must exist.
     *     If modelName is passed, gatewayType will be ignored if exists.
     * @param modelName hibernate entity name, (e.g., "EmailFilter"). For example, check {@link EmailFilter} in the "persistence" project.
     * @param id sys_id of the gateway filter.
     * @param request
     * @param response
     * @return
     * @throws ServletException
     * @throws IOException
     */
    @RequestMapping(value = "/gateway/getFilter", method = { RequestMethod.GET }, headers = "Accept=application/json; charset=utf-8")
    @ResponseBody
    public ResponseDTO<Object> getFilter(
            @RequestParam(name="modelName", required=false) String modelName,
            @RequestParam(name="gatewayName", required=false) String gatewayName,
            @RequestParam String id,
            HttpServletRequest request,
            HttpServletResponse response
            ) throws ServletException, IOException    {
        ResponseDTO<Object> result = new ResponseDTO<Object>();
        String username = (String) request.getAttribute(Constants.HTTP_REQUEST_USERNAME);
        if (StringUtils.isBlank(gatewayName) && StringUtils.isBlank(modelName)) {
            String warningMsg = "Either gatewayType and modelName must exist. Can't be both empty";
            Log.log.warn(warningMsg);
            return result.setSuccess(false).setMessage(warningMsg);
        }
        if (StringUtils.isBlank(modelName)) {
            modelName = GatewayUtil.getGatewayFilterModelName(gatewayName, false);
        }
        try
        {
            if (StringUtils.isBlank(modelName))
                throw new Exception("modelName must be provided");

            if (StringUtils.isBlank(id))
                throw new Exception("id must be provided");
            
            if(isNewSDK.get(modelName) == null)
                isNewSDK.put(modelName, ServiceGateway.isNewSDKGateway(modelName));
            
            if(!isNewSDK.get(modelName)) {
                GatewayVO vo = ServiceGateway.getFilter(modelName, id, username, true);
                result.setData(vo);
                result.setSuccess(true);
            }

            else {
                if (StringUtils.isNotBlank(gatewayName)) {
                    modelName = StringUtils.toCamelCase(gatewayName) + "Filter";
                }
                int index = modelName.indexOf("Filter");
                if(index != -1) {
                    GatewayFilterVO vo = ServiceGateway.getFilterById(modelName.substring(0, index), id);
                    if(vo == null) {
                        Log.log.warn("Filter not found");
                        result.setSuccess(false).setMessage("Filter not found");
                    }
                    
                    if(vo instanceof PushGatewayFilterVO) {
                        Map<String, String> attrs = ((PushGatewayFilterVO)vo).toMap();
                        result.setData(attrs);
                    }
                    
                    else if(vo instanceof PullGatewayFilterVO) {
                        Map<String, String> attrs = ((PullGatewayFilterVO)vo).toMap();
                        result.setData(attrs);
                    }
                    
                    else if(vo instanceof MSGGatewayFilterVO) {
                        Map<String, String> attrs = ((MSGGatewayFilterVO)vo).toMap();
                        result.setData(attrs);
                    }

                    result.setSuccess(true);
                }
                
                else {
                    Log.log.warn("modelName does not contain 'Filter'.");
                    result.setSuccess(false).setMessage("Model name not found");
                }
            }
        }
        catch (Exception e)
        {
            Log.log.error("Error retrieving filters", e);
            result.setSuccess(false).setMessage("Error retrieving filters");
        }
        return result;
    }

    /**
     * This method return details of a filter. Refer to the corresponding VO for the JSON object's key. For example check EmailFilterVO
     * for probable JSON data.
     * Note: to ensure backward compatibility modelName is not required 
     * @param gatewayType name of the gateway type. For example, HTTP, EWS
     * @param modelName hibernate entity name, (e.g., "EmailFilter"). For example, check {@link EmailFilter} in the "persistence" project.
     * @param ids sys_ids of the gateway filters to be deleted.
     * @param request
     * @param response
     * @return
     * @throws ServletException
     * @throws IOException
     */
    @RequestMapping(value = "/gateway/deleteFilters", method = { RequestMethod.GET, RequestMethod.POST }, headers = "Accept=application/json; charset=utf-8")
    @ResponseBody
    public ResponseDTO<Object> deleteFilter(
            @RequestParam(name="gatewayName") String gatewayName, 
            @RequestParam(name="modelName", required=false) String modelName, 
            @RequestParam String[] ids, 
            HttpServletRequest request, 
            HttpServletResponse response
        ) throws ServletException, IOException
    {
        ResponseDTO<Object> result = new ResponseDTO<Object>();
        List<String> deleted = new ArrayList<String>();
        List<String> nonDeleted = new ArrayList<String>();

        if (StringUtils.isBlank(modelName)) {
            modelName = GatewayUtil.getGatewayFilterModelName(gatewayName, false);
        }

        try
        {
            gatewayName = ESAPI.validator().getValidInput(modelName.replace("Filter", "Gateway") +
            		": Invalid input gatewayName ", gatewayName, "HTTPURL", 4000, true);
            
            if (StringUtils.isBlank(gatewayName))
                throw new Exception(gatewayName + "Gateway: Gateway name must be provided");
            
            if (StringUtils.isBlank(modelName))
                throw new Exception(gatewayName + "Gateway: Model name must be provided");

            
            boolean success = true;
            
            if(!isNewSDK.containsKey(modelName))
                isNewSDK.put(modelName, ServiceGateway.isNewSDKGateway(modelName));
            
            if(isNewSDK.get(modelName)) {
                modelName = ServiceGateway.getModelName(gatewayName);
            }
            else
                gatewayName = null;

            if(StringUtils.isBlank(modelName)) {
            	String errMsg = String.format("Cannot find model name for gateway %s", gatewayName);
                Log.log.error(errMsg);
                result.setSuccess(false).setMessage(errMsg);
                return result;
            }            

            Map<String, String> unDeletableFilters = getUnDeletableFilterIds(ids, modelName, gatewayName);

            for(int i=0; i<ids.length; i++) {
                String id = ids[i];
                if(unDeletableFilters.containsKey(id))
                    nonDeleted.add(unDeletableFilters.get(id));
                else
                    deleted.add(id);
            }
            
            int size = deleted.size();
            Log.log.info(String.format("Deleting gateway %s (model %s) filters with the ids --> %s", 
            						   gatewayName, modelName, StringUtils.listToString(deleted, ", ")));
            
            if(size > 0) {
                String[] sysIds = new String[size];
                sysIds = deleted.toArray(sysIds);
                if(gatewayName == null) {
                	success = ServiceGateway.deleteFilter(modelName, sysIds);
                }else {
                	success = GatewayFilterUtil.deleteFilter(gatewayName, sysIds);
                }
                	
            }
            
            if(success) {
                if(nonDeleted.size() == 0)
                    result.setSuccess(true);
                else {
                    result.setSuccess(false);
                    Log.log.info(modelName.replace("Filter", "Gateway") + ": Successfully deleted filters ");
                    result.setMessage(String.format("The following filters cannot be deleted before they are undeployed: %s", StringUtils.listToString(nonDeleted, ", ")));
                }
            }
            
            else {
            	Log.log.info(modelName.replace("Filter", "Gateway") + ": Error when deleting filters ");
                result.setSuccess(false).setMessage("Error deleting filter.");
            }
        }
        catch (Exception e)
        {
            Log.log.error(modelName.replace("Filter", "Gateway") + ": Error deleting filter", e);
            result.setSuccess(false).setMessage("Error deleting filter.");
        }
        
        return result;
    }
    
    private Map<String, String> getUnDeletableFilterIds(String[] ids, String modelName, String gatewayName) throws Exception {

        Map<String, String> unDeletableFilters = new HashMap<String, String>();
        GatewayFilterVO gwVo = null;
        for(int i=0; i<ids.length; i++) {
            String id = ids[i];
            String filterName = "";
            if(gatewayName == null)
            	filterName = ServiceGateway.getFilterDeployed(modelName, gatewayName, id);
            else
            	gwVo = GatewayFilterUtil.getDeployedFilterById(gatewayName, id);
            if(gwVo != null)
            	filterName = gwVo.getUName();
            if(StringUtils.isNotEmpty(filterName))
                unDeletableFilters.put(id, filterName);
        }
        
        return unDeletableFilters;
    }

    /**
     * This method saves a filter in the database. Refer to the corresponding VO for the JSON object's key. For example check EmailFilterVO
     * for probable JSON data.
     *
     * @param jsonProperty along with the actual data for the filter it also should have an additional property name "modelName" with the
     * value like "EmailFilter".
     * @param request
     * @param response
     * @return
     * @throws ServletException
     * @throws IOException
     */
    @SuppressWarnings("unchecked")
	@RequestMapping(value = "/gateway/saveFilter", method = { RequestMethod.POST }, headers = "Accept=application/json; charset=utf-8")
    @ResponseBody
    public ResponseDTO<Object> saveFilter(@RequestBody JSONObject jsonProperty, HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException
    {
        ResponseDTO<Object> result = new ResponseDTO<Object>();
        String username = (String) request.getAttribute(Constants.HTTP_REQUEST_USERNAME);
        
        try
        {
            if (jsonProperty == null)
            {
                throw new Exception("jsonProperty must be provided");
            }
            String modelName = "";
            String gatewayName = "";
            try {
                modelName = jsonProperty.getString("modelName");
            } catch (JSONException e) {
                // when model name is not found, JSONException will be thrown
                gatewayName = jsonProperty.getString("gatewayName");
                try {
                    String gwType = ServiceGateway.getGatewayType(gatewayName);
                    if (gwType.toLowerCase().equals("none")) {
                        modelName = GatewayUtil.getGatewayFilterModelName(gatewayName, false);
                    } else {
                        modelName =  gwType+"GatewayFilter";
                    }
                } catch (JSONException e1) {
                    String warningMsg = "Either gatewayName or modelName must exist. Can't be both empty";
                    Log.log.warn(warningMsg, e1);
                    return result.setSuccess(false).setMessage(warningMsg);
                }
            }

            if(modelName != null)
            	Log.log.info(modelName.replace("Filter", "Gateway") + ": Gateway Filter creation request received for --> " + modelName);
            if (StringUtils.isBlank(modelName))
            {
                throw new Exception("modelName must be provided");
            }
            else
            {
                String voName = modelName + "VO";
                String className = "com.resolve.services.hibernate.vo." + voName;
                
                if(isNewSDK.get(modelName) == null)
                    isNewSDK.put(modelName, ServiceGateway.isNewSDKGateway(modelName));
                
                if(isNewSDK.get(modelName)) {
                    if (StringUtils.isNotBlank(gatewayName)) {
                        modelName = StringUtils.toCamelCase(gatewayName) + "Filter";
                    }
                    int index = modelName.indexOf("Filter");
                    if(index != -1) {
                        Map<String, String> params = new HashMap<String, String>();
                        
                        try {
                            JSONObject json = JSONObject.fromObject(jsonProperty);
                            
                            for(Iterator<String> it=json.keys(); it.hasNext();) {
                                String key = it.next();
                                String val = json.getString(key);
                                //Adding logic to encrypt the JSON Data
                                JSONObject jsonData = GatewayFilterUtil.jsonField(val);
                                if(jsonData != null) {
                                	val = GatewayFilterUtil.encryptDecryptJSONVal(jsonData, true);
                                }
                                params.put(key, val);
                            }
                            
                            params.put("username", username);

                            GatewayFilterVO vo = GatewayFilterUtil.saveGatewayFilter(params, modelName.substring(0, index));
                            
                            if(vo instanceof PushGatewayFilterVO) {
                                Map<String, String> attrs = ((PushGatewayFilterVO)vo).toMap();
                                result.setData(attrs);
                            }
                            
                            else if(vo instanceof PullGatewayFilterVO) {
                                Map<String, String> attrs = ((PullGatewayFilterVO)vo).toMap();
                                result.setData(attrs);
                            }
                            
                            else if(vo instanceof MSGGatewayFilterVO) {
                                Map<String, String> attrs = ((MSGGatewayFilterVO)vo).toMap();
                                result.setData(attrs);
                            }

                            result.setSuccess(true);
                            Log.log.debug( modelName.replace("Filter", "Gateway") + ": Gateway Filter creation successful for With data -->"+ jsonProperty.toString());
                            		
                            return result;
                        } catch(Exception ee) {
                        	Log.log.debug( modelName.replace("Filter", "Gateway") + ": Gateway Filter creation failed with data -->"+ jsonProperty.toString());
                            Log.log.error(ee.getMessage(), ee);
                            result.setSuccess(false).setMessage("Error saving the Filter: " + modelName);
                            return result;
                        }
                    }
                    
                    else {
                        Log.log.error(modelName.replace("Filter", "Gateway") + ": No model name is found.");
                        result.setSuccess(false).setMessage(modelName.replace("Filter", "Gateway") + ": No model name is found.");
                        
                        return result;
                    }
                }

                //using jackson to deserialize the json
                String json = jsonProperty.toString();
                ObjectMapper mapper = new ObjectMapper();
                mapper.configure(Feature.FAIL_ON_UNKNOWN_PROPERTIES, false);
                Object filterVo = mapper.readValue(json, Class.forName(className));

                //we expect UI to send sys_id as part of the JSON object.
                String id = jsonProperty.getString("id");
                if (StringUtils.isNotBlank(id))
                {
                    //this is most likely an update unless the id is wrong or there is a bug.
                    GatewayVO vo = ServiceGateway.getFilter(modelName, id, username);
                    if (vo == null)
                    {
                        throw new Exception("Invalid ID passed: " + id);
                    }
                }
                
                try
                {
                    Object saveResponse = ServiceGateway.saveFilter(modelName, (GatewayVO) filterVo, username);
                    if(modelName != null) {
                    	String filterName = jsonProperty.containsKey("upoolName")? jsonProperty.getString("upoolName"):jsonProperty.getString("uname");
                    	Log.log.info(modelName.replace("Filter", "Gateway") + ": filter --> " + filterName + ", saved successfully");
                    }
                    result.setData(saveResponse);
                    result.setSuccess(true);
                }
                catch (Exception e)
                {
                	if(modelName != null)
                		Log.log.error(modelName.replace("Filter", "Gateway") + ": Error saving the Filter: " + modelName, e);
                	if (e.getMessage() != null && e.getMessage().startsWith("Record already exists")) {
                		result.setSuccess(false).setMessage("Can not save filter because a filter with same name already exists");
                	} else {
                		result.setSuccess(false).setMessage("Error saving the Filter: " + modelName);
                	}
                    
                }

                return result;
            }

        }
        catch (Exception e)
        {
            Log.log.error("Error saving filter", e);
            result.setSuccess(false).setMessage("Error saving filter.");
        }
        return result;
    }

    /**
     * This method return list of all deployed filter for a queue.
     * Note: to ensure backward compatibility.
     *    Even both gatewayType and modelName are optional but at least on of them must exist.
     *    If modelName is passed, gatewayType will be ignored if exists.
     *
     * @param gatewayType name of the gateway type. For example, HTTP, EWS
     * @param modelName hibernate entity name, (e.g., "EmailFilter"). For example, check {@link EmailFilter} in the "persistence" project.
     * @param queueName for example "EMAIL", this queue name is defined in the blueprint like rsremote.receive.email.queue=EMAIL
     * @param request
     * @param response
     * @return
     * @throws ServletException
     * @throws IOException
     */
    @RequestMapping(value = "/gateway/listDeployedFilters", method = { RequestMethod.GET }, headers = "Accept=application/json; charset=utf-8")
    @ResponseBody
    public ResponseDTO<GatewayVO> listDeployedFilters(
            @RequestParam(name="gatewayType", required=false) String gatewayType,
            @RequestParam(name="modelName", required=false) String modelName,
            @RequestParam("queueName") String queueName,
            HttpServletRequest request,
            HttpServletResponse response
        ) throws ServletException, IOException
    {

        ResponseDTO<GatewayVO> result = new ResponseDTO<GatewayVO>();

        if (StringUtils.isBlank(gatewayType) && StringUtils.isBlank(modelName)) {
            String warningMsg = "Either gatewayType and modelName must exist. Can't be both empty";
            Log.log.warn(warningMsg);
            return result.setSuccess(false).setMessage(warningMsg);
        }
 
        String username = (String) request.getAttribute(Constants.HTTP_REQUEST_USERNAME);

        if (StringUtils.isBlank(modelName)) {
            modelName = GatewayUtil.getGatewayFilterModelName(gatewayType, false);
        }

        try
        {
            if (StringUtils.isBlank(modelName))
                throw new Exception("modelName must be provided");

            else if (StringUtils.isBlank(queueName))
                throw new Exception("queueName must be provided");
            
            if(isNewSDK.get(modelName) == null)
                isNewSDK.put(modelName, ServiceGateway.isNewSDKGateway(modelName));
            
            if(!isNewSDK.get(modelName)) {
                if (StringUtils.isBlank(modelName))
                    throw new Exception("modelName must be provided");

                else if (StringUtils.isBlank(queueName))
                    throw new Exception("queueName must be provided");

                else {
                    //The QueryDTO will provide a HQL statement like:
                    //select tableData from EmailFilter tableData  where UQueue='MyQueue'
                    List<GatewayVO> records = ServiceGateway.getAllFilters(modelName, queueName, username);
                    Log.log.info("getAllFilter(" + queueName + ") returned " + (records != null ? records.size() : "null"));
                    
                    List<GatewayVO> defaultFilters = ServiceGateway.getAllFilters(modelName, Constants.DEFAULT_GATEWAY_QUEUE_NAME, username);
                    Log.log.info("getAllFilter(" + Constants.DEFAULT_GATEWAY_QUEUE_NAME + ") returned " + (defaultFilters != null ? defaultFilters.size() : "null"));
                    
                    Map<String, GatewayVO> map = new HashMap<String, GatewayVO>();
                    for (GatewayVO vo : defaultFilters)
                        map.put(vo.getUniqueId(), vo);

                    for (GatewayVO vo : records)
                        vo.setChanged(!vo.equals(map.get(vo.getUniqueId())));

                    result.setRecords(records);
                    result.setTotal(records.size());
                }
            }
            
            else {
                int index = modelName.indexOf("Filter");
                
                if(index != -1) {
                    List<GatewayVO> records = ServiceGateway.listFilters(modelName.substring(0, index), queueName, true);
                 
                    for(GatewayVO vo:records) {
                        if(vo instanceof PushGatewayFilterVO) {
                        	//Get the updated status for 
                            vo.setChanged(((PushGatewayFilterVO)vo).getUUpdated());
                            String id = vo.getSys_id();
                            Map<String, String> attrs = PushGatewayFilterUtil.getPushGatewayFilterAttributes(id);
                            ((PushGatewayFilterVO)vo).setAttrs(attrs);
                        }

                        else if(vo instanceof PullGatewayFilterVO) {
                            vo.setChanged(((PullGatewayFilterVO)vo).getUUpdated());
                            String id = vo.getSys_id();
                            Map<String, String> attrs = PullGatewayFilterUtil.getPullGatewayFilterAttributes(id);
                            ((PullGatewayFilterVO)vo).setAttrs(attrs);
                        }
                        
                        else if(vo instanceof MSGGatewayFilterVO) {
                            vo.setChanged(((MSGGatewayFilterVO)vo).getUUpdated());
                            String id = vo.getSys_id();
                            Map<String, String> attrs = MSGGatewayFilterUtil.getMSGGatewayFilterAttributes(id);
                            ((MSGGatewayFilterVO)vo).setAttrs(attrs);
                        }
                    }
                    result.setRecords(records);
                    result.setTotal(records.size());
                    result.setSuccess(true);
                }
                
                else {
                    Log.log.warn(modelName.replace("Filter", "Gateway") + ": modelName does not contain 'Filter'.");
                    result.setSuccess(false);
                    return result;
                }
            }
        }
        catch (Exception e)
        {
            Log.log.error("Error retrieving filters", e);
            result.setSuccess(false).setMessage("Error retrieving filters");
        }
        return result;
    }

    private String parseDeployFilters2Result(ResponseDTO<Pair<String, Boolean>> result) {
    	StringBuilder strBldr = new StringBuilder();
    	
    	if (result.isSuccess()) {
    		if (CollectionUtils.isNotEmpty(result.getRecords())) {
    			result.getRecords().stream().forEach(resultPair -> {
    				if (strBldr.length() > 0) {
    					strBldr.append(", ");
    				}
    				strBldr.append(resultPair);
    			});
    		}
    	} else {
    		strBldr.append(result.getMessage());
    	}
    	
    	return strBldr.toString();
    }
    
    /**
     * This method deploys filter to a RSRemote where the queue is configured.
     *
     * @param modelName hibernate entity name, (e.g., "EmailFilter"). For example, check {@link EmailFilter} in the "persistence" project.
     * @param queueName for example "EMAIL", this queue name is defined in the blueprint like rsremote.receive.email.queue=EMAIL
     * @param gatewayClass example MEmail or MDatabase. Check gateway project for all available classes like this.
     * @param clearAndSetMethod example clearAndSetFilter which is available in the gatewayClass or its super class.
     * @param filterNames an array of the filter names we want to deploy.
     * @param deleteFilterNames an array of filters we want to delete.
     * @param request
     * @param response
     * @return
     * @throws ServletException
     * @throws IOException
     */
//    @SuppressWarnings("unchecked")
	@RequestMapping(value = "/gateway/deployFilters", method = { RequestMethod.GET, RequestMethod.POST }, headers = "Accept=application/json; charset=utf-8")
    @ResponseBody
    public ResponseDTO<GatewayVO> deployFilters(
            @RequestParam(name="modelName", required=false) String modelName,
            @RequestParam("queueName") final String queueName,
            @RequestParam(name="gatewayClass", required=false) String gatewayClass,
            @RequestParam("clearAndSetMethod") String clearAndSetMethod,
            @RequestParam("filterNames") final String[] filterNames, 
            @RequestParam("deleteFilterNames") final String[] deleteFilterNames,
            HttpServletRequest request,
            HttpServletResponse response
        ) throws ServletException, IOException
    {
    	ResponseDTO<GatewayVO> result = new ResponseDTO<GatewayVO>();
    	/*
    	 * Test list gateway health check reports
    	 */
    	
//    	ResponseDTO<GatewayHealthCheckVO> listGTWHCResults = listGatewayHealthCheckReports(request, response);
    	
    	/*
    	 * Test Conduct (All Active) Gateway Health Checks
    	 */
    	
//    	try {
//    		Map<String, Object> params = new HashMap<String, Object>();
//    		
//    		params.put(ServiceGateway.PARAM_MAINBASE_KEY, Main.main);
//    		params.put(ServiceGateway.PARAM_USERNAME_KEY, UserUtils.SYSTEM);
//    		
//			ServiceGateway.conductGatewayHealthChecks(params);
//			
//			if (Log.log.isDebugEnabled()) {
//				Log.log.debug("Conduct gateway healthChecks on all active gateways");
//			}
//		} catch (Exception e1) {
//			Log.log.debug(String.format("Error %sin performing gateway health checks on all active gateways", 
//										(StringUtils.isNotBlank(e1.getMessage()) ? 
//										 "[" + e1.getMessage() + "] " : "")), e1);
//		}
    	
    	/*
    	 * Test Gateway Health Check Only
    	 */
//    	Map<String, String> healthCheckResult = new HashMap<String, String>();
//    	
//    	try {    		
//    		String tGatewayClass = ServiceGateway.getMessageHandlerNameForQName(Main.main, queueName);
//    		
//    		long startTime = System.currentTimeMillis();
//			healthCheckResult = Main.esb.call(queueName.toUpperCase(), 
//											  String.format("%s.healthCheck", tGatewayClass), 
//											  new HashMap<String, Object>(), 60000);
//			
//			if (Log.log.isDebugEnabled()) {
//				Log.log.debug(String.format("%s gateway healthCheck returned [%s] in %d msec", queueName,
//											(MapUtils.isNotEmpty(healthCheckResult) ? 
//											 StringUtils.mapToString(healthCheckResult, "=", ", ") : ""),
//											System.currentTimeMillis() - startTime));
//			}
//		} catch (Exception e1) {
//			Log.log.debug(String.format("Error %sin performing health check on %s gateway", 
//										(StringUtils.isNotBlank(e1.getMessage()) ? 
//										 "[" + e1.getMessage() + "] " : ""), queueName), e1);
//		}
    	
    	/*
    	 * Test deployFilters2 Only
    	 */
    	
    	/*
    	 * Clear And Set
    	 */
    	
//    	String[] queues = new String[] {"DATABASE", "DATABASE2"};
//    	String[] queues = new String[] {"ZENOSS", "ZENOSS2"};
    	
//    	ResponseDTO<Pair<String, Boolean>> result2 = deployFilters2(modelName, 
//    																queues,
//    																(String) request.getParameter("gatewayName"),
//    																gatewayClass,
//    																Constants.FILTER_OPERATION_CLEARANDSET,
//    																filterNames,
//    																deleteFilterNames,
//    																request,
//    																response);
    	        
//        return result.setMessage(String.format("Submit operation %s request(s) to gateway queues [%s] for deploying %d " +
//        									   "[%s] and undeploying %d [%s] filters of %s gateway Result: %b, %s", 
//        									   Constants.FILTER_OPERATION_CLEARANDSET, 
//        									   StringUtils.arrayToString(queues, ", "),
//        									   (filterNames != null ? filterNames.length : 0),
//        									   StringUtils.arrayToString(filterNames, ", "),
//        									   (deleteFilterNames != null ? deleteFilterNames.length : 0),
//        									   StringUtils.arrayToString(deleteFilterNames, ", "),
//        									   (String) request.getParameter("gatewayName"),
//        									   result2.isSuccess(), 
//        									   parseDeployFilters2Result(result2))).setSuccess(result2.isSuccess());

    	/*
    	 * Deploy
    	 */
    	
//        Set<String> filterNamesSet = new HashSet<String>();
//        Arrays.asList(filterNames).forEach(filterName -> {filterNamesSet.add(filterName);});
//        Set<String> deleteFilterNamesSet = new HashSet<String>();
//        Arrays.asList(deleteFilterNames).forEach(deleteFilterName -> {deleteFilterNamesSet.add(deleteFilterName);});
//        filterNamesSet.removeAll(deleteFilterNamesSet);
//        
//        ResponseDTO<Pair<String, Boolean>> result2 = deployFilters2(modelName, 
//																	queues,
//																	(String) request.getParameter("gatewayName"),
//																	gatewayClass,
//																	Constants.FILTER_OPERATION_DEPLOY,
//																	filterNamesSet.toArray(new String[0]),
//																	new String[0],
//																	request,
//																	response);
//
//        return result.setMessage(String.format("Submit operation %s request(s) to gateway queues [%s] for deploying %d " +
//        									   "[%s] filters of %s gateway Result: %b, %s", 
//        									   Constants.FILTER_OPERATION_DEPLOY, StringUtils.arrayToString(queues, ", "),
//        									   filterNamesSet.size(),
//        									   StringUtils.setToString(filterNamesSet, ", "),
//        									   (String) request.getParameter("gatewayName"),
//        									   result2.isSuccess(), 
//        									   parseDeployFilters2Result(result2))).setSuccess(result2.isSuccess());
       
        /*
    	 * UnDeploy
    	 */
        
//        ResponseDTO<Pair<String, Boolean>> result2 = deployFilters2(modelName, 
//																	queues,
//																	(String) request.getParameter("gatewayName"),
//																	gatewayClass,
//																	Constants.FILTER_OPERATION_UNDEPLOY,
//																	null,
//																	deleteFilterNames,
//																	request,
//																	response);

//        return result.setMessage(String.format("Submit operation %s request(s) to gateway queues [%s] for undeploying %d " +
//        									   "[%s] filters of %s gateway Result: %b, %s", 
//        									   Constants.FILTER_OPERATION_UNDEPLOY, StringUtils.arrayToString(queues, ", "),
//        									   (deleteFilterNames != null && deleteFilterNames.length > 0 ? 
//        										deleteFilterNames.length : 0),
//        									   StringUtils.arrayToString(deleteFilterNames, ", "),
//        									   (String) request.getParameter("gatewayName"),
//        									   result2.isSuccess(), 
//        									   parseDeployFilters2Result(result2))).setSuccess(result2.isSuccess());
    	
        Long filterCount = 0L;
        Long deleteFilterCount = 0L;
        
        if (filterNames != null)
            filterCount = Long.valueOf(filterNames.length);

        if (deleteFilterNames != null)
            deleteFilterCount = Long.valueOf(deleteFilterNames.length);

        //now that for all gateways are High Availability (HA) built in, we'll use topic for messaging
        final String tmpQueueName = queueName.toUpperCase();
        final String gatewayTopic = tmpQueueName + "_TOPIC";

        //this is a way to pass the user
        final String username = (String) request.getAttribute(Constants.HTTP_REQUEST_USERNAME);
        Map<String, String> user = new HashMap<String, String>();
        user.put(FILTER_OPERATION_BY_USER_FIELD_NAME, username);
        
        try
        {
            String gatewayName = (String) request.getParameter("gatewayName");
            gatewayName = ESAPI.validator().getValidInput(GATEWAY_NAME_INVALID_MSG, gatewayName, ESAPI_HTTP_URL_FILTER, 40, 
            											  true);
            if (StringUtils.isBlank(gatewayClass)) {
                gatewayClass = "DEFAULT";
            }
            if (StringUtils.isBlank(modelName)) {
                modelName = GatewayUtil.getGatewayFilterModelName(gatewayName, false);
            }
            Log.log.debug( gatewayName + "Gateway: Deployment request received for all filteres -->"+ 
            	filterNames != null ? Arrays.toString(filterNames) : "No Filters to deploy");
            Log.log.debug( gatewayName + "Gateway: Request received for undeploying/deleting filteres -->"+ 
            	deleteFilterNames != null ? Arrays.toString(deleteFilterNames) : "No Filters to delete/undeploy");
            
            if (filterCount == 0 && deleteFilterCount == 0)
                throw new Exception(gatewayName + "Gateway: There is no nothing to do, try adding or deleting some items.");

            if (StringUtils.isBlank(modelName))
                throw new Exception(gatewayName + "Gateway: modelName parameter must be provided");

            if (StringUtils.isBlank(queueName))
                throw new Exception(gatewayName + "Gateway: queueName parameter must be provided");
            
            if (!isNewSDK.containsKey(modelName)) {
            	isNewSDK.putIfAbsent(modelName, ServiceGateway.isNewSDKGateway(modelName));
            }
            
            if(!isNewSDK.get(modelName)) {
                result = deployFilters(result, gatewayName, gatewayClass, gatewayTopic, filterCount, clearAndSetMethod, modelName, tmpQueueName, user, filterNames, deleteFilterNames);
                if(result != null)
                	Log.log.info( gatewayName + "Gateway: Filter deployment successful by deploying --> "+ filterCount + ", Filter details "
                			+ filterNames != null ? Arrays.toString(filterNames) : "");
            }
            else {
                try {
                    if(deleteFilterCount != 0) {
                        result = undeployFilters(result, gatewayName, gatewayClass, gatewayTopic, deleteFilterCount, filterCount, clearAndSetMethod, modelName, tmpQueueName, user, deleteFilterNames);
                    }
                } catch(Exception ee) {
                    Log.log.error(ee.getMessage(), ee);
                }
                
                if(filterCount != 0)
                    result = deployFilters(result, gatewayName, gatewayClass, gatewayTopic, filterCount, clearAndSetMethod, modelName, tmpQueueName, user, filterNames, deleteFilterNames);
            }
        }    
        catch (Exception e)
        {
            Log.log.error("Error deploying filters", e);
            result.setSuccess(false).setMessage(e.getMessage());
        }
        return result;
    }
    
    private ResponseDTO<GatewayVO> deployFilters(ResponseDTO<GatewayVO> result, String gatewayName, String gatewayClass, String gatewayTopic, Long filterCount,
                                                 String clearAndSetMethod, String modelName, String tmpQueueName, Map<String, String> user, String[] filterNames, String[] deleteFilterNames) throws ServletException, IOException {

        String username = user.get("RESOLVE_USERNAME");
        String camelCaseGatewayName= StringUtils.toCamelCase(gatewayName); 
        try
        {            
            Log.log.info(gatewayName + "Gateway:  Filter Deployment --> " + modelName + " Filters Triggered by " + username);

            List<Map<String, String>> filters = new ArrayList<Map<String, String>>();
            
            filters.add(user);
            
            if(!isNewSDK.containsKey(modelName)) {
                isNewSDK.putIfAbsent(modelName, ServiceGateway.isNewSDKGateway(modelName));
            }

            //at this point we'll inject the user who's deploying the filter
            for (String filterName : filterNames) {
                GatewayVO vo = null;
                
                if(!isNewSDK.get(modelName)) {
                    vo = ServiceGateway.getFilterByName(modelName, Constants.DEFAULT_GATEWAY_QUEUE_NAME, filterName);
                } else {
                    int index = modelName.indexOf(FILTER_SUFFIX);
                    if(index != -1) {
                        vo = GatewayFilterUtil.getFilterByName(gatewayName, filterName, Constants.DEFAULT_GATEWAY_QUEUE_NAME);
                    }
                    
                    else {
                        Log.log.info(gatewayName + "Gateway: modelName does not contain 'Filter'.");
                        result.setSuccess(false);
                        return result;
                    }
                }

                if (vo != null) {
                    Map<String, String> filterMap = convert(vo);

                    if(vo instanceof PushGatewayFilterVO) {
                    	if(((PushGatewayFilterVO)vo).getUDeployed() && !((PushGatewayFilterVO)vo).getUUpdated())
                            continue;
//                        filterMap = ((PushGatewayFilterVO)vo).toMap();
//                        filterMap = convert(vo);
                        Map<String, String> attrs = ((PushGatewayFilterVO)vo).getAttrs();
                        if(MapUtils.isNotEmpty(attrs))
                            filterMap.putAll(attrs);
                        gatewayClass = "MPushGateway";
                        filterMap.put("QUEUE", tmpQueueName);
                        clearAndSetMethod = "deployFilters";
                    } else if(vo instanceof PullGatewayFilterVO) {
                    	if(((PullGatewayFilterVO)vo).getUDeployed() && !((PullGatewayFilterVO)vo).getUUpdated())
                            continue;
//                        filterMap = convert(vo); // ((PullGatewayFilterVO)vo).toMap();
                        Map<String, String> attrs = ((PullGatewayFilterVO)vo).getAttrs();
                        if(MapUtils.isNotEmpty(attrs))
                            filterMap.putAll(attrs);
                        gatewayClass = "MPullGateway";
                        filterMap.put("QUEUE", tmpQueueName);
                        clearAndSetMethod = "deployFilters";
                    } else if(vo instanceof MSGGatewayFilterVO) {
                    	if(((MSGGatewayFilterVO)vo).getUDeployed() && !((MSGGatewayFilterVO)vo).getUUpdated())
                            continue;
//                        filterMap = convert(vo); // ((MSGGatewayFilterVO)vo).toMap();
                        Map<String, String> attrs = ((MSGGatewayFilterVO)vo).getAttrs();
                        if(MapUtils.isNotEmpty(attrs))
                            filterMap.putAll(attrs);
                        gatewayClass = "MMSGGateway";
                        filterMap.put("QUEUE", tmpQueueName);
                        clearAndSetMethod = "deployFilters";
                    }
                    filters.add(filterMap);
                }
                
                else
                {
                    Log.log.error(gatewayName + "Gateway: Error in deploying filter(s). Failed to get " + filterName + " of " + modelName);
                    throw new Exception("Error in deploying filter(s). Failed to get " + filterName + " of " + modelName); 
                }
            }
            
            if(filters.size() == 1 && deleteFilterNames.length == 0) {
                String errorMessage = gatewayName + "Gateway: Filter(s) are already deployed.";
                throw new Exception(errorMessage);
            }
            
            Log.log.info(gatewayName + "Gateway: Deploying Filters --> " + Arrays.toString(filterNames));
            String filtersStr = reductFilterMessage(filters.toString());
            Log.log.debug(gatewayName + "Gateway: Deploying filters by sending filters to " + gatewayTopic + ", " + gatewayClass + "." + 
            		clearAndSetMethod + " with filters data [" + filtersStr + "]");
            if(gatewayClass.equals("DEFAULT")) {
                Main.esb.sendInternalMessage(gatewayTopic, "DEFAULT", clearAndSetMethod, filters);
            } else {
                Main.esb.sendInternalMessage(gatewayTopic, gatewayClass + "." + clearAndSetMethod, filters);
            }
            Long deployedFilterCount = getDeployedFilterCount(modelName, tmpQueueName, gatewayName, gatewayClass, username, filterCount, true);
            Log.log.info(gatewayName + "Gateway: deployedFilterCount = " + deployedFilterCount);
            
            //in some situation user may delete all filters. in that case the filterCount
            //will be 0. But if deleteFilterCount is 0 and filterCount1 is 0 then we have
            //a problem. Maybe The QUEUE is not available or wrongly added in the UI because
            //no message went to RSRemote. Or RSRemote failed to send the message to RSControl, or RSControl
            //failed to persist the data, or EHCache blinked to synchronize its caches or DB.
            //as you can see there may a tons of failure that could happen. we just need to
            //tell the user about the possibilities.
            if (deployedFilterCount == null || (deployedFilterCount == 0 && filterCount > 0))
            {
                String errorMessage = gatewayName + "Gateway: Unable to deploy, make sure the following:<br/>" + "1. Check the gateway configuration and make sure it is active.<br/>" + "2. RSRemote is running.<br/>" + "3. Check for possibly incorrect gateway/queue name in blueprint.<br/>";
                throw new Exception(errorMessage);
            }else {
            	Log.log.debug(gatewayName + "Gateway: Gateway Filter deployed successfully");
            }
            
            if(gatewayClass.equals("MPushGateway")) {
                List<GatewayVO> records = PushGatewayFilterUtil.listPushFilters(camelCaseGatewayName, tmpQueueName, true);
                Log.log.debug(gatewayName + "Gateway: deployFilters returning " + records.size() + " for " 
                              + modelName + ", Filters = [" + records + "]");
                result.setRecords(records);
                result.setTotal(records.size());
            }
            
            else if(gatewayClass.equals("MPullGateway")) {
                List<GatewayVO> records = PullGatewayFilterUtil.listPullFilters(camelCaseGatewayName, tmpQueueName, true);
                Log.log.debug(gatewayName + "Gateway: deployFilters returning " + records.size() + " for " 
                              + modelName + ", Filters = [" + records + "]");
                result.setRecords(records);
                result.setTotal(records.size());
            }
            
            else if(gatewayClass.equals("MMSGGateway")) {
                List<GatewayVO> records = MSGGatewayFilterUtil.listMSGFilters(camelCaseGatewayName, tmpQueueName, true);
                Log.log.debug(gatewayName + ": deployFilters returning " + records.size() + " for " 
                              + modelName + ", Filters = [" + records + "]");
                result.setRecords(records);
                result.setTotal(records.size());
            }
            
            else
            {
                //we need to query the DB and place records.
                /*if (deployedFilterCount > 0)
                {*/
                    //The QueryDTO will provide a HQL statement like:
                    //select tableData from EmailFilter tableData  where UQueue='MyQueue'
                    //HibernateUtil.evict("com.resolve.persistence.model." + modelName);
                    QueryDTO query = new QueryDTO();
                    query.setModelName(modelName);
                    query.setWhereClause("UQueue='" + tmpQueueName + "'");

                    List<GatewayVO> records = ServiceGateway.getAllFilter(query, username);
                    Log.log.debug(gatewayName + "Gateway: deployFilters returning " + records.size() + " for " 
                                  + modelName + ", Filters = [" + records + "]");
                    result.setRecords(records);
                    result.setTotal(records.size());
                /*}*/
            }
            result.setSuccess(true);
        }
        catch (Exception e)
        {
            Log.log.error(gatewayName +"Gateway: Error deploying filters", e);
            result.setSuccess(false).setMessage("Error deploying filters");
        }
        
        return result;
    }
    
    private static String reductFilterMessage(String originalMsg) {
    	String resultMsg = originalMsg;
    	Matcher m = PASSWORD_KEY_VALUE.matcher(originalMsg);
		while (m.find()) {
			String pair = m.group();
			String[] keyValue = pair.split("=");
			String reducted = String.format("%s=%s,", keyValue[0], STARS);
			resultMsg = resultMsg.replace(pair, reducted);
		}
		
		return resultMsg;
    }
    
    private ResponseDTO<GatewayVO> undeployFilters(ResponseDTO<GatewayVO> result, 
    		String gatewayName, String gatewayClass, String gatewayTopic, 
    		Long deleteFilterCount, Long filterCount, String clearAndSetMethod,
            String modelName, String tmpQueueName, Map<String, String> user, 
            String[] deleteFilterNames) throws ServletException, IOException {
        
        String username = user.get("RESOLVE_USERNAME");
        
        try
        {            
            Log.log.info(gatewayName + "Gateway: Filter undeploy for " + modelName + " Filters Triggered by " + username);

            List<Map<String, String>> filters = new ArrayList<Map<String, String>>();
            
            filters.add(user);

            for (String filterName : deleteFilterNames)
            {
                GatewayVO vo = null;

                int index = modelName.indexOf("Filter");
                if(index != -1) {
                    vo = GatewayFilterUtil.getFilterByName(gatewayName, filterName, tmpQueueName);
                }
                
                else {
                    Log.log.debug(gatewayName + "Gateway::" + modelName + " --> modelName does not contain 'Filter'.");
                    result.setSuccess(false);
                    return result;
                }

                if (vo != null)
                {
                    Map<String, String> filterMap = convert(vo);
                    
                    filterMap.put("QUEUE", tmpQueueName);
                    clearAndSetMethod = "undeployFilters";
                    Log.log.debug(gatewayName + "Gateway: Adding filter for undeploying with data--> " + filterMap.toString());
                    filters.add(filterMap);

                    if(vo instanceof PushGatewayFilterVO)
                        gatewayClass = "MPushGateway";
                    
                    else if(vo instanceof PullGatewayFilterVO)
                        gatewayClass = "MPullGateway";
                    
                    else if(vo instanceof MSGGatewayFilterVO)
                        gatewayClass = "MMSGGateway";
                }
                
                else
                {
                	String errMsg = String.format("%s: Error in undeploying filter(s). Failed to get filter named %s " +
                								  "of model %s for queue %s", gatewayName, filterName, modelName,
                								  tmpQueueName);
                    Log.log.error(errMsg);
                    throw new Exception(errMsg); 
                }
            }
            
            Log.log.trace(gatewayName + "Gateway: Ajax Undeploying Filters: " + deleteFilterNames.toString());
            Log.log.debug(gatewayName + "Gateway: Sending message to " + gatewayTopic + ", " + gatewayClass + "." + clearAndSetMethod + " with filters [" + filters + "]");
            if (gatewayClass.equals("DEFAULT")) {
                Main.esb.sendInternalMessage(gatewayTopic, "DEFAULT", clearAndSetMethod, filters);
            } else {
                Main.esb.sendInternalMessage(gatewayTopic, gatewayClass + "." + clearAndSetMethod, filters);
            }
            Long undeployedFilterCount = getDeployedFilterCount(modelName, tmpQueueName, gatewayName, gatewayClass, username, filterCount, false);
            
            if (undeployedFilterCount == 0 && filterCount != 0)
            {
                String errorMessage = gatewayName + "Gateway: Unable to deploy, make sure the following:<br/>" + "1. Check the gateway configuration and make sure it is active.<br/>" + "2. RSRemote is running.<br/>" + "3. Check for possibly incorrect gateway/queue name in blueprint.<br/>";
                throw new Exception(errorMessage);
            }
            
            if(gatewayClass.equals("MPushGateway")) {
                List<GatewayVO> records = PushGatewayFilterUtil.listPushFilters(gatewayName, tmpQueueName, true);
                Log.log.debug(gatewayName + "Gateway: deployFilters returning " + records.size() + " for " 
                              + modelName + ", Filters = [" + records + "]");
                result.setRecords(records);
                result.setTotal(records.size());
            }
            
            else if(gatewayClass.equals("MPullGateway")) {
                List<GatewayVO> records = PullGatewayFilterUtil.listPullFilters(gatewayName, tmpQueueName, true);
                Log.log.debug(gatewayName + "Gateway: deployFilters returning " + records.size() + " for " 
                              + modelName + ", Filters = [" + records + "]");
                result.setRecords(records);
                result.setTotal(records.size());
            }
            
            else if(gatewayClass.equals("MMSGGateway")) {
                List<GatewayVO> records = MSGGatewayFilterUtil.listMSGFilters(gatewayName, tmpQueueName, true);
                Log.log.debug(gatewayName + "Gateway: deployFilters returning " + records.size() + " for " 
                              + modelName + ", Filters = [" + records + "]");
                result.setRecords(records);
                result.setTotal(records.size());
            }
            
            else
            {
                QueryDTO query = new QueryDTO();
                query.setModelName(modelName);
                query.setWhereClause("UQueue='" + tmpQueueName + "'");

                List<GatewayVO> records = ServiceGateway.getAllFilter(query, username);
                Log.log.debug(gatewayName + "Gateway:: undeployFilters returning " + records.size() + " for " 
                              + modelName + ", Filters = [" + records + "]");
                result.setRecords(records);
                result.setTotal(records.size());
            }
            
            result.setSuccess(true);
        }
        catch (Exception e)
        {
            Log.log.error(gatewayName + "Gateway: Error undeploying filters", e);
            result.setSuccess(false).setMessage(e.getMessage());
        }
        
        return result;
    }

    /**
     * This method returns a <code>Map</code> of different database drivers (a fully qualified driver class name). E.g. driver for MariaDB is <code>org.mariadb.jdbc.Driver</code>
     * <p>
     * @param request
     * @param response
     * @return
     * @throws ServletException
     * @throws IOException
     */
    @RequestMapping(value = "/gateway/getDatabaseDrivers", method = { RequestMethod.GET }, headers = "Accept=application/json; charset=utf-8")
    @ResponseBody
    public ResponseDTO<Map<String, String>> getDatabaseDrivers(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException
    {
        ResponseDTO<Map<String, String>> result = new ResponseDTO<Map<String, String>>();
        Map<String, String> drivers = new HashMap<String, String>();

        //drivers.put("MySQL Driver", "com.mysql.jdbc.Driver");
        drivers.put("MariaDB Driver", Constants.MARIADBDRIVER);
        drivers.put("Oracle Driver", Constants.ORACLEDRIVER);
        drivers.put("DB2 Driver", Constants.DB2DRIVER);
        drivers.put("MSSQL Server Driver", Constants.MSSQLDRIVER);
        drivers.put("PostgeSQL Driver", Constants.POSTGRESQLDRIVER);

        result.setData(drivers);
        result.setSuccess(true);
        return result;
    }

    @RequestMapping(value = "/gateway/listObjectNames", method = { RequestMethod.GET,RequestMethod.POST }, headers = "Accept=application/json; charset=utf-8")
    @ResponseBody
    public ResponseDTO<String> listObjectNames(@RequestParam("gatewayName") String gatewayName, HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException
    {
        ResponseDTO<String> result = new ResponseDTO<String>();
        try
        {
            if (StringUtils.isBlank(gatewayName))
            {
                throw new Exception("gatewayName must be provided");
            }
            else
            {
                //LinkedHashSet is serializable
                Set<String> items = new TreeSet<String>();

                List<ResolveBlueprintVO> blueprints = ServiceHibernate.findAllResolveBlueprint();
                for (ResolveBlueprintVO blueprintVO : blueprints)
                {
                    Properties properties = new Properties();
                    properties.load(StringUtils.toInputStream(blueprintVO.getUBlueprint()));
                    gatewayName = gatewayName.toLowerCase();
                    String tmpModelname = gatewayName.toLowerCase();
                    if (GatewayUtil.GATEWAY_EXCEPTION_MAPPING.containsKey(gatewayName))
                    {
                        tmpModelname = GatewayUtil.GATEWAY_EXCEPTION_MAPPING.get(gatewayName);
                    }
                    //for example we want to check property like rsremote.receive.email.active=true
                    String supportedObjects = "rsremote.receive." + tmpModelname + ".objects";
                    String value = properties.getProperty(supportedObjects);
                    if (StringUtils.isNotBlank(value))
                    {
                        String[] objects = value.split(",");
                        for(String object : objects)
                        {
                            items.add(object.trim());
                        }
                    }
                }
                
                List<String> itemList = new ArrayList<String>();
                itemList.addAll(items);
                result.setRecords(itemList);
                result.setTotal(items.size());
            }
        }
        catch (Exception e)
        {
            Log.log.error("Error retrieving supported objects from resolve_blueprint table", e);
            result.setSuccess(false).setMessage("Error retrieving supported objects");
        }
        return result;
    }
    
    @SuppressWarnings({ "rawtypes", "unchecked" })
    @RequestMapping(value = "/gateway/getCustomSDKFields", method = { RequestMethod.GET }, headers = "Accept=application/json; charset=utf-8")
    @ResponseBody
    public ResponseDTO getCustomSDKFields(@RequestParam("customGatewayName") String customGatewayName, HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException
    {
        ResponseDTO result = new ResponseDTO();
        try
        {
            
            String sdkGatewayType = ServiceGateway.getSDKGatewayType(customGatewayName.toUpperCase());
           
            Map<String, List<Map<String, String>>> fieldsStd =   getCommonFields(sdkGatewayType); 
            Map<String, Object> fieldsMap = ServiceGateway.getCustomSDKFields(customGatewayName, RSContext.getResolveHome());
            
            if(fieldsStd.isEmpty()){
                throw new Exception("Error retrieving custom fields..Gateway Type Can't be specified.");
            }
            List<Map> standard = (List)fieldsStd.get("records");
            List<Map> attr = (List)fieldsMap.get("records");
            
            List finalList = new ArrayList();
            finalList.addAll(standard);
            if(attr != null) {
                finalList.addAll(attr);
            }
            result.setData (fieldsMap.get("config"));
            result.setRecords(finalList);
//            result.setRecords((List)fieldsMap.get("records"));
            result.setTotal(finalList.size());
            if (fieldsMap.size() > 0)
            {
                result.setSuccess(true);
            }
            else
            {
                result.setMessage("No custom fields found for: " + customGatewayName);
                result.setSuccess(false);
            }
        }
        catch (Exception e)
        {
            result.setSuccess(false);
            result.setMessage("Error retrieving custom fields");
            Log.log.error(e.getMessage(), e);
        }
        return result;
    }
    
    
 private Map<String, List<Map<String, String>>> getCommonFields(String gatewayType){
        
        Map<String, List<Map<String, String>>> commonAttrs = new HashMap<String, List<Map<String, String>>>();
        
        //Build the Strink Token for default or Pull gateway
        StringTokenizer commonAttrFields = new StringTokenizer("Name, Order, RunBook, Active, Resolve Event ID, "
                                                            + " Interval", ",");
        
        if(gatewayType.equalsIgnoreCase("push"))
                commonAttrFields = new StringTokenizer("Name, Order, RunBook, Active, Resolve Event ID, "
                            + " URI, Port, SSL, Block Call", ",");
        else if(gatewayType.equalsIgnoreCase("msg"))
        {
            commonAttrFields = new StringTokenizer("Name, Order, RunBook, Active, Resolve Event ID", ",");
        
        }
        List<Map<String, String>> recordsList = new ArrayList<Map<String, String>>();
        while(commonAttrFields.hasMoreTokens()) {
            recordsList.add(getMainFiledMap(commonAttrFields.nextToken().trim(), gatewayType.trim()));
         }
        commonAttrs.put("records", recordsList);
        return commonAttrs;
    }
    
    private Map<String, String> getMainFiledMap(String disName, String gwType){
        
        Map<String, String> recData = new HashMap<String, String>();
        recData.put("DISPLAYNAME", disName);
        recData.put("STANDARD", "TRUE");
        switch(disName.trim()) {
            case "Name":
                recData.put("POSITION", "left");
                recData.put("NAME", "name");
                recData.put("VALUES", "");
                recData.put("TYPE", "text");
                recData.put("REQUIRED", "true");
                break;
            case "Order":
                recData.put("POSITION", "left");
                recData.put("NAME", "order");
                recData.put("DEFAULTVALUE", "1");
                recData.put("VALUES", "");
                recData.put("TYPE", "text");
                break;
            case "RunBook":
                recData.put("POSITION", "right");
                recData.put("NAME", "runbook");
                recData.put("VALUES", "");
                recData.put("TYPE", "text");
                break;
            case "Active":
                recData.put("POSITION", "left");
                recData.put("NAME", "active");
                recData.put("DEFAULTVALUE", "true");
                recData.put("VALUES", "");
                recData.put("TYPE", "checkbox");
                break;
            case "Resolve Event ID":
                recData.put("POSITION", "right");
                recData.put("NAME", "eventEventId");
                recData.put("VALUES", "");
                recData.put("TYPE", "text");
                break;
            case "Interval":
                recData.put("POSITION", "right");
                recData.put("NAME", "interval");
                recData.put("DEFAULTVALUE", "10");
                recData.put("VALUES", "");
                recData.put("TYPE", "text");
                recData.put("REQUIRED", "true");
                break;
            case "URI":
                recData.put("POSITION", "left");
                recData.put("NAME", "uri");
                recData.put("VALUES", "");
                recData.put("TYPE", "text");
                recData.put("REQUIRED", "true");
                break;
            case "Port":
                recData.put("POSITION", "right");
                recData.put("NAME", "port");
                recData.put("VALUES", "");
                recData.put("TYPE", "text");
                recData.put("REQUIRED", "true");
                break;
            case "SSL":
                recData.put("POSITION", "left");
                recData.put("NAME", "ssl");
                recData.put("VALUES", "");
                recData.put("TYPE", "checkbox");
                break;
            case "Block Call":
                recData.put("POSITION", "right");
                recData.put("NAME", "blocking");
                recData.put("VALUES", "Default,Gateway Script,Worksheet ID,Execution Complete");
                recData.put("TYPE", "list");
                recData.put("DEFAULTVALUE", "Default");
                break;
            case "Queue/Topic Name":
                recData.put("POSITION", "left");
                recData.put("NAME", "");
                recData.put("VALUES", "");
                recData.put("TYPE", "text");
                recData.put("REQUIRED", "true");
                break;
            case "Type":
                recData.put("POSITION", "right");
                recData.put("NAME", "type");
                recData.put("VALUES", "Queue,Topic");
                recData.put("TYPE", "list");
                recData.put("REQUIRED", "true");
                break;
            case "Acknowledgement":
                recData.put("POSITION", "left");
                recData.put("NAME", "AUTO_ACKNOWLEDGE,NO_ACKNOWLEDGE");
                recData.put("VALUES", "");
                recData.put("TYPE", "list");
                break;
            default:
                break;
        }
            
        return recData;
    }   
    @RequestMapping(value = "/gateway/getCustomGatewayNames", method = { RequestMethod.GET }, headers = "Accept=application/json; charset=utf-8")
    @ResponseBody
    public ResponseDTO<Map<String, String>> getCustomGatewayNames(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException
    {
        ResponseDTO<Map<String, String>> result = new ResponseDTO<Map<String, String>>();
        try
        {
            Map<String, String> customGatewayNameMap = ServiceGateway.getCustomaGatewayNames(RSContext.getResolveHome());
            if (customGatewayNameMap.isEmpty())
            {
                result.setMessage("No custom gateway found.");
                result.setSuccess(false);
            }
            else
            {
                result.setData(customGatewayNameMap);
                result.setSuccess(true);
            }
        }
        catch (Exception e)
        {
            result.setSuccess(false);
            result.setMessage("Error retrieving custom gateway");
            Log.log.error(e.getMessage(), e);
        }
        
        return result;
    }
    
    @SuppressWarnings("rawtypes")
    @RequestMapping(value = "/gateway/getDefaultInterval", method = { RequestMethod.GET }, headers = "Accept=application/json; charset=utf-8")
    @ResponseBody
    public ResponseDTO getDefaultInterval(@RequestParam("gatewayName") String gatewayName, HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException
    {
        ResponseDTO<Integer> result = new ResponseDTO<Integer>();
        
        try
        {
            int interval = ServiceGateway.getDefaultInterval(gatewayName, RSContext.getResolveHome());
            result.setData (new Integer(interval));
            result.setSuccess(true);
        }
        catch (Exception e)
        {
            result.setSuccess(false);
            result.setMessage(e.getMessage());
            Log.log.error(e.getMessage(), e);
        }
        
        return result;
    }
    
    
    @SuppressWarnings("rawtypes")
    @RequestMapping(value = "/gateway/getHttpAuthType", method = { RequestMethod.GET }, headers = "Accept=application/json; charset=utf-8")
    @ResponseBody
    public ResponseDTO getHttpAuthType(@RequestParam("gatewayName") String gatewayName, HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException
    {
        ResponseDTO<String> result = new ResponseDTO<String>();
        
        try
        {
            String authType = ServiceGateway.getHttpAuthType(gatewayName, RSContext.resolveHome);
            result.setData (authType);
            result.setSuccess(true);
        }
        catch (Exception e)
        {
            result.setSuccess(false);
            result.setMessage(e.getMessage());
            Log.log.error(e.getMessage(), e);
        }
        
        return result;
    }
    
    
    private Long getDeployedFilterCount(String modelName, String queueName, String gatewayName, String gatewayClass, String userName, Long expectedCount, boolean deployed)
    {
        Long result = null;
        //Weird way to take the name of the gateway - tightly coupled to specific setup in the blueprint.properties
        //TODO: we must think of a way to detach this.
        String camelCaseGatewayName= StringUtils.toCamelCase(gatewayName); 
        try
        {
            for (int i = 0; i < RETRIES; i++)
            {
                if (gatewayClass.equals("MPushGateway"))
                {
                    result = GatewayFilterUtil.getDeployedGatewayFilterCount(GatewayType.PUSH.getEntityFilterClass(), queueName, camelCaseGatewayName);
                }

                else if (gatewayClass.equals("MPullGateway"))
                {
                    result = GatewayFilterUtil.getDeployedGatewayFilterCount(GatewayType.PULL.getEntityFilterClass(), queueName, camelCaseGatewayName);
                }
                else if (gatewayClass.equals("MMSGGateway"))
                {
                    result = GatewayFilterUtil.getDeployedGatewayFilterCount(GatewayType.MSG.getEntityFilterClass(), queueName, camelCaseGatewayName);
                }
                else
                {
                  HibernateProxy.setCurrentUser((String) userName);
                    result = (Long) HibernateProxy.execute(() -> {
                    	return HibernateUtil.createQuery("select count(*) from " + modelName + " where UQueue='" + queueName + "'").list().get(0);
                    });
                }
                
                if (expectedCount.equals(result))
                    break;

                Thread.sleep(SLEEPTIME); //just wait a while
            }
        }
        catch (Throwable t)
        {
            Log.log.error(t.getMessage(), t);
                      HibernateUtil.rethrowNestedTransaction(t);
        }

        return result;
    }

    /**
     * Converts a VO to a Map.
     *
     * @param filterVo
     * @return
     */
    private Map<String, String> convert(Object filterVo)
    {
        Map<String, String> result = new HashMap<String, String>();

        Method[] methods = filterVo.getClass().getMethods();
        for (Method method : methods)
        {
            //we're only interested in the getters as we make sure annotations are
            //specified in getters only.
            if (method.getName().startsWith("get"))
            {
                MappingAnnotation mappingAnnotation = method.getAnnotation(MappingAnnotation.class);
                if (mappingAnnotation != null)
                {
                    try
                    {
                        String columnName = mappingAnnotation.columnName();
                        String propertyName = method.getName().substring(3); //ignore "get"
                        Log.log.trace("Method:" + method.getName() + ", Column:" + columnName + 
                        			  ", Property Name:" + propertyName + ", Filter Object:" + filterVo);
                        
                        try {
                        	result.put(columnName, BeanUtil.getStringProperty(filterVo, propertyName));
                        } catch (NoSuchMethodException nsme){
                        	result.put(columnName, (String) method.invoke(filterVo, (Object[])null));                   	
                        }
                    }
                    catch (Exception e)
                    {
                        Log.log.warn(e.getMessage(), e);
                    }
                }
            }
        }
        return result;
    }
    
    private Map<String, Map<String, String>> getFilterVOMaps (final String[] deployFilters, 
    														  final String[] unDeployFilters,
    														  final String modelName,
    														  final String gatewayName,
    														  final String qName) {
    	final Map<String, Map<String, String>> filterVOMaps = new HashMap<String, Map<String, String>>();
    	
    	Set<String> uniqueFilterNames = new HashSet<String>();
    	
    	if (deployFilters != null && deployFilters.length > 0) {
    		Arrays.asList(deployFilters).stream().forEach(filterName -> uniqueFilterNames.add(filterName));
    	}
    	
    	if (unDeployFilters != null && unDeployFilters.length > 0) {
    		Arrays.asList(unDeployFilters).stream().forEach(filterName -> uniqueFilterNames.add(filterName));
    	}
    	
    	if (CollectionUtils.isNotEmpty(uniqueFilterNames)) {
    		uniqueFilterNames.parallelStream().forEach(uniqueFilterName -> {
    			GatewayVO vo = null;
    			String tModelName = modelName;
                
    			try {
    				if(!isNewSDK.containsKey(modelName)) {
    	                isNewSDK.putIfAbsent(modelName, ServiceGateway.isNewSDKGateway(modelName));
    	            }
    				
	                if(!isNewSDK.get(modelName)) {
	                    vo = ServiceGateway.getFilterByName(modelName, Constants.DEFAULT_GATEWAY_QUEUE_NAME, 
	                    									uniqueFilterName);
	                } else {
	                    if(modelName.indexOf(FILTER_SUFFIX) != -1) {
	                        vo = GatewayFilterUtil.getFilterByName(gatewayName, uniqueFilterName, 
	                        									   Constants.DEFAULT_GATEWAY_QUEUE_NAME);
	                    } else {
	                        Log.log.warn(String.format("%s Gateway: %s does not contain '%s'.", gatewayName, modelName, 
	                        						   FILTER_SUFFIX));
	                    }
	                }
	
	                if (vo != null) {
	                    Map<String, String> filterMap = convert(vo);
	
	                    if (vo instanceof PushGatewayFilterVO) {
	                    	if (((PushGatewayFilterVO)vo).getUDeployed() && !((PushGatewayFilterVO)vo).getUUpdated()) {
	                    		filterVOMaps.put(uniqueFilterName, Collections.emptyMap());
	                            return;
	                        }
	                    	
	                        Map<String, String> attrs = ((PushGatewayFilterVO)vo).getAttrs();
	                        
	                        if(MapUtils.isNotEmpty(attrs)) {
	                            filterMap.putAll(attrs);
	                        }
	                        
	                        filterMap.put(FILTER_MAP_KEY_GATEWAY_CLASS, PUSH_GATEWAY_CLASS_NAME);
	                        filterMap.put(FILTER_MAP_KEY_IS_SDK2, Boolean.toString(true));
	                    } else if (vo instanceof PullGatewayFilterVO) {
	                    	if (((PullGatewayFilterVO)vo).getUDeployed() && !((PullGatewayFilterVO)vo).getUUpdated()) {
	                    		filterVOMaps.put(uniqueFilterName, Collections.emptyMap());
	                            return;
	                    	}
	                    	
	                        Map<String, String> attrs = ((PullGatewayFilterVO)vo).getAttrs();
	                        
	                        if (MapUtils.isNotEmpty(attrs)) {
	                            filterMap.putAll(attrs);
	                        }
	                        
	                        filterMap.put(FILTER_MAP_KEY_GATEWAY_CLASS, PULL_GATEWAY_CLASS_NAME);
	                        filterMap.put(FILTER_MAP_KEY_IS_SDK2, Boolean.toString(true));
	                    } else if (vo instanceof MSGGatewayFilterVO) {
	                    	if (((MSGGatewayFilterVO)vo).getUDeployed() && !((MSGGatewayFilterVO)vo).getUUpdated()) {
	                    		filterVOMaps.put(uniqueFilterName, Collections.emptyMap());
	                            return;
	                    	}
	                    	
	                        Map<String, String> attrs = ((MSGGatewayFilterVO)vo).getAttrs();
	                        
	                        if(MapUtils.isNotEmpty(attrs)) {
	                            filterMap.putAll(attrs);
	                        }
	                        
	                        filterMap.put(FILTER_MAP_KEY_GATEWAY_CLASS, MSG_GATEWAY_CLASS_NAME);
	                        filterMap.put(FILTER_MAP_KEY_IS_SDK2, Boolean.toString(true));
	                    } else {
	                    	if (!StringUtils.substringBefore(vo.getClass().getSimpleName(), 
	                    									 VO.class.getSimpleName()).equals(modelName)) {
	                    		tModelName = StringUtils.substringBefore(vo.getClass().getSimpleName(), 
	                    												 VO.class.getSimpleName());
	                    		filterMap.put(FILTER_MAP_KEY_GATEWAY_COMPANION_MODEL_CLASS_NAME, tModelName);
	                    	}
	                    	
	                    	filterMap.put(FILTER_MAP_KEY_GATEWAY_CLASS, "DEFAULT");
	                    }
	                    
	                    /*
	                     * Add keys for operation name if model is secondary i.e. filter companion model 
	                     * like DatabaseConnectionPool
	                     */
	                    
	                    Pair<String, Map<String, String>> fcmDetail = 
	                    									ServiceGateway.getFilterCompanionModelDetails(
	                    										Main.main, tModelName, qName);
	                    
	                    if (fcmDetail != null) {
	                    	filterMap.put(FILTER_MAP_KEY_COMPANION_MODEL_ROOT_MESSAGE_HANDLER,
	                    				  fcmDetail.getLeft());
	                    	
	                    	if (StringUtils.isNotBlank(fcmDetail.getRight().get(Constants.FILTER_OPERATION_CLEARANDSET))) {
	                    		filterMap.put(FILTER_MAP_KEY_COMPANION_MODEL_CLEARANDSET_METHOD,
	                    					  fcmDetail.getRight().get(Constants.FILTER_OPERATION_CLEARANDSET));
	                    	}
	                    	
	                    	if (StringUtils.isNotBlank(fcmDetail.getRight().get(Constants.FILTER_OPERATION_DEPLOY))) {
	                    		filterMap.put(FILTER_MAP_KEY_COMPANION_MODEL_DEPLOY_METHOD,
	                    					  fcmDetail.getRight().get(Constants.FILTER_OPERATION_DEPLOY));
	                    	}
	                    	
	                    	if (StringUtils.isNotBlank(fcmDetail.getRight().get(Constants.FILTER_OPERATION_UNDEPLOY))) {
	                    		filterMap.put(FILTER_MAP_KEY_COMPANION_MODEL_UNDEPLOY_METHOD,
	                    					  fcmDetail.getRight().get(Constants.FILTER_OPERATION_UNDEPLOY));
	                    	}
	                    }
	                    
	                    filterVOMaps.put(uniqueFilterName, filterMap);
	                } else {
	                    Log.log.error(String.format("%s Gateway: Failed to get FilterVO Map for filter named %s and " +
	                    							"model %s", gatewayName, uniqueFilterName, modelName));
	                }
    			} catch (Throwable t) {
    				Log.log.error(String.format("Error %sin getting FilterVO Map for filter named %s",
												(StringUtils.isNotBlank(t.getMessage()) ? 
												 "[" + t.getMessage() + "] " : ""), uniqueFilterName));
    			}
    		});
    	}
    	
    	return filterVOMaps;
    }
    
    // Remove marker fields FILTER_MAP_KEY_GATEWAY_CLASS, FILTER_MAP_KEY_IS_SDK2 from filter VOs
    private void removeMarkerFieldsFromFilterVOMaps (final List<Map<String, String>> filterVOMaps) {
    	filterVOMaps.stream().forEach(filterVO ->  {
    		filterVO.remove(FILTER_MAP_KEY_GATEWAY_CLASS);
    		filterVO.remove(FILTER_MAP_KEY_IS_SDK2);
    		filterVO.remove(FILTER_MAP_KEY_COMPANION_MODEL_ROOT_MESSAGE_HANDLER);
    		filterVO.remove(FILTER_MAP_KEY_COMPANION_MODEL_CLEARANDSET_METHOD);
    		filterVO.remove(FILTER_MAP_KEY_COMPANION_MODEL_DEPLOY_METHOD);
    		filterVO.remove(FILTER_MAP_KEY_COMPANION_MODEL_UNDEPLOY_METHOD);
    		filterVO.remove(FILTER_MAP_KEY_GATEWAY_COMPANION_MODEL_CLASS_NAME);
    	});
    }
    
    private void submitDeployFiltersRequest (final Map<String, Boolean> opResultsByQNames, final String qName, 
    										 final String gatewayClass, final String[] deployFilters, 
    										 final String[] unDeployFilters, final Map<String, String> userFilter, 
    										 final Map<String, Map<String, String>> filterVOMaps) {
    	if (Log.log.isDebugEnabled()) {
    		Log.log.debug(String.format("%s Queue : Submit Deploy Filter(s) Request to Gateway Class %s --> Deploy(%d) " +
    								    "[%s] Filters, UnDeploy(%d) [%s] Triggered by %s", 
    								    qName, gatewayClass, deployFilters.length, 
    								    StringUtils.arrayToString(deployFilters, ", "), 
    								    (unDeployFilters != null ? unDeployFilters.length : 0),
    								    (unDeployFilters != null ? StringUtils.arrayToString(unDeployFilters, ", ") : ""),
    								    userFilter.get(FILTER_OPERATION_BY_USER_FIELD_NAME)));
    	}
    	
    	List<Map<String, String>> filtersToDeploy = new ArrayList<Map<String, String>>();
    	
    	if (deployFilters != null && deployFilters.length > 0) {
    		Arrays.asList(deployFilters).stream().forEach(filterName -> {
    			Map<String, String> filterVOMapForQ = new HashMap<String, String>();
    			filterVOMapForQ.putAll(filterVOMaps.get(filterName));
    			filterVOMapForQ.put(FILTER_MAP_KEY_QUEUE, qName.toUpperCase());
    			filtersToDeploy.add(filterVOMapForQ);
    		});
    	}
    	
    	// Inject user as filter, RSRemote container assigns user initiating clear and set operation on filters
    	filtersToDeploy.add(0, userFilter);
    	
    	String filterGatewayClass = gatewayClass;
    	boolean isSDK2 = false;
    	boolean isFCM = false;
    	String operation = GATEWAY_CLEAR_AND_SET_FILTERS_OPERATION;
    	
    	if (filtersToDeploy.size() > 1) {
    		filterGatewayClass = filtersToDeploy.get(1).get(FILTER_MAP_KEY_GATEWAY_CLASS);
    		
    		if (StringUtils.isNotBlank(filtersToDeploy.get(1).get(FILTER_MAP_KEY_IS_SDK2)) && 
    			Boolean.parseBoolean(filtersToDeploy.get(1).get(FILTER_MAP_KEY_IS_SDK2))) {
    			isSDK2 = true;
    			operation = GATEWAY_DEPLOY_FILTERS_OPERATION;
    		}
    		
    		if (StringUtils.isNotBlank(filtersToDeploy.get(1).get(FILTER_MAP_KEY_COMPANION_MODEL_ROOT_MESSAGE_HANDLER))) {
    			isFCM = true;
    			
    			filterGatewayClass = filtersToDeploy.get(1).get(FILTER_MAP_KEY_COMPANION_MODEL_ROOT_MESSAGE_HANDLER);
    			
    			if (isSDK2) {
    				operation = filtersToDeploy.get(1).get(FILTER_MAP_KEY_COMPANION_MODEL_DEPLOY_METHOD);
    			} else {
    				operation = filtersToDeploy.get(1).get(FILTER_MAP_KEY_COMPANION_MODEL_CLEARANDSET_METHOD);
    			}
    		}
    	} else  if (unDeployFilters != null && unDeployFilters.length > 0) {
    		filterGatewayClass = filterVOMaps.get(unDeployFilters[0]).get(FILTER_MAP_KEY_GATEWAY_CLASS);
    		
    		if (StringUtils.isNotBlank(filterVOMaps.get(unDeployFilters[0]).get(FILTER_MAP_KEY_IS_SDK2)) && 
        		Boolean.parseBoolean(filterVOMaps.get(unDeployFilters[0]).get(FILTER_MAP_KEY_IS_SDK2))) {
        		isSDK2 = true;
        		operation = GATEWAY_DEPLOY_FILTERS_OPERATION;
        	}
    		
    		if (StringUtils.isNotBlank(filterVOMaps.get(unDeployFilters[0])
    								   .get(FILTER_MAP_KEY_COMPANION_MODEL_ROOT_MESSAGE_HANDLER))) {
    			isFCM = true;
    			
    			filterGatewayClass = filterVOMaps.get(unDeployFilters[0])
    								 .get(FILTER_MAP_KEY_COMPANION_MODEL_ROOT_MESSAGE_HANDLER);
    			
    			if (isSDK2) {
    				operation = filterVOMaps.get(unDeployFilters[0]).get(FILTER_MAP_KEY_COMPANION_MODEL_DEPLOY_METHOD);
    			} else {
    				operation = filterVOMaps.get(unDeployFilters[0]).get(FILTER_MAP_KEY_COMPANION_MODEL_CLEARANDSET_METHOD);
    			}
    		}
    	}
    	
    	if (Log.log.isDebugEnabled()) {
    		Log.log.debug(String.format("Sending message to %s_TOPIC, method %s.%s, %d elements", qName.toUpperCase(),
    									filterGatewayClass, operation, filtersToDeploy.size()));
    	}
    	
    	removeMarkerFieldsFromFilterVOMaps(filtersToDeploy);
    	
    	boolean result = Main.esb.sendInternalMessage(String.format("%s_TOPIC", qName.toUpperCase()), 
    								 				  String.format("%s.%s", filterGatewayClass, operation), 
    								 				  filtersToDeploy);
    	
    	opResultsByQNames.put(qName, Boolean.valueOf(result));
    }
    
    private void submitUnDeployFiltersRequest (final Map<String, Boolean> opResultsByQNames, final String qName, 
			 								   final String gatewayClass, final String[] unDeployFilters, 
			 								   final Map<String, String> userFilter, 
			 								   final Map<String, Map<String, String>> filterVOMaps) {
    	if (Log.log.isDebugEnabled()) {
    		Log.log.debug(String.format("%s Queue : Submit Undeploy Filter(s) Request to Gateway Class %s --> " +
    								    "UnDeploy(%d) [%s] Filters Triggered by %s", qName, gatewayClass, 
    								    unDeployFilters.length, StringUtils.arrayToString(unDeployFilters, ", "), 
    								    userFilter.get(FILTER_OPERATION_BY_USER_FIELD_NAME)));
    	}

    	List<Map<String, String>> filtersToUnDeploy = new ArrayList<Map<String, String>>();

    	if (unDeployFilters != null && unDeployFilters.length > 0) {
    		Arrays.asList(unDeployFilters).stream().forEach(filterName -> {
    			Map<String, String> filterVOMapForQ = new HashMap<String, String>();
    			filterVOMapForQ.putAll(filterVOMaps.get(filterName));
    			filterVOMapForQ.put(FILTER_MAP_KEY_QUEUE, qName.toUpperCase());
    			filtersToUnDeploy.add(filterVOMapForQ);
    		});
    	}

    	// Inject user as filter, RSRemote container assigns user initiating clear and set operation on filters
    	filtersToUnDeploy.add(0, userFilter);

    	String filterGatewayClass = gatewayClass;
    	String operation = GATEWAY_UNDEPLOY_FILTERS_OPERATION;

    	if (filtersToUnDeploy.size() > 1) {
    		filterGatewayClass = filtersToUnDeploy.get(1).get(FILTER_MAP_KEY_GATEWAY_CLASS);
    		
    		if (StringUtils.isNotBlank(filtersToUnDeploy.get(1).get(FILTER_MAP_KEY_COMPANION_MODEL_ROOT_MESSAGE_HANDLER))) {
    			filterGatewayClass = filtersToUnDeploy.get(1).get(FILTER_MAP_KEY_COMPANION_MODEL_ROOT_MESSAGE_HANDLER);    			
    			operation = filtersToUnDeploy.get(1).get(FILTER_MAP_KEY_COMPANION_MODEL_UNDEPLOY_METHOD);
    		}
    	
    		if (Log.log.isDebugEnabled()) {
    			Log.log.debug(String.format("Sending message to %s_TOPIC, method %s.%s, %d elements", qName.toUpperCase(),
    										filterGatewayClass, operation, filtersToUnDeploy.size()));
    		}
    	
    		removeMarkerFieldsFromFilterVOMaps(filtersToUnDeploy);
    			
    		boolean result = Main.esb.sendInternalMessage(String.format("%s_TOPIC", qName.toUpperCase()), 
					 				  				  	  String.format("%s.%s", filterGatewayClass, operation), 
					 				  				  	  filtersToUnDeploy);
    	
    		opResultsByQNames.put(qName, Boolean.valueOf(result));
    	} else {
    		opResultsByQNames.put(qName, Boolean.TRUE); 
    	}
    }
    
    private void clearAndSetFilters(final Map<String, Boolean> opResultsByQNames, final String modelName, 
    								final String qName, final String gatewayName, final String gatewayClass, 
    								final String[] deployFilters, final String[] unDeployFilters,
    								final Map<String, String> userFilter, 
    								final Map<String, Map<String, String>> filterVOMaps) {
    	if(!isNewSDK.containsKey(modelName)) {
            try {
				isNewSDK.putIfAbsent(modelName, ServiceGateway.isNewSDKGateway(modelName));
			} catch (Throwable t) {
				Log.log.error(String.format("Error %sin identifying if filter model %s is SDK2 type filter for queue %s", 
											(StringUtils.isNotBlank(t.getMessage()) ? "[" + t.getMessage() + "] " : ""),
											modelName, qName));
				opResultsByQNames.put(qName, Boolean.FALSE);
				return;
			}
        }
    	
    	if(!isNewSDK.get(modelName)) {
    		submitDeployFiltersRequest(opResultsByQNames, qName, gatewayClass, deployFilters, 
    								   unDeployFilters, userFilter, filterVOMaps);
    	} else {
    		/*
    		 * Since SDK2 gateways does not support clear and set filter operation, 
    		 * mimic clear and set filters for SDK2 using undeploy all and deploy
    		 * specified filters.
    		 */
    		
    		List<GatewayVO> deployedFilterVOs = null;
    		
    		try {
    			/*
    			 * Note: 
    			 * 
    			 * Currently SDK2 does not support filter companion models like DatabaseConnectionPool.
    			 * Once it starts supporting filter companion models, modelName could be filter companion model
    			 * instead of root model.
    			 */
    			deployedFilterVOs = ServiceGateway.getAllSDK2Filters(modelName, StringUtils.toCamelCase(gatewayName), 
    																 qName);
			} catch (Throwable t) {
				Log.log.error(String.format("Error %sin getting currently deployed SDK2 %s type filters on %s " +
											"gateway/queue", 
											(StringUtils.isNotBlank(t.getMessage()) ? "[" + t.getMessage() + "] " : ""),
											gatewayName, qName));
				opResultsByQNames.put(qName, Boolean.FALSE);
				return;
			}
    		
    		ConcurrentMap<String, Map<String, String>> qFilterVOMaps = 
    														new ConcurrentHashMap<String, Map<String, String>>(filterVOMaps);
    		Set<String> uniqueUnDeployFilterNames = new HashSet<String>();
    		
    		String mappedColumn = GatewayUtil.getFilterModelVOMappedColumnName(modelName);
    		
    		if (CollectionUtils.isNotEmpty(deployedFilterVOs)) {
    			deployedFilterVOs.parallelStream().forEach(deployedFilterVO -> {
    				Map<String, String> filterMap = convert(deployedFilterVO);
    				
    				if (deployedFilterVO instanceof PushGatewayFilterVO) {
                        Map<String, String> attrs = ((PushGatewayFilterVO)deployedFilterVO).getAttrs();
                        
                        if(MapUtils.isNotEmpty(attrs)) {
                            filterMap.putAll(attrs);
                        }
                        
                        filterMap.put(FILTER_MAP_KEY_GATEWAY_CLASS, PUSH_GATEWAY_CLASS_NAME);
                        filterMap.put(FILTER_MAP_KEY_IS_SDK2, Boolean.toString(true));
                    } else if (deployedFilterVO instanceof PullGatewayFilterVO) {
                        Map<String, String> attrs = ((PullGatewayFilterVO)deployedFilterVO).getAttrs();
                        
                        if (MapUtils.isNotEmpty(attrs)) {
                            filterMap.putAll(attrs);
                        }
                        
                        filterMap.put(FILTER_MAP_KEY_GATEWAY_CLASS, PULL_GATEWAY_CLASS_NAME);
                        filterMap.put(FILTER_MAP_KEY_IS_SDK2, Boolean.toString(true));
                    } else if (deployedFilterVO instanceof MSGGatewayFilterVO) {
                        Map<String, String> attrs = ((MSGGatewayFilterVO)deployedFilterVO).getAttrs();
                        
                        if(MapUtils.isNotEmpty(attrs)) {
                            filterMap.putAll(attrs);
                        }
                        
                        filterMap.put(FILTER_MAP_KEY_GATEWAY_CLASS, MSG_GATEWAY_CLASS_NAME);
                        filterMap.put(FILTER_MAP_KEY_IS_SDK2, Boolean.toString(true));
                    }
    				
    				String deployedFilterName = filterMap.get(mappedColumn);
					uniqueUnDeployFilterNames.add(deployedFilterName);
					qFilterVOMaps.put(deployedFilterName, filterMap);
    			});
    		}
    		
    		// Undeploy only those deployed filters which are not being updated (i.e. are specified in deployFilters)
    		if (deployFilters != null && deployFilters.length > 0) {
    			uniqueUnDeployFilterNames.removeAll(Arrays.asList(deployFilters));
    		}
    		
    		if (CollectionUtils.isNotEmpty(uniqueUnDeployFilterNames)) {
    			if (Log.log.isDebugEnabled()) {
        			Log.log.debug(String.format("Undeploying [%s] filters from %s", 
        										StringUtils.collectionToString(uniqueUnDeployFilterNames, ", "), qName));
        		}
    			
    			submitUnDeployFiltersRequest(opResultsByQNames, qName, gatewayClass, 
    										 uniqueUnDeployFilterNames.toArray(new String[0]), userFilter, qFilterVOMaps);
    		}
    		
    		if (((CollectionUtils.isNotEmpty(uniqueUnDeployFilterNames) && opResultsByQNames.containsKey(qName) && 
    			  opResultsByQNames.get(qName).booleanValue()) || CollectionUtils.isEmpty(uniqueUnDeployFilterNames)) && 
    			deployFilters != null && deployFilters.length > 0) {
    			submitDeployFiltersRequest(opResultsByQNames, qName, gatewayClass, deployFilters, 
    									   null, userFilter, filterVOMaps);
    		}
    	}
    }
    
    private void deployFilters(final Map<String, Boolean> opResultsByQNames, final String modelName, 
							   final String qName, final String gatewayName, final String gatewayClass, 
							   final String[] deployFilters, final Map<String, String> userFilter, 
							   final Map<String, Map<String, String>> filterVOMaps) {
    	if(!isNewSDK.containsKey(modelName)) {
            try {
				isNewSDK.putIfAbsent(modelName, ServiceGateway.isNewSDKGateway(modelName));
			} catch (Throwable t) {
				Log.log.error(String.format("Error %sin identifying if filter model %s is SDK2 type filter for queue %s", 
											(StringUtils.isNotBlank(t.getMessage()) ? "[" + t.getMessage() + "] " : ""),
											modelName, qName));
				opResultsByQNames.put(qName, Boolean.FALSE);
				return;
			}
        }
    	
    	if(!isNewSDK.get(modelName)) {
    		// Get deployed filters to add since legacy and SDK1 gateways only support clear and set filters
    		List<GatewayVO> deployedFilterVOs = null;
    		try {
    			deployedFilterVOs = ServiceGateway.getAllFilters(modelName, qName, 
																 userFilter.get(FILTER_OPERATION_BY_USER_FIELD_NAME));
			} catch (Throwable t) {
				Log.log.error(String.format("Error %sin getting currently deployed %s type filters on %s gateway/queue", 
											(StringUtils.isNotBlank(t.getMessage()) ? "[" + t.getMessage() + "] " : ""),
											gatewayName, qName));
				opResultsByQNames.put(qName, Boolean.FALSE);
				return;
			}
    		
    		Map<String, Map<String, String>> qFilterVOMaps = new HashMap<String, Map<String, String>>(filterVOMaps);
    		Set<String> uniqueDeployFilterNames = new HashSet<String>();
    		
    		/*
    		 * For legacy and SDK1 gateways, model name could be companion filter model name
    		 */
    		
    		final StringBuilder filterVOGatewayClassNameBldr = new StringBuilder();
    		final StringBuilder fcmClearAndSetMethodBldr = new StringBuilder();
    		
    		if (MapUtils.isNotEmpty(filterVOMaps)) {
    			Map<String, String> firstFilterVO = filterVOMaps.get(deployFilters[0]);
    			
    			if (MapUtils.isNotEmpty(firstFilterVO) && 
    				StringUtils.isNotEmpty(firstFilterVO.get(FILTER_MAP_KEY_COMPANION_MODEL_ROOT_MESSAGE_HANDLER)) &&
    				StringUtils.isNotEmpty(firstFilterVO.get(FILTER_MAP_KEY_COMPANION_MODEL_CLEARANDSET_METHOD))) {
    				filterVOGatewayClassNameBldr.append(
    												firstFilterVO.get(FILTER_MAP_KEY_COMPANION_MODEL_ROOT_MESSAGE_HANDLER));
    				fcmClearAndSetMethodBldr.append(firstFilterVO.get(FILTER_MAP_KEY_COMPANION_MODEL_CLEARANDSET_METHOD));
    			} else {
    				filterVOGatewayClassNameBldr.append(gatewayClass);
    			}
    		}
    		
    		// Add existing deployed filters
    		if (CollectionUtils.isNotEmpty(deployedFilterVOs)) {
    			Arrays.asList(deployFilters).stream().forEach(filterName -> {
    				if (StringUtils.isNotBlank(filterName)) {
    					uniqueDeployFilterNames.add(filterName);
    				}
    			});
    			
    			deployedFilterVOs.parallelStream().forEach(deployedFilterVO -> {
    				if (deployedFilterVO != null) {
	    				Map<String, String> filterMap = convert(deployedFilterVO);
	    				
	    				filterMap.put(FILTER_MAP_KEY_GATEWAY_CLASS, filterVOGatewayClassNameBldr.toString());
	    				
	    				if (fcmClearAndSetMethodBldr.length() > 0) {
	    					filterMap.put(FILTER_MAP_KEY_COMPANION_MODEL_CLEARANDSET_METHOD, 
	    								  fcmClearAndSetMethodBldr.toString());
	    				}
	    				
	    				String deployedFilterName = filterMap.get(GatewayUtil.getFilterModelVOMappedColumnName(modelName));
	    				
	    				if (StringUtils.isNotBlank(deployedFilterName)) {
	    					uniqueDeployFilterNames.add(deployedFilterName);
	    					qFilterVOMaps.put(deployedFilterName, filterMap);
	    				}
    				}
    			});
    		}
    		
    		// Now add new/update existing filters specified for deployment
    		
    		uniqueDeployFilterNames.addAll(filterVOMaps.keySet());
    		qFilterVOMaps.putAll(filterVOMaps);
    		
    		if (CollectionUtils.isNotEmpty(uniqueDeployFilterNames)) {
    			submitDeployFiltersRequest(opResultsByQNames, qName, gatewayClass, 
    								   	   uniqueDeployFilterNames.toArray(new String[0]), null, userFilter, 
    								   	   qFilterVOMaps);
    		} else {
    			opResultsByQNames.put(qName, Boolean.TRUE);
    		}
    	} else {
    		if (deployFilters != null && deployFilters.length > 0) {
    			submitDeployFiltersRequest(opResultsByQNames, qName, gatewayClass, deployFilters, null, userFilter, 
    									   filterVOMaps);
    		}
    	}
    }
    
    private void unDeployFilters(final Map<String, Boolean> opResultsByQNames, final String modelName, 
			   					 final String qName, final String gatewayName, final String gatewayClass, 
			   					 final String[] undeployFilters, final Map<String, String> userFilter, 
			   					 final Map<String, Map<String, String>> filterVOMaps) {
    	if(!isNewSDK.containsKey(modelName)) {
            try {
				isNewSDK.putIfAbsent(modelName, ServiceGateway.isNewSDKGateway(modelName));
			} catch (Throwable t) {
				Log.log.error(String.format("Error %sin identifying if filter model %s is SDK2 type filter for queue %s", 
											(StringUtils.isNotBlank(t.getMessage()) ? "[" + t.getMessage() + "] " : ""),
											modelName, qName));
				opResultsByQNames.put(qName, Boolean.FALSE);
				return;
			}
        }
    	
    	if(!isNewSDK.get(modelName)) {
    		/*
    		 * Get deployed filters to remove if they match any of the specified filter to undeploy and redeploy
    		 * remaining non matching filters since legacy and SDK1 gateways only support clear and set filters
    		 */
    		List<GatewayVO> deployedFilterVOs = null;
    		try {
    			deployedFilterVOs = ServiceGateway.getAllFilters(modelName, qName, 
																 userFilter.get(FILTER_OPERATION_BY_USER_FIELD_NAME));
			} catch (Throwable t) {
				Log.log.error(String.format("Error %sin getting currently deployed %s type filters on %s gateway/queue", 
											(StringUtils.isNotBlank(t.getMessage()) ? "[" + t.getMessage() + "] " : ""),
											gatewayName, qName));
				opResultsByQNames.put(qName, Boolean.FALSE);
				return;
			}
    		            
    		Map<String, Map<String, String>> qFilterVOMaps = new HashMap<String, Map<String, String>>(filterVOMaps);
    		Set<String> uniqueUnDeployFilterNames = new HashSet<String>();
    		Set<String> uniqueDeployFilterNames = new HashSet<String>();
    		
    		if (CollectionUtils.isNotEmpty(deployedFilterVOs)) {
    			Arrays.asList(undeployFilters).stream().forEach(filterName -> {
    				if (StringUtils.isNotBlank(filterName)) {
    					uniqueUnDeployFilterNames.add(filterName);
    				}
    			});
    			
    			final StringBuilder fcmRootMessageHandlerNameBldr = new StringBuilder();
    			final StringBuilder fcmClearAndSetMethodNameBldr = new StringBuilder();
    			final StringBuilder fcmDeployMethodNameBldr = new StringBuilder();
    			final StringBuilder fcmUnDeployMethodNameBldr = new StringBuilder();
    			
    			if (StringUtils.isNotBlank(filterVOMaps.get(undeployFilters[0])
						   				   .get(FILTER_MAP_KEY_COMPANION_MODEL_ROOT_MESSAGE_HANDLER))) {
    				fcmRootMessageHandlerNameBldr.append(filterVOMaps.get(undeployFilters[0])
				  				   						 .get(FILTER_MAP_KEY_COMPANION_MODEL_ROOT_MESSAGE_HANDLER));
	
				  	if (StringUtils.isNotBlank(filterVOMaps.get(undeployFilters[0])
				  							   .get(FILTER_MAP_KEY_COMPANION_MODEL_CLEARANDSET_METHOD))) {
				  		fcmClearAndSetMethodNameBldr.append(filterVOMaps.get(undeployFilters[0])
	  							   				   			.get(FILTER_MAP_KEY_COMPANION_MODEL_CLEARANDSET_METHOD));
				  	}
				  	
				  	if (StringUtils.isNotBlank(filterVOMaps.get(undeployFilters[0])
							   				   .get(FILTER_MAP_KEY_COMPANION_MODEL_DEPLOY_METHOD))) {
				  		fcmDeployMethodNameBldr.append(filterVOMaps.get(undeployFilters[0])
				   				   			  		   .get(FILTER_MAP_KEY_COMPANION_MODEL_DEPLOY_METHOD));
				  	}
				  	
				  	if (StringUtils.isNotBlank(filterVOMaps.get(undeployFilters[0])
			   				   				   .get(FILTER_MAP_KEY_COMPANION_MODEL_UNDEPLOY_METHOD))) {
				  		fcmUnDeployMethodNameBldr.append(filterVOMaps.get(undeployFilters[0])
				   				   						 .get(FILTER_MAP_KEY_COMPANION_MODEL_UNDEPLOY_METHOD));
				  	}
    			}
    			
    			deployedFilterVOs.parallelStream().forEach(deployedFilterVO -> {
    				Map<String, String> filterMap = convert(deployedFilterVO);
    				filterMap.put(FILTER_MAP_KEY_GATEWAY_CLASS, gatewayClass);
    				
    				if (fcmRootMessageHandlerNameBldr.length() > 0) {
    					filterMap.put(FILTER_MAP_KEY_COMPANION_MODEL_ROOT_MESSAGE_HANDLER, 
    								  fcmRootMessageHandlerNameBldr.toString());
    					
    					if (fcmClearAndSetMethodNameBldr.length() > 0) {
    						filterMap.put(FILTER_MAP_KEY_COMPANION_MODEL_CLEARANDSET_METHOD, 
    									  fcmClearAndSetMethodNameBldr.toString());
    					}
    					
    					if (fcmDeployMethodNameBldr.length() > 0) {
    						filterMap.put(FILTER_MAP_KEY_COMPANION_MODEL_DEPLOY_METHOD, fcmDeployMethodNameBldr.toString());
    					}
    					
    					if (fcmUnDeployMethodNameBldr.length() > 0) {
    						filterMap.put(FILTER_MAP_KEY_COMPANION_MODEL_UNDEPLOY_METHOD, 
    									  fcmUnDeployMethodNameBldr.toString());
    					}
    				}
    				
    				String deployedFilterName = filterMap.get(GatewayUtil.getFilterModelVOMappedColumnName(modelName));
    				
    				if (StringUtils.isNotBlank(deployedFilterName) && 
    					!uniqueUnDeployFilterNames.contains(deployedFilterName)) {
    					uniqueDeployFilterNames.add(deployedFilterName);
    					qFilterVOMaps.put(deployedFilterName, filterMap);
    				}
    			});
    		}
    		
    		// Submit deploy filters request even if list of filters to deploy is empty to clear out last deployed filters
			submitDeployFiltersRequest(opResultsByQNames, qName, gatewayClass, 
								   	   uniqueDeployFilterNames.toArray(new String[0]), null, userFilter, 
								   	   qFilterVOMaps);
    	} else {
    		if (undeployFilters != null && undeployFilters.length > 0) {
    			submitUnDeployFiltersRequest(opResultsByQNames, qName, gatewayClass, undeployFilters, userFilter, 
    									   	 filterVOMaps);
    		}
    	}
    }
    
    /**
     * This method submits filter operation requests to one or more gateway queues.
     * 
     * It differs from deployFilters which accepts single gateway queue and waits for 
     * result of filter operation. This method does not wait for result filter operation.
     * 
     * Filter Operations:
     * 
     *  1. clearAndSet (same as used in deployFilters method)
     *  
     *  Make current selection of filter(s) as the only filters running on selected gateway queue(s)
     *  
     *  2. deploy 
     *  
     *  Deploy current selection of filter(s) to selected gateway queue(s) (if not already deployed) without 
     *  changing existing deployed filters (if any)
     *  
     *  3. undeploy 
     *  
     *  Undeploy current selection of filter(s) from selected RSRemote(s)/gateway queue(s) (if not already deployed) 
     *  without changing existing deployed filters (if any)
     *  
     * @param queueNames for example "EMAIL", "EMAIL1" these queue names are defined in the blueprint like rsremote.receive.email.queue=EMAIL rsremote1.receive.email.queue=EMAIL1
     * @param gatewayName Gateway type like "HTTP", "DB"
     * @param operation clearAndSet/deploy/undeploy example clearAndSetFilter which is available in the gatewayClass or its super class or .
     * @param deployFilters an array of the filter names to deploy (operation is clearAndSet/deploy) 
     * @param unDeployFilters an array of filters to undeploy (operation is clearAndSet/undeploy).
     * @param request
     * @param response
     * @return
     * @throws ServletException
     * @throws IOException
     */
	@RequestMapping(value = "/gateway/deployFilters2", 
					method = { RequestMethod.GET, RequestMethod.POST }, 
					headers = "Accept=application/json; charset=utf-8")
    @ResponseBody
    public ResponseDTO<Pair<String, Boolean>> deployFilters2(
    								  				@RequestParam("queueNames") final String[] queueNames,
    								  				@RequestParam("gatewayName") final String gatewayName,
    								  				@RequestParam("operation") final String operation, 
    								  				@RequestParam("deployFilters") final String[] deployFilters, 
    								  				@RequestParam("unDeployFilters") final String[] unDeployFilters, 
    								  				HttpServletRequest request, 
    								  				HttpServletResponse response) throws ServletException, IOException {
    	long startTime = System.currentTimeMillis();
    	ResponseDTO<Pair<String, Boolean>> result = new ResponseDTO<Pair<String, Boolean>>();
    	String safeGatewayName = null;
    	final String[] tDeployFilters = deployFilters == null ? new String[0] : deployFilters;
		final String[] tUndeployFilters = unDeployFilters == null ? new String[0] : unDeployFilters;
        String modelName = GatewayUtil.getGatewayFilterModelName(gatewayName, false);

    	try {
    		if (StringUtils.isBlank(modelName)) {
    			throw new IllegalArgumentException("Missing modelName");
    		}
    		
    		if (queueNames == null || queueNames.length == 0) {
    			throw new IllegalArgumentException("Missing gateway queue names");
    		}
    		
    		if (StringUtils.isBlank(operation)) {
    			throw new IllegalArgumentException("Missing operation");
    		}
    		
    		if (queueNames == null || queueNames.length == 0) {
    			throw new IllegalArgumentException("Missing gateway queue names");
    		}
    		
    		if (operation.equalsIgnoreCase(Constants.FILTER_OPERATION_DEPLOY) && 
    			(deployFilters == null || deployFilters.length == 0)) {
    			throw new IllegalArgumentException("Missing filter names to deploy operation");
    		}
    		
    		if (operation.equalsIgnoreCase(Constants.FILTER_OPERATION_UNDEPLOY) && 
        		(unDeployFilters == null || unDeployFilters.length == 0)) {
        			throw new IllegalArgumentException("Missing filter names to undeploy operation");
        	}
    		
    		if (operation.equalsIgnoreCase(Constants.FILTER_OPERATION_CLEARANDSET) && 
    			(deployFilters == null || deployFilters.length == 0) && 
    			(unDeployFilters == null || unDeployFilters.length == 0)) {
    			throw new IllegalArgumentException("Missing filter names to deploy/undeploy for clear and set operation");
    		}
    		
    		if (StringUtils.isBlank(gatewayName)) {
    			throw new IllegalArgumentException("Missing filters gateway name/type");
    		}
    		
    		/*
    		 * For SDK2 gateways UI sends this parameter from URL (user specified) and hence it needs to be sanitized 
    		 * versus other parameters which are retrieved from back end and hence are already safe
    		 * 
    		 * SDK2 gateways use single table to store all filters for all gateways by category Push, Pull, and MSG
    		 * compared to legacy and SDK1 gateways which has gateway specific model/table 
    		 */
    		
    		safeGatewayName = ESAPI.validator().getValidInput(GATEWAY_NAME_INVALID_MSG, gatewayName, 
    														  ESAPI_HTTP_URL_FILTER, 40, true);
    		
    		//this is a way to pass the user
            final String username = (String) request.getAttribute(Constants.HTTP_REQUEST_USERNAME);
            final Map<String, String> userFilter = new HashMap<String, String>();
            userFilter.put(FILTER_OPERATION_BY_USER_FIELD_NAME, username);
            
            final Map<String, Map<String, String>> filterVOMaps = getFilterVOMaps(deployFilters, unDeployFilters, modelName,
            																gatewayName, queueNames[0]);
            
            if (MapUtils.isNotEmpty(filterVOMaps) && 
            	StringUtils.isNotBlank((String)filterVOMaps.values().iterator().next()
           			 						   .get(FILTER_MAP_KEY_GATEWAY_COMPANION_MODEL_CLASS_NAME)) &&
            	(!((String)filterVOMaps.values().iterator().next()
            			 .get(FILTER_MAP_KEY_GATEWAY_COMPANION_MODEL_CLASS_NAME))
            	   .equals(modelName))) {
            	modelName = (String)filterVOMaps.values().iterator().next()
            			    .get(FILTER_MAP_KEY_GATEWAY_COMPANION_MODEL_CLASS_NAME);            	
            }
            
            String filterVONotFound = filterVOMaps.keySet().stream()
            						  .filter(uniqueFilterName -> {
            							  return MapUtils.isEmpty(filterVOMaps.get(uniqueFilterName));
            						  }).findFirst().orElse(null);
            
            if (StringUtils.isNotEmpty(filterVONotFound)) {
            	throw new IllegalArgumentException(String.format("Failed to find filter VO for %s filter", 
            													 filterVONotFound));
            }
            
            if (Log.log.isInfoEnabled()) {
            	Log.log.info(String.format("deployFilters2(safeGatewayName = %s, modelName = %s, gatewayClass = DEFAULT, " +
            							   "operation = %s, queueNames(%d) = [%s], deployFilters(%d) = [%s], " +
            							   "undeployFilters(%d) = [%s]", safeGatewayName, modelName,
            							   operation,queueNames.length, StringUtils.arrayToString(queueNames, ", "), 
            							   tDeployFilters.length, 
            							   (tDeployFilters.length > 0 ? 
            								StringUtils.arrayToString(tDeployFilters, ", ") : ""),
            							   tUndeployFilters.length,
            							   (tUndeployFilters.length > 0 ? 
            							    StringUtils.arrayToString(tUndeployFilters, ", ") : ""),
            							   StringUtils.arrayToString(queueNames, ", ")));
            }
            
            Map<String, Boolean> opResultsByQNames = new HashMap<String, Boolean>();
            
            final String fModelName = modelName;
            
            Arrays.asList(queueNames).parallelStream().forEach(qName -> {
            	switch (operation) {
            	case Constants.FILTER_OPERATION_CLEARANDSET:
            		clearAndSetFilters(opResultsByQNames, fModelName, qName, gatewayName, "DEFAULT", tDeployFilters, 
            						   tUndeployFilters, userFilter, filterVOMaps);
            		break;
            	case Constants.FILTER_OPERATION_DEPLOY:
            		deployFilters(opResultsByQNames, fModelName, qName, gatewayName, "DEFAULT", tDeployFilters,
            					  userFilter, filterVOMaps);
            		break;
            	case Constants.FILTER_OPERATION_UNDEPLOY:
            		unDeployFilters(opResultsByQNames, fModelName, qName, gatewayName, "DEFAULT", tUndeployFilters,
      					  			userFilter, filterVOMaps);
            		break;
            	}
            });
            
            List<Pair<String, Boolean>> results = new ArrayList<Pair<String, Boolean>>();
            
            if (MapUtils.isNotEmpty(opResultsByQNames)) {
            	opResultsByQNames.forEach((k, v) -> {results.add(Pair.of(k, v));});
            }
            
    		result.setRecords(results).setTotal(results.size()).setSuccess(true);
    	} catch (Throwable t) {
    		Log.log.error(String.format("Error %sin deploying filters to specified remotes/gateway queues", 
										(StringUtils.isNotBlank(t.getMessage()) ? "[" + t.getMessage() + "] " : "")), t);
    		result.setMessage("Error in deploying filters to specified remotes/gateway queues)").setSuccess(false);
    	}
    	
    	long endTime = System.currentTimeMillis();
    	
    	if (Log.log.isInfoEnabled()) {
    		Log.log.info(String.format("deployFilters2(safeGatewayName = %s, modelName = %s, gatewayClass = DEFAULT, " +
									   "operation = %s, queueNames(%d) = [%s], deployFilters(%d) = [%s], " +
									   "undeployFilters(%d) = [%s] took %d msec. Result: %b %s", 
									   safeGatewayName, modelName, operation, queueNames.length, 
									   StringUtils.arrayToString(queueNames, ", "), 
									   tDeployFilters.length, 
									   (tDeployFilters.length > 0 ? 
										StringUtils.arrayToString(tDeployFilters, ", ") : ""),
									   tUndeployFilters.length,
									   (tUndeployFilters.length > 0 ? 
										StringUtils.arrayToString(tUndeployFilters, ", ") : ""),
									   (endTime - startTime), result.isSuccess(), parseDeployFilters2Result(result)));
    	}
    	
    	return result;
    }

    /**
     * This method gets all gateways information from all  instances of rsremote. Multiple instances (queues)
     * of a gateway type can be actively running with different IDs (queue names). An example of a gateway
     * information includes but not limited to the following fields:
     * <ul>
     *  <li>1. Type
     *  <li>2. Name
     *  <li>3. Primary
     *  <li>4. Secondary
     *  <li>5. Active indicates the gateway is currently running
     *  <li>9. Filters currently deployed on the gateway. Each filter is a collection of the following information:
     *   <li>9.1. Id
     *   <li>9.2. Order
     *   <li>9.3. Interval
     *   <li>9.4. Runbook
     *   <li>9.5. Active
     *  <ul>
     *  Example of API call:
     *     GET  /resolve/services/gateway/getGatewaysInfo?type=NETCOOL&activeOnly=[true|false]&typeOnly=[true|false]
     *     GET  /resolve/services/gateway/getGatewaysInfo
     *     
     *  Omission of 'activeOnly' implies 'activeOnly=false'
     *  
     *  Example of a call and the return payload:
     *  <pre>
     *   GET  /resolve/services/gateway/getGatewaysInfo?activeOnly=false
     *              records":[{
     *                  "containerIp": "10.50.2.249",
     *                  "containerName": "rsremote",
     *                  "type":"HTTP",
     *                  "originalPrimary": true,
     *                  "updatedBy": 1554756459,
     *                  "updatedOn": 1554756459
     *                  "gatewayProperties": {
     *                    "id":"UNDEFINED",
     *                    "sys_id":"UNDEFINED",
     *                    "sysCreatedOn":0,
     *                    "sysCreatedBy":"UNDEFINED",
     *                    "sysUpdatedOn":0,
     *                    "sysUpdatedBy":"UNDEFINED",
     *                    "sysModCount":-2147483648,
     *                    "sysPerm":"UNDEFINED",
     *                    "sysOrg":"UNDEFINED",
     *                    "sysIsDeleted":false,
     *                    "sysOrganizationName":"UNDEFINED",
     *                    "checksum":-2147483648,
     *                    "changed":false,
     *                    "uport":-2147483648,
     *                    "ussl":false,
     *                    "uactive":true ,
     *                    "uuser":"UNDEFINED",
     *                    "ueventType":"UNDEFINED",
     *                    "upackageName":"UNDEFINED",
     *                    "uprimary":true,
     *                    "uclassName":"UNDEFINED",
     *                    "umock":false,
     *                    "uheartbeat":120,
     *                    "uorgName":"UNDEFINED",
     *                    "usecondary":false,
     *                    "ursremoteName":"UNDEFINED",
     *                    "ufailover":120,
     *                    "usslType":"UNDEFINED",
     *                    "ulicenseCode":"UNDEFINED",
     *                    "uworker":false,
     *                    "uhost":"UNDEFINED",
     *                    "uuppercase":false,
     *                    "ugatewayName":"HTTP",
     *                    "upass":"UNDEFINED",
     *                    "udisplayName":"UNDEFINED",
     *                    "uniqueId":"UNDEFINED",
     *                    "uinterval":120,
     *                    "uqueue":"UNDEFINED"
     *                  }}
     *  </pre>
     * @param type to get information only for specific gateway, i.e. "HTTP"
     * @param activeOnly if this value is true, only active gateways will be returned; otherwise, all gateways are returned.
     *                   A running gateway on a rsremote is an active gateway.
     * @param typeOnly if this value is true, only gateway types will be returned. For example "HTTP", "EWS", "NETCOOL"
     * @param request
     * @param response
     * @return a list of gateways are currently on all rsremote instances.
     * @throws ServletException
     * @throws IOException
     * 
     */
    @RequestMapping(value = "/gateway/getGatewaysInfo", method = { RequestMethod.GET }, headers = "Accept=application/json; charset=utf-8")
    @ResponseBody
    public ResponseDTO<GatewayDTO> getGatewaysInfo (
            @RequestParam(required = false) String type,
            @RequestParam(required = false) boolean activeOnly,
            @RequestParam(required = false) boolean typeOnly,
            HttpServletRequest request,
            HttpServletResponse response
    ) throws ServletException, IOException
    {
        ResponseDTO<GatewayDTO> result = new ResponseDTO<GatewayDTO>();
        try {
            String username = (String) request.getAttribute(Constants.HTTP_REQUEST_USERNAME);
            List<GatewayDTO> records = ServiceGateway.getGatewaysInfo(username, type, activeOnly, typeOnly);
            result.setSuccess(true).setRecords(records).setTotal(records.size());
        } catch (Exception e) {
            Log.log.warn("Error parsing rsremote config.xml", e);
        }
        return result;
    }
    
    /**
     * This method gets health check reports of all active gateways. 
     * An example of a gateway health check record includes following fields
	 *
     * <ul>
     *  <li>1. Org name
     *  <li>2. Remote name (GUID-NAME-GROUP)
     *  <li>3. Gateway Type (for example DB, ZENOSS etc.)
     *  <li>4. Queue name (Composite queue name suffixed with Org if available)
     *  <li>5. Report gateway health check report JSON
     *  <li>6. Stale Indicates if last update on time of health check is equal to
     *  			 more than 2 times gateway health check interval
     *  <ul>
     *  Example of API call:
     *     GET  /resolve/services/gateway/getGatewaysInfo?
     *     GET  /resolve/services/gateway/getGatewaysInfo
     *     
     * @param request
     * @param response
     * @return a list of gateways health check reports.
     * @throws ServletException
     * @throws IOException
     * 
     */
    @RequestMapping(value = "/gateway/listGatewayHealthCheckReports", 
    				method = { RequestMethod.GET }, 
    				headers = "Accept=application/json; charset=utf-8")
    @ResponseBody
    public ResponseDTO<GatewayHealthCheckVO> listGatewayHealthCheckReports ( HttpServletRequest request,
    																		 HttpServletResponse response ) 
    																		 throws ServletException, IOException
    {
        ResponseDTO<GatewayHealthCheckVO> result = new ResponseDTO<GatewayHealthCheckVO>();
        try {
            String username = (String) request.getAttribute(Constants.HTTP_REQUEST_USERNAME);
            List<GatewayHealthCheckVO> records = 
            								ServiceGateway.getGatewayHealthCheckReports(
            											username, 
            											((com.resolve.rsview.main.ConfigGeneral)Main.main.configGeneral).
            											getGtwHealthChkIntrvl());
            result.setSuccess(true).setRecords(records).setTotal(records.size());
        } catch (Exception e) {
        	Log.log.error(String.format("Error [%s]in getting list of gateway health check reports", 
        								(StringUtils.isNotBlank(e.getMessage()) ? "[" + e.getMessage() + "] " : "")), e);
            result.setSuccess(false).setMessage("Error in getting list of gateway health check reports");
        }
        
        return result;
    }
    
    /**
     * API to get the status of filter(s) deployment on queue(s) for a given gateway type.
     * 
     * @param gatewayType: String repsenting gateway type, e.g. Netcool, Http, ServiceNow etc...
     * @param filters: List of String representing filter names needed to be deployed.
     * @param queueNames: List of String representing different queues where the filters needed to be deployed.
     * @param request: HttpServletRequest
     * @return
     *      A Json String with the status.<br>
     *      E.g. for queue names {"HTTP1","HTTP"}, filters {"Test", "RR-CreateSIR"} and gateway type of Http<br>
     *      we might receive a response something like this:<br>
     *          {
     *              "HTTP1": {
     *                      "Test": "false",
     *                      "RR-CreateSIR": "false"
     *              },
     *              "HTTP": {
     *                      "Test": "false",
     *                      "RR-CreateSIR": "true"
     *              }
     *          }
     *      This result specifies, on HTTP1 queue, none of the filters are deployed
     *      whereas on the other queue, HTTP, filter named 'Test' is not deployed and 'RR-CreateSIR' is deployed.
     * @throws ServletException
     * @throws IOException
     */
    @RequestMapping(value = "/gateway/getFilterDeploymentStatus", 
                    method = { RequestMethod.GET }, 
                    headers = "Accept=application/json; charset=utf-8")
    @ResponseBody
    public ResponseDTO<Map<String, Object>> getFilterDeploymentStatus(
                    @RequestParam String gatewayType,
                    @RequestParam List<String> filters,
                    @RequestParam List<String> queueNames,
                    HttpServletRequest request ) throws ServletException, IOException
    {
        ResponseDTO<Map<String, Object>> result = new ResponseDTO<>();
        try {
            String username = (String) request.getAttribute(Constants.HTTP_REQUEST_USERNAME);
            Map<String, Object> statusMap = ServiceGateway.getFilterDeployStatus(filters, queueNames, gatewayType, username);
            result.setData(statusMap);
        } catch (Exception e) {
            String errorMsg = String.format("Error while getting status of filter(s) deployment on queue(s)");
            Log.log.error(errorMsg, e);
            result.setSuccess(false).setMessage(errorMsg);
        }
        return result;
    }
}
