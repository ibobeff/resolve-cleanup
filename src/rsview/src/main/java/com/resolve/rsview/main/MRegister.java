/******************************************************************************
* (C) Copyright 2014
*
* Resolve Systems, LLC
*
* All rights reserved
* This software is distributed under license and may not be used, copied,
* modified, or distributed without the express written permission of
* Resolve Systems, LLC.
*
******************************************************************************/

package com.resolve.rsview.main;

import java.io.File;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.Hashtable;
import java.util.Map;

import com.resolve.util.Constants;
import com.resolve.util.FileUtils;
import com.resolve.util.Log;

public class MRegister
{
    
    private static SimpleDateFormat sdf = new SimpleDateFormat(Constants.HEARTBEAT_FORMAT);
    private static File blueprintFile = null;
    
    @SuppressWarnings("rawtypes")
    public String register(Map params)
    {
        register();
        
        return "SUCCESS: Registration completed";
    } // register

    @SuppressWarnings({ "rawtypes", "unchecked" })
    public static void register()
    {
        try
        {
            Log.log.debug("Registration");
            
            File file = FileUtils.getFile(RSContext.main.getProductHome()+"/WEB-INF/config.xml");
            if (file.exists())
            {
                ArrayList content = new ArrayList();
                
                // set GUID and NAME
                Hashtable reg = new Hashtable();
	            reg.put("GUID", RSContext.main.configId.guid);
	            reg.put("NAME", RSContext.main.configId.name);
	            reg.put("TYPE", RSContext.main.release.type);
	            reg.put("IPADDRESS", RSContext.main.configId.ipaddress);
	            reg.put("VERSION", RSContext.main.release.version);
	            reg.put("DESCRIPTION", RSContext.main.configId.description);
	            reg.put("LOCATION", RSContext.main.configId.location);
	            reg.put("GROUP", RSContext.main.configId.group);
	            reg.put("CONFIG", "");
                reg.put("CONFIG", FileUtils.readFileToString(file, "UTF-8"));
                reg.put("OS", System.getProperty("os.name"));
                content.add(reg);

                // get parent GUID and NAME
                if (Main.esb.sendInternalMessage(Constants.ESB_NAME_SYSTEM_RSCONTROL, "MRegister.register", content) == false)
                {
                    Log.log.warn("Failed to send registration message");
                }
            }
            else
            {
                Log.log.error("Missing config.xml file: "+file.getPath());
            }
        }
        catch (Exception e)
        {
            Log.log.error("Failed to register RSRemote: "+e.getMessage(), e);
        }
    } // register
    
    @SuppressWarnings("rawtypes")
    public String logHeartbeat(Map params)
    {
        logHeartbeat();
        
        return "SUCCESS: Heartbeat completed";
    } // logHeartbeat
    
    public static void logHeartbeat()
    {
        Date time = new Date();
        Log.log.info("HEARTBEAT: " + sdf.format(time));
        System.out.println("HEARTBEAT:" + sdf.format(time));
    } // logHeartbeat
    
    public static String getBlueprintData() {
        
        String blueprintData = "Error: No blueprint data found.";
        
        if(blueprintFile == null)
        {
            blueprintFile = FileUtils.getFile(Main.main.getResolveHome() + "/rsmgmt/config/blueprint.properties");
        }
        
        try
        {
            blueprintData = FileUtils.readFileToString(blueprintFile, "utf8");
        }
        catch (IOException e)
        {
            Log.log.error(e.getMessage(), e);
        }
        
        return blueprintData;
        
    }

} // MRegister
