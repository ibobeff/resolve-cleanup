/******************************************************************************
* (C) Copyright 2014
*
* Resolve Systems, LLC
*
* All rights reserved
* This software is distributed under license and may not be used, copied,
* modified, or distributed without the express written permission of
* Resolve Systems, LLC.
*
******************************************************************************/

package com.resolve.rsview.controller;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.codehaus.jackson.map.ObjectMapper;
import org.springframework.stereotype.Controller;
import org.springframework.stereotype.Service;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.resolve.search.SearchUtils;
import com.resolve.search.model.ProcessRequest;
import com.resolve.search.model.TaskResult;
import com.resolve.search.model.Worksheet;
import com.resolve.search.worksheet.WorksheetSearchAPI;
import com.resolve.services.ServiceHibernate;
import com.resolve.services.ServiceWorksheet;
import com.resolve.services.hibernate.util.ExecuteRunbook;
import com.resolve.services.hibernate.util.UserUtils;
import com.resolve.services.hibernate.vo.OrgsVO;
import com.resolve.services.interfaces.VO;
import com.resolve.services.util.SequenceUtils;
import com.resolve.services.util.WorksheetUtil;
import com.resolve.services.vo.QueryDTO;
import com.resolve.services.vo.QueryFilter;
import com.resolve.services.vo.ResponseDTO;
import com.resolve.util.Constants;
import com.resolve.util.Log;
import com.resolve.util.StringUtils;

import net.sf.json.JSONObject;


/**
 *
 * This controller serves the request(CRUD operations) for Worksheets. The worksheet data is stored in Cassandra and Elastic Search. 
 * <p> 
 * Worksheets are canvas/buckets where all the execution of Runbooks are stored. 
 * <p> 
 * For more information about that, please refer to <a href="http://community.gen-e.com/resolve/jsp/rsclient.jsp">Resolve documentation</a>. 
 * <p>
 * Url : {@code http://localhost:8080/resolve/jsp/rsclient.jsp#RS.worksheet.Worksheets/}
 * 
 * @author jeet.marwah
 *
 */
@Controller
@Service
public class AjaxWorksheet extends GenericController
{    
    /**
     * 
     * Returns/Retrieves list of Worksheets for the grid based on filters, sorts, pagination. 
     * This call does not return the "debug" data. Debug data could be big and it slows down
     * the caller for no real value gain for a list of worksheets. However, getWorksheet() method
     * provides the debug data as well for an individual worksheet.
     *   
     * <p> 
     * <u><b>Example</b></u>:</br> 
     * <code><pre>
     *  filter: [{"field":"summary","type":"auto","condition":"contains","value":"Runbook"}]
     *  page: 1
     *  start: 0
     *  limit: 50
     *  sort: [{"property":"sysUpdatedOn","direction":"DESC"}]
     * </pre></code>
     *  
     * <p>
     * Parameters in the Request object: 
     * <code><pre>
     *  USERNAME - username of the user who is logged in to Resolve
     * </pre></code>
     *  
     * <p>
     * Return object in response:
     * <code><pre>
     *  'records' : List of type {@link Worksheet}
     *  'total' : # of total records
     * </pre></code>
     * 
     * @param query {@link QueryDTO}, represents the query, pagination, sorting, grouping for the UI
     * @param request  HttpServletRequest
     * @param response  HttpServletResponse
     * @return {@link ResponseDTO} - Reponse Data Transfer Object having a List  and total # of records 
     * @throws ServletException
     * @throws IOException
     */
    @RequestMapping(value = "/worksheet/list", method = { RequestMethod.POST, RequestMethod.GET }, headers = "Accept=application/json; charset=utf-8")
    @ResponseBody
    public ResponseDTO<Worksheet> listWorksheets(@ModelAttribute QueryDTO query, @RequestParam(defaultValue="", value="RESOLVE.ORG_ID") String orgId, @RequestParam(defaultValue="", value="RESOLVE.ORG_NAME") String orgName, HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException
    {
        ResponseDTO<Worksheet> result = new ResponseDTO<Worksheet>();
        try
        {
            String username = (String) request.getAttribute(Constants.HTTP_REQUEST_USERNAME);
            
            String effectiveOrgId = null;
            
            if (StringUtils.isNotBlank(orgId) && (orgId.equalsIgnoreCase(VO.STRING_DEFAULT) ||
                                                  orgId.equalsIgnoreCase(Constants.NIL_STRING)))
            {
                orgId = null;
            }
            
            if (StringUtils.isNotBlank(orgId))
            {
                effectiveOrgId = orgId;
            }
            else if (StringUtils.isNotBlank(orgName))
            {
                OrgsVO orgsVO = UserUtils.getOrg(null, orgName);
                
                if (orgsVO != null)
                {
                    effectiveOrgId = orgsVO.getSys_id();
                }
                else
                {
                    if (OrgsVO.NONE_ORG_NAME.equalsIgnoreCase(orgName))
                    {
                        //Check if user has No Org access
                        
                        if (!UserUtils.isNoOrgAccessible(username))
                        {
                            throw new Exception("User " + username + " does not have access to " + orgName);
                        }
                    }
                    else {
                        throw new Exception(String.format("Non existing org name, %s", orgName));
                    }
                }
            }
            
            if (StringUtils.isNotBlank(effectiveOrgId))
            {
                if (query == null)
                {
                    query = new QueryDTO();
                }
                
                if (!UserUtils.isOrgAccessible(effectiveOrgId, username, false, false))
                {
                    throw new Exception("User " + username + " does not have access to Org with sys_id " + effectiveOrgId);
                }
                    
                if (query.getFilters() != null && !query.getFilters().isEmpty())
                {
                    List<QueryFilter> newQryFltrs = new ArrayList<QueryFilter>();
                    
                    for (QueryFilter qryFltr : query.getFilters())
                    {
                        if (!qryFltr.isTermsType() || !qryFltr.getField().equals("sysOrg"))
                        {
                            newQryFltrs.add(qryFltr);
                        }
                    }
                    
                    query.setFilters(newQryFltrs);
                }
                
                query.addFilterItem(new QueryFilter(QueryFilter.TERMS_TYPE, effectiveOrgId, "sysOrg", QueryFilter.EQUALS));
            } else if (OrgsVO.NONE_ORG_NAME.equalsIgnoreCase(orgName)) {
                query.addFilterItem(new QueryFilter(QueryFilter.TERMS_TYPE, "nil", "sysOrg", QueryFilter.EQUALS));
            }
            
            //debug data may be huge and slow down the caller for no apperant value gain for the worksheet list.
            //the getWorksheet method provides the debug data. 
            //TODO, could it be coming from UI?
            query.setExcludeColumns("debug");
            // gets data from Search Service
            result = WorksheetSearchAPI.searchWorksheets(query, username);
            result.setSuccess(true);
        }
        catch (Exception e)
        {
            String message = e.getMessage();
            if (StringUtils.isNotBlank(message) && message.contains("does not have access to"))
            {
                Log.log.info(message);
            }
            else
            {
                Log.log.error("Error retrieving worksheet list.", e);
            }
            
            result.setSuccess(false).setMessage("Error retrieving worksheet list.");
        }

        return result;
    }


    /**
     * 
     * Creates a new worksheet and provides a message to the user to go directly to that new worksheet
     * <p> 
     * <u><b>Example</b></u>:</br> 
     * <code><pre>
     *  reference: 
     *  alertId: 
     *  correlationId: 
     * </pre></code> 
     * <p>
     * Parameters in the Request object: 
     * <code><pre>
     *  USERNAME - username of the user who is logged in to Resolve
     * </pre></code>
     * 
     * <p>
     * Return object in response:
     * <code><pre>
     *  'data' : sysId of the newly creted Worksheet
     * </pre></code>
     *  
     * @param reference reference text that gets mapped to the newly created worksheet
     * @param alertId alertId text that gets mapped to the newly created worksheet
     * @param correlationId correlationId text that gets mapped to the newly created worksheet
     * @param request  HttpServletRequest
     * @return {@link ResponseDTO} - Reponse Data Transfer Object sets with 'data' having the newly created sysId 
     * @throws ServletException
     * @throws IOException
     */
    @RequestMapping(value = "/worksheet/newWorksheet", method = { RequestMethod.POST }, headers = "Accept=application/json; charset=utf-8")
    @ResponseBody
    public ResponseDTO<String> createWorksheet(@RequestParam(defaultValue = "") String reference, @RequestParam(defaultValue = "") String alertId, @RequestParam(defaultValue = "") String correlationId, @RequestParam(defaultValue="", value="RESOLVE.ORG_ID") String orgId, @RequestParam(defaultValue="", value="RESOLVE.ORG_NAME") String orgName, HttpServletRequest request) throws ServletException, IOException
    {
        ResponseDTO<String> result = new ResponseDTO<String>();

        try
        {
            String username = (String) request.getAttribute(Constants.HTTP_REQUEST_USERNAME);

            Worksheet worksheetModel = new Worksheet(SearchUtils.getNewRowKey(), null, username);
            worksheetModel.setAssignedTo(username);
            worksheetModel.setNumber(SequenceUtils.getNextSequence(Constants.PERSISTENCE_SEQ_PRB));
            worksheetModel.setReference(reference);
            worksheetModel.setAlertId(alertId);
            worksheetModel.setCorrelationId(correlationId);
            
            if (orgId != null && orgId.compareToIgnoreCase("nil") == 0 ) {
                orgId = "";
            }

            if (StringUtils.isNotBlank(orgId) || StringUtils.isNotBlank(orgName))
            {
                String orgIdInt = orgId;
                
                if (StringUtils.isBlank(orgIdInt) && StringUtils.isNotBlank(orgName))
                {
                    if (!orgName.equalsIgnoreCase(OrgsVO.NONE_ORG_NAME))
                    {
                        // Identify org id based on org name
                        
                        OrgsVO orgsVO = UserUtils.getOrg(null, orgName);
                        
                        if (orgsVO != null)
                        {
                            orgIdInt = orgsVO.getSys_id();
                        }
                    }
                    else
                    {
                        orgName = null;
                    }
                }
                
                if (!UserUtils.isSuperUser(username))
                {
                    if (StringUtils.isNotBlank(orgIdInt))
                    {
                        if (UserUtils.isOrgAccessible(orgIdInt, username, false, false))
                        {
                            worksheetModel.setSysOrg(orgIdInt);
                        }
                        else
                        {
                            Log.log.warn("User " + username + " does not have permission to create worksheet for specified org with id " + 
                                            orgId);
                            throw new Exception("User does not have permission to create worksheet for specified org");
                        }
                    }
                    else
                    {
                        if (!UserUtils.isNoOrgAccessible(username))
                        {
                            Log.log.warn("User " + username + " does not have permission to create worksheet for specified No Org 'None'.");
                            throw new Exception("User does not have permission to create worksheet for specified No Org 'None'.");
                        }
                    }
                }
                else
                {
                    if (StringUtils.isNotBlank(orgIdInt))
                    {
                        worksheetModel.setSysOrg(orgIdInt);
                    }
                }
            }
            worksheetModel.setModified(true);

            Worksheet response = WorksheetUtil.saveWorksheet(worksheetModel, username);
            String id = response.getSysId();
            WorksheetUtil.activeModel(username, id, orgId, orgName);
            result.setSuccess(true).setData(id).setMessage("Created new worksheet.  <a href=\"#RS.worksheet.Worksheet/id=" + id + "}\">Click to view.</a>");
        }
        catch (Exception e)
        {
            Log.log.error("Error while creating new worksheet.", e);
            result.setSuccess(false).setMessage("Error while creating new worksheet.");
        }
        return result;
    }
    
    /**
     * 
     * Saves/Update a Worksheet. If 'sysId' is set, it will be update else it will create it. 
     * 
     * <p>
     * Parameters in the Request object: 
     * <code><pre>
     *  USERNAME - username of the user who is logged in to Resolve
     * </pre></code>
     * <p>
     * Return object in response 
     * <code><pre>
     *  'data' : {@link Worksheet}
     * </pre></code>
     * 
     * @param jsonProperty JSONObject/JSON having data of type {@link WorksheetCAS}
     * @param request HttpServletRequest
     * @return {@link ResponseDTO} - Reponse Data Transfer Object with 'data' having the created/updated Worksheet
     * @throws ServletException
     * @throws IOException
     */
    @RequestMapping(value = "/worksheet/save", method = { RequestMethod.POST }, headers = "Accept=application/json; charset=utf-8")
    @ResponseBody
    public ResponseDTO<Worksheet> saveWorksheet(@RequestBody JSONObject jsonProperty, HttpServletRequest request) throws ServletException, IOException
    {
        ResponseDTO<Worksheet> result = new ResponseDTO<Worksheet>();

        try
        {
            String username = (String) request.getAttribute(Constants.HTTP_REQUEST_USERNAME);
            String json = jsonProperty.toString();
            Worksheet worksheet = new ObjectMapper().readValue(json, Worksheet.class);
            if (StringUtils.isNotBlank(worksheet.getSirId()))
            {
                worksheet.setModified(true);
            }
            
            Worksheet response = WorksheetUtil.saveWorksheet(worksheet, username);
            result.setSuccess(true).setData(response).setMessage("Worksheet saved successfully.  <a href=\"#RS.worksheet.Worksheet/id=" + response.getSysId() + "}\">Click to view.</a>");
        }
        catch (Exception e)
        {
            Log.log.error("Error while saving worksheet.", e);
            result.setSuccess(false).setMessage("Error while saving worksheet.");
        }

        return result;
    }


    
    /**
     * Delete the Worksheets based on array of sysIds
     * <p> 
     * If there are no ids in the query, then we have to use the filter criteria to delete all the records across all pages for what the user has specified (bulk delete). 
     * THIS FILTER CRITERIA DELETE IS NOT FUNCTIONAL AT THE MOMENT.
     * <p>
     * 
     * @param query QueryDTO which has the query that will be used to delete the worksheets. THIS IS NOT FUNCTIONAL AT THE MOMENT.
     * @param ids list of worksheets sysId to be deleted
     * @param request HttpServletRequest
     * @return {@link ResponseDTO} - Reponse Data Transfer Object that has success as 'true' or 'false'
     * @throws ServletException
     * @throws IOException
     */
    @RequestMapping(value = "/worksheet/delete", method = { RequestMethod.POST }, headers = "Accept=application/json; charset=utf-8")
    @ResponseBody
    public ResponseDTO<String> deleteWorksheet(@ModelAttribute QueryDTO query, @RequestParam List<String> ids, HttpServletRequest request) throws ServletException, IOException
    {
        ResponseDTO<String> result = new ResponseDTO<String>();

        try
        {
            if (ids == null || ids.size() == 0)
            {
                result.setSuccess(false).setMessage("Worksheet IDs must be provided");
            }
            else
            {
                String username = (String) request.getAttribute(Constants.HTTP_REQUEST_USERNAME);
                Collection<String> deletedIds = ServiceWorksheet.deleteWorksheetRows(ids, true, username);
                result.setSuccess(true).setRecordsFromCollection(deletedIds).setTotal(deletedIds.size());
            }
        }
        catch (Exception e)
        {
            Log.log.error("Error while deleting worksheet(s).", e);
            result.setSuccess(false).setMessage("Error while deleting worksheet(s).");
        }

        return result;
    }

    /**
     * Gets the details for the user's active worksheet
     * 
     * <p>
     * Parameters in the Request object: 
     * <code><pre>
     *  USERNAME - username of the user who is logged in to Resolve
     * </pre></code>
     * 
     * @param request HttpServletRequest
     * @return {@link ResponseDTO} - Reponse Data Transfer Object that has 'data' of type WorksheetCAS
     * @throws ServletException
     * @throws IOException
     */
    @RequestMapping(value = "/worksheet/getActive", method = { RequestMethod.GET }, headers = "Accept=application/json; charset=utf-8")
    @ResponseBody
    public ResponseDTO<Worksheet> getActiveWorksheet(@RequestParam(defaultValue="", value="RESOLVE.ORG_ID") String orgId, @RequestParam(defaultValue="", value="RESOLVE.ORG_NAME") String orgName, HttpServletRequest request) throws ServletException, IOException
    {
        ResponseDTO<Worksheet> result = new ResponseDTO<Worksheet>();

        try
        {
            String username = (String) request.getAttribute(Constants.HTTP_REQUEST_USERNAME);

            String orgIdInt = orgId;
            
            if (StringUtils.isNotBlank(orgId) && (orgId.equalsIgnoreCase(VO.STRING_DEFAULT) ||
                                                  orgId.equalsIgnoreCase(Constants.NIL_STRING)))
            {
                orgIdInt = null;
            }
            
            if (orgIdInt == null && StringUtils.isNotBlank(orgName) &&
                !orgName.equalsIgnoreCase(VO.STRING_DEFAULT) &&
                !orgName.equalsIgnoreCase(OrgsVO.NONE_ORG_NAME))
            {
                // Identify org id based on org name
                
                OrgsVO orgsVO = UserUtils.getOrg(null, orgName);
                
                if (orgsVO != null)
                {
                    orgIdInt = orgsVO.getSys_id();
                }
            }
            
            if (StringUtils.isNotBlank(orgName) && (orgName.equalsIgnoreCase(VO.STRING_DEFAULT) ||
                                                    orgName.equalsIgnoreCase(OrgsVO.NONE_ORG_NAME)))
            {
                orgName = null;
            }
            
            if (!UserUtils.isSuperUser(username))
            {
                if (StringUtils.isNotBlank(orgIdInt))
                {
                    if (!UserUtils.isOrgAccessible(orgIdInt, username, false, false))
                    {
                        Log.log.warn("User " + username + " does not have permission to access worksheets for specified Org with id " + 
                                     orgId);
                        throw new Exception("User does not have permission to access worksheets for specified Org");
                    }
                }
                else
                {
                    if (!UserUtils.isNoOrgAccessible(username))
                    {
                        Log.log.warn("User " + username + " does not have permission to access worksheets for No Org 'None'.");
                        throw new Exception("User does not have permission to access worksheets for No Org 'None'.");
                    }
                }
            }
            
            String worksheetId = ServiceHibernate.getActiveWorksheet(username, orgIdInt, orgName);

            if (StringUtils.isBlank(worksheetId))
            {
                result.setSuccess(false).setMessage("No active worksheet found.");
            }
            else
            {
                String timezone = null; // this is for reminder.
                Worksheet data = WorksheetUtil.getModelByID(worksheetId, username, timezone, orgId, orgName);
                result.setData(data);
                result.setSuccess(true);
            }
        }
        catch (Exception e)
        {
            Log.log.error("Error while getting active worksheet.", e);
            result.setSuccess(false).setMessage("Error while getting active worksheet.");
        }

        return result;
    }

    /**
     * Sets the provided worksheet as the active worksheet
     * 
     * @param id The sys_id of the worksheet to set as active
     * @param request HttpServletRequest
     * @return {@link ResponseDTO} - Reponse Data Transfer Object 
     * @throws ServletException
     * @throws IOException
     */
    @RequestMapping(value = "/worksheet/setActive", method = { RequestMethod.POST }, headers = "Accept=application/json; charset=utf-8")
    @ResponseBody
    public ResponseDTO setActiveWorksheet(@RequestParam String id, @RequestParam(defaultValue="", value="RESOLVE.ORG_ID") String orgId, @RequestParam(defaultValue="", value="RESOLVE.ORG_NAME") String orgName, HttpServletRequest request) throws ServletException, IOException
    {
        ResponseDTO result = new ResponseDTO();

        String msg = "Called (/worksheet/setActive) setActiveWorksheet(" + id + ", " + (orgId != null ? orgId : "null") + ", " + (orgName != null ? orgName : "null") + ")";
        
        if (Log.log.isTraceEnabled())
        {
            try
            {
                throw new Exception (msg);
            }
            catch(Exception e)
            {
                Log.log.trace(e.getMessage(), e);
            }
        }
        
        try
        {
            if (StringUtils.isBlank(id))
            {
                result.setSuccess(false).setMessage("Worksheet ID must be provided");
            }
            else
            {
                String username = (String) request.getAttribute(Constants.HTTP_REQUEST_USERNAME);
                
                String orgIdInt = orgId;
                
                if (StringUtils.isNotBlank(orgId) && (orgId.equalsIgnoreCase(VO.STRING_DEFAULT) ||
                                                      orgId.equalsIgnoreCase(Constants.NIL_STRING)))
                {
                    orgIdInt = null;
                }
                
                if (orgIdInt == null && StringUtils.isNotBlank(orgName) &&
                    !orgName.equalsIgnoreCase(VO.STRING_DEFAULT) &&
                    !orgName.equalsIgnoreCase(OrgsVO.NONE_ORG_NAME))
                {
                    // Identify org id based on org name
                    
                    OrgsVO orgsVO = UserUtils.getOrg(null, orgName);
                    
                    if (orgsVO != null)
                    {
                        orgIdInt = orgsVO.getSys_id();
                    }
                }
                
                if (StringUtils.isNotBlank(orgName) && (orgName.equalsIgnoreCase(VO.STRING_DEFAULT) ||
                                                        orgName.equalsIgnoreCase(OrgsVO.NONE_ORG_NAME)))
                {
                    orgName = null;
                }
                
                if (!UserUtils.isSuperUser(username))
                {
                    if (StringUtils.isNotBlank(orgIdInt))
                    {
                        if (!UserUtils.isOrgAccessible(orgIdInt, username, false, false))
                        {
                            Log.log.warn("User " + username + " does not have permission to access worksheets for specified Org with id " + 
                                         orgId);
                            throw new Exception("User does not have permission to access worksheets for specified Org");
                        }
                    }
                    else
                    {
                        if (!UserUtils.isNoOrgAccessible(username))
                        {
                            Log.log.warn("User " + username + " does not have permission to access worksheets for No Org 'None'.");
                            throw new Exception("User does not have permission to access worksheets for No Org 'None'.");
                        }
                    }
                }
                
                WorksheetUtil.activeModel(username, id, orgIdInt, orgName);
                result.setSuccess(true);
            }
        }
        catch (Exception e)
        {
            Log.log.error("Error while setting the active worksheet.", e);
            result.setSuccess(false).setMessage("Error while setting the active worksheet.");
        }
        
        Log.log.debug(msg + " Result is " + result.isSuccess() + (StringUtils.isNotBlank(result.getMessage()) ? " Reason: " + result.getMessage() : ""));
        
        return result;
    }

    /**
     * Gets the details for a worksheet given the provided id
     * 
     * @param id sys_id of the worksheet to return
     * @param request  HttpServletRequest
     * @param response  HttpServletResponse
     * @return {@link ResponseDTO} - Reponse Data Transfer Object having object of type {@link Worksheet} 
     * @throws ServletException
     * @throws IOException
     */
    @RequestMapping(value = "/worksheet/get", method = { RequestMethod.POST, RequestMethod.GET }, headers = "Accept=application/json; charset=utf-8")
    @ResponseBody
    public ResponseDTO<Worksheet> getWorksheet(@RequestParam String id, HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException
    {
        ResponseDTO<Worksheet> result = new ResponseDTO<Worksheet>();
        
        try
        {
            String username = (String) request.getAttribute(Constants.HTTP_REQUEST_USERNAME);

            if(StringUtils.isNotBlank(id))
            {
                result = WorksheetSearchAPI.findWorksheetById(id, username);
                result.setSuccess(true);
            }
        }
        catch (Exception e)
        {
            if (!e.getMessage().startsWith("User does not belongs to"))
            {
                Log.log.error("Error while getting worksheet.", e);
                result.setSuccess(false).setMessage("Error while getting worksheet.");
            }
            else
            {
                Log.log.info("Error " + e.getLocalizedMessage() + 
                              " while getting worksheet for id " + id);
                result.setSuccess(false).setMessage(e.getLocalizedMessage());
            }
        }

        return result;
    }

    /**
     * Returns the next sequence number for the Worksheet.
     * 
     * @return {@link ResponseDTO} - Reponse Data Transfer Object having the next Sequence number String for Worksheet 
     * @throws ServletException
     * @throws IOException
     */
    @RequestMapping(value = "/worksheet/getNextNumber", method = { RequestMethod.POST, RequestMethod.GET }, headers = "Accept=application/json; charset=utf-8")
    @ResponseBody
    public ResponseDTO<String> getNextWorksheetNumber() throws ServletException, IOException
    {
        ResponseDTO<String> result = new ResponseDTO<String>();
        try
        {
            result.setData(SequenceUtils.getNextSequence(Constants.PERSISTENCE_SEQ_PRB));
            result.setSuccess(true);
        }
        catch (Exception e)
        {
            Log.log.error("Error while getting next sequence number for a worksheet.", e);
            result.setSuccess(false).setMessage("Error while getting next sequence number for a worksheet.");
        }

        return result;
    }

    
    /**
     * Returns a list of worksheet results when a Worksheet record is clicked from the Worksheet UI. It provides details for 
     * a particular worksheet. 
     * <p>
     * QueryDTO serves the purpose for pagination as a worksheet can have lot of executions and records. 
     * <p>
     * <u><b>Example</b></u>:</br> 
     * <code><pre>
     * id:babaad92589547f2b8487264793f86d3
     * hidden:false
     * filter:[]
     * page:1
     * start:0
     * limit:50
     * sort:[{"property":"sysUpdatedOn","direction":"ASC"}]
     * </pre></code>
     * 
     * <p>
     * Parameters in the Request object: 
     * <code><pre>
     *  USERNAME - username of the user who is logged in to Resolve
     * </pre></code>
     *  
     * <p>
     * Return object in response:
     * <code><pre>
     *  'records' : List of type {@link TaskResult}
     *  'total' : # of total records
     *  'data' : # of Open process request count based on wiki and worksheet  
     * </pre></code>
     *  
     *  
     *  
     * @param id sysId of the worksheet
     * @param wiki wiki name to get the number of Open process request count inside the worksheet in question, used for filtering 
     * @param query  {@link QueryDTO}, represents the query, pagination, sorting, grouping for the UI
     * @param hidden flag to hide the resolve tasks like start, end, etc to reduce the number of records on the grid
     * @param request  HttpServletRequest
     * @return {@link ResponseDTO} - Reponse Data Transfer Object having a List of type {@link TaskResult} and total # of records 
     * @throws ServletException
     * @throws IOException
     */
    @RequestMapping(value = "/worksheet/results", method = { RequestMethod.POST, RequestMethod.GET }, headers = "Accept=application/json; charset=utf-8")
    @ResponseBody
    public ResponseDTO listWorksheetResultsByWorksheetId(@RequestParam String id, @RequestParam(defaultValue="") String wiki, @ModelAttribute QueryDTO query, @RequestParam("hidden") Boolean hidden, HttpServletRequest request) throws ServletException, IOException
    {
        ResponseDTO result = new ResponseDTO<Object>();
        try
        {
            if (StringUtils.isNotBlank(id))
            {
                String username = (String) request.getAttribute(Constants.HTTP_REQUEST_USERNAME);
    
                query.addFilterItem(new QueryFilter("auto", id, "problemId", QueryFilter.EQUALS));
                if(hidden)
                {
                    query.addFilterItem(new QueryFilter(QueryFilter.BOOL_TYPE, "false", "hidden", QueryFilter.EQUALS));
                }
                result = WorksheetSearchAPI.searchTaskResults(query, username);

                //get the count of the open and the waiting processes, UI uses it to start/stop the polling for the result macro
                ProcessCounts counts = new ProcessCounts();
                counts.open = WorksheetSearchAPI.getOpenProcessRequestsCount(id, wiki, username);
                counts.waiting = WorksheetSearchAPI.getWaitProcessRequestsCount(id, wiki, username);
                
                result.setData(counts);
            }
            result.setSuccess(true);
        }
        catch (Exception e)
        {
            Log.log.error("Error while getting worksheet results.", e);
            result.setSuccess(false).setMessage("Error while getting worksheet results.");
        }
        return result;
    }
    
    @RequestMapping(value = "/sir/getTaskResults", method = { RequestMethod.GET }, headers = "Accept=application/json; charset=utf-8")
    @ResponseBody
    public ResponseDTO<TaskResult> listPbTaskResultsByWorksheetId(@RequestParam String problemId, @ModelAttribute QueryDTO query, @RequestParam("hidden") Boolean hidden, @RequestParam String[] columns,
    		HttpServletRequest request) throws ServletException, IOException
    {
        ResponseDTO<TaskResult> result = new ResponseDTO<>();
        try
        {
            if (StringUtils.isNotBlank(problemId))
            {
                String username = (String) request.getAttribute(Constants.HTTP_REQUEST_USERNAME);
    
                query.addFilterItem(new QueryFilter("auto", problemId, "problemId", QueryFilter.EQUALS));
                if(hidden)
                {
                    query.addFilterItem(new QueryFilter(QueryFilter.BOOL_TYPE, "false", "hidden", QueryFilter.EQUALS));
                }
                result = WorksheetSearchAPI.searchSirTaskResults(query, columns, username);
                result.setSuccess(true);
            }
            else
            {
                result.setMessage("problemId, (worksheetId), is needed to get the worksheet task results.");
                result.setSuccess(false);
            }
        }
        catch (Exception e)
        {
            Log.log.error("Error while getting worksheet results.", e);
            result.setSuccess(false).setMessage("Error while getting worksheet task results.");
        }
        return result;
    }

    /**
     * Returns the list of process requests in the system
     * 
     * Url: {@code http://localhost:8080/resolve/jsp/rsclient.jsp#RS.worksheet.ProcessRequests}
     * 
     * <p> 
     * <u><b>Example</b></u>:</br> 
     * <code><pre>
     *  filter: [{"field":"wiki","type":"auto","condition":"contains","value":"RBA9662"}]
     *  page: 3
     *  start: 100
     *  limit: 50
     *  sort: [{"property":"sysUpdatedOn","direction":"DESC"}]
     * </pre></code>
     *  
     * <p>
     * Parameters in the Request object: 
     * <code><pre>
     *  USERNAME - username of the user who is logged in to Resolve
     * </pre></code>
     *  
     * <p>
     * Return object in response:
     * <code><pre>
     *  'records' : List of type {@link ProcessRequest}
     *  'total' : # of total records in the table/db 
     * </pre></code>
     * 
     * 
     * @param query  {@link QueryDTO}, represents the query, pagination, sorting, grouping for the UI
     * @param request  HttpServletRequest
     * @return {@link ResponseDTO} - Reponse Data Transfer Object having a List(of type {@link ProcessRequest})  and total # of records 
     * @throws ServletException
     * @throws IOException
     */
    @RequestMapping(value = "/processrequest/list", method = { RequestMethod.POST, RequestMethod.GET }, headers = "Accept=application/json; charset=utf-8")
    @ResponseBody
    public ResponseDTO<ProcessRequest> listProcessRequests(@ModelAttribute QueryDTO query, HttpServletRequest request) throws ServletException, IOException
    {
        ResponseDTO<ProcessRequest> result = new ResponseDTO<ProcessRequest>();
        try
        {
            String username = (String) request.getAttribute(Constants.HTTP_REQUEST_USERNAME);

            result = WorksheetSearchAPI.searchProcessRequests(query, username);
            result.setSuccess(true);
        }
        catch (Exception e)
        {
            Log.log.error("Error retrieving process request list.", e);
            result.setSuccess(false).setMessage("Error retrieving process request list.");
        }

        return result;
    }


    /**
     * Get Process Request Details by sysId
     * 
     * <p>
     * Parameters in the Request object:
     * <code><pre> 
     *  USERNAME - username of the user who is logged in to Resolve
     * </pre></code>
     * <p>
     * Return object in response 
     * <code><pre>
     *  'data' : {@link ProcessRequest}
     * </pre></code>
     *  
     * @param id sysId of the Parser
     * @param request HttpServletRequest
     * @return {@link ResponseDTO} - Reponse Data Transfer Object with 'data' of type {@link ProcessRequest}
     * @throws ServletException
     * @throws IOException
     */
    @RequestMapping(value = "/processrequest/get", method = { RequestMethod.POST }, headers = "Accept=application/json; charset=utf-8")
    @ResponseBody
    public ResponseDTO<ProcessRequest> getProcessRequest(@RequestParam String id, HttpServletRequest request) throws ServletException, IOException
    {
        ResponseDTO<ProcessRequest> result = new ResponseDTO<ProcessRequest>();

        try
        {
            String username = (String) request.getAttribute(Constants.HTTP_REQUEST_USERNAME);
            result = WorksheetSearchAPI.findProcessRequestsById(id, username);
            result.setSuccess(true);
        }
        catch (Exception e)
        {
            Log.log.error("Error retrieving process request.", e);
            result.setSuccess(false).setMessage("Error retrieving process request.");
        }
        return result;
    }

    
    /**
     * Deletes the provided process requests from the backend. If there are no
     * ids in the query, then we have to use the filter criteria to delete all
     * the records across all pages for what the user has specified (bulk
     * delete).
     * <p>
     * Delete with QueryDTO is not functional at the moment and is for future use.
     * <p>
     * Parameters in the Request object: 
     * <code><pre>
     *  USERNAME - username of the user who is logged in to Resolve
     * </pre></code> 
     * 
     * @param query Criteria to delete based on Query. Presently not functional and will be ignored
     * @param ids the list of process requests to delete
     * @param request HttpServletRequest
     * @return {@link ResponseDTO} - Reponse Data Transfer Object with status as 'true' or 'false'
     * @throws ServletException
     * @throws IOException
     */
    @RequestMapping(value = "/processrequest/delete", method = { RequestMethod.POST }, headers = "Accept=application/json; charset=utf-8")
    @ResponseBody
    public ResponseDTO deleteProcessRequest(@ModelAttribute QueryDTO query, @RequestParam List<String> ids, @RequestParam String status, HttpServletRequest request) throws ServletException, IOException
    {
        ResponseDTO result = new ResponseDTO();

        try
        {
            if (ids == null || ids.size() == 0)
            {
                result.setSuccess(false).setMessage("Process Request IDs must be provided");
            }
            else
            {
                String username = (String) request.getAttribute(Constants.HTTP_REQUEST_USERNAME);
                if (ids.get(0).equalsIgnoreCase("all"))
                {
                	ServiceWorksheet.deleteAllProcessRequestRows(status, username);
                }
                else
                {
                	ServiceWorksheet.deleteProcessRequestRows(ids, true, username);
                }
                result.setSuccess(true);
            }
        }
        catch (Exception e)
        {
            Log.log.error("Error while deleting process request(s).", e);
            result.setSuccess(false).setMessage("Error while deleting process request(s).");
        }
        return result;
    }


    /**
     * 
     * Aborts the provided process requests to prevent them from being executed.
     * If there are no ids in the query, then we have to use the filter criteria
     * to abort all the records across all pages for what the user has specified
     * (bulk abort).
     *
     * @param query Criteria to abort based on Query. Presently not functional and will be ignored
     * @param ids the list of process requests to abort. If only one element, "ALL", all process requests with the given status will be aborted.
     * @param status: all the process reqeusts with this status will be aborted. If set to ALL, all process requests will be aborted.
     * @param request HttpServletRequest
     * @return {@link ResponseDTO} - Reponse Data Transfer Object with status as 'true' or 'false'
     * @throws ServletException
     * @throws IOException
     */
    @RequestMapping(value = "/processrequest/abort", method = { RequestMethod.POST }, headers = "Accept=application/json; charset=utf-8")
    @ResponseBody
    public ResponseDTO abortProcessRequest(@ModelAttribute QueryDTO query, @RequestParam List<String> ids, @RequestParam String status, HttpServletRequest request) throws ServletException, IOException
    {
        try
        {
            if(ids != null && ids.size() > 0)
            {
            	String username = (String) request.getAttribute(Constants.HTTP_REQUEST_USERNAME);
            	
            	if (ids.get(0).equalsIgnoreCase("all"))
            	{
            		QueryDTO queryDTO = new QueryDTO();
            		if (!status.equalsIgnoreCase("all"))
            		{
            			queryDTO.addFilterItem(new QueryFilter("status", QueryFilter.EQUALS, status.toLowerCase()));
            		}
            		ResponseDTO<ProcessRequest> result = WorksheetSearchAPI.searchProcessRequests(query, username);
            		
            		if( result.getRecords() != null)
            		{
            			for (ProcessRequest processRequest : result.getRecords())
            			{
            				abortProcessRequest(processRequest, username);
            			}
            		}
            	}
            	else
            	{
	                for(String processId : ids)
	                {
	                    ResponseDTO<ProcessRequest> responseDTO = WorksheetSearchAPI.findProcessRequestsById(processId, username);
	                    if(responseDTO !=  null)
	                    {
	                        ProcessRequest processRequest = responseDTO.getData();
	                        abortProcessRequest(processRequest, username);
	                    }
	                }
            	}
            }
            return new ResponseDTO().setSuccess(true);
        }
        catch (Exception e)
        {
            Log.log.error("Error while aborting process request.", e);
            return new ResponseDTO().setSuccess(false).setMessage("Error while aborting process request.");
        }
    }
    
    private void abortProcessRequest(ProcessRequest processRequest, String username) throws Exception
    {
    	Map<String, String> params = new HashMap<String, String>();
        params.put(Constants.EXECUTE_PROCESSID, processRequest.getSysId());
        params.put(Constants.EXECUTE_PROBLEMID, processRequest.getProblem());
        params.put(Constants.EXECUTE_USERID, username);
        Log.log.debug("Sending message to abort process id: " + processRequest.getSysId() + " and problemId: " + processRequest.getNumber());
        ExecuteRunbook.abortProcess(params);
    }

    /**
     * 
     * Gets the list of task results in the system
     *
     * Url: {@code http://localhost:8080/resolve/jsp/rsclient.jsp#RS.worksheet.TaskResults/}
     * 
     * <p> 
     * <u><b>Example</b></u>:</br> 
     * <code><pre>
     *  filter: [{"field":"summary","type":"auto","condition":"equals","value":"Expected value | Got value"}]
     *  page: 3
     *  start: 100
     *  limit: 50
     *  sort: [{"property":"sysUpdatedOn","direction":"DESC"}]
     * </pre></code>
     *  
     * <p>
     * Parameters in the Request object: 
     * <code><pre>
     *  USERNAME - username of the user who is logged in to Resolve
     * </pre></code>
     *  
     * <p>
     * Return object in response:
     * <code><pre>
     *  'records' : List of type {@link TaskResult}
     *  'total' : # of total records in the table/db 
     * </pre></code>     
     * 
     * 
     * @param query  {@link QueryDTO}, represents the query, pagination, sorting, grouping for the UI
     * @param request  HttpServletRequest
     * @return {@link ResponseDTO} - Reponse Data Transfer Object having a List  and total # of records 
     * @throws ServletException
     * @throws IOException
     */
    @RequestMapping(value = "/taskresult/list", method = { RequestMethod.POST, RequestMethod.GET }, headers = "Accept=application/json; charset=utf-8")
    @ResponseBody
    public ResponseDTO<TaskResult> listTaskResults(@ModelAttribute QueryDTO query, HttpServletRequest request) throws ServletException, IOException
    {
        ResponseDTO<TaskResult> result = new ResponseDTO<TaskResult>();
        try
        {
            String username = (String) request.getAttribute(Constants.HTTP_REQUEST_USERNAME);

            result = WorksheetSearchAPI.searchTaskResults(query, username);
            result.setSuccess(true);
        }
        catch (Exception e)
        {
            Log.log.error("Error retrieving Task Results.", e);
            result.setSuccess(false).setMessage("Error retrieving Task Results.");
        }

        return result;
    }

    /**
     * 
     * Returns the provided task result
     * 
     * <p>
     * Parameters in the Request object:
     * <code><pre> 
     *  USERNAME - username of the user who is logged in to Resolve
     * </pre></code>
     * <p>
     * Return object in response 
     * <code><pre>
     *  'data' : {@link TaskResult}
     * </pre></code>
     *
     * 
     * @param id the id of the task result to return
     * @param request HttpServletRequest
     * @return {@link ResponseDTO} - Reponse Data Transfer Object with 'data' of type {@link TaskResult}
     * @throws ServletException
     * @throws IOException
     */
    @RequestMapping(value = "/taskresult/get", method = { RequestMethod.POST, RequestMethod.GET }, headers = "Accept=application/json; charset=utf-8")
    @ResponseBody
    public ResponseDTO<TaskResult> getTaskResult(@RequestParam String id, HttpServletRequest request) throws ServletException, IOException
    {
        ResponseDTO<TaskResult> result = new ResponseDTO<TaskResult>();

        try
        {
            String username = (String) request.getAttribute(Constants.HTTP_REQUEST_USERNAME);
            result = WorksheetSearchAPI.findTaskResultById(id, username);
        }
        catch (Exception e)
        {
            Log.log.error("Error in getting task result.", e);
            result.setSuccess(false).setMessage("Error in getting task result.");
        }

        return result;
    }

    /**
     * 
     * Deletes the provided task results from the backend. If there are no ids
     * in the query, then we have to use the filter criteria to delete all the
     * records across all pages for what the user has specified (bulk delete).
     * <p>
     * Delete with QueryDTO is not functional at the moment and is for future use.
     * <p>
     * Parameters in the Request object: 
     * <code><pre>
     *  USERNAME - username of the user who is logged in to Resolve
     * </pre></code> 
     * 
     * @param query Criteria to delete based on Query. Presently not functional and will be ignored
     * @param ids the list of task results to delete
     * @param request HttpServletRequest
     * @return {@link ResponseDTO} - Reponse Data Transfer Object with status as 'true' or 'false'
     * @throws ServletException
     * @throws IOException
     */
    @RequestMapping(value = "/taskresult/delete", method = { RequestMethod.POST }, headers = "Accept=application/json; charset=utf-8")
    @ResponseBody
    public ResponseDTO deleteTaskResult(@ModelAttribute QueryDTO query, @RequestParam List<String> ids, HttpServletRequest request) throws ServletException, IOException
    {
        ResponseDTO result = new ResponseDTO();

        try
        {
            if (ids == null || ids.size() == 0)
            {
                result.setSuccess(false).setMessage("Task Result IDs must be provided");
            }
            else
            {
                String username = (String) request.getAttribute(Constants.HTTP_REQUEST_USERNAME);
                ServiceWorksheet.deleteTaskResultRows(ids, true, username);
                result.setSuccess(true);
            }
        }
        catch (Exception e)
        {
            Log.log.error("Error while deleting task result.", e);
            result.setSuccess(false).setMessage("Error while deleting task result.");
        }
        return result;
    }
    
    class ProcessCounts
    {
        public long open = 0;
        public long waiting = 0;
    }
    
    /**
     * Gets the details for a worksheet given the provided id
     * 
     * @param id sys_id of the worksheet to return
     * @param request  HttpServletRequest
     * @param response  HttpServletResponse
     * @return {@link ResponseDTO} - Reponse Data Transfer Object having object of type {@link Worksheet} 
     * @throws ServletException
     * @throws IOException
     */
    @RequestMapping(value = "/worksheet/getArchive", method = { RequestMethod.POST, RequestMethod.GET }, headers = "Accept=application/json; charset=utf-8")
    @ResponseBody
    public ResponseDTO<Worksheet> getArchiveWorksheet(@RequestParam String id, HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException
    {
        ResponseDTO<Worksheet> result = new ResponseDTO<Worksheet>();
        try
        {
            String username = (String) request.getAttribute(Constants.HTTP_REQUEST_USERNAME);
            result.setData(ServiceWorksheet.getArchiveWorksheet(id, username));
            result.setSuccess(true);
        }
        catch (Exception e)
        {
            Log.log.error("Error retrieving archived worksheet.", e);
            result.setSuccess(false).setMessage("Error retrieving archived worksheet.");
        }

        return result;
    }

    @RequestMapping(value = "/worksheet/listArchive", method = { RequestMethod.POST, RequestMethod.GET }, headers = "Accept=application/json; charset=utf-8")
    @ResponseBody
    public ResponseDTO<Worksheet> listArchiveWorksheets(@ModelAttribute QueryDTO query, HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException
    {
        ResponseDTO<Worksheet> result = new ResponseDTO<Worksheet>();
        try
        {
            String username = (String) request.getAttribute(Constants.HTTP_REQUEST_USERNAME);
            result.setRecords(ServiceWorksheet.listArchiveWorksheets(query, username));
            int total = ServiceHibernate.getTotalHqlCount(query);
            result.setTotal(total);
            result.setSuccess(true);
        }
        catch (Exception e)
        {
            Log.log.error("Error retrieving archived worksheet list.", e);
            result.setSuccess(false).setMessage("Error retrieving archived worksheet list.");
        }

        return result;
    }
    
    @RequestMapping(value = "/taskresult/getArchiveTaskResult", method = { RequestMethod.POST, RequestMethod.GET }, headers = "Accept=application/json; charset=utf-8")
    @ResponseBody
    public ResponseDTO<TaskResult> getArchiveTaskResult(@RequestParam String id, HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException
    {
        ResponseDTO<TaskResult> result = new ResponseDTO<TaskResult>();
        try
        {
            String username = (String) request.getAttribute(Constants.HTTP_REQUEST_USERNAME);
            result.setData(ServiceWorksheet.getArchiveTaskResult(id, username));
            result.setSuccess(true);
        }
        catch (Exception e)
        {
            Log.log.error("Error retrieving archived task result.", e);
            result.setSuccess(false).setMessage("Error retrieving archived task result.");
        }

        return result;
    }

    @RequestMapping(value = "/worksheet/archiveResults", method = { RequestMethod.POST, RequestMethod.GET }, headers = "Accept=application/json; charset=utf-8")
    @ResponseBody
    public ResponseDTO<TaskResult> listArchiveWorksheetResultsByWorksheetId(@RequestParam String id, @RequestParam(defaultValue="") String wiki, @ModelAttribute QueryDTO query, @RequestParam("hidden") Boolean hidden, HttpServletRequest request) throws ServletException, IOException
    {
        ResponseDTO<TaskResult> result = new ResponseDTO<TaskResult>();
        try
        {
            if (StringUtils.isNotBlank(id))
            {
                String username = (String) request.getAttribute(Constants.HTTP_REQUEST_USERNAME);
                result = ServiceWorksheet.listArchiveWorksheetResultsByWorksheetId(id, query, hidden, username);
            }
            result.setSuccess(true);
        }
        catch (Exception e)
        {
            Log.log.error("Error while retrieving list of archived worksheet results.", e);
            result.setSuccess(false).setMessage("Error while retrieving list of archived worksheet results.");
        }
        return result;
    }
    
    /**
     * This entry point is same as AjaxWSDATA.getWorksheetWSDATA. We had to create this entry point for the Worksheet UI so that App Rights can be 
     * applied to it.
     * 
     * 
     * @param problemId
     * @param keyList
     * @param request
     * @param response
     * @return
     * @throws ServletException
     * @throws IOException
     */
    @RequestMapping(value = "/worksheet/wsdata/getMap", method = { RequestMethod.GET, RequestMethod.POST }, headers = "Accept=application/json; charset=utf-8")
    @ResponseBody
    public ResponseDTO getWorksheetWSDATA(@RequestParam String problemId, @RequestParam(required = false) List<String> keyList, HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException
    {
        return new AjaxWSDATA().getWorksheetWSDATA(problemId, keyList, request, response);
    }
    
    /**
     * Gets the details for a worksheet given the provided id
     * This api is same as the 'get' api but has a different entry point so that App Rights can be applied to it.
     * 
     * @param id sys_id of the worksheet to return
     * @param request  HttpServletRequest
     * @param response  HttpServletResponse
     * @return {@link ResponseDTO} - Reponse Data Transfer Object having object of type {@link Worksheet} 
     * @throws ServletException
     * @throws IOException
     */
    @RequestMapping(value = "/worksheet/getWS", method = { RequestMethod.POST, RequestMethod.GET }, headers = "Accept=application/json; charset=utf-8")
    @ResponseBody
    public ResponseDTO<Worksheet> getWorksheetWS(@RequestParam String id, HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException
    {
        return getWorksheet(id, request, response);
    }
    

    /**
     * Gets the execution history of a SIR for by worksheet id, incident id and other parameters. It supports paging, simulated "free form" search by term, selected task/activity names etc.
     * 
     * @param problemId  sys_id of current SIR worksheet  
     * @param incidentId  incident id of  current SIR  
     * @param query  QueryDTO object to provide paging params ( "start", "limit"), and search term param ("searchTerm")  
     * @param columns  string array to define what columns are included in "free form" term search. If null/empty, then all columns will be searched for match. Support search column names are: "activityId", "activityName", "PROCESSID", "SEVERITY", "CONDITION", "SUMMARY"   
     * @param selectAll  corresponding to "ShowAll" option in UI. Basically, if set to true, UI will show all SIR task/activity in execution history   
     * @param activityNames  array of SIR activity/task names included in execution history search. If names are provided, they function as a filter on execution history results.    
     * @param sources  array of SIR source names included in execution history search. If names are provided, they function as a filter on execution history results.  
     * @param request  HttpServletRequest
     * @param response  HttpServletResponse
     * @return {@link ResponseDTO} - Reponse Data Transfer Object having object of type {@link Map<String,String>} 
     * @throws ServletException
     * @throws IOException
     */

    @RequestMapping(value = "/sir/getExecutionHistory", method = { RequestMethod.GET }, headers = "Accept=application/json; charset=utf-8")
    @ResponseBody
    public ResponseDTO<Map<String, Object>> getExecutionHistory(@RequestParam String problemId, @ModelAttribute QueryDTO query, @RequestParam("selectAll") Boolean selectAll, @RequestParam String[] columns, @RequestParam String[] activityNames, @RequestParam String[] sources, @RequestParam String incidentId,   
    		HttpServletRequest request) throws ServletException, IOException
    {
        ResponseDTO<Map<String,Object>> result = new ResponseDTO<>();
        try
        {
            if (StringUtils.isNotBlank(problemId))
            {
                String username = (String) request.getAttribute(Constants.HTTP_REQUEST_USERNAME);
    
                query.addFilterItem(new QueryFilter("auto", problemId, "problemId", QueryFilter.EQUALS));
                result = WorksheetSearchAPI.searchExecutionHistory(query, columns, username, problemId, selectAll, activityNames, sources, incidentId);
                result.setSuccess(true);
            }
            else
            {
                result.setMessage("problemId, (worksheetId), is needed to get the worksheet task results.");
                result.setSuccess(false);
            }
        }
        catch (Exception e)
        {
            Log.log.error("Error while getting worksheet results.", e);
            result.setSuccess(false).setMessage("Error while getting worksheet task results.");
        }
        return result;
    }
    
}
