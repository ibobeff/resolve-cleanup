/******************************************************************************
* (C) Copyright 2014
*
* Resolve Systems, LLC
*
* All rights reserved
* This software is distributed under license and may not be used, copied,
* modified, or distributed without the express written permission of
* Resolve Systems, LLC.
*
******************************************************************************/

package com.resolve.rsview.controller;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.fileupload.FileUploadException;
import org.apache.commons.lang3.StringUtils;
import org.codehaus.jackson.map.DeserializationConfig;
import org.codehaus.jackson.map.ObjectMapper;
import org.springframework.stereotype.Controller;
import org.springframework.stereotype.Service;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.context.request.WebRequest;

import com.resolve.dto.UploadFileDTO;
import com.resolve.graph.social.ui.dto.UserPickerQueryModelDTO;
import com.resolve.rsbase.MainBase;
import com.resolve.rsview.main.MIndex;
import com.resolve.rsview.main.Main;
import com.resolve.search.customform.CustomFormIndexAPI;
import com.resolve.services.ServiceHibernate;
import com.resolve.services.constants.RightTypeEnum;
import com.resolve.services.hibernate.util.vo.ComboboxModelDTO;
import com.resolve.services.util.PropertiesUtil;
import com.resolve.services.vo.DefaultDataDTO;
import com.resolve.services.vo.QueryDTO;
import com.resolve.services.vo.ResponseDTO;
import com.resolve.services.vo.form.FormSubmitDTO;
import com.resolve.services.vo.form.RsColumnDTO;
import com.resolve.services.vo.form.RsFormDTO;
import com.resolve.services.vo.form.RsPanelDTO;
import com.resolve.services.vo.form.RsTabDTO;
import com.resolve.services.vo.form.RsUIField;
import com.resolve.util.Constants;
import com.resolve.util.Log;
import com.resolve.util.SQLUtils;
import com.resolve.workflow.form.QueryCustomForm;
import com.resolve.workflow.form.dto.CustomFormDTO;
import com.resolve.workflow.form.dto.CustomTableDTO;
import com.resolve.workflow.form.dto.RolesModelDTO;

import net.sf.json.JSONObject;

@Controller
@Service
public class AjaxCustomForm extends GenericController
{

    /**
     * get the model of the custom form - use this link for the form builder
     *
     * @param request
     * @param response
     * @return
     * @throws ServletException
     * @throws IOException
     */
    @RequestMapping(value = "/form/getformfb", method = { RequestMethod.POST, RequestMethod.GET }, headers = "Accept=application/json; charset=utf-8")
    @ResponseBody
    public ResponseDTO getFormFB(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException
    {
        String username = (String) request.getAttribute(Constants.HTTP_REQUEST_USERNAME);

        // either one will be fine to get the form details
        String viewName = request.getParameter("view");
        String formSysId = request.getParameter("formsysid");

        ResponseDTO result = new ResponseDTO();

        try
        {
            // test to get the form
            // To edit the form from Form Builder, user needs to have ADMIN
            // rights

            // RsFormDTO form = testNewCustomForm();
            RsFormDTO form = ServiceHibernate.getMetaFormView(formSysId, viewName, null, username, RightTypeEnum.admin, null, false);

            result.setSuccess(true).setData(form);
        }
        catch (Exception e)
        {
            Log.log.error("error in getting the form info: " + viewName + " - sysId:" + formSysId, e);
            result.setSuccess(false).setMessage("Error for the form :" + viewName + ".");
        }

        return result;
    }

    /**
     * use this link for viewing the form -
     *
     * @param request
     * @param response
     * @return
     * @throws ServletException
     * @throws IOException
     */
    @RequestMapping(value = "/form/getform", method = { RequestMethod.POST, RequestMethod.GET }, headers = "Accept=application/json; charset=utf-8")
    @ResponseBody
    public ResponseDTO getForm(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException
    {
        String username = (String) request.getAttribute(Constants.HTTP_REQUEST_USERNAME);

        // either one will be fine to get the form details
        String viewName = request.getParameter("view");
        String formSysId = request.getParameter("formsysid");
        String recordSysId = request.getParameter("recordid");
        
        boolean formDefinitionOnly = false;
        String formDefinitionOnlyStr = request.getParameter("formdefinition"); //true or false
        if(StringUtils.isNotEmpty(formDefinitionOnlyStr) && formDefinitionOnlyStr.equalsIgnoreCase("true"))
        {
            formDefinitionOnly = true;
        }
        //TODO: Jeet, use the recordSysId to not populate the sequence field default value if its not empty

        /*
         * By default, it will RENDER in EDIT mode. To view the form in rendered
         * mode, it will be VIEW rights but each field will be validated for
         * that user
         */
        String mode = request.getParameter("rights") != null ? "view" : "edit";
        RightTypeEnum right = RightTypeEnum.edit;
        if (mode.equalsIgnoreCase("view"))
        {
            right = RightTypeEnum.view;
        }

        ResponseDTO result = new ResponseDTO();

        try
        {
            // test to get the form
            // RsFormDTO form = testNewCustomForm();
            RsFormDTO form = ServiceHibernate.getMetaFormView(formSysId, viewName, null, username, right, recordSysId, formDefinitionOnly);
                        

            Pattern p = Pattern.compile("\\$\\{(.*?)\\}");
            for (RsPanelDTO panel : form.getPanels())
            {
                for (RsTabDTO tab : panel.getTabs())
                {
                    for (RsColumnDTO column : tab.getColumns())
                    {
                        if (column.getFields() != null)
                        {
                            for (RsUIField field : column.getFields())
                            {
                                if (StringUtils.isNotBlank(field.getHiddenValue()))
                                {
                                    // get the property name
                                    Matcher m = p.matcher(field.getHiddenValue());
                                    if (m.matches())
                                    {
                                        String property = m.group(1);
                                        if (StringUtils.isNotBlank(PropertiesUtil.getPropertyString(property)))
                                        {
                                            field.setHiddenValue(PropertiesUtil.getPropertyString(property));
                                        }
                                    }
                                }

                                if (StringUtils.isNotBlank(field.getDefaultValue()))
                                {
                                    // get the property name
                                    java.util.regex.Matcher m = p.matcher(field.getDefaultValue());
                                    if (m.matches())
                                    {
                                        String property = m.group(1);
                                        if (StringUtils.isNotBlank(PropertiesUtil.getPropertyString(property)))
                                        {
                                            field.setValue(PropertiesUtil.getPropertyString(property));
                                        }
                                    }
                                }

                                if (StringUtils.isNotBlank(field.getValue()))
                                {
                                    // get the property name
                                    java.util.regex.Matcher m = p.matcher(field.getValue());
                                    if (m.matches())
                                    {
                                        String property = m.group(1);
                                        if (StringUtils.isNotBlank(PropertiesUtil.getPropertyString(property)))
                                        {
                                            field.setValue(PropertiesUtil.getPropertyString(property));
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
            }

            result.setSuccess(true).setData(form);
        }
        catch (Exception e)
        {
            if (e.getMessage().contains("View with name ") && e.getMessage().contains(" does not exist."))
            {
                Log.log.warn("Error [" + e.getLocalizedMessage() + "] in getting form for " + 
                             (StringUtils.isNotBlank(viewName) ? "view: " + viewName + " " : "") +
                             (StringUtils.isNotBlank(formSysId) ? "sysId: " + formSysId: ""));
            }
            else
            {
                Log.log.error("Error [" + e.getLocalizedMessage() + "] in getting form for " + 
                              (StringUtils.isNotBlank(viewName) ? "view: " + viewName + " " : "") +
                              (StringUtils.isNotBlank(formSysId) ? "sysId: " + formSysId : ""), e);
            }
            
            result.setSuccess(false).setMessage("Error in getting form for " + 
                                                (StringUtils.isNotBlank(viewName) ? "view: " + viewName + " " : "") +
                                                (StringUtils.isNotBlank(formSysId) ? "sysId: " + formSysId: ""));
        }

        return result;
    }

    /**
     * api to save the form from the form builder
     *
     * Refer to -->
     * http://blog.springsource.org/2010/01/25/ajax-simplifications-
     * in-spring-3-0/
     *
     * @param request
     * @param response
     * @return
     * @throws ServletException
     * @throws IOException
     */
    @RequestMapping(value = "/formbuilder/submitfb", method = { RequestMethod.POST, RequestMethod.GET }, headers = "Accept=application/json; charset=utf-8")
    @ResponseBody
    //public ResponseDTO submitFromFormBuilder(@RequestBody RsFormDTO form, HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException
    public ResponseDTO submitFromFormBuilder(@RequestBody JSONObject form, HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException
    {
        String username = (String) request.getAttribute(Constants.HTTP_REQUEST_USERNAME);
        ResponseDTO result = new ResponseDTO();
        
        try
        {
            ObjectMapper mapper = new ObjectMapper();
            // configure the mapping to accept unknown fields
            mapper.configure(DeserializationConfig.Feature.FAIL_ON_UNKNOWN_PROPERTIES, false);
            RsFormDTO entity = mapper.readValue(form.toString(), RsFormDTO.class);
            entity.setUsername(username);
            RsFormDTO viewx = ServiceHibernate.saveMetaFormView(entity, username);
            result.setSuccess(true).setData(viewx);
        }
        catch (Exception e)
        {
            Log.log.error("error in getting the user info: ", e);
            result.setSuccess(false).setMessage("Error while saving the view");
        }
        
        return result;
    }

    /**
     * api when a rendered form is submitted by the end user
     *
     * @param request
     * @param response
     * @return
     * @throws ServletException
     * @throws IOException
     */
    @RequestMapping(value = "/form/submit", method = { RequestMethod.POST, RequestMethod.GET }, headers = "Accept=application/json; charset=utf-8")
    @ResponseBody
    public ResponseDTO formSubmit(@RequestBody FormSubmitDTO formSubmitDTO, HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException
    {
        String username = (String) request.getAttribute(Constants.HTTP_REQUEST_USERNAME);
        ResponseDTO result = new ResponseDTO();

        try
        {
            Map<String, String> hmValues = ServiceHibernate.executeForm(formSubmitDTO, username);
            
            for (String str : hmValues.keySet())
            {
                if (StringUtils.isNotBlank(hmValues.get(str)) &&  hmValues.get(str).contains("Failed:"))
                {
                    result.setSuccess(false);
                    result.setMessage(hmValues.get(str));
                    return result;
                }
            }
            
            result.setData(hmValues);
            result.setSuccess(true);
        }
        catch (Exception e)
        {
            Log.log.error(e.getMessage(), e);
            result.setSuccess(false).setMessage("Error while submitting the form.");
        }

        return result;
    }

    //    public RsFormDTO testNewCustomForm()
    //    {
    //        // RsFormDTO form = new
    //        // ExtMetaFormLookup("8a9482e53805a125013805af6d690004", null, null,
    //        // "admin", RightType.edit).getMetaFormViewX();
    //        // RsFormDTO form = new
    //        // ExtMetaFormLookup("8a9482e53876f1690138772461cf0041", null, null,
    //        // "admin", RightType.edit).getMetaFormViewX();
    //
    //        // sys_id of meta_form_view table
    //        RsFormDTO form = new ExtMetaFormLookup("8a9482f4397ddef901397e11fa1d0012", null, null, "admin", RightType.edit, null).getMetaFormViewX();
    //        // RsFormDTO form = new
    //        // ExtMetaFormLookup("8a9482f437c95fdf0137dde5a27a0055", null, null,
    //        // "admin", RightType.edit).getMetaFormViewX();
    //
    //        // convert it into json and see what u get
    //        ObjectMapper mapper = new ObjectMapper();
    //        try
    //        {
    //            // used for form builder and rendering of the form
    //            System.out.println(mapper.writeValueAsString(form));
    //
    //            // used to update the custom table
    //            // System.out.println(mapper.writeValueAsString(form.convertToBaseModel()));
    //        }
    //        catch (Exception e)
    //        {
    //            e.printStackTrace();
    //        }
    //
    //        System.out.println("==========================");
    //
    //        return form;
    //
    //    }

    /**
     * get the list of custom tables
     *
     * @param request
     * @param response
     * @return
     * @throws ServletException
     * @throws IOException
     */
    @RequestMapping(value = "/form/getcustomtables", method = { RequestMethod.POST, RequestMethod.GET }, headers = "Accept=application/json; charset=utf-8")
    @ResponseBody
    public ResponseDTO getCustomTables(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException
    {
        //        String username = (String) request.getAttribute(Constants.HTTP_REQUEST_USERNAME);
        ResponseDTO result = new ResponseDTO();
        try
        {
            // Get the list of custom tables currently in the system
            List<CustomTableDTO> tables = new QueryCustomForm().getCustomTables();
            result.setSuccess(true).setRecords(tables);
        }
        catch (Exception e)
        {
            Log.log.error("error in getting the custom table info", e);
            result.setSuccess(false).setMessage("Error for the custom tables");
        }

        return result;
    }

    /**
     * get the list of custom table columns
     *
     * @param request
     * @param response
     * @return
     * @throws ServletException
     * @throws IOException
     */
    @RequestMapping(value = "/form/getcustomtablecolumns", method = { RequestMethod.POST, RequestMethod.GET }, headers = "Accept=application/json; charset=utf-8", params = { "tableName" })
    @ResponseBody
    public ResponseDTO getCustomTableColumns(@RequestParam String tableName, HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException
    {
        //        String username = (String) request.getAttribute(Constants.HTTP_REQUEST_USERNAME);
        ResponseDTO result = new ResponseDTO();

        try
        {
            List<RsUIField> columns = new QueryCustomForm().getColumnsForTable(tableName);
            result.setSuccess(true).setRecords(columns);
        }
        catch (Exception e)
        {
            Log.log.error("error in getting the custom table info", e);
            result.setSuccess(false).setMessage("Error for the custom tables");
        }

        return result;
    }

    /**
     * get the list of custom tables for reference widget
     *
     * @param request
     * @param response
     * @return
     * @throws ServletException
     * @throws IOException
     */
    @RequestMapping(value = "/form/getcustomtablesforref", method = { RequestMethod.POST, RequestMethod.GET }, headers = "Accept=application/json; charset=utf-8")
    @ResponseBody
    public ResponseDTO getCustomTablesForReferenceWidget(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException
    {
        //        String username = (String) request.getAttribute(Constants.HTTP_REQUEST_USERNAME);
        ResponseDTO result = new ResponseDTO();
        try
        {
            // Get the list of custom tables currently in the system
            List<ComboboxModelDTO> tables = new QueryCustomForm().getCustomTablesForReferenceWidget();
            result.setSuccess(true).setRecords(tables);
        }
        catch (Exception e)
        {
            Log.log.error("error in getting the custom table info", e);
            result.setSuccess(false).setMessage("Error for the custom tables");
        }

        return result;
    }

    /**
     * get the list of runbooks
     *
     * @param request
     * @param response
     * @return
     * @throws ServletException
     * @throws IOException
     */
    @RequestMapping(value = "/form/getrunbooks", method = { RequestMethod.POST, RequestMethod.GET }, headers = "Accept=application/json; charset=utf-8")
    @ResponseBody
    public ResponseDTO getRunbooks(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException
    {
        //        String username = (String) request.getAttribute(Constants.HTTP_REQUEST_USERNAME);
        ResponseDTO result = new ResponseDTO();

        try
        {
            // Get the runbooks for the user to choose from
            List<ComboboxModelDTO> runbooks = new QueryCustomForm().getListOfRunbooks();
            result.setSuccess(true).setRecords(runbooks);
        }
        catch (Exception e)
        {
            Log.log.error("error in getting the runbooks", e);
            result.setSuccess(false).setMessage("Error getting runbooks");
        }

        return result;
    }

    /**
     * get the list of scripts
     *
     * @param request
     * @param response
     * @return
     * @throws ServletException
     * @throws IOException
     */
    @RequestMapping(value = "/form/getscripts", method = { RequestMethod.POST, RequestMethod.GET }, headers = "Accept=application/json; charset=utf-8")
    @ResponseBody
    public ResponseDTO getScripts(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException
    {
        //        String username = (String) request.getAttribute(Constants.HTTP_REQUEST_USERNAME);
        ResponseDTO result = new ResponseDTO();
        try
        {
            // Get the scripts for the user to choose from
            List<ComboboxModelDTO> scripts = new QueryCustomForm().getListOfScripts();
            result.setSuccess(true).setRecords(scripts);
        }
        catch (Exception e)
        {
            Log.log.error("error in getting the scripts", e);
            result.setSuccess(false).setMessage("Error getting scripts");
        }

        return result;
    }

    /**
     * get the list of action tasks
     *
     * @param request
     * @param response
     * @return
     * @throws ServletException
     * @throws IOException
     */
    @RequestMapping(value = "/form/getactiontasks", method = { RequestMethod.POST, RequestMethod.GET }, headers = "Accept=application/json; charset=utf-8")
    @ResponseBody
    public ResponseDTO getActionTasks(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException
    {
        //        String username = (String) request.getAttribute(Constants.HTTP_REQUEST_USERNAME);
        ResponseDTO result = new ResponseDTO();
        try
        {
            // Get the scripts for the user to choose from
            List<ComboboxModelDTO> actiontasks = new QueryCustomForm().getListOfActionTask();
            result.setSuccess(true).setRecords(actiontasks);
        }
        catch (Exception e)
        {
            Log.log.error("error in getting the actiontasks", e);
            result.setSuccess(false).setMessage("Error getting action tasks");
        }

        return result;
    }

    /**
     * get the list of runbooks
     *
     * @param request
     * @param response
     * @return
     * @throws ServletException
     * @throws IOException
     */
    @RequestMapping(value = "/form/getUserGroups", method = { RequestMethod.POST, RequestMethod.GET }, headers = "Accept=application/json; charset=utf-8")
    @ResponseBody
    public ResponseDTO getUserGroups(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException
    {
        //        String username = (String) request.getAttribute(Constants.HTTP_REQUEST_USERNAME);
        ResponseDTO result = new ResponseDTO();

        try
        {

            // Get the user groups to allow the user to filter which users show
            // up in the combobox
            List<ComboboxModelDTO> groups = new QueryCustomForm().getGroups();
            result.setSuccess(true).setRecords(groups);
        }
        catch (Exception e)
        {
            Log.log.error("error in getting the user groups", e);
            result.setSuccess(false).setMessage("Error getting user groups");
        }

        return result;
    }

    /**
     * get the list of runbooks - viewer
     *
     * @param request
     * @param response
     * @return
     * @throws ServletException
     * @throws IOException
     */
    @RequestMapping(value = "/form/getUsers", method = { RequestMethod.POST, RequestMethod.GET }, headers = "Accept=application/json; charset=utf-8")
    @ResponseBody
    public ResponseDTO getUsers(@RequestBody UserPickerQueryModelDTO query, HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException
    {
        //        String username = (String) request.getAttribute(Constants.HTTP_REQUEST_USERNAME);
        ResponseDTO result = new ResponseDTO();

        try
        {
            // Get the user groups to allow the user to filter which users show
            // up in the combobox
            // get all the users that belong to these groups and also add the
            // groups to the listing
            List<ComboboxModelDTO> users = new QueryCustomForm().getUsersForGroupsAndTeams(query);
            result.setSuccess(true).setRecords(users).setTotal(users.size());
        }
        catch (Exception e)
        {
            Log.log.error("error in getting the user groups", e);
            result.setSuccess(false).setMessage("Error getting users");
        }

        return result;
    }

    /**
     * get the list of runbooks
     *
     * @param request
     * @param response
     * @return
     * @throws ServletException
     * @throws IOException
     */
    @SuppressWarnings("rawtypes")
    @RequestMapping(value = "/form/getTableData", method = { RequestMethod.POST, RequestMethod.GET }, headers = "Accept=application/json; charset=utf-8")
    @ResponseBody
    public ResponseDTO getTableData(@RequestParam String tableName, HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException
    {
        //        String username = (String) request.getAttribute(Constants.HTTP_REQUEST_USERNAME);
        ResponseDTO result = new ResponseDTO();

        try
        {
            // Get the table data to be able to compare on the client what the
            // changes will be or if its a new table
            CustomTableDTO table = new QueryCustomForm().getCustomTable(tableName);
            result.setSuccess(true).setData(table);
        }
        catch (Exception e)
        {
            Log.log.error("error in getting the user groups", e);
            result.setSuccess(false).setMessage("Error getting custom table:" + tableName);
        }

        return result;
    }

    /**
     * get the list of runbooks
     *
     * @param request
     * @param response
     * @return
     * @throws ServletException
     * @throws IOException
     */
    @RequestMapping(value = "/form/getRoles", method = { RequestMethod.POST, RequestMethod.GET }, headers = "Accept=application/json; charset=utf-8")
    @ResponseBody
    public ResponseDTO getRoles(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException
    {
        //        String username = (String) request.getAttribute(Constants.HTTP_REQUEST_USERNAME);
        ResponseDTO result = new ResponseDTO();

        try
        {
            List<ComboboxModelDTO> roles = new QueryCustomForm().getRoles();
            result.setSuccess(true).setRecords(roles);
        }
        catch (Exception e)
        {
            Log.log.error("error in getting the user groups", e);
            result.setSuccess(false).setMessage("Error getting user groups");
        }

        return result;
    }

    /**
     * get the list of wiki documents in the system
     *
     * @param request
     * @param response
     * @return
     * @throws ServletException
     * @throws IOException
     */
    @RequestMapping(value = "/form/getwikinames", method = { RequestMethod.POST, RequestMethod.GET }, headers = "Accept=application/json; charset=utf-8")
    @ResponseBody
    public ResponseDTO getWikiNames(WebRequest webRequest, HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException
    {
        //        String username = (String) request.getAttribute(Constants.HTTP_REQUEST_USERNAME);
        String wikiName = webRequest.getParameter("wikiName");
        ResponseDTO result = new ResponseDTO();

        try
        {
            List<ComboboxModelDTO> roles = new QueryCustomForm().getListOfAllWikis(wikiName);
            result.setSuccess(true).setRecords(roles);
        }
        catch (Exception e)
        {
            Log.log.error("error in getting the wiki names", e);
            result.setSuccess(false).setMessage("Error getting wiki names");
        }

        return result;
    }

    /**
     * get the list of form names in the system
     *
     * @param request
     * @param response
     * @return
     * @throws ServletException
     * @throws IOException
     */
    @RequestMapping(value = "/form/getformnames", method = { RequestMethod.POST, RequestMethod.GET }, headers = "Accept=application/json; charset=utf-8")
    @ResponseBody
    public ResponseDTO<ComboboxModelDTO> getFormNames(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException
    {
        //        String username = (String) request.getAttribute(Constants.HTTP_REQUEST_USERNAME);
        ResponseDTO<ComboboxModelDTO> result = new ResponseDTO<ComboboxModelDTO>();

        try
        {
            List<ComboboxModelDTO> customFormNamesWithIds = new QueryCustomForm().getListOfAllCustomFormNamesWithIds();
            result.setSuccess(true).setRecords(customFormNamesWithIds);
        }
        catch (Exception e)
        {
            Log.log.error("error in getting the form names", e);
            result.setSuccess(false).setMessage("Error getting form names");
        }

        return result;
    }

    /**
     * get the list of teams - for FB
     *
     * @param request
     * @param response
     * @return
     * @throws ServletException
     * @throws IOException
     */
    @RequestMapping(value = "/form/getAllTeams", method = { RequestMethod.POST, RequestMethod.GET }, headers = "Accept=application/json; charset=utf-8")
    @ResponseBody
    public ResponseDTO getAllTeams(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException
    {
        //        String username = (String) request.getAttribute(Constants.HTTP_REQUEST_USERNAME);
        ResponseDTO result = new ResponseDTO();

        try
        {
            // Get the Teams instead of the groups and filter based on the teams
            // array if there are any provided (otherwise return all)
            List<ComboboxModelDTO> teams = new QueryCustomForm().getTeams();
            result.setSuccess(true).setRecords(teams);
        }
        catch (Exception e)
        {
            Log.log.error("error in getting the user groups", e);
            result.setSuccess(false).setMessage("Error getting user groups");
        }

        return result;
    }

    /**
     * for viewer
     *
     * @param request
     * @param response
     * @return
     * @throws ServletException
     * @throws IOException
     */
    @RequestMapping(value = "/form/getTeams", method = { RequestMethod.POST, RequestMethod.GET }, headers = "Accept=application/json; charset=utf-8")
    @ResponseBody
    public ResponseDTO getTeams(@RequestParam String teamPickerTeamsOfTeams, @RequestParam boolean recurseTeamsOfTeam, HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException
    {
        //        String username = (String) request.getAttribute(Constants.HTTP_REQUEST_USERNAME);
        ResponseDTO result = new ResponseDTO();

        try
        {
            // Get the Teams instead of the groups and filter based on the teams
            // array if there are any provided (otherwise return all
            List<ComboboxModelDTO> groups = new QueryCustomForm().getTeams(teamPickerTeamsOfTeams, recurseTeamsOfTeam);
            result.setSuccess(true).setRecords(groups);
        }
        catch (Exception e)
        {
            Log.log.error("error in getting the user groups", e);
            result.setSuccess(false).setMessage("Error getting user groups");
        }

        return result;
    }

    /**
     * get the default list of access rights to pre-populate a form with
     *
     * @param request
     * @param response
     * @return
     * @throws ServletException
     * @throws IOException
     */
    @RequestMapping(value = "/form/getDefaultAccessRights", method = { RequestMethod.POST, RequestMethod.GET }, headers = "Accept=application/json; charset=utf-8")
    @ResponseBody
    public ResponseDTO getDefaultAccessRights(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException
    {
        //        String username = (String) request.getAttribute(Constants.HTTP_REQUEST_USERNAME);
        ResponseDTO result = new ResponseDTO();

        try
        {
            // Get the default access rights that should be automatically added
            // to a new form
            RolesModelDTO defaultRights = new QueryCustomForm().getDefaultAccessRightsForCustomForms();
            result.setSuccess(true).setData(defaultRights);
        }
        catch (Exception e)
        {
            Log.log.error("Error getting default access rights", e);
            result.setSuccess(false).setMessage("Error getting default access rights");
        }

        return result;
    }

    /**
     * get the link information for the wiki document
     *
     * @param request
     * @param response
     * @return
     * @throws ServletException
     * @throws IOException
     */
    @RequestMapping(value = "/form/getLink", method = { RequestMethod.POST, RequestMethod.GET }, headers = "Accept=application/json; charset=utf-8")
    @ResponseBody
    public ResponseDTO getLink(@RequestParam String wikiLink, @RequestParam String target, HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException
    {
        //        String username = (String) request.getAttribute(Constants.HTTP_REQUEST_USERNAME);
        ResponseDTO result = new ResponseDTO();

        try
        {
            // Lookup the link information for the wiki document
            // if the name is an empty string, then we need to send back the
            // name of the wiki document to display i.e Runbook.WebHome should
            // return Web Home with the link to the Web Home document
            result.setSuccess(true).setData(new QueryCustomForm().getRenderedWikiLink(wikiLink, target));
        }
        catch (Exception e)
        {
            Log.log.error("Error getting link data", e);
            result.setSuccess(false).setMessage("Error getting link data");
        }

        return result;
    }

    @RequestMapping(value = "/form/getReferenceTables", method = { RequestMethod.POST, RequestMethod.GET }, headers = "Accept=application/json; charset=utf-8")
    @ResponseBody
    public ResponseDTO getReferenceTables(@RequestParam String tableName, HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException
    {
        //        String username = (String) request.getAttribute(Constants.HTTP_REQUEST_USERNAME);
        ResponseDTO result = new ResponseDTO();

        try
        {
            //Get the list of tables that have a reference to the provided table
            List<ComboboxModelDTO> roles = new QueryCustomForm().getReferenceTables(tableName);
            result.setSuccess(true).setRecords(roles);
        }
        catch (Exception e)
        {
            Log.log.error("error in getting the list of reference table for " + tableName, e);
            result.setSuccess(false).setMessage("error in getting the list of reference table for " + tableName);
        }

        return result;
    }

    @RequestMapping(value = "/formbuilder/deleteForm", method = { RequestMethod.POST, RequestMethod.GET }, headers = "Accept=application/json; charset=utf-8")
    @ResponseBody
    public ResponseDTO deleteForm(@RequestParam String formId, HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException
    {
        String username = (String) request.getAttribute(Constants.HTTP_REQUEST_USERNAME);
        ResponseDTO result = new ResponseDTO();

        try
        {
            List<String> ids = new ArrayList<String>();
            ids.add(formId);

            ServiceHibernate.deleteMetaForm(ids, username);
            result.setSuccess(true);
        }
        catch (Throwable e)
        {
            Log.log.error("Error getting data", e);
            result.setSuccess(false).setMessage("Error getting data");
        }

        return result;
    }

    @RequestMapping(value = "/form/upload", method = { RequestMethod.POST })
    @ResponseBody
    public ResponseDTO upload(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException, FileUploadException
    {

        //        String username = (String) request.getAttribute(Constants.HTTP_REQUEST_USERNAME);
        ResponseDTO result = new ResponseDTO();

        try
        {
            UploadFileDTO upload = new QueryCustomForm().upload(request, response);
            result.setSuccess(true).setData(upload);
        }
        catch (Throwable e)
        {
            Log.log.error("Error uploading the file", e);
            result.setSuccess(false).setMessage("Error uploading the file");
        }

        /*

        // Create a factory for disk-based file items
        FileItemFactory factory = new DiskFileItemFactory();

        // Create a new file upload handler
        ServletFileUpload upload = new ServletFileUpload(factory);

        // Parse the request
        List<FileItem> items = upload.parseRequest(request);
        if (items == null || items.size() == 0)
        {
            // no items uploaded, possibly throw exception here
        }

        // Process the uploaded items
        Iterator<FileItem> iter = items.iterator();
        while (iter.hasNext())
        {
            FileItem item = iter.next();
            String name = item.getFieldName();

            if (item.isFormField())
            {
                int i = 0;
                // recordId - NOT REQIURED

                // tableName
                //listOfColumns - comma separated col names
                //listOfValues - comma seperated col values
            }
            else
            {
                processUploadedFile(item);
            }
        }
        UploadFileDTO fileUpload = new UploadFileDTO();
        fileUpload.setSys_id("Something");
        */
        return result;
    }

    //    private void processUploadedFile(FileItem item) throws IOException
    //    {
    //        // few attributes for debuging purpose...may be removed/commented out
    //        String fieldName = item.getFieldName();
    //        String fileName = item.getName();// C:\project\resolve3\dist\tomcat\webapps\resolve\jsp\attach.jsp
    //        fileName = fileName.substring(fileName.lastIndexOf("\\") + 1);
    //        String contentType = item.getContentType();// text/plain
    //        boolean isInMemory = item.isInMemory();
    //
    //        long sizeInBytes = item.getSize();
    //        InputStream uploadedStream = item.getInputStream();
    //        byte[] data = new byte[(int) sizeInBytes];
    //        uploadedStream.read(data);
    //
    //        // Write file to the filesystem or whatever else
    //    }

    @RequestMapping(value = "/form/deleteFile", method = { RequestMethod.POST })
    @ResponseBody
    public ResponseDTO deleteFile(@RequestParam String tableName, @RequestParam String[] ids, HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException, FileUploadException
    {
        //        String username = (String) request.getAttribute(Constants.HTTP_REQUEST_USERNAME);
        ResponseDTO result = new ResponseDTO();
        String dbType = ((Main) MainBase.main).getConfigSQL().getDbtype().toUpperCase();
        String whereClause = " sys_id IN (" + SQLUtils.prepareQueryString(ids, dbType) + ")";

        QueryDTO query = new QueryDTO();
        query.setTableName(tableName);
        query.setWhereClause(whereClause);

        try
        {
            new QueryCustomForm().deleteRecordData(query);
            result.setSuccess(true).setMessage("Successfully deleted records");
        }
        catch (Exception e)
        {
            Log.log.error("Error deleting data", e);
            result.setSuccess(false).setMessage("Error deleting data");
        }

        return result;
    }

    @RequestMapping(value = "/form/downloadFile", method = { RequestMethod.POST, RequestMethod.GET })
    @ResponseBody
    public void downloadFile(@RequestParam String sys_id, @RequestParam String tableName, HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException, FileUploadException
    {
        //        String username = (String) request.getAttribute(Constants.HTTP_REQUEST_USERNAME);
        ResponseDTO result = new ResponseDTO();

        try
        {
            new QueryCustomForm().downloadFile(sys_id, tableName, request, response);
            result.setSuccess(true).setMessage("Successfully downloaded file");
        }
        catch (Exception e)
        {
            Log.log.error("Error downloading the file for table:" + tableName, e);
            result.setSuccess(false).setMessage("Error downloading the file for table:" + tableName);
        }

        //        try
        //        {
        //            //get the file as an input stream
        //            // get your file as InputStream
        //            InputStream is = null;
        //            // copy it to response's OutputStream
        //            IOUtils.copy(is, response.getOutputStream());
        //            response.flushBuffer();
        //        }
        //        catch (IOException ex)
        //        {
        //            Log.log.error("Error writing file to output stream. Filename was '" + sys_id + "'");
        //            throw new RuntimeException("IOError writing file to output stream");
        //        }
    }

    @RequestMapping(value = "/form/getdefaultdata", method = { RequestMethod.POST, RequestMethod.GET }, headers = "Accept=application/json; charset=utf-8")
    @ResponseBody
    public ResponseDTO getDefaultData(@RequestParam String view, @RequestParam String formsysid, HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException
    {
        String username = (String) request.getAttribute(Constants.HTTP_REQUEST_USERNAME);
        ResponseDTO result = new ResponseDTO();

        try
        {
            List<DefaultDataDTO> defaultValues = ServiceHibernate.getCustomFormDefaultValues(formsysid, view, null, username, RightTypeEnum.admin, null);
            result.setSuccess(true).setData(defaultValues);
        }
        catch (Exception e)
        {
            Log.log.error("error in getting the form info: " + view + " - sysId:" + formsysid, e);
            result.setSuccess(false).setMessage("Error for the form :" + view + ".");
        }
        return result;
    }

    @RequestMapping(value = "/form/list", method = { RequestMethod.GET, RequestMethod.POST }, headers = "Accept=application/json; charset=utf-8")
    @ResponseBody
    public ResponseDTO<CustomFormDTO> list(@ModelAttribute QueryDTO query, HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException
    {
        ResponseDTO<CustomFormDTO> result = new ResponseDTO<CustomFormDTO>();
        String username = (String) request.getAttribute(Constants.HTTP_REQUEST_USERNAME);

        try
        {
            query.setModelName("MetaFormView");

            List<CustomFormDTO> data = ServiceHibernate.getCustomFormsForUI(query, username);
            int total = ServiceHibernate.getTotalHqlCount(query);

            result.setSuccess(true).setRecords(data);
            result.setTotal(total);
        }
        catch (Exception e)
        {
            Log.log.error("Error retrieving Custom Forms", e);
            result.setSuccess(false).setMessage("Error retrieving forms");
        }

        return result;
    }

    @RequestMapping(value = "/form/delete", method = { RequestMethod.POST }, headers = "Accept=application/json; charset=utf-8")
    @ResponseBody
    public ResponseDTO delete(@RequestBody String[] ids, HttpServletRequest request, HttpServletResponse response) throws Throwable
    {
        ResponseDTO result = new ResponseDTO();
        String username = (String) request.getAttribute(Constants.HTTP_REQUEST_USERNAME);

        try
        {
            List<String> listOfIds = Arrays.asList(ids);
            ServiceHibernate.deleteMetaForm(listOfIds, username);
            result.setSuccess(true);
        }
        catch (Exception e)
        {
            Log.log.error("Error deleting Custom Form", e);
            result.setSuccess(false).setMessage("Error deleting form");
        }

        return result;
    }

    @RequestMapping(value = "/form/updateIndexMapping", method = { RequestMethod.GET, RequestMethod.POST }, headers = "Accept=application/json; charset=utf-8")
    @ResponseBody
    public ResponseDTO<String> updateIndexMapping(HttpServletRequest request, HttpServletResponse response) {
    	  ResponseDTO<String> result = new ResponseDTO<String>();
          String username = (String) request.getAttribute(Constants.HTTP_REQUEST_USERNAME);
          try {
        	  CustomFormIndexAPI.updateIndexMapping(username);
        	  result.setSuccess(true);
          }
          catch (Exception e)
          {
              Log.log.error("Error updating index mappings for custom forms", e);
              result.setSuccess(false).setMessage("Error updating index mapping for custom forms. See log for more info.");
          }
          
          return result;
    }
    
    
    @RequestMapping(value = "/form/index", method = { RequestMethod.GET, RequestMethod.POST }, headers = "Accept=application/json; charset=utf-8")
    @ResponseBody
    public ResponseDTO<String> index(@RequestParam(value = "all",defaultValue="false") Boolean all,@RequestParam(value = "ids",defaultValue = "[]") List<String> ids, HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException
    {
        ResponseDTO<String> result = new ResponseDTO<String>();
        String username = (String) request.getAttribute(Constants.HTTP_REQUEST_USERNAME);
        try
        {
            if(!all)
                CustomFormIndexAPI.indexCustomForms(ids, username);
            else
                MIndex.indexAllCustomForms(new HashMap<String, Object>());
            result.setSuccess(true);
        }
        catch (Exception e)
        {
            Log.log.error("Error indexing custom forms", e);
            result.setSuccess(false).setMessage("Error indexing custom forms. See log for more info.");
        }
        
        return result;
    }
    
}
