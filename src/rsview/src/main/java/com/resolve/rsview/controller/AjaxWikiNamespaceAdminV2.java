package com.resolve.rsview.controller;

import java.io.IOException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Arrays;
import java.util.Date;
import java.util.HashSet;
import java.util.List;
import java.util.Locale;
import java.util.Set;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.lang.ArrayUtils;
import org.apache.commons.lang.BooleanUtils;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.resolve.services.ServiceWiki;
import com.resolve.services.hibernate.util.ResolveNamespaceUtil;
import com.resolve.services.hibernate.util.WikiUtils;
import com.resolve.services.hibernate.vo.AccessRightsVO;
import com.resolve.services.hibernate.vo.NamespaceVO;
import com.resolve.services.hibernate.vo.WikidocResolutionRatingVO;
import com.resolve.services.hibernate.wiki.DeleteHelper;
import com.resolve.services.hibernate.wiki.RatingUtil;
import com.resolve.services.hibernate.wiki.WikiStatusEnum;
import com.resolve.services.hibernate.wiki.WikiStatusUtil;
import com.resolve.services.util.UserUtils;
import com.resolve.services.vo.QueryDTO;
import com.resolve.services.vo.ResponseDTO;
import com.resolve.util.Constants;
import com.resolve.util.Log;
import com.resolve.util.StringUtils;

//TODO uncomment when ready to do a transition to the new namespaces 
//@Controller
//@RequestMapping("/nsadmin")
public class AjaxWikiNamespaceAdminV2 extends GenericController {
	
	private static final String ERROR_SETTING_EXPIRY = "Error setting the expiry on namespace(s)";
	private static final String ERROR_UPDATING_REVIEW = "Error updating the review flag for namespace(s).";
	private static final String ERROR_RESETTING_STATS = "Error during reset of stats for namespace(s).";
	private static final String ERROR_PURGING_WIKI = "Error purging wiki(s) indices from given namespace(s).";
	private static final String ERROR_INDEXING_WIKI = "Error indexing wiki(s) from given namespace(s).";
	private static final String ERROR_REMOVING_TAG = "Error removing tag(s) from namespace(s).";
	private static final String ERROR_ADDING_TAG = "Error adding tag(s) to the namespace(s).";
	private static final String ERROR_COPYING_WIKI = "Error renaming/copying wiki(s).";
	private static final String ERROR_UPDATING_WEIGHTS = "Error updating the adding weights to namespace(s).";
	private static final String ERROR_UPDATING_RATING = "Error updating the rating for namespace(s).";
	private static final String ERROR_COMMITTING_NAMESPACE = "Error during commit of namespace(s).";
	private static final String ERROR_PURGING_NAMESPACES = "Error purging namespace(s).";
	private static final String ERROR_UPDATING_DELETED_FLAG = "Error updating the namespaces deleted flag";
	private static final String ERROR_UPDATING_HIDDEN_FLAG = "Error updating the hidden flag for namespace(s).";
	private static final String ERROR_UPDATING_LOCK_FLAG = "Error updating the lock flag for namespace(s).";
	private static final String ERROR_UPDATING_ACTIVE_FLAG = "Error updating the active flag for namespace(s).";
	private static final String ERROR_SETTING_DEFAULT_ACCESS_RIGHTS = "Error setting default access rights to namespace(s).";
	private static final String ERROR_ADDING_ACCESS_RIGHTS = "Error adding access rights to namespace(s).";
	private static final String ERROR_GETTING_ACCESS_RIGHTS = "Error during getting access rights for namespace(s).";
	private static final String ERROR_RETRIEVING_NAMESPACE_LIST = "Error retrieving namespace list.";

	private static final DateFormat DATE_FORMAT = new SimpleDateFormat("yyyy-MM-dd", Locale.ENGLISH);

	@RequestMapping(value = {"/v2", "/list"}, method = RequestMethod.GET)
    @ResponseBody
	public ResponseDTO<NamespaceVO> getAllWikiNamespaces(@ModelAttribute QueryDTO query, HttpServletRequest request,
			HttpServletResponse response) throws ServletException, IOException {
		ResponseDTO<NamespaceVO> result = new ResponseDTO<>();

		try {
			List<NamespaceVO> ns = ResolveNamespaceUtil.convertToList(ResolveNamespaceUtil.getAllWikiNamespaces());
			result.setRecords(ns).setTotal(ns.size()).setSuccess(true);
		} catch (Exception e) {
			Log.log.error(ERROR_RETRIEVING_NAMESPACE_LIST, e);
			result.setSuccess(false).setMessage(ERROR_RETRIEVING_NAMESPACE_LIST);
		}

		return result;
	}

    @RequestMapping(value = {"/accessRights/v2", "/accessRights"}, method = RequestMethod.POST)
    @ResponseBody
	public ResponseDTO<AccessRightsVO> getNamespaceAccessRights(
			@RequestParam("namespaces") String[] namespaces,
			@RequestParam("defaultRights") Boolean defaultRights,
			HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

    	ResponseDTO<AccessRightsVO> result = new ResponseDTO<AccessRightsVO>();

		String username = (String) request.getAttribute(Constants.HTTP_REQUEST_USERNAME);

		try {
			if (ArrayUtils.isNotEmpty(namespaces)) {

				Set<String> ns = new HashSet<String>(Arrays.asList(namespaces));
				List<AccessRightsVO> rights = null;
				if (defaultRights) {
					rights = ServiceWiki.getDefaultAccessRightsForNamespaces(ns, username);
				} else {
					rights = ResolveNamespaceUtil.getAccessRights(ns);
				}

				result.setData(AjaxWikiAdmin.mergeAccessRights(rights)).setSuccess(true);
			}

		} catch (Exception e) {
			Log.log.error(ERROR_GETTING_ACCESS_RIGHTS, e);
			result.setSuccess(false).setMessage(ERROR_GETTING_ACCESS_RIGHTS);
		}

		return result;
	}

    @RequestMapping(value = {"/setAccessRights/v2", "/setAccessRights"}, method = RequestMethod.POST)
    @ResponseBody
    public ResponseDTO<String> setAccessRights(
                    @RequestParam("namespaces") String[] namespaces,
                    @RequestParam("uexecuteAccess") String uexecuteAccess,
                    @RequestParam("uadminAccess") String uadminAccess,
                    @RequestParam("ureadAccess") String ureadAccess,
                    @RequestParam("uwriteAccess") String uwriteAccess,
                    @RequestParam("defaultRights") boolean defaultRights,
			HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		ResponseDTO<String> result = new ResponseDTO<>();
		String username = (String) request.getAttribute(Constants.HTTP_REQUEST_USERNAME);
		try {
			if (namespaces != null && namespaces.length > 0) {
				AccessRightsVO accessRights = new AccessRightsVO();
				accessRights.setUAdminAccess(uadminAccess);
				accessRights.setUReadAccess(ureadAccess);
				accessRights.setUWriteAccess(uwriteAccess);
				accessRights.setUExecuteAccess(uexecuteAccess);

				Set<String> ns = new HashSet<String>(Arrays.asList(namespaces));
				ServiceWiki.setRolesForNamespaces(ns, accessRights, defaultRights, username, true);

				result.setSuccess(true);
			}
		} catch (Exception e) {
			Log.log.error(ERROR_ADDING_ACCESS_RIGHTS, e);
			result.setSuccess(false).setMessage(ERROR_ADDING_ACCESS_RIGHTS);
		}

		return result;
	}
    
    @RequestMapping(value = {"/setDefaultAccessRights", "/defaultAccessRights"}, method = RequestMethod.POST)
    @ResponseBody
    public ResponseDTO<String> setDefaultAccessRights(
                    @RequestParam("namespaces") String[] namespaces,
                    @RequestParam("uexecuteAccess") String uexecuteAccess,
                    @RequestParam("uadminAccess") String uadminAccess,
                    @RequestParam("ureadAccess") String ureadAccess,
                    @RequestParam("uwriteAccess") String uwriteAccess,
			HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		ResponseDTO<String> result = new ResponseDTO<>();
		String username = (String) request.getAttribute(Constants.HTTP_REQUEST_USERNAME);
		try {
			if (namespaces != null && namespaces.length > 0) {
				if (!uadminAccess.contains(UserUtils.ADMIN)) {
					uadminAccess = StringUtils.isBlank(uadminAccess) ? UserUtils.ADMIN : String.join(",", UserUtils.ADMIN, uadminAccess);
				}
				if (!uexecuteAccess.contains(UserUtils.ADMIN)) {
					uexecuteAccess = StringUtils.isBlank(uexecuteAccess) ? UserUtils.ADMIN : String.join(",", UserUtils.ADMIN, uexecuteAccess);
				}
				if (!uwriteAccess.contains(UserUtils.ADMIN)) {
					uwriteAccess = StringUtils.isBlank(uwriteAccess) ? UserUtils.ADMIN : String.join(",", UserUtils.ADMIN, uwriteAccess);
				}
				if (!ureadAccess.contains(UserUtils.ADMIN)) {
					ureadAccess = StringUtils.isBlank(ureadAccess) ? UserUtils.ADMIN : String.join(",", UserUtils.ADMIN, ureadAccess);
				}
				AccessRightsVO accessRights = new AccessRightsVO();
				accessRights.setUAdminAccess(uadminAccess);
				accessRights.setUReadAccess(ureadAccess);
				accessRights.setUWriteAccess(uwriteAccess);
				accessRights.setUExecuteAccess(uexecuteAccess);

				Set<String> ns = new HashSet<String>(Arrays.asList(namespaces));
				ServiceWiki.setNamespaceDefaultRoles(ns, accessRights, username, true);

				result.setSuccess(true);
			}

		} catch (Exception e) {
			Log.log.error(ERROR_SETTING_DEFAULT_ACCESS_RIGHTS, e);
			result.setSuccess(false).setMessage(ERROR_SETTING_DEFAULT_ACCESS_RIGHTS);
		}

		return result;
	}
    
    @RequestMapping(value = {"/setActivated", "/activate"}, method = RequestMethod.POST)
    @ResponseBody
	public ResponseDTO<String> setActivated(@RequestParam("namespaces") String[] namespaces, @RequestParam("on") Boolean on,
			HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

    	String username = (String) request.getAttribute(Constants.HTTP_REQUEST_USERNAME);
		return setFlag(namespaces, WikiStatusEnum.ACTIVE, on, username);
	}
    
    @RequestMapping(value = {"/setLocked", "/lock"}, method = RequestMethod.POST)
    @ResponseBody
	public ResponseDTO<String> setLocked(@RequestParam("namespaces") String[] namespaces, @RequestParam("on") Boolean on,
			HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

    	String username = (String) request.getAttribute(Constants.HTTP_REQUEST_USERNAME);
		return setFlag(namespaces, WikiStatusEnum.LOCKED, on, username);
	}
    
    @RequestMapping(value = {"/setHidden", "/hide"}, method = RequestMethod.POST)
    @ResponseBody
	public ResponseDTO<String> setHidden(@RequestParam("namespaces") String[] namespaces,
			@RequestParam("on") Boolean on, HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {

    	String username = (String) request.getAttribute(Constants.HTTP_REQUEST_USERNAME);
		return setFlag(namespaces, WikiStatusEnum.HIDDEN, on, username);
	}

    @RequestMapping(value = { "/setDeleted", "/delete" }, method = RequestMethod.POST)
    @ResponseBody
	public ResponseDTO<String> setDeleted(@RequestParam("namespaces") String[] namespaces,
			@RequestParam("on") Boolean on, HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {

    	String username = (String) request.getAttribute(Constants.HTTP_REQUEST_USERNAME);
		return setFlag(namespaces, WikiStatusEnum.DELETED, on, username);
	}

    @RequestMapping(value = "/purge", method = RequestMethod.POST)
    @ResponseBody
	public ResponseDTO<String> purgeWikis(@RequestParam("namespaces") String[] namespaces, @RequestParam("all") Boolean all,
			HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException    {
		ResponseDTO<String> result = new ResponseDTO<>();
		String username = (String) request.getAttribute(Constants.HTTP_REQUEST_USERNAME);

		try {
			if (namespaces != null && namespaces.length > 0) {
				Set<String> namespaceNames = new HashSet<String>(Arrays.asList(namespaces));
				
				for (String namespace : namespaceNames) {
					Set<String> sysIds = ResolveNamespaceUtil.findAllDocumentsSysIdsForNamespace(namespace);
					DeleteHelper.deleteWikiDocuments(sysIds, username);
				}
			} else {
				if (all) {
					ServiceWiki.deleteAllWikiDocuments(username);
				}
			}

			result.setSuccess(true);
		} catch (Exception e) {
			Log.log.error(ERROR_PURGING_NAMESPACES, e);
			result.setSuccess(false).setMessage(ERROR_PURGING_NAMESPACES);
		}

		return result;
	}

	@RequestMapping(value = "/commit", method = RequestMethod.POST)
	@ResponseBody
	public ResponseDTO<String> commitRevision(@RequestParam("namespaces") String[] namespaces,
			@RequestParam("comment") String comment, HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		ResponseDTO<String> result = new ResponseDTO<>();
		String username = (String) request.getAttribute(Constants.HTTP_REQUEST_USERNAME);

		try {
			if (namespaces != null && namespaces.length > 0) {
				Set<String> ns = new HashSet<String>(Arrays.asList(namespaces));

				for (String namespace : ns) {
					Set<String> sysIds = ResolveNamespaceUtil.findAllDocumentsSysIdsForNamespace(namespace);
                    WikiUtils.archiveDocuments(sysIds, comment, username);
				}

				result.setSuccess(true);
			}
		} catch (Exception e) {
			Log.log.error(ERROR_COMMITTING_NAMESPACE, e);
			result.setSuccess(false).setMessage(ERROR_COMMITTING_NAMESPACE);
		}

		return result;
	}

    @RequestMapping(value = "/rate", method = RequestMethod.POST)
    @ResponseBody
    public ResponseDTO<String> rate(
                    @RequestParam("namespaces") String[] namespaces,
                    @RequestParam("u1StarCount") Integer u1StarCount,
                    @RequestParam("u2StarCount") Integer u2StarCount,
                    @RequestParam("u3StarCount") Integer u3StarCount,
                    @RequestParam("u4StarCount") Integer u4StarCount,
                    @RequestParam("u5StarCount") Integer u5StarCount,
			HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		ResponseDTO<String> result = new ResponseDTO<>();
		String username = (String) request.getAttribute(Constants.HTTP_REQUEST_USERNAME);
		try {
			if (namespaces != null && namespaces.length > 0) {
				WikidocResolutionRatingVO rating = new WikidocResolutionRatingVO();
				rating.setU1StarCount(new Long(u1StarCount));
				rating.setU2StarCount(new Long(u2StarCount));
				rating.setU3StarCount(new Long(u3StarCount));
				rating.setU4StarCount(new Long(u4StarCount));
				rating.setU5StarCount(new Long(u5StarCount));

				Set<String> namespacesSet = new HashSet<String>(Arrays.asList(namespaces));

				for (String ns : namespacesSet) {
					Set<String> sysIds = ResolveNamespaceUtil.findAllDocumentsSysIdsForNamespace(ns);
					RatingUtil.updateResolutionRatingCount(sysIds, rating, username);
				}
				
				result.setSuccess(true);
			}
		} catch (Exception e) {
			Log.log.error(ERROR_UPDATING_RATING, e);
			result.setSuccess(false).setMessage(ERROR_UPDATING_RATING);
		}

		return result;
	}

    @RequestMapping(value = { "/addSearchWeight", "/searchWeight" }, method = RequestMethod.POST)
    @ResponseBody
    public ResponseDTO<String> addSearchWeight(
                    @RequestParam("namespaces") String[] namespaces,
                    @RequestParam("weight") Integer weight,
			HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		ResponseDTO<String> result = new ResponseDTO<>();
		String username = (String) request.getAttribute(Constants.HTTP_REQUEST_USERNAME);
		try {
			if (namespaces != null && namespaces.length > 0) {
				Set<String> namespacesSet = new HashSet<String>(Arrays.asList(namespaces));
				
				for (String ns : namespacesSet) {
					Set<String> sysIds = ResolveNamespaceUtil.findAllDocumentsSysIdsForNamespace(ns);
					RatingUtil.updateWikidocsWeight(sysIds, weight, username);
				}

				result.setSuccess(true);
			}
		} catch (Exception e) {
			Log.log.error(ERROR_UPDATING_WEIGHTS, e);
			result.setSuccess(false).setMessage(ERROR_UPDATING_WEIGHTS);
		}

		return result;
	}

    @RequestMapping(value = "/moveRenameCopy", method = RequestMethod.POST)
    @ResponseBody
    public ResponseDTO<String> moveRenameCopy(
                    @RequestParam("action") String action,
                    @RequestParam("namespaces") String[] namespaces,
                    @RequestParam("namespace") String namespace,
                    @RequestParam("choice") String choice,
			HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		ResponseDTO<String> result = new ResponseDTO<>();
		String username = (String) request.getAttribute(Constants.HTTP_REQUEST_USERNAME);

		try {
			if (namespaces != null && namespaces.length > 0 && StringUtils.isNotEmpty(action)
					&& StringUtils.isNotEmpty(namespace)) {
				if ("copy".equalsIgnoreCase(action)) {
					if ("overwrite".equalsIgnoreCase(choice)) {
						ServiceWiki.copyDocumentsForNamepaces(Arrays.asList(namespaces), namespace, true, username, true);
					} else if ("skip".equalsIgnoreCase(choice)) {
						ServiceWiki.copyDocumentsForNamepaces(Arrays.asList(namespaces), namespace, false, username, true);
					} else if ("update".equalsIgnoreCase(choice)) {
						ServiceWiki.replaceDocumentsForNamepaces(Arrays.asList(namespaces), namespace, username, true);
					}
				} else {
					ServiceWiki.moveRenameDocumentsForNamepaces(Arrays.asList(namespaces), namespace,
							BooleanUtils.toBoolean(choice), username, true);
				}

				result.setSuccess(true);
			}
		} catch (Exception e) {
			Log.log.error(ERROR_COPYING_WIKI, e);
			result.setSuccess(false).setMessage(ERROR_COPYING_WIKI);
		}

		return result;
	}

    @RequestMapping(value = "/addTags", method = RequestMethod.POST)
    @ResponseBody
	public ResponseDTO<String> addTags(@RequestParam("namespaces") String[] namespaces,
			@RequestParam("tagIds") String[] tagIds, HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {

    	ResponseDTO<String> result = new ResponseDTO<>();
		String username = (String) request.getAttribute(Constants.HTTP_REQUEST_USERNAME);

		try {
			if (ArrayUtils.isNotEmpty(namespaces) && ArrayUtils.isNotEmpty(tagIds)) {
				Set<String> ns = new HashSet<String>(Arrays.asList(namespaces));
				Set<String> tagSysIds = new HashSet<String>(Arrays.asList(tagIds));

				ServiceWiki.addTagsToWikiNamespaces(ns, tagSysIds, username, true);

				result.setSuccess(true);
			}
		} catch (Exception e) {
			Log.log.error(ERROR_ADDING_TAG, e);
			result.setSuccess(false).setMessage(ERROR_ADDING_TAG);
		}

		return result;
	}

    @RequestMapping(value = "/removeTags", method = RequestMethod.POST)
    @ResponseBody
	public ResponseDTO<String> removeTags(@RequestParam("namespaces") String[] namespaces,
			@RequestParam("tagIds") String[] tagIds, HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		ResponseDTO<String> result = new ResponseDTO<>();
		String username = (String) request.getAttribute(Constants.HTTP_REQUEST_USERNAME);

		try {
			if (ArrayUtils.isNotEmpty(namespaces) && ArrayUtils.isNotEmpty(tagIds)) {
				Set<String> ns = new HashSet<String>(Arrays.asList(namespaces));
				Set<String> tagSysIds = new HashSet<String>(Arrays.asList(tagIds));

				ServiceWiki.removeTagsFromWikiNamespaces(ns, tagSysIds, username, true);

				result.setSuccess(true);
			}
		} catch (Exception e) {
			Log.log.error(ERROR_REMOVING_TAG, e);
			result.setSuccess(false).setMessage(ERROR_REMOVING_TAG);
		}

		return result;
	}

    @RequestMapping(value = "/index", method = RequestMethod.POST)
    @ResponseBody
	public ResponseDTO<String> indexWikis(@RequestParam("namespaces") String[] namespaces,
			@RequestParam("all") Boolean all, HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		ResponseDTO<String> result = new ResponseDTO<>();
		String username = (String) request.getAttribute(Constants.HTTP_REQUEST_USERNAME);
		try {
			if (ArrayUtils.isNotEmpty(namespaces)) {
				Set<String> ns = new HashSet<String>(Arrays.asList(namespaces));
				ServiceWiki.indexWikiNamespaces(ns, true, username, true);
			} else {
				if (all) {
					ServiceWiki.indexAllWikiDocuments(true, username);
				}
			}

			result.setSuccess(true);
		} catch (Exception e) {
			Log.log.error(ERROR_INDEXING_WIKI, e);
			result.setSuccess(false).setMessage(ERROR_INDEXING_WIKI);
		}

		return result;
	}

    @RequestMapping(value = "/purgeIndex", method = RequestMethod.POST)
    @ResponseBody
	public ResponseDTO<String> purgeIndex(@RequestParam("namespaces") String[] namespaces,
			@RequestParam("all") Boolean all, HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		ResponseDTO<String> result = new ResponseDTO<>();
		String username = (String) request.getAttribute(Constants.HTTP_REQUEST_USERNAME);
		try {
			if (ArrayUtils.isNotEmpty(namespaces)) {
				Set<String> ns = new HashSet<String>(Arrays.asList(namespaces));
				ServiceWiki.purgeWikiNamespaces(ns, username, true);
			} else {
				if (all) {
					ServiceWiki.purgeAllWikiDocumentIndexes(username);
				}
			}
			result.setSuccess(true);

		} catch (Exception e) {
			Log.log.error(ERROR_PURGING_WIKI, e);
			result.setSuccess(false).setMessage(ERROR_PURGING_WIKI);
		}

		return result;
	}

    @RequestMapping(value = "/resetStats", method = RequestMethod.POST)
    @ResponseBody
	public ResponseDTO<String> resetStatistics(@RequestParam("namespaces") String[] namespaces,
			HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		ResponseDTO<String> result = new ResponseDTO<>();
		String username = (String) request.getAttribute(Constants.HTTP_REQUEST_USERNAME);

		try {
			if (ArrayUtils.isNotEmpty(namespaces)) {
				Set<String> docSysIds = new HashSet<String>(Arrays.asList(namespaces));
				ServiceWiki.resetNamespaces(docSysIds, username, true);

				result.setSuccess(true);
			}

		} catch (Exception e) {
			Log.log.error(ERROR_RESETTING_STATS, e);
			result.setSuccess(false).setMessage(ERROR_RESETTING_STATS);
		}

		return result;
	}

    @RequestMapping(value = "/setReviewed", method = RequestMethod.POST)
    @ResponseBody
	public ResponseDTO<String> setReviewed(@RequestParam("namespaces") String[] namespaces, HttpServletRequest request,
			HttpServletResponse response) throws ServletException, IOException {
		ResponseDTO<String> result = new ResponseDTO<>();
		String username = (String) request.getAttribute(Constants.HTTP_REQUEST_USERNAME);
		try {
			if (ArrayUtils.isNotEmpty(namespaces)) {
				Set<String> ns = new HashSet<String>(Arrays.asList(namespaces));
				ServiceWiki.updateLastReviewedDateForNamespace(ns, username, true);

				result.setSuccess(true);
			}
		} catch (Exception e) {
			Log.log.error(ERROR_UPDATING_REVIEW, e);
			result.setSuccess(false).setMessage(ERROR_UPDATING_REVIEW);
		}

		return result;
	}

    @RequestMapping(value = "/setExpiryDate", method = RequestMethod.POST)
    @ResponseBody
	public ResponseDTO<String> setExpiryDate(@RequestParam("namespaces") String[] namespaces,
			@RequestParam("date") String strDate, HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		ResponseDTO<String> result = new ResponseDTO<>();
		String username = (String) request.getAttribute(Constants.HTTP_REQUEST_USERNAME);
		try {
			if (ArrayUtils.isNotEmpty(namespaces)) {
				Date date = DATE_FORMAT.parse(strDate);

				Set<String> ns = new HashSet<String>(Arrays.asList(namespaces));
				ServiceWiki.updateExpirationDateForNamespace(ns, date, username, true);

				result.setSuccess(true);
			}
		} catch (Exception e) {
			Log.log.error(ERROR_SETTING_EXPIRY, e);
			result.setSuccess(false).setMessage(ERROR_SETTING_EXPIRY);
		}

        return result;
    }

    private static ResponseDTO<String> setFlag(String[] namespaces, WikiStatusEnum flag, boolean flagValue, String username) {
		ResponseDTO<String> result = new ResponseDTO<>();
		
		try {
			if (namespaces != null && namespaces.length > 0) {
				Set<String> namespacesSet = new HashSet<String>(Arrays.asList(namespaces));
				
				for (String namespace : namespacesSet) {
					Set<String> sysIds = ResolveNamespaceUtil.findAllDocumentsSysIdsForNamespace(namespace);
					WikiStatusUtil.updateWikiStatusFlag(sysIds, flag, flagValue, false, username);
				}

				result.setSuccess(true);
			}
		} catch (Exception e) {
			String errMsg = getErrorMessage(flag);
			Log.log.error(errMsg, e);
			result.setSuccess(false).setMessage(errMsg);
		}

		return result;
    }
    
    private static String getErrorMessage(WikiStatusEnum flag) {
		switch (flag) {
			case ACTIVE:
				return ERROR_UPDATING_ACTIVE_FLAG;
			case LOCKED:
				return ERROR_UPDATING_LOCK_FLAG;
			case HIDDEN:
				return ERROR_UPDATING_HIDDEN_FLAG;
			case DELETED:
				return ERROR_UPDATING_DELETED_FLAG;
			default:
				return null;
		}
    }
}
