/******************************************************************************
* (C) Copyright 2014
*
* Resolve Systems, LLC
*
* All rights reserved
* This software is distributed under license and may not be used, copied,
* modified, or distributed without the express written permission of
* Resolve Systems, LLC.
*
******************************************************************************/

package com.resolve.rsview.controller;

import static java.util.stream.Collectors.toList;

import java.io.IOException;
import java.io.InputStream;
import java.security.AccessControlException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.Scanner;
import java.util.Set;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.codehaus.jackson.map.ObjectMapper;
import org.eclipse.jetty.util.StringUtil;
import org.hibernate.DuplicateMappingException;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.google.gson.JsonElement;
import com.google.gson.JsonParser;
import com.resolve.persistence.model.ActionTaskArchive;
import com.resolve.rsview.main.MIndex;
import com.resolve.search.actiontask.ActionTaskIndexAPI;
import com.resolve.services.ServiceHibernate;
import com.resolve.services.ServiceResolutionBuilder;
import com.resolve.services.exception.MissingVersionException;
import com.resolve.services.hibernate.actiontask.ActionTaskArchiveService;
import com.resolve.services.hibernate.actiontask.ActionTaskService;
import com.resolve.services.hibernate.util.ActionTaskUtil;
import com.resolve.services.hibernate.vo.AccessRightsVO;
import com.resolve.services.hibernate.vo.ResolveActionInvocOptionsVO;
import com.resolve.services.hibernate.vo.ResolveActionInvocVO;
import com.resolve.services.hibernate.vo.ResolveActionTaskVO;
import com.resolve.services.hibernate.vo.exception.ValidationException;
import com.resolve.services.util.Constants;
import com.resolve.services.util.DateManipulator;
import com.resolve.services.vo.ActionTaskTreeDTO;
import com.resolve.services.vo.QueryDTO;
import com.resolve.services.vo.ResponseDTO;
import com.resolve.util.Log;
import com.resolve.util.StringUtils;
import com.resolve.wiki.dto.HistoryDTO;

import net.sf.json.JSONObject;

/**
 * 
 * This controller serves the request(CRUD operations) for Action Task. 
 * <p>
 * Actiontask is smallest unit of work in a workflow/runbook. It can accept input parameters, assess it and set output parameters which act as input to other actiontask.</br>
 * This is a complex object which has Parser, Assessor, Preprocessor, Properties, etc as references. They all together work as an actiontask.
 * <p> 
 * For more information about Actiontask, please refer to <a href="http://community.gen-e.com/resolve/jsp/rsclient.jsp">Resolve documentation</a>. 
 * <p>
 * DB Tables: {@code resolve_action_task, resolve_action_invoc, resolve_action_invoc_options, resolve_action_parameter, resolve_action_task_mock_data} </br>
 * Url : {@code http://localhost:8080/resolve/jsp/rsclient.jsp#RS.actiontask.Main/}
 * 
 * @author jeet.marwah
 *
 */
@Controller
public class AjaxActionTask extends GenericController
{
    private static final String SAVE_AT_ERR_MSG = "Error saving the Actiontask.";
    private static final String SAVE_AT_FROM_AUTOMATION_ERR_MSG = "Error saving action task from automation builder.";
    private static final String AT_MANIPULATION_ERR_MSG = "Error in manipulating action task";
    
    private ActionTaskService actionTaskService = ActionTaskService.INSTANCE;
    private ActionTaskArchiveService actionTaskArchiveService = ActionTaskArchiveService.INSTANCE;
    
	@ResponseBody
	@RequestMapping(value = "/actiontask/getHistory", method = {
			RequestMethod.GET }, headers = "Accept=application/json; charset=utf-8")
	public ResponseDTO<HistoryDTO> getHistory(@RequestParam String id, HttpServletRequest request,
			HttpServletResponse response) {

		ResponseDTO<HistoryDTO> responseBody = new ResponseDTO<HistoryDTO>();
		String username = (String) request.getAttribute(Constants.HTTP_REQUEST_USERNAME);

		try {
			List<ActionTaskArchive> history = actionTaskArchiveService.getActionTaskArchiveHistory(id, username);
			String docFullName = actionTaskService.getActionTaskName(id, username);
			
			responseBody.setSuccess(true)
					.setRecords(history.stream()
							.map(archive -> buildHistory(archive, docFullName))
							.collect(toList()));
			
		} catch (AccessControlException e) {
			Log.log.error(e.getMessage(), e);
			responseBody.setSuccess(false).setMessage(e.getMessage());
			response.setStatus(HttpStatus.FORBIDDEN.value());
		} catch (IllegalStateException e) {
			Log.log.error(e.getMessage(), e);
			responseBody.setSuccess(false).setMessage(e.getMessage());
			response.setStatus(HttpStatus.PRECONDITION_FAILED.value());
		} catch (Exception e) {
			Log.log.error(e.getMessage(), e);
			responseBody.setSuccess(false).setMessage(e.getMessage());
			response.setStatus(HttpStatus.INTERNAL_SERVER_ERROR.value());
		}

		return responseBody;
	}
	
	private HistoryDTO buildHistory(ActionTaskArchive archive, String docFullName) {
		return new HistoryDTO(
				docFullName, 
				archive.getUVersion(), 
				null, 
				archive.getUComment(),
				archive.getSysCreatedBy(), 
				DateManipulator.getDateInUTCFormat(archive.getSysCreatedOn()),
				archive.getSysCreatedOn(), archive.isUIsStable());
	}
    
    /**
     * Returns/Retrieves list of Actiontasks for the grid based on filters, sorts, pagination. 
     *  
     * <p>
     * <u><b>Example</b></u>:</br> 
     * <code><pre>
     *  delim:/
     *  id:/charter/
     *  filter:[{"field":"unamespace","type":"auto","condition":"equals","value":"charter"}]
     *  page:1
     *  start:0
     *  limit:50
     *  sort:[{"property":"uname","direction":"ASC"}]
     * </pre></code>
     * 
     * <p>
     * Parameters in the Request object: 
     * <code><pre>
     *  USERNAME - username of the user who is logged in to Resolve
     *  id - path or module name from the UI
     *  delimiter - expected '/' for menu path and '.' for module
     * </pre></code>
     *  
     * <p>
     * Return object in response:
     * <code><pre>
     *  'records' : List of type {@link ResolveActionTaskVO}
     *  'total' : # of total records in the table/db 
     * </pre></code>
     *  
     * Attributes populated in records:
     * <code><pre>
     *  sys_id,sysCreatedOn,sysCreatedBy,sysUpdatedOn,sysUpdatedBy,UName,UDescription
     * </pre></code>
     * 
     * 
     * @param query  {@link QueryDTO}, represents the query, pagination, sorting, grouping for the UI
     * @param request  HttpServletRequest
     * @param response  HttpServletResponse
     * @return {@link ResponseDTO} - Reponse Data Transfer Object having a List  and total # of records 
     * @throws ServletException
     * @throws IOException
     */
    @RequestMapping(value = "/actiontask/list", method = { RequestMethod.GET, RequestMethod.POST }, headers = "Accept=application/json; charset=utf-8")
    @ResponseBody
    public ResponseDTO<ResolveActionTaskVO> list(@ModelAttribute QueryDTO query, HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException
    {	
        ResponseDTO<ResolveActionTaskVO> result = new ResponseDTO<ResolveActionTaskVO>();
        String username = (String) request.getAttribute(Constants.HTTP_REQUEST_USERNAME);
        String node = request.getParameter("id");
        String delimiter = request.getParameter("delim");
        query.setWhereClause(node + "," + delimiter);
        
        try
        {   
            query.setModelName("ResolveActionTask");
            query.setSelectColumns("sys_id,sysCreatedOn,sysCreatedBy,sysUpdatedOn,sysUpdatedBy,UName,UNamespace,UFullName,USummary,UActive");
            
            result = ServiceHibernate.getResolveActionTasks(query, username);
            result.setSuccess(true);
        }
        catch (Exception e)
        {
            Log.log.error("Error retrieving ResolveActionTask", e);
            result.setSuccess(false).setMessage("Error getting ActionTasks list. See log for more info.");
        }
        
        return result;
    }
    
    /**
     * Get Actiontask by sysId or Name. This will pull out the referenced entities like Parser, Assessor, Preprocessor, etc that makes an Actiontask.
     * 
     * <p>
     * Parameters in the Request object:
     * <code><pre> 
     *  USERNAME - username of the user who is logged in to Resolve
     * </pre></code>
     * <p>
     * Return object in response 
     * <code><pre>
     *  'data' : {@link ResolveActionTaskVO}
     * </pre></code>
     *  
     * @param id sysId of the Actiontask
     * @param name fullname of the Actiontask
     * @param request HttpServletRequest
     * @param response HttpServletResponse
     * @return {@link ResponseDTO} - Reponse Data Transfer Object with 'data' of type {@link ResolveActionTaskVO}
     * @throws ServletException
     * @throws IOException
     */
    @RequestMapping(value = "/actiontask/get", method = { RequestMethod.POST }, headers = "Accept=application/json; charset=utf-8")
    @ResponseBody
    public ResponseDTO<ResolveActionTaskVO> get(@RequestParam("id") String id, 
    		@RequestParam("name") String name, 
    		@RequestParam(name = "version", required = false) String version,
    		HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException
    {
        ResponseDTO<ResolveActionTaskVO> result = new ResponseDTO<ResolveActionTaskVO>();
        String username = (String) request.getAttribute(Constants.HTTP_REQUEST_USERNAME);
        
        try
        {
            ResolveActionTaskVO vo = ServiceHibernate.getActionTaskFromIdWithReferences(id, name, username, true, version);
            if(vo == null)
            {
                throw new Exception("Actiontask with ID '" + id + "' or Name '" + name + "' does not exist.");
            }
            result.setData(vo);
            result.setSuccess(true);
            
        }
        catch (MissingVersionException e) {
            result.setSuccess(false).setMessage(checkAndSetErrorMessage(e, "Missing version requested."));
        }
        catch (Exception e)
        {
            Log.log.error("Error retrieving ActionTask ", e);
            result.setSuccess(false).setMessage("Error getting ActionTask. See log for more info.");
        }

        return result;
    }
    
	@RequestMapping(value = "/actiontask/save", method = { RequestMethod.GET,
			RequestMethod.POST }, headers = "Accept=application/json; charset=utf-8")
	@ResponseBody
	public ResponseDTO<ResolveActionTaskVO> save(@RequestBody JSONObject jsonProperty, HttpServletRequest request,
			HttpServletResponse response) throws ServletException {
		return saveCommitActionTask(true, jsonProperty, null, request, response);
	}

	@RequestMapping(value = "/actiontask/commit", method = {
			RequestMethod.POST }, headers = "Accept=application/json; charset=utf-8")
	@ResponseBody
	public ResponseDTO<ResolveActionTaskVO> commit(@RequestBody JSONObject jsonProperty,
			@RequestParam(required = false) String comment, HttpServletRequest request, HttpServletResponse response)
			throws ServletException {
		return saveCommitActionTask(false, jsonProperty, comment, request, response);
	}

	/**
	 * 
	 * Saves/Commit an Actiontask. If 'sysId' is set, it will be update else it will
	 * create it. On a successful operation, it will update the ActionTask version
	 * in the database and preserve the document's data as raw string content under
	 * that version.
	 * <p>
	 * HTTP status codes this method might return:
	 * <p>
	 * 400 - BAD REQUEST: in case the raw action task data failed to convert to
	 * {@link ResolveActionTaskVO}
	 * </p>
	 * <p>
	 * 409 - CONFLICT: in cases where there is already an existing version with the
	 * same number for that action task. Can only happen in concurrent scenarios.
	 * </p>
	 * </p>
	 * 
	 * @param jsonProperty JSONObject/JSON having data of type
	 *                     {@link ResolveActionTaskVO}
	 * @param request      HttpServletRequest, USERNAME - username of the user who
	 *                     is logged in to Resolve
	 * @param response     HttpServletResponse
	 * @return {@link ResponseDTO} - Reponse Data Transfer Object with 'data' having
	 *         the created/updated {@link ResolveActionTaskVO}
	 * @throws ServletException
	 */
	private ResponseDTO<ResolveActionTaskVO> saveCommitActionTask(Boolean isSave, JSONObject jsonProperty,
			String comment, HttpServletRequest request, HttpServletResponse response) {
		if (Log.log.isDebugEnabled()) {
			Log.log.debug("/actiontask/save JSON : [" + jsonProperty + "]");
		}

		ResponseDTO<ResolveActionTaskVO> result = new ResponseDTO<ResolveActionTaskVO>();
		comment = StringUtil.isBlank(comment) ? buildDefaultComment(isSave) : comment;
		
		try {
			ResolveActionTaskVO vo = ServiceHibernate.saveResolveActionTask(
					new ObjectMapper().readValue(
					jsonProperty.toString(), ResolveActionTaskVO.class),
					jsonProperty.toString(), 
					(String) request.getAttribute(Constants.HTTP_REQUEST_USERNAME),
					comment,
					isSave);

			result.setSuccess(true).setData(vo);
		} catch (IOException e) {
			Log.log.error(e.getMessage(), e);
			response.setStatus(HttpStatus.BAD_REQUEST.value());
			result.setSuccess(false).setMessage("Cannot process a malformed action task");
		} catch (DuplicateMappingException e) {
			response.setStatus(HttpStatus.CONFLICT.value());
			result.setSuccess(false).setMessage(e.getMessage());
		} catch (ValidationException ve) {
            result.setSuccess(false).setMessage(ve.getMessage());
		} catch (Exception e) {
			Log.log.error(e.getMessage(), e);
			result.setSuccess(false).setMessage(checkAndSetErrorMessage(e, SAVE_AT_ERR_MSG));
		}

		return result;
	}
	
	private static String buildDefaultComment(boolean isSave) {
		return isSave ? "Saved Draft" : "Committed Version";
	}
	
    /**
     *  Delete the Actiontask based on array of sysIds. It will also delete the following components that are related to the Actiontask
     *  <ul>
     *      <li>Assessor, Prepreprocessor, Parser - If they are LOCAL to this Actiontask. LOCAL are if the name of the component is same as that of Actiontask</li>
     *      <li>Parameters defined for this actiontask</li>
     *      <li>Options defined for this actiontask</li>
     *      <li>Mock Data for this actiontask </li>
     *      <li>Graph node for this actiontask</li>
     *      <li>Indexes in Elastic Search </li>
     *  </ul>
     * <p>
     * Parameters in the Request object: 
     * <code><pre>
     *  USERNAME - username of the user who is logged in to Resolve
     * </pre></code>
     *   
     * @param ids array of sysIds to be deleted
     * @param all true/false if want to delete all the records - THIS IS NOT FUNCTIONAL yet
     * @param validate true/false if wanted to validate before deleteing which Runbooks are using this Actiontask
     * @param request HttpServletRequest
     * @param response HttpServletResponse
     * @return {@link ResponseDTO} - Reponse Data Transfer Object with status as 'true' or 'false'
     * @throws ServletException
     * @throws IOException
     */
    
    @RequestMapping(value = "/actiontask/delete", method = { RequestMethod.POST,RequestMethod.GET }, headers = "Accept=application/json; charset=utf-8")
    @ResponseBody
    public ResponseDTO delete(@RequestParam("ids") List<String> ids, @RequestParam("all") Boolean all, @RequestParam("validate") Boolean validate,
                    HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException
    {
        ResponseDTO<String> result = new ResponseDTO<String>();
        String username = (String) request.getAttribute(Constants.HTTP_REQUEST_USERNAME);
        try
        {
            Set<String> referredWikiNameSet = ServiceHibernate.deleteResolveActionTaskByIds(ids, validate, username);
            List<String> referredWikiNameList = new ArrayList<String>();
            referredWikiNameList.addAll(referredWikiNameSet);
            result.setRecords(referredWikiNameList);
            result.setSuccess(true);
        }
        catch (Exception e)
        {
            String errorMsg = e.getMessage();
            if (StringUtils.isBlank(errorMsg)) {
                errorMsg = "Error deleting ActionTask. See log for more info.";
            }
            Log.log.error(errorMsg, e);
            result.setSuccess(false).setMessage(errorMsg);
        }

        return result;
    }
    
    
    /**
     * 
     * This api is the entry point for Rename/Move or Copy of Actiontask. 
     * <p>
     * Parameters in the Request object: 
     * <code><pre>
     *  USERNAME - username of the user who is logged in to Resolve
     * </pre></code>
     * 
     * @param action valid values are 'rename' OR 'move', 'copy'
     * @param actionTaskIdList list of sysId/s
     * @param module name of the module to be copied/renamed to
     * @param name name of the actiontask to be copied/renamed to
     * @param overwrite true/false - valid only for 'copy'
     * @param request HttpServletRequest
     * @param response HttpServletResponse
     * @return {@link ResponseDTO} - Reponse Data Transfer Object that will have list of Actiontask Fullnames that had error moving/copying
     * @throws ServletException
     * @throws IOException
     */
    @RequestMapping(value = "/actiontask/actiontaskmanipulation", method = { RequestMethod.POST,RequestMethod.GET }, headers = "Accept=application/json; charset=utf-8")
    @ResponseBody
    public ResponseDTO<String> rename(@RequestParam("action") String action, @RequestParam("ids") List<String> actionTaskIdList, @RequestParam("module") String module,
                    @RequestParam("name") String name, @RequestParam("overwrite") Boolean overwrite,
                    HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException
    {
        ResponseDTO<String> result = new ResponseDTO<String>();
        String userName = (String) request.getAttribute(Constants.HTTP_REQUEST_USERNAME);
        
        try
        {
            if (action.equals("rename") || action.equals("move"))
            {
                List<String> errorList = ServiceHibernate.moveOrRenameActionTask(actionTaskIdList, name, module, userName);
                result.setRecords(errorList);
            }
            else if (action.equals("copy"))
            {
                List<String> errorList = ServiceHibernate.copyActionTasks(actionTaskIdList, name, module, overwrite, userName);
                result.setRecords(errorList);
            }
            // System.out.println(action + ":" + actionTaskIdList +":" + module +":"+ name);
            result.setSuccess(true);
        }
        catch (Exception e)
        {
            result.setSuccess(false).setMessage(String.format("%s Manipulation Action - %s.", 
            												  checkAndSetErrorMessage(e, AT_MANIPULATION_ERR_MSG), action));
        }

        return result;
    }
    
    
    /**
     * Returns/Retrieves list of ActionTaskTreeDTO for the grid panel
     * <p>
     * <u><b>Example(for Menu Path)</b></u>:</br> 
     * <code><pre>
     *  node:rsqa/
     *  delim:/
     *  filter:[{"field":"assignedToUsername","type":"auto","condition":"equals","value":""}]
     * </pre></code>
     * 
     * <p>
     * <u><b>Example(for Module)</b></u>:</br> 
     * <code><pre>
     *  node:Demo.
     *  delim:.
     *  filter:[{"field":"assignedToUsername","type":"auto","condition":"equals","value":""}]
     * </pre></code>
     * 
     * <p>
     * Parameters in the Request object: 
     * <code><pre>
     *  USERNAME - username of the user who is logged in to Resolve
     *  node - filter node for menu path or module name
     *  delim - '/' (for Menu Path) or '.'(for Module name)
     * </pre></code>
     *  
     * <p>
     * Return object in response:
     * <code><pre>
     *  'records' : List of type {@link ActionTaskTreeDTO}
     * </pre></code>
     *  
     * @param query  {@link QueryDTO}, represents the query, pagination, sorting, grouping for the UI
     * @param request  HttpServletRequest
     * @param response  HttpServletResponse
     * @return {@link ResponseDTO} - Reponse Data Transfer Object having a List  and total # of records 
     * @throws ServletException
     * @throws IOException
     */
    @RequestMapping(value = "/actiontask/treelist", method = { RequestMethod.GET, RequestMethod.POST }, headers = "Accept=application/json; charset=utf-8")
    @ResponseBody
    public ResponseDTO treelist(@ModelAttribute QueryDTO query, HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException
    {
        ResponseDTO result = new ResponseDTO();
        List<ActionTaskTreeDTO> treeList = null;
        String username = (String) request.getAttribute(Constants.HTTP_REQUEST_USERNAME);
        
        String reqQuery = request.getParameter("node");
        String delimiter = request.getParameter("delim");
        query.setWhereClause(reqQuery + "," + delimiter);
        
        try
        {
            treeList = ServiceHibernate.getActionTaskTree(query, username);
            result.setSuccess(true).setRecords(treeList);
        }
        catch (Exception e)
        {
            Log.log.error("Error retrieving ActionTask Tree", e);
            result.setSuccess(false).setMessage("Error retrieving ActionTask Tree. See log for more info.");
        }
        
        return result;
    }
    
    
    /**
     * @deprecated
     * <p>
     * THIS IS NON FUNCTIONAL. PLEASE IGNORE THIS API. IT WILL BE REMOVED.
     * 
     * @param query
     * @param request
     * @param response
     * @return
     * @throws ServletException
     * @throws IOException
     */
    @RequestMapping(value = "/actiontask/parser/template", method = { RequestMethod.GET, RequestMethod.POST }, headers = "Accept=application/json; charset=utf-8")
    @ResponseBody
    public ResponseDTO<String> template(@ModelAttribute QueryDTO query, HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException
    {
        ResponseDTO<String> result = new ResponseDTO<String>();

        String username = (String) request.getAttribute(Constants.HTTP_REQUEST_USERNAME);
        
        String parser = "";
        
        try
        {
            
            result.setSuccess(true).setData(parser);
        }
        catch (Exception e)
        {
            Log.log.error("Error retrieving ActionTask Tree", e);
            result.setSuccess(false).setMessage("Error retrieving ActionTask Tree. See log for more info.");
        }
        
        return result;
    }
    
    @RequestMapping(value = "/actiontask/updateIndexMapping", method = { RequestMethod.GET, RequestMethod.POST }, headers = "Accept=application/json; charset=utf-8")
    @ResponseBody
    public ResponseDTO<String> updateIndexMapping(HttpServletRequest request, HttpServletResponse response) {
    	  ResponseDTO<String> result = new ResponseDTO<String>();
          String username = (String) request.getAttribute(Constants.HTTP_REQUEST_USERNAME);
          try {
        	  ActionTaskIndexAPI.updateIndexMapping(username);
        	  result.setSuccess(true);
          }
          catch (Exception e)
          {
              Log.log.error("Error update index mapping for action tasks", e);
              result.setSuccess(false).setMessage("Error update index mapping for action tasks. See log for more info.");
          }
          
          return result;
    }
    
    @RequestMapping(value = "/actiontask/index", method = { RequestMethod.GET, RequestMethod.POST }, headers = "Accept=application/json; charset=utf-8")
    @ResponseBody
    public ResponseDTO<String> index(@RequestParam(value = "all",defaultValue="false") Boolean all,@RequestParam(value = "ids",defaultValue = "[]") List<String> ids, HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException
    {
        ResponseDTO<String> result = new ResponseDTO<String>();
        String username = (String) request.getAttribute(Constants.HTTP_REQUEST_USERNAME);
        try
        {
            if(!all)
                ActionTaskIndexAPI.indexActionTasks(ids, username);
            else
                MIndex.indexAllActionTasks(new HashMap<String, Object>());
            result.setSuccess(true);
        }
        catch (Exception e)
        {
            Log.log.error("Error indexing ActionTask", e);
            result.setSuccess(false).setMessage("Error indexing ActionTask. See log for more info.");
        }
        
        return result;
    }
    
    @RequestMapping(value = "/actiontask/getLogRawResultFlag", method = { RequestMethod.GET, RequestMethod.POST }, headers = "Accept=application/json; charset=utf-8")
    @ResponseBody
    public ResponseDTO getLogRawResultFlag(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException
    {   
        ResponseDTO<Boolean> result = new ResponseDTO<Boolean>();
        
        try
        {
            boolean flag = ServiceHibernate.getLogRawResultFlag();
            result.setData(flag);
            result.setSuccess(true);
        }
        catch (Exception e)
        {
            Log.log.error("Error saving the Actiontask", e);
            result.setSuccess(false).setMessage("Error getting action task log raw result flag. See log for more info.");
        }

        return result;
    }
    
    @RequestMapping(value = "/actiontask/getParserAutoGenCode", method = { RequestMethod.POST, RequestMethod.GET }, headers = "Accept=application/json; charset=utf-8")
    @ResponseBody
    public ResponseDTO<String> getParserAutoGenCode(@RequestBody JSONObject jsonProperty, HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException
    {
        // RBA-14037 : JSON format text can't be used in RegExp parser
        // getting the plain "sampleOutput" value from the request and put it again as a JSON plain string
        if (jsonProperty.getString("parserType").equalsIgnoreCase("text"))
        {
            String plainTextPayload = "";
            if ("POST".equalsIgnoreCase(request.getMethod()))
            {
                InputStream is = request.getInputStream();
                Scanner s = new Scanner(is, "UTF-8");
                s.useDelimiter("\\A");
                plainTextPayload = s.hasNext() ? s.next() : "";
                is.close();
                s.close();
            }
            JsonElement sampleOutputJsonElement = new JsonParser().parse(plainTextPayload);
            String sampleOutputTreatedAsText = sampleOutputJsonElement.getAsJsonObject().get("sampleOutput").getAsString();
            jsonProperty.put("sampleOutput", sampleOutputTreatedAsText);
        }
        // using jackson to deserialize the json
        String parserProperties = jsonProperty.toString();
        ResponseDTO<String> result = new ResponseDTO<String>();
        try
        {
           String AutoGenCode = ServiceResolutionBuilder.getParserAutoGenCode(parserProperties);
           result.setSuccess(true).setData(AutoGenCode);
        }
        catch (Exception e)
        {
            Log.log.error("Error saving the task information.", e);
            result.setSuccess(false).setMessage("Error getting action task parser auto gen code. See log for more info.");
        }

        return result;
    }
    
    /**
     * An API to get autogenerated code for the specified <code>connectionType</code>.
     * 
     * @param connectionType : String representing what type of connection code should be auto generated. It could either have SSH, TELNET or HTTP.
     * @param request
     * @return
     * @throws ServletException
     * @throws IOException
     */
    @RequestMapping(value = "/actiontask/getContentAutoGenCode", method = { RequestMethod.POST, RequestMethod.GET }, headers = "Accept=application/json; charset=utf-8")
    @ResponseBody
    public ResponseDTO<String> getContentAutoGenCode(@RequestBody String connectionType, HttpServletRequest request) throws ServletException, IOException
    {
    	ResponseDTO<String> result = new ResponseDTO<String>();
    	String username = (String) request.getAttribute(Constants.HTTP_REQUEST_USERNAME);
    	
    	try
    	{
    		String code = ServiceHibernate.getContentAutoGenCode(connectionType, username);
    		result.setSuccess(true).setData(code);
    	}
    	catch(Exception e)
    	{
    		Log.log.error("Error getting content auto generated code.", e);
            result.setSuccess(false).setMessage("Error get action task content auto gen code. See log for more info.");
    	}
    	return result;
    }
    
    /**
     * API to save an Action Task if saved from Automation Builder. If task type is remote and if it's supposed to use a connection,
     * 		the Content code is auto generated if
     * 1: invoc options is_auto_generated flag is either null (new AT) or Y and 
     * 2: the action task is referred from 1 or less number of runbooks.
     * 
     * @param jsonProperty : JSONObject/JSON having data of type {@link ResolveActionTaskVO}
     * @param connectionType : String representing what type of connection code should be auto generated. It could either have SSH, TELNET or HTTP.
     * @param request HttpServletRequest
     * @return
     * @throws ServletException
     * @throws IOException
     */
    @RequestMapping(value = "/actiontask/saveTaskFromAutomation", method = { RequestMethod.POST, RequestMethod.GET }, headers = "Accept=application/json; charset=utf-8")
    @ResponseBody
    public ResponseDTO<ResolveActionTaskVO> saveTaskFromAutomation(@RequestBody JSONObject jsonProperty, @RequestBody String connectionType, HttpServletRequest request) throws ServletException, IOException
    {
    	ResponseDTO<ResolveActionTaskVO> result = new ResponseDTO<ResolveActionTaskVO>();
    	String username = (String) request.getAttribute(Constants.HTTP_REQUEST_USERNAME);
    	
    	try
    	{
    		ResolveActionTaskVO actionTaskVO = new ObjectMapper().readValue(jsonProperty.toString(), ResolveActionTaskVO.class);
    		if (ActionTaskUtil.getWikiNamesReferringActionTask(actionTaskVO.getUFullName()).size() <= 1)
    		{
    			String code = ServiceHibernate.getContentAutoGenCode(connectionType, username);
    			
    			ResolveActionInvocVO  actionInvocVO = actionTaskVO.getResolveActionInvoc();
    			if (actionInvocVO != null)
    			{
    				Collection<ResolveActionInvocOptionsVO> invocOptions = actionInvocVO.getResolveActionInvocOptions();
    				if (invocOptions != null)
    				{
    					ResolveActionInvocOptionsVO localInvocOption = null;
    					
    					for (ResolveActionInvocOptionsVO invocOption : invocOptions)
    					{
    						if ((invocOption.getIsAutoGenerated() == null || invocOption.getIsAutoGenerated()) && invocOption.getUName().equals("INPUTFILE"))
							{
    							localInvocOption = invocOption;
								break;
							}
    					}
    					
    					if (localInvocOption == null)
    					{
    						localInvocOption = new ResolveActionInvocOptionsVO();
    						invocOptions = new ArrayList<ResolveActionInvocOptionsVO>();
    						invocOptions.add(localInvocOption);
    						actionInvocVO.setResolveActionInvocOptions(invocOptions);
    					}
    					
    					localInvocOption.setUValue(code);
    					localInvocOption.setUName("INPUTFILE");
    					localInvocOption.setUDescription(actionTaskVO.getUFullName());
    				}
    			}
    		}
    		
    		ResolveActionTaskVO vo = ServiceHibernate.saveResolveActionTask(actionTaskVO, username);
            result.setSuccess(true).setData(vo);
    	}
    	catch(Exception e)
    	{
            result.setSuccess(false).setMessage(checkAndSetErrorMessage(e, SAVE_AT_FROM_AUTOMATION_ERR_MSG));
    	}
    	
    	return result;
    }
    
    /**
     * Return default roles for an action task.
     * 
     * @param request
     * @return
     * 		ResponseDTO : ResponseDTO object with data set to {@link AccessRightsVO} which default roles populated. 
     * @throws ServletException
     * @throws IOException
     */
    
	@RequestMapping(value = "/actiontask/getDefaultRoles", method = { RequestMethod.POST, RequestMethod.GET }, headers = "Accept=application/json; charset=utf-8")
    @ResponseBody
    public ResponseDTO<AccessRightsVO> getDefaultRoles(HttpServletRequest request) throws ServletException, IOException
    {
    	ResponseDTO<AccessRightsVO> result = new ResponseDTO<AccessRightsVO>();
    	
    	try
    	{
    		AccessRightsVO accessRights = ActionTaskUtil.getDefaultRoles();
    		result.setData(accessRights);
    		result.setSuccess(true);
    	}
    	catch(Exception e)
    	{
    		Log.log.error("Could not get task default roles", e);
    		result.setMessage("Could not get task default roles");
    		result.setSuccess(false);
    	}
    	
    	return result;
    }
	
}
