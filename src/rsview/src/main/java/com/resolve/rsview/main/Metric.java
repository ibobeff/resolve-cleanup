/******************************************************************************
* (C) Copyright 2014
*
* Resolve Systems, LLC
*
* All rights reserved
* This software is distributed under license and may not be used, copied,
* modified, or distributed without the express written permission of
* Resolve Systems, LLC.
*
******************************************************************************/

package com.resolve.rsview.main;

import java.lang.management.ManagementFactory;
import java.lang.management.ThreadMXBean;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.atomic.AtomicLong;

import org.apache.commons.collections.MapUtils;
import org.apache.commons.lang3.tuple.Pair;

import com.resolve.execute.ExecuteOS;
import com.resolve.execute.ExecuteResult;
import com.resolve.rsbase.BaseMetric;
import com.resolve.rsview.auth.AuthSession;
import com.resolve.rsview.auth.Authentication;
import com.resolve.services.hibernate.util.UserUtils;
import com.resolve.util.MetricMsg;
import com.resolve.util.StringUtils;

public class Metric extends BaseMetric
{

    public static final String USER_POST = "USER_POST";
    public static final String USER_COMMENT = "USER_COMMENT";
    public static final String USER_LIKE = "USER_LIKE";
    
    

    public static AtomicLong wikiResponseTime = new AtomicLong();
    public static AtomicLong wikiViewCount = new AtomicLong();
    public static AtomicLong socialResponseTime = new AtomicLong();
    public static AtomicLong socialPageViewCount = new AtomicLong();
    protected static long startTime = 0;
    protected static long endTime = 0;
    protected static boolean isSocialPage = false;

    //read  and load the threshold values at component startup
    //public static MetricThreshold mthreshold = new MetricThreshold();

    //three map for holding the social report data
    // Map1 (userPostCount)    --- user, post count
    // Map2 (userCommentCount) --- user, comment count
    // Map3 (userLikeCount)    --- user, mark like count
    private static Map<String, Integer> userPostCount = new ConcurrentHashMap<String, Integer>();
    private static Map<String, Integer> userCommentCount = new ConcurrentHashMap<String, Integer>();
    private static Map<String, Integer> userLikeCount = new ConcurrentHashMap<String, Integer>();

    public static Map<String, Map<String, Integer>> alldata = new ConcurrentHashMap<String, Map<String, Integer>>();
    
    public Metric ()
    {
        super(new MetricThresholdInitImpl());
    }
    
    public void sendMetrics()
    {
        if(mthreshold.thresholdProperties.keySet().isEmpty())
        {
            mthreshold.setThresholdProperties();
        }
        // send jvm metrics
        sendMetricJVM();

        // send user metrics
        sendMetricUsers();

        // send cpu (only unix)
        sendMetricCPU();

        //send response time
        sendMetricsResponseTime();

    } // sendMetrics

    void sendMetricJVM()
    {
    	MetricMsg result = new MetricMsg("JVM", Main.main.release.getHostServiceName()+"/"+Main.main.configId.getGuid(), "");

        // MemoryPercentFree
        long free = (Runtime.getRuntime().maxMemory() - Runtime.getRuntime().totalMemory()) + Runtime.getRuntime().freeMemory();
        int value = (int) (free * 100 / Runtime.getRuntime().maxMemory());
    	result.addMetric("mem_free", value);

    	mthreshold.checkThresholds("jvm", value, "mem_free");

    	// PeakThread
        ThreadMXBean thread = ManagementFactory.getThreadMXBean();
    	result.addMetric("thread_count", thread.getThreadCount());
//        int thread_count_percentage = (int) (thread.getThreadCount() * 100) / (Main.main.configGeneral.getMaxThread() * 2); //approx max for the jvm thread = double the thread poll max
//        mthreshold.checkThresholds("jvm", thread_count_percentage, "thread_count");

    	Main.sendMetric(result);
    }

    void sendMetricUsers()
    {
    	MetricMsg result = new MetricMsg("Users", Main.main.release.getHostServiceName()+"/"+Main.main.configId.getGuid(), "");

    	Map<String,AuthSession> cache = Authentication.getAuthTokenCache();
    	if (cache != null)
    	{
    	    Set<String> unique = new HashSet<String>();

    	    // only count unique username+host as 1 user
    	    for (AuthSession session : cache.values())
    	    {
	            unique.add(session.getUsername()+"/"+session.getHost());
    	    }

	        // active users
	    	result.addMetric("active", unique.size());
	        mthreshold.checkThresholds("users", unique.size(), "active");

	    	Main.sendMetric(result);
    	}
    } // sendMetricUsers

    public void sendMetricLicensedUsers()
    {
        MetricMsg result = new MetricMsg("LicensedUsers", Main.main.release.getHostServiceName()+"/"+Main.main.configId.getGuid(), "");

        int endUsers = UserUtils.getNumberOfUsers();
        int adminUsers = UserUtils.getNumberOfAdminUsers();
        if (endUsers > 0 || adminUsers > 0)
        {
            // active users
            result.addMetric("endUsers", endUsers);
            result.addMetric("adminUsers", adminUsers);

            Main.sendMetric(result);
        }
    } // sendMetricUsers

    void sendMetricCPU()
    {
        String osname = System.getProperty("os.name").toLowerCase();
        if (osname.equals("linux") || osname.equals("sunos") || osname.equals("aix"))
        {
            // execute and parse uptime
            ExecuteOS os = new ExecuteOS(Main.main.getProductHome());
            ExecuteResult result = os.execute("uptime", null, true, 10, "sendMetricCPU");
            String out = result.getResultString();
            if (out != null)
            {
            	Map<String, Pair<Long, Integer>> cpuMetrics = MetricMsg.parseLinuxCPUMetricOutput(out);
            	
            	if (MapUtils.isNotEmpty(cpuMetrics)) {
            		MetricMsg msg = new MetricMsg("Server", 
                							  	  Main.main.release.getHostServiceName() + "/" + 
                							  	  Main.main.configId.getGuid(), "");

            		if (cpuMetrics.containsKey(MetricMsg.CPU_METRIC_LOAD_1)) {
            			msg.addMetric(MetricMsg.CPU_METRIC_LOAD_1, 
            						  cpuMetrics.get(MetricMsg.CPU_METRIC_LOAD_1).getLeft().longValue());
            			mthreshold.checkThresholds("server", 
            									   cpuMetrics.get(MetricMsg.CPU_METRIC_LOAD_1).getRight().intValue() / 100, 
            									   MetricMsg.CPU_METRIC_LOAD_1);
            		}
            		
            		if (cpuMetrics.containsKey(MetricMsg.CPU_METRIC_LOAD_5)) {
            			msg.addMetric(MetricMsg.CPU_METRIC_LOAD_5, 
            						  cpuMetrics.get(MetricMsg.CPU_METRIC_LOAD_5).getLeft().longValue());
            			mthreshold.checkThresholds("server", 
            									   cpuMetrics.get(MetricMsg.CPU_METRIC_LOAD_5).getRight().intValue() / 100, 
            									   MetricMsg.CPU_METRIC_LOAD_5);
            		}
            		
            		if (cpuMetrics.containsKey(MetricMsg.CPU_METRIC_LOAD_15)) {
            			msg.addMetric(MetricMsg.CPU_METRIC_LOAD_15, 
            						  cpuMetrics.get(MetricMsg.CPU_METRIC_LOAD_15).getLeft().longValue());
            			mthreshold.checkThresholds("server", 
            									   cpuMetrics.get(MetricMsg.CPU_METRIC_LOAD_15).getRight().intValue() / 100, 
            									   MetricMsg.CPU_METRIC_LOAD_15);
            		}
            		
            		Main.sendMetric(msg);
            	}
            }
        }
    } // sendMetricCPU

    /*******************************************************************
     * Collecting social response time and  wiki response time metrics
     *******************************************************************/
    void sendMetricsResponseTime()
    {
        MetricMsg result = new MetricMsg("Latency", Main.main.release.getHostServiceName()+"/"+Main.main.configId.getGuid(), "");
        Long totalWikiResponseTime = getWikiResponseTime();
        Long totalWikiViewCount = getWikiViewCount();

        Long totalSocialResponseTime = getSocialResponseTime();
        Long totalSocialPageViewCount = getSocialPageViewCount();

        result.addMetric("wiki", totalWikiViewCount);
        mthreshold.checkThresholds("latency", totalWikiViewCount.intValue(), "wiki");
        result.addMetric("wikiresponsetime", totalWikiResponseTime, totalWikiViewCount.intValue());
        mthreshold.checkThresholds("latency", totalWikiResponseTime.intValue(), "wikiresponsetime");
        result.addMetric("social", totalSocialPageViewCount);
        mthreshold.checkThresholds("latency", totalSocialPageViewCount.intValue(), "social");
        result.addMetric("socialresponsetime", totalSocialResponseTime, totalSocialPageViewCount.intValue());
        mthreshold.checkThresholds("latency", totalSocialResponseTime.intValue(), "socialresponsetime");
        Main.sendMetric(result);

        resetWikiResponseTime();
        resetWikiViewCount();
        resetSocialResponseTime();
        resetSocialPageViewCount();

    } //sendMetricsResponseTime

    public static long getStartTime()
    {
        return startTime;
    }

    public static void setStartTime(long startTime)
    {
        Metric.startTime = startTime;
    }

    public static long getEndTime()
    {
        return endTime;
    }

    public static void setEndTime(long endTime)
    {
        Metric.endTime = endTime;
    }

    public static void recordStartTime()
    {
        startTime = System.currentTimeMillis();
    } // startTime

    public static void recorEndTime()
    {
        endTime = System.currentTimeMillis() - startTime;
    } // endTime

    public static long getWikiResponseTime()
    {
        return wikiResponseTime.get();
    } // getWikiResponseTime

    public static void setWikiResponseTime(AtomicLong wikiResponseTime)
    {
        Metric.wikiResponseTime = wikiResponseTime;
    } //setWikiResponseTime

    public static void resetWikiResponseTime()
    {
        wikiResponseTime.set(0);
    } // resetWikiResponseTime

    public static long getWikiViewCount()
    {
        return wikiViewCount.get();
    } // getWikiViewCount

    public static void setWikiViewCount(AtomicLong wikiViewCount)
    {
        Metric.wikiViewCount = wikiViewCount;
    } // setWikiViewCount

    public static void resetWikiViewCount()
    {
        wikiViewCount.set(0);
    } // resetWikiViewCount

    public static long getSocialResponseTime()
    {
        return socialResponseTime.get();
    } // getSocialResponseTime

    public static void setSocialResponseTime(AtomicLong socialResponseTime)
    {
        Metric.socialResponseTime = socialResponseTime;
    } // setSocialResponseTime

    public static void resetSocialResponseTime()
    {
        socialResponseTime.set(0);
    } // resetSocialResponseTime

    public static long getSocialPageViewCount()
    {
        return socialPageViewCount.get();
    } // getSocialPageViewCount

    public static void setSocialPageViewCount(AtomicLong socialPageViewCount)
    {
        Metric.socialPageViewCount = socialPageViewCount;
    } // setSocialPageViewCount

    public static void resetSocialPageViewCount()
    {
        socialPageViewCount.set(0);
    } // resetSocialPageViewCount

    public static boolean isSocialPage()
    {
        return isSocialPage;
    } //isSocialPage

    public static void setSocialPage(boolean isSocialPage)
    {
        Metric.isSocialPage = isSocialPage;
    } //setSocialPage

    public static void updateUserPostCount(String userid)
    {

        if(StringUtils.isNotEmpty(userid))
        {
            if(!userPostCount.containsKey(userid))
            {
                userPostCount.put(userid, 1);
            }
            else
            {
                int count = userPostCount.get(userid);
                count = count + 1;
                userPostCount.put(userid, count);
            }
        }
    }

    public static void updateUserCommentCount(String userid)
    {
        if(StringUtils.isNotEmpty(userid))
        {
            if(!userCommentCount.containsKey(userid))
            {
                userCommentCount.put(userid, 1);
            }
            else
            {
                int count = userCommentCount.get(userid);
                count = count + 1;
                userCommentCount.put(userid, count);
            }
        }
    }

    public static void updateUserLikeCount(String userid)
    {
        if(StringUtils.isNotEmpty(userid))
        {
            if(!userLikeCount.containsKey(userid))
            {
                userLikeCount.put(userid, 1);
            }
            else
            {
                int count = userLikeCount.get(userid);
                count = count + 1;
                userLikeCount.put(userid, count);
            }
        }
    }

    public static void mergeAllMaps()
    {
        Map<String, Integer> mappost = new HashMap<String, Integer>();
        mappost.putAll(userPostCount);
        Map<String, Integer> mapcomment = new HashMap<String, Integer>();
        mapcomment.putAll(userCommentCount);
        Map<String, Integer> maplike = new HashMap<String, Integer>();
        maplike.putAll(userLikeCount);

        alldata.clear();

        alldata.put(USER_POST, mappost);
        alldata.put(USER_COMMENT, mapcomment);
        alldata.put(USER_LIKE, maplike);

        userPostCount.clear();
        userCommentCount.clear();
        userLikeCount.clear();
    }

} // Metric
