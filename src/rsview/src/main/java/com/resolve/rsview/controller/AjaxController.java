/******************************************************************************
* (C) Copyright 2014
*
* Resolve Systems, LLC
*
* All rights reserved
* This software is distributed under license and may not be used, copied,
* modified, or distributed without the express written permission of
* Resolve Systems, LLC.
*
******************************************************************************/

package com.resolve.rsview.controller;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.resolve.rsview.main.RSContext;
import com.resolve.services.constants.ConstantValues;
import com.resolve.util.Log;
import com.resolve.wiki.api.WebApi;


/**
 * This controller serves the Ajax calls from the UI
 * 
 * @deprecated
 * 
 * @author jeet.marwah
 *
 */
@Controller
public class AjaxController
{
//    private static int counter = 0;
    
    /**
     * @deprecated
     * 
     * Not used in Resolve 5.0
     * 
     * returns the html that gets generated using the {result} tag on the wiki page
     * There are 2 helper classes for this tag - ResultMacro.java, ResultAjax.java
     * 
     * Ext.Ajax.request({
                   url: '/resolve/service/result/rpc/html',
                   params: {
            serverUrl : 'http://localhost:8080',
            problemId : (problemIdEl && problemIdEl.innerHTML) ? problemIdEl.innerHTML : '',
            textHideExpand : 'Show / Hide Results',
            encodeSummary : '',
            currentUser : 'admin',
            //documentName : 'Runbook.WebHome',
            type : '', //valid values are : task, <empty>
            refreshStatus : '',
            refreshInterval : '5',//if empty, will be default to 5
            refreshStartSummary : '',
            refreshStartWiki : '',
            refreshStopSummary : '',
            refreshStopWiki : '',
            endstatus : '',
            divStyle : '',
            isDoneYetId : 'isDoneYetId_349967237537',
            isOrderByAsc : 'true',
            actionNamespace : [],
            actionName : [],
            nodeId : [],
            actionWiki : [],
            description : []
                   },
                   success: function(response){
                       var text;
                       try {
                           text = response.responseText;
                           messageEl.update(text);
                           messageEl.show();
                           if(text.indexOf('STOP_REFRESH') > -1){
                               Ext.TaskManager.stop(runnerresults_64886271058);
                           }
                       }
                       catch(e) {                      
                       }
                   },
                   failure: function(response){
                           if(typeof(resultFailureAlerted)=='undefined')resultFailureAlerted = false;
                           if(!resultFailureAlerted){
                               resultFailureAlerted = true;
                               Ext.Msg.alert('Error', 'There was an error communicating with the server.');
                           }
                   },
                   scope: this
               });
     * 
     * 
     * @param request
     * @param response
     * @return
     * @throws ServletException
     * @throws IOException
     */
    @RequestMapping(value = ConstantValues.URL_RESULT_RPC_HTML, method = {RequestMethod.POST, RequestMethod.GET} )
    @ResponseBody
    public String getResultWikiTagDataHTML(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException
    {
        response.setContentType("application/xml");

        String html = "";
        try
        {
            WebApi webApi = (WebApi) RSContext.appContext.getBean(ConstantValues.WIKI_SPRINGID_FOR_WEBAPI); 
            html = webApi.getHtmlForResultWikiTag(request, response);
//          html = getResultXMLData();//for testing
        }
        catch (Exception e)
        {
            Log.log.error(e.getMessage(), e);
        }
        
        return  html;
    }//getResultWikiTagDataHTML
    
    
    /**
     * 
     * @deprecated
     * Not used in Resolve 5.0
     * 
     * returns the html for the {detail} tag
     * 2 helper classes - DetailMacro.java, DetailAjax.java
     * 
     * Ext.Ajax.request({
                   url: '/resolve/service/detail/rpc/html',
                   params: {
            serverUrl : 'http://localhost:8080',
            problemId : (problemIdEl && problemIdEl.innerHTML) ? problemIdEl.innerHTML : '',
            currentUser : 'admin',
            documentName : 'Runbook.WebHome',
            type : '',
            separator : '',
            max : '',
            offset : '',
            refreshStatus : '',
            refreshInterval : '5',
            refreshStartSummary : '',
            refreshStartWiki : '',
            refreshStopSummary : '',
            refreshStopWiki : '',
            isDoneYetId : 'isDoneYetId_238799146256',
            actionNamespace : [],
            actionName : [],
            nodeId : []
                   },
                   success: function(response){
                       var text;
                       try {
                           text = response.responseText;
                           messageEl.update(text);
                           messageEl.show();
                           if(text.indexOf('STOP_REFRESH') > -1){
                               Ext.TaskManager.stop(runnerdetails_735002311195);
                           }
                       }
                       catch(e) {                      
                       }
                   },
                   failure: function(response){
                           if(typeof(resultFailureAlerted)=='undefined')resultFailureAlerted = false;
                           if(!resultFailureAlerted){
                               resultFailureAlerted = true;
                               Ext.Msg.alert('Error', 'There was an error communicating with the server.');
                           }
                   },
                   scope: this
               });
     * 
     * @param request
     * @param response
     * @return
     * @throws ServletException
     * @throws IOException
     */
    @RequestMapping(value = ConstantValues.URL_DETAIL_RPC_HTML, method = {RequestMethod.POST, RequestMethod.GET} )
    @ResponseBody
    public String getDetailWikiTagDataHTML(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException
    {
        response.setContentType("application/xml");

        String html = "";
        try
        {
            WebApi webApi = (WebApi) RSContext.appContext.getBean(ConstantValues.WIKI_SPRINGID_FOR_WEBAPI); 
            html = webApi.getHtmlForDetailWikiTag(request, response);
//          html = getResultXMLData();//for testing
        }
        catch (Exception e)
        {
            Log.log.error(e.getMessage(), e);
        }
        
        return  html;
    }//getResultWikiTagDataHTML
    
    
//    //TEST DATA
//    private String getResultXMLData()
//    {
//        String xml =
//            "<a href=\"javascript:showhide('wiki_results:nc_itsm_EventUpdateTicketStatus')\">"+
//            "    <img border=\"0\" id=\"showHideImg\" src=\"/resolve/images/expandall.gif\" />"+
//            "</a> Show / Hide Results&nbsp;"+
//            "<div id=\"wiki_results:nc_itsm_EventUpdateTicketStatus\" style=\"\" >"+
//            "<input type='hidden' name='isItDoneYet' value='"+ (counter++ % 5 == 0 ? "DONE" : counter) +"'>" +
//             "   <table width=\"100%\" >"+
//              "      <tr class=\"header\" >"+
//               "         <td style=\"padding-left: 5px; padding-right: 5px;font-weight: bold;\" >Description</td>"+
//                "        <td style=\"padding-left: 5px; padding-right: 5px;font-weight: bold;\" >Summary</td>"+
//                 "       <td style=\"padding-left: 5px; padding-right: 5px;font-weight: bold;\" >Result</td>"+
//                  "  </tr>"+
//                   " <tr class=\"odd\" valign=\"top\">"+
//                    "    <td style=\"null\" >updates event with incident information</td>"+
//                     "   <td style=\"null\" >java.lang.NumberFormatException: For input string: &quot;&quot;</td>"+
//                      "  <td style=\"text-align: center; width: 80px;background-color: lightgreen;\" >"+
//                       "     <a style=\"text-decoration: underline; color: black;\" onClick ='gotoURL(\"http://localhost:8080/resolve/service/result/task/detail?TASKRESULTID=2c9181e52ea17e8c012ea193b2e000d2\", \"http://localhost:8080/resolve/service/result/task/detail?TASKRESULTID=2c9181e52ea17e8c012ea193b2e000d2\")'>GOOD</a>"+
//                        "</td>"+
//                    "</tr>"+
//                "</table>"+
//            "</div>";
//
//        return xml;
//    }
} // AjaxController
