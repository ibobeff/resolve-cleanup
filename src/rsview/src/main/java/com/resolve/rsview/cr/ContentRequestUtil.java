/******************************************************************************
* (C) Copyright 2014
*
* Resolve Systems, LLC
*
* All rights reserved
* This software is distributed under license and may not be used, copied,
* modified, or distributed without the express written permission of
* Resolve Systems, LLC.
*
******************************************************************************/

package com.resolve.rsview.cr;

import java.util.HashMap;
import java.util.Map;

import org.jsoup.Jsoup;

import com.resolve.rsview.main.MAction;
import com.resolve.services.constants.ConstantValues;
import com.resolve.util.constants.HibernateConstants;
import com.resolve.util.Constants;
import com.resolve.util.StringUtils;

public class ContentRequestUtil
{
    public static final String CONTENT_REQUEST = "content_request";
    
    public static void submitRequest(Map params)
    {
        String username = (String) params.get(Constants.HTTP_REQUEST_USERNAME);
        String srcName = (String)params.get(ConstantValues.CR_SOURCE);
        if (StringUtils.isEmpty(srcName))
        {
            srcName = (String)params.get(ConstantValues.WIKI_DOCUMENT_NAME);
        }
        String dstName = (String)params.get(ConstantValues.CR_DESTINATION);
        String componentType = (String) params.get(ConstantValues.COMPONENT_TYPE);
        String type = (String) params.get(ConstantValues.CR_TYPE);
        String summary = (String) params.get(ConstantValues.CR_SUMMARY);
        String detail = (String) params.get(ConstantValues.CR_DETAIL);
        String priority = (String) params.get(ConstantValues.CR_PRIORITY);
        
        
        if (StringUtils.isEmpty(summary))
        {
            summary = componentType+" "+type+" - "+srcName;
        }
        else //this is what user had written from the UI
        {
            //remove the html from the string - no new line for the summary as its a TextField
//            summary = summary.replace("<br/>", "\n");
            summary = Jsoup.parse(summary).text();
        }
        
        
        if (StringUtils.isEmpty(detail))
        {
            detail = componentType+" "+type+" - "+srcName;
        }
        else //this is what user had written from the UI
        {
            detail = detail.replace("<br/>", "\n"); //change the <br> to \n
            detail = detail.replaceAll("\\<.*?\\>", ""); //remove the html from the string
        }
        
        // create CR
        Map cr = submitCR(summary, detail, type, componentType, priority, username);
        
        // add asset
        if (cr != null && StringUtils.isNotBlank(srcName))
        {
            String contentRequestSysId = (String)cr.get("sys_id");
            submitAsset(contentRequestSysId, srcName, dstName, componentType, username);
        }
    } // submitRequest
    
    public static void submitGroupRequest(String[] docs, String type, String username)
    {
        String summary = "Recertification Request (grouped)";
        StringBuffer detail = new StringBuffer();
        detail.append("List of expired documents:\n\n");
        for (int i = 0; i < docs.length; i++)
        {
            detail.append(docs[i]);
            detail.append("\n");
        }
        
        submitCR(summary, detail.toString(), type, Constants.CR_COMPONENT_TYPE_DOCUMENT, null, username);
    } // submitGroupRequest
        
    public static Map submitCR(String summary, String detail, String type, String componentType, String priority, String username)
    {
        Map result = null;
        
        Map params = new HashMap();
        params.put(Constants.HTTP_REQUEST_USERNAME, username);
        params.put(HibernateConstants.EXT_FORM_VIEW_NAME_KEY, "NEW_CONTENT_REQUEST");
        params.put(HibernateConstants.EXT_FORM_BUTTON_NAME_KEY, "Submit");
        params.put("u_requester", username);
        params.put("u_request_type", type);
        params.put("u_component_type", componentType);
        params.put("u_summary", summary);
        params.put("u_detail", detail);
        if (priority != null)
        {
            params.put("u_priority", priority);
        }
        
        result = new MAction().submitForm(params);
        
        return result;
    } // submitCR
        
    public static void submitAsset(String contentRequestSysId, String srcName, String dstName, String componentType, String username)
    {
        // add to asset
        Map params = new HashMap();
        params.put(Constants.HTTP_REQUEST_USERNAME, username);
        params.put(HibernateConstants.EXT_FORM_VIEW_NAME_KEY, "CR_ASSET");
        params.put(HibernateConstants.EXT_FORM_BUTTON_NAME_KEY, "Submit");
        params.put("u_content_request", contentRequestSysId);
        params.put("u_type", componentType);
        params.put("u_name", srcName);
        params.put("u_destination", dstName);
        Map asset = new MAction().submitForm(params);
        
    } // submitAsset

} // ContentRequestUtil
