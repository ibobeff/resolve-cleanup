/******************************************************************************
* (C) Copyright 2014
*
* Resolve Systems, LLC
*
* All rights reserved
* This software is distributed under license and may not be used, copied,
* modified, or distributed without the express written permission of
* Resolve Systems, LLC.
*
******************************************************************************/

package com.resolve.rsview.auth.spnego;

import java.security.Principal;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletRequestWrapper;

import org.ietf.jgss.GSSCredential;

import com.resolve.rsview.auth.spnego.SpnegoConstants;
import com.resolve.rsview.auth.DelegateServletRequest;

/**
 * Wrap ServletRequest so we can do our own handling of the principal and auth
 * types.
 * 
 * <p>
 * Also, see the documentation on the {@link DelegateServletRequest} class.
 * </p>
 * 
 * <p>
 * Finally, a credential delegation example can be found on <a
 * href="http://spnego.sourceforge.net"
 * target="_blank">http://spnego.sourceforge.net</a>
 * </p>
 * 
 * @author Darwin V. Felix
 * 
 */
public class SpnegoHttpServletRequest extends HttpServletRequestWrapper implements DelegateServletRequest
{

    /** Client Principal. */
    private final transient SpnegoPrincipal principal;

    /**
     * Creates Servlet Request specifying KerberosPrincipal of user.
     * 
     * @param request
     * @param spnegoPrincipal
     */
    public SpnegoHttpServletRequest(final HttpServletRequest request, final SpnegoPrincipal spnegoPrincipal)
    {

        super(request);

        this.principal = spnegoPrincipal;
    }

    /**
     * Returns "Negotiate" or "Basic" else default auth type.
     * 
     * @see javax.servlet.http.HttpServletRequest#getAuthType()
     */
    @Override
    public String getAuthType()
    {

        final String authType;
        final String header = this.getHeader(SpnegoConstants.AUTHZ_HEADER);

        if (header.startsWith(SpnegoConstants.NEGOTIATE_HEADER))
        {
            authType = SpnegoConstants.NEGOTIATE_HEADER;

        }
        else if (header.startsWith(SpnegoConstants.BASIC_HEADER))
        {
            authType = SpnegoConstants.BASIC_HEADER;

        }
        else
        {
            authType = super.getAuthType();
        }

        return authType;
    }

    /**
     * Return the client's/requester's delegated credential or null.
     * 
     * @return client's delegated credential or null.
     */
    public GSSCredential getDelegatedCredential()
    {
        return this.principal.getDelegatedCredential();
    }

    /**
     * Returns authenticated username (sans domain/realm) else default username.
     * 
     * @see javax.servlet.http.HttpServletRequest#getRemoteUser()
     */
    @Override
    public String getRemoteUser()
    {

        if (null == this.principal)
        {
            return super.getRemoteUser();

        }
        else
        {
            final String[] username = this.principal.getName().split("@", 2);
            return username[0];
        }
    }

    /**
     * Returns KerberosPrincipal of user.
     * 
     * @see javax.servlet.http.HttpServletRequest#getUserPrincipal()
     */
    @Override
    public Principal getUserPrincipal()
    {
        return this.principal;
    }
}
