/******************************************************************************
* (C) Copyright 2014
*
* Resolve Systems, LLC
*
* All rights reserved
* This software is distributed under license and may not be used, copied,
* modified, or distributed without the express written permission of
* Resolve Systems, LLC.
*
******************************************************************************/

package com.resolve.rsview.controller;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.codehaus.jackson.map.ObjectMapper;
import org.springframework.stereotype.Controller;
import org.springframework.stereotype.Service;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.resolve.rsview.util.ExecuteGroovyScript;
import com.resolve.services.ServiceHibernate;
import com.resolve.services.hibernate.vo.ResolveParserVO;
import com.resolve.services.vo.QueryDTO;
import com.resolve.services.vo.ResponseDTO;
import com.resolve.util.Constants;
import com.resolve.util.Log;

import net.sf.json.JSONObject;


/**
 * 
 * This controller serves the request(CRUD operations) for Action Task Parser. 
 * <p>
 * Parsers are sub-component of an Actiontask. They expect data in a certain format (which is Raw data) and parse the data that the Actiontask can work on. 
 * <p>
 * There are different types Templates like CSV, Map, List, etc available to select from and user can create their own custom Parser also.  
 * <p>
 * For more information about Parser, please refer to <a href="http://community.gen-e.com/resolve/jsp/rsclient.jsp">Resolve documentation</a> where it will explain how are they used in the execution of Actiontask. 
 * <p>
 * DB Table: {@code resolve_parser}</br>
 * Url : {@code http://localhost:8080/resolve/jsp/rsclient.jsp#RS.actiontask.ParserDefinitions/}
 * 
 * 
 * @author jeet.marwah
 *
 */
@Controller
@Service
public class AjaxActionTaskParser extends GenericController
{
    
    /**
     * Returns/Retrieves list of Parser for the grid based on filters, sorts, pagination. 
     * <p> 
     * <u><b>Example</b></u>:</br> 
     * <code><pre>
     *  filter: [{"field":"uname","type":"auto","condition":"contains","value":"resolve"}]
     *  page: 3
     *  start: 100
     *  limit: 50
     *  sort: [{"property":"sysUpdatedOn","direction":"ASC"}]
     * </pre></code>
     *  
     * <p>
     * Parameters in the Request object: 
     * <code><pre>
     *  USERNAME - username of the user who is logged in to Resolve
     * </pre></code>
     *  
     * <p>
     * Return object in response:
     * <code><pre>
     *  'records' : List of type {@link ResolveParserVO}
     *  'total' : # of total records in the table/db 
     * </pre></code>
     * 
     * 
     * Attributes populated in records:
     * <code><pre>
     *  sys_id,sysCreatedOn,sysCreatedBy,sysUpdatedOn,sysUpdatedBy,UName,UDescription
     * </pre></code>
     *      
     * @param query  {@link QueryDTO}, represents the query, pagination, sorting, grouping for the UI
     * @param request  HttpServletRequest
     * @param response  HttpServletResponse
     * @return {@link ResponseDTO} - Reponse Data Transfer Object having a List  and total # of records 
     * @throws ServletException 
     * @throws IOException
     */
    @RequestMapping(value = "/parser/list", method = { RequestMethod.GET, RequestMethod.POST }, headers = "Accept=application/json; charset=utf-8")
    @ResponseBody
    public ResponseDTO list(@ModelAttribute QueryDTO query, HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException
    {
        ResponseDTO result = new ResponseDTO();
        String username = (String) request.getAttribute(Constants.HTTP_REQUEST_USERNAME);
        
        try
        {
            query.setModelName("ResolveParser");
            query.setSelectColumns("sys_id,sysCreatedOn,sysCreatedBy,sysUpdatedOn,sysUpdatedBy,UName,UDescription");
            
            List<ResolveParserVO> data = ServiceHibernate.getResolveParser(query, username);
            int total = ServiceHibernate.getTotalHqlCount(query);
            
            result.setSuccess(true).setRecords(data);
            result.setTotal(total);
        }
        catch (Exception e)
        {
            Log.log.error("Error retrieving ResolveParser", e);
            result.setSuccess(false).setMessage("Error retrieving ResolveParser. See log for more info.");
        }
        
        return result;
    }
    
    /**
     * Get Parser by sysId
     * <p>
     * Parameters in the Request object:
     * <code><pre> 
     *  USERNAME - username of the user who is logged in to Resolve
     * </pre></code>
     * <p>
     * Return object in response 
     * <code><pre>
     *  'data' : {@link ResolveParserVO}
     * </pre></code>
     * 
     * @param id sysId of the Parser
     * @param request HttpServletRequest
     * @param response HttpServletResponse
     * @return {@link ResponseDTO} - Reponse Data Transfer Object with 'data' of type {@link ResolveParserVO}
     * @throws ServletException
     * @throws IOException
     */
    @RequestMapping(value = "/parser/get", method = { RequestMethod.POST }, headers = "Accept=application/json; charset=utf-8")
    @ResponseBody
    public ResponseDTO get(@RequestParam String id, HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException
    {
        ResponseDTO result = new ResponseDTO();
        String username = (String) request.getAttribute(Constants.HTTP_REQUEST_USERNAME);
        
        try
        {
            ResolveParserVO vo = ServiceHibernate.findResolveParser(id, null, username);
            result.setData(vo);
            result.setSuccess(true);
            
        }
        catch (Exception e)
        {
            Log.log.error("Error retrieving ResolveParser", e);
            result.setSuccess(false).setMessage("Error retrieving ResolveParser. See log for more info.");
        }

        return result;
    }
    
    /**
     * Delete the Parser based on array of sysIds
     * <p>
     * Parameters in the Request object: 
     * <code><pre>
     *  USERNAME - username of the user who is logged in to Resolve
     * </pre></code>
     *  
     * @param ids  array of sysIds to be deleted
     * @param deleteAll  true/false if want to delete all the records
     * @param validate  true/false if wanted to validate before deleteing which Actiontask does these Parser belong too.
     * @param request HttpServletRequest
     * @param response HttpServletResponse
     * @return {@link ResponseDTO} - Reponse Data Transfer Object with status as 'true' or 'false'
     * @throws ServletException
     * @throws IOException
     */
    @RequestMapping(value = "/parser/delete", method = { RequestMethod.POST }, headers = "Accept=application/json; charset=utf-8")
    @ResponseBody
    public ResponseDTO delete(@RequestParam String[] ids, @RequestParam("all") Boolean deleteAll, @RequestParam("validate") Boolean validate, HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException
    {
        ResponseDTO result = new ResponseDTO();
        String username = (String) request.getAttribute(Constants.HTTP_REQUEST_USERNAME);
        
        try
        {
            if(validate)
            {
                //get the list of actiontask names to be displayed on the UI in the warning message
                Set<String> parserSysIds = new HashSet<String>(Arrays.asList(ids));
                Set<String> actiontaskNames = ServiceHibernate.findActiontaskReferencesForParser(parserSysIds, username);
                if (actiontaskNames.size() > 0)
                {
                    result.setRecords(new ArrayList<String>(actiontaskNames));
                }
                else
                {
                    ServiceHibernate.deleteResolveParserByIds(ids, deleteAll, username);
                }
            }
            else
            {
                ServiceHibernate.deleteResolveParserByIds(ids, deleteAll, username);
            }
            result.setSuccess(true);
        }
        catch (Exception e)
        {
            Log.log.error("Error deleting ResolveParser", e);
            result.setSuccess(false).setMessage("Error deleting ResolveParser. See log for more info.");
        }

        return result;
    }

    /**
     * 
     * Saves/Update a Parser. If 'sysId' is set, it will be update else it will create it. 
     * <p>
     * Parameters in the Request object: 
     * <code><pre>
     *  USERNAME - username of the user who is logged in to Resolve
     * </pre></code>
     * <p>
     * Return object in response 
     * <code><pre>
     *  'data' : {@link ResolveParserVO}
     * </pre></code>
     *        
     * @param jsonProperty JSONObject/JSON having data of type {@link ResolveParserVO}
     * @param request HttpServletRequest
     * @param response HttpServletResponse
     * @return {@link ResponseDTO} - Reponse Data Transfer Object with 'data' having the created/updated Parser VO
     * @throws ServletException
     * @throws IOException
     */
    @RequestMapping(value = "/parser/save", method = { RequestMethod.POST }, headers = "Accept=application/json; charset=utf-8")
    @ResponseBody
    public ResponseDTO save(@RequestBody JSONObject jsonProperty, HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException
    {
      //using jackson to deserialize the json
        String json = jsonProperty.toString();
        ResolveParserVO entity = new ObjectMapper().readValue(json, ResolveParserVO.class);
        
        //convert json to VO - spring does not work as the VO are not 100% java bean
//        JsonConfig jsonConfig = new JsonConfig();
//        jsonConfig.setRootClass(ResolveParserVO.class);
//        ResolveParserVO entity = (ResolveParserVO) JSONSerializer.toJava(jsonProperty, jsonConfig);

        ResponseDTO result = new ResponseDTO();
        String username = (String) request.getAttribute(Constants.HTTP_REQUEST_USERNAME);
        
        try
        {
            ResolveParserVO prop = ServiceHibernate.saveResolveParser(entity, username);
            result.setSuccess(true).setData(prop);
        }
        catch (Exception e)
        {
            Log.log.error("Error saving the ResolveParser", e);
            result.setSuccess(false).setMessage("Error saving the ResolveParser. See log for more info.");
        }

        return result;
    }
    
    /**
     * This api is a test that a user can execute when writing Parser code on the UI. There are different templates that user can use on the UI and test it by passing the 
     * </br>Raw test data that the parser can expect when executed. Think of it as writing and testing the Parser.
     * <p> 
     * Return object in response 
     * <code><pre>
     *  'data' : result of the execution of the groovy script(which is a String) with data as an input
     * </pre></code>
     * 
     * @param script  groovy script to test
     * @param raw  data that the script expects 
     * @param request HttpServletRequest
     * @return {@link ResponseDTO} - Reponse Data Transfer Object having the result of execution in 'data'
     * @throws ServletException
     * @throws IOException
     */
    @RequestMapping(value = "/parser/test", method = { RequestMethod.POST }, headers = "Accept=application/json; charset=utf-8")
    @ResponseBody
    public ResponseDTO testParser(@RequestParam("script") String script, @RequestParam("rawData") String raw, HttpServletRequest request) throws ServletException, IOException
    {
      //using jackson to deserialize the json
        ResponseDTO result = new ResponseDTO();
        String username = (String) request.getAttribute(Constants.HTTP_REQUEST_USERNAME);
        
        try
        {
            String executeResult = ExecuteGroovyScript.executeScript("ParserScriptExecute", script, raw);
            result.setSuccess(true).setData(executeResult);
        }
        catch (Exception e)
        {
            Log.log.error("Error executing the parser.", e);
            result.setSuccess(false).setMessage("Error executing the parser. See log for more info.");
        }

        return result;
    } 
}
