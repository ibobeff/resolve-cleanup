package com.resolve.rsview.sso.authenticator.impl;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.resolve.rsview.sso.authenticator.SSOAuthenticationDTO;
import com.resolve.rsview.sso.authenticator.SSOAuthenticator;
import com.resolve.util.Log;

public class NullAuthenticator implements SSOAuthenticator
{
    
    @Override
    public String getSSOType()
    {
        return null;
    }

    @Override
    public SSOAuthenticationDTO authenticate(HttpServletRequest request, HttpServletResponse response)
    {
        Log.log.error("Can not authenticate due to unrecognized SAML SSO request. Clear the browser cookies and try again.");
        return null;
    }
    
}
