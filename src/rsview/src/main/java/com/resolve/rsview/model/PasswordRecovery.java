/******************************************************************************
* (C) Copyright 2014
*
* Resolve Systems, LLC
*
* All rights reserved
* This software is distributed under license and may not be used, copied,
* modified, or distributed without the express written permission of
* Resolve Systems, LLC.
*
******************************************************************************/

package com.resolve.rsview.model;

import com.resolve.util.StringUtils;


public class PasswordRecovery
{
    private String email = "";
    private String userName = "";
    private String question = "";
    private String answer = "";

    private String message;
    private String showMessage = "none";

    private String submit = "Next";

    private String displayAnswer = "none";

    private String done = "";
    
    private String url;
    
    private String iframeId;

    public PasswordRecovery()
    {

    }

    public String getEmail()
    {
        return email;
    }

    public void setEmail(String email)
    {
        this.email = email;
    }

    public String getUserName()
    {
        return userName;
    }

    public void setUserName(String userName)
    {
        this.userName = userName;
    }

    public String getMessage()
    {
        return message;
    }

    public void setMessage(String message)
    {
        this.message = "<font style=\"color:red;font-weight:bold;\">" + message + "</font>";
        if (StringUtils.isNotBlank(message))
        {
            this.setShowMessage("block");
        }
        else
        {
            this.setShowMessage("none");
        }
    }

    public String getQuestion()
    {
        return question;
    }

    public void setQuestion(String question)
    {
        this.question = question;
    }

    public String getAnswer()
    {
        return answer;
    }

    public void setAnswer(String answer)
    {
        this.answer = answer;
    }

    public String getSubmit()
    {
        return submit;
    }

    public void setSubmit(String submit)
    {
        this.submit = submit;
    }

    public String getDisplayAnswer()
    {
        return displayAnswer;
    }

    public void setDisplayAnswer(boolean displayAnswer)
    {
        if (displayAnswer)
        {
            this.displayAnswer = "block";
        }
        else
        {
            this.displayAnswer = "none";
        }
    }

    public String getDone()
    {
        return done;
    }

    public void setDone(boolean done)
    {
        if (done)
        {
            this.done = "done";
        }
        else
        {
            this.done = "";
        }
    }

    public String getShowMessage()
    {
        return showMessage;
    }

    public void setShowMessage(String showMessage)
    {
        this.showMessage = showMessage;
    }
    
    public String getUrl()
    {
        return url;
    }

    public void setUrl(String url)
    {
        this.url = url;
    }
    
    public String getIframeId()
    {
        return iframeId;
    }
    
    public void setIframeId(String iframeId)
    {
        this.iframeId = iframeId;
    }
}
