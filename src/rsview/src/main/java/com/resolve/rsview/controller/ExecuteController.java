/******************************************************************************
* (C) Copyright 2014
*
* Resolve Systems, LLC
*
* All rights reserved
* This software is distributed under license and may not be used, copied,
* modified, or distributed without the express written permission of
* Resolve Systems, LLC.
*
******************************************************************************/

package com.resolve.rsview.controller;

import java.io.IOException;
import java.util.HashMap;
import java.util.Map;
import java.util.Collection;


import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.http.HttpStatus;
import org.apache.http.entity.ContentType;
import org.owasp.esapi.ESAPI;
import org.owasp.esapi.errors.AccessControlException;
import org.owasp.esapi.errors.IntrusionException;
import org.owasp.esapi.errors.ValidationException;
import org.owasp.esapi.reference.DefaultHTTPUtilities;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.resolve.html.Html;
import com.resolve.rsview.auth.Authentication;
import com.resolve.rsview.main.Main;
import com.resolve.rsview.util.ControllerUtil;
import com.resolve.services.ServiceHibernate;
import com.resolve.services.hibernate.util.ExecuteActionTask;
import com.resolve.services.hibernate.util.ExecuteRunbook;
import com.resolve.services.hibernate.vo.OrgsVO;
import com.resolve.services.hibernate.vo.UsersVO;
import com.resolve.util.Constants;
import com.resolve.util.HttpUtil;
import com.resolve.util.Log;
import com.resolve.util.StringUtils;
import com.resolve.services.hibernate.vo.ResolveActionParameterVO;
import com.resolve.services.hibernate.vo.ResolveActionTaskVO;

/**
 * This controller is for the execution of the Runbook/ActionTask
 * <p>
 * <u><b>List of KEYs that Runbook/ActionTask looks for when EXECUTED:</b></u>
 * <code><pre>
 * USERID --> user executing the task/runbook. This is MANDATORY
 * WIKI --> fullname of the wiki document/runbook that needs to be executed. Based on ACTION parameter, this will be mandatory 
 * ACTIONNAME --> name of the ActionTask that needs to be executed.Based on ACTION parameter, this will be mandatory 
 * ACTION --> EXECUTEPROCESS/PROCESS for Runbook and EXECUTETASK/TASK for ActionTask. This is MANDATORY 
 * PROBLEMID --> Default will CURRENT Active worksheet. set to NEW for new worksheet , can also have the WORKSHEET SYS_ID for a specific worksheet 
 * PROCESSID --> Default is CREATE, else can be NULL or ResolveProcessRequest Sys_Id
 * REDIRECT --> redirect URL after the execution of Runbook 
 * QUERYSTRING --> parameters that will be passed to REDIRECT URL
 * </pre></code>
 * 
 * <p>
 * <u><b>Example to execute using URL</b></u>
 * http://127.0.0.1:8080/resolve/service/execute?_username_=admin&_password_=resolve&WIKI=Runbook.SampleRunbook&PROBLEMID=NEW&REDIRECT=%2Fresolve%2Fservice%2Fwiki%2Fview%2F
 * Test%2Ftest1&PROCESSID=CREATE&ipaddress=172.16.5.205&QUERYSTRING=PROBLEMID%3DNEW%26_username_%3Dadmin%26_password_%3Dresolve&ACTION=EXECUTEPROCESS&USERID=admin
 * 
 * @author jeet.marwah
 * 
 */
@Controller
public class ExecuteController extends GenericController
{
	private static final String USER_MODIFIED_ENCRYPTED_PARAMETER_PREFIX = "_usr:";
    
    /**
     * This api submit's a request to abort a Runbook that may be currently executing.
     * <p>
     * <u><b>Mandatory Parameters:</b></u>:</br> 
     * <code><pre>
     * PROCESSID or PROBLEMID
     * USERID
     * </pre></code>
     * 
     * <p>
     * <u><b>Optional Parameters:</b></u>:</br>
     * <code><pre>
     * WIKI 
     * SUMMARY
     * DETAIL
     * EVENT_REFERENCE
     * PROBLEM_NUMBER
     * </pre></code>
     * 
     * @param request  HttpServletRequest
     * @param response  HttpServletResponse
     * @return message with ProcessId or ProblemId
     * @throws ServletException
     * @throws IOException
     */
    @RequestMapping(value = "/runbook/abort", method = { RequestMethod.GET, RequestMethod.POST })
    @ResponseBody
    public String abortRunbook(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException
    {
        Map<String, String> params = ControllerUtil.getParameterMap(request);

        // for userid
        String userid = (String) request.getAttribute(Constants.HTTP_REQUEST_USERNAME);
        if (StringUtils.isEmpty(userid))
        {
            userid = params.get(Constants.HTTP_REQUEST_INTERNAL_USERNAME);
        }
        params.put(Constants.EXECUTE_USERID, userid);

        return ESAPI.encoder().encodeForHTML(ExecuteRunbook.abortProcess(params));
    }// abortRunbook

    /**
     * This endpoint is to submit a request to Execute a Runbook and return it. It is Asynchronous in nature and will not return the end result of the execution.
     * <p>
     * <u><b>Mandatory Parameters:</b></u>:</br> 
     * <code><pre>
     * WIKI
     * USERID
     * </pre></code>
     * 
     * <p>
     * <u><b>Optional Parameters:</b></u></br>
     * <code><pre>
     * 
     * </pre></code>
     * 
     * <u><b>Example:</b></u>
     * <code><pre>
     * http://localhost:8080/resolve/service/runbook/start?WIKI=Runbook.Document%20One
     * </pre></code>
     * 
     * @param request  HttpServletRequest
     * @param response  HttpServletResponse
     * @return
     * @throws ServletException
     * @throws IOException
     */
    @RequestMapping(value = "/runbook/start", method = { RequestMethod.GET, RequestMethod.POST })
    @ResponseBody
    public String runbookStart(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException
    {
        Map<String, Object> params = new HashMap<String, Object>();
        params.putAll(ControllerUtil.getParameterMap(request));
        params.put(Constants.HTTP_REQUEST_ACTION, "EXECUTEPROCESS");

        // for userid
        String userid = (String) request.getAttribute(Constants.HTTP_REQUEST_USERNAME);
        if (StringUtils.isEmpty(userid))
        {
            userid = (String) params.get(Constants.HTTP_REQUEST_INTERNAL_USERNAME);
        }
        params.put(Constants.EXECUTE_USERID, userid);
        String resp = ExecuteRunbook.start(params);
        try
        {
            resp = ESAPI.validator().getValidInput("Invalid return value ", resp, "ResolveText", 400000, true);
        }
        catch (Exception e)
        {
            Log.log.error(e.getMessage(),e);
            resp = "Failed to generate correct result"; 
        }
        return resp;
//        return ExecuteRunbook.start(params);
    }// runbookStart

    
    /**
     * 
     * This endpoint is to Execute a Runbook and return the results. It is Synchronous in nature and will return the end result of the execution.
     * <p>
     * <u><b>Mandatory Parameters:</b></u>:</br> 
     * <code><pre>
     * WIKI
     * USERID
     * </pre></code>
     * 
     * <p>
     * <u><b>Optional Parameters:</b></u></br>
     * <code><pre>
     * 
     * </pre></code>
     * 
     * <u><b>Example:</b></u>
     * <code><pre>
     * http://localhost:8080/resolve/service/runbook/execute?WIKI=Runbook.Document%20One
     * </pre></code>
     * 
     * @param request  HttpServletRequest
     * @param response  HttpServletResponse
     * @return
     * @throws ServletException
     * @throws IOException
     * @throws IntrusionException 
     * @throws ValidationException 
     */
    @RequestMapping(value = "/runbook/execute", method = { RequestMethod.GET, RequestMethod.POST })
    @ResponseBody
    public String runbookExecute(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException, ValidationException, IntrusionException
    {
        
        Map<String, Object> params = new HashMap<String, Object>();
        params.putAll(ControllerUtil.getParameterMap(request));
        params.put(Constants.HTTP_REQUEST_ACTION, "EXECUTEPROCESS");

        // for userid
        String userid = (String) request.getAttribute(Constants.HTTP_REQUEST_USERNAME);
        if (StringUtils.isEmpty(userid))
        {
            userid = (String) params.get(Constants.HTTP_REQUEST_INTERNAL_USERNAME);
        }
        params.put(Constants.EXECUTE_USERID, userid);

        String result = ExecuteRunbook.execute(params);
        response.setContentType(ContentType.APPLICATION_JSON.toString());
        // change from getValidSafeHTML() to getValidInput because results from runbook execution is always a json not html.
		// getValidSafeHTML() alter result json in unexpected way which break client result handling code and in some case 
		// break json format itself
		return  ESAPI.validator().getValidInput("runbook execute ", result, "ResolveText", 65536, true, false);
    }// runbookExecute

    
    /**
     * 
     * This endpoint is to submit a request to Execute a Actiontask and return it. It is Asynchronous in nature and will not return the end result of the execution.
     * <p>
     * <u><b>Mandatory Parameters:</b></u>:</br> 
     * <code><pre>
     * ACTIONNAME
     * USERID
     * </pre></code>
     * 
     * <p>
     * <u><b>Optional Parameters:</b></u></br>
     * <code><pre>
     * PROBLEMID
     * </pre></code>
     * 
     * <u><b>Example:</b></u>
     * <code><pre>
     * http://localhost:8080/resolve/service/actiontask/start?ACTIONNAME=comment%23resolve&PROBLEMID=NEW
     * </pre></code>
     * 
     * @param request  HttpServletRequest
     * @param response  HttpServletResponse
     * @return
     * @throws ServletException
     * @throws IOException
     */
    @RequestMapping(value = "/actiontask/start", method = { RequestMethod.GET, RequestMethod.POST })
    @ResponseBody
    public String actiontaskStart(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException
    {
        Map<String, Object> params = new HashMap<String, Object>();
        params.putAll(ControllerUtil.getParameterMap(request));
        params.put(Constants.HTTP_REQUEST_ACTION, "EXECUTETASK");

        // for userid
        String userid = (String) request.getAttribute(Constants.HTTP_REQUEST_USERNAME);
        if (StringUtils.isEmpty(userid))
        {
            userid = (String) params.get(Constants.HTTP_REQUEST_INTERNAL_USERNAME);
        }
        //params.put(Constants.EXECUTE_USERID, userid);
        params.put(Constants.RESOLVE_INSTERNAL_EXECUTE_USERID, userid);
        String resp = ExecuteActionTask.start(params);
        try
        {
            resp = ESAPI.validator().getValidInput("Invalid return value ", resp, "ResolveText", 400000, true);
        }
        catch (Exception e)
        {
            Log.log.error(e.getMessage(),e);
            resp = "Failed to generate correct result"; 
        }
        return resp;

//        return ESAPI.encoder().encodeForHTML(ExecuteActionTask.start(params));
    }// actiontaskStart

    
    /**
     * 
     * 
     * This endpoint is to Execute a Actiontask and return it. It is Synchronous in nature and will not return the end result of the execution.
     * <p>
     * <u><b>Mandatory Parameters:</b></u>:</br> 
     * <code><pre>
     * ACTIONNAME
     * USERID
     * </pre></code>
     * 
     * <p>
     * <u><b>Optional Parameters:</b></u></br>
     * <code><pre>
     * PROBLEMID
     * </pre></code>
     * 
     * <u><b>Example:</b></u>
     * <code><pre>
     * http://localhost:8080/resolve/service/actiontask/execute?ACTIONNAME=comment%23resolve&PROBLEMID=NEW
     * </pre></code>
     * 
     * @param request  HttpServletRequest
     * @param response  HttpServletResponse
     * @return
     * @throws ServletException
     * @throws IOException
     */
    @RequestMapping(value = "/actiontask/execute", method = { RequestMethod.GET, RequestMethod.POST })
    @ResponseBody
    public String actiontaskExecute(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException
    {
        Map<String, Object> params = new HashMap<String, Object>();
        params.putAll(ControllerUtil.getParameterMap(request));
        params.put(Constants.HTTP_REQUEST_ACTION, "EXECUTETASK");

        // for userid
        String userid = (String) request.getAttribute(Constants.HTTP_REQUEST_USERNAME);
        if (StringUtils.isEmpty(userid))
        {
            userid = (String) params.get(Constants.HTTP_REQUEST_INTERNAL_USERNAME);
        }
//        params.put(Constants.EXECUTE_USERID, userid);
        params.put(Constants.RESOLVE_INSTERNAL_EXECUTE_USERID, userid);

        return ExecuteActionTask.execute(params);
    }// actiontaskExecute

    
    /**
     * @deprecated
     * 
     * @param request  HttpServletRequest
     * @param response  HttpServletResponse
     * @return
     * @throws ServletException
     * @throws IOException
     */
    @RequestMapping(value = "/actiontask/result", method = { RequestMethod.GET, RequestMethod.POST })
    @ResponseBody
    public String actiontaskResult(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException
    {
        String result = "";

        Map<String, String> params = ControllerUtil.getParameterMap(request);
        params.put(Constants.HTTP_REQUEST_ACTION, "EXECUTERESULT");

        // for userid
        String userid = (String) request.getAttribute(Constants.HTTP_REQUEST_USERNAME);
        if (StringUtils.isEmpty(userid))
        {
            userid = params.get(Constants.HTTP_REQUEST_INTERNAL_USERNAME);
        }
        params.put(Constants.EXECUTE_USERID, userid);

        // get redirect
        String redirect = (String) request.getAttribute(Constants.HTTP_REQUEST_REDIRECT);

        // parameters that user want to pass - limit in IE is about 2048 chars
        String userQueryString = params.get(Constants.HTTP_REQUEST_QUERYSTRING);

        // null indicates no error
        result = ExecuteActionTask.result(params);
        if (result == null)
        {
            if (redirect != null)
            {
                if (!StringUtils.isEmpty(userQueryString))
                {
                    if (redirect.indexOf('?') != -1)
                    {
                        redirect += "&" + userQueryString;
                    }
                    else
                    {
                        redirect += "?" + userQueryString;
                    }
                }
                redirect = StringUtils.removeNewLineAndCarriageReturn(redirect);
                response = HttpUtil.sanitizeResponse(response);
                DefaultHTTPUtilities dhttp = new DefaultHTTPUtilities();
                try {
                    dhttp.sendRedirect(response, redirect);   
                } catch(AccessControlException  ace) {
                    Log.log.error(ace.getMessage(), ace);
                }
            }

            result = "Form submitted successfully";
        }

        return result;
    }// actiontaskResult

    
    /**
     * This is to submit an event through a Url for a Runbook that is executing and waiting for an event so that it can continue execution. Refer to RSQA also for the test cases for event.
     * <p>
     * <u><b>Here is an Example:</b></u>
     * This example uses a Runbook from the RSQA package. Goto 'RSQA.Test Event 8' Runbook and click on the 'Execute' button. This will start the 
     * execution of the runbook and will wait for an event to occur. You can verify that by viewing the Worksheet grid.
     * <p>
     * Now use this URL to send that event through the browser.
     * <code><pre>
     * http://localhost:8080/resolve/service/event/execute?PROBLEMID=LOOKUP&REFERENCE=xyz&EVENT_EVENTID=789&EVENT_REFERENCE=def&EVENT_START=true&EVENT_END=true&WIKI=RSQA.Test%20Event%208
     * </code></pre>
     * <p>
     * This step is similar to going to runbook 'RSQA.Test Event 7' and clicking the 'Execute' button which sends the event to continue the execution.
     * <p>
     * Go back to Worksheet and check that the execution is complete. 
     * 
     * 
     * @param request  HttpServletRequest
     * @param response  HttpServletResponse
     * @return
     * @throws ServletException
     * @throws IOException
     */
    @RequestMapping(value = "/event/execute", method = { RequestMethod.GET, RequestMethod.POST })
    @ResponseBody
    public String eventExecute(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException
    {
        String result = "";

        try
        {
            Map<String, String> params = ControllerUtil.getParameterMap(request);
            String redirect = params.get(Constants.HTTP_REQUEST_REDIRECT);
            String ajaxCall = params.get(Constants.AJAXCALL);
            boolean isAjaxCall = StringUtils.isNotEmpty(ajaxCall) && ajaxCall.equalsIgnoreCase("true") ? true : false;

            String queryString = request.getQueryString();
            params.put(Constants.HTTP_REQUEST_QUERY, queryString);

            // get userid
            String userid = (String) request.getAttribute(Constants.HTTP_REQUEST_USERNAME);
            if (StringUtils.isEmpty(userid))
            {
                userid = params.get(Constants.HTTP_REQUEST_INTERNAL_USERNAME);
            }
            params.put(Constants.EXECUTE_USERID, userid);

            Main.esb.sendEvent(params);
            Html html = new Html();
            html.println("Event submitted successfully");
            html.println();
            html.button("Close Window", Html.URL_CLOSE, Html.COLOR_GREY);
            
            result = html.toString();

            // update redirect
            if (redirect != null && !isAjaxCall)
            {
                if (!StringUtils.isEmpty(queryString))
                {
                    if (redirect.indexOf('?') != -1)
                    {
                        redirect += "&" + queryString;
                    }
                    else
                    {
                        redirect += "?" + queryString;
                    }
                }
                response = HttpUtil.sanitizeResponse(response);
                DefaultHTTPUtilities dhttp = new DefaultHTTPUtilities();
                try {
                    dhttp.sendRedirect(response, redirect);   
                } catch(AccessControlException  ace) {
                    Log.log.error(ace.getMessage(), ace);
                }
            }

            if (isAjaxCall)
            {
                // create a json
                result = "{ \"success\": true}"; //, \"problemId\": \"" + problemid + "\"}";
            }
        }
        catch (Exception e)
        {
            Log.log.error(e.getMessage(), e);
        }

        return result;
    }// eventExecute

    /**
     * 
     * This endpoint is for executing a system script.
     * 
     * <p>
     * <u><b>Mandatory Parameters:</b></u>:</br> 
     * <code><pre>
     * NAME - name of the system script
     * USERNAME
     * </pre></code>
     * 
     * <p>
     * <u><b>Optional Parameters:</b></u>:</br>
     * <code><pre>
     * REDIRECT - url to be redirected to after the execution 
     * AJAXCALL - true/false indicating if its a request is an Ajax call
     * </pre></code> 
     * 
     * <u><b>Example:</b></u>
     * <code><pre>
     * http://localhost:8080/resolve/service/sysscript/execute?NAME=CR%20Check%20Assignee
     * </code></pre>
     * 
     * @param request  HttpServletRequest
     * @param response  HttpServletResponse
     * @return
     * @throws ServletException
     * @throws IOException
     */
    @RequestMapping(value = "/sysscript/execute", method = { RequestMethod.GET, RequestMethod.POST })
    @ResponseBody
    public String sysScriptExecute(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException
    {
        String result = "";

        try
        {
            Map params = new HashMap(ControllerUtil.getParameterMap(request));
            // get script name
            String name = (String) params.get(Constants.SYSSCRIPT_NAME);
            if (StringUtils.isNotEmpty(name))
            {

                String redirect = (String) params.get(Constants.HTTP_REQUEST_REDIRECT);
                String ajaxCall = (String) params.get(Constants.AJAXCALL);
                boolean isAjaxCall = StringUtils.isNotEmpty(ajaxCall) && ajaxCall.equalsIgnoreCase("true") ? true : false;

                String queryString = request.getQueryString();
                params.put(Constants.HTTP_REQUEST_QUERY, queryString);

                // get userid
                String userid = (String) request.getAttribute(Constants.HTTP_REQUEST_USERNAME);
                if (StringUtils.isEmpty(userid))
                {
                    userid = (String) params.get(Constants.HTTP_REQUEST_INTERNAL_USERNAME);
                }

                // params
                params.put(Constants.EXECUTE_USERID, userid);
                params.put(Constants.GROOVY_BINDING_REQUEST, request);
                params.put(Constants.GROOVY_BINDING_RESPONSE, response);

                result = ServiceHibernate.executeScript(name, params);

                // update redirect
                if (redirect != null && !isAjaxCall)
                {
                    if (!StringUtils.isEmpty(queryString))
                    {
                        if (redirect.indexOf('?') != -1)
                        {
                            redirect += "&" + queryString;
                        }
                        else
                        {
                            redirect += "?" + queryString;
                        }
                    }
                    response = HttpUtil.sanitizeResponse(response);
                    DefaultHTTPUtilities dhttp = new DefaultHTTPUtilities();
                    try {
                        dhttp.sendRedirect(response, redirect);   
                    } catch(AccessControlException  ace) {
                        Log.log.error(ace.getMessage(), ace);
                    }
                }

                if (isAjaxCall)
                {
                    // create a json
                    // result = "{ \"success\": true, \"problemId\": \"" + problemid + "\"}";
                }
            }
        }
        catch (Exception e)
        {
            Log.log.error(e.getMessage(), e);
        }

        return result;
    }// eventExecute

    /**
     * <p>
     * This is a different version of AjaxExecuteController.execute api with all the parameters in the Request object rather than the JSON format.
     * <p>
     * <u><b>Example:</b></u>
     * <code><pre>
     * http://127.0.0.1:8080/resolve/service/execute?_username_=admin&_password_=resolve&WIKI=Runbook.SampleRunbook&PROBLEMID=NEW&REDIRECT=%2Fresolve%2Fservice%2Fwiki%2Fview%2F
     *Test%2Ftest1&PROCESSID=CREATE&ipaddress=172.16.5.205&QUERYSTRING=PROBLEMID%3DNEW%26_username_%3Dadmin%26_password_%3Dresolve&ACTION=EXECUTEPROCESS&USERID=admin
     * </code></pre>
     * 
     * @param request  HttpServletRequest
     * @param response  HttpServletResponse
     * @return
     * @throws ServletException
     * @throws IOException
     */
    @RequestMapping(value = "/execute", method = { RequestMethod.GET, RequestMethod.POST })
    @ResponseBody
    public String execute(HttpServletRequest request, HttpServletResponse response) throws Exception
    {
        String result = "";

        Map<String, Object> params = new HashMap<String, Object>();
        params.putAll(ControllerUtil.getParameterMap(request));

        String problemid = (String) params.get(Constants.HTTP_REQUEST_PROBLEMID);
        String processid = (String) params.get(Constants.HTTP_REQUEST_PROCESSID);
        
        String redirect = (String) params.get(Constants.HTTP_REQUEST_REDIRECT);
        String ajaxCall = (String) params.get(Constants.AJAXCALL);
        boolean isAjaxCall = StringUtils.isNotEmpty(ajaxCall) && ajaxCall.equalsIgnoreCase("true") ? true : false;

        // init
        String queryString = request.getQueryString();
        params.put(Constants.HTTP_REQUEST_QUERY, queryString);
        String userid = (String) request.getAttribute(Constants.HTTP_REQUEST_USERNAME);
        if (StringUtils.isEmpty(userid))
        {
            userid = (String) params.get(Constants.HTTP_REQUEST_INTERNAL_USERNAME);
        }
        params.put(Constants.EXECUTE_USERID, userid);

        try
        {
            Authentication.validate(request, response);
            
            // If params does not contain Org Name or Org Id then use users default Org if set
            
            if (!params.containsKey(Constants.EXECUTE_ORG_NAME) &&
                !params.containsKey(Constants.EXECUTE_ORG_ID) &&
                !params.containsKey(Constants.EXECUTE_org_name) &&
                !params.containsKey(Constants.EXECUTE_org_id) &&
                (params.containsKey(Constants.EXECUTE_PROBLEMID) && 
                 (((String)params.get(Constants.EXECUTE_PROBLEMID)).equalsIgnoreCase(Constants.PROBLEMID_NEW) ||
                  ((String)params.get(Constants.EXECUTE_PROBLEMID)).equalsIgnoreCase(Constants.PROBLEMID_ACTIVE) ||
                  ((String)params.get(Constants.EXECUTE_PROBLEMID)).equalsIgnoreCase(Constants.PROBLEMID_LOOKUP))))
            {
                UsersVO userVO = ServiceHibernate.getUserByNameNoCache((String)params.get(Constants.EXECUTE_USERID));
                
                if (userVO != null)
                {
                    if (userVO.getUserOrgs() != null && !userVO.getUserOrgs().isEmpty())
                    {
                        for (OrgsVO orgsVO : userVO.getUserOrgs())
                        {
                            if (orgsVO.getIsDefaultOrg().booleanValue())
                            {
                                params.put(Constants.EXECUTE_ORG_ID, orgsVO.getSys_id());
                            }
                        }
                    }
                }
            }
            
            redirect = execute(params);
            queryString = (String) params.get(Constants.HTTP_REQUEST_QUERY);
        }
        catch (Throwable e)
        {
            //throw new Exception(e);
            if (e.getMessage().contains("does not have permission to create worksheet"))
            {
                result = "Denied permission to create worksheet.";
                Log.log.warn(e.getLocalizedMessage());
            }
            else
            {
                result = "Error occurred while executing.";
                Log.log.error("Error " + e.getLocalizedMessage() + " ocurred while executing.", e);
            }
            
            response.sendError(HttpStatus.SC_BAD_REQUEST, result);
            
            return result;
        }

        // parameters that user want to pass - limit in IE is about 2048 chars
        problemid = (String) params.get(Constants.HTTP_REQUEST_PROBLEMID);
        problemid = HttpUtil.sanitizeValue(problemid);
        processid = (String) params.get(Constants.HTTP_REQUEST_PROCESSID);
        processid = HttpUtil.sanitizeValue(processid);
        if (StringUtils.isEmpty(processid))
        {
            processid = "";
        }
        String userQueryString = (String) params.get(Constants.HTTP_REQUEST_QUERYSTRING);
        if (StringUtils.isNotEmpty(userQueryString))
        {
            userQueryString += "&PROBLEMID=" + problemid+"&PROCESSID="+processid;
        }
        else
        {
            userQueryString = "PROBLEMID=" + problemid+"&PROCESSID="+processid;
        }

        if (params.containsKey(Constants.EXECUTE_ORG_ID) &&
            StringUtils.isNotBlank((String)params.get(Constants.EXECUTE_ORG_ID)))
        {
            userQueryString += "&" + Constants.EXECUTE_ORG_ID + "=" + (String)params.get(Constants.EXECUTE_ORG_ID);
        }
        
        // update redirect
        if (redirect != null && !isAjaxCall)
        {
            if (!StringUtils.isEmpty(userQueryString))
            {
                if (redirect.indexOf('?') != -1)
                {
                    redirect += "&" + userQueryString;
                }
                else
                {
                    redirect += "?" + userQueryString;
                }
            }
            redirect = StringUtils.removeNewLineAndCarriageReturn(redirect);
            DefaultHTTPUtilities dhttp = new DefaultHTTPUtilities();
            try {
                dhttp.sendRedirect(response, redirect);
            } catch(AccessControlException  ace) {
                Log.log.error(ace.getMessage(), ace);
            }
        }

        if (isAjaxCall)
        {
            // create a json
            result = "{ \"success\": true, \"problemId\": \"" + problemid + "\",\"processId\": \"" + processid +"\"}";
        }
        
        return result;
    } // execute
	private void replaceEncryptedValue(Map<String, Object> params, Collection<ResolveActionParameterVO> paramsVO) {
    	for(ResolveActionParameterVO paramVO : paramsVO) {
    		if (paramVO.getUEncrypt() != null && paramVO.getUEncrypt()) {
    			if ((!params.containsKey(paramVO.getUName()) || 
    				 StringUtils.isBlank((String)params.get(paramVO.getUName()))) &&
    				(paramVO.getURequired() != null && paramVO.getURequired())) {
    				
    				params.put(paramVO.getUName(), paramVO.getUDefaultValue());
    				
    			} else {
    				if (paramVO.getURequired() != null && paramVO.getURequired()) {
    					boolean isUserModified = StringUtils.isNotBlank((String)params.get(paramVO.getUName())) &&
    											 ((String)params.get(paramVO.getUName()))
    											 .startsWith(USER_MODIFIED_ENCRYPTED_PARAMETER_PREFIX) ? true : false;
    					
    					if (isUserModified) {
    						params.put(paramVO.getUName(), 
    								   StringUtils.substringAfter((String)params.get(paramVO.getUName()),
    										   					  USER_MODIFIED_ENCRYPTED_PARAMETER_PREFIX));
    					} else {
    						params.put(paramVO.getUName(), paramVO.getUDefaultValue());
    					}
    				} else {
    					boolean isUserModified = StringUtils.isNotBlank((String)params.get(paramVO.getUName())) &&
								 				 ((String)params.get(paramVO.getUName()))
								 				 .startsWith(USER_MODIFIED_ENCRYPTED_PARAMETER_PREFIX) ? true : false;
		
    					if (isUserModified) {
    						params.put(paramVO.getUName(), 
    								   StringUtils.substringAfter((String)params.get(paramVO.getUName()),
							   					  				  USER_MODIFIED_ENCRYPTED_PARAMETER_PREFIX));
    					}
    				}
    			}
    		}
    	}
    }

    public String execute(Map<String, Object> params) throws Exception
    {
        String wiki = (String) params.get(Constants.HTTP_REQUEST_WIKI);
        String redirectWiki = (String) params.get(Constants.HTTP_REQUEST_REDIRECT_WIKI);
        String redirect = (String) params.get(Constants.HTTP_REQUEST_REDIRECT);
        String action = (String) params.get(Constants.HTTP_REQUEST_ACTION);
        String actionid = (String) params.get(Constants.EXECUTE_ACTIONID); 
        String actionTaskName = (String) params.get(Constants.EXECUTE_ACTIONNAME);
        String username = (String) params.get(Constants.HTTP_REQUEST_USERNAME);
        if (StringUtils.isEmpty(action))
        {
            action = "EXECUTEPROCESS";
        }

        String message = null;
        if (action.equalsIgnoreCase("EXECUTEPROCESS") || action.equalsIgnoreCase("PROCESS"))
        {
            message = ExecuteRunbook.start(params);
        }
        else
        {
        	ResolveActionTaskVO atVO = ServiceHibernate.getResolveActionTask(actionid, actionTaskName, username);
        	Collection<ResolveActionParameterVO> paramsVO = atVO.getResolveActionInvoc().getResolveActionParameters();
        	if(paramsVO != null) {
        		replaceEncryptedValue(params, paramsVO);
        	};
            message = ExecuteActionTask.start(params);
        }
        
        if (StringUtils.isNotBlank(message))
        {
            if (message.toUpperCase().contains("ERROR") || message.toUpperCase().contains("FAILURE"))
            {
                throw new Exception (message);
            }
        }

        // remove the '/' at the end of the redirect url
        if (StringUtils.isNotEmpty(redirect))
        {
            redirect = redirect.trim();
            if (redirect.endsWith("/"))
            {
                redirect = redirect.substring(0, redirect.lastIndexOf("/"));
            }
        }

        // redirect to wiki with no parameter checks
        if (StringUtils.isEmpty(redirect))
        {
            if (StringUtils.isNotEmpty(redirectWiki))
            {
                redirectWiki = ESAPI.encoder().encodeForURL(redirectWiki); // Encodes URL to match with user agent
                redirect = "/resolve/service/wiki/view/" + redirectWiki.replace('.', '/');
            }
            else if (!StringUtils.isEmpty(wiki))
            {
                wiki = ESAPI.encoder().encodeForURL(wiki); // Encodes URL to match with user agent
                redirect = "/resolve/service/wiki/view/" + wiki.replace('.', '/');
            }
            else
            {
                redirect = "/resolve/service/wiki/current";
            }
        }
        else
        {
            if (redirect.trim().equals(""))
            {
                redirect = null;
            }
        }

        return redirect;
    }// execute

} // ExecuteController
