/******************************************************************************
 * (C) Copyright 2014
 *
 * Resolve Systems, LLC
 *
 * All rights reserved
 * This software is distributed under license and may not be used, copied,
 * modified, or distributed without the express written permission of
 * Resolve Systems, LLC.
 *
 ******************************************************************************/

package com.resolve.rsview.controller;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.codehaus.jackson.map.ObjectMapper;
import org.dom4j.Node;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;
import org.owasp.esapi.ESAPI;
import org.owasp.esapi.errors.AccessControlException;
import org.owasp.esapi.reference.DefaultHTTPUtilities;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import com.resolve.rsview.main.Main;
import com.resolve.rsview.main.Metric;
import com.resolve.rsview.main.RSContext;
import com.resolve.rsview.util.ControllerUtil;
import com.resolve.services.ServiceWorksheet;
import com.resolve.services.constants.ConstantValues;
import com.resolve.services.constants.WikiAction;
import com.resolve.services.hibernate.util.WikiUtils;
import com.resolve.services.util.WorksheetUtils;
import com.resolve.services.vo.ResponseDTO;
import com.resolve.util.Constants;
import com.resolve.util.HttpUtil;
import com.resolve.util.JspUtils;
import com.resolve.util.Log;
import com.resolve.util.StringUtils;
import com.resolve.util.XDoc;
import com.resolve.vo.DTMetricLogList;
import com.resolve.vo.DTMetricLogVO;
import com.resolve.vo.TMetricLogVO;
import com.resolve.wiki.api.WebApi;
import com.resolve.wiki.attachment.FileUploadUtil;
import com.resolve.wiki.rsradeox.macro.DecisionMacro;

/**
 * This controller serves the request to view and render Wiki.
 * 
 * 
 * 
 * 
 * @author jeet.marwah
 *
 */

@Controller
public class ViewController extends GenericController
{
	private static final String MISSING_WIKI_DOCUMENT = "Missing Wiki document";
	private static final String INVALID_WIKI_NAME_FORMAT = "Invalid Wiki document name format %s. Use: Namespace.Document Name (e.g., network.Health Check)";
	private static final Pattern CSS_URL_PATTERN = Pattern.compile("url(?:\\(['\"]?)(.*?)(?:['\"]?\\))");

	/**
     * This is the entry point for the request to view a wiki doc.
     * <p>
     * The naming convention for the Url is as follows:
     * {@code http://localhost:8080/resolve/service/wiki/view/<Namespace>/<Wiki Name>}
     * <p>
     * eg. {@code http://localhost:8080/resolve/service/wiki/view/Test/test2}
     * 
     * <p>
     * Parameters in the Request object:
     * <code><pre>
     *  USERNAME - username of the user who is logged in to Resolve
     * </pre></code>
     * 
     * @param namespace
     *            Namespace of the wiki document
     * @param docname
     *            Name portion of the wiki document
     * @param request
     *            HttpServletRequest
     * @param response
     *            HttpServletResponse
     * @return {@link ModelAndView} - model returning a jsp page with the rendered content
     * @throws ServletException
     * @throws IOException
     */
    @RequestMapping(value = ConstantValues.URL_VIEWDOC, method = { RequestMethod.GET, RequestMethod.POST })
    public ModelAndView view(@PathVariable(ConstantValues.WIKI_NAMESPACE_KEY) String namespace, @PathVariable(ConstantValues.WIKI_DOCNAME_KEY) String docname, HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException
    {
        Log.start("Wiki View Response Time", "debug");

        // Keep track of runbook response time
        long start = System.currentTimeMillis();

        String content = "";

        // check if the parameter has a paramter to delay the response
        sleepBasedOnParameter(request);

        // set the generic parameters
        ControllerUtil.setRequestParameters(namespace, docname, WikiAction.view, request);

        try
        {
            WebApi webApi = (WebApi) RSContext.appContext.getBean(ConstantValues.WIKI_SPRINGID_FOR_WEBAPI);
            content = webApi.viewAction(request, response);
        }
        catch (Exception e)
        {
            Log.log.error(e.getMessage(), e);
            content = "Error viewing wiki document.";
        }

		ModelAndView result = new ModelAndView(ConstantValues.WIKI_VIEW_JSP, ConstantValues.WIKI_CONTENT_KEY,
				tokenizeContent(content, request));

        // set runbookResponseTime in msecs
        long latency = System.currentTimeMillis() - start;
        Metric.wikiResponseTime.getAndAdd(latency);

        // runbook transaction metric
        Metric.wikiViewCount.incrementAndGet();

        Log.duration("Wiki View Response Time", "debug");

        return result;
    }// view

    /**
     * This api is the extension of the view api where a 'section' of the wiki document can be viewed.
     * A wiki document can have more than 1 sections in it and each 'section' can have a 'title'.
     * This url renders only the 'section' whose title is matches with this url.
     * <p>
     * {@code http://localhost:8080/resolve/service/wiki/view/<Wiki Namespace>/<Wiki Name>/<section title>}
     * <p>
     * eg. {@code http://localhost:8080/resolve/service/wiki/view/Test/test2/test}
     * 
     * <p>
     * Parameters in the Request object:
     * <code><pre>
     *  USERNAME - username of the user who is logged in to Resolve
     * </pre></code>
     * 
     * @param namespace
     *            Namespace of the wiki document
     * @param docname
     *            Name portion of the wiki document
     * @param sectionname
     *            Section name in the wiki document
     * @param request
     *            HttpServletRequest
     * @param response
     *            HttpServletResponse
     * @return {@link ModelAndView} - model returning a jsp page with the rendered content
     * @throws ServletException
     * @throws IOException
     */
    @RequestMapping(value = ConstantValues.URL_VIEWDOC_SECTION, method = { RequestMethod.GET, RequestMethod.POST })
    public ModelAndView view(@PathVariable(ConstantValues.WIKI_NAMESPACE_KEY) String namespace, @PathVariable(ConstantValues.WIKI_DOCNAME_KEY) String docname, @PathVariable(ConstantValues.WIKI_SECTION_KEY) String sectionname, HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException
    {
        Log.start("Wiki View Response Time", "debug");

        // Keep track of runbook response time
        long start = System.currentTimeMillis();

        String content = "";

        // check if the parameter has a paramter to delay the response
        sleepBasedOnParameter(request);

        // set the generic parameters
        ControllerUtil.setRequestParameters(namespace, docname, sectionname, WikiAction.view, request);

        try
        {
            WebApi webApi = (WebApi) RSContext.appContext.getBean(ConstantValues.WIKI_SPRINGID_FOR_WEBAPI);
            content = webApi.viewAction(request, response);
        }
        catch (Exception e)
        {
            Log.log.error(e.getMessage(), e);
            content = "Error while viewing a wiki section.";
        }

        ModelAndView result = new ModelAndView(ConstantValues.WIKI_VIEW_JSP, ConstantValues.WIKI_CONTENT_KEY, content);

        // set runbookResponseTime in msecs
        long latency = System.currentTimeMillis() - start;
        Metric.wikiResponseTime.getAndAdd(latency);

        // runbook transaction metric
        Metric.wikiViewCount.incrementAndGet();

        Log.duration("Wiki View Response Time", "debug");
        return result;
    }// view

    /**
     * This is another entry point to view a wiki doc.
     * <p>
     * {@code http://localhost:8080/resolve/service/wiki/view?wiki=<Wiki Namespace>.<Wiki Name>}
     * <p>
     * eg. {@code http://localhost:8080/resolve/service/wiki/view?wiki=Test.test2}
     * 
     * <p>
     * Parameters in the Request object:
     * <code><pre>
     *  USERNAME - username of the user who is logged in to Resolve
     *  wiki - name of the wiki document
     * </pre></code>
     * 
     * @param namespace
     *            Namespace of the wiki document
     * @param docname
     *            Name portion of the wiki document
     * @param request
     *            HttpServletRequest
     * @param response
     *            HttpServletResponse
     * @return {@link ModelAndView} - model returning a jsp page with the rendered content
     * @throws ServletException
     * @throws IOException
     */
	@RequestMapping(value = ConstantValues.URL_VIEW, method = RequestMethod.GET)
	public ModelAndView view(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		String wiki = request.getParameter(Constants.HTTP_REQUEST_WIKI);
		if (wiki == null) {
			wiki = request.getParameter(ConstantValues.WIKI_NAME_KEY);
			if (wiki == null) {
				throw new ServletException(MISSING_WIKI_DOCUMENT);
			}
		}

		int pos = wiki.indexOf('.');
		if (pos > 0) {
			String namespace = wiki.substring(0, pos);
			String docname = wiki.substring(pos + 1, wiki.length());
			return view(namespace, docname, request, response);
		} else {
		    return  new ModelAndView(ConstantValues.WIKI_VIEW_JSP, ConstantValues.WIKI_CONTENT_KEY, String.format(INVALID_WIKI_NAME_FORMAT, wiki));
		}
	}

    /**
     * This is a VIEW for the MODEL XML code of a Runbook (Wiki). This is a convenience for the user to copy/paste the model xml
     * of one document to another.
     * <p>
     * {@code http://localhost:8080/resolve/service/wiki/viewmodelc/<Wiki Namespace>/<Wiki Name>}
     * <p>
     * eg. {@code http://localhost:8080/resolve/service/wiki/viewmodelc/Test/test2}
     * 
     * <p>
     * Parameters in the Request object:
     * <code><pre>
     *  USERNAME - username of the user who is logged in to Resolve
     * </pre></code>
     * 
     * @param namespace
     *            Namespace of the wiki document
     * @param docname
     *            Name portion of the wiki document
     * @param request
     *            HttpServletRequest
     * @param response
     *            HttpServletResponse
     * @return {@link ModelAndView} - model returning a jsp page with the rendered content
     * @throws ServletException
     * @throws IOException
     */
    @RequestMapping(value = ConstantValues.URL_VIEWMODELC, method = RequestMethod.GET)
    public ModelAndView viewmodelc(@PathVariable(ConstantValues.WIKI_NAMESPACE_KEY) String namespace, @PathVariable(ConstantValues.WIKI_DOCNAME_KEY) String docname, HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException
    {
        String content = "";

        // set the generic parameters
        ControllerUtil.setRequestParameters(namespace, docname, WikiAction.viewmodelc, request);

        try
        {
            WebApi webApi = (WebApi) RSContext.appContext.getBean(ConstantValues.WIKI_SPRINGID_FOR_WEBAPI);
            content = webApi.viewmodelcAction(request, response);
        }
        catch (Exception e)
        {
            Log.log.error(e.getMessage(), e);
            content = "Error while viewing wiki document model.";
        }

        ModelAndView result = new ModelAndView(ConstantValues.WIKI_VIEW_JSP, ConstantValues.WIKI_CONTENT_KEY, content);
        return result;
    }

    /**
     * This is a VIEW for the EXCEPTION XML code of a Runbook (Wiki). This is a convenience for the user to copy/paste the model xml
     * of one document to another.
     * <p>
     * {@code http://localhost:8080/resolve/service/wiki/viewexcpc/<Wiki Namespace>/<Wiki Name>}
     * <p>
     * eg. {@code http://localhost:8080/resolve/service/wiki/viewexcpc/Test/test2}
     * 
     * <p>
     * Parameters in the Request object:
     * <code><pre>
     *  USERNAME - username of the user who is logged in to Resolve
     * </pre></code>
     * 
     * @param namespace
     *            Namespace of the wiki document
     * @param docname
     *            Name portion of the wiki document
     * @param request
     *            HttpServletRequest
     * @param response
     *            HttpServletResponse
     * @return {@link ModelAndView} - model returning a jsp page with the rendered content
     * @throws ServletException
     * @throws IOException
     * 
     */
    @RequestMapping(value = ConstantValues.URL_VIEWEXCPC, method = RequestMethod.GET)
    public ModelAndView viewexcpc(@PathVariable(ConstantValues.WIKI_NAMESPACE_KEY) String namespace, @PathVariable(ConstantValues.WIKI_DOCNAME_KEY) String docname, HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException
    {
        String content = "";

        // set the generic parameters
        ControllerUtil.setRequestParameters(namespace, docname, WikiAction.viewexcpc, request);

        try
        {
            WebApi webApi = (WebApi) RSContext.appContext.getBean(ConstantValues.WIKI_SPRINGID_FOR_WEBAPI);
            content = webApi.viewexcpcAction(request, response);
        }
        catch (Exception e)
        {
            Log.log.error(e.getMessage(), e);
            content = "Error while viewing wiki document.";
        }

        ModelAndView result = new ModelAndView(ConstantValues.WIKI_VIEW_JSP, ConstantValues.WIKI_CONTENT_KEY, content);
        return result;
    }

    /**
     * @deprecated
     * 
     *             DO NOT TEST THIS API. THIS IS NOT USED.
     * 
     *             This is a VIEW for the FINAL XML code of a Runbook (Wiki). This is a convenience for the user to copy/paste the model xml
     *             of one document to another.
     *             <p>
     *             {@code http://localhost:8080/resolve/service/wiki/viewfinalc/<Wiki Namespace>/<Wiki Name>}
     *             <p>
     *             eg. {@code http://localhost:8080/resolve/service/wiki/viewfinalc/Test/test2}
     * 
     * @param namespace
     *            Namespace of the wiki document
     * @param docname
     *            Name portion of the wiki document
     * @param request
     *            HttpServletRequest
     * @param response
     *            HttpServletResponse
     * @return {@link ModelAndView} - model returning a jsp page with the rendered content
     * @throws ServletException
     * @throws IOException
     */
    @RequestMapping(value = ConstantValues.URL_VIEWFINALC, method = RequestMethod.GET)
    public ModelAndView viewfinalc(@PathVariable(ConstantValues.WIKI_NAMESPACE_KEY) String namespace, @PathVariable(ConstantValues.WIKI_DOCNAME_KEY) String docname, HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException
    {
        String content = "";

        // set the generic parameters
        ControllerUtil.setRequestParameters(namespace, docname, WikiAction.viewfinalc, request);

        try
        {
            WebApi webApi = (WebApi) RSContext.appContext.getBean(ConstantValues.WIKI_SPRINGID_FOR_WEBAPI);
            content = webApi.viewfinalcAction(request, response);
        }
        catch (Exception e)
        {
            Log.log.error(e.getMessage(), e);
            content = "Error while viewing wiki document.";
        }

        ModelAndView result = new ModelAndView(ConstantValues.WIKI_VIEW_JSP, ConstantValues.WIKI_CONTENT_KEY, content);
        return result;
    }

    /**
     * 
     * This is the entry point for the request to view the attachments of a wiki doc.
     * <p>
     * {@code http://localhost:8080/resolve/service/wiki/viewattach/<Wiki Namespace>/<Wiki Name>}
     * <p>
     * eg. {@code http://localhost:8080/resolve/service/wiki/viewattach/Test/test2}
     * 
     * <p>
     * Parameters in the Request object:
     * <code><pre>
     *  USERNAME - username of the user who is logged in to Resolve
     * </pre></code>
     * 
     * @param namespace
     *            Namespace of the wiki document
     * @param docname
     *            Name portion of the wiki document
     * @param request
     *            HttpServletRequest
     * @param response
     *            HttpServletResponse
     * @return {@link ModelAndView} - model returning a jsp page with the rendered content
     * @throws ServletException
     * @throws IOException
     */
    @RequestMapping(value = ConstantValues.URL_VIEWATTACH, method = RequestMethod.GET)
    public ModelAndView viewattach(@PathVariable(ConstantValues.WIKI_NAMESPACE_KEY) String namespace, @PathVariable(ConstantValues.WIKI_DOCNAME_KEY) String docname, HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException
    {
        Map<String, Object> hmValues = new HashMap<String, Object>();

        // set the generic parameters
        ControllerUtil.setRequestParameters(namespace, docname, WikiAction.viewattach, request);

        try
        {
            WebApi webApi = (WebApi) RSContext.appContext.getBean(ConstantValues.WIKI_SPRINGID_FOR_WEBAPI);
            hmValues = webApi.viewattachAction(request, response);
        }
        catch (Exception e)
        {
            Log.log.error(e.getMessage(), e);
            hmValues.put("error", "Error while viewing wiki attachments.");
        }

        ModelAndView result = new ModelAndView(ConstantValues.WIKI_ATTACH_JSP);
        result.addAllObjects(hmValues);

        return result;
    } // viewattach

    /**
     * 
     * @deprecated
     *             THIS IS NOT SUPPORTED AS THERE WAS NO REFERENCE OF THIS BEING USED. This was done for a jsp page as presently, the Wiki app itself does that.
     * 
     *             Compares the 2 revisions
     * 
     *             Parameters expected are
     *             - flag - should have values 'content,model,exception,final'
     *             - rev1 - integer rev #
     *             - rev2 - integer rev #
     * 
     *             eg. http://localhost:8080/resolve/service/wiki/diffrev/Test/test2?flag=1&rev1=6&rev2=9 -- For CONTENT
     *             http://localhost:8080/resolve/service/wiki/diffrev/Test/test11?flag=2&rev1=8&rev2=10 -- For MODEL
     *             http://localhost:8080/resolve/service/wiki/diffrev/Test/test11?flag=3&rev1=8&rev2=10 -- For EXCEPTION
     *             http://localhost:8080/resolve/service/wiki/diffrev/Test/test11?flag=4&rev1=8&rev2=10 -- For FINAL
     * 
     * @param namespace
     * @param docname
     * @param request
     * @param response
     * @return {@link ModelAndView} - model returning a jsp page with the rendered content
     * @throws ServletException
     * @throws IOException
     */
    @RequestMapping(value = ConstantValues.URL_DIFFREV, method = RequestMethod.GET)
    public ModelAndView diffrev(@PathVariable(ConstantValues.WIKI_NAMESPACE_KEY) String namespace, @PathVariable(ConstantValues.WIKI_DOCNAME_KEY) String docname, HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException
    {
        Map<String, Object> hmValues = new HashMap<String, Object>();

        // set the generic parameters
        ControllerUtil.setRequestParameters(namespace, docname, WikiAction.diffrev, request);

        // THIS IS NOT SUPPORTED AS THERE WAS NO REFERENCE OF THIS BEING USED.

        // try
        // {
        // WebApi webApi = (WebApi) RSContext.appContext.getBean(ConstantValues.WIKI_SPRINGID_FOR_WEBAPI);
        // hmValues = webApi.diffrevAction(request, response);
        // }
        // catch (Exception e)
        // {
        // Log.log.error(e.getMessage(), e);
        // hmValues.put("error", e.getMessage());
        // }

        ModelAndView result = new ModelAndView(ConstantValues.WIKI_DIFF_JSP);
        result.addAllObjects(hmValues);

        return result;
    } // diffrev

    /**
     * 
     * This is the entry point for the download the attachment of a wiki doc.
     * <p>
     * {@code http://localhost:8080/resolve/service/wiki/download/<Wiki Namespace>/<Wiki Name>?attachFileName=<file name>}
     * <p>
     * {@code http://localhost:8080/resolve/service/wiki/download/<Wiki Namespace>/<Wiki Name>?attach=<file sysId>}
     * <p>
     * eg. {@code http://localhost:8080/resolve/service/wiki/download/Test/test2?attachFileName=ddd.png}
     * 
     * <p>
     * Parameters in the Request object:
     * <code><pre>
     *  USERNAME - username of the user who is logged in to Resolve
     *  attach - sysId of an attachment
     *  attachFileName - file name to be downloaded
     * </pre></code>
     * 
     * @param namespace
     *            Namespace of the wiki document
     * @param docname
     *            Name portion of the wiki document
     * @param request
     *            HttpServletRequest
     * @param response
     *            HttpServletResponse
     * @return {@link ModelAndView} - model returning a jsp page with the rendered content
     * @throws ServletException
     * @throws IOException
     */
    @RequestMapping(value = ConstantValues.URL_DOWNLOAD, method = RequestMethod.GET)
    public ModelAndView download(@PathVariable(ConstantValues.WIKI_NAMESPACE_KEY) String namespace, @PathVariable(ConstantValues.WIKI_DOCNAME_KEY) String docname, HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException
    {
        String attach = request.getParameter(ConstantValues.WIKI_PARAMETER_ATTACHMENT_SYS_ID);
        String attachFilename = request.getParameter(ConstantValues.WIKI_PARAMETER_ATTACHMENT_FILE_NAME);
        String username = (String) request.getAttribute(Constants.HTTP_REQUEST_USERNAME);

        try
        {
            if (StringUtils.isNotEmpty(attach))
            {
                FileUploadUtil.downloadFile(request, response, attach, username);
            }
            else if (StringUtils.isNotBlank(attachFilename) && StringUtils.isNotBlank(namespace) && StringUtils.isNotBlank(docname))
            {
                FileUploadUtil.downloadFile(request, response, attachFilename, namespace + "." + docname, username);
            }
        }
        catch (Exception e)
        {
            Log.log.error(e.getMessage(), e);
            throw new ServletException(e);
        }

        return null;
    } // download

    /**
     * This is the entry point for the download the attachment of a wiki doc.
     * <p>
     * {@code http://localhost:8080/resolve/service/wiki/download?wiki=<Wiki Namespace>.<Wiki Name>}
     * <p>
     * eg. {@code http://localhost:8080/resolve/service/wiki/download?wiki=Test.test2}
     * <p>
     * @param request
     * @param response
     * @return
     * @throws ServletException
     * @throws IOException
     */
	@RequestMapping(value = ConstantValues.URL_DOWNLOAD_BASE, method = RequestMethod.GET)
	public ModelAndView download(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		String wiki = request.getParameter(ConstantValues.WIKI_NAME_KEY).trim();
		if (wiki == null) {
			throw new ServletException(MISSING_WIKI_DOCUMENT);
		}

		if (wiki.indexOf(".") > 0) {
			String[] wikiPath = wiki.split("\\.");
			download(wikiPath[0], wikiPath[1], request, response);
		}
		return null;
	}

    /**
     *
     * This is the entry point for listing all the documents belonging to the namespace of this document
     * <p>
     * {@code http://localhost:8080/resolve/service/wiki/listdoc/<Wiki Namespace>/<Wiki Name>}
     * <p>
     * {@code http://localhost:8080/resolve/service/wiki/listdoc/<Wiki Namespace>/<Wiki Name>}
     * <p>
     * eg. {@code http://localhost:8080/resolve/service/wiki/listdoc/Test/test2}
     * 
     * <p>
     * Parameters in the Request object:
     * <code><pre>
     *  USERNAME - username of the user who is logged in to Resolve
     * </pre></code>
     * 
     * @param namespace
     *            Namespace of the wiki document
     * @param docname
     *            Name portion of the wiki document
     * @param request
     *            HttpServletRequest
     * @param response
     *            HttpServletResponse
     * @return {@link ModelAndView} - model returning a jsp page with the rendered content
     * @throws ServletException
     * @throws IOException
     */
    @RequestMapping(value = ConstantValues.URL_LISTDOC, method = RequestMethod.GET)
    public ModelAndView listdoc(@PathVariable(ConstantValues.WIKI_NAMESPACE_KEY) String namespace, @PathVariable(ConstantValues.WIKI_DOCNAME_KEY) String docname, HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException
    {
        Map<String, Object> hmValues = new HashMap<String, Object>();

        // set the generic parameters
        ControllerUtil.setRequestParameters(namespace, docname, WikiAction.listdoc, request);

        try
        {
            WebApi webApi = (WebApi) RSContext.appContext.getBean(ConstantValues.WIKI_SPRINGID_FOR_WEBAPI);
            hmValues = webApi.listdocAction(request, response);
        }
        catch (Exception e)
        {
            Log.log.error(e.getMessage(), e);
            hmValues.put("error", "Error while retrieving wiki list.");
        }

        ModelAndView result = new ModelAndView(ConstantValues.WIKI_VIEWLIST_JSP);
        result.addAllObjects(hmValues);
        return result;
    } // listdoc

    /**
     * This api looks up the Lookup table for the string, evaluates a Wiki and forwards the url to it.
     * <p>
     * {@code http://localhost:8080/resolve/service/wiki/lookup?lookup=<lookup/regex string>}
     * <p>
     * eg. {@code http://localhost:8080/resolve/service/wiki/lookup?lookup=test}
     * 
     * <p>
     * Parameters in the Request object:
     * <code><pre>
     *  USERNAME - username of the user who is logged in to Resolve
     *  lookup - lookup/regex string 
     * </pre></code>
     * 
     * @param request
     *            HttpServletRequest
     * @param response
     *            HttpServletResponse
     * @return {@link ModelAndView} - model returning a jsp page with the rendered content
     * @throws ServletException
     * @throws IOException
     */
    @RequestMapping(value = ConstantValues.URL_LOOKUP, method = { RequestMethod.GET, RequestMethod.POST })
    public ModelAndView lookup(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException
    {
        Map<String, String> hmValues = new HashMap<String, String>();

        try
        {
            WebApi webApi = (WebApi) RSContext.appContext.getBean(ConstantValues.WIKI_SPRINGID_FOR_WEBAPI);
            hmValues = webApi.lookupAction(request, response);
        }
        catch (Exception e)
        {
            Log.log.error(e.getMessage(), e);
            hmValues.put("error", "Error while wiki lookup action.");
        }

        String forwardUrl = hmValues.get(ConstantValues.WIKI_FORWARD_URL_KEY);
        forwardUrl = StringUtils.removeNewLineAndCarriageReturn(forwardUrl);
        response = HttpUtil.sanitizeResponse(response);
        DefaultHTTPUtilities dhttp = new DefaultHTTPUtilities();
        try
        {
            dhttp.sendRedirect(response, forwardUrl);
        }
        catch (AccessControlException ace)
        {
            Log.log.error(ace.getMessage(), ace);
        }

        return null;
    } // lookup

    /**
     * 
     * This url directs to the last wiki document visited by the user. If there is no current page, it will go to user's homepage.
     * <p>
     * {@code http://localhost:8080/resolve/service/wiki/current}
     * <p>
     * eg. {@code http://localhost:8080/resolve/service/wiki/current}
     * 
     * <p>
     * Parameters in the Request object:
     * <code><pre>
     *  USERNAME - username of the user who is logged in to Resolve
     * </pre></code>
     * 
     * @param request
     *            HttpServletRequest
     * @param response
     *            HttpServletResponse
     * @return {@link ModelAndView} - model returning a jsp page with the rendered content
     * @throws ServletException
     * @throws IOException
     */
    // @RequestMapping(value = ConstantValues.URL_CURRENT, method = {RequestMethod.GET, RequestMethod.POST})
    // public ModelAndView current(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException
    // {
    // Map<String, String> hmValues = new HashMap<String, String>();
    //
    // try
    // {
    // WebApi webApi = (WebApi) RSContext.appContext.getBean(ConstantValues.WIKI_SPRINGID_FOR_WEBAPI);
    // hmValues = webApi.current(request, response);
    // }
    // catch (Exception e)
    // {
    // Log.log.error(e.getMessage(), e);
    // hmValues.put("error", e.getMessage());
    // }
    //
    // String forwardUrl = hmValues.get(ConstantValues.WIKI_FORWARD_URL_KEY);
    // forwardUrl = StringUtils.removeNewLineAndCarriageReturn(forwardUrl);
    // response.sendRedirect(forwardUrl);
    //
    // return null;
    // } // current

    /**
     * This will take the user to its homepage.
     * <p>
     * {@code http://localhost:8080/resolve/service/wiki/homepage}
     * <p>
     * eg. {@code http://localhost:8080/resolve/service/wiki/homepage}
     * 
     * <p>
     * Parameters in the Request object:
     * <code><pre>
     *  USERNAME - username of the user who is logged in to Resolve
     * </pre></code>
     * 
     * @param request
     *            HttpServletRequest
     * @param response
     *            HttpServletResponse
     * @return {@link ModelAndView} - model returning a jsp page with the rendered content
     * @throws ServletException
     * @throws IOException
     */
    @RequestMapping(value = ConstantValues.URL_HOMEPAGE, method = RequestMethod.GET)
    public ModelAndView homepage(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException
    {
        Map<String, String> hmValues = new HashMap<String, String>();

        try
        {
            WebApi webApi = (WebApi) RSContext.appContext.getBean(ConstantValues.WIKI_SPRINGID_FOR_WEBAPI);
            hmValues = webApi.homepage(request, response);
        }
        catch (Exception e)
        {
            Log.log.error(e.getMessage(), e);
            hmValues.put("error", "Error while retrieving wiki homepage.");
        }

        String forwardUrl = hmValues.get(ConstantValues.WIKI_FORWARD_URL_KEY);
        response = HttpUtil.sanitizeResponse(response);
        DefaultHTTPUtilities dhttp = new DefaultHTTPUtilities();
        try
        {
            dhttp.sendRedirect(response, forwardUrl);
        }
        catch (AccessControlException ace)
        {
            Log.log.error(ace.getMessage(), ace);
        }

        return null;
    } // current

    /**
     * 
     * This api is used for Decision Tree. It returns the content as HTML string which is response to the question selected
     * on the Decision Tree.
     * 
     * <p>
     * {@code http://localhost:8080/resolve/jsp/rsclient.jsp#RS.wiki.DecisionTreeViewer/name=DT.TestDT}
     * 
     * <p>
     * Parameters in the Request object:
     * <code><pre>
     *  USERNAME: username of the user who is logged in to Resolve
     *  docFullName: document url that needs to be answered
     *  metricData: encoded log used to trace the DT path and reports
     *  PROBLEMID: sysId of worksheet that this DT is executing in
     *  decisiontreeviewer: flag which indicates if the request is coming from the New DT app or the wiki DT 
     *  selectQuestion: flag to indicate if a click was on the left Question panel in the DT Viewer. If true, then the logs will not get updated
     * </pre></code>
     * 
     * <u><b>Example</b></u>:</br>
     * <code><pre>
     *  docFullName:RSQA.TestEWSGateway?ROOT=DT.dt_reena&NODE_ID=9&ANSWER_TEXT=RSQA.TestEWSGateway
     *  metricData:H4sIAAAAAAAAAFWOzUrDUBCFh5baX%2F8Q3Lhz39CkMbYrBdtioVWhQd3Vyc3URJPceO%2B0ZuWLiA8g%2BAIuxSdw6zv4DiYLFw7MnJnhcPjefqCiFewJGRuKtIxWZKyk4U6JVSgm8vby%2FHnH2H95fW%2BWADIF23e4QmPJYWScog6mmFaq3x%2BfuzdfZSiNoBFJ9EcoWKox1DnIMwMZ%2BVl6dAxFtR5r%2BdzKu85QW2pSCcbEUEE%2FDhOGhlgqNXAvkIOUYSPUxXYi4zQiLmwLjHSua6GeEC4e4AmqDE3KSORIMhn7DJ3h9bDtdR3H88yuaVqefYC9niX6PUd4fSIUpmO2B67h81wRJchQVVLy3Oc86t9%2FEz2p2A1jGiczEpqh3O10GFoF5VV4H55JnwqIFsO6ZvzzTmf5bdodx7Qt27IO%2B072C0fNboVpAQAA
     *  PROBLEMID:b366bb13112b45a882c986cb9eeac161
     *  decisiontreeviewer:true
     *  selectQuestion:false
     * </pre></code>
     * 
     * @param isSubmitted
     *            : Boolean flag which will tell whether the question was answered by clicking on Next button (true), or user is just jumping on the already answered questions (false).
     * @param request
     *            HttpServletRequest
     * @param response
     *            HttpServletResponse
     * @return return the HTML string
     * @throws ServletException
     * @throws IOException
     */
    @RequestMapping(value = ConstantValues.URL_INCLUDE_DOC_CONTENT, method = { RequestMethod.POST, RequestMethod.GET }, produces = "text/plain;charset=UTF-8")
    @ResponseBody
    public String renderWikiDocumentContent(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException
    {
        Log.start("Wiki View Response Time", "debug");

        // Keep track of runbook response time
        long start = System.currentTimeMillis();

        String username = (String) request.getAttribute(Constants.HTTP_REQUEST_USERNAME);

        String content = "";

        // Test.test2?key=Jeet1
        // Test.test2#section1?key=Jeet2
        // Test.test2#section1
        String paramDocFullName = request.getParameter("docFullName");
        String metricData = request.getParameter("metricData");
        metricData = HttpUtil.sanitizeValue(metricData, HttpUtil.VALIDATOR_RESOLVETEXT, 4000, HttpUtil.URL_ENCODING);
        
        /*
         * parentRootNode contains the root node name of the DT which is getting viewed at this time.
         * It's introduced because with current logic, the execution path was getting logged with the
         * previous DT even after new DT execution started.
         */
        String parentRootDT = paramDocFullName;
        String answer = null;
        String nodeId = null;
        // String taskList = null;
        // String wikiDocName = paramDocFullName;
        if (paramDocFullName.contains("?"))
        {
            // wikiDocName = paramDocFullName.substring(0, paramDocFullName.indexOf('?'));
            parentRootDT = StringUtils.substringBetween(paramDocFullName, "ROOT=", "&");
            if (paramDocFullName.contains("EXECUTION_LIST"))
            {
                answer = StringUtils.substringBetween(paramDocFullName, "ANSWER_TEXT=", "&");
            }
            else
            {
                answer = StringUtils.substringAfterLast(paramDocFullName, "ANSWER_TEXT=");
            }
            nodeId = StringUtils.substringBetween(paramDocFullName, "NODE_ID=", "&");
            // taskList = StringUtils.substringAfterLast(paramDocFullName, "EXECUTION_LIST=");
        }

        String problemId = request.getParameter("PROBLEMID");
        //String dtModel = WikiUtils.getDTModel(parentRootDT, username);
//        if (isSubmitted != null && isSubmitted)
//        {
            /* RBA-13360 : the following code was here for testing and has been moved to AjaxExecutecontroller.executeSynchronously()
             * ----------------------------------------------------------------------------------------------------------------------             
            if (StringUtils.isNotBlank(taskList))
            {
                // execute these tasks
                List<String> list = new ObjectMapper().readValue(taskList, LinkedList.class);
                // RBA-13360 : Allow DT to automatically execute automation / action task
                for (Iterator iterator = list.iterator(); iterator.hasNext();)
                {
                    String executionItem = (String) iterator.next();
                    // required
                    String userid = username;
                    String problemid = "ACTIVE";
                    String actionname = executionItem;
                    String executionType = executionItem.contains("#") ? "TASK" : (executionItem.contains(".") ? "PROCESS" : null);
                    // optional
                    // String namespace = (String) params.get(Constants.EXECUTE_NAMESPACE); // optional
                    // String actionid = (String) params.get(Constants.EXECUTE_ACTIONID); // optional
                    // String executeid = (String) params.get(Constants.EXECUTE_EXECUTEID); // optional
                    // String processid = (String) params.get(Constants.EXECUTE_PROCESSID); // optional
                    // String wiki = (String) params.get(Constants.EXECUTE_WIKI); // optional
                    // String reference = (String) params.get(Constants.EXECUTE_REFERENCE); // optional
                    // String alertid = (String) params.get(Constants.EXECUTE_ALERTID); // optional
                    // String correlationid = (String) params.get(Constants.EXECUTE_CORRELATIONID); // optional
                    // String input = (String) params.get(Constants.EXECUTE_INPUT); // optional
                    // String delay = (String) params.get(Constants.EXECUTE_DELAY); // optional
                    // String processDebug = (String) params.get(Constants.EXECUTE_PROCESS_DEBUG); // optional
                    // String processMock = (String) params.get(Constants.EXECUTE_PROCESS_MOCK); // optional
                    Map<String, Object> params = new HashMap<String, Object>();
                    params.put(Constants.EXECUTE_USERID, userid);
                    params.put(Constants.EXECUTE_PROBLEMID, problemid);
                    params.put(Constants.EXECUTE_ACTIONNAME, actionname);
                    params.put(Constants.HTTP_REQUEST_ACTION, executionType);
                    params.put(Constants.HTTP_REQUEST_WIKI, wikiDocName);
                    params.put(Constants.EXECUTE_AT_SYNCHRONOUSLY_TIMEOUT, Long.toString(600 * 1000));
                    if (executionType != null && executionType.equals("TASK"))
                    {
                        Map<?, ?> executionResponse = executeATSynchronously(params);
                    }
                    else if (executionType != null && executionType.equals("PROCESS"))
                    {
                        Map<?, ?> executionResponse = executeRunbookSynchronously(params);
                    }
                }
            }
            */            
            //processAnswerParameters(dtModel, problemId, nodeId, parentRootDT, username);
//        }

        // if this flag is true, than its from the new UI
        boolean dtviewer = (request.getParameter("decisiontreeviewer") != null && request.getParameter("decisiontreeviewer").equalsIgnoreCase("true")) ? true : false;

        boolean selectQuestion = request.getParameter("selectQuestion") != null && request.getParameter("selectQuestion").equalsIgnoreCase("true") ? true : false;
        String docFullName = "";
        // TMetricLogVO prevTMetricLog = null;
        // try
        // {
        // prevTMetricLog = new TMetricLogVO(metricData, true);
        // }
        // catch (Exception e1)
        // {
        // e1.printStackTrace();
        // }

        // make sure it is a valid document
        if (StringUtils.isNotEmpty(paramDocFullName) && paramDocFullName.indexOf('.') > -1)
        {
            try
            {
                String docParams = "";
                if (paramDocFullName.indexOf("?") > -1)
                {
                    docFullName = paramDocFullName.substring(0, paramDocFullName.indexOf("?"));
                    docParams = paramDocFullName.substring(paramDocFullName.indexOf("?") + 1);
                }
                else
                {
                    docFullName = paramDocFullName;
                }

                String[] arr = docFullName.split("\\.");
                String namespace = arr[0];
                String docname = arr[1];
                String sectionname = "";
                if (docname.indexOf("#") > -1)
                {
                    sectionname = docname.substring(docname.indexOf("#") + 1);
                    docname = docname.substring(0, docname.indexOf("#"));
                }

                // set the generic parameters
                request.setAttribute(ConstantValues.RENDER_WIKI_CONTENT_ONLY, true);
                request.setAttribute(ConstantValues.WIKI_URL_PARAMS, docParams);
                // request.setAttribute(ConstantValues.DT_TMETRIC_LOG, prevTMetricLog);
                ControllerUtil.setRequestParameters(namespace, docname, sectionname, WikiAction.view, request);

                WebApi webApi = (WebApi) RSContext.appContext.getBean(ConstantValues.WIKI_SPRINGID_FOR_WEBAPI);
                content = webApi.viewAction(request, response);
            }
            catch (Exception e)
            {
                Log.log.error(e.getMessage(), e);
                content = "Error while retrieving response of a selected question.";
            }
        }

        if (!selectQuestion)
        {
            // create and send the TMetricLog to rsmgmt
            TMetricLogVO currTMetricLog = null;
            try
            {
                /*
                 * Object metric = request.getAttribute(ConstantValues.DT_TMETRIC_LOG);
                 * if (metric != null)
                 * {
                 * currTMetricLog = (TMetricLogVO) metric;
                 * }
                 * else
                 */ if (StringUtils.isNotEmpty(metricData))
                {
                    System.out.println("This is the LEAF - last doc of this DT :" + docFullName);

                    currTMetricLog = DecisionMacro.prepareTmetricLog(metricData, docFullName, parentRootDT, username, true, false, dtviewer, problemId, null);
                }
            }
            catch (Exception e1)
            {
                Log.log.error("error in getting the metric log", e1);
            }
            if (currTMetricLog != null)
            {
                if (StringUtils.isNotBlank(problemId))
                {
                    try
                    {
                        // this is for the new DTViewer or even the old DT that has problem Id associated with it
                        // get the data from WSDATA for this problem id.
                        Object property = ServiceWorksheet.getWorksheetWSDATAProperty(problemId, DecisionMacro.DT_METRIC, username);

                        DTMetricLogList dtMetricLogList = null;
                        List<DTMetricLogVO> dtMetricLogs = null;
                        DTMetricLogVO currDT = null;
                        if (property != null)
                        {
                            dtMetricLogList = (DTMetricLogList) new ObjectMapper().readValue((String) property, DTMetricLogList.class);
                            dtMetricLogs = dtMetricLogList.getDtMetricLogs();

                            // get the object belonging to curr DT in question
                            for (DTMetricLogVO vo : dtMetricLogs)
                            {
                                if (StringUtils.isNotBlank(vo.getRootDtFullName()) && vo.getRootDtFullName().equalsIgnoreCase(parentRootDT))
                                {
                                    currDT = vo;
                                    break;
                                }
                            }

                            // if its not there, instantiate it
                            if (currDT == null)
                            {
                                currDT = new DTMetricLogVO();
                                currDT.setExecutionId(currTMetricLog.getExecutionId());
                                currDT.setProblemId(problemId);
                                currDT.setRootDtFullName(parentRootDT);

                                // start of the DT
                                currTMetricLog.setCurrDTPath(parentRootDT);
                            }
                            else
                            {
                                // remove it from the list as we will be adding it back later on, else it can be duplicated
                                dtMetricLogs.remove(currDT);

                                currDT.setExecutionId(currTMetricLog.getExecutionId());
                            }

                        }
                        else
                        {
                            dtMetricLogList = new DTMetricLogList();
                            dtMetricLogs = new ArrayList<DTMetricLogVO>();

                            currDT = new DTMetricLogVO();
                            currDT.setExecutionId(currTMetricLog.getExecutionId());
                            currDT.setProblemId(problemId);
                            currDT.setRootDtFullName(currTMetricLog.getRootDt());

                            // start of the DT
                            currTMetricLog.setCurrDTPath(currTMetricLog.getRootDt());
                        }

                        // prepare the list of metric logs
                        List<TMetricLogVO> logs = currDT.getLogs();
                        if (logs == null)
                        {
                            logs = new ArrayList<TMetricLogVO>();
                        }

                        // append this one to the end.
                        logs.add(currTMetricLog);
                        currDT.setLogs(logs);

                        // it back to the list
                        dtMetricLogs.add(currDT);

                        // update the list back
                        dtMetricLogList.setDtMetricLogs(dtMetricLogs);

                        // persist in WSDATA
                        ServiceWorksheet.setWorksheetWSDATAProperty(problemId, DecisionMacro.DT_METRIC, new ObjectMapper().writeValueAsString(dtMetricLogList), username);

                        // send the metric to rsmgmt
                        Main.sendMetric(currDT);
                    }
                    catch (Exception e)
                    {
                        Log.log.error("Error preparing the metrics", e);
                    }

                }
                else
                {
                    // for the older logic to work and if the problem id is not there for the older DT, it should still work
                    System.out.println("From RSVIEW : " + currTMetricLog.toString());
                    Main.sendMetric(currTMetricLog);
                }
            }
        }
        Log.duration("Wiki View Response Time", "debug");

        // set runbookResponseTime in msecs
        long latency = System.currentTimeMillis() - start;
        Metric.wikiResponseTime.getAndAdd(latency);

        // runbook transaction metric
        Metric.wikiViewCount.incrementAndGet();

        return ESAPI.encoder().encodeForHTML(content);
    }
    
    @RequestMapping(value = "/dt/processAnswerParams", method = { RequestMethod.POST, RequestMethod.GET }, headers = "Accept=application/json; charset=utf-8")
    @ResponseBody
    public ResponseDTO<String> processAnswerParams(@RequestParam String dtFullName, @RequestParam String nodeId, @RequestParam String problemId, 
                    @RequestParam(defaultValue="", value="RESOLVE.ORG_ID") String orgId, @RequestParam(defaultValue="", value="RESOLVE.ORG_NAME") String orgName, HttpServletRequest request) throws ServletException, IOException 
    {
    	ResponseDTO<String> result = new ResponseDTO<String>();
    	String username = (String) request.getAttribute(Constants.HTTP_REQUEST_USERNAME);
    	
    	try
    	{
	    	String dtModel = WikiUtils.getDTModel(dtFullName, username);
	    	processAnswerParameters(dtModel, problemId, nodeId, dtFullName, username, orgId, orgName);
	    	result.setSuccess(true);
    		result.setMessage("Answer params processed successfully.");
    	}
    	catch(Exception e)
    	{
    		result.setSuccess(false);
    		result.setMessage("Error: Could not process answer params for nodeId: " + nodeId + ", problemId: " + problemId + ", DT: " + dtFullName);
    	}
    	
    	return result;
    }

    private void sleepBasedOnParameter(HttpServletRequest request)
    {
        String sleepInMS = request.getParameter(ConstantValues.SLEEP_IN_MILLISECONDS);
        if (sleepInMS != null)
        {
            try
            {
                long time = System.currentTimeMillis();
                Thread.sleep(Long.parseLong(sleepInMS));
                System.out.println("waited for : " + (System.currentTimeMillis() - time));

            }
            catch (Exception e)
            {
                // do nothing
                Log.log.error(e.getMessage(), e);
            }
        }

    }// sleepBasedOnParameter

    @SuppressWarnings("unchecked")
    private void processAnswerParameters(String dtModel, String problemId, String nodeId, String parentRootDT, String username, String orgId, String orgName)
    {
        try
        {
            XDoc doc = new XDoc(dtModel);
            String params = null;
            
            // nodeId is a component id
            String localNodeId = nodeId;
            
            String id = "";
            String edgeXPath = "//Edge/mxCell[@target='" + localNodeId + "']";
            String idXPath  = "//root/[@id='" + id + "']";
            
            Node node = null;
            while (true)
            {
            	// find an edge whose target is the component id.
            	edgeXPath = "//Edge/mxCell[@target='" + localNodeId + "']";
            	Node edgeNode = doc.getNode(edgeXPath);
            	
            	// get the component id from where this edge originated.
            	id = edgeNode.valueOf("@source");
            	// get the component node.
            	idXPath  = "//*[@id='" + id + "']";
            	Node compNode = doc.getNode(idXPath);
            	if (compNode != null)
            	{
            		// get the component name. Task, Document, Subprocess or a Question.
            		String name = compNode.getName();
            		if (name != null)
            		{
            			if (!name.equalsIgnoreCase("question"))
            			{
            				localNodeId = compNode.valueOf("@id");
            				continue;
            			}
            		}
            	}
            	
            	node = edgeNode;
            	break;
            }
            
            if (node != null)
            {
                String source = node.valueOf("@source");
                Node edgNode = node.getParent();
                
                String paramXpath = "//Edge[@id='" + edgNode.valueOf("@id") + "']/params";
                Node paramNode = doc.getNode(paramXpath);
                if(paramNode != null)
                {
                    params = paramNode.getStringValue();
                    if (StringUtils.isNotBlank(params))
                    {
                    	params = params.trim();
                    }
                }
                
                if (StringUtils.isNotBlank(params))
            	{
                	if (problemId.equalsIgnoreCase("active"))
                	{
                		problemId = WorksheetUtils.getActiveWorksheet(username, orgId, orgName);
                	}
            		checkForOldOrNewQuestion(source, params, problemId, parentRootDT, username);
            		Map<String, String> propertyMap = new ObjectMapper().readValue(params, Map.class);
                    for (String key : propertyMap.keySet())
                    {
                        String value = propertyMap.get(key);
                        ServiceWorksheet.setWorksheetWSDATAProperty(problemId, key, value, username);
                    }
            	}
            }
        }
        catch (Exception e)
        {
            e.printStackTrace();
        }
    }

    @SuppressWarnings("unchecked")
    private void checkForOldOrNewQuestion(String source, String params, String problemId, String parentRootDT, String username)
    {
        String answerParamsMap = (String) ServiceWorksheet.getWorksheetWSDATAProperty(problemId, ConstantValues.ANSWER_PARAMS_MAP, username);
        String key = source + ":" + parentRootDT.toLowerCase();

        if (StringUtils.isBlank(answerParamsMap))
        {
            // First question got answered
            Map<String, String> map = new HashMap<String, String>();
            map.put(key, params);
            try
            {
                ServiceWorksheet.setWorksheetWSDATAProperty(problemId, ConstantValues.ANSWER_PARAMS_MAP, new ObjectMapper().writeValueAsString(map), username);
            }
            catch (IOException e)
            {
                Log.log.error("Error: Could not write map to WSDATA", e);
            }
        }
        else
        {
            try
            {
                Map<String, String> map = new ObjectMapper().readValue(answerParamsMap, LinkedHashMap.class);

                // check whether the question is already answered
                if (!map.keySet().contains(key))
                {
                    // The question is answered for the first time.
                    map.put(key, params);
                    ServiceWorksheet.setWorksheetWSDATAProperty(problemId, ConstantValues.ANSWER_PARAMS_MAP, new ObjectMapper().writeValueAsString(map), username);
                }
                else
                {
                    // The question is already answered so, clear all the answeres (key-value pairs) answered after that and start over.
                    ServiceWorksheet.setWorksheetWSDATAProperty(problemId, ConstantValues.ANSWER_PARAMS_MAP, "", username);
                    Set<String> keys = map.keySet();

                    // delete all key-value pair of answer(s).
                    List<String> keysToBeRemoved = new ArrayList<String>();
                    for (String localKey : keys)
                    {
                        if (StringUtils.isNotBlank(map.get(localKey)))
                        {
                            Map<String, String> answerMap = new ObjectMapper().readValue(map.get(localKey), Map.class);
                            keysToBeRemoved.addAll(answerMap.keySet());
                        }
                    }
                    ServiceWorksheet.deleteWorksheetWSDATA(problemId, keysToBeRemoved.toArray(new String[0]), username);

                    // Add key-value pair of answer(s) until the question user jumped back on.
                    Map<String, String> answerParamsLocalMap = new HashMap<String, String>();
                    Map<String, String> mainAnswerParamsMap = new LinkedHashMap<String, String>();
                    for (String localKey : keys)
                    {
                        if (!localKey.equals(key))
                        {
                            // These are the answers given to the question prior to the jumped question.
                            String localValue = map.get(localKey);
                            if (StringUtils.isNotBlank(localValue))
                            {
                                Map<String, String> localMap = new ObjectMapper().readValue(localValue, Map.class);
                                for (String answerKey : localMap.keySet())
                                {
                                    String value = localMap.get(answerKey);
                                    ServiceWorksheet.setWorksheetWSDATAProperty(problemId, answerKey, value, username);
                                    answerParamsLocalMap.put(answerKey, value);
                                    mainAnswerParamsMap.put(localKey, new ObjectMapper().writeValueAsString(answerParamsLocalMap));
                                }
                            }
                            else
                            {
                                // looks like there wasn't any parameter for this answer.
                                mainAnswerParamsMap.put(localKey, "");
                            }
                        }
                        else
                        {
                        	answerParamsLocalMap.clear();
							if (StringUtils.isNotBlank(params))
                            {
                                Map<String, String> answerMap = new ObjectMapper().readValue(params, Map.class);
                                for (String answerKey : answerMap.keySet())
                                {
                                    String value = answerMap.get(answerKey);
                                    // ServiceWorksheet.setWorksheetWSDATAProperty(problemId, answerKey, value, username);
                                    answerParamsLocalMap.put(answerKey, value);
                                    mainAnswerParamsMap.put(localKey, new ObjectMapper().writeValueAsString(answerParamsLocalMap));
                                }
                            }
                            else
                            {
                                // looks like there wasn't any parameter for this answer.
                                mainAnswerParamsMap.put(localKey, "");
                            }

                            if (localKey.equals(key))
                            {
                                break;
                            }
                        }
                    }

                    if (mainAnswerParamsMap.size() > 0)
                    {
                        ServiceWorksheet.setWorksheetWSDATAProperty(problemId, ConstantValues.ANSWER_PARAMS_MAP, new ObjectMapper().writeValueAsString(mainAnswerParamsMap), username);
                    }
                }
            }
            catch (IOException e)
            {
                Log.log.error("Error: Could not read ", e);
            }

        }
    }

    private static String tokenizeContent(String content, HttpServletRequest request) {
    	content = tokenizeIFrame(content, request);
    	return tokenizeBackgroundUrls(content, request);
    }

    private static String tokenizeIFrame(String content, HttpServletRequest request) {
        Document doc = Jsoup.parse(content.replaceAll("&", ConstantValues.GIBBERISH_TOKEN)); // To replace "&" with GIBBERISH_TOKEN in order to prevent Jsoup from convert into "&amp;"
        Elements elems = doc.getElementsByTag("iframe");
        if (elems.size() > 0) {
            for (Element elem: elems) {
            	boolean isSrc = elem.attributes().hasKey("src");
            	String src = "";

            	if (!isSrc) {
            		if (!elem.attributes().hasKey("data-src")) {
            			continue;
            		} else {
                    	src = elem.attributes().get("data-src");
            		}
            	} else {
                	src = elem.attributes().get("src");
            	}

            	if (!src.startsWith("/resolve/")) {
            		continue;
            	}

            	int startQIndex = src.indexOf('?');
                String urlPath = src.substring(0, startQIndex != -1? startQIndex: src.length());
                int startPIndex = urlPath.indexOf('#');
                urlPath = urlPath.substring(0, startPIndex != -1? startPIndex: urlPath.length());
                String sep = ConstantValues.GIBBERISH_TOKEN;
                if (src.indexOf('?') == -1) {
                    sep = "?";
                }
                urlPath = urlPath.replaceAll(" ", "%20");
                String[] csrfPageTokenDetails = JspUtils.getCSRFTokenForPage(request, urlPath);
                String scrField = isSrc ? "src" : "data-src";
                elem.attr(scrField, src+sep+csrfPageTokenDetails[0]+"="+csrfPageTokenDetails[1]);
            }
            content = doc.html();
            content = content.replaceAll(ConstantValues.GIBBERISH_TOKEN, "&"); // Replace with "&" to prevent Jsoup from encoding "&" to "&amp;"
        }
        
        return content;
    }
    
    private static String tokenizeBackgroundUrls(String content, HttpServletRequest request) {
        Matcher matcher = CSS_URL_PATTERN.matcher(content);
		while (matcher.find()) {
			String bUrl = matcher.group();
			String[] urls;
			if (bUrl.contains("\"")) {
				urls = bUrl.split("\"");
			} else {
				urls = bUrl.split("'");
			}
			
			if (urls != null && urls.length > 1) {
				String url = urls[1];
				String[] splits = url.split("\\?");
				String[] tokens = JspUtils.getCSRFTokenForPage(request, splits[0]);
				String token = tokens[0] + "=" + tokens[1];
				StringBuilder urlBuilder = new StringBuilder("url(\"");
				urlBuilder.append(splits[0]).append("?").append(token);
				if (splits.length > 1) {
					urlBuilder.append("&").append(splits[1]);
				}
				urlBuilder.append("\")");
				content = content.replace(bUrl, urlBuilder.toString());
			}
		}
		return content;
    }
} // ViewController
