/******************************************************************************
* (C) Copyright 2014
*
* Resolve Systems, LLC
*
* All rights reserved
* This software is distributed under license and may not be used, copied,
* modified, or distributed without the express written permission of
* Resolve Systems, LLC.
*
******************************************************************************/

package com.resolve.rsview.controller;

import java.io.IOException;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.codehaus.jackson.map.ObjectMapper;
import org.springframework.stereotype.Controller;
import org.springframework.stereotype.Service;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.resolve.services.ServiceHibernate;
import com.resolve.services.hibernate.vo.SysAppApplicationVO;
import com.resolve.services.vo.QueryDTO;
import com.resolve.services.vo.ResponseDTO;
import com.resolve.util.Constants;
import com.resolve.util.Log;

import net.sf.json.JSONObject;

/**
 * Controller to perform CRUD operation on Resolve Menu Definitions
 *
 */
@Controller
@Service
public class AjaxMenuDefinition extends GenericController
{
    /**
     * Returns list of Menu Definition for the grid based on filters, sorts, pagination
     * 
     * @param query : {@link QueryDTO}, represents the query, pagination, sorting, grouping for the UI
     * @param request {@link HttpServletRequest} </code> containing <code>USERNAME</code>: a username of the user who is logged in to Resolve
     * @param response {@link HttpServletResponse}
     * @return {@link ResponseDTO} with status true, {@link SysAppApplicationVO} list as data and total with total record count if everything goes smooth,
     *          false otherwise with the error message.
     * @throws ServletException
     * @throws IOException
     */
    @RequestMapping(value = "/menudefinition/list", method = { RequestMethod.GET, RequestMethod.POST }, headers = "Accept=application/json; charset=utf-8")
    @ResponseBody
    public ResponseDTO list(@ModelAttribute QueryDTO query, HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException
    {
        ResponseDTO result = new ResponseDTO();
        String username = (String) request.getAttribute(Constants.HTTP_REQUEST_USERNAME);
        try
        {  

            query.setModelName("SysAppApplication");
            
            List<SysAppApplicationVO> data = ServiceHibernate.getMenuDefinitions(query, username);
            int total = ServiceHibernate.getTotalHqlCount(query);
            
            result.setSuccess(true).setRecords(data);
            result.setTotal(total);
        }
        catch (Exception e)
        {
            Log.log.error("Error retrieving menu definition", e);
            result.setSuccess(false).setMessage("Error retrieving menu definition");
        }
        
        return result;
    }
    
    /**
     * Gets data for a specific menu definition based on sysId
     * 
     * @param id : String representing sysId of the menu definition
     * @param request {@link HttpServletRequest} </code> containing <code>USERNAME</code>: a username of the user who is logged in to Resolve
     * @param response {@link HttpServletResponse}
     * @return {@link ResponseDTO} with status true and {@link SysAppApplicationVO} as data if everything goes smooth, false otherwise with the error message.
     * @throws ServletException
     * @throws IOException
     */
    @RequestMapping(value = "/menudefinition/get", method = { RequestMethod.POST }, headers = "Accept=application/json; charset=utf-8")
    @ResponseBody
    public ResponseDTO get(@RequestParam String id, HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException
    {
        ResponseDTO result = new ResponseDTO();
        String username = (String) request.getAttribute(Constants.HTTP_REQUEST_USERNAME);
        
        try
        {
            SysAppApplicationVO vo = ServiceHibernate.findMenuDefinition(id, null, username);
            result.setData(vo);
            result.setSuccess(true);
            
        }
        catch (Exception e)
        {
            Log.log.error("Error retrieving menu definition", e);
            result.setSuccess(false).setMessage("Error retrieving menu definition");
        }

        return result;
    }
    
    /**
     *  Delete the menu definition based on array of sysIds
     * 
     * @param ids : String array containing sysIds of the menu definitions to be deleted.
     * @param request {@link HttpServletRequest} </code> containing <code>USERNAME</code>: a username of the user who is logged in to Resolve
     * @param response {@link HttpServletResponse}
     * @return {@link ResponseDTO} with status true if everything goes smooth, false otherwise with the error message.
     * @throws ServletException
     * @throws IOException
     */
    @RequestMapping(value = "/menudefinition/delete", method = { RequestMethod.POST }, headers = "Accept=application/json; charset=utf-8")
    @ResponseBody
    public ResponseDTO delete(@RequestParam String[] ids, @ModelAttribute QueryDTO query, HttpServletRequest request) throws ServletException, IOException
    {
        ResponseDTO result = new ResponseDTO();
        String username = (String) request.getAttribute(Constants.HTTP_REQUEST_USERNAME);
        
        try
        {
            ServiceHibernate.deleteMenuDefinition(ids, query, username);
            result.setSuccess(true).setMessage("Records deleted successfully.");
        }
        catch (Exception e)
        {
            Log.log.error("Error deleting menu definition", e);
            result.setSuccess(false).setMessage("Error deleting menu definition");
        }

        return result;
    }
    
    /**
     * Save the menu definition record
     * 
     * @param jsonProperty : JSON String representing {@link SysAppApplicationVO} object
     * @param request : {@link HttpServletRequest} </code> containing <code>USERNAME</code>: a username of the user who is logged in to Resolve
     * @return {@link ResponseDTO} with status true, {@link SysAppApplicationVO} list as data if everything goes smooth, false otherwise with the error message.
     *          Null <code>jsonProperty</code> will throw NullPointerException
     * @throws ServletException
     * @throws IOException
     */
    @RequestMapping(value = "/menudefinition/save", method = { RequestMethod.POST, RequestMethod.GET })
    @ResponseBody
    public ResponseDTO save(@RequestBody JSONObject jsonProperty,  HttpServletRequest request) throws ServletException, IOException
    {
        //using jackson to deserialize the json
        String json = jsonProperty.toString();
        SysAppApplicationVO entity = new ObjectMapper().readValue(json, SysAppApplicationVO.class);

        //convert json to VO - spring does not work as the VO are not 100% java bean
//        JsonConfig jsonConfig = new JsonConfig();
//        jsonConfig.setRootClass(SysPerspectiveVO.class);
//        jsonConfig.setPropertySetStrategy(new PropertyStrategyWrapper(PropertySetStrategy.DEFAULT));
//        SysPerspectiveVO entity = (SysPerspectiveVO) JSONSerializer.toJava(jsonProperty, jsonConfig);
        
        ResponseDTO result = new ResponseDTO();
        String username = (String) request.getAttribute(Constants.HTTP_REQUEST_USERNAME);
        
        try
        {
            SysAppApplicationVO vo = ServiceHibernate.saveSysAppApplication(entity, username);
            result.setSuccess(true).setData(vo);
        }
        catch (Exception e)
        {
            Log.log.error("Error saving the menu definition", e);
            result.setSuccess(false).setMessage("Error saving the menu definition");
        }
        
        return result;
    }
    
    /**
     * This api use to set the sequence of the menu definition records. User should be able to move the records on the Grid which defines the position which inturn will trigger this api that sets
     * the sequence for each one.
     * 
     * Gets triggered each time a user moves a record on the grid
     * 
     * @param ids : String array of sysIds of all the menu definition
     * @param request : {@link HttpServletRequest} </code> containing <code>USERNAME</code>: a username of the user who is logged in to Resolve
     * @return {@link ResponseDTO} with status true if everything goes smooth, false otherwise with the error message.
     * @throws ServletException
     * @throws IOException
     */
    @RequestMapping(value = "/menudefinition/sequence", method = { RequestMethod.POST }, headers = "Accept=application/json; charset=utf-8")
    @ResponseBody
    public ResponseDTO setSequence(@RequestParam String[] ids, HttpServletRequest request) throws ServletException, IOException
    {
        ResponseDTO result = new ResponseDTO();
        String username = (String) request.getAttribute(Constants.HTTP_REQUEST_USERNAME);
        
        try
        {
            ServiceHibernate.setMenuDefinitionSequence(ids, username);
            result.setSuccess(true).setMessage("");
        }
        catch (Exception e)
        {
            Log.log.error("Error setting sequence to menu definition", e);
            result.setSuccess(false).setMessage("Error setting sequence to menu definition");
        }

        return result;
    }
    
    
}
