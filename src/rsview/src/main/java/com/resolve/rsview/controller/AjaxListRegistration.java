/******************************************************************************
* (C) Copyright 2014
*
* Resolve Systems, LLC
*
* All rights reserved
* This software is distributed under license and may not be used, copied,
* modified, or distributed without the express written permission of
* Resolve Systems, LLC.
*
******************************************************************************/

package com.resolve.rsview.controller;

import java.io.IOException;
import java.util.List;
import java.util.Map;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.stereotype.Controller;
import org.springframework.stereotype.Service;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.resolve.services.ServiceHibernate;
import com.resolve.services.hibernate.vo.ResolveRegistrationVO;
import com.resolve.services.vo.QueryDTO;
import com.resolve.services.vo.ResponseDTO;
import com.resolve.util.Constants;
import com.resolve.util.Log;

/**
 * ResolveRegistration gives in depth information about ACTIVE/INACTIVE/EXPIRED sessions of different Resolve components.
 * This controller lets user list, view and delete the session.
 *
 */
@Controller
@Service
public class AjaxListRegistration extends GenericController
{
    /**
     * Returns list of Resolve Registration for the grid based on filters, sorts, pagination
     * 
     * @param query : {@link QueryDTO}, represents the query, pagination, sorting, grouping for the UI
     * @param request : {@link HttpServletRequest} containing <code>USERNAME</code> - username of the user who is logged in to Resolve
     * @param response : {@link HttpServletResponse}
     * @return {@link ResponseDTO} with success true, records set to the list of {@link ResolveRegistrationVO} and total as total record count if everything goes smooth.
     *          Otherwise success will be false and message will contain an error message.
     * @throws ServletException
     * @throws IOException
     */
    @RequestMapping(value = "/registration/list", method = { RequestMethod.GET, RequestMethod.POST }, headers = "Accept=application/json; charset=utf-8")
    @ResponseBody
    public ResponseDTO list(@ModelAttribute QueryDTO query, HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException
    {
        ResponseDTO result = new ResponseDTO();
        String username = (String) request.getAttribute(Constants.HTTP_REQUEST_USERNAME);
        try
        {  

            query.setModelName("ResolveRegistration");
            
            List<ResolveRegistrationVO> data = ServiceHibernate.getResolveRegistration(query, username);
            int total = ServiceHibernate.getTotalHqlCount(query);
            
            result.setSuccess(true).setRecords(data);
            result.setTotal(total);
        }
        catch (Exception e)
        {
            Log.log.error("Error retrieving ResolveRegistration", e);
            result.setSuccess(false).setMessage("Error retrieving Resolve registrations");
        }
        
        return result;
    }
    
    /**
     * Gets data for a specific ResolveRegistration based on sysId
     * 
     * @param id : String sys_id of the ResolveRegistration record.
     * @param request : <code> HttpServletRequest </code> containing <code>USERNAME</code> - username of the user who is logged in to Resolve
     * @param response : {@link HttpServletResponse}
     * @return {@link ResponseDTO} with success true and data set to {@link ResolveRegistrationVO} if everything goes smooth.
     *          Otherwise success will be false and message will contain an error message.
     * @throws ServletException
     * @throws IOException
     */
    @RequestMapping(value = "/registration/get", method = { RequestMethod.POST }, headers = "Accept=application/json; charset=utf-8")
    @ResponseBody
    public ResponseDTO get(@RequestParam String id, HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException
    {
        ResponseDTO result = new ResponseDTO();
        String username = (String) request.getAttribute(Constants.HTTP_REQUEST_USERNAME);
        
        try
        {
            ResolveRegistrationVO vo = ServiceHibernate.findResolveRegistrationById(id, username);
            result.setData(vo);
            result.setSuccess(true);
            
        }
        catch (Exception e)
        {
            Log.log.error("Error retrieving registration", e);
            result.setSuccess(false).setMessage("Error retrieving registration");
        }

        return result;
    }
    
    /**
     *  Delete the registration based on array of sysIds
     * 
     * @param ids : String array of sys_ids belonging to the ResolveRegistration to be deleted.
     * @param request : <code> HttpServletRequest </code> containing <code>USERNAME</code> - username of the user who is logged in to Resolve
     * @param response : {@link HttpServletResponse}
     * @return {@link ResponseDTO} with success true and message set to "Records deleted successfully." if everything goes smooth.
     *          Otherwise success will be set to false with the message will contain an error message.
     * @throws ServletException
     * @throws IOException
     */
    @RequestMapping(value = "/registration/delete", method = { RequestMethod.POST }, headers = "Accept=application/json; charset=utf-8")
    @ResponseBody
    public ResponseDTO delete(@RequestParam String[] ids, @ModelAttribute QueryDTO query, HttpServletRequest request) throws ServletException, IOException
    {
        ResponseDTO result = new ResponseDTO();
        String username = (String) request.getAttribute(Constants.HTTP_REQUEST_USERNAME);
        
        try
        {
            ServiceHibernate.deleteResolveRegistrationByIds(ids, query, username);
            result.setSuccess(true).setMessage("Records deleted successfully.");
        }
        catch (Exception e)
        {
            Log.log.error("Error deleting registration", e);
            result.setSuccess(false).setMessage("Error deleting registration");
        }

        return result;
    }
    
    /**
     * API to get host information of a host with provided host name.
     * @param hostName: String representing host name whose host information is needed.
     * @param request: HttpServletRequest
     * @return:
     *      A Map with the host information.<br>
     *      E.g. {OS=Linux, IP=127.0.0.1, VERSION=6.3, NAME=RESOLVESYS.COM}<br>
     *      An empty Map is returned if no information is found.
     *      
     * @throws ServletException
     * @throws IOException
     */
    @RequestMapping(value = "/registration/getHostInfo", method = { RequestMethod.GET }, headers = "Accept=application/json; charset=utf-8")
    @ResponseBody
    public ResponseDTO<Map<String, String>> getHostInfo(@RequestParam String hostName, HttpServletRequest request) throws ServletException, IOException
    {
        ResponseDTO<Map<String, String>> result = new ResponseDTO<>();
        String username = (String) request.getAttribute(Constants.HTTP_REQUEST_USERNAME);
        
        try
        {
            Map<String, String> hostInfoMap = ServiceHibernate.getHostInfo(hostName, username);
            result.setData(hostInfoMap);
        }
        catch (Exception e)
        {
            String errorMsg = String.format("Error while getting host information for the host name %s", hostName);
            Log.log.error(errorMsg, e);
            result.setSuccess(false).setMessage(errorMsg);
        }

        return result;
    }
    
}
