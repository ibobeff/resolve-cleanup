/******************************************************************************
* (C) Copyright 2014
*
* Resolve Systems, LLC
*
* All rights reserved
* This software is distributed under license and may not be used, copied,
* modified, or distributed without the express written permission of
* Resolve Systems, LLC.
*
******************************************************************************/

package com.resolve.rsview.controller;

import java.io.IOException;
import java.util.List;
import java.util.Set;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.codehaus.jackson.map.DeserializationConfig;
import org.codehaus.jackson.map.ObjectMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.resolve.services.MetaTableService;
import com.resolve.services.ServiceHibernate;
import com.resolve.services.constants.RightTypeEnum;
import com.resolve.services.vo.QueryDTO;
import com.resolve.services.vo.ResponseDTO;
import com.resolve.util.Constants;
import com.resolve.util.Log;
import com.resolve.util.StringUtils;
import com.resolve.workflow.table.dto.RsMetaTableViewDTO;

import net.sf.json.JSONObject;

@Controller
public class AjaxView extends GenericController
{
	private static final String ERROR_GETTING_LIST_OF_VIEWS = "Error getting the list of views for table :%s";
	private static final String ERROR_GETTING_VIEW = "Error getting the view :%s";
	private static final String ERROR_IN_SAVING_TABLEVIEW = "Error saving tableview";
    private static final String ERROR_DELETING_VIEWS = "Error deleting views";
    
    private MetaTableService metaTableService;
    
    @Autowired
	public AjaxView(MetaTableService metaTableService) {
		this.metaTableService = metaTableService;
	}

	@RequestMapping(value = "/view/save", method = { RequestMethod.POST, RequestMethod.GET }, headers = "Accept=application/json; charset=utf-8")
    @ResponseBody
    public ResponseDTO<RsMetaTableViewDTO> saveView(@RequestBody JSONObject jsonProperty, HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException
    {
        String username = (String) request.getAttribute(Constants.HTTP_REQUEST_USERNAME);
        String json = jsonProperty.toString();
        
        //added the flag to NOT fail if the property is not present. Done for sysOrg field. Comment out when refactoring/enhancing to make sure that the fields match UI and backend.
        ObjectMapper mapper = new ObjectMapper();
        mapper.configure(DeserializationConfig.Feature.FAIL_ON_UNKNOWN_PROPERTIES, false);
        RsMetaTableViewDTO entity = mapper.readValue(json, RsMetaTableViewDTO.class);
        
        ResponseDTO<RsMetaTableViewDTO> result = new ResponseDTO<>();

        try
        {
        	entity = metaTableService.saveMetaTableView(entity, username);
            result.setSuccess(true).setData(entity);
        }
        catch (Exception e)
        {
            Log.log.error(ERROR_IN_SAVING_TABLEVIEW, e);
            result.setSuccess(false).setMessage(ERROR_IN_SAVING_TABLEVIEW);
        }

        return result;
    }
    
    @RequestMapping(value = "/view/list", method = { RequestMethod.POST, RequestMethod.GET }, headers = "Accept=application/json; charset=utf-8")
    @ResponseBody
    public ResponseDTO<RsMetaTableViewDTO> getMetaTableViews(@RequestParam String table, HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException
    {
        String username = (String) request.getAttribute(Constants.HTTP_REQUEST_USERNAME);
        ResponseDTO<RsMetaTableViewDTO> result = new ResponseDTO<>();

        try
        {
            List<RsMetaTableViewDTO> views = metaTableService.getMetaTableViews(table, username, RightTypeEnum.view);
            result.setSuccess(true).setRecords(views);
        }
        catch (Exception e)
        {
            String error = String.format(ERROR_GETTING_LIST_OF_VIEWS, table);
            Log.log.error(error, e);
            result.setSuccess(false).setMessage(error);
        }

        return result;
    }
    
    @RequestMapping(value = "/view/read", method = { RequestMethod.POST, RequestMethod.GET }, headers = "Accept=application/json; charset=utf-8")
    @ResponseBody
    public ResponseDTO<RsMetaTableViewDTO> getMetaTableView(@RequestParam String table, @RequestParam String view, HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException
    {
        String username = (String) request.getAttribute(Constants.HTTP_REQUEST_USERNAME);
        ResponseDTO<RsMetaTableViewDTO> result = new ResponseDTO<>();

        try
        {
            RsMetaTableViewDTO tableView = metaTableService.getMetaTableView(table, view, username, RightTypeEnum.view);
            result.setSuccess(true).setData(tableView);
        }
        catch (Exception e)
        {
            String error = String.format(ERROR_GETTING_VIEW, view);
            Log.log.error(error, e);
            result.setSuccess(false).setMessage(e.getMessage());
        }

        return result;
    }
    
    /**
     * 
     * @param sysIds - comma seperated filter sysIds
     * @param request
     * @param response
     * @return
     * @throws ServletException
     * @throws IOException
     */
    @RequestMapping(value = "/view/delete", method = { RequestMethod.POST, RequestMethod.GET }, headers = "Accept=application/json; charset=utf-8")
    @ResponseBody
    public ResponseDTO deleteView(@RequestParam String sysIds, @ModelAttribute QueryDTO query, HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException
    {
        String username = (String) request.getAttribute(Constants.HTTP_REQUEST_USERNAME);
        ResponseDTO result = new ResponseDTO();
        
        try
        {
            Set<String> setOfSysIds = StringUtils.stringToSet(sysIds, ",");
            ServiceHibernate.deleteMetaTableView(setOfSysIds, username);
            result.setSuccess(true);
        }
        catch (Exception e)
        {
            Log.log.error(ERROR_DELETING_VIEWS, e);
            result.setSuccess(false).setMessage(ERROR_DELETING_VIEWS);
        }

        return result;
    }
}
