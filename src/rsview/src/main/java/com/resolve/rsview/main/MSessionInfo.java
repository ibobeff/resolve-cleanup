/******************************************************************************
* (C) Copyright 2016
*
* Resolve Systems, LLC
*
* All rights reserved
* This software is distributed under license and may not be used, copied,
* modified, or distributed without the express written permission of
* Resolve Systems, LLC.
*
******************************************************************************/

package com.resolve.rsview.main;

import java.util.HashMap;
import java.util.Map;

import org.apache.commons.lang3.StringUtils;

import com.resolve.rsview.auth.AuthSession;
import com.resolve.rsview.auth.Authentication;
import com.resolve.util.Constants;
import com.resolve.util.Log;

/**
 * This class is used for processing get Session Info messages from RSVIEW
 */
public class MSessionInfo
{
    public static Map<String, String> getSessionInfo(Map<String, String> params)
    {
        Map<String, String> result = new HashMap<String, String>();
        
        if (params != null && !params.isEmpty() && params.containsKey(Constants.HTTP_REQUEST_TOKEN))
        {
           AuthSession authSess = Authentication.getAuthTokenCache().get(params.get(Constants.HTTP_REQUEST_TOKEN));
           
           try
           {
               if (authSess != null && StringUtils.isNotBlank(authSess.getUserCredentials()))
               {
                   result.put(Constants.AUTH_USER_CREDENTIALS, authSess.getUserCredentials());
               }
           }
           catch (Exception e)
           {
               Log.log.error(e.getMessage(), e);
           }
        }
        
        return result;
    } //getSessionInfo    
} // MSessionInfo