/******************************************************************************
* (C) Copyright 2014
*
* Resolve Systems, LLC
*
* All rights reserved
* This software is distributed under license and may not be used, copied,
* modified, or distributed without the express written permission of
* Resolve Systems, LLC.
*
******************************************************************************/

package com.resolve.rsview.controller;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.stereotype.Controller;
import org.springframework.stereotype.Service;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.resolve.services.vo.ResponseDTO;
import com.resolve.util.Constants;
import com.resolve.util.JspUtils;
import com.resolve.util.Log;

@Controller
@Service
public class AjaxCSRF extends GenericController
{

    @RequestMapping(value = "/csrf/getCSRFTokenForPage", method = { RequestMethod.POST }, headers = "Accept=application/json; charset=utf-8")
    @ResponseBody
    public ResponseDTO<String[]> getCSRFTokenForPage(@RequestParam String uri, HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException
    {
        long startTime = System.currentTimeMillis();
        
        ResponseDTO<String[]> result = new ResponseDTO<String[]>();
        String username = (String) request.getAttribute(Constants.HTTP_REQUEST_USERNAME);
        
        try
        {
            String[] csrfTokenForPageNameAndVal = JspUtils.getCSRFTokenForPage(request, uri);
            result.setSuccess(true).setData(csrfTokenForPageNameAndVal);
        }
        catch (Throwable t)
        {
            Log.log.error("Error " + t.getLocalizedMessage() + " in getting the page token for " + uri, t);
            result.setSuccess(false).setMessage("Error in getting page for specified URI.");
        }
        
        Log.log.debug("getCSRFTokenForPage(" + username + ") for " + uri + " Total Time taken :" + (System.currentTimeMillis() - startTime) + " msec");

        return result;
    }
}
