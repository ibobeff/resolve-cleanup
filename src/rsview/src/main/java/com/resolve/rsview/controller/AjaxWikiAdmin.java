/******************************************************************************
* (C) Copyright 2014
*
* Resolve Systems, LLC
*
* All rights reserved
* This software is distributed under license and may not be used, copied,
* modified, or distributed without the express written permission of
* Resolve Systems, LLC.
*
******************************************************************************/

package com.resolve.rsview.controller;

import java.io.IOException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.HashSet;
import java.util.List;
import java.util.Locale;
import java.util.Set;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.resolve.search.model.Tag;
import com.resolve.search.wiki.WikiIndexAPI;
import com.resolve.services.ServiceHibernate;
import com.resolve.services.ServiceResolutionBuilder;
import com.resolve.services.ServiceWiki;
import com.resolve.services.hibernate.vo.AccessRightsVO;
import com.resolve.services.hibernate.vo.ResolveActionTaskVO;
import com.resolve.services.hibernate.vo.ResolveTagVO;
import com.resolve.services.hibernate.vo.WikiDocumentVO;
import com.resolve.services.hibernate.vo.WikidocResolutionRatingVO;
import com.resolve.services.hibernate.wiki.WikiStatusEnum;
import com.resolve.services.vo.QueryDTO;
import com.resolve.services.vo.QuerySort;
import com.resolve.services.vo.QuerySort.SortOrder;
import com.resolve.services.vo.ResponseDTO;
import com.resolve.services.vo.social.NodeType;
import com.resolve.util.Constants;
import com.resolve.util.GMTDate;
import com.resolve.util.Log;
import com.resolve.util.StringUtils;
import com.resolve.wiki.stats.StarRating;

/**
 * Wiki Administration Controller.
 * <P>
 * It provides different set of operations\
 * <p>
 * <dt>Wiki Administration</dt>
 * <dd> Operations include Copy, Move/Rename, Delete/Undelete, Purge and Commit.</dd>
 * <dd> Activate/Deactive, Lock/Unlock, Hide/Unhide or any Runbook.</dd>
 * <dd> Setting Roles</dd>
 * <dd> Indexing and Purging the Indices.</dd>
 * <dd> Setting tags, Rating, Reviewed, Expiration and Reseting the Statistics.</dd>
 * <p>
 */

@Controller
public class AjaxWikiAdmin extends GenericController
{
    /**
     * Get list of wiki docs
     * <br>
     *  Example:<br>
     *  filter: [{"field":"uisActive","type":"bool","condition":"equals","value":true}]<br>
     *  page: 3<br>
     *  start: 100<br>
     *  limit: 50<br>
     *  group: [{"property":"unamespace","direction":"ASC"}]<br>
     *  sort: [{"property":"unamespace","direction":"ASC"},{"property":"uhasActiveModel","direction":"DESC"}]
     *  
     * <p>
     * @param query : QueryDTO - provides start, limit, sorting, and filtering (See {@link QueryDTO} for more information)
     * @param request {@link HttpServletRequest}
     * @param response {@link HttpServletResponse}
     * @return
     * @throws ServletException
     * @throws IOException
     */
    @RequestMapping(value = "/wikiadmin/list", method = { RequestMethod.GET, RequestMethod.POST }, headers = "Accept=application/json; charset=utf-8")
    @ResponseBody
    public ResponseDTO listWikis(@ModelAttribute QueryDTO query, HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException
    {
        ResponseDTO result = new ResponseDTO();
        String username = (String) request.getAttribute(Constants.HTTP_REQUEST_USERNAME);
        try
        {
            query.setModelName("WikiDocument");
            query.setSelectColumns("sys_id,UName,UFullname,UNamespace,USummary,UIsActive,UIsDeleted,UIsLocked,UIsHidden,UHasActiveModel,UIsRoot,sysCreatedOn,sysCreatedBy,sysUpdatedOn,sysUpdatedBy,UResolutionBuilderId");
            query.addSortItem(new QuerySort("UFullname", SortOrder.ASC));
            //query.setWhereClause("UHasActiveModel=false"); //test
            
            //get the data
            long timestart = System.currentTimeMillis();
            List<WikiDocumentVO> data = ServiceWiki.getWikiDocuments(query, null, username);
            System.out.println("time taken to get the list of docs: " + (System.currentTimeMillis() - timestart));
            
            int total = ServiceHibernate.getTotalHqlCount(query);
            result.setSuccess(true).setRecords(data);
            result.setTotal(total);
        }
        catch (Exception e)
        {
            Log.log.error("Error retrieving wiki document list", e);
            result.setSuccess(false).setMessage("Error retrieving  wiki document list");
        }

        return result;
    }
    
    /**
     * Get list of wiki docs. This API will not show hidden, deleted, locked and any wiki whose name ends with '_Decision'.
     * <br>
     * @param query : QueryDTO - provides start, limit, sorting, and filtering (See {@link QueryDTO} for more information)
     * @param request {@link HttpServletRequest}
     * @param response {@link HttpServletResponse}
     * @return
     * @throws ServletException
     * @throws IOException
     */
    @RequestMapping(value = "/wiki/list", method = { RequestMethod.GET, RequestMethod.POST }, headers = "Accept=application/json; charset=utf-8")
    @ResponseBody
    public ResponseDTO listWikidocs(@ModelAttribute QueryDTO query, HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException
    {
        String whereClause = "UIsHidden=false AND wd.UIsDeleted=false AND wd.UIsLocked=false AND wd.UName Not Like '%_Decision'";
        query.setWhereClause(whereClause);
        
        return listWikis(query, request, response);
    }
   
    /**
     *  Move/Rename/Copy wikis
     *  
     * @param action - valid values --> rename, copy
     * @param ids - array of doc sysIds
     * @param namespace : String namespace to copy / move / rename to.
     * @param filename : String new file name.
     * @param choice - valid values - for Copy - overwrite, skip, update and For Move - overwrite, skip
     * @param request {@link HttpServletRequest}
     * @param response {@link HttpServletResponse}
     * @return
     * @throws ServletException
     * @throws IOException
     */
    @RequestMapping(value = "/wikiadmin/moveRenameCopy", method = { RequestMethod.GET, RequestMethod.POST }, headers = "Accept=application/json; charset=utf-8")
    @ResponseBody
    public ResponseDTO moveRenameCopy(
                    @RequestParam("action") String action, //rename or copy
                    @RequestParam("ids") String[] ids,
                    @RequestParam("namespace") String namespace,
                    @RequestParam("filename") String filename,
                    @RequestParam("choice") String choice,
                    HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException
    {
        ResponseDTO result = new ResponseDTO();
        String username = (String) request.getAttribute(Constants.HTTP_REQUEST_USERNAME);
        
        try
        {   
            if(ids != null && ids.length > 0 && StringUtils.isNotEmpty(action) && StringUtils.isNotEmpty(namespace) && StringUtils.isNotBlank(choice))
            {
                if (action.equalsIgnoreCase("copy"))
                {
                    if(ids.length == 1 && StringUtils.isNotBlank(filename))
                    {
                        WikiDocumentVO doc = ServiceWiki.getWikiDoc(ids[0], null, username);
                        if(doc == null)
                        {
                            throw new Exception("Document does not exist.");
                        }
                        
                        String fromDocument = doc.getUFullname();
                        String toDocument = namespace.trim() + "." + filename.trim();

                        if (choice.equalsIgnoreCase("overwrite"))
                        {
                            ServiceWiki.copyDocument(fromDocument, toDocument, true, true, username);
                        }
                        else if (choice.equalsIgnoreCase("skip"))
                        {
                            ServiceWiki.copyDocument(fromDocument, toDocument, false, true, username);
                        }
                        else if (choice.equalsIgnoreCase("update"))
                        {
                            ServiceWiki.replaceDocument(fromDocument, toDocument, true, username);
                        }
                    }
                    else
                    {
                        if (choice.equalsIgnoreCase("overwrite"))
                        {
                            ServiceWiki.copyDocuments(Arrays.asList(ids), namespace, true, username);
                        }
                        else if (choice.equalsIgnoreCase("skip"))
                        {
                            ServiceWiki.copyDocuments(Arrays.asList(ids), namespace, false, username);
                        }
                        else if (choice.equalsIgnoreCase("update"))
                        {
                            ServiceWiki.replaceDocuments(Arrays.asList(ids), namespace, username);
                        }
                    }
                    
                }
                else
                {
                    boolean overwrite = choice.equalsIgnoreCase("skip") ? false : true;
                    if(ids.length == 1 && StringUtils.isNotBlank(filename))
                    {
                        WikiDocumentVO doc = ServiceWiki.getWikiDoc(ids[0], null, username);
                        if(doc == null)
                        {
                            throw new Exception("Document does not exist.");
                        }
                        
                        if (doc.getUIsLocked())
                        {
                            throw new Exception("Document '" + doc.getUFullname() + "' is locked. Please unlock it before moving.");
                        }
                        
                        String fromDocument = doc.getUFullname();
                        String toDocument = namespace.trim() + "." + filename.trim();
                        ServiceWiki.moveRenameDocument(fromDocument, toDocument, overwrite, true, username);
                    }
                    else
                    {
                        ServiceWiki.moveRenameDocuments(Arrays.asList(ids), namespace, overwrite, username);
                    }
                }
                
                result.setSuccess(true);
            }
        }
        catch (Exception e)
        {
            Log.log.error("Error renaming/moving/copying wiki(s).", e);
            result.setSuccess(false).setMessage("Error renaming/moving/copying wiki(s).");
        }

        return result;
    }

	//TODO swap moveRenameCopy with this method when ready for transition to the new namespaces
    //@RequestMapping(value = "/wikiadmin/moveRenameCopy", method = RequestMethod.POST)
    //@ResponseBody
    public ResponseDTO<String> moveRenameCopyV2(
                    @RequestParam("action") String action, //rename or copy
                    @RequestParam("ids") String[] ids,
                    @RequestParam("namespace") String namespace,
                    @RequestParam("filename") String filename,
                    @RequestParam("choice") String choice,
			HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		ResponseDTO<String> result = new ResponseDTO<>();
		String username = (String) request.getAttribute(Constants.HTTP_REQUEST_USERNAME);

		try {
			if (ids != null && ids.length > 0 && StringUtils.isNotEmpty(action) && StringUtils.isNotEmpty(namespace)
					&& StringUtils.isNotBlank(choice)) {
				if ("copy".equalsIgnoreCase(action)) {
					if (ids.length == 1 && StringUtils.isNotBlank(filename)) {
						WikiDocumentVO doc = ServiceWiki.getWikiDoc(ids[0], null, username);
						if (doc == null) {
							throw new Exception("Document does not exist.");
						}

						processDocument(choice, doc, namespace, filename, username);
					} else {
						processDocuments(choice, namespace, ids, username);
					}

				} else {
					boolean overwrite = choice.equalsIgnoreCase("skip") ? false : true;
					if (ids.length == 1 && StringUtils.isNotBlank(filename)) {
						WikiDocumentVO doc = ServiceWiki.getWikiDoc(ids[0], null, username);
						if (doc == null) {
							throw new Exception("Document does not exist.");
						}

						if (doc.getUIsLocked()) {
							throw new Exception(
									"Document '" + doc.getUFullname() + "' is locked. Please unlock it before moving.");
						}

						String fromDocument = doc.getUFullname();
						String toDocument = namespace.trim() + "." + filename.trim();
						ServiceWiki.moveRenameDocument(fromDocument, toDocument, overwrite, true, username, true);
					} else {
						ServiceWiki.moveRenameDocuments(Arrays.asList(ids), namespace, overwrite, username, true);
					}
				}

				result.setSuccess(true);
			}
		} catch (Exception e) {
			Log.log.error("Error renaming/moving/copying wiki(s).", e);
			result.setSuccess(false).setMessage("Error renaming/moving/copying wiki(s).");
		}

		return result;
	}
    
	private void processDocument(String choice, WikiDocumentVO doc, String namespace, String filename, String username)
			throws Exception {
		String fromDocument = doc.getUFullname();
		String toDocument = namespace.trim() + "." + filename.trim();

		switch (choice) {
			case "overwrite":
				ServiceWiki.copyDocument(fromDocument, toDocument, true, true, username, true);
				break;
			case "skip":
				ServiceWiki.copyDocument(fromDocument, toDocument, false, true, username, true);
				break;
			case "update":
				ServiceWiki.replaceDocument(fromDocument, toDocument, true, username, true);
				break;
			default:
		}
	}
	
	private void processDocuments(String choice, String namespace, String[] ids, String username) throws Exception {
		switch (choice) {
			case "overwrite":
				ServiceWiki.copyDocuments(Arrays.asList(ids), namespace, true, username, true);
				break;
			case "skip":
				ServiceWiki.copyDocuments(Arrays.asList(ids), namespace, false, username, true);
				break;
			case "update":
				ServiceWiki.replaceDocuments(Arrays.asList(ids), namespace, username, true);
				break;
			default:
		}
	}

    /**
     * Delete selected wiki docs
     * <p>
     * @param ids : String array of WikiDoc Ids to be deleted.
     * @param on : Boolean if set to true, it's delete operation. Undelete otherwise.
     * @param request {@link HttpServletRequest}
     * @param response {@link HttpServletResponse}
     * @return
     * @throws ServletException
     * @throws IOException
     */
    @RequestMapping(value = "/wikiadmin/setDeleted", method = { RequestMethod.GET, RequestMethod.POST }, headers = "Accept=application/json; charset=utf-8")
    @ResponseBody
    public ResponseDTO setDeleted(@RequestParam("ids") String[] ids, @RequestParam("on") Boolean on,HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException
    {
        ResponseDTO result = new ResponseDTO();
        
        String username = (String) request.getAttribute(Constants.HTTP_REQUEST_USERNAME);
        try
        {   
            if(ids != null && ids.length > 0)
            {
                Set<String> docSysIds = new HashSet<String>(Arrays.asList(ids));
                
                long timestart = System.currentTimeMillis();
                ServiceWiki.updateDeleteForWiki(docSysIds, on, username); 
                System.out.println("time taken to delete the list of docs: " + (System.currentTimeMillis() - timestart));
                
                result.setSuccess(true);
            }
        }
        catch (Exception e)
        {
            Log.log.error("Error updating the hidden flag for docs", e);
            result.setSuccess(false).setMessage("Error updating the hidden flag for docs");
        }
   
        return result;
    }
    
    /**
     * Purge the wiki docs
     * 
     * @param ids : String array of WikiDoc Ids to be purged.
     * @param all : Boolean if set to true, all WikiDoc's will be purged ignoring ids.
     * @param request {@link HttpServletRequest}
     * @param response {@link HttpServletResponse}
     * @return
     * @throws ServletException
     * @throws IOException
     */
    @RequestMapping(value = "/wikiadmin/purge", method = { RequestMethod.GET, RequestMethod.POST }, headers = "Accept=application/json; charset=utf-8")
    @ResponseBody
    public ResponseDTO purgeWikis(@RequestParam("ids") String[] ids,
                    @RequestParam("all") Boolean all, //if this true, than it will purge all
                    HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException
    {
        ResponseDTO result = new ResponseDTO();
        String username = (String) request.getAttribute(Constants.HTTP_REQUEST_USERNAME);
        try
        {   
            if(ids != null && ids.length > 0)
            {
                Set<String> docSysIds = new HashSet<String>(Arrays.asList(ids));
                ServiceWiki.deleteWikiDocuments(docSysIds, username);
            }
            else
            {
                if(all)
                {
                    ServiceWiki.deleteAllWikiDocuments(username);
                }
            }
            
            result.setSuccess(true);
        }
        catch (Exception e)
        {
            Log.log.error("Error purging doc(s).", e);
            result.setSuccess(false).setMessage("Error purging doc(s).");
        }
        
        return result;
    }
    
    /**
     * Activate / Deactivate WikiDoc(s)
     * <p>
     * @param ids : String array of WikiDoc ids to be activated or deactivated.
     * @param on : Boolean if true, activate. Deactivate otherwise.
     * @param request {@link HttpServletRequest}
     * @param response {@link HttpServletResponse}
     * @return
     * @throws ServletException
     * @throws IOException
     */
    @RequestMapping(value = "wikiadmin/setActivated", method = { RequestMethod.GET, RequestMethod.POST }, headers = "Accept=application/json; charset=utf-8")
    @ResponseBody
    public ResponseDTO setActivated(@RequestParam("ids") String[] ids, @RequestParam("on") Boolean on,HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException
    {
        ResponseDTO result = new ResponseDTO();
        String username = (String) request.getAttribute(Constants.HTTP_REQUEST_USERNAME);
        
        try
        {   
            if(ids != null && ids.length > 0)
            {
                Set<String> docSysIds = new HashSet<String>(Arrays.asList(ids));
                ServiceWiki.updateWikiStatusFlag(docSysIds, WikiStatusEnum.ACTIVE, on, true, username);
                
                result.setSuccess(true);
            }
        }
        catch (Exception e)
        {
            Log.log.error("Error updating the active flag for doc(s).", e);
            result.setSuccess(false).setMessage("Error updating the active flag for doc(s).");
        }

        return result;
    }
    
    /**
     * Lock/Unlock wiki
     * @param ids : String array of WikiDocs who will be locked or unlocked.
     * @param on : Boolean if set to true, it will be lock operation. Unlock otherwise.
     * @param request {@link HttpServletRequest}
     * @param response {@link HttpServletResponse}
     * @return
     * @throws ServletException
     * @throws IOException
     */
    @RequestMapping(value = "wikiadmin/setLocked", method = { RequestMethod.GET, RequestMethod.POST }, headers = "Accept=application/json; charset=utf-8")
    @ResponseBody
    public ResponseDTO setLocked(@RequestParam("ids") String[] ids, @RequestParam("on") Boolean on,HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException
    {
        ResponseDTO result = new ResponseDTO();
        String username = (String) request.getAttribute(Constants.HTTP_REQUEST_USERNAME);
        try
        {   
            if(ids != null && ids.length > 0)
            {
                Set<String> docSysIds = new HashSet<String>(Arrays.asList(ids));
                ServiceWiki.updateWikiStatusFlag(docSysIds, WikiStatusEnum.LOCKED, on, true, username);
                
                result.setSuccess(true);
            }
        }
        catch (Exception e)
        {
            Log.log.error("Error updating the locked flag for docs(s).", e);
            result.setSuccess(false).setMessage("Error updating the locked flag for doc(s).");
        }

        return result;
    }
    
    /**
     * Hide / Unhide WikiDoc(s)
     * <p>
     * @param ids : String array of WikiDoc Ids to perform Hide / Unhide operation on.
     * @param on : Boolean if set to true, hide operation will be performed. Unhide otherwise.
     * @param request {@link HttpServletRequest}
     * @param response {@link HttpServletResponse}
     * @return
     * @throws ServletException
     * @throws IOException
     */
        @RequestMapping(value = "wikiadmin/setHidden", method = { RequestMethod.GET, RequestMethod.POST }, headers = "Accept=application/json; charset=utf-8")
        @ResponseBody
        public ResponseDTO setHidden(@RequestParam("ids") String[] ids, @RequestParam("on") Boolean on,HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException
        {
            ResponseDTO result = new ResponseDTO();
            String username = (String) request.getAttribute(Constants.HTTP_REQUEST_USERNAME);
            try
            {   
                if(ids != null && ids.length > 0)
                {
                    Set<String> docSysIds = new HashSet<String>(Arrays.asList(ids));
                    ServiceWiki.updateWikiStatusFlag(docSysIds, WikiStatusEnum.HIDDEN, on, true, username);
                    
                    result.setSuccess(true);
                }
            }
            catch (Exception e)
            {
                Log.log.error("Error updating the hidden flag for doc(s).", e);
                result.setSuccess(false).setMessage("Error updating the hidden flag for doc(s).");
            }

            return result;
        }

    /**
     * Rate WikiDoc(s)
     * <p>
     * @param ids : String array of WikiDoc Ids of which needs to be rated.
     * @param u1StarCount : Integer count of 1 start rating.
     * @param u2StarCount : Integer count of 2 start rating.
     * @param u3StarCount : Integer count of 3 start rating.
     * @param u4StarCount : Integer count of 4 start rating.
     * @param u5StarCount : Integer count of 5 start rating.
     * @param request {@link HttpServletRequest}
     * @param response {@link HttpServletResponse}
     * @return
     * @throws ServletException
     * @throws IOException
     */
    @RequestMapping(value = "wikiadmin/rate", method = { RequestMethod.GET, RequestMethod.POST }, headers = "Accept=application/json; charset=utf-8")
    @ResponseBody
    public ResponseDTO rate(
                    @RequestParam("ids") String[] ids,
                    @RequestParam("u1StarCount") Integer u1StarCount,
                    @RequestParam("u2StarCount") Integer u2StarCount,
                    @RequestParam("u3StarCount") Integer u3StarCount,
                    @RequestParam("u4StarCount") Integer u4StarCount,
                    @RequestParam("u5StarCount") Integer u5StarCount,
                    HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException
    {
        ResponseDTO result = new ResponseDTO();
        String username = (String) request.getAttribute(Constants.HTTP_REQUEST_USERNAME);
        try
        {   
            if(ids != null && ids.length > 0)
            {
                WikidocResolutionRatingVO rating = new WikidocResolutionRatingVO();
                rating.setU1StarCount(new Long(u1StarCount));
                rating.setU2StarCount(new Long(u2StarCount));
                rating.setU3StarCount(new Long(u3StarCount));
                rating.setU4StarCount(new Long(u4StarCount));
                rating.setU5StarCount(new Long(u5StarCount));
                
                Set<String> docSysIds = new HashSet<String>(Arrays.asList(ids));
                ServiceWiki.updateResolutionRatingCount(docSysIds, rating, username); 
                
                result.setSuccess(true);
            }
        }
        catch (Exception e)
        {
            Log.log.error("Error updating the rating for doc(s).", e);
            result.setSuccess(false).setMessage("Error updating the rating for doc(s).");
        }
       
        return result;
    }
    
    /**
     * Get the ratings of specified WikiDoc(s). The result will contain WikidocResolutionRatingVO.
     * <p>
     * 
     * @param id : String array of WikiDoc Ids whoes ratings are needed.
     * @param request {@link HttpServletRequest}
     * @param response {@link HttpServletResponse}
     * @return
     * @throws ServletException
     * @throws IOException
     */
    @RequestMapping(value = "wikiadmin/getRating", method = { RequestMethod.GET, RequestMethod.POST }, headers = "Accept=application/json; charset=utf-8")
    @ResponseBody
    public ResponseDTO getRatingForDocument(
                    @RequestParam("id") String id,
                    HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException
    {
        ResponseDTO result = new ResponseDTO();
        String username = (String) request.getAttribute(Constants.HTTP_REQUEST_USERNAME);
        try
        {   
            if(StringUtils.isNotEmpty(id))
            {
                WikidocResolutionRatingVO rating = ServiceWiki.getResolutionRatingForDocument(id, null, username);
                result.setData(rating);                
                result.setSuccess(true);
            }
        }
        catch (Exception e)
        {
            Log.log.error("Error getting the rating for doc.", e);
            result.setSuccess(false).setMessage("Error getting the rating for doc.");
        }
       
        return result;
    }
    
    /**
     * Add search weight to WikiDoc(s)
     * 
     * @deprecated
     * Commented in the UI 
     * 
     * <p>
     * @param ids : String array of WikiDoc Ids whoes search weight needs to be altered. 
     * @param weight: Integer search weight.
     * @param request {@link HttpServletRequest}
     * @param response {@link HttpServletResponse}
     * @return
     * @throws ServletException
     * @throws IOException
     */
    @RequestMapping(value = "wikiadmin/addSearchWeight", method = { RequestMethod.GET, RequestMethod.POST }, headers = "Accept=application/json; charset=utf-8")
    @ResponseBody
    public ResponseDTO addSearchWeight(
                    @RequestParam("ids") String[] ids, 
                    @RequestParam("weight") Double weight,
                    HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException
    {
        ResponseDTO result = new ResponseDTO();
        String username = (String) request.getAttribute(Constants.HTTP_REQUEST_USERNAME);
        try
        {  
            if(ids != null && ids.length > 0)
            {
                Set<String> docSysIds = new HashSet<String>(Arrays.asList(ids));
                ServiceWiki.updateWikidocsWeight(docSysIds, weight.doubleValue(), username); 
                
                result.setSuccess(true);
            }
        }
        catch (Exception e)
        {
            Log.log.error("Error updating the weights for doc(s).", e);
            result.setSuccess(false).setMessage("Error updating the weights for doc(s).");
        }

        return result;
    }
    
    /**
     * Set WikiDoc(s) as reviewed.
     * <p>
     * @param ids : String array of WikiDoc Ids who needs to be reviewed. 
     * @param request {@link HttpServletRequest}
     * @param response {@link HttpServletResponse}
     * @return
     * @throws ServletException
     * @throws IOException
     */
    @RequestMapping(value = "wikiadmin/setReviewed", method = { RequestMethod.GET, RequestMethod.POST }, headers = "Accept=application/json; charset=utf-8")
    @ResponseBody
    public ResponseDTO setReviewed(
                    @RequestParam("ids") String[] ids, 
                    HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException
    {
        ResponseDTO result = new ResponseDTO();
        String username = (String) request.getAttribute(Constants.HTTP_REQUEST_USERNAME);
        try
        {   
            if(ids != null && ids.length > 0)
            {
                Set<String> docSysIds = new HashSet<String>(Arrays.asList(ids));
                ServiceWiki.updateLastReviewedDateForWikiDocs(docSysIds, username); 
                
                result.setSuccess(true);
            }
        }
        catch (Exception e)
        {
            Log.log.error("Error updating the review flag for doc(s).", e);
            result.setSuccess(false).setMessage("Error updating the review flag for doc(s).");
        }

        return result;
    }
    
    /**
     * Set expiry date of WikiDoc(s)
     * <p>
     * @param ids : String array of WikiDoc Ids whoes expiry date to be set.
     * @param strDate String date of the format "2015-01-10" for Jan, 10 2015
     * @param request {@link HttpServletRequest}
     * @param response {@link HttpServletResponse}
     * @return
     * @throws ServletException
     * @throws IOException
     */
    @RequestMapping(value = "wikiadmin/setExpiryDate", method = { RequestMethod.GET, RequestMethod.POST }, headers = "Accept=application/json; charset=utf-8")
    @ResponseBody
    public ResponseDTO setExpiryDate(
                    @RequestParam("ids") String[] ids, 
                    @RequestParam("date") String strDate,
                    HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException
    {
        ResponseDTO result = new ResponseDTO();
        String username = (String) request.getAttribute(Constants.HTTP_REQUEST_USERNAME);
        try
        {   
            if(ids != null && ids.length > 0)
            {
                DateFormat df = new SimpleDateFormat("yyyy-MM-dd", Locale.ENGLISH);
                df.setLenient(false);
                Date date =  GMTDate.getDate(df.parse(strDate));  
                
                Set<String> docSysIds = new HashSet<String>(Arrays.asList(ids));
                ServiceWiki.updateExpirationDateForWikiDocs(docSysIds, date, username); 
                
                result.setSuccess(true);
            }
        }
        catch (Exception e)
        {
            Log.log.error("Error updating the expiry flag for doc(s).", e);
            result.setSuccess(false).setMessage("Error updating the expiry flag for doc(s).");
        }

        return result;
    }
    
    /**
     * Set homepage of roles.
     * <p>
     * This API allows to set specified WikiDoc(s) as a homepage to a given set of roles. If logged in user clicks on Homepage link, a WikiDoc is shown which is set as a homepage for the role of that user.
     * <br>
     * If user has more than one role and all roles has different homepage set, a list of WikiDocs is shown.
     * <p>
     * @param ids : String array of WikiDoc Ids which will be set a homepage for the given roleIds.
     * @param roleIds : String array of Role Ids for which given WikiDoc(s) will be set as homepage. 
     * @param request {@link HttpServletRequest}
     * @param response {@link HttpServletResponse}
     * @return
     * @throws ServletException
     * @throws IOException
     */
    @RequestMapping(value = "wikiadmin/homepage", method = { RequestMethod.GET, RequestMethod.POST }, headers = "Accept=application/json; charset=utf-8")
    @ResponseBody
    public ResponseDTO setHomepage(
                    @RequestParam("id") String id, 
                    @RequestParam("roleIds") String[] roleIds,
                    HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException
    {
        ResponseDTO result = new ResponseDTO();
        String username = (String) request.getAttribute(Constants.HTTP_REQUEST_USERNAME);
        try
        {   
            if(StringUtils.isNotBlank(id) && roleIds != null)
            {
                Set<String> roleSysIds = new HashSet<String>(Arrays.asList(roleIds));
                ServiceWiki.updateHomePageForRoles(id, roleSysIds, username); 
                
                result.setSuccess(true);
            }
        }
        catch (Exception e)
        {
            Log.log.error("Error updating the homepage for doc(s).", e);
            result.setSuccess(false).setMessage("Error updating the homepage for doc(s).");
        }

        return result;
    }
    
    /**
     * Set access rights of all the WikiDoc(s).
     * <p>
     * @param ids : String array of WikiDoc(s) whoes access rights to be set.
     * @param uexecuteAccess : String of comma separated list of roles for execute access.
     * @param uadminAccess : String of comma separated list of roles for admin access.
     * @param ureadAccess : String of comma separated list of roles for read access.
     * @param uwriteAccess : String of comma separated list of roles for write access.
     * @param defaultRights : Boolean if true, sets the default system access rights to all the WikiDoc(s). 
     * @param request {@link HttpServletRequest}
     * @param response {@link HttpServletResponse}
     * @return
     * @throws ServletException
     * @throws IOException
     */
    @RequestMapping(value = "wikiadmin/setAccessRights", method = { RequestMethod.GET, RequestMethod.POST }, headers = "Accept=application/json; charset=utf-8")
    @ResponseBody
    public ResponseDTO setAccessRights(
                    @RequestParam("ids") String[] ids, 
                    @RequestParam("uexecuteAccess") String uexecuteAccess,
                    @RequestParam("uadminAccess") String uadminAccess,
                    @RequestParam("ureadAccess") String ureadAccess,
                    @RequestParam("uwriteAccess") String uwriteAccess,
                    @RequestParam("defaultRights") boolean defaultRights,
                    HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException
    {
        ResponseDTO result = new ResponseDTO();
        String username = (String) request.getAttribute(Constants.HTTP_REQUEST_USERNAME);
        
        try
        {   
            if(ids != null && ids.length > 0)
            {
                AccessRightsVO accessRights = new AccessRightsVO();
                accessRights.setUAdminAccess(uadminAccess);
                accessRights.setUReadAccess(ureadAccess);
                accessRights.setUWriteAccess(uwriteAccess);
                accessRights.setUExecuteAccess(uexecuteAccess);
                
                Set<String> docSysIds = new HashSet<String>(Arrays.asList(ids));
//                ServiceWiki.addRemoveRoles(docSysIds, accessRights, true, username); 
                ServiceWiki.assignRoles(docSysIds, accessRights, defaultRights, username);
                
                result.setSuccess(true);
            }
        
        }
        catch (Exception e)
        {
            Log.log.error("Error adding access rights", e);
            result.setSuccess(false).setMessage("Error adding access rights");
        }

        return result;
    }
    
    //NOTE : I think this its not used anymore from the UI. Will be removed later on.
    /**
     * Remove access rights
     * @param ids
     * @param sys_id
     * @param uadminAccess
     * @param ureadAccess
     * @param uwriteAccess
     * @param request
     * @param response
     * @return
     * @throws ServletException
     * @throws IOException
     */
//    @RequestMapping(value = "wikiadmin/removeAccessRights", method = { RequestMethod.GET, RequestMethod.POST }, headers = "Accept=application/json; charset=utf-8")
//    @ResponseBody
//    public ResponseDTO removeAccessRights(
//                    @RequestParam("ids") String[] ids, 
//                    @RequestParam("uexecuteAccess") String uexecuteAccess,
//                    @RequestParam("uadminAccess") String uadminAccess,
//                    @RequestParam("ureadAccess") String ureadAccess,
//                    @RequestParam("uwriteAccess") String uwriteAccess,
//                    HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException
//    {
//        ResponseDTO result = new ResponseDTO();
//        String username = (String) request.getAttribute(Constants.HTTP_REQUEST_USERNAME);
//        
//        try
//        {   
//            if(ids != null && ids.length > 0)
//            {
//                AccessRightsVO accessRights = new AccessRightsVO();
//                accessRights.setUAdminAccess(uadminAccess);
//                accessRights.setUReadAccess(ureadAccess);
//                accessRights.setUWriteAccess(uwriteAccess);
//                accessRights.setUExecuteAccess(uexecuteAccess);
//                
//                Set<String> docSysIds = new HashSet<String>(Arrays.asList(ids));
//                ServiceWiki.addRemoveRoles(docSysIds, accessRights, false, username); 
//                
//                result.setSuccess(true);
//            }
//        
//        }
//        catch (Exception e)
//        {
//            Log.log.error("Error removing access rights", e);
//            result.setSuccess(false).setMessage("Error removing access rights");
//        }
//
//        return result;
//    }
    
    /**
     * Add specified tags to WikiDoc(s)
     * <p>
     * @param ids : String array of WikiDoc Ids who needs to be tagged.
     * @param tagIds : String array of existing tag Id's to be assigned.
     * @param request {@link HttpServletRequest}
     * @param response {@link HttpServletResponse}
     * @return
     * @throws ServletException
     * @throws IOException
     */
    @RequestMapping(value = "wikiadmin/addTags", method = { RequestMethod.GET, RequestMethod.POST }, headers = "Accept=application/json; charset=utf-8")
    @ResponseBody
    public ResponseDTO addTags(
                    @RequestParam("ids") String[] ids, 
                    @RequestParam("tagIds") String[] tagIds,
                    HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException
    {
        ResponseDTO result = new ResponseDTO();
        String username = (String) request.getAttribute(Constants.HTTP_REQUEST_USERNAME);
        
        try
        {   
            if(ids != null && ids.length > 0 && tagIds != null && tagIds.length > 0)
            {
                Set<String> docSysIds = new HashSet<String>(Arrays.asList(ids));
                Set<String> tagSysIds = new HashSet<String>(Arrays.asList(tagIds));

                ServiceWiki.addTagsToWiki(docSysIds, tagSysIds, username);
                //reindexing the documents so tags get added.
                WikiIndexAPI.indexWikiDocuments(docSysIds, false, username);
                
                result.setSuccess(true);
            }
        }
        catch (Exception e)
        {
            Log.log.error("Error adding tag(s).", e);
            result.setSuccess(false).setMessage("Error adding tag(s).");
        }
       
        return result;
    }
    
    @RequestMapping(value = "/wikiadmin/updateIndexMapping", method = { RequestMethod.GET, RequestMethod.POST }, headers = "Accept=application/json; charset=utf-8")
    @ResponseBody
    public ResponseDTO<String> updateIndexMapping(HttpServletRequest request, HttpServletResponse response) {
    	  ResponseDTO<String> result = new ResponseDTO<String>();
          String username = (String) request.getAttribute(Constants.HTTP_REQUEST_USERNAME);
          try {
        	  WikiIndexAPI.updateIndexMapping(username);
        	  result.setSuccess(true);
          }
          catch (Exception e)
          {
              Log.log.error("Error update index mapping for wiki documents", e);
              result.setSuccess(false).setMessage("Error update index mapping for wiki documents. See log for more info.");
          }
          
          return result;
    }
    
    /**
     * Remove specified tags from the WikiDoc(s).
     * 
     * @param ids : String array of WikiDoc Ids whoes tag(s) to be removed.
     * @param tagIds : String array of existing tag Id's to be removed.
     * @param request {@link HttpServletRequest}
     * @param response {@link HttpServletResponse}
     * @return
     * @throws ServletException
     * @throws IOException
     */
    @RequestMapping(value = "wikiadmin/removeTags", method = { RequestMethod.GET, RequestMethod.POST }, headers = "Accept=application/json; charset=utf-8")
    @ResponseBody
    public ResponseDTO removeTags(
                    @RequestParam("ids") String[] ids, 
                    @RequestParam("tagIds") String[] tagIds,
                    HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException
    {
        ResponseDTO result = new ResponseDTO();
        String username = (String) request.getAttribute(Constants.HTTP_REQUEST_USERNAME);
        try
        {   
            if(ids != null && ids.length > 0 && tagIds != null && tagIds.length > 0)
            {
                Set<String> docSysIds = new HashSet<String>(Arrays.asList(ids));
                Set<String> tagSysIds = new HashSet<String>(Arrays.asList(tagIds));

                ServiceWiki.removeTagsFromWiki(docSysIds, tagSysIds, username); 
                
                result.setSuccess(true);
            }
        }
        catch (Exception e)
        {
            Log.log.error("Error removing tag(s).", e);
            result.setSuccess(false).setMessage("Error removing tag(s).");
        }
        
        return result;
    }
    
    /**
     * Index WikiDoc.
     * 
     * @param ids : String array of namespace(s) of which WikiDoc(s) needs to be indexed.
     * @param all : Boolean if true, all WikiDocs will be indexed.
     * @param indexAttachment : Boolean if true, all the attachments will also be index. It's set to true by default in the UI.
     * @param indexActionTask : Boolean if true, all the ActionTasks which are part of these WikiDoc(s) will also be indexed. It's set to true by default in the UI.
     * @param request {@link HttpServletRequest}
     * @param response {@link HttpServletResponse}
     * @return
     * @throws ServletException
     * @throws IOException
     */
    @RequestMapping(value = "wikiadmin/index", method = { RequestMethod.GET, RequestMethod.POST }, headers = "Accept=application/json; charset=utf-8")
    @ResponseBody
    public ResponseDTO indexWikis(
                    @RequestParam("ids") String[] ids, 
                    @RequestParam("all") Boolean all, //if this true, than it will index all
                    @RequestParam("indexAttachment") Boolean indexAttachment,//TODO: this value is ALWAYS true, please check the UI
                    @RequestParam("indexActionTask") Boolean indexActionTask, //TODO: will be on the AT admin screen
                    HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException
    {
        ResponseDTO result = new ResponseDTO();
        String username = (String) request.getAttribute(Constants.HTTP_REQUEST_USERNAME);
        
        try
        {   
            if(ids != null && ids.length > 0 )
            {
                Set<String> docSysIds = new HashSet<String>(Arrays.asList(ids));
                ServiceWiki.indexWikiDocuments(docSysIds, indexAttachment, username);                
            }
            else
            {
                if(all)
                {
                    ServiceWiki.indexAllWikiDocuments(indexAttachment, username);                
                }
            }
            
            result.setSuccess(true);
        }
        catch (Exception e)
        {
            Log.log.error("Error indexing wiki(s).", e);
            result.setSuccess(false).setMessage("Error indexing wiki(s).");
        }
        
        return result;
    }
    
    /**
     * Purge Indices of WikiDoc(s).
     * 
     * @param ids : String array of WikiDoc Ids whoes indeces needs to be purged.
     * @param all : Boolean if true, all indices will be purged.
     * @param request {@link HttpServletRequest}
     * @param response {@link HttpServletResponse}
     * @return
     * @throws ServletException
     * @throws IOException
     */
    @RequestMapping(value = "wikiadmin/purgeIndex", method = { RequestMethod.GET, RequestMethod.POST }, headers = "Accept=application/json; charset=utf-8")
    @ResponseBody
    public ResponseDTO purgeIndex(
                    @RequestParam("ids") String[] ids, 
                    @RequestParam("all") Boolean all, //if this true, than it will index all
                    HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException
    {
        ResponseDTO result = new ResponseDTO();
        String username = (String) request.getAttribute(Constants.HTTP_REQUEST_USERNAME);
        
        try
        {   
            if(ids != null && ids.length > 0 )
            {
                Set<String> docSysIds = new HashSet<String>(Arrays.asList(ids));
                ServiceWiki.purgeWikiDocumentIndexes(docSysIds, username);                
            }
            else
            {
                if(all)
                {
                    ServiceWiki.purgeAllWikiDocumentIndexes(username);                
                }
            }
            
            result.setSuccess(true);
            result.setMessage("Request to purge an index is submitted.");
        }
        catch (Exception e)
        {
            Log.log.error("Error purging wiki indices", e);
            result.setSuccess(false).setMessage("Error purging wiki indices");
        }

        return result;
    }
    
    
    /**
     * Commit all the WikiDoc(s) with the given comment.
     * 
     * @param ids : String array of WikiDoc Ids needs to be commited with given comment.
     * @param comment : String comment.
     * @param request {@link HttpServletRequest}
     * @param response {@link HttpServletResponse}
     * @return
     * @throws ServletException
     * @throws IOException
     */
    @RequestMapping(value = "wikiadmin/commit", method = { RequestMethod.GET, RequestMethod.POST }, headers = "Accept=application/json; charset=utf-8")
    @ResponseBody
    public ResponseDTO commitRevision(
                    @RequestParam("ids") String[] ids, 
                    @RequestParam("comment") String comment,
                    HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException
    {
        ResponseDTO result = new ResponseDTO();
        String username = (String) request.getAttribute(Constants.HTTP_REQUEST_USERNAME);
       
        try
        {   
            if(ids != null && ids.length > 0)
            {
                Set<String> docSysIds = new HashSet<String>(Arrays.asList(ids));
                ServiceWiki.archiveDocuments(docSysIds, comment, username); 
                
                result.setSuccess(true);
            }
        
        }
        catch (Exception e)
        {
            Log.log.error("Error during commit of doc(s).", e);
            result.setSuccess(false).setMessage("Error during commit of doc(s).");
        }

        return result;
    }
    
    /**
     * Reset statistics of WikiDoc(s).
     * <br>
     * This includes setting ViewCount, EditCount, ExecuteCount, ReviewCount, UsefulNoCount and UsefulYesCount to zero.
     * 
     * @param ids : String array of WikiDoc Ids whoes statistics needs to be reseted.
     * @param request {@link HttpServletRequest}
     * @param response {@link HttpServletResponse}
     * @return
     * @throws ServletException
     * @throws IOException
     */
    @RequestMapping(value = "wikiadmin/resetStats", method = { RequestMethod.GET, RequestMethod.POST }, headers = "Accept=application/json; charset=utf-8")
    @ResponseBody
    public ResponseDTO resetStatistics(
                    @RequestParam("ids") String[] ids,
                    HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException
    {
        ResponseDTO result = new ResponseDTO();
        String username = (String) request.getAttribute(Constants.HTTP_REQUEST_USERNAME);
       
        try
        {   
            if(ids != null && ids.length > 0)
            {
                Set<String> docSysIds = new HashSet<String>(Arrays.asList(ids));
                ServiceWiki.resetWikidocStats(docSysIds, username); 
                
                result.setSuccess(true);
            }
        
        }
        catch (Exception e)
        {
            Log.log.error("Error during reset of stats for docs", e);
            result.setSuccess(false).setMessage("Error during reset of stats for docs");
        }

        return result;
    }
    
    /**
     * Get list of access rights for WikiDoc(s).
     * 
     * @param ids : String array of WikiDoc Ids whoes access rights to be read.
     * @param request {@link HttpServletRequest}
     * @param response {@link HttpServletResponse}
     * @return
     * @throws ServletException
     * @throws IOException
     */
    @RequestMapping(value = "wikiadmin/accessRights", method = { RequestMethod.GET, RequestMethod.POST }, headers = "Accept=application/json; charset=utf-8")
    @ResponseBody
    public ResponseDTO<AccessRightsVO> accessRights(
                    @RequestParam("ids") String[] ids,
                    HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException
    {
        ResponseDTO<AccessRightsVO> result = new ResponseDTO<AccessRightsVO>();
        String username = (String) request.getAttribute(Constants.HTTP_REQUEST_USERNAME);
       
        try
        {   
            if(ids != null && ids.length > 0)
            {
                Set<String> docSysIds = new HashSet<String>(Arrays.asList(ids));
                List<AccessRightsVO> rights = ServiceWiki.getAccessRightsFor(docSysIds, username);
                
                //merge the access rights
                AccessRightsVO merged = mergeAccessRights(rights);
                
                result.setData(merged);
                result.setSuccess(true);
            }
        
        }
        catch (Exception e)
        {
            Log.log.error("Error during getting access rights.", e);
            result.setSuccess(false).setMessage("Error during getting access rights.");
        }

        return result;
    }

    /**
     * 
     * @Deprecated
     * Get the list of Tags associated with WikiDoc(s).
     * 
     * @param ids : String array of WikiDoc Ids .
     * @param request {@link HttpServletRequest}
     * @param response {@link HttpServletResponse}
     * @return
     * @throws ServletException
     * @throws IOException
     */
    @Deprecated
    @RequestMapping(value = "wikiadmin/docTags", method = { RequestMethod.GET, RequestMethod.POST }, headers = "Accept=application/json; charset=utf-8")
    @ResponseBody
    public ResponseDTO<Tag> getDocumentTags(
                    @RequestParam("ids") String[] ids,
                    HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException
    {
        ResponseDTO<Tag> result = new ResponseDTO<Tag>();
        String username = (String) request.getAttribute(Constants.HTTP_REQUEST_USERNAME);
       
        try
        {   
            if(ids != null && ids.length > 0)
            {
                Set<String> docSysIds = new HashSet<String>(Arrays.asList(ids));
                List<ResolveTagVO> tags = ServiceWiki.getTagsForDocuments(docSysIds, username);
                
                List<Tag> mergedTags = new ArrayList<Tag>();
                for(ResolveTagVO vo : tags)
                {
                    mergedTags.add(com.resolve.rsview.util.TagUtil.convertResolveTagToTag(vo));
                }
                
                result.setRecords(mergedTags);
                result.setTotal(mergedTags.size());
                result.setSuccess(true);
            }
        
        }
        catch (Exception e)
        {
            Log.log.error("Error during getting tag(s).", e);
            result.setSuccess(false).setMessage("Error during getting tag(s).");
        }

        return result;
    }
    
    public static StarRating getStarRating(int starRatingInt)
    {
        StarRating starRating = StarRating.STAR_1;
        if(starRatingInt == 2)
        {
            starRating = StarRating.STAR_2;
        }
        else if(starRatingInt == 3)
        {
            starRating = StarRating.STAR_3;
        }
        else if(starRatingInt == 4)
        {
            starRating = StarRating.STAR_4;
        }
        else if(starRatingInt == 5)
        {
            starRating = StarRating.STAR_5;
        }

        return starRating;
    }//getStarRating
    
    public static AccessRightsVO mergeAccessRights(List<AccessRightsVO> rights)
    {
        Set<String> readRoles = new HashSet<String>();
        Set<String> writeRoles = new HashSet<String>();
        Set<String> adminRoles = new HashSet<String>();
        Set<String> executeRoles = new HashSet<String>();

        for (AccessRightsVO right : rights)
        {
            readRoles.addAll(StringUtils.convertStringToList(right.getUReadAccess(), ","));
            writeRoles.addAll(StringUtils.convertStringToList(right.getUWriteAccess(), ","));
            adminRoles.addAll(StringUtils.convertStringToList(right.getUAdminAccess(), ","));
            executeRoles.addAll(StringUtils.convertStringToList(right.getUExecuteAccess(), ","));
        }                

        //prepare the return object
        AccessRightsVO merged = new AccessRightsVO();
        merged.setUReadAccess(StringUtils.convertCollectionToString(readRoles.iterator(), ","));
        merged.setUWriteAccess(StringUtils.convertCollectionToString(writeRoles.iterator(), ","));
        merged.setUAdminAccess(StringUtils.convertCollectionToString(adminRoles.iterator(), ","));
        merged.setUExecuteAccess(StringUtils.convertCollectionToString(executeRoles.iterator(), ","));
        
        return merged;
    }
    
    /**
     * Get list of runbooks
     * <p>
     *  Example:
     *  filter: [{"field":"uisActive","type":"bool","condition":"equals","value":true}]
     *  page: 3
     *  start: 100
     *  limit: 50
     *  group: [{"property":"unamespace","direction":"ASC"}]
     *  sort: [{"property":"unamespace","direction":"ASC"},{"property":"uhasActiveModel","direction":"DESC"}]
     * 
     * @param query : QueryDTO - provides start, limit, sorting, and filtering (See {@link QueryDTO} for more information)
     * @param request {@link HttpServletRequest}
     * @param response {@link HttpServletResponse}
     * @return
     * @throws ServletException
     * @throws IOException
     */
    @RequestMapping(value = "/wikiadmin/listRunbooks", method = { RequestMethod.GET, RequestMethod.POST }, headers = "Accept=application/json; charset=utf-8")
    @ResponseBody
    public ResponseDTO listRunbooks(@ModelAttribute QueryDTO query, HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException
    {
        ResponseDTO result = new ResponseDTO();
        String username = (String) request.getAttribute(Constants.HTTP_REQUEST_USERNAME);
        try
        {
            NodeType nodeType = NodeType.RUNBOOK;
            
            if (StringUtils.isNotBlank(query.getType()))
            {
                try
                {
                    nodeType = NodeType.valueOf(query.getType());
                }
                catch(Exception e)
                {
                    Log.log.warn("Failed to identify type value of " + query.getType() + 
                                 " set in query field. Defaulting to " + NodeType.RUNBOOK.name(), e);
                }
            }
            
            query.setModelName("WikiDocument");
            query.setSelectColumns("sys_id,UName,UFullname,UNamespace,USummary,UIsActive,UIsDeleted,UIsLocked,UIsHidden,UHasActiveModel,UIsRoot,sysCreatedOn,sysCreatedBy,sysUpdatedOn,sysUpdatedBy,UHasResolutionBuilder,UWikiParameters");
            //wd prefix is from the ServiceWiki
            query.setPrefixTableAlias("wd.");
            String whereClause = null;
            
            switch(nodeType)
            {
                case RUNBOOK:
                default:
                    whereClause = " (wd.UHasResolutionBuilder=true or wd.UHasActiveModel=true) ";
                    break;
                    
                case AB_RUNBOOK:
                    whereClause = " UHasResolutionBuilder=true and UHasActiveModel=true and (rb.generated=true) ";
                    break;
            }
            
            if(query.getFilters().size() > 0)
            {
                whereClause += " and " + query.getFilterWhereClause();
            }
            query.setWhereClause(whereClause);
            
            //get the data
            long timestart = System.currentTimeMillis();
            List<WikiDocumentVO> data = ServiceWiki.getWikiDocuments(query, nodeType, username);
            
            System.out.println("time taken to get the list of runbooks: " + (System.currentTimeMillis() - timestart));
            
            int total = ServiceHibernate.getTotalHqlCount(query);
            result.setSuccess(true).setRecords(data);
            result.setTotal(total);
        }
        catch (Exception e)
        {
            Log.log.error("Error retrieving wiki document/runbook(s).", e);
            result.setSuccess(false).setMessage("Error retrieving wiki document/runbook(s).");
        }

        return result;
    }
        
    /**
     * List all Automation Builder Tasks.
     * <p>
     * @param name : String Wiki FullName
     * @param request {@link HttpServletRequest}
     * @param response {@link HttpServletResponse}
     * @return
     * @throws ServletException
     * @throws IOException
     */
    @RequestMapping(value = "/wikiadmin/listABTasks", method = { RequestMethod.GET, RequestMethod.POST }, headers = "Accept=application/json; charset=utf-8")
    @ResponseBody
    public ResponseDTO<ResolveActionTaskVO> listABTasks(@RequestParam("name") String name, HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException
    {
        ResponseDTO<ResolveActionTaskVO> result = new ResponseDTO<ResolveActionTaskVO>();
        String username = (String) request.getAttribute(Constants.HTTP_REQUEST_USERNAME);
        try
        {
            List<ResolveActionTaskVO> list = ServiceResolutionBuilder.listActionTask(name, username);
            result.setRecords(list);
            result.setSuccess(true);
        }
        catch (Exception e)
        {
            Log.log.error("Error retrieving list of AB Task(s).", e);
            result.setSuccess(false).setMessage("Error retrieving list of AB Task(s).");
        }
        return result;
    }
    
    /**
     * Get list of playbook template wiki docs
     * <br>
     *  Example:<br>
     *  filter: [{"field":"uisActive","type":"bool","condition":"equals","value":true}]<br>
     *  page: 3<br>
     *  start: 100<br>
     *  limit: 50<br>
     *  group: [{"property":"unamespace","direction":"ASC"}]<br>
     *  sort: [{"property":"unamespace","direction":"ASC"},{"property":"uhasActiveModel","direction":"DESC"}]
     *  
     * <p>
     * @param query : QueryDTO - provides start, limit, sorting, and filtering (See {@link QueryDTO} for more information)
     * @param request {@link HttpServletRequest}
     * @param response {@link HttpServletResponse}
     * @return
     * @throws ServletException
     * @throws IOException
     */
    @RequestMapping(value = "/wikiadmin/listPlaybookTemplates", method = { RequestMethod.GET, RequestMethod.POST }, headers = "Accept=application/json; charset=utf-8")
    @ResponseBody
    public ResponseDTO listPlaybookTemplates(@ModelAttribute QueryDTO query, HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException
    {
        ResponseDTO result = new ResponseDTO();
        String username = (String) request.getAttribute(Constants.HTTP_REQUEST_USERNAME);
        try
        {
            query.setModelName("WikiDocument");
            query.setSelectColumns("sys_id,UName,UFullname,UNamespace,USummary,UIsDeleted,UIsActive,UIsLocked,UIsHidden,UDisplayMode,UIsTemplate,UVersion,sysCreatedOn,sysCreatedBy,sysUpdatedOn,sysUpdatedBy");
            //query.setWhereClause("UHasActiveModel=false"); //test
            
            //get the data
            long timestart = System.currentTimeMillis();
            List<WikiDocumentVO> data = ServiceWiki.getWikiDocuments(query, NodeType.PLAYBOOK_TEMPLATE, username);
            System.out.println("time taken to get the list of all playbook templates: " + (System.currentTimeMillis() - timestart));
            
            int total = ServiceHibernate.getTotalHqlCount(query);
            result.setSuccess(true).setRecords(data);
            result.setTotal(total);
        }
        catch (Exception e)
        {
            Log.log.error("Error retrieving playbook template list", e);
            result.setSuccess(false).setMessage("Error retrieving playbook template list");
        }

        return result;
    }
}


