/******************************************************************************
* (C) Copyright 2014
*
* Resolve Systems, LLC
*
* All rights reserved
* This software is distributed under license and may not be used, copied,
* modified, or distributed without the express written permission of
* Resolve Systems, LLC.
*
******************************************************************************/

package com.resolve.rsview.main;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

import com.resolve.rsbase.MainBase;
import com.resolve.services.ServiceMCP;
import com.resolve.util.Log;
import com.resolve.util.MCPConstants;
import com.resolve.util.StringUtils;

/**
 * Called by Rest Webservice to get status of a component.
 *
 */
public class MMCP
{
    /**
     *
     * @param params
     *            RETURN_QUEUE: is where status will be submitted, typically RSMGMT GUID.
     * @return
     */
    public static void getComponentStatus(Map<String, String> params)
    {
        if(params == null || params.size() == 0)
        {
            throw new RuntimeException("params is Null or Empty, can't do anything, check the calling code.");
        }
        else
        {
            String returnQueue = params.get("RETURN_QUEUE");
            if(StringUtils.isEmpty(returnQueue))
            {
                Log.log.warn("params did not have a key RETURN_QUEUE with value in it.");
            }
            else
            {
                Map<String, String> reply = new HashMap<String, String>();
                reply.put("COMPONENT_ID", MainBase.main.configId.getGuid());
                reply.put("STATUS", "RUNNING");
                //this will go to the RSMGMT that asked for the status.
                MainBase.esb.sendInternalMessage(returnQueue, "MMCP.receiveStatus", reply);
            }
        }
    }
    
    /**
     * Update status of blueprint deployment.
     * Note: Only 3.5.1 onwards.
     *
     * @return
     */
    public static void updateServerStatus(Map<String, String> params)
    {
        if (params == null || params.size() == 0)
        {
            throw new RuntimeException("params is Null or Empty, can't do anything, check the calling code.");
        }
        else
        {
            String mcpServerId = params.get(MCPConstants.MCP_SERVER_ID);
            if (StringUtils.isBlank(mcpServerId))
            {
                Log.log.warn("params did not have a key MCP_SERVER_ID with value in it.");
            }
            else
            {
                //sysId of the worksheet.
                String problemId = params.get(MCPConstants.MCP_PROBLEM_ID);
                String status = params.get(MCPConstants.MCP_SERVER_STATUS);
                String username = params.get(MCPConstants.MCP_USERNAME);
                String version = params.get(MCPConstants.MCP_SERVER_VERSION);
                String build = params.get(MCPConstants.MCP_SERVER_BUILD);
                try
                {
                    ServiceMCP.updateServerStatus(mcpServerId, problemId, status, username, version, build);
                }
                catch (Exception e)
                {
                    Log.log.error(e.getMessage(), e);
                }
            }
        }
    }

    /**
     * Update the actual blueprint that's in the remote server.
     * Note: Only 3.5.1 onwards.
     *
     * @param params
     */
    public static void updateActualBlueprint(Map<String, String> params)
    {
        if (params == null || params.size() == 0)
        {
            throw new RuntimeException("params is Null or Empty, can't do anything, check the calling code.");
        }
        else
        {
            String mcpServerId = params.get(MCPConstants.MCP_SERVER_ID);
            if (StringUtils.isBlank(mcpServerId))
            {
                Log.log.warn("params did not have a key MCP_SERVER_ID with value in it.");
            }
            else
            {
                //sysId of the worksheet.
                String actualBlueprint = params.get(MCPConstants.MCP_BLUEPRINT_ACTUAL_BLUEPRINT);
                String username = params.get(MCPConstants.MCP_USERNAME);
                try
                {
                    ServiceMCP.updateActualBlueprint(mcpServerId, actualBlueprint, username);
                }
                catch (Exception e)
                {
                    Log.log.error(e.getMessage(), e);
                }
            }
        }
    }

    /**
     * Update the component status like START or STOP etc.
     * Note: Only 3.5.1 onwards.
     *
     * @param params
     */
    public static void updateComponentStatus(Map<String, String> params)
    {
        if (params == null || params.size() == 0)
        {
            throw new RuntimeException("params is Null or Empty, can't do anything, check the calling code.");
        }
        else
        {
            String mcpServerId = params.get(MCPConstants.MCP_SERVER_ID);
            if (StringUtils.isBlank(mcpServerId))
            {
                Log.log.warn("params did not have a key MCP_SERVER_ID with value in it.");
            }
            else
            {
                //sysId of the worksheet.
                String statusKey = params.get(MCPConstants.MCP_FIELD_STATUS_KEY);
                String componentStatus = params.get(MCPConstants.MCP_COMPONENT_STATUS);
                String username = params.get(MCPConstants.MCP_USERNAME);
                String problemId = params.get(MCPConstants.MCP_PROBLEM_ID);

                try
                {
                    ServiceMCP.updateComponentStatus(mcpServerId, statusKey, componentStatus, problemId, username);
                }
                catch (Exception e)
                {
                    Log.log.error(e.getMessage(), e);
                }
            }
        }
    }
    
    public static void updateMCPFile(Map<String, Object> params)
    {
        if (params == null || params.size() == 0)
        {
            throw new RuntimeException("params is Null or Empty, can't do anything, check the calling code.");
        }
        else
        {
            String mcpFileName = (String) params.get(MCPConstants.MCP_FILE_NAME);
            Object mcpFileContentTmp = params.get(MCPConstants.MCP_FILE_CONTENT);
            if (StringUtils.isBlank(mcpFileName))
            {
                Log.log.error("params did not have MCP_FILE_NAME, unable to update or insert MCP File");
            }
            else if (mcpFileContentTmp == null)
            {
                Log.log.error("params did not have MCP_FILE_CONTENT to write to MCP File");
            }
            else
            {
                byte[] mcpFileContent = null;
                if (mcpFileContentTmp instanceof byte[])
                {
                    mcpFileContent = (byte[]) params.get(MCPConstants.MCP_FILE_CONTENT);
                }
                else if (params.get(MCPConstants.MCP_FILE_CONTENT) instanceof String)
                {
                    mcpFileContent = params.get(MCPConstants.MCP_FILE_CONTENT).toString().getBytes();
                }
                else
                {
                    Log.log.error("Incompatable Type for MCP File Content: " + params.get(MCPConstants.MCP_FILE_CONTENT).getClass().getName());
                }
                if (mcpFileContent != null)
                {
                    String mcpFileType = params.get(MCPConstants.MCP_FILE_TYPE) != null ? params.get(MCPConstants.MCP_FILE_TYPE).toString() : null;
                    String mcpFileVersion = params.get(MCPConstants.MCP_FILE_VERSION) != null ? params.get(MCPConstants.MCP_FILE_VERSION).toString() : null;
                    String username = params.get(MCPConstants.MCP_USERNAME) != null ? params.get(MCPConstants.MCP_USERNAME).toString() : null;
                    String mcpServerId = params.get(MCPConstants.MCP_SERVER_ID) != null ? params.get(MCPConstants.MCP_SERVER_ID).toString() : null;
                    
                    Object mcpFileBuildDateTmp = params.get(MCPConstants.MCP_FILE_DATE);
                    Date mcpFileBuildDate = new Date();
                    if (mcpFileBuildDateTmp instanceof Date)
                    {
                        mcpFileBuildDate = (Date) mcpFileBuildDateTmp;
                    }
                    else if (mcpFileBuildDateTmp instanceof String && mcpFileBuildDateTmp.toString().matches("\\d{8}"))
                    {
                        SimpleDateFormat sdf = new SimpleDateFormat("yyyyMMdd");
                        try
                        {
                            mcpFileBuildDate = sdf.parse(mcpFileBuildDateTmp.toString());
                        }
                        catch (ParseException pe)
                        {
                            Log.log.warn("Unable to parse MCP Build Date", pe);
                            mcpFileBuildDate = new Date();
                        }
                    }
                    else
                    {
                        Log.log.warn("Unable to determine MCP File Build Date from " + mcpFileBuildDateTmp.getClass().getName()
                                        + ": " + mcpFileBuildDateTmp.toString());
                    }
                    
                    try
                    {
                        ServiceMCP.updateMCPFile(mcpFileName, mcpFileType, mcpFileContent, mcpFileVersion, mcpFileBuildDate, username, mcpServerId);
                    }
                    catch (Exception e)
                    {
                        Log.log.error("Failed to save MCP File " + mcpFileName + ", " + mcpFileVersion + ", " + mcpFileBuildDate, e);
                    }
                }
            }
        }
    }
}
