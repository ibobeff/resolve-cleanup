package com.resolve.rsview.sso.saml;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import javax.servlet.http.HttpServletRequest;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import org.opensaml.Configuration;
import org.opensaml.common.SAMLObject;
import org.opensaml.common.binding.BasicSAMLMessageContext;
import org.opensaml.saml2.binding.decoding.BaseSAML2MessageDecoder;
import org.opensaml.saml2.binding.decoding.HTTPPostDecoder;
import org.opensaml.saml2.binding.decoding.HTTPRedirectDeflateDecoder;
import org.opensaml.saml2.core.Assertion;
import org.opensaml.saml2.core.Attribute;
import org.opensaml.saml2.core.AttributeStatement;
import org.opensaml.saml2.core.Response;
import org.opensaml.saml2.core.impl.ResponseBuilder;
import org.opensaml.ws.message.decoder.MessageDecodingException;
import org.opensaml.ws.transport.http.HttpServletRequestAdapter;
import org.opensaml.xml.XMLObject;
import org.opensaml.xml.XMLObjectBuilderFactory;
import org.opensaml.xml.io.Unmarshaller;
import org.opensaml.xml.io.UnmarshallerFactory;
import org.opensaml.xml.io.UnmarshallingException;
import org.opensaml.xml.security.SecurityException;
import org.springframework.web.bind.annotation.RequestMethod;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.xml.sax.SAXException;

import com.resolve.services.hibernate.util.PropertiesUtil;
import com.resolve.util.Log;

public enum SAMLManager {

  INSTANCE;
//	idpPublicKey, String spPublicKey, String spPrivateKey
private static final String SAML_ATTR_IDP_PUBLICKEY = "sso.saml.idp.publickey.attributeName";
private static final String SAML_ATTR_SP_PUBLICKEY = "sso.saml.sp.publickey.attributeName";
private static final String SAML_ATTR_SP_PRIVATEKEY = "sso.saml.sp.privatekey.attributeName";
private static final String SAML_ATTR_KEYAUTH_ENABLED = "sso.saml.keyauth.enabled.attributeName";
  public Response consumeSAMLResponseTest(HttpServletRequest request) throws IOException, ParserConfigurationException, SAXException, UnmarshallingException {
    String path = "C:\\Users\\zdravko.bogdanov\\Downloads\\Ping Identity SAML claim.xml";
    byte[] encoded = Files.readAllBytes(Paths.get(path));
    ByteArrayInputStream is = new ByteArrayInputStream(encoded);

    DocumentBuilderFactory documentBuilderFactory = DocumentBuilderFactory.newInstance();
    documentBuilderFactory.setNamespaceAware(true);
    DocumentBuilder docBuilder = documentBuilderFactory.newDocumentBuilder();

    Document document = docBuilder.parse(is);
    Element element = document.getDocumentElement();
    UnmarshallerFactory unmarshallerFactory = Configuration.getUnmarshallerFactory();
    Unmarshaller unmarshaller = unmarshallerFactory.getUnmarshaller(element);
    XMLObject responseXmlObj = unmarshaller.unmarshall(element);

    Response response = (Response) responseXmlObj;
    return response;
  }

  public Response consumeSAMLResponse(HttpServletRequest request) {

    Response samlResponse = null;
    SAMLParser samlParser = null;
    BaseSAML2MessageDecoder decoder = null;
    if (request.getMethod().equals(RequestMethod.GET.name())) {
      decoder = new HTTPRedirectDeflateDecoder();
    } else if (request.getMethod().equals(RequestMethod.POST.name())) {
      decoder = new HTTPPostDecoder();
    }
    if (decoder == null) {
      Log.log.error(String.format("Unsupported HTTP method : %s", request.getMethod()));
      return samlResponse;
    }
    BasicSAMLMessageContext<Response, ?, ?> messageContext = new BasicSAMLMessageContext<Response, SAMLObject, SAMLObject>();
    messageContext.setInboundMessageTransport(new HttpServletRequestAdapter(request));
    try {
      decoder.decode(messageContext);
    } catch (MessageDecodingException e) {
      Log.log.error(e.getMessage(), e);
    } catch (SecurityException e) {
      Log.log.error(e.getMessage(), e);
    }
    XMLObjectBuilderFactory builderFactory = Configuration.getBuilderFactory();
    ResponseBuilder responseBuilder = (ResponseBuilder) builderFactory.getBuilder(Response.DEFAULT_ELEMENT_NAME);
    samlResponse = responseBuilder.buildObject();
    //If the security encryption for SAML is enabled
    if(PropertiesUtil.getPropertyBoolean(SAML_ATTR_KEYAUTH_ENABLED)) {
    	try {
        	 samlParser = new SAMLParser(PropertiesUtil.getPropertyString(SAML_ATTR_IDP_PUBLICKEY),
            		PropertiesUtil.getPropertyString(SAML_ATTR_SP_PUBLICKEY), PropertiesUtil.
            		getPropertyString(SAML_ATTR_SP_PRIVATEKEY));
        }catch(Exception ex) {
        	Log.log.error("Error creating the SAML Parser" + ex.getMessage(), ex);
        }
    }
    samlResponse = (Response) messageContext.getInboundMessage();
    try {
	    //Parse the response to decrypt the message using the key auth token
	    if(samlParser != null) {
	    	samlResponse = (Response) samlParser.parse(samlResponse);
	    }
    }catch(Exception ex) {
    	Log.log.error("Error parsing the SAML request" + ex.getMessage(), ex);
    }
    Log.log.debug(String.format("Issuer: %s, Status: %s, Destination: %s", samlResponse.getIssuer().getValue(), samlResponse.getStatus().getStatusCode().getValue(), samlResponse.getDestination()));
    return samlResponse;
  }

  public Map<String, List<String>> getAttributeStatement(Response samlResponse) {
    Map<String, List<String>> result = new HashMap<String, List<String>>();
    for (Assertion assertion : samlResponse.getAssertions()) {
      Log.log.debug(String.format("Assertion ID: %s", assertion.getID()));
      for (AttributeStatement attributeStatement : assertion.getAttributeStatements()) {
        for (Attribute attribute : attributeStatement.getAttributes()) {
          List<String> values = new ArrayList<String>();
          for (XMLObject attributeValueXML : attribute.getAttributeValues()) {
            values.add(attributeValueXML.getDOM().getTextContent());
          }
          result.put(attribute.getName(), values);
          Log.log.debug(String.format("Attribute Name: %s, Values: %s", attribute.getName(), values));
        }
      }
    }
    return result;
  }

}
