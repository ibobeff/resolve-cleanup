package com.resolve.rsview.controller;

import java.util.List;
import java.util.Map;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.resolve.services.hibernate.util.UserUtils;
import com.resolve.services.vo.ResponseDTO;
import com.resolve.util.Constants;
import com.resolve.util.Log;
import com.resolve.util.RSLogUtil;

/**
 * An RSView Controller to handle the request(s) to ask RSLog to collect the log files
 * as per given criteria and zip them up in a single, log.zip, and return it to the caller.
 *
 */
@Controller
public class AjaxLog extends GenericController
{
    /**
     * An API to receive log.zip file, which is a collection of different log files
     * based on the given criteria, collected at the cenral localtion.
     *  
     * @param compGUID : String representing GUID of the component whoes log is needed.
     * @param component : Optional String representing the component name. If not provided, all the files from given GUID will be considered. 
     * @param from : Optional Long representing start time from where log files are requested. If not provided, it will consider files from the start of the time.
     * @param to : Optional Long representing end time upto which the log files are requested. If not provided, all the files upto current time are considered.
     *          If both, from and to, are not provided, all the files will be considered.
     * @param request : HttpServletRequest
     * @param response : HttpServletResponse
     * @throws ServletException
     */
    
    @RequestMapping(value = {"/getLog"}, method = {RequestMethod.GET }, headers = "Accept=application/json; charset=utf-8")
    @ResponseBody
    public void getLog(@RequestParam String compGUID, @RequestParam (required = false, defaultValue = "") String component,
                    @RequestParam (required = false) Long from, @RequestParam (required = false) Long to,
                    HttpServletRequest request, HttpServletResponse response) throws ServletException {
        String username = (String) request.getAttribute(Constants.HTTP_REQUEST_USERNAME);
        if (UserUtils.isAdminUser(username)) {
            try {
                RSLogUtil.getLog(compGUID, component, from, to, response);
            } catch (Exception e) {
                Log.log.error("Error while getting log.zip file.", e);
            }
        }
    }
    
    /**
     * API to get the list of log files for specified list of RSRemote GUIDs and an optional gateway type.
     * 
     * @param guids: List of Strings representing RSRemote GUIDs
     * @param components: Optional List of strings representing a gateway(s) type. E.g. netcool,http,servicenow etc. If 'all' is mentioned, all gateway logs will be returned.
     * @param from: Long representing the date from which the logs are needed.
     *        If only from date is provided, logs from the selected date upto current date will be selected.
     * @param to: Long representing a date upto which the logs are needed.
     *        If only to date is provided, logs from the epoch time upto the specified date will be selected.
     * @param needRemoteLogs: Optional Boolean flag specifying whether the RSRemote logs are needed or not.
     * @param request: HttpServletRequest
     * @param response: HttpServletResponse
     * @return: A Map (in JSON String format) where key is a GUID and corresponding value is a list of log files.
     * @throws ServletException
     */
    
    @RequestMapping(value = {"/listLogFiles"}, method = {RequestMethod.GET}, headers = "Accept=application/json; charset=utf-8")
    @ResponseBody
    public ResponseDTO<Map<String, Object>> listLogFiles(@RequestParam List<String> guids,
                    @RequestParam (required = false, defaultValue = "") List<String> components,
                    @RequestParam (required = false) Long from,
                    @RequestParam (required = false) Long to,
                    @RequestParam (required = false, defaultValue = "false") Boolean needRemoteLogs,
                    HttpServletRequest request, HttpServletResponse response) throws ServletException {
        String username = (String) request.getAttribute(Constants.HTTP_REQUEST_USERNAME);
        ResponseDTO<Map<String, Object>> result = new ResponseDTO<>();
        if (UserUtils.isAdminUser(username)) {
            try {
                
                Map<String, Object> responseMap = RSLogUtil.listLogFiles(guids, components, from, to, needRemoteLogs);
                result.setData(responseMap);
                result.setSuccess(true);
            } catch (Exception e) {
                Log.log.error("Error while getting list of log files.", e);
                result.setSuccess(false);
                result.setMessage("Error while getting list of log files.");
            }
        }
        return result;
    }
    
    /**
     * API to download specified log file(s).
     * 
     * @param files: List of log file names needed to be downloaded. 
     * @param request: HttpServletRequest
     * @param response: HttpServletResponse
     * @return: Zip file with the name 'log.zip' will be writte to the response output stream.
     * @throws ServletException
     */
    @RequestMapping(value = {"/downloadLogFiles"}, method = {RequestMethod.GET}, headers = "Accept=application/json; charset=utf-8")
    @ResponseBody
    public void downloadLogFiles(@RequestParam List<String> files,
                    HttpServletRequest request, HttpServletResponse response) throws ServletException {
        String username = (String) request.getAttribute(Constants.HTTP_REQUEST_USERNAME);
        if (UserUtils.isAdminUser(username)) {
            try {
                RSLogUtil.downloadLogFiles(files, response);
            } catch (Exception e) {
                Log.log.error("Error downloading specified log files.", e);
            }
        }
    }
}
