/******************************************************************************
* (C) Copyright 2014
*
* Resolve Systems, LLC
*
* All rights reserved
* This software is distributed under license and may not be used, copied,
* modified, or distributed without the express written permission of
* Resolve Systems, LLC.
*
******************************************************************************/

package com.resolve.rsview.controller;

import java.io.File;
import java.io.IOException;
import java.util.EnumSet;
import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.commons.collections.MapUtils;
import org.apache.commons.lang3.tuple.Pair;
import org.joda.time.format.StrictISODateTimeFormat;
import org.owasp.csrfguard.CsrfGuard;
import org.owasp.csrfguard.util.RandomGenerator;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.stereotype.Service;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.resolve.dto.AboutDTO;
import com.resolve.dto.ClientHeaderDTO;
import com.resolve.dto.UserDTO;
import com.resolve.enums.ColorTheme;
import com.resolve.rsbase.MainBase;
import com.resolve.rsview.auth.Authentication;
import com.resolve.rsview.auth.Authentication.AuthenticateResult;
import com.resolve.rsview.main.RSContext;
import com.resolve.rsview.util.ControllerUtil;
import com.resolve.search.model.Worksheet;
import com.resolve.services.ServiceHibernate;
import com.resolve.services.UserService;
import com.resolve.services.constants.ConstantValues;
import com.resolve.services.exception.ColorThemeException;
import com.resolve.services.hibernate.util.UserUtils;
import com.resolve.services.hibernate.vo.OrgsVO;
import com.resolve.services.hibernate.vo.UsersVO;
import com.resolve.services.util.PropertiesUtil;
import com.resolve.services.util.WorksheetUtil;
import com.resolve.services.vo.QueryDTO;
import com.resolve.services.vo.QueryFilter;
import com.resolve.services.vo.ResponseDTO;
import com.resolve.services.vo.menu.MenuDTO;
import com.resolve.util.Constants;
import com.resolve.util.FileUtils;
import com.resolve.util.JspUtils;
import com.resolve.util.Log;
import com.resolve.util.StringUtils;
import com.resolve.util.SyslogFormatter;
import com.resolve.util.constants.HibernateConstants;

import net.sf.json.JSONObject;
@Controller
@Service
public class AjaxClient extends GenericController
{
	private static String ERROR_SAVING_SETTINGS = "Error saving %s settings";
    private static String ERROR_WRONG_SETTINGS_TYPE = "Setting %s is not recognized";
    private static String ERROR_EMPTY_SETTING_VALUE = "Settings %s could not be empty";
    private static final String ERROR_WRONG_REFRESH_RATE_FORMAT = "Refresh rate must be a valid number";
    private static final String ERROR_WRONG_COLOR_THEME = "%s is not a valid color theme";
    private static final String POLL_LOG = "poll (%d nsecs)";
	
	private UserService userService;
	
	@Autowired
    public AjaxClient(UserService userService) {
		this.userService = userService;
	}

    @RequestMapping(value = "/client/getUser", method = { RequestMethod.POST }, headers = "Accept=application/json; charset=utf-8")
    @ResponseBody
    public ResponseDTO<Object> getUser(@RequestParam long notificationsSince, HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException
    {
        long startTimeInstance = System.currentTimeMillis();
        long startTime = System.currentTimeMillis();
        
        ResponseDTO<Object> result = new ResponseDTO<Object>();
        String username = (String) request.getAttribute(Constants.HTTP_REQUEST_USERNAME);

        UserDTO user = new UserDTO();
        user.setBannerAllowToggle(true);
        try
        {
            // Go get user preferences
            UsersVO userVO = ServiceHibernate.getUserByNameNoReference(username);
            
            if (Log.log.isDebugEnabled()) {
	            Log.log.debug(String.format("Time taken for getting userObj: %d msec", 
	            							(System.currentTimeMillis() - startTimeInstance)));
	            startTimeInstance = System.currentTimeMillis();
            }
            
            if (userVO == null)
            {
                throw new Exception(String.format("%s User does not exist.", username));
            }

            user.fromUserVO(userVO);
            user.setDisplayPRB(StringUtils.getBoolean(PropertiesUtil.getPropertyString("product.displayPRB")));

            //Get map of active worksheets and worksheet numbers by org name for the user
            try
            {
                Map<String, Pair<String, String>> userActiveWorksheetsMap = ServiceHibernate
                															.getMapOfActiveWorksheets(username);
                
                if (Log.log.isDebugEnabled()) {
	                Log.log.debug(String.format("Time taken for getting map of active worksheetIds and worksheet numbers " +
	                							"by org name: %d msec", (System.currentTimeMillis() - startTimeInstance)));
	                startTimeInstance = System.currentTimeMillis();
                }
                
                if (MapUtils.isNotEmpty(userActiveWorksheetsMap))
                {
                	Map<String, String> orgNameToProbIdMap = new HashMap<String, String>(userActiveWorksheetsMap.size());
                    Map<String, String> probIdToProbNumberMap = new HashMap<String, String>(userActiveWorksheetsMap.size());
                    
                    for (String orgName : userActiveWorksheetsMap.keySet())
                    {
                    	orgNameToProbIdMap.put(orgName, userActiveWorksheetsMap.get(orgName).getLeft());
                    	probIdToProbNumberMap.put(userActiveWorksheetsMap.get(orgName).getLeft(), 
                    							  userActiveWorksheetsMap.get(orgName).getRight());
                    }
                    
                    user.setOrgNameToProbIdMap(orgNameToProbIdMap);
                    user.setProbIdToProbNumberMap(probIdToProbNumberMap);
                    
                    if (userActiveWorksheetsMap.containsKey(OrgsVO.NONE_ORG_NAME)) {
                    	user.setProblemId(userActiveWorksheetsMap.get(OrgsVO.NONE_ORG_NAME).getLeft());
                    	user.setProblemNumber(userActiveWorksheetsMap.get(OrgsVO.NONE_ORG_NAME).getRight());
                    }
                }
            }
            catch (Exception e1)
            {
                Log.log.error(String.format("Problem Ids and Problem Numbers will not be set. Error in getting map of " +
                							"worksheetIds and worksheet numbers by org information for user: %s",
                							username), e1);
            }

            //Set the banner logo and url
            user.setBannerLogo(PropertiesUtil.getPropertyString(HibernateConstants.PROPERTY_PRODUCT_LOGO));
            user.setBannerURL(PropertiesUtil.getPropertyString(HibernateConstants.BANNER_URL));
            Map<String, String> licenseInfo = MainBase.main.getLicenseService().getLicenseInfo();
            if ("EVALUATION".equalsIgnoreCase(licenseInfo.get("License Type")) || "COMMUNITY".equalsIgnoreCase("License Type"))
            {
                user.setBannerTitle(licenseInfo.get("License Type"));
            }
            else
            {
                user.setBannerTitle(PropertiesUtil.getPropertyString("product.description"));
            }

            user.setRelease(RSContext.getMain().release);
            
            if (Log.log.isDebugEnabled()) {
	            Log.log.debug(String.format("Time taken for getting license: %d msec", 
	            							(System.currentTimeMillis() - startTimeInstance)));
	            startTimeInstance = System.currentTimeMillis();
            }

            //Get the help context for the system
            user.setHelpContextUrl(PropertiesUtil.getPropertyString(HibernateConstants.HELP_CONTEXT_URL));
            user.setHelpNamespace(PropertiesUtil.getPropertyString(HibernateConstants.HELP_CONTEXT_NAMESPACE));

            // Go get the toolbar for the user
            String toolbarString = PropertiesUtil.getPropertyString(HibernateConstants.MENU_TOOLBAR);
            user.setToolbar(toolbarString);
            
            if (Log.log.isDebugEnabled()) {
	            Log.log.debug(String.format("Time taken for getting properties like help, NS, menu toolbar: %d msec",
	            							(System.currentTimeMillis() - startTimeInstance)));
	            startTimeInstance = System.currentTimeMillis();
            }

            //startpage for user
            if (StringUtils.isNotBlank(userVO.getUStartPage()))
            {
                user.setStartPage(userVO.getUStartPage());
            }
            else
            {
                user.setStartPage(PropertiesUtil.getPropertyString("system.homepage"));
            }

            //homepage for this user - if the homepage for a user is not set
            if (StringUtils.isNotBlank(userVO.getUHomePage()))
            {
                user.setHomePage(userVO.getUHomePage());
            }
            else
            {
                //              this.setHomePage(PropertiesUtil.getPropertyString("wiki.property.table.homepage"));
                try
                {
                    user.setHomePage(userService.getUserHomepage(userVO.getUUserName()));
                }
                catch (Exception e)
                {
                    Log.log.error("Error retriving homepage for user " + userVO.getUUserName(), e);
                }
            }
            
            if (Log.log.isDebugEnabled()) {
	            Log.log.debug(String.format("Time taken for getting user homepage: %d msec",
	            							(System.currentTimeMillis() - startTimeInstance)));
	            startTimeInstance = System.currentTimeMillis();
            }

            // Go get the menu for the user
            MenuDTO menu = ServiceHibernate.getMenu(username);
            user.setMenu(menu);
            
            if (Log.log.isDebugEnabled()) {
	            Log.log.debug(String.format("Time taken for getting user Menu: %d msec",
	            							(System.currentTimeMillis() - startTimeInstance)));
	            startTimeInstance = System.currentTimeMillis();
            }
            
            Pair<JSONObject, JSONObject> permissionsPair = UserUtils.getUserPermissions(username);
            user.setPermissions(/*permissionsPair.getLeft()*/permissionsPair.getRight());
            
		    if (user.getColorTheme() == null) {
				ColorTheme colorTheme = com.resolve.services.util.UserUtils.getDefaultColorTheme();
				user.setColorTheme(colorTheme);
		    }
            
            user.setSource(userVO.getUSource());
            result.setSuccess(true).setData(user);
        }
        catch (Exception e)
        {
            Log.log.error("error in getting the user", e);
            result.setSuccess(false).setMessage("Error in getting the user. See log for more info.");
        }
        
        if (Log.log.isDebugEnabled()) {
	        Log.log.debug(String.format("getUser(%s) Total Time taken: %d msec", username, 
	        							(System.currentTimeMillis() - startTime)));
        }

        return result;
    }

    @RequestMapping(value = "/client/poll", method = { RequestMethod.POST }, headers = "Accept=application/json; charset=utf-8")
    @ResponseBody
    public ResponseDTO<Object> poll(@RequestParam long notificationsSince, @RequestParam String USource, HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException
    {
        long startTime = System.nanoTime();
        ResponseDTO<Object> result = new ResponseDTO<Object>();

        try
        {
            ClientHeaderDTO client = new ClientHeaderDTO();
            
            // Check SSOREDIRECTURL attribute in request, if not empty return false
            
            if (StringUtils.isNotBlank((String)request.getAttribute(Constants.HTTP_ATTRIBUTE_SSO_REDIRECT_URL)))
            {
                client.setRedirectUrl((String)request.getAttribute(Constants.HTTP_ATTRIBUTE_SSO_REDIRECT_URL));
                result.setSuccess(false).setData(client);
            }
            else
            {
                // Return true, because if we get here then the client is already authenticated.
                // If they weren't, they would be redirected to the main login page which is handled on the client as an error
                QueryDTO query = new QueryDTO(0, 1);
                QueryFilter filterItem = new QueryFilter("date", StrictISODateTimeFormat.dateTime().print(notificationsSince), "sysUpdatedOn", "after");
                query.addFilterItem(filterItem);
                
                /* COMMENTING OUT listSmallUserStreams/listUserStreams from getUser. 
                 * UI should make a separate request which is not part of getUser since it 
                 * can be time consuming computation to identify all streams followed by 
                 * user from ResolveNode-ResolveEdge table in DB and then check the social
                 * posts from Elastic Search.
                 *
                
                long startTimeInstance = System.currentTimeMillis();
                ResponseDTO<Object> socialResponse = (new AjaxSocial()).listSmallUserStreams(query, request);
                Log.log.debug("Time taken for getting listSmallUserStreams in poll() since " + ISODateTimeFormat.dateTime().print(notificationsSince) + 
                              " : " + (System.currentTimeMillis() - startTimeInstance) + " msec, returned " +
                              (socialResponse.getRecords().isEmpty() ? 0 : (socialResponse.getRecords().size() - 1)) + " stream(s)");
                
                if (Log.log.isDebugEnabled() && !socialResponse.getRecords().isEmpty())
                {
                    Log.log.debug("poll() Unread Post Counts: " + StringUtils.mapToJson((Map<String, Long>)socialResponse.getRecords().get(socialResponse.getRecords().size() - 1)));
                }
                    
                if (socialResponse.isSuccess())
                {
                    result.setRecords(socialResponse.getRecords());
                }
                */
                
                result.setSuccess(true).setData(client);
            }
        }
        catch (Exception e)
        {
            Log.log.error("error in getting the user", e);
            result.setSuccess(false).setMessage("Error in getting the user. See log for more info.");
        }
        
        Log.log.trace(String.format(POLL_LOG, (System.nanoTime() - startTime)));
        
        return result;
    }

    @SuppressWarnings("unchecked")
    @RequestMapping(value = "/client/login", method = { RequestMethod.POST }, headers = "Accept=application/json; charset=utf-8")
    @ResponseBody
    public ResponseDTO<String[]> login(@RequestParam String username, @RequestParam String password, HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException
    {
        ResponseDTO<String[]> result = new ResponseDTO<String[]>();
        String[] csrfTokenNamesAndVals = new String[4];
        
        try
        {
            if (UserUtils.isLockedOut(username))
            {
                String syslogMessage = Authentication.AUTHENTICATION_SYSLOG_RSVIEW_LOGIN_FAILED_MESAGE_PREFIX + "RSView Client" + Authentication.AUTHENTICATION_SYSLOG_RSVIEW_LOGIN_MESSAGE_SUFFIX;
                Log.syslog.info(SyslogFormatter.INSTANCE.formatAudit(Authentication.getRemoteAddrForSysLog(request), username, request.getLocalAddr(), syslogMessage));
                
                throw new Exception ("User account is locked. Please contact system administrator.");
            }
            
        	Authentication.regenerateSession(request);
        	
        	CsrfGuard csrfGuard = CsrfGuard.getInstance();
        	
        	HttpSession session = request.getSession(false);
        	
        	if (session != null && session.isNew())
            {
        	    csrfTokenNamesAndVals[0] = csrfGuard.getSessionKey();
        	    csrfTokenNamesAndVals[1] = (String)session.getAttribute(csrfGuard.getSessionKey());
        	    csrfTokenNamesAndVals[2] = csrfGuard.getTokenName();
        	    
        	    Map<String, String> pageTokens = (Map<String, String>) session.getAttribute(CsrfGuard.PAGE_TOKENS_KEY);
        	    
        	    // New session should not be having any page tokens so below check should always return false
        	    
                if (pageTokens != null && !(pageTokens instanceof ConcurrentHashMap))
                {
                    synchronized(csrfGuard)
                    {
                        Map<String, String> newPageTokens = new ConcurrentHashMap<String, String>();
                        
                        newPageTokens.putAll(pageTokens);                         
                        session.setAttribute(CsrfGuard.PAGE_TOKENS_KEY, newPageTokens);
                        
                        pageTokens = newPageTokens;
                        
                        Log.auth.info("AjaxClient /client/login : pageTokens map in CsrfGuard replaced with concurrent hash map!!!");
                    }
                }
                
                String tokenFromPages = (pageTokens != null ? pageTokens.get("/resolve/JavaScriptServlet") : null);
                
                if (StringUtils.isBlank(tokenFromPages))
                {
                    if (pageTokens == null)
                    {
                        pageTokens = new ConcurrentHashMap<String, String>();
                        session.setAttribute(CsrfGuard.PAGE_TOKENS_KEY, pageTokens);
                    }
                    
                    try
                    {
                        pageTokens.put("/resolve/JavaScriptServlet", RandomGenerator.generateRandomId(csrfGuard.getPrng(), csrfGuard.getTokenLength()));
                        
                        tokenFromPages = (pageTokens != null ? pageTokens.get("/resolve/JavaScriptServlet") : null);
                        
                        csrfTokenNamesAndVals[3] = tokenFromPages;
                        
                        Log.auth.info("AjaxClient Get CSRFToken For Page /resolve/JavaScriptServlet : After Creation : " + (StringUtils.isNotBlank(tokenFromPages) ? tokenFromPages : "null"));                        
                    } 
                    catch (Exception e)
                    {
                        Log.auth.error("AjaxClient Get CSRFToken For Page /resolve/JavaScriptServlet : unable to generate the random token - " + e.getLocalizedMessage(), e);
                        throw new RuntimeException(String.format("AjaxClient Get CSRFToken For Page /resolve/JavaScriptServlet : unable to generate the random token - " + e.getLocalizedMessage()), e);
                    }
                }
            }
        	
        	AuthenticateResult authResult = Authentication.authenticate(username, password);

            String host = request.getRemoteAddr();
            
            long SESSION_TIMEOUT = (PropertiesUtil.getPropertyLong(ConstantValues.PROPERTIES_SESSION_TIMEOUT) < 720) ?  
                                   720000 : PropertiesUtil.getPropertyLong(ConstantValues.PROPERTIES_SESSION_TIMEOUT) * 1000;
            
            String syslogMessage = authResult.valid ? Authentication.AUTHENTICATION_SYSLOG_RSVIEW_LOGIN_SUCCESS_MESAGE_PREFIX + "RSView Client " + 
                                                      authResult.authType + Authentication.AUTHENTICATION_SYSLOG_RSVIEW_LOGIN_MESSAGE_SUFFIX :
                                                      Authentication.AUTHENTICATION_SYSLOG_RSVIEW_LOGIN_FAILED_MESAGE_PREFIX + "RSView Client " + 
                                                      authResult.authType + Authentication.AUTHENTICATION_SYSLOG_RSVIEW_LOGIN_MESSAGE_SUFFIX;
            Log.syslog.info(SyslogFormatter.INSTANCE.formatAudit(Authentication.getRemoteAddrForSysLog(request), username, request.getLocalAddr(), syslogMessage));
            
            if (authResult.valid)
            {
                String iFrameId = Authentication.getIFrameIdFromIFramePreLoginSessionCache(request);
                Authentication.setAuthToken(request, response, username, host, null, SESSION_TIMEOUT, password, iFrameId);

                result.setData(csrfTokenNamesAndVals);
            }
            
            if (UserUtils.isLockedOut(username))
            {
                throw new Exception ("User account is locked. Please contact system administrator.");
            }
            else
            {
                result.setSuccess(authResult.valid);
            }
        }
        catch (Exception e)
        {
            Log.log.error("error logging the user in", e);
            
            String syslogMessage = Authentication.AUTHENTICATION_SYSLOG_RSVIEW_LOGIN_FAILED_MESAGE_PREFIX + "RSView Client" + Authentication.AUTHENTICATION_SYSLOG_RSVIEW_LOGIN_MESSAGE_SUFFIX;
            Log.syslog.info(SyslogFormatter.INSTANCE.formatAudit(Authentication.getRemoteAddrForSysLog(request), username, request.getLocalAddr(), syslogMessage));
            
            result.setSuccess(false);
            
            if (e.getMessage().toLowerCase().contains("account is locked"))
            {
                result.setMessage(e.getMessage());
            }
            else
            {
                result.setMessage("Error in authenticating user. See log for more info.");
            }
        }

        return result;
    }

    @RequestMapping(value = "/client/logout", method = { RequestMethod.POST }, headers = "Accept=application/json; charset=utf-8")
    @ResponseBody
    public ResponseDTO<String[]> logout(@RequestParam(required = false) String ssoType, HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException
    {

        ResponseDTO<String[]> result = new ResponseDTO<String[]>();
        String sessionId = request.getSession().getId();
        Log.auth.info("LOGOUT - sessionid: " + sessionId);

        try {
            
            String username = (String) request.getAttribute(Constants.HTTP_REQUEST_USERNAME);
    
            // remove token
            Authentication.invalidateForSession(request, response, ssoType);
    
            String syslogMessage = Authentication.AUTHENTICATION_SYSLOG_RSVIEW_LOGIN_SUCCESS_MESAGE_PREFIX + Authentication.AUTHENTICATION_SYSLOG_RSVIEW_LOGOUT_MESSAGE_SUFFIX;
            Log.syslog.info(SyslogFormatter.INSTANCE.formatAudit(Authentication.getRemoteAddrForSysLog(request), username, request.getLocalAddr(), syslogMessage));
            
            Log.auth.info("session active: " + Authentication.getSessionActive() + " max: " + Authentication.getSessionMaxActive());
            
            result.setSuccess(true);

        } catch (Exception e) {

            result.setMessage(e.getMessage());
            result.setSuccess(false);
        }

        return result;
    }

    @SuppressWarnings({ "unchecked", "rawtypes" })
    @RequestMapping(value = "/client/about", method = { RequestMethod.GET }, headers = "Accept=application/json; charset=utf-8")
    @ResponseBody
    public ResponseDTO about() throws ServletException, IOException
    {
        ResponseDTO result = new ResponseDTO();

        try
        {
            result.setSuccess(true).setData(new AboutDTO());
        }
        catch (Exception e)
        {
            Log.log.error("error obtaining about information", e);
            result.setSuccess(false).setMessage("Error in obtaining about information. See log for more info.");
        }

        return result;
    }

    @SuppressWarnings("unchecked")
    @RequestMapping(value = "/client/getUserNotificationsSince", method = { RequestMethod.POST }, headers = "Accept=application/json; charset=utf-8")
    @ResponseBody
    public ResponseDTO<Object> getUserNotificationsSince(@RequestParam long notificationsSince, HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException
    {
        long startTime = System.currentTimeMillis();
        long startTimeInstance = startTime;
        
        ResponseDTO<Object> result = new ResponseDTO<Object>();
        String username = (String) request.getAttribute(Constants.HTTP_REQUEST_USERNAME);

        UserDTO user = new UserDTO();
        user.setBannerAllowToggle(true);
        try
        {
            // Go get user preferences
            UsersVO userVO = ServiceHibernate.getUser(username);
            
            Log.log.trace("Time taken for getting userObj:" + (System.currentTimeMillis() - startTimeInstance) + " msec");
            startTimeInstance = System.currentTimeMillis();
            
            if (userVO == null)
            {
                throw new Exception(username + " User does not exist.");
            }

            user.fromUserVO(userVO);

            QueryDTO socialQuery = new QueryDTO(0, 1);
            QueryFilter filterItem = new QueryFilter("date", StrictISODateTimeFormat.dateTime().print(notificationsSince), "sysUpdatedOn", "after");
            socialQuery.addFilterItem(filterItem);
            
            ResponseDTO<Object> socialResponse = (new AjaxSocial()).listUserStreams(socialQuery, request);
            if (socialResponse.isSuccess())
            {
                user.setNotifications(socialResponse.getRecords());
            }

            Log.log.debug("Time taken for getting listUserStreams in getUserNotificationsSince since " + StrictISODateTimeFormat.dateTime().print(notificationsSince) + 
                          " : " + (System.currentTimeMillis() - startTimeInstance) + " msec, returned " + 
                          (socialResponse.getRecords().isEmpty() ? 0 : (socialResponse.getRecords().size() - 1)) + " stream(s)");
            
            if (Log.log.isDebugEnabled() && !socialResponse.getRecords().isEmpty())
            {
                Log.log.debug("getUserNotificationsSince() Unread Post Counts: " + StringUtils.mapToJson((Map<String, Long>)socialResponse.getRecords().get(socialResponse.getRecords().size() - 1)));
            }
            
            result.setSuccess(true).setData(user);
        }
        catch (Exception e)
        {
            Log.log.error("error in getting the user notifications since " + StrictISODateTimeFormat.dateTime().print(notificationsSince), e);
            result.setSuccess(false).setMessage("Error in getting the user notifications since " + 
                              StrictISODateTimeFormat.dateTime().print(notificationsSince) + ". See log for more info.");
        }
        
        Log.log.debug("getUserNotificationsSince(" + StrictISODateTimeFormat.dateTime().print(notificationsSince) + ") Total Time taken :" + (System.currentTimeMillis() - startTime) + " msec");

        return result;
    }
    
    @SuppressWarnings("unchecked")
    @RequestMapping(value = "/client/pollNotificationsSince", method = { RequestMethod.POST }, headers = "Accept=application/json; charset=utf-8")
    @ResponseBody
    public ResponseDTO<Object> pollNotificationsSince(@RequestParam long notificationsSince, @RequestParam String USource, HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException
    {
        long startTime = System.currentTimeMillis();
        ResponseDTO<Object> result = new ResponseDTO<Object>();

        try
        {
            QueryDTO query = new QueryDTO(0, 1);
            QueryFilter filterItem = new QueryFilter("date", StrictISODateTimeFormat.dateTime().print(notificationsSince), "sysUpdatedOn", "after");
            query.addFilterItem(filterItem);
            
            long startTimeInstance = System.currentTimeMillis();
            ResponseDTO<Object> socialResponse = (new AjaxSocial()).listUserStreams(query, request);
            
            Log.log.debug("Time taken for getting listUserStreams in pollNotificationsSince since " + StrictISODateTimeFormat.dateTime().print(notificationsSince) + 
                          " : " + (System.currentTimeMillis() - startTimeInstance) + " msec, returned " + 
                          (socialResponse.getRecords().isEmpty() ? 0 : (socialResponse.getRecords().size() - 1)) + " notification(s)");
            
            if (Log.log.isDebugEnabled() && !socialResponse.getRecords().isEmpty())
            {
                Log.log.debug("pollNotificationsSince() Unread Post Counts: " + StringUtils.mapToJson((Map<String, Long>)socialResponse.getRecords().get(socialResponse.getRecords().size() - 1)));
            }
            
            if (socialResponse.isSuccess())
            {
                result.setRecords(socialResponse.getRecords());
            }
            
            result.setSuccess(true);
        }
        catch (Exception e)
        {
            Log.log.error("error in polling the user notifications since " + StrictISODateTimeFormat.dateTime().print(notificationsSince), e);
            result.setSuccess(false).setMessage("Error in polling the user notifications since " + 
                                                StrictISODateTimeFormat.dateTime().print(notificationsSince) + 
                                                ". See log for more info.");
        }

        Log.log.debug("pollNotificationsSince() Total Time taken :" + (System.currentTimeMillis() - startTime) + " msec");
        
        return result;
    }

    /**
     * Persist user's specific settings.
     * 
     * @param settingType : type of the property to be persisted.
     * @param settingValue : value to be persisted.
     * @param request : {@link HttpServletRequest} containing <code>USERNAME</code> -
     *  username of the user who is logged in to Resolve.
     * @return {@link ResponseDTO}
     * @throws IOException
     */
    @RequestMapping(value = "/client/{settingType}/save", method = RequestMethod.POST, headers = "Accept=application/json; charset=utf-8")
    @ResponseBody
    public ResponseDTO<String> saveUserSettings(@PathVariable String settingType, @RequestBody String settingValue,
	    HttpServletRequest request) throws IOException {
	ResponseDTO<String> result = new ResponseDTO<>();
	String userName = (String) request.getAttribute(Constants.HTTP_REQUEST_USERNAME);
	
	if (settingType.equalsIgnoreCase(Constants.ANALYTIC_SETTINGS)
		|| settingType.equalsIgnoreCase(Constants.REFRESH_RATE_SETTINGS)
		|| settingType.equalsIgnoreCase(Constants.COLOR_THEME_SETTINGS)) {

	    if (StringUtils.isBlank(settingValue)) {
		ERROR_EMPTY_SETTING_VALUE = String.format(ERROR_EMPTY_SETTING_VALUE, settingType);
		result.setSuccess(false).setMessage(ERROR_EMPTY_SETTING_VALUE);
		return result;
	    }

	    try {
		String property = ServiceHibernate.saveUserSettings(settingType, settingValue.trim(), userName);
		result.setSuccess(true).setData(property);
	    } catch (NumberFormatException e) {
		result.setSuccess(false).setMessage(ERROR_WRONG_REFRESH_RATE_FORMAT);
	    } catch (ColorThemeException e) {
		String error = String.format(ERROR_WRONG_COLOR_THEME, settingValue);
		result.setSuccess(false).setMessage(error);
	    } catch (Exception e) {
		String error = String.format(ERROR_SAVING_SETTINGS, settingType);
		Log.log.error(error, e);
		result.setSuccess(false).setMessage(error);
	    }
	} else {
	    String error = String.format(ERROR_WRONG_SETTINGS_TYPE, settingType);
	    result.setSuccess(false).setMessage(error);
	}

	return result;
    }

    @RequestMapping(value = "/client/getDefaultColorTheme", method = {
	    RequestMethod.GET }, headers = "Accept=application/json; charset=utf-8")
    @ResponseBody
    public ResponseDTO<ColorTheme> getDefaultColorTheme() {
	ResponseDTO<ColorTheme> result = new ResponseDTO<>();
	ColorTheme colorTheme = com.resolve.services.util.UserUtils.getDefaultColorTheme();
	
	result.setSuccess(true).setData(colorTheme);

	return result;
    }
    
    @RequestMapping(value = "/client/colorThemes", method = {
	    RequestMethod.GET }, headers = "Accept=application/json; charset=utf-8")
    @ResponseBody
    public ResponseDTO<EnumSet<ColorTheme>> listColorThemes() {
	ResponseDTO<EnumSet<ColorTheme>> result = new ResponseDTO<>();
	
	result.setSuccess(true).setData(EnumSet.allOf(ColorTheme.class));
	
	return result;
    }

    /**
     * Get the logo and the disclaimer to display on the login screen
     * @param request
     * @param response
     * @return Json object containing the logo and the disclaimer
     * @throws ServletException
     * @throws IOException
     */
    @RequestMapping(value = "/client/getLoginScreenDisplayInfo", method = RequestMethod.GET, headers = "Accept=application/json; charset=utf-8")
    @ResponseBody
    public ResponseDTO<HashMap<String, Object>> getLoginScreenDisplayInfo(
            HttpServletRequest request,
            HttpServletResponse response
            ) throws ServletException, IOException
    {
        ResponseDTO<HashMap<String, Object>> resp = new ResponseDTO<HashMap<String, Object>>();
        HashMap<String, Object> data = new HashMap<String, Object>();
        String rsInstanceType = JspUtils.getResolveInstanceType();
        String logo = Constants.DEFAULT_LOGO;
        String disclaimer = "";
        boolean cloud = rsInstanceType.trim().equalsIgnoreCase("CLOUD");
        try {
            ServletContext context = request.getServletContext();
            File customLogoFile = new File(context.getRealPath(Constants.CUSTOM_LOGO_REALPATH));
            //
            // Custom logo has highest precedence
            //
            if (customLogoFile.exists()){
                logo = Constants.CUSTOM_LOGO;
            } else if (cloud == true){
                logo = Constants.CLOUD_LOGO;
            }
            File disclaimerFile = new File(context.getRealPath(Constants.DISCLAIMER_REALPATH));
            if (!disclaimerFile.exists()) {
                disclaimerFile = new File(context.getRealPath(Constants.DEFAULT_DISCLAIMER_REALPATH));
            }
            if (disclaimerFile.exists()) {
                disclaimer = FileUtils.readFileToString(disclaimerFile, ControllerUtil.ENCODING);
            }
        } catch (Exception e){
            Log.log.warn(e.getMessage(), e);
        }
        data.put("disclaimer", disclaimer);
        data.put("logo", logo);
        data.put("cloud", cloud);
        resp.setData(data).setSuccess(true);
        return resp;
    }
}
