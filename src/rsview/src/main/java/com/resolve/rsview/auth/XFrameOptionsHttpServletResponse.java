/******************************************************************************
* (C) Copyright 2018
*
* Resolve Systems, LLC
*
* All rights reserved
* This software is distributed under license and may not be used, copied,
* modified, or distributed without the express written permission of
* Resolve Systems, LLC.
*
******************************************************************************/

package com.resolve.rsview.auth;

import java.util.Collection;
import java.util.HashSet;
import java.util.Set;

import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpServletResponseWrapper;

import org.owasp.esapi.ESAPI;

import com.resolve.util.HttpUtil;
import com.resolve.util.Log;
import com.resolve.util.StringUtils;

public class XFrameOptionsHttpServletResponse extends HttpServletResponseWrapper
{
    private Set<String> xFrameOptionsHeaderVals;
    
    public XFrameOptionsHttpServletResponse(HttpServletResponse response)
    {
        super(response);
        xFrameOptionsHeaderVals = new HashSet<String>();
        
        Collection<String> xFrameOptHdrVals = response.getHeaders(AuthConstants.X_FRAME_OPTIONS_HEADER.getValue());
        
        if (xFrameOptHdrVals != null && !xFrameOptHdrVals.isEmpty())
        {
            xFrameOptionsHeaderVals.addAll(xFrameOptHdrVals);
        }
        
        Log.auth.trace(AuthConstants.X_FRAME_OPTIONS_HEADER.getValue() + " header values set in response is " + 
                       StringUtils.collectionToString(xFrameOptionsHeaderVals, AuthConstants.COMMA_DELIMITER.getValue()));
    }
    
    @Override
    public String getHeader(String name)
    {
        if (name.equalsIgnoreCase(AuthConstants.X_FRAME_OPTIONS_HEADER.getValue()))
        {
            if (xFrameOptionsHeaderVals != null)
            {
                if (xFrameOptionsHeaderVals.size() > 1)
                {
                    throw new RuntimeException("Use getHeaders() method instead of getHeader() as " + name + " header has more than one unique values set.");
                }
                else
                {
                    return xFrameOptionsHeaderVals.iterator().next();
                }
            }
            else
            {
                if (super.getHeaders(name) != null)
                {
                    if (super.getHeaders(name).size() == 1)
                    {
                        return super.getHeader(name);
                    }
                    else
                    {
                        throw new RuntimeException("Use getHeaders() method instead of getHeader() as " + name + " header has more than one unique values set.");
                    }
                }
                else    
                {
                    return super.getHeader(name);
                }
            }
        }
        else
        {
            return super.getHeader(name);
        }
    }
    
    @Override
    public void addHeader(String name, String value)
    {
    	String safeHeaderName = HttpUtil.getSafeHeaderName(name);
    	String safeHeaderValue = HttpUtil.getSafeHeaderValue(value);
    	
    	if (StringUtils.isNotBlank(safeHeaderName) && StringUtils.isNotBlank(safeHeaderValue)) {
	        if (name.equalsIgnoreCase(AuthConstants.X_FRAME_OPTIONS_HEADER.getValue()))
	        {
	            if (xFrameOptionsHeaderVals == null)
	            {
	                xFrameOptionsHeaderVals = new HashSet<String>();
	            }
	            
	            if (xFrameOptionsHeaderVals.size() == 0)
	            {
	                xFrameOptionsHeaderVals.add(safeHeaderValue);
	            }
	            else if (xFrameOptionsHeaderVals.size() == 1)
	            {
	                if (xFrameOptionsHeaderVals.iterator().next().equals(
	                												AuthConstants.X_FRAME_OPTIONS_SAMEORIGIN.getValue()) &&
	                	safeHeaderValue.startsWith(AuthConstants.X_FRAME_OPTIONS_ALLOW_FROM.getValue()))
	                {
	                    xFrameOptionsHeaderVals.clear();
	                    xFrameOptionsHeaderVals.add(safeHeaderValue);
	                    
	                    // Overwrite the X-Frame-Options header set by STS filter with antiClickJackingEnabled set to true
	                    ESAPI.httpUtilities().setHeader((HttpServletResponse) super.getResponse(), safeHeaderName, 
	                    								safeHeaderValue);
	                }
	                else if (xFrameOptionsHeaderVals.iterator().next().
	                		 startsWith(AuthConstants.X_FRAME_OPTIONS_ALLOW_FROM.getValue()) &&
	                		 safeHeaderValue.equals(AuthConstants.X_FRAME_OPTIONS_SAMEORIGIN.getValue()))
	                {
	                    Log.auth.trace("Attempt to addHeader(" + name + ", " + value + ") in response rejected as " + 
	                    			   safeHeaderValue + " is already set to " + xFrameOptionsHeaderVals.iterator().next() + ".");
	                }
	            }
	        }
	        else
	        {            
	        	ESAPI.httpUtilities().addHeader((HttpServletResponse) super.getResponse(), 
	        									safeHeaderName, safeHeaderValue);
	        }
    	} else {
    		Log.auth.warn("Attempt to addHeader name [" + name + "], value [" + value + 
    					  "] failed. Unable to get safe header name value.");
    	}
    }
    
    @Override
    public void setHeader(String name, String value)
    {
    	String safeHeaderName = HttpUtil.getSafeHeaderName(name);
    	String safeHeaderValue = HttpUtil.getSafeHeaderValue(value);
    	
	    if (StringUtils.isNotBlank(safeHeaderName) && StringUtils.isNotBlank(safeHeaderValue)) {
	        if (safeHeaderName.equalsIgnoreCase(AuthConstants.X_FRAME_OPTIONS_HEADER.getValue()))
	        {
	        	ESAPI.httpUtilities().addHeader((HttpServletResponse) super.getResponse(), safeHeaderName, safeHeaderValue);                
	            Log.auth.trace("setHeader(" + safeHeaderName + ", " + safeHeaderValue + "), " + safeHeaderName + 
	            			   " header values set in response is " + 
	                           StringUtils.collectionToString(
	                        		   xFrameOptionsHeaderVals, AuthConstants.COMMA_DELIMITER.getValue()));
	        }
	        else
	        {
	        	ESAPI.httpUtilities().setHeader((HttpServletResponse) super.getResponse(), safeHeaderName, safeHeaderValue);
	        }
	    } else {
	    	Log.auth.warn("Attempt to setHeader name [" + safeHeaderName + "], value [" + safeHeaderValue + 
					  	  "] failed. Unable to get safe header name value.");
	    }
    }
}
