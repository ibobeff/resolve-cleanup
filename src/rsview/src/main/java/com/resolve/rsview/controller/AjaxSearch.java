/******************************************************************************
* (C) Copyright 2014
*
* Resolve Systems, LLC
*
* All rights reserved
* This software is distributed under license and may not be used, copied,
* modified, or distributed without the express written permission of
* Resolve Systems, LLC.
*
******************************************************************************/

package com.resolve.rsview.controller;

import java.io.IOException;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.elasticsearch.ElasticsearchException;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Controller;
import org.springframework.stereotype.Service;
import org.springframework.validation.BindException;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.ResponseStatus;

import com.resolve.search.QueryAPI;
import com.resolve.search.actiontask.ActionTaskSearchAPI;
import com.resolve.search.automation.AutomationSearchAPI;
import com.resolve.search.content.ContentSearchAPI;
import com.resolve.search.model.ActionTask;
import com.resolve.search.model.AutomationSearchResponse;
import com.resolve.search.model.SocialPostResponse;
import com.resolve.search.model.UserInfo;
import com.resolve.search.model.WikiDocument;
import com.resolve.search.model.query.GlobalQuery;
import com.resolve.search.model.query.Query;
import com.resolve.search.model.query.UserQuery;
import com.resolve.services.ServiceHibernate;
import com.resolve.services.ServiceSocial;
import com.resolve.services.ServiceWiki;
import com.resolve.services.graph.social.model.component.User;
import com.resolve.services.hibernate.vo.NamespaceVO;
import com.resolve.services.hibernate.vo.UsersVO;
import com.resolve.services.util.UserUtils;
import com.resolve.services.validator.sequence.ValidationSequence;
import com.resolve.services.vo.QueryDTO;
import com.resolve.services.vo.QueryFilter;
import com.resolve.services.vo.ResponseDTO;
import com.resolve.services.vo.SearchAttributesDTO;
import com.resolve.services.vo.SearchRecordDTO;
import com.resolve.services.vo.SearchResponseDTO;
import com.resolve.social.SocialUtil;
import com.resolve.util.ConstantMessages;
import com.resolve.util.Constants;
import com.resolve.util.Log;
import com.resolve.util.StringUtils;

/**
 * 
 * Controller for search.
 * 
 * <p>
 * <dt>1. search Catagories:</dt>
 * <p>
 * <dd>Document, Social, Automation</dd>
 * <p>
 * <dt>2. search filter: </dt>
 * <p>
 * <dd>              Search Type:</dd>
 * <dd><dd>                       Document: Document, DecisionTree, Attachment</dd></dd>
 * <dd><dd>                       Social:   Post, Notification</dd></dd>
 * <dd><dd>                       Automation: Runbook, ActionTask</dd></dd>
 * <p>
 * <dd>              Search In:   Document: Title, Content, User</dd>
 * <dd><dd>                       Social:</dd></dd>
 * <dd><dd>                       Automation: Title, Content, User</dd></dd>
 * <p>
 * <dd>              Search Time: Today, Week, Month, Older than</dd>
 * <p>
 * <dd>              Namespace: resolve namespaces</dd>
 * <p>
 * <dd>              Tag: resolve tags</dd>
 * <p>
 * <dt>3. search key word</dt>
 * <p>
 * <dt>4. search suggestions: Did you mean:</dt>
 * <p>
 * <dt>5. search home:</dt>
 * <dd>           my recent search: my recent search query</dd>
 * <dd>           top search: top search query for all</dd>
 * <dd>           top viewd results: top cliked or previewed search results</dd>
 *
 */
@Controller
@Service
public class AjaxSearch extends GenericController
{
	private static final String ERROR_GETTING_AUTOMATION_SUGGESTIONS = "Error retrieving data through getAutomationSuggestions";
	private static final String ERROR_GETTING_SOCIAL_SUGGESTIONS = "Error retrieving data through getSocialSuggestions";
	private static final String ERROR_RETRIEVING_DATA = "Error retrieving data through getSuggestions";
	private static final String ERROR_RETRIEVING_NAMESPACES = "Error retrieving namespaces";
	private static final String ERROR_RETRIEVING_TOP_VIEWED_SEARCH_RESULTS = "Error retrieving Top Viewed Search Results";
	private static final String ERROR_RETRIEVING_USER_QUERY_HISTORY = "Error retrieving user query history";
	private static final String ERROR_SEARCHING_ACTION_TASKS = "Error searching action tasks";
	private static final String ERROR_SEARCHING_AUTOMATIONS = "Error searching automations";
	private static final String ERROR_SEARCHING_POSTS = "Error searching posts";
	private static final String ERROR_SEARCHING_WIKIDOCUMENT = "Error searching wikidocument";
	private static final String ERROR_UPDATE_VIEWEDSEARCHRESULT = "Error updateViewedsearchresult";
    private static final String ERROR_RETRIEVING_TOP_QUERIES = "Error retrieving top queries";
    private static final String ERROR_SAVING_RECENT_ITEM = "Error saving recently opened item";
    private static final String ERROR_RETRIEVING_RECENT_ITEMS = "Error retrieving recently opened items";
    private static final String ERROR_SAVING_RECENT_SEARCH = "Error saving recent search";
    private static final String ERROR_RETRIEVING_RECENT_SEARCHES = "Error retrieving recent searches";
    
	@SuppressWarnings("rawtypes")
	@ExceptionHandler(BindException.class)
	@ResponseStatus(HttpStatus.BAD_REQUEST)
	@ResponseBody
	@Override
	public ResponseDTO handle(BindException e) throws Throwable {
		ResponseDTO result = new ResponseDTO();
		result.setSuccess(false);
		StringBuilder sb = new StringBuilder();
		e.getBindingResult().getAllErrors().forEach(error -> {
			sb.append(error.getDefaultMessage()).append("\n");
		});

		Log.log.error("Error parsing incoming request", e);

		return result.setMessage(sb.toString());
	}
    
    
	/**
	 * Provides aggregated search for items in the systems such as:
	 * 
	 * <ul>
	 * <li>Wiki Page</li>
	 * <li>Automation</li>
	 * <li>Action Task</li>
	 * <li>Decision Tree</li>
	 * <li>Custom Form</li>
	 * <li>Property</li>
	 * <li>Playbook</li>
	 * </ul>
	 * 
	 */
	@RequestMapping(value = "/search", method = { RequestMethod.POST,
			RequestMethod.GET }, headers = "Accept=application/json; charset=utf-8")
	@ResponseBody
	public SearchResponseDTO search(@ModelAttribute @Validated(ValidationSequence.class) SearchAttributesDTO searchDTO,
			@RequestParam(defaultValue = "") String problemId,
			@RequestParam(defaultValue = "", value = "RESOLVE.ORG_ID") String orgId,
			@RequestParam(defaultValue = "", value = "RESOLVE.ORG_NAME") String orgName, HttpServletRequest request) {

		SearchResponseDTO result = new SearchResponseDTO();
		try {
			String username = (String) request.getAttribute(Constants.HTTP_REQUEST_USERNAME);
			UsersVO user = UserUtils.getUser(username);
			result = ContentSearchAPI.searchAllDocuments(searchDTO, getUserInfo(user));
		} catch (ElasticsearchException e) {
			Log.log.error(ConstantMessages.ERROR_SEARCHING_WITH_TERM, e);
			result.setSuccess(false).setMessage(ConstantMessages.ERROR_SEARCHING_WITH_TERM);
		} catch (Exception e) {
			Log.log.error(ConstantMessages.ERROR_PERFORMING_SEARCH, e);
			result.setSuccess(false).setMessage(ConstantMessages.ERROR_PERFORMING_SEARCH);
		}
		return result;
	}

	/**
	 * Saves search criteria for display under last saved searches and returns the
	 * updated list.
	 */
	@RequestMapping(value = "/search/save", method = {
			RequestMethod.POST }, headers = "Accept=application/json; charset=utf-8")
	@ResponseBody
	public ResponseDTO<SearchAttributesDTO> saveRecentSearch(
			@ModelAttribute @Validated(ValidationSequence.class) SearchAttributesDTO searchAttributesDTO,
			@RequestParam(defaultValue = "") String problemId,
			@RequestParam(defaultValue = "", value = "RESOLVE.ORG_ID") String orgId,
			@RequestParam(defaultValue = "", value = "RESOLVE.ORG_NAME") String orgName, HttpServletRequest request)
			throws Exception {

		ResponseDTO<SearchAttributesDTO> result = new ResponseDTO<SearchAttributesDTO>();

		try {
			String username = (String) request.getAttribute(Constants.HTTP_REQUEST_USERNAME);
			ServiceHibernate.saveRecentSearch(searchAttributesDTO, username);
			List<SearchAttributesDTO> savedSearches = ServiceHibernate.listRecentSearches(username);

			result.setTotal(savedSearches.size());
			result.setSuccess(true).setRecords(savedSearches);
		} catch (Exception e) {
			Log.log.error(ERROR_SAVING_RECENT_SEARCH, e);
			result.setSuccess(false).setMessage(ERROR_SAVING_RECENT_SEARCH);
		}

		return result;
	}

	/**
	 * Lists last saved searches.
	 */
	@RequestMapping(value = "/search/list", method = {
			RequestMethod.GET }, headers = "Accept=application/json; charset=utf-8")
	@ResponseBody
	public ResponseDTO<SearchAttributesDTO> listRecentSearches(@RequestParam(defaultValue = "") String problemId,
			@RequestParam(defaultValue = "", value = "RESOLVE.ORG_ID") String orgId,
			@RequestParam(defaultValue = "", value = "RESOLVE.ORG_NAME") String orgName, HttpServletRequest request)
			throws Exception {

		ResponseDTO<SearchAttributesDTO> result = new ResponseDTO<SearchAttributesDTO>();

		try {
			String username = (String) request.getAttribute(Constants.HTTP_REQUEST_USERNAME);
			List<SearchAttributesDTO> savedSearches = ServiceHibernate.listRecentSearches(username);
			if (savedSearches != null) {
				result.setTotal(savedSearches.size());
			}
			result.setSuccess(true).setRecords(savedSearches);
		} catch (Exception e) {
			Log.log.error(ERROR_RETRIEVING_RECENT_SEARCHES, e);
			result.setSuccess(false).setMessage(ERROR_RETRIEVING_RECENT_SEARCHES);
		}

		return result;
	}

	/**
	 * Saves last opened item from search and returns the updated list.
	 */
	@RequestMapping(value = "/search/item/save", method = {
			RequestMethod.POST }, headers = "Accept=application/json; charset=utf-8")
	@ResponseBody
	public ResponseDTO<SearchRecordDTO> saveOpenedItem(
			@ModelAttribute @Validated(ValidationSequence.class) SearchRecordDTO searchRecordDTO,
			HttpServletRequest request) throws Exception {

		ResponseDTO<SearchRecordDTO> result = new ResponseDTO<SearchRecordDTO>();

		try {
			String username = (String) request.getAttribute(Constants.HTTP_REQUEST_USERNAME);
			ServiceHibernate.saveRecentlyOpenedItem(searchRecordDTO, username);
			List<SearchRecordDTO> savedOpenedItems = ServiceHibernate.listRecentlyOpenedItems(username);

			result.setTotal(savedOpenedItems.size());
			result.setSuccess(true).setRecords(savedOpenedItems);
		} catch (Exception e) {
			Log.log.error(ERROR_SAVING_RECENT_ITEM, e);
			result.setSuccess(false).setMessage(ERROR_SAVING_RECENT_ITEM);
		}

		return result;
	}
	
	/**
	 * Lists last opened items from search.
	 */
	@RequestMapping(value = "/search/item/list", method = {
			RequestMethod.GET }, headers = "Accept=application/json; charset=utf-8")
	@ResponseBody
	public ResponseDTO<SearchRecordDTO> listRecentOpenedItems(HttpServletRequest request) throws Exception {

		ResponseDTO<SearchRecordDTO> result = new ResponseDTO<SearchRecordDTO>();

		try {
			String username = (String) request.getAttribute(Constants.HTTP_REQUEST_USERNAME);
			List<SearchRecordDTO> savedOpenedItems = ServiceHibernate.listRecentlyOpenedItems(username);
			if (savedOpenedItems != null) {
				result.setTotal(savedOpenedItems.size());
			}
			result.setSuccess(true).setRecords(savedOpenedItems);
		} catch (Exception e) {
			Log.log.error(ERROR_RETRIEVING_RECENT_ITEMS, e);
			result.setSuccess(false).setMessage(ERROR_RETRIEVING_RECENT_ITEMS);
		}

		return result;
	}
	
	/**
     * 
     * @deprecated
     * Same as the AjaxTag.listTags
     * 
     * Gets the list of tags in the system for the user to select tags for filter search results.
     *
     * @param query
     *            : QueryDTO - provides paging, sorting, filtering (See {@link QueryDTO} for more information)
     * @param request : {@link HttpServletRequest}
     * @return
     * @throws ServletException
     * @throws IOException
     */
    @RequestMapping(value = "/search/tags", method = { RequestMethod.GET }, headers = "Accept=application/json; charset=utf-8")
    @ResponseBody
    public ResponseDTO listTags(@ModelAttribute QueryDTO query, HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException
    {
        return new AjaxTag().listTags(query, request, response);
    }

    /**
     * Gets the list of namespace in the system for the user to select namespace for filter search results.
     *
     * @param query
     *            : QueryDTO - provides paging, sorting, filtering (See QueryDTO for more information)
     */
    @RequestMapping(value = "/search/namespaces", method = { RequestMethod.GET }, headers = "Accept=application/json; charset=utf-8")
    @ResponseBody
    public ResponseDTO<NamespaceVO> listNamespaces(@ModelAttribute QueryDTO query) throws ServletException, IOException
    {
        ResponseDTO<NamespaceVO> result = new ResponseDTO<NamespaceVO>();

        try
        {
            List<NamespaceVO> namespaces = ServiceHibernate.getAllNamespaces(query, true);
            if (namespaces != null)
            {
                result.setTotal(namespaces.size());
            }
            result.setSuccess(true).setRecords(namespaces);
        }
        catch (Exception e)
        {
            Log.log.error(ERROR_RETRIEVING_NAMESPACES, e);
            result.setSuccess(false).setMessage(ERROR_RETRIEVING_NAMESPACES);
        }

        return result;
    }

    /**
     * viewed search results
     *
     */
    @RequestMapping(value = "/search/viewedsearchresult", method = { RequestMethod.POST, RequestMethod.GET }, headers = "Accept=application/json; charset=utf-8")
    @ResponseBody
    public ResponseDTO<Serializable> updateViewedResult(@RequestParam String id, @RequestParam String type, HttpServletRequest request) throws ServletException, IOException
    {
        ResponseDTO<Serializable> result = new ResponseDTO<Serializable>();

        try
        {
            //BD: let's change it to ES based one
            //SearchMonitor.addViewedData(id);
            result.setSuccess(true).setRecords(null);
        }
        catch (Exception e)
        {
            Log.log.error(ERROR_UPDATE_VIEWEDSEARCHRESULT, e);
            result.setSuccess(false).setMessage(ERROR_UPDATE_VIEWEDSEARCHRESULT);
        }

        return result;
    }

    /**
     * Gets the list of top viewed results for all users
     *
     */
    @RequestMapping(value = "/search/getTopViewedResults", method = { RequestMethod.POST, RequestMethod.GET }, headers = "Accept=application/json; charset=utf-8")
    @ResponseBody
    public ResponseDTO getTopViewedResultsOfAll(@ModelAttribute QueryDTO query, HttpServletRequest request) throws ServletException, IOException
    {
        ResponseDTO result = new ResponseDTO();

        try
        {
            //BD: let's change it to ES based one
            //List<Map<String, Object>> data = SearchMonitor.getTopViewedData();
            //result.setTotal(data.size());
            //result.setSuccess(true).setRecords(data);
        }
        catch (Exception e)
        {
            Log.log.error(ERROR_RETRIEVING_TOP_VIEWED_SEARCH_RESULTS, e);
            result.setSuccess(false).setMessage(ERROR_RETRIEVING_TOP_VIEWED_SEARCH_RESULTS);
        }

        return result;
    }

    /**
     * Search Social Posts in Resolve by typing a text in the search box and clicking the search button on the title bar. The result would contain a list of <code>SocialPostResponse</code>
     * <br>
     * The Document tab is selected by default. Click on Social tab to get the expected result.
     * 
     * @param problemId : String current problem ID.
     * @param query : provides paging, sorting, filtering (See {@link QueryDTO} for more information)
     * @param request : {@link HttpServletRequest}
     * @return
     * @throws ServletException
     * @throws IOException
     */
    @RequestMapping(value = "/search/searchPosts", method = { RequestMethod.POST, RequestMethod.GET })
    @ResponseBody
    public ResponseDTO<SocialPostResponse> searchPosts(@RequestParam(defaultValue = "") String problemId, @ModelAttribute QueryDTO query, @RequestParam(defaultValue="", value="RESOLVE.ORG_ID") String orgId, @RequestParam(defaultValue="", value="RESOLVE.ORG_NAME") String orgName, HttpServletRequest request) throws ServletException, IOException
    {

        ResponseDTO<SocialPostResponse> result = new ResponseDTO<SocialPostResponse>();
        User user = SocialUtil.getSocialUser(request);
        try
        {
            if (StringUtils.isNotBlank(query.getSearchTerm()))
            {
                //Post to social that the user is searching wikis for the searchTerm
                if (StringUtils.isNotEmpty(problemId))
                {
                    List<String> streams = new ArrayList<String>();
                    streams.add(problemId);
                    (new AjaxSocial()).submit(streams, "worksheet", "Searched Social", "Searched Social for: <a href=\"#RS.search.Main/search=" + query.getSearchTerm() + "&activeItem=1\">" + query.getSearchTerm()+ "</a>", "", orgId, orgName, request);
                }
                result = ServiceSocial.searchPosts(query, null, null, true, false, user);
            }
            else
            {
                result = ServiceSocial.advancedSearch(query, user);
            }
        }
        catch (Exception e)
        {
            Log.log.error(e.getMessage(), e);
        }

        return result;
    }

    /**
     * Search WikiDoc in Resolve by typing either full or partial document name in the search box and clicking the search button on the title bar.
     * 
     * @param problemId : String current problem ID.
     * @param queryDTO : provides paging, sorting, filtering (See {@link QueryDTO} for more information)
     * @param request : {@link HttpServletRequest}
     * @return
     * @throws ServletException
     * @throws IOException
     */
    @RequestMapping(value = "/search/searchWikis", method = { RequestMethod.POST, RequestMethod.GET })
    @ResponseBody
    public ResponseDTO<WikiDocument> searchWikis(@RequestParam(defaultValue = "") String problemId, @ModelAttribute QueryDTO queryDTO, @RequestParam(defaultValue="", value="RESOLVE.ORG_ID") String orgId, @RequestParam(defaultValue="", value="RESOLVE.ORG_NAME") String orgName, HttpServletRequest request) throws ServletException, IOException
    {

        ResponseDTO<WikiDocument> result = new ResponseDTO<WikiDocument>();

        try
        {
            String username = (String) request.getAttribute(Constants.HTTP_REQUEST_USERNAME);
            UsersVO user = UserUtils.getUser(username);
            String searchTerm = queryDTO.getSearchTerm();
            if(StringUtils.isBlank(searchTerm))
            {
                //find the term from one of the filters
                for(QueryFilter filter : queryDTO.getFilters())
                {
                    if(StringUtils.isBlank(filter.getField()))
                    {
                        searchTerm = filter.getValue();
                        break;
                    }
                }
            }
            if (StringUtils.isNotBlank(searchTerm) && StringUtils.isNotBlank(problemId))
            {
                List<String> streams = new ArrayList<String>();
                streams.add(problemId);
                (new AjaxSocial()).submit(streams, "worksheet", "Searched Wiki Documents", "Searched Wiki Documents for: <a href=\"#RS.search.Main/search=" + searchTerm + "&activeItem=0\">" + searchTerm + "</a>", "", orgId, orgName, request);
            }
            //TODO nowadays everything goes through advanced search.
//            boolean sirPlaybookTemplatesPermission = com.resolve.services.hibernate.util.UserUtils.getUserfunctionPermission(username, "sirPlaybookTemplates", "view");
//            if (!sirPlaybookTemplatesPermission) {
//                // Since RBAC, if user doesn't have permission to view playbook templates, exclude them from the query
//                queryDTO.addFilterItem(new QueryFilter("type", QueryFilter.NOT_EQUALS, "playbook"));
//            }
            result = ServiceWiki.advancedSearch(queryDTO, getUserInfo(user));
            result.setSuccess(true);
        }
        catch (Exception e)
        {
            Log.log.error(ERROR_SEARCHING_POSTS, e);
            result.setSuccess(false).setMessage(ERROR_SEARCHING_POSTS);
        }

        return result;

    }

    /**
     * Search Automations in Resolve by typing a text in the search box and clicking the search button on the title bar.
     * <br>
     * The Document tab is selected by default. Click on Automation tab to get the expected result.
     * <p>
     * @param problemId : String current problem ID.
     * @param queryDTO : provides paging, sorting, filtering (See {@link QueryDTO} for more information)
     * @param request : {@link HttpServletRequest}
     * @return
     * @throws ServletException
     * @throws IOException
     */
    
    @RequestMapping(value = "/search/searchAutomations", method = { RequestMethod.POST, RequestMethod.GET })
    @ResponseBody
    public ResponseDTO<AutomationSearchResponse> searchAutomations(@RequestParam(defaultValue = "") String problemId, @ModelAttribute QueryDTO queryDTO, @RequestParam(defaultValue="", value="RESOLVE.ORG_ID") String orgId, @RequestParam(defaultValue="", value="RESOLVE.ORG_NAME") String orgName, HttpServletRequest request) throws ServletException, IOException
    {
        ResponseDTO<AutomationSearchResponse> result = new ResponseDTO<AutomationSearchResponse>();

        try
        {
            String username = (String) request.getAttribute(Constants.HTTP_REQUEST_USERNAME);
            UsersVO user = UserUtils.getUser(username);

            if (StringUtils.isNotBlank(queryDTO.getSearchTerm()) && StringUtils.isNotBlank(problemId))
            {
                //Post to social that the user is searching wikis for the searchTerm
                List<String> streams = new ArrayList<String>();
                streams.add(problemId);
                (new AjaxSocial()).submit(streams, "worksheet", "Searched Automation", "Searched Automation for: <a href=\"#RS.search.Main/search=" + queryDTO.getSearchTerm() + "&activeItem=2\">" + queryDTO.getSearchTerm()+ "</a>", "", orgId, orgName, request);
            }

            //the result has the total already set in searchPosts call.
            result = AutomationSearchAPI.searchAutomations(queryDTO, getUserInfo(user));

            result.setSuccess(true);
        }
        catch (Exception e)
        {
            Log.log.error(ERROR_SEARCHING_AUTOMATIONS, e);
            result.setSuccess(false).setMessage(ERROR_SEARCHING_AUTOMATIONS);
        }

        return result;

    }

    @RequestMapping(value = "/search/searchByTag", method = { RequestMethod.POST, RequestMethod.GET })
    @ResponseBody
    public ResponseDTO<WikiDocument> searchByTag(@RequestParam String tag, @ModelAttribute QueryDTO queryDTO, HttpServletRequest request) throws ServletException, IOException
    {

        ResponseDTO<WikiDocument> result = new ResponseDTO<WikiDocument>();

        try
        {
            if(StringUtils.isNotBlank(tag))
            {
                String username = (String) request.getAttribute(Constants.HTTP_REQUEST_USERNAME);
                UsersVO user = UserUtils.getUser(username);
                queryDTO.addFilterItem(new QueryFilter("tag", QueryFilter.EQUALS, tag));
                result = ServiceWiki.advancedSearch(queryDTO, getUserInfo(user));
    
                result.setSuccess(true);
            }
        }
        catch (Exception e)
        {
            Log.log.error(ERROR_SEARCHING_WIKIDOCUMENT, e);
            result.setSuccess(false).setMessage(ERROR_SEARCHING_WIKIDOCUMENT);
        }

        return result;

    }

    @RequestMapping(value = "/search/searchActionTask", method = { RequestMethod.POST, RequestMethod.GET })
    @ResponseBody
    public ResponseDTO<ActionTask> searchActionTask(@ModelAttribute QueryDTO queryDTO, HttpServletRequest request) throws ServletException, IOException
    {

        ResponseDTO<ActionTask> result = new ResponseDTO<ActionTask>();

        try
        {
            String username = (String) request.getAttribute(Constants.HTTP_REQUEST_USERNAME);
            UsersVO user = UserUtils.getUser(username);

            UserInfo userInfo = new UserInfo(user.getSys_id(), user.getSysOrg(), user.getUUserName(), user.getRoles(), UserUtils.isAdminUser(user));
            result = ActionTaskSearchAPI.searchActionTasks(queryDTO, userInfo);

            result.setSuccess(true);
        }
        catch (Exception e)
        {
            Log.log.error(ERROR_SEARCHING_ACTION_TASKS, e);
            result.setSuccess(false).setMessage(ERROR_SEARCHING_ACTION_TASKS);
        }

        return result;

    }
    
    @RequestMapping(value = "/search/getWikiNameSuggestions", method = { RequestMethod.POST, RequestMethod.GET })
    @ResponseBody
    public ResponseDTO<Query> getWikiNameSuggestions(@ModelAttribute QueryDTO queryDTO, HttpServletRequest request) throws ServletException, IOException
    {
        ResponseDTO<Query> result = new ResponseDTO<Query>();

        try
        {
            String username = (String) request.getAttribute(Constants.HTTP_REQUEST_USERNAME);
            result = QueryAPI.getWikiNameSuggestions(queryDTO, username);
            result.setSuccess(true);
        }
        catch (Exception e)
        {
            Log.log.error(ERROR_RETRIEVING_DATA, e);
            result.setSuccess(false).setMessage(ERROR_RETRIEVING_DATA);
        }
        return result;
    }

    /**
     * Get the document name suggestions upon typing a text in the search bar on <code>/resolve/jsp/rsclient.jsp#RS.search.Main</code>
     * <br>
     * The result contains a list of DocumentQuery containing suggestion.
     * 
     * @param queryDTO : provides paging, sorting, filtering (See {@link QueryDTO} for more information) In this case, <code>searchTerm</code> property is populated by a partial string.
     * @param request : {@link HttpServletRequest}
     * @return
     * @throws ServletException
     * @throws IOException
     */
    @RequestMapping(value = "/search/getDocumentSuggestions", method = { RequestMethod.POST, RequestMethod.GET })
    @ResponseBody
    public ResponseDTO<Query> getDocumentSuggestions(@ModelAttribute QueryDTO queryDTO, HttpServletRequest request) throws ServletException, IOException
    {
        ResponseDTO<Query> result = new ResponseDTO<Query>();

        try
        {
            String username = (String) request.getAttribute(Constants.HTTP_REQUEST_USERNAME);
            result = QueryAPI.getDocumentQuerySuggestions(queryDTO, username);
            result.setSuccess(true);
        }
        catch (Exception e)
        {
            Log.log.error(ERROR_RETRIEVING_DATA, e);
            result.setSuccess(false).setMessage(ERROR_RETRIEVING_DATA);
        }
        return result;
    }

    /**
     * Get the social suggestions upon typing at least 4 chars in the search bar on <code>/resolve/jsp/rsclient.jsp#RS.search.Main</code> when the Social tab is clicked.
     * <br>
     * The result contains a list of SocialQuery containing suggestion.
     * 
     * @param queryDTO : provides paging, sorting, filtering (See {@link QueryDTO} for more information) In this case, <code>searchTerm</code> property is populated by a partial string.
     * @param request : {@link HttpServletRequest}
     * @return
     * @throws ServletException
     * @throws IOException
     */
    @RequestMapping(value = "/search/getSocialSuggestions", method = { RequestMethod.POST, RequestMethod.GET })
    @ResponseBody
    public ResponseDTO<Query> getSocialSuggestions(@ModelAttribute QueryDTO queryDTO, HttpServletRequest request) throws ServletException, IOException
    {
        ResponseDTO<Query> result = new ResponseDTO<Query>();

        try
        {
            String username = (String) request.getAttribute(Constants.HTTP_REQUEST_USERNAME);
            result = QueryAPI.getSocialQuerySuggestions(queryDTO, username);
            result.setSuccess(true);
        }
        catch (Exception e)
        {
            Log.log.error(ERROR_GETTING_SOCIAL_SUGGESTIONS, e);
            result.setSuccess(false).setMessage(ERROR_GETTING_SOCIAL_SUGGESTIONS);
        }
        return result;
    }

    /**
     * Get the Automation name suggestions upon typing a text in the search bar on <code>/resolve/jsp/rsclient.jsp#RS.search.Main</code>
     * <br>
     * The result contains a list of DocumentQuery containing suggestion.
     * 
     * @param queryDTO : provides paging, sorting, filtering (See {@link QueryDTO} for more information) In this case, <code>searchTerm</code> property is populated by a partial string.
     * @param request : {@link HttpServletRequest}
     * @return
     * @throws ServletException
     * @throws IOException
     */
    @RequestMapping(value = "/search/getAutomationSuggestions", method = { RequestMethod.POST, RequestMethod.GET })
    @ResponseBody
    public ResponseDTO<Query> getAutomationSuggestions(@ModelAttribute QueryDTO queryDTO, HttpServletRequest request) throws ServletException, IOException
    {
        ResponseDTO<Query> result = new ResponseDTO<Query>();

        try
        {
            String username = (String) request.getAttribute(Constants.HTTP_REQUEST_USERNAME);
            result = QueryAPI.getAutomationQuerySuggestions(queryDTO, username);
            result.setSuccess(true);
        }
        catch (Exception e)
        {
            Log.log.error(ERROR_GETTING_AUTOMATION_SUGGESTIONS, e);
            result.setSuccess(false).setMessage(ERROR_GETTING_AUTOMATION_SUGGESTIONS);
        }
        return result;
    }

    /**
     * Get list of queries made my current user along with the date and time of the query.
     * <br>
     * Result contains the list of <code>UserQuerie</code> object.
     * 
     * @param queryDTO : provides paging, sorting, filtering (See {@link QueryDTO} for more information)
     * @param request {@link HttpServletRequest}
     * @return
     * @throws ServletException
     * @throws IOException
     */
    @RequestMapping(value = "/search/getUserQuery", method = { RequestMethod.POST, RequestMethod.GET })
    @ResponseBody
    public ResponseDTO<UserQuery> getUserQueryHistory(QueryDTO queryDTO, HttpServletRequest request) throws ServletException, IOException
    {
        ResponseDTO<UserQuery> result = new ResponseDTO<UserQuery>();

        try
        {
            String username = (String) request.getAttribute(Constants.HTTP_REQUEST_USERNAME);
            result = QueryAPI.getUserQueryHistory(queryDTO, username);
            result.setSuccess(true);
        }
        catch (Exception e)
        {
            Log.log.error(ERROR_RETRIEVING_USER_QUERY_HISTORY, e);
            result.setSuccess(false).setMessage(ERROR_RETRIEVING_USER_QUERY_HISTORY);
        }
        return result;
    }

    /**
     * Get the list of queries along with the number of times the query is fired.
     * </br>
     * Result contains list of <code>GlobalQuery</code> object.
     * 
     * @param queryDTO : provides paging, sorting, filtering (See {@link QueryDTO} for more information)
     * @param request {@link HttpServletRequest}
     * @return
     * @throws ServletException
     * @throws IOException
     */
    @RequestMapping(value = "/search/getTopQueries", method = { RequestMethod.POST, RequestMethod.GET })
    @ResponseBody
    public ResponseDTO<GlobalQuery> getTopQueries(QueryDTO queryDTO, HttpServletRequest request) throws ServletException, IOException
    {
        ResponseDTO<GlobalQuery> result = new ResponseDTO<GlobalQuery>();

        try
        {
            String username = (String) request.getAttribute(Constants.HTTP_REQUEST_USERNAME);
            //queryDTO will have the number start and limit
            result = QueryAPI.getTopQuery(queryDTO, username);
            result.setSuccess(true);
        }
        catch (Exception e)
        {
            Log.log.error(ERROR_RETRIEVING_TOP_QUERIES, e);
            result.setSuccess(false).setMessage(ERROR_RETRIEVING_TOP_QUERIES);
        }
        return result;
    }
}
