package com.resolve.rsview.controller;

import java.util.List;
import java.util.Optional;

import javax.servlet.http.HttpServletRequest;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.resolve.services.hibernate.util.CaseConfigUtil;
import com.resolve.services.hibernate.vo.ResolveCaseConfigVO;
import com.resolve.services.vo.ResponseDTO;
import com.resolve.util.Constants;
import com.resolve.util.Log;
import com.resolve.util.StringUtils;

@Controller
@RequestMapping("/case")
public class AjaxCaseConfig
{
	private static final String ERROR_RETRIEVING_CASE_CONFIG_OF_TYPE = "Error retrieving case configuration(s) of %s type.";
	private static final String ERROR_GETTING_IOC = "Error getting %s ioc(s).";
	private static final String ERROR_SAVING_CASE_CONFIG = "Error saving case configuration(s) of %s type.%s";
	private static final String CASE_CONFIG_TYPE_UNSPECIFIED = "unspecified";
	private static final String WARNING_MARKER = "WARNING: ";
	private static final String GLOBAL = "global";
	private static final String ALL = "all";

	/**
	 * List Case Configuration specified by configType.<br>
	 * 
     * Possible values configType could take are CaseType, CaseStatus, CasePriority, CaseClassification, IOC
     * or CaseAttribute. Any other value will result in an empty list.<br>
     * 
     * The list will be filtered by sysOrg if specified.<br>
     *
	 * @param configType : String representing configType of case configuration.
	 * @param sysOrg : String representing organization id.
	 * @param forCase : boolean flag if set, result list will not contain deactivated configuration.
	 * @param request : {@link HttpServletRequest}
	 * @return
	 *     List of {@link ResolveCaseConfigVO}
	 */
	@RequestMapping(value = {"/{configType}", "/{configType}/{sysOrg}"}, method = { RequestMethod.GET}, headers = "Accept=application/json; charset=utf-8")
    @ResponseBody
    public ResponseDTO<ResolveCaseConfigVO> listCaseConfig(@PathVariable String configType,
                                                           @PathVariable Optional<String> sysOrg,
                                                           @RequestParam (required = false, defaultValue = "false") Boolean forCase,
                                                           HttpServletRequest request)
    {
        ResponseDTO<ResolveCaseConfigVO> result = new ResponseDTO<>();
        String username = (String) request.getAttribute(Constants.HTTP_REQUEST_USERNAME);
        
        try
        {
        	List<ResolveCaseConfigVO> caseConfigVOs = CaseConfigUtil.listCaseConfig(configType, sysOrg, forCase, username);
            result.setSuccess(true).setRecords(caseConfigVOs).setTotal(caseConfigVOs.size());
        }
        catch(Exception e)
        {
        	String message = String.format(ERROR_RETRIEVING_CASE_CONFIG_OF_TYPE, configType);
            Log.log.error(message, e);
            result.setSuccess(false).setMessage(message);
        } 
        
        return result;
    }

	@RequestMapping(value ="/ioc", method = RequestMethod.GET, headers = "Accept=application/json; charset=utf-8")
	@ResponseBody
	public ResponseDTO<ResolveCaseConfigVO> getCaseConfigByIOCType(@RequestParam String sysOrg, @RequestParam String iocType, HttpServletRequest request) {
        ResponseDTO<ResolveCaseConfigVO> result = new ResponseDTO<>();
        
        try {
			List<ResolveCaseConfigVO> response = CaseConfigUtil.getIOCByConfig(iocType, null);
        	result.setRecords(response).setSuccess(true).setTotal(response.size());
        } catch(Exception ex) {
        	String message = String.format(ERROR_GETTING_IOC, (StringUtils.isNotBlank(iocType) ? iocType : GLOBAL));
			Log.log.error(message, ex);
			result.setSuccess(false).setMessage(message);
        }
        
		return result;
	}

	@RequestMapping(value ="/alliocs", method = RequestMethod.GET, headers = "Accept=application/json; charset=utf-8")
	@ResponseBody
	public ResponseDTO<ResolveCaseConfigVO> getCaseConfigAllIOCs(@RequestParam String sysOrg, HttpServletRequest request) {
        ResponseDTO<ResolveCaseConfigVO> result = new ResponseDTO<>();
        
        try {
			List<ResolveCaseConfigVO> response = CaseConfigUtil.getIOCByConfig(CaseConfigUtil.CASE_CONFIG_IOC_TYPE_ALL_IOCS,
																			   null);
        	result.setRecords(response).setSuccess(true).setTotal(response.size());
        } catch(Exception ex) {
        	String message = String.format(ERROR_GETTING_IOC, ALL);
			Log.log.error(message, ex);
			result.setSuccess(false).setMessage(message);
        }
        
		return result;
	}
	
	/**
	 * Save Case Configuration
	 * 
	 * @param configList : List of ResolveCaseConfig to be saved.
	 * @param configType : Optional String representing configuration type, if specified API will return a filtered list. 
	 * @param sysOrg : Optional String respresenting sys organization id, if present API will return a filtered list.
	 * @param request : {@link HttpServletRequest}
	 * @return
	 *     A List of {@link ResolveCaseConfigVO} filtered by configType or sysOrg if provided, otherwise empty.
	 */
    @RequestMapping(value = {"/save", "/save/{configType}", "/save/{configType}/{sysOrg}"}, method = { RequestMethod.POST }, headers = "Accept=application/json; charset=utf-8")
    @ResponseBody
    public ResponseDTO<ResolveCaseConfigVO> save(@RequestBody List<ResolveCaseConfigVO> configList,
                    @PathVariable Optional<String> configType,
                    @PathVariable Optional<String> sysOrg,
                    HttpServletRequest request)
    {
        ResponseDTO<ResolveCaseConfigVO> result = new ResponseDTO<>();
        String username = (String) request.getAttribute(Constants.HTTP_REQUEST_USERNAME);
        
        try
        {
        	List<ResolveCaseConfigVO> savedCaseConfigVOs = CaseConfigUtil.save(configList, configType, sysOrg, username);
        	
        	if (savedCaseConfigVOs != null && !savedCaseConfigVOs.isEmpty()) {
        		result.setSuccess(true).setRecords(savedCaseConfigVOs).setTotal(savedCaseConfigVOs.size());
        	} else {
        		String message = String.format(ERROR_SAVING_CASE_CONFIG, (configType.isPresent() && StringUtils.isNotBlank(configType.get()) ? 
								   										  configType.get() : CASE_CONFIG_TYPE_UNSPECIFIED), "");
        		Log.log.warn(message);
        		result.setSuccess(false).setMessage(message);
        	}
        }
        catch(Exception e)
        {
            String message;
            
            if (StringUtils.isNotBlank(e.getMessage()) && e.getMessage().startsWith(WARNING_MARKER))
            {
                message = String.format(ERROR_SAVING_CASE_CONFIG, 
                						(configType.isPresent() && StringUtils.isNotBlank(configType.get()) ? 
                						 configType.get() : CASE_CONFIG_TYPE_UNSPECIFIED), 
                						StringUtils.substringAfter(e.getMessage(), WARNING_MARKER));
                Log.log.warn(message);
            }
            else
            {
            	message = String.format(ERROR_SAVING_CASE_CONFIG, 
									    (configType.isPresent() && StringUtils.isNotBlank(configType.get()) ? 
									     configType.get() : CASE_CONFIG_TYPE_UNSPECIFIED), "");
                Log.log.error(message, e);
            }
            
            result.setSuccess(false).setMessage(message);
        }
        
        return result;
    }
}
