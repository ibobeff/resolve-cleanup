/******************************************************************************
* (C) Copyright 2014
*
* Resolve Systems, LLC
*
* All rights reserved
* This software is distributed under license and may not be used, copied,
* modified, or distributed without the express written permission of
* Resolve Systems, LLC.
*
******************************************************************************/

package com.resolve.rsview.main;

import com.resolve.util.XDoc;
import com.resolve.util.ConfigMap;

public class ConfigAuth extends ConfigMap
{
	private static final long serialVersionUID = -4561499950419224716L;
	
	String domain        = "";
	String servicenowSSOUsername	= "";
	String servicenowSSOPassword	= "";
    
    public ConfigAuth(XDoc config)
    {
        super(config);
        
        define("domain", STRING, "./AUTH/@DOMAIN");
        define("servicenowSSOUsername",STRING,"./AUTH/SERVICENOWSSO/@USERNAME");
        define("servicenowSSOPassword",SECURE,"./AUTH/SERVICENOWSSO/@PASSWORD");

    } // ConfigId
    
    public void load() throws Exception
    {
        loadAttributes();
    } // load

    public void save()
    {
        saveAttributes();
    } // save

	public String getDomain() {
		return domain;
	}

	public void setDomain(String domain) {
		this.domain = domain;
	}
	
	public String getServicenowSSOUsername() {
		return servicenowSSOUsername;
	}
	
	public void setServicenowSSOUsername(String servicenowSSOUsername) {
		this.servicenowSSOUsername = servicenowSSOUsername;
	}
	
	public String getServicenowSSOPassword() {
		return servicenowSSOPassword;
	}
	
	public void setServicenowSSOPassword(String servicenowSSOPassword) {
		this.servicenowSSOPassword = servicenowSSOPassword;
	}

} // ConfigId
