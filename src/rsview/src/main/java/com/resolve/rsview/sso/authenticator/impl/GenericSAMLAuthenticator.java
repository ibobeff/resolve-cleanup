package com.resolve.rsview.sso.authenticator.impl;

import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.opensaml.saml2.core.Response;

import com.resolve.rsview.auth.Authentication;
import com.resolve.rsview.sso.authenticator.SSOAuthenticationDTO;
import com.resolve.rsview.sso.authenticator.SSOAuthenticator;
import com.resolve.rsview.sso.saml.SAMLManager;
import com.resolve.rsview.sso.util.SSOUtil;
import com.resolve.services.hibernate.vo.UsersVO;
import com.resolve.services.util.PropertiesUtil;
import com.resolve.services.util.UserUtils;
import com.resolve.util.ERR;
import com.resolve.util.Log;
import com.resolve.util.StringUtils;
import com.resolve.util.SyslogFormatter;

public class GenericSAMLAuthenticator implements SSOAuthenticator {

  private static final String CLAIM_USERNAME_ATTRIBUTE_NAME_KEY = "sso.saml.genericSAML.username.attributeName";
  private static final String CLAIM_GROUPS_ATTRIBUTE_NAME_KEY = "sso.saml.genericSAML.groups.attributeName";

  private static final String DESCRIPTION = "Generic SAML SSO";
  private static final String AUTH_FAILURE_MESSAGE_PREFIX = "Generic SAML SSO authentication failed : ";
  private static final String AUTH_SUCCESS_MESSAGE_PREFIX = "Generic SAML SSO successfully authenticated : ";

  @Override
  public String getSSOType() {
    return "GENERIC_SAML";
  }

  @Override
  public SSOAuthenticationDTO authenticate(HttpServletRequest request, HttpServletResponse response) {
    SSOAuthenticationDTO result = null;

    SAMLManager samlManager = SAMLManager.INSTANCE;
    Response samlResponse;
    try {
      samlResponse = SAMLManager.INSTANCE.consumeSAMLResponse(request);
      Map<String, List<String>> attributeStatementMap = samlManager.getAttributeStatement(samlResponse);
      if (attributeStatementMap != null && attributeStatementMap.size() > 0) {
        String usernameAttributeName = PropertiesUtil.getPropertyString(CLAIM_USERNAME_ATTRIBUTE_NAME_KEY);
        String username = attributeStatementMap.get(usernameAttributeName).get(0);
        String groupsAttributeName = PropertiesUtil.getPropertyString(CLAIM_GROUPS_ATTRIBUTE_NAME_KEY);
        List<String> groupsMembership = attributeStatementMap.get(groupsAttributeName);
        int allExternalGroupsSize = SSOUtil.INSTANCE.getAllExternalGroups().size();
        if (allExternalGroupsSize > 0) {
          boolean externalGroupRequirementsMet = SSOUtil.INSTANCE.externalGroupRequirementsMet(username, groupsMembership);
          if (externalGroupRequirementsMet) {
            UsersVO userVO = UserUtils.getUser(username);
            boolean clonedSuccessfully = false;
            if (userVO == null) {
              try {
                clonedSuccessfully = SSOUtil.INSTANCE.createUserAndAssociateGroup(username, groupsMembership, getSSOType());
              } catch (Exception e) {
                String logMessage = String.format(SSO_AUTH_FAILURE_TO_CREATE_USER_AND_ASSOCIATE_GROUP_MESSAGE, 
										  		  getSSOType(), (StringUtils.isNotBlank(e.getMessage()) ? 
										  				  		 e.getMessage() : ""));
            	Log.auth.warn(logMessage);
            	Log.syslog.warn(SyslogFormatter.INSTANCE.formatAudit(Authentication.getRemoteAddrForSysLog(request), 
								 									 "[user " + username + "]", request.getLocalAddr(), 
								 									 logMessage));
            	
            	if (StringUtils.isNotBlank(e.getMessage()) &&
		            (e.getMessage().equalsIgnoreCase(ERR.E10022.getMessage()) ||
		             e.getMessage().equalsIgnoreCase(ERR.E10023.getMessage()))) {
		            result = new SSOAuthenticationDTO(ERR.getERRFromMessage(e.getMessage()));
		        }
            	
	            return result;
              }
              
	          if (clonedSuccessfully) {
	                String logMessage = "Cloned " + DESCRIPTION + " user " + username + " into Resolve";
	                Log.auth.debug(logMessage);
	                Log.syslog.info(SyslogFormatter.INSTANCE.formatAudit(Authentication.getRemoteAddrForSysLog(request), username, request.getLocalAddr(), logMessage));
	              } else {
	                String logMessage = AUTH_FAILURE_MESSAGE_PREFIX + "Failed to clone user " + username + " into Resolve";
	                Log.auth.warn(logMessage);
	                Log.syslog.warn(SyslogFormatter.INSTANCE.formatAudit(Authentication.getRemoteAddrForSysLog(request), username, request.getLocalAddr(), logMessage));
	          }
            } else {
              boolean isLockedOut = UserUtils.isLockedOut(username);
              if (isLockedOut) {
                String logMessage = AUTH_FAILURE_MESSAGE_PREFIX + "User " + username + " is locked";
                Log.auth.warn(logMessage);
                Log.syslog.warn(SyslogFormatter.INSTANCE.formatAudit(Authentication.getRemoteAddrForSysLog(request), username, request.getLocalAddr(), logMessage));
                return null;
              } else {
                Log.auth.debug("Group matching with Resolve found, proceed with login: " + username);
              }
            }
            result = new SSOAuthenticationDTO(username, groupsMembership);
            String logMessage = AUTH_SUCCESS_MESSAGE_PREFIX + "user " + username + ", referrer: " + request.getHeader("Referer");
            Log.auth.info(logMessage);
            Log.syslog.info(SyslogFormatter.INSTANCE.formatAudit(Authentication.getRemoteAddrForSysLog(request), username, request.getLocalAddr(), logMessage));
          } else {
            String logMessage = AUTH_FAILURE_MESSAGE_PREFIX + "No matching external groups found";
            Log.auth.warn(logMessage);
            Log.syslog.warn(SyslogFormatter.INSTANCE.formatAudit(Authentication.getRemoteAddrForSysLog(request), username, request.getLocalAddr(), logMessage));
          }
        } else {
          String logMessage = AUTH_FAILURE_MESSAGE_PREFIX + "No external groups defined";
          Log.auth.warn(logMessage);
          Log.syslog.warn(SyslogFormatter.INSTANCE.formatAudit(Authentication.getRemoteAddrForSysLog(request), username, request.getLocalAddr(), logMessage));
        }
      } else {
        String logMessage = AUTH_FAILURE_MESSAGE_PREFIX + "Can not authenticate due to empty 'attributeStatementMap' from the SAML assertion";
        Log.auth.warn(logMessage);
        Log.syslog.warn(SyslogFormatter.INSTANCE.formatAudit(Authentication.getRemoteAddrForSysLog(request), "[user unknown]", request.getLocalAddr(), logMessage));
      }
    } catch (Exception e) {
      e.printStackTrace();
    }

    return result;
  }

}
