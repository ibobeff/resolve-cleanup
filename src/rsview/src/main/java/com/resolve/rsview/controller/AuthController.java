/******************************************************************************
* (C) Copyright 2014
*
* Resolve Systems, LLC
*
* All rights reserved
* This software is distributed under license and may not be used, copied,
* modified, or distributed without the express written permission of
* Resolve Systems, LLC.
*
******************************************************************************/

package com.resolve.rsview.controller;

import static com.resolve.services.hibernate.util.UserUtils.decrypt;

import java.io.IOException;
import java.net.URLDecoder;
import java.net.URLEncoder;
import java.nio.charset.StandardCharsets;
import java.security.SecureRandom;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.Map;

import javax.servlet.ServletException;
import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.lang.RandomStringUtils;
import org.apache.commons.lang3.tuple.Pair;
import org.apache.http.HttpHeaders;
import org.owasp.esapi.ESAPI;
import org.owasp.esapi.errors.AccessControlException;
import org.owasp.esapi.reference.DefaultHTTPUtilities;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.View;

import com.resolve.notification.NotificationAPI;
import com.resolve.rsview.auth.Authentication;
import com.resolve.rsview.auth.Authentication.AuthenticateResult;
import com.resolve.rsview.auth.IFramePreLoginSession;
import com.resolve.rsview.main.ConfigActiveDirectory;
import com.resolve.rsview.main.ConfigLDAP;
import com.resolve.rsview.main.Main;
import com.resolve.rsview.model.Login;
import com.resolve.rsview.model.PasswordRecovery;
import com.resolve.services.ServiceAuthentication;
import com.resolve.services.constants.ConstantValues;
import com.resolve.services.hibernate.vo.UsersVO;
import com.resolve.services.util.AuditUtils;
import com.resolve.services.util.PropertiesUtil;
import com.resolve.services.util.UserUtils;
import com.resolve.services.vo.ResponseDTO;
import com.resolve.util.Constants;
import com.resolve.util.CryptUtils;
import com.resolve.util.ERR;
import com.resolve.util.Log;
import com.resolve.util.StringUtils;
import com.resolve.util.SyslogFormatter;

@Controller
public class AuthController extends GenericController
{
    private final static String COOKIE_PREFIX = "RSPREF_";
    private final static String PROPERTY_PREFIX = "user.preference.";
    private final static String SSOTYPE_IS_SERVICENOW_URL_PARAM = "SSOTYPE=servicenow";
    private final static String SERVICENOW_SSOTYPE_TOKEN_VALUE = "servicenow";
    private final static String CARROT_DELIMITER_REGEX = "\\^";

    @RequestMapping(value = "/login", method = RequestMethod.GET)
    public ModelAndView loginGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException
    {
        // init params
        String url = request.getParameter("url");
        
        Log.auth.trace("login GET : url = [" + (StringUtils.isNotBlank(url) ? url : "null") + "]" );
        
        String ssoServiceId = request.getParameter(Constants.HTTP_REQUEST_SSOSERVICEID);
        
        Log.auth.trace("login GET : SSO Service Id = [" + (StringUtils.isNotBlank(ssoServiceId) ? ssoServiceId : "null") + "]" );
        
        Map<String, String[]> siteMinderHeaders = null;
        
        if (StringUtils.isNotBlank(ssoServiceId) && Authentication.requestHasSiteMinderHeaders(request))
        {
            siteMinderHeaders = Authentication.getSiteMinderHeaders(request);
        }
        
        String iFrameId = request.getParameter(Constants.HTTP_REQUEST_IFRAMEID);
        
        if (StringUtils.isBlank(iFrameId))
        {
            iFrameId = request.getParameter(Constants.HTTP_REQUEST_IFRAMEID.toLowerCase());
        }
        
        if (StringUtils.isBlank(iFrameId))
        {
        	iFrameId = retrieveIframeIdfromUrl(url,Constants.HTTP_REQUEST_IFRAMEID);
        }
        
        if (StringUtils.isBlank(iFrameId))
        {
        	iFrameId = retrieveIframeIdfromUrl(url, Constants.HTTP_REQUEST_IFRAMEID.toLowerCase());
        }
                
        if (StringUtils.isBlank(iFrameId))
        {
            iFrameId = request.getParameter(Constants.HTTP_REQUEST_SSO_TYPE);
        }
        if(StringUtils.isBlank(iFrameId))
        {
        	iFrameId = retrieveIframeIdfromUrl(url, Constants.HTTP_REQUEST_SSO_TYPE);
        }
        Log.auth.trace("login GET : iFrameId = [" + iFrameId + "]" );
        
        if (StringUtils.isNotBlank(iFrameId))
        {
            if (StringUtils.isNotBlank(request.getHeader(HttpHeaders.REFERER)) && 
                !Authentication.getIFramePreLoginSessionCache().containsKey(request.getSession().getId()))
            {               
                Authentication.cacheIFramePreLoginSession(new IFramePreLoginSession(request.getSession().getId(), 
                                                          iFrameId.toLowerCase(), 
                                                          Authentication.getProtocolAndAuthority(request.getHeader(HttpHeaders.REFERER))));
            }
            else if (Log.auth.isTraceEnabled())
            {
                Log.auth.trace("login GET : iFrameId = [" + iFrameId + "] Referer in request = [" + request.getHeader(HttpHeaders.REFERER) + "]");
                IFramePreLoginSession ifrmPrlgSess = Authentication.getIFramePreLoginSessionCache().get(request.getSession().getId());
                
                if (ifrmPrlgSess != null)
                {
                    Log.auth.trace("login GET : iFrameId = [" + iFrameId + "] iFrame Pre Login Cached Session = " + ifrmPrlgSess);
                }
            }
        }
               
        if (StringUtils.isBlank(url) || 
            (url.contains(Constants.HTTP_REQUEST_IFRAMEID.toLowerCase()) ||
             url.contains(Constants.HTTP_REQUEST_IFRAMEID) ||
             url.contains(Constants.HTTP_REQUEST_SSO_TYPE)))
        {
            if (StringUtils.isNotBlank(url) && StringUtils.isNotBlank(iFrameId))
            {
            	if (url.contains(Constants.HTTP_REQUEST_IFRAMEID.toLowerCase()))
            	{
            		url = replaceIframeIdFromQueryString(url,iFrameId,Constants.HTTP_REQUEST_IFRAMEID.toLowerCase());
            	}
            	else if(url.contains(Constants.HTTP_REQUEST_IFRAMEID))
            	{
            		url = replaceIframeIdFromQueryString(url,iFrameId,Constants.HTTP_REQUEST_IFRAMEID);
            	}
            	else 
            	{
            		url = replaceIframeIdFromQueryString(url,iFrameId,Constants.HTTP_REQUEST_SSO_TYPE);
            	}
            }
            else
            {
                url = "/jsp/rsclient.jsp";
            }
        }

        Log.auth.trace("login GET : url = [" + url + "]" );
        
        //check if the recovery flag is set to true
        boolean allowRecovery = PropertiesUtil.getPropertyBoolean("users.password.allowRecovery");
        
        Login login = new Login(url);
        login.setAllowRecovery(allowRecovery);
        
        if (StringUtils.isNotBlank(iFrameId))
        {
            login.setIframeId(iFrameId);
            Log.auth.trace("Login object passed to create login prompt: iframeId=" + login.getIframeId());
        }
        
        if (StringUtils.isNotBlank(ssoServiceId) && siteMinderHeaders != null && !siteMinderHeaders.isEmpty())
        {
            login.setSSOServiceId(ssoServiceId);
            login.setSSOServiceData(siteMinderHeaders);
            login.setSSOSrvcLoginBtnLbl(PropertiesUtil.getPropertyString(Authentication.SITEMINDER_PROPERTY_PREFIX + 
                                                                         Authentication.SITEMINDER_RELOGIN_BUTTON_LABEL_PROPERTY_SUFFIX));            
        }
        
        String errorMsg = request.getParameter(Constants.HTTP_REQUEST_SSOSERVICEERRORMSG);
        
        if (StringUtils.isNotBlank(errorMsg))
        {
            login.setError(errorMsg);
        }
        
        return new ModelAndView("login", "login", login);
    }
    
    private static String replaceIframeIdFromQueryString(String url, String iframeid, String keyname)
    {
    	String result = url;
    	String queryString = StringUtils.substringAfter(url,"?");
    	if (StringUtils.isNotBlank(url) && url.contains(keyname + "="))
    	{
    		queryString = queryString.replaceAll(keyname+"="+iframeid+"&","");
    		if(queryString.contains(keyname+"="))
    		{
    			queryString = queryString.replaceAll("&*"+keyname+"="+iframeid, "");
    		}
    	}
    	if(StringUtils.isNotBlank(queryString))
    	{
    		queryString = "&"+queryString;
    	}
    	result = "/jsp/rsclient.jsp?"+keyname.toLowerCase()+"="+iframeid+queryString;
    	return result;
    }
    private static String retrieveIframeIdfromUrl(String url, String keyname)
    {
    	String iFrameId = "";
        if (StringUtils.isNotBlank(url) && url.contains(keyname + "="))
        {
            iFrameId = StringUtils.substringBetween(url, keyname + "=", "&");
            
            if (StringUtils.isBlank(iFrameId))
            {
                iFrameId = StringUtils.substringAfter(url, keyname + "=");
            }
        }
        return iFrameId;
    }
    /**
     * Recovers forgotten password provided username, email and answer matching the user profile
     * @param username - the username
     * @param email - the user's email
     * @param question - the user's one of the questions in the profile
     * @param answer - the the question's answer in the profile
     * @param done - set if the call is the last steps in the series of calls on the endpoint
     * @param url - redirect url
     * @param iframeId - the ifranme id
     * @param request
     * @param response
     * @return
     * @throws ServletException
     * @throws IOException
     */
    /**
     * Recovers forgotten password provided username, email and answer matching the user profile
     * @param passwordRecovery - DTO object with the following properties as String:
     *   username - the username
     *   email - the user's email
     *   question - the user's one of the questions in the profile
     *   answer - the the question's answer in the profile
     *   done - set if the call is the last steps in the series of calls on the endpoint
     *   url - redirect url
     *   iframeId - the ifranme id
     * @param request
     * @param response
     * @return
     * @throws ServletException
     * @throws IOException
     */
    @RequestMapping(value = "/passwordRecovery2", method = RequestMethod.POST)
    @ResponseBody
    public ResponseDTO<PasswordRecovery> passwordRecoveryPost2(
                    @RequestBody PasswordRecovery passwordRecovery,
                    HttpServletRequest request,
                    HttpServletResponse response
                    ) throws ServletException, IOException
    {
        ResponseDTO<PasswordRecovery> result = new ResponseDTO<PasswordRecovery>();
        String url = passwordRecovery.getUrl();
        String username = passwordRecovery.getUserName();
        String question = passwordRecovery.getQuestion();
        String email = passwordRecovery.getEmail();
        String done = passwordRecovery.getDone();
        String iframeId = passwordRecovery.getIframeId();
        String answer = passwordRecovery.getAnswer();
        if ("done".equalsIgnoreCase(passwordRecovery.getDone()))
        {
            StringBuilder sb = new StringBuilder();
            if (StringUtils.isNotEmpty(url))
            {
                sb.append("?url=" + url);
            }
            
            if (StringUtils.isNotEmpty(iframeId))
                
            {
                String prefix = StringUtils.isNotEmpty(sb.toString()) ? "&" : "?";
                
                sb.append(prefix + "iframeid=" + iframeId);
            }

            PasswordRecovery recovery = new PasswordRecovery();
            recovery.setUrl("/resolve/sir/index.html#/login" + (StringUtils.isNotEmpty(sb.toString()) ? sb.toString() : ""));
            recovery.setSubmit("login");
            result
            .setSuccess(true)
            .setData(recovery);
        } else {
            ModelAndView pwRecovery = passwordRecoveryPost(username, email, question, answer, done, url, iframeId, request, response);
            result
            .setSuccess(true)
            .setData((PasswordRecovery)pwRecovery.getModel().get("passwordRecovery"));
        }

        return result;
    }


    @RequestMapping(value = "/passwordRecovery", method = RequestMethod.GET)
    public ModelAndView passwordRecovery(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException
    {
        PasswordRecovery recovery = new PasswordRecovery();
        
        String url = request.getParameter("url");
        
        if (StringUtils.isNotBlank(url))
        {
            recovery.setUrl(url);
        }
        
        String iframeid = request.getParameter("iframeid");
        
        if (StringUtils.isNotBlank(iframeid))
        {
            recovery.setIframeId(iframeid);
        }
        
        return new ModelAndView("passwordRecovery", "passwordRecovery", recovery);
    }

    @RequestMapping(value = "/passwordRecovery", method = RequestMethod.POST)
    public ModelAndView passwordRecoveryPost(@RequestParam String username, @RequestParam String email, @RequestParam String question, @RequestParam String answer, @RequestParam String done, @RequestParam String url, @RequestParam String iframeId, HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException
    {
        if ("done".equalsIgnoreCase(done))
        {
            StringBuilder sb = new StringBuilder();
            
            if (StringUtils.isNotEmpty(url))
            {
                sb.append("?url=" + url);
            }
            
            if (StringUtils.isNotEmpty(iframeId))
            {
                String prefix = StringUtils.isNotEmpty(sb.toString()) ? "&" : "?";
                
                sb.append(prefix + "iframeid=" + iframeId);
            }
            
            //return redirectUrl("/service/login" + (StringUtils.isNotEmpty(url) ? "?url=" + url : ""));
            return redirectUrl("/service/login" + (StringUtils.isNotEmpty(sb.toString()) ? sb.toString() : ""));
        }
        PasswordRecovery recovery = new PasswordRecovery();
        
        if (StringUtils.isNotEmpty(url))
        {
            recovery.setUrl(url);
        }
        
        if (StringUtils.isNotEmpty(iframeId))
        {
            recovery.setIframeId(iframeId);
        }
        
        // RBA-14197 : Cross Site Scripting (Reflected)
        recovery.setUserName(ESAPI.encoder().encodeForHTMLAttribute(username));
        recovery.setEmail(ESAPI.encoder().encodeForHTMLAttribute(email));

        if (StringUtils.isNotEmpty(username))
        {
            UsersVO user = UserUtils.getUser(username);
            if (user != null && StringUtils.isNotBlank(email) && (StringUtils.isNotBlank(user.getUEmail()) && user.getUEmail().equalsIgnoreCase(email)))
            {
                if (StringUtils.isNotBlank(question) && StringUtils.isNotBlank(answer))
                {
                    
                    if ((StringUtils.isNotBlank(user.getUQuestion1()) && (decrypt(user.getUQuestion1()).equals(question) && decrypt(user.getUAnswer1()).equalsIgnoreCase(answer))) ||
                        (StringUtils.isNotBlank(user.getUQuestion2()) && (decrypt(user.getUQuestion2()).equals(question) && decrypt(user.getUAnswer2()).equalsIgnoreCase(answer))) || 
                        (StringUtils.isNotBlank(user.getUQuestion3()) && (decrypt(user.getUQuestion3()).equals(question) && decrypt(user.getUAnswer3()).equalsIgnoreCase(answer))))
                    {
                        
                        // Generate new password
                        String newPassword = generateRandomPassword();
                        
                        //String newPassword = RandomStringUtils.randomAlphanumeric(24);
                        user.usetPassword(newPassword);
                        user.setUPasswordNeedsReset(true);
                        UserUtils.saveUser(user, username);

                        recovery.setSubmit("Done");
                        recovery.setDone(true);
                        recovery.setMessage("Your password has been reset.  An email has been sent with credentials that will allow you to login.");

                        // Send email with new credentials
                        try
                        {
                            String subject = PropertiesUtil.getPropertyString("users.password.recoverySubject");
                            String content = PropertiesUtil.getPropertyString("users.password.recoveryContent");
                            StringBuilder body = new StringBuilder();
                            body.append("<html>");
                            body.append("<body style=\"font-family:Verdana\">");
//                            body.append("<p>");
                            body.append(content.replace("${password}", newPassword));
//                            body.append("</p>");
                            body.append("</body>");
                            body.append("</html>");
                           
                            NotificationAPI.sendToUser(user.getUUserName(), subject, body.toString(), "");
                           
                        }
                        catch (Exception e)
                        {
                            recovery.setMessage("An error occurred while sending your recovery email.  Please contact your administrator to reset your password.");
                        }
                    }
                    else
                    {
                        recovery.setMessage("Unable to verify your credentials. Please contact your administrator to reset your password.");
                    }
                }
                else
                {
                    if(!NotificationAPI.isEmailActive())
                    {
                        recovery.setSubmit("Done");
                        recovery.setDone(true);
                        recovery.setMessage("Email password reset is not configured. Please contact your administrator to reset your password.");
                    }
                    else 
                    {
                        //RBA-12137:If resetting a password, credentials cannot be verified unless ALL user security questions are filled out 
                        
                        int totRecoveryQuestions=0;
                        ArrayList<String> recoverQuestioList = new ArrayList<String>();
                        if(StringUtils.isNotEmpty(user.getUQuestion1()) && StringUtils.isNotEmpty(user.getUAnswer1())) {
                            recoverQuestioList.add(totRecoveryQuestions, decrypt(user.getUQuestion1()));
                            totRecoveryQuestions++;
                        }
                        if(StringUtils.isNotEmpty(user.getUQuestion2()) && StringUtils.isNotEmpty(user.getUAnswer2())) {
                            recoverQuestioList.add(totRecoveryQuestions, decrypt(user.getUQuestion2()));
                            totRecoveryQuestions++;
                        }
                        if(StringUtils.isNotEmpty(user.getUQuestion3())  && StringUtils.isNotEmpty(user.getUAnswer3())) {
                            recoverQuestioList.add(totRecoveryQuestions, decrypt(user.getUQuestion3()));
                            totRecoveryQuestions++;
                        }
                        
                        
                        // Pick which question to display to the user
                        if(totRecoveryQuestions > 0) 
                        {
                             SecureRandom random = new SecureRandom();
                             int questionNumber = 1 + (int) (random.nextFloat() * (( totRecoveryQuestions - 1) + 1));
                             
                             
                             if((questionNumber-1)<=recoverQuestioList.size()) 
                             {
                                 recovery.setQuestion(recoverQuestioList.get(questionNumber-1));     
                             }
                            
                             
    
                              if (StringUtils.isNotBlank(recovery.getQuestion()))
                              {
                                  recovery.setSubmit("Submit");
                                  recovery.setDisplayAnswer(true); 
                              } 
                        }
                        else
                        {
                                 recovery.setSubmit("Done");
                                 recovery.setDone(true);
                                 recovery.setMessage("Unable to verify your credentials. Please contact your administrator to reset your password.");
                        }
                    }
                }                  
            }
            else
            {
                recovery.setMessage("Unable to verify your credentials. Please contact your administrator to reset your password.");
            }
        }

        return new ModelAndView("passwordRecovery", "passwordRecovery", recovery);
    }

    @RequestMapping(value = "/login", method = RequestMethod.POST)
    public ModelAndView loginPost(@RequestParam(value = "username", required = false) String username, 
                                  @RequestParam(value = "password", required = false) String password, 
                                  @RequestParam final String url, @RequestParam String hashState, 
                                  @RequestParam String iframeId, 
                                  @RequestParam(value = "sSOServiceId", required = false) String sSOServiceId,
                                  @RequestParam(value = "sSOSrvcDataKeys", required = false) String sSOSrvcDataKeys,
                                  @RequestParam(value = "sSOSrvcDataValues", required = false) String sSOSrvcDataValues,
                                  HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException
    {
        ModelAndView result = null; 
        
        Log.auth.trace("login POST : username = " + (StringUtils.isNotBlank(username) ? username : "") + ", url = [" + (StringUtils.isNotBlank(url) ? url : "null") + 
                       "], hash State = [" + (StringUtils.isNotBlank(hashState) ? hashState : "null") + "], iframeId = [" +
                       (StringUtils.isNotBlank(iframeId) ? iframeId : "null") + "], sSOServiceId = [" +
                       (StringUtils.isNotBlank(sSOServiceId) ? sSOServiceId : "null") + "], sSOSrvcDataKeys = [" +
                       (StringUtils.isNotBlank(sSOSrvcDataKeys) ? sSOSrvcDataKeys : "null") + "], sSOSrvcDataValues = [" +
                       (StringUtils.isNotBlank(sSOSrvcDataValues) ? sSOSrvcDataValues : "null") + "]");
        
        // logging
        String host = request.getRemoteAddr();
        Log.auth.info("login POST host: " + host + " username: " + (StringUtils.isNotBlank(username) ? username : "") + " SSO Service Id: " +
                      (StringUtils.isNotBlank(sSOServiceId) ? sSOServiceId : "") + " SSO Service Data Keys: " + 
                      (StringUtils.isNotBlank(sSOSrvcDataKeys) ? sSOSrvcDataKeys : "") + " SSO Service Data Values: " +
                      (StringUtils.isNotBlank(sSOSrvcDataValues) ? sSOSrvcDataValues : ""));

        try
        {
            int maxSize = 1000;
            if (StringUtils.isBlank(sSOServiceId) && ((StringUtils.isNotEmpty(username)&&username.length()>maxSize) || 
                                                      (StringUtils.isNotEmpty(password)&&password.length()>maxSize) || 
                                                      (StringUtils.isNotEmpty(hashState) && hashState.length()>maxSize)))
            {
                String error = "Incorrect username or password";
                Log.auth.info("Overflow: Invalid login credentials. url: " + url);
                
                String syslogMessage = Authentication.AUTHENTICATION_SYSLOG_RSVIEW_LOGIN_FAILED_MESAGE_PREFIX + Authentication.AUTHENTICATION_SYSLOG_RSVIEW_LOGIN_MESSAGE_SUFFIX;
                Log.syslog.warn(SyslogFormatter.INSTANCE.formatAudit(Authentication.getRemoteAddrForSysLog(request), username, request.getLocalAddr(), syslogMessage));
                
                // set login result
                Login login = new Login(url);
                login.setError(error);
				
				if (StringUtils.isNotBlank(iframeId))
            	{
                	login.setIframeId(iframeId);
            	}
				
                result = new ModelAndView("login", "login", login);
                return result;
            }
            
            Map<String, String[]> siteMinderHeaders = new HashMap<String, String[]>();
            String[] groups = new String[0];
            
            if (StringUtils.isBlank(sSOServiceId))
            {
                // lowercase the username
                username = username.toLowerCase();

                // if username is using format "<domain>\<userid>", change it to "<userid>@<domain>"
                int pos = username.indexOf('\\');
                if (pos > 0)
                {
                    String userId = username.substring(pos + 1, username.length());
                    String domain = username.substring(0, pos);
                    if (domain.indexOf('.') > 0) 
                    {
                        username = userId + "@" + domain;
                    }
                }
            }
            else if (StringUtils.isNotBlank(sSOServiceId) && sSOServiceId.equals(Authentication.SITEMINDER_AUTH_TYPE))
            {
                Map<String, String> siteMinderPropVals = Authentication.getSiteMinderProperties();
                
                String[] keysArray = sSOSrvcDataKeys.split(CARROT_DELIMITER_REGEX);
                String[] valuesArray = sSOSrvcDataValues.split(CARROT_DELIMITER_REGEX);
                
                if (keysArray.length == valuesArray.length)
                {
                    for (int i = 0; i < keysArray.length; i++)
                    {
                        String[] keyValuesArray = valuesArray[i].split(Authentication.MULTIPLE_PARAM_VALUES_DELIMITER_REGEX);
                        siteMinderHeaders.put(keysArray[i], keyValuesArray);
                    }
                }
                
                Log.auth.trace("requests " + Authentication.SITEMINDER_AUTH_TYPE + " Parameters: " + 
                               StringUtils.mapToString(siteMinderHeaders, "=", ","));
                
                for (String siteMinderHeaderName : siteMinderHeaders.keySet())
                {
                    if (siteMinderPropVals.containsKey(Authentication.SITEMINDER_PROPERTY_PREFIX + Authentication.SITEMINDER_USERID_PROPERTY_SUFFIX) && 
                        siteMinderHeaderName.endsWith(siteMinderPropVals.get(Authentication.SITEMINDER_PROPERTY_PREFIX + Authentication.SITEMINDER_USERID_PROPERTY_SUFFIX)))
                    {
                        username = siteMinderHeaders.get(siteMinderHeaderName)[0];
                    }
                    else if (siteMinderPropVals.containsKey(Authentication.SITEMINDER_PROPERTY_PREFIX + Authentication.SITEMINDER_GROUP_PROPERTY_SUFFIX) && 
                             siteMinderHeaderName.endsWith(siteMinderPropVals.get(Authentication.SITEMINDER_PROPERTY_PREFIX + Authentication.SITEMINDER_GROUP_PROPERTY_SUFFIX)))
                    {
                        groups = siteMinderHeaders.get(siteMinderHeaderName);
                    }
                }
            }

            String urlRedirect = url + hashState;

            Log.auth.trace("login POST : redirect URL = [" + (StringUtils.isNotBlank(urlRedirect) ? urlRedirect : "null") + "]");
                            
            Authentication.regenerateSession(request);
            
            AuthenticateResult authResult = null;
            
            Pair<String, String> SiteMinderResult = null;
            
            if (StringUtils.isBlank(sSOServiceId))
            {
                // validate login
                authResult = Authentication.authenticate(username, password);
            }
            else if (StringUtils.isNotBlank(sSOServiceId) && sSOServiceId.equals(Authentication.SITEMINDER_AUTH_TYPE))
            {
                SiteMinderResult = Authentication.authenticateSiteMinder(username, groups);
                
                boolean smAuthResult = false;
                username = SiteMinderResult.getLeft();
                
                if (SiteMinderResult != null && StringUtils.isBlank(SiteMinderResult.getRight()))
                {
                    smAuthResult = true;                    
                }

                authResult = new AuthenticateResult(smAuthResult, Authentication.SITEMINDER_AUTH_TYPE,
                									ERR.getERRFromMessage(SiteMinderResult.getRight()));
            }
            
            if(authResult.valid)
            {
                String sessionId = request.getSession().getId();
                Log.auth.info("Authenticated - username: " + username + " sessionid: " + sessionId + " url: " + url);
                AuditUtils.log("AuthController", "LOGIN - username: " + username + " sessionid: " + sessionId + " url: " + url + " host: " + host);

                if (StringUtils.isNotBlank(sSOServiceId) && sSOServiceId.equals(Authentication.SITEMINDER_AUTH_TYPE))
                {
                    username = username.toLowerCase();
                }
                
                // update user login details
                boolean needNewPassword = ServiceAuthentication.updateAndCheckLogin(username, host);

                long SESSION_TIMEOUT = (PropertiesUtil.getPropertyLong(ConstantValues.PROPERTIES_SESSION_TIMEOUT) < 720) ?  
                                       720000 : PropertiesUtil.getPropertyLong(ConstantValues.PROPERTIES_SESSION_TIMEOUT) * 1000;
                                
                // add token
                                
                String ssoType = null;
                
                /*
                 * If ServiceNow SSO type is disabled, this could be authentic login from ServiceNow
                 * through login prompt, in which case set the sso type in the token. 
                 */
                
                if (urlRedirect.contains(SSOTYPE_IS_SERVICENOW_URL_PARAM))
                {
                    Log.auth.trace("Detected successful login from login prompt in ServiceNow, setting token sso type to " + SERVICENOW_SSOTYPE_TOKEN_VALUE);
                    ssoType = SERVICENOW_SSOTYPE_TOKEN_VALUE;
                }
                
                String tokenId = null;
                
                if (StringUtils.isBlank(sSOServiceId))
                {                
                    tokenId = Authentication.setAuthToken(request, response, username, host, ssoType, SESSION_TIMEOUT, password, iframeId);
                }
                else if (StringUtils.isNotBlank(sSOServiceId) && sSOServiceId.equals(Authentication.SITEMINDER_AUTH_TYPE))
                {
                    // set token
                
                    Map<String, String[]> mockSSOSrvcData = null;
                
                    if (PropertiesUtil.getPropertyBoolean(Authentication.SITEMINDER_PROPERTY_PREFIX + Authentication.SITEMINDER_MOCK_PROPERTY_SUFFIX))
                    {
                        mockSSOSrvcData = siteMinderHeaders;
                    }
                
                    tokenId = Authentication.setSSOServiceAuthToken(request, response, username, request.getRemoteAddr(), null, SESSION_TIMEOUT, password, iframeId, Authentication.SITEMINDER_AUTH_TYPE, mockSSOSrvcData);
                }
                
                Log.auth.info("  tokenid: " + tokenId + " sessionid: " + sessionId);

                // check if password needs to be resetted
                if (StringUtils.isBlank(sSOServiceId) && needNewPassword)
                {
                    if (urlRedirect.contains(SSOTYPE_IS_SERVICENOW_URL_PARAM))
                    {
                        Log.auth.trace("Detected need for password change from login prompt in ServiceNow");
                    }
                    
                    Login login = new Login(urlRedirect);
                    
                    if (StringUtils.isNotBlank(iframeId))
                    {
                        login.setIframeId(iframeId);
                    }
                    
                    result = new ModelAndView("newpassword", "login", login);
                }
                else
                {
                    // Add _token_ to parameters if url to redirect is not null or blank
                    urlRedirect = url;
                    if (StringUtils.isNotBlank(urlRedirect) && urlRedirect.contains(Constants.HTTP_REQUEST_INTERNAL_USERNAME) &&
                        urlRedirect.contains(Constants.HTTP_REQUEST_INTERNALP_ASSWORD))
                    {
                        int startIndxOfIntUserName = urlRedirect.indexOf(Constants.HTTP_REQUEST_INTERNAL_USERNAME);
                        
                        if (startIndxOfIntUserName > 0)
                        {
                            int endIndxOfIntUserName = urlRedirect.indexOf("&", startIndxOfIntUserName);
                            
                            String newUrlRedirect = urlRedirect.substring(0, startIndxOfIntUserName);
                            
                            if (endIndxOfIntUserName > 0 && (endIndxOfIntUserName + 1) < urlRedirect.length())
                            {
                                newUrlRedirect = newUrlRedirect + urlRedirect.substring(endIndxOfIntUserName + 1);
                            }
                            
                            urlRedirect = newUrlRedirect;
                        }
                    }
                    
                    urlRedirect = StringUtils.isNotBlank(urlRedirect) /*&& urlRedirect.contains(Constants.HTTP_REQUEST_INTERNALP_ASSWORD)*/ ? 
                                  urlRedirect + (urlRedirect.contains("?") ? "&" : "?") + Constants.HTTP_REQUEST_INTERNAL_TOKEN + "=" + CryptUtils.encryptToken(tokenId) : urlRedirect;
                                  
                    result = redirectUrl(urlRedirect+hashState);
                    
                    Log.auth.trace("login POST : final redirect URL = [" + (StringUtils.isNotBlank(urlRedirect) ? urlRedirect : "null") + "]");
                    
                    String syslogMessage = Authentication.AUTHENTICATION_SYSLOG_RSVIEW_LOGIN_SUCCESS_MESAGE_PREFIX + authResult.authType + Authentication.AUTHENTICATION_SYSLOG_RSVIEW_LOGIN_MESSAGE_SUFFIX;
                    Log.syslog.info(SyslogFormatter.INSTANCE.formatAudit(Authentication.getRemoteAddrForSysLog(request), username, request.getLocalAddr(), syslogMessage));
                }
            }
            else
            {
                String error = StringUtils.isNotBlank(sSOServiceId) && sSOServiceId.equals(Authentication.SITEMINDER_AUTH_TYPE) && SiteMinderResult != null ? 
                               SiteMinderResult.getRight() : 
                               (authResult.errCode != null ? 
                                authResult.errCode.getMessage() : "Incorrect username or password.");
                
                if (StringUtils.isBlank(error) && authResult.errCode != null && 
                	(authResult.errCode == ERR.E10022 || authResult.errCode == ERR.E10023)) {
                	error = authResult.errCode.getMessage();
                }
                
                boolean isLockedUser = authResult.errCode == null && UserUtils.isLockedOut(username);
                if (isLockedUser)
                {
                    error = "User account is locked. Please contact system administrator.";
                }

                Log.auth.info("Error " + error + " in logging. urlRedirect : [" + urlRedirect + "]");
                
                String syslogMessage = Authentication.AUTHENTICATION_SYSLOG_RSVIEW_LOGIN_FAILED_MESAGE_PREFIX + 
                					   authResult.authType + 
                					   Authentication.AUTHENTICATION_SYSLOG_RSVIEW_LOGIN_MESSAGE_SUFFIX;
                Log.syslog.warn(SyslogFormatter.INSTANCE.formatAudit(Authentication.getRemoteAddrForSysLog(request), 
                													 username, request.getLocalAddr(), syslogMessage));
                
                // set login result
                Login login = new Login(url);
                login.setHashState(hashState);
                login.setError(URLEncoder.encode(error, StandardCharsets.UTF_8.name()));
                login.setAllowRecovery(authResult.errCode == null && checkAllowRecovery(username));
                
                if (urlRedirect.contains(SSOTYPE_IS_SERVICENOW_URL_PARAM))
                {
                    Log.auth.trace("Detected failed login from login prompt in ServiceNow");
                }
                
                if (StringUtils.isNotBlank(iframeId))
                {
                    login.setIframeId(iframeId);
                    Authentication.cacheIFramePreLoginSession(new IFramePreLoginSession(request.getSession().getId(), 
                                                              iframeId,
                                                              Authentication.getProtocolAndAuthority(request.getHeader(HttpHeaders.REFERER))));
                }
                
                if (StringUtils.isNotBlank(sSOServiceId) && siteMinderHeaders != null && !siteMinderHeaders.isEmpty())
                {
                    login.setSSOServiceId(sSOServiceId);
                    login.setSSOServiceData(siteMinderHeaders);
                    login.setSSOSrvcLoginBtnLbl(PropertiesUtil.getPropertyString(Authentication.SITEMINDER_PROPERTY_PREFIX + 
                                                                                 Authentication.SITEMINDER_RELOGIN_BUTTON_LABEL_PROPERTY_SUFFIX));
                    
                    String errorMsg = error;
                    
                    if (StringUtils.isNotBlank(errorMsg))
                    {
                        login.setError(errorMsg);
                    }
                }
                
                result = new ModelAndView("login", "login", login);
            }
        }
        catch (Exception e)
        {
            Log.log.warn(e.getMessage(), e);
        }
        Log.auth.info("session active: " + Authentication.getSessionActive() + " max: " + Authentication.getSessionMaxActive());

        return result;
    }

    @RequestMapping(value = "/logout", method = RequestMethod.GET)
    public String logout(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException
    {
        String sessionId = request.getSession().getId();
        Log.auth.info("LOGOUT - sessionid: " + sessionId);

        String ssoType = request.getParameter(Constants.HTTP_REQUEST_SSO_TYPE);
        String ssoReferer = null;

        if (StringUtils.isBlank(ssoType))
        {
            String ssoInfo = request.getParameter("USource");
            
            if (StringUtils.isNotBlank(ssoInfo))
            {
                
                Map<String, String> ssoInfoMap = StringUtils.jsonObjectToMap((StringUtils.stringToJSONObject(ssoInfo)));
                
                ssoType = ssoInfoMap.get(Constants.HTTP_REQUEST_SSO_TYPE);
                
                Log.auth.trace("LOGOUT - ssoType from USource: " + ssoType);
            }
        }
        
        IFramePreLoginSession iFrmPrlgSess = Authentication.getIFramePreLoginSessionCache().get(request.getSession().getId());
        
        if (iFrmPrlgSess != null)
        {
            ssoType = iFrmPrlgSess.getIFrameId();
            ssoReferer = iFrmPrlgSess.getRefererHost();
            
            Log.auth.trace("LOGOUT - ssoType from iframe pre-login cache: " + ssoType + ", Referer: " + ssoReferer);
        }
        
        // save preference token
        String username = UserUtils.getUserid(Authentication.getAuthToken(request, ssoType));
        if (!StringUtils.isEmpty(username))
        {
            String propName = getPropertyName(username);
            String prefCookieName = getCookieName(username);
            
            Collection<Cookie> prefCookies = Authentication.getCookies(request, prefCookieName, null);
            
            if (prefCookies != null && !prefCookies.isEmpty())
            {
                Cookie prefCookie = prefCookies.iterator().next(); // There should be only one preference cookie
                if (prefCookie != null)
                {
                    String value = java.net.URLDecoder.decode(prefCookie.getValue(), "UTF-8");
                    PropertiesUtil.setPropertyString(propName, value);
                }
            }
        }
        AuditUtils.log("AuthController", "LOGOUT - username: " + username + " sessionid: " + sessionId);

        // remove token
        Authentication.invalidateForSession(request, response, ssoType);

        String syslogMessage = Authentication.AUTHENTICATION_SYSLOG_RSVIEW_LOGIN_SUCCESS_MESAGE_PREFIX + Authentication.AUTHENTICATION_SYSLOG_RSVIEW_LOGOUT_MESSAGE_SUFFIX;
        Log.syslog.info(SyslogFormatter.INSTANCE.formatAudit(Authentication.getRemoteAddrForSysLog(request), username, request.getLocalAddr(), syslogMessage));
        
        Log.auth.info("session active: " + Authentication.getSessionActive() + " max: " + Authentication.getSessionMaxActive());
        String redirect = "redirect:/service/login";
        
        if(request.getParameterMap().containsKey("displayClient") && 
           request.getParameter("displayClient").equals("false")) {
            redirect += "?url="+ URLEncoder.encode("/jsp/rsclient.jsp?displayClient=false", 
            									   StandardCharsets.UTF_8.name());
        }
        
        Log.auth.trace("LOGOUT - redirect: " + redirect);
        
        if (/*!redirect.contains("?url=") &&*/ StringUtils.isNotBlank(ssoType))
        {
            String redirectPrefix = redirect.contains("?") ? "&" : "?";
            
            redirect += redirectPrefix + Constants.HTTP_REQUEST_IFRAMEID + "=" + ssoType;
            
            if (StringUtils.isNotBlank(ssoReferer))
            {
//                request.setAttribute(HttpHeaders.REFERER, ssoReferer);
                redirect += "&" + HttpHeaders.REFERER + "=" + ssoReferer;
            }
        }
        
        Log.auth.trace("LOGOUT - redirect: " + redirect + ", ssoType: " + ssoType);
        
        if (Authentication.requestHasSiteMinderHeaders(request))
        {
            String redirectPrefix = redirect.contains("?") ? "&" : "?";
            
            redirect += redirectPrefix + Constants.HTTP_REQUEST_SSOSERVICEID + "=" + Authentication.SITEMINDER_AUTH_TYPE;
            
            Map<String, String[]> siteMinderHeaders = Authentication.getSiteMinderHeaders(request);
            
            if (siteMinderHeaders != null && !siteMinderHeaders.isEmpty())
            {
                StringBuilder redirectStrngBldr = new StringBuilder(redirect);
                
                for (String siteMinderHeaderName : siteMinderHeaders.keySet())
                {
                    String[] siteMinderHeaderValues = siteMinderHeaders.get(siteMinderHeaderName);
                    
                    for (int i = 0; i < siteMinderHeaderValues.length; i++)
                    {
                        redirectStrngBldr.append("&").append(siteMinderHeaderName).append("=").append(siteMinderHeaderValues[i]);
                    }
                }
                
                redirect = redirectStrngBldr.toString();
            }
        }
        
        Log.auth.trace("LOGOUT returning redirect: " + redirect);
        
        return redirect;
    }

    @RequestMapping(value = "/newpassword", method = RequestMethod.GET)
    public ModelAndView loginNewpassword(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException
    {
        // init params
        String url = request.getParameter("url");
        if (StringUtils.isEmpty(url))
        {
            url = "/jsp/newpassword.jsp";
        }

        return new ModelAndView("newpassword", "newpassword", new Login(url));
    }

    @RequestMapping(value = "/newpassword", method = RequestMethod.POST)
    public ModelAndView newpasswordPost(@RequestParam String password, @RequestParam String confirm, @RequestParam String url, @RequestParam String iframeId, HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException
    {
        ModelAndView result = null;

        // logging
        String host = request.getRemoteAddr();
        Log.auth.info("newpassword POST - host: " + host);

        String ssoType = null;
        
        if (StringUtils.isNotBlank(url) && url.contains(SSOTYPE_IS_SERVICENOW_URL_PARAM))
        {
            Log.auth.trace("Detected new password post from new password prompt in ServiceNow, setting token sso type to " + SERVICENOW_SSOTYPE_TOKEN_VALUE);
            ssoType = SERVICENOW_SSOTYPE_TOKEN_VALUE;
        }
        
        try
        {
            if (StringUtils.isEmpty(password)) {
                throw new IllegalArgumentException("New password cannot be empty");
            }
            String username = UserUtils.getUserid(Authentication.getAuthToken(request, ssoType));
            
            if (!StringUtils.isEmpty(username))
            {
                // update user login details
                if (!password.equals(confirm))
                {
                    String error = "New password confirmation does not match";
                    Log.auth.info(error);
                    Log.log.info(error);
                    // result = "redirect:/jsp/newpassword.jsp?url="+url;

                    Login login = new Login(url);
                    login.setError(error);
                    
                    if (StringUtils.isNotBlank(iframeId))
                    {
                        login.setIframeId(iframeId);
                    }
                    
                    result = new ModelAndView("newpassword", "login", login);
                }
                else if (password.charAt(0) == '_')
                {
                    UserUtils.updatePassword(username, password.substring(1));
                    result = redirectUrl(url);

                    // result = "redirect:"+url;
                }
                else if (password.length() < 8 || !password.matches(".*[a-z]+.*") || !password.matches(".*[A-Z]+.*") || !password.matches(".*[0-9]+.*"))
                {
                    String error = "User password must have: 8 or more letters, an uppercase and a lowercase letter, and a number";
                    Log.auth.info(error);
                    Log.log.info(error);
                    // result = "redirect:/jsp/newpassword.jsp?url="+url;

                    Login login = new Login(url);
                    login.setError(error);
                    
                    if (StringUtils.isNotBlank(iframeId))
                    {
                        login.setIframeId(iframeId);
                    }
                    
                    result = new ModelAndView("newpassword", "login", login);
                }
                else
                {
                    UserUtils.updatePassword(username, password);
                    result = redirectUrl(url);
                }
            }
            else
            {
                String error = "Invalid login credentials.";
                Log.auth.info(error);

                Login login = new Login(url);
                login.setError(error);
                
                if (StringUtils.isNotBlank(iframeId))
                {
                    login.setIframeId(iframeId);
                }
                
                result = new ModelAndView("login", "login", login);
            }
        }
        catch (Exception e)
        {
            String error = e.getMessage();
            Log.log.warn(e.getMessage(), e);

            // remove token
            Authentication.invalidate(request, response, (StringUtils.isNotBlank(ssoType) ? ssoType : iframeId));

            Login login = new Login(url);
            login.setError(error);
            
            if (StringUtils.isNotBlank(iframeId))
            {
                login.setIframeId(iframeId);
            }
            
            result = new ModelAndView("login", "login", login);
        }

        return result;
    }

    @RequestMapping(value = "/remove", method = RequestMethod.POST)
    public String remove(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException
    {

        String[] id = request.getParameterValues("id");

        if (id != null)
        {
            for (int i = 0; i < id.length; i++)
            {
                String token = id[i];
                if (StringUtils.isNotBlank(token))
                {
                    Authentication.removeAuthToken(token);
                }
            }
        }

        return "redirect:/service/wiki/view/SystemAdmin/UserInfo?control=false";
    }

    ModelAndView redirectUrl(final String url)
    {
        ModelAndView result;

        View view = new View()
        {
            @Override
            public String getContentType()
            {
                return "text/html";
            }

            @Override
            public void render(Map<String, ?> arg0, HttpServletRequest arg1, HttpServletResponse arg2) throws Exception
            {
                if (url == null)
                {
                    Log.auth.trace("redirectUrl : redirecting to [/resolve]...");
                    arg2.sendRedirect("/resolve");
                }
                else
                {
                    DefaultHTTPUtilities dhttp = new DefaultHTTPUtilities();
                    
                    try {
                        Log.auth.trace("redirectUrl : redirecting to [/resolve" + StringUtils.removeNewLineAndCarriageReturn(URLDecoder.decode(url, "UTF-8")));
                        dhttp.sendRedirect(arg2, "/resolve" + StringUtils.removeNewLineAndCarriageReturn(URLDecoder.decode(url, "UTF-8")));   
                    } catch(AccessControlException  ace) {
                        Log.auth.error("Error " + ace.getMessage() + " while redirecting to URL [/resolve" + 
                                       StringUtils.removeNewLineAndCarriageReturn(URLDecoder.decode(url, "UTF-8")), ace);
                    }
                }
            }
        };

        result = new ModelAndView(view);
        return result;
    }

    public static String getCookieName(String userName)
    {
        return COOKIE_PREFIX + userName.toUpperCase();
    }

    public static String getPropertyName(String userName)
    {
        return PROPERTY_PREFIX + userName.toLowerCase();
    }

    static boolean checkAllowRecovery(String username)
    {
        boolean result = false;

        // check system properties settings
        boolean allowRecovery = StringUtils.getBoolean(PropertiesUtil.getPropertyString("users.password.allowRecovery"));
        if (allowRecovery && NotificationAPI.isEmailActive())
        {
            // check remote LDAP/AD domain does not exist
            if (!ServiceAuthentication.isDomainExist(Authentication.getDomain(username)))
            {
                // check blueprint LDAP/AD is not active
                Main main = (Main) Main.main;
                ConfigLDAP configLdap = main.getConfigLDAP();
                ConfigActiveDirectory configAD = main.getConfigActiveDirectory();
                if (!configLdap.isActive() && !configAD.isActive())
                {
                    result = true;
                }
            }
        }

        return result;
    }
    
    private static String generateRandomPassword() {
//        SecureRandom random = new SecureRandom();
        String password;
        do {
            password = RandomStringUtils.random(32, 33, 126, true, true, null, new SecureRandom());
        } while (!password.matches(com.resolve.services.hibernate.util.UserUtils.PAS_SWORD_RULES));
        
        return password;
    }
}
