/******************************************************************************
* (C) Copyright 2014
*
* Resolve Systems, LLC
*
* All rights reserved
* This software is distributed under license and may not be used, copied,
* modified, or distributed without the express written permission of
* Resolve Systems, LLC.
*
******************************************************************************/

package com.resolve.rsview.impex;

import java.io.File;

import com.resolve.execute.ExecuteOS;
import com.resolve.execute.ExecuteResult;
import com.resolve.rsview.main.RSContext;
import com.resolve.services.exception.WikiException;
import com.resolve.services.hibernate.util.ImportUtils;
import com.resolve.util.FileUtils;
import com.resolve.util.Log;
import com.resolve.util.StringUtils;

public class ImpexAPI
{
	/**
	 * API for importing a module
	 * 
	 * Related Tables:
	 * - resolve_impex_module
	 * - resolve_impex_wiki
	 * - resolve_impex_glide
	 * 
	 * ExportUtil.java - for Yu's API 
	 * 
	 * @param moduleName
	 * @throws WikiException
	 */
	public static void importModule(String moduleName) throws WikiException, Exception
	{
        importModule(moduleName, false);
	    
	} // importModule
	
	public static void importModule(String moduleName, boolean blockIndex) throws WikiException, Exception
	{
		try
		{
			// WebApi webApi = (WebApi) RSContext.appContext.getBean(ConstantValues.WIKI_SPRINGID_FOR_WEBAPI);
			// webApi.importModule(moduleName, blockIndex);
		    try
	        {
	            new ImportUtils().importModule(moduleName, null, "admin", blockIndex);
	        }
	        catch (Exception e)
	        {
	            Log.log.error("Error while importing a module: " + moduleName, e);
	        }
			//ServiceHibernate.importModule(moduleName, null, "resolve.maint", blockIndex);
		}
		catch(Throwable e)
		{
			Log.log.error("Error in wiki import api:", e);
			throw new WikiException(new Exception(e));
		}

	}//end of importModule
    
	public static void importModule(String moduleName, String username) throws WikiException, Exception
	{
        importModule(moduleName, username, false);
	} // importModule
    
	public static void importModule(String moduleName, String username, boolean blockIndex) throws WikiException, Exception
	{
		try
		{
			// WebApi webApi = (WebApi) RSContext.appContext.getBean(ConstantValues.WIKI_SPRINGID_FOR_WEBAPI);
			// webApi.importModule(moduleName, username, blockIndex);
		    try
	        {
	            new ImportUtils().importModule(moduleName, null, username, blockIndex);
	        }
	        catch (Exception e)
	        {
	            Log.log.error("Error while importing a module: " + moduleName, e);
	        }
		    // ServiceHibernate.importModule(moduleName, null, username, blockIndex);
		}
		catch(Throwable e)
		{
			Log.log.error("Error in wiki import api:", e);
			throw new WikiException(new Exception(e));
		}

	}//end of importModule
	
	public static void exportModule(String moduleName) throws Exception
	{
//		WebApi webApi = (WebApi) RSContext.appContext.getBean(ConstantValues.WIKI_SPRINGID_FOR_WEBAPI);
//		webApi.exportModule(moduleName);
		// ServiceHibernate.export(moduleName, "admin");

	}//end of importModule
	
	
	public static void importReport(String reportName)
	{
	    final int TIMEOUT = 180;
	    
	    if (StringUtils.isNotEmpty(reportName))
	    {
	        // remove lock file - /opt/resolve/tomcat/webapps/sree/WEB-INF/classes/schedule.xml 
		    File lockfile = FileUtils.getFile(RSContext.getResolveHome()+"/tomcat/webapps/sree/WEB-INF/classes/schedule.xml");
		    if (lockfile.exists())
		    {
		        lockfile.delete();
		    }
		    
	        String osname = System.getProperty("os.name").toLowerCase();
	        if (osname.equals("linux") || osname.equals("sunos") || osname.equals("aix"))
	        {
	            String filename;
	            if (reportName.endsWith(".jar"))
	            {
	                filename = reportName;
	            }
	            else
	            {
		            filename = RSContext.getResolveHome()+"/rsexpert/"+reportName+".jar"; 
	            }
	            
	            // execute dashboard.sh IMPORTASSET -DREPORT=$DIST/rsexpert/reportname.jar
	            String command = "/bin/bash "+RSContext.getResolveHome()+"/bin/dashboard.sh IMPORTASSET -DREPORT="+filename;
	            ExecuteOS os = new ExecuteOS(RSContext.getResolveHome()+"/bin");
	            ExecuteResult result = os.execute(command, null, true, TIMEOUT, "IMPORTASSETS: "+reportName);
	            String out = result.getResultString();
	            if (out != null)
	            {
	                Log.log.info(out);
	            }
	        }
	        
	        // windows
	        else
	        {
	            String filename;
	            if (reportName.endsWith(".jar"))
	            {
	                filename = reportName.replace('/', '\\');
	            }
	            else
	            {
		            filename = RSContext.getResolveHome()+"\\rsexpert\\"+reportName+".jar"; 
	            }
	            
	            // execute dashboard.bat IMPORTASSET -DREPORT=$DIST\rsexpert\reportname.jar
	            String command = "cmd.exe /c \\\"\\\""+RSContext.getResolveHome()+"bin\\dashboard.bat\\\" IMPORTASSET \\\"-DREPORT="+filename + "\\\"\\\"";
	            ExecuteOS os = new ExecuteOS(RSContext.getResolveHome()+"\\bin");
	            ExecuteResult result = os.execute(command, null, true, TIMEOUT, "IMPORTASSETS: "+reportName);
	            String out = result.getResultString();
	            if (out != null)
	            {
	                Log.log.info(out);
	            }
	        }
	    }
	    
	} // importReport
	
	public static void exportReport(String reportName)
	{
	    
	} // exportReport
	
    /*
	public static void main(String[] args) throws Exception
	{
		Log.log = Logger.getRootLogger();
		String logConfigFilename = "C:/project/resolve3/dist/tomcat/webapps/resolve/WEB-INF/log.cfg";
        Log.init(logConfigFilename, "ImpexAPI");
        Log.initAuth();

        CustomTableMappingUtil.setCustomMappingFileLocation("C:/project/resolve3/dist/tomcat/webapps/resolve/WEB-INF");

        java.util.Properties props = new java.util.Properties();
		props.setProperty(HibernateUtil.DB_TYPE, "mysql");
		props.setProperty(HibernateUtil.DB_HOST, "localhost");
		props.setProperty(HibernateUtil.DB_NAME, "resolve");
		props.setProperty(HibernateUtil.DB_USERNAME, "resolve");
		props.setProperty(HibernateUtil.DB_DROWSSAP_PROP_NAME, "resolve");
		props.setProperty(HibernateUtil.DB_POOL_SIZE, "8");
		props.setProperty(HibernateUtil.DB_TIMEOUT, "300");

		HibernateUtil.initTransactionManager(props);
		HibernateUtil.init("C:/project/resolve3/dist/tomcat/webapps/resolve/WEB-INF/hibernate.cfg.xml");
//		
		System.out.println("Connection successfull....");
		
//		exportModule("test");
		importModule("test");//testing using the tomcat server
		
		System.exit(0);
	}
    */
	
	
} // ImpexAPI
