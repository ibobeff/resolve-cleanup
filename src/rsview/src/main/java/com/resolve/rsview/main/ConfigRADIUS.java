package com.resolve.rsview.main;

import com.resolve.util.ConfigMap;
import com.resolve.util.Log;
import com.resolve.util.StringUtils;
import com.resolve.util.XDoc;

public class ConfigRADIUS extends ConfigMap
{
    boolean active                  = false;
    /*String domainName               ="";
    String resolveDomain            ="";*/
    String primaryRadiusServerIpAddress    ="";
    String secondaryRadiusServerIpAddress    ="";
    String authProtocol             ="PAP";
    String authPort                 ="1812";
    String accntPort                ="1813";
    String sharedSecret             ="";
    boolean fallback                = true;
    boolean grouprequired           = false;
    
    
    public ConfigRADIUS(XDoc config)
    {
        super(config);
        define("active", BOOLEAN, "./AUTH/RADIUS/@ACTIVE");
        define("primaryRadiusServerIpAddress", STRING, "./AUTH/RADIUS/@PRIMARYRADIUSSERVERIPADDRESS");
        define("secondaryRadiusServerIpAddress", STRING, "./AUTH/RADIUS/@SECONDARYRADIUSERVERIPADDRESS");
        define("authProtocol", STRING, "./AUTH/RADIUS/@AUTH_PROTOCOL");
        define("authPort", STRING, "./AUTH/RADIUS/@AUTH_PORT");
        define("accntPort", STRING, "./AUTH/RADIUS/@ACCOUNTING_PORT");
        define("sharedSecret", ENCRYPT, "./AUTH/RADIUS/@SHARED_SECRET");
        define("fallback", BOOLEAN, "./AUTH/RADIUS/@FALLBACK");
        define("grouprequired", BOOLEAN, "./AUTH/RADIUS/@GROUPREQUIRED");

    }
    @Override
    @SuppressWarnings("rawtypes")
    public void load() throws Exception
    {
        loadAttributes();
        // TODO Auto-generated method stub

    }

    public boolean isActive()
    {
        return active;
    }
    public void setActive(boolean active)
    {
        this.active = active;
    }
    public String getPrimaryRadiusServerIpAddress()
    {
        return primaryRadiusServerIpAddress;
    }
    public void setPrimaryRadiusServerIpAddress(String primaryRadiusServerIpAddress)
    {
        this.primaryRadiusServerIpAddress = primaryRadiusServerIpAddress;
    }
    
    
    public String getSecondaryRadiusServerIpAddress()
    {
        return secondaryRadiusServerIpAddress;
    }
    public void setSecondaryRadiusServerIpAddress(String secondaryRadiusServerIpAddress)
    {
        this.secondaryRadiusServerIpAddress = secondaryRadiusServerIpAddress;
    }
    public String getAuthProtocol()
    {
        return authProtocol;
    }
    public void setAuthProtocol(String authProtocol)
    {
        this.authProtocol = authProtocol;
    }
    public String getAuthPort()
    {
        return authPort;
    }
    public void setAuthPort(String authPort)
    {
        this.authPort = authPort;
    }
    public String getAccntPort()
    {
        return accntPort;
    }
    public void setAccntPort(String accntPort)
    {
        this.accntPort = accntPort;
    }
    public String getSharedSecret()
    {
        return sharedSecret;
    }
    public void setSharedSecret(String sharedSecret)
    {
        this.sharedSecret = sharedSecret;
    }
    public boolean isFallback()
    {
        return fallback;
    }
    public void setFallback(boolean fallback)
    {
        this.fallback = fallback;
    }
    public boolean isGrouprequired()
    {
        return grouprequired;
    }
    public void setGrouprequired(boolean grouprequired)
    {
        this.grouprequired = grouprequired;
    }
//    @Override
    @SuppressWarnings({ "rawtypes", "unchecked" })
    public void save() throws Exception
    {
        // TODO Auto-generated method stub
        saveAttributes();

    }

}
