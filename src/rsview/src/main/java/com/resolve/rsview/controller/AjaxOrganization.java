/******************************************************************************
* (C) Copyright 2014
*
* Resolve Systems, LLC
*
* All rights reserved
* This software is distributed under license and may not be used, copied,
* modified, or distributed without the express written permission of
* Resolve Systems, LLC.
*
******************************************************************************/

package com.resolve.rsview.controller;

import java.io.IOException;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.codehaus.jackson.map.ObjectMapper;
import org.springframework.stereotype.Controller;
import org.springframework.stereotype.Service;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.resolve.services.ServiceHibernate;
import com.resolve.services.hibernate.vo.OrganizationVO;
import com.resolve.services.vo.QueryDTO;
import com.resolve.services.vo.ResponseDTO;
import com.resolve.util.Constants;
import com.resolve.util.Log;

import net.sf.json.JSONObject;

@Controller
@Service
public class AjaxOrganization extends GenericController
{
	private static final String ERROR_DISABLING_ORGANIZATION = "Error disabling Organization";
	private static final String ERROR_ENABLING_ORGANIZATION = "Error enabling Organization";
	private static final String ERROR_RETRIEVING_ORGANIZATION = "Error retrieving Organization";
    private static final String ERROR_SAVING_ORGANIZATION = "Error saving Organization";

	/**
     * Returns list of Organization for the grid based on filters, sorts, pagination
     * 
     * @param query
     * @param request
     * @param response
     * @return
     * @throws ServletException
     * @throws IOException
     */
    @RequestMapping(value = "/organization/list", method = { RequestMethod.GET, RequestMethod.POST }, headers = "Accept=application/json; charset=utf-8")
    @ResponseBody
    public ResponseDTO list(@ModelAttribute QueryDTO query, HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException
    {
        ResponseDTO result = new ResponseDTO();
        try
        {
            query.setModelName("Organization");
//            query.setSelectColumns("sys_id,UName,UDescription,sysUpdatedOn");
            
            List<OrganizationVO> data = ServiceHibernate.getOrganization(query);
            int total = ServiceHibernate.getTotalHqlCount(query);
            
            result.setSuccess(true).setRecords(data);
            result.setTotal(total);
        }
        catch (Exception e)
        {
            Log.log.error(ERROR_RETRIEVING_ORGANIZATION, e);
            result.setSuccess(false).setMessage(ERROR_RETRIEVING_ORGANIZATION);
        }
        
        return result;
    }
    
    /**
     * Gets data for a specific Organization to be edited based on sysId
     * 
     * @param id
     * @param request
     * @param response
     * @return
     * @throws ServletException
     * @throws IOException
     */
    @RequestMapping(value = "/organization/get", method = { RequestMethod.POST }, headers = "Accept=application/json; charset=utf-8")
    @ResponseBody
    public ResponseDTO get(@RequestParam String id, HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException
    {
        ResponseDTO result = new ResponseDTO();

        try
        {
            OrganizationVO vo = ServiceHibernate.findOrganizationById(id);
            result.setData(vo);
            result.setSuccess(true);
            
        }
        catch (Exception e)
        {
            Log.log.error(ERROR_RETRIEVING_ORGANIZATION, e);
            result.setSuccess(false).setMessage(ERROR_RETRIEVING_ORGANIZATION);
        }

        return result;
    }
    
    /**
     * Disable the Organization based on array of sysIds
     * 
     * @param ids
     * @param request
     * @param response
     * @return
     * @throws ServletException
     * @throws IOException
     */
    @RequestMapping(value = "/organization/disable", method = { RequestMethod.POST }, headers = "Accept=application/json; charset=utf-8")
    @ResponseBody
    public ResponseDTO disable(@RequestParam String[] ids, @ModelAttribute QueryDTO query, HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException
    {
        ResponseDTO result = new ResponseDTO();
        String username = (String) request.getAttribute(Constants.HTTP_REQUEST_USERNAME);
        
        try
        {
            ServiceHibernate.disableEnableOrganizationByIds(ids, true, username);
            result.setSuccess(true);
        }
        catch (Exception e)
        {
            Log.log.error(ERROR_DISABLING_ORGANIZATION, e);
            result.setSuccess(false).setMessage(ERROR_DISABLING_ORGANIZATION);
        }

        return result;
    }
    
    /**
     * Enable the Organization based on array of sysIds
     * 
     * @param ids
     * @param request
     * @param response
     * @return
     * @throws ServletException
     * @throws IOException
     */
    @RequestMapping(value = "/organization/enable", method = { RequestMethod.POST }, headers = "Accept=application/json; charset=utf-8")
    @ResponseBody
    public ResponseDTO enable(@RequestParam String[] ids, @ModelAttribute QueryDTO query, HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException
    {
        ResponseDTO result = new ResponseDTO();
        String username = (String) request.getAttribute(Constants.HTTP_REQUEST_USERNAME);
        
        try
        {
            ServiceHibernate.disableEnableOrganizationByIds(ids, false, username);
            result.setSuccess(true);
        }
        catch (Exception e)
        {
            Log.log.error(ERROR_ENABLING_ORGANIZATION, e);
            result.setSuccess(false).setMessage(ERROR_ENABLING_ORGANIZATION);
        }

        return result;
    }
    
    /**
     * Saves/Update an Organization
     * 
     * @param property
     * @param request
     * @param response
     * @return
     * @throws ServletException
     * @throws IOException
     */
    @RequestMapping(value = "/organization/save", method = { RequestMethod.POST }, headers = "Accept=application/json; charset=utf-8")
    @ResponseBody
    public ResponseDTO save(@RequestBody JSONObject jsonProperty, HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException
    {
      //using jackson to deserialize the json
        String json = jsonProperty.toString();
        OrganizationVO entity = new ObjectMapper().readValue(json, OrganizationVO.class);
        
        //convert json to VO - spring does not work as the VO are not 100% java bean
//        JsonConfig jsonConfig = new JsonConfig();
//        jsonConfig.setRootClass(OrganizationVO.class);
//        OrganizationVO entity = (OrganizationVO) JSONSerializer.toJava(jsonProperty, jsonConfig);
        
        ResponseDTO result = new ResponseDTO();
        String username = (String) request.getAttribute(Constants.HTTP_REQUEST_USERNAME);
        
        try
        {
            OrganizationVO vo = ServiceHibernate.saveOrganization(entity, username);
            result.setSuccess(true).setData(vo).setMessage("Organization Created successfully.");
        }
        catch (Exception e)
        {
            Log.log.error(ERROR_SAVING_ORGANIZATION, e);
            result.setSuccess(false).setMessage(ERROR_SAVING_ORGANIZATION);
        }

        return result;
    }
}
