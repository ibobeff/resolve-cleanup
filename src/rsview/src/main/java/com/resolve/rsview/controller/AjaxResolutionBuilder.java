/******************************************************************************
* (C) Copyright 2014
*
* Resolve Systems, LLC
*
* All rights reserved
* This software is distributed under license and may not be used, copied,
* modified, or distributed without the express written permission of
* Resolve Systems, LLC.
*
******************************************************************************/

package com.resolve.rsview.controller;

import java.io.IOException;
import java.net.URLDecoder;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.lang3.StringUtils;
import org.codehaus.jackson.map.DeserializationConfig;
import org.codehaus.jackson.map.ObjectMapper;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.resolve.services.ServiceHibernate;
import com.resolve.services.ServiceResolutionBuilder;
import com.resolve.services.hibernate.vo.RBConditionVO;
import com.resolve.services.hibernate.vo.RBConnectionVO;
import com.resolve.services.hibernate.vo.RBGeneralVO;
import com.resolve.services.hibernate.vo.RBIfVO;
import com.resolve.services.hibernate.vo.RBLoopVO;
import com.resolve.services.hibernate.vo.RBTaskVO;
import com.resolve.services.hibernate.wiki.SaveHelper;
import com.resolve.services.rb.util.ABProcessConst;
import com.resolve.services.vo.QueryDTO;
import com.resolve.services.vo.ResponseDTO;
import com.resolve.util.Constants;
import com.resolve.util.Log;

import net.sf.json.JSONObject;

@Controller
public class AjaxResolutionBuilder extends GenericController
{
	private static final String ERROR_DELETING_CONDITION_INFO = "Error deleting resolution builder condition information";
	private static final String ERROR_DELETING_CONNECTION_INFO = "Error deleting resolution builder connection information";
	private static final String ERROR_DELETING_GENERAL_INFO = "Error deleting resolution builder general information";
	private static final String ERROR_DELETING_IF_INFO = "Error deleting resolution builder IF information";
	private static final String ERROR_DELETING_LOOP_INFO = "Error deleting resolution builder loop information";
	private static final String ERROR_DELETING_TASK_INFO = "Error deleting resolution builder task information";
	private static final String ERROR_FETCHING_GENERATION_PROGRESS = "Error fetching generation progress";
	private static final String ERROR_GENERATING_RUNBOOK = "Error generating runbook from resolution builder";
	private static final String ERROR_GETTING_CONDITION_INFO = "Error getting condition information";
	private static final String ERROR_GETTING_CONNECTION_INFO = "Error getting connection information";
	private static final String ERROR_GETTING_GENERAL_INFO = "Error getting resolution builder general information";
	private static final String ERROR_GETTING_IF_INFO = "Error getting IF information";
	private static final String ERROR_GETTING_LOOP_INFO = "Error getting loop information";
	private static final String ERROR_GETTING_SUBRUNBOOK_INFO = "Error getting subrunbook reference information";
	private static final String ERROR_GETTING_TASK_INFO = "Error getting task information";
	private static final String ERROR_RETRIEVING_RESOLUTION_BUILDER_INFO = "Error retrieving resolution builder general information";
	private static final String ERROR_SAVING_CONDITION_INFO = "Error saving condition information";
	private static final String ERROR_SAVING_IF_INFO = "Error saving IF information";
	private static final String ERROR_SAVING_LOOP_INFO = "Error saving loop information";
	private static final String ERROR_SAVING_RESOLUTION_BUILDER_INFO = "Error saving the resolution builder general information";
	private static final String ERROR_SAVING_SUBRUNBOOK_INFO = "Error saving subrunbook reference information";
	private static final String ERROR_SAVING_TASK_INFO = "Error saving task information";
    private static final String ERROR_DELETING_SUBRUNBOOK_INFO = "Error deleting subrunbook refernce task information";

	@RequestMapping(value = "/resolutionbuilder/listGeneral", method = { RequestMethod.POST, RequestMethod.GET })
    @ResponseBody
    public ResponseDTO<RBGeneralVO> listGeneral(@ModelAttribute QueryDTO query, HttpServletRequest request) throws ServletException, IOException
    {
        ResponseDTO<RBGeneralVO> result = new ResponseDTO<RBGeneralVO>();
        String username = (String) request.getAttribute(Constants.HTTP_REQUEST_USERNAME);

        try
        {
            List<RBGeneralVO> data = ServiceResolutionBuilder.listGeneral(query, username);
            int total = ServiceHibernate.getTotalHqlCount(query);

            result.setSuccess(true).setRecords(data);
            result.setTotal(total);
        }
        catch (Exception e)
        {
            Log.log.error(ERROR_RETRIEVING_RESOLUTION_BUILDER_INFO, e);
            result.setSuccess(false).setMessage(ERROR_RETRIEVING_RESOLUTION_BUILDER_INFO);
        }

        return result;
    }

    @RequestMapping(value = "/resolutionbuilder/getGeneral", method = { RequestMethod.POST, RequestMethod.GET }, headers = "Accept=application/json; charset=utf-8")
    @ResponseBody
    public ResponseDTO<RBGeneralVO> getGeneral(@RequestParam String id, HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException
    {
        ResponseDTO<RBGeneralVO> result = new ResponseDTO<RBGeneralVO>();
        String username = (String) request.getAttribute(Constants.HTTP_REQUEST_USERNAME);
        
        try
        {
            RBGeneralVO vo = ServiceResolutionBuilder.getGeneral(id, username);
            result.setData(vo).setSuccess(true);
        }
        catch (Exception e)
        {
            Log.log.error(ERROR_RETRIEVING_RESOLUTION_BUILDER_INFO, e);
            result.setSuccess(false).setMessage(ERROR_RETRIEVING_RESOLUTION_BUILDER_INFO);
        }

        return result;
    }
    
    @RequestMapping(value = "/resolutionbuilder/saveGeneral", method = { RequestMethod.POST, RequestMethod.GET }, headers = "Accept=application/json; charset=utf-8")
    @ResponseBody
    public ResponseDTO<RBGeneralVO> saveGeneral(@RequestBody JSONObject jsonProperty, HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException
    {
        //using jackson to deserialize the json
        String json = jsonProperty.toString();
        RBGeneralVO entity = new ObjectMapper().readValue(json, RBGeneralVO.class);

        ResponseDTO<RBGeneralVO> result = new ResponseDTO<RBGeneralVO>();
        String username = (String) request.getAttribute(Constants.HTTP_REQUEST_USERNAME);

        try
        {
            //UI may not set "sys_id" which is actually "id" for UI
            entity.setSys_id(entity.getId());
            RBGeneralVO vo = ServiceResolutionBuilder.saveGeneral(entity, username);
            result.setSuccess(true).setData(vo);
        }
        catch (Exception e)
        {
        	if (StringUtils.isNotBlank(e.getMessage())) {
        		if (!e.getMessage().startsWith(SaveHelper.FAILED_TO_SAVE_OBJECT_PREFIX) &&
        			!(e.getMessage().endsWith(SaveHelper.FAILED_TO_SAVE_OBJECT_SUFFIX) ||
        			  e.getMessage().endsWith(SaveHelper.FAILED_TO_SAVE_OBJECT_HA_VIOLATED_SUFFIX))) {
		            String[] exceptionMsgElements = StringUtils.split(e.getMessage(), " ");
		            
		            if (exceptionMsgElements != null && exceptionMsgElements.length >= 1)
		            {
		                RBGeneralVO evo = new RBGeneralVO();
		                evo.setLockedByUser(exceptionMsgElements[exceptionMsgElements.length - 1]);
		                result.setData(evo);
		            }
        		}
        	}
            
            result.setSuccess(false).setMessage(checkAndSetErrorMessage(e, ERROR_SAVING_RESOLUTION_BUILDER_INFO));
        }

        return result;
    }
    
    @RequestMapping(value = "/resolutionbuilder/deleteGeneral", method = { RequestMethod.POST, RequestMethod.GET }, headers = "Accept=application/json; charset=utf-8")
    @ResponseBody
    public ResponseDTO<String> deleteGeneral(@RequestParam String id, HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException
    {
        ResponseDTO<String> result = new ResponseDTO<String>();
        String username = (String) request.getAttribute(Constants.HTTP_REQUEST_USERNAME);
        
        try
        {
            boolean success = ServiceResolutionBuilder.deleteGeneral(id, username);
            result.setSuccess(success);
        }
        catch (Exception e)
        {
            Log.log.error(ERROR_DELETING_GENERAL_INFO, e);
            result.setSuccess(false).setMessage(ERROR_DELETING_GENERAL_INFO);
        }

        return result;
    }

    @RequestMapping(value = "/resolutionbuilder/getLoop", method = { RequestMethod.POST, RequestMethod.GET }, headers = "Accept=application/json; charset=utf-8")
    @ResponseBody
    public ResponseDTO<RBLoopVO> getLoop(@RequestParam String id, HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException
    {
        ResponseDTO<RBLoopVO> result = new ResponseDTO<RBLoopVO>();
        String username = (String) request.getAttribute(Constants.HTTP_REQUEST_USERNAME);
        
        try
        {
            RBLoopVO vo = ServiceResolutionBuilder.getLoop(id, username);
            result.setData(vo).setSuccess(true);
        }
        catch (Exception e)
        {
            Log.log.error(ERROR_GETTING_LOOP_INFO, e);
            result.setSuccess(false).setMessage(ERROR_GETTING_LOOP_INFO);
        }

        return result;
    }

    @RequestMapping(value = "/resolutionbuilder/saveLoop", method = { RequestMethod.POST, RequestMethod.GET }, headers = "Accept=application/json; charset=utf-8")
    @ResponseBody
    public ResponseDTO<RBLoopVO> saveLoop(@RequestBody JSONObject jsonProperty, HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException
    {
        //using jackson to deserialize the json
        String json = jsonProperty.toString();
        RBLoopVO entity = new ObjectMapper().readValue(json, RBLoopVO.class);

        ResponseDTO<RBLoopVO> result = new ResponseDTO<RBLoopVO>();
        String username = (String) request.getAttribute(Constants.HTTP_REQUEST_USERNAME);

        try
        {
            //UI may not set "sys_id" which is actually "id" for UI
            entity.setSys_id(entity.getId());
            RBLoopVO vo = ServiceResolutionBuilder.saveLoop(entity, username);
            result.setSuccess(true).setData(vo);
        }
        catch (Exception e)
        {
            result.setSuccess(false).setMessage(checkAndSetErrorMessage(e, ERROR_SAVING_LOOP_INFO));
        }

        return result;
    }

    @RequestMapping(value = "/resolutionbuilder/deleteLoop", method = { RequestMethod.POST, RequestMethod.GET }, headers = "Accept=application/json; charset=utf-8")
    @ResponseBody
    public ResponseDTO<String> deleteLoop(@RequestParam String id, HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException
    {
        ResponseDTO<String> result = new ResponseDTO<String>();
        String username = (String) request.getAttribute(Constants.HTTP_REQUEST_USERNAME);
        
        try
        {
            boolean success = ServiceResolutionBuilder.deleteLoop(id, username);
            result.setSuccess(success);
        }
        catch (Exception e)
        {
            Log.log.error(ERROR_DELETING_LOOP_INFO, e);
            result.setSuccess(false).setMessage(ERROR_DELETING_LOOP_INFO);
        }

        return result;
    }

    @RequestMapping(value = "/resolutionbuilder/getConnection", method = { RequestMethod.POST, RequestMethod.GET }, headers = "Accept=application/json; charset=utf-8")
    @ResponseBody
    public ResponseDTO<RBConnectionVO> getConnection(@RequestParam String id, HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException
    {
        ResponseDTO<RBConnectionVO> result = new ResponseDTO<RBConnectionVO>();
        String username = (String) request.getAttribute(Constants.HTTP_REQUEST_USERNAME);
        
        try
        {
            RBConnectionVO vo = ServiceResolutionBuilder.getConnection(id, username);
            result.setData(vo).setSuccess(true);
        }
        catch (Exception e)
        {
            Log.log.error(ERROR_GETTING_CONNECTION_INFO, e);
            result.setSuccess(false).setMessage(ERROR_GETTING_CONNECTION_INFO);
        }

        return result;
    }

    @RequestMapping(value = "/resolutionbuilder/saveConnection", method = { RequestMethod.POST, RequestMethod.GET }, headers = "Accept=application/json; charset=utf-8")
    @ResponseBody
    public ResponseDTO<RBConnectionVO> saveConnection(@RequestBody JSONObject jsonProperty, HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException
    {
        //using jackson to deserialize the json
        String json = jsonProperty.toString();
        RBConnectionVO entity = new ObjectMapper().readValue(json, RBConnectionVO.class);

        ResponseDTO<RBConnectionVO> result = new ResponseDTO<RBConnectionVO>();
        String username = (String) request.getAttribute(Constants.HTTP_REQUEST_USERNAME);

        try
        {
            //UI may not set "sys_id" which is actually "id" for UI
            entity.setSys_id(entity.getId());
            RBConnectionVO vo = ServiceResolutionBuilder.saveConnection(entity, username);
            result.setSuccess(true).setData(vo);
        }
        catch (Exception e)
        {
            result.setSuccess(false).setMessage(checkAndSetErrorMessage(e, ERROR_SAVING_CONDITION_INFO));
        }

        return result;
    }

    @RequestMapping(value = "/resolutionbuilder/deleteConnection", method = { RequestMethod.POST, RequestMethod.GET }, headers = "Accept=application/json; charset=utf-8")
    @ResponseBody
    public ResponseDTO<String> deleteConnection(@RequestParam String id, HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException
    {
        ResponseDTO<String> result = new ResponseDTO<String>();
        String username = (String) request.getAttribute(Constants.HTTP_REQUEST_USERNAME);
        
        try
        {
            boolean success = ServiceResolutionBuilder.deleteConnection(id, username);
            result.setSuccess(success);
        }
        catch (Exception e)
        {
            Log.log.error(ERROR_DELETING_CONNECTION_INFO, e);
            result.setSuccess(false).setMessage(ERROR_DELETING_CONNECTION_INFO);
        }

        return result;
    }

    @RequestMapping(value = "/resolutionbuilder/getIf", method = { RequestMethod.POST, RequestMethod.GET }, headers = "Accept=application/json; charset=utf-8")
    @ResponseBody
    public ResponseDTO<RBIfVO> getIf(@RequestParam String id, HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException
    {
        ResponseDTO<RBIfVO> result = new ResponseDTO<RBIfVO>();
        String username = (String) request.getAttribute(Constants.HTTP_REQUEST_USERNAME);
        
        try
        {
            RBIfVO vo = ServiceResolutionBuilder.getIf(id, username);
            result.setData(vo).setSuccess(true);
        }
        catch (Exception e)
        {
            Log.log.error(ERROR_GETTING_IF_INFO, e);
            result.setSuccess(false).setMessage(ERROR_GETTING_IF_INFO);
        }

        return result;
    }

    @RequestMapping(value = "/resolutionbuilder/saveIf", method = { RequestMethod.POST, RequestMethod.GET }, headers = "Accept=application/json; charset=utf-8")
    @ResponseBody
    public ResponseDTO<RBIfVO> saveIf(@RequestBody JSONObject jsonProperty, HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException
    {
        //using jackson to deserialize the json
        String json = jsonProperty.toString();
        RBIfVO entity = new ObjectMapper().readValue(json, RBIfVO.class);

        ResponseDTO<RBIfVO> result = new ResponseDTO<RBIfVO>();
        String username = (String) request.getAttribute(Constants.HTTP_REQUEST_USERNAME);

        try
        {
            //UI may not set "sys_id" which is actually "id" for UI
            entity.setSys_id(entity.getId());
            RBIfVO vo = ServiceResolutionBuilder.saveLoop(entity, username);
            result.setSuccess(true).setData(vo);
        }
        catch (Exception e)
        {
            result.setSuccess(false).setMessage(checkAndSetErrorMessage(e, ERROR_SAVING_IF_INFO));
        }

        return result;
    }

    @RequestMapping(value = "/resolutionbuilder/deleteIf", method = { RequestMethod.POST, RequestMethod.GET }, headers = "Accept=application/json; charset=utf-8")
    @ResponseBody
    public ResponseDTO<String> deleteIf(@RequestParam String id, HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException
    {
        ResponseDTO<String> result = new ResponseDTO<String>();
        String username = (String) request.getAttribute(Constants.HTTP_REQUEST_USERNAME);
        
        try
        {
            boolean success = ServiceResolutionBuilder.deleteIf(id, username);
            result.setSuccess(success);
        }
        catch (Exception e)
        {
            Log.log.error(ERROR_DELETING_IF_INFO, e);
            result.setSuccess(false).setMessage(ERROR_DELETING_IF_INFO);
        }

        return result;
    }

    @RequestMapping(value = "/resolutionbuilder/getCondition", method = { RequestMethod.POST, RequestMethod.GET }, headers = "Accept=application/json; charset=utf-8")
    @ResponseBody
    public ResponseDTO<RBConditionVO> getCondition(@RequestParam String id, HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException
    {
        ResponseDTO<RBConditionVO> result = new ResponseDTO<RBConditionVO>();
        String username = (String) request.getAttribute(Constants.HTTP_REQUEST_USERNAME);
        
        try
        {
            RBConditionVO vo = ServiceResolutionBuilder.getCondition(id, username);
            result.setData(vo).setSuccess(true);
        }
        catch (Exception e)
        {
            Log.log.error(ERROR_GETTING_CONDITION_INFO, e);
            result.setSuccess(false).setMessage(ERROR_GETTING_CONDITION_INFO);
        }

        return result;
    }

    @RequestMapping(value = "/resolutionbuilder/saveCondition", method = { RequestMethod.POST, RequestMethod.GET }, headers = "Accept=application/json; charset=utf-8")
    @ResponseBody
    public ResponseDTO<RBConditionVO> saveCondition(@RequestBody JSONObject jsonProperty, HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException
    {
        //using jackson to deserialize the json
        String json = jsonProperty.toString();
        RBConditionVO entity = new ObjectMapper().readValue(json, RBConditionVO.class);

        ResponseDTO<RBConditionVO> result = new ResponseDTO<RBConditionVO>();
        String username = (String) request.getAttribute(Constants.HTTP_REQUEST_USERNAME);

        try
        {
            //UI may not set "sys_id" which is actually "id" for UI
            entity.setSys_id(entity.getId());
            RBConditionVO vo = ServiceResolutionBuilder.saveCondition(entity, username);
            result.setSuccess(true).setData(vo);
        }
        catch (Exception e)
        {
            result.setSuccess(false).setMessage(checkAndSetErrorMessage(e, ERROR_SAVING_CONDITION_INFO));
        }

        return result;
    }
    
    @RequestMapping(value = "/resolutionbuilder/deleteCondition", method = { RequestMethod.POST, RequestMethod.GET }, headers = "Accept=application/json; charset=utf-8")
    @ResponseBody
    public ResponseDTO<String> deleteCondition(@RequestParam String id, HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException
    {
        ResponseDTO<String> result = new ResponseDTO<String>();
        String username = (String) request.getAttribute(Constants.HTTP_REQUEST_USERNAME);
        
        try
        {
            boolean success = ServiceResolutionBuilder.deleteCondition(id, username);
            result.setSuccess(success);
        }
        catch (Exception e)
        {
            Log.log.error(ERROR_DELETING_CONDITION_INFO, e);
            result.setSuccess(false).setMessage(ERROR_DELETING_CONDITION_INFO);
        }

        return result;
    }

    @RequestMapping(value = "/resolutionbuilder/getTask", method = { RequestMethod.POST, RequestMethod.GET }, headers = "Accept=application/json; charset=utf-8")
    @ResponseBody
    public ResponseDTO<RBTaskVO> getTask(@RequestParam String id, HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException
    {
        ResponseDTO<RBTaskVO> result = new ResponseDTO<RBTaskVO>();
        String username = (String) request.getAttribute(Constants.HTTP_REQUEST_USERNAME);
        
        try
        {
            RBTaskVO vo = ServiceResolutionBuilder.getTask(id, username);
            result.setData(vo).setSuccess(true);
        }
        catch (Exception e)
        {
            Log.log.error(ERROR_GETTING_TASK_INFO, e);
            result.setSuccess(false).setMessage(ERROR_GETTING_TASK_INFO);
        }

        return result;
    }

    @RequestMapping(value = "/resolutionbuilder/saveTask", method = { RequestMethod.POST, RequestMethod.GET }, headers = "Accept=application/json; charset=utf-8")
    @ResponseBody
    public ResponseDTO<RBTaskVO> saveTask(@RequestBody JSONObject jsonProperty, HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException
    {
        //using jackson to deserialize the json
        String json = jsonProperty.toString();
        RBTaskVO entity = new ObjectMapper().configure(DeserializationConfig.Feature.FAIL_ON_UNKNOWN_PROPERTIES, false).readValue(json, RBTaskVO.class);

        ResponseDTO<RBTaskVO> result = new ResponseDTO<RBTaskVO>();
        String username = (String) request.getAttribute(Constants.HTTP_REQUEST_USERNAME);

        try
        {
            //UI may not set "sys_id" which is actually "id" for UI
            entity.setSys_id(entity.getId());
            
            String body = entity.getBody();
            if(StringUtils.isNotBlank(body)) {
                body = URLDecoder.decode(body, "UTF-8");
                entity.setBody(body);
            }

            RBTaskVO vo = ServiceResolutionBuilder.saveTask(entity, username);
            
            result.setSuccess(true).setData(vo);
        }
        catch (Exception e)
        {
            result.setSuccess(false).setMessage(checkAndSetErrorMessage(e, ERROR_SAVING_TASK_INFO));
        }

        return result;
    }

    @RequestMapping(value = "/resolutionbuilder/deleteTask", method = { RequestMethod.POST, RequestMethod.GET }, headers = "Accept=application/json; charset=utf-8")
    @ResponseBody
    public ResponseDTO<String> deleteTask(@RequestParam String id, HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException
    {
        ResponseDTO<String> result = new ResponseDTO<String>();
        String username = (String) request.getAttribute(Constants.HTTP_REQUEST_USERNAME);
        
        try
        {
            boolean success = ServiceResolutionBuilder.deleteTask(id, username);
            result.setSuccess(success);
        }
        catch (Exception e)
        {
            Log.log.error(ERROR_DELETING_TASK_INFO, e);
            result.setSuccess(false).setMessage(ERROR_DELETING_TASK_INFO);
        }

        return result;
    }

    @RequestMapping(value = "/resolutionbuilder/getRBTree", method = { RequestMethod.POST, RequestMethod.GET }, headers = "Accept=application/json; charset=utf-8")
    @ResponseBody
    public ResponseDTO<Object> getRBTree(@RequestParam String id, HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException
    {
        ResponseDTO<Object> result = new ResponseDTO<Object>();
        String username = (String) request.getAttribute(Constants.HTTP_REQUEST_USERNAME);
        
        try
        {
            Object tree = ServiceResolutionBuilder.getRBTree(id, username);
            result.setData(tree).setSuccess(true);
        }
        catch (Exception e)
        {
            Log.log.error(ERROR_GETTING_GENERAL_INFO, e);
            result.setSuccess(false).setMessage(ERROR_GETTING_GENERAL_INFO);
        }

        return result;
    }
    
    @RequestMapping(value = "/resolutionbuilder/generate/poll", method = { RequestMethod.POST, RequestMethod.GET }, headers = "Accept=application/json; charset=utf-8")
    @ResponseBody
    public ResponseDTO<JSONObject> pollGeneration(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException
    {
        ResponseDTO<JSONObject> result = new ResponseDTO<JSONObject>();
        String username = (String) request.getAttribute(Constants.HTTP_REQUEST_USERNAME);
        
        try
        {
            JSONObject status = ServiceResolutionBuilder.pollABGeneration(username);
            result.setData(status);
            result.setSuccess(true);
        }
        catch (Exception e)
        {
            Log.log.error(ERROR_FETCHING_GENERATION_PROGRESS, e);
            result.setSuccess(false).setMessage(ERROR_FETCHING_GENERATION_PROGRESS);
        }

        return result;
    }

    @RequestMapping(value = "/resolutionbuilder/generate", method = { RequestMethod.POST, RequestMethod.GET }, headers = "Accept=application/json; charset=utf-8")
    @ResponseBody
    public ResponseDTO<Object> generate(@RequestParam String id, HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException
    {
        ResponseDTO<Object> result = new ResponseDTO<Object>();
        String username = (String) request.getAttribute(Constants.HTTP_REQUEST_USERNAME);
        
        try
        {
            ABProcessConst c = ServiceResolutionBuilder.generate(id, username);
            result.setData(c).setSuccess(true);
        }
        catch (Exception e)
        {
            Log.log.error(ERROR_GENERATING_RUNBOOK, e);
            result.setSuccess(false).setMessage(ERROR_GENERATING_RUNBOOK);
        }

        return result;
    }
    
    @RequestMapping(value = "/resolutionbuilder/testCode/poll", method = { RequestMethod.POST, RequestMethod.GET }, headers = "Accept=application/json; charset=utf-8")
    @ResponseBody
    public ResponseDTO<JSONObject> pollTestCode( HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException
    {
        ResponseDTO<JSONObject> result = new ResponseDTO<JSONObject>();
        String username = (String) request.getAttribute(Constants.HTTP_REQUEST_USERNAME);
        
        try
        {
            JSONObject status = ServiceResolutionBuilder.pollParserCodeTest(username);
            result.setData(status).setSuccess(true);
        }
        catch (Exception e)
        {
            Log.log.error(ERROR_GENERATING_RUNBOOK, e);
            result.setSuccess(false).setMessage(ERROR_GENERATING_RUNBOOK);
        }

        return result;
    }
    
    @RequestMapping(value = "/resolutionbuilder/testCode", method = { RequestMethod.POST, RequestMethod.GET }, headers = "Accept=application/json; charset=utf-8")
    @ResponseBody
    public ResponseDTO<Object> testCode(@RequestParam("code") String code,@RequestParam("testData") String testData, HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException
    {
        ResponseDTO<Object> result = new ResponseDTO<Object>();
        String username = (String) request.getAttribute(Constants.HTTP_REQUEST_USERNAME);
        
        try
        {
            String assessorScript = code;
            ABProcessConst c = ServiceResolutionBuilder.testCode(assessorScript, testData, username);
            result.setData(c).setSuccess(true);
        }
        catch (Exception e)
        {
            Log.log.error(ERROR_GENERATING_RUNBOOK, e);
            result.setSuccess(false).setMessage(ERROR_GENERATING_RUNBOOK);
        }

        return result;
    }
    
    @RequestMapping(value = "/resolutionbuilder/saveSubrunbook", method = { RequestMethod.POST, RequestMethod.GET }, headers = "Accept=application/json; charset=utf-8")
    @ResponseBody
    public ResponseDTO<RBTaskVO> saveRunbookRef(@RequestBody JSONObject jsonProperty, HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException
    {
        //using jackson to deserialize the json
        String json = jsonProperty.toString();
        RBTaskVO entity = new ObjectMapper().configure(DeserializationConfig.Feature.FAIL_ON_UNKNOWN_PROPERTIES, false).readValue(json, RBTaskVO.class);

        ResponseDTO<RBTaskVO> result = new ResponseDTO<RBTaskVO>();
        String username = (String) request.getAttribute(Constants.HTTP_REQUEST_USERNAME);

        try
        {
            //UI may not set "sys_id" which is actually "id" for UI
            entity.setSys_id(entity.getId());
            
            if (entity.getName().indexOf(".") == -1)
            {
                entity.setName(StringUtils.remove(entity.getName().trim(), " ") + "." + entity.getName());
            }
            
            //we will have a real vo for runbook, but for now let's use the param as a flag...
            if(StringUtils.isEmpty(entity.getRefRunbookParams())||entity.getRefRunbookParams().equals("UNDEFINED"))
                entity.setRefRunbookParams("{}");
            RBTaskVO vo = ServiceResolutionBuilder.saveTask(entity, username);
            result.setSuccess(true).setData(vo);
        }
        catch (Exception e)
        {
            result.setSuccess(false).setMessage(checkAndSetErrorMessage(e, ERROR_SAVING_SUBRUNBOOK_INFO));
        }

        return result;
    }
    
    @RequestMapping(value = "/resolutionbuilder/getSubrunbook", method = { RequestMethod.POST, RequestMethod.GET }, headers = "Accept=application/json; charset=utf-8")
    @ResponseBody
    public ResponseDTO<RBTaskVO> getRunbookRef(@RequestParam String id, HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException
    {
        ResponseDTO<RBTaskVO> result = new ResponseDTO<RBTaskVO>();
        String username = (String) request.getAttribute(Constants.HTTP_REQUEST_USERNAME);
        
        try
        {
            RBTaskVO vo = ServiceResolutionBuilder.getTask(id, username);
            result.setData(vo).setSuccess(true);
        }
        catch (Exception e)
        {
            Log.log.error(ERROR_GETTING_SUBRUNBOOK_INFO, e);
            result.setSuccess(false).setMessage(ERROR_GETTING_SUBRUNBOOK_INFO);
        }

        return result;
    }
    
    @RequestMapping(value = "/resolutionbuilder/deleteSubrunbook", method = { RequestMethod.POST, RequestMethod.GET }, headers = "Accept=application/json; charset=utf-8")
    @ResponseBody
    public ResponseDTO<String> deleteRunbookRef(@RequestParam String id, HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException
    {
        ResponseDTO<String> result = new ResponseDTO<String>();
        String username = (String) request.getAttribute(Constants.HTTP_REQUEST_USERNAME);
        
        try
        {
            boolean success = ServiceResolutionBuilder.deleteTask(id, username);
            result.setSuccess(success);
        }
        catch (Exception e)
        {
            Log.log.error(ERROR_DELETING_SUBRUNBOOK_INFO, e);
            result.setSuccess(false).setMessage(ERROR_DELETING_SUBRUNBOOK_INFO);
        }

        return result;
    }
}
