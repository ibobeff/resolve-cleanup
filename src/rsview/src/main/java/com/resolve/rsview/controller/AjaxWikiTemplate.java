/******************************************************************************
* (C) Copyright 2014
*
* Resolve Systems, LLC
*
* All rights reserved
* This software is distributed under license and may not be used, copied,
* modified, or distributed without the express written permission of
* Resolve Systems, LLC.
*
******************************************************************************/

package com.resolve.rsview.controller;

import java.io.IOException;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.stereotype.Controller;
import org.springframework.stereotype.Service;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.resolve.services.ServiceHibernate;
import com.resolve.services.hibernate.vo.WikiDocumentMetaFormRelVO;
import com.resolve.services.vo.QueryDTO;
import com.resolve.services.vo.ResponseDTO;
import com.resolve.util.Constants;
import com.resolve.util.Log;
import com.resolve.util.json.PropertyStrategyWrapper;

import net.sf.json.JSONObject;
import net.sf.json.JSONSerializer;
import net.sf.json.JsonConfig;
import net.sf.json.util.PropertySetStrategy;

/**
 * Wiki Template controller allows CRUD operation on Wiki Template feature from UI.
 *
 */
@Controller
@Service
public class AjaxWikiTemplate extends GenericController
{
    /**
     * Returns list of Wiki Template for the grid based on filters, sorts, pagination
     * 
     * @param query : QueryDTO - provides start, limit, sorting, and filtering (See {@link QueryDTO} for more information)
     * <br>
     *  Example:<br>
     *  filter: [{"field":"uisActive","type":"bool","condition":"equals","value":true}]<br>
     *  page: 3<br>
     *  start: 100<br>
     *  limit: 50<br>
     *  group: [{"property":"unamespace","direction":"ASC"}]<br>
     *  sort: [{"property":"unamespace","direction":"ASC"},{"property":"uhasActiveModel","direction":"DESC"}]
     * @param request {@link HttpServletRequest}
     * @param response {@link HttpServletResponse}
     * @return
     * @throws ServletException
     * @throws IOException
     */
    @RequestMapping(value = "/wikitemplate/list", method = { RequestMethod.GET, RequestMethod.POST }, headers = "Accept=application/json; charset=utf-8")
    @ResponseBody
    public ResponseDTO list(@ModelAttribute QueryDTO query, HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException
    {
        ResponseDTO result = new ResponseDTO();
        String username = (String) request.getAttribute(Constants.HTTP_REQUEST_USERNAME);
        try
        {
            query.setModelName("WikiDocumentMetaFormRel");
//            query.setSelectColumns("sys_id,UName,UDescription,sysUpdatedOn");
            
            List<WikiDocumentMetaFormRelVO> data = ServiceHibernate.getWikiTemplate(query, username);
            int total = ServiceHibernate.getTotalHqlCount(query);
            
            result.setSuccess(true).setRecords(data);
            result.setTotal(total);
        }
        catch (Exception e)
        {
            Log.log.error("Error retrieving wiki template list.", e);
            result.setSuccess(false).setMessage("Error retrieving wiki template list.");
        }
        
        return result;
    }
    
    /**
     * Gets data for a specific Resolve Template to be edited based on sysId
     * 
     * @param id : String representing Wiki Template SysID
     * @param request {@link HttpServletRequest}
     * @param response {@link HttpServletResponse}
     * @return
     * @throws ServletException
     * @throws IOException
     */
    @RequestMapping(value = "/wikitemplate/get", method = { RequestMethod.POST }, headers = "Accept=application/json; charset=utf-8")
    @ResponseBody
    public ResponseDTO get(@RequestParam String id, HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException
    {
        ResponseDTO result = new ResponseDTO();
        String username = (String) request.getAttribute(Constants.HTTP_REQUEST_USERNAME);
        try
        {
            WikiDocumentMetaFormRelVO vo = ServiceHibernate.findWikiTemplateById(id, username);
            result.setData(vo);
            result.setSuccess(true);
            
        }
        catch (Exception e)
        {
            Log.log.error("Error retrieving wiki template.", e);
            result.setSuccess(false).setMessage("Error retrieving wiki template.");
        }

        return result;
    }
    
    /**
     * Delete the ResolveWikiLookup based on array of sysIds
     * 
     * @param ids : String array of Wiki Template SysIds to be deleted.
     * @param request {@link HttpServletRequest}
     * @param response {@link HttpServletResponse}
     * @return
     * @throws ServletException
     * @throws IOException
     */
    @RequestMapping(value = "/wikitemplate/delete", method = { RequestMethod.POST }, headers = "Accept=application/json; charset=utf-8")
    @ResponseBody
    public ResponseDTO delete(@RequestBody String[] ids, HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException
    {
        ResponseDTO result = new ResponseDTO();
        String username = (String) request.getAttribute(Constants.HTTP_REQUEST_USERNAME);
        
        try
        {
            ServiceHibernate.deleteWikiTemplateByIds(ids, username);
            result.setSuccess(true);
        }
        catch (Exception e)
        {
            Log.log.error("Error deleting wiki template.", e);
            result.setSuccess(false).setMessage("Error deleting wiki template.");
        }

        return result;
    }
    
    /**
     * Saves/Update an ResolveWikiLookup
     * 
     * @param jsonProperty : JSONObject representation of WikiDocumentMetaFormRelVO
     * @param request {@link HttpServletRequest}
     * @param response {@link HttpServletResponse}
     * @return
     * @throws ServletException
     * @throws IOException
     */
    @RequestMapping(value = "/wikitemplate/save", method = { RequestMethod.POST }, headers = "Accept=application/json; charset=utf-8")
    @ResponseBody
    public ResponseDTO save(@RequestBody JSONObject jsonProperty, HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException
    {
      //using jackson to deserialize the json
//        String json = jsonProperty.toString();
//        WikiDocumentMetaFormRelVO entity = new ObjectMapper().readValue(json, WikiDocumentMetaFormRelVO.class);
        
        //convert json to VO - spring does not work as the VO are not 100% java bean
        JsonConfig jsonConfig = new JsonConfig();
        jsonConfig.setRootClass(WikiDocumentMetaFormRelVO.class);
        jsonConfig.setPropertySetStrategy(new PropertyStrategyWrapper(PropertySetStrategy.DEFAULT));
        WikiDocumentMetaFormRelVO entity = (WikiDocumentMetaFormRelVO) JSONSerializer.toJava(jsonProperty, jsonConfig);
        
        ResponseDTO result = new ResponseDTO();
        String username = (String) request.getAttribute(Constants.HTTP_REQUEST_USERNAME);
        
        try
        {
            WikiDocumentMetaFormRelVO vo = ServiceHibernate.saveWikiTemplate(entity, username);
            result.setSuccess(true).setData(vo);
        }
        catch (Exception e)
        {
            Log.log.error("Error saving wiki template.", e);
            result.setSuccess(false).setMessage("Error saving wiki template.");
        }

        return result;
    }
    
    /**
     * Create a copy of any existing Wiki Template
     * <br>
     * @param fromTemplate : String name of the template that we want make a copy of
     * @param toTemplate : String new template name
     * @param request {@link HttpServletRequest}
     * @param response {@link HttpServletResponse}
     * @return
     * @throws ServletException
     * @throws IOException
     */
    @RequestMapping(value = "/wikitemplate/copy", method = { RequestMethod.POST }, headers = "Accept=application/json; charset=utf-8")
    @ResponseBody
    public ResponseDTO copy(@RequestParam("oldName") String fromTemplate, @RequestParam("newName")  String toTemplate, HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException
    {
        ResponseDTO result = new ResponseDTO();
        String username = (String) request.getAttribute(Constants.HTTP_REQUEST_USERNAME);
        
        try
        {
            WikiDocumentMetaFormRelVO newTemplate = ServiceHibernate.copyTemplate(fromTemplate, toTemplate, username);
            result.setSuccess(true).setData(newTemplate);
        }
        catch (Exception e)
        {
            Log.log.error("Error copying wiki template.", e);
            result.setSuccess(false).setMessage("Error copying wiki template.");
        }

        return result;
    }
    
    /**
     * Rename existing Wiki Template.
     * <br>
     * @param fromTemplate : String name of the template that we want to rename to.
     * @param toTemplate : String new template name
     * @param request {@link HttpServletRequest}
     * @param response {@link HttpServletResponse}
     * @return
     * @throws ServletException
     * @throws IOException
     */
    @RequestMapping(value = "/wikitemplate/rename", method = { RequestMethod.POST }, headers = "Accept=application/json; charset=utf-8")
    @ResponseBody
    public ResponseDTO rename(@RequestParam("oldName") String fromTemplate, @RequestParam("newName")  String toTemplate, HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException
    {
        ResponseDTO result = new ResponseDTO();
        String username = (String) request.getAttribute(Constants.HTTP_REQUEST_USERNAME);
        
        try
        {
            WikiDocumentMetaFormRelVO newTemplate = ServiceHibernate.renameTemplate(fromTemplate, toTemplate, username);
            result.setSuccess(true).setData(newTemplate);
        }
        catch (Exception e)
        {
            Log.log.error("Error renaming wiki template.", e);
            result.setSuccess(false).setMessage("Error renaming wiki template.");
        }

        return result;
    }
}
