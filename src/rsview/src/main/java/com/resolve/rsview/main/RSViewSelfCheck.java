/******************************************************************************
* (C) Copyright 2015
*
* Resolve Systems, LLC
*
* All rights reserved
* This software is distributed under license and may not be used, copied,
* modified, or distributed without the express written permission of
* Resolve Systems, LLC.
*
******************************************************************************/

package com.resolve.rsview.main;

import java.net.HttpURLConnection;
import java.net.URL;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.elasticsearch.action.admin.cluster.health.ClusterHealthResponse;
import org.elasticsearch.cluster.health.ClusterHealthStatus;
import org.elasticsearch.client.Requests;
import org.elasticsearch.common.Priority;

import com.novell.ldap.LDAPConnection;
import com.novell.ldap.LDAPException;
import com.novell.ldap.LDAPJSSESecureSocketFactory;
import com.resolve.search.SearchAdminAPI;
import com.resolve.sql.SQL;
import com.resolve.sql.SQLConnection;
import com.resolve.util.ERR;
import com.resolve.util.GMTDate;
import com.resolve.util.Log;
import com.resolve.util.StringUtils;

public class RSViewSelfCheck
{
	private static final String selfCheckValue = "RSVIEWL DB SELFCHECK";
	static int dbTimeout = 5; //how long to wait for heartbeat completion before notifying
	static int esSCRetryCount;
    static int esSCRetryDelay;
    static String dbType;
    static int thresholdTps;
    static String host;
    static int port;
    static boolean ssl;
    static int pingThreshold;
    
    public RSViewSelfCheck()
    {
    }
    
	public void checkDBConnectivity()
	{
		RSViewDBSelfCheck rsviewDBSelfCheck = new RSViewDBSelfCheck();
		try
        {
            Thread thread = new Thread(rsviewDBSelfCheck);
            thread.start();
            
            long startWait = System.currentTimeMillis();
            int waitTime = dbTimeout * 1000;
            long elapsedTime = 0;
            synchronized(rsviewDBSelfCheck)
            {
                while(elapsedTime < waitTime)
                {
                    elapsedTime = System.currentTimeMillis() - startWait;
                    rsviewDBSelfCheck.wait(1000);
                    if (rsviewDBSelfCheck.isBusy())
                    {
                        Log.log.trace("DB Heartbeat Still Waiting");
                    }
                    else
                    {
                        Log.log.trace("DB Heartbeat Finished");
                        break;
                    }
                }
            }
            if (elapsedTime > waitTime)
            {
                Log.alert(ERR.E20005, "DB Heartbeat Failed to Complete in " + waitTime + " seconds");
            }
        }
        catch(Exception e)
        {
            Log.alert(ERR.E20005, e);
        }
	}
	
	class RSViewDBSelfCheck implements Runnable
	{
		private boolean busy;
		
		@Override
		public void run()
		{
			busy = true;
	        SQLConnection conn = null;
	        try
	        {
	            conn = SQL.getConnection();
	                
	            String sql = "update resolve_event set sys_updated_on=? where u_value='" + selfCheckValue + "'";
	            Log.log.trace("DB Self Check update sql: " + sql);
	            
	            PreparedStatement ps = conn.prepareStatement(sql);
	            Timestamp updatedDate = new Timestamp(GMTDate.getTime());
	            
	            ps.setTimestamp(1, updatedDate);
	            
	            Log.start("Begin Update Execute", "debug");
	            int update = ps.executeUpdate();
	            Log.duration("Finish Update Execute", "debug");
	            Log.log.debug("DB Self Check Run, " + update + " rows updated");
	            
	        }
	        catch (Throwable t)
	        {
	            Log.alert(ERR.E20005, t);
	        }
	        finally
	        {
	            if (conn != null)
	            {
	                SQL.close(conn);
	            }
	            busy = false;
	            synchronized(this)
	            {
	                notifyAll();
	            }
	        }
		}

		public boolean isBusy() {
			return busy;
		}

		public void setBusy(boolean busy) {
			this.busy = busy;
		}
	}
	
	public void checkESHealth()
    {
        boolean result = false;
        for (int i = 0; i < esSCRetryCount; ++i)
        {
            try
            {
                Log.log.debug("Checking RSSearch status. Count: " + i);
                ClusterHealthResponse actionGet = SearchAdminAPI.getClient().admin().cluster().health(Requests.clusterHealthRequest().waitForYellowStatus().waitForEvents(Priority.LANGUID).waitForActiveShards(0)).actionGet();
                if (actionGet.isTimedOut())
                {
                    Log.log.info("ensureYellow timed out, cluster state: " + SearchAdminAPI.getClient().admin().cluster().prepareState().get().getState() + " \n " + SearchAdminAPI.getClient().admin().cluster().preparePendingClusterTasks().get());
                    Thread.sleep(esSCRetryDelay * 1000);
                }
                else if(ClusterHealthStatus.RED.equals(actionGet.getStatus()))
                {
                    Log.log.info("RSSearch cluster status is RED, waiting to be yellow.");
                    Thread.sleep(esSCRetryDelay * 1000);
                }
                else
                {
                	Log.log.info("RSSearch cluster status is good (GREEN or YELLOW).");
                    result = actionGet.getStatus().equals(ClusterHealthStatus.GREEN) || actionGet.getStatus().equals(ClusterHealthStatus.YELLOW);
                    break;
                }
            }
            catch(Exception e)
            {
                Log.log.warn(e.getMessage(), e);
            }
        }
        
        if (result == false)
        {
            String message = "RSVIEW ES Self Check: ES status is red after " + esSCRetryCount + " attempts with delay of " + esSCRetryDelay + " secs.";
            Exception t = new Exception(message);
            Log.log.fatal(t.getMessage(), t);
        }
    }
	
	public void checkLdapAdConnection()
	{
		LDAPConnection conn = null;
			
		if (ssl)
		{
			Log.log.debug("Creating Secure LDAP Connection Object");
            LDAPJSSESecureSocketFactory socketFactory = new LDAPJSSESecureSocketFactory();
            conn = new LDAPConnection(socketFactory);
		}
		else
		{
			conn = new LDAPConnection();
		}
		
		if (conn != null)
        {
			try
			{
                // connect to ldap server
                Log.log.debug("RsView self check - Connecting to LDAP/AD");
                conn.connect(host, port);
                Log.log.info("RsView self check - LDAP/AD Connect successfully to host " + host + " and port " +port);
			}
			catch(Exception e)
			{
				Log.alert(ERR.E60004);
			}
			finally
			{
				try
				{
					conn.disconnect();
				}
				catch (LDAPException e)
				{
					e.printStackTrace();
				}
			}
        }
	}
	
	static int httpPort;
    static String httpHost;
    static String httpProtocol;
    
    public void rsviewHttpSelfCheck()
    {
    	URL url = null;
    	HttpURLConnection connection = null;
    	try
    	{
    		if (httpPort != 0 && StringUtils.isNotBlank(httpHost))
    		{
    			if (StringUtils.isBlank(httpProtocol))
    			{
    				httpProtocol = "http";
    			}
    			url = new URL(httpProtocol + "://" + httpHost.trim() + ":" + httpPort + "/resolve/service/selfcheck/ping");
    			connection = (HttpURLConnection) url.openConnection();
                connection.setReadTimeout(15000);
                connection.setConnectTimeout(15000);
                connection.setRequestMethod("POST");
                connection.setDoOutput(true);
                
                int rc = connection.getResponseCode();
                if (rc != 200)
                {
                    Log.log.error("Instead of success, response code of " + rc + " is returned for RsView " + httpProtocol + " self check.");
                    Log.alert(ERR.E70008);
                }
    		}
    		else
    		{
    			Log.log.error("For RsView" + httpProtocol + " self check Port and Host is required.");
    		}
    	}
    	catch(Exception e)
    	{
    		Log.alert(ERR.E70008, e);
    	}
    	finally
    	{
    	    if(connection != null)
    	    {
    	    	connection.disconnect();
    	    }
    	}
    }
    
    @SuppressWarnings("unchecked")
	public void pingRsControl()
    {
    	List<String> guids = getDeployedRSControlGUID();
    	Map<String, String> response = new HashMap<String, String>();
    	for (String guid : guids)
    	{
    		try
			{
    			Log.log.trace("Sending ESB ping to RSCONTROL-" + guid);
    			response = Main.esb.call(guid, "MAction.esbPing", new HashMap<String, String>(), pingThreshold*1000);
    			if (response == null)
    			{
    				Log.log.error("ESB Ping has not been received from RSCONTROL-" + guid + " since the last ping for " + pingThreshold + " seconds. MQ could be too busy or component may not be running.");
    				Log.alert(ERR.E20010);
    			}
    			else
    			{
    				Log.log.trace("Received ESB ping from RSCONTROL-" + guid);
    			}
			}
    		catch (Exception e)
			{
    			Log.log.error(e.getMessage(), e);
			}
    	}
    }
    
    private List<String> getDeployedRSControlGUID()
    {
    	List<String> rsRemoteHuidList = new ArrayList<String>();
    	SQLConnection conn = null;
        try
        {
            conn = SQL.getConnection();
            
            String sql = "select u_guid from resolve_registration where u_type=? and u_status = 'ACTIVE'";
            
            PreparedStatement ps = conn.prepareStatement(sql);
            
            ps.setString(1, "RSCONTROL");
            
            ResultSet rs = ps.executeQuery();
            
            if (rs != null)
            {
            	while(rs.next())
            	{
            		String guid = rs.getString("u_guid");
            		if (StringUtils.isNotBlank(guid))
            		{
            			rsRemoteHuidList.add(guid);
            		}
            	}
            }
            
            
        }
        catch (Throwable t)
        {
            Log.log.error("Could not get deployed RSRemotes from DB.", t);
        }
        finally
        {
            if (conn != null)
            {
                SQL.close(conn);
            }
        }
        
        return rsRemoteHuidList;
    }
}
