/******************************************************************************
* (C) Copyright 2014
*
* Resolve Systems, LLC
*
* All rights reserved
* This software is distributed under license and may not be used, copied,
* modified, or distributed without the express written permission of
* Resolve Systems, LLC.
*
******************************************************************************/

package com.resolve.rsview.controller;

import java.io.IOException;
import java.text.DateFormat;
import java.util.Calendar;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Locale;
import java.util.Map;
import java.util.Set;
import java.util.TimeZone;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.owasp.esapi.errors.AccessControlException;
import org.owasp.esapi.reference.DefaultHTTPUtilities;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

import com.resolve.rsview.main.RSContext;
import com.resolve.rsview.util.ControllerUtil;
import com.resolve.services.ServiceHibernate;
import com.resolve.services.ServiceWiki;
import com.resolve.services.constants.ConstantValues;
import com.resolve.util.constants.HibernateConstants;
import com.resolve.services.constants.WikiAction;
import com.resolve.services.hibernate.vo.WikiDocumentVO;
import com.resolve.util.Constants;
import com.resolve.util.HttpUtil;
import com.resolve.util.Log;
import com.resolve.util.StringUtils;
import com.resolve.wiki.api.WebApi;

/**
 * 
 * @deprecated
 * THIS API IS DEPRECATED. NO TESTING FOR THIS API TILL FURTHUR NOTICE.
 * <p>
 * There was only 1 client that was using this api in the older version. In Resolve 5, most of the use case is different and this api may not be valid anymore.
 * <p>
 * 
 * 
 * @author jeet.marwah
 *
 */
@Controller
public class EditController
{

	/**
	 * @deprecated
	 * THIS API IS DEPRECATED. NO TESTING FOR THIS API TILL FURTHUR NOTICE.
	 * <p>
	 * There was only 1 client that was using this api in the older version. In Resolve 5, most of the use case is different and this api may not be valid anymore.
	 * <p>
	 * This is the entry point for the request to edit a wiki doc.
	 * <p>
	 * 
	 * eg. http://localhost:8080/resolve/service/wiki/edit/Test/test2
	 * 
	 * @param namespace Wiki Namespace
	 * @param docname Wiki Document Name
     * @param request  HttpServletRequest
     * @param response  HttpServletResponse
	 * @return ModelAndView
	 * @throws ServletException
	 * @throws IOException
	 */
	@RequestMapping(value = ConstantValues.URL_EDITDOC, method = { RequestMethod.GET, RequestMethod.POST })
	public ModelAndView edit(@PathVariable("namespace") String namespace, @PathVariable("docname") String docname, HttpServletRequest request,
			HttpServletResponse response) throws ServletException, IOException
	{
		Map<String, String> hmValues = null;

		//set the generic parameters 
		ControllerUtil.setRequestParameters(namespace, docname, WikiAction.edit, request);
		if(request.getParameter(ConstantValues.IS_WYSIWYG_EDIT) != null && request.getParameter(ConstantValues.IS_WYSIWYG_EDIT).trim().equalsIgnoreCase("true"))
		{
			request.setAttribute(ConstantValues.IS_WYSIWYG_EDIT, true);
		}

		try
		{
			WebApi webApi = (WebApi) RSContext.appContext.getBean(ConstantValues.WIKI_SPRINGID_FOR_WEBAPI);
			hmValues = webApi.editAction(request, response);
		}
		catch (Exception e)
		{
			Log.log.error(e.getMessage(), e);
			hmValues.put(ConstantValues.WIKI_CONTENT_KEY, e.getMessage());
		}

		ModelAndView result = new ModelAndView(ConstantValues.WIKI_EDIT_JSP);
		result.addAllObjects(hmValues);

		return result;
	} // welcome

	/**
	 * 
     * @deprecated
     * THIS API IS DEPRECATED. NO TESTING FOR THIS API TILL FURTHUR NOTICE.
     * <p>
     * There was only 1 client that was using this api in the older version. In Resolve 5, most of the use case is different and this api may not be valid anymore.
     * <p>
     * This is for editing the model XML code of a wiki document
     * <p>
     *  
	 * eg. http://localhost:8080/resolve/service/wiki/editmodelc/Test/test2
	 * 
	 * @param namespace
	 * @param docname
	 * @param request
	 * @param response
	 * @return
	 * @throws ServletException
	 * @throws IOException
	 */
	@RequestMapping(value = ConstantValues.URL_EDITMODELC, method = { RequestMethod.GET })
	public ModelAndView editmodelc(@PathVariable("namespace") String namespace, @PathVariable("docname") String docname, HttpServletRequest request,
			HttpServletResponse response) throws ServletException, IOException
	{
		Map<String, String> hmValues = null;

		//set the generic parameters 
		ControllerUtil.setRequestParameters(namespace, docname, WikiAction.editmodelc, request);

		try
		{
			WebApi webApi = (WebApi) RSContext.appContext.getBean(ConstantValues.WIKI_SPRINGID_FOR_WEBAPI);
			hmValues = webApi.editmodelcAction(request, response);
		}
		catch (Exception e)
		{
			Log.log.error(e.getMessage(), e);
			hmValues.put(ConstantValues.WIKI_CONTENT_KEY, e.getMessage());
		}

		ModelAndView result = new ModelAndView(ConstantValues.WIKI_EDIT_JSP);
		result.addAllObjects(hmValues);

		return result;
	} // welcome

	/**
	 * 
     * @deprecated
     * THIS API IS DEPRECATED. NO TESTING FOR THIS API TILL FURTHUR NOTICE.
     * <p>
     * There was only 1 client that was using this api in the older version. In Resolve 5, most of the use case is different and this api may not be valid anymore.
     * <p>
     * For editing the Exception XML Code
     * <p>
	 * 
	 * eg. http://localhost:8080/resolve/service/wiki/editexcpc/Test/test2
	 * 
	 * @param namespace
	 * @param docname
	 * @param request
	 * @param response
	 * @return
	 * @throws ServletException
	 * @throws IOException
	 */
	@RequestMapping(value = ConstantValues.URL_EDITEXCPC, method = { RequestMethod.GET })
	public ModelAndView editexcpc(@PathVariable("namespace") String namespace, @PathVariable("docname") String docname, HttpServletRequest request,
			HttpServletResponse response) throws ServletException, IOException
	{
		Map<String, String> hmValues = null;

		//set the generic parameters 
		ControllerUtil.setRequestParameters(namespace, docname, WikiAction.editexcpc, request);

		try
		{
			WebApi webApi = (WebApi) RSContext.appContext.getBean(ConstantValues.WIKI_SPRINGID_FOR_WEBAPI);
			hmValues = webApi.editexcpcAction(request, response);
		}
		catch (Exception e)
		{
			Log.log.error(e.getMessage(), e);
			hmValues.put(ConstantValues.WIKI_CONTENT_KEY, e.getMessage());
		}

		ModelAndView result = new ModelAndView(ConstantValues.WIKI_EDIT_JSP);
		result.addAllObjects(hmValues);

		return result;
	} // welcome

	/**
	 * 
     * @deprecated
     * THIS API IS DEPRECATED. NO TESTING FOR THIS API TILL FURTHUR NOTICE.
     * <p>
     * There was only 1 client that was using this api in the older version. In Resolve 5, most of the use case is different and this api may not be valid anymore.
     * <p>
     * For the FINAL XML Code
     * <p>
     *  
	 * eg. http://localhost:8080/resolve/service/wiki/editfinalc/Test/test2
	 * 
	 * @param namespace
	 * @param docname
	 * @param request
	 * @param response
	 * @return
	 * @throws ServletException
	 * @throws IOException
	 */
	@RequestMapping(value = ConstantValues.URL_EDITFINALC, method = { RequestMethod.GET })
	public ModelAndView editfinalc(@PathVariable("namespace") String namespace, @PathVariable("docname") String docname, HttpServletRequest request,
			HttpServletResponse response) throws ServletException, IOException
	{
		Map<String, String> hmValues = null;

		//set the generic parameters 
		ControllerUtil.setRequestParameters(namespace, docname, WikiAction.editfinalc, request);

		try
		{
			WebApi webApi = (WebApi) RSContext.appContext.getBean(ConstantValues.WIKI_SPRINGID_FOR_WEBAPI);
			hmValues = webApi.editfinalcAction(request, response);
		}
		catch (Exception e)
		{
			Log.log.error(e.getMessage(), e);
			hmValues.put(ConstantValues.WIKI_CONTENT_KEY, e.getMessage());
		}

		ModelAndView result = new ModelAndView(ConstantValues.WIKI_EDIT_JSP);
		result.addAllObjects(hmValues);

		return result;
	} // welcome

	/**
	 * 
     * @deprecated
     * THIS API IS DEPRECATED. NO TESTING FOR THIS API TILL FURTHUR NOTICE.
     * <p>
     * There was only 1 client that was using this api in the older version. In Resolve 5, most of the use case is different and this api may not be valid anymore.
     * <p>
	 * 
	 * @param namespace
	 * @param docname
	 * @param request
	 * @param response
	 * @return
	 * @throws ServletException
	 * @throws IOException
	 */
	@RequestMapping(value = ConstantValues.URL_SAVEDOC, method = RequestMethod.POST)
	public ModelAndView save(@PathVariable("namespace") String namespace, @PathVariable("docname") String docname, HttpServletRequest request,
			HttpServletResponse response) throws ServletException, IOException
	{
		Map<String, String> hmValues = new HashMap<String, String>();
		ControllerUtil.setRequestParameters(namespace, docname, WikiAction.save, request);
		try
		{
			WebApi webApi = (WebApi) RSContext.appContext.getBean(ConstantValues.WIKI_SPRINGID_FOR_WEBAPI);
			hmValues = webApi.saveAction(request, response);
		}
		catch (Exception e)
		{
			Log.log.error(e.getMessage(), e);
		}

		String forwardUrl = hmValues.get(ConstantValues.WIKI_FORWARD_URL_KEY);

		//		response.sendRedirect("http://localhost:8080/resolve/service/wiki/edit/Test/test2");
		if(StringUtils.isNotEmpty(forwardUrl))
		{
		    response = HttpUtil.sanitizeResponse(response);
		    DefaultHTTPUtilities dhttp = new DefaultHTTPUtilities();
	        try {
	            dhttp.sendRedirect(response, forwardUrl);   
	        } catch(AccessControlException  ace) {
	            Log.log.error(ace.getMessage(), ace);
	        }
		}
		return null;

	}

	/**
	 * 
     * @deprecated
     * THIS API IS DEPRECATED. NO TESTING FOR THIS API TILL FURTHUR NOTICE.
     * <p>
     * There was only 1 client that was using this api in the older version. In Resolve 5, most of the use case is different and this api may not be valid anymore.
     * <p>
	 * 
	 * @param namespace
	 * @param docname
	 * @param request
	 * @param response
	 * @return
	 * @throws ServletException
	 * @throws IOException
	 */
	@RequestMapping(value = ConstantValues.URL_SAVEEXCPC, method = RequestMethod.POST)
	public ModelAndView saveexcpc(@PathVariable("namespace") String namespace, @PathVariable("docname") String docname, HttpServletRequest request,
			HttpServletResponse response) throws ServletException, IOException
	{
		Map<String, String> hmValues = new HashMap<String, String>();
		ControllerUtil.setRequestParameters(namespace, docname, WikiAction.saveexcpc, request);
		try
		{
			WebApi webApi = (WebApi) RSContext.appContext.getBean(ConstantValues.WIKI_SPRINGID_FOR_WEBAPI);
			hmValues = webApi.saveexcpcAction(request, response);
		}
		catch (Exception e)
		{
			Log.log.error(e.getMessage(), e);
		}

		String forwardUrl = hmValues.get(ConstantValues.WIKI_FORWARD_URL_KEY);
		
		response = HttpUtil.sanitizeResponse(response);
		
		DefaultHTTPUtilities dhttp = new DefaultHTTPUtilities();
		try {
            dhttp.sendRedirect(response, forwardUrl);   
        } catch(AccessControlException  ace) {
            Log.log.error(ace.getMessage(), ace);
        }
		return null;

	}
	
	

	/**
	 * 
     * @deprecated
     * THIS API IS DEPRECATED. NO TESTING FOR THIS API TILL FURTHUR NOTICE.
     * <p>
     * There was only 1 client that was using this api in the older version. In Resolve 5, most of the use case is different and this api may not be valid anymore.
     * <p>
	 * 
	 * @param namespace
	 * @param docname
	 * @param request
	 * @param response
	 * @return
	 * @throws ServletException
	 * @throws IOException
	 */
	@RequestMapping(value = ConstantValues.URL_SAVEFINALC, method = RequestMethod.POST)
	public ModelAndView savefinalc(@PathVariable("namespace") String namespace, @PathVariable("docname") String docname, HttpServletRequest request,
			HttpServletResponse response) throws ServletException, IOException
	{
		Map<String, String> hmValues = new HashMap<String, String>();
		ControllerUtil.setRequestParameters(namespace, docname, WikiAction.savefinalc, request);
		try
		{
			WebApi webApi = (WebApi) RSContext.appContext.getBean(ConstantValues.WIKI_SPRINGID_FOR_WEBAPI);
			hmValues = webApi.savefinalcAction(request, response);
		}
		catch (Exception e)
		{
			Log.log.error(e.getMessage(), e);
		}

		String forwardUrl = hmValues.get(ConstantValues.WIKI_FORWARD_URL_KEY);

		//		response.sendRedirect("http://localhost:8080/resolve/service/wiki/edit/Test/test2");
		response = HttpUtil.sanitizeResponse(response);
		DefaultHTTPUtilities dhttp = new DefaultHTTPUtilities();
        try {
            dhttp.sendRedirect(response, forwardUrl);   
        } catch(AccessControlException  ace) {
            Log.log.error(ace.getMessage(), ace);
        }
		return null;

	}

	/**
	 * 
     * @deprecated
     * THIS API IS DEPRECATED. NO TESTING FOR THIS API TILL FURTHUR NOTICE.
     * <p>
     * There was only 1 client that was using this api in the older version. In Resolve 5, most of the use case is different and this api may not be valid anymore.
     * <p>
	 * This is the SAVE of MODEL XML Code
	 * 
	 * @param namespace
	 * @param docname
	 * @param request
	 * @param response
	 * @return
	 * @throws ServletException
	 * @throws IOException
	 */
	@RequestMapping(value = ConstantValues.URL_SAVEMODELC, method = RequestMethod.POST)
	public ModelAndView savemodelc(@PathVariable("namespace") String namespace, @PathVariable("docname") String docname, HttpServletRequest request,
			HttpServletResponse response) throws ServletException, IOException
	{
		Map<String, String> hmValues = new HashMap<String, String>();
		ControllerUtil.setRequestParameters(namespace, docname, WikiAction.savemodelc, request);
		try
		{
			WebApi webApi = (WebApi) RSContext.appContext.getBean(ConstantValues.WIKI_SPRINGID_FOR_WEBAPI);
			hmValues = webApi.savemodelcAction(request, response);
		}
		catch (Exception e)
		{
			Log.log.error(e.getMessage(), e);
		}

		String forwardUrl = hmValues.get(ConstantValues.WIKI_FORWARD_URL_KEY);

		//		response.sendRedirect("http://localhost:8080/resolve/service/wiki/edit/Test/test2");
		//forwardUrl = StringUtils.removeNewLineAndCarriageReturn(forwardUrl);
		response = HttpUtil.sanitizeResponse(response);
		DefaultHTTPUtilities dhttp = new DefaultHTTPUtilities();
        try {
            dhttp.sendRedirect(response, forwardUrl);   
        } catch(AccessControlException  ace) {
            Log.log.error(ace.getMessage(), ace);
        }
		return null;

	}

	/**
	 * 
     * @deprecated
     * THIS API IS DEPRECATED. NO TESTING FOR THIS API TILL FURTHUR NOTICE.
     * <p>
     * There was only 1 client that was using this api in the older version. In Resolve 5, most of the use case is different and this api may not be valid anymore.
     * <p>
	 * 
	 * @param namespace
	 * @param docname
	 * @param request
	 * @param response
	 * @return
	 * @throws ServletException
	 * @throws IOException
	 */
	@RequestMapping(value = ConstantValues.URL_SAVEANDEXIT, method = RequestMethod.POST)
	public ModelAndView saveAndExit(@PathVariable("namespace") String namespace, @PathVariable("docname") String docname, HttpServletRequest request,
			HttpServletResponse response) throws ServletException, IOException
	{
		Map<String, String> hmValues = new HashMap<String, String>();
		ControllerUtil.setRequestParameters(namespace, docname, WikiAction.saveandexit, request);
		try
		{
			WebApi webApi = (WebApi) RSContext.appContext.getBean(ConstantValues.WIKI_SPRINGID_FOR_WEBAPI);
			hmValues = webApi.saveAction(request, response);
		}
		catch (Exception e)
		{
			Log.log.error(e.getMessage(), e);
		}

		String forwardUrl = hmValues.get(ConstantValues.WIKI_FORWARD_URL_KEY);

		//		response.sendRedirect("http://localhost:8080/resolve/service/wiki/view/Test/test2");
		response = HttpUtil.sanitizeResponse(response);
		DefaultHTTPUtilities dhttp = new DefaultHTTPUtilities();
        try {
            dhttp.sendRedirect(response, forwardUrl);   
        } catch(AccessControlException  ace) {
            Log.log.error(ace.getMessage(), ace);
        }
		return null;

	}

	
	
	/**
	 * 
     * @deprecated
     * THIS API IS DEPRECATED. NO TESTING FOR THIS API TILL FURTHUR NOTICE.
     * <p>
     * There was only 1 client that was using this api in the older version. In Resolve 5, most of the use case is different and this api may not be valid anymore.
     * <p>
	 * 
	 * @param namespace
	 * @param docname
	 * @param request
	 * @param response
	 * @return
	 * @throws ServletException
	 * @throws IOException
	 */
	@RequestMapping(value = ConstantValues.URL_ATTACH, method = RequestMethod.POST)
	public ModelAndView attach(@PathVariable("namespace") String namespace, @PathVariable("docname") String docname, HttpServletRequest request,
			HttpServletResponse response) throws ServletException, IOException
	{
		ControllerUtil.setRequestParameters(namespace, docname, WikiAction.attach, request);
		Map<String, Object> hmValues = new HashMap<String, Object>();
		try
		{
			WebApi webApi = (WebApi) RSContext.appContext.getBean(ConstantValues.WIKI_SPRINGID_FOR_WEBAPI);
			hmValues = webApi.attachAction(request, response);
		}
		catch (Exception e)
		{
			Log.log.error(e.getMessage(), e);
		}

		String forwardUrl = (String) hmValues.get(ConstantValues.WIKI_FORWARD_URL_KEY);
		response = HttpUtil.sanitizeResponse(response);
		DefaultHTTPUtilities dhttp = new DefaultHTTPUtilities();
        try {
            dhttp.sendRedirect(response, forwardUrl);   
        } catch(AccessControlException  ace) {
            Log.log.error(ace.getMessage(), ace);
        }
		return null;

	}//attach

	
	/**
	 * 
     * @deprecated
     * THIS API IS DEPRECATED. NO TESTING FOR THIS API TILL FURTHUR NOTICE.
     * <p>
     * There was only 1 client that was using this api in the older version. In Resolve 5, most of the use case is different and this api may not be valid anymore.
     * <p>
	 * 
	 * @param namespace
	 * @param docname
	 * @param request
	 * @param response
	 * @return
	 * @throws ServletException
	 * @throws IOException
	 */
	@RequestMapping(value = ConstantValues.URL_DELETEATTACH, method = RequestMethod.POST)
	public ModelAndView deleteattach(@PathVariable("namespace") String namespace, @PathVariable("docname") String docname, HttpServletRequest request,
			HttpServletResponse response) throws ServletException, IOException
	{
		ControllerUtil.setRequestParameters(namespace, docname, WikiAction.deleteattach, request);
		Map<String, String> hmValues = new HashMap<String, String>();
		try
		{
			WebApi webApi = (WebApi) RSContext.appContext.getBean(ConstantValues.WIKI_SPRINGID_FOR_WEBAPI);
			hmValues = webApi.deleteattachAction(request, response);
		}
		catch (Exception e)
		{
			Log.log.error(e.getMessage(), e);
		}

		String forwardUrl = hmValues.get(ConstantValues.WIKI_FORWARD_URL_KEY);
		response = HttpUtil.sanitizeResponse(response);
		DefaultHTTPUtilities dhttp = new DefaultHTTPUtilities();
        try {
            dhttp.sendRedirect(response, forwardUrl);   
        } catch(AccessControlException  ace) {
            Log.log.error(ace.getMessage(), ace);
        }
		return null;

	}
	
	/**
	 * 
     * @deprecated
     * THIS API IS DEPRECATED. NO TESTING FOR THIS API TILL FURTHUR NOTICE.
     * <p>
     * There was only 1 client that was using this api in the older version. In Resolve 5, most of the use case is different and this api may not be valid anymore.
     * <p>
	 * This is to delete a wiki doc
	 * 
	 * eg. http://localhost:8080/resolve/service/wiki/delete/Test/test2
	 * 
	 * @param namespace
	 * @param docname
	 * @param request
	 * @param response
	 * @return
	 * @throws ServletException
	 * @throws IOException
	 */
	@RequestMapping(value = ConstantValues.URL_DELETE, method = {RequestMethod.POST, RequestMethod.GET} )
	public ModelAndView delete(@PathVariable("namespace") String namespace, @PathVariable("docname") String docname, HttpServletRequest request,
			HttpServletResponse response) throws ServletException, IOException
	{
	    
	    String username = (String) request.getAttribute(Constants.HTTP_REQUEST_USERNAME);
	    if(StringUtils.isNotBlank(docname) && StringUtils.isNotBlank(namespace))
	    {
	        String fullname = namespace.trim() + "." + docname.trim();
	        WikiDocumentVO doc = ServiceWiki.getWikiDoc(null, fullname, username);
	        if(doc != null)
	        {
	            Set<String> docSysIds = new HashSet<String>();
	            docSysIds.add(doc.getSys_id());
	            
	            try
                {
                    ServiceWiki.updateDeleteForWiki(docSysIds, true, username);
                }
                catch (Exception e)
                {
                    Log.log.error("error in deleting the doc with name " + fullname);
                }
	        }
	    }

		return null;

	}

	/**
	 * 
     * @deprecated
     * THIS API IS DEPRECATED. NO TESTING FOR THIS API TILL FURTHUR NOTICE.
     * <p>
     * There was only 1 client that was using this api in the older version. In Resolve 5, most of the use case is different and this api may not be valid anymore.
     * <p>
	 * 
	 * @param request
	 * @param response
	 * @return
	 * @throws ServletException
	 * @throws IOException
	 */
    @RequestMapping(value="/upsert", method={RequestMethod.GET, RequestMethod.POST})
    public String upsert(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException
    {
        //get all the parameters from the request
        Map<String,String> requestParams = ControllerUtil.getParameterMap(request);
        String userid = (String)request.getAttribute(Constants.HTTP_REQUEST_USERNAME);
        if(StringUtils.isEmpty(userid))
        {
            userid = requestParams.get(Constants.HTTP_REQUEST_INTERNAL_USERNAME);
        }
        String action =  requestParams.get(HibernateConstants.ACTION);//UPDATE or DELETE
        String tableName =  requestParams.get(HibernateConstants.TABLENAME);
        String columnNamesToUpdate =  requestParams.get(HibernateConstants.COLUMNNAMES);
        String id = requestParams.get(HibernateConstants.SYS_ID);

        
        //check if the action and tableName exist
        if(StringUtils.isNotEmpty(action) && StringUtils.isNotEmpty(tableName))
        {
            try
            {
                //to delete make sure id exist
                if(action.trim().equalsIgnoreCase("DELETE") && StringUtils.isNotEmpty(id))
                {
                    //for delete
                    try
                    {
                        ServiceHibernate.deleteDBRow(userid, tableName, id, true);
                    }
                    catch (Exception e)
                    {
                        Log.log.error("error deleting the row for table " + tableName + " with sysId " + id, e);
                    }
//                   (new WorkflowServiceImpl()).deleteRowData(id, tableName);
                }
                else if(action.trim().equalsIgnoreCase("UPDATE"))
                {
                    //get the user timezone
                    String tzId = getTimezoneId(request);
                    
                    //prepare the data object
                    Map<String, Object> rowData = new HashMap<String, Object>();
                    rowData.put(HibernateConstants.SYS_ID, id);
                    
                    String[] columnNamesArr = columnNamesToUpdate.split(",");
                    for(String columnName : columnNamesArr)
                    {
                        String colValue = requestParams.get(columnName);
                        
                        //convert the string to its appropriate Datatype.
                        //string, date, integer/long, double/float, boolean
                        String type = "string";//TODO
                        Object objValue = convertValueToType(colValue, type);
                        rowData.put(columnName, objValue);
                    }
                       
                    //create the params to pass
//                    HashMap<String, String> params = new HashMap<String, String>();
//                    params.put(AdminConstants.MODELCLASS.getTagName(), tableName.trim());
//                    params.put(AdminConstants.TIMEZONE.getTagName(), tzId);
                    
                    //persist it
                    ServiceHibernate.insertOrUpdateCustomTable(userid, tableName, rowData);
//                    (new WorkflowServiceImpl()).persistData(rowData, params, userid);
                    
                }//end of if
                    
            }
            catch(Throwable t)
            {
                Log.log.error("Error: in updating the record from Wiki document", t);
            }
        }//end of if
        
        //forward the request to display the current page
//        request.getRequestDispatcher("/service/wiki/current").forward(request, response);        
//        new ViewController().current(request, response);
        
        return null;
    }//upsert
 
    
    private Object convertValueToType(String colValue, String type) throws Throwable
    {
        Object objValue = null;
        if(StringUtils.isNotEmpty(colValue) && StringUtils.isNotEmpty(type))
        {
            if(type.equalsIgnoreCase(Constants.CUSTOMTABLE_TYPE_STRING)
                || type.equalsIgnoreCase(Constants.CUSTOMTABLE_TYPE_BLOB)
                || type.equalsIgnoreCase(Constants.CUSTOMTABLE_TYPE_CLOB))
            {
                objValue = colValue;
            }
            else if(type.equalsIgnoreCase(Constants.CUSTOMTABLE_TYPE_BOOLEAN))
            {
                objValue = new Boolean(colValue);
            }
            else if(type.equalsIgnoreCase(Constants.CUSTOMTABLE_TYPE_TIMESTAMP))
            {
                objValue = DateFormat.getInstance().parse(colValue);
            }
            else if(type.equalsIgnoreCase(Constants.CUSTOMTABLE_TYPE_INTEGER))
            {
                objValue = new Integer(colValue);
            }
            else if(type.equalsIgnoreCase(Constants.CUSTOMTABLE_TYPE_LONG))
            {
                objValue = new Long(colValue);
            }
            else if(type.equalsIgnoreCase(Constants.CUSTOMTABLE_TYPE_FLOAT))
            {
                objValue = new Float(colValue);
            }
            else if(type.equalsIgnoreCase(Constants.CUSTOMTABLE_TYPE_DOUBLE))
            {
                objValue = new Double(colValue);
            }
            
        }//end if if
        
        return objValue;
    }//convertValueToType
	
    public static String getTimezoneId(HttpServletRequest request)
    {
        Locale clientLocale = request.getLocale();
        Calendar calendar = Calendar.getInstance(clientLocale);
        TimeZone clientTimeZone = calendar.getTimeZone();
        String tzId = clientTimeZone.getID();

        return tzId;
    }// getTimezoneId

	
} // EditController
