/******************************************************************************
* (C) Copyright 2014
*
* Resolve Systems, LLC
*
* All rights reserved
* This software is distributed under license and may not be used, copied,
* modified, or distributed without the express written permission of
* Resolve Systems, LLC.
*
******************************************************************************/

package com.resolve.rsview.controller;

import java.io.IOException;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.stereotype.Controller;
import org.springframework.stereotype.Service;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.resolve.services.ServiceHibernate;
import com.resolve.services.hibernate.vo.ConfigLDAPVO;
import com.resolve.services.vo.QueryDTO;
import com.resolve.services.vo.ResponseDTO;
import com.resolve.util.Constants;
import com.resolve.util.Log;

import net.sf.json.JSONObject;
import net.sf.json.JSONSerializer;
import net.sf.json.JsonConfig;

/**
 * LDAP/AD Configuration used by RSRemote for authentication. This controller provides CRUD operation for that configuration.
 *
 */
@Controller
@Service
public class AjaxConfigLDAP extends GenericController
{

    /**
     * Returns list of ConfigLDAP for the grid based on filters, sorts, pagination
     *
     * @param query : {@link QueryDTO}, represents the query, pagination, sorting, grouping for the UI
     * @param request {@link HttpServletRequest} </code> containing <code>USERNAME</code> of the user who is logged in to Resolve
     * @param response {@link HttpServletResponse}
     * @return {@link ResponseDTO} with success true, data containing list of {@link ConfigLDAPVO} and total set to the number of total records found in DB when operation succseeds,
     *          otherwise Success will be set to false with the message containing error message.
     * @throws ServletException
     * @throws IOException
     */
    @RequestMapping(value = "/configldap/list", method = { RequestMethod.GET, RequestMethod.POST }, headers = "Accept=application/json; charset=utf-8")
    @ResponseBody
    public ResponseDTO list(@ModelAttribute QueryDTO query, HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException
    {
        ResponseDTO result = new ResponseDTO();
        String username = (String) request.getAttribute(Constants.HTTP_REQUEST_USERNAME);
        try
        {
            query.setModelName("ConfigLDAP");
//            query.setSelectColumns("sys_id,UName,UDescription,sysUpdatedOn");

            List<ConfigLDAPVO> data = ServiceHibernate.getConfigLDAP(query, username);
            int total = ServiceHibernate.getTotalHqlCount(query);

            result.setSuccess(true).setRecords(data);
            result.setTotal(total);
        }
        catch (Exception e)
        {
            Log.log.error("Error retrieving ConfigLDAP", e);
            result.setSuccess(false).setMessage("Error retrieving ConfigLDAP. See log for more info.");
        }

        return result;
    }

    /**
     * Gets data for a specific ConfigLDAP to be edited based on sysId
     *
     * @param id : String representing ConfigLDAP sys_id
     * @param request {@link HttpServletRequest} </code> containing <code>USERNAME</code> of the user who is logged in to Resolve
     * @param response {@link HttpServletResponse}
     * @return {@link ResponseDTO} with success true and data containing {@link ConfigLDAPVO} when operation succseeds, 
     *          otherwise Success will be set to false with the message containing error message.
     * @throws ServletException
     * @throws IOException
     */
    @RequestMapping(value = "/configldap/get", method = { RequestMethod.POST }, headers = "Accept=application/json; charset=utf-8")
    @ResponseBody
    public ResponseDTO get(@RequestParam String id, HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException
    {
        ResponseDTO result = new ResponseDTO();
        String username = (String) request.getAttribute(Constants.HTTP_REQUEST_USERNAME);
        try
        {
            ConfigLDAPVO vo = ServiceHibernate.findConfigLDAPById(id, username);
            result.setData(vo);
            result.setSuccess(true);

        }
        catch (Exception e)
        {
            Log.log.error("Error retrieving ConfigLDAP", e);
            result.setSuccess(false).setMessage("Error retrieving ConfigLDAP. See log for more info.");
        }

        return result;
    }

    /**
     * Delete the ConfigLDAP based on array of sysIds
     *
     * @param ids : String array containing ConfigLDAP sysIds to be deleted.
     * @param request {@link HttpServletRequest} </code> containing <code>USERNAME</code> of the user who is logged in to Resolve
     * @param response {@link HttpServletResponse}
     * @return {@link ResponseDTO} with success true when operation succseeds, false otherwise with the message containing error message.
     * @throws ServletException
     * @throws IOException
     */
    @RequestMapping(value = "/configldap/delete", method = { RequestMethod.POST }, headers = "Accept=application/json; charset=utf-8")
    @ResponseBody
    public ResponseDTO delete(@RequestBody String[] ids, HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException
    {
        ResponseDTO result = new ResponseDTO();
        String username = (String) request.getAttribute(Constants.HTTP_REQUEST_USERNAME);

        try
        {
            ServiceHibernate.deleteConfigLDAPByIds(ids, username);
            result.setSuccess(true);
        }
        catch (Exception e)
        {
            Log.log.error("Error deleting ConfigLDAP", e);
            result.setSuccess(false).setMessage("Error deleting ConfigLDAP. See log for more info.");
        }

        return result;
    }

    /**
     * Saves/Update an ConfigLDAP
     *
     * @param property : JSON String representing {@link ConfigLDAPVO} object
     * @param request {@link HttpServletRequest} </code> containing <code>USERNAME</code> of the user who is logged in to Resolve
     * @param response {@link HttpServletResponse}
     * @return {@link ResponseDTO} with success true and data containing saved {@link ConfigLDAPVO} when operation succseeds, 
     *          otherwise Success will be set to false with the message containing error message.
     * @throws ServletException
     * @throws IOException
     */
    @RequestMapping(value = "/configldap/save", method = { RequestMethod.POST }, headers = "Accept=application/json; charset=utf-8")
    @ResponseBody
    public ResponseDTO save(@RequestBody JSONObject jsonProperty, HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException
    {
      //using jackson to deserialize the json
//        String json = jsonProperty.toString();
//        ConfigLDAPVO entity = new ObjectMapper().readValue(json, ConfigLDAPVO.class);

        //convert json to VO - spring does not work as the VO are not 100% java bean
        JsonConfig jsonConfig = new JsonConfig();
        jsonConfig.setRootClass(ConfigLDAPVO.class);
        ConfigLDAPVO entity = (ConfigLDAPVO) JSONSerializer.toJava(jsonProperty, jsonConfig);

        ResponseDTO result = new ResponseDTO();
        String username = (String) request.getAttribute(Constants.HTTP_REQUEST_USERNAME);

        try
        {
            ConfigLDAPVO vo = ServiceHibernate.saveConfigLDAP(entity, username);
            result.setSuccess(true).setData(vo);
        }
        catch (Exception e)
        {
            Log.log.error("Error saving the ConfigLDAP", e);
            result.setSuccess(false).setMessage("Error saving the ConfigLDAP.. See log for more info.");
        }

        return result;
    }

    /**
     * Returns a list of the LDAP gateway (Queue) that is configured to be used for Resolve authentication. 
     * <p>
     * @param request {@link HttpServletRequest} </code> containing <code>USERNAME</code> of the user who is logged in to Resolve
     * @param response {@link HttpServletResponse}
     * @return {@link ResponseDTO} with success true and data with list of {@link String} containing Queue names when operation succseeds, 
     *          otherwise Success will be set to false with the message containing error message.
     * @throws ServletException
     * @throws IOException
     */
    @RequestMapping(value = "/configldap/listAuthGateways", method = { RequestMethod.GET }, headers = "Accept=application/json; charset=utf-8")
    @ResponseBody
    public ResponseDTO listAuthGateways(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException
    {
        ResponseDTO result = new ResponseDTO();
        String username = (String) request.getAttribute(Constants.HTTP_REQUEST_USERNAME);

        try
        {
            List<String> queueNames = ServiceHibernate.getUserAuthenticationGateway("ldap");
            result.setRecords(queueNames);
        }
        catch (Exception e)
        {
            Log.log.error("Error saving the ConfigLDAP", e);
            result.setSuccess(false).setMessage("Error getting gateways. See log for more info.");
        }

        return result;
    }
}
