package com.resolve.rsview.sso.util;

public enum SAMLEnum
{
    ADFS_ENDPOINT_KEY("sso.adfs.endpoint"),
    ADFS_ENABLED_KEY("sso.adfs.enabled"),
    QUERY_STRING("QUERY_STRING"),
    REQUEST_PARAMS("REQUEST_PARAMS"),
    REQUEST_URI("REQUEST_URI"),
    AUTHENTICATED("AUTHENTICATED"),
    AUTHORIZED("AUTHORIZED"),
    PARAMS("PARAMS"),
    USERNAME("USERNAME"),
    OPERATION("OPERATION"),
    ADD("ADD"),
    REMOVE("REMOVE"),
    SESSION_MAP("SESSION_MAP"),
    CREATETIME ("CREATETIME"),
    LANDING_URI ("/resolve/jsp/rsclient.jsp"),
    AUTH_HANDLER_ENDPOINT ("/resolve/saml/auth/"),
    ERROR_JSP("/resolve/jsp/ssoAdfsUnauthorized.jsp"),
    UNAUTHORIZED_MESSAGE_KEY("sso.adfs.unauthorized.message"),
    UNAUTHORIZED_MESSAGE("unauthorizedMsg");
    
    private final String value;
        
    private SAMLEnum(String value) {
        this.value = value;
    }
    
    public String getValue() {
        return value;
    }
}
