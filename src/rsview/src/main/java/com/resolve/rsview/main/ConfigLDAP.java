/******************************************************************************
* (C) Copyright 2014
*
* Resolve Systems, LLC
*
* All rights reserved
* This software is distributed under license and may not be used, copied,
* modified, or distributed without the express written permission of
* Resolve Systems, LLC.
*
******************************************************************************/

package com.resolve.rsview.main;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import com.novell.ldap.LDAPConnection;

import com.resolve.util.Log;
import com.resolve.util.StringUtils;
import com.resolve.util.XDoc;
import com.resolve.util.ConfigMap;

public class ConfigLDAP extends ConfigMap
{
    private static final long serialVersionUID = 2173484406771083592L;
    
    boolean active = false;
    String mode = "BIND";
    int version = LDAPConnection.LDAP_V3;
    String ipaddress = "";
    int port = LDAPConnection.DEFAULT_PORT;
    boolean ssl = false;
    String cryptType = "CLEAR";
    String cryptPrefix = "DEFAULT";
    String bindDN = "o=resolve,c=us";
    String bindP_assword = "secret";
    List<String> baseDNList = new ArrayList<String>();
    String defaultDomain    = "";
    String uidAttribute = "uid";
    String p_asswordAttribute = "userPassword";
    boolean sync = false;
    boolean fallback = true;
    boolean grouprequired   = false;
        
    public ConfigLDAP(XDoc config)
    {
        super(config);
        
        define("active", BOOLEAN, "./AUTH/LDAP/@ACTIVE");
        define("mode", STRING, "./AUTH/LDAP/@MODE");
        define("version", INTEGER, "./AUTH/LDAP/@VERSION");
        define("ipaddress", STRING, "./AUTH/LDAP/@IPADDRESS");
        define("port", INTEGER, "./AUTH/LDAP/@PORT");
        define("ssl", BOOLEAN, "./AUTH/LDAP/@SSL");
        define("cryptType", STRING, "./AUTH/LDAP/@CRYPT_TYPE");
        define("cryptPrefix", STRING, "./AUTH/LDAP/@CRYPT_PREFIX");
        define("bindDN", STRING, "./AUTH/LDAP/@BIND_DN");
        define("bindP_assword", ENCRYPT, "./AUTH/LDAP/@BIND_PASSWORD");
        define("defaultDomain", STRING, "./AUTH/LDAP/@DEFAULT_DOMAIN"); 
        define("uidAttribute", STRING, "./AUTH/LDAP/@UID_ATTRIBUTE");
        define("p_asswordAttribute", STRING, "./AUTH/LDAP/@PASSWORD_ATTRIBUTE");
        define("sync", BOOLEAN, "./AUTH/LDAP/@SYNC");
        define("fallback", BOOLEAN, "./AUTH/LDAP/@FALLBACK");
        define("grouprequired", BOOLEAN, "./AUTH/LDAP/@GROUPREQUIRED");
    } // ConfigId
    
    @SuppressWarnings("rawtypes")
    public void load() throws Exception
    {
        loadAttributes();
        
        List baseDNs = xdoc.getListMapValue("./AUTH/LDAP/BASEDN");
        
        if (baseDNs.size() > 0)
        {
            for (Iterator i = baseDNs.iterator(); i.hasNext();)
            {
                Map values = (Map) i.next();

                String dn = (String) values.get("DN");
                
                if (!StringUtils.isEmpty(dn))
                {
                    if (!baseDNList.add(dn))
                    {
                        Log.log.warn("Duplicate baseDN: " + dn);
                    }
                    else
                    {
                        Log.log.info("Added a new baseDN: " + dn);
                    }
                }
            }
        }
    } // load

    @SuppressWarnings({ "rawtypes", "unchecked" })
    public void save()
    {
        List<HashMap> baseDNs = new ArrayList<HashMap>();
        if (baseDNList != null)
        {
            for (String dn : baseDNList)
            {
                HashMap entry = new HashMap();
                entry.put("DN", dn);
                baseDNs.add(entry);
            }
            xdoc.setListMapValue("./AUTH/LDAP/BASEDN", baseDNs);
        } 
        
        saveAttributes();
    } // save

    public boolean isActive()
    {
        return active;
    }

    public void setActive(boolean active)
    {
        this.active = active;
    }

    public String getMode()
    {
        return mode;
    }

    public void setMode(String mode)
    {
        this.mode = mode;
    }

    public int getVersion()
    {
        return version;
    }

    public void setVersion(int version)
    {
        this.version = version;
    }

    public String getIpaddress()
    {
        return ipaddress;
    }

    public void setIpaddress(String ipaddress)
    {
        this.ipaddress = ipaddress;
    }

    public int getPort()
    {
        return port;
    }

    public void setPort(int port)
    {
        this.port = port;
    }

    public boolean isSsl()
    {
        return ssl;
    }

    public void setSsl(boolean ssl)
    {
        this.ssl = ssl;
    }

    public String getCryptType()
    {
        return cryptType;
    }

    public void setCryptType(String cryptType)
    {
        this.cryptType = cryptType;
    }

    public String getCryptPrefix()
    {
        return cryptPrefix;
    }

    public void setCryptPrefix(String cryptPrefix)
    {
        this.cryptPrefix = cryptPrefix;
    }

    public String getBindDN()
    {
        return bindDN;
    }

    public void setBindDN(String bindDN)
    {
        this.bindDN = bindDN;
    }

    public String getBindP_assword()
    {
        return bindP_assword;
    }

    public void setBindP_assword(String bindPassword)
    {
        this.bindP_assword = bindPassword;
    }

    public List<String> getBaseDNList()
    {
        return baseDNList;
    }

    public void setBaseDNList(List<String> baseDN)
    {
        this.baseDNList = baseDN;
    }
    
    public String getDefaultDomain()
    {
        return defaultDomain;
    }

    public void setDefaultDomain(String defaultDomain)
    {
        this.defaultDomain = defaultDomain;
    }
    
    public String getUidAttribute()
    {
        return uidAttribute;
    }

    public void setUidAttribute(String uidAttribute)
    {
        this.uidAttribute = uidAttribute;
    }

    public String getP_asswordAttribute()
    {
        return p_asswordAttribute;
    }

    public void setP_asswordAttribute(String passwordAttribute)
    {
        this.p_asswordAttribute = passwordAttribute;
    }
    
    public boolean isSync()
    {
        return sync;
    }

    public void setSync(boolean sync)
    {
        this.sync = sync;
    }

    public boolean isFallback()
    {
        return fallback;
    }

    public void setFallback(boolean fallback)
    {
        this.fallback = fallback;
    }    

    public boolean isGrouprequired()
    {
        return grouprequired;
    }

    public void setGrouprequired(boolean grouprequired)
    {
        this.grouprequired = grouprequired;
    }

} // ConfigId
