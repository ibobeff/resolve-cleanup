/******************************************************************************
* (C) Copyright 2014
*
* Resolve Systems, LLC
*
* All rights reserved
* This software is distributed under license and may not be used, copied,
* modified, or distributed without the express written permission of
* Resolve Systems, LLC.
*
******************************************************************************/

package com.resolve.rsview.controller;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.codehaus.jackson.map.ObjectMapper;
import org.springframework.stereotype.Controller;
import org.springframework.stereotype.Service;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.resolve.services.ServiceHibernate;
import com.resolve.services.hibernate.util.ExecuteRunbook;
import com.resolve.services.hibernate.vo.ResolveCronVO;
import com.resolve.services.util.ExecuteUtil;
import com.resolve.services.vo.QueryDTO;
import com.resolve.services.vo.ResponseDTO;
import com.resolve.util.Constants;
import com.resolve.util.Log;
import com.resolve.util.StringUtils;

import net.sf.json.JSONObject;

@Controller
@Service
public class AjaxJobScheduler extends GenericController
{
    /**
     * Returns list of Resolve Job Scheduler for the grid based on filters, sorts, pagination
     * 
     * @param query
     * @param request
     * @param response
     * @return
     * @throws ServletException
     * @throws IOException
     */
    @RequestMapping(value = "/jobscheduler/list", method = { RequestMethod.GET, RequestMethod.POST }, headers = "Accept=application/json; charset=utf-8")
    @ResponseBody
    public ResponseDTO list(@ModelAttribute QueryDTO query, HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException
    {
        ResponseDTO result = new ResponseDTO();
        String username = (String) request.getAttribute(Constants.HTTP_REQUEST_USERNAME);
        try
        {

            query.setModelName("ResolveCron");

            List<ResolveCronVO> data = ServiceHibernate.getResolveCronJobs(query, username);
            int total = ServiceHibernate.getTotalHqlCount(query);

            result.setSuccess(true).setRecords(data);
            result.setTotal(total);
        }
        catch (Exception e)
        {
            Log.log.error("Error retrieving ResolveCron", e);
            result.setSuccess(false).setMessage("Error retrieving ResolveCron");
        }

        return result;
    }

    /**
     * Gets data for a specific ResolveCron based on sysId
     * 
     * @param id
     * @param request
     * @param response
     * @return
     * @throws ServletException
     * @throws IOException
     */
    @RequestMapping(value = "/jobscheduler/get", method = { RequestMethod.POST }, headers = "Accept=application/json; charset=utf-8")
    @ResponseBody
    public ResponseDTO get(@RequestParam String id, HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException
    {
        ResponseDTO result = new ResponseDTO();
        String username = (String) request.getAttribute(Constants.HTTP_REQUEST_USERNAME);

        try
        {
            ResolveCronVO vo = ServiceHibernate.findCronJob(id, null, username);
            result.setData(vo);
            result.setSuccess(true);

        }
        catch (Exception e)
        {
            Log.log.error("Error retrieving job scheduler", e);
            result.setSuccess(false).setMessage("Error retrieving job scheduler");
        }

        return result;
    }

    /**
     *  Delete the job scheduler based on array of sysIds
     * 
     * @param ids
     * @param request
     * @param response
     * @return
     * @throws ServletException
     * @throws IOException
     */
    @RequestMapping(value = "/jobscheduler/delete", method = { RequestMethod.POST }, headers = "Accept=application/json; charset=utf-8")
    @ResponseBody
    public ResponseDTO delete(@RequestParam String[] ids, @ModelAttribute QueryDTO query, HttpServletRequest request) throws ServletException, IOException
    {
        ResponseDTO result = new ResponseDTO();
        String username = (String) request.getAttribute(Constants.HTTP_REQUEST_USERNAME);

        try
        {
            ServiceHibernate.deleteCronJobsById(ids, query, username);
            result.setSuccess(true).setMessage("Records deleted successfully.");
        }
        catch (Exception e)
        {
            Log.log.error("Error deleting job scheduler", e);
            result.setSuccess(false).setMessage("Error deleting job scheduler");
        }

        return result;
    }

    /**
     * save the job scheduler record
     * 
     * @param jsonProperty
     * @param request
     * @return
     * @throws ServletException
     * @throws IOException
     */
    @RequestMapping(value = "/jobscheduler/save", method = { RequestMethod.POST, RequestMethod.GET })
    @ResponseBody
    public ResponseDTO save(@RequestBody JSONObject jsonProperty, HttpServletRequest request) throws ServletException, IOException
    {
        //using jackson to deserialize the json
        String json = jsonProperty.toString();
        ResolveCronVO entity = new ObjectMapper().readValue(json, ResolveCronVO.class);

        //convert json to VO - spring does not work as the VO are not 100% java bean
        //        JsonConfig jsonConfig = new JsonConfig();
        //        jsonConfig.setRootClass(ResolveCronVO.class);
        //        jsonConfig.setPropertySetStrategy(new PropertyStrategyWrapper(PropertySetStrategy.DEFAULT));
        //        ResolveCronVO entity = (ResolveCronVO) JSONSerializer.toJava(jsonProperty, jsonConfig);

        ResponseDTO result = new ResponseDTO();
        String username = (String) request.getAttribute(Constants.HTTP_REQUEST_USERNAME);

        try
        {
            ResolveCronVO vo = ServiceHibernate.saveResolveCron(entity, username);
            result.setSuccess(true).setData(vo);
        }
        catch (Exception e)
        {
            Log.log.error("Error saving the job scheduler", e);
            result.setSuccess(false).setMessage("Error saving the job scheduler");
        }

        return result;
    }

    @RequestMapping(value = "/jobscheduler/activateDeactivate", method = { RequestMethod.POST }, headers = "Accept=application/json; charset=utf-8")
    @ResponseBody
    public ResponseDTO activateDeactivate(@RequestParam String[] ids, @RequestParam boolean activateJob, HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException
    {
        ResponseDTO result = new ResponseDTO();
        String username = (String) request.getAttribute(Constants.HTTP_REQUEST_USERNAME);

        try
        {
            ServiceHibernate.activateDeactivateCronJob(ids, activateJob, username);
            result.setSuccess(true);

        }
        catch (Exception e)
        {
            Log.log.error("Error in activating the job scheduler", e);
            result.setSuccess(false).setMessage("Error in activating the job scheduler");
        }

        return result;
    }

    /**
     *  Execute the job scheduler based on array of ids now
     * 
     * @param ids
     * @param request
     * @param response
     * @return
     * @throws ServletException
     * @throws IOException
     */
    @SuppressWarnings("unchecked")
    @RequestMapping(value = "/jobscheduler/execute", method = { RequestMethod.POST }, headers = "Accept=application/json; charset=utf-8")
    @ResponseBody
    public ResponseDTO execute(@RequestParam String[] ids, @ModelAttribute QueryDTO query, HttpServletRequest request) throws ServletException, IOException
    {
        ResponseDTO result = new ResponseDTO();
        String username = (String) request.getAttribute(Constants.HTTP_REQUEST_USERNAME);

        try
        {
            List<ResolveCronVO> vos = new ArrayList<ResolveCronVO>();
            if (ids.length > 0)
            {
                for (String id : ids)
                {
                    ResolveCronVO vo = ServiceHibernate.findCronJob(id, null, username);
                    if (vo != null)
                    {
                        vos.add(vo);
                    }
                }
            }
            else
            {
                vos = ServiceHibernate.getResolveCronJobs(query, username);
            }

            for (ResolveCronVO vo : vos)
            {
                if (vo != null)
                {
                    //Map<String, Object> params = StringUtils.stringToMap(vo.getUParams(), "=", "&");
                    Map params = StringUtils.urlToMap(vo.getUParams(), false);
                    
                    //if this is a job scheduler command and not a runbook 
                    if(vo.getURunbook().indexOf('#') > -1)
                    {
                        ExecuteUtil.sendESBRequest(vo.getURunbook(), params);
                    }
                    else //this is a runbook
                    {
                        if (!params.containsKey(Constants.EXECUTE_WIKI)) params.put(Constants.EXECUTE_WIKI, vo.getURunbook());
                        if( !params.containsKey(Constants.EXECUTE_USERID)) params.put(Constants.EXECUTE_USERID, username);
                        if( !params.containsKey(Constants.EXECUTE_PROBLEMID))params.put(Constants.EXECUTE_PROBLEMID, "NEW");

                        String message = ExecuteRunbook.start(params);
                        
                        if (StringUtils.isNotBlank(message))
                        {
                            if (message.toUpperCase().contains("ERROR") || message.toUpperCase().contains("FAILURE"))
                            {
                                throw new Exception ("Failed to schedule runbook " + vo.getURunbook() + " execution. " + message);
                            }
                        }
                    }
                }
            }

            result.setSuccess(true).setMessage("Records executed successfully.");
        }
        catch (Exception e)
        {
            Log.log.error("Error executing job schedule", e);
            result.setSuccess(false).setMessage("Error executing job schedule");
        }

        return result;
    }

}
