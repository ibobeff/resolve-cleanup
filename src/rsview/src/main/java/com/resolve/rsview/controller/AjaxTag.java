/******************************************************************************
* (C) Copyright 2014
*
* Resolve Systems, LLC
*
* All rights reserved
* This software is distributed under license and may not be used, copied,
* modified, or distributed without the express written permission of
* Resolve Systems, LLC.
*
******************************************************************************/

package com.resolve.rsview.controller;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.stereotype.Controller;
import org.springframework.stereotype.Service;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.resolve.search.model.Tag;
import com.resolve.services.ServiceTag;
import com.resolve.services.hibernate.vo.ResolveTagVO;
import com.resolve.services.vo.QueryDTO;
import com.resolve.services.vo.QueryFilter;
import com.resolve.services.vo.QuerySort;
import com.resolve.services.vo.ResponseDTO;
import com.resolve.util.Constants;
import com.resolve.util.Log;

/**
 * This controller serves the request(CRUD operations) for Resolve Tags.
 * <br>
 * In Resolve, components like WikiDocs, ActionTasks, Catalogs can be taged by any existing tag.
 * <br>
 * Components can also be searched by given tag.
 * <p>
 * URL: <code>http://[IP]:[PORT]/resolve/jsp/rsclient.jsp#RS.tag.Tags/</code>
 *
 * @author jeet.marwah
 *
 */
@Controller
@Service
public class AjaxTag extends GenericController
{
	private static final String ERROR_DELETING_PROPERTIES = "Error deleting Properties";
	private static final String ERROR_GETTING_TAG = "Error getting tag";
	private static final String ERROR_RETRIEVING_TAGS = "Error retrieving tags";
    private static final String ERROR_SAVING_TAG = "Error saving Tag";

	/**
     * List Tags base on query, filter and pagination information.
     * 
     * @param query : QueryDTO - provides paging, sorting, filtering (See {@link QueryDTO} for more information)
     * @param request : {@link HttpServletRequest}
     * @param response : {@link HttpServletResponse}
     * @return
     * @throws ServletException
     * @throws IOException
     */
    @RequestMapping(value = "/tag/listTags", method = { RequestMethod.GET }, headers = "Accept=application/json; charset=utf-8")
    @ResponseBody
    public ResponseDTO listTags(@ModelAttribute QueryDTO query, HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException
    {
        ResponseDTO result = null;

        try
        {
            String username = (String) request.getAttribute(Constants.HTTP_REQUEST_USERNAME);
            
            //NOTE: hack till the time we use Tag
            List<QuerySort> sorts = query.getSortItems();
            if(sorts != null && sorts.size() > 0)
            {
                for(QuerySort sort : sorts)
                {
                    String property = sort.getProperty();
                    if("name".equalsIgnoreCase(property))
                    {
                        sort.setProperty("UName");
                    }
                }
            }
            
            List<QueryFilter> filters = query.getFilters();
            if(filters != null && filters.size() > 0)
            {
                for(QueryFilter filter : filters)
                {
                    String field = filter.getField();
                    if("name".equalsIgnoreCase(field))
                    {
                        filter.setField("UName");
                    }
                    else if("description".equalsIgnoreCase(field))
                    {
                        filter.setField("UDescription");
                    }
                }
            }
            
            
            ////////////////////////////////////////////
            result = ServiceTag.listTags(query, username);
            
            List<Tag> tags = new ArrayList<Tag>();
            List<ResolveTagVO> vos = (List<ResolveTagVO>)result.getRecords();
            for(ResolveTagVO vo : vos)
            {
                tags.add(com.resolve.rsview.util.TagUtil.convertResolveTagToTag(vo));
            }
            
            result.setRecords(tags);
            result.setSuccess(true);
        }
        catch (Exception e)
        {
            Log.log.error(ERROR_RETRIEVING_TAGS, e);
            result.setSuccess(false).setMessage(ERROR_RETRIEVING_TAGS);
        }

        return result;
    }

    /**
     * Gets data for a specific Tag to be edited based on sysId
     *
     * @param id : String tagId to be searched for.
     * @param request : {@link HttpServletRequest}
     * @param response : {@link HttpServletResponse}
     * @return
     * @throws ServletException
     * @throws IOException
     */
    @RequestMapping(value = "/tag/getTag", method = { RequestMethod.POST }, headers = "Accept=application/json; charset=utf-8")
    @ResponseBody
    public ResponseDTO getTag(@RequestParam String id, HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException
    {
        ResponseDTO result = new ResponseDTO();
        String username = (String) request.getAttribute(Constants.HTTP_REQUEST_USERNAME);
        
        try
        {
           ResolveTagVO tagVO = ServiceTag.getTag(id, null, username);
           Tag tag = com.resolve.rsview.util.TagUtil.convertResolveTagToTag(tagVO);
           
           result.setData(tag).setSuccess(true);
        }
        catch (Exception e)
        {
            Log.log.error(ERROR_GETTING_TAG, e);
            result.setSuccess(false).setMessage(ERROR_GETTING_TAG);
        }

        return result;
    }

    /**
     * Delete the Tag based on array of sysIds
     *
     * @param ids : List of String tagIds to be deleted.
     * @param request : {@link HttpServletRequest}
     * @param response : {@link HttpServletResponse}
     * @return
     * @throws ServletException
     * @throws IOException
     */
    @RequestMapping(value = "/tag/deleteTags", method = { RequestMethod.POST }, headers = "Accept=application/json; charset=utf-8")
    @ResponseBody
    public ResponseDTO deleteTags(@RequestParam List<String> ids, HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException
    {
        ResponseDTO result = new ResponseDTO();
        String username = (String) request.getAttribute(Constants.HTTP_REQUEST_USERNAME);
        try
        {
            ServiceTag.deleteResolveTagsByIds(ids, username);
            result.setSuccess(true);
        }
        catch (Exception e)
        {
            Log.log.error(ERROR_DELETING_PROPERTIES, e);
            result.setSuccess(false).setMessage(ERROR_DELETING_PROPERTIES);
        }

        return result;
    }


    /**
     * Saves/Update a Tag
     * <p>
     * First check is made whether the tag already present in the system. If yes, it will be an update operation. Insert otherwise.
     * 
     * @param tag : {@link Tag} to be saved or updated.
     * @param request : {@link HttpServletRequest}
     * @param response : {@link HttpServletResponse}
     * @return
     * @throws ServletException
     * @throws IOException
     */
    @RequestMapping(value = "/tag/saveTag", method = { RequestMethod.POST }, headers = "Accept=application/json; charset=utf-8")
    @ResponseBody
    public ResponseDTO saveTag(@RequestBody Tag tag, HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException
    {
        ResponseDTO result = new ResponseDTO();
        String username = (String) request.getAttribute(Constants.HTTP_REQUEST_USERNAME);
        
        try
        {
            ResolveTagVO rsTag = com.resolve.rsview.util.TagUtil.convertTagToResolveTag(tag);
            rsTag = ServiceTag.saveTag(rsTag, username);
            tag = com.resolve.rsview.util.TagUtil.convertResolveTagToTag(rsTag);
            
            result.setSuccess(true).setData(tag);
            
        }
        catch (Exception e)
        {
            Log.log.error(ERROR_SAVING_TAG, e);
            result.setSuccess(false).setMessage(ERROR_SAVING_TAG);
        }

        return result;
    }
}
