/******************************************************************************
* (C) Copyright 2014
*
* Resolve Systems, LLC
*
* All rights reserved
* This software is distributed under license and may not be used, copied,
* modified, or distributed without the express written permission of
* Resolve Systems, LLC.
*
******************************************************************************/

package com.resolve.rsview.controller;

import java.io.IOException;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.stereotype.Controller;
import org.springframework.stereotype.Service;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.resolve.services.ServiceHibernate;
import com.resolve.services.hibernate.vo.ResolveBusinessRuleVO;
import com.resolve.services.vo.QueryDTO;
import com.resolve.services.vo.ResponseDTO;
import com.resolve.util.Constants;
import com.resolve.util.Log;

import net.sf.json.JSONObject;
import net.sf.json.JSONSerializer;
import net.sf.json.JsonConfig;

@Controller
@Service
public class AjaxBusinessRules extends GenericController
{
    /**
     * Returns list of Bussiness Rules for the grid based on filters, sorts, pagination
     * 
     * @param query
     * @param request
     * @param response
     * @return
     * @throws ServletException
     * @throws IOException
     */
    @RequestMapping(value = "/businessrules/list", method = { RequestMethod.GET, RequestMethod.POST }, headers = "Accept=application/json; charset=utf-8")
    @ResponseBody
    public ResponseDTO list(@ModelAttribute QueryDTO query, HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException
    {
        ResponseDTO result = new ResponseDTO();
        String username = (String) request.getAttribute(Constants.HTTP_REQUEST_USERNAME);
        
        try
        {   
            query.setModelName("ResolveBusinessRule");
            
            List<ResolveBusinessRuleVO> data = ServiceHibernate.getResolveBusinessRules(query, username);
            int total = ServiceHibernate.getTotalHqlCount(query);
            
            result.setSuccess(true).setRecords(data);
            result.setTotal(total);
        }
        catch (Exception e)
        {
            Log.log.error("Error retrieving Bussiness Rules", e);
            result.setSuccess(false).setMessage("Error getting business rule list. See log for more info.");
        }

        return result;
    }
    
    /**
     * Gets data for a specific Business Rule to be edited based on sysId
     * 
     * @param id
     * @param request
     * @param response
     * @return
     * @throws ServletException
     * @throws IOException
     */
    @RequestMapping(value = "/businessrules/get", method = { RequestMethod.POST }, headers = "Accept=application/json; charset=utf-8")
    @ResponseBody
    public ResponseDTO get(@RequestParam String id, HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException
    {
        ResponseDTO result = new ResponseDTO();
        String username = (String) request.getAttribute(Constants.HTTP_REQUEST_USERNAME);
        
        try
        {
            ResolveBusinessRuleVO vo = ServiceHibernate.findBusinessRuleByIdOrName(id, null, username);
            result.setData(vo);
            result.setSuccess(true);
            
        }
        catch (Exception e)
        {
            Log.log.error("Error retrieving Business Rule ", e);
            result.setSuccess(false).setMessage("Error getting business rule. See log for more info.");
        }

        return result;
    }
    
    
    /**
     * Saves/Update an Business Rule
     * 
     * @param property
     * @param request
     * @param response
     * @return
     * @throws ServletException
     * @throws IOException
     */
    @RequestMapping(value = "/businessrules/save", method = { RequestMethod.GET, RequestMethod.POST }, headers = "Accept=application/json; charset=utf-8")
    @ResponseBody
    public ResponseDTO save(@RequestBody JSONObject jsonProperty,  HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException
    {
      //using jackson to deserialize the json
//        String json = jsonProperty.toString();
//        ResolveBusinessRuleVO entity = new ObjectMapper().readValue(json, ResolveBusinessRuleVO.class);
        
        //convert json to VO - spring does not work as the VO are not 100% java bean
        JsonConfig jsonConfig = new JsonConfig();
        jsonConfig.setRootClass(ResolveBusinessRuleVO.class);
        ResolveBusinessRuleVO entity = (ResolveBusinessRuleVO) JSONSerializer.toJava(jsonProperty, jsonConfig);
        
        ResponseDTO result = new ResponseDTO();
        String username = (String) request.getAttribute(Constants.HTTP_REQUEST_USERNAME);
        
        try
        {
            ResolveBusinessRuleVO vo = ServiceHibernate.saveBusinessRule(entity, username);
            result.setSuccess(true).setData(vo);
        }
        catch (Exception e)
        {
            Log.log.error("Error saving the Bussiness Rules", e);
            result.setSuccess(false).setMessage("Error saving business rule. See log for more info.");
        }

        return result;
    }
    
    /**
     * Delete the Business Rule based on array of sysIds
     * 
     * @param ids
     * @param request
     * @param response
     * @return
     * @throws ServletException
     * @throws IOException
     */
    @RequestMapping(value = "/businessrules/delete", method = { RequestMethod.POST }, headers = "Accept=application/json; charset=utf-8")
    @ResponseBody
    public ResponseDTO delete(@RequestBody String[] ids, HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException
    {
        ResponseDTO result = new ResponseDTO();
        String username = (String) request.getAttribute(Constants.HTTP_REQUEST_USERNAME);
        
        try
        {
            ServiceHibernate.deleteBusinessRulesByIds(ids, username);
            result.setSuccess(true);
        }
        catch (Exception e)
        {
            Log.log.error("Error deleting Bussiness Rules", e);
            result.setSuccess(false).setMessage("Error deleting business rule. See log for more info.");
        }

        return result;
    }
}


