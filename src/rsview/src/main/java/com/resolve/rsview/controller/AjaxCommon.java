/******************************************************************************
* (C) Copyright 2014
*
* Resolve Systems, LLC
*
* All rights reserved
* This software is distributed under license and may not be used, copied,
* modified, or distributed without the express written permission of
* Resolve Systems, LLC.
*
******************************************************************************/

package com.resolve.rsview.controller;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.stereotype.Controller;
import org.springframework.stereotype.Service;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.resolve.services.ServiceHibernate;
import com.resolve.services.ServiceWiki;
import com.resolve.services.hibernate.util.vo.ComboboxModelDTO;
import com.resolve.services.hibernate.vo.OrganizationVO;
import com.resolve.services.hibernate.vo.ResolveControllersVO;
import com.resolve.services.hibernate.vo.RolesVO;
import com.resolve.services.hibernate.vo.SysAppApplicationVO;
import com.resolve.services.hibernate.vo.UsersVO;
import com.resolve.services.hibernate.vo.WikiDocumentVO;
import com.resolve.services.vo.QueryDTO;
import com.resolve.services.vo.ResponseDTO;
import com.resolve.services.vo.social.NodeType;
import com.resolve.util.Constants;
import com.resolve.util.Log;
import com.resolve.util.StringUtils;

/**
 * THis is a common ajax service which can be shared between components. For eg, get me list of docs or actiontask which can be used in popups
 * in social or any other place 
 * 
 * @author jeet.marwah
 *
 */
@Controller
@Service
public class AjaxCommon extends GenericController
{
    /**
     * Returns list of Organization to be able to use in a combo box. 
     * 
     * @param query
     * @param request
     * @param response
     * @return
     * @throws ServletException
     * @throws IOException
     */
    @RequestMapping(value = "/common/organizationList", method = { RequestMethod.GET, RequestMethod.POST }, headers = "Accept=application/json; charset=utf-8")
    @ResponseBody
    public ResponseDTO organizationList(@ModelAttribute QueryDTO query, HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException
    {
        ResponseDTO result = new ResponseDTO();

        try
        {
            query.setModelName("Organization");
            query.setSelectColumns("sys_id,UOrganizationName,sysCreatedOn,sysCreatedBy,sysUpdatedOn,sysUpdatedBy");
            
            //get the VOs
            List<OrganizationVO> data = ServiceHibernate.getOrganization(query);
            int total = ServiceHibernate.getTotalHqlCount(query);
            
            //prepare the data
//            List<ComboboxModelDTO> data = new ArrayList<ComboboxModelDTO>();
//            if(list != null)
//            {
//                for(OrganizationVO vo : list)
//                {
//                    data.add(new ComboboxModelDTO(vo.getUOrganizationName(), vo.getSys_id()));
//                }
//            }
            
            //prepare the response
            result.setSuccess(true).setRecords(data);
            result.setTotal(total);
        }
        catch (Exception e)
        {
            Log.log.error("Error retrieving organization list", e);
            result.setSuccess(false).setMessage("Error retrieving organization list. See log for more info.");
        }

        return result;
    }
    
    /**
     * Returns list of table names to be able to use in a combo box. 
     * 
     * @param query
     * @param request
     * @param response
     * @return
     * @throws ServletException
     * @throws IOException
     */
    @RequestMapping(value = "/common/resolveTables", method = { RequestMethod.GET, RequestMethod.POST }, headers = "Accept=application/json; charset=utf-8")
    @ResponseBody
    public ResponseDTO resolveTables(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException
    {
        ResponseDTO result = new ResponseDTO();

        try
        {
           
            Set<String> list = ServiceHibernate.getListOfResolveTables();
            
            //prepare the data
            List<ComboboxModelDTO> data = new ArrayList<ComboboxModelDTO>();
            if(list != null)
            {
                for(String tableName : list)
                {
                    data.add(new ComboboxModelDTO(tableName, tableName));
                }
            }
            
            //prepare the response
            result.setSuccess(true).setRecords(data);
        }
        catch (Exception e)
        {
            Log.log.error("Error retrieving table names list", e);
            result.setSuccess(false).setMessage("Error adding table to resolve. See log for more info.");
        }

        return result;
    }
    
    
    @RequestMapping(value = "/common/user/list", method = { RequestMethod.POST, RequestMethod.GET }, headers = "Accept=application/json; charset=utf-8")
    @ResponseBody
    public ResponseDTO usersList(@ModelAttribute QueryDTO query, HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException
    {
        
        ResponseDTO result = new ResponseDTO();
        String username = (String) request.getAttribute(Constants.HTTP_REQUEST_USERNAME);
        try
        {   
            query.setModelName("Users");
            query.setSelectColumns("sys_id,UUserName,UFirstName,ULastName,sysCreatedOn,sysCreatedBy,sysUpdatedOn,sysUpdatedBy");
            
            //get the data
            List<UsersVO> data = ServiceHibernate.getUsers(query, username);
            int total = ServiceHibernate.getTotalHqlCount(query);

            result.setSuccess(true).setRecords(data);
            result.setTotal(total);
        }
        catch (Exception e)
        {
            Log.log.error("Error retrieving Users", e);
            result.setSuccess(false).setMessage("Error getting user list. See log for more info.");
        }

        return result;
        
    }
    
    @RequestMapping(value = "/common/roles/list", method = { RequestMethod.POST, RequestMethod.GET }, headers = "Accept=application/json; charset=utf-8")
    @ResponseBody
    public ResponseDTO rolesList(@ModelAttribute QueryDTO query, HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException
    {
        
        ResponseDTO result = new ResponseDTO();
        String username = (String) request.getAttribute(Constants.HTTP_REQUEST_USERNAME);
//        String reqStr = (String) request.getAttribute(Constants.REQUIRE_PUBLIC_ROLE);
        String reqStr = (String) request.getParameter(Constants.REQUIRE_PUBLIC_ROLE);
        boolean reqPublic = false;
        if (StringUtils.isNotEmpty(reqStr))
        {
            reqPublic = Boolean.parseBoolean(reqStr);
        }
        
        try
        {   
            query.setModelName("Roles");
            query.setSelectColumns("sys_id,UName,sysCreatedOn,sysCreatedBy,sysUpdatedOn,sysUpdatedBy");
            
            //get the data
            List<RolesVO> data = ServiceHibernate.getRoles(query, username);
            int total = ServiceHibernate.getTotalHqlCount(query);

            if (!reqPublic)
            {
                boolean found = false;
                int index = 0;
                for (RolesVO rvo : data)
                {
                    if (Constants.PUBLIC_ROLE.equals(rvo.getUName()))
                    {
                        found = true;
                        break;
                    }
                    index++;
                }
                if (found)
                {
                    data.remove(index);
                    total--;
                }
            }

            result.setSuccess(true).setRecords(data);
            result.setTotal(total);
        }
        catch (Exception e)
        {
            Log.log.error("Error retrieving Roles", e);
            result.setSuccess(false).setMessage("Error getting role list. See log for more info.");
        }

        return result;
        
    }
    
    
    @RequestMapping(value = "/common/controllers/list", method = { RequestMethod.POST, RequestMethod.GET }, headers = "Accept=application/json; charset=utf-8")
    @ResponseBody
    public ResponseDTO controllersList(@ModelAttribute QueryDTO query, HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException
    {
        
        ResponseDTO result = new ResponseDTO();
        String username = (String) request.getAttribute(Constants.HTTP_REQUEST_USERNAME);
        try
        {   
            query.setModelName("ResolveControllers");
            query.setSelectColumns("sys_id,UControllerName,sysCreatedOn,sysCreatedBy,sysUpdatedOn,sysUpdatedBy");
            
            //get the data
            List<ResolveControllersVO> data = ServiceHibernate.getResolveControllers(query, username);
            int total = ServiceHibernate.getTotalHqlCount(query);

            result.setSuccess(true).setRecords(data);
            result.setTotal(total);
        }
        catch (Exception e)
        {
            Log.log.error("Error retrieving Controller list", e);
            result.setSuccess(false).setMessage("Error getting controller list. See log for more info.");
        }

        return result;
        
    }
    
    /**
     * 
     * @param type - valid values are WIKI,RUNBOOK,DECISIONTREE, ALL
     * @param query
     * @param request
     * @param response
     * @return
     * @throws ServletException
     * @throws IOException
     */
    @RequestMapping(value = "/common/wiki/list", method = { RequestMethod.POST, RequestMethod.GET }, headers = "Accept=application/json; charset=utf-8")
    @ResponseBody
    public ResponseDTO wikiList(@RequestParam("type") String type, @ModelAttribute QueryDTO query, HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException
    {
        
        ResponseDTO result = new ResponseDTO();
        String username = (String) request.getAttribute(Constants.HTTP_REQUEST_USERNAME);
        try
        {   
            query.setModelName("WikiDocument");
            query.setSelectColumns("sys_id,UFullname,sysCreatedOn,sysCreatedBy,sysUpdatedOn,sysUpdatedBy");

            NodeType nodeType = null;
            if(StringUtils.isNotEmpty(type))
            {
                if(type.trim().equalsIgnoreCase("WIKI"))
                {
                    nodeType = NodeType.DOCUMENT;
                }
                else if(type.trim().equalsIgnoreCase("RUNBOOK"))
                {
                    nodeType = NodeType.RUNBOOK;
                }
                else if(type.trim().equalsIgnoreCase("DECISIONTREE"))
                {
                    nodeType = NodeType.DECISIONTREE;
                }
            }
            
            
            //get the data
            List<WikiDocumentVO> data = ServiceWiki.getWikiDocuments(query, nodeType, username);
            int total = ServiceHibernate.getTotalHqlCount(query);

            result.setSuccess(true).setRecords(data);
            result.setTotal(total);
        }
        catch (Exception e)
        {
            Log.log.error("Error retrieving WikiDocument", e);
            result.setSuccess(false).setMessage("Error getting wiki list. See log for more info.");
        }

        return result;
        
    }
    
    @RequestMapping(value = "/common/menudefinition/list", method = { RequestMethod.POST, RequestMethod.GET }, headers = "Accept=application/json; charset=utf-8")
    @ResponseBody
    public ResponseDTO menuDefinitionList(@ModelAttribute QueryDTO query, HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException
    {
        
        ResponseDTO result = new ResponseDTO();
        String username = (String) request.getAttribute(Constants.HTTP_REQUEST_USERNAME);
        try
        {   
            query.setModelName("SysAppApplication");
//            query.setWhereClause(" active is true ");
            query.setSelectColumns("sys_id,name,title,sysCreatedOn,sysCreatedBy,sysUpdatedOn,sysUpdatedBy");
            
            //get the data
            List<SysAppApplicationVO> data = ServiceHibernate.getMenuDefinitions(query, username);
            int total = ServiceHibernate.getTotalHqlCount(query);

            result.setSuccess(true).setRecords(data);
            result.setTotal(total);
        }
        catch (Exception e)
        {
            Log.log.error("Error retrieving menu application", e);
            result.setSuccess(false).setMessage("Error getting menu definition list. See log for more info.");
        }

        return result;
        
    }

    
    @RequestMapping(value = "/common/actiontask/namespaces", method = { RequestMethod.POST, RequestMethod.GET }, headers = "Accept=application/json; charset=utf-8")
    @ResponseBody
    public ResponseDTO getAllActiontaskNamespaces(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException
    {
        
        ResponseDTO result = new ResponseDTO();
        String username = (String) request.getAttribute(Constants.HTTP_REQUEST_USERNAME);
        try
        {   
            //get the data
            List<String> data = new ArrayList<String>(ServiceHibernate.findAllResolveActionTaskModuleNames(username));
            result.setSuccess(true).setRecords(data);
        }
        catch (Exception e)
        {
            Log.log.error("Error retrieving actiontask namespace ", e);
            result.setSuccess(false).setMessage("Error gettoing all action task namespaces. See log for more info.");
        }

        return result;
        
    }
    
}
