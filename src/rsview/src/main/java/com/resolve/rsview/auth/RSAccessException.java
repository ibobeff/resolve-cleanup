/******************************************************************************
* (C) Copyright 2014
*
* Resolve Systems, LLC
*
* All rights reserved
* This software is distributed under license and may not be used, copied,
* modified, or distributed without the express written permission of
* Resolve Systems, LLC.
*
******************************************************************************/

package com.resolve.rsview.auth;


public class RSAccessException extends Exception
{

	private static final long serialVersionUID = 9006718683135009474L;
	
	public RSAccessException() {
		super();
	}
	
	public RSAccessException(String errMsg) {
		super(errMsg);
	}
} // RSAccessException
