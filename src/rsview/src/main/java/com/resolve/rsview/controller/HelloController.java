/******************************************************************************
* (C) Copyright 2014
*
* Resolve Systems, LLC
*
* All rights reserved
* This software is distributed under license and may not be used, copied,
* modified, or distributed without the express written permission of
* Resolve Systems, LLC.
*
******************************************************************************/

package com.resolve.rsview.controller;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.owasp.esapi.ESAPI;
import org.owasp.esapi.errors.AccessControlException;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.CookieValue;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.context.request.NativeWebRequest;
import org.springframework.web.servlet.ModelAndView;

import com.resolve.persistence.model.ResolveActionTask;
import com.resolve.persistence.util.HibernateProxy;
import com.resolve.persistence.util.HibernateUtil;
import com.resolve.rsview.auth.Authentication;
import com.resolve.rsview.auth.RSAccessException;
import com.resolve.rsview.auth.RSAuthException;
import com.resolve.rsview.auth.SSOSessTimedOutException;
import com.resolve.rsview.auth.SSOUserCreateInResolveDueToLicensingIssuesException;
import com.resolve.rsview.auth.SSOUserLockedInResolveException;
import com.resolve.rsview.auth.SSOUserGetFromOrCreateInResolveException;
import com.resolve.rsview.auth.SSOConfigMissingOrIncompleteInResolveException;
import com.resolve.rsview.auth.SSOConfiguredHostAccessException;
import com.resolve.util.Log;

@Controller
public class HelloController //extends AbstractController 
{
	
     public ModelAndView handleRequestInternal(HttpServletRequest request, HttpServletResponse response) throws Exception
     {
    	 String id1 = request.getParameter("id");
    	 String abc = request.getParameter("abc");
    	 String h1 = request.getParameter("h1");
    	 ModelAndView mav = new ModelAndView("welcome");
	
    	 return mav;

     } 

    @RequestMapping(value="/welcome", method={RequestMethod.GET, RequestMethod.POST})
    public ModelAndView welcome(NativeWebRequest wr, HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException
    {
        ModelAndView result = null;
        
        String id1 = wr.getParameter("id1");//from GET
        String h1 = wr.getParameter("h1");//from POST
                
        try
        {
        	Authentication.validate(request, response);
        	result =  new ModelAndView("welcome");

        	long starttime = System.currentTimeMillis();
        	
          HibernateProxy.setCurrentUser("system");
        	HibernateProxy.execute(() -> {

        		System.out.println("after beginTransaction duration: "+(System.currentTimeMillis()-starttime));

        		ResolveActionTask task = HibernateUtil.getDAOFactory().getResolveActionTaskDAO().findById("b77f03e8c0a8a21401c8b31335d471fb");
        		System.out.println("after findFirst duration: "+(System.currentTimeMillis()-starttime));

        		if (task != null)
        		{
        			task.setUSummary(""+starttime);
        			System.out.println("after setUName duration: "+(System.currentTimeMillis()-starttime));

        			HibernateUtil.getDAOFactory().getResolveActionTaskDAO().save(task);
        			System.out.println("after save duration: "+(System.currentTimeMillis()-starttime));
        		}

        	});
        	System.out.println("after commit duration: "+(System.currentTimeMillis()-starttime));

        }
        catch (RSAuthException e)
        {
        	result = e.getModelAndView();

        	Log.log.error(e.getMessage(), e);

        }
        catch (RSAccessException | SSOSessTimedOutException | 
        		SSOUserLockedInResolveException | SSOUserGetFromOrCreateInResolveException |
        		SSOConfigMissingOrIncompleteInResolveException | SSOConfiguredHostAccessException |
        		SSOUserCreateInResolveDueToLicensingIssuesException e)
        {
        	Log.log.error(e.getMessage(), e);
        } catch (Exception e) {
        	Log.log.error(e.getMessage(), e);
                                         HibernateUtil.rethrowNestedTransaction(e);
        }
        
        return result;
    } // welcome

    @RequestMapping(value="/hello/{table}", method=RequestMethod.GET)
    public ModelAndView hello(@PathVariable("table") String type, HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException
    {
        System.out.println("Returning hello view type: "+type);
        ModelAndView result = null;
        
        try
        {
	        Authentication.validate(request, response);
            result =  new ModelAndView("welcome");
        }
        catch (RSAuthException e)
        {
            result = new ModelAndView("login");
        }
        catch (RSAccessException | SSOSessTimedOutException | 
                        SSOUserLockedInResolveException | SSOUserGetFromOrCreateInResolveException |
                        SSOConfigMissingOrIncompleteInResolveException | SSOConfiguredHostAccessException |
                        SSOUserCreateInResolveDueToLicensingIssuesException e)
        {
            Log.log.error(e.getMessage(), e);
        }

        return result;
    } // hello

    @RequestMapping(value="/hello/param", method=RequestMethod.GET)
    public ModelAndView helloParam(@RequestParam("id") int id, HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException
    {

        System.out.println("Returning hello view id: "+id);

        return new ModelAndView("hello.jsp");
    } // helloParam

    @RequestMapping(value="/hello/body", method=RequestMethod.GET)
    @ResponseBody
    public String helloBody(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException
    {

        System.out.println("Returning hello view body");

        return "hello world";
    } // helloBody

    @RequestMapping(value="/hello/cookie", method=RequestMethod.GET)
    public ModelAndView helloCookie(@CookieValue("JSESSIONID") int id, HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException
    {

        System.out.println("Returning hello view id: "+id);

        return new ModelAndView("hello.jsp");
    } // helloCookie

    @RequestMapping(value="/hello/header", method=RequestMethod.GET)
    public ModelAndView helloHeader(@RequestHeader("host") String host, HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException
    {

        System.out.println("Returning hello view host: "+host);

        return new ModelAndView("hello");
    } // helloCookie

    @RequestMapping(value="/redirect/{url}", method=RequestMethod.GET)
    public String redirect(@PathVariable("url") String url, HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException, AccessControlException
    {

        System.out.println("Redirecting to: "+url);

        ESAPI.httpUtilities().sendRedirect(response, "/service/"+url);
        return "/service/"+url;
    } // redirect

    @RequestMapping(value="/wiki/execute/{namespace}/{name}", method=RequestMethod.GET)
    public ModelAndView executeRunbook(@PathVariable("name") String name, @PathVariable("namespace") String namespace, HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException
    {
        String out = "Execute: "+namespace+"."+name;
        System.out.println(out);
        ModelAndView result = null;
        
        try
        {
	        Authentication.validate(request, response);
            result =  new ModelAndView("welcome", "content", out);
        }
        catch (RSAuthException e)
        {
            result = new ModelAndView("login");
        }
        catch (RSAccessException | SSOSessTimedOutException | 
               SSOUserLockedInResolveException | SSOUserGetFromOrCreateInResolveException |
               SSOConfigMissingOrIncompleteInResolveException | SSOConfiguredHostAccessException |
               SSOUserCreateInResolveDueToLicensingIssuesException e)
        {
            Log.log.error(e.getMessage(), e);
        }

        return result;
    } // executeRunbook

} // HelloController
