/******************************************************************************
* (C) Copyright 2014
*
* Resolve Systems, LLC
*
* All rights reserved
* This software is distributed under license and may not be used, copied,
* modified, or distributed without the express written permission of
* Resolve Systems, LLC.
*
******************************************************************************/

package com.resolve.rsview.controller;

import java.io.IOException;
import java.util.Arrays;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.stereotype.Controller;
import org.springframework.stereotype.Service;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.resolve.services.ServiceHibernate;
import com.resolve.services.hibernate.vo.ResolveImpexLogVO;
import com.resolve.services.vo.QueryDTO;
import com.resolve.services.vo.ResponseDTO;
import com.resolve.util.Constants;
import com.resolve.util.Log;

@Controller
@Service
public class AjaxImpexLog extends GenericController
{
    /**
     * Returns list of Impex Logs for the grid based on filters, sorts, pagination
     * 
     * @param query
     * @param request
     * @param response
     * @return
     * @throws ServletException
     * @throws IOException
     */
    @RequestMapping(value = "/impexlog/getImpexLog", method = { RequestMethod.GET, RequestMethod.POST }, headers = "Accept=application/json; charset=utf-8")
    @ResponseBody
    public ResponseDTO list(@ModelAttribute QueryDTO query, HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException
    {
        ResponseDTO result = new ResponseDTO();
        try
        {  

            query.setModelName("ResolveImpexLog");
            
            List<ResolveImpexLogVO> data = ServiceHibernate.getResolveImpexLogs(query);
            int total = ServiceHibernate.getTotalHqlCount(query);
           
            result.setSuccess(true).setRecords(data);
            result.setTotal(total);
        }
        catch (Exception e)
        {
            Log.log.error("Error retrieving impex log", e);
            result.setSuccess(false).setMessage("Error retrieving impex log");
        }
        
        return result;
    }
    
    /**
     * Gets data for a specific Impex Log based on sysId
     * 
     * @param id
     * @param request
     * @param response
     * @return
     * @throws ServletException
     * @throws IOException
     */
    @RequestMapping(value = "/impexlog/getImpexLogRec", method = { RequestMethod.POST }, headers = "Accept=application/json; charset=utf-8")
    @ResponseBody
    public ResponseDTO get(@RequestParam String id, HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException
    {
        ResponseDTO result = new ResponseDTO();

        try
        {
            ResolveImpexLogVO vo = ServiceHibernate.getResolveImpexLog(id);
            result.setData(vo);
            result.setSuccess(true);
            
        }
        catch (Exception e)
        {
            Log.log.error("Error retrieving impex log", e);
            result.setSuccess(false).setMessage("Error retrieving impex log");
        }

        return result;
    }
    
    /**
     *  Delete the impex logs based on array of sysIds
     * 
     * @param ids
     * @param request
     * @param response
     * @return
     * @throws ServletException
     * @throws IOException
     */
    @RequestMapping(value = "/impexlog/deleteImpexLogRecs", method = { RequestMethod.POST }, headers = "Accept=application/json; charset=utf-8")
    @ResponseBody
    public ResponseDTO delete(@RequestParam String[] ids, HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException
    {
        ResponseDTO result = new ResponseDTO();

        String username = (String) request.getAttribute(Constants.HTTP_REQUEST_USERNAME);
        
        try
        {
            List<String> sysIds = Arrays.asList(ids);
            ServiceHibernate.deleteResolveImpexLogBySysId(sysIds, username);
            
            result.setSuccess(true);
        }
        catch (Exception e)
        {
            Log.log.error("Error deleting impex log", e);
            result.setSuccess(false).setMessage("Error deleting impex log");
        }

        return result;
    }
}
