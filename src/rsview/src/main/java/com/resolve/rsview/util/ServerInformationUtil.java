/******************************************************************************
* (C) Copyright 2014
*
* Resolve Systems, LLC
*
* All rights reserved
* This software is distributed under license and may not be used, copied,
* modified, or distributed without the express written permission of
* Resolve Systems, LLC.
*
******************************************************************************/

package com.resolve.rsview.util;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Set;

import javax.servlet.http.HttpServletRequest;

import com.resolve.rsbase.MainBase;
import com.resolve.rsview.auth.AuthSession;
import com.resolve.rsview.auth.Authentication;
import com.resolve.rsview.main.SystemStats;
import com.resolve.services.ServiceHibernate;
import com.resolve.services.util.PropertiesUtil;
import com.resolve.services.vo.ServerInfoDTO;
import com.resolve.services.vo.UserSessionDetailDTO;
import com.resolve.services.vo.UserSessionInfoDTO;
import com.resolve.util.Constants;
import com.resolve.util.StringUtils;

/**
 * util to get the server info
 * 
 * @author jeet.marwah
 *
 */
public class ServerInformationUtil
{
    
    public static UserSessionInfoDTO getUserSessionInfo()
    {
        //get current users that are logged in the server
        List<UserSessionDetailDTO> sessionDetails = getUserSessionDetails();

        //get other session info
        long activeSessions = Authentication.getSessionActive();
        long maxActiveSessions = Authentication.getSessionMaxActive();

        String maxSessions = PropertiesUtil.getPropertyString("session.maxallowed");
        Integer maxSessionInt = new Integer(-1);
        try
        {
            maxSessionInt = Integer.parseInt(maxSessions);
        }
        catch(Exception e)
        {
            maxSessionInt = -1;
        }

        String timeout = PropertiesUtil.getPropertyString("session.timeout");
        Integer timeoutInt = new Integer(-1);
        try
        {
            timeoutInt = Integer.parseInt(timeout);
            
            if (timeoutInt.intValue() < 720)
            {
                timeoutInt = Integer.valueOf(720);
            }
        }
        catch(Exception e)
        {
            timeoutInt = -1;
        }
        
        //prepare the result
        UserSessionInfoDTO result = new UserSessionInfoDTO();
        result.setActiveSessions(new Long(activeSessions).intValue());
        result.setMaxActiveSessions(new Long(maxActiveSessions).intValue());
        result.setMaxSessions(maxSessionInt);
        result.setTimeout(timeoutInt);
        result.setSessionDetails(sessionDetails);
        
        
        return result;
    }
    
    public static void removeSession(String[] sessionIds, String username)
    {
        if(sessionIds != null)
        {
            Set<String> roles = ServiceHibernate.getUserRoles(username);
            if(roles.contains("admin"))
            {
                for(String token : sessionIds)
                {
                    if(StringUtils.isNotBlank(token))
                    {
                        Authentication.removeAuthToken(token);
                    }
                }
            }
            else
            {
                throw new RuntimeException("User needs 'admin' role to do this operation.");
            }
        }
    }
    
    public static ServerInfoDTO getServerInformation(HttpServletRequest request)
    {
        ServerInfoDTO result = new ServerInfoDTO();
        
        //version
        result.setVersion(MainBase.main.release.version);
        
        //server
        result.setOsArchitecture(SystemStats.getOSArch());
        result.setOsName(SystemStats.getOSName());
        result.setOsVersion(SystemStats.getOSVersion());
        result.setAddress(SystemStats.getAddress(request));
        result.setOsProcessor(SystemStats.getOSProcessors());
        result.setJvmVendor(SystemStats.getJVMVendor());
        result.setJvmName(SystemStats.getJVMName());
        result.setJvmVersion(SystemStats.getJVMVersion());
        
        //memory
        result.setMemoryMax(SystemStats.getMemoryMax());
        result.setMemoryTotal(SystemStats.getMemoryTotal());
        result.setMemoryFreePercentage(SystemStats.getMemoryFreePercentage());
        
        //thread
        result.setThreadPeak(SystemStats.getThreadPeak());
        result.setThreadLive(SystemStats.getThreadLive());
        result.setThreadDeamon(SystemStats.getThreadDaemon());
        
        return result;
    }
    
    //private apis
    private static List<UserSessionDetailDTO> getUserSessionDetails()
    {
        List<UserSessionDetailDTO> sessionDetails = new ArrayList<UserSessionDetailDTO>();
        List<AuthSession> userSessions = Authentication.getActiveUserSessions();
        for(AuthSession session : userSessions)
        {
            long effectiveSessionTimeout = (PropertiesUtil.getPropertyBoolean("session.fixed.timeout") ?
                                            PropertiesUtil.getPropertyLong(Constants.SESSION_MAXALLOWED) :
                                            PropertiesUtil.getPropertyLong(Constants.SESSION_TIMEOUT)) * 1000; 

            if (effectiveSessionTimeout < 720000)
            {
                effectiveSessionTimeout = 720000;
            }
            
            long duration = System.currentTimeMillis() - 
                            (PropertiesUtil.getPropertyBoolean("session.fixed.timeout") ? 
                             session.getCreateTime() : session.getUpdateTime());
            
            UserSessionDetailDTO us = new UserSessionDetailDTO();
            us.setId(session.getToken());
            us.setUsername(session.getUsername());
            us.setHost(session.getHost());
            us.setCreateTime(new Date(session.getCreateTime()));
            us.setUpdateTime(new Date(session.getUpdateTime()));
            us.setDurationInSecs((new Long(duration / 1000)).intValue());
            
            if (StringUtils.isNotBlank(session.getSSOType()))
            {
                us.setSsoType(session.getSSOType());
            }
            
            if (session.getSessionTimeout() != null)
            {
                long remainingTime = /*session.getSessionTimeout()*/effectiveSessionTimeout - duration/*((duration / 60) * 60000)*/;
                us.setSessionDurationInMin((Long)(remainingTime / 60000));
            }
            
            if (StringUtils.isNotBlank(session.getSSOSessionId()))
            {
                us.setSsoSessionId(session.getSSOSessionId());
            }
            
            if (session.getLastSSOValidationTime() != null)
            {
                us.setLastSSOValidationTime(new Date(session.getLastSSOValidationTime()));
            }
            
            if (StringUtils.isNotBlank(session.getIFrameId()))
            {
                us.setIframeId(session.getIFrameId());
            }
            
            if (StringUtils.isNotBlank(session.getSessionId()))
            {
                us.setSessionId(session.getSessionId());
            }
            
            sessionDetails.add(us);
        }
        
        return sessionDetails;
    }
    

}
