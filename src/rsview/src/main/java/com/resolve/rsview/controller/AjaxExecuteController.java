/******************************************************************************
 * (C) Copyright 2014
 *
 * Resolve Systems, LLC
 *
 * All rights reserved
 * This software is distributed under license and may not be used, copied,
 * modified, or distributed without the express written permission of
 * Resolve Systems, LLC.
 *
 ******************************************************************************/

package com.resolve.rsview.controller;

import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.codehaus.jackson.JsonNode;
import org.codehaus.jackson.map.ObjectMapper;
import org.springframework.stereotype.Controller;
import org.springframework.stereotype.Service;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.resolve.rsbase.MainBase;
import com.resolve.services.ServiceHibernate;
import com.resolve.services.util.PlaybookUtils;
import com.resolve.services.util.SequenceUtils;
import com.resolve.services.util.WorksheetUtils;
import com.resolve.services.vo.ExecuteDTO;
import com.resolve.services.vo.ResponseDTO;
import com.resolve.util.Constants;
import com.resolve.util.Log;
import com.resolve.util.StringUtils;

import net.sf.json.JSONObject;

/**
 * 
 * This controller accepts request to execute the actiontask and runbook from the UI.
 * 
 * @author jeet.marwah
 *
 */
@Controller
@Service
public class AjaxExecuteController extends GenericController
{
    /**
     * 
     * This api serves the request to execute the Runbook or Actiontask from the UI.
     * 
     * <p>
     * <u><b>Runbook Example</b></u>:</br>
     * <code><pre>
     *  isDebug: true
     *  mockName = ""
     *  params: {}
     *  problemId:"NEW"
     *  wiki:"RB.Test1"
     * </pre></code>
     * 
     * <p>
     * <u><b>Actiontask Example</b></u>:</br>
     * <code><pre>
     *  action: "TASK"
     *  actiontask: "comment#resolve"
     *  actiontaskSysId: "e4e07300c6112272018f2a8daf9fbc46"
     *  isDebug: true
     *  mockName: ""
     *  params: {SUMMARY: hello, DETAIL: world, CONDITION: good, SEVERITY: good}
     *  problemId: "NEW"
     * </pre></code>
     * 
     * <p>
     * Parameters in the Request object:
     * <code><pre>
     *  USERNAME - username of the user who is logged in to Resolve
     * </pre></code>
     * 
     * <p>
     * Return object in response:
     * <code><pre>
     *  'data' : List of type {@link ExecuteDTO}
     * </pre></code>
     * 
     * @param query
     * @param request
     * @param response
     * @return
     * @throws ServletException
     * @throws IOException
     */
    @RequestMapping(value = "/execute/submit", method = { RequestMethod.GET, RequestMethod.POST }, headers = "Accept=application/json; charset=utf-8")
    @ResponseBody
    public ResponseDTO execute(@RequestBody JSONObject json, HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException
    {
        ObjectMapper mapper = new ObjectMapper();
        ExecuteDTO query = mapper.readValue(json.toString(), ExecuteDTO.class);
        ResponseDTO result = new ResponseDTO();
        String username = (String) request.getAttribute(Constants.HTTP_REQUEST_USERNAME);
        String token = (String) request.getAttribute(Constants.HTTP_REQUEST_TOKEN);
        String rsviewIdGuid = (String) request.getAttribute(Constants.HTTP_ATTRIBUTE_RSVIEW_ID_GUID);

        try
        {
            // to verify if we need to validate the user here incase we make this public
            // Authentication.validate(request, response);

            if (query == null)
            {
                String redirect = (new ExecuteController()).execute(request, response);

                // update the response
                result.setSuccess(true).setData(redirect);
            }
            else
            {
                query.setUsername(username);
                query.setToken(token);
                query.setRsviewIdGuid(rsviewIdGuid);

                Map<String, Object> params = query.getMap();
                /*
                String problemId = query.getProblemId();
                if (StringUtils.isNotBlank(problemId))
                {
                	params.put("PROBLEMID", problemId);
                }
                */
                String redirect = (new ExecuteController()).execute(params);

                // update the values
                ExecuteDTO data = new ExecuteDTO(params);
                
                Object sirValue = params.get(Constants.SIR);
                if (sirValue!=null && sirValue instanceof Map)
                {
	                Map<String, String> sir = params.containsKey(Constants.SIR) ? (HashMap<String, String>)params.get(Constants.SIR) : null;
	                if (sir != null)
	                {
	                    // Execution came from SIR.
	                    String incidentId = sir.get(PlaybookUtils.INCIDENT_ID);
	                    String type = sir.get(Constants.PB_ARTIFACT_TYPE);
	                    String name = sir.get(Constants.PB_ARTIFACT_NAME);
	                    String actionDefinition = sir.get(Constants.EXECUTE_ACTION.toLowerCase());
	                    String task = (String)params.get(Constants.EXECUTE_ACTIONNAME);
	                    String description = String.format("User '%s' submitted execution for '%s' task from an artifact (type: '%s', name: '%s' and the action definition: '%s')", username, task, type, name, actionDefinition);
	                    PlaybookUtils.auditAction(description, incidentId, request.getRemoteAddr(), username);
	                    params.remove(Constants.SIR);
	                }
                }

                // update the response
                result.setSuccess(true).setData(data);
                // result.setTotal(total);
            }
        }
        catch (Exception e)
        {
            String message = null;
            if (query.getAction().equalsIgnoreCase("task"))
            {
                message = "Error executing the ActionTask: ";
            }
            else
            {
                message = "Error executing the Wiki: ";
            }

            Log.log.error("Error executing the Runbook", e);
            result.setSuccess(false).setMessage(message + "Error executing the Runbook");
        }

        return result;
    }

    /**
     * This API serves the request to execute a list of ActionTasks/Runbooks from the UI synchronously.
     * 
     * <p>
     * <u><b>Example RESTful HTTP Request (POST)</b></u>:</br>
     * <b>URL:</b>
     * <code><pre>
     * https://localhost.cloud.resolvesys.com:8443/resolve/service/execute/submitSync
     * </pre></code>
     * <b>Headers:</b>
     * <code><pre>
     * Accept: * /*
     * Accept-Encoding: gzip
     * Accept-Language: en-US
     * Cache-Control: no-cache
     * Connection: keep-alive
     * Content-Type: application/json
     * Pragma: no-cache
     * User-Agent: Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML
     * X-Requested-With: XMLHttpRequest
     * </pre></code>
     * <b>JSON body:</b>
     * <code><pre>
     * {
     *     "executionList": [
     *         {
     *             "problemId": "ACTIVE",
     *             "isDebug": false,
     *             "mockName": "",
     *             "params": {
     *                 "INTERVAL": "10"
     *             },
     *             "actiontask": "task1#dttest",
     *             "action": "TASK"
     *         },
     *         {
     *             "problemId": "ACTIVE",
     *             "isDebug": false,
     *             "mockName": "",
     *             "params": {},
     *             "wiki": "test.Dako"
     *         },
     *         {
     *             "problemId": "ACTIVE",
     *             "isDebug": false,
     *             "mockName": "",
     *             "params": {
     *                 "DETAIL": "not change here",
     *                 "SEVERITY": "good",
     *                 "CONDITION": "good",
     *                 "SUMMARY": "test"
     *             },
     *             "actiontask": "task2#dttest",
     *             "action": "TASK"
     *         }
     *     ],
     *     "dtVariables": [
     *         {
     *             "key": "DTTEST_TASK1_KEY1",
     *             "taskName": "task1#dttest",
     *             "outputName": "DTTEST_TASK1_KEY1"
     *         },
     *         {
     *             "key": "DTTEST_TASK2_KEY1",
     *             "taskName": "task2#dttest",
     *             "outputName": "DTTEST_TASK2_KEY1"
     *         }
     *     ]
     * }
     * </pre></code>
     * 
     * @param json
     * @param request
     * @param response
     * @return
     * @throws ServletException
     * @throws IOException
     */
    @RequestMapping(value = "/execute/submitSync", method = { RequestMethod.GET, RequestMethod.POST }, headers = "Accept=application/json; charset=utf-8")
    @ResponseBody
    public ResponseDTO[] executeSynchronously(@RequestBody Map<String, Object> reqParams, HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException
    {
        ResponseDTO[] result = null;
        
        ObjectMapper mapper = new ObjectMapper();
        JsonNode json = mapper.convertValue(reqParams.get("executionList"), JsonNode.class); 
        ExecuteDTO[] executionList = mapper.readValue(json, ExecuteDTO[].class);
        String dtVariables = reqParams.get(Constants.HTTP_REQUEST_DT_VARIABLES).toString();

        if (executionList == null || executionList.length < 1)
        {
            result = new ResponseDTO[1];
            result[0].setSuccess(false).setMessage("Unable to execute due to empty execution list.");
            return result;
        }
        result = new ResponseDTO[executionList.length];
        String username = (String) request.getAttribute(Constants.HTTP_REQUEST_USERNAME);
        String orgId = ((String) request.getAttribute(Constants.EXECUTE_ORG_ID));
        String orgName = ((String) request.getAttribute(Constants.EXECUTE_ORG_NAME));
        String token = (String) request.getAttribute(Constants.HTTP_REQUEST_TOKEN);
        String rsviewIdGuid = (String) request.getAttribute(Constants.HTTP_ATTRIBUTE_RSVIEW_ID_GUID);

        String currentExecutionName = null;
        try
        {
            for (int i = 0; i < executionList.length; i++)
            {
                String action = executionList[i].getAction();
                String userid = username;
                String problemid = executionList[i].getProblemId();
                String actionname = executionList[i].getActiontask();
                String namespace = StringUtils.isNotBlank(actionname) ? StringUtils.substringAfter(executionList[i].getActiontask(), "#") : null;
                String wiki = executionList[i].getWiki();
                Map<String, Object> params = new HashMap<String, Object>();
                params.put(Constants.EXECUTE_USERID, userid);
                params.put(Constants.EXECUTE_PROBLEMID, problemid);
                params.put(Constants.EXECUTE_ACTIONNAME, actionname);
                params.put(Constants.EXECUTE_NAMESPACE, namespace);
                params.put(Constants.EXECUTE_WIKI, wiki);
                params.put(Constants.HTTP_REQUEST_ACTION, action);
                params.put(Constants.EXECUTE_AT_SYNCHRONOUSLY_TIMEOUT, Long.toString(600 * 1000));
                params.put(Constants.DT_AUTO_ANSWER_VARIABLES_JSON, dtVariables);
                
                if (StringUtils.isNotBlank(orgId))
                {
                    params.put(Constants.EXECUTE_ORG_ID, orgId);
                }
                
                if (StringUtils.isNotBlank(orgName))
                {
                    params.put(Constants.EXECUTE_ORG_NAME, orgName);
                }
                
                if (executionList[i].getParams() != null)
                {
                    for (Map.Entry<String, Object> param : executionList[i].getParams().entrySet())
                    {
                        params.put(param.getKey(), param.getValue());
                    }
                }
                if (action != null && action.equals("TASK"))
                {
                    Map<?, ?> executionResponse = executeATSynchronously(params);
                }
                else if (action != null && action.equals("EXECUTEPROCESS"))
                {
                    Map<?, ?> executionResponse = executeRunbookSynchronously(params);
                }
                // update the values
                ExecuteDTO data = new ExecuteDTO(params);
                result[i] = new ResponseDTO();
                // update the response
                result[i].setSuccess(true).setData(data);
            }
        }
        catch (Exception e)
        {
            result = new ResponseDTO[1];
            result[0] = new ResponseDTO();
            result[0].setSuccess(false).setMessage(String.format("Unable to execute '%s' due to the following exception: %s", currentExecutionName, e.getMessage()));
            return result;
        }
        return result;
    }

    @SuppressWarnings("unused")
    private static String initExecuteActionTask(Map<String, Object> params, String problemid, String userid, String reference, String alertid, String correlationid, String queryString) throws Exception
    {
        String execute_sid = null;
        String actionTaskName = (String) params.get(Constants.EXECUTE_ACTIONNAME);
        String actionid = (String) params.get(Constants.EXECUTE_ACTIONID);
        String namespace = (String) params.get(Constants.EXECUTE_NAMESPACE);

        // get the references if the objects that we need
        String newExecNumber = SequenceUtils.getNextSequence(Constants.PERSISTENCE_SEQ_EXEC);
        if (actionTaskName.indexOf("#") == -1 && StringUtils.isNotBlank(namespace))
        {
            actionTaskName = actionTaskName + "#" + namespace;
        }
        boolean isActionTaskExist = ServiceHibernate.isActionTaskExist(actionid, actionTaskName);
        if (!isActionTaskExist)
        {
            throw new Exception("Actiontask name and sys_id are not available. This information is mandatory to execute an Actiontask.");
        }

        // create new worksheet and set active
        if (StringUtils.isEmpty(problemid) || problemid.equalsIgnoreCase("NEW") || problemid.equalsIgnoreCase("ACTIVE"))
        {
            // create worksheet
            problemid = WorksheetUtils.initWorksheet(problemid, reference, alertid, correlationid, userid, null, false, params);

            // set active worksheet
            WorksheetUtils.setActiveWorksheet(userid, problemid, 
                                             (String)params.get(Constants.EXECUTE_ORG_ID), 
                                             (String)params.get(Constants.EXECUTE_ORG_NAME));

            // set params
            params.put(Constants.EXECUTE_PROBLEMID, problemid);

            // replace NEW in query string
            if (!StringUtils.isEmpty(queryString))
            {
                queryString = queryString.replace("NEW", problemid);
            }
        }
        return execute_sid;
    }

    private Map<?, ?> executeATSynchronously(Map<String, Object> params)
    {
        Map<?, ?> result = new HashMap<>();
        try
        {
            params.put("Type", "DT Auto execute automation or action task");
            params.put(Constants.ESB_NAME_RSCONTROL, "Threshold " + 300 + " seconds");
            Map<String, String> response = MainBase.getESB().call(Constants.ESB_NAME_RSCONTROL, "MAction.executeATSynchronously", params, 300 * 1000);
            if (response == null)
            {
                Log.alert("executeSyncAT", "Failed. Params : " + params.toString());
            }
        }
        catch (Exception e)
        {
            String message = "Error executing the ActionTask: ";
            Log.log.error(message, e);
        }
        return result;
    }

    private Map<?, ?> executeRunbookSynchronously(Map<String, Object> params)
    {
        Map<?, ?> result = new HashMap<>();
        try
        {
            params.put("Type", "DT Auto execute automation or action task");
            params.put(Constants.ESB_NAME_RSCONTROL, "Threshold " + 300 + " seconds");

            String runbook = (String) params.get(Constants.EXECUTE_WIKI);
            String problemid = (String) params.get(Constants.EXECUTE_PROBLEMID);
            String userid = (String) params.get(Constants.EXECUTE_USERID);

            Map<String, String> response = MainBase.getESB().call(Constants.ESB_NAME_RSCONTROL, "MAction.executeRunbookWithResult", params, 300 * 1000);
            if (response == null)
            {
                Log.alert("executeSyncRB", "Failed. Params : " + params.toString());
            }
        }
        catch (Exception e)
        {
            String message = "Error executing the Runbook: ";
            Log.log.error(message, e);
        }
        return result;
    }

    /*
     * //test
     * public static void main(String[] args) throws Exception
     * {
     * // ExecuteDTO query = new ExecuteDTO();
     * // query.setWiki("Runbook.WebHome");
     * // query.setUsername("admin");
     * //
     * // new AjaxExecuteController().execute(query, null, null);
     * }
     */
}
