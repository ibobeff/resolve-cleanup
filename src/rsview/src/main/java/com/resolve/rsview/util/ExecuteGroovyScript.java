/******************************************************************************
* (C) Copyright 2014
*
* Resolve Systems, LLC
*
* All rights reserved
* This software is distributed under license and may not be used, copied,
* modified, or distributed without the express written permission of
* Resolve Systems, LLC.
*
******************************************************************************/

package com.resolve.rsview.util;

import groovy.lang.Binding;

import com.resolve.groovy.script.GroovyScript;
import com.resolve.util.Constants;
import com.resolve.util.Log;
import com.resolve.util.ScriptDebug;

public class ExecuteGroovyScript
{
    public static String executeScript(String scriptName, String script, String data) //throws Exception
    {
        Binding binding = new Binding();
        binding.setVariable("RAW", data);
        binding.setVariable(Constants.GROOVY_BINDING_LOG, Log.log);
        
        ScriptDebug debug = new ScriptDebug(true, null);
        binding.setVariable(Constants.GROOVY_BINDING_DEBUG, debug);
        
        Object o = "";
        try
        {
            o = GroovyScript.execute(script, scriptName, false, binding);
        }
        catch (Exception e)
        {
            Log.log.error("Error executing script :" + script, e);
            o = e;
        }

        return o.toString();
    }
}
