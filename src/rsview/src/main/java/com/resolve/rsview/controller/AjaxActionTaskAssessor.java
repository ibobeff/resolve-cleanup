/******************************************************************************
* (C) Copyright 2014
*
* Resolve Systems, LLC
*
* All rights reserved
* This software is distributed under license and may not be used, copied,
* modified, or distributed without the express written permission of
* Resolve Systems, LLC.
*
******************************************************************************/

package com.resolve.rsview.controller;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.codehaus.jackson.map.ObjectMapper;
import org.springframework.stereotype.Controller;
import org.springframework.stereotype.Service;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.resolve.services.ServiceHibernate;
import com.resolve.services.hibernate.vo.ResolveAssessVO;
import com.resolve.services.vo.QueryDTO;
import com.resolve.services.vo.ResponseDTO;
import com.resolve.util.Constants;
import com.resolve.util.Log;

import net.sf.json.JSONObject;


/**
 * 
 * This controller serves the request(CRUD operations) for Action Task Assessors. 
 * <p>
 * Assessors are sub-component of an Actiontask. They assess the data that they have at the time of execution and set the values based on it.
 * <p> 
 * For more information about Assessor, please refer to <a href="http://community.gen-e.com/resolve/jsp/rsclient.jsp">Resolve documentation</a> where it will explain how are they used in the execution of Actiontask. 
 * <p>
 * DB Table: {@code resolve_assess} </br>
 * Url : {@code http://localhost:8080/resolve/jsp/rsclient.jsp#RS.actiontask.AssessDefinitions/}
 * 
 *   
 * @author jeet.marwah
 *
 */
@Controller
@Service
public class AjaxActionTaskAssessor extends GenericController
{
    
    /**
     * Returns/Retrieves list of Assessor for the grid based on filters, sorts, pagination. 
     * <p>
     * <u><b>Example</b></u>:</br> 
     * <code><pre>
     *  filter: [{"field":"uname","type":"auto","condition":"contains","value":"resolve"}]
     *  page: 3
     *  start: 100
     *  limit: 50
     *  sort: [{"property":"sysUpdatedOn","direction":"ASC"}]
     * </pre></code>
     * 
     * <p>
     * Parameters in the Request object: 
     * <code><pre>
     *  USERNAME - username of the user who is logged in to Resolve
     * </pre></code>
     *  
     * <p>
     * Return object in response:
     * <code><pre>
     *  'records' : List of type {@link ResolveAssessVO}
     *  'total' : # of total records in the table/db 
     * </pre></code>
     *  
     * Attributes populated in records:
     * <code><pre>
     *  sys_id,sysCreatedOn,sysCreatedBy,sysUpdatedOn,sysUpdatedBy,UName,UDescription
     * </pre></code>
     * 
     * 
     * @param query  {@link QueryDTO}, represents the query, pagination, sorting, grouping for the UI
     * @param request  HttpServletRequest
     * @param response  HttpServletResponse
     * @return {@link ResponseDTO} - Reponse Data Transfer Object having a List  and total # of records 
     * @throws ServletException
     * @throws IOException
     */
    @RequestMapping(value = "/assessor/list", method = { RequestMethod.GET, RequestMethod.POST }, headers = "Accept=application/json; charset=utf-8")
    @ResponseBody
    public ResponseDTO list(@ModelAttribute QueryDTO query, HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException
    {
        ResponseDTO result = new ResponseDTO();
        String username = (String) request.getAttribute(Constants.HTTP_REQUEST_USERNAME);
        
        try
        {
            query.setModelName("ResolveAssess");
            query.setSelectColumns("sys_id,sysCreatedOn,sysCreatedBy,sysUpdatedOn,sysUpdatedBy,UName,UDescription");
            
            List<ResolveAssessVO> data = ServiceHibernate.getResolveAssess(query, username);
            int total = ServiceHibernate.getTotalHqlCount(query);
            
            result.setSuccess(true).setRecords(data);
            result.setTotal(total);
        }
        catch (Exception e)
        {
            Log.log.error("Error retrieving Assessor", e);
            result.setSuccess(false).setMessage("Error retrieving Assessor list. See log for more info.");
        }
        
        return result;
    }
    
    /**
     * Get Assessor by sysId
     * <p>
     * Parameters in the Request object:
     * <code><pre> 
     *  USERNAME - username of the user who is logged in to Resolve
     * </pre></code>
     * <p>
     * Return object in response 
     * <code><pre>
     *  'data' : {@link ResolveAssessVO}
     * </pre></code>
     * 
     * @param id sysId of the Assessor
     * @param request HttpServletRequest
     * @param response HttpServletResponse
     * @return {@link ResponseDTO} - Reponse Data Transfer Object with 'data' of type {@link ResolveAssessVO}
     * @throws ServletException
     * @throws IOException
     */
    @RequestMapping(value = "/assessor/get", method = { RequestMethod.POST }, headers = "Accept=application/json; charset=utf-8")
    @ResponseBody
    public ResponseDTO get(@RequestParam String id, HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException
    {
        ResponseDTO result = new ResponseDTO();
        String username = (String) request.getAttribute(Constants.HTTP_REQUEST_USERNAME);
        
        try
        {
            ResolveAssessVO vo = ServiceHibernate.findResolveAssess(id, null, username);
            result.setData(vo);
            result.setSuccess(true);
            
        }
        catch (Exception e)
        {
            Log.log.error("Error retrieving Assessor", e);
            result.setSuccess(false).setMessage("Error retrieving Assessor on get. See log for more info.");
        }

        return result;
    }

    /**
     * Delete the Assessor based on array of sysIds  
     * <p>
     * Parameters in the Request object: 
     * <code><pre>
     *  USERNAME - username of the user who is logged in to Resolve
     * </pre></code>
     *   
     * @param ids array of sysIds to be deleted
     * @param deleteAll true/false if want to delete all the records
     * @param validate true/false if wanted to validate before deleteing which Actiontask does these Assessors belong too.
     * @param request HttpServletRequest
     * @param response HttpServletResponse
     * @return {@link ResponseDTO} - Reponse Data Transfer Object with status as 'true' or 'false'
     * @throws ServletException
     * @throws IOException
     */
    @SuppressWarnings("unchecked")
    @RequestMapping(value = "/assessor/delete", method = { RequestMethod.POST }, headers = "Accept=application/json; charset=utf-8")
    @ResponseBody
    public ResponseDTO delete(@RequestParam String[] ids, @RequestParam("all") Boolean deleteAll, @RequestParam("validate") Boolean validate, HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException
    {
        ResponseDTO result = new ResponseDTO();
        String username = (String) request.getAttribute(Constants.HTTP_REQUEST_USERNAME);
        
        try
        {
            if (ids != null && ids.length > 0)
            {
                if (validate)
                {
                    //get the list of actiontask names to be displayed on the UI in the warning message
                    Set<String> assessorSysIds = new HashSet<String>(Arrays.asList(ids));
                    Set<String> actiontaskNames = ServiceHibernate.findActiontaskReferencesForAssessors(assessorSysIds, username);
                    if (actiontaskNames.size() > 0)
                    {
                        result.setRecords(new ArrayList<String>(actiontaskNames));
                    }
                    else
                    {
                        ServiceHibernate.deleteResolveAssessByIds(ids, deleteAll, username);
                    }
                }
                else
                {
                    ServiceHibernate.deleteResolveAssessByIds(ids, deleteAll, username);
                }
            }
            result.setSuccess(true);
        }
        catch (Exception e)
        {
            Log.log.error("Error deleting Assessor", e);
            result.setSuccess(false).setMessage("Error deleting Assessor. See log for more info.");
        }

        return result;
    }
    
    /**
     *  Saves/Update a Assessor. If 'sysId' is set, it will be update else it will create it. 
     * <p>
     * Parameters in the Request object: 
     * <code><pre>
     *  USERNAME - username of the user who is logged in to Resolve
     * </pre></code>
     * <p>
     * Return object in response 
     * <code><pre>
     *  'data' : {@link ResolveAssessVO}
     * </pre></code>
     *  
     * @param jsonProperty JSONObject/JSON having data of type {@link ResolveAssessVO}
     * @param request HttpServletRequest
     * @param response HttpServletResponse
     * @return {@link ResponseDTO} - Reponse Data Transfer Object with 'data' having the created/updated Assessor VO
     * @throws ServletException
     * @throws IOException
     */
    @RequestMapping(value = "/assessor/save", method = { RequestMethod.POST }, headers = "Accept=application/json; charset=utf-8")
    @ResponseBody
    public ResponseDTO save(@RequestBody JSONObject jsonProperty, HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException
    {
      //using jackson to deserialize the json
        String json = jsonProperty.toString();
        ResolveAssessVO entity = new ObjectMapper().readValue(json, ResolveAssessVO.class);
        
        //convert json to VO - spring does not work as the VO are not 100% java bean
//        JsonConfig jsonConfig = new JsonConfig();
//        jsonConfig.setRootClass(ResolveAssessVO.class);
//        ResolveAssessVO entity = (ResolveAssessVO) JSONSerializer.toJava(jsonProperty, jsonConfig);
        
        ResponseDTO result = new ResponseDTO();
        String username = (String) request.getAttribute(Constants.HTTP_REQUEST_USERNAME);
        
        try
        {
            ResolveAssessVO vo = ServiceHibernate.saveResolveAssess(entity, username);
            result.setSuccess(true).setData(vo);
        }
        catch (Exception e)
        {
            Log.log.error("Error saving the Assessor", e);
            result.setSuccess(false).setMessage("Error saving the Assessor. See log for more info.");
        }

        return result;
    }
}
