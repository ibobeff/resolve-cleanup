/******************************************************************************
* (C) Copyright 2014
*
* Resolve Systems, LLC
*
* All rights reserved
* This software is distributed under license and may not be used, copied,
* modified, or distributed without the express written permission of
* Resolve Systems, LLC.
*
******************************************************************************/

package com.resolve.rsview.controller;

import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

import com.resolve.rsview.main.RSContext;
import com.resolve.services.constants.ConstantValues;
import com.resolve.services.constants.WikiAction;
import com.resolve.util.Log;
import com.resolve.wiki.api.WebApi;


@Controller
public class ImpexController extends GenericController
{
	
	/**
	 * 
	 * This is to import a module
	 * 
	 * eg. - http://localhost:8080/resolve/service/wiki/impex/import?module=documentation
	 * 		 http://localhost:8080/resolve/service/wiki/impex/import?impexFile=true  <== this is UPLOAD url for the import.zip file
	 * 
	 * @param namespace
	 * @param docname
	 * @param request
	 * @param response
	 * @return
	 * @throws ServletException
	 * @throws IOException
	 */
	@RequestMapping(value = ConstantValues.URL_IMPEX_IMPORT, method = {RequestMethod.GET, RequestMethod.POST} )
	public ModelAndView importModule(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException
	{
	
//		ControllerUtil.setRequestParameters(namespace, docname, WikiAction.impex, request);
		//prepare the generic params hash map
		Map<String, Object> params = new HashMap<String, Object>();
		params.put(ConstantValues.WIKI_HTTP_REQUEST, request);
		params.put(ConstantValues.WIKI_HTTP_RESPONSE, response);
		params.put(ConstantValues.WIKI_ACTION_KEY, WikiAction.impex);

		Map<String, Object> result = new HashMap<String, Object>();
		String impexFile = request.getParameter(ConstantValues.IMPORT_EXPORT_FILE_UPLOAD);
		try
		{
			WebApi webApi = (WebApi) RSContext.appContext.getBean(ConstantValues.WIKI_SPRINGID_FOR_WEBAPI);
			
//			if(StringUtils.isEmpty(impexFile))
//			{
//			    //import the file without showing manifest...request is coming from a url
//			    webApi.importModule(params);
//			}
//			else
//			{
//			    //upload the file but do not import it ..request is coming from a upload form
//			    webApi.uploadImportFile(params);
//			}
		}
		catch (Exception e)
		{
			Log.log.error(e.getMessage(), e);
			result.put(ConstantValues.ERROR_EXCEPTION_KEY, e.getMessage());
		}
		
		return null;

	}//importModule

	/**
	 * This is to export the module
	 * 
	 * eg. http://localhost:8080/resolve/service/wiki/impex/export?module=export1&tags=system.software
	 * 
	 * @param request
	 * @param response
	 * @return
	 * @throws ServletException
	 * @throws IOException
	 */
	@RequestMapping(value = ConstantValues.URL_IMPEX_EXPORT, method = {RequestMethod.GET} )
	public ModelAndView exportModule(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException
	{
	
//		ControllerUtil.setRequestParameters(namespace, docname, WikiAction.impex, request);
		//prepare the generic params hash map
		Map<String, Object> params = new HashMap<String, Object>();
		params.put(ConstantValues.WIKI_HTTP_REQUEST, request);
		params.put(ConstantValues.WIKI_HTTP_RESPONSE, response);
		params.put(ConstantValues.WIKI_ACTION_KEY, WikiAction.impex);

		Map<String, String> result = new HashMap<String, String>();
//		try
//		{
//			WebApi webApi = (WebApi) RSContext.appContext.getBean(ConstantValues.WIKI_SPRINGID_FOR_WEBAPI);
//			result = webApi.exportModule(params);
//		}
//		catch (Exception e)
//		{
//			Log.log.error(e.getMessage(), e);
//			result.put(ConstantValues.ERROR_EXCEPTION_KEY, e.getMessage());
//		}
		
		return null;

	}//importModule
	
}
