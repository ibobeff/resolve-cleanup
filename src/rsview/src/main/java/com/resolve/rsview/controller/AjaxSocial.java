/******************************************************************************
* (C) Copyright 2014
*
* Resolve Systems, LLC
*
* All rights reserved
* This software is distributed under license and may not be used, copied,
* modified, or distributed without the express written permission of
* Resolve Systems, LLC.
*
******************************************************************************/

package com.resolve.rsview.controller;

import java.awt.Graphics2D;
import java.awt.Image;
import java.awt.RenderingHints;
import java.awt.image.BufferedImage;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Set;

import javax.imageio.ImageIO;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.fileupload.FileItem;
import org.apache.commons.fileupload.FileItemFactory;
import org.apache.commons.fileupload.disk.DiskFileItemFactory;
import org.apache.commons.fileupload.servlet.ServletFileUpload;
import org.springframework.stereotype.Controller;
import org.springframework.stereotype.Service;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.resolve.dto.UploadFileDTO;
import com.resolve.rsview.main.Metric;
import com.resolve.search.SearchException;
import com.resolve.search.model.SocialPostResponse;
import com.resolve.search.model.Tag;
import com.resolve.search.social.SocialIndexAPI;
import com.resolve.services.ServiceHibernate;
import com.resolve.services.ServiceSocial;
import com.resolve.services.graph.social.SocialCompConversionUtil;
import com.resolve.services.graph.social.model.ComponentNotification;
import com.resolve.services.graph.social.model.RSComponent;
import com.resolve.services.graph.social.model.component.User;
import com.resolve.services.graph.social.model.message.UserGlobalNotificationContainerType;
import com.resolve.services.hibernate.util.SocialAdminUtil;
import com.resolve.services.hibernate.util.UserUtils;
import com.resolve.services.hibernate.util.vo.ComboboxModelDTO;
import com.resolve.services.hibernate.vo.ResolveTagVO;
import com.resolve.services.hibernate.vo.SocialPostAttachmentVO;
import com.resolve.services.hibernate.vo.UsersVO;
import com.resolve.services.interfaces.SocialDTO;
import com.resolve.services.vo.QueryDTO;
import com.resolve.services.vo.QueryFilter;
import com.resolve.services.vo.ResponseDTO;
import com.resolve.services.vo.UserStatDTO;
import com.resolve.services.vo.social.NodeType;
import com.resolve.services.vo.social.SocialForumDTO;
import com.resolve.services.vo.social.SocialProcessDTO;
import com.resolve.services.vo.social.SocialTeamDTO;
import com.resolve.social.SocialGenericActionsUtil;
import com.resolve.social.SocialUtil;
import com.resolve.util.Constants;
import com.resolve.util.Log;
import com.resolve.util.StringUtils;
import com.resolve.util.StringUtils.WhiteList;

/**
 * Controller to handle Resolve's Social related activities.
 *
 */
@Controller
@Service
public class AjaxSocial extends GenericController
{
	private static final String ERROR_ACCEPTING_JOIN_REQUEST = "Error accepting the join request";
	private static final String ERROR_GETTING_GLOBAL_NOTIFICATION = "Error getting global notification";
	private static final String ERROR_GETTING_LIST_OF_USERS = "Error getting list of users following a stream";
	private static final String ERROR_GETTING_MENTIONS = "Error getting mentions";
	private static final String ERROR_GETTING_SPECIFIC_NOTIFICATION = "Error getting specific notification";
	private static final String ERROR_GETTING_TAGS = "Error getting tags";
	private static final String ERROR_RETRIEVING_ATTACHMENT_LIST = "Error retrieving attachment list";
	private static final String ERROR_RETRIEVING_GET_STREAM = "Error retrieving getStream";
	private static final String ERROR_RETRIEVING_LIST_ACTION_TASK_USER_STREAMS = "Error retrieving listActionTaskUserStreams";
	private static final String ERROR_RETRIEVING_LIST_ALL_STREAMS = "Error retrieving listAllStreams";
	private static final String ERROR_RETRIEVING_LIST_COMPONENT_TYPES = "Error retrieving listComponentTypes";
	private static final String ERROR_RETRIEVING_LIST_DOCUMENT_USER_STREAMS = "Error retrieving listDocumentUserStreams";
	private static final String ERROR_RETRIEVING_LIST_LIKES = "Error retrieving listLikes";
	private static final String ERROR_RETRIEVING_LIST_POSTS_FOR_STREAM_ID = "Error retrieving listPosts for streamId: %s";
	private static final String ERROR_RETRIEVING_LIST_SMALL_USER_STREAMS = "Error retrieving listSmallUserStreams";
	private static final String ERROR_RETRIEVING_LIST_USER_STREAMS = "Error retrieving listUserStreams";
	private static final String ERROR_SETTING_PINNED_STATE = "Error setting pinned state";
	private static final String ERROR_SETTING_STREAM_FOLLOW_STATE = "Error setting stream follow state";
	private static final String ERROR_UPDATING_GLOBAL_NOTIFICATION = "Error updating global notification";
	private static final String ERROR_UPLOADING_IMAGE = "Error uploading image";
    private static final String ERROR_UPDATING_SPECIFIC_NOTIFICATION = "Error updating specific notification";

	public static final String SOCIAL_DOWNLOAD_URL = "/post/download";
    public static final String USERID_PARAM = "userid";
    public static final String FILEID_PARAM = "fileid";
    public static final String QUERY = "query";
    public static final String ENTITY_TYPE = "type";
    public static final String SHOWPOST = "showpost";

    /**
     * List All the streams logged in user could work with. E.g Follow
     * 
     * @param query : {@link QueryDTO}, represents the query, pagination, sorting, grouping for the UI
     * @param request {@link HttpServletRequest} </code> containing <code>USERNAME</code>: a username of the user who is logged in to Resolve
     * @param response {@link HttpServletResponse}
     * @return {@link ResponseDTO} with success true, records set to the list of {@link RSComponent} and total as total record count if everything goes smooth.
     *          Otherwise success will be false and message will contain an error message.
     * @throws ServletException
     * @throws IOException
     */
    @RequestMapping(value = "/social/listAllStreams", method = { RequestMethod.POST, RequestMethod.GET })
    @ResponseBody
    public ResponseDTO listAllStreams(@ModelAttribute QueryDTO query, HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException
    {
        ResponseDTO result = new ResponseDTO();
        //        String username = (String) request.getAttribute(Constants.HTTP_REQUEST_USERNAME);
        User user = getSocialUser(request);

        try
        {
//            boolean isWorksheet = false;
//            List<QueryFilter> filters = query.getFilterItems();
//            if(filters != null)
//            {
//                for(QueryFilter filter : filters)
//                {
//                    if(filter.getValue().equalsIgnoreCase(NodeType.WORKSHEET.name()))
//                    {
//                        isWorksheet = true;
//                        break;
//                    }
//                }
//            }
            
            //get all the worksheets 
//            if(isWorksheet)
//            {
//                result = new AjaxWorksheet().listWorksheets(query, request, response);
//            }
//            else
//            {
                result = ServiceSocial.getAllStreams(user, query);    
//            }

            result.setSuccess(true);
        }
        catch (Exception e)
        {
            Log.log.error(ERROR_RETRIEVING_LIST_ALL_STREAMS, e);
            result.setSuccess(false).setMessage(ERROR_RETRIEVING_LIST_ALL_STREAMS);
        }

        return result;
    }

    /**
     * List the streams logged in user is following. If user is following any of the below mentioned stream(s):<br>
     * ActionTask, DecisionTree, Document, Forum, Namespace, Process, RSS, Runbook, Team, User or Worksheet, the entire list is returned.
     * 
     * Note: This can be very expensive call depending on # of streams followed by user. 
     * User by default follows every Document created by self.
     * If all or majority of Documents in system are created by single user then this will be an expensive
     * call in terms of time and resources and hence should be used only when required.
     *  
     * @param query : {@link QueryDTO} NOT USED in this API
     * @param request {@link HttpServletRequest} </code> containing <code>USERNAME</code>: a username of the user who is logged in to Resolve
     * @return {@link ResponseDTO} with success true, records set to the list of {@link RSComponent} and total as total record count if everything goes smooth.
     *          Otherwise success will be false and message will contain an error message.
     * @throws ServletException
     * @throws IOException
     */
    @RequestMapping(value = "/social/listUserStreamsOld", method = { RequestMethod.POST, RequestMethod.GET })
    @ResponseBody
    public ResponseDTO<Object> listUserStreams(@ModelAttribute QueryDTO query, HttpServletRequest request) throws ServletException, IOException
    {
        long startTime = System.currentTimeMillis();
        ResponseDTO<Object> result = new ResponseDTO<Object>();
        //      String username = (String) request.getAttribute(Constants.HTTP_REQUEST_USERNAME);
        User user = getSocialUser(request);
        if (user != null && !user.getName().equals("resolve.maint"))
        {
            try
            {
                List<Object> data = new ArrayList<Object>();
                long startTime1 = System.currentTimeMillis();
                List<RSComponent> userStreams = ServiceSocial.getUserStreams(user.getSys_id(), user.getName(), true);
                Log.log.debug("listUserStreams: Get User Streams Time : " + (System.currentTimeMillis() - startTime1) + 
                              " msec, returned " + userStreams.size() + " streams");
                data.addAll(userStreams);
                //this is the consolidation of all display categories (all, inbox, blog etc)
                startTime1 = System.currentTimeMillis();
                data.add(ServiceSocial.getUnreadPostCount(query, user, userStreams));
                Log.log.debug("listUserStreams: Unread Posts Count for " + userStreams.size() + " streams Time : " +
                              (System.currentTimeMillis() - startTime1) + " msec");
                result.setSuccess(true).setRecords(data);
            }
            catch (Exception e)
            {
                Log.log.error(ERROR_RETRIEVING_LIST_USER_STREAMS, e);
                result.setSuccess(false).setMessage(ERROR_RETRIEVING_LIST_USER_STREAMS);
            }
        }
        
        Log.log.debug("listUserStreams Total Time : " + (System.currentTimeMillis() - startTime));
        
        return result;
    }

    /**
     * List the small streams, logged in user is following except possible large ones like Document and ActionTask. 
     * If user is following any of the below mentioned stream(s):<br>
     * DecisionTree, Forum, Namespace, Process, RSS, Runbook, Team, User or Worksheet, the entire list is returned.
     * 
     * @param query : {@link QueryDTO} NOT USED in this API
     * @param request {@link HttpServletRequest} </code> containing <code>USERNAME</code>: a username of the user who is logged in to Resolve
     * @return {@link ResponseDTO} with success true, records set to the list of {@link RSComponent} and total as total record count if everything goes smooth.
     *          Otherwise success will be false and message will contain an error message.
     * @throws ServletException
     * @throws IOException
     */
    @RequestMapping(value = "/social/listUserStreams", method = { RequestMethod.POST, RequestMethod.GET })
    //@RequestMapping(value = "/social/listSmallUserStreams", method = { RequestMethod.POST, RequestMethod.GET })
    @ResponseBody
    public ResponseDTO<Object> listSmallUserStreams(@ModelAttribute QueryDTO query, HttpServletRequest request) throws ServletException, IOException
    {
        ResponseDTO<Object> result = new ResponseDTO<Object>();
        //      String username = (String) request.getAttribute(Constants.HTTP_REQUEST_USERNAME);
        User user = getSocialUser(request);
        if (user != null && !user.getName().equals("resolve.maint"))
        {
            try
            {
                List<Object> data = new ArrayList<Object>();
                List<RSComponent> userStreams = ServiceSocial.getSmallUserStreams(user.getSys_id(), user.getName(), true);
                data.addAll(userStreams);
                //this is the consolidation of all display categories (all, inbox, blog etc)
                data.add(ServiceSocial.getUnreadPostCount(query, user, userStreams));
                result.setSuccess(true).setRecords(data);
            }
            catch (Exception e)
            {
                Log.log.error(ERROR_RETRIEVING_LIST_SMALL_USER_STREAMS, e);
                result.setSuccess(false).setMessage(ERROR_RETRIEVING_LIST_SMALL_USER_STREAMS);
            }
        }

        return result;
    }
    
    /**
     * List Document stream, logged in user is following. 
     * It contains both directly as well as indirectly (from PROCESS(es) followed by user) followed documents.
     * Indirctly followed documents are first in the list.
     * 
     * @param query : {@link QueryDTO} USED for pagination.
     * @param request {@link HttpServletRequest} </code> containing <code>USERNAME</code>: a username of the user who is logged in to Resolve
     * @return {@link ResponseDTO} with success true, records set to the list of {@link RSComponent} and total as total record count if everything goes smooth.
     *          Otherwise success will be false and message will contain an error message.
     * @throws ServletException
     * @throws IOException
     */
    @SuppressWarnings("unchecked")
    //@RequestMapping(value = "/social/listUserStreams", method = { RequestMethod.POST, RequestMethod.GET })
    @RequestMapping(value = "/social/listDocumentUserStreams", method = { RequestMethod.POST, RequestMethod.GET })
    @ResponseBody
    public ResponseDTO<Object> listDocumentUserStreams(@ModelAttribute QueryDTO query, HttpServletRequest request) throws ServletException, IOException
    {
        long startTime = System.currentTimeMillis();
        ResponseDTO<Object> result = new ResponseDTO<Object>();
        User user = getSocialUser(request);
        if (user != null && !user.getName().equals("resolve.maint"))
        {
            /*query.setStart(50);
            query.setLimit(10);
            query.setPage(6);*/
            try
            {
                // Always query All Indirectly followed Documents by user - hopefully this is small
                List<Object> data = new ArrayList<Object>();
                long startTime1 = System.currentTimeMillis();
                List<RSComponent> indirectDocUserStream = ServiceSocial.getIndirectDocumentUserStream(user.getSys_id(), user.getName(), true);
                
                Log.log.debug("listDocumentUserStreams: Indirectly Followed Document Time : " + (System.currentTimeMillis() - startTime1) + 
                                " msec returned " + indirectDocUserStream.size() + " DOCUMENT streams");
                
                List<RSComponent> directPaginatedDocUserStream = new ArrayList<RSComponent>();
                
                int start = query.getStart() - indirectDocUserStream.size();
                int limit = query.getLimit() + (start < 0 ? start : 0);
                
                Log.log.debug("listDocumentUserStreams: start = " + start + ", limit = " + limit + " for directly followed DOCUMENT streams");
                
                // Query (paginated) Directly followed documents only if required.
                if (limit > 0)
                {
                    startTime1 = System.currentTimeMillis();
                    directPaginatedDocUserStream = ServiceSocial.getDirectDocumentUserStream(user.getSys_id(), user.getName(), false, start < 0 ? 0 : start, limit);
                    Log.log.debug("listDocumentUserStreams: Directly followed Document Streams Time : " + (System.currentTimeMillis() - startTime1) + 
                                    " msec returned " + directPaginatedDocUserStream.size() + " DOCUMENT streams");
                }
                
                List<RSComponent> consolidatedDocUserStream = new ArrayList<RSComponent>();
                
                if (start < 0 )
                {
                    consolidatedDocUserStream.addAll(indirectDocUserStream.subList(query.getStart(), 
                                                                                   (((query.getStart() + query.getLimit()) < indirectDocUserStream.size()) ? 
                                                                                    query.getStart() + query.getLimit() : indirectDocUserStream.size())));
                    Log.log.debug("Added " + consolidatedDocUserStream.size() + " Indirectly Followed Documnt streams"); 
                }
                
                if (!directPaginatedDocUserStream.isEmpty())
                {
                    consolidatedDocUserStream.addAll(directPaginatedDocUserStream);
                    Log.log.debug("Added " + directPaginatedDocUserStream.size() + " Directly Followed ActionTask streams"); 
                }
                
                startTime1 = System.currentTimeMillis();
                long totalDocumentCnt = indirectDocUserStream.size() + ServiceSocial.getDirectlyFollowedDocumentCount(user.getSys_id(), user.getName());
                Log.log.debug("listDocumentUserStreams: Directly followed Document Count Time : " + (System.currentTimeMillis() - startTime1) + 
                              " msec, Total DOCUMENT streams " + totalDocumentCnt);
                
                data.addAll(consolidatedDocUserStream);
                //this is the consolidation of all display categories (all, inbox, blog etc)
                data.add(ServiceSocial.getUnreadPostCount(query, user, consolidatedDocUserStream));
                result.setSuccess(true).setRecords(data).setTotal(totalDocumentCnt);
            }
            catch (Exception e)
            {
                Log.log.error(ERROR_RETRIEVING_LIST_DOCUMENT_USER_STREAMS, e);
                result.setSuccess(false).setMessage(ERROR_RETRIEVING_LIST_DOCUMENT_USER_STREAMS);
            }
        }

        Log.log.debug("listDocumentUserStreams Total Time : " + (System.currentTimeMillis() - startTime) + " msec");
        
        return result;
    }
    
    /**
     * List Action Task stream, logged in user is following. 
     * It contains both directly as well as indirectly (from PROCESS(es) followed by user) followed action tasks.
     * Indirctly followed action tasks are first in the list.
     * 
     * @param query : {@link QueryDTO} USED for pagination.
     * @param request {@link HttpServletRequest} </code> containing <code>USERNAME</code>: a username of the user who is logged in to Resolve
     * @return {@link ResponseDTO} with success true, records set to the list of {@link RSComponent} and total as total record count if everything goes smooth.
     *          Otherwise success will be false and message will contain an error message.
     * @throws ServletException
     * @throws IOException
     */
    @SuppressWarnings("unchecked")
    //@RequestMapping(value = "/social/listUserStreams", method = { RequestMethod.POST, RequestMethod.GET })
    @RequestMapping(value = "/social/listActionTaskUserStreams", method = { RequestMethod.POST, RequestMethod.GET })
    @ResponseBody
    public ResponseDTO<Object> listActionTaskUserStreams(@ModelAttribute QueryDTO query, HttpServletRequest request) throws ServletException, IOException
    {
        long startTime = System.currentTimeMillis();
        ResponseDTO<Object> result = new ResponseDTO<Object>();
        User user = getSocialUser(request);
        if (user != null && !user.getName().equals("resolve.maint"))
        {
            /*query.setStart(50);
            query.setLimit(10);
            query.setPage(6);*/
            try
            {
                // Always query All Indirectly followed Documents by user - hopefully this is small
                List<Object> data = new ArrayList<Object>();
                long startTime1 = System.currentTimeMillis();
                List<RSComponent> indirectActionTaskUserStream = ServiceSocial.getIndirectActionTaskUserStream(user.getSys_id(), user.getName(), true);
                
                Log.log.debug("listActionTaskUserStreams: Indirectly Followed Action Task Streams Time : " + (System.currentTimeMillis() - startTime1) + 
                              " msec returned " + indirectActionTaskUserStream.size() + " ACTIONTASK streams");
                
                List<RSComponent> directPaginatedActionTaskUserStream = new ArrayList<RSComponent>();
                
                int start = query.getStart() - indirectActionTaskUserStream.size();
                int limit = query.getLimit() + (start < 0 ? start : 0);
                
                Log.log.debug("listActionTaskUserStreams: start = " + start + ", limit = " + limit + " for directly followed ACTIONTASK streams");
                
                // Query (paginated) Directly followed action tasks only if required.
                if (limit > 0)
                {
                    startTime1 = System.currentTimeMillis();
                    directPaginatedActionTaskUserStream = ServiceSocial.getDirectActionTaskUserStream(user.getSys_id(), user.getName(), false, start < 0 ? 0 : start, limit);
                    Log.log.debug("listActionTaskUserStreams: Directly followed Action Task Streams Time : " + (System.currentTimeMillis() - startTime1) + 
                                  " msec returned " + directPaginatedActionTaskUserStream.size() + " ACTIONTASK streams");
                }
                
                List<RSComponent> consolidatedATUserStream = new ArrayList<RSComponent>();
                
                if (start < 0 )
                {
                    consolidatedATUserStream.addAll(indirectActionTaskUserStream.subList(query.getStart(), 
                                                                                         (((query.getStart() + query.getLimit()) < indirectActionTaskUserStream.size()) ? 
                                                                                          query.getStart() + query.getLimit() : indirectActionTaskUserStream.size())));
                    Log.log.debug("Added " + consolidatedATUserStream.size() + " Indirectly Followed ActionTask streams");  
                }
                
                if (!directPaginatedActionTaskUserStream.isEmpty())
                {
                    consolidatedATUserStream.addAll(directPaginatedActionTaskUserStream);
                    Log.log.debug("Added " + directPaginatedActionTaskUserStream.size() + " Directly Followed ActionTask streams");
                }
                
                startTime1 = System.currentTimeMillis();
                long totalActionTaskCnt = indirectActionTaskUserStream.size() + ServiceSocial.getDirectlyFollowedActionTaskCount(user.getSys_id(), user.getName());
                Log.log.debug("listActionTaskUserStreams: Directly followed Action Tasks Count Time : " + (System.currentTimeMillis() - startTime1) + 
                              " msec, Total ACTIONTASK streams " + totalActionTaskCnt);
                
                data.addAll(consolidatedATUserStream);
                //this is the consolidation of all display categories (all, inbox, blog etc)
                startTime1 = System.currentTimeMillis();
                data.add(ServiceSocial.getUnreadPostCount(query, user, consolidatedATUserStream));
                Log.log.debug("listActionTaskUserStreams: Unread Posts Count Time for " + consolidatedATUserStream.size() + 
                              " streams : " + (System.currentTimeMillis() - startTime1) + " msec");
                
                result.setSuccess(true).setRecords(data).setTotal(totalActionTaskCnt);
            }
            catch (Exception e)
            {
                Log.log.error(ERROR_RETRIEVING_LIST_ACTION_TASK_USER_STREAMS, e);
                result.setSuccess(false).setMessage(ERROR_RETRIEVING_LIST_ACTION_TASK_USER_STREAMS);
            }
        }

        Log.log.debug("listActionTaskUserStreams Total Time : " + (System.currentTimeMillis() - startTime) + " msec");
        
        return result;
    }
    
    /**
     * This method returns all the unique user's streams. The listUserStreams method adds different 
     * data such as duplicate Wiki document with a suffix like -RUNBOOK, -DOCUMENT etc.
     * 
     * @param query : {@link QueryDTO} NOT USED in this API
     * @param request : {@link HttpServletRequest} </code> containing <code>USERNAME</code>: a username of the user who is logged in to Resolve
     * @return {@link ResponseDTO} with success true, records set to the list of {@link RSComponent} and total as total record count if everything goes smooth.
     *          Otherwise success will be false and message will contain an error message.
     * @throws ServletException
     * @throws IOException
     */
    @RequestMapping(value = "/social/getUserStreams", method = { RequestMethod.POST, RequestMethod.GET })
    @ResponseBody
    public ResponseDTO<Object> getUserStreams(@ModelAttribute QueryDTO query, HttpServletRequest request) throws ServletException, IOException
    {

        ResponseDTO<Object> result = new ResponseDTO<Object>();
        User user = getSocialUser(request);
        
        try
        {
            Set<Object> data = new LinkedHashSet<Object>();
            List<RSComponent> userStreams = ServiceSocial.getUserStreams(user.getSys_id(), user.getName(), true);
            for(RSComponent component : userStreams)
            {
                //this is just because listUserStreams sets the id with a -DOCUMENT or -RUNBOOK suffix.
                //plus that makes the userStream list has doplicate component. 
                component.setId(component.getSys_id());
                data.add(component);
            }
            List<Object> streams = new ArrayList<Object>();
            streams.addAll(data);
            result.setSuccess(true).setRecords(streams);
        }
        catch (Exception e)
        {
            Log.log.error(ERROR_RETRIEVING_LIST_USER_STREAMS, e);
            result.setSuccess(false).setMessage(ERROR_RETRIEVING_LIST_USER_STREAMS);
        }

        return result;
    }

    /**
     * Return a stream
     * 
     * @param id : String sysId of the stream which needs to be retrieved.
     * @param request : {@link HttpServletRequest} </code> containing <code>USERNAME</code>: a username of the user who is logged in to Resolve
     * @return {@link ResponseDTO} with success true and data set to the {@link RSComponent} if everything goes smooth.
     *          Otherwise success will be false and message will contain an error message.
     * @throws ServletException
     * @throws IOException
     */
    @RequestMapping(value = "/social/getStream", method = { RequestMethod.POST, RequestMethod.GET })
    @ResponseBody
    public ResponseDTO getStream(@RequestParam String id, HttpServletRequest request) throws ServletException, IOException
    {
        ResponseDTO result = new ResponseDTO();
        //        User user = getSocialUser(request);
        String username = (String) request.getAttribute(Constants.HTTP_REQUEST_USERNAME);
        try
        {
            RSComponent data = ServiceSocial.getComponentById(parseId(id), username);
            result.setSuccess(true).setData(data);
        }
        catch (Exception e)
        {
            Log.log.error(ERROR_RETRIEVING_GET_STREAM, e);
            result.setSuccess(false).setMessage(ERROR_RETRIEVING_GET_STREAM);
        }

        return result;

    }

    /**
     * List all the posts belong to a stream.
     * 
     * @param query : {@link QueryDTO} <code>query</code> contains search, filter, sorting and pagination information for the UI grid.
     * @param streamId : String representing sysId of the stream. If <code>streamType</code> is left blank, it could have either 
     *      <code>all, activity, inbox, starred, blog, sent or system. </code>
     * @param streamType : String representing stream type. If empty, DEFAULT is assumed. Otherwise could have either
     *      <code>USER, ACTIONTASK, PROCESS, TEAM, FORUM, DOCUMENT, RSS, RUNBOOK, WORKSHEET, NAMESPACE or DECISIONTREE. </code> 
     * @param postId : String respresenting post id. If specified, only that post will be returned.
     * @param unreadOnly : Boolean flag representing whether to return unread posts only. 
     * @param request : {@link HttpServletRequest} </code> containing <code>USERNAME</code>: a username of the user who is logged in to Resolve
     * @return {@link ResponseDTO} with success true, records set to the list of {@link SocialPostResponse} and total as total record count if everything goes smooth.
     *          Otherwise success will be false and message will contain an error message.
     * @throws ServletException
     * @throws IOException
     */
    @RequestMapping(value = "/social/listPosts", method = { RequestMethod.POST, RequestMethod.GET })
    @ResponseBody
    public ResponseDTO<SocialPostResponse> listPosts(@ModelAttribute QueryDTO query, 
    												 @RequestParam(defaultValue = "") String streamId, 
    												 @RequestParam(defaultValue = "") String streamType, 
    												 @RequestParam(defaultValue = "") String postId, 
    												 @RequestParam boolean unreadOnly, 
    												 HttpServletRequest request) throws ServletException, IOException {
    	long startTime = System.currentTimeMillis();
   	 	long startTimeInstance = startTime;
        ResponseDTO<SocialPostResponse> result = new ResponseDTO<SocialPostResponse>();
        boolean includeComments = true;
        List<SocialPostResponse> records = new ArrayList<SocialPostResponse>();
        String username = (String) request.getAttribute(Constants.HTTP_REQUEST_USERNAME);
        User user = getSocialUser(request);
        
        if (Log.log.isDebugEnabled()) {
	        Log.log.debug(String.format("Time taken to get Social User %s: %d msec", username, 
	        							(System.currentTimeMillis() - startTimeInstance)));
	        startTimeInstance = System.currentTimeMillis();
        }
        
        if(StringUtils.isEmpty(streamType)) {
            streamType = "DEFAULT";
        }

        try {
            if (!username.equalsIgnoreCase("resolve.maint")) {
                if (StringUtils.isNotEmpty(postId)) {
                    records.add(ServiceSocial.getPostById(user, postId));
                    result.setRecords(records);
                    result.setTotal(1);
                } else {
                    //the result has the total already set in searchPosts call.
                    result = ServiceSocial.searchPosts(query, parseId(streamId), NodeType.valueOf(streamType.toUpperCase()), 
                    								   includeComments, unreadOnly, user);
                    
                    if (Log.log.isDebugEnabled()) {
	                    Log.log.debug(String.format("Time taken to search posts for stream id %s of stream type %s, " +
	                    							"include comments %b, unread only %b for user %s: %d msec", streamId, 
	                    							streamType, includeComments, unreadOnly, username, 
	                    							(System.currentTimeMillis() - startTimeInstance)));
                    }
                }
            }
            
            result.setSuccess(true);
        } catch (Exception e) {
        	String msg = String.format(ERROR_RETRIEVING_LIST_POSTS_FOR_STREAM_ID, streamId);
            Log.log.error(msg, e);
            result.setSuccess(false).setMessage(msg);
        }
        
        if (Log.log.isDebugEnabled()) {
	        Log.log.debug(String.format("Total time taken to list posts for stream id %s of stream type %s for user %s: " +
	        							"%d msec", streamId, streamType, username, 
	        							(System.currentTimeMillis() - startTime)));
        }
        
        return result;
    }

    /**
     * List all users who liked the Post specified by <code>id</code>
     * 
     * @param query : {@link QueryDTO} NOT USED here.
     * @param id : String representing post id.
     * @param request : {@link HttpServletRequest} </code> containing <code>USERNAME</code>: a username of the user who is logged in to Resolve
     * @return {@link ResponseDTO} with success true and data set to {@link RSComponent} if everything goes smooth.
     *          Otherwise success will be false and message will contain an error message.
     * @throws ServletException
     * @throws IOException
     */
    @RequestMapping(value = "/social/listLikes", method = { RequestMethod.POST, RequestMethod.GET })
    @ResponseBody
    public ResponseDTO<RSComponent> listLikes(@ModelAttribute QueryDTO query, @RequestParam String id, HttpServletRequest request) throws ServletException, IOException
    {
        ResponseDTO<RSComponent> result = new ResponseDTO<RSComponent>();
        String username = (String) request.getAttribute(Constants.HTTP_REQUEST_USERNAME);
        //        User user = getSocialUser(request);
        List<RSComponent> data = null;
        try
        {
            if (StringUtils.isNotBlank(id))
            {
                data = ServiceSocial.getLikes(parseId(id), username);
                if (data != null)
                {
                    for (RSComponent comp : data)
                    {
                        String uname = comp.getName();
                        if (StringUtils.isNotBlank(uname) && uname.equalsIgnoreCase(username))
                        {
                            comp.setCurrentUser(true);
                        }
                    }
                }
            }

            result.setSuccess(true).setRecords(data);
            //          result.setTotal(total);
        }
        catch (Exception e)
        {
            Log.log.error(ERROR_RETRIEVING_LIST_LIKES, e);
            result.setSuccess(false).setMessage(ERROR_RETRIEVING_LIST_LIKES);
        }

        return result;

    }

    /**
     * List all available component types.
     * 
     * @param query : {@link QueryDTO} NOT USED here.
     * @param request : {@link HttpServletRequest} </code> containing <code>USERNAME</code>: a username of the user who is logged in to Resolve
     * @return {@link ResponseDTO} with success true and data set to {@link ComboboxModelDTO} if everything goes smooth.
     *          Otherwise success will be false and message will contain an error message.
     * @throws ServletException
     * @throws IOException
     */
    @RequestMapping(value = "/social/listComponentTypes", method = { RequestMethod.POST, RequestMethod.GET })
    @ResponseBody
    public ResponseDTO listComponentTypes(@ModelAttribute QueryDTO query, HttpServletRequest request) throws ServletException, IOException
    {
        ResponseDTO result = new ResponseDTO();

        try
        {
            List<ComboboxModelDTO> data = ServiceSocial.getListOfAllComponentTypes();
            result.setSuccess(true).setRecords(data);

        }
        catch (Exception e)
        {
            Log.log.error(ERROR_RETRIEVING_LIST_COMPONENT_TYPES, e);
            result.setSuccess(false).setMessage(ERROR_RETRIEVING_LIST_COMPONENT_TYPES);
        }

        return result;
    }

    /**
     * Submit a post.
     * 
     * @param streams : String array of sysIds to which post to be submitted.
     * @param streamType : String representing the stream type. Refer to {@link #listPosts(QueryDTO, String, String, String, boolean, HttpServletRequest) } for different stream types.
     * @param subject : String representing subject of the post.
     * @param content : String representing content of the post.
     * @param postId : If specified, it's a comment submission to that post specified by this post id. 
     * @param request : {@link HttpServletRequest} </code> containing <code>USERNAME</code>: a username of the user who is logged in to Resolve
     * @return {@link ResponseDTO} with success true if the post is submitted successfully. Otherwise success will be false and message will contain an error message.
     * @throws ServletException
     * @throws IOException
     */
    @SuppressWarnings("rawtypes")
    @RequestMapping(value = "/social/submit", method = { RequestMethod.POST })
    @ResponseBody
    //    public JsonResponse submit(@RequestParam List<String> targetsSysId, @RequestParam(defaultValue = "") String subject, @RequestParam String content, @RequestParam(defaultValue="") String postId, HttpServletRequest request) throws ServletException, IOException
    public ResponseDTO submit(@RequestParam List<String> streams, @RequestParam(defaultValue = "") String streamType, @RequestParam(defaultValue = "") String subject, @RequestParam String content, @RequestParam(defaultValue = "") String postId, @RequestParam(defaultValue = "") String orgId, @RequestParam(defaultValue = "") String orgName, HttpServletRequest request) throws ServletException, IOException
    {
        try
        {
            User user = getSocialUser(request);
            String username = (String) request.getAttribute(Constants.HTTP_REQUEST_USERNAME);
            
            //If postId provided, then we're sending a comment, so retrieve the post and add a comment to it instead
            if (StringUtils.isNotBlank(postId))
            {
                //adding a comment to this Post
                SocialPostResponse post = ServiceSocial.getPostById(user, postId);
                if (post != null)
                {
                    String trimmedContent = StringUtils.htmlCleaner(content, true, WhiteList.BASICWITHIMAGES);
                    if(StringUtils.isBlank(trimmedContent))
                    {
                       throw new Exception("Blank comment not allowed."); 
                    }
                    ServiceSocial.addComment(username, postId, content, true);
                }
            }
            else
            {
                String trimmedContent = StringUtils.htmlCleaner(content, true, WhiteList.BASICWITHIMAGES);

                if(StringUtils.isNotBlank(trimmedContent))
                {
                    if("worksheet".equalsIgnoreCase(streamType))
                    {
                        List<String> tmpStreams = new ArrayList<String>();
                        for (String id : streams)
                        {
                            RSComponent comp = ServiceSocial.createWorksheetNode(parseId(id), username, orgId, orgName);
                            if (comp != null)
                            {
                                tmpStreams.add(comp.getSys_id());
                            }
                            else
                            {
                                //this can be other components like User, Document, etc
                                tmpStreams.add(id);
                            }
                        }//end of for loop
                        
                        if(tmpStreams.size() > 0)
                        {
                            ServiceSocial.post(subject, content, username, tmpStreams, true);
                        }
                    }
                    else
                    {
                        if(streams.size() > 0)
                        {
                            ServiceSocial.post(subject, content, username, parseIds(streams), true);
                        }
                    }
                }
                else
                {
                    throw new Exception("Blank content not allowed."); 
                }
            }

            return new ResponseDTO().setSuccess(true);
        }
        catch (Exception e)
        {
            return new ResponseDTO().setSuccess(false).setMessage("Error posting content");
        }
    }

    /**
     * Set post as read.
     * 
     * @param id : String sysId of the post 
     * @param read : Boolean flag specifying whether mark the post read (true) or unread (false).
     * @param request : {@link HttpServletRequest} </code> containing <code>USERNAME</code>: a username of the user who is logged in to Resolve
     * @return {@link ResponseDTO} with success true if everything goes smooth. Otherwise success will be false and message will contain an error message.
     * @throws ServletException
     * @throws IOException
     */
    @SuppressWarnings("rawtypes")
    @RequestMapping(value = "/social/setRead", method = { RequestMethod.POST })
    @ResponseBody
    public ResponseDTO setRead(@RequestParam String id, @RequestParam boolean read, HttpServletRequest request) throws ServletException, IOException
    {
        try
        {
            User user = getSocialUser(request);
            ServiceSocial.setRead(user, parseId(id), read);
            return new ResponseDTO().setSuccess(true);
        }
        catch (Exception e)
        {
            return new ResponseDTO().setSuccess(false).setMessage("Error reading content");
        }
    }

    /**
     * Set lock on the stream.
     * 
     * @param id : String sysId of the stream on which lock is to be set.
     * @param lock : Boolean flag if true, lock will be set, unlocked otherwise.
     * @param request : {@link HttpServletRequest} </code> containing <code>USERNAME</code>: a username of the user who is logged in to Resolve
     * @return {@link ResponseDTO} with success true if everything goes smooth. Otherwise success will be false and message will contain an error message.
     * @throws ServletException
     * @throws IOException
     */
    @RequestMapping(value = "/social/setLockStream", method = { RequestMethod.POST })
    @ResponseBody
    public ResponseDTO setStreamLock(@RequestParam String id, @RequestParam boolean lock, HttpServletRequest request) throws ServletException, IOException
    {
        try
        {
            User user = getSocialUser(request);
            ServiceSocial.setStreamLockUnlock(user, parseId(id), lock);

            return new ResponseDTO().setSuccess(true);
        }
        catch (Exception e)
        {
            return new ResponseDTO().setSuccess(false).setMessage("Error locking/unlocking content");
        }
    }

    /**
     * Set lock on the post
     * 
     * @param id : String sysId of the post on which lock is to be set.
     * @param lock : Boolean flag if true, lock will be set, unlocked otherwise.
     * @param request : {@link HttpServletRequest} </code> containing <code>USERNAME</code>: a username of the user who is logged in to Resolve
     * @return {@link ResponseDTO} with success true if everything goes smooth. Otherwise success will be false and message will contain an error message.
     * @throws ServletException
     * @throws IOException
     */
    @RequestMapping(value = "/social/setLockPost", method = { RequestMethod.POST })
    @ResponseBody
    public ResponseDTO setPostLock(@RequestParam String id, @RequestParam boolean lock, HttpServletRequest request) throws ServletException, IOException
    {
        try
        {
            User user = getSocialUser(request);
            ServiceSocial.setPostLock(user, parseId(id), lock);

            return new ResponseDTO().setSuccess(true);
        }
        catch (Exception e)
        {
            return new ResponseDTO().setSuccess(false).setMessage("Error during postlock");
        }
    }

    /**
     * Move the post
     * 
     * @param postId : String sysId of the post which will be moved.
     * @param targets : String srray of the target stream sysIds.
     * @param request : {@link HttpServletRequest} </code> containing <code>USERNAME</code>: a username of the user who is logged in to Resolve
     * @return {@link ResponseDTO} with success true if everything goes smooth. Otherwise success will be false and message will contain an error message.
     * @throws ServletException
     * @throws IOException
     */
    @RequestMapping(value = "/social/move", method = { RequestMethod.POST })
    @ResponseBody
    public ResponseDTO movePost(@RequestParam String postId, @RequestParam List<String> targets, HttpServletRequest request) throws ServletException, IOException
    {
        try
        {
            User user = getSocialUser(request);
            ServiceSocial.movePost(postId, user, parseIds(targets));
            return new ResponseDTO().setSuccess(true);
        }
        catch (Exception e)
        {
            return new ResponseDTO().setSuccess(false).setMessage("Error moving post");
        }
    }

    /**
     * Delete the post.
     *  
     * @param id : String sysId of the post to be deleted.
     * @param request : {@link HttpServletRequest} </code> containing <code>USERNAME</code>: a username of the user who is logged in to Resolve
     * @return {@link ResponseDTO} with success true if everything goes smooth. Otherwise success will be false and message will contain an error message.
     * @throws ServletException
     * @throws IOException
     */
    @RequestMapping(value = "/social/delete", method = { RequestMethod.POST })
    @ResponseBody
    public ResponseDTO delete(@RequestParam String id, HttpServletRequest request) throws ServletException, IOException
    {
        try
        {
            User user = getSocialUser(request);
            ServiceSocial.deletePost(user, parseId(id));
            return new ResponseDTO().setSuccess(true);
        }
        catch (Exception e)
        {
            return new ResponseDTO().setSuccess(false).setMessage("Error deleting post");
        }
    }

    /**
     * Delete comment.
     * 
     * @param id : String sysId of the comment to be deleted.
     * @param request : {@link HttpServletRequest} </code> containing <code>USERNAME</code>: a username of the user who is logged in to Resolve
     * @return {@link ResponseDTO} with success true if everything goes smooth. Otherwise success will be false and message will contain an error message.
     * @throws ServletException
     * @throws IOException
     */
    @RequestMapping(value = "/social/deleteComment", method = { RequestMethod.POST })
    @ResponseBody
    public ResponseDTO deleteComment(@RequestParam String id, HttpServletRequest request) throws ServletException, IOException
    {
        try
        {
            User user = getSocialUser(request);
            ServiceSocial.deleteComment(user, parseId(id));
            return new ResponseDTO().setSuccess(true);
        }
        catch (Exception e)
        {
            return new ResponseDTO().setSuccess(false).setMessage("Error deleting comment");
        }
    }

    /**
     * @deprecated
     * 
     * THis was not implemented from the beginning. Not sure if we want this 
     * 
     * 
     * @param id
     * @param request
     * @return
     * @throws ServletException
     * @throws IOException
     */
    @Deprecated
    @RequestMapping(value = "/social/undoDelete", method = { RequestMethod.POST })
    @ResponseBody
    public ResponseDTO undoDelete(@RequestParam String id, HttpServletRequest request) throws ServletException, IOException
    {
        try
        {
            User user = getSocialUser(request);
//            ServiceSocial.undoDeletePost(user, parseId(id));  //NOT IMPLEMENTED SINCE THE BEGINNING. NOT SURE IF WE NEED IT.

            return new ResponseDTO().setSuccess(true);
        }
        catch (Exception e)
        {
            return new ResponseDTO().setSuccess(false).setMessage("Error undeleting post");
        }
    }

    /**
     * Mark all posts as read of the stream.
     * 
     * @param id : String sysId the stream.
     * @param streamType : String stream type. Refer to {@link #listPosts(QueryDTO, String, String, String, boolean, HttpServletRequest) } for different stream types.
     * @param olderThanDate : String date specifying all the posts older than this date will be selected. E.g. <code>2015-05-30T00:57:00-07:00</code>
     * @param request : {@link HttpServletRequest} </code> containing <code>USERNAME</code>: a username of the user who is logged in to Resolve
     * @return {@link ResponseDTO} with success true and set message to 'Success.' if everything goes smooth. Otherwise success will be false and message will contain an error message.
     * @throws ServletException
     * @throws IOException
     */
    @RequestMapping(value = "/social/markStreamRead", method = { RequestMethod.POST })
    @ResponseBody
    public ResponseDTO<String> markStreamRead(@RequestParam String id, @RequestParam(defaultValue = "") String streamType, @RequestParam String olderThanDate, HttpServletRequest request) throws ServletException, IOException
    {
        try
        {
            String username = (String) request.getAttribute(Constants.HTTP_REQUEST_USERNAME);

            QueryDTO query = new QueryDTO();

            if(StringUtils.isNotEmpty(olderThanDate))
            {
                //DateTime date = DateManipulator.parseISODate(olderThanDate);
                //long dateInMS = date.getMillis();
                query.addFilterItem(new QueryFilter("date", olderThanDate, "sysCreatedOn", "lessThanOrEqual"));
            }
            //query.addFilterItem(new QueryFilter("bool", "false", "owners.read", "equals"));

            ServiceSocial.markPostAsReadByQuery(parseId(id), streamType, query, username, true);

            return new ResponseDTO<String>().setSuccess(true).setMessage("Success.");
        }
        catch (Exception e)
        {
            return new ResponseDTO<String>().setSuccess(false).setMessage("Error marking stream read");
        }
    }

    /**
     * Set link on the Post
     * 
     * @param id : String sysId of the post.
     * @param parentId : String sysId of the parent post. If provided, <code>id</code> represents sysId of a comment.
     * @param like : Boolean flag, if true, mark the port liked, remove the like otehrwise.
     * @param request : {@link HttpServletRequest} </code> containing <code>USERNAME</code>: a username of the user who is logged in to Resolve
     * @return {@link ResponseDTO} with success true if everything goes smooth. Otherwise success will be false and message will contain an error message.
     * @throws ServletException
     * @throws IOException
     */
    @RequestMapping(value = "/social/setLike", method = { RequestMethod.POST })
    @ResponseBody
    public ResponseDTO setLike(@RequestParam String id, @RequestParam String parentId, @RequestParam boolean like, HttpServletRequest request) throws ServletException, IOException
    {
        try
        {
            User user = getSocialUser(request);

            //If postId provided, then we're sending a comment, so retrieve the post and add a comment to it instead
            if (StringUtils.isNotBlank(id))
            {
            	setLike(id, parentId, user, like);
                ServiceSocial.setRead(user, id, true);
            }
            return new ResponseDTO().setSuccess(true);
        }
        catch (Exception e)
        {
            return new ResponseDTO().setSuccess(false).setMessage("Error setting like");
        }
    }

    /**
     * Mark the Post starred
     * 
     * @param id : String sysId of the post.
     * @param starred : Boolean flag, if true, mark the post starred, remove the start otherwise.
     * @param request : {@link HttpServletRequest} </code> containing <code>USERNAME</code>: a username of the user who is logged in to Resolve
     * @return {@link ResponseDTO} with success true if everything goes smooth. Otherwise success will be false and message will contain an error message.
     * @throws ServletException
     * @throws IOException
     */
    @RequestMapping(value = "/social/setStarred", method = { RequestMethod.POST })
    @ResponseBody
    public ResponseDTO setStarred(@RequestParam String id, @RequestParam boolean starred, HttpServletRequest request) throws ServletException, IOException
    {
        try
        {
            User user = getSocialUser(request);
            ServiceSocial.setStarred(user, parseId(id), starred);
            return new ResponseDTO().setSuccess(true);
        }
        catch (Exception e)
        {
            return new ResponseDTO().setSuccess(false).setMessage("Error setting starred");
        }
    }

    /**
     * Set the Comment as Answer
     * 
     * @param id : String sysId of a comment.
     * @param answer : Boolean flag, if true, setting the comment as an answer.
     * @param request : {@link HttpServletRequest} </code> containing <code>USERNAME</code>: a username of the user who is logged in to Resolve
     * @return {@link ResponseDTO} with success true if everything goes smooth. Otherwise success will be false and message will contain an error message.
     * @throws ServletException
     * @throws IOException
     */
    @RequestMapping(value = "/social/setAnswer", method = { RequestMethod.POST })
    @ResponseBody
    public ResponseDTO setAnswer(@RequestParam String id, @RequestParam boolean answer, HttpServletRequest request) throws ServletException, IOException
    {
        try
        {
            User user = getSocialUser(request);
            ServiceSocial.setAnswer(user, parseId(id), answer);
            return new ResponseDTO().setSuccess(true);
        }
        catch (Exception e)
        {
            return new ResponseDTO().setSuccess(false).setMessage("Error setting answer");
        }
    }

    //    @RequestMapping(value = "/social/listDocs", method = { RequestMethod.POST, RequestMethod.GET })
    //    @ResponseBody
    //    public JsonResponse listDocs(@ModelAttribute QueryDTO query, HttpServletRequest request) throws ServletException, IOException
    //    {
    //        try
    //        {
    //            User user = getSocialUser(request);
    //            // Get the list of documents in the system matching the query
    //            return new JsonResponse().setSuccess(true);
    //        }
    //        catch (Exception e)
    //        {
    //            return new JsonResponse().setSuccess(false).setMessage(e.getMessage());
    //        }
    //    }
    //
    //    @RequestMapping(value = "/social/listActionTasks", method = { RequestMethod.POST, RequestMethod.GET })
    //    @ResponseBody
    //    public JsonResponse listActionTasks(@ModelAttribute QueryDTO query, HttpServletRequest request) throws ServletException, IOException
    //    {
    //        try
    //        {
    //            User user = getSocialUser(request);
    //            // Get the list of actionTasks in the system matching the query
    //            return new JsonResponse().setSuccess(true);
    //        }
    //        catch (Exception e)
    //        {
    //            return new JsonResponse().setSuccess(false).setMessage(e.getMessage());
    //        }
    //    }
    //
    //    @RequestMapping(value = "/social/listForums", method = { RequestMethod.POST, RequestMethod.GET })
    //    @ResponseBody
    //    public JsonResponse listForums(@ModelAttribute QueryDTO query, HttpServletRequest request) throws ServletException, IOException
    //    {
    //        try
    //        {
    //            User user = getSocialUser(request);
    //            // Get the list of forums in the system matching the query
    //            return new JsonResponse().setSuccess(true);
    //        }
    //        catch (Exception e)
    //        {
    //            return new JsonResponse().setSuccess(false).setMessage(e.getMessage());
    //        }
    //    }
    //
    //    @RequestMapping(value = "/social/listRss", method = { RequestMethod.POST, RequestMethod.GET })
    //    @ResponseBody
    //    public JsonResponse listRss(@ModelAttribute QueryDTO query, HttpServletRequest request) throws ServletException, IOException
    //    {
    //        try
    //        {
    //            User user = getSocialUser(request);
    //            // Get the list of rss in the system matching the query
    //            return new JsonResponse().setSuccess(true);
    //        }
    //        catch (Exception e)
    //        {
    //            return new JsonResponse().setSuccess(false).setMessage(e.getMessage());
    //        }
    //    }

    /**
     * List all the social attachments of a logged in user
     * 
     * @param request : {@link HttpServletRequest} </code> containing <code>USERNAME</code>: a username of the user who is logged in to Resolve
     * @return {@link ResponseDTO} with success true and records set to the list of {@link SocialPostAttachmentVO} if everything goes smooth.
     *          Otherwise success will be false and message will contain an error message.
     * @throws ServletException
     * @throws IOException
     */
    @RequestMapping(value = "/social/attachmentList", method = { RequestMethod.POST }, headers = "Accept=application/json; charset=utf-8")
    @ResponseBody
    public ResponseDTO attachmentList(HttpServletRequest request) throws ServletException, IOException
    {
        ResponseDTO result = new ResponseDTO();
        String username = (String) request.getAttribute(Constants.HTTP_REQUEST_USERNAME);
        
        try
        {
            List<SocialPostAttachmentVO> attachments = ServiceHibernate.getAllPostAttachments(username);
            
            // If we want to retain what the user has already posted, we could do that... otherwise just keep it blank.
            result.setRecords(attachments).setSuccess(true);
        }
        catch (Exception e)
        {
            Log.log.error(ERROR_RETRIEVING_ATTACHMENT_LIST, e);
            result.setSuccess(false).setMessage(ERROR_RETRIEVING_ATTACHMENT_LIST);
        }

        return result;
    }

    /**
     * Upload an attachment
     * 
     * @param request : {@link HttpServletRequest} </code> containing <code>USERNAME</code>: a username of the user who is logged in to Resolve. Also contain attachment payload.
     * @return {@link ResponseDTO} with success true and data set to {@link UploadFileDTO} if everything goes smooth.
     *          Otherwise success will be false and message will contain an error message.
     * @throws ServletException
     * @throws IOException
     */
    @RequestMapping(value = "/social/upload", method = { RequestMethod.POST }, headers = "Accept=application/json; charset=utf-8")
    @ResponseBody
    public ResponseDTO upload(HttpServletRequest request) throws ServletException, IOException
    {
        User user = getSocialUser(request);
        ResponseDTO result = new ResponseDTO();
        try
        {
            SocialPostAttachmentVO pa = processUpload(request);
            pa = ServiceHibernate.saveSocialPostAttachment(pa, user.getName());

            UploadFileDTO upload = new UploadFileDTO(pa);

            result.setSuccess(true).setData(upload);
        }
        catch (Exception e)
        {
            Log.log.error(ERROR_UPLOADING_IMAGE, e);
            result.setSuccess(false).setMessage(ERROR_UPLOADING_IMAGE);
        }

        return result;
    }
    
    /**
     * Download an attachment (an image) specified by attachment sysId, <code> id </code>
     * 
     * @param id : String sysId of an attachment
     * @param preview : Boolean flag if set preview of the image is downloaded, complete image otherwise.
     * @param request {@link HttpServletRequest} </code> containing <code>USERNAME</code> of the user who is logged in to Resolve
     * @param response {@link HttpServletResponse} with the image comtent payload.
     * 
     * @throws ServletException
     * @throws IOException
     */
    @RequestMapping(value = "/social/download", method = { RequestMethod.POST, RequestMethod.GET }, headers = "Accept=application/json; charset=utf-8")
    @ResponseBody
    public void download(@RequestParam String id, @RequestParam(defaultValue = "false") Boolean preview, HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException
    {
        String username = (String) request.getAttribute(Constants.HTTP_REQUEST_USERNAME);

        try
        {
            SocialPostAttachmentVO pa = ServiceHibernate.findSocialPostAttachmentById(id, username);
            if (pa != null)
            {
                response.addHeader("Content-Disposition", (pa.getUType().toLowerCase().contains("pdf") ? "inline" : "attachment") + "; filename=\"" + pa.getUFilename() + "\"");
                String contentType = "application/octet-stream";
                if (StringUtils.isNotEmpty(pa.getUType())) contentType = pa.getUType();
                response.setContentType(contentType + ";  name=\"" + pa.getUFilename() + "\"");
                if (preview)
                {
                    BufferedImage buffer = ImageIO.read(new ByteArrayInputStream(pa.getUContent()));
                    // Create a Graphics object to do the "drawing"
                    Image img = buffer.getScaledInstance(200, -1, Image.SCALE_DEFAULT);
                    BufferedImage bufferResize = new BufferedImage(img.getWidth(null), img.getHeight(null), BufferedImage.TYPE_INT_RGB);
                    Graphics2D g2d = (Graphics2D) bufferResize.getGraphics();
                    g2d.setRenderingHint(RenderingHints.KEY_INTERPOLATION, RenderingHints.VALUE_INTERPOLATION_BICUBIC);
                    // Draw the resized image
                    g2d.drawImage(img, 0, 0, img.getWidth(null), img.getHeight(null), null);
                    g2d.dispose();
                    // Now our buffered image is ready
                    // Encode it as a JPEG
                    ByteArrayOutputStream encoderOutputStream = new ByteArrayOutputStream();
                    ImageIO.write(bufferResize, "png", encoderOutputStream);
                    //                    JPEGImageEncoder encoder = JPEGCodec.createJPEGEncoder(encoderOutputStream);
                    //                    encoder.encode(bufferResize);
                    byte[] resizedImageByteArray = encoderOutputStream.toByteArray();
                    response.setContentLength(resizedImageByteArray.length);
                    response.getOutputStream().write(resizedImageByteArray);
                }
                else
                {
                    if (pa.getUSize() > 0)
                    {
                        response.setContentLength(pa.getUSize());
                    }
                    response.getOutputStream().write(pa.getUContent());
                }
            }
        }
        catch (Exception e)
        {
            Log.log.error("Error downloading image", e);
        }
    }

    /**
     * Set stream as pinned / unpinned
     * 
     * @param id : String sysId of the stream which needs to be pinned / unpinned.
     * @param pinned : Boolean flag it set, the stream is pinned, unpinned otherwise.
     * @param request {@link HttpServletRequest} </code> containing <code>USERNAME</code> of the user who is logged in to Resolve
     * @return {@link ResponseDTO} with success true if everything goes smooth. Otherwise success will be false and message will contain an error message.
     * @throws ServletException
     * @throws IOException
     */
    @RequestMapping(value = "/social/setPinned", method = { RequestMethod.POST }, headers = "Accept=application/json; charset=utf-8")
    @ResponseBody
    public ResponseDTO setPinned(@RequestParam String id, @RequestParam boolean pinned, HttpServletRequest request) throws ServletException, IOException
    {
        User user = getSocialUser(request);
        ResponseDTO result = new ResponseDTO();
        try
        {
            // Set the pinned state of the stream for the user
            ServiceSocial.setPinUnPin(user, parseId(id), pinned);
            result.setSuccess(true);
        }
        catch (Exception e)
        {
            Log.log.error(ERROR_SETTING_PINNED_STATE, e);
            result.setSuccess(false).setMessage(ERROR_SETTING_PINNED_STATE);
        }

        return result;
    }

    /**
     * Follow / Unfollow streams.
     * 
     * @param ids : String array containing sysId of different sttreams.
     * @param follow : Boolean flag if set the streams will be followed, unfollowed otehrwise.
     * @param streamType : String representing stream type. Refer to {@link #listPosts(QueryDTO, String, String, String, boolean, HttpServletRequest) } for different stream types.
     * @param request {@link HttpServletRequest} </code> containing <code>USERNAME</code> of the user who is logged in to Resolve
     * @return {@link ResponseDTO} with success true if everything goes smooth. Otherwise success will be false and message will contain an error message.
     * @throws ServletException
     * @throws IOException
     */
    @SuppressWarnings("rawtypes")
    @RequestMapping(value = "/social/setFollowStreams", method = { RequestMethod.POST }, headers = "Accept=application/json; charset=utf-8")
    @ResponseBody
    public ResponseDTO setFollowStreams(@RequestParam List<String> ids, @RequestParam boolean follow, @RequestParam(defaultValue="") String streamType, @RequestParam(defaultValue="", value="RESOLVE.ORG_ID") String orgId, @RequestParam(defaultValue="", value="RESOLVE.ORG_NAME") String orgName, HttpServletRequest request) throws ServletException, IOException
    {
        User user = getSocialUser(request);
        
        ResponseDTO result = new ResponseDTO();
        try
        {
            List<String> streamIds = parseIds(ids);
            
            //if its worksheet, than create the nodes
            if("worksheet".equals(streamType))
            {
                for(String id : streamIds)
                {
                    //create the WS node
                    ServiceSocial.createWorksheetNode(parseId(id), user.getName(), orgId, orgName);
                }
            }
            
            ServiceSocial.setFollowStreams(user, streamIds, follow);
            result.setSuccess(true);
        }
        catch (Exception e)
        {
            Log.log.error(ERROR_SETTING_STREAM_FOLLOW_STATE, e);
            result.setSuccess(false).setMessage(ERROR_SETTING_STREAM_FOLLOW_STATE);
        }

        return result;
    }

    /**
     * Follow / Unfollow specified streams <code> streamsIds</code> by users specified by <code> ids</code>
     *  
     * @param streamIds : List of Strings containing sysId of different sttreams.
     * @param ids - List of users who will be following the streams
     * @param follow : Boolean flag if set the streams will be followed, unfollowed otehrwise.
     * @param request {@link HttpServletRequest} </code> containing <code>USERNAME</code> of the user who is logged in to Resolve
     * @return {@link ResponseDTO} with success true if everything goes smooth. Otherwise success will be false and message will contain an error message.
     * @throws ServletException
     * @throws IOException
     */
    @SuppressWarnings("rawtypes")
    @RequestMapping(value = "/social/setFollowers", method = { RequestMethod.POST }, headers = "Accept=application/json; charset=utf-8")
    @ResponseBody
    public ResponseDTO setFollowStreams(@RequestParam List<String> streamIds, @RequestParam List<String> ids, @RequestParam boolean follow, @RequestParam(defaultValue="", value="RESOLVE.ORG_ID") String orgId, @RequestParam(defaultValue="", value="RESOLVE.ORG_NAME") String orgName, HttpServletRequest request) throws ServletException, IOException
    {
        //User user = getSocialUser(request);
        String username = (String) request.getAttribute(Constants.HTTP_REQUEST_USERNAME);
        
        ResponseDTO result = new ResponseDTO();
        String streamType = request.getParameter("streamType");
        try
        {
            List<String> parsedStreamIds = parseIds(streamIds);
            //if its worksheet, than create the nodes
            if("worksheet".equals(streamType))
            {
                for(String id : parsedStreamIds)
                {
                    ServiceSocial.createWorksheetNode(parseId(id), username, orgId, orgName);
                }
            }
            
            ServiceSocial.setFollowStreams(parseIds(ids), parsedStreamIds, follow);
            result.setSuccess(true);
        }
        catch (Exception e)
        {
            Log.log.error(ERROR_SETTING_STREAM_FOLLOW_STATE, e);
            result.setSuccess(false).setMessage(ERROR_SETTING_STREAM_FOLLOW_STATE);
        }

        return result;
    }

    /**
     * List mentions (usernames) when user types '@' in the post
     * 
     * @param query : String containing partial name.
     * @param request {@link HttpServletRequest} </code> containing <code>USERNAME</code> of the user who is logged in to Resolve
     * @return {@link ResponseDTO} with success true and records set to the list of {@link ComboboxModelDTO} if everything goes smooth.
     *          Otherwise success will be false and message will contain an error message.
     * @throws ServletException
     * @throws IOException
     */
    @RequestMapping(value = "/social/listMentions", method = { RequestMethod.POST, RequestMethod.GET }, headers = "Accept=application/json; charset=utf-8")
    @ResponseBody
    public ResponseDTO listMentions(@RequestParam String query, HttpServletRequest request) throws ServletException, IOException
    {
        //        User user = getSocialUser(request);
        ResponseDTO result = new ResponseDTO();
        try
        {
            if (StringUtils.isNotEmpty(query))
            {
                query = query.replace('@', ' ').trim();
            }

            List<ComboboxModelDTO> values = ServiceHibernate.getAllUsersForMentions(query);
            result.setSuccess(true).setRecords(values);
        }
        catch (Exception e)
        {
            Log.log.error(ERROR_GETTING_MENTIONS, e);
            result.setSuccess(false).setMessage(ERROR_GETTING_MENTIONS);
        }

        return result;
    }

    /**
     * List all the tags when user types '#' in the post
     * 
     * @param query : String containing partial tag name.
     * @param request {@link HttpServletRequest} </code> containing <code>USERNAME</code> of the user who is logged in to Resolve
     * @param response {@link HttpServletResponse}
     * @return {@link ResponseDTO} with success true, records set to the list of {@link ResolveTagVO} and total set to the total number of records if everything goes smooth.
     *          Otherwise success will be false and message will contain an error message.
     * @throws ServletException
     * @throws IOException
     */
    @RequestMapping(value = "/social/listTags", method = { RequestMethod.POST, RequestMethod.GET }, headers = "Accept=application/json; charset=utf-8")
    @ResponseBody
    public ResponseDTO<Tag> listTags(@RequestParam String query, HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException
    {
        //        User user = getSocialUser(request);
        ResponseDTO<Tag> result = new ResponseDTO<Tag>();
        String username = (String) request.getAttribute(Constants.HTTP_REQUEST_USERNAME);
        try
        {
            if (StringUtils.isNotBlank(query) && query.startsWith("#"))
            {
                //get rid of the #
                query = query.substring(1);
            }

            QueryDTO queryDTO = new QueryDTO(0, 5);
            if(StringUtils.isNotBlank(query))
            {
                QueryFilter filter = new QueryFilter();
                filter.setField("UName");
                filter.setValue(query.toLowerCase());
                filter.setCondition("contains");
                filter.setType("auto");
                
                queryDTO.addFilterItem(filter);
            }
//            queryDTO.setSearchTerm(query);
            
//            result = QueryAPI.getHashtagSuggestions(queryDTO, username);
            result = new AjaxTag().listTags(queryDTO, request, response);
        }
        catch (Exception e)
        {
            Log.log.error(ERROR_GETTING_TAGS, e);
            result.setSuccess(false).setMessage(ERROR_GETTING_TAGS);
        }

        return result;
    }

    /**
     * List all the followers of a stream
     * 
     * @param streamId : String sysId of the stream
     * @param request {@link HttpServletRequest} </code> containing <code>USERNAME</code> of the user who is logged in to Resolve
     * @return {@link ResponseDTO} with success true, records set to the list of {@link User} if everything goes smooth.
     *          Otherwise success will be false and message will contain an error message.
     * @throws ServletException
     * @throws IOException
     */
    @RequestMapping(value = "/social/listFollowers", method = { RequestMethod.POST, RequestMethod.GET }, headers = "Accept=application/json; charset=utf-8")
    @ResponseBody
    public ResponseDTO listFollowers(@RequestParam String streamId, HttpServletRequest request) throws ServletException, IOException
    {
        ResponseDTO result = new ResponseDTO();
        try
        {
            //Get the list of people following the provided stream
            String username = (String) request.getAttribute(Constants.HTTP_REQUEST_USERNAME);
            List<User> users = ServiceSocial.getAllUsersFollowingThisStream(parseId(streamId), username);
            result.setRecords(users).setSuccess(true);
        }
        catch (Exception e)
        {
            Log.log.error(ERROR_GETTING_LIST_OF_USERS, e);
            result.setSuccess(false).setMessage(ERROR_GETTING_LIST_OF_USERS);
        }

        return result;
    }

    /**
     * List all the users following current logged in user
     * 
     * @param request {@link HttpServletRequest} </code> containing <code>USERNAME</code> of the user who is logged in to Resolve
     * @return {@link ResponseDTO} with success true, records set to the list of {@link User} if everything goes smooth.
     *          Otherwise success will be false and message will contain an error message.
     * @throws ServletException
     * @throws IOException
     */
    @RequestMapping(value = "/social/listFollowing", method = { RequestMethod.POST, RequestMethod.GET }, headers = "Accept=application/json; charset=utf-8")
    @ResponseBody
    public ResponseDTO listFollowing(HttpServletRequest request) throws ServletException, IOException
    {
        ResponseDTO result = new ResponseDTO();
        try
        {
            //Get the list of people following the provided stream
            User user = getSocialUser(request);

            //get the list of people who this user is following
            List<User> users = ServiceSocial.getListOfUsersThisUserIsFollowing(user);
            result.setRecords(users).setSuccess(true);
        }
        catch (Exception e)
        {
            Log.log.error(ERROR_GETTING_LIST_OF_USERS, e);
            result.setSuccess(false).setMessage(ERROR_GETTING_LIST_OF_USERS);
        }

        return result;
    }

    /**
     * For private containers, Owners of the container have to accept the users.
     * This is called when the Owner clicks Accept to the Requested Post that was in its Inbox and want to give access the requested user to join the
     * container.
     *
     * Looks up for the following parameters
     *      targetSysId --> sysId of Process/Team/Forum
     *      targetCompType --> Process/Team/Forum
     *      requestedUser --> username of the user requesting to join the container
     *
     * Actions:
     *      - add the user to the comp
     *      - send a Post to the Inbox of the user that he has been accepted
     *
     * @param request {@link HttpServletRequest} </code> containing <code>USERNAME</code> of the user who is logged in to Resolve
     * @param response {@link HttpServletResponse}
     * @return {@link ResponseDTO} with success true if everything goes smooth.
     *          Otherwise success will be false and message will contain an error message.
     * @throws ServletException
     * @throws IOException
     */
    @RequestMapping(value = "/social/join/accept", method = { RequestMethod.POST, RequestMethod.GET }, headers = "Accept=application/json; charset=utf-8")
    @ResponseBody
    public ResponseDTO acceptJoinRequest(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException
    {
        String username = (String) request.getAttribute(Constants.HTTP_REQUEST_USERNAME);
        String targetSysId = request.getParameter("targetSysId");
        String targetCompType = request.getParameter("targetCompType");
        String requestedUser = request.getParameter("requestedUser");

        ResponseDTO result = new ResponseDTO();
        try
        {
        	acceptUserToJoin(NodeType.valueOf(targetCompType), targetSysId, username, requestedUser);
            result.setSuccess(true);
        }
        catch (Exception e)
        {
            Log.log.error(ERROR_ACCEPTING_JOIN_REQUEST, e);
            result.setSuccess(false).setMessage(ERROR_ACCEPTING_JOIN_REQUEST);
        }

        return result;
    }

    /**
     * For private containers, Owners of the container when reject the user
     * This is called when the Owner clicks Reject to the Requested Post that was in its Inbox
     *
     * Looks up for the following parameters
     *      targetSysId --> sysId of Process/Team/Forum
     *      targetCompType --> Process/Team/Forum
     *      requestedUser --> username of the user requesting to join the container
     *
     * Actions:
     *      - send a Post to the Inbox of the user that he has been rejected
     *
     * @param request {@link HttpServletRequest} </code> containing <code>USERNAME</code> of the user who is logged in to Resolve
     * @param response {@link HttpServletResponse}
     * @return {@link ResponseDTO} with success true if everything goes smooth. Otherwise success will be false and message will contain an error message.
     * @throws ServletException
     * @throws IOException
     */
    @RequestMapping(value = "/social/join/reject", method = { RequestMethod.POST, RequestMethod.GET }, headers = "Accept=application/json; charset=utf-8")
    @ResponseBody
    public ResponseDTO rejectJoinRequest(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException
    {
        String username = (String) request.getAttribute(Constants.HTTP_REQUEST_USERNAME);
        String targetSysId = request.getParameter("targetSysId");
        String targetCompType = request.getParameter("targetCompType");
        String requestedUser = request.getParameter("requestedUser");

        ResponseDTO result = new ResponseDTO();
        try
        {
        	rejectUserToJoin(NodeType.valueOf(targetCompType), targetSysId, username, requestedUser);
            result.setSuccess(true);
        }
        catch (Exception e)
        {
            Log.log.error(ERROR_ACCEPTING_JOIN_REQUEST, e);
            result.setSuccess(false).setMessage(ERROR_ACCEPTING_JOIN_REQUEST);
        }

        return result;
    }

    /**
     * List users statistics
     * 
     * @param username : String user name of the user whose statistics is needed.
     * @return {@link ResponseDTO} with success true and records set to the list of {@link UserStatDTO} if everything goes smooth.
     *          Otherwise success will be false and message will contain an error message.
     * @throws ServletException
     * @throws IOException
     */
    @RequestMapping(value = "/social/userStats", method = { RequestMethod.POST, RequestMethod.GET }, headers = "Accept=application/json; charset=utf-8")
    @ResponseBody
    public ResponseDTO getUserStats(@RequestParam String username) throws ServletException, IOException
    {
        ResponseDTO result = new ResponseDTO();
        try
        {
            UserStatDTO stats = ServiceSocial.getUserStats(username, true, false);
            result.setSuccess(true).setData(stats);
        }
        catch (Exception e)
        {
            Log.log.error(ERROR_ACCEPTING_JOIN_REQUEST, e);
            result.setSuccess(false).setMessage(ERROR_ACCEPTING_JOIN_REQUEST);
        }

        return result;
    }
    
    /**
     * Get the global notification setting of the logged in user.
     * 
     * @param request {@link HttpServletRequest} </code> containing <code>USERNAME</code> of the user who is logged in to Resolve
     * @param response {@link HttpServletResponse}
     * @return {@link ResponseDTO} with success true and records set to the list of {@link ComponentNotification} if everything goes smooth.
     *          Otherwise success will be false and message will contain an error message.
     * @throws ServletException
     * @throws IOException
     */
    @RequestMapping(value = "/social/global/notify/get", method = { RequestMethod.POST, RequestMethod.GET }, headers = "Accept=application/json; charset=utf-8")
    @ResponseBody
    public ResponseDTO getGlobalNotification(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException
    {
        ResponseDTO result = new ResponseDTO();
        User user = getSocialUser(request);

        try
        {
            List<ComponentNotification> data = ServiceSocial.getGlobalNotificationsForUser(user);
            result.setSuccess(true).setRecords(data);
        }
        catch (Exception e)
        {
            Log.log.error(ERROR_GETTING_GLOBAL_NOTIFICATION, e);
            result.setSuccess(false).setMessage(ERROR_GETTING_GLOBAL_NOTIFICATION);
        }
        return result;
    }

    /**
     * Updates global notifications for the logged in user
     * 
     * @param selectedTypes - List of string mapped to enum {@link UserGlobalNotificationContainerType}
     * @param request {@link HttpServletRequest} </code> containing <code>USERNAME</code> of the user who is logged in to Resolve
     * @param response {@link HttpServletResponse}
     * @return {@link ResponseDTO} with success true if everything goes smooth. Otherwise success will be false and message will contain an error message.
     * @throws ServletException
     * @throws IOException
     */
    @RequestMapping(value = "/social/global/notify/update", method = { RequestMethod.POST, RequestMethod.GET }, headers = "Accept=application/json; charset=utf-8")
    @ResponseBody
    public ResponseDTO updateGlobalNotification(@RequestParam Set<String> selectedTypes, HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException
    {
        ResponseDTO result = new ResponseDTO();
        User user = getSocialUser(request);

        try
        {
            ServiceSocial.updateGlobalNotificationsForUser(user, selectedTypes);
            result.setSuccess(true);
        }
        catch (Exception e)
        {
            Log.log.error(ERROR_UPDATING_GLOBAL_NOTIFICATION, e);
            result.setSuccess(false).setMessage(ERROR_UPDATING_GLOBAL_NOTIFICATION);
        }
        return result;
    }
    
    
    /**
     * this api is used to update/set a specific Global type of a component (like Process, Team, etc) for a user. 
     * 
     * Values for the selectedType for each of the components will be:
     * ================================================================
     * Process --> PROCESS_DIGEST_EMAIL, PROCESS_FORWARD_EMAIL
     * Team --> TEAM_DIGEST_EMAIL, TEAM_FORWARD_EMAIL
     * Forum --> FORUM_DIGEST_EMAIL, FORUM_FORWARD_EMAIL
     * Rss --> RSS_DIGEST_EMAIL, RSS_FORWARD_EMAIL
     * Document --> DOCUMENT_DIGEST_EMAIL, DOCUMENT_FORWARD_EMAIL
     * Actiontask --> ACTIONTASK_DIGEST_EMAIL, ACTIONTASK_FORWARD_EMAIL
     * Worksheet --> WORKSHEET_DIGEST_EMAIL, WORKSHEET_FORWARD_EMAIL
     * User --> USER_DIGEST_EMAIL, USER_FORWARD_EMAIL
     * 
     * For eg. if the user wants to get email for all the Post for all the Processes it is following, than the values will be
     * selectedType =  "PROCESS_FORWARD_EMAIL"  <== mapped to UserGlobalNotificationContainerType.PROCESS_FORWARD_EMAIL
     * value = true
     * 
     * @param selectedType
     * @param value
     * @param request
     * @param response
     * @return
     * @throws ServletException
     * @throws IOException
     */
//    @RequestMapping(value = "/social/global/notify/updateType", method = { RequestMethod.POST, RequestMethod.GET }, headers = "Accept=application/json; charset=utf-8")
//    @ResponseBody
//    public ResponseDTO updateGlobalNotificationType(@RequestParam String selectedType, @RequestParam Boolean value, HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException
//    {
//        ResponseDTO result = new ResponseDTO();
//        User user = getSocialUser(request);
//
//        try
//        {
//            ServiceSocial.updateGlobalNotificationTypeForUser(user, UserGlobalNotificationContainerType.valueOf(selectedType), value);
//            result.setSuccess(true);
//        }
//        catch (Exception e)
//        {
//            Log.log.error("Error updating the global notification", e);
//            result.setSuccess(false).setMessage("Error:" + e.getMessage());
//        }
//        return result;
//    }

    /**
     * Get component notification {@link ComponentNotification} of a logged in user for a specified stream.
     * 
     * @param streamId : String sysId of the stream.
     * @param request {@link HttpServletRequest} </code> containing <code>USERNAME</code> of the user who is logged in to Resolve
     * @param response {@link HttpServletResponse}
     * @return {@link ResponseDTO} with success true and data set to {@link ComponentNotification} if everything goes smooth.
     *          Otherwise success will be false and message will contain an error message.
     * @throws ServletException
     * @throws IOException
     */

    @RequestMapping(value = "/social/specific/notify/get", method = { RequestMethod.POST, RequestMethod.GET }, headers = "Accept=application/json; charset=utf-8")
    @ResponseBody
    public ResponseDTO<ComponentNotification> getSpecificNotification(@RequestParam String streamId, HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException
    {
        ResponseDTO<ComponentNotification> result = new ResponseDTO<ComponentNotification>();
        User user = getSocialUser(request);

        try
        {
            ComponentNotification data = ServiceSocial.getSpecificNotificationFor(parseId(streamId), user);
            result.setSuccess(true).setData(data);
        }
        catch (Exception e)
        {
            Log.log.error(ERROR_GETTING_SPECIFIC_NOTIFICATION, e);
            result.setSuccess(false).setMessage(ERROR_GETTING_SPECIFIC_NOTIFICATION);
        }
        return result;
    }

    /**
     * This api is to set/update the value for a specific component type.
     * <p>
     * For eg, if user wants to receive email for all the Post for a Process 'P1', the values for this api will be
     * streamId = '8a9482e54185528801418568767a0009'  <== sysIds of Process P1, Incase its a Group 'Process', it will all the sysIds under that Process.
     * selectedType =  "PROCESS_FORWARD_EMAIL"  <== mapped to UserGlobalNotificationContainerType.PROCESS_FORWARD_EMAIL
     * value = true or false
     * <p>
     * If the user wants to set the conf for Inbox or System or All streams, use 'inbox' or 'system' or 'all' respectively for streamId
     * For Inbox --> USER_DIGEST_EMAIL, USER_FORWARD_EMAIL
     * For System --> USER_SYSTEM_DIGEST_EMAIL, USER_SYSTEM_FORWARD_EMAIL
     * For All --> USER_ALL_DIGEST_EMAIL, USER_ALL_FORWARD_EMAIL
     * 
     * @param streamIds - sysIds of the comp - for a Group selection, it will list of sysIds of the comps and for a specific one, it will be only 1 sysId.
     * @param selectedType - mapped to UserGlobalNotificationContainerType
     * @param value - true or false
     * @param request {@link HttpServletRequest} </code> containing <code>USERNAME</code> of the user who is logged in to Resolve
     * @param response {@link HttpServletResponse}
     * @return {@link ResponseDTO} with success true if everything goes smooth. Otherwise success will be false and message will contain an error message.
     * @throws ServletException
     * @throws IOException
     */

    @RequestMapping(value = "/social/specific/notify/updateType", method = { RequestMethod.POST, RequestMethod.GET }, headers = "Accept=application/json; charset=utf-8")
    @ResponseBody
    public ResponseDTO updateSpecificNotificationType(@RequestParam  Set<String> streamIds, @RequestParam String selectedType, @RequestParam Boolean value, HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException
    {
        ResponseDTO result = new ResponseDTO();
        User user = getSocialUser(request);

        try
        {
            if(streamIds != null && streamIds.size() > 0 && StringUtils.isNotBlank(selectedType))
            {
                
                if(selectedType.startsWith("DECISIONTREE") || selectedType.startsWith("RUNBOOK"))
                {
                    selectedType = selectedType.replace("DECISIONTREE", "DOCUMENT");
                    selectedType = selectedType.replace("RUNBOOK", "DOCUMENT");
                }
                
                for(String streamId : streamIds)
                {
                    try
                    {
                        ServiceSocial.updateSpecificNotificationTypeFor(streamId, user, UserGlobalNotificationContainerType.valueOf(selectedType), value);
                    }
                    catch (Exception e)
                    {
                        //ignore/log the exception and goto the next one.
                        Log.log.info("Issue with setting the notification of type " + selectedType);
                    }
                }
                result.setSuccess(true);
            }
        }
        catch (Exception e)
        {
            Log.log.error(ERROR_UPDATING_SPECIFIC_NOTIFICATION, e);
            result.setSuccess(false).setMessage(ERROR_UPDATING_SPECIFIC_NOTIFICATION);
        }
        return result;
    }

    //private apis
    //THE SETTER/GETTER WILL NOT WORK AS CONTROLLERS ARE SINGLETON. SO IN A MULTI-THREAD ENV, THIS WILL KEEP CHANGING BASED ON THE INCOMING REQUEST.
    //So making an api to return the User object
    private static User getSocialUser(HttpServletRequest request)
    {
        return SocialUtil.getSocialUser(request);
    }

    @SuppressWarnings("unchecked")
    private SocialPostAttachmentVO processUpload(HttpServletRequest request) throws Exception
    {
        SocialPostAttachmentVO file = new SocialPostAttachmentVO();

        boolean isMultipart = ServletFileUpload.isMultipartContent(request);
        if (isMultipart)
        {
            // if is an upload request, then proceed
            // Create a factory for disk-based file items
            FileItemFactory factory = new DiskFileItemFactory();

            // Create a new file upload handler
            ServletFileUpload upload = new ServletFileUpload(factory);

            // Parse the request
            List<FileItem> items = upload.parseRequest(request);

            // Process the uploaded items
            Iterator<FileItem> iter = items.iterator();
            while (iter.hasNext())
            {
                FileItem item = iter.next();
                if (!item.isFormField())
                {
                    String fileName = item.getName();
                    fileName = fileName.substring(fileName.lastIndexOf("\\") + 1);
                    long sizeInBytes = item.getSize();
                    InputStream uploadedStream = item.getInputStream();
                    byte[] data = new byte[(int) sizeInBytes];
                    uploadedStream.read(data);

                    file.setUContent(data);
                    file.setUFilename(fileName);
                    file.setUSize(data.length);
                    file.setUDisplayName(fileName);
                    file.setUType(item.getContentType());
                }
            }
        }

        return file;
    }

    private String parseId(String id)
    {
        if(StringUtils.isNotEmpty(id))
        {
            if(id.indexOf('-') > -1)
            {
                id = id.substring(0, id.indexOf('-'));
            }
        }

        return id;
    }

    private List<String> parseIds(List<String> ids)
    {
        List<String> result = new ArrayList<String>();

        if(ids != null && ids.size() > 0)
        {
            for(String id : ids)
            {
                result.add(parseId(id));
            }
        }

        return result;
    }
    
    private static void setLike(String id, String parentId, User user, boolean isLiked) throws SearchException
    {
        if(StringUtils.isBlank(parentId))
        {
            //this is a post
            SocialIndexAPI.setLikedPost(id, user.getName(), isLiked);
        }
        else
        {
            //this is comment
            SocialIndexAPI.setLikedComment(id, parentId, user.getName(), isLiked);
        }

        updateUserLikeCount(user);
    }
    
    private static void acceptUserToJoin(NodeType comptype, String sysId, String owner, 
    		String requestedUsername) throws Exception
    {
        UsersVO ownerVO = UserUtils.getUser(owner);
        Set<String> roles = UserUtils.getUserRoles(owner);
        
      if(comptype == NodeType.PROCESS)
      {
          SocialProcessDTO processDTO = SocialAdminUtil.getProcess(sysId, owner);

          //if the user who clicked the Accept button is the Owner or is an admin, than he can add the requested user to Process
          if(processDTO.getU_owner().equalsIgnoreCase(owner) || roles.contains("admin"))
          {
              //add the user to Process
              User requestedUser = SocialCompConversionUtil.getSocialUser(requestedUsername);
              
              List<String> ids = new ArrayList<String>();
              ids.add(sysId);
              ServiceSocial.setFollowStreams(requestedUser, ids, true);
              
              //send the Post
              SocialGenericActionsUtil.sendJoinConfirmPostForUser(NodeType.PROCESS, ownerVO, requestedUser, processDTO);
          }
      }
      else if(comptype == NodeType.TEAM)
        {
            SocialTeamDTO teamDTO = SocialAdminUtil.getTeam(sysId, owner);
            
            //if the user who clicked the Accept button is the Owner or is an admin, than he can add the requested user to Team
            if(teamDTO.getU_owner().equalsIgnoreCase(owner) || roles.contains("admin"))
            {
                //add the user to Team
                User requestedUser = SocialCompConversionUtil.getSocialUser(requestedUsername);

                List<String> ids = new ArrayList<String>();
                ids.add(sysId);
                ServiceSocial.setFollowStreams(requestedUser, ids, true);

                //send the Post
                SocialGenericActionsUtil.sendJoinConfirmPostForUser(NodeType.TEAM, ownerVO, requestedUser, teamDTO);
            }
        }
        else if(comptype == NodeType.FORUM)
        {
            SocialForumDTO forumDTO = SocialAdminUtil.getForum(sysId, owner);
            
            //if the user who clicked the Accept button is the Owner or is an admin, than he can add the requested user to Forum
            if(forumDTO.getU_owner().equalsIgnoreCase(owner) || roles.contains("admin"))
            {
                //add the user to Team
                User requestedUser = SocialCompConversionUtil.getSocialUser(requestedUsername);
                
                List<String> ids = new ArrayList<String>();
                ids.add(sysId);
                ServiceSocial.setFollowStreams(requestedUser, ids, true);
                
                //send the Post
                SocialGenericActionsUtil.sendJoinConfirmPostForUser(NodeType.FORUM, ownerVO, requestedUser, forumDTO);
            }
        }
    }
    
    private static void rejectUserToJoin(NodeType comptype, String sysId, String owner, String requestedUsername)
    {
        UsersVO ownerVO = UserUtils.getUser(owner);
        User requestedUser = SocialCompConversionUtil.getSocialUser(requestedUsername);
        SocialDTO socialDTO = null;
        
        if(comptype == NodeType.PROCESS)
        {
            socialDTO = SocialAdminUtil.getProcess(sysId, owner);
        }
        else if(comptype == NodeType.TEAM)
        {
            socialDTO = SocialAdminUtil.getTeam(sysId, owner);            
        }
        else if(comptype == NodeType.FORUM)
        {
            socialDTO = SocialAdminUtil.getForum(sysId, owner);
        }

        //send the Post
        SocialGenericActionsUtil.sendRejectConfirmPostForUser(NodeType.PROCESS, ownerVO, requestedUser, socialDTO);
    }
    
    private static void updateUserLikeCount(User user)
    {
        try
        {
            Metric.updateUserLikeCount(user.getSys_id());
        }
        catch(Exception exp)
        {
            Log.log.info(exp, exp);
        }
    }
}
