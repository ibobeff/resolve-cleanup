package com.resolve.rsview.sso.authenticator;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

public interface SSOAuthenticator
{
	public String SSO_AUTH_FAILURE_TO_CREATE_USER_AND_ASSOCIATE_GROUP_MESSAGE = 
														"%s SSO authentication failed to create user and assciate group. %s";
	
    public String getSSOType();

    public SSOAuthenticationDTO authenticate(HttpServletRequest request, HttpServletResponse response);

}
