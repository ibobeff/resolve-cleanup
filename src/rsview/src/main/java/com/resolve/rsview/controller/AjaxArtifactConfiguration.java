package com.resolve.rsview.controller;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.resolve.services.hibernate.util.ArtifactConfigurationUtil;
import com.resolve.services.hibernate.util.CEFUtil;
import com.resolve.services.hibernate.util.ConverterUtil;
import com.resolve.services.hibernate.util.CustomDictionaryUtil;
import com.resolve.services.hibernate.vo.ArtifactConfigurationVO;
import com.resolve.services.vo.ArtifactConfigurationDTO;
import com.resolve.services.vo.ResponseDTO;
import com.resolve.util.Log;

@Controller
@RequestMapping("/ac")
public class AjaxArtifactConfiguration extends GenericController {
	private static final String ERROR_RETRIEVING_DATA = "Error retrieving data";
	private static final String ERROR_NOT_FOUND_DATA = "Data not found";
	private static final String ERROR_SAVING_DATA = "Error saving data";
	private static final String ERROR_DELETING_DATA = "Error deleting data";
	private static final String SUCCESS_DELETING_DATA = "%s records deleted";

	@RequestMapping(method = RequestMethod.GET)
	@ResponseBody
	public ResponseDTO<List<ArtifactConfigurationDTO>> list() {
		ResponseDTO<List<ArtifactConfigurationDTO>> result = new ResponseDTO<>();
		try {
			List<ArtifactConfigurationVO> data = ArtifactConfigurationUtil.getAllArtifacts();
			List<ArtifactConfigurationDTO> dtoData = new ArrayList<>(data.size());
			for (ArtifactConfigurationVO vo : data) {
				dtoData.add(ConverterUtil.convertArtifactVOToDTO(vo));
			}

			result.setSuccess(true).setData(dtoData);
		} catch (Exception e) {
			Log.log.error(ERROR_RETRIEVING_DATA, e);
			result.setSuccess(false).setMessage(ERROR_RETRIEVING_DATA);
		}

		return result;
	}

	@RequestMapping(value = "/{name}", method = RequestMethod.GET)
	@ResponseBody
	public ResponseDTO<ArtifactConfigurationDTO> getByName(@PathVariable String name) {
		ResponseDTO<ArtifactConfigurationDTO> result = new ResponseDTO<>();
		try {
			ArtifactConfigurationVO vo = ArtifactConfigurationUtil.getArtifactByName(name);
			if (vo != null) {
				result.setSuccess(true).setData(ConverterUtil.convertArtifactVOToDTO(vo));
			} else {
				result.setSuccess(false).setMessage(ERROR_NOT_FOUND_DATA);
			}
		} catch (Exception e) {
			Log.log.error(ERROR_RETRIEVING_DATA, e);
			result.setSuccess(false).setMessage(ERROR_RETRIEVING_DATA);
		}

		return result;
	}

	@RequestMapping(method = RequestMethod.POST)
	@ResponseBody
	public ResponseDTO<ArtifactConfigurationDTO> save(@RequestBody ArtifactConfigurationDTO dto) {
		ResponseDTO<ArtifactConfigurationDTO> result = new ResponseDTO<>();
		try {
			ArtifactConfigurationVO vo = ArtifactConfigurationUtil.saveArtifact(dto);
			result.setSuccess(true).setData(ConverterUtil.convertArtifactVOToDTO(vo));
		} catch (Exception e) {
			Log.log.error(ERROR_SAVING_DATA, e);
			result.setSuccess(false).setMessage(ERROR_SAVING_DATA);
		}

		return result;
	}

	@RequestMapping(value = "/{names}", method = RequestMethod.DELETE)
	@ResponseBody
	public ResponseDTO<ArtifactConfigurationDTO> deleteBulk(@PathVariable List<String> names) {
		ResponseDTO<ArtifactConfigurationDTO> result = new ResponseDTO<>();
		try {
			boolean deleteResult = true;
			for (String name : names) {
				deleteResult = deleteResult && ArtifactConfigurationUtil.deleteArtifact(name);
			}
			result.setSuccess(deleteResult).setMessage(String.format(SUCCESS_DELETING_DATA, names.size()));
			if (!deleteResult) {
				result.setMessage(ERROR_DELETING_DATA);
			}
		} catch (Exception e) {
			Log.log.error(ERROR_DELETING_DATA, e);
			result.setSuccess(false).setMessage(ERROR_DELETING_DATA);
		}

		return result;
	}
	
	@RequestMapping(value = "/item/{shortName}", method = RequestMethod.GET)
	@ResponseBody
	public ResponseDTO<Set<ArtifactConfigurationDTO>> getArtifactConfigurationsByItemShortName(@PathVariable String shortName) {
		ResponseDTO<Set<ArtifactConfigurationDTO>> result = new ResponseDTO<>();
		try {
			// try CEF items first
			Set<ArtifactConfigurationVO> acVOs = CEFUtil.getCEFItemArtifactConfigurations(shortName);
			if (acVOs.isEmpty()) {
				// now try custom items
				acVOs = CustomDictionaryUtil.getCustomItemArtifactConfigurations(shortName);
			}

			result.setSuccess(true);
			if (!acVOs.isEmpty()) {
				Set<ArtifactConfigurationDTO> dtos = new HashSet<>();
				for (ArtifactConfigurationVO vo : acVOs) {
					dtos.add(ConverterUtil.convertArtifactVOToDTO(vo));
				}
				result.setData(dtos);
			}
		} catch (Exception e) {
			Log.log.error(ERROR_RETRIEVING_DATA, e);
			result.setSuccess(false).setMessage(ERROR_RETRIEVING_DATA);
		}

		return result;
	}
}
