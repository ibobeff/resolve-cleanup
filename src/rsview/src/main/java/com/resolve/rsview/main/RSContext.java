/******************************************************************************
* (C) Copyright 2014
*
* Resolve Systems, LLC
*
* All rights reserved
* This software is distributed under license and may not be used, copied,
* modified, or distributed without the express written permission of
* Resolve Systems, LLC.
*
******************************************************************************/

package com.resolve.rsview.main;

import javax.servlet.ServletContext;

import org.springframework.beans.BeansException;
import org.springframework.context.ApplicationContext;
import org.springframework.context.ApplicationContextAware;
import org.springframework.web.context.ServletContextAware;

public class RSContext implements ApplicationContextAware, ServletContextAware
{
    public static ApplicationContext appContext;
    public static ServletContext servletContext;
    public static String resolveHome;            // e.g. C:\project\resolve3\dist\, /opt/resolve, C:\Program Files\Resolve Systems
    public static String webHome;                // e.g. <productHome>/tomcat/webapps/resolve
    public static String resolveUrl;             // e.g. http://localhost:8080/resolve, comes from rsview.properties file.
    public static Main main;
    
    public void setApplicationContext(ApplicationContext ctx) throws BeansException 
    { 
        RSContext.appContext = ctx;
    } // setApplicationContext
    
    public ApplicationContext getApplicationContext()
    {
        return RSContext.appContext;
    } // getApplicationContext
    
    public void setServletContext(ServletContext ctx)
    {
        servletContext = ctx;
    } // setServletContext
    
    public ServletContext getServletContext()
    {
        return RSContext.servletContext;
    } // getServletContext

    public static String getResolveHome()
    {
        return resolveHome;
    } // getResolveHome

    public static String getWebHome()
    {
        return webHome;
    } // getWebHome

    public static Main getMain()
    {
        return main;
    } // getMain

    public static String getResolveUrl()
    {
        return resolveUrl;
    }
} // RSContext
