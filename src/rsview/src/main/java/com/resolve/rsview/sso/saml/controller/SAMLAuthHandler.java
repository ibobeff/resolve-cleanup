
package com.resolve.rsview.sso.saml.controller;

import java.io.IOException;
import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.owasp.esapi.ESAPI;

import com.google.common.collect.ImmutableMap;
import com.resolve.rsview.main.Main;
import com.resolve.rsview.sso.util.SAMLEnum;
import com.resolve.rsview.sso.util.SSOUtil;
import com.resolve.services.util.PropertiesUtil;
import com.resolve.util.Constants;
import com.resolve.util.Log;
import com.resolve.util.StringUtils;

public class SAMLAuthHandler extends HttpServlet
{
    private static final long serialVersionUID = 1958574018612847877L;
    
    private static Map<String, Object> adfsSessionsMap = new ConcurrentHashMap<>();

	@Override
    public void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException
    {
        processSAMLAuthRequest(request, response);
    }

    @Override
    public void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException
    {
        processSAMLAuthRequest(request, response);
    }

    @SuppressWarnings({ "unchecked" })
    private void processSAMLAuthRequest(HttpServletRequest request, HttpServletResponse response) throws IOException
    {
        Map<String, Object> map = new HashMap<>();
        if (request.getSession().getAttribute(SAMLEnum.PARAMS.getValue()) != null)
        {
            map = (Map<String, Object>)request.getSession().getAttribute(SAMLEnum.PARAMS.getValue());
        }
        else
        {
            map.put(SAMLEnum.REQUEST_URI.getValue(), SAMLEnum.LANDING_URI.getValue());
            map.put(SAMLEnum.QUERY_STRING.getValue(), "");
        }
        
        try
        {
            String sessionId = request.getSession().getId();
            if (StringUtils.isBlank(SSOUtil.INSTANCE.getAuthenticatedUser(sessionId)))
            {
                adfsSessionsMap.put(sessionId, map);
                String ssoAdfsEndpoint = PropertiesUtil.getPropertyString(SAMLEnum.ADFS_ENDPOINT_KEY.getValue());
                ESAPI.httpUtilities().sendRedirect(response, StringUtils.removeNewLineAndCarriageReturn(ssoAdfsEndpoint));
            }
        }
        catch (Exception e)
        {
            Log.log.error(e.getMessage(), e);
        }
        finally
        {
            response.flushBuffer();
            response.getWriter().flush();
        }
    }
    
    public static Map<String, Object> getAdfsSessionsMap() {
		return adfsSessionsMap;
	}
    
    /*
     * Should called when user logs out.
     */
    public static void removeADFSSession(String sessionId)
    {
        if (adfsSessionsMap.containsKey(sessionId))
        {
            adfsSessionsMap.remove(sessionId);
            sendMapToSync(sessionId, null, false);
            Log.log.info("Removed session from adfs sessions map");
        }
    }
    
    /*
     * API invoked by MAction as part of sessions Map synchronization.
     */
    @SuppressWarnings("unchecked")
    public static void syncADFSSessionsMap(Map<String, Object> params)
    {
        if (params != null)
        {
            ImmutableMap<String, Object> mapToSynch = (ImmutableMap<String, Object>)params.get(SAMLEnum.SESSION_MAP.getValue());
            SAMLEnum operation = (SAMLEnum)params.get(SAMLEnum.OPERATION.getValue());
            
            switch (operation)
            {
                case ADD : {
                    /*
                     * A new sessionId got added to the sessionsMap on one of the nodes,
                     * so, add it on all the other nodes.
                     */
                    adfsSessionsMap.putAll(mapToSynch); break;
                }
                case REMOVE : {
                    /*
                     * A sessionId got removed from one node, so synch it with
                     * rest of the nodes by removing it.
                     */
                    adfsSessionsMap.remove(mapToSynch.keySet().iterator().next()); break;
                }
                default : Log.log.error("Wrong operation while synchronizing ADFS sessions map.");
            }
        }
    }

    /*
     * API to be called to send the map to be synched with rest of the nodes.
     * operation true is to add and false to remove.
     */
    public static void sendMapToSync(String sessionId, Map<String, Object> map, boolean operation)
    {
        Map<String, Object> params = new HashMap<>();
        params.put(SAMLEnum.OPERATION.getValue(), operation ? SAMLEnum.ADD : SAMLEnum.REMOVE);
        
        ImmutableMap<String, Object> localMap = ImmutableMap.<String, Object>builder()
                        .put(sessionId, map).build();
        
        params.put(SAMLEnum.SESSION_MAP.getValue(), localMap);
        Main.getESB().sendInternalMessage(Constants.ESB_NAME_RSVIEWS, "MAction.syncADFSSessionsMap", params);
    }
}
