package com.resolve.rsview.controller;

import java.util.ArrayList;
import java.util.List;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.resolve.services.hibernate.util.ConverterUtil;
import com.resolve.services.hibernate.util.CustomDictionaryUtil;
import com.resolve.services.hibernate.vo.CustomDictionaryItemVO;
import com.resolve.services.vo.CustomDictionaryItemDTO;
import com.resolve.services.vo.ResponseDTO;
import com.resolve.util.Log;

@Controller
@RequestMapping("/cd")
public class AjaxCustomDictionary extends GenericController {
	private static final String ERROR_RETRIEVING_DATA = "Error retrieving Custom dictionary data";
	private static final String ERROR_NOT_FOUND_DATA = "Custom dictionary data not found";
	private static final String ERROR_SAVING_DATA = "Error saving Custom dictionary data";
	private static final String ERROR_DELETING_DATA = "Error deleting Custom dictionary data";
	private static final String SUCCESS_DELETING_DATA = "%s records deleted";
	
	@RequestMapping(method = RequestMethod.GET)
	@ResponseBody
	public ResponseDTO<List<CustomDictionaryItemDTO>> list() {
		ResponseDTO<List<CustomDictionaryItemDTO>> result = new ResponseDTO<>();
		try {
			List<CustomDictionaryItemVO> data = CustomDictionaryUtil.getAllCustomDictionary();
			List<CustomDictionaryItemDTO> dtoData = new ArrayList<>(data.size());
			for (CustomDictionaryItemVO vo : data) {
				dtoData.add(ConverterUtil.convertCustomItemVOToDTO(vo));
			}

			result.setSuccess(true).setData(dtoData);
		} catch (Exception e) {
			Log.log.error(ERROR_RETRIEVING_DATA, e);
			result.setSuccess(false).setMessage(ERROR_RETRIEVING_DATA);
		}

		return result;
	}

	@RequestMapping(value = "/{shortName}", method = RequestMethod.GET)
	@ResponseBody
	public ResponseDTO<CustomDictionaryItemDTO> getByShortName(@PathVariable String shortName) {
		ResponseDTO<CustomDictionaryItemDTO> result = new ResponseDTO<>();
		try {
			CustomDictionaryItemVO vo = CustomDictionaryUtil.getCustomDictionaryByShortName(shortName);
			if (vo != null) {
				result.setSuccess(true).setData(ConverterUtil.convertCustomItemVOToDTO(vo));
			} else {
				result.setSuccess(false).setMessage(ERROR_NOT_FOUND_DATA);
			}
		} catch (Exception e) {
			Log.log.error(ERROR_RETRIEVING_DATA, e);
			result.setSuccess(false).setMessage(ERROR_RETRIEVING_DATA);
		}

		return result;
	}

	@RequestMapping(method = RequestMethod.POST)
	@ResponseBody
	public ResponseDTO<CustomDictionaryItemDTO> save(@RequestBody CustomDictionaryItemDTO dto) {
		ResponseDTO<CustomDictionaryItemDTO> result = new ResponseDTO<>();
		try {
			CustomDictionaryItemVO vo = CustomDictionaryUtil.saveCustomDictionary(dto);
			result.setSuccess(true).setData(ConverterUtil.convertCustomItemVOToDTO(vo));
		} catch (Exception e) {
			Log.log.error(ERROR_SAVING_DATA, e);
			result.setSuccess(false).setMessage(ERROR_SAVING_DATA);
		}

		return result;
	}

	@RequestMapping(value = "/{shortNames}", method = RequestMethod.DELETE)
	@ResponseBody
	public ResponseDTO<CustomDictionaryItemDTO> deleteBulk(@PathVariable List<String> shortNames) {
		ResponseDTO<CustomDictionaryItemDTO> result = new ResponseDTO<>();
		try {
			boolean deleteResult = true;
			for (String shortName : shortNames) {
				deleteResult = deleteResult && CustomDictionaryUtil.deleteCustomDictionary(shortName);
			}
			result.setSuccess(deleteResult).setMessage(String.format(SUCCESS_DELETING_DATA, shortNames.size()));
			if (!deleteResult) {
				result.setMessage(ERROR_DELETING_DATA);
			}
		} catch (Exception e) {
			Log.log.error(ERROR_DELETING_DATA, e);
			result.setSuccess(false).setMessage(ERROR_DELETING_DATA);
		}

		return result;
	}

}
