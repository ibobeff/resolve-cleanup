/******************************************************************************
* (C) Copyright 2014
*
* Resolve Systems, LLC
*
* All rights reserved
* This software is distributed under license and may not be used, copied,
* modified, or distributed without the express written permission of
* Resolve Systems, LLC.
*
******************************************************************************/

package com.resolve.rsview.view;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.web.servlet.View;

public class HelloView implements View
{
    public static final String TEXT_HTML_UTF8 = "text/html; charset=UTF-8";
    
    public void render(Map<String, ?> model, HttpServletRequest request, HttpServletResponse response)
    {
        try
        {
            PrintWriter out = response.getWriter();
            
            String result = "";
            result += "<html>";
            result += "<head><title>Example :: Spring Application</title></head>";
            result += "<body>";
            result += "<h1>Example - Spring Application</h1>";
            result += "<p>This is my test 3.</p>";
            result += "</body>";
            result += "</html>";
     
            response.setContentType(TEXT_HTML_UTF8);
            response.setContentLength(result.length());
            out.print(result);
        }
        catch (IOException e)
        {
            e.printStackTrace();
        }
    } // render

    public String getContentType()
    {
        return TEXT_HTML_UTF8;
    } // getContentType
}
