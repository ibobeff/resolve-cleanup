/******************************************************************************
* (C) Copyright 2015
*
* Resolve Systems, LLC
*
* All rights reserved
* This software is distributed under license and may not be used, copied,
* modified, or distributed without the express written permission of
* Resolve Systems, LLC.
*
******************************************************************************/

package com.resolve.rsview.controller;

import java.io.IOException;

import javax.servlet.ServletException;

import org.springframework.stereotype.Controller;
import org.springframework.stereotype.Service;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

/**
 * This controller is used by RsMgmt to check whether RsView is responding to http/https requests.
 * @author Mangesh.Shimpi
 *
 */
@Controller
@Service
public class AjaxSelfCheck extends GenericController
{
	/**
	 * 
	 * @param request
	 * @return
	 * @throws ServletException
	 * @throws IOException
	 */
	@RequestMapping(value = "/selfcheck/ping", method = { RequestMethod.POST, RequestMethod.GET }, headers = "Accept=application/text; charset=utf-8")
    @ResponseBody
    public String ping() throws ServletException, IOException
    {
		return "Hello world!";
    }
}
