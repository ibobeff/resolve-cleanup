package com.resolve.rsview.sso.util;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.ServletException;
import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.collections.CollectionUtils;
import org.owasp.esapi.ESAPI;
import org.owasp.esapi.errors.AccessControlException;

import com.resolve.persistence.model.Groups;
import com.resolve.persistence.util.HibernateProxy;
import com.resolve.persistence.util.HibernateUtil;
import com.resolve.rsview.auth.Authentication;
import com.resolve.rsview.auth.RSAccessException;
import com.resolve.rsview.auth.RSAuthException;
import com.resolve.rsview.auth.ValidateTokenResult;
import com.resolve.rsview.sso.saml.controller.SAMLAuthHandler;
import com.resolve.services.ServiceAuthentication;
import com.resolve.services.ServiceHibernate;
import com.resolve.services.hibernate.util.PropertiesUtil;
import com.resolve.util.Constants;
import com.resolve.util.CryptUtils;
import com.resolve.util.JspUtils;
import com.resolve.util.Log;
import com.resolve.util.StringUtils;
import com.resolve.util.SyslogFormatter;

public enum SSOUtil
{
    INSTANCE;
    public static final String LOCAL_REDIRECT_ERROR_MSG = "Could not redirect to: " + SAMLEnum.AUTH_HANDLER_ENDPOINT.getValue();
    public static final String JSP_REDIRECT_ERROR_MSG = "Could not redirect to sso adfs error jsp page.";

    public boolean createUserAndAssociateGroup(String username, List<String> memberOf, String ssoType) throws Exception
    {
        // copy details from template "user" if does not exist
        Log.auth.trace("Cloning user with template");
        boolean cloned = ServiceHibernate.copyTemplateUser(username, null);
        
        if (memberOf != null && !memberOf.isEmpty()) {
	        Log.auth.trace("Assigning Cloned User Groups:" + memberOf);
	        ServiceAuthentication.assignUserGroups(username, memberOf, ssoType);
        }
        
        return cloned;
    }

    public boolean externalGroupRequirementsMet(String username, List<String> availableRolesOrGroupsList)
    {
        boolean result = false;
        // get the list of all the groups that are marked external and their sysIds
        Map<String, String> externalGroups = getAllExternalGroups();
        if (externalGroups.size() == 0)
        {
            Log.log.error("No External groups defined in Resolve. User " + username + " has the following membership:" + StringUtils.convertCollectionToString(availableRolesOrGroupsList.iterator(), ","));
        }
        for (String attributeStatement : availableRolesOrGroupsList)
        {
            for (String externalGroup : externalGroups.keySet())
            {
                if (attributeStatement.equals(externalGroup))
                {
                    result = true;
                    break;
                }
            }
            if (result)
            {
                break;
            }
        }
        return result;
    }

    public Map<String, String> getAllExternalGroups()
    {
        Map<String, String> result = new HashMap<String, String>();

        try
        {
          HibernateProxy.setCurrentUser("system");
        	HibernateProxy.execute(() -> {
                List<Groups> allGroups = HibernateUtil.getDAOFactory().getGroupsDAO().findAll();
                if (allGroups != null)
                {
                    for (Groups group : allGroups)
                    {
                        if (group.ugetUHasExternalLink())
                        {
                            result.put(group.getUName().trim(), group.getSys_id().trim());
                        }
                    }
                }

        	});
        }
        catch (Exception e)
        {
            Log.log.warn("Error in getting the external groups", e);
                      HibernateUtil.rethrowNestedTransaction(e);
        }

        return result;

    }
    
    /*
     * If sso.adfs.enabled property is set to true, all requests will land here.
     */
    public void handleSsoAdfsRequest(HttpServletRequest request, HttpServletResponse response) throws IOException, RSAuthException, RSAccessException, ServletException
    {
        String sessionId = request.getSession().getId();
         
        /*
         *  First check if the sessionsMap contain any entry wrt the sessionId. If it does,
         *  check if the AUTHENTICATED flag is true. (This flag is set to true in the SAMLIDPHandler after successful authentication).
         *  If authenticated, send the username back. Blank otherwise, which means, the user needs to be authenticated.
         *  Store all the request params came along with this request in the sessionsMap.
         */
        String username = getAuthenticatedUser(sessionId);
        
        Log.auth.debug("Is Session " + sessionId + " Authenticated? " + StringUtils.isBlank(username));
        if (StringUtils.isBlank(username))
        {
            //SAMLAuthHandler.eraseCookie(request, response);
            Map<String, Object> params = new HashMap<>();
          
            params.put(SAMLEnum.QUERY_STRING.getValue(), request.getQueryString());
            params.put(SAMLEnum.REQUEST_URI.getValue(), request.getRequestURI());
            params.put(SAMLEnum.REQUEST_PARAMS.getValue(), request.getParameterMap());
            params.put(Constants.HTTP_ATTRIBUTE_CREATETIME, request.getAttribute(Constants.HTTP_ATTRIBUTE_CREATETIME));
            params.put(SAMLEnum.AUTHENTICATED.getValue(), false);
            request.getSession().setAttribute(SAMLEnum.PARAMS.getValue(), params);
            
            try
            {
                ESAPI.httpUtilities().sendRedirect(response, SAMLEnum.AUTH_HANDLER_ENDPOINT.getValue());
            }
            catch (AccessControlException e)
            {
                Log.log.error(LOCAL_REDIRECT_ERROR_MSG, e);
                throw new ServletException(LOCAL_REDIRECT_ERROR_MSG, e);
            }
        }
        else
        {
            /*
             * User is authenticated, check weather he is authorized. If not, display error JSP page.
             */
            boolean autherized = isAutherized(request);
            if (!autherized)
            {
                String unauthorizedMsg = PropertiesUtil.getPropertyString(SAMLEnum.UNAUTHORIZED_MESSAGE_KEY.getValue());
                if (StringUtils.isBlank(unauthorizedMsg))
                {
                    unauthorizedMsg = "Please contact System Administrator.";
                }
                String[] csrfPageTokenDetails = JspUtils.getCSRFTokenForPage(request, SAMLEnum.ERROR_JSP.getValue());
                StringBuilder builder = new StringBuilder();
                builder.append(SAMLEnum.ERROR_JSP.getValue()).append("?").append(csrfPageTokenDetails[0]).append("=").append(csrfPageTokenDetails[1]);
                builder.append("&Referer=").append(request.getRequestURL());
                request.getSession().setAttribute(SAMLEnum.UNAUTHORIZED_MESSAGE.getValue(), unauthorizedMsg);
                try
                {
                    ESAPI.httpUtilities().sendRedirect(response, builder.toString());
                }
                catch (AccessControlException e)
                {
                    Log.log.error(JSP_REDIRECT_ERROR_MSG, e);
                    throw new ServletException(JSP_REDIRECT_ERROR_MSG, e);
                }
            }
            else
            {
                /*
                 * If authorized, validate the token for the session expiry.
                 */
                Collection<Cookie> authTokenCookies = Authentication.getAuthTokenCookies(request, null, response);
                List<String> tokens = new ArrayList<>();
                String token = null;
                
                if (CollectionUtils.isNotEmpty(authTokenCookies))
                {
                    tokens = new ArrayList<String>(authTokenCookies.size());

                    for (Cookie authTokenCookie : authTokenCookies)
                    {
                        token = authTokenCookie.getValue();
                        tokens.add(CryptUtils.decryptToken(token));
                    }
                }
                ValidateTokenResult valTokenResult = Authentication.validateToken(tokens, request, response);
                if (valTokenResult.isValid())
                {
                    /*
                     * If session is not expired (valid), check if the user has access to the application.
                     */
                    boolean valid = Authentication.validateApp(username, token, request, response);
                    if (!valid)
                    {
                        String syslogMessage = "Token user does not have access to requested URI " + request.getRequestURI();
                        Log.syslog.warn(SyslogFormatter.INSTANCE.formatAudit(Authentication.getRemoteAddrForSysLog(request), username, request.getLocalAddr(), syslogMessage));
    
                        throw new RSAccessException();
                    }
                    /*
                     * Everything looks OK. Put the username in the request attribute and let it go...
                     */
                    request.setAttribute(Constants.HTTP_REQUEST_USERNAME, username);
                }
                else
                {
                    /*
                     * If local Resolve session is expired, remove the sessionId from sessionsMap and invalidate the session.
                     * After some time, browser will refresh. A new request will be made (with the cookies already present in the browser session.
                     * ADFS will authenticate the user and a new session will start all over again.
                     */
                    SAMLAuthHandler.removeADFSSession(sessionId);
                    request.getSession().invalidate();
                }
            }
        }
    }
      
    
    /*
     * If the sessionMap associated with the sessionId has an authenticated flag as true,
     * send the user name. Otherwise blank.
     */
    @SuppressWarnings({ "unchecked" })
    public String getAuthenticatedUser(String sessionId)
    {
        String username = null;
        
        if(StringUtils.isNotBlank(sessionId))
        {
        	Log.auth.trace("Checking For Authenticated Session for " + sessionId);
            Map<String, Object> sessionMap = (Map<String, Object>)SAMLAuthHandler.getAdfsSessionsMap().get(sessionId);
            if (sessionMap != null)
            {
                boolean authenticated = sessionMap.containsKey(SAMLEnum.AUTHENTICATED.getValue()) ? (boolean)sessionMap.get(SAMLEnum.AUTHENTICATED.getValue()) : false;
                Log.auth.trace("Is Authenticated? " + authenticated);
                if (authenticated)
                {
                    Object object = sessionMap.get(SAMLEnum.USERNAME.getValue());
                    if (object != null && StringUtils.isNotBlank(object.toString()))
                    {
                        username = object.toString();
                        Log.auth.trace("Found username: " + username);
                    }
                }
            }
        }
        
        return username;
    }
    
    public boolean isAutherized(HttpServletRequest request)
    {
        return request.getSession().getAttribute(SAMLEnum.AUTHORIZED.getValue()) == null ? true : (request.getSession().getAttribute(SAMLEnum.AUTHORIZED.getValue()).toString().equals("TRUE") ? true : false);
    }
    
    @SuppressWarnings({ "unchecked" })
    public void resetAuthFlag(String sessionId)
    {
        Map<String, Object> sessionMap = (Map<String, Object>)SAMLAuthHandler.getAdfsSessionsMap().get(sessionId);
        if (sessionMap != null)
        {
            sessionMap.put(SAMLEnum.AUTHENTICATED.getValue(), false);
        }
    }
}
