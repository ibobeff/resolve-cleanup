/******************************************************************************
* (C) Copyright 2014
*
* Resolve Systems, LLC
*
* All rights reserved
* This software is distributed under license and may not be used, copied,
* modified, or distributed without the express written permission of
* Resolve Systems, LLC.
*
******************************************************************************/

package com.resolve.rsview.main;

import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

import com.resolve.persistence.util.CustomTableMappingUtil;
import com.resolve.services.ServiceGraph;
import com.resolve.services.ServiceHibernate;
import com.resolve.services.ServiceSocial;
import com.resolve.services.constants.ConstantValues;
import com.resolve.services.graph.social.model.RSComponent;
import com.resolve.services.graph.social.model.component.tree.AdvanceTree;
import com.resolve.services.graph.social.model.message.SubmitNotification;
import com.resolve.services.vo.social.NodeType;
import com.resolve.util.Constants;
import com.resolve.util.Log;
import com.resolve.util.StringUtils;

/**
 * This class is invoked from JMS message listeners. This is just another
 * facade to the underlying functionality. This class interacts with the ServiceSocial
 * for actual functionality.
 */
public class MSocial
{
    @Deprecated
    public static void archive(Map params)
    {
        // backup
        MAction.backup(params);

        // check index
        MAction.checkIndex(params);

        Log.log.info("-- the expire post is non blocking.");
        // remove expired posts
        ServiceSocial.removeExpiredPosts(params);

    } // archive

    public static void updateFeed(Map params)
    {
        String sys_id = (String) params.get(Constants.RSS_SYS_ID);
        if (StringUtils.isNotEmpty(sys_id))
        {
            ServiceHibernate.updateRssWithId(sys_id, "system");
        }

    } // updateFeed

    public static String archiveBefore(Map params)
    {
        return ServiceSocial.removeExpiredPostsBefore(params);
    }

    // posts
    public static void postToUser(Map params)
    {
        if (params != null && params.size() > 0)
        {
            try
            {
                ServiceSocial.postToUser(params);
            }
            catch (Exception e)
            {
                Log.log.error(e.getMessage(), e);
            }
        }
    }

    // public void postToRss(User user, Rss rss, Post post);
    public static void postToForum(Map params)
    {
        try
        {
            ServiceSocial.postToComponent(params, AdvanceTree.FORUMS);
        }
        catch (Exception e)
        {
            Log.log.error(e.getMessage(), e);
        }
    }

    public static void postToTeam(Map params)
    {
        try
        {
            if (StringUtils.isNotBlank(CustomTableMappingUtil.table2Class("social_team")) &&
            	CustomTableMappingUtil.isHibernateInitialized)
            {
                ServiceSocial.postToTeam(params);
            }
            else
            {
                /*
                 * Only during fresh install, if an alert message is getting executed before social package got imported,
                 * an exception is getting displayed. This check will make sure message is forwarded only if table exist.
                 */
                Log.log.warn("Custom table, social_team, does not exist yet.");
            }
        }
        catch (Exception e1)
        {
            Log.log.error(e1.getMessage(), e1);
        }
    }

    public static void postToProcess(Map params)
    {
        try
        {
            ServiceSocial.postToComponent(params, AdvanceTree.PROCESS);
        }
        catch (Exception e)
        {
            Log.log.error(e.getMessage(), e);
        }
    }

    public static void postToActionTask(Map params)
    {
        try
        {
            ServiceSocial.postToComponent(params, AdvanceTree.ACTIONTASKS);
        }
        catch (Exception e)
        {
            Log.log.error(e.getMessage(), e);
        }
    }

    public static void postToRunbook(Map params)
    {
        try
        {
            ServiceSocial.postToComponent(params, AdvanceTree.RUNBOOKS);
        }
        catch (Exception e)
        {
            Log.log.error(e.getMessage(), e);
        }
    }

    public static void postToDocument(Map params)
    {
        try
        {
            ServiceSocial.postToComponent(params, AdvanceTree.DOCUMENTS);
        }
        catch (Exception e)
        {
            Log.log.error(e.getMessage(), e);
        }
    }

    public static void postToRSS(Map params)
    {
        try
        {
            ServiceSocial.postToComponent(params, AdvanceTree.RSS);
        }
        catch (Exception e)
        {
            Log.log.error(e.getMessage(), e);
        }
    }

    /**
     * oldTypeDocOrRunbook --. of type Documents or Runbooks newRSComponent --
     * of type RSComponent
     *
     * @param params
     */
    public void updateSocialForWiki(Map<String, Object> params)
    {
        try
        {
            sendNotification(params);
        }
        catch (Exception e)
        {
            List<SubmitNotification> submitNotifications = (List<SubmitNotification>) params.get(ConstantValues.SOCIAL_NOTIFICATION_LIST);
            
            String details = "";
            
            for (SubmitNotification submitNotification : submitNotifications)
            {
                details += submitNotification.getEventType() + " for component " + submitNotification.getCompDisplayName() + ",";
            }
            
            if (details.endsWith(",") && details.length() >= 2)
            {
                details = details.substring(0, details.length() - 2);
            }
            
            Log.log.info("Ignoring update social notification for " + details);
        }
    }

    /**
     * newRSComponent -- of type RSComponent
     *
     * @param params
     */
    public void updateComponent(Map<String, Object> params)
    {
        RSComponent comp = (RSComponent) params.get(ConstantValues.SOCIAL_RS_COMPONENT);
        if(comp != null)
        {
            try
            {
                ServiceGraph.persistNode(comp.prepareNode(), "system");
            }
            catch(Exception e)
            {
                Log.log.error("Error in updating comp:" + e, e);
            }
        }
    }

    /**
     * deleteComponent - of type RSComponent
     *
     * @param params
     */
    public void deleteComponent(Map<String, Object> params)
    {
        try
        {
            sendNotification(params);
            
            Object comp = params.get(ConstantValues.SOCIAL_DELETE_COMPONENT);
            Object sysIds = params.get(ConstantValues.SOCIAL_DELETE_LIST_OF_DOCSYSID);
            Object atsysIds = params.get(ConstantValues.SOCIAL_DELETE_LIST_OF_ATSYSID);
            Object usersysIds = params.get(ConstantValues.SOCIAL_DELETE_LIST_OF_USERS_SYSID);
            
            if (comp != null)
              {
                  if (comp instanceof RSComponent)
                  {
                      ServiceGraph.deleteNode(null, ((RSComponent) comp).getSys_id(), ((RSComponent) comp).getCompName(), null, "system");
                  }
              }
      
              if (sysIds != null)
              {
                  List<String> listSysIds = (List<String>) sysIds;
                  for (String sysId : listSysIds)
                  {
                      ServiceGraph.deleteNode(null, sysId, null, NodeType.DOCUMENT, "system");
                  }
              }
              
              if (atsysIds != null)
              {
                  List<String> listSysIds = (List<String>) atsysIds;
                  for (String sysId : listSysIds)
                  {
                      ServiceGraph.deleteNode(null, sysId, null, NodeType.ACTIONTASK, "system");
                  }
              }
      
              if (usersysIds != null)
              {
                  List<String> listSysIds = (List<String>) usersysIds;
                  for (String sysId : listSysIds)
                  {
                      ServiceGraph.deleteNode(null, sysId, null, NodeType.USER, "system");
                  }
              }
            
//            SocialUtil.deleteComponent(params);
        }
        catch (Exception e)
        {
            Log.log.error("error in deleting the component", e);
        }
    }

    private void sendNotification(Map<String, Object> params) throws Exception
    {
        // We must send notifications before we delete the component.
        List<SubmitNotification> socialNotifications = (List<SubmitNotification>) params.get(ConstantValues.SOCIAL_NOTIFICATION_LIST);
        if (socialNotifications != null)
        {
            for (SubmitNotification notification : socialNotifications)
            {
                submitNotification(notification);
            }
        }
    }

    /**
     *
     * submitNotification -- object of type SubmitNotification
     *
     *
     * @param params
     */
    public void submitNotification(Map<String, Object> params)
    {
        SubmitNotification socialNotification = (SubmitNotification) params.get("submitNotification");
        if (socialNotification != null)
        {
            try
            {
                submitNotification(socialNotification);
            }
            catch (Exception e)
            {
                Log.log.error(e.getMessage(), e);
            }
        }
    }

    @SuppressWarnings("unchecked")
    public void submitBulkNotifications(Map<String, Object> params)
    {
        List<SubmitNotification> socialNotificationList = (ArrayList<SubmitNotification>) params.get("bulkNotifications");
        if (socialNotificationList != null)
        {
            for (SubmitNotification socialNotification : socialNotificationList)
            {
                try
                {
                    submitNotification(socialNotification);
                }
                catch (Exception e)
                {
                    Log.log.error(e.getMessage(), e);
                }
            }
        }
    }

    private void submitNotification(SubmitNotification socialNotification) throws Exception
    {
        if (socialNotification == null)
        {
            Log.log.error("SubmitNotification cannot be null.");
            throw new Exception("SubmitNotification cannot be null.");
        }
        else if (socialNotification.getCompSysId() == null || socialNotification.getEventType() == null || socialNotification.getUsername() == null)
        {
            Log.log.error("Eventtype, sysId and username cannot be null.");
            throw new Exception("Eventtype, sysId and username cannot be null.");
        }
        else
        {
            try
            {
                if(StringUtils.isNotBlank(socialNotification.getCompSysId()) && socialNotification.getEventType() != null)
                {
                    List<String> targetSysIds = new LinkedList<String>();
                    List<RSComponent> users = ServiceSocial.findUsersRegisteredToReceiveThisNotificationForComp(socialNotification.getCompSysId(), socialNotification.getEventType());
                    if(users != null)
                    {
                        for(RSComponent user : users)
                        {
                            targetSysIds.add(user.getSys_id());
                        }
                    }
                    
                    //post the notification only if there are any users associated with it.
                    if(targetSysIds.size() > 0)
                    {
                        ServiceSocial.postSystemMessage(socialNotification.getSubject(), socialNotification.getContent(), socialNotification.getUsername(), targetSysIds, 
                        		RSContext.getResolveUrl());
                    }
                    else
                    {
                        Log.log.info("Ignoring system notification " + socialNotification.getEventType() + " for component " + socialNotification.getCompDisplayName());
                    }
                }
            }
            catch (Exception e)
            {
                Log.log.info("Ignoring system notification " + socialNotification.getEventType() + " for component " + socialNotification.getCompDisplayName());
            }
        }
    }

    /**
     * This method sends email to user with all the social posts accumulated
     * during a time period.
     *
     * @param params
     */
    public static void dailyEmail(Map params)
    {
        try
        {
            ServiceSocial.dailyEmail(params);
        }
        catch (Exception e)
        {
            Log.log.error(e.getMessage(), e);
        }
    } // dailyEmail

    public static void postToSocialComponent(Map params)
    {
        try
        {
            ServiceSocial.postToSocialComponent(params);
        }
        catch (Exception e)
        {
            Log.log.error(e.getMessage(), e);
        }
    } // postToSocialComponent

    public static void forwardToSocialComponent(Map params)
    {
        try
        {
            ServiceSocial.forwardToSocialComponent(params);
        }
        catch (Exception e)
        {
            Log.log.error(e.getMessage(), e);
        }
    } // forwardToSocialComponent
} // MSocial
