/******************************************************************************
* (C) Copyright 2014
*
* Resolve Systems, LLC
*
* All rights reserved
* This software is distributed under license and may not be used, copied,
* modified, or distributed without the express written permission of
* Resolve Systems, LLC.
*
******************************************************************************/

package com.resolve.rsview.controller;

import java.io.IOException;
import java.util.Map;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.owasp.esapi.errors.AccessControlException;
import org.owasp.esapi.reference.DefaultHTTPUtilities;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

import com.resolve.rsview.util.ControllerUtil;
import com.resolve.services.ServiceHibernate;
import com.resolve.services.ServiceWorksheet;
import com.resolve.services.constants.ConstantValues;
import com.resolve.services.hibernate.vo.ArchiveWorksheetVO;
import com.resolve.util.Constants;
import com.resolve.util.Log;
import com.resolve.util.StringUtils;

@Controller
public class WorksheetController extends GenericController
{
    
    /**
     * 
     * Expected params - atleast 1 of the parameters
     * SYS_ID
     * sys_id
     * PROBLEMID
     * NUMBER
     * REFERENCE
     * ALERTID
     * CORRELATIONID
     * 
     * 
     * @param request
     * @param response
     * @return
     * @throws ServletException
     * @throws IOException
     */
    @RequestMapping(value = ConstantValues.URL_WORKSHEET, method = RequestMethod.GET)
    public ModelAndView worksheet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException
    {
        Map<String,String> params = ControllerUtil.getParameterMap(request);
        
        String type = null;
        String sys_id = null;
        String number = null;
        String reference = null;
        String alertid = null;
        String correlationid = null;
        String orgId = null;
        String orgName = null;
        
        // init sys_id
        sys_id = params.get(Constants.HTTP_REQUEST_SYS_ID);
        if (StringUtils.isEmpty(sys_id))
        {
	        sys_id = params.get(Constants.HTTP_REQUEST_sys_id);
        }
        if (StringUtils.isEmpty(sys_id))
        {
	        sys_id = params.get(Constants.HTTP_REQUEST_PROBLEMID);
        }
        
        orgId = params.get(Constants.EXECUTE_ORG_ID);
        
        if (StringUtils.isBlank(orgId))
        {
            orgId = params.get(Constants.EXECUTE_org_id);
        }
        
        orgName = params.get(Constants.EXECUTE_ORG_NAME);
        
        if (StringUtils.isBlank(orgName))
        {
            orgName = params.get(Constants.EXECUTE_org_name);
        }
        
        if (StringUtils.isNotEmpty(sys_id))
        {
            type = "sys_id";
        }
        else
        {
	        // try NUMBER
	        number = params.get(Constants.HTTP_REQUEST_NUMBER);
	        if (StringUtils.isNotEmpty(number))
	        {
	            type = "number";
	        }
	        else
	        {
		        // try REFERENCE
		        reference = params.get(Constants.HTTP_REQUEST_REFERENCE);
		        if (StringUtils.isNotEmpty(reference))
		        {
		            type = "reference";
		        }
		        else
		        {
		            // try ALERTID
			        alertid = params.get(Constants.HTTP_REQUEST_ALERTID);
			        if (StringUtils.isNotEmpty(alertid))
			        {
			            type = "alertid";
			        }
			        else
			        {
			            // try CORRELATIONID
				        correlationid = params.get(Constants.HTTP_REQUEST_CORRELATIONID);
				        if (StringUtils.isNotEmpty(correlationid))
				        {
				            type = "correlationid";
				        }
				        else
				        {
				            type = null;
				        }
			        }
		        }
	        }
        }
        
        String forwardUrl = "/resolve/jsp/rsworksheet.jsp";
        if (type != null)
        {
	        try
	        {
	            String username = (String) request.getAttribute(Constants.HTTP_REQUEST_USERNAME);
	            if (StringUtils.isBlank(username)) {
	                username = "system";
	            }
	            if (type.equals("sys_id"))
	            {
		            // try worksheet
	                boolean isWorksheetExist = ServiceWorksheet.isWorksheetExist(sys_id, null, null, null, null, orgId, orgName);
		            if (isWorksheetExist)
		            {
		                forwardUrl = "/resolve/jsp/rsworksheet.jsp?main=Worksheet&tab=Results&type=form&sys_id="+sys_id;
		            }
		            else
		            {
		                // try archive worksheet
			            ArchiveWorksheetVO archive = new ArchiveWorksheetVO();
			            archive.setSys_id(sys_id);
			            
			            archive = ServiceHibernate.findByExample(archive);
			            if (archive != null)
			            {
			                forwardUrl = "/resolve/jsp/rsworksheet.jsp?main=ArchiveWorksheet&tab=Results&type=form&sys_id="+sys_id;
			            }
		            }
	            }
	            else if (type.equals("number"))
	            {
		            // try worksheet
                    String wsId = ServiceWorksheet.findWorksheetSysId(null, number, null, null, null, orgId, orgName, username);
		            if (StringUtils.isNotBlank(wsId))
		            {
		                forwardUrl = "/resolve/jsp/rsworksheet.jsp?main=Worksheet&tab=Results&type=form&sys_id="+wsId;
		            }
		            else
		            {
		                // try archive worksheet
                        ArchiveWorksheetVO archive = new ArchiveWorksheetVO();
                        archive.setUNumber(number);
                        
                        archive = ServiceHibernate.findByExample(archive);

			            if (archive != null)
			            {
			                forwardUrl = "/resolve/jsp/rsworksheet.jsp?main=ArchiveWorksheet&tab=Results&type=form&sys_id="+archive.getSys_id();
			            }
		            }
	            }
	            else if (type.equals("reference"))
	            {
	                  // try worksheet
                    String wsId = ServiceWorksheet.findWorksheetSysId(null, null, reference, null, null, orgId, orgName, username);
                    if (StringUtils.isNotBlank(wsId))
		            {
		                //get the first entry from the list
		                forwardUrl = "/resolve/jsp/rsworksheet.jsp?main=Worksheet&tab=Results&type=form&sys_id="+wsId;
		            }
		            else
		            {
		                // try archive worksheet
                        ArchiveWorksheetVO archive = new ArchiveWorksheetVO();
                        archive.setUReference(reference);
                        archive = ServiceHibernate.findByExample(archive);

			            if (archive != null)
			            {
			                forwardUrl = "/resolve/jsp/rsworksheet.jsp?main=ArchiveWorksheet&tab=Results&type=form&sys_id="+archive.getSys_id();
			            }
		            }
	            }
	            else if (type.equals("alertid"))
	            {
		            // try worksheet
                    String wsId = ServiceWorksheet.findWorksheetSysId(null, null, null, alertid, null, orgId, orgName, username);
                    if (StringUtils.isNotBlank(wsId))
		            {
		                forwardUrl = "/resolve/jsp/rsworksheet.jsp?main=Worksheet&tab=Results&type=form&sys_id="+wsId;
		            }
		            else
		            {
		                // try archive worksheet
                        ArchiveWorksheetVO archive = new ArchiveWorksheetVO();
                        archive.setUAlertId(alertid);
                        archive = ServiceHibernate.findByExample(archive);

			            if (archive != null)
			            {
			                forwardUrl = "/resolve/jsp/rsworksheet.jsp?main=ArchiveWorksheet&tab=Results&type=form&sys_id="+archive.getSys_id();
			            }
		            }
	            }
	            else if (type.equals("correlationid"))
	            {
		            // try worksheet
                    String wsId = ServiceWorksheet.findWorksheetSysId(null, null, null, null, correlationid, orgId, orgName, username);
                    if (StringUtils.isNotBlank(wsId))
		            {
		                forwardUrl = "/resolve/jsp/rsworksheet.jsp?main=Worksheet&tab=Results&type=form&sys_id="+wsId;
		            }
		            else
		            {
		                // try archive worksheet
                        ArchiveWorksheetVO archive = new ArchiveWorksheetVO();
                        archive.setUCorrelationId(correlationid);
                        archive = ServiceHibernate.findByExample(archive);

			            if (archive != null)
			            {
			                forwardUrl = "/resolve/jsp/rsworksheet.jsp?main=ArchiveWorksheet&tab=Results&type=form&sys_id="+archive.getSys_id();
			            }
		            }
	            }
	        }
	        catch (Exception e)
	        {
	            Log.log.error(e.getMessage(), e);
	        }
        }
        DefaultHTTPUtilities dhttp = new DefaultHTTPUtilities();
        try {
            dhttp.sendRedirect(response, forwardUrl);   
        } catch(AccessControlException  ace) {
            Log.log.error(ace.getMessage(), ace);
        }
        
        return null;
    } // worksheet 
    
} // WorksheetController
