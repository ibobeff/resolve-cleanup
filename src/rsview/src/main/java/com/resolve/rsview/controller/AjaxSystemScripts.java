/******************************************************************************
* (C) Copyright 2014
*
* Resolve Systems, LLC
*
* All rights reserved
* This software is distributed under license and may not be used, copied,
* modified, or distributed without the express written permission of
* Resolve Systems, LLC.
*
******************************************************************************/

package com.resolve.rsview.controller;

import java.io.IOException;
import java.util.HashMap;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.stereotype.Controller;
import org.springframework.stereotype.Service;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.resolve.services.ServiceHibernate;
import com.resolve.services.hibernate.vo.ResolveSysScriptVO;
import com.resolve.services.vo.QueryDTO;
import com.resolve.services.vo.ResponseDTO;
import com.resolve.util.Constants;
import com.resolve.util.Log;

import net.sf.json.JSONObject;
import net.sf.json.JSONSerializer;
import net.sf.json.JsonConfig;

@Controller
@Service
public class AjaxSystemScripts extends GenericController
{
	private static final String ERROR_DELETING_SYS_SCRIPT = "Error deleting ResolveSysScript";
	private static final String ERROR_RETRIEVING_SYS_SCRIPT = "Error retrieving ResolveSysScript";
	private static final String ERROR_SAVING_SYS_SCRIPT = "Error saving ResolveSysScript";
    private static final String ERROR_EXECUTING_SYS_SCRIPT = "Error executing ResolveSysScript";

	/**
     * Returns list of ResolveSysScript for the grid based on filters, sorts, pagination
     * 
     * @param query
     * @param request
     * @param response
     * @return
     * @throws ServletException
     * @throws IOException
     */
    @RequestMapping(value = "/sysscript/list", method = { RequestMethod.GET, RequestMethod.POST }, headers = "Accept=application/json; charset=utf-8")
    @ResponseBody
    public ResponseDTO list(@ModelAttribute QueryDTO query, HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException
    {
        ResponseDTO result = new ResponseDTO();
        String username = (String) request.getAttribute(Constants.HTTP_REQUEST_USERNAME);
        
        try
        {
            query.setModelName("ResolveSysScript");
//            query.setSelectColumns("sys_id,UName,UDescription,sysUpdatedOn");
            
            List<ResolveSysScriptVO> data = ServiceHibernate.getResolveSysScripts(query, username);
            int total = ServiceHibernate.getTotalHqlCount(query);
            
            result.setSuccess(true).setRecords(data);
            result.setTotal(total);
        }
        catch (Exception e)
        {
            Log.log.error(ERROR_RETRIEVING_SYS_SCRIPT, e);
            result.setSuccess(false).setMessage(ERROR_RETRIEVING_SYS_SCRIPT);
        }
        
        return result;
    }
    
    /**
     * Gets data for a specific ResolveSysScript to be edited based on sysId
     * 
     * @param id
     * @param request
     * @param response
     * @return
     * @throws ServletException
     * @throws IOException
     */
    @RequestMapping(value = "/sysscript/get", method = { RequestMethod.POST }, headers = "Accept=application/json; charset=utf-8")
    @ResponseBody
    public ResponseDTO get(@RequestParam String id, HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException
    {
        ResponseDTO result = new ResponseDTO();
        String username = (String) request.getAttribute(Constants.HTTP_REQUEST_USERNAME);
        
        try
        {
            ResolveSysScriptVO vo = ServiceHibernate.findResolveSysScriptByIdOrName(id, null, username);
            result.setData(vo);
            result.setSuccess(true);
            
        }
        catch (Exception e)
        {
            Log.log.error(ERROR_RETRIEVING_SYS_SCRIPT, e);
            result.setSuccess(false).setMessage(ERROR_RETRIEVING_SYS_SCRIPT);
        }

        return result;
    }
    
    /**
     * Delete the ResolveSysScript based on array of sysIds
     * 
     * @param ids
     * @param request
     * @param response
     * @return
     * @throws ServletException
     * @throws IOException
     */
    @RequestMapping(value = "/sysscript/delete", method = { RequestMethod.POST }, headers = "Accept=application/json; charset=utf-8")
    @ResponseBody
    public ResponseDTO delete(@RequestParam("ids") String[] ids,@RequestParam("all") Boolean all, HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException
    {
        ResponseDTO result = new ResponseDTO();
        String username = (String) request.getAttribute(Constants.HTTP_REQUEST_USERNAME);
        
        try
        {
            ServiceHibernate.deleteResolveSysScriptByIds(ids, username);
            result.setSuccess(true);
        }
        catch (Exception e)
        {
            Log.log.error(ERROR_DELETING_SYS_SCRIPT, e);
            result.setSuccess(false).setMessage(ERROR_DELETING_SYS_SCRIPT);
        }

        return result;
    }
    
    /**
     * Saves/Update an ResolveSysScript
     * 
     * @param property
     * @param request
     * @param response
     * @return
     * @throws ServletException
     * @throws IOException
     */
    @RequestMapping(value = "/sysscript/save", method = { RequestMethod.POST }, headers = "Accept=application/json; charset=utf-8")
    @ResponseBody
    public ResponseDTO save(@RequestBody JSONObject jsonProperty, HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException
    {
      //using jackson to deserialize the json
//        String json = jsonProperty.toString();
//        ResolveSysScriptVO entity = new ObjectMapper().readValue(json, ResolveSysScriptVO.class);
        
        //convert json to VO - spring does not work as the VO are not 100% java bean
        JsonConfig jsonConfig = new JsonConfig();
        jsonConfig.setRootClass(ResolveSysScriptVO.class);
        ResolveSysScriptVO entity = (ResolveSysScriptVO) JSONSerializer.toJava(jsonProperty, jsonConfig);
        
        ResponseDTO result = new ResponseDTO();
        String username = (String) request.getAttribute(Constants.HTTP_REQUEST_USERNAME);
        
        try
        {
            ResolveSysScriptVO vo = ServiceHibernate.saveResolveSysScript(entity, username);
            result.setSuccess(true).setData(vo);
        }
        catch (Exception e)
        {
            Log.log.error(ERROR_SAVING_SYS_SCRIPT, e);
            result.setSuccess(false).setMessage(ERROR_SAVING_SYS_SCRIPT);
        }

        return result;
    }
    
    /**
     * 
     * @param ssName - name of the system script that you want to execute
     * @param request
     * @param response
     * @return
     * @throws ServletException
     * @throws IOException
     */
    @RequestMapping(value = "/sysscript/executeScript", method = { RequestMethod.POST }, headers = "Accept=application/json; charset=utf-8")
    @ResponseBody
    public ResponseDTO executeScript(@RequestParam("name") String ssName, HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException
    {
        ResponseDTO result = new ResponseDTO();
//        String username = (String) request.getAttribute(Constants.HTTP_REQUEST_USERNAME);
        
        try
        {
            String resultStr = ServiceHibernate.executeScript(ssName, new HashMap<String, Object>());
            result.setSuccess(true).setMessage(resultStr);
        }
        catch (Exception e)
        {
            Log.log.error(ERROR_EXECUTING_SYS_SCRIPT, e);
            result.setSuccess(false).setMessage(ERROR_EXECUTING_SYS_SCRIPT);
        }

        return result;
    }
}
