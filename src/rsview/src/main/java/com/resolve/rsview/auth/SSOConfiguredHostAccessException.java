/******************************************************************************
* (C) Copyright 2015
*
* Resolve Systems, LLC
*
* All rights reserved
* This software is distributed under license and may not be used, copied,
* modified, or distributed without the express written permission of
* Resolve Systems, LLC.
*
******************************************************************************/

package com.resolve.rsview.auth;

/**
 * Indicates error in accessing configured single signed on host to authenticate user.
 * <p>
 * In a Single Sign On (SSO) integration, users login into host system and access
 * Resolve system through it. In SSO integration users has to login in only once 
 * into host system and allows user to access host and Resolve system simultaneously.
 *  
 * @author Hemant Phanasgaonkar
 * @since  5.5
 */
public class SSOConfiguredHostAccessException extends Exception
{
    private static final long serialVersionUID = 1829220647898308214L;
    private String externalSystem;
    private String url;
    
    public SSOConfiguredHostAccessException(String externalSystem, String url)
    {
        this.externalSystem = externalSystem;
        this.url = url;
    } // SSOConfiguredHostAccessException
    
    public String getExternalSystem()
    {
        return externalSystem;
    }
    
    public String getUrl()
    {
        return url;
    }
    
    @Override
    public String toString()
    {
        return "SSOConfiguredHostAccessException: externalSystem=" + externalSystem + ", url=" + url;
    }
} // SSOConfiguredHostAccessException
