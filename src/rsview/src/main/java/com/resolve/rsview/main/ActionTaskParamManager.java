/******************************************************************************
* (C) Copyright 2014
*
* Resolve Systems, LLC
*
* All rights reserved
* This software is distributed under license and may not be used, copied,
* modified, or distributed without the express written permission of
* Resolve Systems, LLC.
*
******************************************************************************/

package com.resolve.rsview.main;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.lang.StringEscapeUtils;
import org.hibernate.query.Query;

import com.resolve.persistence.dao.ResolveActionTaskDAO;
import com.resolve.persistence.model.ResolveActionInvoc;
import com.resolve.persistence.model.ResolveActionParameter;
import com.resolve.persistence.model.ResolveActionTask;
import com.resolve.persistence.util.HibernateProxy;
import com.resolve.persistence.util.HibernateUtil;
import com.resolve.rsview.util.ControllerUtil;
import com.resolve.services.ServiceWorksheet;
import com.resolve.util.Constants;
import com.resolve.util.Log;
import com.resolve.util.StringUtils;
import com.resolve.util.constants.HibernateConstantsEnum;
import com.resolve.wiki.store.HibernateStoreUtil;

import net.sf.json.JSONArray;
import net.sf.json.JSONObject;

public class ActionTaskParamManager 
{ 

	/**
	 * This method return the action task input parameters.
	 * 
	 * @param parameterMap
	 * @return
	 */
	@SuppressWarnings("unchecked")
    public static JSONObject getActionTaskParametersInput(Map<String,String> parameterMap)
	{	    

	    JSONObject jsonObject = new JSONObject();
		
		Map<String, Object> results = new HashMap<String, Object>();
		List<String> inputList = new ArrayList<String>();
		List<String> outList = new ArrayList<String>();
//		List<String> flowList = new ArrayList<String>();
		String descLocal = "";
		
		
		try
		{
			String targetLabelName = parameterMap.get("TLABEL");
			String targetDescBefore = parameterMap.get("TDESC");
			
			String targetDesc = targetDescBefore; //java.net.URLDecoder.decode(targetDescBefore, "UTF-8");
			
			String targetNamespace = getNamespace(targetDesc);
			String docName = getDocName(targetDesc);
			
			targetLabelName = StringEscapeUtils.unescapeHtml(targetLabelName);
//			targetDesc = StringEscapeUtils.unescapeHtml(targetDesc);
			targetNamespace = StringEscapeUtils.unescapeHtml(targetNamespace);
			
			//inputList = getParameterList(targetNamespace, docName, true, targetDesc);
			results = getParameterList_OLD(targetNamespace, docName, true, targetDesc);
			inputList = (ArrayList<String>) results.get("DATA");
			descLocal = (String) results.get("DESC");
			
			String source = parameterMap.get("SOURCE");
			JSONArray sourceArray = JSONArray.fromObject(source);
			
			String xml = parameterMap.get("XML");
			
			if(sourceArray != null && sourceArray.size() >0)
			{
				outList = getParameterListByJSONArray(sourceArray, targetDesc);
			}
			
			Set<String> flowValues = getAllFlowValues(xml);
			List<String> flowValuesList = new ArrayList<String>();
			flowValuesList.addAll(flowValues);
			Collections.sort(flowValuesList);
			
			jsonObject.put(HibernateConstantsEnum.INPUT.getTagName(), inputList);
			jsonObject.put(HibernateConstantsEnum.OUTPUT.getTagName(), outList);
			jsonObject.put(HibernateConstantsEnum.INPUT_DESC.getTagName(), descLocal);
			jsonObject.put(HibernateConstantsEnum.FLOW_VALUE.getTagName(), flowValuesList);
            
			
		}
		catch(Exception exp)
		{
			exp.printStackTrace();
		}
		
		return jsonObject;
	} //getActionTaskParametersInput
	
	
	public static Set<String> getAllFlowValues(String modelXml)
	{
	    Set<String> flowValues = new HashSet<String>();
	   
        String startTaskTag = "<Task";
        String endTaskTag = "</Task>";
        String flowValue = "";
        String desc = "";
        
	    try
	    {
	        if(StringUtils.isNotEmpty(modelXml))
	        {
	            while(modelXml.contains(startTaskTag))
	            {
	                String actionTask = modelXml.substring(modelXml.indexOf(startTaskTag), modelXml.indexOf(endTaskTag) + endTaskTag.length());
	                
	                if(actionTask != null && !actionTask.equals(""))
	                {
	                    String actionTaskLocal = actionTask.trim();
	                    
	                    if(actionTaskLocal.contains("description=\""))
	                    {
	                        
	                        desc = actionTaskLocal.substring(actionTaskLocal.indexOf("description=\"") + "description=\"".length(), actionTaskLocal.indexOf("\" tooltip=\""));
	                        desc = StringEscapeUtils.unescapeHtml(desc);
	                        desc = java.net.URLDecoder.decode(desc, "UTF-8");
	                        
	                        if(StringUtils.isNotEmpty(desc))
	                        {
	                            String[] kvPairs = desc.split("&");
	                            
	                            if(kvPairs != null)
	                            {
	                                String token1 = "=$FLOW{";
	                                String token2 = "}";
	                                
	                                for(String kvPair: kvPairs)
	                                {
	                                    if(kvPair.contains(token1) && kvPair.contains(token2))
	                                    {
	                                        flowValue = kvPair.substring(kvPair.indexOf(token1)+token1.length(), kvPair.indexOf(token2));
	                                        
	                                        if(StringUtils.isNotEmpty(flowValue))
	                                        {
	                                            flowValues.add(flowValue);
	                                        }
	                                    }
	                                }
	                            }
	                            
	                        }
	                       
	                    }
	                    
	                    modelXml = modelXml.replace(actionTask, "");
	                   
	                }
	            }
	        }
	    }
	    catch(Exception exp)
	    {
	        Log.log.info("-- get exception during getAllFlowValues - " + exp, exp);
	    }
	    
	    return flowValues;
	}
	
	public List<String> getFlowListFromXML(String xml)
	{
	    List<String> flowList = new ArrayList<String>();
	    
	    try
	    {
	        if(StringUtils.isNotEmpty(xml))
	        {
	            
	        }
	    }
	    catch(Exception exp)
	    {
	        Log.log.info("getFlowListFromXML catched exception : " + exp, exp);
	    }
	    
	    return flowList;
	}
	
	/**
     * This method return the action task output parameters.
     * 
     * @param parameterMap
     * @return
     */
    public static JSONObject getRunbooksActionTasksStatus(HttpServletRequest request, HttpServletResponse response)
    {   
        Map<String,String> parameterMap = new HashMap<String, String>();
        String problemID = "";
        String refreshWiki = "";
        String status = "";
       
        JSONObject jsonObject = new JSONObject();
        List<Map<String, String>> results = new ArrayList<Map<String, String>>();
        
        try
        {
            parameterMap = ControllerUtil.getParameterMap(request);
            response.setContentType("application/xml");
            
            problemID = parameterMap.get(Constants.GROOVY_BINDING_PROBLEMID);
            refreshWiki = parameterMap.get(Constants.WIKI);
            status = parameterMap.get(Constants.STATUS);
            
            boolean doRefresh = !HibernateStoreUtil.isProcessCompleted(problemID, refreshWiki);
            
            //if doRefresh = false; still 
            
//            Log.log.info("---------- the problemID is : " + problemID);
//            Log.log.info(" --------- doRefresh is : " + doRefresh);
           
            results = getRunbooksActionTasksResults(request, problemID, status, refreshWiki);

            jsonObject.put(Constants.DOREFRESH, doRefresh);
            jsonObject.put(Constants.RESULTS, results);
            
        }
        catch(Throwable exp)
        {
            Log.log.info(exp.getMessage() + exp);
        }
        
        return jsonObject;
        
    } //getActionTaskParametersOutput
	
    @SuppressWarnings({ "rawtypes", "unchecked" })
    public static List<Map<String, String>> getRunbooksActionTasksResults(HttpServletRequest request, 
                                                                          String problemID, String status, String modelRunbook) throws Throwable
    {
        List<Map<String, String>> results = new ArrayList<Map<String, String>>();
        
        //String sql = "SELECT req.UNodeId, req.UNodeLabel, rel.UCondition ";
        
        
        Set<String> uniqStatus = new HashSet<String>();
        Set<String> uniqID = new HashSet<String>();
       
        try
        {
            if(StringUtils.isNotEmpty(problemID) && StringUtils.isNotEmpty(modelRunbook))
            {               
              HibernateProxy.setCurrentUser((String) request.getAttribute(Constants.HTTP_REQUEST_USERNAME));
            	HibernateProxy.execute(() -> {
            		
            		String sqlGetModelXML = "SELECT UModelProcess from WikiDocument where UFullname = '";
            		String modelLocal = "";
            		String modelRunbookName = "";
            		
                    sqlGetModelXML = sqlGetModelXML + modelRunbook +"'";
                    
                    Query qGetModelXML = HibernateUtil.createQuery(sqlGetModelXML);
                    List<String> models = qGetModelXML.list();
                    
                    if(models != null)
                    {
                        modelLocal = models.get(0);
                    }
                    
                    JSONObject jsonObj = findAllActionTasksAndRunbooks(modelLocal, modelRunbook);
                    
                    Map<String, Map<String, String>> ats = (Map<String, Map<String, String>>) jsonObj.get(HibernateConstantsEnum.OUTPUT.getTagName());
                    Map<String, Map<String, String>> subprocess = (Map<String, Map<String, String>>) jsonObj.get(HibernateConstantsEnum.OUTPUT_RUNBOOK.getTagName());

                    List<Object> recs = new ArrayList<Object>();

                    recs = ServiceWorksheet.getNodeByProblemId(problemID, status);

                    if(recs.size() == 0) // didn't find in current, try archive tables
                    {
                        String sql = "SELECT req.UNodeId, req.UNodeLabel, rel.UCondition "; 
                        
                        if(status.equalsIgnoreCase(Constants.STATUS_SEVERITY))
                        {
                            sql = "SELECT req.UNodeId, req.UNodeLabel, rel.USeverity "; 
                        }
                        
                        sql += " FROM ArchiveExecuteRequest req, ArchiveActionResult rel " +
                               " WHERE req.problem.sys_id = rel.problem.sys_id " + 
                               " AND req.process.sys_id = rel.process.sys_id " +
                               " AND req.sys_id = rel.executeRequest.sys_id " + 
                               " AND req.problem.sys_id = '" + problemID + "' " + 
                               " ORDER BY rel.UTimestamp DESC ";
                        
                        Query q = HibernateUtil.createQuery(sql);
                        recs = q.list();
                    }
                    
                    String nodeID = "";
                    String nodeLabel = "";
                    String statusLocal = "";
                    Map<String, String> singleResult = new HashMap<String, String>();
                    String color = ""; 
                    String graphNodeID = "";
                    String endNodeID = "";
                    Map<String, String> actionTaskNode = new HashMap<String, String>();
                    
                    if(recs != null && recs.size() >0)
                    {
                        for(Object record: recs)
                        {
                            Object[] arr = (Object[]) record;
                            
                            nodeID = (String) arr[0];
                            nodeLabel = (String) arr[1];
                            statusLocal = (String) arr[2]; 
                            
                            Log.log.info("----- the nodeID is : k" +
                            		"" + nodeID);
                            Log.log.info("----- the nodeLabel is : " + nodeLabel);
                            
                            if(StringUtils.isNotEmpty(nodeID) && ats.containsKey(nodeID))
                            {
                                actionTaskNode = ats.get(nodeID);
                                
                                singleResult = new HashMap<String, String>();
                                graphNodeID = actionTaskNode.get("id");
                                singleResult.put(Constants.GRAPHNODEID, graphNodeID);
                                singleResult.put(Constants.NODEID, nodeID);
                                singleResult.put(Constants.NODELABEL, nodeLabel);
                                singleResult.put(Constants.STATUS, statusLocal);
                                
                                color = getColor(status, statusLocal);                        
                                singleResult.put(Constants.COLOR, color);
                                results.add(singleResult); 
                                
                                uniqStatus.add(nodeID);
                            }
//                            else if(StringUtils.isNotEmpty(nodeLabel) && StringUtils.isNotEmpty(nodeID) 
//                                            && nodeID.contains(modelRunbook) && (nodeLabel.equalsIgnoreCase("start") || nodeLabel.equalsIgnoreCase("end")) 
//                                                            && !uniqStatus.contains(nodeID))
//                            {
//                                singleResult = new HashMap<String, String>();
//                                graphNodeID = nodeID.substring(nodeID.indexOf("::")+2, nodeID.length());
//                                singleResult.put(Constants.GRAPHNODEID, graphNodeID);
//                                singleResult.put(Constants.NODEID, nodeID);
//                                singleResult.put(Constants.NODELABEL, nodeLabel);
//                                singleResult.put(Constants.STATUS, statusLocal);
//                                
//                                color = getColor(status, statusLocal);                        
//                                singleResult.put(Constants.COLOR, color);
//                                results.add(singleResult); 
//                                
//                                uniqStatus.add(nodeID);
//                                
//                            }
                            else if(StringUtils.isNotEmpty(nodeID))
                            {
                                modelRunbookName = nodeID.substring(0, nodeID.indexOf("::"));
                                
                                if(subprocess.containsKey(modelRunbookName) && nodeLabel.equalsIgnoreCase("end"))
                                {
                                    singleResult = new HashMap<String, String>();                                
                                    endNodeID = ((Map<String, String>) subprocess.get(modelRunbookName)).get("id");
                                    
                                    if(!uniqID.contains(endNodeID))
                                    {
                                        singleResult.put(Constants.GRAPHNODEID, endNodeID);
                                        singleResult.put(Constants.NODEID, endNodeID);
                                        singleResult.put(Constants.NODELABEL, nodeLabel);
                                        singleResult.put(Constants.STATUS, "");
                                        
                                        color = getColor(status, statusLocal);                        
                                        singleResult.put(Constants.COLOR, color);
                                        results.add(singleResult); 
                                        
                                        uniqID.add(endNodeID);
                                    }
                                }
                            } 
                        }
                    }
                    

            	});
            }
        }
        catch(Throwable e)
        {
            Log.log.info(e.getMessage() + e);
            
            throw e;
        }
        
        return results;
    }
    
    public static JSONObject findAllActionTasksAndRunbooks(String xml, String modelRunbook)
    {
        String modelXML = xml;
        String startTaskTag = "<Task";
        String endTaskTag = "</Task>";
        String startRunbookTag = "<Subprocess";
        String endRunbookTag = "</Subprocess>";
        
        Map<String, Map<String, String>> actionTaskNameMap = new HashMap<String, Map<String, String>>();
        String actionTaskName = "";
        String id = "";
        String status = "";
        
        Map<String, Map<String, String>> runbookNameMap = new HashMap<String, Map<String, String>>();
        String runbookName = "";
        
        JSONObject jsonObject = new JSONObject();
        Map<String, String> actionTaskNode = new HashMap<String, String>();
        Map<String, String> runbookNode = new HashMap<String, String>();
        String key = "";
        
        if(modelXML != null && !modelXML.equals(""))
        {
            while(modelXML.contains(startTaskTag))
            {
                String actionTask = modelXML.substring(modelXML.indexOf(startTaskTag), modelXML.indexOf(endTaskTag) + endTaskTag.length());
                
                if(actionTask != null && !actionTask.equals(""))
                {
                    String actionTaskLocal = actionTask.trim();
                    
                    if(actionTaskLocal.contains("description=\"\""))
                    {
                        actionTaskName = actionTaskLocal.substring(actionTask.indexOf("label=\"") + "label=\"".length(), actionTask.length());
                        actionTaskName = actionTaskName.substring(0, actionTaskName.indexOf("\""));
                    }
                    else
                    {
                        actionTaskName = actionTaskLocal.substring(actionTaskLocal.indexOf("description=\"") + "description=\"".length(), actionTaskLocal.length());
                        actionTaskName = actionTaskName.substring(0, actionTaskName.indexOf("#"));
                    }
    
                    if(actionTaskName != null && !actionTaskName.equals(""))
                    {
                        actionTaskNode = new HashMap<String, String>();
                        actionTaskNode.put("name", actionTaskName);
                        actionTaskNode.put("type", "actiontask");
                        
                        if(actionTaskLocal.contains("id=\""))
                        {
//                            id = actionTaskLocal.substring(actionTaskLocal.indexOf("id=\"") + "id=\"".length(), actionTaskLocal.indexOf("\">"));
                            id = actionTaskLocal.substring(actionTaskLocal.indexOf("id=\"") + "id=\"".length());
                            id = id.substring(0, id.indexOf("\"")); 
                        }
                        
                        if(actionTaskLocal.contains("status=\""))
                        {
                            status = actionTaskLocal.substring(actionTaskLocal.indexOf("status=\"") + "status=\"".length(), actionTaskLocal.length());
                            status = status.substring(0, status.indexOf("\""));
                        }
                        
                        actionTaskNode.put("id", id);
                        actionTaskNode.put("status", status);
                        
                        key = (StringUtils.isNotEmpty(status))?status:modelRunbook+"::"+id;
                        actionTaskNameMap.put(key, (actionTaskNode));
                        
                    }
                    
                    modelXML = modelXML.replace(actionTask, "");
                }
            }
           
            jsonObject.put(HibernateConstantsEnum.OUTPUT.getTagName(), actionTaskNameMap); 

            while(modelXML.contains(startRunbookTag))
            {
                String runbook = modelXML.substring(modelXML.indexOf(startRunbookTag), modelXML.indexOf(endRunbookTag) + endRunbookTag.length());
                
                if(runbook != null && !runbook.equals(""))
                {
                    String runbookLocal = runbook.trim();
                    runbookName = runbookLocal.substring(runbook.indexOf("label=\"") + "label=\"".length(), runbook.length());
                    runbookName = runbookName.substring(0, runbookName.indexOf("\""));
                    runbookName = runbookName.replace("&#xa;", ""); 
                    runbookName = runbookName.trim();
                    
                    if(runbookName != null && !runbookName.equals(""))
                    {
                        runbookNode = new HashMap<String, String>();
                        
                        runbookNode.put("name", runbookName);
                        runbookNode.put("type", "runbook");
                        
                        if(runbookLocal.contains("id=\""))
                        {
                            id = runbookLocal.substring(runbookLocal.indexOf("id=\"") + "id=\"".length(), runbookLocal.indexOf("\">"));                            
                            runbookNode.put("id", id);
                        }
                        
                        runbookNameMap.put(runbookName, runbookNode);
                    }
                    
                    modelXML = modelXML.replace(runbook, "");
                }
            }
             
            jsonObject.put(HibernateConstantsEnum.OUTPUT_RUNBOOK.getTagName(), runbookNameMap); 
            
        }
        
        return jsonObject;
    }
    
    public static String getColor(String status, String conditionOrSeverity)
    {
        String color = "fillColor=#D3D3D3;";
        
        if(StringUtils.isNotEmpty(status) && StringUtils.isNotEmpty(conditionOrSeverity))
        {
            if(status.equalsIgnoreCase(Constants.STATUS_SEVERITY))
            {
                if(conditionOrSeverity.equalsIgnoreCase("CRITICAL"))
                {
                    color = "fillColor=#FF6347;";
                }
                else if(conditionOrSeverity.equals("GOOD"))
                {
                    color = "fillColor=#98FB98;";
                }
                else if(conditionOrSeverity.equals("WARNING"))
                {
                    color = "fillColor=#F0E68C;";
                }
                else if(conditionOrSeverity.equals("SEVERE"))
                {
                    color = "fillColor=#FFA500;";
                }
                else if(conditionOrSeverity.equals("UNKNOWN"))
                {
                    color = "fillColor=#E0FFFF;";
                }
            }
            else
            {
                if(conditionOrSeverity.equals("GOOD"))
                {
                    color = "fillColor=#98FB98;";
                }
                else if(conditionOrSeverity.equals("BAD"))
                {
                    color = "fillColor=#FF6347;";
                }
                else if(conditionOrSeverity.equals("UNKNOWN"))
                {
                    color = "fillColor=#E0FFFF;";
                }
            }
        }
        
        return color;
        
    }
    
    
//    public static String getColor(String status, String conditionOrSeverity)
//    {
//        String color = "fillColor=#D3D3D3;";
//        
//        if(StringUtils.isNotEmpty(status) && StringUtils.isNotEmpty(conditionOrSeverity))
//        {
//            if(status.equalsIgnoreCase(Constants.STATUS_SEVERITY))
//            {
//                if(conditionOrSeverity.equalsIgnoreCase("CRITICAL"))
//                {
//                    color = "fillColor=tomato;";
//                }
//                else if(conditionOrSeverity.equals("GOOD"))
//                {
//                    color = "fillColor=#98FB98;";
//                }
//                else if(conditionOrSeverity.equals("WARNING"))
//                {
//                    color = "fillColor=Khaki;";
//                }
//                else if(conditionOrSeverity.equals("SEVERE"))
//                {
//                    color = "fillColor=orange;";
//                }
//                else if(conditionOrSeverity.equals("UNKNOWN"))
//                {
//                    color = "fillColor=LightCyan;";
//                }
//            }
//            else
//            {
//                if(conditionOrSeverity.equals("GOOD"))
//                {
//                    color = "fillColor=#98FB98;";
//                }
//                else if(conditionOrSeverity.equals("BAD"))
//                {
//                    color = "fillColor=tomato;";
//                }
//                else if(conditionOrSeverity.equals("UNKNOWN"))
//                {
//                    color = "fillColor=LightCyan;";
//                }
//            }
//        }
//        
//        return color;
//        
//    }
    	
	/**
	 * This method return the action task output parameters.
	 * 
	 * @param parameterMap
	 * @return
	 */
	@SuppressWarnings("unchecked")
    public static JSONObject getActionTaskParametersOutput(Map<String,String> parameterMap)
	{
		JSONObject jsonObject = new JSONObject();
		
		List<String> inputList = new ArrayList<String>();
//		List<String> outList = new ArrayList<String>();
		Map<String, Object> result = new HashMap<String, Object>();
		String sourceDescLocal = "";
		
		try
		{
			String sourceLabelName = parameterMap.get("SLABEL");
			String sourceDesc = parameterMap.get("SDESC");
			String sourceNamespace = getNamespace(sourceDesc);
			String docName = getDocName(sourceDesc);
			
			sourceLabelName = StringEscapeUtils.unescapeHtml(sourceLabelName);
			sourceDesc = StringEscapeUtils.unescapeHtml(sourceDesc);
			sourceNamespace = StringEscapeUtils.unescapeHtml(sourceNamespace);
			
			//inputList = getParameterList(sourceNamespace, sourceLabelName, false, sourceDesc);
			result = getParameterList_OLD(sourceNamespace, docName, false, sourceDesc);
			inputList = (ArrayList<String>) result.get("DATA");
			sourceDescLocal = (String) result.get("DESC");
			//outList = getParameterList(sourceLabelName, sourceLabelName, false);
			
			jsonObject.put(HibernateConstantsEnum.INPUT.getTagName(), inputList);
			jsonObject.put(HibernateConstantsEnum.OUTPUT.getTagName(), inputList);
			jsonObject.put(HibernateConstantsEnum.INPUT_DESC.getTagName(), sourceDescLocal);
			
			
		}
		catch(Exception exp)
		{
			exp.printStackTrace();
		}
		
		return jsonObject;
	} //getActionTaskParametersOutput
	
	
	/**
	 * @param sources
	 * @return
	 */
	@SuppressWarnings("unchecked")
    protected static List<String> getParameterListByJSONArray(JSONArray sources, String targetDesc)
	{
		List<String> paramList = new ArrayList<String>();
		String sourceLabel = null;
		String sourceDesc = null;
		
		try
		{
			for(int i=0; i<sources.size(); i++)
			{
				JSONObject sourceLocal = sources.getJSONObject(i);
				JSONObject obj = (JSONObject) sourceLocal.get("data");
				
				if(obj.containsKey("label"))
				{
					sourceLabel = obj.getString("label");
				}
				else if(obj.containsKey("desc"))
				{
					sourceDesc = obj.getString("desc");
				}
				
				String targetNamespace = getNamespace(sourceDesc);
				
				if(sourceLabel != null)
				{
					//List<String> outputParams = getParameterList(targetNamespace, sourceLabel, false, targetDesc);
				    Map<String, Object> result = getParameterList_OLD(targetNamespace, sourceLabel, false, targetDesc);
				    List<String> outputParams = (ArrayList<String>) result.get("DATA");
					
					for(String output: outputParams)
					{
						String outoutLocal = output;
						if(!paramList.contains(outoutLocal))
						{
							paramList.add(outoutLocal);
						}	
					}
				}
			}
		}
		catch(Throwable t)
		{
			t.printStackTrace();
		}
		
		return paramList;
	} //getParameterListByJSONArray
	
    @SuppressWarnings("unused")
    private static Map<String, Object> getParameterList_NEW(String namespace, String atName, boolean isInputs, String targetDesc)
    {
        Map<String, Object> result = new HashMap<String, Object>();
        Map<String, String> allParams = new HashMap<String, String>();
        List<String> paramList = new ArrayList<String>();
        String atFullName = "";
        
        if(StringUtils.isNotBlank(atName))
        {
            //map of the description from the UI
            Map<String, String> descMap = transformDescToMap(targetDesc, isInputs ? "INPUTS" : "OUTPUTS");
            
            //default values in the DB
            Map<String, String> atDefaultParams = new HashMap<String, String>();
            
            
            ResolveActionTask actionTask = new ResolveActionTask();
            actionTask.setUName(atName);
            if(StringUtils.isNotBlank(namespace))
            {
                actionTask.setUNamespace(namespace);
            }
            
            try
            {
              HibernateProxy.setCurrentUser("system");
            	atFullName = (String) HibernateProxy.execute(() -> {
            		 
            		String actionTaskFullName = "";
            		   ResolveActionTask actionTaskLocal = HibernateUtil.getDAOFactory().getResolveActionTaskDAO().findFirst(actionTask);
                       if(actionTaskLocal != null && actionTaskLocal.getResolveActionInvoc() != null)
                       {
                    	   actionTaskFullName = actionTaskLocal.getUFullName();
                           List<ResolveActionParameter> params = (List<ResolveActionParameter>)actionTaskLocal.getResolveActionInvoc().getResolveActionParameters();
                           
                           for(ResolveActionParameter param : params)
                           {
                               String type = param.getUType();
                               String paramName = param.getUName();
                               String defaultValue = param.getUDefaultValue();
                               
                               if(isInputs)
                               {
                                   //INPUTS
                                   if(StringUtils.isEmpty(type) || type.equalsIgnoreCase(HibernateConstantsEnum.INPUT.getTagName()))
                                   {
                                       if(StringUtils.isNotEmpty(defaultValue))
                                       {
                                           atDefaultParams.put(paramName, "$DEFAULT{" + defaultValue + "}");
                                       }
                                       else
                                       {
                                           paramList.add(paramName);
                                       }
                                   }
                               }
                               else
                               {
                                   //OUTPUTS
                                   if(StringUtils.isNotEmpty(type) && type.equalsIgnoreCase(HibernateConstantsEnum.OUTPUT.getTagName()))
                                   {
                                       if(StringUtils.isNotEmpty(defaultValue))
                                       {
                                           atDefaultParams.put(paramName, "$DEFAULT{" + defaultValue + "}");
                                       }
                                       else
                                       {
                                           paramList.add(paramName);
                                       }
                                       
                                   }
                               }
                           }//end of for loop
                           
                       }
                       
                       return actionTaskFullName;
            	});
                
                //consolidate the params
                allParams.putAll(atDefaultParams);
                allParams.putAll(descMap);
                
                //encode the desc for UI
                String desc = atFullName + "?" + StringUtils.encodeMapForUI(allParams);
                
                result.put("DESC", desc);
                result.put("DATA", paramList);
                
            }
            catch(Throwable t)
            {
                Log.log.error(t.getMessage(), t);
                              HibernateUtil.rethrowNestedTransaction(t);
            }
            
        }
        
        return result;
    }

    private static Map<String, String> transformDescToMap(String desc, String type)
    {
        Map<String, String> result = new HashMap<String, String>();
        
        if(StringUtils.isNotBlank(desc))
        {
            
            if(desc.indexOf('?') > -1)
            {
                desc = desc.substring(desc.indexOf('?')+1).trim();
            }
            else
            {
                //do not process if the desc does not have '?' for Task as that is a Task name
                desc = null;
            }
            
            if(StringUtils.isNotBlank(desc))
            {
                try
                {
                    // unescape html
                    String unescapedStr = StringEscapeUtils.unescapeHtml(desc);

                    // url decode
                    String decodedStr = java.net.URLDecoder.decode(unescapedStr, "UTF-8");
                    
                    String[] params = decodedStr.split("&");
                    for (int i = 0; i < params.length; i++)
                    {
                        String name = "";
                        String value = "";

                        String nameValue = java.net.URLDecoder.decode(params[i], "UTF-8");

                        int valIdx = nameValue.indexOf("=");
                        if (valIdx <= 0)
                        {
                            name = nameValue;
                            value = "";
                        }
                        else
                        {
                            name = nameValue.substring(0, valIdx);
                            value = nameValue.substring(valIdx + 1, nameValue.length());
                        }

                        if (StringUtils.isNotEmpty(name))
                        {
//                            value = java.net.URLDecoder.decode(value, "UTF-8");
                            if("OUTPUTS".equalsIgnoreCase(type))
                            {
                                if(name.trim().endsWith(":"))
                                {
                                    result.put(name, value);
                                }
                            }
                            else
                            {
                                result.put(name, value);
                            }
                        }
                    }//end of for loop
                    
                }
                catch(Exception e)
                {
                    Log.log.debug(e.getMessage(), e);
                }
            }
        }
        
        return result;
        
    }
	
	
	/**
	 * 
	 * @param namespace
	 * @param docName
	 * @param isInputs
	 * @return
	 */
	//protected static List<String> getParameterList(String namespace, String docName, boolean isInputs, String targetDesc)
	protected static Map<String, Object> getParameterList_OLD(String namespace, String docName, boolean isInputs, String targetDesc)
	{
	    Map<String, Object> result = new HashMap<String, Object>();
		List<String> paramList = new ArrayList<String>();
		
		
		try
		{
			if(docName != null && !docName.equals(""))
			{
     HibernateProxy.setCurrentUser("system");
				HibernateProxy.execute(() -> {
					String targetDescFinal = targetDesc;
					ResolveActionTaskDAO actionTaskDAO = HibernateUtil.getDAOFactory().getResolveActionTaskDAO();
					ResolveActionTask actionTask = new ResolveActionTask();
					actionTask.setUName(docName);
					
					if(namespace != null && !namespace.equals(""))
					{
						actionTask.setUNamespace(namespace);
					}
						
					ResolveActionTask actionTaskLocal = actionTaskDAO.findFirst(actionTask);
					
					//get input parameter from database.
					if (actionTaskLocal != null)
	                {
					    
	                    ResolveActionInvoc invocation = actionTaskLocal.getResolveActionInvoc();
	                    
	        	        if (invocation != null)
	        	        {
	        	            /*
	        	            // get invocation parameters
	        				ResolveActionParameterDAO paramDAO = HibernateUtil.getDAOFactory().getResolveActionParameterDAO();
	        				ResolveActionParameter paramQuery = new ResolveActionParameter();
	        				paramQuery.setResolveActionInvoc(invocation);//UInvocation(invocation.getSys_Id());
	        				
	        	            List<ResolveActionParameter> actionParams = paramDAO.find(paramQuery);
	        	            */
	        	            List<ResolveActionParameter> actionParams = (List<ResolveActionParameter>)invocation.getResolveActionParameters();
	        	            String paramName = null;
	        	            String defaultValue = null;
	        	            Set<String> keyHasValue = new HashSet<String>();
	                        
		            		String targetDescParameterPart =  "";
	        	            		                
		            		if(!targetDesc.contains("?"))
		            		{
		            		    targetDescParameterPart = "";
		            		}
		            		else 
		            		{
		            		    targetDescParameterPart = targetDesc.substring(targetDesc.indexOf("?")+1, targetDesc.length());
		            		}
	        	            		
		            		List<String> keysInDesc = new ArrayList<String>();
	                                
	                        if(StringUtils.isNotEmpty(targetDescParameterPart))
	                        {
	                            targetDescParameterPart = StringEscapeUtils.unescapeHtml(targetDescParameterPart);
	                            targetDescParameterPart = java.net.URLDecoder.decode(targetDescParameterPart, "UTF-8");
	                            
	                            String[] kvPaire = targetDescParameterPart.split("&");
	                                    
	                            if(kvPaire != null)
	                            {
	                                for(String kvLocal: kvPaire)
	                                {
	                                    
	                                    try
	                                    {
	                                        kvLocal = java.net.URLDecoder.decode(kvLocal, "UTF-8");
	                                    }
	                                    catch (Throwable tr)
	                                    {
	                                        Log.log.info("Data from Resolve3.4");
	                                    }
	                                    
	                                    
	                                    String keyLocal = (kvLocal.contains("="))?(kvLocal.substring(0, kvLocal.indexOf("="))):"";
	                                            
	                                    if(StringUtils.isNotEmpty(keyLocal))
	                                    {
	                                        keysInDesc.add(keyLocal);
	                                    }
	                                }
	                            }
	                        }
	        	            	
	        	            for (ResolveActionParameter actionParam : actionParams)
	        	            {
	        	            	String uType = actionParam.getUType();
	        	            	
	        	            	if(isInputs && (uType == null || uType.equals("") || uType.equals(HibernateConstantsEnum.INPUT.getTagName())))
	        	            	{
	        	            	    paramName = null;
	        	            	    defaultValue = null;
	        	            		paramName = actionParam.getUName();
	        	            		paramName = StringEscapeUtils.escapeHtml(paramName);
	        	            		defaultValue = actionParam.getUDefaultValue();
	        	            		defaultValue = StringEscapeUtils.escapeHtml(defaultValue);

	        	            		if(!keyHasValue.contains(paramName) && !keysInDesc.contains(paramName))
	        	            		{
	        	            		    if(StringUtils.isNotEmpty(defaultValue))
	        	            		    {
	        	            		        defaultValue = defaultValue.replace("&amp;", "AANNDD");
	        	            		        defaultValue = defaultValue.replace("%", "PPEERRCCEENNTT");
	        	            		        defaultValue = "$DEFAULT{" + defaultValue + "}";
	        	            		        
	        	            		        String lastChar = targetDesc.substring(targetDesc.length()-1);
	        	            		        
	        	            		        if(StringUtils.isNotEmpty(lastChar) && lastChar.equals("?"))
	        	            		        {
	        	            		        	targetDescFinal = targetDescFinal + paramName + "=" + defaultValue; 
	        	            		        }
	        	            		        else if(StringUtils.isNotEmpty(lastChar) && !lastChar.equals("?") && !targetDesc.contains("?"))
	        	            		        {
	        	            		        	targetDescFinal = targetDescFinal + "?" + paramName + "=" + defaultValue; 
	        	            		        }
	        	            		        else
	        	            		        {
	        	            		        	targetDescFinal = targetDescFinal + "&" + paramName + "=" + defaultValue;
	        	            		        }
	        	            		        
	        	            		        keyHasValue.add(paramName);
	        	            		    }
	        	            		    else
	        	            		    {
	        	            		        paramList.add(paramName);
	        	            		    }
	        	            		}
	        	            	}
	        	            	else if(!isInputs && uType != null && uType.equals(HibernateConstantsEnum.OUTPUT.getTagName()))
	        	            	{
	        	            	    
	        	            	    defaultValue = null;
	        	            		paramName = actionParam.getUName();
	        	            		paramName = StringEscapeUtils.escapeHtml(paramName);
	        	            		defaultValue = actionParam.getUDefaultValue();
	                                defaultValue = StringEscapeUtils.escapeHtml(defaultValue);
	        	            		
	                                if(!keysInDesc.contains(paramName + ":"))
//	                                if(!targetDesc.contains(paramName + ":="))
	                                {
	                                    if(StringUtils.isNotEmpty(defaultValue))
	                                    {
	                                        defaultValue = "$DEFAULT{" + defaultValue + "}";
	                                        String lastChar = targetDesc.substring(targetDesc.length()-1);
	                                        
	                                        String nameValue =  paramName + ":=" + defaultValue;
	                                        nameValue = java.net.URLEncoder.encode(nameValue, "UTF-8");
	                                        nameValue = java.net.URLEncoder.encode(nameValue, "UTF-8");
	                                        
	                                        if(StringUtils.isNotEmpty(lastChar) && lastChar.equals("?"))
	                                        {
	                                        	targetDescFinal = targetDescFinal + nameValue;//paramName + ":=" + defaultValue; 
	                                        }
	                                        else if(StringUtils.isNotEmpty(lastChar) && !lastChar.equals("?") && !targetDesc.contains("?"))
	                                        {
	                                        	targetDescFinal = targetDescFinal + "?" + nameValue;//paramName + ":=" + defaultValue; 
	                                        }
	                                        else
	                                        {
	                                        	targetDescFinal = targetDescFinal + "&" + nameValue;//paramName + ":=" + defaultValue;
	                                        }
	                                    }
	                                    else
	                                    {
	                                        paramList.add(paramName);
	                                    }
	                                }
	                                
	        	            		//paramList.add(paramName);
	        	            		
	        	            	}
	        	            }
	        	        }
	                }
					
					result.put("DESC", targetDesc);
					result.put("DATA", paramList);
					
				});
			}
		}
		catch(Throwable t)
		{
            Log.log.error(t.getMessage(), t);
                HibernateUtil.rethrowNestedTransaction(t);
		}
		
		//return paramList;
		return result;
	} //getParameterList
	
	/**
	 * 
	 * @param desc
	 * @return
	 */
	protected static String getNamespace(String desc)
	{
		String namespace = null;
		
		if(desc != null && desc.contains("#"))
		{
			if(desc.contains("?"))
			{
				namespace = desc.substring(desc.indexOf("#") + 1, desc.indexOf("?"));
			}
			else
			{
				namespace = desc.substring(desc.indexOf("#") + 1, desc.length());
			}
		}
		
		return namespace;
	} //getNamespace
	
	/**
	 * 
	 * @param desc
	 * @return
	 */
	protected static String getDocName(String desc)
	{
		String docName = null;
		
		if(desc != null && desc.contains("#"))
		{
			docName = desc.substring(0, desc.indexOf("#"));
		}
		
		return docName;
	} //getDocName
}
