/******************************************************************************
 * (C) Copyright 2014
 *
 * Resolve Systems, LLC
 *
 * All rights reserved
 * This software is distributed under license and may not be used, copied,
 * modified, or distributed without the express written permission of
 * Resolve Systems, LLC.
 *
 ******************************************************************************/

package com.resolve.rsview.util.fileupload;

import java.io.IOException;
import java.io.InputStream;
import java.sql.Blob;
import java.util.Arrays;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.TimeZone;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.fileupload.FileItem;
import org.apache.commons.fileupload.FileItemFactory;
import org.apache.commons.fileupload.disk.DiskFileItemFactory;
import org.apache.commons.fileupload.servlet.ServletFileUpload;

import com.resolve.dto.UploadFileDTO;
import com.resolve.services.ServiceHibernate;
import com.resolve.services.util.ServletUtils;
import com.resolve.util.constants.HibernateConstants;
import com.resolve.util.GMTDate;

public class FileUpload
{
    // list of parameters expected from the request object
    public static final String TABLE_NAME = "tableName";
    public static final String LIST_OF_COLUMN_NAMES = "listOfColumnNames";
    public static final String LIST_OF_COLUMN_VALUES = "listOfColumnValues";

    public static final String REF_COLUMN_NAME = "columnName";
    public static final String REF_SYS_ID = "recordId";

    protected HttpServletRequest request = null;
    protected HttpServletResponse response = null;

    protected FileUploadVO fileUploadData = new FileUploadVO();

    public FileUpload(HttpServletRequest request, HttpServletResponse response)
    {
        this.request = request;
        this.response = response;
    }

    @SuppressWarnings("unchecked")
    public UploadFileDTO upload() throws Exception
    {
        UploadFileDTO result = null;

        boolean isMultipart = ServletFileUpload.isMultipartContent(request);
        if (isMultipart)
        {
            // if is an upload request, then proceed
            // Create a factory for disk-based file items
            FileItemFactory factory = new DiskFileItemFactory();

            // Create a new file upload handler
            ServletFileUpload upload = new ServletFileUpload(factory);

            // Parse the request
            List<FileItem> items = upload.parseRequest(request);

            // Process the uploaded items
            Iterator<FileItem> iter = items.iterator();
            while (iter.hasNext())
            {
                FileItem item = iter.next();
                if (item.isFormField())
                {
                    processFormField(item);
                }
                else
                {
                    processUploadedFile(item);
                }
            } // end of while loop

            // insert into the table
            result = insert();
        }

        return result;

    }

    public void download(String sys_id, String tableName) throws Exception
    {
        FileUploadVO record = select(sys_id, tableName);

        // response.setDateHeader("Last-Modified", record.);
        // response.setContentLength(wikiAttachment.UgetWikiAttachmentContent().getUContent().length);
        try
        {
            ServletUtils.sendHeaders(record.fileName, record.size,
                                     Arrays.copyOf(record.content, 
                                                   record.content.length > 128 ? 
                                                   128 : record.content.length),
                                     request, response);
            response.getOutputStream().write(record.content);
        }
        catch (IOException e)
        {
            throw new Exception("Error downloading the file for table " + tableName, e);
        }

    }

    // different for different DB - mysql, oracle
    protected UploadFileDTO insert() throws Exception
    {
        Map<String, Object> data = convertFileUploadVOToMap(fileUploadData);
        data = ServiceHibernate.insertOrUpdateCustomTable("admin", fileUploadData.tableName, data);

        // prepare the return
        UploadFileDTO dto = new UploadFileDTO();
        dto.setSys_id((String) data.get(HibernateConstants.SYS_ID));
        dto.setSysCreatedBy((String) data.get(HibernateConstants.SYS_CREATED_BY));
        // dto.setSysCreatedOn(DateManipulator.getDateInUTCFormat(new Timestamp(((Date) data.get(HibernateConstants.SYS_CREATED_ON)).getTime())));
        Date createdOn = (Date) data.get(HibernateConstants.SYS_CREATED_ON);
        dto.setSysCreatedOn(GMTDate.getLocalServerDate(createdOn));
        dto.setSysUpdatedBy((String) data.get(HibernateConstants.SYS_UPDATED_BY));
        Date updatedOn = (Date) data.get(HibernateConstants.SYS_UPDATED_ON);
        dto.setSysUpdatedOn(GMTDate.getLocalServerDate(updatedOn));
        dto.setFileName(fileUploadData.fileName);

        return dto;
    }

    protected FileUploadVO select(String sys_id, String tableName) throws Exception
    {
        Map<String, Object> m = ServiceHibernate.findById(tableName, sys_id);// EntityUtil.findById(tableName, sys_id);
        return convertMapToFileUploadVO(m);
    }

    private void processFormField(FileItem item)
    {
        String name = item.getFieldName();
        String value = item.getString();
        if (name.equalsIgnoreCase(TABLE_NAME))
        {
            fileUploadData.tableName = value;
        }
        else if (name.equalsIgnoreCase(LIST_OF_COLUMN_NAMES))
        {
            fileUploadData.listOfColumnNames = value;
        }
        else if (name.equalsIgnoreCase(LIST_OF_COLUMN_VALUES))
        {
            fileUploadData.listOfColumnValues = value;
        }
        else if (name.equalsIgnoreCase(REF_COLUMN_NAME))
        {
            fileUploadData.refColumnName = value;
        }
        else if (name.equalsIgnoreCase(REF_SYS_ID))
        {
            fileUploadData.refSysId = value;
        }
    }

    private void processUploadedFile(FileItem item) throws Exception
    {
        // few attributes for debuging purpose...may be removed/commented out
        // String fieldName = item.getFieldName();
        String fileName = item.getName();// C:\project\resolve3\dist\tomcat\webapps\resolve\jsp\attach.jsp
        fileName = fileName.substring(fileName.lastIndexOf("\\") + 1);
        // String contentType = item.getContentType();//text/plain
        // boolean isInMemory = item.isInMemory();

        long sizeInBytes = item.getSize();
        InputStream uploadedStream = item.getInputStream();
        byte[] data = new byte[(int) sizeInBytes];
        uploadedStream.read(data);

        fileUploadData.content = data;
        fileUploadData.fileName = fileName;
        fileUploadData.size = data.length;
    }

    private FileUploadVO convertMapToFileUploadVO(Map<String, Object> data)
    {
        FileUploadVO record = new FileUploadVO();

        record.fileName = (String) data.get(HibernateConstants.FU_UFILENAME);
        record.size = ((Long) data.get(HibernateConstants.FU_USIZE)).intValue();

        Object c = data.get(HibernateConstants.FU_UCONTENT);
        Blob blob = (Blob) c;
        byte[] content = ServiceHibernate.getBytesFromBlob(blob);
        record.content = content;

        return record;
    }

    private Map<String, Object> convertFileUploadVOToMap(FileUploadVO vo)
    {
        Blob blob = ServiceHibernate.getBlobFromBytes(vo.content);

        Date now = new Date();

        Map<String, Object> data = new HashMap<String, Object>();
        data.put(HibernateConstants.SYS_CREATED_BY, "admin");
        data.put(HibernateConstants.SYS_CREATED_ON, GMTDate.getDate(now));
        data.put(HibernateConstants.SYS_UPDATED_BY, "admin");
        data.put(HibernateConstants.SYS_UPDATED_ON, GMTDate.getDate(now));
        data.put(HibernateConstants.FU_UFILENAME, vo.fileName);
        data.put(HibernateConstants.FU_USIZE, new Long(vo.size));
        data.put(HibernateConstants.FU_UCONTENT, blob);
        data.put(vo.refColumnName, vo.refSysId);

        return data;
    }

}
