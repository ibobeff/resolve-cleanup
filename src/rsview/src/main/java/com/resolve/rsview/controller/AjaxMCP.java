/******************************************************************************
* (C) Copyright 2014
*
* Resolve Systems, LLC
*
* All rights reserved
* This software is distributed under license and may not be used, copied,
* modified, or distributed without the express written permission of
* Resolve Systems, LLC.
*
******************************************************************************/

package com.resolve.rsview.controller;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.codehaus.jackson.map.ObjectMapper;
import org.springframework.stereotype.Controller;
import org.springframework.stereotype.Service;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import com.resolve.rsbase.MainBase;
import com.resolve.rsview.main.Main;
import com.resolve.services.ServiceHibernate;
import com.resolve.services.ServiceMCP;
import com.resolve.services.hibernate.vo.MCPComponentVO;
import com.resolve.services.hibernate.vo.MCPFileVO;
import com.resolve.services.hibernate.vo.MCPServerVO;
import com.resolve.services.hibernate.vo.ResolveMCPGroupVO;
import com.resolve.services.util.ServletUtils;
import com.resolve.services.vo.MCPFileTreeDTO;
import com.resolve.services.vo.QueryDTO;
import com.resolve.services.vo.QueryFilter;
import com.resolve.services.vo.ResponseDTO;
import com.resolve.util.Constants;
import com.resolve.util.Log;
import com.resolve.util.MCPConstants;
import com.resolve.util.MCPUtils;
import com.resolve.util.Properties;
import com.resolve.util.StringUtils;

import net.sf.json.JSONObject;

@Controller
public class AjaxMCP extends GenericController
{
    ///////////////////////////////////////////
    // New MCP Controller API's
    ///////////////////////////////////////////
    
	/**
	 * An API to register a component on MCP. This API will be called by every component during it's startup.
	 * 
	 * @param blueprint : String representing the blueprint.properties file content which belongs to the component.
	 * @param request : HttpServletRequest
	 * @param response : HttpServletResponse
	 * @return ResponseDTO with success as true if registration succeeds, false otherwise with the message containing error message.
	 * @throws ServletException
	 * @throws IOException
	 */
    @RequestMapping(value = "/mcp/register", method = { RequestMethod.POST }, headers = "Accept=application/json; charset=utf-8")
    @ResponseBody
    public ResponseDTO<Object> register(@RequestParam String blueprint, 
                    HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException
    {
        ResponseDTO<Object> result = new ResponseDTO<Object>();
        
        try
        {
            ServiceMCP.registerInstance(blueprint);
            result.setSuccess(true);
            Log.log.info("MCP Component registration - SUCCESS!");
        }
        catch(Exception e)
        {
            Log.log.error("MCP Component registration - FAILED!", e);
            result.setSuccess(false).setMessage("Error: Component registration failed");
        }
        
        return result;
    }
    
    /**
     * API to get list of registered component instances with MCP.
     * 
     * @param request : HttpServletRequest
	 * @param response : HttpServletResponse
     * @return ResponseDTO with records containing list of ResolveMCPGroupVO
     * @throws ServletException
     * @throws IOException
     */
    @RequestMapping(value = "/mcp/getRegisteredInstances", method = { RequestMethod.GET }, headers = "Accept=application/json; charset=utf-8")
    @ResponseBody
    public ResponseDTO<ResolveMCPGroupVO> getRegisteredInstances(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException
    {
        ResponseDTO<ResolveMCPGroupVO> result = new ResponseDTO<ResolveMCPGroupVO>();
        
        try
        {
            List<ResolveMCPGroupVO> instanceList = ServiceMCP.getRegisteredInstances(false);
            result.setRecords(instanceList);
            result.setSuccess(true);
        }
        catch(Exception e)
        {
            Log.log.error("Error getting registered instances", e);
            result.setSuccess(false).setMessage("Error getting registered instances");
        }
        
        return result;
    }
    
    /**
     * API to deregister any registered MCP instance.
     * @param clusterName : String representing cluster name to be deregister.
     * @param hostName : String representing host name under the given cluster to be deregister. If not sent, entire cluster is deregistered.
     * @param request
     * @return
     * @throws ServletException
     * @throws IOException
     */
    @RequestMapping(value = "/mcp/deregister", method = { RequestMethod.POST }, headers = "Accept=application/json; charset=utf-8")
    @ResponseBody
    public ResponseDTO<Object> deregister(@RequestParam String clusterName,  @RequestParam String hostName, HttpServletRequest request) throws ServletException, IOException
    {
        ResponseDTO<Object> result = new ResponseDTO<Object>();
        
        try
        {
        	String status = ServiceMCP.deregisterInstance(clusterName, hostName);
            Log.log.info("MCP Component deregistration status: " + status);
            if (status.contains("Error"))
            {
            	result.setSuccess(false);
            }
            else
            {
            	result.setSuccess(true);
            }
            result.setMessage(status);
        }
        catch(Exception e)
        {
            Log.log.error(e.getMessage(), e);
            result.setSuccess(false).setMessage("Error: Component deregistration failed.");
        }
        
        return result;
    }
    
    ///////////////////////////////////////////
    // New MCP Controller API's End
    ///////////////////////////////////////////
    /**
     * Saves/Update an blueprint
     *
     * @param jsonProperty
     * @param request
     * @param response
     * @return
     * @throws ServletException
     * @throws IOException
     */
    @RequestMapping(value = "/mcp/blueprint/save", method = { RequestMethod.POST }, headers = "Accept=application/json; charset=utf-8")
    @ResponseBody
    public ResponseDTO save(@RequestBody JSONObject jsonProperty, HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException
    {
      //using jackson to deserialize the json
        String json = jsonProperty.toString();
        MCPServerVO vo = new ObjectMapper().readValue(json, MCPServerVO.class);
        
        // convert json to VO - spring does not work as the VO are not 100% java
        // bean
//        JsonConfig jsonConfig = new JsonConfig();
//        jsonConfig.setRootClass(MCPServerVO.class);
//        MCPServerVO vo = (MCPServerVO) JSONSerializer.toJava(jsonProperty, jsonConfig);

        ResponseDTO result = new ResponseDTO();
        String username = (String) request.getAttribute(Constants.HTTP_REQUEST_USERNAME);

        try
        {
            // only 'id' comes back from UI
            vo.setSys_id(vo.getId());
            vo.setBlueprint(MCPUtils.initBlueprint(vo.getBlueprint()));
            MCPServerVO responseVo = ServiceMCP.saveMCPServer(vo, username);
            result.setSuccess(true).setData(responseVo);
        }
        catch (Exception e)
        {
            Log.log.error("Error saving the Blueprint", e);
            result.setSuccess(false).setMessage("Error saving the Blueprint.");
        }

        return result;
    }

    /**
     * Returns list of Blueprint for the grid based on filters, sorts,
     * pagination
     *
     * @param query
     * @param request
     * @param response
     * @return
     * @throws ServletException
     * @throws IOException
     */
    @RequestMapping(value = "/mcp/blueprint/list", method = { RequestMethod.GET, RequestMethod.POST }, headers = "Accept=application/json; charset=utf-8")
    @ResponseBody
    public ResponseDTO list(@ModelAttribute QueryDTO query, HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException
    {
        ResponseDTO result = new ResponseDTO();

        String username = (String) request.getAttribute(Constants.HTTP_REQUEST_USERNAME);

        try
        {
            query.setModelName("MCPServer");
            // query.setSelectColumns("sys_id, serverIp, username, blueprint, sysUpdatedOn");

            List<MCPServerVO> data = ServiceMCP.getAllMCPServer(query, username);
            int total = ServiceHibernate.getTotalHqlCount(query);

            result.setSuccess(true).setRecords(data);
            result.setTotal(total);
        }
        catch (Exception e)
        {
            Log.log.error("Error retrieving MCPBlueprint", e);
            result.setSuccess(false).setMessage("Error retrieving MCPBlueprints");
        }

        return result;
    }

    /**
     * Gets data for a specific Blueprint
     *
     * @param id
     * @param request
     * @param response
     * @return
     * @throws ServletException
     * @throws IOException
     */
    @RequestMapping(value = "/mcp/blueprint/get", method = { RequestMethod.POST }, headers = "Accept=application/json; charset=utf-8")
    @ResponseBody
    public ResponseDTO get(@RequestParam String id, HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException
    {
        ResponseDTO result = new ResponseDTO();

        String username = (String) request.getAttribute(Constants.HTTP_REQUEST_USERNAME);

        try
        {
            MCPServerVO vo = ServiceMCP.findServerById(id, username);
            result.setData(vo);
            result.setSuccess(true);
        }
        catch (Exception e)
        {
            Log.log.error("Error retrieving MCPBlueprint", e);
            result.setSuccess(false).setMessage("Error retrieving MCPBlueprint");
        }

        return result;
    }
    
    /**
     * Gets text for a specific MCP Log File
     *
     * @param id
     * @param request
     * @param response
     * @return
     * @throws ServletException
     * @throws IOException
     */
    @RequestMapping(value = "/mcp/log/get", method = { RequestMethod.POST }, headers = "Accept=application/json; charset=utf-8")
    @ResponseBody
    public ResponseDTO getLog(@RequestParam String id, HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException
    {
        ResponseDTO result = new ResponseDTO();

        String username = (String) request.getAttribute(Constants.HTTP_REQUEST_USERNAME);

        try
        {
            MCPFileVO vo = ServiceMCP.getMCPFileWithContent(id, username);
            if (vo != null && vo.ugetMCPFileContent() != null && vo.ugetMCPFileContent().getContent() != null)
            {
                String content = new String(vo.ugetMCPFileContent().getContent());
                result.setData(content);
            }
            else
            {
                Log.log.trace("No MCP Log found for id: " + id);
            }
            result.setSuccess(true);
        }
        catch (Exception e)
        {
            Log.log.error("Error retrieving MCPBlueprint", e);
            result.setSuccess(false).setMessage("Error retrieving MCPBlueprint");
        }

        return result;
    }

    @RequestMapping(value = "/mcp/blueprint/delete", method = { RequestMethod.POST }, headers = "Accept=application/json; charset=utf-8")
    @ResponseBody
    public ResponseDTO delete(@RequestParam String[] ids, HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException
    {
        ResponseDTO result = new ResponseDTO();
        String username = (String) request.getAttribute(Constants.HTTP_REQUEST_USERNAME);

        try
        {
            for (String id : ids)
            {
                ServiceMCP.deleteMCPServer(id, username);
            }
            result.setSuccess(true);
        }
        catch (Exception e)
        {
            Log.log.error("Error deleting MCPBlueprint", e);
            result.setSuccess(false).setMessage("Error deleting MCPBlueprint");
        }

        return result;
    }

    /**
     * Deploys a blueprint
     *
     * @param jsonProperty
     * @param request
     * @param response
     * @return
     * @throws ServletException
     * @throws IOException
     */
    @RequestMapping(value = "/mcp/blueprint/deploy", method = { RequestMethod.POST }, headers = "Accept=application/json; charset=utf-8")
    @ResponseBody
    public ResponseDTO deploy(@RequestBody JSONObject jsonProperty, HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException
    {
        
      //using jackson to deserialize the json
        String json = jsonProperty.toString();
        MCPServerVO vo = new ObjectMapper().readValue(json, MCPServerVO.class);
        
        // convert json to VO - spring does not work as the VO are not 100% java
        // bean
//        JsonConfig jsonConfig = new JsonConfig();
//        jsonConfig.setRootClass(MCPServerVO.class);
//        MCPServerVO vo = (MCPServerVO) JSONSerializer.toJava(jsonProperty, jsonConfig);

        ResponseDTO result = new ResponseDTO();
        String username = (String) request.getAttribute(Constants.HTTP_REQUEST_USERNAME);

        try
        {
            // only 'id' comes back from UI
            vo.setSys_id(vo.getId());
            vo.setStatus(MCPConstants.MCP_SERVER_STATUS_BLUEPRINT_DEPLOYING);
            MCPServerVO responseVo = ServiceMCP.saveMCPServer(vo, username);

            //Now deploy
            deploy(vo, username);

            result.setSuccess(true).setData(responseVo);
        }
        catch (Exception e)
        {
            Log.log.error("Error saving the Blueprint", e);
            result.setSuccess(false).setMessage("Error saving the Blueprint.");
        }

        return result;
    }

    /**
     * Deploy a blueprint
     *
     * @param id
     *            the blueprint's id in the database.
     * @param request
     * @param response
     * @return
     * @throws ServletException
     * @throws IOException
     */
    @RequestMapping(value = "/mcp/blueprint/deployBlueprints", method = { RequestMethod.POST }, headers = "Accept=application/json; charset=utf-8")
    @ResponseBody
    public ResponseDTO deployBlueprints(@RequestParam String[] ids, HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException
    {
        ResponseDTO result = new ResponseDTO();

        try
        {
            String username = (String) request.getAttribute(Constants.HTTP_REQUEST_USERNAME);
            for (String id : ids)
            {
                MCPServerVO vo = ServiceMCP.findServerById(id, username);

                // only 'id' comes back from UI
                vo.setSys_id(vo.getId());
                // save it because user may have changed something and clicked deploy.
                vo.setStatus(MCPConstants.MCP_SERVER_STATUS_BLUEPRINT_DEPLOYING);
                vo = ServiceMCP.saveMCPServer(vo, username);

                deploy(vo, username);
            }
            result.setSuccess(true);
        }
        catch (Exception e)
        {
            Log.log.error("Error deploying Blueprint", e);
            result.setSuccess(false).setMessage("Error deploying Blueprint");
        }

        return result;
    }

    private void deploy(MCPServerVO vo, String username) throws Exception
    {
        Map<String, Object> params = new HashMap<String, Object>();

        String blueprintContent = vo.getBlueprint();
        InputStream inStream = new ByteArrayInputStream(blueprintContent.getBytes());
        Properties properties = new Properties();
        properties.load(inStream);

        String ipAddress = getProperty(properties, "LOCALHOST", null);
        Integer sshPort = vo.getSshPort();
        String remoteUsername = vo.getRemoteUser();
        String remotePassword = vo.getRemotePassword();
        String resolveHome = vo.getResolveHome();
        String terminalPrompt = vo.getTerminalPrompt();
        String privateKeyFile = vo.getPrivateKeyLocation();

        params.put(Constants.EXECUTE_WIKI, "MCP.Deploy Blueprint");
        params.put(Constants.EXECUTE_PROBLEMID, "NEW");
        params.put(Constants.EXECUTE_USERID, username);
        params.put(Constants.EXECUTE_PROCESSID, "NEW");

        params.put("MCP_SERVER_ID", vo.getId());
        params.put("IP_ADDRESS", ipAddress);
        params.put("SSH_PORT", sshPort);
        params.put("REMOTE_USERNAME", remoteUsername);
        params.put("REMOTE_PASSWORD", remotePassword);
        params.put("PRIVATE_KEY_FILE", privateKeyFile);
        params.put("BLUEPRINT_CONTENT", blueprintContent);
        params.put("RESOLVE_HOME_DIR", resolveHome);
        params.put("TARGET_DIR", resolveHome + "/rsmgmt/config");
        params.put("PROMPT", terminalPrompt);

        MainBase.main.getESB().sendMessage(Constants.ESB_NAME_EXECUTEQUEUE, "MAction.executeProcess", params);
    }

    /**
     * Verifies a blueprint with the remote server. This verification is useful because
     * someone might manually change the remote blueprint.
     *
     * @param id
     *            the blueprint's id in the database.
     * @param request
     * @param response
     * @return
     * @throws ServletException
     * @throws IOException
     */
    @RequestMapping(value = "/mcp/blueprint/verify", method = { RequestMethod.POST }, headers = "Accept=application/json; charset=utf-8")
    @ResponseBody
    public ResponseDTO verifyBlueprints(@RequestParam String[] ids, HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException
    {
        ResponseDTO result = new ResponseDTO();

        try
        {
            String username = (String) request.getAttribute(Constants.HTTP_REQUEST_USERNAME);
            for (String id : ids)
            {
                MCPServerVO vo = ServiceMCP.findServerById(id, username);

                // only 'id' comes back from UI
                vo.setSys_id(vo.getId());
                // save it because user may have changed something and clicked deploy.
                vo.setStatus(MCPConstants.MCP_SERVER_STATUS_BLUEPRINT_VERIFYING);
                vo = ServiceMCP.saveMCPServer(vo, username);

                Map<String, Object> params = new HashMap<String, Object>();

                String blueprintContent = vo.getBlueprint();
                InputStream inStream = new ByteArrayInputStream(blueprintContent.getBytes());
                Properties properties = new Properties();
                properties.load(inStream);

                String ipAddress = getProperty(properties, "LOCALHOST", null);
                Integer sshPort = vo.getSshPort();
                String remoteUsername = vo.getRemoteUser();
                String remotePassword = vo.getRemotePassword();
                String resolveHome = vo.getResolveHome();
                String terminalPrompt = vo.getTerminalPrompt();
                String privateKeyFile = vo.getPrivateKeyLocation();

                params.put(Constants.EXECUTE_WIKI, "MCP.Verify Blueprint");
                params.put(Constants.EXECUTE_PROBLEMID, "NEW");
                params.put(Constants.EXECUTE_USERID, username);
                params.put(Constants.EXECUTE_PROCESSID, "NEW");

                params.put("MCP_SERVER_ID", vo.getId());
                params.put("IP_ADDRESS", ipAddress);
                params.put("SSH_PORT", sshPort);
                params.put("REMOTE_USERNAME", remoteUsername);
                params.put("REMOTE_PASSWORD", remotePassword);
                params.put("PRIVATE_KEY_FILE", privateKeyFile);
                params.put("BLUEPRINT_CONTENT", blueprintContent);
                params.put("RESOLVE_HOME_DIR", resolveHome);
                params.put("TARGET_DIR", resolveHome + "/rsmgmt/config");
                params.put("PROMPT", terminalPrompt);

                MainBase.main.getESB().sendMessage(Constants.ESB_NAME_EXECUTEQUEUE, "MAction.executeProcess", params);
            }
            result.setSuccess(true);
        }
        catch (Exception e)
        {
            Log.log.error("Error deploying Blueprint", e);
            result.setSuccess(false).setMessage("Error deploying Blueprint");
        }

        return result;
    }

    private String getProperty(Properties properties, String propertyName, String defaultValue) throws Exception
    {
        // if the caller passes defaultValue then property doesn't have to be
        // set.
        // defaultValue could be an empty string as well
        boolean isNullable = defaultValue != null;
        String result = properties.get(propertyName);
        if (StringUtils.isBlank(result))
        {
            if (isNullable)
            {
                result = defaultValue;
            }
            else
            {
                throw new Exception(propertyName + " property must be set in the blueprint");
            }
        }
        return result;
    }

    /**
     * Returns list of Blueprint for the grid based on filters, sorts,
     * pagination
     *
     * @param query
     * @param request
     * @param response
     * @return
     * @throws ServletException
     * @throws IOException
     */
    @RequestMapping(value = "/mcp/component/list", method = { RequestMethod.GET }, headers = "Accept=application/json; charset=utf-8")
    @ResponseBody
    public ResponseDTO listComponent(@ModelAttribute QueryDTO query, HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException
    {
        ResponseDTO result = new ResponseDTO();

        String username = (String) request.getAttribute(Constants.HTTP_REQUEST_USERNAME);

        try
        {
            query.setModelName("MCPComponent");

            List<MCPComponentVO> data = ServiceMCP.getAllMCPComponent(query, username);
            int total = ServiceHibernate.getTotalHqlCount(query);

            result.setSuccess(true).setRecords(data);
            result.setTotal(total);
        }
        catch (Exception e)
        {
            Log.log.error("Error retrieving MCPBlueprint", e);
            result.setSuccess(false).setMessage("Error retrieving MCPBlueprint");
        }

        return result;
    }
    
    /**
     * Returns list of Updates for the grid based on filters, sorts,
     * pagination
     *
     * @param query
     * @param request
     * @param response
     * @return
     * @throws ServletException
     * @throws IOException
     */
    @RequestMapping(value = "/mcp/update/list", method = { RequestMethod.GET }, headers = "Accept=application/json; charset=utf-8")
    @ResponseBody
    public ResponseDTO listUpdates(@ModelAttribute QueryDTO query, HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException
    {
        ResponseDTO result = new ResponseDTO();

        String username = (String) request.getAttribute(Constants.HTTP_REQUEST_USERNAME);

        try
        {
            query.setModelName("MCPFile");
            query.addFilterItem(new QueryFilter("string", MCPConstants.MCP_FILE_UPDATE + "|" + MCPConstants.MCP_FILE_UPGRADE, "type", "equals")); 

            List<MCPFileVO> data = ServiceMCP.getAllMCPFiles(query, username);
            int total = ServiceHibernate.getTotalHqlCount(query);

            result.setSuccess(true).setRecords(data);
            result.setTotal(total);
        }
        catch (Exception e)
        {
            Log.log.error("Error retrieving MCPFile", e);
            result.setSuccess(false).setMessage("Error retrieving MCPFile");
        }

        return result;
    }
    
    
    @RequestMapping(value = "/mcp/file/delete", method = { RequestMethod.POST }, headers = "Accept=application/json; charset=utf-8")
    @ResponseBody
    public ResponseDTO deleteMCPFile(@RequestParam String[] ids, HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException
    {
        ResponseDTO result = new ResponseDTO();
        String username = (String) request.getAttribute(Constants.HTTP_REQUEST_USERNAME);

        try
        {
            for (String id : ids)
            {
                ServiceMCP.deleteMCPFile(id, username);
            }
            result.setSuccess(true);
        }
        catch (Exception e)
        {
            Log.log.error("Error deleting MCPFile", e);
            result.setSuccess(false).setMessage("Error deleting MCPFile");
        }

        return result;
    }
    
    /**
     * The file name must be in the form update-N.N.N-DDDDDDDD.zip or upgrade-N.N.N-DDDDDDDD.zip
     * where N.N.N is the version for the update/upgrade, and DDDDDDDD is the build date for the update/upgrade
     * 
     * @param request
     * @return
     * @throws ServletException
     * @throws IOException
     */
    @RequestMapping(value = "/mcp/update/upload", method = { RequestMethod.POST }, headers = "Accept=application/json; charset=utf-8")
    @ResponseBody
    public ResponseDTO upload(HttpServletRequest request) throws ServletException, IOException
    {
        ResponseDTO result = new ResponseDTO();
        String username = (String) request.getAttribute(Constants.HTTP_REQUEST_USERNAME);
        
        try 
        {
            if(StringUtils.isEmpty(username))
            {
                throw new Exception("Request username cannot be null");
            }
            ServiceMCP.uploadUpdate(username, request);
            
            result.setSuccess(true);
        }
        catch (Exception e)
        {
            Log.log.error("Error uploading the mcp update/upgrade file ", e);
            result.setSuccess(false).setMessage("Error uploading the mcp update/upgrade file");
        }

        return result;
    }
    
    /**
     * @param request
     * @return
     * @throws ServletException
     * @throws IOException
     */
    @RequestMapping(value = "/mcp/file/download", method = { RequestMethod.GET, RequestMethod.POST }, headers = "Accept=application/json; charset=utf-8")
    @ResponseBody
    public ModelAndView download(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException
    {
        String username = (String) request.getAttribute(Constants.HTTP_REQUEST_USERNAME);
	    String fileId = request.getParameter("MCP_FILE_ID");
        
        try 
        {
            if(StringUtils.isEmpty(username))
            {
                throw new Exception("Request username cannot be null");
            }
            
            MCPFileVO mcpFile = ServiceMCP.getMCPFileWithContent(fileId, username);
            if (mcpFile != null)
            {
                response.setDateHeader("Last-Modified", mcpFile.getSysCreatedOn().getTime());
                ServletUtils.sendHeaders(mcpFile.getName(), mcpFile.ugetMCPFileContent().getContent().length,
                                         Arrays.copyOf(mcpFile.ugetMCPFileContent().getContent(), 
                                                       mcpFile.ugetMCPFileContent().getContent().length > 128 ? 
                                                       128 : mcpFile.ugetMCPFileContent().getContent().length),
                                         request, response);
                response.getOutputStream().write(mcpFile.ugetMCPFileContent().getContent());
            }
        }
        catch (Exception e)
        {
            Log.log.error("Error downloading the mcp file ", e);
            throw new ServletException(e);
        }

        return null;
    }
    
    /**
     * Returns list of Files for the grid based on filters, sorts,
     * pagination
     *
     * @param query
     * @param request
     * @param response
     * @return
     * @throws ServletException
     * @throws IOException
     */
    @RequestMapping(value = "/mcp/log/list", method = { RequestMethod.GET }, headers = "Accept=application/json; charset=utf-8")
    @ResponseBody
    public ResponseDTO listLogs(@ModelAttribute QueryDTO query, HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException
    {
        ResponseDTO result = new ResponseDTO();

        String username = (String) request.getAttribute(Constants.HTTP_REQUEST_USERNAME);

        try
        {
            query.setModelName("MCPFile");

            List<MCPFileVO> data = ServiceMCP.getAllMCPFiles(query, username);
            int total = ServiceHibernate.getTotalHqlCount(query);
            query.addFilterItem(new QueryFilter("string", MCPConstants.MCP_FILE_LOG, "type", "equals")); 

            result.setSuccess(true).setRecords(data);
            result.setTotal(total);
        }
        catch (Exception e)
        {
            Log.log.error("Error retrieving MCPFile", e);
            result.setSuccess(false).setMessage("Error retrieving MCPFile");
        }

        return result;
    }

    @RequestMapping(value = "/mcp/component/start", method = { RequestMethod.POST }, headers = "Accept=application/json; charset=utf-8")
    @ResponseBody
    public ResponseDTO startComponent(@RequestParam String[] ids, HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException
    {
        return manageComponent(ids, true, false, request);
    }

    @RequestMapping(value = "/mcp/component/stop", method = { RequestMethod.POST }, headers = "Accept=application/json; charset=utf-8")
    @ResponseBody
    public ResponseDTO stopComponent(@RequestParam String[] ids, HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException
    {
        return manageComponent(ids, false, true, request);
    }

    @RequestMapping(value = "/mcp/component/restart", method = { RequestMethod.POST }, headers = "Accept=application/json; charset=utf-8")
    @ResponseBody
    public ResponseDTO restartComponent(@RequestParam String[] ids, HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException
    {
        return manageComponent(ids, true, true, request);
    }

    private ResponseDTO manageComponent(String[] ids, Boolean doStart, Boolean doStop, HttpServletRequest request)
    {
        ResponseDTO result = new ResponseDTO();

        try
        {
            String username = (String) request.getAttribute(Constants.HTTP_REQUEST_USERNAME);

            Map<String, Object> params = new HashMap<String, Object>();

            Map<String, MCPServerVO> servers = new HashMap<String, MCPServerVO>();
            List<MCPComponentVO> components = ServiceMCP.findComponentsByIds(ids, username, 
            		((Main) MainBase.main).getConfigSQL());
            for (MCPComponentVO component : components)
            {
                MCPServerVO serverVo = servers.get(component.getServerIp());
                if (serverVo == null)
                {
                    serverVo = ServiceMCP.findServerByIP(component.getServerIp(), username);
                    serverVo.setComponents("");
                    servers.put(component.getServerIp(), serverVo);
                }
                String componentCode = component.getComponentName();
                if (StringUtils.isNotBlank(component.getInstanceName()))
                {
                    //e.g., RSRemote may have more instances in a box.
                    componentCode = component.getInstanceName();
                }
                serverVo.setComponents(serverVo.getComponents() + componentCode + " ");
            }

            for (String ipAddress : servers.keySet())
            {
                MCPServerVO vo = servers.get(ipAddress);

                Integer sshPort = vo.getSshPort();
                String remoteUsername = vo.getRemoteUser();
                String remotePassword = vo.getRemotePassword();
                String resolveHome = vo.getResolveHome();
                String terminalPrompt = vo.getTerminalPrompt();
                String privateKeyFile = vo.getPrivateKeyLocation();

                params.put(Constants.EXECUTE_WIKI, "MCP.Manage Component");
                params.put(Constants.EXECUTE_PROBLEMID, "NEW");
                params.put(Constants.EXECUTE_USERID, username);
                params.put(Constants.EXECUTE_PROCESSID, "NEW");

                params.put("MCP_SERVER_ID", vo.getId());
                params.put("IP_ADDRESS", ipAddress);
                params.put("SSH_PORT", sshPort);
                params.put("REMOTE_USERNAME", remoteUsername);
                params.put("REMOTE_PASSWORD", remotePassword);
                params.put("PRIVATE_KEY_FILE", privateKeyFile);
                params.put("RESOLVE_HOME_DIR", resolveHome);
                params.put("PROMPT", terminalPrompt);
                params.put("DO_START", doStart.toString());
                params.put("DO_STOP", doStop.toString());
                params.put("RESOLVE_COMPONENT", vo.getComponents());

                MainBase.main.getESB().sendMessage(Constants.ESB_NAME_EXECUTEQUEUE, "MAction.executeProcess", params);

            }
            result.setSuccess(true);
        }
        catch (Exception e)
        {
            Log.log.error("Error starting Server", e);
            result.setSuccess(false).setMessage("Error starting Server");
        }

        return result;
    }

    @RequestMapping(value = "/mcp/component/status", method = { RequestMethod.POST }, headers = "Accept=application/json; charset=utf-8")
    @ResponseBody
    public ResponseDTO componentStatus(@RequestParam String[] ids, HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException
    {
        ResponseDTO result = new ResponseDTO();

        try
        {
            String username = (String) request.getAttribute(Constants.HTTP_REQUEST_USERNAME);

            Map<String, Object> params = new HashMap<String, Object>();

            Map<String, MCPServerVO> servers = new HashMap<String, MCPServerVO>();
            List<MCPComponentVO> components = ServiceMCP.findComponentsByIds(ids, username, 
            		((Main) MainBase.main).getConfigSQL());
            for (MCPComponentVO component : components)
            {
                MCPServerVO serverVo = servers.get(component.getServerIp());
                if (serverVo == null)
                {
                    serverVo = ServiceMCP.findServerByIP(component.getServerIp(), username);
                    serverVo.setComponents("");
                    servers.put(component.getServerIp(), serverVo);
                }
                String componentCode = component.getComponentName();
                if (StringUtils.isNotBlank(component.getInstanceName()))
                {
                    //e.g., RSRemote may have more instances in a box.
                    componentCode = component.getInstanceName();
                }
                serverVo.setComponents(serverVo.getComponents() + componentCode + " ");
            }

            for (String ipAddress : servers.keySet())
            {
                MCPServerVO vo = servers.get(ipAddress);

                params.put(Constants.EXECUTE_WIKI, "MCP.Component Status");
                params.put(Constants.EXECUTE_PROBLEMID, "NEW");
                params.put(Constants.EXECUTE_USERID, username);
                params.put(Constants.EXECUTE_PROCESSID, "NEW");

                params.put("MCP_SERVER_ID", vo.getId());
                params.put("IP_ADDRESS", ipAddress);
                params.put("SSH_PORT", vo.getSshPort());
                params.put("REMOTE_USERNAME", vo.getRemoteUser());
                params.put("REMOTE_PASSWORD", vo.getRemotePassword());
                params.put("PRIVATE_KEY_FILE", vo.getPrivateKeyLocation());
                //params.put("BLUEPRINT_CONTENT", vo.getBlueprint());
                params.put("RESOLVE_HOME_DIR", vo.getResolveHome());
                params.put("PROMPT", vo.getTerminalPrompt());
                params.put("RESOLVE_COMPONENT", vo.getComponents());

                MainBase.main.getESB().sendMessage(Constants.ESB_NAME_EXECUTEQUEUE, "MAction.executeProcess", params);

            }
            result.setSuccess(true);
        }
        catch (Exception e)
        {
            Log.log.error("Error starting Server", e);
            result.setSuccess(false).setMessage("Error starting Server");
        }

        return result;
    }
    
    
    @RequestMapping(value = "/mcp/server/update", method = { RequestMethod.POST }, headers = "Accept=application/json; charset=utf-8")
    @ResponseBody
    public ResponseDTO updateServer(@RequestParam String[] ids, HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException
    {
        ResponseDTO result = new ResponseDTO();
        String updateFilename = request.getParameter("updateFilename");
        String force = request.getParameter("force");
        if (StringUtils.isBlank(force))
        {
            Log.log.warn("Missing force parameter, update may error out if being run over old update");
            force = "false";
        }
        if (StringUtils.isBlank(updateFilename))
        {
            Log.log.error("Missing updateFilename parameter to deploy update");
            result.setSuccess(false).setMessage("Missing Update File Name to Deploy");
        }
        else
        {
            result = updateServer(ids, updateFilename, force, request);
        }
        
        return result;
    }
    
    @RequestMapping(value = "/mcp/loglist", method = { RequestMethod.GET, RequestMethod.POST }, headers = "Accept=application/json; charset=utf-8")
    @ResponseBody
    public ResponseDTO logList(@ModelAttribute QueryDTO query, HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException
    {
        ResponseDTO result = new ResponseDTO();
        List<MCPFileTreeDTO> logList = null;
        String username = (String) request.getAttribute(Constants.HTTP_REQUEST_USERNAME);
        
        String field = request.getParameter("field");
        
        if ("server".equalsIgnoreCase(field))
        {
            query.addFilterItem(new QueryFilter("string", "NULL", "serverID", "notEquals", true)); 
        }
        if ("name".equalsIgnoreCase(field))
        {
            String serverId = request.getParameter("serverId");
            query.addFilterItem(new QueryFilter("string", serverId, "serverID", "equals")); 
        }
        
        try
        {
            query.setModelName("MCPFile");
            query.addFilterItem(new QueryFilter("string", MCPConstants.MCP_FILE_LOG, "type", "equals")); 
            logList = ServiceMCP.getMCPFileTree(query, field, username);
            result.setSuccess(true).setRecords(logList);
        }
        catch (Exception e)
        {
            Log.log.error("Error retrieving MCP Log Tree", e);
            result.setSuccess(false).setMessage("Error retrieving MCP Log Tree");
        }
        
        return result;
    }
    
    private ResponseDTO updateServer(String[] ids, String updateFilename, String force, HttpServletRequest request)
    {
        ResponseDTO result = new ResponseDTO();

        try
        {
            String username = (String) request.getAttribute(Constants.HTTP_REQUEST_USERNAME);

            Map<String, Object> params = new HashMap<String, Object>();

            Map<String, MCPServerVO> servers = new HashMap<String, MCPServerVO>();
            for (String id : ids)
            {
                MCPServerVO serverVo = ServiceMCP.findServerById(id, username);
                if (serverVo != null)
                {
                    serverVo.setStatus(MCPConstants.MCP_SERVER_STATUS_UPDATE_STARTING);
                    ServiceMCP.saveMCPServer(serverVo, username);
                    serverVo.setComponents("");
                    servers.put(serverVo.getServerIp(), serverVo);
                }
            }

            for (String ipAddress : servers.keySet())
            {
                MCPServerVO vo = servers.get(ipAddress);

                Integer sshPort = vo.getSshPort();
                String remoteUsername = vo.getRemoteUser();
                String remotePassword = vo.getRemotePassword();
                String resolveHome = vo.getResolveHome();
                String terminalPrompt = vo.getTerminalPrompt();
                String privateKeyFile = vo.getPrivateKeyLocation();

                params.put(Constants.EXECUTE_WIKI, "MCP.Deploy Update");
                params.put(Constants.EXECUTE_PROBLEMID, "NEW");
                params.put(Constants.EXECUTE_USERID, username);
                params.put(Constants.EXECUTE_PROCESSID, "NEW");

                params.put("MCP_SERVER_ID", vo.getId());
                params.put("IP_ADDRESS", ipAddress);
                params.put("SSH_PORT", sshPort);
                params.put("REMOTE_USERNAME", remoteUsername);
                params.put("REMOTE_PASSWORD", remotePassword);
                params.put("PRIVATE_KEY_FILE", privateKeyFile);
                params.put("RESOLVE_HOME_DIR", resolveHome);
                params.put("PROMPT", terminalPrompt);
                params.put("RESOLVE_COMPONENT", vo.getComponents());
                params.put("UPDATE_FILENAME", updateFilename);
                params.put("FORCE", force);

                MainBase.main.getESB().sendMessage(Constants.ESB_NAME_EXECUTEQUEUE, "MAction.executeProcess", params);

            }
            result.setSuccess(true);
        }
        catch (Exception e)
        {
            Log.log.error("Error starting Server", e);
            result.setSuccess(false).setMessage("Error Deploying Update");
        }

        return result;
    }
}
