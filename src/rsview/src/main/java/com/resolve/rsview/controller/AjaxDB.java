/******************************************************************************
* (C) Copyright 2014
*
* Resolve Systems, LLC
*
* All rights reserved
* This software is distributed under license and may not be used, copied,
* modified, or distributed without the express written permission of
* Resolve Systems, LLC.
*
******************************************************************************/

package com.resolve.rsview.controller;

import java.io.IOException;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.resolve.rsbase.MainBase;
import com.resolve.rsview.main.Main;
import com.resolve.services.MetaTableService;
import com.resolve.services.ServiceHibernate;
import com.resolve.services.constants.RightTypeEnum;
import com.resolve.services.vo.DbDTO;
import com.resolve.services.vo.QueryDTO;
import com.resolve.services.vo.ResponseDTO;
import com.resolve.util.Constants;
import com.resolve.util.Log;
import com.resolve.util.StringUtils;
import com.resolve.workflow.form.QueryCustomForm;
import com.resolve.workflow.table.dto.RsMetaTableViewDTO;

@Controller
public class AjaxDB extends GenericController
{
	private MetaTableService metaTableService;
	
	@Autowired
	public AjaxDB(final MetaTableService metaTableService) {
		this.metaTableService = metaTableService;
	}

	/**
     * Gets data out of the database based on the provided parameters to the queryDTO. Two types of queries are supported, hql and sql.
     * If you are using a sql query, then the where clause that is provided to the queryDTO cannot properly handle date filtering because
     * of the different ways mysql/oracle handle dates. If you need date filtering, you should definitely be using the hql (in fact, always use
     * hql if its available as you'll get significantly better support for it as its a part of the hibernate system and isn't running raw queries
     * against the database). With the sql, you'll provide a sql query to the queryDTO to execute on the backend. There is no security being done
     * on any of the data being returned by this query. If someone wanted to completely wipe the database with this, they could do it trivially so use
     * it wisely. With great power comes great responsibility.
     * 
     * @param query
     *            QueryDTO - QueryDTO - provides start, limit, sorting, and filtering (See QueryDTO for more information)
     */

	@SuppressWarnings({ "rawtypes", "unchecked" })
	@RequestMapping(value = "/dbapi/getRecordData", method = { RequestMethod.POST, RequestMethod.GET }, headers = "Accept=application/json; charset=utf-8")
    @ResponseBody
    public ResponseDTO getRecordData(@ModelAttribute QueryDTO query, HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException
    {
        ResponseDTO result = new ResponseDTO();
        
        try
        {
            QueryCustomForm queryUtil = new QueryCustomForm();

            List<Map<String, Object>> recordData = queryUtil.getRecordData(query);
            
            if (query.getTableName().endsWith("_fu"))
            {
	            if (recordData.size() > 0)
	            {
	            	for (Map<String, Object> map : recordData)
	            	{
	            		map.remove("u_content");
	            	}
	            }
            }
            if (StringUtils.isNotBlank(query.getType()) && query.getType().trim().equalsIgnoreCase(QueryDTO.TABLE_TYPE))
            {
                boolean hasPagination = (query.getStart() >= 0 && query.getLimit() > 0) ? true : false;
                if (hasPagination)
                {
                    // get the total # of recs also
                    int totalRecs = queryUtil.getTotalCount(query);
                    result.setTotal(totalRecs);
                }

                // grid..so it will return a list
                result.setRecords(recordData);
            }
            else
            {
                // for form...send only 1 record
                if (recordData.size() > 0)
                {
                    Map<String, Object> record = recordData.get(0);
                    Map<String, String> urlParams = query.getUrlParams();
                    if (urlParams != null && urlParams.size() > 0)
                    {
                        Iterator<String> it = urlParams.keySet().iterator();
                        while (it.hasNext())
                        {
                            String key = it.next();
                            String value = urlParams.get(key);

                            Object currentValue = record.get(key);
                            if (currentValue != null)
                            {
                                // update only if the type is of String, else we
                                // will have to type cast it before we update
                                // the record
                                if (currentValue instanceof String)
                                {
                                    record.put(key, value);
                                }
                            }
                            else
                            {
                                record.put(key, value);
                            }
                        }
                    }

                    result.setData(record);
                }
            }

            // Lookup the record data to display on the form
            
            result.setSuccess(true);
        }
        catch (Exception e)
        {
            Log.log.error("Error getting data", e);
            result.setSuccess(false).setMessage("Error getting data");
        }
        //Test code
        //ObjectMapper mapper = new ObjectMapper();
        //String test =  mapper.writeValueAsString(result) ; 
        return result;
    }

    /**
     * Delete record data does exactly what you think it would. Its used to delete data from the database. The unique part of this is that
     * it deletes in "bulk" instead of individually. It looks to the where clause in order to figure out what to delete. So if you pass the id's of
     * a couple of records to delete, it will handle that no problem. The real power comes when you select all records across all pages. In this case,
     * the where clause is constructed as the same thing the user used when getting the list of records displayed on the screen so that we can delete all the
     * records that they could be looking at. There is no warning from this api that a lot of data is about to get deleted, that is handled on the UI.
     * 
     * Although, if no where clause is passed, the default is a -1 which is caught. We won't ever run a query to remove all the records from the table because
     * there was a lack of a where clause. Meaning "delete from users" won't actually do anything. You would have to pass something like "delete from users where 1=1"
     * if you REALLY wanted to delete all the data in a table
     * 
     * @param query
     *            QueryDTO - QueryDTO - provides start, limit, sorting, and filtering (See QueryDTO for more information)
     */
    @RequestMapping(value = "/dbapi/delete", method = { RequestMethod.POST, RequestMethod.GET }, headers = "Accept=application/json; charset=utf-8")
    @ResponseBody
    public ResponseDTO deleteRecordData(@ModelAttribute QueryDTO query, HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException
    {
        ResponseDTO result = new ResponseDTO();

        try
        {
            new QueryCustomForm().deleteRecordData(query);
            result.setSuccess(true).setMessage("Successfully deleted records");
        }
        catch (Exception e)
        {
            Log.log.error("Error deleting data", e);
            result.setSuccess(false).setMessage("Error deleting data");
        }

        return result;
    }

    /**
     * 
     * This api is use to insert or update records in a table. If 'sys_id' is available, then it acts as an UPDATE, else it acts as an INSERT. It returns the list of all the records
     * that were updated or inserted. 
     * 
        Ext.Ajax.request({
           "url":"/resolve/service/dbapi/save", <== url to submit
           "jsonData":{
              "table":"wikidoc",        <== Table Name
              "records":[
                 {
                    "U_CONTENT":"test1",    <==Clob, Varchar
                    "U_DT_ABORT_TIME":10,   <== Number, Float
                    "U_EXPIRE_ON": 1378338471707, <== Date should be in long
                    "U_HAS_ACTIVE_MODEL":"1", <== boolean - its char in DB , so its a String from UI
                    "U_RATING_BOOST":3  <== float 
                 },
                 {
                    "sys_id":"046f0df50a14026512f51e5ba38dcc7a",
                    "U_CONTENT":"test2",
                    "U_DT_ABORT_TIME":13,
                    "U_EXPIRE_ON": 1378338471707,
                    "U_HAS_ACTIVE_MODEL":"0",
                    "U_RATING_BOOST":4
                 }
              ],
              success: function(r) {
                console.log("success");
              }
           }
        })
     * 
     * @param data - list of map of columnname-date that represents 1 rec in this table. Make sure that the datatype matches what is defined in the table. 
     * @param request
     * @param response
     * @return
     * @throws ServletException
     * @throws IOException
     */
    @RequestMapping(value = "/dbapi/save", method = { RequestMethod.POST, RequestMethod.GET }, headers = "Accept=application/json; charset=utf-8")
    @ResponseBody
    //    public JsonResponse saveRecords(@RequestParam String table, @ModelAttribute ArrayList<HashMap<String, Object>> records, HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException
    public ResponseDTO saveRecords(@RequestBody DbDTO data, HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException
    {

        ResponseDTO result = new ResponseDTO();
        String username = (String) request.getAttribute(Constants.HTTP_REQUEST_USERNAME);

        try
        {
        	if (StringUtils.isNotEmpty(data.getTable()) && data.getRecords() != null)
            {
            	String dbType = ((Main) MainBase.main).getConfigSQL().getDbtype().toUpperCase();
            	List<Map<String, Object>> committedData = ServiceHibernate
                		.insertOrUpdateUsingJDBC(username, data.getTable(), data.getRecords(), dbType);
                result.setSuccess(true).setMessage("Successfully saved records").setRecords(committedData);
            }
            else
            {
                throw new Exception("Tablename and data map are mandatory.");
            }
        }
        catch (Exception e)
        {
            Log.log.error("Error saving data", e);
            result.setSuccess(false).setMessage("Error saving data");
        }

        return result;
    }

    /**
     * NOTE: This should be Deprecated as it is same as AjaxView.getMetaTableView. UI is using this as an entry point and if will have to make 
     * the change to the URL. Also, this controller is only for raw data being pulled from the tables. 
     * 
     * Get table columns does exactly that. Based on the table and the view provided (why not just the view? Great question... I have no idea since
     * the view is clearly linked to the table and giving that kind of knowledge to users is totally ridiculous) returns the list of columns that should
     * be displayed by the UI in the order that the user has configured. Say that there is a table with 100 columns, the user might only care about 10 of them
     * at any given time, so its pointless to show more than the 10. Users can configure the columns displayed in the Custom Table View screen that allows them
     * to pick which columns are displayed and in which order they should be displayed on the UI.
     * 
     * @param table
     *            The name of the table in the system to get the columns for
     * @param view
     *            The name of the view for the table that defines which columns are displayed and in which order they are displayed
     */
    @RequestMapping(value = "/dbapi/getTableColumns", method = { RequestMethod.POST, RequestMethod.GET }, headers = "Accept=application/json; charset=utf-8")
    @ResponseBody
    public ResponseDTO getTableColumns(@RequestParam String table, @RequestParam String view, HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException
    {
        String username = (String) request.getAttribute(Constants.HTTP_REQUEST_USERNAME);
        ResponseDTO result = new ResponseDTO();

        try
        {
            // Get the column metadata for the grid
            String lookupViewName = ServiceHibernate.evaluateViewLookup(view, username); //StoreUtility.evaluateViewLookup(view, username);
            RsMetaTableViewDTO tableView = metaTableService.getMetaTableView(table, lookupViewName, username, RightTypeEnum.admin);
            result.setSuccess(true).setData(tableView);
        }
        catch (Exception e)
        {
            String error = "Error getting column metadata for table : " + table + " and view :" + view;
            Log.log.error(error, e);
            result.setSuccess(false).setMessage(error);
        }

        return result;
    }

    @RequestMapping(value = "/dbapi/selectRecordData", method = { RequestMethod.POST, RequestMethod.GET }, headers = "Accept=application/json; charset=utf-8")
    @ResponseBody
    public ResponseDTO selectRecordData(@RequestParam String tableName, @RequestBody Map<String, String> params, @RequestParam List<String> selectColumns, HttpServletRequest request) throws ServletException, IOException
    {
        String username = (String) request.getAttribute(Constants.HTTP_REQUEST_USERNAME);
        ResponseDTO result = new ResponseDTO();

        try
        {
        	QueryCustomForm queryForm = new QueryCustomForm();
        	result = queryForm.selectRecordData(tableName, params, selectColumns);
        }
        catch (Exception e)
        {
            String error = "Could not read records for table: " + tableName;
            Log.log.error(error, e);
            result.setSuccess(false).setMessage(error);
        }

        return result;
    }
}
