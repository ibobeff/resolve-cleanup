/******************************************************************************
* (C) Copyright 2014
*
* Resolve Systems, LLC
*
* All rights reserved
* This software is distributed under license and may not be used, copied,
* modified, or distributed without the express written permission of
* Resolve Systems, LLC.
*
******************************************************************************/

package com.resolve.rsview.controller;

import java.io.IOException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.stereotype.Controller;
import org.springframework.stereotype.Service;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.resolve.rsview.main.RSContext;
import com.resolve.services.ServiceHibernate;
import com.resolve.services.constants.ConstantValues;
import com.resolve.services.hibernate.vo.ResolveWikiLookupVO;
import com.resolve.services.vo.QueryDTO;
import com.resolve.services.vo.ResponseDTO;
import com.resolve.util.Constants;
import com.resolve.util.Log;
import com.resolve.util.StringUtils;
import com.resolve.wiki.api.WebApi;

import net.sf.json.JSONObject;
import net.sf.json.JSONSerializer;
import net.sf.json.JsonConfig;

/**
 * A Controller to privide CRUD operation for Wiki Lookup
 *
 */
@Controller
@Service
public class AjaxWikiLookupMapping extends GenericController
{
    /**
     * Returns list of Wiki Looup mapping for the grid based on filters, sorts, pagination
     * 
     * @param query : {@link QueryDTO} provides start, limit, sorting, and filtering
     * @param request : {@link HttpServletRequest}
     * @param response : {@link HttpServletResponse}
     * @return
     * @throws ServletException
     * @throws IOException
     */
    @RequestMapping(value = "/wikilookup/list", method = { RequestMethod.GET, RequestMethod.POST }, headers = "Accept=application/json; charset=utf-8")
    @ResponseBody
    public ResponseDTO list(@ModelAttribute QueryDTO query, HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException
    {
        ResponseDTO result = new ResponseDTO();
        String username = (String) request.getAttribute(Constants.HTTP_REQUEST_USERNAME);
        try
        {
            query.setModelName("ResolveWikiLookup");
//            query.setSelectColumns("sys_id,UName,UDescription,sysUpdatedOn");
            
            List<ResolveWikiLookupVO> data = ServiceHibernate.getResolveWikiLookup(query, username);
            int total = ServiceHibernate.getTotalHqlCount(query);
            
            result.setSuccess(true).setRecords(data);
            result.setTotal(total);
        }
        catch (Exception e)
        {
            Log.log.error("Error retrieving wiki lookup listing.", e);
            result.setSuccess(false).setMessage("Error retrieving wiki lookup listing.");
        }
        
        return result;
    }
    
    /**
     * Gets data for a specific ResolveWikiLookup to be edited based on sysId
     * 
     * @param id : String SysId representing the WikiLookup
     * @param request : {@link HttpServletRequest}
     * @param response : {@link HttpServletResponse}
     * @return
     * @throws ServletException
     * @throws IOException
     */
    @RequestMapping(value = "/wikilookup/get", method = { RequestMethod.POST }, headers = "Accept=application/json; charset=utf-8")
    @ResponseBody
    public ResponseDTO get(@RequestParam String id, HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException
    {
        ResponseDTO result = new ResponseDTO();
        String username = (String) request.getAttribute(Constants.HTTP_REQUEST_USERNAME);
        try
        {
            ResolveWikiLookupVO vo = ServiceHibernate.findResolveWikiLookupById(id, username);
            result.setData(vo);
            result.setSuccess(true);
            
        }
        catch (Exception e)
        {
            Log.log.error("Error while retrieving wiki lookup.", e);
            result.setSuccess(false).setMessage("Error while retrieving wiki lookup.");
        }

        return result;
    }
    
    /**
     * Delete the ResolveWikiLookup based on array of sysIds
     * 
     * @param ids : String array of Wiki Lookup SysIds to be deleted
     * @param request : {@link HttpServletRequest}
     * @param response : {@link HttpServletResponse}
     * @return
     * @throws ServletException
     * @throws IOException
     */
    @RequestMapping(value = "/wikilookup/delete", method = { RequestMethod.POST }, headers = "Accept=application/json; charset=utf-8")
    @ResponseBody
    public ResponseDTO delete(@RequestBody String[] ids, HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException
    {
        ResponseDTO result = new ResponseDTO();
        String username = (String) request.getAttribute(Constants.HTTP_REQUEST_USERNAME);
        
        try
        {
            ServiceHibernate.deleteResolveWikiLookupByIds(ids, username);
            result.setSuccess(true);
        }
        catch (Exception e)
        {
            Log.log.error("Error while deleting wiki lookup.", e);
            result.setSuccess(false).setMessage("Error while deleting wiki lookup.");
        }

        return result;
    }
    
    /**
     * Saves/Update an ResolveWikiLookup
     * 
     * @param jsonProperty : JSONObject representing WikiLookup to be saved.
     * @param request : {@link HttpServletRequest}
     * @param response : {@link HttpServletResponse}
     * @return
     * @throws ServletException
     * @throws IOException
     */
    @RequestMapping(value = "/wikilookup/save", method = { RequestMethod.POST }, headers = "Accept=application/json; charset=utf-8")
    @ResponseBody
    public ResponseDTO save(@RequestBody JSONObject jsonProperty, HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException
    {
      //using jackson to deserialize the json
//        String json = jsonProperty.toString();
//        ResolveWikiLookupVO entity = new ObjectMapper().readValue(json, ResolveWikiLookupVO.class);
        
        //convert json to VO - spring does not work as the VO are not 100% java bean
        JsonConfig jsonConfig = new JsonConfig();
        jsonConfig.setRootClass(ResolveWikiLookupVO.class);
        ResolveWikiLookupVO entity = (ResolveWikiLookupVO) JSONSerializer.toJava(jsonProperty, jsonConfig);
        
        ResponseDTO result = new ResponseDTO();
        String username = (String) request.getAttribute(Constants.HTTP_REQUEST_USERNAME);
        
        try
        {
            ResolveWikiLookupVO vo = ServiceHibernate.saveResolveWikiLookup(entity, username);
            result.setSuccess(true).setData(vo);
        }
        catch (Exception e)
        {
            Log.log.error("Error while saving wiki lookup.", e);
            result.setSuccess(false).setMessage("Error while saving wiki lookup.");
        }

        return result;
    }
    /**
     * Match a given lookup against the WikiLookup present in resolve and forward the request to matched WikiDoc. If no match found, forward the request to the Default WikiDoc.
     * <br>
     * @param lookup : String lookup to me matched.
     * @param request : {@link HttpServletRequest}
     * @param response : {@link HttpServletResponse}
     * @return
     * @throws ServletException
     * @throws IOException
     */
    @RequestMapping(value = "/wikilookupnotblocked/match", method = { RequestMethod.POST }, headers = "Accept=application/json; charset=utf-8")
    @ResponseBody
    public ResponseDTO match(@RequestParam("lookup") String lookup, HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException
    {
        ResponseDTO result = new ResponseDTO();
        String username = (String) request.getAttribute(Constants.HTTP_REQUEST_USERNAME);
        Map<String, String> hmValues = new HashMap<String, String>();
        try
        {
            WebApi webApi = (WebApi) RSContext.appContext.getBean(ConstantValues.WIKI_SPRINGID_FOR_WEBAPI); 
            hmValues = webApi.lookupAction(request, response);
        }
        catch (Exception e)
        {
            Log.log.error("Error matching the lookup", e);
            result.setSuccess(false).setMessage("Error:" + e.getMessage());
        }
        //replaceAll call will remove the lookup=xxx from the url to prevent the UI refreshing relentlessly.
        String forwardUrl = hmValues.get(ConstantValues.WIKI_FORWARD_URL_KEY).replaceAll("([lookup=]+)=([^&=]+)(&|$)", "");
        forwardUrl = StringUtils.removeNewLineAndCarriageReturn(forwardUrl);
        
        String docFullName = parseDocumentName(forwardUrl);
        
//      result.setData(forwardUrl);
        result.setData(docFullName);    
        result.setSuccess(true);

        return result;
    }
    
    private String parseDocumentName(String forwardUrl)
    {
//        String rsclientUrl = "/resolve/jsp/rsclient.jsp?wiki=";
        String url=forwardUrl;
        if(forwardUrl.indexOf('?')!=-1)
            url = forwardUrl.substring(0, forwardUrl.indexOf('?'));
        String docName = url.substring(url.lastIndexOf('/')+1);
        
        String urlPart = url.substring(0, url.lastIndexOf('/'));
        String ns = urlPart.substring(urlPart.lastIndexOf('/')+1);
        
        String docFullName = ns + "." + docName;
        String parameters = forwardUrl.substring(forwardUrl.indexOf('?')+1);
        
//        rsclientUrl += docFullName + "&" + parameters;
        
        
        return docFullName;
    }
}
