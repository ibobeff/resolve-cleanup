package com.resolve.rsview.controller;

import java.util.ArrayList;
import java.util.List;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.resolve.services.hibernate.util.ArtifactTypeUtil;
import com.resolve.services.hibernate.util.ConverterUtil;
import com.resolve.services.hibernate.vo.ArtifactTypeVO;
import com.resolve.services.vo.ArtifactTypeDTO;
import com.resolve.services.vo.GenericDictionaryItemDTO;
import com.resolve.services.vo.ResponseDTO;
import com.resolve.util.Log;

@Controller
@RequestMapping("/at")
public class AjaxArtifactType extends GenericController {
	private static final String ERROR_RETRIEVING_DATA = "Error retrieving data";
	private static final String ERROR_NOT_FOUND_DATA = "Data not found";
	private static final String ERROR_SAVING_DATA = "Error saving data";
	private static final String ERROR_DELETING_DATA = "Error deleting data";
	private static final String SUCCESS_DELETING_DATA = "%s records deleted";

	@RequestMapping(method = RequestMethod.GET)
	@ResponseBody
	public ResponseDTO<List<ArtifactTypeDTO>> list() {
		ResponseDTO<List<ArtifactTypeDTO>> result = new ResponseDTO<>();
		try {
			List<ArtifactTypeVO> data = ArtifactTypeUtil.getAllArtifactType();
			List<ArtifactTypeDTO> dtoData = new ArrayList<>(data.size());
			for (ArtifactTypeVO vo : data) {
				dtoData.add(ConverterUtil.convertArtifactTypeVOToDTO(vo));
			}

			result.setSuccess(true).setData(dtoData);
		} catch (Exception e) {
			Log.log.error(ERROR_RETRIEVING_DATA, e);
			result.setSuccess(false).setMessage(ERROR_RETRIEVING_DATA);
		}

		return result;
	}

	@RequestMapping(value = "/{name}", method = RequestMethod.GET)
	@ResponseBody
	public ResponseDTO<ArtifactTypeDTO> getByName(@PathVariable String name) {
		ResponseDTO<ArtifactTypeDTO> result = new ResponseDTO<>();
		try {
			ArtifactTypeVO vo = ArtifactTypeUtil.getArtifactTypeByName(name);
			if (vo != null) {
				result.setSuccess(true).setData(ConverterUtil.convertArtifactTypeVOToDTO(vo));
			} else {
				result.setSuccess(false).setMessage(ERROR_NOT_FOUND_DATA);
			}
		} catch (Exception e) {
			Log.log.error(ERROR_RETRIEVING_DATA, e);
			result.setSuccess(false).setMessage(ERROR_RETRIEVING_DATA);
		}

		return result;
	}

	@RequestMapping(method = RequestMethod.POST)
	@ResponseBody
	public ResponseDTO<ArtifactTypeDTO> save(@RequestBody ArtifactTypeDTO dto) {
		ResponseDTO<ArtifactTypeDTO> result = new ResponseDTO<>();
		try {
			ArtifactTypeVO vo = ArtifactTypeUtil.saveArtifact(dto);
			result.setSuccess(true).setData(ConverterUtil.convertArtifactTypeVOToDTO(vo));
		} catch (Exception e) {
			Log.log.error(ERROR_SAVING_DATA, e);
			result.setSuccess(false).setMessage(ERROR_SAVING_DATA);
		}

		return result;
	}

	@RequestMapping(value = "/{name}/add", method = RequestMethod.POST)
	@ResponseBody
	public ResponseDTO<ArtifactTypeDTO> add(@PathVariable String name, @RequestBody GenericDictionaryItemDTO dto) {
		ResponseDTO<ArtifactTypeDTO> result = new ResponseDTO<>();
		try {
			ArtifactTypeVO vo = ArtifactTypeUtil.getArtifactTypeByName(name);
			ArtifactTypeVO savedVO = ArtifactTypeUtil.addArtifactItem(vo, dto);
			if (savedVO != null) {
				result.setSuccess(true).setData(ConverterUtil.convertArtifactTypeVOToDTO(savedVO));
			} else {
				result.setSuccess(false).setMessage(ERROR_SAVING_DATA);
			}
		} catch (Exception e) {
			Log.log.error(ERROR_SAVING_DATA, e);
			result.setSuccess(false).setMessage(ERROR_SAVING_DATA);
		}

		return result;
	}

	@RequestMapping(value = "/{name}/remove", method = RequestMethod.POST)
	@ResponseBody
	public ResponseDTO<ArtifactTypeDTO> remove(@PathVariable String name, @RequestBody GenericDictionaryItemDTO dto) {
		ResponseDTO<ArtifactTypeDTO> result = new ResponseDTO<>();
		try {
			ArtifactTypeVO vo = ArtifactTypeUtil.getArtifactTypeByName(name);
			ArtifactTypeVO savedVO = ArtifactTypeUtil.removeArtifactItem(vo, dto);
			if (savedVO != null) {
				result.setSuccess(true).setData(ConverterUtil.convertArtifactTypeVOToDTO(savedVO));
			} else {
				result.setSuccess(false).setMessage(ERROR_SAVING_DATA);
			}
		} catch (Exception e) {
			Log.log.error(ERROR_SAVING_DATA, e);
			result.setSuccess(false).setMessage(ERROR_SAVING_DATA);
		}

		return result;
	}

	@RequestMapping(value = "/{names}", method = RequestMethod.DELETE)
	@ResponseBody
	public ResponseDTO<ArtifactTypeDTO> deleteBulk(@PathVariable List<String> names) {
		ResponseDTO<ArtifactTypeDTO> result = new ResponseDTO<>();
		try {
			boolean deleteResult = true;
			for (String name : names) {
				deleteResult = deleteResult && ArtifactTypeUtil.deleteArtifactType(name);
			}
			result.setSuccess(deleteResult).setMessage(String.format(SUCCESS_DELETING_DATA, names.size()));
			if (!deleteResult) {
				result.setMessage(ERROR_DELETING_DATA);
			}
		} catch (Exception e) {
			Log.log.error(ERROR_DELETING_DATA, e);
			result.setSuccess(false).setMessage(ERROR_DELETING_DATA);
		}

		return result;
	}
}
