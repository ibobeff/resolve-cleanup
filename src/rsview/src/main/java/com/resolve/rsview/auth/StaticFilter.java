/******************************************************************************
* (C) Copyright 2014
*
* Resolve Systems, LLC
*
* All rights reserved
* This software is distributed under license and may not be used, copied,
* modified, or distributed without the express written permission of
* Resolve Systems, LLC.
*
******************************************************************************/

package com.resolve.rsview.auth;

import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpServletResponseWrapper;

import org.owasp.esapi.ESAPI;

import com.resolve.util.Log;
import com.resolve.util.StringUtils;

public class StaticFilter implements Filter
{
    public void doFilter(ServletRequest req, ServletResponse resp, FilterChain chain) throws IOException, ServletException
    {
        HttpServletRequest request = (HttpServletRequest) req; 
        HttpServletResponse response = new XFrameOptionsHttpServletResponse((HttpServletResponse)resp); 
 
         Map<String,String[]> params = request.getParameterMap();
         ResolveHttpRequestWrapper rhrw = null;
         
         if (params.get("_dc")!=null)
         {
             String start = null;
             if (params.get("start")!=null)
             {
                 String[] vals = params.get("start");
                 start = vals[0];
             }

             String limit = null;
             if (params.get("limit")!=null)
             {
                 String[] vals = params.get("limit");
                 limit = vals[0];
             }

             String page = null;
             if (params.get("page")!=null)
             {
                 String[] vals = params.get("page");
                 page = vals[0];
             }

             Map<String, String[]> newParams = new HashMap<String, String[]>();

             boolean changed = false;
             if (start!=null && limit!=null && page!=null)
             {
                 if (StringUtils.isNotEmpty(start))
                 {
                     try 
                     {
                         Long.parseLong(start);
                     }  
                     catch(Exception ex)
                     {
                         changed = true;
                         newParams.put("start", new String[] { "0"});
                         Log.log.error("parameter start has wrong value",ex);
                     }
                 }
                 
                 if (StringUtils.isNotEmpty(limit))
                 {
                     try 
                     {
                         Long.parseLong(limit);
                     }  
                     catch(Exception ex)
                     {
                         changed = true;
                         newParams.put("limit", new String[] { "1"});
                         Log.log.error("parameter limit has wrong value",ex);
                     }
                 }

                 if (StringUtils.isNotEmpty(page))
                 {
                     try 
                     {
                         Long.parseLong(page);
                     }  
                     catch(Exception ex)
                     {
                         changed = true;
                         newParams.put("page", new String[] { "1"});
                         Log.log.error("parameter page has wrong value",ex);
                     }
                 }
             }
             
             if(changed)
             {
                 rhrw = new ResolveHttpRequestWrapper(request, newParams); 
             }
         }
         
         if (!response.isCommitted())
         {
             if (StringUtils.isBlank(response.getHeader("p3p")))
             {
                 response./*addHeader*/setHeader("p3p", "CP=\"JUSTFORIE\"");
             }
            
             String iframeid = Authentication.getIFrameId(request);
             
             if (StringUtils.isBlank(iframeid))
             {
                 iframeid = Authentication.getIFrameIdFromIFramePreLoginSessionCache(request);
             }
             
             Map<String, String[]> securityHeaderSuffixes;
             
             if (rhrw!=null)
             {
                 securityHeaderSuffixes = Authentication.getSecurityHeaderSuffixes(iframeid, rhrw);
             }
             else
             {
                 securityHeaderSuffixes = Authentication.getSecurityHeaderSuffixes(iframeid, request);
             }
             
             String cspHeader = request.isSecure() ? 
            		 			AuthConstants.CONTENT_SECURITY_POLICY_UPGRADE_INSECURE_REQUESTS.getValue() : null;
             
             if (securityHeaderSuffixes != null && 
            	 securityHeaderSuffixes.containsKey(AuthConstants.CONTENT_SECURITY_POLICY_HEADER.getValue()))
             {
                 cspHeader = StringUtils.isNotBlank(cspHeader) ? 
                		 	 cspHeader + "; " + 
                		 	 securityHeaderSuffixes.get(AuthConstants.CONTENT_SECURITY_POLICY_HEADER.getValue())[0] :
                             securityHeaderSuffixes.get(AuthConstants.CONTENT_SECURITY_POLICY_HEADER.getValue())[0];
                 /*response.setHeader(Authentication.CONTENT_SECURITY_POLICY_HEADER, 
                                    securityHeaderSuffixes.get(Authentication.CONTENT_SECURITY_POLICY_HEADER));*/
             }
             
             if (StringUtils.isNotBlank(cspHeader))
             {
                 response.setHeader(AuthConstants.CONTENT_SECURITY_POLICY_HEADER.getValue(), cspHeader);
             }
            
             //response.setHeader(Authentication.X_FRAME_OPTIONS_HEADER, allowedURLsSuffix);
            
             if (securityHeaderSuffixes != null && 
            	 securityHeaderSuffixes.containsKey(AuthConstants.X_FRAME_OPTIONS_HEADER.getValue()))
             {
                 String[] xFrameOptionsHeaderVals = 
                		 			securityHeaderSuffixes.get(AuthConstants.X_FRAME_OPTIONS_HEADER.getValue());
                 
                 for (int i = 0; i < xFrameOptionsHeaderVals.length; i++)
                 {
                     response.addHeader(AuthConstants.X_FRAME_OPTIONS_HEADER.getValue(), xFrameOptionsHeaderVals[i]);
                 }
             }
            
             response.setHeader(AuthConstants.X_XSS_PROTECTTION_HEADER.getValue(), 
            		 			AuthConstants.X_XSS_PROTECTTION_ENABLED_MODE_BLOCKED.getValue());
             response.setHeader(AuthConstants.X_CONTENT_TYPE_OPTIONS_HEADER.getValue(), 
            		 			AuthConstants.X_CONTENT_TYPE_OPTIONS_NOSNIFF.getValue());
         }
        
         if (rhrw!=null)
         {
             chain.doFilter(rhrw, new HttpServletResponseWrapper(response) {
                public void setHeader(String name, String value) {
                    if (!"etag".equalsIgnoreCase(name)) {
                        ESAPI.httpUtilities().setHeader((HttpServletResponse)super.getResponse(), name, value);
                        //super.setHeader(name, value);
                    }
                }
             });
         }
         else
         {
             chain.doFilter(request, new HttpServletResponseWrapper(response) {
                public void setHeader(String name, String value) {
                    if (!"etag".equalsIgnoreCase(name)) {
                        //ESAPI.httpUtilities().setHeader((HttpServletResponse)super.getResponse(), name, value);
                         super.setHeader(name, value);
                    }
                }
             });
         }
    }

    public void destroy()
    {
    }

    public void init(FilterConfig arg0) throws ServletException
    {
//        javax.servlet.ServletContext ctx = arg0.getServletContext();
//        System.out.println("context is " + arg0.getServletContext());
    }
}
