/******************************************************************************
* (C) Copyright 2014
*
* Resolve Systems, LLC
*
* All rights reserved
* This software is distributed under license and may not be used, copied,
* modified, or distributed without the express written permission of
* Resolve Systems, LLC.
*
******************************************************************************/

package com.resolve.rsview.main;

import java.io.File;

import org.neo4j.backup.OnlineBackup;

import com.resolve.execute.ExecuteJava;
import com.resolve.execute.ExecuteResult;
import com.resolve.graph.GraphManager;
import com.resolve.util.FileUtils;
import com.resolve.util.Log;
import com.resolve.util.StringUtils;

public class Backup
{
    public static boolean backIndexing; 
    
    public static void backup()
    {
        // backup graphdb
        Backup.backupGraphDB();
    } // backup
    
    // backup graphdb
    public static void backupGraphDB()
    {
        //backup(RSContext.getWebHome()+"/WEB-INF/graphdb");
        String filename = RSContext.getWebHome()+"/WEB-INF/graphdb";
        
        // init dir names
        File dstDir = FileUtils.getFile(filename+".1");
        File oldDir = FileUtils.getFile(filename+".2");
        
        try
        {
            // remove backup.22 dir
            if (oldDir.exists())
            {
                Log.log.info("Removing backup.2 dir: "+oldDir.getAbsolutePath());
                FileUtils.deleteDirectory(oldDir);
            }
        }
        catch (Exception e)
        {
            Log.log.error(e.getMessage(), e);
        }
        
        try
        {
            // rename backup.1 to backup.2
            if (dstDir.exists())
            {
                Log.log.info("Moving backup.1 to dir: "+oldDir.getAbsolutePath());
                dstDir.renameTo(oldDir);
            }
        }
        catch (Exception e)
        {
            Log.log.error(e.getMessage(), e);
        }
            
        
        // backup server
        String server;
        int port;
        String property = GraphManager.configNeo.get("online_backup_server");//((Main)Main.main).configNeo.get("online_backup_server");
        if (StringUtils.isEmpty(property))
        {
            server = Main.main.configId.getIpaddress();
            port = 6372;
        }
        else
        {
            String[] serverPort = property.split(":");
            server = serverPort[0];
            port = Integer.parseInt(serverPort[1]);
        }
        
        OnlineBackup onlineBackup = OnlineBackup.from(server, port);
        onlineBackup.full(RSContext.getWebHome()+"/WEB-INF/graphdb.1");
        
    } // backupGraphDB
        
    static void backup(String filename)
    {
        File srcDir = FileUtils.getFile(filename);
        File dstDir = FileUtils.getFile(filename+".1");
        File oldDir = FileUtils.getFile(filename+".2");
	        
        try
        {
            // remove backup.22 dir
	        if (oldDir.exists())
	        {
	            Log.log.info("Removing backup.2 dir: "+oldDir.getAbsolutePath());
	            FileUtils.deleteDirectory(oldDir);
	        }
        }
        catch (Throwable t)
        {
            Log.log.error(t.getMessage(), t);
        }
	        
        try
        {
	        // rename backup.1 to backup.2
	        if (dstDir.exists())
	        {
	            Log.log.info("Moving backup.1 to dir: "+oldDir.getAbsolutePath());
	            dstDir.renameTo(oldDir);
	        }
            
        }
        catch (Throwable t)
        {
            Log.log.error(t.getMessage(), t);
        }
        
        try
        {
            // copy to backup dir
            Log.log.info("Copying to backup dir: "+dstDir.getAbsolutePath());
	        FileUtils.copyDirectory(srcDir, dstDir);
        }
        catch (Throwable t)
        {
            Log.log.error(t.getMessage(), t);
        }
    } // backup
    
    public static void checkIndex()
    {
        String indexDirName = RSContext.getWebHome()+"/WEB-INF/index";
        File indexDir = FileUtils.getFile(indexDirName);
        if (indexDir.exists())
        {
	        // get list of index directories
            File[] indexDirs = indexDir.listFiles();
            for (int i=0; i < indexDirs.length; i++)
            {
                checkIndexDir(indexDirs[i]);
            }
        }
        
    } // checkIndex
    
    static void checkIndexDir(File dir)
    {
        if (dir != null && dir.exists())
        {
            //java -cp   C:\project\resolve3\dist\lib\lucene.jar   org.apache.lucene.index.CheckIndex   C:\project\resolve3\dist\tomcat\webapps\resolve\WEB-INF\index\posts   -fix
            ExecuteJava exec = new ExecuteJava(RSContext.getResolveHome(), null, "lib/lucene.jar");
            ExecuteResult result = exec.execute("org.apache.lucene.index.CheckIndex", "\""+dir.getAbsolutePath()+"\" -fix", null, true, 1800, "CHECKINDEX: "+dir.getAbsolutePath());
            Log.log.info(result.getResultString());
        }
    } // checkIndexDir
    
} // Backup
