package com.resolve.rsview.sso.authenticator;

import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import com.google.gson.Gson;
import com.resolve.util.ERR;
import com.resolve.util.StringUtils;

public class SSOAuthenticationDTO
{

    private final String username;
    private final List<String> groupsList;
    private final long creationTime = System.currentTimeMillis();
    private final long sessionTimeout = 30 * 60 * 1000; // 30 minutes
    private final ERR errCode;

    public SSOAuthenticationDTO(String username, List<String> groupsList)
    {
        this.username = username;
        this.groupsList = groupsList;
        errCode = null;
    }

    public SSOAuthenticationDTO(ERR errCode) {
    	this.username = null;
    	this.groupsList = null;
    	this.errCode = errCode;
    }
    
    public String getUsername()
    {
        return username;
    }

    public List<String> getGroupsList()
    {
        return groupsList;
    }

    public long getCreationTime()
    {
        return creationTime;
    }

    public long getSessionTimeout()
    {
        return sessionTimeout;
    }

    public ERR getErrCode() {
    	return errCode;
    }
    
    @Override
    public String toString()
    {
        Map<String, Object> map = new LinkedHashMap<String, Object>();
        
        if (errCode == null) {
        	map.put("username", this.username);
        	map.put("groupsList", this.groupsList);
        	map.put("creationTime", this.creationTime);
        	map.put("sessionTimeout", this.sessionTimeout);
        } else {
        	map.put("errCode.code", this.errCode.getCode());
        	map.put("errCode.message", this.errCode.getMessage());
        }
        Map<String, Map<String, Object>> mainMap = new HashMap<String, Map<String, Object>>(1);
        mainMap.put(this.getClass().getSimpleName(), map);
        Gson gson = new Gson();
        String json = gson.toJson(mainMap);
        return json;
    }

}
