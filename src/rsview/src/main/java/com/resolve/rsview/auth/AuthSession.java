/******************************************************************************
* (C) Copyright 2014
*
* Resolve Systems, LLC
*
* All rights reserved
* This software is distributed under license and may not be used, copied,
* modified, or distributed without the express written permission of
* Resolve Systems, LLC.
*
******************************************************************************/

package com.resolve.rsview.auth;

import java.io.Serializable;
import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

import com.resolve.util.CryptUtils;
import com.resolve.util.StringUtils;

public class AuthSession implements Serializable
{
    private static final long serialVersionUID = 6414621379344344661L;
    
    String token;
    Long createTime;
    Long updateTime;
    String username;
    String host;
    String reqURLAuthority;
    String timezone;
    String ssoType;
    Long sessionTimeout;
    String clientReqURLHostName;
    String ssoSessionId;
    Long lastSSOValidationTime;
    String userCredentials;
    String iFrameId;
    String sessionId;
    Map<String, Long> clientSessionIdToUpdateTime;
    String sSOServiceProvider;
    Map<String, Map<String, String[]>> clntSessIdToMockSSOSrvcData; // Used only to mock SSO Service Provider Data 
    
    public AuthSession()
    {
        createTime = System.currentTimeMillis();
        updateTime = createTime;
        lastSSOValidationTime = createTime;
        clientSessionIdToUpdateTime = new ConcurrentHashMap<String, Long>();
        clntSessIdToMockSSOSrvcData = new ConcurrentHashMap<String, Map<String, String[]>>();
    } // AuthSession
    
    public String toString()
    {
        return "[ Authenticates Session: token=" + token + ", createTime=" + createTime + ", updateTime=" + updateTime + 
               ", username=" + username + ", host=" + host + ", Request URL Authority=" + reqURLAuthority +
               (StringUtils.isNotBlank(getSSOType()) ? ", ssoType=" + getSSOType() : "") +
               (StringUtils.isNotBlank(getSSOSessionId()) ? ", ssoSessionId=" + getSSOSessionId() : "") + 
               ", sessionTimeout=" + sessionTimeout + ", clientReqURLHostName=" + clientReqURLHostName +
               ", lastSSOValidationTime=" + lastSSOValidationTime + ", userCredentials=" + 
               (StringUtils.isNotBlank(userCredentials) ? "****" : "No User Credentials") + 
               (StringUtils.isNotBlank(getIFrameId()) ? ", iFrameId=" + getIFrameId() : "") +
               (StringUtils.isNotBlank(getSessionId()) ? ", sessionId=" + getSessionId() : "") + 
               ", clientSessionIdToUpdateTime=" + StringUtils.mapToString(clientSessionIdToUpdateTime, "=", ", ") +
               (StringUtils.isNotBlank(getSSOServiceProvider()) ? ", sSOServiceProvider=" + getSSOServiceProvider() : "") +
               (clntSessIdToMockSSOSrvcData != null && !clntSessIdToMockSSOSrvcData.isEmpty() ? 
                ", clntSessIdToMockSSOSrvcData=" + StringUtils.mapToString(clntSessIdToMockSSOSrvcData, "=", ", ") : "") + "]";
    } // toString

    public Long getUpdateTime()
    {
        return updateTime;
    }

    public void setUpdateTime(Long updateTime)
    {
        this.updateTime = updateTime;
    }

    public Long getCreateTime()
    {
        return createTime;
    }

    public void setCreateTime(Long createTime)
    {
        this.createTime = createTime;
    }

    public String getUsername()
    {
        return username;
    }

    public void setUsername(String username)
    {
        this.username = username;
    }

    public String getToken()
    {
        return token;
    }

    public void setToken(String token)
    {
        this.token = token;
    }

    public String getHost()
    {
        return host;
    }

    public void setHost(String host)
    {
        this.host = host;
    }

    public String getReqURLAuthority()
    {
        return reqURLAuthority;
    }

    public void setReqURLAuthority(String reqURLAuthority)
    {
        this.reqURLAuthority = reqURLAuthority;
    }
    
    public String getTimezone()
    {
        return timezone;
    }

    public void setTimezone(String timezone)
    {
        this.timezone = timezone;
    }
    
    public String getSSOType()
    {
        return ssoType;
    }
    
    public void setSSOType(String ssoType)
    {
        this.ssoType = ssoType;
    }

    public Long getSessionTimeout()
    {
        return sessionTimeout;
    }

    public void setSessionTimeout(Long sessionTimeout)
    {
        this.sessionTimeout = sessionTimeout;
    }
    
    public String getClientReqURLHostName()
    {
        return clientReqURLHostName;
    }

    public void setClientReqURLHostName(String clientReqURLHostName)
    {
        this.clientReqURLHostName = clientReqURLHostName;
    }
    
    public String getSSOSessionId()
    {
        return ssoSessionId;
    }
    
    public void setSSOSessionId(String ssoSessionId)
    {
        this.ssoSessionId = ssoSessionId;
    }
    
    public Long getLastSSOValidationTime()
    {
        return lastSSOValidationTime;
    }

    public void setLastSSOValidationTime(Long lastSSOValidationTime)
    {
        this.lastSSOValidationTime = lastSSOValidationTime;
    }
    
    public String getUserCredentials() throws Exception
    {
        if (StringUtils.isNotBlank(userCredentials))
        {
            return CryptUtils.decrypt(userCredentials);
        }
        
        return userCredentials;
    }
    
    public void setUserCredentials(String userCredentials) throws Exception
    {
        if (StringUtils.isNotBlank(userCredentials))
        {
            this.userCredentials = CryptUtils.encrypt(userCredentials);
        }
    }
    
    public String getIFrameId()
    {
        return iFrameId;
    }
    
    public void setIFrameId(String iFrameId)
    {
        this.iFrameId = iFrameId;
    }
    
    public String getSessionId()
    {
        return sessionId;
    }
    
    public void setSessionId(String sessionId)
    {
        this.sessionId = sessionId;
    }
    
    public Long getUpdateTimeForSession(String sessionId)
    {
        if (clientSessionIdToUpdateTime.containsKey(sessionId))
        {
            return clientSessionIdToUpdateTime.get(sessionId);
        }
            
        return 0l;
    }
    
    public void setUpdateTimeForSession(String sessionId, Long updateTime)
    {
        clientSessionIdToUpdateTime.put(sessionId, updateTime);
        
        if (updateTime > this.updateTime)
        {
            setSessionId(sessionId);
            setUpdateTime(updateTime);
        }
    }
    
    public void removeClientSession(String sesisonId)
    {
        clientSessionIdToUpdateTime.remove(sesisonId);
    }
    
    public boolean hasClientSessions()
    {
        return !clientSessionIdToUpdateTime.isEmpty();
    }
    
    public Map<String, Long> getClientSessionIdToUpdateTime()
    {
        return clientSessionIdToUpdateTime;
    }
    
    public void updateClientSessionIdToUpdateTime(AuthSession updatedAuthSession)
    {
        Map<String, Long> updAuthSessClntSessIdToUpdTimes = updatedAuthSession.getClientSessionIdToUpdateTime();
        
        if (updAuthSessClntSessIdToUpdTimes != null && !updAuthSessClntSessIdToUpdTimes.isEmpty())
        {
            for (String updAuthSessId : updAuthSessClntSessIdToUpdTimes.keySet())
            {
                this.setUpdateTimeForSession(updAuthSessId, updAuthSessClntSessIdToUpdTimes.get(updAuthSessId));
            }
        }
    }
    
    public String getSSOServiceProvider()
    {
        return sSOServiceProvider;
    }
    
    public void setSSOServiceProvider(String sSOServiceProvider)
    {
        this.sSOServiceProvider = sSOServiceProvider;
    }
    
    public Map<String, String[]> getMockSSOSrvcDataForSession(String sessionId)
    {
        return clntSessIdToMockSSOSrvcData.get(sessionId);
    }
    
    public void setMockSSOSrvcDataForSession(String sessionId, Map<String, String[]> mockSSOSrvcData)
    {
        clntSessIdToMockSSOSrvcData.put(sessionId, mockSSOSrvcData);
    }
    
    public void removeMockSSOSrvcData(String sesisonId)
    {
        clntSessIdToMockSSOSrvcData.remove(sesisonId);
    }
    
    public boolean hasMockSSOSrvcData()
    {
        return !clntSessIdToMockSSOSrvcData.isEmpty();
    }
    
    public Map<String, Map<String, String[]>> getClntSessIdToMockSSOSrvcData()
    {
        return clntSessIdToMockSSOSrvcData;
    }
    
    public void setClntSessIdToMockSSOSrvcData(Map<String, Map<String, String[]>> clntSessIdToMockSSOSrvcData)
    {
        this.clntSessIdToMockSSOSrvcData = clntSessIdToMockSSOSrvcData;
    }
    
    
} // AuthSession
