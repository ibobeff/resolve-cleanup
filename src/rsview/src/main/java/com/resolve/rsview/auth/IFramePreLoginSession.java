/******************************************************************************
* (C) Copyright 2016
*
* Resolve Systems, LLC
*
* All rights reserved
* This software is distributed under license and may not be used, copied,
* modified, or distributed without the express written permission of
* Resolve Systems, LLC.
*
******************************************************************************/

package com.resolve.rsview.auth;

import java.io.Serializable;

import com.resolve.util.StringUtils;

public class IFramePreLoginSession implements Serializable
{
    private static final long serialVersionUID = 6329681015724163421L;
    
    String sessionId;
    Long createTime;
    String iFrameId;
    String refererHost;
    
    public IFramePreLoginSession()
    {
        createTime = System.currentTimeMillis();
    } // IFramePreLoginSession
    
    public IFramePreLoginSession(String sessionId, String iFrameId, String refererHost)
    {
        createTime = System.currentTimeMillis();
        this.sessionId = sessionId;
        this.iFrameId = iFrameId;
        this.refererHost = refererHost;
    } // IFramePreLoginSession
    
    public String toString()
    {
        return "[ iframe Pre Login Session: sessionId=" + sessionId + ", createTime=" + createTime + 
               (StringUtils.isNotBlank(getIFrameId()) ? ", iFrameId=" + getIFrameId() : "") + 
               (StringUtils.isNotBlank(getRefererHost()) ? ", refererHost=" + getRefererHost() : "") + " ]";
    } // toString

    public Long getCreateTime()
    {
        return createTime;
    }

    public void setCreateTime(Long createTime)
    {
        this.createTime = createTime;
    }

    public String getSessionId()
    {
        return sessionId;
    }
    
    public void setSessionId(String sessionId)
    {
        this.sessionId = sessionId;
    }
    
    public String getIFrameId()
    {
        if (StringUtils.isNotBlank(iFrameId))
        {
            return iFrameId.toLowerCase();
        }
        
        return iFrameId;
    }
    
    public void setIFrameId(String iFrameId)
    {
        this.iFrameId = iFrameId;
    }
    
    public String getRefererHost()
    {
        return refererHost;
    }
    
    public void setRefererHost(String refererHost)
    {
        this.refererHost = refererHost;
    }
} // IFramePreLoginSession
