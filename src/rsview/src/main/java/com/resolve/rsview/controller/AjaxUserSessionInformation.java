/******************************************************************************
* (C) Copyright 2014
*
* Resolve Systems, LLC
*
* All rights reserved
* This software is distributed under license and may not be used, copied,
* modified, or distributed without the express written permission of
* Resolve Systems, LLC.
*
******************************************************************************/

package com.resolve.rsview.controller;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.resolve.rsview.util.ServerInformationUtil;
import com.resolve.services.vo.ResponseDTO;
import com.resolve.services.vo.UserSessionInfoDTO;
import com.resolve.util.Constants;
import com.resolve.util.Log;

/**
 * 
 * Controller API to get and delete User session information.
 *
 */
@Controller
public class AjaxUserSessionInformation extends GenericController
{
    private static final String ERROR_DELETING_USER_SESSIONS = "Error deleting User sessions";
	private static final String ERROR_RETRIEVING_USER_SESSION_INFO = "Error retrieving user session info";

	/**
     * Get User session information
     * 
     * @param request : {@link HttpServletRequest} containing <code>USERNAME</code> - username of the user who is logged in to Resolve
     * @param response
     * @return {@link ResponseDTO} with status true if everything goes smooth, false otherwise with the error message.
     * @throws ServletException
     * @throws IOException
     */
    @RequestMapping(value = "/user/sessioninfo/get", method = { RequestMethod.POST,RequestMethod.GET }, headers = "Accept=application/json; charset=utf-8")
    @ResponseBody
    public ResponseDTO get(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException
    {
        ResponseDTO result = new ResponseDTO();

        try
        {
            UserSessionInfoDTO dto = ServerInformationUtil.getUserSessionInfo();
            result.setData(dto);
            result.setSuccess(true);
        }
        catch (Exception e)
        {
            Log.log.error(ERROR_RETRIEVING_USER_SESSION_INFO, e);
            result.setSuccess(false).setMessage(ERROR_RETRIEVING_USER_SESSION_INFO);
        }

        return result;
    }
    
    /**
     * Delete User session information.
     * 
     * @param ids : String representing logged in users session id. It has the following format:
     * <code>[USER_NAME]/[Long Date when user logged in]/[IP From where user logged in]</code>. E.g: <code>admin/1431969932129/10.20.2.112</code>
     * <br>
     * This user session will be cleared after user loggs out. If deleted from UI, user would need to log back in.
     * @param request : {@link HttpServletRequest} containing <code>USERNAME</code> - username of the user who is logged in to Resolve
     * @param response
     * @return {@link ResponseDTO} with status true if everything goes smooth, false otherwise with the error message.
     * @throws ServletException
     * @throws IOException
     */
    @RequestMapping(value = "/user/sessioninfo/delete", method = { RequestMethod.POST,RequestMethod.GET }, headers = "Accept=application/json; charset=utf-8")
    @ResponseBody
    public ResponseDTO delete(@RequestParam String[] ids, HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException
    {
        ResponseDTO result = new ResponseDTO();
        String username = (String) request.getAttribute(Constants.HTTP_REQUEST_USERNAME);
        
        try
        {
            ServerInformationUtil.removeSession(ids, username);
            result.setSuccess(true);
        }
        catch (Exception e)
        {
            Log.log.error(ERROR_DELETING_USER_SESSIONS, e);
            result.setSuccess(false).setMessage(ERROR_DELETING_USER_SESSIONS);
        }

        return result;
    }

}
