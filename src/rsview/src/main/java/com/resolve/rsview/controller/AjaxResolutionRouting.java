package com.resolve.rsview.controller;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.codehaus.jackson.map.ObjectMapper;
import org.owasp.esapi.ESAPI;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.resolve.rsbase.MainBase;
import com.resolve.rsview.main.Main;
import com.resolve.services.GatewayUtil;
import com.resolve.services.ServiceHibernate;
import com.resolve.services.ServiceResolutionRouting;
import com.resolve.services.hibernate.util.UserUtils;
import com.resolve.services.hibernate.vo.RRRuleVO;
import com.resolve.services.hibernate.vo.RRSchemaVO;
import com.resolve.services.util.PlaybookUtils;
import com.resolve.services.vo.QueryDTO;
import com.resolve.services.vo.ResponseDTO;
import com.resolve.util.Constants;
import com.resolve.util.GMTDate;
import com.resolve.util.Log;
import com.resolve.util.MacroSubstitution;
import com.resolve.util.StringUtils;

import net.sf.json.JSONObject;

@Controller
public class AjaxResolutionRouting extends GenericController
{
	private static final String ERROR_DELETING_RR_RULE = "Error deleting ResolutionRouting rule";
	private static final String ERROR_DELETING_RR_SCHEMA = "Error deleting ResolutionRouting schema";
	private static final String ERROR_DEPLOYING_RR_RULE = "Error saving and deploying ResolutionRouting rule";
	private static final String ERROR_DOING_RID_LOOKUP = "Error doing rid lookup";
	private static final String ERROR_EXPANDING_RR_MODULES = "Error expanding ResolutionRouting modules";
	private static final String ERROR_GETTING_RR_RULE = "Error getting ResolutionRouting rule";
	private static final String ERROR_GETTING_RR_SCHEMA = "Error getting ResolutionRouting schema";
	private static final String ERROR_INVOKING_BEFORE_IMPORT_TASK = "Error invoking Before Import task";
	private static final String ERROR_LISTING_RR_MODULES = "Error listing ResolutionRouting modules";
	private static final String ERROR_LISTING_RR_RULES = "Error listing ResolutionRouting rules";
	private static final String ERROR_LISTING_RR_SCHEMA = "Error listing ResolutionRouting schema";
	private static final String ERROR_PREPARING_IMPORT_TASK = "Error preparing import task";
	private static final String ERROR_REORDERING_RR_SCHEMAS = "Error re-ordering ResolutionRouting schemas";
	private static final String ERROR_SAVING_RR_RULE = "Error saving ResolutionRouting rule";
	private static final String ERROR_SAVING_RR_SCHEMA = "Error saving ResolutionRouting schema";
    private static final String ERROR_EXPORTING_CSV = "Error exporting csv";
    private static final String ERROR_POLLING_BULK_OP_STATUS = "Error polling bulk opertaion status";

	/*-------------------------------------------rule------------------------------------------------*/
    @RequestMapping(value = "/resolutionrouting/rule/list", method = { RequestMethod.POST, RequestMethod.GET })
    @ResponseBody
    public ResponseDTO<RRRuleVO> listRules(@ModelAttribute QueryDTO query, HttpServletRequest request) throws ServletException, IOException
    {
        ResponseDTO<RRRuleVO> result = new ResponseDTO<RRRuleVO>();
        String username = (String) request.getAttribute(Constants.HTTP_REQUEST_USERNAME);

        try
        {
            query.setModelName("RRRule");
            List<RRRuleVO> records = ServiceResolutionRouting.listRules(query,username);
            int total = ServiceHibernate.getTotalHqlCount(query);

            result.setSuccess(true).setRecords(records);
            result.setTotal(total);
        }
        catch (Exception e)
        {
            Log.log.error(ERROR_LISTING_RR_RULES, e);
            result.setSuccess(false).setMessage(ERROR_LISTING_RR_RULES);
        }

        return result;
    }
    /**
     * get rid mapping rules...remember the fields in the mapping is not ordered...but the rule has schema info so the client code should be able to sort it based on that.
     * @param id
     * @param request
     * @return
     * @throws ServletException
     * @throws IOException
     */
    @RequestMapping(value = "/resolutionrouting/rule/get", method = { RequestMethod.POST, RequestMethod.GET })
    @ResponseBody
    public ResponseDTO<RRRuleVO> getRule(@RequestParam("id") String id, HttpServletRequest request) throws ServletException, IOException
    {
        ResponseDTO<RRRuleVO> result = new ResponseDTO<RRRuleVO>();
        String username = (String) request.getAttribute(Constants.HTTP_REQUEST_USERNAME);

        try
        {
            RRRuleVO vo = ServiceResolutionRouting.getRule(id, username);
            result.setSuccess(true).setData(vo);
        }
        catch (Exception e)
        {
            if (e.getMessage().contains("does not have access to Org schema belongs to"))
            {
                Log.log.debug(ERROR_GETTING_RR_RULE + ":" + e.getLocalizedMessage());
            }
            else
            {
                Log.log.error(ERROR_GETTING_RR_RULE + ":" + e.getLocalizedMessage(), e);
            }
            
            result.setSuccess(false).setMessage(ERROR_GETTING_RR_RULE);
        }

        return result;
    }
    
    @RequestMapping(value = "/resolutionrouting/rule/deploy", method = { RequestMethod.POST, RequestMethod.GET })
    @ResponseBody
    public ResponseDTO<RRRuleVO> deploy(HttpServletRequest request) throws ServletException, IOException
    {
        ResponseDTO<RRRuleVO> result = new ResponseDTO<RRRuleVO>();
      
        try
        {
        	GatewayUtil.synchronizeRoutingSchemas(null);
            result.setSuccess(true);
        }
        catch (Exception e)
        {
            Log.log.error(ERROR_DEPLOYING_RR_RULE, e);
            result.setSuccess(false).setMessage(ERROR_DEPLOYING_RR_RULE);
        }

        return result;
    }

    @RequestMapping(value = "/resolutionrouting/rule/save", method = { RequestMethod.POST, RequestMethod.GET })
    @ResponseBody
    public ResponseDTO<RRRuleVO> saveRule(@RequestBody JSONObject json, HttpServletRequest request) throws ServletException, IOException
    {
        ResponseDTO<RRRuleVO> result = new ResponseDTO<RRRuleVO>();
        String username = (String) request.getAttribute(Constants.HTTP_REQUEST_USERNAME);
      
        try
        {
            RRRuleVO vo = new ObjectMapper().readValue(json.toString(), RRRuleVO.class);
            RRRuleVO rule = ServiceResolutionRouting.saveRule(vo, username);
            result.setData(rule);
            result.setSuccess(true);
        }
        catch (Exception e)
        {
            Log.log.error(ERROR_SAVING_RR_RULE, e);
            result.setSuccess(false).setMessage(ERROR_SAVING_RR_RULE);
        }

        return result;
    }
    
    @RequestMapping(value = "/resolutionrouting/rule/delete/id", method = { RequestMethod.POST, RequestMethod.GET })
    @ResponseBody
    public ResponseDTO<RRRuleVO> deleteRules(@RequestParam("ids") List<String> ids, HttpServletRequest request) throws ServletException, IOException
    {
        ResponseDTO<RRRuleVO> result = new ResponseDTO<RRRuleVO>();
        String username = (String) request.getAttribute(Constants.HTTP_REQUEST_USERNAME);
      
        try
        {
            ServiceResolutionRouting.deleteRulesById(ids, username);
            result.setSuccess(true);
        }
        catch (Exception e)
        {
            Log.log.error(ERROR_DELETING_RR_RULE, e);
            result.setSuccess(false).setMessage(ERROR_DELETING_RR_RULE);
        }

        return result;
    }
    
    @RequestMapping(value = "/resolutionrouting/rule/delete/query", method = { RequestMethod.POST, RequestMethod.GET })
    @ResponseBody
    public ResponseDTO<RRRuleVO> deleteRules(@ModelAttribute QueryDTO query, HttpServletRequest request) throws ServletException, IOException
    {
        ResponseDTO<RRRuleVO> result = new ResponseDTO<RRRuleVO>();
        query.setModelName("RRRule");
        String username = (String) request.getAttribute(Constants.HTTP_REQUEST_USERNAME);
      
        try
        {
            ServiceResolutionRouting.deleteRulesByQuery(query, username);
            result.setSuccess(true);
        }
        catch (Exception e)
        {
            Log.log.error(ERROR_DELETING_RR_RULE, e);
            result.setSuccess(false).setMessage(ERROR_DELETING_RR_RULE);
        }

        return result;
    }
    
    @RequestMapping(value = "/resolutionrouting/rule/module/expand", method = { RequestMethod.POST, RequestMethod.GET })
    @ResponseBody
    public ResponseDTO<Map<String,Boolean>> expandModule(@ModelAttribute QueryDTO query, HttpServletRequest request) throws ServletException, IOException
    {
        ResponseDTO<Map<String,Boolean>> result = new ResponseDTO<Map<String,Boolean>>();
        String username = (String) request.getAttribute(Constants.HTTP_REQUEST_USERNAME);

        try
        {
            query.setModelName("RRRule");
            Map<String,Boolean> data = ServiceResolutionRouting.expandModule(query, username);

            result.setSuccess(true).setData(data);
            result.setTotal(data.size());
        }
        catch (Exception e)
        {
            Log.log.error(ERROR_EXPANDING_RR_MODULES, e);
            result.setSuccess(false).setMessage(ERROR_EXPANDING_RR_MODULES);
        }

        return result;
    }
    
    @RequestMapping(value = "/resolutionrouting/rule/module/list", method = { RequestMethod.POST, RequestMethod.GET })
    @ResponseBody
    public ResponseDTO<String> listModule(@ModelAttribute QueryDTO query, HttpServletRequest request) throws ServletException, IOException
    {
        ResponseDTO<String> result = new ResponseDTO<String>();
        String username = (String) request.getAttribute(Constants.HTTP_REQUEST_USERNAME);

        try
        {
            query.setModelName("RRRule");
            List<String> data = ServiceResolutionRouting.listRuleModules(query,username);

            result.setSuccess(true).setRecords(data);
            result.setTotal(data.size());
        }
        catch (Exception e)
        {
            Log.log.error(ERROR_LISTING_RR_MODULES, e);
            result.setSuccess(false).setMessage(ERROR_LISTING_RR_MODULES);
        }

        return result;
    }
    /*-------------------------------------------------------------------------------------------------*/
    
    /*-------------------------------------------schema------------------------------------------------*/
    @RequestMapping(value = "/resolutionrouting/schema/list", method = { RequestMethod.POST, RequestMethod.GET })
    @ResponseBody
    public ResponseDTO<RRSchemaVO> listSchema(@ModelAttribute QueryDTO query, HttpServletRequest request) throws ServletException, IOException
    {
        ResponseDTO<RRSchemaVO> result = new ResponseDTO<RRSchemaVO>();
        String username = (String) request.getAttribute(Constants.HTTP_REQUEST_USERNAME);
        query.setModelName("RRSchema");
        try
        {       
            List<RRSchemaVO> records = ServiceResolutionRouting.listSchema(query, username);
            int total = ServiceHibernate.getTotalHqlCount(query);
            result.setSuccess(true).setRecords(records);
            result.setTotal(total);
        }
        catch (Exception e)
        {
            Log.log.error(ERROR_LISTING_RR_SCHEMA, e);
            result.setSuccess(false).setMessage(ERROR_LISTING_RR_SCHEMA);
        }

        return result;
    }
    
    @RequestMapping(value = "/resolutionrouting/schema/order/next", method = { RequestMethod.POST, RequestMethod.GET })
    @ResponseBody
    public ResponseDTO<Integer> getNextSchemaOrder(HttpServletRequest request) throws ServletException, IOException
    {
        ResponseDTO<Integer> result = new ResponseDTO<Integer>();
        String username = (String) request.getAttribute(Constants.HTTP_REQUEST_USERNAME);

        try
        {       
            Integer data = ServiceResolutionRouting.getNextSchemaOrder(username);

            result.setSuccess(true).setData(data);
            
        }
        catch (Exception e)
        {
            Log.log.error(ERROR_GETTING_RR_SCHEMA, e);
            result.setSuccess(false).setMessage(ERROR_GETTING_RR_SCHEMA);
        }

        return result;
    }
    
    @RequestMapping(value = "/resolutionrouting/schema/get", method = { RequestMethod.POST, RequestMethod.GET })
    @ResponseBody
    public ResponseDTO<RRSchemaVO> getSchema(@RequestParam("id") String id, HttpServletRequest request) throws ServletException, IOException
    {
        ResponseDTO<RRSchemaVO> result = new ResponseDTO<RRSchemaVO>();
        String username = (String) request.getAttribute(Constants.HTTP_REQUEST_USERNAME);

        try
        {       
            RRSchemaVO data = ServiceResolutionRouting.getSchema(id, username);

            result.setSuccess(true).setData(data);
        }
        catch (Exception e)
        {
            if (e.getMessage().contains("does not have access to Org schema belongs to"))
            {
                Log.log.debug(ERROR_GETTING_RR_SCHEMA + ":" + e.getLocalizedMessage());
            }
            else
            {
                Log.log.error(ERROR_GETTING_RR_SCHEMA + ":" + e.getLocalizedMessage(), e);
            }
            
            result.setSuccess(false).setMessage(ERROR_GETTING_RR_SCHEMA);
        }

        return result;
    }
    
    @RequestMapping(value = "/resolutionrouting/schema/save", method = { RequestMethod.POST, RequestMethod.GET }, headers = "Accept=application/json; charset=utf-8")
    @ResponseBody
    public ResponseDTO<RRSchemaVO> saveSchema(@RequestBody RRSchemaVO vo, HttpServletRequest request) throws ServletException, IOException
    {
        ResponseDTO<RRSchemaVO> result = new ResponseDTO<RRSchemaVO>();
        String username = (String) request.getAttribute(Constants.HTTP_REQUEST_USERNAME);

        try
        {       
            //RRSchemaVO vo = new ObjectMapper().readValue(json, RRSchemaVO.class);            
            vo = ServiceResolutionRouting.saveSchema(vo, username, true, GMTDate.getTime(), 
            		((Main) MainBase.main).getConfigSQL());
            result.setSuccess(true).setData(vo);
            
        }
        catch (Exception e)
        {
            if (e.getMessage().contains(ServiceResolutionRouting.RESOLVE_RESERVED_FIELD_ERR_MSG_MIDDLE) || 
                e.getMessage().startsWith(ServiceResolutionRouting.RESOLVE_DUPLICATE_CASE_INSENSITIVE_SCHEMA_FIELD_NAME_PREFIX) ||
                e.getMessage().startsWith(ServiceResolutionRouting.NO_SCHEMA_FIELDS_DEFINED_ERR_MSG) ||
                e.getMessage().startsWith(ServiceResolutionRouting.INSUFFICIENT_RIGHTS_TO_REORDER_SCHEMA) ||
                e.getMessage().startsWith(ServiceResolutionRouting.ERR_MSG_SCHEMA_NOT_UPDATED_BECAUSE_IN_USE) ||
                e.getMessage().startsWith(ServiceResolutionRouting.USER_NOT_MEMBER_OF_SPECIFIED_ORG) ||
                e.getMessage().startsWith(ServiceResolutionRouting.USER_NOT_MEMBER_OF_NO_ORG))
            {
                Log.log.info(e.getLocalizedMessage());
            }
            else
            {
                Log.log.error(e.getLocalizedMessage(), e);
            }
            
            result.setSuccess(false).setMessage(e.getMessage().contains(ServiceResolutionRouting.RESOLVE_RESERVED_FIELD_ERR_MSG_MIDDLE) || 
                                                e.getMessage().startsWith(ServiceResolutionRouting.RESOLVE_DUPLICATE_CASE_INSENSITIVE_SCHEMA_FIELD_NAME_PREFIX) ||
                                                e.getMessage().startsWith(ServiceResolutionRouting.NO_SCHEMA_FIELDS_DEFINED_ERR_MSG) ||
                                                e.getMessage().startsWith(ServiceResolutionRouting.INSUFFICIENT_RIGHTS_TO_REORDER_SCHEMA) ||
                                                e.getMessage().startsWith(ServiceResolutionRouting.ERR_MSG_SCHEMA_NOT_UPDATED_BECAUSE_IN_USE) ||
                                                e.getMessage().startsWith(ServiceResolutionRouting.USER_NOT_MEMBER_OF_SPECIFIED_ORG) ||
                                                e.getMessage().startsWith(ServiceResolutionRouting.USER_NOT_MEMBER_OF_NO_ORG) ? 
                                                e.getLocalizedMessage() : ERROR_SAVING_RR_SCHEMA);
        }

        return result;
    }
    
    @RequestMapping(value = "/resolutionrouting/schema/delete", method = { RequestMethod.POST, RequestMethod.GET })
    @ResponseBody
    public ResponseDTO<RRSchemaVO> deleteSchema(@RequestParam("ids") List<String> ids, HttpServletRequest request) throws ServletException, IOException
    {
        ResponseDTO<RRSchemaVO> result = new ResponseDTO<RRSchemaVO>();
        String username = (String) request.getAttribute(Constants.HTTP_REQUEST_USERNAME);

        try
        {       
            ServiceResolutionRouting.deletSchemas(ids, username, ((Main) MainBase.main).getConfigSQL());
            result.setSuccess(true);    
        }
        catch (Exception e)
        {
            if (e.getMessage().startsWith(ServiceResolutionRouting.ERR_MSG_SCHEMA_NOT_DELETED_BECAUSE_IN_USE))
            {
                Log.log.info(e.getLocalizedMessage());
            }
            else
            {
                Log.log.error(e.getLocalizedMessage(), e);
            }
                        
            result.setSuccess(false).setMessage(e.getMessage().startsWith(ServiceResolutionRouting.ERR_MSG_SCHEMA_NOT_DELETED_BECAUSE_IN_USE)? 
                                                e.getLocalizedMessage() : ERROR_DELETING_RR_SCHEMA);
        }

        return result;
    }
    
    @RequestMapping(value = "/resolutionrouting/schema/reorder", method = { RequestMethod.POST, RequestMethod.GET })
    @ResponseBody
    public ResponseDTO<RRSchemaVO> orderSchema(@RequestBody List<RRSchemaVO> jsonSchemas, HttpServletRequest request) throws ServletException, IOException
    {
        // Comparator for RRSchemaVO based on order to sort in reverse order
        class SchemaComparatorDescOrder implements Comparator<RRSchemaVO>
        {
            Map<String, Integer> idToDBSchemaOrder;
            
            SchemaComparatorDescOrder(Map<String, Integer> idToDBSchemaOrder)
            {
                this.idToDBSchemaOrder = idToDBSchemaOrder;
            }
            
            @Override
            public int compare(RRSchemaVO arg0, RRSchemaVO arg1)
            {
                if (idToDBSchemaOrder.get(arg1.getSys_id()).intValue() < idToDBSchemaOrder.get(arg0.getSys_id()).intValue())
                {
                    return -1;
                }
                else if (idToDBSchemaOrder.get(arg1.getSys_id()).intValue() > idToDBSchemaOrder.get(arg0.getSys_id()).intValue())
                {
                    return 1;
                }
                
                return 0;
            }
        }
        
        ResponseDTO<RRSchemaVO> result = new ResponseDTO<RRSchemaVO>();
        String username = (String) request.getAttribute(Constants.HTTP_REQUEST_USERNAME);
        Map<String, Integer> idToDBSchemaOrder = new HashMap<String, Integer>();
        
        try
        {
            if (!UserUtils.isSuperUser(username))
            {
                throw new Exception("Only users with admin role are permitted to change the order.");
            }
            
            long updatedOnTime = GMTDate.getTime();
            
            ArrayList<RRSchemaVO> schemesToSaveReverseRelOrder = new ArrayList<RRSchemaVO>(jsonSchemas.size());
            int index = -1;
            for(RRSchemaVO vo:jsonSchemas) {
                //RRSchemaVO vo = mapper.readValue(json, RRSchemaVO.class);
                schemesToSaveReverseRelOrder.add(++index, vo);
                
                RRSchemaVO dbVO = ServiceResolutionRouting.getSchema(vo.getSys_id(), username);
                
                idToDBSchemaOrder.put(vo.getSys_id(), dbVO.getOrder());
            }
            
            if (schemesToSaveReverseRelOrder != null && !schemesToSaveReverseRelOrder.isEmpty())
            {
                Collections.reverse(schemesToSaveReverseRelOrder);
                
                // Schemes ordered by order in passed in RRSchemaVOs in descending order
                
                ArrayList<RRSchemaVO> schemesToSaveReverseOrder = new ArrayList<RRSchemaVO>(schemesToSaveReverseRelOrder);
                schemesToSaveReverseOrder.sort(new SchemaComparatorDescOrder(idToDBSchemaOrder));
                
                index = 1;
                for (RRSchemaVO vo : schemesToSaveReverseRelOrder)
                {
                    if (idToDBSchemaOrder.get(vo.getSys_id()).intValue() != 
                        idToDBSchemaOrder.get(schemesToSaveReverseOrder.get(index - 1).getSys_id()).intValue())
                    {
                        RRSchemaVO tmpVO = new RRSchemaVO(vo);
                        tmpVO.setOrder(idToDBSchemaOrder.get(schemesToSaveReverseOrder.get(index - 1).getSys_id()));
                        
                        ServiceResolutionRouting.saveSchema(tmpVO, username, false, updatedOnTime + (index * 1000),
                        		((Main) MainBase.main).getConfigSQL());
                    }
                    
                    index++;
                }
            }
            
            ServiceResolutionRouting.ReOrderSchemes(username, ((Main) MainBase.main).getConfigSQL());
            
            result.setSuccess(true);    
        }
        catch (Exception e)
        {
            if (e.getMessage().contains(ServiceResolutionRouting.RESOLVE_RESERVED_FIELD_ERR_MSG_MIDDLE) || 
                e.getMessage().startsWith(ServiceResolutionRouting.RESOLVE_DUPLICATE_CASE_INSENSITIVE_SCHEMA_FIELD_NAME_PREFIX) ||
                e.getMessage().startsWith(ServiceResolutionRouting.NO_SCHEMA_FIELDS_DEFINED_ERR_MSG))
            {
                Log.log.info(e.getLocalizedMessage());
            }
            else
            {
                Log.log.error(e.getLocalizedMessage(), e);
            }
                        
            result.setSuccess(false).setMessage(e.getMessage().contains(ServiceResolutionRouting.RESOLVE_RESERVED_FIELD_ERR_MSG_MIDDLE) || 
                                                e.getMessage().startsWith(ServiceResolutionRouting.RESOLVE_DUPLICATE_CASE_INSENSITIVE_SCHEMA_FIELD_NAME_PREFIX) ? 
                                                e.getLocalizedMessage() : ERROR_REORDERING_RR_SCHEMAS);
        }

        return result;
    }
    
    @SuppressWarnings("unchecked")
    @RequestMapping(value = "/resolutionrouting/routing/lookup", method = { RequestMethod.POST, RequestMethod.GET })
    @ResponseBody
    public ResponseDTO<RRRuleVO> lookup(@RequestParam("queryString") String queryString, HttpServletRequest request) throws ServletException, IOException
    {
        ResponseDTO<RRRuleVO> result = new ResponseDTO<RRRuleVO>();
        String username = (String) request.getAttribute(Constants.HTTP_REQUEST_USERNAME);

        String submittedQueryString = null;
        
        if (request.getContentType().contains("application/x-www-form-urlencoded"))
        {
            submittedQueryString = queryString;
        }
        else if (request.getContentType().contains("application/json"))
        {
            // Handle JSON parsing and creating name1=value1&name2=value2&... pairs
            String jsonString = StringUtils.toString(request.getInputStream(), "utf-8");
            
            Log.log.debug("Submitted JSON content " + jsonString);
            
            JSONObject jsonObj = StringUtils.stringToJSONObject(jsonString);
            
            if (jsonObj != null && !jsonObj.isNullObject())
            {
                Map<String, String> jsonMap = StringUtils.jsonObjectToMap(jsonObj);
                
                submittedQueryString = StringUtils.mapToString(jsonMap, "=", "&");
            }
            else
            {
                Log.log.error("/resolutionrouting/routing/lookup reuqest either has no content or submitted null JSON content {:}.");
                result.setSuccess(false).setMessage("Error: /resolutionrouting/routing/lookup reuqest either has no content or submitted null JSON content {:}.");
                return result;
            }
        }
        else
        {
            Log.log.error("/resolutionrouting/routing/lookup reuqest content type " + request.getContentType() + " is currently not supported.");
            result.setSuccess(false).setMessage("Error: /resolutionrouting/routing/lookup reuqest content type " + request.getContentType() + " is currently not supported.");
            return result;
        }
                        
        try
        {       
            RRRuleVO rule = ServiceResolutionRouting.lookup(submittedQueryString,username);
            if (rule == null) {
                result.setData(null);
                result.setSuccess(false);
                Map<String, String> params = new HashMap<String, String>();
                params.put("queryString", submittedQueryString);
                Main.getESB().sendMessage(Constants.ESB_NAME_RSCONTROL, "MAction.logNoRBEvent", params);
            } else {
            	//
                Boolean sirEnabled = rule.getSir();
                if (sirEnabled != null && sirEnabled) {
                	// Put '?' in front of queryString as an indication that it's actually a query string, then remove it after decoding
                	String decodedQueryString = ESAPI.encoder().decodeFromURL("?" + queryString).substring(1);
                	Map<String, String> parametersFromQueryString = new HashMap<String, String>(StringUtils.stringToMap(decodedQueryString, "=", "&"));
                	// Macro substitution
                    MacroSubstitution ms = new MacroSubstitution();
                    ms.addAllMappings(parametersFromQueryString);
                    // TODO : need to implement mapping in cases when none on the folowing parameters is not present
                	// create a compound reference containing all 3 possible components: reference, alertid and correlationid
                	// only if there is at least reference available in the request
                	String reference = parametersFromQueryString.get(Constants.EXECUTE_REFERENCE);
                	
                	if (StringUtils.isBlank(reference))
                	{
                	    reference = parametersFromQueryString.get(Constants.EXECUTE_REFERENCE.toLowerCase());
                	}
                	
                	String alertId = parametersFromQueryString.get(Constants.EXECUTE_ALERTID);
                	
                	if (StringUtils.isBlank(alertId))
                    {
                        alertId = parametersFromQueryString.get(Constants.EXECUTE_ALERTID.toLowerCase());
                    }
                	
                	if (StringUtils.isBlank(alertId))
                    {
                	    alertId = parametersFromQueryString.get(Constants.EXECUTE_ALERTID_UI_MIXED_CASE);
                    }
                	
                	String correlationId = parametersFromQueryString.get(Constants.EXECUTE_CORRELATIONID);
                	
                	if (StringUtils.isBlank(correlationId))
                	{
                	    correlationId = parametersFromQueryString.get(Constants.EXECUTE_CORRELATIONID.toLowerCase());
                	}
                	
                	if (StringUtils.isBlank(correlationId))
                    {
                	    correlationId = parametersFromQueryString.get(Constants.EXECUTE_CORRELATIONID_UI_MIXED_CASE);
                    }
                	
                	String orgName = parametersFromQueryString.get(Constants.EXECUTE_ORG_NAME);
                	
                	if (StringUtils.isBlank(orgName))
                	{
                	    orgName = parametersFromQueryString.get(Constants.EXECUTE_org_name);
                	}
                	
            		Map<String, Object> sirParams = new HashMap<String, Object>();
            		sirParams.put(Constants.EXECUTE_SIR_SOURCE_SYSTEM, ms.evaluate(rule.getSirSource(), true, true));
            		sirParams.put(Constants.EXECUTE_SIR_TITLE, ms.evaluate(rule.getSirTitle(), true, true));
            		sirParams.put(Constants.EXECUTE_SIR_TYPE, Constants.EXECUTE_SIR_SECURITY_TYPE);
            		sirParams.put(Constants.EXECUTE_SIR_INVESTIGATION_TYPE, ms.evaluate(rule.getSirType(), true, true));
            		sirParams.put(Constants.EXECUTE_SIR_PLAYBOOK, rule.getSirPlaybook());
            		sirParams.put(Constants.EXECUTE_SIR_SEVERITY, ms.evaluate(rule.getSirSeverity(), true, true));
            		sirParams.put(Constants.EXECUTE_SIR_OWNER, rule.getSirOwner());
            		
            		if (StringUtils.isNotBlank(reference))
            		{
            		    sirParams.put(Constants.EXECUTE_REFERENCE, reference);
            		}
            		
            		if (StringUtils.isNotBlank(alertId))
                    {
            		    sirParams.put(Constants.EXECUTE_ALERTID, alertId);
                    }
            		
            		if (StringUtils.isNotBlank(correlationId))
                    {
            		    sirParams.put(Constants.EXECUTE_CORRELATIONID, correlationId);
                    }
            		
            		if (StringUtils.isNotBlank(orgName))
            		{
            		    sirParams.put(Constants.EXECUTE_ORG_NAME, orgName);
            		}
            		
            		// check for problem ID being passed to use it (UI RR use case)
            		if (StringUtils.isNotEmpty(request.getParameter(Constants.EXECUTE_SIR_PROBLEMID_ENFORSED_BY_UI_RR))) {
            			sirParams.put(Constants.EXECUTE_SIR_PROBLEMID_ENFORSED_BY_UI_RR, request.getParameter(Constants.EXECUTE_SIR_PROBLEMID_ENFORSED_BY_UI_RR));
            		}
            		
            		// Why not call createSIRAndUpdateWSReferenceDetails directly
            		//Map<String, Object> esbResponse = Main.getESB().call(Constants.ESB_NAME_RSCONTROL, "MAction.createSIRAndUpdateWSReferenceDetails", sirParams, Constants.GLOBAL_SEND_TIMEOUT);
            		Map<String, Object> esbResponse = PlaybookUtils.createSIRAndUpdateWSReferenceDetails(sirParams);
            		
            		String sirId = (String) esbResponse.get(Constants.EXECUTE_SIR_ID);
            		String problemIdEnforced = (String) esbResponse.get(Constants.EXECUTE_PROBLEMID_ENFORCED_BY_SIR);
            		String sirPlaybook = (String) esbResponse.get(Constants.EXECUTE_SIR_PLAYBOOK);
            		rule.setSirId(sirId);
            		rule.setProblemId(problemIdEnforced);
            		rule.setSirPlaybook(sirPlaybook);
            		/*
            		// override the following flag to make sure UI RR will execute the automation
            		rule.setNewWorksheetOnly(false);
            		brought it back to the previous - the 2 tabs are back :) 
            		// override the automation to point to the GW automation, because the UI shows only GW automation when SIR is selected to be created/updated
            		rule.setAutomation(rule.getRunbook());
            		rule.setAutomationId(rule.getRunbookId());
            		*/
                }
            	//
                result.setData(rule);
                result.setSuccess(true);
            }
        }
        catch (Exception e)
        {
            Log.log.error(ERROR_DOING_RID_LOOKUP, e);
            result.setSuccess(false).setMessage(ERROR_DOING_RID_LOOKUP);
        }

        return result;
    }
    
    @RequestMapping(value = "/resolutionrouting/rule/csv/import/upload", method = {RequestMethod.GET, RequestMethod.POST} )
    @ResponseBody
    public ResponseDTO<Object> upload(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException
    {
        ResponseDTO<Object> result = new ResponseDTO<Object>();
        String username = (String) request.getAttribute(Constants.HTTP_REQUEST_USERNAME);
        try
        {
            result.setData(ServiceResolutionRouting.prepareImportTask(request, username));
            result.setSuccess(true);
        }
        catch (Exception e)
        {
            Log.log.error(ERROR_PREPARING_IMPORT_TASK, e);
            result.setSuccess(false).setMessage(ERROR_PREPARING_IMPORT_TASK);
        }
        
        return result;
    }
    
    @RequestMapping(value = "/resolutionrouting/csv/import/before", method = {RequestMethod.GET, RequestMethod.POST} )
    @ResponseBody
    public ResponseDTO<Object> beforeImport(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException
    {
        ResponseDTO<Object> result = new ResponseDTO<Object>();
        String username = (String) request.getAttribute(Constants.HTTP_REQUEST_USERNAME);
        try
        {
            result.setData(ServiceResolutionRouting.beforeImport(username));
            result.setSuccess(true);
        }
        catch (Exception e)
        {
            Log.log.error(ERROR_INVOKING_BEFORE_IMPORT_TASK, e);
            result.setSuccess(false).setMessage(ERROR_INVOKING_BEFORE_IMPORT_TASK);
        }
        
        return result;
    }
    
    @RequestMapping(value = "/resolutionrouting/rule/csv/export/init/id", method = { RequestMethod.POST, RequestMethod.GET })
    @ResponseBody
    public ResponseDTO<Map<String,String>> startExport(@RequestParam("ids") List<String> ids, @RequestParam("force") Boolean force, HttpServletRequest request) throws ServletException, IOException
    {
        ResponseDTO<Map<String,String>> result = new ResponseDTO<Map<String,String>>();
        String username = (String) request.getAttribute(Constants.HTTP_REQUEST_USERNAME);

        try
        {
            result.setData(ServiceResolutionRouting.prepareExportTask(ids,force,username));
            result.setSuccess(true);
        }
        catch (Exception e)
        {
            Log.log.error(ERROR_EXPORTING_CSV, e);
            result.setSuccess(false).setMessage(ERROR_EXPORTING_CSV);
        }

        return result;
    }
    
    @RequestMapping(value = "/resolutionrouting/rule/csv/export/init/query", method = { RequestMethod.POST, RequestMethod.GET })
    @ResponseBody
    public ResponseDTO<Map<String,String>> startExport(@ModelAttribute QueryDTO query, @RequestParam("force") Boolean force, HttpServletRequest request) throws ServletException, IOException
    {
        ResponseDTO<Map<String,String>> result = new ResponseDTO<Map<String,String>>();
        String username = (String) request.getAttribute(Constants.HTTP_REQUEST_USERNAME);
        query.setModelName("RRRule");
        try
        {
            result.setData(ServiceResolutionRouting.prepareExportTask(query, force,username));
            result.setSuccess(true);
        }
        catch (Exception e)
        {
            Log.log.error(ERROR_EXPORTING_CSV, e);
            result.setSuccess(false).setMessage(ERROR_EXPORTING_CSV);
        }

        return result;
    }
    
    @RequestMapping(value = "/resolutionrouting/rule/bulk/poll", method = { RequestMethod.POST, RequestMethod.GET })
    @ResponseBody
    //for now we don't care the type of the action coz only one bulk op is allowed in the clusters
    public ResponseDTO<JSONObject> bulkPoll(@RequestParam("tid") String tid,HttpServletRequest request) throws ServletException, IOException
    {
        ResponseDTO<JSONObject> result = new ResponseDTO<JSONObject>();
        String username = (String) request.getAttribute(Constants.HTTP_REQUEST_USERNAME);

        try
        {       
            result.setData(ServiceResolutionRouting.poll(tid,username));
            result.setSuccess(true);
        }
        catch (Exception e)
        {
            Log.log.error(ERROR_POLLING_BULK_OP_STATUS, e);
            result.setSuccess(false).setMessage(ERROR_POLLING_BULK_OP_STATUS);
        }

        return result;
    }
    
    @RequestMapping(value = "/resolutionrouting/rule/csv/export/download", method = { RequestMethod.POST, RequestMethod.GET })
    @ResponseBody
    public ResponseDTO<String> downloadCSV(@RequestParam("tid") String tid,HttpServletRequest request, HttpServletResponse response) throws ServletException
    {
        String username = (String) request.getAttribute(Constants.HTTP_REQUEST_USERNAME);
        
        try
        {
            ServiceResolutionRouting.startExportTask(tid,request,response, username);
        }
        catch (Exception e)
        {
            Log.log.error(ERROR_EXPORTING_CSV, e);
        }
      
        return null;
    }
    
    @RequestMapping(value = "/resolutionrouting/rule/activation/id", method = { RequestMethod.POST, RequestMethod.GET })
    @ResponseBody
    public ResponseDTO<Map<String,String>> activate(@RequestParam("ids") List<String> ids,@RequestParam("isActivated") Boolean isActivated,@RequestParam("force") Boolean force,HttpServletRequest request, HttpServletResponse response) throws ServletException
    {
        String username = (String) request.getAttribute(Constants.HTTP_REQUEST_USERNAME);
        ResponseDTO<Map<String,String>> resp = new ResponseDTO<Map<String,String>>();
        try
        {
            resp.setData(ServiceResolutionRouting.toggleActivationByIds(ids,isActivated,force,username));
            resp.setSuccess(true);
        }
        catch (Exception e)
        {
            Log.log.error(ERROR_EXPORTING_CSV, e);
            resp.setSuccess(false);
        }
      
        return resp;
    }
    
    @RequestMapping(value = "/resolutionrouting/rule/activation/query", method = { RequestMethod.POST, RequestMethod.GET })
    @ResponseBody
    public ResponseDTO<Map<String,String>> activate(@ModelAttribute QueryDTO query,@RequestParam("isActivated") Boolean isActivated, @RequestParam("force") Boolean force,HttpServletRequest request, HttpServletResponse response) throws ServletException
    {
        String username = (String) request.getAttribute(Constants.HTTP_REQUEST_USERNAME);
        query.setModelName("RRRule");
        ResponseDTO<Map<String,String>> resp = new ResponseDTO<Map<String,String>>();
        try
        {
            resp.setData(ServiceResolutionRouting.toggleActivationByQuery(query,isActivated,force,username));
            resp.setSuccess(true);
        }
        catch (Exception e)
        {
            Log.log.error(ERROR_EXPORTING_CSV, e);
            resp.setSuccess(false);
        }
      
        return resp;
    }
    
    @RequestMapping(value = "/resolutionrouting/rule/bulk/abort", method = { RequestMethod.POST, RequestMethod.GET })
    @ResponseBody
    public ResponseDTO<Map<String,String>> abort(@RequestParam("tid") String tid,HttpServletRequest request, HttpServletResponse response) throws ServletException
    {
        String username = (String) request.getAttribute(Constants.HTTP_REQUEST_USERNAME);
        ResponseDTO<Map<String,String>> resp = new ResponseDTO<Map<String,String>>();
        try
        {
            ServiceResolutionRouting.abort(tid, username);
            resp.setSuccess(true);
        }
        catch (Exception e)
        {
            Log.log.error(ERROR_EXPORTING_CSV, e);
            resp.setSuccess(false);
        }
      
        return resp;
    }
}
