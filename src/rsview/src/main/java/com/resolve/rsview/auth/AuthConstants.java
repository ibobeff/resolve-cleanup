/******************************************************************************
* (C) Copyright 2018
*
* Resolve Systems, LLC
*
* All rights reserved
* This software is distributed under license and may not be used, copied,
* modified, or distributed without the express written permission of
* Resolve Systems, LLC.
*
******************************************************************************/

package com.resolve.rsview.auth;

public enum AuthConstants
{
	QUERY_PARAMETER_SEPARATOR("&"),
	QUERY_PARAMETER_EQUAL_TO("="),
	MULTIPLE_PARAM_VALUES_DELIMITER("|"),
	COMMA_DELIMITER(","),
	
	/*
	 * DO NOT CHANGE Has to be same as used GlobalFilter where params passed to / 
	 * can be read as headers prefixed with RESOLVE_PARAM_AS_HEADER_
	 */
    RESOLVE_PARAM_AS_HEADER_HEADER_NAME_PREFIX("RESOLVE_PARAM_AS_HEADER_"),
    
    X_FRAME_OPTIONS_HEADER("X-Frame-Options"),
    X_FRAME_OPTIONS_SAMEORIGIN("SAMEORIGIN"),
    X_FRAME_OPTIONS_DENY("DENY"),
    X_FRAME_OPTIONS_ALLOW_FROM("ALLOW-FROM"),
    X_XSS_PROTECTTION_HEADER("X-XSS-Protection"),
    X_XSS_PROTECTTION_ENABLED_MODE_BLOCKED("1; mode=block "),
    X_CONTENT_TYPE_OPTIONS_HEADER("X-Content-Type-Options"),
    X_CONTENT_TYPE_OPTIONS_NOSNIFF("nosniff"),
    
    CONTENT_SECURITY_POLICY_HEADER("Content-Security-Policy"),
    CONTENT_SECURITY_POLICY_UPGRADE_INSECURE_REQUESTS("upgrade-insecure-requests"),
    
    RESOLVE_SERVICE_LOGIN_URL("/resolve/service/login"),
	RESOLVE_SERVICE_LOGIN_URL_QUESTION_PREFIX("/resolve/service/login?"),
	RESOLVE_SERVICE_LOGIN_URL_QUESTION_URL_PARAMETER_PREFIX("/resolve/service/login?url="),
	RESOLVE_SESSION_TIMED_OUT_JSP_URL_PREFIX("/resolve/jsp/sessionTimedOut.jsp?externalSystem="),
	SSO_USER_LOCKED_IN_RESOLVE_JSP_URL_PREFIX("/resolve/jsp/ssoUserLockedInResolve.jsp?externalSystem="),
	SSO_USER_GET_FROM_OR_CREATE_IN_RESOLVE_JSP_URL_PREFIX(
											 	"/resolve/jsp/ssoUserGetFromOrCreateInResolve.jsp?externalSystem="),
	SSO_CONFIG_MISSING_OR_INCOMPLETE_IN_RESOLVE_JSP_URL_PREFIX(
											 	"/resolve/jsp/ssoConfigMissingOrIncompleteInResolve.jsp?externalSystem="),
	SSO_CONFIGURED_HOST_ACCESS_EXCEPTION_JSP_URL_PREFIX(
											 	"/resolve/jsp/ssoConfiguredHostAccessException.jsp?externalSystem="),
	RESOLVE_RSCLIENT_JSP_URL_PREFIX("/resolve/jsp/rsclient.jsp"),
	RESOLVE_RSCLIENT_JSP_URL_QUESTION_PREFIX("/resolve/jsp/rsclient.jsp?"),
	SSO_USER_CREATE_IN_RESOLVE_DUE_TO_LICENSING_ISSUES_JSP_URL_PREFIX(
		 	"/resolve/jsp/ssoUserCreateInResolveDueToLicensingIssues.jsp?externalSystem=");
	
    private final String value;
    
    private AuthConstants(String value) {
    	this.value = value;
    }
    
    public String getValue() {
    	return value;
    }
}
