/******************************************************************************
* (C) Copyright 2014
*
* Resolve Systems, LLC
*
* All rights reserved
* This software is distributed under license and may not be used, copied,
* modified, or distributed without the express written permission of
* Resolve Systems, LLC.
*
******************************************************************************/

package com.resolve.rsview.controller;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.SortedSet;
import java.util.TreeSet;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.fileupload.FileItem;
import org.apache.commons.fileupload.disk.DiskFileItemFactory;
import org.apache.commons.fileupload.servlet.ServletFileUpload;
import org.springframework.stereotype.Controller;
import org.springframework.stereotype.Service;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.resolve.rsbase.MainBase;
import com.resolve.service.License;
import com.resolve.service.LicenseService;
import com.resolve.services.hibernate.util.UserUtils;
import com.resolve.services.vo.QueryDTO;
import com.resolve.services.vo.ResponseDTO;
import com.resolve.util.Constants;
import com.resolve.util.LicenseEnum;
import com.resolve.util.Log;
import com.resolve.util.StringUtils;


@Controller
@Service
public class AjaxLicense extends GenericController
{

    @RequestMapping(value = "/license/licenses", method = { RequestMethod.GET }, headers = "Accept=application/json; charset=utf-8")
    @ResponseBody
    public ResponseDTO<License> listLicenses(@ModelAttribute QueryDTO query, HttpServletRequest request) throws ServletException, IOException
    {
        ResponseDTO<License> result = new ResponseDTO<License>();

        try
        {
            List<License> licenses = MainBase.main.getLicenseService().getAvailableLicenses(true);
            
            if(licenses != null && licenses.size() > 0)
            {
            	if (licenses.size() > 1) {
            		// Check V2 license at the last element
            		Collections.reverse(licenses);
            		
            		SortedSet<License> sortedV2Licenses = new TreeSet<License>();
            		boolean foundUnExpiredV2License = false;
            		
            		for (License lic : licenses) {            		
	            		if (LicenseEnum.LICENSE_TYPE_V2 == 
	            			 LicenseEnum.valueOf(License.LICENSE_TYPE_PREFIX + lic.getType())) {
	            			sortedV2Licenses.add(lic);
	            			foundUnExpiredV2License = foundUnExpiredV2License || (!lic.isExpired());
	            		} else {
	            			break;
	            		}
            		}
            		
            		if (foundUnExpiredV2License && sortedV2Licenses.size() > 0) {
            			licenses.clear();
            			licenses.addAll(sortedV2Licenses);
            		} else {
            			Collections.reverse(licenses);
            		}
            	}
            	
                result.setRecords(licenses);
                result.setTotal(licenses.size());
            }
        }
        catch (Exception e)
        {
            Log.log.error("Error retrieving tags", e);
            result.setSuccess(false).setMessage("Error retrieving tags");
        }

        return result;
    }
    
    @RequestMapping(value = "/license/get", method = { RequestMethod.GET, RequestMethod.POST}, headers = "Accept=application/json; charset=utf-8")
    @ResponseBody
    public ResponseDTO<License> getLicenses(@RequestParam("key") String key, HttpServletRequest request) throws ServletException, IOException
    {
        ResponseDTO<License> result = new ResponseDTO<License>();

        try
        {
            License license = MainBase.main.getLicenseService().getLicenseByKey(key);
            if(license != null)
            {
                result.setData(license);
                result.setSuccess(true);
            }
        }
        catch (Exception e)
        {
            Log.log.error("Error retrieving tags", e);
            result.setSuccess(false).setMessage("Error retrieving tags");
        }

        return result;
    }
    
    @SuppressWarnings("unchecked")
	@RequestMapping(value = "/license/upload", method = { RequestMethod.GET, RequestMethod.POST}, headers = "Accept=application/json; charset=utf-8")
    @ResponseBody
    public ResponseDTO<License> upload( HttpServletRequest request) throws ServletException, IOException
    {
        ResponseDTO<License> result = new ResponseDTO<License>();
        
        List<FileItem> fileItems = null;
        try
        {
            fileItems = new ServletFileUpload(new DiskFileItemFactory()).parseRequest(request);
            for (FileItem fileItem : fileItems)
            {
                if (!fileItem.isFormField())
                {
                    InputStreamReader ir = new InputStreamReader(fileItem.getInputStream());
                    BufferedReader br = new BufferedReader(ir);
                    
                    while(br.ready()) 
                    {
                        String licenseContent =  br.readLine();
                        License license = MainBase.main.getLicenseService().loadLicenseByEncryptedData(licenseContent);
                        //not yet uploaded for real.
                        license.setUploadDate(0);
                        if(license.isDurationBasedExpiration())
                        {
                            license.setExpirationDate(0L);
                        }
                        Log.log.debug("License prepared to be applied: " + license);
                        result.setData(license);
                        break;
                    }
                }
            }
           
            result.setSuccess(true);
        }
        catch (Exception e)
        {
            Log.log.error(e.getMessage(), e);
            result.setSuccess(false).setMessage("Error uploading license file.");
        }

        return result;
    }
    
    @RequestMapping(value = "/license/submit", method = { RequestMethod.GET, RequestMethod.POST}, headers = "Accept=application/json; charset=utf-8")
    @ResponseBody
    public ResponseDTO<License> submit(@RequestParam("data") String data, HttpServletRequest request) throws ServletException, IOException
    {
        ResponseDTO<License> result = new ResponseDTO<License>();
        
        try
        {
        	License lic = MainBase.main.getLicenseService().loadLicenseByEncryptedData(data);
        	
        	// Allow uploading of expired V2 license
        	if (lic != null && !lic.isDeleted() && 
        		(!lic.isExpired() || LicenseEnum.LICENSE_TYPE_V2 == 
        							 LicenseEnum.valueOf(License.LICENSE_TYPE_PREFIX + lic.getType().toUpperCase()))) {
        		result.setData(lic);
        		result.setSuccess(true);
        	} else {
        		result.setSuccess(false);
        		
        		if (lic == null) {
        			result.setMessage("Submitted invalid license.");
        		} else if (lic.isDeleted()) {
        			result.setMessage("Submitted deletd licnese.");
        		} else if (lic.isExpired()) {
        			result.setMessage("Submitted expired licnese.");
        		}
        	}
        }
        catch (Exception e)
        {
            Log.log.error(e.getMessage(), e);
            result.setSuccess(false).setMessage("Error submitting license data");
        }

        return result;
    }
    
    @RequestMapping(value = "/license/summary", method = { RequestMethod.GET, RequestMethod.POST}, headers = "Accept=application/json; charset=utf-8")
    @ResponseBody
    public ResponseDTO<License> summary(HttpServletRequest request) throws ServletException, IOException
    {
        ResponseDTO<License> result = new ResponseDTO<License>();
        
        try
        {
            result.setData(MainBase.main.getLicenseService().getCurrentLicense());
            result.setSuccess(true);
        }
        catch (Exception e)
        {
            Log.log.error(e.getMessage(), e);
            result.setSuccess(false).setMessage("Error retrieving license info");
        }

        return result;
    }
    
    @RequestMapping(value = "/license/apply", method = { RequestMethod.GET, RequestMethod.POST}, headers = "Accept=application/json; charset=utf-8")
    @ResponseBody
    public ResponseDTO<License> applyNewLicense(@RequestParam("data") String data, HttpServletRequest request) throws ServletException, IOException
    {
        ResponseDTO<License> result = new ResponseDTO<License>();
        
        String username = (String) request.getAttribute(Constants.HTTP_REQUEST_USERNAME);
        try
        {
        	boolean success = true;
            if(StringUtils.isBlank(data))
            {
                Log.log.debug("Nothing to upload/apply, empty license data.");
            }
            else
            {
                Log.log.debug("License uploaded by " + username);
                Map<String, String> params = new HashMap<String, String>();
                params.put(Constants.LICENSE_CONTENT, data);
                LicenseService licenseService = MainBase.main.getLicenseService();
                success = licenseService.uploadLicense(params);
                
                // Check for user count violations after applying license
                License license = licenseService.getLicense();
                boolean userCountChekedOut = LicenseService.checkUsers(license, UserUtils.getNamedUserCount());
                MainBase.main.getLicenseService().setIsUserCountViolated(!userCountChekedOut);
            }
            
            result.setSuccess(success);
            
            if (!success) {
            	result.setMessage("Failed to apply new license!!!");
            } else {
            	result.setData(MainBase.main.getLicenseService().getCurrentLicense());
            }
        }
        catch (Exception e)
        {
            Log.log.error(e.getMessage(), e);
            result.setSuccess(false).setMessage("Error applying new license");
        }

        return result;
    }
    
    /**
     * Delete the licenses based on array of keys
     * 
     * @param keys
     * @param request
     * @param response
     * @return
     * @throws ServletException
     * @throws IOException
     */
    @RequestMapping(value = "/license/delete", method = { RequestMethod.POST }, headers = "Accept=application/json; charset=utf-8")
    @ResponseBody
    public ResponseDTO<License> delete(@RequestParam String[] keys, HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException
    {
        ResponseDTO<License> result = new ResponseDTO<License>();
        String username = (String) request.getAttribute(Constants.HTTP_REQUEST_USERNAME);

        try
        {
            if(keys != null && keys.length > 0)
            {
                Map<String, String> params = new HashMap<String, String>();
                Log.log.debug("License(s) deleted by " + username);
                StringBuilder licenseKeys = new StringBuilder();
                
                for(String key : keys)
                {
                	if (StringUtils.isNotBlank(key) && licenseKeys.length() > 0) {
                		licenseKeys.append(",");
                	}
                    Log.log.debug("license key: " + key);
                    licenseKeys.append(key)/*.append(",")*/;
                    params.put(key, "");
                }
                
                if (licenseKeys.length() > 0) {
                	LicenseService licenseService = MainBase.main.getLicenseService();
                    licenseService.removeLicense(params);
                }
            }

            result.setData(MainBase.main.getLicenseService().getCurrentLicense());
            result.setSuccess(true);
        }
        catch (Exception e)
        {
            Log.log.error("Error deleting Properties", e);
            result.setSuccess(false).setMessage("Error deleting license properties");
        }

        return result;
    }
}
