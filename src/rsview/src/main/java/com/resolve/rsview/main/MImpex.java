/******************************************************************************
* (C) Copyright 2014
*
* Resolve Systems, LLC
*
* All rights reserved
* This software is distributed under license and may not be used, copied,
* modified, or distributed without the express written permission of
* Resolve Systems, LLC.
*
******************************************************************************/

package com.resolve.rsview.main;

import java.io.File;
import java.net.URL;
import java.net.URLConnection;
import java.util.Map;

import org.apache.commons.httpclient.util.URIUtil;

import com.resolve.impex.ImpexAPI;
import com.resolve.services.ImpexService;
import com.resolve.services.hibernate.util.ImportUtils;
import com.resolve.services.hibernate.util.ResolveEventUtil;
import com.resolve.services.hibernate.vo.ResolveEventVO;
import com.resolve.util.Constants;
import com.resolve.util.FileUtils;
import com.resolve.util.Log;
import com.resolve.util.StringUtils;

public class MImpex
{
    public static String importModule(String moduleUrl, Integer connectionTimeout, Integer readTimeout, String username, String password, Boolean importFlag, boolean blockIndex)
    {
        String result = null;
        
        Log.log.debug("Remote module URL:" + moduleUrl);

        try
        {
            if(moduleUrl != null && StringUtils.isNotEmpty(username))
            {
                //this is very specific to resolve where we can pass username and password 
                //just by appending this way. The ImpexAPI caller could append these by themselves.
                moduleUrl += "&_username_=" + username + "&_password_=" + password;
            }

            String moduleFileName = null;
            String moduleName = null;

            URL url = new URL(moduleUrl);

            String query = url.getQuery();
            //Sometime it's outside URL, not resolve so we
            //expect the URL to be http://server:port/ctxroot/modulename.zip
            //TODO need to implement better logic than this. 
            if(StringUtils.isEmpty(query))
            {
                moduleFileName = moduleUrl.substring(moduleUrl.lastIndexOf("/")+1);
            }
            else
            {
                String[] splitted = query.split("&");
                for (String tokens : splitted)
                {
                    if (tokens.startsWith("filename=")) // http://remote_server:port/resolve/service/wiki/impex/download?filename=doc3.zip
                    {
                        moduleFileName = tokens.substring(9);
                        break;
                    }
                }
                
            }

            moduleName = moduleFileName.substring(0, moduleFileName.indexOf(".zip"));

            Log.log.debug("Module file name:" + moduleFileName);
            Log.log.debug("Module name:" + moduleName);
            Log.log.debug("Resolve home:" + RSContext.getResolveHome());
            
            //In case this is running won windows then convert.
            String destination = RSContext.getResolveHome().replaceAll("\\\\", "/") + "/rsexpert/" + moduleFileName;
            url = new URL(URIUtil.encodeQuery(moduleUrl));
            File moduleFile = FileUtils.getFile(destination);
            if (connectionTimeout != null && readTimeout != null)
            {
                FileUtils.copyURLToFile(url, moduleFile, connectionTimeout * 1000, readTimeout * 1000);
            }
            else
            {
                FileUtils.copyURLToFile(url, moduleFile);
            }
            
            result = moduleFileName;
            
            //Sometime invoker may not like it to be imported right away, like the UI for
            //module import where we want to display what's being imported so the
            //user can decided to go ahead with the import or not.
            if(importFlag)
            {
                com.resolve.rsview.impex.ImpexAPI.importModule(moduleName, blockIndex);
                Log.log.debug("Module " + moduleFileName + " imported successfully.");
            }
        }
        catch (Throwable e)
        {
            Log.log.error("Error during import", e);
        }
        
        return result;
    }
    /**
     * Once this method is completed the module file will be in the rsexpert directory. Additonaly
     * it'll import if the import flag is true.
     * 
     * @param params
     * @return
     */
    public static String importModule(Map<String, String> params)
    {
        String moduleUrl = params.get(ImpexAPI.MODULE_URL);
        String connectionTimeoutString = params.get(ImpexAPI.CONNECTION_TIMEOUT);
        String readTimeoutString = params.get(ImpexAPI.READ_TIMEOUT);
        String username = params.get(ImpexAPI.RESOLVE_USERNAME);
        String password = params.get(ImpexAPI.RESOLVE_P_ASSWORD);
        
        Boolean blockIndex = Boolean.FALSE;
        if(params.containsKey(Constants.IMPEX_PARAM_BLOCK_INDEX))
        {
            blockIndex = Boolean.valueOf(params.containsKey(Constants.IMPEX_PARAM_BLOCK_INDEX));
        }
        
        Boolean importFlag = Boolean.FALSE;
        if(params.containsKey(ImpexAPI.IMPORT_FLAG))
        {
            importFlag = Boolean.valueOf(params.containsKey(ImpexAPI.IMPORT_FLAG));
        }

        Integer connectionTimeout = null;
        Integer readTimeout = null;
        if(connectionTimeoutString != null)
        {
            try
            {
                connectionTimeout = Integer.parseInt(connectionTimeoutString);
                readTimeout = Integer.parseInt(readTimeoutString);
            }
            catch (NumberFormatException e)
            {
                Log.log.warn("Invalid value for a numeric field, ignoring..");
            }
        }
        return importModule(moduleUrl, connectionTimeout, readTimeout, username, password, importFlag, blockIndex);
    }
    
    public static String uploadFile(Map<String, String> params)
    {
        String moduleUrl = params.get("URL");
        String moduleFileName = null;
        
        try
        {
            URL url = new URL(moduleUrl);
            String query = url.getQuery();
            
            url = new URL(URIUtil.encodeQuery(moduleUrl));
            URLConnection connection = url.openConnection();
            int fileSize = connection.getContentLength();
            
            if(StringUtils.isEmpty(query))
            {
                moduleFileName = moduleUrl.substring(moduleUrl.lastIndexOf("/")+1);
            }
            else
            {
                String[] splitted = query.split("&");
                for (String tokens : splitted)
                {
                    if (tokens.startsWith("filename=")) // http://remote_server:port/resolve/service/wiki/impex/download?filename=doc3.zip
                    {
                        moduleFileName = tokens.substring(9);
                        break;
                    }
                }
            }
            
            ResolveEventVO eventVO = ResolveEventUtil.getLatestEventByValue("URLUPLOAD");
            if (eventVO != null)
            {
                String value = "";
                if (fileSize == -1) {
                	value = String.format("IMPEX-URLUPLOAD:%s,-1,File could not be downloaded. Make sure the file is ready " +
                						  "to download from source server and try again.", moduleFileName);
                } else {
                	value = String.format("IMPEX-URLUPLOAD:%s,%d,0", moduleFileName, fileSize);
                }
                
                eventVO.setUValue(value);
                ResolveEventUtil.insertResolveEvent(eventVO);
                
                if (fileSize == -1) {
                    return "";
                }
            }
            String destination = RSContext.getResolveHome().replaceAll("\\\\", "/") + "/rsexpert/";
            File distFolder = new File(destination);
            if (!distFolder.exists())
            {
                distFolder.mkdir();
            }
            //url = new URL(URIUtil.encodeQuery(moduleUrl));
            File moduleFile = FileUtils.getFile(destination + moduleFileName);
            FileUtils.copyURLToFile(url, moduleFile);
            
            ImportUtils importUtils = new ImportUtils();
            importUtils.evaluateModuleName(moduleFileName, true);
            importUtils.storeZipFileToDB(moduleFileName, false, "admin", true);
            
            importUtils = null;
        }
        catch (Throwable e)
        {
        	try {
	            ResolveEventVO eventVO = ResolveEventUtil.getLatestEventByValue("URLUPLOAD");
	
	            if (eventVO != null)
	            {
	                String value = String.format("IMPEX-URLUPLOAD:URL Upload File%s,-1,0",
	                							 (StringUtils.isNotBlank(e.getMessage()) ? 
	                							  " - " + e.getMessage().replaceAll(",", "<COMMA>") : ""));
	                eventVO.setUValue(value);
	                ResolveEventUtil.insertResolveEvent(eventVO);
	            }
	            Log.log.error("Error in uploadFile of MImpex.", e);
        	} catch (Throwable t) {
        		Log.log.error(String.format("Error%sin Upload File Request with parameters [%s]",
	  						 				(StringUtils.isNotEmpty(t.getMessage()) ? " [" + t.getMessage() + "] " : " "),
	  						 				StringUtils.mapToString(params, "=", ", ")));
        	}
        }
        
        return moduleFileName;
    }
    
    public static void importModuleFromBrowser(Map<String, Object> params)
    {
    	try {
	        params.put("OPERATION", "IMPORT");
	        ImpexService.impexSync(params);
    	} catch (Throwable t) {
    		Log.log.error(String.format("Error%sin Import Module Request From Browser with parameters [%s]",
					  					(StringUtils.isNotEmpty(t.getMessage()) ? " [" + t.getMessage() + "] " : " "),
					  					StringUtils.mapToString(params, "=", ", ")));
    	}
    }
    
    public static void exportModuleFromBrowser(Map<String, Object> params)
    {
    	try {
	        params.put("OPERATION", "EXPORT");
	        ImpexService.impexSync(params);
    	} catch (Throwable t) {
    		Log.log.error(String.format("Error%sin Export Module Request From Browser with parameters [%s]",
						  (StringUtils.isNotEmpty(t.getMessage()) ? " [" + t.getMessage() + "] " : " "),
						  StringUtils.mapToString(params, "=", ", ")));
    	}
    }
    
    public static void buildImpexManifest(Map<String, Object> params)
    {
    	try {
	        params.put("TASK", "MANIFEST");
	        ImpexService.impexSync(params);
    	} catch (Throwable t) {
    		Log.log.error(String.format("Error%sin building Impex Manifest Request with parameters [%s]",
    									(StringUtils.isNotEmpty(t.getMessage()) ? " [" + t.getMessage() + "] " : " "),
    									StringUtils.mapToString(params, "=", ", ")));
    	}
    }
}