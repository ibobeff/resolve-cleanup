/******************************************************************************
* (C) Copyright 2014
*
* Resolve Systems, LLC
*
* All rights reserved
* This software is distributed under license and may not be used, copied,
* modified, or distributed without the express written permission of
* Resolve Systems, LLC.
*
******************************************************************************/

package com.resolve.rsview.main;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.TimeZone;
import java.util.concurrent.atomic.AtomicLong;

import com.resolve.rsview.auth.Authentication;
import com.resolve.search.SearchAdminAPI;
import com.resolve.search.SearchConstants;
import com.resolve.search.SearchException;
import com.resolve.search.actiontask.ActionTaskIndexAPI;
import com.resolve.search.customform.CustomFormIndexAPI;
import com.resolve.search.dictionary.DictionaryIndexAPI;
import com.resolve.search.property.PropertyIndexAPI;
import com.resolve.search.social.SocialIndexAPI;
import com.resolve.search.wiki.WikiIndexAPI;
import com.resolve.search.worksheet.WorksheetIndexAPI;
import com.resolve.services.ServiceHibernate;
import com.resolve.services.ServiceWiki;
import com.resolve.util.constants.HibernateConstants;
import com.resolve.services.hibernate.util.ActionTaskUtil;
import com.resolve.services.hibernate.vo.ResolveActionTaskVO;
import com.resolve.services.hibernate.vo.WikiDocumentVO;
import com.resolve.services.util.MigrationUtil;
import com.resolve.services.vo.QueryDTO;
import com.resolve.services.vo.QueryFilter;
import com.resolve.services.vo.QuerySort;
import com.resolve.util.Constants;
import com.resolve.util.DateUtils;
import com.resolve.util.Log;
import com.resolve.util.StringUtils;

public class MIndex
{
    public static AtomicLong receivedCounts = new AtomicLong(0);
    private static boolean synchronizingWiki = false;
    private static boolean synchronizingActionTask = false;

    /**
     * This method deletes all the data from following indices:
     * 
     * 1. worksheet_xxx
     * 2. processrequest_xxx
     * 3. taskresult_xxx
     * 4. worksheetdata
     * 5. executestate
     * 
     * @param params
     */
    public static void removeAllWorkSheetsRelatedIndex(Map params)
    {
        try
        {
            // by default assume it's the system user that's is calling this
            String username = "system";
            // however param could pass USERNAME to explicitly define the user's
            // name
            if (params != null && params.containsKey("USERNAME"))
            {
                String tmpUsername = (String) params.get("USERNAME");

                if (StringUtils.isNotBlank(tmpUsername))
                {
                    username = tmpUsername;
                }
            }
            WorksheetIndexAPI.removeAllWorkSheetsRelatedIndex(username);
            Log.log.info("Removed all worksheet, process request and task results index data");
        }
        catch (SearchException e)
        {
            // since this method is called from ESM listener just simply logging
            // the error.
            Log.log.error(e.getMessage(), e);
        }
    }

    /**
     * Provides the total number of documents in each index/alias.
     * 
     * @param params
     * @return
     */
    public static Map<String, Long> getIndexRecordSize(Map params)
    {
        return SearchAdminAPI.getIndexRecordSize();
    }
    
    /**
     * This method will simply read everynode in the graph and save it so the
     * Neo4J's indexes are refreshed. It requires that the "index" folder inside
     * "graphdb" folder was removed prior to running RSView.
     * 
     * After the graph node refresh it migrates Social posts and tags from old
     * Lucene to ES.
     * 
     * @param params
     */
    public static Map<String,String> migratePosts(Map<String, Object> params) throws Exception
    {
        Log.log.error("Calling MIndex.migratePosts() is not available in 5.2 onwards.");
        throw new IllegalAccessException("Calling MIndex.migratePosts() is not available in 5.2 onwards.");
    }

    public static void migrateGraphOnly(Map<String, Object> params) throws Exception
    {
        Log.log.error("Calling MIndex.migrateGraphOnly() is not available in 5.2 onwards.");
        throw new IllegalAccessException("Calling MIndex.migrateGraphOnly() is not available in 5.2 onwards.");
    }

    public static void migratePostsOnly(Map<String, Object> params) throws Exception
    {
        Log.log.error("Calling MIndex.migratePostsOnly() is not available in 5.2 onwards.");
        throw new IllegalAccessException("Calling MIndex.migratePostsOnly() is not available in 5.2 onwards.");
    }

    /**
     * This method indexes all the wiki documents from the database.
     * 
     * @param params
     */
    public static void indexAllWikiDocuments(Map<String, Object> params)
    {
        Log.log.debug("All Wiki document indexing started.");
        ServiceWiki.indexAllWikiDocuments(true, "system");
        Log.log.debug("All Wiki document indexing finished.");
    }
    
    public static void indexAllResolveProperties(Map<String, Object> params)
    {
        Log.log.debug("All Wiki document indexing started.");
        ServiceHibernate.indexAllResolveProperties("system");
        Log.log.debug("All Wiki document indexing finished.");
    }

    public static void indexAllCustomForms(Map<String, Object> params)
    {
        Log.log.debug("All Wiki document indexing started.");
        ServiceHibernate.indexAllCustomForms("system");
        Log.log.debug("All Wiki document indexing finished.");
    }
    
    /**
     * This method permanently removes all the wiki documents and attachments
     * from the ElasticSearch wikidoument index.
     * 
     * @param params
     */
    public static void purgeAllWikiDocuments(Map<String, Object> params)
    {
        Log.log.debug("All Wiki document purge started.");
        ServiceWiki.purgeAllWikiDocumentIndexes("system");
        Log.log.debug("All Wiki document purge finished.");
    }

    /**
     * This method indexes all the action tasks from the database.
     * 
     * @param params
     */
    public static void indexAllActionTasks(Map<String, Object> params)
    {
        Log.log.debug("All Action Task indexing started.");
        Set<String> sysIds = ActionTaskUtil.getAllActionTaskSysIds();
        ActionTaskIndexAPI.indexActionTasks(sysIds, "system");
        Log.log.debug("All Action Task indexing finished.");
    }

    /**
     * This method permanently removes all the action tasks from 
     * the ElasticSearch actiontask index.
     * 
     * @param params
     */
    public static void purgeAllActionTasks(Map<String, Object> params)
    {
        Log.log.debug("All Action Task purge started.");
        ActionTaskIndexAPI.purgeAllActionTasks("system");
        Log.log.debug("All Action Task purge finished.");
    }

    /**
     * This method permanently removes all the action task properties from 
     * the ElasticSearch properties index.
     * 
     * @param params
     */
    public static void purgeAllResolveProperties(Map<String, Object> params)
    {
        Log.log.debug("All action task properties purge started.");
        PropertyIndexAPI.purgeAllProperties("system");
        Log.log.debug("All action task proper8n ties purge finished.");
    }

    /**
     * This method permanently removes all the custom forms from 
     * the ElasticSearch customform index.
     * 
     * @param params
     */
	public static void purgeAllCustomForms(Map<String, Object> params)
    {
        Log.log.debug("All custom forms purge started.");
        CustomFormIndexAPI.deleteAllCustomForms("system");
        Log.log.debug("All custom forms purge finished.");
    }
    
    /**
     * Indexes all the dictionary words from the fulldictionary.txt
     * 
     * TODO: not sure if it is relevant anymore.
     *  
     * @param params
     */
    public static void indexDictionary(Map<String, Object> params)
    {
        Log.log.debug("Dictionary indexing started.");
        DictionaryIndexAPI.indexAllWords("system");
        Log.log.debug("Dictionary indexing finished.");
    }
    
    /**
     * This method allows to index all wikis and actiontasks based on the incoming parameter.
     * 
     * TODO: may be unused.
     * 
     * @param params
     */
    public static void indexAll(Map<String, Object> params)
    {
        boolean indexWikiDocuments = params.get(HibernateConstants.INDEX_WIKIDOCUMENTS) != null ? (Boolean) params.get(HibernateConstants.INDEX_WIKIDOCUMENTS) : false;
        boolean indexActionTask = params.get(HibernateConstants.INDEX_ACTIONTASK) != null ? (Boolean) params.get(HibernateConstants.INDEX_ACTIONTASK) : false;
        boolean indexActionTaskProperties = params.get(HibernateConstants.INDEX_ACTIONTASK_PROPERTIES) != null ? (Boolean) params.get(HibernateConstants.INDEX_ACTIONTASK_PROPERTIES) : false;
        boolean indexCustomForms = params.get(HibernateConstants.INDEX_CUSTOM_FORMS) != null ? (Boolean) params.get(HibernateConstants.INDEX_CUSTOM_FORMS) : false;
        
        Log.start("Start Index ALL - Wikidocuments: " + indexWikiDocuments + " ActionTasks: " + indexActionTask, "DEBUG");

        if (indexWikiDocuments)
        {
            indexAllWikiDocuments(null);
        }
        if (indexActionTask)
        {
            indexAllActionTasks(null);
        }
        if(indexActionTaskProperties)
        {
        	indexAllResolveProperties(null);
        }
        if(indexCustomForms) 
        {
        	indexAllCustomForms(null);
        }
        Log.duration("Finish Index ALL", "DEBUG");
    }
    
    /**
     * This is a major operation that will move data from social index,
     * drop the index, recreate it and move the data back in. Make
     * sure the system is in maintenance mode during this operation so
     * that no user logs in.
     * 
     * This method is mainly called by update/upgrade scripts if there is a 
     * new version of the following mapping files in the current Resolve release: 
     * 
     * social_posts_mappings.json
     * social_comments_mappings.json
     * system_posts_mappings.json
     * system_comments_mappings.json
     * 
     * @param params
     */
    public static void updateSocialIndex(Map<String, Object> params)
    {
        Log.log.debug("Social index update started.");
        String username = "system";
        String indexName = "all";
        int batchSize = 5000;
        if(params != null)
        {
            if(params.containsKey("USERNAME"))
            {
                username = (String) params.get("USERNAME");
            }
            indexName = (String) params.get("INDEXNAME");
            String batch = (String) params.get("BATCHSIZE");
            if(StringUtils.isNotBlank(batch))
            {
                if(StringUtils.isNumeric(batch))
                {
                    batchSize = Integer.valueOf(batch);
                }
            }
        }
        SocialIndexAPI.updateIndex(indexName, batchSize, username);
        Log.log.debug("Social index update finished.");
    }
    
    /**
     * This method can be safely removed. 
     * 
     * @param param
     * @return
     */
    public static Map<String,String> migrateCassandraData(Map<String, Object> param)
    {
        Map<String,String> result = new HashMap<String,String>();
        Log.log.debug("Migrating WSDATA started.");
        MigrationUtil.migrateWsdata();
        Log.log.debug("Migrating WSDATA finished.");
        Log.log.debug("Migrating ExecuteState started.");
        MigrationUtil.migrateExecuteState();
        Log.log.debug("Migrating ExecuteState finished.");
        result.put("RESULT", "Cassandra Data Migrated");
        return result;
    }
    
    /**
     * This is a major operation that will move data from task result index,
     * drop the index, recreate it and move the data back in. Make
     * sure the system is in maintenance mode during this operation so
     * that no user logs in.
     * 
     * This method is mainly called by update/upgrade scripts if there is a 
     * new version of the worksheets_mappings.json file in the current Resolve release. 
     * 
     * @param params
     */
    public static void updateWorksheetIndex(Map<String, Object> params)
    {
        Log.log.debug("Worksheet index update started.");
        String username = "system";
        int batchSize = 5000;
        if(params != null)
        {
            if(params.containsKey("USERNAME"))
            {
                username = (String) params.get("USERNAME");
            }
            String batch = (String) params.get("BATCHSIZE");
            if(StringUtils.isNotBlank(batch))
            {
                if(StringUtils.isNumeric(batch))
                {
                    batchSize = Integer.valueOf(batch);
                }
            }
        }
        try
        {
            WorksheetIndexAPI.updateIndex(SearchConstants.INDEX_NAME_WORKSHEET, batchSize, false, username);
        }
        catch (Exception e)
        {
            Log.log.error(e.getMessage(), e);
        }
        Log.log.debug("Worksheet index update finished.");
    }
    
    /**
     * This is a major operation that will move data from process request index,
     * drop the index, recreate it and move the data back in. Make
     * sure the system is in maintenance mode during this operation so
     * that no user logs in.
     * 
     * This method is mainly called by update/upgrade scripts if there is a 
     * new version of the processrequests_mappings.json file in the current Resolve release. 
     * 
     * @param params
     */
    public static void updateProcessRequestIndex(Map<String, Object> params)
    {
        Log.log.debug("Process request index update started.");
        String username = "system";
        int batchSize = 5000;
        if(params != null)
        {
            if(params.containsKey("USERNAME"))
            {
                username = (String) params.get("USERNAME");
            }
            String batch = (String) params.get("BATCHSIZE");
            if(StringUtils.isNotBlank(batch))
            {
                if(StringUtils.isNumeric(batch))
                {
                    batchSize = Integer.valueOf(batch);
                }
            }
        }
        try
        {
            WorksheetIndexAPI.updateIndex(SearchConstants.INDEX_NAME_PROCESSREQUEST, batchSize, false, username);
        }
        catch (Exception e)
        {
            Log.log.error(e.getMessage(), e);
        }
        Log.log.debug("Process request index update finished.");
    }
    
    /**
     * This is a major operation that will move data from task result index,
     * drop the index, recreate it and move the data back in. Make
     * sure the system is in maintenance mode during this operation so
     * that no user logs in.
     * 
     * This method is mainly called by update/upgrade scripts if there is a 
     * new version of the taskresults_mappings.json file in the current Resolve release. 
     * 
     * @param params
     */
    public static void updateTaskResultIndex(Map<String, Object> params)
    {
        Log.log.debug("Task Result index update started.");
        String username = "system";
        int batchSize = 5000;
        boolean force = false;
        if(params != null)
        {
            if(params.containsKey("USERNAME"))
            {
                username = (String) params.get("USERNAME");
            }
            String batch = (String) params.get("BATCHSIZE");
            if(StringUtils.isNotBlank(batch))
            {
                if(StringUtils.isNumeric(batch))
                {
                    batchSize = Integer.valueOf(batch);
                }
            }
            if (params.containsKey("force"))
            {
                force = Boolean.valueOf((String) params.get("force"));
            }
        }
        try
        {
            WorksheetIndexAPI.updateIndex(SearchConstants.INDEX_NAME_TASKRESULT, batchSize, force, username);
        }
        catch (Exception e)
        {
            Log.log.error(e.getMessage(), e);
        }
        Log.log.debug("Task Result index update finished.");
    }
    
    /**
     * This method creates unavailable indices.
     *  
     * @param params
     */
    public static Map<String,String> configureIndex(Map<String, Object> params)
    {
        Map<String,String> result = new HashMap<String,String>();
        String indices = "all";
        if(params != null)
        {
            //passed as a comma separated string of index names
            indices = (String) params.get("indices");
        }
        SearchAdminAPI.configureIndex(indices);
        
        result.put("RESULT", "RSSearch Index Configured");
        return result; 
    }
    
    
    
    /**
     * This method creates snapshot of your Elastic Search Index
     *  
     * @param params
     */
    public static Map<String, String> createElasticSearchSnapShot(Map<String, Object> params) {
        
        Map<String, String> result = new HashMap<String, String>();
        String snapshotName = (String) params.get("snapshotName");
        
        if (snapshotName == null || snapshotName.isEmpty()){
            Date date = new Date();
            SimpleDateFormat sdf = new SimpleDateFormat("yyyy/MM/dd");
            snapshotName = "snapshot_backup"+sdf.format(date);
        }
        
         boolean results = SearchAdminAPI.createSnapshotAllIndices(Constants.ELASTIC_SEARCH_REPOSITORY_NAME, snapshotName);  
         result.put("RESULT",results ? "Elastic Search snapshot Created":"Elastic Search snapshot Failed");
         
        return result;
    }
    
    
    /**
     * This method truncates the runbook related index and configure with latest
     * mapping.
     * 
     * From 5.2 onwards it will help truncating old monthly index data.
     * 
     * @param params
     */
    public static Map<String, String> truncateRunbookIndex(Map<String, Object> params)
    {
        Map<String, String> result = new HashMap<String, String>();
        String indexName = (String) params.get("index");
        String password = (String) params.get("pwd");
        String month = (String) params.get("month");

        if (StringUtils.isBlank(indexName) || StringUtils.isBlank(password))
        {
            Log.log.error("index name and resolve.maint password are mandatory.");
            result.put("RESULT", "index name and resolve.maint password are mandatory.");
        }
        else
        {
            if (Authentication.authenticate(Constants.USER_RESOLVE_MAINT, password).valid)
            {
                Log.log.info("Truncating index: " + indexName.toLowerCase());
                WorksheetIndexAPI.truncateRunbookIndex(indexName.toLowerCase(), month);
                result.put("RESULT", "Index truncated successfully: " + indexName);
            }
            else
            {
                Log.log.error("Check resolve.maint password.");
                result.put("RESULT", "Check resolve.maint password.");
            }
        }
        return result;
    }
    
    /**
     * Part of the site2site replication. This is the intellgent synchronizer that
     * will selectively index wikis 
     * 
     * @return
     */
    public String synchronizeWiki()
    {
        String result;

        if(synchronizingWiki)
        {
            result = "synchronizeWiki is busy, ignoring for now.";
            Log.log.debug("synchronizeWiki is busy, ignoring for now.");
        }
        else
        {
            synchronizingWiki = true;

            try
            {
                Log.log.debug("Checking out of sync wiki documents.");
                
                long lastIndexTime = WikiIndexAPI.getLastIndexTime();
                Log.log.debug("WikiDocument Last sysUpdatedOn from ES timestamp : " + lastIndexTime + ", Date : " + DateUtils.getISODateString(lastIndexTime));
                int utcOffset = -1 * TimeZone.getDefault().getOffset(lastIndexTime);
                //this workaround seems to be necessary to overcome the date conversion by hibernate to the 
                //default timezone of the server. since we store the dates in UTC by converting ourself
                //it is necessary to convert during query as well.
                lastIndexTime += utcOffset;
                
                QueryDTO query = new QueryDTO(0, 500); //500 wikis at a time
                query.setModelName("WikiDocument");
                query.setSelectColumns("sys_id,sysUpdatedOn");
                query.addFilterItem(new QueryFilter(QueryFilter.DATE_TYPE, DateUtils.getISODateString(lastIndexTime), "sysUpdatedOn", QueryFilter.GREATER_THAN));
                query.addSortItem(new QuerySort("sysUpdatedOn", QuerySort.SortOrder.ASC));
                
                List<WikiDocumentVO> data = ServiceWiki.getWikiDocuments(query, null, "resolve.maint");
                if(data != null && data.size() > 0)
                {
                    Collection<String> sysIds = new ArrayList<String>();
                    for(WikiDocumentVO wiki : data)
                    {
                        sysIds.add(wiki.getSys_id());
                    }
                    Log.log.debug("Synchronizing " + data.size() + " wiki document(s) from database to RSSearch.");
                    WikiIndexAPI.indexWikiDocuments(sysIds, true, "resolve.maint");
                }
                result = "Wiki document synchronization done.";
            }
            catch (Throwable t)
            {
                Log.log.info("Failed to synchronize wiki document: " + t.getMessage(), t);
                result = "Failed to synchronize wiki document";
            }
            synchronizingWiki = false;
        }
        return result;
    }

    /**
     * Part of the site2site replication. This is the intellgent synchronizer that
     * will selectively index action tasks 
     * 
     * @return
     */
    public String synchronizeActionTask()
    {
        String result;

        if(synchronizingActionTask)
        {
            result = "synchronizeActionTask is busy, ignoring for now.";
            Log.log.debug("synchronizeActionTask is busy, ignoring for now.");
        }
        else
        {
            synchronizingActionTask = true;
            try
            {
                Log.log.debug("Checking out of sync action tasks.");
                
                long lastIndexTime = ActionTaskIndexAPI.getLastIndexTime();
                
                Log.log.debug("ActionTask last sysUpdatedOn from ES timestamp : " + lastIndexTime + ", Date : " + DateUtils.getISODateString(lastIndexTime));
                int utcOffset = -1 * TimeZone.getDefault().getOffset(lastIndexTime);
                //this workaround seems to be necessary to overcome the date conversion by hibernate to the 
                //default timezone of the server. since we store the dates in UTC by converting ourself
                //it is necessary to convert during query as well.
                lastIndexTime += utcOffset;
                
                QueryDTO query = new QueryDTO(0, 500);
                query.setModelName("ResolveActionTask");
                query.setSelectColumns("sys_id,sysUpdatedOn");
                query.addFilterItem(new QueryFilter(QueryFilter.DATE_TYPE, DateUtils.getISODateString(lastIndexTime), "sysUpdatedOn", QueryFilter.GREATER_THAN));
                query.addSortItem(new QuerySort("sysUpdatedOn", QuerySort.SortOrder.ASC));
                    
                List<ResolveActionTaskVO> data = ServiceHibernate.getResolveActionTasks(query, "resolve.maint").getRecords();
                if(data != null && data.size() > 0)
                {
                    Collection<String> sysIds = new ArrayList<String>();
                    for(ResolveActionTaskVO wiki : data)
                    {
                        sysIds.add(wiki.getSys_id());
                    }
                    Log.log.debug("Synchronizing " + data.size() + " action task(s) from database to RSSearch.");
                    ActionTaskIndexAPI.indexActionTasks(sysIds, "resolve.maint");
                }
                result = "Action Task synchronization done.";
            }
            catch (Throwable t)
            {
                Log.log.info("Failed to synchronize action task: " + t.getMessage(), t);
                result = "Failed to synchronize action task";
            }
            synchronizingActionTask = false;
        }
        return result;
    }
    
    /**
     * This method truncates simple indexes and configure with latest
     * mapping. Do not use this to remove worksheet related indexes.
     * 
     * @param params
     */
    public static Map<String, String> truncateIndex(Map<String, Object> params)
    {
        Map<String, String> result = new HashMap<String, String>();
        String indexName = (String) params.get("index");
        String password = (String) params.get("pwd");

        if (StringUtils.isBlank(indexName) || StringUtils.isBlank(password))
        {
            Log.log.error("index name and resolve.maint password are mandatory.");
            result.put("RESULT", "index name and resolve.maint password are mandatory.");
        }
        else
        {
            if (Authentication.authenticate(Constants.USER_RESOLVE_MAINT, password).valid)
            {
                //just a way to protect monthly indexes from getting clobbered by mistake
                if("worksheet_processrequest_taskresult".contains(indexName.toLowerCase()))
                {
                    result.put("ERROR", "Call the truncateRunbookIndex instead.");
                }
                else
                {
                    try
                    {
                        SearchAdminAPI.updateIndexMapping(indexName.toLowerCase());
                        result.put("RESULT", "Index truncated successfully: " + indexName);
                    }
                    catch (SearchException e)
                    {
                        Log.log.error(e.getMessage(), e);
                        result.put("ERROR", "Error truncating index. " + e.getMessage());
                    }
                }
            }
            else
            {
                Log.log.error("Check resolve.maint password.");
                result.put("ERROR", "Check resolve.maint password.");
            }
        }
        return result;
    }
    
    /**
     * This method removes simple indexes. This removal is permanent
     * so it is recommended to be very careful calling this method
     * 
     * Required params:
     *   index - name of index to remove
     *   pwd   - resolve.maint password
     * 
     * @param params
     */
    public static Map<String, String> removeIndex(Map<String, Object> params)
    {
        Map<String, String> result = new HashMap<String, String>();
        String indexName = (String) params.get("index");
        String password = (String) params.get("pwd");

        if (StringUtils.isBlank(indexName) || StringUtils.isBlank(password))
        {
            Log.log.error("index name and resolve.maint password are mandatory.");
            result.put("RESULT", "index name and resolve.maint password are mandatory.");
        }
        else
        {
            if (Authentication.authenticate(Constants.USER_RESOLVE_MAINT, password).valid)
            {
                try
                {
                    SearchAdminAPI.removeIndex(indexName.toLowerCase());
                    result.put("RESULT", "Index truncated successfully: " + indexName);
                }
                catch (SearchException e)
                {
                    Log.log.error(e.getMessage(), e);
                    result.put("ERROR", "Error truncating index. " + e.getMessage());
                }
            }
            else
            {
                Log.log.error("Check resolve.maint password.");
                result.put("ERROR", "Check resolve.maint password.");
            }
        }
        return result;
    }

	/*
	 * Drop specified of indices and creates them with the new mapping and then
	 * reindex
	 */
	public static void updateIndexMappingAndReindex(Map<String, Object> params) {
		for (String indexName : params.keySet()) {
			if (params.get(indexName) instanceof Boolean && (Boolean)params.get(indexName)) {
				try {
					SearchAdminAPI.updateIndexMapping(indexName.toLowerCase());
				} catch (SearchException e) {
					Log.log.error("Error updating index mapping of " + indexName.toLowerCase() + e);
				}
			}
		}

		boolean isWikiReindexed = false;
		for (String indexName : params.keySet()) {
			if (params.get(indexName) instanceof Boolean && (Boolean)params.get(indexName)) {
				isWikiReindexed = reindex(indexName.toLowerCase(), isWikiReindexed);
			}
		}

	}

	private static boolean reindex(String indexName, boolean isWikiReindexed) {

		if (!isWikiReindexed && (indexName.equals(SearchConstants.SEARCH_TYPE_AUTOMATION)
				|| indexName.equals(SearchConstants.SEARCH_TYPE_DECISION_TREE)
				|| indexName.equals(SearchConstants.SEARCH_TYPE_DOCUMENT)
				|| indexName.equals(SearchConstants.SEARCH_TYPE_PLAYBOOK))) {
			indexAllWikiDocuments(null);
			isWikiReindexed = true;
		}
		if (indexName.equals(SearchConstants.INDEX_NAME_ACTION_TASK)) {
			indexAllActionTasks(null);
		}
		if (indexName.equals(SearchConstants.INDEX_NAME_PROPERTY)) {
			indexAllResolveProperties(null);
		}
		if (indexName.equals(SearchConstants.INDEX_NAME_CUSTOM_FORM)) {
			indexAllCustomForms(null);
		}

		return isWikiReindexed;
	}

	/**
     * This will delete the wiki documents index, set latest mapping and reindex all wiki documents.
     * 
     * @param params
     */
    public static void updateWikiIndex(Map<String, Object> params)
    {
        Log.log.debug("Wikidocument index update started.");
        try
        {
            SearchAdminAPI.updateIndexMapping(SearchConstants.INDEX_NAME_WIKI_DOCUMENT);
            indexAllWikiDocuments(new HashMap<String, Object>());
        }
        catch (SearchException e)
        {
            Log.log.error(e.getMessage(), e);
        }
        Log.log.debug("Wikidocument index update finished.");
    }

    /**
     * This method deletes the runbook related indices like worksheet, processrequest, taskresult etc.
     * 
     * pass Map with following values
     *  index=worksheet
     *  pwd=resolve
     *  start=20150101
     *  end=20150131
     * 
     * This will remove all worksheet indexes January'15. If "start" is not provided
     * it will delete everything before "end" date 
     * 
     * @param params
     */
    public static Map<String, String> deleteRunbookIndex(Map<String, Object> params)
    {
        Map<String, String> result = new HashMap<String, String>();
        String indexName = (String) params.get("index");
        String password = (String) params.get("pwd");
        String start = (String) params.get("start");
        String end = (String) params.get("end");

        if (StringUtils.isBlank(indexName) || StringUtils.isBlank(password) || StringUtils.isBlank(end))
        {
            Log.log.error("index, resolve.maint password, end are mandatory.");
            result.put("RESULT", "index, resolve.maint password, start, end are mandatory.");
        }
        else
        {
            if (Authentication.authenticate(Constants.USER_RESOLVE_MAINT, password).valid)
            {
                Log.log.info("Deleting runbook index:");
                Log.log.info("\tindex : " + indexName);
                Log.log.info("\tstart : " + start);
                Log.log.info("\tend   : " + end);

                try
                {
                    WorksheetIndexAPI.deleteRunbookIndex(indexName, start, end);
                    result.put("RESULT", "Index deleted successfully: " + indexName);
                }
                catch (SearchException e)
                {
                    Log.log.error(e.getMessage(), e);
                    result.put("ERROR", "Error truncating index. " + e.getMessage());
                }
            }
            else
            {
                Log.log.error("Check resolve.maint password.");
                result.put("ERROR", "Check resolve.maint password.");
            }
        }
        return result;
    }
    
    public static void generateRBCForOldWorksheets(Map<String, String> params) throws Exception
    {
    	if (params != null)
    	{
    		String startDate = params.get("START_DATE");
    		String rbc = params.get("RBC");
    		WorksheetIndexAPI.generateRBCForOldWorksheets(startDate, rbc);
    	}
    }
}
