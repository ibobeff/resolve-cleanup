/******************************************************************************
* (C) Copyright 2017
*
* Resolve Systems, LLC
*
* All rights reserved
* This software is distributed under license and may not be used, copied,
* modified, or distributed without the express written permission of
* Resolve Systems, LLC.
*
******************************************************************************/

package com.resolve.rsview.controller;

import java.io.IOException;
import java.math.BigDecimal;
import java.math.BigInteger;
import java.math.RoundingMode;
import java.time.temporal.ChronoUnit;
import java.time.temporal.TemporalUnit;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;
import java.util.stream.Collectors;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.lang3.StringEscapeUtils;
import org.apache.commons.lang3.tuple.Pair;
import org.codehaus.jackson.map.DeserializationConfig;
import org.codehaus.jackson.map.DeserializationConfig.Feature;
import org.codehaus.jackson.map.ObjectMapper;
import org.springframework.stereotype.Controller;
import org.springframework.stereotype.Service;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import com.google.gson.JsonElement;
import com.google.gson.JsonParser;
import com.resolve.dto.SIRBulkUpdatePayloadDTO;
import com.resolve.dto.SIRConfigDTO;
import com.resolve.exception.LimitExceededException;
import com.resolve.persistence.model.Orgs;
import com.resolve.persistence.model.ResolveSecurityIncident;
import com.resolve.rsbase.MainBase;
import com.resolve.rsview.main.Main;
import com.resolve.rsview.model.sir.MeanTimeFilterContainer;
import com.resolve.rsview.model.sir.SIRMeanTimeReportType;
import com.resolve.search.model.PBArtifacts;
import com.resolve.search.model.PBAttachments;
import com.resolve.search.model.PBAuditLog;
import com.resolve.search.model.PBNotes;
import com.resolve.search.model.SecurityIncident;
import com.resolve.search.playbook.PlaybookSearchAPI;
import com.resolve.services.ServiceHibernate;
import com.resolve.services.ServiceWiki;
import com.resolve.services.hibernate.util.OrgsUtil;
import com.resolve.services.hibernate.util.PlaybookActivityUtils;
import com.resolve.services.hibernate.util.PropertiesUtil;
import com.resolve.services.hibernate.util.RBACUtils;
import com.resolve.services.hibernate.util.SavedSearchUtil;
import com.resolve.services.hibernate.util.UserUtils;
import com.resolve.services.hibernate.vo.ActivityVO;
import com.resolve.services.hibernate.vo.CaseAttributeVO;
import com.resolve.services.hibernate.vo.CustomDataFormVO;
import com.resolve.services.hibernate.vo.OrgsVO;
import com.resolve.services.hibernate.vo.ResolveSecurityIncidentVO;
import com.resolve.services.hibernate.vo.SIRConfigVO;
import com.resolve.services.hibernate.vo.WikiDocumentVO;
import com.resolve.services.hibernate.wiki.WikiStatusEnum;
import com.resolve.services.interfaces.VO;
import com.resolve.services.sir.SIRMeanTimeReportResult;
import com.resolve.services.util.CaseAttributeUtil;
import com.resolve.services.util.CustomDataFormUtil;
import com.resolve.services.util.PlaybookUtils;
import com.resolve.services.vo.ArtifactConfigurationDTO;
import com.resolve.services.vo.QueryDTO;
import com.resolve.services.vo.QueryFilter;
import com.resolve.services.vo.QuerySort;
import com.resolve.services.vo.ResponseDTO;
import com.resolve.services.vo.SavedSearchDTO;
import com.resolve.services.vo.SecurityIncidentLightDTO;
import com.resolve.util.Constants;
import com.resolve.util.Log;
import com.resolve.util.StringUtils;

import net.sf.json.JSONArray;
import net.sf.json.JSONObject;

@Controller
@Service
public class AjaxPlaybook extends GenericController
{
	public static final String PROPERTY_SIR_MAX_ATTACHMENTS = "sir.maxattachments";
	public static final String PROPERTY_SIR_MAX_ARTIFACTS = "sir.maxartifacts";
	public static final String PROPERTY_SIR_MAX_NOTES = "sir.maxnotes";
	public static final String PROPERTY_SIR_MAX_WIKINOTES = "sir.maxwikinotes";
	public static final String PROPERTY_SIR_MAX_AUDIT_LOGS = "sir.maxauditlogs";
	
	private static final String NO_RESULTS_FOUND = "No results found";
	private static final String ERROR_ADDING_INCIDENT_ACTIVITIES = "Could not add activities of incident id: %s";
	private static final String ERROR_LISTING_INCIDENT_ACTIVITIES = "Could not list activities of incident id: %s";
	private static final String ERROR_APPENDING_SECURITY_INCIDENT_ARTIFACT = 
																"Error appending security incident artifact for SIR: %s";
	private static final String SUCCESS_APPENDING_SECURITY_INCIDENT_ARTIFACT = 
																		"Appended secirity incident artifact for SIR: %s";
	private static final String ERROR_CLOSING_SECURITY_INCIDENT_BY_ID = "Could not close resolve security incident id: %s";
	private static final String ERROR_CREATING_NEW_SIR_TEMPLATE = "Could not create new SIR Template";
	private static final String ERROR_DELETING_ARTIFACTS = "Error deleting artifact(s)";
	private static final String ERROR_DELETING_SECURITY_INCIDENT_ATTACHMENT = "Error deleting security incident attachment for incidentId: %s";
	private static final String ERROR_GETTING_SECURITY_GROUP_MEMBERS = "Could not get resolve security group members";
	private static final String ERROR_GETTING_SECURITY_INCIDENT_BY_ID = "Could not get resolve security incident with id: %s";
	private static final String ERROR_GETTING_SIR_COUNTS_BY_INVESTIGATION_REPORT = "Error getting SIR counts by investigaion type report";
	private static final String ERROR_GETTING_SIR_COUNTS_BY_REPORT = "Error getting SIR counts by %s report";
    private static final String ERROR_GETTING_SIR_COUNTS_BY_SEVERITY_REPORT = "Error getting SIR counts by severity report";
    private static final String ERROR_GETTING_SIR_MEAN_TIME_REPORT = "Error getting SIR mean time report";
	private static final String ERROR_GETTING_SIR_COUNTS_OVER_TIME_REPORT = "Error getting SIR counts over Time report";
	private static final String ERROR_GETTING_SIR_COUNTS_REPORT = "Error getting SIR counts report";
	private static final String ERROR_RESETTING_DT_DATA = "Error resetting DT data";
	private static final String ERROR_RETRIEVING_ARTIFACTS = "Error retrieving security incident artifacts";
	private static final String ERROR_RETRIEVING_ARTIFACTS_BY_SIR = "Error retrieving security artifacts by SIR: %s";
	private static final String ERROR_RETRIEVING_ARTIFACTS_BY_SIR_AND_NAME = "Error retrieving security artifacts by SIR: %s and name: %s";
	private static final String ERROR_RETRIEVING_ATTACHMENT_BY_NAME = "Error retrieving attachment by incidentId: %s and name: %s";
	private static final String ERROR_RETRIEVING_INCIDENT_WIKI_NOTES = "Error retrieving security incident wiki notes";
	private static final String ERROR_RETRIEVING_INCIDENT_WIKI_NOTES_BY_ID = "Error retrieving security incident wiki notes by incidentId: %s";
	private static final String ERROR_RETRIEVING_REQUEST_DATA_BY_SIR = "Error retrieving request data by SIR: %s";
	private static final String ERROR_RETRIEVING_SECURITY_INCIDENT_ATTACHMENTS = "Error retrieving security incident attachments";
	private static final String ERROR_RETRIEVING_SECURITY_INCIDENT_ATTACHMENTS_BY_ID = "Error retrieving security incident attachments by incidentId: %s";
	private static final String ERROR_RETRIEVING_SECURITY_INCIDENT_AUDIT_LOGS = "Error retrieving security incident audit logs";
	private static final String ERROR_RETRIEVING_SECURITY_INCIDENT_AUDIT_LOGS_BY_ID = "Error retrieving security incident audit logs by incidentId: %s";
	private static final String ERROR_RETRIEVING_SECURITY_INCIDENT_NOTE = "Error retrieving security incident note for incidentId: %s";
	private static final String ERROR_RETRIEVING_SECURITY_INCIDENT_NOTES = "Error retrieving security incident notes";
	private static final String ERROR_RETRIEVING_SECURITY_INCIDENT_NOTES_BY_ID = "Error retrieving security incident notes by incidentId: %s";
	private static final String ERROR_RETRIEVING_SECURITY_INCIDENT_NOTES_BY_NAME = "Error retrieving security incident notes by incidentId: %s, ActivityName %s";
	private static final String ERROR_RETRIEVING_SECURITY_INCIDENTS = "Could not retrieve resolve security incidents";
	private static final String ERROR_SAVING_ALL_INCIDENT_ACTIVITIES = "Could not save all activities of incident id: %s";
	private static final String ERROR_SAVING_INCIDENT_ACTIVITY_NOTE =
			"Error saving security incident activity note for incidentId: %s, Activity Name: %s, Activity Id: %s, Component Id: %s";
	private static final String ERROR_SAVING_SECURITY_INCIDENT = "Could not save resolve security incident.";
	private static final String ERROR_SAVING_SECURITY_INCIDENT_AUDIT_LOG = "Error saving security incident audit log for incidentId: %s";
	private static final String ERROR_SAVING_SECURITY_INCIDENT_NOTE = "Error saving security incident note";
	private static final String ERROR_UPDATING_INCIDENT_ATTACHMENT = "Error updating security incident attachment for incidentId: %s";
	private static final String ERROR_UPLOADING_SECURITY_INCIDENT_ATTACHMENT = "Error uploading security incident attachment for incidentId: %s";
	private static final String ERROR_SEARCHING_SECURITY_INCIDENTS = "Error searching security incidents";

//	private static final String PlaybookUtils.USER_DOES_NOT_ERROR_MSG_PREFIX = "User %s does not";
	private static final String DENIED_ACCESS_TO_SIR_ORG_OR_NO_PERMISSION_FOR_ACTION_ON_SIR = "User does not have access to SIR's Org.";
	private static final String DENIED_ACCESS_TO_SIR_WORKSHEET = "User does not have access to SIR's Worksheet with Id %s.";
	private static final String DENIED_ACCESS_TO_SIR_ORG = "User does not have access to SIR's Org.";
	
	private static final String ERROR_SAVING_SIR_CONFIGURATION = "Could not save SIR configuration.";
	private static final String ERROR_GETTING_SIR_CONFIGURATION = "Could not get SIR configuration.";
	private static final String NO_UNDELETED_SIR_CONFIG_FOR_MSG_PREFIX = "No un-deleted SIR Config for";
	private static final String NO_UNDELETED_SIR_CONFIG_FOR_ORG = NO_UNDELETED_SIR_CONFIG_FOR_MSG_PREFIX + " specified Org exists in system.";
	
	private static final String ERROR_GETTING_SAVED_SEARCH = "Error getting saved searches";
	private static final String ERROR_SAVING_SEARCH = "Error saving search";
	private static final String ERROR_DELETING_DATA = "Error deleting data";

	private static final String ERROR_GETTING_ASSOCIATES = "Error getting associated incidents";

	private static final String ERROR_GETTING_SIR_ACTIVITY_COUNTS_BY_STATUS_REPORT = "Error getting SIR Activity count by Status report.";

	private static final String ERROR_GETTING_CASE_ATTRIBUTES = "Error getting case attributes";
	private static final String ERROR_SAVING_CASE_ATTRIBUTES = "Error saving case attributes";
	private static final String ERROR_DELETING_CASE_ATTRIBUTES = "Error deleting case attributes";

//	private static final String ERROR_MARK_AS_DUPLICATE = "Error mark security incident as duplicate. MasterId: %s DuplicateId: %s";
	private static final String ERROR_MARK_AS_DUPLICATE = "Error marking security incident as duplicate.";
	private static final String ERROR_REMOVE_DUPLICATE = "Eoror removing security incident as duplicate.";
	private static final String ERROR_LIST_DUPLICATE = "Error listing duplicate security incidents.";
	private static final String ERROR_COULD_NOT_FIND_INCIDENT_FOR_SIR_WITH_SYS_ID_PREFIX = "Could not find incident for SIR with sys_id: ";
	private static final String ERROR_COULD_NOT_FIND_MASTER_SIR_INCIDENT_WITH_SYS_ID_PREFIX = "Could not find Master SIR incident, sys_id: ";
	private static final String ERROR_MASTER_AND_DUPLICATE_NO_COMMON_ROOT_ORG_PREFIX = "Master and duplicate do not have common root Org.";
	private static final String ERROR_SAVING_ARTIFACT = "Error while saving artifact.";
	private static final String ERROR_DOWNLOAFING_ATTACHMENT = "Error while downloading an attachment.";

	private static final String ERROR_GETTING_CUSTOM_DATA_FORM = "Error getting custom data form";
	private static final String ERROR_SAVING_CUSTOM_DATA_FORM = "Error saving custom data form";
	private static final String USER_DOES_NOT_HAVE_PREMISSION_TO_PREFIX = "User does not have permission";
	private static final String ERROR_DELETING_CUSTOM_DATA_FORM = "Error deleting custom data form from SIR.";

	private static final String ERROR_MAXIMUM_LIMIT_EXCEEDED = "Maxmimum limit of items to retrieve exceeded";
	private static final String ALL_FIELD = "_all";
	
	private static final String ERROR_UPDATING_SECURITY_INCIDENT_ACTIVITIES = "Error updating activities of SIR %s.";

	/**
	 * Index a note
	 * 
	 * @param jsonProperty JSON representation of PBNotes
	 * @param request
	 * @return
	 * @throws ServletException
	 * @throws IOException
	 */
	@RequestMapping(value = "/playbook/note/save", method = { RequestMethod.GET, RequestMethod.POST }, headers = "Accept=application/json; charset=utf-8")
    @ResponseBody
	public ResponseDTO<PBNotes> saveNote(@RequestBody JSONObject jsonProperty, HttpServletRequest request)
			throws ServletException, IOException {
		ResponseDTO<PBNotes> result = new ResponseDTO<PBNotes>();
		String username = (String) request.getAttribute(Constants.HTTP_REQUEST_USERNAME);
		try {
			PBNotes pbNote = PlaybookUtils.saveNote(jsonProperty.toString(), username);

			PlaybookUtils.auditAction(pbNote.getAuditMessage(), pbNote.getIncidentId(), request.getRemoteAddr(),
					username);
			pbNote.setAuditMessage(null);

			result.setSuccess(true).setData(pbNote)
					.setMessage("Saved security incident note for SIR: " + pbNote.getSir());
		} catch (Exception e) {
            result.setSuccess(false)
            .setMessage(checkAndSetErrorMessage(
    						e, ERROR_SAVING_SECURITY_INCIDENT_NOTE, 
    						Arrays.asList(String.format(PlaybookUtils.USER_DOES_NOT_ERROR_MSG_PREFIX, username),
    									  String.format(PlaybookUtils.DENIED_VIEW_ACCESS_SECURITY_INCIDENTS, username),
    									  PlaybookUtils.ERROR_CLOSED_INCIDENT,
    									  PlaybookUtils.NOTE_TITLE_CANNOT_BE_BLANK_MSG),
    						null, null));
		}
		return result;
	}
	
	@RequestMapping(value = "/playbook/note/delete", method = { RequestMethod.GET, RequestMethod.POST }, headers = "Accept=application/json; charset=utf-8")
    @ResponseBody
    public ResponseDTO<PBNotes> deleteNotes(@RequestParam String[] ids, @RequestParam String incidentId, HttpServletRequest request) throws ServletException, IOException
    {
		ResponseDTO<PBNotes> result = new ResponseDTO<PBNotes>();
		String username = (String) request.getAttribute(Constants.HTTP_REQUEST_USERNAME);
		
		try
		{
			result = PlaybookUtils.deleteNotes(ids, incidentId, username);
		}
		catch(Exception e)
		{
			String msg = String.format(ERROR_RETRIEVING_SECURITY_INCIDENT_NOTE, incidentId);
			Log.log.error(msg, e);
            result.setSuccess(false).setMessage(msg);
		}
		
		return result;
    }
	
	/**
	 * Query index activity
	 * 
	 * @param queryDTO : Query criteria for the search.
	 * @param request
	 * @return
	 * @throws ServletException
	 * @throws IOException
	 */
	@RequestMapping(value = "/playbook/note/list", method = { RequestMethod.GET, RequestMethod.POST }, headers = "Accept=application/json; charset=utf-8")
    @ResponseBody
    public ResponseDTO<PBNotes> listNotes(@ModelAttribute QueryDTO queryDTO, HttpServletRequest request) throws ServletException, IOException
    {
		ResponseDTO<PBNotes> result = new ResponseDTO<PBNotes>();
		String username = (String) request.getAttribute(Constants.HTTP_REQUEST_USERNAME); 
		try
		{
			int limit = PlaybookSearchAPI.getItemsLimit(queryDTO.getLimit(), PropertiesUtil.getPropertyInt(PROPERTY_SIR_MAX_NOTES));
			queryDTO.setLimit(limit);
			result = PlaybookUtils.searchPBNotes(queryDTO, username);
		}
		catch(LimitExceededException e) {
			Log.log.error(ERROR_MAXIMUM_LIMIT_EXCEEDED, e);
            result.setSuccess(false).setMessage(ERROR_MAXIMUM_LIMIT_EXCEEDED);
		}
		catch(Exception e)
		{
			Log.log.error(ERROR_RETRIEVING_SECURITY_INCIDENT_NOTES, e);
            result.setSuccess(false).setMessage(ERROR_RETRIEVING_SECURITY_INCIDENT_NOTES);
		}
		
		return result;
    }
	
	/**
	 * Search all notes with the given SIR
	 * 
	 * @param incidentId : String representing Service Incident Request ID, SIR
	 * @param request
	 * @return
	 * @throws ServletException
	 * @throws IOException
	 */
	@RequestMapping(value = "/playbook/note/listBySir", method = { RequestMethod.GET, RequestMethod.POST }, headers = "Accept=application/json; charset=utf-8")
    @ResponseBody
    public ResponseDTO<PBNotes> listNotesBySir(@RequestParam String incidentId, HttpServletRequest request) throws ServletException, IOException
    {
		ResponseDTO<PBNotes> result = new ResponseDTO<PBNotes>();
		String username = (String) request.getAttribute(Constants.HTTP_REQUEST_USERNAME); 
		try
		{
			int limit = PropertiesUtil.getPropertyInt(PROPERTY_SIR_MAX_NOTES);
			result = PlaybookUtils.searchPBNotesByIncidentId(incidentId, username, limit);
		}
		catch(LimitExceededException e) {
			Log.log.error(ERROR_MAXIMUM_LIMIT_EXCEEDED, e);
            result.setSuccess(false).setMessage(ERROR_MAXIMUM_LIMIT_EXCEEDED);
		}
		catch(Exception e)
		{
		    boolean isError = !e.getMessage().startsWith(String.format(PlaybookUtils.USER_DOES_NOT_ERROR_MSG_PREFIX, username));
            
            String msg = String.format((isError ? ERROR_RETRIEVING_SECURITY_INCIDENT_NOTES_BY_ID : 
                                                  DENIED_ACCESS_TO_SIR_ORG_OR_NO_PERMISSION_FOR_ACTION_ON_SIR), 
                                       incidentId);
            
            if (isError)
            {
                Log.log.error(msg + " " + e.getLocalizedMessage() , e);
            }
            else
            {
                Log.log.warn(msg);
            }
            
            result.setSuccess(false).setMessage(msg);
		}
		
		return result;
    }
	
	/**
	 * Query Notes based on given parameters
	 * 
	 * @param category : String representing category to which the note belongs
	 * @param activityName : String representing activity name to which the note belongs
	 * @param incidentId : String representing SIR to which the note belongs
	 * @param request
	 * @return
	 * @throws ServletException
	 * @throws IOException
	 */
	@RequestMapping(value = "/playbook/note/getByParams", method = { RequestMethod.GET, RequestMethod.POST }, headers = "Accept=application/json; charset=utf-8")
    @ResponseBody
    public ResponseDTO<PBNotes> getNoteByParams(@RequestParam String category, @RequestParam String activityName, @RequestParam String incidentId,
    		HttpServletRequest request) throws ServletException, IOException
    {
		ResponseDTO<PBNotes> result = new ResponseDTO<PBNotes>();
		String username = (String) request.getAttribute(Constants.HTTP_REQUEST_USERNAME); 
		try
		{
			result = PlaybookUtils.searchPBNote(category, activityName, incidentId, username);
			
		}
		catch(Exception e)
		{
		    boolean isError = !e.getMessage().startsWith(String.format(PlaybookUtils.USER_DOES_NOT_ERROR_MSG_PREFIX, username));
            
            String msg = String.format((isError ? ERROR_RETRIEVING_SECURITY_INCIDENT_NOTES_BY_NAME : 
                                                  DENIED_ACCESS_TO_SIR_ORG_OR_NO_PERMISSION_FOR_ACTION_ON_SIR), 
                                       incidentId);
            
            if (isError)
            {
                Log.log.error(msg + " " + e.getLocalizedMessage() , e);
            }
            else
            {
                Log.log.warn(msg);
            }
            
            result.setSuccess(false).setMessage(msg);
		}
		
		return result;
    }
	
	/**
	 * Add a new artifact name (key) - value pair to an incident represented by SIR.
	 * 
	 * @param jsonProperty : JSON string representing an artifact in the format:<br>
     *      <code> {"sir":"SIR-000000001","name":"Test Name","value":"Test Value","description":"Test Desc."}</code>
	 * @param request
	 * @return
	 * @throws ServletException
	 * @throws IOException
	 */
	@SuppressWarnings("unchecked")
	@RequestMapping(value = "/playbook/artifact/save", method = { RequestMethod.GET, RequestMethod.POST }, headers = "Accept=application/json; charset=utf-8")
    @ResponseBody
    public ResponseDTO<PBArtifacts> saveArtifact(@RequestBody JSONObject jsonProperty, HttpServletRequest request) throws ServletException, IOException
    {
		ResponseDTO<PBArtifacts> result = new ResponseDTO<PBArtifacts>();
		String username = (String) request.getAttribute(Constants.HTTP_REQUEST_USERNAME);
		PBArtifacts artifact = null;
		Map<String, Object> pbArtifactMap = null;
		
		try
		{
			String artifactJson = jsonProperty.toString();
            pbArtifactMap = new ObjectMapper().readValue(artifactJson, Map.class);
            
            artifact = PlaybookUtils.saveArtifact(pbArtifactMap, false, username);
            
            PlaybookUtils.auditAction(artifact.getAuditMessage(), artifact.getIncidentId(), request.getRemoteAddr(), username);
            artifact.setAuditMessage(null);
            
            List<PBArtifacts> artifactMapList = new ArrayList<PBArtifacts>();
            artifactMapList.add(artifact);
            result.setSuccess(true).setRecords(artifactMapList).setMessage("Saved secirity incident artifact for SIR: " + pbArtifactMap.get("sir"));
		}
		catch(Exception e)
		{
            result.setSuccess(false)
            .setMessage(checkAndSetErrorMessage(
    						e, ERROR_SAVING_ARTIFACT, 
    						Arrays.asList(String.format(PlaybookUtils.USER_DOES_NOT_ERROR_MSG_PREFIX, username),
    									  String.format(PlaybookUtils.DENIED_VIEW_ACCESS_SECURITY_INCIDENTS, username),
    									  PlaybookUtils.ERROR_CLOSED_INCIDENT),
    						null, null));
		}
		
		return result;
    }
	
	/**
     * API to append a new value to an existing value of a given name (key). If name (key) is not found, an error message will be returned and a detailed message will be logged.
     * 
     * @param jsonProperty : JSON string representing an artifact in the format:<br>
     *      <code> {"sir":"SIR-000000002","name":"Test Name","value":"Test Value","description":"Test Desc."}</code>
     * @param request
     * @return
     * @throws ServletException
     * @throws IOException
     */
	@SuppressWarnings("unchecked")
    @RequestMapping(value = "/playbook/artifact/append", method = { RequestMethod.GET, RequestMethod.POST }, headers = "Accept=application/json; charset=utf-8")
    @ResponseBody
    public ResponseDTO<PBArtifacts> appendArtifact(@RequestBody JSONObject jsonProperty, HttpServletRequest request) throws ServletException, IOException
    {
        ResponseDTO<PBArtifacts> result = new ResponseDTO<PBArtifacts>();
        String username = (String) request.getAttribute(Constants.HTTP_REQUEST_USERNAME);
        PBArtifacts artifact = null;
        Map<String, Object> pbArtifactMap = null;
        
        try
        {
            String artifactJson = jsonProperty.toString();
            pbArtifactMap = new ObjectMapper().readValue(artifactJson, Map.class);
            artifact = PlaybookUtils.saveArtifact(pbArtifactMap, true, username);
            List<PBArtifacts> artifactMapList = new ArrayList<PBArtifacts>();
            artifactMapList.add(artifact);
            result.setSuccess(true).setRecords(artifactMapList)
            .setMessage(String.format(SUCCESS_APPENDING_SECURITY_INCIDENT_ARTIFACT, pbArtifactMap.get("sir")));
        }
        catch(Exception e)
        {
            result.setSuccess(false)
            .setMessage(checkAndSetErrorMessage(
							e, String.format(ERROR_APPENDING_SECURITY_INCIDENT_ARTIFACT, pbArtifactMap.get("sir")), 
							Arrays.asList(String.format(PlaybookUtils.USER_DOES_NOT_ERROR_MSG_PREFIX, username),
										  String.format(PlaybookUtils.DENIED_VIEW_ACCESS_SECURITY_INCIDENTS, username)),
							null, null));
        }
        
        return result;
    }
	
	@SuppressWarnings({ "unchecked", "rawtypes" })
    @RequestMapping(value = "/playbook/artifact/delete", method = { RequestMethod.POST }, headers = "Accept=application/json; charset=utf-8")
    @ResponseBody
    public ResponseDTO<String> deleteArtifact(@RequestBody JSONObject artifacts, @RequestParam String incidentId, HttpServletRequest request) throws ServletException, IOException
    {
        ResponseDTO<String> result = new ResponseDTO<String>();
        
        String username = (String) request.getAttribute(Constants.HTTP_REQUEST_USERNAME);
        
        Map artifact = new ObjectMapper().readValue(artifacts.toString(), Map.class);
        List<Map<String, String>> artifactList = null;
        
        if (artifact != null)
        {
            artifactList = (List)artifact.get("artifacts");
        }
        
        try
        {
            if (artifactList == null || artifactList.size() == 0)
            {
                result.setSuccess(false).setMessage("Artifact IDs must be provided");
            }
            else
            {
                PlaybookUtils.deleteArtifacts(artifactList, incidentId, username);
                result.setSuccess(true).setMessage("Successfully deleted the artifact(s)");
        
                StringBuffer values = new StringBuffer();
                for (Map<String, String> artifactMap : artifactList)
                {
                    artifactMap.remove("id");
                    values.append(" Name = '" + artifactMap.get("name") + "' and Value = '" + artifactMap.get("value") +"'");
                }
                
                String auditLog = String.format("User '%s' deleted following artifact(s): %s", username, values);
                PlaybookUtils.auditAction(auditLog, incidentId, request.getRemoteAddr(), username);
            }
        }
        catch (Exception e)
        {
            result.setSuccess(false)
            .setMessage(checkAndSetErrorMessage(
							e, ERROR_DELETING_ARTIFACTS, 
							Arrays.asList(String.format(PlaybookUtils.USER_DOES_NOT_ERROR_MSG_PREFIX, username),
										  String.format(PlaybookUtils.DENIED_VIEW_ACCESS_SECURITY_INCIDENTS, username),
										  PlaybookUtils.ERROR_CLOSED_INCIDENT),
							Arrays.asList(PlaybookUtils.RBAC_DOES_NOT_HAVE_ACCESS_TO_MESSAGE_MARKER), null));
        }

        return result;
    }
	
	/**
	 * Retrieve all artifacts
	 * 
	 * @param queryDTO : Query criteria for the search.
	 * @param request
	 * @return
	 * @throws ServletException
	 * @throws IOException
	 */
	@RequestMapping(value = "/playbook/artifact/list", method = { RequestMethod.GET, RequestMethod.POST }, headers = "Accept=application/json; charset=utf-8")
    @ResponseBody
    public ResponseDTO<PBArtifacts> listArtifacts(@ModelAttribute QueryDTO queryDTO, HttpServletRequest request) throws ServletException, IOException
    {
		ResponseDTO<PBArtifacts> result = new ResponseDTO<PBArtifacts>();
		String username = (String) request.getAttribute(Constants.HTTP_REQUEST_USERNAME); 
		try
		{
			int limit = PlaybookSearchAPI.getItemsLimit(queryDTO.getLimit(), PropertiesUtil.getPropertyInt(PROPERTY_SIR_MAX_ARTIFACTS));
			queryDTO.setLimit(limit);
			result = PlaybookUtils.searchPBArtifacts(queryDTO, username);
		}
		catch(LimitExceededException e) {
			Log.log.error(ERROR_MAXIMUM_LIMIT_EXCEEDED, e);
            result.setSuccess(false).setMessage(ERROR_MAXIMUM_LIMIT_EXCEEDED);
		}
		catch(Exception e)
		{
			Log.log.error(ERROR_RETRIEVING_ARTIFACTS, e);
            result.setSuccess(false).setMessage(ERROR_RETRIEVING_ARTIFACTS);
		}
		
		return result;
    }
	
	/**
	 * Get all artifacts belong to security incident.
	 * 
	 * @param incidentId : String representing security incident
	 * @param request
	 * @return
	 * @throws ServletException
	 * @throws IOException
	 */
	@RequestMapping(value = "/playbook/artifact/listBySir", method = { RequestMethod.GET, RequestMethod.POST }, headers = "Accept=application/json; charset=utf-8")
    @ResponseBody
    public ResponseDTO<Map<String, Object>> listArtifactsBySir(@RequestParam String sir, HttpServletRequest request) throws ServletException, IOException
    {
		ResponseDTO<Map<String, Object>> result = new ResponseDTO<Map<String, Object>>();
		String username = (String) request.getAttribute(Constants.HTTP_REQUEST_USERNAME);
		try
		{
			int limit = PropertiesUtil.getPropertyInt(PROPERTY_SIR_MAX_ARTIFACTS);
		    List<Map<String, Object>> artifactMapList = PlaybookUtils.searchPBArtifactsBySir(sir, username, limit);
		    result.setRecords(artifactMapList);
            result.setSuccess(true);
		}
		catch(LimitExceededException e) {
			Log.log.error(ERROR_MAXIMUM_LIMIT_EXCEEDED, e);
            result.setSuccess(false).setMessage(ERROR_MAXIMUM_LIMIT_EXCEEDED);
		}
		catch(Exception e)
		{
		    boolean isError = !e.getMessage().startsWith(String.format(PlaybookUtils.USER_DOES_NOT_ERROR_MSG_PREFIX, username));
            
            String msg = String.format((isError ? ERROR_RETRIEVING_ARTIFACTS_BY_SIR : 
                                                  DENIED_ACCESS_TO_SIR_ORG_OR_NO_PERMISSION_FOR_ACTION_ON_SIR), 
                                       sir);
            
            if (isError)
            {
                Log.log.error(msg + " " + e.getLocalizedMessage() , e);
            }
            else
            {
                Log.log.warn(msg);
            }
            
            result.setSuccess(false).setMessage(msg);
		}
		
		return result;
    }
	
	/**
	 * Get artifact value proving name and incident id.
	 * 
	 * @param sir : String representing security incident id
	 * @param name : String representing name (key) of an artifact
	 * @param request
	 * @return
	 * @throws ServletException
	 * @throws IOException
	 */
	@RequestMapping(value = "/playbook/artifact/getByName", method = { RequestMethod.GET, RequestMethod.POST }, headers = "Accept=application/json; charset=utf-8")
    @ResponseBody
    public ResponseDTO<PBArtifacts> getArtifactByName(@RequestParam String sir, @RequestParam String name, HttpServletRequest request) throws ServletException, IOException
    {
		ResponseDTO<PBArtifacts> result = new ResponseDTO<PBArtifacts>();
		String username = (String) request.getAttribute(Constants.HTTP_REQUEST_USERNAME);
		try
		{
			result = PlaybookUtils.searchPBArtifact(sir, name, username);
			
			result.setSuccess(true);
		}
		catch(Exception e)
		{
		    boolean isError = !e.getMessage().startsWith(String.format(PlaybookUtils.USER_DOES_NOT_ERROR_MSG_PREFIX, username));
            
            String msg = String.format((isError ? ERROR_RETRIEVING_ARTIFACTS_BY_SIR_AND_NAME : 
                                                  DENIED_ACCESS_TO_SIR_ORG_OR_NO_PERMISSION_FOR_ACTION_ON_SIR), 
                                       name);
            
            if (isError)
            {
                Log.log.error(msg + " " + e.getLocalizedMessage() , e);
            }
            else
            {
                Log.log.warn(msg);
            }
            
            result.setSuccess(false).setMessage(msg);
		}
		
		return result;
    }
	
	/**
	 * Upload incident attachment and Index it.
	 * 
	 * @param request : {@link HttpServletRequest} with attachment payload
	 * @return
	 * @throws ServletException
	 * @throws IOException
	 */
	@RequestMapping(value = "/playbook/attachment/upload", method = { RequestMethod.GET, RequestMethod.POST }, headers = "Accept=application/json; charset=utf-8")
    @ResponseBody
    public ResponseDTO<PBAttachments> uploadAttachment(HttpServletRequest request) throws ServletException, IOException
    {
		ResponseDTO<PBAttachments> result = new ResponseDTO<PBAttachments>();
		String username = (String) request.getAttribute(Constants.HTTP_REQUEST_USERNAME);
		String incidentId = null;
		try
		{
			PBAttachments attachment = PlaybookUtils.uploadAttachment(request, username);
			incidentId = attachment.getIncidentId();
			PlaybookUtils.auditAction(attachment.getAuditMessage(), incidentId, request.getRemoteAddr(), username);
			attachment.setAuditMessage(null);
			
			result.setSuccess(true).setMessage("Attachment uploaded.").setData(attachment);
		}
		catch(Exception e)
		{
		    String message = e.getMessage();
            if (StringUtils.isNotBlank(message) &&
                            (message.startsWith(String.format(PlaybookUtils.USER_DOES_NOT_ERROR_MSG_PREFIX, username)) || message.startsWith(String.format(PlaybookUtils.RBAC_IS_DENIED_ACCESS_TO_MESSAGE_MARKER, username))))
            {
                Log.log.error(message);
                result.setSuccess(false).setMessage(message);
            }
            else
            {
                message = String.format(ERROR_UPLOADING_SECURITY_INCIDENT_ATTACHMENT, incidentId);
                Log.log.error(message, e);
                result.setSuccess(false).setMessage(message);
            }
		}
		
		return result;
    }
	
	@RequestMapping(value = "/playbook/attachment/download", method = { RequestMethod.GET})
    @ResponseBody
    public ModelAndView downloadAttachment(@RequestParam String attachId, @RequestParam (required = false, defaultValue = "false") Boolean malicious, 
                    HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException
    {
		String username = (String) request.getAttribute(Constants.HTTP_REQUEST_USERNAME);
		try
		{
			PlaybookUtils.downloadAttachment(request, response, attachId, malicious, username);
		}
		catch(Exception e)
		{
		    String message = e.getMessage();
            if (StringUtils.isNotBlank(message) &&
                            (message.startsWith(String.format(PlaybookUtils.USER_DOES_NOT_ERROR_MSG_PREFIX, username)) || message.startsWith(String.format(PlaybookUtils.RBAC_IS_DENIED_ACCESS_TO_MESSAGE_MARKER, username))))
            {
                Log.log.error(message);
            }
            else
            {
                Log.log.error(ERROR_DOWNLOAFING_ATTACHMENT, e);
                throw new ServletException(e);
            }
		}
		
		return null;
    }
	
	@SuppressWarnings({ "unchecked", "rawtypes" })
	@RequestMapping(value = "/playbook/attachment/delete", method = { RequestMethod.GET, RequestMethod.POST }, headers = "Accept=application/json; charset=utf-8")
    @ResponseBody
    public ResponseDTO<PBAttachments> deleteAttachments(@RequestBody JSONObject attachments, @RequestParam String incidentId, HttpServletRequest request) throws ServletException, IOException
    {
		ResponseDTO<PBAttachments> result = new ResponseDTO<PBAttachments>();
		String username = (String) request.getAttribute(Constants.HTTP_REQUEST_USERNAME);
		try
		{
		    // Check's users access to SIR's Org
		    PlaybookUtils.getSecurityIncident(incidentId, null, username);
		    
			Map artifact = new ObjectMapper().readValue(attachments.toString(), Map.class);
	        List<Map<String, String>> attachmentList = null;
	        
	        if (artifact != null)
	        {
	        	attachmentList = (List)artifact.get("attachments");
	        }
            
	        PlaybookUtils.deleteAttachments(attachmentList, username);
	        List<String> deletedAttachments = attachmentList.stream().parallel().map(m -> m.get("name"))
	        								  .collect(Collectors.toList());

            String auditLog = String.format("User '%s' deleted following attachment(s): %s", username, deletedAttachments);
            PlaybookUtils.auditAction(auditLog, incidentId, request.getRemoteAddr(), username);
            
			result.setSuccess(true).setMessage("Attachment(s) deleted.");
		}
		catch(Exception e)
		{
            result.setSuccess(false).
            setMessage(checkAndSetErrorMessage(
							e, String.format(ERROR_DELETING_SECURITY_INCIDENT_ATTACHMENT, incidentId),
							Arrays.asList(String.format(PlaybookUtils.USER_DOES_NOT_ERROR_MSG_PREFIX, username),
										  String.format(PlaybookUtils.DENIED_VIEW_ACCESS_SECURITY_INCIDENTS, username)),
							Arrays.asList(PlaybookUtils.RBAC_DOES_NOT_HAVE_ACCESS_TO_MESSAGE_MARKER), null));
		}
		
		return result;
    }
	
	/**
	 * List all attachments
	 * 
	 * @param queryDTO : {@ink QueryDTO} containing search criteria
	 * @param request
	 * @return
	 * @throws ServletException
	 * @throws IOException
	 */
	@RequestMapping(value = "/playbook/attachment/list", method = { RequestMethod.GET, RequestMethod.POST }, headers = "Accept=application/json; charset=utf-8")
    @ResponseBody
    public ResponseDTO<PBAttachments> listAttachments(@ModelAttribute QueryDTO queryDTO, HttpServletRequest request) throws ServletException, IOException
    {
		ResponseDTO<PBAttachments> result = new ResponseDTO<PBAttachments>();
		String username = (String) request.getAttribute(Constants.HTTP_REQUEST_USERNAME); 
		try
		{
			int limit = PlaybookSearchAPI.getItemsLimit(queryDTO.getLimit(), PropertiesUtil.getPropertyInt(PROPERTY_SIR_MAX_ATTACHMENTS));
			queryDTO.setLimit(limit);
			result = PlaybookUtils.searchPBAttachments(queryDTO, username);
		}
		catch(LimitExceededException e) {
			Log.log.error(ERROR_MAXIMUM_LIMIT_EXCEEDED, e);
            result.setSuccess(false).setMessage(ERROR_MAXIMUM_LIMIT_EXCEEDED);
		}
		catch(Exception e)
		{
			Log.log.error(ERROR_RETRIEVING_SECURITY_INCIDENT_ATTACHMENTS, e);
            result.setSuccess(false).setMessage(ERROR_RETRIEVING_SECURITY_INCIDENT_ATTACHMENTS);
		}
		
		return result;
    }
	
	/**
	 * List all attachments belonging to an incident id
	 * 
	 * @param incidentId : String representing incident id
	 * @param request
	 * @return
	 * @throws ServletException
	 * @throws IOException
	 */
	@RequestMapping(value = "/playbook/attachment/listBySir", method = { RequestMethod.GET, RequestMethod.POST }, headers = "Accept=application/json; charset=utf-8")
    @ResponseBody
    public ResponseDTO<PBAttachments> listAttachmentsBySir(@RequestParam("incidentId") String incidentId, @RequestParam("docFullName") String docFullName, 
    		HttpServletRequest request) throws ServletException, IOException
    {
		ResponseDTO<PBAttachments> result = new ResponseDTO<PBAttachments>();
		String username = (String) request.getAttribute(Constants.HTTP_REQUEST_USERNAME);
		try
		{
			/*
			 * Limitation in UI uploader component forces to pass incidentId in docFullName. 
			 */
			if (StringUtils.isBlank(incidentId))
			{
				incidentId = docFullName;
			}
			int limit = PropertiesUtil.getPropertyInt(PROPERTY_SIR_MAX_ATTACHMENTS);
			result = PlaybookUtils.searchPBAttachmentsByIncident(incidentId, username, limit);
		}
		catch(LimitExceededException e) {
			Log.log.error(ERROR_MAXIMUM_LIMIT_EXCEEDED, e);
            result.setSuccess(false).setMessage(ERROR_MAXIMUM_LIMIT_EXCEEDED);
		}
		catch(Exception e)
		{
		    boolean isError = !e.getMessage().startsWith(String.format(PlaybookUtils.USER_DOES_NOT_ERROR_MSG_PREFIX, username));
            
            String msg = String.format((isError ? ERROR_RETRIEVING_SECURITY_INCIDENT_ATTACHMENTS_BY_ID : 
                                                  DENIED_ACCESS_TO_SIR_ORG_OR_NO_PERMISSION_FOR_ACTION_ON_SIR), 
                                       incidentId);
            
            if (isError)
            {
                Log.log.error(msg + " " + e.getLocalizedMessage() , e);
            }
            else
            {
                Log.log.warn(msg);
            }
            
            result.setSuccess(false).setMessage(msg);
		}
		
		return result;
    }
	
	/**
	 * Get attachment by given file name and security incident id.
	 * 
	 * @param incidentId : String representing security incident id
	 * @param name : String representing name (key) of an artifact whose value is needed.
	 * @param request
	 * @return
	 * @throws ServletException
	 * @throws IOException
	 */
	@RequestMapping(value = "/playbook/attachment/getByName", method = { RequestMethod.GET, RequestMethod.POST }, headers = "Accept=application/json; charset=utf-8")
    @ResponseBody
    public ResponseDTO<PBAttachments> getAttachmentByName(@RequestParam String incidentId, @RequestParam String name, HttpServletRequest request) throws ServletException, IOException
    {
		ResponseDTO<PBAttachments> result = new ResponseDTO<PBAttachments>();
		String username = (String) request.getAttribute(Constants.HTTP_REQUEST_USERNAME);
		try
		{
			result = PlaybookUtils.searchPBAttachmentsByName(incidentId, name, username);
		}
		catch(Exception e)
		{
		    boolean isError = !e.getMessage().startsWith(String.format(PlaybookUtils.USER_DOES_NOT_ERROR_MSG_PREFIX, username));
            
            String msg = String.format((isError ? ERROR_RETRIEVING_ATTACHMENT_BY_NAME : 
                                                  DENIED_ACCESS_TO_SIR_ORG_OR_NO_PERMISSION_FOR_ACTION_ON_SIR), 
                                       name);
            
            if (isError)
            {
                Log.log.error(msg + " " + e.getLocalizedMessage() , e);
            }
            else
            {
                Log.log.warn(msg);
            }
            
            result.setSuccess(false).setMessage(msg);
		}
		
		return result;
    }
	
	/**
	 * Index Audit Log
	 * 
	 * @param jsonProperty : JSON string representing {@link PBAuditLog}
	 * @param request
	 * @return
	 * @throws ServletException
	 * @throws IOException
	 */
	@RequestMapping(value = "/playbook/auditlog/save", method = { RequestMethod.GET, RequestMethod.POST }, headers = "Accept=application/json; charset=utf-8")
    @ResponseBody
    public ResponseDTO<PBAuditLog> saveAuditLog(@RequestBody JSONObject jsonProperty, HttpServletRequest request) throws ServletException, IOException
    {
		ResponseDTO<PBAuditLog> result = new ResponseDTO<PBAuditLog>();
		String username = (String) request.getAttribute(Constants.HTTP_REQUEST_USERNAME);
		String incidentId = null;
		try
		{
			String json = jsonProperty.toString();
            PBAuditLog auditLog = new ObjectMapper().readValue(json, PBAuditLog.class);
            incidentId = auditLog.getIncidentId();
            auditLog.setUserName(username);
            auditLog.setIpAddress(request.getRemoteAddr());
            // PlaybookUtils.indexAuditLog(auditLog, username);
            result.setSuccess(true).setMessage("Saved secirity incident audit log for SIR: " + auditLog.getIncidentId());
		}
		catch(Exception e)
		{
		    boolean isError = !e.getMessage().startsWith(String.format(PlaybookUtils.USER_DOES_NOT_ERROR_MSG_PREFIX, username));
            
            String msg = String.format((isError ? ERROR_SAVING_SECURITY_INCIDENT_AUDIT_LOG : 
                                                  DENIED_ACCESS_TO_SIR_ORG_OR_NO_PERMISSION_FOR_ACTION_ON_SIR), 
                                       incidentId);
            
            if (isError)
            {
                Log.log.error(msg + " " + e.getLocalizedMessage() , e);
            }
            else
            {
                Log.log.warn(msg);
            }
            
            result.setSuccess(false).setMessage(msg);
		}
		
		return result;
    }
	
	/**
	 * List Audit Log
	 * 
	 * @param queryDTO : {@ink QueryDTO} containing search criteria
	 * @param request
	 * @return
	 * @throws ServletException
	 * @throws IOException
	 */
	@RequestMapping(value = "/playbook/auditlog/list", method = { RequestMethod.GET, RequestMethod.POST }, headers = "Accept=application/json; charset=utf-8")
    @ResponseBody
    public ResponseDTO<PBAuditLog> listAuditLogs(@ModelAttribute QueryDTO queryDTO, HttpServletRequest request) throws ServletException, IOException
    {
		ResponseDTO<PBAuditLog> result = new ResponseDTO<PBAuditLog>();
		String username = (String) request.getAttribute(Constants.HTTP_REQUEST_USERNAME); 
		try
		{
			int limit = PlaybookSearchAPI.getItemsLimit(queryDTO.getLimit(), PropertiesUtil.getPropertyInt(PROPERTY_SIR_MAX_AUDIT_LOGS));
			queryDTO.setLimit(limit);
			result = PlaybookUtils.searchPBAuditLogs(queryDTO, username);
		}
		catch(LimitExceededException e) {
			Log.log.error(ERROR_MAXIMUM_LIMIT_EXCEEDED, e);
            result.setSuccess(false).setMessage(ERROR_MAXIMUM_LIMIT_EXCEEDED);
		}
		catch(Exception e)
		{
		    String message = e.getMessage();
		    if (StringUtils.isNotBlank(message) &&
		                    (message.startsWith(String.format(PlaybookUtils.USER_DOES_NOT_ERROR_MSG_PREFIX, username)) || message.startsWith(String.format(PlaybookUtils.RBAC_IS_DENIED_ACCESS_TO_MESSAGE_MARKER, username))))
		    {
		        Log.log.error(message);
		        result.setSuccess(false).setMessage(message);
		    }
		    else
		    {
    			Log.log.error(ERROR_RETRIEVING_SECURITY_INCIDENT_AUDIT_LOGS, e);
                result.setSuccess(false).setMessage(ERROR_RETRIEVING_SECURITY_INCIDENT_AUDIT_LOGS);
		    }
		}
		
		return result;
    }
	
	/**
	 * List security incident related audit logs
	 * 
	 * @param incidentId : String representing security incident id
	 * @param request
	 * @return
	 * @throws ServletException
	 * @throws IOException
	 */
	@RequestMapping(value = "/playbook/auditlog/listBySir", method = { RequestMethod.GET, RequestMethod.POST }, headers = "Accept=application/json; charset=utf-8")
    @ResponseBody
    public ResponseDTO<PBAuditLog> listAuditLogBySir(@RequestParam String incidentId, HttpServletRequest request) throws ServletException, IOException
    {
		ResponseDTO<PBAuditLog> result = new ResponseDTO<PBAuditLog>();
		String username = (String) request.getAttribute(Constants.HTTP_REQUEST_USERNAME);
		try
		{
			int limit = PropertiesUtil.getPropertyInt(PROPERTY_SIR_MAX_AUDIT_LOGS);
			result = PlaybookUtils.searchPBAuditLogsBySIR(incidentId, username, limit);
		}
		catch(LimitExceededException e) {
			Log.log.error(ERROR_MAXIMUM_LIMIT_EXCEEDED, e);
            result.setSuccess(false).setMessage(ERROR_MAXIMUM_LIMIT_EXCEEDED);
		}
		catch(Exception e)
		{
		    boolean isError = !e.getMessage().startsWith(String.format(PlaybookUtils.USER_DOES_NOT_ERROR_MSG_PREFIX, username));
            
            String msg = String.format((isError ? ERROR_RETRIEVING_SECURITY_INCIDENT_AUDIT_LOGS_BY_ID : 
                                                  DENIED_ACCESS_TO_SIR_ORG_OR_NO_PERMISSION_FOR_ACTION_ON_SIR), 
                                       incidentId);
            
            if (isError)
            {
                Log.log.error(msg + " " + e.getLocalizedMessage() , e);
            }
            else
            {
                Log.log.warn(msg);
            }
            
            result.setSuccess(false).setMessage(msg);
		}
		
		return result;
    }

	/**
	 * List Resolve Security Incidents
	 * 
	 * @param queryDTO : {@ink QueryDTO} containing search criteria
	 * @param request
	 * @return
	 * @throws ServletException
	 * @throws IOException
	 */
	@RequestMapping(value = "/playbook/securityIncident/list", method = { RequestMethod.GET, RequestMethod.POST }, headers = "Accept=application/json; charset=utf-8")
	@ResponseBody
	public ResponseDTO<ResolveSecurityIncidentVO> listSecurityIncidents(
		@ModelAttribute QueryDTO queryDTO,
		@RequestParam(required=false) boolean includeDeleted,
		HttpServletRequest request) throws ServletException, IOException
	{
	    // This is an ad-hoc structure allowing adding mix AND/OR operations of the query's filters.
	    class CustomQueryDTO extends QueryDTO {
	        private String customFilters;
	        CustomQueryDTO(QueryDTO query, String customFilters) {
                super(query.getStart(), query.getLimit(), query.getOperator());
				this.setModelName(query.getModelName());
				this.setFilterItems(query.getFilterItems());
				this.setSortItems(query.getSortItems());
				this.customFilters = customFilters;
				}
			/**
			 * Get the where clause and add the custom AND operators
			 */
			@Override
			public String getFilterWhereClause() {
				String	whereClause	=	super.getFilterWhereClause();
				if (customFilters == null || customFilters.isEmpty()) {
				    return whereClause;
				}
				return "(" + whereClause + "AND " + customFilters + ")";
				}
		}
		ResponseDTO<ResolveSecurityIncidentVO> result = new ResponseDTO<ResolveSecurityIncidentVO>();
		String username = (String) request.getAttribute(Constants.HTTP_REQUEST_USERNAME);
		try
		{
			if (!includeDeleted) {
				queryDTO.addFilterItem(new QueryFilter(QueryFilter.BOOL_TYPE, "false|null", "sysIsDeleted", QueryFilter.EQUALS));
			}
			queryDTO.addFilterItem(new QueryFilter("auto", "null", "masterSir", QueryFilter.EQUALS));
			
			QueryFilter orgFilter = null;
			boolean hasNoOrgAcess = false;
            
			Collection<String> orgIdList = null;
            
            if (queryDTO.getFilterItems() != null && !queryDTO.getFilterItems().isEmpty())
            {
                for (QueryFilter qryFltr : queryDTO.getFilterItems())
                {
                    if ("sysOrg".equals(qryFltr.getField()))
                    {
                        orgFilter = qryFltr;
                        
                        if (StringUtils.isNotBlank(qryFltr.getValue()) && 
                            !qryFltr.getValue().equalsIgnoreCase(VO.STRING_DEFAULT) &&
                            !qryFltr.getValue().equalsIgnoreCase(Constants.NIL_STRING))
                        {
                            Set<OrgsVO> hierarchyOrgVOs = UserUtils.getAllOrgsInHierarchy(qryFltr.getValue(), null);
                            
                            if (hierarchyOrgVOs != null && !hierarchyOrgVOs.isEmpty())
                            {
                                for (OrgsVO hierarchyOrgVO : hierarchyOrgVOs)
                                {
                                    if (hierarchyOrgVO.getUHasNoOrgAccess().booleanValue())
                                    {
                                        hasNoOrgAcess = true;
                                    }
                                    
                                    if (orgIdList == null)
                                    {
                                        orgIdList = new ArrayList<String>(); 
                                    }
                                    
                                    orgIdList.add(hierarchyOrgVO.getSys_id());
                                }
                            }
                        }
                        else
                        {
                            hasNoOrgAcess = UserUtils.isNoOrgAccessible(username);
                        }
                        
                        break;
                    }
                }                
            }
            else
            {
                orgIdList = UserUtils.getUserOrgHierarchyIdList(username);
                hasNoOrgAcess = UserUtils.isNoOrgAccessible(username);
            }
            
            if (orgIdList != null && !orgIdList.isEmpty())
            {
                String orgIdListQueryString = StringUtils.collectionToString(orgIdList, "|");
                
                if (StringUtils.isNotBlank(orgIdListQueryString))
                {
                    if (hasNoOrgAcess)
                    {
                        orgIdListQueryString += "|nil";
                    }
                }
                
                if (orgFilter != null)
                {
                    orgFilter.setValue(orgIdListQueryString);
                    orgFilter.setType("auto");
                    orgFilter.setCondition("equals");
                    orgFilter.setCaseSensitive(Boolean.FALSE);
                }
                else
                {
                    orgFilter = new QueryFilter("auto", orgIdListQueryString, "sysOrg", "equals");
                    queryDTO.addFilterItem(orgFilter);
                }
            }
            else
            {
                if (hasNoOrgAcess)
                {
                    if (orgFilter != null)
                    {
                        orgFilter.setValue("nil");
                        orgFilter.setType("auto");
                        orgFilter.setCondition("equals");
                        orgFilter.setCaseSensitive(Boolean.FALSE);
                    }
                    else
                    {
                        orgFilter = new QueryFilter("auto", "nil", "sysOrg", "equals");
                        queryDTO.addFilterItem(orgFilter);
                    }
                }
            }
            
			// Consider this "admin,admin, admin, admin ,admin"
			String customFilters  = PlaybookUtils.buildUserOnlyVisibilityCondition(username);
			String updatedCustomFilters = PlaybookUtils.buildRiskScoreCondition(queryDTO, customFilters);
			CustomQueryDTO customQueryDTO = new CustomQueryDTO(queryDTO, updatedCustomFilters);
			List<ResolveSecurityIncidentVO> list = PlaybookUtils.listSecurityIncidents(customQueryDTO, username);
			int total = ServiceHibernate.getTotalHqlDistinctSysIdCount(customQueryDTO);
			result.setSuccess(true).setRecords(list).setTotal(total);
		}
		catch(Exception e)
		{
			boolean isError = StringUtils.isNotBlank(e.getMessage()) && 
			  				  !e.getMessage().startsWith(USER_DOES_NOT_HAVE_PREMISSION_TO_PREFIX);

			String msg = isError ? ERROR_RETRIEVING_SECURITY_INCIDENTS : "";

			if (isError)
			{
				Log.log.error(msg + " " + e.getLocalizedMessage() , e);
			}
			else
			{
				Log.log.warn(e.getLocalizedMessage());
			}

			result.setSuccess(false).setMessage(ERROR_RETRIEVING_SECURITY_INCIDENTS + 
												(isError ? "" : " " + e.getLocalizedMessage()));
		}
		
		return result;
	}
	
	/**
	 * API to Save/Update resolve security incident
	 * 
	 * Here's a sample input JSON string representing ResolveSecurityIncident:
	 * <pre><code>
	 * {
	   "title": "Phishing Investigation", // Title of an incident.
	   "investigationType": "Phishing",   // phishing, malware, etc...
	   "playbook": "Phishing.Template",   // playbook associated with this incident.
	   "severity": "Critical",            // Severity of this incident. Critical, High, Medium, Low etc...
	   "owner": "admin",                  // User who is an owner of this incident.
	   "sourceSystem": "Resolve",         // Source system from where the incident generated (Resolve or Splunk or Arcsight, etc...)
	   "externalReferenceId": "",         // If the sourceSystem is not Resolve, this field indicates the assigned ID from the external system.
	   "status": "Open",                  // Status of the incident in Resolve. (Open, In Progress, Closed, etc...)
	   "problemId": ""                    // problem id (worksheet id) of an assigned worksheet.
	 * }
	 * </code>
	 * 
	 * If the sourceSystem is Resolve, externalReferenceId is ignored.
	 * If problemId field is empty, new worksheet will be created. Not otherwise.
	 * During save operation, incident will be searched for sourceSystem, externalReferenceId and problemId. If an incident is found, it will be updated. New incident will be created oterwise.
	 * If user assigned as an owner does not belong to the security group(s) specified in the system property, app.security.groups, he will not be assigned as an owner.
	 * <p>
	 * @param jsonProperty : JSON string representing {@link ResolveSecurityIncident}
	 * @param request
	 * @return
	 * 		<code>{@link ResponseDTO<ResolveSecurityIncidentVO>}</code>
	 * @throws ServletException
	 * @throws IOException
	 */
	@RequestMapping(value = "/playbook/securityIncident/save", method = { RequestMethod.GET, RequestMethod.POST }, headers = "Accept=application/json; charset=utf-8")
    @ResponseBody
    public ResponseDTO<ResolveSecurityIncidentVO> saveSecurityIncident(@RequestBody JSONObject jsonProperty, HttpServletRequest request) throws ServletException, IOException
    {
		ResponseDTO<ResolveSecurityIncidentVO> result = new ResponseDTO<ResolveSecurityIncidentVO>();
		String username = (String) request.getAttribute(Constants.HTTP_REQUEST_USERNAME);
		
		try
		{
		    jsonProperty.remove("sysCreatedOn"); //We don't need sysCreatedOn field from front-end. It's done once and by the back-end.
		    String json = jsonProperty.toString();
			ObjectMapper mapper = new ObjectMapper();
			mapper.configure(Feature.FAIL_ON_UNKNOWN_PROPERTIES, false);			
			ResolveSecurityIncidentVO jsonVO = mapper.readValue(json, ResolveSecurityIncidentVO.class);
			ResolveSecurityIncident model = new ResolveSecurityIncident();
			model.applyVOToModel(jsonVO);

			if (StringUtils.isNotBlank(model.getSysOrg()) && 
			    (model.getSysOrg().equalsIgnoreCase(VO.STRING_DEFAULT) ||
			     model.getSysOrg().equalsIgnoreCase(Constants.NIL_STRING)))
			{
			    model.setSysOrg(null);
			}

			ResolveSecurityIncidentVO vo = PlaybookUtils.saveSecurityIncident(model, username);
			result.setSuccess(true).setData(vo).setMessage("Saved resolve security incident.");

			if (StringUtils.isNotBlank(vo.getAuditMessage()) && !vo.getAuditMessage().equals("UNDEFINED"))
			    PlaybookUtils.auditAction(vo.getAuditMessage(), vo.getSys_id(), request.getRemoteAddr(), username);
            vo.setAuditMessage(null);
		}
		catch(Exception e)
		{
            result.setSuccess(false)
            .setMessage(checkAndSetErrorMessage(
    						e, ERROR_SAVING_SECURITY_INCIDENT, 
    						Arrays.asList(String.format(PlaybookUtils.USER_DOES_NOT_ERROR_MSG_PREFIX, username),
    									  PlaybookUtils.BLANK_EXT_REF_ID_ERR_MSG, PlaybookUtils.ERROR_CLOSED_INCIDENT),
    						Arrays.asList(PlaybookUtils.RBAC_IS_DENIED_ACCESS_TO_MESSAGE_MARKER), null));
		}
		
		return result;
    }
	
	/**
	 * Get a security incident
	 * 
	 * @param incidentId : String representing sysId of the security incident.
	 * @param request
	 * @return
	 * @throws ServletException
	 * @throws IOException
	 */
	@RequestMapping(value = "/playbook/securityIncident/get", method = { RequestMethod.GET, RequestMethod.POST }, headers = "Accept=application/json; charset=utf-8")
    @ResponseBody
    public ResponseDTO<Map<String, Object>> getSecurityIncident(@RequestParam(value = "incidentId", required = false) String incidentId,
    		@RequestParam(value = "sir", required = false) String sir,
    		HttpServletRequest request) throws ServletException, IOException
    {
		ResponseDTO<Map<String, Object>> result = new ResponseDTO<Map<String, Object>>();
		Map<String, Object> map = new HashMap<String, Object>();
		String username = (String) request.getAttribute(Constants.HTTP_REQUEST_USERNAME); 
		try
		{
			ResolveSecurityIncidentVO vo = PlaybookUtils.getSecurityIncident(incidentId, sir, username);
            if (vo == null || vo.getSysIsDeleted()) {
                return result.setSuccess(false).setMessage("Could not get resolve security incident with id: " + (incidentId != null? incidentId : sir));
            }
			String[] membersStr = {};
			
			if (vo.getMembers() != null)
			{
			    membersStr = vo.getMembers().split(",");
			}
			
			String owner = vo.getOwner();
			boolean accessOk = true;
			//
			// Allow access if user has permission to view all investigation
			//
            boolean pViewAll = UserUtils.getUserfunctionPermission(username, RBACUtils.RBAC_SIR_DASHBOARD_INCIDENTLIST_VIEWALL, null);
			if (pViewAll) {
			    accessOk = true;
			} else if (!username.equals(owner)) {
			    accessOk = false;
	            for (int i=0; i<membersStr.length; i++) {
	                if (username.equals(membersStr[i].trim())) {
	                    accessOk = true;
	                    break;
	                }
	            }
			}
			if (vo.getSysIsDeleted() || !accessOk) {
                result.setSuccess(false).setMessage("Could not get resolve security incident with id: " + incidentId);
			}
			else if (vo != null)
			{
				List<ActivityVO> activityList = PlaybookUtils.getAllPbActivities(vo.getSys_id(), vo.getPlaybook(), 
				                                                                 vo.getPlaybookVersion(), 
				                                                                 vo.getSysOrg(), username);
				Map<String, Object> automationFilterMap = PlaybookUtils.getAutomationFilter(vo.getPlaybook(), vo.getPlaybookVersion());
				map.put("INCIDENT_INFO",  vo);
				map.put("INCIDENT_ACTIVITIES",  activityList);
				map.put("AUTOMATION_FILTER", automationFilterMap);
				result.setSuccess(true).setData(map);
			}
		}
		catch(Exception e)
		{
			Log.log.error(e,e);
		    boolean isError = !e.getMessage().startsWith(String.format(PlaybookUtils.USER_DOES_NOT_ERROR_MSG_PREFIX, username));
		    
			String msg = String.format((isError ? ERROR_GETTING_SECURITY_INCIDENT_BY_ID : 
			                                      DENIED_ACCESS_TO_SIR_ORG_OR_NO_PERMISSION_FOR_ACTION_ON_SIR), 
			                           incidentId);
			
			if (isError)
			{
			    Log.log.error(msg + " " + e.getLocalizedMessage() , e);
			}
			else
			{
			    Log.log.warn(msg);
			}
			
            result.setSuccess(false).setMessage(msg);
		}
		
		return result;
    }
	
	/**
	 * 
	 * @param request
	 * @return
	 * @throws ServletException
	 * @throws IOException
	 */
	@RequestMapping(value = "/playbook/securityIncident/getSecurityGroup", method = { RequestMethod.GET, RequestMethod.POST }, headers = "Accept=application/json; charset=utf-8")
    @ResponseBody
    public ResponseDTO<Map<String, String>> getSecurityGroupMembers(HttpServletRequest request) throws ServletException, IOException
    {
		ResponseDTO<Map<String, String>> result = new ResponseDTO<Map<String, String>>();
		
		String username = (String) request.getAttribute(Constants.HTTP_REQUEST_USERNAME); 
		try
		{
			List<Map<String, String>> memberList = PlaybookUtils.getSecurityGroupMembers(username);
			result.setSuccess(true).setRecords(memberList);
		}
		catch(Exception e)
		{
			Log.log.error(ERROR_GETTING_SECURITY_GROUP_MEMBERS, e);
            result.setSuccess(false).setMessage(ERROR_GETTING_SECURITY_GROUP_MEMBERS);
		}
		
		return result;
    }
	
	/**
	 * Close a security incident
	 * 
	 * @param incidentId : String representing security incident id.
	 * @param sir : String representing human redable security incident id. e.g. SIR-000000001
	 * @param request
	 * @return
	 * @throws ServletException
	 * @throws IOException
	 */
	@RequestMapping(value = "/playbook/securityIncident/close", method = { RequestMethod.GET, RequestMethod.POST }, headers = "Accept=application/json; charset=utf-8")
    @ResponseBody
    public ResponseDTO<ResolveSecurityIncidentVO> closeIncident(@RequestParam(value = "incidentId", required = false) String incidentId,
    		@RequestParam(value = "sir", required = false) String sir, HttpServletRequest request) throws ServletException, IOException
    {
		ResponseDTO<ResolveSecurityIncidentVO> result = new ResponseDTO<ResolveSecurityIncidentVO>();
		String username = (String) request.getAttribute(Constants.HTTP_REQUEST_USERNAME); 
		try
		{
			ResolveSecurityIncidentVO vo = PlaybookUtils.closeIncident(incidentId, sir, username);
			result.setData(vo).setSuccess(true);
			
		}
		catch(Exception e)
		{
            result.setSuccess(false)
            .setMessage(checkAndSetErrorMessage(
                			e, String.format(ERROR_CLOSING_SECURITY_INCIDENT_BY_ID, incidentId), 
                			Arrays.asList(String.format(PlaybookUtils.USER_DOES_NOT_ERROR_MSG_PREFIX, username),
                						  String.format(PlaybookUtils.DENIED_VIEW_ACCESS_SECURITY_INCIDENTS, username),
                						  PlaybookUtils.COMPLETE_REQUIRED_ACTIVITIES),
                			Arrays.asList(PlaybookUtils.RBAC_IS_DENIED_ACCESS_TO_MESSAGE_MARKER), null));
		}
		
		return result;
    }
	
	@SuppressWarnings("unchecked")
	@RequestMapping(value = "/playbook/activity/update", method = { RequestMethod.GET, RequestMethod.POST }, headers = "Accept=application/json; charset=utf-8")
    @ResponseBody
    public ResponseDTO<ActivityVO> updateActivity(@RequestBody JSONObject jsonProperty, HttpServletRequest request) throws ServletException, IOException
    {
		ResponseDTO<ActivityVO> result = new ResponseDTO<ActivityVO>();
		String username = (String) request.getAttribute(Constants.HTTP_REQUEST_USERNAME);
		Map<String, Object> activityMap = null;
		
		try
		{
			String json = jsonProperty.toString();
			ObjectMapper mapper = new ObjectMapper().configure(DeserializationConfig.Feature.FAIL_ON_UNKNOWN_PROPERTIES, false);
			activityMap = mapper.readValue(json, Map.class);
			String name = (String)activityMap.get(WikiDocumentVO.PLAYBOOK_ACTIVITY_MAP_ACITIVITY_NAME_KEY);
			if (StringUtils.isBlank(name))
			{
				throw new Exception("Activity name cannot be an empty string or only spaces.");
			}
			activityMap.put(WikiDocumentVO.PLAYBOOK_ACTIVITY_MAP_ACITIVITY_NAME_KEY, name.trim());
			
			ActivityVO activity = PlaybookUtils.updateActivity(activityMap, username);
			
			PlaybookUtils.auditAction(activity.getAuditMessage(), activity.getIncidentId(), request.getRemoteAddr(), username);
			activity.setAuditMessage(null);
			
			result.setSuccess(true).setData(activity);
		}
		catch(Exception e)
		{
            result.setSuccess(false)
            .setMessage(checkAndSetErrorMessage(
    						e, String.format(ERROR_UPDATING_SECURITY_INCIDENT_ACTIVITIES, 
    										 (activityMap != null ? (String)activityMap.get("incidentId") : "null")), 
    						Arrays.asList(String.format(PlaybookUtils.USER_DOES_NOT_ERROR_MSG_PREFIX, username),
    									  String.format(PlaybookUtils.DENIED_VIEW_ACCESS_SECURITY_INCIDENTS, username)),
    						Arrays.asList(PlaybookActivityUtils.DUPLICATE_ACTIVITIES_TASKS_IN_PHASE_PROHIBITED_MSG,
    									  PlaybookUtils.RBAC_IS_DENIED_ACCESS_TO_MESSAGE_MARKER), null));
		}
		
		return result;
    }
	
	/**
	 * Save all activities. This API will be invoked mainly when user changes the order of any activity.
	 * 
	 * @param jsonProperty : JSONObject representing array of activities
	 * @param incidentId : String representing incident id of an incident of which we are changing the activity order
	 * @param request
	 * @return
	 * @throws ServletException
	 * @throws IOException
	 */
	@RequestMapping(value = "/playbook/activity/saveAll", method = { RequestMethod.GET, RequestMethod.POST }, headers = "Accept=application/json; charset=utf-8")
    @ResponseBody
    public ResponseDTO<ActivityVO> saveActivities(@RequestBody JSONArray jsonProperty, @RequestParam String incidentId, HttpServletRequest request) throws ServletException, IOException
    {
		ResponseDTO<ActivityVO> result = new ResponseDTO<ActivityVO>();
		String username = (String) request.getAttribute(Constants.HTTP_REQUEST_USERNAME);
		try
		{
			List<ActivityVO> activityVOList = PlaybookUtils.saveActivities(incidentId, jsonProperty, username);
			result.setSuccess(true).setRecords(activityVOList);
		}
		catch(Exception e)
		{
		    boolean isError = !e.getMessage().startsWith(String.format(PlaybookUtils.USER_DOES_NOT_ERROR_MSG_PREFIX, username));
            
            String msg = String.format((isError ? ERROR_SAVING_ALL_INCIDENT_ACTIVITIES : 
                                                  DENIED_ACCESS_TO_SIR_ORG_OR_NO_PERMISSION_FOR_ACTION_ON_SIR), 
                                       incidentId);
            
            if (isError)
            {
                Log.log.error(msg + " " + e.getLocalizedMessage() , e);
            }
            else
            {
                Log.log.warn(msg);
            }
            
            result.setSuccess(false).setMessage(msg);
		}
		
		return result;
    }
	
	/**
	 * Add a new activity
	 * 
	 * @param jsonProperty : JSONObject representing activity in json format
	 * @param incidentId : String representing incident id of an incident of which we are changing the activity order
	 * @param request
	 * @return
	 * @throws ServletException
	 * @throws IOException
	 */
	@SuppressWarnings("unchecked")
	@RequestMapping(value = "/playbook/activity/add", method = { RequestMethod.GET, RequestMethod.POST }, headers = "Accept=application/json; charset=utf-8")
    @ResponseBody
    public ResponseDTO<ActivityVO> addActivity(@RequestBody JSONObject jsonProperty, HttpServletRequest request) throws ServletException, IOException
    {
		ResponseDTO<ActivityVO> result = new ResponseDTO<ActivityVO>();
		String username = (String) request.getAttribute(Constants.HTTP_REQUEST_USERNAME);
		String incidentId = null;
		try
		{
			String activityJson = jsonProperty.toString();
			Map<String, String> activityMap = new ObjectMapper().readValue(activityJson, Map.class);
			if (activityMap != null)
			{
				incidentId = activityMap.get("incidentId");
				
				Object obj = activityMap.get(Constants.PB_DESCRIPTION);
				String desc = activityMap.get(Constants.PB_DESCRIPTION);
				
				if (obj instanceof Map )
				{
					desc = new ObjectMapper().writeValueAsString(obj);
				}
				activityMap.put(Constants.PB_DESCRIPTION, desc);
				
				activityJson = new ObjectMapper().writeValueAsString(activityMap);
				
				List<ActivityVO> activityVOList = PlaybookUtils.addActivity(incidentId, activityJson, username);
				result.setSuccess(true).setRecords(activityVOList);
				
				String auditMessage = String.format("User: %s added a new custom activity named: %s", username, activityMap.get("activityName"));
				PlaybookUtils.auditAction(auditMessage, incidentId, request.getRemoteAddr(), username);
			}
		}
		catch(Throwable e)
		{
            result.setSuccess(false)
            .setMessage(checkAndSetErrorMessage(
            				e, String.format(ERROR_ADDING_INCIDENT_ACTIVITIES, incidentId), 
            				Arrays.asList(String.format(PlaybookUtils.USER_DOES_NOT_ERROR_MSG_PREFIX, username),
            							  String.format(PlaybookUtils.DENIED_VIEW_ACCESS_SECURITY_INCIDENTS, username)),
            				Arrays.asList(PlaybookActivityUtils.DUPLICATE_ACTIVITIES_TASKS_IN_PHASE_PROHIBITED_MSG,
            							  PlaybookUtils.RBAC_IS_DENIED_ACCESS_TO_MESSAGE_MARKER), null));
		}
		
		return result;
    }
	
	/**
	 * List all the activities associated with an incident
	 * 
	 * @param incidentId : String representing incident id
	 * @param request
	 * @return
	 * @throws ServletException
	 * @throws IOException
	 */
	@RequestMapping(value = "/playbook/activity/listByIncidentId", method = { RequestMethod.GET, RequestMethod.POST }, headers = "Accept=application/json; charset=utf-8")
    @ResponseBody
    public ResponseDTO<ActivityVO> listActivitiesByIncidentId(@RequestParam String incidentId, HttpServletRequest request) throws ServletException, IOException
    {
		ResponseDTO<ActivityVO> result = new ResponseDTO<ActivityVO>();
		String username = (String) request.getAttribute(Constants.HTTP_REQUEST_USERNAME);
		try
		{
			List<ActivityVO> activityVOList = PlaybookUtils.listActivitiesByIncidentId(incidentId,  username);
			result.setSuccess(true).setRecords(activityVOList);
		}
		catch(Exception e)
		{
		    boolean isError = !e.getMessage().startsWith(String.format(PlaybookUtils.USER_DOES_NOT_ERROR_MSG_PREFIX, username));
            
            String msg = String.format((isError ? ERROR_LISTING_INCIDENT_ACTIVITIES : 
                                                  DENIED_ACCESS_TO_SIR_ORG_OR_NO_PERMISSION_FOR_ACTION_ON_SIR), 
                                       incidentId);
            
            if (isError)
            {
                Log.log.error(msg + " " + e.getLocalizedMessage() , e);
            }
            else
            {
                Log.log.warn(msg);
            }
            
            result.setSuccess(false).setMessage(msg);
		}
		
		return result;
    }
	
	/**
     * Get count report of Security Investigattion Response (SIR) records in Open, In Progress, Closed, Complete, 
     * Past Due, and, SLA at Risk state within specified scope.
     * 
     * Scope is specified as # of units days/hours/minutes (default days) 
     * to go back in history from current datetime.
     * 
     * SIRs having dueBy date set within scope will only be considered for count
     * for Past Due and SLA at Risk.
     * Past Due and SLA at Risk are based off of create date of SIR record.
     * SIR is considered as Past Due when current time is after dueBy datetime.
     * SIR's Service Level Agreement (SLA) is considered as at Risk when current time is 
     * after # SLA at Risk factors units before dueBy datetime.  
     * 
     * @param scopeUnits         # of days to go back in history from current time (max 365 days)
     * @param slaAtRiskUnits     # of days to before dueBy datetime when SIR's SLA is considered 
     *                          as at Risk
     * @param unitType           Chronological type (DAYS/HOURS/MINUTES) of units
     *                        
     * @param request
     * @return
     * @throws ServletException
     * @throws IOException
     */
	@RequestMapping(value = "/playbook/getsircountreport", method = { RequestMethod.POST, RequestMethod.GET })
    @ResponseBody
    public ResponseDTO<Map<String, Object>> getSIRCountReport(
                    @RequestParam Integer scopeUnits,
                    @RequestParam Integer slaAtRiskUnits,
                    @RequestParam(value = "unitType", required = false) String unitType,
                    @RequestParam String orgId,
                    HttpServletRequest request) throws ServletException, IOException
    {
	    String userUnitType = unitType;
	    ResponseDTO<Map<String, Object>> result = new ResponseDTO<Map<String, Object>>();
	    
	    if (StringUtils.isBlank(userUnitType))
	    {
	        userUnitType = ChronoUnit.DAYS.name();
	    }
	    
	    TemporalUnit tmprlUnit = ChronoUnit.DAYS;
	    
	    switch (userUnitType.toUpperCase())
	    {
	        case "DAYS":
	            tmprlUnit = ChronoUnit.DAYS;
	            break;
	            
	        case "HOURS":
	            tmprlUnit = ChronoUnit.HOURS;
                break;
                
	        case "MINUTES":
                tmprlUnit = ChronoUnit.MINUTES;
                break;
                
            default:
                tmprlUnit = ChronoUnit.DAYS;
                break;                
	    }
	    
	    try
        {
	        String username = (String) request.getAttribute(Constants.HTTP_REQUEST_USERNAME); 
	        result = PlaybookUtils.getSIRCountReport(username, scopeUnits, slaAtRiskUnits, tmprlUnit, orgId);
	       
	        try
	        {
	            ResponseDTO<Map<String, Object>> activityCountResult = PlaybookUtils
	            		.getSIRActivityCountByStatusReport(orgId, username, ((Main) MainBase.main).getConfigSQL());
	            
	            // Add activity count result to SIR count result
	            
	            if (activityCountResult != null && activityCountResult.getRecords() != null &&
	                !activityCountResult.getRecords().isEmpty())
	            {
	                Map<String, Object> sirCountMap = result.getData();
	                
	                if (sirCountMap == null)
	                {
	                    sirCountMap = new HashMap<String, Object>();
	                }
	                
	                for (Map<String, Object> activityCountData : activityCountResult.getRecords())
	                {
	                    for (String activityStatus : activityCountData.keySet())
	                    {
	                        result.getData().put(PlaybookUtils.ACTIVITY_STATUS_FIELD_PREFIX + activityStatus, 
	                                             activityCountData.get(activityStatus));
	                    }
	                }
	                
	                result.setTotal(result.getData().size());
	            }
	        }
	        catch (Exception e)
	        {
	            Log.log.warn(ERROR_GETTING_SIR_ACTIVITY_COUNTS_BY_STATUS_REPORT, e);
	            
	            //result.setSuccess(false).setMessage(ERROR_GETTING_SIR_COUNTS_REPORT);
	        }
        }
	    catch (Exception e)
        {
	    	boolean isError = StringUtils.isNotBlank(e.getMessage()) && 
	    					  !e.getMessage().startsWith(USER_DOES_NOT_HAVE_PREMISSION_TO_PREFIX);

			String msg = isError ? ERROR_GETTING_SIR_COUNTS_REPORT : "";
			
			if (isError)
			{
			    Log.log.error(msg + " " + e.getLocalizedMessage() , e);
			}
			else
			{
			    Log.log.warn(e.getLocalizedMessage());
			}
			
			result.setSuccess(false).setMessage(ERROR_GETTING_SIR_COUNTS_REPORT + 
												(isError ? "" : " " + e.getLocalizedMessage()));
        }
        
        return result;
    }

    /**
     * Get count report of SIR records over time within specified scope (how many Investigations have been created over time)
     * 
     * Scope is specified as # of units days/hours/minutes (default days) 
     * to go back in history from current datetime.
     * 
     * @param numberOfCols       Evenly divided buckets over scope, i.e. 10 1-day buckets on 10 days.
     * @param scopeUnits         # of days to go back in history from current time (max 365 days)
     * @param unitType           Chronological type (DAYS/HOURS/MINUTES) of units
     * @param typeFilter         Query filters by type, i.e. All|Malware|Phishing    
     * @param severityFilter     Query filters by severity, i.e. Critical|High|Medium|Low                 
     * @param ownerFilter        Query filters by owner, i.e. admin|resolve_user              
     * @param teamFilter         Query filters by team member, i.e. admin|resolve_user              
     * @param statusFilter       Query filters by status, i.e. Open|In Progress|Closed                                 
     * @param request
     * @return
     * @throws ServletException
     * @throws IOException
     */
    @RequestMapping(value = "/playbook/getsircountovertimes", method = { RequestMethod.POST, RequestMethod.GET })
    @ResponseBody
    public ResponseDTO<Map<Object, Object>> getSIRCountOverTimeReport(
                    @RequestParam Integer numberOfCols,
                    @RequestParam Integer scopeUnits,
                    @RequestParam String unitType,
                    @RequestParam String typeFilter,
                    @RequestParam String severityFilter,
                    @RequestParam String ownerFilter,
                    @RequestParam String teamFilter,
                    @RequestParam String priorityFilter,
                    @RequestParam String statusFilter,
                    @RequestParam String orgId,
                    HttpServletRequest request) throws ServletException, IOException
    {
        ResponseDTO<Map<Object, Object>> result = new ResponseDTO<Map<Object, Object>>();
       
        try
        {
            String username = (String) request.getAttribute(Constants.HTTP_REQUEST_USERNAME);
			result = PlaybookUtils.getSIRCountOverTimeReport(numberOfCols, scopeUnits, unitType, typeFilter,
					severityFilter, ownerFilter, teamFilter, priorityFilter, statusFilter, orgId, username,
					((Main) MainBase.main).getConfigSQL());
        }
        catch (Exception e)
        {
            Log.log.error(ERROR_GETTING_SIR_COUNTS_OVER_TIME_REPORT, e);
            result.setSuccess(false).setMessage(ERROR_GETTING_SIR_COUNTS_OVER_TIME_REPORT);
        }

        return result;
    }
	
	/**
     * Get count report of SIR records by staus within specified scope (how many Investigations are in each Status)
     * 
     * Scope is specified as # of units days/hours/minutes (default days) 
     * to go back in history from current datetime.
     * 
     * @param scopeUnits         # of days to go back in history from current time (max 365 days)
     * @param unitType           Chronological type (DAYS/HOURS/MINUTES) of units
     * @param typeFilter         Query filters by type, i.e. All|Malware|Phishing    
     * @param severityFilter     Query filters by severity, i.e. Critical|High|Medium|Low                 
     * @param ownerFilter        Query filters by owner, i.e. admin|resolve_user              
     * @param teamFilter         Query filters by team member, i.e. admin|resolve_user              
     * @param statusFilter       Query filters by status, i.e. Open|In Progress|Closed    
     *                        
     * @param request
     * @return
     * @throws ServletException
     * @throws IOException
     */
    @RequestMapping(value = "/playbook/getsircountbystatusreport", method = { RequestMethod.POST, RequestMethod.GET })
    @ResponseBody
    public ResponseDTO<Map<String, Object>> getSIRCountByStatusReport(
                    @RequestParam Integer scopeUnits,
                    @RequestParam String unitType,
                    @RequestParam String typeFilter,
                    @RequestParam String severityFilter,
                    @RequestParam String ownerFilter,
                    @RequestParam String teamFilter,
                    @RequestParam String priorityFilter,
                    @RequestParam String statusFilter,
                    @RequestParam String orgId,
                    HttpServletRequest request) throws ServletException, IOException
    {
        return getSIRCountByTypeReport("u_status", scopeUnits, unitType, typeFilter, severityFilter, ownerFilter, teamFilter, priorityFilter, statusFilter, orgId, request);
    }

	/**
     * Get count report of SIR records by team member within specified scope (how many tickets
     * belong to which users that are members of the team)
     * 
     * Scope is specified as # of units days/hours/minutes (default days) 
     * to go back in history from current datetime.
     * 
     * @param scopeUnits         # of days to go back in history from current time (max 365 days)
     * @param unitType           Chronological type (DAYS/HOURS/MINUTES) of units
     * @param typeFilter         Query filters by type, i.e. All|Malware|Phishing    
     * @param severityFilter     Query filters by severity, i.e. Critical|High|Medium|Low                 
     * @param ownerFilter        Query filters by owner, i.e. admin|resolve_user              
     * @param teamFilter         Query filters by team member, i.e. admin|resolve_user              
     * @param statusFilter       Query filters by status, i.e. Open|In Progress|Closed    
     *                        
     * @param request
     * @return
     * @throws ServletException
     * @throws IOException
     */
    @RequestMapping(value = "/playbook/getsircountbyteammember", method = { RequestMethod.POST, RequestMethod.GET })
    @ResponseBody
    public ResponseDTO<Map<String, Object>> getSIRCountByTeamMemberReport(
                    @RequestParam Integer scopeUnits,
                    @RequestParam String unitType,
                    @RequestParam String typeFilter,
                    @RequestParam String severityFilter,
                    @RequestParam String ownerFilter,
                    @RequestParam String teamFilter,
                    @RequestParam String priorityFilter,
                    @RequestParam String statusFilter,
                    @RequestParam String orgId,
                    HttpServletRequest request) throws ServletException, IOException
    {
        ResponseDTO<Map<String, Object>> result = getSIRCountByTypeReport("u_members", scopeUnits, unitType, typeFilter, severityFilter, ownerFilter, teamFilter, priorityFilter, statusFilter, orgId, request);
        
        if (result != null && result.isSuccess()) { 
            if (result.getRecords() != null && !result.getRecords().isEmpty())
            {
                HashMap<String, Object> newMap = new HashMap<String, Object>();
                List<Map<String, Object>> records = result.getRecords();
                Iterator<Map<String, Object>> iter = records.listIterator();
                String dbType = ((Main) MainBase.main).getConfigSQL().getDbtype().toUpperCase();
        
                List<String> memberFilters = Arrays.asList(teamFilter.split(","));
                for (String member: memberFilters) {
                    memberFilters.set(memberFilters.indexOf(member), member.trim());
                }
                while(iter.hasNext()) {
                    Map<String, Object> curElem = iter.next();
                    String k = (String)curElem.keySet().toArray()[0];
                    String [] users = k.split(",");
                    for (String user: users) {
                        user = user.trim();
                        if (!user.isEmpty() && (teamFilter.trim().equals("All") || memberFilters.contains(user))) {
                            if (dbType.equals("MYSQL")) {
                                newMap.put(user, (newMap.get(user) == null)? (curElem.get(k)):
                                    BigInteger.valueOf((((BigInteger)curElem.get(k)).longValue()+((BigInteger)newMap.get(user)).longValue())));
                            } else {
                                newMap.put(user, (newMap.get(user) == null)? (curElem.get(k)):
                                    BigDecimal.valueOf((((BigDecimal)curElem.get(k)).longValue()+((BigDecimal)newMap.get(user)).longValue())));
                            }
                        }
                    }
                }
                List<Map<String, Object>> newRecords = new ArrayList<Map<String, Object>> ();
                newRecords.add(newMap);
                result.setRecords(newRecords);
            }
        }
        else
        {
            String msg = String.format(ERROR_GETTING_SIR_COUNTS_BY_REPORT, "members");
            Log.log.error(msg);
            result.setSuccess(false).setMessage("Error:" + msg);
        }
        
        return result;
    }
    
	/**
     * Get count report of Security Investigattion Response (SIR) records by 
     * investigation type like Phishing, Malware etc within specified scope.
     * 
     * Scope is specified as # of units days/hours/minutes (default days) 
     * to go back in history from current datetime.
     * 
     * @param scopeUnits         # of days to go back in history from current time (max 365 days)
     * @param unitType           Chronological type (DAYS/HOURS/MINUTES) of units
     *                        
     * @param request
     * @return
     * @throws ServletException
     * @throws IOException
     */
    @RequestMapping(value = "/playbook/getsircountbytypereport", method = { RequestMethod.POST, RequestMethod.GET })
    @ResponseBody
    public ResponseDTO<Map<String, Object>> getSIRCountByTypeReport(@RequestParam(required = false) String dataSource,
                                                                    @RequestParam Integer scopeUnits, @RequestParam String unitType, 
                                                                    @RequestParam String typeFilter, @RequestParam String severityFilter, 
                                                                    @RequestParam String ownerFilter, @RequestParam String teamFilter, 
                                                                    @RequestParam String priorityFilter, @RequestParam String statusFilter,
                                                                    @RequestParam String orgId,
                                                                    HttpServletRequest request) throws ServletException, IOException
    {
        String userUnitType = unitType;
        String dataSrc = dataSource != null? dataSource: "u_investigation_type";
        ResponseDTO<Map<String, Object>> result = new ResponseDTO<Map<String, Object>>();
        
        if (StringUtils.isBlank(userUnitType))
        {
            userUnitType = ChronoUnit.DAYS.name();
        }
        
        TemporalUnit tmprlUnit = ChronoUnit.DAYS;
        
        switch (userUnitType.toUpperCase())
        {
            case "DAYS":
                tmprlUnit = ChronoUnit.DAYS;
                break;
                
            case "HOURS":
                tmprlUnit = ChronoUnit.HOURS;
                break;
                
            case "MINUTES":
                tmprlUnit = ChronoUnit.MINUTES;
                break;
                
            default:
                tmprlUnit = ChronoUnit.DAYS;
                break;                
        }
        
        try
        {
            String username = (String) request.getAttribute(Constants.HTTP_REQUEST_USERNAME);
			result = PlaybookUtils.getSIRCountByColumnReport(dataSrc, scopeUnits, tmprlUnit, typeFilter, severityFilter,
					ownerFilter, teamFilter, priorityFilter, statusFilter, orgId, username,
					((Main) MainBase.main).getConfigSQL());
        }
        catch (Exception e)
        {
        	String msg = String.format(ERROR_GETTING_SIR_COUNTS_BY_REPORT, dataSrc);
            Log.log.error(msg, e);
            result.setSuccess(false).setMessage("Error:" + e.getMessage());
        }
        
        return result;
    }
    
    /**
     * Get count report of Security Investigattion Response (SIR) records by 
     * type of severity i.e. Critical, High, Medium and, Low within specified scope.
     * 
     * Scope is specified as # of units days/hours/minutes (default days) 
     * to go back in history from current datetime.
     * 
     * @param scopeUnits         # of days to go back in history from current time (max 365 days)
     * @param unitType           Chronological type (DAYS/HOURS/MINUTES) of units
     *                        
     * @param request
     * @return
     * @throws ServletException
     * @throws IOException
     */
    @RequestMapping(value = "/playbook/getsircountbyseverityreport", method = { RequestMethod.POST, RequestMethod.GET })
    @ResponseBody
    public ResponseDTO<Map<String, Object>> getSIRCountBySeverityReport(@RequestParam Integer scopeUnits, @RequestParam String unitType, 
                                                                        @RequestParam String typeFilter, @RequestParam String severityFilter, 
                                                                        @RequestParam String ownerFilter, @RequestParam String teamFilter, 
                                                                        @RequestParam String orgId, @RequestParam String priorityFilter,
                                                                        @RequestParam String statusFilter, HttpServletRequest request) throws ServletException, IOException
    {
        String userUnitType = unitType;
        ResponseDTO<Map<String, Object>> result = new ResponseDTO<Map<String, Object>>();
        
        if (StringUtils.isBlank(userUnitType))
        {
            userUnitType = ChronoUnit.DAYS.name();
        }
        
        TemporalUnit tmprlUnit = ChronoUnit.DAYS;
        
        switch (userUnitType.toUpperCase())
        {
            case "DAYS":
                tmprlUnit = ChronoUnit.DAYS;
                break;
                
            case "HOURS":
                tmprlUnit = ChronoUnit.HOURS;
                break;
                
            case "MINUTES":
                tmprlUnit = ChronoUnit.MINUTES;
                break;
                
            default:
                tmprlUnit = ChronoUnit.DAYS;
                break;                
        }
        
        String dbType = ((Main) MainBase.main).getConfigSQL().getDbtype().toUpperCase();
        
        try
        {
            String username = (String) request.getAttribute(Constants.HTTP_REQUEST_USERNAME);
            result = PlaybookUtils.getSIRCountBySeverityReport(scopeUnits, tmprlUnit, dbType, typeFilter, 
                                                               severityFilter, ownerFilter, teamFilter, statusFilter, 
                                                               priorityFilter, orgId, username);
        }
        catch (Exception e)
        {
            Log.log.error(ERROR_GETTING_SIR_COUNTS_BY_SEVERITY_REPORT, e);
            result.setSuccess(false).setMessage(ERROR_GETTING_SIR_COUNTS_BY_SEVERITY_REPORT);
        }
        
        return result;
    }
    
    private List<Map<String, Object>> parseAgeBuckets(String ageBuckets)
    {
        List<Map<String, Object>> bucketsInfo = new ArrayList<Map<String, Object>>();
        
        String[] buckets = ageBuckets.split(",");
        
        if (buckets.length > 0)
        {
            int prevAgeBucketStart = 0;
            String lastAgeBucketId = "";
            
            for (int i = 0; i < buckets.length; i++)
            {
                int ageBucketEnd = prevAgeBucketStart;
                
                if ( i == 0 )
                {
                    String[] firstEntry = buckets[i].trim().split("-");
                    
                    if (firstEntry != null && firstEntry.length == 2)
                    {
                        String start = firstEntry[0].trim();
                        String end = firstEntry[1].trim();
                        
                        String[] startEntry = start.split(" ");
                        
                        if (startEntry != null && startEntry.length == 2)
                        {
                            prevAgeBucketStart = extractBucketBoundary(startEntry);
                        }
                        
                        String[] endEntry = end.split(" ");
                        
                        if (endEntry != null && endEntry.length == 2)
                        {
                            ageBucketEnd = extractBucketBoundary(endEntry);
                        }
                    }
                }
                else
                {
                    String[] endEntry = buckets[i].trim().split(" ");
                    
                    if (endEntry != null && endEntry.length == 2)
                    {
                        ageBucketEnd = extractBucketBoundary(endEntry);
                    }
                }
                
                if (ageBucketEnd <= prevAgeBucketStart)
                {
                    ageBucketEnd = prevAgeBucketStart;
                }
                
                int maximumEnd = 2880 * 60;
                if (ageBucketEnd > maximumEnd) {
                    ageBucketEnd = maximumEnd;
                }

                Map<String, Object> bucketInfo = new HashMap<String, Object>();
                bucketsInfo.add(bucketInfo);
                
                bucketInfo.put("AGE_BUCKET_ID", buckets[i].trim());
                bucketInfo.put("AGE_BUCKET_START", new BigDecimal(prevAgeBucketStart));
                bucketInfo.put("AGE_BUCKET_END", new BigDecimal(ageBucketEnd));
                
                prevAgeBucketStart = ageBucketEnd;
                lastAgeBucketId = buckets[i].trim();
            }
            
            Map<String, Object> bucketInfo = new HashMap<String, Object>();
            bucketsInfo.add(bucketInfo);
            
            bucketInfo.put("AGE_BUCKET_ID", "> " + lastAgeBucketId);
            bucketInfo.put("AGE_BUCKET_START", new BigDecimal(prevAgeBucketStart));
            bucketInfo.put("AGE_BUCKET_END", new BigDecimal(Integer.MAX_VALUE));
        }
        
        return bucketsInfo;
    }
    
    private int extractBucketBoundary(String[] startEntry)
    {
        int prevAgeBucketStartHrs = 0;
        
        int startVal = Integer.parseInt(startEntry[0].trim());
        
        if (startEntry[1].trim().equalsIgnoreCase("minutes"))
        {
            prevAgeBucketStartHrs = startVal;
        }
        else if (startEntry[1].trim().equalsIgnoreCase("hours"))
        {
            prevAgeBucketStartHrs = startVal * 60;
        }
        else if (startEntry[1].trim().equalsIgnoreCase("days"))
        {
            prevAgeBucketStartHrs = startVal * 24 * 60;
        }
        else if (startEntry[1].trim().equalsIgnoreCase("weeks"))
        {
            prevAgeBucketStartHrs = startVal * 7 * 24 * 60;
        }
        
        return prevAgeBucketStartHrs;
    }

    /**
     * Returns data for MTTA report. 
     * 
     * http://devjira.resolvesys.com/browse/SIR-461
     * 
     * 
     * @param visualizedFilter - determines which of the passed filters will be used for grouping/visualizing.
     * @return
     * @throws ServletException
     * @throws IOException
     */
    @RequestMapping(value = "/playbook/getmttareport", method = { RequestMethod.POST, RequestMethod.GET })
    @ResponseBody
    public ResponseDTO<Map<String,Object>> getMTTAReport(@RequestParam String typeFilter, 
                                                       @RequestParam String severityFilter, 
                                                       @RequestParam String ownerFilter,
                                                       @RequestParam String statusFilter,
                                                       @RequestParam String priorityFilter,
                                                       @RequestParam String ageBuckets,  
                                                       @RequestParam String orgId,
                                                       @RequestParam String visualizedFilter,
                                                       HttpServletRequest request) throws ServletException, IOException
    {
        String username = (String) request.getAttribute(Constants.HTTP_REQUEST_USERNAME);
        MeanTimeFilterContainer filterContainer = new MeanTimeFilterContainer(username, typeFilter, severityFilter, ownerFilter, statusFilter, priorityFilter, visualizedFilter, orgId);
        return getMeanTimeReportByType(SIRMeanTimeReportType.MTTA, filterContainer, ageBuckets);
    }
    
    /**
     * Returns data for MTTR report.   
     * 
     * 
     * http://devjira.resolvesys.com/browse/SIR-459
     * 
     * 
     * @param visualizedFilter - determines which of the passed filters will be used for grouping/visualizing.
     * @return
     * @throws ServletException
     * @throws IOException
     */
    @RequestMapping(value = "/playbook/getmttrreport", method = { RequestMethod.POST, RequestMethod.GET })
    @ResponseBody
    public ResponseDTO<Map<String, Object>> getMTTRReport(@RequestParam String typeFilter, 
                                                       @RequestParam String severityFilter, 
                                                       @RequestParam String ownerFilter,
                                                       @RequestParam String priorityFilter,
                                                       @RequestParam String ageBuckets, 
                                                       @RequestParam String orgId,
                                                       @RequestParam String visualizedFilter,
                                                       HttpServletRequest request) throws ServletException, IOException
    {
        String mttrStatusFilter = "Closed";
        String username = (String) request.getAttribute(Constants.HTTP_REQUEST_USERNAME);
        MeanTimeFilterContainer filterContainer = new MeanTimeFilterContainer(username, typeFilter, severityFilter, ownerFilter, mttrStatusFilter, priorityFilter, visualizedFilter, orgId);
        return getMeanTimeReportByType(SIRMeanTimeReportType.MTTR, filterContainer, ageBuckets);
    }
    
    private ResponseDTO<Map<String, Object>> getMeanTimeReportByType(SIRMeanTimeReportType reportType,
                    MeanTimeFilterContainer filterContainer,
                     String ageBuckets){
        ResponseDTO<Map<BigDecimal, Map<String, BigDecimal>>> result = new ResponseDTO<Map<BigDecimal, Map<String, BigDecimal>>>();
        
        Integer lastBucketLowerBoundary = 0;
        List<Map<String, Object>> bucketsInfo = parseAgeBuckets(ageBuckets);
        
        if (bucketsInfo != null && !bucketsInfo.isEmpty())
        {
            for (Map<String, Object> bucketInfo : bucketsInfo)
            {
                if (((String)bucketInfo.get("AGE_BUCKET_ID" )).startsWith("> "))
                {
                    lastBucketLowerBoundary = new Integer(((BigDecimal)bucketInfo.get("AGE_BUCKET_START")).intValue());
                }
            }
        }
        
        ResponseDTO<Map<String, Object>> finalResult = new ResponseDTO<Map<String, Object>>();
        try
        {
            result = PlaybookUtils.getMeanTimeSIRReport(reportType, lastBucketLowerBoundary, filterContainer, 
            		((Main) MainBase.main).getConfigSQL());
            finalResult = prepareMeanTimeReportResult(result, bucketsInfo, filterContainer);
        }
        catch (Exception e)
        {
            Log.log.error(ERROR_GETTING_SIR_MEAN_TIME_REPORT, e);
            finalResult.setSuccess(false).setMessage(ERROR_GETTING_SIR_MEAN_TIME_REPORT);
        }
        
        return finalResult;
        
    }

    @SuppressWarnings("unchecked")
	private ResponseDTO<Map<String, Object>> prepareMeanTimeReportResult(ResponseDTO<Map<BigDecimal, Map<String, BigDecimal>>> result,
                    List<Map<String, Object>> bucketsInfo,
                    MeanTimeFilterContainer filterContainer
                    ) throws Exception
    {
        ResponseDTO<Map<String, Object>> finalResult = new ResponseDTO<Map<String, Object>>();
        
        if (result.isSuccess() && result.getTotal() > 0 && result.getRecords() != null &&
            !result.getRecords().isEmpty())
        {
            Map<BigDecimal, Map<String, BigDecimal>> agingRecs = result.getRecords().get(0);
            calculateAvgValuesPerBucket(bucketsInfo, agingRecs);
        }
        
        List<Map<String, Object>> finalRecords = new ArrayList<Map<String, Object>>();
        
        for (Map<String, Object> bucketInfo : bucketsInfo)
        {               
            Map<String, Object> finalRecord = new HashMap<String, Object>();
            
            Object valObj = null;
            
            if (bucketInfo.containsKey("AGE_BUCKET_VALUE"))
            {
                valObj = bucketInfo.get("AGE_BUCKET_VALUE");
            }
            else
            {
                Map<String, BigDecimal> emptyBucket = new HashMap<>();
                valObj = emptyBucket;
            }
            
            finalRecord.put((String)bucketInfo.get("AGE_BUCKET_ID"), valObj);
            finalRecords.add(finalRecord);                
        }
        
        for(Map<String, Object> bucketData : finalRecords) {
            for(String key : bucketData.keySet()) {
                List<SIRMeanTimeReportResult> valueList = new ArrayList<>();
                Map<String, BigDecimal> valueMap = (Map<String, BigDecimal>) bucketData.get(key);
                for(Map.Entry<String,BigDecimal> value : valueMap.entrySet()) {
                    valueList.add(new SIRMeanTimeReportResult(value.getKey(), value.getValue()));
                }
                bucketData.put(key, valueList);
            }
        }
        
        updateBucketsWithMissingData(finalRecords, filterContainer);
        
        finalResult.setRecords(finalRecords);
        finalResult.setTotal(finalRecords.size());
        
        finalResult.setSuccess(true);
        
        return finalResult;
    }
    
    @SuppressWarnings("unchecked")
	private void updateBucketsWithMissingData(List<Map<String, Object>> finalRecords, MeanTimeFilterContainer filterContainer) throws Exception
    {
        List<String> bucketKeys = PlaybookUtils.getMTBucketKeys(filterContainer);

        for (Map<String, Object> bucketData : finalRecords){
            for (Entry<String, Object> data : bucketData.entrySet()){
                List<SIRMeanTimeReportResult> columnsData = (List<SIRMeanTimeReportResult>) data.getValue();
                
                if(columnsData.isEmpty()) {
                    data.setValue(null);
                    continue;
                }
                
                boolean shouldAddMissingDataPlaceholder = true;
                for(String key : bucketKeys) {
                    for(SIRMeanTimeReportResult columnData : columnsData) {
                        if(key.equals(columnData.getKey())) {
                            shouldAddMissingDataPlaceholder = false;
                        }
                    }
                    
                    if(shouldAddMissingDataPlaceholder) {
                        columnsData.add(new SIRMeanTimeReportResult(key, null));
                    }
                }
            }
        }
    }

    @SuppressWarnings("unchecked")
	private void calculateAvgValuesPerBucket(List<Map<String, Object>> bucketsInfo, Map<BigDecimal, Map<String, BigDecimal>> agingRecs)
    {
        for (Map<String, Object> bucketInfo : bucketsInfo)
        {
            Map<String, Integer> countByKey = new HashMap<>();
            for (BigDecimal ageInMinutes : agingRecs.keySet())
            {
                if (ageInMinutes.longValue() >= ((BigDecimal)bucketInfo.get("AGE_BUCKET_START")).longValue() && 
                    ageInMinutes.longValue() < ((BigDecimal)bucketInfo.get("AGE_BUCKET_END")).longValue())
                {
                    if(bucketInfo.containsKey("AGE_BUCKET_VALUE")) {
                        Map<String, BigDecimal> bucketsValue = (Map<String, BigDecimal>) bucketInfo.get("AGE_BUCKET_VALUE");
                        Map<String, BigDecimal> valuesToAdd = agingRecs.get(ageInMinutes);
                        
                        for(String key : valuesToAdd.keySet()) {
                            BigDecimal bucketValue = bucketsValue.get(key);
                            if(bucketValue != null) {
                                bucketValue = bucketValue.add(valuesToAdd.get(key));
                            } else {
                                bucketValue = valuesToAdd.get(key);
                            }
                            
                            if(countByKey.containsKey(key)) {
                                countByKey.put(key, countByKey.get(key) + 1);
                            } else {
                                countByKey.put(key, 1);
                            }
                            
                            bucketsValue.put(key, bucketValue);
                        }
                        
                        bucketInfo.put("AGE_BUCKET_VALUE", bucketsValue);
                    } else {
                        for(String key : agingRecs.get(ageInMinutes).keySet()) {
                            countByKey.put(key, 1);
                        }
                        bucketInfo.put("AGE_BUCKET_VALUE", agingRecs.get(ageInMinutes));
                    }
                }
            }
            
            Map<String, BigDecimal> bucketsValue = (Map<String, BigDecimal>) bucketInfo.get("AGE_BUCKET_VALUE");
            if(bucketsValue != null) {
                for(String key : bucketsValue.keySet()) {
                    BigDecimal timesAdded = new BigDecimal(countByKey.get(key));
                    if (bucketsValue.get(key) != null)
                        bucketsValue.put(key, bucketsValue.get(key).divide(timesAdded, 2, RoundingMode.HALF_UP));
                }
                bucketInfo.put("AGE_BUCKET_VALUE", bucketsValue);
            }
        }
    }

    /**
     * Get count report of Security Investigattion Response (SIR) records by 
     * age.
     * 
     * Age of SIR is defined as period between its creation and closing.
     * 
     * The returned result includes aggregated count of SIRs by age for every 
     * threshold unit period (day/hour/minute) from 0 to maxThresholdUnits (both inclusive)
     * and last recod with thresholdUnits + 1 indicates count of SIRs open for
     * more than threholdUnits period.
     * 
     * The last SIR not closed (age) for more than threshold days count is from the
     * beginning.
     * 
     * @param thresholdUnits    Max period (0 to thresholdUnits) to get count of closed SIRs
     * @param unitType          Chronological type (DAYS/HOURS/MINUTES) of units
     *                        
     * @param request
     * @return
     * @throws ServletException
     * @throws IOException
     */
    @RequestMapping(value = "/playbook/getsircountbyagereport", method = { RequestMethod.POST, RequestMethod.GET })
    @ResponseBody
    public ResponseDTO<Map<String, Object>> getSIRCountByAgeReport(@RequestParam String typeFilter, @RequestParam String severityFilter, 
                                                                   @RequestParam String ownerFilter, @RequestParam String teamFilter, 
                                                                   @RequestParam String statusFilter, @RequestParam String ageBuckets, 
                                                                   @RequestParam String priorityFilter, @RequestParam String orgId,
                                                                   HttpServletRequest request) throws ServletException, IOException
    {
        ResponseDTO<Map<BigDecimal, Object>> result = new ResponseDTO<Map<BigDecimal, Object>>();
        ResponseDTO<Map<String, Object>> finalResult = new ResponseDTO<Map<String, Object>>();
        
        Integer thresholdUnits = 0;
        
        String dbType = ((Main) MainBase.main).getConfigSQL().getDbtype().toUpperCase();
        
        List<Map<String, Object>> bucketsInfo = parseAgeBuckets(ageBuckets);
        
        if (bucketsInfo != null && !bucketsInfo.isEmpty())
        {
            for (Map<String, Object> bucketInfo : bucketsInfo)
            {
                if (((String)bucketInfo.get("AGE_BUCKET_ID" )).startsWith("> "))
                {
                    thresholdUnits = new Integer(((BigDecimal)bucketInfo.get("AGE_BUCKET_START")).intValue());
                }
            }
        }
        
        try
        {
            String username = (String) request.getAttribute(Constants.HTTP_REQUEST_USERNAME);
            result = PlaybookUtils.getSIRCountByAgeReport(thresholdUnits, dbType, typeFilter, 
                                                          severityFilter, ownerFilter, teamFilter, statusFilter, 
                                                          priorityFilter, orgId, username);
            
            if (result.isSuccess() && result.getTotal() > 0 && result.getRecords() != null &&
                !result.getRecords().isEmpty())
            {
                List<Map<BigDecimal, Object>> agingRecs = result.getRecords();
                
                for (Map<BigDecimal, Object> agingRec : agingRecs)
                {
                    BigDecimal ageInHrs = agingRec.keySet().iterator().next();
                    
                    for (Map<String, Object> bucketInfo : bucketsInfo)
                    {
                        if (ageInHrs.longValue() >= ((BigDecimal)bucketInfo.get("AGE_BUCKET_START")).longValue() && 
                            ageInHrs.longValue() < ((BigDecimal)bucketInfo.get("AGE_BUCKET_END")).longValue())
                        {
                            if (dbType.equals("MYSQL"))
                            {
                                BigInteger count = (BigInteger)bucketInfo.get("AGE_BUCKET_COUNT");
                                
                                if (count == null)
                                {
                                    bucketInfo.put("AGE_BUCKET_COUNT", new BigInteger("0"));
                                    count = (BigInteger)bucketInfo.get("AGE_BUCKET_COUNT");
                                }
                                
                                bucketInfo.put("AGE_BUCKET_COUNT", count.add((BigInteger)agingRec.get(ageInHrs)));
                            }
                            else
                            {
                                BigDecimal count = (BigDecimal)bucketInfo.get("AGE_BUCKET_COUNT");
                                
                                if (count == null)
                                {
                                    bucketInfo.put("AGE_BUCKET_COUNT", new BigDecimal("0"));
                                    count = (BigDecimal)bucketInfo.get("AGE_BUCKET_COUNT");
                                }
                                
                                bucketInfo.put("AGE_BUCKET_COUNT", count.add((BigDecimal)agingRec.get(ageInHrs)));
                            }
                            
                            break;
                        }
                    }
                }
            }
            
            List<Map<String, Object>> finalRecords = new ArrayList<Map<String, Object>>();
            
            for (Map<String, Object> bucketInfo : bucketsInfo)
            {               
                Map<String, Object> finalRecord = new HashMap<String, Object>();
                
                Object valObj = null;
                
                if (bucketInfo.containsKey("AGE_BUCKET_COUNT"))
                {
                    valObj = bucketInfo.get("AGE_BUCKET_COUNT");
                }
                else
                {
                    if (dbType.equals("MYSQL"))
                    {
                        valObj = new BigInteger("0");
                    }
                    else
                    {
                        valObj = new BigDecimal("0");
                    }
                }
                
                finalRecord.put((String)bucketInfo.get("AGE_BUCKET_ID"), valObj);
                
                finalRecords.add(finalRecord);                
            }
            
            finalResult.setRecords(finalRecords);
            finalResult.setTotal(finalRecords.size());
            
            finalResult.setSuccess(true);
        }
        catch (Exception e)
        {
            Log.log.error(ERROR_GETTING_SIR_COUNTS_BY_SEVERITY_REPORT, e);
            finalResult.setSuccess(false).setMessage(ERROR_GETTING_SIR_COUNTS_BY_SEVERITY_REPORT);
        }
        
        return finalResult;
    }
    
    /**
     * API to get incidents which were tagged by a given artifact.
     * 
     * @param queryDTO : {@link QueryDTO} holding a filter with name (key) and value information to be searched on. It also contain pagination and sorting information
     * @param request
     * @return
     * @throws ServletException
     * @throws IOException
     */
	@RequestMapping(value = "/playbook/artifact/getIncidentsByArtifact", method = { RequestMethod.GET, RequestMethod.POST }, headers = "Accept=application/json; charset=utf-8")
	@ResponseBody
	public ResponseDTO<ResolveSecurityIncidentVO> getIncidentsByArtifact(@ModelAttribute QueryDTO queryDTO, HttpServletRequest request) throws ServletException, IOException
	{
		if (queryDTO.getFilterItems() != null) {
			for (QueryFilter filter : queryDTO.getFilterItems()) {
				filter.setValue(StringEscapeUtils.escapeJava(filter.getValue()));
				filter.setField(StringEscapeUtils.escapeJava(filter.getField()));
			}
		}
		
		ResponseDTO<ResolveSecurityIncidentVO> result = new ResponseDTO<ResolveSecurityIncidentVO>();
		String username = (String) request.getAttribute(Constants.HTTP_REQUEST_USERNAME); 
		try
		{
		    result = PlaybookUtils.getIncidentsFromArtifact(queryDTO, username);
		}
		catch(Exception e)
		{
			Log.log.error(ERROR_RETRIEVING_ARTIFACTS, e);
			result.setSuccess(false).setMessage(ERROR_RETRIEVING_ARTIFACTS);
		}
		
		return result;
	}
	
	/**
	 * API to reset DT data saved during navigation. If user clicks on Reset button, it will clean all the data generated by specified DT execution
	 * and user could make a fresh start.
	 * 
	 * @param problemId : String representing worksheet id (problemId) of which DT data needs to be cleaned.
	 * @param dtFullName : String representing decition tree full name of which data needs to be cleaned.
	 * @param request : {@link HttpServletRequest} Httpt request.
	 * @return : {@link ResponseDTO} 
	 * @throws ServletException
	 * @throws IOException
	 */
	@RequestMapping(value = "/playbook/artifact/resetDTData", method = { RequestMethod.GET, RequestMethod.POST }, headers = "Accept=application/json; charset=utf-8")
	@ResponseBody
	public ResponseDTO<String> resetDTData(@RequestParam String problemId, @RequestParam String dtFullName, HttpServletRequest request) throws ServletException, IOException
	{
		ResponseDTO<String> result = new ResponseDTO<String>();
		String username = (String) request.getAttribute(Constants.HTTP_REQUEST_USERNAME); 
		try
		{
		    PlaybookUtils.resetDTData(problemId, dtFullName, username);
		    result.setSuccess(true);
		    result.setMessage("DT data reset complate.");
		}
		catch(Exception e)
		{
		    boolean isError = !e.getMessage().startsWith(String.format(PlaybookUtils.USER_DOES_NOT_ERROR_MSG_PREFIX, username));
            
            String msg = String.format((isError ? ERROR_RESETTING_DT_DATA : 
                                                  DENIED_ACCESS_TO_SIR_WORKSHEET), 
                                       problemId);
            
            if (isError)
            {
                Log.log.error(msg + " " + e.getLocalizedMessage() , e);
            }
            else
            {
                Log.log.warn(msg);
            }
			
			result.setSuccess(false).setMessage(ERROR_RESETTING_DT_DATA);
		}
		
		return result;
	}
	
	/**
     * Query wiki notes
     * 
     * @param queryDTO : Query criteria for the search.
     * @param request
     * @return
     * @throws ServletException
     * @throws IOException
     */
    @RequestMapping(value = "/playbook/wikinote/list", method = { RequestMethod.GET, RequestMethod.POST }, headers = "Accept=application/json; charset=utf-8")
    @ResponseBody
    public ResponseDTO<PBNotes> listWikiNotes(@ModelAttribute QueryDTO queryDTO, HttpServletRequest request) throws ServletException, IOException
    {
        ResponseDTO<PBNotes> result = new ResponseDTO<PBNotes>();
        String username = (String) request.getAttribute(Constants.HTTP_REQUEST_USERNAME); 
        try
        {
			int limit = PlaybookSearchAPI.getItemsLimit(queryDTO.getLimit(), PropertiesUtil.getPropertyInt(PROPERTY_SIR_MAX_WIKINOTES));
			queryDTO.setLimit(limit);
            result = PlaybookUtils.searchPBNotes(queryDTO, username);
        }
        catch(LimitExceededException e) {
			Log.log.error(ERROR_MAXIMUM_LIMIT_EXCEEDED, e);
            result.setSuccess(false).setMessage(ERROR_MAXIMUM_LIMIT_EXCEEDED);
		}
        catch(Exception e)
        {
            Log.log.error(ERROR_RETRIEVING_INCIDENT_WIKI_NOTES, e);
            result.setSuccess(false).setMessage(ERROR_RETRIEVING_INCIDENT_WIKI_NOTES);
        }
        
        return result;
    }
    
    /**
     * Search all wiki notes for given SIR
     * 
     * @param incidentId : String representing Service Incident Request ID, SIR
     * @param request
     * @return
     * @throws ServletException
     * @throws IOException
     */
    @RequestMapping(value = "/playbook/wikinote/listBySir", method = { RequestMethod.GET, RequestMethod.POST }, headers = "Accept=application/json; charset=utf-8")
    @ResponseBody
    public ResponseDTO<PBNotes> listWikiNotesBySir(@RequestParam String incidentId, HttpServletRequest request) throws ServletException, IOException
    {
        ResponseDTO<PBNotes> result = new ResponseDTO<PBNotes>();
        String username = (String) request.getAttribute(Constants.HTTP_REQUEST_USERNAME); 
        try
        {
			int limit = PropertiesUtil.getPropertyInt(PROPERTY_SIR_MAX_WIKINOTES);
            result = PlaybookUtils.searchPBWikiNotesByIncidentId(incidentId, username, limit);
        }
        catch(LimitExceededException e) {
			Log.log.error(ERROR_MAXIMUM_LIMIT_EXCEEDED, e);
            result.setSuccess(false).setMessage(ERROR_MAXIMUM_LIMIT_EXCEEDED);
		}
        catch(Exception e)
        {
            boolean isError = !e.getMessage().startsWith(String.format(PlaybookUtils.USER_DOES_NOT_ERROR_MSG_PREFIX, username));
            
            String msg = String.format((isError ? ERROR_RETRIEVING_INCIDENT_WIKI_NOTES_BY_ID : 
                                                  DENIED_ACCESS_TO_SIR_ORG_OR_NO_PERMISSION_FOR_ACTION_ON_SIR), 
                                       incidentId);
            
            if (isError)
            {
                Log.log.error(msg + " " + e.getLocalizedMessage() , e);
            }
            else
            {
                Log.log.warn(msg);
            }
            
            result.setSuccess(false).setMessage(msg);
        }
        
        return result;
    }
    
    /**
     * Index a wiki note
     * 
     * @param jsonProperty JSON representation of PNBotes
     * @param request
     * @return
     * @throws ServletException
     * @throws IOException
     */
    @RequestMapping(value = "/playbook/wikinote/save", method = { RequestMethod.GET, RequestMethod.POST }, headers = "Accept=application/json; charset=utf-8")
    @ResponseBody
    public ResponseDTO<PBNotes> saveWikiNote(@RequestBody JSONObject jsonProperty, HttpServletRequest request) throws ServletException, IOException
    {
        ResponseDTO<PBNotes> result = new ResponseDTO<PBNotes>();
        String username = (String) request.getAttribute(Constants.HTTP_REQUEST_USERNAME);
        String incidentId = null;
        String activityId = null;
        String componentId = null;
        String activityName = null;
        
        try
        {
            String json = jsonProperty.toString();
            PBNotes note = new ObjectMapper().readValue(json, PBNotes.class);
            
            incidentId = note.getIncidentId();
            activityId = note.getActivityId();
            activityName = note.getActivityName();
            componentId = note.getComponentId();
            
            if (StringUtils.isBlank(activityId) || StringUtils.isBlank(componentId) || StringUtils.isBlank(activityName))
            {
                throw new Exception("Activity Name, ActivityId, and ComponentId are required fields to create Acivity note.");
            }

            note = PlaybookUtils.indexNote(note, username);
            
            PlaybookUtils.auditAction(note.getAuditMessage(), note.getIncidentId(), request.getRemoteAddr(), username);
            note.setAuditMessage(null);
            
            result.setSuccess(true).setData(note);
            result.setMessage("Saved saving security incident activity note for incidentId: " + incidentId + 
                              " , Activity Name: " + activityName + ", Activity Id: " + activityId + 
                              ", Component Id: " + componentId);
        }
        catch(Exception e)
        {
            boolean isError = !e.getMessage().startsWith(String.format(PlaybookUtils.USER_DOES_NOT_ERROR_MSG_PREFIX, username));
            
            String msg = null;
            
            if (isError)
            {
                msg = String.format(ERROR_SAVING_INCIDENT_ACTIVITY_NOTE, incidentId, activityName, activityId, componentId);
                Log.log.error(msg + " " + e.getLocalizedMessage() , e);
            }
            else
            {
                msg = String.format(DENIED_ACCESS_TO_SIR_ORG_OR_NO_PERMISSION_FOR_ACTION_ON_SIR, incidentId);
                Log.log.warn(msg);
            }
            
            result.setSuccess(false).setMessage(msg);
        }
        
        return result;
    }
    
    /**
     * Query wiki notes for given parameters
     * 
     * @param incidentId : String representing SIR to which the wiki notes belongs
     * @param activityId : String representing activity id
     * @param componentId : String representing component id
     * @param request
     * @return
     * @throws ServletException
     * @throws IOException
     */
    @RequestMapping(value = "/playbook/wikinote/getByParams", method = { RequestMethod.GET, RequestMethod.POST }, headers = "Accept=application/json; charset=utf-8")
    @ResponseBody
    public ResponseDTO<PBNotes> getWikiNoteByParams(@RequestParam(value = "incidentId", required = false) String incidentId, 
                                                    @RequestParam(value = "activityId", required = false) String activityId, 
                                                    @RequestParam(value = "componentId", required = false) String componentId,
                                                    HttpServletRequest request) throws ServletException, IOException
    {
        ResponseDTO<PBNotes> result = new ResponseDTO<PBNotes>();
        String username = (String) request.getAttribute(Constants.HTTP_REQUEST_USERNAME); 
        try
        {
            if (StringUtils.isNotBlank(activityId) && StringUtils.isNotBlank(componentId))
            {
                result = PlaybookUtils.searchPBWikiNoteByActivityAndComponentId(activityId, componentId, username);
            }
            else if (StringUtils.isNotBlank(incidentId) && StringUtils.isNotBlank(activityId) &&
                     StringUtils.isBlank(componentId))
            {
                result = PlaybookUtils.searchPBWikiNotesByIncidentAndActivityId(incidentId, activityId, username);
            }
            else if (StringUtils.isNotBlank(incidentId) && StringUtils.isBlank(activityId) &&
                     StringUtils.isBlank(componentId))
            {
            	int limit = PropertiesUtil.getPropertyInt(PROPERTY_SIR_MAX_WIKINOTES);
                result = PlaybookUtils.searchPBWikiNotesByIncidentId(incidentId, username, limit);
            }
            else
            {
                throw new Exception("Invalid parameters for getting wiki notes.");
            }
        }
        catch(Exception e)
        {
            String errMsg = "Error retrieving security incident wiki notes by" + 
                            (StringUtils.isNotBlank(incidentId) ? " incidentId: " + incidentId : "") +
                            (StringUtils.isNotBlank(activityId) ? " activityId: " + activityId : "") +
                            (StringUtils.isNotBlank(componentId) ? " componentId: " + componentId : "");
            
            boolean isError = !e.getMessage().startsWith(String.format(PlaybookUtils.USER_DOES_NOT_ERROR_MSG_PREFIX, username));
            
            String msg = String.format((isError ? errMsg + ". " + e.getLocalizedMessage() : 
                                                  DENIED_ACCESS_TO_SIR_ORG_OR_NO_PERMISSION_FOR_ACTION_ON_SIR), 
                                       (StringUtils.isNotBlank(incidentId) ? incidentId : ""));
            
            if (isError)
            {
                Log.log.error(msg, e);
            }
            else
            {
                Log.log.warn(msg);
            }
            
            result.setSuccess(false).setMessage(msg);
        }
        
        return result;
    }
    
    /**
	 * Get initial request data (all parameters when RR through a GW) that belongs to a security incident.
	 * 
	 * @param incidentId : String representing security incident
	 * @param request
	 * @return
	 * @throws ServletException
	 * @throws IOException
	 */
	@RequestMapping(value = "/playbook/securityIncident/getRequestData", method = { RequestMethod.GET, RequestMethod.POST }, headers = "Accept=application/json; charset=utf-8")
    @ResponseBody
    public ResponseDTO<String> getRequestDataBySir(@RequestParam String sir, HttpServletRequest request) throws ServletException, IOException
    {
		ResponseDTO<String> result = new ResponseDTO<String>();
		String username = (String) request.getAttribute(Constants.HTTP_REQUEST_USERNAME);
		try
		{
			List<String> requestDataList = new ArrayList<String>();
			JsonElement jsonRequestData = new JsonParser().parse(PlaybookUtils.getSecurityIncident(null, sir, username).getRequestData());
			requestDataList.add(jsonRequestData.toString());
            result.setSuccess(true).setRecords(requestDataList).setSuccess(true);
		}
		catch(Exception e)
		{
		    boolean isError = !e.getMessage().startsWith(String.format(PlaybookUtils.USER_DOES_NOT_ERROR_MSG_PREFIX, username));
            
            String msg = String.format((isError ? ERROR_RETRIEVING_REQUEST_DATA_BY_SIR : 
                                                  DENIED_ACCESS_TO_SIR_ORG_OR_NO_PERMISSION_FOR_ACTION_ON_SIR), 
                                       sir);
            
            if (isError)
            {
                Log.log.error(msg + " " + e.getLocalizedMessage() , e);
            }
            else
            {
                Log.log.warn(msg);
            }
            
            result.setSuccess(false).setMessage(msg);
		}
		
		return result;
    }
    
	/**
     * Get distinct investigation types like Phishing, Malware etc within specified scope.
     * 
     * Scope is specified as # of units days/hours/minutes (default days) 
     * to go back in history from current datetime.
     * 
     * @param scopeUnits         # of days to go back in history from current time (max 365 days)
     * @param unitType           Chronological type (DAYS/HOURS/MINUTES) of units
     *                        
     * @param request
     * @return
     * @throws ServletException
     * @throws IOException
     */
    @RequestMapping(value = "/playbook/getsirtypesinscope", method = { RequestMethod.POST, RequestMethod.GET })
    @ResponseBody
    public ResponseDTO<Map<String, String>> getSIRTypesInScope(@RequestParam Integer scopeUnits, @RequestParam(value = "unitType", required = false) String unitType, HttpServletRequest request) throws ServletException, IOException
    {
        String userUnitType = unitType;
        ResponseDTO<Map<String, String>> result = new ResponseDTO<Map<String, String>>();
        
        if (StringUtils.isBlank(userUnitType))
        {
            userUnitType = ChronoUnit.DAYS.name();
        }
        
        TemporalUnit tmprlUnit = ChronoUnit.DAYS;
        
        switch (userUnitType.toUpperCase())
        {
            case "DAYS":
                tmprlUnit = ChronoUnit.DAYS;
                break;
                
            case "HOURS":
                tmprlUnit = ChronoUnit.HOURS;
                break;
                
            case "MINUTES":
                tmprlUnit = ChronoUnit.MINUTES;
                break;
                
            default:
                tmprlUnit = ChronoUnit.DAYS;
                break;                
        }
        
        try
        {
            result = PlaybookUtils.getSIRTypesInScope(scopeUnits, tmprlUnit);
        }
        catch (Exception e)
        {
            Log.log.error(ERROR_GETTING_SIR_COUNTS_BY_INVESTIGATION_REPORT, e);
            result.setSuccess(false).setMessage(ERROR_GETTING_SIR_COUNTS_BY_INVESTIGATION_REPORT);
        }
        
        return result;
    }
    
    /**
     * Activate / Deactivate Playbook(s)
     * <p>
     * @param playbookSysids : String array of Playbook ids to be activated or deactivated.
     * @param status : Boolean if true, activate. Deactivate otherwise.
     * @param request {@link HttpServletRequest}
     * @param response {@link HttpServletResponse}
     * @return
     * @throws ServletException
     * @throws IOException
     */
    @SuppressWarnings({ "rawtypes", "unchecked" })
    @RequestMapping(value = "/playbook/updatePBActiveStatus", method = { RequestMethod.GET, RequestMethod.POST }, headers = "Accept=application/json; charset=utf-8")
    @ResponseBody
    public ResponseDTO updatePBActiveStatus(@RequestBody JSONObject playbookSysids, @RequestParam("status") Boolean status,HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException
    {
        ResponseDTO result = new ResponseDTO();
        String username = (String) request.getAttribute(Constants.HTTP_REQUEST_USERNAME);
        
        Map ids = new ObjectMapper().readValue(playbookSysids.toString(), Map.class);
        List<String> idList = null;
        
        if (ids != null)
        {
            idList = (List)ids.get("playbookSysids");
        }
        
        try
        {
            if (idList == null || idList.size() == 0)
            {
                result.setSuccess(false).setMessage("Playbooks IDs must be provided");
            }
            else
            {
                Set<String> pbSysIds = 
                		new HashSet<String>(idList);
                ServiceWiki.updateWikiStatusFlag(pbSysIds, WikiStatusEnum.ACTIVE, status, true, username);
                
                result.setSuccess(true);
            }
        }
        catch (Exception e)
        {
          Log.log.error("Error updating the active flag for playbook(s).", e);
          result.setSuccess(false).setMessage("Error updating the active flag for playbook(s).");
        }
        
        return result;
    }
    
    /**
     * Update isMalicious indicator and/or descriptions for specified attachments of secuirty investigation record.
     * 
     * @param attachments   Attachments to update with updated values for isMalicious and/or description
     * @param incidentId    Security investigation incident id
     *                        
     * @param request
     * @return
     * @throws ServletException
     * @throws IOException
     */
    @SuppressWarnings({ "unchecked", "rawtypes" })
    @RequestMapping(value = "/playbook/attachment/update", method = { RequestMethod.GET, RequestMethod.POST }, headers = "Accept=application/json; charset=utf-8")
    @ResponseBody
    public ResponseDTO<PBAttachments> updateAttachments(@RequestBody JSONObject attachments, @RequestParam String incidentId, HttpServletRequest request) throws ServletException, IOException
    {
        ResponseDTO<PBAttachments> result = new ResponseDTO<PBAttachments>();
        String username = (String) request.getAttribute(Constants.HTTP_REQUEST_USERNAME);
        try
        {
            Map artifact = new ObjectMapper().readValue(attachments.toString(), Map.class);
            List<Map<String, Object>> attachmentList = null;
            
            if (artifact != null)
            {
                attachmentList = (List)artifact.get("attachments");
            }
            
            PlaybookUtils.updateAttachments(attachmentList, incidentId, username);
            List<String> updatedAttachments = new ArrayList<String>();
            for (Map<String, Object> artifactMap : attachmentList)
            {
                updatedAttachments.add((String)artifactMap.get("name"));
            }
            
            String auditLog = String.format("User '%s' updated following attachment(s): %s", username, StringUtils.join(updatedAttachments, ","));
            PlaybookUtils.auditAction(auditLog, incidentId, request.getRemoteAddr(), username);
            
            result.setSuccess(true).setMessage("Attachment(s) updated.");
        }
        catch(Exception e)
        {
            boolean isError = !e.getMessage().startsWith(String.format(PlaybookUtils.USER_DOES_NOT_ERROR_MSG_PREFIX, username));
            
            String msg = String.format((isError ? ERROR_UPDATING_INCIDENT_ATTACHMENT : 
                                                  DENIED_ACCESS_TO_SIR_ORG_OR_NO_PERMISSION_FOR_ACTION_ON_SIR), 
                                       incidentId);
            
            if (isError)
            {
                Log.log.error(msg + " " + e.getLocalizedMessage() , e);
            }
            else
            {
                Log.log.warn(msg);
            }
            
            result.setSuccess(false).setMessage(msg);
        }
        
        return result;
    }
    
    @RequestMapping(value = "/playbook/template/new", method = { RequestMethod.GET, RequestMethod.POST }, headers = "Accept=application/json; charset=utf-8")
    @ResponseBody
    public ResponseDTO<WikiDocumentVO> createNewTemplate(@RequestParam("fullname") String fullname, HttpServletRequest request) throws ServletException, IOException
    {
        ResponseDTO<WikiDocumentVO> result = new ResponseDTO<WikiDocumentVO>();
        try
        {
            WikiDocumentVO wikiVO = PlaybookUtils.createNewTemplate(fullname); 
            if (wikiVO == null)
            {
                result.setMessage("Same named Wiki already exist. Please try different name/namespace.");
                result.setSuccess(false);
            }
            else
            {
                result.setSuccess(true);
                result.setData(wikiVO);
            }
        }
        catch(Exception e)
        {
            Log.log.error(ERROR_CREATING_NEW_SIR_TEMPLATE, e);
            result.setSuccess(false).setMessage(ERROR_CREATING_NEW_SIR_TEMPLATE);
        }
        
        return result;
    }
    
    @RequestMapping(value = "/playbook/getsircountbyslareport", method = { RequestMethod.GET, RequestMethod.POST })
    @ResponseBody
    public ResponseDTO<Map<String, Object>> getSIRCountBySLAReport(
                    @RequestParam Integer scopeUnits,
                    @RequestParam String unitType,
                    @RequestParam String typeFilter,
                    @RequestParam String severityFilter,
                    @RequestParam String ownerFilter,
                    @RequestParam String teamFilter,
                    @RequestParam String priorityFilter,
                    @RequestParam String statusFilter,
                    @RequestParam int nDaysBAtRisk,  // number of days to be considered at risk
                    @RequestParam String orgId,
                    HttpServletRequest request) throws ServletException, IOException
    { 
        String userUnitType = unitType;
        ResponseDTO<Map<String, Object>> result = new ResponseDTO<Map<String, Object>>();
        
        if (StringUtils.isBlank(userUnitType))
        {
            userUnitType = ChronoUnit.DAYS.name();
        }
        
        TemporalUnit tmprlUnit = ChronoUnit.DAYS;
        
        switch (userUnitType.toUpperCase())
        {
            case "DAYS":
                tmprlUnit = ChronoUnit.DAYS;
                break;
                
            case "HOURS":
                tmprlUnit = ChronoUnit.HOURS;
                break;
                
            case "MINUTES":
                tmprlUnit = ChronoUnit.MINUTES;
                break;
                
            default:
                tmprlUnit = ChronoUnit.DAYS;
                break;                
        }
        
        String dbType = ((Main) MainBase.main).getConfigSQL().getDbtype().toUpperCase();
        
        try
        {
            String username = (String) request.getAttribute(Constants.HTTP_REQUEST_USERNAME);
            result = PlaybookUtils.getSIRCountBySLAReport(scopeUnits, tmprlUnit, dbType, typeFilter, 
                                                          severityFilter, ownerFilter, teamFilter, priorityFilter, statusFilter, nDaysBAtRisk*86400, 
                                                          orgId, username);
        }
        catch (Exception e)
        {
            Log.log.error("Error getting Sir count by SLA report", e);
            result.setSuccess(false).setMessage("Error getting Sir count by SLA report");
        }
        
        return result;
    }

    @RequestMapping(value = "/playbook/getsircountbyworkloadreport", method = { RequestMethod.GET, RequestMethod.POST })
    @ResponseBody
    public ResponseDTO<Map<String, Object>> getSIRCountByWorkloadReport(
                    @RequestParam Integer scopeUnits,
                    @RequestParam String unitType,
                    @RequestParam String typeFilter,
                    @RequestParam String severityFilter,
                    @RequestParam String ownerFilter,
                    @RequestParam String teamFilter,
                    @RequestParam String priorityFilter,
                    @RequestParam String statusFilter,
                    @RequestParam String orgId,
                    HttpServletRequest request) throws ServletException, IOException
    { 
        String userUnitType = unitType;
        ResponseDTO<Map<String, Object>> result = new ResponseDTO<Map<String, Object>>();
        
        if (StringUtils.isBlank(userUnitType))
        {
            userUnitType = ChronoUnit.DAYS.name();
        }
        
        TemporalUnit tmprlUnit = ChronoUnit.DAYS;
        
        switch (userUnitType.toUpperCase())
        {
            case "DAYS":
                tmprlUnit = ChronoUnit.DAYS;
                break;
                
            case "HOURS":
                tmprlUnit = ChronoUnit.HOURS;
                break;
                
            case "MINUTES":
                tmprlUnit = ChronoUnit.MINUTES;
                break;
                
            default:
                tmprlUnit = ChronoUnit.DAYS;
                break;                
        }
        
        String dbType = ((Main) MainBase.main).getConfigSQL().getDbtype().toUpperCase();
        
        try
        {
            String username = (String) request.getAttribute(Constants.HTTP_REQUEST_USERNAME);
            result = PlaybookUtils.getSIRCountByWorkloadReport(scopeUnits, tmprlUnit, dbType, typeFilter, 
                                                               severityFilter, ownerFilter, teamFilter, statusFilter, 
                                                               priorityFilter, orgId, username);
        }
        catch (Exception e)
        {
            Log.log.error("Error getting Sir count by Workload report", e);
            result.setSuccess(false).setMessage("Error getting Sir count by Workload report");
        }
        
        return result;
    }
    
    /**
     * A generic API to retrieve list of associated SIR models from a specified model.
     * As an example, if sourceId is an Activity, and the model is Notes, this API will return all the
     * notes associated with that activity. If the sourceId is an Activity and model is Artifact, this API
     * will return a list of artifacts associated with that Activity.
     * <br>
     * @since Resolve 6.2
     * <br>
     * @param sourceId : String representing the Id of a source which created the model.
     * @param model : String representing the model whose list is needed. Possible values are notes, artifacts, attachments.
     * @param request
     * @return
     */
    @RequestMapping(value = "/playbook/getCompList", method = { RequestMethod.GET})
    @ResponseBody
    public Object getComponentList(@RequestParam String sourceId, @RequestParam String model, HttpServletRequest request)
    {
        Object result = null;
        String username = (String) request.getAttribute(Constants.HTTP_REQUEST_USERNAME);
        
        try
        {
            result = PlaybookUtils.getCompList(sourceId, model, username);
        }
        catch (Exception e)
        {
            Log.log.error("Error while retrieving list of " + model, e);
            ResponseDTO<String> response = new ResponseDTO<String>();
            result = response.setSuccess(false).setMessage("Error while retrieving list of " + model);
        }
        
        return result;
    }
    
    /**
     * API to Save/Update SIR Configuration
     * 
     * Here's a sample input JSON string representing SIR Configuration:
     * <pre><code>
     * {
       orgId: "Org Id",                      // (Optional) Org sys id, Do not specify or set to blank for "None"
       orgName: "Org Name",                  // (Optional) Org Name, Do not specify or set to blank for "None"
                                             // Note: orgId takes precedence over orgName 
       customDataFormTabs:                   // (Optional) Array of Custom Data Form Tabs in order 
       [                                     // 
         {name: "MYCDFT1",                   // (Mandatory) Custom Data Form Tab Name
          isActive: "true/false",            // (Mandatory) Is Active/Is Not Active
          baseWikiId: "sys_id of base wiki", // One of baseWikiId or baseWikiFullName is mandatory
          baseWikiFullName: "Full Wiki Name" // 
         },
                       :
                       :
       ]
     * }
     * </code>
     * 
     * SIR Config will be created for specified Org or None if not already found.
     * There will never be more than one undeleted (i.e not soft deleted) SIR Config per Org including None in the system.
     * Custom Data Form Tag names are unique within an Org including None.
     * Order of the Custom Data Form Tags in SIR Config is as specified during save.
     * Custom Data Form Tags specified will be added into SIR Config if not already found.
     * Custom Data Form Tags specified with same name are updated.
     * Custom Data Form Tags not found in SIR Config are deleted.
     * Custom Data Form Tabs are ordered as specified in the save request.
     * 
     * <p>
     * @param jsonProperty : JSON string representing {@link SIRConfigDTO}
     * @param request
     * @return
     *      <code>{@link ResponseDTO<SIRConfigVO>}</code>
     * @throws ServletException
     * @throws IOException
     */
    @RequestMapping(value = "/playbook/sirConfig/save", method = { RequestMethod.GET, RequestMethod.POST }, headers = "Accept=application/json; charset=utf-8")
    @ResponseBody
    public ResponseDTO<SIRConfigDTO> saveSIRConfig(@RequestBody JSONObject jsonProperty, HttpServletRequest request) throws ServletException, IOException
    {
        ResponseDTO<SIRConfigDTO> result = new ResponseDTO<SIRConfigDTO>();
        
        String username = (String) request.getAttribute(Constants.HTTP_REQUEST_USERNAME);
        
        try
        {
            String json = jsonProperty.toString();
            ObjectMapper mapper = new ObjectMapper();
            mapper.configure(Feature.FAIL_ON_UNKNOWN_PROPERTIES, false);
            SIRConfigDTO sirConfigDTO = mapper.readValue(json, SIRConfigDTO.class);
            SIRConfigVO sirConfigVO = null;
            
            if (sirConfigDTO != null)
            {
                sirConfigVO = sirConfigDTO.toSIRConfigVO(username);
            }
            
            if (StringUtils.isNotBlank(sirConfigVO.getSysOrg()) && 
                (sirConfigVO.getSysOrg().equalsIgnoreCase(VO.STRING_DEFAULT) ||
                 sirConfigVO.getSysOrg().equalsIgnoreCase(Constants.NIL_STRING)))
            {
                sirConfigVO.setSysOrg(null);
            }
            
            SIRConfigVO savedSIRConfigVO = PlaybookUtils.saveSIRConfig(sirConfigVO, username);
            result.setSuccess(true).setData(new SIRConfigDTO(savedSIRConfigVO)).setMessage("Saved SIR Config.");
        }
        catch(Exception e)
        {
            boolean isError = !e.getMessage().startsWith(String.format(PlaybookUtils.USER_DOES_NOT_ERROR_MSG_PREFIX, username));
            
            String msg = isError ? ERROR_SAVING_SIR_CONFIGURATION : DENIED_ACCESS_TO_SIR_ORG;
            
            if (isError)
            {
                Log.log.error(msg + " " + e.getLocalizedMessage() , e);
            }
            else
            {
                Log.log.warn(msg);
            }
            
            result.setSuccess(false).setMessage(ERROR_SAVING_SIR_CONFIGURATION);
        }
        
        return result;
    }
    
    /**
     * Get a SIR Config
     * 
     * @param orgId   : String representing org id of the Org to get SIR Config for.  (Optional)
     * @param orgName : String representing org name of the Org to get SIR Config for.  (Optional)
     * @param request
     * @return
     * @throws ServletException
     * @throws IOException
     */
    @RequestMapping(value = "/playbook/sirConfig/get", method = { RequestMethod.GET, RequestMethod.POST }, headers = "Accept=application/json; charset=utf-8")
    @ResponseBody
    public ResponseDTO<SIRConfigDTO> getSIRConfig(@RequestParam(value = "orgId", required = false) String orgId,
                                                  @RequestParam(value = "orgName", required = false) String orgName,
                                                  HttpServletRequest request) throws ServletException, IOException
    {
        ResponseDTO<SIRConfigDTO> result = new ResponseDTO<SIRConfigDTO>();
        
        String username = (String) request.getAttribute(Constants.HTTP_REQUEST_USERNAME);
        
        String internalOrgId = orgId;
        
        try
        {
            if (StringUtils.isBlank(internalOrgId) && StringUtils.isNotBlank(orgName) &&
                !orgName.equalsIgnoreCase(OrgsVO.NONE_ORG_NAME))
            {
                Orgs orgs = OrgsUtil.findOrgModelByName(orgName);
                
                if (orgs != null)
                {
                    internalOrgId = orgs.getSys_id();
                }
            }
            
            if (StringUtils.isNotBlank(internalOrgId))
            {
                if (!UserUtils.isOrgAccessible(internalOrgId, username, false, false))
                {
                    throw new Exception("User " + username + " does not belong to SIR Config's Org.");
                }
            }
            else
            {
                if (!UserUtils.isNoOrgAccessible(username))
                {
                    throw new Exception("User " + username + " does not have access to No/None Org.");
                }
            }
            
            SIRConfigVO sirConfigVO = PlaybookUtils.findSIRConfig(null, internalOrgId, username, false);
            
            if (sirConfigVO != null)
            {
                result.setSuccess(true).setData(new SIRConfigDTO(sirConfigVO));
            }
            else
            {
            	result.setSuccess(true).setData(new SIRConfigDTO());
//                throw new Exception(NO_UNDELETED_SIR_CONFIG_FOR_ORG);
            }
        }
        catch(Exception e)
        {
            boolean isError = e.getMessage() == null || 
            				  (!(e.getMessage().startsWith(String.format(PlaybookUtils.USER_DOES_NOT_ERROR_MSG_PREFIX, username)) ||
                                 e.getMessage().startsWith(NO_UNDELETED_SIR_CONFIG_FOR_MSG_PREFIX)));
            
            String msg = isError ? ERROR_GETTING_SIR_CONFIGURATION : 
                                  (e.getMessage().startsWith(NO_UNDELETED_SIR_CONFIG_FOR_MSG_PREFIX) ? 
                                   NO_UNDELETED_SIR_CONFIG_FOR_ORG : DENIED_ACCESS_TO_SIR_ORG);
            
            if (isError)
            {
                Log.log.error(msg + " " + e.getLocalizedMessage() , e);
            }
            else
            {
                Log.log.warn(msg);
            }
            
            result.setSuccess(false).setMessage(ERROR_GETTING_SIR_CONFIGURATION);
        }
        
        return result;
    }
    
    /**
     * Fulltext search in security-incident index, compatible with Apache Lucene query syntax.
     *   
     * @param text fulltext string that may contain the termscompatible with Apache Lucene
     * @param request
     * @return
     */
	@RequestMapping(value = "/playbook/securityIncident/ftsearch", method = RequestMethod.GET, headers = "Accept=application/json; charset=utf-8")
	@ResponseBody
	public ResponseDTO<ResolveSecurityIncidentVO> searchSecurityIncidentsFullText(@RequestParam(required = true) String text,
			HttpServletRequest request) {
        ResponseDTO<ResolveSecurityIncidentVO> result = new ResponseDTO<>();
        
        String username = (String) request.getAttribute(Constants.HTTP_REQUEST_USERNAME);
        try {
            ResponseDTO<SecurityIncident> response = PlaybookUtils.searchSecurityIncidents(text, username);
            
			List<String> incidentIds = response.getRecords().stream().map(SecurityIncident::getSysId)
					.collect(Collectors.toList());
            
            QueryDTO dto = buildSearchQueryDTO(incidentIds, null);
    		List<ResolveSecurityIncidentVO> resultList = PlaybookUtils.listSecurityIncidents(dto, username);
			result.setRecords(resultList).setMessage(response.getMessage()).setSuccess(true).setTotal(resultList.size());
        } catch(Exception ex) {
			result.setMessage(ERROR_SEARCHING_SECURITY_INCIDENTS).setSuccess(false);
        }
        
		return result;
	}

	/**
	 * Traditional search in security-index using {@link QueryDTO}
	 * @param queryDTO {@ink QueryDTO} with search criteria
	 * @param request
	 * @return
	 * @throws IOException 
	 * @throws ServletException 
	 */
	@RequestMapping(value = "/playbook/securityIncident/search", method = RequestMethod.GET, headers = "Accept=application/json; charset=utf-8")
	@ResponseBody
	public ResponseDTO<ResolveSecurityIncidentVO> searchSecurityIncidents(
		@ModelAttribute QueryDTO queryDTO,
		@RequestParam(required = false) boolean includeDeleted,
		HttpServletRequest request) throws ServletException, IOException {
		boolean hasFullText = false;
        if (queryDTO.getFilterItems() != null && !queryDTO.getFilterItems().isEmpty()) {
			hasFullText = queryDTO.getFilterItems().stream()
					.filter(p -> (QueryFilter.FULLTEXT.equalsIgnoreCase(p.getCondition())
							&& ALL_FIELD.equalsIgnoreCase(p.getField())))
					.findAny().isPresent();
        }

        if (!hasFullText) {
    		return listSecurityIncidents(queryDTO, includeDeleted, request);
        }

        ResponseDTO<ResolveSecurityIncidentVO> result = new ResponseDTO<>();

		String username = (String) request.getAttribute(Constants.HTTP_REQUEST_USERNAME);
        List<QuerySort> sortItems = cloneSortItems(queryDTO.getSortItems());
        
        try {
            ResponseDTO<SecurityIncident> response = PlaybookUtils.searchSecurityIncidents(queryDTO, username);
			Map<String, Map<String, String>> idToHighlights = response.getRecords().stream()
					.collect(Collectors.toMap(SecurityIncident::getSysId, SecurityIncident::getHighlightFields));
            
			List<String> incidentIds = response.getRecords().stream().map(SecurityIncident::getSysId)
					.collect(Collectors.toList());
            if (incidentIds.isEmpty()) {
    			result.setMessage(NO_RESULTS_FOUND).setSuccess(true).setTotal(0);
            } else {
	            QueryDTO dto = buildSearchQueryDTO(incidentIds, sortItems);
	            /*
	             * HP TODO 
	             * 
	             * Following is trying to exclude all Master SIRs from listing.
	             * 
	             * Issue # 1
	             * 
	             * It should try to filter out only Master SIRs which user is not owner of.
	             * Please note user can be member of same team whose other member owns the
	             * Master SIR. Only owner of Master SIR should be able to view Master SIRs
	             * assigned to him/her.
	             * 
	             * This is the only way one team member can investigate other team member without each
	             * knowing about who is being investigated.
	             * 
	             * Issue # 2
	             * 
	             * The original query on ES is pagination query so it retrieves only limited records from ES.
	             * If the records returned by ES get filtered out further then pagination logic will break.
	             * The above filtering should happen in Logstash generated/updated "security-incident" index.
	             * Logstash query needs to include isMaster SIR flag from DB into ES "security-incident" and
	             * ES query needs to be modified to account for Issue # 1.
	             *
				 * dto.addFilterItem(new QueryFilter("auto", "null", "masterSir", QueryFilter.EQUALS));
	             * 
	             */
	            
	            List<ResolveSecurityIncidentVO> resultList = PlaybookUtils.listSecurityIncidents(dto, username);
	            
	            resultList.forEach(e -> e.setHighlightFields(idToHighlights.get(e.getSys_id())));
	            
				result.setRecords(resultList).setMessage(response.getMessage()).setSuccess(true).setTotal(response.getTotal());
            }
        } catch(Exception ex) {
			result.setMessage(ERROR_SEARCHING_SECURITY_INCIDENTS).setSuccess(false);
        }
        
		return result;
	}

	@RequestMapping(value = "/playbook/savedsearch", method = RequestMethod.GET, headers = "Accept=application/json; charset=utf-8")
	@ResponseBody
	public ResponseDTO<SavedSearchDTO> getAllSavedSearch(@ModelAttribute QueryDTO queryDTO, HttpServletRequest request) {
        ResponseDTO<SavedSearchDTO> result = new ResponseDTO<>();
        
        String username = (String) request.getAttribute(Constants.HTTP_REQUEST_USERNAME);
        
        try {
			List<SavedSearchDTO> response = SavedSearchUtil
					.covertSavedSearchDTOList(SavedSearchUtil.getAllUserSavedSearch(username));
        	result.setRecords(response).setSuccess(true);
        } catch(Exception ex) {
			Log.log.error(ERROR_GETTING_SAVED_SEARCH, ex);
			result.setMessage(ERROR_GETTING_SAVED_SEARCH).setSuccess(false);
        }
        
		return result;
	}

	@RequestMapping(value = "/playbook/savedsearch", method = RequestMethod.POST)
	@ResponseBody
	public ResponseDTO<SavedSearchDTO> save(@RequestBody SavedSearchDTO dto, HttpServletRequest request) {
		ResponseDTO<SavedSearchDTO> result = new ResponseDTO<>();

        String username = (String) request.getAttribute(Constants.HTTP_REQUEST_USERNAME);
        
        try {
			SavedSearchDTO savedDto = SavedSearchUtil
					.covertSavedSearchDTO(SavedSearchUtil.saveSavedSearch(dto, username));
			result.setSuccess(true).setData(savedDto);
		} catch (Throwable e) {
			Log.log.error(ERROR_SAVING_SEARCH, e);
			result.setSuccess(false).setMessage(ERROR_SAVING_SEARCH);
		}

		return result;
	}

	@RequestMapping(value = "/playbook/savedsearch/{name}", method = RequestMethod.DELETE)
	@ResponseBody
	public ResponseDTO<ArtifactConfigurationDTO> deleteBulk(@PathVariable String name, HttpServletRequest request) {
		ResponseDTO<ArtifactConfigurationDTO> result = new ResponseDTO<>();

        String username = (String) request.getAttribute(Constants.HTTP_REQUEST_USERNAME);
        
        try {
        	boolean deleteResult = SavedSearchUtil.deleteSavedSearch(username, name);
        			
			if (deleteResult) {
				result.setSuccess(deleteResult);
			} else {
				result.setMessage(ERROR_DELETING_DATA);
			}
		} catch (Exception e) {
			Log.log.error(ERROR_DELETING_DATA, e);
			result.setSuccess(false).setMessage(ERROR_DELETING_DATA);
		}

		return result;
	}

	private List<QuerySort> cloneSortItems(List<QuerySort> sortItems) {
		List<QuerySort> sorts = new ArrayList<QuerySort>();
		for (QuerySort sort : sortItems) {
			sorts.add(new QuerySort(sort.getProperty(), sort.getDirection()));
		}
		
		return sorts;
	}

	private QueryDTO buildSearchQueryDTO(List<String> incidentIds, List<QuerySort> sortItems) {
        QueryDTO dto = new QueryDTO();
		dto.setModelName("ResolveSecurityIncident");
		dto.addFilterItem(new QueryFilter(QueryFilter.BOOL_TYPE, "false|null", "sysIsDeleted", QueryFilter.EQUALS));
		dto.addFilterItem(new QueryFilter("terms", String.join(",", incidentIds), "sys_id", QueryFilter.EQUALS, false));
		if (sortItems != null) {
			dto.setSortItems(sortItems);
		}
		
		return dto;
	}
	
	/**
     * Get count report of SIR/Case Activities/Tasks record counts  
     * by Activity/Task Status such as "Open", "In Progress" etc
     * for logged in user.
     * 
     * @param orgId            Org sys id of currently selected Org, for None Org 
     *                         pass blank or null.                        
     * @param request
     * @return
     * @throws ServletException
     * @throws IOException
     */
    @RequestMapping(value = "/playbook/getsiractivitycountbystatus", method = { RequestMethod.POST, RequestMethod.GET })
    @ResponseBody
    public ResponseDTO<Map<String, Object>> getSIRActivityCountByStatusReport(@RequestParam String orgId,
                                                                              HttpServletRequest request) throws ServletException, IOException
    {
        ResponseDTO<Map<String, Object>> result = new ResponseDTO<Map<String, Object>>();
        
        try
        {
            String username = (String) request.getAttribute(Constants.HTTP_REQUEST_USERNAME);
            
            result = PlaybookUtils.getSIRActivityCountByStatusReport(orgId, username,
            		((Main) MainBase.main).getConfigSQL());
        }
        catch (Exception e)
        {
            String msg = ERROR_GETTING_SIR_ACTIVITY_COUNTS_BY_STATUS_REPORT;
            Log.log.error(msg, e);
            result.setSuccess(false).setMessage("Error:" + e.getMessage());
        }
        
        return result;
    }
    
	@RequestMapping(value = "/playbook/associatedsir", method = RequestMethod.GET)
	@ResponseBody
	public ResponseDTO<SecurityIncidentLightDTO> getAssociatedSir(@RequestParam String sir, @RequestParam String orgId, HttpServletRequest request) {
        ResponseDTO<SecurityIncidentLightDTO> result = new ResponseDTO<>();
        
        String username = (String) request.getAttribute(Constants.HTTP_REQUEST_USERNAME);
        
        try {
        	List<SecurityIncidentLightDTO> associated = PlaybookUtils.searchAssociatedSir(sir, username);
        	result.setRecords(associated).setSuccess(true);
        } catch(Exception ex) {
			Log.log.error(ERROR_GETTING_ASSOCIATES, ex);
			result.setMessage(ERROR_GETTING_ASSOCIATES).setSuccess(false);
        }
        
		return result;
	}

	@RequestMapping(value = "/playbook/caseattributes", method = RequestMethod.GET)
	@ResponseBody
	public ResponseDTO<CaseAttributeVO> getCaseAttributes(HttpServletRequest request) {
        ResponseDTO<CaseAttributeVO> result = new ResponseDTO<>();
        
        String username = (String) request.getAttribute(Constants.HTTP_REQUEST_USERNAME);

        try {
        	List<CaseAttributeVO> ca = CaseAttributeUtil.getCaseAttributes(username);
        	result.setRecords(ca).setSuccess(true);
        } catch(Exception ex) {
			Log.log.error(ERROR_GETTING_CASE_ATTRIBUTES, ex);
			result.setMessage(ERROR_GETTING_CASE_ATTRIBUTES).setSuccess(false);
        }
	
        return result;
	}

	@RequestMapping(value = "/playbook/caseattributes/{columnName}", method = RequestMethod.GET)
	@ResponseBody
	public ResponseDTO<CaseAttributeVO> getCaseAttribute(@PathVariable String columnName, HttpServletRequest request) {
        ResponseDTO<CaseAttributeVO> result = new ResponseDTO<>();
        
        String username = (String) request.getAttribute(Constants.HTTP_REQUEST_USERNAME);

        try {
        	CaseAttributeVO ca = CaseAttributeUtil.getCaseAttribute(columnName, username);
        	result.setData(ca).setSuccess(true);
        } catch(Exception ex) {
			Log.log.error(ERROR_GETTING_CASE_ATTRIBUTES, ex);
			result.setMessage(ERROR_GETTING_CASE_ATTRIBUTES).setSuccess(false);
        }
	
        return result;
	}

	@RequestMapping(value = "/playbook/caseattributes", method = RequestMethod.POST, headers = "Accept=application/json; charset=utf-8")
	@ResponseBody
	public ResponseDTO<CaseAttributeVO> saveCaseAttributes(@RequestBody CaseAttributeVO vo, HttpServletRequest request) {
		
		ResponseDTO<CaseAttributeVO> result = new ResponseDTO<>();

        String username = (String) request.getAttribute(Constants.HTTP_REQUEST_USERNAME);
        
        try {
        	CaseAttributeVO savedVO = CaseAttributeUtil.saveCaseAttribute(vo, username);
			result.setSuccess(true).setData(savedVO);
		} catch (Exception e) {
			Log.log.error(ERROR_SAVING_CASE_ATTRIBUTES, e);
			result.setSuccess(false).setMessage(ERROR_SAVING_CASE_ATTRIBUTES);
		}

		return result;
	}

	@RequestMapping(value = "/playbook/caseattributes/{columnName}", method = RequestMethod.DELETE, headers = "Accept=application/json; charset=utf-8")
	@ResponseBody
	public ResponseDTO<CaseAttributeVO> deleteCaseAttributes(@PathVariable String columnName, HttpServletRequest request) {
		ResponseDTO<CaseAttributeVO> result = new ResponseDTO<>();

        String username = (String) request.getAttribute(Constants.HTTP_REQUEST_USERNAME);
        
        try {
        	boolean deleteResult = CaseAttributeUtil.deleteCaseAttribute(columnName, username);
			result.setSuccess(deleteResult);
		} catch (Exception e) {
			Log.log.error(ERROR_DELETING_CASE_ATTRIBUTES, e);
			result.setSuccess(false).setMessage(ERROR_DELETING_CASE_ATTRIBUTES);
		}

		return result;
	}

	@RequestMapping(value = "/playbook/securityIncident/attributes", method = RequestMethod.POST, headers = "Accept=application/json; charset=utf-8")
	@ResponseBody
	public ResponseDTO<ResolveSecurityIncidentVO> addIncidentCaseAttribute(@RequestParam String incidentId,
			@RequestParam String columnName, @RequestParam String columnValue,
			@RequestParam(required = false) String orgId, HttpServletRequest request) {
		
		ResponseDTO<ResolveSecurityIncidentVO> result = new ResponseDTO<>();

        String username = (String) request.getAttribute(Constants.HTTP_REQUEST_USERNAME);
        
        try {
        	ResolveSecurityIncidentVO savedVO = CaseAttributeUtil.addIcidentAttribute(incidentId, columnName, columnValue, orgId, username);
			result.setSuccess(true).setData(savedVO);
		} catch (Exception e) {
			result.setSuccess(false)
			.setMessage(checkAndSetErrorMessage(
							e, ERROR_SAVING_CASE_ATTRIBUTES, 
							Arrays.asList(String.format(PlaybookUtils.USER_DOES_NOT_ERROR_MSG_PREFIX, username),
										  String.format(PlaybookUtils.DENIED_VIEW_ACCESS_SECURITY_INCIDENTS, username)),
							null, null));
		}

		return result;
	}

	/**
	 * Mark a SIR incident as duplicate of a Master security incident. Note: one Master SIR can have zero or more SIRs as duplicates. Each duplicate SIR can have only one Master.
	 * The call will create Master-Duplicate relation between two SIR incidents. 
	 * 
	 * @param masterId : sys_id of master SIR incident.  
	 * @param duplicateId : sys_id of duplicate SIR incident.  
	 * @param request
	 * @return ResolveSecurityIncidentVO of Master SIR, which contains the collection of all its duplicate SIRs. 
	 * @throws ServletException
	 * @throws IOException
	 */
	@RequestMapping(value = "/playbook/securityIncident/markAsDuplicate", method = { RequestMethod.GET, RequestMethod.POST }, headers = "Accept=application/json; charset=utf-8")
    @ResponseBody
    public ResponseDTO<ResolveSecurityIncidentVO> markAsDuplicate(@RequestParam(value = "masterId", required = true) String masterId,
    		@RequestParam(value = "duplicateId", required = true) String duplicateId, HttpServletRequest request) throws ServletException, IOException
    {
		ResponseDTO<ResolveSecurityIncidentVO> result = new ResponseDTO<ResolveSecurityIncidentVO>();
		String username = (String) request.getAttribute(Constants.HTTP_REQUEST_USERNAME); 
		try
		{
			ResolveSecurityIncidentVO vo = PlaybookUtils.markAsDuplicateIncident(masterId, duplicateId, username, request);
			PlaybookUtils.auditAction(vo.getAuditMessage(), masterId, request.getRemoteAddr(), username);
			PlaybookUtils.auditAction(vo.getAuditMessage(), duplicateId, request.getRemoteAddr(), username);

			result.setData(vo).setSuccess(true);
		}
		catch(Exception e)
		{
			boolean isError = !(e.getMessage().startsWith(String.format(PlaybookUtils.USER_DOES_NOT_ERROR_MSG_PREFIX, username)) ||
								e.getMessage().contains(PlaybookUtils.RBAC_IS_DENIED_ACCESS_TO_MESSAGE_MARKER));

			String msg = isError ? ERROR_MARK_AS_DUPLICATE : "";

			if (isError)
			{
			    Log.log.error(msg + " " + e.getLocalizedMessage() , e);
			}
			else
			{
			    Log.log.warn(msg + " " + e.getLocalizedMessage());
			}
			
			result.setSuccess(false).
				setMessage(ERROR_MARK_AS_DUPLICATE + 
						   (isError ?
							e.getLocalizedMessage() : 
							" " + (e.getMessage().startsWith(String.format(PlaybookUtils.USER_DOES_NOT_ERROR_MSG_PREFIX, username)) ? 
								   DENIED_ACCESS_TO_SIR_ORG : e.getLocalizedMessage())));
		}
		
		return result;
    }

	/**
	 * Remove a duplicate SIR incident from its Master incident. The duplicate should be marked as one of duplicates of its Master incident before the call. 
	 * The call provide a way to cut the existing Master-Duplicate relation between these two SIR incidents. 
	 * 
	 * @param masterId : sys_id of master SIR incident.  
	 * @param duplicateId : sys_id of duplicate SIR incident.  
	 * @param request
	 * @return ResolveSecurityIncidentVO of Master SIR, which has the duplicate SIR as one of its duplicates. 
	 * @throws ServletException
	 * @throws IOException
	 */
	@RequestMapping(value = "/playbook/securityIncident/removeDuplicate", method = { RequestMethod.GET, RequestMethod.POST }, headers = "Accept=application/json; charset=utf-8")
    @ResponseBody
    public ResponseDTO<ResolveSecurityIncidentVO> removeDuplicate(@RequestParam(value = "masterId", required = true) String masterId,
    		@RequestParam(value = "duplicateId", required = true) String duplicateId, HttpServletRequest request) throws ServletException, IOException
    {
		ResponseDTO<ResolveSecurityIncidentVO> result = new ResponseDTO<ResolveSecurityIncidentVO>();
		String username = (String) request.getAttribute(Constants.HTTP_REQUEST_USERNAME); 
		try
		{
			ResolveSecurityIncidentVO vo = PlaybookUtils.removeDuplicateIncident(masterId, duplicateId, username);
			PlaybookUtils.auditAction(vo.getAuditMessage(), masterId, request.getRemoteAddr(), username);
			PlaybookUtils.auditAction(vo.getAuditMessage(), duplicateId, request.getRemoteAddr(), username);
			result.setData(vo).setSuccess(true);
		}
		catch(Exception e)
		{
			boolean isError = !(e.getMessage().startsWith(String.format(PlaybookUtils.USER_DOES_NOT_ERROR_MSG_PREFIX, username)) ||
					e.getMessage().contains(ERROR_COULD_NOT_FIND_INCIDENT_FOR_SIR_WITH_SYS_ID_PREFIX) ||
					e.getMessage().contains(ERROR_MASTER_AND_DUPLICATE_NO_COMMON_ROOT_ORG_PREFIX) ||
					e.getMessage().contains(PlaybookUtils.RBAC_IS_DENIED_ACCESS_TO_MESSAGE_MARKER));

			String msg = isError ? ERROR_REMOVE_DUPLICATE : "";
			
			if (isError)
			{
			    Log.log.error(msg + " " + e.getLocalizedMessage() , e);
			}
			else
			{
			    Log.log.warn(msg + " " + e.getLocalizedMessage());
			}
			
			result.setSuccess(false).
				setMessage(ERROR_REMOVE_DUPLICATE + 
						   (isError ?
							"" : 
							" " + (e.getMessage().startsWith(String.format(PlaybookUtils.USER_DOES_NOT_ERROR_MSG_PREFIX, username)) ? 
								   DENIED_ACCESS_TO_SIR_ORG : e.getLocalizedMessage())));
		}
		
		return result;
    }
	
	/**
	 * List Resolve Security Incident Duplicates
	 * 
	 * @param queryDTO : {@ink QueryDTO} containing search paging settings
	 * @param masterId : sys_id of master SIR incident.  
	 * @param request
	 * @return List of ResolveSecurityIncidentVO of duplicates 
	 * @throws ServletException
	 * @throws IOException
	 */
	@RequestMapping(value = "/playbook/securityIncident/getDuplicates", method = { RequestMethod.GET, RequestMethod.POST }, headers = "Accept=application/json; charset=utf-8")
	@ResponseBody
	public ResponseDTO<ResolveSecurityIncidentVO> listSecurityIncidents(@ModelAttribute QueryDTO queryDTO, 
																		@RequestParam(value = "masterId", 
																					  required = true) String masterId, 
																		HttpServletRequest request) throws ServletException, 
																										   IOException {
		ResponseDTO<ResolveSecurityIncidentVO> result = new ResponseDTO<ResolveSecurityIncidentVO>();
		String username = (String) request.getAttribute(Constants.HTTP_REQUEST_USERNAME);
		try {
			result = PlaybookUtils.listIncidentDuplicates(queryDTO, masterId, username);
		} catch(Exception e) {
			boolean isError = !(e.getMessage().startsWith(String.format(PlaybookUtils.USER_DOES_NOT_ERROR_MSG_PREFIX, 
																		username)) ||
								e.getMessage().contains(ERROR_COULD_NOT_FIND_INCIDENT_FOR_SIR_WITH_SYS_ID_PREFIX) ||
								e.getMessage().contains(ERROR_COULD_NOT_FIND_MASTER_SIR_INCIDENT_WITH_SYS_ID_PREFIX) ||
								e.getMessage().contains(ERROR_MASTER_AND_DUPLICATE_NO_COMMON_ROOT_ORG_PREFIX) ||
								e.getMessage().contains(PlaybookUtils.RBAC_IS_DENIED_ACCESS_TO_MESSAGE_MARKER));

			String msg = String.format("%s%s", (isError ? ERROR_LIST_DUPLICATE : ""), 
									   (StringUtils.isNotBlank(e.getMessage()) ? " " + e.getMessage() : ""));
			
			if (isError) {
			    Log.log.error(msg, e);
			} else {
			    Log.log.warn(msg);
			}
			
			result.setSuccess(false)
			.setMessage(ERROR_LIST_DUPLICATE + 
						(isError ?
						 "" : 
						 " " + (e.getMessage().startsWith(String.format(PlaybookUtils.USER_DOES_NOT_ERROR_MSG_PREFIX, 
								 										username)) ? 
								DENIED_ACCESS_TO_SIR_ORG : e.getMessage())));
		}
		
		return result;
	}

	@RequestMapping(value = "/playbook/customdata/{incidentId}", method = RequestMethod.GET)
	@ResponseBody
	public ResponseDTO<CustomDataFormVO> getCustomDataForms(@PathVariable String incidentId, HttpServletRequest request) {
        ResponseDTO<CustomDataFormVO> result = new ResponseDTO<>();
        
        String username = (String) request.getAttribute(Constants.HTTP_REQUEST_USERNAME);

        try {
        	List<CustomDataFormVO> cdf = CustomDataFormUtil.getSIRCustomDataForms(incidentId, username);
        	result.setRecords(cdf).setSuccess(true);
        } catch(Exception ex) {
			Log.log.error(ERROR_GETTING_CUSTOM_DATA_FORM, ex);
			result.setMessage(ERROR_GETTING_CUSTOM_DATA_FORM).setSuccess(false);
        }
	
        return result;
	}

	@RequestMapping(value = "/playbook/customdata/{incidentId}/{wikiId}", method = RequestMethod.GET)
	@ResponseBody
	public ResponseDTO<CustomDataFormVO> getCustomDataForm(@PathVariable String incidentId, @PathVariable String wikiId,
			HttpServletRequest request) {
        ResponseDTO<CustomDataFormVO> result = new ResponseDTO<>();
        
        String username = (String) request.getAttribute(Constants.HTTP_REQUEST_USERNAME);

        try {
        	CustomDataFormVO cdf = CustomDataFormUtil.getSIRCustomDataForm(incidentId, wikiId, username);
        	result.setData(cdf).setSuccess(true);
        } catch(Exception ex) {
			Log.log.error(ERROR_GETTING_CUSTOM_DATA_FORM, ex);
			result.setMessage(ERROR_GETTING_CUSTOM_DATA_FORM).setSuccess(false);
        }
	
        return result;
	}

	@RequestMapping(value = "/playbook/customdata", method = RequestMethod.POST, headers = "Accept=application/json; charset=utf-8")
	@ResponseBody
	public ResponseDTO<CustomDataFormVO> saveCustomDataForm(@RequestBody CustomDataFormVO formVO,
			HttpServletRequest request) {
		ResponseDTO<CustomDataFormVO> result = new ResponseDTO<>();

        String username = (String) request.getAttribute(Constants.HTTP_REQUEST_USERNAME);
        
        try {
        	CustomDataFormVO savedVO = CustomDataFormUtil.saveSIRCustomDataForm(formVO, username);
			result.setSuccess(true).setData(savedVO);
		} catch (Exception e) {
			Log.log.error(ERROR_SAVING_CUSTOM_DATA_FORM, e);
			result.setSuccess(false).setMessage(ERROR_SAVING_CUSTOM_DATA_FORM);
		}

		return result;
	}

	@RequestMapping(value = "/playbook/customdata/{incidentId}/{wikiId}", method = RequestMethod.DELETE)
	@ResponseBody
	public ResponseDTO<CustomDataFormVO> deleteCustomDataForm(@PathVariable String incidentId,
			@PathVariable String wikiId, HttpServletRequest request) {
		ResponseDTO<CustomDataFormVO> result = new ResponseDTO<>();

        String username = (String) request.getAttribute(Constants.HTTP_REQUEST_USERNAME);
        
        try {
        	boolean deleteResult = CustomDataFormUtil.deleteSIRCustomDataForm(incidentId, wikiId, username);
			result.setSuccess(deleteResult);
		} catch (Exception e) {
			result.setSuccess(false).setMessage(checkAndSetErrorMessage(e, ERROR_DELETING_CUSTOM_DATA_FORM));
		}

		return result;
	}

    @RequestMapping(value = "/playbook/securityIncident/bulkUpdate", method = { RequestMethod.POST }, headers = "Accept=application/json; charset=utf-8")
    @ResponseBody
    public ResponseDTO<ResolveSecurityIncidentVO> saveSecurityIncident(
                    @RequestBody SIRBulkUpdatePayloadDTO payload,
                    HttpServletRequest request) throws ServletException, IOException
    {

        ResponseDTO<ResolveSecurityIncidentVO> result = new ResponseDTO<>();

        String username = (String) request.getAttribute(Constants.HTTP_REQUEST_USERNAME);

        try
        {

            Pair<List<ResolveSecurityIncidentVO>, String> updatedIncidents = PlaybookUtils.updateSecurityIncidents(payload, username, request);
            result.setSuccess(StringUtils.isBlank(updatedIncidents.getRight())).setRecords(updatedIncidents.getLeft()).setMessage(updatedIncidents.getRight());
        }
        catch (Exception e)
        {
            result.setSuccess(false)
            .setMessage(checkAndSetErrorMessage(
    						e, ERROR_SAVING_SECURITY_INCIDENT, 
    						Arrays.asList(String.format(PlaybookUtils.USER_DOES_NOT_ERROR_MSG_PREFIX, username),
    									  PlaybookUtils.BLANK_EXT_REF_ID_ERR_MSG, PlaybookUtils.ERROR_CLOSED_INCIDENT),
    						Arrays.asList(PlaybookUtils.RBAC_IS_DENIED_ACCESS_TO_MESSAGE_MARKER,
    									  ERROR_COULD_NOT_FIND_INCIDENT_FOR_SIR_WITH_SYS_ID_PREFIX,
    									  ERROR_MASTER_AND_DUPLICATE_NO_COMMON_ROOT_ORG_PREFIX,
    									  PlaybookUtils.COULD_NOT_FIND_ERR_MSG_PREFIX, PlaybookUtils.NOT_SUPPORTED_FOR),
    						null));
        }

        return result;
    }
    
}
