/******************************************************************************
* (C) Copyright 2014
*
* Resolve Systems, LLC
*
* All rights reserved
* This software is distributed under license and may not be used, copied,
* modified, or distributed without the express written permission of
* Resolve Systems, LLC.
*
******************************************************************************/

package com.resolve.rsview.controller;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Set;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.codehaus.jackson.map.ObjectMapper;
import org.springframework.stereotype.Controller;
import org.springframework.stereotype.Service;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.resolve.services.ServiceHibernate;
import com.resolve.services.graph.social.model.component.tree.AdvanceTree;
import com.resolve.services.interfaces.SocialDTO;
import com.resolve.services.vo.QueryDTO;
import com.resolve.services.vo.QueryFilter;
import com.resolve.services.vo.QuerySort;
import com.resolve.services.vo.ResponseDTO;
import com.resolve.services.vo.social.SocialForumDTO;
import com.resolve.services.vo.social.SocialProcessDTO;
import com.resolve.services.vo.social.SocialRssDTO;
import com.resolve.services.vo.social.SocialTeamDTO;
import com.resolve.util.Constants;
import com.resolve.util.Log;

import net.sf.json.JSONObject;
/**
 * 
 * Controller for Resolve profile administration.
 * It provides CRUD operation to all the components (here component is meant by Process, Teams, Forum and RSS) along with
 * user preferences, notifications and profile. It also provides an API to follow / un-follow Documents, Runbooks, ActionTasks and User.
 *
 */
@Controller
@Service
public class AjaxSocialAdmin extends GenericController
{
	private static final String ERROR_ADDING_CONTAINER_COMPONENTS = "Error adding container components";
	private static final String ERROR_GETTING_CONTAINER_COMPONENTS = "Error getting container components";
	private static final String ERROR_REMOVING_CONTAINER_COMPONENTS = "Error removing container components";
	private static final String ERROR_RETRIEVING_COMPONENT = "Error retrieving component %s";
	private static final String ERROR_RETRIEVING_COMPONENTS = "Error retrieving components";
	private static final String ERROR_SAVING_COMPONENT = "Error saving component %";
    private static final String ERROR_DELETING_COMPONENT = "Error deleting component";

	/**
     * Retrieves list of components. It's a generic method to get listing of Process, Team, Forum and RSS.
     * 
     * @param query : {@link QueryDTO}, represents the query, pagination, sorting, grouping for the UI
     * @param socialTableName : String representing table name which will be queried for the list operation. Possible options could either be
     * <code> social_process, social_team, social_forum or rss_subscription</code>.
     * @param request {@link HttpServletRequest} </code> containing <code>USERNAME</code>: a username of the user who is logged in to Resolve
     * @param response {@link HttpServletResponse}
     * @return {@link ResponseDTO} with success true, records set to the list of either {@link SocialProcessDTO}, {@link SocialTeamDTO}, {@link SocialForumDTO} or {@link SocialRssDTO} depending upon which Social component is requested.
     *          Otherwise success will be set to false, message set to the predefined error message, "Error retrieving components from: [SocialTableName]" and an exception will be logged into rsview log.
     * @throws ServletException
     * @throws IOException
     */
    @SuppressWarnings("unchecked")
    @RequestMapping(value = "/socialadmin/component/list", method = { RequestMethod.GET, RequestMethod.POST }, headers = "Accept=application/json; charset=utf-8")
    @ResponseBody
    public ResponseDTO list(@ModelAttribute QueryDTO query, @RequestParam String socialTableName, HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException
    {
        ResponseDTO result = new ResponseDTO();
        String username = (String) request.getAttribute(Constants.HTTP_REQUEST_USERNAME);
        try
        {
            query.setModelName(socialTableName);
            
            //correct system columns from the client to match the custom table filters (because we don't have models for these components like we do for everything else)
            List<QueryFilter> filters = query.getFilterItems();
            for( QueryFilter filter : filters) {
                if( "sysId".equalsIgnoreCase(filter.getField()))filter.setField("sys_id");
                if( "sysUpdatedOn".equalsIgnoreCase(filter.getField()))filter.setField("sys_updated_on");
                if( "sysUpdatedBy".equalsIgnoreCase(filter.getField()))filter.setField("sys_updated_by");
                if( "sysCreatedOn".equalsIgnoreCase(filter.getField()))filter.setField("sys_created_on");
                if( "sysCreatedBy".equalsIgnoreCase(filter.getField()))filter.setField("sys_created_by");
            }
            query.setFilterItems(filters);
            
            List<QuerySort> sorts = query.getSortItems();
            for( QuerySort filter : sorts) {
                if( "sysId".equalsIgnoreCase(filter.getProperty()))filter.setProperty("sys_id");
                if( "sysUpdatedOn".equalsIgnoreCase(filter.getProperty()))filter.setProperty("sys_updated_on");
                if( "sysUpdatedBy".equalsIgnoreCase(filter.getProperty()))filter.setProperty("sys_updated_by");
                if( "sysCreatedOn".equalsIgnoreCase(filter.getProperty()))filter.setProperty("sys_created_on");
                if( "sysCreatedBy".equalsIgnoreCase(filter.getProperty()))filter.setProperty("sys_created_by");
                if( "u_user_display_name".equalsIgnoreCase(filter.getProperty()))filter.setProperty("u_owner");
            }
            query.setSortItems(sorts);
            
            
            Map<String, Object> data = ServiceHibernate.getSocialComponents(query, username);
            if (query.getModelName().contains("process"))
            {
                List<SocialProcessDTO> list = (List<SocialProcessDTO>) data.get("DATA");
                result.setSuccess(true).setRecords(list);
            }
            else if (query.getModelName().contains("forum"))
            {
                List<SocialForumDTO> list = (List<SocialForumDTO>) data.get("DATA");
                result.setSuccess(true).setRecords(list);
            }
            else if (query.getModelName().contains("team"))
            {
                List<SocialTeamDTO> list = (List<SocialTeamDTO>) data.get("DATA");
                result.setSuccess(true).setRecords(list);
            }
            else if (query.getModelName().contains("rss"))
            {
                List<SocialRssDTO> list = (List<SocialRssDTO>) data.get("DATA");
                result.setSuccess(true).setRecords(list);
            }
            
            int total = ServiceHibernate.getTotalHqlCount(query);
            result.setTotal(total);
        }
        catch (Exception e)
        {
            Log.log.error(ERROR_RETRIEVING_COMPONENTS, e);
            result.setSuccess(false).setMessage(ERROR_RETRIEVING_COMPONENTS);
        }
       
        return result;
    }
    /**
     * Saves a component. If component sys_id is null, it's a new component to save, otherwise it's an update to an existing one.
     * 
     * @param container : JSON String representing either <code>SocialProcessDTO, SocialTeamDTO, SocialForumDTO or SocialRssDTO</code>
     * @param compType : String representing component type. It could either be <code>Process, Teams, Forums or RSS</code>
     * @param validate : Boolean flag. NOT USED anymore.
     * @param request {@link HttpServletRequest} </code> containing <code>USERNAME</code>: a username of the user who is logged in to Resolve
     * @param response {@link HttpServletResponse}
     * @return {@link ResponseDTO} with success true, data could either be {@link SocialProcessDTO}, {@link SocialTeamDTO}, {@link SocialForumDTO} or {@link SocialRssDTO} depending upon which Social component is getting saved.
     *          Otherwise success will be set to false, message set to the predefined error message, "Error saving the [Component name] : [Error Message]" and an exception will be logged into rsview log.
     * @throws ServletException
     * @throws IOException
     */
    @RequestMapping(value = "/socialadmin/component/save", method = { RequestMethod.GET, RequestMethod.POST }, headers = "Accept=application/json; charset=utf-8")
    @ResponseBody
    public ResponseDTO save(@RequestBody JSONObject container, @RequestParam String compType, @RequestParam Boolean validate, HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException
    {
        String component = null;
        ResponseDTO result = new ResponseDTO();
        String username = (String) request.getAttribute(Constants.HTTP_REQUEST_USERNAME);
        String json = container.toString();
        
        try
        {
            if(compType.equalsIgnoreCase(AdvanceTree.PROCESS))
            {
                component = "Process";
                SocialProcessDTO entity = new ObjectMapper().readValue(json, SocialProcessDTO.class);
                SocialProcessDTO vo = ServiceHibernate.saveProcess(entity, username, validate);
                result.setData(vo);
            }
            else if (compType.equalsIgnoreCase(AdvanceTree.TEAMS))
            {
                component = "Team";
                SocialTeamDTO entity = new ObjectMapper().readValue(json, SocialTeamDTO.class);
                SocialTeamDTO vo = ServiceHibernate.saveTeam(entity, username, validate);
                result.setData(vo);
            }
            else if(compType.equalsIgnoreCase(AdvanceTree.FORUMS))
            {
                component = "Forum";
                SocialForumDTO entity = new ObjectMapper().readValue(json, SocialForumDTO.class);
                SocialForumDTO vo = ServiceHibernate.saveForum(entity, username, validate);
                result.setData(vo);
            }
            else if(compType.equalsIgnoreCase(AdvanceTree.RSS))
            {
                component = "Rss";
                SocialRssDTO entity = new ObjectMapper().readValue(json, SocialRssDTO.class);
                SocialRssDTO vo = ServiceHibernate.saveRss(entity, username, validate);
                result.setData(vo);
            }
            
            result.setSuccess(true);
        }
        catch (Exception e)
        {
        	String msg = String.format(ERROR_SAVING_COMPONENT, component);
            Log.log.error(msg, e);
            result.setSuccess(false).setMessage(msg);
        }

        return result;
    }

    /**
     * Read social component.
     * 
     * @param id : String representing sysId of the component
     * @param compType: String representing component type. It could either be <code>Process, Teams, Forums or RSS</code>
     * @param request {@link HttpServletRequest} </code> containing <code>USERNAME</code>: a username of the user who is logged in to Resolve
     * @param response {@link HttpServletResponse}
     * @return {@link ResponseDTO} with success true, data could either be {@link SocialProcessDTO}, {@link SocialTeamDTO}, {@link SocialForumDTO} or {@link SocialRssDTO} depending upon which Social component is requested.
     *          Otherwise success will be set to false, message set to the predefined error message, "Error retrieving [Component type] : [Error Message]" and an exception will be logged into rsview log.
     * @throws ServletException
     * @throws IOException
     */
    @RequestMapping(value = "/socialadmin/component/get", method = { RequestMethod.GET, RequestMethod.POST }, headers = "Accept=application/json; charset=utf-8")
    @ResponseBody
    public ResponseDTO get(@RequestParam String id, @RequestParam String compType, HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException
    {
        ResponseDTO result = new ResponseDTO();
        String username = (String) request.getAttribute(Constants.HTTP_REQUEST_USERNAME);
        String component = null;
        try
        {
            if(compType.equalsIgnoreCase(AdvanceTree.PROCESS))
            {
                SocialProcessDTO vo = ServiceHibernate.getProcess(id, username);
                component = "Process";
                result.setData(vo);
            }
            else if(compType.equalsIgnoreCase(AdvanceTree.TEAMS))
            {
                SocialTeamDTO vo = ServiceHibernate.getTeam(id, username);
                component = "Team";
                result.setData(vo);
            }
            else if(compType.equalsIgnoreCase(AdvanceTree.FORUMS))
            {
                SocialForumDTO vo = ServiceHibernate.getForum(id, username);
                component = "Forum";
                result.setData(vo);
            }
            else if(compType.equalsIgnoreCase(AdvanceTree.RSS))
            {
                SocialRssDTO vo = ServiceHibernate.getRss(id, username);
                component = "Rss";
                result.setData(vo);
            }
            
            result.setSuccess(true);
        }
        catch (Exception e)
        {
        	String msg = String.format(ERROR_RETRIEVING_COMPONENT, component);
            Log.log.error(msg, e);
            result.setSuccess(false).setMessage(msg);
        }

        return result;
    }
    
    /**
     * List all the component which are part of the parent component represented by the <code> id </code> and <code> compType </code>
     * 
     * @param id : String representing sysId of the parent component.
     * @param compType : String representing parent component type. It could either be <code>Process, Teams, Forums or RSS</code>
     * @param request {@link HttpServletRequest} </code> containing <code>USERNAME</code>: a username of the user who is logged in to Resolve
     * @param response {@link HttpServletResponse}
     * @return {@link ResponseDTO} with success true and records with list of {@link SocialDTO} if everything goes smooth.
     *          Otherwise success will be set to false, message set to the exception message and an exception will be logged into rsview log.
     * @throws ServletException
     * @throws IOException
     * 
     * This method get the list of components present inside a containter (Process, Team and Forum)
     * If the container (CompType) is Process, list of Docs, Runbooks, Actiontaks, Teams, Forums, Rss's and Users is returned.
     * If the container is Team, list of Teams and Users and returned.
     * If the container is Forum, list of Users is returned.
     */
    @RequestMapping(value = "/socialadmin/container/components/get", method = { RequestMethod.GET, RequestMethod.POST }, headers = "Accept=application/json; charset=utf-8")
    @ResponseBody
    public ResponseDTO getContainerComponents(@RequestParam String id, @RequestParam String compType, HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException
    {
        ResponseDTO result = new ResponseDTO();
        String username = (String) request.getAttribute(Constants.HTTP_REQUEST_USERNAME);
        
        try
        {
            Set<SocialDTO> set = ServiceHibernate.getContainerComponents(id, compType, username);
            List<SocialDTO> list = new ArrayList<SocialDTO>(set);
            result.setSuccess(true).setRecords(list);
        }
        catch (Exception e)
        {
            Log.log.error(ERROR_GETTING_CONTAINER_COMPONENTS, e);
            result.setSuccess(false).setMessage(ERROR_GETTING_CONTAINER_COMPONENTS);
        }
        
        return result;
    }
    
    /**
     * Remove components which are part of parent component
     * 
     * @param compList : Array of JSONObject representing components to be removed.
     * @param containerType : String representing parent component type. It could either be <code>Process, Teams, Forums or RSS</code>
     * @param containerId : String representing sysId of the parent component.
     * @param request {@link HttpServletRequest} </code> containing <code>USERNAME</code>: a username of the user who is logged in to Resolve
     * @param response {@link HttpServletResponse}
     * @return {@link ResponseDTO} with success true and optionaly a message will be populated if user tried to remove an admin user from a component. 
     *          Otherwise success will be set to false, message set to the exception message and an exception will be logged into rsview log.
     * @throws ServletException
     * @throws IOException
     */
    @RequestMapping(value = "/socialadmin/container/components/remove", method = { RequestMethod.GET, RequestMethod.POST }, headers = "Accept=application/json; charset=utf-8")
    @ResponseBody
    public ResponseDTO removeContainerComponents(@RequestBody JSONObject[] compList, @RequestParam String containerType, @RequestParam String containerId,
                    HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException
    {
        ResponseDTO result = new ResponseDTO();
        String userName = (String) request.getAttribute(Constants.HTTP_REQUEST_USERNAME);
        List<SocialDTO> socialDTOList = new ArrayList<SocialDTO>();
        
        for (JSONObject jsonOnject : compList)
        {
          //using jackson to deserialize the json
            String json = jsonOnject.toString();
            SocialDTO socialDTO = new ObjectMapper().readValue(json, SocialDTO.class);    
            
            socialDTOList.add(socialDTO);
        }             
        
        try
        {
            String message = ServiceHibernate.removeContainerComponents(containerType, containerId, socialDTOList, userName);
            result.setMessage(message);
        }
        catch (Exception e)
        {
            Log.log.error(ERROR_REMOVING_CONTAINER_COMPONENTS, e);
            result.setSuccess(false).setMessage(ERROR_REMOVING_CONTAINER_COMPONENTS);
        }
        
        return result;
    }
    
    /**
     * Add components to the parent component
     * 
     * @param compList : Array of JSONObject representing component(s) to be added.
     * @param containerType : String representing parent component type. It could either be <code>Process, Teams, Forums or RSS</code>
     * @param containerId : String representing sysId of the parent component.
     * @param addUsersToGroup : Boolean flag representing whether user needs to be added to the group.
     * Applicable only when the parent component type is Team, it's linked to the Group and User is getting added to it. In this case, u_display_name of JSONObject (SocialDTO) is populated with the user name. This flag will be ignored during all other other operations.
     * @param validate : Boolean flag NOT USED anymore.
     * @param request {@link HttpServletRequest} </code> containing <code>USERNAME</code>: a username of the user who is logged in to Resolve
     * @param response {@link HttpServletResponse}
     * @return {@link ResponseDTO} with success true if everything goes smooth. Otherwise success will be set to false, message set to the exception message and an exception will be logged into rsview log.
     *          A ResolveNode record is assumed to be there for the component. Otherwise, exception will be thrown with the message: "Node for the [container Type] SysId: [Container SysId] could not be found." 
     * @throws ServletException
     * @throws IOException
     */
    @RequestMapping(value = "/socialadmin/container/components/add", method = { RequestMethod.GET, RequestMethod.POST }, headers = "Accept=application/json; charset=utf-8")
    @ResponseBody
    public ResponseDTO addComponentsToContainer(@RequestBody JSONObject[] compList, @RequestParam String containerType, @RequestParam String containerId,
                    @RequestParam Boolean addUsersToGroup, @RequestParam Boolean validate,
                    HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException
    {
        ResponseDTO result = new ResponseDTO();
        String userName = (String) request.getAttribute(Constants.HTTP_REQUEST_USERNAME);
        List<SocialDTO> socialDTOList = new ArrayList<SocialDTO>();
        
        for (JSONObject jsonOnject : compList)
        {
          //using jackson to deserialize the json
            String json = jsonOnject.toString();
            SocialDTO socialDTO = new ObjectMapper().readValue(json, SocialDTO.class);
            
            socialDTOList.add(socialDTO);
        }
        
        try
        {
            List<String> missingComps = ServiceHibernate.addComponentToContainer(containerType, containerId, socialDTOList, userName, addUsersToGroup, validate);
            result.setSuccess(true);
            
            if (socialDTOList.size() > 0 && missingComps.size() < socialDTOList.size())
            {
                result.setSuccess(true);
                result.setMessage("Resolve does not contain node for " + missingComps.size() + 
                                  " of the " + socialDTOList.size() + 
                                  " components selected for adding into " + containerType + 
                                  ", components (type: name) not added to " + containerType + 
                                  " are " + missingComps.toString() + ".");
            }
            else if (socialDTOList.size() > 0 && missingComps.size() == socialDTOList.size())
            {
                result.setSuccess(false);
                result.setMessage("Failed to add any of the selected components to " + containerType + 
                                  " due to node not found.");
            }
            else
            {
                result.setSuccess(true);
            }
        }
        catch (Exception e)
        {
            Log.log.error(ERROR_ADDING_CONTAINER_COMPONENTS, e);
            result.setSuccess(false).setMessage(ERROR_ADDING_CONTAINER_COMPONENTS);
        }
        
        return result;
    }
    
    /**
     * Deletes a component.
     * 
     * @param ids : String array of component sysIds to be deleted.
     * @param compType : String representing component type. It could either be <code>Process, Teams, Forums or RSS</code>
     * @param request {@link HttpServletRequest} </code> containing <code>USERNAME</code>: a username of the user who is logged in to Resolve
     * @param response {@link HttpServletResponse}
     * @return {@link ResponseDTO} with success true if everything goes smooth. Otherwise success will be set to false, message set to the exception message and an exception will be logged into rsview log.
     * @throws ServletException
     * @throws IOException
     */
    @RequestMapping(value = "/socialadmin/component/delete", method = { RequestMethod.GET, RequestMethod.POST }, headers = "Accept=application/json; charset=utf-8")
    @ResponseBody
    public ResponseDTO delete(@RequestParam String[] ids, @RequestParam String compType, HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException
    {
        ResponseDTO result = new ResponseDTO();
        String username = (String) request.getAttribute(Constants.HTTP_REQUEST_USERNAME);
        
        try
        {
            if(compType.equalsIgnoreCase(AdvanceTree.PROCESS))
            {
                ServiceHibernate.deleteProcesses(ids, username);
            }
            else if(compType.equalsIgnoreCase(AdvanceTree.TEAMS))
            {
                ServiceHibernate.deleteTeam(ids, username);
            }
            else if(compType.equalsIgnoreCase(AdvanceTree.FORUMS))
            {
                ServiceHibernate.deleteForum(ids, username);
            }
            else if(compType.equalsIgnoreCase(AdvanceTree.RSS))
            {
                ServiceHibernate.deleteRss(ids, username);
            }
            result.setSuccess(true);
        }
        catch (Exception e)
        {
            Log.log.error(ERROR_DELETING_COMPONENT, e);
            result.setSuccess(false).setMessage(ERROR_DELETING_COMPONENT);
        }

        return result;
    }
    
    /**
     * Get the default access rights for the specified component type.
     * 
     * @param compType : String representing parent component type. It could either be <code>Process, Teams, Forums or RSS</code>
     * @param request {@link HttpServletRequest} </code> containing <code>USERNAME</code>: a username of the user who is logged in to Resolve
     * @param response {@link HttpServletResponse}
     * @return {@link ResponseDTO} with success true and data set to {@link SocialDTO} where edir, read and post roles are populated depending upon the component type.
     *          This API returnd a static information, so no exception is cought.
     * @throws ServletException
     * @throws IOException
     */
    @RequestMapping(value = "/socialadmin/defaultAccessRights/get", method = { RequestMethod.GET, RequestMethod.POST }, headers = "Accept=application/json; charset=utf-8")
    @ResponseBody
    public ResponseDTO getDefaultAccessRights(@RequestParam String compType, HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException
    {
        ResponseDTO result = new ResponseDTO();
        
        SocialDTO dto = ServiceHibernate.getDefaultAccessRights(compType);
        result.setData(dto);
        
        return result;
    }
    
    /**
     * List different components which could be added to the parent component
     * 
     * @param query : {@link QueryDTO}, represents the query, pagination, sorting, grouping of the list
     * @param parentName : String representing parent component type. It could either be <code>Process, Teams or Forums</code>
     * @param parentId : String representing sysId of the parent component.
     * @param request {@link HttpServletRequest} </code> containing <code>USERNAME</code>: a username of the user who is logged in to Resolve
     * @param response {@link HttpServletResponse}
     * @return {@link ResponseDTO} with success true, records set to the list of components which could be added to the parent component.
     *          Otherwise success will be set to false, message will be set to the exception error message and an exception will be logged into rsview log.
     * @throws ServletException
     * @throws IOException
     */
    @RequestMapping(value = "/socialadmin/addcomponent/list", method = { RequestMethod.GET, RequestMethod.POST }, headers = "Accept=application/json; charset=utf-8")
    @ResponseBody
    public ResponseDTO compList(@ModelAttribute QueryDTO query, @RequestParam String parentName, @RequestParam String parentId, HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException
    {
        String userName = (String) request.getAttribute(Constants.HTTP_REQUEST_USERNAME);
        
        ResponseDTO result = ServiceHibernate.getListOfComponentsToBeAdded(query, parentName, parentId, userName);
        return result;
    }
}
