package com.resolve.rsview.sso.saml;

public enum SAMLAuthenticatorType
{
    
    NULL, GENERIC_SAML, ADFS;
    
}
