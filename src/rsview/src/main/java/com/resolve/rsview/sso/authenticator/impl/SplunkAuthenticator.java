package com.resolve.rsview.sso.authenticator.impl;

import java.util.Arrays;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.resolve.rsview.auth.Authentication;
import com.resolve.rsview.sso.authenticator.SSOAuthenticationDTO;
import com.resolve.rsview.sso.authenticator.SSOAuthenticator;
import com.resolve.rsview.sso.util.SSOUtil;
import com.resolve.services.hibernate.vo.UsersVO;
import com.resolve.services.util.PropertiesUtil;
import com.resolve.services.util.UserUtils;
import com.resolve.util.ERR;
import com.resolve.util.Log;
import com.resolve.util.StringUtils;
import com.resolve.util.SyslogFormatter;
import com.splunk.HttpException;
import com.splunk.HttpService;
import com.splunk.SSLSecurityProtocol;
import com.splunk.Service;
import com.splunk.User;

public class SplunkAuthenticator implements SSOAuthenticator
{

    public static final String SPLUNK_USERNAME_PARAM_NAME = "USERNAME";
    public static final String SPLUNK_CREDTOKEN_PARAM_NAME = "CREDTOKEN";

    public static final String SPLUNK_INSTANCE_HOST_KEY = "sso.splunk.instanceHost";
    public static final String SPLUNK_INSTANCE_PORT_KEY = "sso.splunk.instancePort";

    private static final String SPLUNK_SSO_DESCRIPTION = "Splunk SSO";
    private static final String SPLUNK_SSO_AUTH_FAILURE_MESSAGE_PREFIX = "Splunk SSO authentication failed : ";
    private static final String SPLUNK_SSO_AUTH_SUCCESS_MESSAGE_PREFIX = "Splunk SSO successfully authenticated : ";

    @Override
    public String getSSOType()
    {
        return "SPLUNK";
    }

    @Override
    public SSOAuthenticationDTO authenticate(HttpServletRequest request, HttpServletResponse response)
    {

        SSOAuthenticationDTO result = null;
        boolean propertiesNotAvailable = StringUtils.isBlank(PropertiesUtil.getPropertyString(SPLUNK_INSTANCE_HOST_KEY)) || StringUtils.isBlank(PropertiesUtil.getPropertyString(SPLUNK_INSTANCE_PORT_KEY));
        if (propertiesNotAvailable)
        {
            String logMessage = SPLUNK_SSO_AUTH_FAILURE_MESSAGE_PREFIX + "Failed to retrieve the system properties for " + SPLUNK_SSO_DESCRIPTION;
            Log.auth.warn(logMessage);
            Log.syslog.warn(SyslogFormatter.INSTANCE.formatAudit(Authentication.getRemoteAddrForSysLog(request), "[user unknown]", request.getLocalAddr(), logMessage));
            return result;
        }
        String instanceHost = PropertiesUtil.getPropertyString(SPLUNK_INSTANCE_HOST_KEY).trim();
        int port = Integer.valueOf(PropertiesUtil.getPropertyString(SPLUNK_INSTANCE_PORT_KEY).trim());
        String protocol = "https";
        String credToken = request.getParameter(SPLUNK_CREDTOKEN_PARAM_NAME);

        Service service = getServiceUsingTokenAuth(instanceHost, port, protocol, credToken);

        if (service != null)
        {
            String splunkToken = service.getToken();
            if (splunkToken != null)
            {
                Log.auth.trace(String.format("SSO credential token: %s", credToken));

                // String username = service.getUsername();
                String username = request.getParameter(SPLUNK_USERNAME_PARAM_NAME);
                
                if (StringUtils.isNotBlank(username)) {
	                User user = null;
	                try
	                {
	                    for (User currentUser : service.getUsers().values())
	                    {
	                        if (currentUser.getName().equals(username))
	                        {
	                            user = currentUser;
	                            break;
	                        }
	                    }
	                    	                    
	                    if (user != null)
	                    {
	                        String[] roles = user.getRoles();
	                        List<String> rolesList = Arrays.asList(roles);
	                        Log.log.debug(String.format("Splunk SSO (username: %s, name: %s, email: %s, Splunk Roles: %s)", 
	                        							user.getName(), user.getRealName(), user.getEmail(), 
	                        							StringUtils.convertCollectionToString(rolesList.iterator(), ",")));
	                        
	                        UsersVO userVO = UserUtils.getUser(username);
	                        boolean clonedSuccessfully = false;
	                        if (userVO == null)
	                        {
	                            try {
									clonedSuccessfully = SSOUtil.INSTANCE.createUserAndAssociateGroup(username, rolesList, 
																									  getSSOType());
								} catch (Exception e) {
									String logMessage = 
												String.format(SSO_AUTH_FAILURE_TO_CREATE_USER_AND_ASSOCIATE_GROUP_MESSAGE, 
															  getSSOType(),
															  (StringUtils.isNotBlank(e.getMessage()) ? 
															   e.getMessage() : "")); 
						            Log.auth.warn(logMessage);
						            Log.syslog.warn(
						            	SyslogFormatter.INSTANCE.formatAudit(Authentication.getRemoteAddrForSysLog(request), 
						            										 "[user " + username + "]", 
						            										 request.getLocalAddr(), logMessage));
						            
						            if (StringUtils.isNotBlank(e.getMessage()) &&
						            	(e.getMessage().equalsIgnoreCase(ERR.E10022.getMessage()) ||
						            	 e.getMessage().equalsIgnoreCase(ERR.E10023.getMessage()))) {
						            	result = new SSOAuthenticationDTO(ERR.getERRFromMessage(e.getMessage()));
						            }
						            
						            return result;
								}
	                            
	                            if (clonedSuccessfully)
	                            {
	                                String logMessage = String.format("Cloned %s user %s into Resolve", 
	                                								  SPLUNK_SSO_DESCRIPTION, 
	                                								  username);
	                                Log.auth.debug(logMessage);
	                                Log.syslog.info(SyslogFormatter.INSTANCE.formatAudit(
	                                									Authentication.getRemoteAddrForSysLog(request), 
	                                									username, request.getLocalAddr(), logMessage));
	                            }
	                            else
	                            {
	                                String logMessage = String.format(SPLUNK_SSO_AUTH_FAILURE_MESSAGE_PREFIX + 
	                                								  "Failed to clone user %s into Resolve", username);
	                                Log.auth.warn(logMessage);
	                                Log.syslog.warn(SyslogFormatter.INSTANCE.formatAudit(
	                                										Authentication.getRemoteAddrForSysLog(request),
	                                										username, request.getLocalAddr(), logMessage));
	                            }
	                        }
	                        else
	                        {
	                            boolean isLockedOut = UserUtils.isLockedOut(username);
	                            if (isLockedOut)
	                            {
	                                    String logMessage = String.format(SPLUNK_SSO_AUTH_FAILURE_MESSAGE_PREFIX + 
	                                    								  "User %s is locked", username);
	                                    Log.auth.warn(logMessage);
	                                    Log.syslog.warn(SyslogFormatter.INSTANCE.formatAudit(
	                                    									Authentication.getRemoteAddrForSysLog(request),
	                                    									username, request.getLocalAddr(), logMessage));
	                                return null;
	                            }
	                            else
	                            {
	                                    Log.auth.debug(String.format("Splunk user matching in Resolve found, " +
	                                    							 "proceed with login: %s.", username));
	                            }
	                        }
	                        
	                        result = new SSOAuthenticationDTO(username, rolesList);
	                    }
	                    else
	                    {
	                        Log.auth.debug("Splunk user not found with username: " + username);
	                    }
	                }
	                catch (HttpException httpe)
	                {
	                    Log.auth.warn(String.format("Splunk authentication exception: %s.", httpe.getLocalizedMessage()));
	                    // TODO syslog auth failure
	                }
                }
                else
                {
                	Log.log.warn("Splunk SSO authentication request is missing " + SPLUNK_USERNAME_PARAM_NAME + 
                				 " parameter or value of " + SPLUNK_USERNAME_PARAM_NAME + " is blnak.");
                }
            }
            else
            {
                String logMessage = SPLUNK_SSO_AUTH_FAILURE_MESSAGE_PREFIX + 
                					"Could not authenticate with Splunk using the CREDTOKEN passed";
                Log.auth.warn(logMessage);
                Log.syslog.warn(SyslogFormatter.INSTANCE.formatAudit(Authentication.getRemoteAddrForSysLog(request), 
                													 "[user unknown]", request.getLocalAddr(), logMessage));
            }

            // service.logout();
        }
        else
        {
            String logMessage = SPLUNK_SSO_AUTH_FAILURE_MESSAGE_PREFIX + "Could not create a Splunk Service";
            Log.auth.warn(logMessage);
            Log.syslog.warn(SyslogFormatter.INSTANCE.formatAudit(Authentication.getRemoteAddrForSysLog(request), 
            				"[user unknown]", request.getLocalAddr(), logMessage));
        }
        return result;
    }

    public static Service getServiceUsingTokenAuth(String instanceHost, int port, String protocol, String credToken)
    {
        HttpService.setSslSecurityProtocol(SSLSecurityProtocol.TLSv1_2);
        Service service = new Service(instanceHost, port, protocol);
        service.setToken(String.format("Splunk %s", credToken));
        return service;
    }

    public static Service getServiceUsingBasicAuth(String instanceHost, int port, String protocol, String username, 
    											   String password)
    {
        HttpService.setSslSecurityProtocol(SSLSecurityProtocol.TLSv1_2);
        // Service service = new Service("10.40.1.137", 8089, "https");
        Service service = new Service(instanceHost, port, protocol);
        service.login(username, password);

        // Print the session token
        // System.out.println("Your session token: " + service.getToken());

        service.login();
        return service;
    }

}
