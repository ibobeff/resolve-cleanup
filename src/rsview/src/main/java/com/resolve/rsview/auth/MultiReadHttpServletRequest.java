/******************************************************************************
 * (C) Copyright 2014
 *
 * Resolve Systems, LLC
 *
 * All rights reserved
 * This software is distributed under license and may not be used, copied,
 * modified, or distributed without the express written permission of
 * Resolve Systems, LLC.
 *
 ******************************************************************************/

package com.resolve.rsview.auth;

import java.io.BufferedReader;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import java.nio.charset.StandardCharsets;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Enumeration;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.StringTokenizer;

import javax.servlet.ReadListener;
import javax.servlet.ServletInputStream;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletRequestWrapper;

import org.apache.commons.io.IOUtils;
import org.apache.http.HttpHeaders;

import com.resolve.util.Constants;
import com.resolve.util.JspUtils;
import com.resolve.util.Log;
import com.resolve.util.StringUtils;

public class MultiReadHttpServletRequest extends HttpServletRequestWrapper
{	
    private ByteArrayOutputStream cachedBytes;
    private String queryStringSuffix;
    private String pseudoReferer;
    private Map<String, String[]> requestParamMap = new HashMap<String, String[]>();

    public MultiReadHttpServletRequest(HttpServletRequest request)
    {
        super(request);
        
        if (StringUtils.isNotBlank(request.getParameter(HttpHeaders.REFERER)))
        {
            pseudoReferer = request.getParameter(HttpHeaders.REFERER);
        }
        
        if (StringUtils.isBlank(pseudoReferer) && 
            StringUtils.isNotBlank(request.getParameter(HttpHeaders.REFERER.toLowerCase())))
        {
            pseudoReferer = request.getParameter(HttpHeaders.REFERER.toLowerCase());
        }
        
        requestParamMap = request.getParameterMap();
    }

    @Override
    public ServletInputStream getInputStream() throws IOException
    {
        if (cachedBytes == null) cacheInputStream();

        return new CachedServletInputStream();
    }

    @Override
    public BufferedReader getReader() throws IOException
    {
        return new BufferedReader(new InputStreamReader(getInputStream()));
    }

    private void cacheInputStream() throws IOException
    {
        /*
         * Cache the inputstream in order to read it multiple times. For
         * convenience, I use apache.commons IOUtils
         */
        cachedBytes = new ByteArrayOutputStream();
        IOUtils.copy(super.getInputStream(), cachedBytes);
    }

    /* An input stream which reads the cached request body */
    public class CachedServletInputStream extends ServletInputStream
    {
        private ByteArrayInputStream input;
        @SuppressWarnings("unused")
        private ReadListener readListener;

        public CachedServletInputStream()
        {
            /* create a new input stream from the cached request body */
            input = new ByteArrayInputStream(cachedBytes.toByteArray());
        }

        @Override
        public int read() throws IOException
        {
            return input.read();
        }

        // @Override
        public void setReadListener(ReadListener readListener)
        {
            this.readListener = readListener;
        }

        // @Override
        public boolean isFinished()
        {
            return true;
        }

        // @Override
        public boolean isReady()
        {
            return true;
        }
    }
    
    public void setQueryStringSuffix(String queryStringSuffix)
    {
        this.queryStringSuffix = queryStringSuffix;
    }
    
    @Override
    public String getQueryString()
    {
        /*
         * Filter out CSRFToken and _token_ params (if present) from   
         * returned query string as they are not read from URL and
         * are not required to be present in redirected URL
         */
        
        StringBuilder baseQueryStringSb = new StringBuilder();
        
        Map<String, String[]> baseQueryParams = super.getParameterMap();
        
        if (baseQueryParams != null && !baseQueryParams.isEmpty())
        {
            String csrfTokenName = JspUtils.getCSRFTokenName();
            
            for (String baseQueryParamName : baseQueryParams.keySet())
            {
                if (!baseQueryParamName.equals(csrfTokenName) &&
                    !baseQueryParamName.equals(Constants.HTTP_REQUEST_INTERNAL_TOKEN))
                {
                    String[] baseQueryParamValues = baseQueryParams.get(baseQueryParamName);
                    
                    Set<String> uniqueBaseQueryParamValues = new HashSet<String>(Arrays.asList(baseQueryParamValues));
                    
                    for (String uniaqueBaseQueryParamValue : uniqueBaseQueryParamValues)
                    {
                        if (baseQueryStringSb.length() > 0)
                        {
                            baseQueryStringSb.append(AuthConstants.QUERY_PARAMETER_SEPARATOR.getValue());
                        }
                        
                        baseQueryStringSb.append(baseQueryParamName).
                        append(AuthConstants.QUERY_PARAMETER_EQUAL_TO.getValue()).append(uniaqueBaseQueryParamValue);
                    }
                }
            }
        }
        
        String qryStr = baseQueryStringSb.toString() + 
                        (StringUtils.isNotBlank(queryStringSuffix) ? 
                         (baseQueryStringSb.length() > 0 ? AuthConstants.QUERY_PARAMETER_SEPARATOR.getValue() + 
                        		 						   queryStringSuffix : queryStringSuffix) : 
                         "");
        
        if (StringUtils.isNotBlank(qryStr))
        {
            try
            {
                return URLEncoder.encode(qryStr, StandardCharsets.UTF_8.name());
            }
            catch(UnsupportedEncodingException e)
            {
                Log.auth.warn("Error " + e.getLocalizedMessage() + " occured while encoding query string " +
                              qryStr + " using " + StandardCharsets.UTF_8.name() + " character set," +
                              " returning un-encoded query string!!!", e);
                return qryStr;
            }
        }
        else
        {
            return qryStr;
        }
    }
    
    @Override
    public String getHeader(String name)
    {
        if (name.equalsIgnoreCase(HttpHeaders.REFERER))
        {
        	return StringUtils.isNotBlank(pseudoReferer) ? pseudoReferer : super.getHeader(name);
        }
        else
        {
            String header = super.getHeader(name);
            
            if (StringUtils.isBlank(header))
            {
                if (requestParamMap.containsKey(name))
                {
                    header = requestParamMap.get(name)[0];
                }
                
                if (StringUtils.isBlank(header) && 
                    name.startsWith(AuthConstants.RESOLVE_PARAM_AS_HEADER_HEADER_NAME_PREFIX.getValue()))
                {
                    String nameWithoutPrefix = 
                    			name.substring(
                    					AuthConstants.RESOLVE_PARAM_AS_HEADER_HEADER_NAME_PREFIX.getValue().length());
                    
                    header = super.getHeader(nameWithoutPrefix);
                    
                    if (StringUtils.isBlank(header))
                    {
                        if (requestParamMap.containsKey(nameWithoutPrefix))
                        {
                            header = requestParamMap.get(nameWithoutPrefix)[0];
                        }
                        else if (requestParamMap.containsKey(name))
                        {
                            header = requestParamMap.get(name)[0];
                        }
                    }
                }
            }
            
            return header;
        }
    }
    
    @SuppressWarnings({ "unchecked", "rawtypes" })
    @Override 
    public Enumeration getHeaders(String name)
    {
        Enumeration headers = super.getHeaders(name);
        
        if (headers == null || !headers.hasMoreElements())
        {
            if (requestParamMap.containsKey(name))
            {
                StringBuilder sb = new StringBuilder();
                
                for (int i = 0; i < (requestParamMap.get(name)).length; i++)
                {
                    if (sb.length() > 0)
                    {
                        sb.append(AuthConstants.MULTIPLE_PARAM_VALUES_DELIMITER.getValue());
                    }
                    
                    sb.append(requestParamMap.get(name)[i]);
                }
                
                headers = new StringTokenizer(sb.toString(), AuthConstants.MULTIPLE_PARAM_VALUES_DELIMITER.getValue());
            }
            
            if ((headers == null || !headers.hasMoreElements()) &&
                name.startsWith(AuthConstants.RESOLVE_PARAM_AS_HEADER_HEADER_NAME_PREFIX.getValue()))
            {
                String nameWithoutPrefix = 
                			name.substring(AuthConstants.RESOLVE_PARAM_AS_HEADER_HEADER_NAME_PREFIX.getValue().length());
                
                headers = super.getHeaders(nameWithoutPrefix);
                
                if (headers == null || !headers.hasMoreElements())
                {
                    if (requestParamMap.containsKey(nameWithoutPrefix))
                    {
                        if (requestParamMap.get(nameWithoutPrefix) != null && 
                        	(requestParamMap.get(nameWithoutPrefix)).length >= 1)
                        {
                            StringBuilder sb = new StringBuilder();
                            
                            for (int i = 0; i < (requestParamMap.get(nameWithoutPrefix)).length; i++)
                            {
                                if (sb.length() > 0)
                                {
                                    sb.append(AuthConstants.MULTIPLE_PARAM_VALUES_DELIMITER.getValue());
                                }
                                
                                sb.append(requestParamMap.get(nameWithoutPrefix)[i]);
                            }
                            
                            headers = new StringTokenizer(sb.toString(), 
                            							  AuthConstants.MULTIPLE_PARAM_VALUES_DELIMITER.getValue());
                        }                            
                    }
                }
            }
        }
        
        return headers;
    }
    
    @SuppressWarnings({ "unchecked", "rawtypes" })
    @Override
    public Enumeration getHeaderNames()
    {
        StringBuilder sb = new StringBuilder();
        
        Enumeration<String> baseHeaderNames = super.getHeaderNames();
        
        while (baseHeaderNames.hasMoreElements())
        {
            if (sb.length() > 0)
            {
                sb.append(AuthConstants.COMMA_DELIMITER.getValue());
            }
            
            sb.append(baseHeaderNames.nextElement());
        }
        
        if (requestParamMap != null && !requestParamMap.isEmpty())
        {
            for (String requestParamName : requestParamMap.keySet())
            {
                if (sb.length() > 0)
                {
                    sb.append(AuthConstants.COMMA_DELIMITER.getValue());
                }
                
                sb.append(
                		requestParamName.startsWith(AuthConstants.RESOLVE_PARAM_AS_HEADER_HEADER_NAME_PREFIX.getValue()) ? 
                        requestParamName : AuthConstants.RESOLVE_PARAM_AS_HEADER_HEADER_NAME_PREFIX.getValue() + 
                        				   requestParamName);
            }
        }
        
        return new StringTokenizer(sb.toString(), AuthConstants.COMMA_DELIMITER.getValue());
    }

    // Get URL containing only query string from the URL NOT from form data
    String getURL() {
        try
        {
            String uri = URLEncoder.encode(getRequestURI(), StandardCharsets.UTF_8.name());
            String queryString = super.getQueryString();
            return StringUtils.isNotBlank(queryString)? (uri + "?" + queryString) : uri;
        }
        catch (Exception e)
        {
            Log.auth.error(e.getLocalizedMessage(), e);
            return null;
        }
    }
}