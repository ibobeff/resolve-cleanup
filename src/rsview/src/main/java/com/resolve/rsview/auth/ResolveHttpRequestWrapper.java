package com.resolve.rsview.auth;

import java.util.Collections;
import java.util.Enumeration;
import java.util.Map;
import java.util.TreeMap;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletRequestWrapper;

public class ResolveHttpRequestWrapper extends HttpServletRequestWrapper
{
    private final Map<String, String[]> modifiableParameters;
    public ResolveHttpRequestWrapper(final HttpServletRequest request, final Map<String, String[]> additionalParams)
    {
        super(request);
        modifiableParameters = new TreeMap<String, String[]>();
        modifiableParameters.putAll(request.getParameterMap());
        modifiableParameters.putAll(additionalParams);
    }
    
    @Override
    public Map<String, String[]> getParameterMap()
    {
        //Return an unmodifiable collection because we need to uphold the interface contract.
        return Collections.unmodifiableMap(modifiableParameters);
    }
    
    
    @Override
    public String getParameter(final String name)
    {
        String[] strings = getParameterMap().get(name);
        if (strings != null)
        {
            return strings[0];
        }
        return super.getParameter(name);
    }
    
    @Override
    public Enumeration<String> getParameterNames()
    {
        return Collections.enumeration(getParameterMap().keySet());
    }

    @Override
    public String[] getParameterValues(final String name)
    {
        return getParameterMap().get(name);
    }
    
}
