/******************************************************************************
* (C) Copyright 2014
*
* Resolve Systems, LLC
*
* All rights reserved
* This software is distributed under license and may not be used, copied,
* modified, or distributed without the express written permission of
* Resolve Systems, LLC.
*
******************************************************************************/

package com.resolve.rsview.model;

import java.util.Map;

import com.resolve.services.util.PropertiesUtil;
import com.resolve.util.StringUtils;


public class Login
{
    private String username;
    private String password;
    private String url;
    private String error;
    private String allowRecovery = "inline";
    private String showError = "none";
    private String hashState = "";
    private String iframeId;
    private String sSOServiceId;
    private Map<String, String[]> sSOServiceData;
    private String sSOSrvcLoginBtnLbl;

    public Login(String url)
    {
        this.url = url;
    } // Login

    public String getUsername()
    {
        return username;
    }

    public void setUsername(String username)
    {
        this.username = username;
    }

    public String getPassword()
    {
        return password;
    }

    public void setPassword(String password)
    {
        this.password = password;
    }

    public String getUrl()
    {
        return url;
    }

    public void setUrl(String url)
    {
        this.url = url;
    }

    public String getError()
    {
        return error;
    }

    public void setError(String error)
    {
        this.error = error;
        if (StringUtils.isNotBlank(error))
        {
            this.setShowError(true);
        }
    }

    public String getAllowRecovery()
    {
        return allowRecovery;
    }

    public void setAllowRecovery(boolean allowRecovery)
    {
        if (allowRecovery)
        {
            this.allowRecovery = "inline";
        }
        else
        {
            this.allowRecovery = "none";
        }
    }

    public String getShowError()
    {
        return showError;
    }

    public void setShowError(boolean showError)
    {
        if (showError)
        {
            this.showError = "block";
        }
        else
        {
            this.showError = "none";
        }
    }

    public String getHashState()
    {
        return hashState;
    }

    public void setHashState(String hashState)
    {
        this.hashState = hashState;
    }

    public String getAllowRememberMe()
    {
        if( StringUtils.getBoolean(PropertiesUtil.getPropertyString("users.login.allowRememberMe")) ) {
            return "inline";
        }
        return "none";
    }

    public String getIframeId()
    {
        return iframeId;
    }
    
    public void setIframeId(String iframeId)
    {
        this.iframeId = iframeId;
    }
    
    public String getSSOServiceId()
    {
        return sSOServiceId;
    }
    
    public void setSSOServiceId(String sSOServiceId)
    {
        this.sSOServiceId = sSOServiceId;
    }
    
    public Map<String, String[]> getSSOServiceData()
    {
        return sSOServiceData;
    }
    
    public void setSSOServiceData(Map<String, String[]> sSOServiceData)
    {
        this.sSOServiceData = sSOServiceData;
    }
    
    public String getSSOSrvcLoginBtnLbl()
    {
        return sSOSrvcLoginBtnLbl;
    }
    
    public void setSSOSrvcLoginBtnLbl(String sSOSrvcLoginBtnLbl)
    {
        this.sSOSrvcLoginBtnLbl = sSOSrvcLoginBtnLbl;
    }
}
