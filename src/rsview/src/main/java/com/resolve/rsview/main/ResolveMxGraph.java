/******************************************************************************
* (C) Copyright 2014
*
* Resolve Systems, LLC
*
* All rights reserved
* This software is distributed under license and may not be used, copied,
* modified, or distributed without the express written permission of
* Resolve Systems, LLC.
*
******************************************************************************/

package com.resolve.rsview.main;

import java.io.File;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.resolve.util.FileUtils;

public class ResolveMxGraph 
{
	private static Map<String, List<String>> imageFiles = new HashMap();
	
	public synchronized void init()
	{
		String imagesHome = RSContext.getWebHome()+"jsp/model/images/resolve";
	
		File resolveImages = FileUtils.getFile(imagesHome); 
		
		if(resolveImages != null)
		{
			String[] resolveImageDirs = resolveImages.list();
			
			if(resolveImageDirs != null && resolveImageDirs.length >= 0)
			{
				for(String resolveImageDir: resolveImageDirs)
				{
					String imageDirLocal = resolveImageDir;
					
					if(imageDirLocal != null && !imageDirLocal.equals(""))
					{
						File fileUnderImageDirLocal = FileUtils.getFile(imagesHome + "/" + imageDirLocal);
						File[] filesArray = fileUnderImageDirLocal.listFiles();
						List<String> fileNameList = new ArrayList();
						
						if(filesArray != null && filesArray.length >= 0)
						{
							for(File file: filesArray)
							{
								if(!file.isDirectory())
								{
									String fileName = file.getName();
									fileNameList.add(fileName);
								}
							}
							
							if(!fileNameList.isEmpty())
							{
								imageFiles.put(imageDirLocal, fileNameList);
							}
						}
					}
				}
			}
		}
	}
	
	public static Map<String, List<String>> getImageFiles()
	{
		return imageFiles;
	}
	
}
