/******************************************************************************
* (C) Copyright 2014
*
* Resolve Systems, LLC
*
* All rights reserved
* This software is distributed under license and may not be used, copied,
* modified, or distributed without the express written permission of
* Resolve Systems, LLC.
*
******************************************************************************/

package com.resolve.rsview.util;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.resolve.util.Log;
import com.resolve.util.StringUtils;

public class RenderJspUtil
{

    /**
     * 
     * @param jspName - /client/module-loader.jsp
     * @param request
     * @param response
     * @return
     */
    public static String renderJsp(String jspName, HttpServletRequest request, HttpServletResponse response)
    {
        String html = "";
        
        if (StringUtils.isNotBlank(jspName))
        {
            CharArrayWriterResponse customResponse = new CharArrayWriterResponse(response);
            try
            {
                request.getRequestDispatcher(jspName).include(request, customResponse);
                html = customResponse.getOutput();
            }
            catch (Exception e)
            {
                Log.log.error(e);
            }
        }
        return html;
         
    }
    
    
}
