/******************************************************************************
* (C) Copyright 2014
*
* Resolve Systems, LLC
*
* All rights reserved
* This software is distributed under license and may not be used, copied,
* modified, or distributed without the express written permission of
* Resolve Systems, LLC.
*
******************************************************************************/

package com.resolve.rsview.controller;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Set;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.stereotype.Controller;
import org.springframework.stereotype.Service;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.resolve.rsview.main.MIndex;
import com.resolve.search.actiontask.ActionTaskIndexAPI;
import com.resolve.search.property.PropertyIndexAPI;
import com.resolve.services.ServiceHibernate;
import com.resolve.services.hibernate.util.ActionTaskPropertiesUtil;
import com.resolve.services.hibernate.vo.ResolvePropertiesVO;
import com.resolve.services.vo.QueryDTO;
import com.resolve.services.vo.ResponseDTO;
import com.resolve.util.Constants;
import com.resolve.util.Log;


/**
 * 
 * This controller serves the request(CRUD operations) for Action Task Properties. 
 * <p>
 * Actiontask Properties are used in Actiontask to set the values during runtime. 
 * <p> 
 * For more information about that, please refer to <a href="http://community.gen-e.com/resolve/jsp/rsclient.jsp">Resolve documentation</a>. 
 * <p>
 * DB Table: {@code resolve_properties} </br>
 * Url : {@code http://localhost:8080/resolve/jsp/rsclient.jsp#RS.actiontask.PropertyDefinitions/}
 * 
 * 
 * @author jeet.marwah
 *
 */
@Controller
@Service
public class AjaxActionTaskProperties extends GenericController
{
    /**
     * 
     * Returns/Retrieves list of Actiontask Properties for the grid based on filters, sorts, pagination. 
     * <p> 
     * <u><b>Example</b></u>:</br> 
     * <code><pre>
     *  filter: [{"field":"uname","type":"auto","condition":"contains","value":"AN"}]
     *  page: 3
     *  start: 100
     *  limit: 50
     *  sort: [{"property":"uname","direction":"ASC"}]
     * </pre></code>
     *  
     * <p>
     * Parameters in the Request object: 
     * <code><pre>
     *  USERNAME - username of the user who is logged in to Resolve
     * </pre></code>
     *  
     * <p>
     * Return object in response:
     * <code><pre>
     *  'records' : List of type {@link ResolvePropertiesVO}
     *  'total' : # of total records in the table/db 
     * </pre></code>
     * 
     *       
     * @param query  {@link QueryDTO}, represents the query, pagination, sorting, grouping for the UI
     * @param request  HttpServletRequest
     * @param response  HttpServletResponse
     * @return {@link ResponseDTO} - Reponse Data Transfer Object having a List  and total # of records 
     * @throws ServletException
     * @throws IOException
     */
    @RequestMapping(value = "/atproperties/list", method = { RequestMethod.GET, RequestMethod.POST }, headers = "Accept=application/json; charset=utf-8")
    @ResponseBody
    public ResponseDTO list(@ModelAttribute QueryDTO query, HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException
    {
        ResponseDTO result = new ResponseDTO();
        String username = (String) request.getAttribute(Constants.HTTP_REQUEST_USERNAME);
        try
        {
            query.setModelName("ResolveProperties");
            
            List<ResolvePropertiesVO> data = ServiceHibernate.getResolveProperties(query, username);
            int total = ServiceHibernate.getTotalHqlCount(query);
            
            result.setSuccess(true).setRecords(data);
            result.setTotal(total);
        }
        catch (Exception e)
        {
            Log.log.error("Error retrieving Properties", e);
            result.setSuccess(false).setMessage("Error retrieving Properties. See log for more info.");
        }
        
        return result;
    }
    
    
    /**
     * This api returns a Set of Properties Module Names which is presently used in a drop down on the UI.
     * 
     * <p>
     * Parameters in the Request object:
     * <code><pre> 
     *  USERNAME - username of the user who is logged in to Resolve - CURRENTLY THIS IS NOT USED
     * </pre></code>
     * <p>
     * Return object in response 
     * <code><pre>
     *  'records' : Set of type String
     * </pre></code>
     * 
     * 
     * @param request  HttpServletRequest
     * @param response  HttpServletResponse
     * @return {@link ResponseDTO} - Reponse Data Transfer Object having a Set of Module names
     * @throws ServletException
     * @throws IOException
     */
    @RequestMapping(value = "/atproperties/modules", method = { RequestMethod.GET, RequestMethod.POST }, headers = "Accept=application/json; charset=utf-8")
    @ResponseBody
    public ResponseDTO getModules(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException
    {
        ResponseDTO result = new ResponseDTO();
        String username = (String) request.getAttribute(Constants.HTTP_REQUEST_USERNAME);
        try
        {
            Set<String> data = ServiceHibernate.getAllAvailablePropertyModules();
            result.setSuccess(true).setRecords(new ArrayList<String>(data));
        }
        catch (Exception e)
        {
            Log.log.error("Error retrieving Property Module", e);
            result.setSuccess(false).setMessage("Error retrieving Property Module" + e.getMessage());
        }
        
        return result;
    }
    

    /**
     * 
     * Get Actiontask Property by sysId
     * 
     * <p>
     * Parameters in the Request object:
     * <code><pre> 
     *  USERNAME - username of the user who is logged in to Resolve
     * </pre></code>
     * <p>
     * Return object in response 
     * <code><pre>
     *  'data' : {@link ResolvePropertiesVO}
     * </pre></code>
     * 
     * @param id sysId of the Actiontask Property
     * @param request HttpServletRequest
     * @param response HttpServletResponse
     * @return {@link ResponseDTO} - Reponse Data Transfer Object with 'data' of type {@link ResolvePropertiesVO}
     * @throws ServletException
     * @throws IOException
     */
    @RequestMapping(value = "/atproperties/get", method = { RequestMethod.POST }, headers = "Accept=application/json; charset=utf-8")
    @ResponseBody
    public ResponseDTO get(@RequestParam String id, HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException
    {
        ResponseDTO result = new ResponseDTO();
        String username = (String) request.getAttribute(Constants.HTTP_REQUEST_USERNAME);
        try
        {
            ResolvePropertiesVO vo = ServiceHibernate.findResolvePropertiesById(id, null, null, username);
            result.setData(vo);
            result.setSuccess(true);
            
        }
        catch (Exception e)
        {
            Log.log.error("Error retrieving Properties", e);
            result.setSuccess(false).setMessage("Error retrieving Properties. See log for more info.");
        }

        return result;
    }
    
  
    /**
     *  Delete the Properties based on array of sysIds
     *  
     * <p>
     * Parameters in the Request object: 
     * <code><pre>
     *  USERNAME - username of the user who is logged in to Resolve
     * </pre></code>
     * 
     * @param ids  array of sysIds to be deleted
     * @param deleteAll true/false if want to delete all the records
     * @param request HttpServletRequest
     * @param response HttpServletResponse
     * @return {@link ResponseDTO} - Reponse Data Transfer Object with status as 'true' or 'false'
     * @throws ServletException
     * @throws IOException
     */
    @RequestMapping(value = "/atproperties/delete", method = { RequestMethod.POST }, headers = "Accept=application/json; charset=utf-8")
    @ResponseBody
    public ResponseDTO delete(@RequestParam String[] ids, @RequestParam("all") Boolean deleteAll, HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException
    {
        ResponseDTO result = new ResponseDTO();
        String username = (String) request.getAttribute(Constants.HTTP_REQUEST_USERNAME);
        
        try
        {
            ServiceHibernate.deleteResolvePropertiesByIds(ids, deleteAll, username);
            result.setSuccess(true);
        }
        catch (Exception e)
        {
            Log.log.error("Error deleting Properties", e);
            result.setSuccess(false).setMessage("Error deleting Properties. See log for more info.");
        }

        return result;
    }
    

    /**
     * 
     * Saves/Update a property. If 'sysId' is set, it will be update else it will create it. 
     * 
     * <p>
     * Parameters in the Request object: 
     * <code><pre>
     *  USERNAME - username of the user who is logged in to Resolve
     * </pre></code>
     * <p>
     * Return object in response 
     * <code><pre>
     *  'data' : {@link ResolvePropertiesVO}
     * </pre></code>
     *  
     * @param entity {@link ResolvePropertiesVO}
     * @param request HttpServletRequest
     * @param response HttpServletResponse
     * @return {@link ResponseDTO} - Reponse Data Transfer Object with 'data' having the created/updated properties
     * @throws ServletException
     * @throws IOException
     */
    @RequestMapping(value = "/atproperties/save", method = { RequestMethod.POST }, headers = "Accept=application/json; charset=utf-8")
    @ResponseBody
    public ResponseDTO save(@RequestBody ResolvePropertiesVO entity, HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException
    {
      //using jackson to deserialize the json
        //String json = jsonProperty.toString();
        //ResolvePropertiesVO entity = new ObjectMapper().readValue(jsonProperty, ResolvePropertiesVO.class);
        
        //convert json to VO - spring does not work as the VO are not 100% java bean
//        JsonConfig jsonConfig = new JsonConfig();
//        jsonConfig.setRootClass(ResolvePropertiesVO.class);
//        ResolvePropertiesVO entity = (ResolvePropertiesVO) JSONSerializer.toJava(jsonProperty, jsonConfig);
        
        ResponseDTO result = new ResponseDTO();
        String username = (String) request.getAttribute(Constants.HTTP_REQUEST_USERNAME);
        
        try
        {
            ResolvePropertiesVO prop = ServiceHibernate.saveResolveProperties(entity, username);
            result.setSuccess(true).setData(prop);
        }
        catch (Exception e)
        {
            Log.log.error("Error saving the Properties", e);
            result.setSuccess(false).setMessage("Error saving the Properties.. See log for more info.");
        }

        return result;
    }
    
    @RequestMapping(value = "/atproperties/updateIndexMapping", method = { RequestMethod.GET, RequestMethod.POST }, headers = "Accept=application/json; charset=utf-8")
    @ResponseBody
    public ResponseDTO<String> updateIndexMapping(HttpServletRequest request, HttpServletResponse response) {
    	  ResponseDTO<String> result = new ResponseDTO<String>();
          String username = (String) request.getAttribute(Constants.HTTP_REQUEST_USERNAME);
          try {
        	  PropertyIndexAPI.updateIndexMapping(username);
        	  result.setSuccess(true);
          }
          catch (Exception e)
          {
              Log.log.error("Error updating index mapping for action task properties", e);
              result.setSuccess(false).setMessage("Error updating index mapping for action task properties. See log for more info.");
          }
          
          return result;
    }
    
    @RequestMapping(value = "/atproperties/index", method = { RequestMethod.GET, RequestMethod.POST }, headers = "Accept=application/json; charset=utf-8")
    @ResponseBody
    public ResponseDTO<String> index(@RequestParam(value = "all",defaultValue="false") Boolean all,@RequestParam(value = "ids",defaultValue = "[]") List<String> ids, HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException
    {
        ResponseDTO<String> result = new ResponseDTO<String>();
        String username = (String) request.getAttribute(Constants.HTTP_REQUEST_USERNAME);
        try
        {
            if(!all)
                PropertyIndexAPI.indexProperties(ids, username);
            else
                MIndex.indexAllResolveProperties(new HashMap<String, Object>());
            result.setSuccess(true);
        }
        catch (Exception e)
        {
            Log.log.error("Error indexing action task properties", e);
            result.setSuccess(false).setMessage("Error indexing action task properties. See log for more info.");
        }
        
        return result;
    }
    
}
