/******************************************************************************
* (C) Copyright 2014
*
* Resolve Systems, LLC
*
* All rights reserved
* This software is distributed under license and may not be used, copied,
* modified, or distributed without the express written permission of
* Resolve Systems, LLC.
*
******************************************************************************/

package com.resolve.rsview.controller;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.codehaus.jackson.map.ObjectMapper;
import org.springframework.stereotype.Controller;
import org.springframework.stereotype.Service;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.resolve.rsview.util.CatalogFileUpload;
import com.resolve.services.ServiceCatalog;
import com.resolve.services.catalog.Catalog;
import com.resolve.services.hibernate.vo.CatalogAttachmentVO;
import com.resolve.services.vo.QueryDTO;
import com.resolve.services.vo.QueryFilter;
import com.resolve.services.vo.QuerySort;
import com.resolve.services.vo.ResponseDTO;
import com.resolve.services.vo.WikiDocumentDTO;
import com.resolve.util.Constants;
import com.resolve.util.Log;

import net.sf.json.JSONObject;

/**
 * Controller for Catalogs:
 * Catalogs can be of 2 types: Catalog and Training
 * <p>
 * Catalog - Forces structure to be Group -> Folder,Item; Folder -> Group so that we can display things properly in the catalog viewer
 * As an example, look to the Sencha Examples page for what we were trying to replicate
 * <p>
 * Training - Removes the Group and simply supports Folder -> Item structure to display as a basic tree on the screen
 * As an example, look to the Sencha Training for what we were trying to replicate
 * <p>
 * Terms:
 * Catalog Reference (Catalog only)- A reference to another catalog from a catalog.
 * <p>
 * Catalog Group (Catalog only)- A simple title to allow for grouping in the viewer.
 * <p>
 * Catalog Folder (Catalog)- Displays the same as an item in the viewer, but clicking on it will display the folder's contents instead of taking you somewhere like an item
 * <p>
 * Catalog Folder (Training) - Displays as a folder in a tree. Displays exactly like an item and will display wiki documents when selected
 * <p>
 * Catalog Item (Catalog)- Displays an image, title, and description as a large box that users can click on to take them to somewhere in the system
 * <p>
 * Catalog Item (Training)- Displays a wiki document or url when selected, but doesn't have any child elements.
 */
@Controller
@Service
public class AjaxCatalog extends GenericController
{
    /**
     * Retrieves the list of Catalogs in the system based on the provided query.
     * 
     * @param query
     *            QueryDTO - provides start, limit, sorting, and filtering (See QueryDTO for more information)
     * @param request HttpServletRequest
     * 
     */
    @RequestMapping(value = "/catalog/list", method = { RequestMethod.GET, RequestMethod.POST }, headers = "Accept=application/json; charset=utf-8")
    @ResponseBody
    public ResponseDTO<Catalog> list(@ModelAttribute QueryDTO query, HttpServletRequest request) throws ServletException, IOException
    {
        ResponseDTO<Catalog> result = new ResponseDTO<Catalog>();
        String username = (String) request.getAttribute(Constants.HTTP_REQUEST_USERNAME);
        try
        {
            //**HACK - should be done on the UI
            List<QuerySort> sorts = query.getSortItems();
            for (QuerySort sort : sorts)
            {
                if (sort.getProperty().equalsIgnoreCase("name"))
                {
                    sort.setProperty("UName");
                }
                if (sort.getProperty().equalsIgnoreCase("catalogType"))
                {
                    sort.setProperty("UType");
                }
            }
            query.setSortItems(sorts);
            
            List<QueryFilter> filters = query.getFilters();
            if(filters != null)
            {
                for(QueryFilter filter : filters)
                {
                    String field = filter.getField();
                    if(field.equalsIgnoreCase("name"))
                    {
                        filter.setField("UName");
                    }
                    if(field.equalsIgnoreCase("catalogType"))
                    {
                        filter.setField("UType");
                    }
                }
            }
            query.setFilters(filters);

            
            
            result = ServiceCatalog.getCatalogs(query, username);
            result.setSuccess(true);
        }
        catch (Exception e)
        {
            Log.log.error("Error retrieving catalogs", e);
            result.setSuccess(false).setMessage("Error retrieving catalogs " + e.getMessage());
        }
        return result;
    }

    /**
     * Retrieves a single catalog from the system given the provided id. The catalog will be returned regardless of the type, so its the
     * responsibility of the client to properly enforce the rules of structure between the Training and Catalog types
     * 
     * @param id
     *            : String - The id of the catalog to be retrieved
     * 
     */
    @RequestMapping(value = "/catalog/get", method = { RequestMethod.POST }, headers = "Accept=application/json; charset=utf-8")
    @ResponseBody
    public ResponseDTO<Catalog> get(@RequestParam String id, HttpServletRequest request) throws ServletException, IOException
    {
        ResponseDTO<Catalog> result = new ResponseDTO<Catalog>();
        String username = (String) request.getAttribute(Constants.HTTP_REQUEST_USERNAME);
        try
        {
            Catalog catalog = ServiceCatalog.getCatalog(id, null, username);
            result.setSuccess(true).setData(catalog);
        }
        catch (Exception e)
        {
            Log.log.error("Error retrieving catalog", e);
            result.setSuccess(false).setMessage("Error retrieving catalog " + e.getMessage());
        }

        return result;
    }

    /**
     * Deletes the catalogs provided by the client
     * 
     * @param ids
     *            : String[] - the list of catalog ids to remove from the system
     */
    // TODO: This needs to account for selecting across pages
    @RequestMapping(value = "/catalogbuilder/delete", method = { RequestMethod.POST }, headers = "Accept=application/json; charset=utf-8")
    @ResponseBody
    public ResponseDTO<Catalog> delete(@RequestParam String[] ids, HttpServletRequest request) throws ServletException, IOException
    {
        ResponseDTO<Catalog> result = new ResponseDTO<Catalog>();
        String username = (String) request.getAttribute(Constants.HTTP_REQUEST_USERNAME);
        try
        {
            for (String id : ids)
            {
                ServiceCatalog.deleteCatalog(id, null, username);
            }
            result.setSuccess(true);
        }
        catch (Exception e)
        {
            Log.log.error("Error deleting catalog", e);
            result.setSuccess(false).setMessage("Error deleting catalog " + e.getMessage());
        }

        return result;
    }

    /**
     * Renames a catalog to the provided name if there isn't already a duplicate catalog with that name in the system
     * 
     * @param id
     *            : String - the id of the catalog to rename
     * @param newName
     *            : String - The name of the catalog after the rename is done
     * @param request HttpServletRequest
     */
    // TODO: Needs to validate catalog before updating to ensure its not only valid, but has a unique name
    @RequestMapping(value = "/catalogbuilder/rename", method = { RequestMethod.POST }, headers = "Accept=application/json; charset=utf-8")
    @ResponseBody
    public ResponseDTO<Catalog> rename(@RequestParam String id, @RequestParam String newName, HttpServletRequest request) throws ServletException, IOException
    {
        String username = (String) request.getAttribute(Constants.HTTP_REQUEST_USERNAME);
        
        ResponseDTO<Catalog> result = new ResponseDTO<Catalog>();
        result.setSuccess(false).setMessage("Error locating the catalog to rename.");

        try
        {
            Catalog catalog = ServiceCatalog.getCatalog(id, null, username);
            if (catalog != null)
            {
                catalog.applyPath(catalog.getName(), newName);//, true);
                catalog.setName(newName);
                
                ServiceCatalog.saveCatalog(catalog, username);
                result.setSuccess(true);
            }
        }
        catch (Exception e)
        {
            Log.log.error("Error renaming catalog", e);
            result.setSuccess(false).setMessage("Error renaming catalog " + e.getMessage());
        }

        return result;
    }

    /**
     * Copies the contents of one catalog to another catalog.
     * 
     * @param id
     *            : String - the id of the source catalog to copy from
     * @param newName
     *            : String - the name of the new catalog to copy into
     *            
     * @param request HttpServletRequest
     */
    // TODO: Needs to validate name before copying to ensure unique catalog names
    @RequestMapping(value = "/catalogbuilder/copy", method = { RequestMethod.POST }, headers = "Accept=application/json; charset=utf-8")
    @ResponseBody
    public ResponseDTO<Catalog> copy(@RequestParam String id, @RequestParam String newName, HttpServletRequest request) throws ServletException, IOException
    {
        String username = (String) request.getAttribute(Constants.HTTP_REQUEST_USERNAME);
        
        ResponseDTO<Catalog> result = new ResponseDTO<Catalog>();

        try
        {
            Catalog catalog = ServiceCatalog.getCatalog(id, null, username);
            if (catalog != null)
            {
                catalog.applyPath(catalog.getName(), newName);
                catalog.setName(newName);
                catalog.purgeIds();
                
                ServiceCatalog.saveCatalog(catalog, username);
                result.setSuccess(true);
            }
            else
            {
                result.setSuccess(false).setMessage("Error locating the catalog to copy");
            }
        }
        catch (Exception e)
        {
            Log.log.error("Error copying catalog", e);
            result.setSuccess(false).setMessage("Error copying catalog " + e.getMessage());
        }

        return result;
    }

    /**
     * Returns the list of catalog names in the system.
     * <p>
     * This is used when referring to another catalog in the system (catalog reference).
     * <p>
     * Essentially, this allows you to create a catalog with a specific structure and then re-use that structure in other catalogs. The list
     * of names that are returned to the client allow the client to properly reference another catalog by id while giving the user the name.
     */
    // TODO: This should be paged and filtered in case the list of names gets to be really long
    @RequestMapping(value = "/catalog/names", method = { RequestMethod.GET }, headers = "Accept=application/json; charset=utf-8")
    @ResponseBody
    public ResponseDTO<Catalog> names(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException
    {
        ResponseDTO<Catalog> result = new ResponseDTO<Catalog>();
        String username = (String) request.getAttribute(Constants.HTTP_REQUEST_USERNAME);
        
        try
        {
            result = ServiceCatalog.getCatalogs(null, username);
            result.setSuccess(true);
        }
        catch (Exception e)
        {
            Log.log.error("Error retrieving catalog names", e);
            result.setSuccess(false).setMessage("Error retrieving catalog names " + e.getMessage());
        }

        return result;
    }
    
    /**
     * Get list of catalogs that this catalog can add it as a reference
     * 
     * @param catalogId : Parent catalog Id.
     *          
     * @param catalogName : Parent catalog Id.
     * @param request : HttpServletRequest
     * @param response : HttpServletResponse
     */
    @RequestMapping(value = "/catalog/refnames", method = { RequestMethod.GET }, headers = "Accept=application/json; charset=utf-8")
    @ResponseBody
    public ResponseDTO<Catalog> refNames(@RequestParam String catalogId, @RequestParam String catalogName, HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException
    {
        ResponseDTO<Catalog> result = new ResponseDTO<Catalog>();
        String username = (String) request.getAttribute(Constants.HTTP_REQUEST_USERNAME);
        
        try
        {
            result = ServiceCatalog.getRefCatalogs(catalogId, catalogName, username);
            result.setSuccess(true);
        }
        catch (Exception e)
        {
            Log.log.error("Error retrieving ref catalog names", e);
            result.setSuccess(false).setMessage("Error retrieving ref catalog names " + e.getMessage());
        }

        return result;
    }

    /**
     * Saves the provided catalog. Depending on the type of catalog (Catalog,Training) the structure can vary wildly. All we do for the backend
     * is turn each node into a ResolveCatalog and then store the structure. Its up to the client to ensure the structure of the catalog being persisted,
     * but the server will provide data integrity checking to ensure that the name of the catalogs is unique in the system.
     * 
     * @param catalogJson
     *            : JSONObject - the java object representation of the json sent from the client. This is deserialized as a ResolveCatalog
     */
    @RequestMapping(value = "/catalogbuilder/save", method = { RequestMethod.POST }, headers = "Accept=application/json; charset=utf-8")
    @ResponseBody
    public ResponseDTO save(@RequestBody JSONObject catalogJson, HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException
    {
        ResponseDTO result = new ResponseDTO();
        String id = null;
        String username = (String) request.getAttribute(Constants.HTTP_REQUEST_USERNAME);
        
        //NOTE: I think we should send the complete obj of Catalog to UI to refresh it like we have done for other functionalities
        
        try
        {
            //using jackson to deserialize the json
            String json = catalogJson.toString();
            Catalog catalog = new ObjectMapper().readValue(json, Catalog.class);
            catalog.setSys_updated_by(username);
            catalog.setIsRoot(true);//make sure that its always true for the root node.
            
//            JsonConfig jsonConfig = new JsonConfig();
//            jsonConfig.setRootClass(ResolveCatalog.class);
//            ResolveCatalog catalog = (ResolveCatalog) JSONSerializer.toJava(catalogJson, jsonConfig);
            
            if (catalog.isValid())
            {
                catalog = ServiceCatalog.saveCatalog(catalog, username);
                result.setSuccess(true).setData(catalog);
            }
            else
            {
                result.setSuccess(false).setMessage(catalog.getError());
            }
        }
        catch (Exception e)
        {
            Log.log.error("Error saving the catalog", e);
            result.setSuccess(false).setMessage("Error saving the catalog. " + e.getMessage());
        }

        return result;
    }

    /**
     * Retrieves the list of catalog images in the system to display in the image picker.
     */
    
    //TODO: this should be paged/filtered in case the list gets to be large
    @RequestMapping(value = "/catalog/imageList", method = { RequestMethod.GET, RequestMethod.POST }, headers = "Accept=application/json; charset=utf-8")
    @ResponseBody
    public ResponseDTO imageList(HttpServletRequest request) throws ServletException, IOException
    {
        ResponseDTO result = new ResponseDTO();
        String username = (String) request.getAttribute(Constants.HTTP_REQUEST_USERNAME);
        try
        {
            QueryDTO query = new QueryDTO();
            query.setSelectColumns("sys_id,sysCreatedOn,sysCreatedBy,sysUpdatedOn,sysUpdatedBy,fileName,displayName,type,location,size");
            query.setModelName("CatalogAttachment");
            query.setType(QueryDTO.TABLE_TYPE);
            query.setLimit(-1);
            query.setStart(0);
            
            result = ServiceCatalog.getCatalogAttachments(query, username);
            result.setSuccess(true);
        }
        catch (Exception e)
        {
            Log.log.error("Error retrieving image list", e);
            result.setSuccess(false).setMessage("Error retrieving image list " + e.getMessage());
        }

        return result;
    }

    /**
     * Uploads an image to the catalog image repository for users to pick from.
     * <p>
     * Images are able to be referenced from catalog items in a Catalog (not Training).
     */
    @RequestMapping(value = "/catalogbuilder/upload", method = { RequestMethod.POST }, headers = "Accept=application/json; charset=utf-8")
    @ResponseBody
    public ResponseDTO upload(HttpServletRequest request) throws ServletException, IOException
    {
        ResponseDTO result = new ResponseDTO();
        String username = (String) request.getAttribute(Constants.HTTP_REQUEST_USERNAME);
        try
        {
            CatalogAttachmentVO ca = new CatalogFileUpload(request, username).upload(); 
            result.setSuccess(true).setData(ca);
        }
        catch (Exception e)
        {
            Log.log.error("Error uploading image", e);
            result.setSuccess(false).setMessage("Error uploading image " + e.getMessage());
        }

        return result;
    }


    /**
     * Downloads the catalog image with the id provided by the client.
     * <br>
     * Used in the CatalogViewer and the CatalogBuilder to display the selected images to the user
     * 
     * @param id
     *            : String - the id of the catalog image to download
     */
    @RequestMapping(value = "/catalog/download", method = { RequestMethod.POST, RequestMethod.GET }, headers = "Accept=application/json; charset=utf-8")
    @ResponseBody
    public void download(@RequestParam String id, HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException
    {
        String username = (String) request.getAttribute(Constants.HTTP_REQUEST_USERNAME);
        try
        {
            CatalogAttachmentVO ca = ServiceCatalog.downloadCatalogFile(id, username);
            if (ca != null)
            {
                response.addHeader("Content-Disposition", "attachment; filename=\"" + ca.getFilename() + "\"");
                String fileExt = ca.getFilename().substring(ca.getFilename().lastIndexOf(".") + 1);
                if (fileExt != null) {
                    fileExt = fileExt.toLowerCase();
                }
                if (fileExt.equals("jpg") || fileExt.equals("jpe") || fileExt.equals("jif") || fileExt.equals("jfif") || fileExt.equals("jfi")) {
                    // RBA-16467
                    // https://msdn.microsoft.com/en-us/library/ms527550(v=exchg.10).aspx
                    // With nosniff in response header, IE won't display jpeg images if response header's content-type isn't image/jpeg
                    // Here we check for all variants of jpeg file extension and set only 'jpeg'.
                    // Other browsers are smart to figure out the file type and will display correctly.
                    fileExt = "jpeg";
                }
                response.setContentType("image/" + fileExt + ";  name=\"" + ca.getFilename() + "\"");
                if (ca.getU_size() > 0)
                {
                    response.setContentLength(ca.getU_size());
                }
                response.getOutputStream().write(ca.getContent());
            }
        }
        catch (Exception e)
        {
            Log.log.error("Error downloading image", e);
        }
    }

    /**
     * Gets the list of tags in the system for the user to associate tag(s) to a folder, item, or catalog. Tags are used from the catalog to display
     * wiki documents that are associated to particular tags. For example, if there is a folder with a display type of search grid, the search grid will display
     * a list of wiki documents that match that folder's tag requirements.
     * 
     * @param query
     *            : QueryDTO - provides paging, sorting, filtering (See {@link QueryDTO} for more information)
     */
    @RequestMapping(value = "/catalog/tags", method = { RequestMethod.GET }, headers = "Accept=application/json; charset=utf-8")
    @ResponseBody
    public ResponseDTO listTags(@ModelAttribute QueryDTO query, HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException
    {
        return new AjaxTag().listTags(query, request, response);
    }

    /**
     * Short desc: Retrieves a list of documents that match either the path or all of the provided tags
     * <p>
     * Retrieves a list of documents given the path and the tags for a particular item.<br>This is used when a catalog item has a display type "listOfDocuments"
     * The viewer will load the grid when the user selects that item, and will call into the server for the list of documents to display. The list returned
     * from the server contains 2 parts. If a user has associated wiki documents with the path (which itself is really a tag) then obviously those documents should
     * show up when the item is selected. If a user has configured the item with additional tags, then documents that match all (AND operation) tags should also
     * be returned to the client. Users can associate wiki documents to broad tags (Windows, Network, Security) or to a specific tag which references a particular
     * catalog item (/Windows/Network/Security). If they make a specific association (using the path/ method) then that wikidocument is associated 1-1 with the
     * catalog item. But it could be that they want to display additional helper runbooks that match the area you are searching in. It allows for maximum flexibility
     * in the system.
     * 
     * 
     * @param query
     *            : QueryDTO - provides paging, sorting, filtering (See {@link QueryDTO} for more information)
     * @param path
     *            : String - path of the selected "item" (/Windows/Network/Security for example)
     * @param tags
     *            : String[] - Array of tag ids to match
     */
    @RequestMapping(value = "/catalog/listOfDocuments", method = { RequestMethod.GET }, headers = "Accept=application/json; charset=utf-8")
    @ResponseBody
    public ResponseDTO<WikiDocumentDTO> listOfDocuments(@ModelAttribute QueryDTO query, @RequestParam String path, @RequestParam List<String> tags, HttpServletRequest request) throws ServletException, IOException
    {
        ResponseDTO<WikiDocumentDTO> result = new ResponseDTO<WikiDocumentDTO>();
        String username = (String) request.getAttribute(Constants.HTTP_REQUEST_USERNAME);
        try
        {
            Collection<WikiDocumentDTO> wikiDtos = ServiceCatalog.getWikidocmentOf(path, tags, username);
            result.setSuccess(true).setRecords(new ArrayList<WikiDocumentDTO>(wikiDtos));
        }
        catch (Exception e)
        {
            Log.log.error("Error retrieving documents", e);
            result.setSuccess(false).setMessage("Error retrieving documents " + e.getMessage());
        }

        return result;
    }

}
