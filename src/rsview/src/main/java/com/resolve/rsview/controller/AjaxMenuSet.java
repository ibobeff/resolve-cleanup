/******************************************************************************
* (C) Copyright 2014
*
* Resolve Systems, LLC
*
* All rights reserved
* This software is distributed under license and may not be used, copied,
* modified, or distributed without the express written permission of
* Resolve Systems, LLC.
*
******************************************************************************/

package com.resolve.rsview.controller;

import java.io.IOException;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.codehaus.jackson.map.ObjectMapper;
import org.springframework.stereotype.Controller;
import org.springframework.stereotype.Service;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.resolve.services.ServiceHibernate;
import com.resolve.services.hibernate.menu.MenuSetUtil;
import com.resolve.services.hibernate.vo.SysPerspectiveVO;
import com.resolve.services.vo.QueryDTO;
import com.resolve.services.vo.ResponseDTO;
import com.resolve.util.Constants;
import com.resolve.util.Log;

import net.sf.json.JSONObject;

/**
 * Resolve, by default, has 5 different Menu Sets. These are User, Process, Developer, Report and Admin. These Menu Sets contain a collection different Menu Definitions.
 * This controller provides CRUD operation for the Menu Sets.
 *
 */
@Controller
@Service
public class AjaxMenuSet extends GenericController
{
    /**
     * Returns list of Menu Sets for the grid based on filters, sorts, pagination
     * 
     * @param query : {@link QueryDTO}, represents the query, pagination, sorting, grouping for the UI
     * @param request {@link HttpServletRequest} </code> containing <code>USERNAME</code>: a username of the user who is logged in to Resolve
     * @param response {@link HttpServletResponse}
     * @return {@link ResponseDTO} with status true, {@link SysPerspectiveVO} list as data if everything goes smooth, false otherwise with the error message.
     * @throws ServletException
     * @throws IOException
     */
    @RequestMapping(value = "/menuset/list", method = { RequestMethod.GET, RequestMethod.POST }, headers = "Accept=application/json; charset=utf-8")
    @ResponseBody
    public ResponseDTO list(@ModelAttribute QueryDTO query, HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException
    {
        ResponseDTO result = new ResponseDTO();
        String username = (String) request.getAttribute(Constants.HTTP_REQUEST_USERNAME);
        try
        {
            query.setModelName("SysPerspective");
            
            List<SysPerspectiveVO> data = MenuSetUtil.getMenuSets(query, username);
            int total = ServiceHibernate.getTotalHqlCount(query);
            
            result.setSuccess(true).setRecords(data);
            result.setTotal(total);
        }
        catch (Exception e)
        {
            Log.log.error("Error retrieving menu sets", e);
            result.setSuccess(false).setMessage("Error retrieving menu sets");
        }
        
        return result;
    }
    
    /**
     * Gets data for a specific menu set based on sysId
     * 
     * @param id : String representing sysId of the menu set
     * @param request : {@link HttpServletRequest} </code> containing <code>USERNAME</code>: a username of the user who is logged in to Resolve
     * @param response : {@link HttpServletResponse}
     * @return {@link ResponseDTO} with status true, {@link SysPerspectiveVO} as data if everything goes smooth, false otherwise with the error message.
     * @throws ServletException
     * @throws IOException
     */
    @RequestMapping(value = "/menuset/get", method = { RequestMethod.POST }, headers = "Accept=application/json; charset=utf-8")
    @ResponseBody
    public ResponseDTO get(@RequestParam String id, HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException
    {
        ResponseDTO result = new ResponseDTO();
        String username = (String) request.getAttribute(Constants.HTTP_REQUEST_USERNAME);
        
        try
        {
        	SysPerspectiveVO vo = MenuSetUtil.findMenuSetByIdOrByName(id, null, username);
            result.setData(vo);
            result.setSuccess(true);
        }
        catch (Exception e)
        {
            Log.log.error("Error retrieving menu set", e);
            result.setSuccess(false).setMessage("Error retrieving menu set");
        }

        return result;
    }
    
    /**
     *  Delete the menu set based on array of sysIds
     * 
     * @param ids : String array containing sysIds of the menu sets to be deleted.
     * @param request : {@link HttpServletRequest} </code> containing <code>USERNAME</code>: a username of the user who is logged in to Resolve
     * @param response : {@link HttpServletResponse}
     * @return {@link ResponseDTO} with status true if everything goes smooth, false otherwise with the error message.
     * @throws ServletException
     * @throws IOException
     */
    @RequestMapping(value = "/menuset/delete", method = { RequestMethod.POST }, headers = "Accept=application/json; charset=utf-8")
    @ResponseBody
    public ResponseDTO delete(@RequestParam String[] ids, @ModelAttribute QueryDTO query, HttpServletRequest request) throws ServletException, IOException
    {
        ResponseDTO result = new ResponseDTO();
        String username = (String) request.getAttribute(Constants.HTTP_REQUEST_USERNAME);
        
        try
        {
        	MenuSetUtil.deleteMenuSets(ids, query, username);
            result.setSuccess(true).setMessage("Records deleted successfully.");
        }
        catch (Exception e)
        {
            Log.log.error("Error deleting menu set", e);
            result.setSuccess(false).setMessage("Error deleting menu set");
        }

        return result;
    }
    
    /**
     * Save the menu set record
     * 
     * @param jsonProperty : JSON String representing {@link SysPerspectiveVO} object 
     * @param request : {@link HttpServletRequest} </code> containing <code>USERNAME</code>: a username of the user who is logged in to Resolve
     * @return {@link ResponseDTO} with status true, {@link SysPerspectiveVO} as data if everything goes smooth, false otherwise with the error message.
     * @throws ServletException
     * @throws IOException
     */
    @RequestMapping(value = "/menuset/save", method = { RequestMethod.POST, RequestMethod.GET })
    @ResponseBody
    public ResponseDTO save(@RequestBody JSONObject jsonProperty,  HttpServletRequest request) throws ServletException, IOException
    {
        //using jackson to deserialize the json
        String json = jsonProperty.toString();
        SysPerspectiveVO entity = new ObjectMapper().readValue(json, SysPerspectiveVO.class);

        //convert json to VO - spring does not work as the VO are not 100% java bean
//        JsonConfig jsonConfig = new JsonConfig();
//        jsonConfig.setRootClass(SysPerspectiveVO.class);
//        jsonConfig.setPropertySetStrategy(new PropertyStrategyWrapper(PropertySetStrategy.DEFAULT));
//        SysPerspectiveVO entity = (SysPerspectiveVO) JSONSerializer.toJava(jsonProperty, jsonConfig);
        
        ResponseDTO result = new ResponseDTO();
        String username = (String) request.getAttribute(Constants.HTTP_REQUEST_USERNAME);
        
        try
        {
            SysPerspectiveVO vo = MenuSetUtil.saveSysPerspective(entity, username);
            result.setSuccess(true).setData(vo);
        }
        catch (Exception e)
        {
            Log.log.error("Error saving the menu set", e);
            result.setSuccess(false).setMessage("Error saving the menu set");
        }
        
        return result;
    }
    
    /**
     * This api sets the sequence on the menu set records. User should be able to move the records on the Grid which defines the position which inturn will trigger this api that sets
     * the sequence for each one.
     * 
     * Gets trigger each time a user moves a record on the grid
     * 
     * @param ids : String array of sysIds of all the menu definition
     * @param request : {@link HttpServletRequest} </code> containing <code>USERNAME</code>: a username of the user who is logged in to Resolve
     * @return {@link ResponseDTO} with status true if everything goes smooth, false otherwise with the error message.
     * @throws ServletException
     * @throws IOException
     */
    @RequestMapping(value = "/menuset/sequence", method = { RequestMethod.POST }, headers = "Accept=application/json; charset=utf-8")
    @ResponseBody
    public ResponseDTO setSequence(@RequestParam String[] ids, HttpServletRequest request) throws ServletException, IOException
    {
        ResponseDTO result = new ResponseDTO();
        String username = (String) request.getAttribute(Constants.HTTP_REQUEST_USERNAME);
        
        try
        {
        	MenuSetUtil.setMenuSetSequence(ids, username);
            result.setSuccess(true).setMessage("");
        }
        catch (Exception e)
        {
            Log.log.error("Error setting sequence to menu sets", e);
            result.setSuccess(false).setMessage("Error setting sequence to menu sets");
        }

        return result;
    }
    
    
}
