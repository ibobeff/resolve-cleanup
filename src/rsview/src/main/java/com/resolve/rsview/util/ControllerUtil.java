/******************************************************************************
* (C) Copyright 2014
*
* Resolve Systems, LLC
*
* All rights reserved
* This software is distributed under license and may not be used, copied,
* modified, or distributed without the express written permission of
* Resolve Systems, LLC.
*
******************************************************************************/

package com.resolve.rsview.util;

import java.net.URLDecoder;
import java.net.URLEncoder;
import java.util.Enumeration;
import java.util.HashMap;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import com.novell.ldap.util.Base64;
import com.resolve.services.constants.ConstantValues;
import com.resolve.services.constants.WikiAction;
import com.resolve.util.Constants;
import com.resolve.util.StringUtils;
import com.resolve.wiki.store.helper.StoreUtility;

public class ControllerUtil
{
	public static final String ENCODING = "UTF-8";
	
    public static String getParameter(HttpServletRequest request, String name)
    {
        String result = null;
        
        result = request.getParameter(name);
        if (StringUtils.isEmpty(result))
        {
            result = request.getParameter(name.toUpperCase());
        }
        
        return result;
    } // getParameter
    
    public static Map<String,String> getParameterMap(HttpServletRequest request)
    {
        Map<String,String> result = new HashMap<String,String>();
        
        Enumeration<String> names = request.getParameterNames();
        
        while (names.hasMoreElements())
        {
            String name = names.nextElement();
            String value = request.getParameter(name);
            String values[] = request.getParameterValues(name);
            if (values.length > 1)
            {
                value = StringUtils.arrayToString(values);
            }
            
            if (value != null)
            {
            	if(convertToUpperCase(name))
            	{
            		result.put(name.toUpperCase(), value);
            	}
            	else
            	{
            		result.put(name, value);
            	}
            }
        }
        
        return result;
    } // getParameter
    
	public static void setRequestParameters(String namespace, String docname, WikiAction wikiAction, HttpServletRequest request)
	{
		if(StringUtils.isEmpty(namespace))
		{
			namespace = ConstantValues.WIKI_NAMESPACE_HOME;
		}
		
		//if docname is null, then goto 'WebHome'
		if (StringUtils.isEmpty(docname))
		{
			docname = ConstantValues.WIKI_WEB_HOME;
		}
		
		String user_id = (String)request.getAttribute(Constants.HTTP_REQUEST_USERNAME);
		if(StringUtils.isEmpty(user_id))
		{
		    user_id = request.getParameter(Constants.HTTP_REQUEST_INTERNAL_USERNAME);
		}
		
		//if the value is not empty then only add it 
		if(!StringUtils.isEmpty(user_id))
		{
		    request.setAttribute(Constants.HTTP_REQUEST_USERNAME, user_id);
		}
		
		//Test.test#section1
		namespace = decode(namespace);//Test
		docname = decode(docname);//test#section1
		String sectionName = getSectionNameFromDocName(docname);//section1
		docname = getDocumentNameFrom(docname);//test -- refresh the value of docname
		
		request.setAttribute(ConstantValues.WIKI_ACTION_KEY, wikiAction);
		request.setAttribute(ConstantValues.WIKI_NAMESPACE_KEY, namespace);
		request.setAttribute(ConstantValues.WIKI_DOCNAME_KEY, docname);
		request.setAttribute(ConstantValues.WIKI_SECTION_KEY, sectionName);
		request.setAttribute(ConstantValues.WIKI_DOC_FULLNAME_KEY, namespace + "." + docname);

        // set problemid
        String problemid = request.getParameter(Constants.HTTP_REQUEST_PROBLEMID);
        if (StringUtils.isBlank(problemid))
        {
            problemid = getProblemId(request);
            if (problemid != null)
            {
		        request.setAttribute(Constants.HTTP_REQUEST_PROBLEMID, problemid);
            }
        }
	} // setRequestParameters

	public static void setRequestParameters(String namespace, String docname, String sectionname, WikiAction wikiAction, HttpServletRequest request)
    {
        if (StringUtils.isEmpty(namespace))
        {
            namespace = ConstantValues.WIKI_NAMESPACE_HOME;
        }

        // if docname is null, then goto 'WebHome'
        if (StringUtils.isEmpty(docname))
        {
            docname = ConstantValues.WIKI_WEB_HOME;
        }

        String user_id = (String) request.getAttribute(Constants.HTTP_REQUEST_USERNAME);
        if (StringUtils.isEmpty(user_id))
        {
            user_id = request.getParameter(Constants.HTTP_REQUEST_INTERNAL_USERNAME);
        }

        // if the value is not empty then only add it
        if (!StringUtils.isEmpty(user_id))
        {
            request.setAttribute(Constants.HTTP_REQUEST_USERNAME, user_id);
        }

        // Test.test#section1
        namespace = decode(namespace);// Test
        docname = decode(docname);// test
        sectionname = decode(sectionname);

        request.setAttribute(ConstantValues.WIKI_ACTION_KEY, wikiAction);
        request.setAttribute(ConstantValues.WIKI_NAMESPACE_KEY, namespace);
        request.setAttribute(ConstantValues.WIKI_DOCNAME_KEY, docname);
        request.setAttribute(ConstantValues.WIKI_SECTION_KEY, sectionname);
        request.setAttribute(ConstantValues.WIKI_DOC_FULLNAME_KEY, namespace + "." + docname);

        // set problemid
        String problemid = request.getParameter(Constants.HTTP_REQUEST_PROBLEMID);
        if (StringUtils.isBlank(problemid))
        {
            problemid = getProblemId(request);
            if (problemid != null)
            {
                request.setAttribute(Constants.HTTP_REQUEST_PROBLEMID, problemid);
            }
        }
    }
	
	private static String getProblemId(HttpServletRequest request)
	{
	    String result = null;
	    
        Map<String,String> params = ControllerUtil.getParameterMap(request);
        String username = (String)request.getAttribute(Constants.HTTP_REQUEST_USERNAME);
        
        String type = null;
        String sys_id = null;
        String value = null;
        String orgId = null;
        String orgName = null;
        
        // problemid
        // init sys_id
        sys_id = params.get(Constants.HTTP_REQUEST_SYS_ID);
        if (StringUtils.isBlank(sys_id))
        {
	        sys_id = params.get(Constants.HTTP_REQUEST_sys_id);
        }
        if (StringUtils.isEmpty(sys_id))
        {
	        sys_id = params.get(Constants.HTTP_REQUEST_PROBLEMID);
        }
        
        orgId = params.get(Constants.EXECUTE_ORG_ID);
        
        if (StringUtils.isBlank(orgId))
        {
            orgId = params.get(Constants.EXECUTE_org_id);
        }
        
        orgName = params.get(Constants.EXECUTE_ORG_NAME);
        
        if (StringUtils.isBlank(orgName))
        {
            orgName = params.get(Constants.EXECUTE_org_name);
        }
        
        if (StringUtils.isNotEmpty(sys_id))
        {
            result = sys_id;
        }
        else
        {
            // try NUMBER
            value = params.get(Constants.HTTP_REQUEST_NUMBER);
            if (StringUtils.isNotEmpty(value))
            {
                type = Constants.HTTP_REQUEST_NUMBER;
            }
            else
            {
                // try REFERENCE
                value = params.get(Constants.HTTP_REQUEST_REFERENCE);
                if (StringUtils.isNotEmpty(value))
                {
                    type = Constants.HTTP_REQUEST_REFERENCE;
                }
                else
                {
                    // try ALERTID
                    value = params.get(Constants.HTTP_REQUEST_ALERTID);
                    if (StringUtils.isNotEmpty(value))
                    {
                        type = Constants.HTTP_REQUEST_ALERTID;
                    }
                    else
                    {
                        // try CORRELATIONID
                        value = params.get(Constants.HTTP_REQUEST_CORRELATIONID);
                        if (StringUtils.isNotEmpty(value))
                        {
                            type = Constants.HTTP_REQUEST_CORRELATIONID;
                        }
                        else
                        {
                            type = null;
                        }
                    }
                }
            }
            
            // get problemid
            if (type != null)
            {
	            result = StoreUtility.getProblemId(type, value, orgId, orgName, username);
                if (result == null)
                {
                    /*
                     * This should never happen for an existing WS. Looks like a new WS just got created and
                     * WS alias is not yet ready to search for it. Lets sleep for half a sec. and try again.
                     * Repeat this for 5 times and then give up.
                     */
                    for (int i=0; i<=4; i++)
                    {
                        try
                        {
                            Thread.sleep(500);
                            result = StoreUtility.getProblemId(type, value, orgId, orgName, username);
                            if (result != null)
                            {
                                break;
                            }
                        }
                        catch (InterruptedException e)
                        {
                            // don't do anything here.
                        }
                    }
                    
                    if (result == null)
                    {
                        result = "-1";
                    }
                }
            }
        } 
        
        return result;
	} // getProblemdId
	
    /**
     * Static method to set the values in the Request object
     * 
     * @param namespace
     * @param docname
     * @param request
     */
    
    
    public static String decode(String name)
    {
    	try
    	{
    		// Make sure + is considered as a space
    		String result = name.replace('+', ' ');
    
    		// It seems Internet Explorer can send us back UTF-8 instead of ISO-8859-1 for URLs
    		if (Base64.isValidUTF8(result.getBytes(), false))
    		{
    			result = new String(result.getBytes(), ENCODING);
    		}
    
    		// Still need to decode URLs
    		return URLDecoder.decode(result, ENCODING);
    	}
    	catch (Exception e)
    	{
    		return name;
    	}
    } // decode

    public static String encode(String name)
    {
    	try
    	{
    		return URLEncoder.encode(name, ENCODING);
    	}
    	catch (Exception e)
    	{
    		return name;
    	}
    } // encode
    
    //test#main
    public static String getSectionNameFromDocName(String docname)
    {
        String section = "";
        if(docname.indexOf("#") > -1)
        {
            section = docname.substring(docname.indexOf("#")+1);
        }

        return section;
    }//getSectionNameFromDocName

    //test#main OR test
    public static String getDocumentNameFrom(String docname)
    {
        String name = docname;
        if(docname.indexOf("#") > -1)
        {
            name = docname.substring(0, docname.indexOf("#"));
        }

        return name;
    }//getSectionNameFromDocName

    private static boolean convertToUpperCase(String name)
    {
    	boolean convertToUpperCase = false;

    	if(name.equalsIgnoreCase(Constants.HTTP_REQUEST_USERNAME) 
    			|| name.equalsIgnoreCase(Constants.HTTP_REQUEST_TOKEN)
    			|| name.equalsIgnoreCase(Constants.HTTP_REQUEST_QUERY)
    			|| name.equalsIgnoreCase(Constants.HTTP_REQUEST_WIKI)
    			|| name.equalsIgnoreCase(Constants.HTTP_REQUEST_ACTION)
	    		|| name.equalsIgnoreCase(Constants.HTTP_REQUEST_ACTIONNAME)
    	    	|| name.equalsIgnoreCase(Constants.HTTP_REQUEST_ACTIONID)
    	    	|| name.equalsIgnoreCase(Constants.HTTP_REQUEST_ALERTID)
    	    	|| name.equalsIgnoreCase(Constants.HTTP_REQUEST_PROBLEMID)
    	    	|| name.equalsIgnoreCase(Constants.HTTP_REQUEST_PROCESSID)
    	    	|| name.equalsIgnoreCase(Constants.HTTP_REQUEST_EXECUTEID)
    	    	|| name.equalsIgnoreCase(Constants.HTTP_REQUEST_REFERENCE)
    	    	|| name.equalsIgnoreCase(Constants.HTTP_REQUEST_REDIRECT)
    	    	|| name.equalsIgnoreCase(Constants.HTTP_REQUEST_DEBUG)
    	    	|| name.equalsIgnoreCase(Constants.EXECUTE_ORG_ID)
    	    	|| name.equalsIgnoreCase(Constants.EXECUTE_ORG_NAME)
    		)
    		{
    				convertToUpperCase = true;	
    		}
    	
    	
    	return convertToUpperCase;
    	
    }
    
} // ControllerUtil
