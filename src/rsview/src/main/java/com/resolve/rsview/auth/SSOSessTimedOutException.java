/******************************************************************************
* (C) Copyright 2015
*
* Resolve Systems, LLC
*
* All rights reserved
* This software is distributed under license and may not be used, copied,
* modified, or distributed without the express written permission of
* Resolve Systems, LLC.
*
******************************************************************************/

package com.resolve.rsview.auth;

/**
 * Indicates single signed on user's session in host system has timed out.
 * <p>
 * In a Single Sign On (SSO) integration, users login into host system and access
 * Resolve system through it. In SSO integration users has to login in only once 
 * into host system and allows user to access host and Resolve system simultaneously.
 *  
 * @author Hemant Phanasgaonkar
 * @since  5.2.0.2
 */
public class SSOSessTimedOutException extends Exception
{
    private static final long serialVersionUID = -824276915021767147L;
	private String externalSystem;
    private String url;
    
    public SSOSessTimedOutException(String externalSystem, String url)
    {
        this.externalSystem = externalSystem;
        this.url = url;
    } // SSOSessTimedOutException
    
    public String getExternalSystem()
    {
        return externalSystem;
    }
    
    public String getUrl()
    {
        return url;
    }
    
    @Override
    public String toString()
    {
        return "SSOSessTimedOutException: externalSystem=" + externalSystem + ", url=" + url;
    }
} // SSOSessTimedOutException
