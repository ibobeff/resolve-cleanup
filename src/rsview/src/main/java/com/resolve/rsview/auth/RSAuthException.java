/******************************************************************************
* (C) Copyright 2014
*
* Resolve Systems, LLC
*
* All rights reserved
* This software is distributed under license and may not be used, copied,
* modified, or distributed without the express written permission of
* Resolve Systems, LLC.
*
******************************************************************************/

package com.resolve.rsview.auth;

import org.springframework.web.servlet.ModelAndView;

import com.resolve.rsview.model.Login;

public class RSAuthException extends Exception
{
    private static final long serialVersionUID = -8014903683204320930L;
	String url;
    
    public RSAuthException(String url)
    {
        this.url = url;
    } // RSAuthException
    
    public RSAuthException(String url, String errorMsg)
    {
        super(errorMsg);
        this.url = url;
    } // RSAuthException
    
    public ModelAndView getModelAndView()
    {
        return new ModelAndView("login", "login", new Login(url));
    } // getModelAndView
    
} // RSAuthException
