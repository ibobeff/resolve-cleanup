package com.resolve.rsview.main;

import com.resolve.util.XDoc;

public class ConfigMCPRegistration extends ConfigGeneral
{
    private static final long serialVersionUID = -7207358148873791451L;
    private String mcpURI = "";
    private String mcpPort = "";
    private String mcpProtocol = "";
    private String mcpUsername = "";
    private String mcpPassword = "";
    private String mcpRegistrationEnabled = "";
    
    public ConfigMCPRegistration(XDoc config) throws Exception
    {
        super(config);
        
        define("mcpRegistrationEnabled", STRING, "./MCP/REGISTRATION/@ENABLED");
        define("mcpPassword", SECURE, "./MCP/REGISTRATION/@PASSWORD");
        define("mcpPort", STRING, "./MCP/REGISTRATION/@PORT");
        define("mcpProtocol", STRING, "./MCP/REGISTRATION/@PROTOCOL");
        define("mcpURI", STRING, "./MCP/REGISTRATION/@URI");
        define("mcpUsername", STRING, "./MCP/REGISTRATION/@USERNAME");
        
    }

    public String getMcpRegistrationEnabled()
    {
        return mcpRegistrationEnabled;
    }

    public void setMcpRegistrationEnabled(String mcpRegistrationEnabled)
    {
        this.mcpRegistrationEnabled = mcpRegistrationEnabled;
    }

    public String getMcpUsername()
    {
        return mcpUsername;
    }

    public void setMcpUsername(String mcpUsername)
    {
        this.mcpUsername = mcpUsername;
    }

    public String getMcpPassword()
    {
        return mcpPassword;
    }

    public void setMcpPassword(String mcpPassword)
    {
        this.mcpPassword = mcpPassword;
    }

    public String getMcpPort()
    {
        return mcpPort;
    }

    public void setMcpPort(String mcpPort)
    {
        this.mcpPort = mcpPort;
    }

    public String getMcpURI()
    {
        return mcpURI;
    }

    public void setMcpURI(String mcpURI)
    {
        this.mcpURI = mcpURI;
    }

    public String getMcpProtocol()
    {
        return mcpProtocol;
    }

    public void setMcpProtocol(String mcpProtocol)
    {
        this.mcpProtocol = mcpProtocol;
    }
    
    

}
