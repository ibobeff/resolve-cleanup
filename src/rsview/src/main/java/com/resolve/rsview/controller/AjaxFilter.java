/******************************************************************************
* (C) Copyright 2014
*
* Resolve Systems, LLC
*
* All rights reserved
* This software is distributed under license and may not be used, copied,
* modified, or distributed without the express written permission of
* Resolve Systems, LLC.
*
******************************************************************************/

package com.resolve.rsview.controller;

import java.io.IOException;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.stereotype.Controller;
import org.springframework.stereotype.Service;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.resolve.services.ServiceCustomTable;
import com.resolve.services.hibernate.util.vo.RsMetaFilterDTO;
import com.resolve.services.vo.ResponseDTO;
import com.resolve.util.Constants;
import com.resolve.util.Log;
import com.resolve.workflow.table.QueryCustomTable;

@Controller
@Service
public class AjaxFilter extends GenericController
{
    @RequestMapping(value = "/filter/list", method = { RequestMethod.POST, RequestMethod.GET }, headers = "Accept=application/json; charset=utf-8")
    @ResponseBody
    public ResponseDTO getFilters(@RequestParam String table, HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException
    {
        String username = (String) request.getAttribute(Constants.HTTP_REQUEST_USERNAME);
        ResponseDTO result = new ResponseDTO();

        try
        {
            List<RsMetaFilterDTO> filters =  ServiceCustomTable.getUserFilters(table, username);
            result.setSuccess(true).setRecords(filters);
        }
        catch (Exception e)
        {
            String error = "error in getting the list of filters for table :" + table;
            Log.log.error(error, e);
            result.setSuccess(false).setMessage(error);
        }

        return result;
    }

    @RequestMapping(value = "/filter/save", method = { RequestMethod.POST, RequestMethod.GET }, headers = "Accept=application/json; charset=utf-8")
    @ResponseBody
    public ResponseDTO saveFilter(@ModelAttribute RsMetaFilterDTO filter, HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException
    {
        String username = (String) request.getAttribute(Constants.HTTP_REQUEST_USERNAME);
        ResponseDTO result = new ResponseDTO();

        try
        {
            result = new QueryCustomTable().saveMetaFilter(filter, username);
        }
        catch (Exception e)
        {
            String error = "error in saving filter:" + filter.getName();
            Log.log.error(error, e);
            result.setSuccess(false).setMessage(error);
        }

        return result;
    }
    
    /**
     * 
     * @param sysIds - comma seperated filter sysIds
     * @param request
     * @param response
     * @return
     * @throws ServletException
     * @throws IOException
     */
    @RequestMapping(value = "/filter/delete", method = { RequestMethod.POST, RequestMethod.GET }, headers = "Accept=application/json; charset=utf-8")
    @ResponseBody
    public ResponseDTO deleteFilter(@RequestParam String sysIds, HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException
    {
        String username = (String) request.getAttribute(Constants.HTTP_REQUEST_USERNAME);
        ResponseDTO result = new ResponseDTO();
        
        try
        {
            result = new QueryCustomTable().deleteFilters(sysIds, username);
        }
        catch (Exception e)
        {
            String error = "error in deleting filters";
            Log.log.error(error, e);
            result.setSuccess(false).setMessage(error);
        }

        return result;
    }

}
