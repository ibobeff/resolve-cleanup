package com.resolve.rsview.auth;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.fail;

import java.io.File;
import java.net.URL;
import java.util.Properties;
import java.util.concurrent.ConcurrentMap;

import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.log4j.AppenderSkeleton;
import org.apache.log4j.Logger;
import org.apache.log4j.spi.LoggingEvent;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.ArgumentCaptor;
import org.mockito.Captor;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.mock.web.MockHttpServletRequest;
import org.springframework.mock.web.MockHttpServletResponse;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import com.resolve.persistence.util.CustomTableMappingUtil;
import com.resolve.persistence.util.HibernateUtil;
import com.resolve.rsview.auth.Authentication;
import com.resolve.rsview.main.ConfigGeneral;
import com.resolve.rsview.main.ConfigWindows;
import com.resolve.rsbase.ConfigId;
import com.resolve.rsbase.ConfigSQL;
import com.resolve.rsview.main.Main;
import com.resolve.rsview.main.RSContext;
import com.resolve.sql.SQL;
import com.resolve.sql.SQLDriverInterface;
import com.resolve.util.Constants;
import com.resolve.util.CryptUtils;
import com.resolve.util.Log;
import com.resolve.util.StringUtils;


@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration("/context.xml")
public class AuthenticationTest {
	
	private static MockHttpServletRequest mockRequest;
	private static MockHttpServletResponse mockResponse;
	private static Cookie cookie;
	
	@Autowired
	private Properties hibernateProperties;
	static AppenderSkeleton appender;
	@Captor 
	ArgumentCaptor<LoggingEvent> logCaptor;
	
	static {
		try {
			CryptUtils.setENCData(CryptUtils.getAESKey());
			Log.log = Logger.getLogger("RSVIEW");
			Log.auth = Logger.getLogger("AUTH");
			Log.syslog = Logger.getLogger("SYSLOG");
			Log.log.addAppender(appender);
			Log.auth.addAppender(appender);
			Log.syslog.addAppender(appender);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	public void setupHibernate() {
		try {
			Main main = Mockito.mock(Main.class);
			Main.main = main;
			RSContext.main = main;
			main.configGeneral = Mockito.mock(ConfigGeneral.class);
			main.configId = Mockito.mock(ConfigId.class);
			main.configId.guid = "test";
			Mockito.when(main.getConfigSQL()).thenReturn(Mockito.mock(ConfigSQL.class));
			Mockito.when(main.getConfigSQL().getDbtype()).thenReturn("MySql");
			Mockito.when(main.getConfigWindows()).thenReturn(Mockito.mock(ConfigWindows.class));
			Mockito.when(main.getConfigWindows().isActive()).thenReturn(false);
			Mockito.when(((com.resolve.rsview.main.ConfigGeneral) main.configGeneral).isSaveUserCredentials()).thenReturn(false);
	        final URL cfg = AuthenticationTest.class.getResource("/hibernate.cfg.xml");
	        final URL ctm = AuthenticationTest.class.getResource("/customtables.hbm.xml");
	        SQLDriverInterface driver = SQL.getDriver(hibernateProperties.getProperty("dbtype"), hibernateProperties.getProperty("resolve"), hibernateProperties.getProperty("localhost"),
	        											hibernateProperties.getProperty("username"),hibernateProperties.getProperty("password"),"", false);
	        if (driver != null)
	        {
	            SQL.init(driver);
	            SQL.start();
	        }
	        if (StringUtils.isNotEmpty(ctm.getPath()))
	        {
	            CustomTableMappingUtil.setCustomMappingFileLocation(ctm.getPath()+File.separator+"..");
	        }
//	        HibernateUtil.initTransactionManager(hibernateProperties, hibernateProperties);
			HibernateUtil.init(cfg.getPath());
			appender = Mockito.mock(AppenderSkeleton.class);
			
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	@Before
	public void setup() {
		//@BeforeClass runs before spring injection causing init to fail
		//workaround
		if(appender == null)
		{
			setupHibernate();
		}
		mockRequest = new MockHttpServletRequest();
		HttpSession sess = Mockito.mock(HttpSession.class);
		Mockito.when(sess.getId()).thenReturn("1");
		mockRequest.setSession(sess);
		mockResponse = new MockHttpServletResponse();
		cookie = new Cookie(Constants.AUTH_TOKEN,CryptUtils.encryptToken("testToken"));
		cookie.setValue(CryptUtils.encryptToken("testToken"));
		cookie.setPath("/");
		cookie.setMaxAge(2356);
		Authentication.getAuthTokenCache().clear();
	}
	
	@Test
	public void invalidateNoCookieNoAuthSessionTest() {
		HttpServletResponse response = mockResponse;
		HttpServletRequest request = mockRequest;
		Authentication.invalidate(request, response, "");
		
		mockResponse = (MockHttpServletResponse) response;
		Cookie invalidatedCookie = mockResponse.getCookie(Constants.AUTH_TOKEN);
		ConcurrentMap<String,AuthSession> authCache = Authentication.getAuthTokenCache();
		assertEquals(0,authCache.keySet().size());
		assertNull(invalidatedCookie);
	}
	
	@Test (expected = RuntimeException.class)
	public void invalidateNullValueNoAuthSessionTest() {
		cookie.setValue(null);
		mockRequest.setCookies(cookie);
		HttpServletResponse response = mockResponse;
		HttpServletRequest request = mockRequest;
		Authentication.invalidate(request, response, "");
		
		mockResponse = (MockHttpServletResponse) response;
		Cookie invalidatedCookie = mockResponse.getCookie(Constants.AUTH_TOKEN);
		ConcurrentMap<String,AuthSession> authCache = Authentication.getAuthTokenCache();
		assertEquals(0,authCache.keySet().size());
		assertNull(invalidatedCookie);
	}
	
	@Test
	public void invalidateNoAuthSessionTest() {
		mockRequest.setCookies(cookie);
		
		HttpServletResponse response = mockResponse;
		HttpServletRequest request = mockRequest;
		Authentication.invalidate(request, response, "");
		
		mockResponse = (MockHttpServletResponse) response;
		Cookie invalidatedCookie = mockResponse.getCookie(Constants.AUTH_TOKEN);
		ConcurrentMap<String,AuthSession> authCache = Authentication.getAuthTokenCache();
		assertEquals(0,authCache.keySet().size());
		assertNotNull(invalidatedCookie);
		assertEquals(invalidatedCookie.getName(),Constants.AUTH_TOKEN);
		assertEquals(invalidatedCookie.getMaxAge(),0);
	}
	
	@Test
	public void invalidateWithValidAuthSessionTest() {
		AuthSession auth = Mockito.mock(AuthSession.class);
		Authentication.getAuthTokenCache().put("testToken", auth);
		
		mockRequest.setCookies(cookie);
		
		HttpServletResponse response = mockResponse;
		HttpServletRequest request = mockRequest;
		Authentication.invalidate(request, response, "");
		
		mockResponse = (MockHttpServletResponse) response;
		Cookie invalidatedCookie = mockResponse.getCookie(Constants.AUTH_TOKEN);
		ConcurrentMap<String,AuthSession> authCache = Authentication.getAuthTokenCache();
		assertEquals(0,authCache.keySet().size());
		assertNotNull(invalidatedCookie);
		assertEquals(invalidatedCookie.getName(),Constants.AUTH_TOKEN);
		assertEquals(invalidatedCookie.getMaxAge(),0);
	}
	
	@Test
	public void invalidateWithValidAuthSessionNoMatchTest() {
		AuthSession auth = Mockito.mock(AuthSession.class);
		Authentication.getAuthTokenCache().put("testTokens", auth);
		
		mockRequest.setCookies(cookie);
		
		HttpServletResponse response = mockResponse;
		HttpServletRequest request = mockRequest;
		Authentication.invalidate(request, response, "");
		
		mockResponse = (MockHttpServletResponse) response;
		Cookie invalidatedCookie = mockResponse.getCookie(Constants.AUTH_TOKEN);
		ConcurrentMap<String,AuthSession> authCache = Authentication.getAuthTokenCache();
		assertEquals(1,authCache.keySet().size());
		assertNotNull(invalidatedCookie);
		assertEquals(invalidatedCookie.getName(),Constants.AUTH_TOKEN);
		assertEquals(invalidatedCookie.getMaxAge(),0);
	}
	
	@Test
	public void invalidateWithValidAuthSessionNoTokenTest() {
		AuthSession auth = Mockito.mock(AuthSession.class);
		Authentication.getAuthTokenCache().put("testToken", auth);
		
		HttpServletResponse response = mockResponse;
		HttpServletRequest request = mockRequest;
		Authentication.invalidate(request, response, "");
		
		mockResponse = (MockHttpServletResponse) response;
		Cookie invalidatedCookie = mockResponse.getCookie(Constants.AUTH_TOKEN);
		ConcurrentMap<String,AuthSession> authCache = Authentication.getAuthTokenCache();
		assertEquals(1,authCache.keySet().size());
		assertNull(invalidatedCookie);
	}
	
	@Test (expected = RuntimeException.class)
	public void invalidateWithValidAuthSessionNoValueTest() {
		AuthSession auth = Mockito.mock(AuthSession.class);
		Authentication.getAuthTokenCache().put("testToken", auth);
		cookie.setValue(null);
		mockRequest.setCookies(cookie);
		
		HttpServletResponse response = mockResponse;
		HttpServletRequest request = mockRequest;
		Authentication.invalidate(request, response, "");
		
		mockResponse = (MockHttpServletResponse) response;
		Cookie invalidatedCookie = mockResponse.getCookie(Constants.AUTH_TOKEN);
		ConcurrentMap<String,AuthSession> authCache = Authentication.getAuthTokenCache();
		assertEquals(0,authCache.keySet().size());
		assertNotNull(invalidatedCookie);
		assertEquals(invalidatedCookie.getName(),Constants.AUTH_TOKEN);
		assertEquals(invalidatedCookie.getMaxAge(),0);
	}
	
	@Test
	public void validatewithUserNameAndPassTest() {
		mockRequest.setRequestURI("/resolve/service/login");
		mockRequest.setQueryString("_login_=admin&_password_=resolve");
		mockRequest.setParameter("_username_", "admin");
		mockRequest.setParameter("_password_", "resolve");
		mockRequest.setRemoteHost("testhost.com");
		mockRequest.setRemoteAddr("testHost.com");
		try {
			Authentication.validate(mockRequest, mockResponse);
		} catch (Exception e) {
			e.printStackTrace();
			fail();
		}
		ConcurrentMap<String,AuthSession> authCache = Authentication.getAuthTokenCache();
		assertEquals(1,authCache.keySet().size());
		authCache.keySet().forEach(key -> {
			AuthSession sess = authCache.get(key);
			assertNotNull(sess);
			assertEquals("testHost.com",sess.getHost());
			assertEquals("admin",sess.getUsername());
			assertEquals(key,sess.getToken());
		});
	}
	
	@Test (expected = RSAuthException.class)
	public void FailedvalidationwithUserNameAndPassTest() throws Exception {
		mockRequest.setRequestURI("/resolve/service/login");
		mockRequest.setQueryString("_login_=admin&_password_=resolve");
		mockRequest.setParameter("_username_", "admin");
		mockRequest.setParameter("_password_", "");
		mockRequest.setRemoteHost("testhost.com");
		mockRequest.setRemoteAddr("testHost.com");
		try {
			Authentication.validate(mockRequest, mockResponse);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			ConcurrentMap<String,AuthSession> authCache = Authentication.getAuthTokenCache();
			assertEquals(0,authCache.keySet().size());
			e.printStackTrace();
			throw e;
			
		}
		fail();
	}
	
	@Test
	public void logoutAfterValidatedTest() {
		mockRequest.setRequestURI("/resolve/service/login");
		mockRequest.setQueryString("_login_=admin&_password_=resolve");
		mockRequest.setParameter("_username_", "admin");
		mockRequest.setParameter("_password_", "resolve");
		mockRequest.setParameter("_logout_", "true");
		mockRequest.setRemoteHost("testhost.com");
		mockRequest.setRemoteAddr("testHost.com");
		try {
			Authentication.validate(mockRequest, mockResponse);
		} catch (Exception e) {
			e.printStackTrace();
			fail();
		}
		ConcurrentMap<String,AuthSession> authCache = Authentication.getAuthTokenCache();
		assertEquals(0,authCache.keySet().size());
	}
	
	@Test (expected = RSAuthException.class)
	public void logoutafterFailedValidationTest() throws Exception {
		mockRequest.setRequestURI("/resolve/service/login");
		mockRequest.setQueryString("_login_=admin&_password_=resolve");
		mockRequest.setParameter("_username_", "admin");
		mockRequest.setParameter("_password_", "");
		mockRequest.setParameter("_logout_", "true");
		mockRequest.setRemoteHost("testhost.com");
		mockRequest.setRemoteAddr("testHost.com");
		try {
			Authentication.validate(mockRequest, mockResponse);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			ConcurrentMap<String,AuthSession> authCache = Authentication.getAuthTokenCache();
			assertEquals(0,authCache.keySet().size());
			e.printStackTrace();
			throw e;
		}
	}
	
	@Test (expected = RSAuthException.class)
	public void logoutAfterValidatedWithPlainTokenTest() throws Exception {
		AuthSession auth = Mockito.mock(AuthSession.class);
		Authentication.getAuthTokenCache().put("testToken", auth);
		mockRequest.setRequestURI("/resolve/service/login");
		mockRequest.setParameter("_logout_", "true");
		mockRequest.setParameter(Constants.HTTP_REQUEST_INTERNAL_TOKEN,"testToken");
		mockRequest.setRemoteHost("testhost.com");
		mockRequest.setRemoteAddr("testHost.com");
		try {
			Authentication.validate(mockRequest, mockResponse);
		} catch (Exception e) {
			ConcurrentMap<String,AuthSession> authCache = Authentication.getAuthTokenCache();
			assertEquals(0,authCache.keySet().size());
			e.printStackTrace();
			throw e;
		}
		fail();
	}
	
	@Test
	public void logoutAfterValidatedWithEncryptedTokenTest() {
		AuthSession auth = Mockito.mock(AuthSession.class);
		Mockito.when(auth.getUpdateTimeForSession("1")).thenReturn(10000L);
		Mockito.when(auth.getUpdateTime()).thenReturn(System.currentTimeMillis());
		Mockito.when(auth.getToken()).thenReturn("testToken/1000");
		Mockito.when(auth.getSessionTimeout()).thenReturn(10000L);
		Mockito.when(auth.getUsername()).thenReturn("admin");
		Mockito.when(auth.getClientReqURLHostName()).thenReturn("localhost");
		Authentication.getAuthTokenCache().put("testToken/1000", auth);
		mockRequest.setRequestURI("/resolve/service/login");
		mockRequest.setParameter("_logout_", "true");
		mockRequest.setParameter(Constants.HTTP_REQUEST_INTERNAL_TOKEN,CryptUtils.encryptToken("testToken/1000"));
		mockRequest.setRemoteHost("testhost.com");
		mockRequest.setRemoteAddr("testHost.com");
		HttpServletRequest request = new MultiReadHttpServletRequest((HttpServletRequest) mockRequest);
		try {
			Authentication.validate(request, mockResponse);
		} catch (Exception e) {
			e.printStackTrace();
			fail();
		}
		ConcurrentMap<String,AuthSession> authCache = Authentication.getAuthTokenCache();
		assertEquals(0,authCache.keySet().size());
	}
	
	@Test (expected = RSAuthException.class)
	public void logoutafterFailedValidationWithEncryptedTokenTest() throws Exception {
		mockRequest.setRequestURI("/resolve/service/login");
		mockRequest.setParameter("_logout_", "true");
		mockRequest.setParameter(Constants.HTTP_REQUEST_INTERNAL_TOKEN,CryptUtils.encryptToken("testTokens"));
		mockRequest.setRemoteHost("testhost.com");
		mockRequest.setRemoteAddr("testHost.com");
		try {
			Authentication.validate(mockRequest, mockResponse);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			ConcurrentMap<String,AuthSession> authCache = Authentication.getAuthTokenCache();
			assertEquals(0,authCache.keySet().size());
			e.printStackTrace();
			throw e;
		}
	}
	
	
	public Properties getHibernateProperties() {
		return hibernateProperties;
	}

	public void setHibernateProperties(Properties hibernatepPoperties) {
		this.hibernateProperties = hibernatepPoperties;
	}

}
