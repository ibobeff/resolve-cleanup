<!DOCTYPE html>
<%@page import="java.util.*,java.nio.charset.StandardCharsets,org.owasp.esapi.ESAPI,com.resolve.util.JspUtils,java.net.*" %>
<% 
    String[] csrfTokenForPageNameAndVal = JspUtils.getCSRFTokenForPage(request, "/resolve/jsp/rsclient.jsp");
	
    Map<String, String[]> parameterMap = request.getParameterMap();
    StringBuilder sb = new StringBuilder("");
	
	Set<String> parameterKeys = parameterMap.keySet();
	
	String csrfTokenForPageStr = "";
	String refererStr = "";
	
	if (!parameterKeys.contains(csrfTokenForPageNameAndVal[0]))
	{
	    csrfTokenForPageStr = "&" + URLEncoder.encode(csrfTokenForPageNameAndVal[0], StandardCharsets.UTF_8.name()) + "=" + URLEncoder.encode(csrfTokenForPageNameAndVal[1], StandardCharsets.UTF_8.name());
//  	    System.out.println("csrfTokenForPageStr=[" + csrfTokenForPageStr + "]");
	}
// 	else
// 	{
//  	    System.out.println("Parameter map contains " + csrfTokenForPageNameAndVal[0] + " = " + parameterMap.get(csrfTokenForPageNameAndVal[0])[0]);
// 	}
	
	if (request.getHeader("Referer") == null || request.getHeader("Referer").length() == 0)
	{
	    refererStr = "&Referer=" + URLEncoder.encode(request.getRequestURL().toString(), StandardCharsets.UTF_8.name());
 	}
	else
	{
	    refererStr = "&Referer=" + URLEncoder.encode(request.getHeader("Referer"), StandardCharsets.UTF_8.name());
	}
	
//  	System.out.println("refererStr=[" + refererStr + "]");
	
    for (String key: parameterKeys)
    {
		String[] paramVals = parameterMap.get(key);
		
		for (int i = 0; i < paramVals.length; i++)
		{
		    // If key name matches CSRF token name then filter out invalid values keeping only valid single value
		    
		    boolean add = true;
		    
		    if (key.equals(csrfTokenForPageNameAndVal[0]))
		    {
// 		        System.out.println("/resolve/jsp/rsclient.jsp " + csrfTokenForPageNameAndVal[0] + 
// 		                           " actual value is " + csrfTokenForPageNameAndVal[1] + 
// 		                           " and value in parameter map at " + i + " is " + paramVals[i]);
		        
		        if (!csrfTokenForPageNameAndVal[1].equals(paramVals[i]))
		        {
		            paramVals[i] = csrfTokenForPageNameAndVal[1];
// 		            System.out.println("paramVals[" + i + "]=" + paramVals[i] + ", add is " + (add ? "true" : "false"));
		        }
		        else
		        {
		            add = false;
		        }
		    }
		    
		    if (add)
		    {
		        sb.append("&").append(key).append("=").append(URLEncoder.encode(paramVals[i], StandardCharsets.UTF_8.name()));
// 		        System.out.println("Added &" + key + "=" + URLEncoder.encode(paramVals[i], "UTF-8") + " to query string");
		    }
		}
    }
    
    String inputParams = sb.toString();
%>
<html>
<head>
  <script type="text/javascript">
	document.write('<meta http-equiv="refresh" content="0; url=/resolve/jsp/rsclient.jsp?cb=' + Math.round((new Date).getTime()/1e3) + '<%=ESAPI.encoder().encodeForHTML(csrfTokenForPageStr)%>' + '<%=ESAPI.encoder().encodeForHTML(refererStr)%>' + '<%=ESAPI.encoder().encodeForHTML(inputParams)%>' + '">');
  </script>
</head>
</html>
