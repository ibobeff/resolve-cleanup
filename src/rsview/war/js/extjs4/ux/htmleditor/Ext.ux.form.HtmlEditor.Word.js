/**
 * @author Shea Frederick - http://www.vinylfox.com
 * @class Ext.ux.form.HtmlEditor.Word
 * @extends Ext.util.Observable
 * <p>A plugin that creates a button on the HtmlEditor for pasting text from Word without all the jibberish html.</p>
 */

Ext.define('Ext.ux.form.HtmlEditor.Word', {
	extend: 'Ext.AbstractPlugin',
	alias: 'plugin.wordpaste',

	// Word language text
	langTitle: 'Word Paste',
	langToolTip: 'Cleanse text pasted from Word',
	wordPasteEnabled: true,
	// private
	curLength: 0,
	lastLength: 0,
	lastValue: '',
	// private
	init: function(cmp) {
		this.cmp = cmp;
		this.cmp.on('render', this.onRender, this);
		this.cmp.on('initialize', this.onInit, this, {
			delay: 100,
			single: true
		});

	},
	// private
	onInit: function() {
		this.lastValue = this.cmp.getValue();
		this.curLength = this.lastValue.length;
		this.lastLength = this.lastValue.length;

	},
	// private
	findValueDiffAt: function(val) {

		for (i = 0; i < this.curLength; i++) {
			if (this.lastValue[i] != val[i]) {
				return i;
			}
		}

	},
	/**
	 * Cleans up the jubberish html from Word pasted text.
	 * @param wordPaste String The text that needs to be cleansed of Word jibberish html.
	 * @return {String} The passed in text with all Word jibberish html removed.
	 */
	fixWordPaste: function(wordPaste) {
		return wordPaste;
	},
	// private
	onRender: function() {
		this.cmp.getToolbar().add({
			iconCls: 'x-edit-wordpaste',
			itemId: 'wordpaste',
			pressed: true,
			handler: function(t) {
				t.toggle(!t.pressed);
				this.wordPasteEnabled = !this.wordPasteEnabled;
			},
			scope: this,
			tooltip: {
				text: this.langToolTip
			},
			overflowText: this.langTitle
		});

	}
});