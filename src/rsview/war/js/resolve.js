Ext.EventManager.on(window, 'keydown', function(e, t) {
	if(t.allowBackspace)
		return
	if (e.getKey() == e.BACKSPACE && ((!/^input$/i.test(t.tagName) && !/^textarea$/i.test(t.tagName)) || t.disabled || t.readOnly)) {
		e.stopEvent();
	}
});

function gotoURL(windowName, url) {
	var params = windowName.substring(windowName.indexOf('?') + 1);
	var target = '';
	var arrOfStrings = params.split('&');
	for (var i = 0; i < arrOfStrings.length; i++) {
		var value = arrOfStrings[i];
		if (value && value.indexOf('=') > -1) {
			var kvarr = value.split('=');
			if (kvarr[0] == 'target' && Ext.String.trim(kvarr[1]) != '') {
				target = kvarr[1];
				break;
			}
		}
	}
	target = Ext.String.trim(target);
	if (!target) {
		doAddTab(windowName, url);
	} else {
		doOpenWindow(windowName, target, url)
	}
}

function doOpenWindow(windowName, target, url) {
		window.open(url, target);
	}
	//always add a new tab or open a new window

function doAddTab(windowName, url) {
	if (window.navigator.userAgent.indexOf('MSIE') != -1 || !windowName || windowName.indexOf('.') > 0) {
		window.open(url, '_blank');
	} else {
		if (windowName) {
			windowName = windowName.replace('#', '');
		}
		window.open(url, windowName);
	}
}

function closePopup() {
	window.parent.closePopupWindow();
}

function changeNode(uName, uNameSpace, inputOutputDesc, uDesc) {
	if (window.graph.getSelectionCell() != null) {
		var userObject = window.graph.getSelectionCell().getValue();
		labelDisplayNameBefore = userObject.getAttribute('label');
		userObject.setAttribute('label', uName);
		userObject.setAttribute('description', uName + "#" + uNameSpace);
		userObject.setAttribute('tooltip', inputOutputDesc);
		userObject.setAttribute('href', uName + "#" + uNameSpace);
		userObject.setAttribute('detail', uDesc);

		window.graph.getSelectionCell().setValue(userObject);

		labelDisplayNameAfter = userObject.getAttribute('label');

		if (labelDisplayNameBefore != labelDisplayNameAfter) {
			window.thisModified = true;
			window.updateTitle();
		}

		window.graph.setAutoSizeCells = true;

		var styleName = window.graph.getSelectionCell().getStyle();

		window.updateConfig(styleName);

		window.graph.getView().refresh();
	}
}

function changeNodeRunbook(uName) {
	if (window.graph.getSelectionCell() != null) {
		var userObject = window.graph.getSelectionCell().getValue();
		labelDisplayNameBefore = userObject.getAttribute('label');
		userObject.setAttribute('label', uName);
		window.graph.getSelectionCell().setValue(userObject);

		labelDisplayNameAfter = userObject.getAttribute('label');

		if (labelDisplayNameBefore != labelDisplayNameAfter) {
			window.thisModified = true;
			window.updateTitle();
		}

		window.graph.setAutoSizeCells = true;

		var styleName = window.graph.getSelectionCell().getStyle();

		window.updateConfig(styleName);

		window.graph.getView().refresh();
	}
}

function setTitle(title) {
	if (title) {
		setParentTitle(title, window, window.parent);
	}
}

// Set the document title by recursively finding parent

function setParentTitle(title, child, parent) {
	if (child != parent) {
		setParentTitle(title, parent, parent.parent);
	} else {
		child.document.title = title;
		return;
	}
}

function openPage(web, doc, opt) {
	win = open("/xwiki/bin/view/" + web + "/" + doc + "?" + opt, doc, "titlebar=0,width=950,height=500,resizable,scrollbars");
	if (win) {
		win.focus();
	}
	if (win.opener == null) {
		win.opener = self;
	}
}

function openURL(url) {
	win = open(url, "", "titlebar=0,width=950,height=500,resizable,scrollbars");
	if (win) {
		win.focus();
	}
	if (win.opener == null) {
		win.opener = self;
	}
}

function showhide(id, imgId) {
	if (document.getElementById) {
		obj = document.getElementById(id);
		imageObj = document.getElementById(imgId);

		if (obj.style.display == "none") {
			obj.style.display = "";
			imageObj.src = "/resolve/images/collapseall.gif";
		} else {
			obj.style.display = "none";
			imageObj.src = "/resolve/images/expandall.gif";
		}
	}
}

function toggle(id) {
	obj = document.getElementById(id);
	if (obj.style.display == "none") {
		obj.style.display = "";
	} else {
		obj.style.display = "none";
	}
}

function trimString(str) {
	var trimStr = str.replace(/^\s+|\s+$/g, '');
	return trimStr;

}

function submitComboBoxDecisionRequest(resultDivElement, selectedElement, mainElement) {
	submitDecisionRequest(resultDivElement, selectedElement.getValue(), mainElement);
}

function submitRadioDecisionRequest(resultDivElement, radioButtons, mainElement) {
	var selectedElement = null;

	for (var x = 0; x < radioButtons.length; x++) {
		if (radioButtons[x].checked) {
			selectedElement = radioButtons[x];
			break;
		}
	}
	submitDecisionRequest(resultDivElement, selectedElement.value, mainElement);
}

function submitDecisionRequest(resultDivElement, docFullNameValue, mainElement) {
	var metricData = mainElement.query('div[name=data]')[0].innerHTML;
	var problemId = getProblemIdFromWiki();

	if (problemId == 'NEW') {
		problemId = '';
	}
	resultDivElement.load({
		url: '/resolve/service/wiki/include',
		scripts: true,
		params: {
			docFullName: docFullNameValue,
			metricData: metricData,
			PROBLEMID: problemId,
			isSubmitted: true
		},
		renderer : function(loader, response, active){
			var decodedContent = decodeHTML(response.responseText);
		  	loader.getTarget().update(decodedContent, active.scripts === true);
    		return true;
		},
		scope: this
	});

	resultDivElement.scrollIntoView();

}
function decodeHTML(html){
    var txt = document.createElement("textarea");
    txt.innerHTML = html;
    return txt.value;
}

function retriggerExtjsElements() {
	triggerExtjsWithName("__results");
	triggerExtjsWithName("__details");
	refreshModelLinks();
}

function refreshModelLinks() {
	var modelFrames = Ext.query('iframe[src^=/resolve/service/wiki/viewmodel]');
	Ext.Array.forEach(modelFrames, function(frame) {
		var splits = Ext.fly(frame).getAttribute('src').split('?'),
			params = Ext.Object.fromQueryString(splits[1]);
		if (params.PROBLEMID != getProblemIdFromWiki()) {
			params.PROBLEMID = getProblemIdFromWiki()
			frame.src = splits[0] + '?' + Ext.Object.toQueryString(params)
		}
	})
}

function triggerExtjsWithName(elementName) {
	var jsFunctions = document.getElementsByName(elementName),
		ln;
	for (var i = 0, ln = jsFunctions.length; i < ln; i++) {
		invokeFunction(jsFunctions[i].value);
	}
}

function closeTab() {
	closeTabRecurse(parent);
}

function closeTabRecurse(parent) {
	if (parent == parent.parent) {
		setTimeout(function() {
			parent.close()
		}, 500);
	} else {
		closeTabRecurse(parent.parent);
	}
}

/**
 *
 * api to trigger execution through javascript
 *
 */

function __getProblemIdDiv() {
	return Ext.DomQuery.select('div[id^=_problemId_]');
}

function getProblemIdFromWiki() {
	return clientVM.problemId || 'NEW'
		// var probIdValue = 'NEW';

	// //get the div that has id starting with _problemId_
	// var probIdDiv = __getProblemIdDiv();

	// if (probIdDiv && probIdDiv.length > 0) {
	// 	var probIdInnerHtml = probIdDiv[0].innerHTML;
	// 	if (probIdInnerHtml && probIdInnerHtml.indexOf("$") == -1) {
	// 		probIdValue = probIdInnerHtml;
	// 	}
	// }
	// return probIdValue;
}

function setProblemIdOnWiki(problemId) {
	var resolveClient =  getResolveRoot().clientVM;
	if (resolveClient && resolveClient.problemId != problemId) 
		resolveClient.updateProblemInfo(problemId);
		//get the div that has id starting with _problemId_
	var probIdDiv = __getProblemIdDiv();
	if (probIdDiv && probIdDiv.length > 0) {
		Ext.get(probIdDiv[0]).update(problemId);
	}
}

function getFormInputElementsToUrl(myform) {
	//get all the input elements in this form element
	var allEles = Ext.DomQuery.select('form[name="' + myform + '"] input');
	var url = '';
	var params = {};
	Ext.each(allEles, function(el) {
		if ((el.type.toLowerCase() == 'radio' || el.type.toLowerCase() == 'checkbox')) {
			if(el.checked){
				if(!params[el.name] )
					params[el.name] = [];
				params[el.name].push(el.value);
			}
			return;
		}
		if (el.type.toLowerCase() != 'button' && el.type.toLowerCase() != 'submit'){
			if(!params[el.name] )
				params[el.name] = [];
			params[el.name].push(el.value);
		}
	});
	var selectEls = Ext.DomQuery.select('form[name=' + myform + '] select');
	Ext.each(selectEls, function(el) {
		for(var i = 0; i < el.length; i++){
			var option = el[i];
			if (option.selected){
				if(!params[el.name] )
					params[el.name] = [];
				params[el.name].push(option.value);
			}
		}
		
	});
	var textAreaEls = Ext.DomQuery.select('form[name=' + myform + '] textarea');
	Ext.each(textAreaEls, function(el) {
		if(!params[el.name] )
			params[el.name] = [];
		params[el.name].push(el.value);
	})
	if (!params['PROBLEMID'])
		params['PROBLEMID'] = getProblemIdFromWiki();
	return Ext.Object.toQueryString(params);
}

function executeRunbookUsingAjax(myform) {
	var url = '/resolve/service/execute';
	var resolveClient = getResolveRoot().clientVM;
	//get the parameters from the form
	var params = 'AJAXCALL=true&';
	params += getFormInputElementsToUrl(myform);
	params += '&RESOLVE.ORG_NAME=' + resolveClient.orgName + '&RESOLVE.ORG_ID=' + resolveClient.orgId;
	Ext.Ajax.request({
		url: url,
		params: params,
		success: function(response) {
			var text = response.responseText;
			var jsonObj = Ext.JSON.decode(text);
			var problemIdFromServer = jsonObj.problemId;
			
			if (resolveClient) {
				if(resolveClient.problemId == problemIdFromServer)
					resolveClient.fireEvent('refreshResult');
				else 
					resolveClient.updateProblemInfo(problemIdFromServer);
				resolveClient.fireEvent('worksheetExecuted');
			}		
			try {
				if (resolveClient) 
					resolveClient.displaySuccess('Runbook submitted successfully.')
				else 
					Ext.MessageBox.alert('Success', 'Runbook submitted successfully.');
				setProblemIdOnWiki(problemIdFromServer);
				retriggerExtjsElements();
			} catch (e) {}
		},
		failure: function(response) {},
		scope: this
	});

}

function executeEventUsingAjax(myform) {
	var url = '/resolve/service/event/execute';
	var resolveClient = getResolveRoot().clientVM;
	var params = 'AJAXCALL=true&';

	//get the parameters from the form
	params += getFormInputElementsToUrl(myform);
	params += '&RESOLVE.ORG_NAME=' + resolveClient.orgName + '&RESOLVE.ORG_ID=' + resolveClient.orgId;
	Ext.Ajax.request({
		url: url,
		params: params,
		success: function(response) {
			var text = response.responseText;
			var jsonObj = Ext.JSON.decode(text);
			//var problemIdFromServer = jsonObj.problemId;

			try {
				Ext.MessageBox.alert('Success', 'Event submitted successfully.');
				//setProblemIdOnWiki(problemIdFromServer);
				retriggerExtjsElements();
			} catch (e) {}
		},
		failure: function(response) {

		},
		scope: this
	});

}

function getDecisions() {
	var decisionForms = document.getElementsByName("WikiDecisionForm");
	var result = {};

	if (decisionForms != null) {
		for (var i = 0; i < decisionForms.length; i++) {
			var decisionForm = decisionForms[i];
			var isSelected = false;
			var tmpResult = "";
			var answer = "unknown";
			for (var j = 0; j < decisionForm.elements.length; j++) {
				var decisionElement = decisionForm.elements[j];
				if (decisionElement.type == 'button') {
					var onClick = decisionElement.onclick.toString();
					var divIdx = onClick.indexOf('div_result');
					var divName = "";
					if (divIdx > 0) {
						divName = onClick.substring(divIdx);
					}
					divIdx = divName.indexOf('")');
					if (divIdx == -1) {
						divIdx = divName.indexOf("')");
					}
					if (divIdx > 0) {
						divName = divName.substring(0, divIdx);
					}

					var div = document.getElementById(divName);
					if (div != null) {
						if (div.innerHTML != "") {
							isSelected = true;
						}
					}
				}
				if (decisionElement.type == 'radio') {
					if (decisionElement.checked) {
						var labelId = decisionElement.id;
						if (labelId != "") {
							labelId = labelId + "_label";
							var label = document.getElementById(labelId);
							if (label != null) {
								answer = label.innerHTML;
							}
						}
						tmpResult = decisionElement.value;
					}
				}
				if (decisionElement.type == 'select-one') {
					var options = decisionElement.options;
					for (var k = 0; k < options.length; k++) {
						var option = options[k];
						if (option.selected) {
							answer = option.innerHTML;
							tmpResult = option.value;
						}
					}
				}
			}
			if (isSelected) {
				if (tmpResult != "") {
					var docIdx = tmpResult.indexOf("?");
					if (docIdx > 0) {
						tmpResult = tmpResult.substring(0, docIdx);
					}
				}
				result[answer] = tmpResult;
			}
		}
	}
	return result;
}

function getResolveRoot() {
	var theWindow = window;
	try {
		while (theWindow.parent.RS && theWindow.parent != theWindow)
			theWindow = theWindow.parent
	} catch (e) {
		//cross-site scripting err,meaning we've reached the root ignore
	}
	return theWindow;
}

function saveWsdataProperty(key, value) {
	var url = '/resolve/service/wsdata/save';
	var params = 'propertyName=' + key + '&propertyValue=' + value;
	
	Ext.Ajax.request({
		url: url,
		params:params,
		success: function(response) {
		},
		failure: function(response) {

		},
		scope: this
	});
}

function getWsdataProperty(key) {
	var url = '/resolve/service/wsdata/get';
	var params = 'propertyName=' + key;
	var value;
	
	Ext.Ajax.request({
		url: url,
		async : false,
		params:params,
		success: function(response) {
			var text = response.responseText;
			var jsonObj = Ext.JSON.decode(text);
			value=jsonObj.data;
		},
		failure: function(response) {

		},
		scope: this
	});
	
	return value;
}