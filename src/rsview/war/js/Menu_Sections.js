const menuSets = [
  {
    id: 'all',
    name: 'All',
    items: [
      'menu',
      'securityOperations',
      'developmentTools',
      'runbookCatalog',
      'contentRequest',
      'crAdmin',
      'worksheet',
      'requestHistory',
      'archiveHistory',
      'gatewayAdmin',
      'actionDefinition',
      'socialAdmin',
      'wikiAdmin',
      'reports',
      'reportsAdmin',
      'userAdmin',
      'tableAdmin', 
      'resolutionRouting', 
      'menuAdmin',
      'systemAdmin', 
      'impex',
      'systemLogs', 
      'help'
    ]
  }
]

const menuSections = {
  menu: {
    name: "Main Menu",
    link: 'RS.client.Menu'
  },
  securityOperations: {
    name: "Security Operations",
    items: [
      { name: "Investigation Dashboard", id: "8a9494a959f1499f0159f1f76ae3001b" },
      { name: "Playbook Templates", id: "8a9494a959f1499f0159f1f76ae4001c" },
      { name: "Artifact Configurations", id: "8a9494c15eab7b2f015eabb56e2f0004" }
    ]
  },
  runbookCatalog: {
    name: "Runbook Catalog",
    items: [
      { name: "Homepage", id: "3f8f1f68c6112272012e4ff923e661ce" },
      { name: "Runbooks", id: "6c5ba982c611227201fdc6f6df379eac" }
    ]
  },
  contentRequest: {
    name: "Content Request",
    items: [
      { name: "New Request", id: "8a9482e6392c162301392cdef12e00d9" },
      { name: "Submit for Review", id: "8a9482e6392c162301392cdf1be900da" },
      { name: "My Requests", id: "8a9482e6392c162301392cdf4d3600db" },
      { name: "My Assigned", id: "8a9482e6392c162301392cdf734400dc" },
      { name: "My Closed", id: "8a9482e6392c162301392ce9228f00e1" }
    ]
  },
  crAdmin: {
    name: "CR Administration",
    items: [
      { name: "All Requests", id: "8a9482e6392c162301392c64477f00a3" },
      { name: "New", id: "8a9482e6392c162301392cdad3d700d0" },
      { name: "Unassigned", id: "8a9482e6392c162301392cdb24c500d2" },
      { name: "Pending Testing", id: "8a9482e6392c162301392cdb745d00d4" },
      { name: "Pending Approval", id: "8a9482e6392c162301392cdb950b00d5" },
      { name: "Pending Deployment", id: "8a9482e6392c162301392cdbdcae00d6" },
      { name: "Pending Close", id: "8a9482e6392c162301392cdc0b3f00d7" },
      { name: "Open", id: "8a9482e6392c162301392cdb53a300d3" },
      { name: "Developing", id: "8a9482e6392c162301392ce7b9f400dd" },
      { name: "Testing", id: "8a9482e6392c162301392ce7e03600de" },
      { name: "Deploying", id: "8a9482e6392c162301392ce80a4800df" },
      { name: "Closed", id: "8a9482e6392c162301392ce8b32600e0" },
      { name: "Aborted", id: "8a9482e63b0190a1013b028ab1750111" },
      { name: "Namespace Mapping", id: "8a9482e5454d6e2b01454dda6bc300fe" },
      { name: "Rejected", id: "8a9482e63b956078013b9c415070006d" }
    ]
  },
  worksheet: {
    name: "Worksheet",
    items: [
      { name: "New Worksheet", id: "6c314db2c6112272009e40d2e2af0221" },
      { name: "My Worksheets", id: "f0faba78c61122720025c6e8a024c381" },
      { name: "Unassigned", id: "f1471a69c6112272007d6ca2ec633a6e" },
      { name: "All Worksheets", id: "f1071281c611227200833a1440f0eb54" }
    ]
  },
  requestHistory: {
    name: "Request History",
    items: [
      { name: "Open", id: "e24a1d00c6112272004145b1fd3cbcd5" },
      { name: "Completed", id: "f628cdc0c611227201ee88dd427e08b4" },
      { name: "Aborted", id: "cf7828a4c611227200bead4fbde5c1d0" },
      { name: "Results", id: "2f4cd863c6112272019e920004b7b004" }
    ]
  },
  gatewayAdmin: {
    name: "Gateway Administration",
    items: [
      /*
      { name: "Gateway List", id: "8ab282e56914dbd201697c5dab5304a4" },
      { name: "Gateway Filters List", id: "4028a18169c5ba460169c64df9de0278" }
      */
    ]
  },
  actionDefinition: {
    name: "Action Definition",
    items: [
      { name: "My Automations", id: "8a9482f148b3d8280148b42ac0790392" },
      { name: "All Automations", id: "8a9482f148b3d8280148b42ac07b0393" },
      { name: "My ActionTasks", id: "d8e09a69c6112272009a9d3b40962cb1" },
      { name: "All ActionTasks", id: "d85b810bc611227201cf33a6b25f2044" },
      { name: "Properties", id: "955f84d9c6112272013be41607c510e2" },
      { name: "Tag Definition", id: "d85b0dafc611227200f0d3a659f0e721" },
      { name: "CNS Definition", id: "e1260de9c611227200fd5428588faacf" }
    ]
  },
  socialAdmin: {
    name: "Social Administration",
    items: [
      { name: "Process Definition", id: "8a9482e63f5eb7ad013f5f09ae580027" },
      { name: "Team Definition", id: "8a9482e63f5eb7ad013f5f0a0ea0002a" },
      { name: "Forum Definition", id: "8a9482e63f5eb7ad013f5f0a7355002d" },
      { name: "RSS Definition", id: "8a9482e63f5eb7ad013f5f0aa7c50030" }
    ]
  },
  wikiAdmin: {
    name: "Wiki Administration",
    items: [
      { name: "Namespace Administration", id: "8a9482e64055e457014059cefae9000c" },
      { name: "Wiki Administration", id: "a6824f72c61122720130adbf383edcd6" },
      { name: "Global Attachments", id: "8a9482e64055e457014059d04b7d000d" },
      { name: "Tag Definition", id: "d85a9c39c611227200d52411b9cfc2f1" },
      { name: "Template Definition", id: "8a9482e63a71517b013a716991670006" },
      { name: "Lookup Mapping", id: "31cec593c0a8a21a01f31d15c42ed045" },
      { name: "Catalog Builder", id: "8a9482e63e865b41013e866203080004" }
    ]
  },
  reports: {
    name: "Resolve Reports",
    items: [
      { name: "Business Reports", id: "8a9482e549ca1db40149ca33cc8e013c" },
      { name: "Operation Reports", id: "8a9482e549ca1db40149ca33cc95013e" },
      { name: "Development Reports", id: "8a9482e549ca1db40149ca33cc950140" },
      { name: "Admin Reports", id: "8a9482e549ca1db40149ca33cc950142" },
      { name: "Incident Reports", id: "8a9482ba668980750166898592930003" }
    ]
  },
  reportsAdmin: {
    name: "Report Administration",
    items: [
      { name: "Runbook Properties", id: "2c9181e6324aca4701324aedaf2c001a" },
      { name: "Task Properties", id: "8a9482e64dd50bc2014dd5b003fb0070" },
      { name: "Timer Properties", id: "8a9482e64dc088bf014dc50f48df0189" },
      { name: "MCP", id: "8a949451552d4b6f01552d703d3f0005" }
    ]
  },
  archiveHistory: {
    name: "Archive History",
    items: [
      { name: "Archive Worksheets", id: "2c9181e631c04e5c0131c055475e0008" },
    ]
  },
  userAdmin: {
    name: "User Administration",
    items: [
      { name: "Users", id: "555aed3fc0a8016600657c7b0ddc6e97" },
      { name: "Groups", id: "0e57b4d7c0a8016401796649a62fcde3" },
      { name: "Organizations", id: "8a9494c15c7992de015c84a94792009f" },
      { name: "Roles", id: "282ac91bc611228500862041c32642e0" },
      { name: "Active Directory Configuration", id: "8a9482e63ff25f2f013ff3cfef7500a2" },
      { name: "LDAP Configuration", id: "8a9482e63ff25f2f013ff3d11e6100a4" },
      { name: "RADIUS Configuration", id: "8a9494c655e094160155e097df390004" }
    ]
  },
  tableAdmin: {
    name: "Table Administration",
    items: [
      { name: "View Tables", id: "ff808081300538a3013005448bf907ac" },
      { name: "View Forms", id: "2c9181e6311a399101311afa12d10006" }
    ]
  },
  resolutionRouting: {
    name: "Resolution Routing",
    items: [
      { name: "Resolution ID Schema", id: "8a9482e64c5cb9cb014c5cc751e80067" },
      { name: "Resolution ID Mapping", id: "8a9482e64c5cb9cb014c5cc8769d0069" }
    ]
  },
  menuAdmin: {
    name: "Menu Administration",
    items: [
      { name: "Menu Definition", id: "8a9482e641dbe8bc0141dd0f40600007" },
      { name: "Menu Set", id: "8a9482e641dbe8bc0141dd0f40610008" },
      { name: "Toolbar Definition", id: "8a9482e641dbe8bc0141dd0f40610009" }
    ]
  },
  systemAdmin: {
    name: "System Administration",
    items: [
      { name: "List Registration", id: "941c2513c611227200407072a9fc8e1a" },
      { name: "Session Information", id: "2c9181e630a026250130a02d8aab000e" },
      { name: "Server Information", id: "2c9181e630a026250130a02e6253000f" },
      { name: "Job Scheduler", id: "8a9482e641dbe8bc0141dd117a13000b" },
      { name: "System Properties", id:"f830ee2bc61122720090d4d1a198b418" },
      { name: "Business Rules", id: "ff80808129249f92012937946204001a" },
      { name: "System Scripts", id: "ff80808129249f92012937978a3d001c" },
      { name: "Application Security", id: "8a9482e640bb51180140bb9f87720012" }
    ]
  },
  impex: {
    name: "Import Export",
    items: [
      { name: "Package Manager", id: "8a9482ba6621b237016621bd618b0025" },
      { name: "Import Export Module", id: "bc5e67aec6112272007704dd14e34df2" },
      { name: "Import Export Log", id: "8a9482e63b879c28013b8b817e180131" }
    ]
  },
  systemLogs: {
    name: "System Logs",
    items: [
      { name: "Alert Log", id: "8a9482e63b879c28013b8b802c17012b" },
      { name: "Audit Log", id: "8a9482e63b879c28013b8b807d42012d" },
      { name: "Import Export Log", id: "8a9482e63b879c28013b8b80c4ab012f" },
      { name: "Gateway Log", id: "8a9482f13d5bb4c0013d5bc3d5ad000e" }
    ]
  },
  help: {
    name: "Help",
    items: [
      { name: "Support Help", id: "6c5d1382c611227200d8116d513b2a3e" },
      { name: "License Info", id: "8a9481ce3da398b0013da3a6f8f00003" },
      { name: "About", id: "ad3157aec6112272016ff5a3b9b6fae4" }
    ]
  },
  developmentTools: {
    name: "Development Tools",
    items: [
      { name: 'Content Browser', id: "8a9482ba661e468101662191cd7e0276" },
      { name: "ActionTask Builder", id: "8a9482ba661e46810166219065890271" },
      { name: "Automation Designer", id: "8a9482ba661e468101662190658a0272" },
      { name: "Page Builder", id: "8a9482ba661e468101662190658a0273" },
      { name: "Decision Tree Builder", id: "8a9482ba661e468101662190658a0274" },
      { name: "Custom Form Builder", id: "8a9482ba661e468101662190658a0275" },
    ]
  }
};