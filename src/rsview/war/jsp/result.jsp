<!DOCTYPE html>
    <%@page contentType="text/html"%>
    <%@page pageEncoding="UTF-8"%>

    <%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
    <%@taglib uri="http://www.springframework.org/tags/form" prefix="form" %>
    <%@taglib uri="http://www.springframework.org/tags" prefix="spring" %>
    
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
        <META HTTP-EQUIV="Pragma" CONTENT="no-cache">
        <META HTTP-EQUIV="Expires" CONTENT="-1">
        <META HTTP-EQUIV="Expires" CONTENT="Tue, 01 Jan 1980 1:00:00 GMT">
        <jsp:include page="/client/module-loader.jsp" flush="true"/>
    </head>
    <body>
        <pre>
        ${content}
        </pre>
    </body>
</html>