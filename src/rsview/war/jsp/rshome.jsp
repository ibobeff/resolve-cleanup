<!DOCTYPE html>
<%@page import="com.resolve.util.JspUtils" %>
<%@page contentType="text/html"%>
<%@page pageEncoding="UTF-8"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%
    boolean debug = false;
    String d = request.getParameter("debug");
    if( d != null && d.indexOf("t") > -1){
    debug = true;
    }
%>
<html>
<head>
    <meta http-equiv="content-type" content="text/html; charset=UTF-8">
    <link rel="SHORTCUT ICON" href="/resolve/favicon.ico"/>

    <%
    String title = JspUtils.getResolveTitle();
    %>
    <title><%=title%></title>

    <!-- Resolve Modules -->
    <%@ include file="../client/common.jsp" %>
    <%@ include file="../client/loader.jsp" %>
    <%@ include file="../formbuilder/loader.jsp" %>

    <!-- Resolve Library -->
    <!-- <script type="text/javascript" src="/resolve/js/resolve.js"></script> -->

    <!-- Launch resolve app -->
    <script type="text/javascript">

        Ext.onReady(function() {
            //Initialize State Management
            if(Ext.supports.LocalStorage) Ext.state.Manager.setProvider(new Ext.state.LocalStorageProvider());
            else Ext.state.Manager.setProvider(new Ext.state.CookieProvider());

            //Initialize extjs tooltips
            Ext.tip.QuickTipManager.init();

            //Launch Resolve
            glu.viewport({
                mtype : 'RS.Main'
            });
        });

    </script>

</head>

<body>
</body>
</html>