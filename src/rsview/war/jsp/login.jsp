<!DOCTYPE html>
<%@page import="org.owasp.esapi.ESAPI,com.resolve.util.JspUtils,com.resolve.rsview.model.Login" %>
<%@page import="java.io.File"%>
<%@ taglib prefix="esapi" uri="http://www.owasp.org/index.php/Category:OWASP_Enterprise_Security_API" %>
<%@ taglib uri="http://www.owasp.org/index.php/Category:OWASP_CSRFGuard_Project/Owasp.CsrfGuard.tld" prefix="csrf" %>
<% String ver = ESAPI.encoder().encodeForURL(JspUtils.getResolveVersion()); %>
<html>
  <head>
  <meta charset="utf-8">
  <title>Resolve RBA</title>
  <link rel="SHORTCUT ICON" href="/resolve/favicon.ico"/>
  <link rel="stylesheet" href="/resolve/css/login.css?_v=<%=ver%>" type="text/css"/>
  <link rel="stylesheet" href="/resolve/css/logo.css?_v=<%=ver%>" type="text/css"/>
  <link rel="stylesheet" type="text/css" href="/resolve/css/font-awesome.min.css?_v=<%=ver%>">
    <script src="/resolve/JavaScriptServlet?<csrf:tokenname/>=<csrf:tokenvalue uri="/resolve/JavaScriptServlet"/>"></script>
  <script>
    function onBodyLoad(){
        //if no cookie, focus the username
        var cookies = document.cookie.split(';'),
            username = document.getElementById('username'),
            password = document.getElementById('password'),
            rememberCheckbox = document.getElementById('rememberMe'),
            i=0, cookie=null, rememberMe=false, usernameString='', hashState = window.location.hash;
        
        //Persist hash state
        document.getElementById('hashState').value = hashState

        //Flag we are at login page
        window.sessionStorage.setItem('service-login', true);

        <%
            Login login = (Login)request.getAttribute("login");
            String sSOServiceId = login.getSSOServiceId();
            if (sSOServiceId == null || sSOServiceId.isEmpty())
            {
                String showError = login.getShowError();
                String error = ESAPI.encoder().encodeForURL(login.getError());
				String params = "urlData=" + ESAPI.encoder().encodeForURL(login.getUrl()) + "&iframeId=" + ESAPI.encoder().encodeForURL(login.getIframeId());
                if (!showError.equals("none")) {
                    String hashState = ESAPI.encoder().encodeForURL(login.getHashState().replace("#", ""));
                    String errorParam = "error=" + error;
                    out.write("        window.location = '/resolve/sir/index.html?hashState="+ hashState + "&" + errorParam + "&" + params + "#/login';\n");
                } else {
                    out.write("        var hash = encodeURIComponent(window.location.hash.replace(/^#/, ''));\n");
                    out.write("        window.location = '/resolve/sir/index.html?hashState='+hash+'&" + params + "#/login';\n");
                }
                out.write("        return;\n");

            }
            else if (sSOServiceId != null && !sSOServiceId.isEmpty())
            {
//                 System.out.println("" + sSOServiceId + " SSO Service Re-Login");
                
                out.write("//add the tz value\n");
                out.write("        var tz = document.getElementById('tz');\n");
                out.write("        tz.value = getTimezone();\n");
                out.write("        \n");
                out.write("        //set the user timezone\n"); 
                out.write("        setUserTimezone(tz.value)\n");
            }
        %>
          
      //center the login
      var marginTop = Math.round((document.documentElement.clientHeight - document.body.offsetHeight) / 2);
      document.getElementById('signinDiv').style.marginTop = marginTop+'px';
      document.body.style.visibility = 'visible';
      localStorage.removeItem('orgInfo');
    }
    

    function pad(number, length){
        var str = "" + number
        while (str.length < length) {
            str = '0'+str
        }
        
        return str
    }

    function getTimezone()
    {
        var offset = new Date().getTimezoneOffset()
        offset = 'GMT' + ((offset<0? '+':'-')+ // Note the reversed sign!
            pad(parseInt(Math.abs(offset/60)), 2)+ ':' +
            pad(Math.abs(offset%60), 2))
            
        return offset
    }

    function checkRemember(){
      disableLogin() //Disable login button after submit
      var exdate=new Date()
      exdate.setDate(exdate.getDate() + 365)
      return true;
    }
    
  function setUserTimezone(timezone) {
    document.cookie = 'browserTimeZone=' + timezone + ';';
  }

  function showHideLabel(inputId, labelId) {
    var input = document.getElementById(inputId), label = document
        .getElementById(labelId)
    if (input.value)
      label.style.display = 'none'
    else
      label.style.display = 'block'
  }

  function disableLogin() {
    document.getElementById('loginButton').disabled = true;
  }
    </script>
  </head>
   <body onload="onBodyLoad();">
   <div id="signinDiv" class="front-signin" style="margin:0 auto">
    <% 
      String type = JspUtils.getResolveInstanceType();
      String imgTag = "<img class='logo' src='/resolve/images/logo-login.png' />";
      try {
          File f = new File(getServletContext().getRealPath("/login/logo.png"));
          if (f.exists()){
              imgTag = "<img class='logo' src='/resolve/login/logo.png' />";
          } else if (type.equalsIgnoreCase("CLOUD")){
              imgTag = "<img class='logo-cloud' src='/resolve/images/logo-cloud.png' />";
          }
      } 
      catch (Exception e){}
      out.write(imgTag);
    %>
    
    <csrf:form name="login" action="/resolve/service/login" class="signin" method="post" onsubmit="return checkRemember()">
        <table class="flex-table password-signin">
          <tbody>
          <tr>
            <td class="flex-table-secondary">
              <button id="loginButton" type="submit" class="submit btn primary-btn flex-table-btn js-submit" tabindex="4" style="margin-right:3px;margin-bottom:10px;">
                <%if (sSOServiceId != null && !sSOServiceId.isEmpty())
                    {
                        String encodedLoginBtnLabel = login.getSSOSrvcLoginBtnLbl() != null && !login.getSSOSrvcLoginBtnLbl().isEmpty() ?
                                                      ESAPI.encoder().encodeForHTMLAttribute(login.getSSOSrvcLoginBtnLbl()): 
                                                      sSOServiceId + " Resolve Re-Login";
                                                      
//                      System.out.println(sSOServiceId + " Resolve Re-Login");
                        out.write(ESAPI.encoder().encodeForHTML(encodedLoginBtnLabel));
                    }                 
                %>
              </button>
            </td>
          </tr>
          </tbody>
        </table>

        <div class="placeholding-input username" id="errorMsg" style="display:${esapi:encodeForHTMLAttribute(login.showError)};color:red;font-weight:bold">
          ${esapi:encodeForHTML(login.error)}
        </div>
        
        <input id='tz' type='hidden' name='TIMEZONE_STR' value=''>
        <input name="url" type="hidden" value="${esapi:encodeForHTMLAttribute(login.url)}" size="60"/>
        <input id="hashState" name="hashState" type="hidden" value="${esapi:encodeForHTMLAttribute(login.hashState)}"/>
        <input name="iframeId" type="hidden" value="${esapi:encodeForHTMLAttribute(login.iframeId)}" size="60"/>
        <%
            if (sSOServiceId != null && !sSOServiceId.isEmpty())
            {
                String encodedSSOServiceId = ESAPI.encoder().encodeForHTMLAttribute(sSOServiceId);
                out.write("<input name=\"sSOServiceId\" type=\"hidden\" value=\"" + encodedSSOServiceId + "\" size=\"100\"/>\n");
                
                StringBuilder sbKeys = new StringBuilder();
                StringBuilder sbValues = new StringBuilder();
                
                for (String sSOSrvcDataKey : login.getSSOServiceData().keySet())
                {
                    String encodedSSOSrvcDataKey = ESAPI.encoder().encodeForHTMLAttribute(sSOSrvcDataKey);
                    
                    String[] sSOSrvcDataValues = login.getSSOServiceData().get(sSOSrvcDataKey);
                    
                    StringBuilder sSOSrvcDataValuesSB = new StringBuilder();
                    
                    for (int i = 0; i < sSOSrvcDataValues.length; i++)
                    {
                        if (sSOSrvcDataValuesSB.length() > 0)
                        {
                            sSOSrvcDataValuesSB.append("|");
                        }
                        sSOSrvcDataValuesSB.append(sSOSrvcDataValues[i]);
                    }
                    
                    String encodedSSOSrvcDataValue = ESAPI.encoder().encodeForHTMLAttribute(sSOSrvcDataValuesSB.toString());
                    
                    if (encodedSSOSrvcDataKey != null && !encodedSSOSrvcDataKey.isEmpty() &&
                        encodedSSOSrvcDataValue != null && !encodedSSOSrvcDataValue.isEmpty())
                    {
                        if (sbKeys.length() > 0 && sbValues.length() > 0)
                        {
                            sbKeys.append("^");
                            sbValues.append("^");
                        }
                        
                        sbKeys.append(encodedSSOSrvcDataKey);
                        sbValues.append(encodedSSOSrvcDataValue);
                    }
                }
                
                if (sbKeys.length() > 0 && sbValues.length() > 0)
                {
                    out.write("        <input name=\"sSOSrvcDataKeys\" type=\"hidden\" value=\"" + sbKeys.toString() + "\" size=\"512\"/>\n");
                    out.write("        <input name=\"sSOSrvcDataValues\" type=\"hidden\" value=\"" + sbValues.toString() + "\" size=\"512\"/>\n");
                }
            }
        %>
        
    </csrf:form>
      
    </div>

    <% 
    try {
      File disclaimerFile = new File(getServletContext().getRealPath("/login/disclaimer.html"));
      if (disclaimerFile.exists()) {%>
         <jsp:include page="../login/disclaimer.html" flush="true" />
      <% } else {%>
         <jsp:include page="disclaimer.html" flush="true" />
      <% }
    }
    catch (Exception e){}
    %>

  </body>
</html>
