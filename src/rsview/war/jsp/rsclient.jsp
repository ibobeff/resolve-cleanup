<!DOCTYPE html>
<!-- ******************************************************************************
* (C) Copyright 2016
*
* Resolve Systems, LLC
*
* All rights reserved
* This software is distributed under license and may not be used, copied,
* modified, or distributed without the express written permission of
* Resolve Systems, LLC.
*
********************************************************************************* -->
<%@ taglib uri="http://www.owasp.org/index.php/Category:OWASP_CSRFGuard_Project/Owasp.CsrfGuard.tld" prefix="csrf" %>
<%@page import="com.resolve.util.JspUtils" %>
<%@page import="java.io.*" %>
<%@page import="java.util.*" %>
<%@page contentType="text/html"%>
<%@page pageEncoding="UTF-8"%>
<%@ page import="org.owasp.esapi.ESAPI" %>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%
    String ver = ESAPI.encoder().encodeForURL(JspUtils.getResolveVersion());

    boolean debug = false;
    String d = request.getParameter("debug");
    if( d != null && d.indexOf("t") > -1){
        debug = true;
    }

    boolean mock = false;
    String m = request.getParameter("mock");
    if( m != null && m.indexOf("t") > -1){
        mock = true;
    }

    boolean mockScreen = false;
    String ms = request.getParameter("mockScreen");
    if( ms != null && ms.indexOf("t") > -1){
        mockScreen = true;
    }
%>
<html>
<head>
    <meta http-equiv="content-type" content="text/html; charset=UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />
    <link rel="SHORTCUT ICON" href="/resolve/favicon.ico"/>
	<script src="/resolve/JavaScriptServlet?<csrf:tokenname/>=<csrf:tokenvalue uri="/resolve/JavaScriptServlet"/>&_v=<%=ver%>"></script>

    <%
    String title = JspUtils.getResolveTitle();
    String notificationLevel = ESAPI.encoder().encodeForHTMLAttribute(JspUtils.getResolveNotificationLevel());
    String resolveLogoInfo = ESAPI.encoder().decodeForHTML(ESAPI.encoder().encodeForHTML(JspUtils.getResolveLogoInfo()));
    String resolveToolbarLogoInfo = ESAPI.encoder().decodeForHTML(ESAPI.encoder().encodeForHTML(JspUtils.getResolveToolbarLogoInfo()));
    %>
    <title><%=ESAPI.encoder().encodeForHTML(title)%></title>

    <jsp:include page="/client/module-loader.jsp" flush="true"/>
    <!-- Launch resolve app -->
    <script type="text/javascript">

        Ext.onReady(function() {
            //Initialize State Management
            if (Ext.supports.LocalStorage) Ext.state.Manager.setProvider(new Ext.state.LocalStorageProvider())
            else Ext.state.Manager.setProvider(new Ext.state.CookieProvider())

			//Flag we just logged in if coming from service/login page
			if (window.sessionStorage.getItem('service-login')) {
				window.localStorage.setItem('csrftoken-flag', true);
				window.sessionStorage.removeItem('service-login')
			}

            //Initialize extjs tooltips
            Ext.tip.QuickTipManager.init()

            //Configure glu for optimal performance
            glu.asyncLayouts = true

            //Launch Resolve
            glu.viewport({
                mtype: 'RS.client.Main',
                mock: <%=mock%>,
                mockScreen: <%=mockScreen%>,
                currentNotificationLevel : '<%=notificationLevel%>',
                resolveLogoInfo: '<%=resolveLogoInfo%>',
                resolveToolbarLogoInfo: '<%=resolveToolbarLogoInfo%>'
            })
            Ext.getBody().addCls('rs-body');
            Ext.setGlyphFontFamily('FontAwesome');
        })

    </script>

</head>

<body>
</body>
</html>
