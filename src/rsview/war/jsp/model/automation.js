/*
 * Automation scripts
 *
 */

var graph;
var previousCreateGroupCell;
var myPopupList;
var fillColorMenu;
var gradientColorMenu;
var fontColorMenu;
var labelBackgroundMenu;
var labelBorderMenu;
var lineColorMenu;
var fonts;
var fontCombo;

var selectionListener;
var updateHandler;
var listener;
var historyListener;
var drillHandler;
var undoHandler;
var changeHandler;
var keyHandler;

var toolbarItems;
var tooltip;
var fontCombo;
var sizeCombo;

var mainPanel;
var firstPart;
var menuLocal;
var graphPanelLocal;
var thisMenu;
var lastSelectedCellLabel;
var menuMask;
var saveModelMask;
var updateTitle;
var title;
var winPopup;

function executeTask() {
    var cell = graph.getSelectionCell();

    if (cell == null) {
        cell = graph.getCurrentRoot();

        if (cell == null) {
            cell = graph.getModel().getRoot();
        }
    }

    if (cell != null) {
        var href = "";
        var description = cell.getAttribute('description');
        description = unescape(description);

        if (description != null && description.length > 0) {
            var pos = description.indexOf("#");

            var taskname = "";
            var namespace = "";
            var params = "";
            if (pos > 0) {
                taskname = description.substring(0, pos);
                namespace = description.substring(pos + 1, description.length);

                pos = namespace.indexOf("\?");
                if (pos > 0) {
                    params = namespace.substring(pos + 1, namespace.length);
                    namespace = namespace.substring(0, pos);
                }

                if (params != "") {
                    href = taskname + "%23" + namespace + "&" + params;
                } else {
                    href = taskname + "%23" + namespace;
                }
            } else {
                alert('Invalid ActionTask description ...');
                showProperties(graph, graph.getSelectionCell());
            }


            //var titleLocal = title;
            var runbookName = "";
            var runbobkNamesapce = "";
            var index = title.indexOf(".");

            if (index > 0) {
                runbobkNamesapce = title.substring(0, index);
                runbookName = title.substring(index + 1, title.length);
            }

            if (runbookName != "" && runbobkNamesapce != "") {
                window.location = "/resolve/service/execute?ACTION=TASK&ACTIONNAME=" + href + "&REDIRECT=/resolve/service/wiki/editmodel/" + runbobkNamesapce + "/" + runbookName;
                // window.location = "/resolve/service/execute?ACTION=PROCESS&ACTIONNAME="+href+"&REDIRECT=/resolve/service/wiki/editmodel/" + runbobkNamesapce +"/" + runbookName; 
            } else {
                window.location = "/resolve/service/execute?ACTION=TASK&ACTIONNAME=" + href + "&REDIRECT=/resolve/service/wiki/editmodel/" + runbobkNamesapce + "/" + runbookName;
            }

        } else {
            alert('Invalid ActionTask description ...');
            showProperties(graph, graph.getSelectionCell());
        }
    }
};

function editTask() {
    var cell = graph.getSelectionCell();

    if (cell == null) {
        cell = editor.graph.getCurrentRoot();

        if (cell == null) {
            cell = editor.graph.getModel().getRoot();
        }
    }

    if (cell != null) {
        var description = cell.getAttribute('description');

        if (description != null && description.length > 0) {
            var pos = description.indexOf("#");

            var actionname = null;
            var namespace = null;

            if (pos > 0) {
                actionname = description.substring(0, pos);
                namespace = description.substring(pos + 1, description.length);

                if (namespace != null && namespace.length > 0) {
                    var posOfChar = namespace.indexOf("?");

                    if (posOfChar > 0) {
                        namespace = namespace.substring(0, posOfChar);
                    }
                }
            } else {
                actionname = description;
            }

            var displayName = namespace + '.' + actionname;
            var url = '/resolve/jsp/rsclient.jsp?#RS.actiontask.ActionTask/' + Ext.urlEncode({
                name: actionname,
                namespace: namespace
            });
            //var url = '/resolve/jsp/rsclient.jsp?#RS.actiontask.ActionTask/name=' + actionname + '&namespace=' + namespace

            doAddTab(displayName, url);
        } else {
            alert('No URL defined. Showing properties...');
            showProperties(graph, graph.getSelectionCell());
        }
    }
};

function editSubprocess() {
    var cell = graph.getSelectionCell();

    if (cell == null) {
        cell = editor.graph.getCurrentRoot();

        if (cell == null) {
            cell = editor.graph.getModel().getRoot();
        }
    }

    if (cell != null) {
        var label = cell.getAttribute('label');
        var url = '';

        url = '/resolve/jsp/rsclient.jsp?#RS.wiki.Main/name=' + label + '&activeTab=2';
        doAddTab("", url);
    }
};

function viewDTModel() {
    var cell = graph.getSelectionCell();

    if (cell == null) {
        cell = editor.graph.getCurrentRoot();

        if (cell == null) {
            cell = editor.graph.getModel().getRoot();
        }
    }

    if (cell != null) {
        var description = cell.getAttribute('description');
        var label = cell.getAttribute('label');
        var url = '';

        if (description != null) {
            url = '/resolve/jsp/rsclient.jsp?#RS.wiki.Main/name=' + description;
        } else if (label != null && label != null) {
            url = '/resolve/jsp/rsclient.jsp?#RS.wiki.Main/name=' + label;
        }
        doAddTab("", url);
    }
};

function editDTModel() {
    var cell = graph.getSelectionCell();

    if (cell == null) {
        cell = editor.graph.getCurrentRoot();

        if (cell == null) {
            cell = editor.graph.getModel().getRoot();
        }
    }

    if (cell != null) {
        var description = cell.getAttribute('description');
        var label = cell.getAttribute('label');
        var url = '';

        if (description != null) {
            url = '/resolve/jsp/rsclient.jsp?#RS.wiki.Main/name=' + description;
        } else if (label != null && label != null) {
            url = '/resolve/jsp/rsclient.jsp?#RS.wiki.Main/name=' + label;
        }
        doAddTab("", url);
    }
};

function updateConfig(styleName) {
    if (styleName.indexOf('image') == 0) {
        graph.setCellStyles(mxConstants.STYLE_VERTICAL_LABEL_POSITION, mxConstants.ALIGN_BOTTOM);
        graph.setCellStyles(mxConstants.STYLE_VERTICAL_ALIGN, mxConstants.ALIGN_TOP);
    } else {
        graph.setCellStyles(mxConstants.STYLE_VERTICAL_LABEL_POSITION, mxConstants.ALIGN_MIDDLE);
        graph.setCellStyles(mxConstants.STYLE_VERTICAL_ALIGN, mxConstants.ALIGN_MIDDLE);
    }
};

function closePopupWindow() {
    winPopup.close();
};

function editOutput() {
    if (graph.tooltipHandler != null) {
        graph.tooltipHandler.hide();
    }

    var cell = graph.getSelectionCell();

    var sourceLabel = '';
    var sourceDesc = '';

    if (cell != null && cell.getAttribute('label') != null) {
        sourceLabel = cell.getAttribute('label');
        sourceDesc = cell.getAttribute('description');
    } else {
        sourceLabel = cell.source.getAttribute('label');
        sourceDesc = cell.source.getAttribute('description');
    }

    Ext.Ajax.request({

        url: '/resolve/service/wiki/params/getoutput',
        params: {
            SLABEL: sourceLabel,
            SDESC: sourceDesc
        },

        success: function(response, opts) {
            processCallback(response, opts, cell, sourceLabel, sourceDesc, false);
        },

        failure: function(response) {
            Ext.Msg.alert('Status', 'Unable to show Inputs at this time. Please try again later.');
        }
    });
}


var executeLayout = function(layout, animate, ignoreChildCount) {
    var cell = graph.getSelectionCell();

    if (cell == null ||
        (!ignoreChildCount &&
            graph.getModel().getChildCount(cell) == 0)) {
        cell = graph.getDefaultParent();
    }

    // Animates the changes in the graph model except
    // for Camino, where animation is too slow
    if (animate && navigator.userAgent.indexOf('Camino') < 0) {
        var listener = function(sender, evt) {
            mxEffects.animateChanges(graph, evt.getProperty('changes'));
            graph.getModel().removeListener(listener);
        };

        graph.getModel().addListener(mxEvent.CHANGE, listener);
    }

    layout.execute(cell);
};


function searchTask() {
    var mainname = 'SearchTask';
    showWindows(mainname);
}

function searchRunbook() {
    var mainname = 'SearchRunbook';
    showWindows(mainname);
}

function searchDT() {
    showWindows('DecisionTreePicker');
}

function showWindows(mainname) {
    var mtype = '',
        dump;

    switch (mainname) {
        case 'SearchTask':
            mtype = 'RS.actiontask.ActionTaskPicker'
            dump = this.selectActionTask
            break;
        case 'SearchRunbook':
            mtype = 'RS.wiki.RunbookPicker'
            dump = this.selectRunbook
            break;
		default:
			mtype = 'RS.wiki.DecisionTreePicker'
			dump = this.selectDT
    }

    if (mtype) {
        var model = getResolveRoot().glu.model({
            mtype: mtype,
            dumper: {
                dump: dump
            }
        })
        model.init()
        var winPopup = getResolveRoot().glu.openWindow(model)

        // Fits the SVG container into the window
        winPopup.on('resize', function() {
            graph.sizeDidChange()
        })

        winPopup.show(this)
    }

}

function selectActionTask(selections) {
    if (selections.length > 0) {
        var selection = selections[0]
        changeNode(selection.get('uname'), selection.get('unamespace'), selection.get('inputOutputDesc'), selection.get('udescription'))
    }
}

function selectRunbook(selections) {
    if (selections.length > 0) {
        var selection = selections[0]
        changeNodeRunbook(selection.get('ufullname'))
    }
}

function selectDT(selections) {
    if (selections.length > 0) {
        var selection = selections[0]
        changeNodeRunbook(selection.get('ufullname'))
    }
}

function editInputs() {

    if (graph.tooltipHandler != null) {
        graph.tooltipHandler.hide();
    }

    var cell = graph.getSelectionCell();

    var targetLabel = '';
    var targetDesc = '';
    var sourceArray = new Array();

    var sourceRecord = Ext.data.Record.create(
        [{
            name: 'label'
        }, {
            name: 'desc'
        }]);

    if (cell != null && cell.getAttribute('label') != null) {
        targetLabel = cell.getAttribute('label');
        targetDesc = cell.getAttribute('description');
        var nodeName = cell.value.nodeName.toLowerCase();

        // if (targetDesc != null)
        //     targetDesc = unescape(targetDesc);
        // targetDesc = decodeURIComponent(targetDesc);

        var incomingEdges = graph.getIncomingEdges(cell);

        for (var k = 0; k < incomingEdges.length; k++) {
            var theEdgeSource = incomingEdges[k].source;
            var sourceLabel = theEdgeSource.getAttribute('label');
            var sourceDesc = theEdgeSource.getAttribute('description');

            var sourceRecordLocal = new sourceRecord({
                label: sourceLabel,
                desc: sourceDesc
            });

            sourceArray.push(sourceRecordLocal);
        }
    }

    var sourceString = Ext.util.JSON.encode(sourceArray);

    //if(targetLabel == 'Start' && targetDesc == null)
    if (targetLabel == 'Start') {
        if (targetDesc == null) {
            targetDesc = 'start#resolve';
        } else {
            targetDesc = 'start#resolve?' + targetDesc;
        }
    }
    //else if(targetLabel == 'End' && targetDesc == null)
    else if (targetLabel == 'End') {
        if (targetDesc == null) {
            targetDesc = 'end#resolve';
        } else {
            targetDesc = 'end#resolve?' + targetDesc;
        }
    } else if (targetLabel == 'Event' || nodeName == 'event') {
        if (targetDesc == null) {
            targetDesc = 'event#resolve';
        } else {
            targetDesc = 'event#resolve?' + targetDesc;
        }
    }

    var encLocal = new mxCodec(mxUtils.createXmlDocument());
    var nodeLocal = encLocal.encode(graph.getModel());
    var xmlLocal = nodeLocal.innerHTML;
    //var xmlLocal = mxUtils.getPrettyXml(nodeLocal);

    Ext.Ajax.request({

        url: '/resolve/service/wiki/params/getinput',
        params: {
            TLABEL: targetLabel,
            TDESC: targetDesc,
            SOURCE: sourceString,
            XML: xmlLocal
        },

        success: function(response, opts) {
            processCallback(response, opts, cell, targetLabel, targetDesc, true);
        },

        failure: function(response) {
            Ext.Msg.alert('Status', 'Unable to show Inputs at this time. Please try again later.');
        }
    });
}

function processCallback(response, opts, cell, targetLabel, targetDesc, isInput) {
    var jsonData = Ext.util.JSON.decode(response.responseText);
    var inputData = jsonData.data.INPUT;
    var outputData = jsonData.data.OUTPUT;
    var flowData = jsonData.data.FLOWVALUE;
    targetDesc = jsonData.data.INPUT_DESC;
    targetDesc = unescape(targetDesc);
    var outputNameRecord = Ext.data.Record.create([{
        name: 'name'
    }]);

    var outputNameStore = new Ext.data.Store({
        reader: new Ext.data.ArrayReader({
                idIndex: 0
            },
            outputNameRecord
        )
    });

    var outputArray = new Array();

    if (outputData != null && outputData.length > 0) {
        for (var j = 0; j < outputData.length; j += 1) {
            var outputRecord = new outputNameRecord({
                name: outputData[j]
            });

            outputArray.push(outputRecord);
        }
    }

    var flowArray = new Array();

    if (flowData != null && flowData.length > 0) {
        for (var j = 0; j < flowData.length; j += 1) {
            var flowRecord = new outputNameRecord({
                name: flowData[j]
            });

            flowArray.push(flowRecord);
        }
    }

    var tableRecord = Ext.data.Record.create(
        [{
            name: 'inputs'
        }, {
            name: 'from'
        }, {
            name: 'name'
        }]);

    var tableStore = new Ext.data.Store({

        reader: new Ext.data.ArrayReader({
                idIndex: 0
            },
            tableRecord
        )
    });

    var inputPropArray = new Array();

    if (targetDesc != null && targetDesc != '') {
        var totleLength = targetDesc.length;
        var thisIndex = targetDesc.indexOf('?');
        var thisIndexPlusOne = thisIndex + 1;

        if (thisIndex > 0 && thisIndexPlusOne != totleLength) {
            targetDesc = targetDesc.substring(targetDesc.indexOf('?') + 1, targetDesc.length);
            var descArray = targetDesc.split('&');
            var inputsArray = new Array();

            if (descArray != null && descArray.length > 0) {

                for (var k = 0; k < descArray.length; k++) {
                    var elem = descArray[k];
                    if (!elem)
                        continue;
                    elem = unescape(elem);
                    elem = elem.replace('PPEERRCCEENNTT', '%');

                    if (isInput) {
                        //for input '='     
                        var inputDisplay = elem.substring(0, elem.indexOf('='));

                        if (inputDisplay.indexOf(':') < 0) {
                            var fromDisplay = '';
                            var nameDisplay = '';
                            var firstChart = '';

                            var valueString = elem.substring(elem.indexOf('=') + 1, elem.length);

                            if (valueString != null && valueString != '') {
                                firstChart = valueString.substring(0, 1);
                            }


                            if (firstChart != '$') {
                                fromDisplay = 'CONSTANT';
                                nameDisplay = elem.substring(elem.indexOf('=') + 1, elem.length);
                                nameDisplay = nameDisplay.replace('AANNDD', '&');
                            } else {

                                var p1 = elem.indexOf('}') + 1;
                                var p2 = elem.length;

                                if (p1 != p2) {
                                    fromDisplay = elem.substring(elem.indexOf('$') + 1, elem.indexOf('{'));

                                    if (fromDisplay != null && fromDisplay == 'DEFAULT') {
                                        nameDisplay = elem.substring(elem.indexOf('{') + 1, elem.lastIndexOf('}'));
                                        nameDisplay = nameDisplay.replace('%26', '&');
                                    } else {
                                        fromDisplay = 'CONSTANT';
                                        nameDisplay = elem.substring(elem.indexOf('=') + 1, elem.length);
                                    }
                                } else {
                                    fromDisplay = elem.substring(elem.indexOf('$') + 1, elem.indexOf('{'));


                                    if (fromDisplay != null && fromDisplay == 'CNS' || fromDisplay == 'WSDATA' || fromDisplay == 'PARAM' || fromDisplay == 'PROPERTY' || fromDisplay == 'FLOW' ||
                                        fromDisplay == 'OUTPUT' || fromDisplay == 'DEFAULT') {
                                        nameDisplay = elem.substring(elem.indexOf('{') + 1, elem.lastIndexOf('}'));
                                        nameDisplay = nameDisplay.replace('%26', '&');

                                    } else {
                                        fromDisplay = 'CONSTANT';
                                        nameDisplay = elem.substring(elem.indexOf('=') + 1, elem.length);
                                    }
                                }
                            }

                            var tableRecordThis = new tableRecord({
                                inputs: inputDisplay,
                                from: fromDisplay,
                                name: nameDisplay
                            });

                            inputsArray.push(tableRecordThis);
                        }
                    } else {
                        if (elem.indexOf(':=') == -1)
                            continue;
                        var pair = elem.split(':=');
                        var key = pair[0];
                        var v = pair[1];
                        //for output ':='
                        inputDisplay = key;
                        var fromDisplay = '';
                        var nameDisplay = '';

                        if (v.indexOf('$') < 0) {
                            fromDisplay = 'CONSTANT';
                            nameDisplay = v.substring(v.indexOf('=') + 1, v.length);
                        } else {
                            fromDisplay = v.substring(v.indexOf('$') + 1, v.indexOf('{'));
                            nameDisplay = v.substring(v.indexOf('{') + 1, v.lastIndexOf('}'));
                        }

                        var tableRecordThis = new tableRecord({
                            inputs: inputDisplay,
                            from: fromDisplay,
                            name: nameDisplay
                        });

                        inputsArray.push(tableRecordThis);

                    }
                }
            }
        }

        var inputLocal = '';
        var fromLocal = '';
        var nameLocal = '';


        if (inputsArray != null && inputsArray.length > 0) {
            for (var m = 0; m < inputsArray.length; m += 1) {
                var tableRecordLocal = inputsArray[m];

                inputLocal = tableRecordLocal.get('inputs');
                fromLocal = tableRecordLocal.get('from');
                nameLocal = tableRecordLocal.get('name');

                if (inputData != null && inputData.length > 0) {
                    for (var j = 0; j < inputData.length; j += 1) {
                        var inputDataElem = inputData[j];

                        if (inputDataElem == inputLocal) {
                            inputData.splice(j, 1);
                            break;
                        }
                    }
                }

                var tableRecordThis = new tableRecord({
                    inputs: inputLocal,
                    from: fromLocal,
                    name: nameLocal
                });
                tableStore.add(tableRecordThis);

            }
        }

        var fromLocalDef = '';

        if (isInput) {
            fromLocalDef = 'CONSTANT';
        } else {
            fromLocalDef = 'FLOW';
        }

        if (inputData != null && inputData.length > 0) {
            for (var j = 0; j < inputData.length; j += 1) {
                var inputDataElem = inputData[j];
                var fromLocal = fromLocalDef;
                var nameLocal = '';

                var tableRecordThis = new tableRecord({
                    inputs: inputDataElem,
                    from: fromLocal,
                    name: nameLocal
                });
                tableStore.add(tableRecordThis);
            }
        }
    }

    var storeOutput = new Ext.data.ArrayStore({
        id: 0,
        fields: ['getValueFrom'],
        data: [
            ['DEFAULT'],
            ['CONSTANT'],
            ['OUTPUT'],
            ['FLOW'],
            ['CNS'],
            ['WSDATA'],
            ['PARAM'],
            ['PROPERTY']
        ]
    });

    showInputs(tableStore, storeOutput, outputNameStore, targetLabel, outputArray, isInput, flowArray);
}

function showInputs(tableStore, storeOutput, outputNameStore, targetLabel, outputArray, isInput, flowArray) {
    var storeOutputLocal = new Ext.data.ArrayStore({
        id: 0,
        fields: ['getValueFrom'],
        data: [
            ['OUTPUT'],
            ['FLOW'],
            ['CNS'],
            ['WSDATA'],
            ['PARAM']
        ]
    });

    var win;
    if (graph.tooltipHandler != null) {
        graph.tooltipHandler.hide();
    }

    var theDisplayName = 'INPUT: ';
    var theType = 'Get Value From';
    var theInputParams = 'Input Parameters';

    if (!isInput) {
        theDisplayName = 'OUTPUT: ';
        theType = 'Copy Value To';
        theInputParams = 'Output Parameters';
    }

    if (!win) {
        var fm = Ext.form;

        var comboValue = new Ext.form.ComboBox({
            typeAhead: true,
            triggerAction: 'all',
            lazyRender: true,
            listClass: 'x-combo-list-small',
            mode: 'local',
            store: (isInput) ? storeOutput : storeOutputLocal,
            displayField: 'getValueFrom'
        });

        var comboName = new Ext.form.ComboBox({
            typeAhead: true,
            triggerAction: 'all',
            lazyRender: false,
            listClass: 'x-combo-list-small',
            mode: 'local',
            store: outputNameStore,
            displayField: 'name'
        });

        comboName.on('beforeshow', function(combo, value) {
            var model = grid.getSelectionModel();
            if (model != null) {
                var selectItem = model.getSelected();

                if (selectItem != null) {
                    var fromName = selectItem.get('from');

                    var myData = null;

                    if (fromName == 'OUTPUT') {
                        outputNameStore.removeAll();
                        outputNameStore.add(outputArray);
                    } else {
                        if (isInput && fromName == 'FLOW') {
                            outputNameStore.removeAll();
                            outputNameStore.add(flowArray);
                        } else {
                            myData = [];
                            outputNameStore.removeAll();
                            outputNameStore.loadData(myData);
                        }
                    }
                }
            }
        });

        comboName.on('change', function(combo, value) {
            var model = grid.getSelectionModel();
            if (model != null) {
                var selectItem = model.getSelected();

                if (selectItem != null) {

                    var fromName = selectItem.get('from');
                    if (fromName == 'DEFAULT' && selectItem.isModified('name')) {
                        selectItem.set('from', 'CONSTANT');
                    }
                }
            }
        });

        var grid = new Ext.grid.EditorGridPanel({
            store: tableStore,
            cm: new Ext.grid.ColumnModel({
                defaults: {
                    sortable: true
                },
                columns: [{
                    id: 'inputs',
                    header: theInputParams,
                    dataIndex: 'inputs',
                    width: 150,
                    renderer: function(v) {
                        return Ext.util.Format.htmlEncode(v)
                    },
                    editor: new fm.TextField({
                        allowBlank: false
                    })
                }, {
                    id: 'from',
                    header: theType,
                    dataIndex: 'from',
                    width: 100,
                    renderer: function(v) {
                        return Ext.util.Format.htmlEncode(v)
                    },
                    editor: comboValue

                }, {
                    id: 'name',
                    header: 'Name',
                    dataIndex: 'name',
                    width: 150,
                    renderer: function(v) {
                        return Ext.util.Format.htmlEncode(v)
                    },
                    editor: comboName

                }]
            }),
            width: 600,
            height: 300,
            autoExpandColumn: 'inputs',
            title: theDisplayName + targetLabel,
            frame: false,
            sm: new Ext.grid.RowSelectionModel(),
            clicksToEdit: 1,

            bbar: [{
                iconCls: 'icon-user-add',
                text: 'Add',
                //disabled: (isInput)?false:true,
                handler: function() {

                    var tableRecord = grid.getStore().recordType;

                    var tr = new tableRecord({
                        inputs: '',
                        from: 'OUTPUT',
                        name: ''
                    });

                    grid.stopEditing();
                    tableStore.insert(tableStore.getCount(), tr);
                    grid.getView().refresh();

                }
            }, {
                iconCls: 'icon-user-delete',
                text: 'Remove',
                disabled: false,
                handler: function() {
                    grid.stopEditing();
                    var s = grid.getSelectionModel().getSelections();
                    for (var i = 0, r; r = s[i]; i++) {
                        tableStore.remove(r);
                    }
                }
            }]
        });


        win = new Ext.Window({
            layout: 'fit',
            width: 500,
            height: 300,
            plain: true,

            items: grid,

            buttons: [{
                text: 'Save',
                disabled: false,
                handler: function() {
                    var allStoreRecord = grid.store.getRange(0, grid.store.getCount());

                    var cell = graph.getSelectionCell();

                    if (cell != null) {
                        if (cell.getAttribute('label') == null) {
                            saveModifiedRecord(allStoreRecord, cell.target, isInput);
                        } else {
                            saveModifiedRecord(allStoreRecord, cell, isInput);
                        }
                    }

                    //win.hide();
                    win.close();
                }
            }, {
                text: 'Cancel',
                handler: function() {
                    //win.hide();
                    win.close();
                }
            }]
        });

        // Fits the SVG container into the window
        win.on('resize', function() {
            graph.sizeDidChange();
        });
    }

    win.show(this);
};


function saveModifiedRecord(records, cell, isInput) {
    var recordsStr = '';

    //update the cell attributes
    if (records != null && records.length > 0) {
        for (var i = 0; i < records.length; i += 1) {
            var theRecord = records[i];
            var inputL = theRecord.get('inputs');
            if (/^\s*$/.test(inputL)) {
                if (this.parent && this.parent.clientVM) {
                    this.parent.clientVM.message({
                        title: 'The parameter name is required',
                        msg: 'The parameter name is required. Please note that special characters in the default values of ActionTask parameters may lead to an improper display of this automation parameter grid.',
                        buttons: this.parent.Ext.MessageBox.OK,
                        buttonText: {
                            ok: 'OK'
                        }
                    });
                } else {
                    alert('The parameter name is required. Please note that special characters in the default values of ActionTask parameters may lead to an improper display of this automation parameter grid.');
                }

                return;
            }

            var fromL = theRecord.get('from');
            var name = theRecord.get('name');

            inputL = encodeURIComponent(inputL);
            name = encodeURIComponent(name);

            if (name != null && name != '' && fromL != 'DEFAULT') {
                if (i != 0) {
                    recordsStr += '&';
                }

                recordsStr += inputL;

                if (isInput) {
                    if (fromL == 'CONSTANT') {
                        recordsStr += "=" + name;
                    } else if (fromL == 'PARAM' || fromL == 'PROPERTY') {
                        var nameLocal = name.replace('&', '%26');

                        recordsStr += "=";
                        recordsStr += '$';
                        recordsStr += fromL;
                        recordsStr += '{';
                        recordsStr += nameLocal;
                        recordsStr += '}';
                    } else {
                        recordsStr += "=";
                        recordsStr += '$';
                        recordsStr += fromL;
                        recordsStr += '{';
                        recordsStr += name;
                        recordsStr += '}';
                    }
                } else {
                    if (fromL == 'CONSTANT') {
                        recordsStr += ":=" + name;
                    } else {
                        recordsStr += ":=";
                        recordsStr += '$';
                        recordsStr += fromL;
                        recordsStr += '{';
                        recordsStr += name;
                        recordsStr += '}';
                    }
                }
            }
        }
    }

    var urlParts = decodeWhateverItIsNow(cell);
    if (recordsStr != null && recordsStr != '') {
        firstPart = recordsStr.substring(0, 1);

        if (firstPart == '&') {
            recordsStr = recordsStr.substring(1, recordsStr.length);
        }
    }
    var theParamStr = urlParts[isInput ? 'outputKVs' : 'inputKVs'].join('&');
    if (theParamStr)
        theParamStr += '&';

    var theEncoded = (urlParts.task ? (urlParts.task + '?') : '') + encodeURIComponent(theParamStr + (!recordsStr ? '' : recordsStr));
    cell.setAttribute('description', theEncoded);
    updateTitle();
};

function decodeWhateverItIsNow(cell) {
    var desc = cell.getAttribute('description');
    var splits = (!!desc && (desc.indexOf('?') != -1 || desc.indexOf('#') != -1)) ? desc.split('?') : [''];
    var inputKVs = [];
    var outputKVs = [];
    var labelStr = cell.getAttribute('label');
    var nodeName = cell.value.nodeName.toLowerCase();
    if (splits.length < 2 || (labelStr == 'Start' || labelStr == 'End' || labelStr == 'Event' || nodeName == 'event'))
        return {
            task: splits[0],
            inputKVs: inputKVs,
            outputKVs: outputKVs
        };
    var encodedParams = splits[1];
    var params = {};
    var kv = unescape(encodedParams).split('&');
    for (var i = 0; i < kv.length; i++) {
        var uescp = unescape(kv[i]);
        if (uescp.indexOf(':=') != -1)
            outputKVs.push(kv[i])
        else
            inputKVs.push(kv[i]);
    }
    return {
        task: splits[0],
        inputKVs: inputKVs,
        outputKVs: outputKVs
    };
}

function getDisplayString(cell, isInput) {
    var nameStr = '';

    var labelStr = cell.getAttribute('label');
    var nodeName = cell.value.nodeName.toLowerCase();

    if (labelStr != null && (labelStr == 'Start' || labelStr == 'End' || labelStr == 'Event' || nodeName == 'event')) {
        nameStr = '';
        /*
                if(labelStr == 'Start')
                {
                    nameStr = 'start#resolve?';
                }
                else if(labelStr == 'End')
                {
                    nameStr = 'end#resolve?';
                }
               */
    } else {
        var desc = cell.getAttribute('description');

        if (desc != null && desc != '') {
            var firstPart = '';

            if (desc.indexOf('?') > 0) {
                firstPart = desc.substring(0, desc.indexOf('?'));

                var displayDesc = '';
                var outputString = desc.substring(desc.indexOf('?') + 1, desc.length);
                var descArray = outputString.split('&');

                if (descArray != null && descArray.length > 0) {
                    for (var m = 0; m < descArray.length; m++) {
                        var elem = descArray[m];
                        var outputDisplay = elem.substring(0, elem.indexOf('='));

                        if (isInput) {
                            if (outputDisplay.indexOf(':') > 0) {
                                if (m != 0) {
                                    displayDesc += '&';
                                }

                                displayDesc += elem;
                            }
                        } else {
                            if (outputDisplay.indexOf(':') < 0) {
                                if (m != 0) {
                                    displayDesc += '&';
                                }

                                displayDesc += elem;
                            }
                        }
                    }

                    if (displayDesc.indexOf('&') == 0) {
                        displayDesc = displayDesc.substring(1, displayDesc.length);
                    }

                    nameStr = firstPart + '?' + displayDesc;
                }
            } else {
                nameStr = desc + '?';
            }
        }
    }

    return nameStr;
};

function showProperties(graph, cell) {
    if (graph.tooltipHandler != null) {
        graph.tooltipHandler.hide();
    }

    var descString = cell.getValue().getAttribute('description');
    var isTaskOrImage = (cell.value.nodeName.toLowerCase() == ('task')) ? true : false;
    var labelStr = cell.getAttribute('label') || '';
    var descStr = '';
    var orderVal = cell.getAttribute('order') || '9999';
    var mergeLocal = cell.getValue().getAttribute('merge');
    var statusStr = (cell.getValue().getAttribute('status') != null && cell.getValue().getAttribute('status') != 'null') ? (cell.getValue().getAttribute('status')) : '';

    if (!isTaskOrImage || (descString != null && isTaskOrImage)) {
        //descString = unescape(descString);
        descStr = descString;
        var nodeName = cell.value.nodeName.toLowerCase();

        if ((labelStr == 'Start' || labelStr == 'End' || labelStr == 'Event' || nodeName == 'event') && descStr != '') {
            var indexStart = descStr.indexOf('?');

            if (indexStart > 0) {
                descStr = descStr.substring(indexStart + 1, descStr.length);
            }
        }
    } else if (isTaskOrImage && (descString == null || descString == '')) {
        //wnd = showModalWindow(name, form.table, width, height);
    } else {
        Ext.Msg.alert('Warning:', 'Task ' + labelStr + ' is not correctly defined');
    }


    var winProp;

    if (!winProp) {
        var propForm = new Ext.FormPanel({
            labelWidth: 75,
            //url:'save-form.php',
            frame: true,
            title: 'Properties',
            bodyStyle: 'padding:5px 5px 0',
            width: 350,
            defaults: {
                width: 340
            },
            defaultType: 'textfield',

            items: [{
                fieldLabel: 'ID',
                id: 'ID',
                name: 'ID',
                value: '' + cell.getId(),
                allowBlank: false

            }, {
                fieldLabel: 'Label',
                id: 'Label',
                name: 'Label',
                value: labelStr
            }, {
                fieldLabel: 'Order #',
                id: 'Order',
                name: 'Order',
                value: orderVal
            }, {
                xtype: 'textarea',
                fieldLabel: 'Status',
                id: 'Status',
                name: 'Status',
                value: statusStr,
                flex: 0.2
            }, {
                xtype: 'textarea',
                fieldLabel: 'Params',
                id: 'description',
                name: 'description',
                value: descStr,
                flex: 0.2
            }, {
                xtype: 'textarea',
                fieldLabel: (mergeLocal == null) ? '' : 'Merge',
                id: 'merge',
                name: 'merge',
                value: (mergeLocal == null) ? '' : mergeLocal,
                flex: 0.2,
                hidden: (mergeLocal == null)
            }]

        });

        //simple.render(document.body);
        var winProp = new Ext.Window({
            layout: 'fit',
            width: 500,
            height: 350,
            plain: true,

            items: propForm,

            buttons: [{
                text: 'OK',
                disabled: false,
                handler: function() {
                    var elt = cell.value.cloneNode(true);
                    elt.setAttribute('order', Ext.getCmp('Order').getValue());
                    elt.setAttribute('label', Ext.getCmp('Label').getValue());
                    elt.setAttribute('status', Ext.getCmp('Status').getValue());
                    elt.setAttribute('description', Ext.getCmp('description').getValue());
                    elt.setAttribute('href', Ext.getCmp('description').getValue());

                    if (mergeLocal != null) {
                        elt.setAttribute('merge', Ext.getCmp('merge').getValue());
                    }

                    graph.model.setValue(cell, elt);

                    winProp.close();
                }
            }, {
                text: 'Cancel',
                handler: function() {
                    winProp.close();
                }
            }]
        });

        // Fits the SVG container into the window
        winProp.on('resize', function() {
            graph.sizeDidChange();
        });
    }

    winProp.show(this);

}

function showPropertiesLocal(graph, cell) {
    if (graph.tooltipHandler != null) {
        graph.tooltipHandler.hide();
    }

    var form = new mxForm('properties');

    var idInput = document.createElement('input');
    idInput.setAttribute('type', 'text');
    idInput.setAttribute('size', '50');
    idInput.value = cell.getId();
    var idField = form.addField('ID', idInput);

    var labelInput = document.createElement('textarea');
    //labelInput.setAttribute('type','textarea');
    labelInput.setAttribute('rows', '2');
    labelInput.setAttribute('multiple', 'true');
    labelInput.setAttribute('cols', '90');
    labelInput.value = cell.getValue().getAttribute('label');
    var labelField = form.addField('label', labelInput);

    var statusInput = document.createElement('textarea');
    statusInput.setAttribute('rows', '2');
    statusInput.setAttribute('multiple', 'true');
    statusInput.setAttribute('cols', '90');
    statusInput.value = cell.getValue().getAttribute('status');
    var statusField = form.addField('status', statusInput);

    var descInput = document.createElement('textarea');
    //descInput.setAttribute('type','textarea');
    descInput.setAttribute('rows', '4');
    descInput.setAttribute('multiple', 'true');
    descInput.setAttribute('cols', '90');

    var descString = cell.getValue().getAttribute('description');
    var isTaskOrImage = (cell.value.nodeName.toLowerCase() == ('task')) ? true : false;

    var width = 600;
    var height = 260;

    var name = 'Properties';

    if (!isTaskOrImage || (descString != null && isTaskOrImage)) {
        descString = unescape(descString);
        descInput.value = descString;

        var labelStr = cell.getAttribute('label');
        var descStr = descInput.value;
        var nodeName = cell.value.nodeName.toLowerCase();

        if ((labelStr == 'Start' || labelStr == 'End' || labelStr == 'Event' || nodeName == 'event') && descStr != '') {
            var indexStart = descStr.indexOf('?');

            if (indexStart > 0) {
                descStr = descStr.substring(indexStart + 1, descStr.length);
                descInput.value = descStr;
            }
        }

        var descField = form.addField('description', descInput);

        var mergeLocal = cell.getValue().getAttribute('merge');

        var mergeInput = null;

        if (mergeLocal != null) {
            mergeInput = document.createElement('textarea');
            //mergeInput.setAttribute('type','textarea');
            mergeInput.setAttribute('rows', '4');
            mergeInput.setAttribute('multiple', 'true');
            mergeInput.setAttribute('cols', '90');
            mergeInput.value = cell.getValue().getAttribute('merge');
            var mergeField = form.addField('merge', mergeInput);
            height = 360;
        }

        var wnd = null;

        var okFunction = function() {
            var elt = cell.value.cloneNode(true);
            elt.setAttribute('label', labelInput.value);
            elt.setAttribute('status', statusInput.value);
            elt.setAttribute('description', descInput.value);

            if (mergeInput != null) {
                elt.setAttribute('merge', mergeInput.value);
            }

            graph.model.setValue(cell, elt);

            wnd.destroy();
        }

        var cancelFunction = function() {
            wnd.destroy();
        }

        form.addButtons(okFunction, cancelFunction);
        name = 'Properties';

        wnd = showModalWindow(name, form.table, width, height);
    } else if (isTaskOrImage && (descString == null || descString == '')) {
        wnd = showModalWindow(name, form.table, width, height);
    } else {
        Ext.Msg.alert('Warning:', 'Task ' + labelInput.value + ' is not correctly defined');
    }
};

function showModalWindow(title, content, width, height) {
    var background = document.createElement('div');
    background.style.position = 'absolute';
    background.style.left = '0px';
    background.style.top = '0px';
    background.style.right = '0px';
    background.style.bottom = '0px';
    background.style.background = 'black';
    mxUtils.setOpacity(background, 20);
    document.body.appendChild(background);

    if (mxClient.IS_IE) {
        new mxDivResizer(background);
    }

    var x = Math.max(0, document.body.scrollWidth / 2 - width * 2 / 3);
    var y = Math.max(10, (document.body.scrollHeight || document.documentElement.scrollHeight) / 2 - height * 7 / 8);

    var wnd = new mxWindow(title, content, x, y, width, height, false, true);
    wnd.setClosable(true);

    wnd.addListener(mxEvent.DESTROY, function(evt) {
        mxEffects.fadeOut(background, 50, true,
            10, 30, true);
    });

    graph.setTooltips = false;
    wnd.setVisible(true);

    return wnd;
};