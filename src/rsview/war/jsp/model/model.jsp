<!DOCTYPE html>
<!--
  $Id: grapheditor.html,v 1.7 2010/01/02 09:45:15 gaudenz Exp $
  Copyright (c) 2006-2010, JGraph Ltd
  
  Graph Editor example for mxGraph. This example demonstrates using
  mxGraph inside an ExtJs panel, and integrating tooltips, popupmenus,
  toolbars and dialogs into mxGraph.
-->
<%@page import="com.resolve.util.JspUtils" %>
<%@page import="org.owasp.esapi.ESAPI"%>
<%@ page language="java" pageEncoding="UTF-8"%>
<%@ page contentType="text/html;charset=UTF-8" %>
<%@ taglib prefix="esapi" uri="http://www.owasp.org/index.php/Category:OWASP_Enterprise_Security_API" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<% String ver = ESAPI.encoder().encodeForURL(JspUtils.getResolveVersion()); %>
<html>
<head>
    <meta http-equiv="content-type" content="text/html; charset=UTF-8">
    <title>Resolve Graph Editor</title>
    <script src="/resolve/JavaScriptServlet?_v=<%=ver%>"></script>
    <!--<link rel="stylesheet" type="text/css" href="http://extjs.com/deploy/dev/resources/css/ext-all.css" />
    <link rel="stylesheet" type="text/css" href="http://extjs.com/deploy/dev/resources/css/xtheme-default.css" />
    <script type="text/javascript" src="http://extjs.com/deploy/dev/adapter/ext/ext-base.js"></script>
    <script type="text/javascript" src="http://extjs.com/deploy/dev/ext-all.js"></script>-->
    
    <link rel="stylesheet" type="text/css" href="/resolve/css/ext-all.css?_v=<%=ver%>" />
    <link rel="stylesheet" type="text/css" href="/resolve/css/xtheme-gray.css?_v=<%=ver%>" />
    <script type="text/javascript" src="/resolve/js/ext-base.js?_v=<%=ver%>"></script>
    <script type="text/javascript" src="/resolve/js/ext-all.js?_v=<%=ver%>"></script>
    <script type="text/javascript" src="/resolve/js/resolve.js?_v=<%=ver%>"></script>
    
     <script type="text/javascript">
        mxBasePath = '/resolve/jsp/model/mxBase';
        Ext.BLANK_IMAGE_URL='/resolve/images/s.gif';
    </script>
    
    <script type="text/javascript" src="/resolve/jsp/model/mxBase/js/mxClient.js?_v=<%=ver%>"></script>
    <link rel="stylesheet" type="text/css" href="/resolve/jsp/model/css/grapheditor.css?_v=<%=ver%>" />
     
    <script type="text/javascript" src="/resolve/jsp/model/js/GraphEditor.js?_v=<%=ver%>"></script>
    <script type="text/javascript" src="/resolve/jsp/model/js/MainPanel.js?_v=<%=ver%>"></script>
    <script type="text/javascript" src="/resolve/jsp/model/js/LibraryPanel.js?_v=<%=ver%>"></script>
    <script type="text/javascript" src="/resolve/jsp/model/js/ResolveImagesPanel.js?_v=<%=ver%>"></script>
    <script type="text/javascript" src="/resolve/jsp/model/js/DiagramStore.js?_v=<%=ver%>"></script>
    <script type="text/javascript" src="/resolve/jsp/model/js/DiagramPanel.js?_v=<%=ver%>"></script>
    <script type="text/javascript" src="/resolve/jsp/model/automation.js?_v=<%=ver%>"></script>
    <script type="text/javascript">
 
 //override the getPrettyXml api as a part of new encoding/decoding strategy 
 mxClient.IS_SVG=true;
 mxUtils.getPrettyXmlOld = mxUtils.getPrettyXml;
 mxUtils.getPrettyXml =function(node)
 {
     return node.outerHTML||mxUtils.getPrettyXmlOld(node).replace('<#cdata-section/>','');
 }
    
 function main()
 {              
 
        Ext.QuickTips.init();
    
        // Disables browser context menu
        mxEvent.disableContextMenu(document.body);  
        
        // Makes the connection are smaller
        mxConstants.DEFAULT_HOTSPOT = 0.3;
        
        // Creates the graph and loads the default stylesheet
        graph = new mxGraph();
        
        mxGraph.prototype.convertValueToString=function(cell)
        {
            var value=this.model.getValue(cell);
        
            if(value!=null)
            {
                if(mxUtils.isNode(value))
                {
                    var lableValue = cell.getAttribute('label');
                    
                    if(lableValue != null)
                    {
                        return lableValue;
                    }
                    else
                    {
                        if(value.nodeName != 'Edge')
                        {
                            return value.nodeName;
                        }
                    }
                }
                else if(typeof(value.toString)=='function')
                {
                    return value.toString();
                }
            }   
            return '';
        };
        
        
        graph.labelChanged = function(cell, newValue, trigger)
        {
            if(cell != null && newValue != null)
            {
                var elt = cell.value.cloneNode(true);
                elt.setAttribute('label', newValue);
 
                graph.model.setValue(cell, elt);    
            }
        };
        
        // Inverts the elbow edge style without removing existing styles
        graph.flipEdge = function(edge)
        {
            if (edge != null)
            {
                var state = this.view.getState(edge);
                var style = (state != null) ? state.style : this.getCellStyle(edge);
                
                if (style != null)
                {
                    var elbow = mxUtils.getValue(style, mxConstants.STYLE_ELBOW,
                        mxConstants.ELBOW_HORIZONTAL);
                    var value = (elbow == mxConstants.ELBOW_HORIZONTAL) ?
                        mxConstants.ELBOW_VERTICAL : mxConstants.ELBOW_HORIZONTAL;
                    this.setCellStyles(mxConstants.STYLE_ELBOW, value, [edge]);
                }
            }
        };
        
        // Creates the command history (undo/redo)
        var history = new mxUndoManager();
    
        // Loads the default stylesheet into the graph
        var node = mxUtils.load('/resolve/jsp/model/resources/default-style.xml').getDocumentElement();
        var dec = new mxCodec(node.ownerDocument);
        dec.decode(node, graph.getStylesheet());
        
        // Sets the style to be used when an elbow edge is double clicked
        graph.alternateEdgeStyle = 'vertical';
        
        // Creates the main containers
        mainPanel = new MainPanel(graph, history);
       
        var library = new LibraryPanel();
        
        var store = new Ext.data.ArrayStore({
            fields: ['name']
        });
        store.loadData([['test'], ['test2']]);
        
        updateHandler = function()
        {
            var data = [];
            var names = DiagramStore.getNames();
            
            for (var i = 0; i < names.length; i++)
            {
                data.push([names[i]]);
            }
            
            store.loadData(data);
        };
        
        DiagramStore.addListener('put', updateHandler);
        DiagramStore.addListener('remove', updateHandler);
        updateHandler();
        
        var diagramPanel = new DiagramPanel(store, mainPanel);
        
        diagramPanel.on('dblclick', function(view, index, node, e)
        {
            var name = store.getAt(index).get('name');
            mainPanel.openDiagram(name);
        });
        
        var resolveImagesPanel = new ResolveImagesPanel();
        //var tabItems = (DiagramStore.isAvailable()) ? [library, diagramPanel] : [library];
        var tabItems = [library, resolveImagesPanel];
        
        // Creates the container for the outline
        var tabPanel = new Ext.TabPanel(
        {
            id: 'tabPanel',
            region: 'center',
            activeTab: 0,
            border : false,
            width: 180,
            items: tabItems
        });
        
        // Creates the container for the outline
        var mainTabPanel = new Ext.TabPanel(
        {
            id: 'mainTabPanel',
            region: 'center',
            activeTab: 0,
            items: [mainPanel]
        });
        
        // Creates the container for the outline
        
        var outlinePanel = new Ext.Panel(
        {
            id: 'outlinePanel',
            layout: 'fit',
            split: true,
            height: 200,
            region:'south'
        });
       
        // Creates the enclosing viewport
        var viewport = new Ext.Viewport(
        {
            layout:'border',
            items:
            [{
                xtype: 'panel',
                margins: '0 0 0 0',
                region: 'center',
                layout: 'border',
                border: false,
                
                items:('${modelonly}' == 'true')?[mainPanel]:
                [
                        new Ext.Panel(
                        {
                            region: 'west',
                            layout: 'border',
                            split: true,
                            width: 180,
                            border: false,
                            items: ('${modelonly}' == 'true')?
                            [
                                tabPanel
                            ]:[
                                tabPanel,
                                outlinePanel
                            ]
                            
                        }),   
                        mainTabPanel
                ]
              } // end master panel
            ] // end viewport items
        }); // end of new Viewport
    
        // Enables scrollbars for the graph container to make it more
        // native looking, this will affect the panning to use the
        // scrollbars rather than moving the container contents inline
        mainPanel.graphPanel.body.dom.style.overflow = 'auto';
    
        // Installs the command history after the initial graph
        // has been created
        listener = function(sender, evt)
        {
            history.undoableEditHappened(evt.getProperty('edit'));
        };
        
        graph.getModel().addListener(mxEvent.UNDO, listener);
        graph.getView().addListener(mxEvent.UNDO, listener);
    
        // Keeps the selection in sync with the history
        undoHandler = function(sender, evt)
        {
            var changes = evt.getProperty('edit').changes;
            graph.setSelectionCells(graph.getSelectionCellsForChanges(changes));
        };
        
        history.addListener(mxEvent.UNDO, undoHandler);
        history.addListener(mxEvent.REDO, undoHandler);
    
        
        mxGraphHandler.prototype.guidesEnabled = true;
        // Initializes the graph as the DOM for the panel has now been created  
        graph.init(mainPanel.graphPanel.body.dom);
        graph.setConnectable(true);
        graph.setDropEnabled(true);
        graph.setPanning(true);
        graph.setTooltips(true);
        graph.connectionHandler.setCreateTarget(true);
        
        
        // Sets the cursor
        graph.container.style.cursor = 'default';
    
        // Creates rubberband selection
        var rubberband = new mxRubberband(graph);
    
        // Adds some example cells into the graph
        mainPanel.newDiagram();
        
        var menuPopupList;
        
        var viewonlyLocal = '${modelonly}';
        
        Ext.Ajax.request({
                    url: '/resolve/service/wiki/params/getpopuplist',
                    timeout: 300000,
                    params: {
                        name: 'getpopuplist'
                    },
                    success: function(response){
                        var text;
                        
                        try {
                            text = response.responseText;
                            eval(text); 
                            menuMask.hide();
                        } 
                        catch(e) {
                            
                        }
                    },
                    failure: function(response){
                            Ext.Msg.alert('Error', response.responseText);
                            menuMask.hide();
                    },
                    scope: this
                });
         
        // Toolbar object for updating buttons in listeners
        if('${modelonly}' == 'false')
        {
            toolbarItems = mainPanel.graphPanel.getTopToolbar().items;
            toolbarItems.get('saveButton').setVisible(false);
            
            // Updates the states of all buttons that require a selection
            selectionListener = function()
            {
                var selected = !graph.isSelectionEmpty();
                
                toolbarItems.get('cut').setDisabled(!selected);
                toolbarItems.get('copy').setDisabled(!selected);
                toolbarItems.get('delete').setDisabled(!selected);
                toolbarItems.get('italic').setDisabled(!selected);
                toolbarItems.get('bold').setDisabled(!selected);
                toolbarItems.get('underline').setDisabled(!selected);
                toolbarItems.get('fillcolor').setDisabled(!selected);
                toolbarItems.get('fontcolor').setDisabled(!selected);
                toolbarItems.get('linecolor').setDisabled(!selected);
                toolbarItems.get('align').setDisabled(!selected);
            };
        
            graph.getSelectionModel().addListener(mxEvent.CHANGE, selectionListener);
        
            // Updates the states of the undo/redo buttons in the toolbar
            historyListener = function()
            {
                toolbarItems.get('undo').setDisabled(!history.canUndo());
                toolbarItems.get('redo').setDisabled(!history.canRedo());
            };
      
            history.addListener(mxEvent.ADD, historyListener);
            history.addListener(mxEvent.UNDO, historyListener);
            history.addListener(mxEvent.REDO, historyListener);
        
            // Updates the button states once
            selectionListener();
            historyListener();
        }
        
        // Installs outline in outlinePanel
        graph.useScrollbarsForPanning = false;
        if('${modelonly}' == 'false')
        {
            var outline = new mxOutline(graph, outlinePanel.body.dom);
        }
                
        // Adds the entries into the library
        insertVertexTemplate(library, graph, 'Start', '/resolve/jsp/model/images/symbols/start.png', 'symbol;image=/resolve/jsp/model/images/symbols/start.png', 30, 30);
        insertVertexTemplate(library, graph, 'End', '/resolve/jsp/model/images/symbols/end.png', 'symbol;image=/resolve/jsp/model/images/symbols/end.png', 30, 30);
        insertVertexTemplate(library, graph, 'Event', '/resolve/jsp/model/images/symbols/cancel_end.png', 'symbol;image=/resolve/jsp/model/images/symbols/cancel_end.png', 30, 30);
        insertVertexTemplate(library, graph, 'Text', '/resolve/jsp/model/images/text.gif', 'rounded=1', 100, 40);
        insertVertexTemplate(library, graph, 'Container', '/resolve/jsp/model/images/swimlane.gif', 'swimlane', 200, 200);
        insertVertexTemplate(library, graph, 'Runbook', '/resolve/jsp/model/images/rectangle.gif', null, 100, 40);
        insertVertexTemplate(library, graph, 'Task', '/resolve/jsp/model/images/rounded.gif', 'rounded=1', 100, 40);
        insertVertexTemplate(library, graph, 'Precondition', '/resolve/jsp/model/images/rhombus.gif', 'rhombus', 60, 60);
        
        insertEdgeTemplate(library, graph, 'Straight Connector', '/resolve/jsp/model/images/straight.gif', 'straight;noEdgeStyle=1', 100, 100);
        insertEdgeTemplate(library, graph, 'Horizontal Connector', '/resolve/jsp/model/images/connect.gif', null, 100, 100);
        insertEdgeTemplate(library, graph, 'Vertical Connector', '/resolve/jsp/model/images/vertical.gif', 'vertical', 100, 100);
        
         ${images}
       
        // Overrides createGroupCell to set the group style for new groups to 'group'
        previousCreateGroupCell = graph.createGroupCell;
        
        graph.createGroupCell = function()
        {
            var group = previousCreateGroupCell.apply(this, arguments);
            group.setStyle('group');
            
            return group;
        };
    
        graph.connectionHandler.factoryMethod = function()
        {       
            if(GraphEditor.edgeTemplate != null)
            {
                return graph.cloneCells([GraphEditor.edgeTemplate])[0];
            }
            else
            {               
                var resolveEdge = new mxCell('');
                resolveEdge.setEdge(true);
                var geo = new mxGeometry();
                geo.relative = true;
                resolveEdge.setGeometry(geo);
                resolveEdge.setStyle('straight;noEdgeStyle=1');
                
                var edgeObject = mxUtils.createXmlDocument().createElement("Edge");
                edgeObject.setAttribute('label', "");
                edgeObject.setAttribute('description', "");
                resolveEdge.setValue(edgeObject);
                
                return resolveEdge;
            }
            
            return null;
        };
       
        var modelXml = '${modelXML}';
       
        mainPanel.openExistedDiagram(modelXml);
        
        // Redirects tooltips to ExtJs tooltips. First a tooltip object
        // is created that will act as the tooltip for all cells.
        
        tooltip = new Ext.ToolTip
        ({
            //target: graph.container,
            target: graph.t,
            plain: true,
            width: 150,
            anchorOffset: 10
        });
        
        // Disables the built-in event handling
        tooltip.disabled = true;
        
        // Installs the tooltip by overriding the hooks in mxGraph to
        // show and hide the tooltip.
        
        graph.getTooltipForCell = function(cell)
        {            
            var tip = '';
            
            if(cell != null)
            {
                
                graph.getView().refresh();
                var labelStr = cell.getAttribute('label');
              
                if(labelStr != null && labelStr != 'Start' && labelStr != 'End')
                {
                    var merge = cell.getAttribute('merge');
                    var detail = cell.getAttribute('detail');
                    var desc = cell.getAttribute('description');
                    var ioputDesc = cell.getAttribute('tooltip')
                       
                    var nameAndNamespace = getNameAndNamespace(desc);
                    
                    tip = '<b>' + nameAndNamespace +'('+cell.getId()+ ')</b>\n';
                    
                    if(merge != null)
                    {
                        tip += merge + '\n'; 
                    }
                    
                    if(detail != null)
                    {
                        tip += detail + '\n';
                    }
                    
                    var userObject = cell.getValue();
                                
                    if(ioputDesc != null && ioputDesc != '')
                    {
                        var jsonData = Ext.util.JSON.decode(ioputDesc);
                        
                        var inputData = null;
                        var outputData = null;
                        
                        if(jsonData != null && jsonData.data != null)
                        {
                            inputData = jsonData.data.INPUT_DESC;
                            outputData = jsonData.data.OUTPUT_DESC;
                        }

                        if(inputData != null && inputData.length >0)
                        {
                            tip = tip + '<br/>';
                            tip = tip + '<b>Inputs:</b>\n';
                            
                            for (var j=0; j<inputData.length; j+=1)
                            {
                                var inputParam = inputData[j]; 
                              
                                if(inputParam != null && inputParam != '')
                                {
                                    var theFirstPart = inputParam.substring(0, inputParam.indexOf('&'));
                                    var secondPart = inputParam.substring(inputParam.indexOf('&') + 1, inputParam.length);
                                      
                                    while(secondPart.indexOf('&amp;quot;') > 0)
                                    {
                                        secondPart = secondPart.replace('&amp;quot;', '"');
                                    }
                                    
                                    tip = tip + theFirstPart + ' - ' + secondPart + '\n';
                                }                          
                            }
                        }
                        
                        if(outputData != null && outputData.length >0)
                        {
                            tip = tip + '<br/>';
                            tip = tip + '<b>Output:</b>\n';
                            
                            for (var j=0; j<outputData.length; j+=1)
                            {
                                var outputParam = outputData[j]; 
                                
                                if(outputParam != null && outputParam != '')
                                {
                                    var theFirstPart = outputParam.substring(0, outputParam.indexOf('&'));
                                    var secondPart = outputParam.substring(outputParam.indexOf('&') + 1, outputParam.length);
                                      
                                    while(secondPart.indexOf('&amp;quot;') > 0)
                                    {
                                        secondPart = secondPart.replace('&amp;quot;', '"');
                                    }
                                }
                            
                                tip = tip + theFirstPart + ' - ' + secondPart + '\n';                          
                            } 
                        }
                    }
               }
            }
            
            if(tip == null || tip == '')
            {
                if(graph.tooltipHandler != null)
                {
                    graph.tooltipHandler.hide();
                }
            }
           
            return tip;
            
        }
        
        getInputOrOutputArray = function(targetDesc, isInput)
        {
            var inputsArray = new Array();
            
            var tableRecord = Ext.data.Record.create(
              [
                    {name: 'inputs'},
                    {name: 'from'},
                    {name: 'name'}
              ]);
            
            if(targetDesc != null && targetDesc != '')
             {
                if(targetDesc.indexOf('?')>0)
                {
                    targetDesc = targetDesc.substring(targetDesc.indexOf('?')+1, targetDesc.length);
                    var descArray = targetDesc.split('&');
                    
                     if(descArray != null && descArray.length >0)
                     {           
                        for(var k=0; k<descArray.length; k++)
                        {
                            var elem = descArray[k];
                            
                            if(isInput)
                            {           
                                //for input '='     
                                var inputDisplay = elem.substring(0, elem.indexOf('='));
                                
                                if(inputDisplay.indexOf(':') <0)
                                {
                                    var fromDisplay = '';
                                    var nameDisplay = '';
                                    
                                    if(elem.indexOf('$')<0)
                                    {
                                        fromDisplay = 'CONSTANT';
                                        nameDisplay = elem.substring(elem.indexOf('=')+1, elem.length);
                                    }
                                    else
                                    {
                                        fromDisplay = elem.substring(elem.indexOf('$')+1, elem.indexOf('{'));
                                        nameDisplay = elem.substring(elem.indexOf('{')+1, elem.indexOf('}'));
                                    }
                                    
                                    var tableRecordThis = new tableRecord(
                                    {
                                        inputs: inputDisplay,
                                        from: fromDisplay,
                                        name: nameDisplay
                                    });
                                
                                    inputsArray.push(tableRecordThis);  
                                }
                            }
                            else
                            {
                                var inputDisplay = elem.substring(0, elem.indexOf('='));
                                
                                //for output ':='
                                if(inputDisplay.indexOf(':') >0)
                                {
                                    inputDisplay = inputDisplay.substring(0, elem.indexOf(':'));
                                    var fromDisplay = '';
                                    var nameDisplay = '';
                                    
                                    if(elem.indexOf('$')<0)
                                    {
                                        fromDisplay = 'CONSTANT';
                                        nameDisplay = elem.substring(elem.indexOf('=')+1, elem.length);
                                    }
                                    else
                                    {
                                        fromDisplay = elem.substring(elem.indexOf('$')+1, elem.indexOf('{'));
                                        nameDisplay = elem.substring(elem.indexOf('{')+1, elem.indexOf('}'));
                                    }
                                    
                                    var tableRecordThis = new tableRecord(
                                    {
                                        inputs: inputDisplay,
                                        from: fromDisplay,
                                        name: nameDisplay
                                    });
                                
                                    inputsArray.push(tableRecordThis);  
                                }
                            }                                   
                        }
                    }
                }
              }
                 
              return inputsArray;
        };
    
        getNameAndNamespace = function(description)
        {
            var nameAndNamespace = '';
            
            if(description != null && description != '')
            {
                if(description.indexOf('?')>0)
                {
                    nameAndNamespace = description.substring(0, description.indexOf('?'));
                }
                else
                {
                    nameAndNamespace = description;
                }   
            }
            
            return nameAndNamespace;
        };
        
        // Updates the document title if the current root changes (drilling)
        drillHandler = function(sender)
        {
            var model = graph.getModel();
            var cell = graph.getCurrentRoot();
            var title = '';
            
            while (cell != null &&
                  model.getParent(model.getParent(cell)) != null)
            {
                // Append each label of a valid root
                if (graph.isValidRoot(cell))
                {
                    title = ' > ' +
                    graph.convertValueToString(cell) + title;
                }
                
                cell = graph.getModel().getParent(cell);
            }
            
            document.title = 'Graph Editor' + title;
        };
            
        graph.getView().addListener(mxEvent.DOWN, drillHandler);
        graph.getView().addListener(mxEvent.UP, drillHandler);
    
        // Transfer initial focus to graph container for keystroke handling
        // graph.container.focus();
           
        // Handles keystroke events
        keyHandler = new mxKeyHandler(graph);
        
        // Ignores enter keystroke. Remove this line if you want the
        // enter keystroke to stop editing
        keyHandler.enter = function() {};
        
        keyHandler.bindKey(8, function()
        {
            graph.foldCells(true);
        });
        
        keyHandler.bindKey(13, function()
        {
            graph.foldCells(false);
        });
        
        keyHandler.bindKey(33, function()
        {
            graph.exitGroup();
        });
        
        keyHandler.bindKey(34, function()
        {
            graph.enterGroup();
        });
        
        keyHandler.bindKey(36, function()
        {
            graph.home();
        });
    
        keyHandler.bindKey(35, function()
        {
            graph.refresh();
        });
        
        keyHandler.bindKey(37, function()
        {
            graph.selectPreviousCell();
        });
            
        keyHandler.bindKey(38, function()
        {
            graph.selectParentCell();
        });
    
        keyHandler.bindKey(39, function()
        {
            graph.selectNextCell();
        });
        
        keyHandler.bindKey(40, function()
        {
            graph.selectChildCell();
        });
        
        keyHandler.bindKey(46, function()
        {
            graph.removeCells();
        });
        
        keyHandler.bindKey(107, function()
        {
            graph.zoomIn();
        });
        
        keyHandler.bindKey(109, function()
        {
            graph.zoomOut();
        });
        
        keyHandler.bindKey(113, function()
        {
            graph.startEditingAtCell();
        });
      
        keyHandler.bindControlKey(65, function()
        {
            graph.selectAll();
        });
    
        keyHandler.bindControlKey(89, function()
        {
            history.redo();
        });
        
        keyHandler.bindControlKey(90, function()
        {
            history.undo();
        });
        
        keyHandler.bindControlKey(88, function()
        {
            mxClipboard.cut(graph);
        });
        
        keyHandler.bindControlKey(67, function()
        {
            mxClipboard.copy(graph);
        });
        
        keyHandler.bindControlKey(86, function()
        {
            mxClipboard.paste(graph);
        });
        
        keyHandler.bindControlKey(71, function()
        {
            graph.setSelectionCell(graph.groupCells(null, 20));
        });
        
        keyHandler.bindControlKey(85, function()
        {
            graph.setSelectionCells(graph.ungroupCells());
        });
        
        
        //start the ajax to refresh the condition of action task results
        var problemIDLocal = '${esapi:encodeForJavaScript(PROBLEMID)}';
        var refreshWikiLocal = '${esapi:encodeForJavaScript(WIKI)}';
        var statusLocal = '${esapi:encodeForJavaScript(STATUS)}';
        var zoomLocal = '${esapi:encodeForJavaScript(ZOOM)}';
        var refreshLocal = '${esapi:encodeForJavaScript(REFRESH)}';
        var refreshIntervalLocal = '${esapi:encodeForJavaScript(REFRESHINTERVAL)}';
        var refreshCountMax = '${esapi:encodeForJavaScript(REFRESHCOUNTMAX)}';
        var refreshIntervalLocalInt = 3000;
        var refreshCount = 1;
        
        if(zoomLocal != null && zoomLocal.length >0)
        {
            graph.zoom(zoomLocal);
        }
        
        if(refreshIntervalLocal != null && refreshIntervalLocal.length >0)
        {
            refreshIntervalLocalInt = refreshIntervalLocal.valueOf();
        }
        
        var task = {
        
        run: function() {
        
            Ext.Ajax.request({
            
                    url: '/resolve/service/wiki/params/changecolor',
                    params: {                
                        PROBLEMID: problemIDLocal,
                        WIKI: refreshWikiLocal, 
                        STATUS: statusLocal,
                        ZOOM: zoomLocal,
                        REFRESH: refreshLocal, 
                        REFRESHINTERVAL: refreshIntervalLocal                        
                    },
                    success: function(response) {
                        var text;
                        
                        try {
                            var jsonData = Ext.util.JSON.decode(response.responseText); 

                            if(jsonData != null) {
                                if (jsonData.data.DOREFRESH && refreshCount < refreshCountMax) {
                                    refreshCount++;
                                } else {
                                    runner.stop(task);                                    
                                }
                               
                                updateNodeColor(jsonData.data.RESULTS);
                            }
                        } catch(e) {
                        }
                    },
                    failure: function(response){
                            Ext.Msg.alert('Error', response.responseText);
                            //menuMask.hide();
                    },
                    scope: this
                });
         },
         interval: refreshIntervalLocalInt
      }              
      
      var runner = new Ext.util.TaskRunner(); runner.start(task);
      
      if('${modelonly}' != 'true')
      {
            mxConnectionHandler.prototype.connectImage = new mxImage('/resolve/jsp/model/images/connector.gif', 16, 16);
      }
      
      function updateNodeColor(results)
      {
           var resultsLocal = results;
           var graphModel = graph.getModel();
           
           if(resultsLocal != null && resultsLocal.length >0)
           {
               for (var j=0; j<resultsLocal.length; j+=1)
               {
                  var rel = resultsLocal[j];
                  var graphNodeID = rel.GRAPHNODEID;
                  var nodeID = rel.NODEID;
                  var statusLocal = rel.STATUS;
                  var color = rel.COLOR;
                  
                  var nodeLocal = graphModel.getCell(graphNodeID);
                  
                  if(nodeLocal != null)
                  {
                     var nodeStyleLocal = nodeLocal.style;
                  
                     if(nodeStyleLocal != null)
                     {
                           //nodeStyleLocal = nodeStyleLocal.replace('fillColor=#D3D3D3;', color);
                           //nodeStyleLocal = nodeStyleLocal.replace('fillColor=#ADD8E6;', color);
                           
                           if(color != null && color.length == 18)
                           {
                              nodeStyleLocal = 'rounded;labelBackgroundColor=none;strokeColor=#A9A9A9;' + color + 'gradientColor=white';
                           }
                           
                           if(graphModel != null)
                           {
                                graphModel.setStyle(nodeLocal, nodeStyleLocal);
                                graph.getView().refresh();   
                           }    
                     }
                  } 
               }
               
               graph.getView().refresh();        
               
           }
      }
        
    }; // end of main
    
    
    MainPanel = function(graph, history)
    {
      
    // Defines various color menus for different colors
    fillColorMenu = new Ext.menu.ColorMenu(
    {
        items: [
        {
            text: 'None',
            handler: function()
            {
                graph.setCellStyles(mxConstants.STYLE_FILLCOLOR, mxConstants.NONE);
            }
        },
        '-'
        ],
        handler : function(cm, color)
        {
            if (typeof(color) == "string")
            {
                graph.setCellStyles(mxConstants.STYLE_FILLCOLOR, '#'+color);
            }
        }
    });         
    

    gradientColorMenu = new Ext.menu.ColorMenu(
    {
        items: [
        {
            text: 'North',
            handler: function()
            {
                graph.setCellStyles(mxConstants.STYLE_GRADIENT_DIRECTION, mxConstants.DIRECTION_NORTH);
            }
        },
        {
            text: 'East',
            handler: function()
            {
                graph.setCellStyles(mxConstants.STYLE_GRADIENT_DIRECTION, mxConstants.DIRECTION_EAST);
            }
        },
        {
            text: 'South',
            handler: function()
            {
                graph.setCellStyles(mxConstants.STYLE_GRADIENT_DIRECTION, mxConstants.DIRECTION_SOUTH);
            }
        },
        {
            text: 'West',
            handler: function()
            {
                graph.setCellStyles(mxConstants.STYLE_GRADIENT_DIRECTION, mxConstants.DIRECTION_WEST);
            }
        },
        '-',
        {
            text: 'None',
            handler: function()
            {
                graph.setCellStyles(mxConstants.STYLE_GRADIENTCOLOR, mxConstants.NONE);
            }
        },
        '-'
        ],
        handler : function(cm, color)
        {
            if (typeof(color) == "string")
            {
                graph.setCellStyles(mxConstants.STYLE_GRADIENTCOLOR, '#'+color);
            }
        }
    });

    fontColorMenu = new Ext.menu.ColorMenu(
    {
        items: [
        {
            text: 'None',
            handler: function()
            {
                graph.setCellStyles(mxConstants.STYLE_FONTCOLOR, mxConstants.NONE);
            }
        },
        '-'
        ],
        handler : function(cm, color)
        {
            if (typeof(color) == "string")
            {
                graph.setCellStyles(mxConstants.STYLE_FONTCOLOR, '#'+color);
            }
        }
    });

    lineColorMenu = new Ext.menu.ColorMenu(
    {
        items: [
        {
            text: 'None',
            handler: function()
            {
                graph.setCellStyles(mxConstants.STYLE_STROKECOLOR, mxConstants.NONE);
            }
        },
        '-'
        ],
        handler : function(cm, color)
        {
            if (typeof(color) == "string")
            {
                graph.setCellStyles(mxConstants.STYLE_STROKECOLOR, '#'+color);
            }
        }
    });

    labelBackgroundMenu = new Ext.menu.ColorMenu(
    {
        items: [
        {
            text: 'None',
            handler: function()
            {
                graph.setCellStyles(mxConstants.STYLE_LABEL_BACKGROUNDCOLOR, mxConstants.NONE);
            }
        },
        '-'
        ],
        handler : function(cm, color)
        {
            if (typeof(color) == "string")
            {
                graph.setCellStyles(mxConstants.STYLE_LABEL_BACKGROUNDCOLOR, '#'+color);
            }
        }
    });

    labelBorderMenu = new Ext.menu.ColorMenu(
    {
        items: [
        {
            text: 'None',
            handler: function()
            {
                graph.setCellStyles(mxConstants.STYLE_LABEL_BORDERCOLOR, mxConstants.NONE);
            }
        },
        '-'
        ],
        handler : function(cm, color)
        {
            if (typeof(color) == "string")
            {
                graph.setCellStyles(mxConstants.STYLE_LABEL_BORDERCOLOR, '#'+color);
            }
        }
    });
    
    // Defines the font family menu
    fonts = new Ext.data.SimpleStore(
    {
        fields: ['label', 'font'],
        data : [['Helvetica', 'Helvetica'], ['Verdana', 'Verdana'],
            ['Times New Roman', 'Times New Roman'], ['Garamond', 'Garamond'],
            ['Courier New', 'Courier New']]
    });
    
    fontCombo = new Ext.form.ComboBox(
    {
        store: fonts,
        displayField:'label',
        mode: 'local',
        width:120,
        triggerAction: 'all',
        emptyText:'Select a font...',
        selectOnFocus:true,
        onSelect: function(entry)
        {
            if (entry != null)
            {
                graph.setCellStyles(mxConstants.STYLE_FONTFAMILY, entry.data.font);
                this.collapse();
            }
        }
    });
    
    // Handles typing a font name and pressing enter
    fontCombo.on('specialkey', function(field, evt)
    {
        if (evt.keyCode == 10 ||
            evt.keyCode == 13)
        {
            var family = field.getValue();
            
            if (family != null &&
                family.length > 0)
            {
                graph.setCellStyles(mxConstants.STYLE_FONTFAMILY, family);
            }
        }
    });

    // Defines the font size menu
    var sizes = new Ext.data.SimpleStore({
        fields: ['label', 'size'],
        data : [['6pt', 6], ['8pt', 8], ['9pt', 9], ['10pt', 10], ['12pt', 12],
            ['14pt', 14], ['18pt', 18], ['24pt', 24], ['30pt', 30], ['36pt', 36],
            ['48pt', 48],['60pt', 60]]
    });
    
    sizeCombo = new Ext.form.ComboBox(
    {
        store: sizes,
        displayField:'label',
        mode: 'local',
        width:50,
        triggerAction: 'all',
        emptyText:'12pt',
        selectOnFocus:true,
        onSelect: function(entry)
        {
            if (entry != null)
            {
                graph.setCellStyles(mxConstants.STYLE_FONTSIZE, entry.data.size);
                this.collapse();
            }
        }
    });
    
    // Handles typing a font size and pressing enter
    sizeCombo.on('specialkey', function(field, evt)
    {
        if (evt.keyCode == 10 ||
            evt.keyCode == 13)
        {
            var size = parseInt(field.getValue());
            
            if (!isNaN(size) &&
                size > 0)
            {
                graph.setCellStyles(mxConstants.STYLE_FONTSIZE, size);
            }
        }
    });
    
    var sizeCombo = new Ext.form.ComboBox(
    {
        store: sizes,
        displayField:'label',
        mode: 'local',
        width:50,
        triggerAction: 'all',
        emptyText:'12pt',
        selectOnFocus:true,
        onSelect: function(entry)
        {
            if (entry != null)
            {
                graph.setCellStyles(mxConstants.STYLE_FONTSIZE, entry.data.size);
                this.collapse();
            }
        }
    });
    
    // Simplified file and modified state handling
    this.filename = null;
    thisModified = false;

    updateTitle = mxUtils.bind(this, function()
    {
        title = '${modelTitle}'; 
        
        if('${modelonly}' == 'true')
        {
            //todo need disable the title    
        }
        else
        {
            this.setTitle((title || 'New Diagram') + ((thisModified) ? ' *' : '') + ' ');
        }
    });
    
    changeHandler = mxUtils.bind(this, function(sender, evt)
    {
        thisModified = true;
        updateTitle();
        if( window.changedModel)window.changedModel()
    });
    
    graph.getModel().addListener(mxEvent.CHANGE, changeHandler);
    
    this.saveDiagram = function(forceDialog)
    {
        var enc = new mxCodec(mxUtils.createXmlDocument());
        //enc.encodeDefaults = true;
        var node = enc.encode(graph.getModel());
        var xml = mxUtils.getPrettyXml(node);
        
        var saveUrl = '${saveUrl}';
        
        var invalidEdge = 0;
        
        var xmlLocal = xml;
        var startEdge = xmlLocal.indexOf("<Edge>");
        var endEdge = xmlLocal.indexOf("</Edge>");
        
        while(startEdge>=0 && endEdge>=0 && endEdge>startEdge)
        {
            var edgeString = xmlLocal.substring(startEdge, endEdge);
            
            var indexOfSource = edgeString.indexOf("source=");
            var indexOfTarget = edgeString.indexOf("target=");  
            
            if(indexOfSource <0 || indexOfTarget <0)
            {
                invalidEdge = 1;
                break;
            } 
            
            var startNextEdge =  endEdge + '</Edge>'.length; 
            xmlLocal = xmlLocal.substring(startNextEdge, xml.length);
            startEdge = xmlLocal.indexOf("<Edge");
            endEdge = xmlLocal.indexOf("</Edge>");
        }
        
        if(invalidEdge == 1)
        {
            Ext.Msg.alert('Status', 'Some Graph lines are not connected.');
        }
        else
        {
            Ext.Ajax.request({
                    
            url: '/resolve/service/wiki/modelxml/validation',
            params:{
                      content: xml
            },
                    
            success: function(response, opts)
            {
                validationOfModelXML(response, opts, saveUrl, xml);               
            },
                    
            failure: function(response)
            {
                Ext.Msg.alert('Status', 'save xml failed.');  
            }
            });
         }
            
         thisModified = false;
         updateTitle();
                 
    };
    
    function validationOfModelXML(response, opts, saveUrl, xml)
    {
        var jsonData = Ext.util.JSON.decode(response.responseText);
       
        if(jsonData != null)
        {
            var outputData = jsonData.data.OUTPUT; 
            var outputDataRunbook = jsonData.data.OUTPUTRUNBOOK;
           
            if(outputData == null || outputData.length<=0)
            {
                if(outputDataRunbook != null && outputDataRunbook.length >0)
                {
                    var warningMsg = '';
                    
                    for (var j=0; j<outputDataRunbook.length; j+=1)
                    {
                        warningMsg += '&nbsp;&nbsp;' + '-' + '&nbsp;&nbsp;' + outputDataRunbook[j] + '<br>';    
                    }
                    
                    Ext.Msg.minWidth = 200;
                    var msg = 'Runbook Not Found: <br><br>' 
                              + warningMsg 
                              + '<br>Do you want to save?';
                              
                    Ext.Msg.confirm('Warning:', msg, function(btn, text)
                    {
                        if (btn == 'yes')
                        {
                           processSaveModel(saveUrl, xml);
                        }
                    });
                }
                else
                {
                    processSaveModel(saveUrl, xml);
                }   
            }    
            else
            {
                if(outputData != null && outputData.length>0)
                {                    
                    var warningMsg = '';
                    
                    for (var j=0; j<outputData.length; j+=1)
                    {
                        warningMsg += '&nbsp;&nbsp;' + '-' + '&nbsp;&nbsp;' + outputData[j] + '<br>';    
                    }
                    
                    Ext.Msg.minWidth = 200;
                    var msg = 'ActionTasks Not Found: <br><br>' 
                              + warningMsg 
                              + '<br>Do you want to save?';
                              
                    Ext.Msg.confirm('Warning:', msg, function(btn, text)
                    {
                        if (btn == 'yes')
                        {
                           if(outputDataRunbook != null && outputDataRunbook.length >0)
                           {
                                var warningMsg = '';
                    
                                for (var j=0; j<outputDataRunbook.length; j+=1)
                                {
                                    warningMsg += '&nbsp;&nbsp;' + '-' + '&nbsp;&nbsp;' + outputDataRunbook[j] + '<br>';    
                                }
                    
                                Ext.Msg.minWidth = 200;
                                var msg = 'Runbook Not Found: <br><br>' 
                                          + warningMsg 
                                          + '<br>Do you want to save?';
                              
                                Ext.Msg.confirm('Warning:', msg, function(btn, text)
                                {
                                    if (btn == 'yes')
                                    {
                                       processSaveModel(saveUrl, xml);
                                    }
                                });
                            } 
                            else
                            {
                                processSaveModel(saveUrl, xml);
                            }
                        }
                        else
                        {
                            //donothing
                        }
                    });
                }
                else
                {
                    processSaveModel(saveUrl, xml); 
                }               
            }
        }
        else
        {
            processSaveModel(saveUrl, xml);
        }    
    }
    
    function processSaveModel(saveUrl, xml)
    {
        if(xml == null || xml == '')
        {
             Ext.Msg.minWidth = 200;
             var msg = 'Do you want to save empty model xml? <br><br>'; 
                                                     
             Ext.Msg.confirm('Warning:', msg, function(btn, text)
             {
                if (btn == 'yes')
                {
                    processSaveModelLocal(saveUrl, xml);
                }
             });
        }
        else
        {
            processSaveModelLocal(saveUrl, xml);
        }
    }
    
    function processSaveModelLocal(saveUrl, xml)
    {
        saveModelMask.show(); 
        Ext.Ajax.request({
                    
           url: saveUrl,
           params:{
                    content: xml
           },
                        
           success: function(response, opts)
           {
                if( window.savedModel)window.savedModel(response)
                saveModelMask.hide(); 
           },
                        
           failure: function(response)
           {
              saveModelMask.hide(); 
              Ext.Msg.alert('Status', 'save xml failed.');
           }
        });
    }
    
    this.openExistedDiagram = function(modelXml)
    {
        if (modelXml != null && modelXml.length > 0)
        {
            var doc = mxUtils.parseXml(modelXml); 
            var dec = new mxCodec(doc.ownerDocument); 
            dec.decode(doc.documentElement, graph.getModel());
            
            history.clear();
            //this.filename = name;
            thisModified = false;
            updateTitle();
           
        }
        
        title = '${modelTitle}';
        
        if('${modelonly}' == 'true')
        {
            //todo need disable the title    
        }
        else
        {
            this.setTitle(title);
        }
        
        //graph.container.focus();
    };
    
    this.newDiagram = function()
    {
        if (!thisModified ||
            mxUtils.confirm('Lose changes?'))
        {
            var cell = new mxCell();
            cell.insert(new mxCell());
            graph.getModel().setRoot(cell);
            history.clear();
            this.filename = null;
            thisModified = false;
            updateTitle();
        }
    };
    
    menuMask = new Ext.LoadMask(Ext.getBody(), {msg:"Please wait..."});
    saveModelMask = new Ext.LoadMask(Ext.getBody(), {msg:"Please wait..."});
         
    this.graphPanel = new Ext.Panel(
    {
    
        region: 'center',
        border:false,
        tbar: ('${modelonly}' == 'true')?null:[     
        {
            id: 'saveButton',
            text:'',
            iconCls: 'save-icon',
            tooltip: 'Save Diagram',
            handler: function()
            {
                this.saveDiagram();
            },
            scope:this
        },
        {
            id: 'saveAsButton',
            text:'',
            iconCls: 'saveas-icon',
            //tooltip: 'Save Diagram As',
            tooltip: 'Save',
            handler: function()
            {
                this.saveDiagram(true);
            },
            scope:this
        },
        '-',
        {
            id: 'print',
            text:'',
            iconCls: 'print-icon',
            tooltip: 'Print Preview',
            handler: function()
            {
                var preview = new mxPrintPreview(graph, 1);
                preview.open();
            },
            scope:this
        },
        {
            id: 'Select',
            text:'',
            iconCls: 'select-icon',
            tooltip: 'Select',
            handler: function()
            {
                graph.panningHandler.useLeftButtonForPanning=false;
                //graph.setConnectable(false);
            },
            scope:this
        },
        {
            id: 'Pan',
            text:'',
            iconCls: 'pan-icon',
            tooltip: 'Pan',
            handler: function()
            {
                graph.panningHandler.useLeftButtonForPanning=true;
                //graph.setConnectable(false);
            },
            scope:this
        },
        '-',
        {
            id: 'cut',
            text:'',
            iconCls: 'cut-icon',
            tooltip: 'Cut',
            handler: function()
            {
                mxClipboard.cut(graph);
            },
            scope:this
        },{
            id: 'copy',
            text:'',
            iconCls: 'copy-icon',
            tooltip: 'Copy',
            handler: function()
            {
                mxClipboard.copy(graph);
            },
            scope:this
        },{
            text:'',
            iconCls: 'paste-icon',
            tooltip: 'Paste',
            handler: function()
            {
                mxClipboard.paste(graph);
            },
            scope:this
        },
        '-',
        {
            id: 'delete',
            text:'',
            iconCls: 'delete-icon',
            tooltip: 'Delete',
            handler: function()
            {
                graph.removeCells();
            },
            scope:this
        },
        '-',
        {
            id: 'undo',
            text:'',
            iconCls: 'undo-icon',
            tooltip: 'Undo',
            handler: function()
            {
                history.undo();
            },
            scope:this
        },{
            id: 'redo',
            text:'',
            iconCls: 'redo-icon',
            tooltip: 'Redo',
            handler: function()
            {
                history.redo();
            },
            scope:this
        },
        '-',
        {
            text:'',
            iconCls: 'zoomactual-icon',
            scope:this,
            handler: function(item)
            {
                graph.zoomActual();
            }
        },
        {
            text:'',
            iconCls: 'zoomin-icon',
            scope:this,
            handler: function(item)
            {
                graph.zoomIn();
            }
        },
        {
            text:'',
            iconCls: 'zoomout-icon',
            scope:this,
            handler: function(item)
            {
                graph.zoomOut();
            }
        },
        '-',
        fontCombo,
        ' ',
        sizeCombo,
        '-',
        {
            id: 'bold',
            text: '',
            iconCls:'bold-icon',
            tooltip: 'Bold',
            handler: function()
            {
                graph.toggleCellStyleFlags(mxConstants.STYLE_FONTSTYLE, mxConstants.FONT_BOLD);
            },
            scope:this
        },
        {
            id: 'italic',
            text: '',
            tooltip: 'Italic',
            iconCls:'italic-icon',
            handler: function()
            {
                graph.toggleCellStyleFlags(mxConstants.STYLE_FONTSTYLE, mxConstants.FONT_ITALIC);
            },
            scope:this
        },
        {
            id: 'underline',
            text: '',
            tooltip: 'Underline',
            iconCls:'underline-icon',
            handler: function()
            {
                graph.toggleCellStyleFlags(mxConstants.STYLE_FONTSTYLE, mxConstants.FONT_UNDERLINE);
            },
            scope:this
        },
        '-',
        {
            id: 'align',
            text:'',
            iconCls: 'left-icon',
            tooltip: 'Text Alignment',
            handler: function() { },
            menu:
            {
                id:'reading-menu',
                cls:'reading-menu',
                items: [
                {
                    text:'Left',
                    checked:false,
                    group:'rp-group',
                    scope:this,
                    iconCls:'left-icon',
                    handler: function()
                    {
                        graph.setCellStyles(mxConstants.STYLE_ALIGN, mxConstants.ALIGN_LEFT);
                    }
                },
                {
                    text:'Center',
                    checked:true,
                    group:'rp-group',
                    scope:this,
                    iconCls:'center-icon',
                    handler: function()
                    {
                        graph.setCellStyles(mxConstants.STYLE_ALIGN, mxConstants.ALIGN_CENTER);
                    }
                },
                {
                    text:'Right',
                    checked:false,
                    group:'rp-group',
                    scope:this,
                    iconCls:'right-icon',
                    handler: function()
                    {
                        graph.setCellStyles(mxConstants.STYLE_ALIGN, mxConstants.ALIGN_RIGHT);
                    }
                },
                '-',
                {
                    text:'Top',
                    checked:false,
                    group:'vrp-group',
                    scope:this,
                    iconCls:'top-icon',
                    handler: function()
                    {
                        graph.setCellStyles(mxConstants.STYLE_VERTICAL_ALIGN, mxConstants.ALIGN_TOP);
                    }
                },
                {
                    text:'Middle',
                    checked:true,
                    group:'vrp-group',
                    scope:this,
                    iconCls:'middle-icon',
                    handler: function()
                    {
                        graph.setCellStyles(mxConstants.STYLE_VERTICAL_ALIGN, mxConstants.ALIGN_MIDDLE);
                    }
                },
                {
                    text:'Bottom',
                    checked:false,
                    group:'vrp-group',
                    scope:this,
                    iconCls:'bottom-icon',
                    handler: function()
                    {
                        graph.setCellStyles(mxConstants.STYLE_VERTICAL_ALIGN, mxConstants.ALIGN_BOTTOM);
                    }
                }]
            }
        },
        '-',
        {
            id: 'fontcolor',
            text: '',
            tooltip: 'Fontcolor',
            iconCls:'fontcolor-icon',
            menu: fontColorMenu // <-- submenu by reference
        },
        {
            id: 'linecolor',
            text: '',
            tooltip: 'Linecolor',
            iconCls:'linecolor-icon',
            menu: lineColorMenu // <-- submenu by reference
        },
        {
            id: 'fillcolor',
            text: '',
            tooltip: 'Fillcolor',
            iconCls:'fillcolor-icon',
            menu: fillColorMenu // <-- submenu by reference
        },
        '-',
        {
            text:'Menu',
            //iconCls: 'preferences-icon',
            iconCls: '',
            handler: function(menu) { },
            menu:
            {
                items: [
             
                  {
                    text:'Show XML',
                    scope:this,
                    handler: function(item)
                    {
                        var enc = new mxCodec(mxUtils.createXmlDocument());
                        var node = enc.encode(graph.getModel());
                        
                        mxUtils.popup(mxUtils.getPrettyXml(node));
                    }
                }
              ]
            }
        }], 
        
        //this is menu
        onContextMenu : function(node, e)
        {   
            if('${modelonly}' == 'true')
            {
                        if(this.menu.findById('Selected Task') != null) { this.menu.findById('Selected Task').setDisabled(true);}
                        if(this.menu.findById('Search Task') != null) { this.menu.findById('Search Task').setDisabled(true);}
                        if(this.menu.findById('Search Runbook') != null) { this.menu.findById('Search Runbook').setDisabled(true);}
                        if(this.menu.findById('Edit Inputs') != null) { this.menu.findById('Edit Inputs').setDisabled(true);}
                        if(this.menu.findById('Edit Outputs') != null) { this.menu.findById('Edit Outputs').setDisabled(true);}
                        if(this.menu.findById('Edit Task') != null) {this.menu.findById('Edit Task').setDisabled(true);}
                        if(this.menu.findById('Edit Properties') != null) { this.menu.findById('Edit Properties').setDisabled(true);}
                        if(this.menu.findById('Dependency') != null) { this.menu.findById('Dependency').setDisabled(true);}
                        if(this.menu.findById('Format') != null) { this.menu.findById('Format').setDisabled(true);}
                        if(this.menu.findById('Edit Runbook') != null){this.menu.findById('Edit Runbook').setDisabled(true);}
                        if(this.menu.findById('Execute Task') != null) {this.menu.findById('Execute Task').setDisabled(true);}
            }
                    
            if(!graph.isSelectionEmpty())
            {
                //if(this.menu != null)
                if(menuLocal != null)
                {                   
                    this.menu = menuLocal;
                    this.menu.showAt([e.clientX, e.clientY]);
                    
                    //some item need disabled
                    
                    var cell = graph.getSelectionCell();
                   
                    if(cell != null && cell.getAttribute('label') == null)
                    {
                        if(this.menu.findById('Selected Task') != null) { this.menu.findById('Selected Task').setDisabled(true);}
                        if(this.menu.findById('Search Task') != null) { this.menu.findById('Search Task').setDisabled(true);}
                        if(this.menu.findById('Search Runbook') != null) { this.menu.findById('Search Runbook').setDisabled(true);}
                        if(this.menu.findById('Edit Inputs') != null) { this.menu.findById('Edit Inputs').setDisabled(true);}
                        if(this.menu.findById('Edit Outputs') != null) { this.menu.findById('Edit Outputs').setDisabled(true);}
                        if(this.menu.findById('Edit Task') != null) {this.menu.findById('Edit Task').setDisabled(true);}
                        if(this.menu.findById('Edit Runbook') != null){this.menu.findById('Edit Runbook').setDisabled(true);}
                        if(this.menu.findById('Execute Task') != null) {this.menu.findById('Execute Task').setDisabled(true);}
                        
                        //set dependency merge disable = true
                        var thisMenuItem = this.menu.findById('Dependency'); 
                        
                        if(thisMenuItem != null)
                        {
                            var thisMenuSubMenu = thisMenuItem.menu;
                            
                            if(thisMenuSubMenu != null)
                            {
                                thisMenuSubMenu.findById('Execution').setDisabled(false);
                                thisMenuSubMenu.findById('Condition').setDisabled(false);
                                thisMenuSubMenu.findById('Merge').setDisabled(true);
                            }
                        }
                    }
                    else
                    {
                        if(cell.value.nodeName.toLowerCase() == ('task') || cell.value.nodeName.toLowerCase() == ('end')
                            || cell.value.nodeName.toLowerCase() == ('start') ||  cell.value.nodeName.toLowerCase() == ('event'))
                        {
                            if(cell.value.nodeName.toLowerCase() == ('task'))
                            {
                                if(this.menu.findById('Selected Task') != null) {this.menu.findById('Selected Task').setDisabled(false);}
                                if(this.menu.findById('Search Task') != null) { this.menu.findById('Search Task').setDisabled(false);}
                                if(this.menu.findById('Search Runbook') != null) { this.menu.findById('Search Runbook').setDisabled(true);}
                                if(this.menu.findById('Execute Task') != null) { this.menu.findById('Execute Task').setDisabled(false); }
                                if(this.menu.findById('Dependency') != null) { this.menu.findById('Dependency').setDisabled(false); }
                            }    
                              
                            var thisMenuItem = this.menu.findById('Dependency'); 
                            
                            if(thisMenuItem != null)
                            {
                                var thisMenuSubMenu = thisMenuItem.menu;
                                
                                if(thisMenuSubMenu != null)
                                {
                                    thisMenuSubMenu.findById('Execution').setDisabled(true);
                                    thisMenuSubMenu.findById('Condition').setDisabled(true);
                                    thisMenuSubMenu.findById('Merge').setDisabled(false);
                                }
                            }
                             
                            if(cell.value.nodeName.toLowerCase() == ('end')
                                || cell.value.nodeName.toLowerCase() == ('start') ||  cell.value.nodeName.toLowerCase() == ('event'))
                            {
                                if(this.menu.findById('Selected Task') != null) { this.menu.findById('Selected Task').setDisabled(true);}
                                if(this.menu.findById('Search Task') != null) { this.menu.findById('Search Task').setDisabled(true);}
                                if(this.menu.findById('Search Runbook') != null) { this.menu.findById('Search Runbook').setDisabled(true);}
                                if(this.menu.findById('Edit Inputs') != null) {this.menu.findById('Edit Inputs').setDisabled(false);}
                                if(this.menu.findById('Edit Outputs') != null) {this.menu.findById('Edit Outputs').setDisabled(true);}
                                if(this.menu.findById('Edit Task') != null) {this.menu.findById('Edit Task').setDisabled(true);}
                                if(this.menu.findById('Edit Runbook') != null) { this.menu.findById('Edit Runbook').setDisabled(true); }
                            }
                            else
                            { 
                                if(this.menu.findById('Edit Inputs') != null) {this.menu.findById('Edit Inputs').setDisabled(false);}
                                if(this.menu.findById('Edit Outputs') != null) {this.menu.findById('Edit Outputs').setDisabled(false);}
                                if(this.menu.findById('Edit Task') != null) {this.menu.findById('Edit Task').setDisabled(false);}
                                if(this.menu.findById('Edit Runbook') != null) { this.menu.findById('Edit Runbook').setDisabled(true); }
                            }
                        }
                        else if(cell.value.nodeName.toLowerCase() == 'precondition')
                        {
                            if(this.menu.findById('Selected Task') != null) { this.menu.findById('Selected Task').setDisabled(true);}
                            if(this.menu.findById('Search Task') != null) { this.menu.findById('Search Task').setDisabled(true);}
                            if(this.menu.findById('Search Runbook') != null) { this.menu.findById('Search Runbook').setDisabled(true);}
                            if(this.menu.findById('Edit Inputs') != null) { this.menu.findById('Edit Inputs').setDisabled(true);}
                            if(this.menu.findById('Edit Outputs') != null) { this.menu.findById('Edit Outputs').setDisabled(true);}
                            if(this.menu.findById('Edit Task') != null) {this.menu.findById('Edit Task').setDisabled(true);}
                            if(this.menu.findById('Edit Runbook') != null){this.menu.findById('Edit Runbook').setDisabled(true);}
                        }
                        else if(cell.value.nodeName.toLowerCase() == 'subprocess')
                        {
                            if(this.menu.findById('Selected Task') != null) {this.menu.findById('Selected Task').setDisabled(true);}
                            if(this.menu.findById('Search Task') != null) { this.menu.findById('Search Task').setDisabled(true);}
                            if(this.menu.findById('Search Runbook') != null) { this.menu.findById('Search Runbook').setDisabled(false);}
                            if(this.menu.findById('Dependency') != null) {this.menu.findById('Dependency').setDisabled(true);}
                            if(this.menu.findById('Execute Task') != null) { this.menu.findById('Execute Task').setDisabled(true); }
                            if(this.menu.findById('Edit Inputs') != null) {this.menu.findById('Edit Inputs').setDisabled(true);}
                            if(this.menu.findById('Edit Outputs') != null) {this.menu.findById('Edit Outputs').setDisabled(true);}
                            if(this.menu.findById('Edit Task') != null) {this.menu.findById('Edit Task').setDisabled(true);}
                            if(this.menu.findById('Edit Runbook') != null) { this.menu.findById('Edit Runbook').setDisabled(false); }
                        }
                        else if(cell.value.nodeName.toLowerCase() == 'text')
                        {
                            if(this.menu.findById('Selected Task') != null) {this.menu.findById('Selected Task').setDisabled(true);}
                            if(this.menu.findById('Search Task') != null) { this.menu.findById('Search Task').setDisabled(true);}
                            if(this.menu.findById('Search Runbook') != null) { this.menu.findById('Search Runbook').setDisabled(true);}
                            if(this.menu.findById('Dependency') != null) {this.menu.findById('Dependency').setDisabled(true);}
                            if(this.menu.findById('Execute Task') != null) { this.menu.findById('Execute Task').setDisabled(true); }
                            if(this.menu.findById('Edit Inputs') != null) {this.menu.findById('Edit Inputs').setDisabled(true);}
                            if(this.menu.findById('Edit Outputs') != null) {this.menu.findById('Edit Outputs').setDisabled(true);}
                            if(this.menu.findById('Edit Task') != null) {this.menu.findById('Edit Task').setDisabled(true);}
                            if(this.menu.findById('Edit Runbook') != null) { this.menu.findById('Edit Runbook').setDisabled(true); }
                            if(this.menu.findById('Edit Properties') != null) { this.menu.findById('Edit Properties').setDisabled(true); }
                            if(this.menu.findById('Format') != null) { this.menu.findById('Format').setDisabled(true); }
                        }
                        else
                        {
                            if(this.menu.findById('Selected Task') != null) { this.menu.findById('Selected Task').setDisabled(true);}
                            if(this.menu.findById('Search Task') != null) { this.menu.findById('Search Task').setDisabled(true);}
                            if(this.menu.findById('Search Runbook') != null) { this.menu.findById('Search Runbook').setDisabled(false);}
                            
                            if(this.menu.findById('Execute Task') != null) { this.menu.findById('Execute Task').setDisabled(true); }
                            
                            if(this.menu.findById('Edit Inputs') != null) {this.menu.findById('Edit Inputs').setDisabled(false);}
                            if(this.menu.findById('Edit Outputs') != null) {this.menu.findById('Edit Outputs').setDisabled(false);}
                            if(this.menu.findById('Edit Task') != null) {this.menu.findById('Edit Task').setDisabled(false);}
                            if(this.menu.findById('Edit Runbook') != null) { this.menu.findById('Edit Runbook').setDisabled(false); }
                        }                                             
                    }
                }
                else
                {
                    if(myPopupList == null)
                    {
                        menuMask.show(); 
                    }
                    else
                    {
                        myPopupList(node, e);
                        this.menu = menuLocal;
                        this.menu.showAt([e.clientX, e.clientY]);
                    }
                }
            }
            else
            {
                //if(this.menu != null)
                if(menuLocal != null)
                {
                    this.menu = menuLocal;
                    this.menu.setVisible(false);
                }
            }
        },
        
        onContextHide : function()
        {
            if(this.ctxNode)
            {
                this.ctxNode.ui.removeClass('x-node-ctx');
                this.ctxNode = null;
            }
        }        
    });
    
    MainPanel.superclass.constructor.call(this,
    {
        region:'center',
        layout: 'fit',
        items: this.graphPanel
    });
    
    // Redirects the context menu to ExtJs menus
    graph.panningHandler.popup = mxUtils.bind(this, function(x, y, cell, evt)
    {   
        //graph.dblClick(evt, cell);
        this.graphPanel.onContextMenu(null, evt);   
    });
    
    if(graph.panningHandler != null)
    {  
        graph.panningHandler.hideMenu = mxUtils.bind(this, function()
        {
            if(cell != null )
            {
                var cellLabel = cell.getAttribute('label');
                
                if(cellLabel != null && lastSelectedCellLabel != null && 
                    cellLabel != lastSelectedCellLabel && menuLocal != null)
                {
                    var isPopup = menuLocal.isVisible();
                    
                    if(isPopup)
                    {
                        menuLocal.hide(true);
                    }
                }
                
                lastSelectedCellLabel = cellLabel;
            }
            else
            {
                if(menuLocal != null && menuLocal.isVisible())
                {
                    menuLocal.hide(true);
                }
            }
            
            if (this.graphPanel.menuPanel != null)
            {
                this.graphPanel.menuPanel.hide();
            }
            
            updateTitle();
            
        });
    }

    // Fits the SVG container into the panel body
    this.graphPanel.on('resize', function()
    {
        graph.sizeDidChange();
    });
};

 
Ext.extend(MainPanel, Ext.Panel);

    </script>
</head>

<body onload="main();">
<div id="header"><div style="float:right;margin:5px;" class="x-small-editor"></div></div>
    
</body>
</html>
