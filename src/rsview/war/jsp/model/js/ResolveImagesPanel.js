
ResolveImagesPanel = function()
{
    ResolveImagesPanel.superclass.constructor.call(this,
    {
        title: 'Images',
        region:'center',
        split:true,
        rootVisible:false,
        lines:false,
        autoScroll:true,
        root: new Ext.tree.TreeNode('Graph Editor'),
        collapseFirst:false
    });
};

Ext.extend(ResolveImagesPanel, Ext.tree.TreePanel, {

    createSection : function(subdirectoryNames)
    {
        if(subdirectoryNames != null && subdirectoryNames.length >0)
        {
            for(var j = 0; j<subdirectoryNames.length; j+=1)
            {
                var subDir = subdirectoryNames[j];
                
                if(subDir != null && subDir != '')
                {
                    this.root.appendChild
                    (
                        this.subDir = new Ext.tree.TreeNode({
                            text:subDir,
                            cls:'feeds-node',
                            expanded:false
                        })
                     );
                }
            }
        }
    },

    addTemplate : function(name, icon, parentNode, cells)
    {
        var exists = this.getNodeById(name);
        
        if(exists)
        {
            //if(!inactive)
            //{
                //exists.select();
                //exists.ui.highlight();
            //}
            
            return exists;
        }
        
        var node = new Ext.tree.TreeNode(
        {
            text: name,
            icon: icon,
            leaf: true,
            cls: 'feed',
            cells: cells,
            id: name
        });
        
        if (parentNode == null)
        {
            parentNode = this.templates;
        }
        
        parentNode.appendChild(node);
        
        return node;
    },

    // prevent the default context menu when you miss the node
    afterRender : function()
    {
        ResolveImagesPanel.superclass.afterRender.call(this);
        /*
        this.el.on('contextmenu', function(e)
        {
            e.preventDefault();
        });
        */
    }
});