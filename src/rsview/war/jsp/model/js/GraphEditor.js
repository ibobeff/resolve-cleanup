

/*
 * $Id: GraphEditor.js,v 1.48 2010/02/24 10:50:29 gaudenz Exp $
 * Copyright (c) 2006-2010, JGraph Ltd
 */
GraphEditor = {};

var imglist = [];

function insertSymbolTemplate(panel, graph, name, icon, rhombus)
{
    var imagesNode = panel.symbols;
    var style = (rhombus) ? 'rhombusImage' : 'roundImage';
    return insertVertexTemplate(panel, graph, name, icon, style+';image='+icon, 50, 50, '', imagesNode);
};

function insertImageTemplateToResolveImages(panel, graph, name, icon, round, imagesNode)
{
    var style = (round) ? 'roundImage' : 'image';
    return insertVertexTemplate(panel, graph, name, icon, style+';image='+icon, 50, 50, name, imagesNode);
};

function insertImageTemplate(panel, graph, name, icon, round)
{
    var imagesNode = panel.images;
    var style = (round) ? 'roundImage' : 'image';
    return insertVertexTemplate(panel, graph, name, icon, style+';image='+icon, 50, 50, name, imagesNode);
};

function insertVertexTemplate(panel, graph, name, icon, style, width, height, value, parentNode)
{
              
        var img = name + "&" + style;
        imglist.push(img);        
		var cells = [new mxCell((value != null) ? value : '', new mxGeometry(0, 0, width, height), style)];
		cells[0].vertex = true;
		
		var funct = function(graph, evt, target, x, y)
		{
		
		    var styleLocal = style;
		    var cells = [new mxCell((value != null) ? value : '', new mxGeometry(0, 0, width, height), styleLocal)];
            cells[0].vertex = true;
			cells = graph.getImportableCells(cells);
			
			if (cells.length > 0)
			{		
				if(name == 'Task')
				{
					var actionObject = mxUtils.createXmlDocument().createElement(name);
					actionObject.setAttribute('label', name);
					actionObject.setAttribute('description', "");
					actionObject.setAttribute('tooltip', "");
					actionObject.setAttribute('href', "");
					
					cells[0].setValue(actionObject);
					cells[0].style = "rounded;labelBackgroundColor=none;strokeColor=#A9A9A9;fillColor=#D3D3D3;gradientColor=white";
				}			
				else if(name == 'Runbook')
				{
					var actionObject = mxUtils.createXmlDocument().createElement("Subprocess");
					actionObject.setAttribute('label', name);
					actionObject.setAttribute('description', "");
					cells[0].setValue(actionObject);
					cells[0].style = "labelBackgroundColor=none;strokeColor=#A9A9A9;fillColor=#ADD8E6;gradientColor=white";
					//cells[0].geometry = new  mxGeometry(620, 40, 110, 32);
				}
				else if(name == 'Document')
				{
					var actionObject = mxUtils.createXmlDocument().createElement("Document");
					actionObject.setAttribute('label', name);
					actionObject.setAttribute('description', "");
					cells[0].setValue(actionObject);
					cells[0].style = "labelBackgroundColor=none;strokeColor=#A9A9A9;fillColor=#ADD8E6;gradientColor=white";
					//cells[0].geometry = new  mxGeometry(620, 40, 110, 32);
				}
				else if(name == 'Precondition')
				{
					var actionObject = mxUtils.createXmlDocument().createElement("Precondition");
					actionObject.setAttribute('label', "Precondition");
					actionObject.setAttribute('description', "");
					cells[0].setValue(actionObject);
					cells[0].style = "rhombus;labelBackgroundColor=none;strokeColor=#FFA500;fillColor=#FFA500;gradientColor=white";
					cells[0].geometry.width = 32;
					cells[0].geometry.height = 32;
				}
				else if(name == 'Question')
				{
					var actionObject = mxUtils.createXmlDocument().createElement("Question");
					actionObject.setAttribute('label', "Question");
					actionObject.setAttribute('description', "");
					cells[0].setValue(actionObject);
					cells[0].style = "rhombus;labelBackgroundColor=none;strokeColor=#FFA500;fillColor=#FFA500;gradientColor=white";
					cells[0].geometry.width = 32;
					cells[0].geometry.height = 32;
				}
				else if(name =='Start')
				{
					var actionObject = mxUtils.createXmlDocument().createElement("Start");
					actionObject.setAttribute('label', "Start");
					actionObject.setAttribute('description', "");
					cells[0].setValue(actionObject);
					cells[0].style = "symbol;image=/resolve/jsp/model/images/symbols/start.png";
				}
				else if(name =='Root')
				{
					var actionObject = mxUtils.createXmlDocument().createElement("Start");
					actionObject.setAttribute('label', "Root");
					actionObject.setAttribute('description', "");
					cells[0].setValue(actionObject);
					cells[0].style = "symbol;image=/resolve/jsp/model/images/symbols/start.png";
				}
				else if(name =='End')
				{
					var actionObject = mxUtils.createXmlDocument().createElement("End");
					actionObject.setAttribute('label', "End");
					actionObject.setAttribute('description', "");
					cells[0].setValue(actionObject);
					cells[0].style = "symbol;image=/resolve/jsp/model/images/symbols/end.png";
				}
				else if(name =='Event')
				{
					var actionObject = mxUtils.createXmlDocument().createElement("Event");
					actionObject.setAttribute('label', "Event");
					actionObject.setAttribute('description', "");
					cells[0].setValue(actionObject);
					cells[0].style = "symbol;image=/resolve/jsp/model/images/symbols/cancel_end.png";
				}
				else if(name =='Container')
                {
                    var actionObject = mxUtils.createXmlDocument().createElement("Container");
                    actionObject.setAttribute('label', "Container");
                    actionObject.setAttribute('description', "");
                    cells[0].setValue(actionObject);
                    cells[0].style = "swimlane;labelBackgroundColor=none;strokeColor=#A9A9A9;fillColor=#357EC7;gradientColor=white;fontColor=#FFFFFF";
                }
                else if(name =='Text')
                {
                    var actionObject = mxUtils.createXmlDocument().createElement("Text");
                    actionObject.setAttribute('label', "Text");
                    actionObject.setAttribute('description', "");
                    cells[0].setValue(actionObject);
                    cells[0].style = "labelBackgroundColor=none;strokeColor=none;fillColor=none;gradientColor=white;dashed=1";
                }
				else 
				{	
					//var style  = "";//cells[0].getStyle();
				
                    if(imglist != null && imglist.length>0)
                    {
                        for(var i=0; i<imglist.length; i++)
                        {
                            var imgstring =  imglist[i];
                            var endIndex = imgstring.indexOf("&");
                            var imgname = imgstring.substring(0, endIndex)
                            
                            if(imgname != null && imgname != '' && imgname == name)
                            {
                                styleLocal = imgstring.substring(endIndex+1, imgstring.length);
                                break;
                            } 
                        }
                    }
            		
					if(styleLocal != null && styleLocal.indexOf('image') == 0)
					{
						var actionObject = mxUtils.createXmlDocument().createElement('Task');
						actionObject.setAttribute('label', name);
						actionObject.setAttribute('description', "");
						actionObject.setAttribute('inputOutputDesc', "");
						actionObject.setAttribute('href', "");
						actionObject.style = styleLocal;
						cells[0].setValue(actionObject);
					}
				}
			
				var validDropTarget = (target != null) ?
				graph.isValidDropTarget(target, cells, evt) : false;
				var select = null;
					
				if (target != null &&
					!validDropTarget &&
					graph.getModel().getChildCount(target) == 0 &&
					graph.getModel().isVertex(target) == cells[0].vertex)
				{
					graph.getModel().setStyle(target, styleLocal);
					select = [target];
				}
				else
				{
					if (target != null && !validDropTarget)
					{
						target = null;
					}
						
					var pt = graph.getPointForEvent(evt);
					var ptLocal = pt;
					
					if(name == 'Task' || name == 'Runbook' || name =='Event' || name =='Container' || name =='Text')
					{	
					   			
					   ptLocal.x -= graph.snap(width/2-6);
                       //pt.y -= graph.snap(height/2);
                    }
					
					// Splits the target edge or inserts into target group
					if (graph.isSplitEnabled() &&
							graph.isSplitTarget(target, cells, evt))
					{
						graph.splitEdge(target, cells, null, ptLocal.x, pt.y);
						select = cells;
					}
					else
					{
						cells = graph.getImportableCells(cells);
						
						if (cells.length > 0)
						{
							select = graph.importCells(cells, pt.x+width/2, pt.y, target);
						}
					}
				}
					
				if (select != null && select.length > 0)
				{
					graph.scrollCellToVisible(select[0]);
					graph.setSelectionCells(select);
				}
			}
		 };
		
		// Small hack to install the drag listener on the node's DOM element
		// after it has been created. The DOM node does not exist if the parent
		// is not expanded.
	
		var node = panel.addTemplate(name, icon, parentNode, cells);
		
		var installDrag = function(expandedNode)
		{
			if (node.ui.elNode != null)
			{
				// Creates the element that is being shown while the drag is in progress
				var dragPreview = document.createElement('div');
				dragPreview.style.border = 'dashed black 1px';
				dragPreview.style.width = width+'px';
				dragPreview.style.height = height+'px';
				
				mxUtils.makeDraggable(node.ui.elNode, graph, funct, dragPreview, 0, 0,
						graph.autoscroll, true);
					
			    
			}
		};
		
		if (!node.parentNode.isExpanded())
		{	
			panel.on('expandnode', installDrag);
		}
		else
		{	
			installDrag(node.parentNode);
		}
		
		return node;
};


function insertEdgeTemplate(panel, graph, name, icon, style, width, height, value, parentNode)
{
		var cells = [new mxCell((value != null) ? value : '', new mxGeometry(0, 0, width, height), style)];
		cells[0].geometry.setTerminalPoint(new mxPoint(0, height), true);
		cells[0].geometry.setTerminalPoint(new mxPoint(width, 0), false);
		cells[0].edge = true;
		
		var funct = function(graph, evt, target)
		{
			cells = graph.getImportableCells(cells);
			
			if (cells.length > 0)
			{				
				if(name == 'Straight Connector' || name == 'Horizontal Connector' || name == 'Vertical Connector'
                    || name == 'Answer' || name == 'Answer (Horizontal)')
				{					
					var edgeObject = mxUtils.createXmlDocument().createElement("Edge");
					edgeObject.setAttribute('label', "");
					edgeObject.setAttribute('description', "");
					cells[0].setValue(edgeObject);
				}
				
				var validDropTarget = (target != null) ?
					graph.isValidDropTarget(target, cells, evt) : false;
				var select = null;
				
				if (target != null &&
					!validDropTarget)
				{
					target = null;
				}
				
				var pt = graph.getPointForEvent(evt);
				var scale = graph.view.scale;
				
				pt.x -= graph.snap(width / 2);
				pt.y -= graph.snap(height / 2);
				
				select = graph.importCells(cells, pt.x, pt.y, target);
				
				// Uses this new cell as a template for all new edges
				GraphEditor.edgeTemplate = select[0];
				
				graph.scrollCellToVisible(select[0]);
				graph.setSelectionCells(select);
			}
		};
		
		// Small hack to install the drag listener on the node's DOM element
		// after it has been created. The DOM node does not exist if the parent
		// is not expanded.
		var node = panel.addTemplate(name, icon, parentNode, cells);
		var installDrag = function(expandedNode)
		{
			if (node.ui.elNode != null)
			{
				// Creates the element that is being shown while the drag is in progress
				var dragPreview = document.createElement('div');
				dragPreview.style.border = 'dashed black 1px';
				dragPreview.style.width = width+'px';
				dragPreview.style.height = height+'px';
				
				mxUtils.makeDraggable(node.ui.elNode, graph, funct, dragPreview, -width / 2, -height / 2,
									  graph.autoscroll, true);
			}
		};
		
		if (!node.parentNode.isExpanded())
		{
			panel.on('expandnode', installDrag);
		}
		else
		{
			installDrag(node.parentNode);
		}
		
		return node;
};


