<%@ taglib uri="http://www.owasp.org/index.php/Category:OWASP_CSRFGuard_Project/Owasp.CsrfGuard.tld" prefix="csrf" %>
<%@page import="com.resolve.util.JspUtils,com.resolve.util.StringUtils,org.owasp.esapi.ESAPI" %>
<%
String ver = JspUtils.getResolveVersion();
%>
<html>
  <head>
  <script src="/resolve/JavaScriptServlet?<csrf:tokenname/>=<csrf:tokenvalue uri="/resolve/JavaScriptServlet"/>&_v=<%=ver%>"></script>
  <title>Resolve RBA </title>
  <link rel="SHORTCUT ICON" href="/resolve/favicon.ico"/>
  <link rel="stylesheet" href="/resolve/css/rsview.css" type="text/css"/>
  
  	<script type="text/javascript">
	  Ext.onReady(function() {
	            //Initialize State Management
	            if (Ext.supports.LocalStorage)
	            	Ext.state.Manager.setProvider(new Ext.state.LocalStorageProvider())
	            else Ext.state.Manager.setProvider(new Ext.state.CookieProvider())
					window.localStorage.setItem('csrftoken-flag', true);
	            
	            Ext.getBody().addCls('rs-body');
	            Ext.setGlyphFontFamily('FontAwesome');
        })
	</script>
  
  </head>
<body>
    <%
    String message = (String)request.getSession().getAttribute("unauthorizedMsg");
    message = StringUtils.isBlank(message) ? "" : ESAPI.encoder().encodeForHTML(message);
    %>
    <p style="text-align:center; color:red;font-weight:bold;margin-top:50px;font-size:20px">You are unauthorized to access Resolve. <% out.write(message);%></p>
</body>

</html>