<!DOCTYPE html>
<html>
  <head>
    <meta http-equiv="content-type" content="text/html; charset=UTF-8">

    <!--                                                               -->
    <!-- Consider inlining CSS to reduce the number of requested files -->
    <!--                                                               -->
    <link rel="SHORTCUT ICON" href="/resolve/favicon.ico"/>
    <link type="text/css" rel="stylesheet" href="/resolve/css/rswiki.css"/>
    <link type="text/css" rel="stylesheet" href="/resolve/css/rsworksheet.css"/>
    <link type="text/css" rel="stylesheet" href="/resolve/css/rsgateway.css"/>
    <link type="text/css" rel="stylesheet" href="/resolve/css/gxt-all.css"/>
    <link type="text/css" rel="stylesheet" href="/resolve/css/gxt-gray.css"/>
    
    <!--                                           -->
    <!-- Any title is fine                         -->
    <!--                                           -->
    <title>Resolve RBA</title>
    
    <!--                                           -->
    <!-- This script loads your compiled module.   -->
    <!-- If you add any GWT meta tags, they must   -->
    <!-- be added before this line.                -->
    <!--                                           -->
	<script type="text/javascript" language="javascript" src="/resolve/js/extjs4/ext-all.js"></script>
    <script type="text/javascript" language="javascript" src="/resolve/rsgateway/rsgateway.nocache.js"></script>
    <script language="javascript" type="text/javascript" src="/resolve/js/resolve.js"></script>
    
    <script src="/resolve/js/ace/ace.js" type="text/javascript" charset="utf-8"></script>
    <script src="/resolve/js/ace/theme-eclipse.js" type="text/javascript" charset="utf-8"></script>
    <script src="/resolve/js/ace/mode-groovy.js" type="text/javascript" charset="utf-8"></script>
    <script src="/resolve/js/ace/mode-perl.js" type="text/javascript" charset="utf-8"></script>
    <script src="/resolve/js/ace/mode-powershell.js" type="text/javascript" charset="utf-8"></script>
    <script src="/resolve/js/ace/mode-ruby.js" type="text/javascript" charset="utf-8"></script>
    <script src="/resolve/js/ace/mode-sql.js" type="text/javascript" charset="utf-8"></script>
    <script src="/resolve/js/ace/mode-textile.js" type="text/javascript" charset="utf-8"></script>
    <script src="/resolve/js/ace/mode-xml.js" type="text/javascript" charset="utf-8"></script>
    <script src="/resolve/js/ace/keybinding-emacs.js" type="text/javascript" charset="utf-8"></script>
    <script src="/resolve/js/ace/keybinding-vim.js" type="text/javascript" charset="utf-8"></script>
  </head>

  <!--                                           -->
  <!-- The body can have arbitrary html, or      -->
  <!-- you can leave the body empty if you want  -->
  <!-- to create a completely dynamic UI.        -->
  <!--                                           -->
  <body>
    <!-- OPTIONAL: include this if you want history support -->
    <iframe src="javascript:''" id="__gwt_historyFrame" tabIndex='-1' style="position:absolute;width:0;height:0;border:0"></iframe>

<!--
    <h1>Web Application Starter Project</h1>

    <table align="center">
      <tr>
        <td colspan="2" style="font-weight:bold;">Please enter your name:</td>        
      </tr>
      <tr>
        <td id="nameFieldContainer"></td>
        <td id="sendButtonContainer"></td>
      </tr>
    </table>
-->

  </body>
</html>
