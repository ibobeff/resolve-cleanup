<!DOCTYPE html>
<%@ taglib prefix="esapi" uri="http://www.owasp.org/index.php/Category:OWASP_Enterprise_Security_API" %>
<%@page import="com.resolve.util.JspUtils" %>
<%@page import="org.owasp.esapi.ESAPI"%>
<%@page import="com.resolve.rsview.model.Login"%>
<% String ver = ESAPI.encoder().encodeForURL(JspUtils.getResolveVersion()); %>
<html>
  <head>
  <script type="text/javascript">
    function onBodyLoad() {
        <%
            Login login = (Login)request.getAttribute("login");
            String rootUrl = "/resolve/sir/index.html?";
            String params = "url=" + ESAPI.encoder().encodeForURL(login.getUrl()) + "&iframeId=" + ESAPI.encoder().encodeForURL(login.getIframeId());
            params = params + "&error=" + ESAPI.encoder().encodeForURL(login.getError());
            out.write("        window.location = '" + rootUrl + params + "#/resetpassword';\n");
        %>
    }
  </script>
  </head>
  <body onload="onBodyLoad()">
  </body>
</html>
