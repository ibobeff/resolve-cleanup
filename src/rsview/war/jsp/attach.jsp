<!DOCTYPE html>
<%@page import="com.resolve.wiki.attachment.WikiAttachmentType,com.resolve.services.constants.ConstantValues" %>
<%@page contentType="text/html"%>
<%@page pageEncoding="UTF-8"%>

<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@taglib uri="http://www.springframework.org/tags/form" prefix="form" %>
<%@taglib uri="http://www.springframework.org/tags" prefix="spring" %>


<html>
  <%@include file="header.jsp"%>

	<script type="text/javascript"> 
	function updateName(form) {
	  var fname = form.filepath.value;
	 
	  if (fname=="") {
	    return false;
	  }
	 
	  var i = fname.lastIndexOf('\\');
	  if (i==-1)
	   i = fname.lastIndexOf('/');
	 
	  fname = fname.substring(i+1);
	  if (form.<%=ConstantValues.WIKI_FILENAME_KEY%>.value==fname)
	   return true;
	 
	  if (form.filename.value=="")
	   form.<%=ConstantValues.WIKI_FILENAME_KEY%>.value = fname;
	 
	  return true;
	}
	</script>

  <body>
    <table>
    	<tr>
    		<td>FILENAME</td>
    		<td>TYPE</td>
    		<td>LOCATION</td>
    		<td>SIZE</td>
    		<td>CREATED ON</td>
    		<td>CREATED BY</td>
    	</tr>
    	
    	<c:forEach items="${attachments}" varStatus="loop">
    		<tr>   
    		<!-- 
    			<c:if test="${attachments[loop.index].UType == 'DB' }">
					<td><a href='${downloadFileUrl}?attach=${attachments[loop.index].sys_id}'>${attachments[loop.index].UFilename}</a></td>
				</c:if>
				<c:if test="${attachments[loop.index].UType != 'DB' }">
	        		<td><a href='/resolve/files/${attachments[loop.index].UFilename}'>${attachments[loop.index].UFilename}</a></td>
	        	</c:if>	
	        	-->
	        	<td><a href='${downloadFileUrl}?attach=${attachments[loop.index].sys_id}'>${attachments[loop.index].UFilename}</a></td>
	        	<td>${attachments[loop.index].UType}</td>  
	        	<td>${attachments[loop.index].ULocation}</td>  
	        	<td>${attachments[loop.index].USize}</td>  
	        	<td>${attachments[loop.index].sysCreatedOn}</td>  
	        	<td>${attachments[loop.index].sysCreatedBy}</td>
        	</tr>     
      	</c:forEach>      
    	
    </table>
  
  
	<form action="${attachFileUrl}" enctype="multipart/form-data" method="post">
		<p>
			Choose file to upload:
			<br/>
			<input type="hidden" name="<%=ConstantValues.WIKI_FILENAME_KEY%>" value="" />
			<select name="<%=ConstantValues.WIKI_ATTACH_TYPE_KEY%>">
			  <option value="<%=WikiAttachmentType.DB.toString()%>">DB</option>
			  <option value="<%=WikiAttachmentType.filesystem.toString()%>">File System</option>
			</select>
			<input type="file" name="filepath" value="" size="40"  />
			<input type="submit" value="Attach File" onclick="return updateName(this.form)" />
		</p>
	</form>
  </body>
</html>