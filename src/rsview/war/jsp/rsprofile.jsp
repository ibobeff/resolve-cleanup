<!DOCTYPE html>
<html>
  <head>
    <meta http-equiv="content-type" content="text/html; charset=UTF-8">

    <!--                                                               -->
    <!-- Consider inlining CSS to reduce the number of requested files -->
    <!--                                                               -->
    <link rel="SHORTCUT ICON" href="/resolve/favicon.ico"/>
    <link type="text/css" rel="stylesheet" href="/resolve/css/gxt-all.css"/>
    <link type="text/css" rel="stylesheet" href="/resolve/css/gxt-gray.css"/>
    <link type="text/css" rel="stylesheet" href="/resolve/css/rsprofile.css"/>
    
    <!--                                           -->
    <!-- Any title is fine                         -->
    <!--                                           -->
    <title>Resolve RBA</title>
    
    <!--                                           -->
    <!-- This script loads your compiled module.   -->
    <!-- If you add any GWT meta tags, they must   -->
    <!-- be added before this line.                -->
    <!--                                           -->
	<script type="text/javascript" language="javascript" src="/resolve/js/extjs4/ext-all.js"></script>
    <script language="javascript" type="text/javascript" src="/resolve/js/resolve.js"></script>
    <script type="text/javascript" language="javascript" src="/resolve/rsprofile/rsprofile.nocache.js"></script>
  </head>

  <!--                                           -->
  <!-- The body can have arbitrary html, or      -->
  <!-- you can leave the body empty if you want  -->
  <!-- to create a completely dynamic UI.        -->
  <!--                                           -->
  <body>
    <!-- OPTIONAL: include this if you want history support -->
    <iframe src="javascript:''" id="__gwt_historyFrame" tabIndex='-1' style="position:absolute;width:0;height:0;border:0"></iframe>
  </body>
</html>
