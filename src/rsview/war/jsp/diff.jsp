<%@page contentType="text/html"%>
<%@page pageEncoding="UTF-8"%>

<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@taglib uri="http://www.springframework.org/tags/form" prefix="form" %>
<%@taglib uri="http://www.springframework.org/tags" prefix="spring" %>


<html>
    <head>
    
        <link type="text/css" rel="stylesheet" href="/resolve/css/diffview.css"/>
    
        <script language="javascript" type="text/javascript" src="/resolve/js/resolve.js"></script>
    
        <script language="javascript" type="text/javascript" src="/resolve/js/dojo.js"></script>
        <script language="javascript" type="text/javascript" src="/resolve/js/diffview.js"></script>
        <script language="javascript" type="text/javascript" src="/resolve/js/difflib.js"></script>
    
        <script language="javascript">
        //refer - http://snowtide.com/files/media/jsdifflib/jsdifflibtest.html
        //http://snowtide.com/jsdifflib
            function diff() 
            {
                var base =  difflib.stringAsLines(document.getElementById("revision1").value);
                var newtxt = difflib.stringAsLines(document.getElementById("revision2").value);
                var sm = new difflib.SequenceMatcher(base, newtxt);
                var opcodes = sm.get_opcodes();
                var diffoutputdiv = document.getElementById("diffoutput");
                while (diffoutputdiv.firstChild) diffoutputdiv.removeChild(diffoutputdiv.firstChild);
                var contextSize = null;
                diffoutputdiv.appendChild(diffview.buildView({ baseTextLines:base,
                                                               newTextLines:newtxt,
                                                               opcodes:opcodes,
                                                               baseTextName:"${revision1.displayName}",
                                                               newTextName:"${revision2.displayName}",
                                                               contextSize:contextSize,
                                                               viewType: 0}));
            }
        
        </script>    
    </head>
    
    <body>
        <textarea id="revision1" style="display: none;">${revision1.text}</textarea>
        <textarea id="revision2" style="display: none;">${revision2.text}</textarea>
        <div id="diffoutput" style="width:100%"> </div>
    </body>
    
    <script language="javascript">
        addLoadEvent(diff);
    </script>

</html>