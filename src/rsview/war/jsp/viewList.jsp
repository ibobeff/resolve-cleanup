<!DOCTYPE html>
<%@page contentType="text/html"%>
<%@page pageEncoding="UTF-8"%>

<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@taglib uri="http://www.springframework.org/tags/form" prefix="form" %>
<%@taglib uri="http://www.springframework.org/tags" prefix="spring" %>


<html>
  <head>
  	<title>Resolve RBA</title>
  	<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
  	<META HTTP-EQUIV="Pragma" CONTENT="no-cache">
	<META HTTP-EQUIV="Expires" CONTENT="-1">
	<META HTTP-EQUIV="Expires" CONTENT="Tue, 01 Jan 1980 1:00:00 GMT">  
  	<link href="/resolve/css/style.css" rel="stylesheet" type="text/css"  />
	<link href="/resolve/css/ie.css" rel="stylesheet" type="text/css" title="default" />
	  		
  </head>
  <body>
    ${content}
    
     <table>
    	<tr>
    		<td>Page Name</td>
    		<td>Modified By</td>
    		<td>Date</td>
    	</tr>
    	
    	<c:forEach items="${listdoc}" varStatus="loop">
    		<tr>   
	        	<td><a href='${listdoc[loop.index].viewLink}'>${listdoc[loop.index].documentName}</a> <a href='${listdoc[loop.index].editLink}'>[Edit]</a></td>
	        	<td>${listdoc[loop.index].modifiedBy}</td>  
	        	<td>${listdoc[loop.index].modifiedOn}</td>  
        	</tr>     
      	</c:forEach>      
    	
    </table>
    
    
    
  </body>
</html>