<!DOCTYPE html>
<%@page import="org.owasp.esapi.ESAPI"%>
<html>
  <head>
  <title>Resolve RBA </title>
  <link rel="SHORTCUT ICON" href="/resolve/favicon.ico"/>
  <link rel="stylesheet" href="/resolve/css/rsview.css" type="text/css"/> 
  </head>
<body>
    </br><center><b><font color="RED"><% 
    	String encodedExternalSystem = ESAPI.encoder().encodeForHTML(request.getParameter("externalSystem"));
    	out.write(encodedExternalSystem);
    %> configuration missing or incomplete in Resolve. Please contact system administrator.</font></b></center>
</body>

</html>