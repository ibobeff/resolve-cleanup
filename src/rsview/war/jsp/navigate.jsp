<!DOCTYPE html>
<%@page contentType="text/html"%>
<%@page pageEncoding="UTF-8"%>
<%@page import="com.resolve.services.constants.WikiAction,com.resolve.services.constants.ConstantValues" %>
<%@ page import="org.owasp.esapi.ESAPI" %>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@taglib uri="http://www.springframework.org/tags/form" prefix="form" %>
<%@taglib uri="http://www.springframework.org/tags" prefix="spring" %>


<html>
  <head>
  	<title>Resolve RBA</title>
  	<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
  	<META HTTP-EQUIV="Pragma" CONTENT="no-cache">
	<META HTTP-EQUIV="Expires" CONTENT="-1">
	<META HTTP-EQUIV="Expires" CONTENT="Tue, 01 Jan 1980 1:00:00 GMT">  
  	<link href="/resolve/css/style.css" rel="stylesheet" type="text/css"  />
	<link href="/resolve/css/ie.css" rel="stylesheet" type="text/css" title="default" />
	
	<script language="javascript" type="text/javascript" src="/resolve/js/resolve.js"></script>
	  		
  </head>
  <body>
  	<c:if test="${TYPE == 'WIKILINK'}">
    	<font face='arial'><h3><%=ESAPI.encoder().encodeForHTML(fullname)%></h3>Right-click displays selected wiki document. </font>
 	</c:if>
  	<c:if test="${TYPE == 'WIKITAG'}">
    	<font face='arial'><h3>Tag Definition</h3>Right-click displays Wiki documents with selected tag. </font>
 	</c:if>
  	<c:if test="${TYPE == 'ACTION'}">
    	<font face='arial'><h3>Tag Definition</h3>Right-click displays ActionTasks with selected tag. </font>
 	</c:if>
 	 	
    <applet code="prefuse.resolve.applets.TreeViewApplet" archive="/resolve/applet/rsprefuse.jar" width="100%" height="90%">
       <param name='<%=ConstantValues.TYPE_KEY%>' value="${TYPE}"/>
       <param name='<%=ConstantValues.LOCATION_KEY%>' value="${LOCATION}" />
       <param name='NAME' value="<%=ESAPI.encoder().encodeForHTML(docname)%>"/>
       <param name='WEB' value="<%=ESAPI.encoder().encodeForHTML(namespace)%>"/>
       <param name='<%=ConstantValues.LASTMODIFIED%>' value="${LASTMODIFIED}"/>
       <param name='<%=ConstantValues.CONTROL_ID%>' value="${CONTROL_ID}"/>
       <param name='<%=ConstantValues.TIMEZONE%>' value="${TIMEZONE}"/>
       <param name='<%=ConstantValues.GWT_ENTRY_POINT%>' value="${GWT_ENTRY_POINT}"/>
    </applet>

  </body>
</html>