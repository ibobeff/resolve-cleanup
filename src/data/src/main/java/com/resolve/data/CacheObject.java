/******************************************************************************
* (C) Copyright 2014
*
* Resolve Systems, LLC
*
* All rights reserved
* This software is distributed under license and may not be used, copied,
* modified, or distributed without the express written permission of
* Resolve Systems, LLC.
*
******************************************************************************/

package com.resolve.data;

import java.io.Serializable;

public abstract class CacheObject implements Serializable
{
    private static final long serialVersionUID = -6728938178464216672L;
	protected String sid;
    
    public String getSid()
    {
        return this.sid;
    } // getSid
    
    public void setSid(String sid)
    {
        this.sid = sid;
    } // setSid
    
    public abstract void cache();
    
    public abstract void removeCache();
    
} // CacheObject
