/******************************************************************************
* (C) Copyright 2014
*
* Resolve Systems, LLC
*
* All rights reserved
* This software is distributed under license and may not be used, copied,
* modified, or distributed without the express written permission of
* Resolve Systems, LLC.
*
******************************************************************************/

package com.resolve.data;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import com.resolve.util.Constants;

public class DataListMap extends DataBase
{
    public DataListMap()
    {
        this.type = Constants.DATA_TYPE_LISTMAP;
        this.data = new ArrayList();            // list -> map
    } // DataListMap
    
    public void add(Map values)
    {
        ((List)this.data).add(values);
    } // add
    
    public String toString()
    {
        String result = "";
        
        result += "type: "+this.type+"\n";
        for (Iterator i=((List)this.data).iterator(); i.hasNext(); )
        {
            Map values = (Map)i.next();
            for (Iterator row=values.entrySet().iterator(); row.hasNext(); )
            {
                Map.Entry entry = (Map.Entry)row.next();
                result += entry.getKey()+":"+entry.getValue()+", ";
            }
            result += "\n";
        }
        
        return result;
    } // toString
    
    public Map getAt(int idx)
    {
        return (Map)((List)data).get(idx);
    } // getAt
    
    public List getDataList()
    {
        return (List)this.data;
    } // getDataList

} // DataListMap
