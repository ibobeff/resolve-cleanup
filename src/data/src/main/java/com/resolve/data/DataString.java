/******************************************************************************
* (C) Copyright 2014
*
* Resolve Systems, LLC
*
* All rights reserved
* This software is distributed under license and may not be used, copied,
* modified, or distributed without the express written permission of
* Resolve Systems, LLC.
*
******************************************************************************/

package com.resolve.data;

import com.resolve.util.Constants;

public class DataString extends DataBase
{
    public DataString()
    {
        this.type = Constants.DATA_TYPE_STRING;
        this.data = null;
    } // DataString
    
    public DataString(String data)
    {
        this.type = Constants.DATA_TYPE_STRING;
        if (data == null)
        {
            data = "";
        }
        this.data = data;
    } // DataString
    
    public String get()
    {
        return (String)this.data;
    } // get
    
    public void put(String value)
    {
        if (value == null)
        {
            value = "";
        }
        this.data = value;
    } // put
    
    public String toString()
    {
        return (String)this.data;
    } // toString

} // DataString
