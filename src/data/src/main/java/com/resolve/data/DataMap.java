/******************************************************************************
* (C) Copyright 2014
*
* Resolve Systems, LLC
*
* All rights reserved
* This software is distributed under license and may not be used, copied,
* modified, or distributed without the express written permission of
* Resolve Systems, LLC.
*
******************************************************************************/

package com.resolve.data;

import java.util.Iterator;
import java.util.Map;
import java.util.LinkedHashMap;

import com.resolve.util.XDoc;
import com.resolve.util.Constants;

public class DataMap extends DataBase
{
    public DataMap()
    {
        this.type = Constants.DATA_TYPE_MAP;
        this.data = new LinkedHashMap();
    } // DataMap
    
    public void put(String key, String value)
    {
        key = XDoc.getValidName(key);
        value = value.trim();
        
        ((LinkedHashMap)this.data).put(key, value);
    } // put
    
    public String toString()
    {
        String result = "";
        
        result += "type: "+this.type+"\n";
        
        for (Iterator i=((LinkedHashMap)this.data).entrySet().iterator(); i.hasNext(); )
        {
            Map.Entry entry = (Map.Entry)i.next();
            result += "key: "+entry.getKey()+" value: "+entry.getValue()+"\n";
        }
        result += "\n";
        
        return result;
    } // toString

    public Map getDataMap()
    {
        return (Map)this.data;
    } // getDataMap

} // DataMap
