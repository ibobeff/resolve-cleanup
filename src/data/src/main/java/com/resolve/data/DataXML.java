/******************************************************************************
* (C) Copyright 2014
*
* Resolve Systems, LLC
*
* All rights reserved
* This software is distributed under license and may not be used, copied,
* modified, or distributed without the express written permission of
* Resolve Systems, LLC.
*
******************************************************************************/

package com.resolve.data;

import com.resolve.util.XDoc;
import com.resolve.util.Constants;

public class DataXML extends DataBase
{
    public DataXML() throws Exception
    {
        this.type = Constants.DATA_TYPE_XML;
        this.data = null;
    } // DataString
    
    public void put(XDoc value)
    {
        this.data = value;
    } // put
    
    public String toString()
    {
        String result = "";
        
        result += "type: "+this.type+"\n";
        result += ((XDoc)this.data).toString()+"\n";
        
        return result;
    } // toString

} // DataXML
