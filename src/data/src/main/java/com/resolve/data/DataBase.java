/******************************************************************************
* (C) Copyright 2014
*
* Resolve Systems, LLC
*
* All rights reserved
* This software is distributed under license and may not be used, copied,
* modified, or distributed without the express written permission of
* Resolve Systems, LLC.
*
******************************************************************************/

package com.resolve.data;

public abstract class DataBase
{
    String type;
    Object data;

    public String getType()
    {
        return type;
    } // getType

    public Object getData()
    {
        return this.data;
    } // getData
    
    public void setData(Object data) throws Exception
    {
        this.data = data;
        this.type = getDataType();
        
        if (type == null) 
        {
            throw new Exception("Invalid data type: "+data.getClass().getName());
        }
    } // setData
    
    public String getDataType()
    {
        String result = null;
        
        if (data instanceof DataString)
        {
            result = "STRING";
        }
        else if (data instanceof DataXML)
        {
            result = "XML";
        }
        else if (data instanceof DataMap)
        {
            result = "MAP";
        }
        else if (data instanceof DataList)
        {
            result = "LIST";
        }
        else if (data instanceof DataListMap)
        {
            result = "LISTMAP";
        }
        
        return result;
    } // getDataType
    
} // DataBase
 