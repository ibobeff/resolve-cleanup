/******************************************************************************
* (C) Copyright 2014
*
* Resolve Systems, LLC
*
* All rights reserved
* This software is distributed under license and may not be used, copied,
* modified, or distributed without the express written permission of
* Resolve Systems, LLC.
*
******************************************************************************/

package com.resolve.data;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import com.resolve.util.Constants;

public class DataList extends DataBase
{
    public DataList()
    {
        this.type = Constants.DATA_TYPE_LIST;
        this.data = new ArrayList();            // list -> map
    } // DataListMap
    
    public void add(Object values)
    {
        ((List)this.data).add(values);
    } // add
    
    public String toString()
    {
        String result = "";
        
        result += "type: "+this.type+"\n";
        for (Iterator i=((List)this.data).iterator(); i.hasNext(); )
        {
            result += i.next().toString();
	        result += "\n";
        }
        
        return result;
    } // toString
    
    public Map getAt(int idx)
    {
        return (Map)((List)data).get(idx);
    } // getAt
    
} // DataList
