package com.resolve.codeGenerate;

import japa.parser.ASTHelper;
import japa.parser.JavaParser;
import japa.parser.ParseException;
import japa.parser.ast.CompilationUnit;
import japa.parser.ast.body.FieldDeclaration;
import japa.parser.ast.body.MethodDeclaration;
import japa.parser.ast.body.ModifierSet;
import japa.parser.ast.body.Parameter;
import japa.parser.ast.body.VariableDeclarator;
import japa.parser.ast.body.VariableDeclaratorId;
import japa.parser.ast.body.ConstructorDeclaration;
import japa.parser.ast.expr.AnnotationExpr;
import japa.parser.ast.expr.AssignExpr;
import japa.parser.ast.expr.BinaryExpr;
import japa.parser.ast.expr.BooleanLiteralExpr;
import japa.parser.ast.expr.CastExpr;
import japa.parser.ast.expr.Expression;
import japa.parser.ast.expr.FieldAccessExpr;
import japa.parser.ast.expr.LongLiteralExpr;
import japa.parser.ast.expr.MarkerAnnotationExpr;
import japa.parser.ast.expr.MemberValuePair;
import japa.parser.ast.expr.MethodCallExpr;
import japa.parser.ast.expr.NameExpr;
import japa.parser.ast.expr.NormalAnnotationExpr;
import japa.parser.ast.expr.ObjectCreationExpr;
import japa.parser.ast.expr.StringLiteralExpr;
import japa.parser.ast.expr.SuperExpr;
import japa.parser.ast.expr.ThisExpr;
import japa.parser.ast.expr.UnaryExpr;
import japa.parser.ast.stmt.BlockStmt;
import japa.parser.ast.stmt.CatchClause;
import japa.parser.ast.stmt.EmptyStmt;
import japa.parser.ast.stmt.ExplicitConstructorInvocationStmt;
import japa.parser.ast.stmt.ExpressionStmt;
import japa.parser.ast.stmt.IfStmt;
import japa.parser.ast.stmt.ReturnStmt;
import japa.parser.ast.stmt.Statement;
import japa.parser.ast.stmt.TryStmt;
import japa.parser.ast.type.ClassOrInterfaceType;

import java.io.ByteArrayInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.PrintWriter;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashSet;
import java.util.LinkedList;
import java.util.List;

import com.resolve.codeGenerate.GatewayData.GATEWAY_TYPE;

public class GenerateBasedTemp
{
    static final String TEMPLATE_PROJECT = new String("templateProject");
    static final String TEMPLATE_PACKAGE = new String("templatePackage");
    static final String TEMPLATE_NAME_LOWER = new String("template");
    static final String TEMPLATE_NAME_UP = new String("Template");
    static final String TEMPLATE_NAME_UPPER = new String("TEMPLATE");
    static HashSet<String> excludeSet = new HashSet<String>();
    static{
        excludeSet.add("FilterTest.java");
        excludeSet.add("UnitTestSuite.java");
        excludeSet.add("TemplateAPI.java");
        
    }
    //Method for generating files and directory for SDK
    public static void generateSourceCode(File dir, GatewayData data) throws IOException, ParseException{
        LinkedList<File> queue = new LinkedList<File>();
        File cur = dir;
        //Checking for existing files
        File temp = new File(dir.getAbsolutePath().replace(dir.getName(), data.getName()));
        
        if(temp != null && temp.exists()){
            DateFormat dateFormat = new SimpleDateFormat("yyyyMMddHHmmss");
            String newName = data.getName()+dateFormat.format(new Date()).toString();
            System.out.println("It renames your last time generated " + data.getName() + " project to " + newName);
            
            temp.renameTo(new File(dir.getAbsolutePath().replace(dir.getName(), newName)));
        }
        
        System.out.println("Generate eclipse project " + data.getName() + " in " + temp.getAbsolutePath());
        
        while(cur!=null){
            if(cur.isDirectory()){
                File newDir = new File(cur.getPath().replace(TEMPLATE_PROJECT, data.getName().toLowerCase()).replace(
                                TEMPLATE_PACKAGE, data.getPackageName().replace(".", File.separator)));
                newDir.mkdirs();
                File[] files = cur.listFiles();
                for(File f : files){
                    queue.add(f);
                }
            }
            else if(!cur.getName().endsWith("TemplateGateway.java") || cur.getName().endsWith(data.type.toString()+"TemplateGateway.java")){
               
                    File newFile = new File(cur.getPath().replace(TEMPLATE_PROJECT, data.getName().toLowerCase()).replace
                                    (TEMPLATE_PACKAGE, data.getPackageName().replace(".", File.separator)).replace(data.getType().toString()+
                                                    TEMPLATE_NAME_UP, data.getNameUp()).replace(TEMPLATE_NAME_UP, data.getNameUp()));
                    FileInputStream fis = new FileInputStream(cur);
                    byte[] byteData = new byte[(int) cur.length()];
                    fis.read(byteData);
                    fis.close();
                    String str = new String(byteData, "UTF-8");
                    
                    
                    if(cur.getName().endsWith(".java") && !excludeSet.contains(cur.getName())){
                        
                        InputStream in = new ByteArrayInputStream(str.replace(TEMPLATE_PACKAGE, data.getPackageName()).replace(TEMPLATE_NAME_LOWER, 
                                       data.getNameLower()).replace(TEMPLATE_NAME_UP, data.getNameUp()).replace(TEMPLATE_NAME_UPPER, data.getNameUpper()).getBytes("UTF-8"));
                        
                        CompilationUnit cu;
                        try {
                            // parse the file
                            cu = JavaParser.parse(in);
                        } finally {
                            in.close();
                        }
                        
                        addCustomizedData(cu, data);
                        PrintWriter out = new PrintWriter(newFile);
                        out.println(cu.toString());
                        out.close();
                    }
                    else{
                        PrintWriter out = new PrintWriter(newFile);
                        out.println(str.replace(TEMPLATE_PACKAGE, data.getPackageName()).replace(TEMPLATE_NAME_LOWER, data.getNameLower()).replace(TEMPLATE_NAME_UP, data.getNameUp()).replace(TEMPLATE_NAME_UPPER, data.getNameUpper()));
                        out.close();
                    }
                
                
            }
            if(!queue.isEmpty()){
                cur=queue.pollFirst(); 
            }
            else{
                break;
            }
        }
    }
    
    private static void addCustomizedData(CompilationUnit cu, GatewayData data)
    {
        String packageName = cu.getPackage().getName().toString();
        HashSet<Field> fields = data.getFilter();
        HashSet<Field> configs = data.getConfigs();
        if(packageName.equals("com.resolve.services.hibernate.vo")){
            for(Field field : fields){
                addField(cu, field, true);
            }
            for(Field field : fields){
                addGetMethod(cu, field, true, false).setAnnotations(createMappingAnnotation(field));
                addSetMethod(cu, field, true, false);
            }
       
        }
        else if(packageName.equals("com.resolve.rscontrol")){
       
        }
        else if(packageName.equals("com.resolve.persistence.model")){
            MethodDeclaration  ugetCdataProperty = (MethodDeclaration) cu.getTypes().get(0).getMembers().remove(cu.getTypes().get(0).getMembers().size()-1);
            MethodDeclaration  applyVOToModel = (MethodDeclaration) cu.getTypes().get(0).getMembers().remove(cu.getTypes().get(0).getMembers().size()-1);
            MethodDeclaration  doGetVO = (MethodDeclaration) cu.getTypes().get(0).getMembers().remove(cu.getTypes().get(0).getMembers().size()-1);
            
            for(Field field : fields){
                addField(cu, field, true);
            }
            for(Field field : fields){
                addGetMethod(cu, field, true, false).setAnnotations(createColumnAnnotation(field));
                addSetMethod(cu, field, true, false);

                MethodCallExpr exprArg = new MethodCallExpr(null, "get" + field.getName(true));
                ExpressionStmt esArg = new ExpressionStmt(exprArg);
                ArrayList<Expression> args = new ArrayList<Expression>();
                args.add(exprArg);
                MethodCallExpr expr = new MethodCallExpr(new NameExpr("vo"), "set" + field.getName(true), args);
                ExpressionStmt es = new ExpressionStmt(expr);
                doGetVO.getBody().getStmts().add(es);
                
                
                exprArg = new MethodCallExpr(new NameExpr("vo"), "get" + field.getName(true));
                esArg = new ExpressionStmt(exprArg);
                args = new ArrayList<Expression>();
                args.add(exprArg);
                expr = new MethodCallExpr(new ThisExpr(), "set" + field.getName(true), args);
                es = new ExpressionStmt(expr);
                
                if(field.getJavaType().equalsIgnoreCase("boolean")){
                    applyVOToModel.getBody().getStmts().add(es);
                }
                else{
                    MethodCallExpr condition = new MethodCallExpr(new FieldAccessExpr(new NameExpr("VO"), field.getJavaType().toUpperCase().concat("_DEFAULT")), "equals", args);
                    IfStmt voToModel = new IfStmt(new UnaryExpr(condition, UnaryExpr.Operator.not),es, new EmptyStmt());
                    applyVOToModel.getBody().getStmts().add(voToModel);
                }
                
            }
            
            ReturnStmt rs = new ReturnStmt(new NameExpr("vo"));
            doGetVO.getBody().getStmts().add(rs);
            cu.getTypes().get(0).getMembers().add(doGetVO);
            cu.getTypes().get(0).getMembers().add(applyVOToModel);
            
            boolean includeXML = false;
            for(Field field : fields){
                if(field.getJavaType().equalsIgnoreCase("String")){
                    includeXML = true;
                    ArrayList<Expression> args = new ArrayList<Expression>();
                    args.add(new StringLiteralExpr(field.getName(true)));
                    MethodCallExpr expr = new MethodCallExpr(new NameExpr("list"), "add", args);
                    ugetCdataProperty.getBody().getStmts().add(new ExpressionStmt(expr));
                }
            }
            if(includeXML){
                ugetCdataProperty.getBody().getStmts().add(new ReturnStmt(new NameExpr("list")));
                cu.getTypes().get(0).getMembers().add(ugetCdataProperty);
            }
            
        }
        else if(packageName.equals("com.resolve.gateway")){
            MethodDeclaration  populateGatewaySpecificFilterProperties = (MethodDeclaration) cu.getTypes().get(0).getMembers().remove(cu.getTypes().get(0).getMembers().size()-1);
            for(Field field : fields){
                FieldAccessExpr esArg1 = new FieldAccessExpr(new NameExpr(data.getName().concat("Filter")), field.getName().toUpperCase());
                ArrayList<Expression> args = new ArrayList<Expression>();
                args.add(esArg1);
                MethodCallExpr esArg2 = new MethodCallExpr(new NameExpr(data.getNameLower().concat("Filter")), "get" + field.getNameUp(), new ArrayList<Expression>());
                if(field.getJavaType().equals("String")){
                    args.add(esArg2);
                }
                else{
                    args.add(new BinaryExpr(new StringLiteralExpr(""), esArg2,  BinaryExpr.Operator.plus));
                }
                
                MethodCallExpr expr = new MethodCallExpr(new NameExpr("filterMap"), "put", args);
                IfStmt stmt = (japa.parser.ast.stmt.IfStmt) populateGatewaySpecificFilterProperties.getBody().getStmts().get(0);
                BlockStmt block =  (BlockStmt) stmt.getThenStmt();
                block.getStmts().add(new ExpressionStmt(expr));
            }
            cu.getTypes().get(0).getMembers().add(populateGatewaySpecificFilterProperties);
        }
        else if(packageName.equals(data.getPackageName())){
            String className = cu.getTypes().get(0).getName();
            if(className.endsWith("Filter")){
                for(Field field : fields){
                    FieldDeclaration fd = new FieldDeclaration(ModifierSet.STATIC | ModifierSet.PUBLIC | ModifierSet.FINAL, new ClassOrInterfaceType("String"), new VariableDeclarator(new VariableDeclaratorId (field.getName().toUpperCase()), new StringLiteralExpr(field.getName().toUpperCase())));
                    ASTHelper.addMember(cu.getTypes().get(0), fd);
                }
                for(Field field : fields){
                    addField(cu, field, false);
                }
                
                String[] params = {"id", "active", "order", "interval", "eventEventId", "runbook", "script"};
                ArrayList<Statement> stmts = new ArrayList<Statement>();
                BlockStmt block = new BlockStmt(stmts);
                ArrayList<Expression> args = new ArrayList<Expression>();
                for(String param : params)  args.add(new NameExpr(param));
                block.getStmts().add(new ExplicitConstructorInvocationStmt(false, null, args));
                for(Field field : fields) {
                    if(field.getJavaType().equals("String")){
                        block.getStmts().add(new ExpressionStmt(new AssignExpr(new FieldAccessExpr(new ThisExpr(), field.getName()), new NameExpr(field.getName()), AssignExpr.Operator.assign)));
                    }
                    else{
                        ArrayList<Expression> args1 = new ArrayList<Expression>();
                        args1.add(new NameExpr(field.getName()));
                        ObjectCreationExpr oe = new ObjectCreationExpr(null, new ClassOrInterfaceType(field.getJavaType()), args1);
                        BlockStmt tryBlock = new BlockStmt(new ArrayList<Statement>());
                        tryBlock.getStmts().add(new ExpressionStmt(new AssignExpr(new FieldAccessExpr(new ThisExpr(), field.getName()), oe, AssignExpr.Operator.assign)));
                   
                        ArrayList<Expression> args2 = new ArrayList<Expression>();
                        args2.add(new BinaryExpr(new BinaryExpr(new StringLiteralExpr(field.getName()), new StringLiteralExpr(" should be of type "), BinaryExpr.Operator.plus), new StringLiteralExpr(field.getJavaType()), BinaryExpr.Operator.plus));
                        MethodCallExpr printOutErr = new MethodCallExpr(new FieldAccessExpr(new NameExpr("Log"), "log"), "error", args2);
                        BlockStmt catchBlock = new BlockStmt(new ArrayList<Statement>());
                        catchBlock.getStmts().add(new ExpressionStmt(printOutErr));
                        Parameter param = new Parameter(new ClassOrInterfaceType("Exception"), new VariableDeclaratorId("e"));
                        CatchClause cc = new CatchClause(param, catchBlock);
                        List<CatchClause> catchs = new ArrayList<CatchClause>();
                        catchs.add(cc);
                        TryStmt ts = new TryStmt(tryBlock, catchs, null);
                        
                        block.getStmts().add(ts);
                    }
                }
                ArrayList<Parameter> parameters = new ArrayList<Parameter>();
                for(String param : params) parameters.add(new Parameter(new ClassOrInterfaceType("String"), new VariableDeclaratorId(param)));
                for(Field field : fields)      parameters.add(new Parameter(new ClassOrInterfaceType("String"), new VariableDeclaratorId(field.getName())));
                
                cu.getTypes().get(0).getMembers().add(new ConstructorDeclaration(null, ModifierSet.PUBLIC, null, null, data.getNameUp().concat("Filter"), parameters, null, block));
                
                for(Field field : fields){
                    addGetMethod(cu, field, false, false);
                    addSetMethod(cu, field, false, false);
                }
                
            }
            else if(className.endsWith("Gateway")){
                MethodDeclaration  getFilter = (MethodDeclaration) cu.getTypes().get(0).getMembers().remove(cu.getTypes().get(0).getMembers().size()-1);
                ReturnStmt rs = (ReturnStmt) getFilter.getBody().getStmts().get(0);
                ObjectCreationExpr expr = (ObjectCreationExpr) rs.getExpr();
                for(Field field : fields){
                    ArrayList<Expression> args = new ArrayList<Expression>();
                    args.add(new FieldAccessExpr(new NameExpr(data.getNameUp().concat(("Filter"))), field.getName().toUpperCase()));
                    MethodCallExpr e = new MethodCallExpr(new NameExpr("params"), "get", args);
                    expr.getArgs().add(new CastExpr(new ClassOrInterfaceType("String"), e));
                }
                
                MethodDeclaration  stop = (MethodDeclaration) cu.getTypes().get(0).getMembers().remove(cu.getTypes().get(0).getMembers().size()-1);
                MethodDeclaration  initialize = (MethodDeclaration) cu.getTypes().get(0).getMembers().remove(cu.getTypes().get(0).getMembers().size()-1);
                String initializeMessage = null;
                if(data.getType().toString().equalsIgnoreCase("PUSH")){
                    MethodDeclaration  cleanFilter = (MethodDeclaration) cu.getTypes().get(0).getMembers().remove(cu.getTypes().get(0).getMembers().size()-1);
                    ArrayList<Statement> stmt = new ArrayList<Statement>();
                    stmt.add(new ExpressionStmt(new NameExpr("//Add customized code here to modify your server when you clear out the deployed filters")));
                    cleanFilter.getBody().setStmts(stmt);
                    cu.getTypes().get(0).getMembers().add(cleanFilter);
                    
                    initializeMessage = new String("//Add customized code here to initilize your server based on configuration and deployed filter data");
                    ArrayList<Statement> stmt1 = new ArrayList<Statement>();
                    stmt1.add(new ExpressionStmt(new NameExpr(initializeMessage)));
                    initialize.getBody().setStmts(stmt1);
                }
                else{
                    MethodDeclaration  invokeService = (MethodDeclaration) cu.getTypes().get(0).getMembers().remove(cu.getTypes().get(0).getMembers().size()-1);
                    invokeService.getBody().getStmts().add(new ExpressionStmt(new NameExpr("//Add customized code here to get data from 3rd party system based the information in the filter")));
                    invokeService.getBody().getStmts().add(new ReturnStmt(new NameExpr("result")));
                    cu.getTypes().get(0).getMembers().add(invokeService);
                    
                    initializeMessage = new String("//Add customized code here to initilize the connection with 3rd party system");
                    initialize.getBody().getStmts().add(new ExpressionStmt(new NameExpr(initializeMessage)));
                }
                
                stop.getBody().getStmts().add(new ExpressionStmt(new NameExpr("//Add customized code here to stop the connection with 3rd party system")));
                stop.getBody().getStmts().add(new ExpressionStmt(new MethodCallExpr(new SuperExpr(), "stop", null)));
                cu.getTypes().get(0).getMembers().add(initialize);
                cu.getTypes().get(0).getMembers().add(stop);
                cu.getTypes().get(0).getMembers().add(getFilter);
            }
            else if(className.startsWith("ConfigReceive")){
                MethodDeclaration  putFilterDataIntoEntry = (MethodDeclaration) cu.getTypes().get(0).getMembers().remove(cu.getTypes().get(0).getMembers().size()-1);
                MethodDeclaration  save = (MethodDeclaration) cu.getTypes().get(0).getMembers().remove(cu.getTypes().get(0).getMembers().size()-1);
                MethodDeclaration  load = (MethodDeclaration) cu.getTypes().get(0).getMembers().remove(cu.getTypes().get(0).getMembers().size()-1);
                MethodDeclaration  getRootNode = (MethodDeclaration) cu.getTypes().get(0).getMembers().remove(cu.getTypes().get(0).getMembers().size()-1);
                ConstructorDeclaration  ConfigReceiveTemplate = (ConstructorDeclaration) cu.getTypes().get(0).getMembers().remove(cu.getTypes().get(0).getMembers().size()-1);
                MethodDeclaration  setQueue = (MethodDeclaration) cu.getTypes().get(0).getMembers().remove(cu.getTypes().get(0).getMembers().size()-1);
                MethodDeclaration  getQueue = (MethodDeclaration) cu.getTypes().get(0).getMembers().remove(cu.getTypes().get(0).getMembers().size()-1);
                
                for(Field cf : configs){
                    ArrayList<Expression> args = new ArrayList<Expression>();
                    args.add(new StringLiteralExpr(cf.getName()));
                    args.add(new NameExpr(cf.getType().toUpperCase()));
                    StringBuilder sb = new StringBuilder().append("RECEIVE_").append(data.getNameUpper()).append("_ATTR_").append(cf.getName().toUpperCase());
                    args.add(new NameExpr(sb.toString()));
                    MethodCallExpr expr = new MethodCallExpr(null, "define", args);
                    ConfigReceiveTemplate.getBlock().getStmts().add(new ExpressionStmt(expr));
                    
                    BinaryExpr be = new BinaryExpr(new NameExpr("RECEIVE_"+data.getNameUpper()+"_NODE"), new StringLiteralExpr("@"+cf.getNameUpper()),  BinaryExpr.Operator.plus);
                    
                    FieldDeclaration fd = new FieldDeclaration(ModifierSet.PRIVATE | ModifierSet.STATIC | ModifierSet.FINAL, new ClassOrInterfaceType("String"), new VariableDeclarator(new VariableDeclaratorId(sb.toString()), be));
                    ASTHelper.addMember(cu.getTypes().get(0), fd);
                    
                }
                
                for (Field cf : configs)
                {
                    Expression initExpr = null;
                    if(cf.getJavaType().equals("Boolean")){
                        initExpr = new BooleanLiteralExpr(false);
                    }
                    else if(cf.getJavaType().equals("Long")){
                        initExpr =  new NameExpr("0");
                    }
                    else if(cf.getJavaType().equals("Integer")){
                        initExpr =  new NameExpr("0");
                    }
                    else{
                        initExpr = new StringLiteralExpr("");
                    }
                    FieldDeclaration fd = new FieldDeclaration(ModifierSet.PRIVATE, new ClassOrInterfaceType(cf.getPrimitiveType()), new VariableDeclarator(new VariableDeclaratorId(cf.getName()), initExpr));
                    ASTHelper.addMember(cu.getTypes().get(0), fd);
                }

                cu.getTypes().get(0).getMembers().add(getQueue);
                cu.getTypes().get(0).getMembers().add(setQueue);
                
                for (Field cf : configs)
                {
                    addGetMethod(cu, cf, false, true);
                    addSetMethod(cu, cf, false, true);
                }

                for(Field field : fields){
                    ArrayList<Expression> args = new ArrayList<Expression>();
                    MethodCallExpr expr = new MethodCallExpr(new NameExpr(data.getNameLower()+"Filter"), "get"+field.getNameUp(), new ArrayList<Expression>());
                    args.add(new FieldAccessExpr(new NameExpr(data.getName()+"Filter"), field.getName().toUpperCase()));
                    args.add(expr);
                    MethodCallExpr exprPut = new MethodCallExpr(new NameExpr("entry"), "put", args);
                    putFilterDataIntoEntry.getBody().getStmts().add(new ExpressionStmt(exprPut));
                }
                cu.getTypes().get(0).getMembers().add(ConfigReceiveTemplate);
                cu.getTypes().get(0).getMembers().add(getRootNode);
                cu.getTypes().get(0).getMembers().add(load);
                cu.getTypes().get(0).getMembers().add(save);
                cu.getTypes().get(0).getMembers().add(putFilterDataIntoEntry);
            }
        }
    }

    private static void addField(CompilationUnit cu, Field field, boolean isPrefixU) {
        FieldDeclaration fd = new FieldDeclaration(ModifierSet.PRIVATE, new ClassOrInterfaceType(field.getJavaType()), new VariableDeclarator(new VariableDeclaratorId(field.getName(isPrefixU))));
        ASTHelper.addMember(cu.getTypes().get(0), fd);
    }

    private static List<AnnotationExpr> createColumnAnnotation(Field field){
        List<AnnotationExpr> annotations=new ArrayList<AnnotationExpr>(1);
        if(field.type.equalsIgnoreCase("longString")) annotations.add(new MarkerAnnotationExpr(new NameExpr("Lob")));
        List<MemberValuePair> pairs = new ArrayList<MemberValuePair>();
        pairs.add(new MemberValuePair("name", new StringLiteralExpr(new String("u_").concat(field.name).toLowerCase())));
        if(field.maxLength!=0){
            pairs.add(new MemberValuePair("length", new LongLiteralExpr(Integer.toString(field.maxLength))));
        }
        annotations.add(new NormalAnnotationExpr(new NameExpr("Column"), pairs));
        return annotations;
    }

    private static List<AnnotationExpr> createMappingAnnotation(Field field){
        List<AnnotationExpr> annotations=new ArrayList<AnnotationExpr>(1);
        List<MemberValuePair> pairs = new ArrayList<MemberValuePair>();
        pairs.add(new MemberValuePair("columnName", new StringLiteralExpr(field.getNameUpper())));
        annotations.add(new NormalAnnotationExpr(new NameExpr("MappingAnnotation"), pairs));
        return annotations;
    }
    
    private static MethodDeclaration addGetMethod(CompilationUnit cu, Field field, boolean isPrefixU, boolean isPrimitive) {
        MethodDeclaration method = new MethodDeclaration(ModifierSet.PUBLIC, new ClassOrInterfaceType(field.getType(isPrimitive)), "get" + field.getNameUp(isPrefixU));
        method.setModifiers(ModifierSet.addModifier(method.getModifiers(), ModifierSet.PUBLIC));
        BlockStmt block = new BlockStmt();
        method.setBody(block);
        ReturnStmt rs = new ReturnStmt(new FieldAccessExpr(new ThisExpr(), field.getName(isPrefixU)));
        ASTHelper.addStmt(block, rs);
        ASTHelper.addMember(cu.getTypes().get(0), method);
        return method;
    }
    
    private static void addSetMethod(CompilationUnit cu, Field field, boolean isPrefixU, boolean isPrimitive) {
        MethodDeclaration method = new MethodDeclaration(ModifierSet.PUBLIC, ASTHelper.VOID_TYPE, "set" + field.getNameUp(isPrefixU));
        method.setModifiers(ModifierSet.addModifier(method.getModifiers(), ModifierSet.PUBLIC));
        Parameter param = ASTHelper.createParameter(new ClassOrInterfaceType(field.getType(isPrimitive)), field.getNameLow(isPrefixU));
        ASTHelper.addParameter(method, param);
        BlockStmt block = new BlockStmt();
        method.setBody(block);
        AssignExpr ae = new AssignExpr(new FieldAccessExpr(new ThisExpr(), field.getName(isPrefixU)), new NameExpr(field.getNameLow(isPrefixU)), AssignExpr.Operator.assign);
        ASTHelper.addStmt(block, ae);
        ASTHelper.addMember(cu.getTypes().get(0), method);
    }
    
}
