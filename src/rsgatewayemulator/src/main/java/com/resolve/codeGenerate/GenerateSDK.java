package com.resolve.codeGenerate;

import java.io.File;
import java.io.IOException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

import org.apache.commons.io.FileUtils;
import org.apache.commons.io.FilenameUtils;
import org.apache.log4j.*;

import com.resolve.codeGenerate.GatewayData.GATEWAY_TYPE;
import com.resolve.util.EmulatorUtil;


public class GenerateSDK 
{    
    private static Logger log =  Logger.getLogger(GenerateSDK.class);
    static final String TEMPLATE_PROJECT = new String("templateprojectredesigned");
    static final String TEMPLATE_PACKAGE = new String("templatePackage");
    static final String TEMPLATE_NAME_LOWER = new String("template");
    static final String TEMPLATE_NAME_UP = new String("Template");
    static final String TEMPLATE_NAME_UPPER = new String("TEMPLATE");
    static final String GATEWAY = new String("gateway");
    static final String TEST_PATH = new String( File.separator + "src" + File.separator + "com" +  
                        File.separator + "resolve" + File.separator  + "gateway" + File.separator + "test" + File.separator);
    //Stores the path of the Main directory
    File rootDir;
    //Map that stores the file find and replace string (The key will be the find "string" and the value will be the "replace" string
    Map<String, String> fileReplacesMap ;
    
    private EmulatorUtil util;
    public GenerateSDK() {
        util = new EmulatorUtil();
    }

    //Method for generating files and directory for SDK
    public void generateSourceFile(File templateDir, GatewayData data) throws IOException{

        File cur = templateDir;
        //Checking for existing files
        File temp = new File(templateDir.getAbsolutePath().replace(templateDir.getName(), data.getName().toLowerCase()));
        //If exists change the current projects name with date and create a new one
        if(temp != null && temp.exists()){
            DateFormat dateFormat = new SimpleDateFormat("yyyyMMddHHmmss");
            String newName = data.getName()+ dateFormat.format(new Date()).toString();
            log.info("It renames your last time generated " + data.getName() + " project to " + newName);
            temp.renameTo(new File(templateDir.getAbsolutePath().replace(templateDir.getName(), newName)));
        }
        log.info("Generating eclipse project " + data.getName() + " in " + temp.getAbsolutePath());
            //If Directory
        if(cur.isDirectory()){
            rootDir = cur;
            processDir(cur, data);
        }else {
            copyResolveFile(cur,data);
        }
    }
    /**
     * 
     * @param srcFile
     * @param data
     */
   private void processDir(File srcFile, GatewayData data) {
           File newDir;
           String filePath = srcFile.getPath().replace(TEMPLATE_PROJECT,  "connector" + File.separator + data.getName().toLowerCase()).replace(
                           TEMPLATE_PACKAGE, data.getPackageName().replace(".", File.separator));
           //If you have found "gateway folder" Jump to the Specific folder and copy it 
           //e.g. if the  blueprint is for jms jump to copying the jms folder content only
           if(srcFile.getName().equalsIgnoreCase(GATEWAY)) {
                   File[] files = srcFile.listFiles();
                   for(File file : files){
                       if(file.getName().equalsIgnoreCase(data.getType().toString())){
                           copyFileInGWDir(file ,filePath + File.separator + file.getName(),data);
                       }
                   }                   
           }else {
               copyFileInGWDir(srcFile,filePath,data);
           }
   }
   /**
    * 
    * @param srcFile
    * @param data
    */
   private void copyFileInGWDir(File srcFile,  String newDirPath, GatewayData data) {
       File newDir = new File(newDirPath);
       newDir.mkdirs();
       File[] files = srcFile.listFiles();
       try {
           for(File file : files){
               if(file.isDirectory()){                   
                   //Recursive call to add files inside the folder
                   processDir(file, data);
               }else {
                   if(file.getName().equalsIgnoreCase("blueprint.properties")) {
                       //Comparingg with template and and replacing with the new folder structure
                       String strFileName= changeFileLocation(rootDir.getPath(), data);
                       File newFile = new File(strFileName.toLowerCase() +  File.separator + "blueprint.properties");
                       FileUtils.copyFile(file, newFile);
                       //Update the file contents from template to product specific 
                       updatePropFileContents(newFile, data);
                       
                   }else {
                       copyResolveFile(file,data);
                   }
               }
           }
       }catch(Exception ex) {
               log.error("Error when replacing Template text in file Method copyFileInGWDir in class" + this.getClass().getName());
              ex.printStackTrace();
       }
   }
   /**
    * 
    * @param propFile
    * @param gwData
    * @return
    */
   private void updatePropFileContents(File propFile, GatewayData gwData) {
     //Update the blueprint with new data
       try {
           fileReplacesMap = new HashMap<String, String>();
          //Based on what type gate way find and replace strings from the generated blue print properties file
           if(gwData.getType().toString().equals(GATEWAY_TYPE.PUSH.toString())){
               fileReplacesMap.put("push_template", gwData.getName().toLowerCase());
               fileReplacesMap.put("PUSH_TEMPLATE", gwData.getName().toUpperCase());
               fileReplacesMap.put("Push_template", gwData.getName().replace(
                             gwData.getName().charAt(0), Character.toUpperCase(gwData.getName().charAt(0))));
           }
           else if(gwData.getType().toString().equals(GATEWAY_TYPE.PULL.toString())){
               fileReplacesMap.put("pull_template", gwData.getName().toLowerCase());
               fileReplacesMap.put("PULL_TEMPLATE", gwData.getName().toUpperCase());
               fileReplacesMap.put("Pull_template", gwData.getName().replace(
                             gwData.getName().charAt(0), Character.toUpperCase(gwData.getName().charAt(0))));
           }else{
               fileReplacesMap.put("msg_template", gwData.getName().toLowerCase());
               fileReplacesMap.put("MSG_TEMPLATE", gwData.getName().toUpperCase());
               fileReplacesMap.put("Msg_template", gwData.getName().replace(
                             gwData.getName().charAt(0), Character.toUpperCase(gwData.getName().charAt(0))));
           }     
           //Call the util class to replace the file data
           util.replaceFileData(propFile, fileReplacesMap);
       }catch(Exception ex) {
           log.error("Error when replacing Template text in file Method updatePropFileContents in class" + this.getClass().getName());
           ex.printStackTrace();
       }
   }
    /**
     * 
     * @param src
     * @param data
     */
    private void copyResolveFile(File srcFile, GatewayData data) {
        
        try {

            String strFileName= changeFileLocation(srcFile.getPath(), data).replace(TEMPLATE_NAME_LOWER, data.getName().toLowerCase());
            File newFile = new File(strFileName);
            //Ignore copying the compiled class files
            if(!(FilenameUtils.getExtension(newFile.getAbsolutePath()).equalsIgnoreCase("class"))){
                if(newFile.getAbsolutePath().contains("Test")){
                        newFile =new File(rootDir.getAbsolutePath().replace(TEMPLATE_PROJECT,  "connector" + File.separator +
                                        data.getName().toLowerCase()) + TEST_PATH + newFile.getAbsoluteFile().getName());
                  }
                FileUtils.copyFile(srcFile, newFile); 
                //Add the values to the Map for file replacement
                if((FilenameUtils.getExtension(newFile.getAbsolutePath()).equalsIgnoreCase("java"))
                                || (FilenameUtils.getExtension(newFile.getAbsolutePath()).equalsIgnoreCase("html"))){
                    fileReplacesMap = new HashMap<String, String>();
                    fileReplacesMap.put(TEMPLATE_NAME_UP, data.getName());
                    fileReplacesMap.put("push_template", data.getName().toLowerCase());
                    fileReplacesMap.put("pull_template", data.getName().toLowerCase());
                    fileReplacesMap.put("msg_template", data.getName().toLowerCase());
                    util.replaceFileData(newFile, fileReplacesMap);
                }else if((newFile.getName()).equalsIgnoreCase(".project")){
                    fileReplacesMap = new HashMap<String, String>();
                    fileReplacesMap.put("templateproject", data.getName().toLowerCase());
                    util.replaceFileData(newFile, fileReplacesMap);
                }
            }
        }catch(IOException ex) {
            ex.printStackTrace();
        }
    }
    /**
     * 
     * @param fileAbsPath
     * @param data
     * @return
     */
    private String changeFileLocation(String fileAbsPath, GatewayData data) {
        if(fileAbsPath != null) {
          //Replacing the template file path with the new generated project path
          return  fileAbsPath.replace(TEMPLATE_PROJECT,  "connector" + File.separator + data.getName().toLowerCase()).replace
            (TEMPLATE_PACKAGE, data.getPackageName().replace(".", File.separator)).replace(data.getType().toString()+
                            TEMPLATE_NAME_UP, data.getNameUp()).replace(TEMPLATE_NAME_UP, data.getNameUp().replace(data.getType().toString()+
                                            TEMPLATE_NAME_LOWER, data.getNameUp()));
                                                         
        }else {
            return null;
        }
    }
    
}
