package com.resolve.codeGenerate;

public class Field
{
    String name;
    String  type;
    int maxLength;
    public Field(String name, String type, int maxLength)
    {
        super();
        this.name = name;
        if(name.toUpperCase().contains("PASS")){
            this.type=new String("secure");
        }
        else{
            this.type=type;
        }
        if(maxLength==0){
            if(type.equalsIgnoreCase("String"))   maxLength = 256;
            else if(type.equalsIgnoreCase("longString")) maxLength = 16777215; 
            else if(type.equalsIgnoreCase("boolean")) maxLength = 1;  
        }
        this.maxLength = maxLength;
    }
    public Field(String name, String type)
    {
        this(name, type, 0);
        
    }
    public Field(String name)
    {
        this(name, "String", 0);
    }
    public String getNameUpper()
    {
        return name.toUpperCase();
    }
    public String getNameUp()
    {
        return new StringBuilder().append(Character.toUpperCase(name.charAt(0))).append(name.substring(1)).toString();
    }
    public String getNameLow()
    {
        return new StringBuilder().append(Character.toLowerCase(name.charAt(0))).append(name.substring(1)).toString();
    }
    public String getName()
    {
        return name;
    }
    public String getNameUp(boolean isPrefixU)
    {
        if(isPrefixU) return "U" + new StringBuilder().append(Character.toUpperCase(name.charAt(0))).append(name.substring(1)).toString();
        else return new StringBuilder().append(Character.toUpperCase(name.charAt(0))).append(name.substring(1)).toString();
    }
    public String getNameLow(boolean isPrefixU)
    {
        if(isPrefixU) return "u" + new StringBuilder().append(Character.toUpperCase(name.charAt(0))).append(name.substring(1)).toString();
        else return new StringBuilder().append(Character.toLowerCase(name.charAt(0))).append(name.substring(1)).toString();
    }
    public String getName(boolean isPrefixU)
    {
        if(isPrefixU) return "U" + new StringBuilder().append(Character.toUpperCase(name.charAt(0))).append(name.substring(1)).toString();
        else return name;
    }
    public void setName(String name)
    {
        this.name = name;
        if(name.toUpperCase().contains("PASS")){
            this.type=new String("secure");
        }
    }
    public String getJavaType()
    {
        return getType(false);
    }
    public String getPrimitiveType()
    {
        return getType(true);
    }
    public String getType(boolean isPrimitive)
    {
        if(type.equalsIgnoreCase("boolean")){
            if(isPrimitive) return "boolean";
            else return "Boolean";
        }
        else if(type.equalsIgnoreCase("int") || type.equalsIgnoreCase("Integer")){
            if(isPrimitive) return "int";
            else return "Integer";
        }
        else if(type.equalsIgnoreCase("long")){
            if(isPrimitive) return "long";
            else return "Long";
        }
        else if(type.equalsIgnoreCase("longString") || type.equalsIgnoreCase("secure")){
            return "String";
        }
        else{
            return new StringBuilder().append(Character.toUpperCase(type.charAt(0))).append(type.substring(1).toLowerCase()).toString();
        }
    }
    public String getType()
    {
        return type;
    }
    public void setType(String type)
    {
        if(type.equalsIgnoreCase("boolean")){
            this.type = "Boolean";
            this.maxLength = 1;
        }
        else if(type.equalsIgnoreCase("int")){
            this.type = "Integer";
        }
        else if(type.equalsIgnoreCase("long")){
            this.type = "Long";
        }
        else{
            this.type = new StringBuilder().append(Character.toUpperCase(type.charAt(0))).append(type.substring(1).toLowerCase()).toString();
        }
        
        if(this.type.equalsIgnoreCase("longString")){
            this.maxLength = 16777215;
        }
        else if(!this.type.equalsIgnoreCase("String") && !this.type.equalsIgnoreCase("Boolean")){
            this.maxLength = 0;
        }
    }
    public int getMaxLength()
    {
        return maxLength;
    }
    public void setMaxLength(int maxLength)
    {
        this.maxLength = maxLength;
    }
    @Override
    public int hashCode()
    {
        final int prime = 31;
        int result = 1;
        result = prime * result + ((name == null) ? 0 : name.hashCode());
        return result;
    }
    @Override
    public boolean equals(Object obj)
    {
        if (this == obj) return true;
        if (obj == null) return false;
        if (getClass() != obj.getClass()) return false;
        Field other = (Field) obj;
        if (name == null)
        {
            if (other.name != null) return false;
        }
        else if (!name.equals(other.name)) return false;
        return true;
    }
    
}
