package com.resolve.codeGenerate;

import java.util.HashSet;

public class GatewayData
{
    public static enum GATEWAY_TYPE{
        PULL, PUSH, MSG
    }
    String name;
    String packageName;
    GATEWAY_TYPE type = GATEWAY_TYPE.PULL;
    HashSet<Field> filter = new HashSet<Field>();
    HashSet<Field> configs = new HashSet<Field>();
    
    public GatewayData(String name, String packageName, HashSet<Field> filter, HashSet<Field> configs, GATEWAY_TYPE type)
    {
        super();
        this.name = name;
        this.packageName = packageName;
        this.filter = filter;
        this.configs = configs;
        this.type = type;
    }
    public GatewayData()
    {
        super();
    }
    public String getNameLower()
    {
        return name.toLowerCase();
    }
    public String getNameUpper()
    {
        return name.toUpperCase();
    }
    public String getNameUp()
    {
        return new StringBuilder().append(Character.toUpperCase(name.charAt(0))).append(name.substring(1)).toString();
    }
    public String getName()
    {
        return name;
    }
    public void setName(String name)
    {
        this.name = name;
    }
    public String getPackageName()
    {
        return packageName;
    }
    public void setPackageName(String packageName)
    {
        this.packageName = packageName;
    }
    public HashSet<Field> getFilter()
    {
        return filter;
    }
    public void setFilter(HashSet<Field> filter)
    {
        this.filter = filter;
    }
    public HashSet<Field> getConfigs()
    {
        return configs;
    }
    public void setConfigs(HashSet<Field> configs)
    {
        this.configs = configs;
    }
    public GATEWAY_TYPE getType()
    {
        return type;
    }
    public void setType(GATEWAY_TYPE type)
    {
        this.type = type;
    }
    public void setType(String type)
    {
        if(type.toUpperCase().equals(GATEWAY_TYPE.PUSH.toString())){
            this.type = GATEWAY_TYPE.PUSH;
        }
        else if(type.toUpperCase().equals(GATEWAY_TYPE.PULL.toString())){
            this.type = GATEWAY_TYPE.PULL;
        }else{
            this.type = GATEWAY_TYPE.MSG;
        }
     }
    
}
