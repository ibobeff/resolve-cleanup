package com.resolve.codeGenerate;

import java.io.File;
import java.io.FileInputStream;
import java.util.List;
import com.resolve.util.Properties;
/**
 * Test class for developer testing the new SDK
 *
 *
 */
public class CodeGeneFactory extends ConfigData
{

    public static void main(String[] args)
    {
        
        //This should be provided by the command line
        String productName = "Generic";
        String gwType = "MSG";
     // TODO Auto-generated method stub
            
        List<GatewayData> gatewayData = initDataFromProp(loadProp(gwType+ "_blueprint.properties"));
//        List<String> genProjProps = initDataFromProp(loadProp("sdk.properties"));
        File dir = new File("C:\\project\\resolve\\src\\templateprojectredesigned");
        GenerateSDK sdkGen = new GenerateSDK();
        try {
            for(GatewayData gdata : gatewayData){
                gdata.setName(productName);
                gdata.setType(gwType);
                sdkGen.generateSourceFile(dir, gdata);
            }
        }catch(Exception ex) {
            ex.printStackTrace();
        }
    }
    public static Properties loadProp(String file){
        Properties blueprintProperties = new Properties();
        try
        {
//            File blueprintFile = FileUtils.getFile(file);
            blueprintProperties.load(CodeGeneFactory.class.getClassLoader().getResourceAsStream(file));
//            if (blueprintFile.exists()) {
//                    FileInputStream fis = new FileInputStream(file);
//                    blueprintProperties.load(fis);
//                    fis.close();
                    return blueprintProperties;
//                }
//            else
//                {
//                    System.out.println("blueprint.properties file not found, please put it into ");
//                    return null;
//                }
        }
        catch(Exception e)
        {
            e.printStackTrace();
        }
        return blueprintProperties;
    }
}
