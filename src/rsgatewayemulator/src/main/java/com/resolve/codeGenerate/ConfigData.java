package com.resolve.codeGenerate;

import japa.parser.ParseException;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.io.*;


import com.resolve.util.FileUtils;
import com.resolve.util.Properties;

import com.resolve.codeGenerate.Field;

public class ConfigData
{
    static String[] defaultField = {"active", "failover", "heartbeat", "interval", "primary", "queue", "secondary", "uppercase", "worker", "menutitle", "mock"};
    static HashSet<String> defaultFieldSet = new HashSet<String>();
    static {
        for(String field : defaultField){
            defaultFieldSet.add(field);
        }
    }
    public static Properties loadProp(String file){
        Properties blueprintProperties = new Properties();
        try
        {
            File blueprintFile = FileUtils.getFile(file);
            if (blueprintFile.exists()) {
                    FileInputStream fis = new FileInputStream(blueprintFile);
                    blueprintProperties.load(fis);
                    fis.close();
                    return blueprintProperties;
                }
            else
                {
                    System.out.println("blueprint.properties file not found, please put it into ");
                    return null;
                }
        }
        catch(Exception e)
        {
            e.printStackTrace();
        }
        return blueprintProperties;
    }
    
    public static Properties loadPropWoUtil(String file){
        Properties blueprintProperties = new Properties();
        try
        {
            File blueprintFile = new File("C:\\project\\resolve\\src\\" + file);
//            if (blueprintFile.exists()) {
                    FileInputStream fis = new FileInputStream(blueprintFile);
                    blueprintProperties.load(fis);
                    fis.close();
                    return blueprintProperties;
//                }
//            else
//                {
//                    System.out.println("blueprint.properties file not found, please put it into ");
//                    return null;
//                }
        }
        catch(Exception e)
        {
            e.printStackTrace();
        }
        return blueprintProperties;
    }
    
    public static List<GatewayData> initDataFromProp(Properties blueprintProperties){
        if(blueprintProperties==null) return null; 
        ArrayList<GatewayData> data = new ArrayList<GatewayData>();
        ArrayList<String> implprefixes = new ArrayList<String>();
      
        for (Object key : blueprintProperties.keySet())
        {
            String strKey = key.toString();
            if(strKey.endsWith(".implprefix") && strKey.startsWith("rsremote.receive.")){
                implprefixes.add((String) blueprintProperties.get(key));
            }
        }
        for(String implprefix : implprefixes){
            if(implprefix.length()>0 && implprefix.charAt(0)>='A' && implprefix.charAt(0)<='Z'){
                data.add(initDataFromProp(blueprintProperties, implprefix));
            }
            else{
                System.out.println("implprefix " + implprefix + " should start with upper case character");
            }
            
        }
        return data;
    }
    
    public static GatewayData initDataFromProp(Properties blueprintProperties, String implprefix){
        
        GatewayData data = new GatewayData();
        HashMap<String, String> dataType = new HashMap<String, String>();
        HashSet<String> dataName = new HashSet<String>();
        
        HashMap<String, String> fieldType = new HashMap<String, String>();
        HashMap<String, String> fieldName = new HashMap<String, String>();
        HashMap<String, Integer> maxLength = new HashMap<String, Integer>();
        
        
        
        for (Object key : blueprintProperties.keySet())
        {
            String strKey = key.toString();
           
            if(strKey.startsWith("rsremote.receive."+implprefix.toLowerCase()+".")){
                String keyName = strKey.substring(implprefix.length()+18);
                if(keyName.startsWith("sdkfield")){
                    String temp = keyName.substring(8);
                    if(!temp.startsWith(".")){
                        if(temp.substring(temp.indexOf('.')+1).equals("name")){
            //                fieldName.put(temp.substring(0, temp.indexOf('.')), ((String) blueprintProperties.get(key)).substring(1));
                            String nameLow = (String) blueprintProperties.get(key);
                            nameLow = (new StringBuilder().append(Character.toLowerCase(nameLow.charAt(0))).append(nameLow.substring(1))).toString();
                            fieldName.put(temp.substring(0, temp.indexOf('.')), nameLow);
                        }
                        else if(temp.substring(temp.indexOf('.')+1).equals("type")){
                            if(((String) blueprintProperties.get(key)).equalsIgnoreCase("textarea")){
                                fieldType.put(temp.substring(0, temp.indexOf('.')), "longString");
                            }
                        }
                        else if(temp.substring(temp.indexOf('.')+1).equals("sdkdatatype")){
                            fieldType.put(temp.substring(0, temp.indexOf('.')), (String) blueprintProperties.get(key));
                        }
                        else if(temp.substring(temp.indexOf('.')+1).equals("maxlength")){
                            maxLength.put(temp.substring(0, temp.indexOf('.')), new Integer((String) blueprintProperties.get(key)));
                        }
                    }
                    
                }
                else if(keyName.equals("implprefix")){
                    data.setName(blueprintProperties.get(strKey));
                }
                else if(keyName.equals("type")){
                    data.setType((String)blueprintProperties.get(strKey));
                }
                else if(keyName.equals("package")){
                    data.setPackageName(blueprintProperties.get(strKey));
                }
                else if(!defaultFieldSet.contains(keyName)){
                    if(keyName.startsWith("sdkdatatype")){
                        dataType.put(keyName.substring(12), blueprintProperties.get(strKey));
                    }
                    else{
                        dataName.add(keyName);
                    }
                    
                }
            }
        }
        for(String key : dataName){
            if(dataType.containsKey(key)){
                data.getConfigs().add(new Field(key, dataType.get(key)));
            }
            else{
                data.getConfigs().add(new Field(key)); 
            }
        }
        
        for(String key : fieldName.keySet()){
            Field temp = new Field(fieldName.get(key));
            if(fieldType.containsKey(key)){
                temp.setType(fieldType.get(key));
            }
            if(maxLength.containsKey(key)){
                temp.setMaxLength(maxLength.get(key).intValue());
            }
            data.getFilter().add(temp);
        }
        return data;
    }
    
    public static void main(String[] args) throws IOException, ParseException{
        
        List<GatewayData> data = initDataFromProp(loadProp("blueprint.properties"));
        
        File dir = new File("C:\\project\\resolve\\src\\templateProject");
        
        for(GatewayData d : data){
            d.setName("Resilient");
            d.setType("PULL");
            GenerateBasedTemp.generateSourceCode(dir, d);
        }
        
    }
}
