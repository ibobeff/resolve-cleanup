package com.resolve.util;

import java.io.File;
import java.io.IOException;
import java.nio.charset.Charset;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.Map;
import java.util.Set;

import org.apache.log4j.Logger;


public class EmulatorUtil
{
    private static Logger log =  Logger.getLogger(EmulatorUtil.class);
    
    public EmulatorUtil() {
        
    }
    /**
     * <p> 
     *          <br> This util method accepts a file and a Map. The Map has the </br>
     *          <br> Key="Value to be Replaced" Value="Replace the Key with this value" </br>
     *          <br> The Method reads the file and finds the KEYS and replace them with </br>
     *          <br> value. Importantly keeping the file data formatting intact </br> 
     *          
     * </p>
     * @param newFile
     * @param replaceMap
     * @param data
     */
    public void replaceFileData(File newFile, Map<String, String> replaceMap) {
        Path path = newFile.toPath();
        Charset charset = StandardCharsets.UTF_8;
        try {
            String content = new String(Files.readAllBytes(path), charset);
            
            Set<String> keySet = replaceMap.keySet();
            for(String key: keySet) {
                replaceMap.put(key, replaceMap.get(key));
                content = content.replaceAll(key, replaceMap.get(key));
            }
            Files.write(path, content.getBytes(charset));
        }catch(IOException ex) {
            log.error("Error when reading class files in side "+ this.getClass().getName());
            ex.printStackTrace();
        }
    }
    
   

}
