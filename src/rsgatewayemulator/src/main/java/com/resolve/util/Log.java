/******************************************************************************
* (C) Copyright 2014
*
* Resolve Systems, LLC
*
* All rights reserved
* This software is distributed under license and may not be used, copied,
* modified, or distributed without the express written permission of
* Resolve Systems, LLC.
*
******************************************************************************/

package com.resolve.util;

import java.io.File;
import java.io.PrintWriter;
import java.io.StringWriter;
import java.util.Date;
//import java.util.HashMap;
//import java.util.Map;

import org.apache.log4j.BasicConfigurator;
import org.apache.log4j.Level;
import org.apache.log4j.Logger;
import org.apache.log4j.PropertyConfigurator;
import org.owasp.esapi.reference.Log4JLogger;

import com.resolve.esb.ESB;

public class Log
{
    public static Logger log;
    public static Logger auth;
    public static ESB esb;
    public static String component;

    private static ThreadLocal<Integer> hereCount = new ThreadLocal<Integer>();
    private static ThreadLocal<Long> hereTime = new ThreadLocal<Long>();

    private static ThreadLocal<String> prevThreadName = new ThreadLocal<String>();
    private static ThreadLocal<Long> startTime = new ThreadLocal<Long>();

    public static void init()
    {
        init("");
    } // init

    private static Logger getLogger(String name)
    {
        return Log4JLogger.getLogger(name);
    }

    public static void init(String name)
    {
        try
        {
            log = getLogger(name); // .getRootLogger();
            if (log == null)
            {
                throw new Exception("ERROR: Failed to initialize logging");
            }

            BasicConfigurator.configure();
            log.setLevel(Level.INFO);
        }
        catch (Exception e)
        {
            System.out.println(e.getMessage());
        }
    } // init

    public static void init(String configFile, String name) throws Exception
    {
        try
        {
            // remapping file
            String remapFile;
            int pos = configFile.lastIndexOf('.');
            if (pos > 0)
            {
                remapFile = configFile.substring(0, pos) + ".map";
            }
            else
            {
                remapFile = configFile + ".map";
            }

            // init logger
            log = getLogger("com.resolve." + name);
            if (log != null)
            {
                File file = FileUtils.getFile(configFile);
                if (file.exists() == false)
                {
                    throw new Exception("Unable to find Log configuration file: " + file.getAbsolutePath());
                }

                // configure logger
                PropertyConfigurator.configure(configFile);
            }

            // initRemapFile
            LogFilter.init(remapFile);
        }
        catch (Exception e)
        {
            System.out.println(e.getMessage());
        }
    } // init

    public static void initAuth() throws Exception
    {
        try
        {
            // check that log has been init
            if (log == null)
            {
                throw new Exception("Normal LOG must be initialized first");
            }
            else
            {
                // init AUTH logger
                auth = getLogger("com.resolve.auth");
            }
        }
        catch (Exception e)
        {
            System.out.println(e.getMessage());
        }
    } // initAuth

    public static void initESB(ESB esb) throws Exception
    {
        Log.esb = esb;
    } // initESB

    public static void initComponent(String component) throws Exception
    {
        Log.component = component;
    } // initComponent

    public static void setProcessId(String processId)
    {
        if (!StringUtils.isEmpty(processId))
        {
            String name = Thread.currentThread().getName();
            String[] names = name.split("-");
            if (names.length >= 5)
            {
                name = names[0] + "-" + names[1] + "-" + names[2] + "-" + names[3];
            }

            prevThreadName.set(name);
            Thread.currentThread().setName(name + "-" + processId);
            Log.log.debug("LOG rename threadid: " + Thread.currentThread().getName() + " prevName: " + prevThreadName.get());
        }
    } // setProcessId

    public static void clearProcessId()
    {
        String prevName = prevThreadName.get();
        if (!StringUtils.isEmpty(prevName))
        {
            Log.log.debug("LOG clear threadid: " + prevName + " prevName: " + Thread.currentThread().getName());

            Thread.currentThread().setName(prevName);

            prevThreadName.set("");
        }
    } // clearProcessId

    public static void setLevel(String level)
    {
        log.setLevel(Level.toLevel(level));
    } // setlevel

    public static void setDebug()
    {
        log.setLevel(Level.DEBUG);
    } // setDebug

    public static void hereReset()
    {
        hereCount.set(0);
        hereTime.set(System.currentTimeMillis());
    } // hereReset

    public static void here()
    {
        if (hereTime.get() == null)
        {
            hereReset();
        }

        long temp = System.currentTimeMillis();
        long diffTime = temp - hereTime.get().longValue();
        if (hereCount.get().intValue() > 0)
        {
            long hours = diffTime / 3600000;
            diffTime = diffTime - (hours * 3600000);
            long minutes = diffTime / 60000;
            diffTime = diffTime - (minutes * 60000);
            long seconds = diffTime / 1000;
            diffTime = diffTime - (seconds * 1000);
            long millis = diffTime;
            log.trace("HERE" + hereCount.get() + " elapsed time: " + hours + "hrs " + minutes + "mins " + seconds + "secs " + millis + "msecs");
        }
        else
        {
            log.trace("HERE" + hereCount.get());
        }
        hereCount.set(hereCount.get().intValue() + 1);
        hereTime.set(temp);
    } // here

    public static void loc()
    {
        loc(null, 0);
    } // loc

    public static void loc(String prefix, int prevStackLevel)
    {
        if (prefix == null)
        {
            prefix = "LOCATION:";
        }
        StackTraceElement stack = new Throwable().getStackTrace()[prevStackLevel + 1];
        Log.log.trace(prefix + " " + stack.getClassName() + "#" + stack.getMethodName());
    } // loc

    public static boolean loadMapping()
    {
        return LogFilter.load();
    } // loadMapping

    public static String getStrackTrace(Throwable e)
    {
        String result = "";

        StringWriter sw = new StringWriter();
        PrintWriter pw = new PrintWriter(sw);

        e.printStackTrace(pw);
        result = sw.toString();

        return result;
    } // getStackTrace

    public static long reset()
    {
        long result = System.currentTimeMillis();

        startTime.set(new Long(result));

        return result;
    } // reset

    public static long start()
    {
        return start("Time start:");
    } // start

    public static long start(String msg)
    {
        long result = reset();

        log.trace(msg + " " + new Date(result) + "(" + result + ")");

        return result;
    } // start

    public static long start(String msg, String level)
    {
        long result = reset();

        if (level.equalsIgnoreCase("trace"))
        {
            log.trace(msg + " " + new Date(result) + "(" + result + ")");
        }

        return result;
    } // start

    public static long start(String msg, Level level)
    {
        long result = reset();

        if (level == Level.TRACE)
        {
            log.trace(msg + " " + new Date(result) + "(" + result + ")");
        }

        return result;
    } // start

    public static long duration()
    {
        return duration("Time duration:");
    } // duration

    public static long duration(String msg, String level)
    {
        long result = System.currentTimeMillis() - startTime.get().longValue();
        if (level.equalsIgnoreCase("debug"))
        {
            log.debug(msg + " " + result + "msec");
        }
        else
        {
            log.trace(msg + " " + result + "msec");
        }

        return result;
    } // duration

    public static long duration(String msg, Level level)
    {
        long result = System.currentTimeMillis() - startTime.get().longValue();
        if (level == Level.DEBUG)
        {
            log.debug(msg + " " + result + "msec");
        }
        else
        {
            log.trace(msg + " " + result + "msec");
        }

        return result;
    } // duration

    public static long duration(String msg)
    {
        long result = System.currentTimeMillis() - startTime.get().longValue();
        log.trace(msg + " " + result);

        return result;
    } // duration

    public static void alert(ERR errorCode)
    {
        alert(errorCode, null, null);
    } // alert

    public static void alert(ERR errorCode, String message)
    {
        alert(errorCode, message, null);
    } // alert

    public static void alert(ERR errorCode, Throwable t)
    {
        alert(errorCode, null, t);
    } // alert

    /**
     * Log a FATAL log message and sends a JMS request to the RSMGMT (ALERT
     * queue) to post a notification to the event management system
     *
     * @param type
     *            - type of problem. Use (or add to)
     *            Constants.ALERT_TYPE_DATABASE, etc
     * @param message
     *            - error message
     * @param t
     *            - exceptiona throwable to retrieve stack trace
     *
     * @throws Exception
     */
    public static void alert(ERR errorCode, String detail, Throwable t)
    {
        // init message
        String message = errorCode.getMessage();
        if (detail != null)
        {
            message += " - " + detail;
        }

        // init exception
        if (t == null)
        {
            t = new Exception(message);
        }

        // send fatal message to RSMgmt
        if (esb == null)
        {
            Log.log.fatal("ESB not initialized", new Exception());
        }
        else
        {
            message += "\n";
            message += StringUtils.getStackTrace(t);
            message += "\n";

            alert(errorCode.getCode(), message);
        }
    } // alert

    public static void alert(String code, String message)
    {
        alert(code, message, "CRITICAL");
    } // alert

    public static void alert(String code, String message, String severity)
    {
        alert(code, message, severity, null, null);
    } // alert
    
    public static void alert(String code, String message, String severity, String src, String action)
    {
        alert(code, message, severity, null, null, src, action);
    } // alert
    
    //@SuppressWarnings({ "rawtypes", "unchecked" })
    public static void alert(String code, String detail, String severity, String subject, String worksheetId, String src, String action)
    {
        String message = detail;
        
        if (StringUtils.isNotEmpty(message))
        {
            /*
            Map params = new HashMap();
            params.put("SEVERITY", severity);
            params.put("COMPONENT", StringUtils.isBlank(src)?component:src);
            params.put("CODE", code);
            params.put("MESSAGE", message);
            
            if (StringUtils.isNotBlank(worksheetId))
            {
                params.put("WORKSHEETID", worksheetId);
            }
            
            if (StringUtils.isNotBlank(action))
            {
                params.put("ACTION", action);
            }

            // notify rsmgmt
            esb.sendInternalMessage(Constants.ESB_NAME_ALERT, "MAlert.alert", params);
            */
            // log fatal message
            Exception t = new Exception(message);
            log.fatal(message, t);
            System.out.println(message);
            t.printStackTrace();
            
            /*
            try
            {
                // post to Admin Group team
                if (StringUtils.isBlank(subject))
                {
                    int pos = message.indexOf('\n');
                    if (pos > 0)
                    {
                        subject = message.substring(0, pos);
                    }
                    else
                    {
                        subject = message;
                    }
                }
                if (subject.length() > 60)
                {
                    subject = subject.substring(0, 60);
                }

                String header = "Alert Notification from "+MainBase.main.release.getType()+" ("+MainBase.main.configId.getGuid()+")";
                message = header + "\n\n" + message;
                NotificationAPI.sendToSocialTeam(Constants.SOCIAL_TEAM_ADMIN_GROUP, subject, message, "system", false);
            }
            catch (Exception e)
            {
                Log.log.error(e.getMessage(), e);
            }
            */
        }
    }
} // Log
