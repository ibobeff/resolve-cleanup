/******************************************************************************
* (C) Copyright 2014
*
* Resolve Systems, LLC
*
* All rights reserved
* This software is distributed under license and may not be used, copied,
* modified, or distributed without the express written permission of
* Resolve Systems, LLC.
*
******************************************************************************/

package com.resolve.gateway;

import com.resolve.rsbase.SessionObjectInterface;

public interface Gateway
{
    /**
     * This method provides the code used in license (e.g., EMAIL, REMEDYX). Resolve license
     * must have the same string to start this gateway.
     *
     * @return
     */
    String getLicenseCode();

    void init() throws Exception;

    void reinitialize();

    void start();

    void stop();

    boolean isActive();

    void setActive(boolean active);

    void deactivate();

    String getQueueName();

    void registerSessionObject(SessionObjectInterface sessionObject);
}
