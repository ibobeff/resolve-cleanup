/******************************************************************************
 * (C) Copyright 2014
 * 
 * Resolve Systems, LLC
 * 
 * All rights reserved This software is distributed under license and may not be
 * used, copied, modified, or distributed without the express written permission
 * of Resolve Systems, LLC.
 * 
 ******************************************************************************/

package com.resolve.gateway;

import java.net.MalformedURLException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Queue;
import java.util.Set;
import java.util.TreeSet;
import java.util.concurrent.BlockingQueue;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.CopyOnWriteArrayList;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.LinkedBlockingQueue;
import java.util.concurrent.ThreadPoolExecutor;
import java.util.concurrent.TimeUnit;
import java.util.regex.Pattern;

import javax.mail.Message;

import com.resolve.esb.MMsgHeader;
import com.resolve.rsbase.MainBase;
import com.resolve.groovy.script.GroovyScript;
import com.resolve.query.mapper.ResolveMapper;
import com.resolve.rsgatewayemulator.ExecuteUnitTest;
import com.resolve.rsremote.ConfigReceiveGateway;
import com.resolve.util.ConfigMap;
import com.resolve.util.Constants;
import com.resolve.util.GatewayEvent;
import com.resolve.util.Log;
import com.resolve.util.ObjectProperties;
import com.resolve.util.StringUtils;
import com.resolve.util.queue.AbstractQueueListener;
import com.resolve.util.queue.QueueListener;
import com.resolve.util.queue.QueueManager;

import groovy.lang.Binding;

/**
 * This class provides a partial implementation for clustered gateway sub
 * classes implemting most common functionalities of the gateways. Most of the
 * gateways are part of a cluster which provide the High Availability (HA)
 * functionality. This class provides the necessary functionalities for a gatway
 * to be a member of a cluster.
 */
public abstract class BaseClusteredGateway extends BaseGateway implements ClusteredGateway
{
    protected static final String QUEUE = "QUEUE";

    public static final String GATEWAYTYPE_PRIMARY = "PRIMARY";
    public static final String GATEWAYTYPE_SECONDARY = "SECONDARY";
    public static final String GATEWAYTYPE_WORKER = "WORKER";
    //public static final int GATEWAY_PRIMARY_EXECUTOR_QUEUE_SIZE = 1000;
    public static final String FILTER_ID_NAME = "FILTER_ID";
    protected Pattern VAR_REGEX_GATEWAY_VARIABLE = Pattern.compile(Constants.VAR_REGEX_GATEWAY_VARIABLE);

//    protected final MMsgHeader esbMessageHeader = new MMsgHeader();
//    protected final MMsgHeader heartbeatHeader = new MMsgHeader();

    // executor service to add data into primary data queue
    //protected ExecutorService executor = Executors.newFixedThreadPool(SystemUtil.getCPUCount());
    protected ExecutorService executor = null/*new ThreadPoolExecutor(SystemUtil.getCPUCount(), SystemUtil.getCPUCount(), 100, TimeUnit.SECONDS, new ArrayBlockingQueue<Runnable>(GATEWAY_PRIMARY_EXECUTOR_QUEUE_SIZE))*/;

    protected ConcurrentHashMap<String, Filter> filters = new ConcurrentHashMap<String, Filter>();

    protected volatile List<Filter> orderedFilters = new CopyOnWriteArrayList<Filter>();
    protected volatile Map<String, Filter> orderedFiltersMapById = new ConcurrentHashMap<String, Filter>();

    private NameProperties nameProperties = new NameProperties();
    private Set<RoutingSchema> routingSchemas = new TreeSet<RoutingSchema>();

    private volatile boolean primary = true;
    private volatile boolean secondary = true;
    private volatile boolean worker = false;
    private boolean uppercase = true;
    private boolean originalPrimary = true;
    protected long heartbeatInterval = 20000;
    protected long failover = 60000;
    protected long lastHeartbeat;
    protected String topicName;
    protected String workerQueueName;
    private Map<String, String> message = new HashMap<String, String>();

    // just for analytics purpose
    private static long dataCounter;

    // when this gate starts
    protected long startTime;

    // initialized during gateway instance initialize()
    // Provides the location where configurations like name properties are
    // stored (e.g., /config/netcool/);
    protected String gatewayConfigDir;

    protected volatile boolean isSyncDone = false;

    protected int interval = 10000;
    protected int minimumInterval = 10000;

    private int TASK_TIMEOUT = 60 * 1000; // 60 seconds

    protected Thread thread;

    // used by primary to gather data and distribute to workers.
    protected Queue<Map<String, String>> primaryDataQueue = new LinkedList<Map<String, String>>();
    protected QueueListener<Map<String, String>> primaryDataQueueListener = new PrimaryDataQueueListener();

    // exclusively used by the worker gateways
    protected Queue<GatewayEvent<String, Object>> workerDataQueue = new LinkedList<GatewayEvent<String, Object>>();
    protected QueueListener<GatewayEvent<String, Object>> workerDataQueueListener = new WorkerDataQueueListener();

    // Set of undeployed filters to ignore data received while filter was getting undeployed
    
    protected Set<String> undeployedFilterIds = new HashSet<String>();
    
    private int gatewayPrimaryExecutorQueueSize = ConfigReceiveGateway.MIN_PRIMARY_DATA_QUEUE_EXECUTOR_QUEUE_SIZE;
    
    public boolean noFilter = true;
 
    public boolean isNoFilter()
    {
        return noFilter;
    }

    public void setNoFilter(boolean noFilter)
    {
        this.noFilter = noFilter;
    }
    
    /**
     * Implemented method in the sub class will add any gateway specific
     * key/value pairs in the event map.
     * 
     * @param event
     * @param result
     */
    abstract protected void addAdditionalValues(Map<String, Object> event, Map<String, String> result);

    /**
     * Overwrite method in the sub class to add any gateway specific
     * key/value pairs in the event map based on the filter object type
     *
     * @param event
     * @param result
     * @param filter
     */
    protected void addAdditionalValues(Map<String, Object> event, Map<String, String> result, Filter filter)
    {
        addAdditionalValues(event, result);
    }
    
    protected BaseClusteredGateway(ConfigMap config)
    {
        super(config);
        try
        {
            ConfigReceiveGateway gatewayConfig = (ConfigReceiveGateway) configurations;
            
            gatewayPrimaryExecutorQueueSize = gatewayConfig.getPrimaryDataQueueExecutorQueueSize();
            
            if (gatewayConfig.isActive() && (gatewayConfig.isPrimary() || gatewayConfig.isSecondary()))
            {
                executor = new ThreadPoolExecutor(gatewayConfig.getPrimaryDataQueueExecutorCorePoolSize(), 
                                                  gatewayConfig.getPrimaryDataQueueExecutorMaxPoolSize(), 
                                                  gatewayConfig.getPrimaryDataQueueExecutorKeepAliveTimeInSec(), 
                                                  TimeUnit.SECONDS, 
                                                  new LinkedBlockingQueue<Runnable>(gatewayPrimaryExecutorQueueSize));
            }
            
            this.thread = new Thread(this);
        }
        catch (Exception e)
        {
            Log.log.warn(e.getMessage(), e);
        }
    }

    @Override
    public boolean isPrimary()
    {
        return primary;
    }

    @Override
    public void setPrimary(boolean primary)
    {
        this.primary = primary;
    }

    @Override
    public boolean isOriginalPrimary()
    {
        return originalPrimary;
    }

    @Override
    public boolean isSecondary()
    {
        return secondary;
    }

    @Override
    public boolean isWorker()
    {
        return worker;
    }

    public boolean isUppercase()
    {
        return uppercase;
    }

    public void setUppercase(boolean uppercase)
    {
        this.uppercase = uppercase;
    }

    @Override
    public void setLastHeartbeat()
    {
        this.lastHeartbeat = System.currentTimeMillis();
    } // setLastHeartbeat

    @Override
    public Map<String, String> getMessage()
    {
        return message;
    }

    @Override
    public void setMessage(Map<String, String> message)
    {
        this.message = message;
    }

    @Override
    public GatewayHeartbeat getHeartbeatThread()
    {
        return null;
    }

    @Override
    public void setHeartbeatThread(GatewayHeartbeat heartbeatThread)
    {
    }

    @Override
    public long getHeartbeatInterval()
    {
        return heartbeatInterval;
    }

    @Override
    public MMsgHeader getHeartbeatHeader()
    {
        return null;
    }

    @Override
    public long getLastHeartbeat()
    {
        return lastHeartbeat;
    }

    @Override
    public void setLastHeartbeat(long lastHeartbeat)
    {
        this.lastHeartbeat = lastHeartbeat;
    }

    @Override
    public long getFailoverInterval()
    {
        return this.failover;
    }

    /**
     * Verifies if the System clock is set backward. In that case we need to
     * make sure that the heart beat counter is reset as well. Otherwise it will
     * fall behind and not send hearbeat until system time catch up to it..
     */
    @Override
    public void checkSystemClockReset()
    {
        if (lastHeartbeat > System.currentTimeMillis())
        {
            setLastHeartbeat();
            Log.log.debug("HEARTBEAT RESET: Due to system clock set backward, lastHeartBeat is reset to " + lastHeartbeat);
        }
    }

    @Override
    protected String getInstanceType()
    {
        if (isPrimary())
        {
            return GATEWAYTYPE_PRIMARY;
        }
        else if (isSecondary())
        {
            return GATEWAYTYPE_SECONDARY;
        }
        else if (isWorker())
        {
            return GATEWAYTYPE_WORKER;
        }
        else
        {
            return null;
        }
    }

    @Override
    public void init() throws Exception
    {
        try
        {
            super.init();

            // init
            this.active = false;
            this.running = true;

            topicName = getQueueName() + "_TOPIC";

            startTime = System.currentTimeMillis();

            primary = true;
            
            primaryDataQueue = QueueManager.getInstance().getResolveConcurrentLinkedQueue(primaryDataQueueListener);

            if (primary)
            {
                setActive(true);
            }
        }
        catch (Exception e)
        {
            Log.log.warn(e.getMessage(), e);
            throw e;
        }
    }

    @Override
    public void reinitialize()
    {
        super.reinitialize();

        setActive(true);
        setPrimary(true);

        // start the data reader
        this.running = true;
        thread = new Thread(this);
        thread.start();
    }

    public void start()
    {
        // initialize resources
        try
        {
            init();

            if (isPrimary())
            {
                // start the gateway
                thread = new Thread(this);
                thread.start();
            }
        }
        catch (Exception e)
        {
            Log.log.error(e.getMessage(), e);
        }
    } // start

    public void stop()
    {
        Log.log.warn("Stopping the " + getQueueName() + " gateway.");
        
        active = false;
        running = false;
        try
        {
            thread = null;
        }
        catch (Exception e)
        {
            Log.log.warn("ERROR: " + e.getMessage(), e);
        }
        
        System.out.println("\nExiting Emulator...\n");
        
        executor.shutdownNow();
    }

    @Override
    public void deactivate()
    {
        super.deactivate();
        setActive(false);
        setPrimary(false);
        this.running = false;
        // stop the thread
        thread = null;
    }

    public synchronized boolean isSyncDone()
    {
        return isSyncDone;
    }

    public synchronized void setSyncDone(boolean isSyncDone)
    {
        this.isSyncDone = isSyncDone;
    }

    @Override
    public Map<String, String> getSyncRequestParameters()
    {
        Map<String, String> params = new HashMap<String, String>();
        params.put("GATEWAY_NAME", getQueueName());
        params.put("QUEUE_NAME", MainBase.main.configId.getGuid());
        params.put("MESSAGE_HANDLER_NAME", "com.resolve.gateway." + getMessageHandlerName());
        return params;
    }
    
    protected void sendSyncRequest()
    {
    }
    
    protected void groovyScriptBindingCleanup(Binding binding)
    {
    }
    
    protected void addAdditionalBindings(String script, Binding binding)
    {
    }
    
    public String getQueueName()
    {
        return "DUMMY";
    }

    /**
     * This method is implemented by subclasses to update various custom fields
     * in the target server. For example in Netcool or HPOM gateway it will
     * update RESOLVE_STATUS and RESOLVE_RUNBOOKID.
     * 
     * @param filter
     * @param content
     */
    @SuppressWarnings("rawtypes")
    public void updateServerData(Filter filter, Map content)
    {
        // Not implemented here, normaly subclasses like HPOM, Netcool etc.
        // should implement this method to update the RESOLVE_STATUS,
        // RESOLVE_RUNBOOKID field etc.
    }

    /**
     * This method decides if the filter should execute or not. It's based on
     * the interval for the filter and the last execution time.
     * 
     * @param filter
     * @param startTime
     * @return
     */
    protected boolean shouldFilterExecute(Filter filter, long startTime)
    {
        if(noFilter) return false;
        boolean result = false;
       
        // if this is primary then only we need to execute filters.
        if (isPrimary())
        {
            if (filter != null && filter.isActive())
            {
                if (filter.getLastExecutionTime() == 0)
                {
                    result = true;
                }
                else
                {
                    result = startTime >= (filter.getLastExecutionTime() + filter.getInterval() * 1000);
                }
                if (result)
                {
                    Log.log.debug(getQueueName() + " gateway: filter " + filter.getId() + " is ready to execute...");
                    // register this startTime as last execution time
                    filter.setLastExecutionTime(startTime);
                }
            }
            if (!result)
            {
                try
                {
                    Log.log.trace("Sleeping for " + minimumInterval);
                    Thread.sleep(1000L);
                    // This needs to be revisited. With 1 second sleep we have
                    // not see any CPU usage spike.
                    /*
                     * long min = minimumInterval; for(Filter filter1 :
                     * getFilters().values()) { long newInterval =
                     * System.currentTimeMillis() -
                     * filter1.getLastExecutionTime() + filter1.getInterval();
                     * if(min > newInterval) { min = newInterval; } }
                     */
                }
                catch (InterruptedException e)
                {
                    // nothing to do
                }
            }
        }
        return result;
    }

    /**
     * Processes data received from the target system. It checks the data with
     * any interested filter and process the filter.
     * 
     * @param data
     *            could be a {@link Message} or an XMPP message etc.
     * @throws Exception
     */
    protected void processData(Object data) throws Exception
    {
        throw new RuntimeException("If a subclass wants to use this method, it must be implemented by the that Subclass.");
    }

    @Override
    public void setFilter(Map<String, Object> params)
    {
        Filter filter = getFilter(params);

        // remove existing filter
        removeFilter(filter.getId());

        // update filters
        filters.put(filter.getId(), filter);

        int filterInterval = filter.getInterval() * 1000;
        if (filterInterval < minimumInterval)
        {
            minimumInterval = filterInterval;
        }
        // create new orderedFilters
        synchronized (getClass())
        {
            List<Filter> syncedOrderededFilters = new ArrayList<Filter>(filters.values());
            Collections.sort(syncedOrderededFilters);
            
            orderedFilters = new CopyOnWriteArrayList<Filter>(syncedOrderededFilters);
            //Collections.sort(orderedFilters);
            
            orderedFiltersMapById.clear();
            
            for (Filter ofilter : orderedFilters)
            {
                orderedFiltersMapById.put(ofilter.getId(), ofilter);
            }
        }
    }

    /**
     * Adds event related items in the params {@link Map} by parsing the
     * incoming message. This method is mainly used by the gateways which
     * receives messages (e.g., XMPP, Email etc.).
     * 
     * @param message
     *            in the form of:
     * 
     *            Persistent Event: 1) EVENT= My Event EVENTREF=Event reference
     *            WIKI=ns.wiki name PROBLEMID=LOOKUP NUMBER=PRB1234-2
     *            PERSISTENT=true; 2) EVENT= My Event EVENTREF=Event reference
     *            WIKI=ns.wiki name PROBLEMID=LOOKUP ALERTID=Worksheet Alert ID
     *            PERSISTENT=true; 3) EVENT= My Event EVENTREF=Event reference
     *            WIKI=ns.wiki name PROBLEMID=LOOKUP REFERENCE=Worksheet
     *            Reference PERSISTENT=true;
     * 
     *            In-memory Event: 4) EVENT= My Event EVENTREF=Event reference
     *            PROBLEMID=LOOKUP NUMBER=PRB1234-1;
     * 
     * @param actual
     *            parameters added for the system to execute the event are:
     *            EVENT => EVENT_EVENTID EVENTREF=>EVENT_REFERENCE
     *            WIKI=>WIKI=ns.wiki PROBLEMID=>PROBLEMID
     *            PERSISTENT=>EVENT_START NUMBER=>NUMBER ALERTID=>ALERTID
     *            REFERENCE=>REFERENCE
     */
    protected void addEvent(String message, Map<String, String> params)
    {
        try
        {
            ResolveMapper resolveMapper = new ResolveMapper();
            HashMap<String, String> result = resolveMapper.mapQuery(message);

            // if the message has event then we'll check for whether it's
            // persistent or not in the message itself
            // because if the event is passed we'll ignore the event configured
            // in the filter configuration
            String eventId = result.get("EVENT");
            if (StringUtils.isNotBlank(eventId))
            {
                params.put(Constants.EVENT_EVENTID, eventId);
            }

            params.put(Constants.EVENT_REFERENCE, result.get("EVENTREF"));
            params.put(Constants.EXECUTE_WIKI, result.get("WIKI"));

            params.put(Constants.EXECUTE_PROBLEMID, result.get("PROBLEMID"));
            params.put(Constants.WORKSHEET_ALERTID, result.get("ALERTID"));
            params.put(Constants.WORKSHEET_NUMBER, result.get("NUMBER"));
            params.put(Constants.WORKSHEET_REFERENCE, result.get("REFERENCE"));
        }
        catch (Exception e)
        {
            Log.log.info("Error parsing the message, might be something Resolve does not understand, it's fine!");
            Log.log.info("Actual error: " + e.getMessage());
        }
    }

    /**
     * Listener for the primary data queue. This prevents us from reading data
     * through a loop because it's on demand.
     */
    private class PrimaryDataQueueListener extends AbstractQueueListener<Map<String, String>>
    {
        public boolean process(Map<String, String> message)
        {
            boolean result = true;

            if (Log.log.isTraceEnabled())
            {
                Log.log.trace("Received data to be sent");
                Log.log.trace("Contents:" + message);
            }

            // Check if filter in message still exists since it may have been undeployed before message gets processed
            
            Filter filter = null;
            
            if (message.containsKey(FILTER_ID_NAME))
            {
                filter = orderedFiltersMapById.get(message.get(FILTER_ID_NAME));
                
                if (filter != null)
                {
                    // TODO Log the message instread of sending it to worker.
                    Log.log.info("PrimaryDataQueueListener message: " + message);
                    
                    // This method is implemented by subclasses to update various custom
                    // fields in the target server. For example in Netcool or HPOM
                    // gateway
                    // it will update RESOLVE_STATUS and RESOLVE_RUNBOOKID.
                    
                    long updateStartTmNSec = System.nanoTime();
                    
                    BaseClusteredGateway.this.updateServerData(filter, message);
                    
                    Log.log.debug("Update server data took " + ((System.nanoTime() - updateStartTmNSec) / 1000) + " micro secs");
                }
                else
                {
                    Log.log.warn("Skipping adding message [" + StringUtils.mapToLogString(message) + 
                                 "] received by Primary Data Queue Listener as specified filter with id [" + 
                                 message.get(FILTER_ID_NAME) + "] has been undeployed.");
                }
            }
            else
            {
                Log.log.warn("Message [" + StringUtils.mapToLogString(message) + 
                             "] received by Primary Data Queue Listener does not contain key " + 
                             FILTER_ID_NAME + ", skipping adding message to primary data queue.");                
            }
            
            return result;
        }
    }

    @SuppressWarnings("rawtypes")
    public void addToPrimaryDataQueue(Filter filter, Map<String, String> params)
    {
        Runnable worker = new PrimaryDataQueueWorkerThread(this, filter, params);
        
        BlockingQueue bq = ((ThreadPoolExecutor)executor).getQueue();
        while ((bq.size() >= gatewayPrimaryExecutorQueueSize))
        {
            PrimaryDataQueueWorkerThread e = (PrimaryDataQueueWorkerThread)bq.poll();
            if (e!=null)
            {
                Log.log.warn("Gateway event queue is full. Drop the oldest event from queue: " + e.runbookParams);
            }
        }
        executor.execute(worker);
        
        processFilter(filter, params);
        
    }

    @Override
    public String receiveData(Map<String, String> params)
    {
        String result = null;
        
        if (Log.log.isTraceEnabled())
        {
            dataCounter++;
            Log.log.trace("######################################");
            Log.log.trace(workerQueueName + " processed " + dataCounter + " item(s) so far.");
            Log.log.trace("######################################");
        }

        if (params.containsKey("FILTER_ID"))
        {
            String filterId = params.get("FILTER_ID");
            Filter filter = filters.get(filterId);
            if (filter == null)
            {
                // this may happen when synchronization is not done for this
                // gateway in this rsremote. so it may not
                // have the filter yet or filter may have been undeployed
                
                if (undeployedFilterIds.contains(filterId))
                {
                    Log.log.warn("Filter: " + filterId + " was undeployed from this RSRemote, ignoring data");
                }
                else
                {
                    Log.log.warn("Filter: " + filterId + " not available in this RSRemote, resending the data back to the worker queue");
                    MainBase.esb.sendInternalMessage(workerQueueName, getMessageHandlerName() + ".receiveData", params);
                }
            }
            else
            {
                params.remove(filterId);
                result = processFilter(filter, params);
            }
        }
        else
        {
            for (Filter filter : orderedFilters)
            {
                result = processFilter(filter, params);
            }
        }
        
        return result;
    }

    /**
     * Listener for the worker data queue. This prevents us from reading data
     * through a loop because it's on demand.
     */
    private class WorkerDataQueueListener extends AbstractQueueListener<GatewayEvent<String, Object>>
    {
        public boolean process(GatewayEvent<String, Object> data)
        {
            boolean result = true;

            if (data != null)
            {
                if (Log.log.isTraceEnabled())
                {
                    Log.log.trace("Worker: Process the data received from primary gateway.");
                    Log.log.trace("Contents:" + data.content);
                }

                try
                {
                    if (!processFilterEvents(startTime, data))
                    {
                        Log.log.warn("Could not process gateway data, check the log file." + data.content);
                        result = false;
                    }
                }
                catch (Exception e)
                {
                    Log.log.error(e.getMessage(), e);
                }
            }
            else
            {
                Log.log.debug("Something wrong, worker is being called with empty data, check log file.");
            }
            return result;
        }
    }

    /**
     * This method is used by the Workers when they receives message to be
     * processed from primary.
     * 
     * @param filter
     * @param params
     */
    protected String processFilter(final Filter filter, final Map<String, String> params)
    {
        String result = "";
        
        Map<String, String> content = setRunbookParam(params);
        String eventId = content.get(Constants.EXECUTE_EVENTID);//response.get(Constants.EXECUTE_EVENTID);
        String runbookName = content.get(Constants.EXECUTE_WIKI);//response.get(Constants.EXECUTE_WIKI);
        
        //check if the incoming params Map has wiki or event id.

        if (content != null)
        {
            //still no event, get it from the filter
            if(StringUtils.isBlank(eventId))
            {
                // Oracle DB returns "undefined" for the null value so this comparison is necessary.
                if (StringUtils.isNotBlank(filter.getEventEventId()) && !"undefined".equalsIgnoreCase(filter.getEventEventId()))
                {
                    eventId = filter.getEventEventId();
                    content.put(Constants.EXECUTE_EVENTID, eventId);
                }
            }

            //still no runbook, get it from the filter
            if(StringUtils.isBlank(runbookName))
            {
                // Oracle DB returns "undefined" for the null value so this comparison is necessary.
                if (StringUtils.isNotBlank(filter.getRunbook()) && !"undefined".equalsIgnoreCase(filter.getRunbook()))
                {
                    runbookName = filter.getRunbook();
                    content.put(Constants.EXECUTE_WIKI, runbookName);
                }
            }
        }
        
        if (filter.hasGroovyScript())
        {
            String script = filter.getScript();
            String scriptName = filter.getId();

            Binding binding = new Binding();
            binding.setVariable(Constants.GROOVY_BINDING_LOG, Log.log);
            binding.setVariable(Constants.GROOVY_BINDING_DEBUG, debug);
            binding.setVariable(Constants.GROOVY_BINDING_NAME_PROPERTIES, nameProperties);

            // Filter specific java.util.regex.Matcher object may come from the
            // subclasses for example, XMPP gateway provides a matcher.
            binding.setVariable(Constants.GROOVY_BINDING_REGEX_MATCHER, filter.getMatcher(content));

            // parameters
            binding.setVariable(Constants.GROOVY_BINDING_INPUTS, content);

            // assess output
            Map<String, String> outputs = new HashMap<String, String>();
            // pass all the params
            outputs.putAll(content);

            binding.setVariable(Constants.GROOVY_BINDING_OUTPUTS, outputs);

            // put additional bindings, for example gateways Netcool or DB
            // provides a java.sql.Connection in the script as "DB"
            addAdditionalBindings(script, binding);

            // execute script
            try
            {
                GroovyScript.executeAsync(script, scriptName, true, binding, TASK_TIMEOUT);
            }
            catch (Exception ex)
            {
                sendAlert(ALERT_SEVERITY_CRITICAL, false, "Filter:" + filter.getId() + ":Groovy Script Error", ex.getMessage());
                Log.log.error("Failed to execute script for the Filter: " + filter.getId(), ex);
            }
            finally
            {
                // subclasses may need some cleanup (like Netcool wants to
                // return connection to the pool.
                groovyScriptBindingCleanup(binding);
            }
            Log.log.debug("Filter Script OUTPUTS: " + outputs);

            // groovy script might have changed EventId
            if (StringUtils.isNotBlank(outputs.get(Constants.EXECUTE_EVENTID)))
            {
                eventId = outputs.get(Constants.EXECUTE_EVENTID);
            }

            // groovy script might have changed WIKI
            if (StringUtils.isNotBlank(outputs.get(Constants.EXECUTE_WIKI)))
            {
                runbookName = outputs.get(Constants.EXECUTE_WIKI);
            }

            // set content to outputs
            content = outputs;
        }
        
        ExecuteUnitTest t = new ExecuteUnitTest();
        
        try
        {
            t.executeFilterTest(filter, content);
        }
        catch (MalformedURLException e1)
        {
            Log.log.error(e1.getMessage(), e1);
        }
        
        return result;
    }

    protected void executeRunbook(Filter filter, String wikiName, Map<String, String> result)
    {
        Map<String, Object> event = new HashMap<String, Object>();
        event.putAll(result);
        if (StringUtils.isEmpty(wikiName))
        {
            event.put(Constants.EXECUTE_WIKI, filter.getRunbook());
        }
        else
        {
            event.put(Constants.EXECUTE_WIKI, wikiName);
        }
        event.put(QUEUE, topicName);

        // Put any additional key/value needs to be added to the event.
        addAdditionalValues(event, result, filter);

        GatewayEvent<String, Object> gwEvent = new GatewayEvent<String, Object>(System.currentTimeMillis(), event);
        workerDataQueue.offer(gwEvent);
    }

    /**
     * This sets the runbook parameter to the Map, the interesting part here is,
     * if the gateway is configured to make the Map's keys uppercase then it
     * will be done here otherwise it will keep whatever form it came in.
     */
    private Map<String, String> setRunbookParam(Map<String, String> params)
    {
        Map<String, String> result = params;

        if (isUppercase())
        {
            Map<String, String> params1 = new HashMap<String, String>();
            if (params != null)
            {
                for (String key : params.keySet())
                {
                    params1.put(key.toUpperCase(), params.get(key));
                }
                result = params1;
            }
        }
        return result;
    }

    /**
     * Processes events collected by processFilter. These events are mainly
     * runbook execution.
     */
    protected boolean processFilterEvents(long startTime, GatewayEvent<String, Object> event) throws Exception
    {
        boolean result = true;
        return result;
    }

    @Override
    public void removeFilter(String id)
    {
        Filter filter = filters.get(id);
        if (filter != null)
        {
            // remove from filters
            filters.remove(id);

            // create new orderedFilters
            synchronized (BaseClusteredGateway.class)
            {
                List<Filter> syncedOrderededFilters = new ArrayList<Filter>(filters.values());
                Collections.sort(syncedOrderededFilters);
                
                orderedFilters.clear();
                orderedFilters.addAll(syncedOrderededFilters);
                //Collections.sort(orderedFilters);
                orderedFiltersMapById.remove(id);
            }
        }
    } // removeFilter

    @Override
    public Map<String, Filter> getFilters()
    {
        return filters;
    } // getFilters

    @Override
    public void clearAndSetFilters(List<Map<String, Object>> filterList)
    {
    } // clearAndSetFilters

    /**
     * Clean any resources to be cleared out when filters are undeployed. sub
     * classes will implement this method.
     * 
     * @param originalFilters
     */
    protected void cleanFilter(ConcurrentHashMap<String, Filter> originalFilters)
    {
        // let the subclass override this
    }

    @Override
    public void setFilterActive(String id, boolean active)
    {
        Filter filter = filters.get(id);
        if (filter != null)
        {
            filter.setActive(active);
        }
    } // setFilterActive

    @Override
    public NameProperties getNameProperties()
    {
        return nameProperties;
    }

    @Override
    public Set<RoutingSchema> getRoutingSchemas()
    {
        return routingSchemas;
    }

    private void sendMessage(String name, Map<String, String> raw)
    {
        Map<String, Object> params = new HashMap<String, Object>();
        params.put("QUEUE", getQueueName());
        params.put("NAME", name);
        if (raw != null)
        {
            // params.put("CONTENT", StringUtils.mapToString(raw));
            params.put("CONTENT", raw);
        }
        MainBase.esb.sendInternalMessage("RSCONTROL", getMessageHandlerName() + ".setNameProperties", params);
    }

    @SuppressWarnings({ "rawtypes", "unchecked" })
    @Override
    public synchronized void setNameProperties(String name, Map prop, boolean doSave)
    {
        Log.log.debug("Saving name properties for " + name);
        getNameProperties().setNameProperties(name, prop);

        if (doSave)
        {
            ObjectProperties props = new ObjectProperties(prop);
            // send message to RSCONROL
            Log.log.debug("Sending name property " + name + " to RSCONTROL for persistence.");
            sendMessage(name, props.getRaw());
        }
    }

    @SuppressWarnings("unchecked")
    @Override
    public synchronized void setNameProperty(String name, String key, String value)
    {
        Log.log.debug("Saving name properties for " + name);
        getNameProperties().getNameProperties(name).put(key, value);

        setNameProperties(name, getNameProperties().getNameProperties(name), true);
    }

    @Override
    public synchronized void removeNameProperties(String name)
    {
    }

    @Override
    public synchronized void removeNameProperty(String name, String key)
    {
        Log.log.debug("Removing name properties for " + name + " with key " + key);
        getNameProperties().getNameProperties(name).remove(key);
        // save it
        setNameProperties(name, getNameProperties().getNameProperties(name), true);
    }

    @Override
    public synchronized void clearAndSetNameProperties(List<Map<String, Object>> paramList)
    {
    }

    /**
     * For all the key's in the Map, refer to getRoutingSchemas(String queueName) method in MGateway class from RSControl project.
     */
    @Override
    public synchronized void clearAndSetRoutingSchemas(List<Map<String, Object>> paramList)
    {
    }

    /**
     * This method requests RSCONTROL to provide routing schemas belongs to this gateway.
     */
    @Override
    public void sendRoutingSchemaSyncRequest()
    {
    }

    @Override
    public void processFilter(Map<String, String> params)
    {
    }

    @Override
    public Queue<Map<String, String>> getPrimaryDataQueue()
    {
        return primaryDataQueue;
    }

    static class PrimaryDataQueueWorkerThread implements Runnable
    {
        final ClusteredGateway instance;
        final Filter filter;
        Map<String, String> runbookParams = new HashMap<String, String>();

        public PrimaryDataQueueWorkerThread(ClusteredGateway instance, Filter filter, Map<String, String> runbookParams)
        {
            this.instance = instance;
            this.filter = filter;
            this.runbookParams = runbookParams;
        }

        @Override
        public void run()
        {
            Log.log.debug(Thread.currentThread().getName() + " thread adding to primary data queue for filter: " + filter.getId());
            processCommand();
        }

        private void processCommand()
        {
            if (filter != null)
            {
                runbookParams.put("FILTER_ID", filter.getId());
            }

            // put into the queue so it's processed asynchronously.
            instance.getPrimaryDataQueue().offer(runbookParams);

            // This method is implemented by subclasses to update various custom
            // fields in the target server. For example in Netcool or HPOM
            // gateway
            // it will update RESOLVE_STATUS and RESOLVE_RUNBOOKID.
            //instance.updateServerData(filter, runbookParams);
        }
    }
    
    public boolean primaryDataQueueExecutorBusy()
    {
        if (executor != null)
        {
            return (((ThreadPoolExecutor)executor).getQueue()).size() > 0;
        }
        else
            return false;
    }
    
    public int getPrimaryDataQueueExecutorQueueSize()
    {
        return gatewayPrimaryExecutorQueueSize;
    }
    
    @Override
    public String getOrgSuffix()
    {
        return "";
    }
}
