package com.resolve.gateway;

import org.codehaus.jackson.annotate.JsonProperty;

public class RoutingSchemaField implements Comparable<RoutingSchemaField>
{
    @JsonProperty("name")
    public String name;

    @JsonProperty("source")
    public String source;
    
    @JsonProperty("regex")
    public String regex;

    @JsonProperty("order")
    public Integer order;

    public RoutingSchemaField()
    {
        
    }
    
    public String getName()
    {
        return name;
    }

    public void setName(String name)
    {
        this.name = name;
    }

    public String getSource()
    {
        return source;
    }

    public void setSource(String source)
    {
        this.source = source;
    }

    public String getRegex()
    {
        return regex;
    }

    public void setRegex(String regex)
    {
        this.regex = regex;
    }

    public Integer getOrder()
    {
        return order;
    }

    public void setOrder(Integer order)
    {
        this.order = order;
    }
    
    @Override
    public int compareTo(RoutingSchemaField field)
    {
        return this.order.compareTo(field.order);
    }

    @Override
    public String toString()
    {
        return "RoutingSchemaField [name=" + name + ", source=" + source + ", regex=" + regex + ", order=" + order + "]";
    }
}
