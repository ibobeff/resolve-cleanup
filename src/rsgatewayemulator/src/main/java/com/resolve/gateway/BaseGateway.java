/******************************************************************************
* (C) Copyright 2014
*
* Resolve Systems, LLC
*
* All rights reserved
* This software is distributed under license and may not be used, copied,
* modified, or distributed without the express written permission of
* Resolve Systems, LLC.
*
******************************************************************************/

package com.resolve.gateway;

import java.util.HashMap;
import java.util.Map;

import com.resolve.groovy.ThreadGroovy;
import com.resolve.rsbase.SessionObjectInterface;
import com.resolve.util.ConfigMap;
import com.resolve.util.Log;

/**
 * This class provides a partial implementation for gateway sub classes
 * implemting most common functionalities of the gateways. In general
 * every gateway does not participate in a cluster environment (e.g., ITM, SSH)
 * so this class makes sure those non-clustered gateways get their common
 * functionalities from here.
 */
public abstract class BaseGateway implements Gateway
{
    protected final static String ALERT_SEVERITY_CRITICAL = "CRITICAL";
    protected final static String ALERT_SEVERITY_WARNING = "WARNING";
    protected final static String ALERT_SEVERITY_INFO = "INFO";

    protected final static int LICENSE_CHECK_INTERVAL = 2; //in minutes
    protected boolean debug = false;

    protected boolean active = true;
    protected volatile boolean running = true;
    protected ConfigMap configurations;

    private long gatewayLicenseExpiredTime = 0;
    
    //keep track of the alert messages being sent from this gateway so it doesn't
    //keep sending same aler over an over again.
    protected Map<String, String> alertCache = new HashMap<String, String>();

    abstract protected void initialize();

    abstract protected String getGatewayEventType();

    abstract protected String getMessageHandlerName();

    abstract protected Class getMessageHandlerClass();

    public void reinitialize()
    {
        alertCache.clear();
        //Subclass must explicitly override this method if needed (e.g., XMPP).
    }

    public void deactivate()
    {
        alertCache.clear();
        //Subclass must explicitly override this method if needed (e.g., XMPP).
    }

    /**
     * This method is implemented by subclasses to update various custom fields
     * in the target server. For example in Netcool or HPOM gateway it will
     * update RESOLVE_STATUS and RESOLVE_RUNBOOKID.
     *
     * @param filter
     * @param content
     */
    protected void updateServerData(Filter filter, Map content)
    {
        // Not implemented here, normaly subclasses like HPOM, Netcool etc.
        // should implement this method
        // to update the RESOLVE_STATUS, RESOLVE_RUNBOOKID field etc.
    }

    @Override
    public boolean isActive()
    {
        return active;
    }

    @Override
    public void setActive(boolean active)
    {
        this.active = active;
    }

    protected BaseGateway(ConfigMap config)
    {
        try
        {
            // assign to the configurations.
            configurations = config;
        }
        catch (Exception e)
        {
            Log.log.warn(e.getMessage(), e);
        }
    }

    @Override
    public void init() throws Exception
    {
        try
        {
            // Call this subclass method if there are any specific
            // initialization. Make sure it's always called first.
            initialize();
        }
        catch (Exception e)
        {
            Log.log.warn(e.getMessage(), e);
            throw e;
        }
    }

    /**
     * This method is implemented by the gateway that are open. For example if PS team
     * develops a gateway for certain customer then they do no need to buy license.
     * In that situation the PS developed gateway will override this method and return 
     * false.
     *  
     * @return
     */
    protected boolean requireLicense()
    {
        return true;
    }
    
    protected boolean isValidLicense(boolean checkToStart, boolean originalPrimary) // True - Check if ok to start, False - Check if ok to keep running
    {
//        if(requireLicense())
//        {
//            return MainBase.main.getLicenseService().checkGatewayLicense(getLicenseCode(), MainBase.main.configId.getGuid(), getInstanceType(), getQueueName(), System.currentTimeMillis(), checkToStart, originalPrimary);
//        }
//        else
//        {
//            return true;
//        }
        return true;
    }
    
    protected void checkGatewayLicense() throws Exception
    {
//        if(requireLicense())
//        {
//            if(MainBase.main.getLicenseService().isGatewayLicenseExpired(getLicenseCode()))
//            {
//                long currentTime = System.currentTimeMillis();
//                if (gatewayLicenseExpiredTime == 0)
//                {
//                    gatewayLicenseExpiredTime = currentTime;
//                }
//                //send the alert every 24 hours.
//                if ((currentTime - gatewayLicenseExpiredTime) > 24*60*60*1000L)
//                {
//                    Log.alert(ERR.E10021.getCode(), ERR.E10021.getMessage() + " " + getLicenseCode() + " gateway license has expired.", Constants.ALERT_TYPE_LICENSE);
//                    gatewayLicenseExpiredTime = currentTime;
//                }
//            }
//            else
//            {
//                boolean originalPrimary = this instanceof BaseClusteredGateway ? ((BaseClusteredGateway)this).isOriginalPrimary() : false;
//                
//                boolean isValid = isValidLicense(false, originalPrimary); // originalPrimary matters only for start
//        
//                Log.log.info("License for : " + getLicenseCode() + (isValid ? " is valid" : " is not valid"));
//        
//                if(!isValid && running)
//                {
//                    // If Current Gateway is found to be running with invalid license then stop
//                    Log.log.warn("Found running gateway with invalid license, stopping " + getLicenseCode() + "-" + getInstanceType() + "-" + getQueueName() + " gateway.");
//                    stop();
//                }
//        
//                //it is possible that a license got uploaded and we can start the gateway now
//                if(isValid && !running && MainBase.main.isClusterModeActive())
//                {
//                    Log.log.info("Gateway license found, restarting " + getLicenseCode() + " gateway.");
//                    start();
//                }
//            }
//        }
//        else
//        {
//            if(Log.log.isDebugEnabled())
//            {
//                Log.log.debug("License not required for " + getLicenseCode() + "-" + getInstanceType() + "-" + getQueueName());
//            }
//        }
    }

    /**
     * Clustered gateway override this method to indicate if it's PRIMARY, SECONDARY, or a WORKER.
     * Other gateways like ITM etc keeps it blank.
     *   
     * @return
     */
    protected String getInstanceType()
    {
        return "";
    }

    @Override
    public void registerSessionObject(SessionObjectInterface sessionObject)
    {
        Log.log.debug("Register " + sessionObject.getClass().getSimpleName() + ":" + sessionObject.toString());
        ThreadGroovy.getConnectionList().add(sessionObject);
    }

    protected void resetAlertCache()
    {
        alertCache.clear();
    }

    public void sendAlert(String severity, boolean cache, String code, String message)
    {
        Log.log.error("Severity: " + severity + ", Code: " + code + ", Message: " + message);
    }
}
