package com.resolve.gateway;

/******************************************************************************
* (C) Copyright 2015
*
* Resolve Systems, LLC
*
* All rights reserved
* This software is distributed under license and may not be used, copied,
* modified, or distributed without the express written permission of
* Resolve Systems, LLC.
*
******************************************************************************/

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;

import com.resolve.rsbase.MainBase;
import com.resolve.util.StringUtils;

/**
 * This class provides a skeletal implementation of standard functionality
 * which is common to all external system specific gateways.
 * <p> 
 * Derived external system specific gateway APIs must override the skeletal implementations
 * of standard functionality as required in addition to providing system specific functionalities.
 * <p>
 * All methods take user name and password as parameters. If user name and/or password is not specified 
 * then user name password configured in blueprint.properties are used.
 * 
 * @author Hemant Phanasgaonkar
 * @since  5.3
 * 
 */
public abstract class AbstractGatewayAPI
{
    /**
     * An instance of {@link BaseClusteredGateway}.
     */
    protected static BaseClusteredGateway instance;
    
    /** 
     * Returns type of gateway {@value com.resolve.gateway.GatewayConstants#PULL_TYPE_GATEWAY}/{@value com.resolve.gateway.GatewayConstants#PUSH_TYPE_GATEWAY}. 
     * <p>
     * Default type returned is {@value com.resolve.gateway.GatewayConstants#PULL_TYPE_GATEWAY}. Derived external 
     * system specific gateway API must override this method to return appropriate type if not 
     * {@value com.resolve.gateway.GatewayConstants#PULL_TYPE_GATEWAY}.
     * 
     * @return Type of Gateway
     * @see    GatewayConstants#PULL_TYPE_GATEWAY
     * @see    GatewayConstants#PUSH_TYPE_GATEWAY
     *         
     */
    public static int getType()
    {
        return GatewayConstants.PULL_TYPE_GATEWAY;
    }
    
    /**
     * Returns {@link Set} of supported object types.
     * <p>
     * Default no object types are supported returning {@link Collections.EMPTY_SET}.
     * Derived external system specific gateway API must override this method to return
     * {@link Set<String>} of supported object types.
     * 
     * @return {@link Set} of supported object types
     */
    public static Set<String> getObjectTypes()
    {
        return Collections.emptySet();
    }
    
    private static Map<String, String> returnMethoNotSupportedMap()
    {
        Map<String, String> mthdNotSupported = new HashMap<String, String>();
        
        mthdNotSupported.put(GatewayConstants.METHOD_NOT_SUPPORTED_KEY, "true");
        
        return mthdNotSupported;
    }
    
    /**
     * Creates instance of specified object type in external system.
     * <p>
     * Default attribute name-value pair returned is  
     * {@value com.resolve.gateway.GatewayConstants#METHOD_NOT_SUPPORTED_KEY}-true
     * indicating external system specific gateway does not support creating instances of
     * specified object type.
     * <p>
     * If external system specific gateway does support creation of object then it must return
     * {@link Map} of attribute name-value pairs.
     * 
     * @param  objType   Type of object to create in external system
     * @param  objParams {@link Map} of object parameters
     * @param  userName  User name
     * @param  password  Password
     * @return           Attribute id-value {@link Map} of created object in external system
     * @throws Exception If any error occurs in creating object of specified
     *                   type in external system
     */
    public static Map<String, String> createObject(String objType, Map<String, String> objParams, String userName, String password) throws Exception
    {
        return returnMethoNotSupportedMap();
    }
    
    /**
     * Reads attributes of specified object in external system.
     * <p>
     * Default attribute id-value pair returned is  
     * {@value com.resolve.gateway.GatewayConstants#METHOD_NOT_SUPPORTED_KEY}-true
     * indicating external system specific gateway does not support reading atributes of
     * object.
     * <p>
     * If external system specific gateway does support reading attributes of object then 
     * it must return {@link Map} of attribute name-value pairs. 
     *
     * @param  objType    Object type
     * @param  objId      Id of the object to read attributes of from external system
     * @param  attribs    {@link List} of attributes of object to read
     * @param  userName   User name
     * @param  password   Password
     * @return            {@link Map} of object attribute id-value pairs
     * @throws Exception  If any error occurs in reading attributes of the object in 
     *                    external system
     */
    public static Map<String, String> readObject(String objType, String objId, List<String> attribs, String userName, String password) throws Exception
    {
        return returnMethoNotSupportedMap();
    }
    
    /**
     * Updates attributes of specified object in external system.
     * <p>
     * Default updated attribute name-value pair returned is  
     * {@value com.resolve.gateway.GatewayConstants#METHOD_NOT_SUPPORTED_KEY}-true
     * indicating external system specific gateway does not support updating attributes of
     * object.
     * <p>
     * If external system specific gateway does support updating attributes of object then it 
     * must return updated {@link Map} of attribute name-value pairs.
     * 
     * @param  objType   Object type
     * @param  objId     Id of the object to update attributes of in external system
     * @param  updParams Key-value {@link Map} of object attributes to update
     * @param  userName  User name
     * @param  password  Password
     * @return           Key-value {@link Map} of the updated object attributes
     * @throws Exception If any error occurs in updating attributes of the object in 
     *                   external system
     */
    public static Map<String, String> updateObject(String objType, String objId, Map<String, String> updParams, String userName, String password) throws Exception
    {
        return returnMethoNotSupportedMap();
    }
    
    /**
     * Deletes specified object from external system.
     * <p>
     * Default deleted attribute name-value pair returned is  
     * {@value com.resolve.gateway.GatewayConstants#METHOD_NOT_SUPPORTED_KEY}-true
     * indicating external system specific gateway does not support object deletion.
     * <p>
     * If external system specific gateway does support object deletion then it 
     * must return {@link Map} of object deletion operation reult.
     * 
     * @param  objType   Object type
     * @param  objId     Id of the object to delete from external system
     * @param  userName  User name
     * @param  password  Password
     * @return           Key-value {@link Map} result of object delete operation
     * @throws Exception If any error occurs in deleting object from external system
     */
    public static Map<String, String> deleteObject(String objType, String objId, String userName, String password) throws Exception
    {
        return returnMethoNotSupportedMap();
    }
    
    /**
     * Get list of objects from external system matching specified filter condition.
     * <p>
     * Default implementation returns {@link List} containing {@link Map} with 
     * {@value com.resolve.gateway.GatewayConstants#METHOD_NOT_SUPPORTED_KEY}-true indicating
     * external system specific gateway does not support getting objects matching filter condition.
     * <p>
     * If external system specific gateway does support getting objects matching filter condition then it 
     * must return {@link List} of {@link Map} of matching objects attribute id-value pairs.
     * 
     * @param  objType  Object type
     * @param  filter   External system gateway specific filer conditions
     * @param  userName User name
     * @param  password Password
     * @return          {@link List} of {@link Map}s of object attribute id-value pairs matching
     *                  filter condition
     * @throws Exception If any error occurs in getting object from external system
     */
    
    public static List<Map<String, String>> getObjects(String objType, BaseFilter filter, String userName, String password) throws Exception
    {
        List<Map<String, String>> objs = new ArrayList<Map<String, String>>();
        
        objs.add(returnMethoNotSupportedMap());
        
        return objs;
    }
    
    /**
     * Saves configuration for all configured gateways into conf/config.xml.
     * <p>
     * Derived implementaion classes should overwrite this method if required but
     * should always have super.saveConfig() as first statement.
     * 
     * @throws Exception
     */
    public static void saveConfig() throws Exception
    {
        MainBase.main.callSaveConfig();
    }
    
    /**
     * Returns the {@link Object} value for the given key in the current
     * gateway instance's NameProperty map.
     * 
     * @param name
     *            the name of the property {@link Map} in the
     *            gateway instance's {@link NameProperties}
     *            object
     * @param key
     *            the key associated with the expected value in the property
     *            {@link Map} returned from the gateway instance's
     *            {@link NameProperties} object
     * @return {@link Object} value associated with the key in the property
     *         {@link Map} returned by the gateway instance's
     *         {@link NameProperties} object
     */
    public static Object getNameObject(String name, String key)
    {
        return instance.getNameProperties().getNameObject(name, key);
    }
    
    /**
     * Sets the given name parameter to return a property {@link Map} within the
     * gateway instance's {@link NameProperties} object. If
     * the returned {@link Map} isn't null, it adds the given {@link String} key
     * and {@link Object} value to it.
     * 
     * @param name
     *            the name of the {@link Map} in the gateway
     *            instance's {@link NameProperties} object. Cannot be null or
     *            empty.
     * @param key
     *            the key in the {@link Map} returned from the
     *            {@link NameProperties} object in the gateway
     *            instance. Cannot be null or empty.
     * @param value
     *            the value associated with the given key in the {@link Map}
     *            returned from the {@link NameProperties} object in the
     *            gateway instance.
     * 
     * @throws Exception
     */

    @SuppressWarnings({ "rawtypes", "unchecked" })
    public static void setNameObject(String name, String key, Object value) throws Exception
    {
        Map nameProperties = getNameProperties(name);
        if(nameProperties != null)
        {
            nameProperties.put(key, value);
            setNameProperties(name, nameProperties, true);
        }
    }
    
    /**
     * Uses the given {@link String} name as a key to return a {@link Map} of
     * properties from the gateway instance's {@link NameProperties} object.
     *
     * @param name
     *            The name associated with the properties {@link Map} in the
     *            gateway instance's {@link NameProperties} object.
     *            Cannot be null or empty.
     * @return a Map of gateway instances name properties
     * @throws Exception
     */

    @SuppressWarnings("rawtypes")
    public static Map getNameProperties(String name) throws Exception
    {
        return instance.getNameProperties().getNameProperties(name);
    }
    
    /**
     * Associates the given name to the given properties Map in the
     * gateway instance's NameProperties object.
     *
     * @param name
     *            the key in the gateway instance's {@link NameProperties}
     *            object. Cannot be null or empty.
     * @param properties
     *            A {@link Map} of properties to associate with the given name
     *            parameter
     * @param doSave
     *            a boolean value that designates whether to send this change to
     *            RSControl for persistence
     * @throws Exception
     */

    @SuppressWarnings("rawtypes")
    public static void setNameProperties(String name, Map properties, boolean doSave) throws Exception
    {
        if (StringUtils.isBlank(name))
        {
            throw new Exception("'name' parameter must be passed");
        }
        else
        {
            instance.setNameProperties(name, properties, doSave);
        }
    }
    
    /**
     * Removes a {@link Map} of properties associated with the given name in the
     * gateway instance's {@link NameProperties} object.
     *
     * @param name
     *            The name associated with a property {@link Map} in the
     *            gateway instance's {@link NameProperties}
     *            object. Cannot be null or empty.
     * @throws Exception
     */

    public static void removeNameProperties(String name) throws Exception
    {
        if (StringUtils.isBlank(name))
        {
            throw new Exception("'name' parameter must be passed");
        }
        else
        {
            instance.removeNameProperties(name);
        }
    }
}
