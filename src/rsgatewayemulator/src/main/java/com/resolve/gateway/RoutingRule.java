package com.resolve.gateway;

import java.io.Serializable;

public class RoutingRule implements Serializable
{
    private static final long serialVersionUID = 7603254752344441582L;
	private String rid;
    private String runbook;
    private String eventId;
    
    public RoutingRule(String rid, String runbook, String eventId)
    {
        super();
        this.rid = rid;
        this.runbook = runbook;
        this.eventId = eventId;
    }

    public String getRid()
    {
        return rid;
    }

    public void setRid(String rid)
    {
        this.rid = rid;
    }

    public String getRunbook()
    {
        return runbook;
    }

    public void setRunbook(String runbook)
    {
        this.runbook = runbook;
    }

    public String getEventId()
    {
        return eventId;
    }

    public void setEventId(String eventId)
    {
        this.eventId = eventId;
    }

    @Override
    public String toString()
    {
        return "RoutingRule [rid=" + rid + ", runbook=" + runbook + ", eventId=" + eventId + "]";
    }
}
