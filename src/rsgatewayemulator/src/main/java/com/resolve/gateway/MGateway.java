/******************************************************************************
 * (C) Copyright 2014
 *
 * Resolve Systems, LLC
 *
 * All rights reserved This software is distributed under license and may not be
 * used, copied, modified, or distributed without the express written permission
 * of Resolve Systems, LLC.
 *
 ******************************************************************************/

package com.resolve.gateway;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.resolve.rsbase.MainBase;
import com.resolve.util.Constants;
import com.resolve.util.Log;
import com.resolve.util.Properties;
import com.resolve.util.StringUtils;

/**
 * This is an abstract class that's extended by various subclasses for Resolve
 * Gateways (Netcool, Remedyx etc.)
 *
 * While looking at the Gateway code think Abstraction and OO concepts like
 * inheritance etc. There are hardly any copy/paste in these codes. It helps in
 * reducing maintenance overhead and keep the codebase more meaningful. These
 * codes go through "FindBug" which makes sure that there is no obvious problem
 * with the code.
 *
 * Check {@link MNetcool} or {@link MRemedyx} for its usage.
 */
public abstract class MGateway
{
    private static final String TRUE = "TRUE";

    protected ClusteredGateway instance;

    protected abstract void populateGatewaySpecificFilterProperties(Map<String, String> filterMap, Filter filter);

    protected abstract String getSetFilterMethodName();

    // flag to set if we want to RSControl to be update
    // used during synchronization with RSControl when we
    // do not need to send update message back to RSCotnrol unnecessarily.
    protected boolean isUpdateRSControl = true;

    /**
     * Initiates a heartbeat from the current Gateway instance
     * 
     * @param params
     *            a map of parameters used in exectuting a heartbeat, mapping
     *            "PRIMARY" to a boolean value, intended to ascertain if you're
     *            looking for a heartbeat from a primary or secondary gateway
     * 
     */
    public void heartbeat(Map<String, String> params)
    {
        if (instance.getHeartbeatThread() != null)
        {
            instance.getHeartbeatThread().processHeartbeatMessage(params);
        }
    } // heartbeat

    /**
     * Sets the filters for the current Gateway singleton from a {@link List} of
     * parameter {@link Map}s
     * 
     * @param paramList
     *            a {@link List} of {@link Map}s of filters
     * 
     */
    public void setFilters(List<Map<String, Object>> paramList)
    {
        if (paramList != null)
        {
            for (Map<String, Object> params : paramList)
            {
                instance.setFilter(params);
            }
        }

        // save config
        MainBase.main.exitSaveConfig();
    } // setFilters

    /**
     * Sets the filters for the current Gateway singleton
     * 
     * @param params
     *            a {@link Map} of filters
     * 
     */
    public void setFilter(Map<String, Object> params)
    {
        instance.setFilter(params);
        // save config
        MainBase.main.exitSaveConfig();
    } // setFilter

    /**
     * Activates a filter based on a Map of parameters.
     * 
     * @param params
     *            a {@link Map} of parameters to set a filter active. The Map
     *            should have the following key:value format: BaseFilter.ID :
     *            String BaseFilter.ACTIVE : String
     * 
     */
    public void setFilterActive(Map<String, String> params)
    {
        String name = (String) params.get(BaseFilter.ID);
        String activeString = (String) params.get(BaseFilter.ACTIVE);

        // boolean active = !(activeString != null &&
        // activeString.equalsIgnoreCase("FALSE"));
        boolean active = (activeString == null || activeString.equalsIgnoreCase(TRUE));

        instance.setFilterActive(name, active);

        // save config
        MainBase.main.exitSaveConfig();
    } // setFilterActive

    /**
     * This method is invoked once filter synchronization message from RSCONTROL
     * is received.
     *
     * @param paramList
     */
    public void synchronizeGateway(Map<String, List<Map<String, Object>>> paramList)
    {
        if (paramList != null)
        {
            isUpdateRSControl = false;
            Log.log.info("Processing Gateway synchronization message received for " + instance.getQueueName());
            List<Map<String, Object>> filters = (List<Map<String, Object>>) paramList.get("FILTERS");
            int number = 0;
            if (filters != null)
            {
                number = filters.size();
            }
            Log.log.info("NUMBER of filters " + number);
            clearAndSetFilters(filters);

            List<Map<String, Object>> nameProperties = (List<Map<String, Object>>) paramList.get("NAMEPROPERTIES");
            number = 0;
            if (nameProperties != null)
            {
                number = nameProperties.size();
            }

            Log.log.info("NUMBER of nameProperties " + number);
            clearAndSetNameProperties(nameProperties);
            
            List<Map<String, Object>> routingSchemas = (List<Map<String, Object>>) paramList.get("ROUTINGSCHEMAS");
            number = 0;
            if (routingSchemas != null)
            {
                number = routingSchemas.size();
            }

            Log.log.info("NUMBER of Resolution Routing Schemas " + number);
            clearAndSetRoutingSchemas(routingSchemas);

            isUpdateRSControl = true;
            // tell the gateway that we have received RSControl sync message.
            instance.setSyncDone(true);
        }
    }

    /**
     * This method requests rscontrol to provide routing schemas.
     *
     * @param params
     */
    public void synchronizeRoutingSchemas(Map<String, String> params)
    {
        if (params != null && params.containsKey("RESET_ROUTING_SCHEMAS"))
        {
            Log.log.info("Processing Gateway Routing Schema synchronization message received for " + instance.getQueueName());
            instance.sendRoutingSchemaSyncRequest();
        }
    }

    /**
     * This method receives routing schemas from RSCONTROL.
     *
     * @param paramList
     */
    public void receiveRoutingSchemas(Map<String, List<Map<String, Object>>> paramList)
    {
        if (paramList != null)
        {
            isUpdateRSControl = false;
            Log.log.info("Processing Gateway Routing Schema synchronization message received for " + instance.getQueueName());
            
            List<Map<String, Object>> routingSchemas = (List<Map<String, Object>>) paramList.get("ROUTINGSCHEMAS");
            int number = 0;
            if (routingSchemas != null)
            {
                number = routingSchemas.size();
            }

            Log.log.info("NUMBER of Resolution Routing Schemas " + number);
            clearAndSetRoutingSchemas(routingSchemas);
        }
    }

    /**
     * Clears the filters associated with the current Gateway instance. Resets
     * the new filter list with those in the paramList, if paramLIst is not
     * null.
     *
     * @param paramList
     *            a {@link List} of filter {@link Map}s.
     */

    public void clearAndSetFilters(List<Map<String, Object>> paramList)
    {
        Log.log.debug("Received clearAndSetFilter" + (paramList != null ? " " + paramList.size() : ""));
        // clear and set filters
        instance.clearAndSetFilters(paramList);

        // Send mesage to RSControl to clear deployed filters for Queue from DB
        if (instance.isPrimary())
        {
            // send new filters
            Map<String, Object> params = null;
            // extract the first Map so we get the username who is deploying the
            // filter
            if (paramList != null && paramList.size() > 0)
            {
                params = paramList.get(0);
            }
            getFilters(params);
        }

        // save config
        MainBase.main.exitSaveConfig();

    } // clearAndSetFilters

    /**
     * Clears the name properties associated with the current {@link Gateway}
     * instance and resets the new name properties link List with those in the
     * paramList, if paramList is not null.
     *
     * @param paramList
     *            a {@link List} of name property {@link Map}s
     */

    public void clearAndSetNameProperties(List<Map<String, Object>> paramList)
    {
        instance.clearAndSetNameProperties(paramList);
    } // clearAndSetNameProperties


    /**
     * Clears the resolution routing schemas with the current {@link Gateway}
     * instance and resets the new schemas link List with those in the
     * paramList, if paramList is not null.
     *
     * @param paramList
     *            a {@link List} of routing schemas {@link Map}s
     */

    public void clearAndSetRoutingSchemas(List<Map<String, Object>> paramList)
    {
        instance.clearAndSetRoutingSchemas(paramList);
    } // clearAndSetRoutingSchemas

    /**
     * Sends a {@link Map} of gateway {@link Filter}s from the current
     * {@link Gateway} instance as an internal message. If the params
     * {@link Map} isn't valid, it will append its value to the Gateway
     * instance's filterMap. The params map should only feature a
     * Constants.SYS_UPDATED_BY key and value.
     *
     * @param paramList
     *            a parameter {@link Map}, which has the following format:
     *            Constants.SYS_UPDATED_BY : String
     */

    public void getFilters(Map<String, Object> params)
    {
        Map<String, String> content = new HashMap<String, String>();

        for (Filter filter : instance.getFilters().values())
        {
            BaseFilter baseFilter = (BaseFilter) filter;
            Map<String, String> filterMap = new HashMap<String, String>();
            filterMap.put(BaseFilter.ID, baseFilter.getId());
            filterMap.put(BaseFilter.ACTIVE, String.valueOf(baseFilter.isActive()));
            filterMap.put(BaseFilter.ORDER, String.valueOf(baseFilter.getOrder()));
            filterMap.put(BaseFilter.INTERVAL, String.valueOf(baseFilter.getInterval()));
            filterMap.put(BaseFilter.EVENT_EVENTID, baseFilter.getEventEventId());
            filterMap.put(BaseFilter.RUNBOOK, baseFilter.getRunbook());
            filterMap.put(BaseFilter.SCRIPT, baseFilter.getScript());
            filterMap.put(BaseFilter.QUEUE, instance.getQueueName());

            // subclasses populates their data
            populateGatewaySpecificFilterProperties(filterMap, filter);

            // This value comes all the way from the gateway filter
            // deployment UI.
            // This is important so RSConrtol can set the user name who
            // deployed the filter.
            if (params != null)
            {
                filterMap.put(Constants.SYS_UPDATED_BY, (String) params.get(Constants.SYS_UPDATED_BY));
            }

            content.put(filter.getId(), StringUtils.remove(StringUtils.mapToString(filterMap), "null"));
        }

        // send content to rscontrol
        // this is important as RSControl need this
        content.put("QUEUE", instance.getQueueName());

        if (MainBase.esb.sendInternalMessage(Constants.ESB_NAME_RSCONTROL, getSetFilterMethodName(), content) == false)
        {
            Log.log.warn("Failed to send filters to RSCONTROL");
        }
        
        Log.log.debug("Sent " + getSetFilterMethodName() + " message with content [" + content + "] to " + Constants.ESB_NAME_RSCONTROL);
    } // getFilters

    /**
     * Removes a set of {@link Filter}s from the current {@link Gateway}
     * instance using the paramList {@link List}.
     *
     * @param paramList
     *            a {@link List} of {@link Filter} id {@link String}s.
     */

    public void removeFilters(List<String> paramList)
    {
        if (paramList != null)
        {
            for (String id : paramList)
            {
                instance.removeFilter(id);
            }
        }

        // save config
        MainBase.main.exitSaveConfig();
    } // removeFilters

    /**
     * Removes a single {@link Filter} from the current {@link Gateway} instance
     * using the given {@link String} id.
     *
     * @param id
     *            a {@link String} id.
     */
    public void removeFilter(String id)
    {
        instance.removeFilter(id);

        // save config
        MainBase.main.exitSaveConfig();
    } // removeFilter

    /**
     * Returns a {@link HashMap} of name properties from the current
     * {@link Gateway} instance.
     *
     * @param name
     *            the id of the name property.
     */
    public Map<String, Map<String, Object>> getNameProperties(String name)
    {
        Map<String, Map<String, Object>> result = new HashMap<String, Map<String, Object>>();
        if (StringUtils.isBlank(name))
        {
            for (String key : instance.getNameProperties().keySet())
            {
                result.put(key, instance.getNameProperties().getNameProperties(key));
            }
        }
        else
        {
            result.put(name, instance.getNameProperties().getNameProperties(name));
        }
        return result;
    } // getNameProperties

    /**
     * Sets the name properties of the current {@link Gateway} instance.
     *
     * @param params
     *            a {@link Map} of name properties.
     */
    public void setNameProperties(Map params)
    {
        String name = (String) params.get(Constants.GROOVY_BINDING_NAME_PROPERTIES);
        Map prop = new Properties();
        prop.putAll(params);
        prop.remove(Constants.GROOVY_BINDING_NAME_PROPERTIES);

        instance.setNameProperties(name, prop, true);
    } // setNameProperties

    /**
     * Sets a name property in the current {@link Gateway} instance.
     *
     * @param params
     *            a {@link Map} of name properties.
     */
    public void setNameProperty(Map params)
    {
        String name = (String) params.get(Constants.GROOVY_BINDING_NAME_PROPERTIES);
        String key = (String) params.get("KEY");
        String value = (String) params.get("VALUE");

        instance.setNameProperty(name, key, value);
    } // setNameProperty

    /**
     * Removes name properties in the current {@link Gateway} instance.
     *
     * @param params
     *            a {@link Map} of name properties to remove.
     */
    public void removeNameProperties(Map params)
    {
        String name = (String) params.get(Constants.GROOVY_BINDING_NAME_PROPERTIES);

        instance.getNameProperties().removeNameProperties(name);
    } // removeNameProperties

    /**
     * Removes a name property in the current {@link Gateway} instance.
     *
     * @param params
     *            a {@link Map} of a name property to remove.
     */
    public void removeNameProperty(Map params)
    {
        String name = (String) params.get(Constants.GROOVY_BINDING_NAME_PROPERTIES);
        String key = (String) params.get("KEY");

        instance.removeNameProperty(name, key);
    } // removeNameProperty

    /**
     * This method is called from the primary gateway once some data is
     * available to be processed by workers.
     * 
     * @param params
     */
    public void receiveData(Map<String, String> params)
    {
        Log.log.debug("Message received from primary and need to process data, My GUID: " + MainBase.main.configId.getGuid());
        instance.receiveData(params);
    } // processData

    /**
     * Processes a filter in the current {@link Gateway} instance, so long that
     * the current {@link Gateway} instance is the primary Gateway
     *
     * @param params
     *            a {@link Map} of a name property to remove.
     */
    public void processFilter(Map<String, String> params)
    {
        if (instance.isPrimary())
        {
            Log.log.debug("Filter processing message received, My GUID: " + MainBase.main.configId.getGuid());
            instance.processFilter(params);
        }
    } // processData
}
