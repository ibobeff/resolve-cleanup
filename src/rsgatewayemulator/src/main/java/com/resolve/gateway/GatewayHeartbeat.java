/******************************************************************************
* (C) Copyright 2014
*
* Resolve Systems, LLC
*
* All rights reserved
* This software is distributed under license and may not be used, copied,
* modified, or distributed without the express written permission of
* Resolve Systems, LLC.
*
******************************************************************************/

package com.resolve.gateway;

import java.util.Map;

import com.resolve.rsbase.MainBase;
import com.resolve.util.Log;

public class GatewayHeartbeat extends Thread
{
    private final ClusteredGateway gateway;
    private volatile boolean active = true;
    
    public GatewayHeartbeat(ClusteredGateway gateway)
    {
        this.gateway = gateway;
    }

    public boolean isActive()
    {
        return active;
    }

    public void setActive(boolean active)
    {
        this.active = active;
    }

    @Override
    public void run()
    {
        while (isActive())
        {
            try
            {
                //send the heartbeat for failover purpose
                sendHeartbeat(false);
                //once the hearbeat is sent sleep for minimum heartbeat interval
                sleep(gateway.getHeartbeatInterval());
                checkState();
            }
            catch (InterruptedException e)
            {
                Log.log.error(e.getMessage(), e);
                break;
            }
        }
    }// end run

    private void sendHeartbeat(final boolean force)
    {
        //Only original primary needs to send heartbeat.
        if (gateway.isOriginalPrimary())
        {
            try
            {
                if (Log.log.isTraceEnabled())
                {
                    Log.log.trace(gateway.getQueueName() + " gateway: Primary=" + gateway.isPrimary());
                }
                long currentTime = System.currentTimeMillis();
                if (force || (gateway.isPrimary() && ((currentTime - gateway.getHeartbeatInterval()) > gateway.getLastHeartbeat())))
                {
                    MainBase.esb.sendMessage(gateway.getHeartbeatHeader(), gateway.getMessage());
                    if (Log.log.isTraceEnabled())
                    {
                        Log.log.trace(gateway.getQueueName() + " gateway: Sent Heartbeat.");
                    }
                    gateway.setLastHeartbeat(currentTime);
                }
                gateway.checkSystemClockReset();
            }
            catch (Exception e)
            {
                Log.log.warn(e.getMessage(), e);
            }
        }
    } // sendHeartbeat

    /**
     * This method checks the state of this gateway whether it needs to take
     * the primary role or not.
     */
    private void checkState()
    {
        //if this is just a worker gateway don't bother to check for state.
        if (gateway.isPrimary())
        {
            if (Log.log.isTraceEnabled())
            {
                Log.log.trace(gateway.getQueueName() + " gateway: Primary gateway. current - lastHeartbeat: " + (System.currentTimeMillis() - gateway.getLastHeartbeat()));
            }
            sendHeartbeat(false);
            //gateway.setLastHeartbeat();
        }
        else
        {
            //secondary setting comes from the blueprint.
            if(gateway.isSecondary())
            {
                if (Log.log.isTraceEnabled())
                {
                    Log.log.trace(gateway.getQueueName() + " gateway: Secondary gateway. current - lastHeartbeat: " + (System.currentTimeMillis() - gateway.getLastHeartbeat()));
                }
    
                // heartbeat expired
                if (System.currentTimeMillis() - gateway.getLastHeartbeat() > gateway.getFailoverInterval())
                {
                    if (!gateway.isActive())
                    {
                        Log.log.warn(gateway.getQueueName() + " gateway: Primary failover detected. Activating this secondary gateway to be Primary.");
                        gateway.reinitialize();
                    }
                }
                else
                {
                    if (gateway.isActive())
                    {
                        Log.log.info(gateway.getQueueName() + " gateway: Primary restore detected. Deactivating this secondary gateway.");
                        // close queue consumer
                        gateway.deactivate();
                    }
                }
            }
        }
    }

    public void processHeartbeatMessage(Map params)
    {
        if(gateway.isPrimary() || gateway.isSecondary())
        {
            if (Log.log.isTraceEnabled())
            {
                Log.log.trace(gateway.getQueueName() + " gateway received heartbeat message from : " + params.get("COMPONENT_ID"));
            }
            //If I was not the original primary gateway but assumed the primary responsibility then I have to check if the
            //original primary sent this heartbeat. In that case I need to self-deactivate and let the original primary take over.
            if (!gateway.isOriginalPrimary() && gateway.isPrimary())
            {
                Boolean otherPrimary = false;
                if (params.containsKey("PRIMARY"))
                {
                    otherPrimary = Boolean.valueOf((String) params.get("PRIMARY"));
                    Log.log.info(gateway.getQueueName() + " gateway: Received heartbeat from a gateway and its primary=" + otherPrimary);
                }
                //if there is other primary but I wasn't the primary originally I'll have to become secondary again so
                //the original primary can take over.
                if (otherPrimary)
                {
                    Log.log.info(gateway.getQueueName() + " gateway: Becoming secondary again and self deactivating.");
                    gateway.deactivate();
                }
            }
    
            // ignore if primary
            if (!gateway.isPrimary())
            {
                gateway.setLastHeartbeat();
            }
        }
    }
}
