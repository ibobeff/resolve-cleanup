/******************************************************************************
* (C) Copyright 2015
*
* Resolve Systems, LLC
*
* All rights reserved
* This software is distributed under license and may not be used, copied,
* modified, or distributed without the express written permission of
* Resolve Systems, LLC.
*
******************************************************************************/

package com.resolve.gateway;

/**
 * Set of constants for Gateways.
 *  
 * @author Hemant Phanasgaonkar
 * @since  5.3
 *
 */

public final class GatewayConstants
{
    private GatewayConstants() {}
    
    /** Gateway pulls data from external system. */
    public static final int PULL_TYPE_GATEWAY = 1;
    
    /** Data from external system gets pushed to Gateway. */
    public static final int PUSH_TYPE_GATEWAY = 2;
    
    /** Key/id to return when external system does not support method invoked. */
    public static final String METHOD_NOT_SUPPORTED_KEY = "METHOD_NOT_SUPPORTED";
}
