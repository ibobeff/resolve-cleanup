package com.resolve.rsgatewayemulator;

import java.io.File;
import java.io.IOException;
import java.lang.reflect.Method;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;

import javax.persistence.Column;
import javax.persistence.Table;

import org.dom4j.Element;

import com.resolve.util.Compress;
import com.resolve.util.FileUtils;
import com.resolve.util.Guid;
import com.resolve.util.XDoc;

public class ExportFilter
{
    protected String DESTINATION_FOLDER;
    protected String ZIP_FILE;
    protected String MODULE_FOLDER_LOCATION;
    protected String WIKI_FOLDER_LOCATION;
    protected String GLIDE_INSTALL_FOLDER_LOCATION;
    protected final String GLIDE_FOLDER = "update/";
    protected String gatewayName;
    private String moduleSysId;
    private String moduleName;
    private final SimpleDateFormat dateFormat = new SimpleDateFormat("yyyyMMdd-HHmmss");

    private String implPrefix;

    public static void main(String... args)
    {
        new ExportFilter().export();
    }

    @SuppressWarnings({ "rawtypes", "unchecked" })
    private void export()
    {
        try
        {
            File configFile = new File("rsgatewayemulator/config/config.xml");
            if (configFile.exists())
            {
                XDoc configDoc = new XDoc(configFile);
                String xPath = "/*/RECEIVE/*/@IMPLPREFIX";
                List implPrefixList = configDoc.getListValue(xPath);
                moduleSysId = Guid.getGuid(true);
                init();
                boolean filterFound = false;

                for (Object o : implPrefixList)
                {
                    implPrefix = o.toString();
                    String filterXPath = "//RECEIVE/" + implPrefix.toUpperCase() + "/*";
                    List<Map> filterListMap = configDoc.getListMapValue(filterXPath);

                    for (Map map : filterListMap)
                    {
                        gatewayName = map.get("ID").toString();
                        filterFound = true;
                        Map<String, String> modelFieldMap = getModelFieldsFromFilter(map);

                        writeXMLDoc(modelFieldMap);
                        writeResolveGlide();
                    }
                }
                if (filterFound)
                {
                    writeResolveModule();

                    zip();
                    cleanFolder();

                    System.out.println("Module exported: " + ZIP_FILE);
                }
                else
                {
                    System.out.println("No filter found to export.");
                }
            }
            else
            {
                System.out.println("No config.xml file found.");
                return;
            }
        }
        catch (Exception e)
        {
            e.printStackTrace();
        }
    }

    private void init()
    {
        String longTime = dateFormat.format(new Date());
        moduleName = "SDK Filter-" + longTime;
        DESTINATION_FOLDER = "rsgatewayemulator/export/";
        ZIP_FILE = DESTINATION_FOLDER + moduleName + ".zip";
        MODULE_FOLDER_LOCATION = DESTINATION_FOLDER + moduleName + "/";
        GLIDE_INSTALL_FOLDER_LOCATION = MODULE_FOLDER_LOCATION + GLIDE_FOLDER + "install/";

    }

    @SuppressWarnings("rawtypes")
    private Map<String, String> getModelFieldsFromFilter(Map filterMap) throws ClassNotFoundException
    {
        Map<String, String> result = new HashMap<String, String>();

        Set keySet = filterMap.keySet();
        Method[] methods = Class.forName("com.resolve.persistence.model." + implPrefix + "Filter").getMethods();

        if (methods != null)
        {
            for (Object o : keySet)
            {
                for (Method method : methods)
                {
                    if (method.getName().startsWith("get"))
                    {
                        String modelMethod = method.getName().substring(4);
                        if (modelMethod.equalsIgnoreCase(o.toString()))
                        {
                            Column columnAnnotation = method.getAnnotation(Column.class);
                            if (columnAnnotation != null)
                            {
                                String modelColumnName = columnAnnotation.name();
                                result.put(modelColumnName, (String) filterMap.get(o));
                            }
                        }
                    }
                }
            }
        }

        return result;
    }

    @SuppressWarnings("rawtypes")
    private void writeXMLDoc(Map columnValueMap) throws Exception
    {
        String table = getTableName(implPrefix);
        XDoc doc = new XDoc();
        Element root = doc.addRoot("record_update");
        doc.removeResolveNamespaces();
        doc.setStringValue(root, "./@table", table);
        Element update = doc.createPath(root, "./" + table);
        doc.setStringValue(update, "./@action", "INSERT_OR_UPDATE");

        doc.setStringValue(update, "./u_name", gatewayName);
        doc.setStringValue(update, "./u_active", "true");
        doc.setStringValue(update, "./u_order", "1");
        doc.setStringValue(update, "./u_interval", "10");

        Set keySet = columnValueMap.keySet();
        for (Object o : keySet)
        {
            String key = o.toString();
            String value = columnValueMap.get(key).toString();

            doc.setStringValue(update, "./" + key, value);
        }
        doc.setStringValue(update, "./u_queue", "<|Default|Queue|>");
        addSystemColumns(doc, update);
        String id = Guid.getGuid(true);
        doc.setStringValue(update, "sys_id", id);

        File file = FileUtils.getFile(GLIDE_INSTALL_FOLDER_LOCATION + File.separator + table + "_" + id + "_sdk.xml");
        doc.toFile(file);
    }

    @SuppressWarnings({ "rawtypes", "unchecked" })
    private static String getTableName(String implPrefix) throws ClassNotFoundException
    {
        String tableName = "";

        Class clazz = Class.forName("com.resolve.persistence.model." + implPrefix + "Filter");
        Table t = (Table) clazz.getAnnotation(Table.class);
        tableName = t.name();

        return tableName;
    }

    private void writeResolveGlide() throws Exception
    {
        String table = "resolve_impex_glide";
        XDoc doc = new XDoc();
        Element root = doc.addRoot("record_update");
        doc.removeResolveNamespaces();
        doc.setStringValue(root, "./@table", table);
        Element update = doc.createPath(root, "./" + table);
        doc.setStringValue(update, "./@action", "INSERT_OR_UPDATE");
        doc.setStringValue(update, "./u_description", "{\"uname\":\"" + gatewayName + "\"}");
        doc.setStringValue(update, "./u_module_name", implPrefix + gatewayName);
        doc.setStringValue(update, "./u_type", implPrefix + "Filter");
        doc.setStringValue(update, "./u_module", moduleSysId);
        addSystemColumns(doc, update);
        String id = Guid.getGuid(true);
        doc.setStringValue(update, "sys_id", id);

        File file = FileUtils.getFile(GLIDE_INSTALL_FOLDER_LOCATION + File.separator + table + "_" + id + ".xml");
        doc.toFile(file);
    }

    private void writeResolveModule() throws Exception
    {
        String table = "resolve_impex_module";
        XDoc doc = new XDoc();
        Element root = doc.addRoot("record_update");
        doc.removeResolveNamespaces();
        doc.setStringValue(root, "./@table", table);
        Element update = doc.createPath(root, "./" + table);
        doc.setStringValue(update, "./@action", "INSERT_OR_UPDATE");
        doc.setStringValue(update, "./u_name", moduleName);
        doc.setStringValue(update, "./u_version", "5.3.0");

        addSystemColumns(doc, update);

        doc.setStringValue(update, "sys_id", moduleSysId);

        File file = FileUtils.getFile(GLIDE_INSTALL_FOLDER_LOCATION + File.separator + table + "_" + moduleSysId + ".xml");
        doc.toFile(file);
    }

    private void zip()
    {
        Compress compress = new Compress();
        compress.zip(FileUtils.getFile(MODULE_FOLDER_LOCATION), FileUtils.getFile(ZIP_FILE));
    }

    private void cleanFolder() throws IOException
    {
        File file = new File(MODULE_FOLDER_LOCATION);
        FileUtils.deleteDirectory(file);
    }

    private void addSystemColumns(XDoc doc, Element update)
    {
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        Date now = new Date();
        String date = sdf.format(now);

        doc.setStringValue(update, "./sys_created_on", date);
        doc.setStringValue(update, "./sys_updated_on", date);
        doc.setStringValue(update, "./sys_created_by", "admin");
        doc.setStringValue(update, "./sys_updated_by", "admin");
    }
}
