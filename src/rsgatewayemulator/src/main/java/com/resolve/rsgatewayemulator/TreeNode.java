package com.resolve.rsgatewayemulator;

import java.util.HashMap;

public class TreeNode{
    String path;
    boolean isLeaf=false;
    HashMap<String, TreeNode> children = new HashMap<String, TreeNode>();
    String jarName;

    public TreeNode(String path, boolean isLeaf, String jarName)
    {
        super();
        this.path = path;
        this.isLeaf = isLeaf;
        this.jarName=jarName;
    }
    
    public TreeNode(String path, boolean isLeaf)
    {
        super();
        this.path = path;
        this.isLeaf = isLeaf;
    }

    public TreeNode()
    {
        super();
        path= new String("");
        isLeaf=false;
    }

    public void addNode(String path, String separate, String jarName){
        String[] paths = path.split(separate);
        TreeNode cur=this;
        for(int i=0;i<paths.length;i++){
            if(cur.findChild(paths[i])){
                cur=cur.getChild(paths[i]);
            }
            else{
                if(i==paths.length-1){
                    cur.addChild(paths[i], true, jarName);
                }
                else{
                    cur.addChild(paths[i], false);
                }
                cur=cur.getChild(paths[i]);
            }
        }
    }

    private void addChild(String path, boolean isLeaf)
    {
        children.put(path, new TreeNode(path, isLeaf));
    }

    private void addChild(String path, boolean isLeaf, String jarName)
    {
        children.put(path, new TreeNode(path, isLeaf, jarName));
    }
    
    private TreeNode getChild(String p)
    {
        return children.get(p);
    }

    private boolean findChild(String p)
    {
        return children.containsKey(p);
    }

    public String getLeafNode(String path, String separate)
    {
        String[] paths = path.split(separate);
        TreeNode cur=this;
        for(int i=0;i<paths.length;i++){
            if(cur.findChild(paths[i])){
                cur=cur.getChild(paths[i]);
            }
            else{
                return null;
            }
        }
        if(cur.isLeaf){
            return cur.jarName;
        }
        else{
            return null;
        }
    }
}