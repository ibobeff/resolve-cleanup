
/******************************************************************************
* (C) Copyright 2016
*
* Resolve Systems, LLC
*
* All rights reserved
* This software is distributed under license and may not be used, copied,
* modified, or distributed without the express written permission of
* Resolve Systems, LLC.
*
******************************************************************************/

package com.resolve.rsgatewayemulator;

public class Release extends com.resolve.rsbase.Release
{
    public String supportedResolveVersions;

    public Release(String service)
    {
        super();

        name = "RSGatewayEmulator";
        type = "RSGATEWAYEMULATOR";
        if (service == null || service.equals(""))
        {
            service = "rsgatewayemulator";
        }
        serviceName = service;
        version = "1.0 Beta";
        year = "2018";
        copyright = "(C) Copyright 2018 Resolve Systems, LLC";
        supportedResolveVersions = "BlueMoon(5.3), Corona(5.4)";
    } // Release

    public String getSupportedResolveVersions()
    {
        return supportedResolveVersions;
    }

    public void setSupportedResolveVersions(String supportedResolveVersions)
    {
        this.supportedResolveVersions = supportedResolveVersions;
    }

    public String toString()
    {
        return super.toString() +
               "Supported Resolve Versions: " + supportedResolveVersions + "\n";
    }

} // Release
        