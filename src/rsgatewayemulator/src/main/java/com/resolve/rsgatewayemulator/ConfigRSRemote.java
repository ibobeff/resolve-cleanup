/******************************************************************************
* (C) Copyright 2014
*
* Resolve Systems, LLC
*
* All rights reserved
* This software is distributed under license and may not be used, copied,
* modified, or distributed without the express written permission of
* Resolve Systems, LLC.
*
******************************************************************************/

package com.resolve.rsgatewayemulator;

import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import com.resolve.util.CryptUtils;
import com.resolve.util.Log;
import com.resolve.util.Properties;
import com.resolve.util.StringUtils;

public class ConfigRSRemote
{
    private String instanceName = "rsremote";
    
    private Properties rsremoteProperties;
    private Properties runProperties;
    private Properties logProperties;

    public ConfigRSRemote()
    {
        this.rsremoteProperties = new Properties();
        this.runProperties = new Properties();
        this.logProperties = new Properties();
    } // ConfigRSRemote
    
    public ConfigRSRemote(Properties properties) throws Exception
    {
        this(properties, null);
    } // ConfigRSRemote
    
    public ConfigRSRemote(Properties properties, String instanceName) throws Exception
    {
        this();
        if (instanceName != null)
        {
	        this.instanceName = instanceName;
        }
        for (Object key : properties.keySet())
        {
            String strKey = key.toString();
            if (strKey.startsWith(instanceName + ".") && !strKey.contains(".instance"))
            {
                String value = properties.getProperty(strKey).trim();
                if (value.contains("${"))
                {
                    value = properties.getReplacement(key).trim();
                }
                if (value.startsWith("ENC"))
                {
                    try
                    {
                        value = CryptUtils.decrypt(value);
                    }
                    catch (Exception e)
                    {
                        Log.log.error("Failed to decrypt blueprint value", e);
                        value = "";
                    }
                }
                if (strKey.toUpperCase().contains("PASS"))
                {
                    if (StringUtils.isNotEmpty(value) && !value.startsWith("NO_ENC:") && !value.matches(".*(?<!\\\\)\\$\\{.*\\}.*"))
                    {
                        value = CryptUtils.encrypt(value);
                    }
                }
                if (value.startsWith("TO_ENC:"))
                {
                    value = CryptUtils.encrypt(value.substring(7));
                }
                if (value.startsWith("NO_ENC:"))
                {
                    value = value.substring(7);
                }
                if (strKey.contains("brokeraddr") && value.matches(":\\d*"))
                {
                    value = "";
                }
                if (value != null)
                {
	                if (strKey.startsWith(instanceName + ".run"))
	                {
	                    this.runProperties.put(key, value);
	                }
	                else if (strKey.startsWith(instanceName + ".log4j"))
	                {
	                    this.logProperties.put(key, value);
	                }
	                else
	                {
	                    if( (!strKey.contains(".sdkfield") || strKey.matches(".*.sdkfield[\\d].name")) && !strKey.contains(".sdkdatatype"))
	                    {
	                        this.rsremoteProperties.put(key, value);
	                    }
	                }
                }
            }
        }
    } // ConfigRSRemote
    
    @SuppressWarnings("unchecked")
    public Map<String, Object> rsremoteXPathValues()
    {
        Map<String, Object> result = new HashMap<String, Object>();
        
        for (Object key : rsremoteProperties.keySet())
        {
            String strKey = key.toString();
            String value = (String) rsremoteProperties.get(key);
            
            strKey = strKey.replaceFirst(instanceName, "").toUpperCase();
            strKey = strKey.replaceAll("(?<!\\\\)\\.", "/");
            strKey = strKey.replaceAll("\\\\\\.", ".");
            
            Pattern pattern = Pattern.compile("(.+)/(.+)/(\\d+)");
            Matcher matcher = pattern.matcher(strKey);
            if (matcher.matches())
            {
                strKey = "." + matcher.group(1);
                String xdocKey = matcher.group(2);
                int listPosition = Integer.parseInt(matcher.group(3))-1;

                List<Map<String, String>> list = (List<Map<String, String>>) result.get(strKey);
                if (list == null)
                {
                    list = new LinkedList<Map<String, String>>();
                    result.put(strKey, list);
                }
                for (int j=list.size(); j<=listPosition; j++)
                {
                    list.add(new HashMap<String, String>());
                }
                Map<String, String> entry = list.get(listPosition);
                entry.put(xdocKey, value);
                list.set(listPosition, entry);
            }
            else
            {
                strKey = strKey.replaceFirst("^(.*)/(.*?)$", ".$1/@$2");
                result.put(strKey, value);
            }
        }
        
        return result;
    } //rsremoteXPathValues()
}//ConfigRSRemote
