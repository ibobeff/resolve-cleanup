package com.resolve.rsgatewayemulator;

import java.io.File;
import java.io.FileInputStream;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.dom4j.Node;

import com.resolve.rsremote.ConfigENC;
import com.resolve.util.FileUtils;
import com.resolve.util.Log;
import com.resolve.util.Properties;
import com.resolve.util.XDoc;

public class BlueprintConfig
{
    public String blueprintFileName = "blueprint.properties";
    public Properties blueprintProperties = new Properties();
    public Map<String, ConfigRSRemote> configRSRemotes;
    public Set<String> implPrefixes = new HashSet<String>();
    public final String configFilename = "config/config.xml";
    
    public static void main(String[] args)
    {
        BlueprintConfig config = new BlueprintConfig();
        config.init();
    }
    
    @SuppressWarnings("unchecked")
    private void init()
    {
        try
        {
            ConfigENC.load();
            Log.init("rsgatewayemulator/config/log.cfg", "rsgatewayemulator");
            File blueprintFile = FileUtils.getFile("rsgatewayemulator/" + blueprintFileName);
            if (blueprintFile.exists())
            {
                FileInputStream fis = new FileInputStream(blueprintFile);
                blueprintProperties.load(fis);
                fis.close();
            }
            else
            {
                System.out.println("blueprint.properties file not found");
                return;
            }
            
            configRSRemotes = new HashMap<String, ConfigRSRemote>();
                    
            ConfigRSRemote configRSRemote = new ConfigRSRemote(blueprintProperties, "rsremote");
            if (configRSRemote != null)
            {
                //Configure config.xml
                File configFile = new File("rsgatewayemulator/" + configFilename);
                if (configFile.exists())
                {
                    Map<String, Object> configXPath = configRSRemote.rsremoteXPathValues();
                    XDoc configDoc = new XDoc(configFile);
                    
                    configDoc.removeElements("./ESB/QUEUE");
                    // SDK gateways configured in blueprint.properties
                    List<String> configuredSDKGtws = new ArrayList<String>();
                    List<String> filterKeyPrefixList = new ArrayList<String>();
                    for (String key : configXPath.keySet())
                    {
                        Object value = configXPath.get(key);
                        
                        if (key.endsWith("/@IMPLPREFIX"))
                        {
                            int endIndx = key.indexOf("/@IMPLPREFIX");
                            String keyPrefix = key.substring(0,endIndx);
                            int beginIndx = keyPrefix.lastIndexOf("/");
                            
                            if (beginIndx != -1)
                            {
                                String sdkGtwName = keyPrefix.substring(beginIndx + 1);
                                
                                if (!configuredSDKGtws.contains(sdkGtwName))
                                {
                                    configuredSDKGtws.add(sdkGtwName);
                                }
                            }
                        }
                        
                        if (value instanceof String)
                        {
                            if (key.contains("SDKFIELD"))
                            {
                                String filterKeyPrefix = key.substring(0, key.indexOf("SDKFIELD"));
                                if (!filterKeyPrefixList.contains(filterKeyPrefix))
                                {
                                    filterKeyPrefixList.add(filterKeyPrefix);
                                }
                                key = filterKeyPrefix + "FILTER/@" + ((String)value).toUpperCase();
                                configDoc.setStringValue(key, "");
                            }
                            else
                            {
                                configDoc.setStringValue(key, (String)value);
                            }
                        }
                        else if (value instanceof List)
                        {
                            List<Object> listValue = (List<Object>)value;
                            
                            if (!listValue.isEmpty() && listValue.get(0) instanceof Map)
                            {
                                configDoc.setListMapValue(key, listValue);
                            }
                            else if (!listValue.isEmpty() && listValue.get(0) instanceof String)
                            {
                                configDoc.setListValue(key, listValue);
                            }
                        }
                    }
                    
                    for (String filterKeyPrefixString : filterKeyPrefixList)
                    {
                        configDoc.setStringValue(filterKeyPrefixString + "FILTER/@SCRIPT", "");
                        configDoc.setStringValue(filterKeyPrefixString + "FILTER/@EVENT_EVENTID", "");
                        configDoc.setStringValue(filterKeyPrefixString + "FILTER/@INTERVAL", "10");
                        configDoc.setStringValue(filterKeyPrefixString + "FILTER/@RUNBOOK", "");
                        configDoc.setStringValue(filterKeyPrefixString + "FILTER/@ACTIVE", "true");
                        configDoc.setStringValue(filterKeyPrefixString + "FILTER/@ORDER", "1");
                        configDoc.setStringValue(filterKeyPrefixString + "FILTER/@ID", "");
                    }
                    
                    List<Node> sdkGtwElements = configDoc.getNodes("/CONFIGURATION/RECEIVE/*[@IMPLPREFIX]");
                    List<String> sdkGtwElmNamesToPurge = new ArrayList<String>();
                    
                    for (Node sdkGtwElement : sdkGtwElements)
                    {
                        if (!configuredSDKGtws.contains(sdkGtwElement.getName()))
                        {
                            sdkGtwElmNamesToPurge.add(sdkGtwElement.getName());
                        }
                    }
                    
                    if (!sdkGtwElmNamesToPurge.isEmpty())
                    {
                        for (String sdkGtwElmNameToPurge : sdkGtwElmNamesToPurge)
                        {
                            System.out.println( "Removing ./RECEIVE/" + sdkGtwElmNameToPurge + " from config.xml...");
                            configDoc.removeElement("./RECEIVE/" + sdkGtwElmNameToPurge);
                        }
                    }
                    
                    configDoc.toPrettyFile(configFile);
                }
                else
                {
                    System.out.println("config.xml file not found");
                    return;
                }
            }
            
            System.out.println("config.xml populated.");
        }
        catch(Exception e)
        {
            e.printStackTrace();
        }
    }
}
