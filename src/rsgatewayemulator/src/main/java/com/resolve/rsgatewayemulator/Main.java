/******************************************************************************
* (C) Copyright 2014
*
* Resolve Systems, LLC
*
* All rights reserved
* This software is distributed under license and may not be used, copied,
* modified, or distributed without the express written permission of
* Resolve Systems, LLC.
*
******************************************************************************/

package com.resolve.rsgatewayemulator;

import japa.parser.ParseException;

import java.io.File;
import java.io.IOException;
import java.lang.annotation.Annotation;
import java.lang.reflect.Constructor;
import java.lang.reflect.Field;
import java.lang.reflect.Method;
import java.lang.reflect.Modifier;
import java.net.MalformedURLException;
import java.util.Arrays;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.concurrent.ConcurrentHashMap;

import javax.persistence.Column;
import javax.persistence.Lob;

import org.apache.log4j.Logger;
import org.dom4j.Element;

import com.resolve.rsbase.MainBase;
import com.resolve.codeGenerate.ConfigData;
import com.resolve.codeGenerate.GenerateBasedTemp;
import com.resolve.codeGenerate.GenerateSDK;
import com.resolve.gateway.BaseClusteredGateway;
import com.resolve.gateway.Filter;
import com.resolve.gateway.MGateway;
import com.resolve.persistence.model.GatewayFilter;
import com.resolve.rsremote.ConfigENC;
import com.resolve.rsremote.ConfigGeneral;
import com.resolve.rsremote.ConfigReceiveGateway;
import com.resolve.rsremote.ConfigRegistration;
import com.resolve.rsremote.ExecuteMain;
import com.resolve.services.hibernate.vo.GatewayFilterVO;
import com.resolve.util.Constants;
import com.resolve.util.ERR;
import com.resolve.util.Log;
import com.resolve.util.MappingAnnotation;
import com.resolve.util.StringUtils;
import com.resolve.util.XDoc;

import com.resolve.codeGenerate.GatewayData;

public class Main extends MainBase
{
    final static int CHECKOUT_INTERVAL = 1;
    final static int UPDATE_INTERVAL = 6;
    private static Logger log = Logger.getLogger(Main.class);
    
    ConfigRegistration configRegistration;
    private Map<String, String> sdkDvlpdImplPrefxToPkg = new HashMap<String, String>();
    private final static Set<String> reservedUFilterFields = new HashSet<String>(Arrays.asList("UQueue",
                    "UOrder",
                    "UEventEventId",
                    "URunbook",
                    "UActive",
                    "UName",
                    "UInterval",
                    "UScript"));
    private Map<String, ConfigReceiveGateway> sdkdDvlpdCfgRcvs = new HashMap<String, ConfigReceiveGateway>();
    
    ExecuteUnitTest t = new ExecuteUnitTest();
    static boolean unitTestOnly = true;
    static boolean unitTestNewSDK = false;

    /*
     * Main execution method for RSRemote
     */
    public static void main(String[] args) throws IOException, ParseException
    {
/*        System.out.println(args.length);
        for(String str : args){
            System.out.println(str);
        }*/
        String serviceName = new String("");
        String helpMessage = new String("Usage: [-GT <ProductName> <GateWay(PUSH, PULL, MSG)>] Generate Code based on provided blueprint files (Preffered)\n"
                                        + "     [-GC] Generate Code based on older blueprint files \n"
                                        + "     [-UT] Execute the API Unit Test, if any \n"
                                        + "     [-SE] Simulator Execution of gateway, also go through filter test, if any \n");
        
        if(args.length<2){
            System.out.println(helpMessage);
        }
        else if(args[1].equalsIgnoreCase("-GC")){
            System.out.println("Based on property file rsgatewayemulator"+File.separator+"blueprint.properties");
            List<GatewayData> data = ConfigData.initDataFromProp(ConfigData.loadProp("rsgatewayemulator"+File.separator+"blueprint.properties"));
//            List<GatewayData> data = ConfigData.initDataFromProp(ConfigData.loadProp("blueprint.properties"));
            if(data==null) {
                System.out.println("Re-check your blueprint setting");
                return;
            }
            File dir = new File("rsgatewayemulator"+File.separator+"templateProject");
            
            for(GatewayData d : data){
                GenerateBasedTemp.generateSourceCode(dir, d);
            }
        }
        else if(args[1].equalsIgnoreCase("-GT")){
                if(args.length < 4) {
                    System.out.println("Usage: [-GT <ProductName> <GateWay(PUSH, PULL, MSG)>]");
                    return;
                }
                String productName = args[2];
                String gwType = args[3];
                if(!gwType.equalsIgnoreCase(GatewayData.GATEWAY_TYPE.MSG.toString()) &&
                       !gwType.equalsIgnoreCase(GatewayData.GATEWAY_TYPE.PULL.toString()) &&
                        !gwType.equalsIgnoreCase(GatewayData.GATEWAY_TYPE.PUSH.toString())) {
                    System.out.println("Gateway can only be PUSH, PULL or MSG");
                    return;
                }
                gwType = gwType.toUpperCase();
                String file = "blueprint.properties";
                
                // Auto-generated method stub
                System.out.println("Based on property file rsgatewayemulator"+File.separator+ gwType+ "_blueprint.properties");
                List<GatewayData> gatewayData = ConfigData.initDataFromProp(ConfigData.loadProp("rsgatewayemulator"+File.separator+ gwType+ "_blueprint.properties"));
                if(gatewayData==null) {
                    System.out.println("Re-check your blueprint setting. Gateway data incorrect or not found");
                    return;
                }
    //            List<String> genProjProps = initDataFromProp(loadProp("sdk.properties"));
                File dir = new File("rsgatewayemulator" + File.separator + "templateprojectredesigned");
                GenerateSDK sdkGen = new GenerateSDK();
                try {
                    for(GatewayData gdata : gatewayData){
                        gdata.setName(productName);
                        gdata.setType(gwType);
                        sdkGen.generateSourceFile(dir, gdata);
                        System.out.println("=======================================================");
                        System.out.println("Project " + productName + ", Type " +  gwType + " was successfully created at --> " + 
                                        dir.getAbsolutePath().replace("templateprojectredesigned", "connector"));
                        System.out.println("=======================================================");
                    }
                    
                }catch(Exception ex) {
                    System.out.println("exception when creating project, Check the stack trace below or check bluepriint.properties file");
                    ex.printStackTrace();
                }
        }
        else if(args[1].equalsIgnoreCase("-UT")){
            try
            {
                //If the -UT is provided with a "-NW" option run the unit test for the new SDKs
                if(args.length < 2) {
                    unitTestOnly = true;
                    unitTestNewSDK = false;
                }else if(args[2].equalsIgnoreCase("NW")){
                    unitTestOnly = false;
                    unitTestNewSDK = true;
                }else {
                    System.out.println("If you are trying to run the new sdk unit test please say -UT -NW");
                    log.error("-UT was ran with the wrong unit test args ");
                    return;
                }
                if (args.length > 0)
                {
                    serviceName = args[0];
                }
                
                Main main = new Main();
                main.init(serviceName);
                
            }
            catch (Throwable t)
            {
                t.printStackTrace();
                Log.alert(ERR.E10005, t);

                System.exit(0);
            }
        }
        else if(args[1].equalsIgnoreCase("-SE")){
            try
            {
                unitTestOnly = false;
                unitTestNewSDK = false;
                if (args.length > 0)
                {
                    serviceName = args[0];
                }
                
                Main main = new Main();
                main.init(serviceName);
                
            }
            catch (Throwable t)
            {
                t.printStackTrace();
                Log.alert(ERR.E10005, t);

                System.exit(0);
            }
        }
        else{
            System.out.println(helpMessage);
        }
/*        Scanner scanner = new Scanner(System.in);
        int option=0;
        do{
            System.out.println("Enter your option: ");
            System.out.println("1 Generate code based on your setting in blueprint ");
            System.out.println("2 Execute API unit test suite ");
            System.out.println("3 Emulating gateway execution with filters ");
            System.out.println("4 Quit ");
            try{
                option = new Integer(scanner.next()).intValue();
            }
            catch(NumberFormatException e){
                option = 0;
                System.out.println("Please input correct option ");
            }
            String serviceName = new String("");
            switch(option){
                case 1: 
                   // System.out.println(new java.io.File( "." ).getCanonicalPath());
                    List<GatewayData> data = ConfigData.initDataFromProp(ConfigData.loadProp("rsgatewayemulator"+File.separator+"blueprint.properties"));
                    if(data==null) {
                        option = 0;
                        break;
                    }
                    File dir = new File("rsgatewayemulator"+File.separator+"templateProject");
                    
                    for(GatewayData d : data){
                        GenerateBasedTemp.generateSourceCode(dir, d);
                    }
                    
                    break;
                case 2:
                    try
                    {
                        unitTestOnly = true;
                        if (args.length > 0)
                        {
                            serviceName = args[0];
                        }
                        
                        Main main = new Main();
                        main.init(serviceName);
                        
                    }
                    catch (Throwable t)
                    {
                        t.printStackTrace();
                        Log.alert(ERR.E10005, t);

                        System.exit(0);
                    }
                    break;
                case 3:
                    try
                    {
                        unitTestOnly = false;
                        if (args.length > 0)
                        {
                            serviceName = args[0];
                        }
                        
                        Main main = new Main();
                        main.init(serviceName);
                        
                    }
                    catch (Throwable t)
                    {
                        t.printStackTrace();
                        Log.alert(ERR.E10005, t);

                        System.exit(0);
                    }
                    break;
                case 4:
                    break;
                default:
                        
            }
        }while(option==0);*/
        
    } // main

    public void init(String serviceName) throws Exception
    {
        Release rsgtwemuRelease = new Release(serviceName);
        
        // init release information
        initRelease(rsgtwemuRelease);
        
        // init logging
        initLog(rsgtwemuRelease);
        
        // init enc
        initENC();

        // init signal handlers
        initShutdownHandler();

        // init configuration
        initConfigFile();
        initConfig();

        // init startup
        initStartup();

        // init timezone
        initTimezone();

        // display started message and set MListener to active
        initServiceComplete();
        
        // start receiver
        startReceivers();

    } // init

    /*
     * NOTE: String args[] is required to catch NT service shutdown message
     */
    protected void exit(String[] argv)
    {
        System.exit(0);
    } // exit

    protected void terminate()
    {
        for (String implPrefix : sdkDvlpdImplPrefxToPkg.keySet())
        {
            try
            {
                t.clearDataForFilterTest(sdkDvlpdImplPrefxToPkg.get(implPrefix));
            }
            catch (MalformedURLException e)
            {
                Log.log.error(e.getMessage(), e);
            }
        }
        // shutdown
        isShutdown = true;

        Log.log.warn("Terminating " + release.name.toUpperCase());
        
        try
        {
            // wait for completion or timeout
            ExecuteMain.waitForActiveCompletion();
        }
        catch (Exception e)
        {
            Log.log.error("Failed to terminate RSGatewayEmulator: " + e.getMessage(), e);
        }
        
        try
        {
            // stop gateways
            Log.log.info("Stopping receivers");
            stopReceivers();
        }
        catch (Exception e)
        {
            Log.log.error("Failed to terminate RSGatewayEmulator: " + e.getMessage(), e);
        }

        
        for (String implPrefix : sdkdDvlpdCfgRcvs.keySet())
        {
            try
            {
                Method saveConfigMethod = sdkdDvlpdCfgRcvs.get(implPrefix).getClass().getDeclaredMethod("save", (Class<?>[])null);            
                saveConfigMethod.invoke(sdkdDvlpdCfgRcvs.get(implPrefix), (Object[])null);
            }
            catch(Exception e)
            {
                Log.log.error("Failed to save configuration for " + implPrefix, e);
            }
        }
        
        
        try
        {
            // save config
            exitSaveConfig();

            // terminate
            super.terminateService();
        }
        catch (Exception e)
        {
            Log.log.error("Failed to terminate RSGatewayEmulator: " + e.getMessage(), e);
        }
        Log.log.warn("Terminated " + release.name.toUpperCase());
    } // terminate

    protected void loadConfig(XDoc configDoc) throws Exception
    {
        // override and additional configX.load
        configGeneral = new ConfigGeneral(configDoc);
        configGeneral.load();
        setClusterModeProperty(configGeneral.getClusterName() + "." + Constants.CLUSTER_MODE);

        configRegistration = new ConfigRegistration(configDoc);
        configRegistration.load();
        
        loadSDKDevelopedGateways(configDoc);
        
        super.loadConfig(configDoc);
    } // loadConfig
    
    protected void initENC() throws Exception
    {
        // load configuration state and initialization overrides
        Log.log.info("Loading configuration settings for enc");
        loadENCConfig();

    } // initENC

    protected void loadENCConfig() throws Exception
    {
        ConfigENC.load();
    }
    
    private void loadSDKDevelopedGateways(XDoc configDoc) throws Exception
    {
        sdkDvlpdImplPrefxToPkg = getSDKDevelopedGateways(configDoc);
        
        for (String implPrefix : sdkDvlpdImplPrefxToPkg.keySet())
        {
            try
            {
                validatedateSDKDvlpdGtwImpl(implPrefix, sdkDvlpdImplPrefxToPkg.get(implPrefix));
                try
                {
                    ConfigReceiveGateway cfgReceiveGtwInst = loadSDKDevelopedGateway(sdkDvlpdImplPrefxToPkg.get(implPrefix) + ".ConfigReceive" + implPrefix, configDoc);
                    sdkdDvlpdCfgRcvs.put(implPrefix, cfgReceiveGtwInst);
                }
                catch (Exception e)
                {
                    Log.log.warn("Error loading " + sdkDvlpdImplPrefxToPkg.get(implPrefix) + ".ConfigReceive" + implPrefix, e);
                }
            }
            catch (Exception ve)
            {
                Log.log.warn("Failed to Validate SDK Developed " + sdkDvlpdImplPrefxToPkg.get(implPrefix) + "." + implPrefix + " Gateway Implementation." , ve);
            }
        }
    }
    
    @SuppressWarnings("unchecked")
    private ConfigReceiveGateway loadSDKDevelopedGateway(String gtwClassName, XDoc configDoc) throws Exception
    {
        Class<? extends ConfigReceiveGateway> cfgReceiveGtwClass = 
            (Class<? extends ConfigReceiveGateway>) Class.forName(gtwClassName, false, this.getClass().getClassLoader());
        
        
        Constructor<? extends ConfigReceiveGateway> cfgReceiveGtwConstructor = cfgReceiveGtwClass.getDeclaredConstructor(configDoc.getClass());
        
        ConfigReceiveGateway cfgReceiveGtwInst = cfgReceiveGtwConstructor.newInstance(configDoc);
        cfgReceiveGtwInst.load();
        
        return cfgReceiveGtwInst;
    }
    
    private void validatedateSDKDvlpdGtwImpl(String implPrefix, String implPackage) throws Exception
    {
        validateConfigReceiveImpl(implPackage + ".ConfigReceive" + implPrefix);
        Map<String, Class<?>> getterToRtnType = validateFilterVOImpl("com.resolve.services.hibernate.vo." + implPrefix + "FilterVO");
        validateFilterModelImpl("com.resolve.persistence.model." + implPrefix + "Filter", 
                                "com.resolve.services.hibernate.vo." + implPrefix + "FilterVO",
                                getterToRtnType);
        validateGtwMsgHandlerImpl("com.resolve.gateway." + "M" + implPrefix);
        validateGtwRSCtrlMsgHandlerImpl("com.resolve.rscontrol." + "M" + implPrefix);
        validateGtwImpl(implPackage + "." + implPrefix + "Gateway", implPrefix, implPackage);
    }
    
    @SuppressWarnings("unchecked")
    private void validateConfigReceiveImpl(String cfgReceiveGtwClassName) throws Exception
    {
        Class<? extends ConfigReceiveGateway> cfgReceiveGtwClass = null;
        
        try
        {
            cfgReceiveGtwClass = 
                (Class<? extends ConfigReceiveGateway>) Class.forName(cfgReceiveGtwClassName, false, 
                                                                      this.getClass().getClassLoader());
        }
        catch(Exception e)
        {
            Log.log.warn("Failed to load " + cfgReceiveGtwClassName + ".", e);
            throw e;
        }
        
        // Constructor with XDoc as parameter
        
        try
        {
            cfgReceiveGtwClass.getDeclaredConstructor(XDoc.class);
        }
        catch (Exception e)
        {
            Log.log.warn("Expected constructor " + cfgReceiveGtwClassName + "(" + XDoc.class.getName() + ") not found", e);
            throw e;
        }
        
        // Methods getRootNode(), load() and save()
        
        try
        {
           cfgReceiveGtwClass.getDeclaredMethod("getRootNode", (Class<?>[])null);
        }
        catch (Exception e)
        {
            Log.log.warn("Expected method getRootNode() returning String not found in " + cfgReceiveGtwClassName + ".", e);
            throw e;
        }
        
        try
        {
            cfgReceiveGtwClass.getDeclaredMethod("load", (Class<?>[])null);
        }
        catch (Exception e)
        {
            Log.log.warn("Expected method load() not found in " + cfgReceiveGtwClassName + ".", e);
            throw e;
        }
        
        try
        {
            cfgReceiveGtwClass.getMethod("save", (Class<?>[])null);
        }
        catch (Exception e)
        {
            Log.log.warn("Expected method save() not found in " + cfgReceiveGtwClassName + ".", e);
            throw e;
        }
    }
    
    @SuppressWarnings("unchecked")
    private Map<String, Class<?>> validateFilterVOImpl(String gtwFilterVOClassName) throws Exception
    {
        Class<? extends GatewayFilterVO> gtwFilterVOClass = null;
        Map<String, Class<?>> getterToRtnType = new HashMap<String, Class<?>>();
        
        // Load com.resolve.services.hibernate.vo.<implprefix>FilterVO CLass
        
        try
        {
            gtwFilterVOClass = (Class<? extends GatewayFilterVO>) Class.forName(gtwFilterVOClassName, 
                                                                                false, 
                                                                                this.getClass().getClassLoader());
        }
        catch(Exception e)
        {
            Log.log.warn("Failed to load " + gtwFilterVOClassName + ".", e);
            throw e;
        }
        
        // Identify and validate U<xxxx> Filter Fields and getU<xxxx> public methods with @MappingAnnotation
        
        Field[] userFields = null;
        
        try
        {
            userFields = gtwFilterVOClass.getDeclaredFields();
            
            for (int i = 0; i < userFields.length; i++)
            {
                Field field = userFields[i];
                
                int fldModifiers = field.getModifiers();
                
                // field is not Static and not Final
                
                if (!Modifier.isStatic(fldModifiers) && !Modifier.isFinal(fldModifiers))
                {
                    if (!field.getName().startsWith("U"))
                    {
                        Log.log.warn("User declared field " + field.getName() + " in " + gtwFilterVOClassName + " does not start with U.");
                        throw new Exception ("User declared field " + field.getName() + " in " + gtwFilterVOClassName + " does not start with U.");
                    }
                    
                    if (reservedUFilterFields.contains(field.getName()))
                    {
                        Log.log.warn("User declared field name " + field.getName() + " is reserved and should not be used in " + gtwFilterVOClassName + ".");
                        throw new Exception ("User declared field name " + field.getName() + " is reserved and should not be used in " + gtwFilterVOClassName + ".");
                    }
                    
                    // Check if method getU<xxxx> is annotaed with @MappingAnnotation
                    
                    Method getMethod = null;
                    
                    try
                    {
                        getMethod = gtwFilterVOClass.getDeclaredMethod("get" + field.getName(), (Class<?>[])null);
                    }
                    catch(Exception mnfe)
                    {
                        Log.log.info("User declared field " + field.getName() + " in " + gtwFilterVOClassName + " does not have corresponding getter method.");
                    }
                    
                    if (getMethod != null && Modifier.isPublic(getMethod.getModifiers()) && 
                        getMethod.getAnnotation(MappingAnnotation.class) != null)
                    {
                        getterToRtnType.put("get" + field.getName(), getMethod.getReturnType());
                    }
                }
            }
        }
        catch(Exception gdfe)
        {
            Log.log.warn("Failed to get declared fields in " + gtwFilterVOClassName + ".", gdfe);
            throw gdfe;
        }
        
        if (!getterToRtnType.isEmpty())
        {
            Log.log.info(gtwFilterVOClassName + " has " + getterToRtnType.size() + " user defined private, non Static, non Final Fields." );
        }
        else
        {
            Log.log.warn(gtwFilterVOClassName + " does not have any user defined non Static, non Final Fields.");
        }
                    
        return getterToRtnType;
    }
    
    @SuppressWarnings("unchecked")
    private void validateFilterModelImpl(String gtwFilterModelClassName, String gtwFilterVOClassName,
                                         Map<String, Class<?>> getterToRtnType) throws Exception
    {
        Class<? extends GatewayFilter<? extends GatewayFilterVO>> gtwFilterModelClass = null;
        
        // Load com.resolve.persistence.model.<implprefix>Filter CLass
        
        try
        {
            gtwFilterModelClass = 
                (Class<? extends GatewayFilter<? extends GatewayFilterVO>>)Class.forName(gtwFilterModelClassName, 
                                                                                         false, this.getClass().getClassLoader());
        }
        catch(Exception e)
        {
            Log.log.warn("Failed to load " + gtwFilterModelClassName + ".", e);
            throw e;
        }
        
        // Methods doGetVO(), applyVOToModel(? extends GatewayFilterVO)
        
        try
        {
            gtwFilterModelClass.getDeclaredMethod("doGetVO", (Class<?>[])null);
        }
        catch (Exception e)
        {
            Log.log.warn("Expected method doGetVO() returning " + gtwFilterVOClassName + " not found in " + gtwFilterModelClassName + ".", e);
            throw e;
        }
        
        try
        {
            gtwFilterModelClass.getDeclaredMethod("applyVOToModel", Class.forName(gtwFilterVOClassName));
        }
        catch (Exception e)
        {
            Log.log.warn("Expected method applyVOToModel(" + gtwFilterVOClassName + ") not found in " + gtwFilterModelClassName + ".", e);
            throw e;
        }
        
        boolean hasLob = false;
        
        for (String getterMthdName : getterToRtnType.keySet())
        {
            Method getMethod = null;
            
            try
            {
                getMethod = gtwFilterModelClass.getDeclaredMethod(getterMthdName, (Class<?>[])null);
            }
            catch(Exception mnfe)
            {
                Log.log.warn("Expected method " + getterMthdName + " returning " + getterToRtnType.get(getterMthdName).getName() + " not found in " + gtwFilterModelClassName + ".", mnfe);
                
                try
                {
                    throw mnfe;
                }
                catch (Exception e1)
                {
                    ;
                }
            }
            
            if (getMethod != null)
            {
                if (!getMethod.getReturnType().getName().equals(getterToRtnType.get(getterMthdName).getName()))
                {
                    Log.log.warn("Return type " + getMethod.getReturnType().getName() + " of " + getterMthdName + 
                                 " in " + gtwFilterModelClassName + " does not match return type " + 
                                 getterToRtnType.get(getterMthdName).getName() + " of " + getterMthdName + 
                                 " in " + gtwFilterVOClassName + ".");
                    throw new Exception("Return type " + getMethod.getReturnType().getName() + " of " + getterMthdName + 
                                        " in " + gtwFilterModelClassName + " does not match return type " + 
                                        getterToRtnType.get(getterMthdName).getName() + " of " + getterMthdName + 
                                        " in " + gtwFilterVOClassName + ".");
                }
                
                Annotation clmnAntn = null;
                
                clmnAntn = getMethod.getAnnotation(Column.class);
                
                if (clmnAntn == null)
                {
                    Log.log.warn(getterMthdName + " is missing mandatory @Column annotation.");
                    throw new Exception(getterMthdName + " is missing mandatory @Column annotation.");
                }
                
                if (!hasLob)
                {
                    if (getMethod.getAnnotation(Lob.class) != null)
                    {
                        hasLob = true;
                    }
                }
            }
        }
        
        if (hasLob)
        {
            try
            {
                gtwFilterModelClass.getDeclaredMethod("ugetCdataProperty", (Class<?>[])null);
            }
            catch (Exception e)
            {
                Log.log.warn("Expected method ugetCdataProperty() returning List<String> not found in " + gtwFilterModelClassName + ".", e);
                throw e;
            }
        }
    }
    
    @SuppressWarnings("unchecked")
    private void validateGtwMsgHandlerImpl(String gtwMsgHandlerClassName) throws Exception
    {
        Class<? extends MGateway> gtwMsgHandlerClass = null;
        
        // Load com.resolve.gateway.M<implprefix> CLass
        
        try
        {
            gtwMsgHandlerClass = 
                (Class<? extends MGateway>) Class.forName(gtwMsgHandlerClassName, false, 
                                                          this.getClass().getClassLoader());
        }
        catch(Exception e)
        {
            Log.log.warn("Failed to load " + gtwMsgHandlerClassName + ".", e);
            throw e;
        }
        
        // Methods getSetFilterMethodName(), populateGatewaySpecificFilterProperties(Map<String, String>, Filter)
        
        try
        {
            gtwMsgHandlerClass.getDeclaredMethod("getSetFilterMethodName", (Class<?>[])null);
        }
        catch (Exception e)
        {
            Log.log.warn("Expected method getSetFilterMethodName() returning String not found in " + gtwMsgHandlerClassName + ".", e);
            throw e;
        }
        
        try
        {
            gtwMsgHandlerClass.getDeclaredMethod("populateGatewaySpecificFilterProperties", Map.class, Filter.class);
        }
        catch (Exception e)
        {
            Log.log.warn("Expected method populateGatewaySpecificFilterProperties(Map<String, String>, Filter) not found in " + gtwMsgHandlerClassName + ".", e);
            throw e;
        }
    }
    
    @SuppressWarnings("unchecked")
    private void validateGtwRSCtrlMsgHandlerImpl(String gtwRSCtrlMsgHandlerClassName) throws Exception
    {
        Class<? extends com.resolve.rscontrol.MGateway> gtwRSCtrlMsgHandlerClass = null;
        
        // Load com.resolve.rscontrol.M<implprefix> CLass
        
        try
        {
            gtwRSCtrlMsgHandlerClass = 
                (Class<? extends com.resolve.rscontrol.MGateway>) Class.forName(gtwRSCtrlMsgHandlerClassName, false, 
                                                                                this.getClass().getClassLoader());
        }
        catch(Exception e)
        {
            Log.log.warn("Failed to load " + gtwRSCtrlMsgHandlerClassName + ".", e);
            throw e;
        }
        
        // Methods getModelName()
        
        try
        {
            gtwRSCtrlMsgHandlerClass.getDeclaredMethod("getModelName", (Class<?>[])null);
        }
        catch (Exception e)
        {
            Log.log.warn("Expected method getModelName() returning String not found in " + gtwRSCtrlMsgHandlerClassName + ".", e);
            throw e;
        }
    }
    
    @SuppressWarnings("unchecked")
    private void validateGtwImpl(String gtwClassName, String implPrefix, String implPackage) throws Exception
    {
        Class<? extends BaseClusteredGateway> gtwClass = null;
        
        // Load <package>.<implprefix>Gateway CLass
        
        try
        {
            gtwClass = 
                (Class<? extends BaseClusteredGateway>) Class.forName(gtwClassName, false, 
                                                                      this.getClass().getClassLoader());
        }
        catch(Exception e)
        {
            Log.log.warn("Failed to load " + gtwClassName + ".", e);
            throw e;
        }
        
        // Method Validation
        
        // getInstance(<implPackage>.ConfigReceive<implPrefix>)
        
        try
        {
            gtwClass.getDeclaredMethod("getInstance", Class.forName(implPackage + ".ConfigReceive" + implPrefix));
        }
        catch (Exception e)
        {
            Log.log.warn("Expected method getInstance(" + (implPackage + ".ConfigReceive" + implPrefix) + 
                         ") returning " + gtwClassName + " not found in " + gtwClassName + ".", e);
            throw e;
        }
        
        // getInstance()
        
        try
        {
            gtwClass.getDeclaredMethod("getInstance", (Class<?>[])null);
        }
        catch (Exception e)
        {
            Log.log.warn("Expected method getInstance() returning " + gtwClassName + " not found in " + gtwClassName + ".", e);
            throw e;
        }
        
        // getLicenseCode()
        
        try
        {
            gtwClass.getDeclaredMethod("getLicenseCode", (Class<?>[])null);
        }
        catch (Exception e)
        {
            Log.log.warn("Expected method getLicenseCode() returning String not found in " + gtwClassName + ".", e);
            throw e;
        }
        
        // getGatewayEventType()
        
        try
        {
            gtwClass.getDeclaredMethod("getGatewayEventType", (Class<?>[])null);
        }
        catch (Exception e)
        {
            Log.log.warn("Expected method getGatewayEventType() returning String not found in " + gtwClassName + ".", e);
            throw e;
        }
        
        // getMessageHandlerName()
        
        try
        {
            gtwClass.getDeclaredMethod("getMessageHandlerName", (Class<?>[])null);
        }
        catch (Exception e)
        {
            Log.log.warn("Expected method getMessageHandlerName() returning String not found in " + gtwClassName + ".", e);
            throw e;
        }
        
        // getMessageHandlerClass()
        
        try
        {
            gtwClass.getDeclaredMethod("getMessageHandlerClass", (Class<?>[])null);
        }
        catch (Exception e)
        {
            Log.log.warn("Expected method getMessageHandlerClass() returning M" + implPrefix + " not found in " + gtwClassName + ".", e);
            throw e;
        }
        
        // getQueueName()
        
        try
        {
            gtwClass.getDeclaredMethod("getQueueName", (Class<?>[])null);
        }
        catch (Exception e)
        {
            Log.log.warn("Expected method getQueueName() returning String not found in " + gtwClassName + ".", e);
            throw e;
        }
        
        // initialize()
        
        try
        {
            gtwClass.getDeclaredMethod("initialize", (Class<?>[])null);
        }
        catch (Exception e)
        {
            Log.log.warn("Expected method initialize not found in " + gtwClassName + ".", e);
            throw e;
        }
        
        // getFilter()
        
        try
        {
            gtwClass.getDeclaredMethod("getFilter", Map.class);
        }
        catch (Exception e)
        {
            Log.log.warn("Expected method getFilter(Map<String, Object>) not found in " + gtwClassName + ".", e);
            throw e;
        }
        
        // start()
        
        try
        {
            gtwClass.getDeclaredMethod("start", (Class<?>[])null);
        }
        catch (Exception e)
        {
            Log.log.warn("Expected method start() not found in " + gtwClassName + ".", e);
            throw e;
        }
        
        // run()
        
        try
        {
            gtwClass.getDeclaredMethod("run", (Class<?>[])null);
        }
        catch (Exception e)
        {
            Log.log.warn("Expected method run() not found in " + gtwClassName + ".", e);
            throw e;
        }
        
        // cleanFilter(ConcurrentHashMap<String, Filter>) - Optional
        
        try
        {
            gtwClass.getDeclaredMethod("cleanFilter", ConcurrentHashMap.class);
            Log.log.info("Optional method cleanFilter() found in " + gtwClassName + ".");
        }
        catch (Exception e)
        {
            ;
        }
        
        // addAdditionalValues(Map<String, Object>, Map<String, String>)
        
        try
        {
            gtwClass.getDeclaredMethod("addAdditionalValues", Map.class, Map.class);
        }
        catch (Exception e)
        {
            Log.log.warn("Expected method addAdditionalValues(Map<String, Object>, Map<String, String>) not found in " + gtwClassName + ".", e);
            throw e;
        }
        
        // addAdditionalValues(Map<String, Object>, Map<String, String>, Filter) - Optional
        
        try
        {
            gtwClass.getDeclaredMethod("addAdditionalValues", Map.class, Map.class, Filter.class);
            Log.log.info("Optional method addAdditionalValues(Map<String, Object>, Map<String, String>, Filter) found in " + gtwClassName + ".");
        }
        catch (Exception e)
        {
            ;
        }
        
        // requireLicense()
        
        try
        {
            gtwClass.getDeclaredMethod("requireLicense", (Class<?>[])null);
            Log.log.info("Optional method requireLicense() found in " + gtwClassName + ".");
        }
        catch (Exception e)
        {
            ;
        }
    }
    
    
    
    @SuppressWarnings("unchecked")
    private Map<String, String> getSDKDevelopedGateways(XDoc configDoc) throws Exception
    {
        Map<String, String> sdkDevelopedGateways = new HashMap<String, String>();
        
        List<Element> sdkGtwNodes = configDoc.getNodes("/CONFIGURATION/RECEIVE/*[@IMPLPREFIX]");
        
        if (sdkGtwNodes != null && !sdkGtwNodes.isEmpty())
        {
            for (Element sdkGtwNode : sdkGtwNodes)
            {
                String implprefix = sdkGtwNode.attributeValue("IMPLPREFIX"); //configDoc.getStringValue(sdkGtwNode, "@IMPLPREFIX", "");
                String sdkgtwpackage = sdkGtwNode.attributeValue("PACKAGE"); //configDoc.getStringValue(sdkGtwNode, "@PACKAGE", "");
                
                if (StringUtils.isNotBlank(implprefix) && StringUtils.isNotBlank(sdkgtwpackage))
                {
                    sdkDevelopedGateways.put(implprefix, sdkgtwpackage);
                }
            }
        }
        
        return sdkDevelopedGateways;
    }

    @Override
    protected void saveConfig() throws Exception
    {
        // save config
        super.saveConfig();
    } // saveConfig

    protected void startReceivers() throws Exception
    {   
        startStopSDKDvlpdGtwRcvrs(true);

    } // startReceivers
    
    private void startStopSDKDvlpdGtwRcvrs(boolean start)
    {
        for (String implPrefix : sdkDvlpdImplPrefxToPkg.keySet())
        {
            try
            {
                startStopSDKDvlpdGtwRcvr(implPrefix, sdkDvlpdImplPrefxToPkg.get(implPrefix) + "." + implPrefix + "Gateway", start ? "start" : "stop");
                if(start){
                    if(unitTestOnly){
                        t.executeUnitTest(sdkDvlpdImplPrefxToPkg.get(implPrefix));
                    }else if(unitTestNewSDK) {
                        t.exeNewSDKUnitTest(sdkDvlpdImplPrefxToPkg.get(implPrefix));
                    }
                    else{
                        t.prepareDataForFilterTest(sdkDvlpdImplPrefxToPkg.get(implPrefix));
                    }
                }
            }
            catch (Throwable t)
            {
                if (start)
                {
                    sdkdDvlpdCfgRcvs.get(implPrefix).setActive(false);
                }
                Log.log.warn("Error " + (start ? "starting " : "stopping ") + sdkDvlpdImplPrefxToPkg.get(implPrefix) + "." + 
                             implPrefix + "Gateway" + (start ? ", check blueprint configuration" : ""), t);
            }
        }
        
        if(start && unitTestOnly){
            terminate();
        }
        
    }
    
    @SuppressWarnings("unchecked")
    private void startStopSDKDvlpdGtwRcvr(String implPrefix, String gtwClassName, String methodName) throws Exception
    {
        Class<? extends BaseClusteredGateway> sdkDvlpdGtwClass = 
                        (Class<? extends BaseClusteredGateway>) Class.forName(gtwClassName, false, this.getClass().getClassLoader());
        
        
        Method gtwGetInstanceMethod = sdkDvlpdGtwClass.getDeclaredMethod("getInstance", sdkdDvlpdCfgRcvs.get(implPrefix).getClass());
                    
        Object gtwInstanceObj = gtwGetInstanceMethod.invoke(null, sdkdDvlpdCfgRcvs.get(implPrefix));
        
        Method gtwStartStopMethod = gtwInstanceObj.getClass().getMethod((methodName), (Class<?>[])null);
        
        if(methodName.equals("start")){
            gtwInstanceObj.getClass().getMethod("setNoFilter", boolean.class).invoke(gtwInstanceObj, unitTestOnly);
        }
        
        gtwStartStopMethod.invoke(gtwInstanceObj, (Object[])null);
    }

    protected void stopReceivers()
    {
        startStopSDKDvlpdGtwRcvrs(false);
    } // stopReceivers
    
    @Override
    protected void loadESB(XDoc configDoc) throws Exception
    {
        //configESB = new ConfigESB(configDoc);
        //configESB.load();
        configESB = null;
    }

    protected void initLog(Release release) throws Exception
    {
        // init logging
        Log.init(release.serviceName+"/"+logConfigFilename, release.type.toLowerCase());
        Log.log.warn("*** STARTING "+release.name.toUpperCase()+" ***");
        Log.log.warn("Version:                  "+release.version);
        Log.log.warn("ServiceName:              "+release.serviceName);
        Log.log.warn("HostName:                 "+release.hostName);
        Log.log.warn("SupportedResolveVersions: "+release.supportedResolveVersions);

        // stdout
        if (!release.name.toUpperCase().equals("RSCONSOLE") && !release.name.toUpperCase().equals("RSMGMT"))
        {
            System.out.println();
            System.out.println("*** STARTING "+release.name.toUpperCase()+" *** ("+new Date()+")");
            System.out.println("Version:                  "+release.version);
            System.out.println("ServiceName:              "+release.serviceName);
            System.out.println("HostName:                 "+release.hostName);
            System.out.println("SupportedResolveVersions: "+release.supportedResolveVersions);
        }

        // init log component / serviceName
        Log.initComponent(release.serviceName);
    } 
} // Main
