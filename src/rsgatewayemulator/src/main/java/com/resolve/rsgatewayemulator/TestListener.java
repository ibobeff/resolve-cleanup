package com.resolve.rsgatewayemulator;

import org.junit.runner.Description;
import org.junit.runner.Result;
import org.junit.runner.notification.Failure;
import org.junit.runner.notification.RunListener;

public class TestListener extends RunListener
{  
    static StringBuilder resultMessage = new StringBuilder();
    
    static public void clearResult(){
        resultMessage = new StringBuilder();
    }
    
    static public String getResult(){
        return resultMessage.toString();
    }
    
    public void testRunStarted(Description description) throws Exception {
        resultMessage.append("Start execute test cases in: " + description.getClass().toString()).append("\n");
    }

    public void testRunFinished(Result result) throws Exception {
    //    resultMessage.append("End tests execution of " + result.getRunCount() + "tests").append("\n");
    }

    public void testStarted(Description description) throws Exception {
    //    resultMessage.append("Starting: " + description.getMethodName()).append("\n");
    }

    public void testFinished(Description description) throws Exception {
        resultMessage.append("Finished: " + description.getMethodName()).append("\n");
    }

    public void testFailure(Failure failure) throws Exception {
        resultMessage.append("Failed: " + failure.toString()).append("\n");
    }

    public void testAssumptionFailure(Failure failure) {
        resultMessage.append("Failed: " + failure.toString()).append("\n");
    }

    public void testIgnored(Description description) throws Exception {
    //    resultMessage.append("Ignored: " + description.getMethodName()).append("\n");
    }
}
