package com.resolve.rsgatewayemulator;

import java.net.MalformedURLException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Map;

import org.junit.runner.JUnitCore;
import org.junit.runner.Result;
import org.junit.runner.notification.Failure;

import com.resolve.gateway.Filter;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.lang.reflect.Constructor;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;

public class ExecuteUnitTest
{
    final static String unitTestClassName = new String(".test.UnitTestSuite");
    final static String filterTestClassName = new String(".test.FilterTest");
    final static String sdkUnitTestClzName = new String(".test.EmulatorUnitTests");
    static String fileName = new String();
    static FileWriter fw = null;
    
    static {
        DateFormat dateFormat = new SimpleDateFormat("yyyyMMddHHmmss");
        fileName = "testResult" + dateFormat.format(new Date()).toString() + ".txt";
        
        try
        {
            File file = new File(fileName);
            if(!file.exists()){
                file.createNewFile();
            }
            fw = new FileWriter(fileName, true);
        }
        catch (IOException e)
        {
            System.out.println(e.getMessage());
        }
    }
    
    public void executeUnitTest(String packageName) throws MalformedURLException{
        String className = packageName.concat(unitTestClassName);
        try
        {
            Class<?> clz = Class.forName(className, false, this.getClass().getClassLoader());
            Result result = JUnitCore.runClasses(clz);

            if(result.wasSuccessful()){
                System.out.println("Pass test in UnitTestSuite, see detail in " + fileName);
            }
            else{
                for(Failure fail : result.getFailures()){
                    System.out.println(fail.getMessage());
                }
            }
            fw.append("Start API unit test suite\n");
            fw.append(TestListener.getResult());
            fw.flush();
        }
        catch (ClassNotFoundException e)
        {
            System.out.println("Mising " + className);
        }
        catch (IOException e)
        {
            System.out.println(e.getMessage());
        }
    }
    /**
     * 
     * @param packageName
     */
    public void exeNewSDKUnitTest(String classPkgName) {
        String className = classPkgName.concat(sdkUnitTestClzName);
        try
        {
            Class<?> clz = Class.forName(className, false, this.getClass().getClassLoader());
            Result result = JUnitCore.runClasses(clz);

            if(result.wasSuccessful()){
                System.out.println("Pass test in " + className + " , see detail in " + fileName);
            }
            else{
                for(Failure fail : result.getFailures()){
                    System.out.println(fail.getMessage());
                }
            }
            fw.append("Start API unit test suite\n");
            fw.append(TestListener.getResult());
            fw.flush();
        }
        catch (ClassNotFoundException e)
        {
            System.out.println("The asked Unit test class not found " + className);
        }
        catch (IOException e)
        {
            System.out.println(e.getMessage());
        }
    }
    
    public void prepareDataForFilterTest(String packageName) throws MalformedURLException{
        try
        {
            Class<?> clz = Class.forName(packageName.concat(filterTestClassName), false, this.getClass().getClassLoader());
            Constructor<?> ctor = clz.getDeclaredConstructor();
            Method method = clz.getMethod("prepareTestData", (Class<?>[])null);
            method.invoke(ctor.newInstance());
        }
        catch (ClassNotFoundException e)
        {
        }
        catch (NoSuchMethodException e)
        {
        }
        catch (SecurityException e)
        {
        }
        catch (IllegalAccessException e)
        {
        }
        catch (IllegalArgumentException e)
        {
        }
        catch (InvocationTargetException e)
        {
        }
        catch (InstantiationException e)
        {
        }
    }
    
    public void clearDataForFilterTest(String packageName) throws MalformedURLException{
        try
        {
            Class<?> clz = Class.forName(packageName.concat(filterTestClassName), false, this.getClass().getClassLoader());
            Constructor<?> ctor = clz.getDeclaredConstructor();
            Method method = clz.getMethod("clearTestData", (Class<?>[])null);
            method.invoke(ctor.newInstance());
        }
        catch (ClassNotFoundException e)
        {
        }
        catch (NoSuchMethodException e)
        {
        }
        catch (SecurityException e)
        {
        }
        catch (IllegalAccessException e)
        {
        }
        catch (IllegalArgumentException e)
        {
        }
        catch (InvocationTargetException e)
        {
        }
        catch (InstantiationException e)
        {
        }
    }
    
    public void executeFilterTest(Filter filter, Map<String, String> data) throws MalformedURLException{
        String className = filter.getClass().getPackage().toString().concat(filterTestClassName).substring(8);
        try
        {
            //String className = filter.getClass().getPackage().toString().concat(filterTestClassName).substring(8);
            Class<?> clz = Class.forName(className, false, this.getClass().getClassLoader());
           
            Constructor<?> ctor = clz.getDeclaredConstructor();
            Method method = clz.getMethod("testFilter", Filter.class, Map.class);
            
            fw.append("\n[Start filter test]\n");
            fw.append((String)method.invoke(ctor.newInstance(), filter, data));
            fw.append("\n[End filter test]\n");
            fw.flush();
            
        }
        catch (ClassNotFoundException e)
        {
            System.out.println("Mising " + className);
        }
        catch (NoSuchMethodException e)
        {
            System.out.println("There is not testFilterWithData method: ");
            System.out.println("    boolean testFilterWithData(MariadbFilter filter, Map<String, String> result)");
            
        }
        catch (SecurityException e)
        {
            System.out.println("Cannot access testFilterWithData method: ");
            System.out.println("    boolean testFilterWithData(MariadbFilter filter, Map<String, String> result)");
        }
        catch (IllegalAccessException e)
        {
            e.printStackTrace(System.out);
        }
        catch (IllegalArgumentException e)
        {
            e.printStackTrace(System.out);
        }
        catch (InvocationTargetException e)
        {
            e.printStackTrace(System.out);
        }
        catch (InstantiationException e)
        {
            e.printStackTrace(System.out);System.out.println(e);
        }
        catch (IOException e)
        {
            e.printStackTrace(System.out);;
        }
    }
    
   
}
