package com.resolve.rsgatewayemulator;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.regex.Pattern;
import java.util.zip.ZipEntry;
import java.util.zip.ZipInputStream;

import com.resolve.util.Log;

public class ValidationUtil
{

    public static boolean detectLibConflict(List<String> resolveLibDirs, List<String> customerLibDirs)
    {
        TreeNode root = new TreeNode();
        boolean found = false;
        for (String resolveLibDir : resolveLibDirs)
        {
            File folder = new File(resolveLibDir);
            if (!folder.exists())
            {
                Log.log.warn(resolveLibDir + " does not exist.");
            }
            if (folder.isDirectory())
            {
                addDirToTree(folder, root);
            }
            else if (folder.isFile())
            {
                if(folder.getName().endsWith(".jar")){
                    addJarToTree(folder, root);
                }
            }
        }
        
        for (String customerLibDir : customerLibDirs)
        {
            File folder = new File(customerLibDir);
            if (!folder.exists())
            {
                Log.log.warn(customerLibDir + " does not exist.");
            }
            if (folder.isDirectory())
            {
                if(findDirInTree(folder, root)){
                    found=true;;
                }
            }
            else if (folder.isFile())
            {
                if(folder.getName().endsWith(".jar")){
                    if(findJarInTree(folder, root)){
                        return true;
                    }
                }
            }
        }
        return found;
    }

    private static void addDirToTree(File folder, TreeNode root)
    {
        File[] listOfFiles = folder.listFiles();
        for (int i = 0; i < listOfFiles.length; i++)
        {
            if (listOfFiles[i].isFile())
            {
                if (listOfFiles[i].getName().endsWith(".jar"))
                {
                    addJarToTree(listOfFiles[i], root);
                }
            }
            else if (listOfFiles[i].isDirectory())
            {
                addDirToTree(listOfFiles[i], root);
            }
        }
    }

    private static void addJarToTree(File file, TreeNode root)
    {
        ZipInputStream zip;
        try
        {
            zip = new ZipInputStream(new FileInputStream(file));
        }
        catch (FileNotFoundException e)
        {
            Log.log.warn(e.getMessage(), e);
            return;
        }

        try
        {
            for (ZipEntry entry = zip.getNextEntry(); entry != null; entry = zip.getNextEntry())
            {
                if (!entry.isDirectory() && entry.getName().endsWith(".class"))
                {
                    root.addNode(entry.getName().substring(0, entry.getName().lastIndexOf('.')), Pattern.quote(System.getProperty("file.separator")), file.getPath());
                }
            }
        }
        catch (IOException e)
        {
            Log.log.warn(e.getMessage(), e);
        }
    }
    private static boolean findDirInTree(File folder, TreeNode root)
    {
        File[] listOfFiles = folder.listFiles();
        boolean found = false;
        for (int i = 0; i < listOfFiles.length; i++)
        {
            if (listOfFiles[i].isFile())
            {
                if (listOfFiles[i].getName().endsWith(".jar"))
                {
                    if(findJarInTree(listOfFiles[i], root)){
                        found=true;
                    }
                }
            }
            else if (listOfFiles[i].isDirectory())
            {
                if(findDirInTree(listOfFiles[i], root)){
                    found=true;
                }
            }
        }
        return found;
    }
    
    private static boolean findJarInTree(File file, TreeNode root)
    {
        ZipInputStream zip;
        String jarName = null;
        try
        {
            zip = new ZipInputStream(new FileInputStream(file));
        }
        catch (FileNotFoundException e)
        {
            Log.log.warn(e.getMessage(), e);
            return false;
        }

        try
        {
            for (ZipEntry entry = zip.getNextEntry(); entry != null; entry = zip.getNextEntry())
            {
                if (!entry.isDirectory() && entry.getName().endsWith(".class"))
                {
                    jarName  = root.getLeafNode(entry.getName().substring(0, entry.getName().lastIndexOf('.')), Pattern.quote(System.getProperty("file.separator")));
                    if(jarName!=null){    
              //          System.out.println("There is library conflict between " + file.getPath() + " and " + jarName);
                        Log.log.warn("There is library conflict between " + file.getName() + " and " + jarName);
                        return true;
                    }
                }
            }
        }
        catch (IOException e)
        {
            Log.log.warn(e.getMessage(), e);
        }
        return false;
    }

    public static void main(String[] args)
    {
        ArrayList<String> resolveLibDirs = new ArrayList<String>();
        ArrayList<String> customerLibDirs = new ArrayList<String>();
        resolveLibDirs.add("C:\\project\\resolve\\src\\lib");
        customerLibDirs.add("C:\\project\\resolve\\dist\\gatewaylibs");
        System.out.println(detectLibConflict(resolveLibDirs, customerLibDirs));
    }
}
