package com.resolve.thread;

import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;
import java.util.concurrent.SynchronousQueue;
import java.util.concurrent.ThreadPoolExecutor;
import java.util.concurrent.TimeUnit;
import com.resolve.esb.MMsgHeader;
import com.resolve.rsbase.MainBase;
import com.resolve.util.ERR;
import com.resolve.util.Log;

public class TaskExecutor {
	private static volatile ExecutorService cachedThreadPool;
	
	public static ExecutorService init() {
		if (cachedThreadPool == null) {
			synchronized (TaskExecutor.class) {
				if (cachedThreadPool == null) {
					cachedThreadPool = new ThreadPoolExecutor(0, Integer.MAX_VALUE, 60L, TimeUnit.SECONDS, new SynchronousQueue<Runnable>());
					// add thread to track limit of active threads in pool
					ScheduledExecutor.getInstance().executeRepeat(TaskExecutor.class, "checkPoolState", 2, TimeUnit.MINUTES);
				}
			}
			return cachedThreadPool;
		}
		throw new IllegalStateException(String.format("CachedThreadPool is already initialized"));
	}
	
	public void checkPoolState() {
		int executorMaxPoolSize = MainBase.main.configGeneral.getExecutorAlarmThreadLimit();
		ThreadPoolExecutor ex = (ThreadPoolExecutor)cachedThreadPool;
		int poolSize = ex.getPoolSize();
		if (poolSize >= executorMaxPoolSize) {
			Log.log.warn("Task executor pool size larger than expected, Qeueued Submission Count: " + poolSize);
			Log.alert(ERR.E70012);
		}
	}

	public static ExecutorService getCachedThreadPool() {
		if (cachedThreadPool == null) {
			throw new IllegalStateException(
					String.format("%s is null, you have to call init first", TaskExecutor.class.getName()));
		}
		return cachedThreadPool;
	}

	public static String execute(Object invokeObject, String methodname) {
		return execute("", invokeObject, methodname);
	}

	public static String execute(Class<?> invokeClass, String methodname) {
		return execute("", invokeClass, methodname);
	}

	public static String execute(String jobid, Object invokeObject, String methodname) {
		String result = jobid;

		try {
			ResolveTask task = new ResolveTask(jobid, invokeObject, methodname);
			Future<?> future = cachedThreadPool.submit(task);
			result = task.getJobId();
		} catch (Throwable e) {
			Log.log.warn(e.getMessage());
		}

		return result;
	}

	public static String execute(String jobid, Class<?> invokeClass, String methodname) {
		String result = jobid;

		try {
			ResolveTask task = new ResolveTask(jobid, invokeClass, methodname);
			Future<?> future = cachedThreadPool.submit((ResolveTask) task);
			result = task.getJobId();
		} catch (Throwable e) {
			Log.log.warn(e.getMessage());
		}

		return result;
	}

	public static String execute(Object invokeObject, String methodname, Object params) {
		return execute("", invokeObject, methodname, params);
	} // execute

	public static String execute(Class<?> invokeClass, String methodname, Object params) {
		return execute("", invokeClass, methodname, params);
	} // execute

	public static String execute(String jobid, Object invokeObject, String methodname, Object params) {
		String result = jobid;

		try {
			ResolveTask task = new ResolveTask(jobid, invokeObject, methodname, params);
			Future<?> future = cachedThreadPool.submit((ResolveTask) task);
			result = task.getJobId();
		} catch (Throwable e) {
			Log.log.warn(e.getMessage());
		}

		return result;
	} // execute

	public static String execute(String jobid, Class<?> invokeClass, String methodname, Object params) {
		String result = jobid;

		try {
			ResolveTask task = new ResolveTask(jobid, invokeClass, methodname, params);
			Future<?> future = cachedThreadPool.submit((ResolveTask) task);
			result = task.getJobId();
		} catch (Throwable e) {
			Log.log.warn(e.getMessage());
		}

		return result;
	} // execute

	public static String execute(MMsgHeader header, Object invokeObject, String methodname, Object params) {
		return execute("", header, invokeObject, methodname, params);
	} // execute

	public static String execute(MMsgHeader header, Class<?> invokeClass, String methodname, Object params) {
		return execute("", header, invokeClass, methodname, params);
	} // execute

	public static String execute(String jobid, MMsgHeader header, Object invokeObject, String methodname, Object params) {
		String result = jobid;

		try {
			ResolveTask task = new ResolveTask(jobid, header, invokeObject, methodname, params);
			Future<?> future = cachedThreadPool.submit((ResolveTask) task);
			result = task.getJobId();
		} catch (Throwable e) {
			Log.log.warn(e.getMessage());
		}

		return result;
	} // execute

	public static String execute(String jobid, MMsgHeader header, Class<?> invokeClass, String methodname, Object params) {
		String result = jobid;

		try {
			ResolveTask task = new ResolveTask(jobid, header, invokeClass, methodname, params);
			Future<?> future = cachedThreadPool.submit((ResolveTask) task);
			result = task.getJobId();
		} catch (Throwable e) {
			Log.log.warn(e.getMessage());
		}

		return result;
	}

}