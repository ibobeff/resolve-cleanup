package com.resolve.thread;

import java.util.concurrent.Callable;
import java.util.concurrent.Future;
import java.util.concurrent.ScheduledThreadPoolExecutor;
import java.util.concurrent.TimeUnit;
import com.resolve.esb.MMsgHeader;
import com.resolve.util.Log;

public class ScheduledExecutor extends ScheduledThreadPoolExecutor {

	private static volatile ScheduledExecutor instance;

	public static ScheduledExecutor init(int corePoolSize) {
		if (instance == null) {
			synchronized (ScheduledExecutor.class) {
				if (instance == null) {
					instance = new ScheduledExecutor(corePoolSize == 0 ? 50 : corePoolSize);
				}
			}
			return instance;
		}
		throw new IllegalStateException(
				String.format("%s is already initialized", ScheduledExecutor.class.getName()));
	}

	public static ScheduledExecutor getInstance() {
		if (instance == null) {
			throw new IllegalStateException(String.format("%s is null, you have to call init first",
					ScheduledExecutor.class.getName()));
		}
		return instance;
	}


	private ScheduledExecutor(int initThread) {
		super(initThread);
	}

	public void stop() {
		this.shutdownNow();
	} // stop

	public int getActiveThreads() {
		return super.getActiveCount();
	} // getActiveThreads

	public int getMaxThread() {
		return super.getMaximumPoolSize();
	} // getMaxThread

	public int getQueueSize() {
		return getQueue().size();
	} // getQueueSize

	public int getCorePoolSize() {
		return super.getPoolSize();
	} // getCorePoolSize

	public int getPoolSize() {
		return super.getPoolSize();
	} // getPoolSize

	public int getPeakPoolSize() {
		return super.getLargestPoolSize();
	} // getPeakPoolSize

	public int getMaxPoolSize() {
		return super.getMaximumPoolSize();
	} // getMaxPoolSize

	public String executeDelayed(Object invokeObject, String methodname, long delay, TimeUnit unit) {
		return executeDelayed("", invokeObject, methodname, delay, unit);
	} // executeDelayed

	public String executeDelayed(Class invokeClass, String methodname, long delay, TimeUnit unit) {
		return executeDelayed("", invokeClass, methodname, delay, unit);
	} // executeDelayed

	public String executeDelayed(String jobid, Object invokeObject, String methodname, long delay, TimeUnit unit) {
		String result = jobid;

		try {
			ScheduledTask task = new ScheduledTask(this, jobid, invokeObject,
					methodname);
			Future future = schedule((Callable) task, delay, unit);

			result = task.getJobId();
		} catch (Throwable e) {
			Log.log.warn(e.getMessage());
		}

		return result;
	} // executeDelayed

	public String executeDelayed(String jobid, Class invokeClass, String methodname, long delay, TimeUnit unit) {
		String result = jobid;

		try {
			ScheduledTask task = new ScheduledTask(this, jobid, invokeClass,
					methodname);
			Future future = schedule((Callable) task, delay, unit);

			result = task.getJobId();
		} catch (Throwable e) {
			Log.log.warn(e.getMessage());
		}

		return result;
	} // executeDelayed

	public String executeDelayedRepeat(Object invokeObject, String methodname, long delay, long interval,
			TimeUnit unit) {
		return executeDelayedRepeat("", invokeObject, methodname, delay, interval, unit);
	} // executeDelayedRepeat

	public String executeDelayedRepeat(Class invokeClass, String methodname, long delay, long interval, TimeUnit unit) {
		return executeDelayedRepeat("", invokeClass, methodname, delay, interval, unit);
	} // executeDelayedRepeat

	public String executeDelayedRepeat(String jobid, Object invokeObject, String methodname, long delay, long interval,
			TimeUnit unit) {
		String result = jobid;

		try {
			ScheduledTask task = new ScheduledTask(this, jobid, invokeObject,
					methodname);
			scheduleWithFixedDelay(task, delay, interval, unit);

			result = task.getJobId();
		} catch (Throwable e) {
			Log.log.warn(e.getMessage());
		}

		return result;
	} // executeDelayedRepeat

	public String executeDelayedRepeat(String jobid, Class invokeClass, String methodname, long delay, long interval,
			TimeUnit unit) {
		String result = jobid;

		try {
			ScheduledTask task = new ScheduledTask(this, jobid, invokeClass,
					methodname);
			scheduleWithFixedDelay(task, delay, interval, unit);

			result = task.getJobId();
		} catch (Throwable e) {
			Log.log.warn(e.getMessage());
		}

		return result;
	} // executeDelayedRepeat

	public String executeRepeat(Object invokeObject, String methodname, long interval, TimeUnit unit) {
		return executeRepeat("", invokeObject, methodname, interval, unit);
	} // executeRepeat

	public String executeRepeat(Class invokeClass, String methodname, long interval, TimeUnit unit) {
		return executeRepeat("", invokeClass, methodname, interval, unit);
	} // executeRepeat

	public String executeRepeat(String jobid, Object invokeObject, String methodname, long interval, TimeUnit unit) {
		String result = jobid;

		try {
			ScheduledTask task = new ScheduledTask(this, jobid, invokeObject,
					methodname);
			scheduleWithFixedDelay(task, 0, interval, unit);

			result = task.getJobId();
		} catch (Throwable e) {
			Log.log.warn(e.getMessage());
		}

		return result;
	} // executeRepeat

	public String executeRepeat(String jobid, Class invokeClass, String methodname, long interval, TimeUnit unit) {
		String result = jobid;

		try {
			ScheduledTask task = new ScheduledTask(this, jobid, invokeClass,
					methodname);
			scheduleWithFixedDelay(task, 0, interval, unit);

			result = task.getJobId();
		} catch (Throwable e) {
			Log.log.warn(e.getMessage());
		}

		return result;
	} // executeRepeat

	public String executeRepeat(Object invokeObject, String methodname, long delay, long interval, TimeUnit unit) {
		return executeRepeat("", invokeObject, methodname, delay, interval, unit);
	} // executeRepeat

	public String executeRepeat(Class invokeClass, String methodname, long delay, long interval, TimeUnit unit) {
		return executeRepeat("", invokeClass, methodname, delay, interval, unit);
	} // executeRepeat

	public String executeRepeat(String jobid, Object invokeObject, String methodname, long delay, long interval,
			TimeUnit unit) {
		String result = jobid;
		try {
			ScheduledTask task = new ScheduledTask(this, jobid, invokeObject,
					methodname);
			scheduleWithFixedDelay(task, delay, interval, unit);

			result = task.getJobId();
		} catch (Throwable e) {
			Log.log.warn(e.getMessage());
		}

		return result;
	} // executeRepeat

	public String executeRepeat(String jobid, Class invokeClass, String methodname, long delay, long interval,
			TimeUnit unit) {
		String result = jobid;
		try {
			ScheduledTask task = new ScheduledTask(this, jobid, invokeClass,
					methodname);
			scheduleWithFixedDelay(task, delay, interval, unit);

			result = task.getJobId();
		} catch (Throwable e) {
			Log.log.warn(e.getMessage());
		}

		return result;
	} // executeRepeat

	public String executeDelayed(Object invokeObject, String methodname, Object params, long delay, TimeUnit unit) {
		return executeDelayed("", invokeObject, methodname, params, delay, unit);
	} // executeDelayed

	public String executeDelayed(Class invokeClass, String methodname, Object params, long delay, TimeUnit unit) {
		return executeDelayed("", invokeClass, methodname, params, delay, unit);
	} // executeDelayed

	public String executeDelayed(String jobid, Object invokeObject, String methodname, Object params, long delay,
			TimeUnit unit) {
		String result = jobid;

		try {
			ScheduledTask task = new ScheduledTask(this, jobid, invokeObject,
					methodname, params);

			// schedule job
			Future future = schedule((Callable) task, delay, unit);

			result = task.getJobId();
		} catch (Throwable e) {
			Log.log.warn(e.getMessage());
		}

		return result;
	} // executeDelayed

	public String executeDelayed(String jobid, Class invokeClass, String methodname, Object params, long delay,
			TimeUnit unit) {
		String result = jobid;

		try {
			ScheduledTask task = new ScheduledTask(this, jobid, invokeClass,
					methodname, params);
			Future future = schedule((Callable) task, delay, unit);

			result = task.getJobId();
		} catch (Throwable e) {
			Log.log.warn(e.getMessage());
		}

		return result;
	} // executeDelayed

	public String executeRepeat(Object invokeObject, String methodname, Object params, long interval, TimeUnit unit) {
		return executeRepeat("", invokeObject, methodname, params, interval, unit);
	} // executeRepeat

	public String executeRepeat(Class invokeClass, String methodname, Object params, long interval, TimeUnit unit) {
		return executeRepeat("", invokeClass, methodname, params, interval, unit);
	} // executeRepeat

	public String executeRepeat(String jobid, Object invokeObject, String methodname, Object params, long interval,
			TimeUnit unit) {
		String result = jobid;

		try {
			ScheduledTask task = new ScheduledTask(this, jobid, invokeObject,
					methodname, params);
			scheduleWithFixedDelay(task, 0, interval, unit);

			result = task.getJobId();
		} catch (Throwable e) {
			Log.log.warn(e.getMessage());
		}

		return result;
	} // executeRepeat

	public String executeRepeat(String jobid, Class invokeClass, String methodname, Object params, long interval,
			TimeUnit unit) {
		String result = jobid;

		try {
			ScheduledTask task = new ScheduledTask(this, jobid, invokeClass,
					methodname, params);
			scheduleWithFixedDelay(task, 0, interval, unit);

			result = task.getJobId();
		} catch (Throwable e) {
			Log.log.warn(e.getMessage());
		}

		return result;
	} // executeRepeat

	public String executeRepeat(Object invokeObject, String methodname, Object params, long delay, long interval,
			TimeUnit unit) {
		return executeRepeat("", invokeObject, methodname, params, delay, interval, unit);
	} // executeRepeat

	public String executeRepeat(Class invokeClass, String methodname, Object params, long delay, long interval,
			TimeUnit unit) {
		return executeRepeat("", invokeClass, methodname, params, delay, interval, unit);
	} // executeRepeat

	public String executeRepeat(String jobid, Object invokeObject, String methodname, Object params, long delay,
			long interval, TimeUnit unit) {
		String result = jobid;

		try {
			ScheduledTask task = new ScheduledTask(this, jobid, invokeObject,
					methodname, params);
			Future future = scheduleWithFixedDelay(task, delay, interval, unit);

			result = task.getJobId();
		} catch (Throwable e) {
			Log.log.warn(e.getMessage());
		}

		return result;
	} // executeRepeat

	public String executeRepeat(String jobid, Class invokeClass, String methodname, Object params, long delay,
			long interval, TimeUnit unit) {
		String result = jobid;

		try {
			ScheduledTask task = new ScheduledTask(this, jobid, invokeClass,
					methodname, params);
			Future future = scheduleWithFixedDelay(task, delay, interval, unit);

			result = task.getJobId();
		} catch (Throwable e) {
			Log.log.warn(e.getMessage());
		}

		return result;
	} // executeRepeat

	public String execute(MMsgHeader header, Object invokeObject, String methodname, Object params) {
		return execute("", header, invokeObject, methodname, params);
	} // execute

	public String execute(MMsgHeader header, Class invokeClass, String methodname, Object params) {
		return execute("", header, invokeClass, methodname, params);
	} // execute

	public String execute(String jobid, MMsgHeader header, Object invokeObject, String methodname, Object params) {
		String result = jobid;

		try {
			ScheduledTask task = new ScheduledTask(this, jobid, header,
					invokeObject, methodname, params);
			Future future = submit((Callable) task);

			result = task.getJobId();
		} catch (Throwable e) {
			Log.log.warn(e.getMessage());
		}

		return result;
	} // execute

	public String execute(String jobid, MMsgHeader header, Class invokeClass, String methodname, Object params) {
		String result = jobid;

		try {
			ScheduledTask task = new ScheduledTask(this, jobid, header, invokeClass,
					methodname, params);
			Future future = submit((Callable) task);

			result = task.getJobId();
		} catch (Throwable e) {
			Log.log.warn(e.getMessage());
		}

		return result;
	} // execute

	public String executeDelayed(MMsgHeader header, Object invokeObject, String methodname, Object params, long delay,
			TimeUnit unit) {
		return executeDelayed("", header, invokeObject, methodname, params, delay, unit);
	} // executeDelayed

	public String executeDelayed(MMsgHeader header, Class invokeClass, String methodname, Object params, long delay,
			TimeUnit unit) {
		return executeDelayed("", header, invokeClass, methodname, params, delay, unit);
	} // executeDelayed

	public String executeDelayed(String jobid, MMsgHeader header, Object invokeObject, String methodname, Object params,
			long delay, TimeUnit unit) {
		String result = jobid;

		try {
			ScheduledTask task = new ScheduledTask(this, jobid, header,
					invokeObject, methodname, params);
			Future future = schedule((Callable) task, delay, unit);

			result = task.getJobId();
		} catch (Throwable e) {
			Log.log.warn(e.getMessage());
		}

		return result;
	} // executeDelayed

	public String executeDelayed(String jobid, MMsgHeader header, Class invokeClass, String methodname, Object params,
			long delay, TimeUnit unit) {
		String result = jobid;

		try {
			ScheduledTask task = new ScheduledTask(this, jobid, header, invokeClass,
					methodname, params);
			Future future = schedule((Callable) task, delay, unit);

			result = task.getJobId();
		} catch (Throwable e) {
			Log.log.warn(e.getMessage());
		}

		return result;
	} // executeDelayed

	public String executeRepeat(MMsgHeader header, Object invokeObject, String methodname, Object params, long interval,
			TimeUnit unit) {
		return executeRepeat("", header, invokeObject, methodname, params, interval, unit);
	} // executeRepeat

	public String executeRepeat(MMsgHeader header, Class invokeClass, String methodname, Object params, long interval,
			TimeUnit unit) {
		return executeRepeat("", header, invokeClass, methodname, params, interval, unit);
	} // executeRepeat

	public String executeRepeat(String jobid, MMsgHeader header, Object invokeObject, String methodname, Object params,
			long interval, TimeUnit unit) {
		String result = jobid;

		try {
			ScheduledTask task = new ScheduledTask(this, jobid, header,
					invokeObject, methodname, params);
			scheduleWithFixedDelay(task, 0, interval, unit);
			result = task.getJobId();
		} catch (Throwable e) {
			Log.log.warn(e.getMessage());
		}

		return result;
	} // executeRepeat

	public String executeRepeat(String jobid, MMsgHeader header, Class invokeClass, String methodname, Object params,
			long interval, TimeUnit unit) {
		String result = jobid;

		try {
			ScheduledTask task = new ScheduledTask(this, jobid, header, invokeClass,
					methodname, params);
			scheduleWithFixedDelay(task, 0, interval, unit);

			result = task.getJobId();
		} catch (Throwable e) {
			Log.log.warn(e.getMessage());
		}

		return result;
	} // executeRepeat

	public String executeRepeat(MMsgHeader header, Object invokeObject, String methodname, Object params, long delay,
			long interval, TimeUnit unit) {
		return executeRepeat("", header, invokeObject, methodname, params, delay, interval, unit);
	} // executeRepeat

	public String executeRepeat(MMsgHeader header, Class invokeClass, String methodname, Object params, long delay,
			long interval, TimeUnit unit) {
		return executeRepeat("", header, invokeClass, methodname, params, delay, interval, unit);
	} // executeRepeat

	public String executeRepeat(String jobid, MMsgHeader header, Object invokeObject, String methodname, Object params,
			long delay, long interval, TimeUnit unit) {
		String result = jobid;

		try {
			ScheduledTask task = new ScheduledTask(this, jobid, header,
					invokeObject, methodname, params);
			scheduleWithFixedDelay(task, delay, interval, unit);

			result = task.getJobId();
		} catch (Throwable e) {
			Log.log.warn(e.getMessage());
		}

		return result;
	} // executeRepeat

	public String executeRepeat(String jobid, MMsgHeader header, Class invokeClass, String methodname, Object params,
			long delay, long interval, TimeUnit unit) {
		String result = jobid;

		try {
			ScheduledTask task = new ScheduledTask(this, jobid, header, invokeClass,
					methodname, params);
			scheduleWithFixedDelay(task, delay, interval, unit);

			result = task.getJobId();
		} catch (Throwable e) {
			Log.log.warn(e.getMessage());
		}

		return result;
	}

	public String printInfo() {
		return "poolSize: " + super.getPoolSize() + " activeThreads: " + super.getActiveCount();
	}
}
