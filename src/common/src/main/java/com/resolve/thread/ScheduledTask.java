package com.resolve.thread;

import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.concurrent.Callable;

import com.resolve.esb.MMsgHeader;
import com.resolve.util.Log;

public class ScheduledTask implements Callable<Object>, Runnable {
	public ScheduledExecutor executor;
	public String jobid;
	public MMsgHeader header;
	public Object invokeObject;
	public Class invokeClass;
	public String methodname;
	public Object params;
	public boolean hasParams;
	private String context = "";

	public ScheduledTask(ScheduledExecutor executor, String jobid,
			Object invokeObject, String methodname) {
		try {
			context = Log.getCurrentContext();
			setJobId(jobid);

			this.executor = executor;
			this.header = null;
			this.invokeClass = invokeObject.getClass();
			this.invokeObject = invokeObject;
			this.methodname = methodname;
			this.params = null;
			this.hasParams = false;

			if (invokeClass.getSimpleName().equals("Class")) {
				throw new Exception("Invalid class. Please use specific class with method");
			}
		} catch (Exception e) {
			Log.log.error(e.getMessage(), e);
		}
	}

	public ScheduledTask(ScheduledExecutor executor, String jobid,
			Class invokeClass, String methodname) {
		try {
			context = Log.getCurrentContext();
			setJobId(jobid);

			this.executor = executor;
			this.header = null;
			this.invokeClass = invokeClass;
			this.invokeObject = invokeClass.newInstance();
			this.methodname = methodname;
			this.params = null;
			this.hasParams = false;
			
			if (invokeClass.getSimpleName().equals("Class")) {
				throw new Exception("Invalid class. Please use specific class with method");
			}
		} catch (Exception e) {
			Log.log.error(e.getMessage(), e);
		}
	}

	public ScheduledTask(ScheduledExecutor executor, String jobid,
			Object invokeObject, String methodname, Object params) {
		try {
			context = Log.getCurrentContext();
			setJobId(jobid);

			this.executor = executor;
			this.header = null;
			this.invokeClass = invokeObject.getClass();
			this.invokeObject = invokeObject;
			this.methodname = methodname;
			this.params = params;
			this.hasParams = true;

			if (invokeClass.getSimpleName().equals("Class")) {
				throw new Exception("Invalid class. Please use specific class with method");
			}
		} catch (Exception e) {
			Log.log.error(e.getMessage(), e);
		}
	}

	public ScheduledTask(ScheduledExecutor executor, String jobid,
			Class invokeClass, String methodname, Object params) {
		try {
			context = Log.getCurrentContext();
			setJobId(jobid);

			this.executor = executor;
			this.header = null;
			this.invokeClass = invokeClass;
			this.invokeObject = invokeClass.newInstance();
			this.methodname = methodname;
			this.params = params;
			this.hasParams = true;

			if (invokeClass.getSimpleName().equals("Class")) {
				throw new Exception("Invalid class. Please use specific class with method");
			}
		} catch (Exception e) {
			Log.log.error(e.getMessage(), e);
		}
	}

	public ScheduledTask(ScheduledExecutor executor, String jobid,
			MMsgHeader header, Object invokeObject, String methodname, Object params) {
		try {
			context = Log.getCurrentContext();
			setJobId(jobid);

			this.executor = executor;
			this.header = header;
			this.invokeClass = invokeObject.getClass();
			this.invokeObject = invokeObject;
			this.methodname = methodname;
			this.params = params;
			this.hasParams = true;

			if (invokeClass.getSimpleName().equals("Class")) {
				throw new Exception("Invalid class. Please use specific class with method");
			}
		} catch (Exception e) {
			Log.log.error(e.getMessage(), e);
		}
	}

	public ScheduledTask(ScheduledExecutor executor, String jobid,
			MMsgHeader header, Class invokeClass, String methodname, Object params) {
		try {
			context = Log.getCurrentContext();
			setJobId(jobid);

			this.executor = executor;
			this.header = header;
			this.invokeClass = invokeClass;
			this.invokeObject = invokeClass.newInstance();
			this.methodname = methodname;
			this.params = params;
			this.hasParams = true;

			if (invokeClass.getSimpleName().equals("Class")) {
				throw new Exception("Invalid class. Please use specific class with method");
			}
		} catch (Exception e) {
			Log.log.error(e.getMessage(), e);
		}
	}

	public Object call() throws Exception {
		Object result = null;
		long startTime = System.currentTimeMillis();

		result = execute();

		return result;
	}

	public void run() {
		long startTime = System.currentTimeMillis();
		try {
			execute();
		} catch (Throwable e) {
			Log.log.warn(e.getMessage());
		}

	}

	public Object execute() throws Exception {
		Log.setCurrentContext(context);
		Object result = null;

		Log.log.trace("Executor jobid: " + jobid + " class: " + invokeClass.getName() + " method: " + methodname);
		if (invokeObject != null) {
			Method invokeMethod = null;

			Class[] argClasses = null;
			Object[] argValues = null;

			// if class == "MService" - call MService.gse(MMsgHeader header, Map params,
			// String script)
			String classname = invokeClass.getSimpleName();
			if (classname.equals("MService")) {
				String scriptName = methodname.replace('.', '/') + ".groovy";
				methodname = "gse";

				argClasses = new Class[3];
				argValues = new Object[3];

				argClasses[0] = MMsgHeader.class;
				argValues[0] = header;
				argClasses[1] = Object.class;
				argValues[1] = params;
				argClasses[2] = String.class;
				argValues[2] = scriptName;
				invokeMethod = invokeObject.getClass().getDeclaredMethod(methodname, argClasses);
				invokeMethod.setAccessible(true);
			} else {
				if (hasParams == false) {
					// First find method name in declared methods of given class
					try {
						invokeMethod = invokeObject.getClass().getDeclaredMethod(methodname, (Class<?>[]) null);
					} catch (NoSuchMethodException e0) {
						Log.log.trace("Unknown declared method, handler - class: " + invokeObject.getClass().getName()
								+ " method: " + methodname + ", will try to find in superclass recursively...");
					}

					if (invokeMethod == null) {
						try {
							// Try to find method name in superclass recursively
							invokeMethod = invokeObject.getClass().getMethod(methodname, (Class<?>[]) null);
							invokeMethod.setAccessible(true);
						} catch (NoSuchMethodException e0) {
							String err = "Unknown method, handler - class: " + invokeObject.getClass().getName()
									+ " and superclass (recursively) method: " + methodname;
							Log.log.warn(err);

							// throw exception for return error message
							throw new Exception(err);
						}
					} else
						invokeMethod.setAccessible(true);
				} else {
					try {
						// search for MClass.method(<type> params)
						argClasses = new Class[1];
						argValues = new Object[1];
						argClasses[0] = getParamsClass();
						argValues[0] = params;
						invokeMethod = findInvokeMethod(invokeObject.getClass(), methodname, argClasses);
						invokeMethod.setAccessible(true);
					} catch (NoSuchMethodException e1) {
						try {
							// search for MClass.method(Object params)
							argClasses = new Class[1];
							argValues = new Object[1];
							argClasses[0] = Object.class;
							argValues[0] = params;
							invokeMethod = invokeObject.getClass().getDeclaredMethod(methodname, argClasses);
							invokeMethod.setAccessible(true);
						} catch (NoSuchMethodException e2) {
							if (header != null) {
								try {
									// search for MClass.method(MMsgHeader msgHeader, <type> params)
									argClasses = new Class[2];
									argValues = new Object[2];

									argClasses[0] = MMsgHeader.class;
									argValues[0] = header;
									argClasses[1] = getParamsClass();
									argValues[1] = params;
									invokeMethod = invokeObject.getClass().getDeclaredMethod(methodname, argClasses);
									invokeMethod.setAccessible(true);
								} catch (NoSuchMethodException e3) {
									try {
										// search for MClass.method(MMsgHeader msgHeader, Object params)
										argClasses = new Class[2];
										argValues = new Object[2];

										argClasses[0] = MMsgHeader.class;
										argValues[0] = header;
										argClasses[1] = Object.class;
										argValues[1] = params;
										invokeMethod = invokeObject.getClass().getDeclaredMethod(methodname,
												argClasses);
										invokeMethod.setAccessible(true);
									} catch (NoSuchMethodException e4) {
										try {
											// search for MClass.method(MMsgHeader msgHeader, Map params)
											argClasses = new Class[2];
											argValues = new Object[2];

											argClasses[0] = MMsgHeader.class;
											argValues[0] = header;
											argClasses[1] = Map.class;
											Map argValuesMap = new HashMap();
											// tricking the value for the Map
											argValuesMap.put(params, params);
											argValues[1] = argValuesMap;
											invokeMethod = invokeObject.getClass().getDeclaredMethod(methodname,
													argClasses);
											invokeMethod.setAccessible(true);
										} catch (NoSuchMethodException e5) {
											String err = "Unknown method, handler - class: "
													+ invokeObject.getClass().getName() + " method: " + methodname;
											Log.log.warn(err);

											// throw exception for return error message
											throw new Exception(err);
										}
									}
								}
							}
						}
					}
				}
			}

			// make all public methods callable except java.lang.Object methods
			if (invokeMethod != null) {
				if (invokeMethod.getDeclaringClass() == Object.class) {
					throw new Exception("ERROR: Not permitted to call methods defined in java.lang.Object");
				}

				// invoke method
				try {
					result = invokeMethod.invoke(invokeObject, argValues);
				} catch (InvocationTargetException e5) {
					Log.log.warn(e5.getMessage(), e5);

					Throwable t = e5.getTargetException();
					if (t != null) {
						throw new Exception(t.toString());
					}
				}
			} else {
				Log.log.warn("Unknown method, handler - class: " + invokeObject.getClass().getName() + " method: "
						+ methodname);
			}
		}
		Log.clearContext();
		return result;

	}

	/**
	 * Finds method by travering the entire class inheritance hierarchy. Without
	 * this method reflection API only checks the method in the current class and
	 * throw NoSuchMEthodException.
	 * 
	 * @param clazz
	 * @param methodname
	 * @param argClasses
	 * @return
	 * @throws NoSuchMethodException
	 */
	private Method findInvokeMethod(Class<? extends Object> clazz, String methodname, Class[] argClasses)
			throws NoSuchMethodException {
		while (true) {
			try {
				Method method = clazz.getDeclaredMethod(methodname, argClasses);
				method.setAccessible(true);
				return method;
			} catch (NoSuchMethodException e) {
				// if there is no more superclass then we're at the
				// end of the search and the method doesn't exists.
				if (clazz.getSuperclass() == null) {
					throw e;
				} else {
					// get the super class.
					clazz = clazz.getSuperclass();
				}
			}
		}
	}

	Class getParamsClass() {
		Class result = null;

		if (params instanceof String) {
			result = String.class;
		} else if (params instanceof List) {
			result = List.class;
		} else if (params instanceof Map) {
			result = Map.class;
		} else if (params instanceof Set) {
			result = Set.class;
		} else if (params instanceof byte[]) {
			result = byte[].class;
		} else {
			result = params.getClass();
		}

		return result;
	}

	public String getJobId() {
		return this.jobid;
	}

	public void setJobId(String jobid) {
		if (jobid == null || jobid.length() == 0) {
			this.jobid = "" + this.hashCode();
		} else {
			this.jobid = jobid;
		}
	}

	public String toString() {
		return this.jobid;
	}

}