/******************************************************************************
* (C) Copyright 2014
*
* Resolve Systems, LLC
*
* All rights reserved
* This software is distributed under license and may not be used, copied,
* modified, or distributed without the express written permission of
* Resolve Systems, LLC.
*
******************************************************************************/

package com.resolve.search.model.query;

public class Suggest
{
    private String[] input = new String[1];
    private int weight;

    public Suggest()
    {
        
    }

    public Suggest(String input, int weight)
    {
        super();
        this.input[0] = input;
        this.weight = weight;
    }
    
    public String[] getInput()
    {
        return input;
    }
    public void setInput(String[] input)
    {
        this.input = input;
    }
    public int getWeight()
    {
        return weight;
    }
    public void setWeight(int weight)
    {
        this.weight = weight;
    }
    public void addWeight(int weight)
    {
        this.weight += weight;
    }
}
