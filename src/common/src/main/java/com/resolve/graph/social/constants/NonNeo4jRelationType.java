/******************************************************************************
* (C) Copyright 2017
*
* Resolve Systems, LLC
*
* All rights reserved
* This software is distributed under license and may not be used, copied,
* modified, or distributed without the express written permission of
* Resolve Systems, LLC.
*
******************************************************************************/

package com.resolve.graph.social.constants;

/**
 * 
 * This class is copy of Neo4j RelationType for use
 * after migration of Neo4j to SQL.
 *
 */
public enum NonNeo4jRelationType
{
    SENT, TARGET, RECEIVER, MENTIONS, MEMBER, NAMESPACE, FOLLOWER, //INDIRECT_FOLLOW, 
    SUBSCRIBE, STARRED, LIKE, READ, SOLVED, NOTIFICATION, FAVORITE, COMMENT 
}
