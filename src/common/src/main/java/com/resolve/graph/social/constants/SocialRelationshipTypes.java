/******************************************************************************
* (C) Copyright 2014
*
* Resolve Systems, LLC
*
* All rights reserved
* This software is distributed under license and may not be used, copied,
* modified, or distributed without the express written permission of
* Resolve Systems, LLC.
*
******************************************************************************/

package com.resolve.graph.social.constants;

import org.neo4j.graphdb.RelationshipType;

/**
 * servers dual purpose of enum and relationship type for graph
 * 
 * @author jeet.marwah
 *
 */

public enum SocialRelationshipTypes implements RelationshipType
{
    PROCESS, 
    TEAM, 
    FORUM, 
    
    USER, 
    ACTIONTASK, 
    WORKSHEET, 
    NAMESPACE, 
    RUNBOOK, 
    DOCUMENT, 
    DECISIONTREE,
    RSS, 
    
    POST, 
    DELETEDPOST, 
    DELETEDCOMP, 
    NOTIFICATION,
    
    // same as the ResolveTag. The one in the ResolveTag will be removed and this should be used
    ResolveTag,
    
    //Relationships from CatalogRelationshipType
    CATALOG_REF, 
    CHILD_OF_CATALOG, 
    TAG_OF_CAT, //relationship between tag and catalog node
    CATALOG_DOCUMENT_REF, //relationship between catalog and the document
    DOCUMENT_TO_CATALOG_REF, //ref added when a Document is edited and added list of catalog items - this is separate as the adding of docs is not done from the catalog screen but only from the wiki
    
    //used as constants for catalog - it was in REsolveCatalog
    CatalogReference, 
    CatalogGroup,   
    CatalogFolder, 
    CatalogItem
}
