/******************************************************************************
* (C) Copyright 2019
*
* Resolve Systems, LLC
*
* All rights reserved
* This software is distributed under license and may not be used, copied,
* modified, or distributed without the express written permission of
* Resolve Systems, LLC.
*
******************************************************************************/

package com.resolve.annotations;
import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/**
 * CData annotation is used during export operation.
 * The value of any field of a model which has this annotation 
 * will be wrapped inside XML CDATA section.
 * 
 * @author mangeshshimpi
 *
 */
@Target (ElementType.FIELD)
@Retention(RetentionPolicy.RUNTIME)
public @interface CData {

}
