/******************************************************************************
* (C) Copyright 2014
*
* Resolve Systems, LLC
*
* All rights reserved
* This software is distributed under license and may not be used, copied,
* modified, or distributed without the express written permission of
* Resolve Systems, LLC.
*
******************************************************************************/

package com.resolve.wiki.stats;

/**
 * 
 * 
 * @author jeet.marwah
 *
 */
public enum StarRating
{
	STAR_1, STAR_2, STAR_3, STAR_4, STAR_5;
}
