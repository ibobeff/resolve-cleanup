/******************************************************************************
* (C) Copyright 2014
*
* Resolve Systems, LLC
*
* All rights reserved
* This software is distributed under license and may not be used, copied,
* modified, or distributed without the express written permission of
* Resolve Systems, LLC.
*
******************************************************************************/

package com.resolve.services.vo;


/**
 * use as a DTO for showing where an actiontask or any other comp is referenced
 * For now, used in Actiontask to show the list of docs that it is referenced in
 * 
 * @author jeet.marwah
 *
 */
public class ReferenceDTO
{
    private String name;//document name referred by
    private boolean refInContent;
    private boolean refInMain;
    private boolean refInAbort;
    
    public String getName()
    {
        return name;
    }
    public void setName(String name)
    {
        this.name = name;
    }
    public boolean isRefInContent()
    {
        return refInContent;
    }
    public void setRefInContent(boolean refInContent)
    {
        this.refInContent = refInContent;
    }
    public boolean isRefInMain()
    {
        return refInMain;
    }
    public void setRefInMain(boolean refInMain)
    {
        this.refInMain = refInMain;
    }
    public boolean isRefInAbort()
    {
        return refInAbort;
    }
    public void setRefInAbort(boolean refInAbort)
    {
        this.refInAbort = refInAbort;
    }

    @Override
    public boolean equals(Object otherObj)
    {
        boolean isEquals = false;
        if (otherObj instanceof ReferenceDTO)
        {
            ReferenceDTO that = (ReferenceDTO) otherObj;
            // if sys_id are not equal, they are diff
            if (that.getName().equals(this.getName()))
            {
                isEquals = true;
            }
        }
        return isEquals;
    }

    @Override
    public int hashCode()
    {
        int hash = 1;
        hash = hash * 37 + this.getName().hashCode();
        hash = hash * 37 + (this.getName() == null ? 0 : this.getName().hashCode());
        return hash;
    }
    
}
