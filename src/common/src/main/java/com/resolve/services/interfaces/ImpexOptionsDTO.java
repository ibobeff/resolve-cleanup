/******************************************************************************
* (C) Copyright 2014
*
* Resolve Systems, LLC
*
* All rights reserved
* This software is distributed under license and may not be used, copied,
* modified, or distributed without the express written permission of
* Resolve Systems, LLC.
*
******************************************************************************/

package com.resolve.services.interfaces;

import java.util.Date;

public class ImpexOptionsDTO
{
    public Boolean atProperties = true;
    public Boolean atPost = false;
    public Boolean excludeRefAssessor = false;
    public Boolean excludeRefParser = false;
    public Boolean excludeRefPreprocessor = false;
    
    // ActionTask Namespace options
    public Boolean atNsProperties = true;
    public Boolean excludeNsRefAssessor = false;
    public Boolean excludeNsRefParser = false;
    public Boolean excludeNsRefPreprocessor = false;
    public Boolean atNsPosts = false;
    
    // Wiki/Runbook options
    public Boolean wikiOverride = true;
    public Boolean wikiSubRB = true;
    public Boolean wikiTags = false;
    public Boolean wikiSubDT = false;
    public Boolean wikiPost = false;
    public Boolean wikiForms = true;
    public Boolean wikiRefATs = true; // Action Tasks referred in Wiki
    public Boolean wikiCatalogs = false;
    public Boolean wikiDeleteIfSameSysId = false;
    public Boolean wikiDeleteIfSameName = false;
    
    // Wiki/Runbook Namespace options
    /**
     * A range (start and end) of date to select components to export.
     * 
     * Shall we also consider a flag indicating whether it's update or created date?
     */
    public Date startDate;
    public Date endDate;
    public Boolean wikiNsRefATs = true; // Action Tasks referred in Wiki/Runbooks present in the namespace
    public Boolean wikiNsSubRB = true;
    public Boolean wikiNsTags = false;
    public Boolean wikiNsSubDT = false;
    public Boolean wikiNsOverride = true;
    public Boolean wikiNsPost = false;
    public Boolean wikiNsForms = true;
    public Boolean wikiNsDeleteIfSameSysId = false;
    public Boolean wikiNsDeleteIfSameName = false;
    
    
    // Menu Set
    public Boolean mSetMenuSection = false;
    public Boolean mSetMenuItem = false;
    
    // Menu Section
    public Boolean mSecMenuItem = false;

    // Social Components - Process
    public Boolean processWiki = true; // Wiki/Runbooks inside Process
    public Boolean processUsers = true;
    public Boolean processTeams = true;
    public Boolean processForums = true;
    public Boolean processRSS = true;
    public Boolean processATs = true;
    public Boolean processPosts = false;
    
    // Team
    public Boolean teamUsers = true;  // Users of a Team
    public Boolean teamTeams = false;   // Teams of a Team
    public Boolean teamPosts = false;  //Team Posts
    
    // Forum
    public Boolean forumUsers = true;
    public Boolean forumPosts = false;
    
    // RSS
    public Boolean rssUsers = true;
    
    // User
    public Boolean userRoleRel = false;
    public Boolean userGroupRel = false;
    public Boolean userInsert = false;
    
    // Role
    public Boolean roleUsers = false;
    public Boolean roleGroups = false;
    
    // Group
    public Boolean groupUsers = false;
    public Boolean groupRoleRel = false;
    
    // Form
    public Boolean formTables = true; // Form Custom tables
    public Boolean formRBs = true;
    //public Boolean formData = false;
    public Boolean formSS = true; // Form System Scripts
    public Boolean formATs = true; // Form ActionTasks
    
    // Table
    public Boolean tableForms = true; // Custom Forms
    public Boolean tableData = false;
    //public Boolean tableSchema = true; // Schema is included by default. 
    // public Boolean tableRBs = false;
    // public Boolean tableSS = false;
    // public Boolean tableATs = false;
    
    // Catalog
    public Boolean catalogWikis = true;
    public Boolean catalogTags = true;
    public Boolean catalogRefCatalogs = true;
    
    // Worksheet
    // public Boolean wsRBs = false;   // Runbooks referred in a worksheet
    // public Boolean wsATs = false;   // ActionTasks referred in a worksheet
    public Boolean wsPosts = false;
    // public Boolean wsDTs = false;
    
    
    // Wiki Templates
    public Boolean templateForm = true;
    public Boolean templateWiki = true;
    
    //DB Connection Pool
    public Boolean exportDBPool = true;
    
    //Remedyx Form
    public Boolean exportRemedyxForm = true;
    
    // Resolution Rounting
    public Boolean includeSchema = true;
    public Boolean includeMapping = false;
    public Boolean includeUIDisplay = true;
    public Boolean includeUIAutomation = true;
    public Boolean includeGWAutomation = true;
    public Boolean includeSirPlaybook = true;
    
    // Metric Threshold
    public boolean includeAllVersions = true;
    
    // Email Address
    public boolean emailAddress = true;
    
    // EWS Address
    public boolean ewsAddress = true;
    
    // CEF Custom Keys
    private boolean includeCustomKeys = false;

    public Date getStartDate()
    {
        return startDate;
    }

    public void setStartDate(Date startDate)
    {
        this.startDate = startDate;
    }

    public Date getEndDate()
    {
        return endDate;
    }

    public void setEndDate(Date endDate)
    {
        this.endDate = endDate;
    }

    public Boolean getAtPost()
    {
        return atPost;
    }

    public void setAtPost(Boolean atPost)
    {
        this.atPost = atPost;
    }

    public Boolean getExcludeRefAssessor()
    {
        return excludeRefAssessor;
    }

    public void setExcludeRefAssessor(Boolean assessor)
    {
        this.excludeRefAssessor = assessor;
    }

    public Boolean getExcludeRefParser()
    {
        return excludeRefParser;
    }

    public void setExcludeRefParser(Boolean parser)
    {
        this.excludeRefParser = parser;
    }

    public Boolean getExcludeRefPreprocessor()
    {
        return excludeRefPreprocessor;
    }

    public void setExcludeRefPreprocessor(Boolean preprocessor)
    {
        this.excludeRefPreprocessor = preprocessor;
    }

    public Boolean getAtProperties()
    {
        return atProperties;
    }

    public void setAtProperties(Boolean atProperties)
    {
        this.atProperties = atProperties;
    }
    
    public Boolean getAtNsProperties()
    {
        return atNsProperties;
    }

    public void setAtNsProperties(Boolean atNsProperties)
    {
        this.atNsProperties = atNsProperties;
    }

    public Boolean getExcludeNsRefAssessor()
    {
        return excludeNsRefAssessor;
    }

    public void setExcludeNsRefAssessor(Boolean excludeNsRefAssessor)
    {
        this.excludeNsRefAssessor = excludeNsRefAssessor;
    }

    public Boolean getExcludeNsRefParser()
    {
        return excludeNsRefParser;
    }

    public void setExcludeNsRefParser(Boolean excludeNsRefParser)
    {
        this.excludeNsRefParser = excludeNsRefParser;
    }

    public Boolean getExcludeNsRefPreprocessor()
    {
        return excludeNsRefPreprocessor;
    }

    public void setExcludeNsRefPreprocessor(Boolean excludeNsRefPreprocessor)
    {
        this.excludeNsRefPreprocessor = excludeNsRefPreprocessor;
    }

    public Boolean getAtNsPosts()
    {
        return atNsPosts;
    }

    public void setAtNsPosts(Boolean atNsPosts)
    {
        this.atNsPosts = atNsPosts;
    }
    
    public Boolean getWikiOverride()
    {
        return wikiOverride;
    }

    public void setWikiOverride(Boolean override)
    {
        this.wikiOverride = override;
    }

    public Boolean getWikiSubRB()
    {
        return wikiSubRB;
    }

    public void setWikiSubRB(Boolean wikiSubRB)
    {
        this.wikiSubRB = wikiSubRB;
    }

    public Boolean getWikiTags()
    {
        return wikiTags;
    }

    public void setWikiTags(Boolean wikiTags)
    {
        this.wikiTags = wikiTags;
    }

    public Boolean getWikiSubDT()
    {
        return wikiSubDT;
    }

    public void setWikiSubDT(Boolean wikiSubDT)
    {
        this.wikiSubDT = wikiSubDT;
    }

    public Boolean getWikiPost()
    {
        return wikiPost;
    }

    public void setWikiPost(Boolean wikiPost)
    {
        this.wikiPost = wikiPost;
    }

    public Boolean getWikiForms()
    {
        return wikiForms;
    }

    public void setWikiForms(Boolean wikiForms)
    {
        this.wikiForms = wikiForms;
    }

    public Boolean getWikiRefATs()
    {
        return wikiRefATs;
    }

    public void setWikiRefATs(Boolean wikiRefATs)
    {
        this.wikiRefATs = wikiRefATs;
    }
    
    public Boolean getWikiCatalogs()
    {
        return wikiCatalogs;
    }

    public void setWikiCatalogs(Boolean wikiCatalogs)
    {
        this.wikiCatalogs = wikiCatalogs;
    }

    public Boolean getWikiDeleteIfSameSysId()
    {
        return wikiDeleteIfSameSysId;
    }

    public void setWikiDeleteIfSameSysId(Boolean wikiDeleteIfSameSysId)
    {
        this.wikiDeleteIfSameSysId = wikiDeleteIfSameSysId;
    }

    public Boolean getWikiDeleteIfSameName()
    {
        return wikiDeleteIfSameName;
    }

    public void setWikiDeleteIfSameName(Boolean wikiDeleteIfSameName)
    {
        this.wikiDeleteIfSameName = wikiDeleteIfSameName;
    }

    public Boolean getWikiNsRefATs()
    {
        return wikiNsRefATs;
    }

    public void setWikiNsRefATs(Boolean wikiNsRefATs)
    {
        this.wikiNsRefATs = wikiNsRefATs;
    }

    public Boolean getWikiNsSubRB()
    {
        return wikiNsSubRB;
    }

    public void setWikiNsSubRB(Boolean wikiNsSubRB)
    {
        this.wikiNsSubRB = wikiNsSubRB;
    }

    public Boolean getWikiNsTags()
    {
        return wikiNsTags;
    }

    public void setWikiNsTags(Boolean wikiNsTags)
    {
        this.wikiNsTags = wikiNsTags;
    }

    public Boolean getWikiNsSubDT()
    {
        return wikiNsSubDT;
    }

    public void setWikiNsSubDT(Boolean wikiNsSubDT)
    {
        this.wikiNsSubDT = wikiNsSubDT;
    }

    public Boolean getWikiNsOverride()
    {
        return wikiNsOverride;
    }

    public void setWikiNsOverride(Boolean wikiNsOverride)
    {
        this.wikiNsOverride = wikiNsOverride;
    }

    public Boolean getWikiNsPost()
    {
        return wikiNsPost;
    }

    public void setWikiNsPost(Boolean wikiNsPost)
    {
        this.wikiNsPost = wikiNsPost;
    }

    public Boolean getWikiNsForms()
    {
        return wikiNsForms;
    }

    public void setWikiNsForms(Boolean wikiNsForms)
    {
        this.wikiNsForms = wikiNsForms;
    }

    public Boolean getWikiNsDeleteIfSameSysId()
    {
        return wikiNsDeleteIfSameSysId;
    }

    public void setWikiNsDeleteIfSameSysId(Boolean wikiNsDeleteIfSameSysId)
    {
        this.wikiNsDeleteIfSameSysId = wikiNsDeleteIfSameSysId;
    }

    public Boolean getWikiNsDeleteIfSameName()
    {
        return wikiNsDeleteIfSameName;
    }

    public void setWikiNsDeleteIfSameName(Boolean wikiNsDeleteIfSameName)
    {
        this.wikiNsDeleteIfSameName = wikiNsDeleteIfSameName;
    }

    public Boolean getmSetMenuSection()
    {
        return mSetMenuSection;
    }

    public void setmSetMenuSection(Boolean mSetMenuSection)
    {
        this.mSetMenuSection = mSetMenuSection;
    }

    public Boolean getmSetMenuItem()
    {
        return mSetMenuItem;
    }

    public void setmSetMenuItem(Boolean mSetMenuItem)
    {
        this.mSetMenuItem = mSetMenuItem;
    }

    public Boolean getmSecMenuItem()
    {
        return mSecMenuItem;
    }

    public void setmSecMenuItem(Boolean mSecMenuItem)
    {
        this.mSecMenuItem = mSecMenuItem;
    }

    public Boolean getProcessWiki()
    {
        return processWiki;
    }

    public void setProcessWiki(Boolean processWiki)
    {
        this.processWiki = processWiki;
    }

    public Boolean getProcessUsers()
    {
        return processUsers;
    }

    public void setProcessUsers(Boolean processUsers)
    {
        this.processUsers = processUsers;
    }

    public Boolean getProcessTeams()
    {
        return processTeams;
    }

    public void setProcessTeams(Boolean processTeams)
    {
        this.processTeams = processTeams;
    }

    public Boolean getProcessForums()
    {
        return processForums;
    }

    public void setProcessForums(Boolean processForums)
    {
        this.processForums = processForums;
    }

    public Boolean getProcessRSS()
    {
        return processRSS;
    }

    public void setProcessRSS(Boolean processRSS)
    {
        this.processRSS = processRSS;
    }

    public Boolean getProcessATs()
    {
        return processATs;
    }

    public void setProcessATs(Boolean processATs)
    {
        this.processATs = processATs;
    }

    public Boolean getProcessPosts()
    {
        return processPosts;
    }

    public void setProcessPosts(Boolean processPosts)
    {
        this.processPosts = processPosts;
    }

    public Boolean getTeamUsers()
    {
        return teamUsers;
    }

    public void setTeamUsers(Boolean teamUsers)
    {
        this.teamUsers = teamUsers;
    }

    public Boolean getTeamTeams()
    {
        return teamTeams;
    }

    public void setTeamTeams(Boolean teamTeams)
    {
        this.teamTeams = teamTeams;
    }

    public Boolean getTeamPosts()
    {
        return teamPosts;
    }

    public void setTeamPosts(Boolean teamPosts)
    {
        this.teamPosts = teamPosts;
    }

    public Boolean getForumUsers()
    {
        return forumUsers;
    }

    public void setForumUsers(Boolean forumUsers)
    {
        this.forumUsers = forumUsers;
    }

    public Boolean getForumPosts()
    {
        return forumPosts;
    }

    public void setForumPosts(Boolean forumPosts)
    {
        this.forumPosts = forumPosts;
    }

    public Boolean getRssUsers()
    {
        return rssUsers;
    }

    public void setRssUsers(Boolean rssUsers)
    {
        this.rssUsers = rssUsers;
    }

    public Boolean getUserRoleRel()
    {
        return userRoleRel;
    }

    public void setUserRoleRel(Boolean userRoleRel)
    {
        this.userRoleRel = userRoleRel;
    }

    public Boolean getUserGroupRel()
    {
        return userGroupRel;
    }

    public void setUserGroupRel(Boolean userGroupRel)
    {
        this.userGroupRel = userGroupRel;
    }

    public Boolean getUserInsert()
    {
        return userInsert;
    }

    public void setUserInsert(Boolean userInsert)
    {
        this.userInsert = userInsert;
    }

    public Boolean getRoleUsers()
    {
        return roleUsers;
    }

    public void setRoleUsers(Boolean roleUsers)
    {
        this.roleUsers = roleUsers;
    }

    public Boolean getRoleGroups()
    {
        return roleGroups;
    }

    public void setRoleGroups(Boolean roleGroups)
    {
        this.roleGroups = roleGroups;
    }

    public Boolean getGroupUsers()
    {
        return groupUsers;
    }

    public void setGroupUsers(Boolean groupUsers)
    {
        this.groupUsers = groupUsers;
    }

    public Boolean getGroupRoleRel()
    {
        return groupRoleRel;
    }

    public void setGroupRoleRel(Boolean groupRoleRel)
    {
        this.groupRoleRel = groupRoleRel;
    }

    public Boolean getFormTables()
    {
        return formTables;
    }

    public void setFormTables(Boolean formTables)
    {
        this.formTables = formTables;
    }

    public Boolean getFormRBs()
    {
        return formRBs;
    }

    public void setFormRBs(Boolean formRBs)
    {
        this.formRBs = formRBs;
    }

    public Boolean getFormSS()
    {
        return formSS;
    }

    public void setFormSS(Boolean formSS)
    {
        this.formSS = formSS;
    }
    
    public Boolean getFormATs()
    {
        return formATs;
    }

    public void setFormATs(Boolean formATs)
    {
        this.formATs = formATs;
    }

    public Boolean getTableForms()
    {
        return tableForms;
    }

    public void setTableForms(Boolean tableForms)
    {
        this.tableForms = tableForms;
    }

    public Boolean getTableData()
    {
        return tableData;
    }

    public void setTableData(Boolean tableData)
    {
        this.tableData = tableData;
    }

    public Boolean getCatalogWikis()
    {
        return catalogWikis;
    }

    public void setCatalogWikis(Boolean catalogWikis)
    {
        this.catalogWikis = catalogWikis;
    }

    public Boolean getCatalogTags()
    {
        return catalogTags;
    }

    public void setCatalogTags(Boolean catalogTags)
    {
        this.catalogTags = catalogTags;
    }

    public Boolean getCatalogRefCatalogs()
    {
        return catalogRefCatalogs;
    }

    public void setCatalogRefCatalogs(Boolean catalogRefCatalogs)
    {
        this.catalogRefCatalogs = catalogRefCatalogs;
    }

    public Boolean getWsPosts()
    {
        return wsPosts;
    }

    public void setWsPosts(Boolean wsPosts)
    {
        this.wsPosts = wsPosts;
    }
    
    public Boolean getTemplateForm()
    {
        return templateForm;
    }

    public void setTemplateForm(Boolean templateForm)
    {
        this.templateForm = templateForm;
    }

    public Boolean getTemplateWiki()
    {
        return templateWiki;
    }

    public void setTemplateWiki(Boolean templateWiki)
    {
        this.templateWiki = templateWiki;
    }
    
    public Boolean getExportDBPool()
    {
        return exportDBPool;
    }
    public void setExportDBPool(Boolean exportDBPool)
    {
        this.exportDBPool = exportDBPool;
    }

    public Boolean getExportRemedyxForm()
    {
        return exportRemedyxForm;
    }
    public void setExportRemedyxForm(Boolean exportRemedyxForm)
    {
        this.exportRemedyxForm = exportRemedyxForm;
    }

    public Boolean getIncludeSchema()
    {
        return includeSchema;
    }
    public void setIncludeSchema(Boolean includeSchema)
    {
        this.includeSchema = includeSchema;
    }

    public Boolean getIncludeMapping()
    {
        return includeMapping;
    }
    public void setIncludeMapping(Boolean includeMapping)
    {
        this.includeMapping = includeMapping;
    }

    public Boolean getIncludeUIDisplay()
    {
        return includeUIDisplay;
    }
    public void setIncludeUIDisplay(Boolean includeUIDisplay)
    {
        this.includeUIDisplay = includeUIDisplay;
    }

    public Boolean getIncludeUIAutomation()
    {
        return includeUIAutomation;
    }
    public void setIncludeUIAutomation(Boolean includeUIAutomation)
    {
        this.includeUIAutomation = includeUIAutomation;
    }

    public Boolean getIncludeGWAutomation()
    {
        return includeGWAutomation;
    }
    public void setIncludeGWAutomation(Boolean includeGWAutomation)
    {
        this.includeGWAutomation = includeGWAutomation;
    }

    public Boolean getIncludeSirPlaybook()
    {
        return includeSirPlaybook;
    }
    public void setIncludeSirPlaybook(Boolean includeSirPlaybook)
    {
        this.includeSirPlaybook = includeSirPlaybook;
    }

    public void copyWikiNsOptionsToWiki()
    {
        setWikiOverride(getWikiNsOverride());
        setWikiSubRB(getWikiNsSubRB());
        setWikiTags(getWikiNsTags());
        setWikiSubDT(getWikiNsSubDT());
        setWikiPost (getWikiNsPost());
        setWikiForms(getWikiNsForms());
        setWikiRefATs(getWikiNsRefATs()); // Action Tasks referred in Wiki
        setWikiDeleteIfSameSysId(getWikiNsDeleteIfSameSysId());
        setWikiDeleteIfSameName(getWikiNsDeleteIfSameName());
    }
    
    public void copyAtNsOptionsToAt()
    {
        setAtProperties(getAtNsProperties());
        setExcludeRefAssessor(getExcludeNsRefAssessor());
        setExcludeNsRefParser(getExcludeNsRefParser());
        setExcludeNsRefPreprocessor(getExcludeNsRefPreprocessor());
        setAtPost(getAtNsPosts());
    }

	public boolean isIncludeAllVersions()
	{
		return includeAllVersions;
	}

	public void setIncludeAllVersions(boolean includeAllVersions)
	{
		this.includeAllVersions = includeAllVersions;
	}

    public boolean isEmailAddress()
    {
        return emailAddress;
    }
    public void setEmailAddress(boolean emailAddress)
    {
        this.emailAddress = emailAddress;
    }

    public boolean isEwsAddress()
    {
        return ewsAddress;
    }
    public void setEwsAddress(boolean ewsAddress)
    {
        this.ewsAddress = ewsAddress;
    }

    public boolean isIncludeCustomKeys() {
        return includeCustomKeys;
    }
    public void setIncludeCustomKeys(boolean includeCustomKeys) {
        this.includeCustomKeys = includeCustomKeys;
    }
}
