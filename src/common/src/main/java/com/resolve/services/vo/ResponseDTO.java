/******************************************************************************
* (C) Copyright 2014
*
* Resolve Systems, LLC
*
* All rights reserved
* This software is distributed under license and may not be used, copied,
* modified, or distributed without the express written permission of
* Resolve Systems, LLC.
*
******************************************************************************/

package com.resolve.services.vo;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import com.resolve.util.StringUtils;

/**
 * This is a generic reponse from the server that the UI consumes in JSON form. Mostly this is used with conjunction 
 * with the 'QueryDTO'.
 * 
 * There are 2 different type of data/info being transferred 
 * records - list of records which is usually used in 'grid'. 'total' is # of available records and used for pagination 
 * data - single record, usually used when a 'record' is edited or more detail info is required for that record
 * 
 * To know if the request was a 'success' or 'failure', the 'success' flag will be 'true' 
 * and incase of failure, it will be 'false' and the 'message' will provide the reason of failure.
 *   
 *   
 * @author jeet.marwah
 *
 * @param <T>
 */
public class ResponseDTO<T>
{
    protected boolean success = true;
    protected String message;
    protected String errorCode;

    protected T data;
    protected List<T> records = null;
    protected long total = 0;

    public T getData()
    {
        return data;
    }

    public ResponseDTO<T> setData(T data)
    {
        this.data = data;
        return this;
    }

    /**
     * list of records returned to the UI (mostly used for the 'grid')
     * 
     * @return
     */
    public List<T> getRecords()
    {
        return records;
    }

    public ResponseDTO<T> setRecords(List<T> records)
    {
        this.records = records;
        return this;
    }

    public ResponseDTO<T> setRecordsFromCollection(Collection<T> c)
    {
        List<T> temp = new ArrayList<T>();
        for (T item : c)
        {
            temp.add(item);
        }
        return this.setRecords(temp);
    }

    /**
     * this is flag to indicate if the api call was a success (true) or failure (false).
     *  
     * @return
     */
    public boolean isSuccess()
    {
        return success;
    }

    public ResponseDTO<T> setSuccess(boolean isSuccess)
    {
        this.success = isSuccess;
        return this;
    }

    /**
     * returns the reason of failure
     * 
     * @return
     */
    public String getMessage()
    {
        return message;
    }

    public ResponseDTO<T> setMessage(String message)
    {
        this.message = message;
        return this;
    }

    /**
     * total # of records available.
     * 
     * For eg, in case of pagination, 'list' may return 50 records but the 'total' can be 150 thus helping the UI to indicate that there are 3 pages
     * 
     * 
     * @return
     */
    public long getTotal()
    {
        return total;
    }

    public ResponseDTO<T> setTotal(long total)
    {
        this.total = total;
        return this;
    }

    public boolean assertSuccess()
    {
        return this.success;
    }

    public boolean assertFailure()
    {
        return !this.success && StringUtils.isNotBlank(this.message);
    }

    public boolean assertListSuccess(int queryLimit)
    {
        // compare query start and limit with actual results in the response
        if (queryLimit < this.getTotal())
        {
            return this.getRecords().size() == this.getTotal();
        }
        else
        {
            // if( limit >= this.getTotal()) {
            return this.getRecords().size() == queryLimit;
        }
    }

    public String getErrorCode()
    {
        return errorCode;
    }

    public ResponseDTO<T> setErrorCode(String errorCode)
    {
        this.errorCode = errorCode;
        return this;
    }
}