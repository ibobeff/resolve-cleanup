/******************************************************************************
* (C) Copyright 2014
*
* Resolve Systems, LLC
*
* All rights reserved
* This software is distributed under license and may not be used, copied,
* modified, or distributed without the express written permission of
* Resolve Systems, LLC.
*
******************************************************************************/

package com.resolve.services.interfaces;

/**
 * A generic Interface for all the model object for display information
 * 
 * NOTE: No method names should start with 'get' as it will not work with Hibernate
 *  
 * @author jeet.marwah
 *
 */
public interface UIDisplayAPI
{
	public String ui_getDisplayType();
	public String ui_getDisplayName();
}
