/******************************************************************************
* (C) Copyright 2014
*
* Resolve Systems, LLC
*
* All rights reserved
* This software is distributed under license and may not be used, copied,
* modified, or distributed without the express written permission of
* Resolve Systems, LLC.
*
******************************************************************************/

package com.resolve.services.vo;


/**
 * use as a DTO for showing where the comps are used
 * For eg, for Assessor, which ones use this assessor, which assessor is this one using, which actiontask uses this assessor
 * 
 * @author jeet.marwah
 *
 */
public class ATReferenceDTO
{
    private String name;//document name referred by
    private String type;//actiontask, parser, preprocessor, assessor
    private String description;
    private String refInOrBy;//string to refered in or referred by
    
    public String getName()
    {
        return name;
    }

    public void setName(String name)
    {
        this.name = name;
    }

    public String getType()
    {
        return type;
    }

    public void setType(String type)
    {
        this.type = type;
    }

    public String getDescription()
    {
        return description;
    }

    public void setDescription(String description)
    {
        this.description = description;
    }

    public String getRefInOrBy()
    {
        return refInOrBy;
    }

    public void setRefInOrBy(String refInOrBy)
    {
        this.refInOrBy = refInOrBy;
    }

    @Override
    public boolean equals(Object otherObj)
    {
        boolean isEquals = false;
        if (otherObj instanceof ATReferenceDTO)
        {
            ATReferenceDTO that = (ATReferenceDTO) otherObj;
            // if sys_id are not equal, they are diff
            if (that.getName().equals(this.getName()))
            {
                isEquals = true;
            }
        }
        return isEquals;
    }

    @Override
    public int hashCode()
    {
        int hash = 1;
        hash = hash * 37 + this.getName().hashCode();
        hash = hash * 37 + (this.getName() == null ? 0 : this.getName().hashCode());
        return hash;
    }
    
}
