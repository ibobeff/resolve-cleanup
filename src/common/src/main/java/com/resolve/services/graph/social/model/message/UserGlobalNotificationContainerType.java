/******************************************************************************
* (C) Copyright 2014
*
* Resolve Systems, LLC
*
* All rights reserved
* This software is distributed under license and may not be used, copied,
* modified, or distributed without the express written permission of
* Resolve Systems, LLC.
*
******************************************************************************/

package com.resolve.services.graph.social.model.message;

import java.util.EnumSet;
import java.util.HashSet;
import java.util.Iterator;
import java.util.Set;

/**
 * 
 * IMPORTANT NOTE : This Enum should match exactly as the table SocialNotifications and the social_notifications.properties file
 * 
 * 
 * @author jeet.marwah
 *
 */

public enum UserGlobalNotificationContainerType //implements RelationshipType  
{
    //Document
    DOCUMENT_CREATE, DOCUMENT_UPDATE, 
    DOCUMENT_ACTIVE, DOCUMENT_INACTIVE,
    DOCUMENT_LOCKED, DOCUMENT_UNLOCKED,
    DOCUMENT_HIDE, DOCUMENT_UNHIDE,
    DOCUMENT_DELETE, DOCUMENT_UNDELETE,
    DOCUMENT_PURGED,  
    DOCUMENT_ADDED_TO_PROCESS, DOCUMENT_REMOVED_FROM_PROCESS, DOCUMENT_COMMIT, DOCUMENT_DIGEST_EMAIL, DOCUMENT_FORWARD_EMAIL,
    
    //Namespace
    NAMESPACE_DIGEST_EMAIL, NAMESPACE_FORWARD_EMAIL,
    
    //Actiontask
    ACTIONTASK_CREATE, ACTIONTASK_UPDATE, ACTIONTASK_PURGED, ACTIONTASK_ADDED_TO_PROCESS, ACTIONTASK_REMOVED_FROM_PROCESS, ACTIONTASK_DIGEST_EMAIL, ACTIONTASK_FORWARD_EMAIL,
    
    //WorkSheet
//    WORKSHEET_CREATE, WORKSHEET_UPDATE, WORKSHEET_PURGED, 
    WORKSHEET_DIGEST_EMAIL, WORKSHEET_FORWARD_EMAIL,
    
    //Rss
    RSS_CREATE, RSS_UPDATE, RSS_PURGED, RSS_ADDED_TO_PROCESS, RSS_REMOVED_FROM_PROCESS, RSS_DIGEST_EMAIL, RSS_FORWARD_EMAIL,
    
    //Forum
    FORUM_CREATE, FORUM_UPDATE, FORUM_PURGED, FORUM_ADDED_TO_PROCESS, FORUM_REMOVED_FROM_PROCESS, FORUM_USER_ADDED, FORUM_USER_REMOVED, FORUM_DIGEST_EMAIL, FORUM_FORWARD_EMAIL,
    
    //Team
    TEAM_CREATE, TEAM_UPDATE, TEAM_PURGED, TEAM_ADDED_TO_PROCESS, TEAM_REMOVED_FROM_PROCESS,
    TEAM_ADDED_TO_TEAM, TEAM_REMOVED_FROM_TEAM, TEAM_USER_ADDED, TEAM_USER_REMOVED, TEAM_DIGEST_EMAIL, TEAM_FORWARD_EMAIL,
    
    //Process
    PROCESS_CREATE, PROCESS_UPDATE, PROCESS_PURGED,  
    PROCESS_DOCUMENT_ADDED, PROCESS_DOCUMENT_REMOVED, 
    PROCESS_ACTIONTASK_ADDED, PROCESS_ACTIONTASK_REMOVED, 
    PROCESS_RSS_ADDED, PROCESS_RSS_REMOVED, 
    PROCESS_FORUM_ADDED, PROCESS_FORUM_REMOVED, 
    PROCESS_TEAM_ADDED, PROCESS_TEAM_REMOVED, 
    PROCESS_USER_ADDED, PROCESS_USER_REMOVED, PROCESS_DIGEST_EMAIL, PROCESS_FORWARD_EMAIL,
    
    //User
    USER_ADDED_TO_PROCESS, USER_ADDED_TO_TEAM, USER_FOLLOW_ME, USER_REMOVED_FROM_PROCESS, 
    USER_REMOVED_FROM_TEAM, USER_UNFOLLOW_ME, USER_ADDED_TO_FORUM, USER_REMOVED_FROM_FORUM, USER_LIKE_POST, USER_PROFILE_CHANGE, 
    USER_DIGEST_EMAIL, USER_FORWARD_EMAIL, USER_SYSTEM_DIGEST_EMAIL, USER_SYSTEM_FORWARD_EMAIL, USER_ALL_DIGEST_EMAIL, USER_ALL_FORWARD_EMAIL,
    
    //Playbook Template
    PLAYBOOK_TEMPLATE_CREATE, PLAYBOOK_TEMPLATE_UPDATE, PLAYBOOK_TEMPLATE_COMMIT, PLAYBOOK_TEMPLATE_DIGEST_EMAIL, PLAYBOOK_TEMPLATE_FORWARD_EMAIL; 
     
    public static final EnumSet<UserGlobalNotificationContainerType> allOfGlobalNotification = EnumSet.allOf(UserGlobalNotificationContainerType.class);
    
    //string representation of the types
    public static final Set<String> globalNotificationTypeSet = new HashSet<String>();
    static {
        Iterator<UserGlobalNotificationContainerType> it = UserGlobalNotificationContainerType.allOfGlobalNotification.iterator();
        while(it.hasNext())
        {
            UserGlobalNotificationContainerType t = it.next();
            globalNotificationTypeSet.add(t.name());
        }
    }
    
}

