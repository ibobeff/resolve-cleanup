/******************************************************************************
* (C) Copyright 2014
*
* Resolve Systems, LLC
*
* All rights reserved
* This software is distributed under license and may not be used, copied,
* modified, or distributed without the express written permission of
* Resolve Systems, LLC.
*
******************************************************************************/

package com.resolve.services.util;

import java.util.HashMap;
import java.util.Map;

import org.codehaus.jackson.map.ObjectMapper;
import org.codehaus.jackson.map.type.MapType;

import com.resolve.util.Log;
import com.resolve.util.StringUtils;

public class JsonUtils
{
    
    public static Map convertJsonStringToMap(String str)
    {
        Map result = new HashMap();
        
        if(StringUtils.isNotBlank(str))
        {
            try
            {
                ObjectMapper mapper = new ObjectMapper();
                MapType type = mapper.getTypeFactory().constructMapType(HashMap.class, String.class, Object.class);
                result = mapper.readValue(str, type);
            }
            catch (Exception e)
            {
                Log.log.error("error in converting str to map :" + str, e);
            }
        }
        
        return result;
    }
    
    public static String convertMapToJsonString(Map map)
    {
        String result = "";
        
        if(map != null)
        {
            try
            {
                result = new ObjectMapper().writeValueAsString(map);
            }
            catch(Exception e)
            {
                Log.log.error("error in converting map to json string :" + map, e);
            }
        }
        
        return result;
    }


    

}
