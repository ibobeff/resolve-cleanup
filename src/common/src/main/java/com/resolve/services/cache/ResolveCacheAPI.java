/******************************************************************************
* (C) Copyright 2014
*
* Resolve Systems, LLC
*
* All rights reserved
* This software is distributed under license and may not be used, copied,
* modified, or distributed without the express written permission of
* Resolve Systems, LLC.
*
******************************************************************************/

package com.resolve.services.cache;

/**
 * apis that needs to be implemented for any custom cache
 * 
 * @author jeet.marwah
 *
 */
public interface ResolveCacheAPI
{
    public void putCacheObj(String name, Object obj);
    public Object getCacheObj(String name);
    public boolean isKeyInCache(String name);
    public void removeCacheObj(String name);
    
}
