/******************************************************************************
* (C) Copyright 2014
*
* Resolve Systems, LLC
*
* All rights reserved
* This software is distributed under license and may not be used, copied,
* modified, or distributed without the express written permission of
* Resolve Systems, LLC.
*
******************************************************************************/

package com.resolve.services.vo;

import java.io.Serializable;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;


/**
 * this is a substitute for ResolveBaseModel which is being phased out
 * 
 * @author jeet.marwah
 *
 */
public class ResolveHashMap  extends HashMap<String, Object> implements  Serializable
{
    private static final long serialVersionUID = 322075996730591052L;

    public void set(String key, Object value)
    {
        put(key, value);
    }
    
    /**
     * utility method to add the map values to model
     * 
     * @param map
     */
    public void addHashMap(Map<String, String> map)
    {
        Iterator<String> it = map.keySet().iterator();
        while(it.hasNext())
        {
            String key = it.next();
            String value = map.get(key);
            
            put(key, value);
        }//end of while loop
    }//addHashMap
}//ResolveHashMap

