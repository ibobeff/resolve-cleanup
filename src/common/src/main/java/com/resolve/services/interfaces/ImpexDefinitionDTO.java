/******************************************************************************
* (C) Copyright 2014
*
* Resolve Systems, LLC
*
* All rights reserved
* This software is distributed under license and may not be used, copied,
* modified, or distributed without the express written permission of
* Resolve Systems, LLC.
*
******************************************************************************/

package com.resolve.services.interfaces;

import java.util.Date;


public class ImpexDefinitionDTO
{
    private String sys_id;
    private String type;
    private String name;//option
    private String namespace;//mandatory
    private String description;//json
    private String sysUpdatedBy;
    private Date sysUpdatedOn;
    
    private ImpexOptionsDTO options;
    
    public String getSys_id()
    {
        return sys_id;
    }

    public void setSys_id(String sys_id)
    {
        this.sys_id = sys_id;
    }

    public String getType()
    {
        return type;
    }

    public void setType(String type)
    {
        this.type = type;
    }

    public String getName()
    {
        return name;
    }

    public void setName(String name)
    {
        this.name = name;
    }

    public String getNamespace()
    {
        return namespace;
    }

    public void setNamespace(String namespace)
    {
        this.namespace = namespace;
    }

    public String getDescription()
    {
        return description;
    }

    public void setDescription(String description)
    {
        this.description = description;
    }

    public ImpexOptionsDTO getOptions()
    {
        return options;
    }

    public void setOptions(ImpexOptionsDTO options)
    {
        this.options = options;
    }

    public String getSysUpdatedBy()
    {
        return sysUpdatedBy;
    }

    public void setSysUpdatedBy(String sysUpdatedBy)
    {
        this.sysUpdatedBy = sysUpdatedBy;
    }

    public Date getSysUpdatedOn()
    {
        return sysUpdatedOn;
    }

    public void setSysUpdatedOn(Date sysUpdatedOn)
    {
        this.sysUpdatedOn = sysUpdatedOn;
    }
}
