/******************************************************************************
* (C) Copyright 2014
*
* Resolve Systems, LLC
*
* All rights reserved
* This software is distributed under license and may not be used, copied,
* modified, or distributed without the express written permission of
* Resolve Systems, LLC.
*
******************************************************************************/

package com.resolve.esb;

import com.resolve.util.Constants;

public class MServerConfig
{
    public static final int DEFAULT_MAX_QUEUE_MESSAGE_TTL = 1200000; // 20 min.
    public static final int DEFAULT_QUEUE_TTL = 60000; // 1 min.
    public String product;
    public String guid; //this is actually the GUID of the component (e.g., rscontrol's guid
    public String name;
    public String brokerAddr;
    public String brokerAddr2;
    public String username;
    public String p_assword;
    public int createQueueRetry;
    public int createQueueWait;
    public int keepalive;
    public int maxfailed;
    public int maxlistenthreads;
    public int cache_size;
    public int max_messages;
    public int messageTTL; // Min is 10 min (DEFAULT_MAX_QUEUE_MESSAGE_TTL / 2)
    public boolean flushQueue;
    public int eventQueueLimit=1000;
    public String sslProtocol;
    public int connectionTimeout=5000;
	private int maxexecutionevents=8;
    public MServerConfig(String product)
    {
        this.product = product;

        if (Constants.ESB_RABBITMQ.equalsIgnoreCase(product))
        {
            init_RabbitMQ();
        }

    } // MServer

    public MServerConfig(String product, String guid, String name)
    {
        this.product = product;
        this.guid = guid;
        this.name = name;

        if (Constants.ESB_RABBITMQ.equalsIgnoreCase(product))
        {
            init_RabbitMQ();
        }

    } // MServer

    public void init_RabbitMQ()
    {
        this.product                        = Constants.ESB_RABBITMQ;
        this.brokerAddr                     = "localhost:5673";    // port 5673 (plainsocket) 5674 (sslsocket)
        this.brokerAddr2                    = "";
        this.username                       = "guest";
        this.p_assword                      = "guest";
        this.createQueueRetry               = 30;
        this.createQueueWait                = 10;
        this.keepalive                      = 0;
        this.maxfailed                      = 10;
        this.cache_size                     = 500;                 // max number of messages cached
        this.maxlistenthreads               = 10;                  // 8 cores + 2
        this.max_messages                   = -1;                  // max number of messages stored
        this.flushQueue                     = false;               // flushQueue on startup
        this.messageTTL                     = DEFAULT_MAX_QUEUE_MESSAGE_TTL;             // message ttl default 20 min
        this.sslProtocol                    ="TLSv1.2";
        this.connectionTimeout					=5000;
    } // init_RabbitMQ

    public String getProduct()
    {
        return product;
    }
    public void setProduct(String product)
    {
        this.product = product;
    }
    public String getGuid()
    {
        return guid;
    }
    public void setGuid(String guid)
    {
        this.guid = guid;
    }
    public String getName()
    {
        return name;
    }
    public void setName(String name)
    {
        this.name = name;
    }
    public String getBrokerAddr()
    {
        return brokerAddr;
    }
    public void setBrokerAddr(String brokerAddr)
    {
        this.brokerAddr = brokerAddr;
    }
    public String getBrokerAddr2()
    {
        return brokerAddr2;
    }
    public void setBrokerAddr2(String brokerAddr)
    {
        this.brokerAddr2 = brokerAddr;
    }
    public int getMaxExecutionEvents() {
    	return this.maxexecutionevents;
    }
    public void setMaxExecutionEvents(int maxexecutionevents) {
    	this.maxexecutionevents = maxexecutionevents;
    }
    public String getP_assword()
    {
        return p_assword;
    }
    public void setP_assword(String password)
    {
        this.p_assword = password;
    }
    public String getUsername()
    {
        return username;
    }
    public void setUsername(String username)
    {
        this.username = username;
    }
    public int getCreateQueueRetry()
    {
        return createQueueRetry;
    }
    public void setCreateQueueRetry(int createQueueRetry)
    {
        this.createQueueRetry = createQueueRetry;
    }
    public int getCreateQueueWait()
    {
        return createQueueWait;
    }
    public void setCreateQueueWait(int createQueueWait)
    {
        this.createQueueWait = createQueueWait;
    }
    public int getKeepalive()
    {
        return keepalive;
    }
    public void setKeepalive(int keepalive)
    {
        this.keepalive = keepalive;
    }
    public int getMaxFailed()
    {
        return maxfailed;
    }
    public void setMaxFailed(int maxfailed)
    {
        this.maxfailed = maxfailed;
    }
    public int getMaxListenThreads()
    {
        return maxlistenthreads;
    }
    public void setMaxListenThreads(int maxlistenthreads)
    {
        this.maxlistenthreads = maxlistenthreads;
    }
    public int getCacheSize()
    {
        return cache_size;
    }
    public void setCacheSize(int cache_size)
    {
        this.cache_size = cache_size;
    }
    public int getMaxMessages()
    {
        return max_messages;
    }
    public void setMaxMessages(int max_messages)
    {
        this.max_messages = max_messages;
    }
    public int getMessageTTL()
    {
        return messageTTL;
    }
    public void setMessageTTL(int messageTTL)
    {
        if (messageTTL < (DEFAULT_MAX_QUEUE_MESSAGE_TTL / 2))
        {
            this.messageTTL = DEFAULT_MAX_QUEUE_MESSAGE_TTL / 2;
        }
        else if (messageTTL > DEFAULT_MAX_QUEUE_MESSAGE_TTL)
        {
            this.messageTTL = DEFAULT_MAX_QUEUE_MESSAGE_TTL;
        }
        else
        {
            this.messageTTL = messageTTL;
        }
    }
    public boolean isFlushQueue()
    {
        return flushQueue;
    }
    public void setFlushQueue(boolean flushQueue)
    {
        this.flushQueue = flushQueue;
    }

    public int getEventQueueLimit()
    {
        return eventQueueLimit;
    }
    public void setEventQueueLimit(int limit)
    {
        this.eventQueueLimit = limit;
    }

    public String getSslProtocol()
    {
        return sslProtocol;
    }

    public void setSslProtocol(String sslProtocol)
    {
        this.sslProtocol = sslProtocol;
    }

	public int getConnectionTimeout() {
		return connectionTimeout;
	}

	public void setConnectionTimeout(int connectionTimeout) {
		this.connectionTimeout = connectionTimeout;
	}
    
} // MServerConfig