/******************************************************************************
* (C) Copyright 2014
*
* Resolve Systems, LLC
*
* All rights reserved
* This software is distributed under license and may not be used, copied,
* modified, or distributed without the express written permission of
* Resolve Systems, LLC.
*
******************************************************************************/

package com.resolve.esb;

import javax.jms.Message;
import javax.jms.TextMessage;
import javax.jms.MapMessage;
import javax.jms.ObjectMessage;
import javax.jms.JMSException;

import java.lang.Runnable;
import java.util.Map;
import java.util.Hashtable;
import java.util.HashMap;
import java.util.Enumeration;
import java.util.List;
import java.util.ArrayList;

import com.resolve.util.Log;

public class MHandler implements Runnable
{
    String type;
    Object msg;
    Message jmsMsg;
    MMsgHeader msgHeader;
    MSender mSender;
    MListener mListener;

    public MHandler()
    {
    } // MHandler

    public Hashtable getMapMessageContent(MapMessage msg) throws JMSException
    {
        Hashtable result = new Hashtable();

        // extract content
        for (Enumeration keys = msg.getMapNames(); keys.hasMoreElements();)
        {
            String key = (String)keys.nextElement();

            result.put(key, msg.getObject(key));
        }

        return result;
    } // getMapMessageContent

    public String getType()
    {
        return type;
    } // getType

    public void setType(String type)
    {
        this.type = type.toUpperCase();
    } // setType

    public Object getMsgString()
    {
        return msg;
    } // getMsgString

    public Hashtable getMsgHashtable()
    {
        return (Hashtable)msg;
    } // getMsgHashtable

    public Object getMsgObject()
    {
        return msg;
    } // getMsgObject

    public Class getMsgClass()
    {
        Class result = null;

        if (msg instanceof String)
        {
            result = String.class;
        }
        else if (msg instanceof ArrayList)
        {
            result = List.class;
        }
        else if (msg instanceof Hashtable)
        {
            result = Map.class;
        }
        else if (msg instanceof HashMap)
        {
            result = Map.class;
        }
        else
        {
            result = Object.class;
        }

        return result;
    } // getMsgClass

    public void setMsg(Message message) throws JMSException
    {
        this.jmsMsg = message;
        if (message instanceof TextMessage)
        {
            this.msg = ((TextMessage)message).getText();
        }
        else if (message instanceof MapMessage)
        {
            this.msg = getMapMessageContent((MapMessage)message);
        }
        else  if (message instanceof ObjectMessage)
        {
            this.msg = ((ObjectMessage)message).getObject();
        }
    } // setMsg

    public void setHeader(MMsgHeader header)
    {
        this.msgHeader = header;
    } // setHeader

    public MSender getMSender()
    {
        return mSender;
    } // getMSender

    public void setMSender(MSender mSender)
    {
        this.mSender = mSender;
    } // setMSender

    public void setMListener(MListener mListener)
    {
        this.mListener = mListener;
    } // setMListener

    //This method becomes final. All subclasses should override runInternal() instead.
    public final void run()
    {
        try
        {
            runInternal();
        }
        finally
        {
            if (jmsMsg != null)
            {
                try
                {
                    //Acknowledge message for all handler subclasses
                    (jmsMsg).acknowledge();
                } catch (Exception ex)
                {
                    Log.log.warn("MHandler exception: "+ex.getMessage(), ex);
                }
            }
        }
    } // run

    //Needs to be overridden by all subclasses
    protected void runInternal()
    {
        throw new IllegalStateException("Subclass must override runInternal() method.");
    } //runInternal

} // MHandler
