/******************************************************************************
* (C) Copyright 2014
*
* Resolve Systems, LLC
*
* All rights reserved
* This software is distributed under license and may not be used, copied,
* modified, or distributed without the express written permission of
* Resolve Systems, LLC.
*
******************************************************************************/

package com.resolve.esb;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.resolve.util.Constants;

public class MMsgOptions extends HashMap
{
    final static String MSGOPTION = "_MSGOPTION_";
    
    // routing options
    final static String MSGIGNOREGUID = MSGOPTION + "IGNOREGUID";
    
    // file message options
    final static String MSGFILENAME = MSGOPTION + "FILENAME";
    final static String MSGFILECOMPLETE = MSGOPTION + "FILECOMPLETE";
    final static String MSGFILECHECKSUM = MSGOPTION + "FILECHECKSUM";
    final static String MSGFILEPIECE = MSGOPTION + "FILEPIECE";
    final static String MSGCHECKSUM = MSGOPTION + "CHECKSUM";
    
    // execute options
    final static String MSGACTIONNAME = MSGOPTION + Constants.EXECUTE_ACTIONNAME;
    final static String MSGACTIONNAMESPACE = MSGOPTION + Constants.EXECUTE_ACTIONNAMESPACE;
    final static String MSGACTIONSUMMARY = MSGOPTION + Constants.EXECUTE_ACTIONSUMMARY;
    final static String MSGACTIONHIDDEN = MSGOPTION + Constants.EXECUTE_ACTIONHIDDEN;
    final static String MSGACTIONTAGS = MSGOPTION + Constants.EXECUTE_ACTIONTAGS;
    final static String MSGACTIONROLES = MSGOPTION + Constants.EXECUTE_ACTIONROLES;
    final static String MSGACTIONID = MSGOPTION + Constants.EXECUTE_ACTIONID;
    final static String MSGPROCESSID = MSGOPTION + Constants.EXECUTE_PROCESSID;
    final static String MSGWIKI = MSGOPTION + Constants.EXECUTE_WIKI;
    final static String MSGEXECUTEID = MSGOPTION + Constants.EXECUTE_EXECUTEID;
    final static String MSGPARSERID = MSGOPTION + Constants.EXECUTE_PARSERID;
    final static String MSGASSESSID = MSGOPTION + Constants.EXECUTE_ASSESSID;
    final static String MSGDURATION = MSGOPTION + Constants.EXECUTE_DURATION;
    final static String MSGLOGRESULT = MSGOPTION + Constants.EXECUTE_LOGRESULT;
    final static String MSGUSERID = MSGOPTION + Constants.EXECUTE_USERID;
    final static String MSGPROBLEMID = MSGOPTION + Constants.EXECUTE_PROBLEMID;
    final static String MSGREFERENCE = MSGOPTION + Constants.EXECUTE_REFERENCE;
    final static String MSGRETURNCODE = MSGOPTION + Constants.EXECUTE_RETURNCODE;
    final static String MSGCOMPLETION = MSGOPTION + Constants.EXECUTE_COMPLETION;
    final static String MSGCOMMAND = MSGOPTION +Constants.EXECUTE_COMMAND;
    final static String MSGTIMEOUT = MSGOPTION + Constants.EXECUTE_TIMEOUT;
    final static String MSGPROCESSTIMEOUT = MSGOPTION + Constants.EXECUTE_PROCESS_TIMEOUT;
    final static String MSGMETRICID = MSGOPTION + Constants.EXECUTE_METRICID;
    final static String MSGMOCK = MSGOPTION + Constants.EXECUTE_MOCK;
    final static String MSGADDRESS = MSGOPTION + Constants.EXECUTE_ADDRESS;     // TargetInstance.address
    final static String MSGTARGET = MSGOPTION + Constants.EXECUTE_TARGET;       // TargetInstance.guid
    final static String MSGAFFINITY = MSGOPTION + Constants.EXECUTE_AFFINITY;   // TargetInstance.affinity
    final static String MSGTASKNAME = MSGOPTION + Constants.EXECUTE_TASKNAME;
    final static String MSGTYPE = MSGOPTION + Constants.EXECUTE_TYPE;
    final static String MSGARGS = MSGOPTION + Constants.EXECUTE_ARGS;
    final static String MSGINPUT = MSGOPTION + Constants.EXECUTE_INPUT;
    final static String MSGDELAY = MSGOPTION + Constants.EXECUTE_DELAY;
    final static String MSGREQUESTTIMESTAMP = MSGOPTION + Constants.EXECUTE_REQUEST_TIMESTAMP;
    final static String MSGRESULTADDR = MSGOPTION + Constants.EXECUTE_RESULTADDR;
    final static String MSGBLANKVALUES = MSGOPTION + Constants.EXECUTE_BLANKVALUES;
    
    public MMsgOptions()
    {
        super();
    } // MMsgOptions
    
    public MMsgOptions(Map data)
    {
        this.putAll(data);
    } // MMsgOptions 

    public MMsgOptions(String msgOptions)
    {
        super();
        if (msgOptions != null)
        {
            String[] options = msgOptions.split("&");
            for (int i=0; i<options.length; i++)
            {
                String[] option = options[i].split("=");
                if (option.length == 2)
                {
                    String key = option[0];
                    String value = option[1];
                    
                    if (key.startsWith(MSGOPTION))
                    {
                        this.put(key, value);
                    }
                }
            }
        }
    } // MMsgOptions
    
    public MMsgOptions(MMsgHeader msgHeader)
    {
        super();
        if (msgHeader != null)
        {
            for (Object key : msgHeader.keySet())
            {
                Object value = msgHeader.get(key);
                if (key instanceof String && (value instanceof String || value instanceof Integer))
                {
                    if (((String) key).startsWith(MSGOPTION))
                    {
                        this.put((String) key, value);
                    }
                }
            }
        }
    } // MMsgOptions
    
    public String toString()
    {
        StringBuffer result = new StringBuffer();
        
        for (Object key : this.keySet())
        {
            result.append(key);
            result.append("=");
            result.append(this.get(key));
            result.append("&");
        }
        
        return result.toString();
    } // toString
    
    public String getFormattedString()
    {
        StringBuffer result = new StringBuffer();
        
        for (Object key : this.keySet())
        {
            String keyStr = key.toString();
            keyStr = keyStr.replaceFirst(MSGOPTION, "");
            
            result.append(keyStr);
            result.append("=");
            result.append(this.get(key));
            result.append(" ");
        }
        
        return result.toString();
    } // getFormattedString
    
    public void setFileName(String fileName)
    {
        if (fileName != null)
        {
            this.put(MSGFILENAME, fileName);
        }
    } // setFileName
    
    public String getFileName()
    {
        return (String) this.get(MSGFILENAME);
    } // getFileName
    
    public void setFileComplete(String fileComplete)
    {
        if (fileComplete != null)
        {
            this.put(MSGFILECOMPLETE, fileComplete);
        }
    } // setFileComplete
    
    public void setFileComplete(Boolean fileComplete)
    {
        if (fileComplete != null)
        {
	        this.put(MSGFILECOMPLETE, String.valueOf(fileComplete));
        }
    } // setFileComplete
    
    public String getFileComplete()
    {
        return (String) this.get(MSGFILECOMPLETE);
    } // getFileComplete
    
    public void setFileChecksum(String fileChecksum)
    {
        if (fileChecksum != null)
        {
            this.put(MSGFILECHECKSUM, fileChecksum);
        }
    } // setFileChecksum
    
    public String getFileChecksum()
    {
        return (String) this.get(MSGFILECHECKSUM);
    } // getFileChecksum
    
    public void setFilePiece(String filePiece)
    {
        if (filePiece != null)
        {
            this.put(MSGFILEPIECE, filePiece);
        }
    } //setFilePiece
    
    public void setFilePiece(Integer filePiece)
    {
        if (filePiece != null)
        {
	        this.put(MSGFILEPIECE, String.valueOf(filePiece));
        }
    } // setFilePiece
    
    public String getFilePiece()
    {
        return (String) this.get(MSGFILEPIECE);
    } // getFilePiece
    
    public void setChecksum(String checksum)
    {
        if (checksum != null)
        {
            this.put(MSGCHECKSUM, checksum);
        }
    } // setChecksum
    
    public String getChecksum()
    {
        return (String) this.get(MSGCHECKSUM);
    } // getChecksum
    
    public void addIgnoreGUID(String guid)
    {
        String ignore = getIgnoreGUID();
        
        if (ignore == null)
        {
            ignore = guid;
        }
        else
        {
            ignore = ignore + "," + guid;
        }
        
        this.put(MSGIGNOREGUID, ignore);
    } // addIgnoreGUID
    
    public void addIgnoreGUIDs(List<String> guids)
    {
        for(String guid : guids)
        {
            addIgnoreGUID(guid);
        }
    } // addIgnoreGUIDs
    
    public void removeIgnoreGUID(String guid)
    {
        String ignore = getIgnoreGUID();
        
        if (ignore != null)
        {
            ignore = ignore.replaceAll(guid, "");
            ignore = ignore.replaceAll(",,", ",");
            
            this.put(MSGIGNOREGUID, ignore);
        }
    } // removeIgnoreGUID
    
    public String getIgnoreGUID()
    {
        String ignore = (String) this.get(MSGIGNOREGUID);
        
        return ignore;
    } // getIgnoreGUID
    
    public String getActionName()
    {
        String actionName = (String) this.get(MSGACTIONNAME);

        return actionName;
    } // getActionName

    public void setActionName(String actionName)
    {
        if (actionName != null)
        {
            this.put(MSGACTIONNAME, actionName);
        }
    } // setActionName

    public String getActionNamespace()
    {
        String actionNamespace = (String) this.get(MSGACTIONNAMESPACE);

        return actionNamespace;
    } // getActionNamespace

    public void setActionNamespace(String actionNamespace)
    {
        if (actionNamespace != null)
        {
            this.put(MSGACTIONNAMESPACE, actionNamespace);
        }
    } // setActionNamespace

    public String getActionSummary()
    {
        String actionSummary = (String) this.get(MSGACTIONSUMMARY);

        return actionSummary;
    } // getActionSummary

    public void setActionSummary(String actionSummary)
    {
        if (actionSummary != null)
        {
            this.put(MSGACTIONSUMMARY, actionSummary);
        }
    } // setActionSummary

    public String getActionHidden()
    {
        String actionHidden = (String) this.get(MSGACTIONHIDDEN);

        return actionHidden;
    } // getActionHidden

    public void setActionHidden(String actionHidden)
    {
        if (actionHidden != null)
        {
            this.put(MSGACTIONHIDDEN, actionHidden);
        }
    } // setActionHidden

    public String getActionTags()
    {
        String actionTags = (String) this.get(MSGACTIONTAGS);

        return actionTags;
    } // getActionHidden

    public void setActionTags(String actionTags)
    {
        if (actionTags != null)
        {
            this.put(MSGACTIONTAGS, actionTags);
        }
    } // setActionHidden

    public String getActionRoles()
    {
        String actionRoles = (String) this.get(MSGACTIONROLES);

        return actionRoles;
    } // getActionRoles

    public void setActionRoles(String actionRoles)
    {
        if (actionRoles != null)
        {
            this.put(MSGACTIONROLES, actionRoles);
        }
    } // setActionRoles
    
    public String getActionID()
    {
        String actionID = (String) this.get(MSGACTIONID);

        return actionID;
    } // getActionID

    public void setActionID(String actionID)
    {
        if (actionID != null)
        {
            this.put(MSGACTIONID, actionID);  
        }
    } // setActionID

    public String getProcessID()
    {
        String processID = (String) this.get(MSGPROCESSID);

        return processID;
    } // getProcessID

    public void setProcessID(String processID)
    {
        if (processID != null)
        {
            this.put(MSGPROCESSID, processID);
        }
    } // setProcessID

    public String getWiki()
    {
        String wiki = (String) this.get(MSGWIKI);

        return wiki;
    } // getWiki

    public void setWiki(String wiki)
    {
        if (wiki != null)
        {
            this.put(MSGWIKI, wiki);
        }
    } // setWiki

    public String getExecuteID()
    {
        String executeID = (String) this.get(MSGEXECUTEID);

        return executeID;
    } // getExecuteID

    public void setExecuteID(String executeID)
    {
        if (executeID != null)
        {
            this.put(MSGEXECUTEID, executeID);
        }
    } // setExecuteID

    public String getParserID()
    {
        String parserID = (String) this.get(MSGPARSERID);

        return parserID;
    } // getParserID

    public void setParserID(String parserID)
    {
        if (parserID != null)
        {
            this.put(MSGPARSERID, parserID);
        }
    } // setParserID

    public String getAssessID()
    {
        String assessID = (String) this.get(MSGASSESSID);

        return assessID;
    } // getAssessID

    public void setAssessID(String assessID)
    {
        if (assessID != null)
        {
            this.put(MSGASSESSID, assessID);
        }
    } // setAssessID

    public String getDuration()
    {
        String duration = (String) this.get(MSGDURATION);

        return duration;
    } // getDuration

    public void setDuration(String duration)
    {
        if (duration != null)
        {
            this.put(MSGDURATION, duration);
        }
    } // setDuration
    
    public void setDuration(long duration)
    {
        this.put(MSGDURATION, String.valueOf(duration));
    } // setDuration

    public String getLogResult()
    {
        String logResult = (String) this.get(MSGLOGRESULT);

        return logResult;
    } // getLogResult

    public void setLogResult(String logResult)
    {
        if (logResult != null)
        {
            this.put(MSGLOGRESULT, logResult);
        }
    } // setLogResult

    public String getUserID()
    {
        String userID = (String) this.get(MSGUSERID);

        return userID;
    } // getUserID

    public void setUserID(String userID)
    {
        if (userID != null)
        {
            this.put(MSGUSERID, userID);
        }
    } // setUserID

    public String getProblemID()
    {
        String problemID = (String) this.get(MSGPROBLEMID);

        return problemID;
    } // getProblemID

    public void setProblemID(String problemID)
    {
        if (problemID != null)
        {
            this.put(MSGPROBLEMID, problemID);
        }
    } // setProblemID

    public String getReference()
    {
        String reference = (String) this.get(MSGREFERENCE);

        return reference;
    } // getReference

    public void setReference(String reference)
    {
        if (reference != null)
        {
            this.put(MSGREFERENCE, reference);
        }
    } // setReference

    public String getReturnCode()
    {
        String returnCode = (String) this.get(MSGRETURNCODE);

        return returnCode;
    } // getReturnCode

    public void setReturnCode(String returnCode)
    {
        if (returnCode != null)
        {
            this.put(MSGRETURNCODE, returnCode);
        }
    } // setReturnCode
    
    public void setReturnCode(Integer returnCode)
    {
        if (returnCode != null)
        {
	        this.put(MSGRETURNCODE, String.valueOf(returnCode));
        }
    } //setReturnCode

    public String getCompletion()
    {
        String completion = (String) this.get(MSGCOMPLETION);

        return completion;
    } // getCompletion

    public void setCompletion(String completion)
    {
        if (completion != null)
        {
            this.put(MSGCOMPLETION, completion);
        }
    } // setCompletion
    
    public void setCompletion(Boolean completion)
    {
        if (completion != null)
        {
	        this.put(MSGCOMPLETION, String.valueOf(completion));
        }
    } // setCompletion

    public String getCommand()
    {
        String command = (String) this.get(MSGCOMMAND);

        return command;
    } // getCommand

    public void setCommand(String command)
    {
        if (command != null)
        {
            this.put(MSGCOMMAND, command);
        }
    } // setCommand

    public String getTimeout()
    {
        String timeout = (String) this.get(MSGTIMEOUT);

        return timeout;
    } // getTimeout

    public void setTimeout(String timeout)
    {
        if (timeout != null)
        {
            this.put(MSGTIMEOUT, timeout);
        }
    } // setTimeout
    
    public void setTimeout(Integer timeout)
    {
        if (timeout != null)
        {
	        this.put(MSGTIMEOUT, String.valueOf(timeout));
        }
    } // setTimeout

    public String getProcessTimeout()
    {
        String processTimeout = (String) this.get(MSGPROCESSTIMEOUT);

        return processTimeout;
    } // getProcessTimeout

    public void setProcessTimeout(String processTimeout)
    {
        if (processTimeout != null)
        {
            this.put(MSGPROCESSTIMEOUT, processTimeout);
        }
    } // setProcessTimeout
    
    public void setProcessTimeout(Integer processTimeout)
    {
        if (processTimeout != null)
        {
	        this.put(MSGPROCESSTIMEOUT, String.valueOf(processTimeout));
        }
    } // setProcessTimeout
    
    public String getMetricId()
    {
        String metricId = (String) this.get(MSGMETRICID);

        return metricId;
    } // getMetricId

    public void setMetricId(String metricId)
    {
        if (metricId != null)
        {
            this.put(MSGMETRICID, metricId);
        }
    } // setMetricId
    
    public String getMock()
    {
        String mock = (String) this.get(MSGMOCK);

        return mock;
    } // getMock

    public void setMock(String mock)
    {
        if (mock != null)
        {
            this.put(MSGMOCK, mock);
        }
    } // setMock
    
    public String getTarget()
    {
        String target = (String) this.get(MSGTARGET);

        return target;
    } // getTarget

    public void setTarget(String target)
    {
        if (target != null)
        {
            this.put(MSGTARGET, target);
        }
    } // setTarget
    
    public String getAffinity()
    {
        String affinity = (String) this.get(MSGAFFINITY);

        return affinity;
    } // getAffinity

    public void setAffinity(String affinity)
    {
        if (affinity != null)
        {
            this.put(MSGAFFINITY, affinity);
        }
    } // setAffinity
    
    public String getAddress()
    {
        String address = (String) this.get(MSGADDRESS);

        return address;
    } // getTarget

    public void setAddress(String address)
    {
        if (address != null)
        {
            this.put(MSGADDRESS, address);
        }
    } // setAddress
    
    public String getTaskname()
    {
        String taskname = (String) this.get(MSGTASKNAME);

        return taskname;
    } // getTaskname

    public void setTaskname(String taskname)
    {
        if (taskname != null)
        {
            this.put(MSGTASKNAME, taskname);
        }
    } // setTaskname
    
    public String getType()
    {
        String type = (String) this.get(MSGTYPE);

        return type;
    } // getType

    public void setType(String type)
    {
        if (type != null)
        {
            this.put(MSGTYPE, type);
        }
    } // setType
    
    public String getInput()
    {
        String input = (String) this.get(MSGINPUT);

        return input;
    } // getInput

    public void setInput(String input)
    {
        if (input != null)
        {
            this.put(MSGINPUT, input);
        }
    } // setInput
    
    public String getArgs()
    {
        String args = (String) this.get(MSGARGS);

        return args;
    } // getArgs

    public void setArgs(String args)
    {
        if (args != null)
        {
            this.put(MSGARGS, args);
        }
    } // setArgs
    
    public String getDelay()
    {
        String delay = (String) this.get(MSGDELAY);

        return delay;
    } // getDelay

    public void setDelay(String delay)
    {
        if (delay != null)
        {
            this.put(MSGDELAY, delay);
        }
    } // setDelay
    
    public void setDelay(long delay)
    {
        this.put(MSGDELAY, ""+delay);
    } // setDelay
    
    public String getRequestTimestamp()
    {
        String timestamp = (String) this.get(MSGREQUESTTIMESTAMP);

        return timestamp;
    } // getRequestTimestamp

    public void setRequestTimestamp(String timestamp)
    {
        if (timestamp != null)
        {
            this.put(MSGREQUESTTIMESTAMP, timestamp);
        }
    } // setRequestTimestamp
    
    public void setRequestTimestamp(long timestamp)
    {
        this.put(MSGREQUESTTIMESTAMP, ""+timestamp);
    } // setRequestTimestamp
    
    public String getResultAddr()
    {
        String resultAddr = (String) this.get(MSGRESULTADDR);

        return resultAddr;
    } // getResultAddr
    
    public void setResultAddr(String resultAddr)
    {
        if (resultAddr != null)
        {
            this.put(MSGRESULTADDR, resultAddr);
        }
    } // setResultAddr
    
    public String getBlankValues()
    {
        String blankValues = (String) this.get(MSGBLANKVALUES);

        return blankValues;
    } // getBlankValues
    
    public void setBlankValues(String blankValues)
    {
        if (blankValues != null)
        {
            this.put(MSGBLANKVALUES, blankValues);
        }
    } // setBlankValues
    
} // MMsgOptions
