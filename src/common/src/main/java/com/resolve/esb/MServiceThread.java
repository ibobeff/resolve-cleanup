/******************************************************************************
* (C) Copyright 2014
*
* Resolve Systems, LLC
*
* All rights reserved
* This software is distributed under license and may not be used, copied,
* modified, or distributed without the express written permission of
* Resolve Systems, LLC.
*
******************************************************************************/

package com.resolve.esb;

import java.util.List;
import java.util.concurrent.DelayQueue;
import java.util.concurrent.Delayed;
import java.util.concurrent.TimeUnit;

import com.resolve.util.Log;

public abstract class MServiceThread implements Runnable
{
    protected String name = null;
    protected long nanosDelay = 0;
    protected boolean isRunning = false;
    protected DelayQueue<Delayed> blockingQueue = new DelayQueue<Delayed>();

    public MServiceThread(final String serviceQueue, ESB esb)
    {
        this(serviceQueue, esb, 0);
    } // MServiceThread

    public MServiceThread(final String serviceQueue, ESB esb, long nanosDelay)
    {
        if (esb != null)
        {
            this.name = serviceQueue;
            this.nanosDelay = nanosDelay;

            List<MListener> listeners = esb.mServer.getListeners();
            if (listeners != null)
            {
                for (MListener listener : listeners)
                {
                    if (listener != null)
                    {
                        listener.addServiceThread(serviceQueue, this);
                    }
                }
            }
            else
            {
                Log.log.error("No MListeners initialized");
            }
        }
        else
        {
            Log.log.error("ESB not initialized");
        }
    } // MServiceThread

    abstract public void execute(Object params);

    public void run()
    {
        while (isRunning)
        {
            try
            {
                Log.log.trace("ServiceQueue: "+name+" size:"+blockingQueue.size());
                DelayedMessage msg = (DelayedMessage)blockingQueue.take();

                Log.log.trace("Executing serviceQueue: "+name);
                this.execute(msg.getMsg());

            }
            catch (Throwable t)
            {
                Log.log.error(t.getMessage(), t);
            }
        }
    } // run

    public void start()
    {
        isRunning = true;
        new Thread(this).start();
    } // start

    public void stop()
    {
        isRunning = false;
    } // stop

    public void addMessage(Object msg)
    {
        blockingQueue.add(new DelayedMessage(msg, nanosDelay));
    } // addMessage


} // MServiceThread

class DelayedMessage implements Delayed
{
    Object msg;
    long expirationTime;

    public DelayedMessage(Object msg, long nanosDelay)
    {
        this.msg = msg;
        this.expirationTime = System.nanoTime() + nanosDelay;
    } // DelayedMessage

    public Object getMsg()
    {
        return msg;
    }

    public long getExpirationTime()
    {
        return expirationTime;
    }

    public long getDelay(TimeUnit unit)
    {
        long result = 0;

        long remainingTime = this.expirationTime - System.nanoTime();
        if (unit == TimeUnit.NANOSECONDS)
        {
            result = remainingTime;
        }
        else if (unit == TimeUnit.MILLISECONDS)
        {
            result =  TimeUnit.MICROSECONDS.convert(remainingTime, TimeUnit.NANOSECONDS);
        }
        else if (unit == TimeUnit.SECONDS)
        {
            result =  TimeUnit.SECONDS.convert(remainingTime, TimeUnit.NANOSECONDS);
        }
        else
        {
            Log.log.warn("TimeUnit not supported");
        }
        return result;
    } // getDelay

    public int compareTo(Delayed o)
    {
        int result = 0;

        long systemTime = System.nanoTime();
        long remainingTime = this.expirationTime - systemTime;
        long oRemainingTime = ((DelayedMessage)o).getExpirationTime() - systemTime;

        if (remainingTime == oRemainingTime)
        {
            result = 0;
        }
        else if (remainingTime > oRemainingTime)
        {
            result = 1;
        }
        else
        {
            result = -1;
        }
        return result;
    } // compareTo

} // DelayedMessage
