/******************************************************************************
* (C) Copyright 2014
*
* Resolve Systems, LLC
*
* All rights reserved
* This software is distributed under license and may not be used, copied,
* modified, or distributed without the express written permission of
* Resolve Systems, LLC.
*
******************************************************************************/

package com.resolve.esb;

import java.util.HashMap;
import java.util.Map;

public class MMsgContent extends HashMap
{
    public final static String MSGCONTENT = "_MSGCONTENT";

    final static String MSGRAW = MSGCONTENT + "RAW";
    final static String MSGDEBUG = MSGCONTENT + "DEBUG";
    final static String MSGPROPERTIES = MSGCONTENT + "PROPERTIES";
    final static String MSGPARAMS = MSGCONTENT + "PARAMS";
    final static String MSGFLOWS = MSGCONTENT + "FLOWS";
    final static String MSGINPUTS = MSGCONTENT + "INPUTS";
    final static String MSGVO = MSGCONTENT + "VO";

    public MMsgContent()
    {
        super();
    } // MMsgOptions

    public MMsgContent(Map data)
    {
        this.putAll(data);
    } // MMsgOptions

    public MMsgContent(String msgOptions)
    {
        super();
        if (msgOptions != null)
        {
            String[] options = msgOptions.split("&");
            for (int i=0; i<options.length; i++)
            {
                String[] option = options[i].split("=");
                if (option.length == 2)
                {
                    String key = option[0];
                    String value = option[1];

                    if (key.startsWith(MSGCONTENT))
                    {
                        this.put(key, value);
                    }
                }
            }
        }
    } // MMsgOptions

    public String toString()
    {
        StringBuffer result = new StringBuffer();

        for (Object key : this.keySet())
        {
            result.append(key);
            result.append("=");
            result.append(this.get(key));
            result.append("&");
        }

        return result.toString();
    } // toString

    public String getFormattedString()
    {
        StringBuffer result = new StringBuffer();

        for (Object key : this.keySet())
        {
            String keyStr = key.toString();
            keyStr = keyStr.replaceFirst(MSGCONTENT, "");

            result.append(keyStr);
            result.append("=");
            result.append(this.get(key));
            result.append(" ");
        }

        return result.toString();
    } // getFormattedString

    public void setDebug(String debug)
    {
        if (debug != null)
        {
            this.put(MSGDEBUG, debug);
        }
    } // setDebug

    public String getDebug()
    {
        return (String) this.get(MSGDEBUG);
    } // getDebug

    public String getProperties()
    {
        String properties = (String) this.get(MSGPROPERTIES);

        return properties;
    } // getProperties

    public void setProperties(String properties)
    {
        if (properties != null)
        {
            this.put(MSGPROPERTIES, properties);
        }
    } // setProperties

    public String getParams()
    {
        String params = (String) this.get(MSGPARAMS);

        return params;
    } // getParams

    public void setParams(String params)
    {
        if (params != null)
        {
            this.put(MSGPARAMS, params);
        }
    } // setParams

    public String getFlows()
    {
        String flows = (String) this.get(MSGFLOWS);

        return flows;
    } // getFlows

    public void setFlows(String flows)
    {
        if (flows != null)
        {
            this.put(MSGFLOWS, flows);
        }
    } // setFlows

    public String getInputs()
    {
        String inputs = (String) this.get(MSGINPUTS);

        return inputs;
    } // getInputs

    public void setInputs(String inputs)
    {
        if (inputs != null)
        {
            this.put(MSGINPUTS, inputs);
        }
    } // setInputs

    public String getVO()
    {
        String vo = (String) this.get(MSGVO);

        return vo;
    } // getVO

    public void setVO(String vo)
    {
        if (vo != null)
        {
            this.put(MSGVO, vo);
        }
    } // setVO

    public Object getRaw()
    {
        Object raw = this.get(MSGRAW);
        if (raw == null)
        {
            raw = "";
        }

        return raw;
    } // getRaw

    public void setRaw(Object raw)
    {
        if (raw != null)
        {
            this.put(MSGRAW, raw);
        }
        else
        {
        	this.put(MSGRAW, "");
        }
    } // setRaw

} // MMsgOptions
