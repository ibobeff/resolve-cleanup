/******************************************************************************
* (C) Copyright 2014
*
* Resolve Systems, LLC
*
* All rights reserved
* This software is distributed under license and may not be used, copied,
* modified, or distributed without the express written permission of
* Resolve Systems, LLC.
*
******************************************************************************/

package com.resolve.esb.amqp;

import java.io.IOException;
import java.net.InetAddress;
import java.util.HashMap;

import com.rabbitmq.client.AMQP;
import com.rabbitmq.client.Channel;
import com.rabbitmq.client.Consumer;
import com.rabbitmq.client.Envelope;
import com.rabbitmq.client.ShutdownSignalException;
import com.resolve.esb.DefaultHandler;
import com.resolve.esb.DefaultMessageProcessor;
import com.resolve.esb.ESBException;
import com.resolve.esb.MListener;
import com.resolve.esb.MMsgContent;
import com.resolve.esb.MMsgHeader;
import com.resolve.esb.MServer;
import com.resolve.util.Guid;
import com.resolve.util.Log;
import com.resolve.util.ObjectProperties;
import com.resolve.util.StringUtils;

/**
 * RabbitMQ Queue/Topic listener. This has mechanism to reestablish broken channel connection.
 *
 */
public class MAmqpListener extends MListener implements Consumer
{
    private final MAmqpServer mAmqpServer;
    protected Channel receiveChannel;
    
    private boolean isOpen = false;

    public MAmqpListener(final MServer mServer, final String queueName, final String defaultClassPackage) throws ESBException
    {
        this(mServer, queueName, queueName, defaultClassPackage);
    } // MAmqpListener

    public MAmqpListener(final MServer mServer, final String queueName, final String defaultClassPackage, final DefaultHandler handler) throws ESBException
    {
        this(mServer, queueName, queueName, defaultClassPackage, handler);
    } // MAmqpListener

    public MAmqpListener(final MServer mServer, final String name, final String queueName, final String defaultClassPackage) throws ESBException
    {
        this(mServer, name, queueName, defaultClassPackage, null);
    } // MAmqpListener

    public MAmqpListener(final MServer mServer, final String name, final String queueName, final String defaultClassPackage, final DefaultHandler handler) throws ESBException
    {
        this(mServer, name, queueName, defaultClassPackage, handler, null);
    } // MAmqpListener

    public MAmqpListener(final MServer mServer, final String name, final String queueName, final String defaultClassPackage, final DefaultHandler handler, final DefaultMessageProcessor messageProcessor) throws ESBException
    {
        super(mServer, name, queueName, defaultClassPackage, handler, messageProcessor);
        mAmqpServer = (MAmqpServer) mServer;
        receiveChannel = mAmqpServer.createChannel();
        isOpen = true;
    } // MAmqpListener

    public Channel getReceiveChannel() throws ESBException
    {
        if(receiveChannel != null && !receiveChannel.isOpen())
        {
            receiveChannel = mAmqpServer.createChannel();
            isOpen = true;
        }
        return receiveChannel;
    }

    @Override
    public void init(final boolean flushQueue) throws ESBException
    {
        if (receiveChannel == null || !receiveChannel.isOpen())
        {
            receiveChannel = mAmqpServer.createChannel();
            isOpen = true;
        }
        super.init(flushQueue);
    } // init

    public void initChannelOnly() throws ESBException
    {
        if (receiveChannel == null || !receiveChannel.isOpen())
        {
            receiveChannel = mAmqpServer.createChannel();
            isOpen = true;
            Log.log.info("MAmqpListener inited channel for listener " + getListenerId());
        }
    } 
    
    @Override
    public void close()
    {
        super.close();
        try
        {
            Log.log.info("Closing listener: " + getListenerId());
            isOpen = false;
            if (receiveChannel != null && receiveChannel.isOpen())
            {
                receiveChannel.close();
            }
        }
        catch (Exception e)
        {
            Log.log.warn(e.getMessage(), e);
        }
    } // close
    public void deleteQueue(String queueName)
    {
    	try {
    		getReceiveChannel().queueDelete(queueName);
    		mAmqpServer.getDriver().queues.remove(queueName);
		} catch (IOException | ESBException e) {
				Log.log.error(e.getMessage(),e);
		}
    }
    /**
     * This method gets activated by AMQP server when a message arrives.
     *
     */
    @SuppressWarnings({ "unchecked", "rawtypes" })
    public void handleDelivery(final String consumerTag, final Envelope envelope, final AMQP.BasicProperties properties, final byte[] body) throws IOException
    {
        String oldThreadName = "PoolExecutor";
        String messageId = "";
        Log.setCurrentContext(getContext());
        try
        {
            if (body != null)
            {
                messageId = "" + envelope.getDeliveryTag();
                if (mServer.isActive())
                {
                    if (receiveChannel.isOpen())
                    {
                        oldThreadName = Thread.currentThread().getName();
                        Thread.currentThread().setName(envelope.getRoutingKey());
                        Object message = ObjectProperties.deserialize(body);
                        if (message != null)
                        {
                            MMsgHeader msgHeader = setMMsgHeader((HashMap) message);
                            if (Log.log.isTraceEnabled())
                            {
                                Log.log.trace("Received delivery tag: " + envelope.getDeliveryTag());
                                Log.log.trace("Message received: " + message);
                                Log.log.trace("Message header: " + msgHeader);
                            }
                            
                            String className = msgHeader.getDest().getClassname();
                            if("DEFAULT".equalsIgnoreCase(className))
                            {
                                if(StringUtils.isNotBlank(getDefaultHandler().getDefaultClassName()))
                                {
                                    msgHeader.getDest().setClassname(getDefaultHandler().getDefaultClassName());
                                }
                            }
                            // Call the method to process this message.
                            defaultProcessor.processMessage(msgHeader, messageId, message);
                        }
                    }
                    else
                    {
                        Log.log.info("Received message with delivery tag " + envelope.getDeliveryTag() + " not processed and not acknoledged since listener channel is closed");
                    }
                }
                else
                {
                    Log.log.warn("Message with delivery tag " + messageId + " discarded. MServer is inactive and Listener receive channel open is " + receiveChannel.isOpen());
                }
            }
        }
        catch (Throwable e)
        {
            Log.log.warn("Failed to execute message handler: " + e.getMessage(), e);
        }
        finally
        {
            // make sure msgs are acknowledged
            try
            {
                if (mServer.isActive() && receiveChannel.isOpen())
                {
                    receiveChannel.basicAck(envelope.getDeliveryTag(), false);
                    // restore thread name
                    Thread.currentThread().setName(oldThreadName);
                }
                else
                {
                    Log.log.warn("Message with delivery tag " + messageId + " not acked becuase MServer is inactive and Listener receive channel open is " + receiveChannel.isOpen());
                }
            }
            catch (Throwable e)
            {
                Log.log.warn("Failed to acknowledge received message with delivery tag " + 
                             envelope.getDeliveryTag() + " due to " + e.getMessage(), e);
            }
        }
    }

    protected MMsgHeader setMMsgHeader(final HashMap<String, Object> msg) throws Exception
    {
        MMsgHeader mMsgHeader = new MMsgHeader();

        if (msg != null)
        {
            for (String name : msg.keySet())
            {
                if (MMsgHeader.MSGTYPE.equals(name))
                {
                    mMsgHeader.setType((String) msg.get(MMsgHeader.MSGTYPE));
                }
                else if (MMsgHeader.MSGSOURCE.equals(name))
                {
                    mMsgHeader.setSource((String) msg.get(MMsgHeader.MSGSOURCE));
                }
                else if (MMsgHeader.MSGROUTE.equals(name))
                {
                    mMsgHeader.setRoute((String) msg.get(MMsgHeader.MSGROUTE));
                }
                else
                {
                    Object value = msg.get(name);
                    if (value != null)
                    {
                        mMsgHeader.put(name, value);
                    }
                }
            }
        }
        mMsgHeader.set_caller(null);
        return mMsgHeader;
    }

    /**
     * We're guranteeing that the message is actually will alway is a HashMap.
     * Other checks are for future.
     *
     * @param message
     * @return
     */
    @SuppressWarnings("rawtypes")
    @Override
    public Object getParams(final Object message)
    {
        Object result = null;

        if (message instanceof String)
        {
            result = (String) message;
        }
        else if (message instanceof HashMap)
        {
            result = ((HashMap) message).get(MMsgContent.MSGCONTENT);
        }
        else
        {
            result = message;
        }
        return result;
    } // getParams

    public void handleCancel(String consumerTag) throws IOException
    {
        Log.log.trace("Handling CANCEL AMQP:" + consumerTag);
    }

    public void handleCancelOk(String consumerTag)
    {
        Log.log.trace("Handling CANCEL OK AMQP:" + consumerTag);
    }

    public void handleConsumeOk(String consumerTag)
    {
        Log.log.trace("Handling CONSUME OK AMQP:" + consumerTag);
    }

    public void handleRecoverOk(String consumerTag)
    {
        Log.log.trace("Handling RECOVERY OK AMQP:" + consumerTag);
    }

    /*This method is invoked by rabitmq if shutdown listener is registered with the channel.
     * This method is called when the AMQP listener is shutdown or the channel is closed accidently when the channel is still open.
     * The isOpen flag is left open when this condition occurs.
     * By calling this method, the channel will be automatically recovered.
     * @see com.rabbitmq.client.Consumer#handleShutdownSignal(java.lang.String, com.rabbitmq.client.ShutdownSignalException)
     */
    public void handleShutdownSignal(String consumerTag, ShutdownSignalException exception)
    {
        Log.log.debug("AMQP service unreachable for consumer: " + consumerTag + ", the reason provided is " + exception.getReason());
        while (isOpen)
        {
            try
            {
                Log.log.debug("Listener: " + getListenerId() + " is reconnecting to " + getReceiveQueueName());
                if (mAmqpServer.getConnection() != null && mAmqpServer.getConnection().isOpen())
                {
                    //give it a new listener id, since the listener id is used as "consumerTag", RabbitMQ doesn't
                    //like to reuse a tag that was failed at sometime.
                	InetAddress host = getReceiveChannel().getConnection().getAddress();
                    int port = getReceiveChannel().getConnection().getPort();
                	mAmqpServer.deleteTopic(getListenerId());
                	setListenerId(getReceiveQueueName() + "#" + new Guid().toString());
                    init(false);
                    
                    Log.log.debug("Connection established for listener: " + getListenerId() + " to " + getReceiveQueueName());
                    Log.log.debug("Connected to: " + host.getHostAddress() + ":" + port);
                    break;
                }
            }
            catch (ESBException e)
            {
                Log.log.warn(e.getMessage());
                Log.log.trace(e.getMessage(),e);
            }

            try
            {
                Thread.sleep(5000L);
            }
            catch (InterruptedException e1) { }
        }
    }
    
    /* This method is used when the listener is shutdown purposely when thread pool is maxed out.
     * The isOpen flag is set to be false when the lister channel is closed.
     * When this condition is met, no auto-recovery function should be invoked. 
     */
    public void reopenQueueConsumer()
    {     
        try {
            // If the channel is not created successfully, the following process will be retried until the channel is open.
            // No further action is taken like the handleShutdownSignal.
            while(!isOpen) {
                try
                {
                    Log.log.info("Listener: " + getListenerId() + " is reconnecting to " + getReceiveQueueName());
                    if (mAmqpServer.getConnection() != null && mAmqpServer.getConnection().isOpen())
                    {
                        //give it a new listener id, since the listener id is used as "consumerTag", RabbitMQ doesn't
                        //like to reuse a tag that was failed at sometime.
                        
                        setOldListenerId(getListenerId());
                        setListenerId(getReceiveQueueName() + "#" + new Guid().toString());
                        Log.log.debug("Listener Id: Old " + getOldListenerId() + ", New " + getListenerId());
                        
                        initChannelOnly(/*false*/);
        
                        InetAddress host = getReceiveChannel().getConnection().getAddress();
                        int port = getReceiveChannel().getConnection().getPort();
        
                        Log.log.info("Reopened queue consumer with listener: Old Id " + getOldListenerId() + ", New Id " + getListenerId() + " to " + getReceiveQueueName());
                        Log.log.debug("Connected to: " + host.getHostAddress() + ":" + port);
                        break;
                    }
                }
                catch (ESBException e)
                {
                    Log.log.warn(e.getMessage());
                    Log.log.trace(e.getMessage(),e);
                }
                catch (Throwable e)
                {
                    Log.log.warn(e.getMessage());
                    Log.log.trace(e.getMessage(),e);
                }
        
                try
                {
                    Thread.sleep(5000L);
                }
                catch (InterruptedException e1) { }
            } // end of while()
        } catch(Exception e) {
            Log.log.warn(e.getMessage());
            Log.log.trace(e.getMessage(),e);
        }
    }

} // MAmqpListener