/******************************************************************************
* (C) Copyright 2014
*
* Resolve Systems, LLC
*
* All rights reserved
* This software is distributed under license and may not be used, copied,
* modified, or distributed without the express written permission of
* Resolve Systems, LLC.
*
******************************************************************************/

package com.resolve.esb;

import com.resolve.util.StringUtils;

public class MMsgDest
{
    String address;
    String classname;
    String methodname;

    public MMsgDest()
    {
        this.address = null;
        this.classname = null;
        this.methodname = null;
    } // MMsgDest

    public MMsgDest(String address, String classMethod)
    {
        // set id/name
        setAddress(address);

        // set class.method
        setClassMethod(classMethod);
    } // MMsgDest

    public MMsgDest(String address, String className, String methodName)
    {
        // set id/name
        setAddress(address);

        setClassname(className);
        
        // set method name
        setMethodname(methodName);
    } // MMsgDest

    public MMsgDest(MMsgDest msgDest)
    {
        this.address = msgDest.getAddress();
        this.classname = msgDest.getClassname();
        this.methodname = msgDest.getMethodname();
    } // MMsgDest

    public MMsgDest(MMsgDest msgDest, String classMethod)
    {
        this.address = msgDest.getAddress();
        if (classMethod == null)
        {
            this.classname = msgDest.getClassname();
            this.methodname = msgDest.getMethodname();
        }
        else
        {
            setClassMethod(classMethod);
        }
    } // MMsgDest

    public MMsgDest(String dest) throws Exception
    {
        String[] addrClassMeth = dest.split("#");
        if (addrClassMeth.length == 2)
        {
            // set id/name
            setAddress(addrClassMeth[0]);

            // set class.method
            setClassMethod(addrClassMeth[1]);
        }
    } // MMsgDest

    public String getClassMethod()
    {
        return classname + "." + methodname;
    } // getClassMethod

    public void setClassMethod(String def)
    {
        String[] classMethod = def.split("\\.");
        if (classMethod.length > 1)
        {
            //classname = classMethod[0];
            //methodname = StringUtils.join(classMethod, ".", 1, classMethod.length - 1);
            classname = StringUtils.join(classMethod, ".", 0, classMethod.length - 2);
            methodname = classMethod[classMethod.length - 1];
        }
    } // setClassMethod

    public String toString()
    {
        return address + "#" + classname + "." + methodname;
    } // toString

    public String getFormattedString()
    {
        return "address: " + address + " method: " + classname + "." + methodname;
    } // getFormattedString

    public boolean equals(String dest) throws Exception
    {
        MMsgDest compareMsgDest = new MMsgDest(dest);
        return this.equals(compareMsgDest);
    } // equals

    public boolean isDefined()
    {
        boolean result = false;

        if (address != null && classname != null && methodname != null)
        {
            result = true;
        }
        return result;
    } // isDefined

    public String getClassname()
    {
        return classname;
    }

    public void setClassname(String classname)
    {
        this.classname = classname;
    }

    public String getAddress()
    {
        return address.toUpperCase();
    }

    public void setAddress(String address)
    {
        this.address = address.toUpperCase();
    }

    public String getMethodname()
    {
        return methodname;
    }

    public void setMethodname(String methodname)
    {
        this.methodname = methodname;
    }

    @Override
    public int hashCode()
    {
        final int prime = 31;
        int result = 1;
        result = prime * result + ((address == null) ? 0 : address.hashCode());
        result = prime * result + ((classname == null) ? 0 : classname.hashCode());
        result = prime * result + ((methodname == null) ? 0 : methodname.hashCode());
        return result;
    }

    @Override
    public boolean equals(Object obj)
    {
        if (this == obj) return true;
        if (obj == null) return false;
        if (getClass() != obj.getClass()) return false;
        MMsgDest other = (MMsgDest) obj;
        if (address == null)
        {
            if (other.address != null) return false;
        }
        else if (!address.equals(other.address)) return false;
        if (classname == null)
        {
            if (other.classname != null) return false;
        }
        else if (!classname.equals(other.classname)) return false;
        if (methodname == null)
        {
            if (other.methodname != null) return false;
        }
        else if (!methodname.equals(other.methodname)) return false;
        return true;
    }

} // MMsgDest
