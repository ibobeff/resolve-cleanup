/******************************************************************************
* (C) Copyright 2014
*
* Resolve Systems, LLC
*
* All rights reserved
* This software is distributed under license and may not be used, copied,
* modified, or distributed without the express written permission of
* Resolve Systems, LLC.
*
******************************************************************************/

package com.resolve.esb;

import com.resolve.util.Log;

public class EchoHandler extends MHandler
{
    public EchoHandler()
    {
        super();
        
        type = "ECHO";
    } // EchoHandler

	protected void runInternal() 
    {
		try 
        {
            System.out.println();
            System.out.println(MUtils.dumpMessage(msg));
		} 
        catch (Exception e) 
        {
            Log.log.warn("Failed to execute echo handler: "+e.getMessage(), e);
		} 
	} // run

} // EchoHandler
