/******************************************************************************
* (C) Copyright 2014
*
* Resolve Systems, LLC
*
* All rights reserved
* This software is distributed under license and may not be used, copied,
* modified, or distributed without the express written permission of
* Resolve Systems, LLC.
*
******************************************************************************/

package com.resolve.esb;

public interface MDriverInterface
{
    //public void initReceiveQueue(MListener listener, boolean flushQueue) throws Exception;
    public boolean registerQueue(String queueName);
} // MDriverInterface
