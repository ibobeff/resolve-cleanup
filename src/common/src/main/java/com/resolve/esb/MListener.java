/******************************************************************************
* (C) Copyright 2014
*
* Resolve Systems, LLC
*
* All rights reserved
* This software is distributed under license and may not be used, copied,
* modified, or distributed without the express written permission of
* Resolve Systems, LLC.
*
******************************************************************************/

package com.resolve.esb;

import org.apache.commons.collections.MapUtils;

import com.resolve.esb.amqp.MAmqpListener;
import com.resolve.thread.ScheduledExecutor;
import com.resolve.util.ERR;
import com.resolve.util.Guid;
import com.resolve.util.Log;
import com.resolve.util.StringUtils;

public abstract class MListener
{
    //this is the Queue
    protected String receiveQueueName;
    protected MServer mServer;
    protected String defaultClassPackage;
    protected DefaultHandler defaultHandler;
    protected DefaultMessageProcessor defaultProcessor;

    private String listenerId;
    private String context = "";
    public String getContext() {
        return context;
    }
    // keep track of old lister ids when listener is reopened with new id
    private String oldListenerId;

    public abstract Object getParams(Object message) throws ESBException;

    protected MListener(MServer mServer, String name, String queueName, String defaultClassPackage, DefaultHandler handler) throws ESBException
    {
        this(mServer, name, queueName, defaultClassPackage, handler, null);
    }

    protected MListener(MServer mServer, String name, String queueName, String defaultClassPackage, DefaultHandler handler, DefaultMessageProcessor processor) throws ESBException
    {
        context = Log.getCurrentContext();
        // init references
        this.listenerId = queueName + "#" + new Guid().toString();
        this.mServer = mServer;
        this.receiveQueueName = queueName;
        if (StringUtils.isBlank(defaultClassPackage))
        {
            this.defaultClassPackage = mServer.getDefaultClassPackage();
        }
        else
        {
            this.defaultClassPackage = defaultClassPackage;
        }

        // add listener
        if (StringUtils.isBlank(name))
        {
            name = "DEFAULT";
        }

        // init defaultHandler
        if (handler == null)
        {
            this.defaultHandler = new DefaultHandler(mServer.getDefaultMSender(), mServer.getServiceName());
        }
        else
        {
            this.defaultHandler = handler;
        }

        //init defaultMessageProcessor
        if (processor == null)
        {
            this.defaultProcessor = new DefaultMessageProcessor();
        }
        else
        {
            this.defaultProcessor = processor;
        }
        //very important initialization
        this.defaultProcessor.setMListener(this);
        this.defaultProcessor.setMServer(mServer);

    } // MListener

    public MServer getMServer()
    {
        return mServer;
    }

    public void setMServer(MServer mServer)
    {
        this.mServer = mServer;
    }

    public String getListenerId()
    {
        return listenerId;
    }

    public void setListenerId(String listenerId)
    {
        this.listenerId = listenerId;
    }

    public void init(boolean flushQueue) throws ESBException
    {
        String address = getReceiveQueueName();
        if (mServer.getPublications().containsKey(address) || 
            mServer.getDurablePublications().containsKey(address) || 
            address.endsWith("_TOPIC"))
        {
            mServer.subscribePublication(receiveQueueName, this);
        }
        else
        {
            mServer.createQueueConsumer(receiveQueueName, this);
        }
    } // init

    public void close()
    {
    	if (MapUtils.isNotEmpty(mServer.listeners) && mServer.listeners.containsKey(listenerId)) {
    		mServer.listeners.remove(listenerId);
    	}
    } // close

    public String getReceiveQueueName()
    {
        return receiveQueueName;
    } // getReceiveQueueName

    public void setReceiveQueueName(String receiveQueueName)
    {
        this.receiveQueueName = receiveQueueName;
    }

    public String getDefaultClassPackage()
    {
        return defaultClassPackage;
    }

    public void setDefaultClassPackage(String defaultClassPackage)
    {
        this.defaultClassPackage = defaultClassPackage;
    }

    public DefaultHandler getDefaultHandler()
    {
        return defaultHandler;
    }

    public void setDefaultHandler(DefaultHandler defaultHandler)
    {
        this.defaultHandler = defaultHandler;
    }

    public DefaultMessageProcessor getDefaultProcessor()
    {
        return defaultProcessor;
    }

    public void setDefaultProcessor(DefaultMessageProcessor defaultProcessor)
    {
        this.defaultProcessor = defaultProcessor;
    }

    public static class MListenerControl implements Runnable
    {
        public static String CREATE_CONSUMER = "CREATE_CONSUMER";
        public static String CLOSE_CONSUMER = "CLOSE_CONSUMER";

        MServer server;
        MListener listener;
        String action;

        public MListenerControl(MServer server, MListener listener, String action)
        {
            this.server = server;
            this.listener = listener;
            this.action = action;
        } // MListenerControl

        public void run()
        {
            try
            {
                if (action.equals(CREATE_CONSUMER))
                {
                    this.server.createQueueConsumer(listener.getReceiveQueueName(), listener);
                }
                else if (action.equals(CLOSE_CONSUMER))
                {
                    // close consumer
                    Log.log.warn("MListenerControl : ThreadPool - closing consumer with listener " + listener.getListenerId() + " on queue " + listener.getReceiveQueueName() + " with hash " + listener.hashCode());
                    this.server.closeQueueConsumer(listener.getListenerId());

                    int count = 0;
                    long startWait = System.currentTimeMillis();
                    while (ScheduledExecutor.getInstance().getQueueSize() > ScheduledExecutor.getInstance().getCorePoolSize())
                    {
                        // display msg every 2 mins - 5 = secs * 120
                        if (count >= 600/*5*/)
                        {
                            //if (Log.log.isTraceEnabled())
                            //{
                                Log.log.info("MListenerControl : Active Threads: " + ScheduledExecutor.getInstance() + " Queue: " + ScheduledExecutor.getInstance().getQueueSize() + " PoolSize: " + ScheduledExecutor.getInstance().getPoolSize() + " CorePoolSize: " + ScheduledExecutor.getInstance().getCorePoolSize());
                            //}
                            Log.log.warn("MListenerControl : ThreadPool MAX - waiting");
                            count = 0;
                        }

                        count++;
                        Thread.sleep(200);

                        // fatal if waited for more than 20mins
                        if (System.currentTimeMillis() - startWait > 1200000/*10000*/)
                        {
                            Log.log.error("MListenerControl : Error Code [" + ERR.E70012.getCode() + "] Error Message [" + ERR.E70012.getMessage() + "] still waiting...");
                            Log.alert(ERR.E70012); //RBA-2978 - Converted Instances of Log.log.fatal to Log.alert
                            
                            // reset start time
                            startWait = System.currentTimeMillis();
                        }
                    }

                    // restore consumer
                    Log.log.info("MListenerControl : ThreadPool - reopening queue consumer with listener " + listener.getListenerId() + " on queue " + listener.getReceiveQueueName() + " with hash " + listener.hashCode());
                    
                    if(listener instanceof MAmqpListener)
                        ((MAmqpListener)listener).reopenQueueConsumer();
                    
                    else
                        this.server.createQueueConsumer(listener.getReceiveQueueName(), listener);

                    //control = new MListenerControl(this, MListenerControl.CREATE_CONSUMER);
                    //new Thread(control).start();
                }
            }
            catch (Throwable e)
            {
                Log.log.error(e.getMessage(), e);
            }
        } // run

    } // MListenerControl

    public void addServiceThread(String serviceQueue, MServiceThread serviceThread)
    {
        if (defaultProcessor != null && serviceQueue != null)
        {
            MServiceThread oldServiceThread = defaultProcessor.getServiceThreads().get(serviceQueue);
            if (oldServiceThread != null)
            {
                oldServiceThread.stop();
            }
            defaultProcessor.getServiceThreads().put(serviceQueue, serviceThread);
        }
    } // addServiceThread
    
    public String getOldListenerId()
    {
        return oldListenerId;
    }

    public void setOldListenerId(String oldListenerId)
    {
        this.oldListenerId = oldListenerId;
    }
} // MListener

