/******************************************************************************
* (C) Copyright 2014
*
* Resolve Systems, LLC
*
* All rights reserved
* This software is distributed under license and may not be used, copied,
* modified, or distributed without the express written permission of
* Resolve Systems, LLC.
*
******************************************************************************/

package com.resolve.esb.amqp;

import java.io.IOException;
import java.net.ConnectException;
import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.TimeoutException;

import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.collections.MapUtils;

import com.rabbitmq.client.AMQP.Queue.DeclareOk;
import com.rabbitmq.client.Channel;
import com.rabbitmq.client.Connection;
import com.rabbitmq.client.Consumer;
import com.rabbitmq.client.DefaultConsumer;
import com.rabbitmq.client.ShutdownSignalException;
import com.resolve.esb.DefaultHandler;
import com.resolve.esb.DefaultMessageProcessor;
import com.resolve.esb.ESBException;
import com.resolve.esb.MListener;
import com.resolve.esb.MSender;
import com.resolve.esb.MServer;
import com.resolve.esb.MServerConfig;
import com.resolve.util.Constants;
import com.resolve.util.Log;
import com.resolve.util.StringUtils;

/**
 * This class manages entire interaction with the RabbitMQ server. This has mechanism to re-establish
 * connection to the server, intializing Topics.
 *
 */
public class MAmqpServer extends MServer
{
    private static volatile MAmqpServer instance = null;

    private Connection connection;
    private Channel channel;
    private MAmqpDriver driver;
    private Channel durableChannel;

    private DefaultConsumer channelConsumer;
    private DefaultConsumer durableChannelConsumer;
    private Map<String, Channel> messageIdToChannel = new HashMap<String, Channel>();

	private int maxexecutionevents;
	
    protected MAmqpServer(final String serviceName, final MServerConfig config, final Map<String, String> publications, final Map<String, String> subscriptions, final String defaultClassPackage) throws ESBException {
    	this(serviceName, config, publications, subscriptions, defaultClassPackage, false);
    }
    
    protected MAmqpServer(final String serviceName, final MServerConfig config, final Map<String, String> publications, final Map<String, String> subscriptions, final String defaultClassPackage, final boolean createSubscribers) throws ESBException    {
        super(serviceName, config, publications, subscriptions, defaultClassPackage);

        // create connection
        boolean created = false;
        while (!created)
        {
            try
            {
            	maxexecutionevents = config.getMaxExecutionEvents();
                connection = driver.getConnection();
                channel = connection.createChannel();
                durableChannel = connection.createChannel();
                if(createSubscribers) {
                	setUpChannel();
                	setUpDurableChannel();
                }
                created = true;
            }
            catch (Exception e)
            {
                Log.log.warn("Retry createConnection. " + e.getMessage());
                try
                {
                    Thread.sleep(5000L);
                }
                catch (InterruptedException e1)
                {
                    // ignore
                }
            }
        }
    } // MAmqpServer

    private void setUpDurableChannel() throws IOException, ESBException {
    	durableChannel.basicQos(maxexecutionevents);
    	durableChannelConsumer = new MAmqpRunbookExecutionListener(durableChannel, true, this);
        createDurableQueue(Constants.ESB_NAME_DURABLE_EXECUTEQUEUE);
    	durableChannel.basicConsume(Constants.ESB_NAME_DURABLE_EXECUTEQUEUE, durableChannelConsumer);
    }
    
    private void setUpChannel() throws IOException, ESBException {
    	channel.basicQos(maxexecutionevents);
    	channelConsumer = new MAmqpRunbookExecutionListener(channel, false, this);
        createQueue(Constants.ESB_NAME_EXECUTEQUEUE);
    	channel.basicConsume(Constants.ESB_NAME_EXECUTEQUEUE, channelConsumer);
    }
    
    public static MAmqpServer getInstance(final String serviceName, final MServerConfig config, final Map<String, String> publications, final Map<String, String> subscriptions, final String defaultClassPackage) throws ESBException {
    	return getInstance(serviceName, config, publications, subscriptions, defaultClassPackage, false);
    }
    	
    public static MAmqpServer getInstance(final String serviceName, final MServerConfig config, final Map<String, String> publications, final Map<String, String> subscriptions, final String defaultClassPackage, boolean createSubscriptions) throws ESBException
    {
        if (instance == null)
        {
        	instance = new MAmqpServer(serviceName, config, publications, subscriptions, defaultClassPackage, createSubscriptions);
        }
        return instance;
    }

    /*
     * RabbitMQ Initialization
     */
    @Override
    public void init() throws ESBException
    {
        // initialize AMQP Driver
        Log.log.info("Initializing AMQP service");
        driver = new MAmqpDriver(config);
    } // init

    @Override
    public void start() throws ESBException
    {
        if (connection.isOpen())
        {
        	// Following is NOOP if server is already started, MServerFactory starts server before handing it out
            super.start();
            
            /*
             * Avoid recreating connection checker thread when start is called on creation of server by MServerFactory
             * and then again by the component using that mServer via ESB. 
             */
            
            if (!isStarted()) {
            	//initialize the connection checker thread.
            	ConnectionChecker connectionChecker = new ConnectionChecker(5);
            	connectionChecker.start();
            }
        }
        else
        {
            Log.log.error("Could not open connection to AMQP server");
            throw new ESBException("Could not open connection to AMQP server");
        }
    } // start

    @Override
    public void start(boolean isActive) throws ESBException
    {
        super.start(isActive);
        if (!connection.isOpen())
        {
            Log.log.error("Could not open connection to AMQP server");
            throw new ESBException("Could not open connection to AMQP server");
        }
    } // start

    @Override
    public void stop() throws ESBException
    {
        super.stop();
        try
        {
            channel.close();
            durableChannel.close();
            connection.close();
        }
        catch (IOException e)
        {
            Log.log.warn(e.getMessage(), e);
        }
        catch (/*Timeout*/Exception e)
        {
            Log.log.error(e.getMessage(), e);
        }
    } // stop

    /*
     * This method is called by the COnnectionChecker thread if the connection
     * is broken for some reason.
     *
     * @see com.resolve.esb.MServer#restart()
     */
    @Override
    public void restart() throws ESBException
    {
        try
        {
            // reconnect
            connection = driver.getConnection();
            // start connection
            if (connection == null || !connection.isOpen())
            {
                Log.log.warn("Connection to the AMQP service is down, waiting and then retrying.");
                Thread.sleep(5000L);
            }
            else
            {
                channel = getOpenChannel();
                durableChannel = getOpenDurableChannel();

                /*
                 * Re-creation of all topics/exchanges has to catch timeout exception and retry topic creations from 
                 * where it left off
                 * 
                 * NOTE: No parallel stream processing as Channel is not thread safe
                 */
                
                Map<String, Throwable> topicCreationExceptions = new HashMap<String, Throwable> ();
                
                if (CollectionUtils.isNotEmpty(publications.keySet())) {
	                publications.keySet().stream().forEach(topic -> {
	                	boolean topicCreated = false;
	                	int retryCount = 5;
	                	
	                	while (!topicCreated && retryCount-- >= 0) {
		                	try {
		                		topicCreated = createTopic(topic);
		                	} catch (Throwable t) {
		                		if (t instanceof ESBException && t.getCause() != null && 
		                			t.getCause() instanceof TimeoutException) {
		                			if (Log.log.isDebugEnabled()) {
		                				Log.log.debug(String.format("Creation of %s non durable topic/exchange timedout, " +
		                											"sleeping for 5 secs before retrying...", topic));
		                			}
		                			
		                			try {
										Thread.sleep(5000L);
									} catch (InterruptedException e) {
										// Safe to ignore
									}
		                		} else {
		                			Log.log.error(String.format("Error %s while creating non durable topic/exchange %s", 
		                										(StringUtils.isNotBlank(t.getMessage()) ? 
		                										 "[" + t.getMessage() + "] " : ""), topic));
		                		}
		                		
		                		if (retryCount == 0) {
			                		topicCreationExceptions.put(topic, t);
			                	}
		                	}
	                	}
	                });
                }
                
                // Create durable topics/exchanges
                
                if (CollectionUtils.isNotEmpty(durablePublications.keySet())) {
	                durablePublications.keySet().stream().forEach(dtopic -> {
	                	boolean dTopicCreated = false;
	                	int retryCount = 5;
	                	
	                	while (!dTopicCreated && retryCount-- >= 0) {
		                	try {
		                		dTopicCreated = createDurableTopic(dtopic);
		                	} catch (Throwable t) {
		                		if (t instanceof ESBException && t.getCause() != null && 
		                			t.getCause() instanceof TimeoutException) {
		                			if (Log.log.isDebugEnabled()) {
		                				Log.log.debug(String.format("Creation of %s durable topic/exchange timedout, " +
		                											"sleeping for 5 secs before retrying...", dtopic));
		                			}
		                			
		                			try {
										Thread.sleep(5000L);
									} catch (InterruptedException e) {
										// Safe to ignore
									}
		                		} else {
		                			Log.log.error(String.format("Error %s while creating durable topic/exchange %s", 
		                										(StringUtils.isNotBlank(t.getMessage()) ? 
		                										 "[" + t.getMessage() + "] " : ""), dtopic));
		                		}
		                		
		                		if (retryCount == 0) {
			                		topicCreationExceptions.put(dtopic, t);
			                	}
		                	}
	                	}
	                });
                }
                
                //it's every individual publisher's duty to get the broken channel itself.
                //you may check MAmqpSender.sendMessage() method in MAmqpSender class for an idea.
                publishers.clear();

                //it's every individual listener's duty to reconnect itself.
                //at this point we'll simply remove the listeners.
                //check the MAmqpListener.handleShutdownSignal() method.
                listeners.clear();
                
                if (MapUtils.isNotEmpty(topicCreationExceptions)) {
                	throw new Exception (String.format("Failed to create %d [%s] durable/non durable topic/exchanges", 
                									   topicCreationExceptions.size(), 
                									   StringUtils.setToString(topicCreationExceptions.keySet(), ", ")));
                }
            }
        } catch (Exception e) {
            Log.log.error(e.getMessage(), e);
            throw new ESBException(e.getMessage(), e);
        }
    } // restart

    @Override
    public void close()
    {
        try
        {
            super.close();
            if (connection != null)
            {
                connection.close();
                connection = null;
            }
            driver.close();
        }
        catch (Exception e)
        {
            Log.log.warn("Close connection. " + e.getMessage(), e);
        }
    } // close

    public MAmqpDriver getDriver()
    {
        return driver;
    }

    public void setDriver(final MAmqpDriver driver)
    {
        this.driver = driver;
    }

    public Channel createChannel() throws ESBException
    {
        try {
        	return getConnection().createChannel();
        } catch (IOException e) {
            Log.log.error(e.getMessage(), e);
            throw new ESBException(e.getMessage(), e);
        }
    }
    
    public Connection getConnection() throws ESBException
    {
        if (connection == null || !connection.isOpen())
        {
            connection = driver.getConnection();
        }
        return connection;
    }

    public void setConnection(final Connection connection)
    {
        this.connection = connection;
    }

    @Override
    public boolean subscribePublication(final String topicName, final MListener subscriber)
    {
        boolean result = false;

        String tmpTopicName = topicName.toUpperCase();

        // release old subscriber if present
        
        if (StringUtils.isNotBlank(subscriber.getOldListenerId()))
        {
            unsubscribePublication(subscriber.getOldListenerId());
        }
        
        // release current subscriber if present
        unsubscribePublication(subscriber.getListenerId());
        
        // subscribe
        Log.log.info("Registering subscriber " + subscriber.getListenerId() + " to the Exchange " + tmpTopicName);

        try
        {
            MAmqpListener mAmqpListener = (MAmqpListener) subscriber;
            Channel receiveChannel = mAmqpListener.getReceiveChannel();
            
            if (durablePublications.containsKey(tmpTopicName))
            {
                Log.log.info("Registering subscriber " + subscriber.getListenerId() + " to Durable Queues bounded to Durable Exchange " + tmpTopicName);
                result = getDriver().subscribeDurable(receiveChannel, tmpTopicName, subscriber.getListenerId());
            }
            else
            {
                Log.log.info("Registering subscriber " + subscriber.getListenerId() + " to Queues bounded to Exchange " + tmpTopicName);
                result = getDriver().subscribe(receiveChannel, tmpTopicName, subscriber.getListenerId());
            }

            if (result)
            {
                if (durablePublications.containsKey(tmpTopicName))
                {
                    durableQueues.add(subscriber.getListenerId());
                }
                createQueueConsumer(subscriber.getListenerId(), subscriber, true);
            }
        }
        catch (ESBException e)
        {
            Log.log.error(e.getMessage(), e);
        }
        return result;
    } // subscribePublication

    @Override
    public boolean unsubscribePublication(final String subscriberId)
    {
        boolean result = false;

        try
        {
            MAmqpListener subscriber = (MAmqpListener) listeners.get(subscriberId);
            if (subscriber != null)
            {
                subscriber.close();
                listeners.remove(subscriberId);
            }
            result = true;
        }
        catch (Exception e)
        {
            Log.log.warn("Failed to unsubscribe publication: " + e.getMessage(), e);
        }

        return result;
    } // unsubscribePublication

    @Override
    public synchronized void createQueueConsumer(final String queueName, final MListener listener)
    {
        createQueueConsumer(queueName, listener, false);
    }
    public void deleteTopic(String listenerId)
    { 
    	 MAmqpListener publicationListener = (MAmqpListener) publicationListeners.get(listenerId);
    	 if(publicationListener != null && !getDriver().durableTopics.contains(listenerId))
    	 {
    		 publicationListener.deleteQueue(listenerId);
    		 publicationListeners.remove(listenerId);
    	 }
    }
    public synchronized void createQueueConsumer(final String queueName, final MListener listener, boolean isPublication)
    {
        try
        {
            {
                MAmqpListener mAmqpListener = (MAmqpListener) listener;
                
                if (durableQueues.contains(mAmqpListener.getReceiveQueueName()))
                {
                    Log.log.info("Creating AMQP consumer " + mAmqpListener.getListenerId() + " to the Durable Queue " + mAmqpListener.getReceiveQueueName());
                    getDriver().createDurableQueue(mAmqpListener.getReceiveChannel(), queueName);
                }
                else
                {
                    Log.log.info("Creating AMQP consumer " + mAmqpListener.getListenerId() + " to the Queue " + mAmqpListener.getReceiveQueueName());
                    getDriver().createQueue(mAmqpListener.getReceiveChannel(), queueName);
                }
                
                boolean autoAck = false;
                mAmqpListener.getReceiveChannel().basicConsume(queueName, autoAck, listener.getListenerId(), (Consumer) listener);
                
                if (!isPublication)
                {
                    addListener(listener);
                }
                else 
                {
                	addPublicationListener(listener);
                }
            }
        }
        catch (Throwable e)
        {
            Log.log.error("Failed to initialize consumers: " + e.getMessage(), e);
        }
    } // createConsumers

    @Override
    public synchronized void closeQueueConsumer(final String consumerId)
    {
        try
        {
            // close queue consumer
            if (listeners.containsKey(consumerId))
            {
                MAmqpListener mAmqpListener = (MAmqpListener) listeners.get(consumerId);
                Log.log.info("Closing AMQP consumer: " + consumerId + ", queue: " + mAmqpListener.getReceiveQueueName() + " hash: " + mAmqpListener.hashCode());
                mAmqpListener.close();
                listeners.remove(consumerId);
            }
        }
        catch (Exception e)
        {
            Log.log.warn(e.getMessage(), e);
        }
    } // closeConsumers

    @Override
    public boolean createPublisher(final String topicName, final String publisherId, final MSender publisher)
    {
        boolean result = false;

        try
        {
            Log.log.debug("Creating publisher " + publisherId);
            MAmqpSender mAmqpSender = (MAmqpSender) publisher;
            mAmqpSender.setSendChannel(createChannel());
            addPublisher(publisherId, publisher);
            result = true;
        }
        catch (Exception e)
        {
            Log.log.warn(e.getMessage());
        }

        return result;
    } // createPublication

    @Override
    public boolean removePublisher(final String publisherId)
    {
        boolean result = true;

        try
        {
            if (publishers.containsKey(publisherId))
            {
                MAmqpSender mAmqpSender = (MAmqpSender) publishers.get(publisherId);
                if (mAmqpSender != null)
                {
                    mAmqpSender.close();
                    publishers.remove(publisherId);
                }
            }
        }
        catch (Exception e)
        {
            Log.log.warn(e.getMessage());
            result = false;
        }

        return result;
    } // removePublication

    @Override
    public MSender createSender() throws ESBException
    {
        return new MAmqpSender(this, config);
    }

    @Override
    public MListener createListener(final String queueName, final String defaultClassPackage) throws ESBException
    {
        return createListener(queueName, defaultClassPackage, null);
    }

    @Override
    public MListener createListener(final String queueName, final String defaultClassPackage, final DefaultHandler handler) throws ESBException
    {
        return createListener(queueName, defaultClassPackage, handler, null);
    }

    @Override
    public MListener createListener(String queueName, String defaultClassPackage, DefaultHandler handler, DefaultMessageProcessor messageProcessor) throws ESBException
    {
        MListener listener = new MAmqpListener(this, queueName, queueName, defaultClassPackage, handler, messageProcessor);
        if(queueName.contains("#"))
        {
        	addPublicationListener(listener);
        }
        else
        {
        	addListener(listener);
        }
        return listener;
    }

    @Override
    public boolean removePublication(final String topicName)
    {
        boolean result = false;

        try
        {
            if (publications.containsKey(topicName.toUpperCase()))
            {
                publications.remove(topicName.toUpperCase());
                result = true;
            }
        }
        catch (Exception e)
        {
            // why do we almost always swallow?
            Log.log.warn(e.getMessage());
        }

        return result;
    }

    @Override
    public boolean createTopic(final String topicName) throws ESBException {
        boolean result = false;
        if (StringUtils.isNotBlank(topicName))
        {
            Log.log.info(String.format("Creating topic/exchange: %s...",topicName.toUpperCase()));
            Channel chnl = getOpenChannel();            
            result = driver.createExchange(chnl, topicName.toUpperCase());
            Log.log.info(String.format("Create topic/exchange result: %s, %b", topicName.toUpperCase(), result));
        }
        return result;
    }

    @Override
    public boolean createQueue(final String queueName) throws ESBException {
        boolean result = false;
        if (StringUtils.isNotBlank(queueName))
        {
            Log.log.info("Creating queue: " + queueName.toUpperCase());
            
            Channel chnl = getOpenChannel();
            result = driver.createQueue(chnl, queueName.toUpperCase());
        }
        return result;
    }
    
    /**
     * A tiny little Thread class that checks the connectivity and in case it
     * is not open it retries.
     */
    private class ConnectionChecker extends Thread
    {
        private int waitInSeconds;
        private boolean prevCheckConnState; // State of connection, true = open, false = closed

        public ConnectionChecker(final int waitInSeconds)
        {
            this.waitInSeconds = waitInSeconds;
            prevCheckConnState = true; 		// ConnectionChecker thread starts only if connection is open
        }

        @Override
        public void run()
        {
        	Connection conn = null;
        	boolean currCheckConnState = false;
        	
        	Log.log.info(String.format("Initiated Connection Checker to check connection to Amqp service every %d secs" + 
        							   " for service %s",
					   				   waitInSeconds, getServiceName()));
        	
            while (true)
            {
                try
                {
                    Thread.sleep(this.waitInSeconds * 1000);
                    
                    try {
                    	conn = getConnection();
                    } catch (ESBException gce) {
                    	conn = null;
                    }
                    
                    if (conn != null ) {
                    	try {
                    		currCheckConnState = conn.isOpen();
                    	} catch (ShutdownSignalException sse) {
                    		// In this case the component is shutting down, stop the connection checker thread
                    		break;
                    	} catch (Throwable t) {
                    		Log.log.warn(String.format("Unexpected error %sin checking if connection is open", 
     							   					   (StringUtils.isNotBlank(t.getMessage()) ? 
     							   					   "[" + t.getMessage() + "] " : "")), t);
                    		currCheckConnState = false;
                    	}
                    } else {
                    	currCheckConnState = false;
                    }
                    
                    if (Log.log.isDebugEnabled() && prevCheckConnState != currCheckConnState) {
                    	Log.log.debug(String.format("Connection Checker identified connection state change " +
													"from %b to %b", prevCheckConnState, currCheckConnState));
                    }
                    
                    // On transition from previous no connection or closed connection to current open connection state
                    if (conn != null && !prevCheckConnState && prevCheckConnState != currCheckConnState) {
                		Log.log.info(String.format("Connection Checker identified connection state change " +
                				 	 			   "from %b to %b, re-creating durable and non-durable topics/exchanges",
                				 	 			   prevCheckConnState, currCheckConnState));
                		
                		// Create durable and non durable channels
                		channel = conn.createChannel();
                		durableChannel = conn.createChannel();
                		
                		// recreate topics/exchanges
                		restart();
                    }
                    
                    if (!currCheckConnState) {
                    	connection = null;
                    	channel = null;
                    	durableChannel = null;
                    }
                } catch (Throwable t) {
                	Log.log.warn(String.format("Unexpected error %sin Connection Checker, assuming connection loss", 
                							   (StringUtils.isNotBlank(t.getMessage()) ? "[" + t.getMessage() + "] " : "")), 
                				 t);
                	conn = null;
                	currCheckConnState = false;
                }
                
                prevCheckConnState = currCheckConnState;
            }
            
            Log.log.warn("Stopping Connection Checker as received shutdown signal exception while checking" +
            			 " if connection is still open");
        }
    }
    
    public int getMessageCount(String queueName) {
        int result = 0;
        try {
            Channel chnl = null;
            
            if (durableQueues.contains(queueName)) {
                chnl = getOpenDurableChannel();
            } else {
                chnl = getOpenChannel();
            }
    
            DeclareOk ok = chnl.queueDeclarePassive(queueName);
            //this number is never accurate, but this is the best that can be done at this point
            result = ok.getMessageCount();
            if(Log.log.isDebugEnabled() && result > 0) {
                Log.log.debug("Total message accumulated in " + queueName + " : " + result);
            }            
        } catch (IOException e) {
        	if (Log.log.isTraceEnabled()) {
        		Log.log.trace(String.format("Error %sin getting message count in queue %s", 
            						   		(StringUtils.isNotBlank(e.getMessage()) ? "[" + e.getMessage() + "] " : ""), 
            						   		queueName));
        	}
        	
            throw new RuntimeException(e);
        } catch (ESBException e) {
        	if (e.getCause() != null && e.getCause() instanceof IOException) {
        		throw new RuntimeException(e);
        	} else {
        		Log.log.error(e.getMessage(), e);
        	}
        }
        
        return result;
    }

    /*
     * HP TO DO  
     * 
     * Please note channel is shared by all queues/topics in MServer so every consumer for queue/topic
     * gets set with QOS primarily required for EXECUTE/DEXECUTE queue consumer only.
     * Should not be an issue for now.
     * 
     * RSCONTROL originally used to pull messages from EXECUTE/DEXECUTE queue causing asymmetric
     * distribution of execution request load among all RSControls in cluster.
     */
    public Channel getChannel()
    {
        Channel result = channel;
        if(channel != null && !channel.isOpen())
        {
            try
            {
                channel = createChannel();
                setUpChannel();
                result = channel;
            }
            catch (ESBException | IOException e)
            {
            	if (e.getCause() != null && e.getCause() instanceof ConnectException) {
            		Log.log.error(String.format("Error %sin getting and setting up channel with QOS", 
            									StringUtils.isNotBlank(e.getMessage()) ? "[" + e.getMessage() + "] " : ""));
            	} else {
            		Log.log.error(e.getMessage(), e);
            	}
            }
        }

        return result;
    }
    
    public Channel getOpenChannel() throws ESBException {
        if(!getConnection().isOpen()) {    
        	throw new ESBException(ESBException.ESB_CONNECTION_CLOSED_ERR_MSG, new IOException());
        }
        
        if (!channel.isOpen()) {
        	throw new ESBException(ESBException.ESB_COMPONENT_SHUTTING_DONW_ERR_MSG, new IOException());
        }

        return channel;
    }
    
    public Channel getDurableChannel()
    {
        Channel result = durableChannel;
        
        if(durableChannel != null && !durableChannel.isOpen()) {
            try {
                durableChannel = createChannel();
                setUpDurableChannel();
                result = durableChannel;
            } catch (ESBException | IOException e) {
            	if (e.getCause() != null && e.getCause() instanceof ConnectException) {
            		Log.log.error(String.format("Error %sin getting and setting up channel with QOS", 
            									StringUtils.isNotBlank(e.getMessage()) ? "[" + e.getMessage() + "] " : ""));
            	} else {
            		Log.log.error(e.getMessage(), e);
            	}
            }
        }

        return result;
    }
    
    public Channel getOpenDurableChannel() throws ESBException {
        if(!getConnection().isOpen()) {    
        	throw new ESBException("Connection Closed", new IOException());
        }
        
        if (!durableChannel.isOpen()) {
        	throw new ESBException(ESBException.ESB_COMPONENT_SHUTTING_DONW_ERR_MSG, new IOException());
        }
        
        return durableChannel;
    }
    
    public DefaultConsumer getChannelConsumer() {
    	 return channelConsumer;
    }
    	 
    public DefaultConsumer getDurableChannelConsumer() {
    	 return durableChannelConsumer;
    }
    
    public void registerUnackedMessageId(String messageId, Channel channel) {
    	messageIdToChannel.put(messageId, channel);
    }
    	
    public void unregisterUnackedMessageId(String messageId) {
    	messageIdToChannel.remove(messageId, channel);
    }
    	
    public Channel getUnackedMessageChannel(String messageId) {
    	return messageIdToChannel.get(messageId);
    }
    
    @Override
    public boolean createDurableQueue(final String queueName) throws ESBException {
        boolean result = false;
        if (StringUtils.isNotBlank(queueName)) {
            Log.log.info(String.format("Creating durable queue: ", queueName.toUpperCase()));
            
            Channel dchnl = getOpenDurableChannel();
            
            result = driver.createDurableQueue(dchnl, queueName.toUpperCase());
            
            if (result) {
                durableQueues.add(queueName.toUpperCase());
            }
        }
        return result;
    }
    
    @Override
    public boolean createDurableTopic(final String topicName) throws ESBException {
        boolean result = false;
        if (StringUtils.isNotBlank(topicName)) {
            Log.log.info(String.format("Creating durable topic/exchange: %s...", topicName.toUpperCase()));
            
            Channel dchnl = getOpenDurableChannel();
            result = driver.createDurableExchange(dchnl, topicName.toUpperCase());
            Log.log.info(String.format("Create durable topic/exchange result: %s, %b", topicName.toUpperCase(), result));
        }
        return result;
    }
} // MAmqpServer
