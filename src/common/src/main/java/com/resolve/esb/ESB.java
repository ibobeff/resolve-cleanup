/******************************************************************************
* (C) Copyright 2014
*
* Resolve Systems, LLC
*
* All rights reserved
* This software is distributed under license and may not be used, copied,
* modified, or distributed without the express written permission of
* Resolve Systems, LLC.
*
******************************************************************************/

package com.resolve.esb;

import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.locks.Condition;
import java.util.concurrent.locks.ReentrantLock;

import com.resolve.esb.MMsgDest;
import com.resolve.esb.MMsgHeader;
import com.resolve.esb.MMsgOptions;
import com.resolve.esb.MMsgRoute;
import com.resolve.esb.MServer;
import com.resolve.thread.ScheduledExecutor;
import com.resolve.util.Constants;
import com.resolve.util.LockUtils;
import com.resolve.util.Log;
import com.resolve.util.StringUtils;

public class ESB
{
    MServer mServer;
    String serviceName;

    static ConcurrentHashMap<String,Condition> callbackCondition = new ConcurrentHashMap<String,Condition>();
    static ConcurrentHashMap<String,Map> callbackResult = new ConcurrentHashMap<String,Map>();

    public ESB(MServer mServer, String serviceName)
    {
        this.mServer = mServer;
        this.serviceName = serviceName;
    } // ESB

    public MServer getMServer()
    {
        return this.mServer;
    } // getMServer

    public boolean sendMessage(MMsgHeader header, Object content)
    {
        boolean result = false;

        try
        {
            header.setCaller(serviceName);
            result = mServer.sendMessage(header, content, null);
        }
        catch (Exception e)
        {
            Log.log.warn(e.getMessage(), e);
        }
        return result;
    } // sendMessage

    public boolean sendMessage(MMsgRoute route, Object content)
    {
        return sendMessage(route, content, null);
    } // sendMessage

    public boolean sendMessage(MMsgRoute route, Object content, String callback)
    {
        boolean result = false;

        try
        {
            // init message header
            MMsgHeader header = new MMsgHeader();
            header.setRoute(route);
            header.setCaller(serviceName);

            result = mServer.sendMessage(header, content, callback);
        }
        catch (Exception e)
        {
            Log.log.warn(e.getMessage(), e);
        }
        return result;
    } // sendMessage

    public boolean sendMessage(String address, String classMethod, MMsgOptions options, Object content)
    {
        return sendMessage(null, address, classMethod, options, content, null);
    } // sendMessage

    public boolean sendMessage(String address, String classMethod, Object content)
    {
        return sendMessage(null, address, classMethod, null, content, null);
    } // sendMessage

    public boolean sendMessage(String sid, String address, String classMethod, MMsgOptions options, Object content, String callback)
    {
        boolean result = false;

        try
        {
            // define route
            MMsgRoute route = new MMsgRoute();
            MMsgDest dest = new MMsgDest(address, classMethod);
            route.addRouteDest(dest);

            // set header parameters
            MMsgHeader msgHeader = new MMsgHeader();
            if (options != null)
            {
                msgHeader.setOptions(options);
            }

            // define header
            msgHeader.setRoute(route);
            msgHeader.setCaller(serviceName);
            msgHeader.setSid(sid);

            // send message
            result = mServer.sendMessage(msgHeader, content, callback);
            if (result == false)
            {
                Log.log.warn("Failed to send message to "+address);
            }
        }
        catch (Exception e)
        {
            Log.log.warn("Failed to send message to "+address+". "+e.getMessage(), e);
        }
        return result;
    } // sendMessage

    public boolean sendDelayedMessage(String address, String classMethod, MMsgOptions options, Object content, long delaySecs)
    {
        return sendDelayedMessage(null, address, classMethod, options, content, null, delaySecs);
    } // sendMessage

    public boolean sendDelayedMessage(String address, String classMethod, Object content, long delaySecs)
    {
        return sendDelayedMessage(null, address, classMethod, null, content, null, delaySecs);
    } // sendMessage

    public boolean sendDelayedMessage(String sid, String address, String classMethod, MMsgOptions options, Object content, String callback, long delaySecs)
    {
        Map params = new HashMap();
        params.put("_ESB_ADDR_", address);
        params.put("_ESB_CLASSMETHOD_", classMethod);
        params.put("_ESB_OPTIONS_", options);
        params.put("_ESB_CONTENT_", content);

        ScheduledExecutor.getInstance().executeDelayed(this, "sendDelayedMessage", params, delaySecs, TimeUnit.SECONDS);
        return true;

        /*
        try
        {
            // define route
            MMsgRoute route = new MMsgRoute();
            MMsgDest dest = new MMsgDest(address, classMethod);
            route.addRouteDest(dest);

            // set header parameters
            MMsgHeader msgHeader = new MMsgHeader();
            if (options != null)
            {
                msgHeader.setOptions(options);
            }
            msgHeader.getOptions().setDelay(delaySecs);

            // define header
            msgHeader.setRoute(route);
            msgHeader.setCaller(serviceName);
            msgHeader.setSid(sid);

            // send message
            result = mServer.sendMessage(msgHeader, content, callback);
            if (result == false)
            {
                Log.log.warn("Failed to send message to "+address);
            }
        }
        catch (Exception e)
        {
            Log.log.warn("Failed to send message to "+address+". "+e.getMessage(), e);
        }
        return result;
        */
    } // sendMessage

    public void sendDelayedMessage(Map params)
    {
        String address = StringUtils.getString("_ESB_ADDR_", params);
        String classMethod = StringUtils.getString("_ESB_CLASSMETHOD_", params);
        MMsgOptions options = (MMsgOptions)params.get("_ESB_OPTIONS_");

        params.remove("_ESB_ADDR_");
        params.remove("_ESB_CLASSMETHOD_");
        params.remove("_ESB_OPTIONS_");
        
        Object content = params.get("_ESB_CONTENT_");

        sendMessage(address, classMethod, options, content);
    } // sendDelayedMessage

    /**
     * Sends a message to a queue or topic. 
     * 
     * @param address queue or topic name
     * @param classMethod processing class name and method. e.g., MAction.executeProcess
     * @param content the actual message content
     * @return
     */
    public boolean sendInternalMessage(String address, String classMethod, Object content)
    {
        return sendInternalMessage(null, address, null, classMethod, null, content, null);
    } // sendInternalMessage

    /**
     * Sends a message to a queue or topic.
     * 
     * @param address queue or topic name
     * @param className if the value is DEFAULT then the default handler's classname is used, e.g., MAction 
     * @param methodName method to be executed from the class, e.g., executeProcess. if className is null/empty then 
     * @param content the actual message content
     * @return
     */
    public boolean sendInternalMessage(String address, String className, String methodName, Object content)
    {
        return sendInternalMessage(null, address, className, methodName, null, content, null);
    } // sendInternalMessage
    
    public boolean sendInternalMessage(String sid, String address, String classMethod, MMsgOptions options, 
    								   Object content, String callback)
    {
        return sendInternalMessage(sid, address, null, classMethod, options, content, callback);
    }
    
    public boolean sendInternalMessage(String sid, String address, String className, String classMethod, MMsgOptions options, Object content, String callback)
    {
        boolean result = false;

        try
        {
            // define route
            MMsgRoute route = new MMsgRoute();
            MMsgDest dest;
            if(StringUtils.isBlank(className))
            {
                dest = new MMsgDest(address, classMethod);
            }
            else
            {
                dest = new MMsgDest(address, className, classMethod);
            }
            route.addRouteDest(dest);

            // set header parameters
            MMsgHeader msgHeader = new MMsgHeader();
            if (options != null)
            {
                msgHeader.setOptions(options);
            }

            // define header
            msgHeader.setInternal(true);
            msgHeader.setRoute(route);
            msgHeader.setCaller(serviceName);
            msgHeader.setSid(sid);

            // send message
            Log.log.info(String.format("Send to %s", dest));
            result = mServer.sendInternalMessage(msgHeader, content, callback);
            if (result == false)
            {
                Log.log.warn("Failed to send message to "+address);
            }
        }
        catch (Exception e)
        {
            Log.log.warn("Failed to send message to "+address+". "+e.getMessage(), e);
        }
        return result;
    } // sendInternalMessage
    
    public void sendEvent(Map params) throws Exception
    {
        if (params != null)
        {
        	params.put(Constants.EXECUTE_RUNBOOK_PRIORITY,  Integer.toString(Constants.RUNBOOK_PRIORITY_EVENT_INCREMENT_VALUE+Constants.RUNBOOK_PRIORITY_NORMAL_VALUE));       	
            String eventid = (String)params.get(Constants.EVENT_EVENTID);
            String processid = (String)params.get(Constants.EXECUTE_PROCESSID);
            String eventReference = (String)params.get(Constants.EVENT_REFERENCE);
            String eventEnd = (String)params.get(Constants.EVENT_END);
            String userid = (String)params.get(Constants.EXECUTE_USERID);
            String wiki = (String)params.get(Constants.EXECUTE_WIKI);
            String problemid = (String)params.get(Constants.EXECUTE_PROBLEMID);

            // remove PROCESS_NOTEVENT
            params.remove(Constants.EXECUTE_PROCESS_NOTEVENT);
            
            String eventAbort = (String)params.get(Constants.EVENT_EVENTABORT);
            if (StringUtils.isNotEmpty(eventAbort))
            {
            	if (!Boolean.parseBoolean(eventAbort))
            	{
            		params.put(Constants.EVENT_SEVERITY, "GOOD");
            		params.put(Constants.EVENT_CONDITION, "GOOD");
            	}
            }
            else
            {
        		params.put(Constants.EVENT_SEVERITY, "GOOD");
        		params.put(Constants.EVENT_CONDITION, "GOOD");
            }

	        if (StringUtils.isNotEmpty(eventid) && StringUtils.isNotEmpty(userid) && (StringUtils.isNotEmpty(eventReference) || StringUtils.isNotEmpty(processid)))
	        {
		        sendMessage(Constants.ESB_NAME_RSCONTROL, "MAction.executeProcess", params);
	        }
	        else
	        {
	            throw new Exception("Missing parameters for sendEvent - EVENT_EVENTID: "+eventid+" USERID: "+userid+" EVENT_REFERENCE: "+eventReference+" PROCESSID: "+processid);
	        }
        }
        else
        {
            Log.log.warn("Missing parameters for sendEvent");
        }
    } // sendEvent

    public void sendEventContinue(String eventid, String eventReference, String eventEnd, String wiki, String problemid, String userid, Map params)
    {
        if (params == null)
        {
	        params = new HashMap<String,String>();
        }

        // remove PROCESS_NOTEVENT
        params.remove(Constants.EXECUTE_PROCESS_NOTEVENT);

        params.put(Constants.EXECUTE_USERID, userid);
        if (!params.containsKey(Constants.EXECUTE_PROCESSID))
        {
            params.put(Constants.EXECUTE_PROCESSID, "");
        }
        params.put(Constants.EXECUTE_WIKI, wiki);
        params.put(Constants.EXECUTE_PROBLEMID, problemid);
        params.put(Constants.EVENT_EVENTID, eventid);
        params.put(Constants.EVENT_END, eventEnd);
        params.put(Constants.EVENT_REFERENCE, "");
        if (StringUtils.isNotBlank(eventReference))
        {
            params.put(Constants.EVENT_REFERENCE, eventReference);
        }
        try
        {
            sendEvent(params);
        }
        catch (Exception e)
        {
            Log.log.error("Failed to Send Event Continue", e);
        }
    } // sendEventContinue

    public void executeRunbook(String runbook, String problemid, String userid, Map params)
    {
        Map content = new HashMap(params);
        content.put(Constants.EXECUTE_WIKI, runbook);
        content.put(Constants.EXECUTE_PROBLEMID, problemid);
        content.put(Constants.EXECUTE_USERID, userid);
        content.put(Constants.EXECUTE_PROCESSID, "NEW");
        sendMessage(Constants.ESB_NAME_EXECUTEQUEUE, "MAction.executeProcess", content);
    } // executeRunbook

    public void submitForm(String formName, String buttonName, String userid, Map params)
    {
        params.put(Constants.HTTP_REQUEST_USERNAME, userid);
        params.put("FORM_VIEW_NAME", formName);
        params.put("BUTTON_NAME", buttonName);
        sendMessage("RSVIEW", "MAction.submitForm", params);
    } // submitForm

    public Map call(String address, String classMethod, Object content, long timeout) throws Exception
    {
        return call(address, classMethod, null, content, timeout);
    } // call

    /**
     * This will send a message to the remote system and block until the execution has completed. Once the callback is received, it will unblock this call.
     *
     * @return Map of parameter from the called method
     */
    public Map call(String address, String classMethod, MMsgOptions options, Object content, long timeout) throws Exception
    {
        Map result = null;

        try
        {
            // define route
            MMsgRoute route = new MMsgRoute();
            MMsgDest dest = new MMsgDest(address, classMethod);
            route.addRouteDest(dest);

            // set header parameters
            MMsgHeader msgHeader = new MMsgHeader();
            if (options != null)
            {
                msgHeader.setOptions(options);
            }

            // define header
            msgHeader.setRoute(route);
            msgHeader.setCaller(serviceName);

            // set session id
            String sid = Thread.currentThread().getName()+"/"+System.currentTimeMillis();
            msgHeader.setSid(sid);

            // send message with callback. full package name is provided to avoid the conflict between
            // same class name in different package (e.g., MESB in rscontrol and rsbase package
            String callback = "com.resolve.rsbase.MESB.callback";
            if (mServer.sendInternalMessage(msgHeader, content, callback) == false) {
                String msg = "Failed to send message to "+address;
                Log.log.warn(msg);
                throw new Exception(msg);
            }

            // block on sid lock
            ReentrantLock lock = LockUtils.getLock(sid);

            try
            {
                lock.lock();
                Condition condition = lock.newCondition();
                setCallbackCondition(sid, condition);

                if (condition.await(timeout, TimeUnit.MILLISECONDS) == false)
                {
                    Log.log.warn(String.format("ESB.call timedout after %d %s after waiting for response from - target: %s classMethod: %s",
                    						   timeout, TimeUnit.MILLISECONDS.toString(), address, classMethod));
                }
            }
            finally
            {
                lock.unlock();
            }

            // get callback results
            result = getAndRemoveCallbackResult(sid);
        }
        catch (Exception e)
        {
            Log.log.warn(e.getMessage(), e);
        }
        return result;
    } // call

    public static Condition getAndRemoveCondition(String sid)
    {
        Condition result = callbackCondition.get(sid);
        callbackCondition.remove(sid);

        return result;
    } // getAndRemoveCondition

    public static void setCallbackCondition(String sid, Condition condition)
    {
        callbackCondition.put(sid, condition);
    } // setCallbacCondition

    public static Map getAndRemoveCallbackResult(String sid)
    {
        Map result = callbackResult.get(sid);
        callbackResult.remove(sid);

        return result;
    } // getCallbackResult

    public static void setCallbackResult(String sid, Map value)
    {
        callbackResult.put(sid, value);
    } // setCallbackResult
        
    public boolean sendMessageRepeatedly(String address, String classMethod, Object content, long repeatCount)
    {
        return sendMessageRepeatedly(null, address, classMethod, null, content, null, repeatCount);
    } // sendDurableMessageRepeatedly
    
    public boolean sendMessageRepeatedly(String sid, String address, String classMethod, MMsgOptions options, Object content, String callback, long repeatCount)
    {
        boolean result = false;

        try
        {
            // define route
            MMsgRoute route = new MMsgRoute();
            MMsgDest dest = new MMsgDest(address, classMethod);
            route.addRouteDest(dest);

            // set header parameters
            MMsgHeader msgHeader = new MMsgHeader();
            if (options != null)
            {
                msgHeader.setOptions(options);
            }

            // define header
            msgHeader.setRoute(route);
            msgHeader.setCaller(serviceName);
            msgHeader.setSid(sid);

            // send message
            result = mServer.sendMessageRepeatedly(msgHeader, content, callback, repeatCount);
            if (result == false)
            {
                Log.log.warn("Failed to send durable message to "+address);
            }
        }
        catch (Exception e)
        {
            Log.log.warn("Failed to send durable message to "+address+". "+e.getMessage(), e);
        }
        return result;
    } // sendMessageRepeatedly
} // ESB
