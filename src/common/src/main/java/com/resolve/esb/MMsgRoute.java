/******************************************************************************
* (C) Copyright 2014
*
* Resolve Systems, LLC
*
* All rights reserved
* This software is distributed under license and may not be used, copied,
* modified, or distributed without the express written permission of
* Resolve Systems, LLC.
*
******************************************************************************/

package com.resolve.esb;

import java.util.Iterator;
import java.util.Vector;

public class MMsgRoute extends Vector
{
    public MMsgRoute()
    {
        super();
    } // MMsgRoute
    
    public MMsgRoute(MMsgRoute route)
    {
        super();
        
        this.addAll(route);
    } // MMsgRoute
    
    public MMsgRoute(MMsgDest dest)
    {
        super();
        
        this.add(dest);
    } // MMsgRoute
    
    public MMsgRoute(String route) throws Exception
    {
        super();
        
        if (route != null && !route.equalsIgnoreCase("UNKNOWN"))
        {
            String[] routeDests = route.split("&");
            if (routeDests.length > 0)
            {
                for (int i=0; i < routeDests.length; i++)
                {
                    this.add(new MMsgDest(routeDests[i]));
                }
            }
        }
    } // MMsgRoute
    
    public String toString()
    {
        String result = "";
        
        for (Iterator i=this.iterator(); i.hasNext(); )
        {
            MMsgDest msgRoute = (MMsgDest)i.next();
            result += msgRoute.toString();
            if (i.hasNext())
            {
                result += "&";
            }
        }
        return result;
    } // toString
    
    public MMsgDest getDest()
    {
        MMsgDest result = null;
        
        if (this.size() > 0)
        {
            result = (MMsgDest)this.firstElement();
        }
        return result;
    } // getDest
    
    public String getDestString()
    {
        String result = null;
        
        if (this.size() > 0)
        {
            result = ((MMsgDest)this.firstElement()).toString();
        }
        return result;
    } // getDestString
    
    public void addRouteDest(String dest) throws Exception
    {
        this.add(new MMsgDest(dest));
    } // addRouteDest
    
    public void addRouteDest(MMsgDest dest)
    {
        this.add(dest);
    } // addRouteDest
    
    public void insertRouteDest(String dest) throws Exception
    {
        this.add(0, new MMsgDest(dest));
    } // insertRouteDest
    
    public void insertRouteDest(MMsgDest dest)
    {
        this.add(0, dest);
    } // insertRouteDest
    
    public void insertNextRouteDest(String dest) throws Exception
    {
        if (this.size() == 0)
        {
           this.add(dest); 
        }
        this.add(1, new MMsgDest(dest));
    } // insertNextRouteDest
    
    public void insertNextRouteDest(MMsgDest dest)
    {
        if (this.size() == 0)
        {
           this.add(dest); 
        }
        this.add(1, dest);
    } // insertNextRouteDest
    
    public void removeRouteDest(String dest) throws Exception
    {
        for (Iterator i=this.iterator(); i.hasNext(); )
        {
            MMsgDest msgRoute = (MMsgDest)i.next();
            if (msgRoute.equals(dest))
            {
                i.remove();
            }
        }
    } // removeRouteDest
    
    public void removeRouteDest(MMsgDest dest) throws Exception
    {
        for (Iterator i=this.iterator(); i.hasNext(); )
        {
            MMsgDest msgRoute = (MMsgDest)i.next();
            if (msgRoute.equals(dest))
            {
                i.remove();
            }
        }
    } // removeRouteDest
    
    public void nextRouteDest()
    {
        if (this.size() > 0)
        {
            this.remove(0);
        }
    } // nextRouteDest
    
    public boolean hasNextRouteDest()
    {
        boolean result = false;
        if (this != null)
        {
            if (this.size() > 1)
            {
                result = true;
            }
        }
        return result;
    } // hasNextRouteDest
    
} // MMsgRoute
