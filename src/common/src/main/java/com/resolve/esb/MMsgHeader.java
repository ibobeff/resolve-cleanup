/******************************************************************************
* (C) Copyright 2014
*
* Resolve Systems, LLC
*
* All rights reserved
* This software is distributed under license and may not be used, copied,
* modified, or distributed without the express written permission of
* Resolve Systems, LLC.
*
******************************************************************************/

package com.resolve.esb;

import java.util.Hashtable;
import java.util.Iterator;

import com.resolve.util.Constants;
import com.resolve.util.StringUtils;

@SuppressWarnings("serial")
public class MMsgHeader extends Hashtable<Object, Object>
{
    public final static String MSGHEADER_PREFIX = "_MSGHEADER_";
    public final static String MSGSID           = MSGHEADER_PREFIX + "SID";      // user-defined identifier
    public final static String MSGTYPE          = MSGHEADER_PREFIX + "TYPE";     // message handler name
    public final static String MSGSOURCE        = MSGHEADER_PREFIX + "SOURCE";   // source queue name(GUID1-NAME1#Class1.Method1)
    public final static String MSGROUTE         = MSGHEADER_PREFIX + "ROUTE";    // list of MMsgDest (GUID1-NAME1#Class1.Method1&GUID2-NAME2#Class2.Method2&...)
    public final static String MSGTARGET        = MSGHEADER_PREFIX + "TARGET";   // target destination
    public final static String MSGINTERNAL      = MSGHEADER_PREFIX + "INTERNAL"; // internal message (i.e. use System Thread Pool)

    private String _caller;       // processed by MSender to update source ClassMethod

    public MMsgHeader()
    {
        this.put(MSGTYPE, "DEFAULT");
        this.put(MSGROUTE, new MMsgRoute());
        this.put(MSGTARGET, "");
        this.put(MSGINTERNAL, "false");

        _caller = null;
    } // MMsgHeader

    public MMsgHeader(MMsgHeader hdr) throws Exception
    {
        setSid(hdr.getSid());
        setType(hdr.getType());
        setSource(hdr.getSourceString());
        setRoute(new MMsgRoute(hdr.getRoute()));
        setTarget(hdr.getTarget());

        MMsgOptions oldOptions = hdr.getOptions();

        if (oldOptions.getFilePiece() != null)
        {
	        MMsgOptions options = new MMsgOptions();
            options.setFilePiece(oldOptions.getFilePiece());
            setOptions(options);
        }

        _caller = null;
    } // MMsgHeader

    public MMsgHeader(MMsgHeader hdr, String caller) throws Exception
    {
        this(hdr);
        setCaller(caller);
    } // MMsgHeader

    public String get_caller()
    {
        return _caller;
    }

    public void set_caller(String _caller)
    {
        this._caller = _caller;
    }

    public String toString()
    {
        String result = "";

        result += "Message Header\n";
        result += "  sid:         "+getSid()+"\n";
        result += "  type:        "+getType()+"\n";
        result += "  source:\n";
        if (getSource() != null)
        {
            result += "    "+getSource().getFormattedString()+"\n";
        }
        else
        {
            result += "    undefined\n";
        }
        result += "  target:      "+getTarget()+"\n";
        result += "  route:\n";
        for (Iterator<?> i=getRoute().iterator(); i.hasNext(); )
        {
            MMsgDest dest = (MMsgDest)i.next();
            result += "    "+dest.getFormattedString()+"\n";
        }
        result += "  _caller:     "+getCaller()+"\n";
        result += "  options:     "+getOptions().getFormattedString()+"\n";
        result += "  values:      "+super.toString();
        return result;
    } // toString

    public boolean isHeader(String fieldname)
    {
        boolean result = false;

        if (fieldname.startsWith(MSGHEADER_PREFIX))
        {
            result = true;
        }

        return result;
    } // isHeader

    public String getCaller()
    {
        return this._caller;
    } // getCaller

    public void setCaller(String caller)
    {
        this._caller = caller;
    } // setCaller

    public String getType()
    {
        return (String)this.get(MSGTYPE);
    } // getType

    public void setType(String type)
    {
        if (StringUtils.isEmpty(type))
        {
            type = "DEFAULT";
        }
        this.put(MSGTYPE, type);
    } // setType

    public MMsgDest getSource()
    {
        return (MMsgDest)this.get(MSGSOURCE);
    } // getSource

    public String getSourceString()
    {
        String result = null;

        MMsgDest source = getSource();
        if (source != null)
        {
            result = source.toString();
        }
        return result;
    } // getSourceString

    public void setSource(MMsgDest source)
    {
        if (source != null)
        {
            this.put(MSGSOURCE, source);
        }
    } // setSource

    public void setSource(String source) throws Exception
    {
        this.put(MSGSOURCE, new MMsgDest(source));
    } // setSource

    public MMsgRoute getRoute()
    {
        return (MMsgRoute)this.get(MSGROUTE);
    } // getRoute

    public String getRouteString()
    {
        String result = null;

        MMsgRoute route = getRoute();
        if (route != null)
        {
            result = route.toString();
        }
        return result;
    } // getRouteString

    public void setRoute(MMsgRoute route) throws Exception
    {
        if (route != null)
        {
            this.put(MSGROUTE, route);
        }
    } // setRoute

    public void setRoute(String route) throws Exception
    {
        this.put(MSGROUTE, new MMsgRoute(route));
    } // setRoute

    public void setRouteDest(String address, String classMethod) throws Exception
    {
        MMsgRoute route = new MMsgRoute();
        MMsgDest dest = new MMsgDest(address, classMethod);
        route.addRouteDest(dest);

        this.put(MSGROUTE, route);
    } // setRouteDest

    public void setEventType(String value)
    {
        if(value != null && !value.equals(""))
        {
            this.put(Constants.ESB_PARAM_EVENTTYPE, value);
        }
    }

    public String getEventType()
    {
        Object typeObject = this.get(Constants.ESB_PARAM_EVENTTYPE);
        return (typeObject != null)?((String) typeObject):"";
    }

    public MMsgDest getDest()
    {
        MMsgDest result = null;

        MMsgRoute route = getRoute();
        if (route != null)
        {
            result = route.getDest();
        }
        return result;
    } // getMsgDest

    public String getDestString()
    {
        String result = null;

        MMsgRoute route = getRoute();
        if (route != null)
        {
            result = route.getDestString();
        }
        return result;
    } // getDestString

    public void addRouteDest(String dest) throws Exception
    {
        MMsgRoute route = getRoute();
        if (route != null)
        {
            route.addRouteDest(dest);
        }
    } // addRouteDest

    public void addRouteDest(MMsgDest dest)
    {
        MMsgRoute route = getRoute();
        if (route != null)
        {
            route.addRouteDest(dest);
        }
    } // addRouteDest

    public void insertRouteDest(String dest) throws Exception
    {
        MMsgRoute route = getRoute();
        if (route != null)
        {
            route.insertRouteDest(dest);
        }
    } // insertRouteDest

    public void insertRouteDest(MMsgDest dest)
    {
        MMsgRoute route = getRoute();
        if (route != null)
        {
            route.insertRouteDest(dest);
        }
    } // insertRouteDest

    public void insertNextRouteDest(String dest) throws Exception
    {
        MMsgRoute route = getRoute();
        if (route != null)
        {
            route.insertNextRouteDest(dest);
        }
    } // insertRouteDest

    public void insertNextRouteDest(MMsgDest dest)
    {
        MMsgRoute route = getRoute();
        if (route != null)
        {
            route.insertNextRouteDest(dest);
        }
    } // insertRouteDest

    public void removeRouteDest(String dest) throws Exception
    {
        MMsgRoute route = getRoute();
        if (route != null)
        {
            route.removeRouteDest(dest);
        }
    } // removeRouteDest

    public void removeRouteDest(MMsgDest dest) throws Exception
    {
        MMsgRoute route = getRoute();
        if (route != null)
        {
            route.removeRouteDest(dest);
        }
    } // removeRouteDest

    public void nextRouteDest()
    {
        MMsgRoute route = getRoute();
        if (route != null)
        {
            route.nextRouteDest();
        }
    } // nextRouteDest

    public boolean hasNextRouteDest()
    {
        boolean result = false;
        MMsgRoute route = getRoute();
        if (route != null)
        {
            result = route.hasNextRouteDest();
        }
        return result;
    } // hasNextRouteDest

    public String getTarget()
    {
        return (String)this.get(MSGTARGET);
    } // getTarget

    public void setTarget(String target)
    {
        if (target != null)
        {
            this.put(MSGTARGET, target);
        }
    } // setTarget

    public String getTargetName()
    {
        String result = "";

        String target = getTarget();
        if (target != null)
        {
            String[] nameMethod = target.split("#");
            result = nameMethod[0];
        }

        return result;
    } // getTargetName

    public void setTargetName(String name)
    {
        String target = getTarget();
        if (target != null)
        {
            String[] nameMethod = target.split("#");
            String method = nameMethod[1];

            target = name.toUpperCase() + "#" + method;
        }
    } // setTargetName

    public String getTargetMethod()
    {
        String result = "";

        String target = getTarget();
        if (target != null)
        {
            String[] nameMethod = target.split("#");
            result = nameMethod[1];
        }

        return result;
    } // getTargetMethod

    public void setTargetMethod(String method)
    {
        String target = getTarget();
        if (target != null)
        {
            String[] nameMethod = target.split("#");
            String name = nameMethod[0];

            target = name + "#" + method;
        }
    } // setTargetMethod

    public String getSid()
    {
        return (String)this.get(MSGSID);
    } // getSid

    public void setSid(String sid)
    {
        if (sid != null)
        {
            this.put(MSGSID, sid);
        }
    } // setSid

    public MMsgOptions getOptions()
    {
        return new MMsgOptions(this);
    } // getOptions

    public String getOptionsString()
    {
        String result = null;

        MMsgOptions options = new MMsgOptions(this);
        if (options != null)
        {
            result = options.toString();
        }
        return result;
    } // getOptionsString
//
    public void setOptions(MMsgOptions options)
    {
        if (options != null)
        {
            for (Object key : options.keySet())
            {
                this.put(key, options.get(key));

                // use options target
                if (key instanceof String && ((String) key).equals(MMsgOptions.MSGTARGET))
                {
                    setTarget((String) options.get(key));
                }
            }
        }
    } // setOptions

    public void setOptions(String strOptions)
    {
        MMsgOptions options = new MMsgOptions(strOptions);

        if (options != null)
        {
            for (Object key : options.keySet())
            {
                this.put(key, options.get(key));

                // use options target
                if (key instanceof String && ((String) key).equals(MMsgOptions.MSGTARGET))
                {
                    setTarget((String) options.get(key));
                }
            }
        }
    } // setOptions

    public boolean getInternal()
    {
        boolean result = false;

        String value = (String)this.get(MSGINTERNAL);
        if (value != null && value.equalsIgnoreCase("true"))
        {
            result = true;
        }

        return result;
    } // setInternal

    public void setInternal(boolean internal)
    {
        this.put(MSGINTERNAL, ""+internal);
    } // setInternal

} // MMsgHeader
