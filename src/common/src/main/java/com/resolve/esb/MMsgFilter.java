/******************************************************************************
* (C) Copyright 2018
*
* Resolve Systems, LLC
*
* All rights reserved
* This software is distributed under license and may not be used, copied,
* modified, or distributed without the express written permission of
* Resolve Systems, LLC.
*
* @author hemant.phanasgaonkar
* 
******************************************************************************/

package com.resolve.esb;

import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ConcurrentMap;

import org.apache.commons.collections.MapUtils;

public class MMsgFilter
{
	public static Map<String, ? extends Object> filterMsg(Map<String, ? extends Object> msgMap)
    {
        ConcurrentMap<String, Object> filteredMsg = msgMap.containsKey("RESOLVE.JOB_ID") ?
        								  		    new ConcurrentHashMap<String, Object>() : null;
        								  		    
        if (filteredMsg == null) {
        	return msgMap;
        }
        
        if (MapUtils.isNotEmpty(msgMap) && MapUtils.isEmpty(filteredMsg)) {
        	msgMap.keySet().parallelStream()
        	.forEach(msgKey -> {
        		// HP TODO RESOLVE.JOB_ID should be in ResolveNamespace Enum or Enum Map with value as description may be
        		if (!msgKey.equalsIgnoreCase("RESOLVE.JOB_ID")) { 
        			filteredMsg.put(msgKey, (Object)msgMap.get(msgKey));
        		}
        	});
        }
        
        return filteredMsg;
    }
}
