/******************************************************************************
* (C) Copyright 2014
*
* Resolve Systems, LLC
*
* All rights reserved
* This software is distributed under license and may not be used, copied,
* modified, or distributed without the express written permission of
* Resolve Systems, LLC.
*
******************************************************************************/

package com.resolve.esb.amqp;

import java.io.IOException;
import java.security.KeyManagementException;
import java.security.NoSuchAlgorithmException;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;

import com.rabbitmq.client.AMQP;
import com.rabbitmq.client.Address;
import com.rabbitmq.client.BlockedListener;
import com.rabbitmq.client.Channel;
import com.rabbitmq.client.Connection;
import com.rabbitmq.client.ConnectionFactory;
import com.resolve.esb.ESBException;
import com.resolve.esb.MDriver;
import com.resolve.esb.MServerConfig;
import com.resolve.rsbase.MainBase;
import com.resolve.util.Log;
import com.resolve.util.StringUtils;

/**
 * This class is the lowest level member which directly talks to RabbitMQ using RabbitMQ APIs.
 *
 */
public class MAmqpDriver extends MDriver
{
    private static String hosts = "";
    private boolean isSSL = true;
    private /*static*/ AMQP.BasicProperties properties;
    private /*static*/ AMQP.BasicProperties durableProperties;
    private static Map<String, Object> queueTtlArgs = new HashMap<String, Object>();
    private static Map<String, Object> topicTtlArgs = new HashMap<String, Object>();
    Set<String> queues = new HashSet<String>();
    Set<String> durableQueues = new HashSet<String>();
    Set<String> topics = new HashSet<String>();
    Set<String> durableTopics = new HashSet<String>();
    
    static
    {
        queueTtlArgs.put("x-message-ttl", new Integer(MServerConfig.DEFAULT_MAX_QUEUE_MESSAGE_TTL));
        topicTtlArgs.put("x-message-ttl", new Integer(MServerConfig.DEFAULT_MAX_QUEUE_MESSAGE_TTL));
        topicTtlArgs.put("x-expires", new Integer(MServerConfig.DEFAULT_MAX_QUEUE_MESSAGE_TTL));
    }
    
    public MAmqpDriver(final MServerConfig config) throws ESBException
    {
        this.config = config;

        //generate the RabbitMQ host string
        //e.g., 192.168.1.1:5674,192.168.1.2:5674
        StringBuilder addresses = new StringBuilder();
        addresses.append(config.getBrokerAddr());
        if (StringUtils.isNotBlank(config.getBrokerAddr2()))
        {
            addresses.append(",");
            addresses.append(config.getBrokerAddr2());
        }
        hosts = addresses.toString();

        //Check Javadoc http://www.rabbitmq.com/releases/rabbitmq-java-client/v3.1.3/rabbitmq-java-client-javadoc-3.1.3/com/rabbitmq/client/AMQP.BasicProperties.html
        //properties = new AMQP.BasicProperties(contentType, contentEncoding, headers, deliveryMode, priority, correlationId, replyTo, expiration, messageId, timestamp, type, userId, appId, clusterId)
        properties = new AMQP.BasicProperties(null, null, null, null, null, null, null, "" + config.getMessageTTL(), null, null, null, null, null, null);

        // Create AMQP.BasicProperties for durable messaging
        durableProperties = new AMQP.BasicProperties(null, null, null, new Integer(2), null, null, null, "" + config.getMessageTTL(), null, null, null, null, null, null);
        
        // create connection factory
        boolean connected = false;
        Connection connection = getConnection();
        if (connection != null && connection.isOpen())
        {
            Channel channel;
            try
            {
                channel = connection.createChannel();
                if (channel != null && channel.isOpen())
                {
                    connected = true;
                    channel.close();
                    connection.close();
                }
            }
            catch (IOException e)
            {
                Log.log.error(e.getMessage(), e);
                throw new ESBException(e.getMessage(), e);
            }
            catch (/*Timeout*/Exception e)
            {
                Log.log.error(e.getMessage(), e);
                throw new ESBException(e.getMessage(), e);
            }
        }

        if (connected == false)
        {
            Log.log.fatal("Failed to connect to AMQP messaging service");
        }
    } // MDriverRabbitMQ

    public Connection getConnection() throws ESBException
    {
        Connection connection = null;
        boolean connected = false;
        String sslProtocol = MainBase.main.configESB.config.getSslProtocol();
        int connectionTimeout = MainBase.main.configESB.config.getConnectionTimeout();
        for (int connectRetry = 0; !connected && connectRetry < config.getCreateQueueRetry(); connectRetry++)
        {
            try
            {
                ConnectionFactory factory = new ConnectionFactory();
                if (isSSL)
                {
                    factory.useSslProtocol(sslProtocol);
                }
                factory.setConnectionTimeout(connectionTimeout);
                factory.setUsername(config.getUsername());
                factory.setPassword(config.getP_assword());
                factory.setAutomaticRecoveryEnabled(false);
                Address[] addresses = Address.parseAddresses(hosts);
                connection = factory.newConnection(addresses);
                if (connection != null && connection.isOpen())
                {
                    connected = true;
                    connection.addBlockedListener(new BlockedListener() {
						@Override
						public void handleBlocked(String reason) throws IOException {
							Log.log.error(String.format("ESB connection is blocked due to [%s]", reason));
						}

						@Override
						public void handleUnblocked() throws IOException {
							Log.log.info("ESB connection is unblocked");
						}
                    });
                }
            }
            catch (IOException e)
            {
            	if (StringUtils.isNotBlank(e.getMessage()) && 
            		!e.getMessage().toLowerCase().contains(ESBException.ESB_CONNECTION_REFUSED_ERR_MSG)) {
            		Log.log.error(e.getMessage() + ". Use Loglevel TRACE to see stack");
            	}
            	
                if (Log.log.isTraceEnabled()) {
                	Log.log.trace(e.getMessage(), e);
                }
                throw new ESBException(e.getMessage(), e);
            }
            catch (KeyManagementException e)
            {
                Log.log.error(e.getMessage(), e);
                throw new ESBException(e.getMessage(), e);
            }
            catch (NoSuchAlgorithmException e)
            {
                Log.log.error(e.getMessage(), e);
                throw new ESBException(e.getMessage(), e);
            }
            catch (/*Timeout*/Exception e)
            {
                Log.log.error(e.getMessage(), e);
                throw new ESBException(e.getMessage(), e);
            }
        }
        return connection;
    } // getConnection

    /**
     * Removes Queues from the server
     */
    public void close() throws ESBException
    {
    }

    public boolean createQueue(final Channel channel, final String queueName) throws ESBException
    {
        boolean result = true;
        try
        {
            if(queueName.contains("#")) {
            	channel.queueDeclare(queueName, false, false, true, topicTtlArgs);
            }else 
            {
            	channel.queueDeclare(queueName, false, false, true, queueTtlArgs);
            }
            queues.add(queueName);
        }
        catch (IOException e)
        {
            Log.log.error(e.getMessage(), e);
            throw new ESBException(e.getMessage(), e);
        }
        return result;
    }

    public boolean createQueue(final Channel channel, final String queueName,Map<String,Object> properties) throws ESBException
    {
        boolean result = true;
        try
        {
            if(queueName.contains("#")) {
            	channel.queueDeclare(queueName, false, false, true, topicTtlArgs);
            }else 
            {
            	channel.queueDeclare(queueName, false, false, true, queueTtlArgs);
            }
            queues.add(queueName);
        }
        catch (IOException e)
        {
            Log.log.error(e.getMessage(), e);
            throw new ESBException(e.getMessage(), e);
        }
        return result;
    }
    
    /**
     * Exchange in AMQP is similar to Topic in JMS. Subscribers normally
     * subscribes to a queue that is bounded to the exchange. When a message
     * arrives at the exchange it delivers to every queue that is bounded to it.
     *
     * @param channel
     * @param exchangeName
     * @return
     */
    public boolean createExchange(final Channel channel, final String exchangeName) throws ESBException
    {
        boolean result = true;
        try
        {
            // fanout means the exchange will send message to all the queues
            // that are bounded to this exchange.
            channel.exchangeDeclare(exchangeName, "fanout", false);
            topics.add(exchangeName);
        }
        catch (IOException e)
        {
            Log.log.error(e.getMessage(), e);
            throw new ESBException(e.getMessage(), e);
        }
        return result;
    }

    public boolean subscribe(final Channel channel, final String exchangeName, final String subscriberId) throws ESBException
    {
        boolean result = true;
        try
        {
            // actually in RabbitMQ exchange every subscribers listens
            // to a unique queue which is bounded to an exchange.
            String queueName = subscriberId;
            String routingKey = subscriberId;

            // try to declare it if it isn't already created, not harm trying
            createExchange(channel, exchangeName);
            createQueue(channel, queueName);
            // now bind the subscriber queue to the exchange.
            channel.queueBind(queueName, exchangeName, routingKey);
        }
        catch (IOException e)
        {
            Log.log.error(e.getMessage(), e);
            throw new ESBException(e.getMessage(), e);
        }
        return result;
    }

    public boolean unsubscribe(final Channel channel, final String exchangeName, final String subscriberId) throws ESBException
    {
        boolean result = true;
        try
        {
            if (channel != null && channel.isOpen())
            {
                // actually in exchange every subscribers listens
                // to a unique queue which is bounded to an exchange.
                String queueName = subscriberId;
                String routingKey = subscriberId;

                channel.queueUnbind(queueName, exchangeName, routingKey);
            }
        }
        catch (IOException e)
        {
            Log.log.error(e.getMessage(), e);
            throw new ESBException(e.getMessage(), e);
        }
        return result;
    }

    /**
     * This sends a message to specified queue.
     *
     * @param channel
     * @param queueName
     * @param message
     * @return
     * @throws ESBException
     */
    public boolean send(final Channel channel, final String queueName, final byte[] message) throws ESBException
    {
        /*
        if (!queues.contains(queueName) && !durableQueues.contains(queueName))
        {
            throw new ESBException("Specified queue " + queueName + " does not exist.");
        }*/
        
        boolean result = true;
        try
        {
            boolean durable = durableQueues.contains(queueName) ? true : false;
            AMQP.BasicProperties amqpProps = durable ? durableProperties : properties;
            
            if (durable)
            {
                channel.confirmSelect(); // Use confirm to check delivery to broker
                if (Log.log.isTraceEnabled()) {
	                Log.log.trace("Next Publish Sequence # on " + queueName + " = " + channel.getNextPublishSeqNo());
                }
            }
            
            if (Log.log.isDebugEnabled()) {
	            Log.log.debug("send to " + queueName + " : durable = " + durable);
            }
            if(queueName.contains("#")) {
            	channel.queueDeclare(queueName, durable, false, true, topicTtlArgs);
            	synchronized(this) {
            	    channel.basicPublish("", queueName, amqpProps, message);
            	}
            }else 
            {
            	channel.queueDeclare(queueName, durable, false, true, queueTtlArgs);
            	synchronized(this) {
            	    channel.basicPublish("", queueName, amqpProps, message);
            	}
            }
            if (durable)
            {
                result = channel.waitForConfirms();
            }
            // System.out.println(" [x] Sent '" + message + "'");
        }
        catch (Exception e)
        {
            Log.log.error(e.getMessage(), e);
            throw new ESBException(e.getMessage(), e);
        }
        return result;
    }

    /**
     * This sends message to a queue repeatedly.
     *
     * @param channel
     * @param queueName
     * @param message
     * @param repeatCount
     * @return
     * @throws ESBException
     */
    public boolean sendRepeatedly(final Channel channel, final String queueName, final byte[] message, long repeatCount) throws ESBException
    {
        /*
        if (!queues.contains(queueName) && !durableQueues.contains(queueName))
        {
            throw new ESBException("Specified queue " + queueName + " does not exist.");
        }*/
        
        if (repeatCount <= 0)
        {
            return false;
        }
        
        if (repeatCount == 1)
        {
            return send(channel, queueName, message);
        }

        boolean result = true;
        try
        {
            boolean durable = durableQueues.contains(queueName) ? true : false;
            AMQP.BasicProperties amqpProps = durable ? durableProperties : properties;
            
            if (durable)
            {
                channel.confirmSelect(); // Use confirm to check delivery to broker
            }
            
            Log.log.info("sendRepeatedly to " + queueName + " with repeat count of " + repeatCount + " : durable = " + durable);
            
            if(queueName.contains("#")) {
            	channel.queueDeclare(queueName, durable, false, true, topicTtlArgs);
            }else 
            {
            	channel.queueDeclare(queueName, durable, false, true, queueTtlArgs);
            }
            
            for (long i = 0; i < repeatCount; i++)
            {
                if (durable)
                {
                    if (Log.log.isInfoEnabled())
                    {
                        if (i == 0)
                        {
                            Log.log.info("Repeat Count of " + repeatCount + ", first publishing sequence # on " + queueName + " = " + channel.getNextPublishSeqNo());
                        }
                        
                        if ( i == (repeatCount - 1))
                        {
                            Log.log.info("Repeat Count of " + repeatCount + ", last publishing sequence # on " + queueName + " = " + channel.getNextPublishSeqNo());
                        }
                    }
                    
                    if (Log.log.isTraceEnabled() && (i > 0 && i < (repeatCount - 1)))
                    {
                        Log.log.trace("Next Publish Sequence # on " + queueName + " = " + channel.getNextPublishSeqNo());
                    }
                }
                
                synchronized(this) {
                    channel.basicPublish("", queueName, amqpProps, message);
                }
                // System.out.println(" [x] Sent '" + message + "'");
            }
            
            if (durable)
            {
                try
                {
                    channel.waitForConfirmsOrDie();
                }
                catch(IOException | InterruptedException ioe)
                {
                    result = false;
                }
            }
        }
        catch (Exception e)
        {
            Log.log.error(e.getMessage(), e);
            throw new ESBException(e.getMessage(), e);
        }
        return result;
    }
    
    /**
     * Publishes a message to an exchange (similar to JMS Topic)
     *
     * @param channel
     * @param topicName
     * @param message
     * @return
     * @throws ESBException
     */
    public boolean publish(final Channel channel, final String topicName, final byte[] message) throws ESBException
    {
        /*
        if (!topics.contains(topicName) && !durableTopics.contains(topicName))
        {
            throw new ESBException("Specified topic/exchange " + topicName + " does not exist.");
        }*/
        
        boolean result = true;
        try
        {
            String exchangeName = topicName;
            String routingKey = topicName;

            boolean durable = durableTopics.contains(topicName) ? true : false;
            AMQP.BasicProperties amqpProps = durable ? durableProperties : properties;
            
            if (durable)
            {
                channel.confirmSelect(); // Use confirm to check delivery to broker
            }
            
            synchronized(this) {
                channel.basicPublish(exchangeName, routingKey, amqpProps, message);
            }
            
            if (durable)
            {
                try
                {
                    channel.waitForConfirmsOrDie();
                }
                catch(IOException | InterruptedException ioe)
                {
                    result = false;
                }
            }
        }
        catch (IOException e)
        {
            Log.log.error(e.getMessage(), e);
            throw new ESBException(e.getMessage(), e);
        }
        return result;
    }
    
    /**
     * Publishes a message to an exchange (similar to JMS Topic) repeatedly
     *
     * @param channel
     * @param topicName
     * @param message
     * @param repeatCount
     * @return
     * @throws ESBException
     */
    public boolean publishRepeatedly(final Channel channel, final String topicName, final byte[] message, long repeatCount) throws ESBException
    {
        boolean result = true;
        try
        {
            String exchangeName = topicName;
            String routingKey = topicName;
            
            boolean durable = durableTopics.contains(topicName) ? true : false;
            AMQP.BasicProperties amqpProps = durable ? durableProperties : properties;
            
            if (durable)
            {
                channel.confirmSelect(); // Use confirm to check delivery to broker
            }
            
            for (long i = 0; i < repeatCount; i++)
            {
                if (durable)
                {
                    if (Log.log.isInfoEnabled())
                    {
                        if (i == 0)
                        {
                            Log.log.info("Repeat Count of " + repeatCount + ", first publishing sequence # on " + topicName + " = " + channel.getNextPublishSeqNo());
                        }
                        
                        if ( i == (repeatCount - 1))
                        {
                            Log.log.info("Repeat Count of " + repeatCount + ", last publishing sequence # on " + topicName + " = " + channel.getNextPublishSeqNo());
                        }
                    }
                    
                    if (Log.log.isTraceEnabled() && (i > 0 && i < (repeatCount - 1)))
                    {
                        Log.log.trace("Next Publish Sequence # on " + topicName + " = " + channel.getNextPublishSeqNo());
                    }
                }
                synchronized(this) {
                    channel.basicPublish(exchangeName, routingKey, amqpProps, message);
                }
            }
            
            if (durable)
            {
                try
                {
                    channel.waitForConfirmsOrDie();
                }
                catch(IOException | InterruptedException ioe)
                {
                    result = false;
                }
            }
        }
        catch (IOException e)
        {
            Log.log.error(e.getMessage(), e);
            throw new ESBException(e.getMessage(), e);
        }
        return result;
    }
    
    public boolean registerQueue(final String queuename)
    {
        boolean result = false;

        try
        {
            Log.log.info("Creating new non durable queue: " + queuename);
            createQueue(getConnection().createChannel(), queuename);
            result = true;
        }
        catch (Exception e)
        {
            Log.log.warn("Error creating non-durable queue " + queuename + " : " + e.getMessage() + ". Use Loglevel TRACE to see complete stack trace.");
            Log.log.trace("Error creating non-durable queue " + queuename + " : " + e.getMessage(), e);
        }
        return result;
    } // registerQueue
    
    public boolean registerDurableQueue(final String queuename)
    {
        boolean result = false;

        try
        {
            Log.log.info("Creating new durable queue: " + queuename);
            createDurableQueue(getConnection().createChannel(), queuename);
            result = true;
        }
        catch (Exception e)
        {
            Log.log.warn("Error creating durable queue " + queuename + " : " + e.getMessage() + ". Use Loglevel TRACE to see complete stack trace.");
            Log.log.trace("Error creating durable queue " + queuename + " : " + e.getMessage(), e);
        }
        return result;
    } // registerQueue
    
    public boolean createDurableQueue(final Channel channel, final String queueName) throws ESBException
    {
        boolean result = true;
        try
        {
            channel.queueDeclare(queueName, true, false, false, queueTtlArgs);
            durableQueues.add(queueName);
        }
        catch (IOException e)
        {
            Log.log.error(e.getMessage(), e);
            throw new ESBException(e.getMessage(), e);
        }
        return result;
    }
    
    /**
     * Exchange in AMQP is similar to Topic in JMS. Subscribers normally
     * subscribes to a queue that is bounded to the exchange. When a message
     * arrives at the exchange it delivers to every queue that is bounded to it.
     *
     * @param channel
     * @param exchangeName
     * @return
     */
    public boolean createDurableExchange(final Channel channel, final String exchangeName) throws ESBException
    {
        boolean result = true;
        try
        {
            // fanout means the exchange will send message to all the queues
            // that are bounded to this exchange.
            channel.exchangeDeclare(exchangeName, "fanout", true);
            durableTopics.add(exchangeName);
        }
        catch (IOException e)
        {
            Log.log.error(e.getMessage(), e);
            throw new ESBException(e.getMessage(), e);
        }
        return result;
    }
    
    public boolean subscribeDurable(final Channel channel, final String exchangeName, final String subscriberId) throws ESBException
    {
        boolean result = true;
        try
        {
            // actually in RabbitMQ exchange every subscribers listens
            // to a unique queue which is bounded to an exchange.
            String queueName = subscriberId;
            String routingKey = subscriberId;

            // try to declare it if it isn't already created, not harm trying
            createDurableExchange(channel, exchangeName);
            createDurableQueue(channel, queueName);
            // now bind the subscriber queue to the exchange.
            channel.queueBind(queueName, exchangeName, routingKey);
        }
        catch (IOException e)
        {
            Log.log.error(e.getMessage(), e);
            throw new ESBException(e.getMessage(), e);
        }
        return result;
    }
} // MAmqpDriver