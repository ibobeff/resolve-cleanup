/******************************************************************************
* (C) Copyright 2014
*
* Resolve Systems, LLC
*
* All rights reserved
* This software is distributed under license and may not be used, copied,
* modified, or distributed without the express written permission of
* Resolve Systems, LLC.
*
******************************************************************************/

package com.resolve.esb;

public abstract class MDriver implements MDriverInterface
{
    protected MServerConfig config;

    public MServerConfig getConfig()
    {
        return config;
    } // getConfig
} // MDriver
