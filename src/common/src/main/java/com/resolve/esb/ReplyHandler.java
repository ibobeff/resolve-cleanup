/******************************************************************************
* (C) Copyright 2014
*
* Resolve Systems, LLC
*
* All rights reserved
* This software is distributed under license and may not be used, copied,
* modified, or distributed without the express written permission of
* Resolve Systems, LLC.
*
******************************************************************************/

package com.resolve.esb;

import java.util.Hashtable;

import com.resolve.util.Log;

public class ReplyHandler extends MHandler
{
    public ReplyHandler()
    {
        super();
        
        type = "REPLY";
    } // ReplyHandler

	protected void runInternal() 
    {
		try 
        {
            System.out.println(type+" "+MUtils.dumpMessage(msg));
                
            if (mSender != null)
            {
                MMsgHeader newHeader = new MMsgHeader();
                newHeader.setType("ECHO");
                newHeader.setRoute(msgHeader.getSourceString());
                
                Hashtable newMsg = new Hashtable();
                newMsg.put("STATUS", "OK");
                mSender.sendMessage(newHeader, newMsg);
            }
		} 
        catch (Exception e) 
        {
            Log.log.error("Failed to execute ReplyHandler: "+e.getMessage(), e);
		} 
	} // run

} // ReplyHandler
