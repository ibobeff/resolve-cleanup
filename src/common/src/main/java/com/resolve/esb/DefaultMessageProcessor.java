/******************************************************************************
* (C) Copyright 2014
*
* Resolve Systems, LLC
*
* All rights reserved
* This software is distributed under license and may not be used, copied,
* modified, or distributed without the express written permission of
* Resolve Systems, LLC.
*
******************************************************************************/

package com.resolve.esb;

import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.locks.ReentrantLock;
import com.resolve.rsbase.MainBase;
import com.resolve.thread.ScheduledExecutor;
import com.resolve.thread.TaskExecutor;
import com.resolve.util.Constants;
import com.resolve.util.Log;
import com.resolve.util.StringUtils;

/**
 * This is the default message processor that is used by the message listeners. This class
 * is extended by RSControlMessageProcessor to add some additional functionality to
 * message processing mechanism.
 */
public class DefaultMessageProcessor
{
    protected Map<String, MServiceThread> serviceThreads = new HashMap<String, MServiceThread>();

    protected MServer mServer;
    protected MListener mListener;
    
    public MServer getMServer()
    {
        return mServer;
    }

    public void setMServer(MServer mServer)
    {
        this.mServer = mServer;
    }

    public MListener getMListener()
    {
        return mListener;
    }

    public void setMListener(MListener mListener)
    {
        this.mListener = mListener;
    }

    protected ReentrantLock lock = new ReentrantLock();

    public Map<String, MServiceThread> getServiceThreads()
    {
        return serviceThreads;
    }

    public void setServiceThreads(Map<String, MServiceThread> serviceThreads)
    {
        this.serviceThreads = serviceThreads;
    }

    /**
     * Subclasses (e.g. RSControlMessageProcessor) overrides this.
     *
     * @param msgHeader
     * @param messageId
     * @param message
     */
    public void processMessage(MMsgHeader msgHeader, String messageId, Object message)
    {

        if (msgHeader != null)
        {
            String classMethod = msgHeader.getDest().getClassMethod();
            String className = msgHeader.getDest().getClassname();
            String methodName = msgHeader.getDest().getMethodname();
            String delay = msgHeader.getOptions().getDelay();

            long delayTime = 0;
            if (!StringUtils.isEmpty(delay) && !delay.equalsIgnoreCase("NOW"))
            {
                delayTime = Long.parseLong(delay);
            }

            if (Log.log.isTraceEnabled())
            {
                Log.log.trace("Message received with Id: " + messageId);
                Log.log.trace("  source: " + msgHeader.getSourceString());
                Log.log.trace("  method: " + className + "." + methodName);
                Log.log.trace("  delay: " + delayTime);
            }

            try
            {
                Class<?> invokeClass = null;

                // check SERVICE_QUEUE
                if (className.equals(Constants.ESB_SERVICE_QUEUE))
                {
                    MServiceThread serviceThread = serviceThreads.get(classMethod);
                    if (serviceThread == null)
                    {
                        throw new Exception("Missing MServiceThread: " + classMethod);
                    }

                    serviceThread.addMessage(mListener.getParams(message));
                }

                // check explicit class handlers
                else if (mServer != null && !mServer.getHandlers().isEmpty() && mServer.getHandlers().containsKey(className))
                {
                    invokeClass = mServer.getHandler(className);
                    Log.log.trace("using: " + invokeClass.getName());
                }

                // check for groovy MService wrapper
                // BD: Added the (.) so we can add class name that
                // starts with "MService" (e.g., MServiceNow.java).
                else if (invokeClass == null && className.startsWith("MService."))
                {
                    // try com.resolve.rsbase.MService
                    try
                    {
                        Log.log.trace("trying: com.resolve.rsbase.MService");
                        invokeClass = Class.forName("com.resolve.rsbase.MService");

                        // fix classname and method name
                        msgHeader.getDest().setClassname("MService");
                        msgHeader.getDest().setMethodname((className + "." + methodName).substring(8));
                    }
                    // try defaultClassPackage.MService
                    // (fall-through
                    // below)
                    catch (Exception e)
                    {
                        invokeClass = null;
                    }
                }

                // try specified defaultClassPackage
                if (invokeClass == null)
                {
                    if (mListener!=null)
                    {
                        try
                        {
                            String invokeClassName = mListener.getDefaultClassPackage() + "." + className;
    
                            Log.log.trace("trying: " + invokeClassName);
                            invokeClass = Class.forName(invokeClassName);
                        }
                        catch (Exception e)
                        {
                            invokeClass = null;
                        }
                    }
                }

                // try com.resolve.<service>
                if (invokeClass == null)
                {
                    try
                    {
                        String invokeClassName = "com.resolve." + MainBase.main.release.getName().toLowerCase() + "." + className;

                        Log.log.trace("trying: " + invokeClassName);
                        invokeClass = Class.forName(invokeClassName);
                    }
                    catch (Exception e)
                    {
                        invokeClass = null;
                    }
                }

                // try com.resolve.rsbase
                if (invokeClass == null)
                {
                    try
                    {
                        String invokeClassName = "com.resolve.rsbase." + className;

                        Log.log.trace("trying: " + invokeClassName);
                        invokeClass = Class.forName(invokeClassName);
                    }
                    catch (Exception e)
                    {
                        invokeClass = null;
                    }
                }

                // try fully qualified class
                if (invokeClass == null)
                {
                    try
                    {
                        String invokeClassName = className;

                        Log.log.trace("trying: " + invokeClassName);
                        invokeClass = Class.forName(invokeClassName);
                    }
                    catch (Exception e)
                    {
                        invokeClass = null;
                    }
                }

                // execute class.method handler
                if (invokeClass != null)
                {
                    // execute handler
                    msgHeader.put("INVOKECLASS", invokeClass);

					Object params = mListener.getParams(message);

					if (Log.log.isDebugEnabled()) {
						Log.log.trace("DefaultMessageProcessor invoking " + className + "." + methodName
								+ (delayTime == 0 ? " immediately" : " after " + delayTime + " secs."));
					}

					if (delayTime == 0) {
						// immediate delivery
						TaskExecutor.execute(messageId, msgHeader, mListener.getDefaultHandler(),
								"execute", params);
					} else {
						// delayed delivery
						ScheduledExecutor.getInstance().executeDelayed(messageId, msgHeader,
								mListener.getDefaultHandler(), "execute", params, delayTime, TimeUnit.SECONDS);
					}
                }
            }
            catch (Throwable e)
            {
                Log.log.warn("Failed to execute message handler: " + e.getMessage(), e);
            }

            if (Log.log.isTraceEnabled())
            {
                Log.log.trace("Message Id: " + messageId);
            }
        }
    }
}
