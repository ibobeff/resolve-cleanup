/******************************************************************************
* (C) Copyright 2014
*
* Resolve Systems, LLC
*
* All rights reserved
* This software is distributed under license and may not be used, copied,
* modified, or distributed without the express written permission of
* Resolve Systems, LLC.
*
******************************************************************************/

package com.resolve.esb;

import java.util.Hashtable;
import java.util.Iterator;
import java.util.Map;

public class MUtils
{
    public static String dumpMessage(Object msg) throws ESBException
    {
        String result = "";
        
        if (msg instanceof Hashtable)
        {
            result = dumpMessage((Hashtable)msg);
        }
        else
        {
            result = msg.toString();
        }
            
        return result;
    } // dumpMessage
    
    public static String dumpMessage(Map msg) throws ESBException
    {
        String result = "";
        
        for (Iterator i = msg.entrySet().iterator(); i.hasNext();)
        {
            Map.Entry entry = (Map.Entry)i.next();
            result += entry.getKey()+"="+entry.getValue()+"\n";
        }
        return result;
    } // dumpMessage
    
} // MUtils
