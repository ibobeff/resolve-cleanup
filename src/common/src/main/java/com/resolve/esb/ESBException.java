/******************************************************************************
* (C) Copyright 2014
*
* Resolve Systems, LLC
*
* All rights reserved
* This software is distributed under license and may not be used, copied,
* modified, or distributed without the express written permission of
* Resolve Systems, LLC.
*
******************************************************************************/

package com.resolve.esb;

/**
 * This exception is thrown from the ESB layer. In general when a protocol specific
 * class catches catches its own exception this exception get thrown to the outside world.
 */
public class ESBException extends Exception
{
	public static final String ESB_CONNECTION_CLOSED_ERR_MSG = "Connection closed";
	public static final String ESB_COMPONENT_SHUTTING_DONW_ERR_MSG = "Component shutting down";
	public static final String ESB_CONNECTION_REFUSED_ERR_MSG = "Connection refused";
	
    private static final long serialVersionUID = 8486072769471686698L;
    
    public ESBException(String message)
    {
        super(message);
    }
    public ESBException(String message, Throwable t)
    {
        super(message, t);
    }
}
