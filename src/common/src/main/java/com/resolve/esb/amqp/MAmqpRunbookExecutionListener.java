package com.resolve.esb.amqp;

import java.io.IOException;
import java.util.HashMap;
import java.util.Map;
import com.rabbitmq.client.AMQP.BasicProperties;
import com.rabbitmq.client.Channel;
import com.rabbitmq.client.DefaultConsumer;
import com.rabbitmq.client.Envelope;
import com.resolve.esb.DefaultMessageProcessor;
import com.resolve.esb.MMsgContent;
import com.resolve.esb.MMsgHeader;
import com.resolve.util.Log;
import com.resolve.util.ObjectProperties;
import com.resolve.util.performance.ExecutionTimeUtil;
import com.resolve.util.performance.datasources.ExecutionStep;
import com.resolve.util.performance.datasources.ExecutionStepPhase;

public class MAmqpRunbookExecutionListener extends DefaultConsumer {

	private boolean durableChannelUsed = false;

	public static DefaultMessageProcessor msgProcessor = new DefaultMessageProcessor();

	private MAmqpServer mServer;

	public MAmqpRunbookExecutionListener(Channel channel, boolean durableChannelUsed, MAmqpServer mServer) {
		super(channel);
		this.durableChannelUsed = durableChannelUsed;
		this.mServer = mServer;
	}

	public static void acknowledgeRunbookExecution(Map params, MAmqpServer mServer) {
      if(params != null) {

        String jobId = null;
        if(params.containsKey("RESOLVE.JOB_ID")) {
            jobId = (String) params.get("RESOLVE.JOB_ID");
        } else if(params.containsKey("params")) {
            Map internalParams = (Map) params.get("params");
            if(internalParams.containsKey("RESOLVE.JOB_ID")) {
                jobId = (String) internalParams.get("RESOLVE.JOB_ID");
            }
        }
        
        if(jobId != null) {
            MAmqpRunbookExecutionListener.acknowledgeRunbookExecution(jobId, mServer);
        }
      }
	}
	
	public static void acknowledgeRunbookExecution(String messageId, MAmqpServer mServer) {
		if (messageId != null) {
			Channel channel = mServer.getUnackedMessageChannel(messageId);
			if (channel != null) {
				try {
					channel.basicAck(Long.parseLong(messageId), false);
					mServer.unregisterUnackedMessageId(messageId);
				} catch (NumberFormatException e) {
					Log.log.error("Failed to parse runbook execution message id as long: " + messageId);
				} catch (IOException e) {
					Log.log.error("Failed to acknowledge runbook execution for message id: " + messageId, e);
				}
			}
		}
	}

	private MMsgHeader setMMsgHeader(final HashMap<String, Object> msg) throws Exception {
		MMsgHeader mMsgHeader = new MMsgHeader();

		if (msg != null) {
			for (String name : msg.keySet()) {
				if (MMsgHeader.MSGTYPE.equals(name)) {
					mMsgHeader.setType((String) msg.get(MMsgHeader.MSGTYPE));
				} else if (MMsgHeader.MSGSOURCE.equals(name)) {
					mMsgHeader.setSource((String) msg.get(MMsgHeader.MSGSOURCE));
				} else if (MMsgHeader.MSGROUTE.equals(name)) {
					mMsgHeader.setRoute((String) msg.get(MMsgHeader.MSGROUTE));
				} else {
					Object value = msg.get(name);
					if (value != null) {
						mMsgHeader.put(name, value);
					}
				}
			}
		}
		mMsgHeader.set_caller(null);
		return mMsgHeader;
	}

	@Override
	public void handleDelivery(String consumerTag, Envelope env, BasicProperties properties, byte[] body)
			throws IOException {
		String messageId = "" + env.getDeliveryTag();
		ExecutionTimeUtil.log(messageId, null, null, ExecutionStep.MESSAGE_CONSUMED, ExecutionStepPhase.START);
		mServer.registerUnackedMessageId(messageId,
				durableChannelUsed ? mServer.getDurableChannel() : mServer.getChannel());

		try {
			if (body != null && body.length > 0) {
				Object message = ObjectProperties.deserialize(body);
				if (message != null) {
					MMsgHeader msgHeader = setMMsgHeader((HashMap) message);
					if (Log.log.isTraceEnabled()) {
						Log.log.trace("Received delivery tag: " + env.getDeliveryTag());
						Log.log.trace("Message received: " + message);
						Log.log.trace("Message header: " + msgHeader);
					}
					// Call the method to process this message.
					Log.log.info("pollEventQueue calling DefaultMessageProcessor.processMessage for mesage with id "
							+ messageId);
					
					// add RESOLVE.JOB_ID for acknowledgement after runbook execution
			        if (message instanceof Map)
			        {
			            Object content = ((Map) message).get(MMsgContent.MSGCONTENT);
			            if(content instanceof Map) {
                    		((Map) content).put("RESOLVE.JOB_ID", messageId);
                    	}
			        }
					msgProcessor.processMessage(msgHeader, messageId, message);
					ExecutionTimeUtil.log(messageId, null, null, ExecutionStep.MESSAGE_CONSUMED, ExecutionStepPhase.END);
				} else {
					Log.log.warn("pollEventQueue basicGet received message whose body failed to deserialize");
				}
			} else {
				Log.log.warn("pollEventQueue basicGet received message with null or 0 length body");
			}
		} catch (Exception ex) {
			if (messageId != null) {
				acknowledgeRunbookExecution(messageId, mServer);
			}
			throw new RuntimeException(ex);
		}
	}
}

