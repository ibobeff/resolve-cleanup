/******************************************************************************
* (C) Copyright 2014
*
* Resolve Systems, LLC
*
* All rights reserved
* This software is distributed under license and may not be used, copied,
* modified, or distributed without the express written permission of
* Resolve Systems, LLC.
*
******************************************************************************/

package com.resolve.esb.amqp;

import java.io.IOException;
import java.io.Serializable;
import java.util.HashMap;
import java.util.Hashtable;
import java.util.Iterator;
import java.util.Map;

import com.rabbitmq.client.AMQP.Queue.DeclareOk;
import com.rabbitmq.client.Channel;
import com.resolve.esb.ESBException;
import com.resolve.esb.MMsgContent;
import com.resolve.esb.MMsgDest;
import com.resolve.esb.MMsgHeader;
import com.resolve.esb.MMsgOptions;
import com.resolve.esb.MSender;
import com.resolve.esb.MServer;
import com.resolve.esb.MServerConfig;
import com.resolve.util.Log;
import com.resolve.util.ObjectProperties;
import com.resolve.util.StringUtils;

/**
 * This class is used for sending messages regardless of Topic or Queue. This implements mechanism
 * to recover from connection failure.
 */
public class MAmqpSender extends MSender
{
    private MAmqpServer mAmqpServer;
    private Channel sendChannel;
    MMsgDest sourceDest;

    public MAmqpSender(final MServer mServer, final MServerConfig config) throws ESBException
    {
        super(mServer, config);
        this.mAmqpServer = (MAmqpServer) mServer;
    } // MAmqpSender

    public Channel getSendChannel()
    {
        return sendChannel;
    }

    public void setSendChannel(final Channel sendChannel)
    {
        this.sendChannel = sendChannel;
    }

    public void init() throws ESBException
    {
        super.init();
        mAmqpServer.createPublisher(getSourceDest().getAddress(), getSenderId(), this);
    } // init

    public void close()
    {
        super.close();
        try
        {
            if (sendChannel != null && sendChannel.isOpen())
            {
                sendChannel.close();
                sendChannel = null;
            }
        }
        catch (Exception e)
        {
            Log.log.warn(e.getMessage(), e);
        }
    } // close
    
    @SuppressWarnings("rawtypes")
    @Override
    public boolean sendMessage(final MMsgHeader header, final Object content, final String callback, final boolean internal) throws ESBException
    {
        boolean result = false;

        if (header != null && !header.getDestString().startsWith("null") && !header.getDestString().startsWith("NULL"))
        {
            Object tmpContent = content;

            // allocate empty content if not defined
            if (content == null)
            {
                tmpContent = new Hashtable();
            }

            // get queue/topc name
            String address = header.getDest().getAddress();
            
            this.sourceDest = header.getDest();
            HashMap sendMsg = createMessage(header, tmpContent, callback);
            try
            {
                if (sendMsg != null)
                {
                    if (sendChannel == null || !sendChannel.isOpen())
                    {
                    	Log.log.info("Send channel to the AMQP service not there or is not open, " +
                    				 "will retry to create channel back for 30 secs...");
                        //this loop makes sure if the channel is closed, we retry to open the channel.
                        //MAmqpServer's ConnectionChecker thread keeps trying to open
                        //the connection in every 5 seconds in case it was broken for max 30 secs 
                    	int retryCount = 5;
                        while (retryCount-- >= 0)
                        {
                            try
                            {
                                this.sendChannel = mAmqpServer.createChannel();
                                break;
                            }
                            catch (ESBException e)
                            {
                            	if (Log.log.isDebugEnabled()) {
                            		Log.log.debug(String.format("Attempt #%d to recreate channel to AMQP service failed, " +
                                								"sleeping for 5 secs before retrying...", 
                                								((5 - retryCount) + 1)));
                            	}
                            	
                                this.wait(5000L);
                                //Thread.sleep(5000L);
                            }
                        }
                        
                        if (sendChannel == null || !sendChannel.isOpen()) {
                        	String errMsg = "Failed to re-create channel to AMQP service within 30 sec, " +
            								"AMQP service unavailable";
                        	Log.log.error(errMsg);
                        	throw new Exception(errMsg);
                        }
                    }

                    // check if destination is a topic publication
                    if (mAmqpServer.getPublications().containsKey(address) || 
                        mAmqpServer.getDurablePublications().containsKey(address) ||
                        address.endsWith("_TOPIC"))
                    {
                        if(Log.log.isTraceEnabled())
                        {
                            Log.log.trace("publishing[" + header.getRouteString() + "]");
                        }
                        // send to topic destination
                        result = mAmqpServer.getDriver().publish(getSendChannel(), address, 
                        										 ObjectProperties.serialize(sendMsg));
                        
                        if (Log.log.isDebugEnabled() && !result) {
                        	Log.log.debug(String.format("Failed to publish message to %d", address));
                        }
                    }
                    else
                    {
                        if(Log.log.isTraceEnabled())
                        {
                            Log.log.trace("sending[" + header.getRouteString() + "]");
                        }

                        // send to queue destination
                        result = mAmqpServer.getDriver().send(getSendChannel(), address, 
                        									  ObjectProperties.serialize(sendMsg));
                        
                        if (Log.log.isDebugEnabled() && !result) {
                        	Log.log.debug(String.format("Failed to send message to %d", address));
                        }
                    }
                }
            }
            catch (Exception e)
            {
                Log.log.error(e.getMessage(), e);
                throw new ESBException(e.getMessage(), e);
            }
        }

        return result;
    } // sendMessage

    @SuppressWarnings("rawtypes")
    private HashMap createMessage(final MMsgHeader header, final Object content, final String callback)
    {
        return createObjectMessage(header, content, callback);
    }// createMessage

    @SuppressWarnings({ "rawtypes", "unchecked" })
    private HashMap createObjectMessage(final MMsgHeader header, final Object content, final String callback)
    {
        HashMap result = new HashMap();

        // initialize message header
        createMessageHeader(header, callback, result);

        // set content
        result.put(MMsgContent.MSGCONTENT, (Serializable) content);

        return result;
    }// createObjectMessage

    @SuppressWarnings("rawtypes")
    private HashMap createMessageHeader(final MMsgHeader header, final String callback, final HashMap message)
    {
        return createMessageHeader(header, callback, message, null);
    } // createMessageHeader

    @SuppressWarnings({ "rawtypes", "unchecked" })
    private HashMap createMessageHeader(final MMsgHeader header, final String callback, final HashMap message, final MMsgOptions options)
    {
        super.createCommonMessageHeader(header, callback, options);

        // update message
        for (Iterator i = header.entrySet().iterator(); i.hasNext();)
        {
            Map.Entry entry = (Map.Entry) i.next();
            String name = (String) entry.getKey();
            Object value = entry.getValue();

            if (value != null)
            {
                message.put(name.toUpperCase(), value.toString());
            }
        }

        return message;
    } // createMessageHeader
    
    @SuppressWarnings("rawtypes")
    @Override
    public  boolean sendMessageRepeatedly(final MMsgHeader header, final Object content, final String callback, final boolean internal, long repeatCount) throws ESBException
    {
        boolean result = false;

        if (header != null && !header.getDestString().startsWith("null") && !header.getDestString().startsWith("NULL"))
        {
            Object tmpContent = content;

            // allocate empty content if not defined
            if (content == null)
            {
                tmpContent = new Hashtable();
            }

            // get queue/topc name
            String address = header.getDest().getAddress();
            
            this.sourceDest = header.getDest();
            HashMap sendMsg = createMessage(header, tmpContent, callback);
            try
            {
                if (sendMsg != null)
                {
                    if (sendChannel == null || !sendChannel.isOpen())
                    {
                        //this loop makes sure if the channel is closed, we retry to open the channel.
                        //MAmqpServer's CheckConnection thread keeps trying to open
                        //the connection in every 5 seconds in case it was broken.
                        while (true)
                        {
                            try
                            {
                                this.sendChannel = mAmqpServer.createChannel();
                                break;
                            }
                            catch (ESBException e)
                            {
                                Log.log.warn("Connection to the AMQP service broke, retrying...");
                                this.wait(5000L);
                                //Thread.sleep(5000L);
                            }
                        }
                    }

                    // check if destination is a topic publication
                    if (mAmqpServer.getPublications().containsKey(address) ||
                        mAmqpServer.getDurablePublications().containsKey(address) ||
                        address.endsWith("_TOPIC"))
                    {
                        if(Log.log.isTraceEnabled())
                        {
                            Log.log.trace("publishing[" + header.getRouteString() + "] " + repeatCount + " times");
                        }
                        // send to topic destination
                        mAmqpServer.getDriver().publishRepeatedly(getSendChannel(), address, ObjectProperties.serialize(sendMsg), repeatCount);

                        result = true;
                    }
                    else
                    {
                        if(Log.log.isTraceEnabled())
                        {
                            Log.log.trace("sending[" + header.getRouteString() + "] " + repeatCount + " times");
                        }

                        // send to queue destination
                        mAmqpServer.getDriver().sendRepeatedly(getSendChannel(), address, ObjectProperties.serialize(sendMsg), repeatCount);

                        result = true;
                    }
                }
            }
            catch (Exception e)
            {
                Log.log.error(e.getMessage(), e);
                throw new ESBException(e.getMessage(), e);
            }
        }

        return result;
    } // sendDurableMessage
    
    @Override
    public boolean sendMessage(String queueName, String message) throws ESBException, IOException {
    	boolean retVal = false;
    	// Check if connection is open    	
    	try {
    		mAmqpServer.getConnection(); // Confirms connection is open
    		Channel channel = getSendChannel();
    		
            DeclareOk declareOk = channel.queueDeclare(queueName, true, false, false, null);
            
            if (declareOk != null && declareOk.getQueue().equalsIgnoreCase(queueName)) {
            	channel.basicPublish("", queueName, null, message.getBytes());
            	retVal = true;
            } else {
            	throw new Exception(String.format("Failed to declare queue %s", queueName));
            }
    	} catch (Throwable t) {
    		String errMsg = String.format("Error %sin sending message [%s] to queue %s", 
	  									  (StringUtils.isNotBlank(t.getMessage()) ? "[" + t.getMessage() + "] " : ""),
	  									  message, queueName);
    		
    		throw new ESBException(errMsg, t);
    	}
    	
    	return retVal;
    }
    
} // MAmqpSender

