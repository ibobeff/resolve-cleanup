/******************************************************************************
* (C) Copyright 2014
*
* Resolve Systems, LLC
*
* All rights reserved
* This software is distributed under license and may not be used, copied,
* modified, or distributed without the express written permission of
* Resolve Systems, LLC.
*
******************************************************************************/

package com.resolve.esb;

// import com.resolve.esb.amqp.MAmqpListener;
import java.util.Map;

import com.resolve.esb.amqp.MAmqpServer;
import com.resolve.util.Constants;
import com.resolve.util.Log;

public class MServerFactory
{
	public static MServer getMServer(String hostServiceName, MServerConfig config, Map<String, String> publications, Map<String, String> subscriptions, String defaultClassPackage) throws ESBException {
		return getMServer(hostServiceName, config, publications, subscriptions, defaultClassPackage, false);
	}
	
	public static MServer getMServer(String hostServiceName, MServerConfig config, Map<String, String> publications, Map<String, String> subscriptions, String defaultClassPackage, boolean createSubscriptions) throws ESBException    {
        MServer mServer = null;

        try
        {
            if (Constants.ESB_RABBITMQ.equals(config.product))
            {
            	 mServer = MAmqpServer.getInstance(hostServiceName, config, publications, subscriptions, defaultClassPackage, createSubscriptions);
            }
            //start the server before it's given to the caller.
            if(mServer != null)
            {
                mServer.start();
                mServer.setIsStarted(true);
            }
            else
            {
                throw new ESBException("Could not instantiate a new MServer. This is not good at all!");
            }

            return mServer;
        }
        catch (Exception e)
        {
            Log.log.error(e.getMessage(), e);
            throw new ESBException(e.getMessage(), e);
        }
    }
}
