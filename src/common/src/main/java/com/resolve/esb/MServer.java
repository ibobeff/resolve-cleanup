/******************************************************************************
* (C) Copyright 2014
*
* Resolve Systems, LLC
*
* All rights reserved
* This software is distributed under license and may not be used, copied,
* modified, or distributed without the express written permission of
* Resolve Systems, LLC.
*
******************************************************************************/

package com.resolve.esb;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.concurrent.ConcurrentHashMap;

import com.resolve.rsbase.MainBase;
import com.resolve.util.Constants;
import com.resolve.util.Log;
import com.resolve.util.StringUtils;

/**
 * This is an abstract class that holds common functionalities provided by the protocol specific subclasses.
 *
 */
public abstract class MServer
{
    protected String serviceName;
    protected MServerConfig config;
    protected String defaultClassPackage;
    protected DefaultHandler defaultHandler;
    protected MSender defaultMSender;
    protected MListener defaultMListener;

    //This stores various topics like RSCONTROLS, RSREMOTES etc.
    protected Map<String, String> publications = new HashMap<String, String>();
    protected Map<String, String> subscriptions = new HashMap<String, String>();
    protected Map<String, String> durablePublications = new HashMap<String, String>();
    
    //message handlers
    protected ConcurrentHashMap<String, Class<?>> handlers = new ConcurrentHashMap<String, Class<?>>();
    //topic or queue listeners
    protected Map<String, MListener> listeners = new HashMap<String, MListener>();
    protected Map<String, MListener> publicationListeners = new HashMap<String,MListener>(); 
    protected Map<String, MListener> durableListeners = new HashMap<String,MListener>();
    //topic or queue publishers
    protected Map<String, MSender> publishers = new HashMap<String, MSender>();
    // Durable queues
    protected Set<String> durableQueues = new HashSet<String>();

    private boolean isActive = false;
    private Long startTimestamp;
    public boolean isAMQP = true;
    protected boolean isStarted = false;
    
    public MServer(String serviceName, MServerConfig config, Map<String, String> publications, Map<String, String> subscriptions, String defaultClassPackage) throws ESBException
    {
        this.serviceName = serviceName;
        this.config = config;
        if(publications != null)
        {
            this.publications = publications;
        }
        if(subscriptions != null)
        {
            this.subscriptions = subscriptions;
        }

        init();
    } // MServer

    /**
     * This method creates a subscriber to a Topic. This must register the listener into the
     * publishers collection.
     *
     * @param topicName
     * @param listener
     * @return
     */
    public abstract boolean subscribePublication(String topicName, MListener listener);
    
    public abstract boolean unsubscribePublication(String subscriberId);

    public abstract boolean createTopic(String topicName) throws ESBException;
    
    public abstract boolean createDurableTopic(String topicName) throws ESBException;

    public abstract boolean removePublication(String topicName);

    public abstract boolean createQueue(String queueName) throws ESBException;
    
    public abstract boolean createDurableQueue(String queueName) throws ESBException;

    /**
     * This method creates a listener to a Queue. This must register the listener into the
     * listeners collection.
     *
     * @param queueName
     * @param listener
     */
    public abstract void createQueueConsumer(String queueName, MListener listener);

    public abstract void closeQueueConsumer(String consumerId);

    public abstract boolean createPublisher(String topicName, String publisherId, MSender publisher);

    public abstract boolean removePublisher(String publisherId);

    public abstract MSender createSender() throws ESBException;

    public abstract MListener createListener(String queueName, String defaultClassPackage) throws ESBException;
    public abstract MListener createListener(String queueName, String defaultClassPackage, DefaultHandler handler) throws ESBException;
    public abstract MListener createListener(String queueName, String defaultClassPackage, DefaultHandler handler, DefaultMessageProcessor messageProcessor) throws ESBException;

    public abstract int getMessageCount(String queueName);
    
    public DefaultHandler getDefaultHandler()
    {
        return defaultHandler;
    }

    public void setDefaultHandler(DefaultHandler defaultHandler)
    {
        this.defaultHandler = defaultHandler;
    }

    public MListener getDefaultMListener()
    {
        return defaultMListener;
    }

    public void setDefaultMListener(MListener defaultMListener)
    {
        this.defaultMListener = defaultMListener;
    }

    public void setDefaultMSender(MSender defaultMSender)
    {
        this.defaultMSender = defaultMSender;
    }

    /**
     * Some common initializations, subclasses may override this.
     *
     * @throws ESBException
     */
    public void init() throws ESBException
    {

    }

    public void start() throws ESBException
    {
        try
        {
            //this check is useful because there is not way to block the client to call
            //this multiple times
            if(defaultMSender == null)
            {
                defaultMSender = createSender();
                defaultMSender.init();
            }
            if(defaultMListener == null)
            {
                // add DEFAULT listener
                defaultMListener = createListener(config.getGuid(), defaultClassPackage);
                defaultMListener.init(config.flushQueue);
            }
        }
        catch (Exception e)
        {
            Log.log.error(e.getMessage(), e);
        }
    }

    /**
     * Place common functionalities here. Subclasses may override this method.
     *
     * @param isActive
     * @throws ESBException
     */
    public void start(boolean isActive) throws ESBException
    {
    }

    public void stop() throws ESBException
    {
        setActive(false);
        this.close();
    }

    public void restart() throws ESBException
    {
    }

    public void close()
    {
        try
        {
            if(defaultMSender != null)
            {
                defaultMSender.close();
            }

            if(defaultMListener != null)
            {
                defaultMListener.close();
            }

            for (MSender sender : publishers.values())
            {
                sender.close();
            }
            publishers.clear();

            for (MListener listener : listeners.values())
            {
                listener.close();
            }
            listeners.clear();
        }
        catch (Exception e)
        {
            Log.log.warn("Close connection. " + e.getMessage(), e);
        }
    } // close

    public String getDefaultClassPackage()
    {
        if (StringUtils.isBlank(defaultClassPackage))
        {
            //this will always make sure the default pacage from the Main.java (e.g., com.resolve.racontrol)
            setDefaultClassPackage(MainBase.main.getClass().getPackage().getName());
        }
        return defaultClassPackage;
    }

    public void setDefaultClassPackage(String defaultClassPackage)
    {
        this.defaultClassPackage = defaultClassPackage;
    }

    public boolean subscribePublication(String topicName) throws ESBException
    {
        return subscribePublication(topicName, getDefaultClassPackage());
    } // subscribePublication

    public boolean subscribePublication(String topicName, String defaultClassPackage) throws ESBException
    {
        publications.put(topicName, "");
        MListener mListener = createListener(topicName, defaultClassPackage);
        return subscribePublication(topicName, mListener);
    } // subscribePublication

    public Map<String, String> getPublications()
    {
        return publications;
    }

    public void closeListeners(boolean includeGuid)
    {
        for (MListener listener : listeners.values())
        {
            if (includeGuid || !MainBase.main.configId.getGuid().equals(listener.getReceiveQueueName()))
            {
                listener.close();
            }
        }
    } // closeListeners

    public void initDefaultPublications() throws Exception
    {
        createPublication(Constants.ESB_NAME_BROADCAST);
        createPublication(Constants.ESB_NAME_RSPROCESS);
        createPublication(Constants.ESB_NAME_RSACTIONS);
        createPublication(Constants.ESB_NAME_RSREMOTES);
        createPublication(Constants.ESB_NAME_RSMGMTS);
        createPublication(Constants.ESB_NAME_RSSERVERS);
        createPublication(Constants.ESB_NAME_RSCONTROLS);
        createPublication(Constants.ESB_NAME_RSVIEWS);
        createPublication(Constants.ESB_NAME_RSWIKIS);
        createPublication(Constants.ESB_NAME_RSLICENSES);
        createPublication(Constants.ESB_NAME_EVENT_SOURCING_DCS);
        createPublication(Constants.ESB_NAME_DCS_REPORTINGS);
    } // initDefaultPublications

    public boolean createPublication(String topicName)
    {
        boolean result = false;

        try
        {
            topicName = topicName.toUpperCase();
            Log.log.debug("Initialize publication: " + topicName);
            createTopic(topicName);
            publications.put(topicName, "");
            result = true;
        }
        catch (Exception e)
        {
            Log.log.warn(e.getMessage());
        }

        return result;
    } // createPublication

    public boolean sendMessage(MMsgHeader header, Object content) throws Exception
    {
        return defaultMSender.sendMessage(header, content);
    } // sendMessage

    public boolean sendInternalMessage(MMsgHeader header, Object content, String callback) throws Exception
    {
        return defaultMSender.sendMessage(header, content, callback, true);
    } // sendInternalMessage
    
    public boolean sendMessage(MMsgHeader header, Object content, String callback) throws Exception
    {
        return defaultMSender.sendMessage(header, content, callback, false);
    } // sendMessage
    
    public boolean sendMessageRepeatedly(MMsgHeader header, Object content, String callback, long repeatCount) throws Exception
    {
        return defaultMSender.sendMessageRepeatedly(header, content, callback, false, repeatCount);
    } // sendMessageRepeatedly
    
    public MSender getDefaultMSender()
    {
        return defaultMSender;
    } // getMSender
    
    protected void addPublicationListener(MListener listener)
    {
    	if(listener != null)
    	{
    		publicationListeners.put(listener.getListenerId(), listener);
    	}
    }
    protected void addListener(MListener listener)
    {
        if(listener != null)
        {
            listeners.put(listener.getListenerId(), listener);
        }
    } // addListener

    protected void addPublisher(String name, MSender sender)
    {
        publishers.put(name.toUpperCase(), sender);
    } // addPublisher

    public List<MListener> getListeners()
    {
        List<MListener> result = null;

        result = (List<MListener>) new ArrayList<MListener>(listeners.values());

        return result;
    } // getListeners

    public MListener getListener(String listenerId)
    {
        return listeners.get(listenerId);
    } // getListener

    public MListener getListener()
    {
        return listeners.get("DEFAULT");
    } // getListener

    public void setActive(boolean active)
    {
        this.isActive = active;
        setStartTimestamp(Calendar.getInstance().getTimeInMillis());
    } // setActive

    public boolean isActive()
    {
        return isActive;
    } // getActive

    public Long getStartTimestamp()
    {
        return startTimestamp;
    }

    public void setStartTimestamp(Long startTimestamp)
    {
        this.startTimestamp = startTimestamp;
    }

    public MServerConfig getConfig()
    {
        return this.config;
    } // getConfig

    public String getServiceName()
    {
        return serviceName;
    }

    public void addHandler(String className, Class<?> classObject)
    {
        handlers.put(className, classObject);
    } // addHandler

    public Class<?> getHandler(String className)
    {
        return (Class<?>) handlers.get(className);
    } // getHandler

    public ConcurrentHashMap<String, Class<?>> getHandlers()
    {
        return handlers;
    } // getHandlers

    public Map<String, String> getDurablePublications()
    {
        return durablePublications;
    }
    
    public boolean createDurablePublication(String topicName)
    {
        boolean result = false;

        try
        {
            topicName = topicName.toUpperCase();
            Log.log.debug("Initialize durable publication: " + topicName);
            createDurableTopic(topicName);
            durablePublications.put(topicName, "");
            result = true;
        }
        catch (Exception e)
        {
            Log.log.warn(e.getMessage());
        }

        return result;
    } // createDurablePublication 
    
    public void setIsStarted(boolean isStarted) {
        this.isStarted = isStarted;
    } // setIsStarted

    public boolean isStarted() {
        return isStarted;
    } // isStarted
} // MServer
