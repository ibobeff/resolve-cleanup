/******************************************************************************
* (C) Copyright 2014
*
* Resolve Systems, LLC
*
* All rights reserved
* This software is distributed under license and may not be used, copied,
* modified, or distributed without the express written permission of
* Resolve Systems, LLC.
*
******************************************************************************/

package com.resolve.esb;

import com.resolve.thread.ScheduledExecutor;
import com.resolve.thread.ScheduledTask;
import com.resolve.util.Log;

public class DefaultHandler
{
    protected MSender mSender;
    protected String serviceName;
    protected String defaultClassName; //fully qualified classname of the Mxxxx class.
    
    public DefaultHandler(MSender mSender, String serviceName)
    {
        this.mSender = mSender;
        this.serviceName = serviceName;
    } // DefaultHandler
    
    public DefaultHandler(MSender mSender, String serviceName, String defaultClassName)
    {
        this.mSender = mSender;
        this.serviceName = serviceName;
        this.defaultClassName = defaultClassName;
    } // DefaultHandler
    
	public void execute(MMsgHeader msgHeader, Object obj) 
    {
		try 
        {
            String className = msgHeader.getDest().getClassname();
            String methodName = msgHeader.getDest().getMethodname();
            Class invokeClass = (Class)msgHeader.get("INVOKECLASS");
            
            if(Log.log.isTraceEnabled())
            {
                Log.log.trace("DefaultHandler classMethod: "+className+"."+methodName+" class: "+invokeClass.getName());
            }       
            // executing directly since DefaultHandler was already issued via the Executor
            ScheduledTask task = new ScheduledTask(ScheduledExecutor.getInstance(), "", msgHeader, invokeClass, methodName, obj);
            Object newMsg = task.execute();
            
            // forward results to next destination
            if (newMsg != null && mSender != null)
            {
                if (msgHeader.hasNextRouteDest())
                {
                    msgHeader.nextRouteDest();
                    //MMsgHeader newHeader = new MMsgHeader(msgHeader, className+"."+methodName);
                    MMsgHeader newHeader = new MMsgHeader(msgHeader, serviceName);
                    mSender.sendMessage(newHeader, newMsg);
                }
            }
		} 
        catch (Exception e) 
        {
            Log.log.warn("Failed to execute method handler: "+e.getMessage(), e);
		} 
	} // run

    public String getDefaultClassName()
    {
        return defaultClassName;
    }

    public void setDefaultClassName(String defaultClassName)
    {
        this.defaultClassName = defaultClassName;
    }
    
} // DefaultHandler

