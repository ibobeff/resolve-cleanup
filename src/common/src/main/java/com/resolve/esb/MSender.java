/******************************************************************************
* (C) Copyright 2014
*
* Resolve Systems, LLC
*
* All rights reserved
* This software is distributed under license and may not be used, copied,
* modified, or distributed without the express written permission of
* Resolve Systems, LLC.
*
******************************************************************************/

package com.resolve.esb;

import java.io.IOException;

import com.resolve.util.Guid;

public abstract class MSender
{
    protected static final int SESSION_RETRY = 3;
    protected static final int SESSION_WAIT = 1000;
    protected static final int CACHE_REFRESH_INTERVAL = 300000;
    protected static final int MAX_FILE_TRANSFER_SIZE = 5242880;
    protected long MSG_TTL = 1200000; // 20mins

    private MServer mServer;
    private MMsgDest sourceDest;
    private final String senderId;

    long lastRefresh = System.currentTimeMillis();

    public MSender(MServer mServer, MServerConfig config) throws ESBException
    {
        this.senderId = new Guid().toString();
        this.mServer = mServer;
        setSourceDest(config.guid);
        this.MSG_TTL = config.getMessageTTL();
    } // MSender

    /* Abstract methods */
    /**
     * Publish a text message to the named queue
     * @param queueName - the name of the queue to publish to
     * @param message - the text message
     * @return true if message sent successfully; false otherwise;
     * @throws ESBException
     */
    public abstract boolean sendMessage(String queue, String message) throws ESBException, IOException;
    public abstract boolean sendMessage(MMsgHeader header, Object content, String callback, boolean internal) throws ESBException;
    public abstract boolean sendMessageRepeatedly(MMsgHeader header, Object content, String callback, boolean internal, long repeatCount) throws ESBException;
    
    public String getSenderId()
    {
        return senderId;
    }

    public void init() throws ESBException
    {
    } // init

    public void close()
    {
    } // close

    public MMsgDest getSourceDest()
    {
        return this.sourceDest;
    } // getSourceDest

    public void setSourceDest(String address, String classname, String methodname)
    {
        this.sourceDest = new MMsgDest(address, classname + "." + methodname);
    } // setSourceDest

    public void setSourceDest(String address)
    {
        setSourceDest(address, "MDefault", "unknown");
    } // setSourceDest

    public boolean sendMessage(MMsgHeader header, Object content) throws Exception
    {
        return sendMessage(header, content, null);
    } // sendMessage

    public boolean sendMessageRepeatedly(MMsgHeader header, Object content, long repeatCount) throws Exception
    {
        return sendMessageRepeatedly(header, content, null, repeatCount);
    } // sendMessageRepeatedly
    
    public boolean sendMessage(MMsgHeader header, Object content, String callback) throws Exception
    {
        return sendMessage(header, content, callback, false);
    } // sendMessage
    
    public boolean sendMessageRepeatedly(MMsgHeader header, Object content, String callback, long repeatCount) throws Exception
    {
        return sendMessageRepeatedly(header, content, callback, false, repeatCount);
    } // sendMessageRepeatedly
    
    /**
     * This adds message header that are common to JMS and AMQP.
     *
     * @param header
     * @param callback
     * @param options
     */
    protected void createCommonMessageHeader(MMsgHeader header, String callback, MMsgOptions options)
    {
        // set source caller
        String caller = header.getCaller();
        if (caller == null && callback != null)
        {
            caller = callback;
        }
        header.setSource(new MMsgDest(getSourceDest(), caller));

        // set callback route
        if (callback != null)
        {
            header.addRouteDest(new MMsgDest(getSourceDest(), callback));
        }

        if (options != null)
        {
            header.setOptions(options);
        }
    } // createMessageHeader
} // MSender
