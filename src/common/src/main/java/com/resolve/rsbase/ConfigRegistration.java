/******************************************************************************

(C) Copyright 2006
 
Resolve Systems, Inc.
 Delaware, USA

This software may not be given to any party other than authorised
employees of Resolve Systems, Inc.

******************************************************************************/

package com.resolve.rsbase;

import com.resolve.util.ConfigMap;
import com.resolve.util.XDoc;

public class ConfigRegistration extends ConfigMap
{
    private static final long serialVersionUID = -2154069653748822828L;
	int interval = 5;           // 5 mins
    int expiration = 10080;     // 7 days
    int discard = 43200;        // 30 days
    boolean logHeartbeat = true;
    
    public ConfigRegistration(XDoc config) throws Exception
    {
        super(config);
        
        define("interval", INTEGER, "./REGISTRATION/@INTERVAL");
        define("expiration", INTEGER, "./REGISTRATION/@EXPIRATION");
        define("discard", INTEGER, "./REGISTRATION/@DISCARD");
        define("logHeartbeat", BOOLEAN, "./REGISTRATION/@LOGHEARTBEAT");
    } // ConfigRegistration
    
    public void load()
    {
        loadAttributes();
    } // load

    public void save()
    {
        saveAttributes();
    } // save

    public int getInterval()
    {
        return interval;
    }

    public void setInterval(int interval)
    {
        this.interval = interval;
    }

    public int getExpiration()
    {
        return expiration;
    }

    public void setExpiration(int expiration)
    {
        this.expiration = expiration;
    }

    public int getDiscard()
    {
        return discard;
    }

    public void setDiscard(int discard)
    {
        this.discard = discard;
    }

    public boolean isLogHeartbeat()
    {
        return logHeartbeat;
    }

    public void setLogHeartbeat(boolean logHeartbeat)
    {
        this.logHeartbeat = logHeartbeat;
    }
    
} // ConfigRegistration
