package com.resolve.rsbase;

public interface SessionObjectInterface
{
    // close session
    public abstract void close();

    // MUST call close() in the finalize method
    public abstract void finalize() throws Throwable;

} // SessionObjectInterface
