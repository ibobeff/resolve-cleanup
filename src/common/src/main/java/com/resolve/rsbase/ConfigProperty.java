/******************************************************************************

(C) Copyright 2006
 
Resolve Systems, Inc.
 Delaware, USA

This software may not be given to any party other than authorised
employees of Resolve Systems, Inc.

******************************************************************************/

package com.resolve.rsbase;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.nio.charset.Charset;
import java.nio.charset.StandardCharsets;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Properties;
import java.util.Set;

import com.resolve.util.ConfigMap;
import com.resolve.util.Constants;
import com.resolve.util.CryptUtils;
import com.resolve.util.FileUtils;
import com.resolve.util.Log;
import com.resolve.util.StringUtils;
import com.resolve.util.XDoc;

public class ConfigProperty extends ConfigMap
{
    private static final long serialVersionUID = 613413605862717414L;
	Properties properties;
	Properties sysProperties;
	Properties envProperties;
    String filename;
    
    public ConfigProperty(XDoc config) throws Exception
    {
        super(config);
    } // ConfigProperty
    
    public void load() throws Exception
    {
        properties = new Properties();
        sysProperties = new Properties();
        envProperties = new Properties();
        
        sysProperties.putAll(System.getProperties());
        properties.putAll(sysProperties);
        
        filename = MainBase.main.getProductHome()+"/config/properties.txt";
        File file = FileUtils.getFile(filename);
        if (file.exists())
        {
            FileInputStream fis = new FileInputStream(file);
            properties.load(fis);
            fis.close();
        }
        
        // add environment variables
        Map<String, String> env = System.getenv();
        Set<String> keys = env.keySet();
        keys.forEach(key -> {
            properties.put(key, env.get(key));
            envProperties.put(key, env.get(key));
        });
        
    } // load

    public void save() throws Exception
    {
        //tempProperties.store(new FileOutputStream(file), null);
        File file = FileUtils.getFile(filename);
        if (file.exists()) {
            String ver = MainBase.main.release.version;
            String year = MainBase.main.release.year;
            List<String> lines = new ArrayList<>();
            
            
            lines.add("Resolve_Version=" + ver + " - " + year);
            lines.add("");
            lines.add("################### System Properties ###################");
            lines.add("");
            FileUtils.writeLines(file, lines, false);
            
            sysProperties.store(new FileOutputStream(file, true), null);
            lines.clear();
            lines.add("");
            lines.add("################### Environment Properties ###################");
            lines.add("");
            FileUtils.writeLines(file, lines, true);
            envProperties.store(new FileOutputStream(file, true), null);
        }
    } // save
    
    public Properties getProperties()
    {
        return properties;
    } // getProperties
    
    public String getProperty(String name)
    {
        String result = null;
        String propertyName = name;
        
        try
        {
	        // get property
	        result = (String)properties.get(propertyName);
            
            // decrypt if required
            result = CryptUtils.decrypt(result);
        }
        catch (Exception e)
        {
            Log.log.debug(e.getMessage(), e);
            result = "FAIL: Unable to decrypt property: "+name;
        }
        
        return result;
    } // getProperty
    
    public void setProperty(String name, String value)
    {
        try
        {
	        if (name.startsWith(Constants.PROPERTY_ENCRYPT_PREFIX) && !StringUtils.isEmpty(value))
	        {
	            name = name.substring(Constants.PROPERTY_ENCRYPT_PREFIX.length());
                value = CryptUtils.encrypt(value);
	        }
	                        
	        if (value == null)
	        {
	            value = "";
	        }
	        properties.put(name, value);
        }
        catch (Exception e)
        {
            Log.log.debug(e.getMessage(), e);
        }
    } // setProperty
    
    public void setPropertyNoEncrypt(String name, String value)
    {
        if (value == null)
        {
            value = "";
        }
        properties.put(name, value);
    } // setPropertyNoEncrypt
    
    public void removeProperty(String name)
    {
        properties.remove(name);
    } // removeProperty
    
} // ConfigProperty
