/******************************************************************************

(C) Copyright 2006
 
Resolve Systems, Inc.
 Delaware, USA

This software may not be given to any party other than authorised
employees of Resolve Systems, Inc.

******************************************************************************/

package com.resolve.rsbase;

import java.net.InetAddress;

import com.resolve.util.ConfigMap;
import com.resolve.util.Guid;
import com.resolve.util.Log;
import com.resolve.util.XDoc;
import com.resolve.util.StringUtils;

public class ConfigId extends ConfigMap
{
    private static final long serialVersionUID = -5347038809776201078L;
	public String name = InetAddress.getLocalHost().getHostName().toUpperCase();
    public String description = MainBase.main.release.type;
    public String location = "default";
    public String group = "default";
    public String guid = null;
    public String nodeid = null;
    public String ipaddress = InetAddress.getLocalHost().toString().toUpperCase();
    
    public ConfigId(XDoc config) throws Exception
    {
        super(config);
        
        define("name", STRING, "./ID/@NAME");
        define("description", STRING, "./ID/@DESCRIPTION");
        define("location", STRING, "./ID/@LOCATION");
        define("group", STRING, "./ID/@GROUP");
        define("guid", STRING, "./ID/@GUID");
        define("nodeid", STRING, "./ID/@NODEID");
    } // ConfigId
    
    public void load() throws Exception
    {
        loadAttributes();
        
        if (StringUtils.isEmpty(guid))
        {
            guid = new Guid().toString();
            Log.log.info("Generating new GUID: "+guid);
        }
    } // load

    public void save()
    {
        saveAttributes();
    } // save

    public String getName()
    {
        return name;
    }

    public void setName(String name)
    {
        this.name = name;
    }

    public String getDescription()
    {
        return description;
    }

    public void setDescription(String description)
    {
        this.description = description;
    }

    public String getLocation()
    {
        return location;
    }

    public void setLocation(String location)
    {
        this.location = location;
    }

    public String getGroup()
    {
        return group;
    }

    public void setGroup(String group)
    {
        this.group = group;
    }

    public String getGuid()
    {
        return guid;
    }

    public void setGuid(String guid)
    {
        this.guid = guid;
    }

    public String getIpaddress()
    {
        return ipaddress;
    }

    public void setIpaddress(String ipaddress)
    {
        this.ipaddress = ipaddress;
    }

    public String getNodeid()
    {
        return nodeid;
    }

    public void setNodeid(String nodeid)
    {
        this.nodeid = nodeid;
    }
    
} // ConfigId
