/******************************************************************************

(C) Copyright 2006
 
Resolve Systems, Inc.
 Delaware, USA

This software may not be given to any party other than authorised
employees of Resolve Systems, Inc.

******************************************************************************/

package com.resolve.rsbase;

import com.resolve.util.ConfigMap;
import com.resolve.util.Constants;
import com.resolve.util.LicenseEnum;
import com.resolve.util.XDoc;

public class ConfigGeneral extends ConfigMap {
	private static final long serialVersionUID = -303876417940314768L;
	public String home = "ERROR MISSING INSTALL PATH";
	public String clusterName = "RESOLVE";
	public int configRevisions = 2;
	public String configFileVersion = "1.0";
	public boolean saveConfigOnStart = true;
	public boolean saveConfigOnExit = true;
	public int startupDelay = 0;
	public int threadThreshold = 900;
	public int threadKeepAlive = 30;
	public int worksheetDelayTime = 1 * 5 * 1000;
	boolean perfDebug = false;
	public String mcpMode = Constants.MCP_MODE_LOCAL;
	public String env = LicenseEnum.LICENSE_ENV_NON_PROD.getKeygenName();
	public int scheduledPool = 100;
	private int executorAlarmThreadLimit = 800;
    public boolean logActionResults = true;
	private int scriptPoolAlarmThreadLimit = 800;

	public ConfigGeneral(XDoc config) throws Exception {
		super(config);

		define("home", STRING, "./GENERAL/@HOME");
		define("clusterName", STRING, "./GENERAL/@CLUSTERNAME");
		define("configRevisions", INTEGER, "./GENERAL/@CONFIGREVISIONS");
		define("configFileVersion", STRING, "./GENERAL/@CONFIGFILEVERSION");
		define("saveConfigOnStart", BOOLEAN, "./GENERAL/@SAVECONFIGONSTART");
		define("saveConfigOnExit", BOOLEAN, "./GENERAL/@SAVECONFIGONEXIT");
		define("startupDelay", INTEGER, "./GENERAL/@STARTUPDELAY");
		define("threadThreshold", INTEGER, "./GENERAL/@THREADTHRESHOLD");
		define("threadKeepAlive", INTEGER, "./GENERAL/@THREADKEEPALIVE");
		define("worksheetDelayTime", INTEGER, "./GENERAL/@TWORKSHEETDELAYTIME");
		define("perfDebug", BOOLEAN, "./GENERAL/@PERFDEBUG");
		define("mcpMode", STRING, "./GENERAL/@MCPMODE");
		define("env", STRING, "./GENERAL/@ENVIRONMENT");
		define("scheduledPool", INTEGER, "./GENERAL/@SCHEDULEDPOOL");
		define("executorAlarmThreadLimit", INTEGER, "./GENERAL/TASKEXECUTOR/@EXECUTORALARMTHREADLIMIT");
        define("logActionResults", BOOLEAN, "./GENERAL/@LOGACTIONRESULTS");
        define("scriptPoolAlarmThreadLimit", INTEGER, "./GENERAL/SCRIPTPOOL/@SCRIPTPOOLALARMTHREADLIMIT");
	} // ConfigGeneral

	public void load() {
		loadAttributes();
	} // load

	public void save() {
		saveAttributes();
	} // save

	public String getHome() {
		return home;
	}

	public void setHome(String home) {
		this.home = home;
	}

	public String getClusterName() {
		return clusterName;
	}

	public void setClusterName(String clusterName) {
		this.clusterName = clusterName;
	}

	public String getMcpMode() {
		return mcpMode;
	}

	public void setMcpMode(String mode) {
		this.mcpMode = mode;
	}

	public int getConfigRevisions() {
		return configRevisions;
	}

	public void setConfigRevisions(int configRevisions) {
		this.configRevisions = configRevisions;
	}

	public String getConfigFileVersion() {
		return configFileVersion;
	}

	public void setConfigFileVersion(String configFileVersion) {
		this.configFileVersion = configFileVersion;
	}

	public boolean isSaveConfigOnStart() {
		return saveConfigOnStart;
	}

	public void setSaveConfigOnStart(boolean saveConfigOnStart) {
		this.saveConfigOnStart = saveConfigOnStart;
	}

	public boolean isSaveConfigOnExit() {
		return saveConfigOnExit;
	}

	public void setSaveConfigOnExit(boolean saveConfigOnExit) {
		this.saveConfigOnExit = saveConfigOnExit;
	}

	public int getStartupDelay() {
		return startupDelay;
	}

	public void setStartupDelay(int startupDelay) {
		this.startupDelay = startupDelay;
	}

	public void setThreadThreshold(int threadThreshold) {
		this.threadThreshold = threadThreshold;
	}

	public int getThreadThreshold() {
		return threadThreshold;
	}

	public void setThreadKeepAlive(int threadKeepAlive) {
		this.threadKeepAlive = threadKeepAlive;
	}

	public int getThreadKeepAlive() {
		return threadKeepAlive;
	}

	public void setWorksheetDelayTime(int worksheetDelayTime) {
		this.worksheetDelayTime = worksheetDelayTime;
	}

	public int getWorksheetDelayTime() {
		return this.worksheetDelayTime;
	}

	public boolean isPerfDebug() {
		return perfDebug;
	}

	public void setPerfDebug(boolean b) {
		this.perfDebug = b;
	}

	public String getEnv() {
		return env;
	}

	public void setEnv(String env) {
		this.env = env;
	}

	public int getScheduledPool() {
		return scheduledPool;
	}

	public void setScheduledPool(int scheduledPool) {
		this.scheduledPool = scheduledPool;
	}

  
    public int getExecutorAlarmThreadLimit() {
      return executorAlarmThreadLimit;
    }
  
    public void setExecutorAlarmThreadLimit(int executorAlarmThreadLimit) {
      this.executorAlarmThreadLimit = executorAlarmThreadLimit;
    }

    public boolean isLogActionResults()
    {
        return logActionResults;
    }

    public void setLogActionResults(boolean logActionResults)
    {
        this.logActionResults = logActionResults;
    }
    
	public int getScriptPoolAlarmThreadLimit() {
      return scriptPoolAlarmThreadLimit;
    }
	
	public void setScriptPoolAlarmThreadLimit(int scriptPoolAlarmThreadLimit) {
      this.scriptPoolAlarmThreadLimit = scriptPoolAlarmThreadLimit;
    }
	
} // ConfigGeneral