 /******************************************************************************

 (C) Copyright 2006

 Resolve Systems, Inc.
 Delaware, USA

 This software may not be given to any party other than authorised
 employees of Resolve Systems, Inc.

 ******************************************************************************/

package com.resolve.rsbase;

import java.io.File;
import java.io.IOException;
import java.util.Date;
import java.util.HashMap;
import java.util.Hashtable;
import java.util.Map;
import java.util.concurrent.TimeUnit;

import org.apache.log4j.Level;
import org.codehaus.jackson.JsonGenerationException;
import org.codehaus.jackson.map.JsonMappingException;
import org.codehaus.jackson.map.ObjectMapper;

import com.resolve.esb.ESB;
import com.resolve.esb.ESBException;
import com.resolve.esb.MListener;
import com.resolve.esb.MServer;
import com.resolve.esb.MServerFactory;
import com.resolve.groovy.script.GroovyScript;import com.resolve.esb.amqp.MAmqpServer;import com.resolve.service.License;
import com.resolve.service.LicenseService;
import com.resolve.thread.TaskExecutor;
import com.resolve.thread.ScheduledExecutor;
import com.resolve.util.Constants;
import com.resolve.util.CopyFile;
import com.resolve.util.FileUtils;
import com.resolve.util.GMTDate;
import com.resolve.util.JavaUtils;
import com.resolve.util.LicenseEnum;
import com.resolve.util.Log;
import com.resolve.util.MCPConstants;
import com.resolve.util.MCPLog;
import com.resolve.util.MetricMsg;
import com.resolve.util.XDoc;
import com.resolve.util.restclient.ConfigProxy;

public abstract class MainBase
{
    public static MainBase main;
    public static ESB esb;
    public static MetricInterface metric;
    public static Hashtable locks = new Hashtable();
    public static boolean overrideSaveConfigOnExit = false;
    public static boolean active = false;
    protected String defaultClassPackage;
    public static boolean isUnix = true;
    public static String viewResolveUrl;

    public String configFilename = "config/config.xml";
    public String initFilename = "config/init.cfg";
    public String logConfigFilename = "config/log.cfg";

    public ConfigGeneral configGeneral;
    public ConfigId configId;
    public ConfigProperty configProperty;
    public ConfigRegistration configRegistration;
    public ConfigESB configESB;
    public ConfigProxy configProxy;
    
    private ConfigKeyService configKeyService;


    public File configFile;
    public File initFile;
    public XDoc configDoc;
    public Release release;
    public MServer mServer;

    public boolean isShutdown = false;
    public boolean isLicenseMaster = false;

    protected LicenseService licenseService;
//    protected License license;

    //stores cluster based properties (e.g., clustername.clustermode=active)
    private Map<String, String> clusterProperties = new HashMap<String, String>();
    private String clusterModeProperty = null;
    private boolean clusterModeActive = false;

    public MainBase()
    {
        if (main == null)
        {
	        main = this;
        }

        String osname = System.getProperty("os.name").toLowerCase();
        if (osname.indexOf("win") >= 0)
        {
            isUnix = false;
        }

    } // Main

    public LicenseService getLicenseService()
    {
        return licenseService;
    }

    public void setLicenseService(LicenseService licenseService)
    {
        this.licenseService = licenseService;
    }

    abstract public void init(String serviceName) throws Exception;

    public void start() throws Exception
    {
    } // start

    protected void initThreadPools() {
        ScheduledExecutor.init(configGeneral.getScheduledPool());
        TaskExecutor.init();
    }
    
    protected void initLicenseService() throws ESBException
    {
        loadLicense();

        // request licenses from other components
        Map<String, String> params = new HashMap<String, String>();
        params.put(LicenseEnum.RETURN_QUEUE.getKeygenName(), configId.getGuid());
        Log.log.debug("Requesting licenses from other components.");
        if (MainBase.esb.sendInternalMessage(Constants.ESB_NAME_RSLICENSES, "MLicense.sendLicense", params) == false)
        {
            Log.log.warn("Failed to request licenses from other components, failed request " + 
            			 params.get(LicenseEnum.RETURN_QUEUE.getKeygenName()));
        }
    }

    /**
     * Loads the license if available.
     *
     * @return
     */
    protected void loadLicense()
    {
        licenseService = new LicenseService(configId.getGuid(), MainBase.main.getProductHome() + "/log/tm");
        licenseService.start();

        License license = licenseService.getCurrentLicense();
        if (license != null)
        {
            Log.log.debug("****** LICENSE ******");
            Log.log.debug(license.toString());
            Log.log.debug("****** LICENSE ******");
            // add more common logic here.
        }
    }

    protected void initRelease(Release release)
    {
        this.release = release;
    } // initRelease

    protected void initLog() throws Exception
    {
        // init logging
        Log.init(release.serviceName+"/"+logConfigFilename, release.type.toLowerCase());
        Log.log.warn("*** STARTING "+release.name.toUpperCase()+" ***");
        Log.log.warn("Version:     "+release.version);
        Log.log.warn("ServiceName: "+release.serviceName);
        Log.log.warn("HostName:    "+release.hostName);

        // stdout
        if (!release.name.toUpperCase().equals("RSCONSOLE") && !release.name.toUpperCase().equals("RSMGMT"))
        {
	        System.out.println();
	        System.out.println("*** STARTING "+release.name.toUpperCase()+" *** ("+new Date()+")");
	        System.out.println("Version:     "+release.version);
	        System.out.println("ServiceName: "+release.serviceName);
	        System.out.println("HostName:    "+release.hostName);
        }

        // init log component / serviceName
        Log.initComponent(release.serviceName);
    } // initLog

    protected void initConfigFile() throws Exception {
      Log.log.info("Loading configuration settings");
      configFile = FileUtils.getFile(release.serviceName+"/"+configFilename);
      configDoc = new XDoc(configFile);
    }

    protected void initConfig() throws Exception
    {
        if (configDoc == null)
        {
            initConfigFile();
        }
		loadConfig(configDoc);

        Log.log.info("Config version: "+configGeneral.configFileVersion);
        Log.log.info("  name: "+configId.name);
        Log.log.info("  id:   "+configId.guid);
        
        //reassign the component with the guid.
        String serviceName = release.getServiceName();
        String id = configId.getGuid();
        String name = configId.getName();
        String org = configDoc.getStringValue("./GENERAL/@ORG", "").replaceAll("[^0-9A-Za-z_]", "");
        Log.putCurrentNamedContext(Constants.GUID, id);
        Log.putCurrentNamedContext(Constants.GW_CONTAINER_NAME, name);
        Log.putCurrentNamedContext(Constants.GW_CONTAINER_ORG, org);
        

        Log.initComponent(serviceName + "/" + id);
    } // initConfig

    protected void initStartup()
    {
        // startup delay
        if (configGeneral.getStartupDelay() > 0)
        {
            try
            {
                Log.log.info("Startup delay: "+configGeneral.getStartupDelay()+" mins");
                Thread.sleep(configGeneral.getStartupDelay() * 60 * 1000);
                Log.log.info("Startup delay completed");
            }
            catch (Exception e) { } // ignore
        }
    } // initStartup

    protected void initTimezone()
    {
        Log.log.info("Initialize Timezone");
        //change to hard code UTC since other time zones will cause problems
        GMTDate.initTimezone("UTC");
    } // initTimezone

	protected void initESB() throws Exception {
		initESB(false);
	}

    protected void initESB(boolean createSubscriptions) throws Exception
    {
        // init esb
        if (mServer == null)
        {
            defaultClassPackage = "com.resolve."+release.name.toLowerCase();
            mServer = MServerFactory.getMServer(release.getHostServiceName(), configESB.getConfig(), null, configESB.getSubscriptions(), defaultClassPackage, createSubscriptions);
        }
        
		esb = new ESB(mServer, release.hostName+"."+release.serviceName);

		// init esb in com.resolve.util.Log
		Log.initESB(esb);
    } // initESB

    protected void startESB() throws Exception
    {
        Log.log.info("Starting Message Bus");
        mServer.setDefaultClassPackage(defaultClassPackage);
        mServer.start();

        // add default publications
        mServer.initDefaultPublications();
        
        // License service
        MListener listener = mServer.createListener(Constants.ESB_NAME_RSLICENSE, defaultClassPackage);
        listener.init(false);

        //every component listens to the RSLICENSES topic.
        mServer.subscribePublication(Constants.ESB_NAME_RSLICENSES);
        
        // Every component listens to RSMQFTP_<instance guid> queue
        /*
         * Note: Package name cannot be set with following 
         * 		 MRSMQFTPReceiver.class.getPackage().getName()
         * 		 as it will introduce cyclic dependency between common
         * 		 and services.
         * 
         *       Moving rsqmqftp to common at current juncture seems to 
         *       be out of place.
         */
        MListener rsmqFTPListener = mServer.createListener(String.format("RSMQFTP_%s", configId.guid.toUpperCase()),
        												   "com.resolve.rsmqftp");
        rsmqFTPListener.init(false);
    } // startESB

    protected void closeESB() throws Exception
    {
        if (mServer != null)
        {
            mServer.close();
            mServer = null;
        }
    } // closeESB

    protected void initSaveConfig() throws Exception
    {
        if (configGeneral.saveConfigOnStart)
        {
            Log.log.info("Saving configuration");

            saveConfig();

            // copy config.xml to config.bak
            Log.log.info("Backup configuration");
            CopyFile.setRevisions(configGeneral.configRevisions);
            CopyFile.backup(configFile.getAbsolutePath());

            // save configuration to file
            configDoc.toPrettyFile(configFile);
            Log.log.info("Saved configuration");
        }
    } // initSaveConfig

    protected void initComplete() throws Exception
    {
        initServiceComplete();

        // log to indicate the component is started.
        MCPLog mcpLog = new MCPLog(configGeneral.getHome() + "/" +  release.serviceName + "/log/" + MCPConstants.MCP_COMPONENT_START_LOGFILE);
        if(mcpLog != null)
        {
            //mcpLog.write(release.serviceName.toUpperCase() + "#" + configId.getGuid(), "STARTED", release.serviceName.toUpperCase() + "#" + configId.getGuid() + " is started");
            mcpLog.write(MCPConstants.MCP_FIELD_KEY_START, MCPConstants.MCP_FIELD_VALUE_SUCCESS, release.serviceName.toUpperCase() + "#" + configId.getGuid());
        }
    } // initComplete

    protected void initServiceComplete() throws Exception
    {
        // display shutdown hooks
        if (Log.log.getLevel() == Level.DEBUG)
        {
            Log.log.debug(JavaUtils.getShutdownHooks());
        }

        MainBase.setActive(true);
        if(mServer != null)
        {
            mServer.setActive(true);
        }
        Log.log.warn(release.name.toUpperCase()+" started ...");
        System.out.println(release.name.toUpperCase()+" started ...");
    }
    
    public void exitSaveConfig()
    {
        // saving configuration // saving configuration
    	if (!overrideSaveConfigOnExit && configGeneral.saveConfigOnExit == true)
    	{
            try
            {
                Log.log.info("Saving configuration");

                // save config
                saveConfig();

                // copy config.xml to config.bak
                Log.log.info("Backup configuration");
                CopyFile.setRevisions(configGeneral.configRevisions);
                CopyFile.backup(configFile.getAbsolutePath());

                // save configuration to file
                configDoc.toPrettyFile(configFile);
                Log.log.info("Saved configuration");
            }
            catch (Exception e)
            {
                Log.log.error("Unable to save configuration file: "+e.getMessage(), e);
            }

        }
    } // exitSaveConfig


    protected void initShutdownHandler()
    {
        Runtime.getRuntime().addShutdownHook(
                new java.lang.Thread()
                {
                    @Override
                    public void run()
                    {
                        terminate();
                    }
                }
            );

    } // initShutdownHandler

    protected void terminate()
    {
        try
        {
            terminateService();

            // log to indicate the component is stopped.
            MCPLog mcpLog = new MCPLog(configGeneral.getHome() + "/" +  release.serviceName + "/log/" + MCPConstants.MCP_COMPONENT_STOP_LOGFILE);
            if(mcpLog != null)
            {
                mcpLog.write(MCPConstants.MCP_FIELD_KEY_STOP, MCPConstants.MCP_FIELD_VALUE_SUCCESS, release.serviceName.toUpperCase() + "#" + configId.getGuid());
            }
        }
        catch (Exception e)
        {
            Log.log.error("Failed to terminate component: "+e.getMessage(), e);
        }
    } // terminate

    protected void terminateService()
    {
        try
        {
            // this should already be set by the sub-class terminate()
            isShutdown = true;

            // stopping config
            stopConfig();

            // stopping message server
            if (mServer != null)
            {
                Log.log.info("Stopping Message Bus");
                closeESB();
            }

            // stopping resolve executor
            Log.log.info("Stopping Executor Services");
            ScheduledExecutor.getInstance().shutdown();
            TaskExecutor.getCachedThreadPool().shutdown();
        }
        catch (Exception e)
        {
            Log.log.error("Failed to terminate component: "+e.getMessage(), e);
        }
    } // terminate
    
    /*
     * NOTE: String args[] is required to catch NT service shutdown message
     */
    abstract protected void exit(String[] argv);

    protected void loadConfig(XDoc configDoc) throws Exception
    {
        if (configGeneral == null)
        {
            configGeneral = new ConfigGeneral(configDoc);
            configGeneral.load();
            clusterModeProperty = configGeneral.getClusterName() + "." + Constants.CLUSTER_MODE;
        }
        if (configId == null)
        {
            configId = new ConfigId(configDoc);
            configId.load();
        }
        if (configProperty == null)
        {
            configProperty = new ConfigProperty(configDoc);
            configProperty.load();
        }
        if (configRegistration == null)
        {
            configRegistration = new ConfigRegistration(configDoc);
            configRegistration.load();
        }
        if (configESB == null)
        {
            loadESB(configDoc);
        }        
        if (configProxy == null)
        {
            loadProxy(configDoc);
        }
        
        MetricMsg.stcomponent = release.getName();
        MetricMsg.mcpMode = configGeneral.getMcpMode();
    } // loadConfig
    
    // give subclass a change to overwrite it
    protected void loadESB(XDoc configDoc) throws Exception
    {
        configESB = new ConfigESB(configDoc);
        configESB.load();
    } // loadESB
    
    // give subclass a change to overwrite it
    protected void loadProxy(XDoc configDoc) throws Exception
    {
        configProxy = new ConfigProxy(configDoc);
        configProxy.load();
    } // loadProxy
    
    /**
     * This method simply calls the saveConfig().
     *
     * @throws Exception
     */
    public void callSaveConfig() throws Exception
    {
        Log.log.debug("Saving configurations.");
        saveConfig();
        //saves the config.xml
        configDoc.toPrettyFile(configFile);
    }

    protected void saveConfig() throws Exception
    {
        if (configGeneral != null)
        {
            configGeneral.save();
        }
        if (configId != null)
        {
            configId.save();
        }
        if (configProperty != null)
        {
            configProperty.save();
        }
        if (configRegistration != null)
        {
            configRegistration.save();
        }
        if (configESB != null)
        {
            configESB.save();
        }
        if (configProxy != null)
        {
            configProxy.save();
        }
        if (configKeyService != null)
        {
	        configKeyService.save();
        }
    } // saveConfig

    protected void startConfig() throws Exception
    {
        if (configGeneral != null)
        {
            configGeneral.start();
        }
        if (configId != null)
        {
            configId.start();
        }
        if (configRegistration != null)
        {
            configRegistration.start();
        }
        if (configESB != null)
        {
            configESB.start();
        }
        if (configProxy != null)
        {
            configProxy.start();
        }
    } // startConfig

    protected void stopConfig() throws Exception
    {
        if (configGeneral != null)
        {
            configGeneral.stop();
        }
        if (configId != null)
        {
            configId.stop();
        }
        if (configRegistration != null)
        {
            configRegistration.stop();
        }
        if (configESB != null)
        {
            configESB.stop();
        }
        if (configProxy != null)
        {
            configProxy.stop();
        }
    } // stopConfig

    public String getResolveHome()
    {
        return configGeneral.home;
    } // getResolveHome

    public String getProductHome()
    {
        return configGeneral.home+"/"+release.serviceName;
    } // getProductHome

    public String getGSEHome()
    {
        return configGeneral.home+"/"+release.serviceName+"/service";
    } // getGSEHome

    public static ESB getESB()
    {
        return esb;
    }//getESB

    public ConfigKeyService getConfigKeyService() {
		return this.configKeyService;
	}

    public static void sendMetric(Object msg)
    {
    	esb.sendInternalMessage(Constants.ESB_NAME_METRIC, "MMetric.insert", msg);
    } // sendMetric

    public static boolean isActive()
    {
        return active;
    } // isActive

    public static void setActive(boolean active)
    {
        MainBase.active = active;
    } // setActive

    public static boolean isUnix()
    {
        return isUnix;
    } // isUnix
    
    protected void initMetric() throws Exception
    {
        // request changes in the threshold every 30 minutes. Make is configurable later
    	ScheduledExecutor.getInstance().executeRepeat(MetricUtil.class, "requestMetricThresholdUpdate", 30, TimeUnit.MINUTES);
    } // initMetric

    /**
     * Make sure RSMGMT is running.
     */
    public void requestMetricThresholdUpdate()
    {
        //delay the execution, make it configurable later.
        Log.log.debug("Sending request to update metric threshold properties");
        Map<String, String> content = new HashMap<String, String>();
        content.put("GUID", MainBase.main.configId.getGuid());
        esb.sendInternalMessage(Constants.ESB_NAME_METRIC, "MMetric.updateMetricThresholdProperties", content);
    } // requestThresholdUpdate
    
    public static boolean isAMQPServer()
    {
        return main.mServer.isAMQP;
    }
    
    //provides the tmp folder inside resolve home
    public static String getTmpDir()
    {
        return MainBase.main.configGeneral.home + "/tmp";
    }

    public String getClusterModeProperty()
    {
        return clusterModeProperty;
    }

    public void setClusterModeProperty(String clusterModeProperty)
    {
        this.clusterModeProperty = clusterModeProperty;
    }

    protected void initClusterProperties()
    {
        while(true)
        {
            try
            {
                Map<String, String> response = MainBase.getESB().call("RSCONTROL", "MAction.getClusterProperties", null, 10000L);
                if(response == null)
                {
                    Log.log.info("None of the RSCONTROLs are running to receive cluster property synchronization message, retry.");
                }
                else
                {
                    if(Log.log.isDebugEnabled())
                    {
                        Log.log.debug("Cluster properties received from RSControl:");
                        for(String key : response.keySet())
                        {
                            Log.log.debug("\tkey : " + key + ", value : " + response.get(key));
                        }
                    }
                    setClusterProperties(response);
                    break;
                }
                Thread.sleep(10000L);
            }
            catch (InterruptedException e)
            {
                Log.log.debug(e.getMessage(), e);
            }
            catch (Exception e)
            {
                Log.log.error(e.getMessage(), e);
            }
        }
    }

    public Map<String, String> getClusterProperties()
    {
        return clusterProperties;
    }

    public void setClusterProperties(Map<String, String> clusterProperties)
    {
        Log.log.info("setClusterProperties: clusterProperties = " + clusterProperties);
        Log.log.info("clusterModeProperty = " + clusterModeProperty);
        this.clusterProperties = clusterProperties;
        for(String key : clusterProperties.keySet())
        {
            if(key.equalsIgnoreCase(clusterModeProperty))
            {
                String value = clusterProperties.get(key);
                if(Constants.CLUSTER_MODE_ACTIVE.equalsIgnoreCase(value))
                {
                    activateCluster();
                }
                else
                {
                    deactivateCluster();
                }
            }
        }
    }
    
    protected void activateCluster()
    {
        Log.log.info("Resolve cluster : " + configGeneral.getClusterName() + ", Cluster mode : active");

        clusterModeActive = true;
    }
    
    protected void deactivateCluster()
    {
        Log.log.info("Resolve cluster : " + configGeneral.getClusterName() + ", Cluster mode : inactive");

        clusterModeActive = false;        
    }

    public boolean isClusterModeActive()
    {
        return clusterModeActive;
    }
    
	protected void initENC() throws Exception {
		initConfigFile();
        configKeyService = new ConfigKeyService(configDoc);
        configKeyService.load();
	}
	
	protected boolean registerDCSClient(String username, String password) throws JsonGenerationException, JsonMappingException, IOException {
		Map<String, String> registrationProps = new HashMap<>();
		registrationProps.put("username", username);
		registrationProps.put("password", password);

		ObjectMapper mapper = new ObjectMapper();
		String payload = mapper.writeValueAsString(registrationProps);
		
		return getESB().sendInternalMessage(Constants.ESB_NAME_CLIENT_REGISTRATION, "", payload);
	}
} // MainBase