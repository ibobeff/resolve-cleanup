/******************************************************************************

(C) Copyright 2006

Resolve Systems, Inc.
 Delaware, USA

This software may not be given to any party other than authorised
employees of Resolve Systems, Inc.

******************************************************************************/

package com.resolve.rsbase;

import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Hashtable;

import com.resolve.esb.MServerConfig;
import com.resolve.util.ConfigMap;
import com.resolve.util.StringUtils;
import com.resolve.util.XDoc;
import com.resolve.util.Constants;

public class ConfigESB extends ConfigMap
{
    private static final long serialVersionUID = 9105502285204820474L;

	public MServerConfig config;

    public List subscriptions;
    public List publications;


    public ConfigESB(XDoc config) throws Exception
    {
        super(config);
    } // ConfigESB

    public void load() throws Exception
    {
        //this.config = new MServerConfig(xdoc.getStringValue("RABBITMQ", Constants.ESB_RABBITMQ), MainBase.main.configId.guid, MainBase.main.configId.name);
        //String product = xdoc.getStringValue("./ESB/@PRODUCT");
        this.config = new MServerConfig(xdoc.getStringValue("./ESB/@PRODUCT", null), MainBase.main.configId.guid, MainBase.main.configId.name);

        config.setBrokerAddr(xdoc.getStringValue("./ESB/@BROKERADDR", config.getBrokerAddr()));
        config.setMaxExecutionEvents(xdoc.getIntValue("./ESB/@MAXEXECUTIONEVENTS", config.getMaxExecutionEvents()));
        config.setBrokerAddr2(xdoc.getStringValue("./ESB/@BROKERADDR2", config.getBrokerAddr2()));
        config.setUsername(xdoc.getStringValue("./ESB/@USERNAME", config.getUsername()));
        config.setP_assword(xdoc.getSecureStringValue("./ESB/@PASSWORD", config.getP_assword()));
        config.setCreateQueueRetry(xdoc.getIntValue("./ESB/@CREATEQUEUERETRY", config.getCreateQueueRetry()));
        config.setCreateQueueWait(xdoc.getIntValue("./ESB/@CREATEQUEUEWAIT", config.getCreateQueueWait()));
        config.setKeepalive(xdoc.getIntValue("./ESB/@KEEPALIVE", config.getKeepalive()));
        config.setMaxFailed(xdoc.getIntValue("./ESB/@MAXFAILED", config.getMaxFailed()));
        config.setCacheSize(xdoc.getIntValue("./ESB/@CACHESIZE", config.getCacheSize()));
        config.setMaxMessages(xdoc.getIntValue("./ESB/@MAXMESSAGES", config.getMaxMessages()));
        config.setFlushQueue(xdoc.getBooleanValue("./ESB/@FLUSHQUEUE", config.isFlushQueue()));
        config.setMessageTTL(xdoc.getIntValue("./ESB/@MESSAGETTL", config.getMessageTTL()));
        config.setEventQueueLimit(xdoc.getIntValue("./ESB/@EVENTQUEUELIMIT", config.getEventQueueLimit()));
        config.setSslProtocol(xdoc.getStringValue("./ESB/@SSLPROTOCOL",config.getSslProtocol()));
        config.setConnectionTimeout(xdoc.getIntValue("./ESB/@CONNECTIONTIMEOUT",config.getConnectionTimeout()));
        // subscription
        subscriptions = xdoc.getListMapValue("./ESB/SUBSCRIPTION", null);
        publications = xdoc.getListMapValue("./ESB/PUBLICATION", null);
    } // load

    public void save() throws Exception
    {
        xdoc.setStringValue("./ESB/@PRODUCT", config.getProduct());
        xdoc.setStringValue("./ESB/@BROKERADDR", config.getBrokerAddr());
        xdoc.setStringValue("./ESB/@BROKERADDR2", config.getBrokerAddr2());
        xdoc.setStringValue("./ESB/@USERNAME", config.getUsername());
        xdoc.setSecureStringValue("./ESB/@PASSWORD", config.getP_assword());
        xdoc.setIntValue("./ESB/@CREATEQUEUERETRY", config.getCreateQueueRetry());
        xdoc.setIntValue("./ESB/@CREATEQUEUEWAIT", config.getCreateQueueWait());
        xdoc.setIntValue("./ESB/@KEEPALIVE", config.getKeepalive());
        xdoc.setIntValue("./ESB/@MAXFAILED", config.getMaxFailed());
        xdoc.setIntValue("./ESB/@CACHESIZE", config.getCacheSize());
        xdoc.setIntValue("./ESB/@MAXMESSAGES", config.getMaxMessages());
        xdoc.getBooleanValue("./ESB/@FLUSHQUEUE", config.isFlushQueue());
        xdoc.setIntValue("./ESB/@MESSAGETTL", config.getMessageTTL());
        xdoc.setIntValue("./ESB/@EVENTQUEUELIMIT", config.getEventQueueLimit());
        xdoc.setStringValue("./ESB/@SSLPROTOCOL", config.getSslProtocol());
        xdoc.setIntValue("./ESB/@CONNECTIONTIMEOUT",config.getConnectionTimeout());
        xdoc.setListMapValue("./ESB/SUBSCRIPTION", subscriptions);
        xdoc.setListMapValue("./ESB/PUBLICATION", publications);
    } // save

    public String toString()
    {
        String result = "";

        result += "ConfigESB:\n";
        result += "  Subscriptions:\n";
        for (Iterator i=subscriptions.iterator(); i.hasNext(); )
        {
            Map entry = (Map)i.next();
            String group = (String)entry.get("NAME");
            result += "    group: "+group+"\n";
        }
        return result;
    } // toString

    public MServerConfig getConfig()
    {
        return config;
    } // getConfig

    public Hashtable getSubscriptions()
    {
        Hashtable result = new Hashtable();

        for (Iterator i=subscriptions.iterator(); i.hasNext(); )
        {
            Map entry = (Map)i.next();
            String group = (String)entry.get("NAME");
            result.put(group.toUpperCase(), "");
        }

        // add default subscriptions
        result.put(Constants.ESB_NAME_BROADCAST, "");

        return result;
    } // getSubscriptions

    public boolean subscribe(String group) throws Exception
    {
        boolean result = false;

        // check if entry already exists
        boolean found = false;
        for (Iterator i=subscriptions.iterator(); !found && i.hasNext(); )
        {
            Map entry = (Map)i.next();
            if (group.equalsIgnoreCase((String)entry.get("NAME")))
            {
                found = true;
            }
        }

        // add only if not exists
        if (!found)
        {
            Hashtable entry = new Hashtable();
            entry.put("NAME", group.toUpperCase());
            subscriptions.add(entry);

            result = MainBase.main.mServer.subscribePublication(group);
        }

        return result;
    } // subscribe

    public boolean unsubscribe(String group) throws Exception
    {
        MainBase.main.mServer.unsubscribePublication(group);
        removeSubscriptionDefinition(group);

        return true;
    } // unsubscribe

    boolean removeSubscriptionDefinition(String name)
    {
        boolean found = false;

        for (Iterator i=subscriptions.iterator(); !found && i.hasNext(); )
        {
            Map entry = (Map)i.next();
            if (name.equalsIgnoreCase((String)entry.get("NAME")))
            {
                i.remove();
                found = true;
            }
        }

        return found;
    } // removeSubscriptionDefinition

    public boolean setUserPass(String olduser, String oldpass, String newuser, String newpass)
    {
        boolean result = false;

        if (olduser.equals(config.getUsername()) && oldpass.equals(config.getP_assword()))
        {
            config.setUsername(newuser);
            config.setP_assword(newpass);
            result = true;
        }
        return result;
    } // setUserPass

    public boolean checkUserPass(String olduser, String oldpass)
    {
        boolean result = false;

        if (olduser.equals(config.getUsername()) && oldpass.equals(config.getP_assword()))
        {
            result = true;
        }
        return result;
    } // checkUserPass

    public boolean setAddress(String brokerAddr, String brokerPort)
    {
        boolean result = false;

        if (!StringUtils.isEmpty(brokerAddr))
        {
            if (StringUtils.isEmpty(brokerPort))
            {
                brokerPort = "4004";
            }
            config.setBrokerAddr(brokerAddr+":"+brokerPort);
            result = true;
        }
        return result;
    } // setAddress

    public boolean setAddress2(String brokerAddr2, String brokerPort2)
    {
        boolean result = false;

        if (!StringUtils.isEmpty(brokerAddr2))
        {
            if (StringUtils.isEmpty(brokerPort2))
            {
                brokerPort2 = "4004";
            }
            config.setBrokerAddr2(brokerAddr2+":"+brokerPort2);
            result = true;
        }
        else
        {
            config.setBrokerAddr2("");
        }
        return result;
    } // setAddress2

} // ConfigESB
