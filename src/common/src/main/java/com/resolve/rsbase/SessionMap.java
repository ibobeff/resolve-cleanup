package com.resolve.rsbase;

import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

import com.resolve.util.Constants;
import com.resolve.util.Log;
import com.resolve.util.StringUtils;

public class SessionMap extends ConcurrentHashMap<String, SessionObjectInterface>
{
    private static final long serialVersionUID = 3873162622769391297L;
	String processId;
    long expiry;
    
    public SessionMap(String processId, int processTimeout)
    {
        this.processId = processId;
        this.expiry = System.currentTimeMillis() + (processTimeout * 1000);
    } // SessionMap
    
    public String getProcessId()
    {
        return processId;
    } // getProcessId
    
    public void setProcessId(String processId)
    {
        this.processId = processId;
    } // setProcessId
    
    public long getExpiry()
    {
        return expiry;
    } // getExpiry
    
    public void setExpiry(long expiry)
    {
        this.expiry = expiry;
    } // setExpiry
    
    public void clear()
    {
        try
        {
            for (SessionObjectInterface obj : this.values())
            {
                obj.close();
            }
            
            super.clear();
        }
        catch (Throwable e)
        {
            Log.log.debug("Remove session error: "+e.getMessage());
        }
    } // removeAll
    
    protected void finalize() throws Throwable
    {
      clear();
        
      super.finalize();
    } // finalize

} // SessionMap
