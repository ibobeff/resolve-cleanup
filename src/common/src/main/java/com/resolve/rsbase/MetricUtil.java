/******************************************************************************
* (C) Copyright 2014
*
* Resolve Systems, LLC
*
* All rights reserved
* This software is distributed under license and may not be used, copied,
* modified, or distributed without the express written permission of
* Resolve Systems, LLC.
*
******************************************************************************/

package com.resolve.rsbase;

import java.util.HashMap;
import java.util.Map;

import com.resolve.util.Constants;
import com.resolve.util.Log;

/**
 * Utility for Metric
 */
public class MetricUtil
{
    /**
     * Make sure RSMGMT is running because "METRIC" queue is only listen by RSMGMT.
     */
    public static void requestMetricThresholdUpdate()
    {
        //delay the execution, make it configurable later.
        //int delay = 30;
        Log.log.debug("Sending request to update metric threshold properties");
        Map<String, String> content = new HashMap<String, String>();
        content.put("GUID", MainBase.main.configId.getGuid());
        MainBase.esb.sendInternalMessage(Constants.ESB_NAME_METRIC, "MMetric.updateMetricThresholdProperties", content);
    } // requestThresholdUpdate
}
