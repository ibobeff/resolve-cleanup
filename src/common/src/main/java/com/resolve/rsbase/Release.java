package com.resolve.rsbase;

import java.net.InetAddress;

public class Release
{
    public String name;
    public String type;
    public String version;
    public String serviceName;
    public String hostName;
    public String copyright;
    public String year;
    
    public Release()
    {
        try
        {
            this.copyright = "(C) Copyright 2015 Resolve Systems, LLC";
	        this.hostName = InetAddress.getLocalHost().getHostName().toUpperCase();
        }
        catch (Exception e) { }
        
    } // Release
    
    public String toString()
    {
        String result = "";
        
        result += "name:        "+name+"\n";
        result += "type:        "+type+"\n";
        result += "version:     "+version+"\n";
        result += "serviceName: "+serviceName+"\n";
        result += "hostName:    "+hostName+"\n";
        result += "copyright:   "+copyright+"\n";
        result += "year:        "+year+"\n";
        
        return result;
    } // toString

    public String getCopyright()
    {
        return copyright;
    }

    public void setCopyright(String copyright)
    {
        this.copyright = copyright;
    }

    public String getName()
    {
        return name;
    }

    public void setName(String name)
    {
        this.name = name;
    }

    public String getServiceName()
    {
        return serviceName;
    }

    public void setServiceName(String serviceName)
    {
        this.serviceName = serviceName;
    }

    public String getType()
    {
        return type;
    }

    public void setType(String type)
    {
        this.type = type;
    }

    public String getVersion()
    {
        return version;
    }

    public void setVersion(String version)
    {
        this.version = version;
    }

    public String getHostName()
    {
        return hostName;
    }

    public void setHostName(String hostName)
    {
        this.hostName = hostName;
    }
    
    public String getHostServiceName()
    {
        return this.hostName+"."+this.serviceName;
    }
    
    public String getYear()
    {
        return year;
    }
    
    public void setYear(String year)
    {
        this.year = year;
    }
} // Release
