package com.resolve.rsbase;

import com.resolve.util.ConfigMap;
import com.resolve.util.XDoc;

public class ConfigKeyService extends ConfigMap {
	
	private static final long serialVersionUID = -7897997617938700962L;
    private String host = "localhost";
    private int port = 9080;
    private String protocol = "http";
    private String username = "";
    private String pwd = "";

	public ConfigKeyService(XDoc config) throws Exception {
		super(config);

		define("host", STRING, "./KEYSERVICE/@HOST");
		define("port", INTEGER, "./KEYSERVICE/@PORT");
		define("protocol", STRING, "./KEYSERVICE/@PROTO");
		define("username", STRING, "./KEYSERVICE/@USERNAME");
		define("pwd", SECURE, "./KEYSERVICE/@PWD");
	}

	@Override
	public void load() throws Exception {
        loadAttributes();
	}

	@Override
	public void save() throws Exception {
        saveAttributes();
	}

	public String getHost() {
		return host;
	}

	public void setHost(String host) {
		this.host = host;
	}

	public int getPort() {
		return port;
	}

	public void setPort(int port) {
		this.port = port;
	}

	public String getProtocol() {
		return protocol;
	}

	public void setProtocol(String protocol) {
		this.protocol = protocol;
	}

	public String getUsername() {
		return username;
	}

	public void setUsername(String username) {
		this.username = username;
	}

	public String getPwd() {
		return pwd;
	}

	public void setPwd(String pwd) {
		this.pwd = pwd;
	}
}
