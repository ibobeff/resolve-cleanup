/******************************************************************************
* (C) Copyright 2014
*
* Resolve Systems, LLC
*
* All rights reserved
* This software is distributed under license and may not be used, copied,
* modified, or distributed without the express written permission of
* Resolve Systems, LLC.
*
******************************************************************************/

package com.resolve.assess;

import com.resolve.util.Constants;
import com.resolve.util.StringUtils;

public class AssessResult
{
    public boolean completed;         // task completed: true or false
    public String condition;          // result status: good, bad or unknown
    public String severity;           // result severity: critical, severe, warn, info or unknown
    public boolean exception;         // result severity: critical, severe, warn, info or unknown
    public String summary;            // short synopsis
    public String detail;             // detailed description with result values
    public String address;            // destination address that the action results refers to
    public int returncode;            // returncode
    public int duration;              // actiontask duration (secs)
    
    public AssessResult(Throwable t)
    {
        
        this("false", ""+Constants.ASSESS_RETURNCODE_EXECUTE_EXCEPTION, StringUtils.getStackTrace(t), StringUtils.getStackTrace(t), 0, "");
    } // AssessResult
    
    public AssessResult(String completed, String returncode, String summary, String detail, int duration, String address)
    {
        this.completed = false;
        this.condition  = Constants.ASSESS_CONDITION_GOOD;
        this.severity  = Constants.ASSESS_SEVERITY_GOOD;
        this.exception = false;
        this.detail    = detail;
        this.duration  = duration;
        this.address   = address;
        this.returncode = Integer.parseInt(returncode);
        
        // returncode = 0
        if (this.returncode == Constants.ASSESS_RETURNCODE_EXECUTE_SUCCESS)
        {
            if (completed != null)
            {
	            this.completed = completed.equalsIgnoreCase("TRUE");
            }
        }
        
        // returncode = -1
        else if (this.returncode == Constants.ASSESS_RETURNCODE_EXECUTE_FAIL)
        {
	        this.completed = false;
	        this.condition  = Constants.ASSESS_CONDITION_BAD;
	        this.severity  = Constants.ASSESS_SEVERITY_CRITICAL;
        }
        
        // returncode = -2
        else if (this.returncode == Constants.ASSESS_RETURNCODE_EXECUTE_EXCEPTION)
        {
	        this.completed = true;
	        this.condition  = Constants.ASSESS_CONDITION_BAD;
	        this.severity  = Constants.ASSESS_SEVERITY_CRITICAL;
        }
        
        // returncode = -3
        else if (this.returncode == Constants.ASSESS_RETURNCODE_EXECUTE_TIMEOUT)
        {
	        this.completed = false;
	        this.condition  = Constants.ASSESS_CONDITION_BAD;
	        this.severity  = Constants.ASSESS_SEVERITY_CRITICAL;
        }
        
        // returncode > 0
        else if (this.returncode > Constants.ASSESS_RETURNCODE_EXECUTE_SUCCESS)
        {
	        //this.completed = true;
            this.completed = false;
	        this.condition  = Constants.ASSESS_CONDITION_BAD;
	        this.severity  = Constants.ASSESS_SEVERITY_CRITICAL;
        }
        
        setSummary(summary);
    } // AssessResult
    
    public String toString()
    {
        String result = "";
        
        result += "completed:    "+completed+"\n";
        result += "condition:    "+condition+"\n";
        result += "severity:     "+severity+"\n";
        result += "exception:    "+exception+"\n";
        result += "duration:     "+duration+"\n";
        result += "address:      "+address+"\n";
        result += "returncode:   "+returncode+"\n";
        result += "summary:\n    "+summary+"\n";
        result += "detail:\n     "+detail+"\n";
        
        return result;
    } // toString
    
    public boolean isCompleted()
    {
        return completed;
    } // isCompleted
    
    public boolean isCompletion()
    {
        return completed;
    } // isCompletion
    
    public void setCompleted(boolean completed)
    {
        this.completed = completed;
    } // setCompletion
    
    public void setCompletion(boolean completed)
    {
        this.completed = completed;
    } // setCompletion
    
    public String getCondition()
    {
        String result = Constants.ASSESS_CONDITION_UNKNOWN;
        if (condition != null)
        {
	        if (condition.equalsIgnoreCase("GOOD") || condition.equalsIgnoreCase("SUCCESS") || condition.equalsIgnoreCase("OK"))
	        {
	            result = Constants.ASSESS_CONDITION_GOOD;
	        }
	        else if (condition.equalsIgnoreCase("BAD") || condition.equalsIgnoreCase("FAIL"))
	        {
	            result = Constants.ASSESS_CONDITION_BAD;
	        }
	        else
	        {
	            result = condition.toUpperCase();
	        }
        }
        return result;
    } // getCondition
    
    public void setCondition(String condition)
    {
        if (condition == null)
        {
            condition = "false";
        }
        
        if (condition.equalsIgnoreCase("false") || condition.equalsIgnoreCase("true"))
        {
            setCondition(new Boolean(condition));
        }
        else
        {
            this.condition = condition;
        }
    } // setCondition
    
    public void setCondition(boolean condition)
    {
        if (condition)
        {
            this.condition = "GOOD";
            this.severity = "GOOD";
        }
        else
        {
            this.condition = "BAD";
            this.severity = "CRITICAL";
        }
    } // setCondition
    
    public String getSeverity()
    {
        String result = Constants.ASSESS_SEVERITY_UNKNOWN;
        
        if (severity != null)
        {
	        if (severity.equalsIgnoreCase("CRITICAL"))
	        {
	            result = Constants.ASSESS_SEVERITY_CRITICAL;
	        }
	        else if (severity.equalsIgnoreCase("SEVERE"))
	        {
	            result = Constants.ASSESS_SEVERITY_SEVERE;
	        }
	        else if (severity.equalsIgnoreCase("WARN") || severity.equalsIgnoreCase("WARNING"))
	        {
	            result = Constants.ASSESS_SEVERITY_WARNING;
	        }
	        else if (severity.equalsIgnoreCase("GOOD") || severity.equalsIgnoreCase("INFO") || severity.equalsIgnoreCase("OK"))
	        {
	            result = Constants.ASSESS_SEVERITY_GOOD;
	        }
	        else
	        {
	            result = severity.toUpperCase();
	        }
        }
        return result;
    } // getSeverity
    
    public void setSeverity(String severity)
    {
        this.severity = severity;
    } // setSeverity
    
    public String getSummary()
    {
        String result = "";
        if (summary != null)
        {
            result = summary;
        }
        return result;
    } // getSummary
    
    public void setSummary(String summary)
    {
        if (summary != null)
        {
	        this.summary = summary;
//	        this.summary = this.summary.replaceAll("<", "&#60;");
//	        this.summary = this.summary.replaceAll(">", "&#62;");
        }
    } // setSummary

    public String getDetail()
    {
        String result = "";
        if (detail != null)
        {
            result = detail;
        }
        return result;
    } // getDetail
    
    public void setDetail(String detail)
    {
        this.detail = detail;
    } // setDetail
    
    public void setFailed(String summary)
    {
        setFailed(summary, summary);
    } // setFailed
    
    public void setFailed(String summary, String detail)
    {
        setCompleted(false);
        setCondition(Constants.ASSESS_CONDITION_BAD);
        setSeverity(Constants.ASSESS_SEVERITY_CRITICAL);
        setSummary(summary);
        setDetail(detail);
    } // setFailed
    
    public int getDuration()
    {
        return this.duration;
    } // getDuration
    
    public void setDuration(int duration)
    {
        this.duration = duration;
    } // setDuration
    
    public void setDuration(String duration)
    {
        if (StringUtils.isEmpty(duration))
        {
            this.duration = 0;
        }
        else
        {
	        this.duration = Integer.parseInt(duration);
        }
    } // setDuration
    
    public String getAddress()
    {
        return address;
    } // getAddress

    public void setAddress(String address)
    {
        this.address = address;
    } // setAddress

    public int getReturncode()
    {
        return returncode;
    } // getReturncode

    public void setReturncode(int returncode)
    {
        this.returncode = returncode;
    } // setReturncode

} // AssessResult
