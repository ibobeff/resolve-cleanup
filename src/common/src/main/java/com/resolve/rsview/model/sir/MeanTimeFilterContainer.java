package com.resolve.rsview.model.sir;

public class MeanTimeFilterContainer
{
    private String username;
    private String typeFilter;
    private String severityFilter;
    private String ownerFilter;
    private String statusFilter;
    private String priorityFilter;
    private String visualizedFilter;
    private String orgId;

    public MeanTimeFilterContainer(String username, String typeFilter, String severityFilter, String ownerFilter, String statusFilter, String priorityFilter, String visualizedFilter, String orgId)
    {
        this.username = username;
        this.typeFilter = typeFilter;
        this.severityFilter = severityFilter;
        this.ownerFilter = ownerFilter;
        this.statusFilter = statusFilter;
        this.priorityFilter = priorityFilter;
        this.visualizedFilter = visualizedFilter;
        this.orgId = orgId;
    }

    public String getUsername()
    {
        return username;
    }

    public void setUsername(String userName)
    {
        this.username = userName;
    }

    public String getTypeFilter()
    {
        return typeFilter;
    }

    public void setTypeFilter(String typeFilter)
    {
        this.typeFilter = typeFilter;
    }

    public String getSeverityFilter()
    {
        return severityFilter;
    }

    public void setSeverityFilter(String severityFilter)
    {
        this.severityFilter = severityFilter;
    }

    public String getOwnerFilter()
    {
        return ownerFilter;
    }

    public void setOwnerFilter(String ownerFilter)
    {
        this.ownerFilter = ownerFilter;
    }

    public String getStatusFilter()
    {
        return statusFilter;
    }

    public void setStatusFilter(String statusFilter)
    {
        this.statusFilter = statusFilter;
    }

    public String getPriorityFilter()
    {
        return priorityFilter;
    }

    public void setPriorityFilter(String priorityFilter)
    {
        this.priorityFilter = priorityFilter;
    }

    public String getVisualizedFilter()
    {
        return visualizedFilter;
    }

    public void setVisualizedFilter(String visualizedFilter)
    {
        this.visualizedFilter = visualizedFilter;
    }

    public String getOrgId()
    {
        return orgId;
    }

    public void setOrgId(String orgId)
    {
        this.orgId = orgId;
    }

}
