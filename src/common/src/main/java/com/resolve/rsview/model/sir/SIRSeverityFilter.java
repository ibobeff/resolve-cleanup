package com.resolve.rsview.model.sir;

import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

public enum SIRSeverityFilter
{
        CRITICAL("Critical"), 
        HIGH("High"), 
        MEDIUM("Medium"), 
        LOW("Low");
    
        private SIRSeverityFilter(String name)
        {
            this.name = name;
        }

        private String name;
    
        public String getName()
        {
            return name;
        }
        
        public static List<String> getSeverityNames() {
            return Arrays.asList(values()).stream().map(p -> p.name).collect(Collectors.toList());
        }

}
