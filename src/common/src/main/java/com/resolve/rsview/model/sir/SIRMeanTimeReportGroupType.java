package com.resolve.rsview.model.sir;

public enum SIRMeanTimeReportGroupType
{
        SEVERITY("u_severity"),
        TYPE("u_investigation_type"),
        OWNER("u_owner");
    
        private SIRMeanTimeReportGroupType(String columnName)
        {
            this.columnName = columnName;
        }

        private String columnName;
    
        public String getColumnName()
        {
            return columnName;
        }
        
}
