package com.resolve.rsview.model.sir;

public enum SIRMeanTimeReportType
{
        MTTR("u_closed_on"), 
        MTTA("u_assignment_date");
    
        private SIRMeanTimeReportType(String columnName)
        {
            this.columnName = columnName;
        }

        private String columnName;
    
        public String getColumnName()
        {
            return columnName;
        }

}
