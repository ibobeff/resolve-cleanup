/******************************************************************************
* (C) Copyright 2014
*
* Resolve Systems, LLC
*
* All rights reserved
* This software is distributed under license and may not be used, copied,
* modified, or distributed without the express written permission of
* Resolve Systems, LLC.
*
******************************************************************************/

package com.resolve.util;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.NotSerializableException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.Serializable;
import java.util.HashMap;
import java.util.Map;

import org.apache.commons.lang3.SerializationUtils;

/**
 * The ObjectProperties class allows a user to persist serialized objects to a
 * specified file in a format that can be reconstructed at a later time. This is
 * done through a Properties map. Make sure the same ObjectProperties instance
 * is used to write to one file, or else the file will be overwritten among
 * different instances. A different instance may be used to reload Objects.
 */
public class ObjectProperties
{
    private final Properties storedProperties = new Properties();

    public ObjectProperties(Map<String, Object> properties)
    {
        if (properties != null)
        {
            for (String key : properties.keySet())
            {
                write((String) key, properties.get(key));
            }
        }
    } // ObjectProperties

    /**
     * Persists all the properties inside a Map into the underlying file.
     *
     * @param properties
     */
    public void save(Map<String, Object> properties)
    {
        if (properties != null)
        {
            for (String key : properties.keySet())
            {
                write((String) key, properties.get(key));
            }
        }
    }

    /**
     * Persists an Object to the specified filepath in serialized form. Be sure
     * that the same ObjectProperties instance is used for multiple write()s to
     * the same file, or else the file will be overwritten and data will be
     * lost.
     *
     * @param key
     *            Unique key that can be used to retrieve the serialized Object
     *            at a later time.
     * @param object
     *            Object (must implement Serializable) that will be persisted.
     */
    public void write(String key, Object object)
    {
        if (key != null && object != null)
        {
            try
            {
                if (!(object instanceof Serializable))
                {
                    throw new NotSerializableException(object.getClass().getName() + " is not Serializable");
                }

                if (object instanceof String)
                {
                    storedProperties.setProperty(key, (String) object);
                }
                else
                {
                    // Serializes to Byte stream
                    ByteArrayOutputStream byteOS = new ByteArrayOutputStream();
                    ObjectOutputStream objectOS = new ObjectOutputStream(byteOS);
                    objectOS.writeObject(object);

                    // Do not change this encoding
                    String serialStr = byteOS.toString("ISO8859_1"); 

                    byteOS.close();
                    objectOS.close();

                    storedProperties.setProperty(key, serialStr);
                }
            }
            catch (NotSerializableException e)
            {
                Log.log.error(e.getMessage(), e);
            }
            catch (IOException e)
            {
                Log.log.error(e.getMessage(), e);
            }
        }
    } // write

    /**
     * Gets the properties as the raw string that was stored from serialized object.
     * 
     * @return
     */
    public Map<String, String> getRaw()
    {
        Map<String, String> result = new HashMap<String, String>();
        try
        {
            for(Object key : storedProperties.keySet())
            {
                String name = (String) key;
                String serializedStr = storedProperties.getProperty(name);
                if (serializedStr == null)
                {
                    Log.log.info("Ignored name property " + name + " since it does not have data");
                }
                else
                {
                    result.put(name, serializedStr);
                    Log.log.trace("Added " + name + " as a String, value: " + serializedStr);
                }
            }
        }
        catch (Exception e)
        {
            Log.log.error(e.getMessage(), e);
        }
        return result;
    } // getRaw

    /**
     * Gets the properties as the raw string that was stored from serialized object.
     * 
     * @return
     */
    public Map<String, String> getInMemoryRaw()
    {
        Map<String, String> result = new HashMap<String, String>();
        try
        {
            for(Object key : storedProperties.keySet())
            {
                String name = (String) key;
                String serializedStr = storedProperties.getProperty(name);
                if (serializedStr == null)
                {
                    Log.log.info("Ignored name property " + name + " since it does not have data");
                }
                else
                {
                    result.put(name, serializedStr);
                    Log.log.debug("Added " + name + " as a String, value: " + serializedStr);
                }
            }
        }
        catch (Exception e)
        {
            Log.log.error(e.getMessage(), e);
        }
        return result;
    } // getRaw

    /**
     * Deserializes a object from a string that was saved through write() method.
     *
     * @param serializedString
     * @return
     */
    public static Object loadObject(String serializedString)
    {
        Object obj = null;
        try
        {
            if(StringUtils.isNotBlank(serializedString))
            {
                // Deserializes the objet. Do not change the encoding.
                ByteArrayInputStream byteIS = new ByteArrayInputStream(serializedString.getBytes("ISO8859_1"));
                ObjectInputStream objectIS = new ObjectInputStream(byteIS);

                obj = objectIS.readObject();

                byteIS.close();
                objectIS.close();
            }
        }
        catch (IOException e)
        {
            //"Ignoring as it may be just a simple String object");
        }
        catch (ClassNotFoundException e)
        {
            Log.log.error(e.getMessage() + " class, which cannot be found.", e);
        }
        return obj;
    } // loadObject

    public Properties getStoredProperties()
    {
        return storedProperties;
    }
    
    /**
     * This method serializes an object (Serializable) to byte array.
     *
     * @param obj
     * @return
     * @throws IOException
     */
    public static byte[] serialize(Serializable obj)
    {
        return SerializationUtils.serialize(obj);
    }

    /**
     * Deserializes an object. If the byte array is corrupted or not
     * a serializable java object then the result is unpredictable.
     *
     * @param data
     * @return
     * @throws IOException
     * @throws ClassNotFoundException
     */
    public static Object deserialize(byte[] data)
    {
        return SerializationUtils.deserialize(data);
    }
} // ObjectProperties
