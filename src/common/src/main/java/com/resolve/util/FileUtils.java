/******************************************************************************
 * (C) Copyright 2014
 *
 * Resolve Systems, LLC
 *
 * All rights reserved This software is distributed under license and may not be
 * used, copied, modified, or distributed without the express written permission
 * of Resolve Systems, LLC.
 *
 ******************************************************************************/

package com.resolve.util;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.URI;
import java.util.UUID;

import org.apache.commons.io.FilenameUtils;
import org.owasp.esapi.ESAPI;
import org.owasp.esapi.errors.IntrusionException;
import org.owasp.esapi.errors.ValidationException;

import com.resolve.rsbase.MainBase;

public class FileUtils extends org.apache.commons.io.FileUtils
{
    static final int MAX_FILE_READ_SIZE = 5242880;

    public static void unzip(File zipFile, File destinationFolder) throws Exception
    {
        if (zipFile != null && zipFile.exists())
        {
            Log.log.info("Unzipping file -->" + zipFile.getName());
            Compress compress = new Compress();
            try
            {
                compress.unzip(zipFile, destinationFolder);
            }
            catch (Exception e)
            {
                Log.log.error("Error in Unzipping file -->" + zipFile, e);
                throw e;
            }
        }
        else
        {
            Log.log.error("Zip file " + zipFile.getName() + " does not exists.");
            throw new Exception("Zip file " + zipFile.getName() + " does not exists.");
        }
    }

    public static void extractFile(File zipFile, File destinationFolder, String partialFileName) throws Exception
    {
        if (zipFile != null && zipFile.exists())
        {
            Compress compress = new Compress();
            try
            {
                compress.extractFile(zipFile, destinationFolder, partialFileName);
            }
            catch (Exception e)
            {
                Log.log.error("Error in Extracting file -->" + partialFileName, e);
                throw e;
            }
        }
        else
        {
            Log.log.error("Zip file " + zipFile.getName() + " does not exists.");
            throw new Exception("Zip file " + zipFile.getName() + " does not exists.");
        }
    }

    /**
     * utility to know if there are any files in this directory
     *
     * @param dirFullPath
     * @return
     */
    public static boolean hasFiles(String dirFullPath)
    {
        boolean hasFiles = false;
        File dir = new File(dirFullPath);

        if (dir.isDirectory())
        {
            File[] files = dir.listFiles();
            if (files != null && files.length > 0)
            {
                hasFiles = true;
            }
        } // end of if

        return hasFiles;
    }// hasFiles

    /**
     * Returns the separator based on the OS
     *
     * Character that separates components of a file path. This is "/" on UNIX
     * and "\" on Windows.
     *
     * @return
     */
    public static String getFileSeparator()
    {
        return System.getProperty("file.separator");
    }

    /**
     * Creates a random file name.
     *
     * @param extension
     * @param defaultExtension
     * @return
     */
    public static String createRandomFileName(String extension, String defaultExtension)
    {
        if (StringUtils.isBlank(extension))
        {
            extension = defaultExtension;
        }

        if (!extension.startsWith("."))
        {
            extension = "." + extension;
        }
        return UUID.randomUUID().toString() + extension;
    }

    /**
     * Generate a random file name and save the content to it in the resolve
     * "tmp" folder inside a random directory.
     *
     * @param fileName
     *            if not provided then a random name is selected
     * @param content
     * @param extension
     * @return
     * @throws Exception
     */
    public static File saveRandomTextFile(String fileName, String content, String extension) throws Exception
    {
        StringBuilder randomDir = new StringBuilder(MainBase.main.configGeneral.home);
        randomDir.append("/tmp/");
        randomDir.append(UUID.randomUUID().toString());
        randomDir.append("/");

        if (StringUtils.isBlank(fileName))
        {
            if (StringUtils.isBlank(extension))
            {
                extension = ".txt";
            }
            if (!extension.startsWith("."))
            {
                extension = "." + extension;
            }
            fileName = UUID.randomUUID().toString() + extension;
        }

        File tmpFile = new File(randomDir + fileName);

        try
        {
            FileUtils.writeStringToFile(tmpFile, content);
        }
        catch (IOException e)
        {
            Log.log.error(e.getMessage(), e);
            throw new Exception(e.getMessage(), e);
        }
        return tmpFile;
    }

    /**
     * Generate a random file name and save the content to it in the resolve
     * "tmp" folder inside a random directory.
     *
     * @param fileName
     *            if not provided then a random name is selected
     * @param content
     * @param extension
     * @return
     * @throws Exception
     */
    public static File saveRandomFile(String fileName, byte[] content, String extension) throws Exception
    {
        StringBuilder randomDir = new StringBuilder(MainBase.main.configGeneral.home);
        randomDir.append("/tmp/");
        randomDir.append(UUID.randomUUID().toString());
        randomDir.append("/");

        if (StringUtils.isBlank(fileName))
        {
            if (StringUtils.isBlank(extension))
            {
                extension = ".txt";
            }
            if (!extension.startsWith("."))
            {
                extension = "." + extension;
            }
            fileName = UUID.randomUUID().toString() + extension;
        }

        File tmpFile = new File(randomDir + fileName);

        try
        {
            FileUtils.writeByteArrayToFile(tmpFile, content);
        }
        catch (IOException e)
        {
            Log.log.error(e.getMessage(), e);
            throw new Exception(e.getMessage(), e);
        }
        return tmpFile;
    }

    public static String getFileExtension(String fileName)
    {
        return fileName.substring(fileName.lastIndexOf("."));
    }

    public static String removeExtension(String fileNameWithExt)
    {
        return FilenameUtils.removeExtension(fileNameWithExt);
    }

    /**
     * Recursively copy all the files of src folder if src is a directory. If
     * there is any failure in copying then it ignores and moves onto the next
     * file/directory.
     *
     * @param src
     * @param dest
     * @throws IOException
     */
    public static void copyDirectory(File src, File dest) throws IOException
    {
        if (src.isDirectory())
        {
            // creating parent folders where source files is to be copied
            try
            {
                dest.mkdirs();
            }
            catch (SecurityException e)
            {
                Log.log.warn("Permission error, couldn't not create all/some directories for " + dest.getAbsolutePath());
                throw new IOException(e.getMessage(), e);
            }
            for (File sourceChild : src.listFiles())
            {
                File destChild = new File(dest, sourceChild.getName());
                copyDirectory(sourceChild, destChild);
            }
        }
        else
        {
            try
            {
                copyFile(src, dest);
            }
            catch (IOException e)
            {
                Log.log.warn("Couldn't not copy " + src.getAbsolutePath() + " to " + dest.getAbsolutePath() + ", IGNORING!");
            }
        }
    }

    /**
     * Zip a folder.
     *
     * @param targetFolder
     *            to be zipped.
     * @param zipFile
     *            absolute file location.
     * @throws Exception
     */
    public static void zip(File targetFolder, File zipFile) throws Exception
    {
        if (targetFolder.exists())
        {
            Log.log.info("Zipping folder -->" + targetFolder.getName());
            Compress compress = new Compress();
            try
            {
                compress.zip(targetFolder, zipFile);
            }
            catch (Exception e)
            {
                Log.log.error("Error in Zipping folder -->" + targetFolder.getAbsoluteFile(), e);
                throw e;
            }
        }
        else
        {
            Log.log.error("Target folder " + targetFolder.getName() + " does not exists.");
            throw new Exception("Target folder " + targetFolder.getName() + " does not exists.");
        }
    }

    public static String separatorsToUnix(String file)
    {
        return FilenameUtils.separatorsToUnix(file);
    }

    public static java.util.Properties readFileAndConvertToProperties(String fileName) throws Exception
    {
        java.util.Properties properties = null;

        try
        {
            InputStream is = new FileInputStream(fileName);
            properties = new java.util.Properties();
            properties.load(is);
            is.close();
        }
        catch (Exception e)
        {
            throw new Exception("Failed to read from " + fileName + " file.");
        }

        return properties;
    }

    public static File getFile(String location)
    {
        File file = null;
        // File file = new File(location);
        // Log.log.info(String.format("FileUtils.getFile('%s')", location));
        String locationNormalizedNoEndSeparator = FilenameUtils.normalizeNoEndSeparator(location);
        String locationNormalizedNoEndSeparatorUNIX = FilenameUtils.separatorsToUnix(locationNormalizedNoEndSeparator);
        boolean isDirectory = FilenameUtils.getName(location).isEmpty() || FilenameUtils.getExtension(location).isEmpty();
        try
        {
            if (isDirectory)
            {
            	ESAPI.validator().getValidInput( "directory validation", locationNormalizedNoEndSeparator, "DirectoryName", 255, false);
                if (new File(ESAPI.validator().getValidDirectoryPath("directory validation", 
                													 locationNormalizedNoEndSeparator, 
                													 new File(File.separator), false)).exists())
                {
                    String dirName = ESAPI.validator().getValidDirectoryPath("directory validation", locationNormalizedNoEndSeparator, new File(File.separator), false);
                    file = new File(dirName);
                }
                else
                {
                    String canonical = ESAPI.validator().getValidInput("new direcotry", locationNormalizedNoEndSeparatorUNIX, "DirectoryName", 255, false);
                    file = new File(canonical);
                }
            }
            else
            {
                // fix all messed up separators
                String name = FilenameUtils.getName(locationNormalizedNoEndSeparator);
                String path = FilenameUtils.getFullPath(locationNormalizedNoEndSeparator);
                
                isDirectory = FilenameUtils.getName(path).isEmpty() || FilenameUtils.getExtension(path).isEmpty();
                
                String safeFilePath = null;
                
                if (isDirectory) {
                	String pathNormalizedNoEndSeparator = FilenameUtils.normalizeNoEndSeparator(path);
                    String pathNormalizedNoEndSeparatorUNIX = FilenameUtils.separatorsToUnix(pathNormalizedNoEndSeparator);
                    
                    if (new File(ESAPI.validator().getValidDirectoryPath("directory validation", 
							   											 pathNormalizedNoEndSeparator, 
							   											 new File(File.separator), false)).exists())
                    {
                        safeFilePath = ESAPI.validator().getValidDirectoryPath("directory validation", 
                        													   pathNormalizedNoEndSeparator, 
                        													   new File(File.separator), false);
                    }
                    else
                    {
                    	safeFilePath = ESAPI.validator().getValidInput("new directory", 
                    												   pathNormalizedNoEndSeparatorUNIX, 
                    												   "DirectoryName", 255, false);
                    }
                } else {
                	throw new ValidationException("Failed to validate specified location.", 
                								  "Specified path [" + path + "] is not a directory.");
                }
                
                String safeFilename = ESAPI.validator().getValidFileName("filename validation", name, ESAPI.securityConfiguration().getAllowedFileExtensions(), false);
                file = new File(safeFilePath, safeFilename);
                if ((MainBase.isUnix() && !file.getAbsolutePath().equals(file.getCanonicalPath())) || (!MainBase.isUnix() && !file.getAbsolutePath().equalsIgnoreCase(file.getCanonicalPath())))
                {
                    Log.log.warn("Absolute Path does not match with Canonical Path for location: " + location + " absolute: " + file.getAbsolutePath() + " canonical: " + file.getCanonicalPath());
                }
            }
        }
        catch (IOException e)
        {
            Log.log.error(e.getMessage(), e);
        }
        catch (ValidationException ve)
        {
            try
            {
                String canonical = null;
                if (isDirectory)
                {
                    canonical = ESAPI.validator().getValidInput("new direcotry", locationNormalizedNoEndSeparatorUNIX, "DirectoryName", 255, false);
                }
                else
                {
                    canonical = ESAPI.validator().getValidInput("new file", locationNormalizedNoEndSeparatorUNIX, "FileName", 255, false);
                }
                file = new File(canonical);
            }
            catch (ValidationException | IntrusionException e)
            {
                Log.log.error(e.getMessage(), e);
            }

        }
        catch (IntrusionException ie)
        {
            Log.log.error(ie.getMessage(), ie);
        }

        return file;

    }

    public static File getFile(URI uri)
    {
        File file = new File(uri);
        try
        {
            if ((MainBase.isUnix() && !file.getAbsolutePath().equals(file.getCanonicalPath())) || (!MainBase.isUnix() && !file.getAbsolutePath().equalsIgnoreCase(file.getCanonicalPath())))
            {
                Log.log.warn("Absolute Path does not match with Canonical Path for location: " + uri + " absolute: " + file.getAbsolutePath() + " canonical: " + file.getCanonicalPath());
            }
        }
        catch (IOException e)
        {
            Log.log.error(e.getMessage(), e);
        }

        return file;
    }

    public static File getFile(String parent, String child)
    {

        return getFile(FilenameUtils.concat(parent, child));
    }

    public static File getFile(File parent, String child)
    {
        return getFile(parent.toString(), child);
    }

    /*
     * public static boolean deleteDirectory(File path) { if (path.exists()) {
     * File[] files = path.listFiles(); for (int i = 0; i < files.length; i++) {
     * if (files[i].isDirectory()) { deleteDirectory(files[i]); } else {
     * files[i].delete(); } } } return (path.delete()); }
     */

    /*
     * public static boolean copyFile(File file1, File file2) { boolean result =
     * false;
     *
     * try { if (file1.isFile()) {
     *
     * if (file2.exists()) { if (!file2.delete()) { Log.log.warn(
     * "Unable to delete existing file " + file2.getAbsolutePath()); } }
     *
     * Log.log.info("Attempting to copy file " + file1.getAbsolutePath() +
     * " to " + file2.getAbsolutePath());
     *
     * FileInputStream fis = new FileInputStream(file1); FileOutputStream fos =
     * new FileOutputStream(file2);
     *
     * byte[] bytes = new byte[16384]; long fileSize = file1.length();
     *
     * while (fileSize > 0) { int read = fis.read(bytes); if (read > fileSize) {
     * read = (int) fileSize; } fos.write(bytes, 0, read); fileSize -= read; }
     *
     * fis.close(); fos.close();
     *
     * result = true; } else { Log.log.warn(file1.getAbsolutePath() +
     * " Does Not Exist or Is A Directory"); } } catch (Exception e) {
     * Log.log.error("Failed to copy file", e); result = false; }
     *
     * return result; } //copyFile
     */

    /*
     * public static boolean copyDirectory(File dir1, File dir2) { return
     * copyDirectory(dir1, dir2, true); } //copyDirectory
     *
     * public static boolean copyDirectory(File dir1, File dir2, boolean
     * recursive) { boolean result = false;
     *
     * try { if (dir1.isDirectory()) { if (dir2.exists()) { if (!dir2.delete())
     * { Log.log.warn("Unable to delete existing file " +
     * dir2.getAbsolutePath()); } }
     *
     * Log.log.info("Attempting to copy directory " + dir1.getAbsolutePath() +
     * " to " + dir2.getAbsolutePath());
     *
     * dir2.mkdirs();
     *
     * File[] dirFiles = dir1.listFiles(); result = true; for (int i=0;
     * i<dirFiles.length; i++) { File copyFile = dirFiles[i]; File toFile = new
     * File(dir2.getAbsolutePath() + "/" + copyFile.getName());
     *
     * if (copyFile.isFile()) { result = (result && FileUtils.copyFile(copyFile,
     * toFile)); } else if (recursive && copyFile.isDirectory()) { result =
     * (result && FileUtils.copyDirectory(copyFile, toFile)); } } } else {
     * Log.log.warn(dir1.getAbsolutePath() +
     * " Does Not Exist or Is Not A Directory" ); } } catch (Exception e) {
     * Log.log.error("Failed to copy directory", e); result = false; }
     *
     * return result; } //copyDirectory
     */

    // public static void main(String[] args)
    // {
    // /*
    // * Log.init(); File src = new File("c:/tmp/src"); File dest = new
    // * File("c:/tmp/dest");
    // *
    // * try { FileUtils.copyDirectory(src, dest); } catch (IOException e) {
    // * Log.log.error(e.getMessage(), e); }
    // */
    // }

} // FileUtils