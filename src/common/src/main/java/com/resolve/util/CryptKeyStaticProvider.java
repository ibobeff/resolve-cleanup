package com.resolve.util;

import com.resolve.util.CryptUtils.KeyType;

public class CryptKeyStaticProvider implements CryptKeyProvider {

	private static final String DEFAULT_ENCRYPTION_KEY = "This is a fairly long phrase used to encrypt";
	private static final String ENC_DATA_LOCAL = "ENC:XgAkfIoSYCVNis5EwVkQCakKjRroa8Gz6qZ6NTfxm4o=";
	private static final String CRYPTKEY = "this is a fairly long phrase - no problems only solutions";

	@Override
	public String getKey(KeyType keyType) {
		switch (keyType) {
		case AES:
			return ENC_DATA_LOCAL;
		case DES:
			return CRYPTKEY;
		case LICENSE:
			return DEFAULT_ENCRYPTION_KEY;
		default:
			throw new IllegalArgumentException("Unknown key type");
		}
	}
}
