/******************************************************************************
* (C) Copyright 2014
*
* Resolve Systems, LLC
*
* All rights reserved
* This software is distributed under license and may not be used, copied,
* modified, or distributed without the express written permission of
* Resolve Systems, LLC.
*
******************************************************************************/

package com.resolve.util;

//import java.lang.reflect.Constructor;
import java.lang.reflect.Field;
import java.lang.reflect.InvocationTargetException;
import java.util.ArrayList;
import java.util.List;

import org.apache.commons.beanutils.BeanUtils;
import org.apache.commons.beanutils.ConversionException;
import org.apache.commons.beanutils.PropertyUtils;

/**
 * Wrapper for apache commons beanutils.
 *
 */
public class BeanUtil
{
    /**
     * Copies all the common properties from origin to target object.
     *
     * Example: MCPServerVO to MCPServer entity.
     *
     * @param origin
     * @param target
     * @return
     */
    public static <T, K> T convert(K origin, T target)
    {
        try
        {
            BeanUtils.copyProperties(target, origin);
        }
        catch (IllegalAccessException e)
        {
            Log.log.error(e.getMessage(), e);
        }
        catch (InvocationTargetException e)
        {
            Log.log.error(e.getMessage(), e);
        }
        catch (ConversionException e)
        {
            //Log.log.error(e.getMessage(), e);
        }
        
        return target;
    }

    /**
     * Creates a new instance of a class.
     *
     * @param className is the full name with package.
     *
     * @return
     */
    @SuppressWarnings({ "rawtypes", "unchecked" })
    public static <T> T getInstance(String className)
    {
        T result = null;

        try
        {
        	if (className.startsWith("com.resolve.persistence.model.") ||
        	    className.startsWith("com.resolve.services.hibernate.vo.")) {
        		Class clazz = Class.forName(className);
        		result = (T) clazz.newInstance();
        	} else {
        		throw new ClassNotFoundException("Class " + className + " not found.");
        	}
        }
        catch (ClassNotFoundException e)
        {
            Log.log.error(e.getMessage(), e);
        }
        catch (InstantiationException e)
        {
            Log.log.error(e.getMessage(), e);
        }
        catch (IllegalAccessException e)
        {
            Log.log.error(e.getMessage(), e);
        }

        return result;
    }

    /**
     * Gets value of a property in an object.
     *
     * @param bean
     * @param propertyName
     * @return
     * @throws IllegalAccessException
     * @throws InvocationTargetException
     * @throws NoSuchMethodException
     */
    public static String getStringProperty(Object bean, String propertyName) throws IllegalAccessException, InvocationTargetException, NoSuchMethodException
    {
        return BeanUtils.getProperty(bean, propertyName);
    }
    
    
    /**
     * Get value of a property in an object
     * 
     * @param bean
     * @param propertyName
     * @return
     * @throws IllegalAccessException
     * @throws InvocationTargetException
     * @throws NoSuchMethodException
     */
    public static Object getProperty(Object bean, String propertyName) throws IllegalAccessException, InvocationTargetException, NoSuchMethodException
    {
        return BeanUtils.getProperty(bean, propertyName);
    }

    /**
     * returns all the Fields for a particular class - even the inherited ones
     *
     * @param fields
     * @param type
     * @return
     */
    public static List<Field> getAllFields(List<Field> fields, Class<?> type)
    {
        if(fields == null)
        {
            fields = new ArrayList<Field>();
        }

        for (Field field : type.getDeclaredFields())
        {
            fields.add(field);
        }

        if (type.getSuperclass() != null)
        {
            fields = getAllFields(fields, type.getSuperclass()); //RECURSIVE
        }

        return fields;
    }

    public static void setProperty(Object bean, String propertyName, Object value)
    {
        Class<?> type = bean.getClass();
        try
        {
            BeanUtils.setProperty(bean, propertyName, value);
        }
        catch (IllegalAccessException e)
        {
            //Log.log.error(e.getMessage(), e);
        }
        catch (InvocationTargetException e)
        {
            //Log.log.error(e.getMessage(), e);
        }
    }
    
    /**
     * Gets Long value of a property in an object.
     *
     * @param bean
     * @param propertyName
     * @return
     * @throws IllegalAccessException
     * @throws InvocationTargetException
     * @throws NoSuchMethodException
     */
    public static Long getLongProperty(Object bean, String propertyName) throws IllegalAccessException, InvocationTargetException, NoSuchMethodException
    {
        Long result = null;
        Object value = PropertyUtils.getSimpleProperty(bean, propertyName);
        if(value instanceof Long)
        {
            result = (Long) value;
        }
        return result;
    }

    public static Object getObjectProperty(Object bean, String propertyName) throws IllegalAccessException, InvocationTargetException, NoSuchMethodException
    {
        return PropertyUtils.getSimpleProperty(bean, propertyName);
    }

    /*
    /**
     * Creates a new instance of a class using zero argument constructor.
     *
     * @param className is the full name with package.
     *
     * @return
     *
    @SuppressWarnings({ "rawtypes", "unchecked" })
    public static <T> T getInstanceNoArgConstructor(String className)
    {
        T result = null;

        try
        {
            Class<T> clazz = (Class<T>) Class.forName(className, false, BeanUtil.class.getClassLoader());
            Constructor<T> clazzNoArgConstructor = clazz.getDeclaredConstructor((Class<?>[])null);
            result = (T) clazzNoArgConstructor.newInstance((Class<?>[])null);
        }
        catch (ExceptionInInitializerError e)
        {
            Log.log.error(e.getMessage(), e);
        }
        catch (LinkageError e)
        {
            Log.log.error(e.getMessage(), e);
        }
        catch (ClassNotFoundException e)
        {
            Log.log.error(e.getMessage(), e);
        }
        catch (NoSuchMethodException e)
        {
            Log.log.error(e.getMessage(), e);
        }
        catch (SecurityException e)
        {
            Log.log.error(e.getMessage(), e);
        }
        catch (InvocationTargetException e)
        {
            Log.log.error(e.getMessage(), e);
        }
        catch (IllegalAccessException e)
        {
            Log.log.error(e.getMessage(), e);
        }
        catch (InstantiationException e)
        {
            Log.log.error(e.getMessage(), e);
        }

        return result;
    }*/
}