package com.resolve.util.performance.datasources;

import static java.time.temporal.ChronoUnit.MILLIS;
import static org.apache.commons.lang3.StringUtils.isEmpty;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.IOException;
import java.io.PrintWriter;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

import com.resolve.util.Log;
import com.resolve.util.performance.ExecutionStatistic;

public class RunbookExecutionTimeDataSource {

	private static final DateTimeFormatter DATE_TIME_FORMAT = DateTimeFormatter.ofPattern("yyyy-MM-dd'T'HH:mm:ss.SSS");

	private static final String CSV_HEADERS = "MESSAGE_ID,STEP,PHASE,RUNBOOK_ID,ACTION_TASK_ID,DATE_TIME,DURATION,DURATION_TOTAL,METHOD_NAME";
	
	private String rootPath;
	private String filename = "performance.csv";
	private Map<String, List<ExecutionStatistic>> msgIdToLog = new HashMap<String, List<ExecutionStatistic>>();

	public RunbookExecutionTimeDataSource(String rootPath) {
		this.rootPath = rootPath;
	}

	public void log(String messageId, String runbookId, String actionTaskId, ExecutionStep step, ExecutionStepPhase phase) {
		msgIdToLog.putIfAbsent(messageId, new LinkedList<ExecutionStatistic>());

		LinkedList<ExecutionStatistic> currentRunbookLog = (LinkedList<ExecutionStatistic>) msgIdToLog.get(messageId);
		LocalDateTime now = LocalDateTime.now();

		StackTraceElement[] stackTraceElements = Thread.currentThread().getStackTrace();
		String caller = stackTraceElements[3].getClassName() + "#" + stackTraceElements[3].getMethodName() + "@" + stackTraceElements[3].getLineNumber();

		long totalDuration = currentRunbookLog.isEmpty() ? 0 : currentRunbookLog.getFirst().getCreated().until(now, MILLIS);
		currentRunbookLog.add(new ExecutionStatistic(messageId, runbookId, actionTaskId, caller, step, phase, now, 0, totalDuration));
	}

	/**
	 * This method exports all {@link ExecutionStatistic} to a given log file.
	 * <p>
	 * For Action Task related statistics - it prints them as they are.
	 * </p>
	 * <p>
	 * For everything else - it aggregates the given {@link ExecutionStep} to two
	 * 'from-to' CSV records in order to make the log more compact.
	 * </p>
	 * 
	 * @param messageId
	 */
	public synchronized void exportExecutionTimeStatistics(String messageId) {
		try (PrintWriter writer = new PrintWriter(new FileOutputStream(new File(rootPath, filename), true), false)) {

			LinkedList<ExecutionStatistic> log = (LinkedList<ExecutionStatistic>) msgIdToLog.get(messageId);
			ExecutionStatistic previous = log.getFirst();

			for (ExecutionStatistic element : log) {
				element.setDuration(previous.getCreated().until(element.getCreated(), MILLIS));
				writer.println(toCsvLine(element));
				previous = element;
			}

			writer.flush();
		} catch (IOException ex) {
			Log.log.error(ex.getMessage(), ex);
		}
	}

	public void initLogFile() {
		File logFile = new File(rootPath, filename);

		try (PrintWriter writer = new PrintWriter(new FileOutputStream(logFile, true));
				BufferedReader reader = new BufferedReader(new FileReader(logFile));) {

			String firstLine = reader.readLine();
			if (isEmpty(firstLine)) {
				writer.println(CSV_HEADERS);
			}

		} catch (IOException ex) {
			Log.log.error(String.format("Error while creating CSV headers, message: %s", ex.getMessage()), ex);
		}
	}

	private String toCsvLine(ExecutionStatistic log) {
		return String.join(",", 
				log.getMessageId(), 
				log.getStep().toString(), 
				log.getPhase().toString(),
				log.getRunbookId(), 
				log.getActionTaskId(), 
				log.getCreated().format(DATE_TIME_FORMAT),
				Long.toString(log.getDuration()), 
				Long.toString(log.getTotalDuration()),
                log.getMethodName());
	}

}