package com.resolve.util.performance.datasources;

public enum ExecutionStep {

	MESSAGE_CONSUMED, //
	
	RUNBOOK_INIT, //
	
	LICENSE_CHECK, //
	
	ACTION_TASK_EXECUTION, //
	
	RUNBOOK_FINALIZE, //, 

	METHOD_EXECUTION,
	
	PARAMETER_DECRYPTION
}