/******************************************************************************
* (C) Copyright 2014
*
* Resolve Systems, LLC
*
* All rights reserved
* This software is distributed under license and may not be used, copied,
* modified, or distributed without the express written permission of
* Resolve Systems, LLC.
*
******************************************************************************/

package com.resolve.util;

import java.text.SimpleDateFormat;
import java.time.ZoneId;
import java.util.Calendar;
import java.util.Date;
import java.util.TimeZone;

public class GMTDate
{
    public static Calendar calendar;

    /*
    public static void main(String[] args)
    {
        GMTDate.initTimezone(null);
        System.out.println(getDefaultTimezone());
    }
    */

    public static void initTimezone(String timezoneId)
    {
        if (StringUtils.isNotBlank(timezoneId))
        {
            Log.log.debug("Setting timezone: " + timezoneId);
            calendar = Calendar.getInstance(TimeZone.getTimeZone(timezoneId));
        }
        else
        {
            calendar = Calendar.getInstance();
        }
        
        Log.log.info("Time Zone Set To : " + calendar.getTimeZone());
        
        if (!calendar.getTimeZone().equals(TimeZone.getTimeZone(ZoneId.of("UTC"))) &&
            !calendar.getTimeZone().equals(TimeZone.getTimeZone(ZoneId.of("GMT"))) &&
            !calendar.getTimeZone().equals(TimeZone.getTimeZone(ZoneId.of("UT"))))
        {
            Log.log.error("Resolve requires Time Zone to be set to GMT/UTC/UT\n" + 
                          "and setting it to any other value may result\n" +
                          "in incorrect operation and/or incorrect\n" +
                          "display of time stamps accross Resolve.\n" +
                          "It is higly recommended to set the Time Zone\n" +
                          "correctly to GMT/UTC/UT before proceeding ahead with using system.");
        }
    } // initTimezone
    
    public static void initTimezoneSOP(String timezoneId)
    {
        if (StringUtils.isNotBlank(timezoneId))
        {
            System.out.println("Setting timezone: " + timezoneId);
            calendar = Calendar.getInstance(TimeZone.getTimeZone(timezoneId));
        }
        else
        {
            calendar = Calendar.getInstance();
        }
        
        System.out.println("Time Zone Set To : " + calendar.getTimeZone());
    } // initTimezoneSOP
    
    public static String getDefaultTimezone()
    {
        Date date = calendar.getTime();
        SimpleDateFormat sdf = new SimpleDateFormat("Z");
        String generalTimezone = sdf.format(date);// -0800
        generalTimezone = generalTimezone.substring(0, generalTimezone.length() - 2) + ":" + generalTimezone.substring(generalTimezone.length() - 2);
        String tz = "GMT" + generalTimezone;
        return tz;
    }

    public static Date getDate()
    {
        return getDate(new Date());
    } // getDate

    /**
     * This API returns a server date when the DB date is passed to it.
     * 
     * @param gmt
     * @return
     */
    public static Date getLocalServerDate(Date gmt)
    {
        long time = gmt.getTime() + (calendar.get(Calendar.ZONE_OFFSET) + calendar.get(Calendar.DST_OFFSET));

        // FIX return GMT zone
        return new Date(time);
    } // getDate

    public static Date getDate(Date current)
    {
        long time = current.getTime() - (calendar.get(Calendar.ZONE_OFFSET) + calendar.get(Calendar.DST_OFFSET));

        // FIX return GMT zone
        return new Date(time);
    } // getDate

    public static Date getDate(long current)
    {
        long time = current - (calendar.get(Calendar.ZONE_OFFSET) + calendar.get(Calendar.DST_OFFSET));

        // FIX return GMT zone
        return new Date(time);
    } // getDate

    public static long getTime()
    {
        return getTime(new Date());
    } // getTime

    public static long getTime(Date current)
    {
        return current.getTime() - (calendar.get(Calendar.ZONE_OFFSET) + calendar.get(Calendar.DST_OFFSET));
    } // getTime

    public static long getTime(long current)
    {
        return current - (calendar.get(Calendar.ZONE_OFFSET) + calendar.get(Calendar.DST_OFFSET));
    } // getTime

}