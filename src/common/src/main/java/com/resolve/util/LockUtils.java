/******************************************************************************
* (C) Copyright 2014
*
* Resolve Systems, LLC
*
* All rights reserved
* This software is distributed under license and may not be used, copied,
* modified, or distributed without the express written permission of
* Resolve Systems, LLC.
*
******************************************************************************/

package com.resolve.util;

import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.locks.ReadWriteLock;
import java.util.concurrent.locks.ReentrantLock;
import java.util.concurrent.locks.ReentrantReadWriteLock;

public class LockUtils
{
    static ConcurrentHashMap<String, Map<String,Object>> locks = new ConcurrentHashMap<String, Map<String,Object>>();
    private static ConcurrentHashMap<String, Set<String>> processid2Locks = new ConcurrentHashMap<String, Set<String>>();
    private static String LOCK = "Lock";
    private static String TIMESTAMP = "TimeStamp";
    private static ReadWriteLock locksRWLock = new ReentrantReadWriteLock(true);
    private static ReadWriteLock pid2LocksRWLock = new ReentrantReadWriteLock(true);
    
    /**
     * Returns a Reentrant lock object using the given name parameter 
     *
     * @param  name the name of the lock
     * @return      the ReentrantLock object associated with the name parameter
     */
    @SuppressWarnings("unchecked")
    public static ReentrantLock getLock(String name)
    {
        ReentrantLock lock = null;
        boolean rlocked = false;
        boolean wlocked = false;
        
        try {
            locksRWLock.readLock().lock();
            rlocked = true;
            
            if (locks.containsKey(name)) {
            	Object obj = locks.get(name);
            	
            	if (obj != null) {
            		Map<String, Object> lockMap = (Map<String, Object>) obj;
            		lockMap.put(TIMESTAMP, System.currentTimeMillis());
            		lock = (ReentrantLock) lockMap.get(LOCK);
            	}
            } else {
            	lock = new ReentrantLock();
            	Map<String, Object> lockMap = new HashMap<String,Object>();
            	lockMap.put(LOCK, lock);
            	lockMap.put(TIMESTAMP, System.currentTimeMillis());
            	locksRWLock.readLock().unlock();
                rlocked = false;
                locksRWLock.writeLock().lock();
                wlocked = true;
            	locks.putIfAbsent(name, lockMap);
            }
        } finally {
            if (rlocked) {
                locksRWLock.readLock().unlock(); 
            }
            
            if (wlocked) {
                locksRWLock.writeLock().unlock(); 
            }
        }

        return lock;
    } // getLock

    
    /**
     * Removes a lock of the given name 
     *
     * @param  name the name with the lock to remove
     */
    public static void removeLock(String name)
    {
    	try {
            locksRWLock.writeLock().lock();
            locks.remove(name);
        } finally {
            locksRWLock.writeLock().unlock();
        }
    } // removeLock

    /**
     *
     * @param  name the name of the lock to add
     * @param  processid the process ID to associate with the added lock
     */
    public static void addProcessLock(String name, String processid)
    {
    	boolean rlocked = false;
        boolean wlocked = false;
        
        try {
            pid2LocksRWLock.readLock().lock();
            rlocked = true;
            
            Set<String> lockNames = processid2Locks.get(processid);
            
	        if (lockNames == null) {
	            lockNames = new HashSet<String>();
	            pid2LocksRWLock.readLock().unlock();
	            rlocked = false;
	            pid2LocksRWLock.writeLock().lock();
	            wlocked = true;
	            processid2Locks.putIfAbsent(processid, lockNames);
	        }
	        
	        if (rlocked) {
                pid2LocksRWLock.readLock().unlock();
                rlocked = false;
            }
            
            if (!wlocked) {
                pid2LocksRWLock.writeLock().lock();
                wlocked = true;
            }
            
            lockNames.add(name);
            
            if (Log.log.isDebugEnabled()) {
                Log.log.debug(String.format("After addProcessLock(%s, %s) : processid2Locks size: %d locks size: %d", 
                                            name, processid, processid2Locks.size(), locks.size()));
            }
        } finally {
            if (rlocked) {
                pid2LocksRWLock.readLock().unlock(); 
            }
            
            if (wlocked) {
                pid2LocksRWLock.writeLock().unlock(); 
            }
        }
    } // addProcessLock

    /**
     * Removes all locks with the given process ID
     *
     * @param  processid the process ID of the lock that you wish to remove
     */
    
    public static void removeProcessLocks(String processid)
    {
    	boolean rlocked = false;
        boolean wlocked = false;
        
        try {
            pid2LocksRWLock.readLock().lock();
            rlocked = true;
            
            Set<String> lockNames = processid2Locks.get(processid);
            
	        if (lockNames != null) {
	        	pid2LocksRWLock.readLock().unlock();
                rlocked = false;
                
	            for (String name : lockNames) {
	                removeLock(name);
	                
	                if (Log.log.isDebugEnabled()) {
                        Log.log.debug(String.format("Remove lock %s for processid %s", name, processid));
                    }
	            }
	            
	            pid2LocksRWLock.writeLock().lock();
                wlocked = true;
	            processid2Locks.remove(processid);
	        }
	        
	        if (rlocked) {
                pid2LocksRWLock.readLock().unlock();
                rlocked = false;
            }
            
            if (Log.log.isDebugEnabled()) {
                Log.log.debug(String.format("After removeProcessLocks(%s) : processid2Locks size: %d locks size: %d", 
                                            processid, processid2Locks.size(), locks.size()));
            }
        } finally {
            if (rlocked) {
                pid2LocksRWLock.readLock().unlock(); 
            }
            
            if (wlocked) {
                pid2LocksRWLock.writeLock().unlock(); 
            }
        }
    } // removeProcessLocks

    /**
     * Returns a ReentrantLock object using the given name parameter. If the current collection of locks doesn't 
     * include the given name parameter, a lock is added to the collection with the given name and associated 
     * with the given process ID. 
     *
     * @param  name the name of the lock
     * @param  processid the process ID associated with the lock.
     * @return      the ReentrantLock object associated with the name parameter
     */
    
    public static ReentrantLock getLock(String name, String processid)
    {
        ReentrantLock lock = null;
        boolean rlocked = false;
        boolean wlocked = false;
        
        try {
            locksRWLock.readLock().lock();
            rlocked = true;
            
            if (locks.containsKey(name)) {
            	lock = getLock(name);
            } else {
            	locksRWLock.readLock().unlock();
                rlocked = false;
                
            	lock = getLock(name);
            	addProcessLock(name, processid);
            }
        } finally {
            if (rlocked) {
                locksRWLock.readLock().unlock(); 
            }
        }
        
        return lock;
    } // getLock

    static 
    {
    	new Thread()
    	{
    		public void run()
    		{
    			while(true)
    			{
	    			try
	    			{
	    				long now = System.currentTimeMillis();
	    				
	    				// Reading without locking lock's read lock intentionally
	    				
		    			for (String key : locks.keySet())
		    			{
		    				if (locks.get(key) != null && ((now - (Long)locks.get(key).get(TIMESTAMP)) > 15*60*1000))
		    				{
		    					/*
		    				     * HP TODO Should remove process id related locks only and 
		    				     * timeout should be based on runbook timeout
		    				     */
		    					try {
		    				        locksRWLock.writeLock().lock();
		    						locks.remove(key);
		    						
		    						Log.log.info(String.format("Removed stale lock %s", key));
		    					} finally {
		    				        locksRWLock.writeLock().unlock(); 
		    				    }
		    				}
		    			}
	
		    		    Thread.sleep(60*1000); //wait for 1 minute
	    			}
	    			catch (Throwable ex)
	    			{
	    				Log.log.error(ex,ex);
	    			}
    			}
    		}
    	}.start();
    }

} // LockUtils
