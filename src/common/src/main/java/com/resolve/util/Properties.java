/******************************************************************************
* (C) Copyright 2014
*
* Resolve Systems, LLC
*
* All rights reserved
* This software is distributed under license and may not be used, copied,
* modified, or distributed without the express written permission of
* Resolve Systems, LLC.
*
******************************************************************************/

package com.resolve.util;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import com.resolve.util.FileUtils;

public class Properties extends java.util.Properties
{
    private static final long serialVersionUID = -8567026053528243049L;
	File file = null;

    public Properties()
    {
    } // Properties

    public Properties(String filename)
    {
        file = FileUtils.getFile(filename);
        if (file != null)
        {
            load();
        }
    } // Properties

    public Properties(File file)
    {
        this.file = file;
        if (file != null)
        {
            load();
        }
    } // Properties

    public String toString()
    {
        String result = "";

        for (Iterator i = this.entrySet().iterator(); i.hasNext();)
        {
            Map.Entry entry = (Map.Entry) i.next();
            String key = (String) entry.getKey();
            String value = (String) entry.getValue();

            result += key.toUpperCase() + "=" + value;
            if (i.hasNext())
            {
                result += "\n";
            }
        }

        return result;
    } // toString

    public Map<String, String> toMapString()
    {
        Map<String, String> result = new HashMap<String, String>();

        for (Iterator i = this.entrySet().iterator(); i.hasNext();)
        {
            Map.Entry entry = (Map.Entry) i.next();
            result.put((String) entry.getKey(), (String) entry.getValue());
        }

        return result;
    } // toMapString

    public void setFilename(String filename)
    {
        file = FileUtils.getFile(filename);
    } // setFilename

    public boolean load(String filename)
    {
        boolean result = false;

        setFilename(filename);
        if (file != null)
        {
            result = load();
        }
        return result;
    } // load

    public boolean load()
    {
        boolean result = false;
        FileInputStream fis = null;
        try
        {
            if (file != null && file.exists())
            {
                fis = new FileInputStream(file);

                load(fis);
                result = true;
            }
        }
        catch (IOException e)
        {
            Log.log.warn("Failed to load properties: " + e.getMessage());
        }
        finally
        {
            if (fis != null)
            {
                try
                {
                    fis.close();
                }
                catch (Exception e)
                {
                    // do nothing
                }
            }
        }
        return result;
    } // load

    public boolean save()
    {
        boolean result = false;
        FileOutputStream fos = null;
        try
        {
            fos = new FileOutputStream(file);

            store(fos, "");
            result = true;
        }
        catch (IOException e)
        {
            Log.log.warn("Failed to save properties: " + e.getMessage());
        }
        finally
        {
            if (fos != null)
            {
                try
                {
                    fos.close();
                }
                catch (Exception e)
                {
                    // do nothing
                }
            }
        }

        return result;
    } // save

    public String get(String key)
    {
        return getProperty(key);
    } // get

    public String get(String key, String defaultvalue)
    {
        return getProperty(key, defaultvalue);
    } // get

    public String set(String key, String value)
    {
        return (String) setProperty(key, value);
    } // set

    public String setDefault(String key, String defaultValue)
    {
        String result = get(key);
        if (result == null)
        {
            result = set(key, defaultValue);
        }
        return result;
    } // setDefault

    public String getReplacement(Object key)
    {
        String value = (String) get(key);
        if (value != null)
        {
            while (value.matches(".*(?<!\\\\)\\$\\{.*\\}.*"))
            {
                String replaceValue = "";
                String replacePattern = "${";
                Pattern pattern = Pattern.compile(".*(?<!\\\\)\\$\\{(.+?)\\}.*");
                Matcher matcher = pattern.matcher(value);
                if (matcher.matches())
                {
                    replacePattern = matcher.group(1);
                    replaceValue = get(replacePattern);
                    if (replaceValue == null)
                    {
                        replaceValue = "";
                    }
                }
                value = value.replace("${" + replacePattern + "}", replaceValue);
            }
            value = value.replaceAll("\\\\\\$", "\\$");
        }

        return value;
    }

    public void trimProperties()
    {
        for (Object key : this.keySet())
        {
            Object value = this.get(key);
            if (value instanceof String)
            {
                value = ((String) value).trim();
                this.put(key, value);
            }
        }
    }

} // Properties
