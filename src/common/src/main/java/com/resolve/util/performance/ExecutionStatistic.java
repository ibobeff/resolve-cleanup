package com.resolve.util.performance;

import java.time.LocalDateTime;

import com.resolve.util.performance.datasources.ExecutionStep;
import com.resolve.util.performance.datasources.ExecutionStepPhase;

public class ExecutionStatistic {

	private String messageId;

	private String runbookId;

	private String actionTaskId;
	
	private String methodName;

	private ExecutionStep step;

	private ExecutionStepPhase phase;

	private LocalDateTime created;

	private long duration;

	private long totalDuration;

	public ExecutionStatistic(String messageId, String runbookId, String actionTaskId, String methodName, ExecutionStep step,
			ExecutionStepPhase phase, LocalDateTime created, long duration, long totalDuration) {
		this.messageId = messageId;
		this.runbookId = runbookId;
		this.actionTaskId = actionTaskId;
		this.methodName = methodName;
		this.step = step;
		this.phase = phase;
		this.created = created;
		this.duration = duration;
		this.totalDuration = totalDuration;
	}

	public String getMessageId() {
		return messageId;
	}

	public void setMessageId(String messageId) {
		this.messageId = messageId;
	}

	public String getRunbookId() {
		return runbookId;
	}

	public void setRunbookId(String runbookId) {
		this.runbookId = runbookId;
	}

	public String getActionTaskId() {
		return actionTaskId;
	}

	public void setActionTaskId(String actionTaskId) {
		this.actionTaskId = actionTaskId;
	}

	public ExecutionStep getStep() {
		return step;
	}

	public void setStep(ExecutionStep step) {
		this.step = step;
	}

	public ExecutionStepPhase getPhase() {
		return phase;
	}

	public void setPhase(ExecutionStepPhase phase) {
		this.phase = phase;
	}

	public LocalDateTime getCreated() {
		return created;
	}

	public void setCreated(LocalDateTime created) {
		this.created = created;
	}

	public long getDuration() {
		return duration;
	}

	public void setDuration(long duration) {
		this.duration = duration;
	}

	public long getTotalDuration() {
		return totalDuration;
	}

	public void setTotalDuration(long totalDuration) {
		this.totalDuration = totalDuration;
	}

    public String getMethodName()
    {
        return methodName;
    }

    public void setMethodName(String methodName)
    {
        this.methodName = methodName;
    }
	

}