/******************************************************************************
* (C) Copyright 2014
*
* Resolve Systems, LLC
*
* All rights reserved
* This software is distributed under license and may not be used, copied,
* modified, or distributed without the express written permission of
* Resolve Systems, LLC.
*
******************************************************************************/

package com.resolve.util;

import java.io.File;
import java.io.IOException;
import java.lang.reflect.Method;
import java.util.HashMap;

import com.resolve.util.FileUtils;


public abstract class ConfigMap extends HashMap<String, ConfigDefinition>
{
    private static final long serialVersionUID = -2386937467142803099L;
	public final static String BOOLEAN = "BOOLEAN";
    public final static String INT = "INT";
    public final static String INTEGER = "INTEGER";
    public final static String LONG = "LONG";
    public final static String SECURE = "SECURE";
    public final static String ENCRYPT = "ENCRYPT";
    public final static String STRING = "STRING";

    public XDoc xdoc;

    public ConfigMap()
    {
    } // ConfigBase

    public ConfigMap(XDoc xdoc)
    {
        this.xdoc = xdoc;
    } // ConfigBase

    public abstract void load() throws Exception;

    public abstract void save() throws Exception;

    public void start() throws Exception
    {
    } // start

    public void stop() throws Exception
    {
    } // stop

    public void define(String name, String type, String location)
    {
        this.put(name, new ConfigDefinition(name, type, location));
    } // define

    public static boolean backup(String configFilename, int revisions) throws IOException
    {
        boolean result = false;

        for (int i = revisions; i > 0; i--)
        {
            int dstIdx = i;
            int srcIdx = i - 1;
            String dstFilename = configFilename + "." + dstIdx;
            String srcFilename;
            if (srcIdx > 0)
            {
                srcFilename = configFilename + "." + srcIdx;
            }
            else
            {
                srcFilename = configFilename;
            }

            File srcFile = FileUtils.getFile(srcFilename);
            if (srcFile.exists())
            {
                File dstFile = FileUtils.getFile(dstFilename);
                FileUtils.copyFile(srcFile, dstFile);
            }
        }
        result = true;

        return result;
    } // backup

    Object getAttribute(String name)
    {
        Object result = null;

        String methodName = "get" + StringUtils.capitalize(name);
        Method method;
        try
        {
            try
            {
                method = this.getClass().getMethod(methodName, null);
            }
            catch (NoSuchMethodException e)
            {
                // try isMethod for boolean types
                methodName = "is" + StringUtils.capitalize(name);
                method = this.getClass().getMethod(methodName, null);
            }

            if (method != null)
            {
                result = method.invoke(this);
            }
        }
        catch (Exception e)
        {
            Log.log.warn("Unable to find getter for " + e.getMessage());
        }

        return result;
    } // getAttribute

    void setAttribute(String name, String type, Object value)
    {
        String methodName = "set" + StringUtils.capitalize(name);

        try
        {

            Method method = null;
            Class[] paramsClass = new Class[1];
            if (type.equalsIgnoreCase(BOOLEAN))
            {
                paramsClass[0] = boolean.class;
            }
            else if (type.equalsIgnoreCase(INT) || type.equalsIgnoreCase(INTEGER))
            {
                paramsClass[0] = int.class;
            }
            else if (type.equalsIgnoreCase(LONG))
            {
                paramsClass[0] = long.class;
            }
            else if (type.equalsIgnoreCase(STRING))
            {
                paramsClass[0] = String.class;
            }

            method = this.getClass().getMethod(methodName, paramsClass);
            if (method != null)
            {
                method.invoke(this, value);
            }
        }
        catch (Exception e)
        {
            Log.log.warn("Unable to find setter for " + e.getMessage());
        }

    } // setAttribute

    public void loadAttributes()
    {
        for (ConfigDefinition def : this.values())
        {
            loadAttribute(def);
        }
    } // loadAttributes

    public void loadAttribute(String name)
    {
        ConfigDefinition def = this.get(name);
        loadAttribute(def);
    } // loadAttribute

    public void loadAttribute(ConfigDefinition def) {
        try {
        	Object attribObj = getAttribute(def.getName());
        	
    		if (def.getType().equalsIgnoreCase(BOOLEAN)) {
    			setAttribute(def.getName(), BOOLEAN, xdoc.getBooleanValue(def.getLocation(), (Boolean)attribObj));
    		} else if (def.getType().equalsIgnoreCase(INT) || def.getType().equalsIgnoreCase(INTEGER)) {
    			setAttribute(def.getName(), INTEGER, xdoc.getIntValue(def.getLocation(), (Integer)attribObj));
    		} else if (def.getType().equalsIgnoreCase(LONG)) {
    			setAttribute(def.getName(), LONG, xdoc.getLongValue(def.getLocation(), (Long)attribObj));
    		} else if (def.getType().equalsIgnoreCase(SECURE) || def.getType().equalsIgnoreCase(ENCRYPT)) {
    			setAttribute(def.getName(), STRING, xdoc.getSecureStringValue(def.getLocation(), (String)attribObj));
    		} else {
    			setAttribute(def.getName(), STRING, xdoc.getStringValue(def.getLocation(), (String)attribObj));
    		}
        }  catch (Exception e) {
            Log.log.error("Failed to load configuration attributes: " + def + " - " + e.getMessage(), e);
        }
    } // loadAttribute

    public void saveAttributes()
    {
        for (ConfigDefinition def : this.values())
        {
            saveAttribute(def);
        }
    } // saveAttributes

    public void saveAttribute(ConfigDefinition def) {
        try {
        	Object attribObj = getAttribute(def.getName());
        	
        	if (attribObj == null) {
        		return;
        	}
        	
            if (def.getType().equalsIgnoreCase(BOOLEAN)) {
                xdoc.setBooleanValue(def.getLocation(), (Boolean)attribObj);
            } else if (def.getType().equalsIgnoreCase(INT) || def.getType().equalsIgnoreCase(INTEGER)) {
                xdoc.setIntValue(def.getLocation(), (Integer)attribObj);
            } else if (def.getType().equalsIgnoreCase(LONG)) {
                xdoc.setLongValue(def.getLocation(), (Long)attribObj);
            } else if (def.getType().equalsIgnoreCase(SECURE) || def.getType().equalsIgnoreCase(ENCRYPT)) {
                xdoc.setSecureStringValue(def.getLocation(), (String)attribObj);
            } else {
            	// String
                xdoc.setStringValue(def.getLocation(), (String)attribObj);
            }
        }
        catch (Exception e)
        {
            Log.log.error("Failed to save configuration attributes: " + def + " - " + e.getMessage(), e);
        }
    } // saveAttribute
    
    public static File getFile(String location)
    {
        return FileUtils.getFile(location);
    }

} // ConfigMap

class ConfigDefinition
{
    String name;
    String type;
    String location;

    public ConfigDefinition(String name, String type, String location)
    {
        this.name = name;
        this.type = type;
        this.location = location;
    } // ConfigDefinition

    public String toString()
    {
        return "[ name=" + name + ", type=" + type + ", location=" + location + " ]";
    } // toString

    public String getName()
    {
        return name;
    }

    public void setName(String name)
    {
        this.name = name;
    }

    public String getType()
    {
        return type;
    }

    public void setType(String type)
    {
        this.type = type;
    }

    public String getLocation()
    {
        return location;
    }

    public void setLocation(String location)
    {
        this.location = location;
    }

} // ConfigDefinition
