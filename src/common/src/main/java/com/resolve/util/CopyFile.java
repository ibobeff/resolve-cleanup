/******************************************************************************
* (C) Copyright 2014
*
* Resolve Systems, LLC
*
* All rights reserved
* This software is distributed under license and may not be used, copied,
* modified, or distributed without the express written permission of
* Resolve Systems, LLC.
*
******************************************************************************/

package com.resolve.util;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;

import com.resolve.util.FileUtils;

public class CopyFile
{
    static int buffersize = 8192;
    static int revisions = 2;

    public static void copyFile(String srcFilename, String dstFilename) throws IOException
    {
        File src = FileUtils.getFile(srcFilename);
        File dst = FileUtils.getFile(dstFilename);

        copyFile(src, dst);
    } // copyFile

    public static void copyFile(File src, File dst) throws IOException
    {
        // check if source file exists
        if (src.exists())
        {
            InputStream in = new FileInputStream(src);
            OutputStream out = new FileOutputStream(dst);

            // Transfer bytes from in to out
            byte[] buf = new byte[buffersize];
            int len;
            while ((len = in.read(buf)) > 0)
            {
                out.write(buf, 0, len);
            }
            in.close();
            out.close();
        }
    } // copyFile

    public static boolean backup(String configfile) throws IOException
    {
        boolean result = false;

        for (int i = revisions; i > 0; i--)
        {
            int dstIdx = i;
            int srcIdx = i - 1;
            String dstFilename = configfile + "." + dstIdx;
            String srcFilename;
            if (srcIdx > 0)
            {
                srcFilename = configfile + "." + srcIdx;
            }
            else
            {
                srcFilename = configfile;
            }

            CopyFile.copyFile(srcFilename, dstFilename);
            result = true;
        }

        return result;
    } // backup

    public static int getBuffersize()
    {
        return buffersize;
    }

    public static void setBuffersize(int buffersize)
    {
        CopyFile.buffersize = buffersize;
    }

    public static int getRevisions()
    {
        return revisions;
    }

    public static void setRevisions(int count)
    {
        revisions = count;
    }
} // CopyFile
