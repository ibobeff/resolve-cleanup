/******************************************************************************
* (C) Copyright 2014
*
* Resolve Systems, LLC
*
* All rights reserved
* This software is distributed under license and may not be used, copied,
* modified, or distributed without the express written permission of
* Resolve Systems, LLC.
*
******************************************************************************/

package com.resolve.util;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.resolve.rsbase.MainBase;
import com.resolve.util.FileUtils;

/**
 * This class writes and reads MCP log file.
 *
 */
public class MCPLog
{
    public static final String TMP_FOLDER = MainBase.main.getResolveHome() + "/tmp/";

    private File logFile;
    private List<Map<String, String>> data = new ArrayList<Map<String, String>>();

    public static Map<String, String> logConfig = new HashMap<String, String>();

    private final String defaultItem;

    static
    {
        logConfig.put(MCPConstants.MCP_FIELD_KEY_START + "#" + MCPConstants.MCP_FIELD_VALUE_SUCCESS, MCPConstants.MCP_COMPONENT_STATUS_UP);
        logConfig.put(MCPConstants.MCP_FIELD_KEY_START + "#" + MCPConstants.MCP_FIELD_VALUE_FAIL, MCPConstants.MCP_COMPONENT_STATUS_FAILED_TO_START);
        logConfig.put(MCPConstants.MCP_FIELD_KEY_STOP + "#" + MCPConstants.MCP_FIELD_VALUE_SUCCESS, MCPConstants.MCP_COMPONENT_STATUS_DOWN);
        logConfig.put(MCPConstants.MCP_FIELD_KEY_STOP + "#" + MCPConstants.MCP_FIELD_VALUE_FAIL, MCPConstants.MCP_COMPONENT_STATUS_FAILED_TO_STOP);
        logConfig.put(MCPConstants.MCP_FIELD_KEY_STATUS + "#" + MCPConstants.MCP_FIELD_VALUE_UP, MCPConstants.MCP_COMPONENT_STATUS_UP);
        logConfig.put(MCPConstants.MCP_FIELD_KEY_STATUS + "#" + MCPConstants.MCP_FIELD_VALUE_DOWN, MCPConstants.MCP_COMPONENT_STATUS_DOWN);
    }

    /**
     * Constructor that will allow the caller to choose backup option.
     *
     * @param logFileName
     *            absolute path to the log file
     * @param backup
     *            if true takes a backup with current timestamp
     */
    public MCPLog(final String logFileName, final boolean backup)
    {
        this(logFileName, backup, true);
    }

    /**
     * Constructor that will allow the caller to choose backup option as well as delete.
     *
     * @param logFileName
     *            absolute path to the log file
     * @param backup
     *            if true takes a backup with current timestamp
     * @param delete
     *            if true deletes the file if it already exists
     */
    public MCPLog(final String logFileName, final boolean backup, final boolean delete)
    {
        this(logFileName, backup, true, null);
    }

    /**
     * Constructor that will allow the caller to choose backup option as well as delete.
     *
     * @param logFileName
     *            absolute path to the log file
     * @param backup
     *            if true takes a backup with current timestamp
     * @param delete
     *            if true deletes the file if it already exists
     * @param defaultItem
     *            some script may provide a default item for its running duration.
     */
    public MCPLog(final String logFileName, final boolean backup, final boolean delete, String defaultItem)
    {
        this.defaultItem = defaultItem;
        logFile = FileUtils.getFile(logFileName);
        try
        {
            if (logFile.exists())
            {
                if (backup)
                {
                    File backupLogFile = FileUtils.getFile(logFileName + "." + System.currentTimeMillis());
                    FileUtils.copyFile(logFile, backupLogFile);
                }
                if(delete)
                {
                    //delete the existing file so we have a fresh start
                    FileUtils.deleteQuietly(logFile);
                }
            }
        }
        catch (IOException e)
        {
            Log.log.error(e.getMessage(), e);
        }
    }

    /**
     * Constructor that will not take a backup.
     *
     * @param logFileName
     *            absolute path to the log file
     */
    public MCPLog(final String logFileName)
    {
        this(logFileName, false);
    }

    /**
     * Default Constructor.
     *
     */
    public MCPLog()
    {
        this.defaultItem = null;
    }

    /**
     * This method will load a properly formatted log string.
     *
     * @param data
     */
    public void load(String logs)
    {
        data = StringUtils.stringToListOfMap(logs, StringUtils.VALUESEPARATOR, StringUtils.FIELDSEPARATOR, MCPConstants.LINE_SEPARATOR);
    }

    /**
     * This method will load a properly formatted log string.
     *
     * @param data
     * @throws IOException
     */
    public void load(File file) throws IOException
    {
        if (file != null && file.exists())
        {
            String logs = FileUtils.readFileToString(file);
            data = StringUtils.stringToListOfMap(logs, StringUtils.VALUESEPARATOR, StringUtils.FIELDSEPARATOR, MCPConstants.LINE_SEPARATOR);
        }
    }

    public List<Map<String, String>> getData()
    {
        return data;
    }

    public void setData(List<Map<String, String>> data)
    {
        this.data = data;
    }

    /**
     * Get status of a code.
     * TODO need to do something better.
     *
     * @param itemCode
     * @return
     */
    public boolean verifyItemStatus(String code, String value)
    {
        boolean result = false;
        for (Map<String, String> item : data)
        {
            if (item.get(MCPConstants.MCP_FIELD_ITEM).equalsIgnoreCase(code))
            {
                String status = item.get(MCPConstants.MCP_FIELD_STATUS);
                if (status.equalsIgnoreCase(value))
                {
                    result = true;
                    break;
                }
            }
        }
        return result;
    }

    public String getComment(String code)
    {
        String result = null;
        for (Map<String, String> item : data)
        {
            if (item.get(MCPConstants.MCP_FIELD_ITEM).equalsIgnoreCase(code))
            {
                result = item.get(MCPConstants.MCP_FIELD_COMMENT);
                break;
            }
        }
        return result;
    }

    public static String parseComponentStatus(String statusResponse, String component)
    {
        String result = null;

        List<String> lines = StringUtils.readLines(statusResponse);
        for (String line : lines)
        {
            if (line.toLowerCase().contains(component.toLowerCase()))
            {
                String[] tmpStatus = line.split(":");
                if (tmpStatus.length == 2)
                {
                    result = tmpStatus[1].trim();
                }
                else
                {
                    result = "UNKNOWN";
                }
            }
        }

        return result;
    }

    /**
     * This method helps creating a string for component status that's used by MCP components.
     *
     * @param componentWithGuid name of the component with its GUID (e.g., RSCONTROL#3E9C9FECC89705319763CB46C772C39A)
     * @param componentGuid actual GUID if available otherwise UNDEFINED
     * @param statusKey for example MCPConstants.MCP_FIELD_KEY_STATUS
     * @param statusValue for example MCPConstants.MCP_FIELD_VALUE_UP
     * @return for example RSCONTROL#3E9C9FECC89705319763CB46C772C39A|=|STATUS#UP
     */
    public static String synthesizeComponentStatus(String componentWithGuid, String statusKey, String statusValue, boolean addLineSeparator)
    {
        StringBuilder result = new StringBuilder();
        if (StringUtils.isNotBlank(componentWithGuid))
        {
            result.append(componentWithGuid);
            result.append("|=|");
            result.append(statusKey);
            result.append("#");
            result.append(statusValue);
            if (addLineSeparator)
            {
                result.append("|&|");
            }
        }

        return result.toString();
    }

    /**
     * This method helps creating a string for component status that's used by MCP components.
     *
     * @param component name of the component (e.g., RSCONTROL)
     * @param componentGuid actual GUID if available otherwise UNDEFINED
     * @param statusKey for example MCPConstants.MCP_FIELD_KEY_STATUS
     * @param statusValue for example MCPConstants.MCP_FIELD_VALUE_UP
     * @return for example RSCONTROL#3E9C9FECC89705319763CB46C772C39A|=|STATUS#UP
     */
    public static String synthesizeComponentStatus(String component, String componentGuid, String statusKey, String statusValue, boolean addLineSeparator)
    {
        String result = null;
        if (StringUtils.isNotBlank(component))
        {
            if (StringUtils.isBlank(componentGuid))
            {
                componentGuid = MCPConstants.MCP_COMPONENT_GUID_UNDEFINED;
            }
            String componentWithGuid = component + "#" + componentGuid;
            result = synthesizeComponentStatus(componentWithGuid, statusKey, statusValue, addLineSeparator);
        }

        return result;
    }

    public void executeStart(String comment)
    {
        executeStart(defaultItem, comment);
    }
    public void executeStart(String item, String comment)
    {
        write(item, MCPConstants.MCP_FIELD_KEY_EXECUTE_START, comment);
    }

    public void executeEnd(String comment)
    {
        executeEnd(defaultItem, comment);
    }
    public void executeEnd(String item, String comment)
    {
        write(item, MCPConstants.MCP_FIELD_KEY_EXECUTE_END, comment);
    }

    public void info(String comment)
    {
        info(defaultItem, comment);
    }
    public void info(String item, String comment)
    {
        write(item, MCPConstants.MCP_LOG_LEVEL_INFO, comment);
    }

    public void warn(String comment)
    {
        warn(defaultItem, comment);
    }
    public void warn(String item, String comment)
    {
        write(item, MCPConstants.MCP_LOG_LEVEL_WARN, comment);
    }

    public void error(String comment)
    {
        error(defaultItem, comment);
    }
    public void error(String item, String comment)
    {
        write(item, MCPConstants.MCP_LOG_LEVEL_ERROR, comment);
    }

    public void fatal(String item, String comment)
    {
        write(item, MCPConstants.MCP_LOG_LEVEL_FATAL, comment);
    }

    public void write(String status, String comment)
    {
        write(defaultItem, status, comment);
    }

    public void write(String item, String status, String comment)
    {
        FileWriter fstream = null;
        BufferedWriter out = null;

        try
        {
            if (!logFile.exists())
            {
                logFile.createNewFile();
            }

            StringBuilder log = new StringBuilder();
            log.append(MCPConstants.MCP_FIELD_TIMESTAMP);
            log.append(StringUtils.VALUESEPARATOR);
            log.append(System.currentTimeMillis());
            log.append(StringUtils.FIELDSEPARATOR);

            log.append(MCPConstants.MCP_FIELD_ITEM);
            log.append(StringUtils.VALUESEPARATOR);
            log.append(item);
            log.append(StringUtils.FIELDSEPARATOR);

            log.append(MCPConstants.MCP_FIELD_STATUS);
            log.append(StringUtils.VALUESEPARATOR);
            log.append(status);
            log.append(StringUtils.FIELDSEPARATOR);

            log.append(MCPConstants.MCP_FIELD_COMMENT);
            log.append(StringUtils.VALUESEPARATOR);
            log.append(comment);
            //log.append(StringUtils.LINESEPARATOR);
            log.append(MCPConstants.LINE_SEPARATOR);

            fstream = new FileWriter(logFile, true);
            out = new BufferedWriter(fstream);
            out.write(log.toString());
        }
        catch (IOException e)
        {
            Log.log.error(e.getMessage(), e);
        }
        finally
        {
            try
            {
                if (out != null)
                {
                    out.close();
                }
                if (fstream != null)
                {
                    fstream.close();
                }
            }
            catch (IOException e)
            {
                //ignore!
            }
        }
    }

//    public static void main(String[] args)
//    {
//        Log.init();
//        MCPLog log = new MCPLog("c:/tmp/logs/mylog.log");
//        log.write("HELLO1", "GOOD", "Nothing");
//        log.write("HELLO2", "GOOD", "Nothing");
//        log.write("HELLO3", "GOOD", "Nothing");
//        log.write("HELLO4", "GOOD", "Nothing");
//    }
}
