/******************************************************************************
* (C) Copyright 2018
*
* Resolve Systems, LLC
*
* All rights reserved
* This software is distributed under license and may not be used, copied,
* modified, or distributed without the express written permission of
* Resolve Systems, LLC.
*
******************************************************************************/

package com.resolve.util;

public enum LicenseEnum {
	CHECK("CHECK", null),
	CREATE_DATE("CREATE_DATE", null),
	UPLOAD_DATE("UPLOAD_DATE", null),
	DELETE_DATE("DELETE_DATE", null),
	CUSTOMER("CUSTOMER", "customer"),
	NO_OF_CORE("NO_OF_CORE", "noOfCore"),
	END_USER_COUNT("END_USER_COUNT", "endUserCount"),
	IP_ADDRESS("IP_ADDRESS", "ip"),
	HOST_IP_ADDRESS("HOST_IP_ADDRESS", null),
	HA("HA", "ha"),
	DURATION("DURATION", "duration"),
	EXPIRATION_DATE("EXPIRATION_DATE", "exp"),
	DATE_FORMAT("yyyyMMdd", null),
	DATE_FORMAT_LONG("yyyy-MM-dd HH:mm:ss", null),
	DATE_FORMAT_MILLISECONDS("yyyyMMdd HH:mm:ss.SSS", null),
	DATE_FORMAT_MILLISECONDS_ZONE("yyyyMMdd HH:mm:ss.SSSZ", null),
	COMPONENT_RSCONTROL("RSCONTROL", null),
	COMPONENT_RSVIEW("RSVIEW", null),
	MAX_MEMORY("MAX_MEMORY", null),
	MAX_MEMORY_RSCONTROL("MAX_MEMORY_RSCONTROL", "maxMemoryRSCONTROL"),
	MAX_MEMORY_RSVIEW("MAX_MEMORY_RSVIEW", "maxMemoryRSVIEW"),
	GATEWAY("GATEWAY", "gateway"),
	GATEWAYS("GATEWAYS", null),
	GATEWAY_CODE("GATEWAY_CODE", null),
	GATEWAY_INSTANCE("GATEWAY_INSTANCE", null),
	GATEWAY_DURATION("GATEWAY_DURATION", null),
	GATEWAY_EXPIRATION("GATEWAY_EXPIRATION", null),
	RETURN_QUEUE("RETURN_QUEUE", null),
	SIGNATURE("SIGNATURE", null),
	INTERVALS("INTERVALS", "intervals"),
	EVENTSOURCES("EVENTSOURCES", "eventsources"),
	LICENSES_FROM("LICENSES_FROM", null),
	
	// license management constants
	
	LICENSE_KEY("LICENSE_KEY", null),
	LICENSE_KEYS("LICENSE_KEYS", null),
	LICENSE_TYPE("LICENSE_TYPE", "type"),
	LICENSE_TYPE_COMMUNITY("COMMUNITY", null),
	LICENSE_TYPE_COM("COM", null),
	LICENSE_TYPE_EVALUATION("EVALUATION", null),
	LICENSE_TYPE_STANDARD("STANDARD", null),
	LICENSE_TYPE_ENTERPRISE("ENTERPRISE", null),
	
	// DO NOT ADD ANY NEW LICENSE TYPES BELOW V2
	LICENSE_TYPE_V2("V2", null),
	
	// Banner (header) for license
	
	LICENSE_BANNER_DEFAULT("Runbook Automation", null),
	LICENSE_BANNER_COMMUNITY("Community Edition", null),
	LICENSE_BANNER_EVALUATION("UNLICENSED VERSION - PLEASE ENTER LICENSE KEY", null),
	LICENSE_BANNER_V2("Runbook Automation With Security Edition", null),
	
	// License Environment
	
	LICENSE_ENV("LICENSE_ENV", "env"),
	LICENSE_ENV_PRODUCTION("PRODUCTION", null),
	LICENSE_ENV_NON_PROD("NON PROD", null),
	LICENSE_ENV_UNKNOWN("UNKNOWN", null),
    
	// License RBC
	
	RBC_SOURCE("RBC_SOURCE", null),
	RBC_SOURCE_LC("rbc_source", null),
	RBC_QUEUE("RBC_QUEUE", null),
	RBC_QUEUE_LC("rbc_queue", null),
	RBC("RBC", null);
	
    private final String keygenName;
    private final String paramName;
    
    LicenseEnum(String keygenName, String paramName) {
        this.keygenName = keygenName;
        this.paramName = paramName;
    }
    
    public String getKeygenName() {
        return keygenName;
    }
    
    public String getParamName() {
        return paramName;
    }
}
