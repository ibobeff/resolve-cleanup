/******************************************************************************
 * (C) Copyright 2014
 *
 * Resolve Systems, LLC
 *
 * All rights reserved
 * This software is distributed under license and may not be used, copied,
 * modified, or distributed without the express written permission of
 * Resolve Systems, LLC.
 *
 ******************************************************************************/

package com.resolve.util;

import java.io.UnsupportedEncodingException;
import java.nio.charset.StandardCharsets;
import java.security.InvalidKeyException;
import java.security.spec.KeySpec;

import javax.crypto.Cipher;
import javax.crypto.KeyGenerator;
import javax.crypto.SecretKey;
import javax.crypto.SecretKeyFactory;
import javax.crypto.spec.DESKeySpec;
import javax.crypto.spec.DESedeKeySpec;
import javax.crypto.spec.SecretKeySpec;

import org.apache.commons.codec.binary.Base64;

/**
 * General purpose encryption/decryption utility. Currently works with
 * algorithms defined by EncryptionScheme.
 */
public final class Crypt {

	private byte[] keyBytesLocal = null;
	private boolean keyInitialized = false;
	private String keyBase = null;
	private EncryptionScheme scheme;
	private KeySpec keySpec;
	private SecretKeyFactory keyFactory;
	private KeyGenerator kgen;
	private SecretKeySpec skeySpec;
	private Cipher cipher;

	public enum EncryptionScheme {
		DESEDE("DESede"),
		DES("DES"), // NOTE: For DES, only first 24 characters of key is unique
		AES128("AES"),
		UNKNOWN("");

		private final String scheme;

		private EncryptionScheme(String scheme) {
			this.scheme = scheme;
		}

		private String getScheme() {
			return scheme;
		}
	}

	public Crypt(EncryptionScheme scheme, String key) throws Exception {
		keyBase = key;
		initKeyScheme(scheme);
	}

	public Crypt(EncryptionScheme scheme, byte[] key) throws Exception {
		keyBytesLocal = key;
		initKeyScheme(scheme);
	}

	public synchronized String encrypt(String unencryptedString) throws Exception {
		return encrypt(unencryptedString, this.scheme);
	}

	public synchronized String decrypt(String encryptedString) throws Exception {
		return decrypt(encryptedString, this.scheme, null);
	}

	public synchronized String decryptUTF8(String encryptedString) throws Exception {
		return decrypt(encryptedString, this.scheme, StandardCharsets.UTF_8.name());
	}

	private void initKeyScheme(EncryptionScheme scheme) throws Exception {
		this.scheme = scheme;

		switch (scheme) {
		case DESEDE:
		case DES:
			keyFactory = SecretKeyFactory.getInstance(scheme.getScheme());
			break;
		case AES128:
			if (this.keyBytesLocal == null) {
				Log.log.fatal("Terminate initiated");
				throw new Exception("keyBytesLocal is null. Terminate initiated");
			}

			kgen = KeyGenerator.getInstance(scheme.getScheme());
			kgen.init(128);

			break;
		case UNKNOWN:
		default:
		}

		cipher = Cipher.getInstance(this.scheme.getScheme());
	}

	private void initKeySession(String keySess) throws UnsupportedEncodingException, InvalidKeyException {
		switch (this.scheme) {
		case DESEDE:
			keySpec = new DESedeKeySpec(getKeyAsBytes(keySess));
			break;
		case DES:
			keySpec = new DESKeySpec(getKeyAsBytes(keySess));
			break;
		case AES128:
			skeySpec = new SecretKeySpec(this.keyBytesLocal, EncryptionScheme.AES128.getScheme());
			break;
		case UNKNOWN:
		default:
			throw new IllegalArgumentException("Encryption scheme not supported: " + scheme);
		}

		keyInitialized = true;
	}

	private byte[] getKeyAsBytes(String keySess) throws UnsupportedEncodingException {
		keySess = keySess == null ? StringUtils.EMPTY : keySess;

		String key = keySess + this.keyBase;
		return key.getBytes(StandardCharsets.UTF_8.name());
	}

	private synchronized String encrypt(String unencryptedString, EncryptionScheme scheme) throws Exception {
		if (StringUtils.isNotBlank(unencryptedString)) {
			if (keyInitialized == false) {
				initKeySession(null);
			}

			if (scheme == EncryptionScheme.AES128) {
				cipher.init(Cipher.ENCRYPT_MODE, skeySpec);
			} else {
				SecretKey key = keyFactory.generateSecret(keySpec);
				cipher.init(Cipher.ENCRYPT_MODE, key);
			}

			byte[] cleartext = unencryptedString.getBytes(StandardCharsets.UTF_8.name());
			byte[] ciphertext = cipher.doFinal(cleartext);
			return new String(Base64.encodeBase64(ciphertext));
		}

		return StringUtils.EMPTY;
	}

	private synchronized String decrypt(String encryptedString, EncryptionScheme scheme, String format)
			throws Exception {
		if (StringUtils.isNotBlank(encryptedString)) {
			if (keyInitialized == false) {
				initKeySession(null);
			}

			if (scheme == EncryptionScheme.AES128) {
				cipher.init(Cipher.DECRYPT_MODE, skeySpec);
			} else {
				SecretKey key = keyFactory.generateSecret(keySpec);
				cipher.init(Cipher.DECRYPT_MODE, key);
			}

			byte[] cleartext = Base64.decodeBase64(encryptedString);
			byte[] ciphertext = cipher.doFinal(cleartext);

			return StringUtils.isEmpty(format) ? new String(ciphertext) : new String(ciphertext, format);
		}

		return StringUtils.EMPTY;
	}
}
