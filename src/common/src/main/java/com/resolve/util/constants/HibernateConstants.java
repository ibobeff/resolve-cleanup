/******************************************************************************
* (C) Copyright 2014
*
* Resolve Systems, LLC
*
* All rights reserved
* This software is distributed under license and may not be used, copied,
* modified, or distributed without the express written permission of
* Resolve Systems, LLC.
*
******************************************************************************/

package com.resolve.util.constants;

import java.text.DateFormat;
import java.text.SimpleDateFormat;

public interface HibernateConstants
{
    //regular expressions
    public String REGEX_ALPHANUMERIC_WITH_SPACE = "[\\p{Alnum}_\\s]*";//aphanum, _, space
    // Combination of LDAP DN in user and group name RFCs 2253, 2254 and valid characters for AD
    public String REGEX_LDAP_AD_DN = "[^/\\\\\\[=\\]:;\\|\\+\\*\\?<>\"@]*";
    
    public String REGEX_ALPHANUMERIC_NO_SPACE = "[\\p{Alnum}_&]*";//aphanum, _, &
    public String REGEX_ALPHANUMERIC_WITH_UNDERSCORE_30CHARS = "^(?=.*[A-Za-z])[\\p{Alnum}_]{1,30}$";//aphanum, _, limit to 25 chars only, atleast 1 alphabet
    //refer - http://www.mkyong.com/regular-expressions/how-to-validate-email-address-with-regular-expression/

    public String REGEX_ALPHANUMERIC_WITH_UNDERSCORE_DOT_DASH = "[\\p{Alnum}_.-]*";//aphanum, _, ., -
    public String REGEX_ALPHANUMERIC_WITH_UNDERSCORE_DOT_DASH_SPACE = "[\\p{Alnum}_\\s.-]*";//aphanum, _, ., -, space

    
    public String REGEX_WIKI_DOCUMENT_NAME_AND_FULLNAME =  "(?:[a-zA-Z0-9 _-]+\\.)?[a-zA-Z0-9 _-]+";
    public String REGEX_WIKI_DOCUMENT_FULLNAME =  "[a-zA-Z0-9 _-]+\\.[a-zA-Z0-9 _-]+";

//    refer - http://social.technet.microsoft.com/Forums/exchange/en-US/69f393aa-d555-4f8f-bb16-c636a129fc25/what-are-valid-and-invalid-email-address-characters?forum=exchangesvradminlegacy
    public String REGEX_EMAIL_VALIDATION = "^[_A-Za-z0-9-\\+]+(\\.[_A-Za-z0-9-]+)*@[A-Za-z0-9-]+(\\.[A-Za-z0-9]+)*(\\.[A-Za-z]{2,})$";
    
    // refer - http://stackoverflow.com/questions/13876955/regex-that-allows-chinese-characters
    //allows characters in any language
    public String REGEX_USERNAME_VALIDATION = "^(?=[a-zA-Z0-9])[\\p{Alnum}_.@-]{1,40}$";
    public String REGEX_ACTIONTASK_FULLNAME_VALIDATION = "[\\p{Alnum}-./#_\\s]*";
    public String REGEX_ACTIONTASK_NAME_VALIDATION = "[\\p{Alnum}_\\s-]*";
    public String REGEX_ACTIONTASK_NAMESPACE_VALIDATION = "[\\p{Alnum}_\\s.-]*";
    
    public String REGEX_USER_FIRST_LAST_NAME = "[\\p{Alnum}.$'#_\\s]*";
    public String REGEX_TAG_NAME = "[^\\dA-Za-z]";
    
    //attributes same as HibernateUtil.java 
    public String WIKI_DOCUMENT_CACHE_REGION = "WIKI_DOCUMENT_CACHE_REGION";

    public String GROOVY_EXECEPTION = "__groovyException__";
    public String GROOVY_OUTPUT_RESULT = "__groovyOutput__";
    
    public String DEFAULT_MENUSET_DISABLED = "__defaultMenuSetFlag__";
    public String MENU_SETS = "__menuSets__";
    
    public DateFormat dateWithoutTime = new SimpleDateFormat("MMM dd yyyy");
    public DateFormat dateWithoutYear = new SimpleDateFormat("MMM dd, HH:mm");

    public String ERROR_EXCEPTION_KEY = "errorException";
    
    public String SYS_ID = "sys_id";
    public String SYSUPDATEDON = "sysUpdatedOn";
    public String SYSCREATEDON = "sysCreatedOn";
    public String SYSUPDATEDBY = "sysUpdatedBy";
    public String SYSCREATEDBY = "sysCreatedBy";
    public String SYS_UPDATED_ON = "sys_updated_on";
    public String SYS_CREATED_ON = "sys_created_on";
    public String SYS_UPDATED_BY = "sys_updated_by";
    public String SYS_CREATED_BY = "sys_created_by";
    public String SYS_MOD_COUNT = "sys_mod_count";
    public String SYS_PERMISSION = "sys_perm";
    public String SYS_ORGANIZATION = "sys_org";

    //custom table - WorkflowMetaService.java
    public static final String COLUMN_PREFIX = "u_";

    //from ClientConstants.java
    public final static String STATUS_SUCCESS = "SUCCESS";
    public final static String STATUS_FAILED = "FAILED";
    public final static String PROPERTY_PRODUCT_LOGO = "product.logo";
    public final static String BANNER_URL = "banner.url";
    public final static String MENU_TOOLBAR = "menu.toolbar";
    public final static String HELP_CONTEXT_URL = "help.context.url";
    public final static String HELP_CONTEXT_NAMESPACE = "help.context.namespace";
    public final static String COPY_USER = "Copy User";
    public final static String COPY_GROUP = "Copy Group";
    public final static String EDIT_VIEW = "Add / Edit View";
    
    //json node name for the meta_field_properties --> section_column
    public String META_FIELD_PROP_SECTION_COLUMN_CONTROLS = "controls";

    //from SharedConstants
    public String EXECUTE_RUNBOOK = "EXECUTE_RUNBOOK";

    //from the TeamBaseModel , but maybe generci
    public static final String U_OWNER = "u_owner";

    //from ConstantValus.java
    public String VIEW_LOOKUP = "view_lookup";
    public String WIKI_TEMPLATE_NAMESPACE_KEY = "template_namespace";
    public String WIKI_TEMPLATE_DOCNAME_KEY = "template_docname";
    public String WIKI_TEMPLATE_VALUES_KEY = "template_values";    
    public String USERNAME = "USERNAME";
    public String WIKI_DOCUMENT_NAME = "wikiDocument";
    public String WIKIDOC_STATS_COUNTER_NAME_KEY = "WikiDocStatsCounterName";
    public String WIKI_ADMIN = "admin";
    public String NAMESPACE_SYSTEM = "System";

    //constants for ExtFormSubmit
    public static final String EXT_FORM_SUBMIT_KEY = "__formSubmitDto";
    public static final String EXT_FORM_BUTTON_NAME_KEY = "BUTTON_NAME";
    public static final String EXT_FORM_VIEW_NAME_KEY = "FORM_VIEW_NAME";

    //wiki template constants -- WikiGuiConstants.java 
    public static final String WIKI_TEMPLATE_NAMESPACE = "Template";
    public static final String FORM_TEMPLATE_NAMESPACE = "TPL_";
    
    //social component types
//    public String SOCIAL_TYPE_ACTIONTASK = "actiontask";
//    public String SOCIAL_TYPE_DECISIONTREE = "decisiontree";
//    public String SOCIAL_TYPE_DOCUMENT = "document";
//    public String SOCIAL_TYPE_FORUM = "forum";
//    public String SOCIAL_TYPE_NAMESPACE = "namespace";
//    public String SOCIAL_TYPE_PROCESS = "process";
//    public String SOCIAL_TYPE_RSS = "rss";
//    public String SOCIAL_TYPE_RUNBOOK = "runbook";
//    public String SOCIAL_TYPE_TEAM = "team";
//    public String SOCIAL_TYPE_USER = "user";
//    public String SOCIAL_TYPE_WORKSHEET = "worksheet";
    
    //for archive 
    public String ARCHIVE_CLASS_NAME = "ARCHIVE_CLASS_NAME";
    public String ARCHIVE_ATTRIBUTE_NAME = "ARCHIVE_ATTRIBUTE_NAME";
    public String ARCHIVE_SYS_ID = "ARCHIVE_SYS_ID";

    //Columns used for the File upload custom table
    public static final String FU_UFILENAME = "u_filename";
    public static final String FU_USIZE = "u_size";
    public static final String FU_UCONTENT = "u_content";

    //constants for file uploads
    public final static String IS_MULTIPLE_FILE = "isMultipleFiles";
    public final static String IS_GLOBAL = "isGlobal";
    
    //from SocialConstants
    public final static String AUTO_WIDTH_GRID_COLUMNS = "AUTO_WIDTH_GRID_COLUMNS";
    public final static String TEAMS_RIGHTS_EDIT = "teams.rights.edit";
    public final static String TEAMS_RIGHTS_ADMIN = "teams.rights.admin";
    public final static String TEAMS_RIGHTS_ACCESS = "teams.rights.access";
    public final static String TEAMS_RIGHTS_POST = "teams.rights.post";

    public final static String FORUMS_RIGHTS_EDIT = "forums.rights.edit";
    public final static String FORUMS_RIGHTS_ADMIN = "forums.rights.admin";
    public final static String FORUMS_RIGHTS_READ = "forums.rights.read";
    public final static String FORUMS_RIGHTS_POST = "forums.rights.post";

    public final static String PROCESS_RIGHTS_EDIT = "process.rights.edit";
    public final static String PROCESS_RIGHTS_ADMIN = "process.rights.admin";
    public final static String PROCESS_READ_ROLES = "process.rights.access";
    public final static String PROCESS_READ_POST = "process.rights.post";

    public final static String RSS_RIGHTS_EDIT = "rss.rights.edit";
    public final static String RSS_RIGHTS_ADMIN = "rss.rights.admin";
    public final static String RSS_RIGHTS_READ = "rss.rights.read";
    public final static String RSS_RIGHTS_POST = "rss.rights.post";
  //generic constants 
    public String TABLENAME = "TABLENAME";
    public String COLUMNNAMES = "COLUMNS";
    public String ACTION = "ACTION";   
    
    public String CONTENT_TYPE = "contentType";
    public String SECTION_ID = "sectionId";
    public String SECTION_TITLE = "sectionTitle";
    public String GRID_TABLENAME = "gridTableName";
    public String GRID_VIEWNAME = "gridViewName";
    public String GRID_QUERY = "gridQuery";
    public String META_FORMNAME = "metaFormName";
    public String FORM_TABLENAME = "formTableName";
    public String FORM_VIEWNAME = "formViewName";
    public String FORM_QUERY = "formQuery";
    public String REPORTS_REPORTNAME = "reportName";
    public String IFRAME_HEIGHT = "height";
    public String URL = "url";
    public String RELATIONS_TABLE_NAME = "relationTableName";
    public String ATTACH_FILE_NAME = "attachFileName";

    public String WIKIDOC_FULL_NAME_KEY = "WIKIDOC_FULL_NAME_KEY";
    public String WIKIDOCS_TO_INDEX = "WIKIDOCS_TO_INDEX";
    public String INDEX_COMPONENTS = "INDEX_COMPONENTS";
    public String WIKIDOCS_TO_RATE_KEY = "wikidocsToRate";
    public String WIKIDOCS_STAR_RATING = "WIKIDOCS_STAR_RATING";
    public String WIKIDOCS_STAR_RATING_COUNT = "WIKIDOCS_STAR_RATING_COUNT";
    public String WIKIDOCS_SEARCH_WEIGHT_VALUE = "WIKIDOCS_SEARCH_WEIGHT_VALUE";
    public final static String INDEX_WIKIDOCUMENTS = "INDEX_ALL_WIKIDOCUMENTS";
    public final static String INDEX_ATTACHMENTS = "INDEX_ALL_ATTACHMENTS";
    public final static String INDEX_ACTIONTASK = "INDEX_ALL_ACTIONTASK";
    public final static String INDEX_ACTIONTASK_PROPERTIES = "INDEX_ALL_ACTIONTASK_PROPERTIES";
    public final static String INDEX_CUSTOM_FORMS = "INDEX_ALL_CUSTOM_FORMS";
    public final static String PURGE_ALL_INDEXES = "PURGE_ALL_INDEXES";
    public String WIKIDOC_REVIEW_STAR_RATING = "WIKIDOC_REVIEW_STAR_RATING";
    public String WIKIDOC_FEEDBACK = "WIKIDOC_FEEDBACK";

    //InfoBar Macro names
    public String INFOBAR_DISPLAY_HEIGHT = "infobar_height";
    public String INFOBAR_DISPLAY_SOCIAL_TAB = "infobar_social";
    public String INFOBAR_DISPLAY_FEEDBACK_TAB = "infobar_feedback";
    public String INFOBAR_DISPLAY_RATING_TAB = "infobar_rating";
    public String INFOBAR_DISPLAY_ATTACHMENTS_TAB = "infobar_attachments";
    public String INFOBAR_DISPLAY_HISTORY_TAB = "infobar_history";
    public String INFOBAR_DISPLAY_TAGS_TAB = "infobar_tags";
    public String INFOBAR_DISPLAY_PAGEINFO_TAB = "infobar_pageInfo";
    public String IMAGE_HEIGHT = "height";
    public String IMAGE_WIDTH = "width";
    public String IMAGE_ZOOM = "zoom";
    
}