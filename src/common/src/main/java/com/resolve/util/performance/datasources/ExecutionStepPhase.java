package com.resolve.util.performance.datasources;

public enum ExecutionStepPhase {

	START, END

}