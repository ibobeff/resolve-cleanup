/******************************************************************************
* (C) Copyright 2014
*
* Resolve Systems, LLC
*
* All rights reserved
* This software is distributed under license and may not be used, copied,
* modified, or distributed without the express written permission of
* Resolve Systems, LLC.
*
******************************************************************************/

package com.resolve.util;

import java.beans.BeanInfo;
import java.beans.Introspector;
import java.beans.PropertyDescriptor;
import java.lang.management.ManagementFactory;
import java.lang.management.MemoryUsage;
import java.lang.management.ThreadMXBean;
import java.lang.reflect.Field;
import java.lang.reflect.Method;
import java.security.SecureRandom;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import org.apache.commons.lang3.RandomStringUtils;

public class JavaUtils
{
    public static String printMethods(String className)
    {
        String result = "";

        Method[] methods;
        try
        {
            methods = Class.forName(className).getMethods();
            for (int i = 0; i < methods.length; i++)
            {
                result += methods[i].toGenericString() + "\n";
            }
        }
        catch (ClassNotFoundException e)
        {
            result = "ERROR: Class not found. " + e.getMessage();
        }

        return result;
    } // printMethods

    public static String printMethods(Object obj)
    {
        String result = "";

        Method[] methods = obj.getClass().getMethods();
        for (int i = 0; i < methods.length; i++)
        {
            result += methods[i].toGenericString() + "\n";
        }

        return result;
    } // printMethods

    public static String printFields(String className)
    {
        String result = "";

        Field[] fields;
        try
        {
            fields = Class.forName(className).getFields();
            for (int i = 0; i < fields.length; i++)
            {
                result += fields[i].toGenericString() + "\n";
            }
        }
        catch (ClassNotFoundException e)
        {
            result = "ERROR: Class not found. " + e.getMessage();
        }

        return result;
    } // printFields

    public static String printFields(Object obj)
    {
        String result = "";

        Field[] fields = obj.getClass().getFields();
        for (int i = 0; i < fields.length; i++)
        {
            result += fields[i].toGenericString() + "\n";
        }

        return result;
    } // printFields

    public static String jvmInfo()
    {
        StringBuilder sb = new StringBuilder();

        sb.append("\n");
        sb.append("====== JVM INFO ======");
        sb.append("\n");

        sb.append("# memory: ");
        sb.append(" max: ");
        sb.append(toMb(Runtime.getRuntime().maxMemory()));
        sb.append(", total: ");
        sb.append(toMb(Runtime.getRuntime().totalMemory()));
        sb.append(", free: " + toMb(Runtime.getRuntime().freeMemory()));
        // sb.append((int) (Runtime.getRuntime().freeMemory() * 100 /
        // Runtime.getRuntime().totalMemory()));
        long free = (Runtime.getRuntime().maxMemory() - Runtime.getRuntime().totalMemory()) + Runtime.getRuntime().freeMemory();
        int value = (int) (free * 100 / Runtime.getRuntime().maxMemory());
        sb.append(value);
        sb.append("%");
        sb.append("\n");

        MemoryUsage heap = ManagementFactory.getMemoryMXBean().getHeapMemoryUsage();
        sb.append("# heap memory: ");
        sb.append(" init: ");
        sb.append(toMb(heap.getInit()));
        sb.append(", max: ");
        sb.append(toMb(heap.getMax()));
        sb.append(", comitted: " + toMb(heap.getCommitted()));
        sb.append(", used: " + toMb(heap.getUsed()));
        sb.append(", free: " + ((int) ((heap.getCommitted() - heap.getUsed()) * 100 / heap.getCommitted())) + "%");
        sb.append("\n");

        MemoryUsage nonheap = ManagementFactory.getMemoryMXBean().getNonHeapMemoryUsage();
        sb.append("# non heap memory: ");
        sb.append(" init:" + toMb(nonheap.getInit()));
        sb.append(", max:" + toMb(nonheap.getMax()));
        sb.append(", comitted: " + toMb(nonheap.getCommitted()));
        sb.append(", used: " + toMb(nonheap.getUsed()));
        sb.append("\n");

        sb.append("# procs: ");
        sb.append(Runtime.getRuntime().availableProcessors());
        sb.append("\n");

        ThreadMXBean thread = ManagementFactory.getThreadMXBean();
        sb.append("# thread peak: " + thread.getPeakThreadCount());
        sb.append(" live: " + thread.getThreadCount());
        sb.append(" daemon: " + thread.getDaemonThreadCount());
        sb.append("\n");

        sb.append("# os info: ");
        sb.append(" arch:" + ManagementFactory.getOperatingSystemMXBean().getArch());
        sb.append(", name:" + ManagementFactory.getOperatingSystemMXBean().getName());
        sb.append(", version:" + ManagementFactory.getOperatingSystemMXBean().getVersion());
        sb.append("\n");

        sb.append("# jvm info: ");
        sb.append(" vendor:" + ManagementFactory.getRuntimeMXBean().getVmVendor());
        sb.append(", name:" + ManagementFactory.getRuntimeMXBean().getVmName());
        sb.append(", version:" + ManagementFactory.getRuntimeMXBean().getVmVersion());
        sb.append("\n");
        sb.append("=======================");
        return sb.toString();
    } // jvmInfo

    public static Map getJvmInfoMap()
    {
        Map result = new LinkedHashMap();

        result.put("MEMORY_MAX", toMb(Runtime.getRuntime().maxMemory()));
        result.put("MEMORY_TOTAL", toMb(Runtime.getRuntime().totalMemory()));
        // result.put("MEMORY_FREE", toMb(Runtime.getRuntime().freeMemory()) +
        // " ("+((int) (Runtime.getRuntime().freeMemory() * 100 /
        // Runtime.getRuntime().totalMemory()))+") %");
        long free = (Runtime.getRuntime().maxMemory() - Runtime.getRuntime().totalMemory()) + Runtime.getRuntime().freeMemory();
        int value = (int) (free * 100 / Runtime.getRuntime().maxMemory());
        result.put("MEMORY_FREE", value + " %");

        MemoryUsage heap = ManagementFactory.getMemoryMXBean().getHeapMemoryUsage();
        result.put("HEAP_INIT", toMb(heap.getInit()));
        result.put("HEAL_MAX", toMb(heap.getMax()));
        result.put("HEAP_COMMITTED", toMb(heap.getCommitted()));
        result.put("HEAP_USED", toMb(heap.getUsed()));
        result.put("HEAP_FREE", (int) ((heap.getCommitted() - heap.getUsed()) * 100 / heap.getCommitted()) + "%");

        MemoryUsage nonheap = ManagementFactory.getMemoryMXBean().getNonHeapMemoryUsage();
        result.put("NON_HEAP_INIT", toMb(nonheap.getInit()));
        result.put("NON_HEAP_MAX", toMb(nonheap.getMax()));
        result.put("NON_HEAP_COMMITTED", toMb(nonheap.getCommitted()));
        result.put("NON_HEAP_USED", toMb(nonheap.getUsed()));

        result.put("PROCESSORS", Runtime.getRuntime().availableProcessors());

        ThreadMXBean thread = ManagementFactory.getThreadMXBean();
        result.put("THREAD_PEAK", thread.getPeakThreadCount());
        result.put("THREAD_LIVE", thread.getThreadCount());
        result.put("THREAD_DAEMON", thread.getDaemonThreadCount());

        result.put("OS_ARCHITECTURE", ManagementFactory.getOperatingSystemMXBean().getArch());
        result.put("OS_NAME", ManagementFactory.getOperatingSystemMXBean().getName());
        result.put("OS_VERSION", ManagementFactory.getOperatingSystemMXBean().getVersion());

        result.put("JVM_VENDOR", ManagementFactory.getRuntimeMXBean().getVmVendor());
        result.put("JVM_NAME", ManagementFactory.getRuntimeMXBean().getVmName());
        result.put("JVM_VERSION", ManagementFactory.getRuntimeMXBean().getVmVersion());
        return result;
    } // getJvmInfoMap

    public static String toMb(long size)
    {
        return size / 1024 / 1024 + "M ";
    } // toMb

    public static long generateUniqueNumber()
    {
        return new SecureRandom().nextLong();//return Math.round((new Date()).getTime() * Math.random());
    }

    public static String generateUniqueString()
    {
        return RandomStringUtils.randomAlphanumeric(32);
    }

//    public static void main(String[] args)
//    {
//        System.out.println(generateUniqueString());
//        System.out.println(generateUniqueString());
//        System.out.println(generateUniqueString());
//    }
    
    public static Map<Thread,Thread> getShutdownHooks()
    {
        Map<Thread,Thread> result = null;
        
        try
        {
            Class clazz = Class.forName("java.lang.ApplicationShutdownHooks");
            Field field = clazz.getDeclaredField("hooks");
            field.setAccessible(true);
            
            //hooks is a Map<Thread, Thread>
            Map<Thread,Thread> hooks = (Map<Thread,Thread>)field.get(null); 
            result = new HashMap(hooks);
        }
        catch (Exception e)
        {
            Log.log.warn(e.getMessage(), e);
        }
        
        return result;
    } // getShutdownHooks

    public static void removeShutdownHook(String threadName)
    {
        try
        {
            Class clazz = Class.forName("java.lang.ApplicationShutdownHooks");
            Field field = clazz.getDeclaredField("hooks");
            field.setAccessible(true);
            
            // hooks is a Map<Thread, Thread>
            Map<Thread,Thread> hooks = (Map<Thread,Thread>)field.get(null);
            
            List<Thread> removeThreads = new ArrayList<Thread>();
            for (Thread hook : hooks.values())
            {
                if (hook.getName().contains(threadName))
                {
                    removeThreads.add(hook);
                }
            }
            
            for (Thread hook : removeThreads)
            {
                Runtime.getRuntime().removeShutdownHook(hook);
            }
        }
        catch (Throwable t)
        {
            Log.log.error(t.getMessage(), t);
        }        
    } // removeShutdownHook
    
    public static void removeShutdownHook(Thread thread)
    {
        try
        {
            Runtime.getRuntime().removeShutdownHook(thread);
        }
        catch (Throwable t)
        {
            Log.log.error(t.getMessage(), t);
        }        
    } // removeShutdownHook
    
    /**
     * util to copy a java object
     *  
     * @param component
     * @return
     */
    public static Object copyObject(Object component)
    {
        String className = component.getClass().getName();
        Object result = null;
        
        try
        {
            //create an instance of the same object
            result = Class.forName(className);
            Map<String, Object> values = new HashMap<String, Object>();
            
            BeanInfo info = Introspector.getBeanInfo(component.getClass()); 
            PropertyDescriptor[] props = info.getPropertyDescriptors();  
            for (PropertyDescriptor pd : props) 
            {  
                Method m = pd.getReadMethod();
                if(m != null)
                {
                    String name = pd.getName();
                    if(name.equalsIgnoreCase("class") || name.equalsIgnoreCase("$type$"))
                    {
                        continue;
                    }
//                    System.out.println(name + " - " + pd.getPropertyType());
//                    System.out.println();// -->java.lang.ref.WeakReference;
                    Object value = null;
                    try
                    {
                       value = m.invoke(component);
                       values.put(name, value);
                    }
                    catch (Exception e)
                    {
                        Log.log.error("Issue with reading the values of the object", e);
                    }
                }
            }//end of for to get the values
            
            //set the values
            for (PropertyDescriptor pd : props)
            {
                //get the setter method for this property
                Method m = pd.getWriteMethod();
                if (m != null)
                {
                    String name = pd.getName();
                    if(name.equalsIgnoreCase("class") || name.equalsIgnoreCase("$type$"))
                    {
                        continue;
                    }
                    
                    //invoke the setter method
                    try
                    {
                        m.invoke(result, values.get(name));
                    }
                    catch (Exception e)
                    {
                        Log.log.error("Issue with writing the values of the object", e);
                    }
                }
            }
            
        }
        catch(Exception e)
        {
            Log.log.error("Unable to copy a comp", e);
        }

        return result;
    }
    
    public static <T> List<List<T>> chopped(List<T> list, final int batchSize)
    {
        List<List<T>> parts = new ArrayList<List<T>>();
        final int totalRecs = list.size();
        for (int count = 0; count < totalRecs; count += batchSize)
        {
            parts.add(new ArrayList<T>(list.subList(count, Math.min(totalRecs, count + batchSize))));
        }
        return parts;
    }
    
    /**
     * 
     * @param list - list of objects
     * @param start - start of the record # like 0, 50, 100, ...
     * @param limit - # of records on 1 page 
     * @return
     */
    public static <T> List<T> pagination(List<T> list, int start, int limit)
    {
        List<T> result = list;
        
        if(result != null && result.size() > 0)
        {
            //page size chops
            List<List<T>> choppedParts = chopped(list, limit);
            
            int pageNumber = 0;
            try
            {
                pageNumber = start / limit;
            }
            catch(Throwable t)
            {
                pageNumber = 0;
            }
            
            //pagination
            result = choppedParts.get(pageNumber);
        }
        
        return result;
    }
    
    public static String buildRegexForValidatingTheCronExpression()
    {
        // numbers intervals and regex
        Map<String, String> numbers = new HashMap<String, String>();
        numbers.put("min", "[0-5]?\\d");
        numbers.put("hour", "[01]?\\d|2[0-3]");
        numbers.put("day", "0?[1-9]|[12]\\d|3[01]");
        numbers.put("month", "[1-9]|1[012]");
        numbers.put("dow", "[0-6]");

        Map<String, String> field_re = new HashMap<String, String>();

        // expand regex to contain different time specifiers
        for (String field : numbers.keySet())
        {
            String number = numbers.get(field);
            String range = "(?:" + number + ")(?:-(?:" + number + ")(?:\\/\\d+)?)?";
            field_re.put(field, "\\*(?:\\/\\d+)?|" + range + "(?:," + range + ")*");
        }

        // add string specifiers
        String monthRE = field_re.get("month");
        monthRE = monthRE + "|jan|feb|mar|apr|may|jun|jul|aug|sep|oct|nov|dec";
        field_re.put("month", monthRE);

        String dowRE = field_re.get("dow");
        dowRE = dowRE + "|mon|tue|wed|thu|fri|sat|sun";
        field_re.put("dow", dowRE);

        StringBuilder fieldsReSB = new StringBuilder();
        fieldsReSB.append("^\\s*(")
                .append("$")
                .append("|#")
                .append("|\\w+\\s*=")
                .append("|")
                .append("(")
                .append(field_re.get("min")).append(")\\s+(")
                .append(field_re.get("hour")).append(")\\s+(")
                .append(field_re.get("day")).append(")\\s+(")
                .append(field_re.get("month")).append(")\\s+(")
                .append(field_re.get("dow"))
                .append(")")
                .append("\\s+)")
                .append("([^\\s]+)\\s+")
                .append("(.*)$");

        return fieldsReSB.toString();
    }
    
    public static Map<String, Object> covertObjectToMap(Object o)
    {
        Map<String, Object> result = new HashMap<String, Object>();
        
        try
        {
            BeanInfo info = Introspector.getBeanInfo(o.getClass());  
            PropertyDescriptor[] props = info.getPropertyDescriptors();  
            for (PropertyDescriptor pd : props) 
            {  
                //get the setter method for this property
                Method m = pd.getReadMethod();
                if(m != null)
                {
                    String name = pd.getName();
                    if(name.equalsIgnoreCase("class") || name.equalsIgnoreCase("$type$"))
                    {
                        continue;
                    }
//                    System.out.println(name + " - " + pd.getPropertyType());
//                    System.out.println();// -->java.lang.ref.WeakReference;
                    Object value = null;
                    try
                    {
                       value = m.invoke(o);
                       result.put(name, value);
                    }
                    catch (Exception e)
                    {
                        // ignore this
                    }
                }
            }
        }
        catch(Exception e)
        {
            Log.log.error("error in conversion of object to map", e);
        }
        
        
        return result;
    }
    
} // JavaUitls
