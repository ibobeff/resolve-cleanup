/******************************************************************************
* (C) Copyright 2014
*
* Resolve Systems, LLC
*
* All rights reserved
* This software is distributed under license and may not be used, copied,
* modified, or distributed without the express written permission of
* Resolve Systems, LLC.
*
******************************************************************************/

package com.resolve.util;

/**
 * This interface defines various constants for MCP.
 */
public interface MCPConstants
{
    String LINE_SEPARATOR = System.getProperty("line.separator");

    String MCP_LOG_LEVEL_INFO="INFO";
    String MCP_LOG_LEVEL_WARN="WARN";
    String MCP_LOG_LEVEL_ERROR="ERROR";
    String MCP_LOG_LEVEL_FATAL="FATAL";

    //These constants are used by groovy scripts and java classes
    //used in writing log by the write() method.

    //configuration
    String MCP_LOG_ITEM_CONFIG="CONFIG";

    //installation
    String MCP_LOG_ITEM_INSTALL_PRE="INSTALL-PRE";
    String MCP_LOG_ITEM_INSTALL_DB="INSTALL-DB";
    String MCP_LOG_ITEM_INSTALL_SETUP="INSTALL-SETUP";
    String MCP_LOG_ITEM_INSTALL_WIN="INSTALL-WIN";
    String MCP_LOG_ITEM_INSTALL_POST="INSTALL-POST";
    String MCP_LOG_ITEM_INSTALL_CLEANUP="INSTALL-CLEANUP";

    String MCP_RESOLVE_INSTALL_PRE_STATUS_LOGFILE = "install-pre-status.log";
    String MCP_RESOLVE_INSTALL_DB_STATUS_LOGFILE = "install-db-status.log";
    String MCP_RESOLVE_INSTALL_SETUP_STATUS_LOGFILE = "install-setup-status.log";
    String MCP_RESOLVE_INSTALL_WIN_STATUS_LOGFILE = "install-win-status.log";
    String MCP_RESOLVE_INSTALL_POST_STATUS_LOGFILE = "install-post-status.log";
    String MCP_RESOLVE_INSTALL_CLEANUP_STATUS_LOGFILE = "install-cleanup-status.log";

    String MCP_RESOLVE_CONFIG_LOGFILE = "resolve-config.log";
    String MCP_COMPONENT_START_LOGFILE = "start_mcp.log";
    String MCP_COMPONENT_STOP_LOGFILE = "stop_mcp.log";

    String MCP_FIELD_TIMESTAMP = "TIMESTAMP";
    String MCP_FIELD_ITEM = "ITEM";
    String MCP_FIELD_STATUS = "STATUS";
    String MCP_FIELD_COMMENT = "COMMENT";

    String MCP_FIELD_STATUS_KEY = "MCP_STATUS_KEY";

    String MCP_FIELD_KEY_CONFIG = "CONFIG";
    String MCP_FIELD_KEY_START = "START";
    String MCP_FIELD_KEY_END = "END";
    String MCP_FIELD_KEY_STOP = "STOP";
    String MCP_FIELD_KEY_STATUS = "STATUS";
    //script (or other) execute status
    String MCP_FIELD_KEY_EXECUTE_START = "EXECUTE_START";
    String MCP_FIELD_KEY_EXECUTE_END = "EXECUTE_END";

    String MCP_FIELD_VALUE_SUCCESS = "SUCCESS";
    String MCP_FIELD_VALUE_FAIL = "FAIL";
    String MCP_FIELD_VALUE_END = "END";
    String MCP_FIELD_VALUE_UP = "UP";
    String MCP_FIELD_VALUE_DOWN = "DOWN";

    //various blueprint related constants
    String MCP_BLUEPRINT_ACTUAL_BLUEPRINT = "MCP_ACTUAL_BLUEPRINT";

    //various server related constants
    String MCP_SERVER_ID = "MCP_SERVER_ID";
    String MCP_SERVER_STATUS = "MCP_SERVER_STATUS";
    String MCP_SERVER_STATUS_PING_P_ASSED = "MCP_PING_PASSED";
    String MCP_SERVER_STATUS_PING_FAILED = "MCP_PING_FAILED";
    String MCP_SERVER_STATUS_CONFIG_FAILED = "MCP_SERVER_CONFIG_FAILED";
    String MCP_SERVER_VERSION = "MCP_SERVER_VERSION";
    String MCP_SERVER_BUILD = "MCP_SERVER_BUILD";

    //blueprint deploy status constants
    String MCP_SERVER_STATUS_BLUEPRINT_SYNCHONIZED = "MCP_BLUEPRINT_SYNCHONIZED";
    String MCP_SERVER_STATUS_BLUEPRINT_OUT_OF_SYNC = "MCP_BLUEPRINT_OUT_OF_SYNC";
    String MCP_SERVER_STATUS_BLUEPRINT_DEPLOY_FAILED = "MCP_BLUEPRINT_DEPLOY_FAILED";
    String MCP_SERVER_STATUS_BLUEPRINT_DEPLOYED = "MCP_BLUEPRINT_DEPLOYED";
    String MCP_SERVER_STATUS_BLUEPRINT_NOT_DEPLOYED = "MCP_BLUEPRINT_NOT_DEPLOYED";
    String MCP_SERVER_STATUS_BLUEPRINT_DEPLOYING = "MCP_BLUEPRINT_DEPLOYING";
    String MCP_SERVER_STATUS_BLUEPRINT_VERIFYING = "MCP_BLUEPRINT_VERIFYING";
    String MCP_SERVER_STATUS_BLUEPRINT_VERIFY_FAILED = "MCP_BLUEPRINT_VERIFY_FAILED";
    
    //update/upgrade deploy status contants
    String MCP_SERVER_STATUS_UPDATE_DEPLOYING = "MCP_UPDATE_DEPLOYING";
    String MCP_SERVER_STATUS_UPDATE_DEPLOYED = "MCP_UPDATE_DEPLOYED";
    String MCP_SERVER_STATUS_UPDATE_STOPPING = "MCP_UPDATE_STOPPING";
    String MCP_SERVER_STATUS_UPDATE_CHECKSUM = "MCP_UPDATE_CHECKSUM";
    String MCP_SERVER_STATUS_UPDATE_APPLYING = "MCP_UPDATE_APPLYING";
    String MCP_SERVER_STATUS_UPDATE_STARTING = "MCP_UPDATE_STARTING";
    String MCP_SERVER_STATUS_UPDATE_IMPORTING = "MCP_UPDATE_IMPORTING";
    String MCP_SERVER_STATUS_UPDATE_FAILED = "MCP_UPDATE_FAILED";
    String MCP_SERVER_STATUS_UPDATE_SUCCESS = "MCP_UPDATE_SUCCESS";
    
    //mcp file types
    String MCP_FILE_UPDATE = "MCP_UPDATE";
    String MCP_FILE_UPGRADE = "MCP_UPGRADE";
    String MCP_FILE_LOG = "MCP_LOG";
    
    //mcp file constants
    String MCP_FILE_NAME = "MCP_FILE_NAME";
    String MCP_FILE_TYPE = "MCP_FILE_TYPE";
    String MCP_FILE_CONTENT = "MCP_FILE_CONTENT";
    String MCP_FILE_VERSION = "MCP_FILE_VERSION";
    String MCP_FILE_DATE = "MCP_FILE_DATE";

    //components
    String MCP_COMPONENT_GUID_UNDEFINED = "UNDEFINED";
    String MCP_COMPONENT_ID = "MCP_COMPONENT_ID";

    //various component related constants
    String MCP_COMPONENT_STATUS = "MCP_COMPONENT_STATUS";
    String MCP_COMPONENT_STATUS_UP = "UP";
    String MCP_COMPONENT_STATUS_DOWN = "DOWN";
    String MCP_COMPONENT_STATUS_FAILED_TO_START = "FAILED_TO_START";
    String MCP_COMPONENT_STATUS_FAILED_TO_STOP = "FAILED_TO_STOP";
    String MCP_COMPONENT_STATUS_CHECK_P_ASSED = "STATUS_CHECK_PASSED";
    String MCP_COMPONENT_STATUS_CHECK_FAILED = "STATUS_CHECK_FAILED";

    String MCP_STATUS_TYPE = "MCP_STATUS_TYPE";
    String MCP_PROBLEM_ID = "MCP_PROBLEM_ID";
    String MCP_ERROR_MESSAGE = "MCP_ERROR_MESSAGE";
    String MCP_USERNAME = "MCP_USERNAME";

    //Resolve install/update constants
    String MCP_INSTALL_PRE_STATUS_FAILED_TO_START = "FAILED_TO_START";

    String MCP_INSTALL_SETUP_RESOLVE_URL_CONFIGURED = "RESOLVE_URL_CONFIGURED";

}
