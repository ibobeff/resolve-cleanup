/******************************************************************************
* (C) Copyright 2014
*
* Resolve Systems, LLC
*
* All rights reserved
* This software is distributed under license and may not be used, copied,
* modified, or distributed without the express written permission of
* Resolve Systems, LLC.
*
******************************************************************************/

package com.resolve.util;

import java.io.Serializable;
import java.net.InetAddress;
import java.util.HashMap;
import java.util.Map;

import org.apache.commons.lang3.tuple.Pair;

public class MetricMsg implements Serializable
{
    /**
     * 
     */
    private static final long serialVersionUID = -2598574856661942633L;
    
    public static final String CPU_METRIC_LOAD_1 = "load1";
    public static final String CPU_METRIC_LOAD_5 = "load5";
    public static final String CPU_METRIC_LOAD_15 = "load15";
    
    private static final String LOAD_AVERAGE = "load average: ";
    private static final String COMMA_DELIMITER = ",";
    
    public static String sthostname;
    public static String stipaddress;
    public static String stcomponent;
    public static String mcpMode = Constants.MCP_MODE_LOCAL;

    {
        try
        {
            sthostname = InetAddress.getLocalHost().getHostName().toUpperCase();
            stipaddress = InetAddress.getLocalHost().getHostAddress().toUpperCase();
        }
        catch(Exception ex)
        {
            Log.log.warn("Failed to retrieve server host name or ip address", ex);  
        }
    }
    
    String name; // metric group name
    String source; // data source
    String id; // identifier string
    MetricMode mode; // metric mode - TOTAL, LAST
    HashMap<String, MetricUnit> units; // metric units
    public String hostname;
    public String ipaddress;
    public String component;
    
    public MetricMsg(String name, String source, String id)
    {
        this.name = name.toLowerCase();
        this.source = source;
        this.id = id;
        this.mode = MetricMode.TOTAL;
        this.units = new HashMap<String, MetricUnit>();
        initLocation();
    } // MetricMsg

    public MetricMsg(String name, String source, String id, MetricMode mode)
    {
        this.name = name.toLowerCase();
        this.source = source;
        this.id = id;
        this.mode = mode;
        this.units = new HashMap<String, MetricUnit>();
        initLocation();
    } // MetricMsg

    public MetricMsg(String name, String source, String id, MetricMode mode, HashMap<String, MetricUnit> units)
    {
        this.name = name.toLowerCase();
        this.source = source;
        this.id = id;
        this.mode = mode;
        this.units = new HashMap<String, MetricUnit>(units);
        initLocation();
    } // MetricMsg

    public MetricMsg(MetricMsg msg)
    {
        this.name = msg.name.toLowerCase();
        this.source = msg.source;
        this.id = msg.id;
        this.mode = msg.mode;
        this.units = new HashMap<String, MetricUnit>(msg.units);
        initLocation();
    } // MetricMsg

    private void initLocation()
    {
        hostname = sthostname;
        ipaddress = stipaddress;
        component = stcomponent;
    }
    
    public String toString()
    {
        String result = "";

        result += getKey() + " => [";
        for (String unitName : units.keySet())
        {
            result += unitName + " = [ tot:" + units.get(unitName).total + ", cnt:" + units.get(unitName).count + ", del:" + units.get(unitName).delta + "],";
        }
        result += "]";
        return result;
    } // toString

    public void consolidate(MetricMsg metric)
    {
        if (metric != null)
        {
            if (metric.mode == MetricMode.LAST)
            {
                // replace units
                this.units = metric.units;
            }
            else if (metric.mode == MetricMode.TOTAL)
            {
                for (String item : metric.getUnits().keySet())
                {
                    MetricUnit u1 = this.units.get(item);
                    MetricUnit u2 = metric.getUnits().get(item);
                    if (u1 != null && u2 != null)
                    {
                        // iterate through list and change it to HasMap
                        String metricName = this.units.get(item).name;
                        MetricUnit sumMetric = MetricUnit.sum(this.units.get(item), metric.getUnits().get(item));
                        this.units.put(metricName, sumMetric);
                    }
                    else
                    {
                        Log.log.warn("Invalid MetricUnit u1: " + u1 + " u2: " + u2);
                    }
                }
            }
        }
    } // consolidate

    public String getKey()
    {
        return name.toLowerCase() + "/" + source + "/" + id;
    } // getKey

    public void addMetric(MetricUnit unit)
    {
        this.units.put(unit.getName(), unit);
    } // addMetric

    public void addMetric(String name, long value)
    {
        MetricUnit unit = new MetricUnit(name, value);
        this.units.put(unit.getName(), unit);
    } // addMetric

    public void addMetric(String name, long value, int count)
    {
        MetricUnit unit = new MetricUnit(name, value, count);
        this.units.put(unit.getName(), unit);
    } // addMetric

    public HashMap<String, MetricUnit> getUnits()
    {
        return this.units;
    } // getUnits

    public MetricUnit getMetricUnit(String name)
    {
        return this.units.get(name);
    }

    public String getSource()
    {
        return this.source;
    } // getSource

    public String getName()
    {
        return this.name.toLowerCase();
    } // getName

    public String getId()
    {
        return this.id;
    } // getId
    
    public static  Map<String, Pair<Long, Integer>> parseLinuxCPUMetricOutput(String out) {
    	Map<String, Pair<Long, Integer>> cpuMetrics = new HashMap<String, Pair<Long, Integer>>();
    	
    	if (StringUtils.isBlank(out) || !out.contains(LOAD_AVERAGE)) {
    		return cpuMetrics;
    	}
    	
    	if (Log.log.isDebugEnabled()) {
    		Log.log.debug(String.format("uptime result [%s]", out));
    	}
    	
        int pos = out.indexOf(LOAD_AVERAGE);
        
        if (pos >= 0 && out.length() < (pos + LOAD_AVERAGE.length() + 1)) {
        	return cpuMetrics;
        }
        
        pos += LOAD_AVERAGE.length();
        
        String remainingStr = out.substring(pos, out.length());
        
        if (Log.log.isDebugEnabled()) {
        	Log.log.debug(String.format("Remaining string [%s] after parsing out [%s] and everything before that " +
        								"from uptime result [%s]", remainingStr, LOAD_AVERAGE, out));
        }
        
        if (StringUtils.isBlank(remainingStr) || !remainingStr.contains(COMMA_DELIMITER)) {
    		return cpuMetrics;
    	}
        
        pos = remainingStr.indexOf(COMMA_DELIMITER);
        
        if (pos <= 0) {
        	return cpuMetrics;
        }
        
        String load1Str = remainingStr.substring(0, pos).trim();
        
        if (Log.log.isDebugEnabled()) {
        	Log.log.debug(String.format("load1Str [%s]", load1Str));
        }
        
        if (remainingStr.length() <= (pos + 1)) {
        	return cpuMetrics;
        }
        
        remainingStr = remainingStr.substring((pos + 1), remainingStr.length());
        
        if (StringUtils.isBlank(remainingStr) || !remainingStr.contains(COMMA_DELIMITER)) {
    		return cpuMetrics;
    	}
        
        pos = remainingStr.indexOf(COMMA_DELIMITER);
        
        if (pos <= 0) {
        	return cpuMetrics;
        }
        
        String load5Str = remainingStr.substring(0, pos).trim();
        
        if (Log.log.isDebugEnabled()) {
        	Log.log.debug(String.format("load5Str [%s]", load5Str));
        }
        
        if (remainingStr.length() <= (pos + 1)) {
        	return cpuMetrics;
        }
        
        remainingStr = remainingStr.substring((pos + 1), remainingStr.length());
        
        if (StringUtils.isBlank(remainingStr)) {
    		return cpuMetrics;
    	}
        
        String load15Str = remainingStr.trim();
        
        if (Log.log.isDebugEnabled()) {
        	Log.log.debug(String.format("load15Str [%s]", load15Str));
        }

        float load1F = 0.0F;
        float load5F = 0.0F;
        float load15F = 0.0F;
        
        if (StringUtils.isNotBlank(load1Str)) {
        	try {
        		load1F = Float.parseFloat(load1Str);
        	} catch (NumberFormatException nfe) {
        		Log.log.warn(String.format("Error %sin parsing float from load1Str %s", 
        								   (StringUtils.isNotBlank(nfe.getMessage()) ? 
        									"[" + nfe.getMessage() + "] " : ""), load1Str));
        	}
        }
        
        if (StringUtils.isNotBlank(load5Str)) {
        	try {
        		load5F = Float.parseFloat(load5Str);
        	} catch (NumberFormatException nfe) {
        		Log.log.warn(String.format("Error %sin parsing float from load5Str %s", 
        								   (StringUtils.isNotBlank(nfe.getMessage()) ? 
        									"[" + nfe.getMessage() + "] " : ""), load5Str));
        	}
        }
        
        if (StringUtils.isNotBlank(load15Str)) {
        	try {
        		load15F = Float.parseFloat(load15Str);
        	} catch (NumberFormatException nfe) {
        		Log.log.warn(String.format("Error %sin parsing float from load15Str %s", 
        								   (StringUtils.isNotBlank(nfe.getMessage()) ? 
        									"[" + nfe.getMessage() + "] " : ""), load15Str));
        	}
        }
        
        long load1 = (long)(load1F * 100);
        long load5 = (long)(load5F * 100);
        long load15 = (long)(load15F * 100);

        int intload1 = (int)(load1F * 100);
        int intload5 = (int)(load5F * 100);
        int intload15 = (int)(load15F * 100);
        
        cpuMetrics.put(CPU_METRIC_LOAD_1, Pair.of(Long.valueOf(load1), Integer.valueOf(intload1)));
        cpuMetrics.put(CPU_METRIC_LOAD_5, Pair.of(Long.valueOf(load5), Integer.valueOf(intload5)));
        cpuMetrics.put(CPU_METRIC_LOAD_15, Pair.of(Long.valueOf(load15), Integer.valueOf(intload15)));
        
    	return cpuMetrics;
    }

} // MetricMsg

