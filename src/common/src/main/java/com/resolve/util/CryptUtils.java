/******************************************************************************
 * (C) Copyright 2014
 *
 * Resolve Systems, LLC
 *
 * All rights reserved
 * This software is distributed under license and may not be used, copied,
 * modified, or distributed without the express written permission of
 * Resolve Systems, LLC.
 *
 ******************************************************************************/

package com.resolve.util;

import org.apache.commons.codec.binary.Base64;
import org.apache.commons.codec.digest.DigestUtils;
import org.apache.commons.lang3.ArrayUtils;

public class CryptUtils
{
	public enum KeyType {
	    DES,
	    AES,
	    LICENSE
	}
	
	// default prefix
	public static String prefixLocal = "ENC:";
	public static String prefix128Local = "ENC1:";

	private static Crypt crypt = null;
	private static Crypt cryptAES = null;
	private static Crypt licenseCrypt = null;
	private static byte[] keyBytesLocal = null;
	private static CryptKeyProvider keyProvider = new CryptKeyStaticProvider();
	//private static CryptKeyProvider keyProvider = new CryptKeyRestProvider();

	public static boolean isEncrypted(String value) {
		return StringUtils.startsWith(value, prefixLocal) || StringUtils.startsWith(value, prefix128Local);
	}

	public static String encryptUnix(String value, String salt, String prefix) throws Exception {
		if (prefix == null) {
			prefix = "";
		}
		return prefix + CryptUnix.crypt(salt, value);
	}

	public static String encryptMD5(String value) {
		return DigestUtils.md5Hex(value);
	}

	public static String getENCData() {
		return ArrayUtils.isNotEmpty(keyBytesLocal) ? new String(Base64.encodeBase64(keyBytesLocal))
				: StringUtils.EMPTY;
	}

	public static void setENCData(String encData) throws Exception {
		keyBytesLocal = Base64.decodeBase64(encData);
	}

	public static String encrypt(String value) throws Exception {
		// by default the encryption is AES128
		// add prefix if defined
		if (value != null && !value.startsWith(prefix128Local)) {
			return prefix128Local + getCryptAES().encrypt(value);
		}

		return value;

	}

	public static String decryptUTF8(String value) throws Exception {
		if (value != null && value.startsWith(prefixLocal)) {
			return decrypt(value, true);
		} else if (value != null && value.startsWith(prefix128Local)) {
			return decryptAES128(value, true);
		}

		return value;
	}

	public static String decrypt(String value) throws Exception {
		if (value != null && value.startsWith(prefixLocal)) {
			return decrypt(value, false);
		} else if (value != null && value.startsWith(prefix128Local)) {
			return decryptAES128(value, false);
		}

		return value;
	}

	/*
	 * License is encrypted without prefix.
	 */
	public static String decryptLicense(String value) throws Exception {
		return getLicenseCrypt().decrypt(value);
	}

	/*
	 * License should be encrypted without prefix.
	 */
	public static String encryptLicense(String value) throws Exception {
		return getLicenseCrypt().encrypt(value);
	}

	public static String getDESKey() {
		return keyProvider.getKey(KeyType.LICENSE);
	}

	public static String getAESKey() throws Exception {
		return decrypt(keyProvider.getKey(KeyType.AES));
	}

	private static String decryptAES128(String value, boolean isUTF) throws Exception {
		return decrypt(getCryptAES(), value, prefix128Local, isUTF);
	}

	private static String decrypt(String value, boolean isUTF) throws Exception {
		return decrypt(getCryptDES(), value, prefixLocal, isUTF);
	}

	private static String decrypt(Crypt cryptInst, String value, String prefix, boolean isUTF) throws Exception {
		// remove prefix if defined
		if (value != null && value.startsWith(prefix)) {
			value = value.substring(prefix.length());
			for (int i = 0; i < 3; i++) {
				try {
					return isUTF ? cryptInst.decryptUTF8(value) : cryptInst.decrypt(value);
				} catch (Exception e) {
					Log.log.error("Failed to Decrypt Value", e);
					try {
						if (i < 2) {
							Thread.sleep(2000);
						}
					} catch (InterruptedException ie) {
						Log.log.error("Interrupted Exception", ie);
					}
				}
			}
		}

		return value;
	}

	private static Crypt getCryptDES() throws Exception {
		if (crypt == null) {
			crypt = new Crypt(Crypt.EncryptionScheme.DESEDE, keyProvider.getKey(KeyType.DES));
		}

		return crypt;
	}

	private static Crypt getCryptAES() throws Exception {
		if (cryptAES == null) {
			cryptAES = new Crypt(Crypt.EncryptionScheme.AES128, keyBytesLocal);
		}

		return cryptAES;
	}

	private static Crypt getLicenseCrypt() throws Exception {
		if (licenseCrypt == null) {
			licenseCrypt = new Crypt(Crypt.EncryptionScheme.DESEDE, keyProvider.getKey(KeyType.LICENSE));
		}

		return licenseCrypt;
	}
	
    public static String encryptToken(String token) {
        try {
            return Base64.encodeBase64String(encrypt(token).getBytes());
        } catch (Exception e) {
            throw new RuntimeException("Unable to encrypt auth token");
        }
    }
    
    public static String decryptToken(String token) {
        try {
            String token64 = new String(Base64.decodeBase64(token));
            if (token64 != null && token64.startsWith("ENC")) {
                return decrypt(token64);
            } else {
                return token;
            }
        } catch (Exception e) {
            throw new RuntimeException("Unable to decrypt auth token");
        }
    }
}