/******************************************************************************
 * (C) Copyright 2014
 *
 * Resolve Systems, LLC
 *
 * All rights reserved
 * This software is distributed under license and may not be used, copied,
 * modified, or distributed without the express written permission of
 * Resolve Systems, LLC.
 *
 ******************************************************************************/

package com.resolve.util;

import java.io.BufferedInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.PrintWriter;
import java.io.StringWriter;
import java.text.SimpleDateFormat;
import java.time.Duration;
import java.time.ZoneId;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;
import java.util.Properties;
import java.util.TimeZone;

import org.apache.log4j.BasicConfigurator;
import org.apache.log4j.Level;
import org.apache.log4j.Logger;
import org.apache.log4j.MDC;
import org.apache.log4j.PropertyConfigurator;
import org.apache.log4j.varia.NullAppender;
import org.owasp.esapi.reference.Log4JLogger;

import com.resolve.esb.ESB;
import com.resolve.rsbase.MainBase;
import com.resolve.util.FileUtils;

public class Log
{
    public static Logger log;
    public static Logger auth;
    public static Logger syslog;
    public static Logger dcs;
    public static ESB esb;
    public static String component;
    
    private static final String SYSTEM_USERNAME = "system";
    
    private static final SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSS'Z'");
    
    private static ThreadLocal<Integer> hereCount = new ThreadLocal<Integer>();
    private static ThreadLocal<Long> hereTime = new ThreadLocal<Long>();

    private static ThreadLocal<String> prevThreadName = new ThreadLocal<String>();
    private static ThreadLocal<Long> startTime = new ThreadLocal<Long>();
    
    private static boolean dcsLoggingEnabled = false;
    private static String dcsClientUsername = null;
    private static String dcsClientPassword = null;
        
    static {
    	sdf.setTimeZone(TimeZone.getTimeZone(ZoneId.of("UTC")));
    }
	
    public static void setCurrentContext(String context) {
        MDC.put("context", context);
    }

    public static String getCurrentContext() {
        Object context = MDC.get("context");
        return (context != null)? (String) context: "";
    }

    public static void putCurrentNamedContext(String name, String context) {
        if (context != null) {
            MDC.put(name,  context);
        }
    }

    public static void clearContext() {
        setCurrentContext("");
    }
    
    public static void clearCurrentNamedContext(String name) {
        MDC.put(name, "");
    }
 
    public static void init()
    {
        init("");
    } // init

    private static Logger getLogger(String name)
    {
    	return Log4JLogger.getLogger(name);
    }

    public static void init(String name)
    {
        try
        {
            log = getLogger(name); // .getRootLogger();
            if (log == null)
            {
                throw new Exception("ERROR: Failed to initialize logging");
            }

            syslog = getLogger("syslog");
            if (syslog == null)
            {
                throw new Exception("ERROR: Failed to initialize syslog");
            }
            
            dcs = getLogger("DCS");
            if (dcs == null)
            {
                throw new Exception("ERROR: Failed to initialize DCS logger");
            }
            
            BasicConfigurator.configure();
            log.setLevel(Level.INFO);
        }
        catch (Exception e)
        {
            System.out.println(e.getMessage());
        }
    } // init

    public static void init(String configFile, String name) throws Exception
    {
        FileInputStream fis = null;
        try
        {
            // remapping file
            String remapFile;
            int pos = configFile.lastIndexOf('.');
            if (pos > 0)
            {
                remapFile = configFile.substring(0, pos) + ".map";
            }
            else
            {
                remapFile = configFile + ".map";
            }

            // init logger
            File file = FileUtils.getFile(configFile);
            if (file.exists() == false)
            {
                throw new Exception("Unable to find Log configuration file: " + file.getAbsolutePath());
            }

            // RBA-13770 : log4j:WARN No such property [enabled] in org.apache.log4j.net.SyslogAppender
            Properties currentLog4JProperties = new Properties();
            fis = new FileInputStream(configFile);
            BufferedInputStream bis = new BufferedInputStream(fis);
            if (bis != null)
            {
                currentLog4JProperties.load(bis);
            }
            Properties elementsToRemove = new Properties();
            String ntEventLogEnabled = currentLog4JProperties.containsKey("log4j.appender.NTEventLog.enabled") ? currentLog4JProperties.getProperty("log4j.appender.NTEventLog.enabled") : "false";
            String syslogEnabled = currentLog4JProperties.containsKey("log4j.appender.SYSLOG.enabled") ? currentLog4JProperties.getProperty("log4j.appender.SYSLOG.enabled") : "false";
            String dcsLogEnabled = currentLog4JProperties.containsKey("log4j.appender.DCS.enabled") ? currentLog4JProperties.getProperty("log4j.appender.DCS.enabled") : "false";
            
			dcsClientUsername = currentLog4JProperties
					.containsKey("log4j.appender.DCS.dcsAuthenticationUsername")
							? currentLog4JProperties
									.getProperty("log4j.appender.DCS.dcsAuthenticationUsername")
							: null;
			dcsClientPassword = currentLog4JProperties
					.containsKey("log4j.appender.DCS.dcsAuthenticationPassword")
							? currentLog4JProperties
									.getProperty("log4j.appender.DCS.dcsAuthenticationPassword")
							: null;
            
            elementsToRemove.put("log4j.appender.NTEventLog.enabled", ntEventLogEnabled);
            elementsToRemove.put("log4j.appender.SYSLOG.enabled", syslogEnabled);
            elementsToRemove.put("log4j.appender.DCS.enabled", dcsLogEnabled);
            
            Properties elementsToReplace = new Properties();
            
            if (!Boolean.valueOf(dcsLogEnabled).booleanValue() && currentLog4JProperties.containsKey("log4j.appender.DCS") &&
                currentLog4JProperties.containsKey("log4j.logger.DCS")) {
                elementsToReplace.put("log4j.appender.DCS", "org.apache.log4j.varia.NullAppender");
                
                for (Object currLog4JProperty : currentLog4JProperties.keySet()) {
                    if (((String)currLog4JProperty).startsWith("log4j.appender.DCS.")) {
                        elementsToRemove.put(currLog4JProperty, currentLog4JProperties.getProperty((String)currLog4JProperty));
                    }
                }
            }
            
            for (Object keyToRemove : elementsToRemove.keySet())
            {
                if (currentLog4JProperties.containsKey(keyToRemove))
                {
                    elementsToRemove.put(keyToRemove, currentLog4JProperties.get(keyToRemove));
                    currentLog4JProperties.remove(keyToRemove);
                }
            }
            
            for (Object keyToReplace : elementsToReplace.keySet()) {
                if (currentLog4JProperties.containsKey(keyToReplace)) {
                    currentLog4JProperties.put(keyToReplace, elementsToReplace.get(keyToReplace));
                }
            }
            
            // configure logger
            // PropertyConfigurator.configure(configFile);
            PropertyConfigurator.configure(currentLog4JProperties);
            // associate logger
            log = getLogger("com.resolve." + name);

            // init syslog
            boolean syslogLinuxEnabled = Boolean.parseBoolean(elementsToRemove.getProperty("log4j.appender.SYSLOG.enabled"));
            // boolean syslogWindowsEnabled = Boolean.parseBoolean(elementsToRemove.getProperty("log4j.appender.NTEventLog.enabled"));
            syslog = getLogger("syslog");
            if (syslog == null)
            {
                throw new Exception("ERROR: Failed to initialize syslog");
            }
            if (!syslogLinuxEnabled)
            {
                syslog.removeAppender("SYSLOG");
            }
            /*
             * if (!syslogWindowsEnabled)
             * {
             * syslog.removeAppender("NTEventLog");
             * }
             * if (!syslogLinuxEnabled && !syslogWindowsEnabled)
             * {
             * syslog.addAppender(new NullAppender());
             * }
             */
            if (!syslogLinuxEnabled)
            {
                syslog.addAppender(new NullAppender());
            }
            
            dcs = getLogger("DCS");
            
            if (dcs == null)
            {
                throw new Exception("ERROR: Failed to initialize DCS logger");
            }
            
            dcsLoggingEnabled = Boolean.parseBoolean(elementsToRemove.getProperty("log4j.appender.DCS.enabled"));
            
            // initRemapFile
            LogFilter.init(remapFile);
        }
        catch (Exception e)
        {
            System.out.println(e.getMessage());
        }
        finally
        {
            if (fis != null)
            {
                fis.close();
            }
        }
    } // init

    public static void initAuth() throws Exception
    {
        try
        {
            // check that log has been init
            if (log == null)
            {
                throw new Exception("Normal LOG must be initialized first");
            }
            else
            {
                // init AUTH logger
                auth = getLogger("com.resolve.auth");
            }
        }
        catch (Exception e)
        {
            System.out.println(e.getMessage());
        }
    } // initAuth

    public static void initESB(ESB esb) throws Exception
    {
        Log.esb = esb;
    } // initESB

    public static void initComponent(String component) throws Exception
    {
        Log.component = component;
    } // initComponent

    public static void setProcessId(String processId)
    {
        if (!StringUtils.isEmpty(processId))
        {
            String name = Thread.currentThread().getName();
            String[] names = name.split("-");
            if (names.length >= 5)
            {
                name = names[0] + "-" + names[1] + "-" + names[2] + "-" + names[3];
            }

            prevThreadName.set(name);
            Thread.currentThread().setName(name + "-" + processId);
            Log.log.debug("LOG rename threadid: " + Thread.currentThread().getName() + " prevName: " + prevThreadName.get());
        }
    } // setProcessId

    public static void clearProcessId()
    {
        String prevName = prevThreadName.get();
        if (!StringUtils.isEmpty(prevName))
        {
            Log.log.debug("LOG clear threadid: " + prevName + " prevName: " + Thread.currentThread().getName());

            Thread.currentThread().setName(prevName);

            prevThreadName.set("");
        }
    } // clearProcessId

    public static void setLevel(String level)
    {
        log.setLevel(Level.toLevel(level));
    } // setlevel

    public static void setDebug()
    {
        log.setLevel(Level.DEBUG);
    } // setDebug

    public static void hereReset()
    {
        hereCount.set(0);
        hereTime.set(System.currentTimeMillis());
    } // hereReset

    public static void here()
    {
        if (hereTime.get() == null)
        {
            hereReset();
        }

        long temp = System.currentTimeMillis();
        long diffTime = temp - hereTime.get().longValue();
        if (hereCount.get().intValue() > 0)
        {
            long hours = diffTime / 3600000;
            diffTime = diffTime - (hours * 3600000);
            long minutes = diffTime / 60000;
            diffTime = diffTime - (minutes * 60000);
            long seconds = diffTime / 1000;
            diffTime = diffTime - (seconds * 1000);
            long millis = diffTime;
            log.trace("HERE" + hereCount.get() + " elapsed time: " + hours + "hrs " + minutes + "mins " + seconds + "secs " + millis + "msecs");
        }
        else
        {
            log.trace("HERE" + hereCount.get());
        }
        hereCount.set(hereCount.get().intValue() + 1);
        hereTime.set(temp);
    } // here

    public static void loc()
    {
        loc(null, 0);
    } // loc

    public static void loc(String prefix, int prevStackLevel)
    {
        if (prefix == null)
        {
            prefix = "LOCATION:";
        }
        StackTraceElement stack = new Throwable().getStackTrace()[prevStackLevel + 1];
        Log.log.trace(prefix + " " + stack.getClassName() + "#" + stack.getMethodName());
    } // loc

    public static boolean loadMapping()
    {
        return LogFilter.load();
    } // loadMapping

    public static String getStrackTrace(Throwable e)
    {
        String result = "";

        StringWriter sw = new StringWriter();
        PrintWriter pw = new PrintWriter(sw);

        e.printStackTrace(pw);
        result = sw.toString();

        return result;
    } // getStackTrace

    public static long reset()
    {
        long result = System.currentTimeMillis();

        startTime.set(new Long(result));

        return result;
    } // reset

    public static long start() {
        return start("Time start:");
    } // start

    public static long start(String msg) {
    	return start(msg, Level.TRACE);
    } // start

    public static long start(String msg, String levelTxt) {
    	if (StringUtils.isNotBlank(levelTxt)) {
    		return start(msg, Level.toLevel(levelTxt.toUpperCase(), Level.WARN));
    	} else {
    		return start(msg);
    	}
    } // start

    private static String getLogMessage(String msg, long result) {
    	String tMsg = StringUtils.isNotBlank(msg) ? msg + " " : "";
		String dateTxt = sdf.format(new Date(result));
		
		String fileClassMethodPrefix = "";
		
		if (Thread.currentThread().getStackTrace().length >= 4) {
			fileClassMethodPrefix = String.format("{%s(%d):%s:%s} ",
												  Thread.currentThread().getStackTrace()[3].getFileName(),
												  Thread.currentThread().getStackTrace()[3].getLineNumber(),
												  Thread.currentThread().getStackTrace()[3].getClassName(),
												  Thread.currentThread().getStackTrace()[3].getMethodName());
		}
		
		return String.format("%s%s%s(%d)", fileClassMethodPrefix, tMsg, dateTxt, result);    }
    
    public static long start(String msg, Level level)
    {
    	long result = reset();

    	if (level != null) {
        	if (level.toInt() < Level.WARN_INT) {
	        	switch (level.toInt()) {
	        		case Level.INFO_INT:
	        			log.info(getLogMessage(msg, result));
	        			break;
	        		
	        		case Level.DEBUG_INT:
	        			log.debug(getLogMessage(msg, result));
	        			break;
	        			
	        		case Level.TRACE_INT:
	        			log.trace(getLogMessage(msg, result));
	        			break;
	        	}
        	}
    	}

        return result;
    } // start

    public static long duration() {
        return duration("Time duration:");
    } // duration
    
    public static long duration(long startTimeLong) {
        return duration("Time duration:", startTimeLong);
    } // duration
    
    public static long duration(String msg) {
    	return duration(msg, Level.TRACE);
    } // duration
    
    public static long duration(String msg, long startTimeLong) {
    	return duration(msg, Level.TRACE, startTimeLong);
    } // duration
    
    public static long duration(String msg, String levelTxt) {
    	if (StringUtils.isNotBlank(levelTxt)) {
    		return duration(msg, Level.toLevel(levelTxt.toUpperCase(), Level.WARN));
    	} else {
    		return duration(msg);
    	}
    } // duration
    
    public static long duration(String msg, String levelTxt, long startTimeLong) {
    	if (StringUtils.isNotBlank(levelTxt)) {
    		return duration(msg, Level.toLevel(levelTxt.toUpperCase(), Level.WARN), startTimeLong);
    	} else {
    		return duration(msg, startTimeLong);
    	}
    } // duration
    
    private static String getLogMessage(String msg, long startTimeLong, long endTime) {
    	String tMsg = StringUtils.isNotBlank(msg) ? msg + " " : "";
		String startDateTxt = sdf.format(new Date(startTimeLong));
		String endDateTxt = sdf.format(new Date(endTime));
		Duration duration = Duration.ofMillis(endTime - startTimeLong);
		
		String fileClassMethodPrefix = "";
		
		if (Thread.currentThread().getStackTrace().length >= 4) {
			fileClassMethodPrefix = String.format("{%s(%d):%s:%s} ",
												  Thread.currentThread().getStackTrace()[3].getFileName(),
												  Thread.currentThread().getStackTrace()[3].getLineNumber(),
												  Thread.currentThread().getStackTrace()[3].getClassName(),
												  Thread.currentThread().getStackTrace()[3].getMethodName());
		}
		
		return String.format("%s%s Start:%s(%d) End:%s(%d) Duration:%s (%d ms)", fileClassMethodPrefix, tMsg, startDateTxt, 
							 startTimeLong, endDateTxt, endTime, duration, duration.toMillis());    }
    
    public static long duration(String msg, Level level) {
    	long startTimeLong = startTime.get().longValue();
    	long endTime = System.currentTimeMillis();
    	
    	if (level != null) {
        	if (level.toInt() < Level.WARN_INT) {
	        	switch (level.toInt()) {
	        		case Level.INFO_INT:
	        			log.info(getLogMessage(msg, startTimeLong, endTime));
	        			break;
	        		
	        		case Level.DEBUG_INT:
	        			log.debug(getLogMessage(msg, startTimeLong, endTime));
	        			break;
	        			
	        		case Level.TRACE_INT:
	        			log.trace(getLogMessage(msg, startTimeLong, endTime));
	        			break;
	        	}
        	}
    	}
    	
    	return (endTime - startTimeLong);
    } // duration

    public static long duration(String msg, Level level, long startTimeLong) {
    	long endTime = System.currentTimeMillis();
    	
    	if (level != null) {
        	if (level.toInt() < Level.WARN_INT) {
	        	switch (level.toInt()) {
	        		case Level.INFO_INT:
	        			log.info(getLogMessage(msg, startTimeLong, endTime));
	        			break;
	        		
	        		case Level.DEBUG_INT:
	        			log.debug(getLogMessage(msg, startTimeLong, endTime));
	        			break;
	        			
	        		case Level.TRACE_INT:
	        			log.trace(getLogMessage(msg, startTimeLong, endTime));
	        			break;
	        	}
        	}
    	}
    	
    	return (endTime - startTimeLong);
    } // duration
    
    public static void alert(ERR errorCode)
    {
        alert(errorCode, null, null);
    } // alert

    public static void alert(ERR errorCode, String message)
    {
        alert(errorCode, message, null);
    } // alert

    public static void alert(ERR errorCode, Throwable t)
    {
        alert(errorCode, null, t);
    } // alert

    /**
     * Log a FATAL log message and sends a JMS request to the RSMGMT (ALERT
     * queue) to post a notification to the event management system
     *
     * @param type
     *            - type of problem. Use (or add to)
     *            Constants.ALERT_TYPE_DATABASE, etc
     * @param message
     *            - error message
     * @param t
     *            - exceptiona throwable to retrieve stack trace
     *
     * @throws Exception
     */
    public static void alert(ERR errorCode, String detail, Throwable t)
    {
        // init message
        String message = errorCode.getMessage();
        if (detail != null)
        {
            message += " - " + detail;
        }

        // init exception
        if (t == null)
        {
            t = new Exception(message);
        }

        // send fatal message to RSMgmt
        if (esb == null)
        {
            Log.log.fatal("ESB not initialized", new Exception());
        }
        else
        {
            message += "\n";
            message += StringUtils.getStackTrace(t);
            message += "\n";

            alert(errorCode.getCode(), message);
        }
    } // alert

    public static void alert(String code, String message)
    {
        alert(code, message, "CRITICAL");
    } // alert

    public static void alert(String code, String message, String severity)
    {
        alert(code, message, severity, null, null);
    } // alert

    public static void alert(String code, String message, String severity, String src, String action)
    {
        alert(code, message, severity, null, null, src, action);
    } // alert

    @SuppressWarnings({ "rawtypes", "unchecked" })
    public static void alert(String code, String detail, String severity, String subject, String worksheetId, String src, String action)
    {
        String message = detail;

        if (StringUtils.isNotEmpty(message))
        {
            Map params = new HashMap();
            params.put("SEVERITY", severity);
            params.put("COMPONENT", StringUtils.isBlank(src) ? component : src);
            params.put("CODE", code);
            params.put("MESSAGE", message);
            params.put("ADDRESS", MainBase.main.configId.getIpaddress());
            params.put("HOSTNAME", MainBase.main.configId.getName());
            if (StringUtils.isNotBlank(worksheetId))
            {
                params.put("WORKSHEETID", worksheetId);
            }

            if (StringUtils.isNotBlank(action))
            {
                params.put("ACTION", action);
            }

            // notify rsmgmt
            esb.sendInternalMessage(Constants.ESB_NAME_ALERT, "MAlert.alert", params);

            // log fatal message
            String logMessage = new StringBuilder().append("Severity: ").append(severity).append(" type: ").append(code).append(" Message: ").append(message).toString();
            Exception t = new Exception(logMessage);
            
            if (ERR.isFatal(code)) {
                log.fatal(logMessage, t);
                System.out.println(logMessage);
                t.printStackTrace();
            } else {
                log.error(logMessage, t);
            }

            try
            {
                // post to Admin Group team
                if (StringUtils.isBlank(subject))
                {
                    int pos = message.indexOf('\n');
                    if (pos > 0)
                    {
                        subject = message.substring(0, pos);
                    }
                    else
                    {
                        subject = message;
                    }
                }
                if (subject.length() > 60)
                {
                    subject = subject.substring(0, 60);
                }

                String header = "Alert Notification from " + MainBase.main.release.getType() + " (" + MainBase.main.configId.getGuid() + ")";
                message = header + "\n\n" + message;
                sendToSocialTeam(subject, message);
            }
            catch (Exception e)
            {
                Log.log.error(e.getMessage(), e);
            }
        }
    }
    
    public static boolean isDcsLoggingEnabled()
    {
        return dcsLoggingEnabled;
    }
    
    private static void sendToSocialTeam(final String subject, final String content) {
        Map<String, String> params = new HashMap<>();
        params.put("USERNAME", SYSTEM_USERNAME);
        params.put("SUBJECT", subject);
        params.put("POSTCONTENT", content);
        params.put("TARGET", Constants.SOCIAL_TEAM_ADMIN_GROUP);
        params.put("SENDEMAIL", Boolean.FALSE.toString());

        // Submit to the ESB
        boolean isSuccess = MainBase.esb.sendMessage(Constants.ESB_NAME_RSVIEW, "MSocial.postToTeam", params);
        if (isSuccess) {
            if (Log.log.isTraceEnabled()) {
                Log.log.trace("Submitted social notification to team:" + Constants.SOCIAL_TEAM_ADMIN_GROUP);
            }
        } else {
            Log.log.warn("Could not submit social notification to team:" + Constants.SOCIAL_TEAM_ADMIN_GROUP);
        }
    }

	public static String getDcsClientUsername() {
		return dcsClientUsername;
	}

	public static String getDcsClientPassword() {
		return dcsClientPassword;
	}

}