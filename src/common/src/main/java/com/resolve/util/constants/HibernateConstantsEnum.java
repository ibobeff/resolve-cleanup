/******************************************************************************
* (C) Copyright 2014
*
* Resolve Systems, LLC
*
* All rights reserved
* This software is distributed under license and may not be used, copied,
* modified, or distributed without the express written permission of
* Resolve Systems, LLC.
*
******************************************************************************/

package com.resolve.util.constants;

public enum HibernateConstantsEnum
{
    //////////////////////////////////////////////////////////////////////////////////////
    //refer SearchConstants
    TIMEZONE("timezone"),
    TIMEZONE_OFFSET("timezoneOffset"),
    SERVER_URL("serverURL"),
    COMPONENT_TAGS("tags"),
    COMPONENT_TITLE("componentTitle"),
    COMPONENT_TYPE("u_component_type"),
    RATING("rating"),
    SEARCH_API_NAMESPACE("NAMESPACE"),
    SEARCH_API_TIMEZONE("TIMEZONE"), 
    SEARCH_API_OFFSET("OFFSET"), 
    SEARCH_API_LIMIT("LIMIT"),
    SEARCH_OF_TITLE("Title"),
    SEARCH_OF_CONTENTS("Contents"),
    SEARCH_OF_LINK("Link"), 
    SEARCH_RESULT_SEPARATOR("##########"),
    SEARCH_BY_TYPE("Search For"),//("Result Type"),
    
    //////////////////////////////////////////////////////////////////////////////////////
    //refer AdminConstants
    MODELCLASS("_Resolve_GDS_tablename"), 
    OUTPUT("OUTPUT"),
    INPUT_DESC("INPUT_DESC"),
    OUTPUT_DESC("OUTPUT_DESC"),
    OUTPUT_RUNBOOK("OUTPUTRUNBOOK"),
    FLOW_VALUE("FLOWVALUE"),
    AUTOMATION_MODEL("AUTOMATION_MODEL"),
    
  //refer Operation.java, DataSource.java
    DB("DB"),
    INPUT("INPUT"),
    
    
    
    
    //////////////////////////////////////////////////////////////////////////////////////
    //meta form constants - refer WorkFlowConstants
    UI_TEXTFIELD("TextField"),
    UI_NUMBERTEXTFIELD("NumberTextField"),
    UI_DECIMALTEXTFIELD("DecimalTextField"),
    UI_TEXTAREA("TextArea"),
    UI_CHECKBOX("CheckBox"),//for boolean
    UI_JOURNALWIDGET("Journal"),//for journal
    UI_COMBOBOX("ComboBox"),//for selecting from list
    UI_CHOICE("RadioButton"),
    UI_MULTIPLECHECKBOX("MultipleCheckBox"),
    UI_DATETIME("DateTime"),
    UI_DATE("Date"),
    UI_LIST("List"),
    UI_SEQUENCE("Sequence"),
    UI_PASSWORD("Password"),
    UI_REFERENCE("Reference"),
    UI_LINK("Link"),
    UI_TIMESTAMP("Timestamp"),
    UI_HIDDEN("HiddenField"),
    UI_TAG("TagField"),
    //new ones for extjs
    UI_NAMEFIELD("NameField"),
    UI_ADDRESS("AddressField"),
    UI_EMAIL("EmailField"),
    UI_PHONE("PhoneField"),
    UI_READONLYTEXTFIELD("ReadOnlyTextField"),
    UI_USERPICKER("UserPickerField"),
    UI_TEAMPICKER("TeamPickerField"),
    
    NORMAL_CUSTOM_TABLE("Normal"),
    RELATION_CUSTOM_TABLE("Relation"),
    
    STRING_TYPE("String"),
    INTEGER_TYPE("Integer"),
    DECIMAL_TYPE("Decimal"),
    BOOLEAN_TYPE("Boolean"),
    SELECT_TYPE("Select"),
    CHOICE_TYPE("Choice"),
    CHECKBOX_TYPE("Checkbox"),
    DATE_TYPE("Date"),
    DATE_TIME_TYPE("Date Time"),
    TIMESTAMP_TYPE("Timestamp"),
    LIST_TYPE("List"),
    TAG_TYPE("Tag"),
    REFERENCE_TYPE("Reference"),
    LINK_TYPE("Link"),
    JOURNAL_TYPE("Journal"),    
    SEQUENCE_TYPE("Sequence"),
    PASSWORD_TYPE("Password"),
    HIDDEN_TYPE("Hidden"),
    TAGS("Tags"),   
    
    //Field type String length
    SMALL_40("Small (40)"),
    MEDIUM_333("Medium (333)"),
    LARGE_4000("Large (4000)"),
    EXTRA_LARGE("Extra Large (>4000)"),
    SMALL_40_VALUE("40"),
    MEDIUM_333_VALUE("333"),
    LARGE_4000_VALUE("4000"),
    EXTRA_LARGE_VALUE("5000"),
    
    //sys field display name
    SYS_ID_DISPLAY("ID"),
    SYS_CREATED_BY_DISPLAY("Created By"),
    SYS_CREATED_ON_DISPLAY("Created On"),
    SYS_UPDATED_BY_DISPLAY("Updated By"),
    SYS_PERMISSION_DISPLAY("Permission Id"),
    SYS_ORGANIZATION_DISPLAY("Organization"),
    SYS_UPDATED_ON_DISPLAY("Updated On"),
    SYS_MOD_COUNT_ON_DISPLAY("Count"),
    
    //column types for custom table
    CUSTOMTABLE_TYPE_STRING("string"),
    CUSTOMTABLE_TYPE_INTEGER("integer"),
    CUSTOMTABLE_TYPE_LONG("long"),
    CUSTOMTABLE_TYPE_FLOAT("float"),
    CUSTOMTABLE_TYPE_DOUBLE("double"),
    CUSTOMTABLE_TYPE_BOOLEAN("boolean"),
    CUSTOMTABLE_TYPE_BYTE("byte"),
    CUSTOMTABLE_TYPE_TIMESTAMP("timestamp"),
    CUSTOMTABLE_TYPE_CLOB("materialized_clob"),
    CUSTOMTABLE_TYPE_BLOB("blob"),
    CUSTOMTABLE_TYPE_BIGDECIMAL("big_decimal"),
    
    DEFAULT("Default"),
    META_TABLE_VIEW_SHARED("Shared"),
    META_TABLE_VIEW_USERS("Self"),

    TARGET_APPLICATION_TAB("default"),// application tab --> NULL, this is default
    TARGET_SAME_WINDOW("_self"),// same window --> _self
    TARGET_BROWSER_TAB("_blank"),// browser tab --> _blank
    
    EXECUTE_CLOSE_ACTION("CLOSE_ACTION"),

    ////////////////
    //CRUDAction.java
    DELETE("DELETE"),
    
    ////////////////
    //from WorksheetConstants
    //url tag
    WS_ACTIVE("ACTIVE"),
    WS_CREATE("CREATE"),
    WS_NEGATIVE_ONE("-1"),
    WS_GENERAL_WN_CREATE_DATE("createDate"),
    WS_GENERAL_WN_USER_ID("userid"),
    WS_GENERAL_WN_DETAIL("worknotesDetail"),
    
    //////////////////////////////////////////////////////////////////////////////////////
    //import export
    IMPEX_MODEL_MODULE("module"),
    IMPEX_MODEL_MODIFIED_BY("modifiedBy"),
    IMPEX_MODEL_MODIFIED_ON("modifiedOn");
    
    
    
    //definition of the
    private final String tagName;
    
    HibernateConstantsEnum(String tagName)
    {
        this.tagName = tagName;
    }
    
    public String getTagName()
    {
        return this.tagName;
    }

}
