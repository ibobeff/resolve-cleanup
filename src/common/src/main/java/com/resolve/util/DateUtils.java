/******************************************************************************
* (C) Copyright 2014
*
* Resolve Systems, LLC
*
* All rights reserved
* This software is distributed under license and may not be used, copied,
* modified, or distributed without the express written permission of
* Resolve Systems, LLC.
*
******************************************************************************/

package com.resolve.util;

import java.sql.Timestamp;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.ZoneId;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.TimeZone;

import javax.xml.datatype.DatatypeConfigurationException;
import javax.xml.datatype.DatatypeFactory;
import javax.xml.datatype.XMLGregorianCalendar;

import org.joda.time.DateTime;
import org.joda.time.DateTimeZone;
import org.joda.time.format.DateTimeFormat;
import org.joda.time.format.ISODateTimeFormat;

public class DateUtils
{
    private static final SimpleDateFormat FORMAT_yyMMddHHmmssz = new SimpleDateFormat("yy-MM-dd HH:mm:ss z");
    private static final SimpleDateFormat FORMAT_yyyyMMddHHmm = new SimpleDateFormat("yyyy-MM-dd HH:mm");
    private static final SimpleDateFormat FORMAT_yyyyMMddHHmmss = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
    private static final SimpleDateFormat FORMAT_yyyyMMddHHmmssSSS = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss,SSS");
    private static final SimpleDateFormat FORMAT_yyyyMMddTHHmmssSSSZ = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSS'Z'");
    private static final SimpleDateFormat FORMAT_yyyyMMddTHHmmssSSSRFC822 = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSSZ");
    private static final SimpleDateFormat FORMAT_yyyyMMddTHHmmssZ = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss'Z'");
    private static final DateFormat DEFAULT_FORMATTER = new SimpleDateFormat("EEE dd MMM yyyy, HH:mm:ss");
    private static final DateFormat ddMMyyyy_Format = new SimpleDateFormat("dd/MM/yyyy");
    private static final DateFormat MMddyyyyHHmm_Format = new SimpleDateFormat("MM/dd/yyyy HH:mm");
    private static final SimpleDateFormat SOCIAL_DATE_FORMAT = new SimpleDateFormat("MMM dd, yyyy");
    private static final ArrayList<DateFormat> formatterList;

  //Creates a list of dateformats
    static
    {
        formatterList = new ArrayList<DateFormat>();
        formatterList.add(FORMAT_yyyyMMddTHHmmssSSSZ);
        formatterList.add(FORMAT_yyyyMMddTHHmmssSSSRFC822);
        formatterList.add(FORMAT_yyyyMMddTHHmmssZ);
        formatterList.add(FORMAT_yyyyMMddHHmmssSSS);
        formatterList.add(FORMAT_yyMMddHHmmssz);
        formatterList.add(FORMAT_yyyyMMddHHmmss);
        formatterList.add(FORMAT_yyyyMMddHHmm);
        formatterList.add(DEFAULT_FORMATTER);
        formatterList.add(DateFormat.getDateInstance(DateFormat.FULL));
        formatterList.add(DateFormat.getDateInstance(DateFormat.LONG));
        formatterList.add(DateFormat.getDateInstance(DateFormat.MEDIUM));
        formatterList.add(DateFormat.getDateInstance(DateFormat.SHORT));
        formatterList.add(ddMMyyyy_Format);
    }
    
    public static DateTime getISODate(long currentTimeMillis)
    {
        return new DateTime(currentTimeMillis, DateTimeZone.UTC);
    } // getISODate

    public static String getISODateString(long currentTimeMillis)
    {
        return new DateTime(currentTimeMillis, DateTimeZone.UTC).toString();
    } // getISODateString

    public static Date getDate(Long currentTimeMillis)
    {
        Date result = null;
        if(currentTimeMillis != null)
        {
            result = getISODate(currentTimeMillis).toDate();
        }
        return result;
    } // getISODate

    /**
     * Converts date to GMT string
     *
     * @param fromDateStr
     *            - yyyy-MM-dd HH:mm:ss , eg - 2010-03-15 00:00:00
     * @param clientTimeZone
     *            - GMT-07:00
     * @return - 2010-03-15 07:00:00
     */
    public static String convertDateToGMTString(String fromDateStr, String clientTimeZone)
    {
        String gmtDate = "";

        String localTimeStr = fromDateStr;
        Timestamp tsGMTTime = convertLocalToGMTTime(localTimeStr, clientTimeZone, true);

        gmtDate = FORMAT_yyyyMMddHHmmss.format(tsGMTTime);

        return gmtDate;
    }

    public static String convertTimeInMillisToLocalString(long timeInMillis, String tzName)
    {
        String localTimeString = "";
        TimeZone tz = TimeZone.getTimeZone(tzName.trim());
        GregorianCalendar calendar = new GregorianCalendar(tz);
        calendar.setTimeInMillis(timeInMillis);

        localTimeString = FORMAT_yyyyMMddHHmmss.format(calendar.getTime());

        return localTimeString;
    }

    public static String convertDateToSalesforceFormat(String fromDateStr, String clientTimeZone)
    {// This is just like GMT, except with a T between days and hours, displying
     // the fractions of seconds, and a Z at the end.
        String gmtDate = "";

        String localTimeStr = fromDateStr;
        Timestamp tsGMTTime = convertLocalToGMTTime(localTimeStr, clientTimeZone, true);

        gmtDate = FORMAT_yyyyMMddTHHmmssSSSZ.format(tsGMTTime);

        return gmtDate;
    }

    public static String convertDateToXMLFormat(Date date)
    {
        return FORMAT_yyyyMMddTHHmmssZ.format(date);
    }

    public static Timestamp convertLocalToGMTTime(String timestr, String tzName, boolean useDaylightTime)
    {
        // if (timestr.length() == "yyyy-MM-dd hh:mm".length())
        // {
        // timestr = timestr + ":00";
        // }
        Timestamp ts = null;
        try
        {
            ts = Timestamp.valueOf(timestr);
        }
        catch (Exception e)
        {
        }
        return convertLocalToGMTTime(ts, tzName, useDaylightTime);
    }

    public static Timestamp convertLocalToGMTTime(Timestamp ts, String tzName, boolean useDaylightTime)
    {
        // set to current time if null
        if (ts == null)
        {
            ts = new Timestamp(System.currentTimeMillis());
        }

        TimeZone tz = TimeZone.getTimeZone(tzName.trim());
        GregorianCalendar calendar = new GregorianCalendar(tz);
        calendar.setTime(ts);

        int daylightOffSet = calendar.get(Calendar.ZONE_OFFSET);
        if (useDaylightTime)
        {
            daylightOffSet += calendar.get(Calendar.DST_OFFSET);
        }

        calendar.add(Calendar.MILLISECOND, (-1) * daylightOffSet);
        Timestamp newts = new Timestamp((calendar.getTime()).getTime());

        // Timestamp newts = new Timestamp(GMTDate.getTime(ts.getTime()));

        return newts;

    }

    public static Timestamp convertGMTToLocalTime(Timestamp ts, String tzName, boolean useDaylightTime)
    {
        TimeZone tz = TimeZone.getTimeZone(tzName.trim());
        GregorianCalendar calendar = new GregorianCalendar(tz);
        calendar.setTime(ts);
        // calendar.setTimeInMillis(millis)

        int daylightOffSet = calendar.get(15);
        if (useDaylightTime)
        {
            daylightOffSet += calendar.get(16);
        }
        calendar.add(14, daylightOffSet);
        Timestamp newts = new Timestamp(calendar.getTime().getTime());

        return newts;
    }

    public static Timestamp convertGMTToLocalTime(Date date, String tzName, boolean useDaylightTime)
    {
        TimeZone tz = TimeZone.getTimeZone(tzName.trim());
        GregorianCalendar calendar = new GregorianCalendar(tz);
        calendar.setTimeInMillis(date.getTime());

        int daylightOffSet = calendar.get(15);
        if (useDaylightTime)
        {
            daylightOffSet += calendar.get(16);
        }
        calendar.add(14, daylightOffSet);
        Timestamp newts = new Timestamp(calendar.getTime().getTime());

        return newts;
    }

    public static String convertGMTToLocalTimeInMilliSecondsString(Timestamp ts, String tzName, boolean useDaylightTime)
    {
        Timestamp newts = convertGMTToLocalTime(ts, tzName, useDaylightTime);
        return FORMAT_yyyyMMddHHmmssSSS.format(newts);
    }

    public static String convertGMTToLocalTimeString(Timestamp ts, String tzName, boolean useDaylightTime)
    {
        Timestamp newts = convertGMTToLocalTime(ts, tzName, useDaylightTime);
        return FORMAT_yyyyMMddHHmm.format(newts);
    }

    public static String convertTimestampToLocalTimeString(Timestamp ts, String tzName, boolean useDaylightTime)
    {
        Timestamp newts = convertGMTToLocalTime(ts, tzName, useDaylightTime);
        return FORMAT_yyyyMMddHHmm.format(newts);
    }

    public static String convertGMTToLocalTimeString(Timestamp ts, String tzName, boolean useDaylightTime, String format)
    {
        SimpleDateFormat sf = null;
        try
        {
            sf = new SimpleDateFormat(format);
        }
        catch (Exception e)
        {
            e.printStackTrace();
        }
        Timestamp newts = convertGMTToLocalTime(ts, tzName, useDaylightTime);
        return sf.format(newts);
    }

    /**
     * Returns the GMT time for a particulat timezone
     *
     * @param timeZone
     * @param useDayLightTime
     * @return
     */
    public static Date getCurrentDateInGMT(String timeZone, boolean useDayLightTime)
    {
        Date curr = new Date();

        Timestamp ts = convertLocalToGMTTime(new Timestamp(curr.getTime()), timeZone, useDayLightTime);

        return new Date(ts.getTime());
    }

    /**
     * Formats the date into string in the following format
     *
     *
     *
     * @param date
     * @return
     */
    public static String convertDateToStringDEFAULT(Date date)
    {
        if (date != null)
        {
            return DEFAULT_FORMATTER.format(date);
        }
        else
        {
            return "";
        }
    }

    /**
     * Formats the date into string in the incoming format
     *
     * @param date
     * @param format example "yyyyMMdd", "MM/dd/yyyy hh:mm:sss" etc.
     * @return
     */
    public static String convertDateToString(Date date, String format)
    {
        String result = "";
        if (date != null)
        {
            DateFormat df = new SimpleDateFormat(format);
            result = df.format(date);
        }
        return result;
    }

    public static String getGMTDateIn_YYYYMMDDmmssFormat()
    {
        String gmtDate = "";

        gmtDate = FORMAT_yyyyMMddHHmmss.format(GMTDate.getDate());

        return gmtDate;
    }

    /**
     * Check if a string matches one of many valid common dates if it does it will
     * return the common its Date object. Otherwise it will return null.
     *
     * @param date 
     * @return Date object representing its detected date format
     */
    public static Date convertStringToDate(String dateString)
    {
        Date returnDate = null;
  
        //This was too linient , single numbers were accepted as dates.
//        //try ISO parsing first
//        try
//        {
//            return parseISODate(dateString).toDate();
//        }
//        catch( IllegalArgumentException e)
//        {
//            //Was not an iso format will ignore exception and move on to other formats
//        }
        
        for(DateFormat format: formatterList)
        {
            try
            {
                format.setLenient(false);
                returnDate = format.parse(dateString);
                return returnDate;
            }
            catch(Exception e)
            {
                //This means there was no match
                //Just ignore exception and try another format
            }
        }
        
        return returnDate;
    }

    public static Date convertStringToDate(String dateString, DateFormat formatter, String userTimezone)
    {
        Date date = null;
        try
        {
            formatter.setTimeZone(TimeZone.getTimeZone(userTimezone));
            System.out.println("timezone -->" + formatter.getTimeZone());
            date = formatter.parse(dateString);
        }
        catch (ParseException e)
        {
            e.printStackTrace();
        }
        return date;
    }

    public static Timestamp convertStringToTimestamp(String dateString, DateFormat formatter)
    {
        Timestamp date = null;
        try
        {
            date = new Timestamp(formatter.parse(dateString).getTime());
        }
        catch (ParseException e)
        {
            e.printStackTrace();
        }
        return date;
    }

    /**
     * Calculates midnight of the day in which date lies with respect to a time
     * zone.
     **/
    public static Date midnight(Date date, TimeZone tz)
    {
        Calendar cal = new GregorianCalendar(tz);
        cal.setTime(date);
        cal.set(Calendar.HOUR_OF_DAY, 0);
        cal.set(Calendar.MINUTE, 0);
        cal.set(Calendar.SECOND, 0);
        cal.set(Calendar.MILLISECOND, 0);
        return cal.getTime();
    }

    public static Date convertDateToGMT(Date userDate, String userTz)
    {
        TimeZone tzGMT = TimeZone.getTimeZone("GMT");
        DateFormat dfGMT = new SimpleDateFormat("MM/dd/yyyy HH:mm");
        dfGMT.setTimeZone(tzGMT);

        String gmtDateStr = dfGMT.format(userDate);

        return convertStringToDate(gmtDateStr, MMddyyyyHHmm_Format, userTz);
    }

    public static Date convertGMTDateToClientLocalDate(Date gmtDate, String clientTimezone)
    {
        Date clientLocalDate = null;

        if (gmtDate != null)
        {
            TimeZone tz = TimeZone.getTimeZone(clientTimezone.trim());
            GregorianCalendar calendarClient = new GregorianCalendar(tz);
            calendarClient.setTime(GMTDate.getLocalServerDate(gmtDate));
            clientLocalDate = calendarClient.getTime();
        }

        return clientLocalDate;

    }

    /**
     * Check the validity of a date string.
     *
     * @param date
     * @param format
     *            could like yyyMMdd etc.
     * @return
     */
    public static boolean isValid(String date, String format)
    {
        boolean result = true;
        SimpleDateFormat formatter = new SimpleDateFormat(format);
        try
        {
            formatter.setLenient(false);
            formatter.parse(date);
        }
        catch (ParseException e)
        {
            // invalid date.
            result = false;
        }
        return result;
    }

    /**
     * Converts a calendar object to string with the incoming format.
     *
     * @param calendar
     * @param format
     *            any valid ones like "E, MMM dd, yyyy" that produces, Wed, Aug
     *            15, 2012 etc.
     *
     * @return
     */
    public static String convertCalendarToString(Calendar calendar, String format)
    {
        String result = null;
        if (calendar != null && format != null)
        {
            DateFormat dateFormat = new SimpleDateFormat(format);
            result = dateFormat.format(calendar.getTime());
        }
        return result;
    }

    public static String convertTimeInMillisToString(long timeInMillis, String format)
    {
        String result = null;
        if (timeInMillis >= 0 && format != null)
        {
            Calendar calendar = Calendar.getInstance();
            calendar.setTimeInMillis(timeInMillis);
            DateFormat dateFormat = new SimpleDateFormat(format);
            result = dateFormat.format(calendar.getTime());
        }
        return result;
    }

    public static String getCurrentDateTimeAsXmlString()
    {
        String result = null;
        try
        {
            GregorianCalendar gc = new GregorianCalendar();
            DatatypeFactory dtf = DatatypeFactory.newInstance();
            XMLGregorianCalendar xgc = dtf.newXMLGregorianCalendar(gc);

            result = xgc.toXMLFormat();
        }
        catch (DatatypeConfigurationException e)
        {
            Log.log.warn("Error getting the current date and time, " + e.getMessage(), e);
        }

        return result;
    }

    public static long getMillisPerMinute()
    {
        return  org.apache.commons.lang3.time.DateUtils.MILLIS_PER_MINUTE;
    }

    public static long GetUTCDateLong()
    {
        return GetUTCDate().getMillis();
    }

    public static Long getDaysFromUTCDate()
    {
        return ((((GetUTCDateLong()/1000)/60)/60)/24);
    }

    public static DateTime parseISODate(String dateString)
    {
        return DateTime.parse(dateString, ISODateTimeFormat.dateTimeParser()).toDateTime(DateTimeZone.UTC);
    }

    public static DateTime GetUTCDate()
    {
        return new DateTime(DateTimeZone.UTC);
    }

    public static String getSocialDate1(long localDate)
    {
        // TimeZone tz = TimeZone.getTimeZone(tzName.trim());
        // GregorianCalendar calendar = new GregorianCalendar(tz);
        // calendar.setTimeInMillis(timeInMillis);
        String result = SOCIAL_DATE_FORMAT.format(new Date(localDate));
        return result;
    }

    public static String getSocialIsoDateString(long localDate)
    {
        return getSocialIsoDate(localDate).toString();
    }

    public static DateTime getSocialIsoDate(long localDate)
    {
        return new DateTime(localDate, DateTimeZone.UTC);
    }


    public static Date convertLocalToGMT(Date localDate)
    {
        DateTime dateTime = new DateTime(localDate);
        // System.out.println("Current Local Date :" + dateTime.toDate());
        // System.out.println("Current Local Date In Millis :" +
        // dateTime.toDate().getTime());

        DateTime gmt = dateTime.withZone(DateTimeZone.forID("GMT"));
        // System.out.println("GMT Date :" + gmt.toDate());
        // System.out.println("GMT Date In Millis :" + gmt.toDate().getTime());

        Date d = gmt.toDate();

        return d;
    }

    public static Date convertGMTToLocal(long gmtTimeInMillis, String timezoneID)
    {
        DateTime dateTime = new DateTime(gmtTimeInMillis, DateTimeZone.forID("GMT"));
        // System.out.println("Current Local Date :" + dateTime.toDate());
        // System.out.println("Current Local Date In Millis :" +
        // dateTime.toDate().getTime());

        DateTime gmt = dateTime.withZoneRetainFields(DateTimeZone.forID(timezoneID));
        // System.out.println("GMT Date :" + gmt.toDate());
        // System.out.println("GMT Date In Millis :" + gmt.toDate().getTime());

        Date d = gmt.toDate();

        return d;
    }

    public static String getUTCDefaultDate()
    {
        String utcdate = GetUTCDate().toString();
        String date = utcdate .substring(0, utcdate .length()-2) + ":" + utcdate .substring(utcdate .length()-2);
        return date;
    }

    public static String getDateInUTCFormat(Timestamp ts)
    {
        String dateStr = null;

        if(ts != null)
        {
            dateStr = new SimpleDateFormat(Constants.DEFAULT_SQL_DATE_FORMAT).format(ts) + "-0000";
        }

        return dateStr;
    }

    public static String getDateInUTCFormat(Date ts)
    {
        String dateStr = null;

        if(ts != null)
        {
            dateStr = new SimpleDateFormat(Constants.DEFAULT_SQL_DATE_FORMAT).format(ts) + "-0000";
        }

        return dateStr;
    }

    /**
     * Gives the month of year (1 for Jan etc.)
     * 
     * @param timesInMillis
     * @return
     */
    public static int getMonthOfYear(long timesInMillis)
    {
        DateTime date = new DateTime(timesInMillis);
        return date.getMonthOfYear();
    }
    
    /**
     * Get the current from UTC
     * 
     * @return
     */
    public static int getCurrentMonthOfYear()
    {
        return getMonthOfYear(GetUTCDateLong());
    }
    
    public static long getCurrentTimeInMillis()
    {
        return DateTime.now().getMillis();
    }
    
    public static Long convertStringToTimesInMillis(String date, String format)
    {
        Long result = null;
        
        if(StringUtils.isNotBlank(date))
        {
            DateTime dt = DateTime.parse(date, DateTimeFormat.forPattern(format));
            if(dt != null)
            {
                result = dt.getMillis();
            }
        }        
        
        return result;
    }
    
    /**
     * Formats the date into string in the incoming format to UTC time zone
     *
     * @param date
     * @param format example "yyyyMMdd", "MM/dd/yyyy hh:mm:sss" etc. 
     * @return
     */
    public static String convertDateToStringGMT(Date date, String format) {
        if (date == null || StringUtils.isBlank(format)) {
            StringBuilder illegalArgsMsgSB = new StringBuilder();
            
            if (date == null) {
                illegalArgsMsgSB.append("Illegal Argument(s): date");
            }
            
            if (StringUtils.isBlank(format)) {
                if (illegalArgsMsgSB.length() == 0) {
                    illegalArgsMsgSB.append("Illegal Argument(s): format");
                } else {
                    illegalArgsMsgSB.append(", format");
                }
            }
            
            throw new IllegalArgumentException(illegalArgsMsgSB.toString());
        }
        
        DateFormat df = new SimpleDateFormat(format);
        df.setTimeZone(TimeZone.getTimeZone(ZoneId.of("UTC")));
        return df.format(date);
    }
}