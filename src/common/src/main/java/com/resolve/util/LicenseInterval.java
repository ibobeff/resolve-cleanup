/******************************************************************************
* (C) Copyright 2018
*
* Resolve Systems, LLC
*
* All rights reserved
* This software is distributed under license and may not be used, copied,
* modified, or distributed without the express written permission of
* Resolve Systems, LLC.
*
******************************************************************************/

package com.resolve.util;

import java.io.Serializable;
import java.util.Date;

/**
 * This class represents a Resolve License Interval.
 *
 */
public class LicenseInterval implements Serializable, Comparable<LicenseInterval> {
	
	private static final long serialVersionUID = 2138850634460278351L;
	
    private Long startDate;
    private Long endDate;
    private Long incidentCount = UNLIMITED_INCIDENT_COUNT;
    private Long eventCount = UNLIMITED_EVENT_COUNT;
    
    public static final Long UNLIMITED_INTERVAL_COUNT = Long.valueOf(-1L);
    public static final Long UNLIMITED_INCIDENT_COUNT = UNLIMITED_INTERVAL_COUNT;
    public static final Long UNLIMITED_EVENT_COUNT = UNLIMITED_INTERVAL_COUNT;
    
    public static final String LICENSE_INTERVAL_START_SUFFIX = " 00:00:00.000-0000";
    public static final String LICENSE_INTERVAL_END_SUFFIX = " 23:59:59.999-0000";
    public static final Long INTERVAL_END_PERPETUAL_MILLISECONDS = 
    								DateUtils.convertStringToTimesInMillis(
    										"99991231" + LICENSE_INTERVAL_END_SUFFIX,
    										LicenseEnum.DATE_FORMAT_MILLISECONDS_ZONE.getKeygenName());
    
    public LicenseInterval(Long startDate, Long endDate, Long incidentCount, Long eventCount) {
        this.startDate = startDate;
        this.endDate = endDate;
        this.incidentCount = incidentCount;
        this.eventCount = eventCount;
    }

    public Long getStartDate() {
        return startDate;
    }

    public void setStartDate(Long startDate) {
        this.startDate = startDate;
    }

    public Long getEndDate() {
        return endDate;
    }

    public void setEndDate(Long endDate) {
        this.endDate = endDate;
    }

    public Long getIncidentCount() {
        return incidentCount;
    }

    public void setIncidentCount(Long incidentCount) {
        this.incidentCount = incidentCount;
    }

    public Long getEventCount() {
        return eventCount;
    }

    public void setEventCount(Long eventCount) {
        this.eventCount = eventCount;
    }

    @Override
    public String toString() {
        return "License Interval [Start Date=" + 
        	   (startDate == null ? 
        	    "" : DateUtils.convertTimeInMillisToString(startDate, LicenseEnum.DATE_FORMAT_LONG.getKeygenName())) + 
        	   ", endDate=" + 
        	   (endDate == null ? 
        		"" : DateUtils.convertTimeInMillisToString(endDate, LicenseEnum.DATE_FORMAT_LONG.getKeygenName())) + 
        	   ", incidentCount=" + (incidentCount == null ? "" : incidentCount) + 
        	   ", eventCount=" + (eventCount == null ? "" : eventCount) + "]";
    }

	@Override
	public int compareTo(LicenseInterval licenseInterval) {
		if (licenseInterval == null) {
			throw new RuntimeException("Passed null license interval to compareTo()!");
		}
		
		if (startDate == null && licenseInterval.startDate != null) {
			return 1;
		}
		
		if (startDate != null && licenseInterval.startDate == null) {
			return -1;
		}

		if (startDate != null && licenseInterval.startDate != null) {
			return startDate.compareTo(licenseInterval.startDate);
		}
		
		return 0;
	}
	
	// Unique LicenseInterval is hash code of start date in text + end date in text
	public int hashCode() {
		return (new Date(startDate).toString() + new Date(endDate).toString()).hashCode();
	}
}
