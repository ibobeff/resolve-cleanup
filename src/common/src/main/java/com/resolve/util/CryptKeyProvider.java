package com.resolve.util;

import com.resolve.util.CryptUtils.KeyType;

public interface CryptKeyProvider {
	String getKey(KeyType keyType);
}
