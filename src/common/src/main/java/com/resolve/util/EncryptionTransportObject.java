/******************************************************************************
* (C) Copyright 2014
*
* Resolve Systems, LLC
*
* All rights reserved
* This software is distributed under license and may not be used, copied,
* modified, or distributed without the express written permission of
* Resolve Systems, LLC.
*
******************************************************************************/

package com.resolve.util;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

public class EncryptionTransportObject implements Serializable
{
    /**
     * 
     */
    private static final long serialVersionUID = -3310267099701716059L;
    private String encryptedText;

    @Override
    public String toString()
    {
        return this.getPlainText();
    }

    public String toLogString()
    {
        return "<encrypted value>";
    }

    public String getPlainText()
    {
        try
        {
            return CryptUtils.decrypt(this.getEncryptedText());
        }
        catch (Exception e)
        {
            Log.log.debug("Error decrypting encrypted text to plain text", e);
        }
        return null;
    }
    
    public void setPlainText(String text)
    {
        try
        {
            this.encryptedText = CryptUtils.encrypt(text);
        }
        catch (Exception e)
        {
            this.encryptedText = null;
            Log.log.error("Failed to Encrypt Parameter", e);
        }
    }

    public String getEncryptedText()
    {
        return encryptedText;
    }

    public void setEncryptedText(String encryptedText)
    {
        this.encryptedText = encryptedText;
    }
    
    public static List decryptMap(Map map)
    {
        if (map == null) return null;
        List temp = new ArrayList();
        for (Object key : map.keySet())
        {
            if (map.get(key) instanceof EncryptionTransportObject)
            {
                temp.add(key);
                map.put(key, ((EncryptionTransportObject) map.get(key)).getPlainText());
            }
        }
        return temp;
    }

    public static void restoreMap(Map original, List keys)
    {
        if (original != null)
        {
            //restore any encrypted values
            if (keys != null)
            {
                for (Object key : keys)
                {
                    if (original.get(key) != null)
                    {
                        EncryptionTransportObject value = new EncryptionTransportObject();
                        value.setPlainText(original.get(key).toString());
                        original.put(key, value);
                    }
                }
            }
            //encrypt any values marked with RS_
            for (Object key : original.keySet())
            {
                if (key != null && key.toString().startsWith("RS_"))
                {
                    if (original.get(key) != null && original.get(key) instanceof String)
                    {
                        EncryptionTransportObject value = new EncryptionTransportObject();
                        value.setPlainText(original.get(key).toString());
                        original.put(key, value);
                    }
                }
            }
        }
    }
}
