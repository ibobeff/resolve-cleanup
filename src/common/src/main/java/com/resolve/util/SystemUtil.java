/******************************************************************************
* (C) Copyright 2014
*
* Resolve Systems, LLC
*
* All rights reserved
* This software is distributed under license and may not be used, copied,
* modified, or distributed without the express written permission of
* Resolve Systems, LLC.
*
******************************************************************************/

package com.resolve.util;

import java.net.InetAddress;
import java.net.NetworkInterface;
import java.util.Enumeration;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * This class is used for utility related tos a computer system.
 */
public class SystemUtil
{
    private static final String IP_ADDRESS_PATTERN = "\\d{1,3}(?:\\.\\d{1,3}){3}";

    public static String getHostIPAddress() throws Exception
    {
        StringBuilder result = new StringBuilder();
        StringBuilder interfaces = new StringBuilder();

        try
        {
            Enumeration<NetworkInterface> networkInterfaces = NetworkInterface.getNetworkInterfaces();
            while (networkInterfaces.hasMoreElements())
            {
                NetworkInterface networkInterface = networkInterfaces.nextElement();
                //if (networkInterface.isUp())
                {
                    Enumeration<InetAddress> inetAddresses = networkInterface.getInetAddresses();
                    while (inetAddresses.hasMoreElements())
                    {
                        InetAddress addr = inetAddresses.nextElement();
                        //if (!addr.isLoopbackAddress())
                        {
                            interfaces.append(addr.getHostAddress() + " ");
                        }
                    }
                }
            }
            if(interfaces.length() > 0)
            {
                //extract only IP addresses, we'll refactor this later.
                Pattern compiledPattern = Pattern.compile(IP_ADDRESS_PATTERN);
                Matcher matcher = compiledPattern.matcher(interfaces);
                while (matcher.find()) {
                    result.append(matcher.group() + ",");
                }
                result.setLength(result.length() - 1);
            }
        }
        catch (Exception e)
        {
            Log.log.error("Could not retrieve Host IP address(es)\n" + e.getMessage());
            throw new Exception("Could not retrieve Host IP address(es)\n" + e.getMessage());
        }

        return result.toString();
    }

    public static int getCPUCount()
    {
        return Runtime.getRuntime().availableProcessors();
    }

    public static long getMaxMemoryHeap()
    {
        // MemoryMXBean memoryBean = ManagementFactory.getMemoryMXBean();
        // return memoryBean.getHeapMemoryUsage().getMax();
        return Runtime.getRuntime().maxMemory();
        // return Runtime.getRuntime().totalMemory();
    }
/*
    public static void main(String[] args) throws Exception
    {
        System.out.println("Processors: " + getCPUCount());
        System.out.println("IP Address: " + getHostIPAddress());

        long maxMemory = getMaxMemoryHeap();
        // long maxMemoryKB = maxMemory / 1024;
        long maxMemoryMB = (maxMemory / 1000) / 1000;
        System.out.println("Max Heap:" + maxMemory + ":" + maxMemoryMB);
    }
*/
}
