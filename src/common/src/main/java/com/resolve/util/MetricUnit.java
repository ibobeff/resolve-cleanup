/******************************************************************************
* (C) Copyright 2014
*
* Resolve Systems, LLC
*
* All rights reserved
* This software is distributed under license and may not be used, copied,
* modified, or distributed without the express written permission of
* Resolve Systems, LLC.
*
******************************************************************************/

package com.resolve.util;

import java.io.Serializable;

public class MetricUnit implements Serializable
{
    private static final long serialVersionUID = -5931759762351564869L;
	final String name;
    final long total;
    final long count;
    long delta;

    public MetricUnit(String name, long value)
    {
        this(name, value, 1);
    } // MetricUnit

    public MetricUnit(String name, long value, long count)
    {
        this.name = name;
        this.total = value;
        this.count = count;

    } // MetricUnit

    public MetricUnit(String name, long value, long count, long delta)
    {
        this.name = name;
        this.total = value;
        this.count = count;
        this.delta = delta;
    } // MetricUnit

    public String toString()
    {
        return "name: " + name + " total: " + total + " count: " + count + " delta: " + delta;
    } // toString

    public static MetricUnit sum(final MetricUnit u1, final MetricUnit u2)
    {
        MetricUnit result = null;
        if (u1.name.equals(u2.name))
        {
            result = new MetricUnit(u1.name, u1.total + u2.total, u1.count + u2.count);
        }
        else
        {
            Log.log.error(new Exception("MetricUnit name mismatched"));
        }
        return result;
    } // sum

    public String getName()
    {
        return name;
    }

    public long getTotal()
    {
        return total;
    }

    public long getCount()
    {
        return count;
    }

    public long getDelta()
    {
        return delta;
    }

    public void setDelta(long delta)
    {
        this.delta = delta;
    }

} // MetricUnit
