/******************************************************************************

(C) Copyright 2006
 
Resolve Systems, Inc.
 Delaware, USA

This software may not be given to any party other than authorised
employees of Resolve Systems, Inc.

******************************************************************************/

package com.resolve.util.restclient;

import java.net.URL;
import java.util.HashSet;
import java.util.Set;

import com.resolve.util.ConfigMap;
import com.resolve.util.StringUtils;
import com.resolve.util.XDoc;

public class ConfigProxy extends ConfigMap
{
    private static final long serialVersionUID = -3048175753274153066L;
    
    public static final String PROXY_AUTHENTICATION_TYPE_NONE = "NONE";
    public static final String PROXY_AUTHENTICATION_TYPE_BASIC = "BASIC";
    public static final String PROXY_AUTHENTICATION_TYPE_NTLM = "NTLM";
    public static final String PROXY_AUTHENTICATION_TYPE_KERBEROS = "KERBEROS";
    
    private static final String HOST_NAMES_SEPARATOR = ",";
    private static final String HOST_NAME_WILDCARD_CHAR = "*";
    
    public boolean proxyEnabled = false;
    public String proxyHost = "127.0.0.1";
    public int proxyPort = -1;
    public String proxyUser = "";
    public String proxyP_assword = "";
    public String authType = PROXY_AUTHENTICATION_TYPE_NONE;
    public String ntlmWorkStation = "";
    public String ntlmDomain = "";
    public String nonProxyHostList = "";
    private Set<String> nonProxyHosts = new HashSet<String>();
    private Set<String> nonProxyHostsStartsWith = new HashSet<String>();
    private Set<String> nonProxyHostsEndsWith = new HashSet<String>();
    
    public ConfigProxy(XDoc config) throws Exception
    {
        super(config);
        
        define("proxyEnabled", BOOLEAN, "./PROXY/@ENABLED");
        define("proxyHost", STRING, "./PROXY/@HOST");
        define("proxyPort", INTEGER, "./PROXY/@PORT");
        define("proxyUser", STRING, "./PROXY/@USER");
        define("proxyP_assword", SECURE, "./PROXY/@PASSWORD");
        define("authType", STRING, "./PROXY/@AUTHTYPE");
        define("ntlmWorkStation", STRING, "./PROXY/NTLM/@WORKSTATION");
        define("ntlmDomain", STRING, "./PROXY/NTLM/@DOMAIN");
        define("nonProxyHostList", STRING, "./PROXY/@NONPROXYHOSTS");
    } // ConfigProxy
    
    public void load()
    {
        loadAttributes();    
    } // load

    public void save()
    {
        saveAttributes();
    } // save
    
    @Override
    public void start() throws Exception
    {
        if (StringUtils.isNotBlank(nonProxyHostList))
        {
            String[] hostNames = nonProxyHostList.split(HOST_NAMES_SEPARATOR);
            
            for (int i = 0; i < hostNames.length; i++)
            {
                String hostName = hostNames[i].trim();
                
                if (hostName.indexOf(HOST_NAME_WILDCARD_CHAR) == -1)
                {
                    nonProxyHosts.add(hostName);
                }
                else if (hostName.startsWith(HOST_NAME_WILDCARD_CHAR) &&
                         !hostName.endsWith(HOST_NAME_WILDCARD_CHAR))
                {
                    int lastIndx = hostName.lastIndexOf(HOST_NAME_WILDCARD_CHAR);
                    
                    if (lastIndx < hostName.length())
                    {
                        String tmpHostName = hostName.substring(lastIndx + 1);
                        
                        if (StringUtils.isNotBlank(tmpHostName))
                        {
                            nonProxyHostsEndsWith.add(tmpHostName);
                        }
                    }
                }
                else if (hostName.endsWith(HOST_NAME_WILDCARD_CHAR) &&
                         !hostName.startsWith(HOST_NAME_WILDCARD_CHAR))
                {
                    int firstIndx = hostName.indexOf(HOST_NAME_WILDCARD_CHAR);
                    
                    if (firstIndx > 0)
                    {
                        String tmpHostName = hostName.substring(0, firstIndx);
                        
                        if (StringUtils.isNotBlank(tmpHostName))
                        {
                            nonProxyHostsStartsWith.add(tmpHostName);
                        }
                    }
                }
            }
        }
    } // start
    
    public boolean isProxyEnabled()
    {
        return proxyEnabled;
    }
    
    public void setProxyEnabled(boolean proxyEnabled)
    {
        this.proxyEnabled = proxyEnabled;
    }
    
    public String getProxyHost()
    {
        return proxyHost;
    }
    
    public void setProxyHost(String proxyHost)
    {
        this.proxyHost = proxyHost;
    }
    
    public int getProxyPort()
    {
        return proxyPort;
    }
    
    public void setProxyPort(int proxyPort)
    {
        this.proxyPort = proxyPort;
    }
    
    public String getProxyUser()
    {
        return proxyUser;
    }
    
    public void setProxyUser(String proxyUser)
    {
        this.proxyUser = proxyUser;
    }
    
    public String getProxyP_assword()
    {
        return proxyP_assword;
    }
    
    public void setProxyP_assword(String proxyP_assword)
    {
        this.proxyP_assword = proxyP_assword;
    }
    
    public String getAuthType()
    {
        return authType;
    }
    
    public void setAuthType(String authType)
    {
        this.authType = authType;
    }
    
    public String getNtlmWorkStation()
    {
        return ntlmWorkStation;
    }
    
    public void setNtlmWorkStation(String ntlmWorkStation)
    {
        this.ntlmWorkStation = ntlmWorkStation;
    }
    
    public String getNtlmDomain()
    {
        return ntlmDomain;
    }
    
    public void setNtlmDomain(String ntlmDomain)
    {
        this.ntlmDomain = ntlmDomain;
    }
    
    public String getNonProxyHostList()
    {
        return nonProxyHostList;
    }
    
    public void setNonProxyHostList(String nonProxyHostList)
    {
        this.nonProxyHostList = nonProxyHostList;
    }
    
    public boolean isNonProxyHost(URL url)
    {
        boolean isNonProxyHost = false;
        
        if (url != null && StringUtils.isNotBlank(url.getHost()))
        {
            if (nonProxyHosts.contains(url.getHost()))
            {
                isNonProxyHost = true;
            }
            
            if (!isNonProxyHost && !nonProxyHostsStartsWith.isEmpty())
            {
                for (String startsWithHostName : nonProxyHostsStartsWith)
                {
                    if (url.getHost().startsWith(startsWithHostName))
                    {
                        isNonProxyHost = true;
                        break;
                    }
                }
            }
            
            if (!isNonProxyHost && !nonProxyHostsEndsWith.isEmpty())
            {
                for (String endsWithHostName : nonProxyHostsEndsWith)
                {
                    if (url.getHost().endsWith(endsWithHostName))
                    {
                        isNonProxyHost = true;
                        break;
                    }
                }
            }
        }
        
        return isNonProxyHost;
    }
} // ConfigProxy
