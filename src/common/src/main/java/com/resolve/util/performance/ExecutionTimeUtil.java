package com.resolve.util.performance;

import com.resolve.util.performance.datasources.ExecutionStep;
import com.resolve.util.performance.datasources.ExecutionStepPhase;
import com.resolve.util.performance.datasources.RunbookExecutionTimeDataSource;

/**
 * The performance metrics utility is used to track execution time statistics
 * for the runbook execution flow. The implementation is pluggable and may use
 * different data sources for writing of execution time data.
 */
public class ExecutionTimeUtil {

	private static RunbookExecutionTimeDataSource outputDataSource;

	public static void setOutputDataSource(RunbookExecutionTimeDataSource outputDataSource) {
		ExecutionTimeUtil.outputDataSource = outputDataSource;
	}
	
	/**
	 * Logs details about the particular execution event used to measure exact duration (compared to the previous logged event).
	 *  
	 * @param messageId The runbook ID (the messageID coming from RSMQ)
	 * @param runbookId sys_id of the runbook being executed (processid) or NULL
	 * @param actionTaskId sys_id of the action task being executed or NULL
	 * @param step the particular {@link ExecutionStep} that identifies the logged event. If {@link ExecutionStep#METHOD_EXECUTION} is specified 
	 * then it needs to be logged with an {@link ExecutionStepPhase#START} phase at the beginning of a method and an {@link ExecutionStepPhase#END} phase 
	 * at the end of a method execution
	 * @param phase the particular {@link ExecutionStepPhase} that identifies the exact phase of the execution event
	 */
	public static void log(String messageId, String runbookId, String actionTaskId, ExecutionStep step,	ExecutionStepPhase phase) {
		if(outputDataSource != null) {
			outputDataSource.log(messageId, runbookId, actionTaskId, step, phase);
		}
	}

	/**
	 * Exports the execution time statistics to an output data sources. Export semantics is identified by the specified data 
	 * source that can be enabled in the blueprint configuration. 
	 * 
	 * @param messageId The runbook ID (the messageID coming from RSMQ)
	 * @see ExecutionTimeUtil#setOutputDataSource()
	 */
	public static void exportExecutionTimeStatistics(String messageId) {
		if(outputDataSource != null) {
			outputDataSource.exportExecutionTimeStatistics(messageId);
		}
	}
	
	public static RunbookExecutionTimeDataSource getOutputDataSource() {
		return outputDataSource;
	}

}
