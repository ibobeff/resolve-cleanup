/******************************************************************************
 * (C) Copyright 2014
 *
 * Resolve Systems, LLC
 *
 * All rights reserved
 * This software is distributed under license and may not be used, copied,
 * modified, or distributed without the express written permission of
 * Resolve Systems, LLC.
 *
 ******************************************************************************/

package com.resolve.util;

import java.io.File;
import java.io.FileFilter;
import java.nio.charset.StandardCharsets;
import java.nio.file.FileSystems;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.StandardCopyOption;
import java.time.Instant;
import java.util.Arrays;
import java.util.HashSet;
import java.util.Set;
import java.util.stream.Collectors;

import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.io.filefilter.WildcardFileFilter;
import org.apache.commons.lang3.tuple.Pair;

import com.resolve.rsbase.MainBase;

/**
 * This is a utility class for resolve license validation.
 */
public class LicenseUtil
{
	private static final String LICENSE_FILE_WILDCARD_FILTER = "*.lic";
	private static final String LICENSE_REPOSITORY_READ_WRITE_LOCK_WILDCARD_FILTER = "*-*.*lock";
	public static final String LICENSE_REPOSITORY_READ_LOCK_EXTENSION = ".rlock";
	public static final String LICENSE_REPOSITORY_WRITE_LOCK_EXTENSION = ".wlock";
	private static final String WILD_CARD = "*";
	private static final String DASH_SEPARATOR = "-";
	private static final String LICENSE_REPOSITORY_WRITE_LOCK_WILDCARD_FILTER = 
																			WILD_CARD + DASH_SEPARATOR + WILD_CARD + 
																			LICENSE_REPOSITORY_WRITE_LOCK_EXTENSION;
	private static final String LICENSE_REPOSITORY_INSTANCE_ID_LOCK_WILDCARD_FILTER_SUFFIX = ".*lock";
	
	
	public enum LicRepoLockType {
	    READ,
	    WRITE
	}
	
	public static String encryptLicenseContent(String decryptedContent)
	{
		String result = null;
		if(StringUtils.isNotBlank(decryptedContent))
		{
			try
			{
				result = CryptUtils.encryptLicense(decryptedContent);
			}
			catch (Exception e)
			{
				Log.log.error(e.getMessage(), e);
			}
		}
		return result;
	}

	public static Pair<String, String> decryptLicense(File licenseFile)
	{
		String decryptedLic = null;
		String signature = null;
		
		if(licenseFile != null && licenseFile.isFile() && licenseFile.exists()) {
			Log.log.info(String.format("Decrypting license file %s...", licenseFile.getAbsolutePath()));
			try {
				lockLicenseRepository(MainBase.main.configId.getGuid(), LicRepoLockType.READ);
				// Filter out signature if present
				String[] licenseParts = FileUtils.readFileToString(licenseFile, StandardCharsets.UTF_8.name()).split(LicenseEnum.SIGNATURE.getKeygenName(), 2);
				// decrypt license
				decryptedLic = CryptUtils.decryptLicense(licenseParts[0]);
					
				if (licenseFile.getName().startsWith("license")) {
					Log.log.info(String.format("Decrypted contents of %s are [%s].", licenseFile.getName(), decryptedLic));
				}
				signature = licenseParts.length == 2 ? licenseParts[1] : null;
			} catch (Exception e) {
				Log.log.error(e.getMessage(), e);
			} finally {
				try {
					unLockLicenseRepository(MainBase.main.configId.getGuid(), LicRepoLockType.READ);
				} catch (Exception e) {
					Log.log.error(e.getMessage(), e);
				}
			}
		}

		return Pair.of(decryptedLic, signature);
	}

	public static String readLicenseFile(File licenseFile)
	{
		String result = null;
		if(licenseFile != null && licenseFile.isFile() && licenseFile.exists())
		{
			try
			{
				lockLicenseRepository(MainBase.main.configId.getGuid(), LicRepoLockType.READ);
				
				result = FileUtils.readFileToString(licenseFile, StandardCharsets.UTF_8.name());
			}
			catch (Exception e)
			{
				Log.log.warn(e.getMessage(), e);
			} finally {
				try {
					unLockLicenseRepository(MainBase.main.configId.getGuid(), LicRepoLockType.READ);
				} catch (Exception e) {
					Log.log.error(e.getMessage(), e);
				}
			}
		}
		return result;
	}

	public static File[] getLicenseFiles()
	{
		File[] result = new File[0];

		try {
			lockLicenseRepository(MainBase.main.configId.getGuid(), LicRepoLockType.READ);
			
			File dir = FileUtils.getFile(MainBase.main.getResolveHome());
			if (dir != null && dir.isDirectory() && dir.exists())
			{
				FileFilter fileFilter = new WildcardFileFilter(LICENSE_FILE_WILDCARD_FILTER);
				result = dir.listFiles(fileFilter);
			}
		} catch (Exception e) {
			Log.log.error(e.getMessage(), e);
		} finally {
			try {
				unLockLicenseRepository(MainBase.main.configId.getGuid(), LicRepoLockType.READ);
			} catch (Exception e) {
				Log.log.error(e.getMessage(), e);
			}
		}

		return result;
	}
	
	private static Set<File> getLicenseRepositoryLocks(Instant currentInst, String wildCardFilter)
	{
		Set<File> licRepoLocks = new HashSet<File>();
		
		File dir = FileUtils.getFile(MainBase.main.getResolveHome());
		
		if (dir != null && dir.isDirectory() && dir.exists())
		{
			FileFilter fileFilter = new WildcardFileFilter(wildCardFilter);
			File[] lockFiles = dir.listFiles(fileFilter);
			
			// Any read locks older than 2 minutes will be considered as stale and removed
			Instant staleReadLockInst = currentInst.minusSeconds(120);
			// Any write locks older than 30 seconds will be considered as stale and removed
			Instant staleWriteLockInst = currentInst.minusSeconds(30);
			if (lockFiles != null && lockFiles.length > 0) {				
				Arrays.asList(lockFiles)
				.parallelStream()
				.forEach(lockFile -> {
					Instant staleReadOrWriteLockInst = lockFile.getName().endsWith(LICENSE_REPOSITORY_READ_LOCK_EXTENSION) ?
													   staleReadLockInst : staleWriteLockInst;
					
					if (staleReadOrWriteLockInst.isAfter(Instant.ofEpochMilli(lockFile.lastModified()))) {
						// Delete stale lock file
						if (Log.log.isTraceEnabled()) {
							Log.log.trace(String.format("Deleting Stale lock file %s last modified %d.",
														lockFile.getAbsolutePath() , lockFile.lastModified()));
						}
						
						try {
							lockFile.delete();
						} catch (Exception e) {
							Log.log.error(String.format("Error%sin deleting lock file %s.", 
														(StringUtils.isNotBlank(e.getMessage()) ? 
														 " [" + e.getMessage() + "] " : " "), lockFile.getAbsolutePath()), e);
						}
					} else {
						licRepoLocks.add(lockFile);
					}
				});
			}
		}

		return licRepoLocks;
	}
	
	/*
	 * Multiple (re-entrant) read locks are granted to an instance ids and threads
	 * with in those instance ids provided there are no write locks
	 * or sole write lock is held by an instance and thread requesting read lock.  
	 */
	private static void lockLicenseRepositoryForRead(String instanceId) throws Exception {
		int retryCount = 17;
		
		Instant currInst = Instant.now();
		Set<File> licRepoLocks = getLicenseRepositoryLocks(currInst, LICENSE_REPOSITORY_WRITE_LOCK_WILDCARD_FILTER);
				
		while (CollectionUtils.isNotEmpty(licRepoLocks) && retryCount > 0) {
			if (licRepoLocks.size() == 1 && 
				licRepoLocks.iterator().next().getName().startsWith(instanceId + DASH_SEPARATOR + 
																	Thread.currentThread().getId())) {
				if (Log.log.isTraceEnabled()) {
					Log.log.trace(String.format("Instance id %s has sole write lock on license repository, " +
												"granting requested read lock.", 
												instanceId + DASH_SEPARATOR + Thread.currentThread().getId()));
				}
				break;
			}
			
			if (Log.log.isTraceEnabled()) {
				Set<String> writeLockInstanceIds = new HashSet<String>();
				
				writeLockInstanceIds = licRepoLocks.parallelStream()
									   .map(licRepoLock -> licRepoLock.getName()).collect(Collectors.toSet());
				
				Log.log.trace(String.format("License Repository has [%s] write locks, sleep for 5 secs before retrying (count #%d)...", 
											StringUtils.collectionToString(writeLockInstanceIds), (17 - retryCount + 1)));
			}
			
			Thread.sleep(5000);
			licRepoLocks = getLicenseRepositoryLocks(Instant.now(), LICENSE_REPOSITORY_WRITE_LOCK_WILDCARD_FILTER);
			retryCount--;
		} 
		
		if (retryCount <= 0) {
			String errMsg = String.format("Failed to lock license repository for read for instance id %s after witing for " +
				    					  "1 minute 25 seconds!!!", instanceId + DASH_SEPARATOR + Thread.currentThread().getId());
			Log.log.error(errMsg);
			throw new Exception(errMsg);
		} else {
			if (CollectionUtils.isEmpty(licRepoLocks)) {
				File lockFile = FileUtils.getFile(MainBase.main.getResolveHome() + "/" + instanceId + DASH_SEPARATOR + 
												  Thread.currentThread().getId() + LICENSE_REPOSITORY_READ_LOCK_EXTENSION);
				
				if (!lockFile.exists()) {
					FileUtils.writeStringToFile(lockFile, String.format("%d,%d", Integer.MIN_VALUE, Integer.MIN_VALUE), 
												StandardCharsets.UTF_8.name(), false);
				} else {
					// read lock exists
					Instant staleReadLockInst = currInst.minusSeconds(120);
					
					// Reset both read write counts if last modified time is more than 2 minutes
					if (staleReadLockInst.isAfter(Instant.ofEpochMilli(lockFile.lastModified()))) {						
						FileUtils.writeStringToFile(lockFile, String.format("%d,%d", Integer.MIN_VALUE, Integer.MIN_VALUE), 
													StandardCharsets.UTF_8.name(), false);
					} else {
						// touch read lock
						FileUtils.touch(lockFile);
					}
				}
			} else if (licRepoLocks.size() == 1) {
				// touch the sole write lock
				FileUtils.touch(licRepoLocks.iterator().next());
			}
			
			if (Log.log.isTraceEnabled()) {
				Log.log.trace(String.format("Instance id %s granted read lock on license repository.", 
											instanceId + DASH_SEPARATOR + Thread.currentThread().getId()));
			}
		}
	}
	
	private static File moveLock(File lockFile, LicRepoLockType from, LicRepoLockType to) throws Exception {
		File movedLockFile = lockFile;
		
		if (from != to) {
			String lockFromExtn = from == LicRepoLockType.READ ? 
								  LICENSE_REPOSITORY_READ_LOCK_EXTENSION : LICENSE_REPOSITORY_WRITE_LOCK_EXTENSION;
			String lockToExtn = to == LicRepoLockType.READ ? 
					  			LICENSE_REPOSITORY_READ_LOCK_EXTENSION : LICENSE_REPOSITORY_WRITE_LOCK_EXTENSION;
			
			Path oldPath = FileSystems.getDefault().getPath(lockFile.getAbsolutePath());
			
			String debugMsg = null;
			
			if (oldPath.endsWith(lockToExtn)) {
				if (Log.log.isTraceEnabled()) {
					debugMsg = String.format("Lock %s is already of type %s.", oldPath, to.name());
				}
				
				// touch the lock
				FileUtils.touch(movedLockFile);
				return movedLockFile;
			}
			
			Path newPath = FileSystems.getDefault().getPath(StringUtils.substringBefore(lockFile.getAbsolutePath(), 
																						lockFromExtn) +
															lockToExtn);
			
			if (Log.log.isTraceEnabled()) {
				debugMsg = String.format("Rename lock %s to %s ", oldPath, newPath);
			}
			
			lockFile = null;
			
			Path renamedPath = null;
			
			try {
				renamedPath = Files.move(oldPath, newPath, StandardCopyOption.ATOMIC_MOVE, 
										  StandardCopyOption.REPLACE_EXISTING);
			} catch (Throwable t) {
				Log.log.error(String.format("Error%sin moving lock from %s to %s.",
											(StringUtils.isNotEmpty(t.getMessage()) ? " [" + t.getMessage() + "] " : " "),
											oldPath, newPath), t);
				throw t;
			}
			
			if (Log.log.isDebugEnabled()) {
				Log.log.debug(String.format("Moved lock %s to %s.", oldPath, renamedPath));
			}
			
			boolean renamed = renamedPath.toFile().getName().endsWith(lockToExtn) ? true : false;
			
			if (Log.log.isTraceEnabled()) {
				Log.log.trace(String.format("%s%s", debugMsg, (renamed ? "succeeded." : "failed.")));
			}
			
			if (renamed) {
				movedLockFile = newPath.toFile();
				// touch the lock
				FileUtils.touch(movedLockFile);
			} else {
				String errMsg = String.format("Failed to move license repository lock from %s to %s.", oldPath, newPath);
				Log.log.error(errMsg);
				throw new Exception(errMsg);
			}
		}
		
		return movedLockFile;
	}
	
	private static Pair<Integer, Integer> getLockCounts(File lockFile) throws Exception {
		int readLockCnt = 0;
		int writeLockCnt = 0;
		String existingLockData = null;
		String[] existingRWLockCnts = null;
		
		existingLockData = FileUtils.readFileToString(lockFile, StandardCharsets.UTF_8.name());
		
		if (StringUtils.isBlank(existingLockData)) {
			String errMsg = String.format("License Repository lock file %s contains no read write lock counts.", 
					  					  lockFile.getAbsolutePath());
			Log.log.error(errMsg);
			throw new Exception(errMsg);
		}
		
		existingRWLockCnts = existingLockData.split(",");
		
		if (existingRWLockCnts != null && existingRWLockCnts.length == 2) {
			readLockCnt = Integer.parseInt(existingRWLockCnts[0]);
			writeLockCnt = Integer.parseInt(existingRWLockCnts[1]);
			
			if (readLockCnt > Integer.MIN_VALUE && readLockCnt < 0) {
				readLockCnt = 0;
			}
			
			if (writeLockCnt > Integer.MIN_VALUE && writeLockCnt < 0) {
				writeLockCnt = 0;
			}
		} else {
			String errMsg = String.format("License Repository lock file %s contains invalid read write lock counts.", 
										  lockFile.getAbsolutePath());
			Log.log.error(errMsg);
			throw new Exception(errMsg);
		}
		
		return Pair.of(Integer.valueOf(readLockCnt), Integer.valueOf(writeLockCnt));
	}
	
	private static Pair<Integer, Integer> adjustRWLockCount(Pair<Integer, Integer> rwLockCounts, LicRepoLockType licRepoLockType, 
														  int factor) throws IllegalArgumentException {		
		if (rwLockCounts == null || rwLockCounts.getLeft() == null || rwLockCounts.getRight() == null) {
			throw new IllegalArgumentException("Passed null read write lock count object or one or both counts in passed " +
											   "read write lock counts are null.");
		}
		
		int tmpFactor = factor;
		int readCount = rwLockCounts.getLeft().intValue() >= 0 ? rwLockCounts.getLeft().intValue() : 0;
		int writeCount = rwLockCounts.getRight().intValue() >= 0 ? rwLockCounts.getRight().intValue() : 0;
		
		if (licRepoLockType == LicRepoLockType.READ) {
			if ((readCount + tmpFactor) < 0) {
				tmpFactor = 0;
			}
			
			readCount += tmpFactor;
		} else {
			if ((writeCount + tmpFactor) < 0) {
				tmpFactor = 0;
			}
			
			writeCount += tmpFactor;
		}
		
		return Pair.of(Integer.valueOf(readCount), Integer.valueOf(writeCount));
	}
	
	/*
	 * Only single (re-entrant) write lock is allowed for one instance id and one 
	 * thread in that instance.
	 * If instance and thread trying to get write lock has sole read lock then write
	 * lock is granted to that instance.
	 */
	private static void lockLicenseRepositoryForWrite(String instanceId) throws Exception {
		int retryCount = 41;
		
		Instant currInst = Instant.now();
		Set<File> licRepoLocks = getLicenseRepositoryLocks(currInst, LICENSE_REPOSITORY_READ_WRITE_LOCK_WILDCARD_FILTER);
				
		while (CollectionUtils.isNotEmpty(licRepoLocks) && retryCount > 0) {
			if (licRepoLocks.size() == 1 && 
				licRepoLocks.iterator().next().getName().startsWith(instanceId + DASH_SEPARATOR +
																	Thread.currentThread().getId())) {
				if (licRepoLocks.iterator().next().getName().endsWith(LICENSE_REPOSITORY_READ_LOCK_EXTENSION)) {					
					// Rename sole read lock file to write
					
					if (Log.log.isTraceEnabled()) {
						Log.log.trace(String.format("Move sole read lock %s to write lock.", 
													licRepoLocks.iterator().next().getName()));
					}
					
					File renamedLockFile = moveLock(licRepoLocks.iterator().next(), LicRepoLockType.READ, LicRepoLockType.WRITE);
					licRepoLocks.clear();
					licRepoLocks.add(renamedLockFile);
				} else {
					FileUtils.touch(licRepoLocks.iterator().next());
				}
				
				if (Log.log.isTraceEnabled()) {
					Log.log.trace(String.format("Instance id %s had sole lock on license repository, " +
												"granting requested write lock.", 
												instanceId + DASH_SEPARATOR + Thread.currentThread().getId()));
				}
				break;
			}
			
			if (Log.log.isTraceEnabled()) {
				Set<String> lockInstanceIds = new HashSet<String>();
				
				lockInstanceIds = licRepoLocks.parallelStream()
								  .map(licRepoLock -> licRepoLock.getName()).collect(Collectors.toSet());
				Log.log.trace(String.format("License Repository has [%s] lock files, sleep for 5 secs before retrying " +
								  			"(count #%d)...", 
											StringUtils.collectionToString(lockInstanceIds), (41 - retryCount + 1)));
			}
			
			Thread.sleep(5000);
			licRepoLocks = getLicenseRepositoryLocks(Instant.now(), LICENSE_REPOSITORY_READ_WRITE_LOCK_WILDCARD_FILTER);
			retryCount--;
		} 
		
		if (retryCount <= 0) {
			String errMsg = String.format("Failed to lock license repository for write for instance id %s after witing for " +
				    					  "over 2 minutes!!!", instanceId + DASH_SEPARATOR + Thread.currentThread().getId());
			Log.log.error(errMsg);
			throw new Exception(errMsg);
		} else {
			if (CollectionUtils.isEmpty(licRepoLocks)) {
				File lockFile = FileUtils.getFile(MainBase.main.getResolveHome() + "/" + instanceId + DASH_SEPARATOR +
												  Thread.currentThread().getId() + LICENSE_REPOSITORY_WRITE_LOCK_EXTENSION);
				
				if (!lockFile.exists()) {
					FileUtils.writeStringToFile(lockFile, String.format("%d,%d", Integer.MIN_VALUE, Integer.MIN_VALUE), 
												StandardCharsets.UTF_8.name(), false);
				} else {
					// Any read locks older than 2 minutes will be considered as stale and removed
					Instant staleReadLockInst = currInst.minusSeconds(120);
					// Any write locks older than 30 seconds will be considered as stale and removed
					Instant staleWriteLockInst = currInst.minusSeconds(30);
					
					// Reset both read write counts if last modified time is more than 2 minutes
					if (staleReadLockInst.isAfter(Instant.ofEpochMilli(lockFile.lastModified()))) {						
						FileUtils.writeStringToFile(lockFile, String.format("%d,%d", Integer.MIN_VALUE, Integer.MIN_VALUE), 
													StandardCharsets.UTF_8.name(), false);
					} else if (staleWriteLockInst.isAfter(Instant.ofEpochMilli(lockFile.lastModified()))){
						// Reset write counts if last modified time is more than 30 seconds
						
						Pair<Integer, Integer> existingRWLockCnts = getLockCounts(lockFile);
						
						int readLockCnt = 0;
						
						if (existingRWLockCnts != null) {
							readLockCnt = existingRWLockCnts.getLeft().intValue();
							FileUtils.writeStringToFile(lockFile, String.format("%d,%d", readLockCnt, Integer.MIN_VALUE), 
														StandardCharsets.UTF_8.name(), false);
						} else {
							String errMsg = String.format("License Repository lock file %s contains no read write lock counts.", 
														  lockFile.getAbsolutePath());
							Log.log.error(errMsg);
							throw new Exception(errMsg);
						}
					}
					FileUtils.touch(lockFile);
				}
			} else if (licRepoLocks.size() == 1) {
				// touch the sole write lock
				FileUtils.touch(licRepoLocks.iterator().next());
			}
			
			if (Log.log.isTraceEnabled()) {
				Log.log.trace(String.format("Instance id %s granted write lock on license repository.", instanceId));
			}
		}
	}
	
	private static void acquireLock(String instanceId, LicRepoLockType licRepoLockType) throws Exception {
		if (licRepoLockType == LicRepoLockType.READ) {
			lockLicenseRepositoryForRead(instanceId);
		} else {
			lockLicenseRepositoryForWrite(instanceId);
		}
	}
	
	private static File getLastUpdatedLock(File[] instIdLockFiles, String instanceId) {
		File lastUpdatedLock = null;
		
		if (instIdLockFiles != null && instIdLockFiles.length >= 1) {
			if (instIdLockFiles.length > 1) {
				Log.log.warn(String.format("Found %d lock files for instance id %s instead of expected 1.", 
						   				   instIdLockFiles.length, 
						   				   instanceId + DASH_SEPARATOR + Thread.currentThread().getId()));

				long[] latestUpdated = {0l};
				File[] tmpLockFiles = new File[1];

				Arrays.asList(instIdLockFiles)
				.stream()
				.forEach(instIdLockFile -> {
					if (instIdLockFile.lastModified() > latestUpdated[0]) {
						latestUpdated[0] = instIdLockFile.lastModified();
						tmpLockFiles[0] = instIdLockFile;
					}
				});
				
				lastUpdatedLock = tmpLockFiles[0];
			} else {
				lastUpdatedLock = instIdLockFiles[0];
			}
		}
			
		return lastUpdatedLock;
	}
	
	public static void lockLicenseRepository(String instanceId, LicRepoLockType licRepoLockType) throws Exception {
		try {
			/*
			 * Acquire lock on license repository
			 * Strategy - Multiple Read locks, Single Write lock
			 * Both read and write locks are re-entrant
			 */
			
			acquireLock(instanceId, licRepoLockType);
			
			File dir = FileUtils.getFile(MainBase.main.getResolveHome());
			
			if (dir != null && dir.isDirectory() && dir.exists()) {
				FileFilter instIdLockFilter = new WildcardFileFilter(instanceId + DASH_SEPARATOR +
						  											 Thread.currentThread().getId() + 
						  											 LICENSE_REPOSITORY_INSTANCE_ID_LOCK_WILDCARD_FILTER_SUFFIX);
				File[] instIdLockFiles = dir.listFiles(instIdLockFilter);
				
				String lockData = null;
				File lockFile = null;
								
				if (instIdLockFiles != null && instIdLockFiles.length >= 1) {
					if (instIdLockFiles.length > 1) {
						lockFile = getLastUpdatedLock(instIdLockFiles, instanceId);
					} else {
						lockFile = instIdLockFiles[0];
					}
					
					Pair<Integer, Integer> adjustedRWLockCounts = adjustRWLockCount(getLockCounts(lockFile), licRepoLockType, 1);
					
					lockData = String.format("%d,%d", adjustedRWLockCounts.getLeft(), adjustedRWLockCounts.getRight());
					
					if (Log.log.isTraceEnabled()) {
						Log.log.trace(String.format("lockLicenseRepository: License repository lock file %s updated data %s.", 
													lockFile.getAbsolutePath(), lockData));
					}
					
					if (adjustedRWLockCounts.getRight() > 0 && 
						lockFile.getName().endsWith(LICENSE_REPOSITORY_READ_LOCK_EXTENSION)) {
						lockFile = moveLock(lockFile, LicRepoLockType.READ, LicRepoLockType.WRITE);
					} else if (adjustedRWLockCounts.getRight() <= 0 && 
							   adjustedRWLockCounts.getLeft() > 0 && 
							   lockFile.getName().endsWith(LICENSE_REPOSITORY_WRITE_LOCK_EXTENSION)) {
						lockFile = moveLock(lockFile, LicRepoLockType.WRITE, LicRepoLockType.READ);
					}
				} else {
					String errMsg = String.format("Acquired %s lock for instance id %s not found.", licRepoLockType.name(),
												  instanceId + DASH_SEPARATOR + Thread.currentThread().getId());
					Log.log.error(errMsg);
					throw new Exception(errMsg);
				}
				
				if (Log.log.isTraceEnabled()) {
					Log.log.trace(String.format("InstanceId Id Lock File : %s, Lock File Data : %s.", 
												lockFile.getAbsolutePath(), lockData));
				}
				
				FileUtils.writeStringToFile(lockFile, lockData, StandardCharsets.UTF_8.name(), false);
			} else {
				String errMsg = String.format("License Repository %s does not exists.", dir.getAbsolutePath());
				Log.log.error(errMsg);
				throw new Exception(errMsg);
			}
		} catch (Exception e) {
			Log.log.error(String.format("Error%sin locking license repository for instance id %s for lock type %s.", 
										(StringUtils.isNotBlank(e.getMessage()) ? 
										 " [" + e.getMessage() + "] " : " "), 
										instanceId + DASH_SEPARATOR + Thread.currentThread().getId(), licRepoLockType.name()), 
						  e);
			throw e;
		}
	}
	
	public static void unLockLicenseRepository(String instanceId, LicRepoLockType licRepoLockType) throws Exception {
		try {
			File dir = FileUtils.getFile(MainBase.main.getResolveHome());
			
			if (dir != null && dir.isDirectory() && dir.exists()) {
				FileFilter instIdLockFilter = new WildcardFileFilter(instanceId + DASH_SEPARATOR +
							 										 Thread.currentThread().getId() + 
							 										 LICENSE_REPOSITORY_INSTANCE_ID_LOCK_WILDCARD_FILTER_SUFFIX);
				File[] instIdLockFiles = dir.listFiles(instIdLockFilter);
				
				String lockData = null;
				File lockFile = null;
				
				if (instIdLockFiles != null && instIdLockFiles.length >= 1) {
					if (instIdLockFiles.length > 1) {
						lockFile = getLastUpdatedLock(instIdLockFiles, instanceId); //tmpLockFiles[0];
					} else {
						lockFile = instIdLockFiles[0];
					}
					
					Pair<Integer, Integer> adjustedRWLockCounts = adjustRWLockCount(getLockCounts(lockFile), licRepoLockType, -1);
					
					lockData = String.format("%d,%d", adjustedRWLockCounts.getLeft(), adjustedRWLockCounts.getRight());
					
					if (Log.log.isTraceEnabled()) {
						Log.log.trace(String.format("unLockLicenseRepository: License repository lock file %s updated data %s.", 
													lockFile.getAbsolutePath(), lockData));
					}
					
					boolean renamed = false;
					boolean deleted = false;
					
					if (adjustedRWLockCounts.getRight() > 0) {
						if (lockFile.getName().endsWith(LICENSE_REPOSITORY_READ_LOCK_EXTENSION)) {					
							lockFile = moveLock(lockFile, LicRepoLockType.READ, LicRepoLockType.WRITE);
						}
						
						FileUtils.writeStringToFile(lockFile, lockData, StandardCharsets.UTF_8.name(), false);
					} else if (adjustedRWLockCounts.getRight() <= 0 && adjustedRWLockCounts.getLeft() > 0) {
						if (lockFile.getName().endsWith(LICENSE_REPOSITORY_WRITE_LOCK_EXTENSION)) {
							lockFile = moveLock(lockFile, LicRepoLockType.WRITE, LicRepoLockType.READ);
						}
						
						FileUtils.writeStringToFile(lockFile, lockData, StandardCharsets.UTF_8.name(), false);
					} else if (adjustedRWLockCounts.getLeft() >= (Integer.MIN_VALUE + 1) && 
							   adjustedRWLockCounts.getLeft() <= 0 && 
							   adjustedRWLockCounts.getRight() >= (Integer.MIN_VALUE + 1) && 
							   adjustedRWLockCounts.getRight() <= 0) {
						String debugMsg = null;
						if (Log.log.isTraceEnabled()) {
							debugMsg = String.format("Delete lock %s ", lockFile.getName());
						}
						
						deleted = lockFile.delete();
						
						if (Log.log.isTraceEnabled()) {
							Log.log.trace(String.format("%s%s", debugMsg, (deleted ? "succeeded." : "failed.")));
						}
					}
				} else {
					Log.log.warn(String.format("No lock by instance id %s found on license repository.",
											   instanceId + DASH_SEPARATOR + Thread.currentThread().getId()));
				}
			} else {
				String errMsg = String.format("License Repository %s does not exists.", dir.getAbsolutePath());
				Log.log.error(errMsg);
				throw new Exception(errMsg);
			}
		} catch (Exception e) {
			Log.log.error(String.format("Error%sin un-locking license repository %s %s lock.", 
										(StringUtils.isNotBlank(e.getMessage()) ? 
										 " [" + e.getMessage() + "] " : " "), instanceId, licRepoLockType.name()), e);
			throw e;
		}
	}
} // LicenseUtil