/******************************************************************************
* (C) Copyright 2014
*
* Resolve Systems, LLC
*
* All rights reserved
* This software is distributed under license and may not be used, copied,
* modified, or distributed without the express written permission of
* Resolve Systems, LLC.
*
******************************************************************************/

package com.resolve.util;

import java.util.stream.Stream;

/**
 * ERR class defines all the error codes and messages. Each error must also
 * document what action needs to be done to address the problem.
 *
 * @author duke.tantiprasut
 *
 */

public enum ERR
{
    /*
     * E10000 - GENERIC ERRORS
     */
    /**
     * License has expired. Shutting Down - Contact support for a
     * valid license key and install the license
     */
    E10001("E10001", "License has expired. Shutting Down"),

    /**
     * Failed to start RSControl - Check rscontrol.log and see why RSControl is
     * unable to start up correctly
     */
    E10002("E10002", "Failed to start RSControl"),

    /**
     * Failed to start RSView - Check rsview.log and catalina.out and see why
     * RSView is unable to start up correctly
     */
    E10003("E10003", "Failed to start RSView"),

    /**
     * Failed to start RSMgmt - Check the rsmgmt.log and see why the RSMgmt is
     * unable to start up correctly
     */
    E10004("E10004", "Failed to start RSMgmt"),

    /**
     * Failed to start RSRemote - Check the rsremote.log and see why the
     * RSRemote is unable to start up correctly
     */
    E10005("E10005", "Failed to start RSRemote"),

    /**
     * Resolve licensed IP address violation. Shutting Down - Contact support
     * for a valid license key and install the license
     */
    E10006("E10006", "Licensed IP Address violated. Shutting Down"),

    /**
     * Resolve License violated for Number of Cores. Resolve will shutdown in 2 weeks if the violation left unresolved - Contact support
     * for a license with more cores and install the license.
     */
    E10007("E10007", "Resolve License violated for maximum number of core."),

    /**
     * Resolve Community Edition must run all components in one server. Shutting Down - Contact support
     * for a valid license and install the license
     */
    E10008("E10008", "Resolve License violated for Community Edition with Multiple Hosts. Shutting Down"),

    /**
     * Resolve Component is running with more memory than the license allowed. Resolve will shutdown in 2 weeks if the violation left unresolved - Contact support
     * for a license with more memories and install the license
     */
    E10009("E10009", "Maximum memory limit violated for Resolve license."),

    /**
     * Component process id does not match value in the lock file - Check that
     * the component was started correctly and that no duplicate process exists
     */
    E10010("E10010", "Component process id does not match value in the lock file"),

    /**
     * Component is running without a lock file - Check that the component was
     * started correctly and that no duplicate process exists
     */
    E10011("E10011", "Component is running without a lock file"),

    /**
     * Component is down but still has a lock file - Check that the component
     * was stopped correctly and that it did not crash
     */
    E10012("E10012", "Component is down but still has a lock file"),

    /**
     * Component is missing its stdout file, it cannot be monitored
     */
    E10013("E10013", "Component stdout log file cannot be found"),

    /**
     * Component stdout error detected
     */
    E10014("E10014", "Component stdout error detected"),

    /**
     * Resolve License violated for Number of End Users. Resolve will shutdown in 2 weeks if the violation left unresolved - Either delete Resolve User or Contact support
     * for a license with more end users and install the license.
     */
    E10015("E10015", "Number of named users limit violated for Resolve license."),

    /**
     * Resolve License violated for Number of Admin Users. Resolve will shutdown in 2 weeks if the violation left unresolved - Either delete Resolve User or Contact support
     * for a license with more end users and install the license.
     */
    E10016("E10016", "Number of admin users limit violated for Resolve license."),

    /**
     * Resolve License violated for Gateway.
     */
    E10017("E10017", "Resolve License violated for Gateway."),

    /**
     * RSRemote is unresponsive.
     */
    E10018("E10018", "RSRemote is unresponsive."),

    /**
     * Resolve License deleted.
     */
    E10019("E10019", "Resolve License deleted."),

    /**
     * Resolve License violated for High Availability (HA). Shutting Down - Contact support
     * for a valid license and install the license
     */
    E10020("E10020", "Resolve License violated for High Availability (HA). Shutting Down"),

    /**
     * Resolve Gateway License expired.
     */
    E10021("E10021", "Resolve Gateway License expired."),
    
    /**
     * Failed to add user as # of named users has reached max count of entitled users.
     */
    E10022("E10022", "Failed to add user as # of named users has reached max count of entitled users.", false),
    
    /**
     * Failed to add user as license has expired.
     */
    E10023("E10023", "Failed to add user as license has expired.", false),
    
    /**
     * Failed to save/add/delete/update ${object} as license has expired.
     */
    E10024("E10024", "Failed to save/add/delete/update object as license has expired.", false),
   
    /**
     * Failed to save/add/delete/update ${object} as license has expired.
     */
    E10025("E10025", "Failed to execute object as license has expired.", false),
    
    /*
     * Failed to get license from License Service.
     */
    E10026("E10026", "Failed to get license from License Service."),
    
    /**
     * Failed to start/keep running/re-start gateway as license has expired.
     */
    E10027("E10027", "Failed to start/keep running/re-start gateway as license has expired.", false),
    
    /*
     * Failed to check if current license from License Service is V2, Expired and for NON-PROD environment/instance type.
     */
    E10028("E10028", 
    	   "check if current license from License Service is V2, Expired and for NON-PROD environment/instance type."),
    
    /**
     * Resolve License not entitled for use in High Availability (HA) setup. 
     */
    E10029("E10029", "Resolve License not entilted for use in High Availability (HA) setup."),
    
    /**
     * Failed to save/add/delete/update ${object} as license is not 
     * entitled for use in High Availability (HA) setup.
     */
    E10030("E10030", "Failed to save/add/delete/update object as license is not " +
    			     "entilted for use in High Availability (HA) setup.", false),
    
    /**
     * Failed to save/add/delete/update ${object} as number of named users 
     * has exceeded entitled number of users.
     */
    E10031("E10031", "Failed to save/add/delete/update object as number of named users " +
    				 "has exceeded entitled number of users.", false),
    
    /**
     * Failed to add user as license is not entitled for use in High Availability (HA) setup.
     */
    E10032("E10032", "Failed to add user as license is not entitled for use in High Availability (HA) setup.", false),
    
    /*
     * E20000 - DATABASE ERRORS
     */
    /**
     * DB Gateway failed to load SQL driver - Check the RSRemote has the gateway
     * turned on and the appropriate DB driver is included in the classpath
     */
    E20001("E20001", "DB Gateway failed to load SQL driver"),

    /**
     * DB Gateway failed to initialize gateway - Check the database connection
     * details is correct and verify connectivity to the database from Resolve
     */
    E20002("E20002", "DB Gateway failed to initialize gateway"),

    /**
     * Missing SQL database driver - Check the DB driver is active in the
     * run.sh. For windows, ensure that the service was installed with the
     * driver active
     */
    E20003("E20003", "Missing SQL database driver"),

    /**
     * Failed to initialize GraphDB. Ensure that all RSZK(s) are running before
     * starting RSView - Check that RSZK is running and able to establish
     * connection from zk.sh / zk.bat. Contact support
     */
    E20004("E20004", "Failed to initialize GraphDB."),

    /**
     * DB Status Check Failed - Check that the connection details in RSMgmt are
     * correct and that the DB is behaving normally
     */
    E20005("E20005", "DB Status Check Failed"),

    /**
     * Failed to initialize custom tables - Check that the connection details in
     * RSMgmt are correct and that the DB is behaving normally
     */
    E20006("E20006", "Failed to initialize custom tables"),

    /**
     * Failed to reinitialize hibernate - Check that the database and database connection
     */
    E20007("E20007", "Failed to reinitialize hibernate"),

    /**
     * Failed to receive ESB Heartbeat message before timeout - Check that RSMQ service
     * is running, that the ESB configuration is correct, and that the network connection
     * between this server and the RSMQ is not down or blocked
     */
    E20008("E20008", "ESB Heartbeat Timeout"),
    
    /**
     * Transactions Per Second exeeded the threshold
     */
    E20009("E20009", "TPS exeeded the threshold"),
    
    /**
     * Failed to receive ESB ping response from the component.
     */
    E20010("E20010", "ESB Self Check Timeout"),
    
    /*
     * E30000 - SCHEDULER and EXECUTION ERRORS
     */
    /**
     * Failed to initialize CronScheduler - RSControl needs to be restarted and
     * verified that it started correctly
     */
    E30001("E30001", "Failed to initialize CronScheduler"),
    
    /**
     * Self Check: Cron scheduled Runbook execution failed.
     */
    E30002("E30002", "Self Check: Cron scheduled Runbook execution failed."),
    
    /**
     * Self Check: Runbook execution failed.
     */
    E30003("E30003", "Self Check: Runbook execution failed."),

    /*
     * E40000 - MESSAGING ERRORS
     */
    /**
     * Failed to find / initialize receiveQueue - Check component is able to
     * connect to RSMQ. Check RSMQ Configuration is using correct IP address.
     */
    E40001("E40001", "Failed to find / initialize receiveQueue"),

    /*
     * E50000 - GATEWAYS and ADAPTORS
     */
    /**
     * Failed to connect to PRIMARY and BACKUP Netcool ObjectServer - Check
     * Resolve connectivity to the Netcool PRIMARY and/or BACKUP Object Server
     */
    E50001("E50001", "Failed to connect to PRIMARY and BACKUP Netcool ObjectServer"),

    /**
     * Failed to initialize RemedyGateway - Check Resolve connectivity to the
     * Remedy server
     */
    E50002("E50002", "Failed to initialize RemedyGateway"),

    /**
     * Failed to connect to PRIMARY and BACKUP TSRM Server - Check
     * Resolve connectivity to the TSRM PRIMARY and/or BACKUP Server
     */
    E50003("E50003", "Failed to connect to PRIMARY and BACKUP TSRM Server"),

    /*
     * E60000 - SECURITY
     */
    /**
     * Failed to connect to ActiveDirectory server - Check Resolve connectivity
     * to the Active Directory server
     */
    E60001("E60001", "Failed to connect to ActiveDirectory server"),

    /**
     * Failed to connect to LDAP server - Check Resolve connectivity to the LDAP
     * server
     */
    E60002("E60002", "Failed to connect to LDAP server"),

    /**
     * Missing resolve.maint authentication file - Check
     * WEB-INF/users/resolve.maint file exists
     */
    E60003("E60003", "Missing resolve.maint authentication file"),
    
    /**
     * Failed to connect to LDAP/AD server - Check Resolve connectivity
     */
    E60004("E60004", "RsView self check - Failed to connect to LDAP/AD"),

    /*
     * E70000 - SYSTEM / RESOURCES
     */
    /**
     * Low available memory - Check the memory usage on the component using
     * RSConsole internal/jvminfo. The -Xmx max heap memory may be needed to be
     * increased
     */
    E70001("E70001", "Low available memory"),

    /**
     * Self Check: Thread pool maxed for configured time.
     */
    E70003("E70003", "Self Check: Thread pool maxed for configured time."),
    
    /**
     * ES Self Check: Available JVM Heap is more than the set threshold.
     */
    E70004("E70004", "ES Self Check: Used JVM Heap is more than the set threshold."),
    
    /**
     * ES Self Check: Available OS memory is less than the set threshold.
     */
    E70005("E70005", "ES Self Check: Used OS memory is greater than the set threshold."),
	
    /**
     * ES Self Check: Available disk space is less than the set threshold.
     */
    E70006("E70006", "ES Self Check: Available disk space is less than the set threshold."),
    
    /**
     * ES Self Check: Time between two GC cycles is less than the set threshold.
     */
    E70007("E70007", "ES Self Check: Time between two GC cycles is greater than the set threshold."),
    
    /**
     * Http response Self Check error: Did not receive http response.
     */
    E70008("E70008", "Http response Self Check error: Did not receive http response."),
    E70009("E70009", "No response is received for RSLog http self check request."),

    E70011("E70011", "Scheduled thread pool size too large"),

    E70012("E70012", "Task pool size too large"),
    
    E70013("E70013", "Groovy Script pool size over the limit"),

    /*
     * E80000 - METRIC THRESHOLDS
     */
    E80001("E80001", "Check metric thresholds"),

    /** test message - This is a test error code only. */
    E99999("E99999", "test message"),
    
    /** Import - Export related messages */
    IMPEX100("IMPEX-100", "The package, '%s', is already present on the system. Would you like to overwrite it? If no, then download the zip module of existing package. Change the package name and import the newly uloaded package, '%s'"),
    IMPEX101("IMPEX-101", "Could not get manifest graph for the module id %s"),
    IMPEX102("IMPEX-102", "Module zip which is getting imported, %s, seems to be exported from Resolve prior to 6.3"),
    IMPEX103("IMPEX-103","Another IMPEX operation is in progress on the same module, %s. Please try again later."),
	IMPEX104("IMPEX-104","Another IMPEX operation is in progress. Please try again later."),
    IMPEX105("IMPEX-105", "Error message(s) in the impex log."),
    IMPEX106("IMPEX-106", "The uploaded file is not a valid Resolve module zip."),
	IMPEX107("IMPEX-107", "License violation : %s. Import module operation is prohibited."),
	IMPEX108("IMPEX-108", "License violation : %s. Upload and Install operation is prohibited.");


    private String code;
    private String message;
    private boolean fatal;

    ERR(String code, String msg)
    {
        this.code = code;
        this.message = msg;
        this.fatal = true;
    }

    ERR(String code, String msg, boolean fatal)
    {
        this.code = code;
        this.message = msg;
        this.fatal = fatal;
    }
    
    public String getCode()
    {
        return this.code;
    } // getType

    public String getMessage()
    {
        return this.message;
    } // getMessage
    
    public boolean isFatal() {
    	return fatal;
    }
    
    public static final boolean isFatal(String code) {
    	boolean errIsFatal = true;
    	
    	if (StringUtils.isBlank(code)) {
    		return errIsFatal;
    	}
    	
    	ERR codeErr = Stream.of(ERR.values()).parallel()
    				  .filter(err -> (err.getCode().equals(code)))
                	  .findAny()
                	  .orElse(null);
    	
    	if (codeErr != null) {
    		errIsFatal = codeErr.isFatal();
    	}
    	
    	return errIsFatal;
    }
    
    public static ERR getERRFromMessage(String message) {
    	
    	if (StringUtils.isBlank(message)) {
    		return null;
    	}
    	
    	return Stream.of(ERR.values())
    		   .parallel().filter(err -> (err.getMessage().equals(message)))
			   .findAny()
			   .orElse(null);
    }

} // ERR
