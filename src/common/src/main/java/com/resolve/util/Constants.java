/******************************************************************************
* (C) Copyright 2014
*
* Resolve Systems, LLC
*
* All rights reserved
* This software is distributed under license and may not be used, copied,
* modified, or distributed without the express written permission of
* Resolve Systems, LLC.
*
******************************************************************************/

package com.resolve.util;

import java.util.concurrent.TimeUnit;

public interface Constants
{
    


    public final static String TRIGGER_EVENT_RETRY_COUNT_KEY = "TRIGGER_EVENT_RETRY_COUNT_KEY";
    public final static Integer TRIGGER_EVENT_RETRY_COUNT = 120;
    
    
    // CACHE
    public final static String CACHE_ACTIONTASK = "ACTIONTASK";
    public final static String CACHE_PARSER = "PARSER";
    public final static String CACHE_ASSESS = "ASSESS";
    public final static String CACHE_EVENTHANDLER = "EVENTHANDLER";
    public final static String CACHE_PREPROCESS = "PREPROCESS";
    public final static String CACHE_REGISTRATION = "REGISTRATION";
    public final static String CACHE_PREPROCESS_SCRIPT = "PREPROCESSSCRIPT";
    public final static String CACHE_ASSESS_SCRIPT = "ASSESSSCRIPT";
    public final static String CACHE_EVENTHANDLER_SCRIPT = "EVENTHANDLERSCRIPT";
    public final static String CACHE_TARGET = "TARGET";
    public final static String CACHE_PROCESS_STATE = "PROCESSEND";

    // ESB
    public final static String ESB_RABBITMQ = "RABBITMQ";
    public final static String ESB_NAME_BROADCAST = "BROADCAST";
    public final static String ESB_NAME_RSPROCESS = "RSPROCESS";
    public final static String ESB_NAME_RSSERVERS = "RSSERVERS";
    public final static String ESB_NAME_RSSERVER = "RSSERVER";
    public final static String ESB_NAME_RSSERVER_EVENT = "RSSERVER_EVENT";
    public final static String ESB_NAME_RSCONFIG = "RSCONFIG";
    public final static String ESB_NAME_RSCONTROL = "RSCONTROL";
    public final static String ESB_NAME_RSCONTROLS = "RSCONTROLS";
    public final static String ESB_NAME_SYSTEM_RSCONTROL = "SYSTEM_RSCONTROL";
    public final static String ESB_NAME_SYSTEM_RSCONTROLS = "SYSTEM_RSCONTROLS";
    public final static String ESB_NAME_RSACTIONS = "RSACTIONS";
    public final static String ESB_NAME_RSREMOTES = "RSREMOTES";
    public final static String ESB_NAME_RSREMOTE = "RSREMOTE";
    public final static String ESB_NAME_RSMGMTS = "RSMGMTS";
    public final static String ESB_NAME_RSMGMT = "RSMGMT";
    public final static String ESB_NAME_RSVIEWS = "RSVIEWS";
    public final static String ESB_NAME_RSVIEW = "RSVIEW";
    public final static String ESB_NAME_RSWIKIS = "RSWIKIS";
    public final static String ESB_NAME_RSWIKI = "RSWIKI";
    public final static String ESB_NAME_RSSYNCS = "RSSYNCS";
    public final static String ESB_NAME_RSSYNC = "RSSYNC";
    public final static String ESB_NAME_METRIC = "METRIC";
    public final static String ESB_NAME_ALERT = "ALERT";
    public final static String ESB_TYPE_GROUP = "GROUP";
    public final static String ESB_TYPE_INTERNAL = "INTERNAL";
    public final static String ESB_SERVICE_QUEUE = "SERVICE_QUEUE";
    public final static String ESB_NAME_RSUPDATES = "RSUPDATES";
    public final static String ESB_NAME_RSLICENSES = "RSLICENSES";
    public final static String ESB_NAME_RSLICENSE = "RSLICENSE";
    public final static String ESB_NAME_EXECUTEQUEUE = "EXECUTEQUEUE";
    public final static String ESB_NAME_DURABLE_EXECUTEQUEUE = "DEXECUTEQUEUE";
    public final static String ESB_NAME_EVENT_SOURCING_DCS = "EVENT_SOURCING_DCS";
    public final static String ESB_NAME_RSLOG =  "RSLOG";
    public final static String ESB_NAME_CLIENT_REGISTRATION = "CLIENT_REGISTRATION";
    public final static String ESB_NAME_DCS_REPORTINGS = "DCS_REPORTINGS";

    // ESB - GATEWAY
    public final static String ESB_NAME_GATEWAY_TOPIC = "GATEWAY_TOPIC";

    public final static String ESB_NAME_NETCOOL = "NETCOOL";
    public final static String ESB_NAME_NETCOOL_TOPIC = "NETCOOL_TOPIC";

    public final static String ESB_NAME_REMEDY = "REMEDY";
    public final static String ESB_NAME_REMEDY_TOPIC = "REMEDY_TOPIC";

    public final static String SYS_UPDATED_BY = "SYS_UPDATED_BY";

    // Action invocation type
    public final static String ACTION_INVOCATION_TYPE_NONE = "NONE";
    public final static String ACTION_INVOCATION_TYPE_OS = "OS";
    public final static String ACTION_INVOCATION_TYPE_BASH = "BASH";
    public final static String ACTION_INVOCATION_TYPE_CMD = "CMD";
    public final static String ACTION_INVOCATION_TYPE_JAVA = "JAVA";
    public final static String ACTION_INVOCATION_TYPE_GROOVY = "GROOVY";
    public final static String ACTION_INVOCATION_TYPE_ANT = "ANT";
    public final static String ACTION_INVOCATION_TYPE_INTERNAL = "INTERNAL";
    public final static String ACTION_INVOCATION_TYPE_REMOTE = "REMOTE";
    public final static String ACTION_INVOCATION_TYPE_CSCRIPT = "CSCRIPT";
    public final static String ACTION_INVOCATION_TYPE_POWERSHELL = "POWERSHELL";
    public final static String ACTION_INVOCATION_TYPE_WINEXEC = "WINEXEC";
    public final static String ACTION_INVOCATION_TYPE_WINTASK = "WINTASK";
    public final static String ACTION_INVOCATION_TYPE_PROCESS = "PROCESS";
    public final static String ACTION_INVOCATION_TYPE_ASSESS = "ASSESS";
    public final static String ACTION_INVOCATION_TYPE_EXPECT = "EXPECT";
    public final static String ACTION_INVOCATION_TYPE_PERL = "PERL";
    public final static String ACTION_INVOCATION_TYPE_EXTERNAL = "EXTERNAL";

    // action invocation options
    public final static String ACTION_INVOCATION_OPTIONS_CLASSPATH = "CLASSPATH";
    public final static String ACTION_INVOCATION_OPTIONS_EXECPATH = "EXECPATH";
    public final static String ACTION_INVOCATION_OPTIONS_JAVAOPTIONS = "JAVAOPTIONS";
    public final static String ACTION_INVOCATION_OPTIONS_BLANKVALUES = "BLANKVALUES";
    public final static String ACTION_INVOCATION_OPTIONS_TAILLENGTH = "TAILLENGTH";
    public final static String ACTION_INVOCATION_OPTIONS_RESULTTYPE = "RESULTTYPE";
    public final static String ACTION_INVOCATION_OPTIONS_RESULTFILE = "RESULTFILE";
    public final static String ACTION_INVOCATION_OPTIONS_RESULTFILE_REMOVE = "RESULTFILE_REMOVE";
    public final static String ACTION_INVOCATION_OPTIONS_RESULTFILE_WAIT = "RESULTFILE_WAIT";
    public final static String ACTION_INVOCATION_OPTIONS_TMPFILE_PREFIX = "TMPFILE_PREFIX";
    public final static String ACTION_INVOCATION_OPTIONS_TMPFILE_POSTFIX = "TMPFILE_POSTFIX";
    public final static String ACTION_INVOCATION_OPTIONS_TMPFILE_REMOVE = "TMPFILE_REMOVE";
    public final static String ACTION_INVOCATION_OPTIONS_INPUTFILE = "INPUTFILE";
    public final static String ACTION_INVOCATION_OPTIONS_INPUTFILE_POSTFIX = "INPUTFILE_POSTFIX";
	public final static String ACTION_INVOCATION_OPTIONS_INPUTFILE_ENCODING = "INPUTFILE_ENCODING";
    public final static String ACTION_INVOCATION_OPTIONS_CMDLINE_EXCLUDE = "CMDLINE_EXCLUDE";
    public final static String ACTION_INVOCATION_OPTIONS_QUEUE_NAME = "QUEUE_NAME";
    public final static String ACTION_INVOCATION_OPTIONS_EXTERNAL_TYPE = "EXTERNAL_TYPE";
    public final static String ACTION_INVOCATION_OPTIONS_METRIC_GROUP = "METRIC_GROUP";
    public final static String ACTION_INVOCATION_OPTIONS_METRIC_ID = "METRIC_ID";
    public final static String ACTION_INVOCATION_OPTIONS_METRIC_UNIT = "METRIC_UNIT";
    public final static String ACTION_INVOCATION_OPTIONS_TIMEOUT_DESTROY_THREAD = "TIMEOUT_DESTROY_THREAD";

    public final static String ACTION_PARAM_PREFIX = "RSI_PREFIX_";

    // action process
    public final static String ACTION_PROCESS_TYPE_MODEL = "MODEL";

    // Special Variables
    // public final static String VAR_REGEX_PARAM = "m/\\$PARAM{(\\w+:*\\w+)}/";
    public final static String VAR_REGEX_PARAM = "\\$PARAM\\{(.*?)\\}";
    public final static String VAR_REGEX_INPUT = "\\$INPUT\\{(.*?)\\}";
    public final static String VAR_REGEX_OUTPUT = "\\$OUTPUT\\{(.*?)\\}";
    public final static String VAR_REGEX_FLOW = "\\$FLOW\\{(.*?)\\}";
    public final static String VAR_REGEX_CONFIG = "\\$CONFIG\\{(.*?)\\}";
    public final static String VAR_REGEX_PROPERTY = "\\$PROPERTY\\{(.*?)\\}";
    public final static String VAR_REGEX_WSDATA = "\\$WSDATA\\{(.*?)\\}";
    public final static String VAR_REGEX_TARGET = "\\$TARGET\\{(.*?)\\}";
    public final static String VAR_REGEX_REFERENCE = "\\$REFERENCE\\{(\\w+)\\.(\\w+):(.*?)\\}";
    public final static String VAR_REGEX_EXPRESSION = "\\@(\\w+)";
    public final static String VAR_REGEX_GATEWAY_VARIABLE = "\\$\\{(.*?)\\}";
    public final static String VAR_REGEX_CONNECTOR_VARIABLE = "\\$\\{[_A-Z]*}";
    
    public final static String VAR_REGEX_URL_PARAM_VARIABLE = "\\{(.*?)\\}";
    
    //Special Variables find if output is nested
    public final static String VAR_REGEX_NESTED_PARAM = "(?<!.)\\$PARAM\\{(.*)\\}";
    public final static String VAR_REGEX_NESTED_INPUT = "(?<!.)\\$INPUT\\{(.*)\\}";
    public final static String VAR_REGEX_NESTED_OUTPUT = "(?<!.)\\$OUTPUT\\{(.*)\\}";
    public final static String VAR_REGEX_NESTED_FLOW = "(?<!.)\\$FLOW\\{(.*)\\}";
    public final static String VAR_REGEX_NESTED_WSDATA = "(?<!.)\\$WSDATA\\{(.*)\\}";
    // Execution Variables
    public final static String OPTIONS_TMPFILE = "${TMPFILE}";
    public final static String OPTIONS_INPUTFILE = "${INPUTFILE}";
    public final static String OPTIONS_EXPECTFILE = "${EXPECTFILE}";
    public final static String OPTIONS_INSTALLDIR = "${INSTALLDIR}";
    public final static String OPTIONS_USERID = "${USERID}";

    // Parser
    public final static String PARSER_METHOD_GENERIC = "GENERIC";
    public final static String PARSER_METHOD_TEMPLATE = "TEMPLATE";
    public final static String PARSER_TEMPLATE_HEADING = "HEADING";
    public final static String PARSER_TEMPLATE_VALUE = "VALUE";

    // Data
    public final static String DATA_TYPE_STRING = "STRING";
    public final static String DATA_TYPE_MAP = "MAP";
    public final static String DATA_TYPE_LIST = "LIST";
    public final static String DATA_TYPE_LISTMAP = "LISTMAP";
    public final static String DATA_TYPE_XML = "XML";

    // Registration
    public final static String REG_STATUS_ACTIVE = "ACTIVE";
    public final static String REG_STATUS_INACTIVE = "INACTIVE";
    public final static String REG_STATUS_EXPIRED = "EXPIRED";

    // Cron
    public final static String CRON_NAME = "NAME";
    public final static String CRON_TRIGGER_SIMPLE = "SIMPLE";
    public final static String CRON_TRIGGER_CRON = "CRON";
    public final static String CRON_MODULE = "MODULE";
    public final static String CRON_RUNBOOK = "RUNBOOK";
    public final static String CRON_PARAMS = "CRONPRARAMS";
    public final static String CRON_EXPRESSION = "EXPRESSION";
    public final static String CRON_START_TIME = "START_TIME";
    public final static String CRON_END_TIME = "END_TIME";
    public final static String CRON_USER_ID = "USER_ID";
    public final static String CRON_ACTIVE = "ACTIVE";
    public final static String CRON_FORCE = "FORCE";

    // assess
    public final static String ASSESS_DEFAULT = "DEFAULT";

    // event handler
    public final static String EVENT_HANDLER_MAIN = "MAIN";

    // Assess
    public final static String UI_TYPE_HTML = "<!-- UI_TYPE_HTML -->";
    public final static String DETAIL_TYPE_HTML = "<!-- DETAIL_TYPE_HTML -->";
    public final static String VALUE_START = "<!-- VALUE_START -->";
    public final static String VALUE_END = "<!-- VALUE_END -->";
    public final static String ASSESS_COMPLETION_TRUE = "TRUE";
    public final static String ASSESS_COMPLETION_FALSE = "FALSE";
    public final static String ASSESS_CONDITION_GOOD = "GOOD";
    public final static String ASSESS_CONDITION_BAD = "BAD";
    public final static String ASSESS_CONDITION_UNKNOWN = "UNKNOWN";
    public final static String ASSESS_SEVERITY_CRITICAL = "CRITICAL";
    public final static String ASSESS_SEVERITY_SEVERE = "SEVERE";
    public final static String ASSESS_SEVERITY_WARNING = "WARNING";
    public final static String ASSESS_SEVERITY_GOOD = "GOOD";
    public final static String ASSESS_SEVERITY_UNKNOWN = "UNKNOWN";
    public final static String ASSESS_SEVERITY_LE_CRITICAL = "LE_CRITICAL";
    public final static String ASSESS_SEVERITY_LE_SEVERE = "LE_SEVERE";
    public final static String ASSESS_SEVERITY_LE_WARNING = "LE_WARNING";
    public final static String ASSESS_SEVERITY_LT_CRITICAL = "LT_CRITICAL";
    public final static String ASSESS_SEVERITY_LT_SEVERE = "LT_SEVERE";
    public final static String ASSESS_SEVERITY_LT_WARNING = "LT_WARNING";
    public final static String ASSESS_SEVERITY_GE_SEVERE = "GE_SEVERE";
    public final static String ASSESS_SEVERITY_GE_WARNING = "GE_WARNING";
    public final static String ASSESS_SEVERITY_GE_GOOD = "GE_GOOD";
    public final static String ASSESS_SEVERITY_GT_SEVERE = "GT_SEVERE";
    public final static String ASSESS_SEVERITY_GT_WARNING = "GT_WARNING";
    public final static String ASSESS_SEVERITY_GT_GOOD = "GT_GOOD";
    public final static String ASSESS_MERGE_ALL = "ALL";
    public final static String ASSESS_MERGE_ANY = "ANY";
    public final static String ASSESS_MERGE_TARGETS = "TARGETS";
    public final static String ASSESS_STATUS_OPEN = "OPEN";
    public final static String ASSESS_STATUS_COMPLETED = "COMPLETED";
    public final static String ASSESS_STATUS_ABORTED = "ABORTED";
    public final static String ASSESS_STATUS_WAITING = "WAITING";
    public final static int ASSESS_RETURNCODE_EXECUTE_SUCCESS = 0;
    public final static int ASSESS_RETURNCODE_EXECUTE_FAIL = -1;
    public final static int ASSESS_RETURNCODE_EXECUTE_EXCEPTION = -2;
    public final static int ASSESS_RETURNCODE_EXECUTE_TIMEOUT = -3;

    // property
    public final static String PROPERTY_TYPE_PLAIN = "PLAIN";
    public final static String PROPERTY_TYPE_ENCRYPT = "ENCRYPT";
    public final static String PROPERTY_ENCRYPT_PREFIX = "ENC:";

    // targets
    public final static String TARGET_GUID = "TARGET_GUID";
    public final static String SOURCE_GUID = "SOURCE_GUID";
    public final static String CUSTOM_TABLE_XML = "CUSTOM_TABLE_XML";
    public final static String BUSINESS_RULE_UPDATE = "BUSINESS_RULE_UPDATE";

    // model
    public final static String MODEL_NODE_TYPE_TASK = "TASK";
    public final static String MODEL_NODE_TYPE_START = "START";
    public final static String MODEL_NODE_TYPE_END = "END";
    public final static String MODEL_NODE_TYPE_EVENT = "EVENT";
    public final static String MODEL_NODE_TYPE_PRECONDITION = "PRECONDITION";
    public final static String MODEL_NODE_TYPE_SUBPROCESS = "SUBPROCESS";
    public final static String MODEL_TYPE = "MODEL_TYPE";
    public final static String MODEL_TYPE_MAIN = "MAIN";
    public final static String MODEL_TYPE_ABORT = "ABORT";
    public final static String MODEL_MODE = "MODEL_MODE";
    public final static String MODEL_MODE_MAIN = "MAIN";
    public final static String MODEL_MODE_ABORT = "ABORT";

    // groovy bindings
    public final static String GROOVY_BINDING_LOG = "LOG";
    public final static String GROOVY_BINDING_SYSLOG = "SYSLOG";
    public final static String GROOVY_BINDING_DEBUG = "DEBUG";
    public final static String GROOVY_BINDING_DB = "DB";
    public final static String GROOVY_BINDING_GRAPHDB = "GRAPHDB";
    public final static String GROOVY_BINDING_ASSESS = "ASSESS";
    public final static String GROOVY_BINDING_PREPROCESS = "PREPROCESS";
    public final static String GROOVY_BINDING_PARSER = "PARSER";
    public final static String GROOVY_BINDING_OPTIONS = "OPTIONS";
    public final static String GROOVY_BINDING_ARGS = "ARGS";
    public final static String GROOVY_BINDING_ESB = "ESB";
    public final static String GROOVY_BINDING_PROCESSID = "PROCESSID";
    public final static String GROOVY_BINDING_EXECUTEID = "EXECUTEID";
    public final static String GROOVY_BINDING_PROBLEMID = "PROBLEMID";
    public final static String GROOVY_BINDING_PROCESSINFO = "PROCESSINFO";
    public final static String GROOVY_BINDING_GLOBALS = "GLOBALS";
    public final static String GROOVY_BINDING_PARAMS = "PARAMS";
    public final static String GROOVY_BINDING_FLOWS = "FLOWS";
    public final static String GROOVY_BINDING_INPUTS = "INPUTS";
    public final static String GROOVY_BINDING_OUTPUTS = "OUTPUTS";
    public final static String GROOVY_BINDING_WIKI = "WIKI";
    public final static String GROOVY_BINDING_USERID = "USERID";
    public final static String GROOVY_BINDING_TARGET = "TARGET";
    public final static String GROOVY_BINDING_TARGETS = "TARGETS";
    public final static String GROOVY_BINDING_ACTIONNAME = "ACTIONNAME";
    public final static String GROOVY_BINDING_RAW = "RAW";
    public final static String GROOVY_BINDING_DATA = "DATA";
    public final static String GROOVY_BINDING_DATATYPE = "DATATYPE";
    public final static String GROOVY_BINDING_ADDRESS = "ADDRESS";
    public final static String GROOVY_BINDING_REFERENCE = "REFERENCE";
    public final static String GROOVY_BINDING_RESULT = "RESULT";
    public final static String GROOVY_BINDING_RECORD = "RECORD";
    public final static String GROOVY_BINDING_WORKSHEET = "WORKSHEET";
    public final static String GROOVY_BINDING_MAIN = "MAIN";
    public final static String GROOVY_BINDING_HEADER = "HEADER";
    public final static String GROOVY_BINDING_PROCESS = "PROCESS";
    public final static String GROOVY_BINDING_SESSIONS = "SESSIONS";
    public final static String GROOVY_BINDING_RC = "RC";
    public final static String GROOVY_GROOVYBUFFER_OUT = "OUT";
    public final static String GROOVY_BINDING_NAME_PROPERTIES = "NAME_PROPERTIES";
    public final static String GROOVY_BINDING_PROPERTIES = "PROPERTIES";
    public final static String GROOVY_BINDING_REGEX_MATCHER = "REGEX_MATCHER";
    public final static String GROOVY_BINDING_REQUEST = "REQUEST";
    public final static String GROOVY_BINDING_RESPONSE = "RESPONSE";
    public final static String GROOVY_BINDING_OLD_PARAMS = "OLD_PARAMS";
    public final static String GROOVY_BINDING_WSDATA = "WSDATA";
    public final static String GROOVY_BINDING_AMQP = "AMQP";

    // INPUTS
    public final static String INPUTS_RSEXCEPTION = "RSEXCEPTION";

    // Businnes rull bindings
    public final static String GROOVY_BINDING_TABLE_ROW = "ROW";
    public final static String GROOVY_BINDING_TABLE_OLD_ROW = "OLDROW";
    public final static String GROOVY_BINDING_TABLE_EXPORT_DATA = "EXPORT_DATA";

    // Resolve RunTime Action task Context Helper (RRATContextHelper) binding
    
    public final static String GROOVY_BINDING_RRATCONTEXTHELPER = "RRATCONTEXTHELPER";
    
    public final static String GROOVY_HTTP_REQUEST = "REQUEST";
    public final static String GROOVY_HTTP_RESPONSE = "RESPONSE";

    // rsconsole script bindings
    public final static String RSCONSOLE_BINDING_VARS = "VARS";
    public final static String RSCONSOLE_BINDING_SYS = "SYS";
    public final static String RSCONSOLE_BINDING_ENV = "ENV";
    public final static String RSCONSOLE_BINDING_HELP = "HELP";
    public final static String RSCONSOLE_BINDING_DB = "DB";
    public final static String RSCONSOLE_BINDING_BLUEPRINT = "BLUEPRINT";

    // rsconsole blueprint properties
    public final static String BLUEPRINT_DIST = "DIST";
    public final static String BLUEPRINT_PRIMARY = "PRIMARY";
    public final static String BLUEPRINT_INSTALL = "INSTALL";
    public final static String BLUEPRINT_LOCALHOST = "LOCALHOST";
    public final static String BLUEPRINT_RSVIEW_NODELIST = "RSVIEW_NODES";
    public final static String BLUEPRINT_RSCONTROL_NODELIST = "RSCONTROL_NODES";
    public final static String BLUEPRINT_CASSANDRA_NODELIST = "CASSANDRA_NODES";
    public final static String BLUEPRINT_RSSEARCH_NODELIST = "RSSEARCH_NODES";
    public final static String BLUEPRINT_CASSANDRA_SEED_PORT = "cassandra.yaml.rpc_port";
    public final static String BLUEPRINT_DB_TYPE = "DB_TYPE";
    public final static String BLUEPRINT_DB_NAME = "DB_NAME";
    public final static String BLUEPRINT_DB_USERNAME = "DB_USERNAME";
    public final static String BLUEPRINT_DB_SCHEMA = "DB_SCHEMA";
    public final static String BLUEPRINT_RSMQ_PRIMARY_HOST = "RSMQ_PRIMARY_HOST";
    public final static String BLUEPRINT_RSMQ_BACKUP_HOST = "RSMQ_BACKUP_HOST";
    public final static String BLUEPRINT_RSMQ_PRODUCT = "rsmq.product";
    public final static String BLUEPRINT_JGROUPS_RSVIEW_PORT = "rsview.jgroups.stack.config.TCP.bind_port";
    public final static String BLUEPRINT_JGROUPS_RSCONTROL_PORT = "rscontrol.jgroups.stack.config.TCP.bind_port";

    // hazelcast collections
    public final static String PROCESS_LOCK_RSCONTROL_MASTER = "PROCESS_LOCK_RSCONTROL_MASTER";
    public final static String PROCESS_LOCK_PROCESS_ABORT = "PROCESS_LOCK_PROCESS_ABORT";
    public final static String PROCESS_LOCK_STRINGMAP = "PROCESS_LOCK_STRINGMAP";
    public final static String PROCESS_LOCK_PROCESS_POOL_STATUS = "PROCESS_LOCK_PROCESS_POOL_STATUS";
    public final static String PROCESS_SET_AVAILABLE_MAPID = "PROCESS_SET_AVAILABLE_MAPID";
    public final static String PROCESS_MAP_RSCONTROL_MASTER = "PROCESS_MAP_RSCONTROL_MASTER";
    public final static String PROCESS_MAP_PROCESS_MAPID = "PROCESS_MAP_PROCESS_MAPID";
    public final static String PROCESS_MAP_PROCESS_TIMEOUT = "PROCESS_MAP_PROCESS_TIMEOUT";
    public final static String PROCESS_MAP_PROCESS_POOL = "PROCESS_MAP_PROCESS_POOL";
    public final static String PROCESS_MAP_PROCESS_POOL_STATUS = "PROCESS_MAP_PROCESS_POOL_STATUS";
    public final static String PROCESS_MAP_PROCESS_INFO = "PROCESS_MAP_PROCESS_INFO";
    public final static String PROCESS_MAP_PROCESS_STATE = "PROCESS_MAP_PROCESS_STATE";
    public final static String PROCESS_MAP_EXECUTE_TARGETS = "PROCESS_MAP_EXECUTE_TARGETS";
    public final static String PROCESS_MAP_EVALUATE_STATUS = "PROCESS_MAP_EVALUATE_STATUS";
    public final static String PROCESS_MAP_EXECUTEPARAM = "PROCESS_MAP_EXECUTEPARAM";
    public final static String PROCESS_MAP_EXECUTEPARAM_PROCEXEC_IDS = "PROCESS_MAP_EXECUTEPARAM_PROCEXEC_IDS";
    public final static String PROCESS_MAP_MERGE_TARGETS = "PROCESS_MAP_MERGE_TARGETS";
    public final static String PROCESS_MAP_RSSREADER_FEED_LOCK = "PROCESS_MAP_RSSREADER_FEED_LOCK";

    // LockUtils locks
    public final static String LOCK_ACTIONPROCESSDEPENDENCY = "ACTIONPROCESSDEPENDENCY";
    public final static String LOCK_ACTIONFINALDEPENDENCY = "ACTIONFINALDEPENDENCY";
    public final static String LOCK_ACTIONABORTDEPENDENCY = "ACTIONABORTDEPENDENCY";
    public final static String LOCK_LOGWORKSHEET = "LOGWORKSHEET";
    public final static String LOCK_TASKCOMPLETED = "TASKCOMPLETED";
    public final static String LOCK_PROCSTATUSUPDATE = "PROCSTATUSUPDATE";
    public final static String LOCK_RESULTMACRO_PROBLEMID = "RESULTMACRO_PROBLEMID";
    public final static String LOCK_POOLED_GATEWAY_CHECKIN = "POOLED_GATEWAY_CHECKIN";
    public final static String LOCK_POOLED_GATEWAY_CHECKOUT = "POOLED_GATEWAY_CHECKOUT";
    public final static String LOCK_REMOVEPROCESS = "REMOVEPROCESS";
    public final static String LOCK_WORKSHEETINDEX = "WORKSHEETINDEX";
    public final static String LOCK_PROCESSREQUESTINDEX = "PROCESSREQUESTINDEX";
    public final static String LOCK_WORKSHEETDATAINDEX = "WORKSHEETDATAINDEX";
    public final static String LOCK_WORKSHEETDATAPROPERTYINDEX = "WORKSHEETDATAPROPERTYINDEX";
    public final static String LOCK_EXECUTESTATEINDEX = "EXECUTESTATEINDEX";

    // internal flow keywords
    public final static String SESSION_AFFINITY = "_SESSION_AFFINITY_";
    public final static String SESSION_ID = "_SESSION_ID_";
    public final static String MERGE_TARGETS = "_MERGE_TARGETS_";

    // actiontask headers
    public final static String ESB_PARAM_PROPERTIES = "PROPERTIES";
    public final static String ESB_PARAM_CLASSMETHOD = "CLASSMETHOD";
    public final static String ESB_PARAM_EVENTTYPE = "EVENT_TYPE";
    public final static String ESB_PARAM_CRONID = "CRONID";
    public final static String ESB_PARAM_CRONNAME = "CRONNAME";
    public final static String ESB_PARAM_REPORTNAME = "REPORTNAME";
    public final static String ESB_PARAM_FORCE = "FORCE";
    public final static String ESB_PARAM_ACTIONTASKID = "ACTIONTASKID";

    public final static String ESB_PARAM_CONDITION = "CONDITION";
    public final static String ESB_PARAM_SEVERITY = "SEVERITY";

    // impex parameters
    public final static String IMPEX_PARAM_MODULENAME = "MODULENAME";
    public final static String IMPEX_PARAM_MODULENAMES = "MODULENAMES";
    public final static String IMPEX_PARAM_DOCUMENT = "DOCUMENT";
    public final static String IMPEX_PARAM_NAMESPACE = "NAMESPACE";
    public final static String IMPEX_PARAM_TAG = "TAG";
    public final static String IMPEX_PARAM_ACTIONTASK = "ACTIONTASK";
    public final static String IMPEX_PARAM_ACTIONTASK_BY_TAG = "ACTIONTASK_BY_TAG";
    public final static String IMPEX_PARAM_PROPERTIES = "PROPERTIES";
    public final static String IMPEX_PARAM_WIKILOOKUP = "WIKILOOKUP";
    public final static String IMPEX_PARAM_CRONJOB = "CRONJOB";
    public final static String IMPEX_PARAM_MENUSET = "MENUSET";
    public final static String IMPEX_PARAM_MENUSECTION = "MENUSECTION";
    public final static String IMPEX_PARAM_MENUITEM = "MENUITEM";
    public final static String IMPEX_PARAM_CUSTOMTABLE = "CUSTOMTABLE";
    public final static String IMPEX_PARAM_METAFORMVIEW = "METAFORMVIEW";
    public final static String IMPEX_PARAM_DB = "DB";
    public final static String IMPEX_PARAM_DESCRIPTION = "DESCRIPTION";
    public final static String IMPEX_PARAM_SCAN = "SCAN";
    public final static String IMPEX_PARAM_BLOCK_INDEX = "BLOCK_INDEX";

    // Decision Tree parameters
    public final static String DT_AUTO_ANSWER_VARIABLES_JSON = "DT_VARIABLES";
    
    // execute parameters
    public final static String EXECUTE_ACTIONNAME = "ACTIONNAME";
    public final static String EXECUTE_ACTIONNAMESPACE = "ACTIONNAMESPACE";
    public final static String EXECUTE_ACTIONSUMMARY = "ACTIONSUMMARY";
    public final static String EXECUTE_ACTIONHIDDEN = "ACTIONHIDDEN";
    public final static String EXECUTE_ACTIONTAGS = "ACTIONTAGS";
    public final static String EXECUTE_ACTIONROLES = "ACTIONROLES";
    public final static String EXECUTE_ACTIONID = "ACTIONID";
    public final static String EXECUTE_AT_SYNCHRONOUSLY_TIMEOUT = "ATSYNC_TIMEOUT";
    public final static String EXECUTE_NAMESPACE = "NAMESPACE";
    public final static String EXECUTE_PROCESSID = "PROCESSID";
    public final static String EXECUTE_WIKI = "WIKI";
    public final static String EXECUTE_REMOVESTATE = "REMOVESTATE";
    public final static String EXECUTE_ABORTWIKI = "ABORT:";
    public final static String EXECUTE_NEWWIKI = "NEWWIKI";
    public final static String EXECUTE_PARENTWIKI = "PARENTWIKI";
    public final static String EXECUTE_EXECUTEID = "EXECUTEID";
    public final static String EXECUTE_PARSERID = "PARSERID";
    public final static String EXECUTE_ASSESSID = "ASSESSID";
    public final static String EXECUTE_DURATION = "DURATION";
    public final static String EXECUTE_LOGRESULT = "LOGRESULT";
    public final static String EXECUTE_USERID = "USERID";
    public final static String EXECUTE_JOBID ="RESOLVE.JOB_ID";
    public final static String RESOLVE_INSTERNAL_EXECUTE_USERID = "__RESOLVE_INSTERNAL_EXECUTE_USERID__";
    public final static String EXECUTE_PROBLEMID = "PROBLEMID";
    public final static String EXECUTE_PROBLEMID_ENFORCED_BY_SIR = "EXECUTE_PROBLEMID_ENFORCED_BY_SIR";
    public final static String EXECUTE_PROBLEM_NUMBER = "PROBLEM_NUMBER";
    public final static String EXECUTE_REFERENCE = "REFERENCE";
    public final static String EXECUTE_RETURNCODE = "RETURNCODE";
    public final static String EXECUTE_ALERTID = "ALERTID";
    public final static String EXECUTE_ALERTID_UI_MIXED_CASE = "alertId";
    public final static String EXECUTE_CORRELATIONID = "CORRELATIONID";
    public final static String EXECUTE_CORRELATIONID_UI_MIXED_CASE = "correlationId";
    public final static String EXECUTE_DELAY = "DELAY";
    public final static String EXECUTE_ADDRESS = "ADDRESS";
    public final static String EXECUTE_TARGET = "TARGET";
    public final static String EXECUTE_AFFINITY = "AFFINITY";
    public final static String EXECUTE_PARAMS = "PARAMS";
    public final static String EXECUTE_FLOWS = "FLOWS";
    public final static String EXECUTE_INPUTS = "INPUTS";
    public final static String EXECUTE_TIMEOUT = "TIMEOUT";
    public final static String EXECUTE_PROCESS_TIMEOUT = "PROCESS_TIMEOUT";
    public final static String EXECUTE_PROCESS_STARTTIME = "PROCESS_STARTTIME";
    public final static String EXECUTE_METRICID = "METRICID";
    public final static String EXECUTE_MOCK = "MOCK";
    public final static String EXECUTE_RESULTADDR = "RESULTADDR";
    public final static String EXECUTE_BLANKVALUES = "BLANKVALUES";
    public final static String EXECUTE_JOBNAME = "JOBNAME";
    public final static String EXECUTE_CMDLINE = "CMDLINE";
    public final static String EXECUTE_EXECPATH = "EXECPATH";
    public final static String EXECUTE_FILENAME = "FILENAME";
    public final static String EXECUTE_INPUT = "INPUT";
    public final static String EXECUTE_ARGS = "ARGS";
    public final static String EXECUTE_TYPE = "TYPE";
    public final static String EXECUTE_TASKNAME = "TASKNAME";
    public final static String EXECUTE_CLASSPATH = "CLASSPATH";
    public final static String EXECUTE_JREPATH = "JREPATH";
    public final static String EXECUTE_JAVAOPTIONS = "JAVAOPTIONS";
    public final static String EXECUTE_GUID = "GUID";
    public final static String EXECUTE_ACTIONRESULT_GUID = "ACTIONRESULT_GUID";
    public final static String EXECUTE_DEBUG = "DEBUG";
    public final static String EXECUTE_WIKI_PROCESS = "WIKI_PROCESS";
    public final static String EXECUTE_WIKI_PARENT = "WIKI_PARENT";
    public final static String EXECUTE_WIKI_WIKIID = "WIKI_WIKIID";
    public final static String EXECUTE_WIKI_PARENTID = "WIKI_PARENTID";
    public final static String EXECUTE_PROCESS_LOOP = "PROCESS_LOOP";
    public final static String EXECUTE_PROCESS_DEBUG = "PROCESS_DEBUG";
    public final static String EXECUTE_PROCESS_MOCK = "PROCESS_MOCK";
    public final static String EXECUTE_PROCESS_CONCURRENT_LIMIT = "PROCESS_CONCURRENT_LIMIT";
    public final static String EXECUTE_PROCESS_CHECK_PRECONDITION_NULL_VARS = "PROCESS_CHECK_PRECONDITION_NULL_VARS";
    public final static String EXECUTE_PROCESS_NUMBER = "RESOLVE.EXECUTE_PROCESS_NUMBER";
    public final static String PROCESS_CONCURRENT_LIMIT_MAP = "PROCESS_CONCURRENT_LIMIT_MAP";
    public final static String EXECUTE_PROCESS_NOTEVENT = "PROCESS_NOTEVENT";
    public final static String EXECUTE_END = "END";
    public final static String EXECUTE_END_QUIT = "END_QUIT";
    public final static String EXECUTE_END_WAIT = "END_WAIT";
    public final static String EXECUTE_END_TYPE = "END_TYPE";
    public final static String EXECUTE_END_CONDITION = "END_CONDITION";
    public final static String EXECUTE_END_SEVERITY = "END_SEVERITY";
    public final static String EXECUTE_BESTCONDITION = "BESTCONDITION";
    public final static String EXECUTE_WORSTCONDITION = "WORSTCONDITION";
    public final static String EXECUTE_BESTSEVERITY = "BESTSEVERITY";
    public final static String EXECUTE_WORSTSEVERITY = "WORSTSEVERITY";
    public final static String EXECUTE_PARAMSSTR = "PARAMSSTR";
    public final static String EXECUTE_FLOWSSTR = "FLOWSSTR";
    public final static String EXECUTE_INPUTSSTR = "INPUTSSTR";
    public final static String EXECUTE_DEBUGSTR = "DEBUGSTR";
    public final static String EXECUTE_COMPLETION = "COMPLETION";
    public final static String EXECUTE_COMMAND = "COMMAND";
    public final static String EXECUTE_RAW = "RAW";
    public final static String EXECUTE_REDIRECT = "REDIRECT";
    public final static String EXECUTE_REQUEST_TIMESTAMP = "REQUEST_TIMESTAMP";
    public final static String EXECUTE_ACTION = "ACTION";
    public final static String EXECUTE_VO = "VO";
    public final static String EXECUTE_VOSTR = "VOSTR";
    public final static String EXECUTE_CONDITION = "CONDITION";
    public final static String EXECUTE_SEVERITY = "SEVERITY";
    public final static String EXECUTE_SUMMARY = "SUMMARY";
    public final static String EXECUTE_DETAIL = "DETAIL";
    public final static String EXECUTE_CRUD_ACTION = "CRUD_ACTION";
    public final static String EXECUTE_RUNBOOK_PRIORITY = "RUNBOOK_PRIORITY";
    public final static String EXECUTE_EVENTID = "EVENT_EVENTID";
    public final static String EXECUTE_TABLE = "TABLE";
    public final static String EXECUTE_CACHEABLE = "CACHEABLE";
    public final static String EXECUTE_STATUS_OPEN = "OPEN";
    public final static String EXECUTE_STATUS_COMPLETED = "COMPLETED";
    public final static String EXECUTE_STATUS_ABORTED = "ABORTED";
    public final static String EXECUTE_STATUS_WAITING = "WAITING";
    public final static String EXECUTE_STATUS_UNKNOWN = "UNKNOWN";
    
    public final static String EXECUTE_RR_SCHEMA_ID = "RR_SCHEMA";
    public final static String EXECUTE_RR_RID = "RR_RID";
    
    
    public final static String EXECUTE_SIR_ID = "SIR_ID";
    public final static String EXECUTE_SIR_ENABLED = "SIR_ENABLED";
    public final static String EXECUTE_SIR_TITLE = "SIR_TITLE";
    public final static String EXECUTE_SIR_TYPE = "SIR_TYPE";
    public final static String EXECUTE_SIR_SECURITY_TYPE = "security";
    public final static String EXECUTE_SIR_INVESTIGATION_TYPE = "SIR_INVESTIGATION_TYPE";
    public final static String EXECUTE_SIR_PLAYBOOK = "SIR_PLAYBOOK";
    public final static String EXECUTE_SIR_SEVERITY = "SIR_SEVERITY";
    public final static String EXECUTE_SIR_OWNER = "SIR_OWNER";
    public final static String EXECUTE_SIR_SOURCE_SYSTEM = "SIR_SOURCE_SYSTEM";
    public final static String EXECUTE_SIR_PROBLEMID_ENFORSED_BY_UI_RR = "problemId";
    public final static String EXECUTE_SIR_INITIAL_REQUEST_DATA = "SIR_INITIAL_REQUEST_DATA";
    
    public final static String EXECUTE_ORG_NAME = "RESOLVE.ORG_NAME";
    public final static String EXECUTE_org_name = "resolve.org_name";
    public final static String EXECUTE_ORG_ID = "RESOLVE.ORG_ID";
    public final static String EXECUTE_org_id = "resolve.org_id";
    
    public static final String EXECUTE_INCIDENT_ID = "EXECUTE_INCIDENT_ID";
    public static final String EXECUTE_ROOT_RUNBOOK_ID = "EXECUTE_ROOT_RUNBOOK_ID";
    public static final String EXECUTE_ACTION_TASK_ID = "EXECUTE_ACTION_TASK_ID";
    
    public final static String ACTIVITY_ID = "activityId";
    
    public final static int RUNBOOK_PRIORITY_NORMAL_VALUE = 10;
    public final static int RUNBOOK_PRIORITY_EVENT_INCREMENT_VALUE = 1;
    public final static String RBC_SOURCE = "RBC_SOURCE";
    public final static String RBC = "RBC";

    public final static String PUBLIC_ROLE = "public";
    public final static String REQUIRE_PUBLIC_ROLE = "includePublic";

    // sysscript parameters
    public final static String SYSSCRIPT_NAME = "NAME";
    public final static String SYSSCRIPT_SCRIPT = "SCRIPT";

    public final static String ACTIONTASK_LOCK_TIMEOUT = "ACTIONTASK_LOCK_TIMEOUT";
    public final static String ACTIONTASK_LOCK_CONDITION = "ACTIONTASK_LOCK_CONDITION";
    public final static String ACTIONTASK_LOCK_SEVERITY = "ACTIONTASK_LOCK_SEVERITY";
    public final static String ACTIONTASK_LOCK_SUMMARY = "ACTIONTASK_LOCK_SUMMARY";
    public final static String ACTIONTASK_LOCK_DETAIL = "ACTIONTASK_LOCK_DETAIL";
    public final static String ACTIONTASK_LOCK_NAME = "ACTIONTASK_LOCK_NAME";
    public final static String ACTIONTASK_MASTER_LOCK = "ACTIONTASK_MASTER_LOCK";
    public final static String MASTER_LOCK_SYSID = "402880e84b2821ef014b2865eab2006c";

    // spring authentication
    public final static String AUTH_TOKEN = "RSTOKEN";
    public final static String AUTH_USERNAME = "USERNAME";
    public final static String AUTH_P_ASSWORD = "PASSWORD";
    public final static String AUTH_DOMAIN = "DOMAIN";
    public final static String AUTH_LDAP_MODE = "MODE";
    public final static String AUTH_LDAP_HOST = "HOST";
    public final static String AUTH_LDAP_PORT = "PORT";
    public final static String AUTH_LDAP_BINDDN = "BINDDN";
    public final static String AUTH_LDAP_BINDP_ASSWORD = "BINDPASSWORD";
    public final static String AUTH_LDAP_BASEDNLIST = "BASEDNLIST";
    public final static String AUTH_LDAP_SSL = "SSL";
    public final static String AUTH_LDAP_CRYPTTYPE = "CRYPTTYPE";
    public final static String AUTH_LDAP_CRYPTPREFIX = "CRYPTPREFIX";
    public final static String AUTH_LDAP_PROPERTIES = "PROPERTIES";
    public final static String AUTH_LDAP_VERSION = "VERSION";
    public final static String AUTH_LDAP_DEFAULT_DOMAIN = "DEFAULT_DOMAIN";
    public final static String AUTH_LDAP_UID_ATTRIBUTE = "UID_ATTRIBUTE";
    public final static String AUTH_LDAP_P_ASSWORD_ATTRIBUTE = "PASSWORD_ATTRIBUTE";
    public final static String AUTH_LDAP_GROUPREQUIRED = "GROUP_REQUIRED";
    
    public final static String AUTH_AD_DOMAIN = "DOMAIN";
    public final static String AUTH_AD_MODE = "MODE";
    public final static String AUTH_AD_HOST = "HOST";
    public final static String AUTH_AD_PORT = "PORT";
    public final static String AUTH_AD_BINDDN = "BINDDN";
    public final static String AUTH_AD_BINDP_ASSWORD = "BINDPASSWORD";
    public final static String AUTH_AD_BASEDNLIST = "BASEDNLIST";
    public final static String AUTH_AD_SSL = "SSL";
    public final static String AUTH_AD_PROPERTIES = "PROPERTIES";
    public final static String AUTH_AD_VERSION = "VERSION";
    public final static String AUTH_AD_DEFAULT_DOMAIN = "DEFAULT_DOMAIN";
    public final static String AUTH_AD_UID_ATTRIBUTE = "UID_ATTRIBUTE";
    public final static String AUTH_AD_GROUPREQUIRED = "GROUP_REQUIRED";

    public final static String AUTH_USER_CREDENTIALS = "CREDENTIALS";
    
    // http session
    public final static String HTTP_REQUEST_USERNAME = "USERNAME";
    public final static String HTTP_REQUEST_TOKEN = "TOKEN";
    public final static String HTTP_REQUEST_QUERY = "QUERY";
    public final static String HTTP_REQUEST_QUERYSTRING = "QUERYSTRING";
    public final static String HTTP_REQUEST_WIKI = "WIKI";
    public final static String HTTP_REQUEST_ACTION = "ACTION";
    public final static String HTTP_REQUEST_ACTIONNAME = "ACTIONAME";
    public final static String HTTP_REQUEST_ACTIONID = "ACTIONID";
    public final static String HTTP_REQUEST_PROBLEMID = "PROBLEMID";
    public final static String HTTP_REQUEST_PROBLEM_NUMBER = "PROBLEM_NUMBER";
    public final static String HTTP_REQUEST_PROCESSID = "PROCESSID";
    public final static String HTTP_REQUEST_EXECUTEID = "EXECUTEID";
    public final static String HTTP_REQUEST_TASKRESULTID = "TASKRESULTID";
    public final static String HTTP_REQUEST_EXECUTERESULTID = "EXECUTERESULTID";
    public final static String HTTP_REQUEST_ADDRESS = "ADDRESS";
    public final static String HTTP_REQUEST_NUMBER = "NUMBER";
    public final static String HTTP_REQUEST_REFERENCE = "REFERENCE";
    public final static String HTTP_REQUEST_ALERTID = "ALERTID";
    public final static String HTTP_REQUEST_CORRELATIONID = "CORRELATIONID";
    public final static String HTTP_REQUEST_REDIRECT = "REDIRECT";
    public final static String HTTP_REQUEST_REDIRECT_WIKI = "REDIRECT_WIKI";
    public final static String HTTP_REQUEST_DEBUG = "DEBUG";
    public final static String HTTP_REQUEST_INTERNAL_TOKEN = "_token_";
    public final static String HTTP_REQUEST_INTERNAL_USERNAME = "_username_";
    public final static String HTTP_REQUEST_INTERNALP_ASSWORD = "_password_";
    public final static String HTTP_REQUEST_INTERNAL_LOGOUT = "_logout_";
    public final static String HTTP_REQUEST_sys_id = "sys_id";
    public final static String HTTP_REQUEST_SYS_ID = "SYS_ID";
    public final static String HTTP_REQUEST_SSO_TYPE = "SSOTYPE";
    public final static String HTTP_REQUEST_SESSION_ID = "SESSIONID";
    public final static String HTTP_ATTRIBUTE_CREATETIME = "CREATETIME";
    public final static String AJAXCALL = "AJAXCALL";
    public final static String HTTP_ATTRIBUTE_SSO_REDIRECT_URL = "SSOREDIRECTURL";
    public final static String HTTP_ATTRIBUTE_RSVIEW_ID_GUID = "RSVIEWIDGUID";
    public final static String HTTP_REQUEST_IFRAMEID = "IFRAMEID";
    public final static String HTTP_REQUEST_DT_VARIABLES = "dtVariables";
    public final static String HTTP_REQUEST_SSOSERVICEID = "SSOSERVICEID";
    public final static String HTTP_REQUEST_SSOSERVICEERRORMSG = "SSOSERVICERRORMSG";

    // change password
    public final static String OLD_PASSWORD="OLD_PASSWORD";
    public final static String NEW_PASSWORD="NEW_PASSWORD";

    // rsexecuteclient
    public final static String DETAIL_OUTPUT_FLAG = "DETAIL_OUTPUT_FLAG";
    public final static String ACTIONTASK_DETAIL_OUTPUT = "ACTIONTASK_DETAIL_OUTPUT";

    // persistence sequence prefix name
    public final static String PERSISTENCE_SEQ_PRB = "WS";
    public final static String PERSISTENCE_SEQ_PROC = "PROC";
    public final static String PERSISTENCE_SEQ_EXEC = "EXEC";
    public final static String PERSISTENCE_SEQ_INIT = "INIT";
    public final static String PERSISTENCE_SEQ_RC = "RC";
    public final static String PERSISTENCE_SEQ_RV = "RV";
    public final static String PERSISTENCE_SEQ_PID = "PID";

    // problemid values
    public final static String PROBLEMID_NEW = "NEW";
    public final static String PROBLEMID_ACTIVE = "ACTIVE";
    public final static String PROBLEMID_LOOKUP = "LOOKUP";

    // process start parameters
    public final static String PROCESS_TIMEOUT = "PROCESS_TIMEOUT";
    public final static String PROCESS_LOOP = "PROCESS_LOOP";
    public final static String PROCESS_DEBUG = "PROCESS_DEBUG";
    public final static String PROCESS_CONCURRENT_LIMIT = "PROCESS_CONCURRENT_LIMIT";
    public final static String PROCESS_DISPLAY = "PROCESS_DISPLAY";
    
    public final static String PROCESS_AT_EXEC_COUNT = "PROCESS_AT_EXEC_COUNT";

    // special usernames
    public final static String USER_ADMIN = "admin";
    public final static String USER_PUBLIC = "public";
    public final static String USER_RESOLVE_MAINT = "resolve.maint";
    public final static String DEFAULT_RIGHT_FOR_ACTIONTASK = "admin,resolve_dev";
    public final static String DEFAULT_EXECUTE_RIGHT_FOR_ACTIONTASK = "admin,resolve_dev,action_execute";
    
    // reserved namespace
    public final static String RESOLVE_NAMESPACE = "resolve";

    // Binding variable names for wiki
    public final static String WIKI = "WIKI";
    public final static String WIKIDOCUMENT = "DOC";
    public final static String CURR_WIKIDOCUMENT = "CURRDOC";
    public final static String WIKI_CONTEXT = "CONTEXT";
    public final static String WIKI_CONTEXT_VELOCITY = "context";

    // parameters for inline model
    public final static String STATUS = "STATUS";
    public final static String TASK_STATUS = "TASK_STATUS";
    public final static String ZOOM = "ZOOM";
    public final static String REFRESH = "REFRESH";
    public final static String REFRESHINTERVAL = "REFRESHINTERVAL";
    public final static String REFRESHCOUNTMAX = "REFRESHCOUNTMAX";
    public final static String GRAPHNODEID = "GRAPHNODEID";
    public final static String NODEID = "NODEID";
    public final static String NODELABEL = "NODELABEL";
    public final static String DOREFRESH = "DOREFRESH";
    public final static String RESULTS = "RESULTS";
    public final static String STATUS_CONDITION = "condition";
    public final static String STATUS_SEVERITY = "severity";
    public final static String COLOR = "COLOR";
    public final static String EXECUTE_REQUEST_NODEID_DELIMITER = "+++";

    // column types for custom table
    public final static String CUSTOMTABLE_TYPE_STRING = "string";
    public final static String CUSTOMTABLE_TYPE_INTEGER = "integer";
    public final static String CUSTOMTABLE_TYPE_LONG = "long";
    public final static String CUSTOMTABLE_TYPE_FLOAT = "float";
    public final static String CUSTOMTABLE_TYPE_DOUBLE = "double";
    public final static String CUSTOMTABLE_TYPE_BOOLEAN = "boolean";
    public final static String CUSTOMTABLE_TYPE_BYTE = "byte";
    public final static String CUSTOMTABLE_TYPE_TIMESTAMP = "timestamp";
    public final static String CUSTOMTABLE_TYPE_CLOB = "clob";
    public final static String CUSTOMTABLE_TYPE_BLOB = "blob";
    public final static String CUSTOMTABLE_TYPE_BIGDECIMAL = "big_decimal";

    // properties for manipulating the wiki control panel
    public final static String SHOW_WIKI_CONTROL_PANEL = "SHOW_WIKI_CONTROL_PANEL";
    // public final static String WIKI_DEFAULT_SHOW_CONTROLPANEL =
    // "wiki.controlpanel.show.default";
    public final static String WIKI_CONTROLPANEL_SHOW_DEFAULT = "wiki.controlpanel.show.default";
    public final static String WIKI_EXCLUDE_ROLES_TO_SHOW_CONTROLPANEL = "wiki.controlpanel.show.exclude.roles";
    public final static String ADMIN = "admin";
    public final static String RESOLVE_MAINT = "resolve.maint";

    // alert types
    public final static String ALERT_TYPE_DATABASE = "DATABASE";
    public final static String ALERT_TYPE_JVM = "JVM";
    public final static String ALERT_TYPE_LICENSE = "LICENSE";
    public final static String ALERT_TYPE_STATUS = "STATUS";

    // event
    public final static String EVENT_REGEX = "REGEX";
    public final static String EVENT_EVENTID = "EVENT_EVENTID";
    public final static String EVENT_END = "EVENT_END";
    public final static String EVENT_REFERENCE = "EVENT_REFERENCE";
    public final static String EVENT_EXECUTEID = "EVENT_EXECUTEID";
    public final static String EVENT_HAS_REFERENCE = "EVENT_HAS_REFERENCE";
    public final static String EVENT_TIMEOUT = "EVENT_TIMEOUT";
    public final static String EVENT_IDLE = "EVENT_IDLE";
    public final static String EVENT_CONDITION = "CONDITION";
    public final static String EVENT_SEVERITY = "SEVERITY";
    public final static String EVENT_COMPLETED = "COMPLETED";
    public final static String EVENT_SUMMARY = "SUMMARY";
    public final static String EVENT_DETAIL = "DETAIL";
    public final static String EVENT_DURATION = "DURATION";
    public final static String EVENT_RETURNCODE = "EVENT_RETURNCODE";
    public final static String EVENT_EVENTABORT = "EVENT_EVENTABORT";

    // gateway events
    public final static String GATEWAY_EVENT_TYPE_NETCOOL = "NETCOOL";
    public final static String GATEWAY_EVENT_TYPE_DB = "DB";
    public final static String GATEWAY_EVENT_TYPE_REMEDY = "REMEDY";
    public final static String GATEWAY_EVENT_TYPE_REMEDYX = "REMEDYX";
    public final static String GATEWAY_EVENT_TYPE_EMAIL = "EMAIL";
    public final static String GATEWAY_EVENT_TYPE_EXCHANGE = "EXCHANGE";
    public final static String GATEWAY_EVENT_TYPE_TSRM = "TSRM";
    public final static String GATEWAY_EVENT_TYPE_SNMP = "SNMP";
    public final static String GATEWAY_EVENT_TYPE_CRON = "CRON";
    public final static String GATEWAY_EVENT_TYPE_RSCONTROL_ABORT = "RSCONTROL_ABORT";
    public final static String GATEWAY_EVENT_TYPE_RSVIEW = "RSVIEW";
    public final static String GATEWAY_EVENT_TYPE_WEBSERVICE = "WEBSERVICE";
    public final static String GATEWAY_EVENT_TYPE_XMPP = "XMPP";
    public final static String GATEWAY_EVENT_TYPE_TELNET = "TELNET";
    public final static String GATEWAY_EVENT_TYPE_SERVICENOW = "SERVICENOW";
    public final static String GATEWAY_EVENT_TYPE_SSH = "SSH";
    public final static String GATEWAY_EVENT_TYPE_HPOM = "HPOM";
    public final static String GATEWAY_EVENT_TYPE_SALESFORCE = "SALESFORCE";
    public final static String GATEWAY_EVENT_TYPE_EWS = "EWS";
    public final static String GATEWAY_EVENT_TYPE_HPSM = "HPSM";
    public final static String GATEWAY_EVENT_TYPE_AMQP = "AMQP";
    public final static String GATEWAY_EVENT_TYPE_HTTP = "HTTP";
    public final static String GATEWAY_EVENT_TYPE_TCP = "TCP";
    public final static String GATEWAY_EVENT_TYPE_TIBCOBESPOKE = "TIBCOBESPOKE";
    public final static String GATEWAY_EVENT_TYPE_CASPECTRUM = "CASPECTRUM";

    public final static String GATEWAY_FILTER = "filter";
    
    public final static String DB_TYPE_ORACLE = "oracle";
    public final static String DB_TYPE_ORACLE_12C = "oracle12c";
    public final static String DB_TYPE_MYSQL = "mysql";
    public final static String DB_TYPE_DB2 = "db2";

    public final static String PROPERTY_SESSION_TIMEOUT = "session.timeout";

    public final static String RSS_SUBSCRIPTION = "rss_subscription";
    public final static String RSS_NAME = "NAME";
    public final static String RSS_SYS_ID = "SYS_ID";

    // system properties
    public final static String APP_RIGHTS_RSCLIENT = "app.rights.rsclient";
    public final static String APP_RIGHTS_RSWIKI = "app.rights.rswiki";
    public final static String APP_RIGHTS_RSWORKSHEET = "app.rights.rsworksheet";
    public final static String APP_RIGHTS_RSACTIONTASK = "app.rights.rsactiontask";
    public final static String APP_RIGHTS_RSADMIN = "app.rights.rsadmin";
    public final static String APP_RIGHTS_RSNETCOOL = "app.rights.rsnetcool";
    public final static String APP_RIGHTS_RSADMIN_MENUSET = "app.rights.rsadmin.menuset";
    public final static String APP_RIGHTS_RSADMIN_MENUDEFINITION = "app.rights.rsadmin.menudefinition";
    public final static String APP_RIGHTS_RSADMIN_PROPERTIES = "app.rights.rsadmin.properties";
    public final static String APP_RIGHTS_RSADMIN_CRON = "app.rights.rsadmin.cron";
    public final static String APP_RIGHTS_RSADMIN_RULE = "app.rights.rsadmin.rule";
    public final static String APP_RIGHTS_RSADMIN_USER_ROLES = "app.rights.rsadmin.user_roles";
    public final static String APP_RIGHTS_RSADMIN_USER_GROUP = "app.rights.rsadmin.user_group";
    public final static String APP_RIGHTS_RSADMIN_USERS = "app.rights.rsadmin.users";
    public final static String APP_RIGHTS_RSADMIN_WIKIADMIN = "app.rights.rsadmin.wikiadmin";
    public final static String APP_RIGHTS_RSADMIN_WIKILOOKUP = "app.rights.rsadmin.wikilookup";
    public final static String APP_RIGHTS_RSADMIN_IMPEX = "app.rights.rsadmin.impex";
    public final static String APP_RIGHTS_RSADMIN_REGISTRATION = "app.rights.rsadmin.registration";
    public final static String APP_RIGHTS_RSADMIN_TARGET = "app.rights.rsadmin.target";
    public final static String APP_RIGHTS_RSADMIN_SYSTEMSCRIPT = "app.rights.rsadmin.systemscript";
    public final static String WIKI_RIGHTS_VIEW_ALL = "wiki.rights.view.ALL";
    public final static String WIKI_RIGHTS_EDIT_ALL = "wiki.rights.edit.ALL";
    public final static String WIKI_RIGHTS_ADMIN_ALL = "wiki.rights.admin.ALL";
    public final static String WIKI_RIGHTS_EXECUTE_ALL = "wiki.rights.execute.ALL";
    public final static String WIKI_RIGHTS_VIEW_EXAMPLE = "wiki.rights.view.Example";
    public final static String CUSTOMTABLE_RIGHTS_VIEW_ALL = "customtable.rights.view.ALL";
    public final static String CUSTOMTABLE_RIGHTS_EDIT_ALL = "customtable.rights.edit.ALL";
    public final static String CUSTOMTABLE_RIGHTS_ADMIN_ALL = "customtable.rights.admin.ALL";
    public final static String CUSTOMTABLE_RIGHTS_EXECUTE_ALL = "customtable.rights.execute.ALL";
    public final static String SESSION_URL_COUNT = "session.url.count";
    public final static String SESSION_TIMEOUT = "session.timeout";
    public final static String SESSION_MAXALLOWED = "session.maxallowed";
    public final static String PRODUCT_DESCRIPTION = "product.description";
    public final static String PRODUCT_LOGO = "product.logo";
    public final static String MEMORY_SORTING_COUNT = "memory.sorting.count";
    public final static String EXPORT_LISTENER_TABLES = "export.listener.tables";
    public final static String NETCOOL_GATEWAYS = "netcool.gateways";
    public final static String USERS_PASSWORD_HISTORYCHECK = "users.password.historycheck";
    public final static String METRIC_GROUP_NAMES = "metric.group.names";
    public final static String METRIC_UNIT_NAMES_JVM = "metric.unit.names.JVM";
    public final static String METRIC_UNIT_NAMES_RUNBOOK = "metric.unit.names.Runbook";
    public final static String SEARCH_DEFAULT_TYPE = "search.default.type";
    public final static String SEARCH_EXCLUDE_NAMESPACE = "search.exclude.namespace";
    public final static String MENU_MENUSET_DEFAULT = "menu.menuset.default";
    public final static String MENUITEMGROUP_COLLAPSE_DEFAULT = "menuitemgroup.collapse.default";
    public final static String WIKI_PROPERTY_TABLE_HOMEPAGE = "wiki.property.table.homepage";
    public final static String WIKI_USER_HISTORY_COUNT = "wiki.user.history.count";
    public final static String WIKI_CONTROLPANEL_SHOW_EXCLUDE_ROLES = "wiki.controlpanel.show.exclude.roles";
    public final static String WIKI_ATTACHMENT_IMAGE_EXTENSION = "wiki.attachment.image.extension";
    public final static String METAFORM_HIDDENFIELDS = "metaform.hiddenfields";
    public final static String METAFORMVIEW_RIGHTS_VIEW_ALL = "metaformview.rights.view.ALL";
    public final static String METAFORMVIEW_RIGHTS_EDIT_ALL = "metaformview.rights.edit.ALL";
    public final static String METAFORMVIEW_RIGHTS_ADMIN_ALL = "metaformview.rights.admin.ALL";
    public final static String METAFORMVIEW_RIGHTS_EXECUTE_ALL = "metaformview.rights.execute.ALL";
    public final static String LOGIN_FAIL_ATTEMPTS_ALLOWED = "login.fail.attempts.allowed";
    public final static String ACTIONTASK_RIGHTS_VIEW_ALL = "actiontask.rights.view.ALL";
    public final static String ACTIONTASK_RIGHTS_EDIT_ALL = "actiontask.rights.edit.ALL";
    public final static String ACTIONTASK_RIGHTS_ADMIN_ALL = "actiontask.rights.admin.ALL";
    public final static String ACTIONTASK_RIGHTS_EXECUTE_ALL = "actiontask.rights.execute.ALL";
    public final static String ACTIONTASK_LOG_RAW_RESULT = "actiontask.log.raw.result";
    public final static String INDEX_ON_STARTUP = "index.on.startup";
    public final static String SOCIAL_EXPIRATION_RSS_INTERVAL = "social.expiration.rss.interval";
    public final static String SYSTEM_HOMEPAGE = "system.homepage";
    public final static String GLOBAL_EXECUTE_TIMEOUT = "global.execute.timeout";
    public final static String IMPEX_EXPORT_PROPERTIES = "impex.export.properties";

    // archive
    public final static String ARCHIVE_SLEEPTIME = "ARCHIVE_SLEEPTIME";

    // purge
    public final static String PURGE_DATE = "PURGE_DATE";
    public final static String PURGE_LOB_ONLY = "PURGE_LOB_ONLY";

    // service queues
//    public final static String SERVICE_QUEUE_WIKI_INDEX = ESB_SERVICE_QUEUE + ".WIKI_INDEX";

    // notification
    public final static String NOTIFICATION_CARRIER_TYPE = "CARRIER_TYPE";
    public final static String NOTIFICATION_CARRIER_TYPE_EMAIL = "EMAIL";
    public final static String NOTIFICATION_CARRIER_TYPE_EXCHANGE = "EXCHANGE";
    public final static String NOTIFICATION_CARRIER_TYPE_SMS = "SMS";
    public final static String NOTIFICATION_CARRIER_TYPE_EWS = "EWS";
    public final static String NOTIFICATION_CARRIER_TYPE_XMPP = "XMPP";
    public final static String NOTIFICATION_CARRIER_USERNAME = "USERNAME";
    public final static String NOTIFICATION_CARRIER_PASSWORD = "PASSWORD";

    public final static String NOTIFICATION_MESSAGE_FROM = "FROM";
    public final static String NOTIFICATION_MESSAGE_REPLY_TO = "REPLY_TO";
    public final static String NOTIFICATION_MESSAGE_TO = "TO";
    public final static String NOTIFICATION_MESSAGE_CC = "CC";
    public final static String NOTIFICATION_MESSAGE_BCC = "BCC";
    public final static String NOTIFICATION_MESSAGE_SUBJECT = "SUBJECT";
    public final static String NOTIFICATION_MESSAGE_CONTENT = "CONTENT";
    public final static String NOTIFICATION_MESSAGE_ATTACHMENTS = "ATTACHMENTS";
    public final static String NOTIFICATION_MESSAGE_FROM_DISPLAY_NAME = "FROM_DISPLAY_NAME";

    public final static String DEFAULT_SQL_DATE_FORMAT = "yyyy-MM-dd'T'HH:mm:ss.SSS";

    // content request
    public String CR_TYPE = "RequestType";
    public String CR_TYPE_NEW_CONTENT = "New Content";
    public String CR_TYPE_UPDATE = "Update";
    public String CR_TYPE_REVIEW = "Review";
    public String CR_TYPE_RECERTIFY = "Recertify";
    public String CR_TYPE_ARCHIVE = "Archive";
    public String CR_COMPONENT_TYPE = "compo";
    public String CR_COMPONENT_TYPE_DOCUMENT = "Document";
    public String CR_COMPONENT_TYPE_DECISIONTREE = "DecisionTree";
    public String CR_COMPONENT_TYPE_RUNBOOK = "Runbook";
    public String CR_COMPONENT_TYPE_ACTIONTASK = "ActionTask";

    public String HEARTBEAT_FORMAT = "MMM-dd-yyyy HH:mm:ss.SSS XXX";

    public String RSMGMT_MONITOR_COMPONENTS = "COMPONENTS";

    //worksheet utils
    public final static String WORKSHEET_REFERENCE = "REFERENCE";
    public final static String WORKSHEET_ALERTID = "ALERTID";
    public final static String WORKSHEET_PROBLEMID = "PROBLEMID";
    public final static String WORKSHEET_NUMBER = "NUMBER";
    public final static String WORKSHEET_SERIAL = "SERIAL";
    public final static String WORKSHEET_IDENTIFIER = "IDENTIFIER";
    public final static String WORKSHEET_ASSIGNED_TO = "ASSIGNED_TO";
    public final static String WORKSHEET_WIKI = "WIKI";

    // REST web service parameters
    public final static String REST_PARAM_CLASSMETHOD = "CLASSMETHOD";

    // result attributes
    public final static String RESULTATTR_ENCRYPTED = "ENCRYPTED";

    //Default gateway UQUEUE names
    public final static String DEFAULT_GATEWAY_QUEUE_NAME = "<|Default|Queue|>";

    // index
    public final static String INDEX_STARTTIME = "STARTTIME";
    public final static String INDEX_ENDTIME = "ENDTIME";

    // db types supported
    public final static String MYSQL = "MySQL";
    public final static String ORACLE_SID = "Oracle-SID";
    public final static String ORACLE_SERVICE = "Oracle-Service";
    public final static String DB2 = "DB2";
    public final static String MSSQL = "MSSQL";
    public final static String POSTGRESQL = "PostgreSQL";

    // driver class constants
    public final static String OFFICIAL_MYSQLDRIVER = "org.mysql.jdbc.Driver";
    public final static String MARIADBDRIVER = "org.mariadb.jdbc.Driver";
    public final static String MYSQLDRIVER = MARIADBDRIVER; //We are Using the MariaDB driver for MySQL, do not change
    public final static String ORACLEDRIVER = "oracle.jdbc.OracleDriver";
    public final static String DB2DRIVER = "com.ibm.db2.jcc.DB2Driver";
    public final static String MSSQLDRIVER = "com.microsoft.sqlserver.jdbc.SQLServerDriver";
    public final static String POSTGRESQLDRIVER = "org.postgresql.Driver";

    // social constants
    public final static String SOCIAL_TEAM_ADMIN_GROUP = "Admin Group";

    public static final String LICENSE_CONTENT = "LICENSE_CONTENT";
    public static final String LOG_ENTRY_CONTENT = "LOG_ENTRY_CONTENT";
    // LDAP / AD
    final static String MEMBEROF = "groups";
    final static String MEMBEROF_DEFAULT = "memberof";
    
    // WSDATA
    final static String WSDATA_DT_DTLIST = "dt_dtList";
    final static String WSDATA_DT_QA = "dt_qa";
    final static String WSDATA_FLAG = "WSDATA_FLAG";

    // CLUSTER
    final static String CLUSTER_MODE = "clusterMode";
    final static String CLUSTER_MODE_ACTIVE = "active";
    final static String CLUSTER_MODE_INACTIVE = "inactive";
    final static String RSSEARCH_SYNC = "rssearchSync";
    
    //Message property uniquely identifying source
    final static String MSG_SOURCE_ID = "Source_ID";
    
    //Elastic Search
    final static String  ELASTIC_SEARCH_REPOSITORY_NAME= "resolve_backup";

    final static String  METRIC_INTERVAL_MINUTE= "METRIC_INTERVAL_MINUTE";
    final static String  METRIC_INTERVAL_HOUR= "METRIC_INTERVAL_HOUR";
    final static String  METRIC_INTERVAL_SECOND= "METRIC_INTERVAL_SECOND";
    final static String  METRIC_INTERVAL_DAY= "METRIC_INTERVAL_DAY";
    final static String  METRIC_INTERVAL_WEEK= "METRIC_INTERVAL_WEEK";
    final static String  METRIC_INTERVAL_MONTH= "METRIC_INTERVAL_MONTH";
    final static String  METRIC_INTERVAL_QUARTER= "METRIC_INTERVAL_QUARTER";
    final static String  METRIC_INTERVAL_YEAR= "METRIC_INTERVAL_YEAR";
    
    final static String METRICTHRESHOLD_LESS = "<";
    final static String METRICTHRESHOLD_LESSEQUAL = "<=";
    final static String METRICTHRESHOLD_GREATER = ">";
    final static String METRICTHRESHOLD_GREATEREQUAL = ">=";
    final static String METRICTHRESHOLD_EQUAL = "=";
    final static String MCP_NAMEPORT_SEPARATOR = "==";
    final static String MCP_MODE_LOCAL = "LOCAL_MODE_MCP";
    final static String MCP_MODE_MANAGED = "MANAGED_MODE_MCP";
    final static String MCP_MODE_CENTRAL = "CENTRAL_MODE_MCP";

    final static String MCP_MANUAL_DEPLOY = "manualDeployMetricThresholds";
    final static String MCP_UNDEPLOY = "undeployMetricThresholds";
    final static String MCP_UNDEPLOY_ALL = "allUndeploylMetricThresholds";
    final static String MCP_AUTO_DEPLOY = "autoDeployMetricThresholds";
    final static String MCP_GROUP_NAME = "__MCP_GROUP__";
    final static String MCP_DEFAULT_GROUP = "Default Group";
    final static String DB_LOCK_ERR_MSG = "distributed transaction waiting for lock";
    
    // RADIUS authentication
    
    final static String AUTH_RADIUS_PRIMARYHOST = "HOST";
    final static String AUTH_RADIUS_SECONDARYHOST = "HOST2";
    final static String AUTH_RADIUS_AUTHPORT = "AUTHPORT";
    final static String AUTH_RADIUS_ACCTPORT = "ACCTPORT";
    final static String AUTH_RADIUS_AUTHPROTOCOL = "AUTHMETHOD";
    final static String AUTH_RADIUS_SHAREDSECRET = "SHAREDSECRET";
    final static String AUTH_RADIUS_AUTHSTATUS = "AUTHSTATUS";
    
    /* HTTP Security X-Frame-Options ALLOW-FROM and 
     * CSP frame-ancestors allowed urls for iframe integration
     * system property.
     */
    
    final static String SECURITY_HTTP_IFRAME_INTEGRATION_ALLOWEDURLS = "security.http.iframe.integration.allowedurls";
    
    final static String SECURITY_HTTP_LOCALDOMAIN_CERT_BYPASS = "security.http.localdomain.cert.bypass";
    
    // RBA-14583 : Create a black list of commands
    final static String SECURITY_COMMANDS_BLACKLIST = "security.commands.blacklist";
    // Delimiter is picked to be " ** ", because that sequence is safe when we delimit multiple RegEx definitions
    final static String SECURITY_COMMANDS_BLACKLIST_DELIMITER_REGEX = " \\*\\* ";
    // Comma delimited list of parameter names considered to be execution commands
    final static String SECURITY_COMMANDS_AS_PARAMETERS_TO_CHECK = "command";
    
    //Http gateway return type
    final static String  HTTP_GATEWAY_RETURN_JSON= "HTTP_GATEWAY_RETURN_JSON";
    
    /* Disable HTTP Security X-Frame-Options ALLOW-FROM and 
     * CSP frame-ancestors headers system property.
     */
    
    final static String DISABLE_HTTP_IFRAME_INTEGRATION_SECUIRTY = "disable.http.iframe.integration.security";
    
    public final static String GUID = "guid";
    public final static String GW_CONTAINER_NAME = "ctname";
    public final static String GW_CONTAINER_ORG = "ctorg";

    public static String MACRO_TYPE = "macrotype";
    public static String MACRO_TYPE_DETAILS = "details";
    public static String MACRO_TYPE_RESULTS = "results";
    public static String MACRO_TYPE_RESULTS2 = "results2";
    
    public static String MACRO_RESULTS_INCLUDED_FIELDS = "";//"sysId,severity,taskName,taskFullName,taskNamespace,taskSummary,summary,wiki,completion,condition,problemId,taskTags,sysUpdatedOn,processId,sysCreatedOn,executeRequestId,processNumber";
    public static String MACRO_RESULTS2_INCLUDED_FIELDS = "";//"sysId,severity,taskName,taskFullName,taskNamespace,taskSummary,summary,wiki,completion,condition,problemId,taskTags,sysUpdatedOn,processId,sysCreatedOn,executeRequestId,processNumber";
    public static String MACRO_DETAIL_INCLUDED_FIELDS = "";//"sysId,severity,taskName,taskFullName,taskNamespace,taskSummary,summary,wiki,completion,condition,problemId,taskTags,sysUpdatedOn,processId,sysCreatedOn,executeRequestId,processNumber,detail";
    public static String MACRO_RESULTS_EXCLUDED_FIELDS = "detail";
    public static String MACRO_RESULTS2_EXCLUDED_FIELDS = "detail";
    public static String MACRO_DETAIL_EXCLUDED_FIELDS = "";
    
    // RBA-14583 : Create a black list of commands
    final static String EXCEPTION_PREFIX_BLACKLISTED_COMMANDS = "ActionTask has been blocked from execution due to containing blacklisted command";
    
    public final static long GLOBAL_SEND_TIMEOUT = TimeUnit.MINUTES.toMillis(10); // 10 minutes in milliseconds
    
    // SIR system "app.security.*" properties constants
    
    public static final String SECURITY_GROUPS = "app.security.groups";
    public static final String SECURITY_DEFAULT_OWNER = "app.security.defaultowner";
    public static final String SECURITY_SIR_PREFIX = "app.security.sir.prefix";
    
    public static final String SIR = "sir";
    public static final String PB_ARTIFACT_TYPE = "type";
    public static final String PB_ARTIFACT_NAME = "name";
    public static final String PB_ARTIFACT_VALUE = "value";
    public static final String PB_DESCRIPTION = "description";
    
    public static final String MAX_FILE_UPLOAD_SIZE_IN_MB_SYSTEM_PROPERTY = "fileupload.max.size";
    public static final int MIN_FILE_UPLOAD_SIZE_IN_MB = 100;
    
    public static final String SECURITY_DB_ORDER_BY_FUNCTIONS_BLACKLIST_SYSTEM_PROPERTY = "security.db.orderby.functions.blacklist";
    
    public static final String NIL_STRING = "nil";
    
    //SIR RBAC functions
    
    String ACTIVITIES = "sirInvestigationViewerActivities";
    String NOTES = "sirInvestigationViewerNotes";
    String ARTIFACTS = "sirInvestigationViewerArtifacts";
    String ATTACHMENTS = "sirInvestigationViewerAttachments";
    
    String VIEW_ACCESS = "view";
    String CREATE_ACCESS = "create";
    String MODIFY_ACCESS = "modify";
    String DELETE_ACCESS = "delete";
    String EXECUTE_ACCESS = "execute";
    
    public static final String SIR_PHASES_STANDARDS_SYSTEM_PROPERTY = "sir.phases.standards";
    
    // saving user's settings endpoints 
    public static final String ANALYTIC_SETTINGS = "analyticSettings";
    public static final String REFRESH_RATE_SETTINGS = "refreshRate";
    public static final String COLOR_THEME_SETTINGS = "colorTheme";
    
    public static final String DT_METRIC = "dt_metric";
    

    // Application Right
    public static final String USER_SAVE_USER = "/user/saveUser";

    // Login logo and disclaimer path
    public static final String DEFAULT_LOGO = "/resolve/images/logo-login.png";
    public static final String CLOUD_LOGO = "/resolve/images/logo-cloud.png";
    public static final String CUSTOM_LOGO = "/resolve/login/logo.png";
    public static final String CUSTOM_LOGO_REALPATH = "/login/logo.png";
    public static final String DISCLAIMER_REALPATH = "/login/disclaimer.html";
    public static final String DEFAULT_DISCLAIMER_REALPATH = "disclaimer.html";
    
    // Gateway Filter Operations
    public static final String FILTER_OPERATION_CLEARANDSET = "clearAndSet";
	public static final String FILTER_OPERATION_DEPLOY = "deploy";
	public static final String FILTER_OPERATION_UNDEPLOY = "undeploy";

    public static final Object RESOLVE_USERNAME = "RESOLVE_USERNAME";
} // Constants