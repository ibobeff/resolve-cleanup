/******************************************************************************
* (C) Copyright 2014
*
* Resolve Systems, LLC
*
* All rights reserved
* This software is distributed under license and may not be used, copied,
* modified, or distributed without the express written permission of
* Resolve Systems, LLC.
*
******************************************************************************/

package com.resolve.util;

import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.Enumeration;
import java.util.zip.ZipEntry;
import java.util.zip.ZipFile;
import java.util.zip.ZipInputStream;
import java.util.zip.ZipOutputStream;

import com.resolve.util.FileUtils;

public class Compress
{
    public static final String ZIP_ZIPPING = "zipping";
    public static final String ZIP_UNZIPPING = "unzipping";
    static final int BUFFER = 2048;
    private boolean fIncludeSubdirectories = true;

    /**
     * @param zipFile
     * @param outputDir
     * @return
     */
    public int unzip(File zipFile, File outputDir)
    {
        int cnt = 0;

        try
        {
            FileInputStream fis = new FileInputStream(zipFile);
            cnt = unzip(fis, outputDir);
            fis.close();
        }
        catch (Exception e)
        {
            Log.log.warn("Failed to unzip. " + e.getMessage(), e);
        }

        return cnt;
    } // unzip(File zipFile, File outputDir)
    
    public void extractFile(File zipFile, File outputDir, String partialFileName)
    {
        try
        {
            ZipFile zip = new ZipFile(zipFile);
            Enumeration<? extends ZipEntry> e = zip.entries();
            while (e.hasMoreElements())
            {
                ZipEntry entry = e.nextElement();
                if (entry.getName().contains("update/uninstall")) 
                {
                    continue;
                }
                if (entry.getName().contains(partialFileName))
                {
                    File outFile = FileUtils.getFile(outputDir.getAbsolutePath() + File.separator + entry.getName());
                    outFile.getAbsolutePath();
                    if (!outFile.getParentFile().exists())
                    {
                        outFile.getParentFile().mkdirs();
                    }
                    
                    InputStream zin = zip.getInputStream(entry);
                    OutputStream out = new FileOutputStream(outFile);
                    byte[] buffer = new byte[8192];
                    int len;
                    while ((len = zin.read(buffer)) != -1)
                    {
                        out.write(buffer, 0, len);
                    }
                    out.close();
                    zin.close();
                    /*
                     * Changed in 3.5.1 We need all the files with name starting from impex_resolve_
                     * (ImpexResolveModule and it's corresponding definitions).
                     * So, partial file name is changed to 'resolve_impex_' and removed the break.
                     */
                    // break;
                }
            }
        }
        catch (Exception e)
        {
            Log.log.warn("Failed to extractFile. " + e.getMessage(), e);
        }
    }

    /**
     * @param fis
     * @param outputDir
     * @throws IOException
     * @throws FileNotFoundException
     */
    public int unzip(InputStream fis, File outputDir) throws IOException, FileNotFoundException
    {
        int zcnt = 0;
        BufferedOutputStream dest;
        ZipInputStream zis = new ZipInputStream(new BufferedInputStream(fis));
        ZipEntry entry;
        while ((entry = zis.getNextEntry()) != null)
        {
            if (entry.getName().contains("update/uninstall"))
            {
                continue;
            }
            Log.log.info("Extracting: " + entry);
            int count;
            byte data[] = new byte[BUFFER];
            File f = FileUtils.getFile(outputDir.getAbsolutePath() + File.separator + entry.getName());
            // write the files to the disk

            if (entry.isDirectory())
            {
                if (!f.exists()) f.mkdirs();
            }
            else
            {
                // create parent directories
                if (!f.getParentFile().exists())
                {
                    f.getParentFile().mkdirs();
                }

                FileOutputStream fos = new FileOutputStream(f);
                dest = new BufferedOutputStream(fos, BUFFER);
                while ((count = zis.read(data, 0, BUFFER)) != -1)
                {
                    dest.write(data, 0, count);
                }
                dest.flush();
                dest.close();
                zcnt++;
            }
        }
        zis.close();

        return zcnt;
    }// unzip(File outputDir, InputStream fis)

    /**
     * @param zipFile
     * @return
     */
    public String[] zipContent(File zipFile)
    {
        String[] content = null;
        ZipFile zip = null;
        try
        {
            zip = new ZipFile(zipFile);
            content = new String[zip.size()];
            Enumeration<ZipEntry> entries = (Enumeration<ZipEntry>) zip.entries();
            int i = 0;
            while (entries.hasMoreElements())
            {
                ZipEntry zipEntry = entries.nextElement();
                content[i++] = zipEntry.getName();
            }
        }
        catch (Exception e)
        {
            Log.log.warn("Failed to Read Zip Content. " + e.getMessage(), e);
        }
        finally
        {
            try
            {
                if (zip != null)
                {
                    zip.close();
                }
            }
            catch (Exception e)
            {
                Log.log.warn("Failed to Close Zip File. " + e.getMessage(), e);
            }
        }
        return content;
    }// zipContent(File zipFile)

    /**
     * @param dir
     * @param outFile
     */
    public void zip(File dir, File outFile)
    {
        try
        {
            zip(dir.getAbsoluteFile(), new FileOutputStream(outFile));
        }
        catch (FileNotFoundException e)
        {
            Log.log.warn("Failed to zip. " + e.getMessage(), e);
        }
    }// zip(File dir, File outFile)

    /**
     * @param dir
     * @param outputStream
     */
    public void zip(File dir, OutputStream outputStream)
    {
        ZipOutputStream out = null;
        try
        {
            // Create the ZIP file
            out = new ZipOutputStream(outputStream);
            zipDir(dir.getParentFile(), dir, out);
            out.close();
        }
        catch (FileNotFoundException e)
        {
            Log.log.warn("Failed to zip. " + e.getMessage(), e);
        }
        catch (IOException e)
        {
            Log.log.error("Failed to zip outputstream: " + e.toString(), e);
        }
        finally
        {
            if (out != null)
            {
                try
                {
                    out.close();
                }
                catch (IOException ioe)
                {
                    Log.log.error("Failed to close outputstream: " + ioe.getMessage(), ioe);
                }
            }
        }
    }// zip(File dir, OutputStream outputStream)

    /**
     * @param dirs
     * @param outFile
     */
    public void zip(File[] dirs, File rootDir, File outFile)
    {
        try
        {
            zip(dirs, rootDir, new FileOutputStream(outFile));
        }
        catch (FileNotFoundException e)
        {
            Log.log.warn("Failed to zip. " + e.getMessage(), e);
        }
    }// zip(File[] dir, File outFile)

    /**
     * @param dir
     * @param outputStream
     */
    public void zip(File[] dirs, File rootDir, OutputStream outputStream)
    {
        ZipOutputStream out = null;
        try
        {
            // Create the ZIP file
            out = new ZipOutputStream(outputStream);
            for (File dir : dirs)
            {
                if (!dir.getAbsolutePath().contains(rootDir.getAbsolutePath()))
                {
                    Log.log.warn("File " + dir.getAbsolutePath() + " Is Not on the Path of the Root Directory " + rootDir.getAbsolutePath());
                    zipDir(dir.getAbsoluteFile().getParentFile(), dir.getAbsoluteFile(), out);
                }
                else
                {
                    zipDir(rootDir.getAbsoluteFile(), dir.getAbsoluteFile(), out);
                }
            }
            out.close();
        }
        catch (FileNotFoundException e)
        {
            Log.log.warn("Failed to zip. " + e.getMessage(), e);
        }
        catch (IOException e)
        {
            Log.log.error("Failed to zip outputstream: " + e.toString(), e);
        }
        finally
        {
            if (out != null)
            {
                try
                {
                    out.close();
                }
                catch (IOException ioe)
                {
                    Log.log.error("Failed to close outputstream: " + ioe.getMessage(), ioe);
                }
            }
        }
    }// zip(File[] dirs, OutputStream outputStream)

    /**
     * @param workDir
     * @param toZip
     * @param zos
     */
    public void zipDir(File workDir, File toZip, ZipOutputStream zos)
    {
        try
        {
            // create a new File object based on the directory we
            // have to zip File
            if (toZip.isDirectory())
            {
                // get a listing of the directory content
                String[] dirList = toZip.list();

                // loop through dirList, and zip the files
                for (int i = 0; i < dirList.length; i++)
                {
                    File f = FileUtils.getFile(toZip, dirList[i]);
                    if (f.isDirectory())
                    {
                        // if the File object is a directory, call this
                        // function again to add its content recursively
                        String filePath = f.getPath();
                        if (fIncludeSubdirectories) zipDir(workDir, FileUtils.getFile(filePath), zos);
                        // loop again
                        continue;
                    }

                    addEntry(workDir, zos, f);
                }
            }
            else
            {
                addEntry(workDir, zos, toZip);
            }
        }
        catch (IOException e)
        {
            Log.log.error("Failed to zip directory: " + e.toString());
        }
    }// zipDir(File workDir, File toZip, ZipOutputStream zos)

    /**
     * @param workDir
     * @param zos
     * @param readBuffer
     * @param zipDir
     * @throws FileNotFoundException
     * @throws IOException
     */
    private static void addEntry(File workDir, ZipOutputStream zos, File zipDir) throws FileNotFoundException, IOException
    {
        byte[] readBuffer = new byte[BUFFER];
        FileInputStream fis = new FileInputStream(zipDir);
        // create a new zip entry
        // figure out relative dir for zip entry
        String relative = zipDir.getPath().substring(workDir.getPath().length() + 1);
        relative = relative.replace(File.separatorChar, '/');
        ZipEntry anEntry = new ZipEntry(relative);
        // place the zip entry in the ZipOutputStream object
        zos.putNextEntry(anEntry);
        // now write the content of the file to the ZipOutputStream
        int bytesIn = 0;
        while ((bytesIn = fis.read(readBuffer)) != -1)
        {
            zos.write(readBuffer, 0, bytesIn);
        }
        // close the Stream
        fis.close();
        zos.closeEntry();
    }// addEntry(File workDir, ZipOutputStream zos, File zipDir)

    /**
     * @param includeSubdirectories
     *            The includeSubdirectories to set.
     */
    public void setIncludeSubdirectories(boolean includeSubdirectories)
    {
        fIncludeSubdirectories = includeSubdirectories;
    }// setIncludeSubdirectories(boolean includeSubdirectories)

} // Compress
