/******************************************************************************
* (C) Copyright 2014
*
* Resolve Systems, LLC
*
* All rights reserved
* This software is distributed under license and may not be used, copied,
* modified, or distributed without the express written permission of
* Resolve Systems, LLC.
*
******************************************************************************/

package com.resolve.util;

import java.beans.Introspector;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.PrintWriter;
import java.io.Reader;
import java.io.StringReader;
import java.io.StringWriter;
import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Properties;
import java.util.Set;
import java.util.StringTokenizer;

import org.apache.commons.io.IOUtils;
import org.apache.commons.lang3.CharUtils;
import org.apache.commons.lang3.StringEscapeUtils;
import org.jsoup.Jsoup;
import org.jsoup.safety.Whitelist;

import com.google.common.base.CharMatcher;

import groovy.lang.GString;
import net.sf.json.JSON;
import net.sf.json.JSONArray;
import net.sf.json.JSONException;
import net.sf.json.JSONNull;
import net.sf.json.JSONObject;
import net.sf.json.JSONSerializer;
import net.sf.json.JsonConfig;

public class StringUtils extends org.apache.commons.lang3.StringUtils
{
    public final static String VALUESEPARATOR = "|=|";
    public final static String FIELDSEPARATOR = "|&|";
    public final static String LINESEPARATOR = "|;|";
    public final static String VALUESEPARATOR_REGEX = "\\|=\\|";
    public final static String FIELDSEPARATOR_REGEX = "\\|&\\|";
    public final static String LINESEPARATOR_REGEX = "\\|;\\|";

    public static final String HTML_BEGIN = "<!-- HTML_BEGIN -->";
    public static final String HTML_END = "<!-- HTML_END -->";

    //this is a special delimeter that is used for some special situation to concatenate string
    public static final String DELIMITER = Character.toString((char) 0x00A7);

    public static String join(String[] strings, String separator, int start, int end)
    {
        String result = null;
        if (strings != null && strings.length > 0)
        {
            if (start >= 0 && end < strings.length)
            {
                StringBuffer sb = new StringBuffer();

                for (int x = start; x <= end; x++)
                {
                    sb.append(strings[x]);
                    if (x + 1 <= end)
                    {
                        sb.append(separator);
                    }
                }
                // sb.append( strings[ strings.length - 1 ] );
                result = sb.toString();
            }
        }

        return result;
    } // join

    public static String quote(String str)
    {
        return quote(str, "\"");
    } // quote

    public static String quote(String str, String quote)
    {
        String result = null;

        if (str.startsWith(quote))
        {
            result = str;
        }
        else
        {
            result = quote + str + quote;
        }

        return result;
    } // quote

    public static String escapeQuotes(String str)
    {
        String result = str;

        // must replace \ first
        result = result.replace("\\", "\\\\");

        result = result.replace("\"", "\\\"");
        result = result.replace("\'", "\\\'");
        result = result.replace("`", "\\`");
        result = result.replace("$", "\\$");

        return result;
    } // escapeQuotes

    public static String escapeQuotesForBash(String str)
    {
        String result = str;

        // must replace \ first
        result = result.replace("\\", "\\\\");

        // NOTE single-quotes are not escaped
        result = result.replace("\"", "\\\"");
        result = result.replace("`", "\\`");
        result = result.replace("$", "\\$");

        return result;
    } // escapeQuotesForBash
    /**
     * Converts any passed string to Camel case
     * @param passStr
     * @return
     */
    public static String toCamelCase(String passStr) {
        String result = "";
        if(passStr != null && !passStr.isEmpty()) {
            result = Character.toUpperCase(passStr.charAt(0)) + passStr.substring(1, passStr.length()).toLowerCase();
        }
        return result;
    }
    public static String objToString(Object obj)
    {
        String result = "";
        ByteArrayOutputStream baos = null;
        ObjectOutputStream oos = null;

        try
        {
            if (obj != null)
            {
                baos = new ByteArrayOutputStream();
                oos = new ObjectOutputStream(baos);
                oos.writeObject(obj);
                baos.toByteArray();
                result = Base64.encodeBytes(baos.toByteArray(), Base64.GZIP | Base64.DONT_BREAK_LINES);
            }
        }
        catch (Exception e)
        {
            Log.log.warn(e.getMessage(), e);
        }
        finally
        {
            try
            {
                if (oos != null)
                {
                    oos.close();
                }
                if (baos != null)
                {
                    baos.close();
                }
            }
            catch (Exception exp)
            {
            }
        }

        return result;
    } // objToString

    public static String setToString(Set set)
    {
        return setToString(set, FIELDSEPARATOR);
    } // setToString

    public static String setToString(Set<String> set, String fieldSeparator)
    {
        String result = "";

        if (set != null)
        {
            for (Iterator i = set.iterator(); i.hasNext();)
            {
                String value = (String) i.next();
                if (value == null)
                {
                    value = "";
                }

                result += value.toString();

                if (i.hasNext())
                {
                    result += fieldSeparator;
                }
            }
        }

        return result;
    } // setToString

    public static String mapToLogString(Map map)
    {
        StringBuilder sb = new StringBuilder();
        if (map != null)
        {
            sb.append("{");
            for (Object key : map.keySet())
            {
                Object temp = map.get(key);
                // temp could be null and then it'll throw NPE in line#231
                if (temp instanceof EncryptionTransportObject)
                {
                    if (StringUtils.isNotEmpty(((EncryptionTransportObject) temp).toLogString()))
                    {
                        if (sb.length() > 1) sb.append(",");
                        sb.append(key.toString()).append("=").append(((EncryptionTransportObject) temp).toLogString());
                    }
                }
                else
                {
                    if (temp == null)
                    {
                        if (sb.length() > 1) sb.append(",");
                        sb.append(key.toString()).append("=NULL");
                    }
                    else
                    {
                        if (StringUtils.isNotEmpty(temp.toString()))
                        {
                            if (sb.length() > 1) sb.append(",");
                            sb.append(key.toString()).append("=").append(temp.toString());
                        }
                    }
                }
            }
            sb.append("}");
        }
        return sb.toString();
    }

    
    /**
     * Converts any Object to a loggable String.
     *
     * @param object
     */
    public static String objectToLogString(Object object, String... getMethodsToIngore)
    {
        Map<String, String> result = new HashMap<String, String>();

        List<String> methodsToIgnore = new ArrayList<String>();
        
        if (getMethodsToIngore != null && getMethodsToIngore.length > 0)
        {
            methodsToIgnore = Arrays.asList(getMethodsToIngore);
        }
        
        Method[] methods = object.getClass().getMethods();
        for (Method method : methods)
        {
            if (method.getName().startsWith("get") && !method.getName().equals("getClass") && 
                (methodsToIgnore.isEmpty() || !methodsToIgnore.contains(method.getName())))
            {
                try
                {
                    String propertyName = method.getName().substring(3); //ignore "get"
                    result.put(propertyName.toUpperCase(), BeanUtil.getStringProperty(object, Introspector.decapitalize(propertyName)));
                }
                catch (NoSuchMethodException nsme)
                {
                    Log.log.debug("Object of type " + object.getClass().getSimpleName() + " has a getter method named " + 
                                  method.getName() + " but is missing corresponding variable named " + 
                                  Introspector.decapitalize(method.getName().substring(3)) + 
                                  " in converting object to log string objectToLogString(Object, String...) method.");
                }
                catch (Exception e)
                {
                    Log.log.warn("Error " + e.getMessage() + " in converting object of type " + 
                                 object.getClass().getSimpleName() + 
                                 " to log string in method objectToLogString(Object, String...) is being ignored", e);
                }
            }
        }
        
        return mapToLogString(result);
    }

    public static String mapToString(Map map)
    {
        return mapToString(map, VALUESEPARATOR, FIELDSEPARATOR);
    } // mapToString

    public static String mapToJson(Map map)
    {
        String result = "";

        if (map != null)
        {
            JSONObject json = new JSONObject();
            json.putAll(map);
            result = json.toString();
        }
        return result;
    } // mapToString

    public static String mapToString(Map map, String valueSeparator, String fieldSeparator)
    {
        StringBuilder result = new StringBuilder("");

        if (map != null)
        {
            for (Iterator i = map.entrySet().iterator(); i.hasNext();)
            {
                Map.Entry entry = (Map.Entry) i.next();
                String key = (String) entry.getKey();
                Object value = entry.getValue();
                if (value == null)
                {
                    value = "";
                }

                // encode value
                // value = java.net.URLEncoder.encode(value, "UTF-8");
                
                result.append(key).append(valueSeparator)
                .append(value.getClass().isArray() ? Arrays.toString((Object[])value) : value);
                
                if (i.hasNext())
                {
                    result.append(fieldSeparator);
                }
            }
        }
        
        return result.toString();
    } // mapToString

    /**
     * Converts a {@link List} of {@link Map} to String.
     *
     * @param list
     * @return
     */
    public static String listToStringOfMap(List<Map<String, String>> list)
    {
        return listToStringOfMap(list, VALUESEPARATOR, FIELDSEPARATOR, LINESEPARATOR);
    }

    public static String listToStringOfMap(List<Map<String, String>> list, String valueSeparator, String fieldSeparator, String lineSeparator)
    {
        StringBuilder result = new StringBuilder();

        if (list != null)
        {
            for (Map<String, String> map : list)
            {
                for (String key : map.keySet())
                {
                    result.append(key + valueSeparator + ((map.get(key) == null ? "" : map.get(key))));
                    result.append(fieldSeparator);
                }
                // removing the extra field seperator.
                if (result.length() >= fieldSeparator.length())
                {
                    result.setLength(result.length() - fieldSeparator.length());
                }
                result.append(lineSeparator);
            }

            // removing the extra line seperator.
            if (result.length() >= lineSeparator.length())
            {
                result.setLength(result.length() - lineSeparator.length());
            }
        }
        return result.toString();
    } // listToStringOfMap

    /**
     * Converts a String to a List of Map. For example if the seed is
     * "key10|=|value10|&amp;|key11|=|value11|;|key20|=|value20|&amp;|key21|=|bar21" it
     * will make a {@link List} with two {@link Map} in it.
     *
     * @param seed
     * @return
     */
    public static List<Map<String, String>> stringToListOfMap(String seed)
    {
        return stringToListOfMap(seed, VALUESEPARATOR, FIELDSEPARATOR, LINESEPARATOR_REGEX);
    }

    public static List<Map<String, String>> stringToListOfMap(String seed, String valueSeparator, String fieldSeparator, String lineSeparator)
    {
        List<Map<String, String>> result = new ArrayList<Map<String, String>>();

        List<String> mapsString = stringToList(seed, lineSeparator);
        for (String mapString : mapsString)
        {
            Map<String, String> map = stringToMap(mapString, valueSeparator, fieldSeparator);
            result.add(map);
        }
        return result;
    } // listToStringOfMap

    public static String mapToUrl(Map<String, String> map)
    {
        StringBuilder result = new StringBuilder("");

        try
        {
            if (map != null)
            {
                for (String key : map.keySet())
                {
                    String value = map.get(key);
                    String encodedValue = "";
                    
                    if (value != null)
                    {
                        encodedValue = java.net.URLEncoder.encode(value.toString(), "UTF-8");
                    }
                    String encodedKey = java.net.URLEncoder.encode(key, "UTF-8");
                    result.append(encodedKey).append("=").append(encodedValue).append("&");
                }
                //removing the last "&"
                result.setLength(result.length() - 1);
            }
        }
        catch (Exception e)
        {
            Log.log.debug(e.getMessage(), e);
        }
        return result.toString();
    } // mapToUrl

    public static Object stringToObj(String str)
    {
        Object result = null;

        ByteArrayInputStream bais = null;
        ObjectInputStream objInputStream = null;

        try
        {
            if (str != null && !str.equals(""))
            {
                byte[] base64DecodedBytes = Base64.decode(str, Base64.GZIP | Base64.DONT_BREAK_LINES);
                
                if (base64DecodedBytes != null)
                {
                    bais = new ByteArrayInputStream(base64DecodedBytes);
                    objInputStream = new ObjectInputStream(bais);
                    result = objInputStream.readObject();
                }
                else
                {
                    //Log.log.warn("Failed to convert Base64 encoded string [" + str + "] to object, returning null object!!!");
                    System.err.println("Failed to convert Base64 encoded string [" + str + "] to object, returning null object!!!");
                }
            }
        }
        catch (Exception e)
        {
            Log.log.warn(e.getMessage(), e);
        }
        finally
        {
            try
            {
                if (bais != null)
                {
                    bais.close();
                }
                if (objInputStream != null)
                {
                    objInputStream.close();
                }
            }
            catch (Exception exp)
            {
            }
        }

        return result;
    } // stringToObj
    
    public static Set<String> stringToSet(String str)
    {
        return stringToSet(str, FIELDSEPARATOR_REGEX);
    } // stringToSet

    /**
     * Convert a string with given field separator in a Collection (Set)
     * 
     * @param str : The imput String which should be split and converted into a Set.
     * @param fieldSeparator : String representing a separator
     * @return
     *      An empty Set if str is blank or if there's no char other than a separator
     *      or a Set of chars/Strings seperated by a separator.
     *      E.g. ", a, , , b " =>[a,b]
     *           "  ,  ,  ,  " =>[]
     */
    public static Set<String> stringToSet(String str, String fieldSeparator)
    {
        Set<String> result = new HashSet<String>();

        if (isNotBlank(str) && !str.equalsIgnoreCase("NULL"))
        {
            result.addAll(Arrays.asList(StringUtils.split(str.replaceAll("\\s+", ""), fieldSeparator)));
        }
        return result;
    } // stringToSet

    public static Map stringToMap(String str)
    {
        return stringToMap(str, VALUESEPARATOR, FIELDSEPARATOR);
    } // stringToMap

    public static Map stringToMap(String str, String valueSeparator, String fieldSeparator)
    {
        return stringToMap(str, valueSeparator, fieldSeparator, true, false);
    } // stringToMap

    public static Map<String, String> stringToMap(String str, String valueSeparator, String fieldSeparator, boolean trim, boolean keyToUppercase)
    {
        Map<String, String> result = new LinkedHashMap<String, String>();

        if (!isEmpty(str) && !str.equalsIgnoreCase("NULL"))
        {
            String[] params = StringUtils.splitByWholeSeparator(str, fieldSeparator);
            for (int i = 0; i < params.length; i++)
            {
                String name = "";
                String value = "";

                int valIdx = params[i].indexOf(valueSeparator);
                if (valIdx <= 0)
                {
                    name = params[i];
                    value = "";
                }
                else
                {
                    name = params[i].substring(0, valIdx);
                    value = params[i].substring(valIdx + valueSeparator.length(), params[i].length());
                }
                if (StringUtils.isNotEmpty(name))
                {
                    if (keyToUppercase)
                    {
                        name = name.toUpperCase();
                    }
                    if (trim)
                    {
                        result.put(name.trim(), value.trim());
                    }
                    else
                    {
                        result.put(name, value);
                    }
                }
            }
        }

        return result;
    } // stringToMap

    public static Map<String, String> stringToMap(String str, String valueSeparator, String fieldSeparator, boolean keyToLowerCase)
    {
        Map<String, String> result = new LinkedHashMap<String, String>();

        if (!isEmpty(str) && !str.equalsIgnoreCase("NULL"))
        {
            String[] params = StringUtils.splitByWholeSeparator(str, fieldSeparator);
            for (int i = 0; i < params.length; i++)
            {
                String name = "";
                String value = "";

                int valIdx = params[i].indexOf(valueSeparator);
                if (valIdx <= 0)
                {
                    name = params[i];
                    value = "";
                }
                else
                {
                    name = params[i].substring(0, valIdx);
                    value = params[i].substring(valIdx + valueSeparator.length(), params[i].length());
                }
                if (StringUtils.isNotEmpty(name))
                {
                    if (keyToLowerCase)
                    {
                        name = name.toLowerCase();
                    }
                    result.put(name.trim(), value.trim());
                }
            }
        }

        return result;
    } // stringToMap

    public static Map stringToMapWithTrim(String str, String valueSeparator, String fieldSeparator)
    {
        LinkedHashMap result = new LinkedHashMap();

        if (!isEmpty(str) && !str.equalsIgnoreCase("NULL"))
        {
            String[] params = str.split(fieldSeparator);
            for (int i = 0; i < params.length; i++)
            {
                String name = "";
                String value = "";

                int valIdx = params[i].indexOf(valueSeparator);
                if (valIdx <= 0)
                {
                    name = params[i];
                    value = "";
                }
                else
                {
                    name = params[i].substring(0, valIdx);
                    value = params[i].substring(valIdx + valueSeparator.length(), params[i].length());
                }
                if (StringUtils.isNotEmpty(name))
                {
                    result.put(name, value);
                }
            }
        }

        return result;
    } // stringToMapWithTrim

    public static Map stringToMapBooleanValue(String str)
    {
        return stringToMapBooleanValue(str, VALUESEPARATOR, FIELDSEPARATOR_REGEX);
    } // stringToMapBooleanValue

    public static Map stringToMapBooleanValue(String str, String valueSeparator, String fieldSeparator)
    {
        LinkedHashMap result = new LinkedHashMap();

        if (!isEmpty(str) && !str.equalsIgnoreCase("NULL"))
        {
            String[] params = str.split(fieldSeparator);
            for (int i = 0; i < params.length; i++)
            {
                String name = "";
                String value = "";

                int valIdx = params[i].indexOf(valueSeparator);
                if (valIdx <= 0)
                {
                    name = params[i];
                    value = "";
                }
                else
                {
                    name = params[i].substring(0, valIdx);
                    value = params[i].substring(valIdx + valueSeparator.length(), params[i].length());
                }
                if (StringUtils.isNotEmpty(name))
                {
                    result.put(name, new Boolean(value));
                }
            }
        }

        return result;
    } // stringToMapBooleanValue

    /**
     * Converts a true application/x-www-form-urlencoded string to a Map.
     * 
     * @param url - example WIKI%3Dtest.wiki%26PARAM%3Dmy%20value
     * @return
     */
    public static Map<String, String> urlToMap(final String url)
    {
        return urlToMap(url, true);
    }
    
    /**
     * Converts a complete or partial application/x-www-form-urlencoded string to a Map.
     * The "isFullyEncoded" parameter indicates if the incoming "url" parameter is a truly 
     * encoded url or not. In Resolve there are certain usage where only the keys and values
     * are encoded but the "=" and "&amp;" signs come as it is (refer AjaxJobScheduler.java).
     * 
     * For example if a raw URL is like "WIKI=test.wiki&amp;PARAM=my value" then the full or
     * partially encoded URL would look like:
     * 
     * Full: WIKI%3Dtest.wiki%26PARAM%3Dmy%20value
     * Partial: WIKI=test.wiki&amp;PARAM=my%20value
     *  
     * @param url
     * @param isFullyEncoded
     * @return
     */
    public static Map<String, String> urlToMap(final String url, final boolean isFullyEncoded)
    {
        Map<String, String> result = new LinkedHashMap<String, String>();

        try
        {
            if(isNotBlank(url))
            {
                String tmpStr = (isFullyEncoded ? java.net.URLDecoder.decode(url, "UTF-8") : url);
                
                if (isNotBlank(tmpStr) && !tmpStr.equalsIgnoreCase("NULL"))
                {
                    String[] params = tmpStr.split("&");
                    for (int i = 0; i < params.length; i++)
                    {
                        String name = "";
                        String value = "";
    
                        int valIdx = params[i].indexOf("=");
                        if (valIdx <= 0)
                        {
                            name = params[i];
                            value = "";
                        }
                        else
                        {
                            if(isFullyEncoded)
                            {
                                name = params[i].substring(0, valIdx);
                                value = params[i].substring(valIdx + 1, params[i].length());
                            }
                            else
                            {
                                name = java.net.URLDecoder.decode(params[i].substring(0, valIdx), "UTF-8");
                                value = java.net.URLDecoder.decode(params[i].substring(valIdx + 1, params[i].length()), "UTF-8");
                            }
                        }
                        if (StringUtils.isNotEmpty(name))
                        {
                            if (result.get(name) != null)
                            {
                                value = result.get(name) + "," + value;
                            }
                            result.put(name, value);
                        }
                    }
                }
            }
        }
        catch (Exception e)
        {
            Log.log.debug(e.getMessage(), e);
        }

        return result;
    } // urlToMap

    public static String getUrlString(String inputString)
    {
        String urlEncodeString = "";

        try
        {
            if (inputString != null && !inputString.equals(""))
            {
                urlEncodeString = java.net.URLDecoder.decode(inputString, "UTF-8");
            }
        }
        catch (Exception exp)
        {
        }

        return urlEncodeString;
    }

    public static String listToString(List<String> list)
    {
        return listToString(list, FIELDSEPARATOR);
    } // listToString

    public static String listToString(List<String> list, String separator)
    {
        StringBuilder resultBldr = new StringBuilder("");

        if (list != null) {
        	if (StringUtils.isBlank(separator)) {
        		separator = FIELDSEPARATOR;
        	}
        	
	        for (Iterator<String> i = list.iterator(); i.hasNext();)
	        {
	            String value = i.next();
	            resultBldr.append(value);
	
	            if (i.hasNext())
	            {
	                resultBldr.append(separator);
	            }
	        }
        }

        return resultBldr.toString();
    } // listToString

    public static String collectionToString(Collection<String> collection)
    {
        return collectionToString(collection, FIELDSEPARATOR);
    } // collectionToString
    
    public static String collectionToString(Collection<String> collection, String separator)
    {
        String result = "";

        if (collection.isEmpty())
            return result;
        
        for (String collectionElement : collection)
        {
            result += ( collectionElement + separator);
        }

        // remove last separator
        
        return StringUtils.substringBeforeLast(result, separator);
    }
    
    public static List<String> stringToList(String str)
    {
        return stringToList(str, FIELDSEPARATOR_REGEX);
    } // stringToList

    /**
     * Convert a string with given field separator in a Collection (List)
     * 
     * @param str : The imput String which should be split and converted into a List.
     * @param fieldSeparator : String representing a separator
     * @return
     *      An empty List if str is blank or if there's no char other than a separator
     *      or a List of chars/Strings seperated by a separator.
     *      E.g. ", a, , , b " =>[a,b]
     *           " one two, five six, ," =>[one two,five six]
     *           "  ,  ,  ,  " =>[]
     */
    public static List<String> stringToList(String str, String separator)
    {
        ArrayList<String> result = new ArrayList<String>();

        if (isNotBlank(str) && !str.equalsIgnoreCase("NULL"))
        {
            result.addAll(Arrays.asList(StringUtils.split(str.trim().replaceAll("\\s*" + separator + "\\s*", separator), separator)));
        }

        return result;
    } // stringToList

    public static int stringToInt(String str)
    {
        int result;

        try
        {
            if (StringUtils.isEmpty(str))
            {
                result = 0;
            }
            else if (str.equalsIgnoreCase("null"))
            {
                result = 0;
            }
            else
            {
                result = Integer.parseInt(str);
            }
        }
        catch (NumberFormatException e)
        {
            Log.log.warn(e.getMessage());
            result = 0;
        }

        return result;
    } // stringToInt

    public static long stringToLong(String str)
    {
        long result;

        try
        {
            if (StringUtils.isEmpty(str))
            {
                result = 0;
            }
            else
            {
                result = Long.parseLong(str);
            }
        }
        catch (NumberFormatException e)
        {
            Log.log.warn(e.getMessage());
            result = 0;
        }

        return result;
    } // stringToLong

    public static String printPrettyMap(Map data)
    {
        String result = "";

        // get max key length
        int maxKeyLength = 0;
        for (Iterator i = data.entrySet().iterator(); i.hasNext();)
        {
            Map.Entry entry = (Map.Entry) i.next();
            String key = (String) entry.getKey();

            if (key.length() > maxKeyLength)
            {
                maxKeyLength = key.length();
            }
        }

        // print data
        for (Iterator i = data.entrySet().iterator(); i.hasNext();)
        {
            Map.Entry entry = (Map.Entry) i.next();
            String key = (String) entry.getKey();
            String value = (String) entry.getValue();

            result += StringUtils.rightPad(key, maxKeyLength) + "  " + value + "\n";
        }

        return result;
    } // printPrettyMap

    /*
     * Convert msecs to friendly string representation
     */
    public static String getDuration(long duration)
    {
        String result = "";

        long msec;
        long sec;
        long min;
        long hr;
        long day;

        msec = duration % 1000;
        duration = duration / 1000;

        sec = duration % 60;
        duration = duration / 60;

        min = duration % 60;
        duration = duration / 60;

        hr = duration % 24;
        duration = duration / 24;

        day = duration % 365;

        result = msec + "ms";
        if (sec > 0)
        {
            result = sec + "s " + result;
        }
        if (min > 0)
        {
            result = min + "m " + result;
        }
        if (hr > 0)
        {
            result = hr + "h " + result;
        }
        if (day > 0)
        {
            result = day + "d " + result;
        }

        return result;
    } // getDuration

    public static String getDuration(String duration)
    {
        return getDuration(Long.parseLong(duration));
    } // getDuraiton

    public static String getXMLString(String data, String tag)
    {
        String result = null;

        int startPos = data.indexOf("<" + tag + ">");
        int endPos = data.indexOf("</" + tag + ">") + tag.length() + 3;
        if (startPos >= 0 && endPos > 0)
        {
            result = data.substring(startPos, endPos);
        }
        return result;
    } // getXMLString

    /**
     * Converts the iterator data to a string with the required separator eg.
     * Set&lt;String&gt; readAccess = new TreeSet&lt;String&gt;(); ... String str =
     * Utils.convertCollectioToString(readAccess.iterator(), ",");
     *
     * @param it
     * @param separator
     * @return
     */
    public static String convertCollectionToString(Iterator<String> it, String separator)
    {
        String str = null;
        if (it != null)
        {
            while (it.hasNext())
            {
                String tempStr = it.next();
                if (tempStr != null)
                {
                    if (str == null)
                    {
                        str = tempStr.trim();
                    }
                    else
                    {
                        str = str.trim() + separator + tempStr.trim();
                    }
                }
            }
        }
        return str;
    } // collectionToString

    /**
     * Converts the string with a delimiter to a List eg. a, b, c will be
     * converted in a list
     *
     * @param str
     * @param seperatedBy
     * @return
     */
    public static List<String> convertStringToList(String str, String seperatedBy)
    {

        List<String> list = new ArrayList<String>();

        if (str != null && str.trim().length() > 0)
        {
            StringTokenizer st = new StringTokenizer(str, seperatedBy);
            while (st.hasMoreTokens())
            {
                list.add(st.nextToken().trim());
            }
        }

        return list;
    } // convertStringToList

    /*
     * public static String getStackTrace(Throwable e) { String result = "\n";
     *
     * StackTraceElement[] trace = e.getStackTrace(); for (int i=0; i <
     * trace.length; i++) { result += trace[i].toString()+"\n"; }
     *
     * return result; } // getStacktrace
     */

    public static String getStackTrace(Throwable t)
    {
        StringWriter sw = new StringWriter();
        PrintWriter pw = new PrintWriter(sw, true);

        t.printStackTrace(pw);
        pw.flush();
        sw.flush();

        return sw.toString();
    } // getStackTrace

    public static void trimStringCollection(Collection<String> collection)
    {
        if (collection != null && collection.size() > 0)
        {
            for (String s : collection)
            {
                s = s.trim();
            }
        }
    }// trimStringCollection

    /**
     * This method cleans the HTML tags from the incoming string.
     *
     * Example:
     *  incoming: &lt;table&gt;&lt;tr&gt;foo&lt;/tr&gt;&lt;td&gt;bar&lt;/td&gt;&lt;/table&gt;
     *  outgoing: foobar
     *
     * @param str
     * @return
     */
    public static String htmlCleaner(String str)
    {
        return htmlCleaner(str, false);
    } // htmlCleaner

    /**
     * This method cleans the incoming string from html.
     * 
     * @param str
     * @param removeSpace if true removes &amp;nbsp;
     * @return
     */
    public static String htmlCleaner(String str, boolean removeSpace)
    {
        String result = null;
        if (StringUtils.isNotBlank(str))
        {
            result = Jsoup.clean(str, Whitelist.simpleText());
            if(removeSpace && result.contains("&nbsp;"));
            {
                result = result.replaceAll("&nbsp;", "");
            }
        }
        return result;
    }
    
    /**
     * Enum to indicate the what type of WhiteList the calling code needs.
     *
     */
    public static enum WhiteList
    {
        SIMPLETEXT, BASIC, BASICWITHIMAGES, NONE, RELAXED, CUSTOM;
    }
    
    /**
     * Advanced html cleaner that uses entire feature from the JSoup library.
     * 
     * @param str
     * @param removeSpace
     * @param whiteList
     * @param attributes such as "img", "src" are useful with CUSTOM whitelist. at least one
     * attribute must be provided if the whitelist is CUSTOM.
     * @return
     */
    public static String htmlCleaner(String str, boolean removeSpace, WhiteList whiteList, String... attributes)
    {
        String result = null;
        if (StringUtils.isNotBlank(str))
        {
            switch (whiteList)
            {
                case SIMPLETEXT:
                    result = Jsoup.clean(str, Whitelist.simpleText());
                    break;
                case BASIC:
                    result = Jsoup.clean(str, Whitelist.basic());
                    break;
                case BASICWITHIMAGES:
                    result = Jsoup.clean(str, Whitelist.basicWithImages());
                    break;
                case NONE:
                    result = Jsoup.clean(str, Whitelist.none());
                    break;
                case RELAXED:
                    result = Jsoup.clean(str, Whitelist.relaxed());
                    break;
                case CUSTOM:
                    if(attributes != null && attributes.length > 0)
                    {
                        Whitelist customWhiteList = new Whitelist();
                        customWhiteList.addAttributes(attributes[0], attributes);
                        result = Jsoup.clean(str, customWhiteList);
                    }
                    break;
                default:
                    //nothing to do so send back the original string
                    result = str;
            }
            
            if(removeSpace && result.contains("&nbsp;"));
            {
                result = result.replaceAll("&nbsp;", "");
            }
        }
        return result;
    }
    
    public static String escapeHtml(String str)
    {
        String result = str;
        try
        {
            result = org.apache.commons.lang3.StringEscapeUtils.escapeHtml3(str);
        }
        catch (Exception e)
        {
            Log.log.error("Failed to encode HTML: " + e.getMessage(), e);
        }
        return result;
    } // escapeHtml

    public static String unescapeHtml(String str)
    {
        String result = str;
        try
        {
            result = org.apache.commons.lang3.StringEscapeUtils.unescapeHtml3(str);
        }
        catch (Exception e)
        {
            Log.log.error("Failed to encode HTML: " + e.getMessage(), e);
        }
        return result;
    } // unescapeHtml

    /**
     * @param searchString
     *        eg User entered --&gt; "passport interface"
     *           output --&gt; %passport%, %interface%
     *
     *
     * @return
     */
    public static List<String> prepareSearchStringList(String searchString)
    {
        List<String> searchStringList = new ArrayList<String>();

        searchString = searchString.toLowerCase().trim();
        searchString = searchString.replace('*', '%');

        String[] searchWords = searchString.split(" ");
        for (String searchWord : searchWords)
        {
            searchWord = searchWord.trim();
            if (!searchWord.startsWith("%"))
            {
                searchWord = "%" + searchWord;
            }
            if (!searchWord.endsWith("%"))
            {
                searchWord = searchWord + "%";
            }

            searchStringList.add(searchWord);
        }

        return searchStringList;
    }

    public static String convertMapToString(Map<String, String> status)
    {
        StringBuffer result = new StringBuffer();
        Iterator<String> it = status.keySet().iterator();

        while (it.hasNext())
        {
            String key = it.next();
            String value = status.get(key);

            result.append(key).append("=").append(value).append("\n");
        }// end of while loop

        return result.toString();
    }// convertMapToString
    
   
    public static boolean isNotEmpty(String string)
    {
        return (!isEmpty(string));
    }

    public static Map<String, String> arraysToMap(String[] names, Object[] values)
    {
        Map<String, String> result = new HashMap<String, String>();

        if (names != null && values != null)
        {
            for (int i = 0; i < names.length; i++)
            {
                if (names[i] != null && values[i] != null)
                {
                    String value = "";

                    try
                    {
                        value = values[i].toString();
                    }
                    catch (Throwable e)
                    {
                    }
                    ;

                    result.put(names[i], value);
                }
            }
        }

        return result;
    } // arraysToMap

    public static String arrayToString(String[] list)
    {
        return arrayToString(list, FIELDSEPARATOR);
    } // arrayToString

    public static String arrayToString(String[] tokens, String seperatedBy)
    {
        StringBuilder result = new StringBuilder();
        if(isBlank(seperatedBy))
        {
            seperatedBy = ",";
        }
        if(tokens != null && tokens.length > 0)
        {
            for(String token : tokens)
            {
            	if (token != null) {
	                result.append(token.trim());
	                result.append(seperatedBy);
            	}
            }
            result.setLength(result.length() - seperatedBy.length());
        }
        return result.toString();
    } // arrayToString

    public static String[] stringToArray(String str)
    {
        return stringToArray(str, FIELDSEPARATOR_REGEX);
    } // stringToArray

    public static String[] stringToArray(String str, String separator)
    {
        String[] result = null;

        if (StringUtils.isNotBlank(str))
        {
            result = str.split(separator);
        }

        return result;
    } // stringToArray

    /**
     * Return truth of the string value.
     *
     * null - false "" - false "0" - false "false" - false "true" - true "1" -
     * true "xyz" - true
     *
     * @param str
     * @return boolean value of string
     */
    public static boolean getBoolean(String str)
    {
        boolean result;

        if (str == null)
        {
            result = false;
        }
        else
        {
            if (str.length() == 0)
            {
                result = false;
            }
            else if (str.equalsIgnoreCase("false"))
            {
                result = false;
            }
            else if (str.equals("0"))
            {
                result = false;
            }
            else
            {
                result = true;
            }
        }
        return result;
    } // getBoolean

    public static boolean getBoolean(String key, Map params)
    {
        boolean result = false;

        Object value = params.get(key);
        if (value instanceof Boolean)
        {
            result = ((Boolean) value).booleanValue();
        }
        else if (value instanceof String)
        {
            result = ((String) value).equalsIgnoreCase("true") ? true : false;
        }

        return result;
    } // getBoolean

    public static int getInt(String key, Map params)
    {
        int result = -1;

        Object value = params.get(key);
        if (value instanceof Integer)
        {
            result = ((Integer) value).intValue();
        }
        else if (value instanceof String)
        {
            result = Integer.parseInt((String) value);
        }

        return result;
    } // getInt

    public static long getLong(String key, Map params)
    {
        long result = -1;

        Object value = params.get(key);
        if (value instanceof Long)
        {
            result = ((Long) value).longValue();
        }
        else if (value instanceof String)
        {
            result = Long.parseLong((String) value);
        }

        return result;
    } // getLong

    public static String getString(String key, Map params)
    {
        String result = null;

        Object value = params.get(key);
        if (value instanceof String)
        {
            result = (String) value;
        }
        else if (value instanceof GString)
        {
            result = value.toString();
        }

        return result;
    } // getString

    public static List<String> getList(String key, Map params)
    {
        return getList(key, params, FIELDSEPARATOR);
    } // getList

    public static List<String> getList(String key, Map params, String separator)
    {
        List<String> result = null;

        String strList = null;

        Object value = params.get(key);
        if (value instanceof String)
        {
            strList = (String) value;
        }
        else if (value instanceof GString)
        {
            strList = value.toString();
        }

        // parse list
        if (strList != null)
        {
            result = stringToList(strList);
        }

        return result;
    } // getList

    public static Properties getProperties(String key, Map params)
    {
        Properties result = null;

        String strVal = null;
        Object value = params.get(key);
        if (value instanceof String)
        {
            strVal = (String) value;
        }
        else if (value instanceof GString)
        {
            strVal = value.toString();
        }

        if (StringUtils.isNotEmpty(strVal))
        {
            try
            {
                result = new Properties();
                result.load(new StringReader(strVal));
            }
            catch (IOException e)
            {
                Log.log.error(e.getMessage(), e);
            }
        }

        return result;
    } // getProperties

    public static Properties stringToProperties(String str)
    {
        Properties result = null;

        if (StringUtils.isNotEmpty(str))
        {
            try
            {
                result = new Properties();
                result.load(new StringReader(str));
            }
            catch (IOException e)
            {
                Log.log.error(e.getMessage(), e);
            }
        }

        return result;
    } // stringToProperties

    /**
     * This is used to convert a JSONObject into String
     *
     * @param jobj
     * @return String
     */
    public static String jsonObjectToString(JSONObject jobj)
    {
        String jsonObjectStr = null;
        try
        {
            if (jobj != null)
            {
                jsonObjectStr = jobj.toString(2); // 2 -> indentation factor
            }
        }
        catch (RuntimeException e)
        {
            Log.log.error("JSONObject contains an invalid number: " + e.getMessage(), e);
            throw e;
        }
        return jsonObjectStr;
    }// jsonObjectToString

    /**
     * This is used to convert a JSONArray (collection of JSONObjects) into
     * String
     *
     * @param jsonArray
     * @return
     */

    public static String jsonArrayToString(JSONArray jsonArray)
    {
        String jsonArrayStr = null;
        try
        {
            if (jsonArray != null)
            {
                jsonArrayStr = jsonArray.toString(); // 2 -> indentation factor
            }
        }
        catch (RuntimeException e)
        {
            Log.log.error("JSONArray contains an invalid number: " + e.getMessage(), e);
            throw e;
        }
        return jsonArrayStr;
    }// jsonArrayToString

    public static JSONObject stringToJSONObject(String jsonString,boolean ignoreDefaultExcludes){
    	JSONObject jsonObject= null;
    	try
    	{
    		JsonConfig jsonConfig = new JsonConfig();
    		jsonConfig.setIgnoreDefaultExcludes(true);
    		jsonObject = (JSONObject) JSONSerializer.toJSON(jsonString,jsonConfig);
    	}
    	catch (RuntimeException e)
        {
            Log.log.error("JSONObject cannot be converted: " + e.getMessage(), e);
            throw e;
        }
    	return jsonObject;
    }
    /**
     * This is used to create a JSONObject from a JSON formatted string
     *
     * @param jsonString
     *            [user format:{'string':'JSON', 'integer': 1, 'double': 2.0,
     *            'boolean': true}]
     * @return
     */
    public static JSONObject stringToJSONObject(String jsonString)
    {
    	JSONObject jsonObject = null;
        try
        {
            //Object o = JSONSerializer.toJSON(jsonString);
            jsonObject = (JSONObject) JSONSerializer.toJSON(jsonString);
        }
        catch (RuntimeException e)
        {
            Log.log.error("JSONObject cannot be converted: " + e.getMessage(), e);
            throw e;
        }
        return jsonObject;
    }// stringToJSONObject
    
    public static JSONArray stringToJSONArray(String jsonString,boolean ignoreDefaultExcludes)
    {
        JSONArray jsonArray = null;
        try
        {
            if (isNotEmpty(jsonString))
            {
            	JsonConfig jsonConfig = new JsonConfig();
            	jsonConfig.setIgnoreDefaultExcludes(ignoreDefaultExcludes);
                jsonArray = (JSONArray) JSONSerializer.toJSON(jsonString,jsonConfig);
            }
        }
        catch (RuntimeException e)
        {
            Log.log.error("JSONArray cannot be converted: " + e.getMessage(), e);
            throw e;
        }
        return jsonArray;
    }// stringToJSONArray
    /**
     * This is used to create a JSONArray from a JSON formatted string
     *
     * @param jsonString
     *            [user format:['JSON', 1, 2.0, true]]
     * @return
     */
    public static JSONArray stringToJSONArray(String jsonString)
    {
        JSONArray jsonArray = null;
        try
        {
            if (isNotEmpty(jsonString))
            {
                jsonArray = (JSONArray) JSONSerializer.toJSON(jsonString);
            }
        }
        catch (RuntimeException e)
        {
            Log.log.error("JSONArray cannot be converted: " + e.getMessage(), e);
            throw e;
        }
        return jsonArray;
    }// stringToJSONArray

    /**
     * Check if the string is "true" (case-insensitive)
     *
     */
    public static boolean isTrue(String value)
    {
        boolean result = false;

        if (value != null && value.equalsIgnoreCase("true"))
        {
            result = true;
        }
        return result;
    } // isTrue

    /**
     * Check if the string is "false" (case-insensitive)
     *
     */
    public static boolean isFalse(String value)
    {
        boolean result = false;

        if (value == null || value.length() == 0 || value.equalsIgnoreCase("false"))
        {
            result = true;
        }
        return result;
    } // isFalse

    public static String utf8Conversion(String src, boolean failOnError)
    {
        String result = null;
        if (src != null)
        {
            try
            {
                byte[] bytes = src.getBytes("UTF-8");
                result = new String(bytes);
            }
            catch (Exception ex)
            {
                if (failOnError)
                {
                    Log.log.error(ex, ex);
                    throw new RuntimeException(ex);
                }
                else
                {
                    Log.log.warn("Bad data provided for utf8 conversion:" + src);
                    result = "Bad data provided for utf8 conversion";
                }
            }
        }
        return result;
    }

    public static String utf8Conversion(String src)
    {
        return utf8Conversion(src, true);
    }

    public static String escapeXml(String src)
    {
        return StringEscapeUtils.escapeXml(src);
    }
    
    public static String unescapeXml(String src)
    {
        return StringEscapeUtils.unescapeXml(src);
    }
    
    public static String escapeJava(String src)
    {
        return StringEscapeUtils.escapeJava(src);
    }
    
    public static String escapeJavascript(String src)
    {
        String result = src;
        
        if(StringUtils.isNotBlank(result))
        {
            //replace the <script> to <\script> to disable any js in the string
            result = result.replaceAll("(?mi)&lt;script&gt;", "&lt;\\\\script&gt;");
            
            //replace the </script> to <\/script> to disable any js in the string
            result = result.replaceAll("(?mi)&lt;/script&gt;", "&lt;\\\\/script&gt;");
        }
        
        return result;
    }
    
    public static String escapeMeta(String src)
    {
        String result = src;
        
        if(StringUtils.isNotBlank(result))
        {
            //replace the <meta to <\meta to disable any the META tag in the string
            result = result.replaceAll("(?mi)&lt;meta", "&lt;\\\\meta");
        }
        
        return result;
    }

    /**
     * This method is no longer agvailable in apache commons lang 3 so the code
     * here is copied from the commons lang 2.x source.
     *
     * @param src
     * @return
     */
    public static String escapeSql(String src)
    {
        String result = null;
        if (isNotBlank(src))
        {
            result = StringUtils.replace(src, "'", "''");
        }
        return result;
    }

    /**
     * This api is used mostly with respect to Resolve Social where you have an
     * html string and you want to Post it to a TARGET.
     *
     *
     * @param str
     * @return
     */
    public static String html2Post(String str)
    {
        String htmlstr = str;
        if (isNotEmpty(htmlstr))
        {
            htmlstr = HTML_BEGIN + str + HTML_END;
        }

        return htmlstr;
    }

    /**
     * Converts a String into InputStream using the apache common IO package.
     *
     * @param seed
     * @return
     */
    public static InputStream toInputStream(String seed)
    {
        return IOUtils.toInputStream(seed);
    }

    /**
     * Converts an InputStream into a String.
     *
     * @param seed
     * @param encoding
     * @return
     */
    public static String toString(InputStream seed, String encoding)
    {
        String result = null;
        try
        {
            result = IOUtils.toString(seed, encoding);
        }
        catch (IOException e)
        {
            Log.log.warn(e.getMessage(), e);
        }
        return result;
    }

    /**
     * Returns a substring of str with a length at least that of subLength. Cuts
     * the string at the first instance (after subLength characters) of an end
     * of a word or a punctuation mark. Starts at index 0. Will not cut until
     * all parenthetical symbols have been closed. If parenthetical symbols are
     * not balanced, then cuts normally.
     *
     * @param str
     *            The String to be cut.
     * @param subLength
     *            The minimum number of characters in the desired substring.
     */
    public static String smartSubstring(String str, int subLength)
    {
        return smartSubstring(str, subLength, true);
    } // smartSubstring

    /**
     * Returns a substring of str with a length at least that of subLength. Cuts
     * the string at the first instance (after subLength characters) of an end
     * of a word or a punctuation mark. Starts at index 0. If specified, will
     * not cut until all parenthetical symbols have been closed. If
     * parenthetical symbols are not balanced, then cuts normally.
     *
     * @param str
     *            The String to be cut.
     * @param subLength
     *            The minimum number of characters in the desired substring.
     * @param shouldCatchParens
     *            True if should wait for all parenthetical symbols to close
     *            before cutting.
     */
    public static String smartSubstring(String str, int subLength, boolean shouldCatchParens)
    {
        if (str == null)
        {
            return null;
        }

        int strLength = str.length();
        if (subLength >= strLength) // Return str directly if subLength is too
                                    // long
        {
            return str;
        }

        int parens = 0; // Counters for each parenthetical symbol
        int brackets = 0;
        int braces = 0;

        for (int charNum = (shouldCatchParens ? 1 : subLength); charNum <= strLength; charNum++)
        {
            char ch = str.charAt(charNum - 1);
            if (shouldCatchParens) // Balaced parentheses check if specified
            {
                switch (ch)
                {
                    case '(':
                        parens++;
                        break;
                    case ')':
                        parens = (parens == 0 ? 0 : parens - 1);
                        break;
                    case '[':
                        brackets++;
                        break;
                    case ']':
                        brackets = (brackets == 0 ? 0 : brackets - 1);
                        break;
                    case '{':
                        braces++;
                        break;
                    case '}':
                        braces = (braces == 0 ? 0 : braces - 1);
                        break;
                }
                if (parens != 0 || brackets != 0 || braces != 0) // Will not
                                                                 // return until
                                                                 // parentheses
                                                                 // are balanced
                {
                    continue;
                }
            }

            if (charNum == subLength && Character.isWhitespace(ch))
            {
                return str.substring(0, charNum);
            }

            if (charNum > subLength && !Character.isLetterOrDigit(ch))
            {
                if (ch != ')' && ch != ']' && ch != '}')
                {
                    charNum--;
                }
                return str.substring(0, charNum);
            }
        }

        if (shouldCatchParens && (parens != 0 || brackets != 0 || braces != 0)) // Unbalanced
                                                                                // parentheses
        {
            return smartSubstring(str, subLength, false);
        }

        return str;
    } // smartSubstring

    public static List<String> printList(List<String> inList)
    {
        List<String> outList = new ArrayList<String>();

        if (inList != null)
        {
            for (String value : inList)
            {
                outList.add("\"" + value + "\"");
            }
        }

        return outList;
    }

    public static String readAll(Reader reader) throws IOException
    {
        char[] arr = new char[8 * 1024]; // 8K at a time
        StringBuffer buf = new StringBuffer();
        int numChars;

        while ((numChars = reader.read(arr, 0, arr.length)) > 0)
        {
            buf.append(arr, 0, numChars);
        }

        return buf.toString();
    }

    public static byte[] readlInputStream(InputStream is) throws Exception
    {
        byte[] bytes = IOUtils.toByteArray(is);
        return bytes;
    }

    public static String readInputStreamAsString(InputStream is) throws Exception
    {
        byte[] bytes = IOUtils.toByteArray(is);
        return byteToString(bytes);
    }

    public static String byteToString(byte[] bytes)
    {
        String str = Arrays.toString(bytes);
        return str;
    }

    public static byte[] stringToBytes(String byteString)
    {
        String[] byteValues = byteString.substring(1, byteString.length() - 1).split(",");
        byte[] bytes = new byte[byteValues.length];

        for (int i = 0, len = bytes.length; i < len; i++)
        {
            bytes[i] = Byte.valueOf(byteValues[i].trim());
        }

        return bytes;
    }

    public static String getParam(Map params, String key)
    {
        return getParam(params, key, null);
    } // getParam

    public static String getParam(Map params, String key, String defaultValue)
    {
        String result = defaultValue;

        if (params.containsKey(key))
        {
            result = (String) params.get(key);
        }

        return result;
    } // getParam

    public static String getParam(Map params, String[] keys)
    {
        String result = null;

        int i = 0;
        boolean found = false;
        while (!found && i < params.size())
        {
            String key = keys[i++];

            if (params.containsKey(key))
            {
                result = (String) params.get(key);
                found = true;
            }
        }

        return result;
    } // getParam

    public static String prettyString(String str)
    {
        return (str == null) ? "" : str;
    } // prettyString

    /**
     *
     * This method is getting called from PCG
     *
     * It will return all the lines, including, From and To line number An empty
     * String is returned If 'From' line number is greater than the total lines
     * OR if 'To' line number is smaller than 'From' line number OR both 'From'
     * and 'To' line number is &lt;= 0. If 'To' line number is greater than total
     * lines or negative, it's considered as a last line. Zero is not a valid
     * input.
     *
     * @param rawData
     *            Input data
     * @param from
     *            Start line number
     * @param to
     *            End line number
     * @return Section of Data (String) from input rawData including from and to
     *         line. An empty String is returned if above criteria does not
     *         match.
     *
     **/
    public static String getContentBetweenLines(String rawData, int from, int to)
    {
        StringBuffer result = new StringBuffer();

        if ((from <= 0 && to <= 0))
        {
            /*
             * If either, 'from' or 'to' are negative or zero, it's wrong input.
             * Return an empty String
             */
            return "";
        }

        if (rawData != null)
        {
            int start = 1;
            if (from > 0)
            {
                start = from;
            }

            String[] lineArray = rawData.split("\n");
            int end = lineArray.length;

            if (to < lineArray.length && to > 0)
            {
                end = to;
            }

            if (end >= start)
            {
                List<String> lines = Arrays.asList(lineArray);
                lines = lines.subList(start - 1, end);

                for (String line : lines)
                {
                    result.append(line).append("\n");
                }
            }
        }

        return result.toString();
    } // getContentBetweenLines

    public static String stringToIdentifier(String str)
    {
        StringBuilder sb = new StringBuilder();
        if (!Character.isJavaIdentifierStart(str.charAt(0)))
        {
            sb.append("_");
        }

        for (char c : str.toCharArray())
        {
            if (!Character.isJavaIdentifierPart(c))
            {
                sb.append("_");
            }
            else
            {
                sb.append(c);
            }
        }

        return sb.toString();
    } // stringToIdentifier

    /**
     * Read a String and if it has line line put them into the list.
     *
     * @param seed
     * @return
     */
    public static List<String> readLines(String seed)
    {
        List<String> result = new ArrayList<String>();

        try
        {
            result = IOUtils.readLines(new StringReader(seed));
        }
        catch (IOException e)
        {
            Log.log.error(e.getMessage(), e);
        }
        return result;
    }

    /**
     * Remove the text from the start and end of the string.
     *
     * @param str
     *            - text to be processed
     * @param remove
     *            - text to be removed
     * @return
     */

    public static String removeStartAndEnd(final String str, final String remove)
    {
        String removedStart = StringUtils.removeStart(str, remove);
        return StringUtils.removeEnd(removedStart, remove);
    }

    /**
     * Remove the text from the start and end of the string.
     *
     * @param str
     *            - text to be processed
     * @param remove
     *            - text to be removed
     * @return
     */

    public static String removeStartAndEndIgnoreCase(final String str, final String remove)
    {
        String removedStart = StringUtils.removeStartIgnoreCase(str, remove);
        return StringUtils.removeEndIgnoreCase(removedStart, remove);
    }

    /**
     * Remove the prefix text and the postfix text from the string.
     *
     * @param str
     *            - text to be processed
     * @param removeStart
     *            - prefix text to be removed
     * @param removeEnd
     *            - postfix text to be removed
     * @return
     */
    public static String removeStartAndEnd(final String str, final String removeStart, final String removeEnd)
    {
        String removedStart = StringUtils.removeStart(str, removeStart);
        return StringUtils.removeEnd(removedStart, removeEnd);
    }

    /**
     * Remove the prefix text and the postfix text from the string.
     *
     * @param str
     *            - text to be processed
     * @param removeStart
     *            - prefix text to be removed
     * @param removeEnd
     *            - postfix text to be removed
     * @return
     */
    public static String removeStartAndEndIgnoreCase(final String str, final String removeStart, final String removeEnd)
    {
        String removedStart = StringUtils.removeStartIgnoreCase(str, removeStart);
        return StringUtils.removeEndIgnoreCase(removedStart, removeEnd);
    }

    /**
     * Removes one newline from start of a String if it's there, otherwise leave it alone.
     * A newline is "\n","\r", or "\r\n".
     *
     * @param str the String to remove a newline from start, may be null
     * @return String without start newline, {@code null} if null String input
     */
    public static String removeStartNewLine(String str) {
        if (isEmpty(str)) {
            return str;
        }
        final char ch = str.charAt(0);
        if (ch == CharUtils.CR || ch == CharUtils.LF) {
            return str.substring(1);
        }
        return str;
    }

    /**
     * Removes one newline from start and end of a String if it's there, otherwise leave it alone.
     * A newline is "\n","\r", or "\r\n".
     *
     * @param str the String to remove newline from start and end, may be null
     * @return String without start and end newline, {@code null} if null String input
     */
    public static String removeStartAndEndNewLine(String str) {
        return chomp(removeStartNewLine(str));
    }

    public static String removeNewLineAndCarriageReturn(String str)
    {
        String result = str;

        if (isNotEmpty(str))
        {
            result = str.replaceAll("\\r\\n|\\r|\\n", "");
        }

        return result;
    }

    /**
     * Appends the string and prefix with separator if already defined. Using
     * default separator ","
     *
     * @param dest
     * @param appendStr
     * @return
     */

    public static String append(String dest, String appendStr)
    {
        return append(dest, appendStr, ",");
    } // append

    /**
     * Appends the string and prefix with separator if already defined.
     *
     * @param dest
     * @param appendStr
     * @param separator
     * @return
     */

    public static String append(String dest, String appendStr, String separator)
    {
        String result = "";

        if (dest == null)
        {
            result = appendStr;
        }
        else
        {
            if (StringUtils.isNotBlank(dest))
            {
                result = dest + separator + appendStr;

            }
            else
            {
                result = appendStr;
            }
        }

        return result;
    } // append
    
    public static boolean isInteger(String s) {
        try { 
            Integer.parseInt(s); 
        } catch(NumberFormatException e) { 
            return false; 
        }
        // only got here if we didn't return false
        return true;
    }
    
    public static String encodeMapForUI(Map<String, String> paramsMap)
    {
        String result = "";
        
        if(paramsMap != null && paramsMap.size() > 0)
        {
            try
            {
                StringBuilder sb = new StringBuilder();
                Iterator<String> it = paramsMap.keySet().iterator();
                while (it.hasNext())
                {
                    String key = it.next();
                    String value = paramsMap.get(key);

                    String nameValue = key + "=" + value;
                    nameValue = java.net.URLEncoder.encode(nameValue, "UTF-8");
                    nameValue = nameValue.replace("+", "%20");//this is for space

                    //                    key = java.net.URLEncoder.encode(key, "UTF-8");
                    //                    value = java.net.URLEncoder.encode(value, "UTF-8");

                    //                    sb.append(key).append("=").append(value).append("&");
                    sb.append(nameValue).append("&");
                }//end of while

                sb.setLength(sb.length() - 1); //remove the last '&'

                //encode
                result = java.net.URLEncoder.encode(sb.toString(), "UTF-8");

                //escape HTML
                result = org.apache.commons.lang.StringEscapeUtils.escapeHtml(result);                
            }
            catch (Exception e)
            {
                Log.log.debug(e.getMessage(), e);
            }
        }
        
        return result;
        
    }
    
    /**
     * This method extracts all the digits from a string.
     * example: 1.2.3.a.b.c.4 will return 1234
     * 
     * @param seed
     * @return
     */
    public static String getDigitsX(String seed)
    {
        String result = null;
        if(isNotBlank(seed))
        {
            result = CharMatcher.digit().retainFrom(seed.toString());
        }
        return result;
    }
    
    /**
     * This method creates flattened map from JSON object.
     * 
     * Keys are element names
     * Values are string or string representation of the referenced JSONObject/JSONArray
     * 
     * @param   jobj JSON object
     * @return  Map&lt;String, String&gt;
     */
    @SuppressWarnings("unchecked")
    public static Map<String, String> jsonObjectToMap(JSONObject jobj)
    {
        Map<String, String> jsonMap = new HashMap<String, String>();
        
        try
        {
            if (jobj != null)
            {
                Set<String> jobjKeys = jobj.keySet();
                
                for(String jobjKey : jobjKeys)
                {
                    jsonMap.put(jobjKey, jobj.getString(jobjKey));
                }
            }
        }
        catch (RuntimeException e)
        {
            Log.log.error("Error in converting JSONObject to Map<String, String>: " + e.getMessage(), e);
            throw e;
        }
        
        return jsonMap;
    }

    /**
     * This method creates list of maps from JSON array object.
     * 
     * Maps are falttened JSONObjects in JSONArray
     * 
     * @param   jsonArray JSON array object
     * @return  List&lt;Map&lt;String, String&gt;&gt;
     */
    public static List<Map<String, String>> jsonArrayToList(JSONArray jsonArray)
    {
        List<Map<String, String>> jsonList = new ArrayList<Map<String, String>>();
        
        try
        {
            if (jsonArray != null && !jsonArray.isEmpty())
            {
                for(int i = 0; i < jsonArray.size(); i++)
                {
                    jsonList.add(jsonObjectToMap(jsonArray.getJSONObject(i)));
                }
            }
        }
        catch (RuntimeException e)
        {
            Log.log.error("Error in converting JSONArray to List<Map<String, String>>: " + e.getMessage(), e);
            throw e;
        }
        
        return jsonList;
    }
    
    /**
     * This method creates flattened map from string containing JSON object
     * associated with specified element name.
     * 
     * Keys are element names
     * Values are string or string representation of the referenced JSONObject/JSONArray
     *
     * @param   elementName Name of the element
     * @param   jsonString String containing name and string representation of associated JSONObject
     * @return  Map&lt;String, String&gt;
     */
    public static Map<String, String> jsonObjectStringToMap(String elementName, String jsonString)
    {
        JSONObject rootJObj = StringUtils.stringToJSONObject(jsonString);
        
        if(!rootJObj.has(elementName))
        {
            Map<String, String> emptyMap = Collections.emptyMap();
            return emptyMap;
        }
        
        JSONObject jobj = rootJObj.getJSONObject(elementName);
        
        if(jobj.isArray() || jobj.isEmpty() || jobj.isNullObject())
        {
            Log.log.error("JSONObject associated with " + elementName + " is JSONArray or is null or is empty");
            throw new RuntimeException("JSONObject assocaited with " + elementName + " is JSONArray or is null or is empty");
        }
        
        return StringUtils.jsonObjectToMap(jobj);
    }
    
    /**
     * This method creates list of maps from string containing JSON object array
     * associated with specified element name.
     * 
     * Maps are falttened JSONObjects in JSONArray
     *
     * @param   elementName Name of the element
     * @param   jsonString String conaining name and string representation of associated JSONArray
     * @return  List&lt;Map&lt;String, String&gt;&gt;
     */
    public static List<Map<String, String>> jsonArrayStringToList(String elementName, String jsonString)
    {
        JSONObject rootJObj = StringUtils.stringToJSONObject(jsonString);
        
        if(!rootJObj.has(elementName))
        {
            List<Map<String, String>> emptyList = Collections.emptyList();
            return emptyList;
        }
        
        JSONArray jarray = StringUtils.stringToJSONObject(jsonString).getJSONArray(elementName);
        
        if(jarray.isEmpty())
        {
            Log.log.error("JSONArray associated with " + elementName + " is empty");
          //  throw new RuntimeException("JSONArray assocaited with " + elementName + " is empty");
        }
        
        return StringUtils.jsonArrayToList(jarray);
    }
    
    /**
     * This method creates list from JSON array that comes as a String.
     * 
     * @param   json array in String form as ["foo", "bar"]
     * @return  List&lt;String&gt;
     */
    public static List<String> jsonArrayToList(String jsonString)
    {
        List<String> result = new ArrayList<String>();
        
        try
        {
            JSONArray jsonArray = (JSONArray) JSONSerializer.toJSON(jsonString);
            for(int i = 0; i < jsonArray.size(); i++)
            {
                result.add(jsonArray.get(i).toString());
            }
        }
        catch (RuntimeException e)
        {
            Log.log.error("JSONObject cannot be converted: " + e.getMessage(), e);
            throw e;
        }
        return result;
    }
    
    /**
     * This method converts JSON object string to Map
     * 
     * Keys are element names
     * Values are string or map of key value pairs.
     * JSONArrays are created as value objects with index as string key.
     *
     * @param jsonString    String representation of JSONObject
     * @param key           Key
     * @param jObjMap       Map to store converted JSON object string
     */
    public static void jsonObjectStringToMap(String jsonString, String key, Map<String, Object> jObjMap)
    {
        //try to convert, if exception while converting we reached leaf
        JSON jSon = null;
        try
        {
            jSon = JSONSerializer.toJSON(jsonString);
        }
        catch (JSONException jsone)
        {
            if (StringUtils.isNotBlank(jsonString))
            {
                jObjMap.put(key, jsonString);
            }
            return;
        }
        
        if (jSon == null || JSONNull.getInstance().equals(jSon) || jSon.isEmpty())
        {
            return;
        }
                
        //Check if array
        
        if (jSon.isArray() && !((JSONArray)jSon).isEmpty())
        {
            for (int i = 0; i < ((JSONArray)jSon).size(); i++)
            {
                Object value = ((JSONArray)jSon).get(i);
                
                if (value != null )
                {
                    Map<String, Object> JObjMapNew = jsonObjectStringToMapNewMapCheck(jObjMap, Integer.toString(i), value);
                    
                    jsonObjectStringToMap(value.toString(), Integer.toString(i), JObjMapNew);
                }
            }
            
            return;
        }
        
        //It is JSONObject
        
        JSONObject jSONObj = (JSONObject)jSon;
        
        for (Object jSONObjKey : jSONObj.keySet())
        {
            Object value = jSONObj.get(jSONObjKey);
            
            if (value != null )
            {
                Map<String, Object> JObjMapNew = jsonObjectStringToMapNewMapCheck(jObjMap, (String)jSONObjKey, value);
                
                jsonObjectStringToMap(value.toString(), (String)jSONObjKey, JObjMapNew);
            }
        }
    }
    
    private static Map<String, Object> jsonObjectStringToMapNewMapCheck(Map<String, Object> oldJObjMap, String key, Object value)
    {
        Map<String, Object> JObjMapNew = oldJObjMap;
        
        if (value instanceof JSONObject || value instanceof JSONArray)
        {
            JObjMapNew = new HashMap<String, Object>();
            oldJObjMap.put(key, JObjMapNew);
        }
        
        return JObjMapNew;
    }
    
    public static String pairedMapToUrl(Map<String, String> map, String keyPrefix, String valPrefix)
    {
        StringBuilder result = new StringBuilder("");

        try
        {
            if (map != null)
            {
                for (String key : map.keySet())
                {
                    String value = map.get(key);
                    String encodedValue = "";
                    
                    if (value != null)
                    {
                        encodedValue = java.net.URLEncoder.encode(value.toString(), "UTF-8");
                    }
                    String encodedKey = java.net.URLEncoder.encode(key, "UTF-8");
                    
                    result.append(keyPrefix).append("=").append(encodedKey).append("&").
                    append(valPrefix).append("=").append(encodedValue).append("&");
                }
                //removing the last "&"
                result.setLength(result.length() - 1);
            }
        }
        catch (Exception e)
        {
            Log.log.debug(e.getMessage(), e);
        }
        return result.toString();
    } // pairedMapToUrl

    public static String listToUrl(List<String> list, String key)
    {
        StringBuilder result = new StringBuilder("");

        try
        {
            if (list != null)
            {
                String encodedKey = "";
                
                if (key != null)
                {
                    encodedKey = java.net.URLEncoder.encode(key, "UTF-8");
                }
                
                for (String value : list)
                {
                    String encodedValue = "";
                    
                    if (value != null)
                    {
                        encodedValue = java.net.URLEncoder.encode(value.toString(), "UTF-8");
                    }
                    
                    result.append(encodedKey).append("=").append(encodedValue).append("&");
                }
                //removing the last "&"
                result.setLength(result.length() - 1);
            }
        }
        catch (Exception e)
        {
            Log.log.debug(e.getMessage(), e);
        }
        return result.toString();
    } // listToUrl
    
    public static String collectionOfObjectsToString(Collection<? extends Object> collection)
    {
        return collectionOfObjectsToString(collection, FIELDSEPARATOR);
    } // collectionOfObjectsToString
    
    public static String collectionOfObjectsToString(Collection<? extends Object> collection, String separator)
    {
    	StringBuilder result = new StringBuilder("");

        if (collection.isEmpty())
            return result.toString();
        
        for (Object collectionElement : collection)
        {
            result.append(collectionElement).append(separator);
        }

        // remove last separator
        
        return StringUtils.substringBeforeLast(result.toString(), separator);
    }
    
    public static String mapToString(Map map, boolean recursive)
    {
        return mapToString(map, VALUESEPARATOR, FIELDSEPARATOR, recursive);
    } // mapToString
    
    public static String mapToString(Map map, String valueSeparator, String fieldSeparator, boolean recursive)
    {
        StringBuilder result = new StringBuilder(recursive ? "{" : "");

        if (map != null)
        {
            for (Iterator i = map.entrySet().iterator(); i.hasNext();)
            {
                Map.Entry entry = (Map.Entry) i.next();
                String key = (String) entry.getKey();
                Object value = entry.getValue();
                if (value == null)
                {
                    value = "";
                }

                // encode value
                // value = java.net.URLEncoder.encode(value, "UTF-8");
                
                result.append(key).append(valueSeparator)
                .append(value.getClass().isArray() ? 
                		Arrays.toString((Object[])value) : 
                		((value instanceof Map && recursive) ? 
                		 mapToString((Map)value, valueSeparator, fieldSeparator, recursive) : value));
                
                if (i.hasNext())
                {
                    result.append(fieldSeparator);
                }
            }
        }

        result.append(recursive ? "}" : "");
        
        return result.toString();
    } // mapToString

    public static String blankVals(String line, String blankValues, String delimiter)
    {
        final String stars = "*****";

        String result = line;
        if (result != null && blankValues != null)
        {
            String[] values = blankValues.split(delimiter);
            for (int i = 0; i < values.length; i++)
            {
                String value = values[i].trim();
                if (!StringUtils.isEmpty(value))
                {
                    result = result.replace(value, stars);
                }
            }
        }

        return result;
    } // blankVals 

} // StringUtils
