/******************************************************************************
* (C) Copyright 2014
*
* Resolve Systems, LLC
*
* All rights reserved
* This software is distributed under license and may not be used, copied,
* modified, or distributed without the express written permission of
* Resolve Systems, LLC.
*
******************************************************************************/

package com.resolve.util;

public class ResultCode
{
    public boolean isException = false;

    public boolean isException()
    {
        return isException;
    }

    public void setException(boolean isException)
    {
        this.isException = isException;
    }

} // ResultCode
