/******************************************************************************
* (C) Copyright 2014
*
* Resolve Systems, LLC
*
* All rights reserved
* This software is distributed under license and may not be used, copied,
* modified, or distributed without the express written permission of
* Resolve Systems, LLC.
*
******************************************************************************/

package com.resolve.util;

import java.io.File;
import java.util.Enumeration;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.apache.commons.io.FileUtils;
import org.apache.log4j.Appender;
import org.apache.log4j.Priority;
import org.apache.log4j.spi.Filter;
import org.apache.log4j.spi.LocationInfo;
import org.apache.log4j.spi.LoggingEvent;

public class LogFilter extends Filter
{
    final static ConcurrentHashMap classRemap = new ConcurrentHashMap();
    final static ConcurrentHashMap fileRemap = new ConcurrentHashMap();
    final static ConcurrentHashMap msgRemap = new ConcurrentHashMap();

    static File remapFile;

    public static void init(String remapFilename)
    {
        boolean hasCondition = false;

        try
        {
            // set mapping filename
            remapFile = new File(remapFilename);

            // load mapping file
            hasCondition = load();

            // add filter to appenders
            if (hasCondition)
            {
                // Enumeration<Appender> appenders =
                // Logger.getRootLogger().getAllAppenders();
                Enumeration<Appender> appenders = Log.log.getAllAppenders();
                Enumeration<Appender> rootAppenders = Log.log.getRootLogger().getAllAppenders();
                while (appenders.hasMoreElements())
                {
                    Appender appender = appenders.nextElement();
                    appender.addFilter(new LogFilter());
                }
                while (rootAppenders.hasMoreElements())
                {
                    Appender appender = rootAppenders.nextElement();
                    appender.addFilter(new LogFilter());
                }
            }
        }
        catch (Exception e)
        {
            System.out.println("Unable to initialize LOG remapping file");
        }

    } // init

    public static boolean load()
    {
        boolean result = false;

        try
        {
            if (remapFile.exists() == true)
            {
                List<String> lines = FileUtils.readLines(remapFile, "UTF-8");

                // clear
                classRemap.clear();
                fileRemap.clear();
                msgRemap.clear();

                // get severity and regex
                for (String line : lines)
                {
                    if (line.charAt(0) != '#')
                    {
                        int pos = line.indexOf(' ');
                        String severity = line.substring(0, pos);
                        String typeValue = line.substring(pos + 1);

                        pos = typeValue.indexOf(' ');
                        String type = typeValue.substring(0, pos);
                        String value = typeValue.substring(pos + 1);

                        if (type.equalsIgnoreCase("CLASS"))
                        {
                            classRemap.put(value, severity);
                            result = true;
                        }
                        else if (type.equalsIgnoreCase("FILE"))
                        {
                            fileRemap.put(value, severity);
                            result = true;
                        }
                        else if (type.equalsIgnoreCase("MSG"))
                        {
                            msgRemap.put(Pattern.compile(value), severity);
                            result = true;
                        }
                    }
                }
            }
        }
        catch (Exception e)
        {
            System.out.println("Fail to load log mapping file");
        }

        return result;
    } // load

    public int decide(LoggingEvent event)
    {
        int result = Filter.NEUTRAL;

        // class remapping
        if (classRemap.size() > 0)
        {
            LocationInfo location = event.getLocationInformation();
            String value = location.getClassName() + ":" + location.getLineNumber();

            for (Iterator i = classRemap.entrySet().iterator(); i.hasNext();)
            {
                Map.Entry entry = (Map.Entry) i.next();

                String cls = (String) entry.getKey();
                if (cls.equals(value))
                {
                    String severity = (String) entry.getValue();
                    if (severity.equalsIgnoreCase("DROP"))
                    {
                        result = Filter.DENY;
                    }
                    else if (severity.equalsIgnoreCase("ACCEPT"))
                    {
                        result = Filter.ACCEPT;
                    }
                    else
                    {
                        event.level = Priority.toPriority(severity);
                    }
                }
            }
        }

        // file remapping
        if (fileRemap.size() > 0)
        {
            LocationInfo location = event.getLocationInformation();
            String value = location.getFileName() + ":" + location.getLineNumber();

            for (Iterator i = fileRemap.entrySet().iterator(); i.hasNext();)
            {
                Map.Entry entry = (Map.Entry) i.next();

                String file = (String) entry.getKey();
                if (file.equals(value))
                {
                    String severity = (String) entry.getValue();
                    if (severity.equalsIgnoreCase("DROP"))
                    {
                        result = Filter.DENY;
                    }
                    else if (severity.equalsIgnoreCase("ACCEPT"))
                    {
                        result = Filter.ACCEPT;
                    }
                    else
                    {
                        event.level = Priority.toPriority(severity);
                    }
                }
            }
        }

        // msg remapping
        if (msgRemap.size() > 0)
        {
            String value = (String) event.getMessage();

            for (Iterator i = msgRemap.entrySet().iterator(); i.hasNext();)
            {
                Map.Entry entry = (Map.Entry) i.next();

                Pattern regex = (Pattern) entry.getKey();
                String severity = (String) entry.getValue();

                Matcher matcher = regex.matcher(value);
                if (matcher.find())
                {
                    if (severity.equalsIgnoreCase("DROP"))
                    {
                        result = Filter.DENY;
                    }
                    else if (severity.equalsIgnoreCase("ACCEPT"))
                    {
                        result = Filter.ACCEPT;
                    }
                    else
                    {
                        event.level = Priority.toPriority(severity);
                    }
                }
            }
        }

        return result;
    } // decide

} // LogFilter
