/******************************************************************************
* (C) Copyright 2014
*
* Resolve Systems, LLC
*
* All rights reserved
* This software is distributed under license and may not be used, copied,
* modified, or distributed without the express written permission of
* Resolve Systems, LLC.
*
******************************************************************************/

package com.resolve.dto;

import com.resolve.services.vo.ResolveHashMap;


/**
 * This model is to show the values in the drop down combo box
 * 
 * @author jeet.marwah
 *
 */
public class ComboBoxModel  extends ResolveHashMap//  implements Serializable, IsSerializable
{
    private static final long serialVersionUID = -5304961006980336698L;
    
    public final static String NAME = "name";
	public final static String VALUE = "value";
	
	public 	ComboBoxModel() {}
	
	public ComboBoxModel(String name, String value)
	{
		set(NAME, name);
		set(VALUE, value);
	}
	
	public String getName()
	{
		return (String) get(NAME);
	}
	
	public void setName(String name)
	{
	    set(NAME, name);
	}
	
	public String getValue()
	{
		return (String) this.get(VALUE);
	}
	
	public void setValue(String value)
	{
	    set(VALUE, value);
	}
	
}
