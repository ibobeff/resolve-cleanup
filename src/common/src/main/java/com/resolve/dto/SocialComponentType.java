/******************************************************************************
* (C) Copyright 2014
*
* Resolve Systems, LLC
*
* All rights reserved
* This software is distributed under license and may not be used, copied,
* modified, or distributed without the express written permission of
* Resolve Systems, LLC.
*
******************************************************************************/

package com.resolve.dto;

import java.util.ArrayList;
import java.util.List;


public enum SocialComponentType
{
	NOTIFICATION("Notification"),
	PROCESS("Process"),
	TEAMS("Teams"),
	RUNBOOKS("Runbooks"),
	DOCUMENTS("Documents"),
	ACTIONTASKS("Actiontasks"),
	USERS("Users"),
	FOLLOWS("Follows"),
	RSS("RSS"),
    FORUMS("Forums");
	
	//definition of the
	private final String tagName;
	
	private SocialComponentType(String tagName)
	{
		this.tagName = tagName;
	}
	
	public String getTagName()
	{
		return this.tagName;
	}
	
	public static List<String> getAllSectionTypes()
	{
	    List<String> list = new ArrayList<String>();

        for( SocialComponentType type : SocialComponentType.values()) {
            list.add(type.getTagName());
        }
        
        return list;
	}

	public static List<ComboBoxModel> getAllSectionTypesModel()
    {
	    List<ComboBoxModel> list = new ArrayList<ComboBoxModel>();

        for( SocialComponentType type : SocialComponentType.values()) {
            list.add(new ComboBoxModel(type.getTagName(), type.toString()));
        }
        
        return list;
    }
	
	
	public static SocialComponentType getRevisionType(String tagName)
	{
	    for( SocialComponentType type : SocialComponentType.values()) {
            if( type.toString().equalsIgnoreCase(tagName))return type;
        }
        return null;
	}
}
