/******************************************************************************
 * (C) Copyright 2014
 *
 * Resolve Systems, LLC
 *
 * All rights reserved
 * This software is distributed under license and may not be used, copied,
 * modified, or distributed without the express written permission of
 * Resolve Systems, LLC.
 *
 ******************************************************************************/

package com.resolve.service;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.nio.charset.StandardCharsets;
import java.security.InvalidKeyException;
import java.security.NoSuchAlgorithmException;
import java.security.NoSuchProviderException;
import java.security.PublicKey;
import java.security.Signature;
import java.security.SignatureException;
import java.text.SimpleDateFormat;
import java.time.Instant;
import java.time.ZoneId;
import java.time.temporal.ChronoUnit;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.Collections;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.Properties;
import java.util.Set;
import java.util.SortedSet;
import java.util.TimeZone;
import java.util.TreeSet;
import java.util.stream.Collectors;

import org.apache.commons.codec.DecoderException;
import org.apache.commons.codec.binary.Hex;
import org.codehaus.jackson.annotate.JsonIgnore;

import com.resolve.rsbase.MainBase;
import com.resolve.util.CryptUtils;
import com.resolve.util.DateUtils;
import com.resolve.util.LicenseEnum;
import com.resolve.util.LicenseInterval;
import com.resolve.util.Log;
import com.resolve.util.StringUtils;

/**
 * This class represents a Resolve License.
 *
 */
public class License implements Comparable<License>
{
	public static final String NEW_LINE = "\n";
	public static final String EQUAL = "=";
	
	public static final String LICENSE_TYPE_PREFIX = "LICENSE_TYPE_";
	public static final String VALUES_SEPARATOR = ",";
	public static final String FIELDS_SEPARATOR = "#";
		
	// Resolve's java.security.PublicKey serialized object bytes (hex) string used by V2 licenses to verify signature
	private static final String rpubksobs = 
									"aced0005737200146a6176612e73656375726974792e4b6579526570bdf94fb3889aa5430200044c000" +
									"9616c676f726974686d7400124c6a6176612f6c616e672f537472696e673b5b0007656e636f64656474" +
									"00025b424c0006666f726d617471007e00014c00047479706574001b4c6a6176612f736563757269747" +
									"92f4b657952657024547970653b7870740003445341757200025b42acf317f8060854e0020000787000" +
									"0001bb308201b73082012c06072a8648ce3804013082011f02818100fd7f53811d75122952df4a9c2ee" +
									"ce4e7f611b7523cef4400c31e3f80b6512669455d402251fb593d8d58fabfc5f5ba30f6cb9b556cd781" +
									"3b801d346ff26660b76b9950a5a49f9fe8047b1022c24fbba9d7feb7c61bf83b57e7c6a8a6150f04fb8" +
									"3f6d3c51ec3023554135a169132f675f3ae2b61d72aeff22203199dd14801c70215009760508f15230b" +
									"ccb292b982a2eb840bf0581cf502818100f7e1a085d69b3ddecbbcab5c36b857b97994afbbfa3aea82f" +
									"9574c0b3d0782675159578ebad4594fe67107108180b449167123e84c281613b7cf09328cc8a6e13c16" +
									"7a8b547c8d28e0a3ae1e2bb3a675916ea37f0bfa213562f1fb627a01243bcca4f1bea8519089a883dfe" +
									"15ae59f06928b665e807b552564014c3bfecf492a0381840002818058cc517a989ba4f7ac1d8ec7fc82" +
									"382ad277d19df055b00f92006605a72a98dd0b5a98a3f8b4e400e45937b1be5cfe8ae426d8ec9ef09e2" +
									"0d05a62fecc1e78790cea895ffb616defe56f061429920863b5652ffdf14b4953a8390fcf4059cf821f" +
									"401c5e8fda55176c54f9b16eb3895191d6d586bbdd6c529da87e57596b444d740005582e3530397e720" +
									"0196a6176612e73656375726974792e4b6579526570245479706500000000000000001200007872000e" +
									"6a6176612e6c616e672e456e756d000000000000000012000078707400065055424c4943";
	
	// Resolve's java.security.PublicKey
	private static PublicKey dspub;
		
	@JsonIgnore
	private Map<String, String> licenseKeys = new HashMap<String, String>();

	private String check;
	private String key;
	private long createDate;
	private long uploadDate;
	private long deleteDate;
	private String customer;
	// license type like STANDARD, ENTERPRISE, EVALUATION, SECURITY
	private String type = LicenseEnum.LICENSE_TYPE_EVALUATION.getKeygenName();
	private boolean isHA = false;
	private int duration;
	private Long expirationDate;
	private Map<String, String> ipAddresses = new HashMap<String, String>();
	private int cores;
	// keys are like RSCONTROL, RSVIEW
	private Map<String, Integer> maxMemory = new HashMap<String, Integer>();
	private int endUserCount;

	private boolean hasLicense = false;
	private long startTimeInMillis;
	private boolean isOldLicense = false;

	// keys like EMAIL with duration as value. Duration is number of days
	private Map<String, GatewayLicense> gateway = new HashMap<String, GatewayLicense>();

	private String rawLicense;
	
	/*
	 * License deployment environments PROD, <rest are all user defined and treated as NON PROD like Eval etc.
	 * Blueprint property specified environment must match environment license is issued for
	 */
	private String env = LicenseEnum.LICENSE_ENV_UNKNOWN.getKeygenName();
	
	// Sorted Set (by start date) of license intervals 
	private SortedSet<LicenseInterval> intervals = new TreeSet<LicenseInterval>();
	
	/*
	 * List of sources (gateways) in RBC stamped execution data which should be treated as Events form 
	 * Usage entitlement and reporting perspective
	 */
	private Set<String> eventSources = new HashSet<String>();
	
	static {
		ByteArrayInputStream bipub;
		try {
			bipub = new ByteArrayInputStream(Hex.decodeHex(rpubksobs.toCharArray()));
			ObjectInputStream oipub = new ObjectInputStream(bipub);
		    Object obj = oipub.readObject();
		    
		    dspub = (PublicKey)obj;
		} catch (DecoderException | IOException | ClassNotFoundException e) {
			Log.log.error(String.format("Error %s occurred while initializing Resolve's public key!!!", 
						  			    (StringUtils.isNotBlank(e.getMessage()) ? e.getMessage() : "")), e);
		}
	}
	
	public License(long startTimeInMillis)
	{
		this.startTimeInMillis = startTimeInMillis;
	}

	public License(String encryptedLicenseContent, long startTimeInMillis) throws Exception
	{
		this(startTimeInMillis);
		Properties licenseProperties = new Properties();
		if(StringUtils.isBlank(encryptedLicenseContent))
		{
			throw new Exception("License content is blank, provide proper content");
		}

		try
		{
			// decrypt license file
			String[] licenseParts = encryptedLicenseContent.split(LicenseEnum.SIGNATURE.getKeygenName(), 2);
			
			String licenseContent = CryptUtils.decryptLicense(licenseParts[0]);
			String licenseSignature = licenseParts.length == 2 ? licenseParts[1] : null;
			
			if (StringUtils.isBlank(licenseParts[0]) || StringUtils.isBlank(licenseContent) ||
				licenseContent.equals(licenseParts[0])) {
				throw new Exception("Invalid license.");
			}
			
			licenseProperties.load(new ByteArrayInputStream(licenseContent.getBytes()));
			
			hasLicense = true;

			//keep this as well for other usage
			rawLicense = encryptedLicenseContent;

			if (licenseProperties.containsKey(LicenseEnum.LICENSE_TYPE.getKeygenName()) &&
			    StringUtils.isNotBlank(licenseProperties.getProperty(LicenseEnum.LICENSE_TYPE.getKeygenName()))) {
				type = licenseProperties.getProperty(LicenseEnum.LICENSE_TYPE.getKeygenName());
			} else {
				Log.log.warn(String.format("License [%s] is missing " + LicenseEnum.LICENSE_TYPE.getKeygenName(), 
										   licenseContent));
				if (!licenseProperties.containsKey(LicenseEnum.CUSTOMER.getKeygenName()) || 
				    StringUtils.isBlank(licenseProperties.getProperty(LicenseEnum.CUSTOMER.getKeygenName()))) {
					Log.log.warn(String.format("License [%s] is missing " + LicenseEnum.CUSTOMER.getKeygenName(), 
							   				   licenseContent));
					type = LicenseEnum.LICENSE_TYPE_STANDARD.getKeygenName();
					Log.log.info(String.format(LicenseEnum.LICENSE_TYPE.getKeygenName() + 
											   " of license [%s] with missing " + 
											   LicenseEnum.LICENSE_TYPE.getKeygenName() + 
											   " and " + LicenseEnum.CUSTOMER.getKeygenName() +
											   " is being set to " + LicenseEnum.LICENSE_TYPE_STANDARD.getKeygenName(), 
											   licenseContent));
				} else {
					type = LicenseEnum.LICENSE_TYPE_EVALUATION.getKeygenName();
					Log.log.info(String.format(LicenseEnum.LICENSE_TYPE.getKeygenName() + 
							   				   " of license [%s] with missing " + 
							   				   LicenseEnum.LICENSE_TYPE.getKeygenName() + 
							   				   " is being set to " + LicenseEnum.LICENSE_TYPE_STANDARD.getKeygenName(), 
							   				   licenseContent));
				}
			}
			
			key = licenseProperties.getProperty(LicenseEnum.LICENSE_KEY.getKeygenName());
			
			if(StringUtils.isBlank(key) || key.toLowerCase().contains("license")) {
				String errMsg = String.format("License with %s key, containing [%s] failed to instantiate.", 
											  (StringUtils.isBlank(key) ? "blank" : key), licenseContent);
				
				Log.log.error(errMsg);
				throw new Exception(errMsg);
			}
			
			setEnv(licenseProperties.getProperty(LicenseEnum.LICENSE_ENV.getKeygenName()));
			
			// V2 licenses must be Signed and signature has to be valid
			
			if (LicenseEnum.LICENSE_TYPE_V2 == LicenseEnum.valueOf(LICENSE_TYPE_PREFIX + type)) {
				if (StringUtils.isBlank(licenseSignature)) {
					String errMsg = String.format(LicenseEnum.LICENSE_TYPE_V2.getKeygenName() + 
												  " license with key %s is missing mandatory signature!!!", key);
					Log.log.error(errMsg);
					throw new Exception(errMsg);
				}
				
				// Validate Signature
				
				String originalEncLicense = getOriginalEncLicense(licenseContent);
				
				if (!verifySignature(originalEncLicense, licenseSignature)) {
					String errMsg = String.format(LicenseEnum.LICENSE_TYPE_V2.getKeygenName() + 
												  " original license with key %s failed to verify signature!!!", key);
					Log.log.error(errMsg);
					throw new Exception(errMsg);
				}
			}
			
			if (MainBase.main != null) {
				if ((LicenseEnum.LICENSE_TYPE_V2 == 
					 LicenseEnum.valueOf(LICENSE_TYPE_PREFIX + getType().toUpperCase())) &&
					StringUtils.isNotBlank(getEnv()) &&
					!LicenseEnum.LICENSE_ENV_UNKNOWN.getKeygenName().equalsIgnoreCase(getEnv()) &&
					!getEnv().equalsIgnoreCase(MainBase.main.configGeneral.getEnv())) {
					if ((LicenseEnum.LICENSE_ENV_PRODUCTION.getKeygenName().equalsIgnoreCase(getEnv())) ||
						(LicenseEnum.LICENSE_ENV_PRODUCTION.getKeygenName().
						 equalsIgnoreCase(MainBase.main.configGeneral.getEnv()))) {
						Log.log.error(String.format("Environment licensed for (%s) and environment license is deployed on " +
													"(%s) do not match!!!", getEnv(), MainBase.main.configGeneral.getEnv()));
						throw new Exception(
										"Mismatch in environment licensed for and environment license deployed on!!!");
					} else {
						/*
						 * Ignore mismatch in environment license is issued for and environment license is deployed on,
						 * as long as none of them are PROD. 
						 */
						Log.log.warn(String.format("Environment licensed for (%s) and environment license is deployed on " +
												   "(%s) do not match!!!", getEnv(), MainBase.main.configGeneral.getEnv()));
					}
				}
			}
			
			check = licenseProperties.getProperty(LicenseEnum.CHECK.getKeygenName());
			customer = licenseProperties.getProperty(LicenseEnum.CUSTOMER.getKeygenName());			
			uploadDate = startTimeInMillis;
			
			if(StringUtils.isBlank(key))
			{
				isOldLicense = true;
				key = "license"; //actually no key
				type = LicenseEnum.LICENSE_TYPE_ENTERPRISE.getKeygenName();
			}
			licenseKeys.put(key, key);
			
			if (licenseProperties.containsKey(LicenseEnum.CREATE_DATE.getKeygenName()))
			{
				createDate = Long.parseLong(licenseProperties.getProperty(LicenseEnum.CREATE_DATE.getKeygenName()));
			}

			if (licenseProperties.containsKey(LicenseEnum.UPLOAD_DATE.getKeygenName()))
			{
				Log.log.debug( String.format("%s, Upload date:%s", key, 
											 licenseProperties.getProperty(LicenseEnum.UPLOAD_DATE.getKeygenName())));
				uploadDate = Long.parseLong(licenseProperties.getProperty(LicenseEnum.UPLOAD_DATE.getKeygenName()));
			}
			
			if (licenseProperties.containsKey(LicenseEnum.DELETE_DATE.getKeygenName()))
			{
				deleteDate = Long.parseLong(licenseProperties.getProperty(LicenseEnum.DELETE_DATE.getKeygenName()));
			}
			
			if (StringUtils.isNotBlank(licenseProperties.getProperty(LicenseEnum.NO_OF_CORE.getKeygenName())))
			{
				cores = Integer.parseInt(licenseProperties.getProperty(LicenseEnum.NO_OF_CORE.getKeygenName()));
			}
			
			if (StringUtils.isNotBlank(licenseProperties.getProperty(LicenseEnum.END_USER_COUNT.getKeygenName())))
			{
				endUserCount = Integer.parseInt(licenseProperties.getProperty(LicenseEnum.END_USER_COUNT.getKeygenName()));
			}
			
			String ipAddresses1 = licenseProperties.getProperty(LicenseEnum.IP_ADDRESS.getKeygenName());
			if (StringUtils.isNotBlank(ipAddresses1))
			{
				String[] ipAddresses2 = ipAddresses1.split(VALUES_SEPARATOR);
				for (String ipAddress : ipAddresses2)
				{
					if (StringUtils.isNotBlank(ipAddress))
					{
						ipAddresses.put(ipAddress.trim(), ipAddress.trim());
					}
				}
			}
			
			if (StringUtils.isNotBlank(licenseProperties.getProperty(LicenseEnum.HA.getKeygenName())))
			{
				isHA = Boolean.parseBoolean(licenseProperties.getProperty(LicenseEnum.HA.getKeygenName()));
			}
			
			if(isOldLicense)
			{
				isHA = true;
			}
			
			if (StringUtils.isNotBlank(licenseProperties.getProperty(LicenseEnum.DURATION.getKeygenName())))
			{
				duration = Integer.parseInt(licenseProperties.getProperty(LicenseEnum.DURATION.getKeygenName()));
			}
			
			if (StringUtils.isNotBlank(licenseProperties.getProperty(LicenseEnum.EXPIRATION_DATE.getKeygenName()))) {
				expirationDate = DateUtils.convertStringToTimesInMillis(
												licenseProperties.getProperty(LicenseEnum.EXPIRATION_DATE.getKeygenName()) +
												LicenseInterval.LICENSE_INTERVAL_START_SUFFIX, 
												LicenseEnum.DATE_FORMAT_MILLISECONDS_ZONE.getKeygenName());
			} else {
				// For Non V2 license blank expiration date means perpetual
				if (LicenseEnum.LICENSE_TYPE_V2 != LicenseEnum.valueOf(LICENSE_TYPE_PREFIX + type)) {
					expirationDate = LicenseInterval.INTERVAL_END_PERPETUAL_MILLISECONDS;
				}
			}
			
			maxMemory.put(LicenseEnum.COMPONENT_RSCONTROL.getKeygenName(), 0);
			if (StringUtils.isNotBlank(licenseProperties.getProperty(LicenseEnum.MAX_MEMORY_RSCONTROL.getKeygenName())))
			{
				maxMemory.put(LicenseEnum.COMPONENT_RSCONTROL.getKeygenName(), 
							  Integer.parseInt(licenseProperties.getProperty(
									  								LicenseEnum.MAX_MEMORY_RSCONTROL.getKeygenName())));
			}

			maxMemory.put(LicenseEnum.COMPONENT_RSVIEW.getKeygenName(), 0);
			if (StringUtils.isNotBlank(licenseProperties.getProperty(LicenseEnum.MAX_MEMORY_RSVIEW.getKeygenName())))
			{
				maxMemory.put(LicenseEnum.COMPONENT_RSVIEW.getKeygenName(), 
							  Integer.parseInt(licenseProperties.getProperty(
									  								LicenseEnum.MAX_MEMORY_RSVIEW.getKeygenName())));
			}

			// configured as EMAIL#1#30, REMEDYX#1#90, or *#0#0 to give all access
			String gateways1 = licenseProperties.getProperty(LicenseEnum.GATEWAY.getKeygenName());

			//old licenses did not used to have any gateway restriction
			//so we allow all the gateways to be running.
			if(isOldLicense)
			{
				gateways1 = "*#0#0";
			}

			if (StringUtils.isNotBlank(gateways1))
			{
				String[] gateways2 = gateways1.split(VALUES_SEPARATOR);
				for (String gateway : gateways2)
				{
					if (StringUtils.isNotBlank(gateway))
					{
						String[] gateways3 = gateway.split(FIELDS_SEPARATOR);
						if (StringUtils.isNotBlank(gateways3[0]))
						{
							String gatewayLicenseCode = gateways3[0].trim().toUpperCase();
							Integer instanceCount = 0;
							if (StringUtils.isNotBlank(gateways3[1]))
							{
								instanceCount = Integer.parseInt(gateways3[1].trim());
							}
							this.gateway.put(gatewayLicenseCode, 
											 new GatewayLicense(gatewayLicenseCode, instanceCount, getDuration(), 
													 			getExpirationDate(), uploadDate));
						}
					}
				}
			}
			
			// Process License Intervals and event sources for V2 license
			
			if (LicenseEnum.LICENSE_TYPE_V2 == LicenseEnum.valueOf(LICENSE_TYPE_PREFIX + type)) {
				intervals = processLicenseIntervals(licenseProperties);
				eventSources = processEventSources(licenseProperties);
			}
		}
		catch (Exception e)
		{
			Log.log.error(e.getMessage(), e);
			throw new Exception(e.getMessage());
		}
	}
	
	public static SortedSet<LicenseInterval> processLicenseIntervals(Properties licenseProperties) {
		SortedSet<LicenseInterval> parsedSortedLicensesIntervals = new TreeSet<LicenseInterval>();
		
		String csvLicenseIntervals = licenseProperties.getProperty(LicenseEnum.INTERVALS.getKeygenName());
		
		if (StringUtils.isBlank(csvLicenseIntervals)) {
			return parsedSortedLicensesIntervals;
		}
		
		String[] licenseIntervals = csvLicenseIntervals.split(VALUES_SEPARATOR);
		
		for (String licenseInterval : licenseIntervals) {
			String[] licenseIntervalFlds = licenseInterval.split(FIELDS_SEPARATOR, 4);
			
			Long startDate = DateUtils
							 .convertStringToTimesInMillis(licenseIntervalFlds[0].trim() + 
									 					   LicenseInterval.LICENSE_INTERVAL_START_SUFFIX,
														   LicenseEnum.DATE_FORMAT_MILLISECONDS_ZONE.getKeygenName());
			Long endDate = DateUtils
						   .convertStringToTimesInMillis(licenseIntervalFlds[1].trim() + 
														 LicenseInterval.LICENSE_INTERVAL_END_SUFFIX, 
														 LicenseEnum.DATE_FORMAT_MILLISECONDS_ZONE.getKeygenName());
			
			Long incidentCount = 0L;
			
			try {
				incidentCount = Long.valueOf(licenseIntervalFlds[2].trim());
			} catch (NumberFormatException nfe) {
				//
			}
			
			Long eventCount = 0L;
			
			try {
				eventCount = Long.valueOf(licenseIntervalFlds[3].trim());
			} catch (NumberFormatException nfe) {
				//
			}
			
			parsedSortedLicensesIntervals.add(new LicenseInterval(startDate, endDate, incidentCount, eventCount));
		}
		
		return parsedSortedLicensesIntervals;
	}
	
	public static Long getLicenseIntervalBasedExpirationDate(SortedSet<LicenseInterval> sortedLicenseIntervals) {
		Long liBasedExpirationDate = LicenseInterval.INTERVAL_END_PERPETUAL_MILLISECONDS;
		
		List<LicenseInterval> licIntrvls = new ArrayList<LicenseInterval>(sortedLicenseIntervals);
		Collections.reverse(licIntrvls);
		
		Long lastIntrvlEndDate = licIntrvls.get(0).getEndDate();
		
		if ( lastIntrvlEndDate != null && lastIntrvlEndDate >= 0 && lastIntrvlEndDate < liBasedExpirationDate) {
			liBasedExpirationDate = lastIntrvlEndDate + 1;
		}
		
		return liBasedExpirationDate;
	}
	
	public static Set<String> processEventSources(Properties licenseProperties) {
		Set<String> parsedEventSources = new HashSet<String>();
		
		String csvEventSources = licenseProperties.getProperty(LicenseEnum.EVENTSOURCES.getKeygenName());
		
		if (StringUtils.isBlank(csvEventSources)) {
			return parsedEventSources;
		}
		
		String[] licenseEventSources = csvEventSources.split(VALUES_SEPARATOR);
		
		// Lower case event source names
		parsedEventSources.addAll(
				(Collection<? extends String>) Arrays.asList(licenseEventSources).parallelStream().
								  			   map(eventSrc -> ((String)eventSrc).toLowerCase()).
								  			   collect(Collectors.toCollection(ArrayList::new)));
		
		return parsedEventSources;
	}
	
	public void reset()
	{
		key = null;
		licenseKeys = new HashMap<String, String>();
		type = null;
		customer = null;
		cores = 0;
		isHA = false;
		duration = 0;
		expirationDate = null;
		// by default this should have the community edtion stuff
		endUserCount = 0;

		maxMemory.put(LicenseEnum.COMPONENT_RSCONTROL.getKeygenName(), 0);
		maxMemory.put(LicenseEnum.COMPONENT_RSVIEW.getKeygenName(), 0);

		this.gateway = new HashMap<String, GatewayLicense>();
	}

	public String getCheck()
	{
		return check;
	}

	public void setCheck(String check)
	{
		this.check = check;
	}

	public String getKey()
	{
		return key;
	}

	public void setKey(String key)
	{
		this.key = key;
	}

	public long getCreateDate()
	{
		return createDate;
	}

	public void setCreateDate(long createDate)
	{
		this.createDate = createDate;
	}

	public long getUploadDate()
	{
		return uploadDate;
	}

	public void setUploadDate(long uploadDate)
	{
		this.uploadDate = uploadDate;
	}

	public long getDeleteDate()
	{
		return deleteDate;
	}

	public void setDeleteDate(long deleteDate)
	{
		this.deleteDate = deleteDate;
	}

	@JsonIgnore
	public boolean isDeleted()
	{
		return deleteDate > 0;
	}

	public String getCustomer()
	{
		return customer;
	}

	public void setCustomer(String customer)
	{
		this.customer = customer;
	}

	public String getType()
	{
		return type;
	}

	public void setType(String type)
	{
		//if the new type outrank old one then replace it.
		if(this.type == null || LicenseEnum.valueOf(LICENSE_TYPE_PREFIX + type).ordinal() > 
								LicenseEnum.valueOf(LICENSE_TYPE_PREFIX + this.type).ordinal())
		{
			this.type = type;
		}
	}

	public String getEnv() {
		if (!StringUtils.isBlank(env)) {
			return env;
		} else {
			return LicenseEnum.LICENSE_ENV_UNKNOWN.getKeygenName();
		}
	}
	
	public void setEnv(String env) {
		if (StringUtils.isNotBlank(env)) {
			this.env = env;
		}
	}
	
	public boolean isHA()
	{
		return isHA;
	}

	public void setHA(boolean isHA)
	{
		this.isHA = isHA;
	}

	public int getDuration()
	{
		return duration;
	}

	public void setDuration(int duration)
	{
		this.duration = duration;
	}

	@JsonIgnore
	public Long getDurationBasedExpirationDate()
	{
		Long durationBasedExpirationDate = null;
		
		if (duration > 0)
		{
			long durationInMillis = duration * 24 * 60 * 60 * 1000L;
			if(isOldLicense)
			{
				//for old licenses we count the duration from the component start date.
				durationBasedExpirationDate = getStartTimeInMillis() + durationInMillis;
			}
			else
			{
				//for new licenses we count the duration from the upload date.
				durationBasedExpirationDate = getUploadDate() + durationInMillis;
			}
		}
		return durationBasedExpirationDate;
	}

	public Long getExpirationDate()
	{
		Long result = expirationDate;
		
		// blank key indicates License object from constructor using starttimeinmillis of type EVALUATION
		if (StringUtils.isNotBlank(key)) {
			//V2 license uses expiration date only
			if (LicenseEnum.LICENSE_TYPE_V2 != LicenseEnum.valueOf(LICENSE_TYPE_PREFIX + type)) {
				// find out the expiration based on duration
				Long durationBasedExpirationDate = getDurationBasedExpirationDate();
												   
				if(durationBasedExpirationDate != null)
				{
					if (result == null && durationBasedExpirationDate != null) {
						result = durationBasedExpirationDate;
					} else {
						result = result > durationBasedExpirationDate ? result : durationBasedExpirationDate;
					}
				}
			}
		} else {
			result = startTimeInMillis;
		}

		return result;
	}

	public void setExpirationDate(Long expirationDate)
	{
		this.expirationDate = expirationDate;
	}

	public void setExpirationDateFromString(String expirationDate)
	{
		this.expirationDate = DateUtils.convertStringToTimestamp(expirationDate, 
																 new SimpleDateFormat(
																		 LicenseEnum.DATE_FORMAT.getKeygenName(), 
																		 Locale.ENGLISH)).getTime();
	}

	@JsonIgnore
	public boolean isDurationBasedExpiration()
	{
		return expirationDate == null;
	}

	public Map<String, String> getIpAddresses()
	{
		return ipAddresses;
	}

	public void setIpAddresses(Map<String, String> ipAddresses)
	{
		this.ipAddresses = ipAddresses;
	}

	public int getCores()
	{
		return cores;
	}

	public void setCores(int cores)
	{
		this.cores = cores;
	}

	public Map<String, Integer> getMaxMemory()
	{
		return maxMemory;
	}

	public void setMaxMemory(Map<String, Integer> maxMemory)
	{
		this.maxMemory = maxMemory;
	}

	public int getEndUserCount()
	{
		return endUserCount;
	}

	public void setEndUserCount(int endUserCount)
	{
		this.endUserCount = endUserCount;
	}

	public GatewayLicense getGatewayLicense(String code)
	{
		GatewayLicense result = null;
		if(gateway.containsKey("*"))
		{
			result = gateway.get("*");
		}
		else
		{
			result = gateway.get(code);
		}

		if (result == null && gateway.containsKey("ANY"))
		{
			result = gateway.get("ANY");
		}

		return result;
	}

	public void setGatewayLicense(String code, GatewayLicense gatewayLicense)
	{
		gateway.put(code, gatewayLicense);
	}

	@JsonIgnore
	public Collection<GatewayLicense> getAllGatewayLicenses()
	{
		return gateway.values();
	}

	public Map<String, GatewayLicense> getGateway()
	{
		return gateway;
	}

	public void setGateway(Map<String, GatewayLicense> gateway)
	{
		this.gateway = gateway;
	}

	@JsonIgnore
	public boolean isEvaluation()
	{
		return LicenseEnum.LICENSE_TYPE_EVALUATION.getKeygenName().equalsIgnoreCase(getType());
	}

	public boolean hasLicense()
	{
		return hasLicense;
	}

	public Map<String, String> getLicenseKeys()
	{
		return licenseKeys;
	}

	public void setLicenseKeys(Map<String, String> licenseKeys)
	{
		this.licenseKeys = licenseKeys;
	}

	public long getStartTimeInMillis()
	{
		return startTimeInMillis;
	}

	public void setStartTimeInMillis(long startDate)
	{
		this.startTimeInMillis = startDate;
		for (GatewayLicense gatewayLicense : gateway.values())
		{
			gatewayLicense.setStartTimeInMillis(startDate);
		}
	}

	@JsonIgnore
	public boolean isOldLicense()
	{
		return isOldLicense;
	}

	public void setOldLicense(boolean isOldLicense)
	{
		this.isOldLicense = isOldLicense;
	}

	public String getRawLicense()
	{
		return rawLicense;
	}

	public void setRawLicense(String rawLicense)
	{
		this.rawLicense = rawLicense;
	}
	
	public SortedSet<LicenseInterval> getIntervals() {
		return intervals;
	}

	public void setIntervals(SortedSet<LicenseInterval> intervals) {
		this.intervals = intervals;
	}
	
	public Set<String> getEventSources() {
		return eventSources;
	}
	
	public void setEventSources(Set<String> eventSources) {
		this.eventSources = eventSources;
	}
	
	/**
	 * Returns a banner generally displayed by the RSVIEW.
	 *
	 * @return
	 */
	public String getBanner()
	{
		String result = LicenseEnum.LICENSE_BANNER_DEFAULT.getKeygenName();
		if (LicenseEnum.LICENSE_TYPE_COMMUNITY.getKeygenName().equals(getType()))
		{
			result = LicenseEnum.LICENSE_BANNER_COMMUNITY.getKeygenName();
		}
		else if (LicenseEnum.LICENSE_TYPE_EVALUATION.getKeygenName().equals(getType()))
		{
			result = LicenseEnum.LICENSE_BANNER_EVALUATION.getKeygenName();
		}
		else if (LicenseEnum.LICENSE_TYPE_V2.getKeygenName().equals(getType())) {
			result = LicenseEnum.LICENSE_BANNER_V2.getKeygenName();
		}
		
		return result;
	}

	/**
	 * This method appends this license with the one passed through the
	 * parameter.
	 *
	 * @param newLicense
	 *            to be applied.
	 * @throws Exception
	 */
	public void append(License newLicense) throws Exception
	{
		//this condition makes sure that deleted and NON V2 expired licenses don't get appended
		if (!newLicense.isDeleted() && 
			((LicenseEnum.LICENSE_TYPE_V2 == 
			  LicenseEnum.valueOf(License.LICENSE_TYPE_PREFIX + newLicense.getType().toUpperCase())) ||
			 !newLicense.isExpired()))
		{			
			/*
			 * Non-V2 licenses are cumulative i.e. effective license entitlements can be combination of one or more
			 * Non-V2 licenses for example # of CPUs, gateways, and/or # of users etc. 
			 * 
			 * Cumulative Non V2 License Entitlements:
			 * 
			 * # of cores or vCPUs
			 * IP Addresses
			 * Max RSView and RSControl Memory
			 * Gateway licenses
			 * 
			 * V2 licenses are not cumulative and hence effective license entitlements will be same as V2 license
			 * entitlements with latest expiry date.
			 * 
			 * Please note V2 license being loaded could be expired.
			 */
			Log.log.info(String.format("Appending New Licenese with license key %s. New License Info [%s]", 
									   newLicense.getKey(), newLicense));		
			this.update(newLicense);
		} else {
			Log.log.info(String.format("New License with license key %s not appended. New License Info [%s]", 
									   newLicense.getKey(), newLicense));
		}
	}

	private void update(License newLicense)
	{
		/*
		 * V2 license supersedes all other types of licenses viz. EVALUATION, STANDARD, ENTERPRISE
		 *  
		 * Following effective license entitlements/attributes are set for all licenses
		 * 
		 * duration
		 * key
		 * customer
		 * type
		 * expiration date
		 * end user count
		 * env
		 * uploadDate
		 * createDate
		 * rawLicense
		 * 
		 * Following effective license entitlements are reset to V2 license values
		 * 
		 * # of cores or vCPUs
		 * IP Addresses
		 * Max RSView and RSControl Memory
		 * Gateway licenses
		 */
		
		boolean overwrite = false;
		
		/*
		 * V2 type license will always overwrite any other type 
		 * irrespective of expiration date of V2 license being earlier or
		 * old license was perpetual.
		 * 
		 * V2 type license created last will overwrite existing V2 type license 
		 * created earlier.
		 */
		
		Long cExpirationDate = getExpirationDate();
		Long nExpirationDate = newLicense.getExpirationDate();
		long cCreateDate = getCreateDate();
		long nCreateDate = newLicense.getCreateDate();
		
		SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSS'Z'");
    	sdf.setTimeZone(TimeZone.getTimeZone(ZoneId.of("UTC")));
    	
		Log.log.debug(String.format("License.update(%s): Current License Expiration Date:%s, New License Expiration Date:%s, " +
									"Current License Create Date:%s, New License Create Date:%s", newLicense, 
									sdf.format(new Date(cExpirationDate.longValue())), 
									sdf.format(new Date(nExpirationDate.longValue())),
									sdf.format(new Date(cCreateDate)),
									sdf.format(new Date(nCreateDate))));
		
		if (Instant.ofEpochMilli(newLicense.getEffectiveStartDate()).isBefore(Instant.now())) {
			if (LicenseEnum.LICENSE_TYPE_V2 != LicenseEnum.valueOf(LICENSE_TYPE_PREFIX + type) &&
				LicenseEnum.valueOf(LICENSE_TYPE_PREFIX + type).ordinal() <= 
				LicenseEnum.valueOf(LICENSE_TYPE_PREFIX + newLicense.getType()).ordinal() &&
				(cExpirationDate == null || (cExpirationDate == null && nExpirationDate == null) || 
				 (cExpirationDate != null && nExpirationDate == null) ||
				 (cExpirationDate != null && nExpirationDate != null && 
				  ((cExpirationDate <= nExpirationDate) || 
				   LicenseEnum.LICENSE_TYPE_V2 == LicenseEnum.valueOf(LICENSE_TYPE_PREFIX + newLicense.getType()))))) {
				overwrite = true; 
			} else if ((LicenseEnum.LICENSE_TYPE_V2 == LicenseEnum.valueOf(LICENSE_TYPE_PREFIX + newLicense.getType())) &&
					   (cCreateDate <= nCreateDate)) {
				overwrite = true;
			}
		}
		
		if (overwrite) {
			//earliest license upload time is the start date
			if(newLicense.getUploadDate() > 0 && getStartTimeInMillis() > newLicense.getUploadDate())
			{
				Log.log.info(String.format("Updating Entitlements Start Date from %s to %s.",
						    			   DateUtils.convertTimeInMillisToString(
						    					   						getStartTimeInMillis(),
		   				   										  		LicenseEnum.DATE_FORMAT_LONG.getKeygenName()),
						    			   DateUtils.convertTimeInMillisToString(
						    					   						newLicense.getUploadDate(),
						    					   						LicenseEnum.DATE_FORMAT_LONG.getKeygenName())));
				setStartTimeInMillis(newLicense.getUploadDate());
			}
			
			check = newLicense.getCheck();
			hasLicense = true;
			isOldLicense = newLicense.isOldLicense;
			
			// Reset the license keys map if transitioning from Non-V2 to V2 licenses HP TODO after ordering license loading
			
			if (LicenseEnum.LICENSE_TYPE_V2 != LicenseEnum.valueOf(LICENSE_TYPE_PREFIX + type) &&
				LicenseEnum.LICENSE_TYPE_V2 == LicenseEnum.valueOf(LICENSE_TYPE_PREFIX + newLicense.getType())) {
				licenseKeys.clear();
			}
			
			licenseKeys.put(newLicense.getKey(), "");
			
			if (LicenseEnum.valueOf(LICENSE_TYPE_PREFIX + type) != 
				LicenseEnum.valueOf(LICENSE_TYPE_PREFIX + newLicense.getType())) {
				String oldType = type;
				type = newLicense.getType();
				Log.log.info(String.format("Updated license type from %s to %s.", oldType, type));
			}
			
			if (duration != newLicense.getDuration()) {
				int oldDuration = duration;
				duration = newLicense.getDuration();
				Log.log.info(String.format("Updated license duration from %d to %d.", oldDuration, duration));
			}
			
			if ((StringUtils.isBlank(customer) && StringUtils.isNotBlank(newLicense.getCustomer())) || 
				(StringUtils.isNotBlank(customer) && StringUtils.isNotBlank(newLicense.getCustomer()) &&
				 !customer.equals(newLicense.getCustomer()))) {
				String oldCustomer = customer;
				customer = newLicense.getCustomer();
				Log.log.info(String.format("Updated license customer from %s to %s.", 
										   (StringUtils.isNotBlank(oldCustomer) ? oldCustomer : ""), customer));
			}
			
			if ((StringUtils.isBlank(key) && StringUtils.isNotBlank(newLicense.getKey())) || 
				(StringUtils.isNotBlank(key) && StringUtils.isNotBlank(newLicense.getKey()) && 
				 !key.equals(newLicense.getKey()))) {
				String oldKey = key;
				key = newLicense.getKey();
				Log.log.info(String.format("Updated license key from %s to %s.", 
										   (StringUtils.isNotBlank(oldKey) ? oldKey : ""), key));
			}
			
			if ((cExpirationDate == null && nExpirationDate != null) ||
				(cExpirationDate != null && nExpirationDate == null) ||
				(cExpirationDate != nExpirationDate)) {
				Long oldExpirationDate = cExpirationDate;
				expirationDate = newLicense.getExpirationDate();
				cExpirationDate = expirationDate;
				Log.log.info(String.format("Updated Expiration Date of license from %s to %s.",
										   (oldExpirationDate != null  && oldExpirationDate.longValue() > 0l ? 
											DateUtils.convertTimeInMillisToString(
																	oldExpirationDate, 
	 					   				   							LicenseEnum.DATE_FORMAT_LONG.getKeygenName()):
	 					   				   	"perpetual"),
										   (cExpirationDate != null && cExpirationDate.longValue() > 0l ?
											DateUtils.convertTimeInMillisToString(
																	cExpirationDate, 
			 					   				   					LicenseEnum.DATE_FORMAT_LONG.getKeygenName()):
			 					   			"perpetual")));
			}
			
			if (!getEnv().equals(newLicense.getEnv())) {
				String oldEnv = getEnv();
				setEnv(newLicense.getEnv());
				Log.log.info(String.format("Updated license environment from %s to %s.", oldEnv, env));
			}
			
			long oldUploadDate = uploadDate;
			uploadDate = newLicense.getUploadDate();
			Log.log.info(String.format("Updated license upload date from %s to %s.", 
									   DateUtils.convertTimeInMillisToString(oldUploadDate, 
											   								 LicenseEnum.DATE_FORMAT_LONG.getKeygenName()),
									   DateUtils.convertTimeInMillisToString(uploadDate, 
				   															 LicenseEnum.DATE_FORMAT_LONG.getKeygenName())));
			
			long oldCreateDate = createDate;
			createDate = newLicense.getCreateDate();
			Log.log.info(String.format("Updated license create date from %s to %s.", 
					   					DateUtils.convertTimeInMillisToString(oldCreateDate, 
							   												  LicenseEnum.DATE_FORMAT_LONG.getKeygenName()),
					   					DateUtils.convertTimeInMillisToString(
					   														createDate, 
																			LicenseEnum.DATE_FORMAT_LONG.getKeygenName())));
			
			String oldRawLicense = rawLicense;
			rawLicense = newLicense.getRawLicense();
			Log.log.info(String.format("Updated raw license from%n[%s]%nto%n[%s].",
					   				   (StringUtils.isNotBlank(oldRawLicense) ? oldRawLicense : ""),
					   				   (StringUtils.isNotBlank(rawLicense) ? rawLicense : "")));
			
			int oldIpAddressesSize = ipAddresses.size();
			int oldCores = cores;
			int oldEndUserCount = endUserCount;
			boolean isOldHA = isHA;
			Integer maxMemoryRSCONTROL = maxMemory.containsKey(LicenseEnum.COMPONENT_RSCONTROL.getKeygenName()) ?
					 					 maxMemory.get(LicenseEnum.COMPONENT_RSCONTROL.getKeygenName()) : -1;
			Integer maxMemoryRSVIEW = maxMemory.containsKey(LicenseEnum.COMPONENT_RSVIEW.getKeygenName()) ?
									  maxMemory.get(LicenseEnum.COMPONENT_RSVIEW.getKeygenName()) : -1;
			
			if (LicenseEnum.LICENSE_TYPE_V2 == LicenseEnum.valueOf(LICENSE_TYPE_PREFIX + type)) {
				// Set IP addresses
				ipAddresses.clear();
				ipAddresses.putAll(newLicense.getIpAddresses());
				Log.log.info(String.format("Reset size of IP Addresses from %d to %d.", oldIpAddressesSize, 
						   				   ipAddresses.size()));
				// Set cores or vCPUs
				cores = newLicense.getCores();
				Log.log.info(String.format("Reset size of vCPUs from %d to %d.", oldCores, cores));
				// Set end user count
				endUserCount = newLicense.getEndUserCount();
				Log.log.info(String.format("Reset End User Count from %d to %d.", oldEndUserCount, endUserCount));
				isHA = newLicense.isHA();
				Log.log.info(String.format("Reset is HA from %b to %b.", isOldHA, isHA));
				
				maxMemory.put(LicenseEnum.COMPONENT_RSCONTROL.getKeygenName(), 
							  newLicense.getMaxMemory().get(LicenseEnum.COMPONENT_RSCONTROL.getKeygenName()));
				Log.log.info(String.format("Reset Max RSControl Memory from %d to %d.", maxMemoryRSCONTROL, 
										   maxMemory.get(LicenseEnum.COMPONENT_RSCONTROL.getKeygenName())));
				
				maxMemory.put(LicenseEnum.COMPONENT_RSVIEW.getKeygenName(), 
							  newLicense.getMaxMemory().get(LicenseEnum.COMPONENT_RSVIEW.getKeygenName()));
				Log.log.info(String.format("Reset Max RSView Memory from %d to %d.", maxMemoryRSVIEW, 
						   				   maxMemory.get(LicenseEnum.COMPONENT_RSVIEW.getKeygenName())));
				
				//Set Gateways
				setGateway(newLicense.getGateway());
				// Set Usage Intervals
				setIntervals(newLicense.getIntervals());
				// Set Event Sources
				eventSources.clear();
				setEventSources(newLicense.getEventSources());
			} else {
				// Accumulate IP addresses				
				ipAddresses.putAll(newLicense.getIpAddresses());
				Log.log.info(String.format("Updated size of IP Addresses from %d to %d.", oldIpAddressesSize, 
										   this.ipAddresses.size()));
				// Accumulate cores or vCPUs
				cores += newLicense.getCores();
				Log.log.info(String.format("Updated size of vCPUs from %d to %d.", oldCores, cores));
				// Accumulate end user count
				endUserCount += newLicense.getEndUserCount();
				Log.log.info(String.format("Updated End User Count from %d to %d.", oldEndUserCount, endUserCount));
				// Accumulate/OR is HA
				isHA = isHA || newLicense.isHA();
				Log.log.info(String.format("Updated is HA from %b to %b.", isOldHA, isHA));
				
				Integer newMaxMemoryRSCONTROL = 
											newLicense.getMaxMemory().get(LicenseEnum.COMPONENT_RSCONTROL.getKeygenName());
				
				if(newMaxMemoryRSCONTROL == 0 || isEvaluation() || 
				   (!isEvaluation() && newMaxMemoryRSCONTROL > maxMemoryRSCONTROL)) {
					maxMemory.put(LicenseEnum.COMPONENT_RSCONTROL.getKeygenName(), newMaxMemoryRSCONTROL);
					Log.log.info(String.format("Updated Max RSControl Memory from %d to %d.", maxMemoryRSCONTROL, 
											   newMaxMemoryRSCONTROL));
				}
				
				Integer newMaxMemoryRSVIEW = newLicense.getMaxMemory().get(LicenseEnum.COMPONENT_RSVIEW.getKeygenName());
				
				if(newMaxMemoryRSVIEW == 0 || isEvaluation() || (!isEvaluation() && newMaxMemoryRSVIEW > maxMemoryRSVIEW)) {
					maxMemory.put(LicenseEnum.COMPONENT_RSVIEW.getKeygenName(), newMaxMemoryRSVIEW);
					Log.log.info(String.format("Updated Max RSView Memory from %d to %d.", maxMemoryRSVIEW, 
											   newMaxMemoryRSVIEW));
				}
				
				// append new gateway(s)/update existing gateway instance count and or days left 
				for (String key : newLicense.getGateway().keySet())
				{
					GatewayLicense gatewayLicense = gateway.get(key);
					GatewayLicense newGatewayLicense = newLicense.getGatewayLicense(key);
					if(gatewayLicense == null ||
							gatewayLicense.getDaysLeft() <= newGatewayLicense.getDaysLeft())
					{
						if (gatewayLicense != null && gatewayLicense.getDaysLeft() == newGatewayLicense.getDaysLeft())
						{
							if (newGatewayLicense.getInstanceCount() != 0)
							{
								int newLicenseCount = gatewayLicense.getInstanceCount() == 0 ?
													  gatewayLicense.getInstanceCount() :
													  newGatewayLicense.getInstanceCount() + 
													  gatewayLicense.getInstanceCount();

								newGatewayLicense.setInstanceCount(newLicenseCount);
							}
						}
						gatewayLicense = newGatewayLicense;
					}

					gateway.put(key, gatewayLicense);
				}
				
				// Clear event sources
				eventSources.clear();
			}
		}
	}

	public long getDaysLeft()
	{
		Long cExpirationDate = getExpirationDate();
		
		if (cExpirationDate == null || cExpirationDate.longValue() <= 0) {
			return Long.MAX_VALUE;
		} else {
			return ChronoUnit.DAYS.between(Instant.now(), Instant.ofEpochMilli(cExpirationDate));
		}		
	}

	/**
	 * Checks if this license is expired or not based on either expiration date
	 * or the duration. There is a file name "tm" that's get created during
	 * first start of any Resolve component. For old licenses we use that to
	 * determine if the license expired or not. For new licenses it is the upload
	 * date that is used.
	 *
	 * @return
	 * @throws Exception
	 */
	// ExtJS requires this derived data to display license expired banner while React UI uses derived days left
//	@JsonIgnore
	public boolean isExpired() throws Exception
	{
		boolean result = false;
		try
		{
			Long expirationDate = getExpirationDate();

			if (expirationDate == null)
			{
				Log.log.trace("This License will never expire");
			}
			else
			{
				Log.log.info("License expiration date is " + DateUtils.convertTimeInMillisToString(
																		expirationDate, 
																		LicenseEnum.DATE_FORMAT_LONG.getKeygenName()));
				long currentTime = DateUtils.getCurrentTimeInMillis();
				result = expirationDate < currentTime;
			}
		}
		catch (Exception e)
		{
			Log.log.error("Error checking if license is expired:\n" + e.getMessage());
			throw new Exception("Error checking if license is expired:\n" + e.getMessage());
		}
		return result;
	}

	@Deprecated
	@SuppressWarnings({ "rawtypes", "unchecked" })
	public Map asMap()
	{
		Map result = new HashMap();
		result.put(LicenseEnum.LICENSE_TYPE.getKeygenName(), getType());
		result.put(LicenseEnum.LICENSE_KEYS.getKeygenName(), getLicenseKeys());
		result.put(LicenseEnum.NO_OF_CORE.getKeygenName(), getCores());
		result.put(LicenseEnum.END_USER_COUNT.getKeygenName(), getEndUserCount());
		result.put(LicenseEnum.HA.getKeygenName(), isHA());
		result.put(LicenseEnum.DURATION.getKeygenName(), getDuration());
		result.put(LicenseEnum.EXPIRATION_DATE.getKeygenName(), getExpirationDate());
		result.put(LicenseEnum.MAX_MEMORY.getKeygenName(), getMaxMemory());
		if (gateway.size() > 0)
		{
			List<Map<String, String>> gatewayMap = new ArrayList<Map<String, String>>();
			for (GatewayLicense gatewayLicense : gateway.values())
			{
				gatewayMap.add(gatewayLicense.asMap());
			}
			result.put(LicenseEnum.GATEWAYS.getKeygenName(), gatewayMap);
		}
		return result;
	}
	
	private boolean verifySignature(String origianlEncLicenseData, String licenseSignature) throws 
																								NoSuchAlgorithmException, 
																								NoSuchProviderException, 
																								InvalidKeyException, 
																								SignatureException, 
																								DecoderException {
		Signature sig = Signature.getInstance("SHA1withDSA", "SUN");   	
    	sig.initVerify(dspub);
    	sig.update(origianlEncLicenseData.getBytes(StandardCharsets.UTF_8));
    	return sig.verify(Hex.decodeHex(licenseSignature.toCharArray()));
	}
	
	private String getOriginalEncLicense(String decLicenseData) throws Exception {
		String orgDecLicensData = decLicenseData;
		
		if (orgDecLicensData.contains(License.NEW_LINE + LicenseEnum.UPLOAD_DATE.getKeygenName() + License.EQUAL)) {
			orgDecLicensData = StringUtils.substringBefore(orgDecLicensData, 
														   License.NEW_LINE + LicenseEnum.UPLOAD_DATE.getKeygenName() + 
														   License.EQUAL);
		}
		
		if (orgDecLicensData.contains(License.NEW_LINE + LicenseEnum.DELETE_DATE.getKeygenName() + License.EQUAL)) {
			orgDecLicensData = StringUtils.substringBefore(orgDecLicensData, 
					   									   License.NEW_LINE + LicenseEnum.DELETE_DATE.getKeygenName() + 
					   									   License.EQUAL);
		}
		
		return CryptUtils.encryptLicense(orgDecLicensData);
	}
	
	public Long getEffectiveStartDate() {
		if ((LicenseEnum.LICENSE_TYPE_V2 != LicenseEnum.valueOf(LICENSE_TYPE_PREFIX + type)) ||
			((LicenseEnum.LICENSE_TYPE_V2 == LicenseEnum.valueOf(LICENSE_TYPE_PREFIX + type)) &&
			 intervals.isEmpty())) {
			return startTimeInMillis;
		}
		
		Long effStartDate = intervals.iterator().next().getStartDate();
		Log.log.trace("getEffectiveStartDate() retruning " + effStartDate);
		return effStartDate;
	}
	
	@Override
	public String toString()
	{
		StringBuilder string = new StringBuilder();
		String creationDate = "";
		String expirationDate = "";
		
		if(createDate > 0)
		{
			creationDate = DateUtils.convertTimeInMillisToString(createDate, LicenseEnum.DATE_FORMAT_LONG.getKeygenName());
		}
		
		Long cExpirationDate = getExpirationDate();
		if(cExpirationDate != null)
		{
			expirationDate = DateUtils.convertTimeInMillisToString(cExpirationDate, 
																   LicenseEnum.DATE_FORMAT_LONG.getKeygenName());
		}
		
		try
		{
			string.append("License [key= " + key + ", type= " + type + 
						  ", Effective Start Date (generated)= " + getEffectiveStartDate() + 
						  ", expirationDate=" + expirationDate + ", create date=" + creationDate + 
						  ", isLegacyLicense=" + isOldLicense + ", env=" + env + ", HA=" + isHA + 
						  ", duration=" + duration + ", isExpired=" + isExpired() + ", ipAddresses=" + ipAddresses + 
						  ", cores=" + cores + ", maxMemory=" + maxMemory + ", endUserCount=" + endUserCount + 
						  ", uploadDate=" + uploadDate + ", deleteDate=" + deleteDate + ", keys=" + 
						  StringUtils.mapToString(getLicenseKeys(), VALUES_SEPARATOR, "="));
		}
		catch (Exception e)
		{
			Log.log.error(e.getMessage(), e);
		}
		
		string.append(", gateway= [");
		for (GatewayLicense gatewayLicense : gateway.values())
		{
			string.append("\n" + gatewayLicense.toString());
		}
		string.append("]\n]");
		
		string.append(", intervals= [");
		for (LicenseInterval interval : intervals) {
			string.append("\n" + interval.toString());
		}
		string.append("]\n]");
		
		string.append(", eventSources= [")
			  .append(StringUtils.collectionToString(eventSources, VALUES_SEPARATOR))
			  .append("]\n]");
		
		return string.toString();
	}

	@Override
	public int compareTo(License license) {
		int result = Integer.compare(LicenseEnum.valueOf(LICENSE_TYPE_PREFIX + type).ordinal(), 
				     				 LicenseEnum.valueOf(LICENSE_TYPE_PREFIX + license.getType()).ordinal());
		
		if (result == 0) {
			result = Long.compare(createDate, license.getCreateDate());
		}
		
		return result;
	}
	
	@JsonIgnore
	public boolean isV2() {
		return LicenseEnum.LICENSE_TYPE_V2 == LicenseEnum.valueOf(LICENSE_TYPE_PREFIX + getType().toUpperCase());
	}
	
	@JsonIgnore
	public boolean isV2AndExpiredAndNonProd() throws Exception {
		return isV2() && isExpired() && !LicenseEnum.LICENSE_ENV_PRODUCTION.getKeygenName().equalsIgnoreCase(getEnv());
	}
	
	@JsonIgnore
	public boolean isV2AndNonProd() throws Exception {
		return isV2() && !LicenseEnum.LICENSE_ENV_PRODUCTION.getKeygenName().equalsIgnoreCase(getEnv());
	}
	
	@JsonIgnore
	public boolean isV2AndExpiredAndProd() throws Exception {
		return isV2() && isExpired() && LicenseEnum.LICENSE_ENV_PRODUCTION.getKeygenName().equalsIgnoreCase(getEnv());
	}
}