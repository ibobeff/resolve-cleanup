/******************************************************************************
* (C) Copyright 2014
*
* Resolve Systems, LLC
*
* All rights reserved
* This software is distributed under license and may not be used, copied,
* modified, or distributed without the express written permission of
* Resolve Systems, LLC.
*
******************************************************************************/

package com.resolve.service;

import java.io.Serializable;
import java.time.Instant;
import java.time.temporal.ChronoUnit;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;
import java.util.SortedMap;
import java.util.TreeMap;
import java.util.TreeSet;

import org.apache.commons.lang3.StringUtils;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.resolve.util.Constants;
import com.resolve.util.DateUtils;
import com.resolve.util.ERR;
import com.resolve.util.LicenseEnum;
import com.resolve.util.Log;

/**
 * This class represents a Resolve Gateway/Connector License.
 *
 */
public class GatewayLicense implements Serializable
{
	
    private static final long serialVersionUID = 5488219664438801638L;
    private String code;
    private int instanceCount;
    private int duration;
    @JsonIgnore
    private long componentStartTimeInMillis;
    @JsonIgnore
    private Long expirationDate;
    //just a runtime property that indicates when this gateway started.
    @JsonIgnore
    private long gatewayStartingTimeInMillis;

    //Stores the key (guid) of the RSRemote that a gateway is running.
    //The value is the queue name that gateway is using (e.g., EMAIL, NETCOOL)
    //Primary and Secondary gateways will have same queue name, that's fine.
    //instanceCount of 1 means actually maximum 2 items in this Map with same queue
    //name can exists.
    @JsonIgnore
    @Deprecated
    private Map<String, GatewayInstance> runningInstances = new HashMap<String, GatewayInstance>();
    
    //Stores the key (gateway code-instance type-queue name-instance id) of the RSRemote that a gateway is running.
    //The value is the gateway info containing instance id, instance type (primary/secondary/worker)
    //queue name (e.g., EMAIL, NETCOOL) and start time.
    //Primary and Secondary gateways will have same queue name, that's fine.
    @JsonIgnore
    private Map<String, GatewayInstanceNew> runningInstancesNew = new HashMap<String, GatewayInstanceNew>();

    public GatewayLicense(String code, int instanceCount, int duration, Long expirationDate, Long startDate)
    {
        this.code = code;
        this.instanceCount = instanceCount;
        this.duration = duration;
        this.expirationDate = expirationDate;
        this.componentStartTimeInMillis = startDate;
    }

    public String getCode()
    {
        return code;
    }

    public void setCode(String code)
    {
        this.code = code;
    }

    public int getInstanceCount()
    {
        return instanceCount;
    }

    public void setInstanceCount(int instanceCount)
    {
        this.instanceCount = instanceCount;
    }

    public int getDuration()
    {
        return duration;
    }

    public void setDuration(int duration)
    {
        this.duration = duration;
    }

    @JsonIgnore
    public long getStartTimeInMillis()
    {
        return componentStartTimeInMillis;
    }

    public void setStartTimeInMillis(long startDate)
    {
        this.componentStartTimeInMillis = startDate;
    }

    @JsonIgnore
    public Date getDurationBasedExpirationDate()
    {
        //first find out the expiration based on duration
        Date durationBasedExpirationDate = null;
        if (getDuration() > 0)
        {
            long durationInMillis = getDuration() * 24 * 60 * 60 * 1000L;
            durationBasedExpirationDate = new Date(getStartTimeInMillis() + durationInMillis);
        }
        return durationBasedExpirationDate;
    }

    public Long getExpirationDate()
    {
        if (expirationDate == null && getDuration() > 0)
        {
            long durationInMillis = getDuration() * 24 * 60 * 60 * 1000L;
            expirationDate = getStartTimeInMillis() + durationInMillis;
        }
        return expirationDate;
    }

    public void setExpirationDate(Long expirationDate)
    {
        this.expirationDate = expirationDate;
    }

    @Deprecated
    public Map<String, GatewayInstance> getRunningInstances()
    {
        return runningInstances;
    }

    public Map<String, GatewayInstanceNew> getRunningInstancesNew()
    {
        return runningInstancesNew;
    }
    
    @Deprecated
    public void setRunningInstances(Map<String, GatewayInstance>  runningInstances)
    {
        this.runningInstances = runningInstances;
    }

    public void setRunningInstancesNew(Map<String, GatewayInstanceNew>  runningInstancesNew)
    {
        this.runningInstancesNew = runningInstancesNew;
    }
    
    @Override
    public String toString()
    {
        return "GatewayLicense [code=" + code + ", instanceCount=" + instanceCount + ", duration=" + duration + ", expirationDate=" + 
               (expirationDate == null ? "" : DateUtils.convertTimeInMillisToString(
            		   										expirationDate, 
            		   										LicenseEnum.DATE_FORMAT_LONG.getKeygenName())) + 
               								  ", runningInstancesNew=" + runningInstancesNew + "]";
    }

    public Map<String, String> asMap()
    {
        Map<String, String> result = new HashMap<String, String>();
        result.put(LicenseEnum.GATEWAY_CODE.getKeygenName(), getCode());
        result.put(LicenseEnum.GATEWAY_INSTANCE.getKeygenName(), ""+getInstanceCount());
        result.put(LicenseEnum.GATEWAY_DURATION.getKeygenName(), ""+getDuration());
        result.put(LicenseEnum.GATEWAY_EXPIRATION.getKeygenName(), DateUtils.convertTimeInMillisToString(
        																getExpirationDate(), 
        																LicenseEnum.DATE_FORMAT_LONG.getKeygenName()));
        return result;
    }

    public String info()
    {
        StringBuilder result = new StringBuilder();
        result.append("Number of Instance Limit=" + instanceCount);

        if (getExpirationDate() != null)
        {
            //daysLeft = String.valueOf(TimeUnit.DAYS.convert(expirationDate.getTime(), TimeUnit.MILLISECONDS) - TimeUnit.DAYS.convert(System.currentTimeMillis(), TimeUnit.MILLISECONDS));
            result.append(", Expiration Date: " + DateUtils.convertTimeInMillisToString(
            													expirationDate, 
            													LicenseEnum.DATE_FORMAT_LONG.getKeygenName()));
        }
        else
        {
            result.append(", Expiration Date: No Expiration");
        }


        return result.toString();
    }

    public String toLicenseFormat()
    {
        return getCode() + "#" + getInstanceCount() + "#" + getDuration();
    }

    @Deprecated
    public void update(GatewayLicense newGatewayLicense)
    {
        this.instanceCount += newGatewayLicense.getInstanceCount();
        this.duration += newGatewayLicense.getDuration();
        //transfer running instances
        this.runningInstances = newGatewayLicense.getRunningInstances();
    }

    public void updateNew(GatewayLicense newGatewayLicense)
    {
        this.instanceCount += newGatewayLicense.getInstanceCount();
        this.duration += newGatewayLicense.getDuration();
        //transfer running instances new 
        Map<String, GatewayInstanceNew> newGatewayLicRunningInsts = newGatewayLicense.getRunningInstancesNew();
        
        if (newGatewayLicRunningInsts != null && !newGatewayLicRunningInsts.isEmpty())
        {
            for (String instKey : newGatewayLicRunningInsts.keySet())
            {
                this.runningInstancesNew.put(instKey, newGatewayLicRunningInsts.get(instKey));
            }
        }
    }
    
    public long getGatewayStartingTimeInMillis()
    {
        return gatewayStartingTimeInMillis;
    }

    public void setGatewayStartingTimeInMillis(long gatewayStartingTimeInMillis)
    {
        this.gatewayStartingTimeInMillis = gatewayStartingTimeInMillis;
    }

    @Deprecated
    class GatewayInstance
    {
        String instanceType;
        String instanceId;
        String queue;
        long startTime;
        
        public GatewayInstance(String instanceId, String instanceType, String queue, long startTime)
        {
            this.instanceId = instanceId;
            this.instanceType = instanceType;
            this.queue = queue;
            this.startTime = startTime;
        }
     
        //"INSTANCE_ID"), instance.get("INSTANCE_TYPE"), instance.get("QUEUE"), instance.get("STARTTIME"))
        public Map<String, String> asMap()
        {
            Map<String, String> result = new HashMap<String, String>();
            result.put("INSTANCE_ID", instanceId);
            result.put("INSTANCE_TYPE", instanceType);
            result.put("QUEUE", queue);
            result.put("STARTTIME", ""+startTime);
            return result;
        }
        
        @Override
        public String toString()
        {
            return "GatewayInstance [gatewayType="+ instanceType +", guid=" + instanceId + ", queue=" + queue + ", startTime=" + startTime + "]";
        }
    }

    public class GatewayInstanceNew implements Comparable<GatewayInstanceNew>
    {
        String instanceType;
        String instanceId;
        String queue;        
        long startTime;
        String gatewayCode;
        long lastCheckRunningTime; // Can be used by LicenseServie to evict running instance if lastCheckRunningTime has not been updated for a long period
        
        public GatewayInstanceNew(String instanceId, String instanceType, String queue, long startTime, String gatewayCode)
        {
            this.instanceId = instanceId;
            this.instanceType = instanceType;
            this.queue = queue;
            this.startTime = startTime;
            this.gatewayCode = gatewayCode;
            this.lastCheckRunningTime = startTime;
        }
     
        //"INSTANCE_ID"), instance.get("INSTANCE_TYPE"), instance.get("QUEUE"), instance.get("STARTTIME"))
        public Map<String, String> asMap()
        {
            Map<String, String> result = new HashMap<String, String>();
            result.put("INSTANCE_ID", instanceId);
            result.put("INSTANCE_TYPE", instanceType);
            result.put("QUEUE", queue);
            result.put("STARTTIME", ""+startTime);
            result.put("GATEWAY_CODE", gatewayCode);
            result.put("LASTCHECKRUNNINGTIME", ""+startTime);
            return result;
        }
        
        @Override
        public String toString()
        {
            Calendar calStartTime = Calendar.getInstance();
            calStartTime.setTimeInMillis(startTime);
            Calendar calLastCheckRunningTime = Calendar.getInstance();
            calLastCheckRunningTime.setTimeInMillis(lastCheckRunningTime);
            return "GatewayInstanceNew [gatewayType="+ instanceType +", guid=" + instanceId + ", queue=" + 
                   queue + ", startTime=" + calStartTime.getTime() + ", gatewayCode=" + gatewayCode + 
                   ", last check running time=" + calLastCheckRunningTime.getTime() + "]";
        }

        public int compareTo(GatewayInstanceNew o)
        {
            long timeDiff = this.startTime - o.startTime;
            
            if (timeDiff < 0)
                return -1;
            else if (timeDiff == 0)
                return 0;
            else
                return 1;
        }
    }
    
    @Deprecated
    public void addRunningInstance(String instanceId, String instanceType, String queue, Long startTime)
    {
        GatewayInstance instance = new GatewayInstance(instanceId, instanceType, queue, startTime);
        runningInstances.put(instanceId, instance);
    }
    
    public void addRunningInstanceNew(String instanceId, String instanceType, String queue, Long startTime, String gatewayCode)
    {
        GatewayInstanceNew instance = new GatewayInstanceNew(instanceId, instanceType, queue, startTime, gatewayCode);
        runningInstancesNew.put(gatewayCode + "-" + instanceType + "-" + queue + "-" + instanceId, instance);
    }
    
    public void removeRunningInstanceNew(String instanceId, String instanceType, String queue, String gatewayCode)
    {
        runningInstancesNew.remove(gatewayCode + "-" + instanceType + "-" + queue + "-" + instanceId);
    }
    
    public void updateRunningInstanceNew(String instanceId, String instanceType, String queue, Long startTime, Long lastCheckRunningTime, String gatewayCode)
    {
        GatewayInstanceNew instance = runningInstancesNew.get(gatewayCode + "-" + instanceType + "-" + queue + "-" + instanceId);
        
        if (instance == null)
        {// Create if not found
            addRunningInstanceNew(instanceId, instanceType, queue, startTime, gatewayCode);
            instance = runningInstancesNew.get(gatewayCode + "-" + instanceType + "-" + queue + "-" + instanceId);
        }
        
        if (instance != null)
        {
            instance.lastCheckRunningTime = lastCheckRunningTime;
        }
        else
        {
            Log.log.warn("Could not find/create gateway running instance for " + gatewayCode + "-" + instanceType + "-" + queue + "-" + instanceId);
        }
    }
    
    public void switchRunningInstanceNew(String instanceId, String instanceType, String queue, String gatewayCode)
    {
        GatewayInstanceNew instance = runningInstancesNew.get(gatewayCode + "-" + instanceType + "-" + queue + "-" + instanceId);
        
        if (instance != null)
        {
            String newInstanceType = instance.instanceType.equals("PRIMARY") ? "SECONDARY" : "PRIMARY";
            
            instance.instanceType = newInstanceType;
            
            runningInstancesNew.remove(gatewayCode + "-" + instanceType + "-" + queue + "-" + instanceId);        
            runningInstancesNew.put(gatewayCode + "-" + newInstanceType + "-" + queue + "-" + instanceId, instance);
        }
        else
        {
            Log.log.warn("Could not find " + gatewayCode + "-" + instanceType + "-" + queue + "-" + instanceId +
                         " in running gateway instances to switch instance type.");
        }
    }
    
    @Deprecated
    public void addRunningInstance(String instanceId, String instanceType, String queue, String startTime)
    {
        if(StringUtils.isNumeric(startTime))
        {
            addRunningInstance(instanceId, instanceType, queue, Long.valueOf(startTime));
        }
    }

    public void addRunningInstanceNew(String instanceId, String instanceType, String queue, String startTime, String gatewayCode) throws Exception
    {
        if(StringUtils.isNumeric(startTime))
        {
            addRunningInstanceNew(instanceId, instanceType, queue, Long.valueOf(startTime), gatewayCode);
        }
        else
        {
            throw new Exception("Error in adding " + gatewayCode + "-" + instanceType + "-" + queue + "-" + instanceId + 
                                " with Start time " + startTime + " to gateway license " + code + " since " + startTime + 
                                " is not numeric.");
        }
    }
    
    public void updateRunningInstanceNew(String instanceId, String instanceType, String queue, String startTime, String lastCheckRunningTime, String gatewayCode) throws Exception
    {
        if(StringUtils.isNumeric(startTime) && StringUtils.isNumeric(lastCheckRunningTime))
        {
            updateRunningInstanceNew(instanceId, instanceType, queue, Long.valueOf(startTime), Long.valueOf(lastCheckRunningTime), gatewayCode);
        }
        else
        {
            throw new Exception("Error in updating " + gatewayCode + "-" + instanceType + "-" + queue + "-" + instanceId + 
                                " with Last Check Running time " + lastCheckRunningTime + " in gateway license " + code + " since " + 
                                startTime + " and/or " + lastCheckRunningTime + " is not numeric.");
        }
    }
    
    @JsonIgnore
    @Deprecated
    public Map<String, Map<String, String>> getRunningInstancesAsMap()
    {
        Map<String, Map<String, String>> result = new HashMap<String, Map<String, String>>();
        for(String instanceId : runningInstances.keySet())
        {
            result.put(instanceId, runningInstances.get(instanceId).asMap());
        }
        return result;
    }
    
    @JsonIgnore
    public Map<String, Map<String, String>> getRunningInstancesNewAsMap()
    {
        Map<String, Map<String, String>> result = new HashMap<String, Map<String, String>>();
        for(String instanceId : runningInstancesNew.keySet())
        {
            result.put(instanceId, runningInstancesNew.get(instanceId).asMap());
        }
        return result;
    }

    /**
     * Validity of gateway instances are as follows. In a distributed rsremote cluster, there could
     * be multiple instances of the same gateway that get started because it did not see the other
     * instances while it was starting. However during the subsequent license check, license service
     * will find out the instances that start later. If a particular instance finds itslef to be the 
     * one that started after permitted number of instances, it will shutdown.
     * 
     * @param key
     * @param instanceType such as PRIMARY, SECONDARY, or WORKER
     * @return
     */
    @Deprecated
    public boolean isValid(String key, String instanceType, String gatewayCode)
    {
        Log.log.debug("Checking if it can run " + code + " gateway with instance id " + key);
        boolean result = false;
        
        if(!runningInstances.containsKey(key))
        {
            //first time this is being added to running instances
            if(instanceCount == 0 || instanceCount > runningInstances.size())
            {
                result = true;
            }
        }
        else
        {
            result = true;
            //the instance already added but due to the messaging delay
            //this gateway might have started without knowing that another instance 
            //already started. this gateway will go down if it started later.
            SortedMap<Long, String> startTimes = new TreeMap<Long, String>();
            for(GatewayInstance instance : runningInstances.values())
            {
                Log.log.debug(code + " gateway instance " + instance.instanceId + ", started at " + instance.startTime);
                startTimes.put(instance.startTime, instance.instanceId);
            }
            int i = 1;
            for(String guid : startTimes.values())
            {
                if(key.equalsIgnoreCase(guid) && instanceCount > 0 && i > instanceCount)
                {
                    Log.log.debug(code + " gateway instance " + key + ", started later than other instance");
                    result = false;
                    break;
                }
                i++;
            }
        }
        
        //instance type based check
        /*
         * If code is 'ANY' then it is assumed that instanceCount will be multiple 
         * of 3 corresponding each gateway instance for primary, secondary and worker
         */
        if(result)
        {
            boolean isInstanceAllowed = false;
            if(getInstanceCount() == 0)
            {
                isInstanceAllowed = true;
            }
            else
            {
                if(StringUtils.isBlank(instanceType))
                {
                    //for SSH/Telnet type gateways where there is no concept of primary or secondary.
                    Log.log.debug("Unclusterable gateway");
                    isInstanceAllowed = true;
                }
                else
                {
                    Log.log.debug("Clusterable gateway");
                    
                    if((getInstanceCount() - runningInstances.size()) >= 1 && "PRIMARY".equals(instanceType))
                    {
                        isInstanceAllowed = true;
                    }
                    else if((getInstanceCount() - runningInstances.size()) >= 2 && "SECONDARY".equals(instanceType))
                    {
                        isInstanceAllowed = true;
                    }
                    else if((getInstanceCount() - runningInstances.size()) >= 3 && "WORKER".equals(instanceType))
                    {
                        isInstanceAllowed = true;
                    }
                }
            }
            result = isInstanceAllowed;
        }
        
        return result;
    }
    
    public boolean isValidNew(String key, String instanceType, String gatewayCode, String queueName, long time, boolean checkToStart, boolean originalPrimary)
    {
        boolean result = false;
        long timeToCheck = new Date().getTime() - (30 /*7*/ * 60 * 1000);
        long lastCheckRunnigTimeThreshold = timeToCheck - 1000;
        Calendar calLastCheckRunnigTimeThreshold = Calendar.getInstance();
        calLastCheckRunnigTimeThreshold.setTimeInMillis(lastCheckRunnigTimeThreshold);
        
        Calendar cal = Calendar.getInstance();
        cal.setTimeInMillis(time);
        Log.log.debug(String.format("Checking if %s %s %s gateway can %s under license for %s with instance id %s" + 
  									" and queue %s at %s", gatewayCode, 
  									(originalPrimary ? "ORIGINAL " : ""), 
  									instanceType, (checkToStart ? "start" : "keep running"), code, key, queueName,
  									cal.getTime()));
        
        // Check if current remote server running instances contains unique key
        if(!runningInstancesNew.containsKey(gatewayCode + "-" + instanceType + "-" + queueName + "-" + key))
        {
            Log.log.debug("Running instances in gateway license " + code + " does not contain " + 
                          gatewayCode + "-" + instanceType + "-" + queueName + "-" + key + " during " + 
                          (checkToStart ? " check if it can start" : " check if it can still keep running") +
                          (originalPrimary ? " (ORIGINAL PRIMARY)" : ""));
            
            result = true;
            
            // Get all running gateway instances
            for (GatewayInstanceNew instance : runningInstancesNew.values())
            {
                Log.log.trace(gatewayCode + " matches with instance " + instance);
                // Filter in only gateway instances of specific gateway to check license
                if (instance.gatewayCode.equals(gatewayCode))
                {
                    // DO not allow more than one instance of any gateway of any type and any queue to run on single remote server or in cluster
                    // (primary and secondary only) unless it is for check to start and is orginal primary and running instance type is secondary
                    // in which case it is check for secondary becoming primary in which case it is allowed
                    if (instance.instanceId.equals(key))
                    {
                        if (instance.instanceType.equals("SECONDARY") && instanceType.equals("PRIMARY") && checkToStart && originalPrimary)
                        {
                            Log.log.debug("This check license request is identified as request before original secondary fails over to become primary.");
                            break;
                        }
                        
                        if (instance.instanceType.equals("PRIMARY") && instanceType.equals("SECONDARY") && !checkToStart && !originalPrimary)
                        {
                            Log.log.debug("This check license request is identified as request before original secondary failed over to become primary fails back to become secondary.");
                            break;
                        }
                        
                        Log.log.warn( ERR.E10017.getCode() + ":" + ERR.E10017.getMessage() + 
                                      " Not permitted to start more than single instance of " + gatewayCode + " " + instanceType + " using queue " + 
                                      queueName + " in cluster since it is already running on different remote server.");
                        alert(ERR.E10017, " Not permitted to start more than single instance of " + gatewayCode + " " + instanceType + " using queue " + 
                              queueName + " in cluster since it is already running on different remote server.");
                        result = false;
                        break;
                    }
                    
                    // If Different instance id then check instance type but allow WORKER or second PRIMARY to run if it happens to be originalPrimary
                    
                    if (instance.instanceType.equals(instanceType) && instance.queue.equals(queueName))
                    {
                        if (instanceType.equals("PRIMARY"))
                        {
                            if (originalPrimary)
                            { // Original PRIMARY then do allow it to get back on as PRIMARY after failover.
                                Log.log.info("Letting " + gatewayCode + " " + instanceType + " for queue " + queueName + 
                                                " to run on remote server with instance id " + key + 
                                                " even though it is running on another remote server with instance id " + 
                                                instance.instanceId + " since it is original " + instanceType);
                            }
                            else
                            {
                                Log.log.warn( ERR.E10017.getCode() + ":" + ERR.E10017.getMessage() + 
                                              " Not starting " + gatewayCode + " " + instanceType + " using queue " + 
                                              queueName + " on current remote server since it was not originally configured to run " + 
                                              gatewayCode + " using queue " + queueName + " as " + instanceType);
                                alert(ERR.E10017, " Not starting " + gatewayCode + " " + instanceType + " using queue " + 
                                      queueName + " on current remote server since it was not originally configured to run " + 
                                      gatewayCode + " using queue " + queueName + " as " + instanceType);
                                result = false;
                            }
                        } 
                        else if (instanceType.equals("SECONDARY"))
                        {
                            Log.log.warn( ERR.E10017.getCode() + ":" + ERR.E10017.getMessage() + 
                                          " Not permitted to run more than one instance of " + gatewayCode + " " + instanceType + " using queue " + 
                                          queueName + " within cluster.");
                            alert(ERR.E10017, " Not permitted to run more than one instance of " + gatewayCode + " " + instanceType + " using queue " + 
                                  queueName + " within cluster.");
                            result = false;
                        }
                        break; // Allow mutiple WORKER instances to run on different remote server instances
                    }
                    
                    // Make sure instance type (PRIMARY/SECONDARY) and queue name combination is not running anywhere else
                    
                    if ((instanceType.equals("PRIMARY") || instanceType.equals("SECONDARY")) && 
                        instance.instanceType.equals(instanceType) && instance.queue.equals(queueName))
                    {
                        Log.log.warn( ERR.E10017.getCode() + ":" + ERR.E10017.getMessage() + 
                                      " Not starting " + gatewayCode + " " + instanceType + " using queue " + 
                                      queueName + " since this gateway is already running on another remote server.");
                        alert(ERR.E10017, " Not starting " + gatewayCode + " " + instanceType + " using queue " + 
                              queueName + " since this gateway is already running on another remote server.");
                        result = false;
                        break;
                    }
                }
            }
        }
        else
        {
        	if (checkToStart) {
        		Log.log.warn(String.format("%s-%s-%s-%s is already running on this remote server instance!!!", 
        								   gatewayCode, instanceType, queueName, key));
        		checkToStart = false;
            }
        	
            // Check to start and already running then do not start.
            if (!checkToStart) // Check if keep running
            {
                result = true;
                                
                //the instance already added but due to the messaging delay
                //this gateway might have started without knowing that another instance 
                //already started. this gateway will go down if it started later.
                // TO DO Will fail if two gateways startTime is exactly same upto ms
                
                SortedMap<Long, String> startTimes = new TreeMap<Long, String>();
                Map<String, Long> keyToStartTime = new HashMap<String, Long>();
                
                for(GatewayInstanceNew instance : runningInstancesNew.values())
                {
                    // Do not allow gateway instance of different queue and/or type to run on same remote instance
                    // Unique Running Instances Key is : Gateway Code-Instance Type-Queue Name
                    
                    if (instance.gatewayCode.equals(gatewayCode)) // Do not allow gateway instance of different queue/type to run on same remote server instance
                    {
                        Log.log.debug("Found Running instances for License " + code + " gateway code " + instance.gatewayCode + ", type " + instance.instanceType + 
                                        ", queue " + instance.queue + ", instance id " + instance.instanceId + ", start time " + instance.startTime + 
                                        ", lastCheckRunningTime " + instance.lastCheckRunningTime + ", lastCheckRunnigTimeThreshold " + lastCheckRunnigTimeThreshold);
                        
                        // If gateway misses license check then consider that gateway as inactive            
                        if (instance.lastCheckRunningTime > lastCheckRunnigTimeThreshold)
                        {
                            startTimes.put(new Long(instance.startTime), instance.gatewayCode + "-" + instance.instanceType + "-" + instance.queue + "-" + instance.instanceId );
                            keyToStartTime.put(instance.gatewayCode + "-" + instance.instanceType + "-" + instance.queue + "-" + instance.instanceId, new Long(instance.startTime));
                        }
                        else
                        {
                            Log.log.warn("Running instance " + instance + " is considered to be inactive since lastCheckRunningTime is earlier than or same as threshold value of " + 
                                         calLastCheckRunnigTimeThreshold.getTime());
                        }
                    }
                }
                
                boolean foundEarliestInstances = false;
                
                int i = 1;
                
                for(String instIdTypeQueueGWCode : startTimes.values())
                {
                    if(instIdTypeQueueGWCode.startsWith(gatewayCode + "-" + instanceType + "-" + queueName + "-"))
                    {
                        if (instanceType.equals("WORKER"))
                        {
                            continue;
                        }
                        
                        if (instanceType.equals("PRIMARY") && i > 1 && keyToStartTime.get(instIdTypeQueueGWCode).longValue() < timeToCheck )
                            foundEarliestInstances = true;
                        
                        if (!foundEarliestInstances)
                        {
                            Log.log.debug("Found " + instIdTypeQueueGWCode + " instance which started " + (i == 1 ? "earliest" : "next"));
                            
                            if (instIdTypeQueueGWCode.endsWith(key))
                            {
                                break;
                            }

                            // Allow max two PRIMARY gateway instances to run (on different remote server instances) for max 30 min
                            foundEarliestInstances = !instanceType.equals("PRIMARY") || i > 1;
                        }
                        else
                        {
                            /*
                             *  Check if running on current remote server instance, 
                             *  if yes then it should stop because it started after 
                             *  ealriest remote server instance. If it is running on different
                             *  remote server instance, that remote servers instance 
                             *  will take care of shutting it down.  
                             *  
                             *  Remove only instances running on current remote server
                             *  instance.
                             */
                            
                            if (instIdTypeQueueGWCode.endsWith(key))
                            {
                                Log.log.warn( ERR.E10017.getCode() + ":" + ERR.E10017.getMessage() + 
                                              " Stopping " + gatewayCode + " " + instanceType + " using queue " + 
                                              queueName + " since this instance was started later than the other instance.");
                                alert(ERR.E10017, "Stopping " + gatewayCode + " " + instanceType + " using queue " + 
                                                queueName + " since this instance was started later than the other instance.");
                                result = false;
                                break;
                            }
                        }
                        i++;
                    }
                }
            }
            else
            {
                Log.log.warn(gatewayCode + " " + instanceType + " for queue " + queueName + 
                             " is already running on this remote server instance");
            }
        }
        
        int subFactor = 0;
        int finalGtwCodeQueueNamePrimaryCnt = 0;
        
        //check instance count for PRIMARY GatewayCode-Queue Name combination only against gateway
        if(result)
        {
            boolean isInstanceAllowed = false;
            if(!code.equals("ANY") && getInstanceCount() == 0) //Like ANY but with unlimited instances - dev
            {
                /* 
                 * Format : <* or gateway code or ANY>#<count of licenses>
                 * Note: # - is separator symbol
                 *       0 - license count means unlimited instances, -ve value - reduce the license count
                 *       * - match any gateway name
                 *       ANY - first match specific gateway named license, if not available or all licenses
                 *             are already consumed then use ANY
                 */
                isInstanceAllowed = true;
            }
            else
            {
                if(StringUtils.isBlank(instanceType))
                {
                    //for SSH/Telnet type gateways where there is no concept of primary or secondary.
                    Log.log.debug("Unclusterable gateway");
                    isInstanceAllowed = true;
                }
                else
                {
                    Log.log.debug("Clusterable gateway");
                    
                    int addFactor = 0;
                    
                    /*
                     * Account for license which will be consumed if
                     * this license check is called before gateway starts
                     * i.e checkToStart = true and instance type of
                     * gateway is PRIMARY (Any PRIMARY gateway trying to 
                     * start is always an ORIGINAL i.e. configured in blueprint
                     * as original primary gateway.
                     *                      
                     */
                    if (checkToStart && instanceType.equals("PRIMARY"))
                    {
                        Set<GatewayInstanceNew> matchedInstances = new HashSet<GatewayInstanceNew>();
                        
		                for(GatewayInstanceNew instance : runningInstancesNew.values())
		                {
		                    if (instance.lastCheckRunningTime > lastCheckRunnigTimeThreshold)
                            {
    		                    Log.log.debug("Checking " + instance.queue + "=" + queueName + " && " + instance.gatewayCode + "=" + gatewayCode + " && " + instance.instanceType + "=" + instanceType);
    		                    if (instance.queue.equals(queueName) && instance.gatewayCode.equals(gatewayCode) && instance.instanceType.equals(instanceType)) // Do not allow gateway instance of different queue/type to run on same remote server instance
                                {
                                    matchedInstances.add(instance);
                                    /*
    		                    	doAddFactor = false;
    		                    	Log.log.debug("Starting on exising queue, so this does not count against total gateway count");
    		                    	break;
    		                    	*/
    		                    }
                            }
		                    else
                            {
                                Log.log.warn("Running PRIMARY instance " + instance + " is considered to be inactive since lastCheckRunningTime is earlier than or same as threshold value of " + 
                                             calLastCheckRunnigTimeThreshold.getTime());
                            }
		                }
		                
		                //If 0 instance i.e new gateway code - queue name primary or more than 1 matched instance then fail
                        //or if matched instance id is same as key i.e. same remote instance
                        if (/*doAddFactor*/matchedInstances.isEmpty() || matchedInstances.size() > 1 ||
                            ( matchedInstances.size() == 1 && matchedInstances.iterator().next().instanceId.equals(key)))
                        {
                            addFactor++;
                        }
                    }
                    
                    //Adjust addFactor to account for additional second instances of PRIMARY gateway which is running
                    
                    Map<String, TreeSet<GatewayInstanceNew>> gtwCodeQNameRunningInsts = new HashMap<String, TreeSet<GatewayInstanceNew>>();
                    
                    for(GatewayInstanceNew instance : runningInstancesNew.values())
                    {                        
                        Log.log.trace("Instance Check: " + instance.instanceId + " " + instance.gatewayCode + " " + instance.queue + 
                                      " [" + instance.instanceType + "] " + instance.lastCheckRunningTime + " " + lastCheckRunnigTimeThreshold);
                        if (instance.instanceType.equals("PRIMARY")) 
                        {
                            if (instance.lastCheckRunningTime > lastCheckRunnigTimeThreshold)
                            {
                                TreeSet<GatewayInstanceNew> gtwRunningInsts = gtwCodeQNameRunningInsts.get(instance.gatewayCode + "-" + instance.queue);
                                
                                if (gtwRunningInsts == null)
                                {
                                    gtwRunningInsts = new TreeSet<GatewayInstanceNew>();
                                    gtwCodeQNameRunningInsts.put(instance.gatewayCode + "-" + instance.queue, gtwRunningInsts);
                                }
                                
                                if (!gtwRunningInsts.contains(instance))
                                    gtwRunningInsts.add(instance);
                            }
                            else
                            {
                                Log.log.warn("Running PRIMARY instance " + instance + " is considered to be inactive since lastCheckRunningTime is earlier than or same as threshold value of " + 
                                             calLastCheckRunnigTimeThreshold.getTime());
                            }
                        }
                    }
                    
                    int gtwCodeQNameRunningInstsSize = 0;
                    
                    for (String gtwCodeQname : gtwCodeQNameRunningInsts.keySet())
                    {
                        TreeSet<GatewayInstanceNew> gtwRunningInsts = gtwCodeQNameRunningInsts.get(gtwCodeQname);
                        
                        if (gtwRunningInsts.size() > 1)
                        {
                            int i = 0;
                            for (GatewayInstanceNew gtwRunningInst : gtwRunningInsts)
                            {
                                i++;
                                
                                if (i == 1)
                                {
                                    continue;
                                }
                                
                                // Check that next instance has not been running for more than 30 min
                                if (i == 2 && gtwRunningInst.startTime > timeToCheck)
                                {
                                    subFactor++;
                                }
                                
                                if (i > 1)
                                    break;
                            }
                        }
                        
                        gtwCodeQNameRunningInstsSize += gtwRunningInsts.size();
                    }
                    
                    Log.log.debug("Total Gateway License Count: " + getInstanceCount() + 
                                  ", Total Running Primary Gateways (PRIMARY Gateway Code-Queue Name) Count (" +
                                  gtwCodeQNameRunningInstsSize + " + " + addFactor + " - " + subFactor + ") : " + 
                                  (gtwCodeQNameRunningInstsSize + addFactor - subFactor));
                    
                    isInstanceAllowed = getInstanceCount() >= (gtwCodeQNameRunningInstsSize + addFactor - subFactor);
                    finalGtwCodeQueueNamePrimaryCnt = gtwCodeQNameRunningInstsSize + addFactor - subFactor;
                }
            }
            
            result = isInstanceAllowed;
            
            if (!result)
            {
                String msgPrefix = checkToStart ? " Trying to start" : "Running";
                String msgSuffix = checkToStart ? " will not start." : " is stopped";
                Log.log.warn( ERR.E10017.getCode() + ":" + ERR.E10017.getMessage() + msgPrefix + " more than permitted number (" + getInstanceCount() + 
                              ") instances of gateways under " + code + " license. " + gatewayCode + " " + instanceType + " using queue " + queueName + msgSuffix);
                alert(ERR.E10017, msgPrefix + " more than permitted number (" + getInstanceCount() + 
                              ") instances of gateways under " + code + " license. " + gatewayCode + " " + instanceType + " using queue " + queueName + msgSuffix);
            }
        }
        
        Log.log.debug("Can " + (result ? "" : "not ") +  (checkToStart ? "start " : "keep running ") + gatewayCode + " " + instanceType + " gateway under license for " + code + " with instance id " + 
                       key + " and queue " + queueName + " at " + cal.getTime());
        
        Log.log.info("Consumed " + finalGtwCodeQueueNamePrimaryCnt + " gateway " + code + " licenses" + (getInstanceCount() == 0 ? "" : " out of " + getInstanceCount()) );
        
        if (Log.log.isTraceEnabled())
        {
            Log.log.trace("Gateway Running Instances:");
            int i = 1;
            for (GatewayInstanceNew instance : runningInstancesNew.values())
            {
                Log.log.trace("" + (i++) + ") " + instance);
            }
        }
        
        return result;
    }

    @JsonIgnore
    public boolean isExpired()
    {
        boolean result = false;
        try
        {
            Long expirationDate = getExpirationDate();
            
            if (expirationDate == null)
            {
                Log.log.trace("This License will never expire");
            }
            else
            {
                Log.log.info("Gateway License expiration date is " + 
                			 DateUtils.convertTimeInMillisToString(expirationDate, 
                												   LicenseEnum.DATE_FORMAT_LONG.getKeygenName()));
                result = expirationDate < System.currentTimeMillis();
            }
        }
        catch (Exception e)
        {
            Log.log.error("Error checking if gateway license is expired:\n" + e.getMessage());
            result = true;
        }
        return result;
    }
    
    @JsonIgnore
    public long getDaysLeft()
    {
        long result = 0;
        Long expirationDate = getExpirationDate();
        
        if (expirationDate == null)
        {
            Log.log.trace("This License will never expire");
        }
        else
        {
        	result = ChronoUnit.DAYS.between(Instant.now(), Instant.ofEpochMilli(getExpirationDate()));
            Log.log.debug("Gateway License time left (days) : " + result);
        }
        return result;
    }
    
    private void alert(ERR err, String message)
    {
        Log.alert(err.getCode(), err.getMessage() + " " + message, Constants.ALERT_TYPE_LICENSE);
    }
    
    public Long getRuningInstanceStartTime(String instanceId, String instanceType, String queue, String gatewayCode)
    {
        long startTime = new Date().getTime(); // Default Current time if not found
        
        GatewayInstanceNew instance = runningInstancesNew.get(gatewayCode + "-" + instanceType + "-" + queue + "-" + instanceId);
        
        if (instance != null)
        {
            startTime = instance.startTime;
        }
        
        return new Long(startTime);
    }
}