package com.resolve.service;

import java.io.File;
import java.nio.charset.StandardCharsets;
import java.time.Instant;
import java.time.temporal.ChronoUnit;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.SortedSet;
import java.util.TreeSet;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.TimeUnit;

import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.collections.MapUtils;
import org.apache.commons.lang3.tuple.Pair;

import com.resolve.rsbase.MainBase;
import com.resolve.util.Constants;
import com.resolve.util.CryptUtils;
import com.resolve.util.DateUtils;
import com.resolve.util.ERR;
import com.resolve.util.FileUtils;
import com.resolve.util.Guid;
import com.resolve.util.LicenseEnum;
import com.resolve.util.LicenseUtil;
import com.resolve.util.Log;
import com.resolve.util.StringUtils;
import com.resolve.util.SystemUtil;

/**
 * This is the License Service that runs in RSMGMT. This service manages the
 * Resolve licenses in an environment.
 */
public class LicenseService
{
	public static final String CHECKCODE = "RESOLVE SYSTEMS";

	public static final String GUID = "GUID";
	public static final String DESCRIPTION = "DESCRIPTION";
	public static final String IP = "IP";
	public static final String RBC_FIELD_SEPARATOR = "/";
	public static final String RBC_RELEASE_63_UNCLASSIFIED_PREFIX = "/$R63Unclassified";

	private static final Long oneDayInMillis = 86400000L; //24 * 60 * 60 * 1000L;
	private static final long twoWeeksInMillis = 14 * oneDayInMillis;
	private static final String SDK2_SOURCE_SUFFIX = "Gateway";
	private static final String EXPIRED_LICENSE_ALERT_MSG_PREFIX = "License has expired.";
	private static final String EXPIRED_LICENSE_PROD_ALERT_MSG_SUFFIX = " instance is running with an expired license!!!";
	private static final String EXPIRED_LICENSE_NON_PROD_ALERT_MSG_SUFFIX = 
									" instance usage is severely restricted untill renewal!!!";
	private static final String RESOLVE_SHUTDOWN_WARNING = 
									"Resolve will shutdown in %d days if the violation left unresolved. License Key : %s";
	private static final String PACKAGE_SEPARATOR = ".";
	private static final String RESOLVE_PACKAGE_PREFIX = "com.resolve.";
	private static final String GATEWAY_DETAILS = " Gateway Code: %s, RSRemote Instance Id: %s, Instance Type: %s, " +
												  "Queue Name: %s, Start Time (ms): %d, Check To Start/Re-Start: %b, " +
												  "Is Original Primary: %b";
	private static final String HA_LICENSE_PROD_ALERT_MSG_SUFFIX = " instance is running in HA setup which is not entitled.";
	private static final String HA_LICENSE_NON_PROD_ALERT_MSG_SUFFIX = 
									" instance usage is severely restricted untill used in Non HA setup!!!";
	private static final String USER_COUNT_LICENSE_PROD_ALERT_MSG_SUFFIX = 
										" instance is running with number of users in excess of entitled number of users.";
	private static final String USER_COUNT_LICENSE_NON_PROD_ALERT_MSG_SUFFIX =
										" instance usage is severely restricted as number of users in excess of " +
										"entitled number of users.";
	
	public static long endUsersCountViolationTime = 0;
	public static long endUsersCountViolationAlertTime = 0;
	public static long adminUsersCountViolationTime = 0;
	public static long coresCountViolationTime = 0;
	public static long coresCountViolationTimeAlertTime = 0;
	public static long maxMemoryViolationTime = 0;
	public static long maxMemoryViolationTimeAlertTime = 0;
	public static final Map<String, Long> gatewayAlertTime = new HashMap<String, Long>();
	
	private boolean master = false;
	private String serviceId = null;
	private boolean active = true;
	private boolean originalPrimary = true;
	private long lastHeartbeat = 0;
	private License license;
	private final long startTimeInMillis;
	private long lastLicenseLoadTimeInMillis = 0l;
	private boolean isHAViolated = false;
	private boolean isUserCountViolated = false;

	// store running component info as Key:GUID, Value:TYPE#IP
	// Key:8AE1043EB377A3B2AEB32427DDFAD12F, Value: RSREMOTE#192.168.1.1
	private ConcurrentHashMap<String, String> runningComponents = new ConcurrentHashMap<String, String>();
	// store the IP of the component that registers, convenient to check IP for
	// CE license
	private ConcurrentHashMap<String, String> runningComponentIps = new ConcurrentHashMap<String, String>();

	/*
	 * Keeps track of allowed number of Gateways in case we have 'ANY' number of licenses.
	 * In case, there are more than 1 license files, this variable will store the max number.
	 */
	protected int allowedNumber = 0;

	/**
	 * This constructor is mainly called by RSMGMT for uploading license during install.
	 *
	 * NOTE: Do not ever call this where the startFileLocation is other that the default (e.g., rsview).
	 *
	 * @param serviceId
	 */
	public LicenseService(String serviceId)
	{
		this(serviceId, MainBase.main.getProductHome() + "/log/tm");
	}

	public LicenseService(String serviceId, String startFileLocation)
	{
		this.serviceId = serviceId;
		long startDate = System.currentTimeMillis();
		
		try
		{
			File startDateFile = new File(startFileLocation);
			if (startDateFile.exists())
			{
				String content = CryptUtils.decrypt(FileUtils.readFileToString(startDateFile, StandardCharsets.UTF_8.name()));
				if (StringUtils.isNotBlank(content))
				{
					startDate = Long.parseLong(content);
					Log.log.trace("tm: " + content);
				}
			}
			else
			{
				// create start date file
				FileUtils.writeStringToFile(startDateFile, CryptUtils.encrypt("" + startDate), StandardCharsets.UTF_8.name());
			}
		}
		catch (Exception e)
		{
			Log.log.error(e.getMessage(), e);
		}
		startTimeInMillis = startDate;
	}

	public String getServiceId()
	{
		return serviceId;
	}

	public void setServiceId(String serviceId)
	{
		this.serviceId = serviceId;
	}

	public boolean isActive()
	{
		return active;
	}

	public void setActive(boolean active)
	{
		this.active = active;
	}

	public boolean isPrimary()
	{
		return master;
	}

	public void setPrimary(boolean primary)
	{
		this.master = primary;
	}

	public boolean isOriginalPrimary()
	{
		return originalPrimary;
	}

	public void setOriginalPrimary(boolean originalPrimary)
	{
		this.originalPrimary = originalPrimary;
	}

	public long getLastHeartbeat()
	{
		return lastHeartbeat;
	}

	public void setLastHeartbeat()
	{
		this.lastHeartbeat = System.currentTimeMillis();
	}

	public License getLicense()
	{
		return license;
	}

	public void setLicense(License license)
	{
		this.license = license;
	}

	public long getStartTimeInMillis()
	{
		return startTimeInMillis;
	}

	/**
	 * Verifies if the System clock is set backward. In that case we need to
	 * make sure that the heart beat counter is reset as well. Otherwise it will
	 * fall behind and not send hearbeat until system time catch up to it..
	 */
	public void checkSystemClockReset()
	{
		if (lastHeartbeat > System.currentTimeMillis())
		{
			setLastHeartbeat();
			Log.log.debug("HEARTBEAT RESET: Due to system clock set backward, lastHeartBeat is reset to " + lastHeartbeat);
		}
	}

	public void start()
	{
		loadLicense(null);
	}

	public void reinitialize()
	{
		setActive(true);
		setPrimary(true);
	}

	public void deactivate()
	{
		setActive(false);
		setPrimary(false);
	}

	/**
	 * Read the available licenses and filter deleted, copy pasted license.
	 *
	 * @return
	 */
	public List<License> getAvailableLicenses(boolean includeExpired)
	{
		SortedSet<License> srtdLicenses = new TreeSet<License>();
		List<License> result = new ArrayList<License>();

		try
		{
			File[] licenseFiles = LicenseUtil.getLicenseFiles();
			if (licenseFiles != null && licenseFiles.length > 0)
			{
				for (File licenseFile : licenseFiles)
				{
					License license = loadLicenseByFile(licenseFile);
					if (license != null)
					{
						if(!license.isDeleted())
						{
							if(license.isOldLicense() && !license.isExpired())
							{
								Log.log.debug("License added (legacy) : " + license.toString());
								srtdLicenses.add(license);
							}
							else if (license.getUploadDate() > 0 && (!license.isExpired() || includeExpired))
							{
								Log.log.debug("License added (current) : " + license.toString());
								srtdLicenses.add(license);
							}
						}
						else
						{
							Log.log.debug("Deleted license : " + license.toString());
						}
					}
				}
			}
		}
		catch (Exception e)
		{
			Log.log.error(e.getMessage(), e);
		}
		
		result.addAll(srtdLicenses);
		
		return result;
	}

	/**
	 * Read the available expired undeleted V2 licenses.
	 *
	 * @return
	 */
	public List<License> getAvailableExpiredUnDeletedV2Licenses()
	{
		SortedSet<License> srtdLicenses = new TreeSet<License>();
		List<License> result = new ArrayList<License>();

		try
		{
			File[] licenseFiles = LicenseUtil.getLicenseFiles();
			if (licenseFiles != null && licenseFiles.length > 0)
			{
				for (File licenseFile : licenseFiles)
				{
					License license = loadLicenseByFile(licenseFile);
					if (license != null)
					{
						if(!license.isDeleted())
						{
							if ((LicenseEnum.LICENSE_TYPE_V2 == 
								 LicenseEnum.valueOf(License.LICENSE_TYPE_PREFIX + license.getType().toUpperCase())) &&
								license.isExpired()) {
								Log.log.debug("Adding license to list of expired undeleted V2 licenses : " + 
											  license.toString());
								srtdLicenses.add(license);
							}
						}
						else
						{
							Log.log.debug("Deleted license : " + license.toString());
						}
					}
				}
			}
		}
		catch (Exception e)
		{
			Log.log.error(e.getMessage(), e);
		}
		
		result.addAll(srtdLicenses);
		
		return result;
	}
	
	public License createEvaluationLicense(long startTimeInMillis)
	{
		License result = new License(startTimeInMillis);
		String key = Guid.getGuid();
		result.setKey(key);
		result.setCheck("RESOLVE SYSTEMS");
		result.setCreateDate(System.currentTimeMillis());
		result.setUploadDate(startTimeInMillis);
		result.getLicenseKeys().put(key, "");

		result.setType(LicenseEnum.LICENSE_TYPE_EVALUATION.getKeygenName());
		result.setCustomer("Evaluation User");
		// Evaluation has no limit on memory
		result.getMaxMemory().put(LicenseEnum.COMPONENT_RSCONTROL.getKeygenName(), 0);
		//result.getMaxMemory().put(LicenseUtil.COMPONENT_RSREMOTE, 0);
		result.getMaxMemory().put(LicenseEnum.COMPONENT_RSVIEW.getKeygenName(), 0);

		result.setCores(0);
		result.setHA(true);
		result.setDuration(60); //Eval is 60 and CE is 90 days
		// by default this should have the community edtion stuff
		result.setEndUserCount(0);

		result.setStartTimeInMillis(startTimeInMillis);
		result.setExpirationDate(result.getExpirationDate());

		//in evaluation edition, all single instances of all gateways are active
		result.getGateway().put("*", new GatewayLicense("*", 0, result.getDuration(), result.getExpirationDate(), startTimeInMillis));
		return result;
	}

	public void loadLicense(License newLicense)
	{
		// load license only if difference between last load time and current time is 25 minutes or more
		
		if (ChronoUnit.MINUTES.between(Instant.ofEpochMilli(lastLicenseLoadTimeInMillis), Instant.now()) < 25L) {
			return;
		} else {
			lastLicenseLoadTimeInMillis = System.currentTimeMillis();
		}
		
		License result = new License(startTimeInMillis);

		try
		{
			List<License> availableLicenses = getAvailableLicenses(false);
			Log.log.debug(String.format("Local License Repository getAvailableLicenses(unexpired) returned %d licenses.", 
										availableLicenses.size()));
			// If no valid (not expired) licenses found then get all expired licenses
			availableLicenses = CollectionUtils.isEmpty(availableLicenses) ? 
								getAvailableExpiredUnDeletedV2Licenses() : availableLicenses;
			Log.log.info(String.format("Local License Repository getAvailableLicenses(unexpired) or " +
									   "(expired undeleted V2 licenses if unexpired count was 0 previously) returned %d " +
									   "licenses.", availableLicenses.size()));
			// If latest deployed license is V2 and expired then load the expired V2 license
			if (CollectionUtils.isNotEmpty(availableLicenses)) {
				License lastLic = availableLicenses.get(availableLicenses.size() - 1);
				
				if ((LicenseEnum.LICENSE_TYPE_V2 == 
					 LicenseEnum.valueOf(License.LICENSE_TYPE_PREFIX + lastLic.getType().toUpperCase())) &&
					lastLic.isExpired()) {
					availableLicenses.clear();
					availableLicenses.add(lastLic);
					Log.log.info("latest uploaded V2 license with key " + lastLic.getKey() + 
								 " is expired, so keep just that one.");
				}
			}
			
			if (CollectionUtils.isEmpty(availableLicenses) && newLicense != null) {
				/*
				 * For some odd reasons if saving of new license into license repository failed 
				 * earlier then load new license passed if it is V1 and not expired or is V2.
				 */
				
				if ((!newLicense.isV2() && !newLicense.isExpired()) || newLicense.isV2()) {
					availableLicenses.clear();
					availableLicenses.add(newLicense);
				}
			}
			
			boolean isBootStrapEvalLic = false;
			
			// If no licenses found then create evaluation license which expires 60 days from first use 
			if (CollectionUtils.isEmpty(availableLicenses))
			{
				availableLicenses.add(createEvaluationLicense(startTimeInMillis));
				isBootStrapEvalLic = true;
				Log.log.info(String.format("Created Boot strapped license with license key %s. Boot strapped License Info [%s]", 
										   availableLicenses.get(0).getKey(), availableLicenses.get(0)));
			}
			
			if (CollectionUtils.isNotEmpty(availableLicenses))
			{
				boolean expiredV2License = availableLicenses.size() == 1 && 
										   (LicenseEnum.LICENSE_TYPE_V2 == 
										    LicenseEnum.valueOf(License.LICENSE_TYPE_PREFIX + 
												  			    availableLicenses.get(0).getType().toUpperCase())) &&
										   availableLicenses.get(0).isExpired() ? true : false;
				
				//copy already loaded gateways from the current license, if any.
				Map<String, GatewayLicense> gatewayLicenses = new HashMap<String, GatewayLicense>();
				if(this.license != null && !this.license.isEvaluation())
				{
					for(GatewayLicense gatewayLicense : this.license.getAllGatewayLicenses())
					{
						gatewayLicenses.put(gatewayLicense.getCode(), gatewayLicense);
					}
				}

				for (License availableLicense : availableLicenses)
				{
					result.append(availableLicense);
				}

				// V2 license entitlements are not cumulative including gateways
				
				if (StringUtils.isNotBlank(result.getKey()) &&
					LicenseEnum.LICENSE_TYPE_V2 != 
					LicenseEnum.valueOf(License.LICENSE_TYPE_PREFIX + result.getType().toUpperCase())) {
						for(GatewayLicense gatewayLicense : result.getAllGatewayLicenses()) {
						if(gatewayLicenses.containsKey(gatewayLicense.getCode()))
						{
							result.setGatewayLicense(gatewayLicense.getCode(), 
													 gatewayLicenses.get(gatewayLicense.getCode()));
						}
					}
				}
				
				/*
				 * None of the licenses could become effective due to future effective start date so 
				 * first attempt to get expired V2 licenses and if none then create 
				 * bootstrap EVALUATION license.
				 */
				
				if (StringUtils.isBlank(result.getKey()) && !isBootStrapEvalLic) {
					License finalAttemptLic = null;
					
					if (!expiredV2License) {
						availableLicenses = getAvailableExpiredUnDeletedV2Licenses();
						
						if (CollectionUtils.isNotEmpty(availableLicenses)) {
							finalAttemptLic = availableLicenses.get(availableLicenses.size() - 1);
						}
					}
					
					if (finalAttemptLic == null) {
						finalAttemptLic = createEvaluationLicense(startTimeInMillis);
					}
					
					if (finalAttemptLic != null) {
						Log.log.info(String.format("Appending Final Attempt License with license key %s. " +
												   "Final Attemp License Info [%s].", finalAttemptLic.getKey(), 
												   finalAttemptLic));
						result.append(finalAttemptLic);
					}
				}
			}
			
			if (StringUtils.isBlank(result.getKey())) {
				/*
				 * Check if new license has been passed and is valid.
				 * For some odd reasons if new license did not get saved in the repository
				 */
				// Failed to apply any un-expired or v2 expired or bootstrap eval license shutdown
				Log.log.fatal("Failed to apply any unexpired or V2 expired or bootstrap license!!!");
				alert(ERR.E10001);
				System.exit(1);
			}
			
			this.license = result;
		}
		catch (Exception e)
		{
			Log.log.error(e.getMessage(), e);
		}
	}

	/**
	 * This method validates and append a new license to the existing license
	 * (if any). Otherwise it simply keep this license as
	 *
	 * @param params
	 */
	public boolean uploadLicense(Map<String, String> params)
	{
		boolean success = true;
		License currentLicense = getLicense();
		try
		{
			String encryptedLicenseContent = params.get(Constants.LICENSE_CONTENT);
			
			// Filter out signature if present
			String[] licenseParts = encryptedLicenseContent.split(LicenseEnum.SIGNATURE.getKeygenName(), 2);
			// decrypt license
			String licenseUnencrypted = CryptUtils.decryptLicense(licenseParts[0]);
			String licenseSignature = licenseParts.length == 2 ? licenseParts[1] : null;
			
			//add upload date
			licenseUnencrypted += License.NEW_LINE + LicenseEnum.UPLOAD_DATE.getKeygenName() + License.EQUAL + 
								  Calendar.getInstance().getTimeInMillis();

			// encrypt again since we've added the upload date, we will
			// store that info in the license file
			String encryptedLicense = CryptUtils.encryptLicense(licenseUnencrypted);

			// Append the signature (if present) to updated license
			if (StringUtils.isNotBlank(licenseSignature)) {
				encryptedLicense += LicenseEnum.SIGNATURE.getKeygenName() + licenseSignature;
			}
			
			License newLicense = new License(encryptedLicense, startTimeInMillis);
			Log.log.info(String.format("License with license key %s being uploaded : License Info [%s]",
									   newLicense.getKey(), newLicense));

			License storedLicense = getLicenseByKey(newLicense.getKey());
			
			// Store new license in local license repository only if not found
			if (storedLicense == null) {
				// Store new license in local license repository only if not found
				Log.log.debug(String.format("Current Effective License: [%s]", currentLicense));
				
				// save the new license with its key as the part of the name
				saveLicenseFile(newLicense.getKey() + ".lic", encryptedLicense);				
				
				// delete old license.
				deleteOldLicenseFile();
			} else {
				Log.log.info(String.format("License with license key %s being uploaded is already present in local " +
										   "license repository. License Info in Local License Repository [%s]", 
										   newLicense.getKey(), storedLicense));
			}
			
			// Reset last license loaded time in ms to reload licenses to get effective license
			lastLicenseLoadTimeInMillis = 0l;
			//load latest license
			loadLicense(params.containsKey(Constants.HTTP_ATTRIBUTE_RSVIEW_ID_GUID) ? null : newLicense);
			Log.log.debug(String.format("Current Effective License after loading/re-loading licenses in local license " +
										"repository: %s", getLicense()));
			
			/*
			 * If first component in node to receive request then publish request for rest of components 
			 * in current node and all other nodes and components within those nodes (in HA setup).
			 */
			if (!params.containsKey(Constants.HTTP_ATTRIBUTE_RSVIEW_ID_GUID)) {
				// Publish 
				params.put(Constants.HTTP_ATTRIBUTE_RSVIEW_ID_GUID, MainBase.main.configId.guid);
				Log.log.debug("Publishing Upload License Key message..." );
				MainBase.getESB().sendMessage(Constants.ESB_NAME_RSLICENSES,"MLicense.upload", params);
			}
		}
		catch (Exception e)
		{
			Log.log.error(e.getMessage(), e);
			success = false;
		}
		
		return success;
	}

	/**
	 * This method simply saves a license mainly used by rsmgmt.
	 *
	 * @param params
	 * 
	 * @deprecated  As of release 6.3, replaced by {@link #uploadLicense()}
	 */
	@Deprecated
	public void saveLicense(Map<String, String> params)
	{
		try
		{
			String encryptedLicenseContent = params.get(Constants.LICENSE_CONTENT);
			
			// Filter out signature if present
			String[] licenseParts = encryptedLicenseContent.split(LicenseEnum.SIGNATURE.getKeygenName(), 2);
			// decrypt license
			String licenseUnencrypted = CryptUtils.decryptLicense(licenseParts[0]);
			String licenseSignature = licenseParts.length == 2 ? licenseParts[1] : null;
			
			// add upload date
			licenseUnencrypted += License.NEW_LINE + LicenseEnum.UPLOAD_DATE.getKeygenName() + License.EQUAL + 
								  Calendar.getInstance().getTimeInMillis();

			// encrypt again since we've added the upload date, we will
			// store that info in the license file
			String encryptedLicense = CryptUtils.encryptLicense(licenseUnencrypted);
			
			// Append the signature (if present) to updated license
			if (StringUtils.isNotBlank(licenseSignature)) {
				encryptedLicense += LicenseEnum.SIGNATURE.getKeygenName() + licenseSignature;
			}
						
			License newLicense = new License(encryptedLicense, startTimeInMillis);
			Log.log.debug("License being saved :" + newLicense.toString());

			License existingLicense = getLicenseByKey(newLicense.getKey());

			if (existingLicense == null)
			{
				Log.log.debug("New license: " + newLicense.toString());
				// save the new license with its key as the part of the name
				saveLicenseFile(newLicense.getKey() + ".lic", encryptedLicense);
			}
		}
		catch (Exception e)
		{
			Log.log.error(e.getMessage(), e);
		}
	}

	/**
	 * This is a special case where old license needs to be deleted if there is a new license available.
	 *
	 * @param licenseType
	 * @throws Exception
	 */
	private void deleteOldLicenseFile() throws Exception
	{
		File[] licenseFiles = LicenseUtil.getLicenseFiles();
		
		File oldLicenseFile = Arrays.asList(licenseFiles)
							  .parallelStream()
							  .filter(licenseFile -> licenseFile.getName().equalsIgnoreCase("license.lic"))
							  .findAny()
							  .orElse(null);
		
		if (oldLicenseFile != null && oldLicenseFile.isFile() && oldLicenseFile.exists()) {
			licenseFileMarkAsDeleted("license");
		}
	}

	/**
	 * Synchronizes the license files. Files are received from another Resolve component.
	 *
	 * @param params
	 *            keys are the license file name from another RSMGMT
	 */
	public void synchronizeLicense(Map<String, String> params)
	{
		if (MapUtils.isNotEmpty(params)) {
			String licensesFrom = params.remove(LicenseEnum.LICENSES_FROM.getKeygenName());
			Log.log.info(String.format("Received %d licenses from %s to synchronize.", params.size(),
									   (StringUtils.isNotEmpty(licensesFrom) ? licensesFrom : "anonymous component")));
			String resolveHome = MainBase.main.getResolveHome();
			for (String key : params.keySet())
			{
				File file = FileUtils.getFile(resolveHome + "/" + key);
				if (!file.exists())
				{
					Map<String, String> licenseContent = new HashMap<String, String>();
					licenseContent.put(Constants.LICENSE_CONTENT, params.get(key));
					Log.log.info(String.format("Uploading received license with license key [%s].", key));
					uploadLicense(licenseContent);
				}
			}
		}
	}

	/**
	 * Removes a set of licenses.
	 *
	 * @param keySet
	 */
	public void removeLicense(Map<String, String> params)
	{
		try
		{
			for (String key : params.keySet())
			{
				if (!key.equals(Constants.HTTP_ATTRIBUTE_RSVIEW_ID_GUID)) {
					licenseFileMarkAsDeleted(key);
				}
			}
			
			// Reset last license loaded time in ms to reload licenses after removal
			lastLicenseLoadTimeInMillis = 0l;
			
			// once the licenses are removed reload the remaining license(s)
			loadLicense(null);
			Log.log.debug("License after remove: " + license);
			
			/*
			 * If first component in node to receive request then publish request for rest of components 
			 * in current node and all other nodes and components within those nodes (in HA setup).
			 */
			if (!params.containsKey(Constants.HTTP_ATTRIBUTE_RSVIEW_ID_GUID)) {
				params.put(Constants.HTTP_ATTRIBUTE_RSVIEW_ID_GUID, MainBase.main.configId.guid);
				Log.log.debug("Publishing Remove License Key(s) message..." );
				MainBase.getESB().sendMessage(Constants.ESB_NAME_RSLICENSES,"MLicense.remove", params);
			}
		}
		catch (Exception e)
		{
			Log.log.error(e.getMessage(), e);
		}
	}

	/**
	 * If a Resolve Component wants to synchronize its license store then it braodcasts a
	 * request. In turn other running components sends the licenses from its store
	 * to the requester. Eventually all the components get synchronized.
	 *
	 * @param params
	 */
	public void sendLicense(Map<String, String> params)
	{
		if (params.containsKey(LicenseEnum.RETURN_QUEUE.getKeygenName()) && 
			StringUtils.isNotBlank(params.get(LicenseEnum.RETURN_QUEUE.getKeygenName())))
		{
			if (!MainBase.main.configId.getGuid().equals(params.get(LicenseEnum.RETURN_QUEUE.getKeygenName())))
			{
				Map<String, String> licenses = new HashMap<String, String>();
				File[] licenseFiles = LicenseUtil.getLicenseFiles();
				for (File licenseFile : licenseFiles)
				{
					if (!licenseFile.getName().contains("license")) {
						licenses.put(licenseFile.getName(), LicenseUtil.readLicenseFile(licenseFile));
					}
				}
				
				if (MapUtils.isNotEmpty(licenses)) {
					Log.log.info(String.format("%s sending licenses with keys [%s to compoent with id %s...", 
											   MainBase.main.configId.getDescription() + "-" + 
											   MainBase.main.configId.getGuid(), 
											   StringUtils.collectionToString(licenses.keySet(), ", "),
											   params.get(LicenseEnum.RETURN_QUEUE.getKeygenName())));
					licenses.put(LicenseEnum.LICENSES_FROM.getKeygenName(), 
								 MainBase.main.configId.getDescription() + "-" + MainBase.main.configId.getGuid());
					
					if (MainBase.esb.sendInternalMessage(params.get(LicenseEnum.RETURN_QUEUE.getKeygenName()), 
														 "MLicense.synchronizeLicense", licenses) == false)
					{
						Log.log.warn("Failed to send synchronization message back to " + 
									 params.get(LicenseEnum.RETURN_QUEUE.getKeygenName()));
					}
					//send gateway instances
					if(getLicense().getAllGatewayLicenses().size() > 0)
					{
						HashSet<GatewayLicense> processedLicenses = new HashSet<GatewayLicense>();
	
						for(GatewayLicense gatewayLicense : getLicense().getAllGatewayLicenses())
						{
							if (processedLicenses.contains(gatewayLicense))
								continue;
	
							processedLicenses.add(gatewayLicense);
	
							if(MapUtils.isNotEmpty(gatewayLicense.getRunningInstancesNew()))
							{
								Map<String, Map<String, String>> instances = gatewayLicense.getRunningInstancesNewAsMap();
								for(Map<String, String> instance : instances.values())
								{
									//Send it if gateway instance is running on current server
									if (instance.get("INSTANCE_ID").equals(MainBase.main.configId.getGuid()))
									{
										sendGatewayRegistration(instance.get("GATEWAY_CODE"), instance.get("INSTANCE_ID"), instance.get("INSTANCE_TYPE"), instance.get("QUEUE"), instance.get("STARTTIME"));
									}
								}
							}
						}
					}
				}

				// also send it's presence so the other component know about it.
				sendLicenseRegistration(params.get(LicenseEnum.RETURN_QUEUE.getKeygenName()));
			}
		}
	}

	public void sendLicenseRegistration(String queue)
	{
		// broadcast the presense to other components
		Map<String, String> params = new HashMap<String, String>();
		params.put(LicenseService.GUID, MainBase.main.configId.getGuid());
		params.put(LicenseService.DESCRIPTION, MainBase.main.configId.getDescription());
		params.put(LicenseService.IP, MainBase.main.configId.getLocation());
		Log.log.debug("Broadcasting component availability message");
		if (MainBase.esb.sendInternalMessage(queue, "MLicense.registerComponent", params) == false)
		{
			Log.log.warn("Failed to send component availability message to " + queue);
		}
	} // sendLicenseRegistration

	public void sendLicenseRegistration()
	{
		sendLicenseRegistration(Constants.ESB_NAME_RSLICENSES);
	} // sendLicenseRegistration

	public void sendGatewayRegistration(String gatewayCode, String instanceId, String instanceType, String gatewayQueueName, String startTime)
	{
		Map<String, String> params = new HashMap<String, String>();
		String value = "INSTANCE_ID|=|" + instanceId + "|&|INSTANCE_TYPE|=|" + instanceType + "|&|QUEUE|=|" + gatewayQueueName + "|&|STARTTIME|=|" + startTime;
		params.put(gatewayCode, value);
		//so that the message is not replayed over and over again
		params.put("SENDERID", MainBase.main.configId.getGuid());
		if (MainBase.esb.sendInternalMessage(Constants.ESB_NAME_RSLICENSES, "MLicense.registerGateway", params) == false)
		{
			Log.log.warn("Failed to send gateway registration message with parameters " + params);
		}
	}

	public Map<String, String> getHostIPAddress(Map<String, String> params)
	{
		Map<String, String> result = new HashMap<String, String>();
		if (isPrimary())
		{
			try
			{
				result.put(LicenseEnum.HOST_IP_ADDRESS.getKeygenName(), SystemUtil.getHostIPAddress());
			}
			catch (Exception e)
			{
				Log.log.error(e.getMessage(), e);
			}
		}
		return result;
	}

	public License getCurrentLicense()
	{
		return getLicense();
	}

	/**
	 * This is the method that is used when every component (RSCONTROL, RSVIEW,
	 * RSREMOTE) announces its presence to the environment.
	 *
	 * @param params
	 *            with TYPE, GUID and IP
	 */
	public void registerComponent(Map<String, String> params)
	{
		Log.log.debug("Registering component: " + params);
		runningComponents.put(params.get(GUID), params.get(DESCRIPTION) + "#" + params.get(IP));
		runningComponentIps.put(params.get("IP"), params.get(DESCRIPTION));
		Log.log.debug("Existing component IPs: " + runningComponentIps.keySet());
	}

	private int getComponentCount(String componentType)
	{
		int result = 0;

		for(String type : runningComponents.values())
		{
			if(type.toLowerCase().contains(componentType.toLowerCase()))
			{
				result++;
			}
		}

		return result;
	}

	/**
	 * Call by products like rscontrol, rsview /etc. This method checks for IP
	 * Addresses, CPU and expiration date.
	 *
	 * @param resolveHome
	 * @param startDateFile
	 * @return
	 */
	public boolean checkLicense()
	{
		boolean licenseCheckedOut = false;
		try
		{
			Log.log.info("*** VALIDATING LICENSE ***");

			// check DIST dir
			if (license != null)
			{
				Log.log.info(license.toString());

				// verify check
				String check = license.getCheck();
				if (CHECKCODE.equalsIgnoreCase(check))
				{
					/*
					 * On license check performed every 30 minutes, re-load to make v2 licenses uploaded in past
					 * with future start date effective. This avoids restarting of components which use license
					 * viz. rsview, rscontrol, and rsremote.  
					 */
					
					loadLicense(null);
					
					/*
					 * On loading license always reset License Service HA violated flag to false, 
					 * preceding HA checks will set it back if HA is still violated.
					 */
					setIsHAViolated(false);
					licenseCheckedOut = true;
					
					if (license.isExpired()) {
						if (LicenseEnum.LICENSE_TYPE_V2 != 
							LicenseEnum.valueOf(License.LICENSE_TYPE_PREFIX + license.getType().toUpperCase())) {
							alert(ERR.E10001);
							System.exit(1);
						} else {
							String alertMsg = EXPIRED_LICENSE_ALERT_MSG_PREFIX + " " + MainBase.main.configGeneral.getEnv() +
											  (LicenseEnum.LICENSE_ENV_PRODUCTION.getKeygenName()
											   .equalsIgnoreCase(license.getEnv()) ?
											   EXPIRED_LICENSE_PROD_ALERT_MSG_SUFFIX : 
											   EXPIRED_LICENSE_NON_PROD_ALERT_MSG_SUFFIX);
							
							Log.alert(ERR.E10001.getCode(), alertMsg, Constants.ALERT_TYPE_LICENSE);
							licenseCheckedOut = false;
						}
					}
					
					//check the cores for V1 only, if violated then system will run for 5 days and shutdown
					if (licenseCheckedOut && 
						LicenseEnum.LICENSE_TYPE_V2 != 
						LicenseEnum.valueOf(License.LICENSE_TYPE_PREFIX + license.getType().toUpperCase())) {
						licenseCheckedOut = checkCores(license, SystemUtil.getCPUCount());
					}
					
					// now check if there is IP restriction for V1 license only.
					if (licenseCheckedOut &&
						LicenseEnum.LICENSE_TYPE_V2 != 
						LicenseEnum.valueOf(License.LICENSE_TYPE_PREFIX + license.getType().toUpperCase()) &&
						license.getIpAddresses().size() > 0)
					{
						String resolveHostIPAddress = SystemUtil.getHostIPAddress();
						if (StringUtils.isNotBlank(resolveHostIPAddress))
						{
							boolean isIPFound = false;
							for(String key : license.getIpAddresses().keySet())
							{
								if (resolveHostIPAddress.contains(key))
								{
									isIPFound = true;
									break;
								}
							}
							if (!isIPFound)
							{
								Log.log.debug("Resolve host IP mismatched, IP(s) allowed by License: " + license.getIpAddresses() + ", but found: " + resolveHostIPAddress);
								alert(ERR.E10006);
								System.exit(1);
							}
						}
					}

					// check the Host IP address
					if (licenseCheckedOut && 
						LicenseEnum.LICENSE_TYPE_COMMUNITY.getKeygenName().equalsIgnoreCase(license.getType()))
					{
						// we will check the registered component for IP
						// every component send registration message to every
						// other
						// component. Eventually this restriction will be
						// enforced because in the
						// second iteration we'll have the runningComponentIps
						// map loaded
						// with all the components.
						if (runningComponentIps.size() > 1)
						{
							alert(ERR.E10008);
							System.exit(1);
						}
					}

					//check High Availability (HA) criteria
					if(licenseCheckedOut && !license.isHA() && !license.isOldLicense())
					{
						if (Log.log.isDebugEnabled()) {
							Log.log.debug(String.format("Registered unique %ss:%d, %ss:%d", 
														LicenseEnum.COMPONENT_RSCONTROL.getKeygenName(),
														getComponentCount(LicenseEnum.COMPONENT_RSCONTROL.getKeygenName()),
														LicenseEnum.COMPONENT_RSVIEW.getKeygenName(),
														getComponentCount(LicenseEnum.COMPONENT_RSVIEW.getKeygenName())));
						}
						
						if(getComponentCount(LicenseEnum.COMPONENT_RSCONTROL.getKeygenName()) > 1 || 
						   getComponentCount(LicenseEnum.COMPONENT_RSVIEW.getKeygenName()) > 1 )
						{
							licenseCheckedOut = false;
							if (LicenseEnum.LICENSE_TYPE_V2 != 
								LicenseEnum.valueOf(License.LICENSE_TYPE_PREFIX + license.getType().toUpperCase())) {
								alert(ERR.E10020);
								System.exit(1);
							} else {
								setIsHAViolated(true);
								String alertMsg = ERR.E10029.getMessage() + " " + MainBase.main.configGeneral.getEnv() +
										  (LicenseEnum.LICENSE_ENV_PRODUCTION.getKeygenName()
										   .equalsIgnoreCase(license.getEnv()) ?
										   HA_LICENSE_PROD_ALERT_MSG_SUFFIX : 
										   HA_LICENSE_NON_PROD_ALERT_MSG_SUFFIX);
								
								Log.alert(ERR.E10029.getCode(), alertMsg, Constants.ALERT_TYPE_LICENSE);
							}
						}
					}
				}
			}
			else
			{
				Log.log.info("Missing license file");
			}
		}
		catch (Exception e)
		{
			Log.log.info(e.getMessage());
		}

		return licenseCheckedOut;

	} // checkLicense

	private static void alert(ERR err)
	{
		alert(err, err.getMessage());
	}

	private static void alert(ERR err, String message)
	{
		Log.alert(err.getCode(), err.getMessage() + " " + message, Constants.ALERT_TYPE_LICENSE);
	}

	private boolean checkCores(License license, int numberOfActualCores)
	{
		if (license == null || 
			(LicenseEnum.LICENSE_TYPE_V2 == 
				 LicenseEnum.valueOf(License.LICENSE_TYPE_PREFIX + license.getType().toUpperCase()))) {
			return true;
		}
		
		int allowedCores = license.getCores();

		long currentTime = System.currentTimeMillis();

		boolean coresCheckedOut = true;
		// endUsers=0 mean no restriction.
		if (allowedCores > 0 && numberOfActualCores > allowedCores)
		{
			// the count down for 2 weeks begins.
			if (coresCountViolationTime == 0)
			{
				coresCountViolationTime = currentTime;
			}

			if ((currentTime - coresCountViolationTime) > twoWeeksInMillis)
			{
				alert(ERR.E10007, "Shutting Down");
				System.exit(1);
			}
			else
			{
				long timePastSinceLastViolation = currentTime - coresCountViolationTime;
				long timeRemaining = twoWeeksInMillis - timePastSinceLastViolation;
				long remainingDays = (((timeRemaining / 1000) / 60) / 60) / 24L;
				//if it's 24 hours since last alert, then send it again
				Long timeElapsedSinceLastAlert = currentTime - coresCountViolationTimeAlertTime;
				if(timeElapsedSinceLastAlert > oneDayInMillis)
				{
					alert(ERR.E10007, String.format(RESOLVE_SHUTDOWN_WARNING, remainingDays, license.getKey()));
					coresCountViolationTimeAlertTime = currentTime;
				}
				
				coresCheckedOut = false;
			}
		}
		
		return coresCheckedOut;
	}
	
	public void checkMaximumMemory(MainBase mainBase)
	{
		String compName = StringUtils.substringBefore(
								StringUtils.substringAfter(mainBase.getClass().getPackage().getName(), 
													   	   RESOLVE_PACKAGE_PREFIX), 
								PACKAGE_SEPARATOR);
		
		if (StringUtils.isBlank(compName)) {
			return;
		}
		
		if (license == null || 
			(LicenseEnum.LICENSE_TYPE_V2 == 
			 LicenseEnum.valueOf(License.LICENSE_TYPE_PREFIX + license.getType().toUpperCase())) ||
			!license.getMaxMemory().containsKey(compName.toUpperCase())) {
			return;
		}
		
		Integer configuredMaximumMemory = license.getMaxMemory().get(compName.toUpperCase());
		
		long currentTime = System.currentTimeMillis();

		if (configuredMaximumMemory != null && configuredMaximumMemory > 0)
		{
			long configuredMaximumMemoryInBytes = configuredMaximumMemory * 1024 * 1024;
			long maximumMemory = SystemUtil.getMaxMemoryHeap();
			if (maximumMemory > configuredMaximumMemoryInBytes)
			{
				// the count down for 2 weeks begins.
				if (maxMemoryViolationTime == 0)
				{
					maxMemoryViolationTime = currentTime;
				}

				if ((currentTime - maxMemoryViolationTime) > twoWeeksInMillis)
				{
					alert(ERR.E10009, "Shutting Down");
					System.exit(1);
				}
				else
				{
					long timePastSinceLastViolation = currentTime - maxMemoryViolationTime;
					long timeRemaining = twoWeeksInMillis - timePastSinceLastViolation;
					long remainingDays = (((timeRemaining / 1000) / 60) / 60) / 24L;
					//if it's 24 hours since last alert, then send it again
					long timeElapsedSinceLastAlert = currentTime - maxMemoryViolationTimeAlertTime;
					if(timeElapsedSinceLastAlert > oneDayInMillis)
					{
						alert(ERR.E10009, 
							  String.format(RESOLVE_SHUTDOWN_WARNING, remainingDays, license.getKey()));
						maxMemoryViolationTimeAlertTime = currentTime;
					}
				}
			}
		}
	}

	public static boolean checkUsers(License license, int numberOfActualEndUsers)
	{
		boolean userCountCheckedOut = true;
		int allowedEndUsers = license.getEndUserCount();

		long currentTime = System.currentTimeMillis();

		// endUsers=0 mean no restriction.
		if (allowedEndUsers > 0 && numberOfActualEndUsers > allowedEndUsers)
		{
			userCountCheckedOut = false;
			// Non V2 check
			if (LicenseEnum.LICENSE_TYPE_V2 != 
				LicenseEnum.valueOf(License.LICENSE_TYPE_PREFIX + license.getType().toUpperCase())) {
				
				if (endUsersCountViolationTime == 0)
				{
					endUsersCountViolationTime = currentTime; // the count down for 14 days begins.
				}
				
				if ((currentTime - endUsersCountViolationTime) > twoWeeksInMillis)
				{
					alert(ERR.E10015, "Shutting Down");
					System.exit(1);
				}
				else
				{
					long timePastSinceLastViolation = currentTime - endUsersCountViolationTime;
					long timeRemaining = twoWeeksInMillis - timePastSinceLastViolation;
					long remainingDays = (((timeRemaining / 1000) / 60) / 60) / 24L;
					//if it's 24 hours since last alert, then send it again
					long timeElapsedSinceLastAlert = currentTime - endUsersCountViolationAlertTime;
					if(timeElapsedSinceLastAlert > oneDayInMillis)
					{
						alert(ERR.E10015, "Resolve will shutdown in " + remainingDays + " days if the violation left unresolved.");
						endUsersCountViolationAlertTime = currentTime;
					}
				}
			} else {
				String alertMsg = ERR.E10015.getMessage() + " " + MainBase.main.configGeneral.getEnv() +
						  (LicenseEnum.LICENSE_ENV_PRODUCTION.getKeygenName()
						   .equalsIgnoreCase(license.getEnv()) ? 
						   USER_COUNT_LICENSE_PROD_ALERT_MSG_SUFFIX : 
						   USER_COUNT_LICENSE_NON_PROD_ALERT_MSG_SUFFIX);
				
				Log.alert(ERR.E10015.getCode(), alertMsg, Constants.ALERT_TYPE_LICENSE);
			}
		}
		
		return userCountCheckedOut;
	}

	/**
	 * This method loads a license file based on its key.
	 *
	 * @return
	 */
	public License getLicenseByKey(String key)
	{
		License result = null;

		File licenseFile = FileUtils.getFile(MainBase.main.getResolveHome() + "/" + key + ".lic");
		if(licenseFile.exists())
		{
			result = loadLicenseByFile(licenseFile);
		}
		else
		{
			//may be the file name got changed, let's find inside the license
			List<License> licenses = getAvailableLicenses(true);
			for(License license : licenses)
			{
				if(key.equals(license.getKey()))
				{
					result = license;
					break;
				}
			}
		}
		return result;
	}

	/**
	 * This method loads a license file based on the license file name.
	 *
	 * @return
	 */
	public License loadLicenseByFile(File licenseFile)
	{
		License result = null;
		
		String licenseEncrypted = LicenseUtil.readLicenseFile(licenseFile);
		result = loadLicenseByEncryptedData(licenseEncrypted);

		return result;
	}


	/**
	 * This method loads a license based on the encrypted license string.
	 *
	 * @return
	 */
	public License loadLicenseByEncryptedData(String licenseEncrypted)
	{
		License result = null;
		try
		{
			result = new License(licenseEncrypted, startTimeInMillis);
		}
		catch (Exception e)
		{
			Log.log.warn(e.getMessage());
		}

		return result;
	}

	private void licenseFileMarkAsDeleted(String licenseKey) throws Exception
	{
		File licenseFile = FileUtils.getFile(MainBase.main.getResolveHome() + "/" + licenseKey + ".lic");
		licenseFileMarkAsDeleted(licenseFile);
	}

	private void licenseFileMarkAsDeleted(File licenseFile) throws Exception
	{
		File dir = FileUtils.getFile(MainBase.main.getResolveHome());
		if (dir != null && dir.isDirectory() && dir.exists())
		{
			Pair<String, String> licenseParts = LicenseUtil.decryptLicense(licenseFile);
			String licenseUnencrypted = licenseParts.getLeft();
			
			
			if(StringUtils.isNotBlank(licenseUnencrypted))
			{
				if (licenseUnencrypted.indexOf(LicenseEnum.DELETE_DATE.getKeygenName()) >= 0)
				{
					// ignore it as it's been already deleted.
					Log.log.debug("License has been deleted already, ignoring");
				}
				else
				{
					licenseUnencrypted += "\n" + LicenseEnum.DELETE_DATE.getKeygenName() + "=" + 
										  Calendar.getInstance().getTimeInMillis();
					Log.log.debug("Marking the License as deleted: " + licenseUnencrypted);
					
					saveLicenseFile(licenseFile.getName(), 
									LicenseUtil.encryptLicenseContent(licenseUnencrypted) + 
									(StringUtils.isNotBlank(licenseParts.getRight()) ? 
									 LicenseEnum.SIGNATURE.getKeygenName() + licenseParts.getRight() : ""));
				}
			}
		}
	}

	public Map<String, String> getLicenseInfo(String key)
	{
		Map<String, String> result = new LinkedHashMap<String, String>();

		License license = null;
		if (StringUtils.isBlank(key))
		{
			license = getLicense();
		}
		else
		{
			license = getLicenseByKey(key);
			if (license == null)
			{
				result.put("License File Found", "FALSE");
				// no need to continue as the license file not found
				return result;
			}
		}

		String daysLeft = "N/A";

		if (license.hasLicense())
		{
			Long expirationDate = license.getExpirationDate();

			if (expirationDate != null)
			{
				daysLeft = String.valueOf(TimeUnit.DAYS.convert(expirationDate, TimeUnit.MILLISECONDS) - TimeUnit.DAYS.convert(System.currentTimeMillis(), TimeUnit.MILLISECONDS));
				result.put("License Expiration Date", DateUtils.convertTimeInMillisToString(
																	expirationDate, 
																	LicenseEnum.DATE_FORMAT_LONG.getKeygenName()));
			}
			else
			{
				result.put("License Expiration Date", "No Expiration");
			}

			StringBuilder licensesKeys = new StringBuilder();
			for (String licenseKey : license.getLicenseKeys().keySet())
			{
				licensesKeys.append(licenseKey);
				licensesKeys.append(",");
			}
			if (licensesKeys.length() > 0)
			{
				licensesKeys.setLength(licensesKeys.length() - 1);
			}
			result.put("License Keys", licensesKeys.toString());
		}
		else
		{
			daysLeft = "" + license.getDaysLeft();
		}

		result.put("License Type", license.getType());
		result.put("License File Found", String.valueOf(license.hasLicense()).toUpperCase());
		result.put("Days Remaining", String.valueOf(daysLeft));
		result.put("License Created", DateUtils.convertDateToStringDEFAULT(new Date(license.getCreateDate())));
		String maxCores = ""+license.getCores();
		if(license.getCores() == 0)
		{
			maxCores = "N/A";
		}
		result.put("Maximum no. of Cores", maxCores);
		String memoryLimit = license.getMaxMemory().containsKey(LicenseEnum.COMPONENT_RSCONTROL.getKeygenName()) ? 
							 "" + license.getMaxMemory().get(LicenseEnum.COMPONENT_RSCONTROL.getKeygenName()) : "";
		if(StringUtils.isBlank(memoryLimit) || 
		   license.getMaxMemory().get(LicenseEnum.COMPONENT_RSCONTROL.getKeygenName()) == 0 )
		{
			memoryLimit = "N/A";
		}
		result.put("Memory Limit (RSCONTROL)", memoryLimit);

		memoryLimit = license.getMaxMemory().containsKey(LicenseEnum.COMPONENT_RSVIEW.getKeygenName()) ? 
					  "" + license.getMaxMemory().get(LicenseEnum.COMPONENT_RSVIEW.getKeygenName()) : "";
		if(StringUtils.isBlank(memoryLimit) ||
		   license.getMaxMemory().get(LicenseEnum.COMPONENT_RSVIEW.getKeygenName()) == 0)
		{
			memoryLimit = "N/A";
		}
		result.put("Memory Limit (RSVIEW)", memoryLimit);

		String userCount = "" + license.getEndUserCount();
		if(license.getEndUserCount() == 0)
		{
			userCount = "N/A";
		}
		result.put("Number of Users", userCount);
		
		for (GatewayLicense gatewayLicense : license.getAllGatewayLicenses())
		{
			result.put("Gateway#" + gatewayLicense.getCode(), gatewayLicense.info());

			Map<String, Map<String, String>> instanceToInfoMap = gatewayLicense.getRunningInstancesNewAsMap();

			for (String instanceKey : instanceToInfoMap.keySet())
			{
				result.put(instanceKey, instanceToInfoMap.get(instanceKey).toString());
			}
		}

		return result;
	} // getLicenseInfo

	private void saveLicenseFile(String licenseFileName, String licenseContent) throws Exception
	{
		if (Log.log.isDebugEnabled()) {
			Log.log.debug(String.format("saveLicenseFile(%s, %s)", licenseFileName, licenseContent));
		}
		
		boolean writeFile = false;
		
		try {
			LicenseUtil.lockLicenseRepository(MainBase.main.configId.getGuid(), LicenseUtil.LicRepoLockType.READ);
			
			File dir = FileUtils.getFile(MainBase.main.getResolveHome());
			
			if (dir != null && dir.isDirectory() && dir.exists()) {
				File licenseFile = FileUtils.getFile(MainBase.main.getResolveHome() + "/" + licenseFileName);
				
				if (!licenseFile.exists()) {
					writeFile = true;
				} else {
					/* 
					 * If change is only in upload date then license file is not updated just because every component 
					 * loads license file at different times. License file is updated only if it is deleted.
					 */
					
					License existingLicense = loadLicenseByFile(licenseFile);
					
					String[] licenseParts = licenseContent.split(LicenseEnum.SIGNATURE.getKeygenName(), 2);
					
					if (licenseParts != null && licenseParts.length >= 1 && StringUtils.isNotBlank(licenseParts[0])) {
						String decryptedLic = CryptUtils.decryptLicense(licenseParts[0]);
						
						if (StringUtils.isNotBlank(decryptedLic) && 
							decryptedLic.contains(LicenseEnum.DELETE_DATE.getKeygenName()) && 
							!existingLicense.isDeleted()) {
							writeFile = true;
						} else {
							Log.log.info(String.format("License file %s of size %d which is close to license content size %d " +
									   				   "already exists in license repository and is undeleted. License file " +
									   				   "upload date is %s.", licenseFile.getAbsolutePath(), 
									   				   licenseFile.length(), licenseContent.length(),
									   				   DateUtils.convertTimeInMillisToString(
									   				       existingLicense.getUploadDate(), 
									   					   LicenseEnum.DATE_FORMAT_LONG.getKeygenName())));
						}
					}
				}
				
				if (writeFile) {
					LicenseUtil.lockLicenseRepository(MainBase.main.configId.getGuid(), LicenseUtil.LicRepoLockType.WRITE);
					// Overwrite file contents
					FileUtils.writeStringToFile(licenseFile, licenseContent, StandardCharsets.UTF_8.name(), false);
					Log.log.info(String.format("Saved license file %s of size %d to local license repository.", 
											   licenseFile.getAbsolutePath(), licenseFile.length()));
				}
			} else {
				String errMsg = String.format("License Repository %s does not exists.", dir.getAbsolutePath());
				Log.log.error(errMsg);
				throw new Exception(errMsg);
			}
		} finally {
			if (writeFile) {
				LicenseUtil.unLockLicenseRepository(MainBase.main.configId.getGuid(), LicenseUtil.LicRepoLockType.WRITE);
			}
			LicenseUtil.unLockLicenseRepository(MainBase.main.configId.getGuid(), LicenseUtil.LicRepoLockType.READ);
		}
	}

	public Map<String, String> getLicenseInfo()
	{
		return getLicenseInfo(null);
	}

	/**
	 * Every gateway broadcasts a message announcing its presence. In turn later
	 * we can check the instance count of running gateways for licensing
	 * purpose.
	 *
	 * @param params
	 *            like key=Gateway Code, value=INSTANCE_ID|=|RSRemote GUID|&|INSTANCE_TYPE|=|PRIMARY|&|QUEUE|=|Gateway Queue|&|STARTTIME|=|254366255
	 * @throws Exception
	 *
	 */
	@SuppressWarnings("unchecked")
	public void registerGateway(Map<String, String> params)
	{
		//the message is not replayed over and over again, and only rsremote will accept this message
		if(MainBase.main.release.serviceName.toLowerCase().startsWith("rsremote")
				&& !MainBase.main.configId.getGuid().equalsIgnoreCase(params.get("SENDERID")))
		{
			params.remove("SENDERID");
			params.remove("RESOLVE.JOB_ID");

			Log.log.debug("Registering Gateway: " + params);
			for (String gatewayCode : params.keySet())
			{
				GatewayLicense gatewayLicense = license.getGatewayLicense(gatewayCode);

				if (gatewayLicense == null)
				{
					Log.log.error(gatewayCode + " gateway license not available.");
				}
				else
				{
					// this will have GATEWAY_TYPE, GUID, QUEUE, STARTTIME
					Map<String, String> instance = StringUtils.stringToMap(params.get(gatewayCode));
					try
					{
						gatewayLicense.addRunningInstanceNew(instance.get("INSTANCE_ID"), instance.get("INSTANCE_TYPE"), instance.get("QUEUE"), instance.get("STARTTIME"), gatewayCode);
					}
					catch(Exception e)
					{
						Log.log.warn("Failed to process time sensitive Gateway Registration Message " + params, e);
					}
					//license.setGatewayLicense(gatewayCode, gatewayLicense);
				}
			}
		}
		else
		{
			if (MainBase.main.release.serviceName.toLowerCase().startsWith("rsremote")
					&& MainBase.main.configId.getGuid().equalsIgnoreCase(params.get("SENDERID")))
			{
				Log.log.debug("Ignoring self generated Register Gateway Running Instance message: " + params);
			}
			else
			{
				Log.log.trace(MainBase.main.release.serviceName + " ignoring Register Gateway Running Instance message meant for rsremote only");
			}
		}
	}

	/**
	 * Every gateway requests for license validation. License service's local
	 * storage for running instances will determine if the gateway can be started or not.
	 * @param gatewayCode like EMAIL, NETCOOL.
	 * @param instanceId
	 * @param instanceType like PRIMARY, SECONDARY, or WORKER
	 * @param queueName
	 * @param startTime
	 * @param checkToStart True - Check if ok to start, False - Check if ok to still keep running
	 * @param originalPrimary
	 *
	 * @return
	 */
	public boolean checkGatewayLicense(String gatewayCode, String instanceId, String instanceType, String queueName, long startTime, boolean checkToStart, boolean origianlPrimary)
	{
		boolean isValid = false;
		
		Log.log.debug(String.format("LicenseService.checkGatewayLicense(%s,%s,%s,%s,%d,%b,%b)", gatewayCode, instanceId, 
				instanceType, queueName, startTime, checkToStart, origianlPrimary));
		
		GatewayLicense gatewayLicense = license.getGatewayLicense(gatewayCode);
		if (gatewayLicense == null)
		{
			Log.log.warn(gatewayCode + " gateway license not available.");
			long currentTime = System.currentTimeMillis();
			Long lastGatewayAlertTime = gatewayAlertTime.get(gatewayCode);
			if(lastGatewayAlertTime == null || (currentTime - lastGatewayAlertTime) > 24*60*60*1000L)
			{
				alert(ERR.E10017, gatewayCode + " gateway license not available.");
				gatewayAlertTime.put(gatewayCode, currentTime);
			}
		}
		else
		{
			// V2 expired license check for NON-PROD environments/instance types
			
			try {
				if (getCurrentLicense().isV2AndExpiredAndNonProd() && 
					!LicenseEnum.LICENSE_ENV_PRODUCTION.getKeygenName()
					 .equalsIgnoreCase(MainBase.main.configGeneral.getEnv())) {
					String msg = ERR.E10027.getMessage() + 
								 String.format(GATEWAY_DETAILS, gatewayCode, instanceId, instanceType, queueName, 
										 	   startTime, checkToStart, origianlPrimary);
					Log.alert(ERR.E10027.getCode(), msg, Constants.ALERT_TYPE_LICENSE);
				}
			} catch (Exception e) {
				Log.alert(ERR.E10028.getCode(), ERR.E10028.getMessage() + " Shutting Down!!!", 
          			  	  Constants.ALERT_TYPE_LICENSE);
				Log.log.fatal(ERR.E10028.getMessage() + " Shutting Down!!!");
				System.exit(1);
			}
			
			boolean gatewayLicenseExpired = gatewayLicense.isExpired();
			
			Log.log.warn(String.format("LicenseService.checkGatewayLicense(%s):gatewayLicenseExpired=%b", 
									   gatewayCode, gatewayLicenseExpired));
			
			try {
				if (gatewayLicenseExpired && (getCurrentLicense().isV2AndExpiredAndProd() &&
										 	  LicenseEnum.LICENSE_ENV_PRODUCTION.getKeygenName()
										 	  .equalsIgnoreCase(MainBase.main.configGeneral.getEnv()))) {
					gatewayLicenseExpired = false;
				}
			} catch (Exception e) {
				Log.log.error(String.format("Error [%s] occurred while checking if current license is V2, Expired and for " +
										    "Prod environment.",
											(StringUtils.isNotBlank(e.getMessage()) ? e.getMessage() : "")), e);
			}
			
			Log.log.debug(String.format("LicenseService.checkGatewayLicense:gatewayLicenseExpired(%s)=%b after V2 and Expired " +
									    "and PROD env check", gatewayCode, gatewayLicenseExpired));
			
			// Check if license has not expired and is still valid
			if(!gatewayLicenseExpired)
			{
				isValid = gatewayLicense.isValidNew(instanceId, instanceType, gatewayCode, queueName, startTime, checkToStart, origianlPrimary);

				Log.log.debug("gatewayLicense.isValidNew(" + instanceId + ", " + instanceType + ", " + gatewayCode + ", " + queueName + ", " + startTime + ", " + checkToStart + ", " + origianlPrimary + ") returned " + isValid);

				/*
				 *  If specific named GATEWAY license is invalid for start then assume that
				 *  its invalid due to insufficient count and check if ANY gateway licenses
				 *  are available to start this gateway
				 */
				if (checkToStart && !isValid && (gatewayLicense.getCode().equalsIgnoreCase(gatewayCode) || (gatewayLicense.getCode().equals("*"))))
				{
					Log.log.debug("Checking for ANY license...");

					gatewayLicense = license.getGatewayLicense("ANY");

					if (gatewayLicense != null) {
						gatewayLicenseExpired = gatewayLicense.isExpired();
						
						try {
							if (gatewayLicenseExpired && (getCurrentLicense().isV2AndExpiredAndProd() &&
														  LicenseEnum.LICENSE_ENV_PRODUCTION.getKeygenName()
														  .equalsIgnoreCase(MainBase.main.configGeneral.getEnv()))) {
								gatewayLicenseExpired = false;
							}
						} catch (Exception e) {
							Log.log.error(String.format("Error [%s] occurred while checking if current license is V2, Expired and for " +
								    "Prod environment.",
									(StringUtils.isNotBlank(e.getMessage()) ? e.getMessage() : "")), e);
						}
						
						if (!gatewayLicenseExpired)
						{
							isValid = gatewayLicense.isValidNew(instanceId, instanceType, gatewayCode, queueName, startTime, checkToStart, origianlPrimary);
							Log.log.debug("ANY gatewayLicense.isValidNew(" + instanceId + ", " + instanceType + ", " + gatewayCode + ", " + queueName + ", " + startTime + ", " + checkToStart + ", " + origianlPrimary + ") returned " + isValid);
						}
					}
				}

				if(isValid) // Send registration message only on startup
				{
					license.setGatewayLicense(gatewayCode, gatewayLicense);

					if (checkToStart) // Send registration message only on startup
					{
						gatewayLicense.addRunningInstanceNew(instanceId, instanceType, queueName, startTime, gatewayCode);
						//this will send this gateway instance to other components for their storage
						sendGatewayRegistration(gatewayCode, instanceId, instanceType, queueName, ""+startTime);
					}
					else
					{
						Long runningInstanceStartTime = gatewayLicense.getRuningInstanceStartTime(instanceId, instanceType, queueName, gatewayCode);
						gatewayLicense.updateRunningInstanceNew(instanceId, instanceType, queueName, runningInstanceStartTime, startTime, gatewayCode);
						//this will send this gateway instance to other components for their storage
						sendGatewayUpdateRunningInstance(gatewayCode, instanceId, instanceType, queueName, runningInstanceStartTime.toString(), ""+startTime);
					}
				}
			} else {
				isValid = !gatewayLicenseExpired;
			}
		}
		
		Log.log.debug(String.format("LicenseService.checkGatewayLicense(%s,%s,%s,%s,%d,%b,%b) returning %b", gatewayCode, 
									instanceId, instanceType, queueName, startTime, checkToStart, origianlPrimary, isValid));
		
		return isValid;
	}

	public boolean isGatewayLicenseExpired(String gatewayCode)
	{
		boolean isExpired = false;

		GatewayLicense gatewayLicense = license.getGatewayLicense(gatewayCode);
		if (gatewayLicense != null)
		{
			isExpired = gatewayLicense.isExpired();
		}

		Log.log.debug(String.format("isGatewayLicenseExpired(%s) returning %b from license with key %s", 
									gatewayCode, isExpired, license.getKey()));
		
		return isExpired;
	}

	/**
	 * Remove gateway running instance.
	 * @param gatewayCode like EMAIL, NETCOOL.
	 * @param instanceId
	 * @param instanceType like PRIMARY, SECONDARY, or WORKER
	 * @param queueName
	 *
	 * @return
	 */
	public void removeGatewayRunningInstance(String gatewayCode, String instanceId, String instanceType, String queueName)
	{
		GatewayLicense gatewayLicense = license.getGatewayLicense(gatewayCode);
		gatewayLicense.removeRunningInstanceNew(instanceId, instanceType, queueName, gatewayCode);
	}

	public void sendGatewayRemoveRunningInstance(String gatewayCode, String instanceId, String instanceType, String gatewayQueueName)
	{
		Map<String, String> params = new HashMap<String, String>();
		String value = "INSTANCE_ID|=|" + instanceId + "|&|INSTANCE_TYPE|=|" + instanceType + "|&|QUEUE|=|" + gatewayQueueName;
		params.put(gatewayCode, value);
		//so that the message is not replayed over and over again
		params.put("SENDERID", MainBase.main.configId.getGuid());
		if (MainBase.esb.sendInternalMessage(Constants.ESB_NAME_RSLICENSES, "MLicense.removeGatewayRunningInstance", params) == false)
		{
			Log.log.warn("Failed to send gateway remove running instance message with parameters " + params);
		}
	}

	@SuppressWarnings("unchecked")
	public void removeGatewayRunningInstance(Map<String, String> params)
	{
		//the message is not replayed over and over again, and only rsremote will accept this message
		if(MainBase.main.release.serviceName.toLowerCase().startsWith("rsremote")
				&& !MainBase.main.configId.getGuid().equalsIgnoreCase(params.get("SENDERID")))
		{
			params.remove("SENDERID");
			params.remove("RESOLVE.JOB_ID");

			Log.log.debug("Remove Gateway Running Instance: " + params);
			for (String gatewayCode : params.keySet())
			{
				GatewayLicense gatewayLicense = license.getGatewayLicense(gatewayCode);

				if (gatewayLicense == null)
				{
					Log.log.error(gatewayCode + " gateway license not available.");
				}
				else
				{
					// this will have GATEWAY_TYPE, GUID, QUEUE, STARTTIME, LASTCHECKRUNNINGTIME
					Map<String, String> instance = StringUtils.stringToMap(params.get(gatewayCode));
					try
					{
						gatewayLicense.removeRunningInstanceNew(instance.get("INSTANCE_ID"), instance.get("INSTANCE_TYPE"), instance.get("QUEUE"), gatewayCode);
					}
					catch(Exception e)
					{
						Log.log.warn("Failed to process time sensitive Remove Gateway Running Instance Message " + params, e);
					}
				}
			}
		}
		else
		{
			if (MainBase.main.release.serviceName.toLowerCase().startsWith("rsremote")
					&& MainBase.main.configId.getGuid().equalsIgnoreCase(params.get("SENDERID")))
			{
				Log.log.debug("Ignoring self generated Remove Gateway Running Instance message: " + params);
			}
			else
			{
				Log.log.trace(MainBase.main.release.serviceName + " ignoring Remove Gateway Running Instance message meant for rsremote only");
			}
		}
	}

	/**
	 * Every gateway checks it license to keep running periodically and
	 * broadcasts a message annoucing that it can still keep running.
	 *
	 * @param params
	 *            like key=Gateway Code, value=INSTANCE_ID|=|RSRemote GUID|&|INSTANCE_TYPE|=|PRIMARY|&|QUEUE|=|Gateway Queue|&|LASTCHECKRUNNINGTIME|=|254366255
	 * @throws Exception
	 *
	 */
	@SuppressWarnings("unchecked")
	public void updateGatewayRunningInstance(Map<String, String> params)
	{
		//the message is not replayed over and over again, and only rsremote will accept this message
		if(MainBase.main.release.serviceName.toLowerCase().startsWith("rsremote")
				&& !MainBase.main.configId.getGuid().equalsIgnoreCase(params.get("SENDERID")))
		{
			params.remove("SENDERID");
			params.remove("RESOLVE.JOB_ID");

			Log.log.debug("Updating Gateway Running Instance: " + params);
			for (String gatewayCode : params.keySet())
			{
				GatewayLicense gatewayLicense = license.getGatewayLicense(gatewayCode);

				if (gatewayLicense == null)
				{
					Log.log.error(gatewayCode + " gateway license not available.");
				}
				else
				{
					// this will have GATEWAY_TYPE, GUID, QUEUE, STARTTIME, LASTCHECKRUNNINGTIME
					Map<String, String> instance = StringUtils.stringToMap(params.get(gatewayCode));
					try
					{
						gatewayLicense.updateRunningInstanceNew(instance.get("INSTANCE_ID"), instance.get("INSTANCE_TYPE"), instance.get("QUEUE"), instance.get("STARTTIME"), instance.get("LASTCHECKRUNNINGTIME"), gatewayCode);
					}
					catch(Exception e)
					{
						Log.log.warn("Failed to process time sensitive Update Gateway Running Instance Message " + params, e);
					}
				}
			}
		}
		else
		{
			if (MainBase.main.release.serviceName.toLowerCase().startsWith("rsremote")
					&& MainBase.main.configId.getGuid().equalsIgnoreCase(params.get("SENDERID")))
			{
				Log.log.debug("Ignoring self generated Update Gateway Running Instance message: " + params);
			}
			else
			{
				Log.log.trace(MainBase.main.release.serviceName + " ignoring Update Gateway Running Instance message meant for rsremote only");
			}
		}
	}

	public void sendGatewayUpdateRunningInstance(String gatewayCode, String instanceId, String instanceType, String gatewayQueueName, String startTime, String lastCheckRunningTime)
	{
		Map<String, String> params = new HashMap<String, String>();
		String value = "INSTANCE_ID|=|" + instanceId + "|&|INSTANCE_TYPE|=|" + instanceType + "|&|QUEUE|=|" + gatewayQueueName + "|&|STARTTIME|=|" + startTime + "|&|LASTCHECKRUNNINGTIME|=|" + lastCheckRunningTime;
		params.put(gatewayCode, value);
		//so that the message is not replayed over and over again
		params.put("SENDERID", MainBase.main.configId.getGuid());
		Log.log.debug("Sending gateway update running instance message " + params + " to RSLICENSES..." );
		if (MainBase.esb.sendInternalMessage(Constants.ESB_NAME_RSLICENSES, "MLicense.updateGatewayRunningInstance", params) == false)
		{
			Log.log.warn("Failed to send gateway update running instance message with parameters " + params);
		}
	}

	/**
	 * Switch gateway running instance type from Primary to Secondary or vice-a-versa.
	 * @param gatewayCode like EMAIL, NETCOOL.
	 * @param instanceId
	 * @param instanceType like PRIMARY, SECONDARY, or WORKER
	 * @param queueName
	 *
	 * @return
	 */
	public void switchGatewayRunningInstanceType(String gatewayCode, String instanceId, String instanceType, String queueName)
	{
		if (!instanceType.equals("WORKER"))
		{
			GatewayLicense gatewayLicense = license.getGatewayLicense(gatewayCode);
			gatewayLicense.switchRunningInstanceNew(instanceId, instanceType, queueName, gatewayCode);
		}
	}

	public void sendGatewaySwitchRunningInstanceType(String gatewayCode, String instanceId, String instanceType, String gatewayQueueName)
	{
		if (!instanceType.equalsIgnoreCase("WORKER"))
		{
			Map<String, String> params = new HashMap<String, String>();
			String value = "INSTANCE_ID|=|" + instanceId + "|&|INSTANCE_TYPE|=|" + instanceType + "|&|QUEUE|=|" + gatewayQueueName;
			params.put(gatewayCode, value);
			//so that the message is not replayed over and over again
			params.put("SENDERID", MainBase.main.configId.getGuid());
			if (MainBase.esb.sendInternalMessage(Constants.ESB_NAME_RSLICENSES, "MLicense.switchGatewayRunningInstanceType", params) == false)
			{
				Log.log.warn("Failed to send gateway switch running instance type message with parameters " + params);
			}
		}
	}

	/**
	 * Clustered SECONDARY gateway sends switch running instance type
	 * from SECONDARY to PRIMARY/SECONDARY when swicthing over to
	 * become PRIMARY during failover and swicthing back to SECONDARY
	 * when original PRIMARY comes back up.
	 *
	 * @param params
	 *            like key=Gateway Code, value=INSTANCE_ID|=|RSRemote GUID|&|INSTANCE_TYPE|=|PRIMARY|&|QUEUE|=|Gateway Queue
	 * @throws Exception
	 *
	 */
	@SuppressWarnings("unchecked")
	public void switchGatewayRunningInstanceType(Map<String, String> params)
	{
		//the message is not replayed over and over again, and only rsremote will accept this message
		if(MainBase.main.release.serviceName.toLowerCase().startsWith("rsremote")
				&& !MainBase.main.configId.getGuid().equalsIgnoreCase(params.get("SENDERID")))
		{
			params.remove("SENDERID");
			params.remove("RESOLVE.JOB_ID");

			Log.log.debug("Switching Gateway Running Instance Type: " + params);
			for (String gatewayCode : params.keySet())
			{
				GatewayLicense gatewayLicense = license.getGatewayLicense(gatewayCode);

				if (gatewayLicense == null)
				{
					Log.log.error(gatewayCode + " gateway license not available.");
				}
				else
				{
					// this will have GATEWAY_TYPE, GUID, QUEUE, STARTTIME, LASTCHECKRUNNINGTIME
					Map<String, String> instance = StringUtils.stringToMap(params.get(gatewayCode));
					try
					{
						gatewayLicense.switchRunningInstanceNew(instance.get("INSTANCE_ID"), instance.get("INSTANCE_TYPE"), instance.get("QUEUE"), gatewayCode);
					}
					catch(Exception e)
					{
						Log.log.warn("Failed to process time sensitive Switch Gateway Running Instance Type Message " + params, e);
					}
				}
			}
		}
		else
		{
			if (MainBase.main.release.serviceName.toLowerCase().startsWith("rsremote")
					&& MainBase.main.configId.getGuid().equalsIgnoreCase(params.get("SENDERID")))
			{
				Log.log.debug("Ignoring self generated Switch Gateway Running Instance Type message: " + params);
			}
			else
			{
				Log.log.trace(MainBase.main.release.serviceName + " ignoring Switch Gateway Running Instance message meant for rsremote only");
			}
		}
	}

	public String getRbc(Map<String, String> rbcParams)
	{
		String rbc = "/$";

		String rbcSource = rbcParams.get(LicenseEnum.RBC_SOURCE.getKeygenName());
		Log.log.info("RBC Source: " + rbcSource);
		if (StringUtils.isBlank(rbcSource))
		{
			rbcSource = rbcParams.get(Constants.HTTP_REQUEST_SSO_TYPE);
			Log.log.info(Constants.HTTP_REQUEST_SSO_TYPE + ": " + rbcSource);
		}

		if (StringUtils.isNotBlank(rbcSource))
		{
			rbcSource = rbcSource.toLowerCase();
			
			// SDK2 gateways adds suffix "Gateway" to RBC source which should be filtered out
			if (rbcSource.endsWith(SDK2_SOURCE_SUFFIX.toLowerCase())) {
				rbcSource = StringUtils.substringBefore(rbcSource, SDK2_SOURCE_SUFFIX.toLowerCase());
			}
			
			/*
			 * Determination of whether data submitted for execution from gateways/UI is Ticket/Incident or 
			 * Event will now be dynamic and based on the source in the RBC stamp from 6.3 onwards.
			 * 
			 * RBC from 6.3 onwards will be 
			 * 
			 * /$R63Unclassified[/<source>/<queue name>]
			 *
			 * Execution data from gateway will only contain source and queue name
			 * 
			 * All data submitted for execution will be considered as Incident/Ticket by default.
			 * 
			 * Exception to consider execution data from specific sources as Events will be encoded 
			 * as such.
			 * 
			 * Legacy data (pre 6.3) will be considered as Incident/Ticket.
			 * 
			 */
			
			switch(rbcSource)
			{
			case "netcool" : rbc = "/$Event"; break;
			case "caspectrum" : rbc = "/$Ticket"; break;
			case "servicenow" : rbc = "/$Ticket"; break;
			case "splunk" : rbc = "/$Ticket"; break;
			case "remedyx" : rbc = "/$Ticket"; break;
			default : rbc = "/$Unclassified"; break;
			}
		}

		return rbc;
	}
	
	public String getRBCR63(Map<String, String> rbcParams) {
		StringBuilder rbcR63SB = new StringBuilder(RBC_RELEASE_63_UNCLASSIFIED_PREFIX);
		
		if (MapUtils.isNotEmpty(rbcParams)) {
			
			String rbcSource = rbcParams.get(LicenseEnum.RBC_SOURCE.getKeygenName());
			
			if (StringUtils.isBlank(rbcSource)) {
				rbcSource = rbcParams.get(Constants.HTTP_REQUEST_SSO_TYPE);
				Log.log.info(Constants.HTTP_REQUEST_SSO_TYPE + ": " + rbcSource);
			}
			
			rbcParams.remove(LicenseEnum.RBC_SOURCE.getKeygenName());
			rbcParams.remove(Constants.HTTP_REQUEST_SSO_TYPE);
			
			if (StringUtils.isNotBlank(rbcSource))
			{
				rbcSource = rbcSource.toLowerCase();
				
				// SDK2 gateways adds suffix "Gateway" to RBC source which should be filtered out
				if (rbcSource.endsWith(SDK2_SOURCE_SUFFIX.toLowerCase())) {
					rbcSource = StringUtils.substringBefore(rbcSource, SDK2_SOURCE_SUFFIX.toLowerCase());
				}
								
				/*
				 * Determination of whether data submitted for execution from gateways/UI is Ticket/Incident or 
				 * Event will now be dynamic and based on the source in the RBC stamp from 6.3 onwards.
				 * 
				 * RBC from 6.3 onwards will be 
				 * 
				 * /$R63Unclassified[/<source>/<queue name>]
				 *
				 * Execution data from gateway will only contain source and queue name
				 * 
				 * All data submitted for execution will be considered as Incident/Ticket by default.
				 * 
				 * Exception to consider execution data from specific sources as Events will be encoded 
				 * as such.
				 * 
				 * Legacy data (pre 6.3) will be considered as Incident/Ticket.
				 * 
				 */
				
				rbcR63SB.append(RBC_FIELD_SEPARATOR).append(rbcSource);
				rbcParams.put(LicenseEnum.RBC_SOURCE.getKeygenName(), rbcSource);
				
				if (rbcParams.containsKey(LicenseEnum.RBC_QUEUE.getKeygenName()) && 
					StringUtils.isNotBlank(rbcParams.get(LicenseEnum.RBC_QUEUE.getKeygenName()))) {
					rbcR63SB.append(RBC_FIELD_SEPARATOR)
					.append(rbcParams.get(LicenseEnum.RBC_QUEUE.getKeygenName()).toLowerCase());
				}
			}
			
			rbcParams.remove(LicenseEnum.RBC_QUEUE.getKeygenName());
		}
		
		return rbcR63SB.toString();
	}
	
	public boolean getIsHAViolated()
	{
		return isHAViolated;
	}

	public void setIsHAViolated(boolean isHAViolated)
	{
		this.isHAViolated = isHAViolated;
	}
	
	public boolean getIsUserCountViolated()
	{
		return isUserCountViolated;
	}
	
	public void setIsUserCountViolated(boolean isUserCountViolated)
	{
		this.isUserCountViolated = isUserCountViolated;
	}
	
	public static boolean canAddNewUser(License license, int numberOfActualEndUsers)
	{
		boolean userCountCheckedOut = true;
		int allowedEndUsers = license.getEndUserCount();

		// endUsers=0 mean no restriction.
		if (allowedEndUsers > 0 && numberOfActualEndUsers >= allowedEndUsers)
		{
			userCountCheckedOut = false;
		}
		
		return userCountCheckedOut;
	}
}