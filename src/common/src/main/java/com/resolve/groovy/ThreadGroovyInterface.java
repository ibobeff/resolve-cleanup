/******************************************************************************
* (C) Copyright 2014
*
* Resolve Systems, LLC
*
* All rights reserved
* This software is distributed under license and may not be used, copied,
* modified, or distributed without the express written permission of
* Resolve Systems, LLC.
*
******************************************************************************/

package com.resolve.groovy;

import groovy.lang.Binding;

public interface ThreadGroovyInterface extends Runnable
{
    public void run();

    public Object getResult();

    public boolean isException();

    public void setBinding(Binding binding);

    public void setArgs(Object[] args);

    public void setScript(String script);
    
    public void setScriptName(String scriptName);

    public void setCacheable(boolean cacheable);

} // ThreadGroovyInterface
