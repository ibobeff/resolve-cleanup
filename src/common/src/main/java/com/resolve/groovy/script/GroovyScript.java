/******************************************************************************
* (C) Copyright 2014
*
* Resolve Systems, LLC
*
* All rights reserved
* This software is distributed under license and may not be used, copied,
* modified, or distributed without the express written permission of
* Resolve Systems, LLC.
*
******************************************************************************/

package com.resolve.groovy.script;

import java.util.HashMap;
import java.util.concurrent.TimeoutException;
import java.util.concurrent.locks.ReentrantLock;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import com.resolve.exception.ActionTaskAbortException;
import com.resolve.groovy.ThreadGroovy;
import com.resolve.groovy.ThreadGroovyInterface;
import com.resolve.util.Constants;
import com.resolve.util.Log;
import com.resolve.util.StringUtils;

import groovy.lang.Binding;
import groovy.lang.GroovyClassLoader;
import groovy.lang.GroovyCodeSource;
import groovy.lang.MissingPropertyException;
import groovy.lang.Script;

public class GroovyScript
{
    static HashMap<String, Class<?>> groovyScriptClassCache = new HashMap<>();
    static HashMap<String, Boolean> groovyScriptUseDB = new HashMap<>();
    static Class<?> threadGroovyFactory = ThreadGroovy.class;
    
    static Pattern dbPattern = Pattern.compile("(?<!\\w)DB(?!\\w)");

    static ReentrantLock lock = new ReentrantLock();
    
    /**
     * Execute groovy script and return result
     * 
     * @param script
     * @return returned object value from the script
     */

    public static Object execute(String script, String scriptName, boolean cacheable, Binding binding) throws Exception
    {
        Object[] args = {};

        return execute(script, scriptName, cacheable, binding, args);
    } // execute

    public static Object execute(String script, String scriptName, boolean cacheable, Binding binding, Object[] args) throws Exception
    {
        Object result = null;

        Script groovyScript = getGroovyScript(script, scriptName, cacheable);

        // execute script
        if (groovyScript != null)
        {
            try
            {
                if (binding != null)
                {
                    groovyScript.setBinding(binding);
                }

                Boolean help = null;
                try
                {
                    help = (Boolean) binding.getVariable(Constants.RSCONSOLE_BINDING_HELP);
                }
                catch (MissingPropertyException mpe)
                {
                    help = false;
                }

                if (help)
                {
                    try
                    {
                        result = groovyScript.invokeMethod("help", args);
                    }
                    catch (Exception e)
                    {
                        if (e.getMessage().contains("No signature of method"))
                        {
                            e = new Exception("No help information available for this command", e);
                        }
                        throw e;
                    }
                }
                else
                {
                    result = groovyScript.invokeMethod("run", args);
                }
            }
            finally
            {
                // remove binding from cached groovyScript object
                groovyScript.setBinding(null);
            }
        }

        return result;
    } // execute

    public static Object execute(String script, String scriptName, String methodName, boolean cacheable, Binding binding, Object[] args) throws Exception
    {
        Object result = null;

        Script groovyScript = getGroovyScript(script, scriptName, cacheable);

        // execute script
        if (groovyScript != null)
        {
            try
            {
                if (binding != null)
                {
                    groovyScript.setBinding(binding);
                }

                Boolean help = null;
                try
                {
                    help = (Boolean) binding.getVariable(Constants.RSCONSOLE_BINDING_HELP);
                }
                catch (MissingPropertyException mpe)
                {
                    help = false;
                }

                if (help)
                {
                    try
                    {
                        result = groovyScript.invokeMethod("help", args);
                    }
                    catch (Exception e)
                    {
                        if (e.getMessage().contains("No signature of method"))
                        {
                            e = new Exception("No help information available for this command", e);
                        }
                        throw e;
                    }
                }
                else
                {
                    result = groovyScript.invokeMethod(methodName, args);
                }
            }
            finally
            {
                // remove binding from cached groovyScript object
                groovyScript.setBinding(null);
            }
        }

        return result;
    } // execute

    public static Object executeAsync(String script, String scriptName, boolean cacheable, Binding binding, long timeout) throws Exception
    {
        Object[] args = {};

        return executeAsync(script, scriptName, cacheable, binding, args, timeout);
    } // thread

    /**
     * 
     * @param script
     * @param binding
     * @param args
     * @param timeout
     *            - wait until timeout (> 0), wait indefinitely (0), no wait
     *            (-1)
     * @param includeDB
     * @return
     * @throws Exception
     */
    public static Object executeAsync(String script, String scriptName, boolean cacheable, Binding binding, Object[] args, long timeout) throws Exception
    {
        Object result = null;
        Thread thread = null;

        try
        {
            // execute script
            ThreadGroovyInterface threadScript = null;
            threadScript = (ThreadGroovyInterface) threadGroovyFactory.newInstance();
            threadScript.setScript(script);
            threadScript.setScriptName(scriptName);
            threadScript.setCacheable(cacheable);
            threadScript.setBinding(binding);
            threadScript.setArgs(args);
            thread = new Thread(threadScript);

            // start thread
            thread.start();
            
            // dont wait
            if (timeout < 0)
            {
                result = null;
            }

            // wait for thread to complete indefinitely
            else if (timeout == 0)
            {
            	thread.join();
                result = threadScript.getResult();
            }

            // wait for thread to complete or timeout
            else
            {
                thread.join(timeout);

            	Thread.State state = thread.getState();
                if (state != Thread.State.TERMINATED)
                {
                    throw new InterruptedException("ERROR: Script execution timed out");
                }
                result = threadScript.getResult();
            }

            if (threadScript.isException())
            {
                throw new Exception(result.toString());
            }
        }
        catch (InterruptedException | TimeoutException ie)
        {
            Log.log.warn(String.format("Aborting execution of groovy script %s, cacheable %b due to error %safter timeout " +
            						   "of %d msec", scriptName, cacheable, 
            						   (StringUtils.isNotBlank(ie.getMessage()) ? "[" + ie.getMessage() + "] " : ""), 
            						   timeout), ie);

            if (thread != null)
            {
                Thread.State state = thread.getState();
                if (state != Thread.State.TERMINATED)
                {
                    thread.interrupt();
                }
            }
            throw new ActionTaskAbortException(ie.getMessage(), ie);
        }

        return result;
    } // thread

    /**
     * Retrieve the compiled groovy script from the cache. If it does not
     * already exist, compile the provided script and insert into cache.
     * 
     * @param script
     *            - groovy script code used to retrieve the compiled version.
     * @return compiled script
     */
    public static Script getGroovyScript(String script, String scriptName, boolean cacheable) throws Exception
    {
        Script result = null;

        if (!StringUtils.isEmpty(script))
        {
            // get script from cache
            Class<?> groovyScriptClass = null;
            if (cacheable)
            {
                groovyScriptClass = (Class<?>) groovyScriptClassCache.get(script);
            }
            
            if(Log.log.isDebugEnabled()) {
                Log.log.debug("Getting Script From Cache: " + (groovyScriptClass != null));
            }

            if (groovyScriptClass == null)
            {
                try
                {
                    lock.lock();

                    // re-check if available from cache
                    if (cacheable)
                    {
                        groovyScriptClass = (Class<?>) groovyScriptClassCache.get(script);
                    }

                    if (groovyScriptClass == null)
                    {
                        // load groovy class and execute instance
                        ClassLoader parent = GroovyScript.class.getClassLoader();
                        GroovyClassLoader loader = new GroovyClassLoader(parent);

                        // prepend script with default imports
                        String mscript = addDefaultScriptImports(script);
                        
                        if (StringUtils.isEmpty(scriptName))
                        {
                            scriptName = "Groovy_" + System.currentTimeMillis()+".groovy";
                        }
                        else
                        {
                            // init scriptClassName
                            scriptName = "Groovy_"+StringUtils.stringToIdentifier(scriptName);
                            if (scriptName.endsWith("_groovy"))
                            {
                                scriptName = scriptName.replace("_groovy", ".groovy");
                            }
                            else
                            {
                                scriptName += ".groovy";
                            }
                        }

                        // parse scripts
                        GroovyCodeSource codeSource = new GroovyCodeSource(mscript, scriptName, "/GroovyScript");
                        codeSource.setCachable(false);
                        groovyScriptClass = loader.parseClass(codeSource, false);

                        // remove class from GroovyClassLoader, use our cache instead
                        loader.clearCache();
                        loader.close();

                        // only add script to cache if does not contain literal params
                        if (cacheable)
                        {
                            groovyScriptClassCache.put(script, groovyScriptClass);
                        }
                    }
                }
                catch (Throwable t)
                {
                    throw new Exception("Groovy Script Exception " + t.getMessage(), t);
                }
                finally
                {
                    lock.unlock();
                }
            }

            try
            {
                if (groovyScriptClass != null)
                {
                    // create script instance
                    result = (Script) groovyScriptClass.newInstance();
                }
            }
            catch (Throwable t)
            {
                throw new Exception("Groovy Script Exception " + t.getMessage(), t);
            }
        }
        else
        {
            Log.log.error("Groovy Script content missing - script: " + script);
        }

        return result;
    } // getGroovyScript

    /*
     * public static Script getGroovyScript(String script) throws Exception {
     * Script result = null;
     * 
     * if (!StringUtils.isEmpty(script)) { try { // load groovy class and
     * execute instance ClassLoader parent =
     * GroovyScript.class.getClassLoader(); GroovyClassLoader loader = new
     * GroovyClassLoader(parent);
     * 
     * // prepend script with default imports String mscript =
     * addDefaultScriptImports(script);
     * 
     * // parse scripts Class groovyScriptClass = loader.parseClass(mscript);
     * 
     * result = (Script)groovyScriptClass.newInstance(); } catch (Throwable t) {
     * throw new Exception("Groovy Script Exception " + t.getMessage(), t); } }
     * else { Log.log.error("Groovy Script content missing - script: " +
     * script); }
     * 
     * return result; } // getGroovyScript
     */

    static String addDefaultScriptImports(String script)
    {
        String result = null;

        if (script != null)
        {
            // needs to be all on one line - note no \n
            String imports = "";
            imports += "import java.util.Date;";
            imports += "import java.sql.*;";
            imports += "import com.resolve.util.*;";
            imports += "import com.resolve.html.*;";
            imports += "import com.resolve.execute.Execute;";
            imports += "import com.resolve.persistence.util.*;";

            imports += "import com.resolve.services.util.AuditUtils;";           
            imports += "import com.resolve.services.util.CacheUtils;";
            imports += "import com.resolve.services.util.DocxAPI;";
            imports += "import com.resolve.services.util.DocxUtil;";
            imports += "import com.resolve.services.util.ExecuteUtil;";
            imports += "import com.resolve.services.util.PropertiesUtil;";
            imports += "import com.resolve.services.util.SequenceUtils;";
            imports += "import com.resolve.services.util.UserUtils;";
            imports += "import com.resolve.services.util.VariableUtil;";
            imports += "import com.resolve.services.util.WikiUtils;";
            imports += "import com.resolve.services.util.WorksheetUtils;";
            
            result = imports + script;
        }

        return result;
    } // addDefaultScriptImports

    public static Class<?> getThreadGroovyFactory()
    {
        return threadGroovyFactory;
    } // getThreadGroovyFactory

    public static void setThreadGroovyFactory(Class<?> threadGroovyFactory)
    {
        GroovyScript.threadGroovyFactory = threadGroovyFactory;
    } // setThreadGroovyFactory
    
    public static boolean checkOpenTransaction(String script)
    {
        boolean result = false;
        
        if (StringUtils.isNotEmpty(script)) 
        {
            if (script.contains(Constants.GROOVY_BINDING_DB))
            {
                result = true;
            }
            else if (script.contains("EntityUtil"))
            {
                result = true;
            }
        }
        return result;
    }// checkOpenTransaction 
    
    public static boolean checkScriptUseDB(String script, boolean cacheable)
    {
        boolean result = false;
        
        if (StringUtils.isNotEmpty(script)) 
        {
            if (cacheable && groovyScriptUseDB.containsKey(script))
            {
                result = groovyScriptUseDB.get(script);
            }
            else
            {
                Matcher dbMatcher = dbPattern.matcher(script);
                result = dbMatcher.find();
                if (cacheable)
                {
                    groovyScriptUseDB.put(script, result);
                }
            }
        }
        return result;
        
    }// checkScriptUseDB

	public static Object thread(String script, String scriptName, boolean cacheable, Binding binding, long timeout) throws Exception {
		return executeAsync(script, scriptName, cacheable, binding, timeout);
	}

} // GroovyScript
