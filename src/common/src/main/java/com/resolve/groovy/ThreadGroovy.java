/******************************************************************************
* (C) Copyright 2014
*
* Resolve Systems, LLC
*
* All rights reserved
* This software is distributed under license and may not be used, copied,
* modified, or distributed without the express written permission of
* Resolve Systems, LLC.
*
******************************************************************************/

package com.resolve.groovy;

import groovy.lang.Binding;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import com.resolve.groovy.script.GroovyScript;
import com.resolve.rsbase.SessionMap;
import com.resolve.rsbase.SessionObjectInterface;
import com.resolve.util.Constants;
import com.resolve.util.Log;
import com.resolve.util.ResultCode;

public class ThreadGroovy implements ThreadGroovyInterface
{
    final static String RC = "RC";
    final static String RC_EXCEPTION = "EXCEPTION";

    Binding binding;
    Object[] args;
    String script;
    String scriptName;
    boolean cacheable;
    Object result;
    ResultCode rc;
    boolean isException;
    SessionMap sessionMap = null;

    private static final ThreadLocal<List<SessionObjectInterface>> threadLocal = new ThreadLocal<List<SessionObjectInterface>>()
    {
        @Override
        protected List<SessionObjectInterface> initialValue()
        {
            return new ArrayList<SessionObjectInterface>();
        }
    };

    public static List<SessionObjectInterface> getConnectionList()
    {
        if(Log.log.isDebugEnabled()) {
            Log.log.debug("Getting the ThreadLocal connection list");
        }
        List<SessionObjectInterface> tmpConnectionList = null;
        try
        {
            tmpConnectionList = threadLocal.get();
        }
        catch (Throwable t)
        {
            t.printStackTrace();
        }

        return tmpConnectionList;
    }

    public ThreadGroovy() throws Exception
    {
        this.script = null;
        this.scriptName = null;
        this.cacheable = false;
        this.binding = null;
        this.args = null;
        this.isException = false;
        this.rc = new ResultCode();
    } // ThreadGroovy

    // public ThreadGroovy(String script, boolean cacheable, Binding binding,
    // Object[] args) throws Exception
    public ThreadGroovy(String script, String scriptName, boolean cacheable, Binding binding, Object[] args) throws Exception
    {
        this.script = script;
        this.scriptName = scriptName;
        this.cacheable = cacheable;
        this.binding = binding;
        this.args = args;
        this.isException = false;
        this.rc = new ResultCode();
    } // ThreadGroovy

    public void run()
    {
        try
        {
            if (script != null)
            {
                // add RC to binding
                binding.setVariable(Constants.GROOVY_BINDING_RC, rc);

                // execute script
                result = GroovyScript.execute(script, scriptName, cacheable, binding, args);

                // post processing
                isException = rc.isException;
            }
        }
        catch (Throwable e)
        {
            this.isException = true;
            Log.log.warn(e.getMessage(), e);

            result = Log.getStrackTrace(e);
        }
        finally
        {
            if(Log.log.isDebugEnabled()) {
                Log.log.debug("Cleaning up from ThreadGroovy");
            }

            Iterator<SessionObjectInterface> iterator = ThreadGroovy.getConnectionList().iterator();
            while (iterator.hasNext())
            {
                SessionObjectInterface object = iterator.next();
                if (this.isException || (this.sessionMap != null && !this.sessionMap.containsValue(object)))
                {
                    if(Log.log.isDebugEnabled()) {
                        Log.log.debug("It's not in the Session so closing it:" + object.toString());
                    }
                    object.close();
                    iterator.remove();
                }
            }
        }
    } // run

    public Object getResult()
    {
        return result;
    } // getResult

    public boolean isException()
    {
        return isException;
    } // isException

    public Binding getBinding()
    {
        return binding;
    }

    public void setBinding(Binding binding)
    {
        this.binding = binding;
    }

    public Object[] getArgs()
    {
        return args;
    }

    public void setArgs(Object[] args)
    {
        this.args = args;
    }

    public String getScript()
    {
        return script;
    }

    public void setScript(String script)
    {
        this.script = script;
    }
    
    public String getScriptName()
    {
        return scriptName;
    }

    public void setScriptName(String scriptName)
    {
        this.scriptName = scriptName;
    }
    
    public boolean isCacheable()
    {
        return cacheable;
    }

    public void setCacheable(boolean cacheable)
    {
        this.cacheable = cacheable;
    }

    public SessionMap getSessionMap()
    {
        return sessionMap;
    }

    public void setSessionMap(SessionMap sessionMap)
    {
        this.sessionMap = sessionMap;
    }

} // ThreadGroovy
