/******************************************************************************
* (C) Copyright 2014
*
* Resolve Systems, LLC
*
* All rights reserved
* This software is distributed under license and may not be used, copied,
* modified, or distributed without the express written permission of
* Resolve Systems, LLC.
*
******************************************************************************/

package com.resolve.exception;

public class ActionTaskAbortException extends Exception
{
    private static final long serialVersionUID = -86722461925065814L;

	public ActionTaskAbortException(String msg)
    {
        super(msg);
    } // ActionTaskAbortException

    public ActionTaskAbortException(String msg, Throwable t)
    {
        super(msg, t);
    } // ActionTaskAbortException

} // ActionTaskAbortException
