package com.resolve.testslib.integration.elasticsearch;

import java.io.File;
import java.io.IOException;
import java.net.ServerSocket;
import java.util.Arrays;
import java.util.Collection;

import org.elasticsearch.common.io.FileSystemUtils;
import org.elasticsearch.common.settings.Settings;
import org.elasticsearch.node.InternalSettingsPreparer;
import org.elasticsearch.node.Node;
import org.elasticsearch.plugins.Plugin;
import org.elasticsearch.transport.Netty4Plugin;

import com.resolve.testslib.util.IntegrationTestUtil;

public class ElasticSearchManager
{

    private static Node esServer;
    private static final String ES_WORKING_DIR = "/target/es";
    private static final String HTTP_TRANSPORT_PORT = "12003";

    private static class PluginConfigurableNode extends Node
    {
        public PluginConfigurableNode(Settings settings, Collection<Class<? extends Plugin>> classpathPlugins)
        {
            super(InternalSettingsPreparer.prepareEnvironment(settings, null), classpathPlugins);
        }
    }
    
    public static void startElasticSearch(String httpPort) throws Exception
    {
        if(IntegrationTestUtil.isLocalPortInUse(httpPort)) {
            return;
        }
        
        Settings settings =  Settings.builder()
                        .put("path.home", ES_WORKING_DIR)
                        .put("path.data", ES_WORKING_DIR)
                        .put("path.logs", ES_WORKING_DIR)
                        .put("http.port", httpPort)
                        .put("transport.tcp.port", HTTP_TRANSPORT_PORT)
                        .put("transport.type", "netty4")
                        .put("http.type", "netty4")
                        .put("http.enabled", "true")
                        .build();

        removeOldDataDir(ES_WORKING_DIR);

        Collection plugins = Arrays.asList(Netty4Plugin.class);
        esServer = new PluginConfigurableNode(settings, plugins).start();
    }

    public static void stopElasticsearch() throws IOException
    {
        if(esServer != null) {
            esServer.close();
        }
    }
    
    private static void removeOldDataDir(String datadir) throws Exception
    {
        File dataDir = new File(datadir);
        if (dataDir.exists())
        {
            FileSystemUtils.deleteSubDirectories(dataDir.toPath());
        }
    }

}
