package com.resolve.testslib.util;

import java.io.IOException;
import java.net.ServerSocket;

public class IntegrationTestUtil
{
    public static boolean isLocalPortInUse(String port)
    {
        try
        {
            // ServerSocket try to open a LOCAL port
            new ServerSocket(Integer.parseInt(port)).close();
            // local port can be opened, it's available
            return false;
        }
        catch (IOException e)
        {
            // local port cannot be opened, it's in use
            return true;
        }
    }

}
