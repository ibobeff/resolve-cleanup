package com.resolve.testslib.integration.rabbitmq;

import org.apache.qpid.server.Broker;
import org.apache.qpid.server.BrokerOptions;

import com.google.common.io.Files;
import com.resolve.testslib.util.IntegrationTestUtil;

public class BrokerManager
{
    private static final String INITIAL_CONFIG_PATH = "broker_config.json";

    private static final String DEFAULT_RABBITMQ_SSL_PORT = "4004";
    private static final String INITIAL_CONFIG_PATH_SSL = "broker_config_ssl.json";
    private static final String KEYSTORE_FILE = "my_keystore";

    private final Broker broker = new Broker();

    public void startBroker(String port, boolean isSSL) throws Exception
    {
        if(IntegrationTestUtil.isLocalPortInUse(port)) {
            return;
        }
        
        final BrokerOptions brokerOptions = new BrokerOptions();
        String path = Files.createTempDir().getAbsolutePath();
        brokerOptions.setConfigProperty("qpid.work_dir", path);

        if (isSSL)
        {
            System.setProperty("keystore_path", BrokerOptions.class.getClassLoader().getResource(KEYSTORE_FILE).toExternalForm());
            brokerOptions.setInitialConfigurationLocation(BrokerOptions.class.getClassLoader().getResource(INITIAL_CONFIG_PATH_SSL).toExternalForm());
            if(port==null) {
                port = DEFAULT_RABBITMQ_SSL_PORT;
            }
        }
        else
        {
            brokerOptions.setInitialConfigurationLocation(BrokerOptions.class.getClassLoader().getResource(INITIAL_CONFIG_PATH).toExternalForm());
        }

        brokerOptions.setConfigProperty("qpid.amqp_port", port);

        broker.startup(brokerOptions);
    }

    public void stopBroker()
    {
        if(broker != null)
        {
            broker.shutdown();
        }
    }

}
