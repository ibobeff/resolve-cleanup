package com.resolve.testslib.integration.hsql;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

import org.hibernate.SessionFactory;
import org.hibernate.boot.registry.StandardServiceRegistryBuilder;
import org.hibernate.cfg.Configuration;
import org.hsqldb.Server;
import org.hsqldb.cmdline.SqlFile;
import org.hsqldb.cmdline.SqlToolError;
import org.hsqldb.persist.HsqlProperties;
import org.hsqldb.server.ServerAcl.AclFormatException;

public class InMemoryDBUtil {
	
	private static final String HSQL_SQL_INIT_FILE = "create.sql";
	private static final String HSQL_SQL_TRUNCATE_FILE = "truncate.sql";
	private static final String HSQL_DB_NAME = "mydb";
	private static final String HSQL_DB_PORT = "9001";
	private static final String HSQL_DB_TARGET_LOCATION = "./target/db/"; 

	private static SessionFactory sessionFactory;
	private static Server server;

	public static SessionFactory getSessionFactory() {

		if (sessionFactory == null) {
			sessionFactory = initSessionFactory();
		}

		return sessionFactory;
	}	
	
	public static void startInMemoryDBServer() throws IOException, AclFormatException, ClassNotFoundException {
		
		if (server == null || server.isNotRunning()) {
			initInMemoryDBServer();
		}
		server.start();
	}


	public static void stopInMemoryDBServer() {
		
		server.stop();
	}
	
	private static void initInMemoryDBServer() throws IOException, AclFormatException, ClassNotFoundException {
		
		HsqlProperties p = new HsqlProperties();
		
		p.setProperty("server.database.0", HSQL_DB_TARGET_LOCATION + HSQL_DB_NAME);
		p.setProperty("server.dbname.0", HSQL_DB_NAME);
		p.setProperty("server.port", HSQL_DB_PORT);
		
		server = new Server();
		server.setProperties(p);
		server.setLogWriter(null); // can use custom writer
		server.setErrWriter(null); // can use custom writer
		Class.forName("org.hsqldb.jdbc.JDBCDriver");
	}	
	
	public static void initResolveDB() throws IOException, SqlToolError, SQLException {	
		
		executeSQLFile(HSQL_SQL_INIT_FILE, true);	
	    
	}
	
	public static void initInMemoryDB(String... filePaths) throws IOException, SqlToolError, SQLException {	
		
		for (String filePath : filePaths) {
			executeSQLFile(filePath, false);	
		}	    
	    
	}	
	
	
	public static void clearInMemoryDB() throws IOException, SqlToolError, SQLException {	
		
		executeSQLFile(HSQL_SQL_TRUNCATE_FILE, true);	
        
	}
	
	private static SessionFactory initSessionFactory() {
		
        Configuration configuration = new Configuration();
        
        configuration.configure(InMemoryDBUtil.class.getClassLoader()
        		.getResource("hibernate.cfg.xml").toExternalForm());
        
        StandardServiceRegistryBuilder ssrb = new StandardServiceRegistryBuilder()
        		.applySettings(configuration.getProperties());
        
        return configuration.buildSessionFactory(ssrb.build());
        
	}
	
	
	private static void executeSQLFile(String filename, boolean isResource) throws SQLException, IOException, SqlToolError {
		
		Connection connection = null;
		InputStreamReader inputStreamReader = null;		
		InputStream stream = null;
		
		if (isResource) {
			stream = InMemoryDBUtil.class.getResourceAsStream("/" + filename);
		} else {
			stream = new FileInputStream(filename);
		}		
		
		try {
			
			inputStreamReader = new InputStreamReader(stream);
			connection = DriverManager.getConnection("jdbc:hsqldb:file:" + HSQL_DB_NAME, "sa", "");
			SqlFile sqlFile = new SqlFile(inputStreamReader, filename, System.out, "UTF-8", false, new File("."));
			sqlFile.setConnection(connection);
	        sqlFile.execute();
	        
		} finally {
			
			if (inputStreamReader != null) {
				inputStreamReader.close();
			}
			
			if (connection != null) {
				connection.close();
			}
		}
	}

}
