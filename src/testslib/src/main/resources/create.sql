DROP TABLE resolve_event IF EXISTS; 

CREATE TABLE resolve_event 
(
   u_value VARCHAR(1024) DEFAULT NULL,
   sys_id VARCHAR(32) NOT NULL,
   sys_updated_by VARCHAR(255) DEFAULT NULL,
   sys_updated_on DATETIME DEFAULT NULL,
   sys_created_by VARCHAR(255) DEFAULT NULL,
   sys_created_on DATETIME DEFAULT NULL,
   sys_mod_count INT DEFAULT NULL,
   PRIMARY KEY (sys_id)
);

DROP TABLE resolve_sequence_generator IF EXISTS; 

CREATE TABLE resolve_sequence_generator
(
   sys_id varchar(32) NOT NULL,
   u_name varchar(100) DEFAULT NULL,
   u_number int DEFAULT NULL,
   PRIMARY KEY (sys_id)
 );
 
DROP TABLE resolve_shared_object IF EXISTS; 
 
CREATE TABLE resolve_shared_object 
(
  sys_id varchar(32) NOT NULL,
  u_content BLOB(1000),
  u_name varchar(400) DEFAULT NULL,
  u_number int DEFAULT NULL,
  PRIMARY KEY (sys_id)
) ;

DROP TABLE archive_action_result IF EXISTS; 
 
CREATE TABLE archive_action_result (
	sys_id varchar(32) NOT NULL,
	u_address varchar(100),
	u_completion varchar(40),
	u_condition varchar(40),
	u_duration int,
	u_esbaddr varchar(100),
	u_severity varchar(40),
	u_timestamp bigint,
	sys_created_by varchar(255),
	sys_created_on datetime,
	sys_mod_count int,
	sys_updated_by varchar(255),
	sys_updated_on datetime(3),
	u_action_result_lob varchar(32),
	u_execute_request varchar(32),
	u_execute_result varchar(32),
	u_problem varchar(32),
	u_process varchar(32),
	u_target varchar(32),
	u_target_guid varchar(32),
	u_actiontask varchar(32),
    PRIMARY KEY (sys_id, sys_created_on)
);

DROP TABLE archive_action_result_lob IF EXISTS; 

CREATE TABLE archive_action_result_lob (
	sys_id varchar(32) NOT NULL,
	u_detail varchar(1000),
	u_summary varchar(1000),
	sys_created_by varchar(255),
	sys_created_on datetime,
	sys_mod_count int,
	sys_updated_by varchar(255),
	sys_updated_on datetime,
    PRIMARY KEY (sys_id, sys_created_on)
) ;

DROP TABLE archive_execute_dependency IF EXISTS; 

CREATE TABLE archive_execute_dependency (
	sys_id varchar(32) NOT NULL,
	u_completion bit(1),
	u_condition varchar(40),
	u_execute varchar(32),
	u_expression varchar(4000),
	u_merge varchar(40),
	u_severity varchar(40),
	u_type varchar(40),
	sys_created_by varchar(255),
	sys_created_on datetime,
	sys_mod_count int,
	sys_updated_by varchar(255),
	sys_updated_on datetime,
	u_execute_request varchar(32),
    PRIMARY KEY (sys_id, sys_created_on)
) ;

DROP TABLE archive_execute_request IF EXISTS; 

CREATE TABLE archive_execute_request (
	sys_id varchar(32) NOT NULL,
	u_duration int,
	u_event_id varchar(255),
	u_execute_status varchar(40),
	u_node_id varchar(255),
	u_node_label varchar(255),
	u_number varchar(40),
	u_trigger varchar(100),
	u_wiki varchar(255),
	sys_created_by varchar(255),
	sys_created_on datetime,
	sys_mod_count int,
	sys_updated_by varchar(255),
	sys_updated_on datetime,
	u_problem varchar(32),
	u_process varchar(32),
	u_task varchar(32),
    PRIMARY KEY (sys_id, sys_created_on)
);

DROP TABLE archive_execute_result IF EXISTS; 

CREATE TABLE archive_execute_result (
	sys_id varchar(32) NOT NULL,
	u_address varchar(100),
	u_duration int,
	u_returncode int,
	u_target varchar(32),
	sys_created_by varchar(255),
	sys_created_on datetime,
	sys_mod_count int,
	sys_updated_by varchar(255),
	sys_updated_on datetime,
	u_request varchar(32),
	u_execute_result_lob varchar(32),
    PRIMARY KEY (sys_id, sys_created_on)
) ;

DROP TABLE archive_execute_result_lob IF EXISTS; 

CREATE TABLE archive_execute_result_lob (
	sys_id varchar(32) NOT NULL,
	u_command varchar(1000),
	u_raw varchar(1000),
	sys_created_by varchar(255),
	sys_created_on datetime,
	sys_mod_count int,
	sys_updated_by varchar(255),
	sys_updated_on datetime,
    PRIMARY KEY (sys_id, sys_created_on)
) ;

DROP TABLE archive_process_request IF EXISTS; 

CREATE TABLE archive_process_request (
	sys_id varchar(32) NOT NULL,
	u_duration int,
	u_number varchar(40),
	u_status varchar(40),
	u_timeout varchar(40),
	u_wiki varchar(255),
	sys_created_by varchar(255),
	sys_created_on datetime,
	sys_mod_count int,
	sys_updated_by varchar(255),
	sys_updated_on datetime,
	u_problem varchar(32),
    PRIMARY KEY (sys_id, sys_created_on)
) ;

DROP TABLE archive_worksheet IF EXISTS; 

CREATE TABLE archive_worksheet (
	sys_id varchar(32) NOT NULL,
	u_alert_id varchar(333),
	u_condition varchar(40),
	u_correlation_id varchar(333),
	u_debug varchar(1000),
	u_description varchar(1000),
	u_number varchar(40),
	u_reference varchar(333),
	u_severity varchar(40),
	u_summary varchar(1000),
	u_work_notes varchar(1000),
	u_worksheet varchar(1000),
	sys_created_by varchar(255),
	sys_created_on datetime,
	sys_mod_count int,
	sys_updated_by varchar(255),
	sys_updated_on datetime,
	u_assigned_to varchar(32),
    PRIMARY KEY (sys_id, sys_created_on)
) ;

DROP TABLE archive_worksheet_debug IF EXISTS; 

CREATE TABLE archive_worksheet_debug (
	sys_id varchar(32) NOT NULL,
	u_debug varchar(1000),
	u_timestamp bigint,
	sys_created_by varchar(40),
	sys_created_on datetime,
	sys_mod_count int,
	sys_updated_by varchar(40),
	sys_updated_on datetime,
    PRIMARY KEY (sys_id, sys_created_on)
) ;





