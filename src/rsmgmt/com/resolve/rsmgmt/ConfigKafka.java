/******************************************************************************
* (C) Copyright 2014
*
* Resolve Systems, LLC
*
* All rights reserved
* This software is distributed under license and may not be used, copied,
* modified, or distributed without the express written permission of
* Resolve Systems, LLC.
*
******************************************************************************/

package com.resolve.rsmgmt;

import com.resolve.util.Properties;

public class ConfigKafka
{
    private static final String instanceName = "kafka";
	private String user;
    
    public ConfigKafka(Properties properties) {
	}
    
    public void setUser(String user)
    {
        this.user = user;
    }

    public String getUser()
    {
        return this.user;
    }

    
}//ConfigKafka
