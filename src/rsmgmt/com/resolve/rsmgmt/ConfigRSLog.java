package com.resolve.rsmgmt;

import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import com.resolve.util.CryptUtils;
import com.resolve.util.Log;
import com.resolve.util.Properties;
import com.resolve.util.StringUtils;

public class ConfigRSLog {
    private String instanceName = "rslog";
    private Properties rslogProperties;
    private String logLocation;

    public ConfigRSLog() {
        rslogProperties = new Properties();
    }

    public ConfigRSLog(Properties properties) {
        this();
        for (Object key : properties.keySet()) {
            String strKey = key.toString();
            if (strKey.startsWith(instanceName + ".")) {
                String value = properties.getProperty(strKey).trim();
                if (value.contains("${")) {
                    value = properties.getReplacement(key).trim();
                }
                if (value.toUpperCase().startsWith("TO_ENC:")) {
                    if (StringUtils.isNotBlank(value.substring(7)) && !value.substring(7).toUpperCase().startsWith("NO_ENC:") && !value.substring(7).matches(".*(?<!\\\\)\\$\\{.*\\}.*")) {
                        try {
                            value = CryptUtils.encrypt(value.substring(7));
                        }
                        catch (Exception e) {
                            Log.log.error("Failed to encrypt blueprint value prefixed with TO_ENC: set for the key " + strKey + " due to error " + e.getMessage() + ", setting un-encrypted value into config.xml", e);
                        }
                    }
                    if (value.toUpperCase().startsWith("NO_ENC:")) {
                        value = value.substring(7);
                    }
                    if (value.startsWith("ENC")) {
                        try {
                            value = CryptUtils.decrypt(value);
                        }
                        catch (Exception e) {
                            Log.log.error("Failed to decrypt blueprint value", e);
                            value = "";
                        }
                    }
                }
                this.rslogProperties.put(key, value);
                if (key.equals("rslog.log.location")) {
                    logLocation = value;
                }
            }
        }
    }

    @SuppressWarnings("unchecked")
    public Map<String, Object> rslogXPathValues() {
        Map<String, Object> result = new HashMap<String, Object>();

        for (Object key : rslogProperties.keySet()) {
            String strKey = key.toString();
            String value = (String) rslogProperties.get(key);

            strKey = strKey.replaceFirst(instanceName, "").toUpperCase();
            strKey = strKey.replaceAll("(?<!\\\\)\\.", "/");
            strKey = strKey.replaceAll("\\\\\\.", ".");

            Pattern pattern = Pattern.compile("(.+)/(.+)/(\\d+)");
            Matcher matcher = pattern.matcher(strKey);
            if (matcher.matches()) {
                strKey = "." + matcher.group(1);
                String xdocKey = matcher.group(2);
                int listPosition = Integer.parseInt(matcher.group(3)) - 1;

                List<Map<String, String>> list = (List<Map<String, String>>) result.get(strKey);
                if (list == null) {
                    list = new LinkedList<Map<String, String>>();
                    result.put(strKey, list);
                }
                for (int j = list.size(); j <= listPosition; j++) {
                    list.add(new HashMap<String, String>());
                }
                Map<String, String> entry = list.get(listPosition);
                entry.put(xdocKey, value);
                list.set(listPosition, entry);
            }
            else {
                strKey = strKey.replaceFirst("^(.*)/(.*?)$", ".$1/@$2");
                result.put(strKey, value);
            }
        }

        return result;
    }

    public String getLogLocation() {
        return logLocation;
    }
}
