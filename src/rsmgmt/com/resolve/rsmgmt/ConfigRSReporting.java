/******************************************************************************
* (C) Copyright 2014
*
* Resolve Systems, LLC
*
* All rights reserved
* This software is distributed under license and may not be used, copied,
* modified, or distributed without the express written permission of
* Resolve Systems, LLC.
*
******************************************************************************/

package com.resolve.rsmgmt;

import com.resolve.util.Properties;

public class ConfigRSReporting
{
    private static final String instanceName = "rsreporting";
	private String user;
    
    public ConfigRSReporting(Properties properties) {
	}
    
    public void setUser(String user)
    {
        this.user = user;
    }

    public String getUser()
    {
        return this.user;
    }
    
}//ConfigGreenplum
