package com.resolve.rsmgmt;

import java.util.HashMap;
import java.util.Map;

import com.resolve.util.CryptUtils;
import com.resolve.util.Log;
import com.resolve.util.Properties;
import com.resolve.util.StringUtils;

public class ConfigLogstash {
    private static final String CONFIG_PREFIX = "logstash";
    private static final String JDBC_MYSQL_DB_URL_PREFIX = "jdbc:mysql://";
    private static final String JDBC_MARIADB_DB_URL_PREFIX = "jdbc:mariadb://";
    private static final String MARIADB_JDBC_DRIVER_LIBRARY = "mariadb-java-client.jar";
    private static final String ORACLE_JDBC_DRIVER_LIBRARY = "ojdbc8.jar";
    private static final String COM_PREFIX = "com.";
    private static final String ORG_PREFIX = "org.";
    private static final String JDBC_DRIVER_CLASS_SUFFIX = ".jdbc.Driver";
    
	private String user = "resolve";
    private Properties logstashProperties = new Properties();
    private Properties resolveConfProperties = new Properties();
    private Properties startupProperties = new Properties();
    private Properties runProperties = new Properties();
    private Properties envProperties = new Properties();

    public ConfigLogstash(Properties properties) {
        for (Object key : properties.keySet()) {
        	String strKey = key.toString();
            if (strKey.startsWith(CONFIG_PREFIX + "."))
            {
                String value = properties.getProperty(strKey) != null ? properties.getProperty(strKey) : "";
                value = value.trim();
                if (value.contains("${"))
                {
                    value = properties.getReplacement(key).trim();
                }
                if (value.toUpperCase().startsWith("TO_ENC:"))
                {
                    if (StringUtils.isNotBlank(value.substring(7)) && !value.substring(7).toUpperCase().startsWith("NO_ENC:") && !value.substring(7).matches(".*(?<!\\\\)\\$\\{.*\\}.*"))
                    {
                        try
                        {
                            value = CryptUtils.encrypt(value.substring(7));
                        }
                        catch (Exception e)
                        {
                            Log.log.error("Failed to encrypt blueprint value prefixed with TO_ENC: set for the key " + strKey + 
                                          " due to error " + e.getMessage() + ", setting un-encrypted value into config.xml", e);
                        }
                    }
                }
                if (value.toUpperCase().startsWith("NO_ENC:"))
                {
                    value = value.substring(7);
                }
                if (value.startsWith("ENC"))
                {
                    try
                    {
                        value = CryptUtils.decrypt(value);
                    }
                    catch (Exception e)
                    {
                        Log.log.error("Failed to decrypt blueprint value", e);
                        value = "";
                    }
                }	
                if (value != null)
                {
		            if (strKey.startsWith(CONFIG_PREFIX + ".yml")) {
		                logstashProperties.put(key, value);
		            }
		            if (strKey.startsWith(CONFIG_PREFIX + ".resolveconf")) {
		            	resolveConfProperties.put(key, value);
		            }
		            if (strKey.startsWith(CONFIG_PREFIX + ".startup")) {
		            	startupProperties.put(key, value);
		            }
		            if (strKey.startsWith(CONFIG_PREFIX + ".run")) {
		            	runProperties.put(key, value);
		            }
					if (strKey.equals(CONFIG_PREFIX + ".run.R_USER")){
						this.user = value;
					}
		            if (strKey.startsWith(CONFIG_PREFIX + ".env")) {
		            	envProperties.put(key, value);
		            }
                }
            }
        }
    }
    
    public Properties logstashResolveConfReplaceValues() {
    	String dbType = resolveConfProperties.get(CONFIG_PREFIX + ".resolveconf.input.DB_TYPE");
    	String jdbcDrvrLibJar = resolveConfProperties.get(CONFIG_PREFIX + ".resolveconf.input.jdbc.jdbc_driver_library_jar");
    	
    	if (StringUtils.isBlank(dbType)) {
    		Log.log.warn(String.format("Missing Logstash DB Type, assuming MySQL/Mariadb with jdbc driver library set to %s",
    								   MARIADB_JDBC_DRIVER_LIBRARY));
    		dbType = "mysql";
    		jdbcDrvrLibJar = MARIADB_JDBC_DRIVER_LIBRARY;
    	} else {
    		if (dbType.equalsIgnoreCase("mysql") || dbType.equalsIgnoreCase("mariadb")) {
    			if (StringUtils.isBlank(jdbcDrvrLibJar)) {
    				jdbcDrvrLibJar = MARIADB_JDBC_DRIVER_LIBRARY;
    				Log.log.info(String.format("Missing jdbc driver library jar, assuming %s with jdbc driver library " +
    										   "set to %s", dbType, MARIADB_JDBC_DRIVER_LIBRARY));
    			} else if (dbType.equalsIgnoreCase("mysql")) {
    				Log.log.warn(String.format("Logstash DB Type is MySQL and jdbc driver library jar is %s, " +
    										   "please make sure jar exists in logstash/lib directory", jdbcDrvrLibJar));
    			}
    		} else if (dbType.toLowerCase().startsWith("oracle")) {
    			jdbcDrvrLibJar = ORACLE_JDBC_DRIVER_LIBRARY;
    		}
    	}
    	
    	if (dbType.equalsIgnoreCase("mysql") || dbType.equalsIgnoreCase("mariadb")) {
    		String dbUrl = resolveConfProperties.get(CONFIG_PREFIX + ".resolveconf.input.DB_URL");
    		if (StringUtils.isBlank(dbUrl)) {
    			String dbHost = resolveConfProperties.get(CONFIG_PREFIX + ".resolveconf.input.DB_HOST");
    			String dbName = resolveConfProperties.get(CONFIG_PREFIX + ".resolveconf.input.DB_NAME");
    			dbUrl = String.format("%s%s",
    								  ((dbType.equalsIgnoreCase("mysql") && 
    									!jdbcDrvrLibJar.equals(MARIADB_JDBC_DRIVER_LIBRARY)) ? 
    								   JDBC_MYSQL_DB_URL_PREFIX : JDBC_MARIADB_DB_URL_PREFIX), dbHost);
    			
    			if (!dbHost.contains(":")) {
    				dbUrl += ":3306";
    			}
    			
    			dbUrl += "/" + dbName;	
    		} else {
    			// Make sure DB URL is always in sync with jdbc driver jar 
    			if (dbType.equalsIgnoreCase("mysql") && !jdbcDrvrLibJar.equals(MARIADB_JDBC_DRIVER_LIBRARY)) {
    				dbUrl = StringUtils.replaceOnce(dbUrl, JDBC_MARIADB_DB_URL_PREFIX, JDBC_MYSQL_DB_URL_PREFIX);
    			} else {
    				dbUrl = StringUtils.replaceOnce(dbUrl, JDBC_MYSQL_DB_URL_PREFIX, JDBC_MARIADB_DB_URL_PREFIX );
    			}
    		}
    		resolveConfProperties.put("logstash.resolveconf.input.jdbc.jdbc_driver_library", 
    								  String.format("%s/logstash/lib/%s", Main.main.getResolveHome(), jdbcDrvrLibJar));
    		resolveConfProperties.put("logstash.resolveconf.input.jdbc.jdbc_driver_class",
    								  String.format("%s%s%s",
    								  		 		((dbType.equalsIgnoreCase("mysql") && 
    								  				  !jdbcDrvrLibJar.equals(MARIADB_JDBC_DRIVER_LIBRARY)) ?
    								  				 COM_PREFIX : ORG_PREFIX), dbType.toLowerCase(), JDBC_DRIVER_CLASS_SUFFIX));
    		resolveConfProperties.put("logstash.resolveconf.input.jdbc.jdbc_connection_string", dbUrl);
    	} else if (dbType.toLowerCase().startsWith("oracle")) {
    		String dbUrl = resolveConfProperties.get(CONFIG_PREFIX + ".resolveconf.input.DB_URL");
    		if (StringUtils.isBlank(dbUrl)) {
    			String dbHost = resolveConfProperties.get(CONFIG_PREFIX + ".resolveconf.input.DB_HOST");
    			String dbName = resolveConfProperties.get(CONFIG_PREFIX + ".resolveconf.input.DB_NAME");
    			dbUrl = "jdbc:oracle:thin:@"+dbHost;
    			if (!dbHost.contains(":")) {
    				dbUrl += ":1521";
    			}
    			dbUrl += ":" + dbName;
    		}
    		resolveConfProperties.put("logstash.resolveconf.input.jdbc.jdbc_driver_library", Main.main.getResolveHome() + "/logstash/lib/ojdbc8.jar");
    		resolveConfProperties.put("logstash.resolveconf.input.jdbc.jdbc_driver_class", "Java::oracle.jdbc.driver.OracleDriver");
    		resolveConfProperties.put("logstash.resolveconf.input.jdbc.jdbc_connection_string", dbUrl);
    	}
    	else {
    		Log.log.error("Unknown DB Type: " + dbType);
    	}
    	
    	if (StringUtils.isNotBlank(resolveConfProperties.get(CONFIG_PREFIX + ".resolveconf.elasticsearch.NODES"))) {
	    	String[] elasticsearchNodes = resolveConfProperties.get(CONFIG_PREFIX + ".resolveconf.elasticsearch.NODES").split(",");
	    	String port = resolveConfProperties.get(CONFIG_PREFIX + ".resolveconf.elasticsearch.PORT", "9200");
	    	StringBuilder hostAry = new StringBuilder();
	    	for (String node : elasticsearchNodes) {
	    		if (hostAry.length() == 0) {
	    			hostAry.append("[\"" + node + ":" + port + "\"");
	    		}
	    		else {
	    			hostAry.append(", \"" + node + ":" + port + "\"");
	    		}
	    	}
	    	hostAry.append("]");
	    	resolveConfProperties.put("logstash.resolveconf.elasticsearch.hosts", hostAry.toString());
    	}
    	else {
    		Log.log.error("Missing Elasticsearch Host Configuration");
    	}
    	
    	return resolveConfProperties;
    }
    
    public Map<String, String> logstashEnvReplaceValues()
    {
        Map<String, String> result = new HashMap<String, String>();
        
        for (Object key : envProperties.keySet())
        {
            String strKey = key.toString();
            String value = envProperties.getProperty(strKey) != null ? envProperties.getProperty(strKey) : "";
            
            strKey = strKey.replaceFirst(CONFIG_PREFIX + ".env.", "");
            value = value.replaceAll("\\\\", "/");
            if (value.equalsIgnoreCase("true") || value.equalsIgnoreCase("false"))
            {
                value = value.toUpperCase();
            }
            
            strKey = strKey.toUpperCase();
            String replaceRegex = strKey + "=.*";
            String replaceString = strKey + "=" + value;

            result.put(replaceRegex, replaceString);
        }
        return result;
    } 

	public Properties logstashStartupReplaceValues() {
		Properties result = new Properties();
        
        for (Object key : startupProperties.keySet())
        {
            String strKey = key.toString();
            String value = startupProperties.getProperty(strKey) != null ? startupProperties.getProperty(strKey) : "";;
            
            strKey = strKey.replaceFirst(CONFIG_PREFIX + ".startup.", "");
            
            value = value.replaceAll("\\\\", "/");
            if (value.equalsIgnoreCase("true") || value.equalsIgnoreCase("false"))
            {
                value = value.toUpperCase();
            }
            
            strKey = strKey.toUpperCase();
            String replaceRegex = strKey + "=.*";
            String replaceString = strKey + "=" + value;

            result.put(replaceRegex, replaceString);
        }
        return result;	
	}

	public Properties logstashRunReplaceValues() {
		Properties result = new Properties();
        
        for (Object key : runProperties.keySet())
        {
            String strKey = key.toString();
            String value = runProperties.getProperty(strKey) != null ? runProperties.getProperty(strKey) : "";;;
            
            strKey = strKey.replaceFirst(CONFIG_PREFIX + ".run.", "");
            
            if (strKey.equalsIgnoreCase("Xms") || strKey.equalsIgnoreCase("mms"))
            {
                if (value.endsWith("M"))
                {
                    value = value.substring(0, value.length()-1);
                }
                result.put("Xms\\d+M", "Xms" + value + "M");
                result.put("MMS=.*", "MMS=" + value);
            }
            else if (strKey.equalsIgnoreCase("Xmx") || strKey.equalsIgnoreCase("mmx"))
            {
                if (value.endsWith("M"))
                {
                    value = value.substring(0, value.length()-1);
                }
                result.put("Xmx\\d+M", "Xmx" + value + "M");
                result.put("MMX=.*", "MMX=" + value);
            }
            else
            {
	            value = value.replaceAll("\\\\", "/");
	            if (value.equalsIgnoreCase("true") || value.equalsIgnoreCase("false"))
	            {
	                value = value.toUpperCase();
	            }
	            
	            strKey = strKey.toUpperCase();
	            String replaceRegex = strKey + "=.*";
	            String replaceString = strKey + "=" + value;
	
	            result.put(replaceRegex, replaceString);
            }
        }
        return result;	
	}

	public Properties logstashReplaceValues() {
		return logstashProperties;
	}
	
	public void setUser(String user)
    {
        this.user = user;
    }

	
	public String getUser()
    {
        return this.user;
    }
}
