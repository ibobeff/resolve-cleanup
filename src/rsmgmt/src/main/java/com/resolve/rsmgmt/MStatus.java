/******************************************************************************
 * (C) Copyright 2014
 *
 * Resolve Systems, LLC
 *
 * All rights reserved This software is distributed under license and may not be
 * used, copied, modified, or distributed without the express written permission
 * of Resolve Systems, LLC.
 *
 ******************************************************************************/

package com.resolve.rsmgmt;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.apache.commons.io.input.Tailer;
import org.apache.commons.io.input.TailerListenerAdapter;
import org.elasticsearch.action.admin.cluster.stats.ClusterStatsNodes;
import org.elasticsearch.action.admin.cluster.stats.ClusterStatsNodes.JvmStats;
import org.elasticsearch.action.admin.cluster.stats.ClusterStatsResponse;
import org.elasticsearch.client.Requests;

import com.resolve.execute.ExecuteOS;
import com.resolve.rsbase.ConfigId;
import com.resolve.rsbase.MainBase;
import com.resolve.search.SearchAdminAPI;
import com.resolve.sql.SQL;
import com.resolve.sql.SQLConnection;
import com.resolve.util.Constants;
import com.resolve.util.ERR;
import com.resolve.util.FileUtils;
import com.resolve.util.Log;
import com.resolve.util.MetricThreshold;
import com.resolve.util.StringUtils;
import com.resolve.util.XDoc;

import net.sf.json.JSONObject;

public class MStatus
{
    static Main main = (Main) MainBase.main;
    static int dbTimeout = 5; // how long to wait for heartbeat completion
                              // before notifying
    static int esbTimeout = 600000; // how many miliseconds without a heartbeat
                                    // message before throwing an error
    static long lastESBHeartbeat = System.currentTimeMillis();
    static boolean restartDown = false;

    static List<String> stdoutErrors = new LinkedList<String>();
    static List<String> suppressErrors = new LinkedList<String>();
    static List<String> ESErrors = new LinkedList<String>();
    static Pattern heartbeatPattern = Pattern.compile("HEARTBEAT:(\\w{3}-\\d{2}-\\d{4} \\d{2}:\\d{2}:\\d{2}.\\d{3} (-|\\+)(\\d{4}|(\\d{2}:\\d{2})))");
    static ThreadLocal<SimpleDateFormat> sdfThreadLocal = new ThreadLocal<SimpleDateFormat>()
    {
        @Override
        protected SimpleDateFormat initialValue()
        {
            return new SimpleDateFormat(Constants.HEARTBEAT_FORMAT);
        }
    };
    static Map<String, Date> lastHeartbeats = new HashMap<String, Date>();
    static int stdoutDelay = 1;
    private static Map<String, TailerAdapter> tailers = new HashMap<String, TailerAdapter>();
    private static Map<String, ESTailerAdapter> estailer = new HashMap<String, ESTailerAdapter>();
    private static MetricThreshold mthreshold = Metric.mthreshold;

    public String localStatus(Map params)
    {
        return localStatus();
    }

    public String localStatus()
    {
        String result = "";

        try
        {
            String os = System.getProperty("os.name");
            String cmd = null;
            ExecuteOS exe = new ExecuteOS(main.getProductHome());

            if (os.contains("Win"))
            {
                cmd = "sc query state= all";
            }
            else if (os.contains("SunOS"))
            {
                cmd = "/usr/ucb/ps -augxwww";
            }
            else
            {
                cmd = "ps -ef";
            }

            Log.log.trace("Getting Processes Command: " + cmd);
            String processes = exe.execute(cmd).toString();

            Log.log.trace("Running Processes: " + processes);

            if (main.isBlueprintRSMQ())
            {
                result += getStatus("rsmq", processes);
            }
            if (main.isBlueprintRSView())
            {
                result += "\n" + getStatus("rsview", processes);
            }
            if (main.isBlueprintRSControl())
            {
                result += "\n" + getStatus("rscontrol", processes);
            }
            if (main.isBlueprintRSMgmt())
            {
                result += "\n" + getStatus("rsmgmt", processes);
            }
            if (main.isBlueprintRSRemote())
            {
                List<String> rsremotes = main.getRSRemoteInstances();
                for (String rsremote : rsremotes)
                {
                    result += "\n" + getStatus(rsremote, processes);
                }
            }
            if (main.isBlueprintRSSearch())
            {
                result += "\n" + getStatus("rssearch", processes);
            }
        }
        catch (Exception e)
        {
            e.printStackTrace();
            Log.log.error("Failed to Gather Local Status", e);
            result = "";
        }
        Log.log.debug("Local Process Status:\n" + result);

        return result.trim();
    }// localStatus

    public void dbStatus()
    {
        Log.start("Starting Database Heartbeat Check", "debug");
        try
        {
            DBStatus dbStatus = DBStatus.getInstance();
            if (dbStatus.isBusy())
            {
                Log.alert(ERR.E20005, "DB Heartbeat Connection is Still in Use, Unable to Complete Heartbeat Check");
            }
            else
            {
                Thread dbTread = new Thread(dbStatus);
                dbTread.start();
                long startWait = System.currentTimeMillis();
                int waitTime = dbTimeout * 1000;
                long elapsedTime = 0;
                synchronized (dbStatus)
                {
                    while (elapsedTime < waitTime)
                    {
                        elapsedTime = System.currentTimeMillis() - startWait;
                        dbStatus.wait(1000);
                        if (dbStatus.isBusy())
                        {
                            Log.log.trace("DB Heartbeat Still Waiting");
                        }
                        else
                        {
                            Log.log.trace("DB Heartbeat Finished");
                            break;
                        }
                    }
                }
                if (elapsedTime > waitTime)
                {
                    Log.alert(ERR.E20005, "DB Heartbeat Failed to Complete in " + waitTime + " seconds");
                }
            }
        }
        catch (Throwable t)
        {
            Log.alert(ERR.E20005, t);
        }
        Log.duration("DB Status Check Completed", "debug");
    } // dbStatus

    public void esbStatus()
    {
        long lastHeartbeat = System.currentTimeMillis() - lastESBHeartbeat;
        if (lastHeartbeat > esbTimeout)
        {
            ERR errorCode = ERR.E20008;
            String message = errorCode.getMessage();
            message += " - ESB Heartbeat has not been received in " + (lastHeartbeat / 1000) + " seconds";

            // log fatal message
            Throwable t = new Exception(message);
            Log.log.fatal(message, t);
            System.out.println(message);
            t.printStackTrace();

            if (main.configAlert.isActive())
            {
                Map params = new HashMap();
                params.put("SEVERITY", "CRITICAL");
                params.put("COMPONENT", main.release.serviceName);
                params.put("CODE", errorCode);
                params.put("MESSAGE", message);
                (new MAlert()).alert(params);
            }
        }
        checkSystemClockReset();
    }// esbStatus

    public void stdoutStatus()
    {
        Log.log.debug("STDOUT Status Start");
        Map params = new HashMap();
        params.put(Constants.RSMGMT_MONITOR_COMPONENTS, "ALL");
        startStdoutMonitor(params);
        startESLogMonitor();
    }

    public String startESLogMonitor()
    {
        String result = "";
        Log.log.debug("Start Monitor for ESLogs");
        String ESlogName = main.blueprintProperties.get("CLUSTERNAME");
        File ESlog = FileUtils.getFile(main.getResolveHome() + "/elasticsearch/logs/" + ESlogName + ".log");
        Log.log.trace("Monitor File: " + ESlog.getAbsolutePath());
        if (ESlog.exists())
        {
            ESTailerAdapter searchAdapter = new ESTailerAdapter();
            estailer.put("RSSEARCH", searchAdapter);
            Tailer searchTailer = new Tailer(ESlog, searchAdapter, stdoutDelay * 1000, true);
            Thread t = new Thread(searchTailer);
            t.start();
            result += "\nRSSEARCH " + ESlogName + ".log monitor started";
        }
        else
        {
            Log.alert(ERR.E10013, "RSSEARCH file " + ESlog.getAbsolutePath() + " cannot be found to monitor");
            result += "\nWARNING!!! RSSEARCH file " + ESlog.getAbsolutePath() + " cannot be found to monitor";
        }
        Log.log.debug("Start ESLog tailing results: " + result);
        return result;
    }

    public String startStdoutMonitor(Map params)
    {
        String result = "";
        String components = params.get(Constants.RSMGMT_MONITOR_COMPONENTS) != null ? params.get(Constants.RSMGMT_MONITOR_COMPONENTS).toString() : null;
        Log.log.debug("Start Monitor for " + components);
        if (StringUtils.isNotBlank(components))
        {
            String[] componentAry = components.split(" ");
            for (String component : componentAry)
            {
                component = component.trim();
                boolean found = false;
                if (component.equalsIgnoreCase("RSVIEW") || component.equalsIgnoreCase("ALL"))
                {
                    Log.log.trace("Start RSView Tailing");
                    if (main.isBlueprintRSView())
                    {
                        TailerAdapter adapter = tailers.get("RSVIEW");
                        if (adapter != null && adapter.isRunning())
                        {
                            Log.log.warn("RSView catalina.out is already being monitored");
                            result += "\nRSView catalina.out is already being monitored";
                        }
                        else
                        {
                            File catalina = FileUtils.getFile(main.getResolveHome() + "/tomcat/logs/catalina.out");
                            Log.log.trace("Monitor File: " + catalina.getAbsolutePath());
                            if (catalina.exists())
                            {
                                TailerAdapter viewAdapter = new TailerAdapter("RSVIEW");
                                Tailer viewTailer = new Tailer(catalina, viewAdapter, stdoutDelay * 1000, true);
                                Thread t = new Thread(viewTailer);
                                t.start();
                                tailers.put("RSVIEW", viewAdapter);
                                result += "\nRSView catalina.out monitor started";
                            }
                            else
                            {
                                Log.alert(ERR.E10013, "RSView file " + catalina.getAbsolutePath() + " cannot be found to monitor");
                                result += "\nWARNING!!! RSVIEW file " + catalina.getAbsolutePath() + " cannot be found to monitor";
                            }
                        }
                    }
                    else
                    {
                        result += "\nRSVIEW is not configured on this system";
                    }
                    found = true;
                }
                if (component.equalsIgnoreCase("RSCONTROL") || component.equalsIgnoreCase("ALL"))
                {
                    Log.log.trace("Start RSControl Tailing");
                    if (main.isBlueprintRSControl())
                    {
                        TailerAdapter adapter = tailers.get("RSCONTROL");
                        if (adapter != null && adapter.isRunning())
                        {
                            Log.log.warn("RSControl stdout is already being monitored");
                            result += "\nRSControl stdout is already being monitored";
                        }
                        else
                        {
                            File stdout = FileUtils.getFile(main.getResolveHome() + "/rscontrol/log/stdout");
                            Log.log.trace("Monitor File: " + stdout.getAbsolutePath());
                            if (stdout.exists())
                            {
                                TailerAdapter controlAdapter = new TailerAdapter("RSCONTROL");
                                Tailer controlTailer = new Tailer(stdout, controlAdapter, stdoutDelay * 1000, true);
                                Thread t = new Thread(controlTailer);
                                t.start();
                                tailers.put("RSCONTROL", controlAdapter);
                                result += "\nRSControl stdout monitor started";
                            }
                            else
                            {
                                Log.alert(ERR.E10013, "RSControl file " + stdout.getAbsolutePath() + " cannot be found to monitor");
                                result += "\nWARNING!!! RSControl file " + stdout.getAbsolutePath() + " cannot be found to monitor";
                            }
                        }
                    }
                    else
                    {
                        result += "\nRSCONTROL is not configured on this system";
                    }
                    found = true;
                }
                if (component.equalsIgnoreCase("RSMGMT") || component.equalsIgnoreCase("ALL"))
                {
                    Log.log.trace("Start RSMgmt Tailing");
                    if (main.isBlueprintRSMgmt())
                    {
                        TailerAdapter adapter = tailers.get("RSMGMT");
                        if (adapter != null && adapter.isRunning())
                        {
                            Log.log.warn("RSMgmt stdout is already being monitored");
                            result += "\nRSMgmt stdout is already being monitored";
                        }
                        else
                        {
                            File stdout = FileUtils.getFile(main.getResolveHome() + "/rsmgmt/log/stdout");
                            Log.log.trace("Monitor File: " + stdout.getAbsolutePath());
                            if (stdout.exists())
                            {
                                TailerAdapter mgmtAdapter = new TailerAdapter("RSMGMT");
                                Tailer mgmtTailer = new Tailer(stdout, mgmtAdapter, stdoutDelay * 1000, true);
                                Thread t = new Thread(mgmtTailer);
                                t.start();
                                tailers.put("RSMGMT", mgmtAdapter);
                                result += "\nRSMgmt stdout monitor started";
                            }
                            else
                            {
                                Log.alert(ERR.E10013, "RSMgmt file " + stdout.getAbsolutePath() + " cannot be found to monitor");
                                result += "\nWARNING!!! RSMgmt file " + stdout.getAbsolutePath() + " cannot be found to monitor";
                            }
                        }
                    }
                    else
                    {
                        result += "\nRSMGMT is not configured on this system";
                    }
                    found = true;
                }
                if (main.isBlueprintRSRemote())
                {
                    List<String> rsremotes = main.getRSRemoteInstances();
                    for (String rsremote : rsremotes)
                    {
                        if (rsremote.equalsIgnoreCase(component) || component.equalsIgnoreCase("ALL"))
                        {
                            Log.log.trace("Start " + rsremote.toUpperCase() + " Tailing");
                            TailerAdapter adapter = tailers.get(rsremote.toUpperCase());
                            if (adapter != null && adapter.isRunning())
                            {
                                Log.log.warn(rsremote + " stdout is already being monitored");
                                result += "\n" + rsremote.toUpperCase() + " stdout is already being monitored";
                            }
                            else
                            {
                                File stdout = FileUtils.getFile(main.getResolveHome() + "/" + rsremote + "/log/stdout");
                                Log.log.trace("Monitor File: " + stdout.getAbsolutePath());
                                if (stdout.exists())
                                {
                                    TailerAdapter remoteAdapter = new TailerAdapter(rsremote.toUpperCase());
                                    Tailer remoteTailer = new Tailer(stdout, remoteAdapter, stdoutDelay * 1000, true);
                                    Thread t = new Thread(remoteTailer);
                                    t.start();
                                    tailers.put(rsremote.toUpperCase(), remoteAdapter);
                                    result += "\n" + rsremote.toUpperCase() + " stdout monitor started";
                                }
                                else
                                {
                                    Log.alert(ERR.E10013, rsremote.toUpperCase() + " file " + stdout.getAbsolutePath() + " cannot be found to monitor");
                                    result += "\nWARNING!!! " + rsremote.toUpperCase() + " file " + stdout.getAbsolutePath() + " cannot be found to monitor";
                                }
                            }
                            found = true;
                            if (!component.equalsIgnoreCase("ALL"))
                            {
                                break;
                            }
                        }
                    }
                }
                if (!found)
                {
                    Log.log.warn("Component " + component + " Not found on this server");
                    result += "\n" + component.toUpperCase() + " Not found on this server";
                }
            }
        }
        else
        {
            result = "No Components Passed to Start STDOUT Monitoring";
        }

        Log.log.debug("Start stdout tailing results: " + result);
        return result;
    }

    public String stopESLogMonitor()
    {
        String result = "";
        Log.log.debug("Stopping ESLog Monitor");
        ESTailerAdapter adapter = estailer.get("RSSEARCH");
        if (adapter != null && adapter.isRunning())
        {
            adapter.stopTail();
            result += "\n ESLog Monitor has been stopped";
            Log.log.info("ESLog Monitor has been stopped");
        }
        else
        {
            Log.log.debug("Tailer RSSEARCH already stopped. but still in map");
        }
        estailer.remove("RSSEARCH");
        Log.log.debug("ESLog Monitor Stop result: " + result);
        return result;
    }

    public String stopStdoutMonitor(Map params)
    {
        String result = "";
        String components = params.get(Constants.RSMGMT_MONITOR_COMPONENTS) != null ? params.get(Constants.RSMGMT_MONITOR_COMPONENTS).toString() : null;
        if (StringUtils.isNotBlank(components))
        {
            Log.log.debug("Stopping Stdout Monitor of Components: " + components);
            String[] componentAry = components.split(",");
            for (String component : componentAry)
            {
                component = component.trim();
                if (component.equalsIgnoreCase("ALL"))
                {
                    result += "\nStopping all active stdout tails";
                    for (String key : tailers.keySet())
                    {
                        TailerAdapter adapter = tailers.get(key);
                        if (adapter != null && adapter.isRunning())
                        {
                            adapter.stopTail();
                            result += "\n" + key + " stdout monitor has been stopped";
                            Log.log.info(key + " stdout monitor has been stopped");
                        }
                        else
                        {
                            Log.log.debug("Tailer " + key + " already stopped, but still in map");
                        }
                        tailers.remove(key);
                    }
                }
                else
                {
                    TailerAdapter adapter = tailers.get(component.toUpperCase());
                    if (adapter != null && adapter.isRunning())
                    {
                        adapter.stopTail();
                        result += "\n" + component.toUpperCase() + " stdout monitor has been stopped";
                        Log.log.info(component.toUpperCase() + " stdout monitor has been stopped");
                    }
                    else
                    {
                        result += "\n" + component.toUpperCase() + " stdout monitor is not active or has already been stopped";
                        Log.log.warn(component.toUpperCase() + " stdout monitor is not active or has already been stopped");
                    }
                    tailers.remove(component.toUpperCase());
                }
            }
        }
        else
        {
            result = "No Components Passed to Stop STDOUT Monitoring";
        }
        Log.log.debug("Stdout Monitor Stop result: " + result);

        return result;
    }

    public void esbHeartbeat()
    {
        Log.log.trace("Sending ESB Heartbeat");
        main.esb.sendInternalMessage(main.configId.guid, "MStatus.esbHeartbeat", new HashMap());
    }// esbHeartbeat

    public void esbHeartbeat(Map params)
    {
        Log.log.trace("Received ESB Heartbeat");
        setLastESBHeartbeat();
    }// esbHeartbeat

    /*
     * public void heartbeatStatus() { Connection conn = null;
     * 
     * try { conn = SQL.getConnection();
     * 
     * String sql = null;
     * 
     * if (main.configSQL.getDbtype().equalsIgnoreCase("mysql")) { sql =
     * "select u_updatedtime, u_runtime, u_stoptime, u_type from resolve_registration where u_is_monitored is true"
     * ; } else if (main.configSQL.getDbtype().equalsIgnoreCase("oracle")) { sql
     * =
     * "SELECT U_UPDATEDTIME, U_RUNTIME, U_STOPTIME, U_TYPE FROM RESOLVE_REGISTRATION WHERE U_IS_MONITORED IS TRUE"
     * ; } else if (main.configSQL.getDbtype().equalsIgnoreCase("db2")) { sql =
     * "SELECT U_UPDATEDTIME, U_RUNTIME, U_STOPTIME, U_TYPE FROM RESOLVE_REGISTRATION WHERE U_IS_MONITORED IS TRUE"
     * ; }
     * 
     * Statement stmt = conn.createStatement();
     * 
     * ResultSet rs = stmt.executeQuery(sql);
     * 
     * while (rs.next()) { Date registered = rs.getDate(1); Date runTime =
     * rs.getDate(2); Date stopTime = rs.getDate(3); String type =
     * rs.getString(4).toLowerCase();
     * 
     * if (runTime.after(stopTime)) { //component is up
     * 
     * //get registration interval String registrationIntervalStr =
     * main.blueprintProperties.get(type + ".registration.interval");
     * 
     * if (StringUtils.isEmpty(registrationIntervalStr)) {
     * registrationIntervalStr = "5"; }
     * 
     * int registrationInterval = Integer.parseInt(registrationIntervalStr);
     * Date now = GMTDate.getDate(); long lastRegistered = now.getTime() -
     * registered.getTime();
     * 
     * if (lastRegistered > (registrationInterval * 60 * 1000)) { //heartbeat
     * failure } } } } catch (Exception e) { Log.log.error(
     * "Failed to Monitor Heartbeat", e); } finally { if (conn != null) {
     * SQL.close(conn); } } }//heartbeatStatus
     */

    private String getStatus(String component, String processes) throws Exception
    {
//        ESLoggerFactory.getRootLogger().setLevel("DEBUG");
        Log.log.debug("Getting Local Status for " + component);
        String result = "";

        String os = System.getProperty("os.name");
        String regex = null;
        String processId = "";

        File lockFile = null;
        if (!os.contains("Win"))
        {
            if (component.equalsIgnoreCase("rsmq"))
            {
                if (Constants.ESB_RABBITMQ.equalsIgnoreCase(main.getConfigRSMQ().getProduct()))
                {
                    if (os.contains("SunOS"))
                    {
                        lockFile = FileUtils.getFile(main.getResolveHome() + "/rabbitmq/solaris/rabbitmq/sbin/lock");
                    }
                    else
                    {
                        lockFile = FileUtils.getFile(main.getResolveHome() + "/rabbitmq/linux64/rabbitmq/sbin/lock");
                    }
                }
            }
            else if (component.equalsIgnoreCase("rsview"))
            {
                lockFile = FileUtils.getFile(main.getResolveHome() + "/tomcat/bin/lock");
            }
            else if (component.equalsIgnoreCase("rszk"))
            {
                lockFile = FileUtils.getFile(main.getResolveHome() + "/zk/data/neo4j-coord.pid");
            }
            else if (component.equalsIgnoreCase("rssearch"))
            {
                lockFile = FileUtils.getFile(main.getResolveHome() + "/elasticsearch/bin/lock");
            }
            else
            {
                lockFile = FileUtils.getFile(main.getResolveHome() + "/" + component + "/bin/lock");
            }
            Log.log.trace("Using Lock File " + lockFile.getAbsolutePath());
        }

        if (os.contains("Win"))
        {
            regex = "(?msi).*^SERVICE_NAME:\\s+" + component + ".+?STATE\\s+:\\s+(\\d)\\s+(\\w+).*$";
        }
        else
        {
            if (component.equalsIgnoreCase("rsmq"))
            {
                if (Constants.ESB_RABBITMQ.equalsIgnoreCase(main.getConfigRSMQ().getProduct()))
                {
                    regex = "(?m)^(\\w+)\\s+(\\d+).+" + main.getResolveHome() + "/?rabbitmq/.+erlang/erts-7.3/bin/beam.*";
                }
            }
            else if (component.equalsIgnoreCase("rsview"))
            {
                regex = "(?m)^(\\w+)\\s+(\\d+).+" + main.getResolveHome() + "/?tomcat.+Bootstrap.*";
            }
            else if (component.equalsIgnoreCase("rszk"))
            {
                regex = "(?m)^(\\w+)\\s+(\\d+).+" + main.getResolveHome() + "/?zk/.+zookeeper.jar.*";
            }
            else if (component.equalsIgnoreCase("rssearch"))
            {
                regex = "(?m)^(\\w+)\\s+(\\d+).+bootstrap.Elasticsearch.*";
            }
            else
            {
                regex = "(?m)^(\\w+)\\s+(\\d+).+" + main.getResolveHome() + "/?" + component + "/.+resolve-\\w+.jar.*";
            }
        }

        Log.log.trace("Search Process Regex: " + regex);

        Pattern pattern = Pattern.compile(regex);
        Matcher matcher = pattern.matcher(processes);

        while (matcher.find())
        {
            if (StringUtils.isEmpty(processId))
            {
                processId = matcher.group(2);
            }
            else
            {
                processId += ", " + matcher.group(2);
            }
        }
        Log.log.info(component + " Process PID is " + processId);
        processId = processId.trim();
        if (StringUtils.isBlank(processId))
        {
            Log.log.info("No PID Found for " + component);
        }

        if (os.contains("Win"))
        {
            if (processId.equalsIgnoreCase("RUNNING"))
            {
                Log.log.info(component + " Process is " + processId);
                result = component + " UP";
            }
            else
            {
                Log.log.info(component + " Process is " + processId);
                result = component + " DOWN";
                // check managed - bring up if so
            }
        }
        else
        {
            if (lockFile.exists())
            {
                byte[] lockBytes = new byte[(int) lockFile.length()];
                InputStream is = new FileInputStream(lockFile);
                is.read(lockBytes);
                is.close();

                String lockId = new String(lockBytes);
                lockId = lockId.trim();
                Log.log.info("Lock file PID " + lockId);

                if (StringUtils.isEmpty(processId))
                {
                    Log.alert(ERR.E10012, component + " DOWN/HANGING - PID is not found but lock file still exists (" + lockId + ")");
                    Log.log.error(component + " Process is DOWN/HANGING");
                    result = component + " DOWN/HANGING: " + processId;
                    if (isRestartDown())
                    {
                        boolean removeLock = lockFile.delete();
                        Log.log.warn("Removing Bad Lock File: " + removeLock);
                        ExecuteOS exe = new ExecuteOS(main.getResolveHome());
                        String restart = exe.execute("bin/run.sh " + component).toString();
                        result += " Attempting Restart";
                        Log.log.warn("Restart results: " + restart);
                    }
                }
                else if (lockId.equals(processId))
                {
                    Log.log.warn(component + " Process is UP - PID: " + lockId);
                    result = component + " UP";
                }
                else
                {
                    // alert
                    Log.alert(ERR.E10010, component + " HANGING - PID: " + processId + " does not match value in Lock file (" + lockId + ")");
                    Log.log.error(component + " Process is UP/HANGING");
                    result = component + " UP/HANGING: " + processId;
                }
            }
            else if (StringUtils.isNotEmpty(processId))
            {
                // alert
                Log.alert(ERR.E10011, component + " HANGING - PID: " + processId + " is running without a Lock file");
                Log.log.error(component + " Process is DOWN/HANGING");
                result = component + " DOWN/HANGING: " + processId;
            }
            else
            {
                // check managed - bring up if so
                Log.log.warn(component + " Process is DOWN");
                result = component + " DOWN";
            }
        }

        return result;
    }// getStatus

    /**
     * Verifies if the System clock is set backward. In that case we need to
     * make sure that the heart beat counter is reset as well. Otherwise it will
     * fall behind and not send heartbeat until system time catch up to it..
     */
    private void checkSystemClockReset()
    {
        if (lastESBHeartbeat > System.currentTimeMillis())
        {
            setLastESBHeartbeat();
            Log.log.debug("ESB HEARTBEAT RESET: Due to system clock set backward, lastESBHeartBeat is reset to " + lastESBHeartbeat);
        }
    }// checkSystemClockReset

    public static boolean isRestartDown()
    {
        return restartDown;
    } // isRestartDown

    public static void setRestartDown(boolean restartDown)
    {
        MStatus.restartDown = restartDown;
    } // setRestartDown

    private void setLastESBHeartbeat()
    {
        MStatus.lastESBHeartbeat = System.currentTimeMillis();
    }// setLastESBHeartbeat

    public static int getDbTimeout()
    {
        return dbTimeout;
    } // getDBTimeout

    public static void setDbTimeout(int dbTimeout)
    {
        MStatus.dbTimeout = dbTimeout;
    } // setDBTimeout

    public static int getEsbTimeout()
    {
        return esbTimeout;
    }// getEsbTimeout

    public static void setEsbTimeout(int esbTimeout)
    {
        MStatus.esbTimeout = esbTimeout * 1000;
    }// setEsbTimeout

    public static List<String> getSuppressStdoutErrors()
    {
        return suppressErrors;
    }// getSuppressStdoutErrors

    public static void setSuppressStdoutErrors(List<String> suppressStdoutErrors)
    {
        MStatus.suppressErrors = suppressStdoutErrors;
    }// setSuppressStdoutErrors

    public static List<String> getStdoutErrors()
    {
        return stdoutErrors;
    }// getStdoutErrors

    public static void setStdoutErrors(List<String> stdoutErrors)
    {
        MStatus.stdoutErrors = stdoutErrors;
    }// setStdoutErrors

    public static void setESErrorsFromFile(String ESErrorsFile)
    {
        File ESFile = FileUtils.getFile(main.getProductHome() + "/config/" + ESErrorsFile);
        List<String> ESErrors = null;
        if (ESFile.exists())
        {
            FileInputStream ESfis = null;
            try
            {
                byte[] ESFileBytes = new byte[(int) ESFile.length()];
                ESfis = new FileInputStream(ESFile);
                ESfis.read(ESFileBytes);
                String ESErrorStr = new String(ESFileBytes);
                Log.log.debug("Set ES Errors to log: " + ESErrorStr);
                String[] ESErrorAry = ESErrorStr.split("\n");
                ESErrors = new ArrayList<String>();
                for (int i = 0; i < ESErrorAry.length; i++)
                {
                    String ESError = ESErrorAry[i];
                    if (StringUtils.isNotBlank(ESError))
                    {
                        ESErrors.add(ESError.trim());
                    }
                }
            }
            catch (Exception e)
            {
                Log.log.error("Failed to Read ES Errors File", e);
            }
            finally
            {
                if (ESfis != null)
                {
                    try
                    {
                        ESfis.close();
                    }
                    catch (Exception e)
                    {
                        Log.log.error("Failed to close ES Errors File", e);
                    }
                }
            }

        }
        else
        {
            Log.log.warn("Unable to find ES Error File: " + ESFile.getAbsolutePath());
        }
        MStatus.ESErrors = ESErrors;

    }

    public static void setSuppressErrorsFromFile(String suppressErrorsFile)
    {
        File suppressFile = FileUtils.getFile(main.getProductHome() + "/config/" + suppressErrorsFile);
        List<String> suppressErrors = null;
        if (suppressFile.exists())
        {
            FileInputStream suppressfis = null;
            try
            {
                byte[] suppressFileBytes = new byte[(int) suppressFile.length()];
                suppressfis = new FileInputStream(suppressFile);
                suppressfis.read(suppressFileBytes);
                String suppressErrorStr = new String(suppressFileBytes);
                Log.log.debug("Set Errors to suppress: " + suppressErrorStr);
                String[] suppressErrorAry = suppressErrorStr.split("\n");
                suppressErrors = new ArrayList<String>();
                for (int i = 0; i < suppressErrorAry.length; i++)
                {
                    String suppressError = suppressErrorAry[i];
                    if (StringUtils.isNotBlank(suppressError))
                    {
                        suppressErrors.add(suppressError.trim());
                    }
                }
            }
            catch (Exception e)
            {
                Log.log.error("Failed to Read Suppress Error File", e);
            }
            finally
            {
                if (suppressfis != null)
                {
                    try
                    {
                        suppressfis.close();
                    }
                    catch (Exception e)
                    {
                        Log.log.error("Failed to close Suppress Errors File", e);
                    }
                }
            }

        }
        else
        {
            Log.log.warn("Unable to find Suppress Error File: " + suppressFile.getAbsolutePath());
        }
        MStatus.suppressErrors = suppressErrors;

    }

    public static void setStdoutErrorsFromFile(String stdoutErrorsFile)
    {
        File stdoutFile = FileUtils.getFile(main.getProductHome() + "/config/" + stdoutErrorsFile);

        List<String> stdoutErrors = null;
        if (stdoutFile.exists())
        {
            FileInputStream fis = null;
            try
            {
                byte[] fileBytes = new byte[(int) stdoutFile.length()];
                fis = new FileInputStream(stdoutFile);
                fis.read(fileBytes);
                String stdoutErrorStr = new String(fileBytes);
                Log.log.debug("Set Stdout Errors: " + stdoutErrorStr);
                String[] stdoutErrorAry = stdoutErrorStr.split("\n");
                stdoutErrors = new ArrayList<String>();
                for (int i = 0; i < stdoutErrorAry.length; i++)
                {
                    String stdoutError = stdoutErrorAry[i];
                    if (StringUtils.isNotBlank(stdoutError))
                    {
                        stdoutErrors.add(stdoutError.trim());
                    }
                }
            }
            catch (Exception e)
            {
                Log.log.error("Failed to Read Stdout Errors File", e);
            }
            finally
            {
                if (fis != null)
                {
                    try
                    {
                        fis.close();
                    }
                    catch (Exception e)
                    {
                        Log.log.error("Failed to close Stdout Errors File", e);
                    }
                }
            }
        }
        else
        {
            Log.log.warn("Unable to find Stdout Error File: " + stdoutFile.getAbsolutePath());
        }

        MStatus.stdoutErrors = stdoutErrors;
    }// setStdoutErrorsFromFile

    public static int getStdoutDelay()
    {
        return stdoutDelay;
    }// getStdoutDelay

    public static void setStdoutDelay(int stdoutDelay)
    {
        MStatus.stdoutDelay = stdoutDelay;
    }// setStdoutDelay

    private class TailerAdapter extends TailerListenerAdapter
    {
        private String component = "UNKNOWN";
        private Tailer tailer;
        private boolean run = false;

        public TailerAdapter()
        {
        }

        public TailerAdapter(String component)
        {
            Log.log.debug("Monitor component stdout: " + component);
            this.component = component;
        }// TailerAdapter

        public void init(Tailer tailer)
        {
            this.tailer = tailer;
            run = true;
        }// init

        public void fileNotFound()
        {
            Log.alert(ERR.E10013);
            tailer.stop();
            run = false;
            Thread.currentThread().interrupt();
        }// fileNotFound

        public void handle(Exception e)
        {
            if (e instanceof InterruptedException)
            {
                Log.log.warn("Stopping " + component + " tail");
            }
            else
            {
                Log.alert(ERR.E10014, e);
                Thread.currentThread().interrupt();
            }
            tailer.stop();
            run = false;
        }// handle

        public void handle(String line)
        {
            Matcher m = heartbeatPattern.matcher(line);
            boolean suppressed = false;
            if (m.find())
            {
                String dateStr = m.group(1);
                // somehow we keep getting format errors for "" string
                if (StringUtils.isNotBlank(dateStr))
                {
                    try
                    {
                        Date date = sdfThreadLocal.get().parse(dateStr);
                        lastHeartbeats.put(component, date);
                    }
                    catch (ParseException pe)
                    {
                        Log.log.error("Failed to parse Heartbeat Date", pe);
                    }
                    catch (NumberFormatException nfe)
                    {
                        Log.log.error("Heartbeat Pattern Pulled Bad Data - " + dateStr, nfe);
                    }
                }
            }
            else
            {
                for (String suppressTxt : suppressErrors)
                {
                    Pattern p = Pattern.compile(suppressTxt);
                    Matcher mat = p.matcher(line);
                    if (mat.find())
                    {
                        suppressed = true;
                    }

                }
                if (!suppressed)
                {
                    for (String errorTxt : stdoutErrors)
                    {
                        Pattern p = Pattern.compile(errorTxt);
                        Matcher mat = p.matcher(line);
                        if (mat.find() && !line.contains(ERR.E10014.getMessage()))
                        {
                            Log.alert(ERR.E10014, "STDOUT Error Message found for component " + component + ": " + line + " (last heartbeat: " + lastHeartbeats.get(component) + ")");
                        }
                    }
                }
            }
        }// handle

        private void stopTail()
        {
            Log.log.info("Stopping " + component + " tail");
            run = false;
            tailer.stop();
            Thread.currentThread().interrupt();
        }// stopTail

        public boolean isRunning()
        {
            return run;
        }// isRunning

        public String getComponent()
        {
            return component;
        }// getComponent

        public void setComponent(String component)
        {
            this.component = component;
        }// setComponent

    }// TailerAdapter class

    private class ESTailerAdapter extends TailerListenerAdapter
    {
        private String component = "RSSEARCH";
        private Tailer tailer;
        private boolean run = false;

        public ESTailerAdapter()
        {
            Log.log.debug("Monitor component stdout: " + component);
        }

        public void init(Tailer tailer)
        {
            this.tailer = tailer;
            run = true;
        }// init

        public void fileNotFound()
        {
            Log.alert(ERR.E10013);
            tailer.stop();
            run = false;
            Thread.currentThread().interrupt();
        }// fileNotFound

        public void handle(Exception e)
        {
            if (e instanceof InterruptedException)
            {
                Log.log.warn("Stopping " + component + " tail");
            }
            else
            {
                Log.alert(ERR.E10014, e);
                Thread.currentThread().interrupt();
            }
            tailer.stop();
            run = false;
        }// handle

        public void handle(String line)
        {
            Matcher m = heartbeatPattern.matcher(line);
            boolean suppressed = false;
            if (m.find())
            {
                String dateStr = m.group(1);
                // somehow we keep getting format errors for "" string
                if (StringUtils.isNotBlank(dateStr))
                {
                    try
                    {
                        Date date = sdfThreadLocal.get().parse(dateStr);
                        lastHeartbeats.put(component, date);
                    }
                    catch (ParseException pe)
                    {
                        Log.log.error("Failed to parse Heartbeat Date", pe);
                    }
                    catch (NumberFormatException nfe)
                    {
                        Log.log.error("Heartbeat Pattern Pulled Bad Data - " + dateStr, nfe);
                    }
                }
            }
            else
            {
                for (String suppressTxt : suppressErrors)
                {
                    Pattern p = Pattern.compile(suppressTxt);
                    Matcher mat = p.matcher(line);
                    if (mat.find())
                    {
                        suppressed = true;
                    }

                }
                if (!suppressed)
                {
                    for (String EStxt : ESErrors)
                    {
                        Pattern p = Pattern.compile(EStxt);
                        Matcher mat = p.matcher(line);
                        if (mat.find())
                        {
                            Log.alert(ERR.E10014, "ESLog Error Message found for component " + component + ": " + line + " (last heartbeat: " + lastHeartbeats.get(component) + ")");
                        }

                    }
                }
            }
        }// handle

        private void stopTail()
        {
            Log.log.info("Stopping " + component + " tail");
            run = false;
            tailer.stop();
            Thread.currentThread().interrupt();
        }// stopTail

        public boolean isRunning()
        {
            return run;
        }// isRunning

        public String getComponent()
        {
            return component;
        }// getComponent

        public void setComponent(String component)
        {
            this.component = component;
        }// setComponent

    }// ESTailerAdapter class

    public void rsremoteStatus()
    {
        try
        {
            Log.log.debug("Checking RSRemote GUID queue status");

            if (main.isBlueprintRSRemote())
            {
                String os = System.getProperty("os.name");
                String cmd = null;
                ExecuteOS exe = new ExecuteOS(main.getProductHome());

                if (os.contains("Win"))
                {
                    cmd = "sc query state= all";
                }
                else if (os.contains("SunOS"))
                {
                    cmd = "/usr/ucb/ps -augxwww";
                }
                else
                {
                    cmd = "ps -ef";
                }

                Log.log.trace("Getting Processes Command: " + cmd);
                String processes = exe.execute(cmd).toString();

                List<String> rsremotes = main.getRSRemoteInstances();
                for (String rsremote : rsremotes)
                {
                    String status = getStatus(rsremote, processes);
                    // this condition makes sure that the rsremote is running.
                    // if it's not running
                    // there is no point of sending message.
                    if (status.contains(rsremote) && status.contains("UP") && !status.contains("HANGING"))
                    {
                        // read the config file and find its GUID
                        File configFile = FileUtils.getFile(main.getResolveHome() + "/" + rsremote + "/config/config.xml");
                        XDoc configDoc = new XDoc(configFile);
                        ConfigId configId = new ConfigId(configDoc);
                        configId.load();
                        String guid = configId.getGuid();
                        String name = configId.getName();
                        Log.log.debug("Checking RSREMOTE Id:" + guid);
                        Map<String, String> response = MainBase.getESB().call("RSREMOTE", "MInfo.info", null, main.configMonitor.getRsremoteTimeout() * 1000L);
                        if (response == null)
                        {
                            Log.alert(ERR.E10018, name + "." + rsremote.toUpperCase() + "/" + guid + " is not responding, please check.");
                        }
                        else
                        {
                            if (Log.log.isTraceEnabled())
                            {
                                Log.log.trace(rsremote.toUpperCase() + "/" + guid + " response:");
                                for (String key : response.keySet())
                                {
                                    Log.log.trace("key:" + key + ", value:" + response.get(key));
                                }
                            }
                        }
                    }
                }
            }
        }
        catch (Exception e)
        {
            Log.log.error(e.getMessage(), e);
        }
    }// rsremoteStatus

    public void diskSpaceStatus()
    {
        try
        {
            Log.log.debug("Checking Local Server Disk Space Available");

            String os = System.getProperty("os.name");
            ExecuteOS exe = new ExecuteOS(main.getProductHome());

            if (os.contains("Win"))
            {
                Log.log.debug("Disk Space check is only run on Solaris and Linux Environments");
            }
            else
            {
                File home = FileUtils.getFile(main.getResolveHome());
                if (home.exists())
                {
                    String homePath = home.getCanonicalPath();

                    String cmd = "df -k";
                    Log.log.trace("Getting Processes Command: " + cmd);
                    String diskSpace = exe.execute(cmd).toString();

                    Log.log.trace("df -k results:\n" + diskSpace);

                    Pattern diskSpacePattern = Pattern.compile("(?m)^(\\S+)\\s+(\\S+)\\s+(\\w+)\\s+(\\w+)\\s+(\\S+)\\s+(.*)$");
                    Matcher diskSpaceMatcher = diskSpacePattern.matcher(diskSpace);

                    String usage = null;
                    String mount = null;
                    while (diskSpaceMatcher.find())
                    {
                        String mountedOn = diskSpaceMatcher.group(6);

                        if (mountedOn.equalsIgnoreCase(homePath))
                        {
                            // Resolve install path matches mount
                            mount = mountedOn;
                            usage = diskSpaceMatcher.group(5);
                        }
                        else if (mountedOn.contains(homePath))
                        {
                            // Mount inside resolve install, e.g.
                            // /opt/resolve/cassandra/data
                            String subUsage = diskSpaceMatcher.group(5);
                            if (subUsage.endsWith("%"))
                            {
                                subUsage = subUsage.substring(0, subUsage.length() - 1);
                            }
                            Log.log.trace("Mount inside Resolve install usage: " + subUsage);
                            mthreshold.checkThresholds("server", Integer.parseInt(subUsage), "disk_space");
                        }
                        else if (homePath.contains(mountedOn))
                        {
                            // Resolve install path a sub-directory of mount
                            // If no proper mount has been found yet, or if this
                            // mount is a more exact path than the current one
                            if (mount == null || mount.length() < mountedOn.length())
                            {
                                mount = mountedOn;
                                usage = diskSpaceMatcher.group(5);
                            }
                        }
                    }

                    if (usage.endsWith("%"))
                    {
                        usage = usage.substring(0, usage.length() - 1);
                    }
                    Log.log.trace("Resolve disk space usage: " + usage);
                    mthreshold.checkThresholds("server", Integer.parseInt(usage), "disk_space");
                }
                else
                {
                    Log.log.error("Configured Resolve Home Path Not Found: " + main.getResolveHome());
                }
            }
        }
        catch (Exception e)
        {
            Log.log.error(e.getMessage(), e);
        }
    }// diskSpaceStatus

    @SuppressWarnings({ "unchecked", "rawtypes" })
    public void esSelfCheck()
    {
        Log.log.trace("ES Self-Check Started.");
        URL url = null;
        BufferedReader reader = null;
        try
        {
            url = new URL("http://" + main.configId.getLocation() + ":9200/_nodes/stats/os,jvm?human&pretty");

            HttpURLConnection connection = (HttpURLConnection) url.openConnection();
            connection.setReadTimeout(5000);
            connection.setConnectTimeout(5000);
            connection.setRequestMethod("GET");
            connection.setDoOutput(true);
            connection.connect();

            int rc = connection.getResponseCode();
            if (rc != 200)
            {
                Log.log.error("Instread of success, HTTP response code of " + rc + " is returned while communicating with MCP gateway connector.");
            }

            reader = new BufferedReader(new InputStreamReader(connection.getInputStream()));
            StringBuilder stringBuilder = new StringBuilder();

            String line = null;
            while ((line = reader.readLine()) != null)
            {
                stringBuilder.append(line + "\n");
            }

            JSONObject object = JSONObject.fromObject(stringBuilder.toString());
            findOsAndJvmParams(object);

        }
        catch (MalformedURLException e)
        {
            Log.log.error(e.getMessage(), e);
        }
        catch (IOException e)
        {
            String message = e.getMessage();
            Log.log.error(message, e);
            if (message != null && message.equals("connect timed out"))
            {
                // As part of Phase I, do the self check of locally running ES
                // node.
                Log.log.error("Looks like there no ES node running locally. No self check will be made.");
                return;
            }
        }
        finally
        {
            if (reader != null)
            {
                try
                {
                    reader.close();
                }
                catch (IOException ioe)
                {
                    ioe.printStackTrace();
                }
            }
        }

        // NodesStatsResponse nodeStattsRes =
        // SearchAdminAPI.getClient().admin().cluster().nodesStats(Requests.nodesStatsRequest(new
        // String[] {"eWONxdVyTy24Fk_gdz-LMg"})).actionGet();
        // NodeStats[] nodeStatsArray = nodeStattsRes.getNodes();
        // if (nodeStatsArray != null && nodeStatsArray.length > 0)
        // {
        // for (NodeStats nodeStats : nodeStatsArray)
        // {
        // String gcFrequency = "";
        // GarbageCollector[] gcArray = nodeStats.getJvm().getGc().collectors();
        // if (gcArray != null && gcArray.length > 0)
        // {
        // GarbageCollector collector = gcArray[0];
        // long collectionCount = collector.getCollectionCount();
        // long collectionTime = collector.getCollectionTime().getMinutes();
        // gcFrequency = (collectionCount / collectionTime) + "";
        // }
        // }
        // }

        ClusterStatsResponse clusterStatts = SearchAdminAPI.getClient().admin().cluster().clusterStats(Requests.clusterStatsRequest()).actionGet();
        if (clusterStatts != null)
        {
            // ClusterStatsNodeResponse clusterStatsNodesResponse =
            // clusterStatts.getAt(0);
            // NodeStats nodeStats = clusterStatsNodesResponse.nodeStats();
            // GarbageCollector[] gcArray =
            // nodeStats.getJvm().getGc().collectors();
            // String gcFrequency = "";
            // if (gcArray != null && gcArray.length > 0)
            // {
            // GarbageCollector collector = gcArray[0];
            // long collectionCount = collector.getCollectionCount();
            // long collectionTime = collector.getCollectionTime().getMinutes();
            // gcFrequency = (collectionCount / collectionTime) + "";
            // }
            //
            // short heapUsedPercent =
            // nodeStats.getJvm().getMem().getHeapUsedPrecent();

            ClusterStatsNodes stattsNodes = clusterStatts.getNodesStats();

            JvmStats jvmStats = stattsNodes.getJvm();
            long jvmHeapMax = jvmStats.getHeapMax().getMb();
            long jvmHeapUsed = jvmStats.getHeapUsed().getMb();
            long heapUsedPercent = (jvmHeapUsed) * 100 / jvmHeapMax;
            Log.log.trace("ES Self-Check Heap Used: " + heapUsedPercent + "%");
            if (heapUsedPercent >= main.configMonitor.getEsJvmMemThreshold())
            {
                Map params = new HashMap();
                params.put("SEVERITY", "CRITICAL");
                params.put("COMPONENT", main.release.serviceName);
                params.put("CODE", "E70004");
                params.put("MESSAGE", ERR.E70004.getMessage());
                (new MAlert()).alert(params);
            }

            long freeFsMem = stattsNodes.getFs().getFree().getMb();
            long availableFsMem = stattsNodes.getFs().getTotal().getMb();
            long freeDiskSpace = freeFsMem * 100 / availableFsMem;
            Log.log.trace("ES Self-Check Free Diskspace: " + freeDiskSpace + "%");
            if (freeDiskSpace <= main.configMonitor.getEsFsMemThreshold())
            {
                Map params = new HashMap();
                params.put("SEVERITY", "CRITICAL");
                params.put("COMPONENT", main.release.serviceName);
                params.put("CODE", "E70006");
                params.put("MESSAGE", ERR.E70006.getMessage());
                (new MAlert()).alert(params);
            }

            // System.out.println("Heap Available: " + heapAvailbalePercent + ",
            // Free Disk Space: " + freeDiskSpace + ", Used Os Memory: " +
            // usedOsMem);
            Log.log.trace("ES Self-Check Finished.");
        }
    }

    @SuppressWarnings({ "rawtypes", "unchecked" })
    private void findOsAndJvmParams(JSONObject jsonObject)
    {
        Set keySet = jsonObject.keySet();
        if (keySet != null)
        {
            for (Object o : keySet)
            {
                String key = (String) o;
                if (key.equalsIgnoreCase("gc"))
                {
                    JSONObject jsonObj = (JSONObject) jsonObject.get(key);
                    JSONObject collectors = (JSONObject) jsonObj.get("collectors");
                    JSONObject oldMem = (JSONObject) collectors.get("old");

                    double collCount = oldMem.getInt("collection_count");
                    double collTime = oldMem.getInt("collection_time_in_millis");
                    double gcFrequency = collCount / collTime;
                    Log.log.trace("ES Self-Check GC Frequency: " + gcFrequency);
                    if (gcFrequency >= main.configMonitor.getEsGCFreqThreshold())
                    {
                        Map params = new HashMap();
                        params.put("SEVERITY", "CRITICAL");
                        params.put("COMPONENT", main.release.serviceName);
                        params.put("CODE", "E70007");
                        params.put("MESSAGE", ERR.E70007.getMessage());
                        (new MAlert()).alert(params);
                    }

                    // System.out.println("GC Frequency: " + gcFrequency );
                }
                else if (key.equalsIgnoreCase("os"))
                {
                    JSONObject jsonObj = (JSONObject) jsonObject.get(key);
                    JSONObject mem = (JSONObject) jsonObj.get("mem");
                    long usedOsMem = mem.getLong("used_percent");
                    Log.log.trace("ES Self-Check OS Used Mem: " + usedOsMem);
                    if (usedOsMem >= main.configMonitor.getEsOsMemThreshold())
                    {
                        Map params = new HashMap();
                        params.put("SEVERITY", "CRITICAL");
                        params.put("COMPONENT", main.release.serviceName);
                        params.put("CODE", "E70005");
                        params.put("MESSAGE", ERR.E70005.getMessage());
                        (new MAlert()).alert(params);
                    }
                }
                else
                {
                    Object obj = jsonObject.get(key);
                    if (obj instanceof JSONObject)
                    {
                        findOsAndJvmParams((JSONObject) obj);
                    }
                }
            }
        }
    }

    private static int port;
    private static String host;
    private static String protocol;

    public void rsviewHttpSelfCheck()
    {
        URL url = null;
        HttpURLConnection connection = null;
        try
        {
            if (MStatus.port != 0 && StringUtils.isNotBlank(MStatus.host))
            {
                if (StringUtils.isBlank(protocol))
                {
                    protocol = "http";
                }
                url = new URL(protocol + "://" + host.trim() + ":" + port + "/resolve/service/selfcheck/ping");
                connection = (HttpURLConnection) url.openConnection();
                connection.setReadTimeout(15000);
                connection.setConnectTimeout(15000);
                connection.setRequestMethod("POST");
                connection.setDoOutput(true);

                int rc = connection.getResponseCode();
                if (rc != 200)
                {
                    Log.log.error("Instead of success, response code of " + rc + " is returned for RsView " + protocol + " self check.");
                    Log.alert(ERR.E70008);
                }
            }
            else
            {
                Log.log.error("For RsView Http self check Port and Host is required.");
            }
        }
        catch (Exception e)
        {
            Log.alert(ERR.E70008, e);
        }
        finally
        {
            if (connection != null)
            {
                connection.disconnect();
            }
        }
    }
    
    private static String rslogHost;
    private static String rslogPort;
    
    public void rslogHttpSelfCheck() {
        URL url = null;
        HttpURLConnection connection = null;
        try {
            if (StringUtils.isNotBlank(rslogHost) && StringUtils.isNotBlank(rslogPort)) {
                StringBuilder urlBuilder = new StringBuilder("http://");
                urlBuilder.append( rslogHost.trim()).append(":").append(rslogPort.trim()).append("/rslog/ping");
                
                url = new URL(urlBuilder.toString());
                connection = (HttpURLConnection) url.openConnection();
                connection.setReadTimeout(15000);
                connection.setConnectTimeout(15000);
                connection.setRequestMethod("GET");
                connection.setDoOutput(true);

                int rc = connection.getResponseCode();
                if (rc != 200) {
                    Log.log.error("Error : A response code of " + rc + " is returned for RSLog http self check.");
                    Log.alert(ERR.E70009);
                }
            } else {
                Log.log.error("For RSLog Http self check Port and Host are required.");
            }
        } catch (Exception e) {
            Log.alert(ERR.E70009, e);
        } finally {
            if (connection != null) {
                connection.disconnect();
            }
        }
    }

    public static void setPort(int port)
    {
        MStatus.port = port;
    }

    public static void setHost(String host)
    {
        MStatus.host = host;
    }

    public static void setProtocol(String protocol)
    {
        MStatus.protocol = protocol;
    }

    public static void getDeployedComponentsGUID(String compName, List<String> guids)
    {
        SQLConnection conn = null;
        try
        {
            if (main.isDBConnected())
            {
                conn = SQL.getConnection();

                String sql = "select u_guid from resolve_registration where u_type=? and u_status = 'ACTIVE'";

                PreparedStatement ps = conn.prepareStatement(sql);

                ps.setString(1, compName.toUpperCase());

                ResultSet rs = ps.executeQuery();

                if (rs != null)
                {
                    while (rs.next())
                    {
                        String guid = rs.getString("u_guid");
                        if (StringUtils.isNotBlank(guid))
                        {
                            guids.add(guid);
                        }
                    }
                }
            }
            else
            {
                Log.log.warn("No DB Pool Available, Unable to get deployed component GUID.");
            }
        }
        catch (Throwable t)
        {
            Log.alert(ERR.E20005, t);
        }
        finally
        {
            if (conn != null)
            {
                SQL.close(conn);
            }
        }
    }

    public void dbTps()
    {
        int dbTpsTHreshold = main.configMonitor.getDbTpsThreshold();
        String dbType = main.configSQL.getDbtype();

        DBStatus dbStatus = DBStatus.getInstance();
        dbStatus.setDbType(dbType);
        dbStatus.setThresholdTps(dbTpsTHreshold);

        dbStatus.checkDBTPS();
    }

    public static void setRsloghost(String rslogHost) {
        MStatus.rslogHost = rslogHost;
    }

    public static void setRslogPort(String rslogPort) {
        MStatus.rslogPort = rslogPort;
    }

}// MStatus
