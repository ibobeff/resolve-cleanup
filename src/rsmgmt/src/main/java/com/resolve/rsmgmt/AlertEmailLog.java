/******************************************************************************
* (C) Copyright 2014
*
* Resolve Systems, LLC
*
* All rights reserved
* This software is distributed under license and may not be used, copied,
* modified, or distributed without the express written permission of
* Resolve Systems, LLC.
*
******************************************************************************/

package com.resolve.rsmgmt;

import java.io.IOException;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.regex.Matcher;

import com.resolve.notification.NotificationAPI;
import com.resolve.notification.NotificationTarget;
import com.resolve.sql.SQL;
import com.resolve.sql.SQLConnection;
import com.resolve.sql.SQLRecord;
import com.resolve.util.Constants;
import com.resolve.util.Log;
import com.resolve.util.Properties;
import com.resolve.util.StringUtils;

public class AlertEmailLog
{
    final static String RESOLVE_BLUEPRINT = "resolve_blueprint";

    private static final String TRUE = "true";
    public final static String VAR_REGEX_SEVERITY = "\\$SEVERITY";
    public final static String VAR_REGEX_COMPONENT = "\\$COMPONENT";
    public final static String VAR_REGEX_CODE = "\\$CODE";
    public final static String VAR_REGEX_MESSAGE = "\\$MESSAGE";

    static 
    {
        List<NotificationTarget> targets = new ArrayList<NotificationTarget>();
        
        //components like RSMGMT doesn't have access to hibernate transaction, so
        //they'll provide the targets in some other means (i.e., direct SQL etc.
//        if(targets.size() == 0)
        {
            SQLConnection conn = null;
            ResultSet resultSet = null;
            
            try
            {
                conn = SQL.getConnection();
                
                SQLRecord rec = new SQLRecord(conn.getConnection(), RESOLVE_BLUEPRINT);
                
                rec.query();
                resultSet = rec.getResultSet();

                while (resultSet.next())
                {
                    Properties prop = new Properties();
                    try
                    {
                        prop.load(StringUtils.toInputStream(resultSet.getString("u_blueprint")));
                        if(prop.get(NotificationAPI.RSREMOTE_RECEIVE_EMAIL_ACTIVE).equalsIgnoreCase(TRUE))
                        {
                            targets.add(new NotificationTarget(Constants.NOTIFICATION_CARRIER_TYPE_EMAIL, prop.get(NotificationAPI.RSREMOTE_RECEIVE_EMAIL_QUEUE)));                    
                        }
                        if(prop.get(NotificationAPI.RSREMOTE_RECEIVE_EWS_ACTIVE).equalsIgnoreCase(TRUE))
                        {
                            targets.add(new NotificationTarget(Constants.NOTIFICATION_CARRIER_TYPE_EWS, prop.get(NotificationAPI.RSREMOTE_RECEIVE_EWS_QUEUE)));                    
                        }
                    }
                    catch (IOException e)
                    {
                        Log.log.warn("Could not load blueprint properties: " + e.getMessage());
                    }
                }
            }
            catch (Throwable t)
            {
                Log.log.error(t.getMessage(), t);
            }
            finally
            {
                if(resultSet != null)
                {
                    try
                    {
                        resultSet.close();
                    }
                    catch (SQLException e)
                    {
                        Log.log.error(e.getMessage(), e);
                    }
                }
                if (conn != null)
                {
                    SQL.close(conn);
                }
            }
            //initialize the targets in notification API.
            NotificationAPI.init(targets);
        }
    }
    
    public static void alert(ConfigAlert emailConfigAlert, String severity, String component, String type, String message)
    {
        try
        {
            String subject= transform(emailConfigAlert.getSubject(), severity, component, type, message);
            String content = transform(emailConfigAlert.getBody(), severity, component, type, message);

            NotificationAPI.send(emailConfigAlert.toEmail, emailConfigAlert.getCcEmail(), emailConfigAlert.getBccEmail(), null, subject, content, null);
        }
        catch (Throwable e)
        {
            Log.log.error(e.getMessage(), e);
        }
    } // alert

    private static String transform(String content, String severity, String component, String type, String message)
    {
        String result = "";

        if(StringUtils.isNotEmpty(content))
        {
            content = content.replaceAll(VAR_REGEX_SEVERITY, (severity == null ? "Undefined" : severity));
            content = content.replaceAll(VAR_REGEX_COMPONENT, (component == null ? "Undefined" : component));
            content = content.replaceAll(VAR_REGEX_CODE, (type == null ? "Undefined" : type));
            //found out that sometime the message might have dollar($) in it and throws exception because
            //dollar is a metacharacter in regex world to indicate group. this line of code fixes that issue.
            message = Matcher.quoteReplacement(message);
            content = content.replaceAll(VAR_REGEX_MESSAGE, (message == null ? "Undefined" : message));
            result = content;
        }
        
        return result;
    }
    
    /*
    public static void main(String [] args)
    {
        System.out.println(transform("Severity: $SEVERITY, Component: $COMPONENT, Type: $CODE, Message: $MESSAGE", "Sev1", "comp1", "type1", "my msg"));
        System.out.println(transform("Resolve alert", "Sev1", "comp1", "type1", "my msg"));
    }
    */
}
