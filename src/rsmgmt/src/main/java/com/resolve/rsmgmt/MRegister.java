/******************************************************************************
* (C) Copyright 2014
*
* Resolve Systems, LLC
*
* All rights reserved
* This software is distributed under license and may not be used, copied,
* modified, or distributed without the express written permission of
* Resolve Systems, LLC.
*
******************************************************************************/

package com.resolve.rsmgmt;

import java.io.File;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.Hashtable;
import java.util.Map;
import java.util.Vector;

import com.resolve.rsbase.MainBase;
import com.resolve.util.Constants;
import com.resolve.util.FileUtils;
import com.resolve.util.GMTDate;
import com.resolve.util.Log;

public class MRegister
{

    private static SimpleDateFormat sdf = new SimpleDateFormat(Constants.HEARTBEAT_FORMAT);
    private static File blueprintFile = null;
    //private static Date blueprintLastUpdated;
    public static volatile boolean isBlueprintAcknowledged = false;

    @SuppressWarnings("rawtypes")
    public String register(Map params)
    {
        register();

        return "SUCCESS: Registration completed";
    } // register

    @SuppressWarnings({ "rawtypes", "unchecked" })
    public static void register()
    {
        try
        {
            Log.log.debug("Registration");

            File file = FileUtils.getFile(Main.main.getProductHome()+"/config/config.xml");
            if (file.exists())
            {
                Vector content = new Vector();

                // set GUID and NAME
                Hashtable reg = new Hashtable();
                reg.put("GUID", Main.main.configId.guid);
                reg.put("NAME", Main.main.configId.name);
                reg.put("TYPE", Main.main.release.type);
                reg.put("IPADDRESS", Main.main.configId.ipaddress);
                reg.put("VERSION", Main.main.release.version);
                reg.put("DESCRIPTION", Main.main.configId.description);
                reg.put("LOCATION", Main.main.configId.location);
                reg.put("GROUP", Main.main.configId.group);
                reg.put("CONFIG", FileUtils.readFileToString(file, "UTF-8"));
		        reg.put("CREATETIME", GMTDate.getDate());
		        reg.put("UPDATETIME", GMTDate.getDate());
		        reg.put("OS", System.getProperty("os.name"));
                content.add(reg);

                // get parent GUID and NAME
                if (Main.esb.sendInternalMessage("RSCONTROL", "MRegister.register", content) == false)
                {
                    Log.log.warn("Failed to send registration message");
                }
            }
            else
            {
                Log.log.error("Missing config.xml file: "+file.getPath());
            }
        }
        catch (Exception e)
        {
            Log.log.error("Failed to register RSMgmt: "+e.getMessage(), e);
        }
    } // register
    
    public static String getBlueprintData() {
        
        String blueprintData = "Error: No blueprint data found.";
        
        if(blueprintFile == null)
        {
            blueprintFile = FileUtils.getFile(Main.main.getResolveHome() + "/rsmgmt/config/blueprint.properties");
        }
        
        try
        {
            blueprintData = FileUtils.readFileToString(blueprintFile, "utf8");
        }
        catch (IOException e)
        {
            Log.log.error(e.getMessage(), e);
        }
        
        return blueprintData;
        
    }

    @SuppressWarnings("rawtypes")
    public String logHeartbeat(Map params)
    {
        logHeartbeat();

        return "SUCCESS: Heartbeat completed";
    } // logHeartbeat

    public static void logHeartbeat()
    {
        Date time = new Date();
        Log.log.info("HEARTBEAT: " + sdf.format(time));
        System.out.println("HEARTBEAT:" + sdf.format(time));
    } // logHeartbeat

    /**
     * Called by RSControl after receiving the update blueprint message.
     */
    public static void acknowledge(Map<String, Object> params)
    {
        isBlueprintAcknowledged = true;
    }

    /**
     * Called by RSControl after receiving the update blueprint message.
     */
    public static void requestState(Map<String, Object> params)
    {
        String requester = (String) params.get("GUID");
        Map<String, String> params1 = new HashMap<String, String>();
        params1.put("GUID", Main.main.configId.getGuid());
        MainBase.esb.sendInternalMessage(requester, "MConfig.receiveState", params1);
        Log.log.debug("Sent alive state message to RSCONTROL:" + requester);
    }
    
} // MRegister
