/******************************************************************************
* (C) Copyright 2014
*
* Resolve Systems, LLC
*
* All rights reserved
* This software is distributed under license and may not be used, copied,
* modified, or distributed without the express written permission of
* Resolve Systems, LLC.
*
******************************************************************************/

package com.resolve.rsmgmt;

import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import com.resolve.util.Constants;
import com.resolve.util.CryptUtils;
import com.resolve.util.Log;
import com.resolve.util.Properties;
import com.resolve.util.StringUtils;

public class ConfigRSMQ
{
    private static final String instanceName = "rsmq";
    
    private Properties rsmqProperties;
    private Properties esapiProperties;
    private boolean primary = true;
    private String username = "admin";
    private String p_assword = "resolve";
    private String brokerAddr = "localhost";
    private String brokerPort = "4004";
    private String brokerAddr2 = null;
    private String brokerPort2 = "4004";
    private String managementPort = "15672";
    private String epmdPort = "4369";
    private String inetPort = "35197";
    private String Xms = "256";
    private String Xmx = "512";
    private String user = "resolve";
    private String product = Constants.ESB_RABBITMQ;
    private String primaryNodeName = null;
    private String backupNodeName = null;

    public ConfigRSMQ()
    {
        this.rsmqProperties = new Properties();
        this.esapiProperties = new Properties();
    } // ConfigRSMQ
    
    public ConfigRSMQ(Properties properties)
    {
        this();
        for (Object key : properties.keySet())
        {
            String strKey = key.toString();
            if (strKey.startsWith(instanceName + "."))
            {
                String value = properties.getProperty(strKey).trim();
                if (value.contains("${"))
                {
                    value = properties.getReplacement(key).trim();
                }
                if (value.startsWith("TO_ENC:") || value.startsWith("NO_ENC:"))
                {
                    value = value.substring(7);
                }
                if (value.startsWith("ENC"))
                {
                    try
                    {
                        value = CryptUtils.decrypt(value);
                    }
                    catch (Exception e)
                    {
                        Log.log.error("Failed to decrypt blueprint value", e);
                        value = "";
                    }
                }
                if (strKey.contains("brokeraddr") && value.matches(":\\d*"))
                {
                    value = "";
                }
                if (value != null)
                {
	                if (strKey.equalsIgnoreCase(instanceName + ".primary"))
	                {
	                    if (value.equals("false"))
	                    {
	                        this.primary = false;
	                    }
	                }
	                else if (strKey.equalsIgnoreCase(instanceName + ".primary.ipaddress"))
	                {
	                    this.brokerAddr = value;
	                }
	                else if (strKey.equalsIgnoreCase(instanceName + ".primary.port"))
	                {
	                    if (value.matches("\\d+"))
	                    {
		                    this.brokerPort = value;
	                    }
	                }
	                else if (strKey.equalsIgnoreCase(instanceName + ".primary.name"))
	                {
	                    this.primaryNodeName = value;
	                }
	                else if (strKey.equalsIgnoreCase(instanceName + ".backup.ipaddress"))
	                {
	                    this.brokerAddr2 = value;
	                }
	                else if (strKey.equalsIgnoreCase(instanceName + ".backup.port"))
	                {
	                    if (value.matches("\\d+"))
	                    {
		                    this.brokerPort2 = value;
	                    }
	                }
	                else if (strKey.equalsIgnoreCase(instanceName + ".backup.name"))
	                {
	                    this.backupNodeName = value;
	                }
	                else if (strKey.equalsIgnoreCase(instanceName + ".username"))
	                {
	                    this.username = value;
	                }
	                else if (strKey.equalsIgnoreCase(instanceName + ".password"))
	                {
	                    this.p_assword = value;
	                }
	                else if (strKey.equalsIgnoreCase(instanceName + ".management.port"))
	                {
	                    if (value.matches("\\d+"))
	                    {
		                    this.managementPort = value;
	                    }
	                }
	                else if (strKey.equalsIgnoreCase(instanceName + ".epmd.port"))
	                {
	                    if (value.matches("\\d+"))
	                    {
		                    this.epmdPort = value;
	                    }
	                }
	                else if (strKey.equalsIgnoreCase(instanceName + ".inet.port"))
	                {
	                    if (value.matches("\\d+"))
	                    {
		                    this.inetPort = value;
	                    }
	                }
	                else if (strKey.equalsIgnoreCase(instanceName + ".Xms"))
	                {
	                    if (value.endsWith("M"))
	                    {
	                        value = value.substring(0, value.length()-1);
	                    }
	                    this.Xms = value;
	                }
	                else if (strKey.equalsIgnoreCase(instanceName + ".mms"))
	                {
	                    this.Xms = value;
	                }
	                else if (strKey.equalsIgnoreCase(instanceName + ".Xmx"))
	                {
	                    if (value.endsWith("M"))
	                    {
	                        value = value.substring(0, value.length()-1);
	                    }
	                    this.Xmx = value;
	                }
	                else if (strKey.equalsIgnoreCase(instanceName + ".mmx"))
	                {
	                    this.Xmx = value;
	                }
	                else if (strKey.equalsIgnoreCase(instanceName + ".user"))
	                {
	                    this.user = value;
	                }
	                else if (strKey.equalsIgnoreCase(instanceName + ".product"))
	                {
	                    this.product = value;
	                } 
	                else if (strKey.startsWith(instanceName + ".ESAPI"))
                    {
                        this.esapiProperties.put(key, value);
                    }
	                else
	                {
		                this.rsmqProperties.put(key, value);
	                }
                }
            } 
        }
    } // ConfigRSMQ
    
    public Map<String, String> rsviewEsapiReplaceValues()
    {
        Map<String, String> result = new HashMap<String, String>();
        
        //System.out.println("ESAPI replacing values...");
        //System.out.println("ESAPI properties: " + esapiProperties.keySet());
        
        for (Object key : esapiProperties.keySet())
        {            
            String strKey = key.toString();
            String value = esapiProperties.getProperty(strKey);
            
            //System.out.println("strKeyBefore: " + strKey);
            
            
            strKey = strKey.replaceFirst("ESAPI.", "");
            if(!strKey.startsWith("Validator")) {
                value = value.replaceAll("\\\\", "/");
            }
            
            //System.out.println("strKeyAfter: " + strKey);
            
            String replaceRegex = strKey + "=.*";
            String replaceString = strKey + "=" + value;
    
            //replaceString = replaceString.replaceAll("\\", "\\\\");
            result.put(replaceRegex, replaceString);
        } 
        
        return result;
    } //rsviewEsapiReplaceValues
    
    public Map<String, Object> rsmqXPathValues()
    {
        Map<String, Object> result = null;
        if (product.equalsIgnoreCase(Constants.ESB_RABBITMQ))
        {
            result = rabbitmqXPathValues();
        }

        return result;
    }//rsmqXPathValues
    
    public Map<String, Object> rabbitmqXPathValues()
    {
        Map<String, Object> result = null;
        
        return result;
    }
    
    public Map<String, String> rsmqRunReplaceValues()
    {
        Map<String, String> result = new HashMap<String, String>();
        
        String regex = "PRIMARY=.*";
        String value = "true";
        if (!primary)
        {
            value = "false";
        }
        value = "PRIMARY=" + value;
        result.put(regex, value);
        
        regex = "Xms\\d+M";
        value = "Xms" + Xms + "M";
        result.put(regex, value);
        
        regex = "MMS=\\d+";
        value = "MMS=" + Xms;
        result.put(regex, value);
        
        regex = "Xmx\\d+M";
        value = "Xmx" + Xmx + "M";
        result.put(regex, value);
        
        regex = "MMX=\\d+";
        value = "MMX=" + Xmx;
        result.put(regex, value);
        
        regex = "R_USER=.*";
        value = "R_USER=" + user;
        result.put(regex, value);
        
        return result;
    } // rsmqRunReplaceValues
    
    public Properties getRsmqProperties()
    {
        return rsmqProperties;
    } // getRsmqProperties

    public void setRsmqProperties(Properties properties)
    {
        this.rsmqProperties = properties;
    } // setRsmqProperties
    
    public boolean isPrimary()
    {
        return primary;
    } // isPrimary

    public String getUsername()
    {
        return username;
    } //getUsername

    public String getP_assword()
    {
        return p_assword;
    } //getPassword

    public String getBrokerAddr()
    {
        return brokerAddr;
    } //getBrokerAddr

    public String getBrokerPort()
    {
        return brokerPort;
    } //getBrokerPort

    public String getBrokerAddr2()
    {
        return brokerAddr2;
    } //getBrokerAddr2

    public String getBrokerPort2()
    {
        return brokerPort2;
    } //getBrokerPort2

    public String getProduct()
    {
        return product;
    } //getProduct

    public void setProduct(String product)
    {
        this.product = product;
    } //getProduct

    public String getManagementPort()
    {
        return managementPort;
    } //getManagementPort

    public String getEpmdPort()
    {
        return epmdPort;
    } //getEpmdPort

    public String getInetPort()
    {
        return inetPort;
    } //getInetPort

    public String getPrimaryNodeName()
    {
        return primaryNodeName;
    } //getPrimaryNodeName

    public String getBackupNodeName()
    {
        return backupNodeName;
    } //getBackupNodeName
    
    public void setEsapiProperties(Properties esapiProperties)
    {
        this.esapiProperties = esapiProperties;
    }
    
    public Properties getEsapiProperties()
    {
        return this.esapiProperties;
    }

    public void setUser(String user)
    {
        this.user = user;
    }

    public String getUser()
    {
        return this.user;
    }
}//ConfigRSMQ
