/******************************************************************************
* (C) Copyright 2014
*
* Resolve Systems, LLC
*
* All rights reserved
* This software is distributed under license and may not be used, copied,
* modified, or distributed without the express written permission of
* Resolve Systems, LLC.
*
******************************************************************************/

package com.resolve.rsmgmt;

import com.resolve.util.ConfigMap;
import com.resolve.util.XDoc;

public class ConfigMonitor extends ConfigMap
{
    private static final long serialVersionUID = 3565857716229957539L;
    
    private static final String GATEWAY_ATTR_QUEUE_SELF_CHECK_THRESHOLD = "@QUEUESELFCHECKTHRESHOLD";
    private static final int DEFAULT_QUEUE_SELF_CHECK_THRESHOLD = 5;
    private static final String GATEWAY_ATTR_QUEUE_SELF_CHECK_INTERVAL = "@QUEUESELFCHECKINTERVAL";
    private static final int DEFAULT_QUEUE_SELF_CHECK_INTERVAL = 300;
    
    private static final String GATEWAY_ATTR_TOPIC_SELF_CHECK_THRESHOLD = "@TOPICSELFCHECKTHRESHOLD";
    private static final int DEFAULT_TOPIC_SELF_CHECK_THRESHOLD = 5;
    private static final String GATEWAY_ATTR_TOPIC_SELF_CHECK_INTERVAL = "@TOPICSELFCHECKINTERVAL";
    private static final int DEFAULT_TOPIC_SELF_CHECK_INTERVAL = 300;
    
    private static final String GATEWAY_ATTR_WORKER_SELF_CHECK_THRESHOLD = "@WORKERSELFCHECKTHRESHOLD";
    private static final int DEFAULT_WORKER_SELF_CHECK_THRESHOLD = 5;
    private static final String GATEWAY_ATTR_WORKER_SELF_CHECK_INTERVAL = "@WORKERSELFCHECKINTERVAL";
    private static final int DEFAULT_WORKER_SELF_CHECK_INTERVAL = 300;
    
    boolean activeLocal = true;
    int localInterval = 300;
    boolean restart = false;
    boolean activeDB = true;
    int dbInterval = 300;
    int dbTimeout = 5;
    boolean activeESB = true;
    int esbInterval = 300;
    boolean activeStdout = true;
    int stdoutInterval = 60;
    String stdoutMonitorFile = "stdoutMonitor.txt";
    String suppressMonitorFile = "suppressMonitor.txt";
    String ESMonitorFile = "ESMonitor.txt";
    int rsremoteInterval = 300;
    int rsremoteTimeout = 60;
    boolean activeDiskSpace = true;
    int diskSpaceInterval = 300;
    boolean pingRSControl;
    boolean pingRSRemote;
    boolean pingRSView;
    int pingInterval;
    int pingThreshold;
    boolean esActive;
    int esInterval;
    // Used OS (RAM) memory threshold
    int esOsMemThreshold;
    // Used FS (Hard Disk) memory threshold
    int esFsMemThreshold;
    // Used JVM heap memory threshold.
    int esJvmMemThreshold;
    // Time in ms between two consecetive GC on old objects.
    int esGCFreqThreshold;
    // http / https request self check
    boolean http;
    int httpInterval;
    // DB TPS
    boolean dbTpsActive;
    int dbTpsInterval;
    int dbTpsThreshold;
    // RSLog self check params
    boolean rslog;
    int rslogInterval;
    
    // Gateway Self-Check
    
    private int queueSCThreshold = DEFAULT_QUEUE_SELF_CHECK_THRESHOLD; // 5 sec
    private int queueSCInterval = DEFAULT_QUEUE_SELF_CHECK_INTERVAL; // 300 sec (5 min)
    
    private int topicSCThreshold = DEFAULT_TOPIC_SELF_CHECK_THRESHOLD; // 5 sec
    private int topicSCInterval = DEFAULT_TOPIC_SELF_CHECK_INTERVAL; // 300 sec (5 min)
    
    private int workerSCThreshold = DEFAULT_WORKER_SELF_CHECK_THRESHOLD; // 5 sec
    private int workerSCInterval = DEFAULT_WORKER_SELF_CHECK_INTERVAL; // 300 sec (5 min)
    
    public ConfigMonitor(XDoc config) throws Exception
    {
        super(config);
        
        define("activeLocal", BOOLEAN, "./MONITOR/LOCAL/@ACTIVE");
        define("localInterval", INTEGER, "./MONITOR/LOCAL/@INTERVAL");
        define("restart", BOOLEAN, "./MONITOR/LOCAL/@RESTART");
        define("activeDB", BOOLEAN, "./MONITOR/DB/@ACTIVE");
        define("dbInterval", INTEGER, "./MONITOR/DB/@INTERVAL");
        define("dbTimeout", INTEGER, "./MONITOR/DB/@TIMEOUT");
        define("activeESB", BOOLEAN, "./MONITOR/ESB/@ACTIVE");
        define("esbInterval", INTEGER, "./MONITOR/ESB/@INTERVAL");
        define("activeStdout", BOOLEAN, "./MONITOR/STDOUT/@ACTIVE");
        define("stdoutInterval", INTEGER, "./MONITOR/STDOUT/@INTERVAL");
        define("stdoutMonitorFile", STRING, "./MONITOR/STDOUT/@FILE");
        define("rsremoteInterval", INTEGER, "./MONITOR/RSREMOTE/@INTERVAL");
        define("rsremoteTimeout", INTEGER, "./MONITOR/RSREMOTE/@TIMEOUT");
        define("activeDiskSpace", BOOLEAN, "./MONITOR/DISKSPACE/@ACTIVE");
        define("diskSpaceInterval", INTEGER, "./MONITOR/DISKSPACE/@INTERVAL");
        define("pingRSControl", BOOLEAN, "./MONITOR/PING/@RSCONTROL");
        define("pingRSRemote", BOOLEAN, "./MONITOR/PING/@RSREMOTE");
        define("pingRSView", BOOLEAN, "./MONITOR/PING/@RSVIEW");
        define("pingInterval", INTEGER, "./MONITOR/PING/@INTERVAL");
        define("pingThreshold", INTEGER, "./MONITOR/PING/@THRESHOLD");
        define("esActive", BOOLEAN, "./MONITOR/ES/@ACTIVE");
        define("esInterval", INTEGER, "./MONITOR/ES/@INTERVAL");
        define("esOsMemThreshold", INTEGER, "./MONITOR/ES/@OSMEMTHRESHOLDPRCNT");
        define("esFsMemThreshold", INTEGER, "./MONITOR/ES/@FSMEMTHRESHOLDPRCNT");
        define("esJvmMemThreshold", INTEGER, "./MONITOR/ES/@JVMHEAPTHRESHOLDPRCNT");
        define("esGCFreqThreshold", INTEGER, "./MONITOR/ES/@GCFREQTHRESHOLD");
        define("http", BOOLEAN, "./MONITOR/HTTP/@ACTIVE");
        define("httpInterval", INTEGER, "./MONITOR/HTTP/@INTERVAL");
        define("dbTpsActive", BOOLEAN, "./MONITOR/DBTPS/@ACTIVE");
        define("dbTpsInterval", INTEGER, "./MONITOR/DBTPS/@INTERVAL");
        define("dbTpsThreshold", INTEGER, "./MONITOR/DBTPS/@THRESHOLD");
        define("rslog", BOOLEAN, "./MONITOR/RSLOG/@ACTIVE");
        define("rslogInterval", INTEGER, "./MONITOR/RSLOG/@INTERVAL");
        
        String rootGtwNode = "./MONITOR/GATEWAY/";
        
        define("queueSCThreshold", INTEGER, rootGtwNode + GATEWAY_ATTR_QUEUE_SELF_CHECK_THRESHOLD);
        define("queueSCInterval", INTEGER, rootGtwNode + GATEWAY_ATTR_QUEUE_SELF_CHECK_INTERVAL);
        
        define("topicSCThreshold", INTEGER, rootGtwNode + GATEWAY_ATTR_TOPIC_SELF_CHECK_THRESHOLD);
        define("topicSCInterval", INTEGER, rootGtwNode + GATEWAY_ATTR_TOPIC_SELF_CHECK_INTERVAL);
        
        define("workerSCThreshold", INTEGER, rootGtwNode + GATEWAY_ATTR_WORKER_SELF_CHECK_THRESHOLD);
        define("workerSCInterval", INTEGER, rootGtwNode + GATEWAY_ATTR_WORKER_SELF_CHECK_INTERVAL);
        
    } // ConfigMonitor
    
    public void load() throws Exception
    {
        loadAttributes();
    } // load

    public void save() throws Exception
    {
        saveAttributes();
    } // save

    public boolean isActiveLocal()
    {
        return activeLocal;
    } //isActiveLocal

    public void setActiveLocal(boolean activeLocal)
    {
        this.activeLocal = activeLocal;
    } //setActiveLocal

    public int getLocalInterval()
    {
        return localInterval;
    } //getLocalInterval

    public void setLocalInterval(int localInterval)
    {
        this.localInterval = localInterval;
    } //setLocalInterval

    public boolean isRestart()
    {
        return restart;
    } //isRestart

    public void setRestart(boolean restart)
    {
        this.restart = restart;
    } //setRestart

    public boolean isActiveDB()
    {
        return activeDB;
    } //isActiveDB

    public void setActiveDB(boolean activeDB)
    {
        this.activeDB = activeDB;
    } //setActiveDB

    public int getDbInterval()
    {
        return dbInterval;
    } //getDbInterval

    public void setDbInterval(int dbInterval)
    {
        this.dbInterval = dbInterval;
    } //setDbInterval

    public int getDbTimeout()
    {
        return dbTimeout;
    } //getDbTimeout

    public void setDbTimeout(int dbTimeout)
    {
        this.dbTimeout = dbTimeout;
    } //setDbTimeout

    public boolean isActiveESB()
    {
        return activeESB;
    } //isActiveESB

    public void setActiveESB(boolean activeESB)
    {
        this.activeESB = activeESB;
    } //setActiveESB

    public int getEsbInterval()
    {
        return esbInterval;
    } //getEsbInterval

    public void setEsbInterval(int esbInterval)
    {
        this.esbInterval = esbInterval;
    } //setEsbInterval

    public boolean isActiveStdout()
    {
        return activeStdout;
    } //isActiveStdout

    public void setActiveStdout(boolean activeStdout)
    {
        this.activeStdout = activeStdout;
    } //setActiveStdout

    public int getStdoutInterval()
    {
        return stdoutInterval;
    } //getStdoutInterval

    public void setStdoutInterval(int stdoutInterval)
    {
        this.stdoutInterval = stdoutInterval;
    } //setStdoutInterval

    public String getSuppressMonitorFile()
    {
        return suppressMonitorFile;
    } //getSuppressMonitorFile

    public void setSuppressMonitorFile(String suppressMonitorFile)
    {
        this.suppressMonitorFile = suppressMonitorFile;
    } //setSuppressMonitorFile

    public String getStdoutMonitorFile()
    {
        return stdoutMonitorFile;
    } //getStdoutMonitorFile

    public void setStdoutMonitorFile(String stdoutMonitorFile)
    {
        this.stdoutMonitorFile = stdoutMonitorFile;
    } //setStdoutMonitorFile
    
    public String getESMonitorFile()
    {
        return ESMonitorFile;
    } //ESMonitorFile

    public void setESMonitorFile(String eSMonitorFile)
    {
        ESMonitorFile = eSMonitorFile;
    } //ESMonitorFile

    public int getRsremoteInterval()
    {
        return rsremoteInterval;
    } //getRsremoteInterval

    public void setRsremoteInterval(int rsremoteInterval)
    {
        this.rsremoteInterval = rsremoteInterval;
    } //setRsremoteInterval
    
    public int getRsremoteTimeout()
    {
        return rsremoteTimeout;
    } //getRsremoteTimeout

    public void setRsremoteTimeout(int rsremoteTimeout)
    {
        this.rsremoteTimeout = rsremoteTimeout;
    } //setRsremoteTimeout

    public boolean isActiveDiskSpace()
    {
        return activeDiskSpace;
    } //isActiveDiskSpace

    public void setActiveDiskSpace(boolean activeDiskSpace)
    {
        this.activeDiskSpace = activeDiskSpace;
    } //setActiveDiskSpace

    public int getDiskSpaceInterval()
    {
        return diskSpaceInterval;
    } //getDiskSpaceInterval

    public void setDiskSpaceInterval(int diskSpaceInterval)
    {
        this.diskSpaceInterval = diskSpaceInterval;
    } //setDiskSpaceInterval

    public boolean isPingRSControl()
    {
        return pingRSControl;
    }

    public void setPingRSControl(boolean pingRSControl)
    {
        this.pingRSControl = pingRSControl;
    }

    public boolean isPingRSRemote()
    {
        return pingRSRemote;
    }

    public void setPingRSRemote(boolean pingRSRemote)
    {
        this.pingRSRemote = pingRSRemote;
    }

    public boolean isPingRSView()
    {
        return pingRSView;
    }

    public void setPingRSView(boolean pingRSView)
    {
        this.pingRSView = pingRSView;
    }

    public int getPingInterval()
    {
        return pingInterval;
    }

    public void setPingInterval(int pingInterval)
    {
        this.pingInterval = pingInterval;
    }

    public int getPingThreshold()
	{
		return pingThreshold;
	}

	public void setPingThreshold(int pingThreshold)
	{
		this.pingThreshold = pingThreshold;
	}

	public boolean isEsActive()
    {
        return esActive;
    }

    public void setEsActive(boolean esActive)
    {
        this.esActive = esActive;
    }

    public int getEsInterval()
    {
        return esInterval;
    }

    public void setEsInterval(int esInterval)
    {
        this.esInterval = esInterval;
    }

	public int getEsOsMemThreshold() {
		return esOsMemThreshold;
	}

	public void setEsOsMemThreshold(int esOsMemThreshold) {
		this.esOsMemThreshold = esOsMemThreshold;
	}

	public int getEsFsMemThreshold() {
		return esFsMemThreshold;
	}

	public void setEsFsMemThreshold(int esFsMemThreshold) {
		this.esFsMemThreshold = esFsMemThreshold;
	}

	public int getEsJvmMemThreshold() {
		return esJvmMemThreshold;
	}

	public void setEsJvmMemThreshold(int esJvmMemThreshold) {
		this.esJvmMemThreshold = esJvmMemThreshold;
	}

	public int getEsGCFreqThreshold() {
		return esGCFreqThreshold;
	}

	public void setEsGCFreqThreshold(int esGCFreqThreshold) {
		this.esGCFreqThreshold = esGCFreqThreshold;
	}
	
	public int getQueueSCThreshold()
    {
        return queueSCThreshold;
    }
    
    public void setQueueSCThreshold(int queueSCThreshold)
    {
        this.queueSCThreshold = queueSCThreshold;
    }
    
    public int getQueueSCInterval()
    {
        return queueSCInterval;
    }
    
    public void setQueueSCInterval(int queueSCInterval)
    {
        int queueSCIntervalTmp = queueSCInterval;
        
        if (queueSCIntervalTmp < queueSCThreshold)
        {
            queueSCIntervalTmp = queueSCThreshold * 2;
        }
        
        this.queueSCInterval = queueSCIntervalTmp;
    }
    
    public int getTopicSCThreshold()
    {
        return topicSCThreshold;
    }
    
    public void setTopicSCThreshold(int topicSCThreshold)
    {
        this.topicSCThreshold = topicSCThreshold;
    }
    
    public int getTopicSCInterval()
    {
        return topicSCInterval;
    }
    
    public void setTopicSCInterval(int topicSCInterval)
    {
        int topicSCIntervalTmp = topicSCInterval;
        
        if (topicSCIntervalTmp < topicSCThreshold)
        {
            topicSCIntervalTmp = topicSCThreshold * 2;
        }
        
        this.topicSCInterval = topicSCIntervalTmp;
    }
    
    public int getWorkerSCThreshold()
    {
        return workerSCThreshold;
    }
    
    public void setWorkerSCThreshold(int workerSCThreshold)
    {
        this.workerSCThreshold = workerSCThreshold;
    }
    
    public int getWorkerSCInterval()
    {
        return workerSCInterval;
    }
    
    public void setWorkerSCInterval(int workerSCInterval)
    {
        int workerSCIntervalTmp = workerSCInterval;
        
        if (workerSCIntervalTmp < workerSCThreshold)
        {
            workerSCIntervalTmp = workerSCThreshold * 2;
        }
        
        this.workerSCInterval = workerSCIntervalTmp;
    }

	public boolean isHttp()
	{
		return http;
	}

	public void setHttp(boolean http)
	{
		this.http = http;
	}

	public int getHttpInterval()
	{
		return httpInterval;
	}

	public void setHttpInterval(int httpInterval)
	{
		this.httpInterval = httpInterval;
	}

	public int getDbTpsInterval()
	{
		return dbTpsInterval;
	}

	public void setDbTpsInterval(int dbTpsInterval)
	{
		this.dbTpsInterval = dbTpsInterval;
	}

	public boolean isDbTpsActive()
	{
		return dbTpsActive;
	}

	public void setDbTpsActive(boolean dbTpsActive)
	{
		this.dbTpsActive = dbTpsActive;
	}

	public int getDbTpsThreshold()
	{
		return dbTpsThreshold;
	}

	public void setDbTpsThreshold(int dbTpsThreshold)
	{
		this.dbTpsThreshold = dbTpsThreshold;
	}

    public boolean isRslog() {
        return rslog;
    }

    public void setRslog(boolean rslog) {
        this.rslog = rslog;
    }

    public int getRslogInterval() {
        return rslogInterval;
    }

    public void setRslogInterval(int rslogInterval) {
        this.rslogInterval = rslogInterval;
    }
} // ConfigMonitor