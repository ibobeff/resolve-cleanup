/******************************************************************************
* (C) Copyright 2014
*
* Resolve Systems, LLC
*
* All rights reserved
* This software is distributed under license and may not be used, copied,
* modified, or distributed without the express written permission of
* Resolve Systems, LLC.
*
******************************************************************************/

package com.resolve.rsmgmt.impex;

import com.resolve.sql.SQLConnection;

public class DataImpexMySql extends BaseDataImpex
{
    public DataImpexMySql(SQLConnection sqlConnection, DataParser dataParser)
    {
        super(sqlConnection, dataParser);
    }
}
