/******************************************************************************
* (C) Copyright 2014
*
* Resolve Systems, LLC
*
* All rights reserved
* This software is distributed under license and may not be used, copied,
* modified, or distributed without the express written permission of
* Resolve Systems, LLC.
*
******************************************************************************/

package com.resolve.rsmgmt;

import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

import com.resolve.rsbase.MainBase;
import com.resolve.rsmgmt.Main;
import com.resolve.util.Log;
import com.resolve.util.StringUtils;

/**
 * Runs in RSMGMT. Called by Rest Webservice in general.
 *
 */
public class MMCP
{
    private static int statusRequestExpiration = 60 * 1000;
    private static String guid = null;

    static
    {
        statusRequestExpiration = Main.configMCP.getStatusRequestExpiration() * 1000;
        guid = MainBase.main.configId.getGuid();
    }

    public static ConcurrentHashMap<String, String> components = new ConcurrentHashMap<String, String>();

    public static String getComponentStatus(Map<String, String> params)
    {
        Map<String, String> result = new HashMap<String, String>();

        if (params == null || params.size() == 0)
        {
            Log.log.warn("params is Null or Empty, can't do anything, check the calling code.");
        }
        else
        {
            String componentId = params.get("COMPONENT_ID");
            if (StringUtils.isBlank(componentId))
            {
                Log.log.warn("params did not have a key COMPONENT_ID with value in it.");
            }
            else
            {
                //status could be asked for this RSMGMT itself
                if (guid.equals(componentId))
                {
                    result.put(componentId, "RUNNING");
                }
                else
                {
                    Map<String, String> reply = new HashMap<String, String>();
                    reply.put("RETURN_QUEUE", guid);

                    long wait = 0;
                    while (!components.containsKey(componentId) && wait < statusRequestExpiration)
                    {
                        try
                        {
                            // this will go to the COMPONENT (RSRemote, RSView
                            // etc.) asking for the status.
                            MainBase.esb.sendInternalMessage(componentId, "MMCP.getComponentStatus", reply);
                            Log.log.debug("Sent message to " + componentId + " asking its status");
                            Thread.sleep(5000);
                            wait += 5000;
                        }
                        catch (InterruptedException e)
                        {
                            // ignore
                        }
                    }
                    if (components.containsKey(componentId))
                    {
                        result.put(componentId, components.get(componentId));
                        components.remove(componentId);
                    }
                    else
                    {
                        result.put(componentId, "NOT RUNNING");
                    }
                }
            }
        }
        return StringUtils.mapToJson(result);
    }

    public static void receiveStatus(Map<String, String> params)
    {
        components.putAll(params);
    }
}
