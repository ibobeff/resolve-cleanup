/******************************************************************************
* (C) Copyright 2014
*
* Resolve Systems, LLC
*
* All rights reserved
* This software is distributed under license and may not be used, copied,
* modified, or distributed without the express written permission of
* Resolve Systems, LLC.
*
******************************************************************************/

package com.resolve.rsmgmt.impex;

import com.resolve.sql.SQLConnection;

public class DataImpexOracle extends BaseDataImpex
{
    public DataImpexOracle(SQLConnection sqlConnection, DataParser dataParser)
    {
        super(sqlConnection, dataParser);
    }
    
    @Override
    public boolean importData(String userId, String tableName, String data, char delimeter, boolean isOverride, boolean stopOnFailure) throws Exception
    {
        boolean result = false;
        try
        {
            result = super.importData(userId, tableName, data, delimeter, isOverride, stopOnFailure);
            //for Oracle database committing the transaction is necessary.
            sqlConnection.commit();
        }
        catch (Exception e)
        {
            sqlConnection.getConnection().rollback();
        }
        return result;
    }
}
