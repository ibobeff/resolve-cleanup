/******************************************************************************
* (C) Copyright 2014
*
* Resolve Systems, LLC
*
* All rights reserved
* This software is distributed under license and may not be used, copied,
* modified, or distributed without the express written permission of
* Resolve Systems, LLC.
*
******************************************************************************/

package com.resolve.rsmgmt.metric;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.resolve.persistence.util.HibernateUtil;
import com.resolve.rsmgmt.ConfigMetric;
import com.resolve.sql.SQL;
import com.resolve.sql.SQLConnection;
import com.resolve.util.GMTDate;
import com.resolve.util.Log;
import com.resolve.util.MetricCache;
import com.resolve.util.MetricMode;
import com.resolve.util.MetricMsg;
import com.resolve.util.MetricType;
import com.resolve.util.MetricUnit;
import com.resolve.util.StringUtils;
import com.resolve.util.SysId;

public class MetricStoreOracle implements MetricStore
{
    //Table Identifiers for min, hr and day metric tables 
    public static final String METRIC_MIN = "METRIC_MIN_";
    public static final String METRIC_HR = "METRIC_HR_";
    public static final String METRIC_DAY = "METRIC_DAY_";
    public static final String TMETRIC_MIN = " TMETRIC_MIN_";
    public static final String TMETRIC_HR = " TMETRIC_HR_";
    public static final String TMETRIC_DAY = " TMETRIC_DAY_";
    public static final String DECISION_TREE_TYPE = "decisiontree";
    
    public void createTables(ConfigMetric config)
    {
        SQLConnection conn = null;
        
        try
        {
            conn = SQL.getConnection();
            
            // create tables for each group
            for (Map<String,String> group : config.getGroups())
            {
                String name = group.get("NAME").toUpperCase();
                String unitsStr = group.get("UNITS").toUpperCase();
                String type = group.get("TYPE").toLowerCase();
                String[] units = unitsStr.split(",");
                analyzeMetricGroup(conn, group, name, units, type);
            }
        }
        catch (Throwable e)
        {
            Log.log.error(e.getMessage(), e);
        }
        finally
        {
            try
            {
                if (conn != null)
                {
                    SQL.close(conn);
                }
            }
            catch (Exception e)
            {
                Log.log.error(e.getMessage(), e);
            }
        }
        
    } // createTables

    /**
     *  This method analyzes the METRIC GROUP from rsmgmt config.xml file and decides whether to create new metric tables
     *  or use the existing ones.
     *  
     * @param conn : sql connection
     * @param group: group embedded on the rsmgmt\config.xml file
     * @param name: name of the group
     * @param units: metrics or fields in a group
     * @param type: type of metric. This could either be a  default metric for the regular Metric tables or 
     *                           could be a special type called TMetric tables 
     */
    private void analyzeMetricGroup(SQLConnection conn, Map<String, String> group, String name, String[] units, String type)
    {
        boolean init = false;
        String initStr = group.get("INIT"); 
        if (initStr != null && initStr.equalsIgnoreCase("true"))
        {
            init = true;
        }
        
        boolean drop = false;
        String dropStr = group.get("DROP"); 
        if (dropStr != null && dropStr.equalsIgnoreCase("true"))
        {
            drop = true;
        }
        
        if(type.equalsIgnoreCase("default"))
        {
            //create regular metric tables
            initializeTables(conn, name, units, init, drop);
        }
        else if(type.equalsIgnoreCase(DECISION_TREE_TYPE))
        {
            //create special tmetric tables
            if (init)
            {
                initializeDecisionTreeTable(conn, name, units, drop);
            }
        }
        else
        {
            // None at the moment
        }
       
        // update init and drop to false
        group.put("INIT", "false");
        group.put("DROP", "false");
        
    }  // analyzeMetricGroup

    /**
     * This method makes a call to create special metric tables called TMETRIC<>
     * metric tables
     * @param conn
     * @param name
     * @param units
     * @param drop
     */
    private void initializeDecisionTreeTable(SQLConnection conn, String name, String[] units, boolean drop)
    {
        Statement stmt = null;
        
        //create the tmetric_lookup table
        stmt = createDTLookupTable(conn);
        
        //TMETRIC HR tables
        stmt = createDTMetricTable(TMETRIC_HR, conn, name, drop, units);
        
        //TMETRIC DAY tables
        stmt = createDTMetricTable(TMETRIC_DAY, conn, name, drop, units);
        
        if (stmt != null) {
            try {
                stmt.close();
            } 
            catch (SQLException sqlEx) 
            {
                Log.log.error(sqlEx.getMessage(), sqlEx);
            } 
        }
    }  // initializeDecisionTreeTable
    
    /**
     * This method creates the tmetric_lookup tables that maps DT full paths to DT path IDs
     * @param conn
     * @return
     */
    private Statement createDTLookupTable(SQLConnection conn)
    {
        Statement stmt = null;
        String sql = "";
        // create empty TMetric lookup table
        try
        {
            sql = "";
            sql += "CREATE TABLE TMETRIC_LOOKUP" + "\n";
            sql += "(" + "\n";
            sql += "SYS_ID VARCHAR2(32) NOT NULL," + "\n";
            sql += "U_FULL_PATH CLOB," + "\n";  // full path of DT
            sql += "U_PATH_ID VARCHAR2(255) NOT NULL," + "\n";  // unique checksum for that path
            sql += "U_DT_ROOT_DOC VARCHAR2(255) NOT NULL," + "\n";  // root dt
            sql += "SYS_CREATED_BY VARCHAR2(255)," + "\n";
            sql += "SYS_CREATED_ON TIMESTAMP(6)," + "\n"; 
            sql += "SYS_MOD_COUNT NUMBER(10)," + "\n";
            sql += "SYS_UPDATED_BY VARCHAR2(255)," + "\n";
            sql += "SYS_UPDATED_ON TIMESTAMP(6)," + "\n";
            sql += "PRIMARY KEY (SYS_ID)" + "\n";
            sql += ")" + "\n";
            stmt = conn.createStatement();
            Log.log.trace(sql);
            stmt.execute(sql);
            stmt.close();
            
            sql = "";
            sql += "CREATE UNIQUE INDEX UNQIDX_PATHID ON TMETRIC_LOOKUP" +" ( U_PATH_ID )\n";
            stmt = conn.createStatement();
            Log.log.trace(sql);
            stmt.execute(sql);
            stmt.close();
            
        }
        catch (SQLException createExistingTableError)
        {
            oracleErrorMsgs("TMETRIC_", "LOOKUP", createExistingTableError);
        }
        return stmt;
    } // createDTLookupTable

    /**
     *  This method creates the special metric tables called TMETRIC (if not present) and alters the data of the existing table (if necessary)
     *   to add new columns or units necessary to log the metrics that need to be reported
     *   
     * @param tmetricID
     * @param conn
     * @param name
     * @param drop
     * @param units
     * @return
     * @throws SQLException
     */
    private Statement createDTMetricTable(String tmetricID, SQLConnection conn, String name, boolean drop, String[] units)
    {
        Statement stmt = null;
        String sql = "";
        String indexUnit = tmetricID.substring(9); // TMETRIC_ contains 8 chars, hence get the substring starting with the 8th index such as HR_ or DAY_
        if (drop)
        {
            try
            {
                sql += "DROP TABLE " + tmetricID +name+"\n";
                Log.log.trace(sql);
                stmt = conn.createStatement();
                stmt.execute(sql);
                stmt.close();
            }
            catch (Throwable e)
            {
                Log.log.warn(e.getMessage(), e);
            }
        }
        
        // create empty TMetric TableID table
        try {
            sql = "";
            sql += "CREATE TABLE " + tmetricID +name+"\n";
            sql += "("+"\n";
            sql += "IDX NUMBER(10,0) DEFAULT NULL,"+"\n";
            sql += "PATHID VARCHAR2(40),"+"\n";
            sql += "NODEID VARCHAR2(255),"+"\n";
            sql += "SEQID NUMBER(19,0),"+"\n";
            sql += "USERID VARCHAR2(32),"+"\n";
            sql += "TS NUMBER(19,0),"+"\n";
            sql += "TS_DATETIME TIMESTAMP(6)"+"\n";
            sql += ")"+"\n";
            stmt = conn.createStatement();
            Log.log.trace(sql);
            stmt.execute(sql);
            stmt.close();
            
            sql = "";
            sql += "CREATE INDEX " + indexUnit +"UNQIDX_"+name+" ON " + tmetricID +name+" ( IDX, PATHID, NODEID, SEQID, USERID )\n";
            stmt = conn.createStatement();
            Log.log.trace(sql);
            stmt.execute(sql);
            stmt.close();

            sql = "";
            sql += "CREATE INDEX " + indexUnit +"INDEXPATHID_"+name+" ON " + tmetricID +name+" ( PATHID )\n";
            stmt = conn.createStatement();
            Log.log.trace(sql);
            stmt.execute(sql);
            stmt.close();

            sql = "";
            sql += "CREATE INDEX " + indexUnit + "INDEXNODEID_"+name+" ON " + tmetricID +name+" ( NODEID )\n";
            stmt = conn.createStatement();
            Log.log.trace(sql);
            stmt.execute(sql);
            stmt.close();
            
            sql = "";
            sql += "CREATE INDEX " + indexUnit + "INDEXUSERID_"+name+" ON " + tmetricID +name+" ( USERID )\n";
            stmt = conn.createStatement();
            Log.log.trace(sql);
            stmt.execute(sql);
            stmt.close();
        } 
        catch(SQLException createExistingTableError)
        {
            oracleErrorMsgs(tmetricID, name, createExistingTableError);
        }

        // alter table for creating or adding new fields in metricTableID table
        alterDTMetricTable(name, tmetricID , units);
        
        return stmt;
    }

    /** 
     *  This method checks the INIT flag from the rsmgmt config.xml and calls a method to create the metric tables
     *  
     * @param conn
     * @param name
     * @param units
     * @param init
     * @param drop
     */
    private void initializeTables(SQLConnection conn, String name, String[] units, boolean init, boolean drop)
    {
        if (init)
        {
            // METRIC_MIN_ table
            createRegularMetricTable(METRIC_MIN, conn, name, units, drop);
            
            // METRIC_HR_ table
            createRegularMetricTable(METRIC_HR, conn, name, units, drop);
            
            // METRIC_DAY_ table
            createRegularMetricTable(METRIC_DAY, conn, name, units, drop);
        }
    } // initializeTables

    /**
     *  This method creates the new metric tables (if not present) and alters the data of the existing table (if necessary)
     *   to add new columns or units to log the metrics that need to be reported
     *    
     * @param metricTableID: Metric table identifier in the database such as METRIC_MIN_ or METRIC_HR_
     * @param conn: sql connection
     * @param name: name of the metric table that concatenates with the metricTableID
     * @param units: field names or column names  of the metric table
     * @param drop: boolean which drops the metric table if drop=true
     */
    private void createRegularMetricTable(String metricTableID, SQLConnection conn, String name, String[] units, boolean drop)
    {
        String sql = "";
        String indexUnit = metricTableID.substring(7); // METRIC_ contains 7 chars, hence get the substring starting with the 7th index such as MIN_ or HR_ or DAY_
        if (drop)
        {
            try
            {
                sql += "DROP TABLE " + metricTableID +name+"\n";
                Log.log.trace(sql);
                Statement stmt = conn.createStatement();
                stmt.execute(sql);
                stmt.close();
            }
            catch (Throwable e)
            {
                Log.log.warn(e.getMessage(), e);
            }
        }
        
        // create empty metricTableID table 
        try {
            sql = "";
            sql += "CREATE TABLE " + metricTableID +name+"\n";
            sql += "("+"\n";
            sql += "IDX NUMBER(10,0) DEFAULT NULL,"+"\n";
            sql += "ID VARCHAR2(100),"+"\n";
            sql += "SRC VARCHAR2(200),"+"\n";
            sql += "TS NUMBER(19,0),"+"\n";
            sql += "TS_DATETIME TIMESTAMP(6)"+"\n";
            sql += ")"+"\n";
            Statement stmt = conn.createStatement();
            Log.log.trace(sql);
            stmt.execute(sql);
            stmt.close();
            
            sql = "";
            sql += "CREATE INDEX " + indexUnit +"UNQIDX_"+name+" ON " + metricTableID +name+" ( IDX, ID, SRC )\n";
            stmt = conn.createStatement();
            Log.log.trace(sql);
            stmt.execute(sql);
            stmt.close();

            sql = "";
            sql += "CREATE INDEX " + indexUnit +"INDEXID_"+name+" ON " + metricTableID +name+" ( ID )\n";
            stmt = conn.createStatement();
            Log.log.trace(sql);
            stmt.execute(sql);
            stmt.close();

            sql = "";
            sql += "CREATE INDEX " + indexUnit + "INDEXSRC_"+name+" ON " + metricTableID +name+" ( SRC )\n";
            stmt = conn.createStatement();
            Log.log.trace(sql);
            stmt.execute(sql);
            stmt.close();
        } 
        catch(SQLException createExistingTableError)
        {
            oracleErrorMsgs(metricTableID, name, createExistingTableError);
        }

        // alter table for creating or adding new fields in metricTableID table
        alterTable(name, metricTableID , units);
        
    } // createRegularMetricTable

    /**
     *  This method logs the info and error messages into rsmgmt log 
     *  
     * @param tableID
     * @param name
     * @param error
     */
    private void oracleErrorMsgs(String tableID, String name, SQLException error)
    {
        if(error.getErrorCode() == 955) //ORA-00955: name is already used by an existing object http://www.techonthenet.com/oracle/errors/index.php
        {
            Log.log.info("Table " + tableID + name + " already exists");
        }
        else if(error.getErrorCode() == 1430) //ORA-01430: column being added already exists in table  - http://www.techonthenet.com/oracle/errors/index.php
        {
            Log.log.info("Duplicate column name in table " + tableID + name);
        }
        else
        {
            Log.log.error(error.getMessage(), error);
            Log.log.error("Error occurs for table " + tableID + name);
        }
    }  // oracleErrorMsgs
    
    /**
     * This method alters an exisiting database table to add new columns based on the MetricUnit 
     * @param tablename
     * @param metricunit : same as the metricTableID such as METRIC_MIN_ or METRIC_HR_
     * @param units
     */
    private void alterTable(String tablename, String metricunit, String[] units)
    {
        SQLConnection conn = null;

        try
        {
            conn = SQL.getConnection();
            
            for (int i=0; i < units.length; i++)
            {
                try 
                {
                    String fieldName = units[i].trim().toLowerCase();
                    
                    String sql = "";
                    sql += "ALTER TABLE " + metricunit + tablename + "\n";
                    sql += "ADD (" + "\n";
                    String fields = "";
                    fields += "TOT_"+fieldName+" NUMBER(10,0),"+"\n";
                    fields += "CNT_"+fieldName+" NUMBER(10,0),"+"\n";
                    fields += "DEL_"+fieldName+" NUMBER(10,0)"+"\n";
                    sql += fields;
                    sql += ")"+"\n";
                    Statement stmt = conn.createStatement();
                    Log.log.trace(sql);
                    stmt.execute(sql);
                    stmt.close();
                }
                catch(SQLException addDBFieldError)
                {
                    oracleErrorMsgs(metricunit, tablename, addDBFieldError);
                }
            }
        }
        catch (Throwable e)
        {
            Log.log.error(e.getMessage(), e);
        }
        finally
        {
            try
            {
                if (conn != null)
                {
                    SQL.close(conn);
                }
            }
            catch (Exception e)
            {
                Log.log.error(e.getMessage(), e);
            }
        }
        
    } // alterTable
    
    /**
     *  This method alters an exsisting special metric database table to add new columns based on the MetricUnit
     *  
     * @param tablename
     * @param metricunit
     * @param units
     */
    private void alterDTMetricTable(String tablename, String metricunit, String[] units)
    {
        Statement stmt = null;
        SQLConnection conn = null;

        try
        {
            conn = SQL.getConnection();
            for (int i = 0; i < units.length; i++)
            {
                String fieldName = null;
                try 
                {
                    String sql = "";
                    sql += "ALTER TABLE " + metricunit + tablename + "\n";
                    sql += "ADD (" + "\n";
                    fieldName = units[i].trim().toLowerCase();
                    String fieldType = null;
                    int fieldLength = 0;
                    if(fieldName.contains(":string"))
                    {
                        fieldType = "VARCHAR2";
                        //pull max field length value present within () in the units notation such as fieldname:fieldtype(fieldlength). 
                        //for example status:string(30)
                        fieldLength = Integer.valueOf(fieldName.substring(fieldName.indexOf("(") + 1, fieldName.indexOf(")"))); 
                        fieldName = fieldName.split(":")[0];
                        String fields = fieldName + " " + fieldType + "(" + fieldLength + ")";
                        sql += fields;
                        sql += ")" + "\n";
                    }
                    else
                    {
                        //default units are int fields
                        String fields = fieldName + " NUMBER(10,0)" ;
                        sql += fields;
                        sql += ")" + "\n";
                    }
                    
                    stmt = conn.createStatement();
                    Log.log.trace(sql);
                    stmt.execute(sql);
                    stmt.close();
                }
                catch(SQLException addDBFieldError)
                {
                    oracleErrorMsgs(metricunit, tablename, addDBFieldError);
                }
            }
        }
        catch (Throwable e)
        {
            Log.log.error(e.getMessage(), e);
        }
        finally
        {
            try
            {
                if (stmt != null)
                {
                    stmt.close();
                }
                if (conn != null)
                {
                    SQL.close(conn);
                }
            }
            catch (Exception e)
            {
                Log.log.error(e.getMessage(), e);
            }
        }
        
    }  // alterDTMetricTable

    public void updateDB(MetricType type, MetricCache cache, int idx)
    {
        SQLConnection conn = null;
        
        String tablePrefix = null;
        if (type == MetricType.MIN)
        {
            tablePrefix = "METRIC_MIN_";
        }
        else if (type == MetricType.HR)
        {
            tablePrefix = "METRIC_HR_";
        }
        else if (type == MetricType.DAY)
        {
            tablePrefix = "METRIC_DAY_";
        }
        
        try
        {
            conn = SQL.getConnection();
            
            //clean up the table for records with this idx --- TODO check may be a performance concern
            for (MetricMsg metric : cache.values())
            {
                String name = metric.getName().toLowerCase();
                //init delete stmt
                String tableName = HibernateUtil.getValidMetaData(tablePrefix + name.toLowerCase());
                String sql = "DELETE FROM " + tableName + " WHERE idx=?";
                Log.log.trace(sql);
                PreparedStatement deleteStmt = conn.prepareStatement(HibernateUtil.getValidQuery(sql));
                deleteStmt.setInt(1, idx);
                //delete the old entries
                deleteStmt.execute();
                deleteStmt.close();
            }
            
            long ts = System.currentTimeMillis();
            
            for (MetricMsg metric : cache.values())
            {
                String name = metric.getName().toUpperCase();
                
                // init prepared statements
                String sql = null;
                
                // init select stmt
                String tableName = HibernateUtil.getValidMetaData(tablePrefix+name.toUpperCase());
                sql = "SELECT IDX,ID,SRC FROM "+tableName+" WHERE IDX=? AND ID=? AND SRC=?";
                Log.log.trace(sql);
                PreparedStatement selectStmt = conn.prepareStatement(HibernateUtil.getValidQuery(sql));
                
                // init insert stmt
                String fields = "";
                String values = "";
                for(String keyName : metric.getUnits().keySet())
                {
                  fields += "," + HibernateUtil.getValidMetaData("TOT_"+keyName);
                  fields += "," + HibernateUtil.getValidMetaData("CNT_"+keyName);
                  fields += "," + HibernateUtil.getValidMetaData("DEL_"+keyName);
                  values += ", ?, ?, ?";
                }
                
                sql = "INSERT INTO "+tableName+" (idx,id,src,ts,ts_datetime" + fields +") ";
                sql += "VALUES (?,?,?,?,?" + values +")";
                Log.log.trace(sql);
                PreparedStatement insertStmt = conn.prepareStatement(HibernateUtil.getValidQuery(sql));
                
                // init update stmt
                fields = "ts=?,ts_datetime=?";
                for(String keyName : metric.getUnits().keySet())
                {
                    fields += "," + HibernateUtil.getValidMetaData("TOT_"+keyName)+"=?";
                    fields += "," + HibernateUtil.getValidMetaData("CNT_"+keyName)+"=?";
                    fields += "," + HibernateUtil.getValidMetaData("DEL_"+keyName)+"=?";
                }
                
                sql = "UPDATE "+tableName+" SET " + fields + " ";
                sql += "WHERE IDX=? AND ID=? AND SRC=?";
                Log.log.trace(sql);
                PreparedStatement updateStmt = conn.prepareStatement(HibernateUtil.getValidQuery(sql));
                
                // check if row exists
                
                //special case for Oracle DB because empty string "" in ID is treated as NULL in oracle unlike MySQL
                if(StringUtils.isEmpty(metric.getId()))
                {
                    sql = "SELECT IDX,ID,SRC FROM "+tableName+" WHERE IDX=? AND SRC=? AND ID IS NULL";
                    selectStmt = conn.prepareStatement(HibernateUtil.getValidQuery(sql));
                    selectStmt.setInt(1, idx);     
                    selectStmt.setString(2, metric.getSource());
                }
                else
                {
                    selectStmt.setInt(1, idx);
                    selectStmt.setString(2, metric.getId());
                    selectStmt.setString(3, metric.getSource());
                }
                
                
                ResultSet rs = selectStmt.executeQuery();
                if (rs.next())
                {
                    // update row
                    int pos = 1;
                    updateStmt.setLong(pos++, ts);
                    updateStmt.setTimestamp(pos++, new Timestamp(GMTDate.getDate(ts).getTime()));
                    for(String keyName : metric.getUnits().keySet())
                    {
                        MetricUnit unitValue = metric.getUnits().get(keyName);
                        updateStmt.setLong(pos++, unitValue.getTotal());
                        updateStmt.setLong(pos++, unitValue.getCount());
                        updateStmt.setLong(pos++, unitValue.getDelta());
                    }
                    
                    updateStmt.setInt(pos++, idx);
                    updateStmt.setString(pos++, metric.getId());
                    updateStmt.setString(pos++, metric.getSource());
                    
                    updateStmt.executeUpdate();
                }
                else
                {
                    // insert row
                    int pos = 1;
                    insertStmt.setInt(pos++, idx);
                    insertStmt.setString(pos++, metric.getId());
                    insertStmt.setString(pos++, metric.getSource());
                    insertStmt.setLong(pos++, ts);
                    insertStmt.setTimestamp(pos++, new Timestamp(GMTDate.getDate(ts).getTime()));
                    
                    for(String keyName : metric.getUnits().keySet())
                    {
                        MetricUnit unitValue = metric.getUnits().get(keyName);
                        insertStmt.setLong(pos++, unitValue.getTotal());
                        insertStmt.setLong(pos++, unitValue.getCount());
                        insertStmt.setLong(pos++, unitValue.getDelta());
                    }
                    
                    insertStmt.execute();
                }
                
                if (rs != null)
                {
                    rs.close();
                }
                if (selectStmt != null)
                {
                    selectStmt.close();
                }
                if (insertStmt != null)
                {
                    insertStmt.close();
                }
                if (updateStmt != null)
                {
                    updateStmt.close();
                }
            }
        }
        catch (Throwable e)
        {
            Log.log.error(e.getMessage(), e);
        }
        finally
        {
            try
            {
                if (conn != null)
                {
                    SQL.close(conn);
                }
            }
            catch (Exception e)
            {
                Log.log.error(e.getMessage(), e);
            }
        }
    } // updateDB
    
    public void loadDB(ConfigMetric config)
    {
        SQLConnection conn = null;

        try
        {
            conn = SQL.getConnection();
            
            // load tables for each group
            for (Map<String, String> group : config.getGroups())
            {
                String type = group.get("TYPE").toLowerCase();
                if(type.equalsIgnoreCase("default"))
                {
                    String tablename = group.get("NAME").toLowerCase();
                    String unitsStr = group.get("UNITS").toLowerCase();
                    String[] units = unitsStr.split(",");
                    
                    String fields = "";
                    for (int i = 0; i < units.length; i++)
                    {
                        String fieldName = units[i].trim().toLowerCase();
                        
                        fields += "," + "tot_" + fieldName;
                        fields += "," + "cnt_" + fieldName;
                        fields += "," + "del_" + fieldName;
                    }
                    
                    /*
                     *  load min data into hr cache
                     */
                    String sql = "SELECT idx,id,src,ts"+fields+" FROM metric_min_" + tablename.toLowerCase() + " WHERE TS_DATETIME > ((SYSTIMESTAMP at TIME ZONE 'GMT') - INTERVAL '2' HOUR) order by TS asc";
                    Log.log.trace(sql);
                    PreparedStatement selectStmt = conn.prepareStatement(sql);
                    
                    ResultSet rs = selectStmt.executeQuery();
                    while (rs.next())
                    {
                        HashMap<String, MetricUnit> metricUnits = new HashMap<String, MetricUnit>();
                        
                        // get values from db
                        String source = rs.getString("src");
                        String id = rs.getString("id");
                        long minTs = rs.getLong("ts");
                        for (int i = 0; i < units.length; i++)
                        {
                            String fieldName = units[i].trim().toLowerCase();
                            
                            long total = rs.getLong("tot_"+fieldName);
                            long count = rs.getLong("cnt_"+fieldName);
                            long delta = rs.getLong("del_"+fieldName);
                            
                            MetricUnit metricUnit = new MetricUnit(fieldName, total, count, delta);
                            metricUnits.put(fieldName, metricUnit);
                        }
                        
                        // FIXME - need to refactor mode into config file
                        MetricMsg metric = new MetricMsg(tablename, source, id, MetricMode.TOTAL, metricUnits);
                        
                        // add metric to cache
                        int hrIdx = TrendMetric.getHrIndex(minTs);
                        TrendMetric.updateCache(MetricType.HR, metric, hrIdx);
                    }
                    
                    if (rs != null)
                    {
                        rs.close();
                    }
                    if (selectStmt != null)
                    {
                        selectStmt.close();
                    }
                    
                    /*
                     *  load hr data into day cache
                     */
                    sql = "SELECT idx,id,src,ts"+fields+" FROM metric_hr_" + tablename.toLowerCase() + " WHERE TS_DATETIME > ((SYSTIMESTAMP at TIME ZONE 'GMT') - INTERVAL '2' DAY) order by TS asc";
                    Log.log.trace(sql);
                    selectStmt = conn.prepareStatement(sql);
                    
                    rs = selectStmt.executeQuery();
                    while (rs.next())
                    {
                        HashMap<String, MetricUnit> metricUnits = new HashMap<String, MetricUnit>();
                        
                        // get values from db
                        String source = rs.getString("src");
                        String id = rs.getString("id");
                        long hrTs = rs.getLong("ts");
                        for (int i = 0; i < units.length; i++)
                        {
                            String fieldName = units[i].trim().toLowerCase();
                            
                            long total = rs.getLong("tot_"+fieldName);
                            long count = rs.getLong("cnt_"+fieldName);
                            long delta = rs.getLong("del_"+fieldName);
                            
                            MetricUnit metricUnit = new MetricUnit(fieldName, total, count, delta);
                            metricUnits.put(fieldName, metricUnit);
                        }
                        
                        // FIXME - need to refactor mode into config file
                        MetricMsg metric = new MetricMsg(tablename, source, id, MetricMode.TOTAL, metricUnits);
                        
                        // add metric to cache
                        int dayIdx = TrendMetric.getDayIndex(hrTs);
                        TrendMetric.updateCache(MetricType.DAY, metric, dayIdx);
                    }
                    
                    if (rs != null)
                    {
                        rs.close();
                    }
                    if (selectStmt != null)
                    {
                        selectStmt.close();
                    }
                } // end of if
  
            } //end of for loop
        }
        catch (Throwable e)
        {
            Log.log.error(e.getMessage(), e);
        }
        finally
        {
            try
            {
                if (conn != null)
                {
                    SQL.close(conn);
                }
            }
            catch (Exception e)
            {
                Log.log.error(e.getMessage(), e);
            }
        }
    } // loadDB

    /**
     * This method gets the database performance metrics for Oracle DB server from the 
     * dba_segments, dba_free_space, SYS.V_$SYSMETRIC_SUMMARY tables that stores information 
     * about all the other databases MySQL server maintains)
     * 
     * It gets the following metrics
     * 1. Total size of DB in MB
     * 2. Available free space (free space within the data file space) in MB
     * 3. Volume of DB used in Percentage(%)
     * 4. Number of queries executed in the current session
     * 5. Response Time of queries executed in the current session
     * 
     * @return <metric name, value> pairs 
     */
    public HashMap<String, Long> getDBMetrics()
    {
        SQLConnection conn = null;
        HashMap<String, Long> unitNameValue = new HashMap<String, Long>();
        String db_schema = com.resolve.rsmgmt.Main.configSQL.getUsername();
        try
        {
            conn = SQL.getConnection();

            // Current size of the DB schema (sum of size of tables under this schema in MB)
            String sql = "SELECT sum(bytes)/(1024*1024) \"Size in MB\"  FROM dba_segments WHERE OWNER='" + db_schema.toUpperCase() + "'";
            Log.log.trace(sql);
            PreparedStatement stmt = conn.prepareStatement(sql);
            ResultSet rs = stmt.executeQuery();

            long db_size = 0;
            while (rs.next())
            {
                db_size = rs.getLong("Size in MB");
                unitNameValue.put("size", db_size);
                Log.log.trace("DB Schema Name: " + db_schema + " size: " + db_size);
            }
            if (rs != null)
            {
                rs.close();
            }
            if (stmt != null)
            {
                stmt.close();
            }
            
            // Get the tablespace name to get the free space available: Oracle stores a db schema object logically within a tablespace of the database
            sql = "SELECT distinct tablespace_name FROM dba_segments WHERE OWNER='" + db_schema.toUpperCase() + "'";
            Log.log.trace(sql);
            stmt = conn.prepareStatement(sql);
            rs = stmt.executeQuery();
            String tablespace_name = null;
            while (rs.next())
            {
                tablespace_name = rs.getString(1);
                Log.log.trace("DB Tablespace Name: " + tablespace_name);
            }
            if (rs != null)
            {
                rs.close();
            }
            if (stmt != null)
            {
                stmt.close();
            }
            
            //Get volume of DB used from Tablespaces
            sql = " SELECT a.tablespace_name, a.bytes/(1024 * 1024) \"Size in MB\", b.free_bytes \"Free Space in MB\" " +
                  " FROM dba_data_files a, " +
                  " (SELECT file_id, SUM(bytes)/(1024*1024) free_bytes FROM dba_free_space b GROUP BY file_id) b " +
                  " WHERE a.file_id=b.file_id and a.tablespace_name='" + tablespace_name.toUpperCase() + "'";
            Log.log.trace(sql);
            stmt = conn.prepareStatement(sql);
            rs = stmt.executeQuery();
            while (rs.next())
            {
                long free_space = rs.getLong("Free Space in MB");
                unitNameValue.put("free_space", free_space);
                long size = rs.getLong("Size in MB");
                double percentage_used;
                try
                {
                    percentage_used = (db_size / (double)(free_space + db_size));
                }
                catch (ArithmeticException exc)
                {
                    Log.log.error("The DB free psace and DB Size can never be zero resulting in an arithmetic error");
                    percentage_used = 0;
                }
                if(percentage_used >= 1)
                {
                    percentage_used = 100;
                }
                else
                {
                    percentage_used =  percentage_used * 100;
                }                
                unitNameValue.put("percentage_used", (long) percentage_used);
                Log.log.trace("tablespace free space: " + free_space + " tablespace size: " + size + " percentage_used: " + percentage_used);
                tablespace_name = rs.getString(1);
                Log.log.trace("DB Tablespace Name: " + tablespace_name);
            }
            if (rs != null)
            {
                rs.close();
            }
            if (stmt != null)
            {
                stmt.close();
            }
            
            
            // Response Time Per Txn is the average level of response time that the user community is experiencing, in seconds
            sql = " SELECT CASE METRIC_NAME " +
                            " WHEN 'Response Time Per Txn' then 'Response Time Per Txn (secs)' "+
                            " ELSE METRIC_NAME " +
                            " END METRIC_NAME, " +
                            " CASE METRIC_NAME " +
                            " WHEN 'Response Time Per Txn' then ROUND((MAXVAL / 100),2) " +
                            " ELSE MAXVAL " +
                            " END MAXIMUM," +
                            " CASE METRIC_NAME " +
                            " WHEN 'Response Time Per Txn' then ROUND((AVERAGE / 100),2) " +
                            " ELSE AVERAGE " +
                            " END AVERAGE " +
                            " FROM SYS.V_$SYSMETRIC_SUMMARY WHERE METRIC_NAME in ('Response Time Per Txn')";
            Log.log.trace(sql);
            stmt = conn.prepareStatement(sql);
            rs = stmt.executeQuery();
            while (rs.next())
            {
                double d = Double.parseDouble(rs.getString(3));
                long response_time = (long) (d * 1000); //convert to millisecs
                unitNameValue.put("response_time", response_time);
                Log.log.trace("Response time of Database: " + response_time + " msec.");
            }
            if (rs != null)
            {
                rs.close();
            }
            if (stmt != null)
            {
                stmt.close();
            }

            // Count the number of queries in profiling
            long query_count = 1L; // Oracle gives the Response Time Per Txn in secs. Since we only report the average, query_count = 1 
            unitNameValue.put("query_count", query_count);
            
            // Database wait time ratio from sys.v_$sysmetric table
            // lower the wait time  % the better it is
            sql = " SELECT METRIC_NAME, VALUE FROM SYS.V_$SYSMETRIC " +
                  " WHERE METRIC_NAME IN ('Database Wait Time Ratio') " +
                  " AND INTSIZE_CSEC = (select max(INTSIZE_CSEC) from SYS.V_$SYSMETRIC)" ;  
            Log.log.trace(sql);
            stmt = conn.prepareStatement(sql);
            rs = stmt.executeQuery();
            while (rs.next())
            {
                long percentage_wait = rs.getLong("VALUE");
                unitNameValue.put("percentage_wait", percentage_wait);
            }
            if (rs != null)
            {
                rs.close();
            }
            if(stmt != null)
            {
                stmt.close();
            }
        }
        catch (Exception e)
        {
            Log.log.error(e.getMessage(), e);
        }
        finally
        {
            try
            {
                if (conn != null)
                {
                    SQL.close(conn);
                }
            }
            catch (Exception e)
            {
                Log.log.error(e.getMessage(), e);
            }
        }

        return unitNameValue;
    } // getDBMetrics

    /* (non-Javadoc)
     * @see com.resolve.rsmgmt.metric.MetricStore#updateDB(com.resolve.util.MetricType, java.util.List, java.lang.String[], int)
     */
    @Override
    public void updateDB(MetricType type, List<DTRecord> dtrecords, String[] fieldNames, int idx, boolean deleteOldIndex)
    {
        SQLConnection conn = null;
        String tablename = "dt";

        String tablePrefix = null;
        if (type == MetricType.HR)
        {
            tablePrefix = "tmetric_hr_";
        }
        else if (type == MetricType.DAY)
        {
            tablePrefix = "tmetric_day_";
        }
        
        try
        {
            conn = SQL.getConnection();
            
            if(deleteOldIndex)
            //clean up the table for records with this idx --- TODO check may be a performance concern
            {
                //init delete stmt        
                String sql = "DELETE FROM " + tablePrefix + tablename.toLowerCase() + " WHERE idx=?";
                Log.log.trace(sql);
                PreparedStatement deleteStmt = conn.prepareStatement(sql);
                deleteStmt.setInt(1, idx);
                //delete the old entries
                deleteStmt.execute();
                deleteStmt.close();
            }
            
            long ts = System.currentTimeMillis();
            
            for(DTRecord dtrecord: dtrecords)
            {
                // init prepared statements
                String sql = null;
                
                // init select stmt
                sql = "SELECT idx,pathid,nodeid,seqid,userid FROM " + tablePrefix + tablename.toLowerCase() + " WHERE idx=? AND pathid=? AND nodeid=? AND seqid=? AND userid=?";
                Log.log.trace(sql);
                PreparedStatement selectStmt = conn.prepareStatement(sql);

                // init insert stmt
                String fields = "";
                String values = "";
                for(String fieldName : fieldNames)
                {
                    if(fieldName.contains(":string"))
                    {
                        fieldName = fieldName.split(":")[0];
                    }
                    fields += "," + fieldName; 
                    values += ", ?";
                }
                sql = "INSERT INTO " + tablePrefix + tablename.toLowerCase() + " (idx,pathid,nodeid,seqid,userid,ts,ts_datetime" + fields + ") ";
                sql += "VALUES (?,?,?,?,?,?,?" + values + ")";
                Log.log.trace(sql);
                PreparedStatement insertStmt = conn.prepareStatement(sql);
                
                // init update stmt
                fields = "ts=?,ts_datetime=?";
                
                for(String fieldName : fieldNames)
                {
                    if(fieldName.contains(":string"))
                    {
                        fieldName = fieldName.split(":")[0];
                    }
                    fields += "," + fieldName; 
                }
                sql = "UPDATE " + tablePrefix + tablename.toLowerCase() + " SET " + fields + " ";
                sql += "WHERE idx=? AND pathid=? AND nodeid=? AND seqid=? AND userid=?";
                Log.log.trace(sql);
                PreparedStatement updateStmt = conn.prepareStatement(sql);
                
                // check if row exists
                selectStmt.setInt(1, idx);
                selectStmt.setString(2, dtrecord.getPathID());
                selectStmt.setString(3, dtrecord.getNodeID());
                selectStmt.setLong(4, dtrecord.getSequenceID());
                selectStmt.setString(5, dtrecord.getUser());
                ResultSet rs = selectStmt.executeQuery();
                if (rs.next())
                {
                    // update row
                    int pos = 1;
                    updateStmt.setLong(pos++, ts);
                    updateStmt.setTimestamp(pos++, new Timestamp(GMTDate.getDate(ts).getTime()));
                    updateStmt.setLong(pos++, dtrecord.getNodeCount());
                    updateStmt.setLong(pos++, dtrecord.getPathCount());
                    updateStmt.setLong(pos++, dtrecord.getTotalTime());
                    updateStmt.setString(pos++, dtrecord.getStatus());
                    updateStmt.setString(pos++, dtrecord.getNamespace());
                    
                    updateStmt.setInt(pos++, idx); //slotid
                    updateStmt.setString(pos++, dtrecord.getPathID()); //pathid
                    updateStmt.setString(pos++, dtrecord.getNodeID()); //nodeid
                    updateStmt.setLong(pos++, dtrecord.getSequenceID()); //sequence id
                    updateStmt.setString(pos++, dtrecord.getUser()); //userid
                    
                    updateStmt.executeUpdate();
                }
                else
                {
                    // insert row
                    int pos = 1;
                    insertStmt.setInt(pos++, idx);
                    insertStmt.setString(pos++, dtrecord.getPathID());
                    insertStmt.setString(pos++, dtrecord.getNodeID());
                    insertStmt.setLong(pos++, dtrecord.getSequenceID());
                    insertStmt.setString(pos++, dtrecord.getUser());
                    insertStmt.setLong(pos++, ts);
                    insertStmt.setTimestamp(pos++, new Timestamp(GMTDate.getDate(ts).getTime()));
                    
                    insertStmt.setLong(pos++, dtrecord.getNodeCount());
                    insertStmt.setLong(pos++, dtrecord.getPathCount());
                    insertStmt.setLong(pos++, dtrecord.getTotalTime());
                    insertStmt.setString(pos++, dtrecord.getStatus());
                    insertStmt.setString(pos++, dtrecord.getNamespace());
                    
                    insertStmt.execute();
                }
                
                if (rs != null)
                {
                    rs.close();
                }
                if (selectStmt != null)
                {
                    selectStmt.close();
                }
                if (insertStmt != null)
                {
                    insertStmt.close();
                }
                if (updateStmt != null)
                {
                    updateStmt.close();
                }
            }
        }
        catch (Throwable e)
        {
            Log.log.error(e.getMessage(), e);
        }
        finally
        {
            try
            {
                if (conn != null)
                {
                    SQL.close(conn);
                }
            }
            catch (Exception e)
            {
                Log.log.error(e.getMessage(), e);
            }
        }
        
    } // updateDB

    /* (non-Javadoc)
     * @see com.resolve.rsmgmt.metric.MetricStore#loadTMetricDB(com.resolve.rsmgmt.ConfigMetric, int)
     */
    @Override
    public void loadTMetricDB(ConfigMetric config, int idx)
    {
        SQLConnection conn = null;

        try
        {
            conn = SQL.getConnection();

            // load tables for each group
            for (Map<String, String> group : config.getGroups())
            {
                String type = group.get("TYPE").toLowerCase();
                if(type.equalsIgnoreCase("decisiontree"))
                {
                    String tablename = group.get("NAME").toLowerCase();
                    String unitsStr = group.get("UNITS").toLowerCase();
                    String[] units = unitsStr.split(",");
                    
                    //clean up the table for records with this idx --- TODO check may be a performance concern
                    //init delete stmt        
                    String sql = "DELETE FROM " + TMETRIC_DAY + tablename.toLowerCase() + " WHERE idx=?";
                    Log.log.trace(sql);
                    PreparedStatement deleteStmt = conn.prepareStatement(sql);
                    deleteStmt.setInt(1, idx);
                    //delete the old entries
                    deleteStmt.execute();
                    deleteStmt.close();

                    // init insert stmt
                    String fields = "";
                    String values = "";
                    for(String fieldName : units)
                    {
                        if(fieldName.contains(":string"))
                        {
                            fieldName = fieldName.split(":")[0];
                        }
                        fields += "," + fieldName; 
                        values += ", ?";
                    }
                    
                    /*
                     *  load hr data from hr table into day table
                     */
                    sql = "SELECT pathid, nodeid, seqid, userid, node_cnt, sum(tot_cnt) as tot_cnt, sum(tot_time) as tot_time, status, dtnamespace from " +  TMETRIC_HR + tablename.toLowerCase() + 
                                    " where TS_DATETIME > ((SYSTIMESTAMP at TIME ZONE 'GMT') - INTERVAL '1' DAY) GROUP BY pathid, nodeid, seqid, userid, node_cnt, status, dtnamespace, TS ORDER BY TS asc";
                    Log.log.trace(sql);
//                    System.out.println(sql);
                    PreparedStatement selectStmt = conn.prepareStatement(sql);

                    ResultSet rs = selectStmt.executeQuery();
                    long ts = System.currentTimeMillis();
                    while(rs.next())
                    {
                        // init insert stmt
                        sql = "INSERT INTO " + TMETRIC_DAY + tablename.toLowerCase() + " (idx,pathid,nodeid,seqid,userid,ts,ts_datetime" + fields + ") ";
                        sql += "VALUES (?,?,?,?,?,?,?" + values + ")";
                        Log.log.trace(sql);
                        PreparedStatement insertStmt = conn.prepareStatement(sql);
                        // insert row
                        int pos = 1;
                        insertStmt.setInt(pos++, idx); // day index
                        insertStmt.setString(pos++, rs.getString("pathid")); //pathID
                        insertStmt.setString(pos++, rs.getString("nodeid")); // node or wiki name
                        insertStmt.setLong(pos++, rs.getLong("seqid")); // sequence id
                        insertStmt.setString(pos++, rs.getString("userid")); // user name
                        insertStmt.setLong(pos++, ts);
                        insertStmt.setTimestamp(pos++, new Timestamp(GMTDate.getDate(ts).getTime()));
                        
                        insertStmt.setLong(pos++, rs.getLong("node_cnt")); //nodes in a path
                        insertStmt.setLong(pos++, rs.getLong("tot_cnt")); //path count
                        insertStmt.setLong(pos++, rs.getLong("tot_time")); //total time 
                        insertStmt.setString(pos++, rs.getString("status")); //status i.e. completed or aborted
                        insertStmt.setString(pos++, rs.getString("dtnamespace"));//namespace
                        
                        insertStmt.execute();
                        
                        if (insertStmt != null)
                        {
                            insertStmt.close();
                        }
                    }
                    
                    if (rs != null)
                    {
                        rs.close();
                    }
                    if (selectStmt != null)
                    {
                        selectStmt.close();
                    }
                }
            }
        }
        catch (Throwable e)
        {
            Log.log.error(e.getMessage(), e);
        }
        finally
        {
            try
            {
                if (conn != null)
                {
                    SQL.close(conn);
                }
            }
            catch (Exception e)
            {
                Log.log.error(e.getMessage(), e);
            }
        }
    } // loadTMetricDB

    /* (non-Javadoc)
     * @see com.resolve.rsmgmt.metric.MetricStore#getAllDTPathIDs()
     */
    @Override
    public List<String> getAllDTPathIDs()
    {
        String sql = "SELECT U_PATH_ID FROM tmetric_lookup";
        List<String> result = new ArrayList<String>();
        
        SQLConnection connection = null;
        ResultSet rs = null;
        Statement statement = null;
        
        try
        {
            connection = SQL.getConnection();
            statement = connection.createStatement();
            rs = statement.executeQuery(sql);

            // while loop to get the data
            while (rs.next())
            {
                result.add(rs.getString("U_PATH_ID"));
            }// end of while loop
        }
        catch (Throwable err)
        {
            Log.log.error("Error in fetching data for :" + sql, err);
            throw new RuntimeException(err.getMessage());
        }
        finally
        {
            try
            {
                if (rs != null)
                {
                    rs.close();
                }

                if (statement != null)
                {
                    statement.close();
                }

                if (connection != null)
                {
                    connection.close();
                }
            }
            catch (Throwable err)
            {
                Log.log.error("Error in fetching data for :" + sql, err);
                throw new RuntimeException(err.getMessage());
            }
        }
        
        return result;
    } // getAllDTPathIDs
    
    /* (non-Javadoc)
     * @see com.resolve.rsmgmt.metric.MetricStore#insertIntoLookUpTable(java.lang.String, java.lang.String, java.lang.String)
     */
    @Override
    public void insertIntoLookUpTable(String pathID, String path, String username, String dtRootDoc)
    {
        SQLConnection connection = null;
        PreparedStatement statement = null;
        String sql = "INSERT INTO TMETRIC_LOOKUP(sys_id, u_path_id, u_full_path, u_dt_root_doc, sys_created_by, sys_updated_by, sys_created_on, sys_updated_on) values " +
                        "(?,?,?,?,?,?,?,?)";
        
        long ts = System.currentTimeMillis();
        int pos = 1;
        try
        {
            connection = SQL.getConnection();
            Log.log.debug("Executing SQL :\n" + sql);
            connection.getConnection().setAutoCommit(false);
            statement = connection.prepareStatement(sql);
            statement.setString(pos++, SysId.getSysId());
            statement.setString(pos++, pathID);
            statement.setString(pos++, path);
            statement.setString(pos++, dtRootDoc);
            statement.setString(pos++, username);
            statement.setString(pos++, username);
            statement.setTimestamp(pos++, new Timestamp(GMTDate.getDate(ts).getTime()));
            statement.setTimestamp(pos++, new Timestamp(GMTDate.getDate(ts).getTime()));
            statement.executeUpdate();
            connection.commit();
        }
        catch (Throwable t)
        {
            try
            {
                connection.getConnection().rollback();
            }
            catch (SQLException e)
            {
                Log.log.error("Rollback call is ignored: ", e);
                throw new RuntimeException(e.getMessage());
            }

            Log.log.error("Error in fetching data for :" + sql, t);
            throw new RuntimeException(t.getMessage());
        }
        finally
        {
            try
            {
                if (statement != null)
                {
                    statement.close();
                }

                if (connection != null)
                {
                    connection.close();
                }
            }
            catch (Throwable t)
            {
                Log.log.error("Error in closing statement or sql connection: ", t);
                throw new RuntimeException(t.getMessage());
            }
        }
        
    } //insertIntoLookUpTable

} // MetricStoreOracle