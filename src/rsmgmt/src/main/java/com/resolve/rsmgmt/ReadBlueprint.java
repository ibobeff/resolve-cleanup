/******************************************************************************
* (C) Copyright 2014
*
* Resolve Systems, LLC
*
* All rights reserved
* This software is distributed under license and may not be used, copied,
* modified, or distributed without the express written permission of
* Resolve Systems, LLC.
*
******************************************************************************/

package com.resolve.rsmgmt;

import java.io.File;
import java.io.FileInputStream;

import com.resolve.rsbase.MainBase;
import com.resolve.util.Constants;
import com.resolve.util.FileUtils;
import com.resolve.util.Log;
import com.resolve.util.Properties;
import com.resolve.util.StringUtils;

public class ReadBlueprint
{
    public static void readFile(String blueprintFileName, boolean reload)
    {
        Main main = (Main) MainBase.main;
        try
        {
            File blueprintFile = FileUtils.getFile(blueprintFileName);
            if (blueprintFile.exists())
            {
                Properties initialProperties = main.blueprintProperties;
                
                Properties configureProperties = new Properties();
                
                if (reload && initialProperties != null)
                {
                    configureProperties.putAll(initialProperties);
                }
                FileInputStream fis = new FileInputStream(blueprintFile);
                configureProperties.load(fis);
                fis.close();
                
                if (!reload && initialProperties != null)
                {
                    configureProperties.putAll(initialProperties);
                }
                
                // set DIST Value
                if (configureProperties.get(Constants.BLUEPRINT_DIST) == null)
                {
                    configureProperties.put(Constants.BLUEPRINT_DIST, main.configGeneral.getHome());
                }
                main.dist = configureProperties.get(Constants.BLUEPRINT_DIST);
                
                configureProperties.trimProperties();
                
                boolean validConfiguration = true;
                
                //get localhost value
                String localhost = configureProperties.get(Constants.BLUEPRINT_LOCALHOST);
                if (StringUtils.isNotEmpty(localhost))
                {
                    if (localhost.contains("${"))
                    {
                        localhost = configureProperties.getReplacement(Constants.BLUEPRINT_LOCALHOST);
                    }
                }
                else
                {
                    Main.println(Constants.BLUEPRINT_LOCALHOST + " Cannot be Empty");
                    Log.log.error(Constants.BLUEPRINT_LOCALHOST + " Cannot be Empty");
                    validConfiguration = false;
                }
                
                //get node list values
                String rsviewListStr = configureProperties.get(Constants.BLUEPRINT_RSVIEW_NODELIST);
                if (StringUtils.isNotEmpty(rsviewListStr) && rsviewListStr.contains("${"))
                {
                    rsviewListStr = configureProperties.getReplacement(Constants.BLUEPRINT_RSVIEW_NODELIST);
                }
                
                String rscontrolListStr = configureProperties.get(Constants.BLUEPRINT_RSCONTROL_NODELIST);
                if (StringUtils.isNotEmpty(rscontrolListStr) && rscontrolListStr.contains("${"))
                {
                    rscontrolListStr = configureProperties.getReplacement(Constants.BLUEPRINT_RSCONTROL_NODELIST);
                }
                
                String rssearchNodeListStr = configureProperties.get(Constants.BLUEPRINT_RSSEARCH_NODELIST);
                if (StringUtils.isNotEmpty(rssearchNodeListStr) && rssearchNodeListStr.contains("${"))
                {
                    rssearchNodeListStr = configureProperties.getReplacement(Constants.BLUEPRINT_RSSEARCH_NODELIST);
                }
                
                //get rsmq primary and backup values
                String rsmqPrimary = configureProperties.get(Constants.BLUEPRINT_RSMQ_PRIMARY_HOST);
                if (StringUtils.isNotEmpty(rsmqPrimary))
                {
                    if (rsmqPrimary.contains("${"))
                    {
                        rsmqPrimary = configureProperties.getReplacement(Constants.BLUEPRINT_RSMQ_PRIMARY_HOST);
                    }
                }
                else
                {
                    Main.println(Constants.BLUEPRINT_RSMQ_PRIMARY_HOST + " Cannot be Empty");
                    Log.log.error(Constants.BLUEPRINT_RSMQ_PRIMARY_HOST + " Cannot be Empty");
                    validConfiguration = false;
                }
                String rsmqBackup = configureProperties.get(Constants.BLUEPRINT_RSMQ_BACKUP_HOST);
                if (StringUtils.isNotEmpty(rsmqBackup))
                {
                    if (rsmqBackup.contains("${"))
                    {
                        rsmqBackup = configureProperties.getReplacement(Constants.BLUEPRINT_RSMQ_BACKUP_HOST);
                    }
                }
                
                //get server primary value
                if (StringUtils.isEmpty(configureProperties.get(Constants.BLUEPRINT_PRIMARY)))
                {
                    if (localhost.equalsIgnoreCase(rsmqPrimary))
                    {
                        configureProperties.put(Constants.BLUEPRINT_PRIMARY, "true");
                    }
                    else
                    {
                        configureProperties.put(Constants.BLUEPRINT_PRIMARY, "false");
                    }
                }
                
                //determine db schema if not set
                if (StringUtils.isEmpty(configureProperties.get(Constants.BLUEPRINT_DB_SCHEMA)))
                {
                    String dbType = configureProperties.get(Constants.BLUEPRINT_DB_TYPE);
                    if (StringUtils.isEmpty(dbType))
                    {
                        Log.log.warn("DB Type Missing, any DB connections will not be Configured");
                    }
                    else
                    {
                        if (dbType.equalsIgnoreCase("mysql"))
                        {
                            configureProperties.set(Constants.BLUEPRINT_DB_SCHEMA,
                                            configureProperties.get(Constants.BLUEPRINT_DB_NAME));
                        }
                        else if (dbType.equalsIgnoreCase("oracle"))
                        {
                            configureProperties.set(Constants.BLUEPRINT_DB_SCHEMA,
                                            configureProperties.get(Constants.BLUEPRINT_DB_USERNAME));
                        }
                        else if (dbType.equalsIgnoreCase("db2"))
                        {
                            configureProperties.set(Constants.BLUEPRINT_DB_SCHEMA,
                                            configureProperties.get(Constants.BLUEPRINT_DB_USERNAME));
                        }
                        else
                        {
                            Log.log.error("Unknown DB Type: " + dbType);
                            configureProperties.set(Constants.BLUEPRINT_DB_SCHEMA,
                                            configureProperties.get(Constants.BLUEPRINT_DB_USERNAME));
                        }
                    }
                }
                
                
                if (validConfiguration)
                {
                    //validate configuring components
                    main.blueprintRSRemote = isConfigured(configureProperties, "rsremote");
                    main.blueprintRSControl = isConfigured(configureProperties, "rscontrol");
                    main.blueprintRSView = isConfigured(configureProperties, "rsview");
                    
                    // RSControl and RSView needs to read RSRemote properties to setup SDK Gateway properties
                    if (!main.blueprintRSRemote && (main.blueprintRSControl || main.blueprintRSView))
                    {
                        main.blueprintSDKGateway = isSDKGatewayConfigured(configureProperties);
                    }
                    
                    main.blueprintRSMQ = isConfigured(configureProperties, "rsmq");
                    main.blueprintRSConsole = isConfigured(configureProperties, "rsconsole");
                    main.blueprintRSMgmt = isConfigured(configureProperties, "rsmgmt");
                    main.blueprintRSSearch = isConfigured(configureProperties, "rssearch");
                    main.blueprintRSSync = isConfigured(configureProperties, "rssync");
                    main.blueprintRSArchive = isConfigured(configureProperties, "rsarchive");
                    main.setBlueprintLogstash(isConfigured(configureProperties, "logstash"));
                    main.setBlueprintRSLog(isConfigured(configureProperties, "rslog"));
                    main.setBlueprintDCS(isConfigured(configureProperties, "dcs"));
                    main.setBlueprintDCSRSDataLoader(isConfigured(configureProperties, "dcs.rsdataloader"));
                    main.setBlueprintDCSRSReporting(isConfigured(configureProperties, "dcs.rsreporting"));
                    
                    String trimViewNodeList = StringUtils.isNotEmpty(rsviewListStr) ? rsviewListStr.replaceAll("\\s", "") : "";
                    configureProperties.set(Constants.BLUEPRINT_RSVIEW_NODELIST, trimViewNodeList);
                    
                    if (main.blueprintRSView && !trimViewNodeList.toUpperCase().contains(localhost.toUpperCase()))
                    {
                        Main.println(Constants.BLUEPRINT_LOCALHOST + " in Blueprint must be in the "
                            + Constants.BLUEPRINT_RSVIEW_NODELIST + " for RSView to be configured");
                        Main.println("Setting RSView Configuration to False");
                        Log.log.error(Constants.BLUEPRINT_LOCALHOST + " in Blueprint (" + localhost + ") must belong to "
                            + Constants.BLUEPRINT_RSVIEW_NODELIST + " (" + trimViewNodeList + ")");
                        main.blueprintRSView = false;
                        configureProperties.set("resolve.rsview", "false");
                    }
                    
                    String trimControlNodeList = StringUtils.isNotEmpty(rscontrolListStr) ? rscontrolListStr.replaceAll("\\s", "") : "";
                    configureProperties.set(Constants.BLUEPRINT_RSCONTROL_NODELIST, trimControlNodeList);
                    
                    if (main.blueprintRSControl && !trimControlNodeList.toUpperCase().contains(localhost.toUpperCase()))
                    {
                        Main.println(Constants.BLUEPRINT_LOCALHOST + " in Blueprint must be in the "
                            + Constants.BLUEPRINT_RSCONTROL_NODELIST + " for RSControl to be configured");
                        Main.println("Setting RSControl Configuration to False");
                        Log.log.error(Constants.BLUEPRINT_LOCALHOST + " in Blueprint (" + localhost + ") must belong to "
                            + Constants.BLUEPRINT_RSCONTROL_NODELIST + " (" + trimControlNodeList + ")");
                        main.blueprintRSControl = false;
                        configureProperties.set("resolve.rscontrol", "false");
                    }
                        
                    String trimRSSearchNodeList = StringUtils.isNotEmpty(rssearchNodeListStr) ? rssearchNodeListStr.replaceAll("\\s", "") : "";
                    configureProperties.set(Constants.BLUEPRINT_RSSEARCH_NODELIST, trimRSSearchNodeList);
                    
                    if (main.blueprintRSSearch && !trimRSSearchNodeList.toUpperCase().contains(localhost.toUpperCase()))
                    {
                        Main.println(Constants.BLUEPRINT_LOCALHOST + " in Blueprint must be in the "
                            + Constants.BLUEPRINT_RSSEARCH_NODELIST + " for RSSearch to be configured");
                        Main.println("Setting RSSearch Configuration to False");
                        Log.log.error(Constants.BLUEPRINT_LOCALHOST + " in Blueprint (" + localhost + ") must belong to "
                            + Constants.BLUEPRINT_RSSEARCH_NODELIST + " (" + trimRSSearchNodeList + ")");
                        main.blueprintRSSearch = false;
                        configureProperties.set("resolve.rssearch", "false");
                    }
                    
                    //double check that rsmq should be configured
                    if (main.blueprintRSMQ)
                    {
                        if (!localhost.equalsIgnoreCase(rsmqPrimary) && !localhost.equalsIgnoreCase(rsmqBackup))
                        {
                            main.blueprintRSMQ = false;
                            configureProperties.set("resolve.rsmq", "false");
                            Main.println(Constants.BLUEPRINT_LOCALHOST + " Does Not Match " + Constants.BLUEPRINT_RSMQ_PRIMARY_HOST
                                + " or " + Constants.BLUEPRINT_RSMQ_BACKUP_HOST);
                            Main.println("Setting RSMQ Configuration to False");
                            Log.log.warn(Constants.BLUEPRINT_LOCALHOST + " (" + localhost + ") Does Not Match "
                                + Constants.BLUEPRINT_RSMQ_PRIMARY_HOST + " (" + rsmqPrimary + ") or " + Constants.BLUEPRINT_RSMQ_BACKUP_HOST
                                + " (" + rsmqBackup + "), Setting Configuration to False");
                        }
                        else if (localhost.equalsIgnoreCase(rsmqPrimary))
                        {
                            configureProperties.set("rsmq.primary", "true");
                        }
                        else
                        {
                            configureProperties.set("rsmq.primary", "false");
                        }
                    }
                        
                    main.blueprintProperties = configureProperties;
                }
                else
                {
                    main.blueprintRSRemote = false;
                    main.blueprintRSControl = false;
                    main.blueprintRSView = false;
                    main.blueprintRSMQ = false;
                    main.blueprintRSConsole = false;
                    main.blueprintRSMgmt = false;
                    main.blueprintRSSearch = false;
                    main.blueprintProperties = null;
                    
                    Main.println("WARNING!!!! Invalid Configuration");
                    Log.log.error("Invalid Configuration, setting all components to false");
                }
            }
            else
            {
                Main.println("Blueprint File Does Not Exist: " + blueprintFile.getAbsolutePath());
                Log.log.error("Blueprint File Does Not Exist: " + blueprintFile.getAbsolutePath());
                main.blueprintProperties = null;
            }
        }
        catch (Exception e)
        {
            Main.println("Failed to Read in Blueprint Configuraion: " + e.getMessage());
            Log.log.error("Failed to Read in Blueprint Configuraion: " + e.getMessage(), e);
            main.blueprintProperties = null;
        }
    } //readFile
    
    private static boolean isConfigured(Properties blueprint, String component)
    {
        boolean isConfigured = false;
        
        String configuredKey = "resolve." + component.toLowerCase();
        String configuredValue = blueprint.get(configuredKey);
        
        if (StringUtils.isNotEmpty(configuredValue))
        {
            if (configuredValue.contains("${"))
            {
                configuredValue = blueprint.getReplacement(configuredKey);
            }
            isConfigured = "true".equalsIgnoreCase(configuredValue);
        }
        Log.log.debug("Component " + component + " is Configured: " + isConfigured);
        
        return isConfigured;
    } //isConfigured
    
    private static boolean isSDKGatewayConfigured(Properties blueprint)
    {
        boolean isSDKGatewayConfigured = false;
        
        for (Object key : blueprint.keySet())
        {
            if (key instanceof String)
            {
                String strKey= (String)key;
                
                if (strKey.endsWith("implprefix") && StringUtils.isNotBlank(blueprint.get(strKey)))
                {
                    isSDKGatewayConfigured = true;
                    break;
                }
            }
        }
        
        return isSDKGatewayConfigured;
    }
} //ReadBlueprint
