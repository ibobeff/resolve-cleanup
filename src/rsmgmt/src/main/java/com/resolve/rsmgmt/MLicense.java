/******************************************************************************
* (C) Copyright 2014
*
* Resolve Systems, LLC
*
* All rights reserved
* This software is distributed under license and may not be used, copied,
* modified, or distributed without the express written permission of
* Resolve Systems, LLC.
*
******************************************************************************/

package com.resolve.rsmgmt;

import java.util.HashMap;
import java.util.Map;

import com.resolve.rsbase.MainBase;
import com.resolve.service.LicenseService;

/**
 * This is just for RSMGMT to handle a very little message about license.
 * Make sure it overrides all other method from super class except upload
 * because RSMGMT does not have to support other operations except upload.
 * This is when RSView cannot start because of expired license and only
 * way to load new license is to send message to RSMgmt via RSConsole.
 */
public class MLicense extends com.resolve.rsbase.MLicense
{
    @Override
    public Map<String, String> getLicenseInfo(Map<String, String> params)
    {
        return new HashMap<String, String>();
    }
    
    @Override
    public void remove(Map<String, String> params)
    {
    }
    
    @Override
    public void synchronizeLicense(Map<String, String> params)
    {
    }

    @Override
    public void sendLicense(Map<String, String> params)
    {
    }

    @Override
    public Map<String, String> getHostIPAddress(Map<String, String> params)
    {
        return new HashMap<String, String>();
    }

    @Override
    public void registerComponent(Map<String, String> params)
    {
    }

    @Override
    public void registerGateway(Map<String, String> params)
    {
    }
    
    @Override
    public void removeGatewayRunningInstance(Map<String, String> params)
    {
    }
    
    @Override
    public void switchGatewayRunningInstanceType(Map<String, String> params)
    {
    }
    
    @Override
    public void updateGatewayRunningInstance(Map<String, String> params)
    {
    }
}
