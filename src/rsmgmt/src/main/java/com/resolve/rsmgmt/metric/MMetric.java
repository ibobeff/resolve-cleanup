/******************************************************************************
* (C) Copyright 2014
*
* Resolve Systems, LLC
*
* All rights reserved
* This software is distributed under license and may not be used, copied,
* modified, or distributed without the express written permission of
* Resolve Systems, LLC.
*
******************************************************************************/

package com.resolve.rsmgmt.metric;

import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.StringTokenizer;
import java.util.WeakHashMap;
import java.util.concurrent.ConcurrentHashMap;
import java.util.regex.Pattern;

import org.codehaus.jackson.map.ObjectMapper;
import org.codehaus.jackson.type.TypeReference;

import com.resolve.rsbase.MainBase;
import com.resolve.rsbase.BaseMetric;
import com.resolve.rsmgmt.Main;
import com.resolve.rsmgmt.Metric;
import com.resolve.search.APIFactory;
import com.resolve.search.MetricAggregationAPI;
import com.resolve.search.SearchAPI;
import com.resolve.search.SearchConstants;
import com.resolve.search.metric.MetricDataIndexAPI;
import com.resolve.search.model.MetricActionTaskData;
import com.resolve.search.model.MetricCPU;
import com.resolve.search.model.MetricDatabase;
import com.resolve.search.model.MetricJMSQueue;
import com.resolve.search.model.MetricJVM;
import com.resolve.search.model.MetricLatency;
import com.resolve.search.model.MetricRunbook;
import com.resolve.search.model.MetricTransaction;
import com.resolve.search.model.MetricUsers;
import com.resolve.search.model.Worksheet;
import com.resolve.services.ServiceJDBC;
import com.resolve.services.hibernate.util.MetricUtil;
import com.resolve.services.hibernate.vo.MetricThresholdVO;
import com.resolve.services.interfaces.VO;
import com.resolve.services.vo.QueryDTO;
import com.resolve.util.Constants;
import com.resolve.util.ERR;
import com.resolve.util.Log;
import com.resolve.util.MetricMsg;
import com.resolve.util.MetricThresholdType;
import com.resolve.util.MetricUnit;
import com.resolve.util.StringUtils;
import com.resolve.vo.DTMetricLogVO;
import com.resolve.vo.TMetricLogVO;

public class MMetric
{
    static ConcurrentHashMap<String, Map<String, MetricThresholdVO>> defaultThresholds = new ConcurrentHashMap<String, Map<String, MetricThresholdVO>>();
    static String GroupNameSeparator = "///";
    static WeakHashMap<String, Pattern> ruleRegx = new WeakHashMap<String, Pattern>();
    static Map<MetricThresholdVO, Map<String, Set<String>>> deployments = new ConcurrentHashMap<MetricThresholdVO, Map<String, Set<String>>>();
    
    
    public static void initThresholds()
    {
        updateThresholds(null);
    }

    public static void updateThresholds(Map<String, MetricThresholdType> params)
    {
        if (!Main.thresholdEnabled)
        {
            return;
        }
        
        QueryDTO query = new QueryDTO();
        query.setTableName("metric_threshold");
//        if (Main.mcpEnabled)
        if (MetricUtil.isManagedMCP())
        {
//            query.setWhereClause(" (u_active='true' or u_active is NULL or u_active='') and u_frommcp <> '' and u_version > 0 ");
            query.setWhereClause(" u_frommcp = 'true' and u_version > 0 ");
        }
        else
        {
            query.setWhereClause(" u_version > 0 and ( u_frommcp != 'true' or u_frommcp is null ) ");
//            query.setWhereClause(" u_version > 0 ");
        }
        
        query.setUseSql(true);
        List<MetricThresholdVO> data = ServiceJDBC.getMetricThreshold(query);
        
        for (MetricThresholdVO vo : data)
        {
            if (StringUtils.isBlank(vo.getUConditionOperator()) || VO.STRING_DEFAULT.equals(vo.getUConditionOperator()))
            {
                if ("undef".equals(vo.getUHigh()) && !"undef".equals(vo.getULow()))
                {
                    vo.setUConditionOperator(Constants.METRICTHRESHOLD_LESS);
                    vo.setUConditionValue(vo.getULow());
                }
                else if ("undef".equals(vo.getULow()) && !"undef".equals(vo.getUHigh()))
                {
                    vo.setUConditionOperator(Constants.METRICTHRESHOLD_GREATER);
                    vo.setUConditionValue(vo.getUHigh());
                }
            }
        }
        
        ConcurrentHashMap<String, Map<String, MetricThresholdVO>> newThresholds = new ConcurrentHashMap<String, Map<String, MetricThresholdVO>>();
        for (MetricThresholdVO vo : data)
        {
            if (StringUtils.isEmpty(vo.getUGroup()) || StringUtils.isEmpty(vo.getUName()))
            {
                throw new RuntimeException("Invalid threshold group or metric name: group=" + vo.getUGroup() + ", metric name=" + vo.getUName() + ", threshold module=" + vo.getURuleModule() + ", threshold name=" + vo.getURuleName()); 
            }
            String keyName = vo.getUGroup() + GroupNameSeparator + vo.getUName();
            Map<String, MetricThresholdVO> currentMap = newThresholds.get(keyName);
            if (currentMap==null)
            {
                newThresholds.put(keyName, currentMap=new HashMap<String, MetricThresholdVO>() );
            }
            
            String thresholdKey = vo.getURuleModule() + GroupNameSeparator + vo.getURuleName();
            MetricThresholdVO currentVO = currentMap.get(thresholdKey);
            if (currentVO!=null)
            {
                if (vo.getUVersion() >= currentVO.getUVersion())
                {
                    currentMap.put(thresholdKey, vo);
                }
            }
            else
            {
                currentMap.put(thresholdKey, vo);
            }
        }
        defaultThresholds = newThresholds;
        
        clearThresholdCache();
        
        for (Map<String, MetricThresholdVO> value : defaultThresholds.values())
        {
            for (MetricThresholdVO m : value.values())
            {
                if ("default".equalsIgnoreCase(m.getUType()))
                {
                    continue;
                }
                
                String addr = m.getUIpaddress();
                if (StringUtils.isNotEmpty(addr))
                {
                    Map<String,Map<String, List<String>>> deployMap = null;
                    try 
                    {
                        deployMap = (Map<String,Map<String, List<String>>>) (new ObjectMapper().readValue(addr, new TypeReference<Map<String,Map<String, List<String>>>>() { }));
                    }
                    catch (Exception ex)
                    {
                        Log.log.error("Failed to read deployment data from rule", ex);
                        continue;
                    }
                  
                    String cluster = MainBase.main.configGeneral.getClusterName();
                    Map<String, List<String>> hMap = deployMap.get(cluster);
                    if (hMap!=null && !hMap.isEmpty())
                    {
                        Map<String, Set<String>> hInfo = new HashMap<String, Set<String>>();
                        for (String host : hMap.keySet())
                        {
                            Set<String> cInfo = new HashSet<String>();
                            for (String cmp : hMap.get(host))
                            {
                                cInfo.add(cmp);
                            }
                            hInfo.put(host, cInfo);
                        }
                        deployments.put(m, hInfo);

                    }
                }
            }
        }
        
        
//        for (String key : newThresholds.keySet())
//        {
//            Map<String, MetricThresholdVO> v = newThresholds.get(key);
//            System.out.println("key=" + key + ", size=" + v.size());
//            for (MetricThresholdVO mt : v.values())
//            {
//                System.out.println(" mt.active is " + mt.getUActive() + ", mt.version = " + mt.getUVersion()); 
//            }
//        }
    }
    
    public static Map info(Map params)
    {
        Map result = new HashMap(params);
        
        result.put("NAME", MainBase.main.configId.name);
        
        // TODO - replace with TrendMetric stats
            
        return result;
    } // info

    public static void insert(Object obj)
    {
        if(obj instanceof TMetricLogVO)
        {
            Log.log.debug(((TMetricLogVO)obj).toString());
            TrendMetric.insert((TMetricLogVO)obj);
        }
        else if(obj instanceof DTMetricLogVO)
        {
            TrendMetric.insert((DTMetricLogVO) obj);
        }
        else if(obj instanceof MetricMsg)
        {
            Log.log.debug(((MetricMsg)obj).toString());
            //TrendMetric.insert((MetricMsg)obj);
            
            MetricMsg msg = (MetricMsg)obj;
            if ("jvm".equals(msg.getName()))
            {
                processJVMMsg(msg);
            }
            else if ("runbook".equals(msg.getName()))
            {
                processRunbookMsg(msg);                
            }
            else if ("transaction".equals(msg.getName()))
            {
                processTransactionMsg(msg);                
            }
            else if ("server".equals(msg.getName()))
            {
                processCPUMsg(msg);                
            }
            else if ("jms_queue".equals(msg.getName()))
            {
                processJMSQueueMsg(msg);                
            }
            else if ("users".equals(msg.getName()))
            {
                processUsersMsg(msg);
            }
            else if ("latency".equals(msg.getName()))
            {
                processLatencyMsg(msg);
            }
            else if ("database".equals(msg.getName()))
            {
            	processDBMessage(msg);
            }
            else if ("licensedUsers".equals(msg.getName()))
            {
            	//currently we aren't reporting on this metric
            }
            else if (msg.getName().startsWith(SearchConstants.METRIC_ACTIONTASK_UNIT_PREFIX))
            {
                processActionTaskMsg(msg);                
            }
            else
            {
                Log.log.error("Unknown metric type: " + msg.getName() + " from source: " + msg.getSource());
                return;
            }

            checkThresholds(msg);
        }
    } // insert
    
//    static Map<MetricThresholdVO, Set<String>> thresholdHosts = new ConcurrentHashMap<MetricThresholdVO, Set<String>>(); 
//    static Map<MetricThresholdVO, Set<String>> thresholdComponents = new ConcurrentHashMap<MetricThresholdVO, Set<String>>();
//    private synchronized static Set<String> getThresholdHosts(MetricThresholdVO vo)
//    {
//        Set<String> result = thresholdHosts.get(vo);
//        if (result==null)
//        {
//            result = new HashSet<String>();
//            StringTokenizer st = new StringTokenizer(vo.getUIpaddress(), ",");
//            while (st.hasMoreTokens())
//                result.add(st.nextToken().toUpperCase());
//        }
//        return result;
//    }
//
//    private synchronized static Set<String> getThresholdComponents(MetricThresholdVO vo)
//    {
//        Set<String> result = thresholdComponents.get(vo);
//        if (result==null)
//        {
//            result = new HashSet<String>();
//            StringTokenizer st = new StringTokenizer(vo.getUComponenttype(), ",");
//            while (st.hasMoreTokens())
//                result.add(st.nextToken().toUpperCase());
//        }
//        return result;
//    }

    private static void clearThresholdCache()
    {
        deployments.clear();
    }
    
    private static void checkThresholds(MetricMsg msg)
    {
        String group = msg.getName();
        HashMap<String, MetricUnit> units = msg.getUnits();
        for (String key : units.keySet())
        {
            Map<String, MetricThresholdVO> voMap = defaultThresholds.get(group + GroupNameSeparator + key);
            
            if (voMap==null||voMap.isEmpty())
            {
                continue;
            }

            MetricUnit u = units.get(key);
            for (MetricThresholdVO vo : voMap.values())
            {
                if (!StringUtils.isTrue(vo.getUActive()))
                {
                    continue;
                }

                if (StringUtils.isBlank(vo.getURuleModule()) && StringUtils.isBlank(vo.getURuleName()))
                {
                    continue;
                }
                
                if (!"default".equals(vo.getUType()))
                {
                    Map<String, Set<String>> hosts = deployments.get(vo); 
                    if (hosts!=null)
                    {
                        Set<String> cmps = hosts.get(msg.hostname);
                        if (cmps==null || cmps.isEmpty())
                        {
                            cmps = hosts.get(msg.ipaddress);
                        }
                        
                        if (!cmps.contains(msg.component))
                        {
                            continue;
                        }
                    }
                    else
                    {
                        continue;
                    }
                    
//                    if (StringUtils.isBlank(vo.getUIpaddress()) && StringUtils.isBlank(vo.getUComponenttype()))
//                    {
//                        Log.log.debug("Both address and component are empty for non-auto rule " + vo.getURuleModule() + "." + vo.getURuleName());
//                        continue;
//                    }
//                    else if (!StringUtils.isBlank(vo.getUIpaddress()))
//                    {
//                        if (!getThresholdHosts(vo).contains(msg.hostname) && !getThresholdHosts(vo).contains(msg.ipaddress))
//                        {
//                            continue;
//                        }
//                        if (!StringUtils.isBlank(vo.getUComponenttype()))
//                        {
//                            if (!getThresholdComponents(vo).contains(msg.component))
//                            {
//                                continue;
//                            }
//                        }
//                    }
//                    else
//                    {
//                        continue;
//                    }
                }
                
                
                if (evaluateThreshold(u, vo, msg.getSource()) && StringUtils.isNotBlank(vo.getURuleModule()) && 
                    StringUtils.isNotBlank(vo.getURuleName()))
                {
                    String fullmetricname = group + "." + key;
                    Log.alert(ERR.E80001.getMessage(), "Metric " + fullmetricname + " with value: " + u.getTotal() + " triggered threshold " + vo.getURuleModule() + "." + vo.getURuleName() + ". Received from " + msg.getSource(), vo.getUAlertStatus(), msg.getSource(), vo.getUAction());
                }
            }
        }            
    }
    
    
    private static boolean evaluateThreshold(MetricUnit u, MetricThresholdVO vo, String source)
    {
        if (StringUtils.isBlank(vo.getUConditionOperator()) || vo.STRING_DEFAULT.equals(vo.getUConditionOperator()))
        {
            return false;
        }
            
        if (StringUtils.isBlank(vo.getUConditionValue()) || vo.STRING_DEFAULT.equals(vo.getUConditionValue()))
        {
            return false;
        }
                        
        String opStr = vo.getUConditionOperator();
        String valueStr = vo.getUConditionValue();
        
        String[] ops = opStr.split(",");
        String[] values = valueStr.split(",");
        
        for (int i=0; i<ops.length; ++i)
        {
            String op = ops[i];
            long total = u.getTotal();
            long value = Long.parseLong(values[i]);
            
            if (Constants.METRICTHRESHOLD_EQUAL.equals(op))
            {
                if (total!=value)
                    return false;
            }
            else if (Constants.METRICTHRESHOLD_GREATER.equals(op))
            {
                if (!(total > value))
                    return false;
            }
            else if (Constants.METRICTHRESHOLD_GREATEREQUAL.equals(op))
            {
                if (!( total >= value))
                    return false;
            }
            else if (Constants.METRICTHRESHOLD_LESS.equals(op))
            {
                if (!(total<value))
                    return false;
            }
            else if (Constants.METRICTHRESHOLD_LESSEQUAL.equals(op))
            {
                if (!(total <= value))
                    return false;
            }
            else
            {
                throw new RuntimeException("Unsupported condition operator: " + op + " in threshold " + vo.getURuleModule() + "." + vo.getURuleName());
            }
        }
        
        if (StringUtils.isNotEmpty(vo.getUSource()) && StringUtils.isNotEmpty(source))
        {
             Pattern p = ruleRegx.get(vo.getUSource());
             if (p==null)
             {
                 ruleRegx.put(vo.getUSource(), p = Pattern.compile(vo.getUSource()));
             }
             if (!p.matcher(source).matches())
             {
                 return false;
             }
        }
        
        return true;
    }
    
    
    //private static SearchAPI<Worksheet> worksheetSearchApi =  APIFactory.getWorksheetSearchAPI(Worksheet.class);//   new SearchAPI<Worksheet>("metricjvm", "metricjvm", null, false, Worksheet.class);
    
    
    private static void processJVMMsg(MetricMsg msg)
    {
        MetricJVM jvm = new MetricJVM();
        jvm.setId(msg.getId());
        jvm.setSrc(msg.getSource());
        jvm.setTs(System.currentTimeMillis());
        HashMap<String, MetricUnit> units = msg.getUnits();
        long memFree = 0, threadCt = 0;
        for (String key : units.keySet())
        {
            MetricUnit u = units.get(key);
            if ("mem_free".equals(key))
            {
                jvm.setFreeMem(memFree=u.getTotal());
            }
            else if ("thread_count".equals(key))
            {
                jvm.setThreadCount(threadCt=u.getTotal());
            }
        }
        MetricDataIndexAPI.indexMetricJVM(jvm);
        
//        checkJVMThreshold(memFree, threadCt, msg);
    }

    private static void processTransactionMsg(MetricMsg msg)
    {
        MetricTransaction transaction = new MetricTransaction();
        transaction.setId(msg.getId());
        transaction.setSrc(msg.getSource());
        transaction.setTs(System.currentTimeMillis());
        HashMap<String, MetricUnit> units = msg.getUnits();
        long trans = 0, latency = 0;
        for (String key : units.keySet())
        {
            MetricUnit u = units.get(key);
            if ("transaction".equals(key))
            {
                transaction.setTransaction(trans=u.getTotal());
            }
            else if ("latency".equals(key))
            {
                transaction.setLatency(latency=u.getTotal());
            }
        }
        MetricDataIndexAPI.indexMetricTransaction(transaction);
  //      checkTransactionThreshold(transaction);
    }
    
    private static void processCPUMsg(MetricMsg msg)
    {
        MetricCPU cpu = new MetricCPU();
        cpu.setId(msg.getId());
        cpu.setSrc(msg.getSource());
        cpu.setTs(System.currentTimeMillis());
        HashMap<String, MetricUnit> units = msg.getUnits();
        for (String key : units.keySet())
        {
            MetricUnit u = units.get(key);
            if ("load1".equals(key))
            {
                cpu.setLoad1(u.getTotal());
            }
            else if ("load5".equals(key))
            {
                cpu.setLoad5(u.getTotal());
            }
            else if ("load15".equals(key))
            {
                cpu.setLoad15(u.getTotal());
            }
        }
        MetricDataIndexAPI.indexMetricCPU(cpu);
//        checkCPUThreshold(cpu);
    }

    private static void processJMSQueueMsg(MetricMsg msg)
    {
        MetricJMSQueue jmsQueue = new MetricJMSQueue();
        jmsQueue.setId(msg.getId());
        jmsQueue.setSrc(msg.getSource());
        jmsQueue.setTs(System.currentTimeMillis());
        HashMap<String, MetricUnit> units = msg.getUnits();
        for (String key : units.keySet())
        {
            MetricUnit u = units.get(key);
            if ("msgs_count".equals(key))
            {
                jmsQueue.setMsgsCount(u.getTotal());
            }
        }
        MetricDataIndexAPI.indexMetricJMSQueue(jmsQueue);
//        checkJMSQueueThreshold(jmsQueue);
    }
    
    private static void processUsersMsg(MetricMsg msg)
    {
        MetricUsers users = new MetricUsers();
        users.setId(msg.getId());
        users.setSrc(msg.getSource());
        users.setTs(System.currentTimeMillis());
        HashMap<String, MetricUnit> units = msg.getUnits();
        for (String key : units.keySet())
        {
            MetricUnit u = units.get(key);
            if ("active".equals(key))
            {
                users.setActive(u.getTotal());
            }
        }
        MetricDataIndexAPI.indexMetricUsers(users);
//        checkUsersThreshold(users);
    }
    
    private static void processLatencyMsg(MetricMsg msg)
    {
        MetricLatency lat = new MetricLatency();
        lat.setId(msg.getId());
        lat.setSrc(msg.getSource());
        lat.setTs(System.currentTimeMillis());
        HashMap<String, MetricUnit> units = msg.getUnits();
        for (String key : units.keySet())
        {
            MetricUnit u = units.get(key);
            if ("wiki".equals(key))
            {
                lat.setWiki(u.getTotal());
            }
            else if ("wikiresponsetime".equals(key))
            {
                lat.setWikiResponseTime(u.getTotal());
            }
            else if ("social".equals(key))
            {
                lat.setSocial(u.getTotal());
            }
            else if ("socialresponsetime".equals(key))
            {
                lat.setWikiResponseTime(u.getTotal());
            }
            else
            {
                throw new RuntimeException("Unknown metric property name " + key);
            }
        }
        MetricDataIndexAPI.indexMetricLatency(lat);
//        checkLatencyThreshold(lat);
    }
    
    private static void processDBMessage(MetricMsg msg)
    {
    	MetricDatabase metricDB = new MetricDatabase();
    	metricDB.setId(msg.getId());
    	metricDB.setSrc(msg.getSource());
    	metricDB.setTs(System.currentTimeMillis());
    	HashMap<String, MetricUnit> units = msg.getUnits();
    	
        for (String key : units.keySet())
        {
        	MetricUnit u = units.get(key);
        	
        	if (key.equals("free_space"))
        	{
        		metricDB.setFreeSpace(u.getTotal());
        	}
        	else if (key.equals("size"))
        	{
        		metricDB.setSize(u.getTotal());
        	}
        	else if (key.equals("percentage_used"))
        	{
        		metricDB.setPercentUsed(u.getTotal());
        	}
        	else if (key.equals("query_count"))
        	{
        		metricDB.setQueryCount(u.getTotal());
        	}
        	else if (key.equals("response_time"))
        	{
        		metricDB.setResponseTime(u.getTotal());
        	}
        	else if (key.equals("percentage_wait"))
        	{
        		metricDB.setPercentWait(u.getTotal());
        	}
        }
        
        MetricDataIndexAPI.indexMetricDB(metricDB);
    }
    
    private static void processActionTaskMsg(MetricMsg msg)
    {
        String group = msg.getName().substring(SearchConstants.METRIC_ACTIONTASK_UNIT_PREFIX.length());
        HashMap<String, MetricUnit> units = msg.getUnits();
        for (String key : units.keySet())
        {
            MetricActionTaskData atd = new MetricActionTaskData();
            atd.setGroup(group);
            atd.setId(msg.getId());
            atd.setSrc(msg.getSource());
            atd.setTs(System.currentTimeMillis());

            MetricUnit u = units.get(key);
            atd.setName(u.getName());
            atd.setValue(u.getTotal());
            atd.setCount(u.getCount());
            MetricDataIndexAPI.indexMetricActionTask(atd);
//            checkActionTaskThreshold(atd);
        }
    }
    
    private static void processRunbookMsg(MetricMsg msg)
    {
        MetricRunbook runbook = new MetricRunbook();
        runbook.setId(msg.getId());
        runbook.setSrc(msg.getSource());
        runbook.setTs(System.currentTimeMillis());
        HashMap<String, MetricUnit> units = msg.getUnits();
        for (String key : units.keySet())
        {
            MetricUnit u = units.get(key);
            if ("aborted".equals(key))
            {
                runbook.setAborted(u.getTotal());
            }
            else if ("completed".equals(key))
            {
                runbook.setCompleted(u.getTotal());
            }
            else if ("gcondition".equals(key))
            {
                runbook.setGcondition(u.getTotal());
            }
            else if ("bcondition".equals(key))
            {
                runbook.setBcondition(u.getTotal());
            }
            else if ("ucondition".equals(key))
            {
                runbook.setUcondition(u.getTotal());
            }
            else if ("gseverity".equals(key))
            {
                runbook.setGseverity(u.getTotal());
            }
            else if ("cseverity".equals(key))
            {
                runbook.setCseverity(u.getTotal());
            }
            else if ("wseverity".equals(key))
            {
                runbook.setWseverity(u.getTotal());
            }
            else if ("sseverity".equals(key))
            {
                runbook.setSseverity(u.getTotal());
            }
            else if ("useverity".equals(key))
            {
                runbook.setUseverity(u.getTotal());
            }
            else if ("duration".equals(key))
            {
                runbook.setDuration(u.getTotal());
            }
        }
        MetricDataIndexAPI.indexMetricRunbook(runbook);
//        checkRunbookThreshold(runbook);
    }
    
    private static void checkJVMThreshold(long memFree, long threadCount, MetricMsg msg)
    {
//        String memKey = "jvm" + GroupNameSeparator + "mem_free";
//        Map<String, MetricThresholdVO> thMap = defaultThresholds.get(memKey);
//        for (MetricThresholdVO v : thMap.values())
//        {
//            if(!VO.STRING_DEFAULT.equals(v.getULow()))
//            {                
//                long th = Long.decode(v.getULow());
//                if (th < memFree)
//                {
//                    String alertMsg = "Free memory on " + msg.getSource() + " is " + memFree + ", lower than threadhold value " + th + ". Threshold name is " +  v.getURuleModule() + "." + v.getURuleName();
//                    Log.alert(ERR.E80001.getMessage() + "-" + alertMsg, alertMsg, v.getUAlertStatus());
//                }
//            }
//            
//            if(!VO.STRING_DEFAULT.equals(v.getUHigh()))
//            {                
//                long th = Long.decode(v.getUHigh());
//                if (th < memFree)
//                {
//                    String alertMsg = "Free memory on " + msg.getSource() + " is " + memFree + ", higher than threadhold value " + th + ". Threshold name is " +  v.getURuleModule() + "." + v.getURuleName();
//                    Log.alert(ERR.E80001.getMessage() + "-" + alertMsg, alertMsg, v.getUAlertStatus());
//                }
//            }
//        }
//        
//        String threadKey = "jvm" + GroupNameSeparator + "thread_count";
//        thMap = defaultThresholds.get(threadKey);
//        for (MetricThresholdVO v : thMap.values())
//        {
//            if(!VO.STRING_DEFAULT.equals(v.getULow()))
//            {                
//                long th = Long.decode(v.getULow());
//                if (th < threadCount)
//                {
//                    String alertMsg = "Thread count on " + msg.getSource() + " is " + threadCount + ", lower than threadhold value " + th + ". Threshold name is " +  v.getURuleModule() + "." + v.getURuleName();
//                    Log.alert(ERR.E80001.getMessage() + "-" + alertMsg, alertMsg, v.getUAlertStatus());
//                }
//            }
//            
//            if(!VO.STRING_DEFAULT.equals(v.getUHigh()))
//            {                
//                long th = Long.decode(v.getUHigh());
//                if (th < threadCount)
//                {
//                    String alertMsg = "Thread count on " + msg.getSource() + " is " + threadCount + ", higher than threadhold value " + th + ". Threshold name is " +  v.getURuleModule() + "." + v.getURuleName();
//                    Log.alert(ERR.E80001.getMessage() + "-" + alertMsg, alertMsg, v.getUAlertStatus());
//                }
//            }
//        }
        
        //Test aggregation APIs
        Map<String, Double> memMap = MetricAggregationAPI.jvmFreeMemoryHistorgram(new String[] {"src"}, new String[] {"YU-HP.rscontrol/9F8DE213CE31148DE3F84607A3EBA563"}, Constants.METRIC_INTERVAL_MINUTE, 5, null, null);
        Log.log.debug("Free memory map " + memMap);
        Map<String, Double> ctMap = MetricAggregationAPI.jvmThreadCountHistorgram(new String[] {"src"}, new String[] {"YU-HP.rscontrol/9F8DE213CE31148DE3F84607A3EBA563"}, Constants.METRIC_INTERVAL_MINUTE, 5);
        Log.log.debug("Thread count map " + ctMap);

        Metric.mthreshold.checkThresholds("jvm", new Long(memFree).intValue(), "mem_free");
        Metric.mthreshold.checkThresholds("jvm", new Long(threadCount).intValue(), "thread_count");
    }

    
    private static void checkRunbookThreshold(MetricRunbook mr)
    {
        //Test aggregation APIs
        Map<String, Double> rbMap = MetricAggregationAPI.runbookMetricHistorgram(new String[] {"src"}, new String[] {"YU-HP.rscontrol/9F8DE213CE31148DE3F84607A3EBA563"}, Constants.METRIC_INTERVAL_MINUTE, 5, "completed", null, null);
        Log.log.debug("runbook completed map " + rbMap);
        
        Metric.mthreshold.checkThresholds("runbook", new Long(mr.getCompleted()).intValue(), "completed");
    }
    
    private static void checkTransactionThreshold(MetricTransaction mr)
    {
        //Test aggregation APIs
        Map<String, Double> rbMap = MetricAggregationAPI.transactionMetricHistorgram(new String[] {"src"}, new String[] {"YU-HP.rscontrol/9F8DE213CE31148DE3F84607A3EBA563"}, Constants.METRIC_INTERVAL_MINUTE, 5, "transaction", null, null);
        Log.log.debug("transaction metric map " + rbMap);
        
        Metric.mthreshold.checkThresholds("transaction", new Long(mr.getTransaction()).intValue(), "transaction");
    }

    private static void checkJMSQueueThreshold(MetricJMSQueue mr)
    {
        //Test aggregation APIs
        Map<String, Double> rbMap = MetricAggregationAPI.jmsQueueMetricHistorgram(new String[] {"src"}, new String[] {"YU-HP.rscontrol/9F8DE213CE31148DE3F84607A3EBA563"}, Constants.METRIC_INTERVAL_MINUTE, 5, "msgsCount", null, null);
        Log.log.debug("JMS queue metric map " + rbMap);
        
        Metric.mthreshold.checkThresholds("jms_queue", new Long(mr.getMsgsCount()).intValue(), "msgs_count");
    }

    private static void checkUsersThreshold(MetricUsers mr)
    {
        //Test aggregation APIs
        Map<String, Double> rbMap = MetricAggregationAPI.usersMetricHistorgram(new String[] {"src"}, new String[] {"YU-HP.rscontrol/9F8DE213CE31148DE3F84607A3EBA563"}, Constants.METRIC_INTERVAL_MINUTE, 5, "active", null, null);
        Log.log.debug("Users metric map " + rbMap);
        
        Metric.mthreshold.checkThresholds("users", new Long(mr.getActive()).intValue(), "active");
    }

    private static void checkLatencyThreshold(MetricLatency mr)
    {
        //Test aggregation APIs
        Map<String, Double> rbMap = MetricAggregationAPI.latencyMetricHistorgram(new String[] {"src"}, new String[] {"YU-HP.rscontrol/9F8DE213CE31148DE3F84607A3EBA563"}, Constants.METRIC_INTERVAL_MINUTE, 5, "wiki", null, null);
        Log.log.debug("Latency metric map " + rbMap);
        
        Metric.mthreshold.checkThresholds("latency", new Long(mr.getWiki()).intValue(), "wiki");
    }

    private static void checkActionTaskThreshold(MetricActionTaskData mr)
    {
        //Test aggregation APIs
        Map<String, Double> rbMap = MetricAggregationAPI.actionTaskMetricHistorgram(new String[] {"src", "group", "name"}, new String[] {"YU-HP.rscontrol/9F8DE213CE31148DE3F84607A3EBA563", mr.getGroup(), mr.getName()}, Constants.METRIC_INTERVAL_MINUTE, 5, "value", null, null);
        Log.log.debug("Action task metric map " + rbMap);
        
        Metric.mthreshold.checkThresholds(mr.getGroup(), new Long(mr.getValue()).intValue(), "value");
    }

    private static void checkCPUThreshold(MetricCPU mr)
    {
        //Test aggregation APIs
        Map<String, Double> rbMap = MetricAggregationAPI.cpuMetricHistorgram(new String[] {"src"}, new String[] {"YU-HP.rscontrol/9F8DE213CE31148DE3F84607A3EBA563"}, Constants.METRIC_INTERVAL_MINUTE, 5, "load1", null, null);
        Log.log.debug("cpu load1 metric map " + rbMap);
        
        Metric.mthreshold.checkThresholds("server", new Long(mr.getLoad1()).intValue(), "load1");
    }
    
    public static void process5Min(Map params)
    {
        TrendMetric.process5Min();
    } // process5Min
    
    public static void process1Hr(Map params)
    {
        TrendMetric.process1Hr();
    } // process1Hr
    
    public static void process1Day(Map params)
    {
        TrendMetric.process1Day();
    } // process1Day
    
    /**
     * This method sends a JMS message to the guid with a map <metricThresholdData> that contains the 
     * updated list of metric threshold names and metric threshold values 
     *  Cases covered
     *   0. When no updates to metricThresholdData is coming from the UI or from the 
     *   1. Deploy a brand new metric created from UI to an existing component
     *   GUID == "" => metricThresholdData will not be null or empty during deploying
     *   2. Deploy a already existing metric threshold which is updatedin the UI to an existing component
     *   GUID == guid
     * @param guid
     * @param metricThresholdData
     */
    public static void refreshMetricThresholdProperties(String guid, Map<String, MetricThresholdType> metricThresholdData)
    {
        // If is no metric threshold data update coming from the UI
        if(metricThresholdData == null || metricThresholdData.keySet().isEmpty())
        {
            // Return all default thresholds from the MetricThreshold table in database
            QueryDTO query = new QueryDTO();
            query.setTableName("metric_threshold");
            query.setUseSql(true);
            List<MetricThresholdVO> data = ServiceJDBC.getMetricThreshold(query);
            if(data.isEmpty()) 
            {
                Log.log.error("No default metric threshold are available -- please check that metric_threshold db table is not empty");
                try
                {
                    throw new Exception(); // helps in debugging
                }
                catch (Exception e)
                {
                    Log.log.error(e.getMessage(), e);
                }
            }
            else
            {
                // get the already persisting metric threshold value to an existing component or guid
                // first and if not available then get the default values in the database. 
                metricThresholdData = new HashMap<String, MetricThresholdType>();
                for(MetricThresholdVO metric : data)
                {     
                    
                    String metricname = metric.getUGroup() + "." + metric.getUName();
                    MetricThresholdType metrictype = new MetricThresholdType();
                    String regex = "[0-9]+";
                    String high_value = metric.getUHigh();
                    String low_value = metric.getULow();
                    if(high_value.matches(regex) && low_value.matches(regex))
                    {
                        metrictype.setHigh(metric.getUHigh());
                        metrictype.setLow(metric.getULow());
                    }
                    else if(high_value.matches(regex))
                    {
                        metrictype.setHigh(metric.getUHigh());
                        metrictype.setLow("undef");
                    }
                    else if(low_value.matches(regex))
                    {
                        metrictype.setLow(metric.getULow());
                        metrictype.setHigh("undef");
                    }
                    else
                    {
                        metrictype.setHigh("undef");
                        metrictype.setLow("undef");
                    }
                    metrictype.setAlertstatus(metric.getUAlertStatus());                    


                    // get the already persisting metric threshold value to an existing component
                    if(metric.getUGuid() == guid)
                    {
                        metricThresholdData.put(metricname, metrictype);
                    }

                    // if value does not persist for this guid and if the value has not been added earlier then add the default values
                    if(StringUtils.isEmpty(metric.getUGuid()) && !isKeyPresent(metricThresholdData, metricname))
                    {
                        metricThresholdData.put(metricname, metrictype);
                    }
                }
            }
        }

        //send JMS to required component
        Log.log.debug("Sending JMS message to " + guid);
        MainBase.esb.sendMessage(guid, "MMetric.updateMetricThresholds", metricThresholdData);

    } // refreshMetricThresholdProperties

    private static boolean isKeyPresent(Map<String, MetricThresholdType> metricThresholdData, String keyname)
    {
        boolean isPresent = false;
        for(String key : metricThresholdData.keySet())
        {
            if(key.equals(keyname)) //is case sensitive so a exact match is necessary
            {
                isPresent = true;
                break;
                
            }
        }
        
        return isPresent;
        
    } // isKeyPresent
    
    /**
     * This method is called when the system components such as rsview,rscontrol,rsmgmt, and rsremote start up.
     * It is called from the Main.java by where each component sends an ESB message to the main RSMGMT to get
     * all the persisted or default metric threshold values.
     * @param params
     */
    @SuppressWarnings({ "unchecked", "rawtypes" })
    public static void updateMetricThresholdProperties(Map params)
    {
        if(params == null)
        {
            Log.log.error("");
            try
            {
                throw new Exception();
            }
            catch (Exception e)
            {
                Log.log.error(e.getMessage(), e);
            }
        }
        else
        {
            String guid = (String) params.get("GUID");
            params.remove("GUID");
            refreshMetricThresholdProperties(guid, params);
        }
    } // updateMetricThresholdProperties
    
    public static void updateMetricThresholds(Map<String, MetricThresholdType> params)
    {
        // update thresholds
        BaseMetric.updateMetricThresholds(params);
    } //updateMetricThresholds
    
} // MMetric
