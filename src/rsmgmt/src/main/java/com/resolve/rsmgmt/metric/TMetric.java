/******************************************************************************
* (C) Copyright 2014
*
* Resolve Systems, LLC
*
* All rights reserved
* This software is distributed under license and may not be used, copied,
* modified, or distributed without the express written permission of
* Resolve Systems, LLC.
*
******************************************************************************/

package com.resolve.rsmgmt.metric;

import com.resolve.util.TMetricCache;

/**
 * @author alokika.dash
 *
 */
public interface TMetric
{
    public Object analyzeCache(TMetricCache metric, MetricStore store);
}
