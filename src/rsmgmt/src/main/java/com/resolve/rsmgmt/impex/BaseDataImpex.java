/******************************************************************************
* (C) Copyright 2014
*
* Resolve Systems, LLC
*
* All rights reserved
* This software is distributed under license and may not be used, copied,
* modified, or distributed without the express written permission of
* Resolve Systems, LLC.
*
******************************************************************************/

package com.resolve.rsmgmt.impex;

import java.io.ByteArrayInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.math.BigDecimal;
import java.sql.Date;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import java.sql.SQLException;
import java.sql.Statement;
import java.sql.Time;
import java.sql.Timestamp;
import java.sql.Types;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.poi.hssf.usermodel.HSSFSheet;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.Row;

import com.resolve.sql.SQLConnection;
import com.resolve.util.DateUtils;
import com.resolve.util.FileUtils;
import com.resolve.util.Log;
import com.resolve.util.StringUtils;
import com.resolve.util.SysId;

public abstract class BaseDataImpex implements DataImpex
{

    protected final SQLConnection sqlConnection;
    protected final DataParser dataParser;

    public BaseDataImpex(SQLConnection sqlConnection, DataParser dataParser)
    {
        this.sqlConnection = sqlConnection;
        this.dataParser = dataParser;
    }

    /**
     * Simply try to reconstruct the original line from the input file. Note
     * that this may not be the exact line because it might have come from an
     * excel file and we convert it to CSV.
     * 
     * @param fileData
     * @return
     */
    private String getDataLine(String[] fileData)
    {
        StringBuilder result = new StringBuilder();
        for (String data : fileData)
        {
            result.append(data + ",");
        }
        result.setLength(result.length() - 1);
        return result.toString();
    }

    private void insertData(String userId, String insertOrUpdateStatement, PreparedStatement ps, ResultSetMetaData metaData, Map<String, Integer[]> fileHeaderMap, String[] fileRecord, boolean stopOnFailure) throws Exception
    {
        int tableColumnCount = metaData.getColumnCount();

        for (int metColumnCounter = 1; metColumnCounter <= tableColumnCount; metColumnCounter++)
        {
            String columnName = metaData.getColumnName(metColumnCounter).toLowerCase();
            int columnType = metaData.getColumnType(metColumnCounter);
            if (fileHeaderMap.containsKey(columnName))
            {
                // set the unavailable system columns
                if (fileHeaderMap.get(columnName)[0] == -1)
                {
                    String value = getEmptySystemColumnValue(columnName, userId);
                    setSqlType(fileHeaderMap.get(columnName)[1], columnName, columnType, ps, value);
                }
                else
                {
                    setSqlType(fileHeaderMap.get(columnName)[1], columnName, columnType, ps, fileRecord[fileHeaderMap.get(columnName)[0]]);
                }
            }
        }
        ps.execute();
        ps.clearParameters();
    }

    private String getEmptySystemColumnValue(String columnName, String userId)
    {
        String result = null;

        long currentTimeInMillis = Calendar.getInstance().getTimeInMillis();

        if ("sys_id".equalsIgnoreCase(columnName))
        {
            result = SysId.getSysId();
        }
        else if ("sys_created_by".equalsIgnoreCase(columnName) || "sys_updated_by".equalsIgnoreCase(columnName))
        {
            result = userId;
        }
        else if ("sys_created_on".equalsIgnoreCase(columnName) || "sys_updated_on".equalsIgnoreCase(columnName))
        {
            result = String.valueOf(currentTimeInMillis);
        }

        return result;
    }

    private void updateData(String sysId, String userId, String updateStatement, PreparedStatement ps, ResultSetMetaData metaData, Map<String, Integer[]> fileHeaderMap, String[] fileRecord, boolean stopOnFailure) throws Exception
    {
        int tableColumnCount = metaData.getColumnCount();

        for (int metColumnCounter = 1; metColumnCounter <= tableColumnCount; metColumnCounter++)
        {
            String columnName = metaData.getColumnName(metColumnCounter).toLowerCase();
            int columnType = metaData.getColumnType(metColumnCounter);
            if (fileHeaderMap.containsKey(columnName))
            {
                if (fileHeaderMap.get(columnName)[0] == -1)
                {
                    String value = getEmptySystemColumnValue(columnName, userId);
                    setSqlType(fileHeaderMap.get(columnName)[1], columnName, columnType, ps, value);
                }
                else
                {
                    setSqlType(fileHeaderMap.get(columnName)[1], columnName, columnType, ps, fileRecord[fileHeaderMap.get(columnName)[0]]);
                }
            }
        }

        // in case of update, where sys_id=? is guranteed to be the
        // last parameter in the prepared statement, so this line
        // makes sure the parameter is set properly.
        ps.setString(fileHeaderMap.size() + 1, sysId);

        ps.execute();
        ps.clearParameters();
    }

    /**
     * This method provides the correct date/time format based on the value
     * being passed.
     * 
     * @param value
     * @return
     */
    private String getDatetimeFormat(String value)
    {
        String result = null;
        if (value.length() == DataImpexUtils.DATETIME_FORMAT1.length())
        {
            result = DataImpexUtils.DATETIME_FORMAT1;
        }
        else if (value.length() == DataImpexUtils.DATETIME_FORMAT2.length())
        {
            result = DataImpexUtils.DATETIME_FORMAT2;
        }
        else if (value.length() == DataImpexUtils.DATE_FORMAT.length())
        {
            result = DataImpexUtils.DATE_FORMAT;
        }
        else if (value.length() == DataImpexUtils.TIME_FORMAT1.length())
        {
            result = DataImpexUtils.TIME_FORMAT1;
        }
        else if (value.length() == DataImpexUtils.TIME_FORMAT2.length())
        {
            result = DataImpexUtils.TIME_FORMAT2;
        }

        return result;
    }

    private void setSqlType(int parameterIndex, String columnName, int columnType, PreparedStatement ps, String value) throws Exception
    {
        if (StringUtils.isNotBlank(value))
        {
            value = value.trim();

            try
            {
                switch (columnType)
                {
                    case -7: // bit
                        ps.setBoolean(parameterIndex, Boolean.parseBoolean(value));
                        break;
                    case -6: // byte
                        ps.setByte(parameterIndex, Byte.valueOf(value));
                        break;
                    case -5: // long
                        ps.setLong(parameterIndex, Long.valueOf(value));
                        break;
                    case -4: // longvarbinary
                    case -3: // byte
                    case -2: // byte
                        ps.setBytes(parameterIndex, value.getBytes());
                        break;
                    case 2004:// oracle blob
                        ps.setBlob(parameterIndex, new ByteArrayInputStream(value.getBytes()));
                        break;
                    case -1: // longvarchar
                    case 2005:// oracle clob
                        ps.setString(parameterIndex, value);
                        break;
                    case 0: // null
                        ps.setNull(parameterIndex, Types.NULL);
                        break;
                    case 1: // String
                        ps.setString(parameterIndex, value);
                        break;
                    case 2: // Numeric
                    case 3: // Decimal
                        ps.setBigDecimal(parameterIndex, new BigDecimal(value));
                        break;
                    case 4: // Integer
                        try
                        {
                            ps.setInt(parameterIndex, Integer.valueOf(value));
                        }
                        catch (NumberFormatException e)
                        {
                            throw new Exception("Invalid value \"" + value + "\" for numeric column \"" + columnName + "\"");
                        }
                        break;
                    case 5: // short
                        ps.setShort(parameterIndex, Short.valueOf(value));
                        break;
                    case 6: // float
                        ps.setFloat(parameterIndex, Float.valueOf(value));
                        break;
                    case 7: // real
                    case 8: // double
                        ps.setDouble(parameterIndex, Double.valueOf(value));
                        break;
                    case 12: // String
                        ps.setString(parameterIndex, value);
                        break;
                    case 91: // Date
                        System.out.println(value);
                        Date date = Date.valueOf(value);
                        System.out.println(date);
                        ps.setDate(parameterIndex, Date.valueOf(value));
                        break;
                    case 92: // Time
                        ps.setTime(parameterIndex, Time.valueOf(value));
                        break;
                    case 93: // Timestamp
                        /*
                         * this is where we have to have restriction on the
                         * value coming in. All time values aretreated in 24
                         * hour form.for example: - time value must be in
                         * HH:mm:ss.SSSS format (e.g., 13:25:10.000) - date
                         * value must be in yyyy/MM/dd (e.g., 2013/12/23) -
                         * datetime value must be in yyyy/MM/dd HH:mm:ss.SSS
                         * format (e.g., 2013/12/23 23:12:23.123)
                         */
                        Long longValue = null;
                        try
                        {
                            longValue = Long.valueOf(value);
                        }
                        catch (NumberFormatException e)
                        {
                            // it's fine, the value is not a long but in other
                            // format
                        }
                        if (longValue == null)
                        {
                            String format = getDatetimeFormat(value);
                            if (format == null)
                            {
                                throw new Exception("Invalid value \"" + value + "\" for date and/or time column \"" + columnName + "\"");
                            }
                            else
                            {
                                Timestamp ts = DateUtils.convertStringToTimestamp(value, new SimpleDateFormat(format));
                                ps.setTimestamp(parameterIndex, ts);
                            }
                        }
                        else
                        {
                            ps.setTimestamp(parameterIndex, new Timestamp(longValue));
                        }
                        break;
                    default:
                        throw new Exception("Unsupported datatype:" + columnType);
                }
            }
            catch (Exception e1)
            {
                // Log.log.error("Exception in column name: " + columnName +
                // " with its value: \"" + value + "\"\nActual exception is:\n"
                // + e1.getMessage(), e1);
                throw e1;
            }
        }
        else
        {
            ps.setNull(parameterIndex, Types.NULL);
        }
    }

    public boolean importData(String userId, String tableName, String data, char delimeter, boolean isOverride, boolean stopOnFailure) throws Exception
    {
        boolean result = true;

        Log.log.debug("Importing data from CSV to the custom table:" + tableName);

        Statement st = sqlConnection.createStatement();
        PreparedStatement preparedStatement = sqlConnection.prepareStatement("SELECT sys_id from " + tableName + " where sys_id=?");

        ResultSet rs;
        try
        {
            // this is a fake query to simply get a ResultSet object for its
            // meta data, we don't want any data here.
            rs = st.executeQuery("SELECT * from " + tableName + " where 1=1");

            ResultSetMetaData metaData = rs.getMetaData();

            List<String[]> fileData = dataParser.parseData(data, delimeter);

            // stores the column header, it's index in the data array and the
            // parameter index for the PreparedStatement.
            // Integer[0]=data array index
            // Integer[1]=parameter index
            Map<String, Integer[]> fileHeaderInsertMap = new HashMap<String, Integer[]>();
            Map<String, Integer[]> fileHeaderUpdateMap = new HashMap<String, Integer[]>();

            if (fileData != null && fileData.size() > 0)
            {
                // first String[] is the header.
                String[] fileHeaders = fileData.get(0);
                // once the header is extracted, remove it because there is no
                // use later.
                fileData.remove(0);

                // move them to a Map so it's efficient.
                for (int fieldCounter = 0; fieldCounter < fileHeaders.length; fieldCounter++)
                {
                    fileHeaderInsertMap.put(fileHeaders[fieldCounter].toLowerCase(), new Integer[] { fieldCounter, -1 });
                    fileHeaderUpdateMap.put(fileHeaders[fieldCounter].toLowerCase(), new Integer[] { fieldCounter, -1 });
                }

                // if the incoming data has sys_id we'll see if that record
                // exists for an update
                StringBuilder insertStatement = prepareInsertStatement(tableName, metaData, fileHeaderInsertMap);
                StringBuilder updateStatement = prepareUpdateStatement(tableName, metaData, fileHeaderUpdateMap);

                int recordCounter = 0;

                PreparedStatement insertPreparedStatement = sqlConnection.prepareStatement(insertStatement.toString());
                PreparedStatement updatePreparedStatement = sqlConnection.prepareStatement(updateStatement.toString());

                for (String[] fileRecord : fileData)
                {
                    try
                    {
                        if (fileHeaderUpdateMap.containsKey("sys_id") && fileHeaderUpdateMap.get("sys_id")[0] > -1)
                        {
                            // get the sys_id from the fileRecord
                            String sysId = fileRecord[fileHeaderUpdateMap.get("sys_id")[0]];
                            if (isRecordExists(sysId, tableName, preparedStatement))
                            {
                                if (isOverride)
                                {
                                    updateData(sysId, userId, updateStatement.toString(), updatePreparedStatement, metaData, fileHeaderUpdateMap, fileRecord, stopOnFailure);
                                }
                            }
                            else
                            {
                                // this must be inserted
                                insertData(userId, insertStatement.toString(), insertPreparedStatement, metaData, fileHeaderInsertMap, fileRecord, stopOnFailure);
                            }
                        }
                        else
                        {
                            insertData(userId, insertStatement.toString(), insertPreparedStatement, metaData, fileHeaderInsertMap, fileRecord, stopOnFailure);
                        }
                        recordCounter++;
                    }
                    catch (Exception e)
                    {
                        Log.log.warn("ERROR: Line#" + (recordCounter + 1) + "->" + getDataLine(fileRecord));
                        Log.log.warn(e.getMessage());
                        if (stopOnFailure)
                        {
                            throw e;
                        }
                    }
                }
            }
        }
        catch (SQLException e)
        {
            Log.log.error(e.getMessage(), e);
            throw e;
        }
        catch (ArrayIndexOutOfBoundsException e)
        {
            Log.log.error("There is error parsing the data file, it's possibly wrongly formatted with wrong number of columns in one or more line, please check", e);
            throw e;
        }
        catch (Exception e)
        {
            Log.log.error(e.getMessage(), e);
            throw e;
        }

        return result;
    }

    private boolean isRecordExists(String sysId, String tableName, PreparedStatement findRecordQuery)
    {
        boolean result = false;
        try
        {
            findRecordQuery.setString(1, sysId);
            ResultSet rs = findRecordQuery.executeQuery();

            if (rs != null && rs.next())
            {
                result = true;
            }
        }
        catch (SQLException e)
        {
            Log.log.error(e.getMessage(), e);
        }

        return result;
    }

    private StringBuilder prepareInsertStatement(String tableName, ResultSetMetaData metaData, Map<String, Integer[]> fileHeaderMap) throws Exception
    {
        // prepare insert statement
        StringBuilder insertStatement = new StringBuilder("insert into " + tableName + "(");
        StringBuilder insertValues = new StringBuilder(" values (");
        int parameterIndex = 1;
        for (int i = 1; i <= metaData.getColumnCount(); i++)
        {
            String columnName = metaData.getColumnName(i).toLowerCase();
            if (fileHeaderMap.containsKey(columnName))
            {
                insertStatement.append(columnName + ",");
                insertValues.append("?,");
                fileHeaderMap.get(columnName)[1] = parameterIndex++;
            }
        }

        if (parameterIndex > 1)
        {
            // The order of this block is important, may refactor later.
            if (!fileHeaderMap.containsKey("sys_id"))
            {
                insertStatement.append("sys_id,");
                insertValues.append("?,");
                fileHeaderMap.put("sys_id", new Integer[] { -1, parameterIndex++ });
            }
            if (!fileHeaderMap.containsKey("sys_created_by"))
            {
                insertStatement.append("sys_created_by,");
                insertValues.append("?,");
                fileHeaderMap.put("sys_created_by", new Integer[] { -1, parameterIndex++ });
            }
            if (!fileHeaderMap.containsKey("sys_created_on"))
            {
                insertStatement.append("sys_created_on,");
                insertValues.append("?,");
                fileHeaderMap.put("sys_created_on", new Integer[] { -1, parameterIndex++ });
            }
            if (!fileHeaderMap.containsKey("sys_updated_by"))
            {
                insertStatement.append("sys_updated_by,");
                insertValues.append("?,");
                fileHeaderMap.put("sys_updated_by", new Integer[] { -1, parameterIndex++ });
            }
            if (!fileHeaderMap.containsKey("sys_updated_on"))
            {
                insertStatement.append("sys_updated_on,");
                insertValues.append("?,");
                fileHeaderMap.put("sys_updated_on", new Integer[] { -1, parameterIndex++ });
            }
            insertStatement.setLength(insertStatement.length() - 1);
            insertStatement.append(")");
            insertValues.setLength(insertValues.length() - 1);
            insertValues.append(")");
            insertStatement.append(insertValues);
        }
        else
        {
            throw new Exception("There is nothing to import, please check the file and the delimiter, also make sure you have exact column names in the file as in the DB Table.");
        }
        return insertStatement;
    }

    private StringBuilder prepareUpdateStatement(String tableName, ResultSetMetaData metaData, Map<String, Integer[]> fileHeaderMap) throws Exception
    {
        // prepare update statement
        StringBuilder updateStatement = new StringBuilder("update " + tableName + " set ");
        int parameterIndex = 1;
        for (int i = 1; i <= metaData.getColumnCount(); i++)
        {
            String columnName = metaData.getColumnName(i).toLowerCase();
            if (fileHeaderMap.containsKey(columnName))
            {
                updateStatement.append(columnName + "=?,");
                fileHeaderMap.get(columnName)[1] = parameterIndex++;
            }
        }

        if (parameterIndex > 1)
        {
            // The order of this block is important, may refactor later.
            if (!fileHeaderMap.containsKey("sys_id"))
            {
                updateStatement.append("sys_id=?,");
                fileHeaderMap.put("sys_id", new Integer[] { -1, parameterIndex++ });
            }
            if (!fileHeaderMap.containsKey("sys_updated_by"))
            {
                updateStatement.append("sys_updated_by=?,");
                fileHeaderMap.put("sys_updated_by", new Integer[] { -1, parameterIndex++ });
            }
            if (!fileHeaderMap.containsKey("sys_updated_on"))
            {
                updateStatement.append("sys_updated_on=?,");
                fileHeaderMap.put("sys_updated_on", new Integer[] { -1, parameterIndex++ });
            }
            updateStatement.setLength(updateStatement.length() - 1);
            updateStatement.append(" where sys_id = ?");
        }
        else
        {
            throw new Exception("There is nothing to import, please check the file and the delimiter");
        }

        return updateStatement;
    }

    public String exportData(String userId, String tableName, char delimeter) throws Exception
    {
        ResultSet rs = sqlConnection.executeQuery("SELECT * from " + tableName);

        DataImpexUtils utils = new DataImpexUtils();
        String[] columns = utils.getColumnNames(rs);
        StringBuilder sb = new StringBuilder();
        // add the columns
        for (String column : columns)
        {
            sb.append("\"" + column + "\"" + delimeter);
        }
        sb.setLength(sb.length() - 1);
        sb.append("\n");
        while (rs.next())
        {
            String[] values = utils.getColumnValues(rs);
            for (String value : values)
            {
                sb.append("\"" + value + "\"" + delimeter);
            }
            sb.setLength(sb.length() - 1);
            sb.append("\n");
        }
        System.out.println(sb.toString());
        return sb.toString();
    }

    public void exportToXLS(String userId, String file, String tableName) throws Exception
    {
        ResultSet rs = sqlConnection.executeQuery("SELECT * from " + tableName);

        DataImpexUtils utils = new DataImpexUtils();
        String[] columns = utils.getColumnNames(rs);

        int rowNumber = 0;
        //int totalColumnCount = rs.getMetaData().getColumnCount();

        HSSFWorkbook workbook = new HSSFWorkbook();
        HSSFSheet sheet = workbook.createSheet(tableName);
        // Create a new row in current sheet to insert the column names
        Row row = sheet.createRow(rowNumber++);
        int columnNumber = 0;
        for (String column : columns)
        {
            // Create a new cell in current row
            Cell cell = row.createCell(columnNumber++);
            // Set value to new value
            cell.setCellValue(column);
        }

        while (rs.next())
        {
            // create a new row for the rs data.
            row = sheet.createRow(rowNumber++);
            columnNumber = 0;
            String[] values = utils.getColumnValues(rs);
            for (String value : values)
            {
                // Create a new cell in current row
                Cell cell = row.createCell(columnNumber++);
                // Set value to new value
                cell.setCellValue(value);
            }
        }

        try
        {
            FileOutputStream out = new FileOutputStream(FileUtils.getFile(file));
            workbook.write(out);
            out.close();
            Log.log.debug("Excel file " + file + " created successfully..");
        }
        catch (FileNotFoundException e)
        {
            Log.log.error(e.getMessage(), e);
            throw e;
        }
        catch (IOException e)
        {
            Log.log.error(e.getMessage(), e);
            throw e;
        }
    }
}
