/******************************************************************************
* (C) Copyright 2014
*
* Resolve Systems, LLC
*
* All rights reserved
* This software is distributed under license and may not be used, copied,
* modified, or distributed without the express written permission of
* Resolve Systems, LLC.
*
******************************************************************************/

package com.resolve.rsmgmt;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.Statement;
import java.sql.Timestamp;

import com.resolve.rsbase.MainBase;
import com.resolve.sql.SQL;
import com.resolve.sql.SQLConnection;
import com.resolve.util.ERR;
import com.resolve.util.GMTDate;
import com.resolve.util.Log;
import com.resolve.util.SysId;

public class DBStatus implements Runnable
{
    public static final String heartbeatUValue = "RSMGMT DB HEARTBEAT";
    private static DBStatus dbStatus;
    private static boolean busy;
    Main main = (Main) MainBase.main;
    private int thresholdTps;
    private String dbType;
    
    private DBStatus()
    {
        busy = false;
    }
    
    public static DBStatus getInstance()
    {
        if (dbStatus == null)
        {
            dbStatus = new DBStatus();
        }
        return dbStatus;
    } //getInstance
    
    public void run() {
        busy = true;
        SQLConnection conn = null;
        try
        {
            if (main.isDBConnected())
            {
                conn = SQL.getConnection();
                
                String sql = "update resolve_event set sys_updated_on=? where u_value='" + heartbeatUValue + "'";
                Log.log.trace("DB Heartbeat update sql: " + sql);
                
                PreparedStatement ps = conn.prepareStatement(sql);
                Timestamp updatedDate = new Timestamp(GMTDate.getTime());
                
                ps.setTimestamp(1, updatedDate);
                
                Log.start("Begin Update Execute", "debug");
                int update = ps.executeUpdate();
                Log.duration("Finish Update Execute", "debug");
                Log.log.debug("DB Hearbeat Run, " + update + " rows updated");
            }
            else
            {
                Log.log.warn("No DB Pool Available, Unable to run DB Status");
            }
        }
        catch (Throwable t)
        {
            Log.alert(ERR.E20005, t);
        }
        finally
        {
            if (conn != null)
            {
                SQL.close(conn);
            }
            busy = false;
            synchronized(this)
            {
                notifyAll();
            }
        }
    } //run
    
    public synchronized boolean startUpCheck()
    {
        boolean result = false;
        SQLConnection conn = null;
        try
        {
            if (main.isDBConnected())
            {
                conn = SQL.getConnection();
                
                String sql = "select sys_id from resolve_event where u_value='" + heartbeatUValue + "'";
                Log.log.debug("Check for Heartbeat DB Entry");
                Log.log.trace("SQL to Find if Heartbeat event already set up: " + sql);
                
                ResultSet rs = conn.executeQuery(sql);
                if (rs.next())
                {
                    String sysId = rs.getString(1);
                    Log.log.trace("Found Heartbeat Entry, sys_id=" + sysId);
                    result = true;
                }
                else
                {
                    String sysId = SysId.generate(conn);
                    sql = "insert into resolve_event (sys_id, u_value, sys_updated_by, sys_updated_on, sys_created_by, "
                        + "sys_created_on) values(?, '" + heartbeatUValue + "', 'system', ?, 'system', ?)";
                    Log.log.debug("Inserting DB Heartbeat event to resolve_event");
                    Log.log.trace("SQL to insert DB Heartbeat event: " + sql);
                    Log.log.trace("Generated sys_id: " + sysId);
                    PreparedStatement ps = conn.prepareStatement(sql);
                    Timestamp createdDate = new Timestamp(GMTDate.getTime());
                    
                    ps.setString(1, sysId);
                    ps.setTimestamp(2, createdDate);
                    ps.setTimestamp(3, createdDate);
                    
                    int insert = ps.executeUpdate();
                    Log.log.trace("" + insert + " rows inserted");
                    result = true;
                    
                    ps.close();
                }
            }
            else
            {
                Log.log.warn("No DB Pool Available, Unable to run DB Status");
            }
        }
        catch (Throwable t)
        {
            Log.alert(ERR.E20005, t);
            result = false;
        }
        finally
        {
            if (conn != null)
            {
                SQL.close(conn);
            }
        }
        return result;
    } //startUpCheck
    
    public boolean isBusy()
    {
        return busy;
    } //isBusy
    
    public void checkDBTPS()
    {
        String sql = null;
        String columnName = null;
        double currentTps = 0;
        if (dbType.equals("oracle"))
        {
            columnName = "VALUE";
            sql = "select VALUE from SYS.V_$SYSMETRIC where METRIC_ID = 2003 and GROUP_ID = 2";
        }
        else if (dbType.equals("mysql"))
        {
            columnName = "avg_count";
            sql = "SELECT avg_count FROM INFORMATION_SCHEMA.INNODB_METRICS WHERE NAME='trx_rw_commits'";
        }
            //v$sysstat
        SQLConnection conn = null;
        try
        {
            conn = SQL.getConnection();
            
            Log.log.trace("DB Self Check update sql: " + sql);
            
            PreparedStatement ps = conn.prepareStatement(sql);
            
            // Log.start("Begin Update Execute", "debug");
            ResultSet rs = ps.executeQuery();
            rs.next();
            currentTps = rs.getDouble(columnName);
            
            Log.log.trace("Current DB TPS: " + currentTps);
            // Log.duration("Finish Update Execute", "debug");
            
            if (dbType.equals("mysql"))
            {
            	// Disable the counter
            	conn.executeQuery("set GLOBAL innodb_monitor_disable = trx_rw_commits");
            	// Reset the counter.
            	conn.executeQuery("set GLOBAL innodb_monitor_reset_all = trx_rw_commits");
            	// Enable the counter
            	conn.executeQuery("set GLOBAL innodb_monitor_enable = trx_rw_commits");
            }
            
        }
        catch (Throwable t)
        {
            Log.alert(ERR.E20005, t);
        }
        finally
        {
            if (conn != null)
            {
                SQL.close(conn);
            }
        }
        
        if (currentTps > thresholdTps)
        {
            Log.alert(ERR.E20009, "Current TPS, " + currentTps + ", exeeded then the set threshold of " + thresholdTps);
        }
    }

	public int getThresholdTps()
	{
		return thresholdTps;
	}

	public void setThresholdTps(int thresholdTps)
	{
		this.thresholdTps = thresholdTps;
	}

	public String getDbType()
	{
		return dbType;
	}

	public void setDbType(String dbType)
	{
		this.dbType = dbType;
	}
}
