/******************************************************************************
* (C) Copyright 2014
*
* Resolve Systems, LLC
*
* All rights reserved
* This software is distributed under license and may not be used, copied,
* modified, or distributed without the express written permission of
* Resolve Systems, LLC.
*
******************************************************************************/

package com.resolve.rsmgmt;

import java.io.File;
import java.util.Map;

import com.resolve.rsmgmt.impex.DataImpex;
import com.resolve.rsmgmt.impex.DataImpexFactory;
import com.resolve.rsmgmt.impex.DataParser;
import com.resolve.rsmgmt.impex.DataParserFactory;
import com.resolve.rsmgmt.impex.DataType;
import com.resolve.sql.SQL;
import com.resolve.sql.SQLConnection;
import com.resolve.util.FileUtils;
import com.resolve.util.Log;
import com.resolve.util.StringUtils;

/**
 * This class is used for receiving message to import data to custom table from a CSV file or export it.
 */
public class MImpex
{
    public static void impexCustomTable(Map<String, Object> params)
    {
        //boolean force = StringUtils.getBoolean(Constants.ESB_PARAM_FORCE, params);
        String operation = StringUtils.getString("OP", params);
        String userId = StringUtils.getString("USERID", params);
        String tableName = StringUtils.getString("TABLE", params);
        String dataType = StringUtils.getString("DATATYPE", params);
        String file = StringUtils.getString("FILE", params);
        String data = StringUtils.getString("DATA", params);
        String delim = StringUtils.getString("DELIMETER", params);
        String override = StringUtils.getString("OVERRIDE", params);
        String stopOnFailure = StringUtils.getString("STOP_ON_FAILURE", params);
        Log.log.debug("Incoming parameter:");
        Log.log.debug("\t Operation=" + operation);
        Log.log.debug("\t User Id=" + userId);
        Log.log.debug("\t Table Name=" + tableName);
        Log.log.debug("\t Data Type=" + dataType);
        Log.log.debug("\t File=" + file);
        Log.log.debug("\t Data=" + data);
        Log.log.debug("\t Delimeter=" + delim);
        Log.log.debug("\t Override=" + override);
        Log.log.debug("\t Stop on Failure=" + stopOnFailure);
        
        //System.out.println(operation + ":::" + userId + ":::" + data + "::::" + tableName + ":::" + dataType + ":::" + file);
        
        char delimeter = ',';
        Boolean isOverride = Boolean.TRUE;
        Boolean isStopOnFailure = Boolean.FALSE;
        
        //by default we expect it to be CSV
        DataType dataType2 = DataType.CSV;
        if(StringUtils.isNotBlank(dataType) && !DataType.CSV.getTagName().equalsIgnoreCase(dataType))
        {
            dataType2 = DataType.valueOf(dataType.toUpperCase());
        }
        
        SQLConnection sqlConnection = null;
        
        try
        {
            sqlConnection = SQL.getConnection();

            if(StringUtils.isNotBlank(delim))
            {
                if(delim.length() == 1)
                {
                    delimeter = delim.charAt(0);
                }
                else
                {
                    throw new Exception("Invalid delimeter: " + delim);
                }
            }

            if(StringUtils.isNotBlank(override))
            {
                if(override.equalsIgnoreCase("true") || override.equalsIgnoreCase("false"))
                {
                    isOverride = Boolean.valueOf(override);
                }
                else
                {
                    throw new Exception("Invalid value for \"override\" parameter: " + override);
                }
            }

            if(StringUtils.isNotBlank(stopOnFailure))
            {
                if(stopOnFailure.equalsIgnoreCase("true") || stopOnFailure.equalsIgnoreCase("false"))
                {
                    isStopOnFailure = Boolean.valueOf(stopOnFailure);
                }
                else
                {
                    throw new Exception("Invalid value for \"stoponfailure\" parameter: " + stopOnFailure);
                }
            }

            DataParser dataParser = DataParserFactory.getDataParser(dataType2);
            DataImpex dataImpex = DataImpexFactory.getDataImport(Main.configSQL.getDbtype(), sqlConnection, dataParser);

            if("IMPORT".equalsIgnoreCase(operation))
            {
                if(data != null && !data.isEmpty()) {
                    dataImpex.importData(userId, tableName, data, delimeter, isOverride, isStopOnFailure);
                }
                if(file != null && !file.isEmpty()) {
                    String dataFileCSV = FileUtils.readFileToString(new File(file), "UTF8");
                    dataImpex.importData(userId, tableName, dataFileCSV, delimeter, isOverride, isStopOnFailure);
                }
            }
            else if("EXPORT".equalsIgnoreCase(operation))
            {
                if(DataType.XLS.getTagName().equalsIgnoreCase(dataType))
                {
                    dataImpex.exportToXLS(userId, file, tableName);
                }
                else
                {
                    data = dataImpex.exportData(userId, tableName, delimeter);
                    FileUtils.writeStringToFile(FileUtils.getFile(file), data, "UTF8");
                }
            }
            else
            {
                throw new Exception("Invalid operation: " + operation);
            }
        }
        catch (Exception e)
        {
            Log.log.error(e.getMessage(), e);
        }
        finally {
            if(sqlConnection != null)
            {
                sqlConnection.close();
            }
        }
    } // importCustomTable
}
