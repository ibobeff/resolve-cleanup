/******************************************************************************
* (C) Copyright 2014
*
* Resolve Systems, LLC
*
* All rights reserved
* This software is distributed under license and may not be used, copied,
* modified, or distributed without the express written permission of
* Resolve Systems, LLC.
*
******************************************************************************/

package com.resolve.rsmgmt;

import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import com.resolve.util.CryptUtils;
import com.resolve.util.Log;
import com.resolve.util.Properties;
import com.resolve.util.StringUtils;

public class ConfigRSArchive
{
    private static final String instanceName = "rsarchive";
    
	private Properties rsarchiveProperties;
    private Properties runProperties;
    private Properties envProperties;
    private Properties logProperties;
    private Properties esapiProperties;
    private String hibernateDBType = "MYSQL";
    private Properties hibernateProperties;
    private List<String> implPrefixes;

    public ConfigRSArchive()
    {
        this.rsarchiveProperties = new Properties();
        this.runProperties = new Properties();
        this.envProperties = new Properties();
        this.logProperties = new Properties();
        this.esapiProperties = new Properties();
        this.hibernateProperties = new Properties();
    } // ConfigRSArchive
    
    public ConfigRSArchive(Properties properties)
    {
        this();
	    for (Object key : properties.keySet())
        {
            String strKey = key.toString();
            if (strKey.startsWith(instanceName + "."))
            {
                String value = properties.getProperty(strKey).trim();
                if (value.contains("${"))
                {
                    value = properties.getReplacement(key).trim();
                }
                if (value.toUpperCase().startsWith("TO_ENC:"))
                {
                    if (StringUtils.isNotBlank(value.substring(7)) && !value.substring(7).toUpperCase().startsWith("NO_ENC:") && !value.substring(7).matches(".*(?<!\\\\)\\$\\{.*\\}.*"))
                    {
                        try
                        {
                            value = CryptUtils.encrypt(value.substring(7));
                        }
                        catch (Exception e)
                        {
                            Log.log.error("Failed to encrypt blueprint value prefixed with TO_ENC: set for the key " + strKey + 
                                          " due to error " + e.getMessage() + ", setting un-encrypted value into config.xml", e);
                        }
                    }
                }
                if (value.toUpperCase().startsWith("NO_ENC:"))
                {
                    value = value.substring(7);
                }
                if (value.startsWith("ENC"))
                {
                    try
                    {
	                    value = CryptUtils.decrypt(value);
                    }
                    catch (Exception e)
                    {
                        Log.log.error("Failed to decrypt blueprint value", e);
                        value = "";
                    }
                }
                if (strKey.contains("brokeraddr") && value.matches(":\\d*"))
                {
                    value = "";
                }
                if (value != null)
                {
	                if (strKey.startsWith(instanceName + ".run"))
	                {
	                    this.runProperties.put(key, value);
	                }
                    else if (strKey.startsWith(instanceName + ".hibernate"))
                    {
                        if (strKey.equalsIgnoreCase(instanceName + ".hibernate.dbtype"))
                        {
                            hibernateDBType = value.toUpperCase();
                        }
                        else
                        {
                            hibernateProperties.put(key, value);
                        }
                    }
	                else if (strKey.startsWith(instanceName + ".log4j"))
	                {
	                    this.logProperties.put(key, value);
	                }
	                else if (strKey.startsWith(instanceName + ".env"))
                    {
                        this.envProperties.put(key, value);
                    }
	                else if (strKey.startsWith(instanceName + ".ESAPI"))
                    {
                        this.esapiProperties.put(key, value);
                    }
	                else
	                {
	                    this.rsarchiveProperties.put(key, value);
	                }
                }
            }
        }
    } // ConfigRSArchive
    
    public Map<String, String> rsarchiveEsapiReplaceValues()
    {
        Map<String, String> result = new HashMap<String, String>();
        
        for (Object key : esapiProperties.keySet())
        {            
            String strKey = key.toString();
            String value = esapiProperties.getProperty(strKey);
            
            strKey = strKey.replaceFirst(instanceName + ".ESAPI.", "");

            // don't replace \\ with / if it's a validator property. It messes with the regex.
            if(strKey.startsWith("Validator"))
            {
                value = value.replace("\\", "\\\\");
            }
            else
            {
                value = value.replace("\\\\", "/");
            }
            
            String replaceRegex = strKey + "=.*";
            String replaceString = strKey + "=" + value;
            
            result.put(replaceRegex, replaceString);
        }
        
        return result;
    } //rsviewEsapiReplaceValues
    
    @SuppressWarnings("unchecked")
    public Map<String, Object> rsarchiveXPathValues()
    {
        Map<String, Object> result = new HashMap<String, Object>();
        
        for (Object key : rsarchiveProperties.keySet())
        {
            String strKey = key.toString();
            String value = (String) rsarchiveProperties.get(key);
            
            strKey = strKey.replaceFirst(instanceName, "").toUpperCase();
            strKey = strKey.replaceAll("(?<!\\\\)\\.", "/");
            strKey = strKey.replaceAll("\\\\\\.", ".");
            
            Pattern pattern = Pattern.compile("(.+)/(.+)/(\\d+)");
            Matcher matcher = pattern.matcher(strKey);
            if (matcher.matches())
            {
                strKey = "." + matcher.group(1);
                String xdocKey = matcher.group(2);
                int listPosition = Integer.parseInt(matcher.group(3))-1;

                List<Map<String, String>> list = (List<Map<String, String>>) result.get(strKey);
                if (list == null)
                {
                    list = new LinkedList<Map<String, String>>();
                    result.put(strKey, list);
                }
                for (int j=list.size(); j<=listPosition; j++)
                {
                    list.add(new HashMap<String, String>());
                }
                Map<String, String> entry = list.get(listPosition);
                entry.put(xdocKey, value);
                list.set(listPosition, entry);
            }
            else
            {
                strKey = strKey.replaceFirst("^(.*)/(.*?)$", ".$1/@$2");
                result.put(strKey, value);
            }
        }
                        
        
        return result;
    } //rsarchiveXPathValues()
    
    public Map<String, String> rsarchiveRunReplaceValues()
    {
        Map<String, String> result = new HashMap<String, String>();
        
        for (Object key : runProperties.keySet())
        {
            String strKey = key.toString();
            String value = runProperties.getProperty(strKey);
            
            strKey = strKey.replaceFirst(instanceName + ".run.", "");
            
            if (strKey.equalsIgnoreCase("Xms") || strKey.equalsIgnoreCase("mms"))
            {
                if (value.endsWith("M"))
                {
                    value = value.substring(0, value.length()-1);
                }
                result.put("Xms\\d+M", "Xms" + value + "M");
                result.put("MMS=.*", "MMS=" + value);
            }
            else if (strKey.equalsIgnoreCase("Xmx") || strKey.equalsIgnoreCase("mmx"))
            {
                if (value.endsWith("M"))
                {
                    value = value.substring(0, value.length()-1);
                }
                result.put("Xmx\\d+M", "Xmx" + value + "M");
                result.put("MMX=.*", "MMX=" + value);
            } 
            else
            {
                value = value.replaceAll("\\\\", "/");
                if (value.equalsIgnoreCase("true") || value.equalsIgnoreCase("false"))
                {
                    value = value.toUpperCase();
                }
                
                strKey = strKey.toUpperCase();
                String replaceRegex = strKey + "=.*";
                String replaceString = strKey + "=" + value;
    
                result.put(replaceRegex, replaceString);
            }
        }
        return result;
    } //rsarchiveRunReplaceValues
    
    
    public Map<String, String> rsarchiveEnvReplaceValues()
    {
        Map<String, String> result = new HashMap<String, String>();
        
        for (Object key : envProperties.keySet())
        {
            String strKey = key.toString();
            String value = envProperties.getProperty(strKey);
            
            strKey = strKey.replaceFirst(instanceName + ".env.", "");
            value = value.replaceAll("\\\\", "/");
            if (value.equalsIgnoreCase("true") || value.equalsIgnoreCase("false"))
            {
                value = value.toUpperCase();
            }
            
            strKey = strKey.toUpperCase();
            String replaceRegex = strKey + "=.*";
            String replaceString = strKey + "=" + value;

            result.put(replaceRegex, replaceString);
        }
        return result;
    }
    
    public Map<String, String> rsarchiveLogReplaceValues()
    {
        Map<String, String> result = new HashMap<String, String>();
        
        for (Object key : logProperties.keySet())
        {
            String strKey = key.toString();
            String value = logProperties.getProperty(strKey);
            
            strKey = strKey.replaceFirst(instanceName + ".", "");
            value = value.replaceAll("\\\\", "/");
	            
            String replaceRegex = strKey + "=.*";
            String replaceString = strKey + "=" + value;

            result.put(replaceRegex, replaceString);
        }
        
        return result;
    } //rsarchiveLogReplaceValues
    
    public Properties getrsarchiveProperties()
    {
        return rsarchiveProperties;
    } // getrsarchiveProperties

    public void setrsarchiveProperties(Properties properties)
    {
        this.rsarchiveProperties = properties;
    } //setrsarchiveProperties

    public Properties getRunProperties()
    {
        return runProperties;
    } //getRunProperties

    public void setRunProperties(Properties runProperties)
    {
        this.runProperties = runProperties;
    } //setRunProperties

    public void setEsapiProperties(Properties esapiProperties)
    {
        this.esapiProperties = esapiProperties;
    } //setEsapiProperties
    
    public Properties getEsapiProperties()
    {
        return this.esapiProperties;
    } //getEsapiProperties
    
    public String getHibernateDBType()
    {
        return hibernateDBType;
    } // getHibernateDBType

    public List<String> getImplPrefixes()
    {
        return implPrefixes;
    } // getImplPrfixes
    
    public Map<String, String> hibernateReplaceValues()
    {
        Map<String, String> result = new HashMap<String, String>();
        
        for (Object key : hibernateProperties.keySet())
        {
            String strKey = key.toString();
            String value = hibernateProperties.getProperty(strKey);
            
            strKey = strKey.replaceFirst(instanceName + ".hibernate.", "");
            
            result.put(strKey, value);
        }
        return result;
    } //hibernateReplaceValues();
    
}//ConfigRSArchive
