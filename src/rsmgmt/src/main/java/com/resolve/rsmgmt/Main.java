/******************************************************************************
* (C) Copyright 2014
*
* Resolve Systems, LLC
*
* All rights reserved
* This software is distributed under license and may not be used, copied,
* modified, or distributed without the express written permission of
* Resolve Systems, LLC.
*
******************************************************************************/

package com.resolve.rsmgmt;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.concurrent.TimeUnit;
import java.util.regex.Matcher;

import org.apache.http.HttpHeaders;
import org.apache.http.HttpResponse;
import org.apache.http.HttpStatus;
import org.apache.http.NameValuePair;
import org.apache.http.client.HttpClient;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.message.BasicNameValuePair;
import org.apache.http.util.EntityUtils;
import org.codehaus.jackson.map.ObjectMapper;

import com.resolve.esb.ESBException;
import com.resolve.esb.MListener;
import com.resolve.execute.ExecuteMain;
import com.resolve.groovy.script.GroovyScript;
import com.resolve.rest.server.RestServer;
import com.resolve.rsbase.ConfigCAS;
import com.resolve.rsbase.MainBase;
import com.resolve.rsmgmt.metric.MMetric;
import com.resolve.rsmgmt.metric.TrendMetric;
import com.resolve.search.ConfigSearch;
import com.resolve.search.SearchAdminAPI;
import com.resolve.service.LicenseService;
import com.resolve.services.util.CSRFUtil;
import com.resolve.services.vo.ResponseDTO;
import com.resolve.sql.SQL;
import com.resolve.sql.SQLConnection;
import com.resolve.sql.SQLDriverInterface;
import com.resolve.thread.ScheduledExecutor;
import com.resolve.util.Constants;
import com.resolve.util.CryptUtils;
import com.resolve.util.ERR;
import com.resolve.util.FileUtils;
import com.resolve.util.Log;
import com.resolve.util.MessageDispatcher;
import com.resolve.util.Properties;
import com.resolve.util.StringUtils;
import com.resolve.util.Tokenizer;
import com.resolve.util.XDoc;

import groovy.lang.Binding;
import ml.options.Options;
import ml.options.Options.Multiplicity;
import ml.options.Options.Prefix;
import ml.options.Options.Separator;

public class Main extends com.resolve.rsbase.MainBase
{
    final static int CHECKOUT_INTERVAL = 1;
    final static int UPDATE_INTERVAL = 6;

    public static ConfigSQL configSQL;
    private ConfigSearch configSearch;

    ConfigRegistration configRegistration;
    ConfigENC configENC;
    ConfigMetric configMetric;
    ConfigMonitor configMonitor;
    // ConfigZK configZK;
    ConfigAlert configAlert;
    ConfigCAS configCAS;
    ConfigRestWebservice configRest;
    public static ConfigMCP configMCP;
    public ConfigLicense configLicense;
    private ConfigLogstash configLogstash;

    public String blueprintFileName = "rsmgmt/config/blueprint.properties";
    public Properties blueprintProperties = new Properties();
    public String dist;
    public boolean saveBlueprint = false;
    public boolean blueprintRSRemote = false;
    public boolean blueprintRSControl = false;
    public boolean blueprintRSMQ = false;
    public boolean blueprintRSView = false;
    public boolean blueprintRSConsole = false;
    public boolean blueprintRSMgmt = false;
    public boolean blueprintRSSearch = false;
    public boolean blueprintRSSync = false;
    public boolean blueprintRSArchive = false;
    private boolean blueprintLogstash = false;
    private boolean blueprintRSLog = false;
    private boolean blueprintDCS = false;
	private boolean blueprintDCSRSDataLoader = false;
	private boolean blueprintDCSRSReporting = false;
    private ConfigDCS configDCS;
    private ConfigRSDataLoader configRSDataLoader;
    private ConfigRSReporting configRSReporting;
    public Map<String, ConfigRSRemote> configRSRemotes;
    public ConfigRSControl configRSControl;
    public ConfigRSMQ configRSMQ;
    public ConfigRSView configRSView;
    public ConfigRSConsole configRSConsole;
    public ConfigRSMgmt configRSMgmt;
    public ConfigRSSync configRSSync;
    public ConfigRSArchive configRSArchive;
    public ConfigRSSearch configRSSearch;
    public ConfigRSLog configRSLog;
    public boolean dbConnected = false;
    public boolean resetGUIDs = false;
    public Set<String> implPrefixes;
    private Map<String, String> implPackages;
    public Map<String, Map<String, String>> sdkFieldsMap;
    public boolean blueprintSDKGateway = false;
    private ConfigMCPRegistration configMCPRegistration;
    public static boolean thresholdEnabled = true;
    public static boolean REMOTE_RSMGMT = false;
    public Map<String, String> intervalMap;   
    public Map<String, String> authTypeMap;
    public String httpGatewayConfigAuthMode;
    
    String scriptFilename;
    File scriptFile;
    String[] scriptArgs;

    ArrayList<MListener> namedQueueListeners = new ArrayList<MListener>();

    TrendMetric trendMetric;
    Alert alert;
    RestServer restServer;
    
    // Set of Active Gateway Queue Names in all RSRemote instance(s)
    private Set<String> activeGatewayQNames;
    private Map<String, Boolean> gtywQSelfCheckStarted;
    private Map<String, Boolean> gtywTSelfCheckStarted;
    private Map<String, Boolean> gtywWSelfCheckStarted;
    
    /*
     * Main execution method for RSMgmt
     */
    public static void main(String[] args)
    {
        String serviceName = null;

        try
        {
            if (args.length > 0)
            {
                serviceName = args[0];
            }

            main = new Main();
            if (args.length > 1)
            {
                ((Main) main).executeScript(serviceName, args);
                System.exit(0);
            }
            else
            {
                main.init(serviceName);
            }
        }
        catch (Throwable e)
        {
            e.printStackTrace();
            Log.alert(ERR.E10004);

            System.exit(1);
        }

    } // main

    public void executeScript(String serviceName, String[] args) throws Exception
    {
        // init release information
        initRelease(new Release(serviceName));

        // init logging
        initLog();

        // init enc
        initENC();

        // init configuration
        initConfig();

        // init timezone
        initTimezone();

        // init options
        initOptions(args);

        // init blueprint
        initBlueprint();

        // init SQL
        // if (!scriptFile.getName().contains("resolve-configure"))
        // {
        initSQL();
        // }

        Log.log.warn("Attempting to Execute Script: " + scriptFile.getAbsolutePath());
        Main.println("Attempting to Execute Script " + scriptFile.getName());
        Object exitCode = null;
        if (scriptFile.exists())
        {
            exitCode = executeScript(scriptFile, scriptArgs);
            if (scriptFile.getName().contains("resolve-configure"))
            {
                //storeBlueprint();
            }
        }
        else
        {
            System.out.println("Unable to find script: " + scriptFilename + " or " + scriptFilename + ".groovy");
            Log.log.error("Unable to find script: " + scriptFilename + " or " + scriptFilename + ".groovy");
        }

        if (saveBlueprint)
        {
            saveBlueprint();
        }
        if(exitCode == null) {
            exitCode = 0;
        } else {
            try {
                exitCode = Integer.parseInt(exitCode.toString().trim());
            } catch (Exception e) {
                exitCode = 0;
            }
        }
        Log.log.info("exit code for " + scriptFilename +".groovy " + exitCode);
        System.exit((Integer)exitCode);

    } // init
    
    

    public void init(String serviceName) throws Exception
    {
        // init release information
        initRelease(new Release(serviceName));

        // init logging
        initLog();

        // init signal handlers
        initShutdownHandler();

        // init enc
        initENC();

        // init configuration
        initConfig();

        // init blueprint
        initBlueprint();

        // init startup
        initStartup();

        // init executor
        initThreadPools();

        // init timezone
        initTimezone();

        // init SQL
        initSQL();

        // init ESB
        initESB();

        // initialize elasticsearch
        initElasticSearch();
        
        // start config
        startConfig();

        // start message bus only after initialization is finished.
        startESB();

        // initialize search
        //initRSSearch();

        // start receivers
        startComponents();

        // save configuration
        initSaveConfig();

        // display started message and set MListener to active
        initComplete();
        
        initLicenseService();
        
        // initMetric
        initMetric();

        // startup tasks
        startDefaultTasks();
        
        startSelfCheckTasks();
        
        // register component with the MCP
        if (!REMOTE_RSMGMT)
        {
            registerWithMCP();
        }

    } // init
    
	private void registerWithMCP() {
		if (!thresholdEnabled) {
			return;
		}

        if(Boolean.valueOf(configMCPRegistration.getMcpRegistrationEnabled())){
            String mcpURI = configMCPRegistration.getMcpURI();
            String mcpPort = configMCPRegistration.getMcpPort();
            String mcpProtocol = configMCPRegistration.getMcpProtocol();
            String mcpUsername = configMCPRegistration.getMcpUsername();
            String mcpPassword = configMCPRegistration.getPassword();

            String protocol = mcpProtocol.isEmpty() ? "http://" : mcpProtocol+"://"; 
            String port = mcpPort.isEmpty() ? "" : ":" + mcpPort;

            String serviceUrl = new StringBuilder(protocol).append(mcpURI).append(port).toString();
            
			try {
				HttpClient httpClient = null;
				if ("http://".equalsIgnoreCase(protocol)) {
					httpClient = CSRFUtil.initSession(serviceUrl);
				} else if ("https://".equalsIgnoreCase(protocol)) {
					httpClient = CSRFUtil.initSslSession(serviceUrl);
				}

				if (httpClient != null) {
					String initialToken = CSRFUtil.fetchInitialToken(httpClient, serviceUrl);
					CSRFUtil.login(httpClient, serviceUrl, mcpUsername, mcpPassword);
					
					String token = CSRFUtil.fetchToken(httpClient, serviceUrl, initialToken);
					registerMCP(httpClient, token, serviceUrl, mcpUsername, mcpPassword);
				}
			} catch (Exception e) {
				Log.log.error("RSMgmt MCP registration - FAILED:" + e.getMessage(), e);
				Log.alert("MCP_REG_FAIL", "RSMgmt MCP registration - FAILED:" + e.getMessage());
			}
		}
        
    }

	@SuppressWarnings("rawtypes")
	private void registerMCP(HttpClient httpclient, String token, String serviceUrl, String user, String password) throws Exception {
		HttpPost httppost = new HttpPost(new StringBuilder(serviceUrl).append("/resolve/service/mcp/register").toString());
        httppost.setHeader("X-Requested-With", "XMLHttpRequest");
        httppost.setHeader(HttpHeaders.REFERER, serviceUrl);
        
        String[] tokens = CSRFUtil.parseCSRFToken(token);
        httppost.setHeader(tokens[0], tokens[1]);

		ArrayList<NameValuePair> postParameters = new ArrayList<NameValuePair>();
		postParameters.add(new BasicNameValuePair("_username_", user));
		postParameters.add(new BasicNameValuePair("_password_", password));
		postParameters.add(new BasicNameValuePair("blueprint", MRegister.getBlueprintData()));

		UrlEncodedFormEntity entity = new UrlEncodedFormEntity(postParameters);
		entity.setChunked(true);
		httppost.setEntity(entity);

		HttpResponse response = httpclient.execute(httppost);

		if (response != null && response.getStatusLine() != null
				&& response.getStatusLine().getStatusCode() == HttpStatus.SC_OK) {
			if (response.getEntity() != null) {
				String jsonStr = EntityUtils.toString(response.getEntity());
				ResponseDTO result = new ObjectMapper().readValue(jsonStr, ResponseDTO.class);

				if (!result.isSuccess()) {
					Log.log.error("RSMgmt MCP registration - FAILED: " + result.getMessage());

				} else {
					Log.log.info("RSMgmt MCP registration - SUCCESS: " + response.toString());
				}
			} else {
				Log.log.info("RSMgmt MCP registration - SUCCESS: " + response.toString());
			}
		} else {
			if (response != null && response.getStatusLine() != null) {
				Log.log.error("RSMgmt MCP registration - FAILED: Returned Response HTTP Status Code and Message "
						+ response.getStatusLine().getStatusCode() + ", " + response.getStatusLine().getReasonPhrase());
			} else {
				Log.log.error("RSMgmt MCP registration - FAILED: Returned No Response.");
			}
		}
	}
	
    /**
     * Initialize search (ElasticSearch) service
     */
    protected void initElasticSearch()
    {
        Log.log.info("Initializing Search.");
        if (configSearch.isActive())
        {
        	SearchAdminAPI.init(configSearch);
            Log.log.info("Search Initialized Successfully.");
        }
        else
        {
            REMOTE_RSMGMT = true;
            Log.log.info("Elastic Search is not initialized.");
        }
    }
    
    void initOptions(String[] args)
    {
        Options opt = new Options(args, Prefix.DASH);
        opt.getSet().addOption("h");
        opt.getSet().addOption("-help");
        opt.getSet().addOption("help");
        opt.getSet().addOption("f", Separator.BLANK, Multiplicity.ZERO_OR_ONE);
        opt.getSet().addOption("F", Separator.NONE, Multiplicity.ZERO_OR_MORE);
        opt.getSet().addOption("b", Separator.BLANK, Multiplicity.ZERO_OR_ONE);
        opt.getSet().addOption("B", Separator.NONE, Multiplicity.ZERO_OR_MORE);
        opt.getSet().addOption("s");
        opt.getSet().addOption("r");
        opt.getSet().addOption("u");
        opt.check();

        Options optSlash = new Options(args, Prefix.SLASH);
        optSlash.getSet().addOption("\\?");
        optSlash.check();

        if (opt.getSet().isSet("h") || optSlash.getSet().isSet("\\?"))
        {
            // Print usage
            printTitle();
            System.out.println("Usage: run.sh -f [FILE] [OPTION]...");
            System.out.println("-f <filename>        script file to execute (script.groovy)");
            System.out.println("-FD<arguments>;...   arguments to provide to script file specified by -f");
            System.out.println("-b <blueprint>       read in specified blueprint file for configuration");
            System.out.println("-BD<key>=<value>;... manually specify blueprint properties");
            System.out.println("-s                   Save blueprint on exit");
            System.out.println();
            System.exit(1);
        }
        if (opt.getSet().isSet("f"))
        {
            String filename = opt.getSet().getOption("f").getResultValue(0);

            // prefix with rsmgmt/service
            if (filename != null && filename.charAt(0) != '/')
            {
                filename = "rsmgmt/service/" + filename;
            }

            // check if file exists
            File file = FileUtils.getFile(filename);
            if (!file.exists())
            {
                // try .groovy extension
                file = FileUtils.getFile(filename + ".groovy");
            }

            scriptFilename = filename;
            scriptFile = file;

            // add script arguments
            scriptArgs = null;
        }
        else
        {
            Log.log.warn("Script File must be passed for Execute Mode");
            // Print usage
            printTitle();
            System.out.println("Usage: run.sh -f [FILE] [OPTION]...");
            System.out.println("-f <filename>        script file to execute (script.groovy)");
            System.out.println("-FD<arguments>;...   arguments to provide to script file specified by -f");
            System.out.println("-b <blueprint>       read in specified blueprint file for configuration");
            System.out.println("-BD<key>=<value>;... manually specify blueprint properties");
            System.out.println("-s                   Save blueprint on exit");
            System.out.println();
            System.exit(1);
        }
        if (opt.getSet().isSet("-help") || opt.getSet().isSet("help"))
        {
            scriptArgs = new String[] { "HELP" };
        }
        else if (opt.getSet().isSet("F"))
        {
            if (scriptFile != null)
            {
                String strArgs = opt.getSet().getOption("F").getResultValue(0);
                strArgs = scriptFile.getName() + ";" + strArgs;
                scriptArgs = StringUtils.stringToList(strArgs, ";").toArray(new String[0]);
            }
        }
        if (opt.getSet().isSet("b"))
        {
            String filename = opt.getSet().getOption("b").getResultValue(0);
            // prefix with rsmgmt/config
            if (filename != null && filename.charAt(0) != '/')
            {
                filename = "rsmgmt/config/" + filename;
            }
            // check if file exists
            File file = FileUtils.getFile(filename);
            if (file.exists())
            {
                blueprintFileName = filename;
            }
            else
            {
                System.out.println("Cannot locate blueprint file at: " + file.getAbsolutePath());
                System.exit(1);
            }
        }
        if (opt.getSet().isSet("B"))
        {
            for (int i = 0; i < opt.getSet().getOption("B").getResultCount(); i++)
            {
                String strProperties = opt.getSet().getOption("B").getResultValue(i);
                Map properties = StringUtils.stringToMap(strProperties, "=", ";");
                blueprintProperties.putAll(properties);
            }
        }
        if (opt.getSet().isSet("s"))
        {
            saveBlueprint = true;
        }
        if (opt.getSet().isSet("r"))
        {
            resetGUIDs = true;
        }
        if (opt.getSet().isSet("u"))
        {
            Log.log.setAdditivity(true);
        }
    } // initOptions

    protected void stopReceivers()
    {
        if (configMetric.isActive())
        {
            if (thresholdEnabled)
            {
                trendMetric.stop();
            }
        }
        if (configAlert.isActive())
        {
            alert.stop();
        }
    } // stopReceivers

    protected void initENC() throws Exception
    {
    	loadENCConfig();
        super.initENC();
    } // initENC

    protected void loadENCConfig() throws Exception
    {
        ConfigENC.load();
    }

    /*
     * NOTE: String args[] is required to catch NT service shutdown message
     */
    protected void exit(String[] argv)
    {
        Log.log.info("Terminate initiated");
        System.exit(0);
    } // exit

    protected void terminate()
    {
        Log.log.warn("Terminating " + release.name.toUpperCase());
        try
        {
            // save config
            exitSaveConfig();
            stopReceivers();

            super.terminate();
        }
        catch (Exception e)
        {
            Log.log.error("Failed to terminate RSMgmt: " + e.getMessage(), e);
        }
        Log.log.warn("Terminated " + release.name.toUpperCase());
    } // terminate

    public void initSaveConfig() {
        try
        {
            this.configMCPRegistration.save();
            super.initSaveConfig();
        }
        catch (Exception e)
        {
            Log.log.error("Unable to save targets.xml file: " + e.getMessage(), e);
        }

    }
    
    public void exitSaveConfig()
    {
        try
        {
            this.configMCPRegistration.save();
        }
        catch (Exception e)
        {
            Log.log.error("Unable to save targets.xml file: " + e.getMessage(), e);
        }

        super.exitSaveConfig();
    } // exitSaveConfig

    public void saveBlueprint()
    {
        List<String> excludeBlueprintList = new ArrayList<String>();

        excludeBlueprintList.add("rsconsole.esb.brokerport2");
        excludeBlueprintList.add("rsconsole.esb.brokerport");
        excludeBlueprintList.add("rscontrol.hazelcast.interface.1");
        excludeBlueprintList.add("rscontrol.hazelcast.interface.2");
        excludeBlueprintList.add("rsview.hazelcast.interface.1");
        excludeBlueprintList.add("rsview.hazelcast.interface.2");
        excludeBlueprintList.add("rscontrol.jgroups.stack.config.TCPPING.initial_hosts");
        excludeBlueprintList.add("rsview.jgroups.stack.config.TCPPING.initial_hosts");
        excludeBlueprintList.add("rsmq.primary");
        excludeBlueprintList.add("resolve.import");

        try
        {
            File blueprintFile = FileUtils.getFile(blueprintFileName);
            if (blueprintFile.isFile())
            {
                byte[] blueprintFileBytes = new byte[(int) blueprintFile.length()];
                FileInputStream fis = new FileInputStream(blueprintFile);
                fis.read(blueprintFileBytes);
                fis.close();

                String blueprintString = new String(blueprintFileBytes);

                for (Object key : blueprintProperties.keySet())
                {
                    String keyStr = (String) key;
                    if (excludeBlueprintList.contains(keyStr))
                    {
                        Log.log.debug("Skipping Exclude Property " + keyStr);
                        continue;
                    }
                    String value = blueprintProperties.get(keyStr);
                    if ((keyStr.toUpperCase().contains("PASS") || keyStr.toUpperCase().contains("PWD"))
                                    && !keyStr.toUpperCase().contains("LDAP.PASSWORD_ATTRIBUTE"))
                    {
                        if (StringUtils.isNotEmpty(value) && !value.startsWith("NO_ENC:") && !value.matches(".*(?<!\\\\)\\$\\{.*\\}.*"))
                        {
                            value = CryptUtils.encrypt(value);
                        }
                    }
					
					if (keyStr.toUpperCase().contains("RADIUS.SHARED_SECRET"))
                    {
                        if (StringUtils.isNotEmpty(value) && !value.startsWith("NO_ENC:") && !value.matches(".*(?<!\\\\)\\$\\{.*\\}.*"))
                        {
                            value = CryptUtils.encrypt(value);
                        }
                    }
					
                    if (value.startsWith("TO_ENC:"))
                    {
                        value = CryptUtils.encrypt(value.substring(7));
                    }
                    if (!value.contains("ENC"))
                    {
                        value = value.replaceAll("\\\\", "\\\\\\\\");
                    }
                    String regex = "(?m)^" + key + "=.*";
                    String replaceStr = key + "=" + value;
                    replaceStr = Matcher.quoteReplacement(replaceStr);
                    Log.log.trace("Saving " + key + " to " + value);
                    blueprintString = blueprintString.replaceFirst(regex, replaceStr);
                }

                blueprintFileBytes = blueprintString.getBytes();
                FileOutputStream fos = new FileOutputStream(blueprintFile);
                fos.write(blueprintFileBytes);
                fos.close();
                Log.log.warn("Blueprint Save Complete");
            }
            else
            {
                System.out.println("Blueprint File Cannot Be Found");
                Log.log.error("Blueprint File Cannot Be Found");
            }
        }
        catch (Exception e)
        {
            System.out.println("Unable to Save blueprint.properties file");
            Log.log.error("Unable to Save blueprint.properties file", e);
        }
    }

    protected void loadConfig(XDoc configDoc) throws Exception
    {
        // override and additional configX.load
        configGeneral = new ConfigGeneral(configDoc);
        configGeneral.load();
        setClusterModeProperty(configGeneral.getClusterName() + "." + Constants.CLUSTER_MODE);

        configLicense = new ConfigLicense(configDoc);
        configLicense.load();

        configRegistration = new ConfigRegistration(configDoc);
        configRegistration.load();

        configSQL = new ConfigSQL(configDoc);
        configSQL.load();

        configMetric = new ConfigMetric(configDoc);
        configMetric.load();

        configMonitor = new ConfigMonitor(configDoc);
        configMonitor.load();

        // configZK = new ConfigZK(configDoc);
        // configZK.load();

        configAlert = new ConfigAlert(configDoc);
        configAlert.load();

        configCAS = new ConfigCAS(configDoc);
        configCAS.load();

        configRest = new ConfigRestWebservice(configDoc);
        configRest.load();

        configMCP = new ConfigMCP(configDoc);
        configMCP.load();

        configSearch = new ConfigSearch(configDoc);
        configSearch.load();
        
        configMCPRegistration = new ConfigMCPRegistration(configDoc);
        configMCPRegistration.load();

        super.loadConfig(configDoc);
    } // loadConfig

    protected void saveConfig() throws Exception
    {
        // save config
        configAlert.save();
        configMetric.save();
        configMonitor.save();
        // configZK.save();
        configSQL.save();
        configESB.save();
        configRegistration.save();
        configCAS.save();
        configLicense.save();

        configSearch.save();
        
        super.saveConfig();
    } // saveConfig

    protected void loadESB(XDoc configDoc) throws Exception
    {
        // Using rsmgmt's own ConfigESB
        configESB = new ConfigESB(configDoc);
        configESB.load();
    }

    protected void initSQL()
    {
        if (configSQL.isActive())
        {
            try
            {
                Log.log.info("Starting SQL");

                // get db driver
                Log.log.debug("Database connection information:");
                Log.log.debug("       type: " + configSQL.getDbtype());
                Log.log.debug("       db name: " + configSQL.getDbname());
                Log.log.debug("       db host: " + configSQL.getHost());
                Log.log.debug("       username: " + configSQL.getUsername());
               // Log.log.debug("       pwd: " + configSQL.getP_assword());
                Log.log.debug("       url: " + configSQL.getUrl());
                SQLDriverInterface driver = SQL.getDriver(configSQL.getDbtype(), configSQL.getDbname(), configSQL.getHost(), configSQL.getUsername(), configSQL.getP_assword(), configSQL.getUrl(), configSQL.isDebug());

                if (driver != null)
                {
                    SQL.init(driver);
                    SQL.start();
                    SQLConnection conn = SQL.getConnection();
                    SQL.close(conn);
                    dbConnected = true;
                    //GenericJDBCHandler.setDBType(configSQL.getDbtype().toUpperCase());
                }
            }
            catch (Exception e)
            {
                Log.log.error("Failed to Create DB Pool", e);
            }
        }
        else
        {
            Log.log.info("No SQL supported");
            thresholdEnabled=false;
        }

        //new GenericJDBCHandler().refreshTableMap(null);
    } // initSQL

    void initBlueprint()
    {
        initBlueprint(false);
    }

    void initBlueprint(boolean reload)
    {
        if (!StringUtils.isEmpty(blueprintFileName))
        {
            ReadBlueprint.readFile(blueprintFileName, reload);
            if (blueprintProperties != null)
            {
                List<String> implPrefixesList = new ArrayList<String>();
                Map<String, String> implPackageList = new HashMap<String, String>();
                
                if (blueprintRSRemote || blueprintSDKGateway || blueprintRSView || 
                    blueprintRSControl || blueprintRSMgmt || blueprintRSSync)
                {
                    String rsremotes = blueprintProperties.get("rsremote.instance.count");
                    Log.log.info("rsremote instance count: " + rsremotes);
                    if (rsremotes != null && rsremotes.matches("\\d+"))
                    {
                        configRSRemotes = new HashMap<String, ConfigRSRemote>();
                        int count = Integer.parseInt(rsremotes);
                        for (int i = 1; i <= count; i++)
                        {
                            String instance = blueprintProperties.get("rsremote.instance" + i + ".name").toLowerCase();
                            Log.log.warn("rsremote.instance" + i + ".name=" + instance);
                            if (instance != null)
                            {
                                if (resetGUIDs)
                                {
                                    blueprintProperties.put(instance + ".id.guid", "");
                                }
                                ConfigRSRemote configRSRemote = new ConfigRSRemote(blueprintProperties, instance);
                                configRSRemotes.put(instance, configRSRemote);
                                
                                if (!configRSRemote.getImplPrefixes().isEmpty())
                                {
                                    if (implPrefixes == null)
                                    {
                                        implPrefixes = new HashSet<String>();
                                    }
                                    
                                    implPrefixes.addAll(configRSRemote.getImplPrefixes());
                                }
                                
                                if (!configRSRemote.getImplPackages().isEmpty())
                                {
                                    if (implPackages == null)
                                    {
                                        implPackages = new HashMap<String, String>();
                                    }
                                    
                                    implPackages.putAll(configRSRemote.getImplPackages());
                                }
                                
                                if (!configRSRemote.getFieldsMap().isEmpty())
                                {
                                    if (sdkFieldsMap == null)
                                    {
                                        sdkFieldsMap = new HashMap<String, Map<String, String>>();
                                    }
                                    sdkFieldsMap.putAll(configRSRemote.getFieldsMap());
                                }
                                
                                if (!configRSRemote.getIntervalMap().isEmpty())
                                {
                                    if (intervalMap == null)
                                    {
                                        intervalMap = new HashMap<String, String>();
                                    }
                                    intervalMap.putAll(configRSRemote.getIntervalMap());
                                }
                                
                                if (!configRSRemote.getAuthTypeMap().isEmpty())
                                {
                                    if (authTypeMap == null)
                                    {
                                        authTypeMap = new HashMap<String, String>();
                                    }
                                    authTypeMap.putAll(configRSRemote.getAuthTypeMap());
                                }
                               
                                
                                Log.log.info("Adding rsremote config: " + instance);
                            }
                        }
                    }
                    else
                    {
                        blueprintRSRemote = false;
                    }
                }
                
                /* RSControl and RSView with Hibernate configuration should succeed
                 * RSRemote configuration. Hibernate configuration requires info on sdk developed
                 * gateways to configure gateway filter mapping class in hibernaate.cfg.xml
                 * of RSControl and RSView.
                 */
                
                if (implPrefixes != null && !implPrefixes.isEmpty())
                {
                    for (String implPrefix : implPrefixes)
                    {
                        implPrefixesList.add(implPrefix);
                    }
                }
                
                if (implPackages != null && !implPackages.isEmpty())
                {
                    for (String implPrefix : implPackages.keySet())
                    {
                        implPackageList.put(implPrefix, implPackages.get(implPrefix));
                    }
                }
                
                if (blueprintRSControl)
                {
                    if (resetGUIDs)
                    {
                        blueprintProperties.put("rscontrol.id.guid", "");
                    }
                    configRSControl = new ConfigRSControl(blueprintProperties);
                    
                    if (!implPrefixesList.isEmpty())
                    {
                        configRSControl.setImplPrefixes(implPrefixesList);
                    }
                    
                    if (!implPackageList.isEmpty())
                    {
                        configRSControl.setImplPackages(implPackageList);
                    }
                }
                if (blueprintRSMQ)
                {
                    configRSMQ = new ConfigRSMQ(blueprintProperties);
                }
                if (blueprintRSView)
                {
                    if (resetGUIDs)
                    {
                        blueprintProperties.put("rsview.id.guid", "");
                    }
                    configRSView = new ConfigRSView(blueprintProperties);
                    
                    if (!implPrefixesList.isEmpty())
                    {
                        configRSView.setImplPrefixes(implPrefixesList);
                    }
                    
                    if (!implPackageList.isEmpty())
                    {
                        configRSView.setImplPackages(implPackageList);
                    }
                    
                    if (sdkFieldsMap != null)
                    {
                        configRSView.setSdkFieldsMap(sdkFieldsMap);
                    }
                    
                    if (intervalMap != null)
                    {
                        configRSView.setIntervalMap(intervalMap);
                    }
                    if (authTypeMap != null)
                    {
                        configRSView.setAuthTypeMap(authTypeMap);
                    }
                }
                if (blueprintRSConsole)
                {
                    if (resetGUIDs)
                    {
                        blueprintProperties.put("rsconsole.id.guid", "");
                    }
                    configRSConsole = new ConfigRSConsole(blueprintProperties);
                }
                if (blueprintRSMgmt)
                {
                    if (resetGUIDs)
                    {
                        blueprintProperties.put("rsmgmt.id.guid", "");
                    }
                    configRSMgmt = new ConfigRSMgmt(blueprintProperties);
                    
                    // Get Set of active gateway queue names in cluster
                    
                    if (configRSRemotes != null && !configRSRemotes.isEmpty())
                    {
                        for (ConfigRSRemote cfgRSRemote : configRSRemotes.values())
                        {
                            if (!cfgRSRemote.getActiveGatewayQNames().isEmpty())
                            {
                                if (activeGatewayQNames == null)
                                {
                                    activeGatewayQNames = new HashSet<String>();
                                }
                                
                                activeGatewayQNames.addAll(cfgRSRemote.getActiveGatewayQNames());
                            }
                        }
                        
                        if (activeGatewayQNames != null && !activeGatewayQNames.isEmpty())
                        {
                            if (gtywQSelfCheckStarted == null)
                            {
                                gtywQSelfCheckStarted = new HashMap<String, Boolean>();
                            }
                            
                            if (gtywTSelfCheckStarted == null)
                            {
                                gtywTSelfCheckStarted = new HashMap<String, Boolean>();
                            }
                            
                            if (gtywWSelfCheckStarted == null)
                            {
                                gtywWSelfCheckStarted = new HashMap<String, Boolean>();
                            }
                            
                            for (String gtwQueueName : activeGatewayQNames)
                            {
                                gtywQSelfCheckStarted.put(gtwQueueName, Boolean.FALSE);
                                gtywTSelfCheckStarted.put(gtwQueueName, Boolean.FALSE);
                                gtywWSelfCheckStarted.put(gtwQueueName, Boolean.FALSE);
                            }
                        }
                    }
                }
                if (blueprintRSSync)
                {
                    if (resetGUIDs)
                    {
                        blueprintProperties.put("rssync.id.guid", "");
                    }
                    configRSSync = new ConfigRSSync(blueprintProperties);
                }
                if (blueprintRSArchive)
                {
                    if (resetGUIDs)
                    {
                        blueprintProperties.put("rssync.id.guid", "");
                    }
                    configRSArchive = new ConfigRSArchive(blueprintProperties);
                }
                if (blueprintRSSearch)
                {
                    configRSSearch = new ConfigRSSearch(blueprintProperties);
                }
                if (blueprintLogstash)
                {
                    configLogstash = new ConfigLogstash(blueprintProperties);
                }
                if (this.blueprintDCS)
                {
                    this.configDCS = new ConfigDCS(blueprintProperties);
                }
                if (this.blueprintDCSRSDataLoader)
                {
                    this.configRSDataLoader = new ConfigRSDataLoader(blueprintProperties);
                }
                if (this.blueprintDCSRSReporting)
                {
                    this.configRSReporting = new ConfigRSReporting(blueprintProperties);
                }
                if (this.blueprintRSLog) {
                    this.configRSLog = new ConfigRSLog(blueprintProperties);
                }
            }
        }
    }

    @Override
    protected void initESB() throws Exception
    {
        super.initESB();
    } // initESB

    @Override
    protected void startESB() throws Exception
    {
        super.startESB();

        // add default subscription
        mServer.subscribePublication(Constants.ESB_NAME_BROADCAST);
        mServer.subscribePublication(Constants.ESB_NAME_RSMGMTS);

        Log.log.info("  Initializing ESB Listener for the queue: " + MainBase.main.configId.getGuid());
        MListener mListener = mServer.createListener(MainBase.main.configId.getGuid(), "com.resolve.rsmgmt");
        mListener.init(false);

        initNamedQueues();
    }

    /*
     * protected void initZookeeper() { if (configZK.isActive()) { try {
     * String[] args = { getProductHome()+"/config/zk.cfg" };
     * Executor.execute(org.apache.zookeeper.server.quorum.QuorumPeerMain.class,
     * "main", (String[])args); } catch (Throwable t) {
     * Log.alert("Graph Database", t.getMessage(), t); };
     *
     * } } // initZookeeper
     */

    protected void initNamedQueues() throws Exception
    {
        com.resolve.rsmgmt.ConfigESB rmtESB = (com.resolve.rsmgmt.ConfigESB) configESB;
        if (rmtESB.namedQueues.size() > 0)
        {
            for (ConfigESB.NamedQueue queue : rmtESB.namedQueues)
            {
                if (!StringUtils.isEmpty(queue.name))
                {
                    //MListener mListener = MListenerFactory.getMListener(MainBase.main.mServer, queue.name, "com.resolve.rsmgmt");
                    MListener mListener = mServer.createListener(queue.name, "com.resolve.rsmgmt");
                    mListener.init(false);
                    namedQueueListeners.add(mListener);
                    Log.log.info("Added a new listener for queue " + queue.name);
                }
            }
        }
    } // initNamedQueues

    @Override
    protected void initMetric() throws Exception
    {
        if (!REMOTE_RSMGMT)
        {
            metric = new Metric(configMetric.isActive(), this.dbConnected);
            
            //super.initMetric();
            // send JMS to rsmgmt with the GUID to refresh threshold values
            //BaseMetric.requestThresholdUpdate(300);
            
            if (thresholdEnabled)
            {
                MMetric.initThresholds();
            }
        }
    } // initMetric

    protected void startComponents() throws Exception
    {
        MessageDispatcher.init();

        // Metric Listener
        if (thresholdEnabled && !REMOTE_RSMGMT)
        {
            trendMetric = new TrendMetric(configMetric, configSQL.getDbtype());
            trendMetric.start();
        }

        // Alert Listener
        if (configAlert.isActive() && !REMOTE_RSMGMT)
        {
            alert = new Alert(configAlert);
            alert.start();
        }

        // RestWebservice
        /* This is disabled until MCP is revived
        if (configRest.isActive())
        {
            String tmpDir = Main.main.configGeneral.getHome() + "/tmp/";
            //Config config = new Config(configRest.isActive(), configRest.isSsl(), configRest.getHttpport(), configRest.getHttpsport(), configRest.getUsername(), configRest.getPassword(), tmpDir);
            Config config = new Config(blueprintProperties.get("LOCALHOST"), configRest.isActive(), configRest.getHttpport(), configRest.getHttpsport(), configRest.getUsername(), configRest.getPassword(), tmpDir, configRest.isSsl(), configRest.getKeystore(), configRest.getKeystorepassword(), configRest.getKeypassword());
            restServer = new RestServer();
            restServer.init(config);
            restServer.start();
        }
        */
    } // startComponents

    void printTitle()
    {
        System.out.println();
        System.out.println("RSMGMT [Version " + release.version + "]");
        System.out.println(release.copyright);
        System.out.println();
    } // printTitle

    public static void print(String out)
    {
        System.out.print(out);
    } // print

    public static void println()
    {
        System.out.println("");
    } // println

    public static void println(String out)
    {
        System.out.println(out);
    } // println

    Object executeCommand(String command) throws Exception
    {
        Object result = null;
        if (!StringUtils.isEmpty(command))
        {
            Log.log.debug("command: " + command);

            // parse input
            Tokenizer tokenizer = new Tokenizer(command, ' ', '"');
            String[] tokens = tokenizer.getAllTokens();

            if (tokens.length > 0)
            {
                File scriptFile = findScriptFile(tokens[0]);
                if (scriptFile.exists())
                {
                    result = executeScript(scriptFile, tokens);
                }
                else
                {
                    Log.log.error("Unknown command or script: " + tokens[0]);
                    Main.println("Unknown command or script");
                    Main.println();
                }
            }
            else
            {
                Log.log.error("No Command Passed");
                Main.println("No Command Passed");
                Main.println();
            }
        }
        return result;
    } // executeCommand

    File findScriptFile(String script) throws IOException
    {
        // check absolute command filename
        String pathname = getGSEHome();

        // update pathname and set command
        String fullpathname = pathname + "/" + script;
        File dir = FileUtils.getFile(fullpathname);
        int pos = dir.getCanonicalPath().lastIndexOf(dir.getName());
        if (pos != -1)
        {
            pathname = dir.getCanonicalPath().replace('\\', '/').substring(0, pos);
        }
        String filename = dir.getName();

        // test different filename extension
        // String filename = cmdName;
        File scriptFile = FileUtils.getFile(pathname + filename);
        if (!scriptFile.exists())
        {
            // try .groovy extension
            scriptFile = FileUtils.getFile(pathname + filename + ".groovy");
        }

        // search current directory for filename.groovy with ignorecase
        if (!scriptFile.exists())
        {
            boolean match = false;

            Log.log.info("pathname: " + pathname);
            dir = FileUtils.getFile(pathname);
            File[] filenames = dir.listFiles();
            Log.log.info("filenames: " + filenames);
            for (int i = 0; !match && i < filenames.length; i++)
            {
                String entryName = filenames[i].getName();
                String matchName = filename + ".groovy";
                if (!filenames[i].isDirectory() && matchName.equalsIgnoreCase(entryName))
                {
                    match = true;
                    scriptFile = filenames[i];
                }
            }
        }
        return scriptFile;
    }

    Object executeScript(File scriptFile, String[] argsArray) throws Exception
    {
        boolean isHelp = false;
        Object result = null;
        if (argsArray != null && argsArray.length > 0)
        {
            if (argsArray[0].equalsIgnoreCase("HELP"))
            {
                isHelp = true;
            }
        }
        else
        {
            argsArray = new String[] { scriptFile.getName() };
        }

        // set args binding
        SQLConnection conn = null;
        try
        {
            Binding binding = new Binding();
            binding.setVariable(Constants.GROOVY_BINDING_MAIN, this);
            binding.setVariable(Constants.GROOVY_BINDING_LOG, Log.log);
            binding.setVariable(Constants.RSCONSOLE_BINDING_ENV, this.configProperty.getProperties());
            binding.setVariable(Constants.RSCONSOLE_BINDING_BLUEPRINT, blueprintProperties);
            binding.setVariable(Constants.RSCONSOLE_BINDING_HELP, isHelp);
            binding.setVariable(Constants.GROOVY_BINDING_ARGS.toLowerCase(), argsArray); // args
            binding.setVariable(Constants.GROOVY_BINDING_ARGS, argsArray); // ARGS

            String execFilename = scriptFile.getCanonicalPath();
            execFilename = execFilename.replace('\\', '/');

            File file = FileUtils.getFile(execFilename);
            String script = FileUtils.readFileToString(file, "UTF-8");

            String scriptName = scriptFile.getName();

            if (script.matches("(?s).*(?<![a-zA-Z])DB.*") && isDBConnected())
            {
                conn = SQL.getConnection();
                binding.setVariable(Constants.GROOVY_BINDING_DB, conn.getConnection());
            }

            // execute
            if (isHelp)
            {
                println();
            }
            result = GroovyScript.execute(script, scriptName, true, binding);
        }
        catch (Throwable t)
        {
            Log.log.error("Failed to Execute Script", t);
            Main.println("Failed to Execute Script: " + t.getMessage());
        }
        finally
        {
            if (conn != null)
            {
                SQL.close(conn);
            }
        }

        return result;
    } // executeScript

    public String getDist()
    {
        return dist;
    } // setDist

    public boolean isBlueprintRSRemote()
    {
        return blueprintRSRemote;
    } // isBlueprintRSRemote

    public boolean isBlueprintRSControl()
    {
        return blueprintRSControl;
    } // isBlueprintRSControl

    public boolean isBlueprintRSMQ()
    {
        return blueprintRSMQ;
    } // isBlueprintRSMQ

    public boolean isBlueprintRSView()
    {
        return blueprintRSView;
    } // isBlueprintRSView

    public boolean isBlueprintRSConsole()
    {
        return blueprintRSConsole;
    } // isBlueprintRSConsole

    public boolean isBlueprintRSMgmt()
    {
        return blueprintRSMgmt;
    } // isBlueprintRSMgmt

    public boolean isBlueprintRSSync()
    {
        return blueprintRSSync;
    } // isBlueprintRSSync
    
    public boolean isBlueprintRSArchive()
    {
    	return blueprintRSArchive;
    } // isBlueprintRSArchive
    
    public boolean isBlueprintRSSearch()
    {
        return blueprintRSSearch;
    } // isBlueprintRSRSSearch

    public boolean isBlueprintLogstash() {
        return blueprintLogstash;
    }
    
    public boolean isBlueprinRSLog() {
        return blueprintRSLog;
    }
    
    public boolean isBlueprintDCS() {
        return this.blueprintDCS;
    }
    
    public boolean isBlueprintDCSRSDataLoader() {
		return blueprintDCSRSDataLoader;
	}
    
    public boolean isBlueprintDCSRSReporting() {
		return blueprintDCSRSReporting;
	}
    
    public void setBlueprintLogstash(boolean blueprintLogstash) {
		this.blueprintLogstash = blueprintLogstash;
	}
    
    public void setBlueprintRSLog(boolean blueprintRSLog) {
        this.blueprintRSLog = blueprintRSLog;
    }
    
    public void setBlueprintDCS(boolean blueprintDCS) {
      this.blueprintDCS = blueprintDCS;
    }

    public void setBlueprintDCSRSDataLoader(boolean blueprintDCSRSDataLoader) {
		this.blueprintDCSRSDataLoader = blueprintDCSRSDataLoader;
	}

    public void setBlueprintDCSRSReporting(boolean blueprintDCSRSReporting) {
		this.blueprintDCSRSReporting = blueprintDCSRSReporting;
	}
    
	public ConfigRSControl getConfigRSControl()
    {
        return configRSControl;
    } // getConfigRSControl

    public ConfigRSMQ getConfigRSMQ()
    {
        return configRSMQ;
    } // getConfigRSMQ

    public ConfigRSView getConfigRSView()
    {
        return configRSView;
    } // getConfigRSView

    public ConfigRSConsole getConfigRSConsole()
    {
        return configRSConsole;
    } // getConfigRSConsole

    public ConfigRSMgmt getConfigRSMgmt()
    {
        return configRSMgmt;
    } // getConfigRSMgmt

    public ConfigRSSync getConfigRSSync()
    {
        return configRSSync;
    } // getConfigRSSync
    
    public ConfigRSArchive getConfigRSArchive()
    {
        return configRSArchive;
    } // getConfigRSArchive
    
    public ConfigRSSearch getConfigRSSearch()
    {
        return configRSSearch;
    } // getConfigRSSearch

    public ConfigMetric getConfigMetric()
    {
        return configMetric;
    } // getConfigMetric

    public ConfigRSRemote getConfigRSRemote(String instance)
    {
        return configRSRemotes.get(instance);
    } // getConfigRSRemote

    public List<String> getRSRemoteInstances()
    {
        List<String> result = new LinkedList<String>();

        if (isBlueprintRSRemote())
        {
            for (String instance : configRSRemotes.keySet())
            {
                result.add(instance);
            }
        }

        return result;
    } // getRSRemoteInstances

    public ConfigMonitor getConfigMonitor()
    {
        return configMonitor;
    }

    public ConfigAlert getConfigAlert()
    {
        return configAlert;
    } // getConfigAlert

    public boolean isDBConnected()
    {
        return dbConnected;
    } // isDBConnected

    protected void startDefaultTasks()
    {
        Log.log.debug("Scheduling default jobs");

        // register
        ScheduledExecutor.getInstance().executeRepeat("REGISTER", MRegister.class, "register", 60, Main.main.configRegistration.getInterval() * 60, TimeUnit.SECONDS);
        if (Main.main.configRegistration.isLogHeartbeat())
        {
            ScheduledExecutor.getInstance().executeRepeat("REGISTER", MRegister.class, "logHeartbeat", 60, Main.main.configRegistration.getInterval() * 60, TimeUnit.SECONDS);
        }

        ScheduledExecutor.getInstance().executeRepeat("RSREMOTE_STATUS", MStatus.class, "rsremoteStatus", 60, configMonitor.getRsremoteInterval(), TimeUnit.SECONDS);

        // config properties
        Map params = new HashMap();
        params.put("JOBNAME", "CONFIG-PROPERTIES");
        params.put("TYPE", Constants.ACTION_INVOCATION_TYPE_INTERNAL);
        params.put("FILENAME", "config/GetProperties.groovy");
        params.put("CLASSMETHOD", "MResult.configResult");
        ScheduledExecutor.getInstance().executeDelayed(ExecuteMain.class, "execute", params, 3, TimeUnit.MINUTES);

        // send metrics
        if (!REMOTE_RSMGMT)
        {
            ScheduledExecutor.getInstance().executeRepeat(metric, "sendMetrics", 5, 5, TimeUnit.MINUTES);
        }

        if (configMonitor.isActiveLocal())
        {
            MStatus.setRestartDown(configMonitor.isRestart());
            ScheduledExecutor.getInstance().executeRepeat(MStatus.class, "localStatus", configMonitor.getLocalInterval(), configMonitor.getLocalInterval(), TimeUnit.SECONDS);
        }
        if (configMonitor.isActiveDB())
        {
            MStatus.setDbTimeout(configMonitor.getDbTimeout());
            if (DBStatus.getInstance().startUpCheck())
            {
                ScheduledExecutor.getInstance().executeRepeat(MStatus.class, "dbStatus", configMonitor.getDbInterval(), configMonitor.getDbInterval(), TimeUnit.SECONDS);
            }
        }
        if (configMonitor.isActiveESB())
        {
            MStatus.setEsbTimeout(configMonitor.getEsbInterval() * 2);
            ScheduledExecutor.getInstance().executeRepeat(MStatus.class, "esbStatus", configMonitor.getEsbInterval(), configMonitor.getEsbInterval(), TimeUnit.SECONDS);
            ScheduledExecutor.getInstance().executeRepeat(MStatus.class, "esbHeartbeat", configMonitor.getEsbInterval(), configMonitor.getEsbInterval(), TimeUnit.SECONDS);
        }
        if (configMonitor.isActiveStdout()&&!REMOTE_RSMGMT)
        {
            MStatus.setSuppressErrorsFromFile(configMonitor.getSuppressMonitorFile());
            MStatus.setStdoutErrorsFromFile(configMonitor.getStdoutMonitorFile());
            MStatus.setESErrorsFromFile(configMonitor.getESMonitorFile());
            if (MStatus.getStdoutErrors() != null && MStatus.getStdoutErrors().size() > 0)
            {
                MStatus.setStdoutDelay(configMonitor.stdoutInterval);
                ScheduledExecutor.getInstance().executeDelayed(MStatus.class, "stdoutStatus", 10, TimeUnit.SECONDS);
            }
            else
            {
                Log.log.warn("Stdout Errors not set or empty, stdout monitoring will not start");
            }
        }
        
        if (configMonitor.isActiveDiskSpace())
        {
            ScheduledExecutor.getInstance().executeRepeat(MStatus.class, "diskSpaceStatus", 500, configMonitor.getDiskSpaceInterval(), TimeUnit.SECONDS);
        }

        if (configMetric.isActive())
        {
            //process 1hr metrics - 3600000
            long tmpoffset = 3600000 - (System.currentTimeMillis() % 3600000);//300000 - (System.currentTimeMillis() % 300000);
            ScheduledExecutor.getInstance().executeDelayedRepeat(TrendMetric.class, "processDecisionTree1Hr", tmpoffset, 3600000, TimeUnit.MILLISECONDS);

            //process 1day metrics - 86400000
            tmpoffset = 86400000 - (System.currentTimeMillis() % 86400000);
            ScheduledExecutor.getInstance().executeDelayedRepeat(TrendMetric.class, "processDecisionTree1Day", tmpoffset, 86400000, TimeUnit.MILLISECONDS);
//
//            // process 5min metrics - 300000
//            long offset = 300000 - (System.currentTimeMillis() % 300000);
//            ScheduledExecutor.getInstance().executeDelayedRepeat(TrendMetric.class, "process5Min", offset, 300000, TimeUnit.MILLISECONDS);
//
//            // process 1hr metrics - 3600000
//            offset = 3600000 - (System.currentTimeMillis() % 3600000);
//            ScheduledExecutor.getInstance().executeDelayedRepeat(TrendMetric.class, "process1Hr", offset, 3600000, TimeUnit.MILLISECONDS);
//
//            // process 1day metrics - 86400000
//            offset = 86400000 - (System.currentTimeMillis() % 86400000);
//            ScheduledExecutor.getInstance().executeDelayedRepeat(TrendMetric.class, "process1Day", offset, 86400000, TimeUnit.MILLISECONDS);
        }

    } // startDefaultTasks
    
    public void startSelfCheckTasks() throws Exception
    {
//        if (configMonitor.isPingRSControl() || configMonitor.isPingRSRemote() || configMonitor.isPingRSView())
//        {
//            ScheduledExecutor.getInstance().executeRepeat(MAction.class, "esbPingStatus", 12, configMonitor.getPingInterval(), TimeUnit.SECONDS);
//        }
        if (configMonitor.isPingRSControl())
        {
            MAction.pingThreshold = configMonitor.getPingThreshold();
            ScheduledExecutor.getInstance().executeRepeat(MAction.class, "esbPingRsControl", 10, configMonitor.getPingInterval(), TimeUnit.SECONDS);
        }
        if (configMonitor.isPingRSRemote())
        {
        	MAction.pingThreshold = configMonitor.getPingThreshold();
            ScheduledExecutor.getInstance().executeRepeat(MAction.class, "esbPingRsRemote", 10, configMonitor.getPingInterval(), TimeUnit.SECONDS);
        }
        if (configMonitor.isPingRSView())
        {
        	MAction.pingThreshold = configMonitor.getPingThreshold();
            ScheduledExecutor.getInstance().executeRepeat(MAction.class, "esbPingRsView", 10, configMonitor.getPingInterval(), TimeUnit.SECONDS);
        }
        
        if (configMonitor.isEsActive())
        {
            ScheduledExecutor.getInstance().executeRepeat(MStatus.class, "esSelfCheck", 10, configMonitor.getEsInterval(), TimeUnit.SECONDS);
        }
        
        if (configMonitor.isDbTpsActive())
        {
            ScheduledExecutor.getInstance().executeRepeat(MStatus.class, "dbTps", 10, configMonitor.getDbTpsInterval(), TimeUnit.SECONDS);
        }

        if (configMonitor.isHttp())
        {
        	Properties tomcatProperties = configRSView.getTomcatProperties();
        	String protocol = null;
        	int port = 0;
        	if (tomcatProperties.getProperty("rsview.tomcat.http").equals("true"))
        	{
        		protocol = "http";
        		port = Integer.parseInt(tomcatProperties.get("rsview.tomcat.connector.http.port"));
        	}
        	else
        	{
        		protocol = "https";
        		port = Integer.parseInt(tomcatProperties.get("rsview.tomcat.connector.https.port"));
        	}
        	MStatus.setProtocol(protocol);
        	MStatus.setPort(port);
        	MStatus.setHost("localhost");

            ScheduledExecutor.getInstance().executeRepeat(MStatus.class, "rsviewHttpSelfCheck", 10, configMonitor.getHttpInterval(), TimeUnit.SECONDS);
        }
        
        if (this.blueprintRSLog && configMonitor.isRslog()) {
            Properties rslogProperties = configRSLog.getRSLogProperties();
            String host = rslogProperties.get("rslog.server.host");
            String port = rslogProperties.get("rslog.server.port");
            
            MStatus.setRsloghost(host);
            MStatus.setRslogPort(port);
            ScheduledExecutor.getInstance().executeRepeat(MStatus.class, "rslogHttpSelfCheck", 10, configMonitor.getRslogInterval(), TimeUnit.SECONDS);
        }
        
        if (activeGatewayQNames != null && !activeGatewayQNames.isEmpty())
        {
            Log.log.debug("Starting Gateway Self-Check for " + activeGatewayQNames.size() + " gateway(s)");
            
            for (String gtwQueueName : activeGatewayQNames)
            {
                GatewaySelfCheckHelper gtwSelfCheckHelper = new GatewaySelfCheckHelper(gtwQueueName);
                gtwSelfCheckHelper.gwQueueSelfCheck();
                gtwSelfCheckHelper.gwTopicSelfCheck();
                gtwSelfCheckHelper.gwWorkerSelfCheck();
            }
        }
    }

    public ConfigSQL getConfigSQL()
    {
        return configSQL;
    }

    public void setConfigSQL(ConfigSQL configSQL)
    {
        this.configSQL = configSQL;
    }

    public void setConfigMetric(ConfigMetric configMetric)
    {
        this.configMetric = configMetric;
    }
    
    public ConfigLogstash getConfigLogstash() {
		return configLogstash;
	}
    
    public ConfigDCS getConfigDCS() {
        return this.configDCS;
    }
    
    public ConfigRSDataLoader getConfigRSDataLoader() {
		return configRSDataLoader;
	}
    
    public ConfigRSReporting getConfigRSReporting() {
		return configRSReporting;
	}
    
    public ConfigRSLog getConfigRSLog() {
        return this.configRSLog;
    }
    
	@Override
    protected void initLicenseService() throws ESBException
    {
        licenseService = new LicenseService(configId.getGuid());
    }
    
    public class GatewaySelfCheckHelper
    {
        private String queueName;
        
        public GatewaySelfCheckHelper(String queueName)
        {
            this.queueName = queueName;
        }
        
        public void gwQueueSelfCheck() throws Exception
        {
            try
            {
                if (gtywQSelfCheckStarted.get(queueName).booleanValue())
                {
                    Map<String, String> params = new HashMap<String, String>();
                    params.put("Type", "Resolve Management Gateway Primary Queue Self-Check");
                    params.put(queueName, "Threshold " + configMonitor.getQueueSCThreshold() + " seconds");
                    params.put("FromInstance", MainBase.main.configId.getGuid());
                    
                    Map<String, String> response = MainBase.getESB().call(queueName, "MRSSelfCheck.selfCheck", 
                                                                          params, configMonitor.getQueueSCThreshold()*1000);
                    
                    if (response == null)
                    {
                        Log.alert("Instance " + MainBase.main.configId.getGuid() + " breached Resolve Management Gateway " + queueName + " Self-Check", 
                                  "Instance " + MainBase.main.configId.getGuid() + " Breached Resolve Management Gateway " + queueName +  
                                  " self-check threshold of " + configMonitor.getQueueSCThreshold() + 
                                  " seconds!!!");
                    }
                }
            }
            finally
            {
                if (!gtywQSelfCheckStarted.get(queueName).booleanValue())
                {
                    // schedule next self-check
                    ScheduledExecutor.getInstance().executeRepeat("RSMgmt-Gateway-" + queueName + "-QueueSelfCheck", this, "gwQueueSelfCheck", configMonitor.getQueueSCInterval(), TimeUnit.SECONDS);
                    gtywQSelfCheckStarted.put(queueName, Boolean.TRUE);
                    Log.log.info("Scheduled RSMgmt-Gateway-" + queueName + "-QueueSelfCheck to run every " + configMonitor.getQueueSCInterval() + " seconds");
                }
            }
        }
        
        public void gwTopicSelfCheck() throws Exception
        {
            try
            {
                if (gtywTSelfCheckStarted.get(queueName).booleanValue())
                {
                    Map<String, String> params = new HashMap<String, String>();
                    params.put("Type", "Resolve Management Gateway Topic Self-Check");
                    params.put(queueName + "_TOPIC", "Threshold " + configMonitor.getTopicSCThreshold() + " seconds");
                    params.put("FromInstance", MainBase.main.configId.getGuid());
                    
                    Map<String, String> response = MainBase.getESB().call(queueName + "_TOPIC", "MRSSelfCheck.selfCheck", 
                                                                          params, configMonitor.getTopicSCThreshold()*1000);
                    
                    if (response == null)
                    {
                        Log.alert("Instance " + MainBase.main.configId.getGuid() + " breached Resolve Management Gateway " + queueName + "_TOPIC Self-Check", 
                                  "Instance " + MainBase.main.configId.getGuid() + " Breached Resolve Management Gateway " + queueName +  "_TOPIC" +
                                  " self-check threshold of " + configMonitor.getTopicSCThreshold() + 
                                  " seconds!!!");
                    }
                }
            }
            finally
            {
                if (!gtywTSelfCheckStarted.get(queueName).booleanValue())
                {
                    // schedule next self-check
                    ScheduledExecutor.getInstance().executeRepeat("RSMgmt-Gateway-" + queueName + "_TOPIC-TopicSelfCheck", this, "gwTopicSelfCheck", configMonitor.getTopicSCInterval(), TimeUnit.SECONDS);
                    gtywTSelfCheckStarted.put(queueName, Boolean.TRUE);
                    Log.log.info("Scheduled RSMgmt-Gateway-" + queueName + "_TOPIC-TopicSelfCheck to run every " + configMonitor.getTopicSCInterval() + " seconds");
                }
            }
        }
        
        public void gwWorkerSelfCheck() throws Exception
        {
            try
            {
                if (gtywWSelfCheckStarted.get(queueName).booleanValue())
                {
                    Map<String, String> params = new HashMap<String, String>();
                    params.put("Type", "Resolve Management Gateway Worker Self-Check");
                    params.put(queueName + "_WORKER", "Threshold " + configMonitor.getWorkerSCThreshold() + " seconds");
                    params.put("FromInstance", MainBase.main.configId.getGuid());
                    
                    Map<String, String> response = MainBase.getESB().call(queueName + "_WORKER", "MRSSelfCheck.selfCheck", 
                                                                          params, configMonitor.getWorkerSCThreshold()*1000);
                    
                    if (response == null)
                    {
                        Log.alert("Instance " + MainBase.main.configId.getGuid() + " breached Resolve Management Gateway " + queueName + "_WORKER Self-Check", 
                                  "Instance " + MainBase.main.configId.getGuid() + " Breached Resolve Management Gateway " + queueName +  "_WORKER" +
                                  " self-check threshold of " + configMonitor.getWorkerSCThreshold() + 
                                  " seconds!!!");
                    }
                }
            }
            finally
            {
                if (!gtywWSelfCheckStarted.get(queueName).booleanValue())
                {
                    // schedule next self-check
                    ScheduledExecutor.getInstance().executeRepeat("RSMgmt-Gateway-" + queueName + "_WORKER-WorkerSelfCheck", this, "gwWorkerSelfCheck", configMonitor.getWorkerSCInterval(), TimeUnit.SECONDS);
                    gtywWSelfCheckStarted.put(queueName, Boolean.TRUE);
                    Log.log.info("Scheduled RSMgmt-Gateway-" + queueName + "_WORKER-WorkerSelfCheck to run every " + configMonitor.getWorkerSCInterval() + " seconds");
                }
            }
        }
    }
    
} // Main
