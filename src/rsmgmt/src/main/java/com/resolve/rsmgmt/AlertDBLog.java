/******************************************************************************
* (C) Copyright 2014
*
* Resolve Systems, LLC
*
* All rights reserved
* This software is distributed under license and may not be used, copied,
* modified, or distributed without the express written permission of
* Resolve Systems, LLC.
*
******************************************************************************/

package com.resolve.rsmgmt;

import org.apache.commons.lang3.StringUtils;

import com.resolve.sql.SQL;
import com.resolve.sql.SQLConnection;
import com.resolve.sql.SQLRecord;
import com.resolve.util.Log;

public class AlertDBLog
{
    final static String ALERT_LOG = "alert_log";
    
    public static void alert(String severity, String component, String type, String message)
    {
        SQLConnection conn = null;
        
        try
        {
            if (StringUtils.isNotBlank(message) && message.length() > 4000)
            {
                Log.log.error("Alert Message is too large, truncating to 4000 characters:\n" + message);
                message = message.substring(0, 4000);
            }
            conn = SQL.getConnection();
            
            Log.log.trace("Alerting to alert_log"
                            + "\nu_severity = " + severity
                            + "\nu_component = " + component
                            + "\nu_type = " + type
                            + "\nu_message = " + message);
            SQLRecord rec = new SQLRecord(conn.getConnection(), ALERT_LOG);
            rec.put("u_severity", severity);
            rec.put("u_component", component);
            rec.put("u_type", type);
            rec.put("u_message", message);
            rec.insert();
        }
        catch (Throwable e)
        {
            Log.log.error(e.getMessage(), e);
        }
        finally
        {
            if (conn != null)
            {
                SQL.close(conn);
            }
        }
        
    } // alert

} // AlertDBLog
