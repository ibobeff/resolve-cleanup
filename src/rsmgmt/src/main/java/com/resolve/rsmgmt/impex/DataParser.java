/******************************************************************************
* (C) Copyright 2014
*
* Resolve Systems, LLC
*
* All rights reserved
* This software is distributed under license and may not be used, copied,
* modified, or distributed without the express written permission of
* Resolve Systems, LLC.
*
******************************************************************************/

package com.resolve.rsmgmt.impex;

import java.util.List;

/**
 * This implementing classes must take care of the proper conversion ofits data. 
 */
public interface DataParser
{
    /**
     * This method parses the data and place all the column value in the array.
     * 
     * @param data could be CSV or XML etc., the implementing class knows how to parse.
     * @param delimeter
     * @return
     */
    List<String[]> parseData(String data, char delimeter) throws Exception;
}
