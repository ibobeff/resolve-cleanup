/******************************************************************************
* (C) Copyright 2014
*
* Resolve Systems, LLC
*
* All rights reserved
* This software is distributed under license and may not be used, copied,
* modified, or distributed without the express written permission of
* Resolve Systems, LLC.
*
******************************************************************************/

package com.resolve.rsmgmt.impex;

public interface DataImpex
{
    /**
     * This method does the data import to a table.
     * 
     * @param userId
     * @param tableName
     * @param data
     * @param delimeter
     * @param isDelimeter
     * @param stopOnFailure
     * @return
     * @throws Exception
     */
    boolean importData(String userId, String tableName, String data, char delimeter, boolean isOverride, boolean stopOnFailure) throws Exception;
    
    /**
     * This method does the data export to a table.
     * 
     * @param userId
     * @param tableName
     * @param delimeter
     * @return a CSV
     * @throws Exception
     */
    String exportData(String userId, String tableName, char delimeter) throws Exception;
    
    /**
     * Exports data from a table into an Excel (XLS) file.
     * 
     * @param userId
     * @param file
     * @param tableName
     * @throws Exception
     */
    void exportToXLS(String userId, String file, String tableName) throws Exception;
}
