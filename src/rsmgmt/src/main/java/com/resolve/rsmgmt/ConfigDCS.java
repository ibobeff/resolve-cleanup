package com.resolve.rsmgmt;

import com.resolve.util.Properties;

public class ConfigDCS {

  private static final String CONFIG_PREFIX = "dcs";

  private Properties dcsProperties = new Properties();

  public ConfigDCS(Properties properties) {

    for (Object key : properties.keySet()) {
      String strKey = key.toString();
      if (strKey.startsWith(CONFIG_PREFIX)) {
        String value = properties.getProperty(strKey).trim();
        if (value.contains("${"))
        {
            value = properties.getReplacement(key).trim();
        }
        this.dcsProperties.put(key, value);
      }
    }
  }

  public Properties getDCSProperties() {

    return this.dcsProperties;
  }

  public void setDCSProperties(Properties dcsProperties) {

    this.dcsProperties = dcsProperties;
  }
  
}
