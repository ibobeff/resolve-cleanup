/******************************************************************************
* (C) Copyright 2014
*
* Resolve Systems, LLC
*
* All rights reserved
* This software is distributed under license and may not be used, copied,
* modified, or distributed without the express written permission of
* Resolve Systems, LLC.
*
******************************************************************************/

package com.resolve.rsmgmt;

import java.net.InetAddress;

import com.resolve.util.ConfigMap;
import com.resolve.util.XDoc;

public class ConfigAlert extends ConfigMap
{
    private static final long serialVersionUID = -412946112329974663L;
	boolean active = false;
    boolean snmpTrapActive = false;
    String snmpTrapHost = "127.0.0.1";
    int snmpTrapPort = 162;
    String snmpTrapCommunity = "public";
    boolean dblogActive = true;
    boolean gatewayLogActive = false;
    boolean emailActive = false;
    //Subject of the email can be configured as follows:
    //"Resolve alert message ${MESSAGE} with Severity ${SEVERITY} where
    //${MESSAGE} and ${SEVERITY} will be replaced with actual message and 
    //severity. Additionally there are ${CODE} and ${COMPONENT} parameters as well
    //All parameters are optional. 
    String subject = "Resolve Alert";  
    String body = "Severity: ${SEVERITY}, Component: ${COMPONENT}, Type: ${CODE}, Message: ${MESSAGE}.";  
    String toEmail = null; //send alert email to this address
    String ccEmail = null; //comma separated cc addresses. 
    String bccEmail = null; //comma separated bcc addresses. 
    boolean httpActive=false;
    String httpHost;
    String httpPort;
    String httpUri;
    String httpProtocol;
    String rsmgmtHost;
    String rsmgmtHostName = InetAddress.getLocalHost().getHostName().toUpperCase();
    
    public ConfigAlert(XDoc config) throws Exception
    {
        super(config);
        
        define("active", BOOLEAN, "./ALERT/@ACTIVE");
        define("snmpTrapActive", BOOLEAN, "./ALERT/SNMP/@ACTIVE");
        define("snmpTrapHost", STRING, "./ALERT/SNMP/@TRAPHOST");
        define("snmpTrapPort", INTEGER, "./ALERT/SNMP/@TRAPPORT");
        define("snmpTrapCommunity", STRING, "./ALERT/SNMP/@COMMUNITY");
        define("dblogActive", BOOLEAN, "./ALERT/DBLOG/@ACTIVE");
        define("gatewayLogActive", BOOLEAN, "./ALERT/GATEWAY/@ACTIVE");
        define("emailActive", BOOLEAN, "./ALERT/EMAIL/@ACTIVE");
        define("subject", STRING, "./ALERT/EMAIL/@SUBJECT");
        define("body", STRING, "./ALERT/EMAIL/@BODY");
        define("toEmail", STRING, "./ALERT/EMAIL/@TO");
        define("ccEmail", STRING, "./ALERT/EMAIL/@CC");
        define("bccEmail", STRING, "./ALERT/EMAIL/@BCC");
        define("httpActive", BOOLEAN, "./ALERT/HTTP/@ACTIVE");
        define("httpHost", STRING, "./ALERT/HTTP/@HOST");
        define("httpPort", STRING, "./ALERT/HTTP/@PORT");
        define("httpUri", STRING, "./ALERT/HTTP/@URI");
        define("httpProtocol", STRING, "./ALERT/HTTP/@PROTOCOL");
        define("rsmgmtHost", STRING, "./ID/@LOCATION");
        define("rsmgmtHostName", STRING, "./ID/@NAME");
    } // ConfigReceiveNetcool
    
    public void load() throws Exception
    {
        loadAttributes();
    } // load

    public void save() throws Exception
    {
        saveAttributes();
    } // save
    
    public boolean isActive()
    {
        return active;
    }

    public void setActive(boolean active)
    {
        this.active = active;
    }

    public boolean isSnmpTrapActive()
    {
        return snmpTrapActive;
    }

    public void setSnmpTrapActive(boolean snmpTrapActive)
    {
        this.snmpTrapActive = snmpTrapActive;
    }

    public String getSnmpTrapHost()
    {
        return snmpTrapHost;
    }

    public void setSnmpTrapHost(String snmpTrapHost)
    {
        this.snmpTrapHost = snmpTrapHost;
    }

    public int getSnmpTrapPort()
    {
        return snmpTrapPort;
    }

    public void setSnmpTrapPort(int snmpTrapPort)
    {
        this.snmpTrapPort = snmpTrapPort;
    }

    public String getSnmpTrapCommunity()
    {
        return snmpTrapCommunity;
    }

    public void setSnmpTrapCommunity(String snmpTrapCommunity)
    {
        this.snmpTrapCommunity = snmpTrapCommunity;
    }

    public boolean isDblogActive()
    {
        return dblogActive;
    }

    public void setDblogActive(boolean dblogActive)
    {
        this.dblogActive = dblogActive;
    }

    public boolean isGatewayLogActive()
    {
        return gatewayLogActive;
    }

    public void setGatewayLogActive(boolean gatewayLogActive)
    {
        this.gatewayLogActive = gatewayLogActive;
    }

    public boolean isEmailActive()
    {
        return emailActive;
    }

    public void setEmailActive(boolean emailActive)
    {
        this.emailActive = emailActive;
    }

    public String getSubject()
    {
        return subject;
    }

    public void setSubject(String subject)
    {
        this.subject = subject;
    }

    public String getBody()
    {
        return body;
    }

    public void setBody(String body)
    {
        this.body = body;
    }

    public String getToEmail()
    {
        return toEmail;
    }

    public void setToEmail(String toEmail)
    {
        this.toEmail = toEmail;
    }

    public String getCcEmail()
    {
        return ccEmail;
    }

    public void setCcEmail(String ccEmail)
    {
        this.ccEmail = ccEmail;
    }

    public String getBccEmail()
    {
        return bccEmail;
    }

    public void setBccEmail(String bccEmail)
    {
        this.bccEmail = bccEmail;
    }

    public boolean isHttpActive()
    {
        return httpActive;
    }

    public void setHttpActive(boolean httpActive)
    {
        this.httpActive = httpActive;
    }

    public String getHttpHost()
    {
        return httpHost;
    }

    public void setHttpHost(String httpHost)
    {
        this.httpHost = httpHost;
    }

    public String getHttpPort()
    {
        return httpPort;
    }

    public void setHttpPort(String httpPort)
    {
        this.httpPort = httpPort;
    }

    public String getHttpUri()
    {
        return httpUri;
    }

    public void setHttpUri(String httpUri)
    {
        this.httpUri = httpUri;
    }

    public String getHttpProtocol()
    {
        return httpProtocol;
    }

    public void setHttpProtocol(String httpProtocol)
    {
        this.httpProtocol = httpProtocol;
    }

    public String getRsmgmtHost()
    {
        return rsmgmtHost;
    }

    public void setRsmgmtHost(String rsmgmtHost)
    {
        this.rsmgmtHost = rsmgmtHost;
    }

    public String getRsmgmtHostName()
    {
        return rsmgmtHostName;
    }

    public void setRsmgmtHostName(String rsmgmtHostName)
    {
        this.rsmgmtHostName = rsmgmtHostName;
    }

} // ConfigAlert