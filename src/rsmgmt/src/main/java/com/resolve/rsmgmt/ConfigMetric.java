/******************************************************************************
* (C) Copyright 2014
*
* Resolve Systems, LLC
*
* All rights reserved
* This software is distributed under license and may not be used, copied,
* modified, or distributed without the express written permission of
* Resolve Systems, LLC.
*
******************************************************************************/

package com.resolve.rsmgmt;

import java.util.List;
import java.util.Map;

import com.resolve.util.ConfigMap;
import com.resolve.util.XDoc;

public class ConfigMetric extends ConfigMap
{
    private static final long serialVersionUID = 368711779316602889L;

	boolean active = false;
    
    List<Map<String,String>> groups;
    
    public ConfigMetric(XDoc config) throws Exception
    {
        super(config);
        
        // netcool
        define("active", BOOLEAN, "./METRIC/@ACTIVE");
    } // ConfigReceiveNetcool
    
    public void load() throws Exception
    {
        loadAttributes();
        
        groups = xdoc.getListMapValue("./METRIC/GROUP");
    } // load

    public void save() throws Exception
    {
        xdoc.setListMapValue("./METRIC/GROUP", groups);
        
        saveAttributes();
    } // save
    
    public boolean isActive()
    {
        return active;
    }

    public void setActive(boolean active)
    {
        this.active = active;
    }

    public List<Map<String,String>> getGroups()
    {
        return this.groups;
    }
    
} // ConfigMetric