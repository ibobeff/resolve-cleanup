/******************************************************************************
 * (C) Copyright 2014
 *
 * Resolve Systems, LLC
 *
 * All rights reserved This software is distributed under license and may not be
 * used, copied, modified, or distributed without the express written permission
 * of Resolve Systems, LLC.
 *
 ******************************************************************************/

package com.resolve.rsmgmt;

import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import com.resolve.connect.DB2DBConnect;
import com.resolve.connect.MySQLDBConnect;
import com.resolve.connect.OracleDBConnect;
import com.resolve.util.CryptUtils;
import com.resolve.util.Log;
import com.resolve.util.Properties;
import com.resolve.util.StringUtils;

public class ConfigRSConsole
{
    private String instanceName = "rsconsole";

    private Properties rsconsoleProperties;
    private Properties logProperties;
    private Properties envProperties;
    private Properties esapiProperties;
    private Properties connectionProperties;

    public ConfigRSConsole()
    {
        this.rsconsoleProperties = new Properties();
        this.logProperties = new Properties();
        this.envProperties = new Properties();
        this.esapiProperties = new Properties();
        this.connectionProperties = new Properties();
    } // ConfigRSConsole

    public ConfigRSConsole(Properties properties)
    {
        this();
        for (Object key : properties.keySet())
        {
            String strKey = key.toString();
            if (strKey.startsWith(instanceName + "."))
            {
                String value = properties.getProperty(strKey).trim();
                if (value.contains("${"))
                {
                    value = properties.getReplacement(key).trim();
                }
                if (value.startsWith("TO_ENC:") || value.startsWith("NO_ENC:"))
                {
                    value = value.substring(7);
                }
                if (value.startsWith("ENC"))
                {
                    try
                    {
                        value = CryptUtils.decrypt(value);
                    }
                    catch (Exception e)
                    {
                        Log.log.error("Failed to decrypt blueprint value", e);
                        value = "";
                    }
                }
                
                if (strKey.contains("brokeraddr") && value.matches(":\\d*"))
                {
                    value = "";
                }
                if (value != null)
                {
                    if (strKey.startsWith(instanceName + ".log4j"))
                    {
                        this.logProperties.put(key, value);
                    }
                    else if (strKey.startsWith(instanceName + ".env"))
                    {
                        this.envProperties.put(key, value);
                    }
                    else if (strKey.startsWith(instanceName + ".ESAPI"))
                    {
                        this.esapiProperties.put(key, value);
                    }
                    else if (strKey.startsWith(instanceName + ".connection"))
                    {
                        this.connectionProperties.put(key, value);
                    }
                    else
                    {
                        this.rsconsoleProperties.put(key, value);
                    }
                }
            }
            
        }
    } // ConfigRSConsole

    public Map<String, String> rsconsoleEsapiReplaceValues()
    {
        Map<String, String> result = new HashMap<String, String>();

        for (Object key : esapiProperties.keySet())
        {
            String strKey = key.toString();
            String value = esapiProperties.getProperty(strKey);
            
            strKey = strKey.replaceFirst(instanceName + ".ESAPI.", "");

            // don't replace \\ with / if it's a validator property. It messes with the regex.
            if(strKey.startsWith("Validator")) {
                value = value.replace("\\", "\\\\");
            } else {
                value = value.replace("\\\\", "/");
            }
            
            String replaceRegex = strKey + "=.*";
            String replaceString = strKey + "=" + value;
            
            result.put(replaceRegex, replaceString);
        }

        return result;
    } // rsviewEsapiReplaceValues

    public Map<String, Object> rsconsoleXPathValues()
    {
        Map<String, Object> result = new HashMap<String, Object>();

        for (Object key : rsconsoleProperties.keySet())
        {
            String strKey = key.toString();
            String value = (String) rsconsoleProperties.get(key);

            strKey = strKey.replaceFirst(instanceName, "").toUpperCase();
            strKey = strKey.replaceAll("(?<!\\\\)\\.", "/");
            strKey = strKey.replaceAll("\\\\\\.", ".");

            Pattern pattern = Pattern.compile("(.+)/(.+)/(\\d+)");
            Matcher matcher = pattern.matcher(strKey);
            if (matcher.matches())
            {
                strKey = "." + matcher.group(1);
                String xdocKey = matcher.group(2);
                int listPosition = Integer.parseInt(matcher.group(3)) - 1;

                List<Map<String, String>> list = (List<Map<String, String>>) result.get(strKey);
                if (list == null)
                {
                    list = new LinkedList<Map<String, String>>();
                    result.put(strKey, list);
                }
                for (int j = list.size(); j <= listPosition; j++)
                {
                    list.add(new HashMap<String, String>());
                }
                Map<String, String> entry = list.get(listPosition);
                entry.put(xdocKey, value);
                list.set(listPosition, entry);
            }
            else
            {
                strKey = strKey.replaceFirst("^(.*)/(.*?)$", ".$1/@$2");
                result.put(strKey, value);
            }
        }

        return result;
    } // rsconsoleXPathValues()

    public Map<String, String> rsconsoleConnectionsReplaceValues()
    {
        Map<String, String> result = new HashMap<String, String>();

        String replaceRegex = "#?connection1\\.type=.*";
        String dbType = connectionProperties.get(instanceName + ".connection.dbtype").toLowerCase();

        String replaceValue = "connection1.type=" + dbType;
        result.put(replaceRegex, replaceValue);

        replaceRegex = "#?connection1\\.username=.*";
        replaceValue = "connection1.username=" + connectionProperties.get(instanceName + ".connection.username");
        result.put(replaceRegex, replaceValue);

        replaceRegex = "#?connection1\\.password=.*";
        replaceValue = "connection1.password=" + connectionProperties.get(instanceName + ".connection.password");
        result.put(replaceRegex, replaceValue);

        String url = connectionProperties.get(instanceName + ".connection.url");
        if (StringUtils.isEmpty(url))
        {
            String hostname = connectionProperties.get(instanceName + ".connection.host");
            String dbname = connectionProperties.get(instanceName + ".connection.dbname");
            if (dbType.equalsIgnoreCase("mysql"))
            {
                url = MySQLDBConnect.getConnectURL(hostname, dbname);
            }
            else if (dbType.equalsIgnoreCase("oracle"))
            {
                url = OracleDBConnect.getConnectURL(hostname, dbname);
            }
            else if (dbType.equalsIgnoreCase("db2"))
            {
                url = DB2DBConnect.getConnectURL(hostname, dbname);
            }
        }

        replaceRegex = "#?connection1\\.url=.*";
        replaceValue = "connection1.url=" + url;
        result.put(replaceRegex, replaceValue);

        return result;
    }
    
    public Map<String, String> rsconsoleEnvReplaceValues()
    {
        Map<String, String> result = new HashMap<String, String>();
        
        for (Object key : envProperties.keySet())
        {
            String strKey = key.toString();
            String value = envProperties.getProperty(strKey);
            
            strKey = strKey.replaceFirst(instanceName + ".env.", "");
            value = value.replaceAll("\\\\", "/");
            if (value.equalsIgnoreCase("true") || value.equalsIgnoreCase("false"))
            {
                value = value.toUpperCase();
            }
            
            strKey = strKey.toUpperCase();
            String replaceRegex = strKey + "=.*";
            String replaceString = strKey + "=" + value;

            result.put(replaceRegex, replaceString);
        }
        return result;
    }
    
    public Map<String, String> rsconsoleLogReplaceValues()
    {
        Map<String, String> result = new HashMap<String, String>();

        for (Object key : logProperties.keySet())
        {
            String strKey = key.toString();
            String value = logProperties.getProperty(strKey);

            strKey = strKey.replaceFirst(instanceName + ".", "");
            value = value.replaceAll("\\\\", "/");

            String replaceRegex = strKey + "=.*";
            String replaceString = strKey + "=" + value;

            result.put(replaceRegex, replaceString);
        }

        return result;
    } // rsconsoleLogReplaceValues

    public void setEsapiProperties(Properties esapiProperties)
    {
        this.esapiProperties = esapiProperties;
    }

    public Properties getEsapiProperties()
    {
        return this.esapiProperties;
    }
}
