/******************************************************************************
* (C) Copyright 2014
*
* Resolve Systems, LLC
*
* All rights reserved
* This software is distributed under license and may not be used, copied,
* modified, or distributed without the express written permission of
* Resolve Systems, LLC.
*
******************************************************************************/

package com.resolve.rsmgmt;

import com.resolve.util.ConfigMap;
import com.resolve.util.XDoc;

public class ConfigRestWebservice extends ConfigMap
{
    private static final long serialVersionUID = 7569626161766673993L;
	private boolean active = false;
    private int httpport = 8082;
    private int httpsport = 8083;
    private String username = "resolve";
    private String p_assword = "resolve";
    private boolean ssl = false;
    private String k_eystore = null;;
    private String k_eystorep_assword = "changeit";
    private String k_eyp_assword = "changeit";

    public ConfigRestWebservice(XDoc config) throws Exception
    {
        super(config);

        define("active", BOOLEAN, "./RESTWEBSERVICE/@ACTIVE");
        define("username", STRING, "./RESTWEBSERVICE/@USERNAME");
        define("p_assword", SECURE, "./RESTWEBSERVICE/@PASSWORD");
        define("httpport", INTEGER, "./RESTWEBSERVICE/@HTTPPORT");
        define("httpsport", INTEGER, "./RESTWEBSERVICE/@HTTPSPORT");
        define("ssl", BOOLEAN, "./RESTWEBSERVICE/@SSL");
        define("k_eystore", STRING, "./RESTWEBSERVICE/@KEYSTORE");
        define("k_eystorep_assword", SECURE, "./RESTWEBSERVICE/@KEYSTOREPASSWORD");
        define("k_eyp_assword", SECURE, "./RESTWEBSERVICE/@KEYPASSWORD");
    } // ConfigRestWebservice

    public void load() throws Exception
    {
        loadAttributes();
    } // load

    public void save() throws Exception
    {
        saveAttributes();
    } // save

    public boolean isActive()
    {
        return active;
    }

    public void setActive(boolean active)
    {
        this.active = active;
    }

    public int getHttpport()
    {
        return httpport;
    }

    public void setHttpport(int httpport)
    {
        this.httpport = httpport;
    }

    public int getHttpsport()
    {
        return httpsport;
    }

    public void setHttpsport(int httpsport)
    {
        this.httpsport = httpsport;
    }

    public String getUsername()
    {
        return username;
    }

    public void setUsername(String username)
    {
        this.username = username;
    }

    public String getP_assword()
    {
        return p_assword;
    }

    public void setP_assword(String p_assword)
    {
        this.p_assword = p_assword;
    }

    public boolean isSsl()
    {
        return ssl;
    }

    public void setSsl(boolean ssl)
    {
        this.ssl = ssl;
    }

    public String getK_eystore()
    {
        return k_eystore;
    }

    public void setK_eystore(String keystore)
    {
        this.k_eystore = keystore;
    }

    public String getK_eystorep_assword()
    {
        return k_eystorep_assword;
    }

    public void setK_eystorep_assword(String keystorepassword)
    {
        this.k_eystorep_assword = keystorepassword;
    }

    public String getK_eyp_assword()
    {
        return k_eyp_assword;
    }

    public void setK_eyp_assword(String keypassword)
    {
        this.k_eyp_assword = keypassword;
    }

} // ConfigRestWebservice