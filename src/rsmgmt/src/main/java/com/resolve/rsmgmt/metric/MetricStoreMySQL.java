/******************************************************************************
* (C) Copyright 2014
*
* Resolve Systems, LLC
*
* All rights reserved
* This software is distributed under license and may not be used, copied,
* modified, or distributed without the express written permission of
* Resolve Systems, LLC.
*
******************************************************************************/

package com.resolve.rsmgmt.metric;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.resolve.persistence.util.HibernateUtil;
import com.resolve.rsmgmt.ConfigMetric;
import com.resolve.sql.SQL;
import com.resolve.sql.SQLConnection;
import com.resolve.util.GMTDate;
import com.resolve.util.Log;
import com.resolve.util.MetricCache;
import com.resolve.util.MetricMode;
import com.resolve.util.MetricMsg;
import com.resolve.util.MetricType;
import com.resolve.util.MetricUnit;
import com.resolve.util.SysId;

public class MetricStoreMySQL implements MetricStore
{
    //Table Identifiers for min, hr and day metric tables 
    public static final String METRIC_MIN = " metric_min_";
    public static final String METRIC_HR = " metric_hr_";
    public static final String METRIC_DAY = " metric_day_";
    public static final String TMETRIC_MIN = " tmetric_min_";
    public static final String TMETRIC_HR = " tmetric_hr_";
    public static final String TMETRIC_DAY = " tmetric_day_";
    public static final String DECISION_TREE_TYPE = "decisiontree";
    
    public void createTables(ConfigMetric config)
    {
        SQLConnection conn = null;
        try
        {
            conn = SQL.getConnection();
            // create tables for each group
            for (Map<String, String> group : config.getGroups())
            {
                String name = group.get("NAME").toLowerCase();
                String unitsStr = group.get("UNITS").toLowerCase();
                if(group.get("TYPE") == null)
                {
                    Log.log.error("Please verify that the rsmgmt/config.xml and blueprint.properties is up to date");
                }
                String type = group.get("TYPE").toLowerCase();
                String[] units = unitsStr.split(",");
                analyzeMetricGroup(conn, group, name, units, type);
            } 
        }
        catch (Throwable e)
        {
            Log.log.error(e.getMessage(), e);
        }
        finally
        {
            try
            {
                if (conn != null)
                {
                    SQL.close(conn);
                }
            }
            catch (Exception e)
            {
                Log.log.error(e.getMessage(), e);
            }
        }

    } // createTables

    /**
     *  This method analyzes the METRIC GROUP from rsmgmt config.xml file and decides whether to create new metric tables
     *  or use the existing ones.
     * @param conn
     * @param stmt
     * @param group
     * @param name
     * @param units
     * @return
     * @throws SQLException
     */
    private  void analyzeMetricGroup(SQLConnection conn, Map<String, String> group, String name, String[] units, String type) throws SQLException
    {
        boolean init = false;
        String initStr = group.get("INIT");
        if (initStr != null && initStr.equalsIgnoreCase("true"))
        {
            init = true;
        }
        boolean drop = false;
        String dropStr = group.get("DROP");
        if (dropStr != null && dropStr.equalsIgnoreCase("true"))
        {
            drop = true;
        }
        
        if(type.equalsIgnoreCase("default"))
        {
            //create regular metric tables
            initializetables(conn, name, units, init, drop);
        }
        else if(type.equalsIgnoreCase(DECISION_TREE_TYPE))
        {
            //create special tmetric tables
            initializeDecisionTreeTable(conn, name, units, init, drop);
        }
        
        // update init and drop to false
        group.put("INIT", "false");
        group.put("DROP", "false");
        
    } // analyzeMetricGroup

    /**
     *  This method makes a call to create special metric tables starting with name TMETRIC
     *  
     * @param conn
     * @param name: table name
     * @param units: table columns
     * @param drop: boolean flag to specify if we need to drop the table
     * @throws SQLException
     */
    private void initializeDecisionTreeTable(SQLConnection conn, String name, String[] units, boolean init, boolean drop) throws SQLException
    {
        Statement stmt = null;
        if (init)
        {
            //create the tmetric_lookup table
            stmt = createDTLookupTable(conn);
            
            //create the TMETRIC hr table
            stmt = createDTMetricTable(TMETRIC_HR, conn, name, drop, units);
            //create the TMETRIC day table
            stmt = createDTMetricTable(TMETRIC_DAY, conn, name, drop, units);
        }

        // close the statements
        if (stmt != null) {
            try {
                stmt.close();
            } 
            catch (SQLException sqlEx) 
            {
                Log.log.error(sqlEx.getMessage(), sqlEx);
            } 
        }
    }  // initializeDecisionTreeTable

    /**
     * This method creates the tmetric_lookup tables that maps DT full paths to DT path IDs
     * @param conn
     * @return
     */
    private Statement createDTLookupTable(SQLConnection conn)
    {
        Statement stmt = null;
        String sql = "";
        // create empty TMetric lookup table
        try
        {
            sql = "";
            sql += "CREATE table tmetric_lookup" + "\n";
            sql += "(" + "\n";
            sql += "sys_id VARCHAR(32) NOT NULL," + "\n";
            sql += "u_full_path LONGTEXT," + "\n";  // full path of DT
            sql += "u_path_id VARCHAR(255) DEFAULT NULL," + "\n";  // unique checksum for that path
            sql += "u_dt_root_doc VARCHAR(255) DEFAULT NULL," + "\n";  // root_dt
            sql += "sys_created_by VARCHAR(255) DEFAULT NULL," + "\n";
            sql += "sys_created_on DATETIME," + "\n"; 
            sql += "sys_mod_count INT(11)," + "\n";
            sql += "sys_updated_by VARCHAR(255)," + "\n";
            sql += "sys_updated_on DATETIME DEFAULT NULL," + "\n";
            sql += "PRIMARY KEY (sys_id)," + "\n";
            sql += "UNIQUE UNQKEY (u_path_id)," + "\n";
            sql += "KEY INDEXPATHID (u_path_id)" + "\n";
            sql += ")" + "\n";
            stmt = conn.createStatement();
            Log.log.trace(sql);
            stmt.execute(sql);
            stmt.close();
        }
        catch (SQLException createExistingTableError)
        {
            mySQLErrorMsgs("tmetric_", "lookup", createExistingTableError);
        }
        return stmt;
    } // createDTLookupTable

    /**
     *  This method creates the special metric tables called TMETRIC (if not present) and alters the data of the existing table (if necessary)
     *   to add new columns or units necessary to log the metrics that need to be reported
     *   
     * @param tmetricID
     * @param conn
     * @param name
     * @param drop
     * @param units
     * @return
     * @throws SQLException
     */
    private Statement createDTMetricTable(String tmetricID, SQLConnection conn, String name, boolean drop, String[] units) throws SQLException
    {
        Statement stmt = null;
        String sql = "";
        
        if (drop)
        {
            sql += "DROP TABLE IF EXISTS " + tmetricID + name + "\n";
            Log.log.trace(sql);
            stmt = conn.createStatement();
            stmt.execute(sql);
            stmt.close();
        }

        // create empty TMetric TableID table
        try
        {
            sql = "";
            sql += "CREATE table " + tmetricID + name + "\n";
            sql += "(" + "\n";
            sql += "idx INTEGER DEFAULT NULL," + "\n";
            sql += "pathid VARCHAR(40)," + "\n";  // max can be 8000 bytes. With UTF8, a character can use up to 4 bytes
            sql += "nodeid VARCHAR(255)," + "\n";  // fullname of a DT document
            sql += "seqid INT(11)," + "\n";        // seq
            sql += "userid VARCHAR(32)," + "\n";  // username initiating the execution of the DT path
            sql += "ts BIGINT(20)," + "\n";
            sql += "ts_datetime DATETIME," + "\n";
            sql += "UNIQUE UNQKEY (idx,pathid,nodeid,seqid,userid)," + "\n";
            sql += "KEY INDEXPATHID (pathid)," + "\n";
            sql += "KEY INDEXNODEID (nodeid)," + "\n";
            sql += "KEY INDEXUSERID (userid)" + "\n";
            sql += ")" + "\n";
            stmt = conn.createStatement();
            Log.log.trace(sql);
            stmt.execute(sql);
            stmt.close();
        }
        catch (SQLException createExistingTableError)
        {
            mySQLErrorMsgs(tmetricID, name, createExistingTableError);
        }

        // alter table for creating or adding new fields in metricTableID table
        alterDTMetricTable(name, tmetricID, units);
        
        return stmt;
    } // createSpecialMetricTable

    /**
     *  This method alters an exisiting special metric database table to add new columns based on the MetricUnit
     *  
     * @param tablename
     * @param metricunit
     * @param units
     */
    private void alterDTMetricTable(String tablename, String metricunit, String[] units)
    {
        Statement stmt = null;
        SQLConnection conn = null;

        try
        {
            conn = SQL.getConnection();
            for (int i = 0; i < units.length; i++)
            {
                String fieldName = null;
                try
                {
                    stmt = conn.createStatement();
                    String sql = "";
                    sql += "alter table " + metricunit + tablename + "\n";
                    sql += "add (" + "\n";
                    
                    fieldName = units[i].trim().toLowerCase();
                    String fieldType = null;
                    int fieldLength = 0;
                    if(fieldName.contains(":string"))
                    {
                        fieldType = "VARCHAR";
                        //pull max field length value present within () in the units notation such as fieldname:fieldtype(fieldlength). 
                        //for example status:string(30)
                        fieldLength = Integer.valueOf(fieldName.substring(fieldName.indexOf("(") + 1, fieldName.indexOf(")"))); 
                        fieldName = fieldName.split(":")[0];
                        String fields = fieldName + " " + fieldType + "(" + fieldLength + ")";
                        sql += fields;
                        sql += ")" + "\n";
                    }
                    else
                    {
                        //default units are int fields
                        String fields = fieldName + " INT(11)" ;
                        sql += fields;
                        sql += ")" + "\n";
                    }
                    Log.log.trace(sql);
                    stmt.execute(sql);
                    stmt.close();
                }
                catch (SQLException addDBFieldError)
                {
                    mySQLErrorMsgs(metricunit, tablename, addDBFieldError);
                }
            }
        }
        catch (Throwable e)
        {
            Log.log.error(e.getMessage(), e);
        }
        finally
        {
            try
            {
                if (stmt != null)
                {
                    stmt.close();
                }
                if (conn != null)
                {
                    SQL.close(conn);
                }
            }
            catch (Exception e)
            {
                Log.log.error(e.getMessage(), e);
            }
        }
        
    }  // alterSpecialMetricTable

    /**
     *  This method checks the INIT flag from the rsmgmt config.xml and calls a method to create the metric tables
     *  
     * @param conn
     * @param name
     * @param units
     * @param init
     * @param drop
     * @throws SQLException
     */
    private void initializetables(SQLConnection conn, String name, String[] units, boolean init, boolean drop) throws SQLException
    {
        Statement stmt = null;
        if (init)
        {
            // METRIC_MIN_ table
            stmt = createRegularMetricTable(METRIC_MIN, conn, name, units, drop, stmt);

            // METRIC_HR_ table
            stmt = createRegularMetricTable(METRIC_HR, conn, name, units, drop, stmt);
            
            //METRIC_DAY_ table
            stmt = createRegularMetricTable(METRIC_DAY, conn, name, units, drop, stmt);
        }
        
        if (stmt != null) {
            try {
                stmt.close();
            } 
            catch (SQLException sqlEx) 
            {
                Log.log.error(sqlEx.getMessage(), sqlEx);
            } // ignore
        }
        
    } // initializetables

    /**
     *  This method creates the new metric tables (if not present) and alters the data of the existing table (if necessary)
     *   to add new columns or units to log the metrics that need to be reported
     *   
     * @param metricTableID: table name prefix 
      * @param conn: sql connection
     * @param name : table name
     * @param units: column names
     * @param drop 
     * @param stmt: sql statement
     * @return
     * @throws SQLException
     */
    private Statement createRegularMetricTable(String metricTableID, SQLConnection conn, String name, String[] units, boolean drop, Statement stmt) throws SQLException
    {
        String sql = "";
        
        if (drop)
        {
            sql += "DROP TABLE IF EXISTS " + metricTableID + name + "\n";
            Log.log.trace(sql);
            stmt = conn.createStatement();
            stmt.execute(sql);
            stmt.close();
        }

        // create empty metricTableID table
        try
        {
            sql = "";
            sql += "CREATE table " + metricTableID + name + "\n";
            sql += "(" + "\n";
            sql += "idx INTEGER DEFAULT NULL," + "\n";
            sql += "id VARCHAR(100)," + "\n";
            sql += "src VARCHAR(200)," + "\n";
            sql += "ts BIGINT(20)," + "\n";
            sql += "ts_datetime DATETIME," + "\n";
            sql += "UNIQUE UNQKEY (idx,src,id)," + "\n";
            sql += "KEY INDEXID (id)," + "\n";
            sql += "KEY INDEXSRC (src)" + "\n";
            sql += ")" + "\n";
            stmt = conn.createStatement();
            Log.log.trace(sql);
            stmt.execute(sql);
            stmt.close();
        }
        catch (SQLException createExistingTableError)
        {
            mySQLErrorMsgs(metricTableID, name, createExistingTableError);
        }

        // alter table for creating or adding new fields in metricTableID table
        alterTable(name, metricTableID, units);
        
        return stmt;
        
    } //createRegularMetricTable

    /**
     * This method logs all the info into the rsmgmt log file and throws all the errors as per mySQL error code 
     * 
     * @param tableID: metric table prefix
     * @param name: metric table name
     * @param error: exception
     */
    private void mySQLErrorMsgs(String tableID,  String name, SQLException error)
    {
        if(error.getErrorCode() == 1050) //Error: 1050 SQLSTATE: 42S01 (ER_TABLE_EXISTS_ERROR) - http://dev.mysql.com/doc/refman/5.1/en/error-messages-server.html#error_er_table_exists_error
        {
            Log.log.info("Table " + tableID + name + " already exists");
        }
        else if(error.getErrorCode() == 1060) //Error: 1060 SQLSTATE: 42S21 (ER_DUP_FIELDNAME)  - http://dev.mysql.com/doc/refman/5.1/en/error-messages-server.html#error_er_table_exists_error
        {
            Log.log.info("Duplicate column name in table " + tableID + name);
        }
        else
        {
            Log.log.error(error.getMessage(), error);
            Log.log.error("Error occurs for table " + tableID + name);
        }
        
    }  // mySQLErrorMsgs

    /**
     * This method alters an exisiting database table to add new columns based on the MetricUnit
     * 
     * @param tablename
     * @param metricunit
     * @param units
     */
    private void alterTable(String tablename, String metricunit, String[] units)
    {
        Statement stmt = null;
        SQLConnection conn = null;

        try
        {
            conn = SQL.getConnection();
            for (int i = 0; i < units.length; i++)
            {
                String fieldName = null;
                try
                {
                    fieldName = units[i].trim().toLowerCase();
                    stmt = conn.createStatement();
                    String sql = "";
                    sql += "alter table " + metricunit + tablename + "\n";
                    sql += "add (" + "\n";
                    String fields = "";
                    fields += "tot_" + fieldName + " INT(11)," + "\n";
                    fields += "cnt_" + fieldName + " INT(11)," + "\n";
                    fields += "del_" + fieldName + " INT(11)" + "\n";
                    sql += fields;
                    sql += ")" + "\n";
                    Log.log.trace(sql);
                    stmt.execute(sql);
                    stmt.close();
                }
                catch (SQLException addDBFieldError)
                {
                    mySQLErrorMsgs(metricunit, tablename, addDBFieldError);
                }
            }
        }
        catch (Throwable e)
        {
            Log.log.error(e.getMessage(), e);
        }
        finally
        {
            try
            {
                if (stmt != null)
                {
                    stmt.close();
                }
                if (conn != null)
                {
                    SQL.close(conn);
                }
            }
            catch (Exception e)
            {
                Log.log.error(e.getMessage(), e);
            }
        }

    } // alterTable
    
    public void updateDB(MetricType type, MetricCache cache, int idx)
    {
        SQLConnection conn = null;

        String tablePrefix = null;
        if (type == MetricType.MIN)
        {
            tablePrefix = "metric_min_";
        }
        else if (type == MetricType.HR)
        {
            tablePrefix = "metric_hr_";
        }
        else if (type == MetricType.DAY)
        {
            tablePrefix = "metric_day_";
        }

        try
        {
            conn = SQL.getConnection();
            
            //clean up the table for records with this idx --- TODO check may be a performance concern
            for (MetricMsg metric : cache.values())
            {
                String name = metric.getName().toLowerCase();
                //init delete stmt
                String tableName = HibernateUtil.getValidMetaData(tablePrefix + name.toLowerCase());
                String sql = "DELETE FROM " + tableName + " WHERE idx=?";
                Log.log.trace(sql);
                PreparedStatement deleteStmt = conn.prepareStatement(HibernateUtil.getValidQuery(sql));
                deleteStmt.setInt(1, idx);
                //delete the old entries
                deleteStmt.execute();
                deleteStmt.close();
            }

            long ts = System.currentTimeMillis();

            for (MetricMsg metric : cache.values())
            {
                String name = metric.getName().toLowerCase();

                // init prepared statements
                String sql = null;

                // init select stmt
                sql = "SELECT idx,id,src FROM " + tablePrefix + name.toLowerCase() + " WHERE idx=? AND id=? AND src=?";
                Log.log.trace(sql);
                PreparedStatement selectStmt = conn.prepareStatement(HibernateUtil.getValidQuery(sql));

                // init insert stmt
                String fields = "";
                String values = "";
                for(String keyName : metric.getUnits().keySet())
                {
                  fields += "," + HibernateUtil.getValidMetaData("tot_" + keyName); 
                  fields += "," + HibernateUtil.getValidMetaData("cnt_" + keyName);
                  fields += "," + HibernateUtil.getValidMetaData("del_" + keyName);
                  values += ", ?, ?, ?";
                    
                }

                sql = "INSERT INTO " + tablePrefix + name.toLowerCase() + " (idx,id,src,ts,ts_datetime" + fields + ") ";
                sql += "VALUES (?,?,?,?,?" + values + ")";
                Log.log.trace(sql);
                PreparedStatement insertStmt = conn.prepareStatement(HibernateUtil.getValidQuery(sql));

                // init update stmt
                fields = "ts=?,ts_datetime=?";
                
                for(String keyName : metric.getUnits().keySet())
                {
                    fields += "," + HibernateUtil.getValidMetaData("tot_" + keyName) + "=?";
                    fields += "," + HibernateUtil.getValidMetaData("cnt_" + keyName) + "=?";
                    fields += "," + HibernateUtil.getValidMetaData("del_" + keyName) + "=?";
                    
                }

                sql = "UPDATE " + tablePrefix + name.toLowerCase() + " SET " + fields + " ";
                sql += "WHERE idx=? AND id=? AND src=?";
                Log.log.trace(sql);
                PreparedStatement updateStmt = conn.prepareStatement(HibernateUtil.getValidQuery(sql));

                // check if row exists
                selectStmt.setInt(1, idx);
                selectStmt.setString(2, metric.getId());
                selectStmt.setString(3, metric.getSource());
                ResultSet rs = selectStmt.executeQuery();
                if (rs.next())
                {
                    // update row
                    int pos = 1;
                    updateStmt.setLong(pos++, ts);
                    updateStmt.setTimestamp(pos++, new Timestamp(GMTDate.getDate(ts).getTime()));
                    for(String keyName : metric.getUnits().keySet())
                    {
                      MetricUnit unitValue = metric.getUnits().get(keyName);
                      updateStmt.setLong(pos++, unitValue.getTotal());
                      updateStmt.setLong(pos++, unitValue.getCount());
                      updateStmt.setLong(pos++, unitValue.getDelta());
                        
                    }
                    
                    updateStmt.setInt(pos++, idx);
                    updateStmt.setString(pos++, metric.getId());
                    updateStmt.setString(pos++, metric.getSource());
                    updateStmt.executeUpdate();
                }
                else
                {
                    // insert row
                    int pos = 1;
                    insertStmt.setInt(pos++, idx);
                    insertStmt.setString(pos++, metric.getId());
                    insertStmt.setString(pos++, metric.getSource());
                    insertStmt.setLong(pos++, ts);
                    insertStmt.setTimestamp(pos++, new Timestamp(GMTDate.getDate(ts).getTime()));

                    for(String keyName : metric.getUnits().keySet())
                    {
                        MetricUnit unitValue = metric.getUnits().get(keyName);
                        insertStmt.setLong(pos++, unitValue.getTotal());
                        insertStmt.setLong(pos++, unitValue.getCount());
                        insertStmt.setLong(pos++, unitValue.getDelta());
                    }

                    insertStmt.execute();
                }
                
                if (rs != null)
                {
                    rs.close();
                }
                if (selectStmt != null)
                {
                    selectStmt.close();
                }
                if (insertStmt != null)
                {
                    insertStmt.close();
                }
                if (updateStmt != null)
                {
                    updateStmt.close();
                }
            }
        }
        catch (Throwable e)
        {
            Log.log.error(e.getMessage(), e);
        }
        finally
        {
            try
            {
                if (conn != null)
                {
                    SQL.close(conn);
                }
            }
            catch (Exception e)
            {
                Log.log.error(e.getMessage(), e);
            }
        }
    } // updateDB

    public void loadDB(ConfigMetric config)
    {
        SQLConnection conn = null;

        try
        {
            conn = SQL.getConnection();

            // load tables for each group
            for (Map<String, String> group : config.getGroups())
            {
                String type = group.get("TYPE").toLowerCase();
                if(type.equalsIgnoreCase("default"))
                {
                    String tablename = group.get("NAME").toLowerCase();
                    String unitsStr = group.get("UNITS").toLowerCase();
                    String[] units = unitsStr.split(",");
                    
                    String fields = "";
                    for (int i = 0; i < units.length; i++)
                    {
                        String fieldName = units[i].trim().toLowerCase();
                        
                        fields += "," + "tot_" + fieldName;
                        fields += "," + "cnt_" + fieldName;
                        fields += "," + "del_" + fieldName;
                    }
                    
                    /*
                     *  load min data into hr cache
                     */
                    String sql = "SELECT idx,id,src,ts"+fields+" FROM " + METRIC_MIN + tablename.toLowerCase() + " where (ts_datetime > (UTC_TIMESTAMP - interval 2 hour)) order by ts asc";
                    Log.log.trace(sql);
                    PreparedStatement selectStmt = conn.prepareStatement(sql);
                    
                    ResultSet rs = selectStmt.executeQuery();
                    while (rs.next())
                    {
                        HashMap<String, MetricUnit> metricUnits = new HashMap<String, MetricUnit>();
                        
                        // get values from db
                        String source = rs.getString("src");
                        String id = rs.getString("id");
                        long minTs = rs.getLong("ts");
                        for (int i = 0; i < units.length; i++)
                        {
                            String fieldName = units[i].trim().toLowerCase();
                            
                            long total = rs.getLong("tot_"+fieldName);
                            long count = rs.getLong("cnt_"+fieldName);
                            long delta = rs.getLong("del_"+fieldName);
                            
                            MetricUnit metricUnit = new MetricUnit(fieldName, total, count, delta);
                            // add into hashmap
                            metricUnits.put(fieldName, metricUnit);
                        }
                        
                        // FIXME - need to refactor mode into config file
                        MetricMsg metric = new MetricMsg(tablename, source, id, MetricMode.TOTAL, metricUnits);
                        
                        // add metric to cache
                        int hrIdx = TrendMetric.getHrIndex(minTs);
                        TrendMetric.updateCache(MetricType.HR, metric, hrIdx);
                    }
                    
                    if (rs != null)
                    {
                        rs.close();
                    }
                    if (selectStmt != null)
                    {
                        selectStmt.close();
                    }
                    
                    /*
                     *  load hr data into day cache
                     */
                    sql = "SELECT idx,id,src,ts"+fields+" FROM " + METRIC_HR + tablename.toLowerCase() + " where (ts_datetime > (UTC_TIMESTAMP - interval 2 day)) order by ts asc";
                    Log.log.trace(sql);
                    selectStmt = conn.prepareStatement(sql);
                    
                    rs = selectStmt.executeQuery();
                    while (rs.next())
                    {
                        HashMap<String, MetricUnit> metricUnits = new HashMap<String, MetricUnit>();
                        
                        // get values from db
                        String source = rs.getString("src");
                        String id = rs.getString("id");
                        long hrTs = rs.getLong("ts");
                        for (int i = 0; i < units.length; i++)
                        {
                            String fieldName = units[i].trim().toLowerCase();
                            
                            long total = rs.getLong("tot_"+fieldName);
                            long count = rs.getLong("cnt_"+fieldName);
                            long delta = rs.getLong("del_"+fieldName);
                            
                            MetricUnit metricUnit = new MetricUnit(fieldName, total, count, delta);
                            metricUnits.put(fieldName, metricUnit);
                        }
                        
                        // FIXME - need to refactor mode into config file
                        MetricMsg metric = new MetricMsg(tablename, source, id, MetricMode.TOTAL, metricUnits);
                        
                        // add metric to cache
                        int dayIdx = TrendMetric.getDayIndex(hrTs);
                        TrendMetric.updateCache(MetricType.DAY, metric, dayIdx);
                    }
                    
                    if (rs != null)
                    {
                        rs.close();
                    }
                    if (selectStmt != null)
                    {
                        selectStmt.close();
                    }
                }
            }
        }
        catch (Throwable e)
        {
            Log.log.error(e.getMessage(), e);
        }
        finally
        {
            try
            {
                if (conn != null)
                {
                    SQL.close(conn);
                }
            }
            catch (Exception e)
            {
                Log.log.error(e.getMessage(), e);
            }
        }
    } // loadDB
    
    /**
     * This method gets the database performance metrics for MySQL DB server from the 
     * INFORMATION_SCHEMA (information database that stores information about all the other databases
     * that MySQL server maintains)
     * 
     * It gets the following metrics
     * 1. Current size of DB in MB
     * 2. Free available space (space allocated on disk for, but not used by, a Disk Data table or fragment on disk) in MB
     * 3. Volume of DB used in Percentage(%)
     * 4. Number of queries executed in the current session
     * 5. Response Time of queries executed in the current session
     * 
     * @return <metric name, value> pairs 
     */
    public HashMap<String, Long> getDBMetrics()
    {
        SQLConnection conn = null;
        
        HashMap<String, Long> unitNameValue = new HashMap<String, Long>();
        String databasename = com.resolve.rsmgmt.Main.configSQL.getDbname();
        try
        {
            conn = SQL.getConnection();

            //Starting SQL Profiler built into the database server for response time
            Log.log.trace("set profiling=1");
            Statement setStmt = conn.createStatement();
            ResultSet rs = setStmt.executeQuery("set profiling=1");
            rs.close();
            setStmt.close();
            
            // Database wait time ratio = single query's wait time / wait time for all queries
            String sql = "";
            sql += "select sum(time) as total_time from information_schema.PROCESSLIST"; // get the wait time for all processes in this session
            Log.log.trace(sql);
            PreparedStatement stmt = conn.prepareStatement(sql);
            rs = stmt.executeQuery();

            double total_execution_time = 0;
            while (rs.next())
            {
                total_execution_time = rs.getDouble("total_time");
            }
            rs.close();
            stmt.close();
            
            if(total_execution_time != 0)
            {
                // total number of MySQL queries in the session
                sql = "select sum(variable_value) total_query_count from information_schema.GLOBAL_STATUS where VARIABLE_NAME IN ( 'Com_select', 'Com_update', 'Com_insert', 'Com_delete')";
                Log.log.trace(sql);
                stmt = conn.prepareStatement(sql);
                rs = stmt.executeQuery();
                double total_query_count = 0;
                while (rs.next())
                {
                    total_query_count = rs.getDouble("total_query_count");
                }
                rs.close();
                stmt.close();
                    
                double average_processing_time = Double.valueOf((total_execution_time / total_query_count)); //in millisecs
                long average_processing_time_long = (long) (average_processing_time * 1000); //convert to millisecs
                
                //single query execution time
                long starttime = System.currentTimeMillis();
                sql = "select * from " + databasename + ".resolve_registration";
                stmt = conn.prepareStatement(sql);
                long single_query_exectime = 0;
                if(!stmt.execute())
                {
                    single_query_exectime = 0L;
                }
                stmt.close();
            
                long endtime = System.currentTimeMillis();
                single_query_exectime = (endtime - starttime);
                long wait_time;
                try
                {
                    wait_time = (single_query_exectime / average_processing_time_long);
                }
                catch (ArithmeticException exc)
                {
                    Log.log.warn("The average_processing_time could be zero resulting in an arithmetic error");
                    wait_time = single_query_exectime;
                }
              
                long percentage_wait;
                if(wait_time >= 1)
                {
                    percentage_wait = 100;
                }
                else
                {
                    percentage_wait =  wait_time * 100;
                }
                unitNameValue.put("percentage_wait", percentage_wait);
            }
            else
            {
                unitNameValue.put("percentage_wait", 0L);
            }

            //Metrics- Volume of DB used and current size of the DB (sum of size of tables under this schema in MB)
            sql = "SELECT table_schema \"Database Name\", sum( data_length + index_length ) / (1024*1024) \"Size in MB\", " +
                            "sum( data_free ) / (1024*1024) \"Free Space in MB\", " +
                            "((sum(data_length + index_length) / (sum(data_length + index_length) + sum( data_free )))) \"Percentage Used\"" +
                            "FROM information_schema.TABLES where table_schema = '" + databasename + "'";
            Log.log.trace(sql);
            stmt = conn.prepareStatement(sql);
            rs = stmt.executeQuery();

            while (rs.next())
            {
                String dbname = rs.getString("Database Name");
                long free_space = rs.getLong("Free Space in MB"); // data_free column shows the free space in bytes for InnoDB tables. 
                                                                  // For MySQL Cluster, data_free shows the space allocated on disk for, but not used by, a Disk Data table or fragment on disk. 
                unitNameValue.put("free_space", free_space);
                long size = rs.getLong("Size in MB");
                unitNameValue.put("size", size);
                long percentage_used = rs.getLong("Percentage Used");
                if(percentage_used >= 1)
                {
                    percentage_used = 100;
                }
                else
                {
                    percentage_used =  percentage_used * 100;
                }
                unitNameValue.put("percentage_used", percentage_used);
                Log.log.trace("Free space: " + free_space + " size: " + size + " dbname: " + dbname + " percentage_used: " + percentage_used);
            }
            rs.close();
            stmt.close();

            // sum of the duration for all the queries executed to calculate average response time
            // Duration indicates how long statement execution remained in the given state, in seconds
            sql = "SELECT sum(duration) \"Response Time\" from information_schema.PROFILING";
            Log.log.trace(sql);
            stmt = conn.prepareStatement(sql);
            rs = stmt.executeQuery();
            while (rs.next())
            {
                double d = Double.parseDouble(rs.getString(1));
                long response_time = (long) (d * 1000); //convert to millisecs
                unitNameValue.put("response_time", response_time);
                Log.log.trace("Response time of Database: " + response_time + " msec.");
            }
            rs.close();
            stmt.close();
            
            // Count the number of queries in profiling
            sql = "SELECT count(*) query_count from information_schema.PROFILING where STATE='starting'";
            stmt = conn.prepareStatement(sql);
            rs = stmt.executeQuery();
            while (rs.next())
            {
                long query_count = rs.getLong("query_count");
                unitNameValue.put("query_count", query_count);
            }
            rs.close();
            stmt.close();

            // stopping SQL Profiler built into the database server for response time
            Log.log.trace("set profiling=0");
            setStmt = conn.createStatement();
            rs = setStmt.executeQuery("set profiling=0");
            rs.close();
            setStmt.close();
        }
        catch (Exception e)
        {
            Log.log.error(e.getMessage(), e);
        }
        finally
        {
            try
            {
                if (conn != null)
                {
                    SQL.close(conn);
                }
            }
            catch (Exception e)
            {
                Log.log.error(e.getMessage(), e);
            }
        }

        return unitNameValue;
    } // getDBMetrics

    /* (non-Javadoc)
     * @see com.resolve.rsmgmt.metric.MetricStore#updateDB(com.resolve.util.MetricType, java.util.List, java.lang.String[], int)
     * This method inserts a new record into the tmetric_hr_dt and tmetric_day_dt tables a list of records
     */
    @Override
    public void updateDB(MetricType type, List<DTRecord> dtrecords, String[] fieldNames, int idx, boolean deleteOldIndex)
    {
        SQLConnection conn = null;
        String tablename = "dt";

        String tablePrefix = null;
        if (type == MetricType.HR)
        {
            tablePrefix = "tmetric_hr_";
        }
        else if (type == MetricType.DAY)
        {
            tablePrefix = "tmetric_day_";
        }
        
        try
        {
            conn = SQL.getConnection();
            
            if(deleteOldIndex)
            //clean up the table for records with this idx --- TODO check may be a performance concern
            {
                //init delete stmt        
                String sql = "DELETE FROM " + tablePrefix + tablename.toLowerCase() + " WHERE idx=?";
                Log.log.trace(sql);
                PreparedStatement deleteStmt = conn.prepareStatement(sql);
                deleteStmt.setInt(1, idx);
                //delete the old entries
                deleteStmt.execute();
                deleteStmt.close();
            }
            
            long ts = System.currentTimeMillis();
            
            for(DTRecord dtrecord: dtrecords)
            {
                // init prepared statements
                String sql = null;
                
                // init select stmt
                sql = "SELECT idx,pathid,nodeid,seqid,userid FROM " + tablePrefix + tablename.toLowerCase() + " WHERE idx=? AND pathid=? AND nodeid=? AND seqid=? AND userid=?";
                Log.log.trace(sql);
                PreparedStatement selectStmt = conn.prepareStatement(sql);

                // init insert stmt
                String fields = "";
                String values = "";
                for(String fieldName : fieldNames)
                {
                    if(fieldName.contains(":string"))
                    {
                        fieldName = fieldName.split(":")[0];
                    }
                    fields += "," + fieldName; 
                    values += ", ?";
                }
                sql = "INSERT INTO " + tablePrefix + tablename.toLowerCase() + " (idx,pathid,nodeid,seqid,userid,ts,ts_datetime" + fields + ") ";
                sql += "VALUES (?,?,?,?,?,?,?" + values + ")";
                Log.log.trace(sql);
                PreparedStatement insertStmt = conn.prepareStatement(sql);
                
                // init update stmt
                fields = "ts=?,ts_datetime=?";
                
                for(String fieldName : fieldNames)
                {
                    if(fieldName.contains(":string"))
                    {
                        fieldName = fieldName.split(":")[0];
                    }
                    fields += "," + fieldName; 
                }
                sql = "UPDATE " + tablePrefix + tablename.toLowerCase() + " SET " + fields + " ";
                sql += "WHERE idx=? AND pathid=? AND nodeid=? AND seqid=? AND userid=?";
                Log.log.trace(sql);
                PreparedStatement updateStmt = conn.prepareStatement(sql);
                
                // check if row exists
                selectStmt.setInt(1, idx);
                selectStmt.setString(2, dtrecord.getPathID());
                selectStmt.setString(3, dtrecord.getNodeID());
                selectStmt.setLong(4, dtrecord.getSequenceID());
                selectStmt.setString(5, dtrecord.getUser());
                ResultSet rs = selectStmt.executeQuery();
                if (rs.next())
                {
                    // update row
                    int pos = 1;
                    updateStmt.setLong(pos++, ts);
                    updateStmt.setTimestamp(pos++, new Timestamp(GMTDate.getDate(ts).getTime()));
                    updateStmt.setLong(pos++, dtrecord.getNodeCount());
                    updateStmt.setLong(pos++, dtrecord.getPathCount());
                    updateStmt.setLong(pos++, dtrecord.getTotalTime());
                    updateStmt.setString(pos++, dtrecord.getStatus());
                    updateStmt.setString(pos++, dtrecord.getNamespace());
                    
                    updateStmt.setInt(pos++, idx); //slotid
                    updateStmt.setString(pos++, dtrecord.getPathID()); //pathid
                    updateStmt.setString(pos++, dtrecord.getNodeID()); //nodeid
                    updateStmt.setLong(pos++, dtrecord.getSequenceID()); //sequence id
                    updateStmt.setString(pos++, dtrecord.getUser()); //userid
                    
                    updateStmt.executeUpdate();
                }
                else
                {
                    // insert row
                    int pos = 1;
                    insertStmt.setInt(pos++, idx);
                    insertStmt.setString(pos++, dtrecord.getPathID());
                    insertStmt.setString(pos++, dtrecord.getNodeID());
                    insertStmt.setLong(pos++, dtrecord.getSequenceID());
                    insertStmt.setString(pos++, dtrecord.getUser());
                    insertStmt.setLong(pos++, ts);
                    insertStmt.setTimestamp(pos++, new Timestamp(GMTDate.getDate(ts).getTime()));
                    
                    insertStmt.setLong(pos++, dtrecord.getNodeCount());
                    insertStmt.setLong(pos++, dtrecord.getPathCount());
                    insertStmt.setLong(pos++, dtrecord.getTotalTime());
                    insertStmt.setString(pos++, dtrecord.getStatus());
                    insertStmt.setString(pos++, dtrecord.getNamespace());
                    
                    insertStmt.execute();
                }
                
                if (rs != null)
                {
                    rs.close();
                }
                if (selectStmt != null)
                {
                    selectStmt.close();
                }
                if (insertStmt != null)
                {
                    insertStmt.close();
                }
                if (updateStmt != null)
                {
                    updateStmt.close();
                }
            }
        }
        catch (Throwable e)
        {
            Log.log.error(e.getMessage(), e);
        }
        finally
        {
            try
            {
                if (conn != null)
                {
                    SQL.close(conn);
                }
            }
            catch (Exception e)
            {
                Log.log.error(e.getMessage(), e);
            }
        }
        
    } // updateDB

    /* (non-Javadoc)
     * @see com.resolve.rsmgmt.metric.MetricStore#loadTMetricDB(com.resolve.rsmgmt.ConfigMetric, int)
     */
    @Override
    public void loadTMetricDB(ConfigMetric config, int idx)
    {
        SQLConnection conn = null;

        try
        {
            conn = SQL.getConnection();

            // load tables for each group
            for (Map<String, String> group : config.getGroups())
            {
                String type = group.get("TYPE").toLowerCase();
                if(type.equalsIgnoreCase("decisiontree"))
                {
                    String tablename = group.get("NAME").toLowerCase();
                    String unitsStr = group.get("UNITS").toLowerCase();
                    String[] units = unitsStr.split(",");
                    
                    //clean up the table for records with this idx --- TODO check may be a performance concern
                    //init delete stmt        
                    String sql = "DELETE FROM " + TMETRIC_DAY + tablename.toLowerCase() + " WHERE idx=?";
                    Log.log.trace(sql);
                    PreparedStatement deleteStmt = conn.prepareStatement(sql);
                    deleteStmt.setInt(1, idx);
                    //delete the old entries
                    deleteStmt.execute();
                    deleteStmt.close();

                    // init insert stmt
                    String fields = "";
                    String values = "";
                    for(String fieldName : units)
                    {
                        if(fieldName.contains(":string"))
                        {
                            fieldName = fieldName.split(":")[0];
                        }
                        fields += "," + fieldName; 
                        values += ", ?";
                    }
                    
                    /*
                     *  load hr data from hr table into day table
                     */
                    sql = "SELECT pathid, nodeid, seqid, userid, node_cnt, sum(tot_cnt) as tot_cnt, sum(tot_time) as tot_time, status, dtnamespace from " +  TMETRIC_HR + tablename.toLowerCase() + 
                                    " where (ts_datetime > (UTC_TIMESTAMP - INTERVAL '1' day)) group by pathid, nodeid,seqid,userid order by ts asc";
                    Log.log.trace(sql);
                    PreparedStatement selectStmt = conn.prepareStatement(sql);

                    ResultSet rs = selectStmt.executeQuery();
                    long ts = System.currentTimeMillis();
                    while(rs.next())
                    {
                        // init insert stmt
                        sql = "INSERT INTO " + TMETRIC_DAY + tablename.toLowerCase() + " (idx,pathid,nodeid,seqid,userid,ts,ts_datetime" + fields + ") ";
                        sql += "VALUES (?,?,?,?,?,?,?" + values + ")";
                        Log.log.trace(sql);
                        PreparedStatement insertStmt = conn.prepareStatement(sql);
                        // insert row
                        int pos = 1;
                        insertStmt.setInt(pos++, idx); // day index
                        insertStmt.setString(pos++, rs.getString("pathid")); //pathID
                        insertStmt.setString(pos++, rs.getString("nodeid")); // node or wiki name
                        insertStmt.setLong(pos++, rs.getLong("seqid")); // sequence id
                        insertStmt.setString(pos++, rs.getString("userid")); // user name
                        insertStmt.setLong(pos++, ts);
                        insertStmt.setTimestamp(pos++, new Timestamp(GMTDate.getDate(ts).getTime()));
                        
                        insertStmt.setLong(pos++, rs.getLong("node_cnt")); //nodes in a path
                        insertStmt.setLong(pos++, rs.getLong("tot_cnt")); //path count
                        insertStmt.setLong(pos++, rs.getLong("tot_time")); //total time 
                        insertStmt.setString(pos++, rs.getString("status")); //status i.e. completed or aborted
                        insertStmt.setString(pos++, rs.getString("dtnamespace"));//namespace
                        
                        insertStmt.execute();
                        
                        if (insertStmt != null)
                        {
                            insertStmt.close();
                        }
                    }
                    
                    if (rs != null)
                    {
                        rs.close();
                    }
                    if (selectStmt != null)
                    {
                        selectStmt.close();
                    }
                }
            }
        }
        catch (Throwable e)
        {
            Log.log.error(e.getMessage(), e);
        }
        finally
        {
            try
            {
                if (conn != null)
                {
                    SQL.close(conn);
                }
            }
            catch (Exception e)
            {
                Log.log.error(e.getMessage(), e);
            }
        }
        
    } // loadTMetricDB
    

    /* (non-Javadoc)
     * @see com.resolve.rsmgmt.metric.MetricStore#getAllDTPathIDs()
     */
    @Override
    public List<String> getAllDTPathIDs()
    {
        String sql = "select u_path_id from tmetric_lookup";
        List<String> result = new ArrayList<String>();
        
        SQLConnection connection = null;
        ResultSet rs = null;
        Statement statement = null;
        
        try
        {
            connection = SQL.getConnection();
            statement = connection.createStatement();
            rs = statement.executeQuery(sql);

            // while loop to get the data
            while (rs.next())
            {
                result.add(rs.getString("u_path_id"));
            }// end of while loop
        }
        catch (Throwable err)
        {
            Log.log.error("Error in fetching data for :" + sql, err);
            throw new RuntimeException(err.getMessage());
        }
        finally
        {
            try
            {
                if (rs != null)
                {
                    rs.close();
                }

                if (statement != null)
                {
                    statement.close();
                }

                if (connection != null)
                {
                    connection.close();
                }
            }
            catch (Throwable err)
            {
                Log.log.error("Error in fetching data for :" + sql, err);
                throw new RuntimeException(err.getMessage());
            }
        }
        
        return result;
        
    } // getAllDTPathIDs

    /* (non-Javadoc)
     * @see com.resolve.rsmgmt.metric.MetricStore#insertIntoLookUpTable(java.lang.String, java.lang.String, java.lang.String)
     */
    @Override
    public void insertIntoLookUpTable(String pathID, String path, String username, String dtRootDoc)
    {
        SQLConnection connection = null;
        PreparedStatement statement = null;
        String sql = "insert into tmetric_lookup(sys_id, u_path_id, u_full_path, u_dt_root_doc, sys_created_by, sys_updated_by, sys_created_on, sys_updated_on) values " +
                        "(?,?,?,?,?,?,?,?)";
        
        long ts = System.currentTimeMillis();
        int pos = 1;
        try
        {
            connection = SQL.getConnection();
            Log.log.debug("Executing SQL :\n" + sql);
            connection.getConnection().setAutoCommit(false);
            statement = connection.prepareStatement(sql);
            statement.setString(pos++, SysId.getSysId());
            statement.setString(pos++, pathID);
            statement.setString(pos++, path);
            statement.setString(pos++, dtRootDoc);
            statement.setString(pos++, username);
            statement.setString(pos++, username);
            statement.setTimestamp(pos++, new Timestamp(GMTDate.getDate(ts).getTime()));
            statement.setTimestamp(pos++, new Timestamp(GMTDate.getDate(ts).getTime()));
            statement.executeUpdate();
            connection.commit();
        }
        catch (Throwable t)
        {
            try
            {
                connection.getConnection().rollback();
            }
            catch (SQLException e)
            {
                Log.log.error("Rollback call is ignored: ", e);
                throw new RuntimeException(e.getMessage());
            }

            Log.log.error("Error in fetching data for :" + sql, t);
            throw new RuntimeException(t.getMessage());
        }
        finally
        {
            try
            {
                if (statement != null)
                {
                    statement.close();
                }

                if (connection != null)
                {
                    connection.close();
                }
            }
            catch (Throwable t)
            {
                Log.log.error("Error in closing statement or sql connection: ", t);
                throw new RuntimeException(t.getMessage());
            }
        }
    } // insertIntoLookUpTable

} // MetricStoreMySQL
