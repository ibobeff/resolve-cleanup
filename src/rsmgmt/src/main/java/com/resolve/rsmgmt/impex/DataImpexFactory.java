/******************************************************************************
* (C) Copyright 2014
*
* Resolve Systems, LLC
*
* All rights reserved
* This software is distributed under license and may not be used, copied,
* modified, or distributed without the express written permission of
* Resolve Systems, LLC.
*
******************************************************************************/

package com.resolve.rsmgmt.impex;

import com.resolve.sql.SQLConnection;

public class DataImpexFactory
{
    public static DataImpex getDataImport(String dbType, SQLConnection sqlConnection, DataParser dataParser)
    {
        DataImpex result = null;
        
        if("MYSQL".equalsIgnoreCase(dbType))
        {
            result = new DataImpexMySql(sqlConnection, dataParser);
        }
        if(dbType.toUpperCase().startsWith("ORACLE"))
        {
            result = new DataImpexOracle(sqlConnection, dataParser);
        }
        return result;
    }
}
