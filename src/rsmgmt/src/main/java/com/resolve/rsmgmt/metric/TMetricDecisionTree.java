/******************************************************************************
* (C) Copyright 2014
*
* Resolve Systems, LLC
*
* All rights reserved
* This software is distributed under license and may not be used, copied,
* modified, or distributed without the express written permission of
* Resolve Systems, LLC.
*
******************************************************************************/

package com.resolve.rsmgmt.metric;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.concurrent.ConcurrentHashMap;

import com.resolve.util.CryptMD5;
import com.resolve.util.GMTDate;
import com.resolve.util.Log;
import com.resolve.util.StringUtils;
import com.resolve.util.TMetricCache;
import com.resolve.vo.TNodeVO;

/**
 * @author alokika.dash
 *
 */
public class TMetricDecisionTree implements TMetric
{
    int slotID;
    
    public TMetricDecisionTree(int slotID)
    {
        this.slotID = slotID;
    } // TMetricDecisionTree
    
    @Override
    @SuppressWarnings("rawtypes")
    public ConcurrentHashMap<String, List<DTRecord>> analyzeCache(TMetricCache tmcache, MetricStore store)
    {
        // create a local copy/snapshot of the TMetricCache
        ConcurrentHashMap<String, List<TNodeVO>> tmcachecopy = new ConcurrentHashMap<String, List<TNodeVO>>(tmcache);
        // create a map of execution IDs and the list of records to be inserted to Database
        ConcurrentHashMap<String, List<DTRecord>> dbRecordsPerExecution = null;
        // local copy of paths that are present in the TMetric lookup database
        List<String> pathIDs = store.getAllDTPathIDs();
        
        // create a list of records that need to be inserted into the DB if items are present 
        if(tmcachecopy.keySet().size() > 0)
        {
            dbRecordsPerExecution = new ConcurrentHashMap<String, List<DTRecord>>();

            //loop through entire in-memory cache
            Iterator it = tmcachecopy.keySet().iterator();
            while(it.hasNext()) 
            {
                // Get the executionID and list of nodes in the path
                String executionID = (String)it.next();
                List<TNodeVO> listOfNodes = tmcachecopy.get(executionID);
                // For each executionID do the following
                if(listOfNodes != null)
                {
                    //DT is infinite and cannot be finished as now it can start where it is left. So get the status for this execution                    
                    String status = getStatus(listOfNodes, false);
                    
                    if(StringUtils.isNotBlank(status))
                    {
                        // traverse all the nodes traversed in this execution to generate pathID
                        String path = getPath(listOfNodes);
                        
                        // generate a unique checksum for this path
                        String checkSumForPathID = CryptMD5.encrypt(path);
                        
                        // get user name of the current executionID
                        String currentUserName = listOfNodes.get(0).getUsername();
                        
                        //get the root dt
                        String dtRootDoc = listOfNodes.get(0).getRootDtFullName();
                        
                        // get namespace of the current executionID, i.e., namespace of the root decision tree 
                        String namespace = dtRootDoc.split("\\.")[0];//getNamespace(listOfNodes);
                        
                        //prepare the list of node names
                        List<String> nodeNames = getNodeNames(listOfNodes);
                        
                        //get the corresponding node timings
                        List<Long> nodeTimings = getNodeTimings(listOfNodes);
                        
                        // Validation Note: number of elements in lists nodeNames and NodeTimings should be same
                        if(nodeNames.size() != nodeTimings.size())
                        {
                            Log.log.debug("Error in generating the node name and node time mappings when analyzing tmetric cache", new Throwable());
                        }
                        
                        //list of records for the DB tables
                        List<DTRecord> listOfRecords = new ArrayList<DTRecord>();
                        
                        // generate new records from this execution's list of nodes
                        checkAndCreateNewDBRecords(listOfRecords, listOfNodes, status, nodeNames, nodeTimings, checkSumForPathID, currentUserName, namespace);
                        
                        // add the checkSum and pathID to the lookup table if not present
                        if(!pathIDs.contains(checkSumForPathID))
                        {
                            createNewEntryInTable(store, checkSumForPathID, path, currentUserName, dtRootDoc);
                            pathIDs.add(checkSumForPathID);
                        }
                        
                        // Insert records into recordsPerExecution
                        if(listOfRecords.size() > 0)
                        {
                            dbRecordsPerExecution.put(executionID, listOfRecords);
                        }
                    }//end of if
                    
                }
            } // end of while
        } //end of if
        return dbRecordsPerExecution;
    } // analyzeCache

    /**
     * This method generates a list for time spent in each node during the traversal of decision trees
     * reported by the rsview's viewcontroller per execution  
     * @param listOfNodes
     * @return
     */
    private List<Long> getNodeTimings(List<TNodeVO> listOfNodes)
    {
        List<Long> nodeTimings = new ArrayList<Long>();
        if(listOfNodes != null && (listOfNodes.size() > 0))
        {
            for (TNodeVO dtnode : listOfNodes)
            {
                nodeTimings.add(dtnode.getNodeTimeInMS());
            }
            
            //for the last node ...as we add the root node at the beginning, last node will always have 0 secs 
            nodeTimings.add(0L);
        }
        
        return nodeTimings;
        
    } // getNodeTimings

    
    /**
     * This method generates a list for decision tree node names during the traversal of decision trees
     * reported by the rsview's viewcontroller per execution  
     * @param listOfNodes
     * @return
     */
    private List<String> getNodeNames(List<TNodeVO> listOfNodes)
    {
        List<String> nodeNames = new ArrayList<String>();
        
        if(listOfNodes != null && (listOfNodes.size() > 0))
        {
            int count = 0;
            for(TNodeVO dtnode : listOfNodes)
            {
                //start with the root DT
                if(count == 0)
                {
                    nodeNames.add(dtnode.getRootDtFullName());
                }
                
                nodeNames.add(dtnode.getCurrWikiFullName());
                count++;
            }
        }
        
        return nodeNames;
        
    } // getNodeNames

    /**
     * @param listOfNodes
     * @return
     */
    private String getPath(List<TNodeVO> listOfNodes)
    {
        StringBuilder path = new StringBuilder();
        if(listOfNodes != null && (listOfNodes.size() > 0))
        {
            int count = 0;
            
            for(TNodeVO node : listOfNodes)
            {
                //start with the root DT
                if(count == 0)
                {
                    path.append(node.getRootDtFullName()).append("||");
                }
                
                path.append(node.getCurrWikiFullName());
                if(count != (listOfNodes.size()-1))
                {
                    path.append("||");
                }
                
                count++;
            }//end of for loop
        }
        
        assert StringUtils.isNotEmpty(path);
        return path.toString();
    } // getPath

    /**
     * @param listOfRecords
     * @param listOfNodes
     * @param status
     * @param nodeNameAndNodeTime
     * @param checkSumForPathID
     * @param currentUserName
     */
    private void checkAndCreateNewDBRecords(List<DTRecord> listOfRecords, List<TNodeVO> listOfNodes, String status, List<String> nodeNames, List<Long> nodeTimings, String pathID, String currentUserName, String namespace)
    {
        // create new records if they donot exist in list of records
        if(!lookUpPathID(listOfRecords, pathID))
        {
            createNewRecords(listOfRecords, currentUserName, status, nodeNames, nodeTimings, pathID, namespace);
        }
        else
        {
            //If record exists for same user, increment the path count of the node id
            for(DTRecord dtrecord: listOfRecords)
            {
                if(dtrecord.getPathID().equalsIgnoreCase(pathID))
                {
                    String userName = dtrecord.getUser();
                    if(currentUserName.equalsIgnoreCase(userName))
                    {
                        //increment the path count of the node id
                        long newPathCount = dtrecord.getPathCount() + 1;
                        dtrecord.setPathCount(newPathCount);
                    }
                }
            }
            
            //If record exists for same user, increment the total node time of the node id
            boolean recordMatches = incrementNodeTime(listOfRecords, listOfNodes, pathID, currentUserName);
            //If the username of listofNodes does not match anything in the list of records
            if(!recordMatches)
            {
                // create a new record for this user
                createNewRecords(listOfRecords, currentUserName, status, nodeNames, nodeTimings, pathID, namespace);
            }
        }
    } // checkAndCreateNewDBRecords


    /**
     * This method manipulates indexes in the listofNodes and listofrecords to track if node time needs to be incremented
     * for duplicate occurances of DT path 
     * @param listOfRecords
     * @param listOfNodes
     * @param pathID
     * @param currentUserName
     * @return
     */
    private boolean incrementNodeTime(List<DTRecord> listOfRecords, List<TNodeVO> listOfNodes, String pathID, String currentUserName)
    {
        int indexInListofRecords = 0; //tracks the index in the listOfRecords.
        int startIndexOfRecords = 0; //tracks the index of first occurance in listOfRecords when there is a match of user name.
        int indexOfNode = 0; //tracks the index in the listOfNodes.
        boolean isFirstRecord = true;
        boolean recordMatches = false; // variable that tracks same pathID and same user if for current execution
        for(DTRecord dtrecord: listOfRecords)
        {
            if(dtrecord.getPathID().equalsIgnoreCase(pathID))
            {
                String userName = dtrecord.getUser();
                
                if(currentUserName.equalsIgnoreCase(userName)) //found user with exact same path of DT traversal
                {
                    long newNodeTime = 0L;
                    String wikiName = ""; //reset the wiki visited in the path
                    if(isFirstRecord)
                    {
                        // keep track of the start index in the list of records
                        isFirstRecord = false;
                        startIndexOfRecords = indexInListofRecords;
                    }
                    //continue comparing the node name as long as all elements in the listOfNodes are not visited
                    if(indexOfNode < listOfNodes.size())
                    {
                        wikiName = listOfNodes.get(indexOfNode).getWikiFullName();
                    }
                    
                    //increment nodeTime if decision tree name in the listOfRecords matches the decision tree name in the listOfNodes 
                    String dtName = dtrecord.getNodeID();
                    if(dtName.equalsIgnoreCase(wikiName))
                    {
                        newNodeTime = dtrecord.getTotalTime() + listOfNodes.get(indexOfNode).getNodeTimeInMS();
                        indexOfNode++;
                    }
                    else
                    {
                        //else keep the old value of node time
                        newNodeTime += dtrecord.getTotalTime();
                    }
                    dtrecord.setTotalTime(newNodeTime);
                    
                    int numRecordsTraversedInThisLoop = (indexInListofRecords + 1) - startIndexOfRecords;
                    //break from loop if all nodes in the path are covered
                    if(numRecordsTraversedInThisLoop  == dtrecord.getNodeCount())
                    {
//                        //reset all variables
//                        numListOfRecords = 0;
//                        startIndexOfNodes = 0;
//                        startIndexOfRecords = 0;
//                        isFirstRecord = true;
//                        indexInListofRecords = 0;
                        // same pathID and same user present in the list of Records
                        recordMatches = true;
                        break;
                    }
                }
            }
            indexInListofRecords++;
        }
        return recordMatches;
    } // incrementNodeTime

    /**
     * This method adds a new row into tmetric_lookup database
     * @param pathID
     * @param path
     */
    private void createNewEntryInTable(MetricStore store, String pathID, String path, String user, String dtRootDoc)
    {
        store.insertIntoLookUpTable(pathID, path, user, dtRootDoc);
    } // createNewEntryInTable

    /**
     * This method checks if a path exists in the list of records that is to be inserted 
     * into the database
     * @param listOfRecords
     * @param pathID
     * @return
     */
    private boolean lookUpPathID(List<DTRecord> listOfRecords, String pathID)
    {
        boolean isPresent = false;
        for(DTRecord dtrecord: listOfRecords)
        {
            if(dtrecord.getPathID().equalsIgnoreCase(pathID))
            {
                isPresent = true;
                break;
            }
        }
        return isPresent;
    } // lookUpPathID
    
    /**
     * This method creates a new record to be inserted into the database
     * @param listOfRecords
     * @param listOfNodes
     * @param status
     * @param nodeNameAndTimePerNode
     * @param checkSumForPathID
     */
    private void createNewRecords(List<DTRecord> listOfRecords, String userName, String status, List<String> nodeNames, List<Long> nodeTimings, String pathID, String namespace)
    {
        // For each node in the path traversed
        // create a new record to be inserted into the DB 
        // add this record to the list of records
        for (int i = 0; i < nodeNames.size(); i++) 
        {
            DTRecord dtrecord = new DTRecord();
            dtrecord.setSlotIDX(slotID);
            dtrecord.setPathID(pathID);
            String dtNodeName = nodeNames.get(i);
            dtrecord.setNodeID(dtNodeName);
            long ts = System.currentTimeMillis();
            dtrecord.setTs(ts);
            Timestamp ts_date = new Timestamp(GMTDate.getDate(ts).getTime()); 
            dtrecord.setTs_datetime(ts_date);
            dtrecord.setNodeCount(nodeNames.size());
            dtrecord.setSequenceID(i+1);
            dtrecord.setPathCount(1);
            dtrecord.setTotalTime(nodeTimings.get(i));
            dtrecord.setStatus(status);
            dtrecord.setUser(userName);
            dtrecord.setNamespace(namespace);
            listOfRecords.add(dtrecord);
        }
    } // createNewRecords

    /**
     *  This method gives the status of a single execution i.e. a single traversal of a decision tree
     *  If status is null, then the current execution has not timed out.
     * @param listofnodes: list of all nodes in one execution
     * @param isFinished: true if the entire decision tree execution is finished, else false
     * @return status: status based on leaf node, status == null only if the execution has not timed out
     */
    private String getStatus(List<TNodeVO> listofnodes, boolean isFinished)
    {
        String status = null;
        if (isFinished)
        {
            status = "COMPLETED";
        }
        else
        {
           if(hasTimedOut(listofnodes))
           {
               if(isLeafNode(listofnodes))
               {
                   status = "COMPLETED";
               }
               else
               {
                   status = "ABORTED";
               }
           }
        }
        return status;
    } // getStatus
    
    /**
     * @param listOfNodes
     * @return
     */
    private boolean isLeafNode(List<TNodeVO> listOfNodes)
    {
        boolean isLeafNode = (listOfNodes.get(listOfNodes.size() - 1)).getIsLeaf();
        return isLeafNode;
    } // isLeafNode
    
    /**
     * This method checks if a execution has timed out
     * @param listOfNodes
     * @return
     */
    private boolean hasTimedOut(List<TNodeVO> listOfNodes)
    {
        boolean hasTimedOut = false;
        long endtime = (listOfNodes.get(listOfNodes.size() - 1)).getStopTimeInMS();
        long node_time_out = (listOfNodes.get(listOfNodes.size() - 1)).getAbortTimeInSecs() * 1000; //convert to millisecs
//        long node_time_out = (listOfNodes.get(listOfNodes.size() - 1)).getAbortTimeInSecs(); //NOTE: for testing
        if((System.currentTimeMillis() - endtime) > node_time_out)
        {
            hasTimedOut = true;
            System.out.println("DT has timed out");
            Log.log.debug("DT has timed out");
        }
        return hasTimedOut;
    } // hasTimedOut
    
} // TMetricDecisionTree
