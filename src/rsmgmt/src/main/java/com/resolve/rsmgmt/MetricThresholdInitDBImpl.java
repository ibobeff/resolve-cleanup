/******************************************************************************
* (C) Copyright 2014
*
* Resolve Systems, LLC
*
* All rights reserved
* This software is distributed under license and may not be used, copied,
* modified, or distributed without the express written permission of
* Resolve Systems, LLC.
*
******************************************************************************/

package com.resolve.rsmgmt;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.resolve.services.ServiceHibernate;
import com.resolve.services.ServiceJDBC;
import com.resolve.services.hibernate.vo.MetricThresholdVO;
import com.resolve.services.vo.QueryDTO;
import com.resolve.util.MetricThresholdInit;
import com.resolve.util.MetricThresholdType;

public class MetricThresholdInitDBImpl implements MetricThresholdInit
{
    @Override
    public Map<String, MetricThresholdType> init()
    {
        Map<String, MetricThresholdType> result = new HashMap<String, MetricThresholdType>();
        
        QueryDTO query = new QueryDTO();
        query.setTableName("metric_threshold");
        query.setWhereClause(" u_guid='' or u_guid is null");
        query.setUseSql(true);
        List<MetricThresholdVO> data = ServiceJDBC.getMetricThreshold(query);
        for(MetricThresholdVO metric : data)
        {
            ServiceHibernate.processThresholdValuesInMap(metric, result);
        }
        
        return result;
    } 
} // MetricThresholdInitImpl 
