/******************************************************************************
* (C) Copyright 2014
*
* Resolve Systems, LLC
*
* All rights reserved
* This software is distributed under license and may not be used, copied,
* modified, or distributed without the express written permission of
* Resolve Systems, LLC.
*
******************************************************************************/

package com.resolve.rsmgmt.metric;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.sql.Timestamp;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.resolve.persistence.util.HibernateUtil;
import com.resolve.rsmgmt.ConfigMetric;
import com.resolve.sql.SQL;
import com.resolve.sql.SQLConnection;
import com.resolve.util.GMTDate;
import com.resolve.util.Log;
import com.resolve.util.MetricCache;
import com.resolve.util.MetricMode;
import com.resolve.util.MetricMsg;
import com.resolve.util.MetricType;
import com.resolve.util.MetricUnit;

public class MetricStoreDB2 implements MetricStore
{
    public void createTables(ConfigMetric config)
    {
        SQLConnection conn = null;
        Statement stmt = null;
        
        try
        {
	        conn = SQL.getConnection();
	        
	        // create tables for each group
	        for (Map<String,String> group : config.getGroups())
	        {
		        String name = group.get("NAME").toUpperCase();
		        String unitsStr = group.get("UNITS").toUpperCase();
		        String[] units = unitsStr.split(",");
		        
		        boolean init = false;
		        String initStr = group.get("INIT"); 
		        if (initStr != null && initStr.equalsIgnoreCase("true"))
		        {
		            init = true;
		        }
		        
		        boolean drop = false;
		        String dropStr = group.get("DROP"); 
		        if (dropStr != null && dropStr.equalsIgnoreCase("true"))
		        {
		            drop = true;
		        }
		        
		        if (init)
		        {
			        String sql = "";
			        
			        // METRIC_MIN_ table
			        if (drop)
			        {
			            try
			            {
					        sql += "DROP TABLE METRIC_MIN_"+name+"\n";
					        stmt = conn.createStatement();
					        Log.log.trace(sql);
					        stmt.execute(sql);
					        stmt.close();
			            }
			            catch (Throwable e) { }
			        }
			        
			        // create empty METRIC_MIN_ table 
			        try {
			            sql = "";
			            sql += "CREATE TABLE METRIC_MIN_"+name+"\n";
			            sql += "("+"\n";
			            sql += "IDX INTEGER DEFAULT NULL,"+"\n";
			            sql += "ID VARCHAR(100),"+"\n";
			            sql += "SRC VARCHAR(200),"+"\n";
//			            sql += fields;
			            sql += "TS BIGINT,"+"\n";
			            sql += "TS_DATETIME TIMESTAMP"+"\n";
			            sql += ")"+"\n";
			            stmt = conn.createStatement();
			            Log.log.trace(sql);
			            stmt.execute(sql);
			            stmt.close();

			            sql = "";
			            sql += "CREATE INDEX MIN_UNQIDX_"+name+" ON METRIC_MIN_"+name+" ( IDX, ID, SRC )\n";
			            stmt = conn.createStatement();
			            stmt.execute(sql);
			            Log.log.trace(sql);
			            stmt.close();

			            sql = "";
			            sql += "CREATE INDEX MIN_INDEXID_"+name+" ON METRIC_MIN_"+name+" ( ID )\n";
			            stmt = conn.createStatement();
			            stmt.execute(sql);
			            Log.log.trace(sql);
			            stmt.close();

			            sql = "";
			            sql += "CREATE INDEX MIN_INDEXSRC_"+name+" ON METRIC_MIN_"+name+" ( SRC )\n";
			            stmt = conn.createStatement();
			            Log.log.trace(sql);
			            stmt.execute(sql);
			            stmt.close();
			        } 
			        catch(SQLException createExistingTableError)
			        {
			            Log.log.error(createExistingTableError.getMessage(), createExistingTableError);
			        }

                    // alter table for creating or adding new fields in METRIC_MIN_ table
                    alterTable(name, "METRIC_MIN_", units);
			        
			        // METRIC_HR_ table
			        if (drop)
			        {
			            try
			            {
					        sql = "";
					        sql += "DROP TABLE METRIC_HR_"+name+"\n";
					        stmt = conn.createStatement();
					        Log.log.trace(sql);
					        stmt.execute(sql);
					        stmt.close();
			            }
			            catch (Throwable e) { }
			        }
			        
                    // create empty METRIC_HR_ table 
			        try {
			            sql = "";
			            sql += "CREATE TABLE METRIC_HR_"+name+"\n";
			            sql += "("+"\n";
			            sql += "IDX INTEGER DEFAULT NULL,"+"\n";
			            sql += "ID VARCHAR(100),"+"\n";
			            sql += "SRC VARCHAR(200),"+"\n";
//			            sql += fields;
			            sql += "TS BIGINT,"+"\n";
			            sql += "TS_DATETIME TIMESTAMP"+"\n";

			            sql += ")"+"\n";
			            stmt = conn.createStatement();
			            Log.log.trace(sql);
			            stmt.execute(sql);
			            stmt.close();

			            sql = "";
			            sql += "CREATE INDEX HR_UNQIDX_"+name+" ON METRIC_HR_"+name+" ( IDX, ID, SRC )\n";
			            stmt = conn.createStatement();
			            Log.log.trace(sql);
			            stmt.execute(sql);
			            stmt.close();

			            sql = "";
			            sql += "CREATE INDEX HR_INDEXID_"+name+" ON METRIC_HR_"+name+" ( ID )\n";
			            stmt = conn.createStatement();
			            Log.log.trace(sql);
			            stmt.execute(sql);
			            stmt.close();

			            sql = "";
			            sql += "CREATE INDEX HR_INDEXSRC_"+name+" ON METRIC_HR_"+name+" ( SRC )\n";
			            stmt = conn.createStatement();
			            Log.log.trace(sql);
			            stmt.execute(sql);
			            stmt.close();
			        } 
			        catch(SQLException createExistingTableError)
			        {
			            Log.log.error(createExistingTableError.getMessage(), createExistingTableError);
			        }
                    
                    // alter table for creating or adding new fields in METRIC_HR_ table
                    alterTable(name, "METRIC_HR_", units);
			        
			        // METRIC_DAY_ table
			        if (drop)
			        {
			            try
			            {
					        sql = "";
					        sql += "DROP TABLE METRIC_DAY_"+name+"\n";
					        stmt = conn.createStatement();
					        Log.log.trace(sql);
					        stmt.execute(sql);
					        stmt.close();
			            }
			            catch (Throwable e) { }
			        }
			        
			        // create empty METRIC_DAY_ table 
			        try {
			            sql = "";
			            sql += "CREATE TABLE METRIC_DAY_"+name+"\n";
			            sql += "("+"\n";
			            sql += "IDX INTEGER DEFAULT NULL,"+"\n";
			            sql += "ID VARCHAR(100),"+"\n";
			            sql += "SRC VARCHAR(200),"+"\n";
			            //sql += fields;
			            sql += "TS BIGINT,"+"\n";
			            sql += "TS_DATETIME TIMESTAMP"+"\n";
			            sql += ")"+"\n";
			            stmt = conn.createStatement();
			            Log.log.trace(sql);
			            stmt.execute(sql);
			            stmt.close();

			            sql = "";
			            sql += "CREATE INDEX DAY_UNQIDX_"+name+" ON METRIC_DAY_"+name+" ( IDX, ID, SRC )\n";
			            stmt = conn.createStatement();
			            Log.log.trace(sql);
			            stmt.execute(sql);
			            stmt.close();

			            sql = "";
			            sql += "CREATE INDEX DAY_INDEXID_"+name+" ON METRIC_DAY_"+name+" ( ID )\n";
			            stmt = conn.createStatement();
			            Log.log.trace(sql);
			            stmt.execute(sql);
			            stmt.close();

			            sql = "";
			            sql += "CREATE INDEX DAY_INDEXSRC_"+name+" ON METRIC_DAY_"+name+" ( SRC )\n";
			            stmt = conn.createStatement();
			            Log.log.trace(sql);
			            stmt.execute(sql);
			            stmt.close();
			        } 
			        catch(SQLException createExistingTableError)
			        {
			            Log.log.error(createExistingTableError.getMessage(), createExistingTableError);
			        }

			        // alter table for creating or adding new fields in METRIC_DAY_ table
			        alterTable(name, "METRIC_DAY_", units);
		        }
		        
		        // update init and drop to false
		        group.put("INIT", "false");
		        group.put("DROP", "false");
		        
		        // commit
		        conn.commit();
	        }
        }
        catch (Throwable e)
        {
            Log.log.error(e.getMessage(), e);
        }
        finally
        {
            try
            {
                if (stmt != null)
                {
                    stmt.close();
                }
	            if (conn != null)
	            {
	                SQL.close(conn);
	            }
            }
            catch (Exception e)
            {
                Log.log.error(e.getMessage(), e);
            }
        }
        
    } // createTables
    
    /**
     * This method alters an exisiting database table to add new columns based on the MetricUnit 
     * @param tablename
     * @param metricunit
     * @param units
     */
    private void alterTable(String tablename, String metricunit, String[] units)
    {
        Statement stmt = null;
        SQLConnection conn = null;

        try
        {
            conn = SQL.getConnection();
            
            for (int i=0; i < units.length; i++)
            {
                try 
                {
                    String fieldName = units[i].trim().toLowerCase();
                    String sql = "";
                    sql += "ALTER TABLE " + metricunit + tablename + "\n";
                    sql += "ADD (" + "\n";
                    String fields = "";
                    fields += "TOT_"+fieldName+" INTEGER,"+"\n";
                    fields += "CNT_"+fieldName+" INTEGER,"+"\n";
                    fields += "DEL_"+fieldName+" INTEGER" +"\n";
                    sql += fields;
                    sql += ")"+"\n";
                    stmt = conn.createStatement();
                    Log.log.trace(sql);
                    stmt.execute(sql);
                    stmt.close();
                }
                catch(SQLException addDBFieldError)
                {
                    Log.log.error(addDBFieldError.getMessage(), addDBFieldError);
                }
            }
        }
        catch (Throwable e)
        {
            Log.log.error(e.getMessage(), e);
        }
        finally
        {
            try
            {
                if (stmt != null)
                {
                    stmt.close();
                }
                if (conn != null)
                {
                    SQL.close(conn);
                }
            }
            catch (Exception e)
            {
                Log.log.error(e.getMessage(), e);
            }
        }
        
    } // alterTable

    public void updateDB(MetricType type, MetricCache cache, int idx)
    {
        SQLConnection conn = null;
        PreparedStatement selectStmt = null;
        PreparedStatement insertStmt = null;
        PreparedStatement updateStmt = null;
        
        String tablePrefix = null;
        if (type == MetricType.MIN)
        {
            tablePrefix = "METRIC_MIN_";
        }
        else if (type == MetricType.HR)
        {
            tablePrefix = "METRIC_HR_";
        }
        else if (type == MetricType.DAY)
        {
            tablePrefix = "METRIC_DAY_";
        }
        
        try
        {
	        conn = SQL.getConnection();
	        
	        long ts = System.currentTimeMillis();
	        
	        for (MetricMsg metric : cache.values())
	        {
		        String name = metric.getName().toUpperCase();
		        
		        // init prepared statements
		        String sql = null;
		        String tableName =  HibernateUtil.getValidMetaData(tablePrefix+name.toUpperCase());

	            // init select stmt
		        sql = "SELECT IDX,ID,SRC FROM "+tableName+" WHERE IDX=? AND ID=? AND SRC=?";
		        Log.log.trace(sql);
		        selectStmt = conn.prepareStatement(HibernateUtil.getValidQuery(sql));
		        
	            // init insert stmt
	            String fields = "";
	            String values = "";

	            for(String unitName : metric.getUnits().keySet())
	            {
	                fields += "," + HibernateUtil.getValidMetaData("TOT_"+unitName);
	                fields += "," + HibernateUtil.getValidMetaData("CNT_"+unitName);
	                fields += "," + HibernateUtil.getValidMetaData("DEL_"+unitName);
	                values += ", ?, ?, ?";
	            }
	            tableName = HibernateUtil.getValidMetaData(tablePrefix+name.toUpperCase());
		        sql = "INSERT INTO "+tableName+" (idx,id,src,ts,ts_datetime" + fields +") ";
		        sql += "VALUES (?,?,?,?,?" + values +")";
		        Log.log.trace(sql);
		        insertStmt = conn.prepareStatement(HibernateUtil.getValidQuery(sql));
		        
	            // init update stmt
	            fields = "ts=?,ts_datetime=?";
	            for (String unitName : metric.getUnits().keySet())
	            {
	                fields += "," + HibernateUtil.getValidMetaData("TOT_"+unitName) +"=?";
	                fields += "," + HibernateUtil.getValidMetaData("CNT_"+unitName) +"=?";
	                fields += "," + HibernateUtil.getValidMetaData("DEL_"+unitName) +"=?";
	            }
	            
		        sql = "UPDATE "+tablePrefix+name.toUpperCase()+" SET " + fields + " ";
		        sql += "WHERE IDX=? AND ID=? AND SRC=?";
		        Log.log.trace(sql);
		        updateStmt = conn.prepareStatement(HibernateUtil.getValidQuery(sql));
		        
		        // check if row exists
                selectStmt.setInt(1, idx);
                selectStmt.setString(2, metric.getId());
                selectStmt.setString(3, metric.getSource());
		        ResultSet rs = selectStmt.executeQuery();
		        if (rs.next())
		        {
		            // update row
	                int pos = 1;
	                updateStmt.setLong(pos++, ts);
	                updateStmt.setTimestamp(pos++, new Timestamp(GMTDate.getDate(ts).getTime()));
                    
	                for(String unitName : metric.getUnits().keySet())
	                {
                      MetricUnit unitValue = metric.getUnits().get(unitName);
                      updateStmt.setLong(pos++, unitValue.getTotal());
                      updateStmt.setLong(pos++, unitValue.getCount());
                      updateStmt.setLong(pos++, unitValue.getDelta());
	                }
	                updateStmt.setInt(pos++, idx);
	                updateStmt.setString(pos++, metric.getId());
	                updateStmt.setString(pos++, metric.getSource());
	                
			        updateStmt.executeUpdate();
		        }
		        else
		        {
		            // insert row
	                int pos = 1;
	                insertStmt.setInt(pos++, idx);
	                insertStmt.setString(pos++, metric.getId());
	                insertStmt.setString(pos++, metric.getSource());
	                insertStmt.setLong(pos++, ts);
	                insertStmt.setTimestamp(pos++, new Timestamp(GMTDate.getDate(ts).getTime()));
		            
	                for(String unitName : metric.getUnits().keySet())
	                {
	                    MetricUnit unitValue = metric.getUnits().get(unitName);
	                    insertStmt.setLong(pos++, unitValue.getTotal());
	                    insertStmt.setLong(pos++, unitValue.getCount());
	                    insertStmt.setLong(pos++, unitValue.getDelta());
	                }

			        insertStmt.execute();
		        }
	        }
	        if (selectStmt != null)
	        {
		        selectStmt.close();
	        }
	        if (insertStmt != null)
	        {
		        insertStmt.close();
	        }
	        if (updateStmt != null)
	        {
		        updateStmt.close();
	        }
        }
        catch (Throwable e)
        {
            Log.log.error(e.getMessage(), e);
        }
        finally
        {
            try
            {
		        if (selectStmt != null)
		        {
			        selectStmt.close();
		        }
		        if (insertStmt != null)
		        {
			        insertStmt.close();
		        }
		        if (updateStmt != null)
		        {
			        updateStmt.close();
		        }
	            if (conn != null)
	            {
		            SQL.close(conn);
	            }
            }
            catch (Exception e)
            {
                Log.log.error(e.getMessage(), e);
            }
        }
    } // updateDB
    
    public void loadDB(ConfigMetric config)
    {
        SQLConnection conn = null;
        PreparedStatement selectStmt = null;

        try
        {
            conn = SQL.getConnection();

            long ts = System.currentTimeMillis();
            
            // load tables for each group
            for (Map<String, String> group : config.getGroups())
            {
                String tablename = group.get("NAME").toLowerCase();
                String unitsStr = group.get("UNITS").toLowerCase();
                String[] units = unitsStr.split(",");
                
                String fields = "";
                for (int i = 0; i < units.length; i++)
                {
                    String fieldName = units[i].trim().toLowerCase();
                        
                    fields += "," + "tot_" + fieldName;
                    fields += "," + "cnt_" + fieldName;
                    fields += "," + "del_" + fieldName;
                }
                
                /*
                 *  load min data into hr cache
                 */
                String sql = "SELECT idx,id,src"+fields+" FROM metric_min_" + tablename.toLowerCase() + " WHERE (ts_datetime > (UTC_TIMESTAMP() - interval 65 minute))";
                Log.log.trace(sql);
                selectStmt = conn.prepareStatement(sql);
                
                ResultSet rs = selectStmt.executeQuery();
                while (rs.next())
                {
                    HashMap<String, MetricUnit> metricUnits = new HashMap<String, MetricUnit>();
                    
                    // get values from db
                    String source = rs.getString("src");
                    String id = rs.getString("id");
                    for (int i = 0; i < units.length; i++)
                    {
                        String fieldName = units[i].trim().toLowerCase();
                        
                        long total = rs.getLong("tot_"+fieldName);
                        long count = rs.getLong("cnt_"+fieldName);
                        long delta = rs.getLong("del_"+fieldName);
                        
                        if(!id.equalsIgnoreCase(""))
                        {    
                            System.out.println("Debug Initializing id: "+ id + " Table name: metric_min_"+tablename.toLowerCase()+ " fieldname: "+fieldName+" src: "+source+" total: "+total+" count: "+count);
                        }
                        MetricUnit metricUnit = new MetricUnit(fieldName, total, count, delta);
                        metricUnits.put(fieldName, metricUnit);
                    }
                    
                    // FIXME - need to refactor mode into config file
                    MetricMsg metric = new MetricMsg(tablename, source, id, MetricMode.TOTAL, metricUnits);
                    
                    // add metric to cache
                    TrendMetric.updateCache(MetricType.HR, metric, TrendMetric.getHrIndex());
                }
                
                /*
                 *  load hr data into day cache
                 */
                sql = "SELECT idx,id,src"+fields+" FROM metric_hr_" + tablename.toLowerCase() + " WHERE (ts_datetime > (UTC_TIMESTAMP() - interval 65 minute))";
                Log.log.trace(sql);
                selectStmt = conn.prepareStatement(sql);
                
                rs = selectStmt.executeQuery();
                while (rs.next())
                {
                    HashMap<String, MetricUnit> metricUnits = new HashMap<String, MetricUnit>();
                    
                    // get values from db
                    String source = rs.getString("src");
                    String id = rs.getString("id");
                    for (int i = 0; i < units.length; i++)
                    {
                        String fieldName = units[i].trim().toLowerCase();
                        
                        long total = rs.getLong("tot_"+fieldName);
                        long count = rs.getLong("cnt_"+fieldName);
                        long delta = rs.getLong("del_"+fieldName);
                        
                        if(!id.equalsIgnoreCase(""))
                        {    
                            System.out.println("Debug Initializing id: "+ id + " Table name: metric_hr_"+tablename.toLowerCase()+ " fieldname: "+fieldName+" src: "+source+" total: "+total+" count: "+count);
                        }
                        MetricUnit metricUnit = new MetricUnit(fieldName, total, count, delta);
                        metricUnits.put(fieldName, metricUnit);
                    }
                    
                    // FIXME - need to refactor mode into config file
                    MetricMsg metric = new MetricMsg(tablename, source, id, MetricMode.TOTAL, metricUnits);
                    
                    // add metric to cache
                    TrendMetric.updateCache(MetricType.DAY, metric, TrendMetric.getDayIndex());
                }
            }
            
            if (selectStmt != null)
            {
                selectStmt.close();
            }
        }
        catch (Throwable e)
        {
            Log.log.error(e.getMessage(), e);
        }
        finally
        {
            try
            {
                if (selectStmt != null)
                {
                    selectStmt.close();
                }
                if (conn != null)
                {
                    SQL.close(conn);
                }
            }
            catch (Exception e)
            {
                Log.log.error(e.getMessage(), e);
            }
        }
    } // loadDB

    @Override
    public void updateDB(MetricType type, List<DTRecord> dtrecords, String[] dbfields, int idx, boolean deleteOldIndex)
    {
        // TODO Auto-generated method stub
    }

    @Override
    public void loadTMetricDB(ConfigMetric config, int idx)
    {
        // TODO Auto-generated method stub
    }

    @Override
    public List<String> getAllDTPathIDs()
    {
        // TODO Auto-generated method stub
        return null;
    }

    @Override
    public void insertIntoLookUpTable(String pathID, String path, String username, String dtRootDoc)
    {
        // TODO Auto-generated method stub
        
    }
 
} // MetricStoreDB2
