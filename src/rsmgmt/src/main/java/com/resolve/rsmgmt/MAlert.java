/******************************************************************************
* (C) Copyright 2014
*
* Resolve Systems, LLC
*
* All rights reserved
* This software is distributed under license and may not be used, copied,
* modified, or distributed without the express written permission of
* Resolve Systems, LLC.
*
******************************************************************************/

package com.resolve.rsmgmt;

import java.util.Map;

import com.resolve.rsbase.MainBase;
import com.resolve.util.Log;
import com.resolve.util.StringUtils;

public class MAlert
{
    public void alert(Map params)
    {
        try
        {
            String severity     = (String)params.get("SEVERITY");
            String component    = (String)params.get("COMPONENT");
            String code         = (String)params.get("CODE");
            String message      = (String)params.get("MESSAGE");
            String action       = (String)params.get("ACTION");
            
            ConfigAlert configAlert = ((Main)MainBase.main).getConfigAlert();
            
            if (StringUtils.isEmpty(action))
            {
                StringBuilder sb = new StringBuilder();
                
                if (configAlert.isSnmpTrapActive())
                {
                    sb.append("snmp");
                }
                
                if (configAlert.isDblogActive())
                {
                    sb.append("database");
                }
                
                if (configAlert.isEmailActive() && StringUtils.isNotEmpty(configAlert.getToEmail()))
                {
                    sb.append("email");
                }
                
                if (configAlert.isHttpActive())
                {
                    sb.append("http");
                }
                
                action = sb.toString();
            }
            
            // SNMP Trap
            if (StringUtils.isNotBlank(action) && action.contains("snmp") && configAlert.isSnmpTrapActive())
            {
	            AlertSNMP.alert(severity, component, code, message);
            }
            // DB Alert Logging
            if (StringUtils.isNotBlank(action) && action.contains("database") && configAlert.isDblogActive())
            {
	            AlertDBLog.alert(severity, component, code, message);
            }
            // Email Alert Logging
            if (StringUtils.isNotBlank(action) && action.contains("email") && configAlert.isEmailActive() && StringUtils.isNotEmpty(configAlert.getToEmail()))
            {
                AlertEmailLog.alert(configAlert, severity, component, code, message);
            }
            // MCP Notification
            if (StringUtils.isNotBlank(action) && action.contains("http") && configAlert.isHttpActive())
            {
                AlertHttp.alert(configAlert, params);
            }
        }
        catch (Exception e)
        {
            Log.log.warn(e.getMessage());
        }
    } // alert

} // MAlert
