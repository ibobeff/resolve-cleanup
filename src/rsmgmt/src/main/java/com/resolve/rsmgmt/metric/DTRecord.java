/******************************************************************************
* (C) Copyright 2014
*
* Resolve Systems, LLC
*
* All rights reserved
* This software is distributed under license and may not be used, copied,
* modified, or distributed without the express written permission of
* Resolve Systems, LLC.
*
******************************************************************************/

package com.resolve.rsmgmt.metric;

import java.sql.Timestamp;

/**
 * @author alokika.dash
 *
 */
public class DTRecord
{
    /*
     * Class that notes each record to be inserted into the TMetric database DB
     */

    private Integer slotIDX;
    private String pathID;
    private String nodeID;
    private Long ts;
    private Timestamp ts_datetime;
    private Long nodeCount;
    private Long sequenceID;
    private Long pathCount;
    private Long totalTime;
    private String status;
    private String user;
    private String namespace;

    /**
     * @return the slotidx
     */
    public int getSlotidx()
    {
        return slotIDX;
    } // getSlotidx
    /**
     * @param slotidx the slotidx to set
     */
    public void setSlotIDX(int slotidx)
    {
        this.slotIDX = slotidx;
    } //setSlotIDX
    /**
     * @return the pathID
     */
    public String getPathID()
    {
        return pathID;
    } //getPathID
    /**
     * @param pathID the pathID to set
     */
    public void setPathID(String pathID)
    {
        this.pathID = pathID;
    } //setPathID
    /**
     * @return the nodeID
     */
    public String getNodeID()
    {
        return nodeID;
    } //getNodeID
    /**
     * @param nodeID the nodeID to set
     */
    public void setNodeID(String nodeID)
    {
        this.nodeID = nodeID;
    } //setNodeID
    /**
     * @return the ts
     */
    public long getTs()
    {
        return ts;
    } //getTs
    /**
     * @param ts the ts to set
     */
    public void setTs(long ts)
    {
        this.ts = ts;
    } //setTs
    /**
     * @return the ts_datetime
     */
    public Timestamp getTs_datetime()
    {
        return ts_datetime;
    } //getTs_datetime
    /**
     * @param ts_datetime the ts_datetime to set
     */
    public void setTs_datetime(Timestamp ts_datetime)
    {
        this.ts_datetime = ts_datetime;
    } // setTs_datetime
    /**
     * @return the nodeCount
     */
    public long getNodeCount()
    {
        return nodeCount;
    } // getNodeCount
    /**
     * @param nodeCount the nodeCount to set
     */
    public void setNodeCount(long nodeCount)
    {
        this.nodeCount = nodeCount;
    } // setNodeCount
    /**
     * @return the sequenceID
     */
    public long getSequenceID()
    {
        return sequenceID;
    } // getSequenceID
    /**
     * @param sequenceID the sequenceID to set
     */
    public void setSequenceID(long sequenceID)
    {
        this.sequenceID = sequenceID;
    } // setSequenceID
    /**
     * @return the pathCount
     */
    public long getPathCount()
    {
        return pathCount;
    } // getPathCount
    /**
     * @param pathCount the pathCount to set
     */
    public void setPathCount(long pathCount)
    {
        this.pathCount = pathCount;
    } // setPathCount
    /**
     * @return the totalTime
     */
    public long getTotalTime()
    {
        return totalTime;
    } // getTotalTime
    /**
     * @param totalTime the totalTime to set
     */
    public void setTotalTime(long totalTime)
    {
        this.totalTime = totalTime;
    } // setTotalTime
    /**
     * @return the status
     */
    public String getStatus()
    {
        return status;
    } // getStatus
    /**
     * @param status the status to set
     */
    public void setStatus(String status)
    {
        this.status = status;
    } // setStatus
    /**
     * @return the user
     */
    public String getUser()
    {
        return user;
    } // getUser
    /**
     * @param user the user to set
     */
    public void setUser(String user)
    {
        this.user = user;
    } // setUser
    /**
     * @return the namespace
     */
    public String getNamespace()
    {
        return namespace;
    } // getNamespace
    /**
     * @param namespace the namespace to set
     */
    public void setNamespace(String namespace)
    {
        this.namespace = namespace;
    } //setNamespace

} // DTRecord
