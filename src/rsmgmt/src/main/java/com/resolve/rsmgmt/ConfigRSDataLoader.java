/******************************************************************************
* (C) Copyright 2014
*
* Resolve Systems, LLC
*
* All rights reserved
* This software is distributed under license and may not be used, copied,
* modified, or distributed without the express written permission of
* Resolve Systems, LLC.
*
******************************************************************************/

package com.resolve.rsmgmt;

import com.resolve.util.Properties;

public class ConfigRSDataLoader
{
    private static final String instanceName = "rsdatacollector";
	private String user;
    
    public ConfigRSDataLoader(Properties properties) {
	}
    
    public void setUser(String user)
    {
        this.user = user;
    }

    public String getUser()
    {
        return this.user;
    }
    
}
