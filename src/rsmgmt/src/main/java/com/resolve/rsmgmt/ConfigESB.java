/******************************************************************************
* (C) Copyright 2014
*
* Resolve Systems, LLC
*
* All rights reserved
* This software is distributed under license and may not be used, copied,
* modified, or distributed without the express written permission of
* Resolve Systems, LLC.
*
******************************************************************************/

package com.resolve.rsmgmt;

import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.util.ArrayList;
import java.util.Set;
import java.util.HashSet;

import com.resolve.util.Log;
import com.resolve.util.StringUtils;
import com.resolve.util.XDoc;

public class ConfigESB extends com.resolve.rsbase.ConfigESB
{
    private static final long serialVersionUID = 8390002925392587343L;
	Set<NamedQueue> namedQueues = new HashSet<NamedQueue>();

    public ConfigESB(XDoc config) throws Exception
    {
        super(config);
    } // ConfigESB

    boolean addNamedQueue(NamedQueue q)
    {
        return namedQueues.add(q);
    }
    public void load() throws Exception
    {
        super.load();

        // Named queues
        List queues = xdoc.getListMapValue("./ESB/QUEUE");

        // Add named queues
        if (queues.size() > 0)
        {
            for (Iterator i = queues.iterator(); i.hasNext();)
            {
                Map values = (Map) i.next();

                String name = (String) values.get("NAME");
                
                if (!StringUtils.isEmpty(name))
                {
                    NamedQueue q = new NamedQueue(name);
                    if (!namedQueues.add(q))
                    {
                        Log.log.warn("Duplicate queue: " + q);
                    }
                    else
                    {
                        Log.log.info("Added a new queue: " + name);
                    }
                }
            }
        }
    } // load

    public void save() throws Exception
    {
        super.save();

        List<HashMap> queues = new ArrayList<HashMap>();
        if (namedQueues != null)
        {
            for (NamedQueue queue : namedQueues)
            {
                HashMap entry = new HashMap();
                entry.put("NAME", queue.name);
                queues.add(entry);
            }
            xdoc.setListMapValue("./ESB/QUEUE", queues);
        }

    } // save

    static class NamedQueue
    {
        String name;

        NamedQueue(String n)
        {
            name = n;
        }

        /* (non-Javadoc)
         * @see java.lang.Object#hashCode()
         */
        @Override
        public int hashCode()
        {
            final int prime = 31;
            int result = 1;
            result = prime * result + ((name == null) ? 0 : name.hashCode());
            return result;
        }

        /* (non-Javadoc)
         * @see java.lang.Object#equals(java.lang.Object)
         */
        @Override
        public boolean equals(Object obj)
        {
            if (this == obj) return true;
            if (obj == null) return false;
            if (getClass() != obj.getClass()) return false;
            NamedQueue other = (NamedQueue) obj;
            if (name == null)
            {
                if (other.name != null) return false;
            }
            else if (!name.equals(other.name)) return false;
            return true;
        }
        
    } // NamedQueue
    
} // ConfigESB
