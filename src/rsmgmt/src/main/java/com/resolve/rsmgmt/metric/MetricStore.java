/******************************************************************************
* (C) Copyright 2014
*
* Resolve Systems, LLC
*
* All rights reserved
* This software is distributed under license and may not be used, copied,
* modified, or distributed without the express written permission of
* Resolve Systems, LLC.
*
******************************************************************************/

package com.resolve.rsmgmt.metric;

import java.util.List;

import com.resolve.rsmgmt.ConfigMetric;
import com.resolve.util.MetricCache;
import com.resolve.util.MetricType;

public interface MetricStore
{
    public void createTables(ConfigMetric config);
    public void updateDB(MetricType type, MetricCache cache, int idx);
    public void loadDB(ConfigMetric config);
    
    /**
     * Updates or inserts into the tmetric_hr decision tree table a list of new records
     * @param type 
     *          distinguishes hr or day in tmetric tables
     * @param dtrecords
     *          list of records/rows to be inserted
     * @param dbfields
     *          names of the columns of the database
     * @param idx
     *          time slot
     */
    public void updateDB(MetricType type, List<DTRecord> dtrecords, String[] dbfields, int idx, boolean deleteOldIndex);
    
    /**
     * Consolidates the entries from the hr db and 
     * loads into the day db for decision tree tmetric tables
     * @param config
     * @param idx
     */
    public void loadTMetricDB(ConfigMetric config, int idx);
    
    /**
     * Returns a list of path identifiers from the tmetric_lookup table
     * @return list of path identifiers which are 32 digits hexadecimal 
     */
    public List<String> getAllDTPathIDs();
    
    /**
     * Inserts into the tmetric_lookup table that maps decision tree paths to path idntifiers
     * @param pathID
     * @param path
     * @param username 
     */
    public void insertIntoLookUpTable(String pathID, String path, String username, String dtRootDoc);
    
} // MetricStore
