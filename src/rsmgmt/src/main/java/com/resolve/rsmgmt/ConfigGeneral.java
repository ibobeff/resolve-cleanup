/******************************************************************************
* (C) Copyright 2014
*
* Resolve Systems, LLC
*
* All rights reserved
* This software is distributed under license and may not be used, copied,
* modified, or distributed without the express written permission of
* Resolve Systems, LLC.
*
******************************************************************************/

package com.resolve.rsmgmt;

import com.resolve.util.MessageDispatcher;
import com.resolve.util.XDoc;

public class ConfigGeneral extends com.resolve.rsbase.ConfigGeneral
{
    private static final long serialVersionUID = 338253705649793503L;
	boolean cluster = false;
    long dropEventTime = MessageDispatcher.MAXIMUM_EVENT_WAITING_TIME;
    int maxWaitingTask = MessageDispatcher.WAITING_TASK_THRESHOLD;
    int maxRunningRunbook = MessageDispatcher.MAXIMUM_RUNNING_RUNBOOKS;
    public int scheduledPool;
    
    public ConfigGeneral(XDoc config) throws Exception
    {
        super(config);
        
        define("cluster", BOOLEAN, "./GENERAL/@CLUSTER");
        define("dropEventTime", LONG, "./GENERAL/@DROPEVENTTIME");
        define("maxWaitingTask", INT, "./GENERAL/@MAXWAITINGTASK");
        define("maxRunningRunbook", INT, "./GENERAL/@MAXRUNNINGRUNBOOK");
        define("scheduledPool", INTEGER, "./GENERAL/@SCHEDULEDPOOL");
    } // ConfigGeneral
    
    public void load()
    {
        super.load();
        
        MessageDispatcher.MAXIMUM_EVENT_WAITING_TIME = dropEventTime;
        MessageDispatcher.WAITING_TASK_THRESHOLD = maxWaitingTask;
        MessageDispatcher.MAXIMUM_RUNNING_RUNBOOKS = maxRunningRunbook;
    } // load

    public void save()
    {
        dropEventTime = MessageDispatcher.MAXIMUM_EVENT_WAITING_TIME;
        maxWaitingTask = MessageDispatcher.WAITING_TASK_THRESHOLD;
        maxRunningRunbook = MessageDispatcher.MAXIMUM_RUNNING_RUNBOOKS;
        
        super.save();
    } // save

    public boolean isCluster()
    {
        return cluster;
    }

    public void setCluster(boolean cluster)
    {
        this.cluster = cluster;
    }

    public long getDropEventTime()
    {
        return dropEventTime;
    }

    public void setDropEventTime(long dropEventTime)
    {
        this.dropEventTime = dropEventTime;
    }

    public int getMaxWaitingTask()
    {
        return maxWaitingTask;
    }

    public void setMaxWaitingTask(int maxWaitingTask)
    {
        this.maxWaitingTask = maxWaitingTask;
    }

    public int getMaxRunningRunbook()
    {
        return maxRunningRunbook;
    }

    public void setMaxRunningRunbook(int maxRunningRunbook)
    {
        this.maxRunningRunbook = maxRunningRunbook;
    }
    
    public int getScheduledPool() {
        return scheduledPool;
    }

    public void setScheduledPool(int scheduledPool) {
        this.scheduledPool = scheduledPool;
    }
    
} // ConfigGeneral
