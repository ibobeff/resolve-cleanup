/******************************************************************************
* (C) Copyright 2014
*
* Resolve Systems, LLC
*
* All rights reserved
* This software is distributed under license and may not be used, copied,
* modified, or distributed without the express written permission of
* Resolve Systems, LLC.
*
******************************************************************************/

package com.resolve.rsmgmt;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import com.resolve.util.Constants;
import com.resolve.util.CryptUtils;
import com.resolve.util.Log;
import com.resolve.util.Properties;
import com.resolve.util.StringUtils;

public class ConfigRSControl
{
    private static final String instanceName = "rscontrol";

    private String user = "resolve";
    
    private Properties rscontrolProperties;
    private Properties hazelcastProperties;
    private Properties jtaProperties;
    private Properties ehcacheProperties;
    private Properties runProperties;
    private Properties envProperties;
    private Properties logProperties;
    private Properties hibernateProperties;
    private Properties neo4jProperties;
    private Properties esapiProperties;
    private String hibernateDBType = "MYSQL";
    private String rscontrolNodes = "127.0.0.1";
    private String cassandraNodes = "127.0.0.1";
    private String cassandraPort = "9160";
    private List<String> implPrefixes;
    private Map<String, String> implPackages;

    public ConfigRSControl()
    {
        this.rscontrolProperties = new Properties();
        this.hazelcastProperties = new Properties();
        this.jtaProperties = new Properties();
        this.ehcacheProperties = new Properties();
        this.runProperties = new Properties();
        this.envProperties = new Properties();
        this.logProperties = new Properties();
        this.hibernateProperties = new Properties();
        this.neo4jProperties = new Properties();
        this.implPrefixes = new ArrayList<String>();
        this.implPackages = new HashMap<String, String>();
        this.esapiProperties = new Properties();
        
    } // ConfigRSControl
    
    public ConfigRSControl(Properties properties)
    {
        this();
	    for (Object key : properties.keySet())
        {
            String strKey = key.toString();
            if (strKey.startsWith(instanceName + "."))
            {
                String value = properties.getProperty(strKey).trim();
                if (value.contains("${"))
                {
                    value = properties.getReplacement(key).trim();
                }
                if (value.toUpperCase().startsWith("TO_ENC:"))
                {
                    if (StringUtils.isNotBlank(value.substring(7)) && !value.substring(7).toUpperCase().startsWith("NO_ENC:") && !value.substring(7).matches(".*(?<!\\\\)\\$\\{.*\\}.*"))
                    {
                        try
                        {
                            value = CryptUtils.encrypt(value.substring(7));
                        }
                        catch (Exception e)
                        {
                            Log.log.error("Failed to encrypt blueprint value prefixed with TO_ENC: set for the key " + strKey + 
                                          " due to error " + e.getMessage() + ", setting un-encrypted value into config.xml", e);
                        }
                    }
                }
                if (value.toUpperCase().startsWith("NO_ENC:"))
                {
                    value = value.substring(7);
                }
                if (value.startsWith("ENC"))
                {
                    try
                    {
	                    value = CryptUtils.decrypt(value);
                    }
                    catch (Exception e)
                    {
                        Log.log.error("Failed to decrypt blueprint value", e);
                        value = "";
                    }
                }
                if (strKey.contains("brokeraddr") && value.matches(":\\d*"))
                {
                    value = "";
                }
                if (value != null)
                {
	                if (strKey.startsWith(instanceName + ".hazelcast") && !value.equals(""))
	                {
	                    this.hazelcastProperties.put(key, value);
	                }
	                else if (strKey.startsWith(instanceName + ".hibernate"))
	                {
	                    if (strKey.equalsIgnoreCase(instanceName + ".hibernate.dbtype"))
	                    {
	                        hibernateDBType = value.toUpperCase();
	                    }
	                    else
	                    {
		                    this.hibernateProperties.put(key, value);
	                    }
	                }
	                else if (strKey.startsWith(instanceName + ".ehcache"))
	                {
	                    this.ehcacheProperties.put(key, value);
	                }
	                else if (strKey.startsWith(instanceName + ".env"))
                    {
                        this.envProperties.put(key, value);
                    }
	                else if (strKey.startsWith(instanceName + ".jta"))
	                {
	                    this.jtaProperties.put(key, value);
	                    if (strKey.equals(instanceName + ".jta.timeout"))
                        {
                            int timeout = Integer.parseInt(value.trim()) / 1000;
                            
                            rscontrolProperties.put(instanceName + ".sql.timeout", "" + timeout);
                        }
	                }
	                else if (strKey.startsWith(instanceName + ".run"))
	                {
	                    this.runProperties.put(key, value);
                        if (strKey.equals(instanceName + ".run.R_USER"))
                        {
                            this.user = value;
                        }
	                }
	                else if (strKey.startsWith(instanceName + ".log4j"))
	                {
	                    this.logProperties.put(key, value);
	                }
	                else if (strKey.startsWith(instanceName + ".neo4j"))
	                {
	                    this.neo4jProperties.put(key, value);
	                }
	                else if (strKey.startsWith(instanceName + ".ESAPI"))
	                {
	                    this.esapiProperties.put(key, value);
	                }
	                else
	                {
	                    if (strKey.contains("pass") && !strKey.contains("ldap.password_attribute"))
                        {
                            if (StringUtils.isNotBlank(value) && !value.toUpperCase().startsWith("NO_ENC:") && !value.matches(".*(?<!\\\\)\\$\\{.*\\}.*"))
                            {
                                try
                                {
                                    value = CryptUtils.encrypt(value);
                                }
                                catch (Exception e)
                                {
                                    Log.log.error("Failed to encrypt blueprint value for the key " + strKey + 
                                                  " due to error " + e.getMessage() + ", setting un-encrypted value into config.xml", e);
                                }
                            }
                        }
	                    this.rscontrolProperties.put(key, value);
	                }
                }
            } 
        }
        
	    rscontrolNodes =  properties.get(Constants.BLUEPRINT_RSCONTROL_NODELIST);
        cassandraNodes = properties.getReplacement(Constants.BLUEPRINT_CASSANDRA_NODELIST);
        cassandraPort = properties.get(Constants.BLUEPRINT_CASSANDRA_SEED_PORT);
        
        if (StringUtils.isBlank(rscontrolProperties.get(instanceName + ".nosql.seeds")))
        {
            //generate seeds
            if (StringUtils.isNotBlank(cassandraNodes) && StringUtils.isNotBlank(cassandraPort))
            {
                StringBuilder configSeeds = new StringBuilder();
                String[] seeds = cassandraNodes.split(",");
                for (String seed : seeds)
                {
                    if (configSeeds.length() > 0)
                    {
                        configSeeds.append("," + seed + ":" + cassandraPort);
                    }
                    else
                    {
                        configSeeds.append(seed + ":" + cassandraPort);
                    }
                }
                rscontrolProperties.put(instanceName + ".nosql.seeds", configSeeds.toString());
            }
        }
    } // ConfigRSControl
    
    public Map<String, String> rscontrolEsapiReplaceValues()
    {
        Map<String, String> result = new HashMap<String, String>();
        
        for (Object key : esapiProperties.keySet())
        {            
            String strKey = key.toString();
            String value = esapiProperties.getProperty(strKey);
            
            strKey = strKey.replaceFirst(instanceName + ".ESAPI.", "");

            // don't replace \\ with / if it's a validator property. It messes with the regex.
            if(strKey.startsWith("Validator")) {
                value = value.replace("\\", "\\\\");
            } else {
                value = value.replace("\\\\", "/");
            }
            
            String replaceRegex = strKey + "=.*";
            String replaceString = strKey + "=" + value;
            
            result.put(replaceRegex, replaceString);
        }
        
        return result;
    } //rsviewEsapiReplaceValues
    public Map<String, String> rscontrolEnvReplaceValues()
    {
        Map<String, String> result = new HashMap<String, String>();
        
        for (Object key : envProperties.keySet())
        {
            String strKey = key.toString();
            String value = envProperties.getProperty(strKey);
            
            strKey = strKey.replaceFirst(instanceName + ".env.", "");
            value = value.replaceAll("\\\\", "/");
            if (value.equalsIgnoreCase("true") || value.equalsIgnoreCase("false"))
            {
                value = value.toUpperCase();
            }
            
            strKey = strKey.toUpperCase();
            String replaceRegex = strKey + "=.*";
            String replaceString = strKey + "=" + value;

            result.put(replaceRegex, replaceString);
        }
        return result;
    }
    @SuppressWarnings("unchecked")
    public Map<String, Object> rscontrolXPathValues()
    {
        Map<String, Object> result = new HashMap<String, Object>();
        
        for (Object key : rscontrolProperties.keySet())
        {
            String strKey = key.toString();
            String value = (String) rscontrolProperties.get(key);
            
            strKey = strKey.replaceFirst(instanceName, "").toUpperCase();
            strKey = strKey.replaceAll("(?<!\\\\)\\.", "/");
            strKey = strKey.replaceAll("\\\\\\.", ".");
            
            Pattern pattern = Pattern.compile("(.+)/(.+)/(\\d+)");
            Matcher matcher = pattern.matcher(strKey);
            if (matcher.matches())
            {
                strKey = "." + matcher.group(1);
                String xdocKey = matcher.group(2);
                int listPosition = Integer.parseInt(matcher.group(3))-1;

                List<Map<String, String>> list = (List<Map<String, String>>) result.get(strKey);
                if (list == null)
                {
                    list = new LinkedList<Map<String, String>>();
                    result.put(strKey, list);
                }
                for (int j=list.size(); j<=listPosition; j++)
                {
                    list.add(new HashMap<String, String>());
                }
                Map<String, String> entry = list.get(listPosition);
                entry.put(xdocKey, value);
                list.set(listPosition, entry);
            }
            else
            {
                strKey = strKey.replaceFirst("^(.*)/(.*?)$", ".$1/@$2");
                result.put(strKey, value);
            }
        }
                        
        
        return result;
    } //rscontrolXPathValues()
    
    @SuppressWarnings("unchecked")
    public Map<String, Object> hazelcastXPathValues()
    {
        Map<String, Object> result = new HashMap<String, Object>();
        
        String[] nodeList = rscontrolNodes.split(",");
        for (int i=0; i<nodeList.length; i++)
        {
            String node = nodeList[i];
            List<String> tcpIPInterfaces = (List<String>) result.get("./network/join/tcp-ip/interface");
            List<String> tcpIPHostnames = (List<String>) result.get("./network/join/tcp-ip/hostname");
                
            if (node.matches("\\d{1,3}\\.\\d{1,3}\\.\\d{1,3}\\.\\d{1,3}"))
            {
                if (tcpIPInterfaces == null)
                {
                    tcpIPInterfaces = new LinkedList<String>();
                    result.put("./network/join/tcp-ip/interface", tcpIPInterfaces);
                }
                tcpIPInterfaces.add(node);
            }
            else
            {
                if (tcpIPHostnames == null)
                {
                    tcpIPHostnames = new LinkedList<String>();
                    result.put("./network/join/tcp-ip/hostname", tcpIPHostnames);
                }
                tcpIPHostnames.add(node);
            }
        }
        for (Object key : hazelcastProperties.keySet())
        {
            String strKey = key.toString();
            String value = (String) hazelcastProperties.get(key);
            strKey = strKey.replaceFirst(instanceName + ".hazelcast", "");
            strKey = strKey.replaceAll("(?<!\\\\)\\.", "/");
            strKey = strKey.replaceAll("\\\\\\.", ".");
            
            Pattern pattern = Pattern.compile("(.+)/(.+)/(\\d+)");
            Matcher matcher = pattern.matcher(strKey);
            if (matcher.matches())
            {
                strKey = "." + matcher.group(1);
                String xdocKey = matcher.group(2);
                int listPosition = Integer.parseInt(matcher.group(3))-1;

                List<Map<String, String>> list = (List<Map<String, String>>) result.get(strKey);
                if (list == null)
                {
                    list = new LinkedList<Map<String, String>>();
                    result.put(strKey, list);
                }
                for (int j=list.size(); j<=listPosition; j++)
                {
                    list.add(new HashMap<String, String>());
                }
                Map<String, String> entry = list.get(listPosition);
                entry.put(xdocKey, value);
                list.set(listPosition, entry);
            }
            else
            {
                result.put(strKey, value);
            }
        }
        List<String> tcpIPInterfaces = (List<String>) result.get("./network/join/tcp-ip/interface");
        List<String> tcpIPHostnames = (List<String>) result.get("./network/join/tcp-ip/hostname");
                
        if (tcpIPInterfaces == null && tcpIPHostnames == null)
        {
            tcpIPInterfaces = new LinkedList<String>();
            tcpIPInterfaces.add("127.0.0.1");
            result.put("./network/join/tcp-ip/interface", tcpIPInterfaces);
        }
        
        return result;
    } //hazelcastXPathValues()
    
    public Map<String, Object> ehcacheXPathValues()
    {
        Map<String, Object> result = new HashMap<String, Object>();
        
        for (Object key : ehcacheProperties.keySet())
        {
            String strKey = key.toString();
            String value = (String) ehcacheProperties.get(key);
            
            strKey = strKey.replaceFirst(instanceName + ".ehcache", "");
            strKey = strKey.replaceAll("(?<!\\\\)\\.", "/");
            strKey = strKey.replaceAll("\\\\\\.", ".");
            
            strKey = strKey.replaceFirst("^(.*)/(.*?)$", ".$1/@$2");
            result.put(strKey, value);
        }
                        
        
        return result;
    } //ehcacheXPathValues()
    
   
    
    public Map<String, String> rscontrolRunReplaceValues()
    {
        Map<String, String> result = new HashMap<String, String>();
        
        for (Object key : runProperties.keySet())
        {
            String strKey = key.toString();
            String value = runProperties.getProperty(strKey);
            
            strKey = strKey.replaceFirst(instanceName + ".run.", "");
            
            if (strKey.equalsIgnoreCase("Xms") || strKey.equalsIgnoreCase("mms"))
            {
                if (value.endsWith("M"))
                {
                    value = value.substring(0, value.length()-1);
                }
                result.put("Xms\\d+M", "Xms" + value + "M");
                result.put("MMS=.*", "MMS=" + value);
            }
            else if (strKey.equalsIgnoreCase("Xmx") || strKey.equalsIgnoreCase("mmx"))
            {
                if (value.endsWith("M"))
                {
                    value = value.substring(0, value.length()-1);
                }
                result.put("Xmx\\d+M", "Xmx" + value + "M");
                result.put("MMX=.*", "MMX=" + value);
            } 
            else
            {
                value = value.replaceAll("\\\\", "/");
                if (value.equalsIgnoreCase("true") || value.equalsIgnoreCase("false"))
                {
                    value = value.toUpperCase();
                }
                
                strKey = strKey.toUpperCase();
                String replaceRegex = strKey + "=.*";
                String replaceString = strKey + "=" + value;
    
                result.put(replaceRegex, replaceString);
            }
        }
        return result;
    } //rscontrolRunReplaceValues
    
    public Map<String, String> rscontrolLogReplaceValues()
    {
        Map<String, String> result = new HashMap<String, String>();
        
        for (Object key : logProperties.keySet())
        {
            String strKey = key.toString();
            String value = logProperties.getProperty(strKey);
            
            strKey = strKey.replaceFirst(instanceName + ".", "");
            value = value.replaceAll("\\\\", "/");
	            
            String replaceRegex = strKey + "=.*";
            String replaceString = strKey + "=" + value;

            result.put(replaceRegex, replaceString);
        }
        
        return result;
    } //rscontrolLogReplaceValues
    
    public Map<String, String> hibernateReplaceValues()
    {
        Map<String, String> result = new HashMap<String, String>();
        
        for (Object key : hibernateProperties.keySet())
        {
            String strKey = key.toString();
            String value = hibernateProperties.getProperty(strKey);
            
            strKey = strKey.replaceFirst(instanceName + ".hibernate.", "");
            
            result.put(strKey, value);
        }
        return result;
    } //hibernateReplaceValues();
    
    public String getHibernateDBType()
    {
        return hibernateDBType;
    } // getHibernateDBType
    
    public Properties getRscontrolProperties()
    {
        return rscontrolProperties;
    } // getRscontrolProperties

    public void setRscontrolProperties(Properties properties)
    {
        this.rscontrolProperties = properties;
    } //setRscontrolProperties

    public Properties getHazelcastProperties()
    {
        return hazelcastProperties;
    } //getHazelcastProperties

    public void setHazelcastProperties(Properties hazelcastProperties)
    {
        this.hazelcastProperties = hazelcastProperties;
    } //setHazelcastProperties

    public Properties getEhcacheProperties()
    {
        return ehcacheProperties;
    } //getEhcacheProperties

    public void setEhcacheProperties(Properties ehcacheProperties)
    {
        this.ehcacheProperties = ehcacheProperties;
    } //setEhcacheProperties

    public Properties getJtaProperties()
    {
        return jtaProperties;
    } //getJtaProperties

    public void setJtaProperties(Properties jtaProperties)
    {
        this.jtaProperties = jtaProperties;
    } //setJtaProperties
    
    public Properties getRunProperties()
    {
        return runProperties;
    } //getRunProperties

    public void setRunProperties(Properties runProperties)
    {
        this.runProperties = runProperties;
    } //setRunProperties

    public Properties getHibernateProperties()
    {
        return hibernateProperties;
    } //getHibernateProperties

    public void setHibernateProperties(Properties hibernateProperties)
    {
        this.hibernateProperties = hibernateProperties;
    } //setHibernateProperties

    public Properties getNeo4jProperties()
    {
        return neo4jProperties;
    } //getNeo4jProperties

    public void setNeo4jProperties(Properties neo4jProperties)
    {
        this.neo4jProperties = neo4jProperties;
    } //setNeo4jProperties
    
    public List<String> getImplPrefixes()
    {
        return implPrefixes;
    } // getImplPrfixes
    
    public void setImplPrefixes(List<String> implPrefixes)
    {
        this.implPrefixes = implPrefixes;
    } // setimplPrefixes
    
    public Map<String, String> getImplPackages()
    {
        return implPackages;
    }

    public void setImplPackages(Map<String, String> implPackages)
    {
        this.implPackages = implPackages;
    }
    
    public void setEsapiProperties(Properties esapiProperties)
    {
        this.esapiProperties = esapiProperties;
    }
    
    public Properties getEsapiProperties()
    {
        return this.esapiProperties;
    }

    public void setUser(String user)
    {
        this.user = user;
    }

    public String getUser()
    {
        return this.user;
    }
}//ConfigRSControl
