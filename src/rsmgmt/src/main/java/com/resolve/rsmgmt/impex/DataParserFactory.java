/******************************************************************************
* (C) Copyright 2014
*
* Resolve Systems, LLC
*
* All rights reserved
* This software is distributed under license and may not be used, copied,
* modified, or distributed without the express written permission of
* Resolve Systems, LLC.
*
******************************************************************************/

package com.resolve.rsmgmt.impex;


public class DataParserFactory
{
    public static DataParser getDataParser(DataType dataType) throws Exception
    {
        DataParser result = null;
        
        if("csv".equalsIgnoreCase(dataType.getTagName()))
        {
            result = new DataParserCSV();
        }
        else if("xls".equalsIgnoreCase(dataType.getTagName()))
        {
            result = new DataParserCSV();
        }
        else
        {
            throw new Exception("Invalid data type \"" + dataType.getTagName() + "\".");
        }
        return result;
    }
}
