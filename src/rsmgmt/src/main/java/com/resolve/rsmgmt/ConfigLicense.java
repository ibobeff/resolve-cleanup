/******************************************************************************
* (C) Copyright 2014
*
* Resolve Systems, LLC
*
* All rights reserved
* This software is distributed under license and may not be used, copied,
* modified, or distributed without the express written permission of
* Resolve Systems, LLC.
*
******************************************************************************/

package com.resolve.rsmgmt;

import com.resolve.util.XDoc;

@SuppressWarnings("serial")
public class ConfigLicense extends com.resolve.rsbase.ConfigGeneral
{
    private static final long serialVersionUID = -1217870665273730337L;
	private boolean active = true;
    private int election = 30;

    //private int interval = 30;
    //private int heartbeat = 20000;
    private int failover = 90;

    public ConfigLicense(XDoc config) throws Exception
    {
        super(config);

        define("active", BOOLEAN, "./LICENSE/@ACTIVE");
        define("election", INT, "./LICENSE/@ELECTION");
        //define("interval", INT, "./LICENSE/@INTERVAL");
        define("failover", INT, "./LICENSE/@FAILOVER");
    } // ConfigLicense

    public void load()
    {
        super.load();
    } // load

    public void save()
    {
        super.save();
    } // save

    public boolean isActive()
    {
        return active;
    }

    public void setActive(boolean active)
    {
        this.active = active;
    }

    public int getElection()
    {
        return election;
    }

    public void setElection(int election)
    {
        this.election = election;
    }

    /*
    public int getInterval()
    {
        return interval;
    }

    public void setInterval(int interval)
    {
        this.interval = interval;
    }

    public int getHeartbeat()
    {
        return heartbeat;
    }

    public void setHeartbeat(int heartbeat)
    {
        this.heartbeat = heartbeat;
    }
    */
    public int getFailover()
    {
        return failover;
    }

    public void setFailover(int failover)
    {
        this.failover = failover;
    }
} // ConfigLicense
