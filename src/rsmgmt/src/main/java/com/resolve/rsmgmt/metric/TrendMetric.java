/******************************************************************************
* (C) Copyright 2014
*
* Resolve Systems, LLC
*
* All rights reserved
* This software is distributed under license and may not be used, copied,
* modified, or distributed without the express written permission of
* Resolve Systems, LLC.
*
******************************************************************************/

package com.resolve.rsmgmt.metric;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

import org.apache.log4j.Level;

import com.resolve.esb.MListener;
import com.resolve.rsbase.MainBase;
import com.resolve.rsmgmt.ConfigMetric;
import com.resolve.util.Constants;
import com.resolve.util.Log;
import com.resolve.util.MetricCache;
import com.resolve.util.MetricMsg;
import com.resolve.util.MetricType;
import com.resolve.util.MetricUnit;
import com.resolve.util.StringUtils;
import com.resolve.util.TMetricCache;
import com.resolve.vo.DTMetricLogVO;
import com.resolve.vo.TMetricLogVO;
import com.resolve.vo.TNodeVO;

public class TrendMetric
{
    static int prevMinIdx = -1;
    static int prevHrIdx = -1;
    static int prevDayIdx = -1;

    static MetricCache[] minCache;
    static MetricCache[] hrCache;
    static MetricCache[] dayCache;

    //cache to store the Tree logs in cache - for DT and Runbook
    // There is only one hour Tcache at any point in time.
    static TMetricCache globalTCache;

    static MListener mListener = null;
    static ConfigMetric config = null;
    static String dbType = null;
    static MetricStore store = null;

    static
    {
        // init min cache
        minCache = new MetricCache[288];
        for (int i = 0; i < 288; i++)
        {
            minCache[i] = new MetricCache();
        }

        // init hr cache
        hrCache = new MetricCache[168];
        for (int i = 0; i < 168; i++)
        {
            hrCache[i] = new MetricCache();
        }

        // init day cache
        dayCache = new MetricCache[365];
        for (int i = 0; i < 365; i++)
        {
            dayCache[i] = new MetricCache();
        }

        //init hr TCache
        globalTCache = new TMetricCache();

    } // static

    public TrendMetric()
    {
        // empty constructor for scheduled executors
    } // TrendMetric

    public TrendMetric(ConfigMetric config, String dbType)
    {
        this.config = config;
        this.dbType = dbType;

    } // TrendMetric

    public void start()
    {
        try
        {
            Log.log.info("Initializing TrendMetric");

            // init database - TODO add support for db2
            if (dbType.equalsIgnoreCase("MYSQL"))
            {
                store = new MetricStoreMySQL();
            }
            else if (dbType.toUpperCase().startsWith("ORACLE"))
            {
                store = new MetricStoreOracle();
            }
            else if (dbType.equalsIgnoreCase("DB2"))
            {
                store = new MetricStoreDB2();
            }

            // init tables
            store.createTables(config);

            // initialize current cache from database
            store.loadDB(config);

            // add listener (METRIC)
            Log.log.info("  Initializing TrendMetric ESB Listener");
            //mListener = MListenerFactory.getMListener(MainBase.main.mServer, Constants.ESB_NAME_METRIC, "com.resolve.rsmgmt.metric");
            mListener = MainBase.main.mServer.createListener(Constants.ESB_NAME_METRIC, "com.resolve.rsmgmt.metric");
            mListener.init(false);

            // init handlers
            MainBase.main.mServer.addHandler("MMetric", MMetric.class);
        }
        catch (Throwable e)
        {
            Log.log.warn(e.getMessage(), e);
        }
    } // start

    public void stop()
    {
    } // stop

    public static void process5Min()
    {
        Log.start("Starting Process5Min", Level.DEBUG);

        try
        {
            // get index
            int idx = getMinIndex();
            int prevIdx = getMinPrevIndex(idx);
            int prevPrevIdx = getMinPrevIndex(prevIdx);

            // get MetricCache for prevPrevIdx first
            MetricCache cache = minCache[prevPrevIdx];

            if (cache != null && store != null)
            {
                store.updateDB(MetricType.MIN, cache, prevPrevIdx);

                // clear prev cache
                if (prevPrevIdx > 0)
                {
                    cache = minCache[prevPrevIdx - 1];
                    if (cache != null)
                    {
                        cache.clear();
                    }
                }
            }

            // get MetricCache for prevIdx
            cache = minCache[prevIdx];
            if (cache != null && store != null)
            {
                store.updateDB(MetricType.MIN, cache, prevIdx);
                // NOTE: dont clear prev cache
            }
        }
        catch (Throwable e)
        {
            Log.log.error(e.getMessage(), e);
        }

        Log.duration("Completed Process5Min", Level.DEBUG);
    } // process5Min

    public static void process1Hr()
    {
        Log.start("Starting Process1Hr", Level.DEBUG);

        try
        {
            // get index
            int idx = getHrIndex();
            int prevIdx = getHrPrevIndex(idx);
            int prevPrevIdx = getHrPrevIndex(prevIdx);

            // get MetricCache for prevPrevIdx
            MetricCache cache = hrCache[prevPrevIdx];
            if (cache != null && store != null)
            {
                store.updateDB(MetricType.HR, cache, prevPrevIdx);

                // clear prev cache
                if (prevPrevIdx > 0)
                {
                    cache = hrCache[prevPrevIdx - 1];
                    // cache is empty but not null ?? How should we handle that????
                    if (cache != null)
                    {
                        cache.clear();
                    }
                }
            }

            // get MetricCache for prevIdx
            cache = hrCache[prevIdx];
            if (cache != null && store != null)
            {
                store.updateDB(MetricType.HR, cache, prevIdx);
                // NOTE: dont clear prev cache
            }
        }
        catch (Throwable e)
        {
            Log.log.error(e.getMessage(), e);
        }

        Log.duration("Completed Process1Hr", Level.DEBUG);
    } // process1Hr

    /*
     * Special procedure to calculate the metrics of a decision tree traversal
     * This executed every 1 hr from rsmgmt/main.java
     */
    @SuppressWarnings("rawtypes")
    public static void processDecisionTree1Hr()
    {
        Log.start("Starting ProcessDecisionTree1Hr", Level.DEBUG);

        try
        {
            // get current index -slotID
            int idx = getHrIndex();

            // get TMetricCache for analysis
            TMetricCache cache = globalTCache;

            if (cache != null && store != null)
            {
                // get a list of records to be inserted into database
//              For DEBUG Only
//				cache.printDTCache();
                TMetricDecisionTree tmetric = new TMetricDecisionTree(idx);
                ConcurrentHashMap<String, List<DTRecord>> recordsPerExecution = tmetric.analyzeCache(cache,store);

                if(recordsPerExecution != null)
                {
                    // get the fieldnames in the decision tree table from db tmetric_hr_dt
                    String[] fieldNames = null;
                    for (Map<String, String> group : config.getGroups())
                    {
                        String type = group.get("TYPE").toLowerCase();
                        if(type.equalsIgnoreCase("decisiontree"))
                        {
                            String unitsStr = group.get("UNITS").toLowerCase();
                            fieldNames = unitsStr.split(",");
                            break;
                        }
                    }
                    // loop through records and insert the records into DB
                    
                    //added this flag as the delete was happening everytime update was executed. These metric classes needs to be refactored
                    boolean deleteOldIndex = true; 
                    
                    Iterator it = recordsPerExecution.keySet().iterator();
                    while(it.hasNext())
                    {
                        // Get the executionID and list of records in that execution
                        String executionID = (String)it.next();
                        List<DTRecord> listOfRecords = recordsPerExecution.get(executionID);
                        store.updateDB(MetricType.HR, listOfRecords, fieldNames, idx, deleteOldIndex);
                        // delete the records inserted into DB from TCache
                        cache.remove(executionID);
                        deleteOldIndex = false;
                    }
                }
            }
        }
        catch (Throwable e)
        {
            Log.log.error(e.getMessage(), e);
        }

        Log.duration("Completed ProcessDecisionTree1Hr ", Level.DEBUG);
    } // processDecisionTree1Hr

    /*
     * Special procedure to calculate the metrics of a decision tree traversal
     * This executed every 1 day from rsmgmt/main.java
     */
    public static void processDecisionTree1Day()
    {
        Log.start("Starting ProcessDecisionTree1Day", Level.DEBUG);

        try
        {
            // get index
            int idx = getDayIndex();
            // consolidate and update the tmetric_day DB with information from tmetric_hr DB
            store.loadTMetricDB(config, idx);
        }
        catch (Throwable e)
        {
            Log.log.error(e.getMessage(), e);
        }

        Log.duration("Completed ProcessDecisionTree1Day", Level.DEBUG);
    } // processDecisionTree1Day

    public static void process1Day()
    {
        Log.start("Starting Process1Day", Level.DEBUG);

        try
        {
            // get index
            int idx = getDayIndex();
            int prevIdx = getDayPrevIndex(idx);
            int prevPrevIdx = getDayPrevIndex(prevIdx);

            // get MetricCache for prevIdx
            MetricCache cache = dayCache[prevPrevIdx];
            if (cache != null)
            {
                store.updateDB(MetricType.DAY, cache, prevPrevIdx);

                // clear prev cache
                if (prevPrevIdx > 0)
                {
                    cache = dayCache[prevPrevIdx - 1];
                    if (cache != null)
                    {
                        cache.clear();
                    }
                }
            }

            // get MetricCache for prevIdx
            cache = dayCache[prevIdx];
            if (cache != null)
            {
                store.updateDB(MetricType.DAY, cache, prevIdx);
                // NOTE: dont clear prev cache
            }
        }
        catch (Throwable e)
        {
            Log.log.error(e.getMessage(), e);
        }

        Log.duration("Completed Process1Day", Level.DEBUG);
    } // process1Day


    /*
     * update metric messgae cache
     */
    public static void insert(MetricMsg metric)
    {
        updateCache(MetricType.MIN, metric, getMinIndex());
        updateCache(MetricType.HR, metric, getHrIndex());
        updateCache(MetricType.DAY, metric, getDayIndex());
    } // insert

    /**
     * This method inserts the user clicks from rsview's viewcontroller when
     * traversing a decision tree to a cache
     * @param metric
     */
    public static void insert(TMetricLogVO metric)
    {
        updateTCache(MetricType.HR, metric);
    } // insert
    
    /**
     * this api is made synchronized on purpose as its adding/deleting the EXE id for a path. 
     * If multiple request are coming, than there can be a conflict between adding the log and other thread removing the 'key' from the map
     * 
     * @param metric
     */
    public synchronized static void insert(DTMetricLogVO metric)
    {
        if(metric != null)
        {
            //if the execution Id exist, first delete it, we will be recreating all the nodes again
            if(globalTCache.get(metric.getExecutionId()) != null)
            {
                globalTCache.remove(metric.getExecutionId());
            }
            
            //add all the logs back in the map
            List<TMetricLogVO> logs = metric.getLogs();
            if(logs != null && logs.size() > 0)
            {
                for(TMetricLogVO log : logs)
                {
                    //this will bypass the first node which is the start of DT and has NODE_TIME as '0'
                    if(StringUtils.isNotBlank(log.getPrevWikiNode()))
                    {
                        updateTCache(MetricType.HR, log);
                    }
                }
            }
        }
        
        
    } // insert

    /**
     * This method reads from the user logged information in the TMetricLogVO object and
     * creates a TNodeVO object for each node traversed in a decision tree.
     * @param type
     * @param log
     * @param idx : index of the current hour
     */
    private static void updateTCache(MetricType type, TMetricLogVO log)
    {
        //update the TCache - only HR for now
        if(type == MetricType.HR)
        {
            //prepare the TNodeVO obj and insert in the list for that execution id
            String executionId = log.getExecutionId();

            List<TNodeVO> listOfNodes = globalTCache.get(executionId);
            if(listOfNodes == null)
            {
                listOfNodes = new ArrayList<TNodeVO>();
            }

            //create the TNodeVO
            TNodeVO node = new TNodeVO();
            node.setWikiFullName(log.getPrevWikiNode()); //start DT node
            node.setRootDtFullName(log.getRootDt());
            node.setStartTimeInMS(StringUtils.isNotBlank(log.getPrevWikiStartTimeInMS()) ? Long.parseLong(log.getPrevWikiStartTimeInMS()) : 0);
            node.setStopTimeInMS(StringUtils.isNotBlank(log.getStopTimeInMS()) ? Long.parseLong(log.getStopTimeInMS()) : 0);
            node.setUsername(log.getUsername());
            node.setIsPathComplete(log.getIsPathComplete().equalsIgnoreCase("true") ? true : false);
            node.setIsLeaf(log.getIsLeaf().equalsIgnoreCase("true") ? true : false);
            node.setCurrWikiFullName(log.getCurrWikiNode()); // end DT node
            node.setAbortTimeInSecs(Integer.parseInt(log.getAbortTimeInSecs()));

            //add it to the list
            listOfNodes.add(node);

            //add it back to the cache
            globalTCache.put(executionId, listOfNodes);
            System.out.println("In TrendMetric.java -->" + node.toString());
            Log.log.debug(node.toString());
        }

    } // updateTCache

    static void updateCache(MetricType type, MetricMsg metric, int idx)
    {
        MetricMsg current = null;

        // init key
        String key = metric.getKey();

        MetricCache[] cache = null;

        int prevIdx = 0;
        if (type == MetricType.MIN)
        {
            prevIdx = prevMinIdx;
            cache = minCache;
        }
        else if (type == MetricType.HR)
        {
            prevIdx = prevHrIdx;
            cache = hrCache;
        }
        else if (type == MetricType.DAY)
        {
            prevIdx = prevDayIdx;
            cache = dayCache;
        }

        // if idx have changed, reset cache
        if (idx != prevIdx)
        {
            prevIdx = idx;
            current = new MetricMsg(metric);
        }
        else
        {
            // get cache metric
            MetricCache metricCache = cache[idx];
            current = metricCache.get(key);
            if (current == null)
            {
                current = new MetricMsg(metric);
            }
            else
            {
                current.consolidate(metric);
            }
        }

        // update current delta
        if (idx > 0)
        {
            MetricCache prevCache = cache[idx - 1];
            if (prevCache != null)
            {
                MetricMsg prevMetric = prevCache.get(key);
                if (prevMetric != null)
                {
                    for (String unitName : current.getUnits().keySet())
                    {
                        MetricUnit unit = current.getUnits().get(unitName);
                        MetricUnit prevUnit = prevMetric.getUnits().get(unitName);

                        if (unit != null && prevUnit != null && unit.getCount() > 0 && prevUnit.getCount() > 0)
                        {
                            long newAvg = unit.getTotal() / unit.getCount();
                            long oldAvg = prevUnit.getTotal() / prevUnit.getCount();

                            unit.setDelta(newAvg - oldAvg);
                        }
                        else
                        {
                            Log.log.warn("Unit / Previous Unit count should not be zero. unit.count: " + unit.getCount() + " prevUnit.count: " + prevUnit.getCount());
                            unit.setDelta(0);
                        }
                    }
                }
            }
        }

        // update cache metric
        MetricCache metricCache = cache[idx];
        metricCache.put(current.getKey(), current);

        // update prevIdx
        if (type == MetricType.MIN)
        {
            prevMinIdx = prevIdx;
        }
        else if (type == MetricType.HR)
        {
            prevHrIdx = prevIdx;
        }
        else if (type == MetricType.DAY)
        {
            prevDayIdx = prevIdx;
        }

    } // updateCache

    public static int getMinIndex()
    {
        return getMinIndex(System.currentTimeMillis());
    } // getMinIndex

    public static int getMinIndex(long ts)
    {
        // 5mins window
        final long interval = 5 * 60 * 1000;

        // mod number of millis per day
        final long millisPerDay = 24 * 3600000;

        // get millis have passed today
        long tsToday = ts % millisPerDay;

        // get window index for today
        long idx = tsToday / interval;

        return (int) idx;
    } // getMinIndex

    public static int getMinPrevIndex(final int currentIdx)
    {
        // get index
        int result = -1;
        if (currentIdx == 0)
        {
            result = 287;
        }
        else
        {
            result = currentIdx - 1;
        }

        return result;
    } // getMinPrevIndex

    public static int getHrIndex()
    {
        return getHrIndex(System.currentTimeMillis());
    } // getHrIndex

    public static int getHrIndex(long ts)
    {
        Date date = new Date(ts);
        return (date.getDay() * 24) + date.getHours();
    } // getHrIndex

    public static int getHrPrevIndex(final int currentIdx)
    {
        int result = -1;
        if (currentIdx == 0)
        {
            result = 167;
        }
        else
        {
            result = currentIdx - 1;
        }
        return result;
    } // getHrPrevIndex

    public static int getDayIndex()
    {
        return getDayIndex(System.currentTimeMillis());
    } // getDayIndex

    public static int getDayIndex(long ts)
    {
        Calendar cal = Calendar.getInstance();
        cal.setTimeInMillis(ts);
        return cal.get(Calendar.DAY_OF_YEAR);
    } // getDayIndex

    public static int getDayPrevIndex(final int currentIdx)
    {
        int result = -1;
        if (currentIdx == 0)
        {
            result = 167;
        }
        else
        {
            result = currentIdx - 1;
        }

        return result;
    } // getDayPrevIndex

    public static String getDbType()
    {
        return dbType;
    }

    public static void setDbType(String dbType)
    {
        TrendMetric.dbType = dbType;
    }

} // TrendMetric
