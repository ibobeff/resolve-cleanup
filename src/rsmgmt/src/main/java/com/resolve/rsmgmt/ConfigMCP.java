/******************************************************************************
* (C) Copyright 2014
*
* Resolve Systems, LLC
*
* All rights reserved
* This software is distributed under license and may not be used, copied,
* modified, or distributed without the express written permission of
* Resolve Systems, LLC.
*
******************************************************************************/

package com.resolve.rsmgmt;

import com.resolve.util.ConfigMap;
import com.resolve.util.XDoc;

public class ConfigMCP extends ConfigMap
{
    private static final long serialVersionUID = -4999894726809335807L;
	boolean active = false;
    int statusRequestExpiration = 60;

    public ConfigMCP(XDoc config) throws Exception
    {
        super(config);

        define("active", BOOLEAN, "./MCP/@ACTIVE");
        define("statusRequestExpiration", INTEGER, "./MCP/@STATUSREQUESTEXPIRATION");
    } // ConfigReceiveNetcool

    public void load() throws Exception
    {
        loadAttributes();
    } // load

    public void save() throws Exception
    {
        saveAttributes();
    } // save

    public boolean isActive()
    {
        return active;
    }

    public void setActive(boolean active)
    {
        this.active = active;
    }

    public int getStatusRequestExpiration()
    {
        return statusRequestExpiration;
    }

    public void setStatusRequestExpiration(int statusRequestExpiration)
    {
        this.statusRequestExpiration = statusRequestExpiration;
    }
} // ConfigAlert