/******************************************************************************
* (C) Copyright 2014
*
* Resolve Systems, LLC
*
* All rights reserved
* This software is distributed under license and may not be used, copied,
* modified, or distributed without the express written permission of
* Resolve Systems, LLC.
*
******************************************************************************/

package com.resolve.rsmgmt;

import com.resolve.util.XDoc;

public class ConfigSQL extends com.resolve.rsbase.ConfigSQL
{
    private static final long serialVersionUID = -8508119620140272684L;
	boolean active = false;
    
    public ConfigSQL(XDoc config) throws Exception
    {
        super(config);
        
        define("active", BOOLEAN, "./SQL/@ACTIVE");
        setMaxpoolsize(5);
    } // ConfigSQL
    
    public void load() throws Exception
    {
        loadAttributes();
    } // load

    public void save() throws Exception
    {
        saveAttributes();
    } // save

    public boolean isActive()
    {
        return active;
    }

    public void setActive(boolean active)
    {
        this.active = active;
    }
} // ConfigSQL