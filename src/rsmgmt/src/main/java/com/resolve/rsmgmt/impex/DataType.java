/******************************************************************************
* (C) Copyright 2014
*
* Resolve Systems, LLC
*
* All rights reserved
* This software is distributed under license and may not be used, copied,
* modified, or distributed without the express written permission of
* Resolve Systems, LLC.
*
******************************************************************************/

package com.resolve.rsmgmt.impex;

public enum DataType
{
    CSV("csv"),
    XLS("xls")
    //XML("xml"),
    ;
    
    private final String tagName;
    
    private DataType(String tagName)
    {
        this.tagName = tagName;
    }
    
    public String getTagName()
    {
        return this.tagName;
    }

}
