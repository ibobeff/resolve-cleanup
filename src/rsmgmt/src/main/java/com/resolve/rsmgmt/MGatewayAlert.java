/******************************************************************************
* (C) Copyright 2014
*
* Resolve Systems, LLC
*
* All rights reserved
* This software is distributed under license and may not be used, copied,
* modified, or distributed without the express written permission of
* Resolve Systems, LLC.
*
******************************************************************************/

package com.resolve.rsmgmt;

import java.util.Map;

import com.resolve.rsbase.MainBase;
import com.resolve.util.Log;

public class MGatewayAlert
{
    public void alert(Map params)
    {
        try
        {
            String severity     = (String)params.get("SEVERITY");
            String component    = (String)params.get("COMPONENT");
            String gateway    = (String)params.get("GATEWAY");
            String code         = (String)params.get("CODE");
            String message      = (String)params.get("MESSAGE");
            
            ConfigAlert configAlert = ((Main)MainBase.main).getConfigAlert();
            
            // Gateway Alert Logging
            if (configAlert.isGatewayLogActive())
            {
                GatewayLog.alert(severity, component, gateway, code, message);
            }
        }
        catch (Exception e)
        {
            Log.log.warn(e.getMessage());
        }
    } // alert

} // MAlert
