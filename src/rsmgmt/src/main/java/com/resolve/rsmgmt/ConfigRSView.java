/******************************************************************************
* (C) Copyright 2014
*
* Resolve Systems, LLC
*
* All rights reserved
* This software is distributed under license and may not be used, copied,
* modified, or distributed without the express written permission of
* Resolve Systems, LLC.
*
******************************************************************************/

package com.resolve.rsmgmt;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import com.resolve.rsbase.MainBase;
import com.resolve.connect.DB2DBConnect;
import com.resolve.connect.MySQLDBConnect;
import com.resolve.connect.OracleDBConnect;
import com.resolve.util.Constants;
import com.resolve.util.CryptUtils;
import com.resolve.util.Log;
import com.resolve.util.Properties;
import com.resolve.util.StringUtils;

public class ConfigRSView
{
    private static final String instanceName = "rsview";

    private String user = "resolve";
    
    private Properties rsviewProperties;
    private Properties hazelcastProperties;
    private Properties jtaProperties;
    private Properties ehcacheProperties;
    private Properties runProperties;
    private Properties envProperties;
    private Properties tomcatProperties;
    private Properties axis2Properties;
    private Properties hibernateProperties;
    private Properties sreeProperties;
    private Properties dashboardProperties;
    private Properties neo4jProperties;
    private Properties krb5Properties;
    private Properties webProperties;
    private Properties logProperties;
    private Properties esapiProperties;
    private Properties csrfguardProperties;
    private String hibernateDBType = "MYSQL";
    private String rsviewNodes = "127.0.0.1";
    private String cassandraNodes = "127.0.0.1";
    private String cassandraPort = "9160";
    private boolean primary = true;
    private String localhost = "127.0.0.1";
    private String rssearchEndpoint = "";
    private List<String> implPrefixes;
    private Map<String, String> implPackages;
    private Map<String, Map<String, String>> sdkFieldsMap;
    private Map<String, String> intervalMap;
    private Map<String, String> authTypeMap;

    public ConfigRSView()
    {
        this.rsviewProperties = new Properties();
        this.hazelcastProperties = new Properties();
        this.jtaProperties = new Properties();
        this.ehcacheProperties = new Properties();
        this.runProperties = new Properties();
        this.envProperties = new Properties();
        this.logProperties = new Properties();
        this.tomcatProperties = new Properties();
        this.axis2Properties = new Properties();
        this.hibernateProperties = new Properties();
        this.sreeProperties = new Properties();
        this.dashboardProperties = new Properties();
        this.neo4jProperties = new Properties();
        this.krb5Properties = new Properties();
        this.webProperties = new Properties();
        this.esapiProperties = new Properties();
        this.csrfguardProperties = new Properties();
        this.implPrefixes = new ArrayList<String>();
        this.implPackages = new HashMap<String, String>();
    } // ConfigRSView
    
    public ConfigRSView(Properties properties)
    {
        this();
        
        //System.out.println("Properties: " + properties);
        
        for (Object key : properties.keySet())
        {
            String strKey = key.toString();
            if (strKey.startsWith(instanceName + "."))
            {
                String value = properties.getProperty(strKey).trim();
                if (value.contains("${"))
                {
                    value = properties.getReplacement(key).trim();
                }
                if (value.toUpperCase().startsWith("TO_ENC:"))
                {
                    if (StringUtils.isNotBlank(value.substring(7)) && !value.substring(7).toUpperCase().startsWith("NO_ENC:") && !value.substring(7).matches(".*(?<!\\\\)\\$\\{.*\\}.*"))
                    {
                        try
                        {
                            value = CryptUtils.encrypt(value.substring(7));
                        }
                        catch (Exception e)
                        {
                            Log.log.error("Failed to encrypt blueprint value prefixed with TO_ENC: set for the key " + strKey + 
                                          " due to error " + e.getMessage() + ", setting un-encrypted value into config.xml", e);
                        }
                    }
                }
                if (value.toUpperCase().startsWith("NO_ENC:"))
                {
                    value = value.substring(7);
                }
                if (value.startsWith("ENC"))
                {
                    try
                    {
                        value = CryptUtils.decrypt(value);
                    }
                    catch (Exception e)
                    {
                        Log.log.error("Failed to decrypt blueprint value", e);
                        value = "";
                    }
                }
                if (strKey.contains("brokeraddr") && value.matches(":\\d*"))
                {
                    value = "";
                }
                if (value != null)
                {
                    //System.out.println("strKey: " + strKey);
                    
                    if (strKey.startsWith(instanceName + ".hazelcast") && !value.equals(""))
                    {
                        this.hazelcastProperties.put(key, value);
                    }
                    else if (strKey.startsWith(instanceName + ".hibernate"))
                    {
                        if (strKey.equalsIgnoreCase(instanceName + ".hibernate.dbtype"))
                        {
                            hibernateDBType = value.toUpperCase();
                        }
                        else
                        {
                            hibernateProperties.put(key, value);
                        }
                    }
                    else if (strKey.startsWith(instanceName + ".ehcache"))
                    {
                        this.ehcacheProperties.put(key, value);
                    }
                    else if (strKey.startsWith(instanceName + ".env"))
                    {
                        this.envProperties.put(key, value);
                    }
                    else if (strKey.startsWith(instanceName + ".jta"))
                    {
                        this.jtaProperties.put(key, value);
                        if (strKey.equals(instanceName + ".jta.timeout"))
                        {
                            int timeout = Integer.parseInt(value.trim()) / 1000;
                            
                            rsviewProperties.put(instanceName + ".sql.timeout", "" + timeout);
                        }
                    }
                    else if (strKey.startsWith(instanceName + ".run"))
                    {
                        this.runProperties.put(key, value);
                        if (strKey.equals(instanceName + ".run.R_USER"))
                        {
                            this.user = value;
                        }
                    }
                    else if (strKey.startsWith(instanceName + ".tomcat"))
                    {
                        this.tomcatProperties.put(key, value);
                    }
                    else if (strKey.startsWith(instanceName + ".axis2"))
                    {
                        this.axis2Properties.put(key, value);
                    }
                    else if (strKey.startsWith(instanceName + ".log4j"))
                    {
                        this.logProperties.put(key, value);
                    }
                    else if (strKey.startsWith(instanceName + ".sree"))
                    {
                        this.sreeProperties.put(key, value);
                    }
                    else if (strKey.startsWith(instanceName + ".env"))
                    {
                        this.envProperties.put(key, value);
                    }
                    else if (strKey.startsWith(instanceName + ".dashboard"))
                    {
                        this.dashboardProperties.put(key, value);
                    }
                    else if (strKey.startsWith(instanceName + ".neo4j"))
                    {
                        this.neo4jProperties.put(key, value);
                    }
                    else if (strKey.startsWith(instanceName + ".krb5"))
                    {
                        this.krb5Properties.put(key, value);
                    }
                    else if (strKey.startsWith(instanceName + ".web"))
                    {
                        this.webProperties.put(key, value);
                    }
                    
                    else if (strKey.startsWith(instanceName + ".search.endpoint"))
                    {
                        if (StringUtils.isNotBlank(value))
                        {
                            this.rssearchEndpoint = value;
                        }
                    } else if (strKey.startsWith(instanceName + ".ESAPI"))
                    {
                        //System.out.println("ESAPI property: [" + key + ", " + value + "]");
                        //String esapiValue = properties.getProperty(strKey).trim();
                        this.esapiProperties.put(key, value);
                    }
                    else if (strKey.startsWith(instanceName + ".csrfguard"))
                    {
                        this.csrfguardProperties.put(key, value);
                    }
                    else
                    {
                        if (strKey.equalsIgnoreCase(instanceName + ".general.primary"))
                        {
                            primary = (value.equalsIgnoreCase("true"));
                        }
                        
                        if (strKey.contains("pass") && !strKey.contains("ldap.password_attribute"))
                        {
                            if (StringUtils.isNotBlank(value) && !value.toUpperCase().startsWith("NO_ENC:") && !value.matches(".*(?<!\\\\)\\$\\{.*\\}.*"))
                            {
                                try
                                {
                                    value = CryptUtils.encrypt(value);
                                }
                                catch (Exception e)
                                {
                                    Log.log.error("Failed to encrypt blueprint value for the key " + strKey + 
                                                  " due to error " + e.getMessage() + ", setting un-encrypted value into config.xml", e);
                                }
                            }
                        }
                        this.rsviewProperties.put(key, value);
                    }
                }
            } 
        }
        
	    rsviewNodes = properties.get(Constants.BLUEPRINT_RSVIEW_NODELIST);
        cassandraNodes = properties.getReplacement(Constants.BLUEPRINT_CASSANDRA_NODELIST);
        cassandraPort = properties.get(Constants.BLUEPRINT_CASSANDRA_SEED_PORT);
        
        if (StringUtils.isBlank(rsviewProperties.get(instanceName + ".nosql.seeds")))
        {
            //generate seeds
            if (StringUtils.isNotBlank(cassandraNodes) && StringUtils.isNotBlank(cassandraPort))
            {
                StringBuilder configSeeds = new StringBuilder();
                String[] seeds = cassandraNodes.split(",");
                for (String seed : seeds)
                {
                    if (configSeeds.length() > 0)
                    {
                        configSeeds.append("," + seed + ":" + cassandraPort);
                    }
                    else
                    {
                        configSeeds.append(seed + ":" + cassandraPort);
                    }
                }
                rsviewProperties.put(instanceName + ".nosql.seeds", configSeeds.toString());
            }
        }
        
        localhost = properties.get(Constants.BLUEPRINT_LOCALHOST);
        
        if (StringUtils.isBlank(rssearchEndpoint))
        {
            String endpointNode = properties.get(Constants.BLUEPRINT_LOCALHOST);
            if (!((Main) MainBase.main).isBlueprintRSSearch())
            {
                endpointNode = rsviewProperties.get("rsview.search.serverlist");
                if (endpointNode.indexOf(",") != -1)
                {
                    endpointNode = endpointNode.substring(0, endpointNode.indexOf(",")).trim();
                }
            }
            String endpointPort = rsviewProperties.get("rsview.search.httpport");
            
            rssearchEndpoint = "http://" + endpointNode + ":" + endpointPort;
        }
    } // ConfigRSView
    public Map<String, String> rsviewEnvReplaceValues()
    {
        Map<String, String> result = new HashMap<String, String>();
        
        for (Object key : envProperties.keySet())
        {
            String strKey = key.toString();
            String value = envProperties.getProperty(strKey);
            
            strKey = strKey.replaceFirst(instanceName + ".env.", "");
            value = value.replaceAll("\\\\", "/");
            if (value.equalsIgnoreCase("true") || value.equalsIgnoreCase("false"))
            {
                value = value.toUpperCase();
            }
            
            strKey = strKey.toUpperCase();
            String replaceRegex = strKey + "=.*";
            String replaceString = strKey + "=" + value;

            result.put(replaceRegex, replaceString);
        }
        return result;
    }
    public Map<String, Object> rsviewXPathValues()
    {
        Map<String, Object> result = new HashMap<String, Object>();
        
        for (Object key : rsviewProperties.keySet())
        {
            String strKey = key.toString();
            String value = (String) rsviewProperties.get(key);
            
            strKey = strKey.replaceFirst(instanceName, "").toUpperCase();
            strKey = strKey.replaceAll("(?<!\\\\)\\.", "/");
            strKey = strKey.replaceAll("\\\\\\.", ".");
            
            Pattern pattern = Pattern.compile("(.+)/(.+)/(\\d+)");
            Matcher matcher = pattern.matcher(strKey);
            if (matcher.matches())
            {
                strKey = "." + matcher.group(1);
                String xdocKey = matcher.group(2);
                int listPosition = Integer.parseInt(matcher.group(3))-1;

                List<Map<String, String>> list = (List<Map<String, String>>) result.get(strKey);
                if (list == null)
                {
                    list = new LinkedList<Map<String, String>>();
                    result.put(strKey, list);
                }
                for (int j=list.size(); j<=listPosition; j++)
                {
                    list.add(new HashMap<String, String>());
                }
                Map<String, String> entry = list.get(listPosition);
                entry.put(xdocKey, value);
                list.set(listPosition, entry);
            }
            else
            {
                strKey = strKey.replaceFirst("^(.*)/(.*?)$", ".$1/@$2");
                result.put(strKey, value);
            }
        }
        
        return result;
    } //rsviewXPathValues()
    
    public Map<String, Object> hazelcastXPathValues()
    {
        Map<String, Object> result = new HashMap<String, Object>();
        
        String[] nodeList = rsviewNodes.split(",");
        for (int i=0; i<nodeList.length; i++)
        {
            String node = nodeList[i];
            List<String> tcpIPInterfaces = (List<String>) result.get("./network/join/tcp-ip/interface");
            List<String> tcpIPHostnames = (List<String>) result.get("./network/join/tcp-ip/hostname");
                
            if (node.matches("\\d{1,3}\\.\\d{1,3}\\.\\d{1,3}\\.\\d{1,3}"))
            {
                if (tcpIPInterfaces == null)
                {
                    tcpIPInterfaces = new LinkedList<String>();
                    result.put("./network/join/tcp-ip/interface", tcpIPInterfaces);
                }
                tcpIPInterfaces.add(node);
            }
            else
            {
                if (tcpIPHostnames == null)
                {
                    tcpIPHostnames = new LinkedList<String>();
                    result.put("./network/join/tcp-ip/hostname", tcpIPHostnames);
                }
                tcpIPHostnames.add(node);
            }
        }
        for (Object key : hazelcastProperties.keySet())
        {
            String strKey = key.toString();
            String value = (String) hazelcastProperties.get(key);
            
            strKey = strKey.replaceFirst(instanceName + ".hazelcast", "");
            strKey = strKey.replaceAll("(?<!\\\\)\\.", "/");
            strKey = strKey.replaceAll("\\\\\\.", ".");
                
            Pattern pattern = Pattern.compile("(.+)/(.+)/(\\d+)");
            Matcher matcher = pattern.matcher(strKey);
            if (matcher.matches())
            {
                strKey = "." + matcher.group(1);
                String xdocKey = matcher.group(2);
                int listPosition = Integer.parseInt(matcher.group(3))-1;
    
                List<Map<String, String>> list = (List<Map<String, String>>) result.get(strKey);
                if (list == null)
                {
                    list = new LinkedList<Map<String, String>>();
                    result.put(strKey, list);
                }
                for (int j=list.size(); j<=listPosition; j++)
                {
                    list.add(new HashMap<String, String>());
                }
                Map<String, String> entry = list.get(listPosition);
                entry.put(xdocKey, value);
                list.set(listPosition, entry);
            }
            else
            {
                result.put(strKey, value);
            }
        }
        List<String> tcpIPInterfaces = (List<String>) result.get("./network/join/tcp-ip/interface");
        List<String> tcpIPHostnames = (List<String>) result.get("./network/join/tcp-ip/hostname");
                
        if (tcpIPInterfaces == null && tcpIPHostnames == null)
        {
            tcpIPInterfaces = new LinkedList<String>();
            tcpIPInterfaces.add("127.0.0.1");
            result.put("./network/join/tcp-ip/interface", tcpIPInterfaces);
        }
        
        return result;
    } //hazelcastXPathValues()
    
    public Map<String, Object> ehcacheXPathValues()
    {
        Map<String, Object> result = new HashMap<String, Object>();
        
        for (Object key : ehcacheProperties.keySet())
        {
            String strKey = key.toString();
            String value = (String) ehcacheProperties.get(key);
            
            strKey = strKey.replaceFirst(instanceName + ".ehcache", "");
            strKey = strKey.replaceAll("(?<!\\\\)\\.", "/");
            strKey = strKey.replaceAll("\\\\\\.", ".");
            
            strKey = strKey.replaceFirst("^(.*)/(.*?)$", ".$1/@$2");
            result.put(strKey, value);
        }
                        
        
        return result;
    } //ehcacheXPathValues() 
    
   
    
    public Map<String, String> rsviewLogReplaceValues()
    {
        Map<String, String> result = new HashMap<String, String>();
        
        for (Object key : logProperties.keySet())
        {
            String strKey = key.toString();
            String value = logProperties.getProperty(strKey);
            
            strKey = strKey.replaceFirst(instanceName + ".", "");
            value = value.replaceAll("\\\\", "/");
            
            String replaceRegex = strKey + "=.*";
            String replaceString = strKey + "=" + value;
    
            result.put(replaceRegex, replaceString);
        }
        
        return result;
    } // rsviewLogReplaceValues
    
    public Map<String, String> rsviewRunReplaceValues()
    {
        Map<String, String> result = new HashMap<String, String>();
        
        for (Object key : runProperties.keySet())
        {
            String strKey = key.toString();
            String value = runProperties.getProperty(strKey);
            
            strKey = strKey.replaceFirst(instanceName + ".run.", "");
            
            if (strKey.equalsIgnoreCase("Xms"))
            {
                if (value.endsWith("M"))
                {
                    value = value.substring(0, value.length()-1);
                }
                result.put("Xms\\d+M", "Xms" + value + "M");
                result.put("--JvmMs\\s*\\d+", "--JvmMs " + value);
            }
            else if (strKey.equalsIgnoreCase("Xmx"))
            {
                if (value.endsWith("M"))
                {
                    value = value.substring(0, value.length()-1);
                }
                result.put("Xmx\\d+M", "Xmx" + value + "M");
                result.put("--JvmMx\\s*\\d+", "--JvmMx " + value);
            }
            else
            {
                value = value.replaceAll("\\\\", "/");
                if (value.equalsIgnoreCase("true") || value.equalsIgnoreCase("false"))
                {
                    value = value.toUpperCase();
                }
                
                strKey = strKey.toUpperCase();
                String replaceRegex = strKey + "=.*";
                String replaceString = strKey + "=" + value;
    
                result.put(replaceRegex, replaceString);
            }
        }
        if (tomcatProperties.get(instanceName + ".tomcat.https").equalsIgnoreCase("true") &&
            !StringUtils.isEmpty(tomcatProperties.get(instanceName + ".tomcat.connector.https.keystoreFile")))
        {
            String replaceRegex="KEYSTORE=.*";
            String os = System.getProperty("os.name");
            String replaceString = "";
            if (os.contains("Win"))
            {
	            replaceString = "KEYSTORE=-Djavax.net.ssl.trustStore=" + tomcatProperties.get(instanceName + ".tomcat.connector.https.keystoreFile") + ";";
            }
            else if (os.contains("Linux") || os.contains("SunOS"))
            {
	            replaceString = "KEYSTORE=\"-Djavax.net.ssl.trustStore=" + tomcatProperties.get(instanceName + ".tomcat.connector.https.keystoreFile") + "\"";
            }
            if (StringUtils.isNotEmpty(replaceString))
            {
	            replaceString = replaceString.replaceAll("\\\\", "/");
	            result.put(replaceRegex, replaceString);
            }
        }
        else
        {
            String replaceRegex="KEYSTORE=.*";
            String replaceString = "KEYSTORE=";
            result.put(replaceRegex, replaceString);
        }
        return result;
    } //rsviewRunValues();
    
    public Map<String, Object> tomcatPropertiesReplaceValues()
    {
        Map<String, Object> result = new HashMap<String, Object>();
        
        if (tomcatProperties.get(instanceName + ".tomcat.http").equalsIgnoreCase("true"))
        {
            String replaceRegex="(?s)(?:<!--\\s*)?<Connector port=\"(\\d+)\" protocol=\"HTTP/1\\.1\"(.*?/>)(?:\\s*-->)?";
            String value = "$1";
            if (StringUtils.isNotEmpty(tomcatProperties.get(instanceName + ".tomcat.connector.http.port")))
            {
                value = tomcatProperties.get(instanceName + ".tomcat.connector.http.port");
            }
            String replaceString="<Connector port=\"" + value + "\" protocol=\"HTTP/1\\.1\"$2";
            result.put(replaceRegex, replaceString);
            
            if (StringUtils.isNotEmpty(tomcatProperties.get(instanceName + ".tomcat.connector.http.redirectport")))
            {
                value = tomcatProperties.get(instanceName + ".tomcat.connector.http.redirectport");
                replaceRegex = "redirectPort=\"\\d+\"";
                replaceString = "redirectPort=\"" + value + "\"";
                result.put(replaceRegex, replaceString);
            }
        }
        else
        {
            String replaceRegex="(?s)(?:<!--\\s*)?<Connector port=\"(\\d+)\" protocol=\"HTTP/1\\.1\"(.*?/>)(?:\\s*-->)?";
            String replaceString="<!--\n    <Connector port=\"$1\" protocol=\"HTTP/1\\.1\"$2\n    -->";
            result.put(replaceRegex, replaceString);
        }
        if (tomcatProperties.get(instanceName + ".tomcat.https").equalsIgnoreCase("true"))
        {
            String protocol = "HTTP/1\\.1";
            if (StringUtils.isNotBlank(tomcatProperties.get(instanceName + ".tomcat.connector.protocol")))
            {
                protocol = tomcatProperties.get(instanceName + ".tomcat.connector.protocol");
            }
            String replaceRegex="(?s)(?:<!--\\s*)?<Connector port=\"(\\d+)\" protocol=\"\\S+\" SSLEnabled=\"true\"(.*?/>)(?:\\s*-->)?";
            String value = "$1";
            if (StringUtils.isNotEmpty(tomcatProperties.get(instanceName + ".tomcat.connector.https.port")))
            {
                value = tomcatProperties.get(instanceName + ".tomcat.connector.https.port");
            }
            String replaceString="<Connector port=\"" + value + "\" protocol=\"" + protocol + "\" SSLEnabled=\"true\"$2";
            result.put(replaceRegex, replaceString);
            
            if (StringUtils.isNotEmpty(tomcatProperties.get(instanceName + ".tomcat.connector.https.keystoreFile")))
            {
                value = tomcatProperties.get(instanceName + ".tomcat.connector.https.keystoreFile");
                replaceRegex = "keystoreFile=\".+?\"";
                replaceString = "keystoreFile=\"" + value + "\"";
                result.put(replaceRegex, replaceString);
            }
            if (StringUtils.isNotEmpty(tomcatProperties.get(instanceName + ".tomcat.connector.https.keystorePass")))
            {
                value = tomcatProperties.get(instanceName + ".tomcat.connector.https.keystorePass");
                if (protocol.equals("com.resolve.tomcat.connector.ResolveHttp11Protocol"))
                {
                    try
                    {
                        String tmpValue = CryptUtils.encrypt(value);
                        value = tmpValue;
                    }
                    catch (Exception e)
                    {
                        Log.log.error("Failed to encrypt keystore password, writing plain value");
                    }
                }
                replaceRegex = "keystorePass=\"\\S+?\"";
                replaceString = "keystorePass=\"" + value + "\"";
                result.put(replaceRegex, replaceString);
            }
            if (StringUtils.isNotEmpty(tomcatProperties.get(instanceName + ".tomcat.connector.https.sslEnabledProtocols")))
            {
                value = tomcatProperties.get(instanceName + ".tomcat.connector.https.sslEnabledProtocols");
                replaceRegex = "sslEnabledProtocols=\".+?\"";
                replaceString = "sslEnabledProtocols=\"" + value + "\"";
                result.put(replaceRegex, replaceString);
            }
            if (StringUtils.isNotEmpty(tomcatProperties.get(instanceName + ".tomcat.connector.https.ciphers")))
            {
                value = tomcatProperties.get(instanceName + ".tomcat.connector.https.ciphers");
                replaceRegex = "ciphers=\".+?\"";
                replaceString = "ciphers=\"" + value + "\"";
                result.put(replaceRegex, replaceString);
            }
        }
        else
        {
            String replaceRegex="(?s)(?:<!--\\s*)?<Connector port=\"(\\d+)\" protocol=\"\\S+\" SSLEnabled=\"true\"(.*?/>)(?:\\s*-->)?";
            String replaceString="<!--\n    <Connector port=\"$1\" protocol=\"HTTP/1\\.1\" SSLEnabled=\"true\"$2\n    -->";
            result.put(replaceRegex, replaceString);
        }
        if (StringUtils.isNotEmpty(tomcatProperties.get(instanceName + ".tomcat.server.port")))
        {
            String value = tomcatProperties.get(instanceName + ".tomcat.server.port");
            String replaceRegex = "Server port=\"\\d+";
            String replaceString = "Server port=\"" + value;
            result.put(replaceRegex, replaceString);
        }
        if (StringUtils.isNotEmpty(tomcatProperties.get(instanceName + ".tomcat.connector.ajp.port")))
        {
            String value = tomcatProperties.get(instanceName + ".tomcat.connector.ajp.port");
            String replaceRegex = "port=\"\\d+\"\\s+protocol=\"AJP";
            String replaceString = "port=\"" + value + "\" protocol=\"AJP";
            result.put(replaceRegex, replaceString);
        }
        if (StringUtils.isNotEmpty(tomcatProperties.get(instanceName + ".tomcat.connector.ajp.redirectport")))
        {
            String value = tomcatProperties.get(instanceName + ".tomcat.connector.ajp.redirectport");
            String replaceRegex = "AJP/1.3\" redirectPort=\"\\d+";
            String replaceString = "AJP/1.3\" redirectPort=\"" + value;
            result.put(replaceRegex, replaceString);
        }
        if (StringUtils.isNotEmpty(tomcatProperties.get(instanceName + ".tomcat.connector.maxthreads")))
        {
            String value = tomcatProperties.get(instanceName + ".tomcat.connector.maxthreads");
            String replaceRegex="(?s)(<Connector[^>]*?)maxThreads=\"\\d+";
            String replaceString = "$1maxThreads=\"" + value;
            result.put(replaceRegex, replaceString);
        }
        if (StringUtils.isNotEmpty(tomcatProperties.get(instanceName + ".tomcat.connector.compression")))
        {
            String value = tomcatProperties.get(instanceName + ".tomcat.connector.compression");
            String replaceRegex="(?s)(<Connector[^>]*?)compression=\"\\w+";
            String replaceString = "$1compression=\"" + value;
            result.put(replaceRegex, replaceString);
        }
        if (StringUtils.isNotEmpty(tomcatProperties.get(instanceName + ".tomcat.connector.compressionMinSize")))
        {
            String value = tomcatProperties.get(instanceName + ".tomcat.connector.compressionMinSize");
            String replaceRegex="(?s)(<Connector[^>]*?)compressionMinSize=\"\\d+";
            String replaceString = "$1compressionMinSize=\"" + value;
            result.put(replaceRegex, replaceString);
        }
        if (StringUtils.isNotEmpty(tomcatProperties.get(instanceName + ".tomcat.connector.noCompressionUserAgents")))
        {
            String value = tomcatProperties.get(instanceName + ".tomcat.connector.noCompressionUserAgents");
            String replaceRegex="(?s)(<Connector[^>]*?)noCompressionUserAgents=\"[^\"]+";
            String replaceString = "$1noCompressionUserAgents=\"" + value;
            result.put(replaceRegex, replaceString);
        }
        if (StringUtils.isNotEmpty(tomcatProperties.get(instanceName + ".tomcat.connector.compressableMimeType")))
        {
            String value = tomcatProperties.get(instanceName + ".tomcat.connector.compressableMimeType");
            String replaceRegex="(?s)(<Connector[^>]*?)compressableMimeType=\"[^\"]+";
            String replaceString = "$1compressableMimeType=\"" + value;
            result.put(replaceRegex, replaceString);
        }
 
        
        return result;
    } //tomcatPropertiesReplaceValues()
    
    public Map<String, Object> axis2PropertiesReplaceValues()
    {
        Map<String, Object> result = new HashMap<String, Object>();
        
        if (axis2Properties.get(instanceName + ".axis2.http").equalsIgnoreCase("true"))
        {
            String replaceRegex="(?s)(?:<!--\\s*)?<transportReceiver name=\"http\"(.*?\"port\">)(\\d+)(.*?/transportReceiver>)(?:\\s*-->)?";
            String value = "$2";
            if (!StringUtils.isEmpty(axis2Properties.get(instanceName + ".axis2.connector.http.port")))
            {
                value = axis2Properties.get(instanceName + ".axis2.connector.http.port");
            }
            String replaceString="<transportReceiver name=\"http\"$1" + value + "$3";
            result.put(replaceRegex, replaceString);
        }
        else
        {
            String replaceRegex="(?s)(?:<!--\\s*)?<transportReceiver name=\"http\"(.*?\"port\">)(\\d+)(.*?/transportReceiver>)(?:\\s*-->)?";
            String replaceString="<!--\n<transportReceiver name=\"http\"$1$2$3\n-->";
            result.put(replaceRegex, replaceString);
        }
        if (axis2Properties.get(instanceName + ".axis2.https").equalsIgnoreCase("true"))
        {
            String replaceRegex="(?s)(?:<!--\\s*)?<transportReceiver name=\"https\"(.*?\"port\">)(\\d+)(.*?/transportReceiver>)(?:\\s*-->)?";
            String value = "$1";
            if (!StringUtils.isEmpty(axis2Properties.get(instanceName + ".axis2.connector.https.port")))
            {
                value = axis2Properties.get(instanceName + ".axis2.connector.https.port");
            }
            String replaceString="<transportReceiver name=\"https\"$1" + value + "$3";
            result.put(replaceRegex, replaceString);
        }
        else
        {
            String replaceRegex="(?s)(?:<!--\\s*)?<transportReceiver name=\"https\"(.*?\"port\">)(\\d+)(.*?/transportReceiver>)(?:\\s*-->)?";
            String replaceString="<!--\n<transportReceiver name=\"https\"$1$2$3\n-->";
            result.put(replaceRegex, replaceString);
        }
        
        return result;
    }//axis2PropertiesReplaceValues
    
    public Map<String, String> sreeReplaceValues()
    {
        Map<String, String> result = new HashMap<String, String>();
        
        for (Object key : sreeProperties.keySet())
        {
            
            String strKey = key.toString();
            String value = sreeProperties.getProperty(strKey);
            value = value.replaceAll("\\\\", "\\\\\\\\"); //replace \ with \\
            
            if (strKey.contains("classpath"))
            {
	            String os = System.getProperty("os.name");
	            if (os.contains("Linux") || os.contains("SunOS"))
	            {
	                value = value.replaceAll(";", ":");
	            }
            }
            value = value.replaceAll(":", "\\\\:"); // replace : with \:
            
            strKey = strKey.replaceFirst(instanceName + ".sree.", "");
            
            strKey = "(?i)(" + strKey + ")=.*";
            value = "$1=" + Matcher.quoteReplacement(value);
            
            result.put(strKey, value);
        }
        return result;
    } //sreeReplaceValues();
    
    public Map<String, String> dashboardReplaceValues()
    {
        Map<String, String> result = new HashMap<String, String>();
        
        String dbType = "";
        String driver = "";
        String url = "";
        String host = "";
        String dbname = "";
        
        for (Object key : dashboardProperties.keySet())
        {
            String strKey = key.toString();
            String value = dashboardProperties.getProperty(strKey);
            
	        if (strKey.equals(instanceName + ".dashboard.dbname"))
	        {
	            dbname = value;
	        }
	        else if (strKey.equals(instanceName + ".dashboard.dbtype"))
            {
		        dbType = value;
            }
            else if (strKey.equals(instanceName + ".dashboard.url"))
            {
                url = value;
            }
            else if (strKey.equals(instanceName + ".dashboard.host"))
            {
                host = value;
            }
            else
            {
                strKey = strKey.replaceFirst(instanceName + ".dashboard.", "");
                
		        String replaceRegex = strKey + "=.*";
		        String replaceValue = strKey + "=" + value;
		        result.put(replaceRegex, replaceValue);
            }
        }
        
        if (dbType.equalsIgnoreCase("MYSQL"))
        {
            driver = Constants.MYSQLDRIVER;
            if (StringUtils.isEmpty(url))
            {
                url = MySQLDBConnect.getConnectURL(host, dbname);
            }
        }
        else if (dbType.toUpperCase().startsWith("ORACLE"))
        {
            driver = Constants.ORACLEDRIVER;
            if (StringUtils.isEmpty(url))
            {
                url = OracleDBConnect.getConnectURL(host, dbname);
            }
        }
        else if (dbType.equalsIgnoreCase("DB2"))
        {
            driver = Constants.DB2DRIVER;
            if (StringUtils.isEmpty(url))
            {
                url = DB2DBConnect.getConnectURL(host, dbname);
            }
        }
        
        String replaceRegex = "url=.*";
        String replaceValue = "url=" + url;
        result.put(replaceRegex, replaceValue);
        
        replaceRegex = "driver=.*";
        replaceValue = "driver=" + driver;
        result.put(replaceRegex, replaceValue);
        
        return result;
    } //dashboardReplaceValues
    
    public Map<String, String> hibernateReplaceValues()
    {
        Map<String, String> result = new HashMap<String, String>();
        
        for (Object key : hibernateProperties.keySet())
        {
            String strKey = key.toString();
            String value = hibernateProperties.getProperty(strKey);
            
            strKey = strKey.replaceFirst(instanceName + ".hibernate.", "");
            
            result.put(strKey, value);
        }
        return result;
    } //hibernateReplaceValues();
    
    public Map<String, String> rsviewNeo4jReplaceValues()
    {
        Map<String,String> result = new HashMap<String,String>();
        
        for (Object key : neo4jProperties.keySet())
        {
            String strKey = key.toString();
            String value = neo4jProperties.get(strKey);
            
            strKey = strKey.replaceFirst(instanceName + ".neo4j.", "");
            
            if (strKey.equalsIgnoreCase("coordinator.port") && value.length() > 1)
            {
                if (!neo4jProperties.containsKey(instanceName + ".neo4j.ha.cluster_server"))
                {
                    String replaceRegex = "ha.cluster_server=.*";
                    String replaceString = "ha.cluster_server=" + localhost + ":" + value;
                    result.put(replaceRegex, replaceString);
                }
                if (!neo4jProperties.containsKey(instanceName + ".neo4j.ha.initial_hosts"))
                {
                    String[] nodeAry = rsviewNodes.split(",");
                    StringBuilder replaceString = new StringBuilder("ha.initial_hosts=");
                    for(int i=1; i<=nodeAry.length; i++)
                    {
                        if (i > 1)
                        {
                            replaceString.append(",");
                        }
                        replaceString.append(nodeAry[i-1] + ":" + value);
                    }
                    String replaceRegex = "ha.initial_hosts=.*";
                    result.put(replaceRegex, replaceString.toString());
                }
            }
            else if (strKey.equalsIgnoreCase("ha.read_timeout") || strKey.equalsIgnoreCase("ha.lock_read_timeout")
                            || strKey.equalsIgnoreCase("ha.state_switch_timeout"))
            {
                String replaceRegex = "(?m)^#?" + strKey + "=.*";
                String replaceString = strKey + "=" + value;
                if (rsviewNodes.split(",").length == 1)
                {
                    replaceString = "#" + replaceString;
                }
                result.put(replaceRegex, replaceString);
            }
            else
            {
                String replaceRegex = strKey + "=.*";
                String replaceString = strKey + "=" + value;
    
                result.put(replaceRegex, replaceString);
            }
        }
        
        return result;
    } //rsviewNeo4jReplaceValues 
    
    public Map<String, String> rsviewKrb5ReplaceValues()
    {
        Map<String, String> result = new HashMap<String, String>();
        
        for (Object key : krb5Properties.keySet())
        {
            String strKey = key.toString();
            String value = krb5Properties.getProperty(strKey);
            
            strKey = strKey.replaceFirst(instanceName + ".krb5.", "");
            
            if (strKey.equals("default_realm"))
            {
                result.put("(?ms)\\[realms\\](\\s+)[\\w\\.]+( = \\{.*)", "[realms]$1" + value + "$2");
            }
            if (strKey.equals("default_domain"))
            {
                result.put("(?ms)\\[domain_realm\\](\\s+).*", "[domain_realm]$1." + value + " = " + value);
            }
            
            String replaceRegex = strKey + "\\s+=.*";
            String replaceString = strKey + " = " + value;
    
            result.put(replaceRegex, replaceString);
        }
        return result;
    } //krb5ReplaceValues();
    
    public Map<String, String> rsviewWebReplaceValues()
    {
        Map<String, String> result = new HashMap<String, String>();
        
        for (Object key : webProperties.keySet())
        {
            String strKey = key.toString();
            String value = webProperties.getProperty(strKey);
            
            strKey = strKey.replaceFirst(instanceName + ".web.", "");
            
            
            if (strKey.equals("httpsForward"))
            {
                String replaceRegex = "(<transport-guarantee>).*(</transport-guarantee>)"; //regex here
                String replaceString = "$1NONE$2";
                if(value.equalsIgnoreCase("true"))
                {
                    replaceString = "$1CONFIDENTIAL$2";
                }
                result.put(replaceRegex, replaceString);
            }
            
        }
        return result;
    } //rsviewWebReplaceValues();
    
    public Map<String, String> rsviewEsapiReplaceValues()
    {
        Map<String, String> result = new HashMap<String, String>();
        
        for (Object key : esapiProperties.keySet())
        {            
            String strKey = key.toString();
            String value = esapiProperties.getProperty(strKey);
            
            strKey = strKey.replaceFirst(instanceName + ".ESAPI.", "");

            // don't replace \\ with / if it's a validator property. It messes with the regex.
            if(strKey.startsWith("Validator")) {
                value = value.replace("\\", "\\\\");
            } else {
                value = value.replace("\\\\", "/");
            }
            
            String replaceRegex = strKey + "=.*";
            String replaceString = strKey + "=" + value;
            
            result.put(replaceRegex, replaceString);
            
        }
        
        if(tomcatProperties.getProperty("rsview.tomcat.https").equalsIgnoreCase("true") && tomcatProperties.getProperty("rsview.tomcat.http").equalsIgnoreCase("true")) {
            
            String replaceRegex = "HttpUtilities.ForceSecureCookies=.*";
            String replaceString = "HttpUtilities.ForceSecureCookies=false";
            result.put(replaceRegex, replaceString);
            
        } else if(tomcatProperties.getProperty("rsview.tomcat.https").equalsIgnoreCase("true") && tomcatProperties.getProperty("rsview.tomcat.http").equalsIgnoreCase("false")) {
            
            String replaceRegex = "HttpUtilities.ForceSecureCookies=.*";
            String replaceString = "HttpUtilities.ForceSecureCookies=true";
            result.put(replaceRegex, replaceString);
            
        }  
      
        return result;
    } //rsviewEsapiReplaceValues
    
    @SuppressWarnings("unchecked")
    public List<String> rsviewCsrfguardUnprotectedValues()
    {
        List<String> result = new LinkedList<String>();
        
        for (Object key : csrfguardProperties.keySet())
        {
            String strKey = key.toString();
            String value = (String) csrfguardProperties.get(key);
            
            Pattern pattern = Pattern.compile(instanceName + "\\.csrfguard\\.unprotected\\.(\\d+)");
            Matcher matcher = pattern.matcher(strKey);
            if (matcher.matches())
            {
                int listPosition = Integer.parseInt(matcher.group(1))-1;

                for (int j=result.size(); j<=listPosition; j++)
                {
                    result.add(null);
                }
                result.set(listPosition, value);
            }
            else
            {
                Log.log.warn("CSRFGuard property " + strKey + " Does not Appear to be an Unprotected Setting, Skipping");
            }
        }
        
        return result;
    } //rsviewCsrfguardUnprotectedValue
    
    public String getHibernateDBType()
    {
        return hibernateDBType;
    } // getHibernateDBType
    
    public Properties getRsviewProperties()
    {
        return rsviewProperties;
    } // getRsviewProperties

    public void setRsviewProperties(Properties properties)
    {
        this.rsviewProperties = properties;
    } //setRsviewProperties

    public Properties getHazelcastProperties()
    {
        return hazelcastProperties;
    } //getHazelcastProperties

    public void setHazelcastProperties(Properties hazelcastProperties)
    {
        this.hazelcastProperties = hazelcastProperties;
    } //setHazelcastProperties

    public Properties getEhcacheProperties()
    {
        return ehcacheProperties;
    } //getEhcacheProperties

    public void setEhcacheProperties(Properties ehcacheProperties)
    {
        this.ehcacheProperties = ehcacheProperties;
    } //setEhcacheProperties

    public Properties getJtaProperties()
    {
        return jtaProperties;
    } //getJtaProperties

    public void setJtaProperties(Properties jtaProperties)
    {
        this.jtaProperties = jtaProperties;
    } //setJtaProperties
    
    public Properties getRunProperties()
    {
        return runProperties;
    } //getRunProperties

    public void setRunProperties(Properties runProperties)
    {
        this.runProperties = runProperties;
    } //setRunProperties

    public boolean isPrimary()
    {
        return primary;
    } //isPrimary

    public void setPrimary(boolean primary)
    {
        this.primary = primary;
    } //setPrimary

    public Properties getTomcatProperties()
    {
        return tomcatProperties;
    } //getTomcatProperties

    public void setTomcatProperties(Properties tomcatProperties)
    {
        this.tomcatProperties = tomcatProperties;
    } //setTomcatProperties

    public Properties getAxis2Properties()
    {
        return axis2Properties;
    } //getAxis2Properties

    public void setAxis2Properties(Properties axis2Properties)
    {
        this.axis2Properties = axis2Properties;
    } //setAxis2Properties

    public Properties getHibernateProperties()
    {
        return hibernateProperties;
    } //getHibernateProperties

    public void setHibernateProperties(Properties hibernateProperties)
    {
        this.hibernateProperties = hibernateProperties;
    } //setHibernateProperties

    public Properties getSreeProperties()
    {
        return sreeProperties;
    } //getSreeProperties

    public void setSreeProperties(Properties sreeProperties)
    {
        this.sreeProperties = sreeProperties;
    } //setSreeProperties

    public Properties getDashboardProperties()
    {
        return dashboardProperties;
    } //getDashboardProperties

    public void setDashboardProperties(Properties dashboardProperties)
    {
        this.dashboardProperties = dashboardProperties;
    } //setDashboardProperties

    public Properties getNeo4jProperties()
    {
        return neo4jProperties;
    } //getNeo4jProperties

    public void setNeo4jProperties(Properties neo4jProperties)
    {
        this.neo4jProperties = neo4jProperties;
    } //setNeo4jProperties

    public String getCassandraNodes()
    {
        return cassandraNodes;
    }//getCassandraNodes

    public void setCassandraNodes(String cassandraNodes)
    {
        this.cassandraNodes = cassandraNodes;
    }//setCassandraNodes

    public String getCassandraPort()
    {
        return cassandraPort;
    }//getCassandraPort

    public void setCassandraPort(String cassandraPort)
    {
        this.cassandraPort = cassandraPort;
    }//setCassandraPort
    
    public String getRSSearchEndpoint()
    {
        return rssearchEndpoint;
    }//getRSSearchEndpoint
    
    public List<String> getImplPrefixes()
    {
        return implPrefixes;
    } // getImplPrfixes
    
    public void setImplPrefixes(List<String> implPrefixes)
    {
        this.implPrefixes = implPrefixes;
    } // setimplPrefixes
    
    public Map<String, String> getImplPackages()
    {
        return implPackages;
    }

    public void setImplPackages(Map<String, String> implPackages)
    {
        this.implPackages = implPackages;
    }
    
    public void setSdkFieldsMap(Map<String, Map<String, String>> sdkFieldsMap)
    {
        this.sdkFieldsMap = sdkFieldsMap;
    }
    
    public Map<String, Map<String, String>> getSdkFieldsMap()
    {
        return this.sdkFieldsMap;
    }
    
    public void setEsapiProperties(Properties esapiProperties)
    {
        this.esapiProperties = esapiProperties;
    }
    
    public Properties getEsapiProperties()
    {
        return this.esapiProperties;
    }

    public void setUser(String user)
    {
        this.user = user;
    }

    public String getUser()
    {
        return this.user;
    }
    
    public void setIntervalMap(Map<String, String> intervalMap)
    {
        this.intervalMap = intervalMap;
    }
    
    public Map<String, String> getIntervalMap()
    {
        return this.intervalMap;
    }

    public void setAuthTypeMap(Map<String, String> authTypeMap)
    {
       this.authTypeMap = authTypeMap;
        
    }
    public Map<String, String> getAuthTypeMap()
    {
        return this.authTypeMap;
    }

} //ConfigRSView
