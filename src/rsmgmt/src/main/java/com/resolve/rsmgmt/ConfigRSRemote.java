/******************************************************************************
* (C) Copyright 2014
*
* Resolve Systems, LLC
*
* All rights reserved
* This software is distributed under license and may not be used, copied,
* modified, or distributed without the express written permission of
* Resolve Systems, LLC.
*
******************************************************************************/

package com.resolve.rsmgmt;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import com.resolve.util.CryptUtils;
import com.resolve.util.Log;
import com.resolve.util.Properties;
import com.resolve.util.StringUtils;

public class ConfigRSRemote
{
    private String instanceName = "rsremote";
    
    private String user = "resolve";
    
    private Properties rsremoteProperties;
    private Properties runProperties;
    private Properties envProperties;
    private Properties logProperties;
    private Properties esapiProperties;
    // SDK developed gateways "implprefix" properties
    private List<String> implPrefixes;
    private Map<String, String> implPackages;
    private Map<String, Map<String, String>> fieldsMap;
    // List of Active Gateway Queue Names
    private List<String> activeGatewayQNames;
    private Map<String, String> intervalMap;
    private Map<String, String> authTypeMap;
    public ConfigRSRemote()
    {
        this.rsremoteProperties = new Properties();
        this.runProperties = new Properties();
        this.envProperties = new Properties();
        this.logProperties = new Properties();
        this.implPrefixes = new ArrayList<String>();
        this.implPackages = new HashMap<String, String>();
        this.fieldsMap = new HashMap<String, Map<String, String>>();
        this.activeGatewayQNames = new ArrayList<String>();
        this.esapiProperties = new Properties();
        this.intervalMap = new HashMap<String, String>();
        this.authTypeMap = new HashMap<String, String>();

    } // ConfigRSRemote
    
    public ConfigRSRemote(Properties properties)
    {
        this(properties, null);
    } // ConfigRSRemote
    
    public ConfigRSRemote(Properties properties, String instanceName)
    {
        this();
        if (instanceName != null)
        {
	        this.instanceName = instanceName;
        }
        for (Object key : properties.keySet())
        {
            String strKey = key.toString();
            if (strKey.startsWith(instanceName + ".") && !strKey.contains(".instance"))
            {
                String value = properties.getProperty(strKey).trim();
                if (value.contains("${"))
                {
                    value = properties.getReplacement(key).trim();
                }
                if (value.toUpperCase().startsWith("TO_ENC:"))
                {
                    if (StringUtils.isNotBlank(value.substring(7)) && !value.substring(7).toUpperCase().startsWith("NO_ENC:") && !value.substring(7).matches(".*(?<!\\\\)\\$\\{.*\\}.*"))
                    {
                        try
                        {
                            value = CryptUtils.encrypt(value.substring(7));
                        }
                        catch (Exception e)
                        {
                            Log.log.error("Failed to encrypt blueprint value prefixed with TO_ENC: set for the key " + strKey + 
                                          " due to error " + e.getMessage() + ", setting un-encrypted value into config.xml", e);
                        }
                    }
                }
                if (value.toUpperCase().startsWith("NO_ENC:"))
                {
                    value = value.substring(7);
                }
                if (value.startsWith("ENC"))
                {
                    try
                    {
                        value = CryptUtils.decrypt(value);
                    }
                    catch (Exception e)
                    {
                        Log.log.error("Failed to decrypt blueprint value", e);
                        value = "";
                    }
                }
                if (strKey.contains("brokeraddr") && value.matches(":\\d*"))
                {
                    value = "";
                }
                if (value != null)
                {
	                if (strKey.startsWith(instanceName + ".run"))
	                {
	                    this.runProperties.put(key, value);
                        if (strKey.equals(instanceName + ".run.R_USER"))
                        {
                            this.user = value;
                        }
	                }
	                else if (strKey.startsWith(instanceName + ".log4j"))
	                {
	                    this.logProperties.put(key, value);
	                }
	                else if (strKey.startsWith(instanceName + ".env"))
                    {
                        this.envProperties.put(key, value);
                    }
	                else if (strKey.startsWith(instanceName + ".ESAPI"))
	                {
	                    //String esapiValue = properties.getProperty(strKey).trim();
	                    this.esapiProperties.put(key, value);
	                }
	                else if (strKey.startsWith(instanceName + ".receive.") && (strKey.endsWith(".active")) &&
	                         value.equalsIgnoreCase("true"))
                    {
	                    this.rsremoteProperties.put(strKey, value);
	                    
	                    if (properties.containsKey(strKey.substring(0, strKey.length() - 6) + "queue") &&
                            StringUtils.isNotBlank(properties.get(strKey.substring(0, strKey.length() - 6) + "queue")))
                        {
	                        activeGatewayQNames.add(properties.get(strKey.substring(0, strKey.length() - 6) + "queue"));
                        }
                    }
	                else
	                {
	                    if (!strKey.contains(".sdkfield") && !strKey.contains(".sdkdatatype"))
	                    {
	                        if (strKey.contains("pass") && !strKey.contains("ldap.password_attribute"))
	                        {
	                            if (StringUtils.isNotBlank(value) && !value.toUpperCase().startsWith("NO_ENC:") && !value.matches(".*(?<!\\\\)\\$\\{.*\\}.*"))
	                            {
	                                try
                                    {
                                        value = CryptUtils.encrypt(value);
                                    }
                                    catch (Exception e)
                                    {
                                        Log.log.error("Failed to encrypt blueprint value for the key " + strKey + 
                                                      " due to error " + e.getMessage() + ", setting un-encrypted value into config.xml", e);
                                    }
	                            }
	                        }
	                        this.rsremoteProperties.put(key, value);
	                        
	                        if (strKey.endsWith(".interval"))
	                        {
	                            String gwName = StringUtils.substringBetween(strKey, instanceName + ".receive.", ".interval");
	                            
	                            if (StringUtils.isNotBlank(gwName))
	                            {
	                                intervalMap.put(gwName, value);
	                            }
	                        }
	                    }
	                    
	                    if(strKey.endsWith(".httpAuthType"))
	                    {
	                         

	                        String[] pth = StringUtils.splitByWholeSeparator(strKey, ".");
	                        if(pth.length > 1)
	                        {
	                            authTypeMap.put(pth[pth.length-2], value);
	                        }
	                        
	                    }
	                    
	                    if (strKey.endsWith("implprefix"))
	                    {
	                        this.implPrefixes.add(value);
	                        
	                        String prefix = value.toLowerCase();
	                        String menuTitle = properties.get(instanceName + ".receive." + prefix + ".menutitle");
	                        String fieldCount = properties.get(instanceName + ".receive." + prefix + ".sdkfield.count");
	                        String packageName = properties.get(instanceName + ".receive." + prefix + ".package");
	                        this.implPackages.put(value, packageName);
	                        
	                        if (fieldCount != null && fieldCount.matches("\\d+"))
	                        {
	                            int count = Integer.parseInt(fieldCount);
	                            for (int i=1; i<=count; i++)
	                            {
	                                Map<String, String> fieldMap = new HashMap<String, String>();
	                                String keyString = instanceName + ".receive." + prefix + ".sdkfield" + i;
	                                String name = properties.get(keyString + ".name");
	                                
	                                if (StringUtils.isNotBlank(name))
	                                {
    	                                String type = properties.get(keyString + ".type");
    	                                String position = properties.get(keyString + ".position");
    	                                String values = properties.get(keyString + ".values");
    	                                String defaultValue = properties.get(keyString + ".defaultvalue");
    	                                String displayName = properties.get(keyString + ".displayname");
    	                                //Adding the required field
    	                                String isRequired = properties.get(keyString + ".required");
    	                                
    	                                fieldMap.put("displayName", displayName);
    	                                fieldMap.put("name", name);
    	                                fieldMap.put("type", type);
    	                                fieldMap.put("position", position);
    	                                fieldMap.put("values", values);
    	                                fieldMap.put("defaultValue", defaultValue);
    	                                fieldMap.put("implPrefixes", value);
    	                                fieldMap.put("menuTitle", menuTitle);
    	                                //Adding the required field 
    	                                fieldMap.put("required", isRequired);
    	                                
    	                                fieldsMap.put(prefix + ".sdkfield" + i, fieldMap);
	                                }
	                            }
	                        }
	                    }
	                }
                }
            } 
        }
    } // ConfigRSRemote
    
    public Map<String, String> rsremoteEsapiReplaceValues()
    {
        Map<String, String> result = new HashMap<String, String>();
       
        for (Object key : esapiProperties.keySet())
        {            
            String strKey = key.toString();
            String value = esapiProperties.getProperty(strKey);
            
            strKey = strKey.replaceFirst(instanceName + ".ESAPI.", "");

            // don't replace \\ with / if it's a validator property. It messes with the regex.
            if(strKey.startsWith("Validator")) {
                value = value.replace("\\", "\\\\");
            } else {
                value = value.replace("\\\\", "/");
            }
            
            String replaceRegex = strKey + "=.*";
            String replaceString = strKey + "=" + value;
            
            result.put(replaceRegex, replaceString);
        }
        
        return result;
    } //rsviewEsapiReplaceValues
    
    @SuppressWarnings("unchecked")
    public Map<String, Object> rsremoteXPathValues()
    {
        Map<String, Object> result = new HashMap<String, Object>();
        
        for (Object key : rsremoteProperties.keySet())
        {
            String strKey = key.toString();
            String value = (String) rsremoteProperties.get(key);
            
            strKey = strKey.replaceFirst(instanceName, "").toUpperCase();
            strKey = strKey.replaceAll("(?<!\\\\)\\.", "/");
            strKey = strKey.replaceAll("\\\\\\.", ".");
            
            Pattern pattern = Pattern.compile("(.+)/(.+)/(\\d+)");
            Matcher matcher = pattern.matcher(strKey);
            if (matcher.matches())
            {
                strKey = "." + matcher.group(1);
                String xdocKey = matcher.group(2);
                int listPosition = Integer.parseInt(matcher.group(3))-1;

                List<Map<String, String>> list = (List<Map<String, String>>) result.get(strKey);
                if (list == null)
                {
                    list = new LinkedList<Map<String, String>>();
                    result.put(strKey, list);
                }
                for (int j=list.size(); j<=listPosition; j++)
                {
                    list.add(new HashMap<String, String>());
                }
                Map<String, String> entry = list.get(listPosition);
                entry.put(xdocKey, value);
                list.set(listPosition, entry);
            }
            else
            {
                strKey = strKey.replaceFirst("^(.*)/(.*?)$", ".$1/@$2");
                result.put(strKey, value);
            }
        }
        
        return result;
    } //rsremoteXPathValues()
    
    public Map<String, String> rsremoteEnvReplaceValues()
    {
        Map<String, String> result = new HashMap<String, String>();
        
        for (Object key : envProperties.keySet())
        {
            String strKey = key.toString();
            String value = envProperties.getProperty(strKey);
            
            strKey = strKey.replaceFirst(instanceName + ".env.", "");
            value = value.replaceAll("\\\\", "/");
            if (value.equalsIgnoreCase("true") || value.equalsIgnoreCase("false"))
            {
                value = value.toUpperCase();
            }
            
            strKey = strKey.toUpperCase();
            String replaceRegex = strKey + "=.*";
            String replaceString = strKey + "=" + value;

            result.put(replaceRegex, replaceString);
        }
        return result;
    }
    
    public Map<String, String> rsremoteRunReplaceValues()
    {
        Map<String, String> result = new HashMap<String, String>();
        
        for (Object key : runProperties.keySet())
        {
            String strKey = key.toString();
            String value = runProperties.getProperty(strKey);
            
            strKey = strKey.replaceFirst(instanceName + ".run.", "");
            
            if (strKey.equalsIgnoreCase("Xms") || strKey.equalsIgnoreCase("mms"))
            {
                if (value.endsWith("M"))
                {
                    value = value.substring(0, value.length()-1);
                }
                result.put("Xms\\d+M", "Xms" + value + "M");
                result.put("MMS=.*", "MMS=" + value);
            }
            else if (strKey.equalsIgnoreCase("Xmx") || strKey.equalsIgnoreCase("mmx"))
            {
                if (value.endsWith("M"))
                {
                    value = value.substring(0, value.length()-1);
                }
                result.put("Xmx\\d+M", "Xmx" + value + "M");
                result.put("MMX=.*", "MMX=" + value);
            }
            else
            {
	            value = value.replaceAll("\\\\", "/");
	            if (value.equalsIgnoreCase("true") || value.equalsIgnoreCase("false"))
	            {
	                value = value.toUpperCase();
	            }
	            
	            strKey = strKey.toUpperCase();
	            String replaceRegex = strKey + "=.*";
	            String replaceString = strKey + "=" + value;
	
	            result.put(replaceRegex, replaceString);
            }
        }
        return result;
    } //rsremoteRunReplaceValues
    
    public Map<String, String> rsremoteLogReplaceValues()
    {
        Map<String, String> result = new HashMap<String, String>();
        
        for (Object key : logProperties.keySet())
        {
            String strKey = key.toString();
            String value = logProperties.getProperty(strKey);
            
            strKey = strKey.replaceFirst(instanceName + ".", "");
            value = value.replaceAll("\\\\", "/");
	            
            String replaceRegex = strKey + "=.*";
            String replaceString = strKey + "=" + value;

            result.put(replaceRegex, replaceString);
        }
        
        return result;
    } //rsremoteLogReplaceValues
    
    public boolean isAmqpAppenderEnabled() {
        return new Boolean(logProperties.get("rsremote.log4j.amqp.appender"));
    }
    
    public Properties getRsremoteProperties()
    {
        return rsremoteProperties;
    } // getRsremoteProperties

    public void setRsremoteProperties(Properties properties)
    {
        this.rsremoteProperties = properties;
    } //setRsremoteProperties

    public Properties getRunProperties()
    {
        return runProperties;
    } //getRunProperties

    public void setRunProperties(Properties hazelcastProperties)
    {
        this.runProperties = hazelcastProperties;
    } //setRunProperties

    public String getInstanceName()
    {
        return instanceName;
    } // getInstanceName

    public void setInstanceName(String instanceName)
    {
        this.instanceName = instanceName;
    } // setIntanceName
    
    public List<String> getImplPrefixes()
    {
        return implPrefixes;
    } // getImplPrefixes
    
    public void setImplPrefixes(List<String> implPrefixes)
    {
        this.implPrefixes = implPrefixes;
    } // setImplPrefixes
    
    public Map<String, String> getImplPackages()
    {
        return implPackages;
    }

    public void setImplPackages(Map<String, String> implPackages)
    {
        this.implPackages = implPackages;
    }
    
    public Map<String, Map<String, String>> getFieldsMap()
    {
        return fieldsMap;
    } // getFieldsMap
    
    public List<String> getActiveGatewayQNames()
    {
        return activeGatewayQNames;
    } // getActiveGatewayQNames
    
    public void setActiveGatewayQNames(List<String> activeGatewayQNames)
    {
        this.activeGatewayQNames = activeGatewayQNames;
    } // setActiveGatewayQNames
    
    public void setEsapiProperties(Properties esapiProperties)
    {
        this.esapiProperties = esapiProperties;
    }
    
    public Properties getEsapiProperties()
    {
        return this.esapiProperties;
    }

    public void setUser(String user)
    {
        this.user = user;
    }

    public String getUser()
    {
        return this.user;
    }
    
    public Map<String, String> getIntervalMap()
    {
        return intervalMap;
    } // getIntervalMap

    public Map<String, String> getAuthTypeMap()
    {
        return authTypeMap;
    }
}//ConfigRSRemote
