package com.resolve.rsmgmt;

import java.io.BufferedWriter;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.io.UnsupportedEncodingException;
import java.net.HttpURLConnection;
import java.net.URL;
import java.net.URLEncoder;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Map;
import java.util.Set;

import com.resolve.util.Log;
import com.resolve.util.StringUtils;

public class AlertHttp
{
    private static String DATE_FORMAT = "MMM d, yyyy HH:mm:ss";
    private static String hostName;
    private static String port;
    private static String detail;
    private static String summary;
    private static String longDate;
    private static String date;
    private static String uri;
    private static String protocol;
    private static String source;
    private static String component;
    
    @SuppressWarnings({ "unchecked", "rawtypes" })
    public static void alert(ConfigAlert configAlert, Map params)
    {
        hostName = configAlert.getHttpHost();
        port = configAlert.getHttpPort();
        detail = (String)params.get("MESSAGE");
        
        if (StringUtils.isNotBlank(detail))
        {
            summary = detail.split("\n")[0];
        }
        
        component = (String)params.get("COMPONENT");
        if (StringUtils.isNotBlank(component))
        {
            String compSource[] = component.split("/");
            component = compSource[0];
            if (compSource.length > 1)
            {
                source = compSource[1];
            }
        }
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat(DATE_FORMAT);
        Date now = new Date();
        longDate = now.getTime()+"";
        date = simpleDateFormat.format(now);
        uri = configAlert.getHttpUri();
        protocol = configAlert.getHttpProtocol();
        
        params.put("COMPONENT", component);
        params.put("SOURCE", source);
        params.put("DETAIL", detail);
        params.put("SUMMARY", summary);
        params.put("LONGDATE", longDate);
        params.put("DATE", date);
        
        Log.log.debug("HTTP Alert Params: " + params);
        
        notifyMCP(params);
    }
    
    @SuppressWarnings("rawtypes")
    private static void notifyMCP(Map params)
    {
        if (validate())
        {
            URL url = null;
            StringBuilder requestURL = new StringBuilder(protocol).append("://");
            requestURL.append(hostName);
            if (StringUtils.isNotBlank(port))
            {
                requestURL.append(":").append(port);
            }
            if (StringUtils.isNotBlank(uri))
            {
                requestURL.append("/").append(uri.trim());
            }
            
            BufferedWriter writer = null;
            OutputStream outputStream = null;
            try
            {
                url = new URL(requestURL.toString());
    
                HttpURLConnection connection = (HttpURLConnection) url.openConnection();
                connection.setReadTimeout(15000);
                connection.setConnectTimeout(15000);
                connection.setRequestMethod("POST");
                connection.setDoOutput(true);
                
                outputStream = connection.getOutputStream();
                writer = new BufferedWriter(new OutputStreamWriter(outputStream, "UTF-8"));
                writer.write(getPostDataString(params));
                writer.flush();
                
                int rc = connection.getResponseCode();
                if (rc != 200)
                {
                    Log.log.error("Instead of success, HTTP response code of " + rc + " is returned while communicating with MCP gateway connector.");
                }
            }
            catch(Exception e)
            {
                Log.log.error(e.getMessage(), e);
            }
            finally
            {
                try
                {
                    if (writer != null)
                    {
                        writer.close();
                    }
                    if (outputStream != null)
                    {
                        outputStream.close();
                    }
                }
                catch(Exception e)
                {
                    Log.log.error(e.getMessage(), e);
                }
            }
        }
    }
    
    @SuppressWarnings({ "rawtypes", "unchecked" })
    private static String getPostDataString(Map params) throws UnsupportedEncodingException
    {
        StringBuilder result = new StringBuilder();
        boolean first = true;
        for(Map.Entry entry : (Set<Map.Entry>)params.entrySet())
        {
            if (first)
                first = false;
            else
                result.append("&");

            result.append(URLEncoder.encode((String)entry.getKey(), "UTF-8"));
            result.append("=");
            String value = entry.getValue() == null ? "" : (String)entry.getValue();
            result.append(URLEncoder.encode(value, "UTF-8"));
        }

        return result.toString();
    }
    
    private static boolean validate()
    {
        boolean result = true;
        
        if (StringUtils.isBlank(hostName))
        {
            Log.log.warn("hostname is blank. Could not alert MCP.");
            result =  false;
        }
        
        if (StringUtils.isBlank(protocol))
        {
            protocol = "http";
        }
        else
        {
            protocol = protocol.trim();
            if (!(protocol.equals("http") || protocol.equals("https")))
            {
                Log.log.warn("Only http or https protocol is supported.");
                result =  false;
            }
        }
        
        if (port != null && !port.matches("\\d+"))
        {
            Log.log.warn("Port number should be a valid integer.");
            result =  false;
        }
        
        return result;
    }
}
