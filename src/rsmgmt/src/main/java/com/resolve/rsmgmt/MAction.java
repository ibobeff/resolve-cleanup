/******************************************************************************
* (C) Copyright 2014
*
* Resolve Systems, LLC
*
* All rights reserved
* This software is distributed under license and may not be used, copied,
* modified, or distributed without the express written permission of
* Resolve Systems, LLC.
*
******************************************************************************/

package com.resolve.rsmgmt;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.resolve.rsbase.MainBase;
import com.resolve.util.Constants;
import com.resolve.util.ERR;
import com.resolve.util.Log;
import com.resolve.util.StringUtils;

public class MAction
{
    static Main main = (Main) MainBase.main;
    static int esbTimeout = 600000; //how many miliseconds without a heartbeat message before throwing an error
    static int pingThreshold;
    
    public static void importReports(Map params)
    {
        String guid = params.get(Constants.TARGET_GUID) != null ? params.get(Constants.TARGET_GUID).toString() : null;
        Log.log.info("Send Import Report Message to RSView GUID: " + guid);
        if (StringUtils.isNotEmpty(guid))
        {
            String reports = main.blueprintProperties.get("resolve.import");
            if (StringUtils.isNotEmpty(reports))
            {
                Map sendParams = new HashMap();
                sendParams.put(Constants.IMPEX_PARAM_MODULENAMES, reports);
                sendParams.put(Constants.HTTP_REQUEST_USERNAME, "system");
                Log.log.debug("Send import reports: " + reports);
                main.esb.sendMessage(guid, "MAction.importReports", sendParams);
            }
        }
    }
    
//    private static long lastRsControlPingSent;
//    private static long lastRsRemotePingSent;
//    private static long lastRsViewPingSent;
//    
//    private static long lastRsControlPingRespReceived;
//    private static long lastRsRemotePingRespReceived;
//    private static long lastRsViewPingRespReceived;
    
    @SuppressWarnings("unchecked")
	public void esbPingRsControl()
    {
    	List<String> guids = new ArrayList<String>();
    	MStatus.getDeployedComponentsGUID("RSCONTROL", guids);
    	Map<String, String> response = new HashMap<String, String>();
    	for (String guid : guids)
    	{
    		try
			{
    			Log.log.trace("Sending ESB ping to RSCONTROL-" + guid);
    			response = Main.esb.call(guid, "MAction.esbPing", new HashMap<String, String>(), pingThreshold*1000);
    			if (response == null)
    			{
    				esbPingAlert("RSCONTROL-" + guid);
    			}
    			else
    			{
    				Log.log.trace("Received ESB ping from RSCONTROL-" + guid);
    			}
			}
    		catch (Exception e)
			{
    			Log.log.error(e.getMessage(), e);
			}
    	}
        
//        Map<String, String> params = new HashMap<String, String>();
//        params.put("FROM", "RSMGMT");
//        Main.esb.sendInternalMessage(Constants.ESB_NAME_RSCONTROLS, "MAction.esbPing", params);
//        lastRsControlPingSent = System.currentTimeMillis();
    }
    
    @SuppressWarnings("unchecked")
	public void esbPingRsRemote()
    {
    	List<String> guids = new ArrayList<String>();
    	MStatus.getDeployedComponentsGUID("RSREMOTE", guids);
    	
    	Map<String, String> response = new HashMap<String, String>();
    	for (String guid : guids)
    	{
    		try
			{
    			Log.log.trace("Sending ESB ping to RSREMOTE-" + guid);
    			response = Main.esb.call(guid, "MAction.esbPing", new HashMap<String, String>(), pingThreshold*1000);
    			if (response == null)
    			{
    				esbPingAlert("RSREMOTE-" + guid);
    			}
    			else
    			{
    				Log.log.trace("Received ESB ping from RSREMOTE-" + guid);
    			}
			}
    		catch (Exception e)
			{
    			Log.log.error(e.getMessage(), e);
			}
    	}
    	
//        Log.log.trace("Sending ESB ping to RSREMOTE");
//        Map<String, String> params = new HashMap<String, String>();
//        params.put("FROM", "RSMGMT");
//        Main.esb.sendInternalMessage(Constants.ESB_NAME_RSREMOTES, "MAction.esbPing", params);
//        lastRsRemotePingSent = System.currentTimeMillis();
    }
    
    @SuppressWarnings("unchecked")
	public void esbPingRsView()
    {
    	List<String> guids = new ArrayList<String>();
    	MStatus.getDeployedComponentsGUID("RSVIEW", guids);
    	
    	Map<String, String> response = new HashMap<String, String>();
    	for (String guid : guids)
    	{
    		try
			{
    			Log.log.trace("Sending ESB ping to RSVIEW-" + guid);
    			response = Main.esb.call(guid, "MAction.esbPing", new HashMap<String, String>(), pingThreshold*1000);
    			if (response == null)
    			{
    				esbPingAlert("RSVIEW-" + guid);
    			}
    			else
    			{
    				Log.log.trace("Received ESB ping from RSVIEW-" + guid);
    			}
			}
    		catch (Exception e)
			{
    			Log.log.error(e.getMessage(), e);
			}
    	}
    	
//        Log.log.trace("Sending ESB ping to RSVIEW");
//        Map<String, String> params = new HashMap<String, String>();
//        params.put("FROM", "RSMGMT");
//        Main.esb.sendInternalMessage(Constants.ESB_NAME_RSVIEWS, "MAction.esbPing", params);
//        lastRsViewPingSent = System.currentTimeMillis();
    }
    
//    public void esbPingResponse(Map<String, String> params)
//    {
//        String compName = params.get("RESPONSE_FROM");
//        if (compName.equalsIgnoreCase("rscontrol"))
//        {
//            lastRsControlPingRespReceived = System.currentTimeMillis();
//        }
//        else if (compName.equalsIgnoreCase("rsremote"))
//        {
//            lastRsRemotePingRespReceived = System.currentTimeMillis();
//        }
//        else if (compName.equalsIgnoreCase("rsview"))
//        {
//            lastRsViewPingRespReceived = System.currentTimeMillis();
//        }
//        Log.log.trace("Received ESB ping to RSCONTROL: " + params);
//    }
    
//    public void esbPingStatus()
//    {
//        long pingTimeDiff = lastRsControlPingRespReceived - lastRsControlPingSent;
//        // System.out.println(lastRsControlPingRespReceived + "," + lastRsControlPingSent + "," + pingTimeDiff);
//        if (pingTimeDiff < 0 || pingTimeDiff > MStatus.esbTimeout)
//        {
//            esbPingAlert(pingTimeDiff, "RsControl");
//        }
//        
//        pingTimeDiff = lastRsRemotePingRespReceived - lastRsRemotePingSent;
//        if (pingTimeDiff < 0 || pingTimeDiff > MStatus.esbTimeout)
//        {
//            esbPingAlert(pingTimeDiff, "RsRemote");
//        }
//        
//        pingTimeDiff = lastRsViewPingRespReceived - lastRsViewPingSent;
//        if (pingTimeDiff < 0 || pingTimeDiff > MStatus.esbTimeout)
//        {
//            esbPingAlert(pingTimeDiff, "RsView");
//        }
//    }
    
    @SuppressWarnings({ "rawtypes", "unchecked" })
    private void esbPingAlert(String compName)
    {
        ERR errorCode = ERR.E20010;
        String message = errorCode.getMessage();
//        long idleTimeSinceLastPing = 0;
//        if (compName.equalsIgnoreCase("rscontrol"))
//        {
//            idleTimeSinceLastPing = (System.currentTimeMillis() - lastRsControlPingSent) / 1000 ;
//        }
//        else if (compName.equalsIgnoreCase("rsremote"))
//        {
//            idleTimeSinceLastPing = (System.currentTimeMillis() - lastRsRemotePingSent) / 1000 ;
//        }
//        else if (compName.equalsIgnoreCase("rsview"))
//        {
//            idleTimeSinceLastPing = (System.currentTimeMillis() - lastRsViewPingSent) / 1000;
//        }
        message += " - ESB Ping has not been received from " + compName + " since the last ping for " + pingThreshold + " seconds. MQ could be too busy or component may not be running.";

        // log fatal message
        Throwable t = new Exception(message);
        Log.log.fatal(message, t);
        System.out.println(message);
        t.printStackTrace();
        
        if (main.configAlert.isActive())
        {
            Map params = new HashMap();
            params.put("SEVERITY", "CRITICAL");
            params.put("COMPONENT", main.release.serviceName);
            params.put("CODE", errorCode);
            params.put("MESSAGE", message);
            (new MAlert()).alert(params);
        }
    }
}
