/******************************************************************************
 * (C) Copyright 2014
 *
 * Resolve Systems, LLC
 *
 * All rights reserved This software is distributed under license and may not be
 * used, copied, modified, or distributed without the express written permission
 * of Resolve Systems, LLC.
 *
 ******************************************************************************/

package com.resolve.rsmgmt;

import java.util.HashMap;
import java.util.Map;

import com.resolve.util.CryptUtils;
import com.resolve.util.Log;
import com.resolve.util.Properties;

public class ConfigRSSearch
{
    private static final String instanceName = "rssearch";

    private String user = "resolve";

    private Properties ymlProperties;
    private Properties runProperties;
    private Properties envProperties;
    private String serverlist = "127.0.0.1";
    private Integer unicastPort = null;

    public ConfigRSSearch()
    {
        this.ymlProperties = new Properties();
        this.runProperties = new Properties();
        this.envProperties = new Properties();
    }

    public ConfigRSSearch(Properties properties)
    {
        this();
        for (Object key : properties.keySet())
        {
            String strKey = key.toString();

            if (strKey.startsWith(instanceName + "."))
            {
                String value = properties.getProperty(strKey).trim();
                if (value.contains("${"))
                {
                    value = properties.getReplacement(key).trim();
                }
                if (value.startsWith("TO_ENC:") || value.startsWith("NO_ENC:"))
                {
                    value = value.substring(7);
                }
                if (value.startsWith("ENC"))
                {
                    try
                    {
                        value = CryptUtils.decrypt(value);
                    }
                    catch (Exception e)
                    {
                        Log.log.error("Failed to decrypt blueprint value", e);
                        value = "";
                    }
                }
                if (value != null)
                {
                    if (strKey.startsWith(instanceName + ".yml"))
                    {
                        this.ymlProperties.put(key, value);
                    }

                    else if (strKey.startsWith(instanceName + ".run"))
                    {
                        this.runProperties.put(key, value);
                        if (strKey.equals(instanceName + ".run.R_USER"))
                        {
                            this.user = value;
                        }
                    }
                    else if (strKey.startsWith(instanceName + ".env"))
                    {
                        this.envProperties.put(key, value);
                    }
                    else if (strKey.equals(instanceName + ".serverlist"))
                    {
                        this.serverlist = value;
                    }
                    else if (strKey.equals(instanceName + ".unicast.hosts_port"))
                    {
                        if (value.matches("\\d+"))
                        {
                            this.unicastPort = Integer.parseInt(value);
                        }
                    }
                }
            }
        }
    } // ConfigRSSearch

    public Map<String, String> rssearchYmlReplaceValues()
    {
        Map<String, String> result = new HashMap<String, String>();

        for (Object key : ymlProperties.keySet())
        {
            String strKey = key.toString();
            String value = ymlProperties.get(strKey);

            strKey = strKey.replaceFirst(instanceName + ".yml.", "");

            String replaceRegex = "(?m)^ *" + strKey + ":.*";
            String replaceString = strKey + ": " + value;

            if (strKey.equals("discovery.zen.ping.unicast.hosts"))
            {
                StringBuilder hostList = new StringBuilder();
                hostList.append("[");
                boolean first = true;
                for (String host : value.split(","))
                {
                    if (!first)
                    {
                        hostList.append(", ");
                    }
                    hostList.append("\"" + host);
                    if (unicastPort != null)
                    {
                        hostList.append(":" + unicastPort.toString());
                    }
                    hostList.append("\"");
                    first = false;
                }
                hostList.append("]");

                replaceString = strKey + ": " + hostList.toString();
            }
            else if (strKey.equals("discovery.zen.minimum_master_nodes"))
            {
                if (value.trim().equals("-1"))
                {
                    int serverCount = serverlist.split(",").length;
                    int masterNodeCount = (int) Math.floor(serverCount / 2);
                    if (serverCount > 2 || masterNodeCount == 0)
                    {
                        masterNodeCount++;
                    }
                    value = "" + masterNodeCount;
                    replaceString = strKey + ": " + value;
                }
            }
            else if (strKey.startsWith("shield"))
            {

                // If no value, add a # to comment out the YML property
                if (value.isEmpty() || value == null)
                {
                    replaceString = "#" + replaceString;
                }
                else
                {
                    // else remove the #
                    replaceRegex = "(?m)^#*" + strKey + ":.*";
                    
                }
            }
            else if (strKey.equals("path.repo"))
            {
            	// If no value, add a # to comment out the YML property
                if (value.isEmpty() || value == null)
                {
                    replaceString = "#" + replaceString;
                }
                else
            	{
            		replaceRegex = "(?m)^#*" + strKey + ":.*";
            	}
            }

            result.put(replaceRegex, replaceString);
        }

        return result;
    } // rssearchYmlReplaceValues

    public Map<String, String> rssearchEnvReplaceValues()
    {
        Map<String, String> result = new HashMap<String, String>();
        
        for (Object key : envProperties.keySet())
        {
            String strKey = key.toString();
            String value = envProperties.getProperty(strKey);
            
            strKey = strKey.replaceFirst(instanceName + ".env.", "");
            value = value.replaceAll("\\\\", "/");
            if (value.equalsIgnoreCase("true") || value.equalsIgnoreCase("false"))
            {
                value = value.toUpperCase();
            }
            
            strKey = strKey.toUpperCase();
            String replaceRegex = strKey + "=.*";
            String replaceString = strKey + "=" + value;

            result.put(replaceRegex, replaceString);
        }
        return result;
    }
	public Map<String, String> rssearchRunReplaceValues() {
		return rssearchRunReplaceValues(true);
	 }
    
    public Map<String, String> rssearchRunReplaceValues(boolean toUpperCase)
    {
        Map<String, String> result = new HashMap<String, String>();

        for (Object key : runProperties.keySet())
        {
            String strKey = key.toString();
            String value = runProperties.getProperty(strKey);

            strKey = strKey.replaceFirst(instanceName + ".run.", "");

            if (strKey.equalsIgnoreCase("Xms") || strKey.equalsIgnoreCase("mms"))
            {
                if (value.endsWith("M"))
                {
                    value = value.substring(0, value.length() - 1);
                }
                //result.put("ES_MIN_MEM=.*", "ES_MIN_MEM=" + value + "M");
                result.put("Xms\\d+M", "Xms" + value + "M");
            }
            else if (strKey.equalsIgnoreCase("Xmx") || strKey.equalsIgnoreCase("mmx"))
            {
                if (value.endsWith("M"))
                {
                    value = value.substring(0, value.length() - 1);
                }
                //result.put("ES_MAX_MEM=.*", "ES_MAX_MEM=" + value + "M");
                result.put("Xmx\\d+M", "Xmx" + value + "M");
            }
            else
            {
                value = value.replaceAll("\\\\", "/");
                if (value.equalsIgnoreCase("true") || value.equalsIgnoreCase("false"))
                {
					if(toUpperCase) {
						value = value.toUpperCase();
					}
                }
				if(toUpperCase) {
					strKey = strKey.toUpperCase();
				}
                String replaceRegex = strKey + "=.*";
                String replaceString = strKey + "=" + value;

                result.put(replaceRegex, replaceString);
            }
        }
        return result;
    } // rssearchRunReplaceValues();

    public Properties getYmlProperties()
    {
        return ymlProperties;
    } // getYmlProperties

    public void setYmlProperties(Properties ymlProperties)
    {
        this.ymlProperties = ymlProperties;
    }// setYmlProperties

    public String getServerlist()
    {
        return serverlist;
    } // getServerlist

    public void setServerlist(String serverlist)
    {
        this.serverlist = serverlist;
    } // setServerlist

    public void setUser(String user)
    {
        this.user = user;
    }

    public String getUser()
    {
        return this.user;
    }
}
