/******************************************************************************
* (C) Copyright 2014
*
* Resolve Systems, LLC
*
* All rights reserved
* This software is distributed under license and may not be used, copied,
* modified, or distributed without the express written permission of
* Resolve Systems, LLC.
*
******************************************************************************/

package com.resolve.rsmgmt;

import com.resolve.esb.MListener;
import com.resolve.rsbase.MainBase;
import com.resolve.util.Constants;
import com.resolve.util.Log;

public class Alert
{
    static MListener mListener = null;
    static ConfigAlert config = null;

    public Alert(ConfigAlert config)
    {
        this.config = config;

        // init alert (snmp trap)
        try
        {
            if (config.isSnmpTrapActive())
            {
	            AlertSNMP.init(config.getSnmpTrapHost(), config.getSnmpTrapPort(), config.getSnmpTrapCommunity());
            }
        }
        catch (Exception e)
        {
            Log.log.error("Failed to initialize SNMP Trap alerting: "+e.getMessage());
        }
    } // Alert

    public void start()
    {
        try
        {
            Log.log.info("Initializing Alert");

            // add listener (ALERT)
            Log.log.info("  Initializing TrendMetric ESB Listener");
            //mListener = MListenerFactory.getMListener(MainBase.main.mServer, Constants.ESB_NAME_ALERT, "com.resolve.rsmgmt");
            MListener mListener = MainBase.main.mServer.createListener(Constants.ESB_NAME_ALERT, "com.resolve.rsmgmt");

            mListener.init(false);

            // init handlers
            MainBase.main.mServer.addHandler("MAlert", MAlert.class);
        }
        catch (Exception e)
        {
            Log.log.warn(e.getMessage(), e);
        }
    } // start

    public void stop()
    {
    } // stop

} // Alert
