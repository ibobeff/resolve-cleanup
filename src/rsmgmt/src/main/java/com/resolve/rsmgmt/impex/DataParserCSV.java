/******************************************************************************
* (C) Copyright 2014
*
* Resolve Systems, LLC
*
* All rights reserved
* This software is distributed under license and may not be used, copied,
* modified, or distributed without the express written permission of
* Resolve Systems, LLC.
*
******************************************************************************/

package com.resolve.rsmgmt.impex;

import java.io.StringReader;
import java.util.ArrayList;
import java.util.List;

import com.resolve.util.StringUtils;

import au.com.bytecode.opencsv.CSVReader;

public class DataParserCSV implements DataParser
{
    @Override
    public List<String[]> parseData(String data, char delimeter) throws Exception
    {
        List<String[]> result = new ArrayList<String[]>();
        
        CSVReader reader = new CSVReader(new StringReader(data), delimeter);
        String[] line = reader.readNext();
        while(line != null)
        {
            //this is important because sometime the file may have blank line.
            //and the reader reads it as a blank row.
            if(line.length > 0)
            {
                //now make sure that at least one column have value
                for(String value : line)
                {
                    if(!StringUtils.isBlank(value))
                    {
                        result.add(line);
                        break;
                    }
                }
            }
            line = reader.readNext();
        }
        
        return result;
    }
}
