/******************************************************************************
* (C) Copyright 2014
*
* Resolve Systems, LLC
*
* All rights reserved
* This software is distributed under license and may not be used, copied,
* modified, or distributed without the express written permission of
* Resolve Systems, LLC.
*
******************************************************************************/

package com.resolve.rsmgmt;

import com.resolve.execute.ExecuteRemote;

public class MSession
{
    public void processCompleted(String processid)
    {
        // remove sessions
        ExecuteRemote.removeSessionMap(processid);
    } // processCompleted
    
} // MSession
