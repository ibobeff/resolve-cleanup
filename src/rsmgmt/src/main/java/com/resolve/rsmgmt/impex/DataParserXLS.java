/******************************************************************************
* (C) Copyright 2014
*
* Resolve Systems, LLC
*
* All rights reserved
* This software is distributed under license and may not be used, copied,
* modified, or distributed without the express written permission of
* Resolve Systems, LLC.
*
******************************************************************************/

package com.resolve.rsmgmt.impex;

import java.util.List;

public class DataParserXLS implements DataParser
{
    @Override
    public List<String[]> parseData(String data, char delimeter) throws Exception
    {
        throw new Exception("You are not supposed to use actual XLS parser, but convert itoto CSV and then use CSV parser");
    }
}
