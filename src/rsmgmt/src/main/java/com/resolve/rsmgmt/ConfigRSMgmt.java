/******************************************************************************
* (C) Copyright 2014
*
* Resolve Systems, LLC
*
* All rights reserved
* This software is distributed under license and may not be used, copied,
* modified, or distributed without the express written permission of
* Resolve Systems, LLC.
*
******************************************************************************/

package com.resolve.rsmgmt;

import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import com.resolve.util.CryptUtils;
import com.resolve.util.Log;
import com.resolve.util.Properties;
import com.resolve.util.StringUtils;

public class ConfigRSMgmt
{
    private String instanceName = "rsmgmt";

    private String user = "resolve";
    
    private Properties rsmgmtProperties;
    private Properties runProperties;
    private Properties envProperties;
    private Properties logProperties;
    private Properties esapiProperties;

    public ConfigRSMgmt()
    {
        this.rsmgmtProperties = new Properties();
	    this.runProperties = new Properties();
	    this.envProperties = new Properties();
	    this.logProperties = new Properties();
	    this.esapiProperties = new Properties();
    } //ConfigRSMgmt
    
    public ConfigRSMgmt(Properties properties)
    {
        this();
        for (Object key : properties.keySet())
        {
            String strKey = key.toString();
            if (strKey.startsWith(instanceName + "."))
            {
                String value = properties.getProperty(strKey).trim();
                if (value.contains("${"))
                {
                    value = properties.getReplacement(key).trim();
                }
                if (value.toUpperCase().startsWith("TO_ENC:"))
                {
                    if (StringUtils.isNotBlank(value.substring(7)) && !value.substring(7).toUpperCase().startsWith("NO_ENC:") && !value.substring(7).matches(".*(?<!\\\\)\\$\\{.*\\}.*"))
                    {
                        try
                        {
                            value = CryptUtils.encrypt(value.substring(7));
                        }
                        catch (Exception e)
                        {
                            Log.log.error("Failed to encrypt blueprint value prefixed with TO_ENC: set for the key " + strKey + 
                                          " due to error " + e.getMessage() + ", setting un-encrypted value into config.xml", e);
                        }
                    }
                }
                if (value.toUpperCase().startsWith("NO_ENC:"))
                {
                    value = value.substring(7);
                }
                if (value.startsWith("ENC"))
                {
                    try
                    {
                        value = CryptUtils.decrypt(value);
                    }
                    catch (Exception e)
                    {
                        Log.log.error("Failed to decrypt blueprint value", e);
                        value = "";
                    }
                }
                if (strKey.contains("brokeraddr") && value.matches(":\\d*"))
                {
                    value = "";
                }
                if (value != null)
                {
	                if (strKey.startsWith(instanceName + ".run"))
	                {
	                    this.runProperties.put(key, value);
                        if (strKey.equals(instanceName + ".run.R_USER"))
                        {
                            this.user = value;
                        }
	                }
	                else if (strKey.startsWith(instanceName + ".log4j"))
	                {
	                    this.logProperties.put(key, value);
	                }
	                else if (strKey.startsWith(instanceName + ".env"))
                    {
                        this.envProperties.put(key, value);
                    }
	                else if (strKey.startsWith(instanceName + ".ESAPI"))
	                {
	                    this.esapiProperties.put(key, value);
	                }
	                else
	                {
	                    if (strKey.contains("pass") && !strKey.contains("ldap.password_attribute"))
                        {
                            if (StringUtils.isNotBlank(value) && !value.toUpperCase().startsWith("NO_ENC:") && !value.matches(".*(?<!\\\\)\\$\\{.*\\}.*"))
                            {
                                try
                                {
                                    value = CryptUtils.encrypt(value);
                                }
                                catch (Exception e)
                                {
                                    Log.log.error("Failed to encrypt blueprint value for the key " + strKey + 
                                                  " due to error " + e.getMessage() + ", setting un-encrypted value into config.xml", e);
                                }
                            }
                        }
		                this.rsmgmtProperties.put(key, value);
	                }
                }
            }
            
        }
        if (!rsmgmtProperties.containsKey(instanceName + ".id.guid"))
        {
            rsmgmtProperties.put(instanceName + ".id.guid", Main.main.configId.getGuid());
        }
    } // ConfigRSMgmt
    
    @SuppressWarnings("unchecked")
    public Map<String, Object> rsmgmtXPathValues()
    {
        Map<String, Object> result = new HashMap<String, Object>();
        
        for (Object key : rsmgmtProperties.keySet())
        {
            String strKey = key.toString();
            String value = (String) rsmgmtProperties.get(key);
            
            strKey = strKey.replaceFirst(instanceName, "").toUpperCase();
            strKey = strKey.replaceAll("(?<!\\\\)\\.", "/");
            strKey = strKey.replaceAll("\\\\\\.", ".");
            
            Pattern pattern = Pattern.compile("(.+)/(.+)/(\\d+)");
            Matcher matcher = pattern.matcher(strKey);
            if (matcher.matches())
            {
                strKey = "." + matcher.group(1);
                String xdocKey = matcher.group(2);
                int listPosition = Integer.parseInt(matcher.group(3))-1;

                List<Map<String, String>> list = (List<Map<String, String>>) result.get(strKey);
                if (list == null)
                {
                    list = new LinkedList<Map<String, String>>();
                    result.put(strKey, list);
                }
                for (int j=list.size(); j<=listPosition; j++)
                {
                    list.add(new HashMap<String, String>());
                }
                Map<String, String> entry = list.get(listPosition);
                entry.put(xdocKey, value);
                list.set(listPosition, entry);
            }
            else
            {
                strKey = strKey.replaceFirst("^(.*)/(.*?)$", ".$1/@$2");
                result.put(strKey, value);
            }
        }
        
        return result;
    } //rsmgmtXPathValues()
    
    public Map<String, String> rsmgmtEnvReplaceValues()
    {
        Map<String, String> result = new HashMap<String, String>();
        
        for (Object key : envProperties.keySet())
        {
            String strKey = key.toString();
            String value = envProperties.getProperty(strKey);
            
            strKey = strKey.replaceFirst(instanceName + ".env.", "");
            value = value.replaceAll("\\\\", "/");
            if (value.equalsIgnoreCase("true") || value.equalsIgnoreCase("false"))
            {
                value = value.toUpperCase();
            }
            
            strKey = strKey.toUpperCase();
            String replaceRegex = strKey + "=.*";
            String replaceString = strKey + "=" + value;

            result.put(replaceRegex, replaceString);
        }
        return result;
    }
    
    public Map<String, String> rsmgmtRunReplaceValues()
    {
        Map<String, String> result = new HashMap<String, String>();
        
        for (Object key : runProperties.keySet())
        {
            String strKey = key.toString();
            String value = runProperties.getProperty(strKey);
            
            strKey = strKey.replaceFirst(instanceName + ".run.", "");
            
            if (strKey.equalsIgnoreCase("Xms") || strKey.equalsIgnoreCase("mms"))
            {
                if (value.endsWith("M"))
                {
                    value = value.substring(0, value.length()-1);
                }
                result.put("Xms\\d+M", "Xms" + value + "M");
                result.put("MMS=.*", "MMS=" + value);
            }
            else if (strKey.equalsIgnoreCase("Xmx") || strKey.equalsIgnoreCase("mmx"))
            {
                if (value.endsWith("M"))
                {
                    value = value.substring(0, value.length()-1);
                }
                result.put("Xmx\\d+M", "Xmx" + value + "M");
                result.put("MMX=.*", "MMX=" + value);
            }
            else
            {
                value = value.replaceAll("\\\\", "/");
                if (value.equalsIgnoreCase("true") || value.equalsIgnoreCase("false"))
                {
                    value = value.toUpperCase();
                }
                
                strKey = strKey.toUpperCase();
                String replaceRegex = strKey + "=.*";
                String replaceString = strKey + "=" + value;
    
                result.put(replaceRegex, replaceString);
            }
        }
        return result;
    } //rsmgmtRunReplaceValues()
    
    public Map<String, String> rsmgmtLogReplaceValues()
    {
        Map<String, String> result = new HashMap<String, String>();
        
        for (Object key : logProperties.keySet())
        {
            String strKey = key.toString();
            String value = logProperties.getProperty(strKey);
            
            strKey = strKey.replaceFirst(instanceName + ".", "");
            value = value.replaceAll("\\\\", "/");
	            
            String replaceRegex = strKey + "=.*";
            String replaceString = strKey + "=" + value;

            result.put(replaceRegex, replaceString);
        }
        
        return result;
    } //rsmgmtLogReplaceValues
    
    public Map<String, String> rsmgmtEsapiReplaceValues()
    {
        Map<String, String> result = new HashMap<String, String>();

        // System.out.println("ESAPI replacing values...");
        // System.out.println("ESAPI properties: " + esapiProperties.keySet());

        for (Object key : esapiProperties.keySet())
        {
            String strKey = key.toString();
            String value = esapiProperties.getProperty(strKey);

            // System.out.println("strKeyBefore: " + strKey);

            strKey = strKey.replaceFirst(instanceName + ".ESAPI.", "");

            
            if(strKey.startsWith("Validator")) {
                //System.out.println("*****=========VALIDATOR FOUND!=========*****");
                //System.out.println("String Key: " + strKey);
                value = value.replace("\\", "\\\\");
                //System.out.println("String Value: " + value);
            } else {
                //System.out.println("String Key: " + strKey);
                value = value.replace("\\\\", "/");
                //System.out.println("String Value: " + value);
            }

            // System.out.println("strKeyAfter: " + strKey);

            String replaceRegex = strKey + "=.*";
            String replaceString = strKey + "=" + value;

            //replaceString = replaceString.replaceAll("\\", "\\\\");
            result.put(replaceRegex, replaceString);
        }

        return result;
    } // rsviewEsapiReplaceValues

    public Properties getRsmgmtProperties()
    {
        return rsmgmtProperties;
    } //getRSmgmtProperties

    public void setRsmgmtProperties(Properties rsmgmtProperties)
    {
        this.rsmgmtProperties = rsmgmtProperties;
    } //setRSmgmtProperties

    public Properties getRunProperties()
    {
        return runProperties;
    } //getRunProperties

    public void setRunProperties(Properties runProperties)
    {
        this.runProperties = runProperties;
    } //setRunProperties
    
    public void setEsapiProperties(Properties esapiProperties)
    {
        this.esapiProperties = esapiProperties;
    }

    public Properties getEsapiProperties()
    {
        return this.esapiProperties;
    }

    public void setUser(String user)
    {
        this.user = user;
    }

    public String getUser()
    {
        return this.user;
    }
}
