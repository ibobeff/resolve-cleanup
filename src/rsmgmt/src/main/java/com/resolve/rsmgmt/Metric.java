/******************************************************************************
* (C) Copyright 2014
*
* Resolve Systems, LLC
*
* All rights reserved
* This software is distributed under license and may not be used, copied,
* modified, or distributed without the express written permission of
* Resolve Systems, LLC.
*
******************************************************************************/

package com.resolve.rsmgmt;

import java.lang.management.ManagementFactory;
import java.lang.management.ThreadMXBean;
import java.util.HashMap;
import java.util.Map;

import com.resolve.rsbase.BaseMetric;
import com.resolve.rsmgmt.metric.MetricStoreMySQL;
import com.resolve.rsmgmt.metric.MetricStoreOracle;
import com.resolve.rsmgmt.metric.TrendMetric;
import com.resolve.util.MetricMsg;
import com.resolve.util.MetricThreshold;

public class Metric extends BaseMetric
{
    //read  and load the threshold values at component startup
    private final boolean active;
    
    public Metric (boolean active, boolean hasDBActive)
    {
        super(hasDBActive ? new MetricThresholdInitDBImpl() : new MetricThresholdInitNoDBImpl());
        this.active = active;
    }
    
    public static MetricThreshold getMthreshold()
    {
        return mthreshold;
    }

    public static void setMthreshold(MetricThreshold mthreshold)
    {
        Metric.mthreshold = mthreshold;
    }

    public void sendMetrics()
    {
        if(mthreshold.thresholdProperties.keySet().isEmpty())
        {
            mthreshold.setThresholdProperties();
        }
        
        // send MetricJVM
        sendMetricJVM();
        
        // send MetricDatabase 
        sendMetricDatabase();
        
        // Number of Messages in ESB
        sendMetricJMSMessagesCount();
        
    } // sendMetrics
    
    void sendMetricJVM()
    { 
    	MetricMsg result = new MetricMsg("JVM", Main.main.release.getHostServiceName()+"/"+Main.main.configId.getGuid(), "");
    	
        // MemoryPercentFree
        long free = (Runtime.getRuntime().maxMemory() - Runtime.getRuntime().totalMemory()) + Runtime.getRuntime().freeMemory();
        int value = (int) (free * 100 / Runtime.getRuntime().maxMemory());
    	result.addMetric("mem_free", value);
    	mthreshold.checkThresholds("jvm", value, "mem_free");
    	
//        String metricname = "JVM".toLowerCase() + ".mem_free";
//        // threshold memory free value 
//        if (value < mthreshold.thresholdProperties.get(metricname).getLow())
//        {
//            Log.alert(ERR.E70001, "Low available memory: "+value);
//        }
    	
    	// PeakThread
        ThreadMXBean thread = ManagementFactory.getThreadMXBean();
    	result.addMetric("thread_count", thread.getThreadCount());
//    	int thread_count_percentage = (int) (thread.getThreadCount() * 100) / (Main.main.configGeneral.getMaxThread() * 2); //approx max for the jvm thread = double the thread poll max
//        mthreshold.checkThresholds("jvm", thread_count_percentage, "thread_count");
//    	mthreshold.checkThresholds("jvm", thread.getThreadCount(), "thread_count");
    	
    	Main.sendMetric(result);
    } // sendMetricJVM
    
    private void sendMetricDatabase()
    {
        // only send if it is the metric rsmgmt
        if(active)
        {
            MetricMsg result = new MetricMsg("Database", Main.main.release.getHostServiceName()+"/"+Main.main.configId.getGuid(), "");
            Map<String, Long> unitNameValues = new HashMap<String, Long>();
        
            if(TrendMetric.getDbType().toUpperCase().startsWith("ORACLE"))
            {
                MetricStoreOracle store = new MetricStoreOracle();
                unitNameValues = store.getDBMetrics();
            }
            else if(TrendMetric.getDbType().equalsIgnoreCase("MYSQL"))
            {
                MetricStoreMySQL store = new MetricStoreMySQL();
                unitNameValues = store.getDBMetrics();
            }

            result.addMetric("free_space", unitNameValues.get("free_space"));
            mthreshold.checkThresholds("database", unitNameValues.get("free_space").intValue(), "free_space");
            result.addMetric("size", unitNameValues.get("size"));
            mthreshold.checkThresholds("database", unitNameValues.get("size").intValue(), "size");
            result.addMetric("percentage_used", unitNameValues.get("percentage_used"));
            mthreshold.checkThresholds("database", unitNameValues.get("percentage_used").intValue(), "percentage_used");
            Long query_count = unitNameValues.get("query_count");
            Long response_time = unitNameValues.get("response_time");
            int avgResponse = new Double(response_time / query_count).intValue();
            result.addMetric("query_count", query_count );
            mthreshold.checkThresholds("database", query_count.intValue(), "query_count");
            result.addMetric("response_time", response_time, query_count.intValue());
            mthreshold.checkThresholds("database", avgResponse, "response_time");
            result.addMetric("percentage_wait", unitNameValues.get("percentage_wait"));
            mthreshold.checkThresholds("database", unitNameValues.get("percentage_wait").intValue(), "percentage_wait");

            Main.sendMetric(result);
        }
        
    } // sendMetricDatabase
    
    private void sendMetricJMSMessagesCount()
    {
        MetricMsg result = new MetricMsg("JMS_QUEUE", Main.main.release.getHostServiceName()+"/"+Main.main.configId.getGuid(), "");
        
        // JMS Msgs Count
        int jmsMsgsCount = 101;
        result.addMetric("msgs_count", jmsMsgsCount);
        mthreshold.checkThresholds("jms_queue", jmsMsgsCount, "msgs_count");        
        
        Main.sendMetric(result);
        
        
    }
    
} // Metric
