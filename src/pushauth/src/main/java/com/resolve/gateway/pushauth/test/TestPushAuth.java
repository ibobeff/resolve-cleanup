package com.resolve.gateway.pushauth.test;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.net.HttpURLConnection;
import java.net.URL;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.beanutils.BeanUtils;
import org.apache.http.HttpResponse;
import org.apache.log4j.Logger;
import org.bouncycastle.util.encoders.Base64Encoder;
import org.dom4j.DocumentException;
import org.eclipse.jetty.security.ConstraintSecurityHandler;
import org.eclipse.jetty.server.HttpConfiguration;
import org.eclipse.jetty.server.HttpConnectionFactory;
import org.eclipse.jetty.server.Server;
import org.eclipse.jetty.server.ServerConnector;
import org.eclipse.jetty.servlet.ServletContextHandler;
import org.eclipse.jetty.servlet.ServletHolder;
import org.eclipse.jetty.util.B64Code;
import org.eclipse.jetty.util.StringUtil;
import org.junit.Assert;
import org.junit.Test;

import com.resolve.gateway.pushauth.InvalidAuthConfigurationException;
import com.resolve.gateway.pushauth.PushAuthHandlerFactory;
import com.resolve.gateway.pushauth.UnknownAuthenticationTypeException;
import com.resolve.rsremote.ConfigReceiveGateway;
import com.resolve.rsremote.ConfigReceiveHTTP;
import com.resolve.util.Base64;
import com.resolve.util.ConfigMap;
import com.resolve.util.Log;
import com.resolve.util.XDoc;
 
import static org.junit.Assert.*;

public class TestPushAuth
{
        
    public static class TestConfigNoRootPWConfig extends ConfigReceiveGateway
    {
        
        public TestConfigNoRootPWConfig(XDoc config) throws Exception
        {
            super(config);
            // TODO Auto-generated constructor stub
        }
        public String getHttpAuthType()
        {
            return "BASIC";
        }
        public void setHttpAuthType(String s)
        { 
        }
        @Override
        public String getRootNode()
        {
            // TODO Auto-generated method stub
            return null;
        }

        @Override
        public void load() throws Exception
        {
            // TODO Auto-generated method stub
            
        }

        @Override
        public void save() throws Exception
        {
            // TODO Auto-generated method stub
            
        } 
        
    }
    
    @Test
    public void testBasicAuthenticationRootParamsNoPW() throws DocumentException, Exception
    {
        
        Log.log = Logger.getLogger(getClass());
        
        final Server server;
         
   
        ConfigReceiveGateway config = new TestConfigNoRootPWConfig(new XDoc()) ;
           
        
        
        Assert.assertTrue(BeanUtils.getSimpleProperty(config, "httpAuthType") != null);
        
        server = new Server();
        HttpConfiguration http_config = new HttpConfiguration();
        http_config.setSecureScheme("https");
        http_config.setSendXPoweredBy(false);
        http_config.setSendServerVersion(false);
        
        
        ServerConnector connector = new ServerConnector(server, new HttpConnectionFactory(http_config));
        connector.setPort(8829);
        connector.setIdleTimeout(30000);
        server.addConnector(connector);
        
        
        
        ServletContextHandler context = new ServletContextHandler(server, "/");
        try
        {
            server.setHandler(PushAuthHandlerFactory.wrapHandler(config, server, context));
            assert(server.getHandler() instanceof ConstraintSecurityHandler);
        }
        catch (UnknownAuthenticationTypeException | InvalidAuthConfigurationException e)
        {
            Log.log.error(e.getMessage(), e);
        }
        
        context.addServlet(new ServletHolder(new EchoServlet()), "/echo");
        boolean thrown = false;
        try {
        
        PushAuthHandlerFactory.registerFilter(config, server.getHandler(), new Object() {});
        }catch(InvalidAuthConfigurationException e)
        {
            
            thrown = true;
            
        }
        
        
        server.stop();
        Assert.assertTrue(thrown);
    }
    
    public static class TestBasicConfigWithRootUNPW extends ConfigReceiveGateway
    {
        
        public TestBasicConfigWithRootUNPW(XDoc config) throws Exception
        {
            super(config);
            // TODO Auto-generated constructor stub
        }
        
        public String getHttpAuthBasicUsername()
        {
            return "testuser";
            
        }
        
        public String getHttpAuthBasicPassword()
        {
            
            return "pw";
        }
        public String getHttpAuthType()
        {
            return "BASIC";
        }
        public void setHttpAuthType(String s)
        { 
        }
        @Override
        public String getRootNode()
        {
            // TODO Auto-generated method stub
            return null;
        }

        @Override
        public void load() throws Exception
        {
            // TODO Auto-generated method stub
            
        }

        @Override
        public void save() throws Exception
        {
            // TODO Auto-generated method stub
            
        } 
      
    }
    public static class FilterWithPath
    {
        public String getUri()
        {
            
            return "/echo";
        }
        
    }
    
    /**
     * 
     * Tests using the password in the blueprint as no filter pw is set
     * @throws DocumentException
     * @throws Exception
     */
    @Test
    public void testAuthenticationWithBlueprintUNPW() throws DocumentException, Exception
    {
        
        Log.log = Logger.getLogger(getClass());
        
        final Server server;
         
   
        ConfigReceiveGateway config = new  TestBasicConfigWithRootUNPW(new XDoc());
        server = new Server();
        HttpConfiguration http_config = new HttpConfiguration();
        http_config.setSecureScheme("https");
        http_config.setSendXPoweredBy(false);
        http_config.setSendServerVersion(false);
        
        
        ServerConnector connector = new ServerConnector(server, new HttpConnectionFactory(http_config));
        connector.setPort(8829);
        connector.setIdleTimeout(30000);
        server.addConnector(connector);
        
        
        
        ServletContextHandler context = new ServletContextHandler(server, "/");
        try
        {
            server.setHandler(PushAuthHandlerFactory.wrapHandler(config, server, context));
        }
        catch (UnknownAuthenticationTypeException | InvalidAuthConfigurationException e)
        {
            Log.log.error(e.getMessage(), e);
        }
        
        context.addServlet(new ServletHolder(new EchoServlet()), "/echo");
        
        PushAuthHandlerFactory.registerFilter(config, server.getHandler(), new FilterWithPath());
        
        server.start();
        
        URL url = new URL ("http://localhost:8829/echo");
 
        HttpURLConnection connection = (HttpURLConnection) url.openConnection();
        connection.setRequestMethod("GET");

        String encoding =          B64Code.encode("testuser:pw",StringUtil.__ISO_8859_1);
 
        
        connection.setRequestProperty  ("Authorization", "Basic " + encoding);

         
        assertEquals( connection.getResponseCode(),200);
        
        connection.disconnect();
    
        
        server.stop();
        
    }
    
    /**
     * Simple object that exposes the properties we're looking for when a UN/PW is set in the filter
     *  
     *
     */
    public static class FilterWithUriAndPW
    {
        public String getUri()
        {
            
            return "/echo";
        }
        public String getHttpAuthBasicUsername()
        {
            return "filteruser";
            
        }
        
        public String getHttpAuthBasicPassword()
        {
            
            return "fpw";
        }
    }
    
    
    /**
     * Uses filter UN/PW when gateway and filter password are set. 
     * @throws DocumentException
     * @throws Exception
     */
    @Test
    public void testAuthenticationWithBlueprintUNPWandFilterUNPW() throws DocumentException, Exception
    {
        
        Log.log = Logger.getLogger(getClass());
        
        final Server server;
         
   
        ConfigReceiveGateway config = new  TestBasicConfigWithRootUNPW(new XDoc());
        server = new Server();
        HttpConfiguration http_config = new HttpConfiguration();
        http_config.setSecureScheme("https");
        http_config.setSendXPoweredBy(false);
        http_config.setSendServerVersion(false);
        
        
        ServerConnector connector = new ServerConnector(server, new HttpConnectionFactory(http_config));
        connector.setPort(8829);
        connector.setIdleTimeout(30000);
        server.addConnector(connector);
        
        
        
        ServletContextHandler context = new ServletContextHandler(server, "/");
        try
        {
            server.setHandler(PushAuthHandlerFactory.wrapHandler(config, server, context));
        }
        catch (UnknownAuthenticationTypeException | InvalidAuthConfigurationException e)
        {
            Log.log.error(e.getMessage(), e);
        }
        
        context.addServlet(new ServletHolder(new EchoServlet()), "/echo");
        
        PushAuthHandlerFactory.registerFilter(config, server.getHandler(), new FilterWithUriAndPW());
        
        server.start();
        
        URL url = new URL ("http://localhost:8829/echo");
 
        HttpURLConnection connection = (HttpURLConnection) url.openConnection();
        connection.setRequestMethod("GET");

        String encoding =          B64Code.encode("filteruser:fpw",StringUtil.__ISO_8859_1);
 
        
        connection.setRequestProperty  ("Authorization", "Basic " + encoding);

         
        assertEquals( connection.getResponseCode(),200);
        
        connection.disconnect();
    
        
        server.stop();
        
    }
    
    
    /**
     * Tests using gateway password even though the filter password is set.  Checks to ensure 401
     * @throws DocumentException
     * @throws Exception
     */
    @Test
    public void testAuthenticationWithBlueprintUNPWandFilterUNPWFailWithGWPW() throws DocumentException, Exception
    {
        
        Log.log = Logger.getLogger(getClass());
        
        final Server server;
         
   
        ConfigReceiveGateway config = new  TestBasicConfigWithRootUNPW(new XDoc());
        server = new Server();
        HttpConfiguration http_config = new HttpConfiguration();
        http_config.setSecureScheme("https");
        http_config.setSendXPoweredBy(false);
        http_config.setSendServerVersion(false);
        
        
        ServerConnector connector = new ServerConnector(server, new HttpConnectionFactory(http_config));
        connector.setPort(8829);
        connector.setIdleTimeout(30000);
        server.addConnector(connector);
        
        
        
        ServletContextHandler context = new ServletContextHandler(server, "/");
        try
        {
            server.setHandler(PushAuthHandlerFactory.wrapHandler(config, server, context));
        }
        catch (UnknownAuthenticationTypeException | InvalidAuthConfigurationException e)
        {
            Log.log.error(e.getMessage(), e);
        }
        
        context.addServlet(new ServletHolder(new EchoServlet()), "/echo");
        
        PushAuthHandlerFactory.registerFilter(config, server.getHandler(), new FilterWithUriAndPW());
        
        server.start();
        
        URL url = new URL ("http://localhost:8829/echo");
 
        HttpURLConnection connection = (HttpURLConnection) url.openConnection();
        connection.setRequestMethod("GET");

        String encoding =          B64Code.encode("testuser:pw",StringUtil.__ISO_8859_1);
 
        
        connection.setRequestProperty  ("Authorization", "Basic " + encoding);

         
        assertEquals( connection.getResponseCode(),401);
        
        connection.disconnect();
    
        
        server.stop();
        
    }
    /**
     * Simple object that exposes the properties we're looking for when a UN/PW is set in the filter
     *  
     *
     */
    public static class FilterWithUriAndNoPW
    {
        public String getUri()
        {
            
            return "/echo";
        }
        public String getHttpAuthBasicUsername()
        {
            return "filteruser";
            
        }
        
        public String getHttpAuthBasicPassword()
        {
            
            return null;
        }
    }
    
    /**
     * Tests unregistering the filter
     * @throws DocumentException
     * @throws Exception
     */
    @Test
    public void testUnregisterRegisterFilter() throws DocumentException, Exception
    {
        
        Log.log = Logger.getLogger(getClass());
        
        final Server server;
         
   
        ConfigReceiveGateway config = new  TestBasicConfigWithRootUNPW(new XDoc());
        server = new Server();
        HttpConfiguration http_config = new HttpConfiguration();
        http_config.setSecureScheme("https");
        http_config.setSendXPoweredBy(false);
        http_config.setSendServerVersion(false);
        
        
        ServerConnector connector = new ServerConnector(server, new HttpConnectionFactory(http_config));
        connector.setPort(8829);
        connector.setIdleTimeout(30000);
        server.addConnector(connector);
        
        
        
        ServletContextHandler context = new ServletContextHandler(server, "/");
        try
        {
            server.setHandler(PushAuthHandlerFactory.wrapHandler(config, server, context));
        }
        catch (UnknownAuthenticationTypeException | InvalidAuthConfigurationException e)
        {
            Log.log.error(e.getMessage(), e);
        }
        
        context.addServlet(new ServletHolder(new EchoServlet()), "/echo");
        
        PushAuthHandlerFactory.registerFilter(config, server.getHandler(), new FilterWithUriAndPW());
    
        PushAuthHandlerFactory.unregisterFilter(config, server.getHandler(), new FilterWithUriAndPW());
        
       
   
        
    }
    /**
     * Uses filter UN/PW when gateway and filter password are set. 
     * @throws DocumentException
     * @throws Exception
     */
    @Test
    public void testAuthenticationWithBadFilterPW() throws DocumentException, Exception
    {
        
        Log.log = Logger.getLogger(getClass());
        
        final Server server;
         
   
        ConfigReceiveGateway config = new  TestBasicConfigWithRootUNPW(new XDoc());
        server = new Server();
        HttpConfiguration http_config = new HttpConfiguration();
        http_config.setSecureScheme("https");
        http_config.setSendXPoweredBy(false);
        http_config.setSendServerVersion(false);
        
        
        ServerConnector connector = new ServerConnector(server, new HttpConnectionFactory(http_config));
        connector.setPort(8829);
        connector.setIdleTimeout(30000);
        server.addConnector(connector);
        
        
        
        ServletContextHandler context = new ServletContextHandler(server, "/");
        try
        {
            server.setHandler(PushAuthHandlerFactory.wrapHandler(config, server, context));
        }
        catch (UnknownAuthenticationTypeException | InvalidAuthConfigurationException e)
        {
            Log.log.error(e.getMessage(), e);
        }
        
        context.addServlet(new ServletHolder(new EchoServlet()), "/echo");
        boolean thrown = false;
        try {
        
            PushAuthHandlerFactory.registerFilter(config, server.getHandler(), new FilterWithUriAndNoPW());
        
        
        } catch(InvalidAuthConfigurationException e)
        {
            
            thrown = true;
        }
        server.start();
         
        assertTrue(thrown);
        
        server.stop();
        
    }
    /**
     * Tests with authentication disabled
     * @throws DocumentException
     * @throws Exception
     */
    @Test
    public void testNoAuthentication() throws DocumentException, Exception
    {
        
        Log.log = Logger.getLogger(getClass());
        
        final Server server;
         
   
        ConfigReceiveGateway config = new ConfigReceiveGateway(new XDoc()) {

            @Override
            public String getRootNode()
            {
                // TODO Auto-generated method stub
                return null;
            }

            @Override
            public void load() throws Exception
            {
                // TODO Auto-generated method stub
                
            }

            @Override
            public void save() throws Exception
            {
                // TODO Auto-generated method stub
                
            }}
        ;
        server = new Server();
        HttpConfiguration http_config = new HttpConfiguration();
        http_config.setSecureScheme("https");
        http_config.setSendXPoweredBy(false);
        http_config.setSendServerVersion(false);
        
        
        ServerConnector connector = new ServerConnector(server, new HttpConnectionFactory(http_config));
        connector.setPort(8829);
        connector.setIdleTimeout(30000);
        server.addConnector(connector);
        
        
        
        ServletContextHandler context = new ServletContextHandler(server, "/");
        try
        {
            server.setHandler(PushAuthHandlerFactory.wrapHandler(config, server, context));
        }
        catch (UnknownAuthenticationTypeException | InvalidAuthConfigurationException e)
        {
            Log.log.error(e.getMessage(), e);
        }
        
        context.addServlet(new ServletHolder(new EchoServlet()), "/echo");
        
        PushAuthHandlerFactory.registerFilter(config, server.getHandler(), new Object() {});
        
        server.start();
        
        
        URL url = new URL ("http://localhost:8829/login");
 
        HttpURLConnection connection = (HttpURLConnection) url.openConnection();
        connection.setRequestMethod("GET");
        
        String encoding =  Base64.encodeBytes("test1:test1".getBytes());

      //  connection.setRequestProperty  ("Authorization", "Basic " + encoding);

         
        assert( connection.getResponseCode() == 200);
        
        connection.disconnect();
    
        
        server.stop();
        
    }
    public class EchoServlet extends HttpServlet {
        /**
         * 
         */
        private static final long serialVersionUID = 4229826376735383176L;

        /**
         * Get a String-object from the applet and send it back.
         */
        public void doGet(
            HttpServletRequest request,
            HttpServletResponse response)
            throws ServletException, IOException {
            try {
                response.setContentType("text/plain");

                
                String echo = "Echo";

                // echo it to the applet
                OutputStream outstr = response.getOutputStream();
               
                outstr.write(echo.getBytes());
                outstr.flush();
                outstr.close();

            } catch (Exception e) {
                e.printStackTrace();
            }
        }

    }
}
