package com.resolve.services.hibernate.vo;

import com.resolve.util.MappingAnnotation;

public class MariadbFilterVO extends GatewayFilterVO {

    private static final long serialVersionUID = 1L;

    public MariadbFilterVO() {
    }

    private String UQuery;

    @MappingAnnotation(columnName = "QUERY")
    public String getUQuery() {
        return this.UQuery;
    }

    public void setUQuery(String uQuery) {
        this.UQuery = uQuery;
    }
}

