package com.resolve.rscontrol;

import com.resolve.persistence.model.MariadbFilter;

public class MMariadb extends MGateway {

    private static final String MODEL_NAME = MariadbFilter.class.getSimpleName();

    protected String getModelName() {
        return MODEL_NAME;
    }
}

