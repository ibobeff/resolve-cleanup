package com.resolve.persistence.model;

import java.util.List;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Lob;
import javax.persistence.Table;
import javax.persistence.UniqueConstraint;
import com.resolve.services.interfaces.VO;
import com.resolve.services.hibernate.vo.MariadbFilterVO;

@Entity
@Table(name = "mariadb_filter", uniqueConstraints = { @UniqueConstraint(columnNames = { "u_name", "u_queue" }) })
public class MariadbFilter extends GatewayFilter<MariadbFilterVO> {

    private static final long serialVersionUID = 1L;

    public MariadbFilter() {
    }

    public MariadbFilter(MariadbFilterVO vo) {
        applyVOToModel(vo);
    }

    private String UQuery;

    @Column(name = "u_query", length = 400)
    public String getUQuery() {
        return this.UQuery;
    }

    public void setUQuery(String uQuery) {
        this.UQuery = uQuery;
    }

    public MariadbFilterVO doGetVO() {
        MariadbFilterVO vo = new MariadbFilterVO();
        super.doGetBaseVO(vo);
        vo.setUQuery(getUQuery());
        return vo;
    }

    @Override
    public void applyVOToModel(MariadbFilterVO vo) {
        if (vo == null) return;
        super.applyVOToModel(vo);
        if (!VO.STRING_DEFAULT.equals(vo.getUQuery())) this.setUQuery(vo.getUQuery()); else ;
    }
}

