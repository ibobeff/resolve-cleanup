package com.resolve.gateway;

import java.util.Map;
import com.mariadb.gateway.MariadbFilter;
import com.mariadb.gateway.MariadbGateway;

public class MMariadb extends MGateway {

    private static final String RSCONTROL_SET_FILTERS = "MMariadb.setFilters";

    private static final MariadbGateway instance = MariadbGateway.getInstance();

    public MMariadb() {
        super.instance = instance;
    }

    protected String getSetFilterMethodName() {
        return RSCONTROL_SET_FILTERS;
    }

    /**
     * Populates the {@link Filter} data into the given filterMap {@link Map} instance
     * 
     * @param filterMap
     *      the {@link Map} instance to hold the {@link Filter} data
     * @param filter
     *      a {@link MariadbFilter} instance
     *            
     */
    public void populateGatewaySpecificFilterProperties(Map<String, String> filterMap, Filter filter) {
        if (instance.isActive()) {
            MariadbFilter mariadbFilter = (MariadbFilter) filter;
            filterMap.put(mariadbFilter.QUERY, mariadbFilter.getQuery());
        }
    }
}

