package com.mariadb.gateway;

import java.io.File;
import java.util.HashMap;
import java.util.Map;
import org.apache.commons.io.FileUtils;

import com.resolve.rsbase.MainBase;
import com.resolve.gateway.Filter;
import com.resolve.rsremote.ConfigReceiveGateway;
import com.resolve.util.Log;
import com.resolve.util.ObjectProperties;
import com.resolve.util.StringUtils;
import com.resolve.util.XDoc;

public class ConfigReceiveMariadb extends ConfigReceiveGateway {

    private static final long serialVersionUID = 1L;

    private static final String RECEIVE_MARIADB_NODE = "./RECEIVE/MARIADB/";

    private static final String RECEIVE_MARIADB_FILTER = RECEIVE_MARIADB_NODE + "FILTER";

    private String queue = "MARIADB";

    private static final String RECEIVE_MARIADB_ATTR_USERNAME = RECEIVE_MARIADB_NODE + "@USERNAME";

    private static final String RECEIVE_MARIADB_ATTR_PORT = RECEIVE_MARIADB_NODE + "@PORT";

    private static final String RECEIVE_MARIADB_ATTR_DBNAME = RECEIVE_MARIADB_NODE + "@DBNAME";

    private static final String RECEIVE_MARIADB_ATTR_IP = RECEIVE_MARIADB_NODE + "@IP";

    private static final String RECEIVE_MARIADB_ATTR_PASSWORD = RECEIVE_MARIADB_NODE + "@PASSWORD";

    private String username = "";

    private int port = 0;

    private String dbname = "";

    private String ip = "";

    private String password = "";

    public String getQueue() {
        return queue;
    }

    public void setQueue(String queue) {
        this.queue = queue;
    }

    public String getUsername() {
        return this.username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public int getPort() {
        return this.port;
    }

    public void setPort(int port) {
        this.port = port;
    }

    public String getDbname() {
        return this.dbname;
    }

    public void setDbname(String dbname) {
        this.dbname = dbname;
    }

    public String getIp() {
        return this.ip;
    }

    public void setIp(String ip) {
        this.ip = ip;
    }

    public String getPassword() {
        return this.password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public ConfigReceiveMariadb(XDoc config) throws Exception {
        super(config);
        define("username", STRING, RECEIVE_MARIADB_ATTR_USERNAME);
        define("port", INTEGER, RECEIVE_MARIADB_ATTR_PORT);
        define("dbname", STRING, RECEIVE_MARIADB_ATTR_DBNAME);
        define("ip", STRING, RECEIVE_MARIADB_ATTR_IP);
        define("password", SECURE, RECEIVE_MARIADB_ATTR_PASSWORD);
    }

    @Override
    public String getRootNode() {
        return RECEIVE_MARIADB_NODE;
    }

    @Override
    public void load() throws Exception {
        try {
            loadAttributes();
            if (isActive()) {
                MariadbGateway mariadbGateway = MariadbGateway.getInstance(this);
                filters = xdoc.getListMapValue(RECEIVE_MARIADB_FILTER);
                Log.log.trace("Loading " + filters.size() + " MariadbFilter from config");
                if (filters.size() > 0) {
                    for (Map<String, Object> values : filters) {
                        String filterName = ((String) values.get(MariadbFilter.ID)).toLowerCase();
                        File scriptFile = getFile(MainBase.main.release.serviceName + "/config/mariadb/" + filterName + ".groovy");
                        try {
                            if (scriptFile.exists()) {
                                String groovy = FileUtils.readFileToString(scriptFile, "UTF-8");
                                if (StringUtils.isNotBlank(groovy)) {
                                    values.put(MariadbFilter.SCRIPT, groovy);
                                    Log.log.debug("Loaded groovy script: " + scriptFile);
                                }
                            }
                            mariadbGateway.setFilter(values);
                        } catch (Exception e) {
                            Log.log.warn(e.getMessage() + ". Skipping file: " + scriptFile.getAbsolutePath());
                        }
                    }
                }
            }
        } catch (Throwable e) {
            setActive(false);
            Log.log.warn("Couldn not load configuration for Mariadb gateway, check blueprint. " + e.getMessage(), e);
        }
    }

    @Override
    public void save() throws Exception {
        File dir = getFile(MainBase.main.release.serviceName + "/config/mariadb");
        if (!dir.exists()) {
            FileUtils.forceMkdir(dir);
        }
        if (filters != null && isActive()) {
            filters.clear();
            for (Filter filter : MariadbGateway.getInstance().getFilters().values()) {
                MariadbFilter mariadbFilter = (MariadbFilter) filter;
                String groovy = mariadbFilter.getScript();
                String scriptFileName;
                File scriptFile = null;
                if (StringUtils.isNotBlank(groovy)) {
                    scriptFileName = mariadbFilter.getId().toLowerCase() + ".groovy";
                    scriptFile = getFile(MainBase.main.release.serviceName + "/config/mariadb/" + scriptFileName);
                }
                Map<String, Object> entry = new HashMap<String, Object>();
                putFilterDataIntoEntry(entry, mariadbFilter);
                filters.add(entry);
                if (scriptFile != null) {
                    try {
                        FileUtils.writeStringToFile(scriptFile, groovy, "UTF-8");
                    } catch (Exception e) {
                        Log.log.warn(e.getMessage() + ". Skipping file: " + scriptFile.getAbsolutePath());
                    }
                }
            }
            xdoc.setListMapValue(RECEIVE_MARIADB_FILTER, filters);
        }
        if (nameProperties != null && isActive()) {
            nameProperties.clear();
            for (String name : MariadbGateway.getInstance().getNameProperties().keySet()) {
                Map<String, Object> entry = new HashMap<String, Object>();
                entry.put(NAME_KEY, name);
                nameProperties.add(entry);
                Map props = MariadbGateway.getInstance().getNameProperties().getNameProperties(name);
                ObjectProperties prop = new ObjectProperties(props);
                prop.save(props);
            }
        }
        saveAttributes();
    }

    private void putFilterDataIntoEntry(Map<String, Object> entry, MariadbFilter mariadbFilter) {
        entry.put(MariadbFilter.ID, mariadbFilter.getId());
        entry.put(MariadbFilter.ACTIVE, String.valueOf(mariadbFilter.isActive()));
        entry.put(MariadbFilter.ORDER, String.valueOf(mariadbFilter.getOrder()));
        entry.put(MariadbFilter.INTERVAL, String.valueOf(mariadbFilter.getInterval()));
        entry.put(MariadbFilter.EVENT_EVENTID, mariadbFilter.getEventEventId());
        entry.put(MariadbFilter.RUNBOOK, mariadbFilter.getRunbook());
        entry.put(MariadbFilter.SCRIPT, mariadbFilter.getScript());
        entry.put(mariadbFilter.QUERY, mariadbFilter.getQuery());
    }
}

