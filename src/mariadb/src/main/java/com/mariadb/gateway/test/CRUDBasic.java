package com.mariadb.gateway.test;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map.Entry;

import org.junit.BeforeClass;
import org.junit.Test;
import org.junit.runner.RunWith;

import com.mariadb.gateway.MariadbAPI;
import com.resolve.rsgatewayemulator.TestRunner;

@RunWith(TestRunner.class)
public class CRUDBasic
{
    String objType = new String("api_test");
    static HashMap<String, String> objParams = new HashMap<String, String>();
    static ArrayList<String> columns = new ArrayList<String>();
    static String objId = new String("name=newName&description=say something about new created api_test"); 
    @BeforeClass public static void initParam() {
        
        columns.add("id");
        columns.add("name");
        columns.add("description");
        
        objParams.put("name", "newName");
        objParams.put("description", "say something about new created api_test");
     }
    
    @Test
    public void testGetObjectTypes() throws Exception{
        assertTrue(MariadbAPI.getObjectTypes().contains(objType));
    }
    
    @Test
    public void testCreateObject() throws Exception{
        MariadbAPI.createObject(objType, objParams);
        assertTrue(MariadbAPI.readObject(objType, objId, columns).size() > 0);
        MariadbAPI.deleteObject(objType, objId);            
    }
    
    @Test
    public void testReadObject() throws Exception{
        MariadbAPI.createObject(objType, objParams);
        assertEquals(MariadbAPI.readObject(objType, objId, columns).get("name"), new String("newName"));
        assertEquals(MariadbAPI.readObject(objType, objId, columns).get("description"), new String("say something about new created api_test"));
        MariadbAPI.deleteObject(objType, objId);  
    }
    
    @Test
    public void testUpdateObject() throws Exception{
        MariadbAPI.createObject(objType, objParams);
        String id = new StringBuilder("id=").append(MariadbAPI.readObject(objType, objId, columns).get("id")).toString();
        
        HashMap<String, String> updateParams = new HashMap<String, String>();
        updateParams.put("name", "updateName");
        updateParams.put("description", "say something about new updated api_test");
        MariadbAPI.updateObject(objType, id, updateParams);
         
        
        for(Entry<String, String> e : updateParams.entrySet()){
            assertEquals(MariadbAPI.readObject(objType, id, columns).get(e.getKey()), e.getValue());
        }
         
        MariadbAPI.deleteObject(objType, id);
    }
    
    @Test
    public void testDeleteObject() throws Exception{
        MariadbAPI.createObject(objType, objParams);
        MariadbAPI.deleteObject(objType, objId);  
        assertTrue(MariadbAPI.readObject(objType, objId, columns).size() == 0);
    }
    
}
