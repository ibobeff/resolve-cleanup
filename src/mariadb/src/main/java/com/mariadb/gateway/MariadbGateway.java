package com.mariadb.gateway;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;
import java.util.List;

import org.mariadb.jdbc.MySQLDataSource;

import com.resolve.gateway.BaseClusteredGateway;
import com.resolve.gateway.BaseFilter;
import com.resolve.gateway.Filter;
import com.resolve.gateway.MMariadb;
import com.resolve.util.Log;
import com.resolve.rsremote.ConfigReceiveGateway;

public class MariadbGateway extends BaseClusteredGateway {

    // Singleton
    private static volatile MariadbGateway instance = null;
    private Connection conn = null;
    private String queue = null;
    
    
    public Connection getConn()
    {
        return conn;
    }

    public void setConn(Connection conn)
    {
        this.conn = conn;
    }


    public static MariadbGateway getInstance(ConfigReceiveMariadb config) {
        if (instance == null) {
            instance = new MariadbGateway(config);
        }
        return instance;
    }

    /**
     * This method is called once the initialization is done. If it gets called
     * before the initialization it will throw exception.
     *
     * @return
     */
    public static MariadbGateway getInstance() {
        if (instance == null) {
            throw new RuntimeException("Mariadb Gateway is not initialized correctly..");
        } else {
            return instance;
        }
    }

    private MariadbGateway(ConfigReceiveMariadb config) {
        super(config);
    }

    @Override
    public String getLicenseCode() {
        return "MARIADB";
    }

    @Override
    protected void addAdditionalValues(Map<String, Object> arg0, Map<String, String> arg1) {
    }

    @Override
    protected String getGatewayEventType() {
        return "MARIADB";
    }

    @Override
    protected String getMessageHandlerName() {
        return MMariadb.class.getSimpleName();
    }

    @Override
    protected Class<MMariadb> getMessageHandlerClass() {
        return MMariadb.class;
    }

    public String getQueueName() {
        return queue;
    }

    @Override
    public void start() {
        Log.log.debug("Starting Mariadb Gateway");
        super.start();
    }

    @Override
    public void run() {
        if (!((ConfigReceiveGateway) configurations).isMock()) super.sendSyncRequest();
        while (running) {
            try {
                long startTime = System.currentTimeMillis();
                if (primaryDataQueue.isEmpty()) {
                    Log.log.trace("Local Primary Event Queue for " + getGatewayEventType() + " is empty.....");
                    for (Filter filter : orderedFilters) {
                        if (shouldFilterExecute(filter, startTime)) {
                            Log.log.trace("Filter " + filter.getId() + " executing");
                            List<Map<String, String>> results = invokeService(filter);
                            for (Map<String, String> result : results) {
                                addToPrimaryDataQueue(filter, result);
                            }
                        } else {
                            Log.log.trace("Filter:" + filter.getId() + " is inactive or not ready to execute yet.....");
                        }
                    }
                    if(orderedFilters==null || orderedFilters.size()==0){
                        Log.log.trace("There is not deployed filter for MariadbGateway");
                    }
                }
                if (orderedFilters.size() == 0) {
                    Thread.sleep(interval);
                }
            } catch (Exception e) {
                Log.log.warn("Warning: " + e.getMessage());
                try {
                    Thread.sleep(interval);
                } catch (InterruptedException ie) {
                    Log.log.warn("sleep interrupted: " + ie.getMessage(), ie);
                }
            }
        }
    }

    /**
     * Add customized code here to initilize the connection with 3rd party system
     * @return List<Map<String, String>>
     */
    @Override
    protected void initialize() {
        ConfigReceiveMariadb config = (ConfigReceiveMariadb) configurations;
        queue = config.getQueue().toUpperCase();
        this.gatewayConfigDir = "/config/mariadb/";
        
        try
        {
            MySQLDataSource DS = new MySQLDataSource(config.getIp(), config.getPort(), config.getDbname());
            
            conn = DS.getConnection(config.getUsername(), config.getPassword());
            
        }
        catch (SQLException e)
        {
            Log.log.error(e.getMessage(), e);
        }
    }

    /**
     * Add customized code here to get the data as List<Map<String, String>> format for each filter 
     * @return List<Map<String, String>>
     */
    public List<Map<String, String>> invokeService(Filter filter) {
        List<Map<String, String>> result = new ArrayList<Map<String, String>>();
        
        Statement stmt;
        try
        {
            stmt = conn.createStatement();
            Log.log.debug("Execute Query:" + ((MariadbFilter) filter).getQuery());
            ResultSet rs = stmt.executeQuery(((MariadbFilter) filter).getQuery());
            while (rs.next()) {
                ResultSetMetaData resultsetmetadata = rs.getMetaData();
                Map<String, String> runbookParams = new HashMap<String, String>();
                for (int i = 1; i <= resultsetmetadata.getColumnCount(); i++)
                {
                    String name = resultsetmetadata.getColumnName(i);
                    String value = rs.getString(i);
                    runbookParams.put(name, value);
                }
                result.add(runbookParams);
            }
        }
        catch (SQLException e)
        {
            Log.log.error(e.getMessage(), e);
        }
        
        
        return result;
    }

    /**
     * Add customized code here to do when stop the gateway
     */ 
    @Override
    public void stop(){
        if(conn!=null){
            try
            {
                conn.close();
            }
            catch (SQLException e)
            {
                Log.log.error(e.getMessage(), e);
            }
        }
        Log.log.warn("Stopping Mariadb gateway");
        super.stop();
    }
    
    @Override
    public Filter getFilter(Map<String, Object> params) {
        return new MariadbFilter((String) params.get(BaseFilter.ID), (String) params.get(BaseFilter.ACTIVE), (String) params.get(BaseFilter.ORDER), (String) params.get(BaseFilter.INTERVAL), (String) params.get(BaseFilter.EVENT_EVENTID), (String) params.get(BaseFilter.RUNBOOK), (String) params.get(BaseFilter.SCRIPT), (String) params.get(MariadbFilter.QUERY));
    }
}

