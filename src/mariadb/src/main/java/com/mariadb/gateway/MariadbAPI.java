package com.mariadb.gateway;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;

import com.resolve.gateway.AbstractGatewayAPI;

public class MariadbAPI extends AbstractGatewayAPI
{
    /**
     * Returns {@link Set} of supported object types.
     * 
     * @return the set of tables we could do CRUD operation in mariadb
     * @throws SQLException 
     */
    public static Set<String> getObjectTypes() 
    {
        Connection conn = MariadbGateway.getInstance().getConn();
        Statement stmt;
        HashSet<String> tables = new HashSet<String>();
        try
        {
            stmt = conn.createStatement();
            ResultSet rs = stmt.executeQuery("show tables");
            
            while (rs.next()) {
                tables.add(rs.getString(1));
            }
        }
        catch (SQLException e)
        {
            System.err.println(e.getMessage());
        }
        
        return tables;
    }
    
    /**
     * Creates instance of specified object type in external system.
     * <p>
     * Default attribute name-value pair returned is  
     * {@value com.resolve.gateway.GatewayConstants#METHOD_NOT_SUPPORTED_KEY}-true
     * indicating external system specific gateway does not support creating instances of
     * specified object type.
     * <p>
     * If external system specific gateway does support creation of object then it must return
     * {@link Map} of attribute name-value pairs.
     * 
     * @param  objType   Type of object to create in external system
     * @param  objParams {@link Map} of object parameters
     * @param  userName  User name
     * @param  password  Password
     * @return           Attribute id-value {@link Map} of created object in external system
     * @throws Exception If any error occurs in creating object of specified
     *                   type in external system
     */
    public static Map<String, String> createObject(String objType, Map<String, String> objParams) throws Exception
    {
        if(objType==null || objType.isEmpty()) return null;
        if(objParams==null || objParams.isEmpty()) return null;
        Connection conn = MariadbGateway.getInstance().getConn();
        Statement stmt = conn.createStatement();
        StringBuilder query = new StringBuilder("insert into ");
     
        query.append(objType);
        
        StringBuilder values = new StringBuilder(" values (");
        query.append(" (");
        for(Entry<String, String> param : objParams.entrySet()){
            query.append(param.getKey());
            query.append(",");
            values.append("'");
            values.append(param.getValue());
            values.append("',");
        }
        query.setCharAt(query.length()-1, ')');
        values.setCharAt(values.length()-1, ')');
        query.append(values);
        
        Map<String, String> result = new HashMap<String, String>();
        ResultSet rs = stmt.executeQuery(query.toString());
        
        while (rs.next()) {
            ResultSetMetaData resultsetmetadata = rs.getMetaData();
            
            for (int i = 1; i <= resultsetmetadata.getColumnCount(); i++)
            {
                String name = resultsetmetadata.getColumnName(i);
                String value = rs.getString(i);
                result.put(name, value);
            }
        }
        
        return result;
    }
    
    /**
     * Reads attributes of specified object in mariadb.
     * @param  objType    table name
     * @param  objId      Id of the object to read attributes of from external system
     * @param  attribs    {@link List} of columns of object to read
     * @param  userName   User name
     * @param  password   Password
     * @return            {@link Map} of object attribute column-value pairs
     * @throws Exception  If any error occurs in reading attributes of the object in 
     *                    external system
     */
    public static Map<String, String> readObject(String objType, String objId, List<String> attribs) throws Exception
    {
        if(objType==null || objType.isEmpty()) return null;
        Connection conn = MariadbGateway.getInstance().getConn();
        Statement stmt = conn.createStatement();
        StringBuilder query = new StringBuilder("select ");
        if(attribs==null || attribs.isEmpty()) query.append("* ");
        else{
            for(String attr : attribs){
                query.append(attr + ", ");
            }
            query.setLength(query.length()-2);
        }
        query.append(" from " + objType);
        
        if(objId!=null && !objId.isEmpty()){
            query.append(" where ");
            for(String clause : objId.split("&")){
                String[] s = clause.split("=");
                query.append(s[0]);
                query.append("='");
                query.append(s[1]);
                query.append("' and ");
            }
            query.setLength(query.length()-4);
            
        }
        
        Map<String, String> result = new HashMap<String, String>();
        ResultSet rs = stmt.executeQuery(query.toString());
        
        while (rs.next()) {
            ResultSetMetaData resultsetmetadata = rs.getMetaData();
            
            for (int i = 1; i <= resultsetmetadata.getColumnCount(); i++)
            {
                String name = resultsetmetadata.getColumnName(i);
                String value = rs.getString(i);
                result.put(name, value);
            }
        }
        return result;
    }
    

    /**
     * Updates attributes of specified object in external system.
     * 
     * @param  objType   Object type
     * @param  objId     Id of the object to update attributes of in external system
     * @param  updParams Key-value {@link Map} of object attributes to update
     * @param  userName  User name
     * @param  password  Password
     * @return           Key-value {@link Map} of the updated object attributes
     * @throws Exception If any error occurs in updating attributes of the object in 
     *                   external system
     */
    public static Map<String, String> updateObject(String objType, String objId, Map<String, String> updParams) throws Exception
    {
        if(objType==null || objType.isEmpty()) return null;
        if(updParams==null || updParams.isEmpty()) return null;
        Connection conn = MariadbGateway.getInstance().getConn();
        Statement stmt = conn.createStatement();
        StringBuilder query = new StringBuilder("update ");
     
        query.append(objType);
        
        query.append(" set ");
        for(Entry<String, String> param : updParams.entrySet()){
            query.append(param.getKey());
            query.append("='");
            query.append(param.getValue());
            query.append("',");
        }
        query.setLength(query.length()-1);
        
        if(objId!=null && !objId.isEmpty()){
            query.append(" where ");
            for(String clause : objId.split("&")){
                String[] s = clause.split("=");
                query.append(s[0]);
                query.append("='");
                query.append(s[1]);
                query.append("' and ");
            }
            query.setLength(query.length()-4);
            
        }
        Map<String, String> result = new HashMap<String, String>();
        ResultSet rs = stmt.executeQuery(query.toString());
        
        while (rs.next()) {
            ResultSetMetaData resultsetmetadata = rs.getMetaData();
            
            for (int i = 1; i <= resultsetmetadata.getColumnCount(); i++)
            {
                String name = resultsetmetadata.getColumnName(i);
                String value = rs.getString(i);
                result.put(name, value);
            }
        }
        
        return result;
    }
    
    /**
     * Deletes specified object from external system.
     * <p>
     * Default deleted attribute name-value pair returned is  
     * {@value com.resolve.gateway.GatewayConstants#METHOD_NOT_SUPPORTED_KEY}-true
     * indicating external system specific gateway does not support object deletion.
     * <p>
     * If external system specific gateway does support object deletion then it 
     * must return {@link Map} of object deletion operation reult.
     * 
     * @param  objType   Object type
     * @param  objId     Id of the object to delete from external system
     * @param  userName  User name
     * @param  password  Password
     * @return           Key-value {@link Map} result of object delete operation
     * @throws Exception If any error occurs in deleting object from external system
     */
    public static Map<String, String> deleteObject(String objType, String objId) throws Exception
    {
        if(objType==null || objType.isEmpty()) return null;
        Connection conn = MariadbGateway.getInstance().getConn();
        Statement stmt = conn.createStatement();
        StringBuilder query = new StringBuilder("delete from ");
     
        query.append(objType);
        
        if(objId!=null && !objId.isEmpty()){
            query.append(" where ");
            for(String clause : objId.split("&")){
                String[] s = clause.split("=");
                query.append(s[0]);
                query.append("='");
                query.append(s[1]);
                query.append("' and ");
            }
            query.setLength(query.length()-4);
            
        }
        Map<String, String> result = new HashMap<String, String>();
        ResultSet rs = stmt.executeQuery(query.toString());
        
        while (rs.next()) {
            ResultSetMetaData resultsetmetadata = rs.getMetaData();
            
            for (int i = 1; i <= resultsetmetadata.getColumnCount(); i++)
            {
                String name = resultsetmetadata.getColumnName(i);
                String value = rs.getString(i);
                result.put(name, value);
            }
        }
        
        return result;
    }
    
    public static void main(String[] args) throws Exception
    {
        ArrayList<String> attribs = new ArrayList<String>();
        attribs.add("name");
        attribs.add("description");
        HashMap<String, String> updParams = new HashMap<String, String>();
        updParams.put("name", "change");
        updParams.put("description", "new");
        System.out.println("create object:" + createObject( "alarm", updParams).get("query"));
    
    }
}
