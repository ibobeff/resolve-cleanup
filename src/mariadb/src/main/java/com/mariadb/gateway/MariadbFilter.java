package com.mariadb.gateway;

import com.resolve.gateway.BaseFilter;
import com.resolve.util.Log;

public class MariadbFilter extends BaseFilter {

    public static final String QUERY = "QUERY";

    private String query;

    public MariadbFilter(String id, String active, String order, String interval, String eventEventId, String runbook, String script, String query) {
        super(id, active, order, interval, eventEventId, runbook, script);
        this.query = query;
    }

    public String getQuery() {
        return this.query;
    }

    public void setQuery(String query) {
        this.query = query;
    }
}

