package com.mariadb.gateway.test;

import java.util.HashMap;
import java.util.Map;

import com.resolve.gateway.Filter;
import com.mariadb.gateway.MariadbAPI;

public class FilterTest
{
   
    public static void prepareTestData(){
        String objType = new String("filter_test");
        HashMap<String, String> objParams = new HashMap<String, String>();
        objParams.put("name", "newName");
        objParams.put("description", "say something about new created alarm");
        try
        {
            MariadbAPI.createObject(objType, objParams);
        }
        catch (Exception e)
        {
            System.out.println(e.getMessage());
        }
    }
    
    public static String testFilter(Filter filter, Map<String, String> result) {
        StringBuilder sb = new StringBuilder("");
        if(filter.getId().equals("test")){
            if(result.get("NAME")!=null && result.get("NAME").equals("newName")){
                sb.append("Filter test get expected result: its name field equal newName\n");
            }
            else{
                sb.append("Fail - Filter test's name should be nweName\n");
            }
        }
        return sb.toString();
    }
    
    public static void clearTestData(){
        String objType = new String("filter_test");
        try
        {
            MariadbAPI.deleteObject(objType, "");
        }
        catch (Exception e)
        {
            System.out.println(e.getMessage());
        }
    }
}
