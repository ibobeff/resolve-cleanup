package com.resolve.services.hibernate.vo;

import com.resolve.util.MappingAnnotation;

public class TemplateFilterVO extends GatewayFilterVO
{

    private static final long serialVersionUID = 1L;
    
    
    public TemplateFilterVO()
    {
    } // TemplateFilterVO
    
    @Override
    public boolean equals(Object obj)
    {
        if (this == obj) return true;
        if (!super.equals(obj)) return false;
        if (getClass() != obj.getClass()) return false;
        TemplateFilterVO other = (TemplateFilterVO) obj;
        
        /*
         * Implement equals for desired fields.
         * 
         * String field type (field name is field1) template
         * 
         * if (UField1 == null)
         * {
         *     if (StringUtils.isNotBlank(other.UField1)) return false;
         * }
         * else if (!UField1.trim().equals(other.UField == null ? "" : other.UField.trim())) return false;
         * 
         * Non String field type (field name is field1) template
         * 
         * if (UField1 == null)
         * {
         *     if (other.UField1 != null) return false;
         * }
         * else if (!UField1.equals(other.UField1)) return false;
         */
        
        return true;
    }
}

