package com.resolve.persistence.model;

import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Lob;
import javax.persistence.Table;
import javax.persistence.UniqueConstraint;

import com.resolve.services.interfaces.VO;
import com.resolve.services.hibernate.vo.TemplateFilterVO;

@Entity
@Table(name = "template_filter", uniqueConstraints = { @UniqueConstraint(columnNames = { "u_name", "u_queue" }) })
public class TemplateFilter extends GatewayFilter<TemplateFilterVO>
{
    private static final long serialVersionUID = 1L;
    
    // object referenced by
    public TemplateFilter()
    {
    } // TemplateFilter

    public TemplateFilter(TemplateFilterVO vo)
    {
        applyVOToModel(vo);
    } // TemplateFilter

    public TemplateFilterVO doGetVO()
    {
        TemplateFilterVO vo = new TemplateFilterVO();
        super.doGetBaseVO(vo);
    }

    @Override
    public void applyVOToModel(TemplateFilterVO vo)
    {
        if (vo == null) return;
        super.applyVOToModel(vo);
    }

	@Override
    public List<String> ugetCdataProperty()
    {
        List<String> list = super.ugetCdataProperty();

    }//ugetCdataProperty
}


