package com.resolve.rscontrol;

import com.resolve.persistence.model.TemplateFilter;

public class MTemplate extends MGateway
{
    private static final String MODEL_NAME = TemplateFilter.class.getSimpleName();

    protected String getModelName()
    {
        return MODEL_NAME;
    }
}


