package com.resolve.gateway;

import java.util.Map;

import templatePackage.TemplateFilter;
import templatePackage.TemplateGateway;

public class MTemplate extends MGateway
{
    private static final String RSCONTROL_SET_FILTERS = "MTemplate.setFilters";

    private static final TemplateGateway instance = TemplateGateway.getInstance();

    public MTemplate()
    {
        super.instance = instance;
    }
    
    protected String getSetFilterMethodName()
    {
        return RSCONTROL_SET_FILTERS;
    }

    /**
     * Populates the {@link Filter} data into the given filterMap {@link Map} instance
     * 
     * @param filterMap
     *      the {@link Map} instance to hold the {@link Filter} data
     * @param filter
     *      a {@link TemplateFilter} instance
     *            
     */
    
    public void populateGatewaySpecificFilterProperties(Map<String, String> filterMap, Filter filter)
    {
        if (instance.isActive())
        {
            TemplateFilter templateFilter = (TemplateFilter) filter;
        }
    } // populateGatewaySpecificProperties
}

