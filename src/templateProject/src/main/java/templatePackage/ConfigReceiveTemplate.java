package templatePackage;

import java.io.File;
import java.util.HashMap;
import java.util.Map;

import org.apache.commons.io.FileUtils;

import com.resolve.rsbase.MainBase;
import com.resolve.gateway.Filter;
import com.resolve.rsremote.ConfigReceiveGateway;
import com.resolve.util.Log;
import com.resolve.util.ObjectProperties;
import com.resolve.util.StringUtils;
import com.resolve.util.XDoc;

public class ConfigReceiveTemplate  extends ConfigReceiveGateway
{

    private static final long serialVersionUID = 1L;

    private static final String RECEIVE_TEMPLATE_NODE = "./RECEIVE/TEMPLATE/";
    private static final String RECEIVE_TEMPLATE_FILTER = RECEIVE_TEMPLATE_NODE + "FILTER";

	private String queue = "TEMPLATE";
	
    public String getQueue()
    {
        return queue;
    }

    public void setQueue(String queue)
    {
        this.queue = queue;
    }
	
	public ConfigReceiveTemplate(XDoc config) throws Exception
    {
        super(config);

    }
	
    @Override
    public String getRootNode()
    {
        return RECEIVE_TEMPLATE_NODE;
    }

    @Override
    public void load() throws Exception
    {
        try
        {
            loadAttributes();
            
            if (isActive()) // no need to unnecessarily load anything if the gateway
                            // is inactive.
            {
                TemplateGateway templateGateway = TemplateGateway.getInstance(this);

                // filters
                filters = xdoc.getListMapValue(RECEIVE_TEMPLATE_FILTER);

                // initialize filters
                if (filters.size() > 0)
                {
                    for (Map<String, Object> values : filters)
                    {
                        //it's important to make it lowercase because we store the files in lowercase
                        //without lowering it, linux systems have problem of not reading the script content.
                        String filterName = ((String)values.get(TemplateFilter.ID)).toLowerCase();
                        File scriptFile = getFile(MainBase.main.release.serviceName + "/config/template/" + filterName + ".groovy");

                        try
                        {
                            if (scriptFile.exists())
                            {
                                String groovy = FileUtils.readFileToString(scriptFile, "UTF-8");
                                if (StringUtils.isNotBlank(groovy))
                                {
                                    values.put(TemplateFilter.SCRIPT, groovy);
                                    Log.log.debug("Loaded groovy script: " + scriptFile);
                                }
                            }

                            // init filter
                            templateGateway.setFilter(values);
                        }
                        catch (Exception e)
                        {
                            Log.log.warn(e.getMessage() + ". Skipping file: " + scriptFile.getAbsolutePath());
                        }
                    }
                }
            }
        }
        catch (Throwable e)
        {
            //deactivate it
            setActive(false);
            Log.log.warn("Couldn not load configuration for Template gateway, check blueprint. " + e.getMessage(), e);
        }
        
    }

    @Override
    public void save() throws Exception
    {

        // create template directory
        File dir = getFile(MainBase.main.release.serviceName + "/config/template");
        if (!dir.exists())
        {
            FileUtils.forceMkdir(dir);
        }
        
        // clear list of filters to save
        if (filters != null && isActive())
        {
            filters.clear();

            // save sql files
            for (Filter filter : TemplateGateway.getInstance().getFilters().values())
            {
                TemplateFilter templateFilter = (TemplateFilter) filter;

                String groovy = templateFilter.getScript();
                
                String scriptFileName;
                File scriptFile = null;
                
                if (StringUtils.isNotBlank(groovy))
                {
                    scriptFileName = templateFilter.getId().toLowerCase() + ".groovy";
                    scriptFile = getFile(MainBase.main.release.serviceName + "/config/template/" + scriptFileName);
                }
                
                // add xdoc entry
                Map<String, Object> entry = new HashMap<String, Object>();

				putFilterDataIntoEntry(entry, templateFilter);
                
                filters.add(entry);

                if (scriptFile != null)
                {
                    try
                    {
                        // create groovy file
                        FileUtils.writeStringToFile(scriptFile, groovy, "UTF-8");
                    }
                    catch (Exception e)
                    {
                        Log.log.warn(e.getMessage() + ". Skipping file: " + scriptFile.getAbsolutePath());
                    }
                }
            }

            // filters
            xdoc.setListMapValue(RECEIVE_TEMPLATE_FILTER, filters);
        }

        // clear list of properties to save
        if (nameProperties != null && isActive())
        {
            // empty properties
            nameProperties.clear();

            // recreate properties
            for (String name : TemplateGateway.getInstance().getNameProperties().keySet())
            {
                // add xdoc entry
                Map<String, Object> entry = new HashMap<String, Object>();
                entry.put(NAME_KEY, name);
                nameProperties.add(entry);

                Map props = TemplateGateway.getInstance().getNameProperties().getNameProperties(name);
                ObjectProperties prop = new ObjectProperties(props);
                prop.save(props);
            }
        }
        saveAttributes();
    }

	private void putFilterDataIntoEntry(Map<String, Object> entry, TemplateFilter templateFilter)
    {
        entry.put(TemplateFilter.ID, templateFilter.getId());
        entry.put(TemplateFilter.ACTIVE, String.valueOf(templateFilter.isActive()));
        entry.put(TemplateFilter.ORDER, String.valueOf(templateFilter.getOrder()));
        entry.put(TemplateFilter.INTERVAL, String.valueOf(templateFilter.getInterval()));
        entry.put(TemplateFilter.EVENT_EVENTID, templateFilter.getEventEventId());
        entry.put(TemplateFilter.RUNBOOK, templateFilter.getRunbook());
        entry.put(TemplateFilter.SCRIPT, templateFilter.getScript());
    }
	

}

