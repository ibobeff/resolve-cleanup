package templatePackage;


import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;
import java.util.List;
import java.util.ArrayList;

import com.resolve.rsbase.MainBase;
import com.resolve.gateway.BaseClusteredGateway;
import com.resolve.gateway.BaseFilter;
import com.resolve.gateway.Filter;
import com.resolve.gateway.MTemplate;
import com.resolve.util.Log;
import com.resolve.rsremote.ConfigReceiveGateway;

public class TemplateGateway extends BaseClusteredGateway
{
    // Singleton
    private static volatile TemplateGateway instance = null;
    private String queue = null;

    public static TemplateGateway getInstance(ConfigReceiveTemplate config)
    {
        if (instance == null)
        {
            instance = new TemplateGateway(config);
        }
        return instance;
    }
    
    /**
     * This method is called once the initialization is done. If it gets called
     * before the initialization it will throw exception.
     *
     * @return
     */
    public static TemplateGateway getInstance()
    {
        if (instance == null)
        {
            throw new RuntimeException("Template Gateway is not initialized correctly..");
        }
        else
        {
            return instance;
        }
    }

    private TemplateGateway(ConfigReceiveTemplate config)
    {
        // Important, call super here.
        super(config);
    }
    
	@Override
    public String getLicenseCode()
    {
        return "TEMPLATE";
    }

    @Override
    protected void addAdditionalValues(Map<String, Object> arg0, Map<String, String> arg1)
    {
        // TODO Auto-generated method stub
        
    }

    @Override
    protected String getGatewayEventType()
    {
        return "TEMPLATE";
    }

    @Override
    protected String getMessageHandlerName()
    {
        return MTemplate.class.getSimpleName();
    }

    @Override
    protected Class<MTemplate> getMessageHandlerClass()
    {
        return MTemplate.class;
    }

    public String getQueueName()
    {
        return queue + getOrgSuffix();
    }
    
    @Override
    public void start()
    {
        Log.log.debug("Starting Template Gateway");
        super.start();
    } // start

    @Override
    protected void cleanFilter(ConcurrentHashMap<String, Filter> deletedFilters)
    {
        /*
         * Passed in originalFilters map contains deleted filters.
         * Gateway implementation should perform necessary cleanup
         * required for deleted filters.
         */
    }
    
    @Override
    public void run()
    {
        /* This call is must for all the ClusteredGateways.
         * For testing filter(s) deploying through config file only 
         * enable the MOCK mode.
         */
        
        if (!((ConfigReceiveGateway)configurations).isMock())
            super.sendSyncRequest();

        
        while (running)
        {
            try
            {
                long startTime = System.currentTimeMillis();

                /* if any remaining events are in primary data queue or 
                 * primary data executor has waiting messages still to be processed, 
                 * do not get messages for more new events.
                 */
				 
                if (primaryDataQueue.isEmpty() && !primaryDataQueueExecutorBusy())
                {
                    Log.log.trace(getQueueName() + " Primary Data Queue is empty and Primary Data Queue Executor is free.....");
					
                    for (Filter filter : orderedFilters)
                    {
                        if (shouldFilterExecute(filter, startTime))
                        {
							Log.log.trace("Filter " + filter.getId() + " executing");
							List<Map<String, String>> results = invokeService(filter);
                            for (Map<String, String> result : results)
                            {
								addToPrimaryDataQueue(filter, result);
							}
                        }
                        else
                        {
                            Log.log.trace("Filter:" + filter.getId() + " is inactive or not ready to execute yet.....");
                        }
                    }
					if(orderedFilters==null || orderedFilters.size()==0){
                        Log.log.trace("There is not deployed filter for TemplateGateway");
                    }
                }

                // If there is no filter yet no need to keep spinning, wait for
                // a while
                if (orderedFilters.size() == 0)
                {
                    Thread.sleep(interval);
                }
            }
            catch (Exception e)
            {
                Log.log.warn("Warning: " + e.getMessage());
                Log.alert("Instance " + MainBase.main.configId.getGuid() + " " + getLicenseCode() + "-" + getQueueName() + "-" + getInstanceType() + " Polling Failed", 
                          "Instance " + MainBase.main.configId.getGuid() + " " + getLicenseCode() + "-" + getQueueName() + "-" + getInstanceType() + " Polling Failed " + 
                          "due to " + e.getMessage());
                
                try
                {
                    Thread.sleep(interval);
                }
                catch (InterruptedException ie)
                {
                    Log.log.warn("sleep interrupted: " + ie.getMessage(), ie);
                }
            }
        }
    }
	
	 /**
     * Add customized code here to get the data as List<Map<String, String>> format for each filter 
     * @return List<Map<String, String>>
     */    
	public List<Map<String, String>> invokeService(Filter filter){
        List<Map<String, String>> result = new ArrayList<Map<String, String>>();

	}
	
	/**
     * Add customized code here to initialize connection with 3rd party system
     * @return List<Map<String, String>>
     */ 
	@Override
    protected void initialize()
    {
        ConfigReceiveTemplate config = (ConfigReceiveTemplate) configurations;
        queue = config.getQueue().toUpperCase();
        this.gatewayConfigDir = "/config/template/";

    }
	
	/**
     * Add customized code here to do when stop the gateway
     */ 
	@Override
    public void stop()
    {
        Log.log.warn("Stopping Template gateway");
    }
	
    @Override
    public Filter getFilter(Map<String, Object> params)
    {
        return new TemplateFilter((String) params.get(BaseFilter.ID), (String) params.get(BaseFilter.ACTIVE), (String) params.get(BaseFilter.ORDER), (String) params.get(BaseFilter.INTERVAL), 
                        (String) params.get(BaseFilter.EVENT_EVENTID), (String) params.get(BaseFilter.RUNBOOK), (String) params.get(BaseFilter.SCRIPT));
    }

}

