package templatePackage;

import java.util.HashMap;
import java.util.Map;
import java.util.List;
import java.util.concurrent.ConcurrentHashMap;

import com.resolve.gateway.BaseClusteredGateway;
import com.resolve.gateway.BaseFilter;
import com.resolve.gateway.Filter;
import com.resolve.gateway.MTemplate;
import com.resolve.util.Constants;
import com.resolve.util.Log;
import com.resolve.util.StringUtils;

public class TemplateGateway extends BaseClusteredGateway
{
    // Singleton
    private static volatile TemplateGateway instance = null;
    private String queue = null;

    public static TemplateGateway getInstance(ConfigReceiveTemplate config)
    {
        if (instance == null)
        {
            instance = new TemplateGateway(config);
        }
        return instance;
    }
    
    /**
     * This method is called once the initialization is done. If it gets called
     * before the initialization it will throw exception.
     *
     * @return
     */
    public static TemplateGateway getInstance()
    {
        if (instance == null)
        {
            throw new RuntimeException("Template Gateway is not initialized correctly..");
        }
        else
        {
            return instance;
        }
    }

    private TemplateGateway(ConfigReceiveTemplate config)
    {
        // Important, call super here.
        super(config);
    }
    
	@Override
    public String getLicenseCode()
    {
        return "TEMPLATE";
    }

    @Override
    protected void addAdditionalValues(Map<String, Object> arg0, Map<String, String> arg1)
    {
        // TODO Auto-generated method stub
        
    }

    @Override
    protected String getGatewayEventType()
    {
        return "TEMPLATE";
    }

    @Override
    protected String getMessageHandlerName()
    {
        return MTemplate.class.getSimpleName();
    }

    @Override
    protected Class<MTemplate> getMessageHandlerClass()
    {
        return MTemplate.class;
    }

    public String getQueueName()
    {
        return queue + getOrgSuffix();
    }
    
    @Override
    public void start()
    {
        Log.log.debug("Starting Template Gateway");
        super.start();
    } // start

    @Override
    public void run()
    {
        //This call is must for all the ClusteredGateways.
        super.sendSyncRequest();
        if(isPrimary() && isActive())
        {
            initTemplateServers();
        }
    }

	@Override
        protected void initialize()
    {
        ConfigReceiveTemplate config = (ConfigReceiveTemplate) configurations;
        queue = config.getQueue().toUpperCase();

        try
        {
            Log.log.info("Initializing Template Listener");

            this.gatewayConfigDir = "/config/template/";
            
        }
        catch (Exception e)
        {
            Log.log.error("Failed to config Template Gateway: " + e.getMessage(), e);
        }
    }
  
    /**
     * This method processes the message received from the Template system.
     *
     * @param message
     */
    public boolean processData(String filterName, Map<String, String> params)
    {
        boolean result = true;
        try
        {
            if(StringUtils.isNotBlank(filterName) && params != null)
            {
                TemplateFilter templateFilter = (TemplateFilter) filters.get(filterName);
                if (templateFilter != null && templateFilter.isActive())
                {
                    if(Log.log.isDebugEnabled())
                    {
                        Log.log.debug("Processing filter: " + templateFilter.getId());
                        Log.log.debug("Data received through Template gateway: " + params);
                    }
                    Map<String, String> runbookParams = params;
                    if(!params.containsKey(Constants.EVENT_EVENTID) || StringUtils.isBlank(params.get(Constants.EVENT_EVENTID)))
                    {
                        runbookParams.put(Constants.EVENT_EVENTID, templateFilter.getEventEventId());
                    }

                    addToPrimaryDataQueue(templateFilter, runbookParams);
                }
                else
                {
                    result = false;
                }
            }
        }
        catch (Exception e)
        {
            Log.log.warn("Error: " + e.getMessage(), e);
            try
            {
                Thread.sleep(interval);
            }
            catch (InterruptedException ie)
            {
                Log.log.warn("sleep interrupted: " + ie.getMessage(), ie);
            }
        }
        return result;
    }    
    
    @Override
    public void clearAndSetFilters(List<Map<String, Object>> filterList)
    {
		Log.log.debug("Calling Gateway specific clearAndSetFilter" + (filterList != null ? " " + filterList.size() + ", [" + filterList + "]": ""));
        super.clearAndSetFilters(filterList);
        if(isPrimary() && isActive())
        {
            try
            {
                initTemplateServers();
            }
            catch (Exception e)
            {
                Log.log.error(e.getMessage(), e);
            } 
        }
        
    } 
            
    @Override
    protected void cleanFilter(ConcurrentHashMap<String, Filter> deletedFilters)
    {
        /*
         * Passed in originalFilters map contains deleted filters.
         * Gateway implementation should perform necessary cleanup
         * required for deleted filters.
         */
    }    
    
	private void initTemplateServers()
    {
    
    }
	
    @Override
    public void stop()
    {
        Log.log.warn("Stopping Template gateway");

    }
	
	@Override
    public Filter getFilter(Map<String, Object> params)
    {
        return new TemplateFilter((String) params.get(BaseFilter.ID), (String) params.get(BaseFilter.ACTIVE), (String) params.get(BaseFilter.ORDER), (String) params.get(BaseFilter.INTERVAL), 
                        (String) params.get(BaseFilter.EVENT_EVENTID), (String) params.get(BaseFilter.RUNBOOK), (String) params.get(BaseFilter.SCRIPT));
    }
}

