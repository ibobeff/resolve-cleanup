/******************************************************************************
* (C) Copyright 2014
*
* Resolve Systems, LLC
*
* All rights reserved
* This software is distributed under license and may not be used, copied,
* modified, or distributed without the express written permission of
* Resolve Systems, LLC.
*
******************************************************************************/

package com.resolve.gateway;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.resolve.rsbase.MainBase;
import com.resolve.gateway.hpsm.HPSMConstants;
import com.resolve.gateway.hpsm.HPSMGateway;
import com.resolve.util.StringUtils;

/**
 * This class provides a convenient way to interact with the Hewlett Packard
 * Operations Manager (HPSM) system.
 *
 */
public class HPSMAPI
{
    private static HPSMGateway instance = HPSMGateway.getInstance();

    /**
     * Returns the {@link Object} value for the given key in the current
     * HPSMGateway instance's NameProperty map.
     * 
     * @param name
     *            the name of the property {@link Map} in the
     *            HPSMGateway instance's {@link NameProperties} object
     * @param key
     *            the key associated with the expected value in the property
     *            {@link Map} returned from the HPSMGateway's
     *            {@link NameProperties} object
     * @return {@link Object} value associated with the key in the property
     *         {@link Map} returned by the HPSMGateway instance's
     *         {@link NameProperties} object
     */
    public static Object getNameObject(String name, String key)
    {
        return instance.getNameProperties().getNameObject(name, key);
    }

    /**
     * Uses the given name parameter to return a property {@link Map} within the
     * HPSMGateway instance's {@link NameProperties} object. If the
     * returned {@link Map} isn't null, it adds the given {@link String} key and
     * {@link Object} value to it.
     * 
     * @param name
     *            the name of the {@link Map} in the HPSMGateway
     *            instance's {@link NameProperties} object. Cannot be null or
     *            empty.
     * @param key
     *            the key in the {@link Map} returned from the
     *            {@link NameProperties} object in the HPSMGateway.
     *            Cannot be null or empty.
     * @param value
     *            the value associated with the given key in the {@link Map}
     *            returned from the {@link NameProperties} object in the
     *            HPSMGateway
     * 
     * @throws Exception
     */
    public static void setNameObject(String name, String key, Object value) throws Exception
    {
        Map nameProperties = getNameProperties(name);
        if(nameProperties != null)
        {
            nameProperties.put(key, value);
            setNameProperties(name, nameProperties, true);
        }
    }
    
    /**
     * Uses the given {@link String} name as a key to return a {@link Map} of
     * properties from the HPSMGateway instance's {@link NameProperties}
     * object.
     *
     * @param name
     *            The name associated with the properties {@link Map} in the
     *            HPSMGateway's {@link NameProperties} object. Cannot be
     *            null or empty.
     * @return a Map of HPSMGateway name properties
     * @throws Exception
     */
    public static Map getNameProperties(String name) throws Exception
    {
        return instance.getNameProperties().getNameProperties(name);
    }

    /**
     * Associates the given name to the given properties {@link Map} in the
     * HPSMGateway instance's {@link NameProperties} object.
     *
     * @param name
     *            the key in the HPSMGateway's {@link NameProperties}
     *            object. Cannot be null or empty.
     * @param properties
     *            A Map of properties to associate with the given name parameter
     * @throws Exception
     */
    public static void setNameProperties(String name, Map properties) throws Exception
    {
        setNameProperties(name, properties, true);
    }

    /**
     * Associates the given name to the given properties Map in the HPSMGateway
     * instance's NameProperties object.
     *
     * @param name
     *            the key in the HPSMGateway's {@link NameProperties}
     *            object. Cannot be null or empty.
     * @param properties
     *            A {@link Map} of properties to associate with the given name
     *            parameter
     * @param doSave
     *            a boolean value that designates whether to send this change to
     *            RSControl for persistence
     * @throws Exception
     */
    public static void setNameProperties(String name, Map properties, boolean doSave) throws Exception
    {
        if (StringUtils.isBlank(name))
        {
            throw new Exception("'name' parameter must be passed");
        }
        else
        {
            instance.setNameProperties(name, properties, doSave);
        }
    }

    /**
     * Removes a {@link Map} of properties associated with the given name in the
     * HPSMGateway instance's {@link NameProperties} object.
     *
     * @param name
     *            The name associated with a property {@link Map} in the
     *            HPSMGateway instance's {@link NameProperties} object.
     *            Cannot be null or empty.
     * @throws Exception 
     */
    public static void removeNameProperties(String name) throws Exception
    {
        if (StringUtils.isBlank(name))
        {
            throw new Exception("'name' parameter must be passed");
        }
        else
        {
            instance.removeNameProperties(name);
        }
    }

    /**
     * Saves the configurations in the RSRemote.
     *
     * @throws Exception
     */
    public static void saveConfig() throws Exception
    {
        MainBase.main.callSaveConfig();
    }

    /**
     * Creates an Incident. Keep in mind that there are several mandatory fields needs to be provided.
     * Value of some important fields (e.g., Urgency, Impact etc.) must match the system defined
     * values. Make sure that you have those information beforehand. If available then this method
     * will pass the error it receives from the server for troubleshooting.
     *
     * <pre>
     * {@code
     * try
     * {
     *      String username = "falcon"; //if null then uses gateway configured value.
     *      String password = null; //if null then uses gateway configured value.
     *
     *      //following are the minimum fields required in the test server.
     *      HPSMIncident incident = new HPSMIncident();
     *      //check the system for valid value.
     *      incident.setService("E-mail / Webmail (North America)");
     *      //check the system for valid value.
     *      incident.setImpact("2");
     *      //check the system for valid value.
     *      incident.setUrgency("2");
     *      //check the system for valid value.
     *      incident.setAssignmentGroup("Application");
     *      //HPSM incident allows adding multiple description
     *      List<String> description = new ArrayList<String>();
     *      description.add("Some description");
     *      incident.setDescription(description);
     *
     *      incident.setTitle("Some title");
     *      //the area has to be a valid one, check the HPSM create incident scree for valid values.
     *      //there is most likely configuration in the system to add remove the areas so we cannot
     *      //provide a list of some fix values
     *      incident.setArea("access");
     *      //same like "area" above, please check the system for valid value
     *      incident.setSubarea("authorization error");
     *
     *      HPSMIncident response = HPSMAPI.createIncident(incident, username, password);
     *      System.out.println("New incident Id:" + response.getIncidentID());
     * }
     * catch (Exception ex)
     * {
     *      //Do something good with this error.
     *      ex.printStackTrace();
     * }
     * }
     * </pre>
     *
     * @param incident is an instance of {@link HPSMIncident} object. The properties of the {@link HPSMIncident} object
     * are similar to the types you'll find in the WSDL file.
     * @param username
     *            if not provided uses the gateway configuration.
     * @param password
     *            ties with the username.
     * @return an the {@link HPSMIncident} instance with system provided information (e.g., incident id, status etc.)
     * @throws Exception
     */
    /**
     * @param incident
     * @param username
     * @param password
     * @return
     * @throws Exception
     */
    public static HPSMIncident createIncident(HPSMIncident incident, String username, String password) throws Exception
    {
        return instance.createIncident(incident, username, password);
    }

    /**
     * Creates an Incident by taking a {@link Map} as the parameter. Keep in mind that there are several mandatory fields
     * needs to be provided. Value of some important fields (e.g., Urgency, Impact etc.) must match the system defined
     * values. Make sure that you have those information beforehand. If available then this method
     * will pass the error it receives from the server for troubleshooting.
     *
     * <pre>
     * {@code
     * try
     * {
     *      String username = "falcon"; // if null then uses gateway configured value.
     *      String password = null; // if null then uses gateway configured value.
     *
     *      Map<String, Object> params = new HashMap<String, Object>();
     *      params.put(HPSMConstants.INCIDENT_SERVICE, "E-mail / Webmail (North America)");
     *      // check the system for valid value.
     *      params.put(HPSMConstants.INCIDENT_IMPACT, "2");
     *      // check the system for valid value.
     *      params.put(HPSMConstants.INCIDENT_URGENCY, "2");
     *      // check the system for valid value.
     *      params.put(HPSMConstants.INCIDENT_ASSIGNMENTGROUP, "Application");
     *      // HPSM incident allows adding multiple description
     *      List<String> description = new ArrayList<String>();
     *      description.add("Some description");
     *      params.put(HPSMConstants.INCIDENT_DESCRIPTION, description);
     *
     *      params.put(HPSMConstants.INCIDENT_TITLE, "Some title");
     *      // the area has to be a valid one, check the HPSM create incident scree for valid values.
     *      // there is most likely configuration in the system to add remove the areas so we cannot
     *      // provide a list of some fix values
     *      params.put(HPSMConstants.INCIDENT_AREA, "access");
     *      // same like "area" above, please check the system for valid value
     *      params.put(HPSMConstants.INCIDENT_SUBAREA, "authorization error");
     *
     *      HPSMIncident response = HPSMAPI.createIncident(params, username, password);
     *      System.out.println("New incident Id:" + response.getIncidentID());
     * }
     * catch (Exception ex)
     * {
     *      //Do something good with this error.
     *      ex.printStackTrace();
     * }
     * }
     * </pre>
     *
     * @param params contains the value for the incident. Check the {@link HPSMConstants} for field keys. The keys are
     * are similar to the types you'll find in the WSDL file.
     * @param username
     *            if not provided uses the gateway configuration.
     * @param password
     *            ties with the username.
     * @return a {@link Map} with incident's properties as the key and their values (e.g., incident id, status etc.)
     * @throws Exception
     */
    public static Map<String, Object> createIncident(Map<String, Object> params, String username, String password) throws Exception
    {
        
        Map<String, Object> result = new HashMap<String, Object>();

        HPSMIncident incident = instance.createIncident(params, username, password);
        if(incident != null)
        {
            result = incident.asMap();
        }
        return result;
    }

    /**
     * Updates an Incident. Value of some important fields (e.g., Urgency, Impact etc.) must match the system defined
     * values. Make sure that you have those information beforehand. If available then this method
     * will pass the error it receives from the server for troubleshooting.
     *
     * <pre>
     * {@code
     * try
     * {
     *      String username = "falcon"; //if null then uses gateway configured value.
     *      String password = null; //if null then uses gateway configured value.
     *
     *      String incidentId = "IM10170"; //get this Id probably from a create incident call.
     *
     *      HPSMIncident incident = new HPSMIncident();
     *      incident.setIncidentID(incidentId);
     *      //in this case we'll just update the status. But you could update other fields too.
     *      //for status or any other fields make sure the values are valid. In some cases
     *      //values are also case sensitive, so carefull.
     *      incident.setStatus("Accepted");
     *
     *      HPSMAPI.updateIncident(incident, username, password);
     *      System.out.println("Updated successfully");
     * }
     * catch (Exception ex)
     * {
     *      //Do something good with this error.
     *      ex.printStackTrace();
     * }
     * }
     * </pre>
     *
     * @param incident is an instance of {@link HPSMIncident} object. The properties of the {@link HPSMIncident} object
     * are similar to the types you'll find in the WSDL file. This instance must have the incidentId.
     * @param username
     *            if not provided uses the gateway configuration.
     * @param password
     *            ties with the username.
     * @throws Exception
     */
    public static void updateIncident(HPSMIncident incident, String username, String password) throws Exception
    {
        instance.updateIncident(incident, username, password);
    }

    /**
     * Updates an Incident. Value of some important fields (e.g., Urgency, Impact etc.) must match the system defined
     * values. Make sure that you have those information beforehand. If available then this method
     * will pass the error it receives from the server for troubleshooting.
     *
     * <pre>
     * {@code
     * try
     * {
     *      String username = "falcon"; //if null then uses gateway configured value.
     *      String password = null; //if null then uses gateway configured value.
     *
     *      String incidentId = "IM10170"; //get this Id probably from a create incident call.
     *
     *      params.put(HPSMConstants.INCIDENT_ID, incidentId);
     *      // in this case we'll just update the status. But you could update other fields too.
     *      // for status or any other fields make sure the values are valid. In some cases
     *      // values are also case sensitive, so carefull.
     *      params.put(HPSMConstants.INCIDENT_STATUS, "Accepted");
     *
     *      HPSMAPI.updateIncident(params, username, password);
     *      System.out.println("Updated successfully");
     *
     * }
     * catch (Exception ex)
     * {
     *      //Do something good with this error.
     *      ex.printStackTrace();
     * }
     * }
     * </pre>
     *
     * @param params
     *            incident is an instance of {@link HPSMIncident} object. The properties of the {@link HPSMIncident} object
     * are similar to the types you'll find in the WSDL file. This instance must have the IncidentId.
     * @param username
     *            if not provided uses the gateway configuration.
     * @param password
     *            ties with the username.
     * @throws Exception
     */
    public static void updateIncident(Map<String, Object> params, String username, String password) throws Exception
    {
        
       
        instance.updateIncident(params, username, password);
    }

    /**
     * Resolves an Incident. When an incident is resolved, it's status becomes resolved.
     *
     * <pre>
     * {@code
     * try
     * {
     *      String username = "falcon"; //if null then uses gateway configured value.
     *      String password = null; //if null then uses gateway configured value.
     *
     *      Map<String, Object> params = new HashMap<String, Object>();
     *
     *      String incidentId = "IM10170"; // get this Id probably from a create incident call.
     *      params.put(HPSMConstants.INCIDENT_ID, incidentId);
     *      //add the closureCode, make sure the code is valid
     *      params.put(HPSMConstants.INCIDENT_CLOSURECODE, "ClosureCode1");
     *
     *      HPSMAPI.resolveIncident(params, username, password);
     *      System.out.println("Incident resolved successfully");
     * }
     * catch (Exception ex)
     * {
     *      //Do something good with this error.
     *      ex.printStackTrace();
     * }
     * }
     * </pre>
     *
     * @param params
     *            must contain "IncidentId" of the incident to be resolved
     * @param username
     *            if not provided uses the gateway configuration.
     * @param password
     *            ties with the username.
     * @throws Exception
     */
    public static void resolveIncident(Map<String, Object> params, String username, String password) throws Exception
    {
        if(params == null || StringUtils.isBlank((String)params.get(HPSMConstants.INCIDENT_ID)))
        {
            throw new Exception("Incident object with incidentID must be provided.");
        }
        //convert to an HPSMIncident
        HPSMIncident incident = new HPSMIncident(params);
        instance.resolveIncident(incident, username, password);
    }

    /**
     * Resolves an Incident. When an incident is resolved, it's status becomes resolved.
     *
     * <pre>
     * {@code
     * try
     * {
     *      String username = "falcon"; //if null then uses gateway configured value.
     *      String password = null; //if null then uses gateway configured value.
     *
     *      String incidentId = "IM10170"; //get this Id from probably from a create incident call.
     *
     *      HPSMIncident incident = new HPSMIncident();
     *      incident.setIncidentID(incidentId);
     *      //add the closureCode, make sure the code is valid
     *      incident.setClosureCode("ClosureCode1");
     *
     *      HPSMAPI.resolveIncident(incident, username, password);
     *      System.out.println("Incident resolved successfully");
     * }
     * catch (Exception ex)
     * {
     *      //Do something good with this error.
     *      ex.printStackTrace();
     * }
     * }
     * </pre>
     *
     * @param incident to be resolved, at least the incidentID must be provided.
     * @param username
     *            if not provided uses the gateway configuration.
     * @param password
     *            ties with the username.
     * @throws Exception
     */
    public static void resolveIncident(HPSMIncident incident, String username, String password) throws Exception
    {
        if(incident == null || StringUtils.isBlank(incident.getIncidentID()))
        {
            throw new Exception("Incident object with incidentID must be provided.");
        }
        instance.resolveIncident(incident, username, password);
    }

    
    /**
     * 
     * @param incidentId incidentId of the incident to be closed, must be provided.
     * @param closureCode closureCode to close incident. Make sure the code is valid 
      * @param username
     *            if not provided uses the gateway configuration.
     * @param password
     *            ties with the username.
     * @throws Exception
     */
    public static void closeIncident(String incidentId, String closureCode, String username, String password) throws Exception
    {
        instance.closeIncident(incidentId, closureCode, username, password);
    }
    
    /**
     * 
     * @param incident incident to be closed
      * @param username
     *            if not provided uses the gateway configuration.
     * @param password
     *            ties with the username.
     * @throws Exception
     */
    public static void closeIncident(HPSMIncident incident, String username, String password) throws Exception
    {
        instance.closeIncident(incident, username, password);
    }

    /**
     * Retrieves an Incident by its Id.
     *
     * <pre>
     * {@code
     * try
     * {
     *      String username = "falcon"; //if null then uses gateway configured value.
     *      String password = null; //if null then uses gateway configured value.
     *
     *      String incidentId = "IM10170"; //get this Id from probably from a create incident call.
     *
     *      HPSMAPI.resolveIncident(incidentId, username, password);
     *      System.out.println("Incident resolved successfully");
     * }
     * catch (Exception ex)
     * {
     *      //Do something good with this error.
     *      ex.printStackTrace();
     * }
     * }
     * </pre>
     *
     * @param incidentId of the incident to be retrieved, must be provided.
     * @param username
     *            if not provided uses the gateway configuration.
     * @param password
     *            ties with the username.
     * @return an the {@link HPSMIncident} instance with system provided information (e.g., incident id, status, title etc.)
     * @throws Exception
     */
    public static HPSMIncident selectIncidentById(String incidentId, String username, String password) throws Exception
    {
        return instance.selectIncidentById(incidentId, username, password);
    }

    /**
     * Retrieves a {@link List} of {@link HPSMIncident} based on a query.
     *
     * <pre>
     * {@code
     * try
     * {
     *      String username = "falcon"; //if null then uses gateway configured value.
     *      String password = null; //if null then uses gateway configured value.
     *
     *      //These are some of the example queries.
     *      //String query = "OpenTime>='12/14/2011 06:36:03' and Status=\"Open\" and Urgency=2 and Impact=1";
     *      //String query = "OpenTime>='12/14/2011 06:36:03' and Status=\"Open\"";
     *      String query = "OpenTime>='12/14/2011 06:36:03' and Status=\"Open\"";
     *      List<HPSMIncident> incidents = HPSMAPI.searchIncident(query, 10, username, password);
     *
     *      for(HPSMIncident incident : incidents)
     *      {
     *          System.out.printf("====================\n");
     *          System.out.println("Incident: " + incident.getIncidentID());
     *          Map<String, String> map = incident.asMap();
     *          System.out.println(map.toString());
     *      }
     * }
     * catch (Exception ex)
     * {
     *      //Do something good with this error.
     *      ex.printStackTrace();
     * }
     * }
     * </pre>
     *
     * @param query must conform to HPSM's query
     * @param maxResult maximum number of records to be retrieved. Set 0 (zero) for all.
     * @param username
     *            if not provided uses the gateway configuration.
     * @param password
     *            ties with the username.
     * @return a {@link List} of {@link HPSMIncident}.
     * @throws Exception
     */
    public static List<HPSMIncident> searchIncident(String query, int maxResult, String username, String password) throws Exception
    {
        return instance.searchIncident(query, maxResult, username, password);
    }
}
