/******************************************************************************
* (C) Copyright 2014
*
* Resolve Systems, LLC
*
* All rights reserved
* This software is distributed under license and may not be used, copied,
* modified, or distributed without the express written permission of
* Resolve Systems, LLC.
*
******************************************************************************/

package com.resolve.gateway;

import java.util.List;

import com.resolve.gateway.itm.ITMGateway;

public class ITMAPI
{
    /**
     * Gets a list of available agent names from the server.
     * 
     * <pre>
     * {@code
     * try
     * {
     *      String username = "root";
     *      String password = "rootpwd";
     *      List<String> agents = ITMAPI.getAgentList(username, password, "LZ", "NT");
     *      for( String agent : agents)
     *      {
     *          System.out.printf("Agent name: %s\n", agent);
     *      } 
     * }
     * catch (Exception ex)
     * {
     *      //Do something good with this error. 
     *      ex.printStackTrace();
     * }
     * }
     * </pre>
     * 
     * @param username
     *            the username for the host server. If not provided uses the
     *            gateway configuration.
     * @param password
     *            ties with the username.
     * @param agentTypes
     *            is the list of agent types (e.g., LZ, NT etc.), to get the
     *            entire list do not set it.
     * @return if no agent found it will return an empty {@link List} so no need
     *         to check for null.
     * @throws Exception
     */
    public static List<String> getAgentList(String username, String password, String... agentTypes) throws Exception
    {
        ITMGateway itmGateway = new ITMGateway(username, password);
        return itmGateway.getAgentList(agentTypes);
    }

    /**
     * Executes a command in the host server.
     * 
     * <pre>
     * {@code
     * try
     * {
     *      String localCommand = "ls -l";
     *      String username = "root";
     *      String password = "rootpwd";
     *      String commandOutput = ITMAPI.executeLocalCommand(localCommand, username, password);
     *      //Do something useful with the output.
     *      System.out.println(commandOutput); 
     * }
     * catch (Exception ex)
     * {
     *      //Do something good with this error. 
     *      ex.printStackTrace();
     * }
     * }
     * </pre>
     * 
     * @param localCommand
     *            to be executed, must be compliant with the host environment
     *            (e.g., "dir c:\temp" for windows or "ls -al" for *nix).
     * @param username
     *            the username for the host server. If not provided uses the
     *            gateway configuration.
     * @param password
     *            ties with the username.
     * @return the output of the command.
     * @throws Exception
     */
    public static String executeLocalCommand(String localCommand, String username, String password) throws Exception
    {
        ITMGateway itmGateway = new ITMGateway(username, password);
        return itmGateway.executeLocalCommand(localCommand);
    }

    /**
     * Executes a command in an agent machine.
     * 
     * <pre>
     * {@code
     * try
     * {
     *      String agent = "netcool:LZ";
     *      String remoteCommand = "df -a";
     *      String remoteUsername = "remoteuser";
     *      String username = "root";
     *      String password = "rootpwd";
     *      String commandOutput = ITMAPI.executeRemoteCommand(agent, remoteCommand, remoteUsername, username, password);
     *      //Do something useful with the output.
     *      System.out.println(commandOutput); 
     * }
     * catch (Exception ex)
     * {
     *      //Do something good with this error. 
     *      ex.printStackTrace();
     * }
     * }
     * </pre>
     * 
     * @param agent
     *            is the agent machine's name and must be provided, caller may
     *            give full name (e.g., agent:LZ) or host name.
     * @param command
     *            to be executed and must be provided. Also must be compliant
     *            with the agent environment (e.g., "dir c:\temp" for windows or
     *            "ls -al" for *nix).
     * @param remoteUsername
     *            is a username that will execute the command on the agent. It's
     *            for *nix agent only. If provided then the gateway will execute
     *            the command with "su - remoteUsername -c command" on the
     *            agent.
     * @param username
     *            the username for the host server. If not provided uses the
     *            gateway configuration.
     * @param password
     *            ties with the username.
     * @return the output of the command.
     * @throws Exception
     */
    public static String executeRemoteCommand(String agent, String command, String remoteUsername, String username, String password) throws Exception
    {
        ITMGateway itmGateway = new ITMGateway(username, password);
        return itmGateway.executeRemoteCommand(agent, command, remoteUsername);
    }

    /**
     * Executes a script in a remote agent machine.
     * 
     * <pre>
     * {@code
     * try
     * {
     *      String agent = "netcool:LZ";
     *      String scriptFileExt = ".ksh";
     *      
     *      StringBuilder scriptContent = new StringBuilder();
     *      scriptContent.append("#!/bin/ksh\n");
     *      scriptContent.append("ls -l\n");
     *      scriptContent.append("df -a\n");
     *      scriptContent.append("echo \"Script executed successfully!!!\"\n");
     * 
     *      String remoteDir = "/tmp";
     *      String remoteUsername = "remoteuser";
     *      String username = "root";
     *      String password = "rootpwd";
     *      String commandOutput = ITMAPI.executeRemoteCommand(agent, null, scriptFileExt, scriptContent.toString(), remoteDir, remoteUsername, username, password);
     *      //Do something useful with the output.
     *      System.out.println(commandOutput); 
     * }
     * catch (Exception ex)
     * {
     *      //Do something good with this error. 
     *      ex.printStackTrace();
     * }
     * }
     * </pre>
     * 
     * @param agent
     *            is the agent machine's name and must be provided, caller may
     *            give full name (e.g., agent:LZ) or host name.
     * @param commandLine
     *            if provided wraps the scriptFileName e.g., "perl myperl.pl"
     *            where perl is the command.
     * @param scriptFileExtension
     *            (e.g., .bat or .ksh). Script content will be saved in a file
     *            name with this extension and executed. This is dependent on
     *            the target agent OS or expectation of the command defined in
     *            "commandLine" above.
     * @param scriptContent
     *            contents to be executed and must be provided. Also should be
     *            compliant with the agent environment.
     * @param remoteDirectory
     *            where to put the script and execute. If not specified it'll be
     *            picked up from the gateway configuration.
     * @param remoteUsername
     *            is a username that will execute the command on the agent. It's
     *            for *nix agent only. If provided then the gateway will execute
     *            the command with "su - remoteUsername -c command" on the
     *            agent.
     * @param username
     *            the username for the host server. If not provided uses the
     *            gateway configuration.
     * @param password
     *            ties with the username.
     * @return the output of the script.
     * @throws Exception
     */
    public static String executeRemoteScript(String agent, String commandLine, String scriptFileExtension, String scriptContent, String remoteDirectory, String remoteUsername, String username, String password) throws Exception
    {
        ITMGateway itmGateway = new ITMGateway(username, password);
        return itmGateway.executeRemoteScript(agent, commandLine, scriptFileExtension, scriptContent, remoteDirectory, remoteUsername);
    }
}
