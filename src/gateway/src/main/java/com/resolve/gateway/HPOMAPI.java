/******************************************************************************
* (C) Copyright 2014
*
* Resolve Systems, LLC
*
* All rights reserved
* This software is distributed under license and may not be used, copied,
* modified, or distributed without the express written permission of
* Resolve Systems, LLC.
*
******************************************************************************/

package com.resolve.gateway;

import java.util.List;
import java.util.Map;

import com.resolve.rsbase.MainBase;
import com.resolve.gateway.amqp.AmqpGateway;
import com.resolve.gateway.hpom.HPOMGateway;
import com.resolve.util.StringUtils;

/**
 * This class provides a convenient way to interact with the Hewlett Packard
 * Operations Manager (HPOM) system.
 *
 */
public class HPOMAPI
{
    private static HPOMGateway instance = HPOMGateway.getInstance();

    /**
     * Returns the {@link Object} value for the given key in the current
     * HPOMGateway instance's NameProperty map.
     * 
     * @param name
     *            the name of the property {@link Map} in the
     *            HPOMGateway instance's {@link NameProperties} object
     * @param key
     *            the key associated with the expected value in the property
     *            {@link Map} returned from the HPOMGateway's
     *            {@link NameProperties} object
     * @return {@link Object} value associated with the key in the property
     *         {@link Map} returned by the HPOMGateway instance's
     *         {@link NameProperties} object
     */
    public static Object getNameObject(String name, String key)
    {
        return instance.getNameProperties().getNameObject(name, key);
    }

    /**
     * Uses the given name parameter to return a property {@link Map} within the
     * HPOMGateway instance's {@link NameProperties} object. If the
     * returned {@link Map} isn't null, it adds the given {@link String} key and
     * {@link Object} value to it.
     * 
     * @param name
     *            the name of the {@link Map} in the HPOMGateway
     *            instance's {@link NameProperties} object. Cannot be null or
     *            empty.
     * @param key
     *            the key in the {@link Map} returned from the
     *            {@link NameProperties} object in the HPOMGateway.
     *            Cannot be null or empty.
     * @param value
     *            the value associated with the given key in the {@link Map}
     *            returned from the {@link NameProperties} object in the
     *            HPOMGateway
     * 
     * @throws Exception
     */
    public static void setNameObject(String name, String key, Object value) throws Exception
    {
        Map nameProperties = getNameProperties(name);
        if(nameProperties != null)
        {
            nameProperties.put(key, value);
            setNameProperties(name, nameProperties, true);
        }
    }
    
    /**
     * Uses the given {@link String} name as a key to return a {@link Map} of
     * properties from the HPOMGateway instance's {@link NameProperties}
     * object.
     *
     * @param name
     *            The name associated with the properties {@link Map} in the
     *            HPOMGateway's {@link NameProperties} object. Cannot be
     *            null or empty.
     * @return a Map of HPOMGateway name properties
     * @throws Exception
     */
    public static Map getNameProperties(String name) throws Exception
    {
        return instance.getNameProperties().getNameProperties(name);
    }

    /**
     * Associates the given name to the given properties {@link Map} in the
     * HPOMGateway instance's {@link NameProperties} object.
     *
     * @param name
     *            the key in the HPOMGateway's {@link NameProperties}
     *            object. Cannot be null or empty.
     * @param properties
     *            A Map of properties to associate with the given name parameter
     * @throws Exception
     */
    public static void setNameProperties(String name, Map properties) throws Exception
    {
        setNameProperties(name, properties, true);
    }

    /**
     * Associates the given name to the given properties Map in the HPOMGateway
     * instance's NameProperties object.
     *
     * @param name
     *            the key in the HPOMGateway's {@link NameProperties}
     *            object. Cannot be null or empty.
     * @param properties
     *            A {@link Map} of properties to associate with the given name
     *            parameter
     * @param doSave
     *            a boolean value that designates whether to send this change to
     *            RSControl for persistence
     * @throws Exception
     */
    public static void setNameProperties(String name, Map properties, boolean doSave) throws Exception
    {
        if (StringUtils.isBlank(name))
        {
            throw new Exception("'name' parameter must be passed");
        }
        else
        {
            instance.setNameProperties(name, properties, doSave);
        }
    }

    /**
     * Removes a {@link Map} of properties associated with the given name in the
     * HPOMGateway instance's {@link NameProperties} object.
     *
     * @param name
     *            The name associated with a property {@link Map} in the
     *            HPOMGateway instance's {@link NameProperties} object.
     *            Cannot be null or empty.
     * @throws Exception 
     */
    public static void removeNameProperties(String name) throws Exception
    {
        if (StringUtils.isBlank(name))
        {
            throw new Exception("'name' parameter must be passed");
        }
        else
        {
            instance.removeNameProperties(name);
        }
    }

    /**
     * Saves the configurations in the RSRemote.
     *
     * @throws Exception
     */
    public static void saveConfig() throws Exception
    {
        MainBase.main.callSaveConfig();
    }

    /**
     * This method creates a new event in the HPOM.
     *
     * <pre>
     * {@code
     *         import com.resolve.gateway.HPOMAPI;
     *         import com.resolve.gateway.HPOMConstants;
     *         ....
     *         ....
     *         try
     *         {
     *             String username = "hpomuser"; //if null then uses gateway configured value.
     *             String password = "hpompassword"; //if null then uses gateway configured value.
     *
     *             String nodeIPAddress = "123.123.123.123";
     *
     *             Map<String, String> params = new HashMap<String, String>();
     *
     *             params.put(HPOMConstants.FIELD_TITLE, "My New Event.");
     *             params.put(HPOMConstants.FIELD_DESCRIPTION, "My description.");
     *             //Could be one of Critical, Major, Minor, Normal, Warning and it's case sensitive.
     *             params.put(HPOMConstants.FIELD_SEVERITY, "Major");
     *             //If there is any custom field then.
     *             params.put("Custom field 1", "Custom field 1 value");
     *             params.put("Custom field 2", "Custom field 2 value");
     *
     *             String eventId = HPOMAPI.createEvent(nodeIPAddress, params, username, password);
     *             System.out.println("New event Id:" + eventId);
     *         }
     *         catch (Exception e)
     *         {
     *             e.printStackTrace();
     *             //Do something usefull with the execption.
     *         }
     * }
     * </pre>
     *
     * @param nodeIPAddress
     *            IP address or DNS host name to be used as the node reference
     *            in the event. If not provided uses the IP address of the
     *            server where RSRemote is running.
     * @param params
     *            is a {@link Map} with various keys (properties of the event)
     *            with their values. For the supported keys, refer to
     *            {@link HPOMConstants}. Anything other than the listed ones
     *            will be treated as custom fields.
     *
     * @param username
     *            if not provided uses the gateway configuration.
     * @param password
     *            ties with the username. Gateway configuration is used if not
     *            provided.
     * @return the newly created event's Id.
     * @throws Exception
     */
    public static String createEvent(String nodeIPAddress, Map<String, String> params, String username, String password) throws Exception
    {
        return instance.createEvent(nodeIPAddress, params, username, password);
    }

    /**
     * Updates an existing event. HPOM may place some restrictions on changing
     * the value for any arbitrary elements, for more information refer to the
     * HPOM documentation. However you can change Title and Severity using this
     * API. For severity the value must be one of these: - Normal - Warning -
     * nMinor - Major - Critical
     *
     * <pre>
     * {@code
     *         import com.resolve.gateway.HPOMAPI;
     *         import com.resolve.gateway.HPOMConstants;
     *         ....
     *         ....
     *         try
     *         {
     *             String username = "hpomuser"; //if null then uses gateway configured value.
     *             String password = "hpompassword"; //if null then uses gateway configured value.
     *
     *             String eventId = "2438be98-01c1-71e2-1fa2-0a1e0a130000";
     *
     *             Map<String, String> params = new HashMap<String, String>();
     *
     *             params.put(HPOMConstants.FIELD_TITLE, "Changed title");
     *             params.put(HPOMConstants.FIELD_SEVERITY, "Minor");
     *
     *             HPOMAPI.updateEvent(eventId, params, username, password);
     *             System.out.println("Event updated sucessfully.");
     *         }
     *         catch (Exception e)
     *         {
     *             e.printStackTrace();
     *             //Do something usefull with the execption.
     *         }
     * }
     * </pre>
     *
     * @param eventId
     *            of the event to be updated.
     * @param params
     *            is a {@link Map} with various keys (properties of the event)
     *            with their values. For the supported keys, refer to
     *            {@link HPOMConstants}. Anything other than the listed ones
     *            will be treated as custom fields.
     *
     * @param username
     *            if not provided uses the gateway configuration.
     * @param password
     *            ties with the username. Gateway configuration is used if not
     *            provided.
     * @throws Exception
     */
    public static void updateEvent(String eventId, Map<String, String> params, String username, String password) throws Exception
    {
        instance.updateEvent(eventId, params, username, password);
    }

    /**
     * Closes an existing event. HPOM may place some restrictions on closing an
     * event based on its business logic, for more information refer to the HPOM
     * documentation.
     *
     * <pre>
     * {@code
     *         import com.resolve.gateway.HPOMAPI;
     *         import com.resolve.gateway.HPOMConstants;
     *         ....
     *         ....
     *         try
     *         {
     *             String username = "hpomuser"; //if null then uses gateway configured value.
     *             String password = "hpompassword"; //if null then uses gateway configured value.
     *
     *             String eventId = "2438be98-01c1-71e2-1fa2-0a1e0a130000";
     *
     *             HPOMAPI.closeEvent(eventId, username, password);
     *             System.out.println("Event closed sucessfully.");
     *         }
     *         catch (Exception e)
     *         {
     *             e.printStackTrace();
     *             //Do something usefull with the execption.
     *         }
     * }
     * </pre>
     *
     * @param eventId
     *            of the event to be closed.
     * @param username
     *            if not provided uses the gateway configuration.
     * @param password
     *            ties with the username. Gateway configuration is used if not
     *            provided.
     * @throws Exception
     */
    public static void closeEvent(String eventId, String username, String password) throws Exception
    {
        instance.closeEvent(eventId, username, password);
    }

    /**
     * Gets the data for an existing event.
     *
     * <pre>
     * {@code
     *         import com.resolve.gateway.HPOMAPI;
     *         import com.resolve.gateway.HPOMConstants;
     *         ....
     *         ....
     *         try
     *         {
     *             String username = "hpomuser"; //if null then uses gateway configured value.
     *             String password = "hpompassword"; //if null then uses gateway configured value.
     *
     *             String eventId = "2438be98-01c1-71e2-1fa2-0a1e0a130000";
     *
     *             Map<String, String> result = HPOMAPI.selectEventById(eventId, username, password);
     *
     *             System.out.println("Event details:");
     *             for(String key : result.keySet())
     *             {
     *                 System.out.printf("%s = %s\n", key, result.get(key));
     *             }
     *         }
     *         catch (Exception e)
     *         {
     *             e.printStackTrace();
     *             //Do something usefull with the execption.
     *         }
     * }
     * </pre>
     *
     * @param eventId
     *            to get data.
     * @param username
     *            if not provided uses the gateway configuration.
     * @param password
     *            ties with the username. Gateway configuration is used if not
     *            provided.
     * @throws Exception
     */
    public static Map<String, String> selectEventById(String eventId, String username, String password) throws Exception
    {
        return instance.selectEventById(eventId, username, password);
    }

    /**
     * Queries the HPOM events based on the provided query.
     *
     * <pre>
     * {@code
     *         import com.resolve.gateway.HPOMAPI;
     *         import com.resolve.gateway.HPOMConstants;
     *         ....
     *         ....
     *         try
     *         {
     *             String username = "hpomuser"; //if null then uses gateway configured value.
     *             String password = "hpompassword"; //if null then uses gateway configured value.
     *
     *             String query = "severity='Major' and title contains 'network'"; //HPOM is case-sensitive on severity
     *             int maxResult = 100;
     *
     *             List<Map<String, String>> result = HPOMAPI.searchEvent(query, maxResult, username, password);
     *
     *             System.out.println("Total number of events returned: " + result.size());
     *             int i = 1;
     *             for(Map<String, String> map : result)
     *             {
     *                 System.out.printf("Event# %d\n", i++);
     *                 for(String key : map.keySet())
     *                 {
     *                     System.out.printf("%s = %s\n", key, map.get(key));
     *                 }
     *             }
     *         }
     *         catch (Exception e)
     *         {
     *             e.printStackTrace();
     *             //Do something usefull with the execption.
     *         }
     * }
     * </pre>
     *
     * @param query
     *            in the form severity = 'major' and title contains 'network'
     * @param maxResult
     *            to be retuened. -1 returns everything.
     * @param username
     *            if not provided uses the gateway configuration.
     * @param password
     *            ties with the username. Gateway configuration is used if not
     *            provided.
     * @return {@link List} of {@link Map} which contain properties for all
     *          events found.
     * @throws Exception
     */
    public static List<Map<String, String>> searchEvent(String query, int maxResult, String username, String password) throws Exception
    {
        return instance.searchEvent(query, maxResult, username, password);
    }

}
