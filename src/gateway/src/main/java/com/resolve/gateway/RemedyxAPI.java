/******************************************************************************
* (C) Copyright 2014
*
* Resolve Systems, LLC
*
* All rights reserved
* This software is distributed under license and may not be used, copied,
* modified, or distributed without the express written permission of
* Resolve Systems, LLC.
*
******************************************************************************/

package com.resolve.gateway;

import java.util.List;
import java.util.Map;

import com.resolve.rsbase.MainBase;
import com.resolve.gateway.remedyx.RemedyxGateway;
import com.resolve.util.StringUtils;

public class RemedyxAPI
{
    private static RemedyxGateway instance = RemedyxGateway.getInstance();

    /**
     * Returns the {@link Object} value for the given key in the current
     * RemedyxGateway instance's NameProperty map.
     * 
     * @param name
     *            the name of the property {@link Map} in the
     *            RemedyxGateway instance's {@link NameProperties} object
     * @param key
     *            the key associated with the expected value in the property
     *            {@link Map} returned from the RemedyxGateway's
     *            {@link NameProperties} object
     * @return {@link Object} value associated with the key in the property
     *         {@link Map} returned by the RemedyxGateway instance's
     *         {@link NameProperties} object
     */
    public static Object getNameObject(String name, String key)
    {
        return instance.getNameProperties().getNameObject(name, key);
    }

    /**
     * Uses the given name parameter to return a property {@link Map} within the
     * RemedyxGateway instance's {@link NameProperties} object. If the
     * returned {@link Map} isn't null, it adds the given {@link String} key and
     * {@link Object} value to it.
     * 
     * @param name
     *            the name of the {@link Map} in the RemedyxGateway
     *            instance's {@link NameProperties} object. Cannot be null or
     *            empty.
     * @param key
     *            the key in the {@link Map} returned from the
     *            {@link NameProperties} object in the RemedyxGateway.
     *            Cannot be null or empty.
     * @param value
     *            the value associated with the given key in the {@link Map}
     *            returned from the {@link NameProperties} object in the
     *            RemedyxGateway
     * 
     * @throws Exception
     */
    public static void setNameObject(String name, String key, Object value) throws Exception
    {
        Map nameProperties = getNameProperties(name);
        if(nameProperties != null)
        {
            nameProperties.put(key, value);
            setNameProperties(name, nameProperties, true);
        }
    }
    
    /**
     * Uses the given {@link String} name as a key to return a {@link Map} of
     * properties from the RemedyxGateway instance's {@link NameProperties}
     * object.
     *
     * @param name
     *            The name associated with the properties {@link Map} in the
     *            RemedyxGateway's {@link NameProperties} object. Cannot be
     *            null or empty.
     * @return a Map of RemedyxGateway name properties
     * @throws Exception
     */
    public static Map getNameProperties(String name) throws Exception
    {
        return instance.getNameProperties().getNameProperties(name);
    }

    /**
     * Associates the given name to the given properties {@link Map} in the
     * RemedyxGateway instance's {@link NameProperties} object.
     *
     * @param name
     *            the key in the RemedyxGateway's {@link NameProperties}
     *            object. Cannot be null or empty.
     * @param properties
     *            A Map of properties to associate with the given name parameter
     * @throws Exception
     */
    public static void setNameProperties(String name, Map properties) throws Exception
    {
        setNameProperties(name, properties, true);
    }

    /**
     * Associates the given name to the given properties Map in the RemedyxGateway
     * instance's NameProperties object.
     *
     * @param name
     *            the key in the RemedyxGateway's {@link NameProperties}
     *            object. Cannot be null or empty.
     * @param properties
     *            A {@link Map} of properties to associate with the given name
     *            parameter
     * @param doSave
     *            a boolean value that designates whether to send this change to
     *            RSControl for persistence
     * @throws Exception
     */
    public static void setNameProperties(String name, Map properties, boolean doSave) throws Exception
    {
        if (StringUtils.isBlank(name))
        {
            throw new Exception("'name' parameter must be passed");
        }
        else
        {
            instance.setNameProperties(name, properties, doSave);
        }
    }

    /**
     * Removes a {@link Map} of properties associated with the given name in the
     * RemedyxGateway instance's {@link NameProperties} object.
     *
     * @param name
     *            The name associated with a property {@link Map} in the
     *            RemedyxGateway instance's {@link NameProperties} object.
     *            Cannot be null or empty.
     * @throws Exception 
     */
    public static void removeNameProperties(String name) throws Exception
    {
        if (StringUtils.isBlank(name))
        {
            throw new Exception("'name' parameter must be passed");
        }
        else
        {
            instance.removeNameProperties(name);
        }
    }

    /**
     * Saves the configurations in the RSRemote.
     *
     * @throws Exception
     */
    public static void saveConfig() throws Exception
    {
        MainBase.main.callSaveConfig();
    }

    /**
     * Creates a new remedy object for a form.
     *
     * <pre>
     * {@code
     *         try
     *         {
     *             java.lang.String formName = "HPD:IncidentInterface_Create";
     *
     *             //the field list and their values must be compilant with your remedy form, make sure all mandatory fields are provided and their values are of
     *             //correct type.
     *             java.lang.String fieldWithValues = "2=Demo&7=2&8=This is short description&1000000001=Calbro Services" +
     *                     "&1000000018=Unser&1000000019=Joe&1000000056=xyz" +
     *                     "&1000000082=Calbro Services&1000000099=0&1000000162=2000" +
     *                     "&1000000163=2000&1000000164=1&1000000000=This is full description&1000000076=CREATE&1000000215=5000)";
     *             java.lang.String username = "remedyuser"; //if null then uses gateway configured value.
     *             java.lang.String password = "remedypassword"; //if null then uses gateway configured value.
     *
     *             java.lang.String entryId = com.resolve.gateway.RemedyxAPI.create(formName, fieldWithValues, username, password);
     *             System.out.printf("This is the entry ID created by Remedy %s.\n" + entryId);
     *         }
     *         catch (java.lang.Exception e)
     *         {
     *             e.printStackTrace();
     *             //Do something usefull with the execption.
     *         }
     * }
     * </pre>
     *
     * @param formName
     *            Remedy Form to be used.
     * @param fieldValues
     *            a delimeted (with "=" and "&amp;") string with form's field and
     *            values. for example
     *            "2=Demo&amp;7=2&amp;8=This is the short description"
     * @param username
     *            if not provided uses the gateway configuration.
     * @param password
     *            ties with the username..
     * @return a String with the Entry ID of the newly created object.
     * @throws Exception
     */
    public static String create(String formName, String fieldValues, String username, String password) throws Exception
    {
        return instance.create(formName, fieldValues, username, password);
    }

    /**
     * Creates a new remedy object for a form.
     *
     * <pre>
     * {@code
     *         try
     *         {
     *              java.lang.String formName = "HPD:IncidentInterface_Create";
     *
     *              //the field list and their values must be compilant with the remedy form, make sure all mandatory fields are provided and their values are of
     *              //correct type.
     *              Map fieldValueMap = new HashMap();
     *              fieldValueMap.put("2", "Demo");                         // Submitter
     *              fieldValueMap.put("7", "1");                            // Status
     *              fieldValueMap.put("8", "The Short Description");        // Short Description
     *              fieldValueMap.put("1000000000", "The Description");     // Description
     *
     *              fieldValueMap.put("1000000018", "Unser");               // Last_Name
     *              fieldValueMap.put("1000000019", "Joe");                 // First_Name
     *              fieldValueMap.put("1000000099", "1");                   // Service_Type
     *              fieldValueMap.put("1000000162", "2000");                // Urgency
     *              fieldValueMap.put("1000000163", "2000");                // Impact
     *
     *              fieldValueMap.put("1000000076", "CREATE");              //z1D Action*
     *              fieldValueMap.put("1000000215", "5000");                //Reported Source*
     *
     *              java.lang.String username = null; //if null then uses gateway configured value.
     *              java.lang.String password = null;  //if null then uses gateway configured value.
     *
     *              java.lang.String entryId = com.resolve.gateway.RemedyxAPI.create(formName, fieldValueMap, username, password);
     *              System.out.printf("This is the entry ID created by Remedy %s.\n" + entryId);
     *         }
     *         catch (java.lang.Exception e)
     *         {
     *             e.printStackTrace();
     *             //Do something usefull with the execption.
     *         }
     * }
     * </pre>
     *
     * @param formName
     *            Remedy Form to be used.
     * @param fieldValueMap
     *            refer to the Remedy documentation for more information for
     *            various properties you can set for the given "form". It may
     *            vary by installation.
     * @param username
     *            if not provided uses the gateway configuration.
     * @param password
     *            ties with the username..
     * @return a String with the Entry ID of the newly created object.
     * @throws Exception
     */
    public static String create(String formName, Map<String, String> fieldValueMap, String username, String password) throws Exception
    {
        return instance.create(formName, fieldValueMap, username, password);
    }

    /**
     * Updated an existing object for a form. There may be restriction
     *
     * <pre>
     * {@code
     *         try
     *         {
     *             java.lang.String formName = "HPD:IncidentInterface_Create";
     *             java.lang.String entryId = "INC000000000011";
     *             java.lang.String fieldValues = "8=Short descripton changed&1000000000=Full description changed";
     *             java.lang.String username = "remedyuser"; //if null then uses gateway configured value.
     *             java.lang.String password = "remedypassword"; //if null then uses gateway configured value.
     *             com.resolve.gateway.RemedyxAPI.update(formName, entryId, fieldValues, username, password);
     *         }
     *         catch (java.lang.Exception e)
     *         {
     *             e.printStackTrace();
     *             //Do something usefull with the execption.
     *         }
     * }
     * </pre>
     *
     * @param formName
     *            Remedy Form to be used.
     * @param entryId
     *            entry id for the object.
     * @param fieldValues
     *            a delimeted (with "=" and "&amp;") string with form's field and
     *            values. for example
     *            "2=Demo&amp;7=2&amp;8=This is the short description"
     * @param username
     *            if not provided uses the gateway configuration.
     * @param password
     *            ties with the username..
     * @throws Exception
     */
    public static void update(String formName, String entryId, String fieldValues, String username, String password) throws Exception
    {
        instance.update(formName, entryId, fieldValues, username, password);
    }

    /**
     * Updated an existing object for a form.
     *
     * <pre>
     * {@code
     *         try
     *         {
     *             java.lang.String formName = "HPD:IncidentInterface_Create";
     *             java.lang.String entryId = "INC000000000011";
     *
     *             // the field list and their values must be compilant with the remedy form, make sure that correct modifiable fields
     *             // are provided with correct values.
     *             // note that some fields may not be modified due to restriction applied by Remedy.
     *             Map<java.lang.String, java.lang.String> fieldValueMap = new HashMap<java.lang.String, java.lang.String>();
     *             fieldValueMap.put("8", "Short descripton changed");
     *             fieldValueMap.put("1000000000", "Full descripton changed");
     *
     *             java.lang.String username = "remedyuser"; //if null then uses gateway configured value.
     *             java.lang.String password = "remedypassword"; //if null then uses gateway configured value.
     *             com.resolve.gateway.RemedyxAPI.update(formName, entryId, fieldValueMap, username, password);
     *         }
     *         catch (java.lang.Exception e)
     *         {
     *             e.printStackTrace();
     *             //Do something usefull with the execption.
     *         }
     * }
     * </pre>
     *
     * @param formName
     *            Remedy Form to be used.
     * @param entryId
     *            entry id for the object.
     * @param fieldValueMap
     *            refer to the Remedy documentation for more information for
     *            various properties you can set for the given "form". It may
     *            vary by installation.
     * @param username
     *            if not provided uses the gateway configuration.
     * @param password
     *            ties with the username..
     * @throws Exception
     */
    public static void update(String formName, String entryId, Map<String, String> fieldValueMap, String username, String password) throws Exception
    {
        instance.update(formName, entryId, fieldValueMap, username, password);
    }

    /**
     * Deletes a Remedy object for a form.
     *
     * Note: Remedy may not allow deleting due to certain dependencies (e.g.,
     * status etc.). Please refer to the documentation.
     *
     * <pre>
     * {@code
     *         try
     *         {
     *             java.lang.String formName = "HPD:IncidentInterface";
     *             java.lang.String entryId = "INC000000000008";
     *
     *             java.lang.String username = "remedyuser"; //if null then uses gateway configured value.
     *             java.lang.String password = "remedypassword"; //if null then uses gateway configured value.
     *
     *             //Note that Remedy may block a record from deleting sue to some business rules.
     *             //refer to Remedy documentation.
     *             com.resolve.gateway.RemedyxAPI.delete(formName, entryId, username, password);
     *         }
     *         catch (java.lang.Exception e)
     *         {
     *             e.printStackTrace();
     *             //Do something usefull with the execption.
     *         }
     * }
     * </pre>
     *
     * @param formName
     *            Remedy Form to be used.
     * @param entryId
     *            must be provided. It's the id (e.g., entry id) auto-generated
     *            by the system.
     * @param username
     *            if not provided uses the gateway configuration.
     * @param password
     *            ties with the username. Gateway configuration is used if not
     *            provided.
     * @throws Exception
     */
    public static void delete(String formName, String entryId, String username, String password) throws Exception
    {
        instance.delete(formName, entryId, username, password);
    }

    /**
     * Searches Remedy form based on the query. The query string must be
     * compliant with the Remedy documentation for form (e.g., HPD:Help Desk)
     * object.
     *
     * <pre>
     * {@code
     *     try
     *     {
     *         java.lang.String formName = "HPD:Help Desk";
     *         java.lang.String criteria = "'7'=2"; //this criteria gets all the tickets where status = 2.
     *         java.lang.String fieldIds = "2, 6, 7, 8, 1000000161"; //we're only interested in these fields
     *         java.lang.String username = "remedyuser"; //if null then uses gateway configured value.
     *         java.lang.String password = "remedypassword"; //if null then uses gateway configured value.
     *
     *         com.resolve.gateway.RemedyxAPI.search(formName, criteria, fieldIds, username, password);
     *     }
     *     catch (java.lang.Exception e)
     *     {
     *         e.printStackTrace();
     *         //Do something usefull with the execption.
     *    }
     * }
     * </pre>
     *
     * @param formName
     *            Remedy Form to be used.
     * @param criteria
     *            the selection riteria.
     * @param fieldIds
     *            the result will have only these fields. This is a comma
     *            separated list of field Ids (e.g., "1,2,6,7,8,100000161"). If
     *            null then uses the field Ids from the form deployed in the
     *            gateway.
     * @param username
     *            if not provided uses the gateway configuration.
     * @param password
     *            ties with the username. Gateway configuration is used if not
     *            provided.
     * @return a {@link List} of {@link Map} which contain properties for all
     *         the forms.
     * @throws Exception
     */
    public static List<Map<String, String>> search(String formName, String criteria, String fieldIds, String username, String password) throws Exception
    {
        //just get everything without any sorting
        return instance.search(formName, criteria, fieldIds, -1, null, null, username, password);
    }

    /**
     * Searches Remedy form based on the query. The query string must be
     * compliant with the Remedy documentation for form (e.g., HPD:Help Desk)
     * object.
     *
     * <pre>
     * {@code
     *     try
     *     {
     *         java.lang.String formName = "HPD:Help Desk";
     *         java.lang.String criteria = "'7'=2"; //this criteria gets all the tickets where status = 2.
     *         java.lang.String fieldIds = "2, 6, 7, 8, 1000000161"; //we're only interested in these fields
     *         int maxResults = 100;
     *         java.lang.String username = "remedyuser"; //if null then uses gateway configured value.
     *         java.lang.String password = "remedypassword"; //if null then uses gateway configured value.
     *
     *         com.resolve.gateway.RemedyxAPI.search(formName, criteria, fieldIds, maxResults, 3, true, username, password);
     *     }
     *     catch (java.lang.Exception e)
     *     {
     *         e.printStackTrace();
     *         //Do something usefull with the execption.
     *     }
     * }
     * </pre>
     *
     * @param formName
     *            Remedy Form to be used.
     * @param criteria
     *            the selection riteria.
     * @param fieldIds
     *            the result will have only these fields. This is a comma
     *            separated list of field Ids (e.g., "1,2,6,7,8,100000161"). If
     *            null then uses the field Ids from the form deployed in the
     *            gateway.
     * @param maxResults
     *            number of records to be returned, pass @lt;= 0 to get everything.
     * @param sortFieldId
     *            field id to sort on (e.g., 3 submitted date or 6 modified date etc).
     * @param isAscending
     *            true to sort in ascending order.
     * @param username
     *            if not provided uses the gateway configuration.
     * @param password
     *            ties with the username. Gateway configuration is used if not
     *            provided.
     * @return a {@link List} of {@link Map} which contain properties for all
     *         the forms.
     * @throws Exception
     */
    public static List<Map<String, String>> search(String formName, String criteria, String fieldIds, int maxResults, Integer sortFieldId, Boolean isAscending, String username, String password) throws Exception
    {
        return instance.search(formName, criteria, fieldIds, maxResults, sortFieldId, isAscending, username, password);
    }

    /**
     * Selects Remedy based on the entry id.
     *
     * <pre>
     * {@code
     *         try
     *         {
     *             java.lang.String formName = "HPD:Help Desk";
     *             java.lang.String entryId = "INC000000000011";
     *             java.lang.String username = "remedyuser"; //if null then uses gateway configured value.
     *             java.lang.String password = "remedypassword"; //if null then uses gateway configured value.
     *
     *             Map<java.lang.String, java.lang.String> result = com.resolve.gateway.RemedyxAPI.selectByEntryId(formName, entryId, username, password);
     *             //result has the fields with their values.
     *         }
     *         catch (java.lang.Exception e)
     *         {
     *             e.printStackTrace();
     *             //Do something usefull with the execption.
     *         }
     * }
     * </pre>
     *
     * @param formName
     *            Remedy Form to be used.
     * @param entryId
     *            must be provided. It's the id (e.g., entry id) auto-generated
     *            by the system.
     * @param username
     *            if not provided uses the gateway configuration.
     * @param password
     *            ties with the username. Gateway configuration is used if not
     *            provided.
     * @return a {@link Map} that contains properties of the form.
     * @throws Exception
     */
    public static Map<String, String> selectByEntryId(String formName, String entryId, String username, String password) throws Exception
    {
        return instance.selectByEntryID(formName, entryId, username, password);
    }

    /**
     * Returns the Entry Ids of a form.
     *
     * <pre>
     * {@code
     *         try
     *         {
     *             java.lang.String formName = "HPD:Help Desk";
     *             java.lang.String criteria = "'7'=2"; //this criteria gets all the tickets where status = 2.
     *             java.lang.String username = "remedyuser"; //if null then uses gateway configured value.
     *             java.lang.String password = "remedypassword"; //if null then uses gateway configured value.
     *
     *             List<java.lang.String> result = com.resolve.gateway.RemedyxAPI.findEntryId(formName, criteria, username, password);
     *             //result has all the entry ids returned by the criteria.
     *         }
     *         catch (java.lang.Exception e)
     *         {
     *             e.printStackTrace();
     *             //Do something usefull with the execption.
     *         }
     * }
     * </pre>
     *
     * @param formName
     *            Remedy Form to be used.
     * @param criteria
     *            the selection criteria.
     * @param username
     *            if not provided uses the gateway configuration.
     * @param password
     *            ties with the username. Gateway configuration is used if not
     *            provided.
     * @return a {@link List} that contains entry Ids of the objects.
     * @throws Exception
     */
    public static List<String> findEntryId(String formName, String criteria, String username, String password) throws Exception
    {
        return instance.find(formName, criteria, username, password);
    }
}
