package com.resolve.gateway.util.rest;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.net.CookieManager;
import java.net.CookiePolicy;
import java.net.CookieStore;
import java.net.MalformedURLException;
import java.net.URI;
import java.net.URL;
import java.security.KeyManagementException;
import java.security.KeyStore;
import java.security.KeyStoreException;
import java.security.NoSuchAlgorithmException;
import java.security.cert.CertificateException;
import java.security.cert.X509Certificate;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;

import javax.net.ssl.HostnameVerifier;
import javax.net.ssl.KeyManagerFactory;
import javax.net.ssl.SSLContext;
import javax.net.ssl.SSLSession;
import javax.net.ssl.TrustManager;
import javax.net.ssl.TrustManagerFactory;
import javax.net.ssl.X509TrustManager;

import org.apache.commons.net.util.TrustManagerUtils;
import org.apache.http.Header;
import org.apache.http.HttpEntity;
import org.apache.http.HttpHeaders;
import org.apache.http.HttpHost;
import org.apache.http.HttpResponse;
import org.apache.http.HttpStatus;
import org.apache.http.NameValuePair;
import org.apache.http.auth.AuthScope;
import org.apache.http.auth.Credentials;
import org.apache.http.auth.KerberosCredentials;
import org.apache.http.auth.NTCredentials;
import org.apache.http.auth.UsernamePasswordCredentials;
import org.apache.http.client.CredentialsProvider;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpDelete;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpHead;
import org.apache.http.client.methods.HttpOptions;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.client.methods.HttpPut;
import org.apache.http.client.methods.HttpRequestBase;
import org.apache.http.client.utils.URIBuilder;
import org.apache.http.conn.HttpClientConnectionManager;
import org.apache.http.conn.ssl.SSLConnectionSocketFactory;
import org.apache.http.entity.ByteArrayEntity;
import org.apache.http.impl.client.BasicCredentialsProvider;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClientBuilder;
import org.apache.http.impl.client.ProxyAuthenticationStrategy;
import org.apache.http.impl.conn.BasicHttpClientConnectionManager;
import org.ietf.jgss.GSSCredential;
import org.ietf.jgss.GSSException;
import org.ietf.jgss.GSSManager;

import com.resolve.util.Base64;
import com.resolve.util.Log;
import com.resolve.util.SSLUtils;
import com.resolve.util.StringUtils;
import com.resolve.util.restclient.ConfigProxy;

/**
 * This is a convenience class for consuming REST web services. This is most
 * generic and lowest level class. It is used by gateways such as TSRM and
 * ServiceNow.
 * 
 * All non standard vendor specific header processing and flow control logic
 * should be in dervived or wrapper class.
 */
public class RestCaller
{
    private final String baseUrl;
    private String protocol = "http";
    private int port = 80;
    protected boolean isSSL = false;
    // private String testUrl;
    private String httpbasicauthusername;
    private String httpbasicauthpassword;

    private String urlSuffix;

    private static SSLConnectionSocketFactory sslSocketFactory = null;
    private static String TLS = "TLS";

    // Response status and headers
    protected int statusCode;
    protected String reasonPhrase;
    protected String responseEntity;
    protected Map<String, String> responseHeaders = new HashMap<String, String>();

    // REST service support

    protected String user = null;
    protected String pass = null;
    protected boolean basicAuth = true;
    protected boolean cookieEnabled = true;
    protected boolean selfSigned = true;
    protected boolean trustAll = false;
    protected static volatile CookieStore cookieStore = null;

    // Proxy Support
    static ConfigProxy configProxy;
    static boolean proxyEnabled = false;
    static Credentials proxyCredentials = null;
    static AuthScope proxyAuthScope = null;
    static HttpHost proxy = null;

    // OverrideProxySupport
    static boolean overrideProxy = false;
    static String proxyHost;
    static int proxyPort;
    static String proxyUser;
    static String proxyPassword;

    // TLS SUpport
    static boolean mutualtlsEnabled = false;
    static String mutualtlsKeyStore;
    static String mutualtlsKeyStorePass;
    static String mutualtlsTrustStore;
    static String mutualtlsTrustStorePass;

    // Header Redirect Support
    static boolean headerRedirEnable = false;
    static String headerRedirURL;
    static String headerRedirHeader;
    static boolean headerRedirEncAuth;

    private static volatile CookieManager cookieManager = null;

    static
    {
        cookieManager = new CookieManager();
        cookieManager.setCookiePolicy(CookiePolicy.ACCEPT_ALL);
        cookieStore = cookieManager.getCookieStore();
        // The CookieHandler honors the "httponly" portion of the cookie; that
        // is why you don't see the value for the Set-Cookie header when using
        // the CookieHandler.
        // CookieHandler.setDefault(cookieManager);

        String version = System.getProperty("java.version");
        int pos = version.indexOf('.');
        pos = version.indexOf('.', pos + 1);
        double v = Double.parseDouble(version.substring(0, pos));
        if (v > 1.7) TLS = "TLSv1.2";
    }

    /**
     * Constructor
     * 
     * @param url
     *            in the form of
     *            http://tsrm_server:port/maxrest/rest/os/mxsr?_lid
     *            =maxadmin&_lpwd=maxadmin
     */
    public RestCaller(String baseUrl)
    {
        if (StringUtils.isBlank(baseUrl))
        {
            throw new IllegalStateException("baseUrl must be provided.");
        }

        // this is universally applicable
        String trimmedBaseUrl = baseUrl.trim();
        this.baseUrl = trimmedBaseUrl + (trimmedBaseUrl.endsWith("/") ? "" : "/");

        URL theUrl;
        try
        {
            theUrl = new URL(baseUrl);
            if ("https".equals(theUrl.getProtocol()))
            {
                try
                {
                    if (!mutualtlsEnabled)
                    {
                        SSLContext ctx = SSLContext.getInstance("SSL");
                        SSLUtils.FakeX509TrustManager tm = new SSLUtils.FakeX509TrustManager();
                        ctx.init(null, new TrustManager[] { tm }, null);
                        // sslSocketFactory = new SSLSocketFactory(ctx, new
                        // SSLUtils.FakeX509HostnameVerifier());
                        sslSocketFactory = new SSLConnectionSocketFactory(ctx, new SSLUtils.FakeHostnameVerifier());
                    }
                    else if (mutualtlsEnabled)
                    {
                        SSLContext ctx = SSLContext.getInstance("TLS");
                        // SSLUtils.FakeX509TrustManager tm = new
                        // SSLUtils.FakeX509TrustManager();
                        try
                        {
                            ctx.init(getKeyManager().getKeyManagers(), getTrustManager(), null);
                        }
                        catch (Exception e)
                        {
                            Log.log.error(e.getMessage(), e);
                        }
                        sslSocketFactory = new SSLConnectionSocketFactory(ctx, new SSLUtils.FakeHostnameVerifier());
                        // sslSocketFactory = new
                        // SSLConnectionSocketFactory(ctx, getTrustManager());
                    }

                    isSSL = true;
                    protocol = "https";
                    if (theUrl.getPort() == -1)
                    {
                        port = 443;
                    }
                }
                catch (NoSuchAlgorithmException e)
                {
                    Log.log.error(e.getMessage(), e);
                }
                catch (KeyManagementException e)
                {
                    Log.log.error(e.getMessage(), e);
                }
            }
            else
            {
                if (theUrl.getPort() == -1)
                {
                    port = 80;
                }
            }
        }
        catch (MalformedURLException e1)
        {
            Log.log.error(e1.getMessage(), e1);
        }
    }

    public RestCaller(String baseUrl, String httpbasicauthusername, String httpbasicauthpassword)
    {
        this(baseUrl);
        this.httpbasicauthusername = httpbasicauthusername;
        this.httpbasicauthpassword = httpbasicauthpassword;
        ;
    }

    public RestCaller(String baseURL, boolean ovverideProxyFlag, String proxyHost, int proxyPort, String proxyUser, String proxyP_assword)
    {
        // Not clear yet if I need String httpbasicauthusername, String
        // httpbasicauthpassword that are basically for third party app.
        this(baseURL);
        proxyCredentials = new UsernamePasswordCredentials(proxyUser, proxyP_assword);
        proxyAuthScope = new AuthScope(proxyHost, proxyPort);
        proxy = new HttpHost(proxyHost, proxyPort);

    }

    public RestCaller(String baseUrl, String httpbasicauthusername, String httpbasicauthpassword, ConfigProxy configProxy)
    {
        this(baseUrl, httpbasicauthusername, httpbasicauthpassword);
        if (!RestCaller.mutualtlsEnabled)
        {
            if (configProxy != null && configProxy.proxyEnabled && (!overrideProxy))
            {
                if (StringUtils.isNotBlank(configProxy.proxyHost) && !configProxy.proxyHost.equals("127.0.0.1") && configProxy.proxyPort > 0)
                {
                    try
                    {
                        URL theUrl = new URL(baseUrl);

                        proxyEnabled = !configProxy.isNonProxyHost(theUrl);

                        if (proxyEnabled)
                        {
                            if (!configProxy.authType.equalsIgnoreCase(ConfigProxy.PROXY_AUTHENTICATION_TYPE_NONE))
                            {
                                switch (configProxy.authType)
                                {
                                    case ConfigProxy.PROXY_AUTHENTICATION_TYPE_BASIC:
                                        proxyCredentials = new UsernamePasswordCredentials(configProxy.proxyUser, configProxy.proxyP_assword);
                                        break;

                                    case ConfigProxy.PROXY_AUTHENTICATION_TYPE_NTLM:
                                        proxyCredentials = new NTCredentials(configProxy.proxyUser, configProxy.proxyP_assword, configProxy.ntlmWorkStation, configProxy.ntlmDomain);
                                        break;

                                    case ConfigProxy.PROXY_AUTHENTICATION_TYPE_KERBEROS:
                                        GSSManager gssMgr = GSSManager.getInstance();
                                        // GSSName name =
                                        // gssMgr.createName(proxyUser,
                                        // GSSName.NT_USER_NAME);
                                        proxyCredentials = new KerberosCredentials(gssMgr.createCredential(GSSCredential.ACCEPT_ONLY));
                                        break;
                                }
                            }

                            proxyAuthScope = new AuthScope(configProxy.proxyHost, configProxy.proxyPort);
                            proxy = new HttpHost(configProxy.proxyHost, configProxy.proxyPort);
                        }
                    }
                    catch (MalformedURLException murle)
                    {
                        Log.log.error(murle.getMessage(), murle);
                    }
                    catch (GSSException gsse)
                    {
                        Log.log.error(gsse.getMessage(), gsse);
                    }
                }
            }
        }

    }

    protected KeyManagerFactory getKeyManager() throws Exception
    {
        KeyStore ks;

        char[] pass = RestCaller.mutualtlsKeyStorePass != null ? RestCaller.mutualtlsKeyStorePass.toCharArray() : null;
        ks = loadKeystore(RestCaller.mutualtlsKeyStore, pass);

        KeyManagerFactory kmf = KeyManagerFactory.getInstance(KeyManagerFactory.getDefaultAlgorithm());
        kmf.init(ks, pass);
        return kmf;
    }

    protected KeyStore loadKeystore(String keystoreFileToLoad, char[] pass) throws KeyStoreException, IOException, NoSuchAlgorithmException, CertificateException, FileNotFoundException
    {
        KeyStore ks;
        if (org.apache.commons.lang3.StringUtils.endsWithIgnoreCase(keystoreFileToLoad, ".p12") || org.apache.commons.lang3.StringUtils.endsWithIgnoreCase(keystoreFileToLoad, ".pfx"))
        {
            ks = KeyStore.getInstance("pkcs12");
            ks.load(new FileInputStream(keystoreFileToLoad), pass);

        }
        else
        {

            ks = KeyStore.getInstance("JKS");
            ks.load(new FileInputStream(keystoreFileToLoad), pass);

        }
        return ks;
    }

    protected TrustManager[] getTrustManager() throws Exception
    {
        KeyStore ks;
        if (RestCaller.mutualtlsTrustStore == null || StringUtils.isEmpty(RestCaller.mutualtlsTrustStore))
        {

            return new TrustManager[] { TrustManagerUtils.getAcceptAllTrustManager() };

        }
        else if ("default".equalsIgnoreCase(RestCaller.mutualtlsTrustStore))
        {

            return new TrustManager[] { TrustManagerUtils.getValidateServerCertificateTrustManager() };

        }

        char[] pass = StringUtils.isNotEmpty(RestCaller.mutualtlsTrustStorePass) ? RestCaller.mutualtlsTrustStorePass.toCharArray() : null;
        ks = loadKeystore(RestCaller.mutualtlsTrustStore, pass);

        TrustManagerFactory tmf = TrustManagerFactory.getInstance(TrustManagerFactory.getDefaultAlgorithm());
        tmf.init(ks);

        return tmf.getTrustManagers();
    }

    public static void configProxyOverride(String proxyHost, int proxyPort, String proxyUser, String proxyPassword)
    {
        RestCaller.overrideProxy = true;
        RestCaller.proxyHost = proxyHost;
        RestCaller.proxyPort = proxyPort;
        RestCaller.proxyUser = proxyUser;
        RestCaller.proxyPassword = proxyPassword;
        proxyCredentials = new UsernamePasswordCredentials(proxyUser, proxyPassword);
        proxyAuthScope = new AuthScope(proxyHost, proxyPort);
        proxy = new HttpHost(proxyHost, proxyPort, "http");
        Log.log.debug("Configured Proxy Details. Overriding the proxy configured via Resolve Rsremote proxy in blueprint");

    }

    public void configProxy(String baseURL, ConfigProxy configProxy)
    {
        RestCaller.configProxy = configProxy;
        if (configProxy != null && configProxy.proxyEnabled && (!overrideProxy))
        {
            if (StringUtils.isNotBlank(configProxy.proxyHost) && !configProxy.proxyHost.equals("127.0.0.1") && configProxy.proxyPort > 0)
            {
                try
                {
                    URL theUrl = new URL(baseUrl);

                    proxyEnabled = !configProxy.isNonProxyHost(theUrl);

                    if (proxyEnabled)
                    {
                        if (!configProxy.authType.equalsIgnoreCase(ConfigProxy.PROXY_AUTHENTICATION_TYPE_NONE))
                        {
                            switch (configProxy.authType)
                            {
                                case ConfigProxy.PROXY_AUTHENTICATION_TYPE_BASIC:
                                    proxyCredentials = new UsernamePasswordCredentials(configProxy.proxyUser, configProxy.proxyP_assword);
                                    break;

                                case ConfigProxy.PROXY_AUTHENTICATION_TYPE_NTLM:
                                    proxyCredentials = new NTCredentials(configProxy.proxyUser, configProxy.proxyP_assword, configProxy.ntlmWorkStation, configProxy.ntlmDomain);
                                    break;

                                case ConfigProxy.PROXY_AUTHENTICATION_TYPE_KERBEROS:
                                    GSSManager gssMgr = GSSManager.getInstance();
                                    // GSSName name =
                                    // gssMgr.createName(proxyUser,
                                    // GSSName.NT_USER_NAME);
                                    proxyCredentials = new KerberosCredentials(gssMgr.createCredential(GSSCredential.ACCEPT_ONLY));
                                    break;
                            }
                        }

                        proxyAuthScope = new AuthScope(configProxy.proxyHost, configProxy.proxyPort);
                        proxy = new HttpHost(configProxy.proxyHost, configProxy.proxyPort);
                    }
                }
                catch (MalformedURLException murle)
                {
                    Log.log.error(murle.getMessage(), murle);
                }
                catch (GSSException gsse)
                {
                    Log.log.error(gsse.getMessage(), gsse);
                }
            }
        }
        Log.log.debug("Configured Resolve Proxy Details set up in Resolve Blueprint.properties");

    }

    public static void configTLS(boolean isMutualtlsEnabled, String mutualtlsKeyStore, String mutualtlsKeyStorePass, String mutualtlsTrustStore, String mutualtlsTrustStorePass)
    {

        RestCaller.mutualtlsEnabled = isMutualtlsEnabled;
        RestCaller.mutualtlsKeyStore = mutualtlsKeyStore;
        RestCaller.mutualtlsKeyStorePass = mutualtlsKeyStorePass;
        RestCaller.mutualtlsTrustStore = mutualtlsTrustStore;
        RestCaller.mutualtlsTrustStorePass = mutualtlsTrustStorePass;
    }

    public static void configHeaderRedirect(boolean isHeaderRedirectEnabled, String headerRedirUrl, String headerRedirHeader, Boolean headerRedirEncAuth)
    {
        RestCaller.headerRedirEnable = isHeaderRedirectEnabled;
        RestCaller.headerRedirEncAuth = headerRedirEncAuth;
        RestCaller.headerRedirHeader = headerRedirHeader;
        RestCaller.headerRedirURL = headerRedirUrl;

    }

    // Uses Existing proxy config which is set at the Resolve BP properties or
    // oveeride the existing proxy based on overrideProxy=true.
    protected void setupProxy(HttpClientBuilder builder, CredentialsProvider cp)
    {
        if (overrideProxy == false && proxyCredentials != null && proxyAuthScope != null)
        {
            cp.setCredentials(proxyAuthScope, proxyCredentials);
            builder.setDefaultCredentialsProvider(cp);
        }

        else if (overrideProxy == true && proxy != null)
        {
            cp.setCredentials(proxyAuthScope, proxyCredentials);
            cp.setCredentials(new AuthScope(proxyHost, proxyPort), new UsernamePasswordCredentials(proxyUser, proxyPassword));

            builder.setProxyAuthenticationStrategy(new ProxyAuthenticationStrategy());

            builder.setProxy(proxy);
        }
    }

    /**
     * Constructor with basic information and user preferences
     * 
     * @param baseUrl:
     *            contains protocol (HTTP/HTTPS), host name or ip, port number
     * @param username:
     *            used for Basic Authentication or login to get a token, can be
     *            blank when token is provided or token is set in the cookie
     *            store
     * @param password:
     *            used for Basic Authentication or login to get a token, can be
     *            blank when token is provided or token is set in the cookie
     *            store
     * @param token:
     *            used for each REST call by explicitly embedded it with the key
     *            name of "token" in HTTPRequest when cookie store is not used.
     *            If the key name is not "token", embed the key and value in the
     *            request params. Token can also be set in the request headers
     *            or request params or in the body payload by the caller based
     *            on the server needs. If the server supports cookies, no token
     *            needs to be embedded.
     * @param baseAuth:
     *            if true, username and password will be encode with Base64 and
     *            embedded into HTTP header for authentication
     * @param timeout:
     *            timeout for HTTP/HTTPS connection
     * @param selfSigned:
     *            HTTPSURLConnection will be eatablished when server provides a
     *            self-signed certificate, no hsst name will be verified
     * @param trustAll:
     *            HTTPSURLConnection will be esatblished with fake certificates
     *            and even without any certificate is provided
     * @throws Exception
     */
    public RestCaller(String baseUrl, String user, String pass, boolean basicAuth, boolean cookieEnabled, boolean selfSigned, boolean trustAll)
    {

        this.baseUrl = baseUrl;
        this.user = user;
        this.pass = pass;
        this.basicAuth = basicAuth;
        this.cookieEnabled = cookieEnabled;
        this.selfSigned = selfSigned;
        this.trustAll = trustAll;
    }

    public String getBaseUrl()
    {
        return baseUrl;
    }

    public String getUrlSufix()
    {
        return urlSuffix;
    }

    public void setUrlSuffix(String urlSuffix)
    {
        if (StringUtils.isNotBlank(urlSuffix))
        {
            this.urlSuffix = urlSuffix.trim();
        }
        else
        {
            this.urlSuffix = urlSuffix;
        }
    }

    public String getHttpbasicauthusername()
    {
        return httpbasicauthusername;
    }

    public void setHttpbasicauthusername(String httpbasicauthusername)
    {
        this.httpbasicauthusername = httpbasicauthusername;
    }

    public String getHttpbasicauthpassword()
    {
        return httpbasicauthpassword;
    }

    public void setHttpbasicauthpassword(String httpbasicauthpassword)
    {
        this.httpbasicauthpassword = httpbasicauthpassword;
    }

    public int getStatusCode()
    {
        return statusCode;
    }

    public String getReasonPhrase()
    {
        return reasonPhrase;
    }

    public Map<String, String> getResponseHeaders()
    {
        return responseHeaders;
    }

    public String getResponseEntity()
    {
        return responseEntity;
    }

    /**
     * Method used to create or update objects in TSRM.
     * 
     * @param params
     * @return
     * @throws Exception
     */
    protected String executeMethod(HttpRequestBase method) throws Exception
    {
        String result = null;

        // Reset response status and headers
        statusCode = -11;
        reasonPhrase = "";
        responseHeaders = new HashMap<String, String>();

        // Create an instance of HttpClient.
        // DefaultHttpClient client = new DefaultHttpClient();

        HttpClientBuilder builder = HttpClientBuilder.create();
        CredentialsProvider credsProvider = new BasicCredentialsProvider();

        // setupProxy(builder,credsProvider);
        HttpClientConnectionManager ccm = new BasicHttpClientConnectionManager();
        try
        {

            builder.setSSLHostnameVerifier(new HostnameVerifier()
            {

                @Override
                public boolean verify(String arg0, SSLSession arg1)
                {
                    return true;
                }
            });

            setupMutualTLS(builder);
            setupProxy(builder, credsProvider);
            setHttpBasicAuth(method, credsProvider);
            setHeaderRedirect(method, credsProvider);
            builder.setDefaultCredentialsProvider(credsProvider);
            
            CloseableHttpClient client = builder.build();
            HttpResponse response = client.execute(method);
            if (response.getStatusLine() != null)
            {
                statusCode = response.getStatusLine().getStatusCode();
                reasonPhrase = response.getStatusLine().getReasonPhrase();
            }

            Header[] httpRespHeaders = response.getAllHeaders();

            if (httpRespHeaders != null && httpRespHeaders.length > 0)
            {
                for (int i = 0; i < httpRespHeaders.length; i++)
                    responseHeaders.put(httpRespHeaders[i].getName(), httpRespHeaders[i].getValue());
            }

            HttpEntity entity = response.getEntity();

            if (entity != null)
            {
                // Read the response body.
                result = StringUtils.toString(entity.getContent(), "utf-8");
            }
        }
        catch (IOException e)
        {
            Log.log.error("Fatal transport error:: " + e.getMessage(), e);

            throw e;
        }
        catch (Exception e)
        {
            Log.log.error(e.getMessage(), e);
            throw e;
        }
        finally
        {
            // Release the connection.
            // builder.getConnectionManager().shutdown();
            ccm.shutdown();

        }
        return result;
    }

    protected void setupMutualTLS(HttpClientBuilder builder) throws NoSuchAlgorithmException, KeyManagementException, Exception
    {
        builder.setSSLContext(getSSLContext());
    }

    // TODO Auto-generated constructor stub
    public SSLContext getSSLContext() throws NoSuchAlgorithmException, KeyManagementException, Exception
    {
        SSLContext context;
        if (RestCaller.mutualtlsEnabled)
        {
            context = SSLContext.getInstance("TLS");
            context.init(getKeyManager().getKeyManagers(), getTrustManager(), null);

        }
        else
        {
            context = SSLContext.getInstance("SSL");
            SSLUtils.FakeX509TrustManager tm = new SSLUtils.FakeX509TrustManager();
            context.init(null, new TrustManager[] { tm }, null);
        }
        return context;
    }

    protected void setHttpBasicAuth(HttpRequestBase method, CredentialsProvider cp) throws Exception
    {
        if (StringUtils.isBlank(httpbasicauthusername) || StringUtils.isBlank(httpbasicauthpassword)) throw new Exception("User is missing for Basic Authentication.");

        if (method.getURI() == null) throw new Exception("URI is missing for Basic Authentication.");

        if (StringUtils.isNotBlank(httpbasicauthusername) && StringUtils.isNotBlank(httpbasicauthpassword) && method.getURI() != null)
        {
            String user = httpbasicauthusername + ":" + httpbasicauthpassword;
            method.addHeader("Authorization", "Basic " + Base64.encodeBytes(user.getBytes()));//
            cp.setCredentials(new AuthScope(method.getURI().getHost(), method.getURI().getPort()), new UsernamePasswordCredentials(httpbasicauthusername, httpbasicauthpassword));

        }

    }

    protected void setHttpBasicAuth(HttpClientBuilder httpclient, HttpRequestBase method) throws Exception
    {
        if (StringUtils.isBlank(httpbasicauthusername) || StringUtils.isBlank(httpbasicauthpassword)) throw new Exception("User is missing for Basic Authentication.");

        if (method.getURI() == null) throw new Exception("URI is missing for Basic Authentication.");

        if (StringUtils.isNotBlank(httpbasicauthusername) && StringUtils.isNotBlank(httpbasicauthpassword) && method.getURI() != null)
        {
            String user = httpbasicauthusername + ":" + httpbasicauthpassword;
            method.addHeader("Authorization", "Basic " + Base64.encodeBytes(user.getBytes()));
            CredentialsProvider cp = new BasicCredentialsProvider();
            cp.setCredentials(new AuthScope(method.getURI().getHost(), method.getURI().getPort()), new UsernamePasswordCredentials(httpbasicauthusername, httpbasicauthpassword));
    
            httpclient.setDefaultCredentialsProvider(cp);
        }

    }

    private void setHeaderRedirect(HttpRequestBase method, CredentialsProvider cp) throws Exception
    {
        if (RestCaller.headerRedirEnable)
        {

            URI uri = method.getURI();
            if (StringUtils.isNotBlank(getHttpbasicauthusername()) && StringUtils.isNotBlank(getHttpbasicauthpassword()))
            {
                if (RestCaller.headerRedirEncAuth)
                {
                    URIBuilder builder = new URIBuilder(uri.toString());
                    builder.setUserInfo(getHttpbasicauthusername(), getHttpbasicauthpassword());

                    uri = builder.build();
                }
                else
                {

                    URI headerUri = URI.create(RestCaller.headerRedirURL);

                    cp.setCredentials(new AuthScope(headerUri.getHost(), headerUri.getPort()), new UsernamePasswordCredentials(getHttpbasicauthusername(), getHttpbasicauthpassword()));

                }
            }
            method.setHeader(RestCaller.headerRedirHeader, uri.toString());
            method.setURI(URI.create(RestCaller.headerRedirURL));
        }
    }

    /**
     * Makes HTTP OPTIONS method request on resource (base URL) to get list of
     * allowed methods.
     * 
     * @return List of allowed methods
     * @throws Exception
     */
    public List<String> optionsMethod() throws Exception
    {
        List<String> allowed = new ArrayList<String>();

        // Create method instance
        HttpOptions method = new HttpOptions(baseUrl);

        executeMethod(method);

        if (statusCode == HttpStatus.SC_OK && responseHeaders.containsKey(HttpHeaders.ALLOW))
        {
            allowed = StringUtils.stringToList(responseHeaders.get(HttpHeaders.ALLOW), ",");
        }

        return allowed;
    }

    /**
     * Makes HTTP HEAD method request on resource (base URL + suffix) to get
     * metainformation for GET method request.
     * 
     * @return Map of metainformation (name-value pairs)
     * @throws Exception
     */
    public Map<String, String> headMethod() throws Exception
    {
        Map<String, String> metaInfo = new HashMap<String, String>();

        // Create method instance
        HttpHead method = new HttpHead(baseUrl + urlSuffix);

        executeMethod(method);

        if (statusCode == HttpStatus.SC_OK)
        {
            metaInfo = new HashMap<String, String>(responseHeaders);
        }
        else
        {
            throw new Exception(statusCode + " " + reasonPhrase);
        }

        return metaInfo;
    }

    /**
     * Makes HTTP GET method request (base URL + suffix) to get resource(s).
     *
     * @param Id
     *            of the resource to get (optional)
     * @param Map
     *            of request parameters and values (optional)
     * @param Map
     *            of request headers (optional - by default Accept is set to
     *            application/json)
     * @return resource(s) in "Accept" format
     * @throws Exception
     */
    public String getMethod(String resourceId, Map<String, String> reqParams, Map<String, String> reqHeaders) throws Exception
    {
        // Add resourceId and request paremeters to URL
        StringBuilder urlBuilder = new StringBuilder(baseUrl + urlSuffix);

        if (StringUtils.isNotBlank(resourceId))
        {
            urlBuilder.append(urlBuilder.toString().endsWith("/") ? "" : "/");
            urlBuilder.append(StringUtils.utf8Conversion(resourceId));
        }

        if (reqParams != null && !reqParams.isEmpty())
        {
            urlBuilder.append("?").append(StringUtils.mapToUrl(reqParams));
        }

        // Create method instance
        HttpGet method = new HttpGet(urlBuilder.toString());

        // Set request headers
        Map<String, String> tReqHeaders = reqHeaders != null ? reqHeaders : new HashMap<String, String>();

        for (String reqHeaderName : tReqHeaders.keySet())
        {
            method.setHeader(reqHeaderName, StringUtils.getParam(tReqHeaders, reqHeaderName));
        }

        /*
         * Accept defaults to application/json and Cache-Control to no-cache, if
         * not specified
         */

        method.setHeader(HttpHeaders.ACCEPT, StringUtils.getParam(tReqHeaders, HttpHeaders.ACCEPT, "application/json"));
        method.setHeader(HttpHeaders.CACHE_CONTROL, StringUtils.getParam(tReqHeaders, HttpHeaders.CACHE_CONTROL, "no-cache"));

        String entity = executeMethod(method);

        if (statusCode != HttpStatus.SC_OK && statusCode != HttpStatus.SC_NOT_FOUND)
        {
            throw new RestCallException(statusCode, reasonPhrase, entity);
        }

        return entity;
    }

    /**
     * Makes HTTP GET method request (base URL + suffix) to get resource(s).
     *
     * @param Id
     *            of the resource to get (optional)
     * @param URL
     *            parameters (optional)
     * @param Map
     *            of request headers (optional - by default Accept is set to
     *            application/json)
     * @return resource(s) in "Accept" format
     * @throws Exception
     */
    public String getMethod(String resourceId, String urlParams, Map<String, String> reqHeaders) throws Exception
    {
        StringBuilder urlBuilder = new StringBuilder(baseUrl + urlSuffix);

        if (StringUtils.isNotBlank(resourceId))
        {
            urlBuilder.append(urlBuilder.toString().endsWith("/") ? "" : "/");
            urlBuilder.append(StringUtils.utf8Conversion(resourceId));
        }

        if (StringUtils.isNotBlank(urlParams))
        {
            urlBuilder.append("?").append(urlParams);
        }

        // Create method instance
        HttpGet method = new HttpGet(urlBuilder.toString());

        // Set request headers
        Map<String, String> tReqHeaders = reqHeaders != null ? reqHeaders : new HashMap<String, String>();

        for (String reqHeaderName : tReqHeaders.keySet())
        {
            method.setHeader(reqHeaderName, StringUtils.getParam(tReqHeaders, reqHeaderName));
        }

        /*
         * Accept defaults to application/json and Cache-Control to no-cache, if
         * not specified
         */

        method.setHeader(HttpHeaders.ACCEPT, StringUtils.getParam(tReqHeaders, HttpHeaders.ACCEPT, "application/json"));
        method.setHeader(HttpHeaders.CACHE_CONTROL, StringUtils.getParam(tReqHeaders, HttpHeaders.CACHE_CONTROL, "no-cache"));

        String entity = executeMethod(method);

        if (statusCode != HttpStatus.SC_OK && statusCode != HttpStatus.SC_NOT_FOUND)
        {
            throw new Exception(statusCode + " " + reasonPhrase + ":" + entity);
        }

        return entity;
    }

    /**
     * Makes HTTP GET method request to specified URL.
     *
     * @param url
     * @param Map
     *            of request headers (optional - by default Accept is set to
     *            application/json)
     * @throws Exception
     */
    public String getMethod(String url, Map<String, String> reqHeaders) throws Exception
    {
        // Create method instance
        HttpGet method = new HttpGet(url);

        // Set request headers
        Map<String, String> tReqHeaders = reqHeaders != null ? reqHeaders : new HashMap<String, String>();

        for (String reqHeaderName : tReqHeaders.keySet())
        {
            method.setHeader(reqHeaderName, StringUtils.getParam(tReqHeaders, reqHeaderName));
        }

        /*
         * Accept defaults to application/json and Cache-Control to no-cache, if
         * not specified
         */

        method.setHeader(HttpHeaders.ACCEPT, StringUtils.getParam(tReqHeaders, HttpHeaders.ACCEPT, "application/json"));
        method.setHeader(HttpHeaders.CACHE_CONTROL, StringUtils.getParam(tReqHeaders, HttpHeaders.CACHE_CONTROL, "no-cache"));

        String entity = executeMethod(method);

        if (statusCode != HttpStatus.SC_OK && statusCode != HttpStatus.SC_NOT_FOUND && statusCode != HttpStatus.SC_NO_CONTENT)
        {
            throw new Exception(statusCode + " " + reasonPhrase + ":" + entity);
        }

        return entity;
    }

    /**
     * Makes HTTP POST method request (base URL + suffix) to create resource.
     *
     * @param Map
     *            of request parameters and values (optional)
     * @param Map
     *            of request headers (optional - Content-Type and Accept is
     *            forced to application/json)
     * @param Map
     *            of resource properties in name-value format (optional)
     * @return Newly created resource. Resource location is in "Location"
     *         (HTTPHeaders.LOCATION) response header.
     * @throws Exception
     */
    public String postMethod(Map<String, String> reqParams, Map<String, String> reqHeaders, Map<String, String> resourceProps) throws Exception
    {

        // Add request paremeters to URL
        StringBuilder urlBuilder = new StringBuilder(baseUrl + urlSuffix);

        if (reqParams != null && !reqParams.isEmpty())
        {
            urlBuilder.append("?").append(StringUtils.mapToUrl(reqParams));
        }

        // Create method instance
        HttpPost method = new HttpPost(urlBuilder.toString());

        // Content-Type and Accept is forced to application/json
        method.setHeader(HttpHeaders.CONTENT_TYPE, "application/json");
        method.setHeader(HttpHeaders.ACCEPT, "application/json");

        // Set request headers
        Map<String, String> tReqHeaders = reqHeaders != null ? reqHeaders : new HashMap<String, String>();

        for (String reqHeaderName : tReqHeaders.keySet())
        {
            method.setHeader(reqHeaderName, StringUtils.getParam(tReqHeaders, reqHeaderName));
        }

        if (resourceProps != null && !resourceProps.isEmpty())
        {
            // Set entity
            HttpEntity entity = new ByteArrayEntity(StringUtils.mapToJson(resourceProps).getBytes("utf-8"));
            method.setEntity(entity);
        }

        String responseEntity = executeMethod(method);

        if (statusCode != HttpStatus.SC_CREATED)
        {
            throw new RestCallException(statusCode, reasonPhrase, responseEntity);
            // throw new Exception(statusCode + " " + reasonPhrase + ":" +
            // StringUtils.mapToJson(resourceProps));
        }

        return responseEntity;
    }

    /**
     * Makes HTTP POST method request (base URL + suffix) to create resource.
     *
     * @param Map
     *            of request parameters and values (optional)
     * @param Map
     *            of request headers (optional - Content-Type and Accept is
     *            forced to application/json)
     * @param Map
     *            of List in name-value format to set in the body of request as
     *            Urlencoded form data(optional)
     * @return Newly created resource. Resource location is in "Location"
     *         (HTTPHeaders.LOCATION) response header.
     * @throws Exception
     */
    public String callPost(Map<String, String> reqParams, Map<String, String> reqHeaders, List<NameValuePair> urlEncodedFormData, Collection<Integer> statusCodes) throws Exception
    {

        // Add request paremeters to URL
        StringBuilder urlBuilder = new StringBuilder(baseUrl + urlSuffix);

        if (reqParams != null && !reqParams.isEmpty())
        {
            urlBuilder.append("?").append(StringUtils.mapToUrl(reqParams));
        }

        // Create method instance
        HttpPost method = new HttpPost(urlBuilder.toString());

        // Content-Type and Accept is forced to application/json
        method.setHeader(HttpHeaders.CONTENT_TYPE, "application/json");
        method.setHeader(HttpHeaders.ACCEPT, "application/json");

        // Set request headers
        Map<String, String> tReqHeaders = reqHeaders != null ? reqHeaders : new HashMap<String, String>();

        for (String reqHeaderName : tReqHeaders.keySet())
        {
            method.setHeader(reqHeaderName, StringUtils.getParam(tReqHeaders, reqHeaderName));
        }

        if (urlEncodedFormData != null && !urlEncodedFormData.isEmpty())
        {
            // Set entity
            UrlEncodedFormEntity entity = new UrlEncodedFormEntity(urlEncodedFormData, "UTF-8");
            method.setEntity(entity);
        }

        String responseEntity = executeMethod(method);

        Collection<Integer> expectedStatusCodes = new HashSet<Integer>();
        expectedStatusCodes.add(new Integer(HttpStatus.SC_CREATED));

        if (statusCodes != null && !statusCodes.isEmpty())
        {
            for (Integer statusCode : statusCodes)
            {
                expectedStatusCodes.add(statusCode);
            }
        }

        boolean validStatus = false;

        for (Integer expectedStatusCode : expectedStatusCodes)
        {
            if (statusCode == expectedStatusCode.intValue())
            {
                validStatus = true;
                break;
            }
        }

        if (!validStatus)
        {
            throw new RestCallException(statusCode, reasonPhrase, responseEntity);
            // throw new Exception(statusCode + " " + reasonPhrase + ":" +
            // StringUtils.mapToJson(resourceProps));
        }

        return responseEntity;
    }

    /**
     * Makes HTTP POST method request (base URL + suffix) to create resource.
     *
     * @param Map
     *            of request parameters and values (optional)
     * @param Map
     *            of request headers (optional - Accept forced to
     *            application/json, Content Type is forced to application/json
     *            if not specified)
     * @param Content
     * @param Collection
     *            of possible status codes (optional HTTP Status Code 201 is
     *            default)
     * @return Newly created resource. Resource location is in "Location"
     *         (HTTPHeaders.LOCATION) response header.
     * @throws Exception
     */
    public String postMethod(Map<String, String> reqParams, Map<String, String> reqHeaders, String content, Collection<Integer> statusCodes) throws Exception
    {
        // Add request paremeters to URL
        StringBuilder urlBuilder = new StringBuilder(baseUrl + urlSuffix);

        if (reqParams != null && !reqParams.isEmpty())
        {
            urlBuilder.append("?").append(StringUtils.mapToUrl(reqParams));
        }

        // Create method instance
        HttpPost method = new HttpPost(urlBuilder.toString());

        // Accept is forced to application/json
        method.setHeader(HttpHeaders.ACCEPT, "application/json");

        // Set request headers - Content Type should be in request headers, if
        // not defaults to application/json
        Map<String, String> tReqHeaders = reqHeaders != null ? reqHeaders : new HashMap<String, String>();

        for (String reqHeaderName : tReqHeaders.keySet())
        {
            method.setHeader(reqHeaderName, StringUtils.getParam(tReqHeaders, reqHeaderName));
        }

        Header[] contentTypes = method.getHeaders(HttpHeaders.CONTENT_TYPE);

        if (contentTypes == null || contentTypes.length == 0)
        {
            method.setHeader(HttpHeaders.CONTENT_TYPE, "application/json");
        }

        if (StringUtils.isNotBlank(content))
        {
            // Set entity
            HttpEntity entity = new ByteArrayEntity(content.getBytes("utf-8"));
            method.setEntity(entity);
        }

        String responseEntity = executeMethod(method);

        Collection<Integer> expectedStatusCodes = new HashSet<Integer>();
        expectedStatusCodes.add(new Integer(HttpStatus.SC_CREATED));

        if (statusCodes != null && !statusCodes.isEmpty())
        {
            for (Integer statusCode : statusCodes)
            {
                expectedStatusCodes.add(statusCode);
            }
        }

        boolean validStatus = false;

        for (Integer expectedStatusCode : expectedStatusCodes)
        {
            if (statusCode == expectedStatusCode.intValue())
            {
                validStatus = true;
                break;
            }
        }

        if (!validStatus)
        {
            throw new RestCallException(statusCode, reasonPhrase, responseEntity);
            // throw new Exception(statusCode + " " + reasonPhrase + ":" +
            // StringUtils.mapToJson(resourceProps));
        }

        return responseEntity;
    }

    /**
     * Makes HTTP PUT method request (base URL + suffix) to update specified
     * resource.
     *
     * @param Id
     *            of the resource to update
     * @param Map
     *            of request parameters and values (optional)
     * @param Map
     *            of request headers (optional - Content-Type and Accept is
     *            forced to application/json)
     * @param Map
     *            of resource properties to update in name-value format
     * @return Updated resource.
     * @throws Exception
     */
    public String putMethod(String resourceId, Map<String, String> reqParams, Map<String, String> reqHeaders, Map<String, String> resourceProps) throws Exception
    {
        if (resourceProps == null || resourceProps.isEmpty())
        {
            throw new Exception("Invalid arguments: Resource Id and/or Resource Properties to update are null or empty.");
        }

        // Add request paremeters to URL
        StringBuilder urlBuilder = new StringBuilder(baseUrl + urlSuffix);

        if (!resourceId.isEmpty())
        {
            urlBuilder.append(urlBuilder.toString().endsWith("/") ? "" : "/");
            urlBuilder.append(StringUtils.utf8Conversion(resourceId));
        }

        if (reqParams != null && !reqParams.isEmpty())
        {
            urlBuilder.append("?").append(StringUtils.mapToUrl(reqParams));
        }

        // Create method instance
        HttpPut method = new HttpPut(urlBuilder.toString());

        // Content-Type and Accept is forced to application/json
        method.setHeader(HttpHeaders.CONTENT_TYPE, "application/json");
        method.setHeader(HttpHeaders.ACCEPT, "application/json");

        // Set request headers
        Map<String, String> tReqHeaders = reqHeaders != null ? reqHeaders : new HashMap<String, String>();

        for (String reqHeaderName : tReqHeaders.keySet())
        {
            method.setHeader(reqHeaderName, StringUtils.getParam(tReqHeaders, reqHeaderName));
        }

        // Set entity
        HttpEntity entity = new ByteArrayEntity(StringUtils.mapToJson(resourceProps).getBytes("utf-8"));
        method.setEntity(entity);
        // sometime you receive status code as 401 and responseEntity as e.g
        // {"errorMessages":["You do not have the permission to see the
        // specified issue.","Login Required"],"errors":{}}
        // in that case it thorws exception and looses the actual message
        // receved in response entity. So adding one filed to RestCaller to hold
        // value of response entity
        responseEntity = executeMethod(method);
        if (statusCode != HttpStatus.SC_OK && statusCode != HttpStatus.SC_NO_CONTENT)
        {
            throw new Exception(statusCode + " " + reasonPhrase + ":" + (StringUtils.mapToJson(resourceProps)));
        }

        return responseEntity;
    }

    /**
     * Makes HTTP PUT method request (base URL + suffix) to update specified
     * resource.
     *
     * @param Id
     *            of the resource to update
     * @param Map
     *            of paired request parameters and values (optional)
     * @param Key
     *            prefix for paired map
     * @param Value
     *            prefix for paired map
     * @param Map
     *            of request parameters and values (optional)
     * @param Map
     *            of request headers (optional - Content-Type and Accept is
     *            forced to application/json)
     * @param Map
     *            of resource properties to update in name-value format
     * @return Updated resource.
     * @throws Exception
     */
    public String putMethod(String resourceId, Map<String, String> pairedReqParams, String keyPrefix, String valuePrefix, Map<String, String> reqParams, Map<String, String> reqHeaders, Map<String, String> resourceProps) throws Exception
    {
        if (resourceId == null || resourceId.isEmpty() || ((pairedReqParams == null || pairedReqParams.isEmpty()) && (resourceProps == null || resourceProps.isEmpty())))
        {
            throw new Exception("Invalid arguments: Resource Id and/or Resource Properties to update or " + "Paired Request Parameters specifying resource properties to update are null or empty.");
        }

        // Add request paremeters to URL
        StringBuilder urlBuilder = new StringBuilder(baseUrl + urlSuffix);

        urlBuilder.append(urlBuilder.toString().endsWith("/") ? "" : "/");
        urlBuilder.append(StringUtils.utf8Conversion(resourceId));

        boolean urlHasParams = false;

        if (pairedReqParams != null && !pairedReqParams.isEmpty())
        {
            urlHasParams = true;
            urlBuilder.append("?").append(StringUtils.pairedMapToUrl(pairedReqParams, keyPrefix, valuePrefix));
        }

        if (reqParams != null && !reqParams.isEmpty())
        {
            String separator = "?";

            if (urlHasParams)
            {
                separator = "&";
            }

            urlBuilder.append(separator).append(StringUtils.mapToUrl(reqParams));
        }

        // Create method instance
        HttpPut method = new HttpPut(urlBuilder.toString());

        // Content-Type and Accept is forced to application/json
        method.setHeader(HttpHeaders.CONTENT_TYPE, "application/json");
        method.setHeader(HttpHeaders.ACCEPT, "application/json");

        // Set request headers
        Map<String, String> tReqHeaders = reqHeaders != null ? reqHeaders : new HashMap<String, String>();

        for (String reqHeaderName : tReqHeaders.keySet())
        {
            method.setHeader(reqHeaderName, StringUtils.getParam(tReqHeaders, reqHeaderName));
        }

        if (resourceProps != null && !resourceProps.isEmpty())
        {
            // Set entity
            HttpEntity entity = new ByteArrayEntity(StringUtils.mapToJson(resourceProps).getBytes("utf-8"));
            method.setEntity(entity);
        }

        String responseEntity = executeMethod(method);
        this.responseEntity = responseEntity;

        if (statusCode != HttpStatus.SC_OK && statusCode != HttpStatus.SC_NO_CONTENT)
        {
            throw new Exception(statusCode + " " + reasonPhrase + ":" + (pairedReqParams != null && !pairedReqParams.isEmpty() ? " Paired Request Parameters " + pairedReqParams + ", " : "") + (resourceProps != null && !resourceProps.isEmpty() ? " Resource Properties " + StringUtils.mapToJson(resourceProps) : ""));
        }

        return responseEntity;
    }

    /**
     * Makes HTTP PATCH method request (base URL + suffix) to patch(update)
     * specified resource.
     *
     * @param Id
     *            of the resource to patch(update)
     * @param Map
     *            of request parameters and values (optional)
     * @param Map
     *            of request headers (optional - Content-Type and Accept is
     *            forced to application/json)
     * @param Map
     *            of resource properties to update in name-value format
     * @param Key
     *            prefix for paired map
     * @param Value
     *            prefix for paired map
     * @return Updated resource.
     * @throws Exception
     */
    public String patchMethod(String resourceId, Map<String, String> reqParams, Map<String, String> reqHeaders, Map<String, String> resourceProps) throws Exception
    {
        if (resourceId == null || resourceId.isEmpty() || resourceProps == null || resourceProps.isEmpty())
        {
            throw new Exception("Invalid arguments: Resource Id and/or Resource Properties to patch(update) are null or empty.");
        }

        // Add resource id and request paremeters to URL
        StringBuilder urlBuilder = new StringBuilder(baseUrl + urlSuffix);

        urlBuilder.append(urlBuilder.toString().endsWith("/") ? "" : "/");
        urlBuilder.append(StringUtils.utf8Conversion(resourceId));

        if (reqParams != null && !reqParams.isEmpty())
        {
            urlBuilder.append("?").append(StringUtils.mapToUrl(reqParams));
        }

        // Create method instance
        // HttpPatch method = new HttpPatch(urlBuilder.toString()); // Uncomment
        // when http client library version supports PATCH
        HttpPut method = new HttpPut(urlBuilder.toString()); // Comment when
                                                             // http client
                                                             // library version
                                                             // supports PATCH

        // Content-Type and Accept is forced to application/json
        method.setHeader(HttpHeaders.CONTENT_TYPE, "application/json");
        method.setHeader(HttpHeaders.ACCEPT, "application/json");

        // Set request headers
        Map<String, String> tReqHeaders = reqHeaders != null ? reqHeaders : new HashMap<String, String>();

        for (String reqHeaderName : tReqHeaders.keySet())
        {
            method.setHeader(reqHeaderName, StringUtils.getParam(tReqHeaders, reqHeaderName));
        }

        // Set entity
        HttpEntity entity = new ByteArrayEntity(StringUtils.mapToJson(resourceProps).getBytes("utf-8"));
        method.setEntity(entity);

        String responseEntity = executeMethod(method);

        if (statusCode != HttpStatus.SC_OK && statusCode != HttpStatus.SC_NO_CONTENT)
        {
            throw new Exception(statusCode + " " + reasonPhrase + ":" + StringUtils.mapToJson(resourceProps));
        }

        return responseEntity;
    }

    /**
     * Makes HTTP DELETE method request (base URL + suffix) to patch(update)
     * specified resource.
     *
     * @param Id
     *            of the resource to patch(update)
     * @param Map
     *            of request parameters and values (optional)
     * @param Map
     *            of request headers (optional - Content-Type and Accept is
     *            forced to application/json)
     * @param Map
     *            of resource properties to update in name-value format
     * @return Updated resource.
     * @throws Exception
     */
    public String deleteMethod(String resourceId) throws Exception
    {
        if (resourceId == null || resourceId.isEmpty())
        {
            throw new Exception("Invalid arguments: Resource Id to delete is null or empty.");
        }

        // Add resource id to URL
        StringBuilder urlBuilder = new StringBuilder(baseUrl + urlSuffix);
        urlBuilder.append(urlBuilder.toString().endsWith("/") ? "" : "/");

        urlBuilder.append(StringUtils.utf8Conversion(resourceId));

        // Create method instance
        HttpDelete method = new HttpDelete(urlBuilder.toString());

        // Content-Type and Accept is forced to application/json
        method.setHeader(HttpHeaders.CONTENT_TYPE, "application/json");
        method.setHeader(HttpHeaders.ACCEPT, "application/json");

        String responseEntity = executeMethod(method);

        if (statusCode != HttpStatus.SC_OK && statusCode != HttpStatus.SC_NO_CONTENT)
        {
            throw new RestCallException(statusCode, reasonPhrase, resourceId + ":" + responseEntity);
        }

        return responseEntity;
    }

    /**
     * Makes HTTP GET method request (base URL + suffix) to get resource(s).
     *
     * @param Id
     *            of the resource to get (optional)
     * @param Map
     *            of paired request parameters and values (optional)
     * @param Key
     *            prefix for paired map
     * @param Value
     *            prefix for paired map
     * @param Map
     *            of request parameters and values (optional)
     * @param Map
     *            of request headers (optional - by default Accept is set to
     *            application/json)
     * @return resource(s) in "Accept" format
     * @throws Exception
     */
    public String getMethod(String resourceId, Map<String, String> pairedReqParams, String keyPrefix, String valuePrefix, Map<String, String> reqParams, Map<String, String> reqHeaders) throws Exception
    {
        // Add resourceId and request paremeters to URL
        StringBuilder urlBuilder = new StringBuilder(baseUrl + urlSuffix);

        if (StringUtils.isNotBlank(resourceId))
        {
            urlBuilder.append(urlBuilder.toString().endsWith("/") ? "" : "/");
            urlBuilder.append(StringUtils.utf8Conversion(resourceId));
        }

        boolean urlHasParams = false;

        if (pairedReqParams != null && !pairedReqParams.isEmpty())
        {
            urlHasParams = true;
            urlBuilder.append("?").append(StringUtils.pairedMapToUrl(pairedReqParams, keyPrefix, valuePrefix));
        }

        if (reqParams != null && !reqParams.isEmpty())
        {
            String separator = "?";

            if (urlHasParams)
            {
                separator = "&";
            }

            urlBuilder.append(separator).append(StringUtils.mapToUrl(reqParams));
        }

        // Create method instance
        HttpGet method = new HttpGet(urlBuilder.toString());

        // Set request headers
        Map<String, String> tReqHeaders = reqHeaders != null ? reqHeaders : new HashMap<String, String>();

        for (String reqHeaderName : tReqHeaders.keySet())
        {
            method.setHeader(reqHeaderName, StringUtils.getParam(tReqHeaders, reqHeaderName));
        }

        /*
         * Accept defaults to application/json and Cache-Control to no-cache, if
         * not specified
         */

        method.setHeader(HttpHeaders.ACCEPT, StringUtils.getParam(tReqHeaders, HttpHeaders.ACCEPT, "application/json"));
        method.setHeader(HttpHeaders.CACHE_CONTROL, StringUtils.getParam(tReqHeaders, HttpHeaders.CACHE_CONTROL, "no-cache"));

        String entity = executeMethod(method);

        if (statusCode != HttpStatus.SC_OK && statusCode != HttpStatus.SC_NOT_FOUND)
        {
            throw new RestCallException(statusCode, reasonPhrase, entity);
        }

        return entity;
    }

    /**
     * Makes HTTP GET method request (base URL + suffix) to get resource(s).
     *
     * @param Id
     *            of the resource to get (optional)
     * @param List
     *            of request values (optional)
     * @param Key
     *            prefix for values
     * @param Map
     *            of request parameters and list of values (optional)
     * @param Map
     *            of request headers (optional - by default Accept is set to
     *            application/json)
     * @return resource(s) in "Accept" format
     * @throws Exception
     */
    public String getMethod(String resourceId, List<String> reqValues, String key, Map<String, List<String>> reqParams, Map<String, String> reqHeaders) throws Exception
    {
        // Add resourceId and request paremeters to URL
        StringBuilder urlBuilder = new StringBuilder(baseUrl + urlSuffix);

        if (StringUtils.isNotBlank(resourceId))
        {
            urlBuilder.append(urlBuilder.toString().endsWith("/") ? "" : "/");
            urlBuilder.append(StringUtils.utf8Conversion(resourceId));
        }

        boolean urlHasParams = false;

        if (reqValues != null && !reqValues.isEmpty())
        {
            urlHasParams = true;
            urlBuilder.append("?").append(StringUtils.listToUrl(reqValues, key));
        }

        if (reqParams != null && !reqParams.isEmpty())
        {
            for (String reqParamKey : reqParams.keySet())
            {
                for (String reqParamVal : reqParams.get(reqParamKey))
                {
                    String separator = urlHasParams ? "&" : "?";
                    Map<String, String> reqParamMap = new HashMap<String, String>();
                    reqParamMap.put(reqParamKey, reqParamVal);
                    urlBuilder.append(separator).append(StringUtils.mapToUrl(reqParamMap));
                    urlHasParams = true;
                }
            }
        }

        // Create method instance
        HttpGet method = new HttpGet(urlBuilder.toString());

        // Set request headers
        Map<String, String> tReqHeaders = reqHeaders != null ? reqHeaders : new HashMap<String, String>();

        for (String reqHeaderName : tReqHeaders.keySet())
        {
            method.setHeader(reqHeaderName, StringUtils.getParam(tReqHeaders, reqHeaderName));
        }

        /*
         * Accept defaults to application/json and Cache-Control to no-cache, if
         * not specified
         */

        method.setHeader(HttpHeaders.ACCEPT, StringUtils.getParam(tReqHeaders, HttpHeaders.ACCEPT, "application/json"));
        method.setHeader(HttpHeaders.CACHE_CONTROL, StringUtils.getParam(tReqHeaders, HttpHeaders.CACHE_CONTROL, "no-cache"));

        String entity = executeMethod(method);

        if (statusCode != HttpStatus.SC_OK && statusCode != HttpStatus.SC_NOT_FOUND)
        {
            throw new RestCallException(statusCode, reasonPhrase, entity);
        }

        return entity;
    }

    /**
     * Makes HTTP POST method request (base URL + suffix) to create resource.
     *
     * @param Map
     *            of request parameters and values (optional)
     * @param Map
     *            of request headers (optional - Accept forced to
     *            application/json, Content Type is forced to application/json
     *            if not specified)
     * @param Content
     * @param Map
     *            of paired name value request parameters
     * @param keyPrefix
     * @param valuePrefix
     * @param Collection
     *            of possible status codes (optional HTTP Status Code 201 is
     *            default)
     * @return Newly created resource. Resource location is in "Location"
     *         (HTTPHeaders.LOCATION) response header.
     * @throws Exception
     */
    public String postMethod(Map<String, String> reqParams, Map<String, String> reqHeaders, String content, Map<String, String> pairedReqParams, String keyPrefix, String valuePrefix, Collection<Integer> statusCodes) throws Exception
    {
        // Add request paremeters to URL
        StringBuilder urlBuilder = new StringBuilder(baseUrl + urlSuffix);

        boolean hasReqParams = false;

        if (reqParams != null && !reqParams.isEmpty())
        {
            urlBuilder.append("?").append(StringUtils.mapToUrl(reqParams));
            hasReqParams = true;
        }

        if (pairedReqParams != null && !pairedReqParams.isEmpty())
        {
            String separator = hasReqParams ? "&" : "?";
            urlBuilder.append(separator).append(StringUtils.pairedMapToUrl(pairedReqParams, keyPrefix, valuePrefix));
        }
        // Create method instance
        HttpPost method = new HttpPost(urlBuilder.toString());

        // Accept is forced to application/json
        method.setHeader(HttpHeaders.ACCEPT, "application/json");

        // Set request headers - Content Type should be in request headers, if
        // not defaults to application/json
        Map<String, String> tReqHeaders = reqHeaders != null ? reqHeaders : new HashMap<String, String>();

        for (String reqHeaderName : tReqHeaders.keySet())
        {
            method.setHeader(reqHeaderName, StringUtils.getParam(tReqHeaders, reqHeaderName));
        }

        Header[] contentTypes = method.getHeaders(HttpHeaders.CONTENT_TYPE);

        if (contentTypes == null || contentTypes.length == 0)
        {
            method.setHeader(HttpHeaders.CONTENT_TYPE, "application/json");
        }

        if (StringUtils.isNotBlank(content))
        {
            // Set entity
            HttpEntity entity = new ByteArrayEntity(content.getBytes("utf-8"));
            method.setEntity(entity);
        }

        String responseEntity = executeMethod(method);

        Collection<Integer> expectedStatusCodes = new HashSet<Integer>();
        expectedStatusCodes.add(new Integer(HttpStatus.SC_CREATED));

        if (statusCodes != null && !statusCodes.isEmpty())
        {
            for (Integer statusCode : statusCodes)
            {
                expectedStatusCodes.add(statusCode);
            }
        }

        boolean validStatus = false;

        for (Integer expectedStatusCode : expectedStatusCodes)
        {
            if (statusCode == expectedStatusCode.intValue())
            {
                validStatus = true;
                break;
            }
        }

        if (!validStatus)
        {
            throw new RestCallException(statusCode, reasonPhrase, responseEntity);
            // throw new Exception(statusCode + " " + reasonPhrase + ":" +
            // StringUtils.mapToJson(resourceProps));
        }

        return responseEntity;
    }

    private static TrustManager[] trustAllCertificates()
    {
        // Create a trust manager that does not validate certificate chains
        TrustManager[] trustAllCerts = new TrustManager[] { new X509TrustManager()
        {
            public X509Certificate[] getAcceptedIssuers()
            {
                return new X509Certificate[0];
            }

            public void checkClientTrusted(X509Certificate[] certs, String authType)
            {
            }

            public void checkServerTrusted(X509Certificate[] certs, String authType)
            {
            }
        } };

        return trustAllCerts;
    }

}
