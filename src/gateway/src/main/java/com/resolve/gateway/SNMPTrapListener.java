/******************************************************************************
* (C) Copyright 2014
*
* Resolve Systems, LLC
*
* All rights reserved
* This software is distributed under license and may not be used, copied,
* modified, or distributed without the express written permission of
* Resolve Systems, LLC.
*
******************************************************************************/

package com.resolve.gateway;

import java.io.PipedReader;
import java.io.PipedWriter;
import java.io.PrintWriter;
import java.net.InetAddress;
import java.util.Hashtable;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import snmp.SNMPObject;
import snmp.SNMPTrapReceiverInterface;
import snmp.SNMPv1TrapListener;
import snmp.SNMPv1TrapPDU;
import snmp.SNMPv2TrapListener;
import snmp.SNMPv2TrapPDU;

import com.resolve.esb.MMsgHeader;
import com.resolve.rsbase.MainBase;
import com.resolve.util.Constants;
import com.resolve.util.Log;

public class SNMPTrapListener implements SNMPv1TrapListener, SNMPv2TrapListener, Runnable
{
    SNMPTrapReceiverInterface listener;

    PipedReader errorReader;
    PipedWriter errorWriter;
    Thread readerThread;

    public SNMPTrapListener()
    {
        try
        {
            Log.log.info("Initializing SNMPTrapListener");

            errorReader = new PipedReader();
            errorWriter = new PipedWriter(errorReader);

            readerThread = new Thread(this);

            listener = new SNMPTrapReceiverInterface(new PrintWriter(errorWriter));
            listener.addv1TrapListener(this);
            listener.addv2TrapListener(this);
        }
        catch (Exception e)
        {
            listener = null;
            Log.log.warn("Failed to configure SNMP Trap Listener: " + e.getMessage(), e);
        }

    } // SNMPTrapListener

    public void run()
    {
        int numChars;
        char[] charArray = new char[256];

        try
        {
            while (!readerThread.isInterrupted() && ((numChars = errorReader.read(charArray, 0, charArray.length)) != -1))
            {
                String msg = "";
                msg += "Problem receiving trap or inform:\n";
                msg += new String(charArray, 0, numChars);
                msg += "\n\n";
                Log.log.debug(msg);
            }
        }
        catch (Exception e)
        {
            if (!readerThread.isInterrupted())
            {
                Log.log.error("Problem receiving SNMP trap: " + e.getMessage(), e);
            }
        }
    } // run

    public void start()
    {
        if (listener != null)
        {
            Log.log.info("Starting SNMP Trap Listener");
            readerThread.start();
            listener.startReceiving();
        }
    } // start

    public void stop() throws Exception
    {
        Log.log.info("Stopping SNMP Trap Listener");
        readerThread.interrupt();
        readerThread.stop();
        listener.stopReceiving();
        Log.log.info("Stopped SNMP Trap Listener");
    } // stop

    public void processv1Trap(SNMPv1TrapPDU pdu, String communityName)
    {
        try
        {
            String msg = "Received SNMPv1 trap:\n";

            msg += "  community name:     " + communityName + "\n";
            msg += "  enterprise OID:     " + pdu.getEnterpriseOID().toString() + "\n";
            msg += "  agent address:      " + pdu.getAgentAddress().toString() + "\n";
            msg += "  generic trap:       " + pdu.getGenericTrap() + "\n";
            msg += "  specific trap:      " + pdu.getSpecificTrap() + "\n";
            msg += "  timestamp:          " + pdu.getTimestamp() + "\n";
            msg += "  supplementary vars: " + pdu.getVarBindList().toString() + "\n";
            msg += "\n";
            Log.log.trace(msg);

            Hashtable data = new Hashtable();
            data.put("ENTERPRISE", pdu.getEnterpriseOID().toString());
            data.put("GENERIC", pdu.getGenericTrap());
            data.put("SPECIFIC", pdu.getSpecificTrap());

            List varbinds = (List) pdu.getVarBindList().getValue();
            int idx = 0;
            for (Iterator i = varbinds.iterator(); i.hasNext();)
            {
                SNMPObject obj = (SNMPObject) i.next();

                // extract value from formatted string
                String formatted = obj.toString();
                int pos = formatted.indexOf(' ', 2);
                if (pos != -1)
                {
                    String value = formatted.substring(pos + 2, formatted.length() - 2);

                    Log.log.trace("VARBIND" + idx + ":" + value);

                    // normalize format - remove carriage return (\r)
                    value = value.replaceAll("\r", "");

                    data.put("VARBIND" + idx, value);
                }
                idx++;
            }

            // send results
            sendResult(data);
        }
        catch (Exception e)
        {
            Log.log.error("Failed to process SNMPv1 Trap: " + e.getMessage(), e);
        }

    } // processv1Trap

    public void processv2Trap(SNMPv2TrapPDU pdu, String communityName, InetAddress agentIPAddress)
    {
        String msg = "Received SNMPv2 trap:\n";

        msg += "  agent IP address:   " + agentIPAddress.getHostAddress() + "\n";
        msg += "  community name:     " + communityName + "\n";
        msg += "  system uptime:      " + pdu.getSysUptime().toString() + "\n";
        msg += "  trap OID:           " + pdu.getSNMPTrapOID().toString() + "\n";
        msg += "  var bind list:      " + pdu.getVarBindList().toString() + "\n";
        msg += "\n";
        msg += "SNMPv2 Trap processing not currently supported\n";
        Log.log.trace(msg);
    } // processv2Trap

    void sendResult(Map content)
    {
        try
        {
            // define result header
            MMsgHeader header = new MMsgHeader();
            header.setEventType(Constants.GATEWAY_EVENT_TYPE_SNMP);
            header.setRouteDest(Constants.ESB_NAME_RSSERVER, "MAction.eventResult");

            // send message
            MainBase.esb.sendMessage(header, content);
        }
        catch (Exception e)
        {
            Log.log.warn(e.getMessage(), e);
        }
    } // sendResult

} // SNMPTrapListener
