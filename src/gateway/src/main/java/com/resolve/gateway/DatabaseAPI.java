/******************************************************************************
 * (C) Copyright 2014
 *
 * Resolve Systems, LLC
 *
 * All rights reserved This software is distributed under license and may not be
 * used, copied, modified, or distributed without the express written permission
 * of Resolve Systems, LLC.
 *
 ******************************************************************************/

package com.resolve.gateway;

import java.sql.Connection;
import java.util.Map;

import org.apache.commons.pool.impl.GenericObjectPool;

import com.resolve.rsbase.MainBase;
import com.resolve.gateway.database.DBGateway;
import com.resolve.gateway.database.DBGatewayFactory;
import com.resolve.util.StringUtils;

public class DatabaseAPI
{
    private static DBGateway instance = DBGatewayFactory.getInstance();

    /**
     * Uses the given name to retrieve a {@link Map} of properties from the
     * current DBGateway instance's {@link NameProperties} object. If
     * the {@link Map} isn't null, it returns the {@link Object} associated with
     * the given key parameter.
     * 
     * @param name
     *            the key associated with the name property {@link Map} in
     *            DBGateway's {@link NameProperties} object. Cannot be
     *            null or empty.
     * @param key
     *            the key in the property {@link Map} returned from
     *            DBGateway's {@link NameProperties}
     * @return the {@link Object} value associated with the given key in the
     *         property {@link Map} from DBGateway's
     *         {@link NameProperties} object
     */
    public static Object getNameObject(String name, String key)
    {
        return instance.getNameProperties().getNameObject(name, key);
    }

    /**
     * Uses the given name to retrieve a {@link Map} of properties from the
     * current DBGateway instance's {@link NameProperties} object. If
     * the {@link Map} isn't null, it adds the given key and value parameters.
     * 
     * @param name
     *            the key associated with the name property {@link Map} in
     *            DBGateway's {@link NameProperties} object. Cannot be
     *            null or empty.
     * @param key
     *            the key in the property {@link Map} returned from
     *            DBGateway's {@link NameProperties}
     * @param value
     *            the value to associate with the given key parameter
     * @throws Exception
     */
    public static void setNameObject(String name, String key, Object value) throws Exception
    {
        Map nameProperties = getNameProperties(name);
        if (nameProperties != null)
        {
            nameProperties.put(key, value);
            setNameProperties(name, nameProperties, true);
        }
    }

    /**
     * Uses the given name parameter to retrieve a {@link Map} of properties
     * from the current DBGateway instance's {@link NameProperties}
     * object.
     *
     * @param name
     *            the key associated with the name property {@link Map} in
     *            DBGateway's {@link NameProperties} object. Cannot be
     *            null or empty.
     * @return the Map associated with the name parameter in the current
     *         DBGateway instance's {@link NameProperties} object.
     * @throws Exception
     */
    public static Map getNameProperties(String name) throws Exception
    {
        return instance.getNameProperties().getNameProperties(name);
    }

    /**
     * Maps the given name to the given properties Map in the current
     * DBGateway instance's {@link NameProperties} object. This change
     * will be pushed onto the RSControl for persistence.
     *
     * @param name
     *            the key associated with the name property {@link Map} in
     *            DBGateway's {@link NameProperties} object. Cannot be
     *            null or empty.
     * @param properties
     *            a {@link Map} of DBGateway properties to add to the current
     *            DBGateway instance's {@link NameProperties} object.
     * @throws Exception
     */
    public static void setNameProperties(String name, Map properties) throws Exception
    {
        setNameProperties(name, properties, true);
    }

    /**
     * Maps the given name to the given properties Map in the current
     * DBGateway instance's {@link NameProperties} object. The doSave
     * parameter allows you to decide whether this change is pushed to RSControl
     * for persistence
     *
     * @param name
     *            the key associated with the name property {@link Map} in
     *            DBGateway's {@link NameProperties} object. Cannot be
     *            null or empty.
     * @param properties
     *            a {@link Map} of DBGateway properties to add to the current
     *            DBGateway instance's {@link NameProperties} object.
     * @param doSave
     *            a boolean value which designates whether this change should be
     *            pushed onto RSControl for persistence
     * @throws Exception
     */
    public static void setNameProperties(String name, Map properties, boolean doSave) throws Exception
    {
        if (StringUtils.isBlank(name))
        {
            throw new Exception("'name' parameter must be passed");
        }
        else
        {
            instance.setNameProperties(name, properties, doSave);
        }
    }

    /**
     * Uses the given name parameter to remove a {@link Map} of properties from
     * the current DBGateway instance's {@link NameProperties} object.
     *
     * @param name
     *            the key associated with the name property {@link Map} in
     *            DBGateway's {@link NameProperties} object. Cannot be
     *            null or empty.
     * @throws Exception
     */
    public static void removeNameProperties(String name) throws Exception
    {
        if (StringUtils.isBlank(name))
        {
            throw new Exception("'name' parameter must be passed");
        }
        else
        {
            instance.removeNameProperties(name);
        }
    }

    /**
     * Sends the current MainBase configuration to the RSRemote for persistence.
     *
     * @throws Exception
     */
    public static void saveConfig() throws Exception
    {
        MainBase.main.callSaveConfig();
    }

    /**
     * Returns the URL of the given {@link Connection} pool name
     * 
     * @param poolName
     *            the name of the {@link Connection} pool
     * @return the URL of the {@link Connection} pool
     */
    public static String getURL(String poolName)
    {
        return instance.getURL(poolName);
    } // getURL

    /**
     * Returns a initialized {@link Connection} pool associated with the given
     * 
     * @param poolName parameter from the current DatabaseGateway instance.
     *            the name of the connection pool
     * @return an Apache GenericObjectPool&lt;{@link Connection}&gt; object
     */
    public static GenericObjectPool<Connection> getConnectionPool(String poolName)
    {
        return instance.getConnectionPool(poolName);
    } // getConnectionPool

    /**
     * Returns the number of active database {@link Connection}s in the
     * connection pool associated with the given poolName parameter
     * 
     * @param poolName
     *            the name of the database {@link Connection} pool
     * @return the number of active connections in the database
     *         {@link Connection} connection pool
     */
    public static int getNumActiveConnections(String poolName)
    {
        return instance.getNumActiveConnections(poolName);
    } // getNumActiveConnections

    /**
     * Returns the maximum number of database {@link Connection}s in the
     * connection pool associated with the given poolName parameter
     * 
     * @param poolName
     *            the name of the database {@link Connection} pool
     * @return the maximum number of connections in the database
     *         {@link Connection} pool
     */
    public static int getNumMaxConnections(String poolName)
    {
        return instance.getNumMaxConnections(poolName);
    } // getNumMaxConnections

    /**
     * Returns an active database {@link Connection} from the connection pool
     * associated with the given poolName parameter
     * 
     * @param poolName
     *            the name of the database {@link Connection} pool
     * @return a database {@link Connection} object
     */
    public static Connection getConnection(String poolName) throws Exception
    {
        return instance.getConnection(poolName);
    } // getConnection

    /**
     * Returns an {@link Connection} object to the connection pool associated
     * with the given poolName parameter
     * 
     * @param poolName
     *            the name of the database {@link Connection} pool
     * @param conn
     *            {@link Connection} object to return to the pool
     */
    public static void returnConnection(String poolName, Connection conn) throws Exception
    {
        instance.returnConnection(poolName, conn);
    } // returnConnection

    /**
     * Executes the following SQL statement on the {@link Connection} pool
     * associated with the given poolName parameter.
     * 
     * @param poolName
     *            the name of the database {@link Connection} pool
     * @param sql
     *            the SQL statement to execute
     * @return either (1) the row count for SQL Data Manipulation Language (DML)
     *         statements or (2) 0 for SQL statements that return nothing
     * @throws Exception
     *             if an error occurs while executing the SQL statement
     */
    public static int update(String poolName, String sql) throws Exception
    {
        return instance.update(poolName, sql);
    } // update
} // DatabaseAPI
