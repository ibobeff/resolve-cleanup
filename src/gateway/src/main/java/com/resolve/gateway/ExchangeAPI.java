/******************************************************************************
 * (C) Copyright 2014
 *
 * Resolve Systems, LLC
 *
 * All rights reserved This software is distributed under license and may not be
 * used, copied, modified, or distributed without the express written permission
 * of Resolve Systems, LLC.
 *
 ******************************************************************************/

package com.resolve.gateway;

import java.util.HashMap;
import java.util.Map;

import com.resolve.rsbase.MainBase;
import com.resolve.util.Log;
import com.resolve.util.StringUtils;

public class ExchangeAPI
{
    public static ExchangeMessage createMessage()
    {
        return new ExchangeMessage();
    }

    /**
     * Sends the given ExchangeMessage object through the ExchangeGateway.
     * @param message the ExchangeMessage object
     */
    public static void sendMessage(ExchangeMessage message)
    {
        message.buildAttachement();
        message.buildEmailList();
        MExchange.sendMessage(message.getMessage());
        // message.reset();

    } // sendMessage

    /**
     * Moves an email associated with the given emailID parameter to the given
     * foldername and subfoldername.
     * 
     * @param emailID
     *            the email ID
     * @param foldername
     *            the name of the destination folder
     * @param subfoldername
     *            the name of the subfolder in the destination folder
     * @return a boolean value stating whether the action was successful
     */
    public static synchronized boolean moveEmailToFolder(String emailID, String foldername, String subfoldername)
    {
        boolean results = false;

        String[] emailIDS = new String[1];

        if (emailID != null && !emailID.equals(""))
        {
            emailIDS[0] = emailID;

            results = new MExchange().moveEmailToFolder(emailIDS, foldername, subfoldername);
        }
        else
        {
            Log.log.info("moveEmailToFolder the emailID is null");
        }

        return results;
    }

    /**
     * Moves all emails associated with the given emailID array parameter to the
     * given foldername and subfoldername.
     * 
     * @param emailIDs
     *            an array of email IDs
     * @param foldername
     *            the name of the destination folder
     * @param subfoldername
     *            the name of the subfolder in the destination folder
     * @return a boolean value stating whether the action was successful
     */
    public static synchronized boolean moveEmailToFolder(String[] emailIDs, String foldername, String subfoldername)
    {
        return new MExchange().moveEmailToFolder(emailIDs, foldername, subfoldername);
    }

    /**
     * Uses the given name parameter to retrieve a {@link Map} of properties
     * from the current ExchangeGateway instance's {@link NameProperties}
     * object.
     *
     * @param name
     *            the key associated with the name property {@link Map} in
     *            ExchangeGateway's {@link NameProperties} object. Cannot
     *            be null or empty.
     * @return the Map associated with the name parameter in the current
     *         ExchangeGateway instance's {@link NameProperties} object.
     * @throws Exception
     */
    public static Map getNameProperties(String name) throws Exception
    {
        return new MExchange().getNameProperties(name);
    }

    /**
     * Inserts the given name parameter to the given properties {@link Map} with
     * an appropriate key. Then sets the ExchangeGateway's
     * {@link NameProperties} properties parameter to the given properties Map
     * parameter
     *
     * @param name
     *            The name associated with the NAME_PROPERTIES key. The value of
     *            the name cannot be null or empty.
     * @param properties
     *            A Map of properties. The ExchangeGateway's
     *            {@link NameProperties} object. If null, it will store empty
     *            properties.
     * @throws Exception
     */
    public static void setNameProperties(String name, Map properties) throws Exception
    {
        if (StringUtils.isBlank(name))
        {
            throw new Exception("'name' parameter must be passed");
        }
        else
        {
            properties.put("NAME_PROPERTIES", name);
            new MExchange().setNameProperties(properties);
        }
    }

    /**
     * Uses the given name parameter to remove a {@link Map} of properties from
     * the current ExchangeGateway instance's {@link NameProperties}
     * object.
     *
     * @param name
     *            the key associated with the name property {@link Map} in
     *            ExchangeGateway's {@link NameProperties} object. Cannot be
     *            null or empty.
     * @throws Exception
     */
    public static void removeNameProperties(String name) throws Exception
    {
        if (StringUtils.isBlank(name))
        {
            throw new Exception("'name' parameter must be passed");
        }
        else
        {
            Map properties = new HashMap();
            properties.put("NAME_PROPERTIES", name);
            new MExchange().removeNameProperties(properties);
        }
    }

    /**
     * Saves the configurations in the RSRemote.
     *
     * @throws Exception
     */
    public static void saveConfig() throws Exception
    {
        MainBase.main.callSaveConfig();
    }
}
