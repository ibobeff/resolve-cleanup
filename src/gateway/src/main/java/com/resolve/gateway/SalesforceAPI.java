/******************************************************************************
 * (C) Copyright 2014
 *
 * Resolve Systems, LLC
 *
 * All rights reserved This software is distributed under license and may not be
 * used, copied, modified, or distributed without the express written permission
 * of Resolve Systems, LLC.
 *
 ******************************************************************************/

package com.resolve.gateway;

import java.util.List;
import java.util.Map;

import com.resolve.rsbase.MainBase;
import com.resolve.gateway.remedyx.RemedyxGateway;
import com.resolve.gateway.salesforce.SalesforceGateway;
import com.resolve.util.StringUtils;

public class SalesforceAPI
{
    private static SalesforceGateway instance = SalesforceGateway.getInstance();

    /**
     * Returns the {@link Object} value for the given key in the current
     * SalesforceGateway instance's NameProperty map.
     * 
     * @param name
     *            the name of the property {@link Map} in the
     *            SalesforceGateway instance's {@link NameProperties}
     *            object
     * @param key
     *            the key associated with the expected value in the property
     *            {@link Map} returned from the SalesforceGateway's
     *            {@link NameProperties} object
     * @return {@link Object} value associated with the key in the property
     *         {@link Map} returned by the SalesforceGateway instance's
     *         {@link NameProperties} object
     */
    public static Object getNameObject(String name, String key)
    {
        return instance.getNameProperties().getNameObject(name, key);
    }

    /**
     * Uses the given name parameter to return a property {@link Map} within the
     * SalesforceGateway instance's {@link NameProperties} object. If
     * the returned {@link Map} isn't null, it adds the given {@link String} key
     * and {@link Object} value to it.
     * 
     * @param name
     *            the name of the {@link Map} in the SalesforceGateway
     *            instance's {@link NameProperties} object. Cannot be null or
     *            empty.
     * @param key
     *            the key in the {@link Map} returned from the
     *            {@link NameProperties} object in the SalesforceGateway
     *            . Cannot be null or empty.
     * @param value
     *            the value associated with the given key in the {@link Map}
     *            returned from the {@link NameProperties} object in the
     *            SalesforceGateway
     * 
     * @throws Exception
     */
    public static void setNameObject(String name, String key, Object value) throws Exception
    {
        Map nameProperties = getNameProperties(name);
        if (nameProperties != null)
        {
            nameProperties.put(key, value);
            setNameProperties(name, nameProperties, true);
        }
    }

    /**
     * Uses the given {@link String} name as a key to return a {@link Map} of
     * properties from the SalesforceGateway instance's
     * {@link NameProperties} object.
     *
     * @param name
     *            The name associated with the properties {@link Map} in the
     *            SalesforceGateway's {@link NameProperties} object.
     *            Cannot be null or empty.
     * @return a Map of SalesforceGateway name properties
     * @throws Exception
     */
    public static Map getNameProperties(String name) throws Exception
    {
        return instance.getNameProperties().getNameProperties(name);
    }

    /**
     * Associates the given name to the given properties {@link Map} in the
     * SalesforceGateway instance's {@link NameProperties} object.
     *
     * @param name
     *            the key in the SalesforceGateway's
     *            {@link NameProperties} object. Cannot be null or empty.
     * @param properties
     *            A Map of properties to associate with the given name parameter
     * @throws Exception
     */
    public static void setNameProperties(String name, Map properties) throws Exception
    {
        setNameProperties(name, properties, true);
    }

    /**
     * Associates the given name to the given properties Map in the
     * SalesforceGateway instance's NameProperties object.
     *
     * @param name
     *            the key in the SalesforceGateway's {@link NameProperties}
     *            object. Cannot be null or empty.
     * @param properties
     *            A {@link Map} of properties to associate with the given name
     *            parameter
     * @param doSave
     *            a boolean value that designates whether to send this change to
     *            RSControl for persistence
     * @throws Exception
     */
    public static void setNameProperties(String name, Map properties, boolean doSave) throws Exception
    {
        if (StringUtils.isBlank(name))
        {
            throw new Exception("'name' parameter must be passed");
        }
        else
        {
            instance.setNameProperties(name, properties, doSave);
        }
    }

    /**
     * Removes a {@link Map} of properties associated with the given name in the
     * SalesforceGateway instance's {@link NameProperties} object.
     *
     * @param name
     *            The name associated with a property {@link Map} in the
     *            SalesforceGateway instance's {@link NameProperties}
     *            object. Cannot be null or empty.
     * @throws Exception
     */
    public static void removeNameProperties(String name) throws Exception
    {
        if (StringUtils.isBlank(name))
        {
            throw new Exception("'name' parameter must be passed");
        }
        else
        {
            instance.removeNameProperties(name);
        }
    }

    /**
     * Saves the configurations in the RSRemote.
     *
     * @throws Exception
     */
    public static void saveConfig() throws Exception
    {
        MainBase.main.callSaveConfig();
    }

    /**
     * Creates a new Salesforce Case.
     *
     * <pre>
     * {@code
     * try
     * {
     *      Map<String, String> params = new HashMap<String, String>();
     *      params.put("Description", "Description for the case");
     * 
     *      Map<String, String> result = SalesforceAPI.createCase(params, "admin", "admin");
     *      // The most important property is the NUMBER that you may need for future references.
     *      // This is how you get the NUMBER.
     *      String id = result.get("id");
     * 
     *      System.out.printf("Here is your id: %s for the case\n", id);
     *      System.out.printf("Now list all the properties and their values\n");
     *      for(String key : result.keySet())
     *      {
     *          System.out.printf("Property:%s, Value:%s\n", key, result.get(key));
     *      }
     * }
     * catch (Exception ex)
     * {
     *      //Do something good with this error.
     *      ex.printStackTrace();
     * }
     * }
     * </pre>
     *
     * @param params
     *            refer to the Salesforce documentation for more information
     *            about various properties you can set for a case. It may vary
     *            by installation.
     * @param username
     *            if not provided uses the gateway configuration.
     * @param password
     *            password corresponding to username including security token,
     *            appended to the standard password
     * @return a {@link Map} with all the properties for the cases.
     * @throws Exception
     */
    public static Map<String, String> createCase(Map<String, String> params, String username, String password) throws Exception
    {
        return instance.createCase(params, username, password);
    }

    /**
     * Creates work note for an Case.
     *
     * <pre>
     * {@code
     * try
     * {
     *      String parentId = "d71b3b41c0a8016700a8ef040791e72a";
     * 
     *      String note = "Work note desc";
     *      Salesforce.createCaseWorknote(parentId, note, "admin", "admin");
     * }
     * catch (Exception ex)
     * {
     *      //Do something good with this error.
     *      ex.printStackTrace();
     * }
     * }
     * </pre>
     *
     * @param parentId
     *            must be provided. It's the Case's id auto-generated by the
     *            system. The work log will be created for this Case.
     * @param note
     *            to be added to the case.
     * @param username
     *            if not provided uses the gateway configuration.
     * @param password
     *            password corresponding to username including security token,
     *            appended to the standard password.
     * @return true if work note is created successfully.
     * @throws Exception
     */
    public static boolean createCaseWorknote(String parentId, String note, String username, String password) throws Exception
    {
        return instance.createCaseWorknote(parentId, note, username, password);
    }

    /**
     * Updates a Salesforce case.
     *
     * Note: Salesforce may not allow update on certain properties, please refer
     * to the Salesforce documentation for more information. It may vary by the
     * installations.
     *
     * <pre>
     * {@code
     * try
     *  {
     *      String id = "d71b3b41c0a8016700a8ef040791e72a";
     * 
     *      Map<String, String> params = new HashMap<String, String>();
     *      params.put("description", "Changed the description for the case");
     *      Salesforce.updateCase(id, params, "admin", "admin");
     *  }
     *  catch (Exception ex)
     *  {
     *       //Do something good with this error.
     *       ex.printStackTrace();
     *  }
     *  }
     * </pre>
     *
     * @param id
     *            must be provided. It's the id auto-generated by the system.
     * @param params
     * @param username
     *            if not provided uses the gateway configuration.
     * @param password
     *            password corresponding to username including security token,
     *            appended to the standard password.
     * @throws Exception
     */
    public static void updateCase(String id, Map<String, String> params, String username, String password) throws Exception
    {
        instance.updateCase(id, params, username, password);
    }

    /**
     * Updates case status.
     *
     * Note: Salesforce may not allow changing status to any arbitrary value due
     * to business rules. Please refer to the documentation for more
     * information.
     *
     * <pre>
     * {@code
     * try
     * {
     *      //Case Update
     *      String updatedStatus = "Working"; //Resolved
     *      String id = "d71b3b41c0a8016700a8ef040791e72a";
     * 
     *      Salesforce.updateCaseStatus(id, updatedStatus, "admin", "admin");
     * }
     * catch (Exception ex)
     * {
     *      //Do something good with this error.
     *      ex.printStackTrace();
     * }
     * }
     * </pre>
     *
     * @param id
     *            must be provided. It's the id auto-generated by the system.
     * @param status
     *            is currently a number in String form and could be one of the
     *            following: New Working Escalated
     * @param username
     * @param password
     *            : password corresponding to username including security token,
     *            appended to the standard password
     * @throws Exception
     */
    public static void updateCaseStatus(String id, String status, String username, String password) throws Exception
    {
        instance.updateCaseStatus(id, status, username, password);
    }

    /**
     * Deletes a Salesforce case.
     *
     * Note: Salesforce may not allow deleting an case due to certain
     * dependencies (e.g., status etc.). Please refer to the documentation.
     *
     * <pre>
     * {@code
     * try
     *  {
     *      String id = "d71b3b41c0a8016700a8ef040791e72a";
     *      Salesforce.deleteCase(id, "admin", "admin");
     *  }
     *  catch (Exception ex)
     *  {
     *       //Do something good with this error.
     *       ex.printStackTrace();
     *  }
     * }
     * </pre>
     *
     * @param id
     *            must be provided. It's the id auto-generated by the system.
     * @param username
     *            if not provided uses the gateway configuration.
     * @param password
     *            password corresponding to username including security token,
     *            appended to the standard password.
     * @throws Exception
     */
    public static void deleteCase(String id, String username, String password) throws Exception
    {
        instance.deleteCase(id, username, password);
    }

    /**
     * Selects Salesforce case based on the id.
     *
     * <pre>
     * {@code
     * try
     * {
     *      String id = "d71b3b41c0a8016700a8ef040791e72a";
     *      Map<String, String> result = Salesforce.selectCaseBySysId(id, "admin", "admin");
     *      for(String key : result.keySet())
     *      {
     *          System.out.printf("Property:%s, Value:%s\n", key, result.get(key));
     *      }
     *  }
     *  catch (Exception ex)
     *  {
     *      //Do something good with this error.
     *      ex.printStackTrace();
     *  }
     *  }
     * </pre>
     *
     * @param id
     *            must be provided. It's the id auto-generated by the system.
     * @param username
     *            if not provided uses the gateway configuration.
     * @param password
     *            password corresponding to username including security token,
     *            appended to the standard password.
     * @return a {@link Map} that contains properties of the case.
     * @throws Exception
     */
    public static Map<String, String> selectCaseBySysId(String id, String username, String password) throws Exception
    {
        return instance.selectCaseBySysId(id, username, password);
    }

    /**
     * Selects Salesforce case based on the case number.
     *
     * <pre>
     * {@code
     * try
     * {
     *      String number = "00001027";
     *      Map<String, String> result = Salesforce.selectCaseByNumber(number, "admin", "admin");
     *      for(String key : result.keySet())
     *      {
     *          System.out.printf("Property:%s, Value:%s\n", key, result.get(key));
     *      }
     *  }
     *  catch (Exception ex)
     *  {
     *      //Do something good with this error.
     *      ex.printStackTrace();
     *  }
     *  }
     * </pre>
     *
     * @param number
     *            must be provided. It's the number auto-generated by the
     *            system.
     * @param username
     *            if not provided uses the gateway configuration.
     * @param password
     *            password corresponding to username including security token,
     *            appended to the standard password.
     * @return a {@link Map} that contains properties of the case.
     * @throws Exception
     */
    public static Map<String, String> selectCaseByNumber(String number, String username, String password) throws Exception
    {
        return instance.selectCaseByNumber(number, username, password);
    }

    /**
     * Searches Salesforce case(s) based on the query.
     *
     * To learn more about how to write Salesforce query, refer to:
     *
     * http://www.salesforce.com/us/developer/docs/soql_sosl/index.htm
     *
     * <pre>
     * {@code
     *  try
     *  {
     *      String query = "SELECT Id, CaseNumber, Subject, Description, Priority from Case where Status = 'New'";
     *      List<Map<String, String>> cases = Salesforce.searchCase(query, null, null);
     * 
     *      for(Map<String, String> case : cases)
     *      {
     *          int i = 1;
     *          System.out.printf("Case# %d:", i++);
     *          for(String key : case.keySet())
     *          {
     *              System.out.printf("Property:%s, Value:%s\n", key, case.get(key));
     *          }
     *      }
     *  }
     *  catch (Exception ex)
     *  {
     *      //Do something good with this error.
     *      ex.printStackTrace();
     *  }
     *  }
     * </pre>
     *
     * @param query
     *
     * @param username
     *            if not provided uses the gateway configuration.
     * @param password
     *            password corresponding to username including security token,
     *            appended to the standard password.
     * @return a {@link List} of {@link Map} which contain properties for all
     *         the tickets.
     * @throws Exception
     */
    public static List<Map<String, String>> searchCase(String query, String username, String password) throws Exception
    {
        return instance.searchCase(query, username, password);
    }
}
