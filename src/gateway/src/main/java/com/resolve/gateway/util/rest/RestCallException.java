/******************************************************************************
* (C) Copyright 2014
*
* Resolve Systems, LLC
*
* All rights reserved
* This software is distributed under license and may not be used, copied,
* modified, or distributed without the express written permission of
* Resolve Systems, LLC.
*
******************************************************************************/

package com.resolve.gateway.util.rest;

public class RestCallException extends Exception
{
    private static final long serialVersionUID = 3393545629903150101L;
    public int statusCode;
    public String reasonPhrase;
    public String responseEntity;
    
    public RestCallException(int statusCodeTmp, String reasonPhraseTmp, String responseEntityTmp)
    {
        super("Status Code : [" + statusCodeTmp + "], Reason Phrase : [" + reasonPhraseTmp + "], Response Entity : [" + responseEntityTmp + "]");
        this.statusCode = statusCodeTmp;
        this.reasonPhrase = reasonPhraseTmp;
        this.responseEntity = responseEntityTmp;        
    }
}