/******************************************************************************
* (C) Copyright 2014
*
* Resolve Systems, LLC
*
* All rights reserved
* This software is distributed under license and may not be used, copied,
* modified, or distributed without the express written permission of
* Resolve Systems, LLC.
*
******************************************************************************/

package com.resolve.gateway;

import java.util.List;
import java.util.Map;

import com.resolve.rsbase.MainBase;
import com.resolve.gateway.ews.EWSGateway;
import com.resolve.util.StringUtils;

public class EWSAPI
{
    private static EWSGateway instance = EWSGateway.getInstance();

    /**
     * Returns a {@link Object} for a specific key in a name property.
     * 
     * @param name
     * @param key
     * @return
     */
    public static Object getNameObject(String name, String key)
    {
        return instance.getNameProperties().getNameObject(name, key);
    }

    /**
     * Sets name object in a name property.
     * 
     * @param name of the nameproperty
     * @param key
     * @param value
     * @throws Exception 
     */
    public static void setNameObject(String name, String key, Object value) throws Exception
    {
        Map nameProperties = getNameProperties(name);
        if(nameProperties != null)
        {
            nameProperties.put(key, value);
            setNameProperties(name, nameProperties, true);
        }
    }
    
    /**
     * Returns a {@link Map} of properties for a specific name property.
     *
     * @param name cannot be null or empty.
     * @return
     * @throws Exception
     */
    public static Map getNameProperties(String name) throws Exception
    {
        return instance.getNameProperties().getNameProperties(name);
    }

    /**
     * Sets properties for a specific name property.
     *
     * @param name cannot be null or empty.
     * @param properties if null it'll store empty properties.
     * @throws Exception
     */
    public static void setNameProperties(String name, Map properties) throws Exception
    {
        setNameProperties(name, properties, true);
    }

    public static void setNameProperties(String name, Map properties, boolean doSave) throws Exception
    {
        if (StringUtils.isBlank(name))
        {
            throw new Exception("'name' parameter must be passed");
        }
        else
        {
            instance.setNameProperties(name, properties, doSave);
        }
    }

    /**
     * Removes properties for a specific name property.
     *
     * @param name cannot be null or empty.
     * @throws Exception
     */
    public static void removeNameProperties(String name) throws Exception
    {
        if (StringUtils.isBlank(name))
        {
            throw new Exception("'name' parameter must be passed");
        }
        else
        {
            instance.removeNameProperties(name);
        }
    }

    /**
     * Saves the configurations in the RSRemote.
     *
     * @throws Exception
     */
    public static void saveConfig() throws Exception
    {
        MainBase.main.callSaveConfig();
    }

    /**
     * This method sends email message using the EWS gateway.
     *
     * <pre>
     * {@code
     * import com.resolve.gateway.EWSAPI;
     * ....
     * ....
     *
     * try
     * {
     *      Map<String, Object> params = new HashMap<String, Object>();
     *      params.put("TO", "someemail@domain");
     *      params.put("SUBJECT", "Hello from Resolve EWS gateway");
     *      params.put("CONTENT", "Type some contents here");
     *
     *      //Map to store attachment name and its content.
     *      Map<String, byte[]> files = new HashMap<String, byte[]>();
     *      //read a file into the memory as a byte array.
     *      //make sure you type the absolute path to the file.
     *      byte[] bytes = FileUtils.readFileToByteArray(new File("c:/tmp/rsremote.log"));
     *      //the attachment will go with the name rsremote.log as specified in the below Map entry.
     *      files.put("rsremote.log", bytes);
     *      params.put("ATTACHMENTS", files);
     *
     *      EWSAPI.sendMessage(params, null, null);
     * }
     * catch (Exception e)
     * {
     *      //do something useful with this exception
     * }
     * }
     * </pre>
     *
     * @param params
     * @param username
     *            if not provided then uses the gateway configuration.
     * @param password
     *            ties with the username.
     * @throws Exception
     */
    public static void sendMessage(Map<String, Object> params, String username, String password) throws Exception
    {
        instance.sendMessage(params, username, password);
    }

    /**
     * This method moves a email ID into a folder or sub folder. If the
     * "subFolderName" is provided then it must be available under the folder
     * defined by "folderName".
     *
     * <pre>
     * {@code
     * import com.resolve.gateway.EWSAPI;
     * ....
     * ....
     *
     *  try
     *  {
     *      //to use this method you will perhaps search for email or execute from
     *      //some action task after the gateway filter sends email data to a runbook.
     *      String emailID = "AAMkADUyYmE2YTY4LWYwMGEtNDcx";
     *      String folderName = "sandbox"; //a folder inside the user's Inbox.
     *      String subFolderName = "subfolder"; //a folder inside the folderName.
     *      EWSAPI.moveEmailToFolder(emailID, folderName, subFolderName, null, null);
     *  }
     *  catch (Exception e)
     *  {
     *      //do something useful with this exception.
     *  }
     * }
     * </pre>
     *
     * @param emailID
     *            is a {@link String} that stores te unique ID of the
     *            email to be moved to a folder or its subfolder.
     * @param folderName
     *            is the display name of a folder under Inbox.
     * @param subFolderName
     *            is the display name of a sub folder under the "folder".
     * @param username
     *            if not provided then uses the gateway configuration.
     * @param password
     *            ties with the username.
     * @return true or false based on the result of the operation.
     * @throws Exception
     */
    public static boolean moveEmailToFolder(String emailID, String folderName, String subFolderName, String username, String password) throws Exception
    {
        return instance.moveEmailToFolder(emailID, folderName, subFolderName, username, password);
    }

    /**
     * This method moves a list of email IDs into a folder or sub folder. If the "subFolderName" is provided then it must be available under the folder
     * defined by "folderName".
     *
     * <pre>
     * {@code
     * import com.resolve.gateway.EWSAPI;
     * ....
     * ....
     *
     *  try
     *  {
     *      //to use this method you will perhaps search for email or execute from
     *      //some action task after the gateway filter sends email data to a runbook.
     *      String[] emailIDs = new String[] {"AAMkADUyYmE2YTY4LWYwMGEtNDcx", "2vTbkDWUpADe0zAAAAg/nEAAA="};
     *      String folderName = "sandbox"; //a folder inside the user's Inbox.
     *      String subFolderName = "subfolder"; //a folder inside the folderName.
     *      EWSAPI.moveEmailToFolder(emailIDs, folderName, subFolderName, null, null);
     *  }
     *  catch (Exception e)
     *  {
     *      //do something useful with this exception.
     *  }
     * }
     * </pre>
     *
     * @param emailIDs is an array of {@link String} that stores te unique IDs of the emails to be moved to a folder or its subfolder.
     * @param folderName is the display name of a folder under Inbox.
     * @param subFolderName is the display name of a sub folder under the "folder".
     * @param username
     *            if not provided then uses the gateway configuration.
     * @param password
     *            ties with the username.
     * @return true or false based on the result of the operation.
     * @throws Exception
     */
    public static boolean moveEmailToFolder(String[] emailIDs, String folderName, String subFolderName, String username, String password) throws Exception
    {
        return instance.moveEmailToFolder(emailIDs, folderName, subFolderName, username, password);
    }

    /**
     * This method helps searching for email message from the user's inbox.
     *
     * <pre>
     * {@code
     * import com.resolve.gateway.EWSAPI;
     * ....
     * ....
     * try
     * {
     *      //this query will read the unread messages where the subject has help in it.
     *      String query = "isread = 'false' and subject contains 'help'";
     *      List<Map> emails = EWSAPI.searchEmail(query, 100, true, null, null);
     *      //now read the details that came through the email
     *      for(Map email : emails)
     *      {
     *          System.out.println("Email ID:" + email.get("EMAIL_ID"));
     *          System.out.println("From:" + email.get("FROM"));
     *          System.out.println("Sender Name:" + email.get("SENDERNAME"));
     *          System.out.println("Sender Address:" + email.get("SENDERADDRESS"));
     *          System.out.println("Subject:" + email.get("SUBJECT"));
     *          System.out.println("Content:" + email.get("CONTENT"));
     *          System.out.println("Now the attachments:");
     *          Map<String, byte[]> attachments = (Map) email.get("ATTACHMENTS");
     *
     *          if(attachments != null && attachments.size() > 0)
     *          {
     *              //at this point you may convert the byte[] and save as a file
     *              //in a convenient place, but in this example we're printing the file content.
     *              //Note that if the attachment is a binary file then you are better off with storing
     *              //it. This example is just for showing how content of a text file could be
     *              //accessed from the email message map.
     *              for(String fileName : attachments.keySet())
     *              {
     *                  String fileContent = new String(attachments.get(fileName));
     *                  System.out.println("#----------------" + fileName + "------------------#\n");
     *                  System.out.println(fileContent);
     *              }
     *          }
     *      }
     * }
     * catch (Exception e)
     * {
     *      //do something useful with this exception.
     * }
     * }
     * </pre>
     *
     * @param query
     *            is the Resolve Query Language with some restrictions. Only
     *            "and" logical operator supported. "ISREAD", "SUBJECT",
     *            "CONTENT", "FROM", "SENDER" fields are supported to be
     *            queried. Here are some examples of the query:
     *            1. ISREAD = 'false' and SUBJECT contains 'help', this query will read all unread emails where subject line contains the word "help".
     *            2. CONTENT contains 'help' and FROM contains 'support', this query will read email with the content (body) with help word
     *            and it's from support.
     * @param maxRecords
     *            to be returned.
     * @param includeAttachments
     *            if true sends attachments in the returned {@link List} of
     *            {@link Map}.
     * @param username
     * @param password
     * @return a {@link List} of {@link Map} for all the emails.
     * @throws Exception
     */
    public static List<Map> searchEmail(String query, int maxRecords, boolean includeAttachments, String username, String password) throws Exception
    {
        return instance.searchEmail(query, maxRecords, includeAttachments, username, password);
    }

    /**
     * This method forwards an email message using the EWS gateway.
     *
     * <pre>
     * {@code
     * import com.resolve.gateway.EWSAPI;
     * ....
     * ....
     *
     * try
     * {
     *      String forwardTo = "valid email address";
     *      //in general you will either get it from the search API result
     *      //or parameter in a runbook (the key of the runbook parameter is EMAILID).
     *      String emailId = "Id of the email";
     *      String content = "content for the forwarding message";
     *      String username = null; //use the gateway configuration
     *      String password = null;
     *      Map<String, Object> params = new HashMap<String, Object>();
     *      params.put("TO", forwardTo);
     *      params.put("CONTENT", content);
     *
     *      EWSAPI.forwardEmail(emailId, params, username, password);
     * }
     * catch (Exception e)
     * {
     *      //do something useful with this exception
     * }
     * }
     * </pre>
     *
     * @param params
     * @param username
     *            if not provided then uses the gateway configuration.
     * @param password
     *            ties with the username.
     * @throws Exception
     */
    public static void forwardEmail(String emailID, Map<String, Object> params, String username, String password) throws Exception
    {
        instance.forwardEmail(emailID, params, username, password);
    }
}
