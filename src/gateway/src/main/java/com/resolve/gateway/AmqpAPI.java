/******************************************************************************
 * (C) Copyright 2014
 *
 * Resolve Systems, LLC
 *
 * All rights reserved This software is distributed under license and may not be
 * used, copied, modified, or distributed without the express written permission
 * of Resolve Systems, LLC.
 *
 ******************************************************************************/

package com.resolve.gateway;

import java.io.Serializable;
import java.util.Map;

import com.resolve.rsbase.MainBase;
import com.resolve.gateway.amqp.AmqpGateway;
import com.resolve.util.StringUtils;

public class AmqpAPI
{
    private static AmqpGateway instance = AmqpGateway.getInstance();

    /**
     * Returns the {@link Object} value for the given key in the current
     * AmqpGateway instance's NameProperty map.
     * 
     * @param name
     *            the name of the property {@link Map} in the
     *            AmqpGateway instance's {@link NameProperties} object
     * @param key
     *            the key associated with the expected value in the property
     *            {@link Map} returned from the AmqpGateway's
     *            {@link NameProperties} object
     * @return {@link Object} value associated with the key in the property
     *         {@link Map} returned by the AmqpGateway instance's
     *         {@link NameProperties} object
     */
    public static Object getNameObject(String name, String key)
    {
        return instance.getNameProperties().getNameObject(name, key);
    }

    /**
     * Uses the given name parameter to return a property {@link Map} within the
     * AmqpGateway instance's {@link NameProperties} object. If the
     * returned {@link Map} isn't null, it adds the given {@link String} key and
     * {@link Object} value to it.
     * 
     * @param name
     *            the name of the {@link Map} in the AmqpGateway
     *            instance's {@link NameProperties} object. Cannot be null or
     *            empty.
     * @param key
     *            the key in the {@link Map} returned from the
     *            {@link NameProperties} object in the AmqpGateway.
     *            Cannot be null or empty.
     * @param value
     *            the value associated with the given key in the {@link Map}
     *            returned from the {@link NameProperties} object in the
     *            AmqpGateway
     * 
     * @throws Exception
     */
    public static void setNameObject(String name, String key, Object value) throws Exception
    {
        Map nameProperties = getNameProperties(name);
        if (nameProperties != null)
        {
            nameProperties.put(key, value);
            setNameProperties(name, nameProperties, true);
        }
    }

    /**
     * Uses the given {@link String} name as a key to return a {@link Map} of
     * properties from the AmqpGateway instance's {@link NameProperties}
     * object.
     *
     * @param name
     *            The name associated with the properties {@link Map} in the
     *            AmqpGateway's {@link NameProperties} object. Cannot be
     *            null or empty.
     * @return a Map of AmqpGateway name properties
     * @throws Exception
     */
    public static Map getNameProperties(String name) throws Exception
    {
        return instance.getNameProperties().getNameProperties(name);
    }

    /**
     * Associates the given name to the given properties {@link Map} in the
     * AmqpGateway instance's {@link NameProperties} object.
     *
     * @param name
     *            the key in the AmqpGateway's {@link NameProperties}
     *            object. Cannot be null or empty.
     * @param properties
     *            A Map of properties to associate with the given name parameter
     * @throws Exception
     */
    public static void setNameProperties(String name, Map properties) throws Exception
    {
        setNameProperties(name, properties, true);
    }

    /**
     * Associates the given name to the given properties Map in the AmqpGateway
     * instance's NameProperties object.
     *
     * @param name
     *            the key in the AmqpGateway's {@link NameProperties}
     *            object. Cannot be null or empty.
     * @param properties
     *            A {@link Map} of properties to associate with the given name
     *            parameter
     * @param doSave
     *            a boolean value that designates whether to send this change to
     *            RSControl for persistence
     * @throws Exception
     */
    public static void setNameProperties(String name, Map properties, boolean doSave) throws Exception
    {
        if (StringUtils.isBlank(name))
        {
            throw new Exception("'name' parameter must be passed");
        }
        else
        {
            instance.setNameProperties(name, properties, doSave);
        }
    }

    /**
     * Removes a {@link Map} of properties associated with the given name in the
     * AmqpGateway instance's {@link NameProperties} object.
     *
     * @param name
     *            The name associated with a property {@link Map} in the
     *            AmqpGateway instance's {@link NameProperties} object.
     *            Cannot be null or empty.
     * @throws Exception 
     */
    
    public static void removeNameProperties(String name) throws Exception
    {
        if (StringUtils.isBlank(name))
        {
            throw new Exception("'name' parameter must be passed");
        }
        else
        {
            instance.removeNameProperties(name);
        }
    }

    /**
     * Saves the configurations in the RSRemote for persistence.
     *
     * @throws Exception
     */
    public static void saveConfig() throws Exception
    {
        MainBase.main.callSaveConfig();
    }

    /**
     * This method sends a message to a AMQP (e.g., RabbitMQ) Queue.
     * 
     * <pre>
     * {@code
     * import com.resolve.gateway.AmqpAPI;
     * ....
     * ....
     * try
     * {
     *    //Content could be anything like XML, JSON etc. It is dictated by the other system. 
     *     String message = "Hello World";
     *     //The queue name is very much case sensitive, verify that with the AMQP server
     *     AmqpAPI.send("MyQueue", message);
     * }
     * catch (Exception e)
     * {
     *     //Do something about this exception
     * }
     * }
     * </pre>
     * 
     * @param queue
     *            name is very much case sensitive, verify that with the AMQP
     *            server
     * @param message
     *            could be anything like XML, JSON etc. Depends on what the
     *            other system is looking for.
     * @throws Exception
     */
    public static void send(String queue, Serializable message) throws Exception
    {
        instance.send(queue, message);
    }

    /**
     * This method sends a message to a AMQP (e.g., RabbitMQ) Exchange. Exchange
     * is similar to a JSM Topic where there could be multiple subscribers that
     * registers to receive messages.
     * 
     * <pre>
     * {@code
     * import com.resolve.gateway.AmqpAPI;
     * ....
     * ....
     * try
     * {
     *    //Content could be anything like XML, JSON etc. It is dictated by the other system. 
     *     String message = "Hello World";
     *     //The exchange name is very much case sensitive, verify that with the AMQP server
     *     AmqpAPI.publish("MyExchange", message);
     * }
     * catch (Exception e)
     * {
     *     //Do something about this exception
     * }
     * }
     * </pre>
     * 
     * @param exchange
     *            name is very much case sensitive, verify that with the AMQP
     *            server
     * @param message
     *            could be anything like XML, JSON etc. It is dictated by the
     *            other system.
     * @throws Exception
     */
    public static void publish(String exchange, Serializable message) throws Exception
    {
        instance.send(exchange, message);
    }
} // AmqpAPI
