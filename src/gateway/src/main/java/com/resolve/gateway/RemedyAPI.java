/******************************************************************************
 * (C) Copyright 2014
 *
 * Resolve Systems, LLC
 *
 * All rights reserved This software is distributed under license and may not be
 * used, copied, modified, or distributed without the express written permission
 * of Resolve Systems, LLC.
 *
 ******************************************************************************/

package com.resolve.gateway;

import java.util.List;
import java.util.Map;

import com.remedy.arsys.api.SortInfo;
import com.remedy.arsys.api.StatusInfo;
import com.resolve.gateway.remedy.RemedyGateway;

public class RemedyAPI
{
    /**
     * Creates an entry using the given form schema ID and the given fieldvalue.
     * 
     * @param form
     *            this sets the schema ID for the new form.
     * @param fieldValueStr
     *            values for the form data.
     * @return the newly created entry ID.
     * @throws Exception
     *             if an error occurs when getting an open Remedy session
     */

    public static String create(String form, String fieldValueStr) throws Exception
    {
        return RemedyGateway.create(form, fieldValueStr);
    } // create

    /**
     * Creates a form using the given form schema ID and the given {@link Map}
     * of field values.
     * 
     * @param form
     *            this sets the schema ID for the new form.
     * @param fieldValues
     *            A {@link Map} of field values to instantiate a new Remedy
     *            entry.
     * @return the newly created entry ID.
     * @throws Exception
     *             if an error occurs when getting an open Remedy session
     */
    public static String create(String form, Map fieldValues) throws Exception
    {
        return RemedyGateway.create(form, fieldValues);
    } // create

    /**
     * Updates a previous entry ID with a new entry using the given form schema
     * ID and the given {@link Map} of field values.
     * 
     * @param form
     *            this sets the schema ID for the new form.
     * @param entryId
     *            the entry ID to update.
     * @param fieldValueStr
     *            A {@link Map} of field values to instantiate a new Remedy
     *            entry.
     * @return the updated entryID
     * @throws Exception
     *             if an error occurs when getting an open Remedy session
     */
    public static String update(String form, String entryId, String fieldValueStr) throws Exception
    {
        return RemedyGateway.update(form, entryId, fieldValueStr);
    } // update

    /**
     * Updates a previous entry ID with a new entry using the given form schema
     * ID and the given {@link Map} of field values.
     * 
     * @param form
     *            this sets the schema ID for the new form.
     * @param entryId
     *            the entry ID to update.
     * @param fieldValues
     *            A {@link Map} of field values to instantiate a new Remedy
     *            entry.
     * @return the updated entryID
     * @throws Exception
     *             if an error occurs when getting an open Remedy session
     */
    public static String update(String form, String entryId, Map fieldValues) throws Exception
    {
        return RemedyGateway.update(form, entryId, fieldValues);
    } // update

    /**
     * Finds all entries that fall within the given schema ID and criteria.
     * 
     * @param form
     *            this is the schema ID to search against.
     * @param criteria
     *            the search criteria
     * @return a {@link List} of matching entry IDs from the search query
     * @throws Exception
     *             if an error occurs when getting an open Remedy session
     */
    public static List find2(String form, String criteria) throws Exception
    {
        return RemedyGateway.find2(form, criteria);
    } // find2

    /**
     * Finds all entries that fall within the given schema ID and criteria.
     * 
     * @param form
     *            this is the schema ID to search against.
     * @param criteria
     *            the search criteria
     * @return a {@link List} of matching entry ID {@link String}s from the
     *         search query
     * @throws Exception
     *             if an error occurs when getting an open Remedy session
     */
    public static List<String> find(String form, String criteria) throws Exception
    {
        return RemedyGateway.find(form, criteria);
    } // find

    /**
     * Finds all entries that fall within the given schema ID and criteria.
     * 
     * @param form
     *            this is the schema ID to search against.
     * @param criteria
     *            the search criteria
     * @param sortInfos
     *            An array of SortInfo objects to format your result
     *            {@link List}
     * @return a {@link List} of matching entry ID {@link String}s from the
     *         search query
     * @throws Exception
     *             if an error occurs when getting an open Remedy session
     */
    public static List<String> find(String form, String criteria, SortInfo[] sortInfos) throws Exception
    {
        return RemedyGateway.find(form, criteria, sortInfos);
    } // find

    /**
     * Retrieves a {@link Map} of all field values with the given form schemaID
     * and entryID.
     * 
     * @param form
     *            this is the schema ID to search against.
     * @param entryId
     *            the ID of the entry which contains the field values
     * @return a {@link Map} of field values
     * @throws Exception
     *             if an error occurs when getting an open Remedy session
     */
    public static Map retrieve(String form, String entryId) throws Exception
    {
        return RemedyGateway.retrieve(form, entryId);
    } // retrieve

    /**
     * Retrieves a {@link Map} of specific field values with the given form
     * schemaID, and entryID, and a list of field IDs.
     * 
     * @param form
     *            this is the schema ID to search against.
     * @param entryId
     *            the ID of the entry which contains the field values
     * @param fieldList
     *            a list of fieldIds separated by '&amp;'
     * @return a {@link Map} of field values
     * @throws Exception
     *             if an error occurs when getting an open Remedy session
     */

    public static Map retrieve(String form, String entryId, String fieldList) throws Exception
    {
        return RemedyGateway.retrieve(form, entryId, fieldList);
    } // retrieve

    /**
     * Returns a string of all field value keys and values for the Remedy entry
     * at the given entry ID.
     * 
     * @param form
     *            this is the schema ID to search against.
     * @param entryId
     *            the ID of the entry
     * @return a string of entry field value keys and values
     * @throws Exception
     *             if an error occurs when getting an open Remedy session
     */
    public static String print(String form, String entryId) throws Exception
    {
        return RemedyGateway.print(form, entryId, null);
    } // print

    /**
     * Returns a string of all field value keys and values for the Remedy entry
     * at the given list of entry IDs.
     * 
     * @param form
     *            this is the schema ID to search against.
     * @param entryId
     *            the ID of the entry
     * @param fieldList
     *            a list of entry IDs
     * @return a string of entry field value keys and values
     * @throws Exception
     *             if an error occurs when getting an open Remedy session
     */
    public static String print(String form, String entryId, String fieldList) throws Exception
    {
        return RemedyGateway.print(form, entryId, fieldList);
    } // print

    /**
     * Removes the entry that coincides with the given form schema ID and
     * entryID
     * 
     * @param form
     *            this is the schema ID to search against.
     * @param entryId
     *            the entry ID of the entry to delete.
     * @return the entry ID of the deleted entry
     * @throws Exception
     *             if an error occurs when getting an open Remedy session
     */
    public static String delete(String form, String entryId) throws Exception
    {
        return RemedyGateway.delete(form, entryId);
    } // delete

    /**
     * Logs all StatusInfo objects in the given statusList.
     * 
     * @param statusList
     *            an array of com.remedy.arsys.api.StatusInfo objects
     */
    public static void printStatusList(StatusInfo[] statusList)
    {
        RemedyGateway.printStatusList(statusList);
    } // printStatusList

    /**
     * Returns a map of Remedy field values from the given log parameter.
     * 
     * @param log
     *            a log String
     * 
     * @return a Map of Remedy field values
     */
    public static Map createFieldValues(String log)
    {
        return RemedyGateway.createFieldValues(log);
    } // createFieldValues

} // RemedyAPI
