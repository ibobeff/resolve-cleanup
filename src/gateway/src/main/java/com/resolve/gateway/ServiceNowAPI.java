/******************************************************************************
* (C) Copyright 2014
*
* Resolve Systems, LLC
*
* All rights reserved
* This software is distributed under license and may not be used, copied,
* modified, or distributed without the express written permission of
* Resolve Systems, LLC.
*
******************************************************************************/

package com.resolve.gateway;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.resolve.rsbase.MainBase;
import com.resolve.gateway.servicenow.ServiceNowGateway;
import com.resolve.util.StringUtils;

public class ServiceNowAPI
{
    private static ServiceNowGateway instance = ServiceNowGateway.getInstance();

    /**
     * Returns the {@link Object} value for the given key in the current
     * ServiceNowGateway instance's NameProperty map.
     * 
     * @param name
     *            the name of the property {@link Map} in the
     *            ServiceNowGateway instance's {@link NameProperties}
     *            object
     * @param key
     *            the key associated with the expected value in the property
     *            {@link Map} returned from the ServiceNowGateway's
     *            {@link NameProperties} object
     * @return {@link Object} value associated with the key in the property
     *         {@link Map} returned by the ServiceNowGateway instance's
     *         {@link NameProperties} object
     */
    public static Object getNameObject(String name, String key)
    {
        return instance.getNameProperties().getNameObject(name, key);
    }

    /**
     * Uses the given name parameter to return a property {@link Map} within the
     * ServiceNowGateway instance's {@link NameProperties} object. If
     * the returned {@link Map} isn't null, it adds the given {@link String} key
     * and {@link Object} value to it.
     * 
     * @param name
     *            the name of the {@link Map} in the ServiceNowGateway
     *            instance's {@link NameProperties} object. Cannot be null or
     *            empty.
     * @param key
     *            the key in the {@link Map} returned from the
     *            {@link NameProperties} object in the ServiceNowGateway
     *            . Cannot be null or empty.
     * @param value
     *            the value associated with the given key in the {@link Map}
     *            returned from the {@link NameProperties} object in the
     *            ServiceNowGateway
     * 
     * @throws Exception
     */
    public static void setNameObject(String name, String key, Object value) throws Exception
    {
        Map nameProperties = getNameProperties(name);
        if(nameProperties != null)
        {
            nameProperties.put(key, value);
            setNameProperties(name, nameProperties, true);
        }
    }
    
    /**
     * Uses the given {@link String} name as a key to return a {@link Map} of
     * properties from the ServiceNowGateway instance's
     * {@link NameProperties} object.
     *
     * @param name
     *            The name associated with the properties {@link Map} in the
     *            ServiceNowGateway's {@link NameProperties} object.
     *            Cannot be null or empty.
     * @return a Map of ServiceNowGateway name properties
     * @throws Exception
     */
    public static Map getNameProperties(String name) throws Exception
    {
        return instance.getNameProperties().getNameProperties(name);
    }

    /**
     * Associates the given name to the given properties {@link Map} in the
     * ServiceNowGateway instance's {@link NameProperties} object.
     *
     * @param name
     *            the key in the ServiceNowGateway's
     *            {@link NameProperties} object. Cannot be null or empty.
     * @param properties
     *            A Map of properties to associate with the given name parameter
     * @throws Exception
     */
    public static void setNameProperties(String name, Map properties) throws Exception
    {
        setNameProperties(name, properties, true);
    }

    /**
     * Associates the given name to the given properties Map in the
     * ServiceNowGateway instance's NameProperties object.
     *
     * @param name
     *            the key in the ServiceNowGateway's {@link NameProperties}
     *            object. Cannot be null or empty.
     * @param properties
     *            A {@link Map} of properties to associate with the given name
     *            parameter
     * @param doSave
     *            a boolean value that designates whether to send this change to
     *            RSControl for persistence
     * @throws Exception
     */
    public static void setNameProperties(String name, Map properties, boolean doSave) throws Exception
    {
        if (StringUtils.isBlank(name))
        {
            throw new Exception("'name' parameter must be passed");
        }
        else
        {
            instance.setNameProperties(name, properties, doSave);
        }
    }

    /**
     * Removes a {@link Map} of properties associated with the given name in the
     * ServiceNowGateway instance's {@link NameProperties} object.
     *
     * @param name
     *            The name associated with a property {@link Map} in the
     *            ServiceNowGateway instance's {@link NameProperties}
     *            object. Cannot be null or empty.
     * @throws Exception
     */
    public static void removeNameProperties(String name) throws Exception
    {
        if (StringUtils.isBlank(name))
        {
            throw new Exception("'name' parameter must be passed");
        }
        else
        {
            instance.removeNameProperties(name);
        }
    }

    /**
     * Saves the configurations in the RSRemote.
     *
     * @throws Exception
     */
    public static void saveConfig() throws Exception
    {
        MainBase.main.callSaveConfig();
    }

    /**
     * Creates a new ServiceNow incident.
     * 
     * @deprecated As of release 5.2, replaced by {@link #createObject(String, Map, String, String)}
     *
     * <pre>
     * {@code
     * try
     * {
     *      Map<String, String> params = new HashMap<String, String>();
     *      params.put("short_description", "Description for the incident");
     *
     *      Map<String, String> result = ServiceNowAPI.createIncident(params, "admin", "admin");
     *      // The most important property is the NUMBER that you may need for future references.
     *      // This is how you get the NUMBER.
     *      String sysId = result.get("SYS_ID");
     *      String number = result.get("NUMBER");
     *
     *      System.out.printf("Here is your SysId: %s and the Number: %s for the incident\n", sysId, number);
     *      System.out.printf("Now list all the properties and their values\n");
     *      for(String key : result.keySet())
     *      {
     *          System.out.printf("Property:%s, Value:%s\n", key, result.get(key));
     *      }
     * }
     * catch (Exception ex)
     * {
     *      //Do something good with this error.
     *      ex.printStackTrace();
     * }
     * }
     * </pre>
     *
     * @param params
     *            refer to the ServiceNow documentation for more information
     *            about various properties you can set for an incident. It may
     *            vary by installation.
     * @param username
     *            if not provided uses the gateway configuration.
     * @param p_assword
     *            ties with the username..
     * @return a {@link Map} with all the properties for the incidents.
     * @throws Exception
     */
    @Deprecated
    public static Map<String, String> createIncident(Map<String, String> params, String username, String p_assword) throws Exception
    {
        return instance.createIncident(params, username, p_assword);
    }

    /**
     * Creates work note for an Incident.
     *
     * @deprecated As of release 5.2, replaced by {@link #updateObject(String, String, Map, String, String)}
     * 
     * <pre>
     * {@code
     * try
     * {
     *      String parentSysId = "d71b3b41c0a8016700a8ef040791e72a";
     *
     *      String note = "Work note desc";
     *      ServiceNowAPI.createIncidentWorknote(parentSysId, note, "admin", "admin");
     * }
     * catch (Exception ex)
     * {
     *      //Do something good with this error.
     *      ex.printStackTrace();
     * }
     * }
     * </pre>
     *
     * @param parentSysId
     *            must be provided. It's the Incident's SysId auto-generated by
     *            the system. The work log will be created for this incident.
     * @param note
     *            to be added to the incident.
     * @param username
     *            if not provided uses the gateway configuration.
     * @param p_assword
     *            ties with the username.
     * @return true if work note is created successfully.
     * @throws Exception
     */
    @Deprecated
    public static boolean createIncidentWorknote(String parentSysId, String note, String username, String p_assword) throws Exception
    {
        return instance.createIncidentWorknote(parentSysId, note, username, p_assword);
    }

    /**
     * Updates a ServiceNow incident.
     *
     * @deprecated As of release 5.2, replaced by {@link #updateObject(String, String, Map, String, String)}
     * 
     * Note: ServiceNow may not allow update on certain properties, please refer
     * to the ServiceNow documentation for more information. It may vary by the
     * installations.
     *
     * <pre>
     * {@code
     * try
     *  {
     *      String sysId = "d71b3b41c0a8016700a8ef040791e72a";
     *
     *      Map<String, String> params = new HashMap<String, String>();
     *      params.put("short_description", "Changed the description for the incident");
     *      ServiceNowAPI.updateIncident(sysId, params, "admin", "admin");
     *  }
     *  catch (Exception ex)
     *  {
     *       //Do something good with this error.
     *       ex.printStackTrace();
     *  }
     *  }
     * </pre>
     *
     * @param sysId
     *            must be provided. It's the sysId auto-generated by the system.
     * @param params
     * @param username
     *            if not provided uses the gateway configuration.
     * @param password
     *            ties with the username. Gateway configuration is used if not
     *            provided.
     * @throws Exception
     */
    @Deprecated
    public static void updateIncident(String sysId, Map<String, String> params, String username, String password) throws Exception
    {
        instance.updateIncident(sysId, params, username, password);
    }

    /**
     * Updates incident status.
     *
     * @deprecated As of release 5.2, replaced by {@link #updateObject(String, String, Map, String, String)}
     * 
     * Note: ServiceNow may not allow changing status to any arbitrary value due
     * to business rules. Please refer to the documentation for more
     * information.
     *
     * <pre>
     * {@code
     * try
     * {
     *      //Incident Update
     *      String updatedStatus = "6"; //Resolved
     *      String sysId = "d71b3b41c0a8016700a8ef040791e72a";
     *
     *      ServiceNowAPI.updateIncidentStatus(sysId, updatedStatus, "admin", "admin");
     * }
     * catch (Exception ex)
     * {
     *      //Do something good with this error.
     *      ex.printStackTrace();
     * }
     * }
     * </pre>
     *
     * @param sysId
     *            must be provided. It's the sysId auto-generated by the system.
     * @param status
     *            is currently a number in String form and could be one of the
     *            following: 1 - New 2 - Active 3 - Awaiting Problem 4 -
     *            Awaiting User Info 5 - Awaiting Evidence 6 - Resolved 7 -
     *            Closed
     * @param username
     * @param password
     * @throws Exception
     */
    @Deprecated
    public static void updateIncidentStatus(String sysId, String status, String username, String password) throws Exception
    {
        instance.updateIncidentStatus(sysId, status, username, password);
    }

    /**
     * Deletes a ServiceNow incident.
     *
     * @deprecated As of release 5.2, replaced by {@link #deleteObject(String, String, String, String)}
     * 
     * Note: ServiceNow may not allow deleting an incident due to certain
     * dependencies (e.g., status etc.). Please refer to the documentation.
     *
     * <pre>
     * {@code
     * try
     *  {
     *      String sysId = "d71b3b41c0a8016700a8ef040791e72a";
     *      ServiceNowAPI.deleteIncident(sysId, "admin", "admin");
     *  }
     *  catch (Exception ex)
     *  {
     *       //Do something good with this error.
     *       ex.printStackTrace();
     *  }
     * }
     * </pre>
     *
     * @param sysId
     *            must be provided. It's the sysId auto-generated by the system.
     * @param username
     *            if not provided uses the gateway configuration.
     * @param password
     *            ties with the username. Gateway configuration is used if not
     *            provided.
     * @throws Exception
     */
    @Deprecated
    public static void deleteIncident(String sysId, String username, String password) throws Exception
    {
        instance.deleteIncident(sysId, username, password);
    }

    /**
     * Selects ServiceNow incident based on the sysId.
     *
     * @deprecated As of release 5.2, replaced by {@link #selectObjectBySysId(String, String, String, String)}
     * 
     * <pre>
     * {@code
     * try
     * {
     *      String sysId = "d71b3b41c0a8016700a8ef040791e72a";
     *      Map<String, String> result = ServiceNowAPI.selectIncidentBySysId(sysId, "admin", "admin");
     *      for(String key : result.keySet())
     *      {
     *          System.out.printf("Property:%s, Value:%s\n", key, result.get(key));
     *      }
     *  }
     *  catch (Exception ex)
     *  {
     *      //Do something good with this error.
     *      ex.printStackTrace();
     *  }
     *  }
     * </pre>
     *
     * @param sysId
     *            must be provided. It's the sysId auto-generated by the system.
     * @param username
     *            if not provided uses the gateway configuration.
     * @param password
     *            ties with the username. Gateway configuration is used if not
     *            provided.
     * @return a {@link Map} that contains properties of the incident.
     * @throws Exception
     */
    @Deprecated
    public static Map<String, String> selectIncidentBySysId(String sysId, String username, String password) throws Exception
    {
        return instance.selectIncidentBySysId(sysId, username, password);
    }

    /**
     * Selects ServiceNow incident based on the incident number.
     *
     * @deprecated As of release 5.2, replaced by {@link #selectObjectByAlternateID(String, String, String, String, String)}
     * 
     * <pre>
     * {@code
     * try
     * {
     *      String number = "INC0000011";
     *      Map<String, String> result = ServiceNowAPI.selectIncidentByNumber(number, "admin", "admin");
     *      for(String key : result.keySet())
     *      {
     *          System.out.printf("Property:%s, Value:%s\n", key, result.get(key));
     *      }
     *  }
     *  catch (Exception ex)
     *  {
     *      //Do something good with this error.
     *      ex.printStackTrace();
     *  }
     *  }
     * </pre>
     *
     * @param number
     *            must be provided. It's the number auto-generated by the
     *            system.
     * @param username
     *            if not provided uses the gateway configuration.
     * @param password
     *            ties with the username. Gateway configuration is used if not
     *            provided.
     * @return a {@link Map} that contains properties of the incident.
     * @throws Exception
     */
    @Deprecated
    public static Map<String, String> selectIncidentByNumber(String number, String username, String password) throws Exception
    {
        return instance.selectIncidentByNumber(number, username, password);
    }

    /**
     * Searches ServiceNow incident(s) based on the query.
     *
     * @deprecated As of release 5.2, replaced by {@link #searchObject(String, String, String, boolean, String, String)}
     * 
     * To learn more about how to write ServiceNow query, refer to:
     *
     * http://wiki.servicenow.com/index.php?title=Embedded:Encoded_Query_Strings
     *
     * <pre>
     * {@code
     *  try
     *  {
     *      String query = "short_descriptionLIKEnot working^category=hardware";
     *      List<Map<String, String>> incidents = ServiceNowAPI.searchIncident(query, null, null);
     *
     *      for(Map<String, String> incident : incidents)
     *      {
     *          int i = 1;
     *          System.out.printf("Incident# %d:", i++);
     *          for(String key : incident.keySet())
     *          {
     *              System.out.printf("Property:%s, Value:%s\n", key, incident.get(key));
     *          }
     *      }
     *  }
     *  catch (Exception ex)
     *  {
     *      //Do something good with this error.
     *      ex.printStackTrace();
     *  }
     *  }
     * </pre>
     *
     * @param query
     *
     * @param username
     *            if not provided uses the gateway configuration.
     * @param password
     *            ties with the username. Gateway configuration is used if not
     *            provided.
     * @return a {@link List} of {@link Map} which contain properties for all
     *         the tickets.
     * @throws Exception
     */
    @Deprecated
    public static List<Map<String, String>> searchIncident(String query, String username, String password) throws Exception
    {
        return instance.searchIncident(query, username, password);
    }
    
    //Generic ServiceNow Object CRUD methods
    
    /**
     * Creates a new ServiceNow object of specified type.
     *
     * <pre>
     * {@code
     * try
     * {
     *      String objectType = "incident";
     *      Map<String, String> params = new HashMap<String, String>();
     *      params.put("short_description", "Description for the incident");
     *
     *      Map<String, String> result = ServiceNowAPI.createObject(objectType, params, "admin", "admin");
     *      // The most important property is the NUMBER that you may need for future references.
     *      // This is how you get the NUMBER.
     *      String sysId = result.get("SYS_ID");
     *      String number = result.get("NUMBER");
     *
     *      System.out.printf("Here is your SysId: %s and the Number: %s for the incident\n", sysId, number);
     *      System.out.printf("Now list all the properties and their values\n");
     *      for(String key : result.keySet())
     *      {
     *          System.out.printf("Property:%s, Value:%s\n", key, result.get(key));
     *      }
     * }
     * catch (Exception ex)
     * {
     *      //Do something good with this error.
     *      ex.printStackTrace();
     * }
     * }
     * </pre>
     *
     * @param objectType
     *            Type of ServiceNow object to create. Examples: "incident", "cmdb_ci" etc.
     * @param params
     *            refer to the ServiceNow documentation for more information
     *            about various properties you can set for specific object type. It may
     *            vary by installation and by object type.
     * @param username
     *            if not provided uses the gateway configuration.
     * @param p_assword
     *            ties with the username..
     * @return a {@link Map} with all the properties of the newly created object.
     * @throws Exception
     */
    public static Map<String, String> createObject(String objectType, Map<String, String> params, String username, String p_assword) throws Exception
    {
        return instance.createObject(objectType, params, username, p_assword);
    }
    
    /**
     * Selects ServiceNow object of specified type and sysId.
     *
     * <pre>
     * {@code
     * try
     * {
     *      String objectType = "incident";
     *      String sysId = "d71b3b41c0a8016700a8ef040791e72a";
     *      Map<String, String> result = ServiceNowAPI.selectObjectBySysId(objectType, sysId, "admin", "admin");
     *      for(String key : result.keySet())
     *      {
     *          System.out.printf("Property:%s, Value:%s\n", key, result.get(key));
     *      }
     *  }
     *  catch (Exception ex)
     *  {
     *      //Do something good with this error.
     *      ex.printStackTrace();
     *  }
     *  }
     * </pre>
     *
     * @param objectType
     *            type of ServiceNow object to select by System Id. for e.g. "incident", "cmdb_ci" etc.
     * @param sysId
     *            must be provided. It's ServiceNow auto-generated id for the object.
     * @param username
     *            if not provided uses the gateway configuration.
     * @param p_assword
     *            ties with the username. Gateway configuration is used if not
     *            provided.
     * @return a {@link Map} that contains properties of the ServiceNow object and sysId.
     * @throws Exception
     */
    public static Map<String, String> selectObjectBySysId(String objectType, String sysId, String username, String p_assword) throws Exception
    {
        return instance.selectObjectBySysId(objectType, sysId, username, p_assword);
    }
    
    /**
     * Selects specified ServiceNow object for specified alternate id name (if one exists) and value.
     *
     * <pre>
     * {@code
     * try
     * {
     *      String objectType = "incident";
     *      String altIDName = "number";
     *      String altIDValue = "INC0000011";
     *      Map<String, String> result = ServiceNowAPI.selectObjectByAlternateId(objectType, altIDName, altIDValue, "admin", "admin");
     *      for(String key : result.keySet())
     *      {
     *          System.out.printf("Property:%s, Value:%s\n", key, result.get(key));
     *      }
     *  }
     *  catch (Exception ex)
     *  {
     *      //Do something good with this error.
     *      ex.printStackTrace();
     *  }
     *  }
     * </pre>
     *
     * @param objectType
     *            type of ServiceNow object to select by System Id. for e.g. "incident", "cmdb_ci" etc.
     * @param altIDName
     *            Name of Alternate ID (if it exists).
     * @param altIDValue
     *            Value of Alternate ID to match.
     * @param username
     *            if not provided uses the gateway configuration.
     * @param p_assword
     *            ties with the username. Gateway configuration is used if not
     *            provided.
     * @return a {@link Map} that contains properties of the incident.
     * @throws Exception if select returns more than one record
     */
    public static Map<String, String> selectObjectByAlternateID(String objectType, String altIDName, String altIDValue, String username, String p_assword) throws Exception
    {
        return instance.selectObjectByAlternateID(objectType, altIDName, altIDValue, username, p_assword);
    }
    
    /**
     * Searches ServiceNow object(s) based on the query.
     *
     * To learn more about how to write ServiceNow query, refer to:
     *
     * http://wiki.servicenow.com/index.php?title=Embedded:Encoded_Query_Strings
     *
     * <pre>
     * {@code
     *  try
     *  {
     *      String objectType = "incident";
     *      String query = "short_descriptionLIKEnot working^category=hardware";
     *      String orderBy = "sys_updated_on";
     *      boolean isAscending = false;
     *      List<Map<String, String>> incidents = ServiceNowAPI.searchObject(objectType, query, orderBy, isAscending, null, null);
     *
     *      for(Map<String, String> incident : incidents)
     *      {
     *          int i = 1;
     *          System.out.printf("Incident# %d:", i++);
     *          for(String key : incident.keySet())
     *          {
     *              System.out.printf("Property:%s, Value:%s\n", key, incident.get(key));
     *          }
     *      }
     *  }
     *  catch (Exception ex)
     *  {
     *      //Do something good with this error.
     *      ex.printStackTrace();
     *  }
     *  }
     * </pre>
     *
     * @param objectType
     *            type of ServiceNow object to select by System Id. for e.g. "incident", "cmdb_ci" etc.
     * @param query
     *            Query in ServiceNow format to filter records. (optional)
     *            Use with caution as records returned could be large in absence of query.        
     * @param orderBy
     *            Order by property name. (optional)
     * @param isAscending
     *            true = ascending, false = descending
     * @param username
     *            if not provided uses the gateway configuration.
     * @param p_assword
     *            ties with the username. Gateway configuration is used if not
     *            provided.
     * @return a {@link List} of {@link Map} which contain properties for all
     *         the objects of specified type matching specified criteria.
     * @throws Exception
     */
    public static List<Map<String, String>> searchObject(String objectType, String query, String orderBy, boolean isAscending, String username, String p_assword) throws Exception
    {
        return instance.searchObject(objectType, query, orderBy, isAscending, username, p_assword);
    }
    
    /**
     * Updates a ServiceNow object of specified type.
     *
     * Note: ServiceNow may not allow update on certain properties, please refer
     * to the ServiceNow documentation for more information. It may vary by the
     * installations and by objec type.
     *
     * <pre>
     * {@code
     * try
     *  {
     *      String objectType = "incident";
     *      String sysId = "d71b3b41c0a8016700a8ef040791e72a";
     *
     *      Map<String, String> params = new HashMap<String, String>();
     *      params.put("short_description", "Changed the description for the incident");
     *      ServiceNowAPI.updateObject(objectType, sysId, params, "admin", "admin");
     *  }
     *  catch (Exception ex)
     *  {
     *       //Do something good with this error.
     *       ex.printStackTrace();
     *  }
     *  }
     * </pre>
     *
     * @param objectType
     *            type of ServiceNow object to update for e.g. "incident", "cmdb_ci" etc.
     * @param sysId
     *            must be provided. It's the sysId auto-generated by the system.
     * @param params
     * @param username
     *            if not provided uses the gateway configuration.
     * @param p_assword
     *            ties with the username. Gateway configuration is used if not
     *            provided.
     * @throws Exception
     */
    public static void updateObject(String objectType, String sysId, Map<String, String> params, String username, String p_assword) throws Exception
    {
        instance.updateObject(objectType, sysId, params, username, p_assword);
    }
    
    /**
     * Deletes ServiceNow object of specified type.
     *
     * Note: ServiceNow may not allow deleting objects due to certain
     * dependencies (e.g., status etc.). Please refer to the documentation.
     *
     * <pre>
     * {@code
     * try
     *  {
     *      String objectType = "incident";
     *      String sysId = "d71b3b41c0a8016700a8ef040791e72a";
     *      ServiceNowAPI.deleteObject(objectType, sysId, "admin", "admin");
     *  }
     *  catch (Exception ex)
     *  {
     *       //Do something good with this error.
     *       ex.printStackTrace();
     *  }
     * }
     * </pre>
     *
     * @param objectType
     *            type of ServiceNow object to update for e.g. "incident", "cmdb_ci" etc.
     * @param sysId
     *            must be provided. It's the sysId auto-generated by the system.
     * @param username
     *            if not provided uses the gateway configuration.
     * @param p_assword
     *            ties with the username. Gateway configuration is used if not
     *            provided.
     * @throws Exception
     */
    public static void deleteObject(String objectType, String sysId, String username, String p_assword) throws Exception
    {
        instance.deleteObject(objectType, sysId, username, p_assword);
    }
    
    /**
     * Searches ServiceNow object(s) based on the query limiting number of objects returned in the result.
     *
     * To learn more about how to write ServiceNow query, refer to:
     *
     * http://wiki.servicenow.com/index.php?title=Embedded:Encoded_Query_Strings
     *
     * <pre>
     * {@code
     *  try
     *  {
     *      String objectType = "incident";
     *      String query = "short_descriptionLIKEnot working^category=hardware";
     *      String orderBy = "sys_updated_on";
     *      boolean isAscending = false;
     *      int pageLimit = 10;
     *      List<Map<String, String>> incidents = ServiceNowAPI.searchObject(objectType, query, orderBy, isAscending, pageLimit, null, null);
     *
     *      for(Map<String, String> incident : incidents)
     *      {
     *          int i = 1;
     *          System.out.printf("Incident# %d:", i++);
     *          for(String key : incident.keySet())
     *          {
     *              System.out.printf("Property:%s, Value:%s\n", key, incident.get(key));
     *          }
     *      }
     *  }
     *  catch (Exception ex)
     *  {
     *      //Do something good with this error.
     *      ex.printStackTrace();
     *  }
     *  }
     * </pre>
     *
     * @param objectType
     *            type of ServiceNow object to select by System Id. for e.g. "incident", "cmdb_ci" etc.
     * @param query
     *            Query in ServiceNow format to filter records. (optional)
     *            Use with caution as records returned could be large in absence of query.        
     * @param orderBy
     *            Order by property name. (optional)
     * @param isAscending
     *            true = ascending, false = descending
     * @param pageLimit
     *            Maximum number of objects returned per page.
     * @param username
     *            if not provided uses the gateway configuration.
     * @param p_assword
     *            ties with the username. Gateway configuration is used if not
     *            provided.
     * @return a {@link List} of {@link Map} which contain properties for all
     *         the objects of specified type matching specified criteria.
     * @throws Exception
     */
    public static List<Map<String, String>> searchObjectWithPageLimit(String objectType, String query, String orderBy, boolean isAscending, int pageLimit, String username, String p_assword) throws Exception
    {
        return instance.searchObjectWithPageLimit(objectType, query, orderBy, isAscending, pageLimit, username, p_assword);
    }
    
    
    
    /**
     * This method inserts/updates incoming data into a specified staging table and triggers transformation based on predefined transform maps in the import set table
     * @param stagingTableName - Required. Name of the import set table that is created in ServiceNow
     * @param params - Required. Key Value pair of parameter names and their values.
     * @param username 
     *          if not provided uses the gateway configuration.
     * @param p_assword
     *           ties with the username. Gateway configuration is used if not
     *            provided.
     * @throws Exception
     */
    public static String importSetApi(String stagingTableName, HashMap<String, String> params, String username, String p_assword) throws Exception
    {
       return instance.importSetApi(stagingTableName, params, username, p_assword);
    }
    
    
}
