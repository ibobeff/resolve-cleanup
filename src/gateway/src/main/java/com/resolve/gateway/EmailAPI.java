/******************************************************************************
* (C) Copyright 2014
*
* Resolve Systems, LLC
*
* All rights reserved
* This software is distributed under license and may not be used, copied,
* modified, or distributed without the express written permission of
* Resolve Systems, LLC.
*
******************************************************************************/

package com.resolve.gateway;

import java.util.Map;

import com.resolve.rsbase.MainBase;
import com.resolve.gateway.database.DBGateway;
import com.resolve.gateway.email.EmailGateway;
import com.resolve.util.StringUtils;

public class EmailAPI
{
    private static EmailGateway instance = EmailGateway.getInstance();

    /**
     * Uses the given name to retrieve a {@link Map} of properties from the
     * current {@link EmailGateway} instance's {@link NameProperties} object. If
     * the {@link Map} isn't null, it returns the {@link Object} associated with
     * the given key parameter.
     * 
     * @param name
     *            the key associated with the name property {@link Map} in
     *            {@link EmailGateway}'s {@link NameProperties} object. Cannot be
     *            null or empty.
     * @param key
     *            the key in the property {@link Map} returned from
     *            {@link EmailGateway}'s {@link NameProperties}
     * @return the {@link Object} value associated with the given key in the
     *         property {@link Map} from {@link EmailGateway}'s
     *         {@link NameProperties} object
     */
    public static Object getNameObject(String name, String key)
    {
        return instance.getNameProperties().getNameObject(name, key);
    }

    /**
     * Uses the given name to retrieve a {@link Map} of properties from the
     * current {@link EmailGateway} instance's {@link NameProperties} object. If
     * the {@link Map} isn't null, it adds the given key and value parameters.
     * 
     * @param name
     *            the key associated with the name property {@link Map} in
     *            {@link EmailGateway}'s {@link NameProperties} object. Cannot be
     *            null or empty.
     * @param key
     *            the key in the property {@link Map} returned from
     *            {@link EmailGateway}'s {@link NameProperties}
     * @param value
     *            the value to associate with the given key parameter
     * @throws Exception
     */
    public static void setNameObject(String name, String key, Object value) throws Exception
    {
        Map nameProperties = getNameProperties(name);
        if(nameProperties != null)
        {
            nameProperties.put(key, value);
            setNameProperties(name, nameProperties, true);
        }
    }

    /**
     * Uses the given name parameter to retrieve a {@link Map} of properties
     * from the current {@link EmailGateway} instance's {@link NameProperties}
     * object.
     *
     * @param name
     *            the key associated with the name property {@link Map} in
     *            {@link EmailGateway}'s {@link NameProperties} object. Cannot be
     *            null or empty.
     * @return the Map associated with the name parameter in the current
     *         {@link EmailGateway} instance's {@link NameProperties} object.
     * @throws Exception
     */
    public static Map getNameProperties(String name) throws Exception
    {
        return instance.getNameProperties().getNameProperties(name);
    }

    /**
     * Maps the given name to the given properties Map in the current
     * {@link EmailGateway} instance's {@link NameProperties} object. This change
     * will be pushed onto the RSControl for persistence.
     *
     * @param name
     *            the key associated with the name property {@link Map} in
     *            {@link EmailGateway}'s {@link NameProperties} object. Cannot be
     *            null or empty.
     * @param properties
     *            a {@link Map} of DBGateway properties to add to the current
     *            {@link EmailGateway} instance's {@link NameProperties} object.
     * @throws Exception
     */
    public static void setNameProperties(String name, Map properties) throws Exception
    {
        setNameProperties(name, properties, true);
    }

    /**
     * Maps the given name to the given properties Map in the current
     * {@link EmailGateway} instance's {@link NameProperties} object. The doSave
     * parameter allows you to decide whether this change is pushed to RSControl
     * for persistence
     *
     * @param name
     *            the key associated with the name property {@link Map} in
     *            {@link EmailGateway}'s {@link NameProperties} object. Cannot be
     *            null or empty.
     * @param properties
     *            a {@link Map} of DBGateway properties to add to the current
     *            {@link EmailGateway} instance's {@link NameProperties} object.
     * @param doSave
     *            a boolean value which designates whether this change should be
     *            pushed onto RSControl for persistence
     * @throws Exception
     */
    public static void setNameProperties(String name, Map properties, boolean doSave) throws Exception
    {
        if (StringUtils.isBlank(name))
        {
            throw new Exception("'name' parameter must be passed");
        }
        else
        {
            instance.setNameProperties(name, properties, doSave);
        }
    }

    /**
     * Uses the given name parameter to remove a {@link Map} of properties from
     * the current {@link EmailGateway} instance's {@link NameProperties} object.
     *
     * @param name
     *            the key associated with the name property {@link Map} in
     *            {@link EmailGateway}'s {@link NameProperties} object. Cannot be
     *            null or empty.
     * @throws Exception
     */
    public static void removeNameProperties(String name) throws Exception
    {
        if (StringUtils.isBlank(name))
        {
            throw new Exception("'name' parameter must be passed");
        }
        else
        {
            instance.removeNameProperties(name);
        }
    }

    /**
     * Saves the configurations in the RSRemote.
     *
     * @throws Exception
     */
    public static void saveConfig() throws Exception
    {
        MainBase.main.callSaveConfig();
    }
} // EmailAPI
