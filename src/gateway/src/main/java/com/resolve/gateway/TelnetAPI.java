/******************************************************************************
 * (C) Copyright 2014
 *
 * Resolve Systems, LLC
 *
 * All rights reserved This software is distributed under license and may not be
 * used, copied, modified, or distributed without the express written permission
 * of Resolve Systems, LLC.
 *
 ******************************************************************************/

package com.resolve.gateway;

public class TelnetAPI
{
    /**
     * Checks out a TelnetGatewayConnection given the ipAddress parameter
     * 
     * @param ipAddress
     *            the IP address of the server you're trying to connect to.
     * @return a {@link TelnetGatewayConnection} object
     * @throws Exception
     *             when a error occurs while trying to establish a Telnet
     *             gateway connection
     */
    public static TelnetGatewayConnection checkOutConnection(String ipAddress) throws Exception
    {
        return MTelnet.checkOutConnection(ipAddress);
    }

    /**
     * Checks out a TelnetGatewayConnection given the ipAddress and port ID
     * parameter
     * 
     * @param ipAddress
     *            the IP address of the server you're trying to connect to.
     * @param port
     *            the port ID
     * @return a {@link TelnetGatewayConnection} object
     * @throws Exception
     *             when a error occurs while trying to establish a Telnet
     *             gateway connection
     */
    public static TelnetGatewayConnection checkOutConnection(String ipAddress, int port) throws Exception
    {
        return MTelnet.checkOutConnection(ipAddress, port);
    }

    /**
     * Checks out a TelnetGatewayConnection given the ipAddress, port ID,
     * terminalType, and echoOption parameter
     * 
     * @param ipAddress
     *            the IP address of the server you're trying to connect to.
     * @param port
     *            the port ID
     * @param terminalType
     *            the terminal type of the Telnet Connection. Types includes:
     *            VT100, VT52, ANSI, or VTNT.
     * @return a {@link TelnetGatewayConnection} object
     * @throws Exception
     *             when a error occurs while trying to establish a Telnet
     *             gateway connection
     */
    public static TelnetGatewayConnection checkOutConnection(String ipAddress, int port, String terminalType) throws Exception
    {
        return MTelnet.checkOutConnection(ipAddress, port, terminalType);
    }

    /**
     * Checks out a TelnetGatewayConnection given the ipAddress, port ID,
     * terminalType, echoOption, and supressGAOption parameter
     * 
     * @param ipAddress
     *            the IP address of the server you're trying to connect to.
     * @param port
     *            the port ID
     * @param terminalType
     *            the terminal type of the Telnet Connection. Types includes:
     *            VT100, VT52, ANSI, or VTNT.
     * @param suppressGAOption
     *            a boolean value which enables a Telnet Connection's go ahead
     *            option
     * @param echoOption
     *            a boolean value which enables a Telnet Connection's echo
     *            option
     * @return a {@link TelnetGatewayConnection} object
     * @throws Exception
     *             when a error occurs while trying to establish a Telnet
     *             gateway connection
     */
    public static TelnetGatewayConnection checkOutConnection(String ipAddress, int port, String terminalType, boolean suppressGAOption, boolean echoOption) throws Exception
    {
        return MTelnet.checkOutConnection(ipAddress, port, terminalType, suppressGAOption, echoOption);
    }

    /**
     * Checks in a telnet connection using the given TelnetGatewayConnection
     * object, which holds all necessary initialization parameters for a new Telnet
     * connection
     * 
     * @param connection
     *            the {@link TelnetGatewayConnection} object
     * @throws Exception
     *             when a error occurs while trying to establish a Telnet
     *             gateway connection
     */
    public static void checkInConnection(TelnetGatewayConnection connection) throws Exception
    {
        MTelnet.checkInConnection(connection);
    }
}
