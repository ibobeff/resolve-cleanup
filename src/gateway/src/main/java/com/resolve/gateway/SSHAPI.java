/******************************************************************************
* (C) Copyright 2014
*
* Resolve Systems, LLC
*
* All rights reserved
* This software is distributed under license and may not be used, copied,
* modified, or distributed without the express written permission of
* Resolve Systems, LLC.
*
******************************************************************************/

package com.resolve.gateway;

import java.util.List;

public class SSHAPI
{
    /*
     * USERNAME/PASSWORD
     */
    /**
     * Checks out a {@link SSHGatewayConnection} from the pool managed by this
     * gateway. Uses default port and timeout values from the Gateway
     * configuration.
     * 
     * <pre>
     * {@code
     * import com.resolve.gateway.SSHAPI;
     * 
     *       SSHGatewayConnection conn = null;
     * 
     *         String result = "";
     * 
     *         try
     *         {
     *             String username = "resolve";
     *             String password = "resolve";
     *             String ipaddress = "99.99.99.99"; // the target server
     * 
     *             String prompt = "$"; // the prompt expected from the connection to
     *                                  // the server.
     * 
     *             conn = SSHAPI.checkOutConnection(username, password, ipaddress);
     *             result += conn.expect(prompt);
     * 
     *             conn.send("pwd");
     *             result += conn.expect("/home/resolve");
     * 
     *             if (conn.isLastExpectTimeout())
     *             {
     *                 result += "\nFAIL - Timeout on login";
     *             }
     *         }
     *         catch (Exception e)
     *         {
     *             result += "\nFAIL - Unexpected Exception: " + e.getMessage();
     *             result += "\n" + e.getClass() + ": " + e.getMessage();
     *         }
     *         finally
     *         {
     *             // do not forget to return the connection at the end.
     *             try
     *             {
     *                 SSHAPI.checkInConnection(conn);
     *             }
     *             catch (Exception e)
     *             {
     *                 //ignore
     *             }
     *         }
     * 
     *         // do something useful with the result
     *         System.out.println(result);
     * }
     * </pre>
     * 
     * @param username
     *            to be used for the connection, must provide.
     * @param password
     *            to be used for the connection, must provide.
     * @param ipAddress
     *            of the host to connect, must provide.
     * @return an {@link SSHGatewayConnection}
     * @throws Exception
     */
    public static SSHGatewayConnection checkOutConnection(String username, String password, String ipAddress) throws Exception
    {
        return MSSH.checkOutConnection(username, password, ipAddress);
    }

    /**
     * Checks out a {@link SSHGatewayConnection} from the pool managed by this
     * gateway. Uses default timeout value from the Gateway configuration.
     * 
     * <pre>
     * {@code
     * import com.resolve.gateway.SSHAPI;
     * 
     *       SSHGatewayConnection conn = null;
     * 
     *         String result = "";
     * 
     *         try
     *         {
     *             String username = "resolve";
     *             String password = "resolve";
     *             String ipaddress = "99.99.99.99"; // the target server
     *             int port = 22; // SSH port on the target server.
     * 
     *             String prompt = "$"; // the prompt expected from the connection to
     *                                  // the server.
     * 
     *             conn = SSHAPI.checkOutConnection(username, password, ipaddress, port);
     *             result += conn.expect(prompt);
     * 
     *             conn.send("pwd");
     *             result += conn.expect("/home/resolve");
     * 
     *             if (conn.isLastExpectTimeout())
     *             {
     *                 result += "\nFAIL - Timeout on login";
     *             }
     *         }
     *         catch (Exception e)
     *         {
     *             result += "\nFAIL - Unexpected Exception: " + e.getMessage();
     *             result += "\n" + e.getClass() + ": " + e.getMessage();
     *         }
     *         finally
     *         {
     *             // do not forget to return the connection at the end.
     *             try
     *             {
     *                 SSHAPI.checkInConnection(conn);
     *             }
     *             catch (Exception e)
     *             {
     *                 //ignore
     *             }
     *         }
     * 
     *         // do something useful with the result
     *         System.out.println(result);
     * }
     * </pre>
     * 
     * @param username
     *            to be used for the connection, must provide.
     * @param password
     *            to be used for the connection, must provide.
     * @param ipAddress
     *            of the host to connect, must provide.
     * @param port
     *            to connect to.
     * @return an {@link SSHGatewayConnection}
     * @throws Exception
     */
    public static SSHGatewayConnection checkOutConnection(String username, String password, String ipAddress, int port) throws Exception
    {
        return MSSH.checkOutConnection(username, password, ipAddress, port);
    }

    /*
     * PUBLIC KEY
     */
    /**
     * Public/Private Key based API that checks out a
     * {@link SSHGatewayConnection} from the pool managed by this gateway. Uses
     * default port and timeout value from the Gateway configuration.
     * 
     * <pre>
     * {@code
     * import com.resolve.gateway.SSHAPI;
     * 
     *       SSHGatewayConnection conn = null;
     * 
     *         String result = "";
     * 
     *         try
     *         {
     *             String username = "resolve";
     *             String filename = "/usr/tmp/mykeys.crt";
     *             String passphrase = "cangeit";
     *             String ipaddress = "99.99.99.99"; // the target server
     * 
     *             String prompt = "$"; // the prompt expected from the connection to
     *                                  // the server.
     * 
     *             conn = SSHAPI.checkOutConnection(username, filename, passphrase, ipaddress);
     *             result += conn.expect(prompt);
     * 
     *             conn.send("pwd");
     *             result += conn.expect("/home/resolve");
     * 
     *             if (conn.isLastExpectTimeout())
     *             {
     *                 result += "\nFAIL - Timeout on login";
     *             }
     *         }
     *         catch (Exception e)
     *         {
     *             result += "\nFAIL - Unexpected Exception: " + e.getMessage();
     *             result += "\n" + e.getClass() + ": " + e.getMessage();
     *         }
     *         finally
     *         {
     *             // do not forget to return the connection at the end.
     *             try
     *             {
     *                 SSHAPI.checkInConnection(conn);
     *             }
     *             catch (Exception e)
     *             {
     *                 //ignore
     *             }
     *         }
     * 
     *         // do something useful with the result
     *         System.out.println(result);
     * }
     * </pre>
     * 
     * @param username
     *            to be used for the connection, must provide.
     * @param filename
     *            absolute location (e.g., /usr/tmp/mykeys.crt) must be
     *            provided. The file contains a DSA or RSA private key of the
     *            user in OpenSSH key format (PEM, you can't miss the
     *            "-----BEGIN DSA PRIVATE KEY-----" or
     *            "-----BEGIN RSA PRIVATE KEY-----" tag).
     * @param passphrase
     *            , if the PEM file is encrypted then must provided. Otherwise,
     *            this argument will be ignored and can be set to null.
     * @param ipAddress
     *            of the host to connect, must provide.
     * @return an {@link SSHGatewayConnection}
     * @throws Exception
     */
    public static SSHGatewayConnection checkOutConnection(String username, String filename, String passphrase, String ipAddress) throws Exception
    {
        return MSSH.checkOutConnection(username, filename, passphrase, ipAddress);
    }

    /**
     * Public/Private Key based API that checks out a
     * {@link SSHGatewayConnection} from the pool managed by this gateway. Uses
     * default timeout value from the Gateway configuration.
     * 
     * <pre>
     * {@code
     * import com.resolve.gateway.SSHAPI;
     * 
     *       SSHGatewayConnection conn = null;
     * 
     *         String result = "";
     * 
     *         try
     *         {
     *             String username = "resolve";
     *             String filename = "/usr/tmp/mykeys.crt";
     *             String passphrase = "cangeit";
     *             String ipaddress = "99.99.99.99"; // the target server
     *             int port = 22; // SSH port on the target server.
     * 
     *             String prompt = "$"; // the prompt expected from the connection to
     *                                  // the server.
     * 
     *             conn = SSHAPI.checkOutConnection(username, filename, passphrase, ipaddress, port);
     *             result += conn.expect(prompt);
     * 
     *             conn.send("pwd");
     *             result += conn.expect("/home/resolve");
     * 
     *             if (conn.isLastExpectTimeout())
     *             {
     *                 result += "\nFAIL - Timeout on login";
     *             }
     *         }
     *         catch (Exception e)
     *         {
     *             result += "\nFAIL - Unexpected Exception: " + e.getMessage();
     *             result += "\n" + e.getClass() + ": " + e.getMessage();
     *         }
     *         finally
     *         {
     *             // do not forget to return the connection at the end.
     *             try
     *             {
     *                 SSHAPI.checkInConnection(conn);
     *             }
     *             catch (Exception e)
     *             {
     *                 //ignore
     *             }
     *         }
     * 
     *         // do something useful with the result
     *         System.out.println(result);
     * }
     * </pre>
     * 
     * @param username
     *            to be used for the connection, must provide.
     * @param filename
     *            absolute location (e.g., /usr/tmp/mykeys.crt) must be
     *            provided. The file contains a DSA or RSA private key of the
     *            user in OpenSSH key format (PEM, you can't miss the
     *            "-----BEGIN DSA PRIVATE KEY-----" or
     *            "-----BEGIN RSA PRIVATE KEY-----" tag).
     * @param passphrase
     *            , if the PEM file is encrypted then must provided. Otherwise,
     *            this argument will be ignored and can be set to null.
     * @param ipAddress
     *            of the host to connect, must provide.
     * @param port
     *            to connect to.
     * @return an {@link SSHGatewayConnection}
     * @throws Exception
     */
    public static SSHGatewayConnection checkOutConnection(String username, String filename, String passphrase, String ipAddress, int port) throws Exception
    {
        return MSSH.checkOutConnection(username, filename, passphrase, ipAddress, port);
    }

    /*
     * INTERACTIVE
     */
    /**
     * Checks out a {@link SSHGatewayConnection} from the pool managed by this
     * gateway. Uses default port and timeout value from the Gateway
     * configuration.
     * 
     * @param username
     *            to be used for the connection, must provide.
     * @param responses
     *            List of String, responses to be sent for keyboard interactive connections.
     * @param ipAddress
     *            of the host to connect, must provide.
     * @return an {@link SSHGatewayConnection}
     * @throws Exception
     */
    public static SSHGatewayConnection checkOutConnection(String username, List<String> responses, String ipAddress) throws Exception
    {
        return MSSH.checkOutConnection(username, responses, ipAddress);
    }

    /**
     * Checks out a {@link SSHGatewayConnection} from the pool managed by this
     * gateway. Uses default timeout value from the Gateway configuration.
     * 
     * 
     * @param username
     *            to be used for the connection, must provide.
     * @param responses
     *            must be provided.
     * @param ipAddress
     *            of the host to connect, must provide.
     * @param port
     *            to connect to.
     * @return an {@link SSHGatewayConnection}
     * @throws Exception
     */
    public static SSHGatewayConnection checkOutConnection(String username, List responses, String ipAddress, int port) throws Exception
    {
        return MSSH.checkOutConnection(username, responses, ipAddress);
    }

    /*
     * NONE - no SSH authentication
     */
    /**
     * Checks out a {@link SSHGatewayConnection} from the pool managed by this
     * gateway. No SSH authentication is done so it'll be the clients job to
     * authenticate once the object is provided. Uses default port and timeout
     * value from the Gateway configuration.
     * 
     * 
     * @param username
     *            to be used for the connection, must provide.
     * @param ipAddress
     *            of the host to connect, must provide.
     * @return an {@link SSHGatewayConnection}
     * @throws Exception
     */
    public static SSHGatewayConnection checkOutConnection(String username, String ipAddress) throws Exception
    {
        return MSSH.checkOutConnection(username, ipAddress);
    }

    /**
     * Checks out a {@link SSHGatewayConnection} from the pool managed by this
     * gateway. No SSH authentication is done so it'll be the clients job to
     * authenticate once the object is provided. Uses default timeout value from
     * the Gateway configuration.
     * 
     * 
     * @param username
     *            to be used for the connection, must provide.
     * @param ipAddress
     *            of the host to connect, must provide.
     * @param port
     *            to connect to.
     * @return an {@link SSHGatewayConnection}
     * @throws Exception
     */
    public static SSHGatewayConnection checkOutConnection(String username, String ipAddress, int port) throws Exception
    {
        return MSSH.checkOutConnection(username, ipAddress, port);
    }

    /**
     * Check in the connection to the pool. This method must be used once the
     * {@link SSHGatewayConnection} object use si completed.
     * 
     * @param sshConnect
     *            object that was checked out using one the the checkOutxxx
     *            methods.
     * @throws Exception
     */
    public static void checkInConnection(SSHGatewayConnection sshConnect) throws Exception
    {
        MSSH.checkInConnection(sshConnect);
    }
}
