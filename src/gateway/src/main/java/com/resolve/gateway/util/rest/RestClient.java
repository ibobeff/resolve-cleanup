package com.resolve.gateway.util.rest;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.ByteArrayOutputStream;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.net.CookieManager;
import java.net.CookiePolicy;
import java.net.CookieStore;
import java.net.HttpCookie;
import java.net.HttpURLConnection;
import java.net.URI;
import java.net.URL;
import java.net.URLConnection;
import java.net.URLEncoder;
import java.nio.charset.StandardCharsets;
import java.security.cert.X509Certificate;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.StringTokenizer;

import javax.net.ssl.HttpsURLConnection;
import javax.net.ssl.SSLContext;
import javax.net.ssl.TrustManager;
import javax.net.ssl.X509TrustManager;

import com.resolve.util.Log;
import com.resolve.util.StringUtils;

public class RestClient
{

    protected boolean basicAuth = true;
    private final String baseUrl;
    
    // Response status and headers 
    protected int statusCode;
    protected String reasonPhrase;
    protected String responseEntity;
    protected Map<String, String> responseHeaders = new HashMap<String, String>();
    protected String user = null;
    protected String pass = null;

    private static final String lineSeparator = System.getProperty("line.separator");
    private static final String COOKIE = "Cookie";
    private static final String PATH = "path";
    private static final String SET_COOKIE = "Set-Cookie";
    private static final String COOKIE_VALUE_DELIMITER = ";";
    private static final char NAME_VALUE_SEPARATOR = '=';
    private static String TLS = "TLS";
    
    private static volatile CookieManager cookieManager = null;
    protected static volatile CookieStore cookieStore = null;
    
    protected boolean selfSigned = true;
    protected boolean trustAll = false;
    protected boolean cookieEnabled = true;
    
    static {
        cookieManager = new CookieManager();
        cookieManager.setCookiePolicy(CookiePolicy.ACCEPT_ALL);
        cookieStore = cookieManager.getCookieStore();
// The CookieHandler honors the "httponly" portion of the cookie; that is why you don't see the value for the Set-Cookie header when using the CookieHandler.
//      CookieHandler.setDefault(cookieManager);

        String version = System.getProperty("java.version");
        int pos = version.indexOf('.');
        pos = version.indexOf('.', pos+1);
        double v = Double.parseDouble (version.substring (0, pos));
        if(v > 1.7)
            TLS = "TLSv1.2";
    }
    
    
    /**
     * Constructor with basic information and user preferences
     * 
     * @param baseUrl: contains protocol (HTTP/HTTPS), host name or ip, port number
     * @param username: used for Basic Authentication or login to get a token, can be blank when token is provided or token is set in the cookie store
     * @param password: used for Basic Authentication or login to get a token, can be blank when token is provided or token is set in the cookie store
     * @param token: used for each REST call by explicitly embedded it with the key name of "token" in HTTPRequest when cookie store is not used.
     *        If the key name is not "token", embed the key and value in the request params. 
     *        Token can also be set in the request headers or request params or in the body payload by the caller based on the server needs.
     *        If the server supports cookies, no token needs to be embedded.
     * @param baseAuth: if true, username and password will be encode with Base64 and embedded into HTTP header for authentication
     * @param timeout: timeout for HTTP/HTTPS connection
     * @param selfSigned: HTTPSURLConnection will be eatablished when server provides a self-signed certificate, no hsst name will be verified
     * @param trustAll: HTTPSURLConnection will be esatblished with fake certificates and even without any certificate is provided
     * @throws Exception
     */
    public RestClient(String baseUrl, String user, String pass, boolean basicAuth, boolean cookieEnabled, boolean selfSigned, boolean trustAll) {
        
        this.baseUrl = baseUrl;
        this.user = user;
        this.pass = pass;
        this.basicAuth = basicAuth;
        this.cookieEnabled = cookieEnabled;
        this.selfSigned = selfSigned;
        this.trustAll = trustAll;
    }
    
    /**
     * Call REST API with the uri defined by the server to get the token back.
     * The user credential can be embeded in HTTP header with Basic Authentication or in the request params or in the payload body based on the server configuration.
     * 
     * @param uri: REST interface/path defined by the server
     * @param body: data in the format of the content type that will be encoded with UTF-8 before sending
     * @param headers: any header properties caller needs to add, such as "Cache-Control": "no-cache"
     * @param contentType: tell the server about the content type encoding for the HTTP request, such as text/xml, application/xml, application/json, etc.
     * @param accept: tell the server what content type can be accepted, such as text/xml, application/xml, application/json
     * @return: a response string with token embedded
     * @throws Exception
     */
    public String login(String uri, String method, Map<String, String> params, String body, Map<String, String>headers, String contentType, String accept) throws Exception {
        
        if(StringUtils.isBlank(method))
            throw new Exception("Method cannot be blank.");

        String response = "";
        
        // Embed the user credential in params
        if(method.equalsIgnoreCase("GET"))
            response = callGet(uri, params, headers, accept);
        
        else if(method.equalsIgnoreCase("POST"))
            response = callPost(uri, params, body, headers, contentType, accept);
        
        else
            throw new Exception("Use GET or POST method for login.");
        
        // The token must be parsed by caller based on the format of the response.
        return response;
    }
    
    /**
     * Call REST API using HTTP GET method.
     * If token value is not empty, token will be embedded as key of "token" for each REST call.
     * If basicAuth is true, Basic Authentication will be used for each REST call.
     * If the method call fails with token expired or unauthorized, caller needs to catch the exception and call login again to get a new token.
     * 
     * @param uri: REST interface/path defined by the server
     * @param params: the query key/value pairs to send to the server
     * @param headers: any header properties caller needs to add, such as "Cache-Control": "no-cache"
     * @param accept: tell the server what content type can be accepted, such as text/xml, application/xml, application/json
     * @return: the response is string or binary
     * @throws Exception
     */
    @Deprecated
    public String callGet(String uri, Map<String, String> params, Map<String, String>headers, String accept) throws Exception {
        
        if(headers == null)
            headers = new HashMap<String, String>();
        
        if(StringUtils.isNotEmpty(accept))
            headers.put("Accept", accept);
        
        return callGet(uri, params, headers);
    }
    
    public String callGet(String uri, Map<String, String> params, Map<String, String>headers) throws Exception {

        HttpURLConnection conn = null;
        InputStream input = null;
        InputStream error = null;
        BufferedReader bufferedReader = null;
        StringBuilder path = new StringBuilder();
        StringBuilder response = new StringBuilder();

        if(StringUtils.isNotBlank(uri))
            path.append(uri);

        String url = "";
        
        if(params != null && params.size() != 0) {
            if(uri.indexOf("?") != -1)
                path.append("&");
            else
                path.append("?");
            
            for(Iterator<String> it=params.keySet().iterator(); it.hasNext();) {
                String key = it.next();
                String value = params.get(key);
                if(value == null)
                    continue;
                path.append(key).append("=").append(URLEncoder.encode(value, StandardCharsets.UTF_8.name())).append("&");
            }
            
            url = path.toString();
            url = url.substring(0, url.length()-1);
        }
        
        else
            url = path.toString();
        
        try {
            conn = getConnection(url, headers);
            
            conn.setRequestMethod("GET");
            
            if(basicAuth)
                doBasicAuthentication(conn);

            conn.setDoInput(true);
            
            // Get response code
            statusCode = conn.getResponseCode();
            
            // Get Response message
            reasonPhrase = conn.getResponseMessage();
            
            // Get response headers
            Map<String, List<String>> headerFields = conn.getHeaderFields();
            
            String domain = conn.getURL().getHost();
            
            for(Iterator<String> it=headerFields.keySet().iterator(); it.hasNext();) {
                String key = it.next();
                
                if(StringUtils.isBlank(key))
                    continue;
                
                if(!key.equalsIgnoreCase(SET_COOKIE)) {
                    List<String> values = headerFields.get(key);
                    StringBuilder sb = new StringBuilder();
                    
                    if(values != null && values.size() > 0) {                
                        int size = values.size();
                        
                        for(int i=0; i<size; i++) {

                            String value = values.get(i);
                            sb.append(value);
                            if(i != size-1)
                                sb.append(",");
                        }
                    }
                    
                    responseHeaders.put(key, sb.toString());
                }

                else {
                    List<String> headerValues = headerFields.get(key);
                    
                    StringTokenizer st = new StringTokenizer(headerValues.get(0), COOKIE_VALUE_DELIMITER);

                    HttpCookie cookie = null;
                    
                    if (st.hasMoreTokens()) {
                        String token  = st.nextToken();
                        String name = token.substring(0, token.indexOf(NAME_VALUE_SEPARATOR));
                        String value = token.substring(token.indexOf(NAME_VALUE_SEPARATOR) + 1, token.length());
                        cookie = new HttpCookie(name, value);
                
                        while (st.hasMoreTokens()) {
                            token  = st.nextToken();
                            if(StringUtils.isBlank(token))
                                continue;
            
                            int index = token.indexOf(NAME_VALUE_SEPARATOR);
                            name = token;
                            value = "";
                            
                            if(index > 0) {
                                name = token.substring(0, index).trim();
                                value = token.substring(index + 1, token.length());
            
                                if(name.equalsIgnoreCase(PATH))
                                    cookie.setPath(value);
                            }
                            
                            else
                                cookie.setComment(name);
                        }
            
                        cookie.setDomain(domain);
                        cookieStore.add(new URI(domain), cookie);
                    }
                }
            }
            
            input = conn.getInputStream();

            bufferedReader = new BufferedReader(new InputStreamReader(input));

            String line = null;
            while ((line = bufferedReader.readLine()) != null)
                response.append(line).append(lineSeparator);
            
//            conn.getContent();
        } catch(Exception e) {
//            e.printStackTrace();
            if(conn == null)
                throw e;
            
            statusCode = conn.getResponseCode();
            reasonPhrase = conn.getResponseMessage();
            Log.log.error("Response code is: " + statusCode);
            Log.log.error("Response message is: " + reasonPhrase);

            String errorMsg = "";
            
            try {
                error = conn.getErrorStream();

                if(bufferedReader != null)
                    bufferedReader.close();
                
                bufferedReader = new BufferedReader(new InputStreamReader(error));
                
                String line = null;
                StringBuilder sb = new StringBuilder();
                while ((line = bufferedReader.readLine()) != null)
                    sb.append(line);
                
                errorMsg = sb.toString();
            } catch(Exception eee) {
                Log.log.debug(eee.getMessage(), eee);
            }
            
            String message = e.getMessage();
            if(StringUtils.isNotEmpty(message))
                message = message + lineSeparator + errorMsg;
            else
                message = errorMsg;
            
            if(StringUtils.isEmpty(message))
                message = "";
            
            Exception ee = new Exception(message);
            ee.setStackTrace(e.getStackTrace());
            
            Log.log.error(ee.getMessage(), ee);
            throw ee;
        } finally {
            try {
                if(input != null)
                    input.close();
                if(error != null)
                    error.close();
                if(bufferedReader != null)
                    bufferedReader.close();
                if(conn != null)
                    conn.disconnect();
            } catch(Exception ee) {}
        }
        
        return response.toString();
    }
    
    @Deprecated
    public byte[] callGetBinary(String uri, Map<String, String> params, Map<String, String>headers, String accept) throws Exception {
        
        if(headers == null)
            headers = new HashMap<String, String>();
        
        if(StringUtils.isNotEmpty(accept))
            headers.put("Accept", accept);
        
        return callGetBinary(uri, params, headers);
    }
    
    public byte[] callGetBinary(String uri, Map<String, String> params, Map<String, String>headers) throws Exception {
        
        HttpURLConnection conn = null;
        InputStream input = null;
        InputStream error = null;
        BufferedReader bufferedReader = null;
        StringBuilder path = new StringBuilder();
        byte[] response = null;

        if(StringUtils.isNotBlank(uri))
            path.append(uri);

        String url = "";
        
        if(params != null && params.size() != 0) {
            if(uri.indexOf("?") != -1)
                path.append("&");
            else
                path.append("?");
            
            for(Iterator<String> it=params.keySet().iterator(); it.hasNext();) {
                String key = it.next();
                String value = params.get(key);
                if(value == null)
                    continue;
                path.append(key).append("=").append(URLEncoder.encode(value, StandardCharsets.UTF_8.name())).append("&");
            }
            
            url = path.toString();
            url = url.substring(0, url.length()-1);
        }
        
        else
            url = path.toString();
        
        try {
            conn = getConnection(url, headers);
            
            conn.setRequestMethod("GET");
            
            if(basicAuth)
                doBasicAuthentication(conn);

            conn.setDoInput(true);
            
            // Get response code
            statusCode = conn.getResponseCode();
            
            // Get Response message
            reasonPhrase = conn.getResponseMessage();
            
            // Get response headers
            Map<String, List<String>> headerFields = conn.getHeaderFields();
            
            String domain = conn.getURL().getHost();
            
            for(Iterator<String> it=headerFields.keySet().iterator(); it.hasNext();) {
                String key = it.next();
                
                if(StringUtils.isBlank(key))
                    continue;
                
                if(!key.equalsIgnoreCase(SET_COOKIE)) {
                    List<String> values = headerFields.get(key);
                    StringBuilder sb = new StringBuilder();
                    
                    if(values != null && values.size() > 0) {                
                        int size = values.size();
                        
                        for(int i=0; i<size; i++) {

                            String value = values.get(i);
                            sb.append(value);
                            if(i != size-1)
                                sb.append(",");
                        }
                    }
                    
                    responseHeaders.put(key, sb.toString());
                }

                else {
                    List<String> headerValues = headerFields.get(key);
                    
                    StringTokenizer st = new StringTokenizer(headerValues.get(0), COOKIE_VALUE_DELIMITER);

                    HttpCookie cookie = null;
                    
                    if (st.hasMoreTokens()) {
                        String token  = st.nextToken();
                        String name = token.substring(0, token.indexOf(NAME_VALUE_SEPARATOR));
                        String value = token.substring(token.indexOf(NAME_VALUE_SEPARATOR) + 1, token.length());
                        cookie = new HttpCookie(name, value);
                
                        while (st.hasMoreTokens()) {
                            token  = st.nextToken();
                            if(StringUtils.isBlank(token))
                                continue;
    
                            int index = token.indexOf(NAME_VALUE_SEPARATOR);
                            name = token;
                            value = "";
                            
                            if(index > 0) {
                                name = token.substring(0, index).trim();
                                value = token.substring(index + 1, token.length());
    
                                if(name.equalsIgnoreCase(PATH))
                                    cookie.setPath(value);
                            }
                            
                            else
                                cookie.setComment(name);
                        }
    
                        cookie.setDomain(domain);
                        cookieStore.add(new URI(domain), cookie);
                    }
                }
            }
            
            input = conn.getInputStream();

            byte[] buffer = new byte[1024];
  
            ByteArrayOutputStream output = new ByteArrayOutputStream();

            int line = 0;
            while ((line = input.read(buffer)) != -1) {
                output.write(buffer, 0, line);
            }
            
            output.flush();
            output.close();
            response = output.toByteArray();
            
//            conn.getContent();
        } catch(Exception e) {
//            e.printStackTrace();
            statusCode = conn.getResponseCode();
            reasonPhrase = conn.getResponseMessage();
            Log.log.error("Response code is: " + statusCode);
            Log.log.error("Response message is: " + reasonPhrase);

            String errorMsg = "";
            
            try {
                error = conn.getErrorStream();

                if(bufferedReader != null)
                    bufferedReader.close();
                
                bufferedReader = new BufferedReader(new InputStreamReader(error));
                
                String line = null;
                StringBuilder sb = new StringBuilder();
                while ((line = bufferedReader.readLine()) != null)
                    sb.append(line);
                
                errorMsg = sb.toString();
            } catch(Exception eee) {
                Log.log.debug(eee.getMessage(), eee);
            }
            
            String message = e.getMessage();
            if(StringUtils.isNotEmpty(message))
                message = message + lineSeparator + errorMsg;
            else
                message = errorMsg;
            
            if(StringUtils.isEmpty(message))
                message = "";
            
            Exception ee = new Exception(message);
            ee.setStackTrace(e.getStackTrace());
            
            Log.log.error(ee.getMessage(), ee);
            throw ee;
        } finally {
            try {
                if(input != null)
                    input.close();
                if(error != null)
                    error.close();
                if(bufferedReader != null)
                    bufferedReader.close();
                if(conn != null)
                    conn.disconnect();
            } catch(Exception ee) {}
        }
        
        return response;
    }
    
    /**
     * Call REST API using HTTP POST method.
     * If token value is not empty, token will be embedded as key of "token" for each REST call.
     * If basicAuth is true, Basic Authentication will be used for each REST call.
     * If the method call fails with token expired or unauthorized, caller needs to catch the exception and call login again to get a new token.
     * 
     * @param uri: REST interface/path defined by the server
     * @param params: the query key/value pairs to send to the server
     * @param body: data encoded in the format of the content type to send to the server
     * @param headers: any header properties caller needs to add
     * @param contentType: tell the server about the content type encoding for the HTTP request, such as text/xml, application/xml, application/json, etc.
     * @param accept: tell the server what content type can be accepted, such as text/xml, application/xml, application/json
     * @return: the response string
     * @throws Exception
     */
    
    @Deprecated
    public String callPost(String uri, Map<String, String> params, String body, Map<String, String>headers, String contentType, String accept) throws Exception {
        
        if(headers == null)
            headers = new HashMap<String, String>();
        
        if(StringUtils.isNotEmpty(contentType))
            headers.put("Content-Type", contentType);
        
        if(StringUtils.isNotEmpty(accept))
            headers.put("Accept", accept);
        
        return callPost(uri, params, body, headers);
    }
    
    public String callPost(String uri, Map<String, String> params, String body, Map<String, String>headers) throws Exception {
        HttpURLConnection conn = null;
        
        InputStream input = null;
        InputStream error = null;
        OutputStream output = null;
        
        BufferedReader bufferedReader = null;
        BufferedWriter bufferedWriter = null;
        
        StringBuilder path = new StringBuilder();
        StringBuilder response = new StringBuilder();
        
        try {
            path.append(uri);
            
            String url = "";

            if(params != null && params.size() != 0) {
                if(uri.indexOf("?") != -1)
                    path.append("&");
                else
                    path.append("?");
                
                for(Iterator<String> it=params.keySet().iterator(); it.hasNext();) {
                    String key = it.next();
                    String value = params.get(key);
                    path.append(key).append("=").append(URLEncoder.encode(value, StandardCharsets.UTF_8.name())).append("&");
                    url = path.toString();
                    url = url.substring(0, url.length()-1);
                }
            }
            
            else
                url = path.toString();

            conn = getConnection(url, headers);
            
            conn.setRequestMethod("POST");

            conn.setDoInput(true);
            conn.setDoOutput(true);
         
            if(basicAuth)
                doBasicAuthentication(conn);
    
            if(body != null) {
                conn.addRequestProperty("Content-Length", Integer.toString(body.length()));
                output = conn.getOutputStream();
                BufferedWriter writer = new BufferedWriter(new OutputStreamWriter(output, "UTF8"));
                writer.write(body.trim());
                writer.flush();
            }
            
//            conn.setInstanceFollowRedirects(false);
            
            // Get response code
            statusCode = conn.getResponseCode();
            
            // Get Response message
            reasonPhrase = conn.getResponseMessage();
            
            // Get response headers
            Map<String, List<String>> headerFields = conn.getHeaderFields();
            
            String domain = conn.getURL().getHost();
            
            for(Iterator<String> it=headerFields.keySet().iterator(); it.hasNext();) {
                String key = it.next();
                
                if(StringUtils.isBlank(key))
                    continue;
                
                if(!key.equalsIgnoreCase(SET_COOKIE)) {
                    List<String> values = headerFields.get(key);
                    StringBuilder sb = new StringBuilder();
                    
                    if(values != null && values.size() > 0) {                
                        int size = values.size();
                        
                        for(int i=0; i<size; i++) {

                            String value = values.get(i);
                            sb.append(value);
                            if(i != size-1)
                                sb.append(",");
                        }
                    }
                    
                    responseHeaders.put(key, sb.toString());
                }

                else {
                    List<String> headerValues = headerFields.get(key);
                    
                    StringTokenizer st = new StringTokenizer(headerValues.get(0), COOKIE_VALUE_DELIMITER);

                    HttpCookie cookie = null;
                    
                    if (st.hasMoreTokens()) {
                        String token  = st.nextToken();
                        String name = token.substring(0, token.indexOf(NAME_VALUE_SEPARATOR));
                        String value = token.substring(token.indexOf(NAME_VALUE_SEPARATOR) + 1, token.length());
                        cookie = new HttpCookie(name, value);
                
                        while (st.hasMoreTokens()) {
                            token  = st.nextToken();
                            if(StringUtils.isBlank(token))
                                continue;
    
                            int index = token.indexOf(NAME_VALUE_SEPARATOR);
                            name = token;
                            value = "";
                            
                            if(index > 0) {
                                name = token.substring(0, index).trim();
                                value = token.substring(index + 1, token.length());
    
                                if(name.equalsIgnoreCase(PATH))
                                    cookie.setPath(value);
                            }
                            
                            else
                                cookie.setComment(name);
                        }
    
                        cookie.setDomain(domain);
                        cookieStore.add(new URI(domain), cookie);
                    }
                }
            }
            
            input = conn.getInputStream();

            bufferedReader = new BufferedReader(new InputStreamReader(input));
            
            String line = null;
            while ((line = bufferedReader.readLine()) != null)
                response.append(line).append(lineSeparator);

//            conn.getContent();
        } catch(Exception e) {
//            e.printStackTrace();
            if(conn == null)
                throw e;
            
            statusCode = conn.getResponseCode();
            reasonPhrase = conn.getResponseMessage();
            Log.log.error("Response code is: " + statusCode);
            Log.log.error("Response message is: " + reasonPhrase);

            String errorMsg = "";
            
            try {
                error = conn.getErrorStream();

                if(bufferedReader != null)
                    bufferedReader.close();
                
                bufferedReader = new BufferedReader(new InputStreamReader(error));
                
                String line = null;
                StringBuilder sb = new StringBuilder();
                while ((line = bufferedReader.readLine()) != null)
                    sb.append(line);
                
                errorMsg = sb.toString();
            } catch(Exception eee) {
                Log.log.debug(eee.getMessage(), eee);
            }
            
            String message = e.getMessage();
            if(StringUtils.isNotEmpty(message))
                message = message + lineSeparator + errorMsg;
            else
                message = errorMsg;
            
            if(StringUtils.isEmpty(message))
                message = "";
            
            Exception ee = new Exception(message);
            ee.setStackTrace(e.getStackTrace());
            
            Log.log.error(ee.getMessage(), ee);
            throw ee;
        } finally {
            try {
                if(input != null)
                    input.close();
                if(error != null)
                    error.close();
                if(output != null)
                    output.close();
                if(bufferedReader != null)
                    bufferedReader.close();
                if(bufferedWriter != null)
                    bufferedWriter.close();
                if(conn != null)
                    conn.disconnect();
            } catch(Exception ee) {}
        }
        
        Log.log.debug("Response code is: " + statusCode);
        Log.log.debug("Response message is: " + response.toString());
        
        return response.toString();
    }
    
    private void doBasicAuthentication(URLConnection conn) throws Exception {
        
        if(StringUtils.isBlank(user))
            throw new Exception("User is missing for Basic Authentication.");
        
        try {
            String auth = user + ":" + pass;
            String authEncoded = new String(org.apache.commons.codec.binary.Base64.encodeBase64(auth.getBytes()));
            conn.setRequestProperty("Authorization", String.format("Basic %s", authEncoded));
        } catch(Exception e) {
            Log.log.error(e.getMessage(), e);
            throw new Exception("Failed to do Basic Authentication.");
        }
    }
    
    private HttpURLConnection getConnection(String uri, Map<String, String>headers) throws Exception {
        
        HttpURLConnection conn = null;
        
        URL url = new URL(baseUrl + uri);
        Log.log.debug("url = " + url.toString());

        String protocol = url.getProtocol();
        if (StringUtils.isNotBlank(protocol) && protocol.equalsIgnoreCase("https"))
        {
            if (selfSigned)
            {
                HttpsURLConnection.setDefaultHostnameVerifier(new javax.net.ssl.HostnameVerifier()
                {
                    public boolean verify(String hostname, javax.net.ssl.SSLSession sslSession)
                    {
                        return true;
                    }
                });
            }

            if (trustAll)
            {
                TrustManager[] trustAllCerts = trustAllCertificates();

                String tls = System.getProperty("https.protocols");

                if (StringUtils.isNotEmpty(tls))
                {
                    if (tls.contains(","))
                    {
                        String tlss[] = tls.split(",");
                        tls = tlss[0];
                    }
                }

                else
                    tls = TLS;
                Log.log.debug("TLS version is: " + tls);

                SSLContext sc = SSLContext.getInstance(tls);
                sc.init(null, trustAllCerts, new java.security.SecureRandom());
                HttpsURLConnection.setDefaultSSLSocketFactory(sc.getSocketFactory());

                HttpsURLConnection.setDefaultHostnameVerifier(new javax.net.ssl.HostnameVerifier()
                {
                    public boolean verify(String hostname, javax.net.ssl.SSLSession sslSession)
                    {
                        return true;
                    }
                });
            }

            conn = (HttpsURLConnection) url.openConnection();
        } // https
        
        else
            conn = (HttpURLConnection) url.openConnection();

        if(headers != null) {
            Set<String> keys = headers.keySet();
            for(Iterator it = keys.iterator(); it.hasNext();) {
                String key = (String)it.next();
                String value = headers.get(key);
                
                conn.addRequestProperty(key, value);
            }
        }
        
        if(cookieEnabled) {
            List<HttpCookie> cookies = cookieStore.getCookies();
            
            if (cookies != null && cookies.size() != 0) {
                StringBuilder sb = new StringBuilder();
                
                for (HttpCookie cookie : cookies) {
                    String cookieName = cookie.getName();
                    String cookieValue = cookie.getValue();
                    
                    if(url.getHost().equals(cookie.getDomain()))
                        sb.append(cookieName).append("=").append(cookieValue);
                }

//              Log.log.info("cookies = " + sb.toString());
                conn.addRequestProperty(COOKIE, sb.toString());
            }
        }
        
        conn.setConnectTimeout(30000);
        
        return conn;
    }
    
    private static TrustManager[] trustAllCertificates() {
        // Create a trust manager that does not validate certificate chains
        TrustManager[] trustAllCerts = new TrustManager[] { new X509TrustManager() {
            public X509Certificate[] getAcceptedIssuers() {
                return new X509Certificate[0];
            }

            public void checkClientTrusted(X509Certificate[] certs, String authType) {
            }

            public void checkServerTrusted(X509Certificate[] certs, String authType) {
            }
        } };
        
        return trustAllCerts;
    }
    
    public CookieStore getCookieStore()
    {
        return cookieStore;
    }

    public void setCookieStore(CookieStore cookieStore)
    {
        this.cookieStore = cookieStore;
    }
    
}
