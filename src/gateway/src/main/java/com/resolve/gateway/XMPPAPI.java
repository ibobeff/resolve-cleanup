/******************************************************************************
* (C) Copyright 2014
*
* Resolve Systems, LLC
*
* All rights reserved
* This software is distributed under license and may not be used, copied,
* modified, or distributed without the express written permission of
* Resolve Systems, LLC.
*
******************************************************************************/

package com.resolve.gateway;

import java.util.Map;

import com.resolve.rsbase.MainBase;
import com.resolve.gateway.tsrm.TSRMGateway;
import com.resolve.gateway.xmpp.XMPPGateway;
import com.resolve.util.StringUtils;


public class XMPPAPI
{
    private static XMPPGateway instance = XMPPGateway.getInstance();

    /**
     * Returns the {@link Object} value for the given key in the current
     * XMPPGateway instance's NameProperty map.
     * 
     * @param name
     *            the name of the property {@link Map} in the
     *            XMPPGateway instance's {@link NameProperties}
     *            object
     * @param key
     *            the key associated with the expected value in the property
     *            {@link Map} returned from the XMPPGateway's
     *            {@link NameProperties} object
     * @return {@link Object} value associated with the key in the property
     *         {@link Map} returned by the XMPPGateway instance's
     *         {@link NameProperties} object
     */
    public static Object getNameObject(String name, String key)
    {
        return instance.getNameProperties().getNameObject(name, key);
    }

    /**
     * Uses the given name parameter to return a property {@link Map} within the
     * XMPPGateway instance's {@link NameProperties} object. If
     * the returned {@link Map} isn't null, it adds the given {@link String} key
     * and {@link Object} value to it.
     * 
     * @param name
     *            the name of the {@link Map} in the XMPPGateway
     *            instance's {@link NameProperties} object. Cannot be null or
     *            empty.
     * @param key
     *            the key in the {@link Map} returned from the
     *            {@link NameProperties} object in the XMPPGateway
     *            . Cannot be null or empty.
     * @param value
     *            the value associated with the given key in the {@link Map}
     *            returned from the {@link NameProperties} object in the
     *            XMPPGateway
     * 
     * @throws Exception
     */
    public static void setNameObject(String name, String key, Object value) throws Exception
    {
        Map nameProperties = getNameProperties(name);
        if(nameProperties != null)
        {
            nameProperties.put(key, value);
            setNameProperties(name, nameProperties, true);
        }
    }
    
    /**
     * Uses the given {@link String} name as a key to return a {@link Map} of
     * properties from the XMPPGateway instance's
     * {@link NameProperties} object.
     *
     * @param name
     *            The name associated with the properties {@link Map} in the
     *            XMPPGateway's {@link NameProperties} object.
     *            Cannot be null or empty.
     * @return a Map of XMPPGateway name properties
     * @throws Exception
     */
    public static Map getNameProperties(String name) throws Exception
    {
        return instance.getNameProperties().getNameProperties(name);
    }

    /**
     * Associates the given name to the given properties {@link Map} in the
     * XMPPGateway instance's {@link NameProperties} object.
     *
     * @param name
     *            the key in the XMPPGateway's
     *            {@link NameProperties} object. Cannot be null or empty.
     * @param properties
     *            A Map of properties to associate with the given name parameter
     * @throws Exception
     */
    public static void setNameProperties(String name, Map properties) throws Exception
    {
        setNameProperties(name, properties, true);
    }

    /**
     * Associates the given name to the given properties Map in the
     * XMPPGateway instance's NameProperties object.
     *
     * @param name
     *            the key in the XMPPGateway's {@link NameProperties}
     *            object. Cannot be null or empty.
     * @param properties
     *            A {@link Map} of properties to associate with the given name
     *            parameter
     * @param doSave
     *            a boolean value that designates whether to send this change to
     *            RSControl for persistence
     * @throws Exception
     */
    public static void setNameProperties(String name, Map properties, boolean doSave) throws Exception
    {
        if (StringUtils.isBlank(name))
        {
            throw new Exception("'name' parameter must be passed");
        }
        else
        {
            instance.setNameProperties(name, properties, doSave);
        }
    }

    /**
     * Removes a {@link Map} of properties associated with the given name in the
     * XMPPGateway instance's {@link NameProperties} object.
     *
     * @param name
     *            The name associated with a property {@link Map} in the
     *            XMPPGateway instance's {@link NameProperties}
     *            object. Cannot be null or empty.
     * @throws Exception
     */
    public static void removeNameProperties(String name) throws Exception
    {
        if (StringUtils.isBlank(name))
        {
            throw new Exception("'name' parameter must be passed");
        }
        else
        {
            instance.removeNameProperties(name);
        }
    }

    /**
     * Saves the configurations in the RSRemote.
     *
     * @throws Exception
     */
    public static void saveConfig() throws Exception
    {
        MainBase.main.callSaveConfig();
    }

    /**
     * Sends a chat message to the chat user.
     *
     * <pre>
     * {@code
     * import com.resolve.gateway.XMPPAPI;
     *
     * try
     * {
     *      String to = "john2453";
     *      String message = "Hello from XMPP gateway";
     *      String username = "john@gmail.com";
     *      String password = "johnspwd";
     *      XMPPAPI.sendMessage(to, message, username, password);
     *  }
     *  catch (Exception ex)
     *  {
     *      //Do something good with this error.
     *      ex.printStackTrace();
     *  }
     *  }
     * </pre>
     *
     * @param to
     *            is the username of the chat user.
     * @param message
     *            to be sent.
     * @param username
     *            if not provided uses the gateway configuration.
     * @param password
     *            ties with the username..
     * @throws Exception
     */
    public static void sendMessage(String to, String message, String username, String password) throws Exception
    {
        instance.sendMessage(to, message, username, password);
    }
}
