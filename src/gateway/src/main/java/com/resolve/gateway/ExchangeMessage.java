/******************************************************************************
* (C) Copyright 2014
*
* Resolve Systems, LLC
*
* All rights reserved
* This software is distributed under license and may not be used, copied,
* modified, or distributed without the express written permission of
* Resolve Systems, LLC.
*
******************************************************************************/

package com.resolve.gateway;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

import com.resolve.util.Log;
import com.resolve.util.StringUtils;

public class ExchangeMessage
{
    private Map<String, Object> message = new ConcurrentHashMap();
    private List<Map<String, Object>> attFiles = new ArrayList<Map<String, Object>>();
    private List<Map<String, Object>> attContents = new ArrayList<Map<String, Object>>();
    private List<String> toList = new ArrayList();
    private List<String> ccList = new ArrayList();
    private List<String> bccList = new ArrayList();

    public void buildAttachement()
    {
        try
        {
            message.put("ATTACHMENT_FILE_COUNT", Integer.toString(attFiles.size()));
            int i = 1;

            for (Map<String, Object> attFile : attFiles)
            {
                message.put("ATTACHMENT_FILE_" + i, attFile.get("ATTACHMENT_FILE_NAME"));
                message.put("ATTACHMENT_TYPE_" + i, attFile.get("ATTACHMENT_TYPE"));
                i++;
            }

            message.put("ATTACHMENT_COUNT", Integer.toString(attContents.size()));
            i = 1;

            for (Map<String, Object> attContent : attContents)
            {
                message.put("ATTACHMENT_NAME_" + i, attContent.get("ATTACHMENT_NAME"));
                message.put("ATTACHMENT_CONTENT_" + i, attContent.get("ATTACHMENT_CONTENT"));
                message.put("ATTACHMENT_TYPE_" + i, attContent.get("ATTACHMENT_TYPE"));
                i++;
            }
        }
        catch (Throwable t)
        {
            Log.log.info("buildAttachement catched exception : " + t.getMessage());
        }
    }

    public void buildEmailList()
    {
        try
        {
            message.put("TOLIST", this.toList);
            message.put("CCLIST", this.ccList);
            message.put("BCCLIST", this.bccList);
        }
        catch (Throwable t)
        {
            Log.log.info("buildEmailList catched exception : " + t.getMessage());
        }
    }

    public void reset()
    {
        try
        {
            message = new ConcurrentHashMap();
            attFiles = new ArrayList<Map<String, Object>>();
            attContents = new ArrayList<Map<String, Object>>();
            this.toList = new ArrayList<String>();
            this.ccList = new ArrayList<String>();
            this.bccList = new ArrayList<String>();
        }
        catch (Throwable t)
        {
            Log.log.info("reset catched exception : " + t.getMessage());
        }
    }

    public void setToName(String toName)
    {
        if (!StringUtils.isEmpty(toName))
        {
            message.put("TONAME", toName);
        }
    }

    public String getToName()
    {
        return (String) message.get("TONAME");
    }

    public void setToAddress(String toAddress)
    {
        if (!StringUtils.isEmpty(toAddress))
        {
            message.put("TOADDRESS", toAddress);
        }
    }

    public String getToAddress()
    {
        return (String) message.get("TOADDRESS");
    }

    public void setTo(String to)
    {
        if (!StringUtils.isEmpty(to))
        {
            message.put("TO", to);
        }
    }

    public String getTo()
    {
        return (String) message.get("TO");
    }

    public void setCCName(String ccName)
    {
        if (!StringUtils.isEmpty(ccName))
        {
            message.put("CCNAME", ccName);
        }
    }

    public String getCCName()
    {
        return (String) message.get("CCNAME");
    }

    public void setCC(String cc)
    {
        if (!StringUtils.isEmpty(cc))
        {
            message.put("CC", cc);
        }
    }

    public String getCC()
    {
        return (String) message.get("CC");
    }

    public void setBCCName(String bccName)
    {
        if (!StringUtils.isEmpty(bccName))
        {
            message.put("BCCNAME", bccName);
        }
    }

    public String getBCCName()
    {
        return (String) message.get("BCCNAME");
    }

    public void setBCC(String bcc)
    {
        if (!StringUtils.isEmpty(bcc))
        {
            message.put("BCC", bcc);
        }
    }

    public String getBCC()
    {
        return (String) message.get("BCC");
    }

    public void setSubject(String subject)
    {
        if (!StringUtils.isEmpty(subject))
        {
            message.put("SUBJECT", subject);
        }
    }

    public String getSubject()
    {
        return (String) message.get("SUBJECT");
    }

    public void setContent(String content)
    {
        if (!StringUtils.isEmpty(content))
        {
            message.put("CONTENT", content);
        }
    }

    public String getContent()
    {
        return (String) message.get("CONTENT");
    }

    public void addAttachment(String filepathandname)
    {
        if (filepathandname != null && !filepathandname.equals(""))
        {
            Map<String, Object> attFile = new HashMap();
            attFile.put("ATTACHMENT_FILE_NAME", filepathandname);
            attFile.put("ATTACHMENT_TYPE", "BINARY");
            attFiles.add(attFile);
        }
    }

    public void addAttachment(String filename, byte[] filecontent)
    {
        if (filename != null && !filename.equals("") && filecontent != null && filecontent.length > 0)
        {
            Map<String, Object> attContent = new HashMap();
            attContent.put("ATTACHMENT_NAME", filename);
            attContent.put("ATTACHMENT_CONTENT", filecontent);
            attContent.put("ATTACHMENT_TYPE", "BINARY");
            attContents.add(attContent);
        }
    }

    // public void addEmbeddedAtatchements(String filepathandname)
    // {
    // if(filepathandname != null && !filepathandname.equals(""))
    // {
    // Map<String, Object> embeddedAttFile = new HashMap();
    // embeddedAttFile.put("ATTACHMENT_FILE_NAME", filepathandname);
    // embeddedAttFile.put("ATTACHMENT_TYPE", "EMBEDDED");
    // attFiles.add(embeddedAttFile);
    // }
    // }
    //
    // public void addEmbeddedAtatchements(String filename, byte[] filecontent)
    // {
    // if(filename != null && !filename.equals("") && filecontent != null &&
    // filecontent.length>0)
    // {
    // Map<String, Object> embeddedAttContent = new HashMap();
    // embeddedAttContent.put("ATTACHMENT_NAME", filename);
    // embeddedAttContent.put("ATTACHMENT_CONTENT", filecontent);
    // embeddedAttContent.put("ATTACHMENT_TYPE", "EMBEDDED");
    //
    // attContents.add(embeddedAttContent);
    // }
    // }

    public void addToEmail(String to)
    {
        this.toList.add(to);
    }

    public void addCCEmail(String cc)
    {
        this.ccList.add(cc);
    }

    public void addBCCEmail(String bcc)
    {
        this.bccList.add(bcc);
    }

    public Map getMessage()
    {
        return message;
    }

}
