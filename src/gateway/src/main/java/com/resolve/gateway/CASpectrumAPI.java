/******************************************************************************
* (C) Copyright 2014
*
* Resolve Systems, LLC
*
* All rights reserved
* This software is distributed under license and may not be used, copied,
* modified, or distributed without the express written permission of
* Resolve Systems, LLC.
*
******************************************************************************/

package com.resolve.gateway;

import java.util.List;
import java.util.Map;

import com.resolve.rsbase.MainBase;
import com.resolve.gateway.caspectrum.CASpectrumGateway;
import com.resolve.util.StringUtils;

public class CASpectrumAPI
{
    private static CASpectrumGateway instance = CASpectrumGateway.getInstance();

    /**
     * Returns the {@link Object} value for the given key in the current
     * CASpectrumGateway instance's NameProperty map.
     * 
     * @param name
     *            the name of the property {@link Map} in the
     *            CASpectrumGateway instance's {@link NameProperties}
     *            object
     * @param key
     *            the key associated with the expected value in the property
     *            {@link Map} returned from the CASpectrumGateway's
     *            {@link NameProperties} object
     * @return {@link Object} value associated with the key in the property
     *         {@link Map} returned by the CASpectrumGateway instance's
     *         {@link NameProperties} object
     */
    public static Object getNameObject(String name, String key)
    {
        return instance.getNameProperties().getNameObject(name, key);
    }
    
    /**
     * Sets the given name parameter to return a property {@link Map} within the
     * CASpectrumGateway} instance's {@link NameProperties} object. If
     * the returned {@link Map} isn't null, it adds the given {@link String} key
     * and {@link Object} value to it.
     * 
     * @param name
     *            the name of the {@link Map} in the CASpectrumGateway
     *            instance's {@link NameProperties} object. Cannot be null or
     *            empty.
     * @param key
     *            the key in the {@link Map} returned from the
     *            {@link NameProperties} object in the CASpectrumGateway
     *            . Cannot be null or empty.
     * @param value
     *            the value associated with the given key in the {@link Map}
     *            returned from the {@link NameProperties} object in the
     *            CASpectrumGateway
     * 
     * @throws Exception
     */

    @SuppressWarnings({ "rawtypes", "unchecked" })
    public static void setNameObject(String name, String key, Object value) throws Exception
    {
        Map nameProperties = getNameProperties(name);
        if(nameProperties != null)
        {
            nameProperties.put(key, value);
            setNameProperties(name, nameProperties, true);
        }
    }
        
    /**
     * Uses the given {@link String} name as a key to return a {@link Map} of
     * properties from the CASpectrumGateway instance's
     * {@link NameProperties} object.
     *
     * @param name
     *            The name associated with the properties {@link Map} in the
     *            CASpectrumGateway's {@link NameProperties} object.
     *            Cannot be null or empty.
     * @return a Map of CASpectrumGateway name properties
     * @throws Exception
     */

    @SuppressWarnings("rawtypes")
    public static Map getNameProperties(String name) throws Exception
    {
        return instance.getNameProperties().getNameProperties(name);
    }
    
    /**
     * Associates the given name to the given properties {@link Map} in the
     * CASpectrumGateway instance's {@link NameProperties} object.
     *
     * @param name
     *            the key in the CASpectrumGateway's
     *            {@link NameProperties} object. Cannot be null or empty.
     * @param properties
     *            A Map of properties to associate with the given name parameter
     * @throws Exception
     */

    @SuppressWarnings("rawtypes")
    public static void setNameProperties(String name, Map properties) throws Exception
    {
        setNameProperties(name, properties, true);
    }
    
    /**
     * Associates the given name to the given properties Map in the
     * CASpectrumGateway instance's NameProperties object.
     *
     * @param name
     *            the key in the CASpectrumGateway's {@link NameProperties}
     *            object. Cannot be null or empty.
     * @param properties
     *            A {@link Map} of properties to associate with the given name
     *            parameter
     * @param doSave
     *            a boolean value that designates whether to send this change to
     *            RSControl for persistence
     * @throws Exception
     */

    @SuppressWarnings("rawtypes")
    public static void setNameProperties(String name, Map properties, boolean doSave) throws Exception
    {
        if (StringUtils.isBlank(name))
        {
            throw new Exception("'name' parameter must be passed");
        }
        else
        {
            instance.setNameProperties(name, properties, doSave);
        }
    }
    
    /**
     * Removes a {@link Map} of properties associated with the given name in the
     * CASpectrumGateway instance's {@link NameProperties} object.
     *
     * @param name
     *            The name associated with a property {@link Map} in the
     *            CASpectrumGateway instance's {@link NameProperties}
     *            object. Cannot be null or empty.
     * @throws Exception
     */

    public static void removeNameProperties(String name) throws Exception
    {
        if (StringUtils.isBlank(name))
        {
            throw new Exception("'name' parameter must be passed");
        }
        else
        {
            instance.removeNameProperties(name);
        }
    }
    
    /**
     * Saves the configurations in the RSRemote.
     *
     * @throws Exception
     */

    public static void saveConfig() throws Exception
    {
        MainBase.main.callSaveConfig();
    }
    
    //CASpectrum Object CRUD Methods
    
    /**
     * Issues an action request specified by action code on SpectroSERVER.
     *
     * For details on parameters refer to "Web Services API Reference Guide (Release 9.4)
     * Chapter 4:RESTful Resources:action:GET action
     * 
     * @param actionCode
     * @param modelHandle 
     * @param attribValMap (optional) 
     *              to pass data to action
     * @param throttlesize
     *              between 1 to 1000 (both inclusive) any other value is capped to 1000 
     * @param username
     *              if not provided uses gateway configured username
     * @param password
     *              tied with username
     * @return a {@link Map} containing acion response attributes. 
     *                       Contents differ depending on reuested action.
     * @throws Exception
     */
    public static Map<String, String> issueAnAction(String actionCode, 
                                                    String modelHandle, 
                                                    Map<String, String> attribValMap, 
                                                    int throttlesize,
                                                    String username,
                                                    String password) throws Exception
    {
        return instance.issueAnAction(actionCode, modelHandle, attribValMap, throttlesize, username, password);
    }
    
    /**
     * Get alarms based on filter conditions in specified xml query string.
     * 
     * For details on parameters and returned alarm data refer to 
     * "Web Services API Reference Guide (Release 9.4)
     * Chapter 4:RESTful Resources:alarms:POST alarms (GET Tunneling)
     * 
     * @param xml
     * @param username
     *              if not provided uses gateway configured username
     * @param password
     *              tied with username
     * @return a {@link List} containing {@link Map} of alarm attributes.
     *                        
     * @throws Exception
     */
    public static List<Map<String, String>> getAlarms(String xml, 
                                                      String username,
                                                      String password) throws Exception
    {
        return instance.getAlarms(xml, username, password);
    }
    
    /**
     * Gets all alarms.
     * 
     * For details on parameters and returned alarm data refer to 
     * "Web Services API Reference Guide (Release 9.4)
     * Chapter 4:RESTful Resources:alarms:GET alarms
     * 
     * @param attributes (optional) List of alarm attributes to retrieve for each alarm
     *              Alarm id is returned by default
     * @param landscapeHandles (optional) List of landscape handles
     * @param throttlesize Sets # of alarms to retrieve in single request to server
     *              If  total # of alarms is more than throttle size then multiple reqeusts are made to
     *              server to retrieve all alarms.
     *              Between 1 to 1000 (both inclusive) any other value is capped to 1000
     * @param username
     *              if not provided uses gateway configured username
     * @param password
     *              tied with username
     * @return a {@link List} containing {@link Map} of alarm attributes.
     *                        
     * @throws Exception
     */
    public static List<Map<String, String>> getAllAlarms(List<String> attributes,
                                                         List<String> landscapeHandles,
                                                         int throttlesize,
                                                         String username,
                                                         String password) throws Exception
    {
        return instance.getAllAlarms(attributes, landscapeHandles, throttlesize, username, password);
    }
    
    /**
     * Update alarm.
     * 
     * For details on parameters and returned data refer to 
     * "Web Services API Reference Guide (Release 9.4)
     * Chapter 4:RESTful Resources:alarms:PUT alarms
     * 
     * @param alarmId Id of the alarm to update.
     * @param attribVal Map of attributes to update as key and values to modify as value
     * @param username
     *              if not provided, uses gateway configured username
     * @param password
     *              tied with username
     * @return a {@link Map} of alarm attribute update operation result.
     *                        
     * @throws Exception
     */
    public static Map<String, String> updateAlarm(String alarmId,
                                                  Map<String, String> attribVal,
                                                  String username,
                                                  String password) throws Exception
    {
        return instance.updateAlarm(alarmId, attribVal, username, password);
    }
    
    /**
     * Delete alarm.
     * 
     * For details on parameters and returned data refer to 
     * "Web Services API Reference Guide (Release 9.4)
     * Chapter 4:RESTful Resources:alarms:PUT alarms
     * 
     * @param alarmId Id of the alarm to delete.
     * @param username
     *              if not provided, uses gateway configured username
     * @param password
     *              tied with username
     * @return a {@link Map} of alarm delete operation result.
     *                        
     * @throws Exception
     */
    public static Map<String, String> deleteAlarm(String alarmId,
                                                  String username,
                                                  String password) throws Exception
    {
        return instance.deleteAlarm(alarmId, username, password);
    }
    
    /**
     * Create an association between two models.
     * 
     * For details on parameters and returned data refer to 
     * "Web Services API Reference Guide (Release 9.4)
     * Chapter 4:RESTful Resources:alarms:POST associations
     * 
     * @param relationHandle
     * @param leftModelHandle
     * @param rightModelHandle
     * @param username
     *              if not provided, uses gateway configured username
     * @param password
     *              tied with username
     * @return a {@link Map} of results of create association operation.
     */
    public static Map<String, String> createAssociation(String relationHandle,
                                                        String leftModelHandle,
                                                        String rightModelHandle,
                                                        String username,
                                                        String password) throws Exception
    {
        return instance.createAssociation(relationHandle, leftModelHandle, rightModelHandle, username, password);
    }
    
    /**
     * Get associations for a specific relation and model.
     * 
     * For details on parameters and returned data refer to 
     * "Web Services API Reference Guide (Release 9.4)
     * Chapter 4:RESTful Resources:alarms:GET associations
     * 
     * @param relationHandle
     * @param modelHandle
     * @param side (left/right)
     * @param username
     *              if not provided, uses gateway configured username
     * @param password
     *              tied with username
     * @return a {@link Map} of results of create association operation.
     */
    public static List<Map<String, String>> getAssociations(String relationHandle,
                                                            String modelHandle,
                                                            String side,
                                                            String username,
                                                            String password) throws Exception
    {
        return instance.getAssociations(relationHandle, modelHandle, side, username, password);
    }
    
    /**
     * Delete an association between two models.
     * 
     * For details on parameters and returned data refer to 
     * "Web Services API Reference Guide (Release 9.4)
     * Chapter 4:RESTful Resources:alarms:DELETE associations
     * 
     * @param relationHandle
     * @param leftModelHandle
     * @param rightModelHandle
     * @param username
     *              if not provided, uses gateway configured username
     * @param password
     *              tied with username
     * @return a {@link Map} of results of create association operation.
     */
    public static Map<String, String> deleteAssociation(String relationHandle,
                                                        String leftModelHandle,
                                                        String rightModelHandle,
                                                        String username,
                                                        String password) throws Exception
    {
        return instance.deleteAssociation(relationHandle, leftModelHandle, rightModelHandle, username, password);
    }
    
    /**
     * Get attribute enumeration for enumerated attribute.
     * 
     * For details on parameters and returned data refer to 
     * "Web Services API Reference Guide (Release 9.4)
     * Chapter 4:RESTful Resources:alarms:GET attribute
     * 
     * @param attrId Enumerated attribute Id
     * @param username
     *              if not provided, uses gateway configured username
     * @param password
     *              tied with username
     * @return a {@link Map} of attribute strings keyed by index.
     */
    public static Map<String, String> getAttributeEnum(String attrId, 
                                                       String username, 
                                                       String password) throws Exception
    {
        return instance.getAttributeEnum(attrId, username, password);
    }
    
    /**
     * Get connectivity information for specified device models IP address.
     * 
     * For details on parameters and returned data refer to 
     * "Web Services API Reference Guide (Release 9.4)
     * Chapter 4:RESTful Resources:alarms:GET connectivity
     * 
     * @param ipAddress
     * @param username
     *              if not provided, uses gateway configured username
     * @param password
     *              tied with username
     * @return a {@link Map} of results of create association operation.
     */
    public static Map<String, Map<String, List<Map<String, String>>>> getConnectivity(String ipAddress,
                                                                                      String username,
                                                                                      String password) throws Exception
    {
        return instance.getConnectivity(ipAddress, username, password);
    }
    
    /**
     * Gets all devices.
     * 
     * For details on parameters and returned device data refer to 
     * "Web Services API Reference Guide (Release 9.4)
     * Chapter 4:RESTful Resources:devices:GET devices
     * 
     * @param attributes (optional) List of device attributes to retrieve for each device
     *              Model handle is returned by default
     * @param landscapeHandles (optional) List of landscape handles
     * @param throttlesize Sets # of devices to retrieve in single request to server
     *              If  total # of devices is more than throttle size then multiple reqeusts are made to
     *              server to retrieve all devices.
     *              Between 1 to 1000 (both inclusive) any other value is capped to 1000
     * @param username
     *              if not provided uses gateway configured username
     * @param password
     *              tied with username
     * @return a {@link List} containing {@link Map} of device attributes.
     *                        
     * @throws Exception
     */
    public static List<Map<String, String>> getAllDevices(List<String> attributes,
                                                          List<String> landscapeHandles,
                                                          int throttlesize,
                                                          String username,
                                                          String password) throws Exception
    {
        return instance.getAllDevices(attributes, landscapeHandles, throttlesize, username, password);
    }
    
    /**
     * Gets landscapes return information about all landscapes.
     * 
     * For details on parameters and returned device data refer to 
     * "Web Services API Reference Guide (Release 9.4)
     * Chapter 4:RESTful Resources:landscapes:GET landscapes
     * 
     * @param username
     *              if not provided uses gateway configured username
     * @param password
     *              tied with username
     * @return a {@link List} containing {@link Map} of landscape information.
     *                        
     * @throws Exception
     */
    public static List<Map<String, String>> getLandscapes(String username,
                                                          String password) throws Exception
    {
        return instance.getLandscapes(username, password);
    }
    
    /**
     * Create a new model.
     * 
     * For details on parameters and returned data refer to 
     * "Web Services API Reference Guide (Release 9.4)
     * Chapter 4:RESTful Resources:mode:POST model
     * 
     * @param landscapeHandle
     * @param modelTypeHandle
     * @param snmpAgentPort
     * @param communityString
     * @param retryCount
     * @param timeout
     * @param ipAddress
     * @param parentModelHandle
     * @param parentToModelRelationHandle
     * @param attribValMap Model attribute value map
     * @param username
     *              if not provided, uses gateway configured username
     * @param password
     *              tied with username
     * @return a {@link Map} of results of create association operation.
     */
    public static Map<String, String> createModel(String landscapeHandle,
                                                  String modelTypeHandle,
                                                  int snmpAgentPort,
                                                  String communityString,
                                                  int retryCount,
                                                  int timeout,
                                                  String ipAddress,
                                                  String parentModelHandle,                                                
                                                  String parentToModelRelationHandle,
                                                  Map<String, String> attribValMap,
                                                  String username,
                                                  String password) throws Exception
    {
        return instance.createModel(landscapeHandle, modelTypeHandle, snmpAgentPort, 
                                    communityString, retryCount, timeout, ipAddress,
                                    parentModelHandle, parentToModelRelationHandle,
                                    attribValMap, username, password);
    }
    
    /**
     * Get specific model and its requested attributes.
     * 
     * For details on parameters and returned model data refer to 
     * "Web Services API Reference Guide (Release 9.4)
     * Chapter 4:RESTful Resources:model:GET model
     *
     * @param modelHandle
     * @param attributes (optional) List of model attributes to retrieve for specified model
     *              Model handle is returned by default
     * @param username
     *              if not provided uses gateway configured username
     * @param password
     *              tied with username
     * @return a {@link Map} of model attributes.
     *                        
     * @throws Exception
     */
    public static Map<String, String> getModel(String modelHandle,
                                               List<String> attributes,
                                               String username,
                                               String password) throws Exception
    {
        return instance.getModel(modelHandle, attributes, username, password);
    }
    
    /**
     * Update model.
     * 
     * For details on parameters and returned data refer to 
     * "Web Services API Reference Guide (Release 9.4)
     * Chapter 4:RESTful Resources:model:PUT model
     * 
     * @param modelHandle Id of the model to update.
     * @param attribVal Map of attributes to update as key and values to modify as value
     * @param username
     *              if not provided, uses gateway configured username
     * @param password
     *              tied with username
     * @return a {@link Map} of model attribute update operation result.
     *                        
     * @throws Exception
     */
    public static Map<String, String> updateModel(String modelHandle,
                                                  Map<String, String> attribVal,
                                                  String username,
                                                  String password) throws Exception
    {
        return instance.updateModel(modelHandle, attribVal, username, password);
    }
    
    /**
     * Delete model.
     * 
     * For details on parameters and returned data refer to 
     * "Web Services API Reference Guide (Release 9.4)
     * Chapter 4:RESTful Resources:model:DELETE model
     * 
     * @param modelHandle Id of the model to delete.
     * @param username
     *              if not provided, uses gateway configured username
     * @param password
     *              tied with username
     * @return a {@link Map} of model delete operation result.
     *                        
     * @throws Exception
     */
    public static Map<String, String> deleteModel(String modelHandle,
                                                  String username,
                                                  String password) throws Exception
    {
        return instance.deleteModel(modelHandle, username, password);
    }
    
    /**
     * Get models based on filter conditions specified in xml query string.
     * 
     * For details on parameters and returned model data refer to 
     * "Web Services API Reference Guide (Release 9.4)
     * Chapter 4:RESTful Resources:models:POST models (GET Tunneling)
     * 
     * @param xml
     * @param username
     *              if not provided uses gateway configured username
     * @param password
     *              tied with username
     * @return a {@link List} containing {@link Map} of model attributes.
     *                        
     * @throws Exception
     */
    public static List<Map<String, String>> getModels(String xml, 
                                                      String username,
                                                      String password) throws Exception
    {
        return instance.getModels(xml, username, password);
    }
    
    /**
     * Update models based on filter conditions and attributes specified in xml query string.
     * 
     * For details on parameters and returned model data refer to 
     * "Web Services API Reference Guide (Release 9.4)
     * Chapter 4:RESTful Resources:models:PUT models
     * 
     * @param xml
     * @param username
     *              if not provided uses gateway configured username
     * @param password
     *              tied with username
     * @return a {@link List} containing {@link Map} of model attribute update results.
     *                        
     * @throws Exception
     */
    public static List<Map<String, String>> updateModels(String xml, 
                                                         String username,
                                                         String password) throws Exception
    {
        return instance.updateModels(xml, username, password);
    }
    
    /**
     * Create an event.
     * 
     * For details on parameters and returned data refer to 
     * "Web Services API Reference Guide (Release 9.4)
     * Chapter 4:RESTful Resources:events:POST event
     * 
     * @param eventType
     * @param modelHandle
     * @param varBindIdValue
     *              Map of event bind variable id and corresponding value 
     * @param username
     *              if not provided, uses gateway configured username
     * @param password
     *              tied with username
     * @return a {@link Map} of results of create event operation.
     */
    public static Map<String, String> createEvent(String eventType,
                                                  String modelHandle,
                                                  Map<String, String> varBindIdValue,
                                                  String username,
                                                  String password) throws Exception
    {
        return instance.createEvent(eventType, modelHandle, varBindIdValue, username, password);
    }
    
    /**
     * Create events on model(s).
     * 
     * For details on parameters and returned data refer to 
     * "Web Services API Reference Guide (Release 9.4)
     * Chapter 4:RESTful Resources:events:POST events
     * 
     * @param createEventsRequestXml
     *              XML document specifying event request to create events for multiple models.
     * @param username
     *              if not provided, uses gateway configured username
     * @param password
     *              tied with username
     * @return a {@link Map} of results of create event operation.
     */
    public static List<Map<String, String>> createEvents(String createEventsRequestXml,
                                                         String username,
                                                         String password) throws Exception
    {
        return instance.createEvents(createEventsRequestXml, username, password);
    }
    
    /**
     * Create a (pull) subscription.
     * 
     * For details on parameters and returned data refer to 
     * "Web Services API Reference Guide (Release 9.4)
     * Chapter 4:RESTful Resources:subscription:POST subscription
     * 
     * @param pullSubscriptionRequestXml
     *              XML document specifying subscription (pull type) for notifications 
     * @param username
     *              if not provided, uses gateway configured username
     * @param password
     *              tied with username
     * @return a Subscription id to use to pull notifications or delete the sunbscription.
     */
    public static String createSubscription(String pullSubscriptionRequestXml,
                                            String username,
                                            String password) throws Exception
    {
        return instance.createSubscription(pullSubscriptionRequestXml, username, password);
    }
    
    /**
     * Get subscription i.e. pull notifications from subscription.
     * 
     * For details on parameters and returned data refer to 
     * "Web Services API Reference Guide (Release 9.4)
     * Chapter 4:RESTful Resources:subscription:GET subscription
     * 
     * @param subscriptionId
     *              Subscriuption id of pull subscription created earlier 
     * @param username
     *              if not provided, uses gateway configured username
     * @param password
     *              tied with username
     * @return a {@link List} containing {@link Map} of notifications.
     */
    public static List<Map<String, String>> getSubscription(String subscriptionId,
                                                            String username,
                                                            String password) throws Exception
    {
        return instance.getSubscription(subscriptionId, username, password);
    }
    
    /**
     * Delete subscription.
     * 
     * For details on parameters and returned data refer to 
     * "Web Services API Reference Guide (Release 9.4)
     * Chapter 4:RESTful Resources:subscripton:DELETE subscription
     * 
     * @param subscriptionId Id of the subscription to delete.
     * @param username
     *              if not provided, uses gateway configured username
     * @param password
     *              tied with username
     * @return a {@link Map} of subscription delete operation result.
     *                        
     * @throws Exception
     */
    public static Map<String, String> deleteSubscription(String subscriptionId,
                                                         String username,
                                                         String password) throws Exception
    {
        return instance.deleteSubscription(subscriptionId, username, password);
    }
}
