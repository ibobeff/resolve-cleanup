/******************************************************************************
* (C) Copyright 2014
*
* Resolve Systems, LLC
*
* All rights reserved
* This software is distributed under license and may not be used, copied,
* modified, or distributed without the express written permission of
* Resolve Systems, LLC.
*
******************************************************************************/

package com.resolve.gateway;

import java.util.List;
import java.util.Map;

import com.resolve.rsbase.MainBase;
import com.resolve.gateway.servicenow.ServiceNowGateway;
import com.resolve.gateway.snmp.SNMPGateway;
import com.resolve.util.StringUtils;

public class SNMPAPI
{
    public static final int SNMP_VERSION_1 = 1;
    public static final int SNMP_VERSION_2C = 2;
    public static final int SNMP_VERSION_3 = 3;

    private static SNMPGateway instance = SNMPGateway.getInstance();

    /**
     * Returns the {@link Object} value for the given key in the current
     * SNMPGateway instance's NameProperty map.
     * 
     * @param name
     *            the name of the property {@link Map} in the
     *            SNMPGateway instance's {@link NameProperties}
     *            object
     * @param key
     *            the key associated with the expected value in the property
     *            {@link Map} returned from the SNMPGateway's
     *            {@link NameProperties} object
     * @return {@link Object} value associated with the key in the property
     *         {@link Map} returned by the SNMPGateway instance's
     *         {@link NameProperties} object
     */
    public static Object getNameObject(String name, String key)
    {
        return instance.getNameProperties().getNameObject(name, key);
    }

    /**
     * Uses the given name parameter to return a property {@link Map} within the
     * SNMPGateway instance's {@link NameProperties} object. If
     * the returned {@link Map} isn't null, it adds the given {@link String} key
     * and {@link Object} value to it.
     * 
     * @param name
     *            the name of the {@link Map} in the SNMPGateway
     *            instance's {@link NameProperties} object. Cannot be null or
     *            empty.
     * @param key
     *            the key in the {@link Map} returned from the
     *            {@link NameProperties} object in the SNMPGateway
     *            . Cannot be null or empty.
     * @param value
     *            the value associated with the given key in the {@link Map}
     *            returned from the {@link NameProperties} object in the
     *            SNMPGateway
     * 
     * @throws Exception
     */
    public static void setNameObject(String name, String key, Object value) throws Exception
    {
        Map nameProperties = getNameProperties(name);
        if(nameProperties != null)
        {
            nameProperties.put(key, value);
            setNameProperties(name, nameProperties, true);
        }
    }
    
    /**
     * Uses the given {@link String} name as a key to return a {@link Map} of
     * properties from the SNMPGateway instance's
     * {@link NameProperties} object.
     *
     * @param name
     *            The name associated with the properties {@link Map} in the
     *            SNMPGateway's {@link NameProperties} object.
     *            Cannot be null or empty.
     * @return a Map of SNMPGateway name properties
     * @throws Exception
     */
    public static Map getNameProperties(String name) throws Exception
    {
        return instance.getNameProperties().getNameProperties(name);
    }

    /**
     * Associates the given name to the given properties {@link Map} in the
     * SNMPGateway instance's {@link NameProperties} object.
     *
     * @param name
     *            the key in the SNMPGateway's
     *            {@link NameProperties} object. Cannot be null or empty.
     * @param properties
     *            A Map of properties to associate with the given name parameter
     * @throws Exception
     */
    public static void setNameProperties(String name, Map properties) throws Exception
    {
        setNameProperties(name, properties, true);
    }

    /**
     * Associates the given name to the given properties Map in the
     * SNMPGateway instance's NameProperties object.
     *
     * @param name
     *            the key in the SNMPGateway's {@link NameProperties}
     *            object. Cannot be null or empty.
     * @param properties
     *            A {@link Map} of properties to associate with the given name
     *            parameter
     * @param doSave
     *            a boolean value that designates whether to send this change to
     *            RSControl for persistence
     * @throws Exception
     */
    public static void setNameProperties(String name, Map properties, boolean doSave) throws Exception
    {
        if (StringUtils.isBlank(name))
        {
            throw new Exception("'name' parameter must be passed");
        }
        else
        {
            instance.setNameProperties(name, properties, doSave);
        }
    }

    /**
     * Removes a {@link Map} of properties associated with the given name in the
     * SNMPGateway instance's {@link NameProperties} object.
     *
     * @param name
     *            The name associated with a property {@link Map} in the
     *            SNMPGateway instance's {@link NameProperties}
     *            object. Cannot be null or empty.
     * @throws Exception
     */
    public static void removeNameProperties(String name) throws Exception
    {
        if (StringUtils.isBlank(name))
        {
            throw new Exception("'name' parameter must be passed");
        }
        else
        {
            instance.removeNameProperties(name);
        }
    }

    /**
     * Saves the configurations in the RSRemote.
     *
     * @throws Exception
     */
    public static void saveConfig() throws Exception
    {
        MainBase.main.callSaveConfig();
    }

    /**
     * Sends Snmp traps to a device or a system.
     *
     * <pre>
     * {@code
     * import com.resolve.gateway.SNMPAPI;
     * import com.resolve.gateway.SNMPData;
     * try
     * {
     *      java.util.List<SNMPData> myData = new java.util.ArrayList<SNMPData>();
     *      myData.add(new SNMPData("1.3.2.4.5.6.2.1", "23", SNMPData.Type.Integer32));
     *      myData.add(new SNMPData("1.3.2.4.5.6.2.2", "Hello Device", SNMPData.Type.OctetString));
     *
     *      //Send a ColdStart trap with myData to the device/system with IP 10.20.10.100 on port 162.
     *      //String snmpTrapOID = "1.2.3.4.5.6.7.8"; //example of an arbitrary enterprise specific OID.
     *      String snmpTrapOID = SNMPData.SNMPTrapOID.ColdStart; // gets translated to 1.3.6.1.6.3.1.1.5.1
     *      //convenient constants SNMPAPI.SNMP_VERSION_1, SNMPAPI.SNMP_VERSION_2C, SNMPAPI.SNMP_VERSION_3 
     *      SNMPAPI.sendTrap(SNMPAPI.SNMP_VERSION_2C, "10.20.10.100", 162, 2, 3000, "public", snmpTrapOID, myData);
     * }
     * catch (Exception e)
     * {
     *      //do something with this exception.
     * }
     * }
     * </pre>
     *
     * @param version
     *            - SNMP version, could be 1, 2, or 3 based on the target device.
     * @param targetIp
     *            - IP address of the trap receiver device/system.
     * @param port
     *            - trap receiver port of the target device/system.
     * @param retries
     *            - number of time to retry before giving up, if @lt;= 0, gateway
     *            configuration is used.
     * @param timeout
     *            - time out in seconds, if @lt;= 0, gateway configuration is used.
     * @param writeCommunity
     *            - if not provided gateway configuration is used.
     * @param snmpTrapOid
     *            - Oid such as 1.3.6.1.6.3.1.1.5.1 (ColdStart),
     *            1.3.6.1.6.3.1.1.5.3 (LinkDown) or other enterprise specific
     *            Snmp trap OID. For some commonly used Snmp Trap OID check
     *            {@link SNMPData.SNMPTrapOID}.
     * @param traps
     *            is a {@link List} of {@link SNMPData}, check the
     *            {@link SNMPData.Type} for all the supported data types.
     * @throws Exception
     */
    public static void sendTrap(int version, String targetIp, int port, int retries, long timeout, String writeCommunity, String snmpTrapOid, List<SNMPData> traps) throws Exception
    {
        instance.sendTrap(version, targetIp, port, retries, timeout, writeCommunity, snmpTrapOid, traps);
    }

    /**
     * Get the available data for the oid.
     *
     * <pre>
     * {@code
     * import com.resolve.gateway.SNMPAPI;
     * import com.resolve.gateway.SNMPResponse;
     *
     *         try
     *         {
     *             String deviceIP = "192.168.56.100";
     *             int port = 161;
     *             String readCommunity = "public";
     *             int retries = 2;
     *             int timeout = 10; //seconds
     *             String oid = "1.3.6.1.2.1.1.1.0";
     *
     *             //convenient constants SNMPAPI.SNMP_VERSION_1, SNMPAPI.SNMP_VERSION_2C, SNMPAPI.SNMP_VERSION_3 
     *             SNMPResponse result = SNMPAPI.get(SNMPAPI.SNMP_VERSION_2C, deviceIP, port, readCommunity, retries, timeout, oid);
     *             System.out.println(result);
     *         }
     *         catch (Exception e)
     *         {
     *             // do something with this exception.
     *         }
     * }
     * </pre>
     *
     * @param version
     *            - SNMP version, could be 1, 2, or 3 based on the target device.
     * @param targetIp
     * @param port
     * @param readCommunity
     * @param retries
     * @param timeout
     * @param oidValue
     * @return a {@link SNMPResponse} object
     * @throws Exception
     */
    public static SNMPResponse get(int version, String targetIp, int port, String readCommunity, int retries, long timeout, String oidValue) throws Exception
    {
        return instance.get(version, targetIp, port, readCommunity, retries, timeout, oidValue);
    }

    /**
     * Get the next data after the oid.
     *
     * <pre>
     * {@code
     * import com.resolve.gateway.SNMPAPI;
     * import com.resolve.gateway.SNMPResponse;
     *
     *         try
     *         {
     *             String deviceIP = "10.30.10.12";
     *             int port = 161;
     *             String readCommunity = "public";
     *             int retries = 2;
     *             int timeout = 10; //seconds
     *             String oid = "1.3.6.1.2.1.1.1.0";
     *             //convenient constants SNMPAPI.SNMP_VERSION_1, SNMPAPI.SNMP_VERSION_2C, SNMPAPI.SNMP_VERSION_3 
     *             SNMPResponse result = SNMPAPI.getNext(SNMPAPI.SNMP_VERSION_2C, deviceIP, port, readCommunity, retries, timeout, oid);
     *             //if there is a next oid then it will return the response otherwise null.
     *             if(result != null)
     *             {
     *                 System.out.println(result.toString());
     *             }
     *         }
     *         catch (Exception e)
     *         {
     *             // do something with this exception.
     *         }
     * }
     * </pre>
     *
     * @param version
     *            - SNMP version, could be 1, 2, or 3 based on the target device.
     * @param targetIp
     * @param port
     * @param readCommunity
     * @param retries
     * @param timeout
     * @param oid
     * @return a {@link SNMPResponse} object
     * @throws Exception
     */
    public static SNMPResponse getNext(int version, String targetIp, int port, String readCommunity, int retries, long timeout, String oid) throws Exception
    {
        return instance.getNext(version, targetIp, port, readCommunity, retries, timeout, oid);
    }

    /**
     * Sets a Counter32 snmp value to the oid.
     *
     * <pre>
     * {@code
     * import com.resolve.gateway.SNMPAPI;
     *
     *         try
     *         {
     *             String deviceIP = "10.30.10.12";
     *             int port = 161;
     *             String writeCommunity = "private";
     *             int retries = 2;
     *             int timeout = 10; //seconds
     *             String oid = "1.3.6.1.2.1.1.1.0"; //make sure it accepts the value type provided.
     *             long oidValue = 1L;
     *
     *             //convenient constants SNMPAPI.SNMP_VERSION_1, SNMPAPI.SNMP_VERSION_2C, SNMPAPI.SNMP_VERSION_3 
     *             boolean result = SNMPAPI.setCounter32(SNMPAPI.SNMP_VERSION_2C, deviceIP, port, writeCommunity, retries, timeout, oid, oidValue);
     *             if(result)
     *             {
     *                 System.out.println("Sucessfully set the value.");
     *             }
     *         }
     *         catch (Exception e)
     *         {
     *             // do something with this exception.
     *         }
     * }
     * </pre>
     *
     * @param version
     *            - SNMP version, could be 1, 2, or 3 based on the target device.
     * @param targetIp
     * @param port
     * @param writeCommunity
     * @param retries
     * @param timeout
     * @param oid
     * @param oidValue
     * @return true if the operation is successful.
     * @throws Exception
     */
    public static boolean setCounter32(int version, String targetIp, int port, String writeCommunity, int retries, long timeout, String oid, long oidValue) throws Exception
    {
        return instance.set(version, targetIp, port, writeCommunity, retries, timeout, oid, Long.toString(oidValue), SNMPData.Type.Counter32);
    }

    /**
     * Sets a Counter64 snmp value to the oid.
     *
     * <pre>
     * {@code
     * import com.resolve.gateway.SNMPAPI;
     *
     *         try
     *         {
     *             String deviceIP = "10.30.10.12";
     *             int port = 161;
     *             String writeCommunity = "private";
     *             int retries = 2;
     *             int timeout = 10; //seconds
     *             String oid = "1.3.6.1.2.1.1.1.0"; //make sure it accepts the value type provided.
     *             long oidValue = 1L;
     *
     *             //convenient constants SNMPAPI.SNMP_VERSION_1, SNMPAPI.SNMP_VERSION_2C, SNMPAPI.SNMP_VERSION_3 
     *             boolean result = SNMPAPI.setCounter64(SNMPAPI.SNMP_VERSION_2C, deviceIP, port, writeCommunity, retries, timeout, oid, oidValue);
     *             if(result)
     *             {
     *                 System.out.println("Sucessfully set the value.");
     *             }
     *         }
     *         catch (Exception e)
     *         {
     *             // do something with this exception.
     *         }
     * }
     * </pre>
     *
     * @param version
     *            - SNMP version, could be 1, 2, or 3 based on the target device.
     * @param targetIp
     * @param port
     * @param writeCommunity
     * @param retries
     * @param timeout
     * @param oid
     * @param oidValue
     * @return true if the operation is successful.
     * @throws Exception
     */
    public static boolean setCounter64(int version, String targetIp, int port, String writeCommunity, int retries, long timeout, String oid, long oidValue) throws Exception
    {
        return instance.set(version, targetIp, port, writeCommunity, retries, timeout, oid, Long.toString(oidValue), SNMPData.Type.Counter64);
    }

    /**
     * Sets a Gauge32 snmp value to the oid.
     *
     * <pre>
     * {@code
     * import com.resolve.gateway.SNMPAPI;
     *
     *         try
     *         {
     *             String deviceIP = "10.30.10.12";
     *             int port = 161;
     *             String writeCommunity = "private";
     *             int retries = 2;
     *             int timeout = 10; //seconds
     *             String oid = "1.3.6.1.2.1.1.1.0"; //make sure it accepts the value type provided.
     *             long oidValue = 1L;
     *
     *             //convenient constants SNMPAPI.SNMP_VERSION_1, SNMPAPI.SNMP_VERSION_2C, SNMPAPI.SNMP_VERSION_3 
     *             boolean result = SNMPAPI.setGauge32(SNMPAPI.SNMP_VERSION_2C, deviceIP, port, writeCommunity, retries, timeout, oid, oidValue);
     *             if(result)
     *             {
     *                 System.out.println("Sucessfully set the value.");
     *             }
     *         }
     *         catch (Exception e)
     *         {
     *             // do something with this exception.
     *         }
     * }
     * </pre>
     *
     * @param version
     *            - SNMP version, could be 1, 2, or 3 based on the target device.
     * @param targetIp
     * @param port
     * @param writeCommunity
     * @param retries
     * @param timeout
     * @param oid
     * @param oidValue
     * @return true if the operation is successful.
     * @throws Exception
     */
    public static boolean setGauge32(int version, String targetIp, int port, String writeCommunity, int retries, long timeout, String oid, long oidValue) throws Exception
    {
        return instance.set(version, targetIp, port, writeCommunity, retries, timeout, oid, Long.toString(oidValue), SNMPData.Type.Gauge32);
    }

    /**
     * Sets a Integer32 snmp value to the oid.
     *
     * <pre>
     * {@code
     * import com.resolve.gateway.SNMPAPI;
     *
     *         try
     *         {
     *             String deviceIP = "10.30.10.12";
     *             int port = 161;
     *             String writeCommunity = "private";
     *             int retries = 2;
     *             int timeout = 10; //seconds
     *             String oid = "1.3.6.1.2.1.1.1.0"; //make sure it accepts the value type provided.
     *             int oidValue = 100;
     *
     *             //convenient constants SNMPAPI.SNMP_VERSION_1, SNMPAPI.SNMP_VERSION_2C, SNMPAPI.SNMP_VERSION_3 
     *             boolean result = SNMPAPI.setInteger32(SNMPAPI.SNMP_VERSION_2C, deviceIP, port, writeCommunity, retries, timeout, oid, oidValue);
     *             if(result)
     *             {
     *                 System.out.println("Sucessfully set the value.");
     *             }
     *         }
     *         catch (Exception e)
     *         {
     *             // do something with this exception.
     *         }
     * }
     * </pre>
     *
     * @param version
     *            - SNMP version, could be 1, 2, or 3 based on the target device.
     * @param targetIp
     * @param port
     * @param writeCommunity
     * @param retries
     * @param timeout
     * @param oid
     * @param oidValue
     * @return true if the operation is successful.
     * @throws Exception
     */
    public static boolean setInteger32(int version, String targetIp, int port, String writeCommunity, int retries, long timeout, String oid, int oidValue) throws Exception
    {
        return instance.set(version, targetIp, port, writeCommunity, retries, timeout, oid, Integer.toString(oidValue), SNMPData.Type.Integer32);
    }

    /**
     * Sets a IpAddress snmp value to the oid.
     *
     * <pre>
     * {@code
     * import com.resolve.gateway.SNMPAPI;
     *
     *         try
     *         {
     *             String deviceIP = "10.30.10.12";
     *             int port = 161;
     *             String writeCommunity = "private";
     *             int retries = 2;
     *             int timeout = 10; //seconds
     *             String oid = "1.3.6.1.2.1.1.1.0"; //make sure it accepts the value type provided.
     *             String oidValue = "10.10.10.120";
     *
     *             //convenient constants SNMPAPI.SNMP_VERSION_1, SNMPAPI.SNMP_VERSION_2C, SNMPAPI.SNMP_VERSION_3 
     *             boolean result = SNMPAPI.setIpAddress(SNMPAPI.SNMP_VERSION_2C, deviceIP, port, writeCommunity, retries, timeout, oid, oidValue);
     *             if(result)
     *             {
     *                 System.out.println("Sucessfully set the value.");
     *             }
     *         }
     *         catch (Exception e)
     *         {
     *             // do something with this exception.
     *         }
     * }
     * </pre>
     *
     * @param version
     *            - SNMP version, could be 1, 2, or 3 based on the target device.
     * @param targetIp
     * @param port
     * @param writeCommunity
     * @param retries
     * @param timeout
     * @param oid
     * @param oidValue
     * @return true if the operation is successful.
     * @throws Exception
     */
    public static boolean setIpAddress(int version, String targetIp, int port, String writeCommunity, int retries, long timeout, String oid, String oidValue) throws Exception
    {
        return instance.set(version, targetIp, port, writeCommunity, retries, timeout, oid, oidValue, SNMPData.Type.IpAddress);
    }

    /**
     * Sets a Null snmp value to the oid, notice that no need to send any value.
     *
     * <pre>
     * {@code
     * import com.resolve.gateway.SNMPAPI;
     *
     *         try
     *         {
     *             String deviceIP = "10.30.10.12";
     *             int port = 161;
     *             String writeCommunity = "private";
     *             int retries = 2;
     *             int timeout = 10; //seconds
     *             String oid = "1.3.6.1.2.1.1.1.0"; //make sure it accepts the value type provided.
     *
     *             //convenient constants SNMPAPI.SNMP_VERSION_1, SNMPAPI.SNMP_VERSION_2C, SNMPAPI.SNMP_VERSION_3 
     *             boolean result = SNMPAPI.setNull(SNMPAPI.SNMP_VERSION_2C, deviceIP, port, writeCommunity, retries, timeout, oid);
     *             if(result)
     *             {
     *                 System.out.println("Sucessfully set the value.");
     *             }
     *         }
     *         catch (Exception e)
     *         {
     *             // do something with this exception.
     *         }
     * }
     * </pre>
     *
     * @param version
     *            - SNMP version, could be 1, 2, or 3 based on the target device.
     * @param targetIp
     * @param port
     * @param writeCommunity
     * @param retries
     * @param timeout
     * @param oid
     * @return true if the operation is successful.
     * @throws Exception
     */
    public static boolean setNull(int version, String targetIp, int port, String writeCommunity, int retries, long timeout, String oid) throws Exception
    {
        return instance.set(version, targetIp, port, writeCommunity, retries, timeout, oid, null, SNMPData.Type.Null);
    }

    /**
     * Sets a OID snmp value to the oid.
     *
     * <pre>
     * {@code
     * import com.resolve.gateway.SNMPAPI;
     *
     *         try
     *         {
     *             String deviceIP = "10.30.10.12";
     *             int port = 161;
     *             String writeCommunity = "private";
     *             int retries = 2;
     *             int timeout = 10; //seconds
     *             String oid = "1.3.6.1.2.1.1.1.0"; //make sure it accepts the value type provided.
     *             String oidValue = "1.2.3.4.5";
     *
     *             //convenient constants SNMPAPI.SNMP_VERSION_1, SNMPAPI.SNMP_VERSION_2C, SNMPAPI.SNMP_VERSION_3 
     *             boolean result = SNMPAPI.setOID(SNMPAPI.SNMP_VERSION_2C, deviceIP, port, writeCommunity, retries, timeout, oid, oidValue);
     *             if(result)
     *             {
     *                 System.out.println("Sucessfully set the value.");
     *             }
     *         }
     *         catch (Exception e)
     *         {
     *             // do something with this exception.
     *         }
     * }
     * </pre>
     *
     * @param version
     *            - SNMP version, could be 1, 2, or 3 based on the target device.
     * @param targetIp
     * @param port
     * @param writeCommunity
     * @param retries
     * @param timeout
     * @param oid
     * @param oidValue
     * @return true if the operation is successful.
     * @throws Exception
     */
    public static boolean setOID(int version, String targetIp, int port, String writeCommunity, int retries, long timeout, String oid, String oidValue) throws Exception
    {
        return instance.set(version, targetIp, port, writeCommunity, retries, timeout, oid, oidValue, SNMPData.Type.OID);
    }

    /**
     * Sets a OctetString snmp value to the oid.
     *
     * <pre>
     * {@code
     * import com.resolve.gateway.SNMPAPI;
     *
     *         try
     *         {
     *             String deviceIP = "10.30.10.12";
     *             int port = 161;
     *             String writeCommunity = "private";
     *             int retries = 2;
     *             int timeout = 10; //seconds
     *             String oid = "1.3.6.1.2.1.1.1.0"; //make sure it accepts the value type provided.
     *             String oidValue = "My Value";
     *
     *             //convenient constants SNMPAPI.SNMP_VERSION_1, SNMPAPI.SNMP_VERSION_2C, SNMPAPI.SNMP_VERSION_3 
     *             boolean result = SNMPAPI.setOctetString(SNMPAPI.SNMP_VERSION_2C, deviceIP, port, writeCommunity, retries, timeout, oid, oidValue);
     *             if(result)
     *             {
     *                 System.out.println("Sucessfully set the value.");
     *             }
     *         }
     *         catch (Exception e)
     *         {
     *             // do something with this exception.
     *         }
     * }
     * </pre>
     *
     * @param version
     *            - SNMP version, could be 1, 2, or 3 based on the target device.
     * @param targetIp
     * @param port
     * @param writeCommunity
     * @param retries
     * @param timeout
     * @param oid
     * @param oidValue
     * @return true if the operation is successful.
     * @throws Exception
     */
    public static boolean setOctetString(int version, String targetIp, int port, String writeCommunity, int retries, long timeout, String oid, String oidValue) throws Exception
    {
        return instance.set(version, targetIp, port, writeCommunity, retries, timeout, oid, oidValue, SNMPData.Type.OctetString);
    }

    /**
     * Sets a Opaque snmp value to the oid.
     *
     * <pre>
     * {@code
     * import com.resolve.gateway.SNMPAPI;
     *
     *         try
     *         {
     *             String deviceIP = "10.30.10.12";
     *             int port = 161;
     *             String writeCommunity = "private";
     *             int retries = 2;
     *             int timeout = 10; //seconds
     *             String oid = "1.3.6.1.2.1.1.1.0"; //make sure it accepts the value type provided.
     *             byte[] oidValue = "My Value".getBytes(); //this is just an example to get some bytes in an array.
     *
     *             //convenient constants SNMPAPI.SNMP_VERSION_1, SNMPAPI.SNMP_VERSION_2C, SNMPAPI.SNMP_VERSION_3 
     *             boolean result = SNMPAPI.setOpaque(SNMPAPI.SNMP_VERSION_2C, deviceIP, port, writeCommunity, retries, timeout, oid, oidValue);
     *             if(result)
     *             {
     *                 System.out.println("Sucessfully set the value.");
     *             }
     *         }
     *         catch (Exception e)
     *         {
     *             // do something with this exception.
     *         }
     * }
     * </pre>
     *
     * @param version
     *            - SNMP version, could be 1, 2, or 3 based on the target device.
     * @param targetIp
     * @param port
     * @param writeCommunity
     * @param retries
     * @param timeout
     * @param oid
     * @param oidValue
     *            is a byte[] array.
     * @return true if the operation is successful.
     * @throws Exception
     */
    public static boolean setOpaque(int version, String targetIp, int port, String writeCommunity, int retries, long timeout, String oid, byte[] oidValue) throws Exception
    {
        return instance.set(version, targetIp, port, writeCommunity, retries, timeout, oid, new String(oidValue), SNMPData.Type.Opaque);
    }

    /**
     * Sets a TimeTicks snmp value to the oid.
     *
     * <pre>
     * {@code
     * import com.resolve.gateway.SNMPAPI;
     *
     *         try
     *         {
     *             String deviceIP = "10.30.10.12";
     *             int port = 161;
     *             String writeCommunity = "private";
     *             int retries = 2;
     *             int timeout = 10; //seconds
     *             String oid = "1.3.6.1.2.1.1.1.0"; //make sure it accepts the value type provided.
     *             long oidValue = 100L;
     *
     *             //convenient constants SNMPAPI.SNMP_VERSION_1, SNMPAPI.SNMP_VERSION_2C, SNMPAPI.SNMP_VERSION_3 
     *             boolean result = SNMPAPI.setTimeTicks(SNMPAPI.SNMP_VERSION_2C, deviceIP, port, writeCommunity, retries, timeout, oid, oidValue);
     *             if(result)
     *             {
     *                 System.out.println("Sucessfully set the value.");
     *             }
     *         }
     *         catch (Exception e)
     *         {
     *             // do something with this exception.
     *         }
     *     }
     * }
     * </pre>
     *
     * @param version
     *            - SNMP version, could be 1, 2, or 3 based on the target device.
     * @param targetIp
     * @param port
     * @param writeCommunity
     * @param retries
     * @param timeout
     * @param oid
     * @param oidValue
     * @return true if the operation is successful.
     * @throws Exception
     */
    public static boolean setTimeTicks(int version, String targetIp, int port, String writeCommunity, int retries, long timeout, String oid, long oidValue) throws Exception
    {
        return instance.set(version, targetIp, port, writeCommunity, retries, timeout, oid, Long.toString(oidValue), SNMPData.Type.TimeTicks);
    }

    /**
     * Sets a UnsignedInteger32 snmp value to the oid.
     *
     * <pre>
     * {@code
     * import com.resolve.gateway.SNMPAPI;
     *
     *         try
     *         {
     *             String deviceIP = "10.30.10.12";
     *             int port = 161;
     *             String writeCommunity = "private";
     *             int retries = 2;
     *             int timeout = 10; //seconds
     *             String oid = "1.3.6.1.2.1.1.1.0"; //make sure it accepts the value type provided.
     *             long oidValue = 100L;
     *
     *             //convenient constants SNMPAPI.SNMP_VERSION_1, SNMPAPI.SNMP_VERSION_2C, SNMPAPI.SNMP_VERSION_3 
     *             boolean result = SNMPAPI.setUnsignedInteger32(SNMPAPI.SNMP_VERSION_2C, deviceIP, port, writeCommunity, retries, timeout, oid, oidValue);
     *             if(result)
     *             {
     *                 System.out.println("Sucessfully set the value.");
     *             }
     *         }
     *         catch (Exception e)
     *         {
     *             // do something with this exception.
     *         }
     * }
     * </pre>
     *
     * @param version
     *            - SNMP version, could be 1, 2, or 3 based on the target device.
     * @param targetIp
     * @param port
     * @param writeCommunity
     * @param retries
     * @param timeout
     * @param oid
     * @param oidValue
     * @return true if the operation is successful.
     * @throws Exception
     */
    public static boolean setUnsignedInteger32(int version, String targetIp, int port, String writeCommunity, int retries, long timeout, String oid, long oidValue) throws Exception
    {
        return instance.set(version, targetIp, port, writeCommunity, retries, timeout, oid, Long.toString(oidValue), SNMPData.Type.Unsigned32);
    }
}
