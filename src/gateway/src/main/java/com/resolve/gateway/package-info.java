/******************************************************************************
* (C) Copyright 2015
*
* Resolve Systems, LLC
*
* All rights reserved
* This software is distributed under license and may not be used, copied,
* modified, or distributed without the express written permission of
* Resolve Systems, LLC.
*
******************************************************************************/
/**
 * Contains classes and interfaces for creating Gateways.
 * <p>
 * Gateway allows Resolve System to interact with external event/ticket generating system.
 * Gateways execute in RSRemote, a component of Resolve System. Gateways retrieve (event/ticket) data 
 * from external system and optionally updates/deletes (event/ticket) data from external system.
 * Gateways use configured filters to retrirve specific data from external system and 
 * passes it to RSControl to run asociated runbook. 
 * <p>
 * All classes and interfaces common to all external system gateways are packaged at root level.
 * All classes and interfaces specific to a external system gateway packaged at  
 * external system named sub-package level.
 * All external system specific gateway message handler classes are prefixed with "M" and are
 * packaged at root level. All external system specific gateway publicly accessible functionlaities are exposed via
 * classes prefixed with "API" and are also packaged at at root level.
 * <p>
 * Following is list of some important generic classes and interfaces.
 * <p>
 * {@link com.resolve.gateway.Filter},
 * {@link com.resolve.gateway.BaseFilter},
 * {@link com.resolve.gateway.Gateway},
 * {@link com.resolve.gateway.BaseGateway},
 * {@link com.resolve.gateway.ClusteredGateway},
 * {@link com.resolve.gateway.BaseClusteredGateway},
 * {@link com.resolve.gateway.BaseSocialGateway},
 * {@link com.resolve.gateway.MGateway},
 * {@link com.resolve.gateway.GatewayConstants},
 * {@link com.resolve.gateway.AbstractGatewayAPI}
 *  
 */
package com.resolve.gateway;