/******************************************************************************
* (C) Copyright 2014
*
* Resolve Systems, LLC
*
* All rights reserved
* This software is distributed under license and may not be used, copied,
* modified, or distributed without the express written permission of
* Resolve Systems, LLC.
*
******************************************************************************/

package com.resolve.gateway;

import java.util.List;
import java.util.Map;

import com.resolve.rsbase.MainBase;
import com.resolve.gateway.hpsm.HPSMGateway;
import com.resolve.gateway.netcool.NetcoolGateway;
import com.resolve.util.StringUtils;

public class NetcoolAPI
{
    private static NetcoolGateway instance = NetcoolGateway.getInstance();

    /**
     * Returns the {@link Object} value for the given key in the current
     * NetcoolGateway instance's NameProperty map.
     * 
     * @param name
     *            the name of the property {@link Map} in the
     *            NetcoolGateway instance's {@link NameProperties} object
     * @param key
     *            the key associated with the expected value in the property
     *            {@link Map} returned from the NetcoolGateway's
     *            {@link NameProperties} object
     * @return {@link Object} value associated with the key in the property
     *         {@link Map} returned by the NetcoolGateway instance's
     *         {@link NameProperties} object
     */
    public static Object getNameObject(String name, String key)
    {
        return instance.getNameProperties().getNameObject(name, key);
    }

    /**
     * Uses the given name parameter to return a property {@link Map} within the
     * NetcoolGateway instance's {@link NameProperties} object. If the
     * returned {@link Map} isn't null, it adds the given {@link String} key and
     * {@link Object} value to it.
     * 
     * @param name
     *            the name of the {@link Map} in the NetcoolGateway
     *            instance's {@link NameProperties} object. Cannot be null or
     *            empty.
     * @param key
     *            the key in the {@link Map} returned from the
     *            {@link NameProperties} object in the NetcoolGateway.
     *            Cannot be null or empty.
     * @param value
     *            the value associated with the given key in the {@link Map}
     *            returned from the {@link NameProperties} object in the
     *            NetcoolGateway
     * 
     * @throws Exception
     */
    public static void setNameObject(String name, String key, Object value) throws Exception
    {
        Map nameProperties = getNameProperties(name);
        if(nameProperties != null)
        {
            nameProperties.put(key, value);
            setNameProperties(name, nameProperties, true);
        }
    }
    
    /**
     * Uses the given {@link String} name as a key to return a {@link Map} of
     * properties from the NetcoolGateway instance's {@link NameProperties}
     * object.
     *
     * @param name
     *            The name associated with the properties {@link Map} in the
     *            NetcoolGateway's {@link NameProperties} object. Cannot be
     *            null or empty.
     * @return a Map of NetcoolGateway name properties
     * @throws Exception
     */
    public static Map getNameProperties(String name) throws Exception
    {
        return instance.getNameProperties().getNameProperties(name);
    }

    /**
     * Associates the given name to the given properties {@link Map} in the
     * NetcoolGateway instance's {@link NameProperties} object.
     *
     * @param name
     *            the key in the NetcoolGateway's {@link NameProperties}
     *            object. Cannot be null or empty.
     * @param properties
     *            A Map of properties to associate with the given name parameter
     * @throws Exception
     */
    public static void setNameProperties(String name, Map properties) throws Exception
    {
        setNameProperties(name, properties, true);
    }

    /**
     * Associates the given name to the given properties Map in the HPSMGateway
     * instance's NameProperties object.
     *
     * @param name
     *            the key in the NetcoolGateway's {@link NameProperties}
     *            object. Cannot be null or empty.
     * @param properties
     *            A {@link Map} of properties to associate with the given name
     *            parameter
     * @param doSave
     *            a boolean value that designates whether to send this change to
     *            RSControl for persistence
     * @throws Exception
     */
    public static void setNameProperties(String name, Map properties, boolean doSave) throws Exception
    {
        if (StringUtils.isBlank(name))
        {
            throw new Exception("'name' parameter must be passed");
        }
        else
        {
            instance.setNameProperties(name, properties, doSave);
        }
    }

    /**
     * Removes a {@link Map} of properties associated with the given name in the
     * NetcoolGateway instance's {@link NameProperties} object.
     *
     * @param name
     *            The name associated with a property {@link Map} in the
     *            NetcoolGateway instance's {@link NameProperties} object.
     *            Cannot be null or empty.
     * @throws Exception 
     */
    public static void removeNameProperties(String name) throws Exception
    {
        if (StringUtils.isBlank(name))
        {
            throw new Exception("'name' parameter must be passed");
        }
        else
        {
            instance.removeNameProperties(name);
        }
    }

    /**
     * Saves the configurations in the RSRemote.
     *
     * @throws Exception
     */
    public static void saveConfig() throws Exception
    {
        MainBase.main.callSaveConfig();
    }

    public static void addJournal(Map params) throws Exception
    {
        instance.addJournal(params);
    } // addJournal

    public static List select(String sql) throws Exception
    {
        return instance.select(sql);
    } // select

    public static void update(String sql)
    {
        MNetcool.update(sql);
    } // update

    public static void asyncUpdate(String sql)
    {
        update(sql);
    } // asyncYpdate

    // Synchronous netcool update
    public static String syncUpdate(String sql) throws Exception
    {
        return MNetcool.syncUpdate(sql);
    }
} // NetcoolAPI
