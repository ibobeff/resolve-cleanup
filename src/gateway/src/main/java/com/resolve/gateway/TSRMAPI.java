/******************************************************************************
* (C) Copyright 2014
*
* Resolve Systems, LLC
*
* All rights reserved
* This software is distributed under license and may not be used, copied,
* modified, or distributed without the express written permission of
* Resolve Systems, LLC.
*
******************************************************************************/

package com.resolve.gateway;

import java.util.List;
import java.util.Map;

import com.resolve.rsbase.MainBase;
import com.resolve.gateway.servicenow.ServiceNowGateway;
import com.resolve.gateway.tsrm.TSRMGateway;
import com.resolve.util.StringUtils;

public class TSRMAPI
{
    private static TSRMGateway instance = TSRMGateway.getInstance();

    /**
     * Returns the {@link Object} value for the given key in the current
     * TSRMGateway instance's NameProperty map.
     * 
     * @param name
     *            the name of the property {@link Map} in the
     *            TSRMGateway instance's {@link NameProperties}
     *            object
     * @param key
     *            the key associated with the expected value in the property
     *            {@link Map} returned from the TSRMGateway's
     *            {@link NameProperties} object
     * @return {@link Object} value associated with the key in the property
     *         {@link Map} returned by the TSRMGateway instance's
     *         {@link NameProperties} object
     */
    public static Object getNameObject(String name, String key)
    {
        return instance.getNameProperties().getNameObject(name, key);
    }

    /**
     * Uses the given name parameter to return a property {@link Map} within the
     * TSRMGateway instance's {@link NameProperties} object. If
     * the returned {@link Map} isn't null, it adds the given {@link String} key
     * and {@link Object} value to it.
     * 
     * @param name
     *            the name of the {@link Map} in the TSRMGateway
     *            instance's {@link NameProperties} object. Cannot be null or
     *            empty.
     * @param key
     *            the key in the {@link Map} returned from the
     *            {@link NameProperties} object in the TSRMGateway
     *            . Cannot be null or empty.
     * @param value
     *            the value associated with the given key in the {@link Map}
     *            returned from the {@link NameProperties} object in the
     *            TSRMGateway
     * 
     * @throws Exception
     */
    public static void setNameObject(String name, String key, Object value) throws Exception
    {
        Map nameProperties = getNameProperties(name);
        if(nameProperties != null)
        {
            nameProperties.put(key, value);
            setNameProperties(name, nameProperties, true);
        }
    }
    
    /**
     * Uses the given {@link String} name as a key to return a {@link Map} of
     * properties from the TSRMGateway instance's
     * {@link NameProperties} object.
     *
     * @param name
     *            The name associated with the properties {@link Map} in the
     *            TSRMGateway's {@link NameProperties} object.
     *            Cannot be null or empty.
     * @return a Map of TSRMGateway name properties
     * @throws Exception
     */
    public static Map getNameProperties(String name) throws Exception
    {
        return instance.getNameProperties().getNameProperties(name);
    }

    /**
     * Associates the given name to the given properties {@link Map} in the
     * TSRMGateway instance's {@link NameProperties} object.
     *
     * @param name
     *            the key in the TSRMGateway's
     *            {@link NameProperties} object. Cannot be null or empty.
     * @param properties
     *            A Map of properties to associate with the given name parameter
     * @throws Exception
     */
    public static void setNameProperties(String name, Map properties) throws Exception
    {
        setNameProperties(name, properties, true);
    }

    /**
     * Associates the given name to the given properties Map in the
     * TSRMGateway instance's NameProperties object.
     *
     * @param name
     *            the key in the TSRMGateway's {@link NameProperties}
     *            object. Cannot be null or empty.
     * @param properties
     *            A {@link Map} of properties to associate with the given name
     *            parameter
     * @param doSave
     *            a boolean value that designates whether to send this change to
     *            RSControl for persistence
     * @throws Exception
     */
    public static void setNameProperties(String name, Map properties, boolean doSave) throws Exception
    {
        if (StringUtils.isBlank(name))
        {
            throw new Exception("'name' parameter must be passed");
        }
        else
        {
            instance.setNameProperties(name, properties, doSave);
        }
    }

    /**
     * Removes a {@link Map} of properties associated with the given name in the
     * TSRMGateway instance's {@link NameProperties} object.
     *
     * @param name
     *            The name associated with a property {@link Map} in the
     *            TSRMGateway instance's {@link NameProperties}
     *            object. Cannot be null or empty.
     * @throws Exception
     */
    public static void removeNameProperties(String name) throws Exception
    {
        if (StringUtils.isBlank(name))
        {
            throw new Exception("'name' parameter must be passed");
        }
        else
        {
            instance.removeNameProperties(name);
        }
    }

    /**
     * Saves the configurations in the RSRemote.
     *
     * @throws Exception
     */
    public static void saveConfig() throws Exception
    {
        MainBase.main.callSaveConfig();
    }

    /**
     * Creates a new TSRM Service Request.
     *
     * <pre>
     * {@code
     * try
     * {
     *      String ticketId = "1001";
     *      Map<String, String> params = new HashMap<String, String>();
     *      params.put("ticketid", ticketId);
     *      params.put("description", "Description for the ticket");
     *
     *      Map<String, String> result = TSRMAPI.createServiceRequest(params, "maxadmin", "maxadmin");
     *      // The most important property is the TICKETUID that you may need for future references.
     *      // This is how you get the TICKETUID.
     *      String ticketUid = result.get("TICKETUID");
     *
     *      System.out.printf("Here is your TicketUID: %s for the ticket: %s\n", ticketUid, ticketId);
     *      System.out.printf("Now list all the properties and their values\n");
     *      for(String key : result.keySet())
     *      {
     *          System.out.printf("Property:%s, Value:%s\n", key, result.get(key));
     *      }
     * }
     * catch (Exception ex)
     * {
     *      //Do something good with this error.
     *      ex.printStackTrace();
     * }
     * }
     * </pre>
     *
     * @param params
     *            refer to the TSRM documentation for more information about
     *            various properties you can set for a Service Request. It may
     *            vary by installation.
     * @param username
     *            if not provided uses the gateway configuration.
     * @param password
     *            ties with the username..
     * @return a {@link Map} with all the properties for the ticket.
     * @throws Exception
     */
    public static Map<String, String> createServiceRequest(Map<String, String> params, String username, String password) throws Exception
    {
        return instance.createServiceRequest(params, username, password);
    }

    /**
     * Updates a TSRM Service Request.
     *
     * Note: TSRM may not allow update on certain properties, please refer to
     * the TSRM documentation for more information. It may vary by the
     * installations.
     *
     * <pre>
     * {@code
     * try
     *  {
     *      String ticketUID = "1999";
     *
     *      Map<String, String> params = new HashMap<String, String>();
     *      params.put("description", "Changed the description for the ticket");
     *
     *      TSRMAPI.updateServiceRequest(ticketUID, params, "maxadmin", "maxadmin");
     *  }
     *  catch (Exception ex)
     *  {
     *       //Do something good with this error.
     *       ex.printStackTrace();
     *  }
     *  }
     * </pre>
     *
     * @param ticketUID
     *            must be provided. It's the TicketUID auto-generated by the
     *            system.
     * @param params
     * @param username
     *            if not provided uses the gateway configuration.
     * @param password
     *            ties with the username. Gateway configuration is used if not
     *            provided.
     * @throws Exception
     */
    public static void updateServiceRequest(String ticketUID, Map<String, String> params, String username, String password) throws Exception
    {
        instance.updateServiceRequest(ticketUID, params, username, password);
    }

    /**
     * Updates Service Request status.
     *
     * Note: TSRM may not allow changing status to any arbitrary one due to
     * business rules. Please refer to the documentation for more information.
     *
     * <pre>
     * {@code
     * try
     * {
     *      //TICKET Update
     *      String updatedStatus = "RESOLVED";
     *      String ticketUID = "1999";
     *
     *      TSRMAPI.updateServiceRequestStatus(ticketUID, updatedStatus, "maxadmin", "maxadmin");
     * }
     * catch (Exception ex)
     * {
     *      //Do something good with this error.
     *      ex.printStackTrace();
     * }
     * }
     * </pre>
     *
     * @param ticketUID
     *            must be provided. It's the TicketUID auto-generated by the
     *            system.
     * @param status
     * @param username
     * @param password
     * @throws Exception
     */
    public static void updateServiceRequestStatus(String ticketUID, String status, String username, String password) throws Exception
    {
        instance.updateServiceRequestStatus(ticketUID, status, username, password);
    }

    /**
     * Deletes a TSRM Service Request.
     *
     * Note: TSRM may not allow deleting a Service Request due to certain
     * dependencies (e.g., status, worklog etc.). Please refer to the
     * documentation.
     *
     * <pre>
     * {@code
     * try
     *  {
     *      String ticketUID = "1999";
     *      TSRMAPI.deleteServiceRequest(ticketUID, "maxadmin", "maxadmin");
     *  }
     *  catch (Exception ex)
     *  {
     *       //Do something good with this error.
     *       ex.printStackTrace();
     *  }
     * }
     * </pre>
     *
     * @param ticketUID
     *            must be provided. It's the TicketUID auto-generated by the
     *            system.
     * @param username
     *            if not provided uses the gateway configuration.
     * @param password
     *            ties with the username. Gateway configuration is used if not
     *            provided.
     * @throws Exception
     */
    public static void deleteServiceRequest(String ticketUID, String username, String password) throws Exception
    {
        instance.deleteServiceRequest(ticketUID, username, password);
    }

    /**
     * Searches TSRM Service Request(s) based on the query. The query string
     * must be compliant with the REST API defined by the TSRM documentation for
     * Service Request (SR) object.
     *
     * <pre>
     * {@code
     *  try
     *  {
     *      String query = "ticketid=1001&description=my desc";
     *      List<Map<String, String>> tickets = TSRMAPI.searchServiceRequest(query, "maxadmin", "maxadmin");
     *
     *      for(Map<String, String> ticket : tickets)
     *      {
     *          int i = 1;
     *          System.out.printf("Ticket# %d:", i++);
     *          for(String key : ticket.keySet())
     *          {
     *              System.out.printf("Property:%s, Value:%s\n", key, ticket.get(key));
     *          }
     *      }
     *  }
     *  catch (Exception ex)
     *  {
     *      //Do something good with this error.
     *      ex.printStackTrace();
     *  }
     *  }
     * </pre>
     *
     * @param query
     *            do not need to be pre-encoded.
     * @param username
     *            if not provided uses the gateway configuration.
     * @param password
     *            ties with the username. Gateway configuration is used if not
     *            provided.
     * @return a {@link List} of {@link Map} which contain properties for all
     *         the tickets.
     * @throws Exception
     */
    public static List<Map<String, String>> searchServiceRequest(String query, String username, String password) throws Exception
    {
        return instance.searchServiceRequest(query, username, password);
    }

    /**
     * Selects TSRM Service Request based on its ID.
     *
     * <pre>
     * {@code
     * try
     * {
     *      String ticketUID = "1999";
     *      Map<String, String> result = TSRMAPI.selectServiceRequestByUID(ticketUID, "maxadmin", "maxadmin");
     *      for(String key : result.keySet())
     *      {
     *          System.out.printf("Property:%s, Value:%s\n", key, result.get(key));
     *      }
     *  }
     *  catch (Exception ex)
     *  {
     *      //Do something good with this error.
     *      ex.printStackTrace();
     *  }
     *  }
     * </pre>
     *
     * @param ticketUID
     *            must be provided. It's the TicketUID auto-generated by the
     *            system.
     * @param username
     *            if not provided uses the gateway configuration.
     * @param password
     *            ties with the username. Gateway configuration is used if not
     *            provided.
     * @return a {@link Map} that contains properties of the ticket.
     * @throws Exception
     */
    public static Map<String, String> selectServiceRequestByUID(String ticketUID, String username, String password) throws Exception
    {
        return instance.selectServiceRequestByUID(ticketUID, username, password);
    }

    /**
     * Creates worklog for a ServiceRequest.
     *
     * <pre>
     * {@code
     * try
     * {
     *      String ticketUID = "1999";
     *
     *      Map<String, String> worklogParams = new HashMap<String, String>();
     *      worklogParams.put("logtype", "CLIENTNOTE");
     *      worklogParams.put("clientviewable", "1");
     *      worklogParams.put("description", "Worklog desc");
     *      TSRMAPI.createServiceRequestWorklog(ticketUID, worklogParams, "maxadmin", "maxadmin");
     * }
     * catch (Exception ex)
     * {
     *      //Do something good with this error.
     *      ex.printStackTrace();
     * }
     * }
     * </pre>
     *
     * @param parentTicketUID
     *            must be provided. It's the Ticket's UID auto-generated by the
     *            system. The work log will be created for this ticket.
     * @param params
     *            refer to the TSRM documentation for more information about
     *            various properties you can set for the worklog. It may vary by
     *            installation.
     * @param username
     *            if not provided uses the gateway configuration.
     * @param password
     *            ties with the username.
     * @return true if service request is created successfully.
     * @throws Exception
     */
    public static boolean createServiceRequestWorklog(String parentTicketUID, Map<String, String> params, String username, String password) throws Exception
    {
        return instance.createServiceRequestWorklog(parentTicketUID, params, username, password);
    }

    /**
     * Creates a new TSRM incident.
     *
     * <pre>
     * {@code
     * try
     * {
     *      Map<String, String> params = new HashMap<String, String>();
     *      params.put("description", "Description for the incident");
     *
     *      Map<String, String> result = TSRMAPI.createIncident(params, "maxadmin", "maxadmin");
     *      // The most important property is the TICKETUID that you may need for future references.
     *      // This is how you get the TICKETUID.
     *      String ticketId = result.get("TICKETID");
     *      String ticketUid = result.get("TICKETUID");
     *
     *      System.out.printf("Here is your TicketId: %s and the TicketUID: %s for the incident\n", ticketId, ticketUid);
     *      System.out.printf("Now list all the properties and their values\n");
     *      for(String key : result.keySet())
     *      {
     *          System.out.printf("Property:%s, Value:%s\n", key, result.get(key));
     *      }
     * }
     * catch (Exception ex)
     * {
     *      //Do something good with this error.
     *      ex.printStackTrace();
     * }
     * }
     * </pre>
     *
     * @param params
     *            refer to the TSRM documentation for more information about
     *            various properties you can set for an incident. It may vary by
     *            installation.
     * @param username
     *            if not provided uses the gateway configuration.
     * @param password
     *            ties with the username..
     * @return a {@link Map} with all the properties for the incidents.
     * @throws Exception
     */
    public static Map<String, String> createIncident(Map<String, String> params, String username, String password) throws Exception
    {
        return instance.createIncident(params, username, password);
    }

    /**
     * Creates worklog for an Incident.
     *
     * <pre>
     * {@code
     * try
     * {
     *      String ticketUID = "1999";
     *
     *      Map<String, String> worklogParams = new HashMap<String, String>();
     *      worklogParams.put("logtype", "CLIENTNOTE");
     *      worklogParams.put("clientviewable", "1");
     *      worklogParams.put("description", "Worklog desc");
     *      TSRMAPI.createIncidentWorklog(ticketUID, worklogParams, "maxadmin", "maxadmin");
     * }
     * catch (Exception ex)
     * {
     *      //Do something good with this error.
     *      ex.printStackTrace();
     * }
     * }
     * </pre>
     *
     * @param parentTicketUID
     *            must be provided. It's the Ticket's UID auto-generated by the
     *            system. The work log will be created for this incident.
     * @param params
     *            refer to the TSRM documentation for more information about
     *            various properties you can set for the worklog. It may vary by
     *            installation.
     * @param username
     *            if not provided uses the gateway configuration.
     * @param password
     *            ties with the username.
     * @return true if incident is created successfully.
     * @throws Exception
     */
    public static boolean createIncidentWorklog(String parentTicketUID, Map<String, String> params, String username, String password) throws Exception
    {
        return instance.createIncidentWorklog(parentTicketUID, params, username, password);
    }

    /**
     * Updates a TSRM incident.
     *
     * Note: TSRM may not allow update on certain properties, please refer to
     * the TSRM documentation for more information. It may vary by the
     * installations.
     *
     * <pre>
     * {@code
     * try
     *  {
     *      String ticketUID = "1999";
     *
     *      Map<String, String> params = new HashMap<String, String>();
     *      params.put("description", "Changed the description for the incident");
     *      TSRMAPI.updateIncident(ticketUID, params, "maxadmin", "maxadmin");
     *  }
     *  catch (Exception ex)
     *  {
     *       //Do something good with this error.
     *       ex.printStackTrace();
     *  }
     *  }
     * </pre>
     *
     * @param ticketUID
     *            must be provided. It's the TicketUID auto-generated by the
     *            system.
     * @param params
     * @param username
     *            if not provided uses the gateway configuration.
     * @param password
     *            ties with the username. Gateway configuration is used if not
     *            provided.
     * @throws Exception
     */
    public static void updateIncident(String ticketUID, Map<String, String> params, String username, String password) throws Exception
    {
        instance.updateIncident(ticketUID, params, username, password);
    }

    /**
     * Updates incident status.
     *
     * Note: TSRM may not allow changing status to any arbitrary one due to
     * business rules. Please refer to the documentation for more information.
     *
     * <pre>
     * {@code
     * try
     * {
     *      //TICKET Update
     *      String updatedStatus = "RESOLVED";
     *      String ticketUID = "1999";
     *
     *      TSRMAPI.updateIncidentStatus(ticketUid, updatedStatus, "maxadmin", "maxadmin");
     * }
     * catch (Exception ex)
     * {
     *      //Do something good with this error.
     *      ex.printStackTrace();
     * }
     * }
     * </pre>
     *
     * @param ticketUID
     *            must be provided. It's the TicketUID auto-generated by the
     *            system.
     * @param status
     * @param username
     * @param password
     * @throws Exception
     */
    public static void updateIncidentStatus(String ticketUID, String status, String username, String password) throws Exception
    {
        instance.updateIncidentStatus(ticketUID, status, username, password);
    }

    /**
     * Deletes a TSRM incident.
     *
     * Note: TSRM may not allow deleting an incident due to certain dependencies
     * (e.g., status etc.). Please refer to the documentation.
     *
     * <pre>
     * {@code
     * try
     *  {
     *      String ticketUID = "1999";
     *      TSRMAPI.deleteIncident(ticketUid, "maxadmin", "maxadmin");
     *  }
     *  catch (Exception ex)
     *  {
     *       //Do something good with this error.
     *       ex.printStackTrace();
     *  }
     * }
     * </pre>
     *
     * @param ticketUID
     *            must be provided. It's the TicketUID auto-generated by the
     *            system.
     * @param username
     *            if not provided uses the gateway configuration.
     * @param password
     *            ties with the username. Gateway configuration is used if not
     *            provided.
     * @throws Exception
     */
    public static void deleteIncident(String ticketUID, String username, String password) throws Exception
    {
        instance.deleteIncident(ticketUID, username, password);
    }

    /**
     * Selects TSRM incident based on the ticketUID.
     *
     * <pre>
     * {@code
     * try
     * {
     *      String ticketUID = "1999";
     *      Map<String, String> result = TSRMAPI.selectIncidentByUID(ticketUID, "maxadmin", "maxadmin");
     *      for(String key : result.keySet())
     *      {
     *          System.out.printf("Property:%s, Value:%s\n", key, result.get(key));
     *      }
     *  }
     *  catch (Exception ex)
     *  {
     *      //Do something good with this error.
     *      ex.printStackTrace();
     *  }
     *  }
     * </pre>
     *
     * @param ticketUID
     *            must be provided. It's the TicketUID auto-generated by the
     *            system.
     * @param username
     *            if not provided uses the gateway configuration.
     * @param password
     *            ties with the username. Gateway configuration is used if not
     *            provided.
     * @return a {@link Map} that contains properties of the ticket.
     * @throws Exception
     */
    public static Map<String, String> selectIncidentByUID(String ticketUID, String username, String password) throws Exception
    {
        return instance.selectIncidentByUID(ticketUID, username, password);
    }

    /**
     * Searches TSRM incident(s) based on the query. The query string must be
     * compliant with the REST API defined by the TSRM documentation for
     * Incident object (INCIDENT) object.
     *
     * <pre>
     * {@code
     *  try
     *  {
     *      String query = "ticketid=1001&description=my desc";
     *      List<Map<String, String>> incidents = TSRMAPI.searchIncident(query, null, null);
     *
     *      for(Map<String, String> incident : incidents)
     *      {
     *          int i = 1;
     *          System.out.printf("Incident# %d:", i++);
     *          for(String key : incident.keySet())
     *          {
     *              System.out.printf("Property:%s, Value:%s\n", key, incident.get(key));
     *          }
     *      }
     *  }
     *  catch (Exception ex)
     *  {
     *      //Do something good with this error.
     *      ex.printStackTrace();
     *  }
     *  }
     * </pre>
     *
     * @param query
     *            do not need to be pre-encoded.
     * @param username
     *            if not provided uses the gateway configuration.
     * @param password
     *            ties with the username. Gateway configuration is used if not
     *            provided.
     * @return a {@link List} of {@link Map} which contain properties for all
     *         the tickets.
     * @throws Exception
     */
    public static List<Map<String, String>> searchIncident(String query, String username, String password) throws Exception
    {
        return instance.searchIncident(query, username, password);
    }

    /**
     * Searches TSRM object(s) based on the query. The query string must be
     * compliant with the REST API defined by the TSRM documentation for the
     * target object.
     *
     * <pre>
     * {@code
     *  try
     *  {
     *      String query = "description=test";
     *      String result = TSRMAPI.search("SRM_SRDET", query, null, null);
     *
     *      System.out.println(result);
     *  }
     *  catch (Exception ex)
     *  {
     *      //Do something good with this error.
     *      ex.printStackTrace();
     *  }
     *  }
     * </pre>
     *
     * @param objectURI
     *            Part of the URI for the target object. For example, SRM_SR or
     *            SRM_SRDET etc.
     * @param query
     *            do not need to be pre-encoded.
     * @param username
     *            if not provided uses the gateway configuration.
     * @param password
     *            ties with the username. Gateway configuration is used if not
     *            provided.
     * @return a {@link String} which contain result in XML format.
     * @throws Exception
     */
    public static String search(String objectURI, String query, String username, String password) throws Exception
    {
        return instance.search(objectURI, query, username, password);
    }
}
