/******************************************************************************
 * (C) Copyright 2015
 *
 * Resolve Systems, LLC
 *
 * All rights reserved This software is distributed under license and may not be
 * used, copied, modified, or distributed without the express written permission
 * of Resolve Systems, LLC.
 *
 ******************************************************************************/

package com.resolve.gateway;

import java.util.HashMap;
import java.util.Map;

import com.resolve.rsbase.MainBase;
import com.resolve.util.Log;
import com.resolve.util.StringUtils;

/**
 * This class is used by the Self-Check Message Listener for gateway
 * configured and primary only for processing self-check messages
 * sent to gateway worker queue.
 *
 *@author Hemant Phanasgaonkar
 */
public class MRSSelfCheck
{
    /**
     * Process self-check request.
     */
    public Map<String, String> selfCheck(Map<String, String> params)
    {
        if (Log.log.isDebugEnabled())
        {
            String msgContent = "";
            
            if (params != null && !params.isEmpty())
            {
                msgContent = StringUtils.mapToLogString(params);
            }
            
            Log.log.debug("Resolve Self-Check Message Handler on instance " + MainBase.main.configId.getGuid() + " received Self-Check message [" + msgContent + "]");
        }
        
        Map<String, String> response = new HashMap<String, String>();
        response.put("selfCheck", "Response");
        return response;
    }
}
