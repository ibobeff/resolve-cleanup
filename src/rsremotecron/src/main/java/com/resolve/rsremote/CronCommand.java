/******************************************************************************
* (C) Copyright 2019
*
* Resolve Systems, LLC
*
* All rights reserved
* This software is distributed under license and may not be used, copied,
* modified, or distributed without the express written permission of
* Resolve Systems, LLC.
*
******************************************************************************/
package com.resolve.rsremote;

import java.io.FileDescriptor;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.PrintWriter;
import java.io.RandomAccessFile;
import java.nio.channels.FileLock;
import java.nio.charset.StandardCharsets;
import java.security.KeyFactory;
import java.security.PublicKey;
import java.security.Signature;
import java.security.spec.X509EncodedKeySpec;
import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;
import java.util.concurrent.TimeUnit;

import org.apache.commons.codec.binary.Hex;
import org.apache.logging.log4j.Level;
import org.apache.logging.log4j.core.Logger;
import org.apache.logging.log4j.core.LoggerContext;
import org.apache.logging.log4j.core.config.Configurator;
import org.apache.logging.log4j.core.config.builder.api.AppenderComponentBuilder;
import org.apache.logging.log4j.core.config.builder.api.ConfigurationBuilder;
import org.apache.logging.log4j.core.config.builder.api.ConfigurationBuilderFactory;
import org.apache.logging.log4j.core.config.builder.impl.BuiltConfiguration;
/**
 * 
 * This is called by a cron job to execute sequence of commands in a file.
 * Commands are digitally signed and signatures must be verified and once
 * verified, command will be executed. Command with invalid signature are
 * ignored and removed from the command file and the next command in the
 * file will be processed. If there is no command file, this program just
 * doesn't do anything and will terminate.
 * 
 * Only one thread can run this code at a time. If a thread, say thread B,
 * is trying to run while other thread is running, thread B will not run.
 * This is enforced by the existence of a locking file.
 * 
 * Refer to the AppTest on the test folder for example of how to sign a command
 * and generate the public key.
 * 
 * @author giao.vo
 *
 */
public class CronCommand {
    private static final String SIGNATURE_SEPARATOR = "SIGNATURE";
    private static final String COMMANDS_SEPARATOR = "RSREMOTECRON\n";
    private static final String LOCKING_FILE = "RSREMOTECRON_LOCK";
    private static final String USERNAME_SEPARATOR = "USERNAMESEPARATOR";
    /**
     * @param args must contain 
     *   1. the path to the file containing commands. Each command precedes by a
     *      username and follows by a signature (separated by a keyword SIGNATURE).
     *      commands are separated by a COMMANDS_SEPARATOR. For example, this is how
     *      a command format looks in the commands file:
     *      gvoUSERNAMESEPARATORstop.shSIGNATURE302c02141b69eea340bd6c9a90ecbb460d4b6197eccd037302144d3c7f32f2c1f4e059c825c1053ec6d68d099ec3RSREMOTECRON
     *   2. the path to public key
     */
    public static void main(String[] args) {
        // Initialize syslog
        Logger log = createSyslogLogger();

        if (args.length < 2) {
            log.error("CronApp Required arguments not met: commands file path and public key file path");
            return;
        }

        String commandFilePath = args[0];
        String publicKeyFilePath = args[1];
        

        // Make sure that no other thread is running this code by
        // checking the existence of a file (the lock).
        FileOutputStream fos = null;
        FileLock fl = null;
        String username = "";
        try {
            fos = new FileOutputStream(LOCKING_FILE);
            fl = fos.getChannel().tryLock();
            if (fl == null) {
                // Other process is running so terminating
                fos.close();
                log.info("CronApp Another thread is currently running ... terminating.");
                return;
            }
        } catch (Exception e) {
            // Can't obtain the lock for unknown reason so terminating
            log.error("CronApp Can not verify if other thread is running; terminating.", e);
            return;
        }

        try {
            // Read the public key
            FileInputStream publicKeyFIS = new FileInputStream(publicKeyFilePath);
            byte[] encKey = new byte[publicKeyFIS.available()];  
            publicKeyFIS.read(encKey);
            publicKeyFIS.close();
            X509EncodedKeySpec pubKeySpec = new X509EncodedKeySpec(encKey);
            KeyFactory keyFactory = KeyFactory.getInstance("DSA", "SUN");
            PublicKey publicKey = keyFactory.generatePublic(pubKeySpec);

            // Repeatedly remove one command from commands file and execute the
            // command if the signature is valid until commands file is empty.
            // The command file is locked while it's being accessed to make sure
            // that no other process will write to the file.
            boolean done;
            do {
                RandomAccessFile raf = new RandomAccessFile(commandFilePath, "rw");

                FileDescriptor fd = raf.getFD();
                FileLock cfil = raf.getChannel().lock(0, raf.length(), false);
                FileInputStream cfis = new FileInputStream(fd);
                Scanner commandScanner = new Scanner(cfis);
                commandScanner.useDelimiter(COMMANDS_SEPARATOR);
                List<String> commands = new ArrayList<String>();
                while (commandScanner.hasNext()) {
                    commands.add(commandScanner.next());
                }
                done = true; // Initial assumption
                int cmdLen = commands.size();
                if (cmdLen == 0) {
                    break;
                }
                PrintWriter pw = null;
                FileOutputStream cfos = null;

                // Clear commands file
                raf.setLength(0);
                fd.sync();

                // Write all but the first command.
                if (cmdLen >= 1) {
                    cfos = new FileOutputStream(fd);
                    pw = new PrintWriter(cfos, true);
                    // Remove the 1st command from the commands file
                    for (int i=1; i < cmdLen; i++) {
                        pw.print(String.format("%s%s", commands.get(i), COMMANDS_SEPARATOR));
                        done = false; // there are still some commands in the file
                    }
                    if (cmdLen == 1) {
                        // Empty command file since it has only 1 command.
                        pw.print("");
                    }
                }
                cfil.release();
                pw.close();
                commandScanner.close();
                cfos.close();
                raf.close();

                // Validate signature and execute command.
                try {
                    String[] commandAndSignature = commands.get(0).split(SIGNATURE_SEPARATOR, 2);
                    if (commandAndSignature.length != 2) {
                        log.error("CronApp Improper command format; a command must follow by a signature.");
                        continue;
                    }
                    /* create a Signature object and initialize it with the public key */
                    Signature signature = Signature.getInstance("SHA1withDSA", "SUN");
                    signature.initVerify(publicKey);
                    String[] usrAndCmd = commandAndSignature[0].split(USERNAME_SEPARATOR);
                    if (usrAndCmd.length != 2) {
                        log.error("CronApp Improper command format; missing a user name.");
                        continue;
                    }
                    username = usrAndCmd[0];
                    String cmd = usrAndCmd[1];
                    String sig = commandAndSignature[1];
                    signature.update(cmd.getBytes(StandardCharsets.UTF_8));
                    boolean isValidSignature = signature.verify(Hex.decodeHex(sig.toCharArray()));
                    if (isValidSignature) {
                        log.info(String.format("%s CronApp Running command %s", username, cmd));
                        Process process = Runtime.getRuntime().exec(cmd);
                        if (!process.waitFor(630, TimeUnit.SECONDS) ) {
                            log.error(String.format("%s CronApp command %s didn't execute successfully", username, cmd));
                        }
                    } else {
                        log.error(String.format("%s CronApp Invalid signature; refuse to run command %s not run", username, cmd));
                    }
                } catch (Throwable t) {
                    log.error(String.format("%s CronApp Exception occurs.", username), t);
                } 
            } while(!done);
            // End process one command

        } catch (Throwable e) {
            log.error(String.format("%s CronApp Exception occurs.", username), e);
        }
        try {
            fl.release();
            fos.close();
        } catch (IOException e) {
            log.error(String.format("%s CronApp can't close or unlock the file.", username), e);
        }
    }

    /**
     * This method creates log4j configurations for syslog, instantiates and return the logger
     * @return the syslog instance.
     */
    private static Logger createSyslogLogger()
    {
        String syslogAppenderLogger = "syslogAppender";
        ConfigurationBuilder< BuiltConfiguration > builder = ConfigurationBuilderFactory.newConfigurationBuilder();
        builder.setStatusLevel( Level.INFO );

        // Create SyslogAppender
        AppenderComponentBuilder appenderBuilder = builder.newAppender(syslogAppenderLogger, "Syslog" )
                .addAttribute("host", "127.0.0.1")
                .addAttribute("port", 514)
                .addAttribute("facility", "LOCAL0")
                .addAttribute("immediateFlush", true)
                .addAttribute("newLine", true);
        builder.add(appenderBuilder);

        // Create new logger
        builder.add(builder.newLogger( syslogAppenderLogger, Level.INFO)
                .add( builder.newAppenderRef(syslogAppenderLogger))
                .addAttribute("additivity", false));

        builder.add(builder.newRootLogger(Level.INFO)
               .add( builder.newAppenderRef(syslogAppenderLogger)));

        // Initialize and instantiate the logger.
        LoggerContext ctx = Configurator.initialize(builder.build());
        Logger logger = ctx.getLogger(syslogAppenderLogger);
        return logger;
    }
}
