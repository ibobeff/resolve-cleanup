package com.resolve.rsremote;

import java.io.File;
import java.io.FileOutputStream;
import java.nio.charset.StandardCharsets;
import java.security.KeyPair;
import java.security.KeyPairGenerator;
import java.security.PrivateKey;
import java.security.PublicKey;
import java.security.SecureRandom;
import java.security.Signature;

import org.apache.commons.codec.binary.Hex;
import org.apache.commons.io.FileUtils;
import org.apache.logging.log4j.Level;
import org.apache.logging.log4j.core.Filter;
import org.apache.logging.log4j.core.Logger;
import org.apache.logging.log4j.core.LoggerContext;
import org.apache.logging.log4j.core.appender.ConsoleAppender;
import org.apache.logging.log4j.core.config.Configurator;
import org.apache.logging.log4j.core.config.builder.api.AppenderComponentBuilder;
import org.apache.logging.log4j.core.config.builder.api.ConfigurationBuilder;
import org.apache.logging.log4j.core.config.builder.api.ConfigurationBuilderFactory;
import org.apache.logging.log4j.core.config.builder.impl.BuiltConfiguration;
import org.junit.Test;

/**
 * This test is an example on how a command file and public key are generated.
 * A command file is a set of items separated by a delimiter 'RSREMOTECRON\n'.
 * Each item has a command preceded by a username and followed by the signature.
 * username and command are separated by SIGNATURE_SEPARATOR. command and
 * A command and the signature are separated by a delimiter 'SIGNATURE'.
 */
public class AppTest 
{
    private static final String COMMANDS_FILE_PATH = "commandsAndSignaturesFile";
    private static final String PUBLICKEY_FILE_PATH = "publickey";
    private static final String USERNAME_SEPARATOR = "USERNAMESEPARATOR";
    private static final String SIGNATURE_SEPARATOR = "SIGNATURE";
    private static final String COMMANDS_SEPARATOR = "RSREMOTECRON\n";
    private static final String STOP_COMMAND = "/usr/bin/touch /tmp/rsremotecron";
    private static final String CONFIG_COMMAND = "/usr/bin/ls /tmp/rsremotecron";
    private static final String START_COMMAND = "/usr/bin/cat /tmp/rsremotecron";
    private static final String[] COMMANDS =  {STOP_COMMAND, CONFIG_COMMAND, START_COMMAND};
    /**
     * Rigorous Test :-)
     */
    @Test
    public void generateSignedCommandFile()
    {
        generateSignedCommandFile(COMMANDS);
    }

    private void generateSignedCommandFile(String[] commands) {
      Logger log = createConsoleLogger();
       try {
            /* Generate a key pair */
            KeyPairGenerator keyGen = KeyPairGenerator.getInstance("DSA", "SUN");
            SecureRandom random = SecureRandom.getInstance("SHA1PRNG", "SUN");

            keyGen.initialize(1024, random);

            KeyPair pair = keyGen.generateKeyPair();
            PrivateKey priv = pair.getPrivate();
            PublicKey pub = pair.getPublic();

            File commandsAndSignaturesFile = new File(COMMANDS_FILE_PATH);
            for (String command: commands) {
                /* Create a Signature object and initialize it with the private key */
                Signature dsa = Signature.getInstance("SHA1withDSA", "SUN"); 
                dsa.initSign(priv);
                /* Update and sign the data */
                dsa.update(command.getBytes(StandardCharsets.UTF_8));
                byte[] realSignature = dsa.sign();
                /* Save the signature in a file */
                FileUtils.writeStringToFile(
                    commandsAndSignaturesFile,
                    String.format("%s%s%s%s%s%s", "gvo", USERNAME_SEPARATOR, command, SIGNATURE_SEPARATOR, Hex.encodeHexString(realSignature), COMMANDS_SEPARATOR),
                    StandardCharsets.UTF_8.name(),
                    true
                );
                log.info(String.format("Generate command and signature for: %s", command));
            }
            log.info(String.format("Saving coomand file to: %s", COMMANDS_FILE_PATH));
            /* Save the public key in a file */
            byte[] key = pub.getEncoded();
            log.info(String.format("Saving public key to: %s", PUBLICKEY_FILE_PATH));
            FileOutputStream keyfos = new FileOutputStream(PUBLICKEY_FILE_PATH);
            keyfos.write(key);

            keyfos.close();
        } catch (Exception e) {
            log.error("Caught exception", e);
        }
    }

    /**
     * This method creates log4j configurations for Stdout appender, instantiates and return the logger
     * @return the Stdout instance.
     */
    private static Logger createConsoleLogger() {
        String loggerName = "Stdout";
        ConfigurationBuilder<BuiltConfiguration> builder = ConfigurationBuilderFactory.newConfigurationBuilder();
        builder.setStatusLevel(Level.INFO)
               .setConfigurationName("rsremoteCronLogConfig")
               .add(builder.newFilter("ThresholdFilter", Filter.Result.ACCEPT, Filter.Result.NEUTRAL).addAttribute("level", Level.INFO));

        AppenderComponentBuilder appenderBuilder = builder.newAppender(loggerName, "CONSOLE").addAttribute("target", ConsoleAppender.Target.SYSTEM_OUT);
        appenderBuilder.add(builder.newLayout("PatternLayout").addAttribute("pattern", "%d [%t] %-5level: %msg%n%throwable"));
        appenderBuilder.add(builder.newFilter("MarkerFilter", Filter.Result.DENY, Filter.Result.NEUTRAL).addAttribute("marker", "FLOW"));

        builder.add(appenderBuilder)
               .add(builder.newLogger("org.apache.logging.log4j", Level.INFO)
               .add(builder.newAppenderRef("Stdout")).addAttribute("additivity", false))
               .add(builder.newRootLogger(Level.INFO).add(builder.newAppenderRef("Stdout")));

        LoggerContext ctx = Configurator.initialize(builder.build());
        return ctx.getLogger(loggerName);
    }
}
