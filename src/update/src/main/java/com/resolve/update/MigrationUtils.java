package com.resolve.update;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.commons.lang3.StringUtils;
import org.apache.log4j.Logger;
import org.codehaus.jackson.map.ObjectMapper;


public class MigrationUtils
{
    private static final ObjectMapper MAPPER = new ObjectMapper();
    public static Logger log;

    public static void setLogger(Logger l)
    {
        log = l;
    }
    
    /**
     * Compare individual index doc using provided index column/field names. It can be used to compare index data
     * migrated from ES 1.7 cluster to ES 5.4 cluster. 
     * It looks for the same index doc by sysId from both ES 1.7 server and ES 5.4 server.  
     * If found, check all required fields/columns for comparison. If all columns have same values, return true. 
     * Otherwise or any error happens, return false.    
     *
     *  Example: isSame = compareIndexDataById("worksheet_20170608", "e06d031626594671bda0840d7d77e887", null, "localhost", "9300", "10.50.2.249", "9700");
     * 
     * @param index           Resolve index name in ES 
     * @param sysId           Resolve index document id 
     * @param fieldNames      Index column names required for comparison ( if null, all columns will be compared instead)
     * @param es17Host        ES 1.7 server hostname/ip 
     * @param es17Port        ES 1.7 server port 
     * @param es54Host        ES 5.4 server hostname/ip  
     * @param es54Port        ES 5.4 server port 
     * @return                true if all required fields have same values, false otherwise. 
     */
    public static boolean compareIndexDataById(String index, String sysId, String[] fieldNames, String es17Host, String es17Port, String es54Host, String es54Port)
    {
        boolean result = true;
        String doc1 = findDocBySysid(index, sysId, es17Host, es17Port);
        String doc2 = findDocBySysid(index, sysId, es54Host, es54Port);
        if (StringUtils.isEmpty(doc1))
        {
            log.error("Can't find index doc by sysId. Index: " + index + ", sysId: " + sysId + ", host: " + es17Host + ", port: " + es17Port);
            result = false;
        }
        
        if (StringUtils.isEmpty(doc2))
        {
            log.error("Can't find index doc by sysId. Index: " + index + ", sysId: " + sysId + ", host: " + es54Host + ", port: " + es54Port);
            result = false;
        }

        if (result)
        {
            result = compareIndexJsonDocs(doc1, doc2, fieldNames);
        }
        return result;
    }
    
    
    /**
     * Compare all docs from the same index on two ES servers (e.g. ES 1.7 and ES 5.4). It can be used to compare index data    
     * migrated from ES 1.7 server to ES 5.4 server.
     * 
     * @param index           target index name
     * @param fieldNames      index field/column names checked for equality  
     * @param es17Host        first ES server host/ip
     * @param es17Port        first ES server port
     * @param es54Host        second ES server host/ip 
     * @param es54Port        second ES server port
     * @return                return "true" if all docs have equal values on specified columns   
     */
    public static boolean compareIndexData(String index, String[] fieldNames, String es17Host, String es17Port, String es54Host, String es54Port)
    {
        boolean result = true;
        int total = 0;
        try 
        {
            Integer es17Total = getIndexDocTotal(index, es17Host, es17Port);
            Integer es54Total = getIndexDocTotal(index, es17Host, es17Port);
            if (es17Total.compareTo(es54Total)!=0)
            {
                log.info("Index doc total are not equal: index= " + index + ", host " + es17Host + " total= " + es17Total + ", host " + es54Host + " total= " + es54Total);
                return false;
            }
            else
            {
                total = es17Total;
            }
        }
        catch (Throwable t)
        {
            log.error(t,t);
            return false;
        }
        
        try
        {
            int pageSize = 2;
            int start =0;
            int left = total;
         
          search:  
            while (left>0)
            {
                String[] ids = getIndexSysids(index, es17Host, es17Port, Integer.toString(start), ""+pageSize);
                boolean isEqual = false;
                for (String id: ids)
                {
                    isEqual = compareIndexDataById(index, id, fieldNames, es17Host, es17Port, es54Host, es54Port);
                    if (!isEqual)
                    {
                        result = false;
                        log.info("Index " + index + " docs with sysId: " + id + " are not equal on host " + es17Host + " and host " + es54Host);
                        break search;
                    }
                }
                left -= ids.length;
                start+=ids.length;
                
                if (left<=0 || start >= total || ids.length==0)
                    break;
            }
        }
        catch (Throwable t)
        {
            log.error(t,t);
            result = false;
        }
        
        return result; 
    }
    
    
    /**
     * Query ES server to check if an index exists or not
     * 
     * @param index     target index name 
     * @param host      ES server hostname/ip
     * @param port      ES port number
     * @return          return "true" if index exists or "false" otherwise
     */
    public static boolean hasIndex(String index, String host, String port)
    {
        String[] arr = getIndexSysids(index, host, port, "0", "1");
        if (arr==null)
        {
            return false;
        }
        else
        {
            return true;
        }
    }
        
    /**
     * Query ES server for ids/sysIds of an index
     *  
     * @param index   target index name
     * @param host    target ES server hostname/ip  
     * @param port    target ES server port
     * @return        sysIds from target index
     */
    public static String[] getIndexSysids(String index, String host, String port)
    {
        return getIndexSysids(index, host, port, null, null);
    }

    /**
     * Query ES server for all ids/sysIds of an index
     *  
     * @param index   index name
     * @param host    target ES server hostname/ip  
     * @param port    target ES server port
     * @param start   result paging start number, default is "0"
     * @param size    paging size, default is "99999"
     * @return        sysIds from target index
     */
    public static String[] getIndexSysids(String index, String host, String port, String start, String end)
    {
        String[] result = null;
        String idStr = queryIndexSysid(index, host, port, start, end);
        try
        {
            if (StringUtils.isNotEmpty(idStr))
            {
                List<String> idList = parseSysidQuery(idStr);
                if (idList!=null)
                {
                    result = idList.toArray(new String[]{});
                }
            }
            else
            {
                log.error("No return value from Sysid search call");
            }
        }
        catch (Throwable t)
        {
            log.error(t,t);
        }
        return result;
    }

    
    /**
     * Query ES server for the document total from an index  
     * 
     * @param index     target index name
     * @param host      ES  server host/ip
     * @param port      ES  server port 
     * @return          document total of an existing index, or null value if error happens 
     */
    public static Integer getIndexDocTotal(String index, String host, String port)
    {
        Integer result = null;
        String idStr = queryIndexSysid(index, host, port, "0", "2");
        try
        {
            if (StringUtils.isNotEmpty(idStr))
            {
                result = parseQueryTotal(idStr);
            }
            else
            {
                log.error("No return value from Sysid search call");
            }
        }
        catch (Throwable t)
        {
            log.error(t,t);
        }
        return result;
    }
    
    
    /**
     * Check if two ES documents (JSON format) have same values on specified columns
     *   
     * @param doc1         first ES document 
     * @param doc2         second document
     * @param fieldNames   index columns included in comparison 
     * @return             "true" if all columns have same values, "false" otherwise or any error happens  
     */
    public static boolean compareIndexJsonDocs(String doc1, String doc2, String[] fieldNames )
    {
     
        log.debug("doc1 --> " + doc1);
        log.debug("doc2 --> " + doc2);
        log.debug("field names: " + fieldNames);
        
        boolean result = true;
        try
        {
            Map<String,Object> docMap1 = parseDocQueryResult(doc1);
            Map<String,Object> docMap2 = parseDocQueryResult(doc2);

            //shoudn't be null
            if (docMap1==null || docMap2==null)
            {
                return false;
            }
            
            if (docMap1.size()!=docMap2.size())
            {
                return false;
            }
            
            // If can't be found from both servers, we think comparison result is true, because neither index has this sysid doc
            if (docMap1!=null && docMap2!=null && docMap1.isEmpty() && docMap2.isEmpty())
            {
                return true;
            }
            
            if (fieldNames==null)
            { 
                if (!docMap1.isEmpty())
                {
                    fieldNames = docMap1.keySet().toArray(new String[]{});
                }
                else
                {
                    fieldNames = docMap2.keySet().toArray(new String[]{});
                }
            }
            
            for (String n : fieldNames)
            {
                String v1 = "" + docMap1.get(n);
                String v2 = "" + docMap2.get(n);
                log.debug("field name: " + n + ", doc1 value: " + v1);
                log.debug("field name: " + n + ", doc2 value: " + v2);

                if (!v1.equals(v2))
                {
                    log.info("Migration field values are different. Field name: " + n +", vlaues v1: " + v1 + ", v2: " + v2);
                    result = false;
                    break;
                }
            }
        }
        catch (Throwable ex)
        {
            log.error(ex,ex);
//            ex.printStackTrace(System.out);
            result = false;
        }
        
        if (!result)
        {
            log.info("Index data comparison failed.");
            log.info("doc1 --> " + doc1);
            log.info("doc2 --> " + doc1);
            log.info("Compare field names: " + fieldNames);
        }
        
        return result;
        
    }
    
    
    /**
     * Parse a JSON string of returned from query
     *  
     * @param doc  ES doc in JSON format
     * @return     parsed doc object. If return value is an empty map, it means ES query returns empty result. If return null, it means error happened at some point.   
     * @throws     Exception if any error happens from parsing
     */
    private static Map parseDocQueryResult(String doc) throws Exception
    {
        Map docObj = null;
        try
        {
           Map obj  = MAPPER.readValue(doc, Map.class);
           Map hits = (Map)obj.get("hits");
           List list = (List)hits.get("hits");
           if (list.isEmpty())
           {
               docObj = new HashMap();
           }
           else
           {
               Map hitObj = (Map)list.get(0);
               docObj = (Map)hitObj.get("_source");
           }
        }
        catch (Exception ex)
        {
            log.error(ex,ex);
            throw new RuntimeException(ex);
        }
        return docObj;
    }

    
    /**
     * Parse sysId query result on an index
     * 
     * @param doc   Query result string      
     * @return      sysIds of target index
     * @throws      Exception if error happens
     */
    private static List<String> parseSysidQuery(String doc) throws Exception
    {
        List<String> ids = new ArrayList<String>();
        try
        {
           Map obj  = MAPPER.readValue(doc, Map.class);
           Map hits = (Map)obj.get("hits");
           Integer total = (Integer)hits.get("total");
           List<Map> list = (List<Map>)hits.get("hits");
           for (Map m : list)
           {
               ids.add((String)m.get("_id"));
           }
        }
        catch (Exception ex)
        {
            log.error(ex,ex);
            throw new RuntimeException(ex);
        }
        return ids;
    }
    
    private static Integer parseQueryTotal(String doc) throws Exception
    {
        Integer total = null;
        try
        {
           Map obj  = MAPPER.readValue(doc, Map.class);
           Map hits = (Map)obj.get("hits");
           total = (Integer)hits.get("total");
        }
        catch (Exception ex)
        {
            log.error(ex,ex);
            throw new RuntimeException(ex);
        }
        return total;
    }

    
    /**
     * Query ES server for a document with provided id/sysId value
     * 
     * @param index    target index name
     * @param id       sysId/id to look for  
     * @param host     ES server hostname/ip
     * @param port     ES server port
     * @return         ES query result string. If return value is null, it means ES query failed. 
     */
    public static String findDocBySysid(String index, String id, String host, String port)
    {
        String result = null;
        String url = "http://" + host + ":" + port + "/" + index + "/_search?pretty";
        String content = "{ \"query\": { \"bool\": { \"must\": [{ \"term\": { \"id\": \"" + id + "\" }  } ] } } }\r\n";
        try
        {
            result = RESTUtils.postQuery(url, "application/json", content, null, null);
        }
        catch (Exception ex)
        {
            ex.printStackTrace(System.out);
            log.error(ex, ex);
        }
        return result;
    }
    
    
    /**
     * Query ES server for sysIds/ids of an index
     * 
     * @param index      target index
     * @param host       ES server hostname/ip
     * @param port       ES port
     * @param start      search result paging start value, default is "0"
     * @param size       paging size, default is "99999" 
     * @return           sysIds/ids of an index
     */
    public static String queryIndexSysid(String index, String host, String port, String start, String size)
    {
        String result = null;
        String url = "http://" + host + ":" + port + "/" + index + "/_search?pretty";
        
        if (StringUtils.isEmpty(start))
        {
            start = "0";
        }
        
        if (StringUtils.isEmpty(size))
        {
            size = "99999";
        }

        String content = "{\"query\":{\"match_all\":{}},\"_source\":[\"sysId\"],\"from\":" + start + ",\"size\":" + size + "}";
        
        try
        {
            result = RESTUtils.postQuery(url, "application/json", content, null, null);
        }
        catch (Exception ex)
        {
            ex.printStackTrace(System.out);
            log.error(ex, ex);
        }
        return result;
    }

    
//    public static void main(String[] args)
//    {
//        String doc ="{\"took\":134,\"timed_out\":false,\"num_reduce_phases\":2,\"_shards\":{\"total\":610,\"successful\":610,\"failed\":0},\"hits\":{\"total\":1,\"max_score\":1.7917595,\"hits\":[{\"_index\":\"worksheet_20170808\",\"_type\":\"worksheet\",\"_id\":\"2bfbd5c1f3ea44e09cd13d4d75aca7c6\",\"_score\":1.7917595,\"_source\":{\"sysId\":\"2bfbd5c1f3ea44e09cd13d4d75aca7c6\",\"sysOrg\":\"8ab282795d528f2f015d529ebff0000c\",\"sysUpdatedBy\":\"admin\",\"sysUpdatedOn\":1502226715597,\"sysUpdatedDt\":1502226715597,\"sysCreatedBy\":\"admin\",\"sysCreatedOn\":1502226715597,\"sysCreatedDt\":1502226715597,\"alertId\":\"\",\"assignedTo\":\"admin\",\"assignedToName\":\"Administrator\",\"condition\":\"GOOD\",\"correlationId\":\"\",\"debug\":null,\"description\":\"\",\"number\":\"WS257-2\",\"reference\":\"\",\"severity\":\"GOOD\",\"summary\":\"Runbook: RSQA.Loop\",\"workNotes\":\"\",\"worksheet\":\"\",\"sirId\":\"\",\"worknotesHtml\":null,\"worksheetXml\":null,\"id\":\"2bfbd5c1f3ea44e09cd13d4d75aca7c6\"}}]}}";
//        String doc1 = findDocBySysid("worksheetalias", "e06d031626594671bda0840d7d77e887", "localhost", "9200");
//        String doc2 = findDocBySysid("worksheetalias", "e06d031626594671bda0840d7d77e887", "localhost", "9200");
//
//        compareIndexJsonDocs(doc, doc, new String[] { "condition", "severity"});
//        boolean isSame = compareIndexJsonDocs(doc, doc, null);
//        System.out.println("1. isSame = " + isSame);
//
//// Compare the whole index        
//        boolean isOk = compareIndexData("worksheetalias",  null, "localhost", "9200", "localhost", "9200");
//        
//// get index doc total        
//        Integer total = getIndexDocTotal("worksheetalias", "localhost", "9200");        
//        
//// compare on alias        
//        isSame = compareIndexDataById("worksheetalias", "e06d031626594671bda0840d7d77e887", null, "localhost", "9200", "localhost", "9200");
//        System.out.println("2. isSame = " + isSame);
//        
//// compare real index 
//        isSame = compareIndexDataById("worksheet_20170608", "e06d031626594671bda0840d7d77e887", null, "localhost", "9200", "localhost", "9200");
//        System.out.println("2. isSame = " + isSame);
//        
////   wrong port
//        isSame = compareIndexDataById("worksheet_20170608", "e06d031626594671bda0840d7d77e887", null, "localhost", "9300", "localhost", "9200");
//        System.out.println("2. isSame = " + isSame);
//
//// query on alias        
//        String[] wsIds = getIndexSysids("worksheetalias", "localhost", "9200", null, null);
//        System.out.println(wsIds);
//       
//// index exists        
//        boolean exists = hasIndex("worksheet_20170620", "localhost", "9200");
//        
//// index doesn't exist        
//        exists = hasIndex("worksheet_20170920", "localhost", "9200");
//        
//// index has docs        
//        wsIds = getIndexSysids("worksheet_20170620", "localhost", "9200");
//
//// wrong index name        
//        wsIds = getIndexSysids("worksheetalias1", "localhost", "9200", null, null);
//        System.out.println(wsIds);
//
//// empty index        
//        wsIds = getIndexSysids("executionsummary_20170524", "localhost", "9200", null, null);
//        System.out.println(wsIds);
//        
//    }
}
