/******************************************************************************
* (C) Copyright 2014
*
* Resolve Systems, LLC
*
* All rights reserved
* This software is distributed under license and may not be used, copied,
* modified, or distributed without the express written permission of
* Resolve Systems, LLC.
*
******************************************************************************/

package com.resolve.update;

import org.apache.log4j.Logger;

import com.resolve.update.Crypt;

public class CryptUtils
{
    public static Logger log; 
    
    // default prefix
    public static String prefixLocal = "ENC:";
    public static String prefix128Local = "ENC1:";

    // crypto
    final static String CRYPTKEY = "this is a fairly long phrase - no problems only solutions";
    static Crypt crypt = null;
    static Crypt cryptAES = null;
    static String encData;
    
    public static boolean isEncrypted(String value)
    {
        boolean result = false;
        
        if (value != null && value.startsWith(prefixLocal))
        {
            result = true;
        }
        else if (value != null && value.startsWith(prefix128Local))
        {
            result = true;
        }
        
        return result;
    } // isEncrypted

    public static String decryptUTF8(String value) throws Exception
    {
        String decryptString = value;

        if (value != null && value.startsWith(prefixLocal))
        {
            decryptString = decryptUTF8(value, prefixLocal);
        }
        else if (value != null && value.startsWith(prefix128Local))
        {
            decryptString = decryptAES128UTF8(value, prefix128Local);
        }

        return decryptString;
    } // decrypt

    public static String decrypt(String value) throws Exception
    {
        String decryptString = value;

        if (value != null && value.startsWith(prefixLocal))
        {
            decryptString = decrypt(value, prefixLocal);
        }
        else if (value != null && value.startsWith(prefix128Local))
        {
            decryptString = decryptAES128(value, prefix128Local);
        }

        return decryptString;
    } // decrypt

    public static String decryptAES128(String value, String prefix) throws Exception
    {
        String result = value;

        if (cryptAES == null)
        {
            cryptAES = new Crypt(Crypt.ENCRYPTION_SCHEME_AES128, CRYPTKEY);
        }

        // remove prefix if defined
        if (prefix != null && value != null && value.startsWith(prefix) && prefix.equals(prefix128Local))
        {
            value = value.substring(prefix.length());
            for (int i = 0; i < 3; i++)
            {
                try
                {
                    result = cryptAES.decryptAES128(value);
                    break;
                }
                catch (Exception e)
                {
                    log.error("Failed to Decrypt Value", e);
                    try
                    {
                        if (i < 2)
                        {
                            Thread.sleep(2000);
                        }
                    }
                    catch (InterruptedException ie)
                    {
                        log.error("Interrupted Exception", ie);
                    }
                }
            }
        }

        return result;

    } // decrypt

    public static String decryptAES128UTF8(String value, String prefix) throws Exception
    {
        String result = value;

        if (cryptAES == null)
        {
            cryptAES = new Crypt(Crypt.ENCRYPTION_SCHEME_AES128, CRYPTKEY);
        }

        // remove prefix if defined
        if (prefix != null && value != null && value.startsWith(prefix) && prefix.equals(prefix128Local))
        {
            value = value.substring(prefix.length());
            for (int i = 0; i < 3; i++)
            {
                try
                {
                    result = cryptAES.decryptAES128UTF8(value);
                    break;
                }
                catch (Exception e)
                {
                    log.error("Failed to Decrypt Value", e);
                    try
                    {
                        if (i < 2)
                        {
                            Thread.sleep(2000);
                        }
                    }
                    catch (InterruptedException ie)
                    {
                        log.error("Interrupted Exception", ie);
                    }
                }
            }
        }

        return result;

    } // decrypt

    public static String decryptUTF8(String value, String prefix) throws Exception
    {
        String result = value;

        if (crypt == null)
        {
            crypt = new Crypt(Crypt.ENCRYPTION_SCHEME_DESEDE, CRYPTKEY);
        }

        // remove prefix if defined
        if (prefix != null && value != null && value.startsWith(prefix) && prefix != null && prefix.equals(prefixLocal))
        {
            value = value.substring(prefix.length());
            for (int i = 0; i < 3; i++)
            {
                try
                {
                    result = crypt.decryptUTF8(value);
                    break;
                }
                catch (Exception e)
                {
                    log.error("Failed to Decrypt Value", e);
                    try
                    {
                        if (i < 2)
                        {
                            Thread.sleep(2000);
                        }
                    }
                    catch (InterruptedException ie)
                    {
                        log.error("Interrupted Exception", ie);
                    }
                }
            }
        }

        return result;

    } // decrypt

    public static String decrypt(String value, String prefix) throws Exception
    {
        String result = value;

        if (crypt == null)
        {
            crypt = new Crypt(Crypt.ENCRYPTION_SCHEME_DESEDE, CRYPTKEY);
        }

        // remove prefix if defined
        if (prefix != null && value != null && value.startsWith(prefix) && prefix != null && prefix.equals(prefixLocal))
        {
            value = value.substring(prefix.length());
            for (int i = 0; i < 3; i++)
            {
                try
                {
                    result = crypt.decrypt(value);
                    break;
                }
                catch (Exception e)
                {
                    log.error("Failed to Decrypt Value", e);
                    try
                    {
                        if (i < 2)
                        {
                            Thread.sleep(2000);
                        }
                    }
                    catch (InterruptedException ie)
                    {
                        log.error("Interrupted Exception", ie);
                    }
                }
            }
        }

        return result;

    } // decrypt
    
    public static void setENCData(String encData) throws Exception
    {
        Crypt.setENDData(encData);
    } // setENCData
    
    public static void setLogger(Logger logger)
    {
        log = logger;
    } // setLogger
} // CryptUtils