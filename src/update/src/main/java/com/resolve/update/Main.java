/******************************************************************************
 * (C) Copyright 2014
 *
 * Resolve Systems, LLC
 *
 * All rights reserved
 * This software is distributed under license and may not be used, copied,
 * modified, or distributed without the express written permission of
 * Resolve Systems, LLC.
 *
 ******************************************************************************/

package com.resolve.update;

import java.io.BufferedInputStream;
import java.io.BufferedReader;
import java.io.ByteArrayInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.lang.reflect.Field;
import java.nio.charset.Charset;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import java.sql.SQLException;
import java.sql.Statement;
import java.sql.Timestamp;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Properties;
import java.util.Scanner;
import java.util.UUID;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.apache.commons.io.FileUtils;
import org.apache.commons.lang3.StringUtils;
import org.apache.log4j.Logger;
import org.apache.log4j.PropertyConfigurator;
import org.dom4j.Attribute;
import org.dom4j.Document;
import org.dom4j.Element;
import org.dom4j.io.SAXReader;

import groovy.lang.Binding;
import ml.options.Options;
import ml.options.Options.Multiplicity;
import ml.options.Options.Prefix;
import ml.options.Options.Separator;

public class Main
{
    public final static String MYSQLDRIVER = "org.mariadb.jdbc.Driver"; // We are Using the MariaDB driver for MySQL, do not change
    public final static String ORACLEDRIVER = "oracle.jdbc.OracleDriver";
    final static String os = System.getProperty("os.name");
    public final static String onetimelog = "file/update_onetime.log";

    private static String filePath = "file/";
    private static String rsmgmtPath = "rsmgmt/service/";
    private static String rsconsolePath = "rsconsole/service/";
    private static String installPath = "";

    public static Logger log;

    public static Main main;

    static Calendar calendar = Calendar.getInstance();
    public static Connection connection;
    public static Binding binding = new Binding();

    public static String consoleLoginUser = "admin";
    public static String consoleLoginPass = "resolve";

    public static String blueprintFileName = "rsmgmt/config/blueprint.properties";
    public static Properties blueprintProperties = new Properties();
    public static String dist = "";
    public static String updateXmlFilename;
    public static String updateZipFilename;
    public static String type;
    public static String buildVersion;
    public static String currentBuildVersion;

    static List<String> updateArgs = new ArrayList<String>();

    public static boolean blueprintRSRemote = false;
    public static boolean primaryRSView = false;
    public static List<String> rsremoteInstances = null;

    public static void main(String[] args) throws Exception
    {
        main = new Main();

        // set crypt data
        String encData = CryptUtils.decrypt(Crypt.getEncDataLocal(), CryptUtils.prefixLocal);
        CryptUtils.setENCData(encData);

        updateArgs.add("update"); // compatibility with older scripts

        main.initOptions(args);

        main.initLog();

        main.initBlueprint();

        main.initExtraOptions();

        main.connectDB();

        main.initBindings();

        main.executeUpdate();
    }

    /**
     * Returns if the given file exists
     * 
     * @author: James Dalby, james.dalby@resolve.systems
     * @param file
     *            the file to validate
     * @return
     *         boolean value of whether it exists
     */
    private boolean runValidation(File file)
    {
        return file.exists();
    }// runValidation

    /**
     * Determines if the given OS command's return value matches the given compareValue
     * 
     * @author Justin Geiser justin.geiser@resolvesys.com
     * @param osCmd
     *            The command to execute
     * @param compareValue
     *            the expected return value
     * @param caseSensitive
     *            is the expected return value case sensitive?
     * @param contains
     *            if true check for just a contains instead of equals
     * @param notValidation
     *            if true then the check will run a not equals or not contains
     * @return
     */
    private boolean runValidation(String osCmd, String compareValue, boolean caseSensitive, boolean contains, boolean notValidation)
    {
        // Validate OS Cmd return value matches compareValue
        // Can use Execute.java as basis to copy over

        // Execute the command
        boolean result = false;
        try
        {
            String cmdOutput = executeOS(osCmd).trim();

            // return if compareValue parameter == process outputString
            if (contains)
            {
                if (caseSensitive)
                {
                    result = cmdOutput.contains(compareValue);
                }
                else
                {
                    result = cmdOutput.toLowerCase().contains(compareValue.toLowerCase());
                }
            }
            else
            {
	            if (caseSensitive)
	            {
	                result = cmdOutput.equals(compareValue);
	            }
	            else
	            {
	                result = cmdOutput.toString().equalsIgnoreCase(compareValue);
	            }
            }
            if (notValidation)
            {
                result = !result;
            }
        }
        catch (Exception e)
        {
            Main.println("There was an error executing the following OS command: " + osCmd);
            log.error("There was an error executing the following OS command: " + osCmd, e);
            result = false;
        }
        return result;
    }// runValidation

    /**
     * Determines if the given OS command's return value matches the given compareValue
     * 
     * @author: James Dalby, james.dalby@resolve.systems
     * @param msgFile
     *            the message file. Contains the content which will be displayed to the user
     * @param agree
     *            the agree message
     * @param decline
     *            is the expected return value case sensitive?
     * @return
     *         boolean value of whether the command return value and the compareValue match
     */
    private boolean getResponse(InputStream msgStream, boolean printAccept, String agree, String decline)
    {
        StringBuffer msgContent = new StringBuffer();
        boolean result = false;
        try
        {
            //log.info("Display Acceptance File " + msgFile);
            log.trace("Cont: " + agree + ", Stop: " + decline);
            // read the msgFile content into a StringBuffer
            BufferedReader br = new BufferedReader(new InputStreamReader(msgStream));
            String line;
            while ((line = br.readLine()) != null)
            {
                msgContent.append(line);
            }
            br.close();

            // set up an input loop
            boolean repeatCommand = true;
            Scanner s = new Scanner(System.in);
            String userInput;

            // display msgFile content
            Main.println(msgContent.toString());
            do
            {
                // get user input
                if (printAccept)
                {
	                Main.print("Do you accept the above? ");
                }
                Main.print("(\"" + agree + "\", \"" + decline + "\"): ");
                userInput = s.nextLine();

                if (userInput.equalsIgnoreCase(agree))
                {
                    // if input == agree
                    repeatCommand = false;
                    result = true;
                }
                else if (userInput.equalsIgnoreCase(decline))
                {
                    // if input == decline
                    repeatCommand = false;
                    result = false;
                }
                else
                {
                    Main.println("Acceptable input is either \"" + agree + "\" or \"" + decline + "\"");
                }
                // otherwise continue looping
            } while (repeatCommand);

        }
        catch (IOException ioe)
        {
            Main.println("There was an error opening above the following file or content");
            log.error("There was an error opening above the following file or content", ioe);
            result = false;
        }
        log.info("User Acceptance: " + result);
        return result;
    }

    /**
     * Executes the given OS Command
     * 
     * @param osCmd
     *            The given command
     * @return
     *         The return value of the executed command
     */
    public String executeOS(String osCmd)
    {
        return executeOS(osCmd, false);
    }

    /**
     * Exectues the given OS Command
     * 
     * @author: James Dalby, james.dalby@resolve.systems
     * @param osCmd
     *            The given command
     * @param output
     *            Whether to output the command results to stdout
     * @return
     *         The return value of the executed command
     */
    private String executeOS(String osCmd, boolean output)
    {
        String commandOutput = null;

        Process process = null;
        BufferedReader outputReader = null;
        try
        {
            // check if windows, if so prepend 'cmd c/'
            String finalCommand = osCmd;
            if (os.startsWith("Windows") && !osCmd.startsWith("cmd c/"))
            {
                finalCommand = "cmd /c " + osCmd;
            }
            log.info("Executing Command: " + finalCommand);

            // start a process and execute the command
            ProcessBuilder pb = new ProcessBuilder(prepareCommand(finalCommand));
            pb.redirectErrorStream(true);
            pb.directory(new File(dist).getCanonicalFile());
            process = pb.start();
            process.getOutputStream().close();

            if (log.isTraceEnabled())
            {
                try
                {
                    Field f = process.getClass().getDeclaredField("pid");
                    f.setAccessible(true);
                    int pid = f.getInt(process);
                    log.trace("PID: " + pid);
                }
                catch (Exception e)
                {
                    log.error("Failed to pull pid", e);
                }
            }

            // read and return the process's return message
            InputStream commandOutputStream = process.getInputStream();
            outputReader = new BufferedReader(new InputStreamReader(commandOutputStream));
            StringBuffer outputString = new StringBuffer();
            boolean blankLine = false;
            String line;
            while ((line = outputReader.readLine()) != null)
            {
                if (output)
                {
                    System.out.println(line);
                }
                if (line.length() > 0)
                {
                    outputString.append(line + "\n");
                    blankLine = false;
                }
                else
                {
                    if (blankLine)
                    {
                        outputString.append("\n");
                        blankLine = false;
                    }
                    else
                    {
                        blankLine = true;
                    }
                }
            }

            int returnCode = process.waitFor();
            log.info("Process Return Code: " + returnCode);

            commandOutputStream.close();
            commandOutput = outputString.toString();
            log.info("Command Output: " + commandOutput);

            if (returnCode != 0)
            {
                throw new RuntimeException("OS Command " + osCmd + " Has Returned Error Code " + returnCode);
            }
        }
        catch (InterruptedException ie)
        {
            Main.println("The following OS Command was interrupted before it could finish: " + osCmd);
            log.error("The following OS Command was interrupted before it could finish: " + osCmd, ie);
        }
        catch (IOException ioe)
        {
            Main.println("There was an error executing the following runtime command: " + osCmd);
            log.error("There was an error executing the following runtime command: " + osCmd, ioe);
            commandOutput = "ERROR";
        }
        finally
        {
            try
            {
                if (process != null)
                {
                    process.getInputStream().close();
                    process.getOutputStream().close();
                    process.getErrorStream().close();
                    process.destroy();
                }
                if (outputReader != null)
                {
                    outputReader.close();
                }

            }
            catch (Exception e)
            {
                log.warn("Failed to Close OS streams", e);
            }
        }
        return commandOutput;
    }

    /**
     * Exectues the script within a given file
     * 
     * @author: James Dalby, james.dalby@resolve.systems
     * @param fileName
     *            The given command
     * @param bindings
     *            A map of bindings
     * @param options
     *            A List of command options
     * @return
     *         A boolean value that denotes whether the execution was successful
     * @throw FileNotFoundException
     */
    private boolean executeScript(String fileName, Map<String, String> bindings, List<String> options) throws FileNotFoundException
    {
        boolean result;
        File file;
        boolean isRSMGMTFile = false;
        boolean isRSCONSOLEFile = false;

        // If start with [A-Za-z]:\\ or / use absolute path
        String absolutePathPattern1 = "^[A-Za-z]:\\\\.*";
        String absolutePathPattern2 = "^/.*";

        log.info("Attempting to execute script file: " + fileName);

        if (fileName.matches(absolutePathPattern1) || fileName.matches(absolutePathPattern2))
        {
            file = new File(fileName);
            if (!runValidation(file))
            {
                throw new FileNotFoundException("Cannot Find File: " + fileName);
            }
        }

        // if path starts with file, then check file/
        if (fileName.matches("^" + filePath + ".*"))
        {
            file = new File(filePath, fileName.substring(filePath.length()));
            if (!runValidation(file))
            {
                throw new FileNotFoundException("Cannot Find File: " + fileName);
            }
        }
        else
        {
            // check for file in rsmgmt/service
            file = new File(rsmgmtPath, fileName);
            if (runValidation(file))
            {
                isRSMGMTFile = true;
            }
            else
            {
                // add .groovy and check again
                file = new File(rsmgmtPath, fileName + ".groovy");
                if (runValidation(file))
                {
                    isRSMGMTFile = true;
                }
                else
                {
                    // check for file in rsconsole/service
                    file = new File(rsconsolePath, fileName);
                    if (runValidation(file))
                    {
                        isRSCONSOLEFile = true;
                    }
                    else
                    {
                        // add .groovy and check again
                        file = new File(rsconsolePath, fileName + ".groovy");
                        if (runValidation(file))
                        {
                            isRSCONSOLEFile = true;
                        }
                        else
                        {
                            // if it still can't be found then check relative to install directory
                            file = new File(installPath, fileName);
                            if (!runValidation(file))
                            {
                                // throw FileNotFoundException if not found
                                throw new FileNotFoundException("Cannot Find File: " + fileName);
                            }
                        }
                    }
                }
            }
        }

        if (isRSMGMTFile)
        {
            log.info("Executing RSMgmt File");
            // If rsmgmt file just call rsmgmt/bin/run.sh -f fileName [options] (how to get return value? is return value needed?)
            StringBuffer commandString = new StringBuffer();
            if (os.startsWith("Windows"))
            {
                commandString.append("rsmgmt\\bin\\run.bat -f ");
            }
            else
            {
                commandString.append("rsmgmt/bin/run.sh -f ");
            }

            commandString.append(fileName);

            for (String option : options)
            {
                commandString.append(" " + option);
            }

            // Execute command
            String returnValue = executeOS(commandString.toString());

            // Future Update - set to true for now for compatibility reasons
            // result = Boolean.parseBoolean(returnValue);
            result = true;
        }
        else if (isRSCONSOLEFile)
        {
            String command = "rsconsole/bin/run";
            if (os.contains("Win"))
            {
                command = command.replaceAll("/", "\\\\") + ".bat";
            }
            else
            {
                command += ".sh";
            }
            command += " -u " + Main.consoleLoginUser + " -p " + Main.consoleLoginPass + " -f " + fileName;
            String output = executeOS(command);
            if ("ERROR".equals(output))
            {
                result = false;
            }
            else
            {
                result = true;
            }
        }
        else
        {
            log.info("Executing script file");
            // else read file to String then call executeGroovy(content, bindings)
            try
            {
                String content = FileUtils.readFileToString(file);

                // execute groovy script
                result = executeGroovy(content.toString(), bindings);
            }
            catch (FileNotFoundException fnfe)
            {
                throw fnfe;
            }
            catch (IOException ioe)
            {
                Main.println("There was a problem reading from the file: " + fileName);
                log.error("There was a problem reading from the file: " + fileName, ioe);
                result = false;
            }
        }
        // Change classpath in update.sh/bat to just use RSConsole's?
        // Create new directory in gseservice/client for update related scripts that are also useful outside of update (e.g. rsmq is running)
        // then add these to the update zip automatically?

        return result;
    }// executeScript

    /**
     * Exectues groovy script
     * 
     * @author: James Dalby
     * @param content
     *            The groovy script
     * @param bindings
     *            A map of bindings
     * @return
     *         A boolean value that denotes whether the execution was successful
     * @throw FileNotFoundException
     */
    private boolean executeGroovy(String content, Map<String, String> bindings) throws FileNotFoundException
    {
        boolean result = true;
        if (content.contains("MAIN.esb"))
        {
            log.warn("ESB in script, sending to RSConsole");
            // if script contains ESB write temporary file to rsconsole/service/mgmt folder
            DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd_HH-mm-ss");
            Date date = new Date();

            String tempFileName = "temp" + dateFormat.format(date);
            File file;
            try
            {
                file = new File(this.rsconsolePath + "mgmt/" + tempFileName + ".groovy");
                content = getReplacementValue(content);
                file.createNewFile();
                FileUtils.writeStringToFile(file, content);

                // then run executeOS(rsconsole/bin/run.sh(bat) -u admin -p resolve -f mgmt/<tmp file name>
                String command = "rsconsole/bin/run";
                if (os.contains("Win"))
                {
                    command = command.replaceAll("/", "\\\\") + ".bat";
                }
                else
                {
                    command += ".sh";
                }
                command += " -u " + Main.consoleLoginUser + " -p " + Main.consoleLoginPass + " -f mgmt/" + tempFileName;
                String output = executeOS(command);
                if ("ERROR".equals(output))
                {
                    result = false;
                }
                else
                {
                    result = true;
                }

                // after completion delete file
                FileUtils.deleteQuietly(file);

            }
            catch (FileNotFoundException fnfe)
            {
                throw fnfe;
            }
            catch (IOException ioe)
            {
                Main.println("An error occured while trying to execute a Groovy Script");
                log.error("An error occured while trying to build a temp file in " + this.rsconsolePath, ioe);
            }
        }
        else
        {
            // else use executeScript() for basis of execution

            //Main.println("Attempting to Execute Script... ");
            log.info("Attempting to Execute Script...");

            Object scriptResult = null;
            try
            {
                // add bindings to base bindings
                for (Map.Entry<String, String> entry : bindings.entrySet())
                {
                    binding.setVariable(entry.getKey(), entry.getValue());
                }

                // execute the script
                scriptResult = GroovyScript.execute(content, "", binding);

                log.trace("Groovy Script Results: " + scriptResult);

                if (scriptResult == null)
                {
                    // return false if result was unsuccessful
                    result = false;
                }
                else
                {
                    if (scriptResult instanceof Boolean)
                    {
                        // return script result if boolean,
                        result = (Boolean) scriptResult;
                    }
                    else
                    {
                        // if not just true if it completes
                        result = true;
                    }
                }

            }
            catch (Throwable t)
            {
                // catch and log/print exception then return false
                log.error("Failed to Execute Script", t);
                Main.println("Failed to Execute Script: " + t.getMessage());
                result = false;
            }

        }
        return result;
    }// executeGroovy

    /*
     * Run update sql script as prepared statement
     * arguments map should be (pos, string)
     * return the number of rows updated
     */
    private int updateSQL(String sql, Map<String, String> arguments)
    {
        int updateResult = -1;
        if (connection != null)
        {
            try
            {
	            PreparedStatement statement = prepareSQL(sql, arguments);
	            updateResult = statement.executeUpdate();
            }
            catch (SQLException sqle)
            {
                log.error("Failed to Execute SQL: " + sql, sqle);
            }
        }
        else
        {
            log.warn("No Connection Available, Skipping SQL");
        }
        return updateResult;
    }

    private int updateDirectSQL(String sql) throws SQLException
    {
        int updateDirectResult = -1;
        if (connection != null)
        {
            Statement statement = connection.createStatement();
            updateDirectResult = statement.executeUpdate(sql);
            
            statement.close();
        }
        else
        {
            log.warn("No Connection Available, Skipping Direct SQL");
        }
        return updateDirectResult;
    }
    
    /*
     * Run select sql script as prepared statement
     * arguments map should be (pos, string)
     * return the results of the query
     */
    private String selectSQL(String sql, Map<String, String> arguments) throws SQLException
    {
        StringBuffer result = new StringBuffer();
        if (connection != null)
        {
            PreparedStatement statement = prepareSQL(sql, arguments);
            ResultSet selectResult = statement.executeQuery();
            ResultSetMetaData metaData = selectResult.getMetaData();
            int numColumns = metaData.getColumnCount();
            String[] columnNames = new String[numColumns + 1];
            for (int i = 1; i <= numColumns; i++)
            {
                columnNames[i] = metaData.getColumnName(i);
            }
            int resultCount = 1;
            while (selectResult.next())
            {
                result.append(resultCount++).append(": ");
                for (int i = 1; i <= numColumns; i++)
                {
                    result.append(columnNames[i] + "=").append(selectResult.getString(i));
                }
                log.info("Update/Upgrade Select Statement result: " + selectResult);
            }
        }
        else
        {
            log.warn("No Connection Available, Skipping SQL");
        }
        return result.toString();
    }

    /*
     * create prepared statement based on sql statement and arguments map
     * arguments map should be (pos, string)
     */
    private PreparedStatement prepareSQL(String sql, Map<String, String> arguments) throws SQLException
    {
        PreparedStatement statement = connection.prepareStatement(sql);
        Timestamp timestamp = new Timestamp((new Date()).getTime() /*- (calendar.get(Calendar.ZONE_OFFSET) + calendar.get(Calendar.DST_OFFSET))*/);
        for (String pos : arguments.keySet())
        {
            if (!pos.matches("\\d+"))
            {
                log.warn("Potential SQL Error, Prepared statement position is not set to a number");
            }
            else
            {
                String value = arguments.get(pos);
                if (value.equals("~TIMESTAMP~"))
                {
                    statement.setTimestamp(Integer.parseInt(pos), timestamp);
                }
                else if (value.equals("~SYSID~"))
                {
                    String sysId = new StringBuffer(UUID.randomUUID().toString()).delete(23, 24).delete(18, 19).delete(13, 14).delete(8, 9).toString();
                    statement.setString(Integer.parseInt(pos), sysId);
                }
                else
                {
                    statement.setString(Integer.parseInt(pos), value);
                }
            }
        }
        return statement;
    }

    /*
     * from xml read in at start, go in order
     * 
     * First should be ARGUMENTS, copy into global arguments if needed
     * 
     * For All, read options forceskip, failmsg, failout, ifbool, if, skipif, and onetimeid
     * Check if command, if argument listed was not passed in skip command
     * Check skipif command, if argument was passed in skip command
     * Check ifbool command, run replace if ${ detected then send through bool check (like precondition in runbook), if false skip command
     * Check onetimeid, if present read file/update_onetime.log
     * if file doesn't exist or does not contain onetime value continue, after sucessful execute write value into this file (create if necessary)
     * TODO: make sure this file is excempt from update_cleanup
     * Run Command (OS, SCRIPT, Etc.) in try/catch for FileNotFound, and general Exception
     * If command threw Exception, or return false, output failmsg (or generic if not present) and log
     * check failout, if false keep going after logging message
     * else check forceskip, if true or not present, and --force option was set keep going otherwise stop process
     * 
     * 
     * Validation - read options file, or os and value (compareValue), and casesensitive
     * if File call runValidation(File)
     * if os and value read option casesensitive, if true change below call to runValidation(String, String, true)
     * if os and value check both for ${ and getReplacementValue if true, then call runValidation(String, String, false)
     * 
     * OS - read options cmd, Unix, Linux, Solaris, Windows
     * if cmd is there just run executeOS(String)
     * if other check os type first, then run matching executeOS(String) value
     * 
     * START - read options component and options
     * run executeOS(bin/run.sh/bat component [Options])
     * special case RSRemote, read rsremote.instance<N> values and run start for all listed
     * STOP - read option component
     * run executeOS(bin/stop.sh/bat component [Options])
     * special case RSRemote, read rsremote.instance<N> values and run stop for all listed
     * 
     * Script - read options file, content, options, arg (key, value, ifbool)
     * use getReplacementValue for any options
     * Read any options to List<String>
     * use getReplacment for any arg key, value, and ifbool, add to map if ifbool returns true, or is not present
     * if file run executeScript
     * if content run executeGroovy with just args
     * 
     * SQL - read options select, update, and arg (pos, value)
     * use getReplacementValue for arg values
     * anything in [] is a local call - other options?
     * prepare statement and bind args if present
     * if select log/output result
     * else if update execute and return true if rows affected > 0
     */
    private void executeUpdate()
    {
        try
        {
            long start = System.currentTimeMillis();
            File updateXmlFile = new File(updateXmlFilename);
            boolean result = false;
            boolean lastFailoutTrueResult = true;
            if (updateXmlFile.exists())
            {
                SAXReader reader = new SAXReader();
                reader.setFeature("http://apache.org/xml/features/disallow-doctype-decl", true);
                reader.setFeature("http://xml.org/sax/features/external-general-entities", false);
                reader.setFeature("http://xml.org/sax/features/external-parameter-entities", false);
                Document document = reader.read(updateXmlFile);

                Element root = document.getRootElement();
                List<Element> elements = root.elements();
                for (Element child : elements)
                {
                    log.info("Processing " + child.getName() + " command");
                    // Universal attributes
                    boolean forceskip = true;
                    boolean failout = true;
                    boolean echo = true;
                    String ifbool = null;
                    String ifargument = null;
                    String skipif = null;
                    String onetimeid = null;
                    String failmsg = "WARNING!!! Unexpected Error Durring Update/Upgrade Process. See update.log for more details";
                    String curVersion = null;

                    // task specific attributes
                    String file = null;
                    String compareValue = null;
                    boolean contains = false;
                    boolean notValidation = false;
                    String cmd = null;
                    boolean casesensitive = false;
                    String component = null;
                    String select = null;
                    String update = null;
                    String updateDirect = null;
                    String agree = null;
                    String decline = null;
                    
                    String continueMessage = null;

                    List<Attribute> attributes = child.attributes();
                    for (Attribute attribute : attributes)
                    {
                        log.debug("Attribute: " + attribute.getName() + "=" + attribute.getValue());
                        try
                        {
                            UpdateAttributes attributeType = UpdateAttributes.valueOf(attribute.getName().toUpperCase());
                            switch (attributeType)
                            {
                                case FORCESKIP:
                                    forceskip = "true".equalsIgnoreCase(attribute.getValue());
                                    break;
                                case FAILOUT:
                                    failout = "true".equalsIgnoreCase(attribute.getValue());
                                    break;
                                case CASESENSITIVE:
                                    casesensitive = "true".equalsIgnoreCase(attribute.getValue());
                                    break;
                                case ECHO:
                                    echo = "true".equalsIgnoreCase(attribute.getValue());
                                    break;
                                case IFBOOL:
                                    ifbool = attribute.getValue();
                                    break;
                                case CURRENTVERSION:
                                    curVersion = attribute.getValue();
                                    break;
                                case IF:
                                    ifargument = attribute.getValue();
                                    break;
                                case SKIPIF:
                                    skipif = attribute.getValue();
                                    break;
                                case ONETIMEID:
                                    onetimeid = attribute.getValue();
                                    break;
                                case FAILMSG:
                                    failmsg = getReplacementValue(attribute.getValue());
                                    break;
                                case FILE:
                                    file = attribute.getValue();
                                    break;
                                case NOTCONTAINS:
                                    notValidation = true;
                                case CONTAINS:
                                    contains = true;
                                case VALUE:
                                    compareValue = attribute.getValue();
                                    break;
                                case CMD:
                                    cmd = attribute.getValue();
                                    break;
                                case COMPONENT:
                                    component = attribute.getValue();
                                    break;
                                case SELECT:
                                    select = attribute.getValue();
                                    break;
                                case UPDATE:
                                    update = attribute.getValue();
                                    break;
                                case UPDATEDIRECT:
                                    updateDirect = attribute.getValue();
                                    break;
                                case CONT:
                                    agree = attribute.getValue();
                                    break;
                                case EXIT:
                                    decline = attribute.getValue();
                                    break;
                                case CONTINUEMESSAGE:
                                	continueMessage = attribute.getValue();
                                default:
                                    break;
                            }
                        }
                        catch (IllegalArgumentException iae)
                        {
                            log.warn("Invalid Argument Name: " + attribute.getName(), iae);
                        }
                    }
                    if (ifargument != null && !updateArgs.contains(ifargument))
                    {
                        log.info("Argument " + ifargument + " not passed in, skipping command");
                        continue;
                    }
                    if (skipif != null)
                    {
                        String[] skipifAry = skipif.split(";");
                        boolean skip = false;
                        for (int i = 0; i < skipifAry.length; i++)
                        {
                            if (updateArgs.contains(skipifAry[i]))
                            {
                                log.info("Argument " + skipif + " passed in, skipping command");
                                skip = true;
                                break;
                            }
                        }
                        if (skip)
                        {
                            continue;
                        }
                    }
                    if (curVersion != null)
                    {
                        log.info("Validating Current Version is in " + curVersion);
                        String[] curVersions = curVersion.split(",");
                        boolean validVersion = false;
                        for (String version : curVersions)
                        {
                            if (currentBuildVersion.startsWith(version))
                            {
                                log.info("Valid Current Version Found: " + version);
                                validVersion = true;
                                break;
                            }
                        }
                        if (!validVersion)
                        {
                            log.warn("Current Version " + currentBuildVersion + " is not valid for this task, skipping");
                            continue;
                        }
                    }
                    if (ifbool != null)
                    {
                        Boolean value = main.checkIfBool(ifbool);
                        if (!value)
                        {
                            log.warn("Validation Check " + ifbool + " returned false, skipping command");
                            continue;
                        }
                    }
                    if (onetimeid != null)
                    {
                        File onetimeFile = new File(onetimelog);
                        log.debug("onetime file exists: " + onetimeFile.exists());
                        if (onetimeFile.exists())
                        {
                            String onetimeStr = FileUtils.readFileToString(onetimeFile);
                            if (onetimeStr.contains(onetimeid))
                            {
                                log.warn("One Time Action " + onetimeid + " Appears to have Already run on this system, skipping command");
                                continue;
                            }
                        }
                    }

                    Map<String, String> arguments = new HashMap<String, String>();
                    List<String> cmdOptions = new ArrayList<String>();
                    List<Element> options = child.elements();
                    String content = null;
                    if (options != null && options.size() > 0)
                    {
                        for (Element option : options)
                        {
                            log.debug("Option: " + option.getName());
                            if (StringUtils.isNotBlank(option.getText()))
                            {
                                log.debug("Option Text: " + option.getText());
                            }
                            try
                            {
                                UpdateOptions optionType = UpdateOptions.valueOf(option.getName().toUpperCase());
                                log.debug("optionType is " + optionType);
                                switch (optionType)
                                {
                                    case ARG:
                                        // arg (key, value, ifbool, pos, value),
                                        String ifArg = option.attributeValue("ifbool");
                                        Boolean ifCheck = true;
                                        if (StringUtils.isNotBlank(ifArg))
                                        {
                                            ifCheck = checkIfBool(ifArg);
                                        }
                                        if (ifCheck)
                                        {
                                            String bindkey = option.attributeValue("bindkey");
                                            // Assume arg for SCRIPT
                                            if (bindkey != null)
                                            {
                                                String value = option.attributeValue("value");
                                                value = getReplacementValue(value);
                                                arguments.put(bindkey, value);
                                                log.debug("Adding binding " + bindkey + "=" + value);
                                            }
                                            // Assume arg for SQL
                                            else
                                            {
                                                String pos = option.attributeValue("pos");
                                                String value = option.attributeValue("value");
                                                value = getReplacementValue(value);
                                                arguments.put(pos, value);
                                                log.debug("Setting pos " + pos + " to " + value);
                                            }
                                        }
                                        else
                                        {
                                            String key = option.attributeValue("bindkey") != null ? option.attributeValue("bindkey") : option.attributeValue("pos");
                                            String value = option.attributeValue("value");
                                            log.info("ifbool on arg (" + ifArg + ") failed, not adding to bindings: " + key + "=" + value);
                                        }
                                        break;
                                    case UNIX:
                                        if (os.contains("Linux") || os.contains("SunOS"))
                                        {
                                            cmd = option.getText();
                                        }
                                        break;
                                    case LINUX:
                                        if (os.contains("Linux"))
                                        {
                                            cmd = option.getText();
                                        }
                                        break;
                                    case SOLARIS:
                                        if (os.contains("SunOS"))
                                        {
                                            cmd = option.getText();
                                        }
                                        break;
                                    case WINDOWS:
                                        if (os.contains("Win"))
                                        {
                                            cmd = option.getText();
                                        }
                                        break;
                                    case OPTION:
                                        String cmdOption = option.getText();
                                        cmdOption = getReplacementValue(cmdOption);
                                        cmdOptions.add(cmdOption);
                                        break;
                                    case UPDATE:
                                        update = option.getText();
                                        break;
                                    case CONTENT:
                                        content = option.getText();
                                    default:
                                }
                            }
                            catch (IllegalArgumentException iae)
                            {
                                log.warn("Invalid Option Name: " + option.getName(), iae);
                            }
                        }
                    }
                    result = false;
                    try
                    {
                        UpdateActions action = UpdateActions.valueOf(child.getName().toUpperCase());
                        switch (action)
                        {
                            case ARGUMENTS:
                                String updateArguments = child.getTextTrim();
                                if (StringUtils.isNotBlank(updateArguments))
                                {
                                    Main.updateArgs.addAll(Main.stringToList(updateArguments, "\n"));
                                }
                                result = true;
                                break;
                            case VALIDATION:
                                if (file != null)
                                {
                                    result = runValidation(new File(file));
                                }
                                else if (cmd != null && compareValue != null)
                                {
                                    result = runValidation(getReplacementValue(cmd), getReplacementValue(compareValue), casesensitive, contains, notValidation);
                                }
                                else
                                {
                                    result = false;
                                    Main.println("WARNING!!! Invalid Validation Command");
                                    log.error("Validation Command missing either file, or os and value");
                                }
                                break;
                            case RESPONSE:
                                if (updateArgs.contains("--auto-accept")) {
                                    result = true;
                                } else if(file != null) {
                                    result = getResponse(new FileInputStream(file), true, agree, decline);
                                } else {
                                    if (StringUtils.isBlank(content)) {
                                        content = child.getText();
                                    }
                                    result = getResponse(new ByteArrayInputStream(content.getBytes(Charset.forName("UTF-8"))), false, agree, decline);
                                }
                                break;
                            case OS:
                                String osResult;
                                if (cmd != null)
                                {
                                    if (cmdOptions != null && cmdOptions.size() > 0)
                                    {
                                        cmd += " " + StringUtils.join(cmdOptions, " ");
                                    }
                                    cmd = main.getReplacementValue(cmd);
                                    osResult = executeOS(cmd, echo);
                                }
                                else
                                {
                                    osResult = "OS Type Does Not Match Local, Skipping OS COmmand";
                                }
                                log.info("OS Execution Result: " + osResult);
                                result = true;
                                break;
                            case START:
                                cmd = "bin/run";
                            case STOP:
                                if (cmd == null)
                                {
                                    cmd = "bin/stop";
                                }
                                if (os.contains("Win"))
                                {
                                    cmd += ".bat ";
                                    cmd = cmd.replaceAll("/", "\\\\");
                                }
                                else
                                {
                                    cmd += ".sh ";
                                }
                                List<String> components = new ArrayList<String>();
                                components.add(component);
                                StringBuffer startResult = new StringBuffer();
                                if (component.equalsIgnoreCase("RSREMOTE"))
                                {
                                    components = rsremoteInstances;
                                }
                                for (String startComponent : components)
                                {
                                    String startCmd = cmd + startComponent;
                                    if (cmdOptions != null && cmdOptions.size() > 0)
                                    {
                                        startCmd += " " + StringUtils.join(cmdOptions);
                                    }
                                    log.info("startCmd = [" + startCmd + "]");
                                    startResult.append(executeOS(startCmd, true));
                                }
                                log.info(cmd + " " + component + " Command Result: " + startResult.toString());
                                result = true;
                                break;
                            case SCRIPT:
                                if (file != null)
                                {
                                    result = executeScript(file, arguments, cmdOptions);
                                }
                                else
                                {
                                    result = executeGroovy(content, arguments);
                                }
                                break;
                            case SQL:
                                if (connection != null)
                                {
                                    //String sql = select != null ? select : update;
                                    if (select != null)
                                    {
                                        String selectResult = selectSQL(select, arguments);
                                        log.info("Update/Upgrade Select Statement result: " + selectResult);
                                        result = true;
                                    }
                                    else if (update != null)
                                    {
                                        result = true;
                                        for (String updateCmd : update.split("\n"))
                                        {
                                            if (StringUtils.isNotBlank(updateCmd))
                                            {
		                                        int updateResult = updateSQL(updateCmd, arguments);
		                                        log.info("Update/Upgrade Update Statement updated: " + updateResult + " rows");
		                                        if (updateResult == -1)
		                                        {
		                                            result = false;
		                                            if (failout)
		                                            {
		                                                log.error("Failout set to true, skipping any further update statements");
		                                                break;
		                                            }
		                                        }
                                            }
                                        }
                                    }
                                    else if (updateDirect != null)
                                    {
                                        int updateDirectResult = updateDirectSQL(updateDirect);
                                        log.info("Update/Upgrade Update Direct Statement updated: " + updateDirectResult + " rows");
                                        // result = updateResult > 0;
                                        result = true;
                                    }
                                }
                                else
                                {
                                    log.info("No Connection Available, Skipping SQL");
                                    result = true;
                                }
                                break;
                            case LOG:
                                String msg = child.getText();
                                log.info(msg);
                                if (echo)
                                {
                                    System.out.println(msg);
                                }
                                result = true;
                                break;
                            default:
                                log.error("Invalid Action: " + action);
                                break;
                        }

                    }
                    catch (IllegalArgumentException iae)
                    {
                        log.warn("Invalid Action: " + child.getName(), iae);
                    }
                    catch (Exception e)
                    {
                        result = false;
                        log.error("Unexpected Exception Running Update Command - " + e.getMessage(), e);
                    }

                    if (!result)
                    {
                        Main.println(failmsg.replaceAll("\\\\n", "\n"));
                        log.warn("Failed to Execute Update Command: " + failmsg);
                        // if failout is set, and either forceskip is now allowed, or --force was not passed in as an argument
                        if (StringUtils.isNotBlank(continueMessage))
                        {
                            if (updateArgs.contains("--auto-accept") || updateArgs.contains("--force"))
                            {
                                result = true;
                            }
                            else
                            {
                                continueMessage = continueMessage.replaceAll("\\\\n", "\n");
	                        	result = getResponse(new ByteArrayInputStream(continueMessage.getBytes(Charset.forName("UTF-8"))), false, "Y", "N");
                            }
                        	if (result)
                        	{
                        		//The --continue option should only apply to the install-update.groovy execution and to skip version check before that
                        		updateArgs.add("--continue");
                        	}
                        	else
                        	{
                        	    failout = true;
                        	    lastFailoutTrueResult = false;
                        	    break;
                        	}
                        }
                        else if (failout && (!forceskip || !updateArgs.contains("--force")))
                        {
                        	// Set last fail out true result
                        	lastFailoutTrueResult = result;
                            break;
                        }
                    }

                    if (onetimeid != null)
                    {
                        File onetimeFile = new File(onetimelog);
                        String onetimeStr = onetimeid;
                        if (onetimeFile.exists())
                        {
                            onetimeStr = FileUtils.readFileToString(onetimeFile) + "\n" + onetimeid;
                        }
                        log.warn("Onetime action " + onetimeid + " appears to have executed successfully, adding to onetime log");
                        FileUtils.writeStringToFile(onetimeFile, onetimeStr);
                    }
                    
                    // Set last fail out true result
                    if (!result && !failout) {
                    	lastFailoutTrueResult = lastFailoutTrueResult || result;
                    	log.info("Even though the current step failed, the fail out for current step is set to false. " +
                    			 "The last fail out true result after current step is " + lastFailoutTrueResult + ".");
                    } else {
                    	lastFailoutTrueResult = result;
                    }
                }
            }
            else
            {
                Main.println("WARNING!!! Update Procedure File : " + updateXmlFile.getAbsolutePath() + " is missing, Cancelling Update");
                log.error("Update Procedure File : " + updateXmlFile.getAbsolutePath() + " is missing, Cancelling Update");
                result = false;
            }
            long updateTime = System.currentTimeMillis() - start;
            log.info("Update Finished in " + updateTime + " milliseconds");
            if (!lastFailoutTrueResult)
            {
                Main.println("Execute Update Process Failed");
                log.error("Execute Update Process Failed");
                System.exit(1);
            }
            else
            {
                updateTime = updateTime / 1000;
                System.out.println("Update Finished in " + updateTime + " seconds");
            }
        }
        catch (Exception e)
        {
            log.error("Update Failed with unexpected exception " + e.getMessage(), e);
            System.exit(1);
        }
    }// executeUpdate

    private void initOptions(String[] args)
    {
        Options opt = new Options(args, Prefix.DASH);
        opt.getSet().addOption("d", Separator.BLANK, Multiplicity.ZERO_OR_ONE); // -d <dist>
        opt.getSet().addOption("f", Separator.BLANK, Multiplicity.ZERO_OR_MORE);// -f update xml
        opt.getSet().addOption("F", Separator.NONE, Multiplicity.ZERO_OR_MORE);// -FD<update args> (e.g. --force, --nostop)
        opt.getSet().addOption("u", Separator.BLANK, Multiplicity.ZERO_OR_ONE);// -u <rsconsole username>
        opt.getSet().addOption("p", Separator.BLANK, Multiplicity.ZERO_OR_ONE);// -p <rsconsole password>
        opt.getSet().addOption("z", Separator.BLANK, Multiplicity.ZERO_OR_ONE);// -z <zip file name>
        opt.check();

        Options optSlash = new Options(args, Prefix.SLASH);
        optSlash.getSet().addOption("\\?");
        optSlash.check();

        if (opt.getSet().isSet("d"))
        {
            dist = opt.getSet().getOption("d").getResultValue(0);
        }
        else
        {
            Main.println("ERROR: Installation Directory must be passed for Update");
            System.exit(1);
        }
        if (opt.getSet().isSet("f"))
        {
            String filename = opt.getSet().getOption("f").getResultValue(0);
            String tmpFilename = filename.replaceAll("\\\\", "/");
            if (tmpFilename.equals("file/install-update.groovy") || tmpFilename.equals("file/install-update"))
            {
                initLog();
                log.warn("Old Process Detected, Updating to New Update Process");
                try
                {
                    File newUpdateScript = os.startsWith("Windows") ? new File("file/bin/update.bat") : new File("file/bin/update.sh");
                    File tmpUpdateScript = os.startsWith("Windows") ? new File("bin/updtmp.bat") : new File("bin/updtmp.sh");
                    boolean tmpSuccess = newUpdateScript.renameTo(tmpUpdateScript);
                    tmpSuccess = tmpSuccess && tmpUpdateScript.setExecutable(true);

                    if (tmpSuccess)
                    {
                        // Change jdk to jre for old update
                        byte[] tmpUpdateScriptBytes = new byte[(int) tmpUpdateScript.length()];
                        FileInputStream fis = new FileInputStream(tmpUpdateScript);
                        fis.read(tmpUpdateScriptBytes);
                        fis.close();

                        String tmpUpdateScriptStr = new String(tmpUpdateScriptBytes);
                        tmpUpdateScriptStr = tmpUpdateScriptStr.replace("jdk", "jre");

                        tmpUpdateScriptBytes = tmpUpdateScriptStr.getBytes();
                        FileOutputStream fos = new FileOutputStream(tmpUpdateScript);
                        fos.write(tmpUpdateScriptBytes);
                        fos.close();

                        String[] strArgs = opt.getSet().getOption("F").getResultValue(0).split(";");
                        String cmd = "\"" + tmpUpdateScript.getAbsolutePath() + "\"";
                        cmd += " -f " + strArgs[0];
                        if (strArgs.length >= 3)
                        {
                            cmd += " -u " + strArgs[1];
                            cmd += " -p " + strArgs[2];
                            for (int i = 3; i < strArgs.length; i++)
                            {
                                cmd += " " + strArgs[i];
                            }
                        }
                        executeOS(cmd, true);
                        tmpUpdateScript.deleteOnExit();
                        System.exit(0);
                    }
                    else
                    {
                        log.error("Failed to Create tmp update file");
                        System.exit(1);
                    }
                }
                catch (Exception e)
                {
                    log.error("Updating to New Update/Upgrade Process Failed", e);
                    System.exit(1);
                }
            }
            else
            {
                File updateXmlFile = new File(filename);
                if (!updateXmlFile.exists())
                {
                	File upgradeXmlFile = new File(filename.replace("update.xml", "upgrade.xml"));
                	if (upgradeXmlFile.exists())
                	{
                		filename = upgradeXmlFile.getAbsolutePath();
                	}
                }
                updateXmlFilename = filename;
            }
        }
        else
        {
            Main.println("ERROR: Update Script File must be passed");
            System.exit(1);
        }
        if (opt.getSet().isSet("F"))
        {
            String strArgs = opt.getSet().getOption("F").getResultValue(0);
            updateArgs.addAll(stringToList(strArgs, ";"));
            if (updateArgs.contains("--for-dr"))
            {
                if (!updateArgs.contains("--no-start"))
                {
                    updateArgs.add("--no-restart");
                }
                if (!updateArgs.contains("--no-sql"))
                {
                    updateArgs.add("--no-sql");
                }
            }
        }
        if (opt.getSet().isSet("u"))
        {
            consoleLoginUser = opt.getSet().getOption("u").getResultValue(0);
        }
        if (opt.getSet().isSet("p"))
        {
            consoleLoginPass = opt.getSet().getOption("p").getResultValue(0);
        }
        if (opt.getSet().isSet("z"))
        {
            updateZipFilename = opt.getSet().getOption("z").getResultValue(0);

            Pattern updateFilePattern = Pattern.compile("(.+)-(.+).zip");
            Matcher updateFileMatcher = updateFilePattern.matcher(updateZipFilename);
            if (updateFileMatcher.find())
            {
                type = updateFileMatcher.group(1);
                if (type.indexOf("-") > 0)
                {
                    type = type.substring(0, type.indexOf("-"));
                }
                buildVersion = updateFileMatcher.group(2);
            }
        }
    } // initOptions

    public void initBlueprint()
    {
        initBlueprint(false);
    }// initBlueprint

    public void initBlueprint(boolean reload)
    {
        File blueprintFile = new File(blueprintFileName);
        if (blueprintFile.exists())
        {
            Properties initialProperties = blueprintProperties;

            Properties configureProperties = new Properties();

            if (reload && initialProperties != null)
            {
                configureProperties.putAll(initialProperties);
            }

            FileInputStream fis = null;
            try
            {
                fis = new FileInputStream(blueprintFile);
                configureProperties.load(fis);
            }
            catch (Exception e)
            {
                log.fatal("Failed to Run Update", e);
                throw new RuntimeException("UPDATE FAILED", e);
            }
            finally
            {
                if (fis != null)
                {
                    try
                    {
                        fis.close();
                    }
                    catch (Exception e)
                    {
                        log.error("Failed to Close Blueprint (" + blueprintFileName + ") Input Stream", e);
                    }
                }
            }

            if (!reload && initialProperties != null)
            {
                configureProperties.putAll(initialProperties);
            }

            blueprintProperties = configureProperties;

            String configRSRemote = blueprintProperties.get("resolve.rsremote") != null ? (String) blueprintProperties.get("resolve.rsremote") : "false";
            if ("true".equalsIgnoreCase(configRSRemote))
            {
                String rsremotes = blueprintProperties.get("rsremote.instance.count") != null ? (String) blueprintProperties.get("rsremote.instance.count") : "1";
                log.info("rsremote instance count: " + rsremotes);
                if (rsremotes != null && rsremotes.matches("\\d+"))
                {
                    blueprintRSRemote = true;
                    int count = Integer.parseInt(rsremotes);
                    rsremoteInstances = new ArrayList<String>(count);
                    for (int i = 1; i <= count; i++)
                    {
                        String instance = blueprintProperties.get("rsremote.instance" + i + ".name") != null ? blueprintProperties.get("rsremote.instance" + i + ".name").toString().toLowerCase() : "rsremote";
                        log.warn("rsremote.instance" + i + ".name=" + instance);
                        rsremoteInstances.add(instance);
                    }
                }
            }

            String configRSView = blueprintProperties.get("resolve.rsview") != null ? (String) blueprintProperties.get("resolve.rsview") : "false";
            String primaryRSView = blueprintProperties.get("rsview.general.primary") != null ? (String) blueprintProperties.get("rsview.general.primary") : "false";
            if (configRSView.contains("${"))
            {
                configRSView = getReplacement("resolve.rsview");
                configRSView = configRSView != null ? configRSView.trim() : "false";
            }
            if (primaryRSView.contains("${"))
            {
                primaryRSView = getReplacement("rsview.general.primary").trim();
                primaryRSView = primaryRSView != null ? primaryRSView.trim() : "false";
            }
            if ("true".equalsIgnoreCase(configRSView) && "true".equalsIgnoreCase(primaryRSView))
            {
                this.primaryRSView = true;
            }
        }
        else
        {
            log.error("Blueprint File Not Found: " + blueprintFileName);
        }
    } // initBlueprint

    private void initLog()
    {
        FileInputStream fis = null;
        try
        {
            // RBA-13770 : log4j:WARN No such property [enabled] in org.apache.log4j.net.SyslogAppender

            String configFile = dist + "/rsmgmt/config/log.cfg";

            File file = FileUtils.getFile(configFile);
            if (file.exists() == false)
            {
                throw new Exception("Unable to find Log configuration file: " + file.getAbsolutePath());
            }

            Properties currentLog4JProperties = new Properties();
            fis = new FileInputStream(configFile);
            BufferedInputStream bis = new BufferedInputStream(fis);
            if (bis != null)
            {
                currentLog4JProperties.load(bis);
            }
            Properties elementsToRemove = new Properties();
            String ntEventLogEnabled = currentLog4JProperties.containsKey("log4j.appender.NTEventLog.enabled") ? currentLog4JProperties.getProperty("log4j.appender.NTEventLog.enabled") : "false";
            String syslogEnabled = currentLog4JProperties.containsKey("log4j.appender.SYSLOG.enabled") ? currentLog4JProperties.getProperty("log4j.appender.SYSLOG.enabled") : "false";
            elementsToRemove.put("log4j.appender.NTEventLog.enabled", ntEventLogEnabled);
            elementsToRemove.put("log4j.appender.SYSLOG.enabled", syslogEnabled);
            for (Object keyToRemove : elementsToRemove.keySet())
            {
                if (currentLog4JProperties.containsKey(keyToRemove))
                {
                    elementsToRemove.put(keyToRemove, currentLog4JProperties.get(keyToRemove));
                    currentLog4JProperties.remove(keyToRemove);
                }
            }
            // configure logger
            PropertyConfigurator.configure(currentLog4JProperties);

            // init logging
            log = Logger.getLogger("com.resolve.update");

            GroovyScript.setLogger(log);
            CryptMD5.setLogger(log);
            Compress.setLogger(log);
            XDoc.setLogger(log);
            RESTUtils.setLogger(log);
            CryptUtils.setLogger(log);
        }
        catch (Exception e)
        {
            throw new RuntimeException("Failed to initialize Update Logging", e);
        }
        finally
        {
            if (fis != null)
            {
                try
                {
                    fis.close();
                }
                catch (IOException e)
                {
                    // do nothing
                }
            }
        }
    } // initLog

    private void initBindings()
    {
        binding.setVariable("MAIN", main);
        binding.setVariable("LOG", log);
        binding.setVariable("BLUEPRINT", blueprintProperties);
        binding.setVariable("HELP", false);
        binding.setVariable("args", updateArgs); // args
        binding.setVariable("ARGS", updateArgs); // ARGS
        binding.setVariable("DB", connection);
        binding.setVariable("DIST", dist);
        binding.setVariable("FILENAME", updateZipFilename);
    } // initBindings

    private void initExtraOptions()
    {
        blueprintProperties.put("RSCONSOLEUSERNAME", consoleLoginUser);
        blueprintProperties.put("RSCONSOLEPASSWORD", consoleLoginPass);
        String rsviewGuid = "RSVIEW";
        if (isPrimaryRSView())
        {
            try
            {
                File configFile = new File(dist + "/tomcat/webapps/resolve/WEB-INF/config.xml");
                if (configFile.exists())
                {
                    XDoc configDoc = new XDoc(configFile);
                    rsviewGuid = configDoc.getStringValue("./ID/@GUID");
                }
            }
            catch (Exception e)
            {
                log.error("Unable to establish local RSView GUID", e);
            }
        }
        blueprintProperties.put("GUID", rsviewGuid);
        blueprintProperties.put("TYPE", type);
        blueprintProperties.put("BUILDVERSION", buildVersion);
        blueprintProperties.put("FILENAME", updateZipFilename);
        blueprintProperties.put("DIST", dist);

        String commandString;
        if (os.startsWith("Windows"))
        {
        	File jarVersion = new File("bin\\jar-version.bat");
        	try {
				String jvContents = FileUtils.readFileToString(jarVersion);
				if(jvContents.contains("bin\\UNZIP.exe"))
				{
					jvContents = jvContents.replace("bin\\UNZIP.exe","UNZIP.exe");
				}
				FileUtils.writeStringToFile(jarVersion, jvContents);
			} catch (IOException e) {
				log.error("failed to read jar-version with error: ",e);
			}
            commandString = "bin\\jar-version.bat rsmgmt\\lib\\resolve-mgmt.jar";
        }
        else
        {
            commandString = "bin/jar-version.sh rsmgmt/lib/resolve-mgmt.jar";
        }
        log.info("jar version command " + commandString);
        String currentVersionAndBuild = executeOS(commandString);

        // example result version.5.2.0-20150220
        Pattern updateFilePattern = Pattern.compile("version\\.(.+)");
        Matcher updateFileMatcher = updateFilePattern.matcher(currentVersionAndBuild);
        log.info("current version and build " + currentVersionAndBuild);
        if (updateFileMatcher.find())
        {
            log.info("found");
            currentBuildVersion = updateFileMatcher.group(1);
        }
        else
        {
            log.info("not found default to 5.0");
            currentBuildVersion = "5.0";
        }
        log.info("current build version is " + currentBuildVersion);
        blueprintProperties.put("CURRENTBUILDVERSION", currentBuildVersion);
    }

    private static List<String> stringToList(String str, String separator)
    {
        ArrayList<String> result = new ArrayList<String>();

        if (str != null && !str.equals(""))
        {
            String[] values = str.split(separator);
            for (int i = 0; i < values.length; i++)
            {
                if (values[i] != null)
                {
                    result.add(values[i].trim());
                }
            }
        }

        return result;
    } // stringToList

    public String executeMgmtScript(String script, String args)
    {
        StringBuilder result = new StringBuilder();
        if (StringUtils.isBlank(script))
        {
            log.error("No Script Passed to Execute");
        }
        else
        {
            log.info("Executing script " + script + " with args " + args);
            Process process = null;
            BufferedReader br = null;
            PrintWriter in = null;

            try
            {
                String command = "\"" + dist;
                if (os.contains("Win"))
                {
                    command += "\\rsmgmt\\bin\\run.bat\" -f " + script;
                }
                else
                {
                    command += "/rsmgmt/bin/run.sh\" -f " + script;
                }
                if (StringUtils.isNotBlank(args))
                {
                    command += " \"-FD" + args + "\"";
                }
                command += " -u";
                ProcessBuilder pb = new ProcessBuilder(prepareCommand(command));
                log.info("dist: " + dist);
                pb.redirectErrorStream(true);
                pb.directory(new File(dist));
                log.info("process builder starting at dir " + pb.directory() + " with command " + Arrays.toString(pb.command().toArray()));
                // start process
                process = pb.start();

                // wait for completion of spawned process
                process.getOutputStream().close();

                br = new BufferedReader(new InputStreamReader(process.getInputStream()));
                boolean blankLine = false;
                String line;
                while ((line = br.readLine()) != null)
                {
                    if (line.length() > 0)
                    {
                        result.append(line + "\n");
                        blankLine = false;
                    }
                    else
                    {
                        if (blankLine)
                        {
                            result.append("\n");
                            blankLine = false;
                        }
                        else
                        {
                            blankLine = true;
                        }
                    }
                }
            }
            catch (Exception e)
            {
                log.error("Failed to Execute RSMgmt script " + script + " with args " + args, e);
                result.append("FAILURE - Unable to Execute script - " + e.getMessage());
            }
            finally
            {
                try
                {
                    if (process != null)
                    {
                        process.getInputStream().close();
                        process.getErrorStream().close();
                        process.getOutputStream().close();
                        process.destroy();
                    }
                    if (br != null)
                    {
                        br.close();
                    }
                    if (in != null)
                    {
                        in.close();
                    }

                }
                catch (Exception e)
                {
                    log.warn(e.getMessage(), e);
                }
            }
        }
        return result.toString();
    } // executeMgmtScript

    private List<String> prepareCommand(String command)
    {
        // LOGGING DISABLED FOR TESTING
        ArrayList<String> result = new ArrayList<String>();

        // if(log.isTraceEnabled())
        // {
        // log.trace("Execute command: "+command);
        // }

        StringBuffer currPart = new StringBuffer();
        boolean isEscaped = false;
        boolean insideQuote = false;
        char quoteChar = '\0';

        // System.out.println("command lenght: " + command.length());
        for (int x = 0; x < command.length(); x++)
        {
            char ch = command.charAt(x);
            char ch2 = '\0';
            if (x + 1 < command.length())
            {
                ch2 = command.charAt(x + 1);
            }

            // handle escaped \ - for the following characters " '
            if ((ch == '\\') && ((ch2 == '"') || ch2 == '\''))
            {
                isEscaped = true;
            }
            else if (isEscaped)
            {
                isEscaped = false;
                currPart.append(ch);
            }

            // match first quote
            else if (insideQuote == false && (ch == '\'' || ch == '"'))
            {
                quoteChar = ch;
                insideQuote = true;
                // if windows, keep the quotes
                if (os.contains("Win"))
                {
                    currPart.append(ch);
                }
            }

            // match end quote
            else if (insideQuote == true && ch == quoteChar)
            {
                quoteChar = '\0';
                insideQuote = false;

                // only add if not empty "" or " "
                if (!currPart.toString().trim().equals(""))
                {
                    // if windows, keep the quotes
                    if (os.contains("Win") && !currPart.toString().trim().equals("" + ch))
                    {
                        currPart.append(ch);
                        result.add(currPart.toString());
                    }
                    else if (!os.contains("Win"))
                    {
                        result.add(currPart.toString());
                    }
                }
                currPart = new StringBuffer();
            }
            else if (insideQuote == false && Character.isWhitespace(ch))
            {
                // only add if not empty "" or " "
                if (!currPart.toString().trim().equals(""))
                {
                    result.add(currPart.toString());
                }
                currPart = new StringBuffer();
            }
            else
            {
                isEscaped = false;
                currPart.append(ch);
            }

            // get the last part of the command
            if (x == command.length() - 1)
            {
                // only add if not empty "" or " "
                if (!currPart.toString().trim().equals(""))
                {
                    result.add(currPart.toString());
                }
            }
        }
        // THIS IS AN INFINITE LOOP!
        //
        //
        // log.trace("preparedCommand:");
        /*
         * int idx = 0;
         * 
         * for (Iterator i=result.iterator(); i.hasNext(); )
         * {
         * //log.trace(idx+": "+i.next());
         * System.out.println("idx: " + idx);
         * idx++;
         * }
         */

        return result;
    } // prepareCommand

    public void connectDB()
    {
        if ("true".equalsIgnoreCase((String) blueprintProperties.get("rsmgmt.sql.active")))
        {
            try
            {
                String url = getReplacement("rsmgmt.sql.url");
                String dbType = getReplacement("rsmgmt.sql.dbtype");
                String username = getReplacement("rsmgmt.sql.username");
                String password = CryptUtils.decrypt(getReplacement("rsmgmt.sql.password"));
                if (StringUtils.isBlank(url))
                {
                    String hostname = getReplacement("rsmgmt.sql.host");
                    String dbname = getReplacement("rsmgmt.sql.dbname");
                    String port;
                    if (dbType.equalsIgnoreCase("mysql") || dbType.equalsIgnoreCase("mariadb"))
                    {
                        port = hostname.indexOf(":") != -1 ? hostname.substring(hostname.indexOf(":")) : "3306";
                        url = "jdbc:mysql://" + hostname + ":" + port + "/" + dbname;
                    }
                    else
                    {
                        port = hostname.indexOf(":") != -1 ? hostname.substring(hostname.indexOf(":")) : "1521";
                        url = "jdbc:oracle:thin:@" + hostname + ":" + port + ":" + dbname;
                    }
                }
                log.info("Connecting to JDBC URL: " + url);
                if (dbType.equalsIgnoreCase("mysql") || dbType.equalsIgnoreCase("mariadb"))
                {
                    Class.forName(MYSQLDRIVER);
                    connection = DriverManager.getConnection(url, username, password);
                }
                else if (dbType.toLowerCase().startsWith("oracle"))
                {
                    Class.forName(ORACLEDRIVER);
                    connection = DriverManager.getConnection(url, username, password);
                }
                else
                {
                    log.info("Not a Valid DB, Skipping connection creation");
                }
            }
            catch (Exception e)
            {
                log.error("Failed to Create DB Connection", e);
            }
        }
    }// connectDB

    public String getBlueprintValue(String key)
    {
        String value = blueprintProperties.get(key) != null ? (String) blueprintProperties.get(key) : null;

        if (value != null && value.contains("${"))
        {
            value = getReplacement(key);
        }

        return value;
    } // getBlueprintValue

    private String getReplacement(Object key)
    {
        String value = (String) blueprintProperties.get(key);
        return getReplacementValue(value);
    } // getReplacement

    private String getReplacementValue(String value)
    {
        if (value != null)
        {
            while (value.matches("(?s).*(?<!\\\\)\\$\\{.*\\}.*"))
            {
                String replaceValue = "";
                String replacePattern = "${";
                Pattern pattern = Pattern.compile("(?s).*(?<!\\\\)\\$\\{(.+?)\\}.*");
                Matcher matcher = pattern.matcher(value);
                if (matcher.matches())
                {
                    replacePattern = matcher.group(1);
                    replaceValue = (String) blueprintProperties.get(replacePattern);
                    log.trace("Get Replacement: " + replacePattern + "->" + replaceValue);
                    if (replaceValue == null)
                    {
                        replaceValue = "";
                    }
                }
                value = value.replace("${" + replacePattern + "}", replaceValue);
            }
            value = value.replaceAll("\\\\\\$", "\\$");
        }

        return value;
    } // getReplacementValue

    private boolean checkIfBool(String ifbool)
    {
        String ifboolStr = getReplacementValue(ifbool);
        ifboolStr = ifboolStr.replaceAll("AND", "&&");
        ifboolStr = ifboolStr.replaceAll("OR", "||");
        String script = "return (Boolean)(" + ifboolStr + ");";
        Binding binding = new Binding();
        Boolean value;
        try
        {
            log.debug("Run Validation: " + script);
            value = (Boolean) GroovyScript.execute(script, "UpdateValidation", binding);
        }
        catch (Exception e)
        {
            log.error("Failed to validate expression: " + ifbool);
            value = false;
        }

        return value;
    }// checkIfBool

    public static void print(String out)
    {
        System.out.print(out);
    } // print

    public static void println()
    {
        System.out.println("");
    } // println

    public static void println(String out)
    {
        System.out.println(out);
    } // println

    public String getDist()
    {
        return dist;
    } // getDist

    public boolean isBlueprintRSRemote()
    {
        return blueprintRSRemote;
    } // isBlueprintRSRemote

    public boolean isDBConnected()
    {
        boolean result = false;
        try {
            result = (connection != null);
            if (result) {
                boolean tested = false;
				int retryCount = 0;
				while (!tested && retryCount < 10) {
					try {
					    if (connection.isClosed() || !connection.isValid(10)) {
							log.error("Connection is closed or invalid, trying reconnect");
							Thread.sleep(1000);
							result = false;
							connectDB();
					    } else {
					        tested = true;
					    }
					} catch (SQLException sqle) {
						log.error("Connection Has Gone Bad, Trying Reconnect", sqle);
					}
				}
				if (!result) {
					log.error("Reconnection to database has failed");
				}
            }
        } catch (Exception e) {
            log.error("Error checking DB connection status: ", e);
            result = false;
        }
        return result;
    }

    public List<String> getRSRemoteInstances()
    {
        return rsremoteInstances;
    } // getRSRemoteInstances

    public boolean isPrimaryRSView()
    {
        return primaryRSView;
    } // isPrimaryRSView

    private static enum UpdateAttributes
    {
        FORCESKIP, FAILMSG, FAILOUT, IFBOOL, IF, SKIPIF, ONETIMEID, FILE, VALUE, CONTAINS, NOTCONTAINS, CMD, COMPONENT, SELECT, UPDATE, CASESENSITIVE, CONT, EXIT, ECHO, CURRENTVERSION, UPDATEDIRECT, CONTINUEMESSAGE;
    }

    private static enum UpdateOptions
    {
        ARG, UNIX, LINUX, SOLARIS, WINDOWS, OPTION, CONTENT, UPDATE;
    }

    private static enum UpdateActions
    {
        ARGUMENTS, VALIDATION, RESPONSE, OS, START, STOP, SCRIPT, SQL, LOG;
    }

    private static enum UpdateOstypes
    {
        UNIX, LINUX, SOLARIS, WINDOWS;
    }

}
