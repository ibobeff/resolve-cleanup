/******************************************************************************
* (C) Copyright 2014
*
* Resolve Systems, LLC
*
* All rights reserved
* This software is distributed under license and may not be used, copied,
* modified, or distributed without the express written permission of
* Resolve Systems, LLC.
*
******************************************************************************/

package com.resolve.update;

import java.io.ByteArrayInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.InputStream;
import java.io.StringWriter;
import java.util.Hashtable;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Vector;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.apache.commons.io.FileUtils;
import org.apache.commons.lang3.StringUtils;
import org.apache.log4j.Logger;
import org.dom4j.Attribute;
import org.dom4j.Document;
import org.dom4j.DocumentException;
import org.dom4j.DocumentHelper;
import org.dom4j.Element;
import org.dom4j.Namespace;
import org.dom4j.Node;
import org.dom4j.XPath;
import org.dom4j.io.OutputFormat;
import org.dom4j.io.SAXReader;
import org.dom4j.io.XMLWriter;
import org.dom4j.tree.DefaultCDATA;
import org.xml.sax.EntityResolver;
import org.xml.sax.InputSource;
import org.xml.sax.SAXException;

public class XDoc
{
    public static Logger log; 
    
    Document doc;
    Element root;

    public XDoc() throws DocumentException
    {
        this.doc = DocumentHelper.createDocument();
        this.root = null;
    } // XDoc

    public XDoc(String xml) throws DocumentException, SAXException
    {
        // xml cleanup
        xml = cleanup(xml);

        SAXReader reader = new SAXReader();
        reader.setFeature("http://apache.org/xml/features/disallow-doctype-decl", true);
        reader.setFeature("http://xml.org/sax/features/external-general-entities", false);
        reader.setFeature("http://xml.org/sax/features/external-parameter-entities", false);
        // read into string then parse
        //doc = DocumentHelper.parseText(xml);
        doc = reader.read(new ByteArrayInputStream(xml.getBytes()));
        root = doc.getRootElement();
        if (root.getNamespaceForPrefix("rsi") == null)
        {
            root.addNamespace("rsi", "Resolve Systems");
        }
        if (root.getNamespaceForPrefix("common") == null)
        {
            root.addNamespace("common", "Common");
        }
    } // XDoc

    public XDoc(File file) throws Exception
    {
        // parse directly from file
        if (file.exists())
        {
            SAXReader xmlReader = new SAXReader();
            xmlReader.setFeature("http://apache.org/xml/features/disallow-doctype-decl", true);
            xmlReader.setFeature("http://xml.org/sax/features/external-general-entities", false);
            xmlReader.setFeature("http://xml.org/sax/features/external-parameter-entities", false);
            doc = xmlReader.read(file);
            root = doc.getRootElement();
            if (root.getNamespaceForPrefix("rsi") == null)
            {
                root.addNamespace("rsi", "Resolve Systems");
            }
            if (root.getNamespaceForPrefix("common") == null)
            {
                root.addNamespace("common", "Common");
            }
        }
        else
        {
            throw new Exception("Missing xml file: " + file.getName());
        }
    } // XDoc

    public XDoc(String xml, boolean hibernate) throws Exception
    {
        // xml cleanup
        xml = cleanup(xml);

        // read into string then parse
        ByteArrayInputStream is = new ByteArrayInputStream(xml.getBytes());
        try
        {
            initHibernate(is, hibernate);
        }
        finally
        {
            is.close();
        }
    } // XDoc

    public XDoc(File file, boolean hibernate) throws Exception
    {
        // parse directly from file
        if (file.exists())
        {
            FileInputStream fis = new FileInputStream(file);
            try
            {
                initHibernate(fis, hibernate);
            }
            finally
            {
                fis.close();
            }
        }
    } // XDoc

    void initHibernate(InputStream istream, boolean isHibernate) throws Exception
    {
        // parse directly from file
        if (istream != null)
        {
            EntityResolver resolver = new EntityResolver()
            {
                public InputSource resolveEntity(String publicId, String systemId)
                {
                    if (publicId.equals("-//Hibernate/Hibernate Configuration DTD 3.0//EN"))
                    {
                        InputStream in = getClass().getResourceAsStream("/org/hibernate/hibernate-configuration-3.0.dtd");
                        return new InputSource(in);
                    }
                    else if (publicId.equals("-//Hibernate/Hibernate Mapping DTD 3.0//EN"))
                    {
                        InputStream in = getClass().getResourceAsStream("/org/hibernate/hibernate-mapping-3.0.dtd");
                        return new InputSource(in);
                    }
                    return null;
                }
            };

            SAXReader xmlReader = new SAXReader();
            xmlReader.setEntityResolver(resolver);
            doc = xmlReader.read(istream);
            root = doc.getRootElement();
            if (root.getNamespaceForPrefix("rsi") == null)
            {
                root.addNamespace("rsi", "Resolve Systems");
            }
            if (root.getNamespaceForPrefix("common") == null)
            {
                root.addNamespace("common", "Common");
            }
        }
        else
        {
            throw new Exception("Missing inputstream");
        }
    } // XDoc

    public Element addRoot(String name)
    {
        Element result = doc.addElement(name);
        root = result;
        if (root.getNamespaceForPrefix("rsi") == null)
        {
            root.addNamespace("rsi", "Resolve Systems");
        }
        if (root.getNamespaceForPrefix("common") == null)
        {
            root.addNamespace("common", "Common");
        }

        return result;
    } // addRoot

    public Element addRoot(Element elm)
    {
        doc.setRootElement(elm);
        root = elm;
        if (root.getNamespaceForPrefix("rsi") == null)
        {
            root.addNamespace("rsi", "Resolve Systems");
        }
        if (root.getNamespaceForPrefix("common") == null)
        {
            root.addNamespace("common", "Common");
        }

        return elm;
    } // addRoot

    public void removeResolveNamespaces()
    {
        if (root == null) return;
        Namespace space = root.getNamespaceForPrefix("rsi");
        if (space != null)
        {
            root.remove(space);
        }
        space = root.getNamespaceForPrefix("common");
        if (space != null)
        {
            root.remove(space);
        }
    }

    @Override
    public String toString()
    {
        return doc.asXML();
    } // toString

    public String toFormattedString() throws Exception
    {
        // Pretty print the document to String
        StringWriter stringWriter = new StringWriter();
        OutputFormat format = OutputFormat.createPrettyPrint();
        format.setTrimText(false);
        format.setEncoding("UTF-8");
        XMLWriter writer = new XMLWriter(stringWriter, format);
        writer.write(doc);

        return stringWriter.toString();
    } // toLog

    public String toPrettyString() throws Exception
    {
        // Pretty print the document to String
        StringWriter stringWriter = new StringWriter();
        OutputFormat format = OutputFormat.createPrettyPrint();
        XMLWriter writer = new XMLWriter(stringWriter, format);
        writer.write(doc);

        return stringWriter.toString();
    } // toLog

    public void toFile(File file) throws Exception
    {
        String xml = translateSpecial(toFormattedString());
        FileUtils.writeStringToFile(file, xml, "UTF-8");
    } // toFile

    public void toPrettyFile(File file) throws Exception
    {
        String xml = translateSpecial(toPrettyString());
        FileUtils.writeStringToFile(file, xml, "UTF-8");
    } // toPrettyFile

    public Document getDocument()
    {
        return this.doc;
    } // getDocument

    public Element getRoot()
    {
        return this.root;
    } // getRoot

    public XDoc getXDocAtElement(String elementName)
    {
        XDoc result = null;

        try
        {
            result = new XDoc();

            Element state = getRoot().element(elementName);
            if (state != null)
            {
                result.addRoot((Element) state.detach());
            }
            else
            {
                result.addRoot(elementName);
            }
        }
        catch (Exception e)
        {
            log.warn("Failed to get XDoc: " + e.getMessage(), e);
        }

        return result;
    } // getXDocAtElement

    public static String getValidName(String name)
    {
        String result = "NULL";

        if (!StringUtils.isEmpty(name))
        {
            name = name.trim();
            name = name.replace(' ', '-');
            result = name.toUpperCase();
        }
        return result;
    } // getValidName

    public static boolean isValidName(String name)
    {
        if (StringUtils.contains(name, ' '))
        {
            return false;
        }
        else if (StringUtils.contains(name, '.'))
        {
            return false;
        }
        return true;
    } // isValidName

    public String getValue(String xpath)
    {
        return doc.valueOf(xpath);
    } // getValue

    public List getNodes(String xpath)
    {
        Object elm = root;
        if (xpath.charAt(0) == '/')
        {
            elm = doc;
        }
        return getNodes(elm, xpath);
    } // getNodes

    public List getNodes(Object obj, String xpath)
    {
        List result = null;

        try
        {
            XPath xpathSelector = DocumentHelper.createXPath(xpath);
            result = xpathSelector.selectNodes(obj);
        }
        catch (Exception e)
        {
            if (e.getMessage().contains("XPath expression uses unbound namespace prefix"))
            {
                log.warn("getNodes() exception: " + e.getMessage());
            }
            else
            {
                log.warn("getNodes() exception: " + e.getMessage(), e);
            }
        }

        return result;
    } // getNodes

    public Node getNode(String xpath)
    {
        Object elm = root;
        if (xpath.charAt(0) == '/')
        {
            elm = doc;
        }
        return getNode(elm, xpath);
    } // getNode

    public Node getNode(Object obj, String xpath)
    {
        XPath xpathSelector = DocumentHelper.createXPath(xpath);
        return xpathSelector.selectSingleNode(obj);
    } // getNode

    public Node getLastNode(String xpath)
    {
        Object elm = root;
        if (xpath.charAt(0) == '/')
        {
            elm = doc;
        }
        return getLastNode(elm, xpath);
    } // getLastNode

    public Node getLastNode(Object obj, String xpath)
    {
        Node result = null;

        XPath xpathSelector = DocumentHelper.createXPath(xpath);
        List list = xpathSelector.selectNodes(obj);
        if (list.size() > 0)
        {
            result = (Node) list.get(list.size() - 1);
        }

        return result;
    } // getNodes

    public void removeElements(String xpath)
    {
        Object elm = root;
        if (xpath.charAt(0) == '/')
        {
            elm = doc;
        }
        removeElements(elm, xpath);
    } // removeElements

    public void removeElements(Object obj, String xpath)
    {
        List nodes = getNodes(obj, xpath);
        if (nodes != null)
        {
            for (Iterator i = nodes.iterator(); i.hasNext();)
            {
                Node node = (Node) i.next();
                if (node.getNodeType() == Node.ATTRIBUTE_NODE)
                {
                    node = node.getParent();
                }
                node.detach();
            }
        }
    } // removeElements

    public void removeElement(String xpath)
    {
        Object elm = root;
        if (xpath.charAt(0) == '/')
        {
            elm = doc;
        }
        removeElement(elm, xpath);
    } // removeElement

    public void removeElement(Object obj, String xpath)
    {
        Node node = getNode(obj, xpath);
        if (node != null)
        {
            if (node.getNodeType() == Node.ATTRIBUTE_NODE)
            {
                node = node.getParent();
            }
            node.detach();
        }
    } // removeElement

    public void removeLastElement(String xpath)
    {
        Object elm = root;
        if (xpath.charAt(0) == '/')
        {
            elm = doc;
        }
        removeLastElement(elm, xpath);
    } // removeLastElement

    public void removeLastElement(Object obj, String xpath)
    {
        Node node = getLastNode(obj, xpath);
        if (node != null)
        {
            if (node.getNodeType() == Node.ATTRIBUTE_NODE)
            {
                node = node.getParent();
            }
            node.detach();
        }
    } // removeLastElement

    public String[] getXPathPathAttr(String xpath)
    {
        return xpath.split("/@");
    } // getXPathPathAttr

    public String getXPathPath(String xpath)
    {
        String result = null;

        String[] pathAttr = xpath.split("/@");
        result = pathAttr[0];
        return result;
    } // getXPathPath

    public String getXPathAttr(String xpath)
    {
        String result = null;

        String[] pathAttr = xpath.split("/@");
        if (pathAttr.length > 1)
        {
            result = pathAttr[1];
        }
        return result;
    } // getXPathAttr

    public Element createPath(String path)
    {
        return createPath(root, path);
    } // createPath

    public Element createPath(Element elm, String path)
    {
        String[] elm_paths = path.split("/");

        Element curr_element = elm;
        Element parent_element = elm;

        for (int i = 0; i < elm_paths.length; i++)
        {
            String curr_path = elm_paths[i];
            if (!StringUtils.isEmpty(curr_path) && !curr_path.equals(".") && !curr_path.startsWith("@"))
            {
                curr_element = curr_element.element(curr_path);
                if (curr_element == null)
                {
                    curr_element = parent_element.addElement(curr_path);
                }
                parent_element = curr_element;
            }
        }
        return curr_element;
    } // createPath

    public String getStringValue(String xpath)
    {
        return getStringValue(root, xpath, "");
    } // getStringValue

    public String getStringValue(String xpath, String defaultValue)
    {
        return getStringValue(root, xpath, defaultValue);
    } // getStringValue

    public String getStringValue(Element elm, String xpath, String defaultValue)
    {
        String result = defaultValue;

        boolean found = false;

        // initialize values
        String[] pathAttr = getXPathPathAttr(xpath);
        String path = pathAttr[0];

        // find latest element with attribute
        List elements = getNodes(elm, path);
        if (elements != null)
        {
            for (int i = elements.size() - 1; !found && i >= 0; i--)
            {
                Element element = (Element) elements.get(i);
                if (element != null)
                {
                    // get attribute
                    if (pathAttr.length > 1)
                    {
                        String attr = pathAttr[1];

                        String attrValue = element.attributeValue(attr);
                        if (!StringUtils.isEmpty(attrValue))
                        {
                            result = attrValue;
                            found = true;
                        }
                    }

                    // get text
                    else
                    {
                        result = decodeSpecial(element.getText());
                        found = true;
                    }
                }
            }
        }

        return result;
    } // getStringValue

    public void setStringValue(String xpath, String value)
    {
        setStringValue(root, xpath, value);
    } // setStringValue

    public void setStringValue(Element elm, String xpath, String value)
    {
        // initialize values
        String[] pathAttr = getXPathPathAttr(xpath);
        String path = pathAttr[0];

        Element element = null;

        // find target element
        List elements = getNodes(elm, path);
        if (elements == null || elements.size() == 0)
        {
            // add new element
            element = createPath(elm, path);
        }
        else
        {
            boolean found = false;

            // find latest element with attribute
            for (int i = elements.size() - 1; !found && i >= 0; i--)
            {
                element = (Element) elements.get(i);
                if (element != null)
                {
                    // set attribute
                    if (pathAttr.length > 1)
                    {
                        String attr = pathAttr[1];

                        String attrValue = element.attributeValue(attr);
                        if (!StringUtils.isEmpty(attrValue))
                        {
                            found = true;
                        }
                    }
                }
            }

            // if not found, add attr value to last element
            if (!found)
            {
                element = (Element) elements.get(elements.size() - 1);
            }
        }

        // set value
        if (element != null)
        {
            if (pathAttr.length > 1)
            {
                addAttribute(element, pathAttr[1], value);
            }
            // set text
            else
            {
                element.setText(encodeSpecial(value));
            }
        }
    } // setStringValue

    public void addStringValue(String xpath, String value)
    {
        addStringValue(root, xpath, value);
    } // addStringValue

    public void addStringValue(Element elm, String xpath, String value)
    {
        // initialize values
        String[] pathAttr = getXPathPathAttr(xpath);
        String path = pathAttr[0];

        // find target element
        Element element = (Element) getLastNode(elm, path);
        if (element == null)
        {
            // add new element
            element = createPath(elm, path);
            if (pathAttr.length > 1)
            {
                addAttribute(element, pathAttr[1], value);
            }
            // set text
            else
            {
                element.setText(encodeSpecial(value));
            }
        }
        else
        {
            Element parent = element.getParent();
            if (parent == null)
            {
                parent = root;
            }
            element = parent.addElement(element.getQualifiedName());
            if (pathAttr.length > 1)
            {
                addAttribute(element, pathAttr[1], value);
            }
            // set text
            else
            {
                element.setText(encodeSpecial(value));
            }
        }
    } // addStringValue

    public void setCDataValue(Element elm, String path, String value)
    {
        // initialize values
        Element element = null;

        // find target element
        List elements = getNodes(elm, path);
        if (elements == null || elements.size() == 0)
        {
            // add new element
            element = createPath(elm, path);
        }
        else
        {
            element = (Element) elements.get(elements.size() - 1);
        }

        // set value
        if (element != null)
        {
            element.addCDATA(value);
        }
    } // setCDataValue

    /**
     * parameter xpath ==> <author> <![CDATA[
     * <p>
     * Paragraph 1
     * </p>
     * <p>
     * Paragraph 2 <a href="lnk.html">lnk</a>
     * </p>
     * ]]> <firstname>Marcello</firstname> <lastname>Vitaletti</lastname>
     * </author>
     *
     * will return
     * <p>
     * Paragraph 1
     * </p>
     * <p>
     * Paragraph 2 <a href="lnk.html">lnk</a>
     * </p>
     *
     * @param xpath
     * @return
     */
    public String getCDataValue(String xpath)
    {
        String cdata = "";
        String[] pathAttr = getXPathPathAttr(xpath);
        String path = pathAttr[0];

        Element elem = (Element) getNode(path);

        return getCDataValue(elem);

    } // getCDatavalue

    public static String getCDataValue(Element element)
    {
        String cdata = "";

        // for cdata
        try
        {
            List contentList = element.content();
            for (int k = 0; k < contentList.size(); k++)
            {
                if ((contentList.get(k)) instanceof DefaultCDATA)
                {
                    DefaultCDATA dc = (DefaultCDATA) contentList.get(k);
                    cdata = dc.getText();
                    break;
                }
            }
        }
        catch (Throwable t)
        {
            // do nothing
        }

        // if its empty
        if (StringUtils.isEmpty(cdata))
        {
            cdata = element.getText();
        }

        return cdata;
    }// getCDataValue

    public boolean getBooleanValue(String xpath)
    {
        return getBooleanValue(root, xpath, false);
    } // getBooleanValue

    public boolean getBooleanValue(String xpath, boolean defaultValue)
    {
        return getBooleanValue(root, xpath, defaultValue);
    } // getBooleanValue

    public boolean getBooleanValue(Element elm, String xpath, boolean defaultValue)
    {
        boolean result = defaultValue;

        // initialise default value parameter for getProperty
        String stringDefaultValue;
        if (defaultValue == true)
        {
            stringDefaultValue = "TRUE";
        }
        else
        {
            stringDefaultValue = "FALSE";
        }

        // get property from configuration XML dom
        String value = getStringValue(elm, xpath, stringDefaultValue);

        // process result value
        if (value != null)
        {
            if (value.equalsIgnoreCase("false"))
            {
                result = false;
            }
            else if (value.equals(""))
            {
                result = false;
            }
            else
            {
                result = true;
            }
        }

        return result;
    } // getBooleanValue

    public void setBooleanValue(String xpath, boolean value)
    {
        setBooleanValue(root, xpath, value);
    }

    public void setBooleanValue(Element elm, String xpath, boolean value)
    {
        setStringValue(elm, xpath, "" + value);
    } // setBooleanValue

    public int getIntValue(String xpath)
    {
        return getIntValue(root, xpath, 0);
    } // getIntValue

    public int getIntValue(String xpath, int defaultValue)
    {
        return getIntValue(root, xpath, defaultValue);
    } // getIntValue

    public int getIntValue(Element elm, String xpath, int defaultValue)
    {
        int result = defaultValue;

        String stringDefaultValue = "" + defaultValue;

        String value = getStringValue(elm, xpath, stringDefaultValue);
        result = Integer.parseInt(value);
        return result;
    } // getIntValue

    public void setIntValue(String xpath, int value)
    {
        setIntValue(root, xpath, value);
    }

    public void setIntValue(Element elm, String xpath, int value)
    {
        setStringValue(elm, xpath, "" + value);
    } // setIntValue

    public long getLongValue(String xpath)
    {
        return getLongValue(root, xpath, 0);
    } // getLongValue

    public long getLongValue(String xpath, long defaultValue)
    {
        return getLongValue(root, xpath, defaultValue);
    } // getLongValue

    public long getLongValue(Element elm, String xpath, long defaultValue)
    {
        long result = defaultValue;

        String stringDefaultValue = "" + defaultValue;

        String value = getStringValue(elm, xpath, stringDefaultValue);
        result = Long.parseLong(value);
        return result;
    } // getLongValue

    public void setLongValue(String xpath, long value)
    {
        setLongValue(root, xpath, value);
    } // setLongValue

    public void setLongValue(Element elm, String xpath, long value)
    {
        setStringValue(elm, xpath, "" + value);
    } // setLongValue

    public List getListValue(String xpath)
    {
        return getListValue(root, xpath, null);
    } // getListValue

    public List getListValue(String xpath, List defaultValues)
    {
        return getListValue(root, xpath, defaultValues);
    } // getListValue

    public List getListValue(Element elm, String xpath, List defaultValues)
    {
        List result = null;

        if (defaultValues != null)
        {
            result = new Vector(defaultValues);
        }
        else
        {
            result = new Vector();
        }

        // initialize values
        String[] pathAttr = getXPathPathAttr(xpath);
        String path = pathAttr[0];

        // find latest element with attribute
        List elements = getNodes(elm, path);
        if (elements != null)
        {
            for (Iterator i = elements.iterator(); i.hasNext();)
            {
                Element element = (Element) i.next();
                if (element != null)
                {
                    // get attribute
                    if (pathAttr.length > 1)
                    {
                        String attr = pathAttr[1];

                        String attrValue = element.attributeValue(attr);
                        if (!StringUtils.isEmpty(attrValue))
                        {
                            result.add(attrValue);
                        }
                    }

                    // get text
                    else
                    {
                        result.add(decodeSpecial(element.getText()));
                    }
                }
            }
        }

        return result;
    } // getListValue

    public void setListValue(String xpath, List values)
    {
        setListValue(root, xpath, values);
    } // setListValue

    public void setListValue(Element elm, String xpath, List values)
    {
        removeElements(elm, xpath);
        for (Iterator i = values.iterator(); i.hasNext();)
        {
            String value = (String) i.next();
            addStringValue(elm, xpath, value);
        }
    } // setListValue

    public void setListValue(Element elm, String xpath, String[] values)
    {
        removeElements(elm, xpath);
        for (int i = 0; i < values.length; i++)
        {
            String value = values[i];
            addStringValue(elm, xpath, value);
        }
    } // setListValue

    public void addListValue(String xpath, List values)
    {
        addListValue(root, xpath, values);
    } // addListValue

    public void addListValue(Element elm, String xpath, List values)
    {
        for (Iterator i = values.iterator(); i.hasNext();)
        {
            String value = (String) i.next();
            addStringValue(elm, xpath, value);
        }
    } // addListValue

    public Map getMapValue(String xpath)
    {
        return getMapValue(root, xpath, null);
    } // getMapValue

    public Map getMapValue(String xpath, Map defaultValue)
    {
        return getMapValue(root, xpath, defaultValue);
    } // getMapValue

    public Map getMapValue(Element elm, String xpath, Map defaultValue)
    {
        Map result = null;

        if (defaultValue != null)
        {
            result = new Hashtable(defaultValue);
        }
        else
        {
            result = new Hashtable();
        }

        // goto / create the correct element path
        Element element = (Element) getLastNode(elm, xpath);
        if (element != null)
        {
            for (Iterator i = element.attributeIterator(); i.hasNext();)
            {
                Attribute attr = (Attribute) i.next();
                result.put(attr.getName().toUpperCase(), attr.getValue());
            }
        }

        return result;
    } // getMapValue

    public void setMapValue(String xpath, Map values)
    {
        setMapValue(root, xpath, values);
    } // setMapValue

    public void setMapValue(Element elm, String xpath, Map values)
    {
        // find elements
        Element element = (Element) getLastNode(elm, xpath);
        if (element == null)
        {
            element = createPath(elm, xpath);
            for (Iterator validx = values.entrySet().iterator(); validx.hasNext();)
            {
                Map.Entry entry = (Map.Entry) validx.next();
                if (entry.getKey() != null)
                {
                    String value = "";
                    if (entry.getValue() != null)
                    {
                        value = entry.getValue().toString();
                    }
                    addAttribute(element, ((String) entry.getKey().toString()), value);
                }
            }
        }
        else
        {
            for (Iterator validx = values.entrySet().iterator(); validx.hasNext();)
            {
                Map.Entry entry = (Map.Entry) validx.next();
                if (entry.getKey() != null)
                {
                    String value = "";
                    if (entry.getValue() != null)
                    {
                        value = entry.getValue().toString();
                    }
                    addAttribute(element, ((String) entry.getKey().toString()), value);
                }
            }
        }
    } // setMapValue

    public void addMapValue(String xpath, Map values)
    {
        addMapValue(root, xpath, values);
    } // addMapValue

    public void addMapValue(Element elm, String xpath, Map values)
    {
        // find elements
        Element element = (Element) getLastNode(elm, xpath);
        if (element == null)
        {
            element = createPath(elm, xpath);

            for (Iterator validx = values.entrySet().iterator(); validx.hasNext();)
            {
                Map.Entry entry = (Map.Entry) validx.next();
                if (entry.getKey() != null)
                {
                    String value = "";
                    if (entry.getValue() != null)
                    {
                        value = entry.getValue().toString();
                    }
                    addAttribute(element, (String) entry.getKey().toString(), value);
                }
            }
        }
        else
        {
            Element parent = element.getParent();
            if (parent == null)
            {
                parent = root;
            }
            element = parent.addElement(element.getQualifiedName());
            for (Iterator validx = values.entrySet().iterator(); validx.hasNext();)
            {
                Map.Entry entry = (Map.Entry) validx.next();
                if (entry.getKey() != null)
                {
                    String value = "";
                    if (entry.getValue() != null)
                    {
                        value = entry.getValue().toString();
                    }
                    addAttribute(element, (String) entry.getKey().toString(), value);
                }
            }
        }
    } // addMapValue

    public List getListMapValue(String xpath)
    {
        return getListMapValue(root, xpath, null);
    } // getListMapValue

    public List getListMapValue(String xpath, Map defaultValue)
    {
        return getListMapValue(root, xpath, defaultValue);
    } // getListMapValue

    public List getListMapValue(Element elm, String xpath, Map defaultValue)
    {
        List result = new Vector();

        List children = getNodes(elm, xpath);
        if (children != null)
        {
            for (Iterator elmidx = children.listIterator(); elmidx.hasNext();)
            {
                Element childElement = (Element) elmidx.next();
                Hashtable childValue;
                if (defaultValue != null)
                {
                    childValue = new Hashtable(defaultValue);
                }
                else
                {
                    childValue = new Hashtable();
                }

                for (Iterator attridx = childElement.attributeIterator(); attridx.hasNext();)
                {
                    Attribute attr = (Attribute) attridx.next();
                    childValue.put(attr.getName().toUpperCase(), attr.getValue());
                }
                result.add(childValue);
            }
        }

        return result;
    } // getListMapValue

    public void setListMapValue(String xpath, List values)
    {
        setListMapValue(root, xpath, values);
    } // setListMapValue

    public void setListMapValue(Element elm, String xpath, List values)
    {
        removeElements(elm, xpath);
        for (Iterator i = values.iterator(); i.hasNext();)
        {
            Map valuemap = (Map) i.next();
            addMapValue(elm, xpath, valuemap);
        }
    } // setListMapValue

    public XDoc getXMLValue(String xpath) throws Exception
    {
        return getXMLValue(root, xpath, null);
    } // getXMLValue

    public XDoc getXMLValue(String xpath, XDoc defaultValue) throws Exception
    {
        return getXMLValue(root, xpath, defaultValue);
    } // getXMLValue

    public XDoc getXMLValue(Element elm, String xpath, XDoc defaultValue) throws Exception
    {
        XDoc result = defaultValue;

        // initialize values
        String[] pathAttr = getXPathPathAttr(xpath);
        String path = pathAttr[0];

        // find latest element with attribute
        Element element = (Element) getNode(elm, path);
        if (element != null)
        {
            result = new XDoc();
            result.addRoot(element);
        }

        return result;
    } // getXMLValue

    public void setXMLValue(String xpath, XDoc value)
    {
        setXMLValue(root, xpath, value);
    } // setXMLValue

    public void setXMLValue(Element elm, String xpath, XDoc value)
    {
        // initialize values
        String[] pathAttr = getXPathPathAttr(xpath);
        String path = pathAttr[0];

        // find latest element with attribute
        Element element = (Element) getNode(elm, path);
        if (element == null)
        {
            // add new element
            element = createPath(elm, path);
        }

        Element parent = element.getParent();
        parent.add((Element) value.getRoot().clone());
        element.detach();
    } // setXMLValue

    public void addXMLValue(String xpath, XDoc value)
    {
        addXMLValue(root, xpath, value);
    } // addXMLValue

    public void addXMLValue(Element elm, String xpath, XDoc value)
    {
        // initialize values
        String[] pathAttr = getXPathPathAttr(xpath);
        String path = pathAttr[0];

        // find latest element with attribute
        Element element = (Element) getNode(elm, path);
        if (element == null)
        {
            // add new element
            element = createPath(elm, path);
        }

        Element parent = element.getParent();
        parent.add((Element) value.getRoot().clone());
    } // addXMLValue

    public void insertXMLValue(String xpath, XDoc value)
    {
        insertXMLValue(root, xpath, value);
    } // insertXMLValue

    public void insertXMLValue(Element elm, String xpath, XDoc value)
    {
        // initialize values
        String[] pathAttr = getXPathPathAttr(xpath);
        String path = pathAttr[0];

        // find latest element with attribute
        Element element = (Element) getNode(elm, path);
        if (element == null)
        {
            // add new element
            element = createPath(elm, path);
        }

        element.add((Element) value.getRoot().clone());
    } // insertXMLValue

    public boolean existsCNSValue(String type, String xpath, Object defaultValue) throws Exception
    {
        boolean result = false;

        if (getCNSValue(type, xpath, defaultValue) != null)
        {
            result = true;
        }
        return result;
    } // existsCNSValue

    public boolean existsCNSValue(Element elm, String type, String xpath, Object defaultValue) throws Exception
    {
        boolean result = false;

        if (getCNSValue(elm, type, xpath, defaultValue) != null)
        {
            result = true;
        }
        return result;
    } // existsCNSValue

    public Object getCNSValue(String type, String xpath, Object defaultValue) throws Exception
    {
        return getCNSValue(root, type, xpath, defaultValue);
    } // getCNSValue

//    public Object getCNSValue(Element elm, String utype, String xpath, Object defaultValue) throws Exception
//    {
//        return getCNSValue(elm, utype, xpath, defaultValue);
//    } // getCNSValue

    public Object getCNSValue(Element elm, String type, String xpath, Object defaultValue) throws Exception
    {
        Object result = null;

//        String utype = name.getUType();
//        String xpath = name.getUXpath();
//        String defaultValue = name.ugetDefaultValue();


//        if (name != null)
//        {
            if (type.equalsIgnoreCase("STRING") || type.equalsIgnoreCase("RAW"))
            {
                result = getStringValue(elm, xpath, (String) defaultValue);
            }
            else if (type.equalsIgnoreCase("NATIVE"))
            {
                result = elm.valueOf(xpath);
            }
            else if (type.equalsIgnoreCase("LIST"))
            {
                result = getListValue(elm, xpath, null);
            }
            else if (type.equalsIgnoreCase("MAP"))
            {
                result = getMapValue(elm, xpath, null);
            }
            else if (type.equalsIgnoreCase("LISTMAP"))
            {
                result = getListMapValue(elm, xpath, null);
            }
            else if (type.equalsIgnoreCase("XML"))
            {
                result = getXMLValue(elm, xpath, null);
            }
            else
            {
                log.warn("Invalid value type: " + type);
            }
//        }

        return result;
    } // getCNSValue

    public List getCNSValues(String xpath) throws Exception
    {
        return getCNSValues(root, xpath);
    } // getCNSValues

    public List getCNSValues(Element elm, String xpath) throws Exception
    {
        Vector result = new Vector();

        // initialize values
        String[] pathAttr = getXPathPathAttr(xpath);
        String path = pathAttr[0];

        // find latest element with attribute
        List elements = getNodes(elm, path);
        if (elements != null)
        {
            for (Iterator i = elements.iterator(); i.hasNext();)
            {
                Element element = (Element) i.next();
                if (element != null)
                {
                    // get attribute
                    if (pathAttr.length > 1)
                    {
                        String attr = pathAttr[1];

                        String attrValue = element.attributeValue(attr);
                        if (!StringUtils.isEmpty(attrValue))
                        {
                            result.add(attrValue);
                        }
                    }

                    // get text
                    else
                    {
                        result.add(decodeSpecial(element.getText()));
                    }
                }
            }
        }

        return result;
    } // getCNSValues

    public void setCNSValue(String prefix, String module, String xpath, String type, Object value) throws Exception
    {
        setCNSValue(root, prefix, module, xpath, type, value);
    } // setCNSValue

//    public void setCNSValue(Element elm, ResolveCnsName name, Object value) throws Exception
//    {
//        setCNSValue(elm, name, name.getUType(), value);
//    } // setCNSValue

    public void setCNSValue(Element elm, String prefix, String module, String xpath, String type, Object value)
    {
        // add namespace to root
        if (root.getNamespaceForPrefix(prefix) == null)
        {
            root.addNamespace(prefix, module);
        }

        if (StringUtils.isNotEmpty(type))
        {
            if (type.equalsIgnoreCase("STRING") || type.equalsIgnoreCase("RAW"))
            {
                setStringValue(elm, xpath, (String) value);
            }
            else if (type.equalsIgnoreCase("LIST"))
            {
                if (value instanceof List)
                {
                    setListValue(elm, xpath, (List) value);
                }
                else
                {
                    setListValue(elm, xpath, (String[]) value);
                }
            }
            else if (type.equalsIgnoreCase("MAP"))
            {
                setMapValue(elm, xpath, (Map) value);
            }
            else if (type.equalsIgnoreCase("LISTMAP"))
            {
                setListMapValue(elm, xpath, (List) value);
            }
            else if (type.equalsIgnoreCase("XML"))
            {
                setXMLValue(elm, xpath, (XDoc) value);
            }
            else
            {
                log.warn("Invalid value type: " + type);
            }
        }
    } // setCNSName

    public void addCNSValue(String prefix, String module, String xpath, String type, Object value)
    {
        addCNSValue(root, prefix, module, xpath, type, value);
    } // addCNSValue

//    public void addCNSValue(Element elm, ResolveCnsName name, Object value)
//    {
//        addCNSValue(elm, name, name.getUType(), value);
//    } // addCNSValue

    public void addCNSValue(Element elm, String prefix, String module, String xpath, String type, Object value)
    {
        // add namespace to root
        if (root.getNamespaceForPrefix(prefix) == null)
        {
            root.addNamespace(prefix, module);
        }

        if (StringUtils.isNotEmpty(type))
        {
            if (type.equalsIgnoreCase("STRING") || type.equalsIgnoreCase("RAW"))
            {
                addStringValue(elm, xpath, (String) value);
            }
            else if (type.equalsIgnoreCase("LIST"))
            {
                addListValue(elm, xpath, (List) value);
            }
            else if (type.equalsIgnoreCase("MAP"))
            {
                addMapValue(elm, xpath, (Map) value);
            }
            else if (type.equalsIgnoreCase("XML"))
            {
                addXMLValue(elm, xpath, (XDoc) value);
            }
            else
            {
                log.warn("Invalid value type: " + type);
            }
        }
    } // addCNSName

    public void insertCNSValue(String prefix, String module, String xpath, String type, Object value)
    {
        insertCNSValue(root, prefix, module, xpath, type, value);
    } // insertCNSValue

//    public void insertCNSValue(Element elm, ResolveCnsName name, Object value)
//    {
//        insertCNSValue(elm, name, name.getUType(), value);
//    } // insertCNSValue

    public void insertCNSValue(Element elm, String prefix, String module, String xpath, String type, Object value)
    {
        // add namespace to root
        if (root.getNamespaceForPrefix(prefix) == null)
        {
            root.addNamespace(prefix, module);
        }

        if (StringUtils.isNotEmpty(type) && type.equalsIgnoreCase("XML"))
        {
            insertXMLValue(elm, xpath, (XDoc) value);
        }
        else
        {
            log.warn("Invalid value type: " + type);
        }
    } // insertCNSName

    public void removeCNSValues(String xpath)
    {
        removeCNSValues(root, xpath);
    } // removeCNSValues

    public void removeCNSValues(Element elm, String xpath)
    {
        removeElements(elm, xpath);
    } // removeCNSValues

    /*
     * Encode string value before element.setText()
     */
    public String encodeSpecial(String value)
    {
        String result = value;

        result = result.replace(System.getProperty("line.separator"), "&#13;");
        result = result.replace("\n", "&#13;");
        result = result.replace("\r", "&#13;");

        return result;
    } // encodeSpecial

    /*
     * Decode string value before element.getText()
     */
    public String decodeSpecial(String value)
    {
        String result = value;

        result = result.replace("&amp;#13;", System.getProperty("line.separator"));

        return result;
    } // decodeSpecial

    /*
     * translate special for file output
     */
    public String translateSpecial(String value)
    {
        String result = value;

        result = result.replace("&amp;#13;", "&#13;");

        return result;
    } // translateSpecial

    public String cleanup(String xml)
    {
        String result = xml;
        String illegalCharacters = "[^\u0009\n\r\u0020-\uD7FF\uE000-\uFFFD\u10000-\u10FFFF]";

        // remove illegal form-feed - html unused chars - refer
        // http://www.obkb.com/dcljr/charstxt.html
        result = result.replaceAll("&#[0-8];", "");
        result = result.replaceAll("&#0[0-8];", "");
        result = result.replaceAll("&#1[124-9];", "");
        result = result.replaceAll("&#2[0-9];", "");
        result = result.replaceAll("&#3[0-1];", "");

        result = result.replaceAll("&#12[7-9];", "");
        result = result.replaceAll("&#13[0-9];", "");
        result = result.replaceAll("&#14[0-9];", "");
        result = result.replaceAll("&#15[0-9];", "");

        // remove illegal characters
        Pattern pattern = Pattern.compile(illegalCharacters);
        Matcher matcher = pattern.matcher(result);
        if (matcher.find())
        {
            log.error("Removing illegal Characters from xml");
            log.debug(result);
            result = result.replaceAll(illegalCharacters, "");
        }

        return result;
    } // cleanup

    private Element addAttribute(Element element, String key, String value)
    {
        // check if key is a number
        boolean isNumber = false;

        try
        {
            Long.parseLong(key);
            isNumber = true;

            log.warn("Number is not permitted for an element attribute", new Exception());
        }
        catch (Exception e)
        {
            isNumber = false;
        }

        if (isNumber == false)
        {
            element.addAttribute(key, value);
        }
        return element;
    } // addAttribute
    
    public static void setLogger(Logger logger)
    {
        log = logger;
    }//setLogger

} // XDoc
