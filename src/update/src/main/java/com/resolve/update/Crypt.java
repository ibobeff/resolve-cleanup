/******************************************************************************
* (C) Copyright 2014
*
* Resolve Systems, LLC
*
* All rights reserved
* This software is distributed under license and may not be used, copied,
* modified, or distributed without the express written permission of
* Resolve Systems, LLC.
*
******************************************************************************/

package com.resolve.update;

import java.io.UnsupportedEncodingException;
import java.security.InvalidKeyException;
import java.security.spec.KeySpec;
import java.util.Date;

import javax.crypto.Cipher;
import javax.crypto.KeyGenerator;
import javax.crypto.SecretKey;
import javax.crypto.SecretKeyFactory;
import javax.crypto.spec.DESKeySpec;
import javax.crypto.spec.DESedeKeySpec;
import javax.crypto.spec.SecretKeySpec;

import org.apache.commons.codec.binary.Base64;

public final class Crypt
{
    public static final String ENCRYPTION_SCHEME_DESEDE = "DESede";
    public static final String ENCRYPTION_SCHEME_DES = "DES"; // NOTE: For DES,
                                                              // only first 24
                                                              // characters of
                                                              // key is unique
    public static final String ENCRYPTION_SCHEME_AES128 = "AES";
    // public static final String ENCRYPTION_SCHEME_AES256 = "AES";

    private static final String UNICODE_FORMAT = "UTF-8";
    private static final String DEFAULT_ENCRYPTION_KEY = "This is a fairly long phrase used to encrypt";
    private static final String encDataLocal = "ENC:XgAkfIoSYCVNis5EwVkQCakKjRroa8Gz6qZ6NTfxm4o=";
    private static byte[] keyBytesLocal = null;

    boolean keyInitialized = false;
    private String keyBase = null;
    private String scheme;
    private KeySpec keySpec;
    private SecretKeyFactory keyFactory;
    private KeyGenerator kgen;
    private SecretKeySpec skeySpec;
    private Cipher cipher;

    public Crypt(String scheme) throws Exception
    {
        setScheme(scheme);
    } // Crypt

    public Crypt(String scheme, String key) throws Exception
    {
        setScheme(scheme);
        setKeyBase(key);
    } // Crypt

    private void setScheme(String scheme) throws Exception
    {
        this.scheme = scheme;

        if (scheme != null && (scheme.equals(ENCRYPTION_SCHEME_DESEDE) || scheme.equals(ENCRYPTION_SCHEME_DES)))
        {
            // init secret key factory
            keyFactory = SecretKeyFactory.getInstance(scheme);
        }
        else if (scheme != null && scheme.equals(ENCRYPTION_SCHEME_AES128))
        {
            kgen = KeyGenerator.getInstance(ENCRYPTION_SCHEME_AES128);
            kgen.init(128);
            // kgen.init(256);

            if (keyBytesLocal == null)
            {
                // keyBytesLocal = kgen.generateKey().getEncoded();
                System.out.println("Terminate initiated");
                System.exit(0);
            }
        }

        cipher = Cipher.getInstance(scheme);

    } // setScheme

    public static String getEncDataLocal()
    {
        return encDataLocal;
    }

    public static String getENCData()
    {
        String encData = "";

        if (keyBytesLocal != null && keyBytesLocal.length > 0)
        {
            // Base64 base64encoder = new Base64();
            encData = new String(Base64.encodeBase64(keyBytesLocal)); // converted
                                                                      // to
                                                                      // apache
                                                                      // encoding
        }

        return encData;
    }

    public static void setENDData(String encData) throws Exception
    {
        // Base64 base64decoder = new Base64();
        keyBytesLocal = Base64.decodeBase64(encData); // converted to apache
                                                      // encoding
    }

    public void setKeyBase(String keyBase)
    {
        this.keyBase = keyBase;
    } // setKeyBase

    public void setKey(String keyBase)
    {
        this.keyBase = keyBase;
    } // setKey

    public void initKeySession(String keySess) throws UnsupportedEncodingException, InvalidKeyException
    {
        if (keyBase == null)
        {
            setKeyBase(DEFAULT_ENCRYPTION_KEY);
        }

        if (keySess == null)
        {
            keySess = "";
        }

        String key = keySess + this.keyBase;
        byte[] keyAsBytes = key.getBytes(UNICODE_FORMAT);

        if (scheme.equals(ENCRYPTION_SCHEME_DESEDE))
        {
            keySpec = new DESedeKeySpec(keyAsBytes);
        }
        else if (scheme.equals(ENCRYPTION_SCHEME_DES))
        {
            keySpec = new DESKeySpec(keyAsBytes);
        }
        else if (scheme.equals(ENCRYPTION_SCHEME_AES128))
        {
            skeySpec = new SecretKeySpec(keyBytesLocal, "AES");
        }
        else
        {
            throw new IllegalArgumentException("Encryption scheme not supported: " + scheme);
        }

        keyInitialized = true;

    } // initKey

    public void setKeyTimestamp(Date timestamp) throws UnsupportedEncodingException, InvalidKeyException
    {
        setKeyTimestamp(timestamp.getTime());
    } // setKey

    public void setKeyTimestamp(long timestamp) throws UnsupportedEncodingException, InvalidKeyException
    {
        initKeySession("" + timestamp);
    } // setKey

    // TODO - added synchronize to avoid JVM core dump until we upgrade to AES
    // 128
    public synchronized String encrypt(String unencryptedString) throws Exception
    {
        String result = "";

        if (unencryptedString != null && unencryptedString.trim().length() > 0)
        {
            if (keyInitialized == false)
            {
                initKeySession(null);
            }

            SecretKey key = keyFactory.generateSecret(keySpec);
            cipher.init(Cipher.ENCRYPT_MODE, key);
            byte[] cleartext = unencryptedString.getBytes(UNICODE_FORMAT);
            byte[] ciphertext = cipher.doFinal(cleartext);

            // Base64 base64encoder = new Base64();
            result = new String(Base64.encodeBase64(ciphertext)); // converted
                                                                  // to apache
                                                                  // encoding
        }
        return result;
    } // encrypt

    public synchronized String encryptAES(String unencryptedString) throws Exception
    {
        String result = "";

        if (unencryptedString != null && unencryptedString.trim().length() > 0)
        {
            if (keyInitialized == false)
            {
                initKeySession(null);
            }

            cipher.init(Cipher.ENCRYPT_MODE, skeySpec);
            byte[] cleartext = unencryptedString.getBytes(UNICODE_FORMAT);
            byte[] ciphertext = cipher.doFinal(cleartext);

            // Base64 base64encoder = new Base64();
            result = new String(Base64.encodeBase64(ciphertext)); // converted
                                                                  // to apache
                                                                  // encoding
        }
        return result;
    } // encryptAES

    public synchronized String decrypt(String encryptedString) throws Exception
    {
        String result = "";

        if (encryptedString != null && encryptedString.trim().length() > 0)
        {
            if (keyInitialized == false)
            {
                initKeySession(null);
            }

            SecretKey key = keyFactory.generateSecret(keySpec);
            cipher.init(Cipher.DECRYPT_MODE, key);
            // Base64 base64decoder = new Base64();
            byte[] cleartext = Base64.decodeBase64(encryptedString);
            byte[] ciphertext = cipher.doFinal(cleartext);

            result = bytes2String(ciphertext);
        }
        return result;
    } // decrypt

    public synchronized String decryptUTF8(String encryptedString) throws Exception
    {
        String result = "";

        if (encryptedString != null && encryptedString.trim().length() > 0)
        {
            if (keyInitialized == false)
            {
                initKeySession(null);
            }

            SecretKey key = keyFactory.generateSecret(keySpec);
            cipher.init(Cipher.DECRYPT_MODE, key);
            // Base64 base64decoder = new Base64();
            byte[] cleartext = Base64.decodeBase64(encryptedString);
            byte[] ciphertext = cipher.doFinal(cleartext);

            result = new String(ciphertext, "UTF-8");
        }
        return result;
    } // decrypt

    public synchronized String decryptAES128(String encryptedString) throws Exception
    {
        String result = "";

        if (encryptedString != null && encryptedString.trim().length() > 0)
        {
            if (keyInitialized == false)
            {
                initKeySession(null);
            }

            cipher.init(Cipher.DECRYPT_MODE, skeySpec);
            // Base64 base64decoder = new Base64();
            byte[] cleartext = Base64.decodeBase64(encryptedString); // converted
                                                                     // to
                                                                     // apache
                                                                     // encoding
            byte[] ciphertext = cipher.doFinal(cleartext);

            result = bytes2String(ciphertext);
        }

        return result;

    } // decryptAES128

    public synchronized String decryptAES128UTF8(String encryptedString) throws Exception
    {
        String result = "";

        if (encryptedString != null && encryptedString.trim().length() > 0)
        {
            if (keyInitialized == false)
            {
                initKeySession(null);
            }

            cipher.init(Cipher.DECRYPT_MODE, skeySpec);
            // Base64 base64decoder = new Base64();
            byte[] cleartext = Base64.decodeBase64(encryptedString); // converted
                                                                     // to
                                                                     // apache
                                                                     // encoding
            byte[] ciphertext = cipher.doFinal(cleartext);

            result = new String(ciphertext, "UTF-8"); // bytes2String(
                                                      // ciphertext );
        }

        return result;

    } // decryptAES128

    private static String bytes2String(byte[] bytes)
    {
        StringBuffer stringBuffer = new StringBuffer();
        for (int i = 0; i < bytes.length; i++)
        {
            stringBuffer.append((char) bytes[i]);
        }
        return stringBuffer.toString();
    } // bytes2String

} // Crypt