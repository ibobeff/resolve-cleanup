/******************************************************************************
* (C) Copyright 2014
*
* Resolve Systems, LLC
*
* All rights reserved
* This software is distributed under license and may not be used, copied,
* modified, or distributed without the express written permission of
* Resolve Systems, LLC.
*
******************************************************************************/

package com.resolve.update;

import groovy.lang.Binding;
import groovy.lang.GroovyClassLoader;
import groovy.lang.GroovyCodeSource;
import groovy.lang.Script;

import org.apache.commons.lang3.StringUtils;
import org.apache.log4j.Logger;

public class GroovyScript
{
    public static Logger log;
    
    public static Object execute(String script, String scriptName, Binding binding) throws Exception
    {
        Object[] args = {};
        Object result = null;

        Script groovyScript = getGroovyScript(script, scriptName);

        // execute script
        if (groovyScript != null)
        {
            try
            {
                if (binding != null)
                {
                    groovyScript.setBinding(binding);
                }

                result = groovyScript.invokeMethod("run", args);
            }
            finally
            {
                // remove binding from cached groovyScript object
                groovyScript.setBinding(null);
            }
        }

        return result;
    } // execute

    public static Script getGroovyScript(String script, String scriptName) throws IllegalAccessException, InstantiationException
    {
        Script result = null;

        if (!StringUtils.isEmpty(script))
        {
            Class groovyScriptClass = null;
            // load groovy class and execute instance
            ClassLoader parent = GroovyScript.class.getClassLoader();
            GroovyClassLoader loader = new GroovyClassLoader(parent);

            // prepend script with default imports
            if (StringUtils.isEmpty(scriptName))
            {
                scriptName = "Groovy_" + System.currentTimeMillis()+".groovy";
            }
            else
            {
                // init scriptClassName
                scriptName = "Groovy_"+stringToIdentifier(scriptName);
                if (scriptName.endsWith("_groovy"))
                {
                    scriptName = scriptName.replace("_groovy", ".groovy");
                }
                else
                {
                    scriptName += ".groovy";
                }
            }

            // parse scripts
            GroovyCodeSource codeSource = new GroovyCodeSource(script, scriptName, "/GroovyScript");
            codeSource.setCachable(false);
            groovyScriptClass = loader.parseClass(codeSource, false);

            // remove class from GroovyClassLoader, use our cache instead
            loader.clearCache();
                
            result = (Script) groovyScriptClass.newInstance();
        }
        else
        {
            log.error("Groovy Script content missing - script: " + script);
        }

        return result;
    } // getGroovyScript

    private static String stringToIdentifier(String str)
    {
        StringBuilder sb = new StringBuilder();
        if (!Character.isJavaIdentifierStart(str.charAt(0)))
        {
            sb.append("_");
        }
        
        for (char c : str.toCharArray())
        {
            if (!Character.isJavaIdentifierPart(c))
            {
                sb.append("_");
            }
            else
            {
                sb.append(c);
            }
        }

        return sb.toString();
    } // stringToIdentifier
    
    public static void setLogger(Logger logger)
    {
        log = logger;
    } // setLogger

} // GroovyScript
