import com.resolve.update.Compress;
import com.resolve.update.CryptMD5;
import com.resolve.update.XDoc;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.util.regex.Pattern;
import java.util.regex.Matcher;

import org.apache.commons.io.FileUtils;

def help()
{
    println "Installation Script For The Resolve Upgrade";
}

dist = MAIN.getDist().replaceAll("\\\\", "/");
backupExtension = "new";
force = false;
contUpdate = false;

def result = true;
def compress = new Compress();
def fileList = fileList(new File(dist + "/file"), false);
fileList = fileList.sort(installSort());
def md5FileName = dist + "/file/update.md5";
def excludeBackupFiles = [];
def removeFiles = [];
def postCommands = "";
def newFiles = [];
def buildVersion = "";
def arguments = args;
def os = System.getProperty("os.name");
def arch = System.getProperty("sun.arch.data.model");
def importFiles;
def type = "";
def jreFile = null;

def exceptions = ["ganymed-ssh2-build250.jar", "rsview.jar", "rscontrol.jar", "rsarchive.jar", "rsremote.jar", "rssync.jar", "rsconsole.jar", "rsmgmt.jar"];
def versionPattern = Pattern.compile(/([a-zA-Z0-9.\-_]+?)-((?:\d+[.]?)+.*).jar/);

try
{
    def updateFileName = FILENAME;
    def updateZip = new File(updateFileName);

    LOG.info("Optional Arguments: " + arguments);

    if (!updateZip.exists())
    {
        println "File " + updateZip.getName() + " Does Not Exist";
        LOG.warn("File " + updateZip.getName() + " Does Not Exist");
        result = false;
    }
    else
    {
        println "Applying Update " + updateFileName;
        LOG.info("Applying Update " + updateFileName);
        updateZip = updateZip.getAbsoluteFile();
        def updateFileMatcher = updateFileName =~ /(.+)-(.+).zip/;
        if (updateFileMatcher.find())
        {
            type = updateFileMatcher.group(1);
            buildVersion = updateFileMatcher.group(2);
            backupExtension = buildVersion.replaceAll("\\.", "_");
            md5FileName = dist + "/file/update-" + buildVersion + ".md5";
        }
        else
        {
            println "WARNING - Unable to determine Update version";
            LOG.warn("WARNING - Unable to determine Update version from " + updateFileName);
        }

        if (!arguments.contains("--no-import"))
        {
            LOG.debug("Finding Import Files");
            def rsexpertDir = new File(dist + "/file/rsexpert");
            if (rsexpertDir.isDirectory())
            {
                importFiles = rsexpertDir.listFiles();
            }
            if (importFiles)
            {
                def importArgs = "";
                if (importFiles)
                {
                    exports = importFiles.sort(importSort());
                    for (export in exports)
                    {
                        def exportName = export.getName();
                        if (exportName.endsWith(".zip"))
                        {
                            exportName = exportName.substring(0, exportName.lastIndexOf(".zip"));
                        }
                        if (importArgs)
                        {
                            importArgs += ";" + exportName;
                        }
                        else
                        {
                            importArgs = exportName;
                        }
                        lastImportModule = exportName;
                    }
                    LOG.info("Updated Imports: " + importArgs);
                }
                postCommands += "IMPORT:" + importArgs + "\n";
            }
            else
            {
                LOG.debug("No Update RSExpert Directory Found: " + rsexpertDir.getAbsolutePath());
            }
        }

        for (int i=1; i<arguments.size(); i++)
        {
            def arg = arguments[i];
            if (arg == "--force")
            {
                force = true;
                LOG.warn("Force Update On, will continue with update even if unable to back up current file");
            }
            if (arg == "--continue")
            {
                contUpdate = true;
                LOG.warn("Continue Update On, will continue with update even if unable to back up current file");
            }
        }

        def md5File = new File(md5FileName);
        def excludeBackupFile = new File(dist + "/file/exclude-backup.properties");
        def removePropertiesFile = new File(dist + "/file/remove-file.properties");

        if (!md5File.exists())
        {
            println "Unable to Read MD5 Checksum File, Cancelling Update";
            LOG.warn("MD5 File " + md5File.getAbsolutePath() + " Does Not Exist");
            result = false;
        }
        else
        {
            def md5Bytes = new byte[md5File.length()];
            def fis = new FileInputStream(md5File);
            fis.read(md5Bytes);
            fis.close();

            def md5Str = new String(md5Bytes);
            def md5Sums = [:];
            for (md5 in md5Str.split("\n"))
            {
                def md5Matcher = md5 =~ /(\w+) \*([^\/\\?%*:|"<>]+)/;
                if (md5Matcher.find())
                {
                    def md5Name = md5Matcher.group(2).trim();
                    if (md5Sums[md5Name])
                    {
                        if (md5Sums[md5Name] instanceof String)
                        {
                            md5Sums[md5Name] = [md5Sums[md5Name],md5Matcher.group(1)];
                        }
                        else
                        {
                            md5Sums[md5Name].add(md5Matcher.group(1));
                        }
                    }
                    else
                    {
                        md5Sums[md5Name] = md5Matcher.group(1);
                    }
                }
                else
                {
                    LOG.error("Invalid MD5 Checksum in MD5 Checksum File: " + md5);
                }
            }
            LOG.trace("MD5 Checksums: " + md5Sums);
            //Verify MD5 Checksums
            //////////////////////////MCP Monitored Statement
            println "Verifying MD5 Checksums";
            //////////////////////////MCP Monitored Statement
            for (fileName in fileList)
            {
                if (!fileName.endsWith(".md5") && !fileName.equals("install-update.groovy")
                    && !fileName.endsWith("-new.log") && !fileName.endsWith("update_onetime.log"))
                {
                    def updatedFile = new File(dist + "/file/" + fileName);
                    def updateMD5 = CryptMD5.encrypt(updatedFile);
                    def checkMD5 = md5Sums[updatedFile.getName()]
                    LOG.debug("Verifying MD5 Checksum for " + updatedFile.getAbsolutePath());
                    LOG.debug(updateMD5 + "==" + checkMD5);
                    if (checkMD5 == null || (checkMD5 instanceof String && updateMD5 != checkMD5) ||
                        (checkMD5 instanceof List && !checkMD5.contains(updateMD5)))
                    {
                        println updatedFile.getName() + " Does Not Match MD5 Checksum, Cancelling Update";
                        LOG.warn("File " + updatedFile.getName() + ": " + updateMD5 + "!=" + checkMD5);
                        result = false;
                        break;
                    }
                }
            }

            if (excludeBackupFile.exists())
            {
                def excludeBackupBytes = new byte[excludeBackupFile.length()];
                fis = new FileInputStream(excludeBackupFile);
                fis.read(excludeBackupBytes);
                fis.close();

                def excludeBackupStr = new String(excludeBackupBytes);
                for (file in excludeBackupStr.split("\n"))
                {
                    if (file != null && file.trim())
                    {
                        excludeBackupFiles.add(file.trim());
                    }
                }
                LOG.warn("Exclude Backup List: " + excludeBackupFiles);
            }

            if (removePropertiesFile.exists())
            {
                def removeBytes = new byte[removePropertiesFile.length()];
                fis = new FileInputStream(removePropertiesFile);
                fis.read(removeBytes);
                fis.close();

                def removeStr = new String(removeBytes);
                for (file in removeStr.split("\n"))
                {
                    if (file != null && file.trim())
                    {
                        removeFiles.add(file.trim());
                    }
                }
                LOG.warn("Remove File List: " + removeFiles);
            }

            if (result)
            {
                //////////////////////////MCP Monitored Statement
                println "Updating Files";
                //////////////////////////MCP Monitored Statement
                for (fileName in fileList)
                {
                    def updatedFile = new File(dist + "/file/" + fileName);
                    def file = new File(dist + "/" + fileName);
                    def parent = file.getParentFile();
                    if (!fileName.endsWith(".md5") && !fileName.equals("install-update.groovy") && !fileName.endsWith("-new.log")
                        && !fileName.endsWith("update_onetime.log")
                        && !fileName.equals("update.properties") && !fileName.equals("exclude-backup.properties")
                        && !fileName.equals("remove-file.properties") && !fileName.equals("lock-" + backupExtension)
                        && !fileName.equals("post_commands.txt") && !parent.equals(new File(dist + "/tmp")))
                    {
                        def componentMatcher = file.getAbsolutePath().replaceAll("\\\\", "/") =~ /${dist}\/?(\w+?)\/.+/;
                        def component = "";
                        if (componentMatcher.matches())
                        {
                            component = componentMatcher.group(1).toLowerCase();
                            LOG.debug("Update For Component: " + component);
                        }

                        if (file.exists())
                        {
                            if (file.getName().equals("blueprint.properties"))
                            {
                                println "Configuring updated blueprint.properties file";
                                LOG.info("Copying properties from configured blueprint.properties file to new blueprint.properties file");
                                def scriptResult = MAIN.executeMgmtScript("install/copy-blueprint ", updatedFile.getAbsolutePath());
                                if (scriptResult.contains("FAILURE"))
                                {
                                    println "WARNING!!! Failure occurred in update - see rsview.log or update.log for more details";
                                    println scriptResult;
                                    result = false;
                                    break;
                                }
                            }

                            if (!file.getName().endsWith(".zip") || parent.getName() == "rsexpert")
                            {
                                def updateMD5 = CryptMD5.encrypt(file);
                                def checkMD5 = md5Sums[updatedFile.getName()]
                                LOG.debug("Checking MD5 Of File To Update " + file.getAbsolutePath());
                                LOG.debug(updateMD5 + "==" + checkMD5);
                                if ((checkMD5 instanceof String && updateMD5 == checkMD5) ||
                                    (checkMD5 instanceof List && checkMD5.contains(updateMD5)))
                                {
                                    //println "Updated File " + file.getName() + " Matches Update, Skipping";
                                    LOG.info("File " + file.getAbsolutePath() + " Matches MD5 Checksum (" + updateMD5 + "==" + checkMD5 + "), No Need to Replace");
                                    continue;
                                }
                                else
                                {
                                    LOG.info("Applying File " + file.getAbsolutePath());
                                }
                            }

                            if (excludeBackup(file, excludeBackupFiles))
                            {
                                LOG.warn("Updated File " + file.getAbsolutePath() + " is in Exclude Backup List");
                            }
                            else
                            {
                                result = backupFile(fileName, component);
                                if (!result)
                                {
                                    break;
                                }
                            }
                        }
                        else if (!file.getName().endsWith(".zip") || parent.getName() == "rsexpert")
                        {
                            LOG.info("New File: " + fileName);
                            newFiles.add(fileName);
                        }
                        if (file.getName().endsWith(".jar") && !exceptions.contains(file.getName()))
                        {
                            LOG.debug("Looking For Older Versions of " + file.getAbsolutePath());
                            def matcher = versionPattern.matcher(file.getName());
                            if (matcher.matches()) {
                                def name = matcher.group(1);
                                def matchingJars = parent.listFiles(new FileFilter() {
                                    @Override
                                    public boolean accept(File subFile) {
                                        def subFileName = subFile.getName();
                                        LOG.trace("Validating " + subFileName);
                                        if (!subFileName.endsWith(".jar") || subFileName.equals(file.getName())) {
                                            LOG.trace("Not a Jar or Same File");
                                            return false;
                                        } else {
                                            def acceptMatcher = versionPattern.matcher(subFileName);
                                            if (acceptMatcher.matches()) {
                                                def subName = acceptMatcher.group(1);
                                                //The files jetty-util-N.jar and jetty-util-N.vD.jar match same base name, but are different libs
                                                //jetty-util is org.mortbay, and jetty-util with the date is org.eclipse.jetty
                                                if (subName.contains("jetty-util") && name.contains("jetty-util")) {
                                                    if (subFileName.matches("jetty-util.*v\\d+.jar")) {
                                                        if (!file.getName().matches("jetty-util.*v\\d+.jar")) {
                                                            LOG.trace("Different Jetty Utils");
                                                            return false;
                                                        }
                                                    } else if (file.getName().matches("jetty-util.*v\\d+.jar")) {
                                                        LOG.trace("Different Jetty Utils");
                                                        return false;
                                                    }
                                                }
                                                LOG.trace("Validating if " + name + " Matches " + subName);
                                                return subName == name;
                                            } else {
                                                LOG.trace("Matcher Failed");
                                                return false;
                                            }
                                        }
                                    }
                                });
                                for(def jar in matchingJars) {
                                    LOG.info("Found Older Version of " + file.getName() + ": " + jar.getName());
                                    if (excludeBackup(file, excludeBackupFiles))
                                    {
                                        LOG.warn("Updated File " + file.getAbsolutePath() + " is in Exclude Backup List");
                                    }
                                    else
                                    {
                                        result = backupFile(jar, new File(jar.getAbsolutePath() + "." + backupExtension));
                                        if (!result)
                                        {
                                            break;
                                        }
                                    }
                                }
                            } else {
                                LOG.warn("Failed To Match New Jar Name, May Not Back Up Older Version Correctly: " + file.getName());
                            }
                        }
                        if (parent && !parent.exists())
                        {
                            LOG.warn("Creating Parent Directory: " + parent.getAbsolutePath());
                            if (!parent.mkdirs())
                            {
                                println "WARNING!!! Unable to Create Directory " + parent.getName() + " for " + updatedFile.getName();
                                println "May be Unable to Move New File";
                                LOG.warn("Unable to Create Directory " + parent.getAbsolutePath() + " for " + updatedFile.getName());
                            }
                        }

                        //println "Updating " + updatedFile.getName();
                        if (component.equalsIgnoreCase("rsremote"))
                        {
                            if (MAIN.isBlueprintRSRemote())
                            {
                                def instances = MAIN.getRSRemoteInstances();
                                if (instances && instances.size() > 1)
                                {
                                    for (instance in instances)
                                    {
                                        if (instance.equalsIgnoreCase("rsremote"))
                                        {
                                            //standard update - skip
                                        }
                                        else
                                        {
                                            //println "\tUpdating " + instance;
                                           // def instanceFile = new File(file.getAbsolutePath().replaceFirst("rsremote", instance));
                                            def instanceFile = new File(dist + File.separator + fileName.replaceFirst("rsremote", instance));
                                            LOG.info("Copying Update File " + updatedFile.getAbsolutePath() + " to " + instanceFile.getAbsolutePath()); 
                                            FileUtils.copyFile(updatedFile, instanceFile);
                                            if (updatedFile.canExecute())
                                            {
                                                instanceFile.setExecutable(true, false);
                                            }
                                        }
                                    }
                                }
                            }
                        }
                        LOG.info("Moving Update File " + updatedFile.getAbsolutePath() + " to " + file.getAbsolutePath());
                        if (file.exists())
                        {
                            if (contUpdate || force || excludeBackup(file, excludeBackupFiles))
                            {
                                if (contUpdate)
                                {
                                    LOG.info("Re-applying update file: " + file.getName());
                                }
                                else
                                {
                                    println "WARNING!!! Update File " + file.getName() + " Still Exists, Deleting It";
                                    LOG.warn("Update File " + file.getAbsolutePath() + " Still Exists, Deleting It");
                                }
                                if (!file.delete())
                                {
                                    println "WARNING!!! Unable to Remove Update File " + file.getName();
                                    LOG.error("Unable to Remove Update File " + file.getAbsolutePath() + ", Update Will likely fail");
                                }
                            }
                            else
                            {
                                println "WARNING!!! Update File " + file.getName() + " Has Not Been Moved/Backed Up, Cancelling Update";
                                LOG.error("Update File " + file.getAbsolutePath() + " Has Not Been Moved/Backed Up, Cancelling Update");
                                result = false;
                                break;
                            }
                        }
                        if (fileName.endsWith("config.xml") && (parent.getName() == "config" || parent.getName() == "WEB-INF"))
                        {
                            def backupComponentFile = new File(dist + "/" + fileName + "." + backupExtension);
                            if (backupComponentFile.exists())
                            {
                                XDoc configDoc = new XDoc(backupComponentFile);
                                def guid = configDoc.getStringValue("./ID/@GUID");

                                if (guid)
                                {
                                    configDoc = new XDoc(updatedFile);
                                    configDoc.setStringValue("./ID/@GUID", guid);
                                    configDoc.toPrettyFile(updatedFile);
                                }
                            }
                        }
                        if (file.getName().contains("_jdk"))
                        {
                            //do not move this file
                            jreFile = updatedFile;
                        }
                        else if (!updatedFile.renameTo(file))
                        {
                            println "WARNING!!! Unable To Move Updated File " + updatedFile.getName() + ", Cancelling Update";
                            LOG.error(updatedFile.getAbsolutePath() + " Could Not Be Updated to " + file.getAbsolutePath());
                            result = false;
                            break;
                        }
                        updatedFile = null;

                        if (file.getName().equals("blueprint.properties"))
                        {
                            MAIN.initBlueprint(true);
                            BLUEPRINT = MAIN.blueprintProperties;
                        }
                    }
                }
                if (result)
                {
                    for (fileName in removeFiles)
                    {
                        def file = new File(dist + "/" + fileName);
                        if (file.exists())
                        {

                            def componentMatcher = file.getAbsolutePath().replaceAll("\\\\", "/") =~ /${dist}\/?(\w+?)\/.+/;
                            def component = "";
                            if (componentMatcher.matches())
                            {
                                component = componentMatcher.group(1).toLowerCase();
                                LOG.debug("Remove File For Component: " + component);
                            }

                            result = backupFile(fileName, component);
                            if (!result)
                            {
                                println "WARNING!!! Unable to Remove " + fileName + ", Cancelling Update";
                                LOG.error("Unable to Back Up " + file.getAbsolutePath() + " Which has been marked for removal");
                                break;
                            }
                            else if (file.exists())
                            {
                                if (force)
                                {
                                    if (file.delete())
                                    {
                                        //println "Removed File " + fileName;
                                        LOG.info("Removed File " + file.getAbsolutePath());
                                    }
                                    else
                                    {
                                        println "WARNING!!! Unable to Force Remove " + fileName;
                                        LOG.error("Unable to Remove " + file.getAbsolutePath() + " Which has been marked for removal");
                                    }
                                }
                                else
                                {
                                    println "WARNING!!! Unable to Remove " + fileName + ", Cancelling Update";
                                    LOG.error("File " + file.getAbsolutePath() + " Still Exists");
                                    result = false;
                                    break;
                                }
                            }

                        }
                        else
                        {
                            //println "File " + fileName + " already removed, skipping";
                            LOG.info("File " + file.getAbsolutePath() + " already removed, skipping");
                        }
                    }
                }
            }
        }
    }
}
catch (Exception e)
{
    result = false;
    println "Unexpected Exception While Applying Update: " + e.getMessage();
    LOG.error("Unexpected Exception While Applying Update: " + e.getMessage(), e);
}
if (!result)
{
    println "Updated Failed, Finish Update Manually or Execute resolve-rollback in the RSConsole";
    LOG.warn("Updated Failed, Finish Update Manually or Execute resolve-rollback in the RSConsole");
}
else if (postCommands)
{
    def postCommandsFile = new File("file/post_commands.txt");
    LOG.info("Writing Post Startup Commands to " + postCommandsFile.getAbsolutePath());
    postCommandBytes = postCommands.getBytes();
    FileOutputStream fos = new FileOutputStream(postCommandsFile, true);
    fos.write(postCommandBytes);
    fos.close();
}

if (newFiles && newFiles.size() > 0)
{
    def newFile = new File(dist + "/file/update-" + backupExtension  + "-new.log");
    LOG.debug("Make New File Checksum log " + newFile.getAbsolutePath() + "\n" + newFiles);
    def newFileStr;
    for (fileName in newFiles)
    {
        if (newFileStr)
        {
            newFileStr += "\n" + fileName;
        }
        else
        {
            newFileStr = fileName;
        }
    }
    def newFileBytes = newFileStr.getBytes();
    fos = new FileOutputStream(newFile);
    fos.write(newFileBytes);
    fos.close();
}


return result;

def fileList(File file, boolean includeDir)
{
    def result;
    if (file.exists())
    {
        if (file.isDirectory())
        {
            result = [];
            def prepend = "";
            if (includeDir)
            {
                prepend = file.getName() + "/";
            }
            for (child in file.listFiles())
            {
                def childResult = fileList(child, true);
                if (childResult instanceof List)
                {
                    for (fileName in childResult)
                    {
                        result.add("${prepend}${fileName}".toString());
                    }
                }
                else if (childResult)
                {
                    result.add("${prepend}${childResult}".toString());
                }
            }
        }
        else
        {
            if (!file.getName().endsWith("md5") && !file.getName().startsWith("."))
            {
                result = file.getName();
            }
        }
    }
    return result;
}

def removeFile(File file)
{
    def result = true;
    if (file.exists())
    {
        if (file.isDirectory())
        {
            for (child in file.listFiles())
            {
                removeFile(child);
            }
        }
        if (file.delete())
        {
            LOG.warn("Removed File: " + file.getAbsolutePath());
        }
        else
        {
            LOG.warn("Failed to Remove File: " + file.getAbsolutePath());
            result = false;
        }
    }

    return result;
}

def excludeBackup(File file, List<String> excludeBackupFiles)
{
    def result = false;
    def absPath = file.getAbsolutePath().replaceAll("\\\\", "/");
    for (excludeFile in excludeBackupFiles)
    {
        if (excludeFile)
        {
            excludeFile = excludeFile.replaceAll("\\\\", "/");
            def matcher = absPath =~ /.*${excludeFile}.*/;
            if (matcher.find())
            {
                LOG.info("File " + absPath + " Matches Exclude Backup Expression: " + excludeFile);
                result = true;
                break;
            }
        }
    }
    return result;
}

def backupFile(String fileName, String component)
{
    def result = true;
    def file = new File(dist + "/" + fileName);

    def backupComponentFile = new File(dist + "/" + fileName + "." + backupExtension);
    result = backupFile(file, backupComponentFile);

    if (component.equalsIgnoreCase("rsremote"))
    {
        if (MAIN.isBlueprintRSRemote())
        {
            def instances = MAIN.getRSRemoteInstances();
            if (instances && instances.size() > 1)
            {
                for (instance in instances)
                {
                    if (instance.equalsIgnoreCase("rsremote"))
                    {
                        //already backed up
                    }
                    else
                    {
                        def instanceFileName = file.getAbsolutePath().replaceFirst("rsremote", instance);
                        def backupInstanceFileName = backupComponentFile.getAbsolutePath().replaceFirst("rsremote", instance);
                        if (instanceFileName.endsWith("init.rsremote"))
                        {
                            instanceFileName = instanceFileName.replaceFirst("init.rsremote", "init." + instance);
                            backupInstanceFileName = backupInstanceFileName.replaceFirst("init.rsremote", "init." + instance);
                        }
                        else if (instanceFileName.endsWith("rsremote.service"))
                        {
                            instanceFileName = instanceFileName.replaceFirst("rsremote.service", instance + ".service");
                            backupInstanceFileName = backupInstanceFileName.replaceFirst("rsremote.service", instance + ".service");
                        }
                        def instanceFile = new File(instanceFileName);
                        def backupInstanceFile = new File(backupInstanceFileName);
                        result = backupFile(instanceFile, backupInstanceFile);
                    }
                }
            }
        }
    }
    return result;
}

def backupFile(File file, File backupFile)
{
    def result = true;
    if (backupFile.exists())
    {
        if (contUpdate || force)
        {
            println "WARNING!!! Intended Backup File " + backupFile.getName() + " Already Exists, Skipping Backup";
            LOG.warn("Intended Backup File " + backupFile.getAbsolutePath() + " Already Exists, Skipping Backup");
        }
        else
        {
            println "Intended Backup File " + backupFile.getName() + " Already Exists, Cancelling Update";
            println "Remove or Rename the File to Continue Update";
            LOG.warn("Backup File " + backupFile.getAbsolutePath() + " Already Exists, Remove To Continue");
            result = false;
        }
    }
    else
    {
        //println "Backing Up " + file.getName();
        LOG.info("Backing Up " + file.getAbsolutePath() + " to " + backupFile.getAbsolutePath());
        if (!file.renameTo(backupFile))
        {
            print "WARNING!!! Unable To Backup File " + file.getName();
            LOG.error(file.getAbsolutePath() + " Could Not Be Backed Up to " + backupFile.getAbsolutePath());
            if (force)
            {
                println "";
            }
            else
            {
                println ", Cancelling Update";
                result = false;
            }
        }
    }
    return result;
}

def installSort()
{
    def sort = { String s1, s2 ->
        int result = 0;
        if (s1.toLowerCase().endsWith("copy-blueprint.groovy"))
        {
            if (!s2.toLowerCase().endsWith("copy-blueprint.groovy"))
            {
                result = -1;
            }
        }
        else if (s2.toLowerCase().endsWith("copy-blueprint.groovy"))
        {
            result = 1;
        }
        else if (s1.toLowerCase().endsWith("blueprint.properties"))
        {
            if (!s2.toLowerCase().endsWith("blueprint.properties"))
            {
                result = -1;
            }
        }
        else if (s2.toLowerCase().endsWith("blueprint.properties"))
        {
            result = 1;
        }
        else if (s1.toLowerCase().endsWith("zip"))
        {
            if (!s2.toLowerCase().endsWith("zip"))
            {
                result = -1;
            }
        }
        else if (s2.toLowerCase().endsWith("zip"))
        {
            result = 1;
        }
        else if (s1.toLowerCase().endsWith("groovy"))
        {
            if (!s2.toLowerCase().endsWith("groovy"))
            {
                result = -1;
            }
        }
        else if (s2.toLowerCase().endsWith("groovy"))
        {
            result = 1;
        }
        return result;
    }
    return sort;
};

def importSort()
{
    def list;
    if (MAIN.blueprintProperties)
    {
        list = MAIN.blueprintProperties.getProperty("resolve.import");
    }
    if (list)
    {
        list = list.split(",");
        for (int i=0; i<list.length; i++)
        {
            if (!list[i].endsWith(".zip"))
            {
                list[i] = list[i] + ".zip";
            }
        }
        list = Arrays.asList(list);
    }
    else
    {
        list = [];
    }
    def sort = { File f1, f2 ->
        int result = 0;

        String s1 = f1.getName();
        String s2 = f2.getName();
        
        int p1 = list.indexOf(s1);
        int p2 = list.indexOf(s2);
        
        if (p1 == -1)
        {
            if (p2 > -1)
            {
                result = 1;
            }
        }
        else if (p2 == -1)
        {
            result = -1;
        }
        else if (p1 > p2)
        {
            result = 1;
        }
        else if (p2 > p1)
        {
            result = -1;
        }
        
        return result;
    }
    return sort;
};
