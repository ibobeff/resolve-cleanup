def result = false;
try
{
    def curVersion = BLUEPRINT.get("CURRENTBUILDVERSION");
    def upVersion = BLUEPRINT.get("BUILDVERSION");
    LOG.info("Testing whether " + curVersion + " can be updated to " + upVersion);

    //Current update process is only for Version 6.X
    if (curVersion.startsWith("6") || curVersion.startsWith("trunk"))
    {
        LOG.info("Minumum version of 6.0 is met: " + curVersion);
        result = true;

        def curVersionMatcher = curVersion =~ /\d+\.([^.-]+)[.-]?([^.-]+)[.-]?(.+)?/
        def upVersionMatcher = upVersion =~ /\d+\.([^.]+)\.?([^.]+)\.?(.+)?/
        if (curVersionMatcher.find() && upVersionMatcher.find())
        {
            def curMajorVersion = curVersionMatcher.group(1);
            def curMinorVersion = curVersionMatcher.group(2);
            def curHotfixVersion = curVersionMatcher.group(3);
            def upMajorVersion = upVersionMatcher.group(1);
            def upMinorVersion = upVersionMatcher.group(2);
            def upHotfixVersion = upVersionMatcher.group(3);
            //Check if current major update version is a decimal, durring development it may be a word
            if (curMajorVersion =~ /\d+/ && upMajorVersion =~ /\d+/)
            {
                curMajorVersion = curMajorVersion.toInteger();
                upMajorVersion = upMajorVersion.toInteger();
                if (curMajorVersion > upMajorVersion)
                {
                    LOG.error("Invalid Update Version - Cannot update current version " + curVersion + " to previous version " + upVersion);
                    println "Invalid Update Version - Cannot update current version " + curVersion + " to previous version " + upVersion;
                    result = false;
                }
                else if (curMajorVersion == upMajorVersion)
                {
                    //if major versions match, check the minor or patch version
                    if (curMinorVersion =~ /\d+/ && upMinorVersion =~ /\d+/)
                    {
                        curMinorVersion = curMinorVersion.toInteger();
                        upMinorVersion = upMinorVersion.toInteger();
                        if (curMinorVersion > upMinorVersion)
                        {
                            LOG.error("Invalid Update Version - Cannot update current version " + curVersion + " to previous version " + upVersion);
                            println "Invalid Update Version - Cannot update current version " + curVersion + " to previous version " + upVersion;
                            result = false;
                        }
                        else if (curMinorVersion == upMinorVersion)
                        {
                            //if minor versions match, check if current version uses older build date, if not compare hotfix version
                            if (curHotfixVersion =~ /\d{8}/)
                            {
                                LOG.info("Current Version Uses Older Datestamp Versioning, Assuming New Version");
                            }
                            else if (curHotfixVersion =~ /\d+/ && upHotfixVersion =~ /\d+/)
                            {
                                curHotfixVersion = curHotfixVersion.toInteger();
                                upHotfixVersion = upHotfixVersion.toInteger();
                                if (curHotfixVersion > upHotfixVersion)
                                {
                                    LOG.error("Invalid Update Version - Cannot update current version " + curVersion + " to previous version " + upVersion);
                                    println "Invalid Update Version - Cannot update current version " + curVersion + " to previous version " + upVersion;
                                    result = false;
                                }
                                else if (curHotfixVersion == upHotfixVersion)
                                {
                                    if (ARGS.contains("--continue"))
                                    {
                                        LOG.info("Continue Option Passed, Allowing Re-Run of same update version");
                                    }
                                    else
                                    {
                                        LOG.error("Invalid Update Version - Cannot update current version " + curVersion + " to same version " + upVersion);
                                        println "Invalid Update Version - Cannot update current version " + curVersion + " to same version " + upVersion;
                                        result = false;
                                    }
                                }
                                else
                                {
                                    LOG.info("Version Validation Passed");
                                }
                            }
                            else
                            {
                                LOG.warn("Hotfix Version Number is not in decimal format so version check cannot be run, assuming update is valid");
                            }
                        }
                    }
                    else
                    {
                        LOG.warn("Minor Version Number is not in decimal format so version check cannot be run, assuming update is valid");
                    }
                }
            }
            else
            {
                LOG.warn("Major Version Number is not in decimal format so version check cannot be run, assuming update is valid");
            }
        }
        else
        {
            LOG.warn("Unable to parse version information, assuming update is valid");
        }
    }
    else
    {
        LOG.error("Update must be run from a minimum version of 6.0, please upgrade to this version before attempting to run this update");
        println "\nUpdate must be run from a minimum version of 6.0, please upgrade to this version before attempting to run this update";
    }
}
catch (Exception e)
{
    LOG.error("Failed to determine current version, failing update", e);
    println "\nFailed to determine current version, failing update - please see update.log for more details";
    result = false;
}

return result;
