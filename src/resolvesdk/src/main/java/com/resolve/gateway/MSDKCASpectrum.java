/******************************************************************************
* (C) Copyright 2015
*
* Resolve Systems, LLC
*
* All rights reserved
* This software is distributed under license and may not be used, copied,
* modified, or distributed without the express written permission of
* Resolve Systems, LLC.
*
******************************************************************************/

package com.resolve.gateway;

import java.util.Map;

import com.resolvesdk.gateway.sdkcaspectrum.SDKCASpectrumFilter;
import com.resolvesdk.gateway.sdkcaspectrum.SDKCASpectrumGateway;

/**
 * This class is used by the Message Listener and invokes it methods. Messages
 * destined for the gateways set this class name in the message header.
 *
 */
public class MSDKCASpectrum extends MGateway
{
    private static final String RSCONTROL_SET_FILTERS = "MSDKCASpectrum.setFilters";

    private static final SDKCASpectrumGateway instance = SDKCASpectrumGateway.getInstance();

    public MSDKCASpectrum()
    {
        super.instance = instance;
    }
    
    protected String getSetFilterMethodName()
    {
        return RSCONTROL_SET_FILTERS;
    }

    /**
     * Populates the {@link Filter} data into the given filterMap {@link Map} instance
     * 
     * @param filterMap
     *      the {@link Map} instance to hold the {@link Filter} data
     * @param filter
     *      a {@link SDKCASpectrumFilter} instance
     *            
     */
    
    public void populateGatewaySpecificFilterProperties(Map<String, String> filterMap, Filter filter)
    {
        if (instance.isActive())
        {
            SDKCASpectrumFilter caSpectrumFilter = (SDKCASpectrumFilter) filter;
            filterMap.put(SDKCASpectrumFilter.URLQUERY, caSpectrumFilter.getUrlQuery());
            filterMap.put(SDKCASpectrumFilter.XMLQUERY, caSpectrumFilter.getXmlQuery());
            filterMap.put(SDKCASpectrumFilter.OBJECT, caSpectrumFilter.getObject());
        }
    } // populateGatewaySpecificProperties
}
