/******************************************************************************
* (C) Copyright 2015
*
* Resolve Systems, LLC
*
* All rights reserved
* This software is distributed under license and may not be used, copied,
* modified, or distributed without the express written permission of
* Resolve Systems, LLC.
*
******************************************************************************/

package com.resolve.gateway;

import java.util.Map;

import com.resolvesdk.gateway.sdkjira.SDKJIRAFilter;
import com.resolvesdk.gateway.sdkjira.SDKJIRAGateway;

/**
 * This class is used by the Message Listener and invokes it methods. Messages
 * destined for the gateways set this class name in the message header.
 *
 */
public class MSDKJIRA extends MGateway
{
    private static final String RSCONTROL_SET_FILTERS = "MSDKJIRA.setFilters";

    private static final SDKJIRAGateway instance = SDKJIRAGateway.getInstance();

    public MSDKJIRA()
    {
        super.instance = instance;
    }
    
    protected String getSetFilterMethodName()
    {
        return RSCONTROL_SET_FILTERS;
    }

    /**
     * Populates the {@link Filter} data into the given filterMap {@link Map} instance
     * 
     * @param filterMap
     *      the {@link Map} instance to hold the {@link Filter} data
     * @param filter
     *      a {@link SDKJIRAFilter} instance
     *            
     */
    
    public void populateGatewaySpecificFilterProperties(Map<String, String> filterMap, Filter filter)
    {
        if (instance.isActive())
        {
            SDKJIRAFilter sdkJIRAFilter = (SDKJIRAFilter) filter;
            filterMap.put(SDKJIRAFilter.URLQUERY, sdkJIRAFilter.getUrlQuery());
            filterMap.put(SDKJIRAFilter.JSONQUERY, sdkJIRAFilter.getJsonQuery());
        }
    } // populateGatewaySpecificProperties
}
