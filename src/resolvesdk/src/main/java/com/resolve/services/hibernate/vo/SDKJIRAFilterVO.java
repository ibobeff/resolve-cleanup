/******************************************************************************
* (C) Copyright 2015
*
* Resolve Systems, LLC
*
* All rights reserved
* This software is distributed under license and may not be used, copied,
* modified, or distributed without the express written permission of
* Resolve Systems, LLC.
*
******************************************************************************/

package com.resolve.services.hibernate.vo;

import com.resolve.services.interfaces.VO;
import com.resolve.util.MappingAnnotation;
import com.resolve.util.StringUtils;

public class SDKJIRAFilterVO extends GatewayFilterVO
{
    private static final long serialVersionUID = 6061631016677683205L;
    public static final int URL_AND_JSON_QUERY_IN_FILTER_IS_INVALID = -1;
    public static final String URL_AND_JSON_QUERY_IN_FILTER_IS_INVALID_MESSAGE = "Specify one of URL/JSON query for filter. Filter cannot have both URL and JSON queries.";
    public static final int MISSING_URL_OR_JSON_QUERY_IN_FILTER = -2;
    public static final String MISSING_URL_OR_JSON_QUERY_IN_FILTER_MESSAGE = "Specify one of URL/JSON query for filter. Filter must have either URL or JSON query.";
    
    private String UUrlQuery;
    private String UJsonQuery;
    
    public SDKJIRAFilterVO()
    {
    } // SDKJIRAFilterVO*/

    @MappingAnnotation(columnName = "URLQUERY")
    public String getUUrlQuery()
    {
        return UUrlQuery;
    } // getUUrlQuery

    public void setUUrlQuery(String UUrlQuery)
    {
        if (StringUtils.isNotBlank(UUrlQuery) || this.UUrlQuery.equals(VO.STRING_DEFAULT))
        {
            this.UUrlQuery = UUrlQuery != null ? UUrlQuery.trim() : UUrlQuery;
        }
    } // setUUrlQuery

    @MappingAnnotation(columnName = "JSONQUERY")
    public String getUJsonQuery()
    {
        return StringUtils.unescapeXml(UJsonQuery);
    } // getUJSONQuery

    public void setUJsonQuery(String UJsonQuery)
    {
        if (StringUtils.isNotBlank(UJsonQuery) || this.UJsonQuery.equals(VO.STRING_DEFAULT))
        {
            this.UJsonQuery = UJsonQuery != null ? StringUtils.escapeXml(UJsonQuery.trim()) : UJsonQuery;
        }
    } // setUJSONQuery
    
    @Override
    public int hashCode()
    {
        final int prime = 31;
        int result = super.hashCode();
        result = prime * result + ((UUrlQuery == null) ? 0 : UUrlQuery.hashCode());
        result = prime * result + ((UJsonQuery == null) ? 0 : UJsonQuery.hashCode());
        return result;
    }

    @Override
    public boolean equals(Object obj)
    {
        if (this == obj)
            return true;
        
        if (!super.equals(obj))
            return false;
        
        if (getClass() != obj.getClass())
            return false;
        
        SDKJIRAFilterVO other = (SDKJIRAFilterVO) obj;

        if (UUrlQuery == null)
        {
            if (StringUtils.isNotBlank(other.UUrlQuery))
                return false;
        }
        else
        {
            if (!UUrlQuery.trim().equals(other.UUrlQuery == null ? "" : other.UUrlQuery.trim()))
                return false;
        }
        
        if (UJsonQuery == null)
        {
            if (StringUtils.isNotBlank(other.UJsonQuery))
                return false;
        }
        else
        {
            if (!UJsonQuery.trim().equals(other.UJsonQuery == null ? "" : other.UJsonQuery.trim()))
                return false;
        }
        
        return true;
    }
    
    @Override
    public int isValid()
    {
        if (StringUtils.isNotBlank(UUrlQuery) && !UUrlQuery.trim().equals(STRING_DEFAULT) && UUrlQuery.trim().length() > 0 && 
            StringUtils.isNotBlank(UJsonQuery) && !UJsonQuery.trim().equals(STRING_DEFAULT) && UJsonQuery.length() > 0)
        {
            return URL_AND_JSON_QUERY_IN_FILTER_IS_INVALID;
        }
        
        if (StringUtils.isBlank(UUrlQuery) && StringUtils.isBlank(UJsonQuery))
        {
            return MISSING_URL_OR_JSON_QUERY_IN_FILTER;
        }
        
        return 0;
    }
    
    @Override
    public String getValidationError(int errorCode)
    {
        switch(errorCode)
        {
            case URL_AND_JSON_QUERY_IN_FILTER_IS_INVALID:
                return URL_AND_JSON_QUERY_IN_FILTER_IS_INVALID_MESSAGE;
            
            case MISSING_URL_OR_JSON_QUERY_IN_FILTER:
                return MISSING_URL_OR_JSON_QUERY_IN_FILTER_MESSAGE;
                
            default:
                return "Error code not found.";
        }
    }
}
