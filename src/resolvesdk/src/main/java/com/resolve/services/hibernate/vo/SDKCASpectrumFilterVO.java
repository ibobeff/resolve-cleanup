/******************************************************************************
* (C) Copyright 2015
*
* Resolve Systems, LLC
*
* All rights reserved
* This software is distributed under license and may not be used, copied,
* modified, or distributed without the express written permission of
* Resolve Systems, LLC.
*
******************************************************************************/

package com.resolve.services.hibernate.vo;

import com.resolve.services.interfaces.VO;
import com.resolve.util.MappingAnnotation;
import com.resolve.util.StringUtils;

public class SDKCASpectrumFilterVO extends GatewayFilterVO
{
    private static final long serialVersionUID = 8625818696032286759L;
    public static final int UNSUPPORTED_FILTER_OBJECT_TYPE = -1;
    public static final String UNSUPPORTED_FILTER_OBJECT_TYPE_MESSAGE = "Filter object types can be one of the alarms, models, devices or subscription.";
    public static final int URL_AND_SQL_QUERY_IN_FILTER_IS_INVALID = -2;
    public static final String URL_AND_SQL_QUERY_IN_FILTER_IS_INVALID_MESSAGE = "Specify one of URL or SQL query for filter. Filter cannot have both URL and SQL queries.";
    public static final int UNSUPPORTED_FILTER_OBJECT_TYPE_FOR_URL_QUERY = -3;
    public static final String UNSUPPORTED_FILTER_OBJECT_TYPE_FOR_URL_QUERY_MESSAGE = "Only alarms, models and devices are valid object types for URL query filter.";
    public static final int UNSUPPORTED_FILTER_OBJECT_TYPE_FOR_XML_QUERY = -4;
    public static final String UNSUPPORTED_FILTER_OBJECT_TYPE_FOR_XML_QUERY_MESSAGE = "Only subscription is valid object type for XML query filter.";
    public static final int MISSING_LAST_ALARM_TIME_LVALUE = -5;
    public static final String MISSING_LAST_ALARM_TIME_LVALUE_MESSAGE = "${LATESTALARMTIME} can only be used with attribute id 0x1321a as lvalue in XML query filter.";
    public static final int MISPLACED_LAST_ALARM_TIME_LVALUE = -6;
    public static final String MISPLACED_LAST_ALARM_TIME_LVALUE_MESSAGE = "Position of attribute id 0x1321a is after ${LATESTALARMTIME} instaed of before it in XML query filter.";
    public static final int MISSING_OR_MISPLACED_REQ_LAST_OCCR_TIME_ATTR = -7;
    public static final String MISSING_OR_MISPLACED_REQ_LAST_OCCR_TIME_ATTR_MESSAGE = "Missing or misplaced requested attribute 0x1321a in XML query filter.";
    public static final int MISSING_URL_OR_SQL_QUERY_IN_FILTER = -8;
    public static final String MISSING_URL_OR_SQL_QUERY_IN_FILTER_MESSAGE = "Missing URL or SQL Query for filter. Filter must have either URL or SQL query.";
    
    private String UObject;
    private String UUrlQuery;
    private String UXmlQuery;
    
    public SDKCASpectrumFilterVO()
    {
    } // CASpectrumFilter*/

    @MappingAnnotation(columnName = "OBJECT")
    public String getUObject()
    {
        return this.UObject;
    } // getUObject

    public void setUObject(String UObject)
    {
        if (StringUtils.isNotBlank(UObject) || this.UObject.equals(VO.STRING_DEFAULT))
        {
            this.UObject = UObject != null ? UObject.trim() : UObject;
        }
    } // setUObject

    @MappingAnnotation(columnName = "URLQUERY")
    public String getUUrlQuery()
    {
        return this.UUrlQuery;
    } // getUUrlQuery

    public void setUUrlQuery(String uUrlQuery)
    {
        if (StringUtils.isNotBlank(uUrlQuery) || UUrlQuery.equals(VO.STRING_DEFAULT))
        {
            this.UUrlQuery = uUrlQuery != null ? uUrlQuery.trim() : uUrlQuery;
        }
    } // setUUrlQuery

    @MappingAnnotation(columnName = "XMLQUERY")
    public String getUXmlQuery()
    {
        return StringUtils.unescapeXml(UXmlQuery);
    } // getUXmlQuery

    public void setUXmlQuery(String uXmlQuery)
    {
        if (StringUtils.isNotBlank(uXmlQuery) || UXmlQuery.equals(VO.STRING_DEFAULT))
        {
            this.UXmlQuery = uXmlQuery != null ? StringUtils.escapeXml(uXmlQuery.trim()) : uXmlQuery;
        }
    } // setUXmlQuery
    
    @Override
    public int hashCode()
    {
        final int prime = 31;
        int result = super.hashCode();
        result = prime * result + ((UObject == null) ? 0 : UObject.hashCode());
        result = prime * result + ((UUrlQuery == null) ? 0 : UUrlQuery.hashCode());
        result = prime * result + ((UXmlQuery == null) ? 0 : UXmlQuery.hashCode());
        return result;
    }

    @Override
    public boolean equals(Object obj)
    {
        if (this == obj) return true;
        if (!super.equals(obj)) return false;
        if (getClass() != obj.getClass()) return false;
        SDKCASpectrumFilterVO other = (SDKCASpectrumFilterVO) obj;
        if (UObject == null)
        {
            if (StringUtils.isNotBlank(other.UObject)) return false;
        }
        else if (!UObject.trim().equals(other.UObject == null ? "" : other.UObject.trim())) return false;
        if (UUrlQuery == null)
        {
            if (StringUtils.isNotBlank(other.UUrlQuery)) return false;
        }
        else if (!UUrlQuery.trim().equals(other.UUrlQuery == null ? "" : other.UUrlQuery.trim())) return false;
        if (UXmlQuery == null)
        {
            if (StringUtils.isNotBlank(other.UXmlQuery)) return false;
        }
        else if (!UXmlQuery.trim().equals(other.UXmlQuery == null ? "" : other.UXmlQuery.trim())) return false;
        return true;
    }
    
    @Override
    public int isValid()
    {
        if (StringUtils.isNotBlank(UUrlQuery) && StringUtils.isNotBlank(UXmlQuery))
        {
            return URL_AND_SQL_QUERY_IN_FILTER_IS_INVALID;
        }
        
        String tmpUObject = StringUtils.isNotBlank(UObject) ? UObject.trim() : UObject;
        
        if (!"alarms".equals(tmpUObject) && !"models".equals(tmpUObject) &&
            !"devices".equals(tmpUObject) && !"subscription".equals(tmpUObject))
        {
            return UNSUPPORTED_FILTER_OBJECT_TYPE;
        }
        /*
        if (StringUtils.isNotBlank(UUrlQuery) && !UObject.equals("alarms") && 
            !UObject.equals("models") && !UObject.equals("devices"))
        {
            return UNSUPPORTED_FILTER_OBJECT_TYPE_FOR_URL_QUERY;
        }
        
        if (StringUtils.isNotBlank(UXmlQuery) && !UObject.equals("subscription"))
        {
            return UNSUPPORTED_FILTER_OBJECT_TYPE_FOR_XML_QUERY;
        }*/
        
        if (StringUtils.isNotBlank(UXmlQuery) && UXmlQuery.trim().contains("&lt;value&gt;${LATESTALARMTIME}&lt;/value&gt;"))
        {
            int indexOfLastOccrTimeAttr = UXmlQuery.trim().indexOf("&lt;attribute id=&quot;0x1321a&quot;&gt;");
            
            if (indexOfLastOccrTimeAttr == -1)
            {
                indexOfLastOccrTimeAttr = UXmlQuery.trim().indexOf("&lt;attribute id=&quot;0X1321A&quot;&gt;");
                
                if (indexOfLastOccrTimeAttr == -1 )
                {
                    return MISSING_LAST_ALARM_TIME_LVALUE;
                }
            }    
                
            int indexOfLastAlarmTime = UXmlQuery.trim().indexOf("&lt;value&gt;${LATESTALARMTIME}&lt;/value&gt;");
                
            if (indexOfLastOccrTimeAttr > indexOfLastAlarmTime)
            {
                return MISPLACED_LAST_ALARM_TIME_LVALUE;
            }
                
            int indexOfReqLastOccrTimeAttr = UXmlQuery.trim().indexOf("&lt;rs:requested-attribute id=&quot;0x1321a&quot;");
            
            if (indexOfReqLastOccrTimeAttr == -1)
            {
                indexOfReqLastOccrTimeAttr = UXmlQuery.trim().indexOf("&lt;rs:requested-attribute id=&quot;0X1321A&quot;");
            }
            
            if (indexOfReqLastOccrTimeAttr == -1 || indexOfReqLastOccrTimeAttr < indexOfLastAlarmTime)
            {
                return MISSING_OR_MISPLACED_REQ_LAST_OCCR_TIME_ATTR;
            }
        }
        
        if (StringUtils.isBlank(UUrlQuery) && StringUtils.isBlank(UXmlQuery))
        {
            return MISSING_URL_OR_SQL_QUERY_IN_FILTER;
        }
        
        return 0;
    }
    
    @Override
    public String getValidationError(int errorCode)
    {
        switch(errorCode)
        {
            case UNSUPPORTED_FILTER_OBJECT_TYPE:
                return UNSUPPORTED_FILTER_OBJECT_TYPE_MESSAGE;

            case URL_AND_SQL_QUERY_IN_FILTER_IS_INVALID:
                return URL_AND_SQL_QUERY_IN_FILTER_IS_INVALID_MESSAGE;
            
            case UNSUPPORTED_FILTER_OBJECT_TYPE_FOR_URL_QUERY:
                return UNSUPPORTED_FILTER_OBJECT_TYPE_FOR_URL_QUERY_MESSAGE;
                
            case UNSUPPORTED_FILTER_OBJECT_TYPE_FOR_XML_QUERY:
                return UNSUPPORTED_FILTER_OBJECT_TYPE_FOR_XML_QUERY_MESSAGE;
            
            case MISSING_LAST_ALARM_TIME_LVALUE:
                return MISSING_LAST_ALARM_TIME_LVALUE_MESSAGE;
                
            case MISPLACED_LAST_ALARM_TIME_LVALUE:
                return MISPLACED_LAST_ALARM_TIME_LVALUE_MESSAGE;
                
            case MISSING_OR_MISPLACED_REQ_LAST_OCCR_TIME_ATTR:
                return MISSING_OR_MISPLACED_REQ_LAST_OCCR_TIME_ATTR_MESSAGE;
            
            case MISSING_URL_OR_SQL_QUERY_IN_FILTER:
                return MISSING_URL_OR_SQL_QUERY_IN_FILTER_MESSAGE;
                
            default:
                return "Error code not found.";
        }
    }
}
