/******************************************************************************
* (C) Copyright 2015
*
* Resolve Systems, LLC
*
* All rights reserved
* This software is distributed under license and may not be used, copied,
* modified, or distributed without the express written permission of
* Resolve Systems, LLC.
*
******************************************************************************/

package com.resolve.persistence.model;

import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Lob;
import javax.persistence.Table;
import javax.persistence.UniqueConstraint;

import com.resolve.services.hibernate.vo.SDKCASpectrumFilterVO;
import com.resolve.services.interfaces.VO;
import com.resolve.util.StringUtils;

/**
 * CASpectrumFilter
 */
@Entity
@Table(name = "sdk_sdkcaspectrum_filter", uniqueConstraints = { @UniqueConstraint(columnNames = { "u_name", "u_queue" }) })
// @Cache(usage = CacheConcurrencyStrategy.READ_WRITE)
public class SDKCASpectrumFilter extends GatewayFilter<SDKCASpectrumFilterVO>
{
    private static final long serialVersionUID = -2699062251078652218L;
    private String UObject;
    private String UUrlQuery;
    private String UXmlQuery;

    // object references

    // object referenced by
    public SDKCASpectrumFilter()
    {
    } // SDKCASpectrumFilter*/

    public SDKCASpectrumFilter(SDKCASpectrumFilterVO vo)
    {
        applyVOToModel(vo);
    } // SDKCASpectrumFilter

    @Column(name = "u_object", length = 40)
    public String getUObject()
    {
        return this.UObject;
    } // getUObject

    public void setUObject(String UObject)
    {
        this.UObject = UObject;
    } // setUObject

    @Column(name = "u_url_query", length = 4000)
    public String getUUrlQuery()
    {
        return this.UUrlQuery;
    } // getUUrlQuery

    public void setUUrlQuery(String uUrlQuery)
    {
        this.UUrlQuery = uUrlQuery;
    } // setUUrlQuery

    @Lob
    @Column(name = "u_xml_query", length = 16777215)
    public String getUXmlQuery()
    {
        return this.UXmlQuery;
    } // getUXmlQuery

    public void setUXmlQuery(String uXmlQuery)
    {
        this.UXmlQuery = uXmlQuery;
    } // setUXmlQuery
    
    //@Override
    public SDKCASpectrumFilterVO doGetVO()
    {
        SDKCASpectrumFilterVO vo = new SDKCASpectrumFilterVO();
        super.doGetBaseVO(vo);

        vo.setUObject(getUObject());
        vo.setUUrlQuery(getUUrlQuery());
        vo.setUXmlQuery(getUXmlQuery());

        return vo;
    }

    @Override
    public void applyVOToModel(SDKCASpectrumFilterVO vo)
    {
        if (vo != null)
        {
            super.applyVOToModel(vo);

            this.setUObject(StringUtils.isNotBlank(vo.getUObject()) && vo.getUObject().equals(VO.STRING_DEFAULT) ? getUObject() : vo.getUObject());
            this.setUUrlQuery(StringUtils.isNotBlank(vo.getUUrlQuery()) && vo.getUUrlQuery().equals(VO.STRING_DEFAULT) ? getUUrlQuery() : vo.getUUrlQuery());
            this.setUXmlQuery(StringUtils.isNotBlank(vo.getUXmlQuery()) && vo.getUXmlQuery().equals(VO.STRING_DEFAULT) ? getUXmlQuery() : vo.getUXmlQuery());
        }
    }
    
    @Override
    public List<String> ugetCdataProperty()
    {
        List<String> list = super.ugetCdataProperty();
        list.add("UXmlQuery");
        
        return list;
    }//ugetCdataProperty
}
