/******************************************************************************
* (C) Copyright 2015
*
* Resolve Systems, LLC
*
* All rights reserved
* This software is distributed under license and may not be used, copied,
* modified, or distributed without the express written permission of
* Resolve Systems, LLC.
*
******************************************************************************/

package com.resolve.persistence.model;

import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Lob;
import javax.persistence.Table;
import javax.persistence.UniqueConstraint;

import com.resolve.services.hibernate.vo.SDKJIRAFilterVO;
import com.resolve.services.interfaces.VO;
import com.resolve.util.StringUtils;

/**
 * SDKJIRAFilter
 */
@Entity
@Table(name = "sdk_sdkjira_filter", uniqueConstraints = { @UniqueConstraint(columnNames = { "u_name", "u_queue" }) })
public class SDKJIRAFilter extends GatewayFilter<SDKJIRAFilterVO>
{
    private static final long serialVersionUID = -1844710270141724750L;
    private String UUrlQuery;
    private String UJsonQuery;

    // object references

    // object referenced by
    
    public SDKJIRAFilter()
    {
    } // SDKJIRAFilter*/

    public SDKJIRAFilter(SDKJIRAFilterVO vo)
    {
        applyVOToModel(vo);
    } // SDKJIRAFilter

    @Column(name = "u_url_query", length = 4000)
    public String getUUrlQuery()
    {
        return this.UUrlQuery;
    } // getUUrlQuery

    public void setUUrlQuery(String uUrlQuery)
    {
        this.UUrlQuery = uUrlQuery;
    } // setUUrlQuery

    @Lob
    @Column(name = "u_json_query", length = 16777215)
    public String getUJsonQuery()
    {
        return this.UJsonQuery;
    } // getUJSONQuery

    public void setUJsonQuery(String uJsonQuery)
    {
        this.UJsonQuery = uJsonQuery;
    } // setUJSONQuery
    
    //@Override
    public SDKJIRAFilterVO doGetVO()
    {
        SDKJIRAFilterVO vo = new SDKJIRAFilterVO();
        super.doGetBaseVO(vo);

        vo.setUUrlQuery(getUUrlQuery());
        vo.setUJsonQuery(getUJsonQuery());

        return vo;
    }

    @Override
    public void applyVOToModel(SDKJIRAFilterVO vo)
    {
        if (vo != null)
        {
            super.applyVOToModel(vo);

            this.setUUrlQuery(StringUtils.isNotBlank(vo.getUUrlQuery()) && vo.getUUrlQuery().equals(VO.STRING_DEFAULT) ? getUUrlQuery() : vo.getUUrlQuery());
            this.setUJsonQuery(StringUtils.isNotBlank(vo.getUJsonQuery()) && vo.getUJsonQuery().equals(VO.STRING_DEFAULT) ? getUJsonQuery() : vo.getUJsonQuery());
        }
    }
    
    @Override
    public List<String> ugetCdataProperty()
    {
        List<String> list = super.ugetCdataProperty();
        list.add("UJsonQuery");
        
        return list;
    }//ugetCdataProperty
}
