/******************************************************************************
* (C) Copyright 2014
*
* Resolve Systems, LLC
*
* All rights reserved
* This software is distributed under license and may not be used, copied,
* modified, or distributed without the express written permission of
* Resolve Systems, LLC.
*
******************************************************************************/

package com.resolve.rscontrol;

import com.resolve.persistence.model.SDKCASpectrumFilter;

public class MSDKCASpectrum extends MGateway
{
    private static final String MODEL_NAME = SDKCASpectrumFilter.class.getSimpleName();

    protected String getModelName()
    {
        return MODEL_NAME;
    }
} // MSDKCASpectrum
