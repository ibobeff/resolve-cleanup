/******************************************************************************
* (C) Copyright 2014
*
* Resolve Systems, LLC
*
* All rights reserved
* This software is distributed under license and may not be used, copied,
* modified, or distributed without the express written permission of
* Resolve Systems, LLC.
*
******************************************************************************/

package com.resolvesdk.gateway.sdkjira;

/*
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
*/
import java.util.List;
import java.util.Map;

import com.resolve.gateway.AbstractGatewayAPI;
//import com.resolve.util.StringUtils;

public class SDKJIRAAPI extends AbstractGatewayAPI
{
    static
    {
        instance = SDKJIRAGateway.getInstance();
    }
    
    //SDKJIRA Specific Methods
    
    public static Map<String, String> getServerInfo(String username, 
                                                    String password) throws Exception
    {
        return ((SDKJIRAGateway)instance).getServerInfo(username, password);
    }
    
    public static List<Map<String, String>> search(String urlQuery, 
                                                   String jsonQuery,
                                                   String username, 
                                                   String password) throws Exception
    {
        return ((SDKJIRAGateway)instance).search(urlQuery, jsonQuery, username, password);
    }
    
    public static boolean addComment(String key,
                                     String comment,
                                     String username, 
                                     String password) throws Exception
    {
        return ((SDKJIRAGateway)instance).addComment(key, comment, username, password);
    }
}
