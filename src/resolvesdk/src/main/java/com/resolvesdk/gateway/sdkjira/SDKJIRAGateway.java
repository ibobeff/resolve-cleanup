/******************************************************************************
* (C) Copyright 2014
*
* Resolve Systems, LLC
*
* All rights reserved
* This software is distributed under license and may not be used, copied,
* modified, or distributed without the express written permission of
* Resolve Systems, LLC.
*
******************************************************************************/

package com.resolvesdk.gateway.sdkjira;

//import java.util.ArrayList;
//import java.util.Calendar;
import java.util.Collections;
import java.util.Date;
import java.util.List;
import java.util.Map;
//import java.util.TimeZone;
import java.util.concurrent.ConcurrentHashMap;

import com.resolve.rsbase.MainBase;
import com.resolve.gateway.BaseClusteredGateway;
import com.resolve.gateway.BaseFilter;
import com.resolve.gateway.Filter;
import com.resolve.gateway.MSDKJIRA;
import com.resolve.rsremote.ConfigReceiveGateway;
import com.resolve.util.Constants;
import com.resolve.util.DateUtils;
import com.resolve.util.Log;
import com.resolve.util.StringUtils;

public class SDKJIRAGateway extends BaseClusteredGateway
{
    private static final String KEY = "key";
    private static final String ID = "id";
    
    // Singleton
    private static volatile SDKJIRAGateway instance = null;

    // Queue name will be set to value of QUEUE attribute of <SDKJIRA> element in config.xml
    private String queue;

    //private ConcurrentHashMap<String, String> filterToSubscription = new ConcurrentHashMap<String, String>();
    
    //public static final String ALARM_ID_KEY_NAME = "@id";
    //public static final String DEVICE_ID_KEY_NAME = "@mh";

    public static volatile ObjectService genericObjectService;

    public static SDKJIRAGateway getInstance(ConfigReceiveSDKJIRA config)
    {
        if (instance == null)
        {
            instance = new SDKJIRAGateway(config);
        }
        return instance;
    }

    /**
     * This method is called once the initialization is done. If it gets called
     * before the initialization it will throw exception.
     *
     * @return
     */
    public static SDKJIRAGateway getInstance()
    {
        if (instance == null)
        {
            throw new RuntimeException("SDKJIRA Gateway is not initialized correctly.");
        }
        else
        {
            return instance;
        }
    }

    /**
     * Private constructor.
     *
     * @param config
     */
    private SDKJIRAGateway(ConfigReceiveSDKJIRA config)
    {
        // Important, call super here.
        super(config);
        //resolveQuery = new ResolveQuery(new ServiceNowQueryTranslator());
    }

    public String getLicenseCode()
    {
        return "SDKJIRA";
    }

    protected String getGatewayEventType()
    {
        return "SDKJIRA";
    }

    protected String getMessageHandlerName()
    {
        return MSDKJIRA.class.getSimpleName();
    }

    protected Class<MSDKJIRA> getMessageHandlerClass()
    {
        return MSDKJIRA.class;
    }

    public String getQueueName()
    {
        return queue;
    }

    /**
     * Initializes the specific gateway resources.
     *
     * @param config
     */
    protected void initialize()
    {
        ConfigReceiveSDKJIRA sdkJIRAConfig = (ConfigReceiveSDKJIRA) configurations;

        queue = sdkJIRAConfig.getQueue().toUpperCase();
        this.gatewayConfigDir = "/config/sdkjira/";

        genericObjectService = new GenericObjectService(sdkJIRAConfig);
    }

    public Filter getFilter(Map<String, Object> params)
    {
        return new SDKJIRAFilter((String) params.get(BaseFilter.ID), (String) params.get(BaseFilter.ACTIVE), (String) params.get(BaseFilter.ORDER), (String) params.get(BaseFilter.INTERVAL), 
                                 (String) params.get(BaseFilter.EVENT_EVENTID), (String) params.get(BaseFilter.RUNBOOK), (String) params.get(BaseFilter.SCRIPT), /*(String) params.get(SDKJIRAFilter.PROJECT),*/ 
                                 (String) params.get(SDKJIRAFilter.URLQUERY), (String) params.get(SDKJIRAFilter.JSONQUERY),
                                 (String) params.get(SDKJIRAFilter.LASTISSUEPROCESSEDCREATETIMESTAMP));
    } // getFilter

    @Override
    public void start()
    {
        Log.log.warn("Starting SDKJIRAListener");
        super.start();
    } // start

    //This method acceses the JIRA server for its Server time stamp.
    private String getSDKJIRAServerTimeStamp()
    {
        String result = null;
        
        Map<String, String> serverInfo = Collections.emptyMap();
        
        try
        {
            serverInfo = genericObjectService.getServerInfo(null, null);
            
            if (serverInfo != null && !serverInfo.isEmpty())
            {
                String serverTimeStamp = serverInfo.get("serverTime");
                
                if (StringUtils.isNotEmpty(serverTimeStamp) && serverTimeStamp.length() >= 16)
                {
                    String tresult = serverTimeStamp.substring(0, 16);
                    result = tresult.replace('T', '+');
                }
            }
        }
        catch(Exception e)
        {
            Log.log.warn("Failed to get JIRA ServerInfo, falling back to local time as JIRA server time.", e);
        }
        
        if (StringUtils.isBlank(result))
        {
            result = DateUtils.convertDateToString(new Date(), "yyyy-MM-dd HH:mm");
        }
        
        Log.log.debug("JIRA Server Time Stamp = " + result);
        
        return result;
    }
    
    private String advanceToNextMinute(String currentLastIssueProcessedCreateTimeStamp)
    {
        Long currentMs = DateUtils.convertStringToTimesInMillis(currentLastIssueProcessedCreateTimeStamp,
                                                                "yyyy-MM-dd'+'HH:mm");
        
        //long currentMsAhead = 0l;
        if (currentMs != null)
        {
            /*
            if (Log.log.isDebugEnabled())
            {
                currentMsAhead = currentMs.longValue() - (3 * 60 * 60 * 1000);
                Log.log.debug ("Time at GMT-11 is " + DateUtils.convertTimeInMillisToString(currentMsAhead, "yyyy-MM-dd'+'HH:mm") + 
                               ", Advancd by 1 min is " + DateUtils.convertTimeInMillisToString(currentMsAhead + ((60 * 1000) + 1), "yyyy-MM-dd'+'HH:mm"));
                
            }*/
            // Advance by 1 minute and 1 msec
            
            long advancedMs = currentMs.longValue() + ((60 * 1000) + 1);
            return DateUtils.convertTimeInMillisToString(advancedMs, "yyyy-MM-dd'+'HH:mm");
        }
        
        return currentLastIssueProcessedCreateTimeStamp;
    }
    
    public void run()
    {
        /* This call is must for all the ClusteredGateways.
         * For testing filter(s) deploying through config file only 
         * enable the MOCK mode.
         */
        
        if (!((ConfigReceiveGateway)configurations).isMock())
            super.sendSyncRequest();

        String sdkJIRAServerTime = getSDKJIRAServerTimeStamp();
        
        while (running)
        {
            try
            {
                long startTime = System.currentTimeMillis();

                /* if any remaining events are in primary data queue or 
                 * primary data executor has waiting messages still to be processed, 
                 * do not get messages for more new events.
                 */
                
                if (primaryDataQueue.isEmpty() && !primaryDataQueueExecutorBusy())
                {
                    Log.log.trace(getQueueName() + " Primary Data Queue is empty and Primary Data Queue Executor is free.....");
                    for (Filter filter : orderedFilters)
                    {
                        if (shouldFilterExecute(filter, startTime))
                        {
                            SDKJIRAFilter sdkJIRAFilter = (SDKJIRAFilter) filter;

                            /*
                             * On Creation of new filter Last Issue Processed Create Time will be blank or null, 
                             * then set it to current JIRA Server Time in 'yyyy-MM-dd HH:mm' format 
                             */
                            
                            if (StringUtils.isBlank(sdkJIRAFilter.getLastIssueProcessedCreateTimeStamp()))
                            {
                                sdkJIRAFilter.setLastIssueProcessedCreateTimeStamp(sdkJIRAServerTime);
                            }
                            
                            List<Map<String, String>> results = invokeSDKJIRAService(sdkJIRAFilter);
                            String newLastIssueProcessedCreateTimeStamp = sdkJIRAFilter.getLastIssueProcessedCreateTimeStamp(); 
                            
                            if (results.size() > 0)
                            {
                                // Enqueue runbook excution for each object
                                for (Map<String, String> sdkJIRAObject : results)
                                {
                                    addToPrimaryDataQueue(sdkJIRAFilter, sdkJIRAObject);
                                    
                                    if ((StringUtils.isNotBlank(sdkJIRAFilter.getUrlQuery()) || 
                                         StringUtils.isNotBlank(sdkJIRAFilter.getJsonQuery())) && 
                                        sdkJIRAObject.containsKey(SDKJIRAFilter.ISSUE_CREATE_TIME_STAMP_ATTRIBUTE))
                                    {
                                        String tmpCreateTimeStamp = sdkJIRAObject.get(SDKJIRAFilter.ISSUE_CREATE_TIME_STAMP_ATTRIBUTE);
                                        
                                        if (StringUtils.isNotEmpty(tmpCreateTimeStamp) && tmpCreateTimeStamp.length() >= 16)
                                        {
                                            String tmp = tmpCreateTimeStamp.substring(0, 16);
                                            newLastIssueProcessedCreateTimeStamp = tmp.replace('T', '+');
                                        }
                                    }
                                }
                                
                                if (StringUtils.isNotBlank(sdkJIRAFilter.getUrlQuery()) || 
                                    StringUtils.isNotBlank(sdkJIRAFilter.getJsonQuery()))
                                {
                                    if (!newLastIssueProcessedCreateTimeStamp.equals(sdkJIRAFilter.getLastIssueProcessedCreateTimeStamp()))
                                    {
                                        String lastIssueProcessedTimeStampAdvanced = advanceToNextMinute(newLastIssueProcessedCreateTimeStamp);
                                        Log.log.debug("Setting Last Issue Processed CreateTime Stamp Advanced to " + lastIssueProcessedTimeStampAdvanced +
                                                      " based on current Last Issue Processed CreateTime Stamp of " + newLastIssueProcessedCreateTimeStamp);                                
                                        sdkJIRAFilter.setLastIssueProcessedCreateTimeStamp(lastIssueProcessedTimeStampAdvanced);
                                    }
                                }
                            }
                        }
                        else
                        {
                            Log.log.trace("Filter:" + filter.getId() + " is inactive or not ready to execute yet.....");
                        }
                    }
                }

                // process events collected by processFilter.
                //processFilterEvents(startTime);

                // If there is no filter yet no need to keep spinning, wait for
                // a while
                if (orderedFilters.size() == 0)
                {
                    Thread.sleep(interval);
                }
            }
            catch (Exception e)
            {
                Log.log.warn("Warning: " + e.getMessage(), e);
                Log.alert("Instance " + MainBase.main.configId.getGuid() + " " + getLicenseCode() + "-" + getQueueName() + "-" + getInstanceType() + " Polling Failed", 
                          "Instance " + MainBase.main.configId.getGuid() + " " + getLicenseCode() + "-" + getQueueName() + "-" + getInstanceType() + " Polling Failed " + 
                          "due to " + e.getMessage());
                
                try
                {
                    Thread.sleep(interval);
                }
                catch (InterruptedException ie)
                {
                    Log.log.warn("sleep interrupted: " + ie.getMessage(), ie);
                }
            }
        }
    } // run

    @Override
    protected void cleanFilter(ConcurrentHashMap<String, Filter> undeployedFilters)
    {
        for(Filter filter : undeployedFilters.values())
        {
            SDKJIRAFilter sdkJIRAFilter = (SDKJIRAFilter) filter;
            
            Log.log.debug("Cleaning up undeployed filter " + sdkJIRAFilter);
        }
    }
    
    /**
     * This method invokes the CASpectrum RESTful service.
     *
     * @param filter
     * @return - Service response or null based on filter condition.
     * @throws Exception
     */
    private List<Map<String, String>> invokeSDKJIRAService(SDKJIRAFilter filter) throws Exception
    {
        List<Map<String, String>> result = Collections.emptyList();
        
        Log.log.trace("SDKJIRAFilter " + filter);
        
        if (StringUtils.isNotBlank(filter.getUrlQuery()) || StringUtils.isNotBlank(filter.getJsonQuery()))
        {
            String txUrlQuery = null;
            String txPostQuery = null;
            
            if (StringUtils.isNotBlank(filter.getUrlQuery()))
            {
                txUrlQuery = filter.getUrlQuery().replaceAll("\\$\\{" + SDKJIRAFilter.LASTISSUEPROCESSEDCREATETIMESTAMP + "\\}", 
                                                             filter.getLastIssueProcessedCreateTimeStamp());
                
            }
            
            if (StringUtils.isNotBlank(filter.getJsonQuery()))
            {
                txPostQuery = filter.getJsonQuery().replaceAll("\\$\\{" + SDKJIRAFilter.LASTISSUEPROCESSEDCREATETIMESTAMP + "\\}", 
                                                               filter.getLastIssueProcessedCreateTimeStamp());
            }
            
            Log.log.debug("Transformed URL Query [" + txUrlQuery + "]");
            
            result = genericObjectService.search(/*filter.getProject(),*/ txUrlQuery, txPostQuery, null, null);
        }
        
        return result;
    }

    protected void addAdditionalValues(Map<String, Object> event, Map<String, String> result)
    {
        if(result.containsKey(ID))
        {
            event.put(Constants.EXECUTE_REFERENCE, result.get(ID));
            event.remove(ID);
        }
        else if(result.containsKey(ID.toUpperCase()))
        {
            event.put(Constants.EXECUTE_REFERENCE, result.get(ID.toUpperCase()));
            event.remove(ID.toUpperCase());
        }
        
        if (result.containsKey(KEY))
        {
            event.put(Constants.EXECUTE_ALERTID, result.get(KEY));
            event.remove(KEY);
        }
        else if(result.containsKey(KEY.toUpperCase()))
        {
            event.put(Constants.EXECUTE_ALERTID, result.get(KEY.toUpperCase()));
            event.remove(KEY.toUpperCase());
        }
    }
    
    public boolean checkGatewayConnection() throws Exception
    {
        Map<String, String> serverInfo = Collections.emptyMap();
        
        try
        {
            serverInfo = genericObjectService.getServerInfo(null, null);
            
            if (serverInfo == null || serverInfo.isEmpty())
            {
                throw new Exception ("Failed to get server info from JIRA server. Connection to JIRA server possibly broken.");
            }
            else
            {
                Log.log.debug("Instance " + MainBase.main.configId.getGuid() + " : " + getLicenseCode() + "-" + 
                              getQueueName() + "-" + getInstanceType() + " connected to configured JIRA server");
            }
        }
        catch(Exception e)
        {
            Log.alert("Instance " + MainBase.main.configId.getGuid() + " breached " + getLicenseCode() + "-" + getQueueName() + "-" + getInstanceType() + "-ConnectionSelfCheck", 
                      "Instance " + MainBase.main.configId.getGuid() + " Breached " + getLicenseCode() + "-" + getQueueName() + "-" + getInstanceType() + "-ConnectionSelfCheck" + 
                      " due to " + e.getMessage());
        }
        
        return true;
    }
    /*
    @SuppressWarnings("rawtypes")
    @Override
    public void updateServerData(Filter filter, Map content)
    {
        // Add comment with functional catagory and type
        
        if (content.containsKey("key"))
        {
            StringBuilder strBldr = new StringBuilder("http://10.20.1.106:8080/resolve");

            if (content.containsKey("functional_catagory") && content.containsKey("functional_type"))
            {
                strBldr.append(" Functional Catagory/Type is " + content.get("functional_catagory") + "/" +
                                content.get("functional_type"));
            }
            
            try
            {
                genericObjectService.addComment(content.get("key").toString(), strBldr.toString(), null, null);
            }
            catch (Exception e)
            {
                Log.log.error("Error adding comments to issue " + content.get("key").toString(), e);
            }
        }
    }
    */
    
    /*
     * SDKJIRA Public API (SDKJIRAAPI) Implementation
     */
    
    public Map<String, String> getServerInfo(String username, 
                                             String password) throws Exception
    {
        return genericObjectService.getServerInfo(username, password);
    }
    
    public List<Map<String, String>> search(String urlQuery, 
                                            String jsonQuery,
                                            String username, 
                                            String password) throws Exception
    {
        return genericObjectService.search(urlQuery, jsonQuery, username, password);
    }
    
    public boolean addComment(String key,
                              String comment,
                              String username, 
                              String password) throws Exception
    {
        return genericObjectService.addComment(key, comment, username, password);
    }
    
    /*
    public Map<String, String> issueAnAction(String actionCode, 
                                             String modelHandle, 
                                             Map<String, String> attribValMap, 
                                             int throttlesize,
                                             String username,
                                             String password) throws Exception
    {
        return genericObjectService.issueAnAction(actionCode, modelHandle, attribValMap, throttlesize, username, password);
    }
    
    public List<Map<String, String>> getAlarms(String xml, 
                                               String username,
                                               String password) throws Exception
    {
        return genericObjectService.getObjects("alarms", null, xml, username, password, null, null);
    }
    
    public List<Map<String, String>> getAllAlarms(List<String> attributes,
                                                  List<String> landscapeHandles,
                                                  int throttlesize,
                                                  String username,
                                                  String password) throws Exception
    {
        return genericObjectService.getObjects("alarms", attributes, landscapeHandles, throttlesize, username, password);
    }
    
    public Map<String, String> updateAlarm(String alarmId,
                                           Map<String, String> attribVal,
                                           String username,
                                           String password) throws Exception
    {
        return genericObjectService.updateObject("alarms", alarmId, attribVal, username, password);
    }
    
    public Map<String, String> deleteAlarm(String alarmId,
                                           String username,
                                           String password) throws Exception
    {
        return genericObjectService.deleteObject("alarms", alarmId, username, password);
    }
    
    public Map<String, String> createAssociation(String relationHandle,
                                                 String leftModelHandle,
                                                 String rightModelHandle,
                                                 String username,
                                                 String password) throws Exception
    {
        return genericObjectService.createAssociation(relationHandle, leftModelHandle, rightModelHandle, username, password);
    }
    
    public List<Map<String, String>> getAssociations(String relationHandle,
                                                     String modelHandle,
                                                     String side,
                                                     String username,
                                                     String password) throws Exception
    {
        return genericObjectService.getAssociations(relationHandle, modelHandle, side, username, password);
    }
    
    public Map<String, String> deleteAssociation(String relationHandle,
                                                 String leftModelHandle,
                                                 String rightModelHandle,
                                                 String username,
                                                 String password) throws Exception
    {
        return genericObjectService.deleteAssociation(relationHandle, leftModelHandle, rightModelHandle, username, password);
    }
    
    public Map<String, String> getAttributeEnum(String attrId, 
                                                String username, 
                                                String password) throws Exception
    {
        return genericObjectService.getAttributeEnum(attrId, username, password);
    }
    
    public Map<String, Map<String, List<Map<String, String>>>> getConnectivity(String ipAddress,
                                                                               String username,
                                                                               String password) throws Exception
    {
        return genericObjectService.getConnectivity(ipAddress, username, password);
    }
    
    public List<Map<String, String>> getAllDevices(List<String> attributes,
                                                   List<String> landscapeHandles,
                                                   int throttlesize,
                                                   String username,
                                                   String password) throws Exception
    {
        return genericObjectService.getObjects("devices", attributes, landscapeHandles, throttlesize, username, password);
    }
    
    public List<Map<String, String>> getLandscapes(String username,
                                                   String password) throws Exception
    {
        return genericObjectService.getLandscapes(username, password);
    }
    
    public Map<String, String> createModel(String landscapeHandle,
                                           String modelTypeHandle,
                                           int snmpAgentPort,
                                           String communityString,
                                           int retryCount,
                                           int timeout,
                                           String ipAddress,
                                           String parentModelHandle,                                                
                                           String parentToModelRelationHandle,
                                           Map<String, String> attribValMap,
                                           String username,
                                           String password) throws Exception
    {
        return genericObjectService.createModel(landscapeHandle, modelTypeHandle, snmpAgentPort, 
                                                communityString, retryCount, timeout, ipAddress,
                                                parentModelHandle, parentToModelRelationHandle,
                                                attribValMap, username, password);
    }
    
    public Map<String, String> getModel(String modelHandle,
                                        List<String> attributes,
                                        String username,
                                        String password) throws Exception
    {
        return genericObjectService.getModel(modelHandle, attributes, username, password);
    }
    
    public Map<String, String> updateModel(String modelHandle,
                                           Map<String, String> attribVal,
                                           String username,
                                           String password) throws Exception
    {
        return genericObjectService.updateObject("model", modelHandle, attribVal, username, password);
    }
    
    public Map<String, String> deleteModel(String modelHandle,
                                           String username,
                                           String password) throws Exception
    {
        return genericObjectService.deleteObject("model", modelHandle, username, password);
    }
    
    public List<Map<String, String>> getModels(String xml, 
                                               String username,
                                               String password) throws Exception
    {
        return genericObjectService.getObjects("models", null, xml, username, password, null, null);
    }
    
    public List<Map<String, String>> updateModels(String xml, 
                                                  String username,
                                                  String password) throws Exception
    {
        return genericObjectService.updateObjects("models", xml, username, password);
    }
    
    public Map<String, String> createEvent(String eventType,
                                           String modelHandle,
                                           Map<String, String> varBindIdValue,
                                           String username,
                                           String password) throws Exception
    {
        return genericObjectService.createEvent( eventType, modelHandle, varBindIdValue, username, password);
    }
    
    public List<Map<String, String>> createEvents(String createEventsRequestXml,
                                                  String username,
                                                  String password) throws Exception
    {
        return genericObjectService.createEvents(createEventsRequestXml, username, password);
    }
    
    public String createSubscription(String pullSubscriptionRequestXml,
                                     String username,
                                     String password) throws Exception
    {
        return genericObjectService.createSubscription(pullSubscriptionRequestXml, username, password);
    }
    
    public List<Map<String, String>> getSubscription(String subscriptionId,
                                                     String username,
                                                     String password) throws Exception
    {
        return genericObjectService.getObjects("subscription", null, null, username, password, null, subscriptionId);
    }
    
    public Map<String, String> deleteSubscription(String subscriptionId,
                                                  String username,
                                                  String password) throws Exception
    {
        return genericObjectService.deleteObject("subscription", subscriptionId, username, password);
    }
    */
}
