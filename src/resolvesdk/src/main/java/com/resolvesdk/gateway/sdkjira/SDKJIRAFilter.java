/******************************************************************************
* (C) Copyright 2014
*
* Resolve Systems, LLC
*
* All rights reserved
* This software is distributed under license and may not be used, copied,
* modified, or distributed without the express written permission of
* Resolve Systems, LLC.
*
******************************************************************************/

package com.resolvesdk.gateway.sdkjira;

import com.resolve.gateway.BaseFilter;
import com.resolve.gateway.RetainValue;
import com.resolve.util.StringUtils;

public class SDKJIRAFilter extends BaseFilter
{
    // Additional field names for this filter.
    public static final String URLQUERY = "URLQUERY";
    public static final String JSONQUERY = "JSONQUERY";
    //public static final String PROJECT = "PROJECT";
    public static final String LASTISSUEPROCESSEDCREATETIMESTAMP = "LASTISSUEPROCESSEDCREATETIMESTAMP";
    public static final String ISSUE_CREATE_TIME_STAMP_ATTRIBUTE = "created";

    //private String project;
    private String urlQuery;
    private String jsonQuery;
    
    // Retained Fields
    
    private String lastIssueProcessedCreateTimeStamp; // Last Issue Processed Create Time Stamp in 'yyyy-MM-dd HH:mm' format

    public SDKJIRAFilter(String id, String active, String order, String interval, String eventEventId, String runbook, String script, /*String project,*/ String urlQuery, String jsonQuery, String lastIssueProcessedCreateTimeStamp)
    {
        super(id, active, order, interval, eventEventId, runbook, script);
        //this.project = project;
        setUrlQuery(urlQuery);
        setJsonQuery(jsonQuery);
        setLastIssueProcessedCreateTimeStamp(lastIssueProcessedCreateTimeStamp);
    } // SDKJIRAFilter
    /*
    public String getProject()
    {
        return project;
    }

    public void setProject(String object)
    {
        this.project = project;
    }
    */
    public String getUrlQuery()
    {
        return urlQuery;
    }

    public void setUrlQuery(String urlQuery)
    {
        this.urlQuery = urlQuery != null ? urlQuery.trim() : urlQuery;
    }

    public String getJsonQuery()
    {
        return jsonQuery;
    }

    public void setJsonQuery(String jsonQuery)
    {
        this.jsonQuery = jsonQuery != null ? jsonQuery.trim() : jsonQuery;
    }
    
    @RetainValue
    public String getLastIssueProcessedCreateTimeStamp()
    {
        return lastIssueProcessedCreateTimeStamp;
    }

    public void setLastIssueProcessedCreateTimeStamp(String lastIssueProcessedCreateTimeStamp)
    {
        this.lastIssueProcessedCreateTimeStamp = lastIssueProcessedCreateTimeStamp != null ? lastIssueProcessedCreateTimeStamp.trim() : lastIssueProcessedCreateTimeStamp;
    }
    
    @Override
    public String toString()
    {
        return super.toString() + /*", project=" + project  + */ (StringUtils.isNotBlank(urlQuery) ? ", urlQuery=" + urlQuery : "") + 
               (StringUtils.isNotBlank(jsonQuery) ? ", jsonQuery=" + jsonQuery : "") + 
               (StringUtils.isNotBlank(lastIssueProcessedCreateTimeStamp) ? ", lastIssueProcessedCreateTimeStamp =" + lastIssueProcessedCreateTimeStamp : "");
    }
}
