/******************************************************************************
* (C) Copyright 2015
*
* Resolve Systems, LLC
*
* All rights reserved
* This software is distributed under license and may not be used, copied,
* modified, or distributed without the express written permission of
* Resolve Systems, LLC.
*
******************************************************************************/

package com.resolvesdk.gateway.sdkjira;

import java.util.List;
import java.util.Map;

public interface ObjectService
{
    Map<String, String> getServerInfo(String username, 
                                      String password) throws Exception;
    
    List<Map<String, String>> search(String urlQuery, 
                                     String jsonQuery,
                                     String username, 
                                     String password) throws Exception;
    boolean addComment(String key,
                       String comment,
                       String username, 
                       String password) throws Exception;
}