/******************************************************************************
* (C) Copyright 2014
*
* Resolve Systems, LLC
*
* All rights reserved
* This software is distributed under license and may not be used, copied,
* modified, or distributed without the express written permission of
* Resolve Systems, LLC.
*
******************************************************************************/

package com.resolvesdk.gateway.sdkcaspectrum;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

import com.resolve.rsbase.MainBase;
import com.resolve.gateway.BaseClusteredGateway;
import com.resolve.gateway.BaseFilter;
import com.resolve.gateway.Filter;
import com.resolve.gateway.MSDKCASpectrum;
import com.resolve.rsremote.ConfigReceiveGateway;
import com.resolve.util.Constants;
import com.resolve.util.Log;
import com.resolve.util.StringUtils;

public class SDKCASpectrumGateway extends BaseClusteredGateway
{
    // Singleton
    private static volatile SDKCASpectrumGateway instance = null;

    // Queue name will be set to value of QUEUE attribute of <SDKCASPECTRUM> element in config.xml
    private String queue;

    private ConcurrentHashMap<String, String> filterToSubscription = new ConcurrentHashMap<String, String>();
    
    public static final String ALARM_ID_KEY_NAME = "@id";
    public static final String DEVICE_ID_KEY_NAME = "@mh";

    public static volatile ObjectService genericObjectService;

    public static SDKCASpectrumGateway getInstance(ConfigReceiveSDKCASpectrum config)
    {
        if (instance == null)
        {
            instance = new SDKCASpectrumGateway(config);
        }
        return instance;
    }

    /**
     * This method is called once the initialization is done. If it gets called
     * before the initialization it will throw exception.
     *
     * @return
     */
    public static SDKCASpectrumGateway getInstance()
    {
        if (instance == null)
        {
            throw new RuntimeException("SDKCASpectrum Gateway is not initialized correctly.");
        }
        else
        {
            return instance;
        }
    }

    /**
     * Private constructor.
     *
     * @param config
     */
    private SDKCASpectrumGateway(ConfigReceiveSDKCASpectrum config)
    {
        // Important, call super here.
        super(config);
        //resolveQuery = new ResolveQuery(new ServiceNowQueryTranslator());
    }

    public String getLicenseCode()
    {
        return "SDKCASPECTRUM";
    }

    protected String getGatewayEventType()
    {
        return "SDKCASPECTRUM";
    }

    protected String getMessageHandlerName()
    {
        return MSDKCASpectrum.class.getSimpleName();
    }

    protected Class<MSDKCASpectrum> getMessageHandlerClass()
    {
        return MSDKCASpectrum.class;
    }

    public String getQueueName()
    {
        return queue;
    }

    /**
     * Initializes the specific gateway resources.
     *
     * @param config
     */
    protected void initialize()
    {
        ConfigReceiveSDKCASpectrum sdkCASpectrumConfig = (ConfigReceiveSDKCASpectrum) configurations;

        queue = sdkCASpectrumConfig.getQueue().toUpperCase();
        this.gatewayConfigDir = "/config/sdkcaspectrum/";

        genericObjectService = new GenericObjectService(sdkCASpectrumConfig, filterToSubscription);
    }

    public Filter getFilter(Map<String, Object> params)
    {
        return new SDKCASpectrumFilter((String) params.get(BaseFilter.ID), (String) params.get(BaseFilter.ACTIVE), (String) params.get(BaseFilter.ORDER), (String) params.get(BaseFilter.INTERVAL), 
                                    (String) params.get(BaseFilter.EVENT_EVENTID), (String) params.get(BaseFilter.RUNBOOK), (String) params.get(BaseFilter.SCRIPT), (String) params.get(SDKCASpectrumFilter.OBJECT), 
                                    (String) params.get(SDKCASpectrumFilter.URLQUERY), (String) params.get(SDKCASpectrumFilter.XMLQUERY),
                                    (String) params.get(SDKCASpectrumFilter.LATESTALARMTIME));
    } // getFilter

    @Override
    public void start()
    {
        Log.log.warn("Starting SDKCASpectrumListener");
        super.start();
    } // start

    /**
     * This method should acces the SDK CA Spectrum server for its current date
     * and time in GMT. Need to create or access REST service in
     * CA Spectrum server for this. If we do not create or access the REST service then we
     * will use whatever current Date and Time in the Resolve server which may
     * be out of sync with CA Spectrum server.
     */
    private String getSDKCASpectrumServerTimeInSec()
    {
        String result = null;
        
        // Code to access CA Spectrum Date Time Service
        
        if (StringUtils.isBlank(result))
        {
            Date currDate = new Date();
            result = Long.toString((int)(currDate.getTime()/1000));
        }
        
        return result;
    }
    
    public void run()
    {
        /* This call is must for all the ClusteredGateways.
         * For testing filter(s) deploying through config file only 
         * enable the MOCK mode.
         */
        
        if (!((ConfigReceiveGateway)configurations).isMock())
            super.sendSyncRequest();

        String sdkCASpectrumServerTimeInSec = getSDKCASpectrumServerTimeInSec();
        
        while (running)
        {
            try
            {
                long startTime = System.currentTimeMillis();

                /* if any remaining events are in primary data queue or 
                 * primary data executor has waiting messages still to be processed, 
                 * do not get messages for more new events.
                 */
                
                if (primaryDataQueue.isEmpty() && !primaryDataQueueExecutorBusy())
                {
                    Log.log.trace(getQueueName() + " Primary Data Queue is empty and Primary Data Queue Executor is free.....");
                    
                    for (Filter filter : orderedFilters)
                    {
                        if (shouldFilterExecute(filter, startTime))
                        {
                            SDKCASpectrumFilter caSpectrumFilter = (SDKCASpectrumFilter) filter;

                            /*
                             * On Creation of new filter Latest Alarm (Processed) Time will be blank or null, 
                             * then set it to current CA Spectrum Server GMT Time (in sec)  
                             */
                            
                            if (StringUtils.isBlank(caSpectrumFilter.getLatestAlarmTime()))
                            {
                                caSpectrumFilter.setLatestAlarmTime(sdkCASpectrumServerTimeInSec);
                            }
                            
                            List<Map<String, String>> results = invokeSDKCASpectrumService(caSpectrumFilter);
                            long newLatestAlarmTime = Long.parseLong(caSpectrumFilter.getLatestAlarmTime()); 
                            if (results.size() > 0)
                            {
                                // Enqueue runbook excution for each object
                                for (Map<String, String> caSpectrumObject : results)
                                {
                                    addToPrimaryDataQueue(caSpectrumFilter, caSpectrumObject);
                                    
                                    if (StringUtils.isNotBlank(caSpectrumFilter.getXmlQuery()) && caSpectrumObject.containsKey(SDKCASpectrumFilter.LAST_OCCURRENCE_TIME_ATTRIBUTE))
                                    {
                                        long alarmLastOccrTime = Long.parseLong(caSpectrumObject.get(SDKCASpectrumFilter.LAST_OCCURRENCE_TIME_ATTRIBUTE));
                                        
                                        if (alarmLastOccrTime == 0)
                                        {
                                            Log.log.debug("Alarm Last Occurrence Time (" + SDKCASpectrumFilter.LAST_OCCURRENCE_TIME_ATTRIBUTE + 
                                                          ") is 0, checking Alarm First Occurrence Time (" + SDKCASpectrumFilter.FIRST_OCCURRENCE_TIME_ATTRIBUTE + ")");
                                            
                                            if (caSpectrumObject.containsKey(SDKCASpectrumFilter.FIRST_OCCURRENCE_TIME_ATTRIBUTE))
                                            {
                                                alarmLastOccrTime = Long.parseLong(caSpectrumObject.get(SDKCASpectrumFilter.FIRST_OCCURRENCE_TIME_ATTRIBUTE));
                                                
                                                if (alarmLastOccrTime == 0)
                                                {
                                                    Log.log.warn("Both Alarm Last Occurrence Time (" + SDKCASpectrumFilter.LAST_OCCURRENCE_TIME_ATTRIBUTE + 
                                                                 ") and First Occurrence Time (" + SDKCASpectrumFilter.FIRST_OCCURRENCE_TIME_ATTRIBUTE + ") are 0, " +
                                                                 SDKCASpectrumFilter.LATESTALARMTIME + " will not be updated");
                                                }
                                            }
                                            else
                                            {
                                                Log.log.warn("CA Spectrum response is missing Alarm First Occurrence Attribute (" + 
                                                             SDKCASpectrumFilter.FIRST_OCCURRENCE_TIME_ATTRIBUTE + ")");
                                            }
                                        }
                                        
                                        if (alarmLastOccrTime > newLatestAlarmTime)
                                        {
                                            newLatestAlarmTime = alarmLastOccrTime;
                                        }
                                    }
                                }
                                
                                if (StringUtils.isNotBlank(caSpectrumFilter.getXmlQuery()))
                                {
                                    Log.log.debug("Setting Latest Alarm Time to " + newLatestAlarmTime);
                                    caSpectrumFilter.setLatestAlarmTime(Long.toString(newLatestAlarmTime));
                                }
                            }
                        }
                        else
                        {
                            Log.log.trace("Filter:" + filter.getId() + " is inactive or not ready to execute yet.....");
                        }
                    }
                }

                // process events collected by processFilter.
                //processFilterEvents(startTime);

                // If there is no filter yet no need to keep spinning, wait for
                // a while
                if (orderedFilters.size() == 0)
                {
                    Thread.sleep(interval);
                }
            }
            catch (Exception e)
            {
                Log.log.warn("Warning: " + e.getMessage());
                Log.alert("Instance " + MainBase.main.configId.getGuid() + " " + getLicenseCode() + "-" + getQueueName() + "-" + getInstanceType() + " Polling Failed", 
                                "Instance " + MainBase.main.configId.getGuid() + " " + getLicenseCode() + "-" + getQueueName() + "-" + getInstanceType() + " Polling Failed " + 
                                "due to " + e.getMessage());
                
                try
                {
                    Thread.sleep(interval);
                }
                catch (InterruptedException ie)
                {
                    Log.log.warn("sleep interrupted: " + ie.getMessage(), ie);
                }
            }
        }
    } // run

    @Override
    protected void cleanFilter(ConcurrentHashMap<String, Filter> undeployedFilters)
    {
        for(Filter filter : undeployedFilters.values())
        {
            SDKCASpectrumFilter caSpectrumFilter = (SDKCASpectrumFilter) filter;
            
            if (filterToSubscription.containsKey(caSpectrumFilter.getId()))
            {
                try
                {
                    deleteSubscription(filterToSubscription.get(caSpectrumFilter.getId()), null, null);
                }
                catch (Exception e)
                {
                    //Ignore subscription deletion exceptions as CA Spectrum will delete it any way after timeout
                }
                
                filterToSubscription.remove(caSpectrumFilter.getId());
            }
        }
    }
    
    /**
     * This method invokes the CASpectrum RESTful service.
     *
     * @param filter
     * @return - Service response or null based on filter condition.
     * @throws Exception
     */
    private List<Map<String, String>> invokeSDKCASpectrumService(SDKCASpectrumFilter filter) throws Exception
    {
        List<Map<String, String>> result = Collections.emptyList();
        
        Log.log.trace("SDKCASpectrumFilter " + filter);
        
        if (StringUtils.isNotBlank(filter.getObject()))
        {
            if (StringUtils.isNotBlank(filter.getUrlQuery()) || filter.getObject().equalsIgnoreCase("landscapes") )
            {
                result = genericObjectService.getObjects(filter.getObject(), filter.getUrlQuery(), null, null, null, filter.getId(), null);
            }
            else if (StringUtils.isNotBlank(filter.getXmlQuery()))
            {
                String txFormedXMLQuery = filter.getXmlQuery().replaceAll("\\$\\{" + SDKCASpectrumFilter.LATESTALARMTIME + "\\}", filter.getLatestAlarmTime());
                Log.log.trace("Transformed XML Query: [" + txFormedXMLQuery + "]");
                result = genericObjectService.getObjects(filter.getObject(), null, txFormedXMLQuery, null, null, filter.getId(), null);
            }
        }
        
        return result;
    }

    protected void addAdditionalValues(Map<String, Object> event, Map<String, String> result)
    {
        return;
    }
    
    @Override
    protected void addAdditionalValues(Map<String, Object> event, Map<String, String> result, Filter filter)
    {
        List<String> objectIdKeyNames = new ArrayList<String>();
        
        switch (((SDKCASpectrumFilter)filter).getObject())
        {
            case "alarms":
                objectIdKeyNames.add(ALARM_ID_KEY_NAME);
                break;
                
            case "devices":
                objectIdKeyNames.add(DEVICE_ID_KEY_NAME);
                break;
            
            case "subscription":
                objectIdKeyNames.add(ALARM_ID_KEY_NAME);
                objectIdKeyNames.add(DEVICE_ID_KEY_NAME);
                break;
        }
        
        if (!objectIdKeyNames.isEmpty())
        {
            for (String objectIdKeyName : objectIdKeyNames)
            {
                if(result.containsKey(objectIdKeyName))
                {
                    event.put(Constants.EXECUTE_REFERENCE, result.get(objectIdKeyName));
                    event.remove(objectIdKeyName);
                    break;
                }
                else if(result.containsKey(objectIdKeyName.toUpperCase()))
                {
                    event.put(Constants.EXECUTE_REFERENCE, result.get(objectIdKeyName.toUpperCase()));
                    event.remove(objectIdKeyName.toUpperCase());
                    break;
                }
            }
        }
    }

    /*
     * SDKCASpectrum Public API (SDKCASpectrumAPI) Implementation
     */
    
    public Map<String, String> issueAnAction(String actionCode, 
                                             String modelHandle, 
                                             Map<String, String> attribValMap, 
                                             int throttlesize,
                                             String username,
                                             String password) throws Exception
    {
        return genericObjectService.issueAnAction(actionCode, modelHandle, attribValMap, throttlesize, username, password);
    }
    
    public List<Map<String, String>> getAlarms(String xml, 
                                               String username,
                                               String password) throws Exception
    {
        return genericObjectService.getObjects("alarms", null, xml, username, password, null, null);
    }
    
    public List<Map<String, String>> getAllAlarms(List<String> attributes,
                                                  List<String> landscapeHandles,
                                                  int throttlesize,
                                                  String username,
                                                  String password) throws Exception
    {
        return genericObjectService.getObjects("alarms", attributes, landscapeHandles, throttlesize, username, password);
    }
    
    public Map<String, String> updateAlarm(String alarmId,
                                           Map<String, String> attribVal,
                                           String username,
                                           String password) throws Exception
    {
        return genericObjectService.updateObject("alarms", alarmId, attribVal, username, password);
    }
    
    public Map<String, String> deleteAlarm(String alarmId,
                                           String username,
                                           String password) throws Exception
    {
        return genericObjectService.deleteObject("alarms", alarmId, username, password);
    }
    
    public Map<String, String> createAssociation(String relationHandle,
                                                 String leftModelHandle,
                                                 String rightModelHandle,
                                                 String username,
                                                 String password) throws Exception
    {
        return genericObjectService.createAssociation(relationHandle, leftModelHandle, rightModelHandle, username, password);
    }
    
    public List<Map<String, String>> getAssociations(String relationHandle,
                                                     String modelHandle,
                                                     String side,
                                                     String username,
                                                     String password) throws Exception
    {
        return genericObjectService.getAssociations(relationHandle, modelHandle, side, username, password);
    }
    
    public Map<String, String> deleteAssociation(String relationHandle,
                                                 String leftModelHandle,
                                                 String rightModelHandle,
                                                 String username,
                                                 String password) throws Exception
    {
        return genericObjectService.deleteAssociation(relationHandle, leftModelHandle, rightModelHandle, username, password);
    }
    
    public Map<String, String> getAttributeEnum(String attrId, 
                                                String username, 
                                                String password) throws Exception
    {
        return genericObjectService.getAttributeEnum(attrId, username, password);
    }
    
    public Map<String, Map<String, List<Map<String, String>>>> getConnectivity(String ipAddress,
                                                                               String username,
                                                                               String password) throws Exception
    {
        return genericObjectService.getConnectivity(ipAddress, username, password);
    }
    
    public List<Map<String, String>> getAllDevices(List<String> attributes,
                                                   List<String> landscapeHandles,
                                                   int throttlesize,
                                                   String username,
                                                   String password) throws Exception
    {
        return genericObjectService.getObjects("devices", attributes, landscapeHandles, throttlesize, username, password);
    }
    
    public List<Map<String, String>> getLandscapes(String username,
                                                   String password) throws Exception
    {
        return genericObjectService.getLandscapes(username, password);
    }
    
    public Map<String, String> createModel(String landscapeHandle,
                                           String modelTypeHandle,
                                           int snmpAgentPort,
                                           String communityString,
                                           int retryCount,
                                           int timeout,
                                           String ipAddress,
                                           String parentModelHandle,                                                
                                           String parentToModelRelationHandle,
                                           Map<String, String> attribValMap,
                                           String username,
                                           String password) throws Exception
    {
        return genericObjectService.createModel(landscapeHandle, modelTypeHandle, snmpAgentPort, 
                                                communityString, retryCount, timeout, ipAddress,
                                                parentModelHandle, parentToModelRelationHandle,
                                                attribValMap, username, password);
    }
    
    public Map<String, String> getModel(String modelHandle,
                                        List<String> attributes,
                                        String username,
                                        String password) throws Exception
    {
        return genericObjectService.getModel(modelHandle, attributes, username, password);
    }
    
    public Map<String, String> updateModel(String modelHandle,
                                           Map<String, String> attribVal,
                                           String username,
                                           String password) throws Exception
    {
        return genericObjectService.updateObject("model", modelHandle, attribVal, username, password);
    }
    
    public Map<String, String> deleteModel(String modelHandle,
                                           String username,
                                           String password) throws Exception
    {
        return genericObjectService.deleteObject("model", modelHandle, username, password);
    }
    
    public List<Map<String, String>> getModels(String xml, 
                                               String username,
                                               String password) throws Exception
    {
        return genericObjectService.getObjects("models", null, xml, username, password, null, null);
    }
    
    public List<Map<String, String>> updateModels(String xml, 
                                                  String username,
                                                  String password) throws Exception
    {
        return genericObjectService.updateObjects("models", xml, username, password);
    }
    
    public Map<String, String> createEvent(String eventType,
                                           String modelHandle,
                                           Map<String, String> varBindIdValue,
                                           String username,
                                           String password) throws Exception
    {
        return genericObjectService.createEvent( eventType, modelHandle, varBindIdValue, username, password);
    }
    
    public List<Map<String, String>> createEvents(String createEventsRequestXml,
                                                  String username,
                                                  String password) throws Exception
    {
        return genericObjectService.createEvents(createEventsRequestXml, username, password);
    }
    
    public String createSubscription(String pullSubscriptionRequestXml,
                                     String username,
                                     String password) throws Exception
    {
        return genericObjectService.createSubscription(pullSubscriptionRequestXml, username, password);
    }
    
    public List<Map<String, String>> getSubscription(String subscriptionId,
                                                     String username,
                                                     String password) throws Exception
    {
        return genericObjectService.getObjects("subscription", null, null, username, password, null, subscriptionId);
    }
    
    public Map<String, String> deleteSubscription(String subscriptionId,
                                                  String username,
                                                  String password) throws Exception
    {
        return genericObjectService.deleteObject("subscription", subscriptionId, username, password);
    }
}
