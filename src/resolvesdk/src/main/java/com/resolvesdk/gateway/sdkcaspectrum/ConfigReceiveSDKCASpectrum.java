/******************************************************************************
* (C) Copyright 2014
*
* Resolve Systems, LLC
*
* All rights reserved
* This software is distributed under license and may not be used, copied,
* modified, or distributed without the express written permission of
* Resolve Systems, LLC.
*
******************************************************************************/

package com.resolvesdk.gateway.sdkcaspectrum;

import java.io.File;
import java.util.HashMap;
import java.util.Map;

import org.apache.commons.io.FileUtils;

import com.resolve.rsbase.MainBase;
import com.resolve.gateway.Filter;
import com.resolve.rsremote.ConfigReceiveGateway;
import com.resolve.util.Log;
import com.resolve.util.ObjectProperties;
import com.resolve.util.StringUtils;
import com.resolve.util.XDoc;

/**
 * This class loads up the configuration for SDKCASpectrum gateway. It reads the
 * configuration file from rsremote/config/config.xml. The element of interest
 * is:
 *
 * <SDKCASPECTRUM....../>
 */
public class ConfigReceiveSDKCASpectrum extends ConfigReceiveGateway
{
    private static final long serialVersionUID = 1L;

    private static final String RECEIVE_SDKCASPECTRUM_NODE = "./RECEIVE/SDKCASPECTRUM/";
    private static final String RECEIVE_SDKCASPECTRUM_FILTER = RECEIVE_SDKCASPECTRUM_NODE + "FILTER";
    //private static final String RECEIVE_SDKCASPECTRUM_PROPERTIES = RECEIVE_SDKCASPECTRUM_NODE + "PROPERTIES";

    // http://<ca spectrum server>:<port>/spectrum/restful
    private static final String RECEIVE_SDKCASPECTRUM_ATTR_URL = RECEIVE_SDKCASPECTRUM_NODE + "@URL";
    private static final String RECEIVE_SDKCASPECTRUM_ATTR_HTTPBASICAUTH_USERNAME = RECEIVE_SDKCASPECTRUM_NODE + "@HTTPBASICAUTHUSERNAME";
    private static final String RECEIVE_SDKCASPECTRUM_ATTR_HTTPBASICAUTH_P_ASSWORD = RECEIVE_SDKCASPECTRUM_NODE + "@HTTPBASICAUTHPASSWORD";
    //private static final String RECEIVE_SDKCASPECTRUM_ATTR_DATETIME_WEBSERVICE_NAME = RECEIVE_SDKCASPECTRUM_NODE + "@DATETIMEWEBSERVICENAME";
    private static final String RECEIVE_SDKCASPECTRUM_ATTR_THROTTLESIZE = RECEIVE_SDKCASPECTRUM_NODE + "@THROTTLESIZE";
    private static final int RECEIVE_SDKCASPECTRUM_DEFAULT_THROTTLESIZE = 1000;

    //private boolean active = false;
    private String url = "";
    private int interval = 120;
    private String httpbasicauthusername = "";
    private String httpbasicauthp_assword = "";
    private String queue = "CASPECTRUM";
    //private String datetimewebservicename = "";
    // private String changeDateField = "CHANGEDATE";
    private int throttleSize = RECEIVE_SDKCASPECTRUM_DEFAULT_THROTTLESIZE;

    public ConfigReceiveSDKCASpectrum(XDoc config) throws Exception
    {
        super(config);

        define("url", STRING, RECEIVE_SDKCASPECTRUM_ATTR_URL);
        define("httpbasicauthusername", STRING, RECEIVE_SDKCASPECTRUM_ATTR_HTTPBASICAUTH_USERNAME);
        define("httpbasicauthp_assword", SECURE, RECEIVE_SDKCASPECTRUM_ATTR_HTTPBASICAUTH_P_ASSWORD);
        //define("datetimewebservicename", STRING, RECEIVE_CASPECTRUM_ATTR_DATETIME_WEBSERVICE_NAME);
        define("throttleSize", INT, RECEIVE_SDKCASPECTRUM_ATTR_THROTTLESIZE);
    } // ConfigReceiveServiceNow

    public String getRootNode()
    {
        return RECEIVE_SDKCASPECTRUM_NODE;
    }

    @SuppressWarnings("unchecked")
    public void load() throws Exception
    {
        try
        {
            loadAttributes();
            
            if (isActive()) // no need to unnecessarily load anything if the gateway
                            // is inactive.
            {
                SDKCASpectrumGateway sdkCASpectrumGateway = SDKCASpectrumGateway.getInstance(this);

                // filters
                filters = xdoc.getListMapValue(RECEIVE_SDKCASPECTRUM_FILTER);

                // initialize filters
                if (filters.size() > 0)
                {
                    for (Map<String, Object> values : filters)
                    {
                        //it's important to make it lowercase because we store the files in lowercase
                        //without lowering it, linux systems have problem of not reading the script content.
                        String filterName = ((String)values.get(SDKCASpectrumFilter.ID)).toLowerCase();
                        File scriptFile = getFile(MainBase.main.release.serviceName + "/config/sdkcaspectrum/" + filterName + ".groovy");

                        try
                        {
                            if (scriptFile.exists())
                            {
                                String groovy = FileUtils.readFileToString(scriptFile, "UTF-8");
                                if (StringUtils.isNotBlank(groovy))
                                {
                                    values.put(SDKCASpectrumFilter.SCRIPT, groovy);
                                    Log.log.debug("Loaded groovy script: " + scriptFile);
                                }
                            }

                            // init filter
                            sdkCASpectrumGateway.setFilter(values);
                        }
                        catch (Exception e)
                        {
                            Log.log.warn(e.getMessage() + ". Skipping file: " + scriptFile.getAbsolutePath());
                        }
                    }
                }
            }
        }
        catch (Throwable e)
        {
            //deactivate it
            setActive(false);
            Log.log.warn("Couldn not load configuration for CA Spectrum gateway, check blueprint. " + e.getMessage(), e);
        }
    } // load

    @SuppressWarnings({ "unchecked", "rawtypes" })
    public void save() throws Exception
    {
        // create caspectrum directory
        File dir = getFile(MainBase.main.release.serviceName + "/config/caspectrum");
        if (!dir.exists())
        {
            FileUtils.forceMkdir(dir);
        }
        
        // clear list of filters to save
        if (filters != null && isActive())
        {
            filters.clear();

            // save sql files
            for (Filter filter : SDKCASpectrumGateway.getInstance().getFilters().values())
            {
                SDKCASpectrumFilter caSpectrumFilter = (SDKCASpectrumFilter) filter;

                String groovy = caSpectrumFilter.getScript();
                
                String scriptFileName;
                File scriptFile = null;
                
                if (StringUtils.isNotBlank(groovy))
                {
                    scriptFileName = caSpectrumFilter.getId().toLowerCase() + ".groovy";
                    scriptFile = getFile(MainBase.main.release.serviceName + "/config/caspectrum/" + scriptFileName);
                }
                
                // add xdoc entry
                Map<String, Object> entry = new HashMap<String, Object>();

                entry.put(SDKCASpectrumFilter.ID, caSpectrumFilter.getId());
                entry.put(SDKCASpectrumFilter.ACTIVE, String.valueOf(caSpectrumFilter.isActive()));
                entry.put(SDKCASpectrumFilter.ORDER, String.valueOf(caSpectrumFilter.getOrder()));
                entry.put(SDKCASpectrumFilter.INTERVAL, String.valueOf(caSpectrumFilter.getInterval()));
                entry.put(SDKCASpectrumFilter.EVENT_EVENTID, caSpectrumFilter.getEventEventId());
                entry.put(SDKCASpectrumFilter.RUNBOOK, caSpectrumFilter.getRunbook());
                entry.put(SDKCASpectrumFilter.SCRIPT, caSpectrumFilter.getScript());
                entry.put(SDKCASpectrumFilter.URLQUERY, caSpectrumFilter.getUrlQuery());
                entry.put(SDKCASpectrumFilter.XMLQUERY, caSpectrumFilter.getXmlQuery());
                entry.put(SDKCASpectrumFilter.OBJECT, caSpectrumFilter.getObject());
                entry.put(SDKCASpectrumFilter.LATESTALARMTIME, caSpectrumFilter.getLatestAlarmTime());
                
                filters.add(entry);

                if (scriptFile != null)
                {
                    try
                    {
                        // create groovy file
                        FileUtils.writeStringToFile(scriptFile, groovy, "UTF-8");
                    }
                    catch (Exception e)
                    {
                        Log.log.warn(e.getMessage() + ". Skipping file: " + scriptFile.getAbsolutePath());
                    }
                }
            }

            // filters
            xdoc.setListMapValue(RECEIVE_SDKCASPECTRUM_FILTER, filters);
        }

        // clear list of properties to save
        if (nameProperties != null && isActive())
        {
            // empty properties
            nameProperties.clear();

            // recreate properties
            for (String name : SDKCASpectrumGateway.getInstance().getNameProperties().keySet())
            {
                // add xdoc entry
                Map<String, Object> entry = new HashMap<String, Object>();
                entry.put(NAME_KEY, name);
                nameProperties.add(entry);

                Map props = SDKCASpectrumGateway.getInstance().getNameProperties().getNameProperties(name);
                ObjectProperties prop = new ObjectProperties(props);
                prop.save(props);
            }
        }

        saveAttributes();
    } // save

    public String getUrl()
    {
        return url;
    }

    public void setUrl(String url)
    {
        this.url = url;
    }

    public int getInterval()
    {
        return interval;
    }

    public void setInterval(int interval)
    {
        this.interval = interval;
    }

    public String getHttpbasicauthusername()
    {
        return httpbasicauthusername;
    }

    public void setHttpbasicauthusername(String httpbasicauthusername)
    {
        this.httpbasicauthusername = httpbasicauthusername;
    }

    public String getHttpbasicauthp_assword()
    {
        return httpbasicauthp_assword;
    }

    public void setHttpbasicauthp_assword(String httpbasicauthpassword)
    {
        this.httpbasicauthp_assword = httpbasicauthpassword;
    }
    
    public String getQueue()
    {
        return queue;
    }

    public void setQueue(String queue)
    {
        this.queue = queue;
    }
    
    public int getThrottleSize()
    {
        return throttleSize;
    }

    public void setThrottleSize(int throttleSize)
    {
        this.throttleSize = throttleSize;
    }
}
