/******************************************************************************
* (C) Copyright 2014
*
* Resolve Systems, LLC
*
* All rights reserved
* This software is distributed under license and may not be used, copied,
* modified, or distributed without the express written permission of
* Resolve Systems, LLC.
*
******************************************************************************/

package com.resolvesdk.gateway.sdkcaspectrum;

import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import com.resolve.gateway.AbstractGatewayAPI;
import com.resolve.util.StringUtils;

public class SDKCASpectrumAPI extends AbstractGatewayAPI
{
    static
    {
        instance = SDKCASpectrumGateway.getInstance();
    }
    
    //SDKCASpectrum Specific Methods
    
    /**
     * Issues an action request specified by action code on SpectroSERVER.
     *
     * For details on parameters refer to "Web Services API Reference Guide (Release 9.4)
     * Chapter 4:RESTful Resources:action:GET action
     * 
     * @param actionCode
     * @param modelHandle 
     * @param attribValMap (optional) 
     *              to pass data to action
     * @param throttleSize
     *              between 1 to 1000 (both inclusive) any other value is capped to 1000 
     * @param username
     *              if not provided uses gateway configured username
     * @param password
     *              tied with username
     * @return a {@link Map} containing acion response attributes. 
     *                       Contents differ depending on reuested action.
     * @throws Exception
     */
    public static Map<String, String> issueAnAction(String actionCode, 
                                                    String modelHandle, 
                                                    Map<String, String> attribValMap, 
                                                    int throttlesize,
                                                    String username,
                                                    String password) throws Exception
    {
        return ((SDKCASpectrumGateway)instance).issueAnAction(actionCode, modelHandle, attribValMap, throttlesize, username, password);
    }
    
    /**
     * Get alarms based on filter conditions in specified xml query string.
     * 
     * For details on parameters and returned alarm data refer to 
     * "Web Services API Reference Guide (Release 9.4)
     * Chapter 4:RESTful Resources:alarms:POST alarms (GET Tunneling)
     * 
     * @param xml
     * @param username
     *              if not provided uses gateway configured username
     * @param password
     *              tied with username
     * @return a {@link List} containing {@link Map} of alarm attributes.
     *                        
     * @throws Exception
     */
    public static List<Map<String, String>> getAlarms(String xml, 
                                                      String username,
                                                      String password) throws Exception
    {
        return ((SDKCASpectrumGateway)instance).getAlarms(xml, username, password);
    }
    
    /**
     * Gets all alarms.
     * 
     * For details on parameters and returned alarm data refer to 
     * "Web Services API Reference Guide (Release 9.4)
     * Chapter 4:RESTful Resources:alarms:GET alarms
     * 
     * @param attributes (optional) List of alarm attributes to retrieve for each alarm
     *              Alarm id is returned by default
     * @param landscapeHandle (optional) List of landscape handles
     * @param throttlesize Sets # of alarms to retrieve in single request to server
     *              If  total # of alarms is more than throttle size then multiple reqeusts are made to
     *              server to retrieve all alarms.
     *              Between 1 to 1000 (both inclusive) any other value is capped to 1000
     * @param username
     *              if not provided uses gateway configured username
     * @param password
     *              tied with username
     * @return a {@link List} containing {@link Map} of alarm attributes.
     *                        
     * @throws Exception
     */
    public static List<Map<String, String>> getAllAlarms(List<String> attributes,
                                                         List<String> landscapeHandles,
                                                         int throttlesize,
                                                         String username,
                                                         String password) throws Exception
    {
        return ((SDKCASpectrumGateway)instance).getAllAlarms(attributes, landscapeHandles, throttlesize, username, password);
    }
    
    /**
     * Update alarm.
     * 
     * For details on parameters and returned data refer to 
     * "Web Services API Reference Guide (Release 9.4)
     * Chapter 4:RESTful Resources:alarms:PUT alarms
     * 
     * @param alarmId Id of the alarm to update.
     * @param attribVal Map of attributes to update as key and values to modify as value
     * @param username
     *              if not provided, uses gateway configured username
     * @param password
     *              tied with username
     * @return a {@link Map} of alarm attribute update operation result.
     *                        
     * @throws Exception
     */
    public static Map<String, String> updateAlarm(String alarmId,
                                                  Map<String, String> attribVal,
                                                  String username,
                                                  String password) throws Exception
    {
        return ((SDKCASpectrumGateway)instance).updateAlarm(alarmId, attribVal, username, password);
    }
    
    /**
     * Delete alarm.
     * 
     * For details on parameters and returned data refer to 
     * "Web Services API Reference Guide (Release 9.4)
     * Chapter 4:RESTful Resources:alarms:PUT alarms
     * 
     * @param alarmId Id of the alarm to delete.
     * @param username
     *              if not provided, uses gateway configured username
     * @param password
     *              tied with username
     * @return a {@link Map} of alarm delete operation result.
     *                        
     * @throws Exception
     */
    public static Map<String, String> deleteAlarm(String alarmId,
                                                  String username,
                                                  String password) throws Exception
    {
        return ((SDKCASpectrumGateway)instance).deleteAlarm(alarmId, username, password);
    }
    
    /**
     * Create an association between two models.
     * 
     * For details on parameters and returned data refer to 
     * "Web Services API Reference Guide (Release 9.4)
     * Chapter 4:RESTful Resources:alarms:POST associations
     * 
     * @param relationHandle
     * @param leftModelHandle
     * @param rightModelHandle
     * @param username
     *              if not provided, uses gateway configured username
     * @param password
     *              tied with username
     * @return a {@link Map} of results of create association operation.
     */
    public static Map<String, String> createAssociation(String relationHandle,
                                                        String leftModelHandle,
                                                        String rightModelHandle,
                                                        String username,
                                                        String password) throws Exception
    {
        return ((SDKCASpectrumGateway)instance).createAssociation(relationHandle, leftModelHandle, rightModelHandle, username, password);
    }
    
    /**
     * Get associations for a specific relation and model.
     * 
     * For details on parameters and returned data refer to 
     * "Web Services API Reference Guide (Release 9.4)
     * Chapter 4:RESTful Resources:alarms:GET associations
     * 
     * @param relationHandle
     * @param modelHandle
     * @param side (left/right)
     * @param username
     *              if not provided, uses gateway configured username
     * @param password
     *              tied with username
     * @return a {@link Map} of results of create association operation.
     */
    public static List<Map<String, String>> getAssociations(String relationHandle,
                                                            String modelHandle,
                                                            String side,
                                                            String username,
                                                            String password) throws Exception
    {
        return ((SDKCASpectrumGateway)instance).getAssociations(relationHandle, modelHandle, side, username, password);
    }
    
    /**
     * Delete an association between two models.
     * 
     * For details on parameters and returned data refer to 
     * "Web Services API Reference Guide (Release 9.4)
     * Chapter 4:RESTful Resources:alarms:DELETE associations
     * 
     * @param relationHandle
     * @param leftModelHandle
     * @param rightModelHandle
     * @param username
     *              if not provided, uses gateway configured username
     * @param password
     *              tied with username
     * @return a {@link Map} of results of create association operation.
     */
    public static Map<String, String> deleteAssociation(String relationHandle,
                                                        String leftModelHandle,
                                                        String rightModelHandle,
                                                        String username,
                                                        String password) throws Exception
    {
        return ((SDKCASpectrumGateway)instance).deleteAssociation(relationHandle, leftModelHandle, rightModelHandle, username, password);
    }
    
    /**
     * Get attribute enumeration for enumerated attribute.
     * 
     * For details on parameters and returned data refer to 
     * "Web Services API Reference Guide (Release 9.4)
     * Chapter 4:RESTful Resources:alarms:GET attribute
     * 
     * @param attrId Enumerated attribute Id
     * @param username
     *              if not provided, uses gateway configured username
     * @param password
     *              tied with username
     * @return a {@link Map} of attribute strings keyed by index.
     */
    public static Map<String, String> getAttributeEnum(String attrId, 
                                                       String username, 
                                                       String password) throws Exception
    {
        return ((SDKCASpectrumGateway)instance).getAttributeEnum(attrId, username, password);
    }
    
    /**
     * Get connectivity information for specified device models IP address.
     * 
     * For details on parameters and returned data refer to 
     * "Web Services API Reference Guide (Release 9.4)
     * Chapter 4:RESTful Resources:alarms:GET connectivity
     * 
     * @param ipAddress
     * @param username
     *              if not provided, uses gateway configured username
     * @param password
     *              tied with username
     * @return a {@link Map} of results of create association operation.
     */
    public static Map<String, Map<String, List<Map<String, String>>>> getConnectivity(String ipAddress,
                                                                                      String username,
                                                                                      String password) throws Exception
    {
        return ((SDKCASpectrumGateway)instance).getConnectivity(ipAddress, username, password);
    }
    
    /**
     * Gets all devices.
     * 
     * For details on parameters and returned device data refer to 
     * "Web Services API Reference Guide (Release 9.4)
     * Chapter 4:RESTful Resources:devices:GET devices
     * 
     * @param attributes (optional) List of device attributes to retrieve for each device
     *              Model handle is returned by default
     * @param landscapeHandle (optional) List of landscape handles
     * @param throttlesize Sets # of devices to retrieve in single request to server
     *              If  total # of devices is more than throttle size then multiple reqeusts are made to
     *              server to retrieve all devices.
     *              Between 1 to 1000 (both inclusive) any other value is capped to 1000
     * @param username
     *              if not provided uses gateway configured username
     * @param password
     *              tied with username
     * @return a {@link List} containing {@link Map} of device attributes.
     *                        
     * @throws Exception
     */
    public static List<Map<String, String>> getAllDevices(List<String> attributes,
                                                          List<String> landscapeHandles,
                                                          int throttlesize,
                                                          String username,
                                                          String password) throws Exception
    {
        return ((SDKCASpectrumGateway)instance).getAllDevices(attributes, landscapeHandles, throttlesize, username, password);
    }
    
    /**
     * Gets landscapes return information about all landscapes.
     * 
     * For details on parameters and returned device data refer to 
     * "Web Services API Reference Guide (Release 9.4)
     * Chapter 4:RESTful Resources:landscapes:GET landscapes
     * 
     * @param username
     *              if not provided uses gateway configured username
     * @param password
     *              tied with username
     * @return a {@link List} containing {@link Map} of landscape information.
     *                        
     * @throws Exception
     */
    public static List<Map<String, String>> getLandscapes(String username,
                                                          String password) throws Exception
    {
        return ((SDKCASpectrumGateway)instance).getLandscapes(username, password);
    }
    
    /**
     * Create a new model.
     * 
     * For details on parameters and returned data refer to 
     * "Web Services API Reference Guide (Release 9.4)
     * Chapter 4:RESTful Resources:mode:POST model
     * 
     * @param landscapeHandle
     * @param modelTypeHandle
     * @param snmpAgentPort
     * @param comunityString
     * @param retryCount
     * @param timeout
     * @param ipAddress
     * @param parentModelHandle
     * @param parentToModelRelationHandle
     * @param attribValMap Model attribute value map
     * @param username
     *              if not provided, uses gateway configured username
     * @param password
     *              tied with username
     * @return a {@link Map} of results of create association operation.
     */
    public static Map<String, String> createModel(String landscapeHandle,
                                                  String modelTypeHandle,
                                                  int snmpAgentPort,
                                                  String communityString,
                                                  int retryCount,
                                                  int timeout,
                                                  String ipAddress,
                                                  String parentModelHandle,                                                
                                                  String parentToModelRelationHandle,
                                                  Map<String, String> attribValMap,
                                                  String username,
                                                  String password) throws Exception
    {
        return ((SDKCASpectrumGateway)instance).createModel(landscapeHandle, modelTypeHandle, snmpAgentPort, 
                                                            communityString, retryCount, timeout, ipAddress,
                                                            parentModelHandle, parentToModelRelationHandle,
                                                            attribValMap, username, password);
    }
    
    /**
     * Get specific model and its requested attributes.
     * 
     * For details on parameters and returned model data refer to 
     * "Web Services API Reference Guide (Release 9.4)
     * Chapter 4:RESTful Resources:model:GET model
     *
     * @param modelHandle
     * @param attributes (optional) List of model attributes to retrieve for specified model
     *              Model handle is returned by default
     * @param username
     *              if not provided uses gateway configured username
     * @param password
     *              tied with username
     * @return a {@link Map} of model attributes.
     *                        
     * @throws Exception
     */
    public static Map<String, String> getModel(String modelHandle,
                                               List<String> attributes,
                                               String username,
                                               String password) throws Exception
    {
        return ((SDKCASpectrumGateway)instance).getModel(modelHandle, attributes, username, password);
    }
    
    /**
     * Update model.
     * 
     * For details on parameters and returned data refer to 
     * "Web Services API Reference Guide (Release 9.4)
     * Chapter 4:RESTful Resources:model:PUT model
     * 
     * @param modelHandle Id of the model to update.
     * @param attribVal Map of attributes to update as key and values to modify as value
     * @param username
     *              if not provided, uses gateway configured username
     * @param password
     *              tied with username
     * @return a {@link Map} of model attribute update operation result.
     *                        
     * @throws Exception
     */
    public static Map<String, String> updateModel(String modelHandle,
                                                  Map<String, String> attribVal,
                                                  String username,
                                                  String password) throws Exception
    {
        return ((SDKCASpectrumGateway)instance).updateModel(modelHandle, attribVal, username, password);
    }
    
    /**
     * Delete model.
     * 
     * For details on parameters and returned data refer to 
     * "Web Services API Reference Guide (Release 9.4)
     * Chapter 4:RESTful Resources:model:DELETE model
     * 
     * @param modelHandle Id of the model to delete.
     * @param username
     *              if not provided, uses gateway configured username
     * @param password
     *              tied with username
     * @return a {@link Map} of model delete operation result.
     *                        
     * @throws Exception
     */
    public static Map<String, String> deleteModel(String modelHandle,
                                                  String username,
                                                  String password) throws Exception
    {
        return ((SDKCASpectrumGateway)instance).deleteModel(modelHandle, username, password);
    }
    
    /**
     * Get models based on filter conditions specified in xml query string.
     * 
     * For details on parameters and returned model data refer to 
     * "Web Services API Reference Guide (Release 9.4)
     * Chapter 4:RESTful Resources:models:POST models (GET Tunneling)
     * 
     * @param xml
     * @param username
     *              if not provided uses gateway configured username
     * @param password
     *              tied with username
     * @return a {@link List} containing {@link Map} of model attributes.
     *                        
     * @throws Exception
     */
    public static List<Map<String, String>> getModels(String xml, 
                                                      String username,
                                                      String password) throws Exception
    {
        return ((SDKCASpectrumGateway)instance).getModels(xml, username, password);
    }
    
    /**
     * Update models based on filter conditions and attributes specified in xml query string.
     * 
     * For details on parameters and returned model data refer to 
     * "Web Services API Reference Guide (Release 9.4)
     * Chapter 4:RESTful Resources:models:PUT models
     * 
     * @param xml
     * @param username
     *              if not provided uses gateway configured username
     * @param password
     *              tied with username
     * @return a {@link List} containing {@link Map} of model attribute update results.
     *                        
     * @throws Exception
     */
    public static List<Map<String, String>> updateModels(String xml, 
                                                         String username,
                                                         String password) throws Exception
    {
        return ((SDKCASpectrumGateway)instance).updateModels(xml, username, password);
    }
    
    /**
     * Create an event.
     * 
     * For details on parameters and returned data refer to 
     * "Web Services API Reference Guide (Release 9.4)
     * Chapter 4:RESTful Resources:events:POST event
     * 
     * @param eventType
     * @param modelHandle
     * @param varBindIdValue
     *              Map of event bind variable id and corresponding value 
     * @param username
     *              if not provided, uses gateway configured username
     * @param password
     *              tied with username
     * @return a {@link Map} of results of create event operation.
     */
    public static Map<String, String> createEvent(String eventType,
                                                  String modelHandle,
                                                  Map<String, String> varBindIdValue,
                                                  String username,
                                                  String password) throws Exception
    {
        return ((SDKCASpectrumGateway)instance).createEvent(eventType, modelHandle, varBindIdValue, username, password);
    }
    
    /**
     * Create events on model(s).
     * 
     * For details on parameters and returned data refer to 
     * "Web Services API Reference Guide (Release 9.4)
     * Chapter 4:RESTful Resources:events:POST events
     * 
     * @param createEventsRequestXml
     *              XML document specifying event request to create events for multiple models.
     * @param username
     *              if not provided, uses gateway configured username
     * @param password
     *              tied with username
     * @return a {@link Map} of results of create event operation.
     */
    public static List<Map<String, String>> createEvents(String createEventsRequestXml,
                                                         String username,
                                                         String password) throws Exception
    {
        return ((SDKCASpectrumGateway)instance).createEvents(createEventsRequestXml, username, password);
    }
    
    /**
     * Create a (pull) subscription.
     * 
     * For details on parameters and returned data refer to 
     * "Web Services API Reference Guide (Release 9.4)
     * Chapter 4:RESTful Resources:subscription:POST subscription
     * 
     * @param pullSubscriptionRequestXml
     *              XML document specifying subscription (pull type) for notifications 
     * @param username
     *              if not provided, uses gateway configured username
     * @param password
     *              tied with username
     * @return a Subscription id to use to pull notifications or delete the sunbscription.
     */
    public static String createSubscription(String pullSubscriptionRequestXml,
                                            String username,
                                            String password) throws Exception
    {
        return ((SDKCASpectrumGateway)instance).createSubscription(pullSubscriptionRequestXml, username, password);
    }
    
    /**
     * Get subscription i.e. pull notifications from subscription.
     * 
     * For details on parameters and returned data refer to 
     * "Web Services API Reference Guide (Release 9.4)
     * Chapter 4:RESTful Resources:subscription:GET subscription
     * 
     * @param subscriptionId
     *              Subscriuption id of pull subscription created earlier 
     * @param username
     *              if not provided, uses gateway configured username
     * @param password
     *              tied with username
     * @return a {@link List} containing {@link Map} of notifications.
     */
    public static List<Map<String, String>> getSubscription(String subscriptionId,
                                                            String username,
                                                            String password) throws Exception
    {
        return ((SDKCASpectrumGateway)instance).getSubscription(subscriptionId, username, password);
    }
    
    /**
     * Delete subscription.
     * 
     * For details on parameters and returned data refer to 
     * "Web Services API Reference Guide (Release 9.4)
     * Chapter 4:RESTful Resources:subscripton:DELETE subscription
     * 
     * @param subscriptionId Id of the subscription to delete.
     * @param username
     *              if not provided, uses gateway configured username
     * @param password
     *              tied with username
     * @return a {@link Map} of subscription delete operation result.
     *                        
     * @throws Exception
     */
    public static Map<String, String> deleteSubscription(String subscriptionId,
                                                         String username,
                                                         String password) throws Exception
    {
        return ((SDKCASpectrumGateway)instance).deleteSubscription(subscriptionId, username, password);
    }
    
    // Methods to hide from AbstractGatewayAPI
    
    /**
     * Returns {@link Set} of supported object types.
     * <p>
     * Supported object types are as follows
     * 
     * action
     * alarms
     * associations
     * connectivity
     * devices
     * events
     * landscapes
     * model
     * models
     * subscription
     * 
     * @return {@link Set} of supported object types
     */
    public static Set<String> getObjectTypes()
    {
        Set<String> objTypes = new HashSet<String>();
        
        objTypes.add("action");
        objTypes.add("alarms");
        objTypes.add("associations");
        objTypes.add("connectivity");
        objTypes.add("devices");
        objTypes.add("event");
        objTypes.add("events");
        objTypes.add("landscapes");
        objTypes.add("model");
        objTypes.add("models");
        objTypes.add("subscription");
        
        return objTypes;
    }
    
    /**
     * Creates instance of specified object type in CA Spectrum.
     * <p>
     * Following objects support create (currently)
     * 
     * event (Note: This results in creaion of alarm in CA Spectrum)
     * 
     * objParams Keys
     * 
     * "EventType"   - Event Type
     * "ModelHandle" - Model Handle
     * VarBindIds    - Variable binding ids
     * 
     * @param  objType   Type of object to create in CA Spectrum
     * @param  objParams {@link Map} of object parameters
     * @param  userName  User name
     * @param  password  Password
     * @return           Attribute id-value {@link Map} of created object in external system
     * @throws Exception If any error occurs in creating object of specified
     *                   type in external system
     */
    public static Map<String, String> createObject(String objType, Map<String, String> objParams, String userName, String password) throws Exception
    {
        if (StringUtils.isEmpty(objType) || objParams == null || objParams.isEmpty())
        {
            throw new Exception("Invalid parameters");
        }
        
        if (!objType.equals("event"))
        {
            throw new Exception("Currently createObject is supported for event object type only");
        }
        
        String eventType = objParams.remove("EventType");
        String modelHandle = objParams.remove("ModelHandle");
        
        if (StringUtils.isEmpty(eventType) || StringUtils.isEmpty(modelHandle) || objParams.isEmpty())
        {
            throw new Exception(
                "Missing EventType and/or ModelHandle and/or passed in object params does not contain required var bindings");
        }
        
        return createEvent(eventType, modelHandle, objParams, userName, password);
    }
    
    /**
     * Reads attributes of specified object in CA Spectrum.
     * <p>
     * Following objects support read (currently)
     *
     * model
     * 
     * @param  objType    Object type
     * @param  objId      Id of the object to read attributes of from CA Spectrum
     * @param  attribs    {@link List} of attributes of object to read
     * @param  userName   User name
     * @param  password   Password
     * @return            {@link Map} of object attribute id-value pairs
     * @throws Exception  If any error occurs in reading attributes of the object in 
     *                    external system
     */
    public static Map<String, String> readObject(String objType, String objId, List<String> attribs, String userName, String password) throws Exception
    {
        if (StringUtils.isEmpty(objType) || StringUtils.isEmpty(objId) || attribs == null || attribs.isEmpty())
        {
            throw new Exception("Passed invalid parameters");
        }
        
        if (!objType.equals("model"))
        {
            throw new Exception("Currently readObject is supported for model object type only");
        }
        
        return getModel(objId, attribs, userName, password);
    }
    
    /**
     * Updates attributes of specified object in CA Spectrum.
     * <p>
     * Following objects support update (currently)
     *
     * alarm
     * model
     * 
     * @param  objType   Object type
     * @param  objId     Id of the object to update attributes of in CA Spectrum
     * @param  updParams Key-value {@link Map} of object attributes to update
     * @param  userName  User name
     * @param  password  Password
     * @return           Key-value {@link Map} of the updated object attributes
     * @throws Exception If any error occurs in updating attributes of the object in 
     *                   external system
     */
    public static Map<String, String> updateObject(String objType, String objId, Map<String, String> updParams, String userName, String password) throws Exception
    {
        if (StringUtils.isEmpty(objType) || StringUtils.isEmpty(objId) || updParams == null || updParams.isEmpty())
        {
            throw new Exception("Passed invalid parameters");
        }
        
        if (!(objType.equals("alarm") || objType.equals("model")))
        {
            throw new Exception("Currently updateObject is supported for alarm or model object types only");
        }
        
        switch(objType)
        {
            case "alarm":
                return updateAlarm(objId, updParams, userName, password);
            
            case "model":
                return updateModel(objId, updParams, userName, password);
        }
        
        throw new Exception("Should never reach here!!!");
    }
    
    /**
     * Deletes specified object from CA Spectrum.
     * <p>
     * Following objects support delete (currently)
     *
     * alarm
     * model
     * 
     * @param  objType   Object type
     * @param  objId     Id of the object to delete from CA Spectrum
     * @param  userName  User name
     * @param  password  Password
     * @return           Key-value {@link Map} result of object delete operation
     * @throws Exception If any error occurs in deleting object from external system
     */
    public static Map<String, String> deleteObject(String objType, String objId, String userName, String password) throws Exception
    {
        if (StringUtils.isEmpty(objType) || StringUtils.isEmpty(objId))
        {
            throw new Exception("Passed invalid parameters");
        }
        
        if (!(objType.equals("alarm") || objType.equals("model")))
        {
            throw new Exception("Currently deleteObject is supported for alarm or model object types only");
        }
        
        switch(objType)
        {
            case "alarm":
                return deleteAlarm(objId, userName, password);
            
            case "model":
                return deleteModel(objId, userName, password);
        }
        
        throw new Exception("Should never reach here!!!");
    }
}
