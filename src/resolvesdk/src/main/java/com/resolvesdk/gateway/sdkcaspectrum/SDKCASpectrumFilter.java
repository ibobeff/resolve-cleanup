/******************************************************************************
* (C) Copyright 2014
*
* Resolve Systems, LLC
*
* All rights reserved
* This software is distributed under license and may not be used, copied,
* modified, or distributed without the express written permission of
* Resolve Systems, LLC.
*
******************************************************************************/

package com.resolvesdk.gateway.sdkcaspectrum;

import com.resolve.gateway.BaseFilter;
import com.resolve.gateway.RetainValue;
import com.resolve.util.StringUtils;

public class SDKCASpectrumFilter extends BaseFilter
{
    // Additional field names for this filter.
    public static final String URLQUERY = "URLQUERY";
    public static final String XMLQUERY = "XMLQUERY";
    public static final String OBJECT = "OBJECT";
    public static final String LATESTALARMTIME = "LATESTALARMTIME";
    public static final String LAST_OCCURRENCE_TIME_ATTRIBUTE = "0x1321a";
    public static final String FIRST_OCCURRENCE_TIME_ATTRIBUTE = "0x11f4e";

    private String object;
    private String urlQuery;
    private String xmlQuery;
    
    // Retained Fields
    
    private String latestAlarmTime; // Latest Alarm Processed Time in sec since epoch

    public SDKCASpectrumFilter(String id, String active, String order, String interval, String eventEventId, String runbook, String script, String object, String urlQuery, String xmlQuery, String latestAlarmTime)
    {
        super(id, active, order, interval, eventEventId, runbook, script);
        setObject(object);
        setUrlQuery(urlQuery);
        setXmlQuery(xmlQuery);
        setLatestAlarmTime(latestAlarmTime);
    } // SDKCASpectrumFilter

    public String getObject()
    {
        return object;
    }

    public void setObject(String object)
    {
        this.object = object != null ? object.trim() : object;
    }

    public String getUrlQuery()
    {
        return urlQuery;
    }

    public void setUrlQuery(String urlQuery)
    {
        this.urlQuery = urlQuery != null ? urlQuery.trim() : urlQuery;
    }

    public String getXmlQuery()
    {
        return xmlQuery;
    }

    public void setXmlQuery(String xmlQuery)
    {
        this.xmlQuery = xmlQuery != null ? xmlQuery.trim() : xmlQuery;
    }
    
    @RetainValue
    public String getLatestAlarmTime()
    {
        return latestAlarmTime;
    }

    public void setLatestAlarmTime(String latestAlarmTime)
    {
        this.latestAlarmTime = latestAlarmTime != null ? latestAlarmTime.trim() : latestAlarmTime;
    }
    
    @Override
    public String toString()
    {
        return super.toString() + ", object=" + object + (StringUtils.isNotBlank(urlQuery) ? ", urlQuery=" + urlQuery : "") + 
               (StringUtils.isNotBlank(xmlQuery) ? ", xmlQuery=" + xmlQuery : "") + 
               (StringUtils.isNotBlank(latestAlarmTime) ? ", latestAlarmTime=" + latestAlarmTime : "");
    }
}
