/******************************************************************************
* (C) Copyright 2014
*
* Resolve Systems, LLC
*
* All rights reserved
* This software is distributed under license and may not be used, copied,
* modified, or distributed without the express written permission of
* Resolve Systems, LLC.
*
******************************************************************************/

package com.resolvesdk.gateway.sdkjira;

import java.io.File;
import java.util.HashMap;
import java.util.Map;

import org.apache.commons.io.FileUtils;

import com.resolve.rsbase.MainBase;
import com.resolve.gateway.Filter;
import com.resolve.rsremote.ConfigReceiveGateway;
import com.resolve.util.Log;
import com.resolve.util.ObjectProperties;
import com.resolve.util.StringUtils;
import com.resolve.util.XDoc;

/**
 * This class loads up the configuration for SDKJIRA gateway. It reads the
 * configuration file from rsremote/config/config.xml. The element of interest
 * is:
 *
 * <SDKJIRA....../>
 */
public class ConfigReceiveSDKJIRA extends ConfigReceiveGateway
{
    private static final long serialVersionUID = 1L;

    private static final String RECEIVE_SDKJIRA_NODE = "./RECEIVE/SDKJIRA/";
    private static final String RECEIVE_SDKJIRA_FILTER = RECEIVE_SDKJIRA_NODE + "FILTER";
    //private static final String RECEIVE_SDKJIRA_PROPERTIES = RECEIVE_SDKJIRA_NODE + "PROPERTIES";

    // http://<JIRA server>:<port>/rest/api/2/
    private static final String RECEIVE_SDKJIRA_ATTR_URL = RECEIVE_SDKJIRA_NODE + "@URL";
    private static final String RECEIVE_SDKJIRA_ATTR_HTTPBASICAUTH_USERNAME = RECEIVE_SDKJIRA_NODE + "@HTTPBASICAUTHUSERNAME";
    private static final String RECEIVE_SDKJIRA_ATTR_HTTPBASICAUTH_P_ASSWORD = RECEIVE_SDKJIRA_NODE + "@HTTPBASICAUTHPASSWORD";
    //private static final String RECEIVE_SDKJIRA_ATTR_DATETIME_WEBSERVICE_NAME = RECEIVE_SDKJIRA_NODE + "@DATETIMEWEBSERVICENAME";
    private static final String RECEIVE_SDKJIRA_ATTR_THROTTLESIZE = RECEIVE_SDKJIRA_NODE + "@THROTTLESIZE";
    private static final int RECEIVE_SDKJIRA_DEFAULT_THROTTLESIZE = 50;

    //private boolean active = false;
    private String url = "";
    private int interval = 120;
    private String httpbasicauthusername = "";
    private String httpbasicauthp_assword = "";
    private String queue = "JIRA";
    //private String datetimewebservicename = "";
    // private String changeDateField = "CHANGEDATE";
    private int throttleSize = RECEIVE_SDKJIRA_DEFAULT_THROTTLESIZE;

    public ConfigReceiveSDKJIRA(XDoc config) throws Exception
    {
        super(config);

        define("url", STRING, RECEIVE_SDKJIRA_ATTR_URL);
        define("httpbasicauthusername", STRING, RECEIVE_SDKJIRA_ATTR_HTTPBASICAUTH_USERNAME);
        define("httpbasicauthp_assword", SECURE, RECEIVE_SDKJIRA_ATTR_HTTPBASICAUTH_P_ASSWORD);
        //define("datetimewebservicename", STRING, RECEIVE_SDKJIRA_ATTR_DATETIME_WEBSERVICE_NAME);
        define("throttleSize", INT, RECEIVE_SDKJIRA_ATTR_THROTTLESIZE);
    } // ConfigReceiveSDKJIRA

    public String getRootNode()
    {
        return RECEIVE_SDKJIRA_NODE;
    }

    @SuppressWarnings("unchecked")
    public void load() throws Exception
    {
        try
        {
            loadAttributes();
            
            if (isActive()) // no need to unnecessarily load anything if the gateway
                            // is inactive.
            {
                SDKJIRAGateway sdkJIRAGateway = SDKJIRAGateway.getInstance(this);

                // filters
                filters = xdoc.getListMapValue(RECEIVE_SDKJIRA_FILTER);

                // initialize filters
                if (filters.size() > 0)
                {
                    for (Map<String, Object> values : filters)
                    {
                        //it's important to make it lowercase because we store the files in lowercase
                        //without lowering it, linux systems have problem of not reading the script content.
                        String filterName = ((String)values.get(SDKJIRAFilter.ID)).toLowerCase();
                        File scriptFile = getFile(MainBase.main.release.serviceName + "/config/sdkjira/" + filterName + ".groovy");

                        try
                        {
                            if (scriptFile.exists())
                            {
                                String groovy = FileUtils.readFileToString(scriptFile, "UTF-8");
                                if (StringUtils.isNotBlank(groovy))
                                {
                                    values.put(SDKJIRAFilter.SCRIPT, groovy);
                                    Log.log.debug("Loaded groovy script: " + scriptFile);
                                }
                            }

                            // init filter
                            sdkJIRAGateway.setFilter(values);
                        }
                        catch (Exception e)
                        {
                            Log.log.warn(e.getMessage() + ". Skipping file: " + scriptFile.getAbsolutePath());
                        }
                    }
                }
            }
        }
        catch (Throwable e)
        {
            //deactivate it
            setActive(false);
            Log.log.warn("Couldn not load configuration for SDK JIRA gateway, check blueprint. " + e.getMessage(), e);
        }
    } // load

    @SuppressWarnings({ "unchecked", "rawtypes" })
    public void save() throws Exception
    {
        // create caspectrum directory
        File dir = getFile(MainBase.main.release.serviceName + "/config/sdkjira");
        if (!dir.exists())
        {
            FileUtils.forceMkdir(dir);
        }
        
        // clear list of filters to save
        if (filters != null && isActive())
        {
            filters.clear();

            // save sql files
            for (Filter filter : SDKJIRAGateway.getInstance().getFilters().values())
            {
                SDKJIRAFilter sdkJIRAFilter = (SDKJIRAFilter) filter;

                String groovy = sdkJIRAFilter.getScript();
                
                String scriptFileName;
                File scriptFile = null;
                
                if (StringUtils.isNotBlank(groovy))
                {
                    scriptFileName = sdkJIRAFilter.getId().toLowerCase() + ".groovy";
                    scriptFile = getFile(MainBase.main.release.serviceName + "/config/sdkjira/" + scriptFileName);
                }
                
                // add xdoc entry
                Map<String, Object> entry = new HashMap<String, Object>();

                entry.put(SDKJIRAFilter.ID, sdkJIRAFilter.getId());
                entry.put(SDKJIRAFilter.ACTIVE, String.valueOf(sdkJIRAFilter.isActive()));
                entry.put(SDKJIRAFilter.ORDER, String.valueOf(sdkJIRAFilter.getOrder()));
                entry.put(SDKJIRAFilter.INTERVAL, String.valueOf(sdkJIRAFilter.getInterval()));
                entry.put(SDKJIRAFilter.EVENT_EVENTID, sdkJIRAFilter.getEventEventId());
                entry.put(SDKJIRAFilter.RUNBOOK, sdkJIRAFilter.getRunbook());
                entry.put(SDKJIRAFilter.SCRIPT, sdkJIRAFilter.getScript());
                entry.put(SDKJIRAFilter.URLQUERY, sdkJIRAFilter.getUrlQuery());
                entry.put(SDKJIRAFilter.JSONQUERY, sdkJIRAFilter.getJsonQuery());
                entry.put(SDKJIRAFilter.LASTISSUEPROCESSEDCREATETIMESTAMP, sdkJIRAFilter.getLastIssueProcessedCreateTimeStamp());
                
                filters.add(entry);

                if (scriptFile != null)
                {
                    try
                    {
                        // create groovy file
                        FileUtils.writeStringToFile(scriptFile, groovy, "UTF-8");
                    }
                    catch (Exception e)
                    {
                        Log.log.warn(e.getMessage() + ". Skipping file: " + scriptFile.getAbsolutePath());
                    }
                }
            }

            // filters
            xdoc.setListMapValue(RECEIVE_SDKJIRA_FILTER, filters);
        }

        // clear list of properties to save
        if (nameProperties != null && isActive())
        {
            // empty properties
            nameProperties.clear();

            // recreate properties
            for (String name : SDKJIRAGateway.getInstance().getNameProperties().keySet())
            {
                // add xdoc entry
                Map<String, Object> entry = new HashMap<String, Object>();
                entry.put(NAME_KEY, name);
                nameProperties.add(entry);

                Map props = SDKJIRAGateway.getInstance().getNameProperties().getNameProperties(name);
                ObjectProperties prop = new ObjectProperties(props);
                prop.save(props);
            }
        }

        saveAttributes();
    } // save

    public String getUrl()
    {
        return url;
    }

    public void setUrl(String url)
    {
        this.url = url;
    }

    public int getInterval()
    {
        return interval;
    }

    public void setInterval(int interval)
    {
        this.interval = interval;
    }

    public String getHttpbasicauthusername()
    {
        return httpbasicauthusername;
    }

    public void setHttpbasicauthusername(String httpbasicauthusername)
    {
        this.httpbasicauthusername = httpbasicauthusername;
    }

    public String getHttpbasicauthp_assword()
    {
        return httpbasicauthp_assword;
    }

    public void setHttpbasicauthp_assword(String httpbasicauthpassword)
    {
        this.httpbasicauthp_assword = httpbasicauthpassword;
    }
    
    public String getQueue()
    {
        return queue;
    }

    public void setQueue(String queue)
    {
        this.queue = queue;
    }
    
    public int getThrottleSize()
    {
        return throttleSize;
    }

    public void setThrottleSize(int throttleSize)
    {
        this.throttleSize = throttleSize;
    }
}
