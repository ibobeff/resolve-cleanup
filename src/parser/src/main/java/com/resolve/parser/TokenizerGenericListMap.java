/******************************************************************************
* (C) Copyright 2014
*
* Resolve Systems, LLC
*
* All rights reserved
* This software is distributed under license and may not be used, copied,
* modified, or distributed without the express written permission of
* Resolve Systems, LLC.
*
******************************************************************************/

package com.resolve.parser;

import java.util.LinkedHashMap;
import java.util.List;

import org.dom4j.Element;

import com.resolve.data.DataBase;
import com.resolve.data.DataListMap;
import com.resolve.util.Constants;
import com.resolve.util.StringUtils;
import com.resolve.util.XDoc;

public class TokenizerGenericListMap extends TokenizerBase
{
    String lineSeparator;
    String fieldSeparator;
    String[] headings;
    
    public TokenizerGenericListMap()
    {
    } // TokenizerGenericListMap
    
    public TokenizerGenericListMap(Parser parser, String lineSeparator, String fieldSeparator, String headings)
    {
        super(parser.getSid(), Constants.PARSER_METHOD_GENERIC, Constants.DATA_TYPE_LISTMAP);
        
        init(parser, lineSeparator, fieldSeparator, headings);
    } // TokenGenericListMap
    
    void init(Parser parser, String lineSeparator, String fieldSeparator, String headings)
    {
        this.setLineSeparator(lineSeparator);
        this.setFieldSeparator(fieldSeparator);
        this.setHeadings(headings);
        
        parser.setTokenizer(this);
    } // init
    
    public static void load(Parser parser, String lineSeparator, String fieldSeparator, String headings) throws Exception
    {
        new TokenizerGenericListMap(parser, lineSeparator, fieldSeparator, headings);
    } // load
    
    public DataBase tokenize(String raw)
    {
        DataListMap result = new DataListMap();
        
        if (raw != null)
        {
            String lines[] = raw.split(lineSeparator);
            
            /*
             * Initialize heading
             */
            // check if heading values are defined
		    String[] dataHeadings = null;
            if (this.headings != null)
            {
                dataHeadings =(String[])this.headings.clone();
            }
            // get heading values from data
            else
            {
                String line = lines[0];
                String[] fields = line.split(fieldSeparator);
                dataHeadings = new String[fields.length];
                for (int i=0; i < fields.length; i++)
                {
                    dataHeadings[i] = fields[i].trim();
                }
            }
            
            /*
             * parse data
             */
            for (int lineIdx=0; lineIdx < lines.length; lineIdx++)
            {
                String line = lines[lineIdx];
                String[] fields = line.split(fieldSeparator);
                
                LinkedHashMap values = new LinkedHashMap();
                for (int i=0; i < dataHeadings.length; i++)
                {
                    String value = "";
                    String heading = XDoc.getValidName(dataHeadings[i]);
                    if (i < fields.length)
                    {
                        value = fields[i];
                    }
                        
                    values.put(heading, value);
                }
                if (values.size() > 0)
                {
                    result.add(values);
                }
            }
        }
        
        return result;
    }  // tokenize
    
    public String getLineSeparator()
    {
        return lineSeparator;
    } // getLineSeparator

    public void setLineSeparator(String lineSeparator)
    {
        if (!StringUtils.isEmpty(lineSeparator) && !lineSeparator.equalsIgnoreCase("DEFAULT"))
        {
            this.lineSeparator = lineSeparator;
        }
        else
        {
            this.lineSeparator = "\n";
        }
    } // setLineSeparator

    public void setFieldSeparator(String fieldSeparator)
    {
        if (!StringUtils.isEmpty(fieldSeparator) && !fieldSeparator.equalsIgnoreCase("DEFAULT"))
        {
            this.fieldSeparator = fieldSeparator;
        }
        else
        {
            this.fieldSeparator = ",";
        }
    } // setFieldSeparator
        
    public void setHeadings(String headings)
    {
        if (!StringUtils.isEmpty(headings))
        {
            this.headings = headings.split(",");
        }
    } // setHeadings
    
    public void setValue(XDoc xml, Element container, String elementName, DataBase data)
    {
        xml.setListMapValue(container, elementName, (List)data.getData());
    } // setValue
    
} // TokenizerGenericListMap
