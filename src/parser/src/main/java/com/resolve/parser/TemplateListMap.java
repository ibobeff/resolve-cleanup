/******************************************************************************
* (C) Copyright 2014
*
* Resolve Systems, LLC
*
* All rights reserved
* This software is distributed under license and may not be used, copied,
* modified, or distributed without the express written permission of
* Resolve Systems, LLC.
*
******************************************************************************/

package com.resolve.parser;

import com.resolve.util.XDoc;
import com.resolve.util.StringUtils;
import com.resolve.util.Constants;

public class TemplateListMap
{
    boolean hasHeadingValue;
    String type;
    
    String[] headingValues;
    String headingRegex;
    int[] headingPositions;
    
    String regex;
    int[] namePositions;
    int[] valuePositions;
    
    public TemplateListMap()
    {
        this.hasHeadingValue = false;
    } // TemplateListMap
    
    public String getType()
    {
        return type;
    } // getType
    
    public void setType(String type)
    {
        if (type.equalsIgnoreCase("HEADING"))
        {
            this.type = type;
        }
        else if (type.equalsIgnoreCase("VALUE"))
        {
            this.type = type;
        }
    } // setType
    
    public String getColumnName(int index)
    {
        String result = "";
        
        if (headingValues != null && index < headingValues.length)
        {
            result = XDoc.getValidName(headingValues[index]);
        }
        return result;
    } // getColumnName
    
    public void setColumnName(int index, String heading)
    {
        if (heading != null && index < headingPositions.length)
        {
            headingValues[index] = heading;
        }
    } // setColumnName
    
    public void setHeadingValues(String headings)
    {
        if (!StringUtils.isEmpty(headings))
        {
            this.hasHeadingValue = true;
            
            String[] columnNames = headings.split(",");
            
            int size = columnNames.length;
            this.headingValues = new String[size];
            
            for (int i=0; i < size; i++)
            {
                String heading = columnNames[i].trim().toUpperCase();
                this.headingValues[i] = heading;
            }
        }
    } // setHeadingValues
    
    public String[] getHeadingValues()
    {
        return (String[])this.headingValues.clone();
    } // getHeadingValues

    public int getHeadingPosition(int i)
    {
        int result = i + 1;
        if (this.headingPositions != null)
        {
            result = this.headingPositions[i];
        }
        return result;
    } // getHeadingPosition
    
    public void setHeadingPositions(String positions)
    {
        if (!StringUtils.isEmpty(positions))
        {
            String[] headingPos = positions.split(",");
            
            int size = headingPos.length;
            this.headingPositions = new int[size];
            this.headingValues = new String[size];
            
            for (int i=0; i < size; i++)
            {
                headingPositions[i] = Integer.parseInt(headingPos[i].trim());
            }
        }
        else
        {
            headingPositions = null;
        }
    } // setHeadingPositions
    
    public int getNamePosition(int i)
    {
        int result = i + 1;
        if (this.namePositions != null)
        {
            result = this.namePositions[i];
        }
        return result;
    } // getNamePosition
    
    public void setNamePositions(String positions)
    {
        if (StringUtils.isNotEmpty(positions))
        {
            String[] keyPos = positions.split(",");
            
            int size = keyPos.length;
            this.namePositions = new int[size];
            this.headingValues = new String[size];
            
            for (int i=0; i < size; i++)
            {
                namePositions[i] = Integer.parseInt(keyPos[i].trim());
            }
        }
        else
        {
            namePositions = null;
        }
    } // setNamePositions
    
    public int getValuePosition(int i)
    {
        int result = i + 1;
        if (this.valuePositions != null)
        {
            result = this.valuePositions[i];
        }
        return result;
    } // getValuePosition
    
    public void setValuePositions(String value)
    {
        if (value != null)
        {
            String[] valuePos = value.split(",");
            
            int size = valuePos.length;
            this.valuePositions = new int[size];
            
            for (int i=0; i < size; i++)
            {
                valuePositions[i] = Integer.parseInt(valuePos[i].trim());
            }
        }
        else
        {
            this.valuePositions = null;
        }
    } // setValuePositions
    
    public String getHeadingRegex()
    {
        return this.headingRegex;
    } // getHeadingRegex
    
    public void setHeadingRegex(String headingRegex)
    {
        if (headingRegex != null)
        {
	        if (headingRegex.charAt(0) == '/' && headingRegex.charAt(headingRegex.length()-1) == '/')
	        {
		        this.headingRegex = headingRegex.substring(1, headingRegex.length()-1);
	        }
	        else
	        {
		        this.headingRegex = headingRegex;
	        }
        }
    } // setHeadingRegex

    public String getRegex()
    {
        return this.regex;
    } // getRegex
    
    public void setRegex(String regex)
    {
        if (regex != null)
        {
	        if (regex.charAt(0) == '/' && regex.charAt(regex.length()-1) == '/')
	        {
		        this.regex = regex.substring(1, regex.length()-1);
	        }
	        else
	        {
		        this.regex = regex;
	        }
        }
    } // setRegex

    public int size()
    {
        int result = 0;
        if (valuePositions != null)
        {
            result = valuePositions.length;
        }
        return result;
    } // size
    
    public boolean hasHeadingValue()
    {
        return this.hasHeadingValue;
    } // hasHeadingValue
    
    public boolean isHeading()
    {
        boolean result = false;
        if (type.equals(Constants.PARSER_TEMPLATE_HEADING))
        {
            result = true;
        }
        return result;
    } // isHeading
    
    public boolean isValue()
    {
        boolean result = false;
        if (type.equals(Constants.PARSER_TEMPLATE_VALUE))
        {
            result = true;
        }
        return result;
    } // isValue
    
} // TemplateListMap
