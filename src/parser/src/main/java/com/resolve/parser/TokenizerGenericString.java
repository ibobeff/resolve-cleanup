/******************************************************************************
* (C) Copyright 2014
*
* Resolve Systems, LLC
*
* All rights reserved
* This software is distributed under license and may not be used, copied,
* modified, or distributed without the express written permission of
* Resolve Systems, LLC.
*
******************************************************************************/

package com.resolve.parser;

import org.dom4j.Element;

import com.resolve.data.DataBase;
import com.resolve.data.DataString;
import com.resolve.util.Constants;
import com.resolve.util.XDoc;

public class TokenizerGenericString extends TokenizerBase
{
    public TokenizerGenericString()
    {
    } // TokenizerGenericString
    
    public TokenizerGenericString(Parser parser)
    {
        super(parser.getSid(), Constants.PARSER_METHOD_GENERIC, Constants.DATA_TYPE_STRING);
        
        init(parser);
    } // TokenGenericString
    
    void init(Parser parser)
    {
        parser.setTokenizer(this);
    } // init
    
    public static void load(Parser parser)
    {
        new TokenizerGenericString(parser);
    } // load
    
    public DataBase tokenize(String raw)
    {
        DataString result = new DataString();
        
        if (raw != null)
        {
            result.put(raw);
        }
        
        return result;
    }  // tokenize
        
    public void setValue(XDoc xml, Element container, String elementName, DataBase data)
    {
        xml.setStringValue(container, elementName, (String)data.getData());
    } // setValue
    

} // TokenizerGenericString
