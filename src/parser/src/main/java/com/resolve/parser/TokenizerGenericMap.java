/******************************************************************************
* (C) Copyright 2014
*
* Resolve Systems, LLC
*
* All rights reserved
* This software is distributed under license and may not be used, copied,
* modified, or distributed without the express written permission of
* Resolve Systems, LLC.
*
******************************************************************************/

package com.resolve.parser;

import java.util.Iterator;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.dom4j.Element;

import com.resolve.data.DataBase;
import com.resolve.data.DataMap;
import com.resolve.util.Constants;
import com.resolve.util.StringUtils;
import com.resolve.util.XDoc;

public class TokenizerGenericMap extends TokenizerBase
{
    String lineSeparator;
    
    public TokenizerGenericMap()
    {
    } // TokenizerGenericMap
    
    public TokenizerGenericMap(Parser parser, String lineSeparator, String fieldSeparator)
    {
        super(parser.getSid(), Constants.PARSER_METHOD_GENERIC, Constants.DATA_TYPE_MAP);
        
        init(parser, lineSeparator, fieldSeparator);
    } // TokenizerGenericMap
    
    void init(Parser parser, String lineSeparator, String fieldSeparator)
    {
        this.setLineSeparator(lineSeparator);
        
        // init fieldSeparator
        if (StringUtils.isEmpty(fieldSeparator) || fieldSeparator.equalsIgnoreCase("DEFAULT"))
        {
            fieldSeparator = "=";
        }
        String regex = "^\\s*(.+?)\\s*?"+fieldSeparator+"\\s*(.*)";
        TemplateMap template = new TemplateMap(regex, 1, 2);
        super.addMember(template);
        
        parser.setTokenizer(this);
    } // init
    
    public static void load(Parser parser, String lineSeparator, String fieldSeparator)
    {
	    new TokenizerGenericMap(parser, lineSeparator, fieldSeparator);
    } // load
    
    public DataBase tokenize(String raw)
    {
        DataMap result = new DataMap();
        
        if (raw != null)
        {
            String lines[] = raw.split(lineSeparator);
            for (int lineIdx=0; lineIdx < lines.length; lineIdx++)
            {
                String line = lines[lineIdx];
                boolean match = false;
                //Log.log.debug("matching line: "+line);
                
                for (Iterator i=members.iterator(); !match && i.hasNext(); )
                {
                    TemplateMap template = (TemplateMap)i.next();
                    
                    //Log.log.debug("  regex: "+template.getRegex());
                    Pattern pattern = Pattern.compile(template.getRegex());
                    Matcher matcher = pattern.matcher(line);
                    if (matcher.find())
                    {
                        match = true;
                        //Log.log.debug("    found matched");
                        
                        for (int idx=0; idx < template.size(); idx++)
                        {
                            String key = matcher.group(template.getKeyPositions()[idx]);
                            String value = matcher.group(template.getValuePositions()[idx]);
                                
                            result.put(key, value);
                        }
                    }
                }
            }
        }
        
        return result;
    }  // tokenize
        
    public String getLineSeparator()
    {
        return lineSeparator;
    } // getLineSeparator

    public void setLineSeparator(String lineSeparator)
    {
        if (!StringUtils.isEmpty(lineSeparator) && !lineSeparator.equalsIgnoreCase("DEFAULT"))
        {
            this.lineSeparator = lineSeparator;
        }
        else
        {
            this.lineSeparator = "\n";
        }
    } // setLineSeparator

    public void setValue(XDoc xml, Element container, String elementName, DataBase data)
    {
        xml.setMapValue(container, elementName, (Map)data.getData());
    } // setValue
    
} // TokenizerGenericMap
