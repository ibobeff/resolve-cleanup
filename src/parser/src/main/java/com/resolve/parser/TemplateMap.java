/******************************************************************************
* (C) Copyright 2014
*
* Resolve Systems, LLC
*
* All rights reserved
* This software is distributed under license and may not be used, copied,
* modified, or distributed without the express written permission of
* Resolve Systems, LLC.
*
******************************************************************************/

package com.resolve.parser;

public class TemplateMap
{
    String regex;
    int[] keyPositions;
    int[] valuePositions;
    
    public TemplateMap()
    {
    } // ParseMapTemplate
    
    public TemplateMap(String regex, String key, String value) throws Exception
    {
        this.regex = regex;
        
        String[] keyPos = key.split(",");
        String[] valuePos = value.split(",");
        
        if (keyPos.length != valuePos.length)
        {
            throw new Exception("Number of key positions does not match number of value positions");
        }
        
        int size = keyPos.length;
        keyPositions = new int[size];
        valuePositions = new int[size];
        
        for (int i=0; i < size; i++)
        {
            keyPositions[i] = Integer.parseInt(keyPos[i].trim());
            valuePositions[i] = Integer.parseInt(valuePos[i].trim());
        }
    } // ParseMapTemplate
    
    public TemplateMap(String regex, int key, int value)
    {
        this.regex = regex;
        
        keyPositions = new int[1];
        valuePositions = new int[1];
        
        keyPositions[0] = key;
        valuePositions[0] = value;
    } // TemplateMap
    
    public int size()
    {
        return keyPositions.length;
    } // size
    
    public int[] getKeyPositions()
    {
        return keyPositions;
    }
    public int[] getValuePositions()
    {
        return valuePositions;
    }
    public String getRegex()
    {
        return regex;
    }
    public void setRegex(String regex)
    {
        this.regex = regex;
    }

} // TemplateMap
 