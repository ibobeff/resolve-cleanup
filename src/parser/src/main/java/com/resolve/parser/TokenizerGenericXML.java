/******************************************************************************
* (C) Copyright 2014
*
* Resolve Systems, LLC
*
* All rights reserved
* This software is distributed under license and may not be used, copied,
* modified, or distributed without the express written permission of
* Resolve Systems, LLC.
*
******************************************************************************/

package com.resolve.parser;

import org.dom4j.Element;

import com.resolve.data.DataBase;
import com.resolve.data.DataXML;
import com.resolve.util.Constants;
import com.resolve.util.XDoc;

public class TokenizerGenericXML extends TokenizerBase
{
    public TokenizerGenericXML()
    {
    } // TokenizerGenericXML
    
    public TokenizerGenericXML(Parser parser)
    {
        super(parser.getSid(), Constants.PARSER_METHOD_GENERIC, Constants.DATA_TYPE_XML);
        
        init(parser);
    } // TokenGenericXML
    
    void init(Parser parser)
    {
        parser.setTokenizer(this);
    } // init
    
    public static void load(Parser parser)
    {
        new TokenizerGenericXML(parser);
    } // load
    
    public DataBase tokenize(String raw) throws Exception
    {
        DataXML result = new DataXML();
        
        if (raw != null)
        {
            result.put(new XDoc(raw));
        }
        
        return result;
    }  // tokenize
        
    public void setValue(XDoc xml, Element container, String elementName, DataBase data)
    {
        // NOTE: inserts below the elementName (i.e. elementName is parent)
        xml.setXMLValue(container, elementName, (XDoc)data.getData());
    } // setValue
    

} // TokenizerGenericString
