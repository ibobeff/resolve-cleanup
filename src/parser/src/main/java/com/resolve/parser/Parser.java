/******************************************************************************
* (C) Copyright 2014
*
* Resolve Systems, LLC
*
* All rights reserved
* This software is distributed under license and may not be used, copied,
* modified, or distributed without the express written permission of
* Resolve Systems, LLC.
*
******************************************************************************/

package com.resolve.parser;

import groovy.lang.Binding;

import java.util.Hashtable;
import java.util.List;
import java.util.Map;

import org.apache.commons.collections4.map.AbstractReferenceMap;
import org.apache.commons.collections4.map.ReferenceMap;
import org.dom4j.Element;

import com.resolve.data.CacheObject;
import com.resolve.data.DataBase;
import com.resolve.data.DataString;
import com.resolve.groovy.script.GroovyScript;
import com.resolve.services.ServiceHibernate;
import com.resolve.services.ServiceWorksheet;
import com.resolve.services.WSDataMap;
import com.resolve.services.hibernate.vo.ResolveParserVO;
import com.resolve.services.util.BindingScript;
import com.resolve.util.Constants;
import com.resolve.util.GMTDate;
import com.resolve.util.Log;
import com.resolve.util.ScriptDebug;
import com.resolve.util.StringUtils;
import com.resolve.util.XDoc;

public class Parser extends CacheObject
{
    private static final long serialVersionUID = 4774092709228509293L;

	public static boolean debug = false;
    public static ReferenceMap cache = new ReferenceMap(AbstractReferenceMap.ReferenceStrength.SOFT, AbstractReferenceMap.ReferenceStrength.HARD);
    
    String name;
    TokenizerBase tokenizer;
    boolean isGenericParser = false;
 
	public Parser()
    {
    } // Parser
    
    public Parser(String name)
    {
        this.name = name.toUpperCase();
    } // Parser
        
    public static void init() throws Exception
    {
        // load targets from SQL DB
        Log.log.info("Loading Parser from DB");
        List<ResolveParserVO> lrp = ServiceHibernate.getAllParserOfMethod("GENERIC");
        lrp.addAll(ServiceHibernate.getAllParserOfMethod("TEMPLATE"));
        for (ResolveParserVO rp : lrp)
        {
            String name = rp.getUName(); 
            String script = rp.getUScript(); 
            String method = rp.getUMethod(); 
            String type = rp.getUType(); 
            String lineSeparator = rp.getULineseparator(); 
            String fieldSeparator = rp.getUSeparator(); 
            String heading = rp.getUHeading();
            String sid = rp.getSys_id();
                
            Parser parser = new Parser();
            parser.setSid(sid);
            parser.setName(name);
                
            //set the flag to true
            parser.isGenericParser = true;
                
            // init tokenizer if script is not defined
//	            if (StringUtils.isEmpty(script))
//	            {
//	            	if (method.equalsIgnoreCase("GENERIC") || (method.equalsIgnoreCase("TEMPLATE")))
//	            	{
            		TokenizerBase.load(parser, method, type, lineSeparator, fieldSeparator, heading);
//	            	}
//	            }
            
            parser.cache();
        }
    } // init
    
    public static Parser get( String sid) throws Exception
    {
        Parser result = null;
        
        result = (Parser)cache.get(sid);
        if (result == null)
        {
	        result = load(sid);
        }
        
        return result;
    } // get
    
    public static Parser getParserByName(String name) throws Exception
    {
        Parser result = null;
        if (name != null)
        {
	        result = (Parser)cache.get(name);
        }
        return result;
    } // getParserByName

    public static void remove(String sid)
    {
        Parser obj = (Parser)cache.get(sid);
        if (obj != null)
        {
            obj.removeCache();
        }
    } // remove
    
    public static Parser load(String parser_sid) throws Exception
    {
        Parser result = null;
        
        // load targets from SQL DB
        Log.log.info("Loading Parser from DB");
        ResolveParserVO rp = ServiceHibernate.findResolveParserWithoutRefs(parser_sid, null, "system");
        
        if (rp != null)
        {
            String name = rp.getUName(); 
            String script = rp.getUScript(); 
            String method = rp.getUMethod(); 
            String type = rp.getUType(); 
            String lineSeparator = rp.getULineseparator(); 
            String fieldSeparator = rp.getUSeparator(); 
            String heading = rp.getUHeading(); 
            
            result = new Parser();
            result.setSid(parser_sid);
            result.setName(name);
            
            // init tokenizer if script is not defined
            if (StringUtils.isEmpty(script) && !StringUtils.isEmpty(method) && !StringUtils.isEmpty(type))
            {
	            TokenizerBase.load(result, method, type, lineSeparator, fieldSeparator, heading);
            }
            
            result.cache();
        }
        
        return result;
    } // load
    
    public void cache()
    {
        cache.put(sid, this);
        if (name != null)
        {
	        cache.put(name, this);
        }
    } // cache
    
    public void removeCache()
    {
        if (name != null)
        {
            cache.remove(name);
        }
        cache.remove(sid);
    } // removeCache
    
    public String toString()
    {
        String result = "";
        result += "name:    "+name+"\n";
        result += "sid:     "+sid+"\n";
        result += "tokenizer:\n"+tokenizer+"\n";
        result += "\n";
        
        return result;
    } // toString
    
    /**
     * this is for all the other user parsers
     * 
     * @param parserId
     * @param raw
     * @param taskTimeout
     * @param debug
     * @return
     * @throws Exception
     */
    public Object parse(String parserId, String problemid, String raw, Map inputs, Map outputs, Map flows, Map params, int taskTimeout, ScriptDebug debug) throws Exception
    {
        Log.log.debug("Parsing started");
        Object result = null;
        String script = null;
        String scriptName = null;
        
        //get the script from the DB
        if(!StringUtils.isEmpty(parserId))
        {
	        ResolveParserVO rp = ServiceHibernate.findResolveParserWithoutRefs(parserId, null, "system");
	        script = rp.getUScript();
            scriptName = rp.getUName();
        }
        
        // extract values from raw into data
        if (StringUtils.isNotBlank(script))
        {
            try
            {
                Binding binding = new Binding();
                binding.setVariable(Constants.GROOVY_BINDING_PARSER, new BindingScript(BindingScript.TYPE_PARSER, binding));
                binding.setVariable(Constants.GROOVY_BINDING_RAW, raw);
                binding.setVariable(Constants.GROOVY_BINDING_LOG, Log.log);
                binding.setVariable(Constants.GROOVY_BINDING_DEBUG, debug);
                
                binding.setVariable(Constants.GROOVY_BINDING_INPUTS, inputs);
                binding.setVariable(Constants.GROOVY_BINDING_OUTPUTS, outputs);
                binding.setVariable(Constants.GROOVY_BINDING_FLOWS, flows);
                binding.setVariable(Constants.GROOVY_BINDING_PARAMS, params);
                if (problemid != null && script.contains(Constants.GROOVY_BINDING_WSDATA)) {
                	WSDataMap wsdata = ServiceWorksheet.getWorksheetWSDATA(problemid);
	                binding.setVariable(Constants.GROOVY_BINDING_WSDATA, wsdata);
                }
                binding.setVariable(Constants.GROOVY_BINDING_SYSLOG, Log.syslog);
                
                // execute script in separate thread then join
                result = (Object)GroovyScript.executeAsync(script, scriptName, true, binding, taskTimeout * 1000);
            }
            catch (Throwable e)
            {
                Log.log.error("Failed to parse RAW");
                throw new Exception(e);
            }
        }
        else if (tokenizer != null)
        {
            if(Log.log.isTraceEnabled())
            {
                Log.log.trace("Tokenizer method: "+tokenizer.getMethod()+" type: "+tokenizer.getType());
            }
            result = (Object)tokenizer.tokenize(raw).getData();
        }
        Log.log.debug("Parsing completed");
        
        return result;
    } // parse
    
    
    /**
     * this is for the GENERIC parser
     * 
     * @param raw
     * @param taskTimeout
     * @param debug
     * @return
     * @throws Exception
     */
    public Object parse(String raw, int taskTimeout, ScriptDebug debug) throws Exception
    {
        Log.log.debug("Parsing started");
        Object result = null;
        if (tokenizer != null)
        {
            if(Log.log.isTraceEnabled())
            {
                Log.log.trace("Tokenizer method: "+tokenizer.getMethod()+" type: "+tokenizer.getType());
            }
            result = (Object)tokenizer.tokenize(raw).getData();
        }
        else
        {
            Log.log.warn("Invalid parser specified. Parser: "+name);
        }
        Log.log.debug("Parsing completed");
        
        return result;
    } // parse
    
    public DataBase parseDefault(String containerName, Hashtable attr, String raw, XDoc resultXML) throws Exception
    {
        DataString result = null;
        
        if (resultXML != null)
        {
            result = new DataString();
            if (raw != null)
            {
                result.put(raw);
            }
            else
            {
                result.put("");
            }
            
            // convert containerName to non-space
            containerName = containerName.replace(' ', '_');
            
            // create xml doc
            Element root = resultXML.getRoot();
            root.addAttribute("TIMESTAMP", ""+GMTDate.getDate().getTime());
            root.addAttribute("TARGET", (String)attr.get("TARGET"));
            root.addAttribute("USER", (String)attr.get("USER"));
            
            // generate xml output
            resultXML.setCDataValue(root, "/"+containerName, raw);
        }
        
        return result;
    }  // parseDefault
    
    public String getName()
    {
        return name;
    }
    public void setName(String name)
    {
        this.name = name.toUpperCase();
    }
    public TokenizerBase getTokenizer()
    {
        return tokenizer;
    }
    public void setTokenizer(TokenizerBase tokenizer)
    {
        this.tokenizer = tokenizer;
    }
    
    public boolean isGenericParser()
	{
		return isGenericParser;
	}
   
    
//    public String getScript()
//    {
//        return script;
//    }
//    public void setScript(String script)
//    {
//        this.script = script;
//    }

} // Parser
