/******************************************************************************
* (C) Copyright 2014
*
* Resolve Systems, LLC
*
* All rights reserved
* This software is distributed under license and may not be used, copied,
* modified, or distributed without the express written permission of
* Resolve Systems, LLC.
*
******************************************************************************/

package com.resolve.parser;

import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.dom4j.Element;

import com.resolve.data.DataBase;
import com.resolve.data.DataListMap;
import com.resolve.services.ServiceHibernate;
import com.resolve.services.hibernate.vo.ResolveParserTemplateVO;
import com.resolve.util.Constants;
import com.resolve.util.Log;
import com.resolve.util.StringUtils;
import com.resolve.util.XDoc;

public class TokenizerTemplateListMap extends TokenizerBase
{
    String lineSeparator;
    
    public TokenizerTemplateListMap()
    {
    } // TokenizerTemplateListMap
    
    public TokenizerTemplateListMap(Parser parser, String lineSeparator)
    {
        super(parser.getSid(), Constants.PARSER_METHOD_TEMPLATE, Constants.DATA_TYPE_LISTMAP);
        
        init(parser, lineSeparator);
    } // TokenizerTemplateListMap
    
    void init(Parser parser, String lineSeparator)
    {
        this.setLineSeparator(lineSeparator);
        
        parser.setTokenizer(this);
    } // init
    
    public static void load(Parser parser, String lineSeparator, String heading) throws Exception
    {
        TokenizerTemplateListMap tokenizer = new TokenizerTemplateListMap(parser, lineSeparator);
        
        // remove all templates
        tokenizer.removeTemplates();
        
        // load heading template from SQL DB
        TemplateListMap template = new TemplateListMap();
        template.setType(Constants.PARSER_TEMPLATE_HEADING);
        template.setHeadingValues(heading);
        
        List<ResolveParserTemplateVO> lrpt = ServiceHibernate.getTemplatesForParser(parser.getSid(), null, "HEADING");
        if (lrpt.size()>0)
        {
        	ResolveParserTemplateVO rpt = lrpt.get(0);
            String match = rpt.getUMatchRegex(); 
            String heading_pos = rpt.getUNamePos(); 
            template.setHeadingRegex(match);
            template.setHeadingPositions(heading_pos);
        }
        tokenizer.addTemplate(template);
        
        // load value template(s) from SQL DB
        lrpt = ServiceHibernate.getTemplatesForParser(parser.getSid(), null, "VALUE");
        for (ResolveParserTemplateVO rpt : lrpt)
        {
            String match = rpt.getUMatchRegex(); 
            String name_pos = rpt.getUNamePos(); 
            String value_pos = rpt.getUValuePos(); 
            
            template = new TemplateListMap();
            template.setType(Constants.PARSER_TEMPLATE_VALUE);
            template.setRegex(match);
            template.setNamePositions(name_pos);
            template.setValuePositions(value_pos);
                
            // create template
            tokenizer.addTemplate(template);
        }
    } // load
    
    public void addTemplate(TemplateListMap template)
    {
        // add member for cleanup
        super.addMember(template);
    } // addTemplate
    
    public void removeTemplates()
    {
        super.removeMembers();
    } // removeTemplates
    
    public DataBase tokenize(String raw) throws Exception
    {
        DataListMap result = new DataListMap();
        
        if (raw != null)
        {
            boolean headingTemplateExists = false;
            String[] dataHeadings = null;
                
            int lineIdx = 0;
            String lines[] = raw.split(lineSeparator);
            
            /*
             * initialize heading
             */
            TemplateListMap headingTemplate = (TemplateListMap)members.iterator().next();
            if (headingTemplate.isHeading())
            {
                headingTemplateExists = true;
                
                // alloc dataHeadings
                dataHeadings = new String[headingTemplate.size()];
                    
                // check if heading value is defined or needs to be parsed from data
                if (headingTemplate.hasHeadingValue())
                {
	                // copy heading values from template
                    dataHeadings = headingTemplate.getHeadingValues();
                }
                
                // get heading values from data
                else
                {
                    // get heading regex to match
                    String regex = headingTemplate.getHeadingRegex();
                    if (StringUtils.isEmpty(regex))
                    {
                        throw new Exception("Missing Heading value or Heading Regex in heading template for TokenizerTemplateListMap");
                    }
                    Pattern pattern = Pattern.compile(regex);
                    
                    // define heading
                    while (lineIdx < lines.length)
                    {
                        String line = lines[lineIdx];
                        
                        // matched heading
                        Matcher matcher = pattern.matcher(line);
                        if (matcher.find())
                        {
                            //int size = matcher.groupCount() - 1;
                            int size = matcher.groupCount();
                            
                            // get heading values
                            for (int idx=0; idx < size; idx++)
                            {
                                String heading = matcher.group(headingTemplate.getHeadingPosition(idx));
                                if (StringUtils.isEmpty(heading))
                                {
                                    heading = "NAME" + idx;
                                }
                                dataHeadings[idx] = heading;
                            }
                        }
                        
                        // next line
                        lineIdx++;
                    } 
                }
            }
            
            // check value template exists
            if (members.size() < 2)
            {
                throw new Exception("Missing value template for TokenizerTemplateListMap");
            }
            
            
            /*
             * parse data
             */
            
            // get values
            while (lineIdx < lines.length)
            {
                String line = lines[lineIdx];
                    
                LinkedHashMap values = new LinkedHashMap();
                boolean refresh = true;
                    
                for (Iterator templateIdx = members.iterator(); templateIdx.hasNext(); )
                {
                    if (refresh && headingTemplateExists)
                    {
                        // skip heading template
                        templateIdx.next(); 
                    }
                    refresh = false;
                        
                    // get value template
                    TemplateListMap template = (TemplateListMap)templateIdx.next();
                    String regex = template.getRegex();
                    if (regex != null)
                    {
                        Pattern pattern = Pattern.compile(regex);
                        Matcher matcher = pattern.matcher(line);
                        if (matcher.find())
                        {
                            // get number of headings
                            //int size = matcher.groupCount() - 1;
                            int size = matcher.groupCount();
                                    
                            for (int idx=0; idx < size; idx++)
                            {
                                String heading;
                                
                                if (dataHeadings != null)
                                {
                                    //heading = headingTemplate.getColumnName(template.getNamePosition(idx)-1);
                                    heading = dataHeadings[template.getNamePosition(idx) - 1];
                                }
                                else
                                {
                                    heading = "NAME" + idx;
                                }
                                String value = matcher.group(template.getValuePosition(idx));
                                            
                                values.put(heading, value);
                            }
                        }
                    }
                    else
                    {
                        Log.log.error("Missing regex match in value template for TokenizerTemplateListMap");
                    }
                }
                
                // add result row
                if (values.size() > 0)
                {
                    result.add(values);
                }
                    
                // next line
                lineIdx++;
            }
        }
        
        return result;
    }  // tokenize
        
    public String getLineSeparator()
    {
        return lineSeparator;
    } // getLineSeparator

    public void setLineSeparator(String lineSeparator)
    {
        if (!StringUtils.isEmpty(lineSeparator) && !lineSeparator.equalsIgnoreCase("DEFAULT"))
        {
            this.lineSeparator = lineSeparator;
        }
        else
        {
            this.lineSeparator = "\n";
        }
    } // setLineSeparator

    public void setValue(XDoc xml, Element container, String elementName, DataBase data)
    {
        xml.setListMapValue(container, elementName, (List)data.getData());
    } // setValue
    
} // TokenizerTemplateListMap
