/******************************************************************************
* (C) Copyright 2014
*
* Resolve Systems, LLC
*
* All rights reserved
* This software is distributed under license and may not be used, copied,
* modified, or distributed without the express written permission of
* Resolve Systems, LLC.
*
******************************************************************************/

package com.resolve.parser;

import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.dom4j.Element;

import com.resolve.data.DataBase;
import com.resolve.data.DataString;
import com.resolve.services.ServiceHibernate;
import com.resolve.services.hibernate.vo.ResolveParserTemplateVO;
import com.resolve.util.Constants;
import com.resolve.util.StringUtils;
import com.resolve.util.XDoc;

public class TokenizerTemplateString extends TokenizerBase
{
    String lineSeparator;
    String match;
    int position;
    
    public TokenizerTemplateString()
    {
    } // TokenizerTemplateMap
    
    public TokenizerTemplateString(Parser parser, String lineSeparator)
    {
        super(parser.getSid(), Constants.PARSER_METHOD_TEMPLATE, Constants.DATA_TYPE_STRING);
        
        init(parser, lineSeparator);
    } // TokenizerTemplateMap
    
    void init(Parser parser, String lineSeparator)
    {
        this.setLineSeparator(lineSeparator);
        
        parser.setTokenizer(this);
    } // init
    
    public static void load(Parser parser, String lineSeparator) throws Exception
    {
        TokenizerTemplateString tokenizer = new TokenizerTemplateString(parser, lineSeparator);
        
        // load template from SQL DB
        List<ResolveParserTemplateVO> lrpt = ServiceHibernate.getTemplatesForParser(parser.getSid(), null, "VALUE");
        for (ResolveParserTemplateVO rpt: lrpt)
        {
            String match = rpt.getUMatchRegex(); 
            tokenizer.setMatch(match);
            
            String position = rpt.getUValuePos(); 
            if (position == null)
            {
                position = "0";
            }
            tokenizer.setPosition(position);
        }
    } // load
    
    public DataBase tokenize(String raw)
    {
        DataString result = new DataString();
        
        if (raw != null)
        {
            Pattern pattern = Pattern.compile(match);
            
            String lines[] = raw.split(lineSeparator);
            for (int lineIdx=0; lineIdx < lines.length; lineIdx++)
            {
                String line = lines[lineIdx];
                //Log.log.debug("matching line: "+line);
                
                //Log.log.debug("  match: "+match);
                Matcher matcher = pattern.matcher(line);
                if (match != null && matcher.find())
                {
                    //Log.log.debug("    found matched");
                        
                    String value = matcher.group(position);
                    result.put(value);
                }
            }
        }
        
        return result;
    }  // tokenize
    
    public String getLineSeparator()
    {
        return lineSeparator;
    } // getLineSeparator

    public void setLineSeparator(String lineSeparator)
    {
        if (!StringUtils.isEmpty(lineSeparator) && !lineSeparator.equalsIgnoreCase("DEFAULT"))
        {
            this.lineSeparator = lineSeparator;
        }
        else
        {
            this.lineSeparator = "\n";
        }
    } // setLineSeparator

    public void setMatch(String match)
    {
        this.match = match;
    } // setMatch
    
    public void setPosition(int pos)
    {
        this.position = pos;
    } // set Position
    
    public void setPosition(String pos)
    {
        this.position = Integer.parseInt(pos);
    } // setPosition
    
    public void setValue(XDoc xml, Element container, String elementName, DataBase data)
    {
        xml.setStringValue(container, elementName, (String)data.getData());
    } // setValue

} // TokenizerTemplateString
