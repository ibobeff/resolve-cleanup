/******************************************************************************
* (C) Copyright 2014
*
* Resolve Systems, LLC
*
* All rights reserved
* This software is distributed under license and may not be used, copied,
* modified, or distributed without the express written permission of
* Resolve Systems, LLC.
*
******************************************************************************/

package com.resolve.parser;

import java.util.List;
import java.util.Vector;

import org.dom4j.Element;

import com.resolve.data.DataBase;
import com.resolve.util.StringUtils;
import com.resolve.util.XDoc;

public abstract class TokenizerBase
{
    String sid;
    String method;
    String type;
    Vector members;
    
    public abstract DataBase tokenize(String raw) throws Exception;
    public abstract void setValue(XDoc xml, Element container, String elementName, DataBase data) throws Exception;
    
    public TokenizerBase()
    {
    } // TokenBase
    
    public TokenizerBase(String parser_sid)
    {
        this.sid = parser_sid;
    } // TokenizerBase
    
    public TokenizerBase(String parser_sid, String method, String type)
    {
        setSid(parser_sid);
        setMethod(method);
        setType(type);
        
        this.members = new Vector();
    } // TokenBase
    
    public void setSid(String sid)
    {
        if (sid != null)
        {
	        if (sid.endsWith("/TOKENIZER"))
	        {
	            this.sid = sid;
	        }
	        else
	        {
		        this.sid = sid+"/TOKENIZER";
	        }
        }
    } // setSid

    public static void load(Parser parser, String method, String type, String lineSeparator, String fieldSeparator, String heading) throws Exception
    {
        // NOTE: TokenizerBase removed at Parser
        
        // initialize lineSeparator
        lineSeparator = initSeparator(lineSeparator);
        
        // initialize fieldSeparator
        fieldSeparator = initSeparator(fieldSeparator);
        
        // load tokenizer
        if (method.equalsIgnoreCase("GENERIC"))
        {
            if (type.equalsIgnoreCase("STRING"))
            {
                TokenizerGenericString.load(parser);
            }
            else if (type.equalsIgnoreCase("XML"))
            {
                TokenizerGenericXML.load(parser);
            }
            else if (type.equalsIgnoreCase("MAP"))
            {
                TokenizerGenericMap.load(parser, lineSeparator, fieldSeparator);
            }
            else if (type.equalsIgnoreCase("LISTMAP"))
            {
                TokenizerGenericListMap.load(parser, lineSeparator, fieldSeparator, heading);
            }
        }
        else if (method.equalsIgnoreCase("TEMPLATE"))
        {
            if (type.equalsIgnoreCase("STRING"))
            {
                TokenizerTemplateString.load( parser, lineSeparator);
            }
            else if (type.equalsIgnoreCase("MAP"))
            {
                TokenizerTemplateMap.load(parser, lineSeparator);
            }
            else if (type.equalsIgnoreCase("LISTMAP"))
            {
                TokenizerTemplateListMap.load(parser, lineSeparator, heading);
            }
        }
    } // load
    
    public String toString()
    {
        String result = "";
        
        result += "  method: "+method+"\n";
        result += "  type: "+type+"\n";
        
        return result;
    } // toString
    
    protected void addMember(Object member)
    {
        members.add(member);
    } // addMember
    
    public List getMembers()
    {
        return this.members;
    } // getMembers
    
    public void removeMembers()
    {
        members.clear();
    } // removeMembers
    
    public String getType()
    {
        return type;
    }

    public void setType(String type)
    {
        this.type = type;
    }
    
    public String getMethod()
    {
        return method;
    }
    
    public void setMethod(String method)
    {
        this.method = method;
    }
    
    static String initSeparator(String separator)
    {
        if (!StringUtils.isEmpty(separator))
        {
            if (separator.equals("|"))
            {
                separator = "\\|";
            }
        }
        
        return separator;
    } // initSeparator

} // TokenizerBase

