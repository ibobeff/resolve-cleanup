package com.resolve;

import java.lang.reflect.Field;
import java.util.HashMap;
import java.util.Map;

import org.apache.log4j.Logger;
import org.junit.BeforeClass;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.powermock.api.mockito.PowerMockito;
import org.powermock.modules.junit4.PowerMockRunner;

import com.resolve.rscontrol.MAction;
import com.resolve.util.Constants;

@RunWith(PowerMockRunner.class)
public class MActionTest {
	
	    /**
	     * @throws java.lang.Exception
	     */
	    @BeforeClass
	    public static void setUpBeforeClass() throws Exception {
	        //processor = new DefaultMessageProcessor();
	        Field field = PowerMockito.field(com.resolve.util.Log.class, "log");
	        field.set(com.resolve.util.Log.class, Logger.getLogger(MActionTest.class));    
	    }

	    /**
	     * Test method for {@link com.resolve.esb.DefaultMessageProcessor#getMServer()}.
	     */
	    @SuppressWarnings({ "rawtypes", "unchecked" })
		@Test
	    public void cacheFlush_ClassCastException_OK() {
	    	Map params = new HashMap();
	    	params.put(Constants.EXECUTE_ACTIONNAME, "ActionName");
	    	params.put(Constants.EXECUTE_ACTIONID, "ActionID");
	    	params.put("FLUSHDEPENDENCIES", "FLUSHDEPENDENCIES");
	    	params.put("FLUSHACCESSRIGHTS", "FLUSHACCESSRIGHTS");
	    	MAction.cacheFlush(params);	    	
	    }
}
   