/******************************************************************************
* (C) Copyright 2014
*
* Resolve Systems, LLC
*
* All rights reserved
* This software is distributed under license and may not be used, copied,
* modified, or distributed without the express written permission of
* Resolve Systems, LLC.
*
******************************************************************************/

package com.resolve.rscontrol;

import java.nio.ByteBuffer;
import java.nio.charset.CharacterCodingException;
import java.nio.charset.Charset;
import java.nio.charset.CharsetDecoder;
import java.nio.charset.CodingErrorAction;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import com.resolve.services.ServiceHibernate;
import com.resolve.util.StringUtils;
import com.resolve.vo.ActionTaskResultVO;

public class ActionTaskUtils
{
    public static String getActionFormURL(String formWiki, Map voParams, Map atParams) throws Exception
    {
        String result = "";

        ActionTaskResultVO vo = new ActionTaskResultVO(voParams);
        String voStr = java.net.URLEncoder.encode(vo.toString(), "UTF-8");

        // init url
        result = "/resolve/service/wiki/view?wiki=" + formWiki + "&VOSTR=" + voStr;

        // append atParams
        if (atParams != null && atParams.size() > 0)
        {
            result += "&" + StringUtils.mapToUrl(atParams);
        }

        return result;
    } // getActionFormURL

    public static String getActionIdFromFullname(String fullname)
    {
        return ServiceHibernate.getActionIdFromFullname(fullname, "system");
    } // getActionIdFromFullname

    public static boolean containsSpecialChar(String string)
    {
        boolean result = false;
        if (StringUtils.isBlank(string))
        {
            return result;
        }

        Charset charset = Charset.forName("UTF-8");
        CharsetDecoder decoder = charset.newDecoder();
        decoder.onMalformedInput(CodingErrorAction.REPORT);
        decoder.onUnmappableCharacter(CodingErrorAction.REPORT);
        ByteBuffer bb = ByteBuffer.wrap(string.getBytes());
        try
        {
            decoder.decode(bb);
        }
        catch (CharacterCodingException e)
        {
            result = true;
        }

        return result;
    }
    
    public static boolean check2OrMoreSpaces(String name)
    {
        Pattern p = Pattern.compile("[ \t]{2,}");
        Matcher m = p.matcher(name);
        return m.find();
    }

} // ActionTaskUtils
