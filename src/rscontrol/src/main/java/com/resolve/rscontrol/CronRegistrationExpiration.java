/******************************************************************************
* (C) Copyright 2014
*
* Resolve Systems, LLC
*
* All rights reserved
* This software is distributed under license and may not be used, copied,
* modified, or distributed without the express written permission of
* Resolve Systems, LLC.
*
******************************************************************************/

package com.resolve.rscontrol;

import java.util.Date;
import java.util.HashMap;
import java.util.Hashtable;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import com.resolve.services.ServiceHibernate;
import com.resolve.services.hibernate.vo.ResolveRegistrationVO;
import com.resolve.util.Constants;
import com.resolve.util.GMTDate;
import com.resolve.util.Log;

public class CronRegistrationExpiration
{
    public void expiration()
    {
        try
        {
            Log.log.debug("CronRegistrationExpiration");
            
            Hashtable updates = new Hashtable();

            List<ResolveRegistrationVO> lrr = ServiceHibernate.findResolveRegistrationByStatus(Constants.REG_STATUS_ACTIVE);
            for (ResolveRegistrationVO row : lrr)
            {
	            String sid = row.getSys_id();
	            String guid = row.getUGuid();
	            String name = row.getUName();
                
                long currentTime = GMTDate.getDate().getTime();
	            long updateTime = row.getUUpdatetime().getTime();
	            //long updateTime = ((Timestamp)row.get("u_updatetime")).getTime();
                long interval = Main.main.configRegistration.getInterval() * 3 * 60000;
                        
                /*
                 * update inactive registration
                 */
                long inactiveTime = updateTime + interval;
                if (inactiveTime < currentTime)
                {
                    // deactivate registration
                    Log.log.info("INACTIVE Name: "+name+" guid: "+guid+" updatetime: "+new Date(updateTime));
                    updates.put(sid, Constants.REG_STATUS_INACTIVE);
                    if(row.getUType().equalsIgnoreCase("RSREMOTE")) {
                        Map unregisterConfigParams = new HashMap();
                        unregisterConfigParams.put("GUID", guid);
                        Main.esb.sendMessage("RSCONTROLS", "MRegister.unregisterRSRemoteConfig", unregisterConfigParams);
                    }
                }
                        
                /*
                 * update expired registration
                 */
                interval = Main.main.configRegistration.getExpiration() * 60000;
                long expireTime = updateTime + interval;
                if (expireTime < currentTime)
                {
                    // expire registration
                    Log.log.info("EXPIRED Name: "+name+" guid: "+guid+" updatetime: "+new Date(updateTime));
                    updates.put(sid, Constants.REG_STATUS_EXPIRED);
                    Map unregisterConfigParams = new HashMap();
                    unregisterConfigParams.put("GUID", guid);
                    Main.esb.sendMessage("RSCONTROLS", "MRegister.unregisterRSRemoteConfig", unregisterConfigParams);
                }
            }
            
            // update registrations
            for (Iterator i=updates.entrySet().iterator(); i.hasNext(); )
            {
                Map.Entry entry = (Map.Entry)i.next();
                String sid = (String)entry.getKey();
                String status = (String)entry.getValue();
                
                ResolveRegistrationVO row = ServiceHibernate.findResolveRegistrationById(sid, "system");
                if (row!=null)
                {
                	row.setUStatus(status);
                	ServiceHibernate.saveResolveRegistration(row, null);
                }
            }
        }
        catch (Throwable e)
        {
            Log.log.warn(e.getMessage(), e);
        }
        Log.log.debug("END CronRegistrationExpiration");
    } // expiration

} // CronRegistrationExpiration
