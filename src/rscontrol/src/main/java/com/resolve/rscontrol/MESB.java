/******************************************************************************
* (C) Copyright 2014
*
* Resolve Systems, LLC
*
* All rights reserved
* This software is distributed under license and may not be used, copied,
* modified, or distributed without the express written permission of
* Resolve Systems, LLC.
*
******************************************************************************/

package com.resolve.rscontrol;

import java.util.Hashtable;
import java.util.Map;

import com.resolve.util.Log;

public class MESB extends com.resolve.rsbase.MESB
{
    public String publish(Map params) throws Exception
    {
        String result = null;

        String group = ((String)params.get("GROUP")).toUpperCase();
        if (((ConfigESB)Main.main.configESB).publish(group))
        {
            result = "SUCCESS: group publication created: "+group;
        }
        else
        {
            result = "FAIL: group publication not created: "+group;
        }
        Log.log.info(result);
        return result;
    } // publish

    public String unpublish(Map params) throws Exception
    {
        String result = null;

        String group = ((String)params.get("GROUP")).toUpperCase();
        if (((ConfigESB)Main.main.configESB).unpublish(group))
        {
            result = "SUCCESS: group publication removed: "+group;
        }
        else
        {
            result = "FAIL: group publication not removed: "+group;
        }
        Log.log.info(result);
        return result;
    } // unpublish

    public Hashtable listPublication(Map params)
    {
        return ((ConfigESB)Main.main.configESB).getPublications();
    } // listPublication

} // MESB
