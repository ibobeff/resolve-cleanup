/******************************************************************************
* (C) Copyright 2014
*
* Resolve Systems, LLC
*
* All rights reserved
* This software is distributed under license and may not be used, copied,
* modified, or distributed without the express written permission of
* Resolve Systems, LLC.
*
******************************************************************************/

package com.resolve.rscontrol;

import com.resolve.services.util.VariableUtil;

public class ActionTaskProperties
{
    private static final ActionTaskProperties instance = new ActionTaskProperties();

    private ActionTaskProperties() {
    }

    public static ActionTaskProperties getInstance() {
      return instance;
    }
    
    public void set(String name, String value)
    {
        VariableUtil.setProperty(name, value);
    } // set

    public void set(String name, String value, String module)
    {
        VariableUtil.setProperty(name, value, module);
    } // set

    public void setEncrypt(String name, String value)
    {
        VariableUtil.setEncryptProperty(name, value);
    } // setEncrypt

    public void setEncrypt(String name, String value, String module)
    {
        VariableUtil.setEncryptProperty(name, value, module);
    } // setEncrypt

    public String get(String name)
    {
        return VariableUtil.getProperty(name);
    } // get
    
    public void remove(String name)
    {
        VariableUtil.removeProperty(name);
    } // remove

} // Properties
