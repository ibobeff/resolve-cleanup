/******************************************************************************
* (C) Copyright 2014
*
* Resolve Systems, LLC
*
* All rights reserved
* This software is distributed under license and may not be used, copied,
* modified, or distributed without the express written permission of
* Resolve Systems, LLC.
*
******************************************************************************/

package com.resolve.rscontrol;

import java.io.File;
import java.util.HashMap;
import java.util.Map;
import java.util.Properties;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.TimeUnit;

import com.resolve.cron.CronScheduler;
import com.resolve.esb.DefaultHandler;
import com.resolve.esb.MListener;
import com.resolve.esb.amqp.MAmqpRunbookExecutionListener;
import com.resolve.execute.ExecuteMain;
import com.resolve.notification.NotificationAPI;
import com.resolve.parser.Parser;
import com.resolve.persistence.util.HibernateUtil;
import com.resolve.rsbase.ConfigCAS;
import com.resolve.rsbase.ConfigId;
import com.resolve.rsbase.ConfigSQL;
import com.resolve.rsbase.MainBase;
import com.resolve.search.ConfigSearch;
import com.resolve.search.SearchAdminAPI;
import com.resolve.service.LicenseService;
import com.resolve.services.ServiceHibernate;
import com.resolve.services.ServiceJDBC;
import com.resolve.services.archive.BaseArchiveTable;
import com.resolve.services.archive.ConfigArchive;
import com.resolve.services.util.PropertiesUtil;
import com.resolve.services.util.SequenceUtils;
import com.resolve.sql.SQL;
import com.resolve.sql.SQLDriverInterface;
import com.resolve.thread.ScheduledExecutor;
import com.resolve.thread.TaskExecutor;
import com.resolve.util.Constants;
import com.resolve.util.ERR;
import com.resolve.util.Log;
import com.resolve.util.PerformanceDebug;
import com.resolve.util.StringUtils;
import com.resolve.util.XDoc;
import com.resolve.util.performance.datasources.ExecutionStep;
import com.resolve.util.performance.datasources.ExecutionStepPhase;
import com.resolve.util.performance.datasources.RunbookExecutionTimeDataSource;
import com.resolve.util.performance.ExecutionTimeUtil;

public class Main extends com.resolve.rsbase.MainBase
{
    public static boolean isMaster = false;
    final static long MAXEVALDAYS = 60;

    // public static GraphDatabaseService graphDB;

    static Properties license;

    public ConfigSQL configSQL;
    public ConfigCAS configCAS;
    public ConfigCron configCron;
    public ConfigArchive configArchive;
    public ConfigSocial configSocial;
    public ConfigGeneral configGeneral;
    private ConfigSearch configSearch;
    private ConfigSelfCheck configSelfCheck;

    CronScheduler cron;

    /*
     * Main execution method for RSCONTROL
     */
    public static void main(String[] args)
    {
        String serviceName = null;

        try
        {
            if (args.length > 0)
            {
                serviceName = args[0];
            }
            main = new Main();
            main.init(serviceName);
        }
        catch (Throwable e)
        {
            e.printStackTrace();
            Log.alert(ERR.E10002, e);

            System.exit(0);
        }

    } // main

    protected void initConfig() throws Exception
    {
    	super.initConfig();
    } // initConfig
    
    public void init(String serviceName) throws Exception
    {
        // init release information
        initRelease(new Release(serviceName));

        // init logging
        initLog();

        // init enc
        initENC();

        // init signal handlers
        initShutdownHandler();

        // init configuration
        initConfig();
        
        // init monitoring/flow control
        initRSControlMListener();

        // init startup
        initStartup();

        initThreadPools();

        // init timezone
        initTimezone();

        // init sql persistence
        initSQL();

        // init persistence
        initPersistence();

        // init hazelcast
//        initHazelcast();

        // init cache
        initCache();

        // add self registration
        initRegistration();

        // initialize elasticsearch
        initElasticSearch();

        // start config
        startConfig();
        
        initPerformanceStatistics();

        // init cluster properties
        initClusterProperties();

        // init scheduler
        startCron(new CronMain());

        // init sequence number
        initSequenceNumbers();

        // init nodeid
        initNodeId();

        // save configuration
        initSaveConfig();

        // init message bus
        initESB();

        // initialize Notification API
        NotificationAPI.init();

        // start message bus only after initialization is finished
        startESB();

        // init metric
        initMetric();

        // init cassandra
        //initCassandra();
        
        // display started message and set MListener to active
        initComplete();

        // init license service
        initLicenseService();

        // init license
        checkLicense();
        
        // start default tasks
        startDefaultTasks();
        
        // start self check tasks
        startSelfCheckTasks();

        // cleans the resolve_blueprint table by asking
        // all the live rsmgmts.
        MConfig.cleanBlueprint();

    } // init
    
	private void initPerformanceStatistics() {
		if (configGeneral.getIsCollectingRunbookStatistics()) {
			String rootPath = configGeneral.getHome() + File.separator + "rscontrol" + File.separator + "log";
			RunbookExecutionTimeDataSource dataSource = new RunbookExecutionTimeDataSource(rootPath);
			dataSource.initLogFile();
			ExecutionTimeUtil.setOutputDataSource(dataSource);
			return;
		}
		ExecutionTimeUtil.setOutputDataSource(new RunbookExecutionTimeDataSource("") {
			// "Mechanism" for turning off the statistics in prod env
			@Override
			public void log(String messageId, String processId, String runbookId, ExecutionStep step, ExecutionStepPhase phase) {

			}

			@Override
			public void exportExecutionTimeStatistics(String messageId) {

			}
		});
	}
    
	protected void startESB() throws Exception
    {
        super.startESB();

        // The following block was in initESB() but during initESB certain properties of the server don't get
        // initialized but startESB finishes all the initialization. Hence this block of code is here. We may
        // think of refactoring in future so the initialization is robust with all the resources created

        // RSCONTROL shared queue
        DefaultHandler handler = new MessageHandler(mServer.getDefaultMSender(), mServer.getServiceName());
        //this listener is using an extended message processor.
        RSControlMessageProcessor messageProcessor = new RSControlMessageProcessor();
        MListener listener1 = mServer.createListener(Constants.ESB_NAME_RSCONTROL, "com.resolve.rscontrol", handler, messageProcessor);
        listener1.init(false);
 
        // RSSERVER shared queue
        MListener listener2 = mServer.createListener(Constants.ESB_NAME_RSSERVER, "com.resolve.rscontrol");
        listener2.init(false);

        // RSSERVER_EVENT shared queue
        MListener listener3 = mServer.createListener(Constants.ESB_NAME_RSSERVER_EVENT, "com.resolve.rscontrol");
        listener3.init(false);

        Log.log.info("  Initializing ESB Listener for the queue: " + MainBase.main.configId.getGuid());
        MListener mListener4 = mServer.createListener(MainBase.main.configId.getGuid(), "com.resolve.rscontrol");
        mListener4.init(false);
        
        MListener listener5 = mServer.createListener(Constants.ESB_NAME_SYSTEM_RSCONTROL, "com.resolve.rscontrol", handler, messageProcessor);
        listener5.init(false);
        
        if (isAMQPServer())
        {
            mServer.createQueue(Constants.ESB_NAME_EXECUTEQUEUE);
            mServer.createDurableQueue(Constants.ESB_NAME_DURABLE_EXECUTEQUEUE);
        }
    
        MAmqpRunbookExecutionListener.msgProcessor.setMServer(mServer);
        MAmqpRunbookExecutionListener.msgProcessor.setMListener(listener2);
        
        // add subscriptions
        mServer.subscribePublication(Constants.ESB_NAME_BROADCAST);
        mServer.subscribePublication(Constants.ESB_NAME_RSPROCESS);
        mServer.subscribePublication(Constants.ESB_NAME_RSCONTROLS);
        mServer.subscribePublication(Constants.ESB_NAME_RSSERVERS);

        // init ServiceHibernate
        ServiceHibernate.setEsb(esb, configId.guid);
    } // startESB

    /**
     * Initialize search (ElasticSearch) service
     */
    protected void initElasticSearch()
    {
        //the search is always on in RSCONTROL
        Log.log.info("Initializing Search.");
        //add system properties that the RSSearch needs, this removes the Hibernate 
        //dependency from RSRemote when we eventually start accessing RSSearch from RSRemote.
        Map<String, String> properties = new HashMap<String, String>();
        properties.put("search.exclude.namespace", PropertiesUtil.getPropertyString("search.exclude.namespace"));
        properties.put("search.exclude.document.pattern", PropertiesUtil.getPropertyString("search.exclude.document.pattern"));
        properties.put("search.attachment.limit", PropertiesUtil.getPropertyString("search.attachment.limit"));
        properties.put("dashboard.namespace.exclude", PropertiesUtil.getPropertyString("dashboard.namespace.exclude"));

        configSearch.setProperties(properties);
        // enable in-memory ES cache for RSCONTROL
        SearchAdminAPI.init(configSearch, true);
        Log.log.info("Search Initialized Successfully.");
    }

    protected void initENC() throws Exception
    {
		// load configuration state and initialization overrides
		Log.log.info("Loading configuration settings for enc");
		loadENCConfig();
		super.initENC();		
    } // initENC

    protected void loadENCConfig() throws Exception
    {
        ConfigENC.load();
    } // loadENCConfig

    public Main()
    {
        Main.main = this;
    } // Main

    protected void initComplete() throws Exception
    {
        super.initComplete();

        // log msg
        String msg = release.name.toUpperCase() + " started ...";
        Log.log.warn(msg);

        // log event
        ServiceHibernate.logEvent(msg, "system");
        
        PerformanceDebug.setDebugRSControl(configGeneral.isPerfDebug());

    } // initComplete

    /*
     * NOTE: String args[] is required to catch NT service shutdown message
     */
    protected void exit(String[] argv)
    {
        System.exit(0);
    } // exit

    protected void terminate()
    {
        // shutdown
        isShutdown = true;

        Log.log.warn("Terminating " + release.name.toUpperCase());

        try
        {
            // log event - needs come before esb stop
            String msg = release.name.toUpperCase() + " terminated";
            initSQL();
            Log.log.info("Log final system event before terminating");
            ServiceJDBC.logEvent(msg, "system");
        }
        catch (Throwable e)
        {
            Log.log.error("Error on terminate RSControl: " + e.getMessage(), e);
        }

        try
        {
            // close queue consumers except GUID queue
            Log.log.info("Closing listeners");
            MainBase.main.esb.getMServer().closeListeners(false);
        }
        catch (Throwable e)
        {
            Log.log.error("Error on terminate RSControl: " + e.getMessage(), e);
        }

        try
        {
            // save cron
            if (cron != null)
            {
                Log.log.info("Stopping CRON");
                cron.stop();
            }
        }
        catch (Throwable e)
        {
            Log.log.error("Error on terminate RSControl: " + e.getMessage(), e);
        }

        try
        {
            // wait for completion or timeout
            RSControlMListenerUtil.waitForActiveCompletion();
        }
        catch (Throwable e)
        {
            Log.log.error("Error on terminate RSControl: " + e.getMessage(), e);
        }

        try
        {
            // stop hazelcast
            Log.log.info("Shutdown hazelcast");
//            Hazelcast.shutdownAll();
        }
        catch (Throwable e)
        {
            Log.log.error("Error on terminate RSControl: " + e.getMessage(), e);
        }

        try
        {
            Log.log.info("Shutdown hibernate");
            ServiceHibernate.shutdownHibernate();
        }
        catch (Throwable e)
        {
            Log.log.error("Error on terminate RSControl: " + e.getMessage(), e);
        }

        // stopping message server
        if (configSearch.isActive())
        {
            Log.log.info("Stopping Search Client");
            SearchAdminAPI.shutdown();
        }

        try
        {
            // save config
            super.exitSaveConfig();

            // terminate
            super.terminate();
        }
        catch (Throwable e)
        {
            Log.log.error("Error on terminate RSControl: " + e.getMessage(), e);
            Log.log.error(e.getMessage(), e);
        }

        Log.log.warn("Terminated " + release.name.toUpperCase());
    } // terminate

    protected void initSQL()
    {
        Log.log.info("Starting SQL");

        // get db driver
        SQLDriverInterface driver = SQL.getDriver(configSQL.getDbtype(), configSQL.getDbname(), configSQL.getHost(), configSQL.getUsername(), configSQL.getP_assword(), configSQL.getUrl(), configSQL.isDebug());

        if (driver != null)
        {
            SQL.init(driver);
            SQL.start();
        }
        
        HibernateUtil.setDebugOutput(configSQL.isPerfDebug());
        HibernateUtil.setStacktraceOutput(configSQL.isDebug());
        //GenericJDBCHandler.setDBType(configSQL.getDbtype().toUpperCase());
    } // initSQL

    // protected void initGraphDB()
    // {
    // Log.log.info("Initializing GraphDB: "+getProductHome()+"/config/graphdb");
    // System.out.println("Initializing GraphDB: "+getProductHome()+"/config/graphdb");
    //
    // // connect to zk and init graphdb
    // boolean connected = connectGraphDB();
    // /*
    // if (!connected)
    // {
    // // remove graphdb store
    // File graphDBDir = FileUtils.getFile(getProductHome()+"/config/graphdb");
    // if (graphDBDir.exists())
    // {
    // try
    // {
    // Log.log.info("Deleting graphdb directory");
    // FileUtils.deleteDirectory(graphDBDir);
    // }
    // catch (Exception e)
    // {
    // Log.log.error(e.getMessage(), e);
    // }
    // }
    //
    // // reconnect
    // connected = connectGraphDB();
    // }
    // */
    //
    // if (connected && graphDB != null)
    // {
    // if (Log.log.getLevel() == Level.TRACE)
    // {
    // checkGraphSize();
    // }
    // System.out.println("GraphDB initialized successfully");
    // Log.log.info("GraphDB initialized successfully");
    // }
    // else
    // {
    // Log.log.error("Failed to initialize GraphDB");
    // }
    // } // initGraphDB

    // boolean connectGraphDB()
    // {
    // boolean result = false;
    //
    // int retry = 3;
    // while (!result && retry > 0)
    // {
    // try
    // {
    // Log.log.debug("Connecting to graphdb");
    // com.resolve.util.Properties config = new
    // com.resolve.util.Properties(getProductHome()+"/config/neo4j.properties");
    // // config.put( Config.NODE_AUTO_INDEXING, "true" );
    // // config.put( Config.RELATIONSHIP_AUTO_INDEXING, "true" );
    // // config.put( Config.NODE_KEYS_INDEXABLE, "sys_id, titleurl");
    // // config.put( Config.RELATIONSHIP_KEYS_INDEXABLE, "xyz");
    // graphDB = new
    // HighlyAvailableGraphDatabase(getProductHome()+"/config/graphdb",
    // config.toMapString());
    // result = true;
    // }
    // catch (ZooKeeperTimedOutException e)
    // {
    // try
    // {
    // Log.log.debug("Waiting to retry connection to graphdb");
    // Thread.sleep(30000);
    // }
    // catch (Throwable t) { }
    // }
    // catch (Throwable t)
    // {
    // Log.log.debug("Error connecting to graphdb");
    // t.printStackTrace();
    // Log.log.error(t.getMessage(), t);
    // }
    // retry--;
    // }
    //
    // if (result)
    // {
    // Log.log.info("Connected to graphdb");
    // }
    // else
    // {
    // Log.log.error("Failed to connect to graphdb");
    // }
    // return result;
    // } // connectGraphDB

    // void checkGraphSize()
    // {
    // org.neo4j.graphdb.Transaction tx = graphDB.beginTx();
    // try
    // {
    // int count = 0;
    // for ( org.neo4j.graphdb.Node node : graphDB.getAllNodes() )
    // {
    // count++;
    // }
    // tx.success();
    //
    // System.out.println("graphdb node count: "+count);
    // Log.log.trace("graphdb node count: "+count);
    // }
    // catch (Throwable t)
    // {
    // t.printStackTrace();
    // }
    // finally
    // {
    // tx.finish();
    // }
    // } // checkGraphSize

    protected void loadConfig(XDoc configDoc) throws Exception
    {
        super.loadConfig(configDoc);
        
        // override and additional configX.load
        configGeneral = new ConfigGeneral(configDoc);
        configGeneral.load();

        // need to load ConfigId before ConfigESB
        configId = new ConfigId(configDoc);
        configId.load();

        configSQL = new ConfigSQL(configDoc);
        configSQL.load();

        configCAS = new ConfigCAS(configDoc);
        configCAS.load();

        configRegistration = new ConfigRegistration(configDoc);
        configRegistration.load();

        configESB = new ConfigESB(configDoc);
        configESB.load();

        configCron = new ConfigCron(configDoc);
        configCron.load();

        configSocial = new ConfigSocial(configDoc);
        configSocial.load();

        configSearch = new ConfigSearch(configDoc);
        configSearch.load();
        
        configSelfCheck = new ConfigSelfCheck(configDoc);
        configSelfCheck.load();
        
    } // loadConfig

    protected void saveConfig() throws Exception
    {
        configSQL.save();
        configCron.save();
        configSocial.save();
        configCAS.save();
        configSearch.save();
        configSelfCheck.save();
        
        super.saveConfig();
    } // saveConfig

    protected void startConfig() throws Exception
    {
        super.startConfig();
    } // startConfig

    protected void startCron(CronMain cronMain)
    {
        Log.log.info("Starting CRON");
        if(isClusterModeActive())
        {
            cron = cronMain;
            cron.start();
        }
        else
        {
            Log.log.info("The cluster mode is inactive, CRON not started.");
        }
    } // startCron

    protected void stopCron()
    {
        Log.log.info("Stopping CRON");
        if(isClusterModeActive())
        {
            Log.log.info("The cluster mode is still active, CRON not stopped.");
        }
        else
        {
            cron.stop();
        }
    } // stopCron

    @Override
    protected void initESB() throws Exception
    {
    	super.initESB(true);
    } // initESB

    protected void initPersistence()
    {
        String customMappingFileLocation = configGeneral.getHome() + File.separator + "rscontrol" + File.separator + "config";
        String hibernateCfgLocation = configGeneral.getHome() + File.separator + "rscontrol" + File.separator + "config" + File.separator + "hibernate.cfg.xml";

        ServiceHibernate.initPersistence(configSQL, configCAS, customMappingFileLocation, hibernateCfgLocation, false);
        
        HibernateUtil.deleteAllRBLimitData();
        HibernateUtil.initMasterLockObject();
        //new GenericJDBCHandler().refreshTableMap(null);

    } // initPersistence

//    void initHazelcast()
//    {
////        Hazelcast.getCluster();
//
//        // remove shutdown hooks - the property
//        // hazelcast.shutdownhook.enabled=false does not seem to work
//        JavaUtils.removeShutdownHook("hz.ShutdownThread");
//    } // initHazelcast

    void initRegistration() throws Exception
    {
        MRegister.registerSelf();
    } // initRegistration

    void initCache() throws Exception
    {
        try
        {
            Log.log.info("Loading PARSERS");
            Parser.init();
        }
        catch (Throwable e)
        {
            Log.log.warn(e.getMessage(), e);
        }
    } // initCache

    static void initSequenceNumbers()
    {
        SequenceUtils.getNextSequence(Constants.PERSISTENCE_SEQ_INIT);
    } // initSequenceNumbers

    void initNodeId()
    {
        if (StringUtils.isEmpty(this.configId.getNodeid()))
        {
            String nodeid = SequenceUtils.getNextSequence(Constants.PERSISTENCE_SEQ_RC);
            this.configId.setNodeid(nodeid);
        }
    } // initNodeId

    @Override
    protected void initMetric() throws Exception
    {
        metric = new Metric();
        
        super.initMetric();
        // send JMS to rsmgmt with the GUID to refresh threshold values
        //BaseMetric.requestThresholdUpdate(300);
    } // initMetric

    protected void checkLicense()
    {
        LicenseService licenseService = getLicenseService();
        
        licenseService.checkLicense();
        //check for maximum heap size
        licenseService.checkMaximumMemory(this);

        //since this component is ok to run, send its presence.
        licenseService.sendLicenseRegistration();

        // schedule next license check
        ScheduledExecutor.getInstance().executeDelayed(this, "checkLicense", 30, TimeUnit.MINUTES);
    } // checkLicense

    @Override
    protected void initClusterProperties()
    {
        String clusterMode = PropertiesUtil.getPropertyString(getClusterModeProperty());
        if(StringUtils.isBlank(clusterMode))
        {
            //this is for mainly standalone system where there is no
            //system property set
            clusterMode = Constants.CLUSTER_MODE_ACTIVE;
        }
        Map<String, String> params = new HashMap<String, String>();
        params.put(getClusterModeProperty(), clusterMode);
        setClusterProperties(params);
        if (isClusterModeActive())
        {
            activateCluster(params);
        }
        
        Log.log.info("Resolve cluster : " + configGeneral.getClusterName() + ", Cluster mode : " + clusterMode);

        //persist the property  in DB
        PropertiesUtil.setProperties(params);
    }

    public void activateCluster(Map<String, String> params)
    {
        Log.log.info("activateCluster: clustModeActive = " + isClusterModeActive() + ", cronActive = " + CronElectMaster.isCronActive()) ;

        //prevent reactivating if it's already active
        if(!isClusterModeActive() || !CronElectMaster.isCronActive())
       {
            Log.log.error("try to start cron again");
            try
            {
                params.put(getClusterModeProperty(), Constants.CLUSTER_MODE_ACTIVE);
                setClusterProperties(params);
                //persist the property in DB
                PropertiesUtil.setProperties(params);
                CronElectMaster.setCronActive(true);
                startCron(new CronMain());
            }
            catch (Exception e)
            {
                Log.log.error(e.getMessage(), e);
            }
        }
    }

    public void deactivateCluster(Map<String, String> params)
    {
        Log.log.info("deactivateCluster: clustModeActive = " + isClusterModeActive() + ", cronActive = " + CronElectMaster.isCronActive()) ;
        
        //prevent deactivating if it's already inactive
        if(isClusterModeActive() || CronElectMaster.isCronActive())
        {
            try
            {
                params.put(getClusterModeProperty(), Constants.CLUSTER_MODE_INACTIVE);
                setClusterProperties(params);
                //persist the property in DB
                PropertiesUtil.setProperties(params);
                CronElectMaster.setCronActive(false);
                stopCron();
            }
            catch (Exception e)
            {
                Log.log.error(e.getMessage(), e);
            }
        }
    }

    void startDefaultTasks()
    {
        // registration
        String jobname = "REGISTRATION";
        ScheduledExecutor.getInstance().executeRepeat(jobname, MRegister.class, "registerSelf", Main.main.configRegistration.getInterval() * 60, TimeUnit.SECONDS);
        if (Main.main.configRegistration.isLogHeartbeat())
        {
        	ScheduledExecutor.getInstance().executeRepeat(jobname, MRegister.class, "logHeartbeat", Main.main.configRegistration.getInterval() * 60, TimeUnit.SECONDS);
        }

        // expiration
        jobname = "EXPIRATION";
        ScheduledExecutor.getInstance().executeRepeat(jobname, CronRegistrationExpiration.class, "expiration", Main.main.configRegistration.getInterval() * 2 * 60, TimeUnit.SECONDS);

        // electMaster
        ScheduledExecutor.getInstance().executeRepeat("ELECTMASTER", CronElectMaster.class, "electMaster", ((ConfigRegistration) Main.main.configRegistration).getElection(), TimeUnit.SECONDS);

        // config properties
        Map<String,String> params = new HashMap<String,String>();
        params.put("JOBNAME", "CONFIG-PROPERTIES");
        params.put("TYPE", Constants.ACTION_INVOCATION_TYPE_INTERNAL);
        params.put("FILENAME", "config/GetProperties.groovy");
        params.put("CLASSMETHOD", "MResult.configResult");
        ScheduledExecutor.getInstance().executeDelayed(ExecuteMain.class, "execute", params, 3, TimeUnit.MINUTES);

        /*
         * Archive has been removed from RSControl.
         * Set ELASTIC_SEARCH_SNAPSHOT cron job manually if required in RSView
         */
         // create snapshot for elasticSearch   
//        MCron.scheduleTask("ELASTIC_SEARCH_SNAPSHOT", "RSVIEWS#MIndex.createElasticSearchSnapShot", configArchive.getSchedule(), "system", false, false);
        
        // schedule social archive
        if (configSocial.isActive())
        {
            MCron.scheduleTask("SOCIAL_ARCHIVE", "RSVIEWS#MSocial.archive", configSocial.getArchive(), "system", true, false);
            MCron.scheduleTask("DAILY_EMAIL", "RSVIEWS#MSocial.dailyEmail", configSocial.getDailyemail(), "system", true, false);
        }

//        // schedule cassandra compaction
//        if (configCAS.isActive())
//        {
//            MCron.scheduleTask("CASSANDRA_ARCHIVE", "CASSANDRA#MAction.compactCassandra", configCAS.getSchedule(), "system", true, false);
//        }
        //Remove Cassandra Archiving
        //params.clear();
        //params.put(Constants.CRON_NAME, "CASSANDRA_ARCHIVE");
        //MCron.removeTask(params);
        MCron.unscheduleTask("CASSANDRA_ARCHIVE");

        // send metrics
        ScheduledExecutor.getInstance().executeRepeat(metric, "sendMetrics", 5, 5, TimeUnit.MINUTES);

        TaskExecutor.execute(RSControlMListenerUtil.class, "pollEventQueue");
        
        // HP TO DO Why every 10 seconds DB connection pool is being refreshed? Does it affects connections in use? 
        ScheduledExecutor.getInstance().executeRepeat(HibernateUtil.class, "refreshPool", 10, HibernateUtil.refreshInterval, TimeUnit.SECONDS);
        
        //load all the RSS when the RSCONTROL comes up
        MSocial.loadAllRss();
        MRegister.syncRSRemoteRegistrations();
        
    } // startDefaultTasks
    
    public void startSelfCheckTasks()
    {
        RSControlSelfCheck rsControlSelfCheck = RSControlSelfCheck.getInstance();
        
        if (configSelfCheck.isDbSCActive())
        {
            SelfCheck.dbTimeout = configSelfCheck.getDbSCTimeout();
            rsControlSelfCheck.setDbType(configSQL.getDbtype());
            ScheduledExecutor.getInstance().executeRepeat(SelfCheck.class, "checkDBConnectivity", 10, configSelfCheck.getDbSCInterval(), TimeUnit.SECONDS);
        }
        
        if (configSelfCheck.isPingActive())
        {
        	SelfCheck.pingThreshold = configSelfCheck.getPingThreshold();
        	ScheduledExecutor.getInstance().executeRepeat(SelfCheck.class, "pingRsRemote", 10, configSelfCheck.getPingInterval(), TimeUnit.SECONDS);
        }
        
        if (configSelfCheck.isEsSCActive())
        {
            rsControlSelfCheck.setEsSCRetryCount(configSelfCheck.getEsSCRetryCount());
            rsControlSelfCheck.setEsSCRetryDelay(configSelfCheck.getEsSCRetryDelay());
            ScheduledExecutor.getInstance().executeRepeat(SelfCheck.class, "checkESHealth", 10, configSelfCheck.getEsSCInterval(), TimeUnit.SECONDS);
        }
        
        if (configSelfCheck.isRbExecSCActive())
        {
            rsControlSelfCheck.setRbExecRunbook(configSelfCheck.getRbExecSCRunbook());
            ScheduledExecutor.getInstance().executeRepeat(SelfCheck.class, "executeRunbook", 10, configSelfCheck.getRbExecSCInterval(), TimeUnit.SECONDS);
        }
    } //startSelfCheckTasks
    
    protected void initRSControlMListener() throws Exception
    {
        if (configGeneral.getExecutorBusyRatio()!=null)
        {
            RSControlMListenerUtil.EXECUTOR_BUSY_RATIO= Float.valueOf(configGeneral.getExecutorBusyRatio());
        }
        Log.log.info("RSControlMListenerUtil.EXECUTOR_BUSY_RATIO = " + RSControlMListenerUtil.EXECUTOR_BUSY_RATIO);
        
        if (configGeneral.getDbpoolBusyRatio()!=null)
        {
            RSControlMListenerUtil.DBPOOL_BUSY_RATIO= Float.valueOf(configGeneral.getDbpoolBusyRatio());
        }
        Log.log.info("RSControlMListenerUtil.DBPOOL_BUSY_RATIO = " + RSControlMListenerUtil.DBPOOL_BUSY_RATIO);

        if (configGeneral.getStartExecutorBusyRatio()!=null)
        {
            RSControlMListenerUtil.START_EXECUTOR_BUSY_RATIO= Float.valueOf(configGeneral.getStartExecutorBusyRatio());
        }
        Log.log.info("RSControlMListenerUtil.START_EXECUTOR_BUSY_RATIO = " + RSControlMListenerUtil.START_EXECUTOR_BUSY_RATIO);
        
        if (configGeneral.getSysExecutorBusyRatio()!=null)
        {
            RSControlMListenerUtil.SYSTEM_EXECUTOR_BUSY_RATIO = Float.valueOf(configGeneral.getSysExecutorBusyRatio());
        }
        Log.log.info("RSControlMListenerUtil.SYSTEM_EXECUTOR_BUSY_RATIO = " + RSControlMListenerUtil.SYSTEM_EXECUTOR_BUSY_RATIO);
        
    	RSControlMListenerUtil.MAX_RUNNING_RUNBOOK_NUM = configGeneral.getMaxRunningRBs();
        Log.log.info("RSControlMListenerUtil.MAX_RUNNING_RUNBOOK_NUM = " + RSControlMListenerUtil.MAX_RUNNING_RUNBOOK_NUM);
        
    	RSControlMListenerUtil.MIN_RUNNING_RUNBOOK_NUM = configGeneral.getMinRunningRBs();
        Log.log.info("RSControlMListenerUtil.MIN_RUNNING_RUNBOOK_NUM = " + RSControlMListenerUtil.MIN_RUNNING_RUNBOOK_NUM);
        
        RSControlMListenerUtil.USE_ACTIVE_RUNBOOK_COUNT = configGeneral.isUseActiveRBCount();
        Log.log.info("RSControlMListenerUtil.USE_ACTIVE_RUNBOOK_COUNT = " + RSControlMListenerUtil.USE_ACTIVE_RUNBOOK_COUNT);
        
    } 

} // Main
