package com.resolve.rscontrol;

import com.resolve.util.ERR;
import com.resolve.util.Log;

public class SelfCheck
{
    private RSControlSelfCheck rsControlSelfCheck = RSControlSelfCheck.getInstance();
    static int dbTimeout = 5; //how long to wait for heartbeat completion before notifying 
    static int pingThreshold;
    
    public SelfCheck()
    {
    }
    
    public void checkDBConnectivity()
    {
        try
        {
            Thread thread = new Thread(rsControlSelfCheck);
            thread.start();
            
            if (dbTimeout == 0)
            {
            	dbTimeout = 5;
            }
            
            long startWait = System.currentTimeMillis();
            int waitTime = dbTimeout * 1000;
            long elapsedTime = 0;
            synchronized(rsControlSelfCheck)
            {
                while(elapsedTime < waitTime)
                {
                    elapsedTime = System.currentTimeMillis() - startWait;
                    rsControlSelfCheck.wait(1000);
                    if (rsControlSelfCheck.isBusy())
                    {
                        Log.log.trace("DB Heartbeat Still Waiting");
                    }
                    else
                    {
                        Log.log.trace("DB Heartbeat Finished");
                        break;
                    }
                }
            }
            if (elapsedTime > waitTime)
            {
                Log.alert(ERR.E20005, "DB Heartbeat Failed to Complete in " + waitTime + " seconds");
            }
        }
        catch(Exception e)
        {
            Log.alert(ERR.E20005, e);
        }
    }
    
    public void pingRsRemote()
    {
    	rsControlSelfCheck.setPingThreshold(pingThreshold);
    	rsControlSelfCheck.pingRsRemote();
    }
    
    public void executeRunbook()
    {
        rsControlSelfCheck.executeRunbook();
    }
    
    public void checkESHealth()
    {
        rsControlSelfCheck.checkESHealth();
    }
    
//    public void checkThreadPool()
//    {
//        rsControlSelfCheck.checkThreadPool();
//    }
}
