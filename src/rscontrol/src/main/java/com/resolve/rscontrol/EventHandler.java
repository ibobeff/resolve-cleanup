/******************************************************************************
* (C) Copyright 2014
*
* Resolve Systems, LLC
*
* All rights reserved
* This software is distributed under license and may not be used, copied,
* modified, or distributed without the express written permission of
* Resolve Systems, LLC.
*
******************************************************************************/

package com.resolve.rscontrol;

import groovy.lang.Binding;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.TimeUnit;

import org.apache.commons.collections.CollectionUtils;

import com.resolve.esb.MMsgContent;
import com.resolve.esb.MMsgOptions;
import com.resolve.rsbase.MainBase;
import com.resolve.groovy.script.GroovyScript;
import com.resolve.rscontrol.ActionTaskUtils;
import com.resolve.rscontrol.Main;
import com.resolve.search.model.ExecuteState;
import com.resolve.services.ServiceHibernate;
import com.resolve.services.ServiceWorksheet;
import com.resolve.services.hibernate.vo.ResolveEventHandlerVO;
import com.resolve.util.Constants;
import com.resolve.util.Log;
import com.resolve.util.StringUtils;
import com.resolve.thread.TaskExecutor;
import com.resolve.thread.ScheduledExecutor;

public class EventHandler
{
    static String eventActionId;
    static Map<String,Map<String,List<String>>> processEventMap = new ConcurrentHashMap<String,Map<String,List<String>>>();
    
    public static Map<String,List<String>> newEventMap(String processid)
    {
        Map<String,List<String>> eventMap = new ConcurrentHashMap<String,List<String>>();
        
        processEventMap.put(processid, eventMap);
        return eventMap;
    } // newEventMap

    public static Map<String,List<String>> getEventMap(String processid)
    {
        return processEventMap.get(processid);
    } // getEventMap
    
    public static void removeEventMap(String processid)
    {
        processEventMap.remove(processid);
    } // removeEventMap

    public static void runbookEventHandler(Map params)
    {
        String wiki = (String) params.get(Constants.EXECUTE_WIKI);
        if (wiki != null)
        {
            if (!params.containsKey("USERID") && !params.containsKey("userid"))
            {
                params.put(Constants.EXECUTE_USERID, "system");
            }

            // init summary
            if (!params.containsKey("SUMMARY") && !params.containsKey("Summary") && !params.containsKey("summary"))
            {
                params.put("SUMMARY", "Runbook: " + wiki);
            }

            // init description
            if (!params.containsKey("DESCRIPTION") && !params.containsKey("Description") && !params.containsKey("description"))
            {
                params.put("DESCRIPTION", "Runbook: " + wiki);
            }

            // create new problem case
            if (!params.containsKey(Constants.EXECUTE_PROBLEMID))
            {
                params.put(Constants.EXECUTE_PROBLEMID, "NEW");
            }

            Main.getESB().sendInternalMessage(Constants.ESB_NAME_EXECUTEQUEUE, "MAction.executeProcess", params);
            // execute runbook
            //MAction.executeProcess(params);
        }
    } // runbookEventHandler

    public static void genericEventHandler(String type, Map params, Map result) throws Exception
    {
        // set WIKI in RESULT map of the event handler to execute wiki process

        // set TABLENAME in HEADER if record is transform and mapped
        if (type.equalsIgnoreCase("MAP"))
        {
            // set WIKI in content to execute wiki process
            result.putAll(params);
        }

        // custom handlers
        else
        {
            // get MAIN handler for type
            ResolveEventHandlerVO eventHandler = getGenericHandler(Constants.EVENT_HANDLER_MAIN, type);
            if (eventHandler == null)
            {
                Log.log.warn("Missing MAIN handler for type: " + type);
            }
            else
            {
                // execute MAIN for type
                String handlerName = (String) executeGenericHandler(eventHandler, params, result);
                if (StringUtils.isEmpty(handlerName))
                {
                    handlerName = "DEFAULT";
                }
                handlerName = handlerName.toUpperCase();
                if (Log.log.isDebugEnabled()) {
                    Log.log.debug("Specific EventHandler: " + handlerName);
                }

                // execute SPECIFIC
                if (!StringUtils.isEmpty(handlerName) && !handlerName.equalsIgnoreCase("MAIN"))
                {
                    // get SPECIFIC handler
                    eventHandler = getGenericHandler(handlerName, type);
                    if (eventHandler == null)
                    {
                        Log.log.warn("Missing SPECIFIC handler: " + handlerName + " for type: " + type + ". Using DEFAULT");

                        // get DEFAULT handler
                        handlerName = "DEFAULT";
                        eventHandler = getGenericHandler(handlerName, type);
                        if (eventHandler == null)
                        {
                            throw new Exception("Missing DEFAULT handler for type: " + type);
                        }
                    }

                    // execute SPECIFIC handler
                    executeGenericHandler(eventHandler, params, result);

                    if (Log.log.isDebugEnabled()) {
                        Log.log.debug("EventHandler content: " + result);
                    }
                }
            }
        }
    } // genericEventHandler

    static ResolveEventHandlerVO getGenericHandler(String name, String type)
    {
        return ServiceHibernate.getResolveEventHandler(name, type);
    } // getGenericHandler

    static Object executeGenericHandler(ResolveEventHandlerVO eventHandler, Map raw, Map result)
    {
        Object handlerName = null;

        try
        {
            HashMap params = new HashMap(raw);

            // convert params to CAPS keys
            for (Iterator i = raw.entrySet().iterator(); i.hasNext();)
            {
                Map.Entry entry = (Map.Entry) i.next();
                String key = (String) entry.getKey();

                params.put(key.toUpperCase(), entry.getValue());
            }

            // execute assessment
            String script = eventHandler.getUScript();
            if (script != null)
            {
                String scriptName = eventHandler.getUName();
                
                Binding binding = new Binding();
                binding.setVariable(Constants.GROOVY_BINDING_PARAMS, params);
                binding.setVariable(Constants.GROOVY_BINDING_RESULT, result);
                binding.setVariable(Constants.GROOVY_BINDING_LOG, Log.log);

                GroovyScript.execute(script, scriptName, true, binding);
            }
        }
        catch (Throwable t)
        {
            Log.log.warn("ERROR: EventHandler exception in " + eventHandler.getUName() + ". " + t.getMessage());
            if (Log.log.isDebugEnabled()) {
                Log.log.debug("ERROR: EventHandler exception in " + eventHandler.getUName() + ". " + t.getMessage(), t);
            }
        }

        return handlerName;
    } // executeGenericHandler

    public static boolean eventEventHandler(Map params) throws Exception
    {
        
        boolean result = false;

        // if value is null, set to ""
        for (Iterator i = params.entrySet().iterator(); i.hasNext();)
        {
            Map.Entry entry = (Map.Entry) i.next();

            if (entry.getValue() == null)
            {
                params.put(entry.getKey(), "");
            }
        }

        // eventid and processid must always be defined 
        final String eventid = (String) params.get(Constants.EXECUTE_EVENTID);
        String processid = (String) params.get(Constants.EXECUTE_PROCESSID);
        String problemid = (String) params.get(Constants.EXECUTE_PROBLEMID);
        
        // get eventMap
        Map<String,List<String>> eventMap = EventHandler.getEventMap(processid);
        if (eventMap == null)
        {
            throw new Exception("Missing eventMap for processid: "+processid);
        }
        
        // set executeid from eventMap
        List<String> executeids = eventMap.get(eventid);
        if (executeids==null || executeids.isEmpty())
        {
            Set<String> keys = eventMap.keySet();
            for (String key : keys)
            {
                if (ProcessRequest.isMatchingEvent(key, eventid, processid, problemid))
                {
                    executeids = eventMap.get(key);
                }
            }
            if (executeids==null || executeids.isEmpty())
            {
                throw new Exception("Missing eventid from eventMap: "+eventMap);
            }
        }
        params.put(Constants.EXECUTE_EXECUTEID, executeids);
        
        // trigger event in a separate thread
        TaskExecutor.execute(EventHandler.class, "triggerEvent", params);

        return result;
    } // eventEventHandler

    public static void triggerEvent(Map params)
    {
        if (Log.log.isDebugEnabled()) {
            Log.log.debug("triggerEvent params: " + params);
        }
        String eventid = (String) params.get(Constants.EVENT_EVENTID);
        String event_executeid = (String) params.get(Constants.EVENT_EXECUTEID);
        String event_reference = (String) params.get(Constants.EVENT_REFERENCE);
        String problemid = (String) params.get(Constants.EXECUTE_PROBLEMID);
        String processid = (String) params.get(Constants.EXECUTE_PROCESSID);
//        String targetAddr = (String) params.get(Constants.EXECUTE_TARGET);
        String targetAddr = (String) params.get(Constants.EXECUTE_ADDRESS);

        
        String userid = (String) params.get(Constants.EXECUTE_USERID);
        
        // executeid of the event MUST be defined
        List<String> executeids = (List<String>) params.get(Constants.EXECUTE_EXECUTEID);
        String executeid = null;
        
        // init event end
        boolean event_end = true;
        if (params.containsKey(Constants.EVENT_END))
        {
            event_end = StringUtils.getBoolean(Constants.EVENT_END, params);
        }

        
        // get persisted values from database
        try
        {
            if (Log.log.isDebugEnabled()) {
                Log.log.debug("Restoring state EVENTID: " + eventid);
                Log.log.debug("  PROCESSID: " + processid);
                Log.log.debug("  EVENT_REFERENCE: " + event_reference);
            }
            
            List<ExecuteState> rows = ServiceWorksheet.getEventRowsFor(event_end, processid, problemid, eventid);
            
            if (CollectionUtils.isNotEmpty(rows))
            {
                ExecuteState row = null;
                boolean isBreak = false;
                for (ExecuteState es: rows)
                {
                    if (isBreak)
                    {
                        break;
                    }
                    for (String exeId : executeids)
                    {
                        if (es.getExecuteId()==null)
                        {
                           row = es;
                           executeid = executeids.get(0);
                           isBreak = true;
                        }
                        else if (es.getExecuteId().equalsIgnoreCase(exeId))
                        {
                            row = es;
                            executeid = es.getExecuteId();
                        }
                    }
                }
                
                if (executeid!=null)
                {
                    List<String> deleteIds = new ArrayList<String>();
                    deleteIds.add(row.getSysId());
                    ServiceWorksheet.deleteExecuteStateRows(deleteIds);
                }

                if (row == null) {
                	Log.log.warn(String.format("Unable to find ResolveExecuteState for processid: %s eventid: %s " +
                							   "problemid: %s params: %s, skipping triggering of event!!!", 
 						   					   processid, eventid, problemid, StringUtils.mapToString(params, "=", ", ")));
                	return;
                }
                
                String UParams = row.getParams();

                String wiki = null;
                String reference = null;
                String timeout = null;
                String processTimeout = null;
                String metricId = null;
                Map persistedParams = null;
                
                // params
                if (UParams != null)
                {
                    persistedParams = (Map) StringUtils.stringToObj(UParams);
                    if (persistedParams != null)
                    {
                        wiki = (String) persistedParams.get(Constants.EXECUTE_WIKI);
                        reference = (String) persistedParams.get(Constants.EXECUTE_REFERENCE);
                        timeout = (String) persistedParams.get(Constants.EXECUTE_TIMEOUT);
                        if(persistedParams.get(Constants.EXECUTE_PROCESS_TIMEOUT) instanceof String) {
                            processTimeout = (String) persistedParams.get(Constants.EXECUTE_PROCESS_TIMEOUT);
                        } else if(persistedParams.get(Constants.EXECUTE_PROCESS_TIMEOUT) instanceof Integer) {
                            processTimeout = ""+persistedParams.get(Constants.EXECUTE_PROCESS_TIMEOUT);
                        }else {
                            processTimeout = "10";
                        }
                        metricId = (String) persistedParams.get(Constants.EXECUTE_METRICID);

                        targetAddr = (String) params.get(Constants.EXECUTE_ADDRESS);
                        
                        // if passed target address is empty, we will use persisted target address
                        if (StringUtils.isEmpty(targetAddr))
                        {
                            targetAddr = (String) persistedParams.get(Constants.EXECUTE_ADDRESS);
                            params.put(Constants.EXECUTE_ADDRESS, targetAddr);
                        }

                        if (StringUtils.isEmpty(userid))
                        {
                            userid = (String) persistedParams.get(Constants.EXECUTE_USERID);
                        }

                        persistedParams.putAll(params);
                    }
                    else
                    {
                        persistedParams = params;
                    }
                }

                // flows
                Map flows = null;
                String UFlows = row.getFlows(); 
                if (UFlows != null)
                {
                    flows = (Map) StringUtils.stringToObj(UFlows);
                    if (flows == null)
                    {
                        flows = new HashMap();
                    }
                }

                // inputs
                Map inputs = null;
                String UInputs = row.getInputs();
                if (UInputs != null)
                {
                    inputs = (Map) StringUtils.stringToObj(UInputs);
                    if (inputs == null)
                    {
                        inputs = new HashMap();
                    }
                }

                // debug
                String debugStr = row.getDebug(); 
                
                // starttime
                long processStartTime = row.getProcessStartTime();
                
                // duration
                String duration = (String) params.get(Constants.EVENT_DURATION);
                if (StringUtils.isEmpty(duration))
                {
                    //Date startTime = GMTDate.getLocalServerDate(state.getSysUpdatedOn());
                    long startTime = row.getSysUpdatedOn();
                    duration = "" + (System.currentTimeMillis() - startTime) / 1000;
                    persistedParams.put(Constants.EVENT_DURATION, duration);
                }

                // send message
                sendMessage(userid, problemid, processid, executeid, wiki, reference, timeout, processTimeout, metricId, 
                			targetAddr, persistedParams, flows, inputs, debugStr);
            }
            else
            {
		        Log.log.warn(String.format("Unable to find ResolveExecuteState for processid: %s executeid: %s params: %s", 
		        						   processid, executeid, StringUtils.mapToString(params, "=", ", ")));
                Log.log.warn("  EVENTMAP: "+EventHandler.getEventMap(processid));
            }
        }
        catch (Throwable e)
        {
            Log.log.error(e.getMessage(), e);
            throw new RuntimeException(e);
        }
    } // triggerEvent

    static void sendMessage(String userid, String problemid, String processid, String executeid, String wiki, String reference, String taskTimeout, String processTimeout, String metricId, String targetAddr, Map params, Map flows, Map inputs, String debugStr)
    {
        if (eventActionId == null)
        {
            eventActionId = ActionTaskUtils.getActionIdFromFullname("event#resolve");
        }
        String eventid = (String) params.get(Constants.EXECUTE_EVENTID);

        String completed = (String) params.get(Constants.EVENT_COMPLETED);
        if (StringUtils.isEmpty(completed))
        {
            completed = "true";
        }

        String condition = (String) params.get(Constants.EVENT_CONDITION);
        if (StringUtils.isEmpty(condition))
        {
            condition = "good";
        }

        String severity = (String) params.get(Constants.EVENT_SEVERITY);
        if (StringUtils.isEmpty(severity))
        {
            severity = "good";
        }

        String summary = "Event - eventid: " + eventid;
        
        String detail = (String) params.get(Constants.EVENT_DETAIL);
        if (StringUtils.isEmpty(detail))
        {
            detail = summary;
        }

        String duration = (String) params.get(Constants.EVENT_DURATION);
        if (StringUtils.isEmpty(duration))
        {
            duration = "0";
        }
        
        String returnCode = (String) params.get(Constants.EVENT_RETURNCODE);
        if (StringUtils.isEmpty(returnCode))
        {
            returnCode = "0";
        }

        MMsgOptions options = new MMsgOptions();
        options.setRequestTimestamp(System.currentTimeMillis());
        options.setUserID(userid);
        options.setActionID(eventActionId);
        options.setActionName("event");
        options.setProblemID(problemid);
        options.setProcessID(processid);
        options.setExecuteID(executeid);
        options.setWiki(wiki);
        options.setReference(reference);
        options.setAssessID(null);
        options.setParserID(null);
        options.setLogResult("false");
        options.setReturnCode(returnCode);
        options.setCompletion("true");
        options.setTimeout(taskTimeout);
        options.setProcessTimeout(processTimeout);
        options.setMetricId(metricId);
        options.setCommand("");
        options.setAddress(targetAddr);
        options.setTarget(Constants.ESB_NAME_RSCONTROL);

        String raw = "";
        raw += "COMPLETED=" + completed + "\n";
        raw += "CONDITION=" + condition + "\n";
        raw += "SEVERITY=" + severity + "\n";
        raw += "SUMMARY=" + summary + "\n";
        raw += "DETAIL=" + detail + "\n";
        raw += "DURATION=" + duration + "\n";

        // init content
        MMsgContent content = new MMsgContent();
        
        params.remove(Constants.EVENT_EVENTABORT);
        
        content.setParams(StringUtils.objToString(params));
        content.setFlows(StringUtils.objToString(flows));
        content.setInputs(StringUtils.objToString(inputs));
        content.setRaw(raw);
        content.setDebug(debugStr);

//        Main.esb.sendMessage(Constants.ESB_NAME_RSSERVER, "MResult.actionResult", options, content);
//        MResult.actionResult(options, content, true);
        Main.esb.sendMessage(MainBase.main.configId.getGuid(), "MResult.actionResult", options, content);
      
    } // sendMessage

} // EventHandler
