package com.resolve.rscontrol;

import java.lang.management.ManagementFactory;
import java.lang.management.ThreadMXBean;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.GregorianCalendar;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.elasticsearch.action.admin.cluster.health.ClusterHealthResponse;
import org.elasticsearch.cluster.health.ClusterHealthStatus;
import org.elasticsearch.client.Requests;
import org.elasticsearch.common.Priority;

import com.resolve.search.SearchAdminAPI;
import com.resolve.search.model.Worksheet;
import com.resolve.services.util.WorksheetUtil;
import com.resolve.sql.SQL;
import com.resolve.sql.SQLConnection;
import com.resolve.util.ERR;
import com.resolve.util.GMTDate;
import com.resolve.util.Log;
import com.resolve.util.StringUtils;

public class RSControlSelfCheck implements Runnable
{
    private static RSControlSelfCheck instance;
    private int timeout;
    private String dbType;
    static int esbTimeout = 600000; //how many miliseconds without a heartbeat message before throwing an error
    static long lastESBHeartbeat = System.currentTimeMillis();
    private int esSCRetryCount;
    private int esSCRetryDelay;
    private String rbExecRunbook; 
    
    private static final String selfCheckValue = "RSCONTROL DB SELFCHECK";
    private static double tps_new;
    private static boolean busy;
    private static final int thresholdTps = 50;
    private int pingThreshold;
    
    private RSControlSelfCheck()
    {
    }
    
    public static RSControlSelfCheck getInstance()
    {
        if (instance == null)
        {
            instance = new RSControlSelfCheck();
        }
        
        return instance;
    }
    
    public void run()
    {
        busy = true;
        SQLConnection conn = null;
        try
        {
            conn = SQL.getConnection();
                
            String sql = "update resolve_event set sys_updated_on=? where u_value='" + selfCheckValue + "'";
            Log.log.trace("DB Self Check update sql: " + sql);
            
            PreparedStatement ps = conn.prepareStatement(sql);
            Timestamp updatedDate = new Timestamp(GMTDate.getTime());
            
            ps.setTimestamp(1, updatedDate);
            
            Log.start("Begin Update Execute", "debug");
            int update = ps.executeUpdate();
            Log.duration("Finish Update Execute", "debug");
            Log.log.trace("DB Self Check Run, " + update + " row(s) updated.");
            
        }
        catch (Throwable t)
        {
            Log.alert(ERR.E20005, t);
        }
        finally
        {
            if (conn != null)
            {
                SQL.close(conn);
            }
            busy = false;
            synchronized(this)
            {
                notifyAll();
            }
        }
    }

    public int getTimeout()
    {
        return timeout;
    }

    public void setTimeout(int timeout)
    {
        this.timeout = timeout;
    }

    public String getDbType()
    {
        return dbType;
    }

    public void setDbType(String dbType)
    {
        this.dbType = dbType;
    }

    public boolean isBusy()
    {
        return busy;
    }
    
    public void checkESHealth()
    {
        boolean result = false;
        for (int i = 0; i < getEsSCRetryCount(); ++i)
        {
            try
            {
                Log.log.trace("Checking RSSearch status. Count: " + i);
                ClusterHealthResponse actionGet = SearchAdminAPI.getClient().admin().cluster().health(Requests.clusterHealthRequest().waitForYellowStatus().waitForEvents(Priority.LANGUID).waitForActiveShards(0)).actionGet();
                if (actionGet.isTimedOut())
                {
                    Log.log.trace("ensureYellow timed out, cluster state: " + SearchAdminAPI.getClient().admin().cluster().prepareState().get().getState() + " \n " + SearchAdminAPI.getClient().admin().cluster().preparePendingClusterTasks().get());
                    Thread.sleep(getEsSCRetryDelay() * 1000);
                }
                else if(ClusterHealthStatus.RED.equals(actionGet.getStatus()))
                {
                    Log.log.trace("RSSearch cluster status is RED, waiting to be yellow.");
                    Thread.sleep(getEsSCRetryDelay() * 1000);
                }
                else
                {
                    result = actionGet.getStatus().equals(ClusterHealthStatus.GREEN) || actionGet.getStatus().equals(ClusterHealthStatus.YELLOW);
                    Log.log.trace("RSSearch cluster check is complete, status is: " + actionGet.getStatus());
                    break;
                }
            }
            catch(Exception e)
            {
                Log.log.warn(e.getMessage(), e);
            }
        }
        
        if (result == false)
        {
            String message = "RSCONTROL ES Self Check: ES status is red after " + getEsSCRetryCount() + " attempts with delay of " + getEsSCRetryDelay() + " secs.";
            Exception t = new Exception(message);
            Log.log.fatal(t.getMessage(), t);
        }
    }
    
    public void executeRunbook()
    {
        if (StringUtils.isNotBlank(rbExecRunbook))
        {
            // Runbook execution Self Check
            Map<String, String> params = new HashMap<String, String>();
            params.put("WIKI", rbExecRunbook);
            params.put("PROBLEMID", "NEW");
            params.put("PROCESS_DEBUG", "false");
            params.put("ACTION", "EXECUTEPROCESS");
            params.put("USERID", "admin");
            MAction.executeProcess(params);
            boolean success = false;
            Log.log.trace("Self Check: Runbook execution request is made.");
            if (params.get("PROBLEMID") != null && !params.get("PROBLEMID").equals("NEW"))
            {
                try
                {
                    Worksheet worksheet = null;
                    boolean result = false;
                    for (int i = 0; i<20; i++)
                    {
                        Thread.sleep(1000);
                        worksheet = WorksheetUtil.findWorksheetById(params.get("PROBLEMID"), "admin");
                        if (StringUtils.isNotBlank(worksheet.getCondition()))
                        {
                            result = true;
                            break;
                        }
                    }
                    
                    if (result)
                    {
                        if (worksheet.getCondition().equals("GOOD") && worksheet.getSeverity().equals("GOOD"))
                        {
                            success = true;
                        }
                    }
                    else
                    {
                        Log.alert(ERR.E30003);
                    }
                }
                catch (InterruptedException e)
                {
                    Log.log.error(e.getMessage(), e);
                    Log.alert(ERR.E30003);
                }
            }
            
            if (success == false)
            {
                Exception e = new Exception("RSCONTROL Self Check Runbook Execution failed.");
                Log.log.fatal("RSCONTROL Self Check Runbook Execution failed.", e);
            }
            
            // Self Check to schedule a cron job and execute a Runbook
            GregorianCalendar calendar = new  GregorianCalendar();
            calendar.add(Calendar.SECOND, 5);
            String schedule = calendar.get(Calendar.SECOND) + " " + calendar.get(Calendar.MINUTE) + " " + calendar.get(Calendar.HOUR_OF_DAY) + " * * ?";
            MCron.scheduleTask("SELF CHECK RUNBOOK EXECUTION", rbExecRunbook, schedule, "system", true, true);
            Log.log.trace("Self Check: Cron job is scheduled to execute a Runbook after 5 seconds.");
            try
            {
                Thread.sleep(10000);
                Worksheet worksheet = null;
                boolean result = false;
                for (int i = 0; i<20; i++)
                {
                    worksheet = WorksheetUtil.findWorksheet(null, null, "Cron: SELF CHECK RUNBOOK EXECUTION", null, null, null, null, params.get("USERID"));
                    if (com.resolve.util.StringUtils.isNotBlank(worksheet.getCondition()))
                    {
                        result = true;
                        break;
                    }
                    Thread.sleep(1000);
                }
                
                if (result)
                {
                    if (!worksheet.getCondition().equals("GOOD") && !worksheet.getSeverity().equals("GOOD"))
                    {
                        Exception e = new Exception("RSCONTROL Cron Self Check Runbook Execution failed.");
                        Log.log.fatal("RSCONTROL Self Check Cron Runbook Execution failed.", e);
                    }
                }
                else
                {
                    Log.alert(ERR.E30002);
                }
                   
            }
            catch (Exception e)
            {
                Log.log.error(e.getMessage(), e);
                Log.alert(ERR.E30002);
            }
        }
        else
        {
            Log.log.error("ERROR: RSCONTROL Self Check: No runbook specified to execute.");
        }
    }
    
    @SuppressWarnings("unchecked")
	public void pingRsRemote()
    {
    	List<String> guids = getDeployedRSRemoteGUID();
    	if (guids.size() > 0)
    	{
    		for (String guid : guids)
        	{
    			Map<String, String> response = null;
        		try
    			{
        			Log.log.trace("Sending ESB ping to RSREMOTE-" + guid);
        			response = Main.esb.call(guid, "MAction.esbPing", new HashMap<String, String>(), pingThreshold*1000);
        			if (response == null)
        			{
        				Log.log.error("ESB Ping has not been received from RSREMOTE-" + guid + " since the last ping for " + pingThreshold + " seconds. MQ could be too busy or component may not be running.");
        				Log.alert(ERR.E20010);
        			}
        			else
        			{
        				Log.log.trace("Received ESB ping from RSREMOTE-" + guid);
        			}
    			}
        		catch (Exception e)
    			{
        			Log.log.error(e.getMessage(), e);
    			}
        	}
    	}
    }
    
    private List<String> getDeployedRSRemoteGUID()
    {
    	List<String> rsRemoteHuidList = new ArrayList<String>();
    	SQLConnection conn = null;
        try
        {
            conn = SQL.getConnection();
            
            String sql = "select u_guid from resolve_registration where u_type=? and u_status = 'ACTIVE'";
            
            PreparedStatement ps = conn.prepareStatement(sql);
            
            ps.setString(1, "RSREMOTE");
            
            ResultSet rs = ps.executeQuery();
            
            if (rs != null)
            {
            	while(rs.next())
            	{
            		String guid = rs.getString("u_guid");
            		if (StringUtils.isNotBlank(guid))
            		{
            			rsRemoteHuidList.add(guid);
            		}
            	}
            }
            
            
        }
        catch (Throwable t)
        {
            Log.log.error("Could not get deployed RSRemotes from DB.", t);
        }
        finally
        {
            if (conn != null)
            {
                SQL.close(conn);
            }
        }
        
        return rsRemoteHuidList;
    }

    public int getEsSCRetryCount()
    {
        return esSCRetryCount;
    }

    public void setEsSCRetryCount(int esSCRetryCount)
    {
        this.esSCRetryCount = esSCRetryCount;
    }

    public int getEsSCRetryDelay()
    {
        return esSCRetryDelay;
    }

    public void setEsSCRetryDelay(int esSCRetryDelay)
    {
        this.esSCRetryDelay = esSCRetryDelay;
    }

    public String getRbExecRunbook()
    {
        return rbExecRunbook;
    }

    public void setRbExecRunbook(String rbExecRunbook)
    {
        this.rbExecRunbook = rbExecRunbook;
    }

	public int getPingThreshold()
	{
		return pingThreshold;
	}

	public void setPingThreshold(int pingThreshold)
	{
		this.pingThreshold = pingThreshold;
	}
}
