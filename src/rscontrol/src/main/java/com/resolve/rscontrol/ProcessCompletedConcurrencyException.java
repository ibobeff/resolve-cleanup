/******************************************************************************
* (C) Copyright 2014
*
* Resolve Systems, LLC
*
* All rights reserved
* This software is distributed under license and may not be used, copied,
* modified, or distributed without the express written permission of
* Resolve Systems, LLC.
*
******************************************************************************/

package com.resolve.rscontrol;

class ProcessCompletedConcurrencyException extends Exception
{
    private static final long serialVersionUID = -5379874649384398616L;

	public ProcessCompletedConcurrencyException(String msg, Exception e)
    {
        super(msg, e);
    }
    
    public ProcessCompletedConcurrencyException(String msg)
    {
        super(msg);
    }
    
} // ProcessCompletedConcurrencyException
