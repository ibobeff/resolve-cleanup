/******************************************************************************
* (C) Copyright 2014
*
* Resolve Systems, LLC
*
* All rights reserved
* This software is distributed under license and may not be used, copied,
* modified, or distributed without the express written permission of
* Resolve Systems, LLC.
*
******************************************************************************/

package com.resolve.rscontrol;

import java.util.HashMap;
import java.util.List;
import java.util.ArrayList;
import java.util.Map;
import java.io.Serializable;

import com.resolve.util.DirectedTree;

import com.resolve.util.Constants;

public class ActionProcessDependency implements Serializable
{
    private static final long serialVersionUID = 4857731090970838674L;
	List<ActionProcessTask> tasks;
    DirectedTree model;
    Map<String,ActionProcessTask> id2task = new HashMap<String, ActionProcessTask>();
    
    public ActionProcessDependency()
    {
        // used by both process type WIKI and MODEL
        tasks = new ArrayList<ActionProcessTask>();
        this.model = new DirectedTree();
    } // ActionProcessDependency
    
    public List<ActionProcessTask> getTasks()
    {
        return tasks;
    }
    public void setTasks(List<ActionProcessTask> tasks)
    {
        this.tasks = tasks;
    }
    public DirectedTree getModel()
    {
        return model;
    }
    public void setModel(DirectedTree model)
    {
        this.model = model;
    }

} // ActionProcessDependency
