/******************************************************************************
* (C) Copyright 2014
*
* Resolve Systems, LLC
*
* All rights reserved
* This software is distributed under license and may not be used, copied,
* modified, or distributed without the express written permission of
* Resolve Systems, LLC.
*
******************************************************************************/

package com.resolve.rscontrol;

public class Counter
{
    int count = 0;
    
    public Counter(int t)
    {
        count = t;
    }
    
    public Counter()
    {
        count = 0;
    }
    
    public synchronized int increment()
    {
        return ++count;
    }
    
    public synchronized int decrement()
    {
        return --count;
    }
    
    public synchronized int get()
    {
        return count;
    }
    
    public synchronized int set(int t)
    {
        int old = count;
        count=t;
        return old;
    }
    
    public String toString() 
    {
       return ""+count;
    }
}


