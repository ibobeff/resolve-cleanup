/******************************************************************************
* (C) Copyright 2014
*
* Resolve Systems, LLC
*
* All rights reserved
* This software is distributed under license and may not be used, copied,
* modified, or distributed without the express written permission of
* Resolve Systems, LLC.
*
******************************************************************************/

package com.resolve.rscontrol;

import java.io.IOException;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.util.HashMap;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import com.resolve.esb.MMsgHeader;
import com.resolve.rsbase.MainBase;
import com.resolve.notification.NotificationAPI;
import com.resolve.persistence.model.ResolveBlueprint;
import com.resolve.persistence.util.CacheUtil;
import com.resolve.persistence.util.DBCacheRegionConstants;
import com.resolve.persistence.util.HibernateUtil;
import com.resolve.services.ServiceHibernate;
import com.resolve.services.hibernate.menu.MenuCache;
import com.resolve.services.hibernate.menu.MenuDefinitionUtil;
import com.resolve.sql.SQL;
import com.resolve.sql.SQLConnection;
import com.resolve.util.Constants;
import com.resolve.util.CryptUtils;
import com.resolve.util.GMTDate;
import com.resolve.util.Log;
import com.resolve.util.Properties;
import com.resolve.util.StringUtils;
import com.resolve.util.SysId;

public class MConfig
{
    public static Map<String, Boolean> rsmgmts = new HashMap<String, Boolean>();

    @SuppressWarnings({ "rawtypes", "unchecked", "static-access" })
    public static Map isMaster(Map params)
    {
        Map result = new HashMap(params);

        if (((Main)MainBase.main).isMaster == true)
        {
            result.put("NAME", MainBase.main.configId.name);
            result.put("DESCRIPTION", MainBase.main.configId.description);
            result.put("TYPE", MainBase.main.release.type);
            result.put("GUID", MainBase.main.configId.guid);
            result.put("NODEID", MainBase.main.configId.nodeid);
            result.put("VERSION", MainBase.main.release.version);
            result.put("CONFIGVERSION", MainBase.main.configGeneral.configFileVersion);
            result.put("IPADDRESS", ""+MainBase.main.configId.ipaddress);
            result.put("ESBUSER", MainBase.main.configESB.config.getUsername());
        }
        return result;
    } // isMaster

    @SuppressWarnings("rawtypes")
    public void setTimezone(Map params)
    {
        String timezone = (String)params.get("TIMEZONE");
        GMTDate.initTimezone(timezone);
    } // setTimezone

    @SuppressWarnings("rawtypes")
    public static void updateProperties(MMsgHeader header, Map properties)
    {
        if (header != null && properties != null)
        {
	        try
	        {

		        String guid = header.getSource().getAddress();
		        updateProperties(guid, properties);

	        }
	        catch (Exception e)
	        {
	        	Log.log.error(e.getMessage(), e);
	        }
        }
    } // updateProperties

    @SuppressWarnings({ "rawtypes", "unchecked" })
    public static void updateProperties(String guid, Map properties) throws Exception
    {
        ServiceHibernate.updateRegistrationProperties(guid, properties);
    } // updateProperties

    public static Map<String, String> storeBlueprint(Map params)
    {
        Map<String, String> result = new HashMap<String, String>();
        
        SQLConnection conn = null;
        try
        {
            if(params.containsKey("BLUEPRINT"))
            {
                String guid = (String) params.get("GUID");
                //send back the acknowledgement message back to the RSMGMT
                result.put(MainBase.main.configId.getGuid(), "Acknowledged by RSCONTROL " + MainBase.main.configId.getGuid());
                //MainBase.esb.sendInternalMessage(guid, "MRegister.acknowledge", null);
                //MainBase.esb.sendInternalMessage(guid, "MImpex.impexCustomTable", null);

                String blueprintString = (String) params.get("BLUEPRINT");
                Properties blueprintProperties = new Properties();
                blueprintProperties.load(StringUtils.toInputStream(blueprintString));

                for (Object key : blueprintProperties.keySet())
                {
                    String keyStr = (String) key;
                    String value = blueprintProperties.get(keyStr);
                    if (value.contains("${"))
                    {
                        value = blueprintProperties.getReplacement(key).trim();
                    }
                    if (keyStr.toUpperCase().contains("PASS") && !keyStr.toUpperCase().contains("LDAP.PASSWORD_ATTRIBUTE"))
                    {
                        if (StringUtils.isNotEmpty(value) && !value.startsWith("NO_ENC:") && !value.matches(".*(?<!\\\\)\\$\\{.*\\}.*"))
                        {
                            value = CryptUtils.encrypt(value);
                        }
                    }
                    if (value.startsWith("TO_ENC:"))
                    {
                        value = CryptUtils.encrypt(value.substring(7));
                    }
                    if (!value.contains("ENC"))
                    {
                        value = value.replaceAll("\\\\", "\\\\\\\\");
                    }
                    String regex = key + "=.*";
                    String replaceStr = key + "=" + value;
                    replaceStr = Matcher.quoteReplacement(replaceStr);
                    Log.log.trace("Storing " + key + " to " + value);
                    blueprintString = blueprintString.replaceFirst(regex, replaceStr);
                }

                conn = SQL.getConnection();
                Timestamp currentTime = new Timestamp(GMTDate.getTime());

                String sql = "select sys_id, sys_mod_count from resolve_blueprint where u_guid='" +  guid + "'";

                ResultSet rs = conn.executeQuery(sql);
                if (rs.next())
                {
                    String sysId = rs.getString("sys_id");
                    int modCount = rs.getInt("sys_mod_count");
                    sql = "update resolve_blueprint set u_localhost=?, u_blueprint=?, sys_updated_on=?, sys_updated_by='system'," +
                            " sys_mod_count=? where sys_id=?";

                    PreparedStatement ps = conn.prepareStatement(sql);
                    ps.setString(1, blueprintProperties.get(Constants.BLUEPRINT_LOCALHOST));
                    ps.setString(2,blueprintString);
                    ps.setTimestamp(3, currentTime);
                    ps.setInt(4, ++modCount);
                    ps.setString(5, sysId);
                    Log.log.trace("Update SQL: " + sql);
                    Log.log.trace("Set Value: 1, " + blueprintProperties.get(Constants.BLUEPRINT_LOCALHOST));
                    Log.log.trace("Set Value: 2, " + blueprintString);
                    Log.log.trace("Set Value: 3, " + currentTime);
                    Log.log.trace("Set Value: 4, " + modCount);
                    Log.log.trace("Set Value: 5, " + sysId);

                    int update = ps.executeUpdate();
                    Log.log.trace("DB Blueprint Update Count: " + update);
                }
                else
                {
                    String sysId = SysId.generate(conn);
                    sql = "insert into resolve_blueprint (sys_id, sys_created_on, sys_created_by, sys_updated_on," +
                            " sys_updated_by, u_localhost, u_guid, u_blueprint, sys_mod_count) values (?, ?, 'system', ?," +
                            " 'system', ?, ?, ?, 0)";

                    PreparedStatement ps = conn.prepareStatement(sql);
                    ps.setString(1, sysId);
                    ps.setTimestamp(2, currentTime);
                    ps.setTimestamp(3, currentTime);
                    ps.setString(4, blueprintProperties.get(Constants.BLUEPRINT_LOCALHOST));
                    ps.setString(5, guid);
                    ps.setString(6,blueprintString);
                    Log.log.trace("Update SQL: " + sql);
                    Log.log.trace("Set Value 1, " + sysId);
                    Log.log.trace("Set Value 2, " + currentTime);
                    Log.log.trace("Set Value 3, " + currentTime);
                    Log.log.trace("Set Value 4, " + blueprintProperties.get(Constants.BLUEPRINT_LOCALHOST));
                    Log.log.trace("Set Value 5, " + guid);
                    Log.log.trace("Set Value 6," + blueprintString);

                    int update = ps.executeUpdate();
                    Log.log.trace("DB Blueprint Insert Count: " + update);
                }

                // once the blueprint is stored in the database, we'll make
                // sure that the gateway menus are
                // turned on/off based on the active flag in the blueprint.
                updateGatewayMenu(guid, conn);

                Log.log.debug("Sending clear DB Cache Region message for " + 
                			  DBCacheRegionConstants.RESOLVE_BLUEPRINT_CACHE_REGION.getValue());
                HibernateUtil.clearDBCacheRegion(DBCacheRegionConstants.RESOLVE_BLUEPRINT_CACHE_REGION, false);

                Map<String, String> msg = new HashMap<String, String>();
                msg.put(Constants.SOURCE_GUID, guid);
                msg.put(CacheUtil.CACHE_NAME, MenuCache.MENU_CACHE.toString());
                
                MainBase.esb.sendInternalMessage(Constants.ESB_NAME_RSVIEWS, "MNotification.invalidateMenuCache", msg);
                
                Log.log.info("Blueprint DB Save Complete");

                //Got to refresh notitifcation API targets so if there is new
                //gateway being activated the RSControl knows about it.
                Log.log.info("Initializing Notification API targets in RSControl");
                NotificationAPI.init();

                MainBase.esb.sendInternalMessage(Constants.ESB_NAME_RSVIEWS, "MNotification.init", params);
                Log.log.debug("Sent message to RSVIEWS to re-initialize NotificationAPI.targets");
            }
        }
        catch (Exception e)
        {
            result.put(MainBase.main.configId.getGuid(), "Failed to update blueprint by RSCONTROL " + MainBase.main.configId.getGuid());
            Log.log.error("Unable to Save blueprint.properties file to DB", e);
        }
        finally
        {
            if (conn != null)
            {
                SQL.close(conn);
            }
        }
        return result;
    }

    private static void updateGatewayMenu(String senderGuid, SQLConnection conn)
    {
        Log.log.debug("Starting to update gateway menus");
        //Log.log.debug("Blueprint sender GUID: " + senderGuid);
        
        String sql = "select sys_id, u_guid, u_blueprint from resolve_blueprint";

        //this is a map where we store some abnormality with the
        //blueprint key (e.g., rsremote.receive.db.active) not same as the
        //sys app module (e.g., database). Normally all other existing
        //gateways are fine except database.
        Map<String, String> exceptionMapping = new HashMap<String, String>();
        exceptionMapping.put("db", "database");

        Map<String, Boolean> map = new HashMap<String, Boolean>();

        try
        {
            ResultSet rs = conn.executeQuery(sql);
            while (rs.next())
            {
            	Log.log.debug("Current blueprint GUID: " + rs.getString("u_guid"));
                String blueprint = rs.getString("u_blueprint");
                Properties properties = new Properties();
                properties.load(StringUtils.toInputStream(blueprint));
                
                int rsRemoteInsnaces = Integer.valueOf(properties.get("rsremote.instance.count"));
                for (int i = 1; i<=rsRemoteInsnaces; i++)
                {
                	String instancename = properties.get("rsremote.instance" + i + ".name");
	                // collecting values
	                Pattern pattern = Pattern.compile(instancename + "\\.receive\\.(.*?)\\.active");
	                for (Object key : properties.keySet())
	                {
	                    Matcher matcher = pattern.matcher((String)key);
	                    if (matcher.matches())
	                    {
	                        String gateway = matcher.group(1);
	                        if (gateway != null && gateway.indexOf('.') < 0)
	                        {
	                            try
	                            {
	                                String value = properties.get((String)key);
	                                //Log.log.debug("Found:" + key + " with value:"+ value);
	
	                                boolean isActive = Boolean.valueOf(value);
	    
	                                //check the exceptional gateways
	                                if(exceptionMapping.containsKey(gateway))
	                                {
	                                    gateway = exceptionMapping.get(gateway);
	                                }
	                                
	                                if (map.containsKey(gateway))
	                                {
	                                	// If we find any gateway active, don't touch it.
	                                	if (!map.get(gateway))
	                                	{
	                                		map.put(gateway, isActive);
	                                	}
	                                }
	                                else
	                                {
	                                	map.put(gateway, isActive);
	                                }
	                            }
	                            catch (ArrayIndexOutOfBoundsException e)
	                            {
	                                // some bad key, ignoring
	                            }
	                        }
	                    }
	                }
                }
                
                //DatabaseConnectionPool follows db/database gateway Active/InActive state
                if (map.containsKey("database") && !map.containsKey("databaseconnectionpool"))
                {
                    map.put("databaseconnectionpool", map.get("database"));
                }
                
                if (map.containsKey("remedyx"))
                {
                    map.put("remedyxform", map.get("remedyx"));
                }
                
                if (map.containsKey("email"))
                {
                    map.put("emailaddress", map.get("email"));
                }
                
                if (map.containsKey("ews"))
                {
                    map.put("ewsaddress", map.get("ews"));
                }
                
                if (map.containsKey("telnet"))
                {
                    map.put("telnetpool", map.get("telnet"));
                }
                
                if (map.containsKey("ssh"))
                {
                    map.put("sshpool", map.get("ssh"));
                }
                
                if (map.containsKey("xmpp"))
                {
                    map.put("xmppaddress", map.get("xmpp"));
                }
                
                if (map.containsKey("email"))
                {
                    map.put("emailaddress", map.get("email"));
                }
            }

            // Now update the sys_app_module
            //get the sys id for "Gateway Administration" app
            sql = "select sys_id from sys_app_application where title='Gateway Administration'";
            ResultSet rsApp = conn.executeQuery(sql);
            String appSysId = null;
            if(rsApp.next())
            {
                appSysId = rsApp.getString("sys_id");
            }
            
            // RS.gateway.Main/gatewaytype=ews
            String updateStatement = "update sys_app_module set active=? where sys_id=?";
            PreparedStatement ps = conn.prepareStatement(updateStatement);
            
            Log.log.debug("Final gateway menu activation map: " + map);
            
            for (String gateway : map.keySet())
            {
                //Log.log.debug("Updating menu for gateway : " + gateway + " with active : " + map.get(gateway));
                // select the
                //sql = "select sys_id, active, query from sys_app_module where query like 'rs.gateway.netcoolmain%'";
                sql = "select sys_id, active, query from sys_app_module where application='" + appSysId + "' and (lower(query) like 'rs.gateway." + gateway + "main%' or lower(query) like 'rs.gateway.custom/name=" + gateway + "')";
                ResultSet rs1 = conn.executeQuery(sql);
                while (rs1.next())
                {
                    Boolean isActive = map.get(gateway);
                    String sysId = rs1.getString("sys_id");
                    ps.setBoolean(1, isActive);
                    ps.setString(2, sysId);
                    int count = ps.executeUpdate();
                    //Log.log.debug("Total " + count + " records updated for " + gateway + " gateway.");
                }
            }

            sql = "select sys_id, active, query from sys_app_module where application='" + appSysId + "' and active=1 and (lower(query) like 'rs.gateway.%main%' or lower(query) like 'rs.gateway.custom/name=%')";
            ResultSet rs2 = conn.executeQuery(sql);
            boolean active = false;
            
            if(rs2 != null && rs2.next())
            {
                Log.log.debug("Activating Gateway Administration menu section");
                updateStatement = "update sys_app_application set active=1 where sys_id = ?";
                active = true;
            }
            else
            {
                Log.log.debug("Deactivating Gateway Administration menu section");
                updateStatement = "update sys_app_application set active=0 where sys_id= ?";
            }
/* Must use Hibernate cache for future read.
            PreparedStatement ps2 = conn.prepareStatement(updateStatement);
            ps2.setString(1, appSysId);

            int count = ps2.executeUpdate();
            
            if(count > 0)
            {
                Log.log.debug("Gateway Administration menu updated successfully");
            }
*/
            //oracle needs explicit commit, MySQL is fine.
            if("oracle".equalsIgnoreCase(((Main)MainBase.main).configSQL.getDbtype()))
            {
                conn.commit();
            }
            
            MenuDefinitionUtil.updateActiveForMenuDefinition(appSysId, active);
            
            MenuCache.getInstance().invalidate();
        }
        catch (SQLException e)
        {
            Log.log.error(e.getMessage(), e);
        }
        catch (IOException e)
        {
            Log.log.error(e.getMessage(), e);
        }
        catch (Exception e)
        {
            Log.log.error(e.getMessage(), e);
        }
        finally
        {
            if(conn != null)
            {
                conn.close();
            }
        }
    }
    
    /**
     * Ask all the RSMGMTs to report their availability so we can clean up the blueprints
     * in resolve_blueprint table. This is required because in times some RSMGMT may go
     * down forever and we could have contaminated data.
     */
    public static void cleanBlueprint()
    {
        SQLConnection conn = null;
        try
        {
            conn = SQL.getConnection();

            String sql = "select sys_id, u_guid from resolve_blueprint";
            String deleteSql = "delete from resolve_blueprint where u_guid = ?";

            ResultSet rs = conn.executeQuery(sql);
            while (rs.next())
            {
                String guid = rs.getString("u_guid");

                //ask the RSMGMT if it's alive, if not then we'll assume
                //it's dead and delete the record
                Map<String, String> params = new HashMap<String, String>();
                //the GUID param is so RSMGMT send the message back to this
                params.put("GUID", Main.main.configId.getGuid());
                boolean isAlive = false;
                long wait = 0;
                //wait for only 90 seconds and then give up
                while(!rsmgmts.containsKey(guid) && wait < 90000)
                {
                    try
                    {
                        MainBase.esb.sendInternalMessage(guid, "MRegister.requestState", params);
                        Log.log.debug("Sent message to " + guid + " asking its state");
                        //wait for 5 seconds
                        Log.log.debug("Have not received status message from RSMGMT: " + guid + ", waiting for 10 seconds");
                        Thread.sleep(10000);
                        wait += 10000;
                    }
                    catch (InterruptedException e)
                    {
                        //ignore
                    }
                }
                if(rsmgmts.containsKey(guid))
                {
                    isAlive = rsmgmts.get(guid);
                }

                if(!isAlive)
                {
                    //the RSMGMT not alive, remove the blueprint from the resolve_blueprint table.
                    PreparedStatement ps = conn.prepareStatement(deleteSql);
                    ps.setString(1, guid);
                    int update = ps.executeUpdate();
                    Log.log.trace("Deleted record from resolve_blueprint, count: " + update);
                }

                //at the end remove the guid as we don't need it to be there anymore.
                rsmgmts.remove(guid);
            }

            //update the gateway menu
            updateGatewayMenu(null, conn);

            Log.log.debug("Sending clear DB Cache Region message for " + 
            			  DBCacheRegionConstants.RESOLVE_BLUEPRINT_CACHE_REGION.getValue());
            HibernateUtil.clearDBCacheRegion(DBCacheRegionConstants.RESOLVE_BLUEPRINT_CACHE_REGION, false);
        }
        catch (Exception e)
        {
            Log.log.error("Unable to Save blueprint.properties file to DB", e);
        }
        finally
        {
            if (conn != null)
            {
                SQL.close(conn);
            }
        }
    }

    /**
     * Ask all the RSMGMTs to report it's availability so we can clean up the blueprints
     * in resolve_blueprint table. This is required because in time some RSMGMT may go
     * down forever and we'll have contaminated data.
     */
    public static void receiveState(Map<String, Object> params)
    {
        if(params.containsKey("GUID"))
        {
            rsmgmts.put((String) params.get("GUID"), Boolean.TRUE);
        }
    }

} // MConfig
