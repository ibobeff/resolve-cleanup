/******************************************************************************
* (C) Copyright 2014
*
* Resolve Systems, LLC
*
* All rights reserved
* This software is distributed under license and may not be used, copied,
* modified, or distributed without the express written permission of
* Resolve Systems, LLC.
*
******************************************************************************/

package com.resolve.rscontrol;

import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.resolve.rsbase.MainBase;
import com.resolve.cron.CronAPI;
import com.resolve.cron.CronStore;
import com.resolve.cron.CronTask;
import com.resolve.services.ServiceHibernate;
import com.resolve.services.hibernate.vo.ResolveCronVO;
import com.resolve.util.Constants;
import com.resolve.util.Log;
import com.resolve.util.StringUtils;

public class MCron
{
    public HashMap listTask(Map params)
    {
        HashMap result = new HashMap();
        
            
        List<CronTask> tasks = CronStore.getTasks();
        for (CronTask task : tasks)
        {
            result.put(task.getName(), task.getWiki()+","+task.getExpression()+","+task.getStartTime()+","+task.getEndTime());
        }
        
        return result;
    } // listTask

    public String save(Map params)
    {
        ((com.resolve.rscontrol.Main)Main.main).cron.save();
        return "SUCCESS";
    } // save
    
    public static HashMap<String, String> loadTask(Map<String, String> params)
    {
        HashMap<String, String> result = new HashMap<String, String>();
        boolean rc = false;
        
        String cronid = (String)params.get(Constants.ESB_PARAM_CRONID);
        String cronName = (String)params.get(Constants.ESB_PARAM_CRONNAME);
        
        if (cronid != null && cronid.equalsIgnoreCase("ALL"))
        {
            CronStore.load();
            rc = true;
        }
        else if (StringUtils.isNotEmpty(cronid))
        {
            ResolveCronVO cronTask = ServiceHibernate.findCronJob(cronid, null, "system");
            if (cronTask != null)
            {
                // check that cronTask is active
                if (cronTask.ugetUActive() == true)
                {
                    rc = CronStore.addTask(new CronTask(cronTask));
                }
                else
                {
                    rc = CronStore.removeTask(cronTask.getUName());
                }
            }
        }
        else if (StringUtils.isNotEmpty(cronName))
        {
            cronName = cronName.toUpperCase();
            ResolveCronVO cronTask = ServiceHibernate.findCronJob(null, cronName, "system");
            if (cronTask != null)
            {
                // check that cronTask is active
                if (cronTask.ugetUActive() == true)
                {
                    rc = CronStore.addTask(new CronTask(cronTask));
                }
                else
                {
                    rc = CronStore.removeTask(cronTask.getUName());
                }
            }
        }
        
        // set result message
        if (rc)
        {
            result.put("MESSAGE", "SUCCESS: scheduled cron task(s)");
        }
        else
        {
            result.put("MESSAGE", "FAIL: unable to schedule cron task(s)");
        }
        return result;
    } // loadTask
    
    public static HashMap addTask(Map params)
    {
        HashMap result = new HashMap();
        boolean rc = false;
        
        String name = (String)params.get(Constants.CRON_NAME);
        String wiki = (String)params.get(Constants.EXECUTE_WIKI);
        String expression = (String)params.get("EXPRESSION");
        String startTimeStr = (String)params.get("STARTTIME");
        String endTimeStr = (String)params.get("ENDTIME");
        Date startTime = null;
        Date endTime = null;
        
        if (!StringUtils.isEmpty(startTimeStr))
        {
            startTime = new Date(Long.parseLong(startTimeStr));
        }
        if (!StringUtils.isEmpty(endTimeStr))
        {
            endTime = new Date(Long.parseLong(endTimeStr));
        }
        
        if (addTask(name, wiki, expression, startTime, endTime, params))
        {
            result.put("MESSAGE", "SUCCESS: scheduled cron task");
        }
        else
        {
            result.put("MESSAGE", "FAIL: unable to schedule cron task");
        }
        return result;
    } // addTask
    
    public static HashMap removeTask(Map params)
    {
        HashMap result = new HashMap();
        boolean rc = false;
        
        String name = (String)params.get(Constants.CRON_NAME);
        if (!StringUtils.isEmpty(name))
        {
            rc = CronStore.removeTask(name.toUpperCase());
        }
        
        // set result message
        if (rc == true)
        {
            result.put("MESSAGE", "SUCCESS: task removed");
        }
        else
        {
            result.put("MESSAGE", "FAIL: task not removed");
        }
        
        return result;
    } // removeTask
    
    static boolean addTask(String name, String wiki, String expression, Date startTime, Date endTime, Map params)
    {
        boolean result = false;
        
        if (!StringUtils.isEmpty(name) && !StringUtils.isEmpty(wiki) && !StringUtils.isEmpty(expression))
        {
	        // init
	        name = name.toUpperCase();
	        Map jobParams = new HashMap(params);
	        jobParams.put("NAME", name);
	        jobParams.put("WIKI", wiki);
	        jobParams.put("EXPRESSION", expression);
	        if (startTime != null)
	        {
		        jobParams.put("STARTTIME", startTime);
	        }
	        if (endTime != null)
	        {
		        jobParams.put("ENDTIME", endTime);
	        }
	        
	        CronTask task = new CronTask();
	        task.setName(name);
	        task.setClassname("com.resolve.rscontrol.CronActionTrigger");
	        task.setWiki(wiki);
	        task.setExpression(expression);
	        task.setParams(jobParams);
	        task.setStartTime(startTime);
	        task.setEndTime(endTime);
	        
	        // schedule task
	        result = CronStore.addTask(task);
        }
        
        return result;
    } // addTask

    /**
     * Scheduled a runbook, gse script or JMS message to be triggered based on a cron expression. This will insert the cron record into 
     * resolve_cron and load into the scheduler.
     * 
     * @param name - unique name for the scheduled cron task
     * @param module - optional namespace, useful for module export
     * @param runbook - name of the runbook wiki to be executed. Also supports <ESBADDR>#Class.Method to send JMS message and MService.<path>.groovy 
     * script to be executed
     * @param cronParams
     * @param expression
     * @param startTime
     * @param endTime
     * @param userid
     * @param active
     * @param force - if true forces the task to be activated/deactivated based on "active" param value.
     */
    public static void scheduleTask(String name, String module, String runbook, Map<String, String> cronParams, String expression, Date startTime, Date endTime, String userid, boolean active, Boolean force)
    {
        //force may be null in some situation like calling CronAPI from an old ActionTask.
        if(force == null)
        {
            force = Boolean.FALSE;
        }

        ResolveCronVO task = ServiceHibernate.findCronJob(null, name, "system");
        if (task == null)
        {
            task = new ResolveCronVO();
            task.setUName(name);
            force=true;
        }
        
        if(force)
        {
            task.setURunbook(runbook);
            task.setUExpression(expression);
            task.setUActive(active);
            
            // optional
            if (module != null)
            {
                task.setUModule(module);
            }
            if (cronParams != null)
            {
                task.setUParams(StringUtils.mapToUrl(cronParams));
            }
            if (startTime != null)
            {
                task.setUStartTime(startTime);
            }
            if (endTime != null)
            {
                task.setUEndTime(endTime);
            }
            
            //save it
            try
            {
                task = ServiceHibernate.saveResolveCron(task, userid);
            }
            catch (Exception e)
            {
               Log.log.error("Error saving the cron task:" + task.getUName(), e);
            }
        }
        HashMap<String, String> params = new HashMap<String, String>();
        params.put(Constants.ESB_PARAM_CRONNAME, name);
        
        
        Log.log.info("Sending message to LoadTask for Cron:" + task.getUName());
        MainBase.esb.sendMessage(Constants.ESB_NAME_RSCONTROLS, "MCron.loadTask", params);
    } // scheduleTask
    
    public static void scheduleTask(String name, String module, String runbook, Map cronParams, String expression, Date startTime, Date endTime, String userid, Boolean active, Boolean force)
    {
        scheduleTask(name, module, runbook, cronParams, expression, startTime, endTime, userid, active.booleanValue(), force);
    } // scheduleTask
    
    public static void scheduleTask(String name, String module, String runbook, Map cronParams, String expression, String userid, Boolean active, Boolean force)
    {
        scheduleTask(name, module, runbook, cronParams, expression, null, null, userid, active.booleanValue(), force);
    } // scheduleTask
    
    public static void scheduleTask(String name, String runbook, Map cronParams, String expression, String userid, Boolean active, Boolean force)
    {
        scheduleTask(name, null, runbook, cronParams, expression, null, null, userid, active.booleanValue(), force);
    } // scheduleTask
    
    public static void scheduleTask(String name, String module, String runbook, String expression, String userid, Boolean active, Boolean force)
    {
        scheduleTask(name, module, runbook, null, expression, null, null, userid, active.booleanValue(), force);
    } // scheduleTask
    
    public static void scheduleTask(String name, String runbook, String expression, String userid, Boolean active, Boolean force)
    {
        scheduleTask(name, null, runbook, null, expression, null, null, userid, active.booleanValue(), force);
    } // scheduleTask

    public static boolean unscheduleTask(String name)
    {
        boolean result = false;
        
        if (CronStore.removeTask(name.toUpperCase()) == true)
        {
            ServiceHibernate.deleteCronByName(name, "system");
            result = true;
        }
        
        return result;
    } // unscheduleTask
    
    public static void activateTask(String name, String userid)
    {
        ResolveCronVO task = ServiceHibernate.findCronJob(null, name, "system");
        if (task != null)
        {
            task.setUActive(true);
            try
            {
                task = ServiceHibernate.saveResolveCron(task, userid);
            }
            catch (Exception e)
            {
               Log.log.error("Error saving the cron task:" + task.getUName(), e);
            }
        }
        else
        {
            throw new RuntimeException("Unable to find cron schedule task name: " + name);
        }

        HashMap<String, String> params = new HashMap<String, String>();
        params.put(Constants.ESB_PARAM_CRONNAME, name);

        loadTask(params);
    } // activateTask
        
    public static void deactivateTask(String name, String userid)
    {
        ResolveCronVO task = ServiceHibernate.findCronJob(null, name, "system");
        if (task != null)
        {
            task.setUActive(false);
            try
            {
                task = ServiceHibernate.saveResolveCron(task, userid);
            }
            catch (Exception e)
            {
               Log.log.error("Error saving the cron task:" + task.getUName(), e);
            }
        }
        else
        {
            throw new RuntimeException("Unable to find cron schedule task name: " + name);
        }
        
        HashMap<String, String> params = new HashMap<String, String>();
        params.put(Constants.ESB_PARAM_CRONNAME, name);
        
        loadTask(params);
    } // deactivateTask

    // ~~~~~~~~~
    // Generally following methods are invoked from a ESB message listener. Message is sent from
    // the CronAPI class.
    // ~~~~~~~~~
    
    /**
     * This method is called from the {@link CronAPI} through the ESB message
     * @param params
     */
    public static void scheduleTask(Map<String, Object> params)
    {
        if(params != null)
        {
            String name = (String) params.get(Constants.CRON_NAME);
            String module = (String) params.get(Constants.CRON_MODULE);
            String runbook = (String) params.get(Constants.CRON_RUNBOOK);
            Map cronParams = (Map) params.get(Constants.CRON_PARAMS);
            String expression = (String) params.get(Constants.CRON_EXPRESSION);
            Date startTime = (Date) params.get(Constants.CRON_START_TIME);
            Date endTime = (Date) params.get(Constants.CRON_END_TIME);
            String userid = (String) params.get(Constants.CRON_USER_ID);
            Boolean active = (Boolean) params.get(Constants.CRON_ACTIVE);
            Boolean force = (Boolean) params.get(Constants.CRON_FORCE);
            
            scheduleTask(name, module, runbook, cronParams, expression, startTime, endTime, userid, active, force);
        }
    }

    /**
     * This method is called from the {@link CronAPI} through the ESB message
     * 
     * @param tasks
     */
    public static void unscheduleTask(Map<String, String> params)
    {
        if(params != null && params.containsKey(Constants.CRON_NAME))
        {
            String name = params.get(Constants.CRON_NAME);
            unscheduleTask(name);
        }
    }

    public static void activateTask(Map<String, String> params)
    {
        if(params != null && params.containsKey(Constants.CRON_NAME))
        {
            String name = params.get(Constants.CRON_NAME);
            String userId = params.get(Constants.CRON_USER_ID);
            activateTask(name, userId);
        }
    }
        
    public static void deactivateTask(Map<String, String> params)
    {
        if(params != null && params.containsKey(Constants.CRON_NAME))
        {
            String name = params.get(Constants.CRON_NAME);
            String userId = params.get(Constants.CRON_USER_ID);
            deactivateTask(name, userId);
        }
    }
    
} // MCron
