/******************************************************************************
* (C) Copyright 2014
*
* Resolve Systems, LLC
*
* All rights reserved
* This software is distributed under license and may not be used, copied,
* modified, or distributed without the express written permission of
* Resolve Systems, LLC.
*
******************************************************************************/

package com.resolve.rscontrol;

import java.sql.Connection;
import java.sql.Statement;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.TimeZone;

import com.resolve.sql.SQL;
import com.resolve.sql.SQLConnection;
import com.resolve.util.Constants;
import com.resolve.util.DateUtils;
import com.resolve.util.Log;
import com.resolve.util.StringUtils;

public class PurgeTable
{
    public synchronized static void purge(String cutoffDate, boolean isLobOnly)
    {
        if(StringUtils.isEmpty(cutoffDate))
        {
            throw new RuntimeException("Cutoff date must be provided in \"yyyymmdd\" format");
        }
        if(!DateUtils.isValid(cutoffDate, "yyyyMMdd"))
        {
            throw new RuntimeException("Invalid Cutoff date, must be provided in \"yyyymmdd\" format");
        }
        
        long startTime = System.currentTimeMillis();

        Log.log.debug("Starting MAIN Purge: " + startTime);

        String dbType = ((Main) Main.main).configSQL.getDbtype().toLowerCase();
        String dateString = convertDate(dbType, cutoffDate);

        SQLConnection sqlConn = null;
        String sql = null;

        try
        {
            sqlConn = SQL.getConnection();
            // Purge from archive_process_request
            StringBuilder sqlStmt = new StringBuilder();
            int records;

            if(isLobOnly)
            {
                Log.log.debug("Purging only LOB tables..");
                //First empty the u_execute_result_lob reference
                sqlStmt.setLength(0);
                sqlStmt.append("UPDATE archive_execute_result set u_execute_result_lob=null WHERE u_execute_result_lob in (select sys_id from archive_execute_result_lob WHERE sys_updated_on < " + dateString + ")");
                records = executeSQL(sqlConn, sqlStmt.toString());
                Log.log.debug("Emptied out the archive_execute_result.u_execute_result_lob, count:" + records);
                
                // Purge from archive_execute_result_lob
                sqlStmt.setLength(0);
                sqlStmt.append("DELETE from archive_execute_result_lob WHERE sys_updated_on < " + dateString);
                records = executeSQL(sqlConn, sqlStmt.toString());
                Log.log.debug("Deleted from archive_execute_result_lob, count:" + records);

                //First empty the u_execute_result_lob reference
                sqlStmt.setLength(0);
                sqlStmt.append("UPDATE archive_action_result set u_action_result_lob=null WHERE u_action_result_lob in (select sys_id from archive_action_result_lob WHERE sys_updated_on < " + dateString + ")");
                records = executeSQL(sqlConn, sqlStmt.toString());
                Log.log.debug("Emptied out the archive_action_result.u_action_result_lob, count:" + records);

                // Purge from archive_action_result_lob
                sqlStmt.setLength(0);
                sqlStmt.append("DELETE from archive_action_result_lob WHERE sys_updated_on < " + dateString);
                records = executeSQL(sqlConn, sqlStmt.toString());
                Log.log.debug("Deleted from archive_action_result_lob, count:" + records);
            }
            else
            {
                Log.log.debug("Purging ALL tables..");
                sqlStmt.append("DELETE from archive_process_request WHERE sys_updated_on < " + dateString);
                records = executeSQL(sqlConn, sqlStmt.toString());
                Log.log.debug("Deleted from archive_process_request, count:" + records);
                
                // Purge from archive_worksheet
                sqlStmt.setLength(0);
                sqlStmt.append("DELETE from archive_worksheet WHERE sys_updated_on < " + dateString);
                records = executeSQL(sqlConn, sqlStmt.toString());
                Log.log.debug("Deleted from archive_worksheet, count:" + records);
    
                // Purge from archive_worksheet_debug
                sqlStmt.setLength(0);
                sqlStmt.append("DELETE from archive_worksheet_debug WHERE sys_updated_on < " + dateString);
                records = executeSQL(sqlConn, sqlStmt.toString());
                Log.log.debug("Deleted from archive_worksheet_debug, count:" + records);
    
                // Purge from archive_execute_request
                sqlStmt.setLength(0);
                sqlStmt.append("DELETE from archive_execute_request WHERE sys_updated_on < " + dateString);
                records = executeSQL(sqlConn, sqlStmt.toString());
                Log.log.debug("Deleted from archive_execute_request, count:" + records);
    
                // Purge from archive_execute_result_lob
                sqlStmt.setLength(0);
                sqlStmt.append("DELETE from archive_execute_result_lob WHERE sys_updated_on < " + dateString);
                records = executeSQL(sqlConn, sqlStmt.toString());
                Log.log.debug("Deleted from archive_execute_result_lob, count:" + records);

                // Purge from archive_execute_result
                sqlStmt.setLength(0);
                sqlStmt.append("DELETE from archive_execute_result WHERE sys_updated_on < " + dateString);
                records = executeSQL(sqlConn, sqlStmt.toString());
                Log.log.debug("Deleted from archive_execute_result, count:" + records);

                // Purge from archive_action_result_lob
                sqlStmt.setLength(0);
                sqlStmt.append("DELETE from archive_action_result_lob WHERE sys_updated_on < " + dateString);
                records = executeSQL(sqlConn, sqlStmt.toString());
                Log.log.debug("Deleted from archive_action_result_lob, count:" + records);

                // Purge from archive_action_result
                sqlStmt.setLength(0);
                sqlStmt.append("DELETE from archive_action_result WHERE sys_updated_on < " + dateString);
                records = executeSQL(sqlConn, sqlStmt.toString());
                Log.log.debug("Deleted from archive_action_result, count:" + records);
            }
            
            //After the purge, optimize the tables.
            optimize(sqlConn, dbType, isLobOnly);
        }
        catch (Throwable e)
        {
            Log.log.warn("SQL: " + sql);
            Log.log.warn(e.getMessage(), e);
        }
        finally
        {
            SQL.close(sqlConn);
        }

        Log.log.debug("Completed MAIN Purge: " + (System.currentTimeMillis() - startTime)/1000 + " seconds");
    }

    private static void optimize(SQLConnection conn, String dbType, boolean isLobOnly)
    {
        StringBuilder sqlStmt = new StringBuilder();
        
        if (dbType.equals(Constants.DB_TYPE_MYSQL))
        {
            Log.log.debug("Optimizing MySQL tables..");
            optimizeLobTables(conn, dbType);
            if(!isLobOnly)
            {
                if (dbType.equals(Constants.DB_TYPE_MYSQL))
                {
                    sqlStmt.setLength(0);
                    sqlStmt.append("OPTIMIZE TABLE archive_process_request");
                    executeSQL(conn, sqlStmt.toString());
                    Log.log.debug("Optimized archive_process_request table");
                    
                    sqlStmt.setLength(0);
                    sqlStmt.append("OPTIMIZE TABLE archive_worksheet");
                    executeSQL(conn, sqlStmt.toString());
                    Log.log.debug("Optimized archive_worksheet table");
                    
                    sqlStmt.setLength(0);
                    sqlStmt.append("OPTIMIZE TABLE archive_worksheet_debug");
                    executeSQL(conn, sqlStmt.toString());
                    Log.log.debug("Optimized archive_worksheet_debug table");
                    
                    sqlStmt.setLength(0);
                    sqlStmt.append("OPTIMIZE TABLE archive_execute_request");
                    executeSQL(conn, sqlStmt.toString());
                    Log.log.debug("Optimized archive_execute_request table");
                    
                    sqlStmt.setLength(0);
                    sqlStmt.append("OPTIMIZE TABLE archive_execute_result");
                    executeSQL(conn, sqlStmt.toString());
                    Log.log.debug("Optimized archive_execute_result table");
                    
                    sqlStmt.setLength(0);
                    sqlStmt.append("OPTIMIZE TABLE archive_action_result");
                    executeSQL(conn, sqlStmt.toString());
                    Log.log.debug("Optimized archive_action_result table");
                }
            }
        }
    }
    
    private static void optimizeLobTables(SQLConnection conn, String dbType)
    {
        StringBuilder sqlStmt = new StringBuilder();
        
        if (dbType.equals(Constants.DB_TYPE_MYSQL))
        {
            Log.log.debug("Optimizing MySQL LOB tables..");
            sqlStmt.setLength(0);
            sqlStmt.append("OPTIMIZE TABLE archive_execute_result_lob");
            executeSQL(conn, sqlStmt.toString());
            Log.log.debug("Optimized archive_execute_result_lob table");

            sqlStmt.setLength(0);
            sqlStmt.append("OPTIMIZE TABLE archive_action_result_lob");
            executeSQL(conn, sqlStmt.toString());
            Log.log.debug("Optimized archive_action_result_lob table");
        }
    }
    
    private static String convertDate(String dbType, String cutoffDate)
    {
        Date theDate = DateUtils.convertStringToDate(cutoffDate, new SimpleDateFormat("yyyyMMdd"), TimeZone.getDefault().getID());
        Log.log.debug("The cutoff date is : " + cutoffDate);
        String dateString = "";
        if (dbType.equals(Constants.DB_TYPE_MYSQL))
        {
            DateFormat format = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
            dateString = "'" + format.format(theDate) + "'";
        }
        else if (dbType.equals(Constants.DB_TYPE_ORACLE))
        {
            DateFormat format = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
            dateString = "to_date('" + format.format(theDate) + "', 'yyyy-mm-dd hh24:mi:ss')";
        }
        else if (dbType.equals(Constants.DB_TYPE_DB2))
        {
            DateFormat format = new SimpleDateFormat("yyyy-MM-dd-HH.mm.ss");
            dateString = "'" + format.format(theDate) + ".000000'";

        }
        return dateString;
    }

    private static int executeSQL(SQLConnection conn, String sql)
    {
        int result = -1;
        Statement stmt = null;
        try
        {
            long time = Calendar.getInstance().getTimeInMillis();
            stmt = conn.createStatement();
            result = stmt.executeUpdate(sql);
            time = Calendar.getInstance().getTimeInMillis() - time;
            if (Log.log.isDebugEnabled())
            {
                Log.log.debug(time / 1000 + " seconds spent on " + result + " records by " + (sql.length() > 100 ? sql.substring(0, 100) : sql));
            }
        }
        catch (Throwable e)
        {
            Log.log.warn(e.getMessage(), e);
        }
        finally
        {
            try
            {
                stmt.close();
            }
            catch (Exception e)
            {
                Log.log.warn(e.getMessage(), e);
            }
        }
        return result;
    } // executeSQL
}
