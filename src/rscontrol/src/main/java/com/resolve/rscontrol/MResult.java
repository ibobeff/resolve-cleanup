/******************************************************************************
* (C) Copyright 2014
*
* Resolve Systems, LLC
*
* All rights reserved
* This software is distributed under license and may not be used, copied,
* modified, or distributed without the express written permission of
* Resolve Systems, LLC.
*
******************************************************************************/

package com.resolve.rscontrol;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.locks.ReentrantLock;
import java.util.regex.Pattern;
import org.codehaus.jackson.JsonNode;
import org.codehaus.jackson.JsonProcessingException;
import org.codehaus.jackson.map.ObjectMapper;
import com.resolve.assess.AssessResult;
import com.resolve.esb.MMsgContent;
import com.resolve.esb.MMsgHeader;
import com.resolve.esb.MMsgOptions;
import com.resolve.exception.ActionTaskAbortException;
import com.resolve.groovy.script.GroovyScript;
import com.resolve.parser.Parser;
import com.resolve.rsbase.MainBase;
import com.resolve.search.model.Worksheet;
import com.resolve.services.ServiceHibernate;
import com.resolve.services.ServiceWorksheet;
import com.resolve.services.WSDataMap;
import com.resolve.services.hibernate.vo.ResolveAssessVO;
import com.resolve.services.util.BindingScript;
import com.resolve.services.util.VariableUtil;
import com.resolve.services.util.WSDataMapImpl;
import com.resolve.services.util.WorksheetUtils;
import com.resolve.thread.TaskExecutor;
import com.resolve.util.Constants;
import com.resolve.util.CryptMD5;
import com.resolve.util.EncryptionTransportObject;
import com.resolve.util.ExceptionUtils;
import com.resolve.util.LockUtils;
import com.resolve.util.Log;
import com.resolve.util.NameTypeValue;
import com.resolve.util.ScriptDebug;
import com.resolve.util.StopWatch;
import com.resolve.util.StringUtils;
import com.resolve.util.XDoc;
import com.resolve.util.eventsourcing.EventMessage;
import com.resolve.util.eventsourcing.EventMessageFactory;
import com.resolve.util.performance.ExecutionTimeUtil;
import com.resolve.util.performance.datasources.ExecutionStep;
import com.resolve.util.performance.datasources.ExecutionStepPhase;
import com.resolve.vo.ActionTaskResultVO;
import groovy.lang.Binding;

public class MResult
{
    final static int MAX_SUMMARY_SIZE = 990;

    static Pattern VAR_REGEX_REFERENCE = Pattern.compile(Constants.VAR_REGEX_REFERENCE);
    final static Pattern VAR_REGEX_PROPERTY = Pattern.compile(Constants.VAR_REGEX_PROPERTY);
    static final List<String> severityList = new ArrayList<String>();
    static final List<String> conditionList = new ArrayList<String>();
    
    static
    {
        GroovyScript.setThreadGroovyFactory(ThreadGroovyDB.class);
        severityList.add("critical");
        severityList.add("severe");
        severityList.add("warning");
        severityList.add("good");
        
        conditionList.add("good");
        conditionList.add("bad");
    } // static

    private static class ActionResultCallHelper
    {
        MMsgOptions options;
        MMsgContent content;

        ActionResultCallHelper(MMsgOptions options, MMsgContent content)
        {
            this.options = options;
            this.content = content;
        }

        public void call()
        {
            actionResult(options, content);
        }
    }

    public static void actionResult(MMsgOptions options, MMsgContent content, boolean local)
    {
        if (local)
        {
            if (Log.log.isDebugEnabled()) {
                Log.log.debug("Calling Local Action Result");
            }
            TaskExecutor.execute(new ActionResultCallHelper(options, content), "call");
        }
        else
        {
            if (Log.log.isDebugEnabled()) {
                Log.log.debug("Calling RSRemote Action Result");
            }
            actionResult(options, content);
        }
    }

    private static void actionResult(MMsgOptions options, MMsgContent content)
    {
        Log.start();

        try
        {
            String targetAddr;
            String targetGuid;
            String targetAffinity;
            String actionName;
            String actionNamespace;
            String actionSummary;
            String actionHidden;
            String actionTags;
            String actionRoles;
            String actionid;
            String processid;
            String wiki;
            String executeid;
            String parserid;
            String assessid;
            String durationStr;
            String logRawResult;
            String userid;
            String problemid;
            String reference;
            String returncode;
            String completion;
            String command;
            String taskTimeoutStr;
            String processTimeoutStr;
            String metricId;
            String paramsStr;
            String flowsStr;
            String inputsStr;
            String debugStr;
            String raw;
            String requestTimestampStr;
            String mock;
            String blankValues;

            // check for ActionTaskResultVO
            String voStr = content.getVO();
            if (StringUtils.isNotEmpty(voStr))
            {
                ActionTaskResultVO vo = new ActionTaskResultVO(voStr, true);

                targetAddr = vo.getAddress();
                targetGuid = vo.getTarget();
                targetAffinity = vo.getAffinity();
                actionName = vo.getActionName();
                actionNamespace = vo.getActionNamespace();
                actionSummary = vo.getActionSummary();
                actionHidden = vo.getActionHidden();
                actionTags = vo.getActionTags();
                actionRoles = vo.getActionRoles();
                actionid = vo.getActionId();
                processid = vo.getProcessId();
                wiki = vo.getWiki();
                executeid = vo.getExecuteId();
                parserid = vo.getParserId();
                assessid = vo.getAssessId();
                durationStr = vo.getDuration();
                logRawResult = vo.getLogResult();
                userid = vo.getUserId();
                problemid = vo.getProblemId();
                reference = vo.getReference();
                returncode = vo.getReturnCode();
                completion = vo.getCompletion();
                command = vo.getCommand();
                taskTimeoutStr = vo.getTimeout();
                processTimeoutStr = vo.getProcessTimeout();
                metricId = vo.getMetricId();
                mock = vo.getMock();
                raw = vo.getRaw();
                debugStr = vo.getDebugStr();
                blankValues = "";

                paramsStr = "";
                flowsStr = "";
                inputsStr = "";

                // request timestamp for detached actiontasks
                requestTimestampStr = vo.getRequestTimestamp();
            }

            // init from options and content
            else
            {
                targetAddr = options.getAddress();
                targetGuid = options.getTarget();
                targetAffinity = options.getAffinity();
                actionName = options.getActionName();
                actionNamespace = options.getActionNamespace();
                actionSummary = options.getActionSummary();
                actionHidden = options.getActionHidden();
                actionTags = options.getActionTags();
                actionRoles = options.getActionRoles();
                actionid = options.getActionID();
                processid = options.getProcessID();
                wiki = options.getWiki();
                executeid = options.getExecuteID();
                parserid = options.getParserID();
                assessid = options.getAssessID();
                durationStr = options.getDuration();
                logRawResult = options.getLogResult();
                userid = options.getUserID();
                problemid = options.getProblemID();
                reference = options.getReference();
                returncode = options.getReturnCode();
                completion = options.getCompletion();
                command = options.getCommand() == null ? "" : options.getCommand();
                taskTimeoutStr = options.getTimeout();
                processTimeoutStr = options.getProcessTimeout();
                metricId = options.getMetricId();
                mock = options.getMock();
                blankValues = options.getBlankValues();

                paramsStr = content.getParams();
                flowsStr = content.getFlows();
                inputsStr = content.getInputs();
                debugStr = content.getDebug();
                raw = content.getRaw().toString();

                // request timestamp for detached actiontasks
                requestTimestampStr = options.getRequestTimestamp();
            }
            
            if (StringUtils.isNotEmpty(paramsStr))
            {
                ExecutionTimeUtil.log((String) ((Map) StringUtils.stringToObj(paramsStr)).get("RESOLVE.JOB_ID"), null, null, ExecutionStep.METHOD_EXECUTION, ExecutionStepPhase.START);
            }
            
            // init log processid
            Log.setProcessId(processid);

            // init debug
            boolean isDebug = false;
            if (StringUtils.isNotEmpty(debugStr))
            {
                isDebug = true;
            }
            ScriptDebug debug = new ScriptDebug(isDebug, problemid);
            debug.setBuffer(debugStr);
            debug.setComponent("actionResult:" + actionName);
            
            Map inputs = null;
            Map params = null;
            Map flows = null;
            long duration = 0;
            int taskTimeout = 300;
            int processTimeout = 600;
            ActionTaskProperties properties = ActionTaskProperties.getInstance();
    
            String actionResultGUID = "";
            String activityId = null;
            AssessResult assessResult = null;
            Map problemRecord = new HashMap();
            
            try
            {
                // init inputs, params, flows
                if (StringUtils.isNotEmpty(inputsStr))
                {
                    inputs = (Map) StringUtils.stringToObj(inputsStr);
                }
                if (inputs == null)
                {
                    inputs = new HashMap();
                }
    
                if (StringUtils.isNotEmpty(paramsStr))
                {
                    params = (Map) StringUtils.stringToObj(paramsStr);
                }
                if (params == null)
                {
                    params = new HashMap();
                }
                if (StringUtils.isNotEmpty(flowsStr))
                {
                    flows = (Map) StringUtils.stringToObj(flowsStr);
                }
                if (flows == null)
                {
                    flows = new HashMap();
                }
                
                ExecutionTimeUtil.log((String) params.get("RESOLVE.JOB_ID"), processid, actionid, ExecutionStep.ACTION_TASK_EXECUTION, ExecutionStepPhase.START);
    
                // init duration (msecs)
                if (!StringUtils.isEmpty(durationStr))
                {
                    duration = Long.parseLong(durationStr);
                }
    
                // init duration from requestTimestamp
                if (StringUtils.isNotEmpty(requestTimestampStr) && duration == 0)
                {
                    long timestamp = Long.parseLong(requestTimestampStr);
                    duration = System.currentTimeMillis() - timestamp;
                }
    
                if(Log.log.isDebugEnabled()) {
                    Log.log.debug("ActionResult Label: " + ActionProcessTaskRequest.getTaskLabel(executeid) + " actionName: " + actionName + " processid: " + processid);
                    Log.log.debug("actionName: " + actionName);
                    Log.log.debug("actionNamespace: " + actionNamespace);
                    Log.log.debug("address: " + targetAddr);
                    Log.log.debug("target: " + targetGuid);
                    Log.log.debug("affinity: " + targetAffinity);
                    Log.log.debug("size: " + raw.length());
                    Log.log.debug("actionid: " + actionid);
                    Log.log.debug("processid: " + processid);
                    Log.log.debug("wiki: " + wiki);
                    Log.log.debug("executeid: " + executeid);
                    Log.log.debug("duration: " + duration);
                    Log.log.debug("logResult: " + logRawResult);
                    Log.log.debug("parserid: " + parserid);
                    Log.log.debug("assessid: " + assessid);
                    Log.log.debug("userid: " + userid);
                    Log.log.debug("problemid: " + problemid);
                    Log.log.debug("reference: " + reference);
                    Log.log.debug("returncode: " + returncode);
                    Log.log.debug("completion: " + completion);
                    Log.log.debug("taskTimeout: " + taskTimeoutStr);
                    Log.log.debug("processTimeout: " + processTimeoutStr);
                    Log.log.debug("command: " + command);
                    Log.log.debug("mock: " + mock);
                    Log.log.debug("debug: " + (!StringUtils.isEmpty(debugStr)));
                    Log.log.debug("inputs: " + StringUtils.mapToLogString(inputs));
                    Log.log.debug("params: " + StringUtils.mapToLogString(params));
                    Log.log.debug("flows: " + StringUtils.mapToLogString(flows));
                    Log.log.debug("raw:\n" + raw);
                }

                if(isDebug) {
                    debug.println("ActionResult Label: " + ActionProcessTaskRequest.getTaskLabel(executeid) + " actionName: " + actionName + " processid: " + processid);
                    debug.println("actionName: " + actionName);
                    debug.println("address: " + targetAddr);
                    debug.println("target: " + targetGuid);
                    debug.println("affinity: " + targetAffinity);
                    debug.println("size: " + raw.length());
                    debug.println("actionid: " + actionid);
                    debug.println("processid: " + processid);
                    debug.println("wiki: " + wiki);
                    debug.println("executeid: " + executeid);
                    debug.println("duration: " + duration);
                    debug.println("logResult: " + logRawResult);
                    debug.println("parserid: " + parserid);
                    debug.println("assessid: " + assessid);
                    debug.println("userid: " + userid);
                    debug.println("problemid: " + problemid);
                    debug.println("reference: " + reference);
                    debug.println("returncode: " + returncode);
                    debug.println("completion: " + completion);
                    debug.println("taskTimeout: " + taskTimeoutStr);
                    debug.println("processTimeout: " + processTimeoutStr);
                    debug.println("command: " + command);
                    debug.println("mock: " + mock);
                    debug.println("debug: " + (!StringUtils.isEmpty(debugStr)));
                    debug.println("inputs: " + StringUtils.mapToLogString(inputs));
                    debug.println("params: " + StringUtils.mapToLogString(params));
                    debug.println("flows: " + StringUtils.mapToLogString(flows));
                    debug.println("raw:\n" + raw);
                    WorksheetUtils.writeDebugCAS(debug);
                }
                
                // wait if process is still initializing, skip if process has
                // already aborted
                if (Log.log.isDebugEnabled()) {
                    Log.log.debug("MResult.actionResult waitForDependInit - wiki: " + wiki + " processid: " + processid);
                }
                if (ProcessExecution.waitForDependInit(processid) == false)
                {
                    return;
                }
    
                // default taskTimeout in secs
                if (!StringUtils.isEmpty(taskTimeoutStr))
                {
                    taskTimeout = Integer.parseInt(taskTimeoutStr);
                }
    
                // default processTimeout in secs
                if (!StringUtils.isEmpty(processTimeoutStr))
                {
                    processTimeout = Integer.parseInt(processTimeoutStr);
                }

                if (!StringUtils.isEmpty(problemid))
                {
                    // init AssessResult
                    String summary;
                    if (raw.length() < MAX_SUMMARY_SIZE)
                    {
                        summary = raw;
                    }
                    else
                    {
                        summary = raw.substring(0, MAX_SUMMARY_SIZE) + "...";
                    }

                    // init assessResult
                    assessResult = new AssessResult(completion, returncode, summary, raw, (int) (duration / 1000), targetAddr);

                    // result XML
                    Map outputs = new ConcurrentHashMap();

                    // assess result
                    if (actionName.equalsIgnoreCase("start") || actionName.equalsIgnoreCase("end") || actionName.equalsIgnoreCase("event") || actionName.equalsIgnoreCase("wait"))
                    {
                        Map<String, String> data = parseStartEnd(raw);

                        assessResult.setCondition(data.get("CONDITION"));
                        assessResult.setSeverity(data.get("SEVERITY"));
                        assessResult.setSummary(data.get("SUMMARY"));
                        assessResult.setDetail(data.get("DETAIL"));
                        assessResult.setDuration(data.get("DURATION"));
                    }
                    else
                    {
                        Object data = parse(parserid, problemid, raw, inputs, outputs, flows, params, taskTimeout, debug);

                        // NOTE: no transaction during assessment unless
                        // discovered
                        ResolveAssessVO assessVO = ServiceHibernate.findResolveAssessWithoutRefs(assessid, null, userid);
                        assess(assessVO, processid, executeid, problemid, assessid, userid, targetAddr, actionName, raw, inputs, outputs, params, flows, debug, data, problemRecord, reference, wiki, taskTimeout, assessResult, properties);
                        
                        try
                        {
                        	if (assessVO != null)
                        	{
                        		processOutputMapping(assessVO.getUOutputMappingScript(), processid, problemid, params, flows, inputs, outputs, actionName, userid, taskTimeout); 
                        		
	                        	processConditionAndSeverity(assessVO.getUExpressionScript(), processid, problemid, inputs, outputs, params, flows, properties, actionName, assessResult, userid, taskTimeout);
	                        	
	                        	processSummaryAndDetail(assessVO.getSummaryRule(), assessVO.getDetailRule(), processid, problemid, params, flows, inputs, outputs, raw, properties, actionName, assessResult, userid, taskTimeout);
	                        	
	                        	setOutputParameters(processid, problemid, executeid, inputs, outputs, params, flows);
                        	}
                        }
                        catch(IOException e)
                        {
                        	Log.log.warn(e.getMessage());
                        }
                        
                    }
                    targetAddr = assessResult.getAddress();
                    
                    if (StringUtils.isNotBlank(blankValues))
                    {
                    	if (StringUtils.isNotBlank(assessResult.getSummary()))
                    	{
                    		String replaceSummary = StringUtils.blankVals(assessResult.getSummary(), blankValues, ",");
                    		if (StringUtils.isNotBlank(replaceSummary))
                    		{
                    			assessResult.setSummary(replaceSummary);
                    		}
                    	}
                    	if (StringUtils.isNotBlank(assessResult.getDetail()))
                    	{
                    		String replaceDetail = StringUtils.blankVals(assessResult.getDetail(), blankValues, ",");
                    		if (StringUtils.isNotBlank(replaceDetail))
                    		{
                    			assessResult.setDetail(replaceDetail);
                    		}
                    	}
                    }

                    // log execute results 
// removing - since all data is from AR from ElasticSearch                    
//                    String executeResult = logExecuteResultCAS(executeid, targetAddr, userid, command, assessResult.getDuration(), returncode, logResult, raw, processid, problemid);

                    // log action result to worksheet
//                    logActionResultCAS(problemid, executeid, userid, targetAddr, targetGuid, targetAffinity, actionid, processid, executeResult, assessResult, actionName, command, returncode);
                    actionResultGUID = (String)params.get(Constants.EXECUTE_ACTIONRESULT_GUID);
                    activityId = (String)params.get(Constants.ACTIVITY_ID);
                    
                    final String targetAddrF = targetAddr;
                    final AssessResult assessResultF = assessResult;
                    final String actionResultGUIDF = actionResultGUID;
                    final String activityIdF = activityId;
                    final Map paramsF = params;
                    
			        Map<String, ActionProcessTaskRequest> aptrMap = null;
			        ActionProcessTask apt = null;
			        if (StringUtils.isNotBlank(processid))
			        {
			            aptrMap = ProcessRequest.getExec2TaskReqMap(processid);
			            apt = ((Map<String, ActionProcessTask>)ProcessRequest.getModelStates(processid).get(ProcessRequest.EXECID_TASK))
			            	  .get(executeid);
			        }
			        if (aptrMap==null && StringUtils.isNotBlank(processid))
			        {
			        	throw new RuntimeException(String.format("Can't find ActionProcessTaskRequest map for processid: %s, " +
								 								 "problemid: %s", processid, problemid));
			        }
			        final String nodeid = apt != null ? apt.getId() : "";
			        ActionProcessTaskRequest aptr = aptrMap != null ? aptrMap.get(executeid) : null;
			        String execNumber = aptr != null ? aptr.getNumber() : "";
			        String execReqNum = StringUtils.isNotEmpty(executeid) ? execNumber : "";
                
			        
			        long timeStamp = System.currentTimeMillis();
			        MResult.logActionResult(problemid, executeid, userid, targetAddrF, targetGuid, targetAffinity, actionid,
			            processid, assessResultF, actionName, actionNamespace, actionSummary, actionHidden, actionTags,
			            actionRoles, command, returncode, actionResultGUIDF, raw, logRawResult, mock, activityIdF,
			            paramsF, nodeid, wiki, execReqNum, timeStamp);

			        boolean reTry = false;
                    int retryCount = 0;
                    do
                    {
                        reTry = false;
                        if (retryCount > 0)
                        {
                            Log.log.info("logWorksheet, count=" + retryCount);
                        }
                        try
                        {
                            logWorksheet(problemid, processid, executeid, userid, problemRecord, debug, assessResult.getCondition(), assessResult.getSeverity(), actionName, params);
                        }
                        catch (Throwable t)
                        {
                            Throwable cause = ExceptionUtils.getRootCause(t);
                            if (cause != null)
                            {
                                String message = cause.getMessage();
                                if (message != null && message.indexOf("waiting for lock") >= 0)
                                {
                                    reTry = true;
                                    ++retryCount;
                                    Log.log.info("Retry to acquire database lock", t);
                                }
                                else
                                {
                                    Log.log.error(t.getMessage(), t);
                                }
                            }
                            else
                            {
                                Log.log.error(t.getMessage(), t);
                            }
                        }
                    } while (reTry);

			        // write debug
                    WorksheetUtils.writeDebugCAS(debug);
                	//sysLog end of an ActionTask
                    String taskName = options.getActionName() + "#" + options.getActionNamespace();
                    String wikiName = StringUtils.isBlank(wiki) ? "Standalone Task" : wiki;
                    
                    EventMessage eventMessage;
                    String incidentId = (String) params.get(Constants.EXECUTE_INCIDENT_ID);
                    String rootResolutionId = (String) params.get(Constants.EXECUTE_ROOT_RUNBOOK_ID);
                    String taskId = (String) params.get(Constants.EXECUTE_ACTION_TASK_ID);
                	if(params.containsKey(Constants.EXECUTE_INCIDENT_ID)){
	                	if (!taskName.equalsIgnoreCase("start#resolve") && !taskName.equalsIgnoreCase("end#resolve"))
	                	{
	                		StringBuilder status = new StringBuilder();
	                		if(assessResult.getCondition().equalsIgnoreCase(Constants.ASSESS_CONDITION_BAD)) 
	                		{
	                			status.append("FAILED ");
	                			eventMessage = EventMessageFactory.getInstance().getIncidentReolutionTaskFailedMessage(incidentId, taskId, rootResolutionId);
	                		}
	                		else
	                		{
	                			status.append("COMPLETED ");
	                            eventMessage = EventMessageFactory.getInstance().getIncidentReolutionTaskCompletedMessage(incidentId, taskId, rootResolutionId);
	                		}
	                		status.append("\"CONDITION:").append(assessResult.getCondition().toUpperCase())
	                		.append("\" \"SEVERITY:").append(assessResult.getSeverity().toUpperCase()).append("\"");
	                		Log.syslog.info(String.format("[RUNBOOK] %s %s %s %s %s", MainBase.main.configId.getLocation(), userid, "\"" + wikiName + "\"", "\"" + taskName + "\"", status.toString()));
	                	} else {
	                	    eventMessage = EventMessageFactory.getInstance().getIncidentReolutionTaskCompletedMessage(incidentId, taskId, rootResolutionId);
	                	}
                	
                	    Main.esb.sendInternalMessage(Constants.ESB_NAME_EVENT_SOURCING_DCS, "", eventMessage.toJSON());
                	}

                    // update actiontask metrics
                    if (StringUtils.isNotEmpty(metricId) && StringUtils.isNotEmpty(processid))
                    {
                        ProcessState state = ProcessExecution.getProcessState(processid);
                        if (state != null)
                        {
                            long value = (System.currentTimeMillis() - state.getStartTime()) / 1000;

                            String[] groupIdUnit = metricId.split("/");
                            String group = groupIdUnit[0];
                            String id = groupIdUnit[1];
                            String unit = groupIdUnit[2];
                            MetricActionTask.addMetricUnitValue(group, id, unit, value, 1);
                        }
                    }

                    ExecutionTimeUtil.log((String) params.get("RESOLVE.JOB_ID"), processid, actionid, ExecutionStep.ACTION_TASK_EXECUTION, ExecutionStepPhase.END);
                    // send notification to RSControl if Process ActionTask
                    ProcessExecution.taskCompleted(processid, executeid, problemid, wiki, targetAddr, targetGuid, targetAffinity, userid, actionName, assessResult, outputs, params, flows, debug, mock, processTimeout, properties);
                }
            }
            catch (Throwable e)
            {
            	if (StringUtils.isNotBlank(e.getMessage()) && 
            		!e.getMessage().startsWith("Missing modelStates for processid")) {
            			Log.log.error(e.getMessage(), e);
            	} else {
            		Log.log.warn(e.getMessage());
            	}
            		
        		debug.println(e.getMessage(), e);
            
        		// log action result to worksheet
        		if (assessResult == null) {
        			assessResult = new AssessResult(e);
        		}
        		
        		final String tProcessId = e.getMessage().startsWith("Missing modelStates for processid") ? null : processid;
        		
        		try {
			        Map<String, ActionProcessTaskRequest> aptrMap = null;
			        ActionProcessTask apt = null;
			        if (StringUtils.isNotBlank(processid))
			        {
			            aptrMap = ProcessRequest.getExec2TaskReqMap(processid);
			            apt = ((Map<String, ActionProcessTask>)ProcessRequest.getModelStates(processid).get(ProcessRequest.EXECID_TASK))
			            	  .get(executeid);
			        }
			        if (aptrMap==null && StringUtils.isNotBlank(processid))
			        {
			        	throw new RuntimeException(String.format("Can't find ActionProcessTaskRequest map for processid: %s, " +
								 								 "problemid: %s", processid, problemid));
			        }
			        
			        final String nodeid = apt != null ? apt.getId() : "";
			        ActionProcessTaskRequest aptr = aptrMap != null ? aptrMap.get(executeid) : null;
			        String execNumber = aptr != null ? aptr.getNumber() : "";
			        String execReqNum = StringUtils.isNotEmpty(executeid) ? execNumber : "";
			        
                    final String targetAddrF = targetAddr;
                    final AssessResult assessResultF = assessResult;
                    final String actionResultGUIDF = actionResultGUID;
                    final String activityIdF = activityId;
                    final Map paramsF = params;
        			

                    long timeStamp = System.currentTimeMillis();
                    logActionResult(problemid, executeid, userid, targetAddrF, targetGuid, targetAffinity, actionid, 
                        tProcessId, assessResultF, actionName, actionNamespace, actionSummary, actionHidden, 
                        actionTags, actionRoles, command, returncode, actionResultGUIDF, raw, logRawResult, 
                        mock, activityIdF, paramsF, nodeid, wiki, execReqNum, timeStamp);
        		} catch (Throwable t1) {
        			Log.log.error(String.format("Error %sin logging Action Result", 
        										(StringUtils.isNotEmpty(t1.getMessage()) ? 
        										 "[" + t1.getMessage() + "] " : "")), t1);
        		}
        		
            }
            finally
            {
                WorksheetUtils.writeDebugCAS(debug);
                ExecutionTimeUtil.log((String) params.get("RESOLVE.JOB_ID"), null, null, ExecutionStep.METHOD_EXECUTION, ExecutionStepPhase.END);
            }
        }
        catch (Throwable e)
        {
            Log.log.warn(e.getMessage());
            throw new RuntimeException(e);
        }
        finally
        {
            Log.duration("ActionResult duration: ");
        }
    	
    } // actionResult

    public void actionResult(MMsgHeader header, Map msg)
    {
        MMsgOptions options = header.getOptions();
        MMsgContent content = new MMsgContent(msg);
        try
        {
            actionResult(options, content, false);
        }
        finally
        {
            Log.clearProcessId();
        }
    } // actionResult

    @SuppressWarnings("rawtypes")
	public void configResult(MMsgHeader header, Map msg) throws Exception
    {
       try
        {
            MMsgContent content = new MMsgContent(msg);
            String targetAddr = (String) header.getTarget();

            // initialize guid
            String guid = targetAddr;
            
            if (content.getRaw() instanceof Map) {
                if (Log.log.isDebugEnabled()) {
                    Log.log.debug("Processing Config results");
                }
            	processConfig(guid, (Map) content.getRaw());
            }

        } catch (Throwable e) {
        	Log.log.error(String.format("Error %sin updating regsistration info of component with id %s with latest " +
										"component configuration properties (config/properties.txt)", 
										(StringUtils.isNotBlank(e.getMessage()) ? "[" + e.getMessage() + "] " : ""),
										(String) header.getTarget()), e);
        }
    } // configResult

    static Map<String, String> parseStartEnd(String raw)
    {
        Map<String, String> result = new HashMap<String, String>();

        String[] lines = raw.split("\n");
        for (int i = 0; i < lines.length; i++)
        {
            String line = lines[i].trim();
            int pos = line.indexOf('=');
            if (pos > 0)
            {
                String name = line.substring(0, pos);
                String value = line.substring(pos + 1, line.length());
                result.put(name, value);
            }
        }

        return result;
    } // parseStartEnd

    static Object parse(String parserid, String problemid, String raw, Map inputs,  Map outputs, Map flows, Map params, int taskTimeout, ScriptDebug debug) throws Exception
    {
        ExecutionTimeUtil.log((String) params.get("RESOLVE.JOB_ID"), null, null, ExecutionStep.METHOD_EXECUTION, ExecutionStepPhase.START);
        Object result = null;

        if (raw != null)
        {
            if (StringUtils.isEmpty(parserid))
            {
                result = raw;
            }
            else 
            {
                Parser parser = null;
                if (parserid.length() == 32)
                {
                    parser = Parser.get(parserid);
                }
                else
                {
                    parser = Parser.getParserByName(parserid.toUpperCase());
                }

                // if generic parser
                if (parser.isGenericParser())
                {
                    result = parser.parse(raw, taskTimeout, debug);
                }
                else
                {
                    result = parser.parse(parserid, problemid, raw, inputs, outputs, flows, params, taskTimeout, debug);
                }

                if (result == null || (result instanceof String && StringUtils.isEmpty((String) result)))
                {
                    result = raw;
                }

                WorksheetUtils.writeDebugCAS(debug);
            }
        }
        else
        {
            Log.log.warn("Missing raw results");
        }


        ExecutionTimeUtil.log((String) params.get("RESOLVE.JOB_ID"), null, null, ExecutionStep.METHOD_EXECUTION, ExecutionStepPhase.END);
        return result;
    } // parse

    static void assess(ResolveAssessVO assess, String processid, String executeid, String problemid, String assessid, String userid, String targetAddr, String actionName, String raw, Map inputs, Map outputs, Map params, Map flows, ScriptDebug debug, Object data, Map problemRecord, String reference, String wiki, int taskTimeout, AssessResult assessResult, ActionTaskProperties properties) throws Exception
    {
    	ExecutionTimeUtil.log((String) params.get("RESOLVE.JOB_ID"), null, null, ExecutionStep.METHOD_EXECUTION, ExecutionStepPhase.START);
    	
        if (data != null)
        {
            // Decrypt map to allow user to use it natively
            List preInputs = EncryptionTransportObject.decryptMap(inputs);
            List preParams = EncryptionTransportObject.decryptMap(params);
            List preFlows = EncryptionTransportObject.decryptMap(flows);

            try
            {
                if(assess == null)
                {
                    assess = ServiceHibernate.getDefaultResolveAssess();
                    if(assess == null)
                    {
                        throw new Exception("Missing \"DEFAULT#resolve\" assessor");
                    }
                }
                if (assess != null && (assess.ugetUOnlyCompleted() == false || (assess.ugetUOnlyCompleted() && assessResult.isCompleted())))
                {
                    if (Log.log.isDebugEnabled()) {
                        Log.log.debug("Calling ActionResult assess: " + assess.getUName());
                    }
                    debug.println("Calling ActionResult assess: " + assess.getUName());
                    // Log.log.debug("script:\n"+assess.getScript());
                    // Log.log.debug("raw:\n"+raw);

                    StopWatch sw = new StopWatch();

                    // replace special vars e.g. $PROPERTY{NAME}
                    String script = assess.getUScript();
                    String scriptName = assess.getUName();

					// execute script
					if (!StringUtils.isEmpty(script)) {

						// replace properties
						script = VariableUtil.replaceProperties(script);

						// init script debug
						debug.setComponent("Assess:" + actionName);
						debug.setMap(inputs, outputs, params, flows);

						// assess input
						Binding binding = new Binding();
						binding.setVariable(Constants.GROOVY_BINDING_ASSESS,
								new BindingScript(BindingScript.TYPE_ASSESS, binding));
						binding.setVariable(Constants.GROOVY_BINDING_ACTIONNAME, actionName);
						binding.setVariable(Constants.GROOVY_BINDING_RAW, raw);
						binding.setVariable(Constants.GROOVY_BINDING_DATA, data);
						binding.setVariable(Constants.GROOVY_BINDING_LOG, Log.log);
						binding.setVariable(Constants.GROOVY_BINDING_SYSLOG, Log.syslog);
						binding.setVariable(Constants.GROOVY_BINDING_DEBUG, debug);
						binding.setVariable(Constants.GROOVY_BINDING_TARGET, targetAddr);
						binding.setVariable(Constants.GROOVY_BINDING_ADDRESS, targetAddr);
						binding.setVariable(Constants.GROOVY_BINDING_ESB, MainBase.esb);
						binding.setVariable(Constants.GROOVY_BINDING_PROCESSID, processid);
						binding.setVariable(Constants.GROOVY_BINDING_EXECUTEID, executeid);
						binding.setVariable(Constants.GROOVY_BINDING_PROBLEMID, problemid);
						binding.setVariable(Constants.GROOVY_BINDING_REFERENCE, reference);
						binding.setVariable(Constants.GROOVY_BINDING_WIKI, wiki);
						binding.setVariable(Constants.GROOVY_BINDING_USERID, userid);
						binding.setVariable(Constants.GROOVY_BINDING_PROPERTIES, properties);

						// parameters
						binding.setVariable(Constants.GROOVY_BINDING_INPUTS, inputs);
						binding.setVariable(Constants.GROOVY_BINDING_PARAMS, params);
						binding.setVariable(Constants.GROOVY_BINDING_FLOWS, flows);

						// assess output
						binding.setVariable(Constants.GROOVY_BINDING_RESULT, assessResult);
						binding.setVariable(Constants.GROOVY_BINDING_OUTPUTS, outputs);
						binding.setVariable(Constants.GROOVY_BINDING_RECORD, problemRecord);
						binding.setVariable(Constants.GROOVY_BINDING_WORKSHEET, problemRecord);

						if (script.contains(Constants.GROOVY_BINDING_WSDATA))
						{
							WSDataMap wsData = ServiceWorksheet.getWorksheetWSDATA(problemid);
							binding.setVariable(Constants.GROOVY_BINDING_WSDATA, wsData);
						}

						// processinfo
						if (!StringUtils.isEmpty(processid)) {
							ProcessInfo processInfo = ProcessExecution.getProcessInfo(processid);
							if (processInfo != null) {
								binding.setVariable(Constants.GROOVY_BINDING_PROCESSINFO, processInfo);
								binding.setVariable(Constants.GROOVY_BINDING_GLOBALS, processInfo);
							}
						}

						// add OLD_PARAMS if exists
						if (params.containsKey(Constants.GROOVY_BINDING_OLD_PARAMS)) {
							binding.setVariable(Constants.GROOVY_BINDING_OLD_PARAMS,
									params.get(Constants.GROOVY_BINDING_OLD_PARAMS));
						}

						if(Log.log.isTraceEnabled()) {
						    Log.log.trace("Assess Script Name: " + scriptName + ", MD5: " + CryptMD5.encrypt(script));
						}
						GroovyScript.executeAsync(script, scriptName, true, binding, taskTimeout * 1000);
					}

					if (Log.log.isDebugEnabled()) {
					    Log.log.debug("Completed Assessment in " + sw.msec());
					}
                    debug.println("Completed Assessment in " + sw.msec());


                    // update duration to include assessment time
                    int totalDuration = assessResult.getDuration() + sw.getDurationSecs();
                    assessResult.setDuration(totalDuration);
                }
            }
            catch (ActionTaskAbortException e)
            {
                assessResult.setFailed("ActionTask aborted by user or timeout.", "Assess timeout: " + e.getMessage());

                String warnMsg = String.format("Processid:%s, Executeid:%s, Problemid:%s, Assessid:%s, Userid:%s, " +
                							   "TargetAddr:%s, ActionName:%s, Reference:%s, Wiki:%s, TaskTimeout:%d sec," +
                							   " aborted by user or timedout. Error message:%s",
                							   processid, executeid, problemid, assessid, userid, targetAddr, actionName,
                							   reference, wiki, taskTimeout, 
                							   (StringUtils.isNotBlank(e.getMessage()) ? e.getMessage() : ""));
                
                Log.log.warn(warnMsg);
                debug.println(warnMsg);
            }
            catch (Throwable e)
            {
                assessResult.setFailed("ERROR: Result assessment exception", e.getMessage());

                Log.log.warn("ERROR: Result assessment exception. " + e.getMessage(), e);
                debug.println("ERROR: Result assessment exception. " + e.getMessage(), e);
            }
            finally
            {
                WorksheetUtils.writeDebugCAS(debug);

                // Restore Encryption objects
                EncryptionTransportObject.restoreMap(inputs, preInputs);
                EncryptionTransportObject.restoreMap(params, preParams);
                EncryptionTransportObject.restoreMap(flows, preFlows);
            }
        }
        
        ExecutionTimeUtil.log((String) params.get("RESOLVE.JOB_ID"), null, null, ExecutionStep.METHOD_EXECUTION, ExecutionStepPhase.END);

    } // assess

    @SuppressWarnings("rawtypes")
	static void processConditionAndSeverity(String expressionScript, String processId, String problemId, Map inputs, Map outputs, Map params, Map flows, ActionTaskProperties properties, String actionName, AssessResult assessResult, String userId, int taskTimeout)
    {
    	try
    	{
			if (StringUtils.isNotBlank(expressionScript))
			{
				Log.log.trace("Condition and Severity script: " + expressionScript);
				String scriptName = "expression#" + actionName;
				
				//binding for expression
				Binding binding = new Binding();
                binding.setVariable(Constants.GROOVY_BINDING_ACTIONNAME, actionName);
                binding.setVariable(Constants.GROOVY_BINDING_PROCESSID, processId);
                binding.setVariable(Constants.GROOVY_BINDING_USERID, userId);
                binding.setVariable(Constants.GROOVY_BINDING_PROPERTIES, properties);

                // parameters
                binding.setVariable(Constants.GROOVY_BINDING_INPUTS, inputs);
                binding.setVariable(Constants.GROOVY_BINDING_OUTPUTS, outputs);
                binding.setVariable(Constants.GROOVY_BINDING_PARAMS, params);
                binding.setVariable(Constants.GROOVY_BINDING_FLOWS, flows);
                binding.setVariable(Constants.GROOVY_BINDING_RESULT, assessResult);

                if (expressionScript.contains(Constants.GROOVY_BINDING_WSDATA))
                {
	                WSDataMap wsData = ServiceWorksheet.getWorksheetWSDATA(problemId);
	                binding.setVariable(Constants.GROOVY_BINDING_WSDATA, wsData);
                }
                
                if(Log.log.isTraceEnabled()) {
                    Log.log.trace("Expression Script Name: " + scriptName + ", MD5: " + CryptMD5.encrypt(expressionScript));
                }
                GroovyScript.executeAsync(expressionScript, scriptName, true, binding, taskTimeout * 1000);
			}
    	}
    	catch(Exception e)
    	{
    		Log.log.error("ERROR: Expressions could not be executed for task: " + actionName, e);
    	}
    }
    
    @SuppressWarnings("rawtypes")
	static void processOutputMapping(String outputMappingScript, String processid, String problemid, Map params, Map flows, Map inputs, Map outputs, String actionName, String userid, int taskTimeout)
    {
    	try
    	{
			if (StringUtils.isNotBlank(outputMappingScript))
			{
				Log.log.trace("Output mapping script: " + outputMappingScript);
				String scriptName = "output_mapping#" + actionName;
				
				//binding for expression
				Binding binding = new Binding();
                binding.setVariable(Constants.GROOVY_BINDING_ACTIONNAME, actionName);
                binding.setVariable(Constants.GROOVY_BINDING_PROCESSID, processid);
                binding.setVariable(Constants.GROOVY_BINDING_USERID, userid);

                // parameters
                binding.setVariable(Constants.GROOVY_BINDING_PARAMS, params);
                binding.setVariable(Constants.GROOVY_BINDING_FLOWS, flows);
                binding.setVariable(Constants.GROOVY_BINDING_INPUTS, inputs);
                binding.setVariable(Constants.GROOVY_BINDING_OUTPUTS, outputs);

                if (outputMappingScript.contains(Constants.GROOVY_BINDING_WSDATA))
                {
	                WSDataMap wsData = ServiceWorksheet.getWorksheetWSDATA(problemid);
	                binding.setVariable(Constants.GROOVY_BINDING_WSDATA, wsData);
                }
                
                if(Log.log.isTraceEnabled()) {
                    Log.log.trace("Output Mapping Script Name: " + scriptName + ", MD5: " + CryptMD5.encrypt(outputMappingScript));
                }
                GroovyScript.executeAsync(outputMappingScript, scriptName, true, binding, taskTimeout * 1000);
			}
    	}
    	catch(Exception e)
    	{
    		Log.log.error("ERROR: Output mapping could not be executed for task: " + actionName, e);
    	}
    }
    
    static void processSummaryAndDetail(String summaryRule, String detailRule, String processid, String problemid, Map params, Map flows, Map inputs, Map outputs, String raw, ActionTaskProperties properties, String actionName, AssessResult assessResult, String userid, int taskTimeout)
    {
    	List<Map<String, String>> ruleMapList = null;
    	String summary = null;
    	String detail = null;
    	
    	if (StringUtils.isNotBlank(summaryRule))
    	{
    		try
			{
				ruleMapList = new ObjectMapper().readValue(summaryRule, ArrayList.class);
			}
    		catch (IOException e)
			{
				Log.log.error("Could not parse summary rule.", e);
			}
    		
    		summary = evaluateSummaryAndDetailRule(ruleMapList, assessResult);
    	}
    	
    	if (StringUtils.isNotBlank(detailRule))
    	{
    		try
			{
				ruleMapList = new ObjectMapper().readValue(detailRule, ArrayList.class);
			}
    		catch (IOException e)
			{
				Log.log.error("Could not parse summary rule.", e);
			}
    		
    		detail = evaluateSummaryAndDetailRule(ruleMapList, assessResult);
    	}
    	
    	StringBuilder summaryAndDetailScript = new StringBuilder();
    	if (StringUtils.isNotBlank(summary))
    	{
    		summaryAndDetailScript.append("RESULT.summary =\"\"\"\\\n").append(summary).append("\n\"\"\"");
    	}
    	if (StringUtils.isNotBlank(detail))
    	{
    		summaryAndDetailScript.append("\nRESULT.detail =\"\"\"\\\n").append(detail).append("\"\"\"");
    	}
    	
    	try
    	{
			if (StringUtils.isNotBlank(summaryAndDetailScript.toString()))
			{
				Log.log.trace("Summary and Detail script: " + summaryAndDetailScript.toString());
				String scriptName = "summary_detail#" + actionName;
				
				//binding for expression
				Binding binding = new Binding();
                binding.setVariable(Constants.GROOVY_BINDING_ACTIONNAME, actionName);
                binding.setVariable(Constants.GROOVY_BINDING_PROCESSID, processid);
                binding.setVariable(Constants.GROOVY_BINDING_USERID, userid);
                binding.setVariable(Constants.GROOVY_BINDING_PROPERTIES, properties);

                // parameters
                binding.setVariable(Constants.GROOVY_BINDING_PARAMS, params);
                binding.setVariable(Constants.GROOVY_BINDING_FLOWS, flows);
                binding.setVariable(Constants.GROOVY_BINDING_INPUTS, inputs);
                binding.setVariable(Constants.GROOVY_BINDING_OUTPUTS, outputs);
                binding.setVariable(Constants.GROOVY_BINDING_RESULT, assessResult);
                binding.setVariable(Constants.GROOVY_BINDING_RAW, raw);

                if (summaryAndDetailScript.toString().contains(Constants.GROOVY_BINDING_WSDATA))
                {
	                WSDataMap wsData = ServiceWorksheet.getWorksheetWSDATA(problemid);
	                binding.setVariable(Constants.GROOVY_BINDING_WSDATA, wsData);
                }
                
                if(Log.log.isTraceEnabled()) {
                    Log.log.trace("Summary and detail Script Name: " + scriptName + ", MD5: " + CryptMD5.encrypt(summaryAndDetailScript.toString()));
                }
                GroovyScript.executeAsync(summaryAndDetailScript.toString(), scriptName, true, binding, taskTimeout * 1000);
			}
    	}
    	catch(Exception e)
    	{
    		Log.log.error("ERROR: Summary and detail script could not be executed for task: " + actionName, e);
    	}
    }
    
    private static String evaluateSummaryAndDetailRule(List<Map<String, String>> ruleMapList, AssessResult assessResult)
    {
    	String result = null;
    	if (ruleMapList != null && ruleMapList.size() > 0)
    	{
    		String resultCondition = (StringUtils.isBlank(assessResult.getCondition()) ? "" : assessResult.getCondition().toLowerCase());
    		if (!conditionList.contains(resultCondition))
			{
    			resultCondition = "any";
			}
    		
			String resultSeverity = (StringUtils.isBlank(assessResult.getSeverity()) ? "" : assessResult.getSeverity().toLowerCase());
			if (!severityList.contains(resultSeverity))
			{
				resultSeverity = "any";
			}
			
	    	for (Map<String, String> ruleMap : ruleMapList)
			{
				String ruleCondition = (StringUtils.isBlank(ruleMap.get("condition")) ? "" : ruleMap.get("condition").toLowerCase());
				if (!conditionList.contains(ruleCondition))
				{
					ruleCondition = "any";
				}
				
				String ruleSeverity = (StringUtils.isBlank(ruleMap.get("severity")) ? "" : ruleMap.get("severity").toLowerCase());
				if (!severityList.contains(ruleSeverity))
				{
					ruleSeverity = "any";
				}
				
				if ( (ruleCondition.equals("any") || resultCondition.equals(ruleCondition)) && 
						(ruleSeverity.equals("any") || resultSeverity.equals(ruleSeverity)) )
				{
					result = ruleMap.get("display");
					break;
				}
			}
    	}
    	
    	return result;
    }

    static void setOutputParameters(String processid, String problemid, String executeid, Map inputs, Map outputs, Map params, Map flows) throws JsonProcessingException, IOException
    {
        WSDataMap wsData = null;
        if (!StringUtils.isEmpty(processid))
        {
            Collection<ExecuteParameter> reqParams = ProcessExecution.executeParameterStateCache
            										 .getParameters(processid, executeid);
            if (reqParams != null)
            {
                Map allOutputs = new HashMap();
                for (ExecuteParameter reqParam : reqParams) {
                    if(reqParam.getType().equals("OUTPUT")) {
                        allOutputs.put(reqParam.getName(), reqParam.getValue());
                    }
                }
                allOutputs.putAll(params);
                allOutputs.putAll(inputs);
                allOutputs.putAll(flows);
                for (ExecuteParameter reqParam : reqParams)
                {
                    String name = reqParam.getName();
                    String value = reqParam.getValue();

                    NameTypeValue ntv = VariableUtil.getNameTypeValue(value);
                    try {
	                    if (ntv != null && !reqParam.getType().equalsIgnoreCase("INPUT"))
	                    {
	                        if (!ntv.getType().equals("INPUT"))
	                        {
	                        	Map<String, Object> wsDataMap = null;
	                        	if (ntv.getName().contains("WSDATA") && wsData == null) {
	                        		wsData = ServiceWorksheet.getWorksheetWSDATA(problemid);
	                        		wsDataMap = ((WSDataMapImpl) wsData).getDataMap();
	                        	}
	                            Object result = VariableUtil.replaceVarsNested(name,ntv.getName(),wsDataMap,inputs, outputs, params, flows, ((Map)new HashMap()),allOutputs );
	                            ntv.setName((String)result);
	                        }
	                        
	                        if (ntv.getType().equals("PARAM"))
	                        {
	                            if (outputs.containsKey(name))
	                            {
	                                params.put(ntv.getName(), outputs.get(name));
	                            }
	
	                        }
	                        else if (ntv.getType().equals("INPUT"))
	                        {
	                            Log.log.warn("Cannot set INPUTS directly from OUTPUT mapping. INPUTS must be defined from INPUT parameters");
	                            // inputs.put(ntv.getName(), outputs.get(name));
	                        }
	                        else if (ntv.getType().equals("OUTPUT"))
	                        {
	                            if (outputs.containsKey(name))
	                            {
	                                outputs.put(ntv.getName(), outputs.get(name));
	                            }
	                        }
	                        else if (ntv.getType().equals("FLOW"))
	                        {
	                            if (outputs.containsKey(name))
	                            {
	                                flows.put(ntv.getName(), outputs.get(name));
	                            }
	                        }
	                        else if (ntv.getType().equals("WSDATA"))
	                        {
	                            if (outputs.containsKey(name))
	                            {
	                            	if (wsData == null) {
	                            		wsData = ServiceWorksheet.getWorksheetWSDATA(problemid);
	                            	}
	                                wsData.put(ntv.getName(), outputs.get(name));
	                            }
	                        }
	                    }
                    } 
                    catch(Exception e)
                    {
                    	Log.log.error("Error Mapping Outputs: " + e.getMessage(), e);
                    }
                }
            }
        }
        // DT Auto answer support (RBA-14145)
        if (params.get(Constants.DT_AUTO_ANSWER_VARIABLES_JSON) != null && outputs != null && outputs.size() > 0)
        {
            String dtVariablesString = (String) params.get(Constants.DT_AUTO_ANSWER_VARIABLES_JSON);
            ObjectMapper mapper = new ObjectMapper();
            JsonNode dtVariables = mapper.readTree(dtVariablesString);
            if (dtVariables.isArray())
            {
                String action = (String) params.get(Constants.HTTP_REQUEST_ACTION);
                String taskName = null;
                if (action != null && action.equals("TASK"))
                {
                    taskName = (String) params.get(Constants.EXECUTE_ACTIONNAME);
                }
                else if (action != null && action.equals("EXECUTEPROCESS"))
                {
                    taskName = (String) params.get(Constants.EXECUTE_WIKI);
                }
                for (JsonNode dtVariable : dtVariables)
                {
                    String wsDataKey = dtVariable.get("variableName").getTextValue();
                    String wsTaskName = dtVariable.get("taskName").getTextValue();
                    String wsOutputName = dtVariable.get("outputName").getTextValue();
                    if (wsTaskName.equals(taskName) && outputs.containsKey(wsOutputName))
                    {
                    	if (wsData == null) {
                    		wsData = ServiceWorksheet.getWorksheetWSDATA(problemid);
                    	}
                        wsData.put(wsDataKey, outputs.get(wsOutputName));
                    }
                }
            }
        }
    } // setOutputParameters
    
    static String renderActionResultRaw(String logRawResult, String raw)
    {
        String value = "";
        
        if (!logRawResult.equalsIgnoreCase("TRUE"))
        {
            value = "Raw results not logged";
        }
        else
        {
            value = removeControlChars(raw);
        }
        return value;
    } // renderActionResultRaw

    static String renderActionResultSummary(String problemid, AssessResult assessment, String actionName, String mock)
    {
        String result = "";
        
        // process summary content
        if (StringUtils.isEmpty(assessment.getSummary()))
        {
            // RBA-14570 : AT, when Mock doesn't have any data in RAW section, it still executes script in Content section
            if (StringUtils.isEmpty(mock))
            {
                result = actionName.toLowerCase() + " results";
                assessment.setSummary(result);
            }
            else
            {
                result = assessment.getSummary();
            }
        }
        else
        {
            // remove control characeters
            result = removeControlChars(assessment.getSummary());
        }
        return result;
    } // renderActionResultSummary

    static String renderActionResultDetail(String problemid, AssessResult assessment)
    {
        return removeControlChars(assessment.getDetail());
    } // renderActionResultDetail
    
    static String removeControlChars(String appendEscapedText)
    {
        return appendEscapedText.replaceAll("[\\p{C}&&[^\\s]]", "");
    } // removeControlChars
    
    @SuppressWarnings("unchecked")
    static void logActionResult(String problemid, String executeid, String userid, String targetAddr, String targetGuid, 
    							String affinity, String actionid, String processid, AssessResult assessment, 
    							String actionName, String actionNamespace, String actionSummary, String actionHidden, 
    							String actionTags, String actionRoles, String command, String returncode, 
    							String actionResultGUID, String raw, String logRawResult, String mock, 
    							String activityId, Map params, String nodeid, String wiki, String execReqNum, Long timeStamp) throws Exception
    {
        //we don't need results in the case where we want maximum performance
        if(!MainBase.main.configGeneral.isLogActionResults()) {
            return;
        }
        
        Map<String,Object> arMap = new HashMap<String,Object>();
        
        //This call is expensive during heavy load
        //ResolveActionTaskVO actionTask = ServiceHibernate.getResolveActionTask(actionid, null, "system");
        String summary  = renderActionResultSummary(problemid, assessment, actionName, mock);
        String detail = renderActionResultDetail(problemid, assessment);
        String encodedRaw = renderActionResultRaw(logRawResult, raw);

        
        // add resolve_action_result
        arMap.put("sys_created_by", userid);
        arMap.put("u_problem", problemid);

        if (processid == null)
        {
            processid = "";
        }
        arMap.put("u_process", processid);

        arMap.put("u_execute_request_id", executeid);
        arMap.put("u_execute_result_id", "");

        arMap.put("u_actiontask", actionid);
        arMap.put("u_actiontask_name", actionName);
        //unused from UI perpective
        //arMap.put("u_actiontask_full_name", actionTask.getUFullName());
        arMap.put("u_actiontask_full_name", "");
        arMap.put("u_address", targetAddr);

        arMap.put("u_target_guid", actionResultGUID);
        
        String esbAddr = StringUtils.isNotEmpty(affinity) ? affinity : targetGuid;
        arMap.put("u_esbaddr", esbAddr);

        String completion = (assessment.isCompleted() + "").toUpperCase();
        arMap.put("u_completion", completion);
        arMap.put("u_condition", assessment.getCondition());
        arMap.put("u_severity", assessment.getSeverity());
        arMap.put("u_duration", assessment.getDuration());

        arMap.put("u_timestamp", timeStamp);

        arMap.put("u_summary", summary);
        
        ProcessState ps = StringUtils.isNotEmpty(processid) ? ProcessExecution.getProcessState(processid) : null;
        
        String actiontaskId = executeid;
        int firstIndx = executeid.indexOf(Constants.EXECUTE_REQUEST_NODEID_DELIMITER);
        
        if (firstIndx > 0)
        {
            int lastIndx = executeid.lastIndexOf(Constants.EXECUTE_REQUEST_NODEID_DELIMITER);
            
            if (lastIndx > (firstIndx + 1))
            {
                actiontaskId = executeid.substring(firstIndx + Constants.EXECUTE_REQUEST_NODEID_DELIMITER.length(), lastIndx);
            }
        }
        
        String preConditionErrMsg = null;
        
        if (ps != null)
        {
            preConditionErrMsg = ps.getAndClearPreConditionError/*getPreConditionError*/(actiontaskId);
        }
        
        String detailSuffix = "";
        
        if (StringUtils.isNotBlank(preConditionErrMsg))
        {
            detailSuffix = "\n\nPreCondition WARNING\n\n" + preConditionErrMsg;
            
            if (assessment.getSeverity().contains(Constants.ASSESS_SEVERITY_GOOD) ||
                assessment.getSeverity().contains(Constants.ASSESS_SEVERITY_UNKNOWN))
            {
                arMap.put("u_severity", Constants.ASSESS_SEVERITY_WARNING);
            }
            
            if (!assessment.getCondition().contains(Constants.ASSESS_CONDITION_BAD))
            {
                arMap.put("u_condition", Constants.ASSESS_CONDITION_UNKNOWN);
            }
            
            if (ProcessState.severityIsLT(Constants.ASSESS_SEVERITY_WARNING, ps.getWorstSeverity()))
            {
                ps.setWorstSeverity(Constants.ASSESS_SEVERITY_WARNING);
            }
            
            if (ProcessState.conditionIsLT(Constants.ASSESS_CONDITION_UNKNOWN, ps.getWorstCondition()))
            {
                ps.setWorstCondition(Constants.ASSESS_CONDITION_UNKNOWN);
            }
        }
        
        arMap.put("u_detail", detail + detailSuffix);
        arMap.put("u_raw", encodedRaw);
        
        String processNum = "";
        if (StringUtils.isNotEmpty(processid))
        {
        	// Get process number from process map if available
            Map<String, Object> processMap = ProcessContext.getProcessMap(processid);
            
            if (processMap != null)
            {
            	ProcessState procState = (ProcessState)processMap.get(Constants.PROCESS_MAP_PROCESS_STATE);
            	
            	if (procState != null) {
            		processNum = procState.getProcessNumber();
            	}
            }
            
            if (StringUtils.isBlank(processNum)) {
            	//this makes sure if both process number and process id is provided
                //we will use the number based on the process id because the provided
                //process number may be wrong.
                com.resolve.search.model.ProcessRequest processRequest = ServiceWorksheet.findProcessRequestById(processid);
                
                if (processRequest != null)
                {
                    processNum = processRequest.getNumber();
                }
            }
            
        }

        String problemNum = "";
        if (StringUtils.isNotEmpty(problemid))
        {
            //this makes sure if problem id is provided we will retrieve the number 
            //based on the problem id.
            Worksheet worksheet = ServiceWorksheet.findWorksheetById(problemid);
            
            if (worksheet != null) {
            	problemNum = worksheet.getNumber();
            } else {
            	throw new RuntimeException(String.format("Can't find Worksheet for specified problemid: %s", problemid));
            }
            
        }

        arMap.put("u_problem", problemid);
        arMap.put("u_wiki", wiki);
        arMap.put("u_problem_num", problemNum);

        arMap.put("u_timestamp", timeStamp);
        arMap.put("u_taskname", actionName);
        //TODO not necessary, refactor later so no need for blank string also.
        //arMap.put("u_task_fullname", actionTask.getUFullName());
        arMap.put("u_task_fullname", actionName + "#" + actionNamespace);
            
        arMap.put("u_taskid", actionid);
        arMap.put("u_task_namespace", actionNamespace);
        arMap.put("u_task_summary", actionSummary);
        arMap.put("u_task_hidden", actionHidden);
        arMap.put("u_task_tags", actionTags);
        arMap.put("u_task_roles", actionRoles);
        
        arMap.put("u_completion", completion);
        
        arMap.put("u_summary", summary);
        arMap.put("u_process_number", processNum);
        arMap.put("u_process_id", processid);

        arMap.put("u_execute_request_number", execReqNum);
        arMap.put("u_execute_request_id", executeid);
//        arMap.put("u_node_id", execReqCols.getColumnByName("u_node_id").getStringValue());
        arMap.put("u_node_id", nodeid);

        arMap.put("u_duration",  assessment.getDuration());
        arMap.put("u_address", targetAddr);
        arMap.put("u_esbaddr", esbAddr);

        arMap.put("u_execute_result_command", command);
        arMap.put("u_execute_result_returncode", returncode);
        arMap.put("u_execute_result_id", "");

        arMap.put("sys_updated_by", userid);
        
        arMap.put("activityId", activityId);
        
        boolean updateSir = ((Main) Main.main).configGeneral.getUpdateSir();
		ServiceWorksheet.createNewActionResult(arMap, updateSir);
        
    } // logActionResult
    

    static XDoc logWorksheet(String problemid, String processid, String executeid, String userid, 
    						 Map problemRecord, ScriptDebug debug, String condition, String severity, 
    						 String actionName, Map params) throws Exception
    {

        XDoc result = null;

        boolean hasUpdate = false;

        // lock worksheet (only for process)
        if (!StringUtils.isEmpty(problemid))
        {
            ReentrantLock lock = null;
            if (!StringUtils.isEmpty(processid))
            {
                lock = LockUtils.getLock(Constants.LOCK_LOGWORKSHEET + problemid, processid);
                lock.lock();
            }
            Worksheet cl = null;
            try
            {
                // check if problemRecord has content
		        if (problemRecord != null && problemRecord.size() > 0)
		        {
		            for (Object key : problemRecord.keySet())
                    {
                        if (problemRecord.get(key) == null)
                        {
                            problemRecord.put(key, "");
                        }
                        if (!(problemRecord.get(key) instanceof String))
                        {
                            problemRecord.put(key, problemRecord.get(key).toString());
                        }
                    }
		            hasUpdate = true;
                    //check if problemRecord has u_reference and if value is over column size (333)
		            if(problemRecord.containsKey("u_reference"))
		            {
		                String ureference = (String) problemRecord.get("u_reference");
		                if(ureference.length() > 333)
		                {
		                    ureference = ureference.substring(0,333);
		                    problemRecord.put("u_reference", ureference);
		                    if (Log.log.isDebugEnabled()) {
		                        Log.log.debug("Data too long for column 'u_reference'. Truncating to 333 characters");
		                    }
		                }
		            }
		        }

                // check if end of runbook process
                boolean isProcessEnd = false;
                if (actionName.equalsIgnoreCase(Constants.EXECUTE_END))
                {
                    String end_quit = (String) params.get(Constants.EXECUTE_END_QUIT);
                    if (end_quit != null && end_quit.equalsIgnoreCase("TRUE"))
                    {
                        isProcessEnd = true;
                        hasUpdate = true;
                    }
                }

                // update db records
                if (hasUpdate)
                {
                    if (cl == null)
                    {
                        cl = ServiceWorksheet.findWorksheetById(problemid, userid);
                    }
                    
                    if (cl == null) {
                    	String errMsg = String.format("Failed to find worksheet by id: %s", problemid);
                    	Log.log.error(errMsg);
                    	throw new RuntimeException(errMsg);
                    }

                    if(Log.log.isDebugEnabled())
                    {
                        Log.log.debug(String.format("Updating worksheet condition: %s severity: %s processid: %s " +
                        							"isProcessEnd: %b worksheet ucondition: %s useverity: %s", 
                        							condition, severity, processid, isProcessEnd,
                        							cl.getCondition(), cl.getSeverity()));
                    }
                    
                    // set condition and severity if process END
                    if (isProcessEnd)
                    {
                        if (StringUtils.isEmpty((cl.getCondition() != null ? cl.getCondition() : null)) && StringUtils.isEmpty(cl.getSeverity() != null ? cl.getSeverity() : null))
                        {
                            if (problemRecord == null)
                            {
                                problemRecord = new HashMap();
                            }
                            problemRecord.put("u_condition", condition);
                            problemRecord.put("u_severity", severity);
                        }
                        
                        String incidentId = cl.getIncidentId();
                        if(incidentId != null && !incidentId.isEmpty()) {
                            String rootResolutionId = (String) params.get(Constants.EXECUTE_ROOT_RUNBOOK_ID);
                            
                            String wiki = (String)params.get(Constants.EXECUTE_WIKI);
                            boolean isAborProcess = false;
                            if(wiki != null && wiki.startsWith(Constants.EXECUTE_ABORTWIKI)){
                                isAborProcess = true;
                            }
                            
                            if(condition.equalsIgnoreCase(Constants.ASSESS_CONDITION_GOOD)) {
                                EventMessage em = EventMessageFactory.getInstance().getIncidentResolutionCompletedMessage(incidentId, rootResolutionId);
                                ((Main) MainBase.main).esb.sendInternalMessage(Constants.ESB_NAME_EVENT_SOURCING_DCS, "", em.toJSON());
                                
                                if(!isAborProcess) {
                                    EventMessage incidentCompletedMessage = EventMessageFactory.getInstance().getIncidentCompletedMessage(incidentId);
                                    Main.esb.sendInternalMessage(Constants.ESB_NAME_EVENT_SOURCING_DCS, "", incidentCompletedMessage.toJSON());
                                }
                                
                            } else if (condition.equalsIgnoreCase(Constants.ASSESS_CONDITION_BAD)){
                                EventMessage em = EventMessageFactory.getInstance().getIncidentResolutionFailedMessage(incidentId, rootResolutionId);
                                Main.esb.sendInternalMessage(Constants.ESB_NAME_EVENT_SOURCING_DCS, "", em.toJSON());

                                if(!isAborProcess) {
                                    EventMessage incidentFailedMessage = EventMessageFactory.getInstance().getIncidentFailedMessage(incidentId);
                                    Main.esb.sendInternalMessage(Constants.ESB_NAME_EVENT_SOURCING_DCS, "", incidentFailedMessage.toJSON());
                                }
                               
                            }
                        }
                    }

                    // set problem fields (optional)
                    if (problemRecord != null && problemRecord.size() > 0)
                    {
                        String rootResolutionId = (String) params.get(Constants.EXECUTE_ROOT_RUNBOOK_ID);
                        if(cl.getIncidentId()!= null && !cl.getIncidentId().isEmpty()) {
                            EventMessage em = EventMessageFactory.getInstance().getIncidentResolutionUpdatedMessage(cl.getIncidentId(), rootResolutionId, cl.getSummary(), condition, severity);
                            MainBase.esb.sendInternalMessage(Constants.ESB_NAME_EVENT_SOURCING_DCS, "", em.toJSON());
                        }
                        ServiceWorksheet.updateWorksheet(problemid, problemRecord, userid);
                    }
                }
            }
            catch (Exception e)
            {
                Log.log.error(String.format("Failed to log worksheet: %s", e.getMessage()), e);
                throw new RuntimeException(e);
            }
            finally
            {
              if (!StringUtils.isEmpty(processid))
              {
                  lock.unlock();
              }
            }
        }
        
        return result;
    } // logWorksheet

    static String getCompletionColor(AssessResult assessment)
    {
        String color = "green";

        if (assessment != null)
        {
            if (assessment.isCompleted() == false)
            {
                color = "red";
            }
        }
        return "images/colorball_" + color + ".gif";
    } // getCompletionColor

    static String getConditionColor(AssessResult assessment)
    {
        String color = "grey";

        if (assessment != null && assessment.getCondition() != null)
        {
            if (assessment.getCondition().equals(Constants.ASSESS_CONDITION_GOOD))
            {
                color = "green";
            }
            else if (assessment.getCondition().equals(Constants.ASSESS_CONDITION_BAD))
            {
                color = "red";
            }
        }
        return "images/colorball_" + color + ".gif";
    } // getConditionColor

    static String getSeverityColor(AssessResult assessment)
    {
        String color = "grey";

        if (assessment != null && assessment.getSeverity() != null)
        {
            if (assessment.getSeverity().equals(Constants.ASSESS_SEVERITY_CRITICAL))
            {
                color = "red";
            }
            else if (assessment.getSeverity().equals(Constants.ASSESS_SEVERITY_SEVERE))
            {
                color = "orange";
            }
            else if (assessment.getSeverity().equals(Constants.ASSESS_SEVERITY_WARNING))
            {
                color = "yellow";
            }
            else if (assessment.getSeverity().equals(Constants.ASSESS_SEVERITY_GOOD))
            {
                color = "green";
            }
        }
        return "images/colorball_" + color + ".gif";
    } // getConditionColor

    String encodePre(String text)
    {
        String result = text;

        result = result.replaceAll("<", "&#60;");
        result = result.replaceAll(">", "&#62;");

        return result;
    } // encodePre

    static String escapeForXMLParser(String text)
    {
        String result = text;

        // escape value to be compatible with javax.xml.parser
        result = result.replaceAll("&", "&#38;");
        result = result.replaceAll("&#38;nbsp;", "&#160;");
        result = result.replaceAll("&#38;lt;", "&#60;");
        result = result.replaceAll("&#38;gt;", "&#62;");

        return result;
    } // escapeForXMLParser

    static String removeInvalidXMLValues(String text)
    {
        String result = text;

        // escape value to be compatible with javax.xml.parser
        result = result.replaceAll("\u0000", "");

        return result;
    } // escapeForXMLParser

    void processConfig(String guid, Map properties) throws Exception
    {
        MConfig.updateProperties(guid, properties);
    } // processConfig

    String addDefaultScriptImports(String script)
    {
        String result = null;

        if (script != null)
        {
            String imports = "";
            imports += "import com.resolve.util.Constants;\n";
            imports += "import com.resolve.util.StringUtils;\n";
            imports += "import com.resolve.html.Table;\n";
            imports += "\n";
            result = imports + script;
        }

        return result;
    } // addDefaultScriptImports

} // MResult
