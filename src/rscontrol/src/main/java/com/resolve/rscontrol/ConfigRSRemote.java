package com.resolve.rscontrol;

import java.io.File;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.Arrays;

import org.dom4j.DocumentException;
import org.xml.sax.SAXException;

import com.resolve.util.Constants;
import com.resolve.util.StringUtils;
import com.resolve.util.XDoc;

public class ConfigRSRemote
{
	/*
    public static void main(String[] args) throws Exception
    {
//        System.out.println(args[0]);
        ConfigRSRemote config = new ConfigRSRemote();
        config.load(new File(args[0]));
        System.out.println(config.validQueues);
    }*/
    
    private Map<String, Set<String>> validQueues = new HashMap<String, Set<String>>();
    private String org;
    
    public ConfigRSRemote() {
    }
    public void load(String xmlConfig) throws DocumentException, SAXException {
       load(new XDoc(xmlConfig)); 
    }
    
    public void load(File xmlConfig) throws Exception {
        load(new XDoc(xmlConfig));
    }
    
    public synchronized void load(XDoc xmlConfigDoc) {
        List generalAttribs = xmlConfigDoc.getListMapValue("./GENERAL");
        Set<String> orgs = getQueueValue(generalAttribs, "ORG", null);
        String orgSuffix = "";
                        
        if (orgs != null && !orgs.isEmpty())
        {
            org = orgs.iterator().next();
            orgSuffix = org.replaceAll("[^0-9A-Za-z_]", "").toUpperCase();
        }
        else
            org = null;
        
        List esbQueues = xmlConfigDoc.getListMapValue("./ESB/QUEUE");
        List receiveQueues = xmlConfigDoc.getListMapValue("./RECEIVE/*");
        List idQueue = xmlConfigDoc.getListMapValue("./ID");
        Set<String> ebsQueueNames = getQueueValue(esbQueues, "NAME", null);
        
        Set<String> ebsQueueNamesWithOrgSuffix = new HashSet<String>();
        
        if (ebsQueueNames != null && !ebsQueueNames.isEmpty())
        {
            for (String esbQueueName : ebsQueueNames)
            {
                ebsQueueNamesWithOrgSuffix.add(esbQueueName.equals(Constants.ESB_NAME_RSREMOTE) ? esbQueueName + orgSuffix : esbQueueName);
            }
        }
        
        Set<String> receiveQueueNames = getQueueValue(receiveQueues, "QUEUE", "ACTIVE");
        Set<String> receiveWorkerQueueNames = getQueueValueWithSuffix(receiveQueues, "QUEUE", "_WORKER", Arrays.asList("ACTIVE", "WORKER"));
        Set<String> rcvQueueNamesWithOrgSuffix = new HashSet<String>();
        
        if (receiveQueueNames != null && !receiveQueueNames.isEmpty())
        {
            for (String rcvQueueName : receiveQueueNames)
            {
                rcvQueueNamesWithOrgSuffix.add(rcvQueueName + orgSuffix);
            }
        }
        
        String queueId = getQueueValue(idQueue, "GUID", null).iterator().next();
        Set<String> rsRemoteQueues = validQueues.get(queueId);
        if(rsRemoteQueues == null) {
            rsRemoteQueues = new HashSet<String>();
            validQueues.put(queueId, rsRemoteQueues);
            rsRemoteQueues.add(queueId);
            rsRemoteQueues.add("RSVIEW");
        }
        rsRemoteQueues.addAll(ebsQueueNamesWithOrgSuffix);
        rsRemoteQueues.addAll(rcvQueueNamesWithOrgSuffix);
		rsRemoteQueues.addAll(receiveWorkerQueueNames);
    }
    
    /**
     * return true if there is at least a valid queue register under this guid. this is indirectly indicated this rsremote with guid is active
     * @param guid
     * @return
     */
    public boolean isRSRemoteActive(String guid) {
        Set<String> validQueue = validQueues.get(guid);
        return validQueue != null && !validQueue.isEmpty();
    }
    
    public Set<String> getValidQueue(String guid) {
        return validQueues.get(guid);
    }
	
	private Set<String> getQueueValueWithSuffix(List queues, String queueAttributeName, String queueSuffix, List<String> activeAttributes) {
		Set<String> queueNames = new HashSet<String>();
        for(Object queue : queues) {
            Map queueMap = (Map) queue;
            Boolean isActive = true;
            if(activeAttributes != null) {
				for(String activeAttributeName : activeAttributes) {
					String activeString = (String) queueMap.get(activeAttributeName);
					isActive = StringUtils.isEmpty(activeString) ? false : activeString.equalsIgnoreCase("true");
					if(!isActive) {
						break;
					}
 				}
            }
            if(!isActive) {
                continue;
            }
            String queueName = (String) queueMap.get(queueAttributeName);
            if(!StringUtils.isEmpty(queueName)) {
                queueNames.add(queueName.toUpperCase()+queueSuffix);
            }
        }
        return queueNames;
	}
    
    private Set<String> getQueueValue(List queues, String queueAttributeName, String activeAttributeName) {
        Set<String> queueNames = new HashSet<String>();
        for(Object queue : queues) {
            Map queueMap = (Map) queue;
            Boolean isActive = true;
            if(activeAttributeName != null) {
                String activeString = (String) queueMap.get(activeAttributeName);
                isActive = StringUtils.isEmpty(activeString) ? false : activeString.equalsIgnoreCase("true");
            }
            if(!isActive) {
                continue;
            }
            String queueName = (String) queueMap.get(queueAttributeName);
            if(!StringUtils.isEmpty(queueName)) {
                queueNames.add(queueName.toUpperCase());
            }
        }
        return queueNames;
    }
    public void load(String guid, Set<String> validQueue, String org)
    {
        validQueues.put(guid, validQueue);
        this.org = org;
    }
    
    public boolean isValidQueue(String guid, String queueName) {
        if(queueName != null) {
            Set<String> validQueue = validQueues.get(guid);
            return validQueue != null && validQueue.contains(queueName.toUpperCase());
        }
        return false;
    }
    
    public boolean isValidQueue(String queueName) {
        for(String guid : validQueues.keySet()) {
            if(isValidQueue(guid, queueName)) {
                return true;
            }
        }
        return false;
    }
    
    public boolean removeValidQueue(String guid) {
        return validQueues.remove(guid) != null;
    }
    
    public String toString() {
        StringBuilder sb = new StringBuilder();
        for(String guid : validQueues.keySet()) {
            sb.append("{" + guid + (StringUtils.isNotBlank(org) ? "(" + org + "): " : "): ") + validQueues.get(guid) +"}\n\r");
        }
        
        return sb.toString();
    }
    public Map<String, Set<String>> getValidQueues()
    {
        return validQueues;
    }
    
    public String getOrg()
    {
        return org;
    }
}
