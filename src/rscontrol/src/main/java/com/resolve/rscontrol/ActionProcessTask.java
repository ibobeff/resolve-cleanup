/******************************************************************************
* (C) Copyright 2014
*
* Resolve Systems, LLC
*
* All rights reserved
* This software is distributed under license and may not be used, copied,
* modified, or distributed without the express written permission of
* Resolve Systems, LLC.
*
******************************************************************************/

package com.resolve.rscontrol;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.resolve.util.Constants;
import com.resolve.util.DirectedTreeNode;
import com.resolve.util.Log;

public class ActionProcessTask
{
    String id;
    String label;
    String name;
    String namespace;
    String prefix;
    String task_sid;
    Map<String, String> params;
    Map<? extends Object, ? extends Object> outputs;
    String expression;
    String condition;
    String wiki;
    String eventid;
    Map<? extends Object, List<? extends Object>> postConditions; // child id => list of conditions
    Map<String, Map<String, String>> postExpressions; // child id => map of expressions by root preconediton id 
    Map<? extends Object, ? extends Object> postEvaluations; // child id => evaluation
    DirectedTreeNode graphNode;
    List<ActionProcessTaskDependency> depends = new ArrayList<ActionProcessTaskDependency>();
    
    public ActionProcessTask(String id, DirectedTreeNode d) 
    {
        this.id = id;
        graphNode = d;
    } // ActionProcessTask
   
    public List<ActionProcessTaskDependency> getDepends()
    {
    	return depends;
    }
    
    public String toString()
    {
        return this.name+"#"+namespace;
    }
    
    public String getId()
    {
        return id;
    }
    public String getName()
    {
        return name;
    }
    public void setName(String name)
    {
        this.name = name;
    }
    public String getLabel()
    {
        return label;
    }
    public void setLabel(String label)
    {
        this.label = label;
    }
    public String getNamespace()
    {
        return namespace;
    }
    public void setNamespace(String namespace)
    {
        this.namespace = namespace;
    }
    public String getWiki()
    {
        return wiki;
    }
    public void setWiki(String wiki)
    {
        this.wiki = wiki;
    }
    public String getPrefix()
    {
        return prefix;
    }
    public void setPrefix(String prefix)
    {
        this.prefix = prefix;
    }
    public String getTaskSid()
    {
        return task_sid;
    }
    public void setTaskSid(String task_sid)
    {
        this.task_sid = task_sid;
    }
    public Map<? extends Object, ? extends Object> getOutputs()
    {
        return outputs;
    }
    public void setOutputs(Map<? extends Object, ? extends Object> outputs)
    {
        this.outputs = outputs;
    }

    public Map<String, String> getParams()
    {
        return params;
    }
    public void setParams(Map<String, String> params)
    {
        this.params = params;
    }
    public void setParam(String name, String value)
    {
        if (this.params == null)
        {
            this.params = new HashMap/*table*/<String, String>();
        }
        this.params.put(name, value);
    }
    public String getEventId()
    {
        return eventid;
    }
    public void setEventId(String eventid)
    {
        this.eventid = eventid;
    }
    public String getCondition()
    {
        return condition;
    }
    public void setCondition(String condition)
    {
        this.condition = condition;
    }
    public String getExpression()
    {
        return expression;
    }
    public void setExpression(String expression)
    {
        this.expression = expression;
    }
    public Map getPostConditions()
    {
        return postConditions;
    }
    public void setPostConditions(Map postConditions)
    {
        this.postConditions = postConditions;
    }
    public Map<String, Map<String, String>> getPostExpressions()
    {
        return postExpressions;
    }
    public void setPostExpressions(Map<String, Map<String, String>> postExpressions)
    {
        this.postExpressions = postExpressions;
    }
    public Map getPostEvaluations()
    {
        return postEvaluations;
    }
    public void setPostEvaluations(Map postEvaluations)
    {
        this.postEvaluations = postEvaluations;
    }
} // ActionTask
