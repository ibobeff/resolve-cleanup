/******************************************************************************
* (C) Copyright 2014
*
* Resolve Systems, LLC
*
* All rights reserved
* This software is distributed under license and may not be used, copied,
* modified, or distributed without the express written permission of
* Resolve Systems, LLC.
*
******************************************************************************/

package com.resolve.rscontrol;

import com.resolve.persistence.model.TSRMFilter;

public class MTSRM extends MGateway
{
    private static final String MODEL_NAME = TSRMFilter.class.getSimpleName();

    protected String getModelName()
    {
        return MODEL_NAME;
    }
} // MTSRM
