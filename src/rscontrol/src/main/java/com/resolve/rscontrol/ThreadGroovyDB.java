/******************************************************************************
* (C) Copyright 2014
*
* Resolve Systems, LLC
*
* All rights reserved
* This software is distributed under license and may not be used, copied,
* modified, or distributed without the express written permission of
* Resolve Systems, LLC.
*
******************************************************************************/

package com.resolve.rscontrol;

import groovy.lang.Binding;

import java.util.Map;

import com.resolve.groovy.ThreadGroovyInterface;
import com.resolve.services.ServiceHibernate;
import com.resolve.util.constants.HibernateConstants;
import com.resolve.util.ResultCode;

public class ThreadGroovyDB implements ThreadGroovyInterface
{
    final static String RC = "RC";
    final static String RC_EXCEPTION = "EXCEPTION";
    
    Binding binding;
    Object[] args;
    String script;
    String scriptName;
    boolean cacheable;
    Object result;
    ResultCode rc;
    boolean isException;
    
    public ThreadGroovyDB() throws Exception
    {
        this.script = null;
        this.scriptName = null;
        this.cacheable = false;
        this.binding = null;
        this.args = null;
        this.isException = false;
        this.rc = new ResultCode();
    } // ThreadGroovyDB
    
    public ThreadGroovyDB(String script, String scriptName, boolean cacheable, Binding binding, Object[] args) throws Exception
    {
        this.script = script;
        this.scriptName = scriptName;
        this.binding = binding;
        this.cacheable = cacheable;
        this.args = args;
        this.isException = false;
        this.rc = new ResultCode();
    } // ThreadGroovyDB
    
    public void run() 
    {
        Map<String, Object> resultMap = ServiceHibernate.executeGroovy(script, scriptName, args, binding, rc, cacheable, isException);
        
        isException = (Boolean) resultMap.get(HibernateConstants.GROOVY_EXECEPTION);
        result = (Object) resultMap.get(HibernateConstants.GROOVY_OUTPUT_RESULT);

    } // run
    
    public Object getResult()
    {
        return result;
    } // getResult
    
    public boolean isException()
    {
        return isException;
    } // isException

    public Binding getBinding()
    {
        return binding;
    }

    public void setBinding(Binding binding)
    {
        this.binding = binding;
    }

    public Object[] getArgs()
    {
        return args;
    }

    public void setArgs(Object[] args)
    {
        this.args = args;
    }

    public String getScript()
    {
        return script;
    }
    
    public String getScriptName()
    {
        return scriptName;
    }

    public void setScript(String script)
    {
        this.script = script;
    }
    
    public void setScriptName(String scriptName)
    {
        this.scriptName = scriptName;
    }

    public boolean isCacheable()
    {
        return cacheable;
    }

    public void setCacheable(boolean cacheable)
    {
        this.cacheable = cacheable;
    }
    
} // ThreadGroovyDB