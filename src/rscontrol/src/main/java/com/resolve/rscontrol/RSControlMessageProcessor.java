/******************************************************************************
* (C) Copyright 2014
*
* Resolve Systems, LLC
*
* All rights reserved
* This software is distributed under license and may not be used, copied,
* modified, or distributed without the express written permission of
* Resolve Systems, LLC.
*
******************************************************************************/

package com.resolve.rscontrol;

import java.util.concurrent.TimeUnit;
import com.resolve.esb.DefaultMessageProcessor;
import com.resolve.esb.MMsgHeader;
import com.resolve.esb.MServiceThread;
import com.resolve.rsbase.MainBase;
import com.resolve.thread.ScheduledExecutor;
import com.resolve.thread.TaskExecutor;
import com.resolve.util.Constants;
import com.resolve.util.Log;
import com.resolve.util.StringUtils;

public class RSControlMessageProcessor extends DefaultMessageProcessor
{
    @Override
    public void processMessage(MMsgHeader msgHeader, String messageId, Object message)
    {
        if (msgHeader != null)
        {
            String classMethod = msgHeader.getDest().getClassMethod();
            String className = msgHeader.getDest().getClassname();
            String methodName = msgHeader.getDest().getMethodname();
            String delay = msgHeader.getOptions().getDelay();

            long delayTime = 0;
            if (!StringUtils.isEmpty(delay) && !delay.equalsIgnoreCase("NOW"))
            {
                delayTime = Long.parseLong(delay);
            }

            if (Log.log.isDebugEnabled())
            {
                Log.log.debug("Message received with Id: " + messageId);
            }

            if (Log.log.isTraceEnabled())
            {
                Log.log.trace("  source: " + msgHeader.getSourceString());
                Log.log.trace("  method: " + className + "." + methodName);
                Log.log.trace("  delay: " + delayTime);
            }

            try
            {
                Class<?> invokeClass = null;

                // check SERVICE_QUEUE
                if (className.equals(Constants.ESB_SERVICE_QUEUE))
                {
                    MServiceThread serviceThread = serviceThreads.get(classMethod);
                    if (serviceThread == null)
                    {
                        throw new Exception("Missing MServiceThread: " + classMethod);
                    }

                    serviceThread.addMessage(mListener.getParams(message));
                }

                // check explicit class handlers
                else if (mServer.getHandlers().containsKey(className))
                {
                    invokeClass = mServer.getHandler(className);
                    if(Log.log.isTraceEnabled()) {
                        Log.log.trace("using: " + invokeClass.getName());
                    }
                }

                // check for groovy MService wrapper
                // BD: Added the (.) so we can add class name that
                // starts with "MService" (e.g., MServiceNow.java).
                else if (invokeClass == null && className.startsWith("MService."))
                {
                    // try com.resolve.rsbase.MService
                    try
                    {
                        Log.log.trace("trying: com.resolve.rsbase.MService");
                        invokeClass = Class.forName("com.resolve.rsbase.MService");

                        // fix classname and method name
                        msgHeader.getDest().setClassname("MService");
                        msgHeader.getDest().setMethodname((className + "." + methodName).substring(8));
                    }
                    // try defaultClassPackage.MService
                    // (fall-through
                    // below)
                    catch (Exception e)
                    {
                        invokeClass = null;
                    }
                }

                // try specified defaultClassPackage
                if (invokeClass == null)
                {
                    try
                    {
                        String invokeClassName = mListener.getDefaultClassPackage() + "." + className;

                        if(Log.log.isTraceEnabled()) {
                            Log.log.trace("trying: " + invokeClassName);
                        }
                        invokeClass = Class.forName(invokeClassName);
                    }
                    catch (Exception e)
                    {
                        invokeClass = null;
                    }
                }

                // try com.resolve.<service>
                if (invokeClass == null)
                {
                    try
                    {
                        String invokeClassName = "com.resolve." + MainBase.main.release.getName().toLowerCase() + "." + className;

                        if(Log.log.isTraceEnabled()) {
                            Log.log.trace("trying: " + invokeClassName);
                        }
                        invokeClass = Class.forName(invokeClassName);
                    }
                    catch (Exception e)
                    {
                        invokeClass = null;
                    }
                }

                // try com.resolve.rsbase
                if (invokeClass == null)
                {
                    try
                    {
                        String invokeClassName = "com.resolve.rsbase." + className;

                        if(Log.log.isTraceEnabled()) {
                            Log.log.trace("trying: " + invokeClassName);
                        }
                        invokeClass = Class.forName(invokeClassName);
                    }
                    catch (Exception e)
                    {
                        invokeClass = null;
                    }
                }

                // try fully qualified class
                if (invokeClass == null)
                {
                    try
                    {
                        String invokeClassName = className;

                        if(Log.log.isTraceEnabled()) {
                            Log.log.trace("trying: " + invokeClassName);
                        }
                        invokeClass = Class.forName(invokeClassName);
                    }
                    catch (Exception e)
                    {
                        invokeClass = null;
                    }
                }

                // execute class.method handler
                if (invokeClass != null)
                {
                    // execute handler
                    msgHeader.put("INVOKECLASS", invokeClass);

                    //update to using StartExcutor
                    if (className.equals("MAction") && methodName.equals("executeProcess"))
                    {
                        RSControlMListenerUtil.enqueueRunbookExecMsg(messageId, msgHeader, mListener.getDefaultHandler(), methodName, mListener.getParams(message), delayTime, TimeUnit.SECONDS);
                    }
                    else
                    {
                        if (delayTime == 0)
                        {
                            // immediate delivery
                        	TaskExecutor.execute(messageId, msgHeader, mListener.getDefaultHandler(), "execute", mListener.getParams(message));
                        }
                        else
                        {
                            // delayed delivery
                        	ScheduledExecutor.getInstance().executeDelayed(messageId, msgHeader, mListener.getDefaultHandler(), "execute", mListener.getParams(message), delayTime, TimeUnit.SECONDS);
                        }
                    }
                }
                else if (!className.equals(Constants.ESB_SERVICE_QUEUE))
                {
                    throw new Exception("Unable to find handler for className: " + className);
                }
            }
            catch (Throwable e)
            {
                Log.log.warn("Failed to execute message handler: " + e.getMessage(), e);
            }

            if (Log.log.isDebugEnabled())
            {
                Log.log.debug("Message Id: " + messageId);
            }
        }
    }
}
