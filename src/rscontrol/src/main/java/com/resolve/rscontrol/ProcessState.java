/******************************************************************************
* (C) Copyright 2014
*
* Resolve Systems, LLC
*
* All rights reserved
* This software is distributed under license and may not be used, copied,
* modified, or distributed without the express written permission of
* Resolve Systems, LLC.
*
******************************************************************************/

package com.resolve.rscontrol;

import java.io.DataInput;
import java.io.DataOutput;
import java.io.IOException;
import java.io.Serializable;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ConcurrentMap;

import net.sf.json.JSONObject;

//import com.hazelcast.nio.DataSerializable;
import com.resolve.util.Constants;
import com.resolve.util.Log;
import com.resolve.util.StringUtils;

public class ProcessState implements Serializable
{
    //DO NOT change this ID as it will effect 5.0->5.1 migration. 
	private static final long serialVersionUID = -8863277762735373865L;
	
	String processid;           // processid
	String processNumber;		// process number
    long startTime;             // process start timestamp
    long processTimeout;
    long lastActivityTime;      // process activity last timestamp
    boolean hasLoop;            // whether this runbook can loop
//    String parentWiki;
    String wiki;
    JSONObject subStates;
    
    String bestCondition;
    String bestSeverity;
    String worstCondition;
    String worstSeverity;

    private String token;
    private String rsviewIdGuid;
    
    static String GLOBAL_SNAPSHOT = "_RESOLVE_PROCESSSTATE_GLOBALS_SNAPSHOT_";
    ConcurrentMap<String, ConcurrentMap<String,Object>> requestedMap = new ConcurrentHashMap<String, ConcurrentMap<String,Object>>(); 

    String mockValue;

    private boolean checkPreConditionNullVars;  // check pre condition for null before evaluation
    
    private int atExecCount; // Total count of ATs executed
    
    /*
     * Map of precondition error messages by action task execute id, MResult.logActionResult 
     * uses it to update severity of action task to WARNING (if it is better than that) and
     * updates detail precondition warning message. 
     */
    ConcurrentMap<String, String> preConditionErrors = new ConcurrentHashMap<String, String>();
    
    public ProcessState()
    {
    } // ProcessState
    
    void setGlobalsSnapshot(ConcurrentMap<String,Object> map)
    {
    	requestedMap.put(GLOBAL_SNAPSHOT, map); 
    }

    public ConcurrentMap<String,Object> getGlobalsSnapshot()
    {
    	return requestedMap.get(GLOBAL_SNAPSHOT); 
    }

    void clearGlobalsSnapshot()
    {
    	requestedMap.remove(processid); 
    }
    
    void setRequested(String id, ConcurrentMap<String,Object> map)
    {
        requestedMap.put(id, map);
    }
    
    public String toString()
    {
        System.out.println("REQUESTED MAP:");
        for(String key : requestedMap.keySet())
        {
            System.out.println("key: " + key + " # value: " + requestedMap.get(key));
        }

        return "ProcessState - processid: "+processid+" startTime: "+startTime+" lastActivityTime: "+lastActivityTime+" hasLoop: "+hasLoop+" bestCondition: "+bestCondition+" bestSeverity: "+bestSeverity+" worstCondition: "+worstCondition+" worstSeverity: "+worstSeverity + " checkPreConditionNullVars: " + checkPreConditionNullVars;
    } // toString
    
    public String toDetailedString()
    {
        String result = "";
        result += "ProcessState - processid: "+processid+"\n";
        result += "  startTime: "+startTime+" lastActivityTime: "+lastActivityTime+" hasLoop: "+hasLoop+" bestCondition: "+bestCondition+" bestSeverity: "+bestSeverity+" worstCondition: "+worstCondition+" worstSeverity: "+worstSeverity+ " checkPreConditionNullVars: " + checkPreConditionNullVars + "\n";
        result += "  subStates size: "+subStates.size();
        result += "  subStates:\n";
        return result;
    } // toString
    
    public ProcessState(String processid, boolean hasLoop, String wiki, long startTime, long processTimeout, String token, String rsviewIdGuid, boolean checkPreConditionNullVars, String processNumber)
    {
        if (startTime <= 0)
        {
            this.startTime = System.currentTimeMillis();
        }
        else
        {
            this.startTime = startTime;
        }
        
        this.processTimeout = processTimeout;
        
        this.processid = processid;
        this.processNumber = processNumber;
        this.startTime = System.currentTimeMillis();
        this.lastActivityTime = this.startTime;
        this.hasLoop = hasLoop;
//        this.parentWiki = parentWiki;
        
        this.wiki = wiki;
        this.subStates = new JSONObject();
        
	    this.bestCondition = "GOOD";
	    this.bestSeverity = "GOOD";
	    this.worstCondition = "GOOD";
	    this.worstSeverity = "GOOD";
	    
	    this.token = token;
	    this.rsviewIdGuid = rsviewIdGuid;
	    this.checkPreConditionNullVars = checkPreConditionNullVars;
    } // ProcessState
    
    public void writeData(DataOutput out) throws IOException 
    {
        if (Log.log.isDebugEnabled()) {
            Log.log.debug("Writing hazelcast state:\n"+this.toDetailedString());        
        }
	    out.writeUTF(processid);
	    out.writeUTF(processNumber);
	    out.writeLong(startTime);
	    out.writeLong(processTimeout);
	    out.writeLong(lastActivityTime);
	    out.writeBoolean(hasLoop);
//	    out.writeUTF(parentWiki);
	    out.writeUTF(wiki);
        
	    String jsonSubstates = StringUtils.jsonObjectToString(subStates);
	    out.writeUTF(jsonSubstates);
        
	    out.writeUTF(bestCondition);
	    out.writeUTF(bestSeverity);
	    out.writeUTF(worstCondition);
	    out.writeUTF(worstSeverity);
	    
	    out.writeBoolean(checkPreConditionNullVars);
    } // writeData

    public void readData(DataInput in) throws IOException 
    {
	    processid = in.readUTF();
	    processNumber = in.readUTF();
	    startTime = in.readLong();
	    try
	    {
	    	processTimeout = in.readLong();
	    }
	    catch (Exception ex)
	    {
	    	processTimeout = in.readInt();
	    }
	    
	    lastActivityTime = in.readLong();
	    hasLoop = in.readBoolean();
//	    parentWiki = in.readUTF();
	    wiki = in.readUTF();
        
	    String jsonSubstates = in.readUTF();
        subStates = StringUtils.stringToJSONObject(jsonSubstates);
        if (subStates == null)
        {
            Log.log.error("Missing substates from cluster readData", new Exception());
            subStates = new JSONObject();
        }
        
	    bestCondition = in.readUTF();
	    bestSeverity = in.readUTF();
	    worstCondition = in.readUTF();
	    worstSeverity = in.readUTF();
	    
	    checkPreConditionNullVars = in.readBoolean();
	    
	    if (Log.log.isDebugEnabled()) {
	        Log.log.debug("Reading hazelcast state:\n"+this.toDetailedString());        
        }
    } // readData
    
    public JSONObject getSubRunbook(String wikiId)
    {
        return this.subStates.getJSONObject(wikiId);
    } // getSubRunbook
    
    public String getParentWiki(String wikiId)
    {
        String result = null;
        
        JSONObject obj = getSubRunbook(wikiId);
        if (obj != null && !obj.isNullObject() && !obj.isEmpty())
        {
            result = obj.getString("parentWiki");
        }
        return result;
    } // getParentWiki
    
    public void addSubRunbook(String subWiki, String wikiId, String parentWiki, String parentWikiId)
    {
        JSONObject state = new JSONObject();
        state.put("wiki", subWiki);
        state.put("wikiId", wikiId);
        state.put("parentWiki", parentWiki);
        state.put("parentWikiId", parentWikiId);
        state.put("startTime", ""+System.currentTimeMillis());
        state.put("lastActivityTime", ""+System.currentTimeMillis());
        state.put("bestCondition", "GOOD");
        state.put("bestSeverity", "GOOD");
        state.put("worstCondition", "GOOD");
        state.put("worstSeverity", "GOOD");
        
        // add subrunbook
        this.subStates.put(wikiId, state);
    } // addSubRunbook
    
    public void removeSubRunbook(String wikiId)
    {
        this.subStates.remove(wikiId);
    } // removeSubRunbook
    
    public boolean isHasLoop()
    {
        return hasLoop;
    } // isHasLoop

    public void setHasLoop(boolean hasLoop)
    {
        this.hasLoop = hasLoop;
    } // setHasLoop

    public void updateEndSummary(String wikiId, String condition, String severity)
    {
        /*
         * update process overall state
         */
        
        // update best
        if (bestCondition == null || bestSeverity == null)
        {
            bestCondition = condition;
            bestSeverity = severity;
        }
        else
        {
            if (conditionIsGT(condition, bestCondition))
            {
                bestCondition = condition;
            }
                
            if (severityIsGT(severity, bestSeverity))
            {
                bestSeverity = severity;
            }
        }
            
        // update worst
        if (worstCondition == null || worstSeverity == null)
        {
            worstCondition = condition;
            worstSeverity = severity;
        }
        else
        {
            if (conditionIsLT(condition, worstCondition))
            {
                worstCondition = condition;
            }
                
            if (severityIsLT(severity, worstSeverity))
            {
                worstSeverity = severity;
            }
        }
        
        
        /*
         * update subrunbook
         */
        
        if (!wikiId.endsWith(":0"))
        {
            JSONObject state = subStates.getJSONObject(wikiId);
            if (state != null && !state.isNullObject())
            {
                String subBestCondition = state.getString("bestCondition");
                String subWorstCondition = state.getString("worstCondition");
                String subBestSeverity = state.getString("bestSeverity");
                String subWorstSeverity = state.getString("worstSeverity");
                
                // update best
                if (subBestCondition == null || subBestSeverity == null)
                {
                    subBestCondition = condition;
                    subBestSeverity = severity;
                }
                else
                {
                    if (conditionIsGT(condition, subBestCondition))
                    {
                        subBestCondition = condition;
                    }
                        
                    if (severityIsGT(severity, subBestSeverity))
                    {
                        subBestSeverity = severity;
                    }
                }
                    
                // update worst
                if (subWorstCondition == null || subWorstSeverity == null)
                {
                    subWorstCondition = condition;
                    subWorstSeverity = severity;
                }
                else
                {
                    if (conditionIsLT(condition, subWorstCondition))
                    {
                        subWorstCondition = condition;
                    }
                        
                    if (severityIsLT(severity, subWorstSeverity))
                    {
                        subWorstSeverity = severity;
                    }
                }
                
                // update sub conditions and severity
                state.put("bestCondition", subBestCondition);
                state.put("worstCondition", subWorstCondition);
                state.put("bestSeverity", subBestSeverity);
                state.put("worstSeverity", subWorstSeverity);
            }
        }
 
    } // updateEndSummary
    
    public String getProcessid()
    {
        return processid;
    }

    public void setProcessid(String processid)
    {
        this.processid = processid;
    }
    
    public String getProcessNumber()
    {
        return processNumber;
    }

    public void setProcessNumber(String processNumber)
    {
        this.processNumber = processNumber;
    }
    
//    public String getParentWiki()
//    {
//        return parentWiki;
//    }
//
//    public void setParentWiki(String parentWiki)
//    {
//        this.parentWiki = parentWiki;
//    }

    public long getTotalDuration()
    {
        return (System.currentTimeMillis() - startTime) / 1000;
    } // getTotalDuration
    
    public long getTotalDuration(String wikiId)
    {
        long result = 0;
        
        if (wikiId.endsWith(":0"))
        {
            result = (System.currentTimeMillis() - startTime) / 1000;
        }
        else
        {
            JSONObject state = subStates.getJSONObject(wikiId);
            if (state != null && !state.isNullObject())
            {
                long subStartTime = state.getLong("startTime");
                result = (System.currentTimeMillis() - subStartTime) / 1000;
            }
            else
            {
                Log.log.error("Missing SubRunbook startTime", new Exception());
            }
        }
        
        return result;
    } // getTotalDuration

    public String getBestCondition()
    {
        return bestCondition;
    }
    
    public String getBestCondition(String wikiId)
    {
        String result = "GOOD";
        
        if (wikiId.endsWith(":0"))
        {
            result = bestCondition;
        }
        else
        {
            JSONObject state = subStates.getJSONObject(wikiId);
            if (state != null && !state.isNullObject())
            {
                result = state.getString("bestCondition");
            }
            else
            {
                Log.log.error("Missing SubRunbook bestCondition", new Exception());
            }
        }
        
        return result;
    }

    public void setBestCondition(String bestCondition)
    {
        this.bestCondition = bestCondition;
    }

    public String getBestSeverity()
    {
        return bestSeverity;
    }
    
    public String getBestSeverity(String wikiId)
    {
        String result = "GOOD";
        
        if (wikiId.endsWith(":0"))
        {
            result = bestSeverity;
        }
        else
        {
            JSONObject state = subStates.getJSONObject(wikiId);
            if (state != null && !state.isNullObject())
            {
                result = state.getString("bestSeverity");
            }
            else
            {
                Log.log.error("Missing SubRunbook bestSeverity", new Exception());
            }
        }
        
        return result;
    }

    public void setBestSeverity(String bestSeverity)
    {
        this.bestSeverity = bestSeverity;
    }

    public String getWorstCondition()
    {
        return worstCondition;
    }
    
    public String getWorstCondition(String wikiId)
    {
        String result = "GOOD";
        
        if (wikiId.endsWith(":0"))
        {
            result = worstCondition;
        }
        else
        {
            JSONObject state = subStates.getJSONObject(wikiId);
            if (state != null && !state.isNullObject())
            {
                result = state.getString("worstCondition");
            }
            else
            {
                Log.log.error("Missing SubRunbook worstCondition", new Exception());
            }
        }
        
        return result;
    }

    public void setWorstCondition(String worstCondition)
    {
        this.worstCondition = worstCondition;
    }

    public String getWorstSeverity()
    {
        return worstSeverity;
    }
    
    public String getWorstSeverity(String wikiId)
    {
        String result = "GOOD";
        
        if (wikiId.endsWith(":0"))
        {
            result = worstSeverity;
        }
        else
        {
            JSONObject state = subStates.getJSONObject(wikiId);
            if (state != null && !state.isNullObject())
            {
                result = state.getString("worstSeverity");
            }
            else
            {
                Log.log.error("Missing SubRunbook worstSeverity", new Exception());
            }
        }
        
        return result;
    }

    public void setWorstSeverity(String worstSeverity)
    {
        this.worstSeverity = worstSeverity;
    }
    
    public static boolean conditionIsGT(String cond1, String cond2)
    {
        boolean result = false;
        
        int condValue1 = getConditionValue(cond1);
        int condValue2 = getConditionValue(cond2);
        
        if (condValue1 > condValue2)
        {
            result = true;
        }
        
        return result;
    } // conditionIsGT
        
    public static boolean conditionIsLT(String cond1, String cond2)
    {
        boolean result = false;
        
        int condValue1 = getConditionValue(cond1);
        int condValue2 = getConditionValue(cond2);
        
        if (condValue1 < condValue2)
        {
            result = true;
        }
        
        return result;
    } // conditionIsLT
        
    public static boolean severityIsGT(String severity1, String severity2)
    {
        boolean result = false;
        
        int severityValue1 = getSeverityValue(severity1);
        int severityValue2 = getSeverityValue(severity2);
        
        if (severityValue1 > severityValue2)
        {
            result = true;
        }
        
        return result;
    } // severityIsGT
        
    public static boolean severityIsLT(String severity1, String severity2)
    {
        boolean result = false;
        
        int severityValue1 = getSeverityValue(severity1);
        int severityValue2 = getSeverityValue(severity2);
        
        if (severityValue1 < severityValue2)
        {
            result = true;
        }
        
        return result;
    } // severityIsLT
        
    public static int getConditionValue(String condition)
    {
        int result = 0;
        
        if (condition.equalsIgnoreCase(Constants.ASSESS_CONDITION_GOOD))
        {
            result = 1;
        }
        else if (condition.equalsIgnoreCase(Constants.ASSESS_CONDITION_BAD))
        {
            result = -1;
        }
        
        return result;
    } // getConditionValue
    
    public static int getSeverityValue(String severity)
    {
        int result = 0;
        
        if (severity.equalsIgnoreCase(Constants.ASSESS_SEVERITY_GOOD))
        {
            result = 1;
        }
        else if (severity.equalsIgnoreCase(Constants.ASSESS_SEVERITY_WARNING))
        {
            result = -2;
        }
        else if (severity.equalsIgnoreCase(Constants.ASSESS_SEVERITY_SEVERE))
        {
            result = -3;
        }
        else if (severity.equalsIgnoreCase(Constants.ASSESS_SEVERITY_CRITICAL))
        {
            result = -4;
        }
        
        return result;
    } // getSeverityValue

    public long getStartTime()
    {
        return startTime;
    } // getStartTime

    public void setStartTime(long startTime)
    {
        if (startTime <= 0)
        {
            startTime = System.currentTimeMillis();
        }
        this.startTime = startTime;
    } // setStartTime

    public long getProcessTimeout()
    {
        return processTimeout;
    } 

    public void setProcessTimeout(int pt)
    {
        this.processTimeout = pt;
    } // setStartTime
    
    public long getLastActivityTime()
    {
        return lastActivityTime;
    } // getStartTime

    public void setLastActivityTime(long lastActivityTime)
    {
        if (lastActivityTime <= 0)
        {
            lastActivityTime = System.currentTimeMillis();
        }
        this.lastActivityTime = lastActivityTime;
    } // setLastActivityTime
    
    public String getToken()
    {
        return token;
    }
    
    public void setToken(String token)
    {
        this.token = token;
    }
    
    public String getRsviewIdGuid()
    {
        return rsviewIdGuid;
    }
    
    public void setRsviewIdGuid(String rsviewIdGuid)
    {
        this.rsviewIdGuid = rsviewIdGuid;
    }
    
    public boolean getCheckPreConditionNullVars()
    {
        return checkPreConditionNullVars;
    } // getCheckPreConditionNullVars

    public void setCheckPreConditionNullVars(boolean checkPreConditionNullVars)
    {
        this.checkPreConditionNullVars = checkPreConditionNullVars;
    } // setCheckPreConditionNullVars
    
    public void setPreconditionError(String executeId, String preConditionError)
    {
        preConditionErrors.put(executeId, preConditionError);
    }
    
    public String getAndClearPreConditionError(String executeId)
    {
        String preConditionError = preConditionErrors.get(executeId);
        preConditionErrors.remove(executeId);
        return preConditionError;
    }
    
    public String getPreConditionError(String executeId)
    {
        return preConditionErrors.get(executeId);
    }
    
    public int getAtExecCount() {
    	return atExecCount;
    }
    
    public void setAtExecCount(int atExecCount) {
    	this.atExecCount = atExecCount;
    }
    
    public int taskCompleted() {
    	return atExecCount++;
    }
} // ProcessState
