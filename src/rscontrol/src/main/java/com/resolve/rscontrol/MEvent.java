/******************************************************************************
* (C) Copyright 2014
*
* Resolve Systems, LLC
*
* All rights reserved
* This software is distributed under license and may not be used, copied,
* modified, or distributed without the express written permission of
* Resolve Systems, LLC.
*
******************************************************************************/

package com.resolve.rscontrol;

import java.util.Map;
import java.util.concurrent.atomic.AtomicLong;

import com.resolve.esb.MMsgHeader;
import com.resolve.esb.amqp.MAmqpRunbookExecutionListener;
import com.resolve.esb.amqp.MAmqpServer;
import com.resolve.thread.ScheduledExecutor;
import com.resolve.util.Constants;
import com.resolve.util.Log;

public class MEvent
{
    // monitoring stats
    static long avgEventTime;
    static long totalEventTime;
    static long totalEventCount;
    static AtomicLong atomEventCount = new AtomicLong(); //thread safe counter
    
    public void eventResult(MMsgHeader header, Map params) throws Exception
    {
        try
        {
            // get headers
            String type = header.getEventType();
            if (type == null)
            {
                type = (String)params.get(Constants.ESB_PARAM_EVENTTYPE);
            }
            else
            {
                type = "UNKNOWN";
            }
            
            // add type into params
            params.put(Constants.ESB_PARAM_EVENTTYPE, type);
            
            if (Log.log.isDebugEnabled()) {
                Log.log.debug("Processing Event results");
                Log.log.debug("params: "+params);
                Log.log.debug("eventtype: "+type);     
            }
            
            if (params != null && params.size() > 0)
            {
                totalEventCount++;
                if (Log.log.isDebugEnabled()) {
                    Log.log.debug("atomEventCount=" + atomEventCount.incrementAndGet());
                }
                long startTime = System.currentTimeMillis();		                
                
                // direct runbook execution
                if (params.containsKey(Constants.EXECUTE_WIKI))
                {
                    EventHandler.runbookEventHandler(params);
                }
                
                // generic event handler (db)
                else
                {
	                EventHandler.genericEventHandler(type, params, params);
                }
                
                // monitoring stats
                totalEventTime += System.currentTimeMillis()-startTime;
                avgEventTime = totalEventTime / totalEventCount;
                if (Log.log.isDebugEnabled()) {
                    Log.log.debug("avgEventTime: "+avgEventTime+" totalEventCount: "+totalEventCount);
                    Log.log.debug("Active Threads: " + ScheduledExecutor.getInstance().getActiveThreads()+" Queue: "+ScheduledExecutor.getInstance().getQueueSize());
                }
            }
        }
        catch (Exception e)
        {
            Log.log.error("Failed to process actiontask result: "+e.getMessage(), e);
        }
        finally
        {
        	MAmqpServer mServer = (MAmqpServer) Main.main.mServer;
        	MAmqpRunbookExecutionListener.acknowledgeRunbookExecution((String) params.get(Constants.EXECUTE_JOBID), mServer);
        }
    } // eventResult

} // MAction
