/******************************************************************************
* (C) Copyright 2014
*
* Resolve Systems, LLC
*
* All rights reserved
* This software is distributed under license and may not be used, copied,
* modified, or distributed without the express written permission of
* Resolve Systems, LLC.
*
******************************************************************************/

package com.resolve.rscontrol;

import java.io.Serializable;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

import com.resolve.util.StringUtils;

public class TargetInstance implements Serializable
{
    private static final long serialVersionUID = -4605578204480148455L;
	String address;
    String guid;
    String affinity;
    Map params;
    
    public TargetInstance(String address, String guid)
    {
        this.address = address;
        this.guid = guid;
        this.affinity = "";
        this.params = new ConcurrentHashMap();
    } // TargetInstance
    
    public TargetInstance(String address, String guid, Map params)
    {
        this.address = address;
        this.guid = guid;
        this.affinity = "";
        this.params = params;
    } // TargetInstance
    
    public TargetInstance(TargetInstance instance)
    {
        this.address = instance.address;
        this.guid = instance.guid;
        this.affinity = "";
        this.params = instance.params;
    } // TargetInstance
    
    public String getEsbAddr()
    {
        String result;
        
        if (StringUtils.isNotEmpty(affinity))
        {
            result = this.affinity;
        }
        else
        {
            result = this.guid;
        }
        
        return result;
    } // getEsbAddr
    
    public String toString()
    {
    	return String.format("[addr: %s guid: %s affinity: %s params: %s]", address, guid, affinity, 
				 			 StringUtils.mapToString(params, "=", ", "));
    } // toString
    
    public String getAddress()
    {
        return address;
    }
    public void setAddress(String address)
    {
        this.address = address;
    }
    public String getGuid()
    {
        return guid;
    }
    public void setGuid(String guid)
    {
        this.guid = guid;
    }
    public String get(String name)
    {
        return (String)params.get(name);
    }
    public void set(String name, String value)
    {
        params.put(name, value);
    }
    public void put(String name, String value)
    {
        params.put(name, value);
    }
    public void setParams(Map params)
    {
        this.params = params;
    }
    public Map getParams()
    {
        return params;
    }
    public String getAffinity()
    {
        return affinity;
    }
    public void setAffinity(String affinity)
    {
        this.affinity = affinity;
    }
    
} // TargetInstance