/******************************************************************************
* (C) Copyright 2014
*
* Resolve Systems, LLC
*
* All rights reserved
* This software is distributed under license and may not be used, copied,
* modified, or distributed without the express written permission of
* Resolve Systems, LLC.
*
******************************************************************************/

package com.resolve.rscontrol;

import java.util.Map;

import com.resolve.persistence.model.TelnetPool;
import com.resolve.services.hibernate.util.GatewayUtil;

/**
 * Although this is a gateway there is no "filter" for this so we do not need to
 * extend {@link MGateway} for this class. However if we introduce filter in the future
 * then we'll extend {@link MGateway}
 */
public class MTelnet
{
    public void setPools(Map<String, String> pools)
    {
        GatewayUtil.getInstance().setGatewayObjects(TelnetPool.class.getSimpleName(), pools);
    } // setPools

} // MTelnet
