/******************************************************************************
* (C) Copyright 2014
*
* Resolve Systems, LLC
*
* All rights reserved
* This software is distributed under license and may not be used, copied,
* modified, or distributed without the express written permission of
* Resolve Systems, LLC.
*
******************************************************************************/

package com.resolve.rscontrol;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.resolve.services.ServiceHibernate;
import com.resolve.services.vo.social.SocialRssDTO;
import com.resolve.util.Constants;
import com.resolve.util.StringUtils;

public class MSocial
{
    //called when the RSCONTROL comes up
    public static void loadAllRss()
    {
        //get list of all the RSS that are active
        List<Map<String, Object>> allRsss = ServiceHibernate.queryCustomTable(SocialRssDTO.TABLE_NAME, "u_is_locked = false", null);
        if(allRsss != null && allRsss.size() > 0)
        {
            for(Map<String, Object> rss : allRsss)
            {
                loadRss(new SocialRssDTO(rss), null);
            }
        }
    }
    
    @SuppressWarnings("rawtypes")
    public static void loadRSS(Map params)
    {
//        String name = (String)params.get(Constants.RSS_NAME);
        String sys_id = (String)params.get(Constants.RSS_SYS_ID);
        
        if (StringUtils.isNotEmpty(sys_id))
        {
            Map<String, Object> entry = ServiceHibernate.findById(Constants.RSS_SUBSCRIPTION, sys_id);
            if (entry != null)
            {
                loadRss(new SocialRssDTO(entry), params);
            }
        }
    } // loadRSS
    
    public static void removeRSS(Map params)
    {
        String name = (String)params.get(Constants.RSS_NAME);
        String sys_id = (String)params.get(Constants.RSS_SYS_ID);
        
        if (StringUtils.isNotEmpty(sys_id))
        {
            // remote rss from db
		    MCron.unscheduleTask("RSS_"+name.toUpperCase());
        }
    } // removeRSS
    
    //private apis
    private static void loadRss(SocialRssDTO rss, Map params)
    {
        if (rss != null && rss.getU_is_locked() != null && !rss.getU_is_locked())
        {
            String schedule = rss.getU_schedule();//(String)entry.get("u_schedule");
            if (StringUtils.isEmpty(schedule))
            {
                schedule = "0 0/30 * * * ?";
                //            entry.put("u_schedule", schedule);
            }
            //prepare the map for schedule task
            if(params == null)
            {
                params = new HashMap();
                params.put(Constants.RSS_NAME, rss.getU_display_name());
                params.put(Constants.RSS_SYS_ID, rss.getSys_id());
            }
            
            MCron.scheduleTask("RSS_" + rss.getU_display_name().toUpperCase(), "RSVIEW#MSocial.updateFeed", params, schedule, "system", true, false);
        }
        
    }

} // MSocial
