/******************************************************************************
* (C) Copyright 2014
*
* Resolve Systems, LLC
*
* All rights reserved
* This software is distributed under license and may not be used, copied,
* modified, or distributed without the express written permission of
* Resolve Systems, LLC.
*
******************************************************************************/

package com.resolve.rscontrol;

import com.resolve.cron.CronScheduler;

public class CronMain extends CronScheduler
{
    public CronMain()
    {
        super(((com.resolve.rscontrol.Main) Main.main).configCron.config);

    } // CronMain

    public void defaultTasks()
    {
    } // defaultTasks

    public void startupTasks() throws Exception
    {
    } // startupTasks

} // CronMain
