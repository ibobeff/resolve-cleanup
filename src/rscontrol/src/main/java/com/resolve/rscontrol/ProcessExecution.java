/******************************************************************************
* (C) Copyright 2014
*
* Resolve Systems, LLC
*
* All rights reserved
* This software is distributed under license and may not be used, copied,
* modified, or distributed without the express written permission of
* Resolve Systems, LLC.
*
******************************************************************************/

package com.resolve.rscontrol;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Queue;
import java.util.StringTokenizer;
import java.util.UUID;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ConcurrentMap;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.concurrent.atomic.AtomicLong;
import java.util.concurrent.locks.ReentrantLock;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import com.resolve.assess.AssessResult;
import com.resolve.esb.MMsgContent;
import com.resolve.esb.MMsgOptions;
import com.resolve.esb.amqp.MAmqpRunbookExecutionListener;
import com.resolve.esb.amqp.MAmqpServer;
import com.resolve.exception.ActionTaskAbortException;
import com.resolve.execute.ExecuteResult;
import com.resolve.groovy.script.GroovyScript;
import com.resolve.persistence.util.HibernateUtil;
import com.resolve.rsbase.MainBase;
import com.resolve.search.SearchService;
import com.resolve.search.SearchUtils;
import com.resolve.search.model.ExecuteState;
import com.resolve.search.model.Worksheet;
import com.resolve.services.ServiceHibernate;
import com.resolve.services.ServiceWorksheet;
import com.resolve.services.WSDataMap;
import com.resolve.services.hibernate.vo.ResolveActionInvocOptionsVO;
import com.resolve.services.hibernate.vo.ResolveActionInvocVO;
import com.resolve.services.hibernate.vo.ResolveActionTaskMockDataVO;
import com.resolve.services.hibernate.vo.ResolveActionTaskVO;
import com.resolve.services.hibernate.vo.ResolvePreprocessVO;
import com.resolve.services.hibernate.vo.ResolveRegistrationPropertyVO;
import com.resolve.services.util.BindingScript;
import com.resolve.services.util.PropertiesUtil;
import com.resolve.services.util.SecurityUtil;
import com.resolve.services.util.UserUtils;
import com.resolve.services.util.VariableUtil;
import com.resolve.services.util.WorksheetUtil;
import com.resolve.services.util.WorksheetUtils;
import com.resolve.thread.TaskExecutor;
import com.resolve.thread.ScheduledExecutor;
import com.resolve.thread.ScheduledTask;
import com.resolve.util.Constants;
import com.resolve.util.CryptUtils;
import com.resolve.util.DirectedTreeNode;
import com.resolve.util.EncryptionTransportObject;
import com.resolve.util.ExecutableObject;
import com.resolve.util.LockUtils;
import com.resolve.util.Log;
import com.resolve.util.ScriptDebug;
import com.resolve.util.StringUtils;
import com.resolve.util.eventsourcing.EventMessage;
import com.resolve.util.eventsourcing.EventMessageFactory;
import com.resolve.util.eventsourcing.IncidentResolutionTaskType;
import com.resolve.util.performance.ExecutionTimeUtil;
import com.resolve.util.performance.datasources.ExecutionStep;
import com.resolve.util.performance.datasources.ExecutionStepPhase;

import groovy.lang.Binding;

public class ProcessExecution
{
    //this Set stores various standard INPUT we need for our own processing
    //they should go inside PARAMS. They are collected from Start, End, and Event
    //if there are more then add those here. Check RBA-10278 for reference.
    private static final HashSet<String> STANDARD_INPUTS = new HashSet<String>();
    static
    {
        //Start ATs
        STANDARD_INPUTS.add(Constants.PROCESS_LOOP);
        STANDARD_INPUTS.add(Constants.PROCESS_DEBUG);
        STANDARD_INPUTS.add(Constants.PROCESS_TIMEOUT);
        STANDARD_INPUTS.add(Constants.EXECUTE_WIKI_PROCESS);
        STANDARD_INPUTS.add(Constants.PROCESS_CONCURRENT_LIMIT);
        STANDARD_INPUTS.add(Constants.MODEL_MODE);
        STANDARD_INPUTS.add(Constants.MODEL_TYPE);
        STANDARD_INPUTS.add(Constants.PROCESS_DISPLAY);
        
        //End AT
        STANDARD_INPUTS.add(Constants.EXECUTE_WIKI_PROCESS);
        STANDARD_INPUTS.add(Constants.EXECUTE_END_QUIT);
        STANDARD_INPUTS.add(Constants.EXECUTE_END_TYPE);
        STANDARD_INPUTS.add(Constants.EXECUTE_END_WAIT);
        STANDARD_INPUTS.add(Constants.EXECUTE_END_SEVERITY);
        STANDARD_INPUTS.add(Constants.EXECUTE_END_CONDITION);

        //Event
        STANDARD_INPUTS.add(Constants.EVENT_IDLE);
        STANDARD_INPUTS.add(Constants.EVENT_END);
        STANDARD_INPUTS.add(Constants.EVENT_EVENTID);
        STANDARD_INPUTS.add(Constants.EVENT_REFERENCE);
        STANDARD_INPUTS.add(Constants.EVENT_TIMEOUT);
        STANDARD_INPUTS.add(Constants.EXECUTE_WIKI_WIKIID);
        STANDARD_INPUTS.add(Constants.EXECUTE_WIKI_PARENT);
        STANDARD_INPUTS.add(Constants.EXECUTE_WIKI_PARENTID);
    }
    
    // regex
    final static Pattern VAR_REGEX_CONFIG = Pattern.compile(Constants.VAR_REGEX_CONFIG);

    static ProcessDependency processDependency = new ProcessDependency();

    public static ExecuteParameterStateCache executeParameterStateCache = new ExecuteParameterStateCache();
    public static ConcurrentHashMap<String, List<ResolveActionInvocOptionsVO>> optionCache = new ConcurrentHashMap<String, List<ResolveActionInvocOptionsVO>>();
    public static ConcurrentHashMap<String, ResolveActionTaskVO> actionTaskCache = new ConcurrentHashMap<String, ResolveActionTaskVO>();

    // map<RUNBOOK> => completed total duration, completed count and aborted count
    public static Map<String, Long> runbookCompletedDuration = new ConcurrentHashMap<String, Long>();
    public static Map<String, Long> runbookCompleted = new ConcurrentHashMap<String, Long>();
    public static Map<String, Long> runbookAborted = new ConcurrentHashMap<String, Long>();
    public static Map<String, Long> runbookConditionGood = new ConcurrentHashMap<String, Long>();
    public static Map<String, Long> runbookConditionBad = new ConcurrentHashMap<String, Long>();
    public static Map<String, Long> runbookConditionUnknown = new ConcurrentHashMap<String, Long>();
    public static Map<String, Long> runbookSeverityGood = new ConcurrentHashMap<String, Long>();
    public static Map<String, Long> runbookSeverityCritical = new ConcurrentHashMap<String, Long>();
    public static Map<String, Long> runbookSeverityWarning = new ConcurrentHashMap<String, Long>();
    public static Map<String, Long> runbookSeveritySevere = new ConcurrentHashMap<String, Long>();
    public static Map<String, Long> runbookSeverityUnknown = new ConcurrentHashMap<String, Long>();
    public static AtomicLong runbookTotal = new AtomicLong();
    public static AtomicLong runbookStartLatency = new AtomicLong();

    static ResolveActionTaskVO abortActionTask = null;
    static ConcurrentHashMap<String, ProcessInfo> processInfoMap = new ConcurrentHashMap<String, ProcessInfo>();

    public static String PROCESS_TIMEOUT_ROWKEY = "RESOLVE_PROCESS_TIMEOUT_ROWKEY";
    static
    {
        // init thread groovy
        GroovyScript.setThreadGroovyFactory(ThreadGroovyDB.class);

        // init abortTask
        if (abortActionTask == null)
        {
            String abortActionTaskName = "abort#resolve";
            abortActionTask = ServiceHibernate.getResolveActionTask(null, abortActionTaskName, "system");
        }
    } // static
    
    
    public ProcessExecution()
    {
    } // MAction

    public static void startProcess(String problemid, String processid, String wiki, String reference, String event_eventid, String event_reference, String userid, long processTimeout, boolean hasLoop, ScriptDebug debug, String mock, boolean isEvent, long processStartTime, Map params, ActionTaskProperties properties, ArrayList start, ActionProcessTaskRequest eventTask,String token, String rsviewIdGuid, boolean checkPreConditionNullVars) throws Exception
    {
    	startProcess(false, problemid, processid, wiki, reference, event_eventid, event_reference, userid, processTimeout, hasLoop, debug, mock, isEvent, processStartTime, params, properties, start, eventTask,token, rsviewIdGuid, checkPreConditionNullVars);
	}
    
    public static void startProcess(boolean cacheOnly, String problemid, String processid, String wiki, String reference, String event_eventid, String event_reference, String userid, long processTimeout, boolean hasLoop, ScriptDebug debug, String mock, boolean isEvent, long processStartTime, Map params, ActionTaskProperties properties, ArrayList start, ActionProcessTaskRequest eventTask,String token, String rsviewIdGuid, boolean checkPreConditionNullVars) throws Exception
    {
        ExecutionTimeUtil.log((String) params.get("RESOLVE.JOB_ID"), null, null, ExecutionStep.METHOD_EXECUTION, ExecutionStepPhase.START);

        if (!ProcessContext.existsProcessMap(processid))
        {
            if (Log.log.isDebugEnabled()) {
                String msg = "Process " + processid + " was already completed or aborted. Bypass startProcess()!";
                Log.log.debug(msg);
            }
            return;
        }
        
        String result = "";
        Log.log.info("ExecuteProcess processid: " + processid);
        debug.setComponent("StartProcess");

        try
        {
            boolean isAbort = false;

            // update wiki docname
            if (wiki.startsWith(Constants.EXECUTE_ABORTWIKI))
            {
                wiki = wiki.substring(Constants.EXECUTE_ABORTWIKI.length());
                isAbort = true;
            }

            if(Log.log.isDebugEnabled()) {
                Log.log.debug("cacheOnly: " + cacheOnly);
                Log.log.debug("problemid: " + problemid);
                Log.log.debug("processid: " + processid);
                Log.log.debug(String.format("processNumber: %s", 
                							params.containsKey(Constants.EXECUTE_PROCESS_NUMBER) ? 
                							(String)params.get(Constants.EXECUTE_PROCESS_NUMBER) : "null"));
                Log.log.debug("wiki: " + wiki);
                Log.log.debug("reference: " + reference);
                Log.log.debug("userid: " + userid);
                Log.log.debug("timeout: " + processTimeout);
                Log.log.debug("hasLoop: " + hasLoop);
                Log.log.debug("checkPreConditionNullVars: " + checkPreConditionNullVars);

                // generate START list and dependency map
                Log.log.debug("Completed process dependencies");
            }

            // add scheduled abort task
            scheduleRunbookAbort(problemid, processid, wiki, reference, userid, processTimeout, hasLoop, isEvent,
					params, isAbort);

            // set dependency initialization completed
            processDependency.setInitCompleted(processid);

            // execute START ActionTasks
            if (!cacheOnly)
            {
                // add ProcessInfo
                createProcessInfo(processid, wiki);
                ProcessContext.put(processid, "RESOLVE.JOB_ID", (String) params.get("RESOLVE.JOB_ID"));

                if (isEvent)
                {
                	  boolean exit = resumeRunbook(problemid, processid, wiki, reference, event_eventid, event_reference, userid,
  							processTimeout, hasLoop, debug, processStartTime, params, eventTask, token, rsviewIdGuid,
  							checkPreConditionNullVars);
                      if(exit) {
                      	return;
                      }
                }
                else
                {
                    startNewRunbook(processid, wiki, processTimeout, hasLoop, debug, mock, processStartTime, params,
							properties, start, token, rsviewIdGuid, checkPreConditionNullVars);
                }
            }
            
            // set runbookStartLatency in msecs
            long latency = System.currentTimeMillis() - Long.parseLong((String) params.get(Constants.EXECUTE_PROCESS_STARTTIME));
            runbookStartLatency.getAndAdd(latency);
            
            if (Log.log.isDebugEnabled()) {
                Log.log.debug("ExecuteProcess initialized processid: " + processid);
            }
            debug.println("ExecuteProcess initialized processid: " + processid);
            
            if(params.containsKey(Constants.EXECUTE_INCIDENT_ID)) {
                String rootResolutionId = (String) params.get(Constants.EXECUTE_ROOT_RUNBOOK_ID);
                String incidentId = (String) params.get(Constants.EXECUTE_INCIDENT_ID);
    
                EventMessage em = EventMessageFactory.getInstance().getIncidentResolutionBeganMessage(incidentId, rootResolutionId);
                Main.esb.sendInternalMessage(Constants.ESB_NAME_EVENT_SOURCING_DCS, "", em.toJSON());
            }

            RSControlMListenerUtil.addActiveRunbook(processid);
        }
        catch (Throwable e)
        {
            Log.log.error("Fail to start runbook: " + wiki + ". " + e.getMessage(), e);
            result = e.getMessage();
        }
        finally
        {
            WorksheetUtils.writeDebugCAS(debug);
        }
        
        ExecutionTimeUtil.log((String) params.get("RESOLVE.JOB_ID"), null, null, ExecutionStep.METHOD_EXECUTION, ExecutionStepPhase.END);

    } // startProcess

	private static boolean resumeRunbook(String problemid, String processid, String wiki, String reference,
			String event_eventid, String event_reference, String userid, long processTimeout, boolean hasLoop,
			ScriptDebug debug, long processStartTime, Map params, ActionProcessTaskRequest eventTask, String token,
			String rsviewIdGuid, boolean checkPreConditionNullVars) throws Exception {
		debug.println("StartProcess", "Continue Runbook Debug");
		if (Log.log.isDebugEnabled()) {
		    Log.log.debug("StartProcess - Continue Runbook");
		}

		// set parentWiki
		String parentWiki = (String) params.get(Constants.EXECUTE_PARENTWIKI);
		if (StringUtils.isEmpty(parentWiki))
		{
		    parentWiki = wiki;
		}

		if (getProcessState(processid) == null)
		{
		    if (!isProcessHibernated(processid))
		   {
		        params.put(Constants.EXECUTE_PROCESSID, processid);
		        Main.esb.sendMessage(Constants.ESB_NAME_RSCONTROLS, "MAction.wakeupExistingProcess",params);
		        return true;
		    }

		    Log.log.error("isProcessHibernated returned true for processid=" + processid);                    	

		    // If reach here, process will be waken up by this rscontrol
		    // added new processState
		    if (getProcessState(processid) == null)
		    {
		        createProcessState(processid, wiki, hasLoop, processStartTime, processTimeout, token, rsviewIdGuid, 
		        				   checkPreConditionNullVars, (String)params.get(Constants.EXECUTE_PROCESS_NUMBER));
		    }

		    // processState
		    ReentrantLock stateLock = LockUtils.getLock(Constants.LOCK_PROCSTATUSUPDATE + processid, processid);
		    try
		    {
		    	stateLock.lock();
		        // processState
		        ProcessState processState = null;

		        //for certain cases we use process id as the execute state's id
		        List<ExecuteState> les = WorksheetUtil.getEventRowsByProcessId(processid);
		        ExecuteState cl = null;
		        for (ExecuteState es: les)
		        {
		        	// ES use empty string as process state value
		        	if (StringUtils.isNotEmpty(es.getProcessState()))
		        	{
		        		cl = es;
		        		break;
		        	}
		        }
		        
		        String UProcessState = cl != null ? cl.getProcessState() : null;
		        if (UProcessState != null)
		        {
		            processState = (ProcessState) StringUtils.stringToObj(UProcessState);
		            if (processState == null)
		            {
		               processState = new ProcessState();
		               processState.setStartTime(processStartTime);
		            }
		            else
		            {
		            	ConcurrentMap<String,Object> snapShot = processState.getGlobalsSnapshot();
		            	if (snapShot!=null)
		            	{
		            		getProcessInfo(processid).setProcessMap(snapShot, processid);
		            	}
		            	processState.clearGlobalsSnapshot();
		            	
		                if (ProcessRequest.getModelStates(processState.processid)!=null)
		                {
		                    for (String id : processState.requestedMap.keySet())
		                    {
		                        ConcurrentMap<String,Object> requested = processState.requestedMap.get(id);
		                        if (requested!=null)
		                        {
		                            Map<String, Object> modelMap = ProcessRequest.getModelStates(processState.processid);
		                            if (modelMap!=null)
		                            {
		                                Map<String, ActionProcessTaskRequest> aptrMap = (Map<String, ActionProcessTaskRequest>)modelMap.get(ProcessRequest.EXECID_TASKREQ);
		                                if (aptrMap!=null)
		                                {
		                                    ActionProcessTaskRequest aptr = aptrMap.get(id);
		                                    if (aptr!=null)
		                                    {
		                                        aptr.setRequestedMap(requested);
		                                    //((Map<String, ActionProcessTaskRequest>)ProcessRequest.getModelStates(processState.processid).get(ProcessRequest.EXECID_TASKREQ)).get(id).setRequestedMap(requested);
		                                    }
		                                }
		                            }
		                        }
		                    }
		                }
		            }
		        }
		        ProcessExecution.setProcessState(processid, processState);
		    }
		    catch (Throwable e)
		    {
		        Log.log.error(e.getMessage(), e);
		        throw new RuntimeException(e);
		    } 
		    finally {
		    	stateLock.unlock();
		    }
		}

		HashMap content = new HashMap();
		content.putAll(params);
		content.put(Constants.EVENT_EVENTID, event_eventid);
		content.put(Constants.EVENT_REFERENCE, event_reference);
		content.put(Constants.EXECUTE_EXECUTEID, eventTask.getExecute());
		content.put(Constants.EXECUTE_PROCESSID, processid);
		content.put(Constants.EXECUTE_PROBLEMID, problemid);
		content.put(Constants.EXECUTE_WIKI, wiki);
		content.put(Constants.EXECUTE_REFERENCE, reference);
		content.put(Constants.EXECUTE_USERID, userid);
		EventHandler.eventEventHandler(content);
		
	    return false;
	}

	private static void startNewRunbook(String processid, String wiki, long processTimeout, boolean hasLoop,
			ScriptDebug debug, String mock, long processStartTime, Map params, ActionTaskProperties properties,
			ArrayList start, String token, String rsviewIdGuid, boolean checkPreConditionNullVars) throws Exception {
		debug.println("StartProcess", "Start New Runbook Debug");
		if (Log.log.isDebugEnabled()) {
		    Log.log.debug("StartProcess - Start New Runbook");
		}

		// Check precondition null varaibles is no more optional, it will be set to true
		ProcessState ps = createProcessState(processid, wiki, hasLoop, processStartTime, processTimeout, token, rsviewIdGuid, 
						   true, (String)params.get(Constants.EXECUTE_PROCESS_NUMBER));
		
		ps.mockValue = StringUtils.isEmpty(mock) ? null: mock;
		
		// runbook transaction metric
		runbookTotal.incrementAndGet();
		
		if(start.size() > 0) {
		    ActionProcessTaskRequest startTask = (ActionProcessTaskRequest) start.get(0);
		    if (Log.log.isDebugEnabled()) {
		        Log.log.debug("Executing start task: " + startTask.getNumber());
		    }
		    executeTask(startTask.getExecute(), startTask.getProcess(), wiki, startTask.getTask(), "NOW", startTask.getProblem(), startTask.getReference(), startTask.getUserid(), "", params, debug, mock, processTimeout, properties, token, rsviewIdGuid, checkPreConditionNullVars);
		    ExecutionTimeUtil.log((String) params.get("RESOLVE.JOB_ID"), processid, null, ExecutionStep.RUNBOOK_INIT, ExecutionStepPhase.END);
		}
	}

	private static void scheduleRunbookAbort(String problemid, String processid, String wiki, String reference,
			String userid, long processTimeout, boolean hasLoop, boolean isEvent, Map params, boolean isAbort) {
		boolean isRunbookExecution = !isAbort && !isEvent;
		if (isRunbookExecution)
		{
		    String jobname = "PROCESSABORT/" + processid;
		    Map abortParams = new HashMap(params);
		    abortParams.put(Constants.EXECUTE_PROCESSID, processid);
		    abortParams.put(Constants.EXECUTE_WIKI, Constants.EXECUTE_ABORTWIKI + wiki);
		    abortParams.put(Constants.EXECUTE_PROBLEMID, problemid);
		    abortParams.put(Constants.EXECUTE_USERID, userid);
		    abortParams.put(Constants.EXECUTE_REFERENCE, reference);
		    abortParams.put(Constants.EXECUTE_PROCESS_TIMEOUT, "" + processTimeout);
		    abortParams.put(Constants.EXECUTE_PROCESS_LOOP, "" + hasLoop);
		    if(params.containsKey("RESOLVE.JOB_ID")) {
		    	abortParams.put("RESOLVE.JOB_ID", params.get("RESOLVE.JOB_ID"));
		    }
		    ScheduledExecutor.getInstance().executeDelayed(jobname, MAction.class, "abortProcess", abortParams, processTimeout, TimeUnit.SECONDS);
		}
	}

    static boolean isProcessHibernated(String processid)
    {
    	boolean result = false;
    	List<ExecuteState> states = WorksheetUtil.getEventRowsByProcessId(processid);
    	if (states!=null)
    	{
    		for (ExecuteState es: states)
    		{
    			if (es.getProcessState()!=null)
    			{
    				result=true;
    				break;
    			}
    		}
    	}
    	return result;
    }

    @SuppressWarnings("unchecked")  
    public static String taskCompleted(final String processid, final String executeid, final String problemid, final String wiki, final String targetAddr, final String targetGuid, final String targetAffinity, final String userid, String actionName, AssessResult assessResult, Map outputs, Map params, Map flows, ScriptDebug debug, String mock, int processTimeout, ActionTaskProperties properties)
    {
        ExecutionTimeUtil.log((String) params.get("RESOLVE.JOB_ID"), null, null, ExecutionStep.METHOD_EXECUTION, ExecutionStepPhase.START);

        String result = "";
        boolean isEnd = false;
        boolean finished = false;
        
        try
        {
        	if (Log.log.isDebugEnabled()) {
        		Log.log.debug(String.format("TaskCompleted Label: %s actionName: %s processid: %s wiki: %s targetAddr: %s " +
        									"targetGuid: %s targetAffinity: %s executeid: %s",
        									ActionProcessTaskRequest.getTaskLabel(executeid), actionName, processid, wiki,
        									targetAddr, targetGuid, targetAffinity, executeid));
        		Log.log.debug(String.format("outputs: %s", StringUtils.mapToLogString(outputs)));
        		Log.log.debug(String.format("params: %s", StringUtils.mapToLogString(params)));
        		Log.log.debug(String.format("flows: %s", StringUtils.mapToLogString(flows)));
        	}
        	
            if (Log.log.isDebugEnabled() && StringUtils.isNotEmpty(mock)) {
                Log.log.debug(String.format("mock:%s", mock));
            }
            
            if (debug.isDebug()) {
	            debug.setComponent(String.format("TaskCompleted Label: %s actionName: %s", 
	            								 ActionProcessTaskRequest.getTaskLabel(executeid), actionName));
	            debug.println(String.format("TaskCompleted Label: %s actionName: %s processid: %s wiki: %s targetAddr: %s " +
	            							"targetGuid: %s targetAffinity: %s executeid: %s",
	            			  				ActionProcessTaskRequest.getTaskLabel(executeid), actionName, processid, wiki,
	            			  				targetAddr, targetGuid, targetAffinity, executeid));
	            debug.println(String.format("  outputs: %s", StringUtils.mapToLogString(outputs)));
	            debug.println(String.format("  params: %s", StringUtils.mapToLogString(params)));
	            debug.println(String.format("  flows: %s", StringUtils.mapToLogString(flows)));
            }
            
            if (StringUtils.isNotEmpty(mock))
            {
            	if (debug.isDebug()) {
            		debug.println(String.format("  mock: %s", mock));
            	}
            }
            
            // Increment AT execution count in process state
            ProcessState state = StringUtils.isNotBlank(processid) ? getProcessState(processid) : null;
            
            if (state != null) {
            	state.taskCompleted();
            } else {
            	if (StringUtils.isNotBlank(processid) && Log.log.isDebugEnabled()) {
            		Log.log.debug(String.format("Failed to find process state for process id %s as it may have already " +
            									"been deleted", processid));
            	}
            }
            
            // check if final end
            // boolean finished = false;
            if (actionName.equalsIgnoreCase("END"))
            {
                isEnd = true;

                String end_quit = (String) params.get("END_QUIT");
                if (end_quit == null || end_quit.equalsIgnoreCase("TRUE"))
                {
                    finished = true;

                    // complete process
                    String event_reference = (String) params.get(Constants.EVENT_REFERENCE);
                    completeProcess(processid, wiki, problemid, event_reference, debug, params);
                }
            }
            
            // write debug
            WorksheetUtils.writeDebugCAS(debug);

            if (StringUtils.isNotBlank(processid) && StringUtils.isNotBlank(wiki) && !finished)
            {
                // wait for dependency init completion
            	if (Log.log.isDebugEnabled()) {
            		Log.log.debug(String.format("taskCompleted waitForDependInit processid: %s", processid));
            	}
            	
                if (waitForDependInit(processid))
                {
                    // update process state
                    if (state != null)
                    {
                        String wikiId;
                        ReentrantLock stateLock = LockUtils.getLock(Constants.LOCK_PROCSTATUSUPDATE + processid, processid);
                        boolean allowLoop = false;
                        try
                        {
                            stateLock.lock();
                            state = getProcessState(processid);

                            // update activity
                            state.setLastActivityTime(System.currentTimeMillis());

                            // check whether looping is allowed
                            allowLoop = state.isHasLoop();

                            // update end summary
                            wikiId = (String) params.get(Constants.EXECUTE_WIKI_WIKIID);
                            state.updateEndSummary(wikiId, assessResult.getCondition(), assessResult.getSeverity());

                            // notify JMS replication / update
                            if (Log.log.isDebugEnabled()) {
                            	Log.log.debug(String.format("calling setProcessState - processid: %s state processid: %s", 
                            								processid, state.getProcessid()));
                            }
                            
                            setProcessState(state.getProcessid(), state);
                        }
                        catch (Throwable e)
                        {
                            Log.log.error(e.getMessage(), e);
                            throw new RuntimeException(e);
                        }
                        finally
                        {
                            WorksheetUtils.writeDebugCAS(debug);
                            stateLock.unlock();
                        }

                        // get list of ActionTask requests depending on this tasks
                        
                        List<DirectedTreeNode> depends = ((Map<String, ActionProcessTask>) (ProcessRequest.getModelStates(processid).get(ProcessRequest.EXECID_TASK))).get(executeid).graphNode.getAllChildren();
                        if (depends != null && !depends.isEmpty())
                        {
                            // iterate through all dependent tasks
                            if (Log.log.isDebugEnabled()) {
                            	Log.log.debug(String.format("# of Dependent Tasks: %d", depends.size()));
                            }
                            

                            for (DirectedTreeNode dependent : depends)
                            {
                                if (finished)
                                {
                                    break;
                                }

	                            ReentrantLock lock = LockUtils.getLock(Constants.LOCK_TASKCOMPLETED + processid + dependent.getId(), processid);
	                            
                                String dependentId = (String) ((Map) ProcessRequest.getModelStates(processid).get(ProcessRequest.TASK_EXECID)).get(dependent.getValue());
                                ActionProcessTaskRequest dependentTask = (ActionProcessTaskRequest) ((Map) ProcessRequest.getModelStates(processid).get(ProcessRequest.EXECID_TASKREQ)).get(dependentId);
                                
                                if (Log.log.isDebugEnabled()) {
                                	Log.log.debug(String.format("** DEPENDENTTASK: %s, %s", dependentTask.getTaskname(), 
                                								dependent.getId()));
                                }

                                Targets dependentTargets = dependentTask.getTargets(params);

                                try
                                {
                                    lock.lock();
                                    if (Log.log.isDebugEnabled()) {
	                                    Log.log.debug(String.format("dependentTask: %s, %s", dependentTask.getTaskLabel(), 
	                                    							dependentTask.getTaskname()));
	                                    Log.log.debug(String.format("allowLoop: %b", allowLoop));
	                                    Log.log.debug(String.format("targetAddr: %s", targetAddr));
	                                    Log.log.debug(String.format("dependentTask.isRequested(%s): %b", targetAddr, 
	                                    							dependentTask.isRequested(targetAddr)));
	                                    Log.log.debug(String.format("dependentTargets: %s", dependentTargets));
	                                    Log.log.debug(String.format("dependentTargets.has(%s): %b", targetAddr, 
	                                    							dependentTargets.has(targetAddr)));
                                    }

                                    if (allowLoop == true || dependentTask.isRequested(targetAddr) == false)
                                    {
                                        if (Log.log.isDebugEnabled()) {
                                        	Log.log.debug(String.format("Evaluating Next Task: %s, %s - %s targetAddr: %s", 
                                        								dependentTask.getTaskLabel(), 
                                        								dependentTask.getTaskname(), 
                                        								dependentId, targetAddr));
                                        }
                                        
                                        if (ProcessRequest.runningModels.get(processid) == null)
                                        {
                                        	if (Log.log.isDebugEnabled()) {
                                        		Log.log.debug(String.format("Runbook was already finished. Bypass result " +
                                        									"evaluation for processid: %s", processid));
                                        	}
                                            break;
                                        }
                                        
                                        if (dependentTask.evaluate(processid, problemid, assessResult.isCompleted(), 
                     						   					   assessResult.getCondition(), assessResult.getSeverity(), 
                     						   					   actionName, executeid, targetAddr, params, 
                     						   					   outputs, flows, dependent.getId()))
                                        {
                                            Map taskParams = dependentTask.getParams();
                                            taskParams.putAll(params);

                                            // get targetInst
                                            TargetInstance targetInst = null;
                                            
                                            String orgSuffix = "";
                                            
                                            if (StringUtils.isNotBlank((String)params.get(Constants.EXECUTE_ORG_ID)) &&
                                                StringUtils.isNotBlank((String)params.get(Constants.EXECUTE_ORG_NAME)))
                                            {
                                                orgSuffix = ((String)params.get(Constants.EXECUTE_ORG_NAME))
                                                			.replaceAll("[^0-9A-Za-z_]", "").toUpperCase();
                                            }
                                            
                                            // reset back to RSREMOTE for merge targets
                                            if (dependentTask.isMergedTargets() == true)
                                            {
                                                targetInst = new TargetInstance("RSREMOTE", 
                                                								Constants.ESB_NAME_RSREMOTE + orgSuffix);
                                            }
                                            else
                                            {
                                                // check if targetInst exists for the targetAddr
                                                targetInst = dependentTargets.get(targetAddr);
                                                if (targetInst == null)
                                                {
                                                    // reset to RSREMOTE if targetGuid not defined or RSCONTROL
                                                    if (StringUtils.isEmpty(targetGuid) || 
                                                    	targetGuid.equals(Constants.ESB_NAME_RSCONTROL))
                                                    {
                                                        targetInst = new TargetInstance(targetAddr, 
                                                        								Constants.ESB_NAME_RSREMOTE + 
                                                        								orgSuffix);
                                                    }
                                                    // use targetGuid
                                                    else
                                                    {
                                                        targetInst = new TargetInstance(targetAddr, targetGuid);
                                                    }
                                                }
                                            }

                                            // update wiki
                                            String nextWiki = wiki;
                                            if (isEnd)
                                            {
                                                if (wikiId != null)
                                                {
                                                    nextWiki = ProcessRequest.getWikiFromWikiId(wikiId);
                                                    if (nextWiki == null)
                                                    {
                                                        nextWiki = wiki;
                                                    }
                                                }

                                            }
                                            
                                            executeNextTask(dependentTask, nextWiki, targetInst, outputs, taskParams, flows, debug, mock, processTimeout, properties, state.getToken(), state.getRsviewIdGuid());
                                            
                                        }
                                    }
                                }
                                catch (ProcessCompletedConcurrencyException e)
                                {
                                    Log.log.warn(e.getMessage());
                                }
                                catch (Throwable e)
                                {
                                	if (!e.getMessage().startsWith(String.format("Missing modelStates for processid: %s. " +
											 									 "Process may have completed", processid)))
                                    {
                                        // RBA-14583 : Create a black list of commands
                                    	// handling of ActionTaskAbortException added in order to not see that 
                                		// exception in the logs
                                    	if (e instanceof ActionTaskAbortException && 
                                    		e.getLocalizedMessage()
                                    		.startsWith(Constants.EXCEPTION_PREFIX_BLACKLISTED_COMMANDS))
                                    	{
                                    		Log.log.error(e.getMessage());
                                    	}
                                    	else
                                    	{
                                    		Log.log.error(e.getMessage(), e);
                                    	}
                                    }
                                    
                                    if (e.getMessage().startsWith("Expression Evaluation Error :"))
                                    {
                                        // send Expression Evaluation Failed message
                                    	String summary = String.format("Failed to evaluate pre condition to execute " +
                 							   						   "next action task %s in wiki %s", 
                 							   						   dependentTask.getTaskLabel(), wiki);
                                    	String detail = String.format("Failed to evaluate pre condition to execute " +
                                    								  "next action task %s (%s.%s) which depends on " +
                                    								  "completed action task %s (%s) Error Message: [%s]" +
                                    								  " in wiki %s", 
                                    								  dependentTask.getTaskLabel(), 
                                    								  dependentTask.getTasknamespace(), 
                                    								  dependentTask.getTaskname(), 
                                    								  ActionProcessTaskRequest.getTaskLabel(executeid), 
                                    								  actionName, e.getMessage(), wiki);
                                        Map<String, String> abortParam = new HashMap<String, String>();
                                        abortParam.put(Constants.EXECUTE_SUMMARY, summary);
                                        abortParam.put(Constants.EXECUTE_DETAIL, detail);
                                        abortParam.put(Constants.EXECUTE_PROCESSID, processid);
                                        abortParam.put(Constants.EXECUTE_PROBLEMID, problemid);
                                        abortParam.put(Constants.EXECUTE_USERID, userid);
                                        if(params.containsKey("RESOLVE.JOB_ID")) {
                                        	abortParam.put("RESOLVE.JOB_ID", (String) params.get("RESOLVE.JOB_ID"));
                                        }
                                        MAction.abortProcess(abortParam);
                                        Log.log.info(detail);
                                        throw new Exception(summary); 
                                    }
                                }
                                finally
                                {
                                    WorksheetUtils.writeDebugCAS(debug);
                                    lock.unlock();
                                }

                                finished = isProcessCompleted(processid);
                            }
                            
                            // remove lock
                            LockUtils.removeLock(Constants.LOCK_TASKCOMPLETED + processid + executeid);
                        }
                        else
                        {
                        	if (Log.log.isDebugEnabled()) {
                        		Log.log.debug(String.format("No more dependencies on processid: %s executeid: %s", 
                        									processid, executeid));
                        	}
                        }
                    }
                }
                else
                {
                	Log.log.warn(String.format("waitForDependInit() failed for processid: %s", processid));
                }
            }
        }
        catch (Throwable e)
        {
        	String errMsg = StringUtils.isNotBlank(e.getMessage()) ? e.getMessage() : "No message";
        	if (debug.isDebug()) {
        		debug.println(errMsg, e);
        	}
        	
        	if (errMsg.startsWith("Failed to evaluate pre condition to execute ")) {
        		Log.log.warn(errMsg);
        	} else {
        		Log.log.error(errMsg, e);
        	}
            result = errMsg;
            throw new RuntimeException(e);
        }
        finally
        {
        	if (finished) {
            	if (Log.log.isDebugEnabled()) {
            		Log.log.debug(String.format("Process Finished Last TaskCompleted actionName: %s processid: %s " +
            									"wiki: %s targetAddr: %s targetGuid: %s targetAffinity: %s executeid: %s", 
            									actionName, processid, wiki, targetAddr, targetGuid, targetAffinity, 
            									executeid));
            	}
            } else {
            	if (Log.log.isDebugEnabled()) {
            		Log.log.debug(String.format("Last TaskCompleted actionName: %s processid: %s wiki: %s " +
 						   						"targetAddr: %s targetGuid: %s targetAffinity: %s executeid: %s", 
 						   						actionName, processid, wiki, targetAddr, targetGuid, targetAffinity, 
 						   						executeid));
            	}
            }
            
            if (debug.isDebug()) {
            	debug.println("End taskCompleted");
            }
            
            WorksheetUtils.writeDebugCAS(debug);
        }

        ExecutionTimeUtil.log((String) params.get("RESOLVE.JOB_ID"), null, null, ExecutionStep.METHOD_EXECUTION, ExecutionStepPhase.END);
    	
        return result;
    } // taskCompleted

    public static boolean isProcessCompleted(String processid)
    {
        return !ProcessContext.processExists(processid);
    } // isProcessCompleted

    private static void completeProcess(String processid, String wiki, String problemid, String event_reference, ScriptDebug debug, Map params)
    {
    	ExecutionTimeUtil.log((String) params.get("RESOLVE.JOB_ID"), processid, null, ExecutionStep.RUNBOOK_FINALIZE, ExecutionStepPhase.START);
    	
        // update completed count
        updateCompleted(processid, wiki, params);

        // remove process
        removeProcess(processid, wiki, problemid, false, false, event_reference, true, params);
        
        ExecutionTimeUtil.log((String) params.get("RESOLVE.JOB_ID"), processid, null, ExecutionStep.RUNBOOK_FINALIZE, ExecutionStepPhase.END);
        ExecutionTimeUtil.exportExecutionTimeStatistics((String) params.get("RESOLVE.JOB_ID"));
        debug.debug("Runbook Completed");
    } // completeProcess

    public static void abortProcessCAS(Map params)
    {
        ExecutionTimeUtil.log((String) params.get("RESOLVE.JOB_ID"), null, null, ExecutionStep.METHOD_EXECUTION, ExecutionStepPhase.START);
        
        String processid = (String) params.get(Constants.EXECUTE_PROCESSID);
        String wiki = (String) params.get(Constants.EXECUTE_WIKI);
        String userid = (String) params.get(Constants.EXECUTE_USERID);
        String summary = (String) params.get(Constants.EXECUTE_SUMMARY);
        String detail = (String) params.get(Constants.EXECUTE_DETAIL);
        String event_reference = (String) params.get(Constants.EVENT_REFERENCE);

        String problemid = (String) params.get(Constants.EXECUTE_PROBLEMID);
        String problemNum = (String) params.get(Constants.EXECUTE_PROBLEM_NUMBER);
        String activityId = params.containsKey(Constants.ACTIVITY_ID) ? (String)params.get(Constants.ACTIVITY_ID) : "";
        
        // init summary and detail
        if (StringUtils.isEmpty(summary))
        {
            summary = "Process Aborted";
        }
        if (StringUtils.isEmpty(detail))
        {
            detail = "Process Aborted";
        }

        // init wiki
        if (wiki != null && wiki.startsWith(Constants.EXECUTE_ABORTWIKI))
        {
            wiki = wiki.substring(Constants.EXECUTE_ABORTWIKI.length());
        }

        boolean canRemove = false;
        try
        {
            if (!StringUtils.isEmpty(processid))
            {
                Log.log.info("Process Aborted: " + processid + " Process Count: " + ProcessExecution.processDependency.getProcessCount());
                canRemove = true;

                // abort execute requests
                String status = Constants.ASSESS_STATUS_COMPLETED;
                //ColumnList<String> processCL = CassandraUtil.findCFRowByKey(processid, CassandraUtil.getProcessRequestCF(), new String[] { "u_status", "u_problem", "u_wiki", "u_number", "sys_created_on" });
                com.resolve.search.model.ProcessRequest processCL = ServiceWorksheet.findProcessRequestById(processid);
                if(processCL != null)
                {
                    String processNum = processCL.getNumber();
                    //CASHelper cah = CassandraUtil.getUpdateMutation(CassandraUtil.getProcessRequestCF(), processid);
                    long now = System.currentTimeMillis();

                    // abort process request
                    status = processCL.getStatus();
                    if (!status.equals(Constants.ASSESS_STATUS_COMPLETED))
                    {
                        processCL.setStatus(Constants.ASSESS_STATUS_ABORTED);
                    }

                    // add resolve_action_result
                    problemid = processCL.getProblem();
                    if (problemid != null)
                    {
                        // get wiki
//                        if (StringUtils.isEmpty(wiki))
//                        {
                            wiki = processCL.getWiki();
//                        }

                        Worksheet worksheet = ServiceWorksheet.findWorksheetById(problemid);
                        worksheet.setCondition(Constants.ASSESS_CONDITION_BAD);
                        worksheet.setSeverity(Constants.ASSESS_SEVERITY_CRITICAL);
                        worksheet.setSysUpdatedOn(System.currentTimeMillis());
                        worksheet.setModified(true);

                        if(params.containsKey(Constants.EXECUTE_INCIDENT_ID)) {
                            String rootResolutionId = (String) params.get(Constants.EXECUTE_ROOT_RUNBOOK_ID);
                            String incidentId = (String) params.get(Constants.EXECUTE_INCIDENT_ID);
                            
                            if(params.containsKey(Constants.EXECUTE_ACTION_TASK_ID)) {
                                String taskId = (String) params.get(Constants.EXECUTE_ACTION_TASK_ID);
                               
                                EventMessage em = EventMessageFactory.getInstance().getIncidentResolutionTaskAbortedMessage(incidentId, taskId, rootResolutionId);
                                Main.esb.sendInternalMessage(Constants.ESB_NAME_EVENT_SOURCING_DCS, "", em.toJSON());
                            }
                            
                            //Updating the status of the execution - severity and condition are critical and bad.
                            EventMessage em = EventMessageFactory.getInstance()
                                            .getIncidentResolutionUpdatedMessage(incidentId, rootResolutionId, worksheet.getSummary(), worksheet.getCondition(), worksheet.getSeverity());
                            MainBase.esb.sendInternalMessage(Constants.ESB_NAME_EVENT_SOURCING_DCS, "", em.toJSON());
                            
                            //Aborting the resolution and incident aswell
                            em = EventMessageFactory.getInstance().getIncidentResolutionAbortedMessage(incidentId, rootResolutionId);
                            Main.esb.sendInternalMessage(Constants.ESB_NAME_EVENT_SOURCING_DCS, "", em.toJSON());
                            
                            em = EventMessageFactory.getInstance().getIncidentAbortedMessage(incidentId);
                            Main.esb.sendInternalMessage(Constants.ESB_NAME_EVENT_SOURCING_DCS, "", em.toJSON());
                        }

                        // insert abort task and result
                        insertAbortTaskResult(processid, wiki, summary, detail, problemid, processNum, userid, worksheet.getNumber(), activityId);

                        
                        String jobId = (String) ProcessContext.get(processid, "RESOLVE.JOB_ID");
                        if(jobId != null) {
                            Log.log.info("acknowledging runbook with processid: " + processid);
                            MAmqpServer mServer = (MAmqpServer) Main.main.mServer;
                            MAmqpRunbookExecutionListener.acknowledgeRunbookExecution(jobId, mServer);
                        }

                        // execute abort process with new processid
                        params.remove(Constants.EXECUTE_PROCESSID);

                        params.put(Constants.ESB_PARAM_EVENTTYPE, Constants.GATEWAY_EVENT_TYPE_RSCONTROL_ABORT);
                        params.put(Constants.EXECUTE_PROBLEMID, problemid);
                        params.put(Constants.EXECUTE_USERID, userid);
                        params.put(Constants.EXECUTE_WIKI, Constants.EXECUTE_ABORTWIKI + wiki);
                        Main.esb.sendInternalMessage(Constants.ESB_NAME_RSCONTROL, "MAction.executeProcess", params);

                        ServiceWorksheet.updateWorksheet(worksheet);
                    }
                }
                // update aborted count
                
            	
                updateAborted(wiki);
                
                ProcessState state = getProcessState(processid);
                if (state != null && state.getAtExecCount() > 0) {
                	// Account for abort task result while setting total execution count
                	processCL.setAtExecCount(Integer.valueOf(state.getAtExecCount() + 1));
                } else {
                	Log.log.error(String.format("Failed to find process state for process id %s and update AT execution " +
                							    "count", processid));
                }
                
                ServiceWorksheet.updatProcessRequest(processCL, userid);
            }
        }
        catch (Throwable e)
        {
            Log.log.warn(e.getMessage(), e);
        }
        finally
        {
            // remove process
            if (canRemove)
            {
                removeProcess(processid, wiki, problemid, true, false, event_reference, true, params);
            }
            ExecutionTimeUtil.log((String) params.get("RESOLVE.JOB_ID"), null, null, ExecutionStep.METHOD_EXECUTION, ExecutionStepPhase.END);
            
        }
    } // abortProcessCAS

    public static void insertAbortTaskResult(String processid, String wiki, String summary, String detail, String problemid, String processNum, String userid, String problemNum, String activityId)
    {
        Map<String,Object> arMap = new HashMap<String,Object>();

        if (StringUtils.isEmpty(processNum))
        {
            com.resolve.search.model.ProcessRequest processCL = ServiceWorksheet.findProcessRequestById(processid);
            processNum = processCL.getNumber();
        }

        if (StringUtils.isEmpty(userid))
        {
            Log.log.warn("Missing USERID");
            userid = "system";
        }

        // insert abort execute request
        String execId = SearchUtils.getNewRowKey();

        arMap.put("u_wiki", wiki);
        arMap.put("u_problem", problemid);
        arMap.put("u_problem_num", problemNum);
        arMap.put("u_timestamp", System.currentTimeMillis());
        arMap.put("u_process", processid);
        arMap.put("u_process_number", processNum);

        arMap.put("u_execute_request_id", execId);
        arMap.put("u_execute_result_id", "");

        arMap.put("u_actiontask", abortActionTask.getSys_id());
        arMap.put("u_actiontask_name", abortActionTask.getUName());
        arMap.put("u_actiontask_full_name", abortActionTask.getUFullName());
        arMap.put("u_task_namespace", abortActionTask.getUNamespace());
        arMap.put("u_task_summary", abortActionTask.getUSummary());
        arMap.put("u_task_hidden", ""+abortActionTask.getUIsHidden());
        arMap.put("u_task_tags", abortActionTask.getTagsAsCSV());
        arMap.put("u_address", Constants.ESB_NAME_RSCONTROL);
        arMap.put("u_esbaddr", Constants.ESB_NAME_RSCONTROL);
        arMap.put("u_target_guid", MainBase.main.configId.getGuid());

        arMap.put("u_completion", Constants.ASSESS_COMPLETION_FALSE);
        arMap.put("u_condition", Constants.ASSESS_CONDITION_BAD);
        arMap.put("u_severity", Constants.ASSESS_SEVERITY_CRITICAL);
        arMap.put("u_duration", 0);
        
        arMap.put("u_summary", summary);
        arMap.put("u_detail", detail);
        arMap.put("u_raw", "");

        arMap.put("u_wiki", wiki);
        //since abort is triggered by rscontrol there is no need of node id. 
        arMap.put("u_node_id", "");

        arMap.put("sys_created_by", userid);
        arMap.put("sys_updated_by", userid);
        arMap.put("activityId", activityId);

        boolean updateSir = ((Main) MainBase.main).configGeneral.getUpdateSir();
        ServiceWorksheet.createNewActionResult(arMap, updateSir);
    } // insertAbortTaskResult

    
    public static void adjustProcessConcurrentLimit(Map<String, Object> map, String guid)
    {
        String processid = (String)map.get(Constants.EXECUTE_PROCESSID);
        String wiki = (String)map.get(Constants.EXECUTE_WIKI);
        
        if (Log.log.isDebugEnabled()) {
            Log.log.debug("removeProcess processid: " + processid);
        }

        if (!StringUtils.isEmpty(wiki))
        {
        	String realWiki = wiki;
            if (wiki.startsWith("ABORT:"))
            {
                realWiki = wiki.substring(6);
            }

            if (realWiki.indexOf(":")>0)
            {
            	realWiki = wiki.substring(0, wiki.indexOf(":"));
            }
            
            //Counter counter = MAction.runningRunbooks.get(wiki);
            if (StringUtils.isNotEmpty(MessageHandler.getConcurrentRBLimit(realWiki))) // && MessageHandler.getConcurrentRBCounter(wiki).get()>0)
            {
            	final String frealWiki = realWiki;
            	ExecutableObject exeObj = new ExecutableObject()
            	{
            		public Object execute()
            		{
		                	boolean launched = false;
		                    int limit = -1;
		                    String oldLimitStr = MessageHandler.getConcurrentRBLimit(frealWiki);
		                    if (StringUtils.isNotEmpty(oldLimitStr))
		                    {
		                        limit = Integer.parseInt(oldLimitStr);
		                    }

		                    int counter = MessageHandler.getConcurrentRBCounter(frealWiki); 
		                    if (limit>0 && counter<limit)
		                    {
			                    Queue<ScheduledTask> queue = MAction.waitingRunbookQueues.get(frealWiki);
			                    if (queue != null)
			                    {
			                    	ScheduledTask task = queue.poll();
			                        if (task != null)
			                        {
			                           TaskExecutor.execute(task.jobid, task.header, task.invokeClass, task.methodname, task.params);
			                           launched = true;
			                           if (Log.log.isDebugEnabled()) {
			                               Log.log.debug("adjustProcessConcurrentLimit: a runbook execution request for " + frealWiki + " has been removed from queue. Current waiting queue length=" + queue.size());
			                           }
			                        }
			                    }
		                    }
		                    
		                    int now = 0;
		                    
		                    if (launched)
		                    {
		                    	now = MessageHandler.getConcurrentRBCounter(frealWiki);
		                    	++now;
		                    	MessageHandler.setConcurrentRBCounter(frealWiki, now);
		                    }
		                    else
		                    {
		                    	now = MessageHandler.getConcurrentRBCounter(frealWiki);
		                    }
		                    
		                    if (now < 0)
		                    {
		                    	MessageHandler.setConcurrentRBCounter(frealWiki, 0);
		                    	if (Log.log.isDebugEnabled()) {
		                    	    Log.log.debug("removeProcess: try to reset concurrent runbook limit counter to 0. wiki=" + frealWiki + ", updated counter value = " + now);
		                    	}
		                    }
		                    
		                    if (Log.log.isDebugEnabled()) {
		                        Log.log.debug("removeProcess: concurrent runbook execution limit : wiki= " + frealWiki + " counter = " + now);
		                    }
		                    return now;
            		}
            	};
            	HibernateUtil.exclusiveClusterExecution(Constants.EXECUTE_PROCESS_CONCURRENT_LIMIT + wiki, exeObj);
            }
        }    	
    }

    public static void removeProcess(String processid, String wiki, String problemid, boolean abortProcess, boolean isEvent, String event_reference, boolean removeState) {
    	removeProcess(processid, wiki, problemid, abortProcess, isEvent, event_reference, removeState, null);
    }
    
    public static void removeProcess(String processid, String wiki, String problemid, boolean abortProcess, boolean isEvent, String event_reference, boolean removeState, Map params)
    {
    	if(params != null) {
            ExecutionTimeUtil.log((String) params.get("RESOLVE.JOB_ID"), null, null, ExecutionStep.METHOD_EXECUTION, ExecutionStepPhase.START);
    	}

        if (Log.log.isDebugEnabled()) {
            Log.log.debug("removeProcess processid: " + processid);
        }
    	
        if (!StringUtils.isEmpty(wiki))
        {
        	String realWiki = wiki;
            if (wiki.startsWith("ABORT:"))
            {
                realWiki = wiki.substring(6);
            }

            if (realWiki.indexOf(":")>0)
            {
            	realWiki = wiki.substring(0, wiki.indexOf(":"));
            }
            
            String concurrentLimit = "";
            if(params != null) {
               concurrentLimit = (String) params.get(Constants.PROCESS_CONCURRENT_LIMIT);
            }
            if (StringUtils.isNotBlank(concurrentLimit) && concurrentLimit.matches("\\d+") && Integer.valueOf(concurrentLimit) > 0) {
	            if (StringUtils.isNotEmpty(MessageHandler.getConcurrentRBLimit(realWiki))) 
	            {
	            	final String frealWiki = realWiki;
	            	ExecutableObject exeObj = new ExecutableObject()
	            	{
	            		public Object execute()
	            		{
	            			int count = MessageHandler.getConcurrentRBCounter(frealWiki);
	            			--count;
	            			MessageHandler.setConcurrentRBCounter(frealWiki, count);
	            			return count;
	            		}
	               };
	               HibernateUtil.exclusiveClusterExecution(Constants.EXECUTE_PROCESS_CONCURRENT_LIMIT + wiki, exeObj);
	            }
            }
        }
    	
        // send removeProcess replication msg
        Map content = new HashMap();
        content.put(Constants.EXECUTE_PROCESSID, processid);
        content.put(Constants.EXECUTE_GUID, Main.main.configId.guid);
        content.put(Constants.EXECUTE_WIKI, wiki);
        content.put(Constants.EXECUTE_PROBLEMID, problemid);
        content.put(Constants.EXECUTE_PROCESS_NOTEVENT, "" + !isEvent);
        content.put(Constants.EVENT_REFERENCE, event_reference);
        content.put(Constants.EXECUTE_REMOVESTATE, "" + removeState);
        
        TaskExecutor.getCachedThreadPool().execute(new Runnable() {
			
			@Override
			public void run() {
				MAction.removeProcessReplicated(content);
			}
		});
        
        // send process completed
        Main.esb.sendInternalMessage(Constants.ESB_NAME_RSREMOTES, "MSession.processCompleted", processid);
        
        // remove process
        removeProcessCleanup(processid, problemid, abortProcess, isEvent, event_reference, removeState);

    	SearchService.invalidateExecutionCache(processid, problemid);

        if(params != null) {

            String jobId = null;
            if(params.containsKey("RESOLVE.JOB_ID")) {
                jobId = (String) params.get("RESOLVE.JOB_ID");
            } else if(params.containsKey("params")) {
                Map internalParams = (Map) params.get("params");
                if(internalParams.containsKey("RESOLVE.JOB_ID")) {
                    jobId = (String) internalParams.get("RESOLVE.JOB_ID");
                }
            }
            
            if(jobId != null) {
                Log.log.info("acknowledging runbook with processid: " + processid);
                MAmqpServer mServer = (MAmqpServer) Main.main.mServer;
                MAmqpRunbookExecutionListener.acknowledgeRunbookExecution(jobId, mServer);
            }
            
            ExecutionTimeUtil.log((String) params.get("RESOLVE.JOB_ID"), null, null, ExecutionStep.METHOD_EXECUTION, ExecutionStepPhase.END);
        }
        
    } // removeProcess

    public static void removeProcessCleanup(String processid, String problemid, boolean abortProcess, boolean isEvent, String event_reference, boolean removeState)
    {
        if (Log.log.isDebugEnabled()) {
            Log.log.debug("removeProcessCleanup processid: " + processid);
        }
        // get lock for removing process
        ReentrantLock lock = LockUtils.getLock(Constants.LOCK_REMOVEPROCESS+"/"+processid);
        try
        {
            lock.lock();
            if (Log.log.isDebugEnabled() && isEvent)
            {
                try
                {
                    ProcessState ps = getProcessState(processid);
                    if (ps!=null)
                    {
                        SimpleDateFormat sdf = new SimpleDateFormat("MM/dd/yyyy hh:mm:ss.SSS");
                        long tm = ps.getStartTime() + ((long) 1000*ps.getProcessTimeout());
                        Log.log.debug("Event Runbook Abort Time: " + sdf.format(tm) + ", " + processid);
                    }
                }
                catch (Throwable ex)
                {
                    Log.log.error(ex, ex);
                    // throw new RuntimeException(ex);
                }
            }

            // remove processInfo
            removeProcessInfo(processid);

            // remove dependencies
            processDependency.removeProcess(processid, isEvent);

            // remove merge targets
            removeMergeTargets(processid);

            // remove process models
            ProcessRequest.cleanupRunningModel(processid, problemid);

            // remove hazelcast processMap
            ProcessContext.removeProcessMap(processid);

            // remove eventMap
            EventHandler.removeEventMap(processid);

            // remove persisted execute state - no longer removed, it should be
            // removed by the archive when removing the worksheet
            /*
             * if (removeState) { // remove state ProcessTask.removeState(processid,
             * problemid, event_reference); }
             */

            // remove scheduled abort job
            // if (!abortProcess)
            // {
            if (Log.log.isDebugEnabled()) {
                Log.log.debug("removing: PROCESSABORT/" + processid);
            }
            // }

            // cleanup locks
            LockUtils.removeProcessLocks(processid);
        }
        catch (Throwable e)
        {
            Log.log.error(e.getMessage(), e);
        } finally {
            lock.unlock();
        }

        if (Log.log.isDebugEnabled()) {
            Log.log.debug("removeProcessCleanup completed processid: " + processid);
        }
    } // removeProcessCleanup

    public static boolean waitForDependInit(String processid)
    {
        boolean result = false;

        if (!StringUtils.isEmpty(processid))
        {
            if (processDependency.isInitCompleted(processid) == false)
            {
                final int RETRY = 30; // 10 mins
                int duration = 20000; // 20 secs
                
                if (ProcessContext.existsProcessMap(processid))
                {
                    Log.log.info("Check for runbook initialization processid: " + processid);
                    for (int i = 0; !result && i < RETRY; i++)
                    {
                        if (processDependency.isInitCompleted(processid))
                        {
                            result = true;
                        }else {
                        	try {
                        		Log.log.warn("WARNING: Process dependency not initialized, waiting for 20 seconds.");
                            	Thread.sleep(duration);
                        	} catch (InterruptedException ex) {
                        	}
                        }
                    }
                }
                else
                {
                    result = false;
                }
            }
            else
            {
                result = true;
            }
        }
        else
        {
            result = true;
        }

        if (result == false)
        {
            Log.log.info("Runbook already completed or aborted, skipping assessment - processid: " + processid);
        }
        if (Log.log.isDebugEnabled()) {
            Log.log.debug("check runbook initialization timeout: " + (!result));
        }

        return result;
    } // waitForDependInit

    /*
     * Execute task from executeProcess or executeTask
     */
    public static String executeTask(String executeid, 
    		String processid, 
    		String wiki, 
    		String taskid, 
    		String delay, 
    		String problemid, 
    		String reference, 
    		String userid, 
    		String input, 
    		Map params, 
    		ScriptDebug debug, 
    		String mock, 
    		long processTimeout, 
    		ActionTaskProperties properties, 
    		String token, 
    		String rsviewIdGuid, 
    		boolean checkPreConditionNullVars) throws Exception
    {
        ExecutionTimeUtil.log((String) params.get("RESOLVE.JOB_ID"), null, null, ExecutionStep.METHOD_EXECUTION, ExecutionStepPhase.START);
        
        String result = "";

        debug.setComponent("ExecuteTask");
        if (Log.log.isDebugEnabled()) {
            Log.log.debug("ExecuteTask executeid: " + executeid);
        }
        debug.println("ExecuteTask executeid: " + executeid);
        WorksheetUtil.flushWorksheetDebug(debug);
        
        Targets targets = initTargets(params);
        Map<?, ?> outputs = new HashMap<>();
        Map<?, ?> flows = new HashMap<>();
        
        actionRequest(executeid, processid, wiki, taskid, delay, problemid, reference, userid, input, targets, outputs, params, flows, debug, mock, processTimeout, properties, token, rsviewIdGuid);
        ExecutionTimeUtil.log((String) params.get("RESOLVE.JOB_ID"), null, null, ExecutionStep.METHOD_EXECUTION, ExecutionStepPhase.END);
        
        return result;
    } // executeTask

    /*
     * Execute task from TaskCompleted
     */
    public static String executeNextTask(ActionProcessTaskRequest task, String wiki, TargetInstance targetInst, Map outputs, Map params, Map flows, ScriptDebug debug, String mock, int processTimeout, ActionTaskProperties properties, String token, String rsviewIdGuid) throws Exception
    {
        ExecutionTimeUtil.log((String) params.get("RESOLVE.JOB_ID"), null, null, ExecutionStep.METHOD_EXECUTION, ExecutionStepPhase.START);
    	
        String result = "";

        String executeid = task.getExecute();
        Log.log.info("ExecuteNextTask executeid: " + executeid);
        if (Log.log.isDebugEnabled()) {
            Log.log.debug("  params: " + StringUtils.mapToLogString(params));
        }
        WorksheetUtils.writeDebugCAS(debug);

        Targets targets = new Targets();
        targets.add(targetInst);

        // send ActionTask request
        actionRequest(task.getExecute(), task.getProcess(), wiki, task.getTask(), "NOW", task.getProblem(), task.getReference(), task.getUserid(), "", targets, outputs, params, flows, debug, mock, processTimeout, properties, token, rsviewIdGuid);

        ExecutionTimeUtil.log((String) params.get("RESOLVE.JOB_ID"), null, null, ExecutionStep.METHOD_EXECUTION, ExecutionStepPhase.END);
        
        return result;
    } // executeNextTask

    public static String getProcessCount()
    {
        return "Process Count: " + processDependency.getProcessCount();
    } // getProcessCount

    public static String getProcessList()
    {
        return processDependency.toString();
    } // getProcessList

    /**
     * Populate the paramXML execution document with values from the execute
     * request
     *
     * @param request_sid
     *            - sys_id of the execute request to retrieve the values
     * @param paramXML
     *            - execution parameter document to store the values
     * @throws Exception
     */
    static void getParameters(String processid, String executeid, Map params, Map flows, Map inputs, Map outputs, boolean addInputsToParams, boolean isStartTask) throws Exception
    {
        ExecutionTimeUtil.log((String) params.get("RESOLVE.JOB_ID"), null, null, ExecutionStep.METHOD_EXECUTION, ExecutionStepPhase.START);

        Collection<ExecuteParameter> executeParams = executeParameterStateCache.getParameters(processid, executeid);
        if (executeParams != null)
        {
            // RBA-14560 : Allow use of input param in another input param in Action Task and Runbook
            Map<String, String> allInputs = new HashMap<String, String>();
            for (ExecuteParameter exp : executeParams)
            {
                if (exp.getType().equalsIgnoreCase("INPUT"))
                {
                    allInputs.put(exp.getName(), exp.getValue());
                }
            }
            
            for (ExecuteParameter exp : executeParams)
            {
                boolean isParamSet = false;

                String type = exp.getType();
                if (type.equalsIgnoreCase("INPUT"))
                {
                    // param value
                    String name = exp.getName();
                    String value = exp.getValue();
                    boolean encrypt = exp.isEncrypt();
                    String prefix = exp.getPrefix();
                    String prefix_separator = exp.getPrefixSeparator();
                    EncryptionTransportObject encryptedObject = new EncryptionTransportObject();

                    // update value from params if not runbook
                    if (StringUtils.isBlank(processid))
                    {
                        // check if value is defined
                        if (params.containsKey(name))
                        {
                            value = (String)params.get(name);
                        }
                    }

                    // decrypt if encrypted
                    if (encrypt)
                    {
                        value = CryptUtils.decrypt(value);
                    }

                    // check if value is null
                    if (value == null)
                    {
                        value = "";
                    }

                    // replace redirection values
                    Map resultAttr = new HashMap();
                    // RBA-14560 : Allow use of input param in another input param in Action Task and Runbook
                    Object valueOfObject;
                    try
                    {
	                    valueOfObject = VariableUtil.replaceVarsIncludingNestedInputs(name, value, new HashMap(), outputs, params, flows, resultAttr, allInputs);
	                    if (valueOfObject != null)
	                    {   
	                        allInputs.put(name, valueOfObject.toString());
	                    }
                    }
                    catch (ActionTaskAbortException atae)
                    {
                    	throw atae;
                    }

                    // update encrypt if properties were encrypted
                    if (resultAttr.containsKey(Constants.RESULTATTR_ENCRYPTED))
                    {
                        if (((Boolean)resultAttr.get(Constants.RESULTATTR_ENCRYPTED)) == true)
                        {
                            encrypt = true;
                        }
                    }

                    // reencrypt if encrypted
                    if (encrypt)
                    {
                        if (valueOfObject != null && (valueOfObject instanceof String))
                        {
                            String valueLocal = String.valueOf(valueOfObject);
                            if (valueLocal.equalsIgnoreCase("[Please Enter Value]"))
                            {
                                valueLocal = "";
                            }
                            valueLocal = CryptUtils.encrypt(valueLocal);
                            encryptedObject.setEncryptedText(valueLocal);
                            valueOfObject = null;
                        }
                        else
                        {
                            value = CryptUtils.encrypt(value);
                            encryptedObject.setEncryptedText(value);
                        }
                        value = "";
                    }
                    
                    // RBA-10278 - stop auto adding INPUTS to PARAMS based on the system property. default DO NOT add
                    if (valueOfObject != null && (valueOfObject instanceof String))
                    {
                        String valueLocal = String.valueOf(valueOfObject);

                        // init valueLocal if set to [Please Enter Value]
                        if (valueLocal.equalsIgnoreCase("[Please Enter Value]"))
                        {
                            valueLocal = "";
                        }

                        // RBA-10278 - stop auto adding INPUTS to PARAMS
                        // set PARAMS
                        if(STANDARD_INPUTS.contains(name) || addInputsToParams)
                        {
                            params.put(name, initParamsValue(name, valueLocal, params, isStartTask));
                        }

                        // set INPUTS
                        inputs.put(name, valueLocal);

                    }
                    else if (valueOfObject != null)
                    {
                        // RBA-10278 - stop auto adding INPUTS to PARAMS
                        // set PARAMS
                        if(STANDARD_INPUTS.contains(name) || addInputsToParams)
                        {
                            params.put(name, initParamsValue(name, valueOfObject, params, isStartTask));
                        }

                        // set INPUTS
                        inputs.put(name, valueOfObject);
                    }
                    else if (encrypt) // value object is null because we emptied the encrypted value string
                    {
                        // RBA-10278 - stop auto adding INPUTS to PARAMS
                        // set PARAMS
                        if(STANDARD_INPUTS.contains(name)|| addInputsToParams)
                        {
                            params.put(name, encryptedObject);
                        }

                        // set INPUTS
                        inputs.put(name, encryptedObject);
                    }
                    
                    // RBA-14583 : Create a black list of commands
                    // It is at the end of the getParameters() method in order to capture the substituted/evaluated commands
    				if (SecurityUtil.isCommandBlacklistEnabled()) 
    				{
    					StringTokenizer tokenizer = new StringTokenizer(Constants.SECURITY_COMMANDS_AS_PARAMETERS_TO_CHECK, ",");
    					while (tokenizer.hasMoreTokens())
    					{
    						String parameterName = tokenizer.nextToken();
    						if (exp.getName().equalsIgnoreCase((parameterName)))
    						{
    							// Object inputObject = exp.getValue();
    							Object inputObject = valueOfObject;
    							if (inputObject instanceof String)
    							{
    								String commandToCheck = (String) inputObject;
    								if (SecurityUtil.isCommandBlacklisted(commandToCheck))
    								{
    									throw new ActionTaskAbortException(Constants.EXCEPTION_PREFIX_BLACKLISTED_COMMANDS + " '" + exp.getName() + "' : " + commandToCheck);
    								}
    							}
    						}
    					}
    				}
    				
                }				
            }
        }

        ExecutionTimeUtil.log((String) params.get("RESOLVE.JOB_ID"), null, null, ExecutionStep.METHOD_EXECUTION, ExecutionStepPhase.END);

    } // getParameters

    static Object initParamsValue(String name, Object value, Map params, boolean isStartTask) throws Exception
    {
        Object result = value;

        boolean isMapValue = false;

        // context value stored in map will take precedence. Overwrite default
        // value if it exists in map. Unless it is a start task
        Object mapValue = isStartTask ? null : params.get(name);

        // get PARAMS map value - don't replace "END_QUIT" because it comes from subprocess.
        // NOTE: values will still get overwritten by empty values
        if (mapValue != null && !"[Please Enter Value]".equalsIgnoreCase(String.valueOf(mapValue)) && !isInternalParameter(name))
        {
            value = mapValue;
            isMapValue = true;
        }
        
        return result;
    } // initParamsValue

    static boolean isInternalParameter(String name)
    {
        boolean result = false;

        if (name.equals(Constants.EXECUTE_WIKI_PROCESS) || name.equals(Constants.EXECUTE_WIKI_PARENT) || name.equals(Constants.EXECUTE_WIKI_WIKIID) || name.equals(Constants.EXECUTE_WIKI_PARENTID) || name.equals(Constants.MODEL_TYPE) || name.equals(Constants.MODEL_MODE) || name.equals(Constants.EXECUTE_END_WAIT) || name.equals(Constants.EXECUTE_END_QUIT) || name.equals(Constants.EXECUTE_END_SEVERITY)|| name.equals(Constants.EXECUTE_END_TYPE)|| name.equals(Constants.EXECUTE_END_CONDITION))
        {
            result = true;
        }

        return result;
    } // isInternalParameter

    public static String actionRequest(String executeid, String processid, String wiki, final String taskid, String delay, 
    								   String problemid, String reference, String userid, String input, Targets targets, 
    								   Map outputs, Map params, Map flows, ScriptDebug debug, String mock, 
    								   long processTimeout, ActionTaskProperties properties, String token, 
    								   String rsviewIdGuid) throws Exception
    {
        String result = null;
        ActionProcessTaskRequest task = null;

		if (StringUtils.isEmpty(processid)) {
			result = actionRequestInternal(executeid, processid, wiki, taskid, delay, problemid, reference, userid,
					input, targets, outputs, params, flows, debug, mock, processTimeout, properties, token,
					rsviewIdGuid);

		} else {
			Map<String, Object> modelStates = ProcessRequest.getModelStates(processid);
			if (modelStates == null) {
				throw new ProcessCompletedConcurrencyException(
						"Process may have completed. No modelStates for processid: " + processid);
			}

			task = (ActionProcessTaskRequest) ((Map) modelStates.get(ProcessRequest.EXECID_TASKREQ)).get(executeid);
			if (task == null) {
				Log.log.warn("No taskdef defined for executeid: " + executeid);
				debug.println("No taskdef defined for executeid: " + executeid);
				throw new RuntimeException("No taskdef defined for executeid: " + executeid);
			}

			result = actionRequestInternal(executeid, processid, wiki, taskid, delay, problemid, reference, userid,
						input, targets, outputs, params, flows, debug, mock, processTimeout, properties, token,
						rsviewIdGuid);				
		}
        return result;
    } // actionRequest

    
    @SuppressWarnings("unchecked")
    public static String actionRequestInternal(String executeid, String processid, String wiki, final String taskid, 
    										   String delay, String problemid, String reference, String userid, String input, 
    										   Targets targets, Map outputs, Map params, Map flows, ScriptDebug debug, 
    										   String mock, long processTimeout, ActionTaskProperties properties, 
    										   String token, String rsviewIdGuid) throws Exception
    {
    	ExecutionTimeUtil.log((String) params.get("RESOLVE.JOB_ID"), null, null, ExecutionStep.METHOD_EXECUTION, ExecutionStepPhase.START);

    	if (StringUtils.isNotBlank(processid) && !StringUtils.isEmpty(getProcessState(processid).mockValue))
        {
            mock = getProcessState(processid).mockValue;
        }
    	
        String result = null;

        Map inputs = new HashMap();

        try
        {
            // find actiontask
        	
        	long startTimeTmp = System.currentTimeMillis();
        	ResolveActionTaskVO actiontask = null;
            actiontask = actionTaskCache.get(taskid);
            if (actiontask == null) {
                // If not available in cache read from DB
                actiontask = ServiceHibernate.getActionTaskWithReferencesExceptGraph(taskid, null, userid);
                
                if (actiontask != null) {
                    actionTaskCache.putIfAbsent(taskid, actiontask);
                } 
            }
            
            if (actiontask == null)
            {
                throw new Exception(String.format("FAIL: unable to find ActionTask: %s", taskid));
            }

            // check user permission for actiontask
            if (!UserUtils.checkTaskPermission(actiontask, userid))
            {
                throw new Exception(String.format("FAIL: userid %s does not have permission to execute task %s", 
                								  userid, actiontask.getUFullName()));
            }

            try
            {
            	String taskName = actiontask.getUFullName();
                String incidentId = sendIncidentResolutionEvent(params, taskName);
                
            	if (!taskName.equalsIgnoreCase("start#resolve") && !taskName.equalsIgnoreCase("end#resolve")) 
            	{
            		String wikiName = StringUtils.isBlank(wiki) ? "Standalone Task" : wiki;
            		Log.syslog.info(String.format("[RUNBOOK] %s %s %s %s %s", MainBase.main.configId.getLocation(), userid, "\"" + wikiName + "\"", "\"" + actiontask.getUFullName() + "\"", "STARTED"));
            	}
            	
                // init task
                ActionProcessTaskRequest task = null;

                // init invocation
                final ResolveActionInvocVO invocation = actiontask.getResolveActionInvoc();
                
                if (invocation != null)
                {
                    if (actiontask.getUActive() == false)
                    {
                        throw new Exception("FAIL: ActionTask is not active: " + actiontask.getUName());
                    }
                    
                    if (params.containsKey(Constants.EXECUTE_INCIDENT_ID)) {
                        String rootResolutionId = (String) params.get(Constants.EXECUTE_ROOT_RUNBOOK_ID);
                        String taskId = (String) params.get(Constants.EXECUTE_ACTION_TASK_ID);
                        EventMessage em = EventMessageFactory.getInstance().getIncidentResolutionTaskStartedMessage(incidentId, taskId, rootResolutionId);
                        Main.esb.sendInternalMessage(Constants.ESB_NAME_EVENT_SOURCING_DCS, "", em.toJSON());
                    }
                    
                    if (!StringUtils.isEmpty(processid))
                    {
                        Map<String, Object> modelStates = ProcessRequest.getModelStates(processid);
                        if (modelStates == null)
                        {
                            throw new ProcessCompletedConcurrencyException(String.format("Process may have completed. " +
                            															 "No modelStates for processid: %s",
                            															 processid));
                        }

                        task = (ActionProcessTaskRequest) ((Map) modelStates.get(ProcessRequest.EXECID_TASKREQ)).get(executeid);
                        if (task == null)
                        {
                        	String errMsg = String.format("No taskdef defined for executeid: %s", executeid);
                        	
                            Log.log.warn(errMsg);
                            debug.println(errMsg);
                            throw new RuntimeException(errMsg);
                        }
                    }
                    
                    // RBA-14583 : Create a black list of commands
                    if (SecurityUtil.isCommandBlacklisted(invocation.getUCommand()))
                    {
                    	String blockingMessage = Constants.EXCEPTION_PREFIX_BLACKLISTED_COMMANDS + " '" + invocation.getUType() + "' : " + invocation.getUCommand();
                    	sendResultErrorOnAbort(executeid, processid, wiki, problemid, reference, userid, actiontask, inputs, params, flows, debug, mock, blockingMessage, processTimeout);
                    	abortProcessCAS(params);
                    	throw new ActionTaskAbortException(blockingMessage);
                    }
                     
                    // automatically add INPUTS into PARAMS
                    boolean addInputsToParams = StringUtils.isTrue(PropertiesUtil.getPropertyString("automation.add.inputs.to.params"));
                    boolean isStartTask = false;
                    
                    // add start#resolve task inputs to params
                    if ("start#resolve".equals(actiontask.getUFullName()))
                    {
                        addInputsToParams = true;
                        isStartTask = true;
                    }
                                    
                    // RBA-14583 : Create a black list of commands
                    // RBA-14560 : Allow use of input param in another input param in Action Task and Runbook
                    try
                    {
                    	getParameters(processid, executeid, params, flows, inputs, outputs, addInputsToParams, isStartTask);
                    }
                    catch (ActionTaskAbortException atae)
                    {
                    	sendResultErrorOnAbort(executeid, processid, wiki, problemid, reference, userid, actiontask, inputs, params, flows, debug, mock, atae.getLocalizedMessage(), processTimeout);
                    	abortProcessCAS(params);
                        throw atae;
                    }

                    // initialize params, flows, inputs from mock data
                    ResolveActionTaskMockDataVO mockData = null;
                    
                    if (StringUtils.isNotEmpty(mock))
                    {
                        mockData = prepareMockParameters(params, flows, mock, inputs, actiontask);
                    }

                    // NOTE: now using the ActionInvocation preprocessor ONLY
                    ResolvePreprocessVO preprocess = invocation.getPreprocess();

                    // execute preprocessor
                    ExecuteResult preprocessResult = null;
                    
                    // Create User Credentials Object for passing to groovy scripts
                    RRATContextHelper rrATCtxHlpr = null;
                    
                    if (StringUtils.isNotBlank(token) && StringUtils.isNotBlank(rsviewIdGuid))
                    {
                        rrATCtxHlpr = new RRATContextHelper(CryptUtils.encrypt(token), CryptUtils.encrypt(rsviewIdGuid));
                        params.put(Constants.GROOVY_BINDING_RRATCONTEXTHELPER, rrATCtxHlpr);
                    }

                    if (preprocess != null)
                    {
                        // actiontask preprocess used to define Targets (if applicable)
                        preprocessResult = preprocessActionTask(preprocess, processid, wiki, actiontask.getUName(), problemid, 
                        									    userid, targets, inputs, outputs, new RATParams(params), flows, 
                        									    debug, invocation.getUTimeout(), properties);
                    }
                    
                    TargetInstance targetInstance = null;
                    for (String key : targets.keySet())
                    {
                        TargetInstance t = targets.get(key);
                        if (t!=null)
                        {
                            targetInstance = t;
                            break;
                        }
                    }

                    if (preprocessResult != null && preprocessResult.isCompletion() == false)
                    {
                        if (targetInstance==null)
                        {
                            targetInstance = new TargetInstance("", Constants.ESB_NAME_RSCONTROL);
                        }
                        sendResultAssess(executeid, processid, wiki, problemid, reference, userid, actiontask, invocation, 
                        				 targetInstance, preprocessResult.getResultString(), inputs, new RATParams(params), 
                        				 flows, debug, mock, processTimeout);

                        // set requested for target address of task
                        if (!StringUtils.isEmpty(processid) && task != null)
                        {
                            task.setRequested(targetInstance.getAddress());
                        }
                    }
                    
                    // process actiontask for invocation type PROCESS
                    else if (invocation.getUType().equalsIgnoreCase(Constants.ACTION_INVOCATION_TYPE_PROCESS))
                    {
                        params.put(Constants.EXECUTE_ACTIONRESULT_GUID, MainBase.main.configId.getGuid());
                        sendResultProcess(executeid, processid, wiki, problemid, reference, userid, actiontask, invocation, 
                        				  targetInstance, inputs, outputs, new RATParams(params), flows, debug, mock, 
                        				  processTimeout);

                        // set requested for target address of task
                        if (!StringUtils.isEmpty(processid) && task != null)
                        {
                            task.setRequested(targetInstance.getAddress());
                        }
                    }
                    
                    // process actiontask for each target
                    else
                    {
                        /*
                         * Use AFFINITY if content has SESSIONS Use TARGET Use
                         * QUEUENAME if TARGET is not defined
                         */
                        boolean hasSessionAffinity = false;
                        boolean hasQueueName = false;

                        String content = getActionInvocOption(actiontask, invocation, 
                        									  Constants.ACTION_INVOCATION_OPTIONS_INPUTFILE, params);
                        
                        // RBA-14583 : Create a black list of commands
                        // Here, just for SCRIPT types, like POWERSHELL and CSCRIPT
                        if ((invocation.getUType().equalsIgnoreCase("powershell") || 
                        	 invocation.getUType().equalsIgnoreCase("cscript")) && SecurityUtil.isCommandBlacklistEnabled()) 
        				{
    						if (SecurityUtil.isCommandBlacklisted(content))
    						{
    							String blockingMessage = String.format("%s '%s' : %s",
    																   Constants.EXCEPTION_PREFIX_BLACKLISTED_COMMANDS, 
    																   invocation.getUType(), content);
    	                    	sendResultErrorOnAbort(executeid, processid, wiki, problemid, reference, userid, actiontask, inputs, params, 
    	                    						   flows, debug, mock, blockingMessage, processTimeout);
    	                    	abortProcessCAS(params);
    	                    	throw new ActionTaskAbortException(blockingMessage);
    						}
    						
        				}

                        // check for SESSIONS in content and set affinity
                        if (StringUtils.isNotEmpty(content) && content.contains(Constants.GROOVY_BINDING_SESSIONS))
                        {
                            // set targetInst guid from SESSION_AFFINITY
                            String affinityGuid = (String) flows.get(Constants.SESSION_AFFINITY);
                            if (affinityGuid != null)
                            {
                                for (TargetInstance targetInst : targets.values())
                                {
                                	String saMsg = String.format("Overriding using session affinity guid: %s", affinityGuid);
                                    Log.log.debug(saMsg);
                                    debug.println(saMsg);

                                    targetInst.setAffinity(affinityGuid);
                                    hasSessionAffinity = true;
                                }
                            }
                        }

                        if (!hasSessionAffinity)
                        {
                            String queueName = getActionInvocOption(actiontask, invocation, Constants.ACTION_INVOCATION_OPTIONS_QUEUE_NAME, params);

                            // use queueName if defined and target is not defined (i.e. RSREMOTE)
                            if (!StringUtils.isEmpty(queueName) && (targets.size() == 1))
                            {
                                TargetInstance targetInst = (TargetInstance) targets.values().iterator().next();
                                if (StringUtils.startsWith(targetInst.getGuid(), Constants.ESB_NAME_RSREMOTE))
                                {
                                    queueName = VariableUtil.replaceVarsString(queueName, inputs, outputs, params, flows);

                                    if (StringUtils.isNotEmpty(queueName))
                                    {
                                    	String qMsg = String.format("Overriding using QUEUE_NAME: %s", queueName);
                                        Log.log.debug(qMsg);
                                        debug.println(qMsg);

                                        targetInst.setAffinity(queueName);
                                        hasQueueName = true;
                                    }
                                }
                            }

                            // using targets
                        }

                        Log.log.debug("Targets: " + targets);
                        debug.println("Targets: " + targets);
                        WorksheetUtils.writeDebugCAS(debug);

                        // update dependency task def with new targets (process
                        // only)

                        boolean targetsUpdatedFromQueueName = false;
                        TargetInstance targetsUpdatedInst = null;

                        // set latch in FLOW[MERGE_TARGETS] if more than one
                        // target
                        if (targets.size() > 1)
                        {
                            setMergeTargets(flows, processid, executeid, targets.size());
                        }

                        for (Iterator i = targets.values().iterator(); i.hasNext();)
                        {
                            TargetInstance targetInst = (TargetInstance) i.next();
                            if (Log.log.isDebugEnabled()) {
                                Log.log.debug("targetInst: " + targetInst);
                            }
                            debug.println("targetInst: " + targetInst);

                            if (invocation.getUType().equalsIgnoreCase(Constants.ACTION_INVOCATION_TYPE_ASSESS))
                            {
                                params.put(Constants.EXECUTE_ACTIONRESULT_GUID, MainBase.main.configId.getGuid());
                                sendResultAssess(executeid, processid, wiki, problemid, reference, userid, actiontask, invocation, targetInst, null, inputs, new RATParams(params), flows, debug, mock, processTimeout);
                            }
                            else if (invocation.getUType().equalsIgnoreCase(Constants.ACTION_INVOCATION_TYPE_PROCESS))
                            {
                                sendResultProcess(executeid, processid, wiki, problemid, reference, userid, actiontask, invocation, targetInst, inputs, outputs, new RATParams(params), flows, debug, mock, processTimeout);
                            }
                            else
                            {
                                // send mock result for assessment
                                // RBA-14570 : AT, when Mock doesn't have any data in RAW section, it still executes script in Content section
                                // if (mockData != null && mockData.getUData() != null)
                                if (mockData != null)
                                {
                                    sendResultAssess(executeid, processid, wiki, problemid, reference, userid, actiontask, invocation, targetInst, mockData.getUData(), inputs, new RATParams(params), flows, debug, mock, processTimeout);
                                }

                                // send execute request
                                else
                                {
                                    sendRequest(executeid, processid, wiki, delay, problemid, reference, userid, input, actiontask, invocation, targetInst, inputs, outputs, new RATParams(params), flows, debug, mock, processTimeout);
                                }
                            }
                            
                            // set requested for target address of task
                            if (!StringUtils.isEmpty(processid) && task != null)
                            {
                                task.setRequested(targetInst.getAddress());
                            }
                        }
                    }
                }
                else
                {
                    Log.log.warn("FAIL: Missing ActionTask invocation or no matching invocation found");
                }

                result = "SUCCESS sent ActionTask request";
            }
            catch (ReturnUserErrorException e)
            {
                debug.println(e.getMessage(), e);

                sendResultError(executeid, processid, wiki, problemid, reference, userid, actiontask, null, params, flows, debug, mock, e.getMessage(), processTimeout);
            }
            finally
            {
                WorksheetUtils.writeDebugCAS(debug);
            }
        }
        catch (ProcessCompletedConcurrencyException e)
        {
            Log.log.warn(e.getMessage());
        }
        // RBA-14583 : Create a black list of commands 
        // handling of ActionTaskAbortException added in order to not see that exception in the logs
        catch (Exception e)
        {
			if (e instanceof ActionTaskAbortException && e.getLocalizedMessage().startsWith(Constants.EXCEPTION_PREFIX_BLACKLISTED_COMMANDS)) {
				Log.log.error(e.getMessage());
			} else {
				debug.println(e.getMessage(), e);
				Log.log.error(e.getMessage(), e);
				throw new RuntimeException(e);
			}
        }
        finally
        {
            WorksheetUtils.writeDebugCAS(debug);
        }

        ExecutionTimeUtil.log((String) params.get("RESOLVE.JOB_ID"), null, null, ExecutionStep.METHOD_EXECUTION, ExecutionStepPhase.END);

        return result;
    } // actionRequestInternal

	private static ResolveActionTaskMockDataVO prepareMockParameters(Map params, Map flows, String mock, Map inputs,
			ResolveActionTaskVO actiontask) {
		ResolveActionTaskMockDataVO mockData;
		mockData = ServiceHibernate.getActionTaskMockDataVO(actiontask.getSys_id(), mock);
		if (mockData != null)
		{
		    Map mockParams = mockData.convertParamsToMap();
		    if (mockParams != null && !mockParams.isEmpty())
		    {
		        params.putAll(mockParams);
		    }

		    Map mockInputs = mockData.convertInputsToMap();
		    if (mockInputs != null && !mockInputs.isEmpty())
		    {
		        inputs.putAll(mockInputs);
		    }

		    Map mockFlows = mockData.convertFlowsToMap();
		    if (mockFlows != null && !mockFlows.isEmpty())
		    {
		        flows.putAll(mockFlows);
		    }
		}
		return mockData;
	}

	private static String sendIncidentResolutionEvent(Map params, String taskName) {
		String incidentId = (String)params.get(Constants.EXECUTE_INCIDENT_ID);
		if(incidentId != null && !incidentId.isEmpty()) {
		    String rootResolutionId = (String) params.get(Constants.EXECUTE_ROOT_RUNBOOK_ID);
		    String rootResolution = (String) params.get(Constants.EXECUTE_WIKI);
		    String parentResolution = (String) params.get(Constants.EXECUTE_WIKI_PROCESS);
		    String type = IncidentResolutionTaskType.ACTION_TASK.toString();
		    String content = taskName;

		    String parentTaskId = (String) params.getOrDefault(Constants.EXECUTE_ACTION_TASK_ID, "");
		    String taskId = UUID.randomUUID().toString();
		     params.put(Constants.EXECUTE_ACTION_TASK_ID, taskId);
		     
		     EventMessage em = EventMessageFactory.getInstance()
		                     .getIncidentResolutionTaskCreatedMessage(incidentId, taskId, parentTaskId, rootResolutionId, type, content, rootResolution, parentResolution);
		     Main.esb.sendInternalMessage(Constants.ESB_NAME_EVENT_SOURCING_DCS, "", em.toJSON());
		}
		return incidentId;
	}


    static void updateTargets(ActionProcessTaskRequest task, String processid, String executeid, Targets targets) throws Exception
    {
        if (!StringUtils.isEmpty(processid) && task != null)
        {
            task.setTargets(targets);
        }
    } // updateTargets

    static void sendRequest(String executeid, String processid, String wiki, String delay, String problemid, String reference, String userid, String input, ResolveActionTaskVO actiontask, ResolveActionInvocVO invocation, TargetInstance targetInst, Map inputs, Map outputs, Map params, Map flows, ScriptDebug debug, String mock, long processTimeout) throws Exception
    {
        String command = invocation.getUCommand();
        String args = invocation.getUArgs();
        String type = invocation.getUType();
        String parserid = invocation.getParser() != null ? invocation.getParser().getSys_id() : null;
        String assessid = invocation.getAssess() != null ? invocation.getAssess().getSys_id() : null;
        int taskTimeout = invocation.getUTimeout();
        String timeoutStr = "" + taskTimeout;
        String processTimeoutStr = "" + processTimeout;

        // update delay
        if (StringUtils.isEmpty(delay))
        {
            delay = "NOW";
        }

        // update input
        if (StringUtils.isEmpty(input))
        {
            input = "";
        }

        /*
         * The following replaceVars comes after the content above to allow the
         * preprocessor to update the command, args and properties
         */
        // combine params
        params.putAll(targetInst.getParams());

        // replace $PARAM, $PROPERTY in command
        command = VariableUtil.replaceVarsString(command, inputs, outputs, params, flows);

        // replace $PARAM, $PROPERTY in arguments
        args = VariableUtil.replaceVarsString(args, inputs, outputs, params, flows);

        // init options
        MMsgOptions options = new MMsgOptions();
        options.setResultAddr(MainBase.main.configId.getGuid());
        options.setAddress(targetInst.getAddress());
        options.setTarget(targetInst.getGuid());
        options.setAffinity(targetInst.getAffinity());
        options.setActionName(actiontask.getUName());
        options.setActionNamespace(actiontask.getUNamespace());
        options.setActionSummary(actiontask.getUSummary());
        options.setActionHidden((actiontask.getUIsHidden() == null ? "FALSE" : "" + actiontask.getUIsHidden()));
        options.setActionTags(actiontask.getTagsAsCSV());
        options.setActionID(actiontask.getSys_id());
        options.setProcessID(processid);
        options.setWiki(wiki);
        options.setReference(reference);
        options.setExecuteID(executeid);
        options.setParserID(parserid);
        options.setAssessID(assessid);
        options.setLogResult("" + actiontask.getULogresult());
        options.setUserID(userid);
        options.setProblemID(problemid);
        options.setTaskname(executeid + "/" + actiontask.getUName());
        options.setType(type);
        options.setCommand(command);
        options.setArgs(args);
        options.setInput(input);
        options.setDelay(delay);
        options.setTimeout(timeoutStr);
        options.setProcessTimeout("" + processTimeoutStr);
        options.setMetricId("");
        options.setMock(mock);

        Map properties = new HashMap();
        List<ResolveActionInvocOptionsVO> invocOptions = optionCache.get(actiontask.getSys_id());
        if (invocOptions == null)
        {
            invocOptions = (List<ResolveActionInvocOptionsVO>) invocation.getResolveActionInvocOptions();
            if (invocOptions != null)
            {
                for (ResolveActionInvocOptionsVO o : invocOptions)
                {
                    break;
                }
            }

            optionCache.put(actiontask.getSys_id(), invocOptions);
        }

        String metricOptionGroup = null;
        String metricOptionId = null;
        String metricOptionUnit = null;
        for (ResolveActionInvocOptionsVO option : invocOptions)
        {
            String optionValue = VariableUtil.replaceVarsString(option.getUValue(), inputs, outputs, params, flows);
            if (optionValue != null)
            {
                properties.put(option.getUName(), optionValue);

                // values for metricId
                if (option.getUName().equals(Constants.ACTION_INVOCATION_OPTIONS_METRIC_GROUP))
                {
                    metricOptionGroup = optionValue;
                }
                else if (option.getUName().equals(Constants.ACTION_INVOCATION_OPTIONS_METRIC_ID))
                {
                    metricOptionId = optionValue;
                }
                else if (option.getUName().equals(Constants.ACTION_INVOCATION_OPTIONS_METRIC_UNIT))
                {
                    metricOptionUnit = optionValue;
                }

                // add cacheable flag for INPUTFILE
                else if (option.getUName().equals(Constants.ACTION_INVOCATION_OPTIONS_INPUTFILE))
                {
                    if (optionValue.equals(option.getUValue()))
                    {
                        properties.put(Constants.EXECUTE_CACHEABLE, "true");
                    }
                    else
                    {
                        properties.put(Constants.EXECUTE_CACHEABLE, "false");
                    }
                }
            }
        }

        // set metricid
        if (metricOptionGroup != null && metricOptionId != null && metricOptionUnit != null)
        {
            options.setMetricId(metricOptionGroup + "/" + metricOptionId + "/" + metricOptionUnit);
        }

        // msg content
        MMsgContent content = new MMsgContent();
        content.setProperties(StringUtils.objToString(properties));
        content.setParams(StringUtils.objToString(params));
        content.setFlows(StringUtils.objToString(flows));
        content.setInputs(StringUtils.objToString(inputs));

        // set debug
        if (debug.isDebug())
        {
            // content.setDebug(debug.toString());
            content.setDebug("true");
        }
        else
        {
            content.setDebug("");
        }

        // init reference
        if (reference == null)
        {
            reference = "";
        }

        try
        {
            Log.log.info("Sending ActionTask Request name: " + actiontask.getUName() + " processid: " + processid + " executeid: " + executeid);
            debug.println("Sending ActionTask Request name: " + actiontask.getUName() + " processid: " + processid + " executeid: " + executeid);
            WorksheetUtils.writeDebugCAS(debug);

            if(Log.log.isTraceEnabled()) {
                Log.log.trace("inputs: " + StringUtils.mapToLogString(inputs));
                Log.log.trace("params: " + StringUtils.mapToLogString(params));
                Log.log.trace("flows: " + StringUtils.mapToLogString(flows));
            }
            if(MRegister.isValidQueue(targetInst.getEsbAddr())) {
                Main.esb.sendMessage(targetInst.getEsbAddr(), "MAction.executeRequest", options, content);
            } else {
                if (Log.log.isDebugEnabled()) {
                    Log.log.debug("can't execute action task: " +  actiontask.getUName() + " processid: " 
                                    + processid + " executeid: " + executeid + ". " + targetInst.getEsbAddr() 
                                    + " is not a valid queue name or there is no RSREMOTE with this queue defined as active.");
                    Log.log.debug("valid queues: " + MRegister.getValidQueues());
                }
                throw new Exception("can't execute action task: " +  actiontask.getUName() + " processid: "
                                    + processid + " executeid: " + executeid + ". " + targetInst.getEsbAddr() 
                                    + " is not a valid queue name or there is no RSREMOTE with this queue defined as active.");
            }
        }
        catch (Throwable e)
        {
            sendResultError(executeid, processid, wiki, problemid, reference, userid, actiontask, inputs, params, flows, debug, mock, e.getMessage(), processTimeout);
        }

    } // sendRequest

    static void sendResultAssess(String executeid, String processid, String wiki, String problemid, String reference, String userid, ResolveActionTaskVO actiontask, ResolveActionInvocVO invocation, TargetInstance targetInst, String raw, Map inputs, Map params, Map flows, ScriptDebug debug, String mock, long processTimeout) throws Exception
    {
        ExecutionTimeUtil.log((String) params.get("RESOLVE.JOB_ID"), null, null, ExecutionStep.METHOD_EXECUTION, ExecutionStepPhase.START);
    	
        // init reference
        if (reference == null)
        {
            reference = "";
        }

        if (raw == null)
        {
            raw = "";
        }

        String parserid = invocation.getParser() != null ? invocation.getParser().getSys_id() : null;
        String assessid = invocation.getAssess() != null ? invocation.getAssess().getSys_id() : null;

        // combine params
        params.putAll(targetInst.getParams());

        // init options
        MMsgOptions options = new MMsgOptions();
        options.setResultAddr(MainBase.main.configId.getGuid());
        options.setAddress(targetInst.getAddress());
        options.setTarget(targetInst.getGuid());
        options.setActionName(actiontask.getUName());
        options.setActionNamespace(actiontask.getUNamespace());
        options.setActionSummary(actiontask.getUSummary());
        options.setActionHidden((actiontask.getUIsHidden() == null ? "FALSE" : "" + actiontask.getUIsHidden()));
        options.setActionTags(actiontask.getTagsAsCSV());
        options.setActionID(actiontask.getSys_id());
        options.setProcessID(processid);
        options.setWiki(wiki);
        options.setReference(reference);
        options.setExecuteID(executeid);
        options.setParserID(parserid);
        options.setAssessID(assessid);
        options.setDuration("0");
        options.setLogResult("" + actiontask.getULogresult());
        options.setUserID(userid);
        options.setProblemID(problemid);
        options.setReturnCode("0");
        options.setCompletion("true");
        options.setTimeout("" + invocation.getUTimeout());
        options.setProcessTimeout("" + processTimeout);
        options.setMetricId("");
        options.setMock(mock);
        
        options.setAffinity("RSCONTROL");

        List<ResolveActionInvocOptionsVO> invocOptions = optionCache.get(actiontask.getSys_id());
        if (invocOptions == null)
        {
            invocOptions = (List<ResolveActionInvocOptionsVO>) invocation.getResolveActionInvocOptions();
            if (invocOptions != null)
            {
                optionCache.put(actiontask.getSys_id(), invocOptions);
            }
        }

        String metricOptionGroup = null;
        String metricOptionId = null;
        String metricOptionUnit = null;
        
        if (invocOptions != null)
        {
            for (ResolveActionInvocOptionsVO option : invocOptions)
            {
                String optionValue = VariableUtil.replaceVarsString(option.getUValue(), inputs, new HashMap(), params, flows);
                if (optionValue != null)
                {
                    // values for metricId
                    if (option.getUName().equals(Constants.ACTION_INVOCATION_OPTIONS_METRIC_GROUP))
                    {
                        metricOptionGroup = optionValue;
                    }
                    else if (option.getUName().equals(Constants.ACTION_INVOCATION_OPTIONS_METRIC_ID))
                    {
                        metricOptionId = optionValue;
                    }
                    else if (option.getUName().equals(Constants.ACTION_INVOCATION_OPTIONS_METRIC_UNIT))
                    {
                        metricOptionUnit = optionValue;
                    }
                    else if (option.getUName().equals(Constants.ACTION_INVOCATION_OPTIONS_BLANKVALUES))
                    {
                    	if (StringUtils.isNotBlank(optionValue))
                    	{
	                    	options.setBlankValues(optionValue);
                    	}
                    }
                }
            }
        }

        // set metricid
        if (metricOptionGroup != null && metricOptionId != null && metricOptionUnit != null)
        {
            options.setMetricId(metricOptionGroup + "/" + metricOptionId + "/" + metricOptionUnit);
        }

        params.put(Constants.EXECUTE_AFFINITY, "RSCONTROL");
        
        // create content
        MMsgContent content = new MMsgContent();
        content.setParams(StringUtils.objToString(params));
        content.setFlows(StringUtils.objToString(flows));
        content.setInputs(StringUtils.objToString(inputs));
        content.setRaw("");
        content.setRaw(raw);

        // set debug
        if (debug.isDebug())
        {
            // content.setDebug(debug.toString());
            content.setDebug("true");
        }
        else
        {
            content.setDebug("");
        }

        Log.log.info("Sending ActionTask Assess Result name: " + actiontask.getUName() + " processid: " + processid + " executeid: " + executeid);
        debug.println("Sending ActionTask Assess Result name: " + actiontask.getUName() + " processid: " + processid + " executeid: " + executeid);
        WorksheetUtils.writeDebugCAS(debug);

        MResult.actionResult(options, content, true);

        ExecutionTimeUtil.log((String) params.get("RESOLVE.JOB_ID"), null, null, ExecutionStep.METHOD_EXECUTION, ExecutionStepPhase.END);
    } // sendResultAssess

    static void sendResultProcess(String executeid, String processid, String wiki, String problemid, String reference, String userid, ResolveActionTaskVO actiontask, ResolveActionInvocVO invocation, TargetInstance targetInst, Map inputs, Map outputs, Map params, Map flows, ScriptDebug debug, String mock, long processTimeout) throws Exception
    {
        String command = "";
        String args = "";

        // add params from targetInst
        params.putAll(targetInst.getParams());

        // replace $PARAM, $PROPERTY in command
        command = VariableUtil.replaceVarsString(invocation.getUCommand(), inputs, outputs, params, flows);

        // replace $PARAM, $PROPERTY in arguments
        args = VariableUtil.replaceVarsString(invocation.getUArgs(), inputs, outputs, params, flows);

        // init reference
        if (reference == null)
        {
            reference = "";
        }

        String parserid = invocation.getParser() != null ? invocation.getParser().getSys_id() : null;
        String assessid = invocation.getAssess() != null ? invocation.getAssess().getSys_id() : null;

        // combine params
        params.putAll(targetInst.getParams());

        // init options
        MMsgOptions options = new MMsgOptions();
        options.setResultAddr(MainBase.main.configId.getGuid());
        options.setAddress(targetInst.getAddress());
        options.setTarget(targetInst.getGuid());
        options.setActionName(actiontask.getUName());
        options.setActionNamespace(actiontask.getUNamespace());
        options.setActionSummary(actiontask.getUSummary());
        options.setActionHidden((actiontask.getUIsHidden() == null ? "FALSE" : "" + actiontask.getUIsHidden()));
        options.setActionTags(actiontask.getTagsAsCSV());
        options.setActionRoles(actiontask.getURoles());
        options.setActionID(actiontask.getSys_id());
        options.setProcessID(processid);
        options.setWiki(wiki);
        options.setReference(reference);
        options.setExecuteID(executeid);
        options.setParserID(parserid);
        options.setAssessID(assessid);
        options.setDuration("0");
        options.setLogResult("" + actiontask.getULogresult());
        options.setUserID(userid);
        options.setProblemID(problemid);
        options.setReturnCode("0");
        options.setCompletion("true");
        options.setTimeout("" + invocation.getUTimeout());
        options.setProcessTimeout("" + processTimeout);
        options.setMetricId("");
        options.setMock(mock);

        options.setAffinity("RSCONTROL");
        params.put(Constants.EXECUTE_AFFINITY, "RSCONTROL");

        // execute ProcessTask
        ProcessTask processTask = new ProcessTask(command, args, options, inputs, outputs, params, flows, debug, executeid, targetInst.getAddress(), processid, wiki, problemid, userid);
        TaskExecutor.execute(processTask, "execute");
    } // sendResultProcess

    static void sendResultError(String executeid, String processid, String wiki, String problemid, String reference, String userid, ResolveActionTaskVO actiontask, Map inputs, Map params, Map flows, ScriptDebug debug, String mock, String raw, long processTimeout) throws Exception
    {
        // init reference
        if (reference == null)
        {
            reference = "";
        }

        // init options
        MMsgOptions options = new MMsgOptions();
        options.setResultAddr(MainBase.main.configId.getGuid());
        options.setAddress("");
        options.setTarget(Constants.ESB_NAME_RSCONTROL);
        options.setActionName(actiontask.getUName());
        options.setActionNamespace(actiontask.getUNamespace());
        options.setActionSummary(actiontask.getUSummary());
        options.setActionHidden((actiontask.getUIsHidden() == null ? "FALSE" : "" + actiontask.getUIsHidden()));
        options.setActionTags(actiontask.getTagsAsCSV());
        options.setActionRoles(actiontask.getURoles());
        options.setActionID(actiontask.getSys_id());
        options.setProcessID(processid);
        options.setWiki(wiki);
        options.setReference(reference);
        options.setExecuteID(executeid);
        options.setParserID("");
        options.setAssessID("");
        options.setDuration("0");
        options.setLogResult("false");
        options.setUserID(userid);
        options.setProblemID(problemid);
        options.setReturnCode("-1");
        options.setCompletion("false");
        options.setProcessTimeout("" + processTimeout);
        options.setMetricId("");
        options.setMock(mock);

        // init content
        MMsgContent content = new MMsgContent();
        content.setParams(StringUtils.objToString(params));
        content.setFlows(StringUtils.objToString(flows));
        content.setInputs(StringUtils.objToString(inputs));
        content.setRaw(raw);

        // set debug
        if (debug.isDebug())
        {
            // content.setDebug(debug.toString());
            content.setDebug("true");
        }
        else
        {
            content.setDebug("");
        }

        // write debug
        WorksheetUtils.writeDebugCAS(debug);

        options.setAffinity("RSCONTROL");
        params.put(Constants.EXECUTE_AFFINITY, "RSCONTROL");
        
        // send execution request
        MResult.actionResult(options, content, true);
    } // sendResultError
    
    static void sendResultErrorOnAbort(String executeid, String processid, String wiki, String problemid, String reference, String userid, ResolveActionTaskVO actiontask, Map inputs, Map params, Map flows, ScriptDebug debug, String mock, String raw, long processTimeout) throws Exception
    {
        // init reference
        if (reference == null)
        {
            reference = "";
        }

        AssessResult assessResult  = new AssessResult("false", "-1", raw, raw, 0, "");
        assessResult.setCondition("BAD");
        assessResult.setSeverity("CRITICAL");
        assessResult.setSummary(raw);
        assessResult.setDetail(raw);
        assessResult.setDuration(0);
        
        String activityId = (String)params.get("activityId");
        
        // String problemid, String executeid, String userid, String targetAddr, String targetGuid, String affinity, String actionid, String processid, AssessResult assessment, String actionName, String actionNamespace, String actionSummary, String actionHidden, String actionTags, String actionRoles, String command, String returncode, String actionResultGUID, String raw, String logRawResult, String mock
        Map<String, ActionProcessTaskRequest> aptrMap = null;
        ActionProcessTask apt = null;
        if (StringUtils.isNotBlank(processid))
        {
            aptrMap = ProcessRequest.getExec2TaskReqMap(processid);
            apt = ((Map<String, ActionProcessTask>)ProcessRequest.getModelStates(processid).get(ProcessRequest.EXECID_TASK))
            	  .get(executeid);
        }
        if (aptrMap==null && StringUtils.isNotBlank(processid))
        {
        	throw new RuntimeException(String.format("Can't find ActionProcessTaskRequest map for processid: %s, " +
					 								 "problemid: %s", processid, problemid));
        }
        final String nodeid = apt != null ? apt.getId() : "";
        ActionProcessTaskRequest aptr = aptrMap != null ? aptrMap.get(executeid) : null;
        String execNumber = aptr != null ? aptr.getNumber() : "";
        String execReqNum = StringUtils.isNotEmpty(executeid) ? execNumber : "";

        long timeStamp = System.currentTimeMillis();
        MResult.logActionResult(problemid, executeid, userid, "", Constants.ESB_NAME_RSCONTROL, Constants.ESB_NAME_RSCONTROL, actiontask.getId(), processid, assessResult, actiontask.getUName(), actiontask.getUName(), raw, "false", "", "", "", "-1", "", raw, "false", mock, activityId, params, nodeid, wiki, execReqNum, timeStamp);
    } // sendResultErrorOnAbort

    static String actionAbort(String executeid, String taskid, Targets targets) throws Exception
    {
        String result = null;
        try
        {
            // find task
            // ActionTask actiontask = ActionTask.get(taskid);
            ResolveActionTaskVO actiontask = ServiceHibernate.getResolveActionTask(taskid, null, "system");
            if (actiontask == null)
            {
                throw new Exception("FAIL: unable to find ActionTask: " + taskid);
            }

            // check if actiontask is active
            if (actiontask.getUActive() == false)
            {
                throw new Exception("FAIL: ActionTask is not active: " + actiontask.getUName());
            }

            // getTarget
            if (targets == null || targets.size() == 0)
            {
                throw new Exception("FAIL: Missing target(s) for the action request");
            }

            for (Iterator i = targets.keySet().iterator(); i.hasNext();)
            {
                TargetInstance targetInst = (TargetInstance) i.next();
                if (targetInst != null)
                {
                    Map content = new HashMap();
                    content.put(Constants.EXECUTE_TASKNAME, executeid + "/" + actiontask.getUName());

                    Main.esb.sendInternalMessage(targetInst.getGuid(), "MAction.abortRequest", content);
                }
            }
            result = "SUCCESS sent ActionTask abort request";

        }
        catch (Throwable e)
        {
            Log.log.warn(e.getMessage(), e);
            result = e.getMessage();

        }

        return result;
    } // actionAbort

    public static String getConfigProperty(String guid, String name) throws Exception
    {
        String result = null;

        ResolveRegistrationPropertyVO prop = ServiceHibernate.findResolveRegistrationProperty(name, guid);
        if(prop != null)
        {
            result = prop.getUValue();
        }
        else
        {
            Log.log.warn("Unknown config property name: " + name + " guid: " + guid);
        }

        return result;
    } // getConfigProperty

    static String replaceConfig(String targetGuid, String template) throws Exception
    {
        String result = "";

        if (template != null)
        {
            String matchContent = template;
            Matcher matcher = VAR_REGEX_CONFIG.matcher(matchContent);
            int last = 0;
            while (matcher.find())
            {
                String name = matcher.group(1);
                String value = getConfigProperty(targetGuid, name);
                if (value == null)
                {
                    result += matchContent.substring(last, matcher.start());
                }
                else
                {
                    result += matchContent.substring(last, matcher.start()) + value;
                }
                last = matcher.end();
            }
            result += matchContent.substring(last, matchContent.length());
        }

        return result;
    } // replaceConfig

    static ExecuteResult preprocessActionTask(ResolvePreprocessVO preprocess, String processid, String wiki, String actionName, String problemid, String userid, Targets targets, Map inputs, Map outputs, Map params, Map flows, ScriptDebug debug, int taskTimeout, ActionTaskProperties properties) throws Exception
    {
        ExecutionTimeUtil.log((String) params.get("RESOLVE.JOB_ID"), null, null, ExecutionStep.METHOD_EXECUTION, ExecutionStepPhase.START);
    	
        ExecuteResult result = null;

        if (preprocess != null)
        {
            if (Log.log.isDebugEnabled()) {
                Log.log.debug("Preprocess: " + preprocess);
            }
            
            if (preprocess != null)
            {
                Log.log.info("Calling ActionTask preprocess: " + preprocess.getUName());
                debug.println("Calling ActionTask preprocess: " + preprocess.getUName());

                // Decrypt map to allow user to use it natively
                List preInputs = EncryptionTransportObject.decryptMap(inputs);
                List preParams = EncryptionTransportObject.decryptMap(params);
                List preFlows = EncryptionTransportObject.decryptMap(flows);

                try
                {
                    // init script
                    String script = preprocess.getUScript();
                    String scriptName = preprocess.getUName();

                    script = VariableUtil.replaceVarsString(script, inputs, outputs, params, flows);
                    if (!StringUtils.isEmpty(script))
                    {
                        boolean cacheable = preprocess.getUScript().equals(script);

                        // init script debug
                        debug.setComponent("Preprocess:" + actionName);
                        debug.setMap(inputs, outputs, params, flows);

                        // set bindings
                        Binding binding = new Binding();
                        binding.setVariable(Constants.GROOVY_BINDING_PREPROCESS, new BindingScript(BindingScript.TYPE_PREPROCESS, binding));
                        binding.setVariable(Constants.GROOVY_BINDING_LOG, Log.log);
                        binding.setVariable(Constants.GROOVY_BINDING_SYSLOG, Log.syslog);
                        binding.setVariable(Constants.GROOVY_BINDING_DEBUG, debug);
                        binding.setVariable(Constants.GROOVY_BINDING_TARGETS, targets);
                        binding.setVariable(Constants.GROOVY_BINDING_ESB, Main.esb);
                        binding.setVariable(Constants.GROOVY_BINDING_PROCESSID, processid);
                        binding.setVariable(Constants.GROOVY_BINDING_PROBLEMID, problemid);
                        binding.setVariable(Constants.GROOVY_BINDING_WIKI, wiki);
                        binding.setVariable(Constants.GROOVY_BINDING_USERID, userid);
                        binding.setVariable(Constants.GROOVY_BINDING_PARAMS, params);
                        binding.setVariable(Constants.GROOVY_BINDING_FLOWS, flows);
                        binding.setVariable(Constants.GROOVY_BINDING_INPUTS, inputs);
                        binding.setVariable(Constants.GROOVY_BINDING_OUTPUTS, outputs);
                        binding.setVariable(Constants.GROOVY_BINDING_PROPERTIES, properties);

                        if (script.contains(Constants.GROOVY_BINDING_WSDATA))
                        {
	                        WSDataMap wsData = ServiceWorksheet.getWorksheetWSDATA(problemid);
	                        binding.setVariable(Constants.GROOVY_BINDING_WSDATA, wsData);
                        }

                        ProcessInfo processInfo = getProcessInfo(processid);
                        if (processInfo != null)
                        {
                            binding.setVariable(Constants.GROOVY_BINDING_PROCESSINFO, processInfo);
                            binding.setVariable(Constants.GROOVY_BINDING_GLOBALS, processInfo);
                        }

                        // add OLD_PARAMS if exists
                        if (params.containsKey(Constants.GROOVY_BINDING_OLD_PARAMS))
                        {
                            binding.setVariable(Constants.GROOVY_BINDING_OLD_PARAMS, params.get(Constants.GROOVY_BINDING_OLD_PARAMS));
                        }

                        // execute preprocessor
                        Object obj = GroovyScript.executeAsync(script, scriptName, cacheable, binding, taskTimeout * 1000);

                        //WSDATA gets stored when the client call WSDATA.put() or WSDATA["xxx"]="yyy" so this call is unnecessary.
                        //Makes it efficient as the data Map may grow throughout the execution of a runbook.
                        //ServiceWorksheet.saveWorksheetWSDATA(problemid, wsData, userid);
                        
                        if (obj != null && obj instanceof ExecuteResult)
                        {
                            result = (ExecuteResult) obj;
                        }
                    }
                }
                catch (Exception e)
                {
                    Log.log.warn(e.getMessage(), e);
                    debug.print(e.getMessage(), e);

                    throw new ReturnUserErrorException("Preprocessor Error: " + e.getMessage());
                }
                finally
                {
                    // WorksheetUtils.writeDebug(debug);
                    WorksheetUtils.writeDebugCAS(debug);

                    // Restore Encryption objects
                    EncryptionTransportObject.restoreMap(inputs, preInputs);
                    EncryptionTransportObject.restoreMap(params, preParams);
                    EncryptionTransportObject.restoreMap(flows, preFlows);
                }
            }
        }

        ExecutionTimeUtil.log((String) params.get("RESOLVE.JOB_ID"), null, null, ExecutionStep.METHOD_EXECUTION, ExecutionStepPhase.END);
    	
        return result;
    } // preprocessActionTask

    public static Targets initTargets(Map params)
    {
        Targets result = new Targets();
        
        String orgSuffix = "";
        
        if (StringUtils.isNotBlank((String)params.get(Constants.EXECUTE_ORG_ID)) &&
            StringUtils.isNotBlank((String)params.get(Constants.EXECUTE_ORG_NAME)))
        {
            orgSuffix = ((String)params.get(Constants.EXECUTE_ORG_NAME)).replaceAll("[^0-9A-Za-z_]", "").toUpperCase();
        }
        
        result.add(Constants.ESB_NAME_RSREMOTE, Constants.ESB_NAME_RSREMOTE + orgSuffix);

        return result;
    } // initTargets

    public static boolean conditionIsGT(String cond1, String cond2)
    {
        boolean result = false;

        int condValue1 = getConditionValue(cond1);
        int condValue2 = getConditionValue(cond2);

        if (condValue1 > condValue2)
        {
            result = true;
        }

        return result;
    } // conditionIsGT

    public static boolean conditionIsLT(String cond1, String cond2)
    {
        boolean result = false;

        int condValue1 = getConditionValue(cond1);
        int condValue2 = getConditionValue(cond2);

        if (condValue1 < condValue2)
        {
            result = true;
        }

        return result;
    } // conditionIsLT

    public static boolean severityIsGT(String severity1, String severity2)
    {
        boolean result = false;

        int severityValue1 = getSeverityValue(severity1);
        int severityValue2 = getSeverityValue(severity2);

        if (severityValue1 > severityValue2)
        {
            result = true;
        }

        return result;
    } // severityIsGT

    public static boolean severityIsLT(String severity1, String severity2)
    {
        boolean result = false;

        int severityValue1 = getSeverityValue(severity1);
        int severityValue2 = getSeverityValue(severity2);

        if (severityValue1 < severityValue2)
        {
            result = true;
        }

        return result;
    } // severityIsLT

    public static int getConditionValue(String condition)
    {
        int result = 0;

        if (condition.equalsIgnoreCase(Constants.ASSESS_CONDITION_GOOD))
        {
            result = 1;
        }
        else if (condition.equalsIgnoreCase(Constants.ASSESS_CONDITION_BAD))
        {
            result = -1;
        }

        return result;
    } // getConditionValue

    public static int getSeverityValue(String severity)
    {
        int result = 0;

        if (severity.equalsIgnoreCase(Constants.ASSESS_SEVERITY_GOOD))
        {
            result = 1;
        }
        else if (severity.equalsIgnoreCase(Constants.ASSESS_SEVERITY_WARNING))
        {
            result = -2;
        }
        else if (severity.equalsIgnoreCase(Constants.ASSESS_SEVERITY_SEVERE))
        {
            result = -3;
        }
        else if (severity.equalsIgnoreCase(Constants.ASSESS_SEVERITY_CRITICAL))
        {
            result = -4;
        }

        return result;
    } // getSeverityValue

    public static void createProcessInfo(String processid, String wiki)
    {
        if (Log.log.isDebugEnabled()) {
            Log.log.debug("createProcessInfo processid: " + processid);
        }
        ProcessInfo info = getProcessInfo(processid);
        if (info == null)
        {
            ProcessContext.put(processid, Constants.PROCESS_MAP_PROCESS_INFO, info = new ProcessInfo(processid, wiki));
        }
        processInfoMap.put(processid, info);
        if (Log.log.isDebugEnabled()) {
            Log.log.debug("Completed createProcessInfo processid: " + processid);
        }
    } // createProcessInfo

    public static ProcessInfo getProcessInfo(String processid)
    {
        ProcessInfo result = null;

        if (processid != null)
        {
            result = (ProcessInfo) ProcessContext.get(processid, Constants.PROCESS_MAP_PROCESS_INFO);
            if (Log.log.isDebugEnabled()) {
                Log.log.debug("getProcessInfo processid: " + processid);
                Log.log.debug("Completed getProcessInfo processid: " + processid);
            }
        }

        return result;
    } // getProcessInfo

    public static void removeProcessInfo(String processid)
    {
        if (Log.log.isDebugEnabled()) {
            Log.log.debug("removeProcessInfo processid: " + processid);
        }

        ProcessInfo.cleanupProcess(processid);

        // cleanup processinfo
        ProcessInfo processInfo = ProcessExecution.getProcessInfo(processid);
        if (processInfo == null)
        {
            processInfo = processInfoMap.get(processid);
        }

        processInfoMap.remove(processid);
        if (processInfo != null)
        {
            processInfo.destroy();
        }

        // remove processinfo
        ProcessContext.remove(processid, Constants.PROCESS_MAP_PROCESS_INFO);
        if (Log.log.isDebugEnabled()) {
            Log.log.debug("Completed removeProcessInfo processid: " + processid);
        }
    } // removeProcessInfo

    public static long getCompletedDuration(String runbook)
    {
        long result = 0;

        Long duration = runbookCompletedDuration.get(runbook);
        if (duration != null)
        {
            result = duration.longValue();
        }
        return result;
    } // getCompletedDuration

    public static void resetCompletedDuration(String runbook)
    {
        runbookCompletedDuration.remove(runbook);
        // runbookCompletedDuration.put(runbook, new Long(0));
    } // resetCompletedDuration

    public static long getCompleted(String runbook)
    {
        long result = 0;

        Long count = runbookCompleted.get(runbook);
        if (count != null)
        {
            result = count.longValue();
        }
        return result;
    } // getCompleted

    public static void resetCompleted(String runbook)
    {
        runbookCompleted.remove(runbook);
        // runbookCompleted.put(runbook, new Long(0));
    } // resetCompleted

    public static long getAborted(String runbook)
    {
        long result = 0;

        Long count = runbookAborted.get(runbook);
        if (count != null)
        {
            result = count.longValue();
        }
        return result;
    } // getAborted

    public static void resetAborted(String runbook)
    {
        runbookAborted.remove(runbook);
        // runbookAborted.put(runbook, new Long(0));
    } // resetAborted

    public static long getRunbookConditionGood(String runbook)
    {
        long result = 0;

        Long count = runbookConditionGood.get(runbook);
        if (count != null)
        {
            result = count.longValue();
        }
        return result;
    } // getRunbookConditionGood

    public static void resetRunbookConditionGood(String runbook)
    {
        runbookConditionGood.remove(runbook);
    } // resetRunbookConditionGood

    public static long getRunbookConditionBad(String runbook)
    {
        long result = 0;

        Long count = runbookConditionBad.get(runbook);
        if (count != null)
        {
            result = count.longValue();
        }
        return result;
    } // getRunbookConditionBad

    public static void resetRunbookConditionBad(String runbook)
    {
        runbookConditionBad.remove(runbook);
    } // resetRunbookConditionBad

    public static long getRunbookConditionUnknown(String runbook)
    {
        long result = 0;

        Long count = runbookConditionUnknown.get(runbook);
        if (count != null)
        {
            result = count.longValue();
        }
        return result;
    } // getRunbookConditionUnknown

    public static void resetRunbookConditionUnknown(String runbook)
    {
        runbookConditionUnknown.remove(runbook);
    } // resetRunbookConditionUnknown

    public static long getRunbookSeverityGood(String runbook)
    {
        long result = 0;

        Long count = runbookSeverityGood.get(runbook);
        if (count != null)
        {
            result = count.longValue();
        }
        return result;
    } // getRunbookSeverityGood

    public static void resetRunbookSeverityGood(String runbook)
    {
        runbookSeverityGood.remove(runbook);;
    } // resetRunbookSeverityGood

    public static long getRunbookSeverityCritical(String runbook)
    {
        long result = 0;

        Long count = runbookSeverityCritical.get(runbook);
        if (count != null)
        {
            result = count.longValue();
        }
        return result;
    } // getRunbookSeverityCritical

    public static void resetRunbookSeverityCritical(String runbook)
    {
        runbookSeverityCritical.remove(runbook);
    } // resetRunbookSeverityCritical

    public static long getRunbookSeverityWarning(String runbook)
    {
        long result = 0;

        Long count = runbookSeverityWarning.get(runbook);
        if (count != null)
        {
            result = count.longValue();
        }
        return result;
    } // getRunbookSeverityWarning

    public static void resetRunbookSeverityWarning(String runbook)
    {
        runbookSeverityWarning.remove(runbook);
    } // resetRunbookSeverityWarning

    public static long getRunbookSeveritySevere(String runbook)
    {
        long result = 0;

        Long count = runbookSeveritySevere.get(runbook);
        if (count != null)
        {
            result = count.longValue();
        }
        return result;
    } // getRunbookSeveritySevere

    public static void resetRunbookSeveritySevere(String runbook)
    {
        runbookSeveritySevere.remove(runbook);
    } // resetRunbookSeveritySevere

    public static long getRunbookSeverityUnknown(String runbook)
    {
        long result = 0;

        Long count = runbookSeverityUnknown.get(runbook);
        if (count != null)
        {
            result = count.longValue();
        }
        return result;
    } // getRunbookSeverityUnknown

    public static void resetRunbookSeverityUnknown(String runbook)
    {
        runbookSeverityUnknown.remove(runbook);
    } // resetRunbookSeverityUnknown

    /*
     * HP TO DO 
     * 
     * Collection of completed runbook statistics should be asynchronous like 
     * WikiIndexer 
     */
    private static void updateCompleted(String processid, String wiki, Map params)
    {
        long duration = 0;

        ExecutionTimeUtil.log((String) params.get("RESOLVE.JOB_ID"), null, null, ExecutionStep.METHOD_EXECUTION, ExecutionStepPhase.START);
        
        
        // update parent wiki
        ProcessState state = ProcessExecution.getProcessState(processid);
        if (state != null)
        {
            duration = state.getTotalDuration();
        }

        // update
        Long count = runbookCompleted.get(wiki);
        if (count == null)
        {
            runbookCompletedDuration.put(wiki, duration);
            runbookCompleted.put(wiki, new Long(1));
        }
        else
        {
            Long totalDuration = runbookCompletedDuration.get(wiki);
            if (totalDuration == null)
            {
                totalDuration = new Long(0);
            }
            totalDuration += duration;
            count++;

            runbookCompletedDuration.put(wiki, totalDuration);
            runbookCompleted.put(wiki, count);
        }
        
        ExecutionTimeUtil.log((String) params.get("RESOLVE.JOB_ID"), null, null, ExecutionStep.METHOD_EXECUTION, ExecutionStepPhase.END);
        
    } // updateCompleted
    
    /*
     * HP TO DO 
     * 
     * Collection of runbook conditions statistics should be asynchronous like 
     * WikiIndexer 
     */
    static void updateCondition(String runbook, String condition)
    {
        Long count;
        if(condition.equalsIgnoreCase(Constants.ASSESS_CONDITION_GOOD))
        {
            count = runbookConditionGood.get(runbook);
            if (count == null)
            {
                runbookConditionGood.put(runbook, new Long(1));
            }
            else
            {
                runbookConditionGood.put(runbook, count + 1);
            }
        }
        else if(condition.equalsIgnoreCase(Constants.ASSESS_CONDITION_BAD))
        {
            count = runbookConditionBad.get(runbook);
            if (count == null)
            {
                runbookConditionBad.put(runbook, new Long(1));
            }
            else
            {
                runbookConditionBad.put(runbook, count + 1);
            }
        }
        else if(condition.equalsIgnoreCase(Constants.ASSESS_CONDITION_UNKNOWN))
        {
            count = runbookConditionUnknown.get(runbook);
            if (count == null)
            {
                runbookConditionUnknown.put(runbook, new Long(1));
            }
            else
            {
                runbookConditionUnknown.put(runbook, count + 1);
            }
        }

    } // updateCondition
    
    /*
     * HP TO DO 
     * 
     * Collection of runbook severity statistics should be asynchronous like 
     * WikiIndexer 
     */
    static void updateSeverity(String runbook, String severity)
    {
        Long count;

        if (severity.equalsIgnoreCase(Constants.ASSESS_SEVERITY_GOOD))
        {
            count = runbookSeverityGood.get(runbook);
            if (count == null)
            {
                runbookSeverityGood.put(runbook, new Long(1));
            }
            else
            {
                runbookSeverityGood.put(runbook, count + 1);
            }
        }
        else if (severity.equalsIgnoreCase(Constants.ASSESS_SEVERITY_WARNING))
        {
            count = runbookSeverityWarning.get(runbook);
            if (count == null)
            {
                runbookSeverityWarning.put(runbook, new Long(1));
            }
            else
            {
                runbookSeverityWarning.put(runbook, count + 1);
            }
        }
        else if (severity.equalsIgnoreCase(Constants.ASSESS_SEVERITY_SEVERE))
        {
            count = runbookSeveritySevere.get(runbook);
            if (count == null)
            {
                runbookSeveritySevere.put(runbook, new Long(1));
            }
            else
            {
                runbookSeveritySevere.put(runbook, count + 1);
            }
        }
        else if (severity.equalsIgnoreCase(Constants.ASSESS_SEVERITY_CRITICAL))
        {
            count = runbookSeverityCritical.get(runbook);
            if (count == null)
            {
                runbookSeverityCritical.put(runbook, new Long(1));
            }
            else
            {
                runbookSeverityCritical.put(runbook, count + 1);
            }
        }
        else if (severity.equalsIgnoreCase(Constants.ASSESS_SEVERITY_UNKNOWN))
        {
            count = runbookSeverityUnknown.get(runbook);
            if (count == null)
            {
                runbookSeverityUnknown.put(runbook, new Long(1));
            }
            else
            {
                runbookSeverityUnknown.put(runbook, count + 1);
            }
        }

    } // updateSeverity
    
    /*
     * HP TO DO 
     * 
     * Collection of aborted runbook statistics should be asynchronous like 
     * WikiIndexer 
     */
    private static void updateAborted(String runbook)
    {
        String condition = "BAD";
        String severity = "CRITICAL";

        // update
        updateCondition(runbook, condition);
        updateSeverity(runbook, severity);
        Long count = runbookAborted.get(runbook);
        if (count == null)
        {
            runbookAborted.put(runbook, new Long(1));
        }
        else
        {
            runbookAborted.put(runbook, count + 1);
        }
    } // updateAborted

    public static long getRunbookTotal()
    {
        return runbookTotal.get();
    } // getRunbookTotal

    public static void resetRunbookTotal()
    {
        runbookTotal.set(0);
    } // resetRunbookTotal

    public static long getRunbookStartLatency()
    {
        return runbookStartLatency.get();
    } // getRunbookStartLatency

    public static void resetRunbookStartLatency()
    {
        runbookStartLatency.set(0);
    } // resetRunbookStartLatency
    
    public static void setMergeTargets(Map flows, String processid, String executeid, int size)
    {
        if (StringUtils.isNotEmpty(processid))
        {
            Map processMap = ProcessContext.getProcessMap(processid);
            if (processMap != null)
            {
                // add names for cleanup
                List<String> atomicNumberNames = (List<String>) processMap.get(Constants.PROCESS_MAP_MERGE_TARGETS);
                if (atomicNumberNames == null)
                {
                    atomicNumberNames = new ArrayList<String>();
                    processMap.put(Constants.PROCESS_MAP_MERGE_TARGETS, atomicNumberNames);
                }
                if(flows.get(Constants.MERGE_TARGETS) == null)
                {
                    atomicNumberNames.add(Constants.MERGE_TARGETS + "/" + processid + "/" + executeid);
    
                    // add atomic number
                    AtomicLong number = ProcessContext.getAtomicNumber(Constants.MERGE_TARGETS + "/" + processid + "/" + executeid);
                    if (number != null)
                    {
                        number.addAndGet(size);
                    }
    
                    // add atomic number name to flow
                    flows.put(Constants.MERGE_TARGETS, Constants.MERGE_TARGETS + "/" + processid + "/" + executeid);
                }
                else
                {
                    AtomicLong number = ProcessContext.getAtomicNumber((String) flows.get(Constants.MERGE_TARGETS));
                    if(number !=null)
                    {
						//add size - 1 to account for merge targets decreasing just the children.
                        number.addAndGet(size-1);
                    }
                }
            }
            else
            {
                Log.log.error("Missing processMap - processid: " + processid);
            }
        }
    } // setMergeTargets

    public static void removeMergeTargets(String processid)
    {
        Map processMap = ProcessContext.getProcessMap(processid);
        if (processMap != null)
        {
            List<String> atomicNumberNames = (List<String>) processMap.get(Constants.PROCESS_MAP_MERGE_TARGETS);
            if (atomicNumberNames != null)
            {
                for (String name : atomicNumberNames)
                {
                    ProcessContext.removeAtomicNumber(name);
                }

                processMap.remove(Constants.PROCESS_MAP_MERGE_TARGETS);
            }
        }
    } // removeMergeTargets

    public static ProcessState getProcessState(String processid)
    {
        ProcessState result = null;
        if (!isProcessCompleted(processid))
        {
            result = (ProcessState) ProcessContext.get(processid, Constants.PROCESS_MAP_PROCESS_STATE);
            if (Log.log.isDebugEnabled()) {
                if (result == null)
                {
                    Log.log.debug("getProcessState - processid: " + processid + " state.processid: null");
                }
                else
                {
                    Log.log.debug("getProcessState - processid: " + processid + " state.processid: " + result.getProcessid());
                }
            }
        }
        else
        {
            Log.log.warn("Process already completed - processid: "+processid);
        }

        return result;
    } // getProcessState

    public static void updateLastActivity(String processid)
    {
        ReentrantLock lock = LockUtils.getLock(Constants.LOCK_REMOVEPROCESS+"/"+processid);
        try
        {
            lock.lock();
            
            // check if process is still running and update lastActivityTime
            ProcessState processState = ProcessExecution.getProcessState(processid);
            if (processState != null)
            {
                processState.setLastActivityTime(System.currentTimeMillis());
            }
        }
        catch (Throwable e)
        {
            Log.log.error(e.getMessage(), e);
        }
        finally {
            lock.unlock();
        }
    } 

    public static ProcessState createProcessState(String processid, String wiki, boolean hasLoop, long startTime, long processTimeout, 
    									  String token, String rsviewIdGuid, boolean checkPreConditionNullVars, 
    									  String processNumber)
    {
        ProcessState state = new ProcessState(processid, hasLoop, wiki, startTime, processTimeout, token, rsviewIdGuid, 
        									  checkPreConditionNullVars, processNumber);
        ProcessContext.put(processid, Constants.PROCESS_MAP_PROCESS_STATE, state);
        return state;
    } 
    
    public static void setProcessState(String processid, ProcessState state)
    {
        ProcessContext.put(processid, Constants.PROCESS_MAP_PROCESS_STATE, state);
    } 

    public static String getActionInvocOption(ResolveActionTaskVO actiontask, ResolveActionInvocVO invoc, String optionName, Map params)
    {
        ExecutionTimeUtil.log((String) params.get("RESOLVE.JOB_ID"), null, null, ExecutionStep.METHOD_EXECUTION, ExecutionStepPhase.START);
        
        String result = null;

        List<ResolveActionInvocOptionsVO> options = optionCache.get(actiontask.getSys_id());
        if (options == null)
        {
            options = (List<ResolveActionInvocOptionsVO>) invoc.getResolveActionInvocOptions();
            if (options != null)
            {
                /*
                for (ResolveActionInvocOptionsVO o : options)
                {
                    break;
                }
                */
                optionCache.put(actiontask.getSys_id(), options);
            }
        }

        if (options != null)
        {
            for (ResolveActionInvocOptionsVO option : options)
            {
                if (option.getUName().equals(optionName))
                {
                    result = option.getUValue();
                    break;
                }
            }
        }
        
        ExecutionTimeUtil.log((String) params.get("RESOLVE.JOB_ID"), null, null, ExecutionStep.METHOD_EXECUTION, ExecutionStepPhase.END);

        return result;
    } // getActionInvocOption

} // ProcessExecution
