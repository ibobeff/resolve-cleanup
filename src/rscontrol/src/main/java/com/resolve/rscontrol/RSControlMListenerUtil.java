/******************************************************************************
* (C) Copyright 2014
*
* Resolve Systems, LLC
*
* All rights reserved
* This software is distributed under license and may not be used, copied,
* modified, or distributed without the express written permission of
* Resolve Systems, LLC.
*
******************************************************************************/

package com.resolve.rscontrol;

import java.util.Date;
import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.PriorityBlockingQueue;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.atomic.AtomicBoolean;
import java.util.concurrent.atomic.AtomicLong;

import org.eclipse.jetty.util.ConcurrentHashSet;

import com.rabbitmq.client.Channel;
import com.resolve.esb.DefaultHandler;
import com.resolve.esb.MMsgHeader;
import com.resolve.esb.amqp.MAmqpServer;
import com.resolve.execute.ExecuteRemote;
import com.resolve.rsbase.MainBase;
import com.resolve.services.archive.BaseArchiveTable;
import com.resolve.thread.TaskExecutor;
import com.resolve.thread.ScheduledExecutor;
import com.resolve.util.Constants;
import com.resolve.util.DBPoolUtils;
import com.resolve.util.EncryptionTransportObject;
import com.resolve.util.Log;
import com.resolve.util.StringUtils;
import com.resolve.util.ThreadUtils;

public class RSControlMListenerUtil
{
    static float EXECUTOR_BUSY_RATIO = 0.9f;
    static float SYSTEM_EXECUTOR_BUSY_RATIO = 0.9f;
    static float START_EXECUTOR_BUSY_RATIO = 0.9f;
    static float DBPOOL_BUSY_RATIO = 0.9f;
    static int RUNBOOK_PRIORITY_NORMAL_VALUE = Constants.RUNBOOK_PRIORITY_NORMAL_VALUE;
	static boolean USE_ACTIVE_RUNBOOK_COUNT = false;
    static int MAX_RUNNING_RUNBOOK_NUM = 400;
    static int MIN_RUNNING_RUNBOOK_NUM = 1;
    static String RUNBOOK_PRIORITY_NORMAL = Integer.toString(RUNBOOK_PRIORITY_NORMAL_VALUE);
    static DefaultHandler handler;

    public static ConcurrentHashSet<Object> activeRunbooks = new ConcurrentHashSet<>();
    public static AtomicLong maxExpirationTime = new AtomicLong(0);
    static AtomicBoolean isRSControlBusy = new AtomicBoolean(false);

    static boolean isAMQP = true;
    
//    static ConcurrentHashMap<String,Long> runningProcesses = new ConcurrentHashMap<String,Long>();
//    static ConcurrentHashMap<String,Long> completedProcesses = new ConcurrentHashMap<String,Long>();

    static PriorityBlockingQueue<RunbookExecMsg> runbookQueue = new PriorityBlockingQueue<RunbookExecMsg>();

    static
    {
        startRunbookSchedulingThread();
        busyRatioCheck();
    }

    public static void waitForActiveCompletion()
    {
        Log.log.info("Wait activity completion");
        while ((getActiveRunbook() > 0 || ExecuteRemote.sessionPool.size() > 0) && System.currentTimeMillis() < maxExpirationTime.get())
        {
            Log.log.warn("Pending Active Runbooks - count: "+getActiveRunbook()+" timeout: " + new Date(maxExpirationTime.get()));
            ThreadUtils.sleep(5000);
        }

        if (getActiveRunbook() > 0)
        {
            Log.log.warn("SHUTDOWN TIMEOUT - Terminating with pending Runbooks  - count: "+getActiveRunbook());
        }

    } // waitForActiveCompletion

    static AtomicLong timeoutCoutner = new AtomicLong();
    private static void processRunbookTimeout(RunbookExecMsg m)
    {
        Map<String, String> params = (Map<String, String>) m.params;

        String wiki = (String) params.get(Constants.EXECUTE_WIKI);

        Long timeout = ProcessRequest.cacheProcessTimeout.get(wiki);
        Log.log.error("Dropping runbook request: " + m.params);
        Log.log.error("Runbook execution request timed out while waiting on queue to be executed! Runbook name: " + wiki + ", process timeout: " + timeout);
        Log.log.error("Runbook timeout counter="+ timeoutCoutner.incrementAndGet() + ", runbookQueue size=" +runbookQueue.size() );
    }

    private static boolean isRunbookExecMsgTimeout(RunbookExecMsg msg)
    {
    	//Keep old timeout semantics, need to streamline this later
    	return false;
    	
//    	boolean result;
//        Map<String, String> params = (Map<String,String>)msg.params;
//        String wiki = (String)params.get(Constants.EXECUTE_WIKI);
//        Integer timeout = ProcessRequest.cacheProcessTimeout.get(wiki);
//        if (timeout == null)
//        {
//            Log.log.error("Missing PROCESS_TIMEOUT for wiki: "+wiki, new Exception());
//            result = false;
//        }
//        else
//        {
//            long processTimeout = timeout * 1000;
//            if ((System.currentTimeMillis() - msg.timeStamp) > processTimeout)
//            {
//                result = true;
//            }
//            else
//            {
//                result = false;
//            }
//        }
//        return result;
    }

    private static void printExecutorQueueSize()
    {
    	int s = ScheduledExecutor.getInstance().getQueueSize();
    	Log.log.info("ScheduledExecutor queue size=" + s + ", active threads=" + ScheduledExecutor.getInstance().getActiveThreads() + ", pool size=" + ScheduledExecutor.getInstance().getPoolSize() + ", max pool size=" + ScheduledExecutor.getInstance().getMaxPoolSize() + ", core pool size=" + ScheduledExecutor.getInstance().getCorePoolSize() + ", max thread=" + ScheduledExecutor.getInstance().getMaxThread());
    }
    
    public static void busyRatioCheck()
    {
        Thread check = new Thread(new Runnable()
        {
            public void run()
            {
                while (true)
                {
                    try
                    {
                        Thread.sleep(30000); 
                        //calculate and print busy ratios
                        if (Log.log.isDebugEnabled())
                        {
                            Log.log.debug(String.format("DB pool busy ratio: r=%f", 
                            							(((DBPoolUtils.getTotalSize() - DBPoolUtils.getAvailableSize()) * 1.0F) / 
                            							 DBPoolUtils.getMaxPoolSize())));
                                
                            Log.log.debug(String.format("ScheduledExecutor busy ratio: r=%f",
                            							(((float)ScheduledExecutor.getInstance().getActiveThreads())/
                            									ScheduledExecutor.getInstance().getInstance().getMaxThread())));
                        }
                    }
                    catch (InterruptedException ex)
                    {
                        Log.log.error("Runboook Busy Ratio Check Died by Unexpected Interruption.", ex);
                        break;
                    }
                    catch (NullPointerException npe)
                    {
                        Log.log.warn("Runbook Busy Ratio Check Not Ready Yet");
                        Log.log.trace(npe, npe);
                    }
                    catch (Throwable ex1)
                    {
                        Log.log.error("Runbook Busy Ratio Check Died Because of Unexpected Exception.", ex1);
                        break;
                    }
                }
            }
        });
        check.start();
    } //busyRatioCheck
    
    public static void startRunbookSchedulingThread()
    {
        Thread cleanup = new Thread(new Runnable()
        {
            public void run()
            {
                while (true)
                {
                    RunbookExecMsg msg=null;
                    
                    //Log.log.info("startRunbookSchedulingThread calling isSystemBusy first time");
                    if (isSystemBusy())
                    {
                        Log.log.warn("startRunbookSchedulingThread system is busy first time, finding messages with priority greater than " + RUNBOOK_PRIORITY_NORMAL_VALUE);
                        msg = runbookQueue.peek();
                        while (msg!=null && msg.priority > RUNBOOK_PRIORITY_NORMAL_VALUE)
                        {
                            //We don't care if top element is the same as msg, but it at least has the same priority
                            msg = runbookQueue.poll();
                            if (Log.log.isDebugEnabled()) {
                                Log.log.debug("System is busy. Dequeue high prority msg only, msg priority=" + msg.priority + ", params= " + (Map)msg.params);
                            }
                            Log.log.warn("System is busy. Dequeueing high prority msg with priority=" + msg.priority);
                            
                            if (isRunbookExecMsgTimeout(msg))
                            {
                              processRunbookTimeout(msg);
                              continue;
                            }

                            if (msg.delay <= 0)
                            {
                                // immediate delivery
                            	TaskExecutor.execute(msg.jobid, msg.header, handler, "execute", msg.params);
                            }
                            else
                            {
                                // delayed delivery
                            	ScheduledExecutor.getInstance().executeDelayed(msg.jobid, msg.header, handler, "execute", msg.params, msg.delay, TimeUnit.SECONDS);
                            }
                            msg = runbookQueue.peek();
                        }
                    }
                    else
                    {
                        if(Log.log.isTraceEnabled()) {
                            Log.log.trace("startRunbookSchedulingThread system is not busy first time rubbookQueue size is " + runbookQueue.size());
                        }
                        
                        msg = runbookQueue.poll();
                        if (msg!=null)
                        {
                            if (Log.log.isDebugEnabled()) {
                                Log.log.debug("System is not busy, dequeue a message with priority=" + msg.priority + ", params= " + (Map)msg.params);
                                Log.log.debug("startRunbookSchedulingThread system is not busy, dequeued a message with priority=" + msg.priority + ", runbookQueue size= " + runbookQueue.size());
                            }
                            //System.out.println("************ System is not busy, dequeue msg, priority=" + msg.priority + ",params: " + (Map)msg.params);
                        }
                        else
                        {
                            if(Log.log.isTraceEnabled()) {
                                Log.log.trace("System is not busy, has no message to process, runbookQueue size is " + runbookQueue.size());
                            }
                            
                            //System.out.println("************ System is not busy, dequeue msg, params: " + msg);
                        }

                        int msgCount = 0;
                        while (msg != null && msgCount <= 10)
                        {
                            if (isRunbookExecMsgTimeout(msg))
                            {
                                processRunbookTimeout(msg);
                            }
                            else
                            {
                                if (Log.log.isDebugEnabled()) {
                                    Log.log.debug("Runbook timeout counter="+ timeoutCoutner.get() + ", queue size=" +runbookQueue.size() + ", message counter" + runbookMsgCounter.get());
                                    Log.log.debug("startRunbookSchedulingThread system is not busy, executing message " + (msg.delay <= 0 ? "immediately" : "after " + msg.delay + " secs, msgCount = " + msgCount));
                                }
                                
                                if (msg.delay <= 0)
                                {
                                    // immediate delivery
                                	TaskExecutor.execute(msg.jobid, msg.header, handler, "execute", msg.params);
                                }
                                else
                                {
                                    // delayed delivery
                                	ScheduledExecutor.getInstance().executeDelayed(msg.jobid, msg.header, handler, "execute", msg.params, msg.delay, TimeUnit.SECONDS);
                                }
                            }

                            msg = runbookQueue.poll();
                            if (msg!=null)
                            {
                                if (Log.log.isDebugEnabled()) {
                                    Log.log.debug("System is not busy, dequeue a message with priority=" + msg.priority + ", params= " + (Map)msg.params);
                                    Log.log.debug("System is not busy, dequeued another message with priority=" + msg.priority + ", runbookQueue size= " + runbookQueue.size() + ", msgCount=" + msgCount);
                                }
                                //System.out.println("************ System is not busy, dequeue msg, priority=" + msg.priority + ",params: " + (Map)msg.params);
                            }
                            else
                            {
                                //System.out.println("************ System is not busy, dequeue msg, params: " + msg);
                            }

                            if (++msgCount > 10)
                            {
                                if (Log.log.isDebugEnabled()) {
                                    Log.log.debug("startRunbookSchedulingThread calling isSystemBusy second time with msgCount = " + msgCount);
                                }
                                if (!isSystemBusy())
                                {
                                    msgCount = 0;
                                    if (Log.log.isDebugEnabled()) {
                                        Log.log.debug("startRunbookSchedulingThread system is still not busy resetting msgCount to 0");
                                    }                                    
                                    
                                    try
                                    {
                                        Thread.sleep(5000);
                                    }
                                    catch (InterruptedException ex)
                                    {
                                        //ignores
                                    }
                                }
                                else
                                {
                                    if (msg!=null)
                                    {
                                        runbookQueue.add(msg);
                                        Log.log.info("System is busy, enqued back previously dequeued message with priority=" + msg.priority + ", runbookQueue size= " + runbookQueue.size() + ", msgCount=" + msgCount);
                                    }
                                }
                            }
                        }
                    }

                    try
                    {
                        if (msg==null)
                        {
                            // Wake up every 2 seconds to schedule runbook execution if runbookQueue is empty
                            Thread.sleep(2000);
                        }
                        else
                        {
                            // Wake up every 3 seconds to schedule runbook execution if runbookQueue is not empty
                            Thread.sleep(3000);
                        }
                    }
                    catch (InterruptedException ex)
                    {
                        Log.log.error("Runbook execution scheduling thread died by unexpected interruption.", ex);
                        break;
                    }
                    catch (Throwable ex1)
                    {
                        Log.log.error("Runbook execution scheduling thread died because of unexpected exception.", ex1);
                        break;
                    }
                }
            }

        });
        cleanup.start();
    }


    private static boolean dbPoolBusy() {
        int s = DBPoolUtils.getPoolSize();
        int s1 = DBPoolUtils.getMaxPoolSize();
        float r = ((s1-s) * 1.0F) / s1;

        if (r > DBPOOL_BUSY_RATIO) {
            if(Log.log.isTraceEnabled()) {
                Log.log.trace(String.format("DB pool is busy (old): r=%f, available=%d, max=%d", r, s, s1));
            }
        }
		        
        r = ((DBPoolUtils.getTotalSize() - DBPoolUtils.getAvailableSize()) * 1.0F) / DBPoolUtils.getMaxPoolSize();
        
        if (r > DBPOOL_BUSY_RATIO) {
            Log.log.warn(String.format("DB pool is busy (new): r=%f, available=%d, max=%d, total=%d",
									   r, DBPoolUtils.getAvailableSize(), DBPoolUtils.getMaxPoolSize(), 
									   DBPoolUtils.getTotalSize()));
        }
        
        return r > DBPOOL_BUSY_RATIO;
    }

    private static boolean systemExecutorBusy() {
    	int maxSize = ScheduledExecutor.getInstance().getMaxPoolSize() == Integer.MAX_VALUE ? MainBase.main.configGeneral.scheduledPool :
    		ScheduledExecutor.getInstance().getMaxPoolSize();
    	
        float r = ((float)ScheduledExecutor.getInstance().getActiveThreads())/maxSize;
        if (r > START_EXECUTOR_BUSY_RATIO) {
            Log.log.warn(String.format("ScheduledExecutor is busy: r=%f, active threads=%d, max thread pool size=%d",
            						   r, ScheduledExecutor.getInstance().getActiveThreads(), maxSize));
        }

        return r > START_EXECUTOR_BUSY_RATIO;
    }

    private static boolean tooFewRBRunning()
    {
        //System.out.println("+++++++++++++++++ runbookTotol=" + MAction.runningRBTotal.get());
        return getActiveRunbook() < MIN_RUNNING_RUNBOOK_NUM;
    }

    private static boolean tooManyActiveRBs()
    {
    	boolean b = USE_ACTIVE_RUNBOOK_COUNT && getActiveRunbook() >= MAX_RUNNING_RUNBOOK_NUM;
    	if (b)
    	{
    		Log.log.info("Has too many active runbooks running: " + getActiveRunbook());
    	}
    	
    	return b;
    }
    
    // NOTE: TO IGNORE ACTIVE RUNBOOK COUNT FOR SYSTEM BUSY SET CONFIGGENERAL/@USERUNBOOKCOUNT TO FALSE IN BLUEPRINT PROPERTY
    
    private static boolean isSystemBusy() {
    	boolean b = false;
        if (!tooFewRBRunning()) {
        	b = (dbPoolBusy() || systemExecutorBusy() || tooManyActiveRBs()) ;
	        if (b){
	        	printExecutorQueueSize();
	        }
        }

        isRSControlBusy.set(b);
        // For in-line archive i.e. archive within RSControl use of which should be discontinued 
        BaseArchiveTable.isSystemBusy=b; 
        return b;
    }

    static AtomicLong runbookMsgCounter = new AtomicLong();

    private static class RunbookExecMsg implements Comparable<RunbookExecMsg>
    {
        String jobid;
        MMsgHeader header;
        Object invokeObject;
        String methodname;
        Object params;
        long   delay;
        TimeUnit unit;
        int priority;
        long timeStamp;

        RunbookExecMsg(String jobid, MMsgHeader header, Object invokeObject, String methodname, Object params, long delay, TimeUnit unit, int priority)
        {
            this.jobid=jobid;
            this.header=header;
            this.invokeObject=invokeObject;
            this.methodname=methodname;
            this.params=params;
            this.delay=delay;
            this.unit=unit;
            this.priority=priority;
            timeStamp = System.currentTimeMillis();
        }

        RunbookExecMsg(String jobid, MMsgHeader header, Object invokeObject, String methodname, Object params, int priority)
        {
            this.jobid=jobid;
            this.header=header;
            this.invokeObject=invokeObject;
            this.methodname=methodname;
            this.params=params;
            this.delay=-100000;
            this.priority=priority;
            timeStamp = System.currentTimeMillis();
        }

        @Override
        public int compareTo(RunbookExecMsg o)
        {
            if (priority>o.priority)
                return -1;
            else if (priority<o.priority)
                return 1;
            else
            {
                long d = timeStamp - o.timeStamp;
                if (d>0)
                    return 1;
                else if (d<0)
                    return -1;
                else
                    return 0;
            }
        }
    }

//    private static void startCleanupThread()
//    {
//        Thread cleanup = new Thread(new Runnable()
//        {
//            public void run()
//            {
//                while(true)
//                {
//                    long now = System.currentTimeMillis(); // minutes before processid is removed.
//                    for (String processid : completedProcesses.keySet())
//                    {
//                        if ((now - completedProcesses.get(processid)) > 20*60*1000) //20 minutes after runbook is done
//                        {
//                            completedProcesses.remove(processid);
//                            runningProcesses.remove(processid);
//                        }
//                    }
//                    try
//                    {
//                        Thread.sleep(20000); // Wake up every 20 seconds to clean up
//                    }
//                    catch (InterruptedException ex)
//                    {
//                        Log.log.error("RSControlMListener cleanup thread is interrupted unexpectedly.", ex);
//                        break;
//                    }
//                }
//            }
//
//        });
//        cleanup.start();
//    } // startCleanupThread

    public static int getActiveRunbook()
    {
        return activeRunbooks.size();
    } // getActiveRunbook

    public static int addActiveRunbook(String processid)
    {
        activeRunbooks.add(processid);
        return activeRunbooks.size();
    } // addActiveRunbook

    public static int removeActiveRunbook(String processid)
    {
        activeRunbooks.remove(processid);
        return activeRunbooks.size();
    } // removeActiveRunbook

    public static boolean enqueueRunbookExecMsg(String jobid, MMsgHeader header, Object invokeObject, String methodname, Object params, long delay, TimeUnit unit)
    {
        boolean result = true;
        if(invokeObject instanceof DefaultHandler)
        {
            /*
             * TODO This is a bad idea, we need to refactor this in future.
             * Bug: Write to static field com.resolve.rscontrol.RSControlMListener.handler from
             * instance method com.resolve.rscontrol.RSControlMListener.enqueueRunbookExecMsg(String, MMsgHeader, Object, String, Object, long, TimeUnit)
             */
            handler = (DefaultHandler) invokeObject;
        }
        EncryptionTransportObject.restoreMap((Map) params, null);
        if (Log.log.isDebugEnabled()) {
            Log.log.debug("Scheduler execution counter=" + runbookMsgCounter.incrementAndGet() + ", runbook queue size=" + runbookQueue.size() + ", message params= " + StringUtils.mapToLogString((Map) params));
        }
        Log.log.info("Scheduler execution counter=" + runbookMsgCounter.get() + ", runbookQueue size=" + runbookQueue.size());
        Log.log.info("enqueueRunbookExecMsg calling isSystemBusy");
        if (isSystemBusy())
        {
            int priority = 0;
            if ("executeProcess".equals(methodname))
            {
                Map map = (Map)params;
                String p = (String)map.get(Constants.EXECUTE_RUNBOOK_PRIORITY);
                String wiki = (String)map.get(Constants.EXECUTE_WIKI);           // required

                if (p==null)
                {
                    String processXML = null;
                    try
                    {
                        if (!ProcessRequest.cacheActionProcessDependency.containsKey(wiki))
                        {
                          processXML = ProcessRequest.getWikiDocumentModelXML(wiki, Constants.MODEL_TYPE_MAIN);
                        }
                        // generate main dependency tree
                        ActionProcessDependency processDependency = ProcessRequest.getActionProcessDependency(wiki, processXML, Constants.MODEL_TYPE_MAIN);

                        if (processDependency != null)
                        {
                            ActionProcessTask startTask = (ActionProcessTask) processDependency.getModel().getStartNode().getValue();
                            Object obj = startTask.getParams().get(Constants.EXECUTE_RUNBOOK_PRIORITY);
                            if (obj!=null)
                            {
                                p = obj.toString();
                            }
                        }
                    }
                    catch (Throwable e)
                    {
                        Log.log.error(e.getMessage(), e);
                    }
                }

                if (p==null)
                {
                    p= RUNBOOK_PRIORITY_NORMAL;
                }
                priority = Integer.parseInt(p);
            }

            RunbookExecMsg msg = null;
            if (delay>0)
            {
                msg = new RunbookExecMsg(jobid, header, invokeObject, methodname, params, delay, unit, priority);
            }
            else
            {
                msg = new RunbookExecMsg(jobid, header, invokeObject, methodname, params, priority);
            }

            runbookQueue.add(msg);
            //System.out.println("+++++++++ System is busy, enqueue msg, priority=" + priority + ", params= " + (Map)msg.params);
            if (Log.log.isDebugEnabled()) {
                Log.log.debug("System is busy, enqueue msg, priority=" + priority + ", params= " + (Map)msg.params);
            }
            Log.log.info("System is busy, enqueue msg, priority=" + priority + ", runbookQueue size = " + runbookQueue.size());
        }
        else
        {
            Log.log.info("System is not busy, execute runbook " + (delay == 0 ? "immediately" : " after " + delay + " seconds") + ", runbookQueue size = " + runbookQueue.size());
            
            if (delay == 0)
            {
                // immediate delivery
            	TaskExecutor.execute(jobid, header, handler, "execute", params);
            }
            else
            {
                // delayed delivery
                ScheduledExecutor.getInstance().executeDelayed(jobid, header, handler, "execute", params, delay, TimeUnit.SECONDS);
            }
            result = false;
        }

        return result;
    }
    
    static void pollEventQueue()
    {
    	MAmqpServer mServer = (MAmqpServer) Main.main.mServer;
        while (true)
        {
            //Log.log.info("pollEventQueue calling isSystemBusy");
            if (!isSystemBusy())
            {
                try
                {
                	if (mServer != null && Main.isActive() && Main.isAMQPServer())
                    {
                		Channel eventChannel = mServer.getChannel();
                        if(eventChannel == null || !eventChannel.isOpen())
                        {
                            //this check is necessary because RSMQ connection could get disconnected and channel closed
                            while(true)
                            {
                                eventChannel = mServer.getChannel();
                                if(eventChannel != null && eventChannel.isOpen())
                                {
                                    break;
                                }
                                else
                                {
                                    if (Log.log.isDebugEnabled()) {
                                        Log.log.debug("pollEventQueue: Channel is not open. Sleeping while waiting to re-establish RSMQ connection and get channel.");
                                    }
                                    Thread.sleep(2000); 
                                }
                            }
                        }
                        
                        Channel durableEventChannel = mServer.getDurableChannel();
                        if(durableEventChannel == null || !durableEventChannel.isOpen())
                        {
                            //this check is necessary because RSMQ connection could get disconnected and channel closed
                            while(true)
                            {
                                durableEventChannel = mServer.getDurableChannel();
                                if(durableEventChannel != null && durableEventChannel.isOpen())
                                {
                                    break;
                                }
                                else
                                {
                                	if (Log.log.isDebugEnabled()) {
                                		Log.log.debug("pollEventQueue: Channel used for durable messaging is not open. " +
                                					  "Sleeping while waiting to re-establish RSMQ connection and get channel " +
                                					  "used for durable messaging.");
                                	}
                                    Thread.sleep(2000); 
                                }
                            }
                        }
                    }
                }
                catch (Exception ex)
                {
                    Log.log.error("pollEventQueue error " + ex.getMessage() + " in processing message",ex);
                }
            }
            try {
            	Thread.sleep(2000);
            } catch (InterruptedException e) {
            	Log.log.info("pollEventQueue sleep (2 secs) interrupted");
            }
        }
    }
    
} // RSControlMListener
