/******************************************************************************
* (C) Copyright 2014
*
* Resolve Systems, LLC
*
* All rights reserved
* This software is distributed under license and may not be used, copied,
* modified, or distributed without the express written permission of
* Resolve Systems, LLC.
*
******************************************************************************/

package com.resolve.rscontrol;

import java.lang.management.ManagementFactory;
import java.lang.management.ThreadMXBean;
import java.util.Iterator;
import java.util.Map;

import org.apache.commons.collections.MapUtils;
import org.apache.commons.lang3.tuple.Pair;

import com.resolve.execute.ExecuteOS;
import com.resolve.execute.ExecuteResult;
import com.resolve.rsbase.BaseMetric;
import com.resolve.search.SearchConstants;
import com.resolve.util.MetricMsg;

public class Metric extends BaseMetric
{
    public Metric ()
    {
        super(new MetricThresholdInitImpl());
    }
    
    public void sendMetrics()
    {
        if(mthreshold.thresholdProperties.keySet().isEmpty())
        {
            mthreshold.setThresholdProperties();
        }
        
        // send jvm
    	sendMetricJVM();
        
        // send runbook
        sendMetricRunbook();
        
        // send transaction
        sendMetricTransaction();
        
        // send cpu (only unix)
        sendMetricCPU();
        
        // send actiontask execute metric
        sendMetricActionTask();

        // send runbook message count in queue
        sendMetricJMSMessagesCount();
        
    } // sendMetrics

    void sendMetricJVM()
    { 
    	MetricMsg result = new MetricMsg("JVM", Main.main.release.getHostServiceName()+"/"+Main.main.configId.getGuid(), "");
    	
        // MemoryPercentFree
        long free = (Runtime.getRuntime().maxMemory() - Runtime.getRuntime().totalMemory()) + Runtime.getRuntime().freeMemory();
        int value = (int) (free * 100 / Runtime.getRuntime().maxMemory());
    	result.addMetric("mem_free", value);
        
    	mthreshold.checkThresholds("jvm", value, "mem_free");
    	
//        String metricname = "JVM".toLowerCase() + ".mem_free";
//        
//        // threshold memory free value 
//        if (value < mthreshold.thresholdProperties.get(metricname).getLow())
//        {
//            Log.alert(ERR.E70001, "Low available memory: "+value);
//        }
    	
    	// PeakThread
        ThreadMXBean thread = ManagementFactory.getThreadMXBean();
    	result.addMetric("thread_count", thread.getThreadCount());
//    	int thread_count_percentage = (int) (thread.getThreadCount() * 100) / (Main.main.configGeneral.getMaxThread() * 2); //approx max for the jvm thread = double the thread poll max
//    	mthreshold.checkThresholds("jvm", thread_count_percentage, "thread_count");
    	Main.sendMetric(result);
    	
    } // getMetricJVM
    
    
    void sendMetricJMSMessagesCount()
    {
    	
        MetricMsg result = new MetricMsg("JMS_QUEUE", Main.main.release.getHostServiceName()+"/"+Main.main.configId.getGuid(), "");
        
        // JMS Msgs Count
        int jmsMsgsCount = RSControlMListenerUtil.runbookQueue.size();
        result.addMetric("msgs_count", jmsMsgsCount);
        mthreshold.checkThresholds("jms_queue", jmsMsgsCount, "msgs_count");        
        
        Main.sendMetric(result);
    	
    } // getMetricJVM

    void sendMetricRunbook()
    { 
        // loop each runbook in the map
        for (String runbook : ProcessExecution.runbookCompleted.keySet())
        {
	        // init
	    	MetricMsg result = new MetricMsg("Runbook", Main.main.release.getHostServiceName()+"/"+Main.main.configId.getGuid(), runbook);
    	
            Long totalDuration = ProcessExecution.getCompletedDuration(runbook);
            Long completed = ProcessExecution.getCompleted(runbook);
            Long aborted = ProcessExecution.getAborted(runbook);
            Long conditiongood = ProcessExecution.getRunbookConditionGood(runbook);
            Long conditionbad = ProcessExecution.getRunbookConditionBad(runbook);
            Long conditionunknown = ProcessExecution.getRunbookConditionUnknown(runbook);
            Long severitygood = ProcessExecution.getRunbookSeverityGood(runbook);
            Long severitycritical = ProcessExecution.getRunbookSeverityCritical(runbook);
            Long severitywarning = ProcessExecution.getRunbookSeverityWarning(runbook);
            Long severitysevere = ProcessExecution.getRunbookSeveritySevere(runbook);
            Long severityunknown = ProcessExecution.getRunbookSeverityUnknown(runbook);
            if(totalDuration <= 0)
            {
                totalDuration = new Long(1);
            }
            
	    	result.addMetric("aborted", aborted);
            mthreshold.checkThresholds("runbook", aborted.intValue(), "aborted");
	    	result.addMetric("completed", completed);
            mthreshold.checkThresholds("runbook", completed.intValue(), "completed");
	    	result.addMetric("gcondition", conditiongood);
            mthreshold.checkThresholds("runbook", conditiongood.intValue(), "gcondition");
	    	result.addMetric("bcondition", conditionbad);
            mthreshold.checkThresholds("runbook", conditionbad.intValue(), "bcondition");
	    	result.addMetric("ucondition", conditionunknown);
            mthreshold.checkThresholds("runbook", conditionunknown.intValue(), "ucondition");
	    	result.addMetric("gseverity", severitygood);
            mthreshold.checkThresholds("runbook", severitygood.intValue(), "gseverity");
	    	result.addMetric("cseverity", severitycritical);
            mthreshold.checkThresholds("runbook", severitycritical.intValue(), "cseverity");
	    	result.addMetric("wseverity", severitywarning);
            mthreshold.checkThresholds("runbook", severitywarning.intValue(), "wseverity");
	    	result.addMetric("sseverity", severitysevere);
            mthreshold.checkThresholds("runbook", severitysevere.intValue(), "sseverity");
	    	result.addMetric("useverity", severityunknown);
            mthreshold.checkThresholds("runbook", severityunknown.intValue(), "useverity");
	    	result.addMetric("duration", totalDuration, completed.intValue());
            mthreshold.checkThresholds("runbook", totalDuration.intValue(), "duration");
	    	Main.sendMetric(result);
	    	
	    	// reset counters
	    	ProcessExecution.resetCompletedDuration(runbook);
	    	ProcessExecution.resetCompleted(runbook);
	    	ProcessExecution.resetAborted(runbook);
            ProcessExecution.resetRunbookConditionGood(runbook);
            ProcessExecution.resetRunbookConditionBad(runbook);
            ProcessExecution.resetRunbookConditionUnknown(runbook);
            ProcessExecution.resetRunbookSeverityGood(runbook);
            ProcessExecution.resetRunbookSeverityCritical(runbook);
            ProcessExecution.resetRunbookSeverityWarning(runbook);
            ProcessExecution.resetRunbookSeveritySevere(runbook);
            ProcessExecution.resetRunbookSeverityUnknown(runbook);
        }
        
        //Account for only abort count, runbook condition, and severity  in the aborted runbooks
        for (String runbook : ProcessExecution.runbookAborted.keySet())
        {
            // init
            MetricMsg result = new MetricMsg("Runbook", Main.main.release.getHostServiceName()+"/"+Main.main.configId.getGuid(), runbook);
            
            Long totalDuration = 0L;
            Long completed = 0L;
            Long aborted = ProcessExecution.getAborted(runbook);
            Long conditiongood = 0L;
            Long conditionbad = ProcessExecution.getRunbookConditionBad(runbook);
            Long conditionunknown = 0L;
            Long severitygood = 0L;
            Long severitycritical = ProcessExecution.getRunbookSeverityCritical(runbook);
            Long severitywarning = 0L;
            Long severitysevere = 0L;
            Long severityunknown = 0L;
            
            result.addMetric("aborted", aborted);
            mthreshold.checkThresholds("runbook", aborted.intValue(), "aborted");
            result.addMetric("completed", completed);
            mthreshold.checkThresholds("runbook", completed.intValue(), "completed");
            result.addMetric("gcondition", conditiongood);
            mthreshold.checkThresholds("runbook", conditiongood.intValue(), "gcondition");
            result.addMetric("bcondition", conditionbad);
            mthreshold.checkThresholds("runbook", conditionbad.intValue(), "bcondition");
            result.addMetric("ucondition", conditionunknown);
            mthreshold.checkThresholds("runbook", conditionunknown.intValue(), "ucondition");
            result.addMetric("gseverity", severitygood);
            mthreshold.checkThresholds("runbook", severitygood.intValue(), "gseverity");
            result.addMetric("cseverity", severitycritical);
            mthreshold.checkThresholds("runbook", severitycritical.intValue(), "cseverity");
            result.addMetric("wseverity", severitywarning);
            mthreshold.checkThresholds("runbook", severitywarning.intValue(), "wseverity");
            result.addMetric("sseverity", severitysevere);
            mthreshold.checkThresholds("runbook", severitysevere.intValue(), "sseverity");
            result.addMetric("useverity", severityunknown);
            mthreshold.checkThresholds("runbook", severityunknown.intValue(), "useverity");
            result.addMetric("duration", totalDuration, completed.intValue());
            mthreshold.checkThresholds("runbook", totalDuration.intValue(), "duration");
            Main.sendMetric(result);
        
            // reset counters
            ProcessExecution.resetCompletedDuration(runbook);
            ProcessExecution.resetCompleted(runbook);
            ProcessExecution.resetAborted(runbook);
            ProcessExecution.resetRunbookConditionGood(runbook);
            ProcessExecution.resetRunbookConditionBad(runbook);
            ProcessExecution.resetRunbookConditionUnknown(runbook);
            ProcessExecution.resetRunbookSeverityGood(runbook);
            ProcessExecution.resetRunbookSeverityCritical(runbook);
            ProcessExecution.resetRunbookSeverityWarning(runbook);
            ProcessExecution.resetRunbookSeveritySevere(runbook);
            ProcessExecution.resetRunbookSeverityUnknown(runbook);
        }
    } // sendMetricRunbook

    void sendMetricTransaction()
    { 
        // init
    	MetricMsg result = new MetricMsg("Transaction", Main.main.release.getHostServiceName()+"/"+Main.main.configId.getGuid(), "");
    	
        Long total = ProcessExecution.getRunbookTotal();
        Long totalLatency = ProcessExecution.getRunbookStartLatency();
            
    	result.addMetric("transaction", total);
    	mthreshold.checkThresholds("transaction", total.intValue(), "transaction");
    	result.addMetric("latency", totalLatency, total.intValue());
    	mthreshold.checkThresholds("transaction", totalLatency.intValue(), "latency");
    	Main.sendMetric(result);
    	
    	ProcessExecution.resetRunbookTotal();
    	ProcessExecution.resetRunbookStartLatency();
    } // sendMetricTransaction
    
    void sendMetricCPU()
    {
        String osname = System.getProperty("os.name").toLowerCase();
        if (osname.equals("linux") || osname.equals("sunos") || osname.equals("aix"))
        {
            // execute and parse uptime
            ExecuteOS os = new ExecuteOS(Main.main.getProductHome());
            ExecuteResult result = os.execute("uptime", null, true, 10, "sendMetricCPU");
            String out = result.getResultString();
            if (out != null)
            {
            	Map<String, Pair<Long, Integer>> cpuMetrics = MetricMsg.parseLinuxCPUMetricOutput(out);
            	
            	if (MapUtils.isNotEmpty(cpuMetrics)) {
            		MetricMsg msg = new MetricMsg("Server", 
                							  	  Main.main.release.getHostServiceName() + "/" + 
                							  	  Main.main.configId.getGuid(), "");

            		if (cpuMetrics.containsKey(MetricMsg.CPU_METRIC_LOAD_1)) {
            			msg.addMetric(MetricMsg.CPU_METRIC_LOAD_1, 
            						  cpuMetrics.get(MetricMsg.CPU_METRIC_LOAD_1).getLeft().longValue());
            			mthreshold.checkThresholds("server", 
            									   cpuMetrics.get(MetricMsg.CPU_METRIC_LOAD_1).getRight().intValue() / 100, 
            									   MetricMsg.CPU_METRIC_LOAD_1);
            		}
            		
            		if (cpuMetrics.containsKey(MetricMsg.CPU_METRIC_LOAD_5)) {
            			msg.addMetric(MetricMsg.CPU_METRIC_LOAD_5, 
            						  cpuMetrics.get(MetricMsg.CPU_METRIC_LOAD_5).getLeft().longValue());
            			mthreshold.checkThresholds("server", 
            									   cpuMetrics.get(MetricMsg.CPU_METRIC_LOAD_5).getRight().intValue() / 100, 
            									   MetricMsg.CPU_METRIC_LOAD_5);
            		}
            		
            		if (cpuMetrics.containsKey(MetricMsg.CPU_METRIC_LOAD_15)) {
            			msg.addMetric(MetricMsg.CPU_METRIC_LOAD_15, 
            						  cpuMetrics.get(MetricMsg.CPU_METRIC_LOAD_15).getLeft().longValue());
            			mthreshold.checkThresholds("server", 
            									   cpuMetrics.get(MetricMsg.CPU_METRIC_LOAD_15).getRight().intValue() / 100, 
            									   MetricMsg.CPU_METRIC_LOAD_15);
            		}
            		
            		Main.sendMetric(msg);
            	}
            }
        }
    } // sendMetricCPU
    
    void sendMetricActionTask()
    {
        Map<String,Map<String,MetricUnitActionTask>> metrics = MetricActionTask.getMetrics();
        if (metrics != null && metrics.size() > 0)
        {
            for (Iterator i=metrics.entrySet().iterator(); i.hasNext(); )
            {
                Map.Entry entry = (Map.Entry)i.next();
                String key = (String)entry.getKey();
                Map<String,MetricUnitActionTask> units = (Map<String,MetricUnitActionTask>)entry.getValue();
                
                if (units != null && units.size() > 0)
                {
                    String[] groupNameId = key.split("/");
                    String groupName = groupNameId[0];
                    String id = groupNameId[1];
                    
    		    	MetricMsg msg = new MetricMsg(SearchConstants.METRIC_ACTIONTASK_UNIT_PREFIX + groupName, Main.main.release.getHostServiceName()+"/"+Main.main.configId.getGuid(), id);
                    
                    for (MetricUnitActionTask unit : units.values())
                    {
        		    	msg.addMetric(unit.getName(), unit.getTotalValue().longValue(), unit.getTotalCount().intValue());
                    }
                    
                    // send metrics
    		    	Main.sendMetric(msg);
                    
                    // remove units
                    units.clear();
                }
                
            }
        }
        
    } // sendMetricActionTask
    
} // Metric
