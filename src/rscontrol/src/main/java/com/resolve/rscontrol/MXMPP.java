/******************************************************************************
* (C) Copyright 2014
*
* Resolve Systems, LLC
*
* All rights reserved
* This software is distributed under license and may not be used, copied,
* modified, or distributed without the express written permission of
* Resolve Systems, LLC.
*
******************************************************************************/

package com.resolve.rscontrol;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.resolve.rsbase.MainBase;
import com.resolve.persistence.model.XMPPFilter;
import com.resolve.services.ServiceGateway;
import com.resolve.util.Log;
import com.resolve.util.StringUtils;

public class MXMPP extends MGateway
{
    private static final String MODEL_NAME = XMPPFilter.class.getSimpleName();

    protected String getModelName()
    {
        return MODEL_NAME;
    }

    public void setXMPPAddresses(Map<String, String> addresses)
    {
        ServiceGateway.setMXMPPAddresses(addresses);
    } // setXMPPAddresses

    protected List<Map<String, String>> getXMPPAddresses(String queueName, boolean isSocialPoster)
    {
        return ServiceGateway.getMXMPPAddresses(queueName, isSocialPoster);
    } // getXMPPAddresses

    /**
     * This method is overridden because in addition to "filter" XMPP gateway also needs to
     * synchronize addresses.
     */
    @Override
    public void synchronizeGateway(Map<String, String> params)
    {
        String gatewayName = params.get("GATEWAY_NAME");
        String queueName = params.get("QUEUE_NAME");
        String messageHandlerName = params.get("MESSAGE_HANDLER_NAME");
        String orgName = params.get("ORG_NAME");
        
        boolean isSocialPoster = false;
        if (params.containsKey("SOCIAL_POSTER"))
        {
            isSocialPoster = Boolean.parseBoolean(params.get("SOCIAL_POSTER"));
        }

        Log.log.debug("Gateway Synchronization message received with following parameters:");
        Log.log.debug("     GATEWAY_NAME: " + gatewayName);
        Log.log.debug("     QUEUE_NAME: " + queueName);
        Log.log.debug("     MESSAGE_HANDLER_NAME: " + messageHandlerName);
        Log.log.debug("     SOCIAL_POSTER: " + isSocialPoster);
        if (StringUtils.isBlank(gatewayName) || StringUtils.isBlank(queueName) || StringUtils.isBlank(messageHandlerName))
        {
            Log.log.error("GATEWAY_NAME, QUEUE_NAME, MESSAGE_HANDLER_NAME must be passed along with the message for synchronizing gateways");
            throw new RuntimeException("GATEWAY_NAME, QUEUE_NAME, MESSAGE_HANDLER_NAME must be passed along with the message for synchronizing gateways");
        }
        else
        {
            Map<String, Object> messageParams = new HashMap<String, Object>();
            messageParams.put("XMPPADDRESSES", getXMPPAddresses(gatewayName, isSocialPoster));
            messageParams.put("FILTERS", getFilters(gatewayName));
            messageParams.put("NAMEPROPERTIES", getNameProperties(gatewayName));
            messageParams.put("ROUTINGSCHEMAS", getRoutingSchemas(gatewayName, orgName));

            //send the message back to the requester who sent this message.
            Log.log.trace("SENDING synchronization message to " + gatewayName + " gateway using queue " + queueName);
            if (MainBase.esb.sendInternalMessage(queueName, messageHandlerName + ".synchronizeGateway", messageParams) == false)
            {
                Log.log.warn("Failed to send synchronization message to " + gatewayName + " gateway.");
            }
        }
    }
} // MXMPP
