/******************************************************************************
* (C) Copyright 2014
*
* Resolve Systems, LLC
*
* All rights reserved
* This software is distributed under license and may not be used, copied,
* modified, or distributed without the express written permission of
* Resolve Systems, LLC.
*
******************************************************************************/

package com.resolve.rscontrol;

import java.io.DataInput;
import java.io.DataOutput;
import java.io.IOException;
import java.io.Serializable;
import java.util.Date;

//import com.hazelcast.nio.DataSerializable;

public class MasterElect implements Serializable
{
	private static final long serialVersionUID = 1668950776087846195L;
	String guid;
    long lastUpdated;
    
    public MasterElect()
    {
        this.guid = "";
        this.lastUpdated = 0;
    } // MasterElect 
    
    public MasterElect(String guid)
    {
        this.guid = guid;
        this.lastUpdated = System.currentTimeMillis();
    } // MasterElect
    
    public String toString()
    {
        return "[guid: "+guid+" lastUpdated: "+new Date(lastUpdated)+"]";
    }
    
    public String getGuid()
    {
        return guid;
    }
    public void setGuid(String guid)
    {
        this.guid = guid;
    }
    public long getLastUpdated()
    {
        return lastUpdated;
    }
    public void setLastUpdated(long lastUpdated)
    {
        this.lastUpdated = lastUpdated;
    }
    
    public void writeData(DataOutput out) throws IOException 
    {
        out.writeUTF(guid);
        out.writeLong(lastUpdated);
    } // writeData

    public void readData(DataInput in) throws IOException 
    {
        guid         = in.readUTF();
        lastUpdated  = in.readLong();
    } // readData

} // MasterElect
