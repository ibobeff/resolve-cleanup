/******************************************************************************
* (C) Copyright 2014
*
* Resolve Systems, LLC
*
* All rights reserved
* This software is distributed under license and may not be used, copied,
* modified, or distributed without the express written permission of
* Resolve Systems, LLC.
*
******************************************************************************/

package com.resolve.rscontrol;

import java.util.Collections;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ConcurrentMap;
import java.util.concurrent.atomic.AtomicLong;
import java.util.concurrent.locks.ReentrantLock;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.apache.commons.collections.CollectionUtils;

import com.resolve.groovy.script.GroovyScript;
import com.resolve.services.ServiceWorksheet;
import com.resolve.services.WSDataMap;
import com.resolve.services.util.PropertiesUtil;
import com.resolve.util.Constants;
import com.resolve.util.LockUtils;
import com.resolve.util.Log;
import com.resolve.util.StringUtils;
import com.resolve.util.XDoc;
import com.resolve.util.performance.ExecutionTimeUtil;
import com.resolve.util.performance.datasources.ExecutionStep;
import com.resolve.util.performance.datasources.ExecutionStepPhase;

import groovy.lang.Binding;

public class ActionProcessTaskRequest
{
    final static Pattern VAR_REGEX_EXPRESSION = Pattern.compile(Constants.VAR_REGEX_EXPRESSION);
    final static String DEPENDENCIES_STATUS_CHECK_LOCK_PREFIX = "DSCL";
    
    String number;
    String execute;
    String userid;
    String task;
    String taskname;
    String tasknamespace;
    String tasksummary;
    String taskhidden;
    String tasktags;
    String process;
    String problem;
    String reference;
    String event;
    Map params = new HashMap();
    ConcurrentMap<String,Object> requested; 		        // targetAddress => Boolean
    List depends; 		        // list of ActionProcessTaskDependency
    String modelType;           // MAIN, FINAL, ABORT
    boolean isMergedTargets = false;    // isMergeTargets
    
    @SuppressWarnings("unchecked")
    public synchronized boolean evaluate(String processid, String problemid, boolean completed, String condition, String severity, String actionName, String completedExecuteid, String target, Map runtimeParams, Map outputs, Map flows, String dependentNodeId) throws Exception
    {
    	ExecutionTimeUtil.log((String) runtimeParams.get("RESOLVE.JOB_ID"), null, null, ExecutionStep.METHOD_EXECUTION, ExecutionStepPhase.START);
    	
        boolean result = false;
        boolean isMergeAny = false;
        boolean isMergeTargets = false;
        boolean oldMergeTargets = false;
        
        Log.log.info(String.format("EVALUATING NEXT TASK: %s-%s", taskname, dependentNodeId));
        
        if (ProcessExecution.isProcessCompleted(processid) == false)
        {
        	List<ActionProcessTaskDependency> taskDeps = null;
        	
	            // determine if all dependencies are satisfied - must loop all dependencies to independently set status for each type
	            // ASSESS dependencies must be evaluated first as EXPRESSIONS may need their results if the precondition has and incoming color line
	            taskDeps = ((Map<String,ActionProcessTask>)ProcessRequest.getModelStates(processid).get(ProcessRequest.EXECID_TASK)).get(execute).depends;
	                        
	            for (int i = 0; i < taskDeps.size(); i++) //ActionProcessTaskDependency dependency : taskDeps)
	            {
	                ActionProcessTaskDependency dependency = taskDeps.get(i);
	                String type = dependency.getType();
	                String mergeType = dependency.getMerge();
	                
	                // init status
	                boolean status = true;
	                
	                // init merge type
	    	        if (mergeType != null)
	    	        {
	    		        if (mergeType.equals("ANY"))
	    		        {
	    		            // NOTE: for isMergeAny (true) - the evaluate() should only consider the CURRENT evaluate and disregard all previous / saved states
	    		            isMergeAny = true;
	    		        }
	    		        else if (mergeType.equals("TARGETS"))
	    		        {
	    		            isMergeTargets = true;
	    		        }
	    	        }
	    	        oldMergeTargets = isMergeTargets;
	    	        // merge targets
	    	        if (isMergeTargets)
	    	        {
	    	            // get latch name from FLOWS["MERGE_TARGETS"]
	    	            String name = (String)flows.get(Constants.MERGE_TARGETS);
	    	            if (name != null)
	    	            {
	                        AtomicLong latch = ProcessContext.getAtomicNumber(name);
	                        long count = latch.get();
	    		            
	    	                // skip target until last target addr
	                        if (count > 1)
	    		            {
	    		                result = false;
	    		            }
	    		            else
	    		            {
	                            isMergeTargets = false;
	                            setMergedTargets(true);
	    		            }
	    	            }
	    	            else
	    	            {
	    	                isMergeTargets = false;
	    	                setMergedTargets(true);
	    	            }
	    	        }
	    	        
	    	        if (status)
	    	        {
	                    String nodeId = ((Map<String,ActionProcessTask>)ProcessRequest.getModelStates(processid)
	                    				 .get(ProcessRequest.EXECID_TASK)).get(completedExecuteid).getId();
	                    
	                    // check if dependency is same as the taskCompleted executeid
	    	            if (dependency.getDependExecuteid().equals(nodeId))
	    	            {
	    		            // ASSESS type is always evaluated first
	    	                Log.log.info(String.format("Dependency type: ASSESS on actionName: %s, executeid: %s", actionName, completedExecuteid));
	    		            
	    		            /*
	    		             * assessment status evaluation
	    		             */
	    	                
	    	                // check completion status
	    	                if (dependency.requiresCompletion())
	    	                {
	    	                    if (Log.log.isDebugEnabled()) {
	    	                        Log.log.debug("  requires completion - true: "+completed);
	    	                    }
	    	                    if (completed == false)
	    	                    {
	    	                        status = false;
	    	                    }
	    	                }
	    	                    
	    	                // check condition status
	    	                if (status && dependency.hasCondition())
	    	                {
	    	                    if (Log.log.isDebugEnabled()) {
	    	                        Log.log.debug("  requires condition - "+dependency.getCondition()+": "+condition);
	    	                    }
	    	                    if (StringUtils.isEmpty(condition) || !dependency.getCondition().equalsIgnoreCase(condition))
	    	                    {
	    	                        status = false;
	    	                    }
	    	                }
	    	                    
	    	                // check severity status
	    	                if (status && dependency.hasSeverity())
	    	                {
	    	                    if (Log.log.isDebugEnabled()) {
	    	                        Log.log.debug("  requires severity - "+dependency.getSeverity()+": "+severity);
	    	                    }
	    	                    if (StringUtils.isEmpty(severity))
	    	                    {
	    	                        status = false;
	    	                    }
	    	                        
	    	                    // equals
	    	                    else if (dependency.getSeverity().indexOf('_') == -1)
	    	                    {
	    	                        if (!dependency.getSeverity().equalsIgnoreCase(severity))
	    	                        {
	    		                        status = false;
	    	                        }
	    	                    }
	    	                        
	    	                    // comparison
	    	                    else
	    	                    {
	    	                        int requiredValue = getSeverityValue(dependency.getSeverity().substring(3));
	    	                        int actualValue = getSeverityValue(severity);
	    	                        if (dependency.getSeverity().startsWith("LE_"))
	    	                        {
	    	                            if (actualValue > requiredValue)
	    	                            {
	    	                                status = false;
	    	                            }
	    	                        }
	    	                        else if (dependency.getSeverity().startsWith("LT_"))
	    	                        {
	    	                            if (actualValue >= requiredValue)
	    	                            {
	    	                                status = false;
	    	                            }
	    	                        }
	    	                        else if (dependency.getSeverity().startsWith("GE_"))
	    	                        {
	    	                            if (actualValue < requiredValue)
	    	                            {
	    	                                status = false;
	    	                            }
	    	                        }
	    	                        else if (dependency.getSeverity().startsWith("GT_"))
	    	                        {
	    	                            if (actualValue <= requiredValue)
	    	                            {
	    	                                status = false;
	    	                            }
	    	                        }
	    	                    }
	    	                }
	    	                
	    		            // check if dependency is same as the taskCompleted executeid
	    		            if (status && type.equalsIgnoreCase("EXPRESSION"))
	    		            {
	    		                Log.log.info(String.format("Dependency type: EXPRESSION on actionName: %s, executeid: %s", 
	    		                						   actionName, completedExecuteid));
	    		                String expression = dependency.getExpression();
	    			            
	    		                if (expression != null)
	    		                {
	                                boolean scriptCacheable = expression.equals(dependency.getExpression());
	                                
	                                String expressionStr =  String.format("expression: (%s)", expression);
	                                
	                                if (Log.log.isDebugEnabled()) {
	                                    Log.log.debug(expressionStr);
	                                }
	    		
	    		                    // execute expression
	    		                    try
	    		                    {
	    				                Binding binding = new Binding();
	    						        binding.setVariable(Constants.GROOVY_BINDING_PARAMS, runtimeParams);
	    						        binding.setVariable(Constants.GROOVY_BINDING_FLOWS, flows);
	    						        binding.setVariable(Constants.GROOVY_BINDING_OUTPUTS, outputs);
	                                    
	    						        WSDataMap wsData = null;
	    						        
	                                    //add WSDATA
	                                    if (expression.contains(Constants.GROOVY_BINDING_WSDATA))
	                                    {
	                                        wsData = ServiceWorksheet.getWorksheetWSDATA(problemid);
	        						        binding.setVariable(Constants.GROOVY_BINDING_WSDATA, wsData);
	                                    }
	    						        
	                                    ProcessState ps = ProcessExecution.getProcessState(processid);
	                                    
	                                    boolean valid = true;
	                                    String errMsg = null;
	                                    
	                                    /* RBA-13922
	                                     * 
	                                     * Disable use of identified variable names from 
	                                     * FLOWS, OUTPUTS, PARAMS, and WSDATA in precondition
	                                     * expressions for pre-check before evaluation of expression. 
	                                     */ 
	                                    Map<String, Set<String>> expressionvarsMap = Collections.EMPTY_MAP;
	                                    
	                                    if (!expressionvarsMap.isEmpty())
	                                    {
	                                        // Validate expression variables for null value, fails on first variable found null
	                                        
	                                        for (String key : expressionvarsMap.keySet())
	                                        {
	                                            if (!valid)
	                                            {
	                                                break;
	                                            }
	                                            
	                                            Map<String, ? extends Object> mapToChk = null;
	                                            WSDataMap wsDataMapToChk = null;
	                                            
	                                            switch(key)
	                                            {
	                                                case Constants.GROOVY_BINDING_PARAMS:
	                                                    mapToChk = runtimeParams;
	                                                    break;
	                                                    
	                                                case Constants.GROOVY_BINDING_FLOWS:
	                                                    mapToChk = flows;
	                                                    break;
	                                                    
	                                                case Constants.GROOVY_BINDING_OUTPUTS:
	                                                    mapToChk = outputs;
	                                                    break;
	                                                    
	                                                case Constants.GROOVY_BINDING_WSDATA:
	                                                    wsDataMapToChk = wsData;
	                                                    break;
	                                            }
	                                            
	                                            Set<String> vars = expressionvarsMap.get(key);
	                                            
	                                            for (String varName : vars)
	                                            {
	                                                if (mapToChk != null)
	                                                {
	                                                    if (!mapToChk.containsKey(varName))
	                                                    {
	                                                        errMsg = varName + " in " + key + " used in expression " + expression + " is not initialized.";
	                                                        Log.log.warn(errMsg);
	                                                        valid = false;
	                                                        break;
	                                                    }
	                                                }
	                                                else if (wsDataMapToChk != null)
	                                                {
	                                                    if (wsDataMapToChk.get(varName) == null)
	                                                    {
	                                                        errMsg = varName + " in " + key + " used in expression " + expression + " is not initialized.";
	                                                        Log.log.warn(errMsg);
	                                                        valid = false;
	                                                        break;
	                                                    }
	                                                }
	                                            }
	                                        }
	                                    }
	                                    
	                                    if (!valid)
	                                    {
	                                        if (ps.getCheckPreConditionNullVars())
	                                        {
	                                            throw new Exception(errMsg);
	                                        }
	                                        else
	                                        {
	                                            ps.setPreconditionError(dependentNodeId, errMsg);
	                                        }
	                                    }
	                                    
	    						        String script = "return (Boolean)("+expression+");";
	    						        Boolean value = (Boolean)GroovyScript.execute(script, actionName+"Precondition", scriptCacheable, binding);
	    						        
	    						        Log.log.info(String.format("(Boolean)(%s) returned %s", expression, 
	    						        						   (value != null ? value.toString() : "null")));
	    						            						        
	    			                    if (value != null)
	    			                    {
	    			                        // expression && assess
	    			                        status = value.booleanValue() && status;
	    			                    }
	    			                    else
	    			                    {
	    			                    	String errMsg2 = String.format("(Boolean)(%s) returned null", expression);
	    			                    	Log.log.error(errMsg2);
	    			                        throw new Exception(errMsg2);
	    			                    }
	    			                    
	    						        Log.log.info(String.format("expressionStr [%s] %s && status: %s = %b", expressionStr, 
	    						        						   (value != null ? value.toString() : "null"), status, 
	    						        						   (value != null ? value.booleanValue() && status : status)));
	    		                    }
	    		                    catch (Throwable t)
	    		                    {
	    		                        String errMsg = "Expression Evaluation Error : " + t.getMessage();
	    		                        Log.log.warn(errMsg, t);
	    		                        throw new Exception(errMsg, t);
	    		                    }
	    		                }
	    			            else
	    			            {
	    			                Log.log.warn("Missing dependency expression");
	    			            }
	    			        }
	
	    	                // set status state
	    	                dependency.setStatus(processid, type, target, status);
	    	                if (Log.log.isDebugEnabled()) {
	    	                    Log.log.debug(String.format("SETTING - status: %b target: %s dependExecuteid: %s", 
	    	                                    dependency.getStatus(processid, type, target), target, 
	    	                                    dependency.getDependExecuteid()));
	                        }
	    		            
	    		            // MERGE ANY - result set to current status evaluation only
	    		            if (isMergeAny)
	    		            {
	    		                if (Log.log.isDebugEnabled()) {
	    		                    Log.log.debug(String.format("isMergeAny: %b", isMergeAny));
	    	                    }
	    		                Log.log.info(String.format("result B4 = %b", result));
	    		                Log.log.info(String.format("status = %b", status));
	    		                
	    		                if (!result)
	    		                {
	    		                    result = status;
	    		                }
	    		                else
	    		                {
	    		                    if (status)
	    		                    {
	    		                        result = status;
	    		                    }
	    		                }
	    		                
	    				        Log.log.info(String.format("result After = %b", result));
	    				        
	    				        if (result)
	    				        {
	    				            if (Log.log.isDebugEnabled()) {
	    				                Log.log.debug(String.format("isMergeAny: %b, result = %b breaking...", isMergeAny, result));
	    		                    }
	    				            break;
	    				        }
	    		            }
	    		            
	    		            if (oldMergeTargets)
	    		            {
	    		                String name = (String)flows.get(Constants.MERGE_TARGETS);
	    		                if (name != null)
	    		                {
	    		                    if (status)
	    		                    {
	    		                        AtomicLong latch = ProcessContext.getAtomicNumber(name);
	    		                        if (latch.decrementAndGet() == 0)
	    		                        {
	    		                        	Log.log.info("Merge Complete, Removing Target Information");
	    		                        	ProcessContext.removeAtomicNumber(name);
	    		                        	flows.remove(Constants.MERGE_TARGETS);
	    		                        	isMergeTargets = false;
	    		                        }
	    		                    }
	    		                }
	    		            }
	    	            }
	    	        }
	            } // for loop

        	if (Log.log.isDebugEnabled()) {
        	    Log.log.debug(String.format("isMergeAll: %b", (!isMergeAny && !isMergeTargets)));
            }
        	
            // init result based on isMergeAny
            if (!isMergeAny && !isMergeTargets)
            {
                result = true;
            
    	        // update result (logical AND of all iterated status)
    	        
    	        for (ActionProcessTaskDependency dependency: taskDeps )
    	        {
    	            // check if dependency is same as the taskCompleted executeid
    	            String completedNodeId = ((Map<String,ActionProcessTask>)ProcessRequest.getModelStates(processid).get(ProcessRequest.EXECID_TASK)).get(completedExecuteid).getId();
    	            String dependencyExecuteId = dependency.getDependExecuteid();
    	            
    	            boolean status = dependency.getStatus(processid, dependency.getType(), target);
    	            if (Log.log.isDebugEnabled()) {
    	                Log.log.debug(String.format("STATE - status: %b target: %s dependExecuteid: %s", status, target, 
    											dependency.getDependExecuteid()));
    	            }
    				
    				if (!status && (!completedNodeId.equals(dependencyExecuteId) && dependencyExecuteId.equals(dependentNodeId)))
    				{
    				    if (Log.log.isDebugEnabled()) {
    				        Log.log.debug(String.format("Skipping loopback status check. Completed Node Id: [%s], " +
    				                        "Dependency Execute Id: [%s], Dependent Node Id: [%s]", completedNodeId, 
    				                        dependencyExecuteId, dependentNodeId));
                        }
    				}
    				else
    				{
    				    Log.log.info(String.format("result = result(%b) && status(%b)", result, status));
    				    result = result && status;
    				}
    	        }
            }
                
            Log.log.info(String.format("evaluate result: %b", result));
        }
        else
        {
            Log.log.warn("Skipping evaluate - Process has already completed.");
        }
        
        ExecutionTimeUtil.log((String) runtimeParams.get("RESOLVE.JOB_ID"), null, null, ExecutionStep.METHOD_EXECUTION, ExecutionStepPhase.END);
        
        
        return result;
    } // evaluate
    
    public Targets getTargets(Map params)
    {
        Targets result = null;
        
        String targetsStr = (String)ProcessContext.get(process, Constants.PROCESS_MAP_EXECUTE_TARGETS+"/"+execute);
        if (targetsStr != null)
        {
            result = (Targets)StringUtils.stringToObj(targetsStr);
        }
        else
        {
            result = ProcessExecution.initTargets(params);
        }
        
        return result;
    } // getTargets
    
    public void setTargets(Targets targets) throws Exception
    {
//        this.targets = targets;
        String targetsStr = StringUtils.objToString(targets);
        
        try
        {
	        ProcessContext.put(process, Constants.PROCESS_MAP_EXECUTE_TARGETS+"/"+execute, targetsStr);
        }
        catch (NullPointerException e)
        {
            throw new Exception("Runbook may have completed or aborted");
        }
        
        initRequested(targets);
    } // setTargets
    
    int getSeverityValue(String severity)
    {
        int value = 0;
        
        if (severity != null)
        {
	        if (severity.equalsIgnoreCase(Constants.ASSESS_SEVERITY_CRITICAL))
	        {
	            value = 4;
	        }
	        else if (severity.equalsIgnoreCase(Constants.ASSESS_SEVERITY_SEVERE))
	        {
	            value = 3;
	        }
	        else if (severity.equalsIgnoreCase(Constants.ASSESS_SEVERITY_WARNING))
	        {
	            value = 2;
	        }
	        else if (severity.equalsIgnoreCase(Constants.ASSESS_SEVERITY_GOOD))
	        {
	            value = 1;
	        }
	        else if (severity.equalsIgnoreCase(Constants.ASSESS_SEVERITY_UNKNOWN))
	        {
	            value = 0;
	        }
        }
        
        return value;
    } // getSeverityValue
    
    public boolean isRequested(String address)
    {
        boolean result = false;
      
      synchronized (this)
      {
        if (requested == null)
        {
        	requested = new ConcurrentHashMap();
        }
        
        Object obj = requested.get(address);
        if (obj == null)
        {
            result = false;
        }
        else
        {
            result = ((Boolean)obj).booleanValue();
        }
      }
    
        return result;
    } // isRequested
    
    public void setRequested(String address)
    {
        synchronized (this)
        {
	        if (requested == null)
	        {
	            requested = new ConcurrentHashMap<String, Object>();
	        }
	        
	        requested.put(address, new Boolean(true));
            ProcessExecution.getProcessState(process).setRequested(execute, requested);
        }
    } // setRequested

    public void setRequestedMap(ConcurrentMap<String, Object> map)
    {
    	requested = map;
    } // setRequested

    public void initRequested(Targets targets)
    {
      synchronized (this)
      {
          if (requested == null)
          {
			requested = new ConcurrentHashMap();
		}

        for (Iterator i=targets.keySet().iterator(); i.hasNext(); )
        {
            String address = (String)i.next();
            
            requested.put(address, new Boolean(false));
        }
      }
    } // initRequested
    
    public String getExecute()
    {
        return execute;
    }
    public void setExecute(String execute)
    {
        this.execute = execute;
    }
    public String getProblem()
    {
        return problem;
    }
    public void setProblem(String problem)
    {
        this.problem = problem;
    }
    public String getEvent()
    {
        return event;
    }
    public void setEvent(String event)
    {
        this.event = event;
    }
    public String getProcess()
    {
        return process;
    }
    public void setProcess(String process)
    {
        this.process = process;
    }
    public String getTask()
    {
        return task;
    }
    public void setTask(String task)
    {
        this.task = task;
    }
    public String getUserid()
    {
        return userid;
    }
    public void setUserid(String userid)
    {
        this.userid = userid;
    }
    public List getDepends()
    {
        return depends;
    }
    public void setDepends(List depends)
    {
        this.depends = depends;
    }
    public String getNumber()
    {
        return number;
    }
    public void setNumber(String number)
    {
        this.number = number;
    }
    public String getReference()
    {
        return reference;
    }
    public void setReference(String reference)
    {
        this.reference = reference;
    }
    public Map getParams()
    {
        return new HashMap(this.params);
    }
    public void setParams(Map params)
    {
        if (params != null)
        {
	        this.params.putAll(params);
        }
    }
    public String getTaskname()
    {
        return taskname;
    }
    public void setTaskname(String taskname)
    {
        this.taskname = taskname;
    }

    public String getTasknamespace()
    {
        return tasknamespace;
    }
    public void setTasknamespace(String tasknamespace)
    {
        this.tasknamespace = tasknamespace;
    }

    public String getTaskhidden()
    {
        return taskhidden;
    }

    public void setTaskhidden(String taskhidden)
    {
        this.taskhidden = taskhidden;
    }

    public String getTasktags()
    {
        return tasktags;
    }

    public void setTasktags(String tasktags)
    {
        this.tasktags = tasktags;
    }

    public String getTasksummary()
    {
        return tasksummary;
    }
    public void setTasksummary(String tasksummary)
    {
        this.tasksummary = tasksummary;
    }

    public String getModelType()
    {
        return modelType;
    }

    public void setModelType(String modelType)
    {
        this.modelType = modelType;
    }
    
    public void setMergedTargets(boolean merged)
    {
        this.isMergedTargets = merged;
    } // setMergedTargets
    
    public boolean isMergedTargets()
    {
        return this.isMergedTargets;
    } // isMergedTargets
    
    public String toString()
    {
        return this.taskname;
    } // toString
    
    public String getTaskLabel()
    {
        String taskLabel = "";
        
        if (StringUtils.isNotBlank(getExecute()))
        {
            String execId = getExecute();
            
            if (execId.lastIndexOf(Constants.EXECUTE_REQUEST_NODEID_DELIMITER) > 0)
            {
                taskLabel = execId.substring(execId.lastIndexOf(Constants.EXECUTE_REQUEST_NODEID_DELIMITER) + 
                                             Constants.EXECUTE_REQUEST_NODEID_DELIMITER.length());
            }
        }
        
        return taskLabel;
    }
    
    public static String getTaskLabel(String executeId)
    {
        String taskLabel = "";
        
        if (StringUtils.isNotBlank(executeId))
        {
            String execId = executeId;
            
            if (execId.lastIndexOf(Constants.EXECUTE_REQUEST_NODEID_DELIMITER) > 0)
            {
                taskLabel = execId.substring(execId.lastIndexOf(Constants.EXECUTE_REQUEST_NODEID_DELIMITER) + 
                                             Constants.EXECUTE_REQUEST_NODEID_DELIMITER.length());
            }
        }
        
        return taskLabel;
    }
} // ActionProcessTaskRequest
