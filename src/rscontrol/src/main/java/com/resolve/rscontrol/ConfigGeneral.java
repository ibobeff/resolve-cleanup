/******************************************************************************
* (C) Copyright 2014
*
* Resolve Systems, LLC
*
* All rights reserved
* This software is distributed under license and may not be used, copied,
* modified, or distributed without the express written permission of
* Resolve Systems, LLC.
*
******************************************************************************/

package com.resolve.rscontrol;

import com.resolve.util.XDoc;

public class ConfigGeneral extends com.resolve.rsbase.ConfigGeneral
{
    private static final long serialVersionUID = -8891912766875088296L;
	public String executorBusyRatio = "0.95";
    public String dbpoolBusyRatio = "0.95";
    public String sysExecutorBusyRatio = "0.95";
    public String startExecutorBusyRatio = "0.95"; 
    public int maxRunningRBs = 400;
    public int minRunningRBs = 1;
    public boolean useActiveRBCount = false; 
    public boolean isCollectingRunbookStatistics;
    public int scheduledPool;
    public boolean enableRunbookStatCounters = true;
    public boolean updateSir = true;
    
    static int waitingTaskThreshold = 50;
    
    public ConfigGeneral(XDoc config) throws Exception
    {
        super(config);
        
        define("executorBusyRatio", STRING, "./GENERAL/@EXECUTORBUSYRATIO");
        define("dbpoolBusyRatio", STRING, "./GENERAL/@DBPOOLBUSYRATIO");
        define("sysExecutorBusyRatio", STRING, "./GENERAL/@SYSEXECUTORBUSYRATIO");
        define("startExecutorBusyRatio", STRING, "./GENERAL/@STARTEXECUTORBUSYRATIO");
        define("waitingTaskThreshold", INTEGER, "./GENERAL/@WAITINGTASKTHRESHOLD");
        define("maxRunningRBs", INTEGER, "./GENERAL/@MAXRUNBOOKLIMIT");
        define("minRunningRBs", INTEGER, "./GENERAL/@MINRUNBOOKLIMIT");
        define("useActiveRBCount", BOOLEAN, "./GENERAL/@USERUNBOOKCOUNT");
        define("isCollectingRunbookStatistics", BOOLEAN, "./GENERAL/@RUNBOOKMETRICS");
        define("scheduledPool", INTEGER, "./GENERAL/@SCHEDULEDPOOL");
        define("enableRunbookStatCounters", BOOLEAN, "./GENERAL/@ENABLERUNBOOKSTATCOUNTERS");
        define("updateSir", BOOLEAN, "./GENERAL/@UPDATESIR");
        
    } // ConfigGeneral
    
    public void load()
    {
        super.load();
    } // load

    public void save()
    {
        super.save();
    } // save

    public String getExecutorBusyRatio()
    {
        return executorBusyRatio;
    } //getExecutorBusyRatio

    public void setExecutorBusyRatio(String executorBusyRatio)
    {
        this.executorBusyRatio = executorBusyRatio;
    } //setExecutorBusyRatio

    public String getDbpoolBusyRatio()
    {
        return dbpoolBusyRatio;
    } //getDbpoolBusyRatio

    public void setDbpoolBusyRatio(String dbpoolBusyRatio)
    {
        this.dbpoolBusyRatio = dbpoolBusyRatio;
    } //setDbpoolBusyRatio

    public String getSysExecutorBusyRatio()
    {
        return sysExecutorBusyRatio;
    } //getSysExecutorBusyRatio

    public void setSysExecutorBusyRatio(String sysExecutorBusyRatio)
    {
        this.sysExecutorBusyRatio = sysExecutorBusyRatio;
    } //setSysExecutorBusyRatio

    public String getStartExecutorBusyRatio()
    {
        return startExecutorBusyRatio;
    } //getStartExecutorBusyRatio

    public void setStartExecutorBusyRatio(String startExecutorBusyRatio)
    {
        this.startExecutorBusyRatio = startExecutorBusyRatio;
    } //setStartExecutorBusyRatio

    public static int getWaitingTaskThreshold()
    {
        return waitingTaskThreshold;
    } // getWaitingTaskThreshold

    public static void setWaitingTaskThreshold(int waitingTaskThreshold)
    {
        ConfigGeneral.waitingTaskThreshold = waitingTaskThreshold;
    } // setWaitingTaskThreshold

    public int getMaxRunningRBs()
    {
        return maxRunningRBs;
    } //getMaxRunningRBs

    public void setMaxRunningRBs(int m)
    {
        this.maxRunningRBs = m;
    } //setMaxRunningRBs
    
    public int getMinRunningRBs()
    {
        return minRunningRBs;
    } //getMinRunningRBs

    public void setMinRunningRBs(int m)
    {
        this.minRunningRBs = m;
    } //setMinRunningRBs
    
    public boolean isUseActiveRBCount()
    {
        return useActiveRBCount;
    } //isUseActiveRBCount

    public void setUseActiveRBCount(boolean b)
    {
        this.useActiveRBCount = b;
    } //setUseActiveRBCount

	public boolean getIsCollectingRunbookStatistics() {
		return isCollectingRunbookStatistics;
	}

	public void setIsCollectingRunbookStatistics(boolean isCollectingRunbookStatistics) {
		this.isCollectingRunbookStatistics = isCollectingRunbookStatistics;
	}

    public int getScheduledPool() {
		return scheduledPool;
	}

	public void setScheduledPool(int scheduledPool) {
		this.scheduledPool = scheduledPool;
	}

	public boolean getEnableRunbookStatCounters() {
		return enableRunbookStatCounters;
	}

	public void setEnableRunbookStatCounters(boolean enableRunbookStatCounters) {
		this.enableRunbookStatCounters = enableRunbookStatCounters;
	}

	public boolean getUpdateSir() {
		return updateSir;
	}

	public void setUpdateSir(boolean updateSir) {
		this.updateSir = updateSir;
	}
	
} // ConfigGeneral