/******************************************************************************
* (C) Copyright 2014
*
* Resolve Systems, LLC
*
* All rights reserved
* This software is distributed under license and may not be used, copied,
* modified, or distributed without the express written permission of
* Resolve Systems, LLC.
*
******************************************************************************/

package com.resolve.rscontrol;

import com.resolve.util.XDoc;

public class ConfigRegistration extends com.resolve.rsbase.ConfigRegistration
{
    private static final long serialVersionUID = 8840674646593832397L;
	int election = 30;
    boolean logHeartbeat = true;
    
    public ConfigRegistration(XDoc config) throws Exception
    {
        super(config);
        
        define("election", INTEGER, "./REGISTRATION/@ELECTION");
        define("logHeartbeat", BOOLEAN, "./REGISTRATION/@LOGHEARTBEAT");
    } // ConfigRegistration
    
    public void load()
    {
        loadAttributes();
    } // load

    public void save()
    {
        saveAttributes();
    } // save

    public int getElection()
    {
        return election;
    }

    public void setElection(int election)
    {
        this.election = election;
    }

    public boolean isLogHeartbeat()
    {
        return logHeartbeat;
    }

    public void setLogHeartbeat(boolean logHeartbeat)
    {
        this.logHeartbeat = logHeartbeat;
    }
    
} // ConfigRegistration
