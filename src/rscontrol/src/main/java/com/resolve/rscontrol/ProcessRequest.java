/******************************************************************************
* (C) Copyright 2014
*
* Resolve Systems, LLC
*
* All rights reserved
* This software is distributed under license and may not be used, copied,
* modified, or distributed without the express written permission of
* Resolve Systems, LLC.
*
******************************************************************************/

package com.resolve.rscontrol;

import java.io.ByteArrayInputStream;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.locks.ReadWriteLock;
import java.util.concurrent.locks.ReentrantLock;
import java.util.concurrent.locks.ReentrantReadWriteLock;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.stream.Collectors;
import org.apache.commons.lang.StringEscapeUtils;
import org.apache.commons.lang3.tuple.Pair;
import org.dom4j.Document;
import org.dom4j.Node;
import org.dom4j.io.SAXReader;
import com.resolve.esb.amqp.MAmqpRunbookExecutionListener;
import com.resolve.esb.amqp.MAmqpServer;
import com.resolve.persistence.model.WikiDocument;
import com.resolve.persistence.util.DBCacheRegionConstants;
import com.resolve.persistence.util.HibernateUtil;
import com.resolve.search.SearchUtils;
import com.resolve.search.model.ExecuteState;
import com.resolve.search.worksheet.WorksheetSearchAPI;
import com.resolve.services.ServiceHibernate;
import com.resolve.services.ServiceWorksheet;
import com.resolve.services.WSDataMap;
import com.resolve.services.hibernate.util.WikiUtils;
import com.resolve.services.hibernate.vo.ResolveActionParameterVO;
import com.resolve.services.hibernate.vo.ResolveActionTaskVO;
import com.resolve.services.hibernate.vo.WikiDocumentVO;
import com.resolve.services.util.JsonUtils;
import com.resolve.services.util.PropertiesUtil;
import com.resolve.services.util.SequenceUtils;
import com.resolve.services.util.UserUtils;
import com.resolve.services.util.VariableUtil;
import com.resolve.services.vo.QueryDTO;
import com.resolve.services.vo.QueryFilter;
import com.resolve.util.Constants;
import com.resolve.util.DirectedTree;
import com.resolve.util.DirectedTreeNode;
import com.resolve.util.EdgeNode;
import com.resolve.util.LockUtils;
import com.resolve.util.Log;
import com.resolve.util.StringUtils;
import com.resolve.util.performance.ExecutionTimeUtil;
import com.resolve.util.performance.datasources.ExecutionStep;
import com.resolve.util.performance.datasources.ExecutionStepPhase;
import net.sf.json.JSONArray;
import net.sf.json.JSONObject;

public class ProcessRequest
{
    final static Pattern VAR_REGEX_PROPERTY = Pattern.compile(Constants.VAR_REGEX_PROPERTY);
    final static Pattern VAR_REGEX_PARAM = Pattern.compile(Constants.VAR_REGEX_PARAM);

    static HashMap<String, ActionProcessDependency> cacheActionProcessDependency = new HashMap<String, ActionProcessDependency>();
    static HashMap<String, ActionProcessDependency> cacheActionAbortDependency = new HashMap<String, ActionProcessDependency>();
    static HashMap<String, Integer> wikiChecksum = new HashMap<String, Integer>();
    static ReadWriteLock wikiChecksumLock = new ReentrantReadWriteLock(true);

    static ConcurrentHashMap<String, ActionProcessDependency> runningModels = new ConcurrentHashMap<String, ActionProcessDependency>();
    static ConcurrentHashMap<String, Map<String, Object>> modelStates = new ConcurrentHashMap<String, Map<String, Object>>();
    static String EXECID_TASK = "EXECID_TASK";
    static String TASK_EXECID = "TASK_EXECID";
    static String EXECID_TASKREQ = "EXECID_TASKREQ";
    static String EVENTID_EXECID = "EVENTID_EXECID";
    static String PROCESSID_DELIMITER = "+";
    
    // Wiki => Long Process Timeout
    public static HashMap<String, Long> cacheProcessTimeout = new HashMap<String, Long>();

    public static ConcurrentHashMap<String, Collection<ResolveActionParameterVO>> parameterCache = new ConcurrentHashMap<String, Collection<ResolveActionParameterVO>>();

    static Map<String, Object> getModelStates(String processid) throws RuntimeException
    {
        Map<String, Object> result = modelStates.get(processid);
        if (result == null)
        {
            throw new RuntimeException(String.format("Missing modelStates for processid: %s. Process may have completed", 
            										 processid));
        }
        return result;
    }

    static Map<String, Object> createModelStates(String processid)
    {
        if (!modelStates.containsKey(processid))
        {
            modelStates.putIfAbsent(processid, new HashMap<String, Object>());
        }
        return modelStates.get(processid);
    }

    //    static Map<String, Object> createModelStates(String processid, Map<String, Object> map)
    //    {
    //        if (!modelStates.containsKey(processid))
    //        {
    //            modelStates.putIfAbsent(processid, map);
    //        }
    //        return modelStates.get(processid);
    //    }

    public static String initProcessModel(String wiki, String processid) throws Exception
    {
        String result = processid;

        if (Log.log.isDebugEnabled()) {
            Log.log.debug("initProcessModel: " + wiki);
        }

        try
        {
            // get document process and abort model
            String processXML = null;
            String abortXML = null;

            // ABORT
            if (wiki.startsWith(Constants.EXECUTE_ABORTWIKI))
            {
                // update wikidocname
                wiki = wiki.substring(Constants.EXECUTE_ABORTWIKI.length());

                if (isWikiDocumentExecutable(wiki))
                {
                    if (!cacheActionAbortDependency.containsKey(wiki))
                    {
                        abortXML = getWikiDocumentModelXML(wiki, Constants.MODEL_TYPE_ABORT);
                    }

                    // generate dependency tree
                    ActionProcessDependency abortDependency = getActionAbortDependency(wiki, abortXML, Constants.MODEL_TYPE_ABORT);

                    if (abortDependency != null)
                    {
                        if (processid != null)
                        {
                            runningModels.put(processid, abortDependency);
                        }
                    }
                }
                else
                {
                    Log.log.warn("Runbook not active - wiki: " + wiki);
                }
            }

            // MAIN
            else
            {
                if (isWikiDocumentExecutable(wiki))
                {
                    if (Log.log.isDebugEnabled()) {
                        Log.log.debug("getWikiDocumentModelXML");
                    }
                    if (!cacheActionProcessDependency.containsKey(wiki))
                    {
                        processXML = getWikiDocumentModelXML(wiki, Constants.MODEL_TYPE_MAIN);
                    }

                    // generate main dependency tree
                    if (Log.log.isDebugEnabled()) {
                        Log.log.debug("getActionProcessDependency");
                    }
                    ActionProcessDependency processDependency = getActionProcessDependency(wiki, processXML, Constants.MODEL_TYPE_MAIN);

                    if (processDependency != null)
                    {
                        if (Log.log.isDebugEnabled()) {
                        // create request records
                            Log.log.debug("createExecuteProcessRequest");
                        }
                        if (processid != null)
                        {
                            runningModels.put(processid, processDependency);
                        }
                    }
                }
                else
                {
                    Log.log.warn("Runbook not active - wiki: " + wiki);
                }
            }
        }
        catch (Exception e)
        {
            Log.log.warn("FAIL: initProcessModel (" + wiki + ") - " + e.getMessage(), e);
            throw e;
        }

        return result;
    } // initProcessModel

    @SuppressWarnings({ "rawtypes", "unchecked" })
    public static void augmentDependency(ActionProcessDependency model)
    {
        for (ActionProcessTask currentTask : model.tasks)
        {
            model.id2task.put(currentTask.id, currentTask);

            List<DirectedTreeNode> parents = (List<DirectedTreeNode>) currentTask.graphNode.getAllParents();

            if (parents != null && parents.size() > 0)
            {
                for (DirectedTreeNode idx : parents)
                {
                    ActionProcessTask parentTask = (ActionProcessTask) idx.getValue();

                    /*
                     * NOTE: PROCESS_TYPE_MODEL - Get and append the post conditions
                     * from parent task to child task condition. get and set
                     * the "expression" value from the parent task for the child
                     */
                    String[] conditions = null;
                    String expression = null;
                    String taskCondition = null;

                    // set MERGE condition
                    if (currentTask != null && currentTask.getCondition() != null)
                    {
                        taskCondition = currentTask.getCondition();
                    }

                    if (parentTask != null && parentTask.getPostConditions() != null && parentTask.getPostConditions().containsKey(currentTask.getId()))
                    {
                        Map postConditions = parentTask.getPostConditions();
                        
                        String rootPreConditionId = null;
                        
                        for (String postCondition : (List<String>)postConditions.get(currentTask.getId()))
                        {
                            conditions = null;
                            expression = null;
                            
                            if (!postCondition.toLowerCase().startsWith("condition") &&
                                !postCondition.toLowerCase().startsWith("merge"))
                            {
                                rootPreConditionId = StringUtils.substringBefore(postCondition, ",");
                            }
                            
                            if (taskCondition == null)
                            {
                                taskCondition = postCondition;
                            }
                            else
                            {
                                taskCondition = taskCondition + "," + postCondition;
                            }
                            
                            // set conditions
                            if (!StringUtils.isEmpty(taskCondition))
                            {
                                conditions = taskCondition.split(",");
                            }
                            
                            // set expression for current task
                            
                            if (StringUtils.isNotBlank(rootPreConditionId) && parentTask != null && parentTask.getPostExpressions() != null)
                            {
                                Map<String, Map<String, String>> postExpressions = parentTask.getPostExpressions();
                                Map<String, String> postExpressionsForCurrentTask = postExpressions.get(currentTask.getId());
                                
                                if (postExpressionsForCurrentTask != null && postExpressionsForCurrentTask.containsKey(rootPreConditionId))
                                {
                                    expression = "(" + postExpressionsForCurrentTask.get(rootPreConditionId) + ")";
                                }
                            }
                            
                            // init conditionals
                            boolean completion = true;
                            String condition = null;
                            String severity = null;
                            String merge = Constants.ASSESS_MERGE_ALL;
                            
                            if (conditions != null && conditions.length > 0)
                            {
                                for (int condIdx = 0; condIdx < conditions.length; condIdx++)
                                {
                                    String def = conditions[condIdx].trim();
                                    if (def.startsWith("completion") || def.startsWith("COMPLETION") || def.startsWith("completed") || def.startsWith("COMPLETED"))
                                    {
                                        completion = getDependencyCompletion(def);
                                    }
                                    else if (def.startsWith("condition") || def.startsWith("CONDITION"))
                                    {
                                        condition = getDependencyCondition(def);
                                    }
                                    else if (def.startsWith("severity") || def.startsWith("SEVERITY"))
                                    {
                                        severity = getDependencySeverity(def);
                                    }
                                    else if (def.startsWith("merge") || def.startsWith("MERGE"))
                                    {
                                        if (!merge.equals(Constants.ASSESS_MERGE_ANY))
                                        {
                                            merge = getDependencyMerge(def);
                                        }
                                    }
                                }
                            }

                            // create dependency
                            ActionProcessTaskDependency dep = new ActionProcessTaskDependency();
                            dep.setsys_id(currentTask.getId() + "-" + parentTask.getId());
                            dep.setCompletion(completion);
                            dep.setCondition(condition);
                            dep.setSeverity(severity);
                            dep.setMerge(merge);

                            dep.setDependExecuteid(parentTask.getId());
                            dep.setDependActionName(parentTask.getName());

                            if (expression != null)
                            {
                                dep.setType("EXPRESSION");
                                dep.setExpression(expression);
                            }
                            else
                            {
                                dep.setType("ASSESS");
                            }

                            currentTask.getDepends().add(dep);
                        }
                    }
                }
            }
        }

    }

    public static String initProcessRequestInternal(String wiki, String userid, String problemid, String processid, boolean isEvent, Map params, ArrayList start, ActionProcessTaskRequest eventTask) throws Exception
    {
        ExecutionTimeUtil.log((String) params.get("RESOLVE.JOB_ID"), null, null, ExecutionStep.METHOD_EXECUTION, ExecutionStepPhase.START);
        
        String result = processid;
        // get document process and abort model
        String processXML = null;
        String abortXML = null;
        String problemNum = (String)params.get(Constants.EXECUTE_PROBLEM_NUMBER);


        // ABORT
        if (wiki.startsWith(Constants.EXECUTE_ABORTWIKI))
        {
            // update wiki docname
            wiki = wiki.substring(Constants.EXECUTE_ABORTWIKI.length());

            // set modelType
            params.put(Constants.MODEL_TYPE, Constants.MODEL_TYPE_ABORT);

            if (isWikiDocumentExecutable(wiki))
            {
                if (!cacheActionAbortDependency.containsKey(wiki))
                {
                    abortXML = getWikiDocumentModelXML(wiki, Constants.MODEL_TYPE_ABORT);
                }

                // generate dependency tree
                ActionProcessDependency abortDependency = getActionAbortDependency(wiki, abortXML, Constants.MODEL_TYPE_ABORT);

                if (abortDependency != null)
                {
                    // update process parameters
                    updateProcessParams(abortDependency, params);

                    // create request records
                    processid = createExecuteProcessRequestCAS(wiki, abortDependency, problemid, processid, params, userid, isEvent, start, eventTask);
                    if (processid != null)
                    {
                        // if (checkProcessPermission(processid, userid))
                        if (checkProcessPermission(wiki, abortDependency, userid, (String) params.get("RESOLVE.JOB_ID")))
                        {
                            result = processid;
                            runningModels.put(processid, abortDependency);
                        }
                        else
                        {
                            // send permission failed message
                            String summary = "FAILED: permission validation";
                            String detail = summary;
                            String activityId = params.containsKey(Constants.ACTIVITY_ID) ? (String)params.get(Constants.ACTIVITY_ID) : "";
                            ProcessExecution.insertAbortTaskResult(processid, wiki, summary, detail, problemid, null, userid, problemNum, activityId);
                            Map<String, String> abortParam = new HashMap<String, String>();
                            abortParam.put(Constants.EXECUTE_PROCESSID, processid);
                            abortParam.put(Constants.EXECUTE_PROBLEMID, problemid);
                            abortParam.put(Constants.EXECUTE_USERID, userid);
                            if(params.containsKey("RESOLVE.JOB_ID")) {
                            	abortParam.put("RESOLVE.JOB_ID", (String) params.get("RESOLVE.JOB_ID"));
                            }
                            MAction.abortProcess(abortParam);
                            throw new Exception("FAILED: permission validation");
                        }
                    }
                }
            }
            else
            {
                Log.log.warn("Runbook not active - wiki: " + wiki);
                MAmqpServer mServer = (MAmqpServer) Main.main.mServer;
                MAmqpRunbookExecutionListener.acknowledgeRunbookExecution(params, mServer);
            }
        }

        // MAIN
        else
        {
            if (isWikiDocumentExecutable(wiki))
            {
                // set modelType
                params.put(Constants.MODEL_TYPE, Constants.MODEL_TYPE_MAIN);

                if (Log.log.isDebugEnabled()) {
                    Log.log.debug("getWikiDocumentModelXML");
                }
                if (!cacheActionProcessDependency.containsKey(wiki))
                {
                    processXML = getWikiDocumentModelXML(wiki, Constants.MODEL_TYPE_MAIN);
                }

                // generate main dependency tree
                if (Log.log.isDebugEnabled()) {
                    Log.log.debug("getActionProcessDependency");
                }
                ActionProcessDependency processDependency = getActionProcessDependency(wiki, processXML, Constants.MODEL_TYPE_MAIN);

                if (processDependency != null)
                {
                    // update process parameters
                    if (Log.log.isDebugEnabled()) {
                        Log.log.debug("updateProcessParams");
                    }
                    updateProcessParams(processDependency, params);

                    // update WSDATA display
                    updateWorksheetDisplay(problemid, (String) params.get(Constants.PROCESS_DISPLAY));
                    
                    try
                    {
                        String param = "";
                        Object valueofObject = "";
                        Pattern paramPat = Pattern.compile(Constants.VAR_REGEX_PARAM);
                        Matcher paramMat;
                        for (Object key : params.keySet())
                        {
                            // The following can only work for String, "params" map might contain HashMap
                            Object paramObject = params.get(key);
                            if (paramObject instanceof String)
                            {
                                param = (String) paramObject;
                                paramMat = paramPat.matcher(param);
                                if (paramMat.find())
                                {
                                    valueofObject = VariableUtil.replaceVars(key.toString(), param, ((Map) new HashMap()), ((Map) new HashMap()), ((Map) new HashMap()), ((Map) new HashMap()), ((Map) new HashMap()));
                                    params.put(key, valueofObject);
                                }
                            }
                        }
                    }
                    catch (Exception e)
                    {
                        Log.log.error(e.getMessage(), e);
                    }
                    // create request records
                    if (Log.log.isDebugEnabled()) {
                        Log.log.debug("createExecuteProcessRequest");
                    }
                    processid = createExecuteProcessRequestCAS(wiki, processDependency, problemid, processid, params, userid, isEvent, start, eventTask);

                    if (processid != null)
                    {
                        // check permission
                        // if (checkProcessPermission(processid, userid))
                        if (checkProcessPermission(wiki, processDependency, userid, (String) params.get("RESOLVE.JOB_ID")))
                        {
                            result = processid;
                            runningModels.put(processid, processDependency);
                        }
                        else
                        {
                            // send permission failed message
                            String summary = "FAILED: permission validation";
                            String detail = summary;
                            String activityId = params.containsKey(Constants.ACTIVITY_ID) ? (String)params.get(Constants.ACTIVITY_ID) : "";
                            ProcessExecution.insertAbortTaskResult(processid, wiki, summary, detail, problemid, null, userid, problemNum, activityId);
                            Map<String, String> abortParam = new HashMap<String, String>();
                            abortParam.put(Constants.EXECUTE_PROCESSID, processid);
                            abortParam.put(Constants.EXECUTE_PROBLEMID, problemid);
                            abortParam.put(Constants.EXECUTE_USERID, userid);
                            if(params.containsKey("RESOLVE.JOB_ID")) {
                            	abortParam.put("RESOLVE.JOB_ID", (String) params.get("RESOLVE.JOB_ID"));
                            }
                            MAction.abortProcess(abortParam);
                            throw new Exception("FAILED: permission validation");
                        }
                    }
                }
            }
            else
            {
                Log.log.warn("Runbook not active - wiki: " + wiki);
                MAmqpServer mServer = (MAmqpServer) Main.main.mServer;
                MAmqpRunbookExecutionListener.acknowledgeRunbookExecution(params, mServer);
            }
        }

        ExecutionTimeUtil.log((String) params.get("RESOLVE.JOB_ID"), null, null, ExecutionStep.METHOD_EXECUTION, ExecutionStepPhase.END);
        
        return result;
    }

    public static String initProcessRequest(String wiki, String userid, String problemid, String processid, boolean isEvent, Map params, ArrayList start, ActionProcessTaskRequest eventTask)
    {
        ExecutionTimeUtil.log((String) params.get("RESOLVE.JOB_ID"), null, null, ExecutionStep.METHOD_EXECUTION, ExecutionStepPhase.START);

        String result = null;

        if (Log.log.isDebugEnabled()) {
            Log.log.debug("initProcessRequest: " + wiki + " user: " + userid);
        }

        try
        {
            if (isEvent && processid != null)
            {
                if (!ProcessExecution.isProcessCompleted(processid))
                {
                	result = processid;
                }
            }

            if (result == null) {
                result = initProcessRequestInternal(wiki, userid, problemid, processid, isEvent, params, start, eventTask);
            }
        }
        catch (Exception e)
        {
            Log.log.warn("FAIL: initProcessRequest (" + wiki + ") - " + e.getMessage(), e);
            MAmqpServer mServer = (MAmqpServer) Main.main.mServer;
            MAmqpRunbookExecutionListener.acknowledgeRunbookExecution(params, mServer);
        }

        ExecutionTimeUtil.log((String) params.get("RESOLVE.JOB_ID"), null, null, ExecutionStep.METHOD_EXECUTION, ExecutionStepPhase.END);
        return result;
    } // initProcessRequest


    static boolean isWikiDocumentExecutable(String wiki) throws Exception
    {
        boolean result = false;
        
        //we have found out that this call is too expensive (MySQL goes 50% during pick load)
        //boolean isExecutable = ServiceWiki.isWikiDocumentExecutable(wiki);
        
        //the solution is get the doc using this API so it could be from the local cache, hib cache
        //or only as a last resort go to the DB. Since the cache is invalidated when a doc is
        //changed this strategy will hold.
        WikiDocumentVO doc = findWikiDocumentByName(wiki);
        if (doc != null)
        {
            result = doc.ugetUIsActive() 
                            && !doc.ugetUIsLocked() //is not locked
                            && !doc.ugetSysIsDeleted() //is not marked deleted
                            && !doc.ugetUIsHidden() //is not hidden
                            ;
        }
        else
        {
            throw new Exception("Missing WIKI document: " + wiki);
        }
        
        if(!result)
        {
            if (Log.log.isDebugEnabled()) {
                Log.log.debug("Runbook " + wiki + " is either Locked/Hidden/Inactive/Mark Deleted. So will not be executed.");
            }
        }
        
        return result;
    } // isWikiDocumentExecutable

    static WikiDocumentVO findWikiDocumentByName(String name)
    {
    	
        if (StringUtils.isBlank(name)) {
            throw new IllegalArgumentException("Missing wiki name");
        }

        Object obj = ServiceHibernate.getCachedWikiDocumentForRSControl(DBCacheRegionConstants.WIKI_DOCUMENT_CACHE_REGION, 
        																name);
        if (obj != null) {
        	if (Log.log.isDebugEnabled()) {
        		Log.log.debug(String.format("Returning %s wiki document from cache region %s (first cache hit)", name, 
        									DBCacheRegionConstants.WIKI_DOCUMENT_CACHE_REGION.getValue()));
        	}
        	
            return (WikiDocumentVO) obj;
        } else {
        	if (Log.log.isDebugEnabled()) {
        		Log.log.debug(String.format("No %s wiki document found in cache region %s (first cache hit)", name, 
        									DBCacheRegionConstants.WIKI_DOCUMENT_CACHE_REGION.getValue()));
        	}
        }
           
        ReentrantLock lock = LockUtils.getLock(Constants.LOCK_ACTIONPROCESSDEPENDENCY + name);
        ReentrantLock alock = LockUtils.getLock(Constants.LOCK_ACTIONABORTDEPENDENCY + name);
        WikiDocumentVO doc = null;
        
        try {
        
            lock.lock();
            alock.lock();
        
            // Check again
            obj = ServiceHibernate.getCachedWikiDocumentForRSControl(DBCacheRegionConstants.WIKI_DOCUMENT_CACHE_REGION, name);
            
            if (obj != null) {
            	if (Log.log.isDebugEnabled()) {
            		Log.log.debug(String.format("Returning %s wiki document from cache region %s (second cache hit)", 
                              					name, DBCacheRegionConstants.WIKI_DOCUMENT_CACHE_REGION.getValue()));
            	}
                
                return (WikiDocumentVO) obj;
            } else {
            	if (Log.log.isDebugEnabled()) {
            		Log.log.debug(String.format("No %s wiki document found in cache region %s (second cache hit)", name, 
												DBCacheRegionConstants.WIKI_DOCUMENT_CACHE_REGION.getValue()));
            	}
            	
            	WikiDocument wikiDoc = WikiUtils.getWikiDocumentModel(null, name, "system", false);
            	
            	if (wikiDoc != null) {
            		doc = wikiDoc.doGetVO();
            	}
            	
            	if(doc == null) {
            		Log.log.error(String.format("Wiki document %s not found", name));
            	} else {
            		ServiceHibernate.cacheWikiDocumentForRSControl(wikiDoc);
            		
            		Integer wikiCheckSum = null;
            		
                    try {
                        wikiChecksumLock.readLock().lock();
                        wikiCheckSum = wikiChecksum.get(name);
                    } finally {
                        wikiChecksumLock.readLock().unlock();
                    }
            		
            		if (wikiCheckSum != null && doc.getChecksum().intValue() != wikiCheckSum.intValue()) {
	            		// Clear cached AT and AT Abort Process dependencies from cache only if checksum is different
	            		
	            		ActionProcessDependency cachedATProcDep = cacheActionProcessDependency.remove(name);
	                    
	                    if (cachedATProcDep != null) {
	                        Log.log.warn(String.format("Removed existing cached AT process dependency for %s", name));
	                    }
	                    
	                    ActionProcessDependency cachedATAProcDep = cacheActionAbortDependency.remove(name);
	                    
	                    if (cachedATAProcDep != null) {
	                        Log.log.warn(String.format("Removed existing cached AT abort process dependency for %s", name));
	                    }
	                    
	                       try {
	                            wikiChecksumLock.writeLock().lock();
	                            wikiChecksum.remove(name);
	                        } finally {
   	                            wikiChecksumLock.writeLock().unlock();
	                        }
            		}
            	}
            }
        }
        catch (Throwable e)
        {
            Log.log.error(e.getMessage(), e);
        } finally {
            lock.unlock();
            alock.unlock();
        }

        return doc;
    }

    static String getWikiDocumentModelXML(String wiki, String modelType) throws Exception
    {
        String result = null;
        WikiDocumentVO doc = findWikiDocumentByName(wiki);
        if (doc != null)
        {
            if (modelType.equals(Constants.MODEL_TYPE_MAIN))
            {
                result = doc.getUModelProcess();
            }
            else if (modelType.equals(Constants.MODEL_TYPE_ABORT))
            {
                result = doc.getUModelException();
            }
        }
        else
        {
            throw new Exception(String.format("Missing WIKI document: %s", wiki));
        }
        return result;
    } // getWikiDocumentModelXML

	public static String initTaskRequest(String actionid, Map params, String wiki, String processid, String executeid, String problemid, String userid, String actionName)
    {
        String result = null;
        String processNum = null;
        if (StringUtils.isNotBlank(processid))
        {
            processNum = ServiceWorksheet.getProcessNumberByProcessId(processid);
        }
        String problemNum = ServiceWorksheet.getProblemNumberByProblemId(problemid);

        if (actionid != null)
        {
            result = createExecuteRequest(actionid, params, new HashMap(), wiki, processid, executeid, problemid, null, null, userid, false, null, actionName, processNum, problemNum);
        }
        else
        {
            Log.log.warn("FAILED: Missing actiontask for actionid: " + actionid);
        }

        return result;
    } // initTaskRequest

    public static String getActionTaskId(String actionname, String namespace)
    {
        //        String result = null;

        //        String name = null;
        String fullname = null;

        //        int pos = actionname.indexOf('#');
        if (actionname.indexOf('#') > -1)
        {
            fullname = actionname;
        }
        else
        {
            fullname = actionname + (StringUtils.isNotBlank(namespace) ? ("#" + namespace) : "");
            //            name = actionname.substring(0, pos);
            //            namespace = actionname.substring(pos + 1);
        }

        //        fullname = name + (StringUtils.isNotEmpty(namespace) ? "#" + namespace : "");

        return ServiceHibernate.getActionIdFromFullname(fullname, "system");

    } // getActionTaskId

    public static String getActionTaskName(String actionid)
    {
        return ServiceHibernate.getActionTaskNameFromId(actionid, "system");
    } // getActionTaskName

    public static void cacheFlush(String docName)
    {
        ReentrantLock lock = LockUtils.getLock(Constants.LOCK_ACTIONPROCESSDEPENDENCY + docName);
        ReentrantLock alock = LockUtils.getLock(Constants.LOCK_ACTIONABORTDEPENDENCY + docName);
    	
        try {
            lock.lock();
            alock.lock();
            // HP TO DO This is not good. We need model-model dependency 
//            cacheActionProcessDependency.clear();
//            cacheActionAbortDependency.clear();
            cacheActionProcessDependency.remove(docName);
            cacheActionAbortDependency.remove(docName);
            
            // remove cache of runbook PROCESS_TIMEOUT
            cacheProcessTimeout.remove(docName);

            // HP TO DO This is not good. We need model-model dependency 
//            ServiceHibernate.clearDBCacheRegion(DBCacheRegionConstants.WIKI_DOCUMENT_CACHE_REGION, false);
            HibernateUtil.clearCachedDBObject(DBCacheRegionConstants.WIKI_DOCUMENT_CACHE_REGION, docName);
        } finally {
            lock.unlock();
            alock.unlock();
        }

    } // flushCache

    static ActionProcessDependency getActionProcessDependency(String wiki, String modelXML, String modelMode) throws Exception
    {
        ActionProcessDependency result = null;

        // try cache first
        result = (ActionProcessDependency) cacheActionProcessDependency.get(wiki);
        
        if (result != null)
        {
            return result;
        }
        
        // FIX - remove lock when wiki document is deleted
        ReentrantLock lock = LockUtils.getLock(Constants.LOCK_ACTIONPROCESSDEPENDENCY + wiki);
        try
        {
            lock.lock();
            
            // try GET again since someone may have created
            //result = (ActionProcessDependency) cacheActionProcessDependency.get(wiki);
            if (result == null)
            {
                if (Log.log.isDebugEnabled()) {
                    Log.log.debug(String.format("Action process dependency for %s wiki document not found in cache", wiki));
                }
                if (modelXML != null && modelMode != null)
                {
                    result = getModelDependency(wiki, modelXML, Constants.MODEL_TYPE_MAIN, modelMode);

                    // add cacheActionProcessDependency
                    if (result != null)
                    {
                        //cacheActionProcessDependency.put(wiki, result);
                        WikiDocumentVO wikiDocVO = findWikiDocumentByName(wiki);
                        try {
                            wikiChecksumLock.writeLock().lock();
                            wikiChecksum.put(wiki, wikiDocVO.getChecksum());
                        } finally {
                            wikiChecksumLock.writeLock().unlock();
                        }
                  
                        if (Log.log.isDebugEnabled()) {
                            Log.log.debug(String.format("Cached action process dependency for %s wiki document", wiki));
                        }
                    }
                }
                else
                {
                    throw new Exception("Missing modelXML or modelMode");
                }
            }
        }
        catch (Throwable e)
        {
            Log.log.debug(e.getMessage(), e);
            throw e;
        } finally {
            lock.unlock();
        }

        return result;
    } // getActionProcessDependency

    static ActionProcessDependency getActionAbortDependency(String wiki, String modelXML, String modelMode) throws Exception
    {
        ActionProcessDependency result = null;

        // try cache first
        result = (ActionProcessDependency) cacheActionAbortDependency.get(wiki);
        
        if (result != null)
        {
            return result;
        }
        
            // try GET again since someone may have created
            result = (ActionProcessDependency) cacheActionAbortDependency.get(wiki);
            if (result == null)
            {
                if (modelXML != null && modelMode != null)
                {
                    result = getModelDependency(wiki, modelXML, Constants.MODEL_TYPE_ABORT, modelMode);

                    // add cacheActionProcessDependency
                    if (result != null)
                    {
                        cacheActionAbortDependency.put(wiki, result);
                        if (Log.log.isDebugEnabled()) {
                        	Log.log.debug(String.format("Cached action abort process dependency for %s wiki document", 
                        								wiki));
                        }
                    }
                }
            }
        
        return result;
    } // getActionAbortDependency

    static ActionProcessDependency getModelDependency(String docName, String modelXML, String modelType, String modelMode) throws Exception
    {
        ActionProcessDependency result = null;

        if (!StringUtils.isEmpty(modelXML))
        {
            // list of referenced docs
            Set references = new HashSet();

            // CREATE
            result = new ActionProcessDependency();
            try
            {
                DirectedTreeNode start = getActionProcessTasksFromModel(result.getTasks(), result.getModel(), modelType, modelMode, docName, modelXML, new HashSet(), result.getModel().getStartNodes(), result.getModel().getEndNodes(), result.getModel().getEventNodes(), result.getModel().getEdgeNodes(), references, "", "0", "0", null);
                result.getModel().setStartNode(start);
                augmentDependency(result);
            }
            catch (Exception ex)
            {
                Log.log.warn(ex.getMessage(), ex);
                throw ex;
            }
        }
        
        return result;
    } // getModelDependency

    @SuppressWarnings({ "unchecked", "rawtypes" })
    static DirectedTreeNode getActionProcessTasksFromModel(List<ActionProcessTask> tasks, DirectedTree model, String modelType, String modelMode, String docName, String xml, Set traversed, Set<DirectedTreeNode> startNodes, Set<DirectedTreeNode> endNodes, Set<DirectedTreeNode> eventNodes, Map<String, EdgeNode> edgeNodes, Set references, String parentDoc, String subprocessId, String parentSubprocessId, String processids) throws Exception
    {
        DirectedTreeNode result = null;

        // com.resolve.util.Log.log.debug("getActionProcessTasksFromModel:");
        // com.resolve.util.Log.log.debug("  docname: "+docname);
        // com.resolve.util.Log.log.debug("  xml: "+xml);

        // check docName permission

        // add document to references
        references.add(docName);

        if (!StringUtils.isEmpty(xml) && !traversed.contains(docName))
        {
            SAXReader reader = new SAXReader();
            reader.setFeature("http://apache.org/xml/features/disallow-doctype-decl", true);
            reader.setFeature("http://xml.org/sax/features/external-general-entities", false);
            reader.setFeature("http://xml.org/sax/features/external-parameter-entities", false);
            Document document = reader.read(new ByteArrayInputStream(xml.getBytes()));

            /*
             * Since getActionProcessTasksFromModel get called recursively for sub-runbooks
             * guard against traversing model which is already being travered
             */
            // add docname to traversed
            traversed.add(docName);

            /*
             * START node
             */
            Node xmlNode = document.selectSingleNode("/mxGraphModel/root/Start");
            if (xmlNode == null)
            {
                throw new Exception("Missing START node");
            }
            else
            {
                String id = xmlNode.valueOf("@id");
                DirectedTreeNode node = new DirectedTreeNode(model, createNodeId(docName, id, parentDoc, processids), Constants.MODEL_NODE_TYPE_START);

                // set result start node
                result = node;
                startNodes.add(node);

                // init task parameters
                Map taskParams = new HashMap();

                //added user parameters by getting the json from the CDATA
                String paramStr = xmlNode.valueOf("@description");
//                String jsonParams = null;
                String jsonInputsParams = null;
//                String jsonOutputsParams = null;
                Node paramsNode = xmlNode.selectSingleNode("params");
                if(paramsNode != null)
                {
//                    jsonParams = paramsNode.getStringValue();
                    
                    //considering both Inputs and Outputs
                    jsonInputsParams = paramsNode.selectSingleNode("inputs").getStringValue();
//                    jsonOutputsParams = paramsNode.selectSingleNode("outputs").getStringValue();
                }

                // add to parameter map
//                taskParams.putAll(decodeModelParam(jsonParams, paramStr));
                taskParams.putAll(decodeModelParam(jsonInputsParams, paramStr));

                // init parentName
                String parentName = docName;
                if (StringUtils.isNotBlank(parentDoc))
                {
                	parentName = parentDoc;
                	if (parentDoc.indexOf(':') != -1)
                	{
	                    parentName = parentDoc.substring(parentDoc.lastIndexOf(':')+1);
                	}
                }

                // init nodeId
                String wikiId = createWikiId(docName, subprocessId);
                String parentId = createWikiId(parentName, parentSubprocessId);

                // add WIKI_PROCESS parameter
                taskParams.put(Constants.EXECUTE_WIKI_PROCESS, docName);
                taskParams.put(Constants.EXECUTE_WIKI_PARENT, parentName);
                taskParams.put(Constants.EXECUTE_WIKI_WIKIID, wikiId);
                taskParams.put(Constants.EXECUTE_WIKI_PARENTID, parentId);
                taskParams.put(Constants.MODEL_TYPE, modelType);
                taskParams.put(Constants.MODEL_MODE, modelMode);

                // init task definition
                ActionProcessTask task = new ActionProcessTask(node.getId(), node);
                task.setLabel("start");
                task.setName("start");
                task.setNamespace("resolve");
                task.setWiki(docName);
                task.setParams(taskParams);

                // resolve taskdef to task_sid
                String task_sid = getActionTask(task.getName(), task.getNamespace());
                if (task_sid != null)
                {
                    task.setTaskSid(task_sid);

                    // add task to the start of the list
                    tasks.add(task);
                }

                // set node value
                node.setValue(task);
            }

            /*
             * TASK node
             */
            List list = document.selectNodes("/mxGraphModel/root/Task");
            if (list == null)
            {
                throw new Exception("Missing TASK node");
            }

            boolean missingActionTask = false;
            String missingActionTaskMsg = "";
            for (Iterator i = list.iterator(); i.hasNext();)
            {
                xmlNode = (Node) i.next();
                DirectedTreeNode node = new DirectedTreeNode(model, createNodeId(docName, xmlNode.valueOf("@id"), parentDoc, processids), Constants.MODEL_NODE_TYPE_TASK);

                String label = xmlNode.valueOf("@label");
                String taskdef = xmlNode.valueOf("@description");
//                String jsonParams = null;
                String jsonInputsParams = null;
                String jsonOutputsParams = null;
                Node paramsNode = xmlNode.selectSingleNode("params");
                if(paramsNode != null)
                {
//                    jsonParams = paramsNode.getStringValue();
                    
                    //considering both Inputs and Outputs
                    jsonInputsParams = paramsNode.selectSingleNode("inputs").getStringValue();
                    jsonOutputsParams = paramsNode.selectSingleNode("outputs").getStringValue();
                }


                // init taskfullname
                int paramPos = taskdef.indexOf('?');
                if (paramPos < 0)
                {
                    paramPos = taskdef.length();
                }
                String taskfullname = taskdef.substring(0, paramPos);

                // get tasksid
                String taskName = "";
                String taskNamespace = "";
                int pos = taskfullname.indexOf('#');
                if (pos > 0)
                {
                    taskName = taskfullname.substring(0, pos);
                    taskNamespace = taskfullname.substring(pos + 1, taskfullname.length());
                }
                else
                {
                    taskName = taskfullname;
                    taskNamespace = "";
                }

                // String taskSid = xmlNode.valueOf("@sys_id");
                String taskSid = getActionTask(taskName, taskNamespace);
                if (!StringUtils.isEmpty(taskSid))
                {

                    // add task to list of ActionProcessTask
                    ActionProcessTask task = new ActionProcessTask(node.getId(), node);
                    task.setWiki(docName);

                    // set parameters
                    Map taskParams = new HashMap();
                    pos = taskdef.indexOf('?');
                    if (pos != -1)
                    {
                        // separate parameters from name#namespace?parameters
                        String paramStr = taskdef.substring(pos + 1, taskdef.length());

                        // update taskdef
                        taskdef = taskdef.substring(0, pos);

//                        Map tmpParams = decodeModelParam(jsonParams, paramStr);
                        Map tmpParams = decodeModelParam(jsonInputsParams, paramStr);
                        task.setOutputs(decodeModelParam(jsonOutputsParams, paramStr));
                        for (Iterator tmpParamIdx = tmpParams.entrySet().iterator(); tmpParamIdx.hasNext();)
                        {
                            Map.Entry tmpParam = (Map.Entry) tmpParamIdx.next();
                            String key = (String) tmpParam.getKey();
                            String value = (String) tmpParam.getValue();
                            if (!StringUtils.isEmpty(key) && !StringUtils.isEmpty(value))
                            {
                                // if (key.charAt(key.length() - 1) == ':')
                                // {
                                // key = key.substring(0, key.length() - 1);
                                // }
                                taskParams.put(key, value);
                            }
                        }
                    }

                    // separate taskdef name and namespace from name#namespace
                    taskName = taskdef;
                    taskNamespace = "";
                    pos = taskdef.indexOf('#');
                    if (pos != -1)
                    {
                        taskName = taskdef.substring(0, pos);
                        taskNamespace = taskdef.substring(pos + 1, taskdef.length());
                    }

                    // set task name and namespace
                    task.setLabel(label);
                    task.setName(taskName);
                    task.setNamespace(taskNamespace);
                    task.setParams(taskParams);
                    task.setTaskSid(taskSid);

                    // add to task list
                    tasks.add(task);

                    // set node value
                    node.setValue(task);
                }
                else
                {
                    missingActionTask = true;
                    missingActionTaskMsg += "<br/>Missing ActionTask: " + taskName + "#" + taskNamespace;
                    Log.log.error(missingActionTaskMsg);
                }
            }

            if (missingActionTask)
            {
                throw new Exception(missingActionTaskMsg);
            }

            /*
             * END node
             */
            list = document.selectNodes("/mxGraphModel/root/End");
            if (list == null)
            {
                throw new Exception("Missing END node");
            }

            for (Iterator i = list.iterator(); i.hasNext();)
            {
                xmlNode = (Node) i.next();

                String id = xmlNode.valueOf("@id");
                DirectedTreeNode node = new DirectedTreeNode(model, createNodeId(docName, id, parentDoc, processids), Constants.MODEL_NODE_TYPE_END);
                endNodes.add(node);

                // init task parameters
                Map taskParams = new HashMap();

                // added user parameters
                String endParamStr = xmlNode.valueOf("@description");
//                String jsonParams = null;
                String jsonInputsParams = null;
//                String jsonOutputsParams = null;
                Node paramsNode = xmlNode.selectSingleNode("params");
                if(paramsNode != null)
                {
//                    jsonParams = paramsNode.getStringValue();
                    
                    //considering both Inputs and Outputs
                    jsonInputsParams = paramsNode.selectSingleNode("inputs").getStringValue();
//                    jsonOutputsParams = paramsNode.selectSingleNode("outputs").getStringValue();
                }

//                taskParams.putAll(decodeModelParam(jsonParams, endParamStr));
                taskParams.putAll(decodeModelParam(jsonInputsParams, endParamStr));

                // init parentName
                String parentName = docName;
                if (StringUtils.isNotBlank(parentDoc))
                {
                	parentName = parentDoc;
                	if (parentDoc.indexOf(':') != -1)
                	{
	                    parentName = parentDoc.substring(parentDoc.lastIndexOf(':')+1);
                	}
                }

                // init nodeId
                String wikiId = createWikiId(docName, subprocessId);
                String parentId = createWikiId(parentName, parentSubprocessId);

                // add WIKI_PROCESS parameter
                taskParams.put(Constants.EXECUTE_WIKI_PROCESS, docName);
                taskParams.put(Constants.EXECUTE_WIKI_PARENT, parentName);
                taskParams.put(Constants.EXECUTE_WIKI_WIKIID, wikiId);
                taskParams.put(Constants.EXECUTE_WIKI_PARENTID, parentId);
                taskParams.put(Constants.MODEL_TYPE, modelType);
                taskParams.put(Constants.MODEL_MODE, modelMode);

                // init task definition
                ActionProcessTask task = new ActionProcessTask(node.getId(), node);
                task.setLabel("end");
                task.setName("end");
                task.setNamespace("resolve");
                task.setWiki(docName);
                task.setParams(taskParams);

                // resolve taskdef to task_sid
                String task_sid = getActionTask(task.getName(), task.getNamespace());
                if (task_sid != null)
                {
                    task.setTaskSid(task_sid);

                    // add task only if task_sid exists
                    tasks.add(task);
                }

                // set node value
                node.setValue(task);
            }

            /*
             * EVENT node
             */
            list = document.selectNodes("/mxGraphModel/root/Event");
            if (list != null)
            {
                for (Iterator i = list.iterator(); i.hasNext();)
                {
                    xmlNode = (Node) i.next();

                    String id = xmlNode.valueOf("@id");
                    DirectedTreeNode node = new DirectedTreeNode(model, createNodeId(docName, id, parentDoc, processids), Constants.MODEL_NODE_TYPE_EVENT);
                    eventNodes.add(node);

                    // init task parameters
                    Map taskParams = new HashMap();

                    // added user parameters
                    String eventParamStr = xmlNode.valueOf("@description");
//                    String jsonParams = null;
                    String jsonInputsParams = null;
//                    String jsonOutputsParams = null;
                    Node paramsNode = xmlNode.selectSingleNode("params");
                    if(paramsNode != null)
                    {
//                        jsonParams = paramsNode.getStringValue();
                        
                        //considering both Inputs and Outputs
                        jsonInputsParams = paramsNode.selectSingleNode("inputs").getStringValue();
//                        jsonOutputsParams = paramsNode.selectSingleNode("outputs").getStringValue();
                    }

//                    taskParams.putAll(decodeModelParam(jsonParams, eventParamStr));
                    taskParams.putAll(decodeModelParam(jsonInputsParams, eventParamStr));

                    // init parentName
                    String parentName = docName;
	                if (StringUtils.isNotBlank(parentDoc))
	                {
	                	parentName = parentDoc;
	                	if (parentDoc.indexOf(':') != -1)
	                	{
		                    parentName = parentDoc.substring(parentDoc.lastIndexOf(':')+1);
	                	}
	                }

                    // init nodeId
                    String wikiId = createWikiId(docName, subprocessId);
                    String parentId = createWikiId(parentName, parentSubprocessId);

                    // add WIKI_PROCESS parameter
                    taskParams.put(Constants.EXECUTE_WIKI_PROCESS, docName);
                    taskParams.put(Constants.EXECUTE_WIKI_PARENT, parentName);
                    taskParams.put(Constants.EXECUTE_WIKI_WIKIID, wikiId);
                    taskParams.put(Constants.EXECUTE_WIKI_PARENTID, parentId);
                    taskParams.put(Constants.MODEL_TYPE, modelType);
                    taskParams.put(Constants.MODEL_MODE, modelMode);

                    // init task definition
                    ActionProcessTask task = new ActionProcessTask(node.getId(), node);
                    task.setLabel("event");
                    task.setName("event");
                    task.setNamespace("resolve");
                    task.setWiki(docName);
                    task.setParams(taskParams);
                    String eventid = (String) taskParams.get(Constants.EVENT_EVENTID);
                    if (eventid == null)
                    {
                        Log.log.error("Missing EVENT_EVENTID value for event in wiki: " + docName + "\nPARAMS: " + taskParams.toString(), new Exception());
                    }
                    else
                    {
                        task.setEventId(eventid);
                    }

                    // resolve taskdef to task_sid
                    String task_sid = getActionTask(task.getName(), task.getNamespace());
                    if (task_sid != null)
                    {
                        task.setTaskSid(task_sid);

                        // add task only if task_sid exists
                        tasks.add(task);
                    }

                    // set node value
                    node.setValue(task);
                }
            }

            /*
             * SUBPROCESS node
             */
            list = document.selectNodes("/mxGraphModel/root/Subprocess");
            if (list != null)
            {
                for (Iterator i = list.iterator(); i.hasNext();)
                {
                    xmlNode = (Node) i.next();
                    DirectedTreeNode node = new DirectedTreeNode(model, createNodeId(docName, xmlNode.valueOf("@id"), parentDoc, processids), Constants.MODEL_NODE_TYPE_SUBPROCESS);

                    ModelSubprocess subprocess = new ModelSubprocess(node.getId());
                    String wiki = xmlNode.valueOf("@description");
                    if (StringUtils.isEmpty(wiki))
                    {
                        wiki = xmlNode.valueOf("@label");

                        // remove carriage returns
                        wiki = wiki.replaceAll("\\r\\n", " ");
                        wiki = wiki.replaceAll("\\n\\r", " ");
                        wiki = wiki.replaceAll("\\n", " ");
                    }
                    
                    //considering Inputs only
                    Node paramsNode = xmlNode.selectSingleNode("params");
                    if(paramsNode != null)
                    {
                        subprocess.setInputParameters(paramsNode.selectSingleNode("inputs").getStringValue());
                    } 

                    // prefix with docname namespace if not defined
                    if (wiki.indexOf('.') < 0)
                    {
                        int pos = docName.indexOf('.');
                        String namespace = docName.substring(0, pos);

                        wiki = namespace + '.' + wiki;
                    }
                    subprocess.setValue(wiki);

                    // set node value - NOTE does not set ActionProcesstask
                    node.setValue(subprocess);
                }
            }

            /*
             * PRECONDITION node
             */
            list = document.selectNodes("/mxGraphModel/root/Precondition");
            if (list != null)
            {
                for (Iterator i = list.iterator(); i.hasNext();)
                {
                    xmlNode = (Node) i.next();
                    DirectedTreeNode node = new DirectedTreeNode(model, createNodeId(docName, xmlNode.valueOf("@id"), parentDoc, processids), Constants.MODEL_NODE_TYPE_PRECONDITION);

                    ModelPrecondition precondition = new ModelPrecondition(node.getId());
                    
                    String value = xmlNode.valueOf("@description");
                    
                    if (Log.log.isDebugEnabled() && StringUtils.isEmpty(value))
                    {
                        value = xmlNode.valueOf("@label");
                        Log.log.debug("Node id:" + node.getId() + ". Precondition description is null/empty. Getting node value from label instead:" + value);
                    }
                    
                    precondition.setValue(value);

                    String merge = xmlNode.valueOf("@merge");
                    
                    if (StringUtils.isEmpty(merge))
                    {
                        merge = ModelPrecondition.DEFAULT_PRECONDITION_MERGE;
                    }
                     
                    precondition.setMerge(merge);
                    
                    // set node value - NOTE does not set ActionProcesstask
                    node.setValue(precondition);
                }
            }

            /*
             * EDGE node (dependency) - init dependency edges
             */
            list = document.selectNodes("/mxGraphModel/root/Edge/mxCell");
            if (list == null)
            {
                throw new Exception("Missing EDGE lines");
            }

            for (Iterator i = list.iterator(); i.hasNext();)
            {
                xmlNode = (Node) i.next();
                String srcId = createNodeId(docName, xmlNode.valueOf("@source"), parentDoc, processids);
                String dstId = createNodeId(docName, xmlNode.valueOf("@target"), parentDoc, processids);
                String value = xmlNode.getParent().valueOf("@label");

                DirectedTreeNode srcNode = model.getNode(srcId);
                DirectedTreeNode dstNode = model.getNode(dstId);

                if (srcNode == null || dstNode == null)
                {
                    throw new Exception("Edge is missing " + (srcNode == null ? "source" : "destination") + " node");
                }

                // add DirectedTreeNode (srcNode is added as parent of dstNode and dstNode is added as child of srcNode)
                srcNode.addChild(dstNode);

                // add EdgeNode
                EdgeNode edge = new EdgeNode(srcNode.getId(), dstNode.getId());
                if (StringUtils.isNotEmpty(value))
                {
                    edge.setValue(value);
                }
                edgeNodes.put(edge.getId(), edge);
            }

            if (Log.log.isDebugEnabled()) {
                Log.log.debug("# of Edge Nodes B4 Processing SubProcesses in " + docName + " is " + edgeNodes.size());
            
                for (String enId : edgeNodes.keySet())
                {
                    Log.log.debug("Edge Node Id: " + enId + ", Edge Node: " + edgeNodes.get(enId));
                }
            }
            
            /*
             * RECURSIVELY BUILD SUBPROCESS and join nodes
             */
            
            Set<DirectedTreeNode> subProcNodes = new HashSet<DirectedTreeNode>();
            
            list = document.selectNodes("/mxGraphModel/root/Subprocess");
            if (list != null)
            {
                for (Iterator i = list.iterator(); i.hasNext();)
                {
                    xmlNode = (Node) i.next();

                    String id = xmlNode.valueOf("@id");
                    DirectedTreeNode node = model.getNode(createNodeId(docName, id, parentDoc, processids));

                    ModelSubprocess subprocess = (ModelSubprocess) node.getValue();
                    String subprocessDocName = subprocess.getValue();

                    WikiDocumentVO doc = findWikiDocumentByName(subprocessDocName);
                    if (doc != null)
                    {
                        String subprocessDocXml = doc.getUModelProcess();
                        Set<DirectedTreeNode> subprocessStartNodes = new HashSet();
                        Set<DirectedTreeNode> subprocessEndNodes = new HashSet();
                        Set<DirectedTreeNode> subprocessEventNodes = new HashSet();
                        Map<String, EdgeNode> subprocessEdgeNodes = new HashMap();
                        
                        getActionProcessTasksFromModel(tasks, model, modelType, modelMode, subprocessDocName, subprocessDocXml, traversed, subprocessStartNodes, subprocessEndNodes, subprocessEventNodes, subprocessEdgeNodes, references, StringUtils.isEmpty(parentDoc) ? docName : (parentDoc + ":" + docName), id, subprocessId, (StringUtils.isBlank(processids) ? "" : processids ) + PROCESSID_DELIMITER + id );

                        // join node's parents to start of subprocess
                        for (Iterator parentIdx = node.getParents().iterator(); parentIdx.hasNext();)
                        {
                            DirectedTreeNode parentNode = (DirectedTreeNode) parentIdx.next();

                            // get condition between parentNode to node

                            String parentId = getModelId(parentNode.getId(), false);
                            String nodeId = getModelId(node.getId(), false);
				        	//format of node id is _id_:+<parent node id>:_id_:<node id>
				        	//if this is a path into a sub-runbook the parent node id is needed
				        	//in all other situations the node id is needed
                            //a subrunbook will have more + than the node going into it
                            if (StringUtils.countMatches(parentNode.getId(), '+') <
	                            StringUtils.countMatches(node.getId(), '+'))
                            {
                            	nodeId = getModelId(node.getId(), true);
                            }

                            Node edge = document.selectSingleNode("/mxGraphModel/root/Edge/mxCell[@source='" + parentId + "' and @target='" + nodeId + "']");
                            String condition = "";
                            if (edge != null)
                            {
                                // get condition from xml model to assign for
                                // new linkage
                                String style = edge.valueOf("@style");
                                condition = edge.getParent().valueOf("@description");
                                condition = createEdgeCondition(style, condition);
                            }

                            // append merge=ANY
                            condition += ",merge=ANY";

                            for (Iterator startIdx = subprocessStartNodes.iterator(); startIdx.hasNext();)
                            {
                                DirectedTreeNode startNode = (DirectedTreeNode) startIdx.next();
                                ActionProcessTask startTask = (ActionProcessTask) startNode.getValue();
                                
                                // set parameters for startNode
                                
                                if (StringUtils.isNotBlank(doc.getUWikiParameters()))
                                {
                                    List<Map<String, String>> wikiParams = new ArrayList<Map<String, String>>();
                                    
                                    try
                                    {
                                        wikiParams = StringUtils.jsonArrayToList(StringUtils.stringToJSONArray(doc.getUWikiParameters()));
                                    }
                                    catch(RuntimeException re)
                                    {
                                        Log.log.warn("Error " + re.getMessage() + " in converting Wiki Parameters JSON " + doc.getUWikiParameters(), re);
                                    }
                                    
                                    //List<Map<String, String>> wikiParams = StringUtils.jsonArrayToList(StringUtils.stringToJSONArray(doc.getUWikiParameters()));
                                    
                                    for (Map<String, String> wikiParam : wikiParams)
                                    {
                                        if (wikiParam.containsKey("source") && wikiParam.containsKey("sourceName") &&
                                            wikiParam.containsKey("name") && wikiParam.get("source").equals("PARAM"))
                                        {
                                            String defaultValue = "";
                                            if (wikiParam.get("source").equals("PARAM"))
                                            {
                                                defaultValue = "$" + wikiParam.get("source") + "{" + wikiParam.get("sourceName") + "}";
                                            }
                                            else if (wikiParam.get("source").equals("CONSTANT"))
                                            {
                                                defaultValue = wikiParam.get("sourceName");
                                            }
                                            
                                            startTask.getParams().put(wikiParam.get("name"), defaultValue);
                                        }                                                       
                                    }
                                }
                                
                                startTask.getParams().putAll(decodeModelParam(subprocess.getInputParameters(), null));
                                
                                // add condition between parentNode to startNode
                                setTaskCondition(condition, parentNode, startNode, subprocessEdgeNodes);

                                startNode.addParent(parentNode);
                                
                                /* Handle pre-condition/action task/start/subprocess parent to subprocess by replacing destination id of 
                                 * pre-condition parent-2-subprocess edge or action task parent-2-subprocess edge or
                                 * start parent-2-subprocess edge or subprocess parent-2-subprocess edge with node id of 
                                 * subprocess start action task
                                 */
                                
                                if (parentNode != null && (parentNode.getType().equals(Constants.MODEL_NODE_TYPE_PRECONDITION) ||
                                                           parentNode.getType().equals(Constants.MODEL_NODE_TYPE_SUBPROCESS) /*||
                                                           parentNode.getType().equals(Constants.MODEL_NODE_TYPE_TASK) ||
                                                           parentNode.getType().equals(Constants.MODEL_NODE_TYPE_START)*/))
                                {
                                    EdgeNode src2SubProcEdge = edgeNodes.get(getEdgeId(parentNode.getId(), node.getId()));
                                    if (Log.log.isDebugEnabled()) {
                                        Log.log.debug("Pre-Condition/Action Task/Start/SubProcess To SubProcess Edge B4 cloning with different destination id : " + src2SubProcEdge);
                                    }
                                    src2SubProcEdge.setNewEndId(startNode.getId());
                                    EdgeNode clonedEdgeNode = src2SubProcEdge.cloneWithNewSrcAndDestIds(parentNode.getId(), startNode.getId());
                                    edgeNodes.put(clonedEdgeNode.getId(), clonedEdgeNode);
                                    if (Log.log.isDebugEnabled()) {
                                        Log.log.debug("Cloned Pre-Condition/Action Task/Start/SubProcess To SubProcess Edge with different destination id : " + edgeNodes.get(clonedEdgeNode.getId()));
                                    }
                                }
                            }
                        }

                        // join node's children to end of subprocess
                        for (Iterator childIdx = node.getChildren().iterator(); childIdx.hasNext();)
                        {
                            DirectedTreeNode childNode = (DirectedTreeNode) childIdx.next();

                            // get condition between node to childNode

                            String nodeId = getModelId(node.getId(), false);
                            String childId = getModelId(childNode.getId(), false);
				        	//format of node id is _id_:+<parent node id>:_id_:<node id>
				        	//if this is a path into a sub-runbook the parent node id is needed
				        	//in all other situations the node id is needed
                            //a subrunbook will have more + than the node going into it
                            if (StringUtils.countMatches(node.getId(), '+') <
	                            StringUtils.countMatches(childNode.getId(), '+'))
                            {
	                            childId = getModelId(childNode.getId(), true);
                            }
                            Node edge = document.selectSingleNode("/mxGraphModel/root/Edge/mxCell[@source='" + nodeId + "' and @target='" + childId + "']");
                            String condition = "";
                            if (edge != null)
                            {
                                // get condition from xml model to assign for
                                // new linkage
                                String style = edge.valueOf("@style");
                                condition = edge.getParent().valueOf("@description");
                                condition = createEdgeCondition(style, condition);
                            }

                            for (Iterator endIdx = subprocessEndNodes.iterator(); endIdx.hasNext();)
                            {
                                DirectedTreeNode endNode = (DirectedTreeNode) endIdx.next();

                                // add condition between endNode to childNode
                                setTaskCondition(condition, endNode, childNode, subprocessEdgeNodes);

                                endNode.addChild(childNode);

                                // set END_QUIT parameter to false to not abort
                                // the process
                                ActionProcessTask endTask = (ActionProcessTask) endNode.getValue();
                                endTask.setParam("END_QUIT", "false");
                                
                                /* Handle pre-condition/action task/end/subprocess child to subprocess by replacing source id of subprocess-2-pre-condition
                                 * or subprocess-2-action-task or subprocess-2-end or subprocess-2-subprocess edge with node id of subprocess end action task.
                                 */
                                
                                if (childNode != null && (childNode.getType().equals(Constants.MODEL_NODE_TYPE_PRECONDITION) ||
                                                          childNode.getType().equals(Constants.MODEL_NODE_TYPE_SUBPROCESS) /*||
                                                          childNode.getType().equals(Constants.MODEL_NODE_TYPE_TASK) ||
                                                          childNode.getType().equals(Constants.MODEL_NODE_TYPE_END)*/))
                                {
                                    EdgeNode subProc2DestEdge = edgeNodes.get(getEdgeId(node.getId(), childNode.getId()));
                                    if (Log.log.isDebugEnabled()) {
                                        Log.log.debug("SubProcess To Pre-Condition/Action Task/SubProcess Edge B4 cloning with different source id : " + subProc2DestEdge);
                                    }
                                    subProc2DestEdge.setNewStartId(endNode.getId());
                                    EdgeNode clonedEdgeNode = subProc2DestEdge.cloneWithNewSrcAndDestIds(endNode.getId(), childNode.getId());
                                    edgeNodes.put(clonedEdgeNode.getId(), clonedEdgeNode);
                                    if (Log.log.isDebugEnabled()) {
                                        Log.log.debug("Cloned SubProcess To Pre-Condition/Action Task/SubProcess Edge with different source id : " + edgeNodes.get(clonedEdgeNode.getId()));
                                    }
                                }
                            }
                        }

                        // remove subprocess node
                        // node.removeNode();
                        // Add subprocess node for removal after edge processing
                        subProcNodes.add(node);
                    }
                    else
                    {
                        Log.log.error("Missing subprocess wiki document: " + subprocessDocName);
                        throw new RuntimeException("Subprocess node " + ((ModelSubprocess) node.getValue()).id + " : failed to load subprocess wiki document " + subprocessDocName);
                    }
                }
            }
            
            if (Log.log.isDebugEnabled()) {
                Log.log.debug("# of Edge Nodes After Processing SubProcesses in " + docName + " is " + edgeNodes.size());
            
                for (String enId : edgeNodes.keySet())
                {
                    Log.log.debug("Edge Node Id: " + enId + ", Edge Node: " + edgeNodes.get(enId));
                }
            }

            /*
             * CONDITIONAL EDGES - reconnect dependency edges for removing
             * preconditions
             */
            list = document.selectNodes("/mxGraphModel/root/Edge/mxCell");
            for (Iterator i = list.iterator(); i.hasNext();)
            {
                xmlNode = (Node) i.next();
                String srcId = createNodeId(docName, xmlNode.valueOf("@source"), parentDoc, processids);
                String dstId = createNodeId(docName, xmlNode.valueOf("@target"), parentDoc, processids);
                String style = xmlNode.valueOf("@style");
                String condition = xmlNode.getParent().valueOf("@description");

                condition = createEdgeCondition(style, condition);
                
                String edgeCondition = createEdgeCondition(style, dstId);

                String edgeId = getEdgeId(srcId, dstId);
                
                EdgeNode edge = edgeNodes.get(edgeId);
                                
                if (edge != null )
                {
                    edge.setCondition(edgeCondition);
                    
                    if (StringUtils.isNotBlank(edge.getNewId()))
                    {
                        EdgeNode newVirtualEdge = edgeNodes.get(edge.getNewId());
                        
                        if (newVirtualEdge != null)
                        {
                            newVirtualEdge.setCondition(edgeCondition);
                            
                            DirectedTreeNode srcNode = model.getNode(srcId);
                            
                            if (srcNode.getType().equals(Constants.MODEL_NODE_TYPE_SUBPROCESS) /*||
                                srcNode.getType().equals(Constants.MODEL_NODE_TYPE_PRECONDITION) ||
                                srcNode.getType().equals(Constants.MODEL_NODE_TYPE_TASK) ||
                                srcNode.getType().equals(Constants.MODEL_NODE_TYPE_START)*/)
                            {
                                if (StringUtils.isNotBlank(edge.getNewStartId()))
                                {
                                    srcId = edge.getNewStartId();
                                }
                            }
                            
                            DirectedTreeNode dstNode = model.getNode(dstId);
                            
                            if (!srcNode.getType().equals(Constants.MODEL_NODE_TYPE_PRECONDITION) &&
                                (dstNode.getType().equals(Constants.MODEL_NODE_TYPE_SUBPROCESS) /*||
                                 dstNode.getType().equals(Constants.MODEL_NODE_TYPE_PRECONDITION) ||
                                 dstNode.getType().equals(Constants.MODEL_NODE_TYPE_TASK) ||
                                 dstNode.getType().equals(Constants.MODEL_NODE_TYPE_END)*/))
                            {
                                if (StringUtils.isNotBlank(edge.getNewEndId()))
                                {
                                    dstId = edge.getNewEndId();
                                }
                            }
                        }
                    }
                }
                
                DirectedTreeNode srcNode = model.getNode(srcId);
                DirectedTreeNode dstNode = model.getNode(dstId);

                setTaskCondition(condition, srcNode, dstNode, edgeNodes);
            }

            // Remove subprocess nodes
            
            for (DirectedTreeNode subProcNode : subProcNodes)
            {
                subProcNode.removeNode();
            }
            
            /*
             * SET EXPRESSION and REMOVE PRECONDITION nodes
             * 
             * Handle PRECONDITION attached to PRECONDITION
             */
            list = document.selectNodes("/mxGraphModel/root/Precondition");
            if (list != null)
            {
                // set expression for task
                for (Iterator i = list.iterator(); i.hasNext();)
                {
                    xmlNode = (Node) i.next();

                    DirectedTreeNode node = model.getNode(createNodeId(docName, xmlNode.valueOf("@id"), parentDoc, processids));
                    if (node != null && isNodeRootPreConditionNode(node))
                    {
                        // get the expression from the precondition node
                        //String expression = ((ModelPrecondition) node.getValue()).getValue();

                        // for each parent of the precondition node, set the
                        // post expression and post condition indexed with child-task's id
                        
                        List<DirectedTreeNode> parents = /*node.getParents()*/new ArrayList<DirectedTreeNode>();
 
                        /*recursivelyC*/collectParentTaskNodes(node, parents);
                        
                        for (Iterator<DirectedTreeNode> parentIdx = parents.iterator(); parentIdx.hasNext();)
                        {
                            // get parent task
                            DirectedTreeNode parentNode = parentIdx.next();
                            
                            if (Log.log.isDebugEnabled()) {
                                Log.log.debug("Parent Node (Id = " + parentNode.getId() + ", Node Type = " + parentNode.getType() + ") of Node (Id = " + node.getId() + ", Node Type = " + node.getType() + ")");
                            }
                            
                            if (parentNode.getType().equals(Constants.MODEL_NODE_TYPE_TASK) || 
                                parentNode.getType().equals(Constants.MODEL_NODE_TYPE_START) || 
                                parentNode.getType().equals(Constants.MODEL_NODE_TYPE_END) ||
                                parentNode.getType().equals(Constants.MODEL_NODE_TYPE_EVENT))
                            {
                                ActionProcessTask parentTask = (ActionProcessTask) parentNode.getValue();
    
                                // get parent postExpressions
                                Map postExpressions = parentTask.getPostExpressions();
                                if (postExpressions == null)
                                {
                                    postExpressions = new HashMap();
                                    parentTask.setPostExpressions(postExpressions);
                                }
    
                                // get parent postConditions
                                Map postConditions = parentTask.getPostConditions();
                                if (postConditions == null)
                                {
                                    postConditions = new HashMap();
                                    parentTask.setPostConditions(postConditions);
                                }
                                
                                String parentToRootPreCondtionEdgeId = getEdgeId(parentTask.getId(), node.getId());
                                
                                EdgeNode parentToRootPreCondtionEdge = edgeNodes.get(parentToRootPreCondtionEdgeId);
                                
                                String parentToRootPreCondtionPostCondition = null;
                                String parentToRootPreCondtionPostConditionSuffix = null; // merge of preconditions in any path
                                
                                if (parentToRootPreCondtionEdge != null)
                                {
                                    if (StringUtils.isNotEmpty(parentToRootPreCondtionEdge.getCondition()))
                                    {
                                        parentToRootPreCondtionPostCondition = parentToRootPreCondtionEdge.getCondition();
                                    }
                                }
                                
                                List<DirectedTreeNode> children = /*node.getChildren()*/ new ArrayList<DirectedTreeNode>();
                                
                                recursivelyCollectChildTaskNodes(node, children);
                                
                                for (Iterator<DirectedTreeNode> childIdx = children.iterator(); childIdx.hasNext();)
                                {
                                    DirectedTreeNode childNode = childIdx.next();
                                    
                                    ActionProcessTask childTask = (ActionProcessTask) childNode.getValue();
    
                                    int pathCnt = childNode.getCountOfPathsToThisNodeFrom(node.getId());
                                    
                                    List<String> pathPostExpressions = new ArrayList<String>(pathCnt);
                                    
                                    for (int pathNo = 1; pathNo <= pathCnt; pathNo++)
                                    {
                                        Integer pathNoInt = new Integer(pathNo);
                                        Map<Integer, String> pathFromPreCondition = childNode./*getPathsFromPreCondition().get*/getPathToThisNodeFrom(node.getId(), new Integer(pathNoInt));
                                        int maxOrder = childNode./*getMaxOrder()*/getMaxOrderFor(node.getId(), pathNoInt);
                                        
                                        if (pathFromPreCondition != null && !pathFromPreCondition.isEmpty() && maxOrder >= 1)
                                        {
                                            String edgeSrcNodeId = parentNode.getId();
                                            DirectedTreeNode edgeSrcNode = model.getNode(edgeSrcNodeId);
                                           
                                            String edgeDstNodeId = null;
                                            String resultExpression = null;
                                            String expression = null;
                                           
                                            int order = 1;
                                            for (; order <= maxOrder; order++)
                                            {
                                                if (edgeSrcNode != null)
                                                {
                                                    edgeDstNodeId = pathFromPreCondition.get(new Integer(order));
                                                    DirectedTreeNode edgeDstNode = model.getNode(edgeDstNodeId);
                                                   
                                                    if (edgeDstNode != null && edgeDstNode.getType().equals(Constants.MODEL_NODE_TYPE_PRECONDITION))
                                                    {
                                                        if (StringUtils.isBlank(parentToRootPreCondtionPostConditionSuffix))
                                                        {
                                                            parentToRootPreCondtionPostConditionSuffix = ((ModelPrecondition)edgeDstNode.getValue()).getMerge();
                                                        }
                                                        
                                                        if (!parentToRootPreCondtionPostConditionSuffix.toUpperCase().contains(Constants.ASSESS_MERGE_ANY) &&
                                                            ((ModelPrecondition)edgeDstNode.getValue()).getMerge().toUpperCase().contains(Constants.ASSESS_MERGE_ANY))
                                                        {
                                                            parentToRootPreCondtionPostConditionSuffix = ((ModelPrecondition)edgeDstNode.getValue()).getMerge();
                                                        }
                                                        
                                                        String edgeId = getEdgeId(edgeSrcNodeId, edgeDstNodeId);
                                                       
                                                        EdgeNode edge = edgeNodes.get(edgeId);
                                                        
                                                        if (edge != null)
                                                        {
                                                            if (StringUtils.isNotEmpty(expression))
                                                            {
                                                                // Process the EdgeCondition label if it exists
                                                                
                                                                if (StringUtils.isNotBlank(edge.getValue()))
                                                                {
                                                                    String updatedExpression = "\"" + edge.getValue() + "\".equals((" + expression + ").toString())";       
                                                                    if(Log.log.isTraceEnabled()) {
                                                                        Log.log.trace("Edge Value = [" + edge.getValue() + "], expression = [" + expression + "], updatedExpression = [" + updatedExpression + "]");
                                                                    }
                                                                    expression = updatedExpression;
                                                                }
                                                                
                                                                String prefix = edge.getCondition().toUpperCase().contains(Constants.ASSESS_CONDITION_BAD) ? "!" : "";
                                                                
                                                                if (StringUtils.isNotEmpty(resultExpression))
                                                                {
                                                                    resultExpression += " && " + prefix + "(" + expression + ")";
                                                                }
                                                                else
                                                                {
                                                                    resultExpression = prefix + "(" + expression + ")";
                                                                }
                                                                
                                                                if (Log.log.isDebugEnabled()) {
                                                                    Log.log.debug("resultExpression : [" + resultExpression + "]");
                                                                }
                                                                
                                                            }
                                                        }
                                                        
                                                        expression = ((ModelPrecondition) edgeDstNode.getValue()).getValue();
                                                        
                                                        if (Log.log.isDebugEnabled()) {
                                                            Log.log.debug("expression : [" + expression + "]");
                                                        }
                                                        
                                                        edgeSrcNodeId = edgeDstNodeId;
                                                        edgeSrcNode = model.getNode(edgeSrcNodeId);
                                                    }
                                                }
                                            }
                                            
                                            // Add the last edge from pre-condition to child node
                                            
                                            String lastPreCondToDestEdgeId = getEdgeId(edgeSrcNodeId, childNode.getId());
                                            
                                            EdgeNode lastPreCondToDestEdge = edgeNodes.get(lastPreCondToDestEdgeId);
                                            
                                            if (lastPreCondToDestEdge != null && StringUtils.isNotEmpty(expression))
                                            {
                                                // Process the EdgeCondition label if it exists
                                                
                                                if (StringUtils.isNotBlank(lastPreCondToDestEdge.getValue()))
                                                {
                                                    String updatedExpression = "\"" + lastPreCondToDestEdge.getValue() + "\".equals((" + expression + ").toString())";
                                                    if (Log.log.isDebugEnabled()) {
                                                        Log.log.debug("Last Edge Value = [" + lastPreCondToDestEdge.getValue() + "], expression = [" + expression + "], updatedExpression = [" + updatedExpression + "]");
                                                    }
                                                    expression = updatedExpression;
                                                }
                                                
                                                String prefix = lastPreCondToDestEdge.getCondition().toUpperCase().contains(Constants.ASSESS_CONDITION_BAD) ? "!" : "";
                                                
                                                if (StringUtils.isNotEmpty(resultExpression))
                                                {
                                                    resultExpression += " && " + prefix + "(" + expression + ")";
                                                }
                                                else
                                                {
                                                    resultExpression = prefix + "(" + expression + ")";
                                                }
                                            }
                                            
                                            if (Log.log.isDebugEnabled()) {
                                                Log.log.debug("Path Post Expression being added : [" + resultExpression + "]");
                                            }
                                            
                                            pathPostExpressions.add(resultExpression);
                                        }
                                    }
                                    
                                    if (!pathPostExpressions.isEmpty())
                                    {
                                        String finalResultExpression = null;
                                        
                                        for (String resultExpression : pathPostExpressions)
                                        {
                                            if (StringUtils.isNotEmpty(finalResultExpression))
                                            {
                                                finalResultExpression += " || (" + resultExpression + ")";
                                            }
                                            else
                                            {
                                                finalResultExpression = "(" + resultExpression + ")";
                                            }
                                        }
                                        
                                        Map<String, String> postExpressionsForChildTask = (Map<String, String>) postExpressions.get(childTask.getId());
                                        
                                        if (postExpressionsForChildTask == null)
                                        {
                                            postExpressionsForChildTask = new HashMap<String, String>();
                                            postExpressions.put(childTask.getId(), postExpressionsForChildTask);
                                        }
                                        else
                                        {
                                            if (postExpressionsForChildTask.containsKey(node.getId()))
                                            {
                                                if (Log.log.isDebugEnabled()) {
                                                    Log.log.debug("Current post path expression to child task " + childTask.getId() + " from " + node.getId() + " is " + postExpressionsForChildTask.get(node.getId()));
                                                }
                                                finalResultExpression += " || " + postExpressionsForChildTask.containsKey(node.getId());
                                            }
                                        }
                                        if (Log.log.isDebugEnabled()) {
                                            Log.log.debug("Setting post path expression to child task " + childTask.getId() + " from " + node.getId() + " to " + finalResultExpression);
                                        }
                                        postExpressionsForChildTask.put(node.getId(), finalResultExpression);
                                    }
                                    
                                    if (StringUtils.isNotBlank(parentToRootPreCondtionPostCondition) /* || StringUtils.isNotBlank(parentToRootPreCondtionPostConditionSuffix)*/)
                                    {
                                        List conditions = (List)postConditions.get(childTask.getId());
                                        
                                        if (conditions == null)
                                        {
                                            conditions = new ArrayList();
                                            postConditions.put(childTask.getId(), conditions);
                                        }
                                        
                                        if (!conditions.contains(parentToRootPreCondtionPostCondition))
                                        {
                                            conditions.add(parentToRootPreCondtionPostCondition);
                                        }
                                        
                                        if (!conditions.contains(parentToRootPreCondtionPostConditionSuffix))
                                        {
                                            conditions.add(parentToRootPreCondtionPostConditionSuffix);
                                        }
                                    }
                                }
                            }
                        }
                    }
                }

                // remove node
                for (Iterator i = list.iterator(); i.hasNext();)
                {
                    xmlNode = (Node) i.next();
                    model.removeNode(createNodeId(docName, xmlNode.valueOf("@id"), parentDoc, processids));
                }
            }

            /*
             * MERGE - get merge condition for tasks and end and update parents
             * conditions
             */
            list = document.selectNodes("/mxGraphModel/root/Task | /mxGraphModel/root/End | /mxGraphModel/root/Event");
            for (Iterator i = list.iterator(); i.hasNext();)
            {
                xmlNode = (Node) i.next();

                String merge = xmlNode.valueOf("@merge");

                DirectedTreeNode node = model.getNode(createNodeId(docName, xmlNode.valueOf("@id"), parentDoc, processids));
                if (!StringUtils.isEmpty(merge) && node != null)
                {
                    List srcNodeParents = node.getParents();
                    if (srcNodeParents != null)
                    {
                        ActionProcessTask nodeTask = (ActionProcessTask) node.getValue();
                        for (Iterator parentIdx = srcNodeParents.iterator(); parentIdx.hasNext();)
                        {
                            DirectedTreeNode parentNode = (DirectedTreeNode) parentIdx.next();

                            ActionProcessTask parentTask = (ActionProcessTask) parentNode.getValue();

                            // init postConditions for parentTask
                            Map postConditions = parentTask.getPostConditions();
                            if (postConditions == null)
                            {
                                postConditions = new HashMap();
                                parentTask.setPostConditions(postConditions);
                            }
                            
                            List conditions = (List)postConditions.get(nodeTask.getId());
                            
                            if (conditions == null)
                            {
                                conditions = new ArrayList();
                                postConditions.put(nodeTask.getId(), conditions);
                            }
                            
                            if (conditions.isEmpty())
                            {
                                conditions.add(merge);
                            }
                            else
                            {
                                List conditionsWithMerge = new ArrayList();
                                
                                for (Object oldConditionObj : conditions)
                                {
                                    String oldCondition = (String)oldConditionObj;
                                    
                                    String conditionWithMerge = StringUtils.isNotBlank(oldCondition) ? oldCondition + "," + merge : merge;
                                    
                                    if (!conditionsWithMerge.contains(conditionWithMerge))
                                    {
                                        conditionsWithMerge.add(conditionWithMerge);
                                    }
                                }
                                
                                postConditions.put(nodeTask.getId(), conditionsWithMerge);
                            }
                        }
                    }
                }
            }

            // remove docname from traversed so that same sub-runbook at different postion can be traversed
            traversed.remove(docName);
        }

        return result;
    } // getActionProcessTasksFromModel

    static String createEdgeCondition(String style, String condition)
    {
        // update condition from style information
        if (style.indexOf("dashed=true") >= 0)
        {
            // init condition
            if (!StringUtils.isEmpty(condition))
            {
                condition += ",";
            }
            condition += "completed == false";
        }
        if (style.indexOf("strokeColor=green") >= 0)
        {
            // init condition
            if (!StringUtils.isEmpty(condition))
            {
                condition += ",";
            }
            condition += "condition == GOOD";
        }
        if (style.indexOf("strokeColor=red") >= 0)
        {
            // init condition
            if (!StringUtils.isEmpty(condition))
            {
                condition += ",";
            }
            condition += "condition == BAD";
        }
        return condition;
    } // createEdgeCondition

    //'str' is a json string now...convert that to Map and return
    private static Map decodeModelParam(final String jsonInputsParams, final String descStr)
    {
        Map<String, String> result = new LinkedHashMap<String, String>();
        
        if(StringUtils.isNotEmpty(jsonInputsParams) )
        {
            if (Log.log.isDebugEnabled()) {
                Log.log.debug("Using the CDATA params for this task");
            }
            try
            {
//                ObjectMapper mapper = new ObjectMapper();
//                mapper.configure(Feature.FAIL_ON_UNKNOWN_PROPERTIES, false);
//                MapType type = mapper.getTypeFactory().constructMapType(HashMap.class, String.class, Object.class);
                
                if(StringUtils.isNotBlank(jsonInputsParams))
                {
                    result.putAll(JsonUtils.convertJsonStringToMap(jsonInputsParams));
                }
                
/*                if(StringUtils.isNotBlank(jsonOutputsParams))
                {
                    result.putAll(JsonUtils.convertJsonStringToMap(jsonOutputsParams));
                }*/
            }
            catch (Exception e)
            {
                Log.log.debug("jsonInputsParams:" + jsonInputsParams);
//                Log.log.debug("jsonOutputsParams:" + jsonOutputsParams);
                Log.log.error("Error in transforming json to Map" + e.getMessage(), e);
                
                //if there is an error, call the old api and continue
//                result = decodeModelParam(descStr);
                
                //this is as per 3.4 - ProcessRequest #960
                result = StringUtils.urlToMap(descStr);
            }
        }
        else if(StringUtils.isNotBlank(descStr))
        {
            if (Log.log.isDebugEnabled()) {
                Log.log.debug("Using the OLD Description api for this task");
            }
//            result = decodeModelParam(descStr);
            
            //this is as per 3.4 - ProcessRequest #960
            result = StringUtils.urlToMap(descStr);
        }
        
        return result;
        
    }
    
    private static Map decodeModelParam(final String str)
    {
        Map<String, String> result = new LinkedHashMap<String, String>();

        if (!StringUtils.isEmpty(str) && !str.equalsIgnoreCase("NULL"))
        {
            try
            {
                // unescape html
                String unescapedStr = StringEscapeUtils.unescapeHtml(str);

                // url decode
                String decodedStr = java.net.URLDecoder.decode(unescapedStr, "UTF-8");

                String[] params = decodedStr.split("&");
                for (int i = 0; i < params.length; i++)
                {
                    String name = "";
                    String value = "";

                    String nameValue = java.net.URLDecoder.decode(params[i], "UTF-8");

                    int valIdx = nameValue.indexOf("=");
                    if (valIdx <= 0)
                    {
                        name = nameValue;
                        value = "";
                    }
                    else
                    {
                        name = nameValue.substring(0, valIdx);
                        value = nameValue.substring(valIdx + 1, nameValue.length());
                    }

                    if (StringUtils.isNotEmpty(name))
                    {
//                        value = java.net.URLDecoder.decode(value, "UTF-8");
                        result.put(name, value);
                    }
                }
            }
            catch (Exception e)
            {
                Log.log.debug(e.getMessage(), e);
            }
        }

        return result;
    } // decodeModelParam

    private static boolean isNodeRootPreConditionNode(DirectedTreeNode srcNode)
    {
        boolean isRootPreConditionNode = /*true*/ false;
        
        List<DirectedTreeNode> parents = srcNode.getParents();
        
        for (DirectedTreeNode parent : parents)
        {
            if (!parent.getType().equals(Constants.MODEL_NODE_TYPE_PRECONDITION))
            {
                isRootPreConditionNode = /*false*/true;
                break;
            }
        }
        
        return isRootPreConditionNode;
    }
    
    private static void recursivelyCollectChildTaskNodesWithPath(DirectedTreeNode rootPreConditionNode, DirectedTreeNode dstNode, List<DirectedTreeNode> actionTaskNodes, Map<String, Map<Integer, String>> pathsFromPreCondition, Integer maxOrder, Map<String, Map<Integer, Integer>> maxOrdersMap, Map<String, EdgeNode> edgeNodes ) throws Exception
    {
        if (dstNode.getType().equals(Constants.MODEL_NODE_TYPE_TASK) ||
            dstNode.getType().equals(Constants.MODEL_NODE_TYPE_END) ||
            dstNode.getType().equals(Constants.MODEL_NODE_TYPE_EVENT) ||
            dstNode.getType().equals(Constants.MODEL_NODE_TYPE_START))
        {
            dstNode.setPathToThisNodeFrom(rootPreConditionNode.getId(), pathsFromPreCondition.get(rootPreConditionNode.getId()), maxOrder);
            //dstNode.setPathsFromPreCondition(pathsFromPreCondition);
            //dstNode.setMaxOrder(maxOrder);
            
            if (!actionTaskNodes.contains(dstNode))
            {
                actionTaskNodes.add(dstNode);
            }
        }
        else if (dstNode.getType().equals(Constants.MODEL_NODE_TYPE_START))
        {
            // Ignore precondition to start action task of subprocess
        }
        else if (dstNode.getType().equals(Constants.MODEL_NODE_TYPE_PRECONDITION) && 
                 dstNode.getChildren() != null && !dstNode.getChildren().isEmpty())
        {
            Integer newMaxOrder = new Integer(maxOrder.intValue() + 1);
            
            pathsFromPreCondition.get(rootPreConditionNode.getId()).put(newMaxOrder, dstNode.getId());
            
            for (DirectedTreeNode child : dstNode.getChildren())
            {
                recursivelyCollectChildTaskNodesWithPath(rootPreConditionNode, child, actionTaskNodes, pathsFromPreCondition, newMaxOrder, maxOrdersMap, edgeNodes);
            }
        }
        // For subprocess only
        else if (dstNode.getType().equals(Constants.MODEL_NODE_TYPE_SUBPROCESS)) /*&& 
                 maxOrder.intValue() == 1 && edgeNodes.containsKey(rootPreConditionNode.getId() + "/" + dstNode.getId()))*/
        {
            String lastPreConNodeId = pathsFromPreCondition.get(rootPreConditionNode.getId()).get(new Integer(pathsFromPreCondition.get(rootPreConditionNode.getId()).size()));
            
            if (edgeNodes.containsKey(getEdgeId(lastPreConNodeId, dstNode.getId())))
            {
                EdgeNode preCon2SubProcEdge = edgeNodes.get(getEdgeId(lastPreConNodeId, dstNode.getId()));
                
                if (StringUtils.isNotBlank(preCon2SubProcEdge.getNewEndId()))
                {
                    DirectedTreeNode subProcStartNode = dstNode.getNode(preCon2SubProcEdge.getNewEndId());
                    
                    if (subProcStartNode != null)
                    {
                        subProcStartNode.setPathToThisNodeFrom(rootPreConditionNode.getId(), pathsFromPreCondition.get(rootPreConditionNode.getId()), maxOrder);
                        
                        if (!actionTaskNodes.contains(subProcStartNode))
                        {
                            actionTaskNodes.add(subProcStartNode);
                        }
                    }
                }
            }
        }
        else
        {
            throw new Exception("Preconditions can be directly connected to Task(s) or another precondition(s) or subprocess only.");
        }
    }
    
    private static void recursivelyCollectChildTaskNodes(DirectedTreeNode dstNode, List<DirectedTreeNode> actionTaskNodes) throws Exception
    {
        if (dstNode.getType().equals(Constants.MODEL_NODE_TYPE_TASK) || 
            dstNode.getType().equals(Constants.MODEL_NODE_TYPE_END) ||
            dstNode.getType().equals(Constants.MODEL_NODE_TYPE_START) ||
            dstNode.getType().equals(Constants.MODEL_NODE_TYPE_EVENT))
        {
            if (!actionTaskNodes.contains(dstNode))
            {
                actionTaskNodes.add(dstNode);
            }
        }
        else if (dstNode.getType().equals(Constants.MODEL_NODE_TYPE_PRECONDITION) && 
                 dstNode.getChildren() != null && !dstNode.getChildren().isEmpty())
        {
            for (DirectedTreeNode child : dstNode.getChildren())
            {
                recursivelyCollectChildTaskNodes(child, actionTaskNodes);
            }
        }
        else if (dstNode.getType().equals(Constants.MODEL_NODE_TYPE_SUBPROCESS))
        {
            // Ignore subprocess child node as start node of subprocess is linked as child with parents of subprocess 
        }
        else
        {
            throw new Exception("Preconditions can be directly connected to Task(s) or another precondition(s) only.");
        }
    }
    
    private static void /*recursivelyC*/collectParentTaskNodes(DirectedTreeNode srcNode, List<DirectedTreeNode> actionTaskNodes) throws Exception
    {
        /*
        if (srcNode.getType().equals(Constants.MODEL_NODE_TYPE_TASK) || 
            srcNode.getType().equals(Constants.MODEL_NODE_TYPE_START) ||
            srcNode.getType().equals(Constants.MODEL_NODE_TYPE_END) ||
            srcNode.getType().equals(Constants.MODEL_NODE_TYPE_EVENT))
        {
            if (!actionTaskNodes.contains(srcNode))
            {
                actionTaskNodes.add(srcNode);
            }
        }
        else*/ if (srcNode.getType().equals(Constants.MODEL_NODE_TYPE_PRECONDITION) && 
                 srcNode.getParents() != null && !srcNode.getParents().isEmpty())
        {
            for (DirectedTreeNode parent : srcNode.getParents())
            {
                if (parent.getType().equals(Constants.MODEL_NODE_TYPE_TASK) ||
                    parent.getType().equals(Constants.MODEL_NODE_TYPE_START) ||
                    parent.getType().equals(Constants.MODEL_NODE_TYPE_END) ||
                    parent.getType().equals(Constants.MODEL_NODE_TYPE_EVENT))
                {
                    if (!actionTaskNodes.contains(parent))
                    {
                        actionTaskNodes.add(parent);
                    }
                }
                //recursivelyCollectParentTaskNodes(parent, actionTaskNodes);
            }
        }
        /*else if (srcNode.getType().equals(Constants.MODEL_NODE_TYPE_SUBPROCESS))
        {
            // Ignore subprocess parent node as end node of subprocess is linked as parent to all childrens of subprocess 
        }
        else
        {
            throw new Exception("Preconditions can be directly connected to Task(s) or another precondition(s) only.");
        }*/
    }
    
    @SuppressWarnings({ "unchecked", "rawtypes" })
    static void setTaskCondition(String condition, DirectedTreeNode srcNode, DirectedTreeNode dstNode, Map<String, EdgeNode> edgeNodes) throws Exception
    {
        if (srcNode != null && dstNode != null)
        {
            // check if srcNode is a precondition
            List<DirectedTreeNode> srcNodeParents = null;
            if (srcNode.getValue() instanceof ModelPrecondition)
            {
                // get srcNode's parents
                srcNodeParents = srcNode.getParents();
            }

            // check if dstNode is a precondition
            List<DirectedTreeNode> dstNodeChildren = null;
            if (dstNode.getValue() instanceof ModelPrecondition)
            {
                // get dstNode's children
                dstNodeChildren = dstNode.getChildren();
            }

            // check that preconditions are not connected to on another
            if (srcNodeParents != null && dstNodeChildren != null)
            {
                Log.log.info("Found precondition connected directly to precondition!!!");
                //throw new Exception("Preconditions may not be directly connected to each other.");
            }

            /*
             *  if srcNode is a root preCondition i.e. at least one parent is of type Action Task, 
             *  add dstNode to each parent of Action Task type along with traversed nodes in order
             */
            if (srcNodeParents != null)
            {
                List<DirectedTreeNode> dstActionTaskNodes = new ArrayList<DirectedTreeNode>();
                
                if (isNodeRootPreConditionNode(srcNode))
                {
                    Map<String, Map<Integer, String>> pathsFromPreCondition = new HashMap<String, Map<Integer, String>>();
                    
                    Map<Integer, String> path = new HashMap<Integer, String>();
                    
                    Integer first = new Integer(1);
                    
                    path.put(first, srcNode.getId());
                    
                    //Map<Integer, Map<Integer, String>> firstPath = new HashMap<Integer, Map<Integer, String>>();
                    
                    //firstPath.put(first, path);
                    
                    pathsFromPreCondition.put(srcNode.getId(), path);
                    
                    Map<String, Map<Integer, Integer>> maxOrdersMap = new HashMap<String, Map<Integer, Integer>>();
                    
                    maxOrdersMap.put(srcNode.getId(), new HashMap<Integer, Integer>());
                    
                    recursivelyCollectChildTaskNodesWithPath(srcNode, dstNode, dstActionTaskNodes, pathsFromPreCondition, first, maxOrdersMap, edgeNodes);
                }
                else
                {
                    // Is it neded?
                    //recursivelyCollectChildTaskNodes(dstNode, dstActionTaskNodes);
                }
                
                for (DirectedTreeNode dstActionTaskNode : dstActionTaskNodes)
                {
                    ActionProcessTask dstTask = (ActionProcessTask) dstActionTaskNode.getValue();
                    
                    for (/*Iterator<*/DirectedTreeNode/*> parentIdx*/parentNode /*=*/: srcNodeParents/*.iterator(); parentIdx.hasNext();*/)
                    {
                        //DirectedTreeNode parentNode = parentIdx.next();
    
                        // reconnect
                        // parentNode.addChild(dstActionTaskNode);
    
                        // set post-evaluation in source task
                        if (parentNode.getValue() instanceof ActionProcessTask)
                        {
                            // reconnect
                            parentNode.addChild(dstActionTaskNode);
                            
                            ActionProcessTask parentTask = (ActionProcessTask) parentNode.getValue();
    
                            // init postEvaluations for parentTask - this is used to
                            // set the TRUE/FALSE for the expression
                            Map postEvaluations = parentTask.getPostEvaluations();
                            if (postEvaluations == null)
                            {
                                postEvaluations = new HashMap();
                                parentTask.setPostEvaluations(postEvaluations);
                            }
                            if (StringUtils.isEmpty((String) postEvaluations.get(dstTask.getId())))
                            {
                                postEvaluations.put(dstTask.getId(), condition);
                            }
                        }
                    }
                }
            }

            // set precondition if both src and dst are tasks
            else if ((srcNode.getValue() instanceof ActionProcessTask) && (dstNode.getValue() instanceof ActionProcessTask))
            {
                // set pre-condition in source task (NOTE WIKI model sets the
                // precondition in the current task)
                ActionProcessTask srcTask = (ActionProcessTask) srcNode.getValue();
                ActionProcessTask dstTask = (ActionProcessTask) dstNode.getValue();

                srcNode.addChild(dstNode);

                Map postConditions = srcTask.getPostConditions();
                if (postConditions == null)
                {
                    postConditions = new HashMap();
                    srcTask.setPostConditions(postConditions);
                }
                
                List conditions = (List)postConditions.get(dstTask.getId());
                
                if (conditions == null)
                {
                    conditions = new ArrayList();
                    postConditions.put(dstTask.getId(), conditions);
                }

                if (!conditions.contains(condition))
                {
                    conditions.add(condition);
                }
            }
        }
    } // setTaskCondition

    // NOTE: This is actually element_id. The real node_id is now generated and
    // overwritten in createExecuteRequestCAS()
    static String createNodeId(String docname, String modelId, String parentDoc, String processids)
    {
        String tmpParentDoc = parentDoc;
        
        if (StringUtils.isBlank(tmpParentDoc))
        {
            tmpParentDoc = "";
        }
        
        String tmpProcessIds = processids;
        
        if (StringUtils.isBlank(tmpProcessIds))
        {
            tmpProcessIds = "";
        }
        
        return (new StringBuilder()).append(tmpParentDoc.hashCode()).append(":").append(tmpProcessIds).append(":").append(docname.hashCode()).append(":").append(modelId).toString();
    } // createNodeId

    public static String createWikiId(String docname, String modelId)
    {
        return (new StringBuilder()).append(docname).append(":").append(modelId).toString();
    } // createWikiId

    public static String getWikiFromWikiId(String wikiId)
    {
        String result = null;

        if (wikiId != null && wikiId.contains(":"))
        {
            String[] values = wikiId.split(":");
            result = values[0];
        }
        return result;
    } // getWikiFromWikiId

    static String getEdgeId(String srcId, String dstId)
    {
        return srcId + "/" + dstId;
    } // getEdgeId

    static String getModelId(String nodeId, boolean isSubrunbook)
    {
        String result = "";
        
        String[] splitResult = nodeId.split(":");
        
        if (splitResult.length == 4)
        {
        	//format of node id is _id_:+<parent node id>:_id_:<node id>
        	//if this is a path into a sub-runbook the parent node id is needed
        	//in all other situations the node id is needed
        	if (isSubrunbook)
        	{
        		System.out.println("First Level Subrunbook");
	            if (StringUtils.isNotBlank(splitResult[1]) && splitResult[1].contains("+"))
	            {
	                result = StringUtils.substringAfterLast(splitResult[1], "+");
	            }
        	}
            
            if (StringUtils.isBlank(result))
            {
                result = splitResult[3];
            }
        }
        
        return result;
    } // getModelId

    public static String getActionTask(String name, String namespace)
    {
        String result = null;

        try
        {
            result = getActionTaskId(name, namespace);
        }
        catch (Exception ex)
        {
            Log.log.error(ex.getMessage(), ex);
        }

        return result;
    } // getActionTask

    public static void updateProcessParams(ActionProcessDependency processDependency, Map params)
    {
        // init process_timeout - default 10mins
        String processTimeout = params.containsKey(Constants.PROCESS_TIMEOUT) ? (String) params.get(Constants.PROCESS_TIMEOUT) : "10";
        String processLoop = params.containsKey(Constants.PROCESS_LOOP) ? (String) params.get(Constants.PROCESS_LOOP) : "false";
        String processDebug = params.containsKey(Constants.PROCESS_DEBUG) ? (String) params.get(Constants.PROCESS_DEBUG) : "false";
        String processConcurrentLimit = params.containsKey(Constants.PROCESS_CONCURRENT_LIMIT) ? (String) params.get(Constants.PROCESS_CONCURRENT_LIMIT) : "0";
        String display = params.containsKey(Constants.PROCESS_DISPLAY) ? (String) params.get(Constants.PROCESS_DISPLAY) : "";

        // init defaults
        Map newParams = new HashMap();
        newParams.put(Constants.PROCESS_TIMEOUT, processTimeout);
        newParams.put(Constants.PROCESS_LOOP, processLoop);
        newParams.put(Constants.PROCESS_DEBUG, processDebug);
        newParams.put(Constants.PROCESS_CONCURRENT_LIMIT, processConcurrentLimit);
        newParams.put(Constants.PROCESS_DISPLAY, display);

        // override defaults with start task params
        ActionProcessTask startTask = (ActionProcessTask) processDependency.getModel().getStartNode().getValue();
        newParams.putAll(startTask.getParams());

        // override from params
        newParams.putAll(params);

        // clear and set params
        params.clear();

        // add all parameters but check for null values
        for (Iterator i = newParams.entrySet().iterator(); i.hasNext();)
        {
            Map.Entry entry = (Map.Entry) i.next();

            Object value = entry.getValue();
            if (value != null)
            {
                params.put(entry.getKey(), value);
            }
        }
        // params.putAll(newParams);
    } // updateProcessParams

    public static String createExecuteProcessRequestCAS(String wiki, ActionProcessDependency processDependency, String problem_sid, String process_sid, Map params, String user, boolean isEvent, ArrayList start, ActionProcessTaskRequest eventTask) throws Exception
    {
        ExecutionTimeUtil.log((String) params.get("RESOLVE.JOB_ID"), null, null, ExecutionStep.METHOD_EXECUTION, ExecutionStepPhase.START);

        String newProcessNumber = null;
        if (processDependency != null && (StringUtils.isEmpty(process_sid) || process_sid.equalsIgnoreCase("NEW")))
        {
            process_sid = null;
            newProcessNumber = SequenceUtils.getNextSequence(Constants.PERSISTENCE_SEQ_PROC);
        }

        if (StringUtils.isNotBlank(newProcessNumber)) {
        	params.put(Constants.EXECUTE_PROCESS_NUMBER, newProcessNumber);
        }
        
        // check for process model
        if (processDependency != null)
        {
            String rowKey = ServiceWorksheet.createExecuteProcessRequestCAS(wiki, newProcessNumber, problem_sid, process_sid, params, user, isEvent);

            process_sid = rowKey;

            // init modelStates
            createModelStates(process_sid);
            //            Map<String, Object> map = createModelStates(process_sid);
            //            createModelStates(problem_sid, map);

            // allocate process map
            
            int processTimeout = 10;
            if(params.get(Constants.EXECUTE_PROCESS_TIMEOUT) instanceof String) {
                processTimeout = Integer.parseInt((String) params.get(Constants.EXECUTE_PROCESS_TIMEOUT)); // optional
            } else if (params.get(Constants.EXECUTE_PROCESS_TIMEOUT) instanceof Integer) {
                processTimeout = (int) params.get(Constants.EXECUTE_PROCESS_TIMEOUT);
            }
            ProcessContext.createProcessMap(process_sid, processTimeout);

            // init worksheet execute_sid => task def
        	createExecuteProcessDependencyNodes(processDependency, problem_sid, params, user, process_sid, isEvent, start, eventTask);
        }
        
        return process_sid;
    } // createExecuteProcessRequestCAS

    private static ConcurrentHashMap<String,ConcurrentHashMap<String,Boolean>> matchingEventCache = new ConcurrentHashMap<String,ConcurrentHashMap<String,Boolean>>(); 
    
    static boolean isMatchingEvent(String eventVal, String eventid, String processid, String problemid)
    {
        boolean result = false;
        if (StringUtils.isEmpty(eventVal))
        {
            return false;
        }
        
        if (StringUtils.equalsIgnoreCase(eventVal, eventid))
        {
            return true;
        }
        
        String eventKey = processid + problemid;
        String eventIdKey = eventVal + eventid;
        if (!matchingEventCache.containsKey(eventKey))
        {
            matchingEventCache.putIfAbsent(eventKey, new ConcurrentHashMap<String,Boolean>());
        }
        
        ConcurrentHashMap<String,Boolean> matchingMap = matchingEventCache.get(eventKey);
        Boolean matchingResult = matchingMap.get(eventIdKey);
        if (matchingResult!=null)
        {
            return matchingResult.booleanValue();
        }
        
        List<ExecuteState> rows = null;
        try
        {
            QueryDTO queryDTO = new QueryDTO();
            queryDTO.addFilterItem(new QueryFilter("processId", QueryFilter.EQUALS, processid))
                .addFilterItem(new QueryFilter("problemId", QueryFilter.EQUALS, problemid));
            
            if (StringUtils.isNotEmpty(eventid))
            {
                queryDTO.addFilterItem(new QueryFilter("targetAddress", QueryFilter.EQUALS, eventid));
            }

            rows = WorksheetSearchAPI.searchExecuteStates(queryDTO, "system").getRecords();
        }
        catch (Exception e)
        {
            Log.log.error("Error getting rows for event, eventid: " + eventid+ " , problemid: " + problemid, e);
        }

        if (rows!=null && !rows.isEmpty())
        {
            ExecuteState row = rows.get(0);
            String UParams = row.getParams();

            Map persistedParams = null;
            // params
            if (UParams != null)
            {
                persistedParams = (Map) StringUtils.stringToObj(UParams);
            }
        
            // flows
            Map flows = null;
            String UFlows = row.getFlows();  
            if (UFlows != null)
            {
                flows = (Map) StringUtils.stringToObj(UFlows);
                if (flows == null)
                {
                    flows = new HashMap();
                }
            }
        
            // inputs
            Map inputs = null;
            String UInputs = row.getInputs(); 
            if (UInputs != null)
            {
                inputs = (Map) StringUtils.stringToObj(UInputs);
                if (inputs == null)
                {
                    inputs = new HashMap();
                }
            }

            try
            {
                Map<String, Object> wsData = WorksheetSearchAPI.getWorksheetData(problemid, Collections.EMPTY_LIST, "system");

//              String updatedEvtVal = com.resolve.util.VariableUtil.replaceVarsString(eventVal, inputs, new HashMap<String,Object>(), persistedParams, flows, new XDoc()); //
              String updatedEvtVal = com.resolve.services.util.VariableUtil.replaceVarsString(eventVal, wsData, inputs, new HashMap<String,Object>(), persistedParams, flows); 
                result = StringUtils.equalsIgnoreCase(updatedEvtVal, eventid);
                matchingMap.put(eventIdKey, result);
            }
            catch (Exception ex)
            {
                Log.log.error(ex,ex);
                throw new RuntimeException(ex);
            }
        }
        
        return result;
    }

    static void createExecuteProcessDependencyNodes(ActionProcessDependency processDependency, String problem_sid, Map params, String user, String process_sid, boolean isEvent, ArrayList start, ActionProcessTaskRequest eventTask) throws Exception
    {
        ExecutionTimeUtil.log((String) params.get("RESOLVE.JOB_ID"), null, null, ExecutionStep.METHOD_EXECUTION, ExecutionStepPhase.START);
        // init execute_sid => task def
        Map exec2task = (Map) getModelStates(process_sid).get(EXECID_TASK);
        if (exec2task == null)
        {
            exec2task = new HashMap();
            getModelStates(process_sid).put(EXECID_TASK, exec2task);
        }

        // init task def => execute_sid
        Map task2exec = (Map) getModelStates(process_sid).get(TASK_EXECID);
        if (task2exec == null)
        {
            task2exec = new HashMap();
            getModelStates(process_sid).put(TASK_EXECID, task2exec);
        }

        // init PROCESSID -> EVENTID -> EXECUTEID
        Map<String, List<String>> eventMap = EventHandler.getEventMap(process_sid);
        if (eventMap == null)
        {
            eventMap = EventHandler.newEventMap(process_sid);
        }

        String processNum = ServiceWorksheet.getProcessNumberByProcessId(process_sid);
        String problemNum = ServiceWorksheet.getProblemNumberByProblemId(problem_sid);

        String reference = (String) params.get(Constants.EXECUTE_REFERENCE); // optional
        boolean isAbort = false;

        String wiki = (String) params.get(Constants.EXECUTE_WIKI); // required
        if (wiki == null)
        {
            wiki = "";
        }

        // update wiki docname
        if (wiki.startsWith(Constants.EXECUTE_ABORTWIKI))
        {
            wiki = wiki.substring(Constants.EXECUTE_ABORTWIKI.length());
            isAbort = true;
        }

        // get targets
        Targets targets = ProcessExecution.initTargets(params);

        Map<String, ActionProcessTaskRequest> exec2taskReq = new HashMap<String, ActionProcessTaskRequest>();
        
        Pair<String, Integer> atSeqPair = SequenceUtils.getNextXSequenceNumbers(Constants.PERSISTENCE_SEQ_EXEC, processDependency.getTasks().size());
        
        int atSeqNum = atSeqPair != null ? atSeqPair.getRight().intValue() : 0;
        
        // create actiontask execute requests for processDependency
        for (Iterator i = processDependency.getTasks().iterator(); i.hasNext();)
        {
            ActionProcessTask task = (ActionProcessTask) i.next();

            // create task request
            String atNumber = atSeqPair.getLeft() + Integer.toString(atSeqNum);
            String execute_sid = createExecuteRequestCASP3(task.getTaskSid(), params, task.getParams(), task.getOutputs(), 
            											   wiki, process_sid, null, problem_sid, task.getId(), task.getLabel(), 
            											   user, isEvent, task.name, processNum, problemNum, 
            											   reference, isAbort, start, eventTask, targets, exec2taskReq,
            											   atNumber);
            
            atSeqNum++;
            
            // add to task lookup map
            exec2task.put(execute_sid, task);

            // generate task dependency input
            task2exec.put(task, execute_sid);

            // add eventid -> executeid
            if (StringUtils.isNotEmpty(task.getEventId()))
            {
                String evtId = task.getEventId();
                if (eventMap.get(evtId)==null)
                {
                    eventMap.put(evtId, new ArrayList<String>());
                }
                eventMap.get(task.getEventId()).add(execute_sid);
            }
        }

        ProcessRequest.getModelStates(process_sid).put(ProcessRequest.EXECID_TASKREQ, exec2taskReq);

        ExecutionTimeUtil.log((String) params.get("RESOLVE.JOB_ID"), null, null, ExecutionStep.METHOD_EXECUTION, ExecutionStepPhase.START);
        
    } // createExecProcessDependencyNodes

    public static Map<String, ActionProcessTaskRequest> getExec2TaskReqMap(String processid)
    {
    	 Map<String, Object> modelMap = null;
    	 
    	 try {
    		 modelMap = ProcessRequest.getModelStates(processid);
    	 } catch (Throwable t) {
    		 if (StringUtils.isNotBlank(t.getMessage()) && 
    			 !t.getMessage().startsWith("Missing modelStates for processid")) {
    			 throw t;
    		 }
    	 }
    	 
    	 return modelMap != null ? 
    			(Map<String, ActionProcessTaskRequest>) modelMap.get(ProcessRequest.EXECID_TASKREQ) : null;
    }

    static class ExecuteRequest implements Comparable<ExecuteRequest>
    {
        public String KEY;
        public Long sys_created_on;
        public String u_number;
        public String u_process;
        public String u_problem;
        public String u_node_id;

        @Override
        public int compareTo(ExecuteRequest o)
        {
            // this makes sure it's sorted in descending order
            return o.sys_created_on.compareTo(this.sys_created_on);
        }
    }

    public static String createExecuteRequestCASP3(String task_sid, Map params, Map modelParams, Map outputs, String wiki, 
    											   String process_sid, String execute_sid, String problem_sid, String node_id, 
    											   String node_label, String user, boolean isEvent, 
    											   String actionName, String processNum, String problemNum, String reference, 
    											   boolean isAbort, ArrayList start, ActionProcessTaskRequest eventTask, 
    											   Targets targets, Map<String, ActionProcessTaskRequest> exec2taskReq,
    											   String atNumber)
    {
        ExecutionTimeUtil.log((String) params.get("RESOLVE.JOB_ID"), null, null, ExecutionStep.METHOD_EXECUTION, ExecutionStepPhase.START);
    	
        String execNumber = null;
        if (wiki == null)
        {
            wiki = "";
        }

        String event_id = (String) modelParams.get(Constants.EVENT_EVENTID);

        // create result record if not specified
        if (StringUtils.isEmpty(problem_sid) || problem_sid.equals("-1"))
        {
            throw new RuntimeException("Missing worksheet id");
        }

        //////////////////////////////////////////////////////////////////////////////////////////        
        //        String executeid = CassandraUtil.getNewRowKey();
        
        String oldId = "";
        String elementId = "";
        if (StringUtils.isNotEmpty(node_id))
        {
            oldId = wiki + "::" + node_id.substring(node_id.lastIndexOf(":") + 1);
            elementId = node_id;
        }
        String nodeLabel = node_label==null? "" : node_label;
        
        String executeid = oldId + Constants.EXECUTE_REQUEST_NODEID_DELIMITER + elementId + Constants.EXECUTE_REQUEST_NODEID_DELIMITER + nodeLabel;

        ActionProcessTaskRequest req = new ActionProcessTaskRequest();
        req.setProcess(process_sid);
        req.setProblem(problem_sid);
        req.setReference(reference);
        req.setUserid(user);
        req.setExecute(executeid);
        req.setTask(task_sid);

        //Do we need a request number?
        req.setNumber(atNumber);

        req.setParams(null);
        if (event_id != null)
        {
            req.setEvent(event_id);
        }

        // get taskname
        if (StringUtils.isNotEmpty(task_sid))
        {
        	// Read from cache
            
        	ResolveActionTaskVO task = null;
        	
        	task = ProcessExecution.actionTaskCache.get(task_sid);
           
            if (task == null) {
            	task = ServiceHibernate.getActionTaskWithReferencesExceptGraph(task_sid, null, "system");
                        
	            if (task != null) {
	                ProcessExecution.actionTaskCache.putIfAbsent(task_sid, task);
	            }
            }
            
            String taskname = task.getUName();
            
            req.setTaskname(task.getUName());
            req.setTasknamespace(task.getUNamespace());
            req.setTaskhidden(task.getUIsHidden() == null ? "FALSE" : "" + task.getUIsHidden());
            req.setTasktags(task.getTagsAsCSV());

            if (taskname.equalsIgnoreCase("START"))
            {
                if (!isAbort)
                {
                    req.setModelType(Constants.MODEL_TYPE_MAIN);
                }
                else
                {
                    req.setModelType(Constants.MODEL_TYPE_ABORT);
                }
            }
        }

        if (isEvent == false)
        {
            // check whether to add task to start list
            if (start.isEmpty() && req.getTaskname().equalsIgnoreCase("start"))
            {
                start.add(req);
            }
        }
        else
        {
            if (req.getTaskname().equalsIgnoreCase("event") && req.getEvent().equalsIgnoreCase(event_id))
            {
                eventTask = req;
            }
        }

        try
        {
            req.setTargets(targets);
        }
        catch (Exception ex)
        {
            throw new RuntimeException("Failed to set targets on ActionProcessTaskRequest: targets=" + targets, ex);
        }

        // add task
        //tasks.add(req);
        exec2taskReq.put(executeid, req);
        /////////////////////////////////////////////////////////

        // copy required params from task invocations and add to execute request
        addExecuteTaskParams(task_sid, executeid, process_sid, params, modelParams, outputs);

        ExecutionTimeUtil.log((String) params.get("RESOLVE.JOB_ID"), null, null, ExecutionStep.METHOD_EXECUTION, ExecutionStepPhase.END);

        return executeid;
    } // createExecuteRequestCASP3

    public static String createExecuteRequest(String task_sid, Map params, Map modelParams, String wiki, String process_sid, String execute_sid, String problem_sid, String node_id, String node_label, String user, boolean isEvent, Map<String, ExecReqPair> rqstMap, String actionName, String processNum, String problemNum)
    {
        // create result record if not specified
        if (StringUtils.isEmpty(problem_sid) || problem_sid.equals("-1"))
        {
            throw new RuntimeException("Missing worksheet id");
        }

        String rowKey = StringUtils.isEmpty(execute_sid) ? SearchUtils.getNewRowKey() : execute_sid;

        execute_sid = rowKey;

        // copy required params from task invocations and add to execute request
        addExecuteTaskParams(task_sid, execute_sid, process_sid, params, modelParams, null);

        return execute_sid;
    } // createExecuteRequestCAS

    /**
     * Copy parameters from action invocation and ActionTask execute request
     * 
     * @param request_sid
     *            - sys_id of the new action execute request
     * @param task_sid
     *            - sys_id of the actiontask
     */
    static void addExecuteTaskParams(String task_sid, String request_sid, String process_sid, Map params, Map modelParams, Map outputs)
    {
    	long tmp = 0;
        ExecutionTimeUtil.log((String) params.get("RESOLVE.JOB_ID"), null, null, ExecutionStep.METHOD_EXECUTION, ExecutionStepPhase.START);
        if (StringUtils.isNotBlank(task_sid))
        {
            try
            {
                List<ExecuteParameter> executeParameters = new ArrayList<ExecuteParameter>();

                Collection<ResolveActionParameterVO> parameters = parameterCache.get(task_sid);
                if (parameters == null)
                {
                    parameters = ServiceHibernate.getParamsForActionTaskSysId(task_sid, "system");
                    if (parameters != null)
                    {
                        parameterCache.put(task_sid, parameters);
                    }
                }
                tmp=System.currentTimeMillis();

                Map remainingModelParams = new HashMap(modelParams);
                Map remainingModelOutputs = new HashMap();
                if(outputs!=null) remainingModelOutputs=new HashMap(outputs);
 
                for (ResolveActionParameterVO row : parameters)
                {
                    String nameLocal = "";
                    String type = row.getUType();

                    // update type to INPUT if not defined
                    if (StringUtils.isEmpty(row.getUType()))
                    {
                        type = "INPUT";
                        nameLocal = row.getUName();
                    }
                    else if (type.equals("OUTPUT"))
                    {
                        nameLocal = row.getUName();
                    }
                    else
                    {
                        nameLocal = row.getUName();
                    }

                    // update value
                    String value = null;
                    
                    if (type.equals("OUTPUT")) {
                        // the old data format
                        if(!remainingModelOutputs.containsKey(nameLocal) && remainingModelOutputs.containsKey(nameLocal.concat(":"))) {
                            nameLocal = nameLocal.concat(":");
                        }
                        value=updateExecuteTaskParamValue(nameLocal, row.getUDefaultValue(), params, remainingModelOutputs, row.getUProtected().booleanValue());
                        // try the old data format
                     /*   if(StringUtils.isEmpty(value)) {
                            nameLocal = row.getUName() + ":";
                        }*/
                    }
                    else value=updateExecuteTaskParamValue(nameLocal, row.getUDefaultValue(), params, remainingModelParams, row.getUProtected().booleanValue());
                    // create new execute request parameter
                    ExecuteParameter reqParam = new ExecuteParameter();
                    reqParam.setName(row.getUName());
                    reqParam.setType(type);
                    reqParam.setValue(value);
                    reqParam.setPrefix(row.getUPrefix());
                    reqParam.setPrefixSeparator(row.getUPrefixSeparator());
                    reqParam.setEncrypt(row.getUEncrypt());
                    executeParameters.add(reqParam);
                }
                
                if (remainingModelParams.size() > 0)
                {
                    for (Object key : remainingModelParams.keySet())
                    {
                        // create new execute request parameter
                        ExecuteParameter reqParam = new ExecuteParameter();
                        reqParam.setName(key.toString());
                        reqParam.setType("INPUT");
                        reqParam.setValue((String) remainingModelParams.get(key));
                        reqParam.setPrefix("");
                        reqParam.setPrefixSeparator("");
                        reqParam.setEncrypt(false);
                        executeParameters.add(reqParam);
                    }
                }

                // sync hazelcast parameters after db connection
                for (ExecuteParameter reqParam : executeParameters)
                {
                    ProcessExecution.executeParameterStateCache.setParameter(process_sid, request_sid, reqParam);
                }
            }
            catch (Throwable e)
            {
                Log.log.error(e.getMessage(), e);
            }
        }
        
        ExecutionTimeUtil.log((String) params.get("RESOLVE.JOB_ID"), null, null, ExecutionStep.METHOD_EXECUTION, ExecutionStepPhase.END);
    } // addExecuteTaskParams

    static String updateExecuteTaskParamValue(String name, String defaultValue, Map params, Map modelParams, boolean protectedInput)
    {
        String result = "";

        try
        {
            // check if name exists in modelParams
            if (modelParams.containsKey(name))
            {
                String key = name;
                result = (String) modelParams.get(name);
                modelParams.remove(key);
            }
            else if (modelParams.containsKey(name.toUpperCase()))
            {
                String key = name.toUpperCase();
                result = (String) modelParams.get(key);
                modelParams.remove(key);
            }
            else if (modelParams.containsKey(name.toLowerCase()))
            {
                String key = name.toLowerCase();
                result = (String) modelParams.get(key);
                modelParams.remove(key);
            }

            if (StringUtils.isEmpty(result))
            {
                // check if name = $PARAM{NAME}, update name with new name
                if (StringUtils.isNotBlank(defaultValue))
                {
                    result = defaultValue;
                }

                boolean addParamsToInputs = StringUtils.isTrue(PropertiesUtil.getPropertyString("automation.inputs.read.from.params"));
                
                if (StringUtils.isEmpty(result) && addParamsToInputs)
                {
                    // check if name exists in params
                    if (params != null && params.containsKey(name))
                    {
                        result = (String) params.get(name);
                    }
                    else if (params != null && params.containsKey(name.toUpperCase()))
                    {
                        result = (String) params.get(name.toUpperCase());
                    }
                    else if (params != null && params.containsKey(name.toLowerCase()))
                    {
                        result = (String) params.get(name.toLowerCase());
                    }
                }
            }
        }
        catch (Exception e)
        {
            Log.log.error(e.getMessage(), e);
        }

        // For protected input variables
        if (protectedInput && StringUtils.isNotBlank(defaultValue))
        {
            result = defaultValue;
        }
        
        return result;
    } // updateExecuteTaskParamValue

    static boolean getDependencyCompletion(String conditions)
    {
        boolean result = true;

        if (conditions != null)
        {
            String[] def = null;
            if (conditions.indexOf("==") > 0)
            {
                def = conditions.split("==");
            }
            else
            {
                def = conditions.split("=");
            }
            if (def.length == 2)
            {
                String completion = def[1].trim();

                if (completion.equalsIgnoreCase(Constants.ASSESS_COMPLETION_TRUE))
                {
                    result = true;
                }
                else if (completion.equalsIgnoreCase(Constants.ASSESS_COMPLETION_FALSE))
                {
                    result = false;
                }
                // else leave null
            }
        }

        return result;
    } // getDependencyCompletion

    static String getDependencyCondition(String conditions)
    {
        String result = null;

        if (conditions != null)
        {
            String[] def = null;
            if (conditions.indexOf("==") > 0)
            {
                def = conditions.split("==");
            }
            else
            {
                def = conditions.split("=");
            }
            if (def.length == 2)
            {
                String condition = def[1].trim();

                if (condition.equalsIgnoreCase(Constants.ASSESS_CONDITION_GOOD))
                {
                    result = Constants.ASSESS_CONDITION_GOOD;
                }
                else if (condition.equalsIgnoreCase(Constants.ASSESS_CONDITION_BAD))
                {
                    result = Constants.ASSESS_CONDITION_BAD;
                }
                // else leave null
            }
        }
        return result;
    } // getDependencyCondition

    static String getDependencySeverity(String conditions)
    {
        String result = null;

        if (conditions != null)
        {
            if (conditions.indexOf("==") != -1 || conditions.indexOf("=") != -1)
            {
                String[] def = null;
                if (conditions.indexOf("==") > 0)
                {
                    def = conditions.split("==");
                }
                else
                {
                    def = conditions.split("=");
                }
                if (def.length == 2)
                {
                    String severity = def[1].trim();

                    if (severity.equalsIgnoreCase(Constants.ASSESS_SEVERITY_CRITICAL))
                    {
                        result = Constants.ASSESS_SEVERITY_CRITICAL;
                    }
                    else if (severity.equalsIgnoreCase(Constants.ASSESS_SEVERITY_SEVERE))
                    {
                        result = Constants.ASSESS_SEVERITY_SEVERE;
                    }
                    else if (severity.equalsIgnoreCase(Constants.ASSESS_SEVERITY_WARNING))
                    {
                        result = Constants.ASSESS_SEVERITY_WARNING;
                    }
                    else if (severity.equalsIgnoreCase(Constants.ASSESS_SEVERITY_GOOD))
                    {
                        result = Constants.ASSESS_SEVERITY_GOOD;
                    }
                    else if (severity.equalsIgnoreCase(Constants.ASSESS_SEVERITY_UNKNOWN))
                    {
                        result = Constants.ASSESS_SEVERITY_UNKNOWN;
                    }
                }
            }
            else if (conditions.indexOf("<=") != -1)
            {
                String[] def = conditions.split("<=");
                if (def.length == 2)
                {
                    String severity = def[1].trim();

                    if (severity.equalsIgnoreCase(Constants.ASSESS_SEVERITY_CRITICAL))
                    {
                        result = Constants.ASSESS_SEVERITY_LE_CRITICAL;
                    }
                    else if (severity.equalsIgnoreCase(Constants.ASSESS_SEVERITY_SEVERE))
                    {
                        result = Constants.ASSESS_SEVERITY_LE_SEVERE;
                    }
                    else if (severity.equalsIgnoreCase(Constants.ASSESS_SEVERITY_WARNING))
                    {
                        result = Constants.ASSESS_SEVERITY_LE_WARNING;
                    }
                }
            }
            else if (conditions.indexOf("<") != -1)
            {
                String[] def = conditions.split("<");
                if (def.length == 2)
                {
                    String severity = def[1].trim();

                    if (severity.equalsIgnoreCase(Constants.ASSESS_SEVERITY_CRITICAL))
                    {
                        result = Constants.ASSESS_SEVERITY_LT_CRITICAL;
                    }
                    else if (severity.equalsIgnoreCase(Constants.ASSESS_SEVERITY_SEVERE))
                    {
                        result = Constants.ASSESS_SEVERITY_LT_SEVERE;
                    }
                    else if (severity.equalsIgnoreCase(Constants.ASSESS_SEVERITY_WARNING))
                    {
                        result = Constants.ASSESS_SEVERITY_LT_WARNING;
                    }
                }
            }
            else if (conditions.indexOf(">=") != -1)
            {
                String[] def = conditions.split(">=");
                if (def.length == 2)
                {
                    String severity = def[1].trim();

                    if (severity.equalsIgnoreCase(Constants.ASSESS_SEVERITY_SEVERE))
                    {
                        result = Constants.ASSESS_SEVERITY_GE_SEVERE;
                    }
                    else if (severity.equalsIgnoreCase(Constants.ASSESS_SEVERITY_WARNING))
                    {
                        result = Constants.ASSESS_SEVERITY_GE_WARNING;
                    }
                    else if (severity.equalsIgnoreCase(Constants.ASSESS_SEVERITY_GOOD))
                    {
                        result = Constants.ASSESS_SEVERITY_GE_GOOD;
                    }
                }
            }
            else if (conditions.indexOf(">") != -1)
            {
                String[] def = conditions.split(">");
                if (def.length == 2)
                {
                    String severity = def[1].trim();

                    if (severity.equalsIgnoreCase(Constants.ASSESS_SEVERITY_SEVERE))
                    {
                        result = Constants.ASSESS_SEVERITY_GT_SEVERE;
                    }
                    else if (severity.equalsIgnoreCase(Constants.ASSESS_SEVERITY_WARNING))
                    {
                        result = Constants.ASSESS_SEVERITY_GT_WARNING;
                    }
                    else if (severity.equalsIgnoreCase(Constants.ASSESS_SEVERITY_GOOD))
                    {
                        result = Constants.ASSESS_SEVERITY_GT_GOOD;
                    }
                }
            }
        }
        return result;
    } // getDependencySeverity

    static String getDependencyMerge(String conditions)
    {
        String result = null;

        if (conditions != null)
        {
            String[] def = null;
            if (conditions.indexOf("==") > 0)
            {
                def = conditions.split("==");
            }
            else
            {
                def = conditions.split("=");
            }
            if (def.length == 2)
            {
                String merge = def[1].trim();

                if (merge.equalsIgnoreCase(Constants.ASSESS_MERGE_ALL))
                {
                    result = Constants.ASSESS_MERGE_ALL;
                }
                else if (merge.equalsIgnoreCase(Constants.ASSESS_MERGE_ANY))
                {
                    result = Constants.ASSESS_MERGE_ANY;
                }
                else if (merge.equalsIgnoreCase(Constants.ASSESS_MERGE_TARGETS))
                {
                    result = Constants.ASSESS_MERGE_TARGETS;
                }
                // else leave null
            }
        }
        return result;
    } // getDependencyMerge

    static boolean checkProcessPermission(String wiki, ActionProcessDependency processDependency, String sysUser, String jobId)
    {
        ExecutionTimeUtil.log(jobId, null, null, ExecutionStep.METHOD_EXECUTION, ExecutionStepPhase.START);
        
        boolean result = false;
        
        // TODO - add username validation
        if (sysUser.equalsIgnoreCase("system") || sysUser.equalsIgnoreCase("admin") || sysUser.equalsIgnoreCase("resolve.maint"))
        {
            result = true;
        }
        else
        {
            if (processDependency != null)
            {
                Set<String> userRoles = Collections.unmodifiableSet(UserUtils.getUserRoles(sysUser));
                
                // check wiki permission
                if (!UserUtils.checkWikiPermission(wiki, sysUser, userRoles, "execute"))
                {
                    Log.log.warn("FAILED: wiki permission validation: " + wiki);
                    result = false;
                }
                
                else if(!result) {
                	final Map<String,ActionProcessTask> uniqueMap = processDependency.getTasks().stream().collect(Collectors.<ActionProcessTask,String,ActionProcessTask>toMap(task->task.getTaskSid(),task->task,(sid1,sid2)->{return sid1;}));
                	// returns false if all permissions are true
                	result = !uniqueMap.values().parallelStream().anyMatch(task -> !checkPermissions(task,task.getId(), sysUser, userRoles)); 
                }
            }
        }


        ExecutionTimeUtil.log(jobId, null, null, ExecutionStep.METHOD_EXECUTION, ExecutionStepPhase.END);
        
        return result;
    } // checkProcessPermission
    
    private static boolean checkPermissions(ActionProcessTask task, String id, String user, Set<String> userRoles)
    {
    
    	Boolean result = true;
    	// check wiki
        if (!UserUtils.checkWikiPermission(task.getWiki(), user, userRoles, "execute"))
        {
        	result = false;
            Log.log.warn("FAILED: wiki permission validation: " + task.getWiki());
        }
        

        // check task
        if (!UserUtils.checkTaskPermission(task.task_sid, userRoles))
        {
        	result = false;
            Log.log.warn("FAILED: task permission validation: " + task.getName() + "#" + task.getNamespace());
        }
           
        return result;
    }
    
    /**
     * Iterates throught list of ActionTask requests and determine if the user
     * is permitted to execute each ActionTask
     * 
     * @param process_sid
     *            - sys_id of the ActionProcess request
     * @throws Exception
     *             - If user does not have permissions, ActionTask is
     *             deactivated or missing
     */
/*    static boolean checkProcessPermission(String process_sid, String sysUser) throws Exception
    {
        boolean result = false;

        // TODO - add username validation
        if (sysUser.equalsIgnoreCase("system") || sysUser.equalsIgnoreCase("admin") || sysUser.equalsIgnoreCase("resolve.maint"))
        {
            result = true;
        }
        else
        {
            try
            {

                Set<ResolveExecuteRequestCAS> executeRequests = CassandraDaoFactory.getResolveExecuteRequestDAO().findByProcessId(process_sid);
                Set<String> userRoles = UserUtils.getUserRoles(sysUser);

                for (ResolveExecuteRequestCAS executeRequest : executeRequests)
                {
                    if (!UserUtils.checkTaskPermission(executeRequest.getTask(), userRoles))
                    {
                        throw new Exception("FAILED: task permission validation");
                    }
                }

                result = true;
            }
            catch (Throwable e)
            {
                Log.log.warn(e.getMessage(), e);

                result = false;
            }
        }

        return result;
    } // checkProcessPermission

*/    
    static void cleanupRunningModel(String processid, String problemid)
    {
        runningModels.remove(processid);
        modelStates.remove(processid);
        modelStates.remove(problemid);
    } // cleanupRunningModels

    public static String getEventExecuteId(String processid, String eventid)
    {
        String result = null;

        Map processStates = (Map) modelStates.get(processid);
        if (processStates != null)
        {
            Map<String, String> eventid2execid = (Map) processStates.get(EVENTID_EXECID);
            if (eventid2execid != null)
            {
                result = eventid2execid.get(eventid.toUpperCase());
            }
        }

        return result;
    } // getEventExecuteId

    /**
     * Adds the specified worksheet display to the WSDATA to allow users to "view" the appropriate page for the data.
     * The displayType is either a wiki full name or "<type>:<value>". Type can be "wiki" or "decisiontree". Value is
     * the full name of the wiki document.
     * 
     * @param problemid
     * @param displayTypeValue
     */
    public static void updateWorksheetDisplay(String problemid, String displayTypeValue)
    {
        String type;
        String value;

        if (StringUtils.isNotEmpty(displayTypeValue))
        {
            int pos = displayTypeValue.indexOf(":");
            if (pos > 0)
            {
                type = displayTypeValue.substring(0, pos);
                value = displayTypeValue.substring(pos + 1, displayTypeValue.length());
            }
            else
            {
                type = "wiki";
                value = displayTypeValue;
            }

            // get dt_dtList from WSDATA
            WSDataMap wsData = ServiceWorksheet.getWorksheetWSDATA(problemid);

            JSONArray dtList = null;
            String dtListStr = (String) wsData.get(Constants.WSDATA_DT_DTLIST);
            if (StringUtils.isNotEmpty(dtListStr))
            {
                dtList = StringUtils.stringToJSONArray(dtListStr);
            }
            else
            {
                dtList = new JSONArray();
            }

            // append display entry
            JSONObject entry = new JSONObject();
            entry.put("name", value);
            entry.put("type", type);
            entry.put("date", "" + System.currentTimeMillis());
            dtList.add(entry);
            
            // update wsdata
            wsData.put(Constants.WSDATA_DT_DTLIST, StringUtils.jsonArrayToString(dtList));
            ServiceWorksheet.saveWorksheetWSDATA(problemid, wsData, "system");
        }
        
    } // updateWorksheetDisplay

    
} // ProcessRequest

class ExecReqPair
{
    Long created;
    String key;

    ExecReqPair(long timestamp, String rowKey)
    {
        created = timestamp;
        key = rowKey;
    }
}

class ModelSubprocess
{
    String id;
    String value;
    String inputParameters;

    public ModelSubprocess(String id)
    {
        this.id = id;
    }

    public String getId()
    {
        return id;
    }

    public void setId(String id)
    {
        this.id = id;
    }

    public String getValue()
    {
        return value;
    }

    public void setValue(String value)
    {
        this.value = value;
    }

    public String getInputParameters()
    {
        return inputParameters;
    }

    public void setInputParameters(String inputParameters)
    {
        this.inputParameters = inputParameters;
    }

} // ModelSubprocess

class ModelPrecondition
{
    static String DEFAULT_PRECONDITION_MERGE = "merge = " + Constants.ASSESS_MERGE_ALL;
    
    String id;
    String value;
    String merge;

    public ModelPrecondition(String id)
    {
        this.id = id;
    }

    public String getId()
    {
        return id;
    }

    public void setId(String id)
    {
        this.id = id;
    }

    public String getValue()
    {
        return value;
    }

    public void setValue(String value)
    {
        this.value = value;
    }
    
    public void setMerge(String merge)
    {
        this.merge = merge;
    }

    public String getMerge()
    {
        return merge;
    }
} // ModelPrecondition
