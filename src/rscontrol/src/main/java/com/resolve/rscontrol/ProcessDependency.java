/******************************************************************************
* (C) Copyright 2014
*
* Resolve Systems, LLC
*
* All rights reserved
* This software is distributed under license and may not be used, copied,
* modified, or distributed without the express written permission of
* Resolve Systems, LLC.
*
******************************************************************************/

package com.resolve.rscontrol;

import java.util.Iterator;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.PriorityBlockingQueue;

import com.resolve.util.Log;
import com.resolve.util.StringUtils;

public class ProcessDependency 
{
    // PROCESS_SID => EXECUTEID => List of dependent EXECUTEID to be evaluated
    Map<String,Boolean> initCompleted = new ConcurrentHashMap<String,Boolean>();
    
    public boolean isInitCompleted(String processid)
    {
        boolean result = false;
        
        if (!StringUtils.isEmpty(processid))
        {
	    	result = initCompleted.containsKey(processid);
        }
    	return result;
    } // isInitCompleted
    
    public void setInitCompleted(String processid)
    {
        if (!StringUtils.isEmpty(processid))
        {
            if (Log.log.isDebugEnabled()) {
                Log.log.debug("startProcess setInitCompleted processid: "+processid); 
            }
	        initCompleted.put(processid, true);
        }
    } // setInitCompleted
    
    public int getProcessCount()
    {
    	return ProcessRequest.runningModels.size();
    } // getProcessCount
    
    static private class ProcessStamp implements Comparable<ProcessStamp>
    {
        String processid;
        long timeStamp;
        
        @Override
        public int compareTo(ProcessStamp o)
        {
            return Long.signum(timeStamp - o.timeStamp);
        }
    }
    
    static private PriorityBlockingQueue<ProcessStamp> removedProcesses = new PriorityBlockingQueue<ProcessStamp>();
    
    static {
        Thread cleanUp = new Thread()
        {
            public void run()
            {
                while (true)
                {
                    long now = System.currentTimeMillis();
                    ProcessStamp p = removedProcesses.peek();
                    long count = 0;
                    while (p!=null)
                    {
                        if ( (now-p.timeStamp) < 12*60*1000 ) // delay for 12 minutes
                        {
                            break;
                        }
                        removeProcessInternal(p.processid);
                        count++;
                        removedProcesses.remove(p);
                        p = removedProcesses.peek();                        
                    }
                    if (Log.log.isDebugEnabled()) {
                        Log.log.debug("Cleanup thread removed " + count + " processid. Queue size: " + removedProcesses.size());
                    }
                    try
                    {
                        Thread.sleep(15*1000); //Sleep for 15 seconds
                    }
                    catch (Exception ex) 
                    {
                        Log.log.error(ex.getMessage(), ex);
                    }
                }
            }
        };
        cleanUp.start();
    }
    
    public void removeProcess(String processid, boolean isEvent)
    {
        ProcessStamp ps = new ProcessStamp();
        ps.processid = processid;
        ps.timeStamp = System.currentTimeMillis();
        if (isEvent)
        {
            removeProcessInternal(processid);
        }
        else
        {
            removedProcesses.add(ps);
        }
    } // removeProcess

    static private void removeProcessInternal(String processid)
    {
        ProcessExecution.processDependency.initCompleted.remove(processid);
    } // removeProcess
    
//    public synchronized void addTaskDependency(String processid, ActionProcessTaskRequest task)
//    {
//        /*
//         * dependencies{processid} => processDependencies{executeid} => taskDependDef
//         * 
//         * taskDependDef.task = ActionProcessTaskRequest
//         * taskDependDef.dependents = Map of ActionProcessTaskRequest 
//         */
//        
//        // initialize dependency definition for processid
//        Map processDependencies = (Map)dependencies.get(processid);
//        if (processDependencies == null)
//        {
//            processDependencies = new ConcurrentHashMap();
//            dependencies.put(processid, processDependencies);
//        }
//        
//        // initialize task dependency definition for executeid
//        TaskDependency taskDependDef = (TaskDependency)processDependencies.get(task.getExecute());
//        if (taskDependDef == null)
//        {
//            taskDependDef = new TaskDependency();
//            processDependencies.put(task.getExecute(), taskDependDef);
//        }
//        
//        // set task request in task dependency definition
//        taskDependDef.setTask(task);
//        
//        // set task dependencies in task dependency definition
//        for (Iterator dependIdx=task.getDepends().iterator(); dependIdx.hasNext(); )
//        {
//            // for each dependent task, add to the dependent task's dependency definition
//            ActionProcessTaskDependency dependency = (ActionProcessTaskDependency)dependIdx.next();
//            String dependExecuteid = dependency.getDependExecuteid();
//            if (dependExecuteid != null)
//            {
//		        TaskDependency refDependDef = (TaskDependency)processDependencies.get(dependExecuteid);
//		        if (refDependDef == null)
//		        {
//		            refDependDef = new TaskDependency();
//		            processDependencies.put(dependExecuteid, refDependDef);
//		        }
//	            refDependDef.putDependent(task);
//            }
//        }
//        
//    } // addTaskDependency
    
//    public TaskDependency getTaskDependency(String processid, String executeid)
//    {
//        TaskDependency result = null;
//        
//        if (processid != null)
//        {
//            Map processDependencies = (Map)dependencies.get(processid);
//            if (processDependencies != null)
//            {
//                result = (TaskDependency)processDependencies.get(executeid);
//            }
//        }
//        
//        return result;
//    } // getTaskDependency
    
//    public void removeTaskDependency(String processid, String executeid)
//    {
//        Map processDependencies = (Map)dependencies.get(processid);
//        if (processDependencies != null)
//        {
//            processDependencies.remove(executeid);
//        }
//    } // removeTaskDependency
    
} // ProcessDependency
