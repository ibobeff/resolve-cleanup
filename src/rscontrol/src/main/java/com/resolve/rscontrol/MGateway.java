/******************************************************************************
* (C) Copyright 2014
*
* Resolve Systems, LLC
*
* All rights reserved
* This software is distributed under license and may not be used, copied,
* modified, or distributed without the express written permission of
* Resolve Systems, LLC.
*
******************************************************************************/

package com.resolve.rscontrol;

import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.resolve.rsbase.MainBase;
import com.resolve.services.ServiceGateway;
import com.resolve.services.ServiceJDBC;
import com.resolve.services.ServiceResolutionRouting;
import com.resolve.services.hibernate.util.UserUtils;
import com.resolve.services.hibernate.vo.GatewayVO;
import com.resolve.services.hibernate.vo.NamePropertyVO;
import com.resolve.services.hibernate.vo.OrgsVO;
import com.resolve.services.hibernate.vo.RRSchemaVO;
import com.resolve.services.vo.QueryDTO;
import com.resolve.services.vo.QueryFilter;
import com.resolve.services.vo.QuerySort;
import com.resolve.util.Log;
import com.resolve.util.MappingAnnotation;
import com.resolve.util.ObjectSizeCalculator;
import com.resolve.util.StringUtils;

/**
 * This is an abstract class that's extended by various subclasses for Resolve Gateways (Netcool, Remedyx etc.)
 *
 * While looking at the Gateway code think Abstraction and OO concepts like inheritance etc.
 * There are hardly any copy/paste in these codes. It helps in reducing maintenance overhead
 * and keep the codebase more meaningful. These codes go through "FindBug" which makes sure
 * that there is no obvious problem with the code.
 *
 * Check {@link MNetcool} or {@link MRemedyx} for its usage.
 */
public abstract class MGateway
{
    protected String gatewayName;
    protected String queueName;
    protected String messageHandlerName;

    public MGateway()
    {
        super();
    }

    /**
     * This method is implemented by subclasses {@link MNetcool} by prodiving the gateway
     * filter's hibernate entity name (e.g., "NetcoolFilter");
     *
     * @return
     */
    abstract protected String getModelName();

    /**
     * Converts an object that has MappingAnnotation to a Map. This is
     * useful for Gateway filters. RSRemote sends gateway filter properties
     * as a Map with keys those are different from the entity's property name.
     * For example EmailFilter has a property UQuery but RSRemote sends the value
     * of this property in a key name QUERY. So we introduced the {@link MappingAnnotation}
     * in the VO's to indicate the key from RSRemote. This method simply populate the
     * VO's properties from the Map.
     *
     * @param mappingAnnotatedVO
     * @return
     */
    protected static Map<String, String> convert(Object mappingAnnotatedVO)
    {
        Map<String, String> result = new HashMap<String, String>();

        Method[] methods = mappingAnnotatedVO.getClass().getMethods();
        for (Method method : methods)
        {
            MappingAnnotation mappingAnnotation = method.getAnnotation(MappingAnnotation.class);
            if (mappingAnnotation != null)
            {
                try
                {
                    String columnName = mappingAnnotation.columnName();
                    Object value = method.invoke(mappingAnnotatedVO);
                    if (value != null)
                    {
                        result.put(columnName, value.toString());
                    }
                }
                catch (Exception e)
                {
                    Log.log.warn(e.getMessage(), e);
                }
            }
        }
        return result;
    }

    /**
     * Query the database and retrieve filters by a queue name.
     *
     * @param queueName
     * @return
     */
    public List<Map<String, String>> getFilters(String queueName)
    {
        List<Map<String, String>> result = new ArrayList<Map<String, String>>();

        try
        {
            QueryDTO query = new QueryDTO();
            query.setModelName(getModelName());
            query.setWhereClause("UQueue='" + queueName + "'");

            List<GatewayVO> filters = ServiceGateway.getAllFilter(query, null);

            for (GatewayVO filterVo : filters)
            {
                result.add(convert(filterVo));
            }
        }
        catch (Exception e)
        {
            Log.log.error(e.getMessage(), e);
        }

        return result;
    } // getFilters

    /**
     * Saves a new set of filters or modifies existing one.
     *
     * @param filters
     */
    public void setFilters(Map<String, String> filters)
    {
        Log.log.info("Received setFilters [" + filters + "] for " + getModelName());
        
        try
        {
            ServiceGateway.setFilters(getModelName(), filters);
        }
        catch (Exception e)
        {
            Log.log.error(e.getMessage(), e);
        }
    } // setFilters

    /**
     * Saves a new set of name properties or modifies existing one.
     *
     * @param nameProperties
     */
    public void setNameProperties(Map<String, Object> nameProperties)
    {
        try
        {
            Log.log.debug("Saving name properties in database, property name: " + nameProperties.get("NAME") + ", queue : " + nameProperties.get("QUEUE"));
            ServiceGateway.setNameProperties("NameProperty", nameProperties);
        }
        catch (Exception e)
        {
            Log.log.error(e.getMessage(), e);
        }
    } // setFilters

    /**
     * Query the database and retrieve name properties by a queue name.
     *
     * @param queueName
     * @return
     */
    public List<Map<String, Object>> getNameProperties(String queueName)
    {
        List<Map<String, Object>> result = new ArrayList<Map<String, Object>>();

        try
        {
            QueryDTO query = new QueryDTO();
            query.setModelName("NameProperty");
            query.setWhereClause("UQueue='" + queueName + "'");

            List<NamePropertyVO> nameProperties = ServiceGateway.getAllNameProperties(query);

            for (NamePropertyVO namePropertyVO : nameProperties)
            {
                Map<String, Object> nameProperty = new HashMap<String, Object>();
                nameProperty.put("QUEUE", namePropertyVO.getUQueue());
                nameProperty.put("NAME", namePropertyVO.getUName());
                nameProperty.put("CONTENT", namePropertyVO.getUContent());
                result.add(nameProperty);
            }
        }
        catch (Exception e)
        {
            Log.log.error(e.getMessage(), e);
        }

        return result;
    } // getNameProperties

    /**
     * Query the database and retrieve resolution routing by a queue name.
     *
     * @param queueName
     * @return
     */
    public Map<String, Object> getRoutingSchemas(String queueName, String orgName)
    {
        Map<String, Object> orgRRSchemaMap = new HashMap<String, Object>();
        
        String origQueueName = queueName;
        orgName = StringUtils.isBlank(orgName) ? "" : orgName;
        
        List<String> orgHirarchy = null;
        if (StringUtils.isNotBlank(orgName))
        {
            origQueueName = StringUtils.substringBefore(queueName, orgName);
            orgHirarchy = new ArrayList<String>();
            orgHirarchy.add(orgName);
            
            List<OrgsVO> parentOrgVos = UserUtils.getParentOrgsInHierarchy(null, orgName);
            
            if (parentOrgVos != null && parentOrgVos.size() > 0)
            {
                for (OrgsVO orgVO : parentOrgVos)
                {
                    orgHirarchy.add(orgVO.getUName().toUpperCase());
                }
            }
            
            orgHirarchy.add(origQueueName);
        }
        
        orgRRSchemaMap.put("ORG_HIRARCHY", orgHirarchy);
        
        if (orgHirarchy != null)
        {
            for (String org : orgHirarchy)
            {
                orgRRSchemaMap.put(org, getSchemaMap(origQueueName + org));
            }
            orgRRSchemaMap.put(origQueueName, getSchemaMap(origQueueName));
        }
        else
        {
            orgRRSchemaMap.put(origQueueName, getSchemaMap(origQueueName));
        }
        
        return orgRRSchemaMap;
    } // getRoutingSchemas
    
    
    private List<Map<String, Object>> getSchemaMap(String queueName)
    {
        List<Map<String, Object>> result = new ArrayList<Map<String, Object>>();
        
        try
        {
            QueryDTO query = new QueryDTO();
            query.setModelName("RRSchema");
            query.addSortItem(new QuerySort("order", QuerySort.SortOrder.ASC));
            //e.g., "HTTP", "NETCOOL", this is because the gatewayQueues stores queues as json.
            query.addFilterItem(new QueryFilter("gatewayQueues", QueryFilter.CONTAINS, "\"" + queueName + "\""));
            //query.addFilterItem(new QueryFilter("gatewayQueues", QueryFilter.CONTAINS, "\"" + queueName + "PARENT" + "\""));
            List<RRSchemaVO> rrSchemaVOs = ServiceResolutionRouting.listSchema(query, "system");
            query.getFilters();

            for (RRSchemaVO rrSchemaVO : rrSchemaVOs)
            {
                Map<String, Object> schema = new HashMap<String, Object>();
                schema.put("sys_id", rrSchemaVO.getSys_id());
                schema.put("u_name", rrSchemaVO.getName());
                schema.put("u_order", rrSchemaVO.getOrder());
                //[{"name":"MYPARAM1","regex":"","order":0},{"name":"MYPARAM2","regex":"","order":1}]
                schema.put("u_json_fields", rrSchemaVO.getJsonFields());

                int start = 0;
                int limit = 10000; //10k at a time

                // TODO make the folloing call going through Hibernate model instead of direct query
                // there is a problem with casting Double to Boolean when Oracle DB is used for u_new_worksheet
                
                //the native query is the fasted in this case. observed that hibernate query took very long time this works in mysql and oracle.
                String sql = "SELECT rrr.*, wiki.u_fullname FROM rr_rule rrr LEFT OUTER JOIN wikidoc wiki ON rrr.u_runbook_id=wiki.sys_id" +
                        " WHERE (rrr.u_sir=1 OR rrr.u_runbook_id IS NOT NULL OR rrr.u_event_id IS NOT NULL) AND rrr.u_active=1 AND rrr.u_schema_id='" + rrSchemaVO.getSys_id() + "'";
                
                List<Map<String, Object>> rulesList = ServiceJDBC.executeSQLSelectForUI(sql, start, limit);
                
                List<Map<String, Object>> rules = new ArrayList<Map<String, Object>>();

                while(rulesList != null && rulesList.size() > 0)
                {
                    rules.addAll(rulesList);
                    //get the next batch
                    start = start + limit;
                    rulesList = ServiceJDBC.executeSQLSelectForUI(sql, start, limit);
                }
                
                if(Log.log.isDebugEnabled())
                {
                    try
                    {
                        Log.log.debug("Total number of rules retrieved for schema " + rrSchemaVO.getName() + " : " + rules.size());
                        Log.log.debug("Approximate size of rules in bytes : " + ObjectSizeCalculator.getObjectSize(rules));
                    }
                    catch(UnsupportedOperationException e)
                    {
                        //ignore
                        Log.log.debug("This is not a bug, just failed to get the size of the rules object due to following: " + e.getMessage());
                    }
                }

                //add all the rules to schema
                schema.put("rules", rules);
                result.add(schema);
            }
        }
        catch (Exception e)
        {
            Log.log.error(e.getMessage(), e);
        }
        
        return result;
    }

    /**
     * This method is called when RSCONTROL receives a JMS message from a Gateway
     * that's just started and wants to sync-up the filter or other stuff.
     * There may be some sub classes (e.g., Database, Remedyx) that may override this
     * method to sync up otherstuff.
     *
     * Note: When new gateway is introduced, make sure you override this method in the subclass
     * every single time until we fix the rscontrol code where it's unable to find method
     * from a super class during reflection.
     *
     * @param params
     */
    public void synchronizeGateway(Map<String, String> params)
    {
        gatewayName = params.get("GATEWAY_NAME");
        queueName = params.get("QUEUE_NAME");
        messageHandlerName = params.get("MESSAGE_HANDLER_NAME");
        String orgName = params.get("ORG_NAME");
        String className = params.get("GATEWAY_CLASS");
        
        Log.log.debug("Gateway Synchronization message received with following parameters:");
        Log.log.debug("     GATEWAY_NAME: " + gatewayName);
        Log.log.debug("     QUEUE_NAME: " + queueName);
        Log.log.debug("     MESSAGE_HANDLER_NAME: " + messageHandlerName);

        if (StringUtils.isBlank(gatewayName) || StringUtils.isBlank(queueName) || StringUtils.isBlank(messageHandlerName))
        {
            Log.log.error("GATEWAY_NAME, QUEUE_NAME, MESSAGE_HANDLER_NAME must be passed along with the message for synchronizing gateways");
            throw new RuntimeException("GATEWAY_NAME, QUEUE_NAME, MESSAGE_HANDLER_NAME must be passed along with the message for synchronizing gateways");
        }
        else
        {
            List<Map<String, String>> filters = null;
            Map<String, Object> messageParams = new HashMap<String, Object>();
            
            try {
                if(messageHandlerName.indexOf("MPushGateway") != -1) {
                    filters = MPushGateway.getGatewayFilters(queueName, gatewayName);
                    List<Map<String, Object>> list = new ArrayList<Map<String, Object>>();
                    Map<String, Object> gateway = new HashMap<String, Object>();
                    gateway.put("GATEWAYNAME", gatewayName);
                    gateway.put("GATEWAYCLASS", className);
                    list.add(gateway);
                    messageParams.put("GATEWAY", list);
                }

                else if(messageHandlerName.indexOf("MPullGateway") != -1) {
                    filters = MPullGateway.getGatewayFilters(queueName, gatewayName);
                    List<Map<String, Object>> list = new ArrayList<Map<String, Object>>();
                    Map<String, Object> gateway = new HashMap<String, Object>();
                    gateway.put("GATEWAYNAME", gatewayName);
                    gateway.put("GATEWAYCLASS", className);
                    list.add(gateway);
                    messageParams.put("GATEWAY", list);
                }
                
                else if(messageHandlerName.indexOf("MMSGGateway") != -1) {
                    filters = MMSGGateway.getGatewayFilters(queueName, gatewayName);
                    List<Map<String, Object>> list = new ArrayList<Map<String, Object>>();
                    Map<String, Object> gateway = new HashMap<String, Object>();
                    gateway.put("GATEWAYNAME", gatewayName);
                    gateway.put("GATEWAYCLASS", className);
                    list.add(gateway);
                    messageParams.put("GATEWAY", list);
                }

                else
                    filters = getFilters(gatewayName);
            } catch(Exception e) {
                Log.log.error(e.getMessage(), e);
            }

            messageParams.put("FILTERS", filters);
            messageParams.put("NAMEPROPERTIES", getNameProperties(gatewayName));
            messageParams.put("ROUTINGSCHEMAS", getRoutingSchemas(gatewayName, orgName));

            //send the message back to the requester who sent this message.
            Log.log.trace("SENDING synchronization message to " + gatewayName + " gateway using queue " + queueName);
            if (MainBase.esb.sendInternalMessage(queueName, messageHandlerName + ".synchronizeGateway", messageParams) == false)
            {
                Log.log.warn("Failed to send filter synchronization message to " + gatewayName + " gateway.");
            }
        }
    }
    
    /**
     * This method is called when RSCONTROL receives a JMS message from a Gateway
     * that's just started and wants to sync-up the filter or other stuff.
     * There may be some sub classes (e.g., Database, Remedyx) that may override this
     * method to sync up otherstuff.
     *
     * Note: When new gateway is introduced, make sure you override this method in the subclass
     * every single time until we fix the rscontrol code where it's unable to find method
     * from a super class during reflection.
     *
     * @param params
     */
    public void synchronizeRoutingSchemas(Map<String, String> params)
    {
        gatewayName = params.get("GATEWAY_NAME");
        queueName = params.get("QUEUE_NAME");
        messageHandlerName = params.get("MESSAGE_HANDLER_NAME");
        String orgName = params.get("ORG_NAME");

        Log.log.debug("Gateway Synchronize Routing Schema message received with following parameters:");
        Log.log.debug("     GATEWAY_NAME: " + gatewayName);
        Log.log.debug("     QUEUE_NAME: " + queueName);
        Log.log.debug("     MESSAGE_HANDLER_NAME: " + messageHandlerName);

        if (StringUtils.isBlank(gatewayName) || StringUtils.isBlank(queueName) || StringUtils.isBlank(messageHandlerName))
        {
            Log.log.error("GATEWAY_NAME, QUEUE_NAME, MESSAGE_HANDLER_NAME must be passed along with the message for synchronizing gateway routing schemas.");
            throw new RuntimeException("GATEWAY_NAME, QUEUE_NAME, MESSAGE_HANDLER_NAME must be passed along with the message for synchronizing gateway routing schemas.");
        }
        else
        {
            Map<String, Object> messageParams = new HashMap<String, Object>();
            messageParams.put("ROUTINGSCHEMAS", getRoutingSchemas(gatewayName, orgName));

            //send the message back to the requester who sent this message.
            Log.log.trace("SENDING gateway routing schema synchronization message to " + gatewayName + " gateway using queue " + queueName);
            if (MainBase.esb.sendInternalMessage(queueName, messageHandlerName + ".receiveRoutingSchemas", messageParams) == false)
            {
                Log.log.warn("Failed to send gateway routing schema synchronization message to " + gatewayName + " gateway.");
            }
        }
    }
}
