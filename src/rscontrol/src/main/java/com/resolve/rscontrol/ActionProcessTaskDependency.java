/******************************************************************************
* (C) Copyright 2014
*
* Resolve Systems, LLC
*
* All rights reserved
* This software is distributed under license and may not be used, copied,
* modified, or distributed without the express written permission of
* Resolve Systems, LLC.
*
******************************************************************************/

package com.resolve.rscontrol;

import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;

import com.resolve.util.Constants;
import com.resolve.util.StringUtils;

public class ActionProcessTaskDependency
{
    String sys_id;
    String type;
    String dependActionName;
    String dependExecuteid;
    boolean completion;
    String condition;
    String severity;
    String merge;
    String expression;
    private Map<String, Set<String>> expressionVarMap = new HashMap<String, Set<String>>();
    
    public ActionProcessTaskDependency()
    {
    } // ActionProcessTaskDependency
    
    public String toString()
    {
        return dependActionName;
    } // toString
    
    public String getsys_id()
    {
        return this.sys_id;
    } // getsys_id
    
    public void setsys_id(String sys_id)
    {
        this.sys_id = sys_id;
    } // setsys_id
    
    String getKey(String processid, String type, String target)
    {
        return processid+"/"+sys_id+"/"+type+"/"+target;
    } // getKey
    
    void putStatus(String processid, String key, Boolean value)
    {
        ProcessContext.put(processid, Constants.PROCESS_MAP_EVALUATE_STATUS+"/"+key, value);
    } 
    
    public boolean getStatus(String processid, String type, String target)
    {
        boolean result;
        
        if (type != null && target != null)
        {
	        Map<String, Object> processMap = ProcessContext.getProcessMap(processid);
	        if (processMap != null)
	        {
	            String key = getKey(processid, type, target);
	            if (processMap.containsKey(Constants.PROCESS_MAP_EVALUATE_STATUS+"/"+key))
	            {
		            result = ((Boolean)processMap.get(Constants.PROCESS_MAP_EVALUATE_STATUS+"/"+key)).booleanValue();
	            }
	            else
	            {
	                result = false;
	            }
	        }
	        else
	        {
		        result = false;
	        }
        }
        else
        {
	        result = true;
        }
        
        return result;
    } // getStatus
    
    public void setStatus(String processid, String type, String target, boolean status)
    {
        if (type != null && target != null)
        {
	        String key = getKey(processid, type, target);
	        putStatus(processid, key, new Boolean(status));
        }
    } // setStatus
    
    public String getDependExecuteid()
    {
        return dependExecuteid;
    }
    public void setDependExecuteid(String dependExecuteid)
    {
        this.dependExecuteid = dependExecuteid;
    }
    public String getDependActionName()
    {
        return dependActionName;
    }
    public void setDependActionName(String dependActionName)
    {
        this.dependActionName = dependActionName;
    }
    public String getType()
    {
        return type;
    }
    public void setType(String type)
    {
        this.type = type;
    }
    public boolean requiresCompletion()
    {
        return this.completion;
    }
    public boolean getCompletion()
    {
        return this.completion;
    }
    public void setCompletion(String completion)
    {
        this.completion = false;
        
        if (!StringUtils.isEmpty(completion) && completion.equalsIgnoreCase("TRUE"))
        {
            this.completion = true;
        }
    }
    public void setCompletion(boolean completion)
    {
        this.completion = completion;
    }
    public boolean hasCondition()
    {
        return !StringUtils.isEmpty(this.condition);
    }
    public String getCondition()
    {
        return condition;
    }
    public void setCondition(String condition)
    {
        this.condition = condition;
    }
    public boolean hasSeverity()
    {
        return !StringUtils.isEmpty(this.severity);
    }
    public String getSeverity()
    {
        return severity;
    }
    public void setSeverity(String severity)
    {
        this.severity = severity;
    }
    public String getExpression()
    {
        return expression;
    }
    public void setExpression(String expression)
    {
        this.expression = expression;
        
        expressionVarMap.clear();
        
        if (StringUtils.isBlank(expression) || 
            (!expression.contains(Constants.GROOVY_BINDING_PARAMS) && 
             !expression.contains(Constants.GROOVY_BINDING_FLOWS) && 
             !expression.contains(Constants.GROOVY_BINDING_OUTPUTS) && 
             !expression.contains(Constants.GROOVY_BINDING_WSDATA)))
        {
            return;
        }
        
        /*
         *  If expression contains any of PARAMS, FLOWS, OUTPUTS and/or WSDATA
         *  identify variable(s) used for null check at the runtime
         */
        
        String[] tokens = (((((expression.replaceAll("[]\\(\\)&\\|!]", " ")).replaceAll(" \\{2,}", " ")).replace("[", ".")).replaceAll("[\"']", "")).trim()).split(" ");
        
        Map<String, Set<String>> checkedVarsMap = new HashMap<String, Set<String>>();
        
        for (int i = 0; i < tokens.length; i++)
        {
            if (tokens[i].trim().startsWith(Constants.GROOVY_BINDING_PARAMS + ".") ||
                tokens[i].trim().startsWith(Constants.GROOVY_BINDING_FLOWS + ".") ||
                tokens[i].trim().startsWith(Constants.GROOVY_BINDING_OUTPUTS + ".") ||
                tokens[i].trim().startsWith(Constants.GROOVY_BINDING_WSDATA + "."))
            {
                String[] mapKeyPair = tokens[i].trim().split("\\.");
                
                if (mapKeyPair.length >= 2)
                {
                    if (mapKeyPair[1].equals("containsKey") || mapKeyPair[1].equals("get"))
                    {
                        Set<String> chckedVars = checkedVarsMap.get(mapKeyPair[0]);
                        
                        if (chckedVars == null)
                        {
                            checkedVarsMap.put(mapKeyPair[0], new HashSet<String>());
                            chckedVars = checkedVarsMap.get(mapKeyPair[0]);
                        }
                                               
                        // Check the next token 
                        if ( i++ < tokens.length)
                        {
                            chckedVars.add(tokens[i].trim());
                            i++;
                        }
                    }
                    else
                    {
                        StringBuilder expressionVarStrBldr = new StringBuilder();
                        
                        for (int j = 1; j < mapKeyPair.length; j++)
                        {
                            if (j > 1)
                            {
                                expressionVarStrBldr.append(".");
                            }
                            
                            expressionVarStrBldr.append(mapKeyPair[j]);
                        }
                        
                        Set<String> expressionVars = expressionVarMap.get(mapKeyPair[0]);
                        
                        if (expressionVars == null)
                        {
                            expressionVarMap.put(mapKeyPair[0], new HashSet<String>());
                            expressionVars = expressionVarMap.get(mapKeyPair[0]);
                        }
                        
                        expressionVars.add(expressionVarStrBldr.toString());
                    }
                }
            }
        }
        
        // Remove checkedVars in checkedVarsMap from corresponding expressionVars in expressionVarsMap
        
        for (String key : expressionVarMap.keySet())
        {
            if (expressionVarMap.get(key) != null && !expressionVarMap.get(key).isEmpty() &&
                checkedVarsMap.containsKey(key) && !checkedVarsMap.get(key).isEmpty())
            {
                expressionVarMap.get(key).removeAll(checkedVarsMap.get(key));
                /*
                for (String checkedVar : checkedVarsMap.get(key))
                {
                    if (expressionVarMap.get(key).contains(checkedVar))
                    {
                        expressionVarMap.get(key).remove(checkedVar);
                        checkedVarsMap.get(key).remove(checkedVar);
                    }
                }*/
            }
        }
        
        Set<String> expVarMapKeysToRemove = new HashSet<String>();
        
        for (String expresionVarMapKey : expressionVarMap.keySet())
        {
            if (expressionVarMap.get(expresionVarMapKey) != null && expressionVarMap.get(expresionVarMapKey).isEmpty())
            {
                expVarMapKeysToRemove.add(expresionVarMapKey);
            }
        }
        
        if (!expVarMapKeysToRemove.isEmpty())
        {
            for (String expVarMapKeyToRemove : expVarMapKeysToRemove)
            {
                expressionVarMap.remove(expVarMapKeyToRemove);
            }
        }
        // If anything still remains in checkdVarsMap does it means it is not checked ? 
    }
    public String getMerge()
    {
        return merge;
    }
    public void setMerge(String merge)
    {
        this.merge = merge;
    }

    public Map<String, Set<String>> getExpressionVarsMap()
    {
        return expressionVarMap;
    }
} // ActionProcssTaskDependency
