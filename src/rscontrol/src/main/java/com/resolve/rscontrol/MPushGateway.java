package com.resolve.rscontrol;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import com.resolve.persistence.model.PushGatewayFilter;
import com.resolve.services.hibernate.util.PushGatewayFilterUtil;
import com.resolve.services.hibernate.vo.GatewayVO;
import com.resolve.util.Log;

public class MPushGateway extends MGateway {

    private static final String MODEL_NAME = PushGatewayFilter.class.getSimpleName();

    protected String getModelName() {
        return MODEL_NAME;
    }
    
    public static List<Map<String, String>> getGatewayFilters(String queueName, String gatewayName)
    {
        List<Map<String, String>> result = new ArrayList<Map<String, String>>();

        try
        {
            List<GatewayVO> filters = PushGatewayFilterUtil.listPushFilters(gatewayName, queueName, null);

            for (GatewayVO filterVo : filters)
            {
                result.add(convert(filterVo));
            }
        }
        catch (Exception e)
        {
            Log.log.error(e.getMessage(), e);
        }

        return result;
    }
    
} // class MPushGateway

