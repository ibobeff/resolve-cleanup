/******************************************************************************
* (C) Copyright 2014
*
* Resolve Systems, LLC
*
* All rights reserved
* This software is distributed under license and may not be used, copied,
* modified, or distributed without the express written permission of
* Resolve Systems, LLC.
*
******************************************************************************/

package com.resolve.rscontrol;

import java.util.Collection;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;
import com.resolve.util.Constants;
import com.resolve.util.Log;
import com.resolve.util.StringUtils;

public class ExecuteParameterStateCache
{
    public static Collection<ExecuteParameter> getParameters(String processid, String executeid)
    {
        Collection<ExecuteParameter> result = new HashSet<ExecuteParameter>();
        
        // init processid to TASK for executeTask
        if (StringUtils.isEmpty(processid))
        {
            processid = "TASK";
        }
        
        Map processMap = ProcessContext.getProcessMap(processid);
        if (processMap != null)
        {
	        for (String id : getProcExecIds(processid, executeid))
	        {
	            ExecuteParameter parameter = (ExecuteParameter)processMap.get(Constants.PROCESS_MAP_EXECUTEPARAM+"/"+id);
	            if (parameter != null)
	            {
		            result.add(parameter);
	            }
	        }
        }
        else
        {
            Log.log.error("Missing processMap - processid: "+processid);
        }
        
        return result;
    } // getParameters
    
    public static ExecuteParameter getParameter(String processid, String executeid, String name, String type)
    {
        // init processid to TASK for executeTask
        if (StringUtils.isEmpty(processid))
        {
            processid = "TASK";
        }
        
        return (ExecuteParameter)ProcessContext.get(processid, Constants.PROCESS_MAP_EXECUTEPARAM+"/"+ExecuteParameter.getId(processid, executeid, name, type));
    } // getParameter
    
    public static void setParameter(String processid, String executeid, ExecuteParameter parameter)
    {
        // init processid to TASK for executeTask
        if (StringUtils.isEmpty(processid))
        {
            processid = "TASK";
        }
        
        // insert paramter
        parameter.setProcessid(processid);
        parameter.setExecuteid(executeid);
        ProcessContext.put(processid, Constants.PROCESS_MAP_EXECUTEPARAM+"/"+parameter.getId(), parameter);
        
        // add ProcExec id
        addProcExecId(processid, executeid, parameter.getId());
    } 
    
    static Set<String> getProcExecIds(String processid, String executeid)
    {
        Set<String> result = new HashSet<String>();
        
        Map processMap = ProcessContext.getProcessMap(processid);
        if (processMap != null)
        {
	        String ids = (String)processMap.get(Constants.PROCESS_MAP_EXECUTEPARAM_PROCEXEC_IDS+"/"+processid+"/"+executeid);
	        if (ids != null)
	        {
	            String[] idList = ids.split(",");
	            for (int i=0; i < idList.length; i++)
	            {
	                result.add(idList[i]);
	            }
	        }
        }
        else
        {
            Log.log.error("Missing processMap - processid: "+processid);
        }
        
        return result;
    } // getProcExecIds
    
    static void addProcExecId(String processid, String executeid, String id)
    {
        Map processMap = ProcessContext.getProcessMap(processid);
        if (processMap != null)
        {
	        String ids = (String)processMap.get(Constants.PROCESS_MAP_EXECUTEPARAM_PROCEXEC_IDS+"/"+processid+"/"+executeid);
	        if (ids == null)
	        {
	            ids = id;
	        }
	        else
	        {
	            ids = ids + "," + id;
	        }
	        processMap.put(Constants.PROCESS_MAP_EXECUTEPARAM_PROCEXEC_IDS+"/"+processid+"/"+executeid, ids);
        }
        else
        {
            Log.log.error("Missing processMap - processid: "+processid);
        }
    } 
    
} 
