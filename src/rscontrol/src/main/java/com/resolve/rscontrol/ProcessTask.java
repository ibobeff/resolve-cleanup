/******************************************************************************
* (C) Copyright 2014
*
* Resolve Systems, LLC
*
* All rights reserved
* This software is distributed under license and may not be used, copied,
* modified, or distributed without the express written permission of
* Resolve Systems, LLC.
*
******************************************************************************/

package com.resolve.rscontrol;

import java.io.File;
import java.sql.Connection;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.locks.ReentrantLock;

import com.resolve.esb.MMsgContent;
import com.resolve.esb.MMsgOptions;
import com.resolve.groovy.script.GroovyScript;
import com.resolve.rsbase.MainBase;
import com.resolve.search.SearchUtils;
import com.resolve.search.model.ExecuteState;
import com.resolve.search.model.ProcessRequest;
import com.resolve.services.ServiceHibernate;
import com.resolve.services.ServiceWorksheet;
import com.resolve.services.util.WorksheetUtil;
import com.resolve.services.util.WorksheetUtils;
import com.resolve.thread.ScheduledExecutor;
import com.resolve.util.Constants;
import com.resolve.util.EncryptionTransportObject;
import com.resolve.util.FileUtils;
import com.resolve.util.LockUtils;
import com.resolve.util.Log;
import com.resolve.util.ScriptDebug;
import com.resolve.util.StringUtils;
import com.resolve.util.XDoc;
import com.resolve.util.performance.ExecutionTimeUtil;
import com.resolve.util.performance.datasources.ExecutionStep;
import com.resolve.util.performance.datasources.ExecutionStepPhase;

import groovy.lang.Binding;

public class ProcessTask
{
    final private static String RUNBOOK_FINISHED = "Runbook already finished";
    final private static String RUNBOOK_DETACHED = "Runbook detached";
    final private static String RUNBOOK_ABORTED = "Runbook aborted";

    String command;
    String args;
    MMsgOptions options;
    Map inputs;
    Map outputs;
    Map params;
    Map flows;
    String executeid;
    String targetAddr;
    String processid;
    String wiki;
    String problemid;
    String userid;
    ScriptDebug debug;

    // static ReferenceMap processScriptCache = new
    // ReferenceMap(AbstractReferenceMap.SOFT, AbstractReferenceMap.HARD);
    static HashMap<String, String> processScriptCache = new HashMap<String, String>();

    public ProcessTask(String command, String args, MMsgOptions options, Map inputs, Map outputs, Map params, Map flows, ScriptDebug debug, String executeid, String targetAddr, String processid, String wiki, String problemid, String userid)
    {
        this.command = command;
        this.args = args;
        this.options = options;
        this.inputs = inputs;
        this.outputs = outputs;
        this.params = params;
        this.flows = flows;
        this.executeid = executeid;
        this.processid = processid;
        this.wiki = wiki;
        this.problemid = problemid;
        this.userid = userid;
        this.debug = debug;
        this.targetAddr = targetAddr;
    } // ProcessTaskComment

    public void execute()
    {
        ExecutionTimeUtil.log((String) params.get("RESOLVE.JOB_ID"), null, null, ExecutionStep.METHOD_EXECUTION, ExecutionStepPhase.START);

        String content = null;

        Connection conn = null;
        boolean transaction = false;

        // Decrypt map to allow user to use it natively
        List preInputs = EncryptionTransportObject.decryptMap(inputs);
        List preParams = EncryptionTransportObject.decryptMap(params);
        List preFlows = EncryptionTransportObject.decryptMap(flows);
        try
        {
            // check and execute if hard command
            content = executePredefinedCmd();
            if (StringUtils.isEmpty(content))
            {
                // init script debug
                debug.setComponent("ProcessTask");
                debug.setMap(inputs, outputs, params, flows);

                long duration = System.currentTimeMillis();

                // update script file cache
                String scriptFile = Main.main.getGSEHome() + "/process/" + command + ".groovy";
                String script = (String) processScriptCache.get(scriptFile);
                String scriptName = command;

                synchronized (processScriptCache)
                {

                    script = (String) processScriptCache.get(scriptFile);
                    if (script == null)
                    {
                        // get script
                        File file = FileUtils.getFile(scriptFile);
                        script = FileUtils.readFileToString(file, "UTF-8");

                        // add handler to cache
                        processScriptCache.put(scriptFile, script);
                    }
                }

                // execute script
                if (script != null)
                {
                    // add DB connection
                    conn = null;
                    if (GroovyScript.checkOpenTransaction(script))
                    {
                        transaction = true;
                        if (GroovyScript.checkScriptUseDB(script, true))
                        {
                            conn = ServiceHibernate.getConnection();
                        }
                    }

                    Binding binding = new Binding();
                    binding.setVariable(Constants.GROOVY_BINDING_PROCESS, this);
                    binding.setVariable(Constants.GROOVY_BINDING_HEADER, options);
                    binding.setVariable(Constants.GROOVY_BINDING_DB, conn);
                    binding.setVariable(Constants.GROOVY_BINDING_WIKI, wiki);
                    binding.setVariable(Constants.GROOVY_BINDING_ARGS, args);
                    binding.setVariable(Constants.GROOVY_BINDING_LOG, Log.log);
                    binding.setVariable(Constants.GROOVY_BINDING_PARAMS, params);
                    binding.setVariable(Constants.GROOVY_BINDING_FLOWS, flows);
                    binding.setVariable(Constants.GROOVY_BINDING_INPUTS, inputs);
                    binding.setVariable(Constants.GROOVY_BINDING_DEBUG, debug);

                    content = (String) GroovyScript.execute(script, scriptName, true, binding);
                }

                // set duration
                duration = System.currentTimeMillis() - duration;
                options.setDuration("" + duration);
            }
        }
        catch (Throwable t)
        {
            Log.log.warn(t.getMessage(), t);
            debug.println(t.getMessage(), t);

            content = t.getMessage();
        }
        finally
        {
         // Restore Encryption objects
            EncryptionTransportObject.restoreMap(inputs, preInputs);
            EncryptionTransportObject.restoreMap(params, preParams);
            EncryptionTransportObject.restoreMap(flows, preFlows);

            if (transaction)
            {
                try
                {
                    if (conn != null && !conn.isClosed())
                    {
                        conn.close();
                    }
                }
                catch (Exception e)
                {
                    Log.log.error(e.getMessage(), e);
                }
            }

            // send result or error
            if (content != null && !content.equals(RUNBOOK_FINISHED) && !content.equals(RUNBOOK_DETACHED) && !content.equals(RUNBOOK_ABORTED))
            {
                Log.log.info("Sending ActionTask Process name: " + command + " processid: " + processid + " executeid: " + executeid);
                debug.println("Sending ActionTask Process name: " + command + " processid: " + processid + " executeid: " + executeid);

                sendMessage(content);
            }
        }
        
        ExecutionTimeUtil.log((String) params.get("RESOLVE.JOB_ID"), null, null, ExecutionStep.METHOD_EXECUTION, ExecutionStepPhase.END);

    } // execute

    void sendMessage(String raw)
    {
        // init content
        MMsgContent content = new MMsgContent();
        content.setParams(StringUtils.objToString(params));
        content.setFlows(StringUtils.objToString(flows));
        content.setInputs(StringUtils.objToString(inputs));
        content.setRaw(raw);

        if (debug.isDebug())
        {
            content.setDebug("true");
        }
        else
        {
            content.setDebug("");
        }
        WorksheetUtils.writeDebugCAS(debug);

        if (Log.log.isDebugEnabled()) {
            Log.log.debug("OPTIONS: " + options.toString());
        }
        MResult.actionResult(options, content, true);
        Log.clearProcessId();

        //Main.esb.sendMessage(Constants.ESB_NAME_RSSERVER, "MResult.actionResult", options, content);
    } // sendMessage

    String executePredefinedCmd() throws Exception
    {
        ExecutionTimeUtil.log((String) params.get("RESOLVE.JOB_ID"), null, null, ExecutionStep.METHOD_EXECUTION, ExecutionStepPhase.START);
    	
        String result = null;

        if (command.equalsIgnoreCase("START"))
        {
            result = cmdStart();
        }
        else if (command.equalsIgnoreCase("END"))
        {
            result = cmdEnd();
        }
        else if (command.equalsIgnoreCase("WAIT"))
        {
            result = cmdWait();
        }
        else if (command.equalsIgnoreCase("ECHO"))
        {
            result = cmdEcho();
        }
        else if (command.equalsIgnoreCase("ABORT"))
        {
            result = cmdAbortProcess();
        }
        else if (command.equalsIgnoreCase("EVENT"))
        {
            result = cmdEvent();
        }
        
        ExecutionTimeUtil.log((String) params.get("RESOLVE.JOB_ID"), null, null, ExecutionStep.METHOD_EXECUTION, ExecutionStepPhase.END);


        return result;
    } // executePredefinedCmd

    String cmdStart()
    {
        String result = "";

        // init parentWiki and update wiki
        wiki = (String) params.get(Constants.EXECUTE_WIKI_PROCESS);
        String parentWiki = (String) params.get(Constants.EXECUTE_WIKI_PARENT);
        String wikiId = (String) params.get(Constants.EXECUTE_WIKI_WIKIID);
        String parentId = (String) params.get(Constants.EXECUTE_WIKI_PARENTID);

        // init modelType and modelMode
        String modelType = (String) params.get(Constants.MODEL_TYPE);
        String modelMode = (String) params.get(Constants.MODEL_MODE);

        // init has loop
        boolean hasLoop = false;
        Object processLoop = params.get(Constants.EXECUTE_PROCESS_LOOP);
        if (processLoop instanceof Boolean)
        {
            hasLoop = ((Boolean) processLoop).booleanValue();
        }
        else if (processLoop != null)
        {
            hasLoop = ((String) processLoop).equalsIgnoreCase("true");
        }

        // check not initial start task
        if (!wiki.equals(parentWiki))
        {
            ReentrantLock stateLock = LockUtils.getLock(Constants.LOCK_PROCSTATUSUPDATE + processid, processid);
            // get processState
            try
            {
                stateLock.lock();
                ProcessState state = ProcessExecution.getProcessState(processid);

                // add subrunbook states
                state.addSubRunbook(wiki, wikiId, parentWiki, parentId);

                // replicate state to other rscontrols
                ProcessExecution.setProcessState(processid, state);
            }
            catch (Throwable e)
            {
                Log.log.error(e.getMessage(), e);
                throw new RuntimeException(e);
            }
            finally {
                stateLock.unlock();
            }
        }

        // init message
        String summary = Constants.DETAIL_TYPE_HTML+"Process started: <a target='" + wiki + "' href='/resolve/jsp/rsclient.jsp#RS.wiki.Main/activeTab=2&name=" + wiki + "'>" + wiki + "</a> Date: " + (new Date());
        if (modelType.equals(Constants.MODEL_TYPE_ABORT))
        {
            summary = "Abort started: " + wiki;
        }

        // init detail
        String detail = "Process started: " + wiki + " processid: " + processid + " problemid: " + problemid;

        result += "COMPLETED=true\n";
        result += "CONDITION=good\n";
        result += "SEVERITY=good\n";
        result += "SUMMARY=" + summary + "\n";
        result += "DETAIL=" + detail + "\n";

        // update header with new wiki
        options.setWiki(wiki);

        // Set RSCONTROL as TARGET for start process task, just like END task
        options.setTarget("RSCONTROL");

        return result;
    } // cmdStart

    String cmdEnd() throws Exception
    {
        ExecutionTimeUtil.log((String) params.get("RESOLVE.JOB_ID"), null, null, ExecutionStep.METHOD_EXECUTION, ExecutionStepPhase.START);

        String result = "";

        // init modelType and modelMode
        String modelType = (String) params.get(Constants.MODEL_TYPE);
        String modelMode = (String) params.get(Constants.MODEL_MODE);
        String wiki_process = (String) params.get(Constants.EXECUTE_WIKI_PROCESS);
        String parentWiki = (String) params.get(Constants.EXECUTE_WIKI_PARENT);
        String wikiId = (String) params.get(Constants.EXECUTE_WIKI_WIKIID);
        String parentId = (String) params.get(Constants.EXECUTE_WIKI_PARENTID);
        String quit = (String) params.get(Constants.EXECUTE_END_QUIT);
        String wait = (String) params.get(Constants.EXECUTE_END_WAIT);
        String type = (String) params.get(Constants.EXECUTE_END_TYPE);
        String condition = (String) params.get(Constants.EXECUTE_END_CONDITION);
        String severity = (String) params.get(Constants.EXECUTE_END_SEVERITY);
        String event_reference = (String) params.get(Constants.EVENT_REFERENCE);

        // delay END processing
        try
        {
            if (wait != null && !wait.equals("") && !wait.equals("0"))
            {
                Thread.sleep(1000 * Long.parseLong(wait));
            }
        } catch (Exception e) {}

        // get process state
        ProcessState state = ProcessExecution.getProcessState(processid);
        if (state == null)
        {
            return RUNBOOK_FINISHED;
        }

        // String parentWiki = state.getParentWiki();
        // if (StringUtils.isEmpty(parentWiki))
        // {
        // parentWiki = wiki;
        // }

        // set duration
        long duration = state.getTotalDuration(wikiId);

        // set condition and severity
        if (StringUtils.isEmpty(condition) || StringUtils.isEmpty(severity))
        {
            if (!StringUtils.isEmpty(type))
            {
                if (type.equalsIgnoreCase("BEST"))
                {
                    if (StringUtils.isEmpty(condition)) condition = state.getBestCondition(wikiId);
                    if (StringUtils.isEmpty(severity)) severity = state.getBestSeverity(wikiId);
                }
                else if (type.equalsIgnoreCase("WORST"))
                {
                    if (!"BAD".equalsIgnoreCase(condition) && StringUtils.isEmpty(condition))
                    {
                        condition = state.getWorstCondition(wikiId);
                    }
                    if (StringUtils.isEmpty(severity)) severity = state.getWorstSeverity(wikiId);
                }
            }

            if (StringUtils.isEmpty(condition))
            {
                condition = "good";
            }
            if (StringUtils.isEmpty(severity))
            {
                severity = "good";
            }
        }

        // update parent wiki
        if (state != null)
        {
            ReentrantLock stateLock = LockUtils.getLock(Constants.LOCK_PROCSTATUSUPDATE + processid, processid);
            try
            {
                stateLock.lock();
                
                state = ProcessExecution.getProcessState(processid);

                // update processState end summary
                state.updateEndSummary(parentId, condition, severity);

                // replicate state to other rscontrols
                ProcessExecution.setProcessState(processid, state);
            }
            catch (Throwable e)
            {
                Log.log.error(e.getMessage(), e);
                throw new RuntimeException(e);
            }
            finally
            {
                stateLock.unlock();
                WorksheetUtils.writeDebugCAS(debug);
            }
        }

        // set summary and description
        String summary = Constants.DETAIL_TYPE_HTML+"Process completed: <a target='" + wiki_process + "' href='/resolve/jsp/rsclient.jsp#RS.wiki.Main/activeTab=2&name=" + wiki_process + "'>" + wiki_process + "</a> Date: " + (new Date());
        String detail = "Process completed: " + wiki + " processid: " + processid + " problemid: " + problemid;

        result += "COMPLETED=true\n";
        result += "CONDITION=" + condition + "\n";
        result += "SEVERITY=" + severity + "\n";
        result += "SUMMARY=" + summary + "\n";
        result += "DETAIL=" + detail + "\n";
        result += "DURATION=" + duration + "\n";

        // update header with parentWiki
        // options.setWiki(parentWiki);
        options.setWiki(wiki);

        // restore wiki to parent wiki
        wiki = parentWiki;
        params.put(Constants.EXECUTE_WIKI_WIKIID, parentId);

        // top level runbook process completed
        if (StringUtils.isEmpty(quit) || quit.equalsIgnoreCase("TRUE"))
        {
            if (Log.log.isDebugEnabled()) {
                Log.log.debug("End Process - process removed: " + processid);
            }

            // set process status
            String status = Constants.ASSESS_STATUS_COMPLETED;

            //Account for severity and condition count for runbook metrics
            
        	
            ProcessExecution.updateCondition(wiki, condition);
            
            ProcessExecution.updateSeverity(wiki, severity);
            
            ProcessRequest processRequest = ServiceWorksheet.findProcessRequestById(processid);

            if (processRequest != null)
            {
                // set process to completed
                processRequest.setStatus(status);
                
                if (state.getAtExecCount() > 0) {
                	// Update AT Execution Count account for execution of End task
                	processRequest.setAtExecCount(Integer.valueOf(state.getAtExecCount() + 1));
                	
                	if (Log.log.isDebugEnabled()) {
                		Log.log.debug(String.format("Process state for process id %s: status = %s and AT execution " +
                									"count = %d", processid, status, 
                									processRequest.getuATExecCount().intValue()));
                	}
                } else {
                	Log.log.warn(String.format("Count of AT executions in process state for process id %s is 0", processid));
                }
                
                processRequest.setDuration(new Integer((new Long(duration)).intValue()));
                ServiceWorksheet.updatProcessRequest(processRequest, userid);
            }

            // Set RSCONTROL as TARGET for final end process task
            options.setTarget("RSCONTROL");
        }
        else
        {
            if (Log.log.isDebugEnabled()) {
                Log.log.debug("End Subprocess - process NOT removed - processid: " + processid);
            }
        }

        ExecutionTimeUtil.log((String) params.get("RESOLVE.JOB_ID"), null, null, ExecutionStep.METHOD_EXECUTION, ExecutionStepPhase.END);
        return result;
    } // cmdEnd

    String cmdWait()
    {
        String result = "";
        String interval = (String)inputs.get("INTERVAL");
        try
        {
        	interval = (String)inputs.get("INTERVAL");
            Thread.sleep(1000 * Integer.parseInt(interval));
        }
        catch (Exception e) { }

        result += "COMPLETED=true\n";
        result += "CONDITION=good\n";
        result += "SEVERITY=good\n";
        result += "SUMMARY=Wait for " + interval + " secs completed\n";
        result += "DETAIL=Wait for " + interval + " secs completed\n";
        result += "DURATION=" + interval + "\n";

        return result;
    } // cmdWait

    String cmdEcho()
    {
        return (String) params.get("MESSAGE");
    } // cmdEcho

    String cmdAbortProcess()
    {
        String summary = (String) inputs.get("SUMMARY");
        String detail = (String) inputs.get("DETAIL");

        Map params = new HashMap();
        params.putAll(this.params);

        params.put(Constants.EXECUTE_PROCESSID, processid);
        params.put(Constants.EXECUTE_USERID, userid);
        params.put(Constants.EXECUTE_WIKI, Constants.EXECUTE_ABORTWIKI + wiki);
        params.put(Constants.EXECUTE_SUMMARY, summary);
        params.put(Constants.EXECUTE_DETAIL, detail);

        ProcessExecution.abortProcessCAS(params);

        return RUNBOOK_ABORTED;
    } // cmdAbortProcess

    String cmdEvent()
    {
        long now = System.currentTimeMillis();
        if(Log.log.isDebugEnabled())
        {
            Log.log.debug("executeid: " + executeid);
            Log.log.debug("processid: " + processid);
            Log.log.debug("wiki: " + wiki);
            Log.log.debug("problemid: " + problemid);
        }
        // terminate runbook
        String eventid = (String) inputs.get(Constants.EVENT_EVENTID);
        String event_reference = (String) inputs.get(Constants.EVENT_REFERENCE);
        String event_end = (String) inputs.get(Constants.EVENT_END);

        if (StringUtils.isTrue(event_end))
        {
            ProcessRequest processRequest = ServiceWorksheet.findProcessRequestById(processid);
            if (processRequest != null)
            {
                processRequest.setStatus(Constants.ASSESS_STATUS_WAITING);
                ServiceWorksheet.updatProcessRequest(processRequest, userid);
            }
        }

        // persist states to db
        persistState();

        // terminate process and cleanup
        String jobid = getJobId(eventid, event_reference);
        if (StringUtils.isTrue(event_end))
        {
            Log.log.info("Runbook waiting for HIBERNATION on EVENTID: " + eventid);
//            ProcessExecution.removeProcess(processid, wiki, problemid, false, true, event_reference, false);

            // schedule remove process
            long eventIdle = getEventIdle();
            ScheduledExecutor.getInstance().executeDelayed(jobid, (Object) this, "eventTerminate", eventIdle, TimeUnit.MILLISECONDS);
        }
        else
        {
            Log.log.info("Runbook WAITING on EVENTID: " + eventid);
        }

        // get timeout value from EVENT_TIMEOUT
        String event_timeout = "";
        if(inputs.get(Constants.EVENT_TIMEOUT) instanceof String) {
            event_timeout = (String) inputs.get(Constants.EVENT_TIMEOUT);
        } else if (inputs.get(Constants.EVENT_TIMEOUT) instanceof Integer){
            event_timeout = "" + inputs.get(Constants.EVENT_TIMEOUT);
        }
        if (StringUtils.isNotEmpty(event_timeout) && !event_timeout.equals("0"))
        {
            // schedule abort timer
            long timeout = Long.parseLong(event_timeout);
            ScheduledExecutor.getInstance().executeDelayed(jobid, (Object) this, "eventAbort", timeout, TimeUnit.SECONDS);
        }

        return RUNBOOK_DETACHED;
    } // cmdEvent

    private String getJobId(String eventid, String event_reference)
    {
        String jobid = problemid+"/"+eventid+"/";
        if (StringUtils.isNotEmpty(event_reference))
        {
            jobid += event_reference;
        }
        else
        {
            jobid += processid;
        }
        return jobid;
    }

    void eventAbort()
    {
        ProcessState ps = ProcessExecution.getProcessState(processid);
        if (ps==null)
        {
            Log.log.info("Event abort message received, but runbook already completed or hibernated. Message ignored. Wiki: " + wiki + ", processid: " + processid);
            return;
        }

        String eventid = (String) inputs.get(Constants.EVENT_EVENTID);
        String event_reference = (String) inputs.get(Constants.EVENT_REFERENCE);
        String event_end = (String) inputs.get(Constants.EVENT_END);
        String event_timeout = options.getTimeout();
        if(inputs.get(Constants.EVENT_TIMEOUT) instanceof String) {
            event_timeout = (String) inputs.get(Constants.EVENT_TIMEOUT);
        } else if (inputs.get(Constants.EVENT_TIMEOUT) instanceof Integer){
            event_timeout = "" + inputs.get(Constants.EVENT_TIMEOUT);
        }
        Log.log.info("Aborting event on EVENTID: " + eventid);

        // check if process is still running and update lastActivityTime
        ProcessExecution.updateLastActivity(processid);

        params.put(Constants.EVENT_COMPLETED, "true");
        params.put(Constants.EVENT_CONDITION, "bad");
        params.put(Constants.EVENT_SEVERITY, "critical");
        params.put(Constants.EVENT_DETAIL, "Event Timed Out - eventid: " + eventid);
        params.put(Constants.EVENT_DURATION, event_timeout);
        params.put(Constants.EXECUTE_PROCESSID, processid);
        params.put(Constants.EVENT_RETURNCODE, "0");  // This means "event" times out (fails)
        params.put(Constants.EVENT_EVENTABORT, "true");

        if(targetAddr!=null)
        {
            params.put(Constants.EXECUTE_ADDRESS, targetAddr);
        }

        Main.esb.sendEventContinue(eventid, event_reference, event_end, wiki, problemid, userid, params);
    } // eventAbort

    void eventTerminate()
    {
        // check if process is still running
        ProcessState processState = ProcessExecution.getProcessState(processid);
        if (processState != null)
        {
            // check if the process last activity is less than EVENT_IDLE
            long lastActivityTime = processState.getLastActivityTime();
            long eventIdle = getEventIdle();

            long diff = System.currentTimeMillis() - lastActivityTime;
            if (diff > eventIdle)
            {
                // remove process
                Log.log.info("Removing runbook process for hibernation - processid: "+processid);
                String event_reference = (String) inputs.get(Constants.EVENT_REFERENCE);
                persistProcessState();
                ProcessExecution.removeProcess(processid, wiki, problemid, false, true, event_reference, false);
            }
            else
            {
                // sleep and check again
                String eventid = (String) inputs.get(Constants.EVENT_EVENTID);
                String event_reference = (String) inputs.get(Constants.EVENT_REFERENCE);
                String jobid = getJobId(eventid, event_reference);
                ScheduledExecutor.getInstance().executeDelayed(jobid, (Object) this, "eventTerminate", eventIdle, TimeUnit.MILLISECONDS);
            }
        }

    } // eventTerminate

    private long getEventIdle()
    {
        long eventIdle = StringUtils.getLong(Constants.EVENT_IDLE, inputs);
        if (eventIdle < 0)
        {
            // default 5mins
            eventIdle = 300000;
        }
        else
        {
            // convert to msec
            eventIdle = eventIdle * 1000;
        }
        return eventIdle;
    } // getEventIdle

    void persistProcessState()
    {
        //this simply stores the process state,  
        ProcessState processState = ProcessExecution.getProcessState(processid);
        processState.setGlobalsSnapshot(ProcessExecution.getProcessInfo(processid).getProcessMap(processid));
        ExecuteState executeState = WorksheetUtil.findExecuteStateByProcessId(processid);
        executeState.setProcessState(StringUtils.objToString(processState));
        ServiceWorksheet.saveExecuteState(executeState);
    }

    void persistState()
    {
        ExecutionTimeUtil.log((String) params.get("RESOLVE.JOB_ID"), null, null, ExecutionStep.METHOD_EXECUTION, ExecutionStepPhase.START);
    	
        String stateid = null;
        String eventid = (String) inputs.get(Constants.EVENT_EVENTID);
        String event_reference = (String) inputs.get(Constants.EVENT_REFERENCE);
        String event_end = (String) inputs.get(Constants.EVENT_END);

        if (StringUtils.isEmpty(event_reference))
        {
            event_reference = "PROBLEMID"; 
        }
        
        // replace PROBLEMID constant
        if ("PROBLEMID".equals(event_reference))
        {
            event_reference = problemid;
        } 
        
        if(Log.log.isDebugEnabled())
        {
            Log.log.debug("Persisting state EVENTID: " + eventid + " REFERENCE: " + event_reference + " EVENT_END: " + event_end);
            
            if (StringUtils.isFalse(event_end))
            {
                Log.log.debug("    Component GUID= " + MainBase.main.configId.getGuid());
            }
            
            Log.log.debug("    PROCESSID=" + processid + "&PROBLEMID=" + problemid + "&EXECUTEID=" + executeid + "&TARGET=" + targetAddr);
        }
        
        // get parent wiki and add to PARAMS
        ProcessState processState = ProcessExecution.getProcessState(processid);

        List<ExecuteState> executeStates = ServiceWorksheet.findExecuteStates(null, processid, null, eventid, null);
        
        String rowKey = null;
        ExecuteState executeState = null;
        
        if(executeStates == null || executeStates.size() == 0)
        {
            executeState = new ExecuteState(SearchUtils.getNewRowKey(), null, userid);
            executeState.setProcessId(processid);
            executeState.setProblemId(problemid);
            executeState.setTargetAddress(eventid);
            executeState.setExecuteId(executeid);
            //ServiceWorksheet.saveExecuteState(executeState);
        }
        else
        {
            rowKey = executeStates.get(0).getSysId();
            executeState = executeStates.get(0);
        }
        
        
        // Set componentId if triggering of event does not ends the runbook, 
        if (StringUtils.isFalse(event_end))
        {
            executeState.setComponentId(MainBase.main.configId.getGuid());
        }
        
        // set timeouts
        params.put(Constants.EXECUTE_TIMEOUT, options.getTimeout());
        params.put(Constants.EXECUTE_PROCESS_TIMEOUT, options.getProcessTimeout());
        params.put(Constants.EXECUTE_METRICID, options.getMetricId());
        params.put(Constants.EXECUTE_ADDRESS, targetAddr==null?"":targetAddr);

        if (event_reference != null)
        {
            executeState.setReference(event_reference+eventid);
        }
        executeState.setParams(StringUtils.objToString(params));
        executeState.setFlows(StringUtils.objToString(flows));
        executeState.setInputs(StringUtils.objToString(inputs));
        executeState.setDebug(debug.toString());

        if (processState != null)
        {
            executeState.setProcessStartTime(processState.getStartTime());
        }
        ServiceWorksheet.saveExecuteState(executeState);
        if (processState != null)
        {
            persistProcessState();
        }
        
        if (Log.log.isDebugEnabled()) {
            Log.log.debug("Persisted state STATEID: " + rowKey + " EVENTID: " + eventid + " REFERENCE: " + event_reference);
        }
        
        ExecutionTimeUtil.log((String) params.get("RESOLVE.JOB_ID"), null, null, ExecutionStep.METHOD_EXECUTION, ExecutionStepPhase.END);
    } // persistState

    public static void removeState(String processid, String problemid, String event_reference, String eventid)
    {
        // delete using processid
        List<ExecuteState> rows = ServiceWorksheet.findExecuteStates(null, processid, null, null, null);

        if (Log.log.isDebugEnabled()) {
            Log.log.debug("Removing states for PROCESSID: " + processid);
        }

        List<String> executeStateIds = new ArrayList<String>();
        
        if (rows != null && !rows.isEmpty())
        {
            for (ExecuteState row : rows)
            {
                executeStateIds.add(row.getSysId());
            }
        }

        // delete using event_reference
        if (StringUtils.isNotEmpty(problemid) && StringUtils.isNotEmpty(event_reference))
        {
            List<ExecuteState> rows1 = ServiceWorksheet.findExecuteStates(null, null, problemid, null, event_reference+eventid);
            if (rows1 != null && !rows1.isEmpty())
            {
                for (ExecuteState row : rows1)
                {
                    executeStateIds.add(row.getSysId());
                }
            }
        }
        ServiceWorksheet.deleteExecuteStateRows(executeStateIds);
    } // removeState

} // ProcessTask
