/******************************************************************************
* (C) Copyright 2014
*
* Resolve Systems, LLC
*
* All rights reserved
* This software is distributed under license and may not be used, copied,
* modified, or distributed without the express written permission of
* Resolve Systems, LLC.
*
******************************************************************************/

package com.resolve.rscontrol;

import java.io.File;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.apache.commons.io.FileUtils;
import org.dom4j.DocumentException;
import org.xml.sax.SAXException;

import com.resolve.services.ServiceHibernate;
import com.resolve.services.hibernate.vo.ResolveRegistrationVO;
import com.resolve.util.Constants;
import com.resolve.util.GMTDate;
import com.resolve.util.Guid;
import com.resolve.util.Log;
import com.resolve.util.StringUtils;

public class MRegister
{
    private static File blueprintFile = null;    
    private static SimpleDateFormat sdf = new SimpleDateFormat(Constants.HEARTBEAT_FORMAT);
    private static ConfigRSRemote configRSRemote = new ConfigRSRemote();
    private static String registrationId = null;
    public static String registerSelf() throws Exception
    {
        ResolveRegistrationVO reg = ServiceHibernate.findResolveRegistrationByGuid(Main.main.configId.guid);
        if(reg == null)
        {
            reg = new ResolveRegistrationVO();
        }
        
        // update registration
        reg.setUGuid(Main.main.configId.guid);
        reg.setUName(Main.main.configId.name);
        reg.setUType(Main.main.release.type);
        reg.setUIpaddress(Main.main.configId.ipaddress);
        reg.setUVersion(Main.main.release.version);
        reg.setUDescription(Main.main.configId.description);
        reg.setULocation(Main.main.configId.location);
        reg.setUGroup(Main.main.configId.group);
        reg.setUConfig(FileUtils.readFileToString(Main.main.configFile, "UTF-8"));
        reg.setUStatus(Constants.REG_STATUS_ACTIVE);
        reg.setUCreatetime(GMTDate.getDate());
        reg.setUUpdatetime(GMTDate.getDate());
        reg.setOS(System.getProperty("os.name"));
        
        //save it
        ServiceHibernate.saveResolveRegistration(reg, "system");
        
        return "SUCCESS: Registration completed";
    } // registerSelf
    
    public void removeRegistration(String guid)
    {
        ResolveRegistrationVO reg = ServiceHibernate.findResolveRegistrationByGuid(guid);
        if(reg != null)
        {
            String[] sysIds = {reg.getSys_id()};
            try
            {
                ServiceHibernate.deleteResolveRegistrationByIds(sysIds, null, "system");
            }
            catch (Exception e)
            {
                Log.log.error("Error deleting the reg:", e);
            }
        }
    } // removeRegistration
    
    public void register(Map params) throws Exception
    {
        registerSelf();
    } // register
    
    public Map register(List params) throws Exception
    {
        try
        {
            for (Iterator i=(params).iterator(); i.hasNext(); )
            {
                Map regdata = (Map)i.next();
                
                // process registration entry
                processRegistration(regdata);
                Log.log.info("Registration completed");
            }
        }
        catch (Throwable e)
        {
            Log.log.warn(e.getMessage(), e);
        }
        
        return null;
    } // register

    void processRegistration(Map params) throws Exception
    {
        // init new registration
        ResolveRegistrationVO newReg = new ResolveRegistrationVO();
        newReg.initRegistration(params);
        Log.log.info("Process REGISTRATION entry: "+newReg.getUGuid());
            
        String guid = newReg.getUGuid();
        String name = newReg.getUName();
        String type = newReg.getUType();
        // check if registration exists using GUID
        ResolveRegistrationVO reg = ServiceHibernate.findResolveRegistrationByGuid(guid);
        boolean isRegModified = false;
        if(reg == null)
        {
            // new registration
            Log.log.info("New Registration: "+guid);
            reg = newReg;
            reg.setUStatus(Constants.REG_STATUS_ACTIVE);
            reg.setUGuid(newReg.getUGuid()); // NOTE: needed as it doesnt get across on serialization
            reg.setUCreatetime(GMTDate.getDate());
            reg.setUUpdatetime(GMTDate.getDate());
            
            if (reg.getUType().equalsIgnoreCase("RSVIEW"))
            {
                Map importReportParams = new HashMap();
                importReportParams.put(Constants.TARGET_GUID, reg.getUGuid());
                Main.esb.sendInternalMessage("RSMGMT", "MAction.importReports", importReportParams);
            }
            isRegModified = true;
        }
        else  
        {
            if (reg.isModified(newReg))
            {
                Log.log.info("Updated Registration: "+guid);
                reg.update(newReg);
                isRegModified = true;
            }
            else
            {
                Log.log.info("Refreshed Registration: "+guid);
            }
            reg.setUStatus(Constants.REG_STATUS_ACTIVE);
            reg.setUUpdatetime(GMTDate.getDate());
        }
        if(reg.getUType().equalsIgnoreCase("RSREMOTE")) {
            if(isRegModified || !configRSRemote.isRSRemoteActive(reg.getUGuid())) {
                processRSRemoteRegistration(guid, reg.getUConfig());
            }
        }
        //save it
        ServiceHibernate.saveResolveRegistration(reg, "system");
        
    } // processRegistration
    
    private void processRSRemoteRegistration(String guid, String rsRemoteRegConfig) throws DocumentException, SAXException {
        configRSRemote.load(rsRemoteRegConfig);
        Map rsremoteConfigParam = new HashMap();
        registrationId = Guid.getGuid(false);
        rsremoteConfigParam.put("GUID", guid);
        rsremoteConfigParam.put("registrationId", registrationId);
        rsremoteConfigParam.put("validQueue", configRSRemote.getValidQueue(guid));
        
        if (StringUtils.isNotBlank(configRSRemote.getOrg()))
        {
            rsremoteConfigParam.put("ORG", configRSRemote.getOrg());
        }
        
        Log.log.info("Process rsremote registery queues: " + guid + " - " + configRSRemote.getValidQueue(guid));
        Log.log.debug("Process rsremote registery queues all: "  + configRSRemote);
        Main.esb.sendMessage("RSCONTROLS", "MRegister.registerRSRemoteConfig", rsremoteConfigParam);
    }
    
    public static void registerRSRemoteConfig(Map rsremoteConfigParam) {
        String proccessRegistrationId = (String) rsremoteConfigParam.get("registrationId");
        if(registrationId == null || !proccessRegistrationId.equalsIgnoreCase(registrationId)) {
            String guid = (String)rsremoteConfigParam.get("GUID");
            Set<String> validQueues = (Set<String>) rsremoteConfigParam.get("validQueue");
            Log.log.info("Process rsremote registery queues: " + guid + " - " + validQueues);
            configRSRemote.load(guid, validQueues, (String)rsremoteConfigParam.get("ORG"));
            Log.log.debug("Process rsremote registery queues all: "  + configRSRemote);
        }
    }
    
    public static void syncRSRemoteRegistrations()
    {
        registrationId = Guid.getGuid(false);
        Map rsremoteConfigParam = new HashMap();
        rsremoteConfigParam.put("requestRegistrationId", registrationId);
        Log.log.info("sending request to sync rsremote registration: MRegister.syncRSRemoteRegistrations " + registrationId);
        Main.esb.sendMessage("RSCONTROLS", "MRegister.syncRSRemoteRegistrations", rsremoteConfigParam);
        
    }
    
    @SuppressWarnings("unchecked")
    public static void syncRSRemoteRegistrations(Map rsremoteConfigParam) {
        String requestRegistrationId = (String) rsremoteConfigParam.get("requestRegistrationId");
        // check whether this request was make by this rscontrol. if so skip it.
        if(registrationId == null || !registrationId.equals(requestRegistrationId)) {
            Map<String, Set<String>> validQueues = (Map<String, Set<String>>) rsremoteConfigParam.get("validQueues");
            String org = (String) rsremoteConfigParam.get("ORG");
            
         // valid queue == null meaning request to sync come from rscontrol that just come alive to this server
            if(validQueues == null) {
                //sending its available valid queues to all RSCONTROLS so other rscontrol can check against its valid queue caches and sync them up.
                registrationId = Guid.getGuid(false);
                rsremoteConfigParam = new HashMap();
                rsremoteConfigParam.put("requestRegistrationId", registrationId);
                rsremoteConfigParam.put("orginalRegistrationId", requestRegistrationId);
                rsremoteConfigParam.put("validQueues", configRSRemote.getValidQueues());
                
                if (StringUtils.isNotBlank(configRSRemote.getOrg()))
                {
                    rsremoteConfigParam.put("ORG", configRSRemote.getOrg());
                }
                
                Log.log.info("sending request to sync rsremote registration valid queues: MRegister.syncRSRemoteRegistrations " + configRSRemote);
                Main.esb.sendMessage("RSCONTROLS", "MRegister.syncRSRemoteRegistrations", rsremoteConfigParam);
            } else if(!validQueues.isEmpty()) {
                String originalRegistrationId = (String) rsremoteConfigParam.get("orginalRegistrationId");
                if(registrationId.equals(originalRegistrationId)) {
                    Log.log.info("process  rsremote registration valid queues sync: MRegister.syncRSRemoteRegistrations " + validQueues);
                    for(String queue : validQueues.keySet()) {
                        configRSRemote.load(queue, validQueues.get(queue), org);
                    }
                    Log.log.debug("process rsremote registration valid queues sync: MRegister.syncRSRemoteRegistrations " + configRSRemote);
                } else {
                    Log.log.debug("ignore this rsremote registration set because orginal registration id is " + originalRegistrationId + " != " + registrationId);
                    Log.log.debug("ignore valid queues: " + validQueues);
                }
            }
        } else {
            Log.log.debug("ignore this syncRSRemoteRegistrations request because requestRegistrationId is " + requestRegistrationId + " = " + registrationId);
        }
    }
    
    public static void unregisterRSRemoteConfig(Map unconfigParams) {
        String guid = (String) unconfigParams.get("GUID");
        ResolveRegistrationVO reg = ServiceHibernate.findResolveRegistrationByGuid(guid);
        reg.setUStatus(Constants.REG_STATUS_INACTIVE);
        ServiceHibernate.saveResolveRegistration(reg, "system");
        Log.log.info("unregister rsremote queues: " + guid + " - " + configRSRemote.getValidQueue(guid));
        configRSRemote.removeValidQueue(guid);
        Log.log.info("rsremote registery queues all: "  + configRSRemote);
    }
    
    public static boolean isValidQueue(String queueName) {
        return configRSRemote.isValidQueue(queueName);
    }
    
    public String logHeartbeat(Map params)
    {
        logHeartbeat();
        
        return "SUCCESS: Heartbeat completed";
    } // logHeartbeat
    
    public static void logHeartbeat()
    {
        Date time = new Date();
        Log.log.info("HEARTBEAT: " + sdf.format(time));
        System.out.println("HEARTBEAT:" + sdf.format(time));
    } // logHeartbeat 
    
    public static String getBlueprintData() {
        
        String blueprintData = "Error: No blueprint data found.";
        
        if(blueprintFile == null)
        {
            blueprintFile = FileUtils.getFile(Main.main.getResolveHome() + "/rsmgmt/config/blueprint.properties");
        }
        
        try
        {
            blueprintData = FileUtils.readFileToString(blueprintFile, "utf8");
        }
        catch (IOException e)
        {
            Log.log.error(e.getMessage(), e);
        }
        
        return blueprintData;
        
    }

    public static String getValidQueues()
    {
        return configRSRemote.toString();
    }

   

} // MRegister
