/******************************************************************************
* (C) Copyright 2014
*
* Resolve Systems, LLC
*
* All rights reserved
* This software is distributed under license and may not be used, copied,
* modified, or distributed without the express written permission of
* Resolve Systems, LLC.
*
******************************************************************************/

package com.resolve.rscontrol;

import java.util.Map;

import com.resolve.rsbase.MainBase;
import com.resolve.notification.NotificationAPI;
import com.resolve.notification.NotificationTarget;
import com.resolve.util.Constants;
import com.resolve.util.Log;
import com.resolve.util.StringUtils;

/**
 * This class is the receipient of notifications to be sent using various gateway (e.g., Email, Exchange etc.).
 * Mainly used by components which doesn't have direct access to the Resolve database (e.g., RSRemote etc.) and sent over 
 * ESB to RSCONTROL queue.
 * 
 */
public class MNotification extends NotificationAPI
{
    /**
     * Submits a notification message to the ESB that will get picked up by a
     * gateway deployed in certain RSRemote instance.
     * 
     * @param params
     *            , is a map with all the information needed for an email or IM. Following keys are expected:
     *            {@link Constants.NOTIFICATION_CARRIER_TYPE}
     *            {@link Constants.NOTIFICATION_MESSAGE_FROM}
     *            {@link Constants.NOTIFICATION_MESSAGE_TO}
     *            {@link Constants.NOTIFICATION_MESSAGE_CC}
     *            {@link Constants.NOTIFICATION_MESSAGE_BCC}
     *            {@link Constants.NOTIFICATION_MESSAGE_SUBJECT}
     *            {@link Constants.NOTIFICATION_MESSAGE_CONTENT}
     *            {@link Constants.NOTIFICATION_MESSAGE_ATTACHMENTS} value must be the byte array. 
     */
    public static void sendMessage(Map<String, Object> params)
    {
        try
        {
            String to = null;
            if(params.get(Constants.NOTIFICATION_MESSAGE_TO) != null)
            {
                to = (String) params.get(Constants.NOTIFICATION_MESSAGE_TO);
            }
            
            if(StringUtils.isNotEmpty(to) && targets.size() > 0)
            {
                String classMethod = null;
                for(NotificationTarget target : targets)
                {
                    if(target.carrierType.equals(Constants.NOTIFICATION_CARRIER_TYPE_EMAIL))
                    {
                        classMethod = "MEmail.sendMessage";
                    }
                    else if(target.carrierType.equals(Constants.NOTIFICATION_CARRIER_TYPE_EXCHANGE))
                    {
                        classMethod = "MExchange.sendMessage";
                    }
                    else if(target.carrierType.equals(Constants.NOTIFICATION_CARRIER_TYPE_XMPP))
                    {
                        classMethod = "MXMPP.sendMessage";
                    }
                    else if(target.carrierType.equals(Constants.NOTIFICATION_CARRIER_TYPE_EWS))
                    {
                        classMethod = "MEWS.sendMessage";
                    }
                    
                    //Submit to the ESB
                    boolean isSuccess = MainBase.esb.sendMessage(target.gateway + "_TOPIC", classMethod, params);
                    if(isSuccess)
                    {   
                        //TODO as soon as the first gateway is used, get out of here. However, in the future there will be
                        //more logic
                        break;
                    }
                    else
                    {
                        Log.log.warn("The Gateway: " + target.gateway + " is invalid, may be bad data in resolve_blueprint. Ignoring.");
                    }
                }
            }
        }
        catch (Exception e)
        {
            Log.log.error(e.getMessage(), e);
        }
    }
}
