/******************************************************************************
* (C) Copyright 2014
*
* Resolve Systems, LLC
*
* All rights reserved
* This software is distributed under license and may not be used, copied,
* modified, or distributed without the express written permission of
* Resolve Systems, LLC.
*
******************************************************************************/

package com.resolve.rscontrol;

public class ReturnUserErrorException extends Exception 
{
	private static final long serialVersionUID = -8879911793732255632L;

	public ReturnUserErrorException(String msg)
	{
		super(msg);
	} // ReturnUserErrorException
	
	public ReturnUserErrorException(Exception e)
	{
		super(e);
	} // ReturnUserErrorException
	
} // ReturnUserErrorException
