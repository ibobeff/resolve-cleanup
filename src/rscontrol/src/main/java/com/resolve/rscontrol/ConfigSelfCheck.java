package com.resolve.rscontrol;

import com.resolve.util.ConfigMap;
import com.resolve.util.XDoc;

public class ConfigSelfCheck extends ConfigMap
{
    private static final long serialVersionUID = 4256218062089210270L;
	// DB Self Check
    boolean dbSCActive;
    int dbSCInterval;
    int dbSCTimeout;
    // Elacticsearch Self Check
    boolean esSCActive;
    int esSCInterval;
    int esSCRetryCount;
    int esSCRetryDelay;
    // Ping RSREMOTE
    boolean pingActive;
    int pingThreshold;
    int pingInterval;
    // Max thread pool size Slef Check
    int mtpsSCInterval;
    // RB Excecution Self Check
    boolean rbExecSCActive;
    int rbExecSCInterval;
    String rbExecSCSchedule;
    String rbExecSCRunbook;

    public ConfigSelfCheck(XDoc config) throws Exception
    {
        super(config);
        
        define("dbSCActive", BOOLEAN, "./SELFCHECK/DB/@ACTIVE");
        define("dbSCInterval", INTEGER, "./SELFCHECK/DB/@INTERVAL");
        define("dbSCTimeout", INTEGER, "./SELFCHECK/DB/@TIMEOUT");
        
        define("pingActive", BOOLEAN, "./SELFCHECK/PING/@ACTIVE");
        define("pingInterval", INTEGER, "./SELFCHECK/PING/@INTERVAL");
        define("pingThreshold", INTEGER, "./SELFCHECK/PING/@THRESHOLD");
        
        define("esSCActive", BOOLEAN, "./SELFCHECK/ES/@ACTIVE");
        define("esSCInterval", INTEGER, "./SELFCHECK/ES/@INTERVAL");
        define("esSCRetryCount", INTEGER, "./SELFCHECK/ES/@RETRY");
        define("esSCRetryDelay", INTEGER, "./SELFCHECK/ES/@RETRYDELAY");
        
        define("mtpsSCInterval", INTEGER, "./SELFCHECK/THREADPOOL/@INTERVAL");
        
        define("rbExecSCActive", BOOLEAN, "./SELFCHECK/RBEXEC/@ACTIVE");
        define("rbExecSCInterval", INTEGER, "./SELFCHECK/RBEXEC/@INTERVAL");
        define("rbExecSCSchedule", STRING, "./SELFCHECK/RBEXEC/@SCHEDULEs");
        define("rbExecSCRunbook", STRING, "./SELFCHECK/RBEXEC/@RUNBOOK");
    }
    
    @Override
    public void load() throws Exception
    {
        loadAttributes();
    }

    @Override
    public void save() throws Exception
    {
        saveAttributes();
    }

    public boolean isDbSCActive()
    {
        return dbSCActive;
    }

    public void setDbSCActive(boolean dbSCActive)
    {
        this.dbSCActive = dbSCActive;
    }

    public int getDbSCInterval()
    {
        return dbSCInterval;
    }

    public void setDbSCInterval(int dbSCInterval)
    {
        this.dbSCInterval = dbSCInterval;
    }

    public int getDbSCTimeout()
    {
        return dbSCTimeout;
    }

    public void setDbSCTimeout(int dbSCTimeout)
    {
        this.dbSCTimeout = dbSCTimeout;
    }

    public boolean isPingActive()
    {
        return pingActive;
    }

    public void setPingActive(boolean pingActive)
    {
        this.pingActive = pingActive;
    }

    public int getPingThreshold()
    {
        return pingThreshold;
    }

    public void setPingThreshold(int pingThreshold)
    {
        this.pingThreshold = pingThreshold;
    }

    public int getPingInterval()
    {
        return pingInterval;
    }

    public void setPingInterval(int pingInterval)
    {
        this.pingInterval = pingInterval;
    }

    public boolean isEsSCActive()
    {
        return esSCActive;
    }

    public void setEsSCActive(boolean esSCActive)
    {
        this.esSCActive = esSCActive;
    }

    public int getEsSCInterval()
    {
        return esSCInterval;
    }

    public void setEsSCInterval(int esSCInterval)
    {
        this.esSCInterval = esSCInterval;
    }

    public int getEsSCRetryCount()
    {
        return esSCRetryCount;
    }

    public void setEsSCRetryCount(int esSCRetryCount)
    {
        this.esSCRetryCount = esSCRetryCount;
    }

    public int getEsSCRetryDelay()
    {
        return esSCRetryDelay;
    }

    public void setEsSCRetryDelay(int esSCRetryDelay)
    {
        this.esSCRetryDelay = esSCRetryDelay;
    }

    public int getMtpsSCInterval()
    {
        return mtpsSCInterval;
    }

    public void setMtpsSCInterval(int mtpsSCInterval)
    {
        this.mtpsSCInterval = mtpsSCInterval;
    }

    public boolean isRbExecSCActive()
    {
        return rbExecSCActive;
    }

    public void setRbExecSCActive(boolean rbExecSCActive)
    {
        this.rbExecSCActive = rbExecSCActive;
    }

    public int getRbExecSCInterval()
    {
        return rbExecSCInterval;
    }

    public void setRbExecSCInterval(int rbExecSCInterval)
    {
        this.rbExecSCInterval = rbExecSCInterval;
    }

    public String getRbExecSCSchedule()
    {
        return rbExecSCSchedule;
    }

    public void setRbExecSCSchedule(String rbExecSCSchedule)
    {
        this.rbExecSCSchedule = rbExecSCSchedule;
    }

    public String getRbExecSCRunbook()
    {
        return rbExecSCRunbook;
    }

    public void setRbExecSCRunbook(String rbExecSCRunbook)
    {
        this.rbExecSCRunbook = rbExecSCRunbook;
    }
}
