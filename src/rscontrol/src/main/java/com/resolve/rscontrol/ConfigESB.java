/******************************************************************************
* (C) Copyright 2014
*
* Resolve Systems, LLC
*
* All rights reserved
* This software is distributed under license and may not be used, copied,
* modified, or distributed without the express written permission of
* Resolve Systems, LLC.
*
******************************************************************************/

package com.resolve.rscontrol;

import java.util.Hashtable;
import java.util.Iterator;
import java.util.Map;

import com.resolve.util.Log;
import com.resolve.util.XDoc;

public class ConfigESB extends com.resolve.rsbase.ConfigESB
{
    private static final long serialVersionUID = 6097080005654574104L;

    public ConfigESB(XDoc config) throws Exception
    {
        super(config);
    } // ConfigESB
    
    public void load() throws Exception
    {
        super.load();
    } // load

    public void save() throws Exception
    {
        super.save();
    } // save
    
    @SuppressWarnings("rawtypes")
    public void start()
    {
        try
        {
            for (Iterator i=publications.iterator(); i.hasNext(); )
            {
                Hashtable group = (Hashtable)i.next();
                String name = (String)group.get("name");
                
                // add publication
                Main.main.mServer.createPublication(name);
            }
        }
        catch (Exception e)
        {
            Log.log.warn(e.getMessage(), e);
        }
    } // start
    
    @SuppressWarnings("rawtypes")
    protected Hashtable getPublication(String name)
    {
        Hashtable result = null;
        boolean found = false;
        
        for (Iterator i=publications.iterator(); !found && i.hasNext(); )
        {
            Hashtable entry = (Hashtable)i.next();
            if (name.equalsIgnoreCase((String)entry.get("NAME")))
            {
                result = entry;
                found = true;
            }
        }
        
        return result;
    } // getPublication
    
    @SuppressWarnings({ "unchecked", "rawtypes" })
    protected void addPublication(Map params)
    {
        String name = (String)params.get("NAME");
        
        removePublication(name);
        publications.add(new Hashtable(params));
    } // addPublication
    
    @SuppressWarnings("rawtypes")
    protected void removePublication(String name)
    {
        boolean found = false;
        
        for (Iterator i=publications.iterator(); !found && i.hasNext(); )
        {
            Map entry = (Map)i.next();
            if (name.equalsIgnoreCase((String)entry.get("NAME")))
            {
                i.remove();
                found = true;
            }
        }
    } // removePublication
    
    @SuppressWarnings({ "rawtypes", "unchecked" })
    public Hashtable getPublications()
    {
        Hashtable result = new Hashtable();
        
        for (Iterator i=publications.iterator(); i.hasNext(); )
        {
            Map entry = (Map)i.next();
            String group = (String)entry.get("NAME");
            result.put(group.toUpperCase(), "");
        }
        
        return result;
    } // getPublications
    
    @SuppressWarnings({ "rawtypes", "unchecked" })
    public boolean publish(String group) throws Exception
    {
        boolean result = false;
        
        // check if entry already exists
        boolean found = false;
        for (Iterator i=publications.iterator(); !found && i.hasNext(); )
        {
            Map entry = (Map)i.next();
            if (group.equalsIgnoreCase((String)entry.get("NAME")))
            {
                found = true;
            }
        }
            
        // add only if not exists
        if (!found)
        {
            Hashtable entry = new Hashtable();
            entry.put("NAME", group.toUpperCase());
            publications.add(entry);
                
            result = Main.main.mServer.createPublication(group);
        }
        
        return result;
    } // publish
    
    public boolean unpublish(String group) throws Exception
    {
        boolean result = false;
        
        result = Main.main.mServer.removePublication(group);
        if (result)
        {
            removePublication(group);
            result = true;
        }
        return result;
    } // unpublish
} // ConfigESB
