/******************************************************************************
* (C) Copyright 2014
*
* Resolve Systems, LLC
*
* All rights reserved
* This software is distributed under license and may not be used, copied,
* modified, or distributed without the express written permission of
* Resolve Systems, LLC.
*
******************************************************************************/

package com.resolve.rscontrol;

import java.io.DataInput;
import java.io.DataOutput;
import java.io.IOException;
import java.io.Serializable;

import com.resolve.util.Log;

public class ExecuteParameter implements Serializable
{
	private static final long serialVersionUID = 2113363038580320570L;
	String name = "";
    String type = "";
    String value = "";
    boolean encrypt = false;
    String prefix = "";
    String prefixSeparator = "";
    
    // lookup fields
    String processid;
    String executeid;
    
    public ExecuteParameter()
    {
    } // ExecuteParameter
    
    public String getId()
    {
        return processid + "/" + executeid + "/" + name + "/" + type;
    } // getId
    
    public static String getId(String processid, String executeid, String name, String type)
    {
        return processid + "/" + executeid + "/" + name + "/" + type;
    } // getId
    
    public void writeData(DataOutput out) throws IOException 
    {
	    out.writeUTF(name);
	    out.writeUTF(type);
	    out.writeUTF(value);
	    out.writeBoolean(encrypt);
	    out.writeUTF(prefix);
	    out.writeUTF(prefixSeparator);
	    out.writeUTF(processid);
	    out.writeUTF(executeid);
    } // writeData

    public void readData (DataInput in) throws IOException 
    {
	    name = in.readUTF();
	    type = in.readUTF();
	    value = in.readUTF();
	    encrypt = in.readBoolean();
	    prefix = in.readUTF();
	    prefixSeparator = in.readUTF();
	    processid = in.readUTF();
	    executeid = in.readUTF();
    } // readData
    
    public String toString()
    {
        String result = "";
        
        result += "[";
        result += "name: "+name+", ";
        result += "type: "+type+", ";
        result += "value: "+value+", ";
        result += "processid: "+processid+", ";
        result += "executeid: "+executeid;
        result += "]";
        
        return result;
    } // toString

    public String getName()
    {
        return name;
    }
    public void setName(String name)
    {
        if (name == null)
        {
            name = "";
        }
        this.name = name;
    }
    public String getType()
    {
        return type;
    }
    public void setType(String type)
    {
        if (type == null)
        {
            type = "";
        }
        this.type = type;
    }
    public String getValue()
    {
        return value;
    }
    public void setValue(String value)
    {
        if (value == null)
        {
            value = "";
        }
        this.value = value;
    }
    public boolean isEncrypt()
    {
        return encrypt;
    }
    public void setEncrypt(boolean encrypt)
    {
        this.encrypt = encrypt;
    }
    public void setEncrypt(Boolean encrypt)
    {
        if (encrypt == null)
        {
            this.encrypt = false;
        }
        else
        {
	        this.encrypt = encrypt;
        }
    }
    public String getPrefix()
    {
        return prefix;
    }
    public void setPrefix(String prefix)
    {
        if (prefix == null)
        {
            prefix = "";
        }
        this.prefix = prefix;
    }
    public String getPrefixSeparator()
    {
        return prefixSeparator;
    }
    public void setPrefixSeparator(String prefixSeparator)
    {
        if (prefixSeparator == null)
        {
            prefixSeparator = "";
        }
        this.prefixSeparator = prefixSeparator;
    }
    public String getProcessid()
    {
        return processid;
    }
    public void setProcessid(String processid)
    {
        if (processid == null)
        {
            processid = "";
        }
        this.processid = processid;
    }
    public String getExecuteid()
    {
        return executeid;
    }
    public void setExecuteid(String executeid)
    {
        if (executeid == null)
        {
            executeid = "";
        }
        this.executeid = executeid;
    }
    
} // ExecuteParameter
