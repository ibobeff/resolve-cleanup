/******************************************************************************
* (C) Copyright 2014
*
* Resolve Systems, LLC
*
* All rights reserved
* This software is distributed under license and may not be used, copied,
* modified, or distributed without the express written permission of
* Resolve Systems, LLC.
*
******************************************************************************/

package com.resolve.rscontrol;

import java.util.concurrent.atomic.AtomicInteger;
import java.util.concurrent.atomic.AtomicLong;

public class MetricUnitActionTask
{
    String name;
    AtomicLong totalValue;
    AtomicInteger totalCount;
    
    public MetricUnitActionTask(String name)
    {
        this.name = name;
        this.totalValue = new AtomicLong(0);
        this.totalCount = new AtomicInteger(0);
    } // MetricUnitActionTask
    
    public void addValue(long value, int count)
    {
        totalValue.addAndGet(value);
        totalCount.addAndGet(count);
    } // addValue
    
    public void reset()
    {
        totalValue.set(0);
        totalCount.set(0);
    } // reset
    
    public String getName()
    {
        return name;
    }

    public AtomicLong getTotalValue()
    {
        return totalValue;
    }

    public AtomicInteger getTotalCount()
    {
        return totalCount;
    }
} // MetricUnitActionTask
