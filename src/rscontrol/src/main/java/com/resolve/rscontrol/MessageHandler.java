/******************************************************************************
* (C) Copyright 2014
*
* Resolve Systems, LLC
*
* All rights reserved
* This software is distributed under license and may not be used, copied,
* modified, or distributed without the express written permission of
* Resolve Systems, LLC.
*
******************************************************************************/

package com.resolve.rscontrol;

import java.util.HashMap;
import java.util.Map;
import java.util.Queue;
import java.util.concurrent.ConcurrentLinkedQueue;

import com.resolve.esb.DefaultHandler;
import com.resolve.esb.MMsgHeader;
import com.resolve.esb.MSender;
import com.resolve.persistence.util.HibernateUtil;
import com.resolve.thread.ScheduledExecutor;
import com.resolve.util.Constants;
import com.resolve.util.ExecutableObject;
import com.resolve.util.Log;
import com.resolve.util.StringUtils;
import com.resolve.thread.TaskExecutor;
import com.resolve.thread.ScheduledTask;

public class MessageHandler extends DefaultHandler
{
    public MessageHandler(MSender mSender, String serviceName)
    {
        super(mSender, serviceName);
    } // MessageHandler

    private static String getRunbookLimitFromStartTask(String wiki)
    {
        String processXML = null;
        String result = null;
        try
        {
            if (!ProcessRequest.cacheActionProcessDependency.containsKey(wiki))
            {
              processXML = ProcessRequest.getWikiDocumentModelXML(wiki, Constants.MODEL_TYPE_MAIN);
            }
            // generate main dependency tree
            ActionProcessDependency processDependency = ProcessRequest.getActionProcessDependency(wiki, processXML, Constants.MODEL_TYPE_MAIN);

            if (processDependency != null)
            {
                Map newParams = new HashMap();
                ActionProcessTask startTask = (ActionProcessTask) processDependency.getModel().getStartNode().getValue();
                Object obj = startTask.getParams().get(Constants.EXECUTE_PROCESS_CONCURRENT_LIMIT);
                if (obj!=null)
                {
                    result = obj.toString();
                }
            }
        }
        catch (Throwable e)
        {
            Log.log.error(e.getMessage(), e);
        }
        return result;
    }
    
    public static String getConcurrentRBLimit(String wiki)
    {
    	String mapName = Constants.PROCESS_CONCURRENT_LIMIT_MAP + wiki;
    	Integer obj = HibernateUtil.getJavanNumberOnly(mapName);
    	if (obj==null)
    	{
    		return null;
    	}
    	return obj.toString();
    }

    public static void setConcurrentRBLimit(String wiki, String newLimit)
    {
    	String mapName = Constants.PROCESS_CONCURRENT_LIMIT_MAP + wiki;
    	HibernateUtil.saveJavaNumberOnly(mapName, Integer.valueOf(newLimit));
    }

    public static Integer getConcurrentRBCounter(String wiki)
    {
    	String mapName = Constants.EXECUTE_PROCESS_CONCURRENT_LIMIT + wiki;
    	
    	Integer num =  HibernateUtil.getJavanNumberOnly(mapName);
    	if (num!=null)
    	{
    		return num;
    	}
    	else
    	{
    		HibernateUtil.saveJavaNumberOnly(mapName, 0);
    		return 0;
    	}
    }

    public static void setConcurrentRBCounter(String wiki, Integer t)
    {
    	String mapName = Constants.EXECUTE_PROCESS_CONCURRENT_LIMIT + wiki;
    	HibernateUtil.saveJavaNumberOnly(mapName, t);
    }
    
    public void execute(MMsgHeader msgHeader, Object obj) 
    {
        try 
        {
            String className = msgHeader.getDest().getClassname();
            String methodName = msgHeader.getDest().getMethodname();
            Class invokeClass = (Class)msgHeader.get("INVOKECLASS");

            final ScheduledTask task = new ScheduledTask(ScheduledExecutor.getInstance(), "", msgHeader, invokeClass, methodName, obj);

            boolean addedToQueue = false;
            String wiki = null;
            if ("executeProcess".equals(methodName))
            {
                Map params = (Map)obj;
                wiki = (String)params.get(Constants.EXECUTE_WIKI);           // required
                String runbookLimit = (String)params.get(Constants.EXECUTE_PROCESS_CONCURRENT_LIMIT);
                
                if( wiki!=null && !wiki.startsWith(Constants.EXECUTE_ABORTWIKI))
                {
                    if (StringUtils.isEmpty(runbookLimit))
                    {
                        runbookLimit = getRunbookLimitFromStartTask(wiki); 
                    }
                    
    		        if ((!StringUtils.isEmpty(runbookLimit)) && (!runbookLimit.equals("0")) && (!StringUtils.isEmpty(wiki)))
                    {
    		            synchronized (this.getClass())
    		            {
                            int oldLimit = -1;
                            String oldLimitStr = getConcurrentRBLimit(wiki);
                            if (StringUtils.isNotEmpty(oldLimitStr))
                            {
                                oldLimit = Integer.parseInt(oldLimitStr);
                            }
        		            
        		            final int limit = Integer.parseInt(runbookLimit);
        		            if (oldLimit!=limit)
        		            {
        		            	setConcurrentRBLimit(wiki, runbookLimit);
        		            }
        
        		            if (Log.log.isDebugEnabled()) {
        		            	Log.log.debug(String.format("MessageHandler: try to acquire limit lock. wiki=%s, runbookLimit=%s", 
                            								wiki, runbookLimit));
        		            }
                            
                            final String fwiki = wiki;
                            final String frunbookLimit = runbookLimit;
                            final int foldLimit = oldLimit;
                            ExecutableObject exeObj = new ExecutableObject()
                            {
                            	public Object execute()
                            	{
                                    boolean added = false;
        
        	                    	int count = getConcurrentRBCounter(fwiki);
        	                    	
        	                    	if (Log.log.isDebugEnabled()) {
        	                    		Log.log.debug(String.format("MessageHandler: acquired limit lock. wiki=%s, " +
        	                        						    	"current counter value=%d", fwiki, count));
        	                    	}
        	                    	
        	                        if (count>=limit)
        	                        {
        	                            Queue<ScheduledTask> queue = MAction.waitingRunbookQueues.putIfAbsent(fwiki, new ConcurrentLinkedQueue<ScheduledTask>());
        	                            if (queue==null)
        	                            {
        	                                queue=MAction.waitingRunbookQueues.get(fwiki);
        	                            }
        	                            queue.add(task);
        	                            added = true;
        	                        }
        	                        else
        	                        {
        	                        	count++;
        	                        	setConcurrentRBCounter(fwiki, count);
        	                        }
        	                        
        	                        if (Log.log.isDebugEnabled()) {
        	                        	Log.log.debug(String.format("MessageHandler: runbook has concurrent limit. wiki=%s, " +
        	                        								"counter=%d, addedToQueue=%b, runbookLimit=%s", fwiki, 
        	                        								count, added, frunbookLimit));
        	                        }
                            
        		                    // Limit has been increased
        		                    if ((foldLimit>=0) && (foldLimit<limit))
        		                    {
        		                        Queue<ScheduledTask> queue = MAction.waitingRunbookQueues.get(fwiki);
        		                        if (queue != null)
        		                        {
        		                            count = getConcurrentRBCounter(fwiki);
        		                            ++count;
        		                            setConcurrentRBCounter(fwiki, count);
        		                            
        		                        	while (!queue.isEmpty() && count <= limit)
        		                            {
        		                        		ScheduledTask ta = queue.poll();
        		                                if (ta != null)
        		                                {
        		                                	TaskExecutor.execute(ta.jobid, ta.header, ta.invokeClass, ta.methodname, ta.params);
        		                                    
        		                                    if (Log.log.isDebugEnabled()) {
        		                                    	Log.log.debug(String.format("removeProcessReplicated: a runbook request" +
        		                                    								" for %s has been removed from queue. " +
        		                                    								"Queue length=%d", fwiki, queue.size()));
        		                                    }
        		                                }
        		                                count = getConcurrentRBCounter(fwiki);
        		                                ++count;
        		                                setConcurrentRBCounter(fwiki, count);
        		                            }
        		                        }
        		                    }
        		                    return added;
                                }
                            };    
                            addedToQueue = (Boolean)HibernateUtil.exclusiveClusterExecution(Constants.EXECUTE_PROCESS_CONCURRENT_LIMIT + wiki, exeObj);
    		            }
                    }
                }
            }
            
            Object newMsg = null;
            if (!addedToQueue)
            {
                newMsg = task.execute();
            }
            else
            {
                newMsg = String.format("Runbook execution request was put on waiting queue because execution limit was " +
                					   "reached. wiki=%s", wiki);
                
            }
            
            // forward results to next destination
            if (newMsg != null && mSender != null)
            {
                if (msgHeader.hasNextRouteDest())
                {
                    msgHeader.nextRouteDest();
                    //MMsgHeader newHeader = new MMsgHeader(msgHeader, className+"."+methodName);
                    MMsgHeader newHeader = new MMsgHeader(msgHeader, serviceName);
                    mSender.sendMessage(newHeader, newMsg);
                }
            }
        } 
        catch (Exception e) 
        {
            Log.log.warn(String.format("Failed to execute method handler: %s", e.getMessage()), e);
        } 
    } // execute
    
}
