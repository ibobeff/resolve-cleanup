/******************************************************************************
* (C) Copyright 2014
*
* Resolve Systems, LLC
*
* All rights reserved
* This software is distributed under license and may not be used, copied,
* modified, or distributed without the express written permission of
* Resolve Systems, LLC.
*
******************************************************************************/

package com.resolve.rscontrol;

import java.util.Map;

import org.quartz.JobExecutionContext;
import org.quartz.JobExecutionException;

import com.resolve.rsbase.MainBase;
import com.resolve.cron.CronExec;
import com.resolve.rsbase.MService;
import com.resolve.services.util.ExecuteUtil;
import com.resolve.util.Log;
import com.resolve.util.StringUtils;
import com.resolve.thread.TaskExecutor;

public class CronActionTrigger extends CronExec
{
    public CronActionTrigger()
    {
    } // ActionTrigger
    
    @SuppressWarnings({ "rawtypes", "static-access" })
    public void execute(JobExecutionContext context) throws JobExecutionException
    {
        super.execute(context);

        try
        {
	        data = context.getJobDetail().getJobDataMap();
	        if (data != null)
	        {
	            //data from ResolveCron model
	            String name = getJobData("NAME"); // name of the job scheduler like 'DOCUMENT ARCHIVE CHECK'
	            String wiki = getJobData("WIKI"); // will be command like 'RSVIEWS#MSocial.dailyEmail' or a wiki document like 'EventCatalog.UpdateEventHistory'
	            String paramsStr = getJobData("PARAMS");
				Map params = StringUtils.urlToMap(paramsStr, false);                                        
                Main main = (Main)MainBase.main;
	            if (main.isMaster && main.isActive())
	            {
	                if (wiki.startsWith("MService."))
	                {
	                	TaskExecutor.execute(new MService(), wiki, params);
	                }
	                else if (wiki.indexOf('#') > 0)
	                {
	                    ExecuteUtil.sendESBRequest(wiki, params);
	                }
	                else
	                {
	                    ExecuteUtil.sendExecuteProcess(name, wiki, params);
	                }
	            }
	        }
        }
        catch (Exception e)
        {
            Log.log.error("Failed to execute CronActionTrigger: "+e.getMessage(), e);
        }
    } // execute

} // ActionTrigger

