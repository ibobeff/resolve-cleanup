/******************************************************************************
 * (C) Copyright 2014
 *
 * Resolve Systems, LLC
 *
 * All rights reserved
 * This software is distributed under license and may not be used, copied,
 * modified, or distributed without the express written permission of
 * Resolve Systems, LLC.
 *
 ******************************************************************************/

package com.resolve.rscontrol;

import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Queue;
import java.util.Set;
import java.util.concurrent.ConcurrentHashMap;
import java.util.stream.Collectors;

import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang3.tuple.Pair;

import com.resolve.esb.MListener;
import com.resolve.esb.amqp.MAmqpRunbookExecutionListener;
import com.resolve.esb.amqp.MAmqpServer;
import com.resolve.persistence.model.ArchiveWorksheet;
import com.resolve.persistence.util.HibernateUtil;import com.resolve.rsbase.MainBase;
import com.resolve.search.APIFactory;
import com.resolve.search.SearchException;
import com.resolve.search.model.ExecuteState;
import com.resolve.search.model.ExecutionSummary;
import com.resolve.search.model.TaskResult;
import com.resolve.search.model.Worksheet;
import com.resolve.search.worksheet.WorksheetSearchAPI;
import com.resolve.services.ServiceHibernate;
import com.resolve.services.ServiceWorksheet;
import com.resolve.services.constants.ConstantValues;
import com.resolve.services.constants.WikiDocStatsCounterName;
import com.resolve.services.hibernate.util.ActionTaskUtil;
import com.resolve.services.hibernate.util.GatewayFilterUtil;
import com.resolve.services.hibernate.util.GatewayUtil;
import com.resolve.services.hibernate.util.MSGGatewayFilterUtil;
import com.resolve.services.hibernate.util.OrgsUtil;
import com.resolve.services.hibernate.util.PullGatewayFilterUtil;
import com.resolve.services.hibernate.util.PushGatewayFilterUtil;
import com.resolve.services.hibernate.util.UserUtils;
import com.resolve.services.hibernate.util.WikiUtils;
import com.resolve.services.hibernate.vo.GatewayVO;
import com.resolve.services.hibernate.vo.OrgsVO;
import com.resolve.services.hibernate.vo.WikiDocumentVO;
import com.resolve.services.hibernate.wiki.SaveHelper;
import com.resolve.services.interfaces.VO;
import com.resolve.services.util.PlaybookUtils;
import com.resolve.services.util.WorksheetUtil;
import com.resolve.services.util.WorksheetUtils;
import com.resolve.services.vo.QueryDTO;
import com.resolve.services.vo.QueryFilter;
import com.resolve.services.vo.ResponseDTO;
import com.resolve.thread.ScheduledTask;
import com.resolve.util.Constants;
import com.resolve.util.ERR;
import com.resolve.util.EncryptionTransportObject;
import com.resolve.util.LicenseEnum;
import com.resolve.util.Log;
import com.resolve.util.PerformanceDebug;
import com.resolve.util.ScriptDebug;
import com.resolve.util.StringUtils;
import com.resolve.util.constants.HibernateConstants;
import com.resolve.util.eventsourcing.EventMessage;
import com.resolve.util.eventsourcing.EventMessageFactory;
import com.resolve.util.performance.ExecutionTimeUtil;
import com.resolve.util.performance.datasources.ExecutionStep;
import com.resolve.util.performance.datasources.ExecutionStepPhase;

public class MAction
{
	private static final String ERR_MSG_PREFIX = "Error : ";
	private static final String AT_SYNCHRONOUSLY = "Action Task synchronously";
	private static final String EVENT_WITH_DETAILS = "Event with event id %s, even reference %s, and process id %s";
	private static final String RECEIVED_INVALID_ORG_INEXECUTE_REQUEST = 
																	"Received invalid %s in execute %s request params [%s]";
	private static final String RUNBOOK_WITH_DETAILS = "%s Runbook with problem id %s from user %s";
	private static final String AT_ASYNCHRONOUSLY = "Action Task %s with problem id %s from user %s asynchronously";
	
    public static ConcurrentHashMap<String, Queue<ScheduledTask>> waitingRunbookQueues = new ConcurrentHashMap<String, Queue<ScheduledTask>>();
    
    public MAction()
    {
    } // MAction

    // Used by synchronous call to get the worksheet id back
    public static Map<String, Object> executeRunbook(Map<String, Object> params)
    {
        String processId = executeProcess(params);
        String wiki = (String) params.get(Constants.EXECUTE_WIKI);
        String problemId = (String) params.get(Constants.EXECUTE_PROBLEMID);

        Log.log.info("problemId = " + problemId);
        Log.log.info("processId = " + processId);

        Map<String, Object> result = new HashMap<String, Object>();

        if (StringUtils.isNotBlank(processId))
        {
            result.put(Constants.EXECUTE_PROCESSID, processId);
        }
        
        result.put(Constants.EXECUTE_PROBLEMID, problemId);
        result.put(Constants.EXECUTE_WIKI, wiki);
        
        if (StringUtils.isNotBlank((String)params.get(Constants.EXECUTE_ORG_ID)))
        {
            result.put(Constants.EXECUTE_ORG_ID, params.get(Constants.EXECUTE_ORG_ID)); 
        }
        
        if (StringUtils.isNotBlank((String)params.get(Constants.EXECUTE_ORG_NAME)))
        {
            result.put(Constants.EXECUTE_ORG_NAME, params.get(Constants.EXECUTE_ORG_NAME)); 
        }
        
        Worksheet worksheet = ServiceWorksheet.findWorksheetById(problemId/*, (String)params.get(Constants.EXECUTE_USERID)*/);

        if (worksheet != null)
        {
            String condition = worksheet.getCondition();
            String severity = worksheet.getSeverity();
            String number = worksheet.getNumber();
    
            result.put(Constants.STATUS_CONDITION, condition);
            result.put(Constants.STATUS_SEVERITY, severity);
            result.put(Constants.WORKSHEET_NUMBER, number);
        }

        return result;
    } // executeRunbook()

    // Used by synchronous call to get the detailed result back
    public static Map<String, Object> getRunbookResult(Map<String, Object> params)
    {
        String problemId = null;
        String processId = null;
        String orgId = null;
        String orgName = null;
        Pair<String, Integer> statusAndATExecCount = null;
        long processCompletedCheckTime = 0l;
        long timeout = 100;
        
        if (params != null)
        {
            problemId = (String) params.get(Constants.EXECUTE_PROBLEMID);
            processId = (String) params.get(Constants.EXECUTE_PROCESSID);
            orgId = (String) params.get(Constants.EXECUTE_ORG_ID);
            orgName = (String) params.get(Constants.EXECUTE_ORG_NAME);

            String processTimeoutStr = "10";
            if(params.get(Constants.EXECUTE_PROCESS_TIMEOUT) instanceof String) {
                processTimeoutStr = (String) params.get(Constants.EXECUTE_PROCESS_TIMEOUT); // optional
            } else if (params.get(Constants.EXECUTE_PROCESS_TIMEOUT) instanceof Integer) {
                processTimeoutStr = "" + params.get(Constants.EXECUTE_PROCESS_TIMEOUT);
            }
            if (StringUtils.isNotEmpty(processTimeoutStr)) timeout = Long.parseLong(processTimeoutStr) * 60;

            long procCompleteCheckStartTime = System.currentTimeMillis();
            statusAndATExecCount = waitForProcessToComplete(problemId, processId, timeout, orgId, orgName);
            processCompletedCheckTime = System.currentTimeMillis() - procCompleteCheckStartTime;
        }
        
        return generateResult(problemId, processId, statusAndATExecCount, orgId, orgName, 
        					  (timeout * 1000) - processCompletedCheckTime);
    }

    private static Map<String, Object> generateResult(String problemId, String processId, 
    												  Pair<String, Integer> statusAndATExecCount, 
    												  String orgId, String orgName, long timeOut)
    {
    	long mStartTime = System.currentTimeMillis();
        Map<String, Object> result = new LinkedHashMap<String, Object>();

        if (Log.log.isDebugEnabled()) {
        	Log.log.debug(String.format("generateResult(Status: %s, AT Exec Count: %d, Problem Id: %s, Process Id: %s)", 
        								statusAndATExecCount.getLeft(), statusAndATExecCount.getRight().intValue(),
        								problemId, processId));
        }
        
        if (StringUtils.isBlank(statusAndATExecCount.getLeft()) || 
        	statusAndATExecCount.getLeft().equals("UNKNOWN")) return result;
        
        long wsStartTime = System.currentTimeMillis();
        Worksheet worksheet = null;
        
        do {
        	worksheet = ServiceWorksheet.findWorksheetById(problemId);
        	
        	if (worksheet != null && StringUtils.isNotBlank(worksheet.getCondition())) {
        		if (Log.log.isDebugEnabled()) {
        			Log.log.debug(String.format("Found worksheet for problem Id %s with non null/empty Condition %s", 
        						  				problemId, worksheet.getCondition()));
        		}
        		break;
        	}
        	
        	if ((System.currentTimeMillis() - wsStartTime + 1000) < timeOut) {
        		try{
        			if (Log.log.isDebugEnabled()) {
        				Log.log.debug(String.format("Sleeping for 1 sec before checking Worksheet for problem Id %s " +
        											"with non null/empty Condition...", problemId));
        			}
                    Thread.sleep(1000);
                }catch (Exception ee) {
                	// Ignore
                }
        	} else {
        		if (Log.log.isDebugEnabled()) {
        			Log.log.debug(String.format("Failed to get Worksheet for problem Id %s with non null/empty Condition " +
        										"even after waiting for %d msec", problemId, (timeOut - 1000)));
        		}
        		break;
        	}
        } while (Boolean.TRUE.booleanValue());
        
        if (worksheet == null || StringUtils.isBlank(worksheet.getCondition())) {
        	Log.log.warn(String.format("Failed to get Worksheet for Problem Id %s with non null and non empty Condition" + 
        							   " returning without any Action Task Results!!!", 
        							   problemId));        	
        	return result;
        }

        String condition = worksheet.getCondition();
        String severity = worksheet.getSeverity();
        String number = worksheet.getNumber();

        result.put(Constants.EXECUTE_PROBLEMID, problemId);
        result.put(Constants.EXECUTE_PROCESSID, processId);
        result.put(Constants.WORKSHEET_NUMBER, number);
        result.put(Constants.STATUS, statusAndATExecCount.getLeft());
        
        if (statusAndATExecCount.getRight().intValue() > 0) {
        	result.put(Constants.PROCESS_AT_EXEC_COUNT, statusAndATExecCount.getRight());
        }

//        if (StringUtils.isNotEmpty(condition)) {
        result.put(Constants.EXECUTE_CONDITION, condition);
        result.put(Constants.EXECUTE_SEVERITY, severity);

        List<TaskResult> taskResults = null;
        long startTime = System.currentTimeMillis();
        
        do {
            if (StringUtils.isNotBlank(processId)) {
            	ResponseDTO<TaskResult> taskResultResponse = null;
            	
				try {
					taskResultResponse = WorksheetSearchAPI.findTaskResultByProcessId(processId, "admin");
				} catch (SearchException e) {
					Log.log.error(String.format("Error %sin finding task results for process id %s", 
												(StringUtils.isNotBlank(e.getMessage()) ? e.getMessage() : ""), 
												processId), e);
				}
            	
            	if (taskResultResponse != null && taskResultResponse.isSuccess() && 
            		CollectionUtils.isNotEmpty(taskResultResponse.getRecords())) {
            		taskResults = taskResultResponse.getRecords();
            	}
            } else if (StringUtils.isNotBlank(problemId)){
            	taskResults = WorksheetUtil.getActionResultsByWorksheetId(problemId, "admin", null, orgId, orgName);
            }
            
            if (statusAndATExecCount.getRight().intValue() == 0 ||
                StringUtils.isBlank(processId)) {
            	break;
            }
            
            if ((statusAndATExecCount.getRight().intValue() > 0) &&
                CollectionUtils.isNotEmpty(taskResults) && 
                (taskResults.size() >= statusAndATExecCount.getRight().intValue())) {
            	if (Log.log.isDebugEnabled()) {
    				Log.log.debug(String.format("Found %d AT Results for Process Id %s, minimum expected count was %d",
    											taskResults.size(), processId, 
    											statusAndATExecCount.getRight().intValue()));
    			}
            	break;
            } else {
            	if ((System.currentTimeMillis() - startTime + 1000) < timeOut) {
            		try{
            			if (Log.log.isDebugEnabled()) {
            				Log.log.debug(String.format("Sleeping for 1 sec before checking AT Results again for " +
            											"Process Id %s...", processId));
            			}
                        Thread.sleep(1000);
                    }catch (Exception ee) {
                    	// Ignore
                    }
            	} else {
            		Log.log.warn(String.format("Failed to get all AT Results for Process Id %s even after " +
            								   "waiting for %d msec, returning incomplete Action Task Results!!!", 
            								   processId, (timeOut - 1000)));
            		break;
            	}
            }	            
        } while (Boolean.TRUE.booleanValue());
        

        List<Map<String, String>> taskStates = new ArrayList<Map<String, String>>();

        for (Iterator<TaskResult> it = taskResults.iterator(); it.hasNext();)
        {
            TaskResult task = it.next();

            String name = task.getTaskFullName();
            Log.log.debug("task name = " + name);
            Map<String, String> state = new HashMap<String, String>();
            state.put(Constants.EXECUTE_TASKNAME, name);
            state.put(Constants.EXECUTE_ACTIONID, task.getId());
            state.put(Constants.EXECUTE_COMPLETION, task.getCompletion());
            state.put(Constants.EXECUTE_CONDITION, task.getCondition());
            state.put(Constants.EXECUTE_SEVERITY, task.getSeverity());
            state.put(Constants.EXECUTE_SUMMARY, task.getSummary());
            state.put(Constants.EXECUTE_DETAIL, task.getDetail());

            taskStates.add(state);
        }
        
        if (Log.log.isDebugEnabled()) {
            Log.log.debug(String.format("generateResult(problemId:%s,processId%s) returning %d results, expected " +
            							"results count %s in %d msec", problemId, processId, taskStates.size(),
            							(statusAndATExecCount.getRight().intValue() > 0 ? 
            							 statusAndATExecCount.getRight().toString() : "unknown"), 
            							(System.currentTimeMillis() - mStartTime)));
        }

        result.put(Constants.TASK_STATUS, taskStates);
//        }

        return result;
    } // generateResult()

    private static Pair<String, Integer> waitForProcessToComplete(String problemId, String processId, long timeout, 
    															  String orgId, String orgName) {
        long startTime = System.currentTimeMillis();
        long currentTime = startTime;
        String status = Constants.EXECUTE_STATUS_UNKNOWN;
        Integer atExecCount = VO.NON_NEGATIVE_INTEGER_DEFAULT;
        Pair<String, Integer> statsAndATExecCount = Pair.of(status, atExecCount);

        if (StringUtils.isEmpty(problemId)) return statsAndATExecCount;

        if (StringUtils.isEmpty(processId)) return statsAndATExecCount;

        while (currentTime - startTime < timeout * 1000) {
            try {
                Thread.sleep(1000);
            } catch (Exception ee) {
            	// Ignore
            }

            statsAndATExecCount = ServiceWorksheet.getProcessStatusAndATExecCountForRunbook(processId, true);
            if (Log.log.isDebugEnabled()) {
                Log.log.debug(String.format("state = %s, AT Exec Count = %d", statsAndATExecCount.getLeft(), 
                							statsAndATExecCount.getRight().intValue()));
            }
            if (statsAndATExecCount.getLeft() != null && 
            	statsAndATExecCount.getLeft().equals(Constants.EXECUTE_STATUS_COMPLETED)) {
                return statsAndATExecCount;
            } else if (statsAndATExecCount.getLeft() != null && 
            		   statsAndATExecCount.getLeft().equals(Constants.EXECUTE_STATUS_WAITING)) {
            	// If Runbook is waiting for an event return status as wiating with AT results executed so far
                return statsAndATExecCount;
            } else if (statsAndATExecCount.getLeft() != null && 
            		   statsAndATExecCount.getLeft().equals(Constants.EXECUTE_STATUS_OPEN)) {
                currentTime = System.currentTimeMillis();
                continue;
            } else if (statsAndATExecCount.getLeft() != null && 
            		   statsAndATExecCount.getLeft().equals(Constants.EXECUTE_STATUS_ABORTED)) {
            	List<TaskResult> taskResults = null;
            	
            	if (StringUtils.isNotBlank(processId)) {
	            	ResponseDTO<TaskResult> taskResultResponse = null;
	            	
					try {
						taskResultResponse = WorksheetSearchAPI.findTaskResultByProcessId(processId, "admin");
					} catch (SearchException e) {
						Log.log.error(String.format("Error %sin finding task results for process id %s", 
													(StringUtils.isNotBlank(e.getMessage()) ? e.getMessage() : ""), 
													processId), e);
					}
	            	
	            	if (taskResultResponse != null && taskResultResponse.isSuccess() && 
	            		CollectionUtils.isNotEmpty(taskResultResponse.getRecords())) {
	            		taskResults = taskResultResponse.getRecords();
	            	}
	            } else if (StringUtils.isNotBlank(problemId)){
	            	taskResults = WorksheetUtil.getActionResultsByWorksheetId(problemId, "admin", null, orgId, orgName);
	            }

                if (CollectionUtils.isNotEmpty(taskResults)) {
                    int starts = 0;

                    for (Iterator<TaskResult> it = taskResults.iterator(); it.hasNext();) {
                        TaskResult task = it.next();
                        String taskName = task.getTaskFullName();
                        if (taskName.indexOf("start#resolve") != -1)
                            starts++;
                        else if (taskName.indexOf("abort#resolve") != -1)
                            starts--;
                        else if (taskName.indexOf("end#resolve") != -1) starts--;
                    }

                    if (starts != 0) {
                        currentTime = System.currentTimeMillis();
                        continue;
                    } else {
                        return statsAndATExecCount;
                    }
                } else { // no task results are available
                    currentTime = System.currentTimeMillis();
                    continue;
                }
            } else {
                return statsAndATExecCount;
            }
        } // while()

        return statsAndATExecCount;
    }

    // Used by synchronous call to execute the runbook and get the result back
    public static Map<String, Object> executeRunbookWithResult(Map<String, Object> params)
    {

        Map<String, Object> result = executeRunbook(params);

        return getRunbookResult(result);
    } // executeRunbookWithResult()

    @SuppressWarnings("unchecked")
    public static String executeProcess(Map params)
    {
        ExecutionTimeUtil.log((String) params.get("RESOLVE.JOB_ID"), null, null, ExecutionStep.METHOD_EXECUTION, ExecutionStepPhase.START);

        String result = null;
        
        // return if shutting down
        if (MainBase.main.isShutdown == true)
        {
            return "";
        }

        // parameters
        String problemid = (String) params.get(Constants.EXECUTE_PROBLEMID); // required
        boolean isProblemIdNew = (StringUtils.isBlank(problemid) || problemid.equalsIgnoreCase(Constants.PROBLEMID_NEW));
        String userid = (String) params.get(Constants.EXECUTE_USERID); // required
        String wiki = (String) params.get(Constants.EXECUTE_WIKI); // required
        String processid = (String) params.get(Constants.EXECUTE_PROCESSID); // optional
        String reference = (String) params.get(Constants.EXECUTE_REFERENCE); // optional
        String alertid = (String) params.get(Constants.EXECUTE_ALERTID); // optional
        String correlationid = (String) params.get(Constants.EXECUTE_CORRELATIONID); // optional
        String guid = (String) params.get(Constants.EXECUTE_GUID); // optional
        String event_eventid = (String) params.get(Constants.EVENT_EVENTID); // optional
        String event_reference = (String) params.get(Constants.EVENT_REFERENCE); // optional
        boolean event_end = true;
        if (params.containsKey(Constants.EVENT_END))
        {
            event_end = StringUtils.getBoolean(Constants.EVENT_END, params);
        }
        ExecutionTimeUtil.log((String) params.get("RESOLVE.JOB_ID"), processid, null, ExecutionStep.RUNBOOK_INIT, ExecutionStepPhase.START);
        
        String validationMessage = validateOrg(params);
        if(StringUtils.isNotBlank(validationMessage)) {
            MAmqpServer mServer = (MAmqpServer) Main.main.mServer;
            MAmqpRunbookExecutionListener.acknowledgeRunbookExecution(params, mServer);
            return validationMessage;
        }
        
        try {
        	ExecutionTimeUtil.log((String) params.get("RESOLVE.JOB_ID"), processid, null, ExecutionStep.LICENSE_CHECK, ExecutionStepPhase.START);
        	expiredV2LicenseAndEnvCheck(String.format(RUNBOOK_WITH_DETAILS, wiki, problemid, userid));
        	ExecutionTimeUtil.log((String) params.get("RESOLVE.JOB_ID"), processid, null, ExecutionStep.LICENSE_CHECK, ExecutionStepPhase.END);
        } catch (Exception e) {
            MAmqpServer mServer = (MAmqpServer) Main.main.mServer;
            MAmqpRunbookExecutionListener.acknowledgeRunbookExecution(params, mServer);
        	return ERR_MSG_PREFIX + e.getMessage();
        }
        
        // Remove token and rsview id guid from params since params gets bound to groovy scripts
        String token = (String) params.remove(Constants.HTTP_REQUEST_TOKEN);
        String rsviewIdGuid = (String) params.remove(Constants.HTTP_REQUEST_TOKEN);;
        
        reference = (reference == null) ? "" : reference;
        
        boolean isEvent = false;
        
        // init isEvent
        String processNotEvent = (String) params.get(Constants.EXECUTE_PROCESS_NOTEVENT); // optional

        boolean notEvent = "true".equalsIgnoreCase(processNotEvent);
        if (notEvent == false && StringUtils.isNotEmpty(event_eventid) && (StringUtils.isNotEmpty(event_reference) || StringUtils.isNotEmpty(processid)))
        {
            isEvent = true;
        }
        
        /*
         * If event and worksheet execute state contains 
         * component Id which does not match with current component, 
         * then route process execution to component (RSControl) specified in 
         * saved execute state for merge to work.
         */

        
        if (isEvent)
        {
        	if (Log.log.isDebugEnabled()) {
        		Log.log.debug(String.format("%s: %s, %s: %b%s%s", Constants.EVENT_EVENTID, event_eventid, Constants.EVENT_END, 
            								event_end,
            								(StringUtils.isNotBlank(event_reference) ? 
            								 ", " + Constants.EVENT_REFERENCE + ": " + event_reference : ""),
            								(StringUtils.isNotBlank(processid) ? 
            								 ", " + Constants.EXECUTE_PROCESSID + ": " + processid : "")));
        	}
            
            try
            {
                // init problemid
                problemid = WorksheetUtils.initWorksheet(problemid, reference, alertid, correlationid, userid, processid, isEvent, params);
                String eventResult = processEvent(params, problemid, processid, event_eventid, event_reference, event_end);
                if("".equals(eventResult)) {
                	return eventResult;
                } else if(eventResult != null) {
                	processid = eventResult;
                }
            }
            catch (Exception e)
            {
            	Log.log.error(e.getMessage(), e);
            }
        }
                
        Log.reset();

        // Execute counter will increase only if rscontrol is running.
        /*
         * HP TO DO This causes RSControls wiki process dependency cache to get busted affecting execution of same runbook 
         * as WIKI_DOCUMENT_CACHE_REGION is refreshed due to change in WikiDocument.wikiDocStatistics by Hibernate.
         * 
		 * Issue is actually any change in runbook's main model or any sub-runbook's main model should trigger
		 * refresh on RSControls wiki process dependency cache. 
         */
        if(((Main)Main.main).configGeneral.getEnableRunbookStatCounters()) {
            updateExecuteCountForWikiDocument(wiki);
        }
        
        try
        {
        	ExecutionTimeUtil.log((String) params.get("RESOLVE.JOB_ID"), null, null, ExecutionStep.PARAMETER_DECRYPTION, ExecutionStepPhase.START);
            EncryptionTransportObject.restoreMap(params, null);
        	ExecutionTimeUtil.log((String) params.get("RESOLVE.JOB_ID"), null, null, ExecutionStep.PARAMETER_DECRYPTION, ExecutionStepPhase.END);

        	Log.log.info(String.format("executeProcess params: %s", StringUtils.mapToString(params, "=", ", ")));

            // cleanup
            wiki = wiki != null ? wiki.trim() : null;
            problemid = problemid != null ? problemid.trim() : null;
            userid = userid != null ? userid.trim() : null;

            // init EXECUTE_PROCESS_STARTTIME
            params.put(Constants.EXECUTE_PROCESS_STARTTIME, "" + System.currentTimeMillis());

            // parameter validation
            if (StringUtils.isEmpty(wiki))
            {
                throw new Exception("Missing runbook WIKI to execute");
            }
            if (StringUtils.isEmpty(userid))
            {
                throw new Exception(String.format("Missing USERID to execute runbook: %s", wiki));
            }
            
            if (StringUtils.isBlank(problemid) || problemid.equalsIgnoreCase(Constants.PROBLEMID_NEW) || 
                problemid.equalsIgnoreCase(Constants.PROBLEMID_ACTIVE) || problemid.equalsIgnoreCase(Constants.PROBLEMID_LOOKUP))
            {
                // init problemid
            	Worksheet ws = ServiceWorksheet.initWorksheet2(problemid, reference, alertid, correlationid, userid, processid, 
            												   isEvent, params);

            	if (ws != null) {
            		problemid = ws.getSysId();
            		params.put(Constants.EXECUTE_PROBLEMID, problemid);
            		params.put(Constants.WORKSHEET_NUMBER, ws.getNumber());
            	}
            }
            
            if (!StringUtils.isEmpty(problemid))
            {
                ArrayList start = new ArrayList();
                ActionProcessTaskRequest eventTask = new ActionProcessTaskRequest();

                // init execute process request
                if (!isEvent)
                {
                    processid = ProcessRequest.initProcessRequest(wiki, userid, problemid, processid, isEvent, params, start, eventTask);
                    result = executeProcessInternal(params, problemid, processid, isEvent, start, eventTask, wiki, userid, 
                    								reference, event_reference, event_eventid, token, rsviewIdGuid,
                    								isProblemIdNew);
                }
                else
                {
                	executeEventProcess(params, problemid, isProblemIdNew, userid, wiki, processid, reference,
							event_eventid, event_reference, token, rsviewIdGuid, isEvent, start, eventTask);
                }
            }
            else
            {
                Log.log.warn("FAIL: Unable to create worksheet. Runbook not executed");
                MAmqpServer mServer = (MAmqpServer) Main.main.mServer;
                MAmqpRunbookExecutionListener.acknowledgeRunbookExecution(params, mServer);
            }
        }
        catch (Exception e)
        {
          MAmqpServer mServer = (MAmqpServer) Main.main.mServer;
          MAmqpRunbookExecutionListener.acknowledgeRunbookExecution(params, mServer);
            Log.log.warn(e.getMessage(), e);
        }
        if (Log.log.isDebugEnabled()) {
            Log.log.debug(String.format("Execute Process Result: %s", result));
        }
        ExecutionTimeUtil.log((String) params.get("RESOLVE.JOB_ID"), null, null, ExecutionStep.METHOD_EXECUTION, ExecutionStepPhase.END);
        return result;

    } // executeProcess

	private static String processEvent(Map params, String problemid, String processid, String event_eventid,
			String event_reference, boolean event_end) {
		
		String result = null;
		if (StringUtils.isEmpty(problemid))
		{
		    throw new RuntimeException("Unable to create worksheet. Runbook not executed");
		}
		
		if (Log.log.isDebugEnabled()) {
			Log.log.debug(String.format("problemId: %s", problemid));
		}
		
		if (StringUtils.isNotBlank(event_reference))
		{
			if("LOOKUP".equals(event_reference)) {
				event_reference = problemid;
			}
		    List<ExecuteState> rows = ServiceWorksheet.getEventRowsByReference(event_end, event_reference, problemid, 
		    																   event_eventid);
		    if (rows != null && !rows.isEmpty())
		    {
		        long oldest = Long.MAX_VALUE;
		        long startTime = -1;
		        for (ExecuteState row : rows)
		        {
		            long up = row.getSysUpdatedOn();
		            processid = row.getProcessId();
		            result = processid;
		            
		            if (up < oldest)
		            {
		                oldest = up;

		                Long processStartTime = row.getProcessStartTime();
		                if (processStartTime != null)
		                {
		                    startTime = processStartTime;
		                }
		            }
		        }

		        if (startTime > 0)
		        {
		            params.put(Constants.EXECUTE_PROCESS_STARTTIME, "" + startTime);
		        }
		        else
		        {
		            throw new RuntimeException(String.format("Could not find latest process waiting for event " +
		            										 "with reference: %s, worksheet: %s", event_reference,
		            										 problemid));
		        }
		    }
		    else
		    {
		    	String errMsg = String.format("No process with problem id %s found waiting for event with " +
		    								  "reference %s. Event is being ignored!!!", problemid, 
		    								  event_reference);
		    	Log.log.warn(errMsg);
		    	return "";
		    }
		}
		else
		{
			if (Log.log.isDebugEnabled()) {
				Log.log.debug(String.format("Using provided PROCESSID for event: %s", processid));
			}
		}
		
		String componentId = null;
		
		if (Log.log.isDebugEnabled()) {
			Log.log.debug(String.format("Finding execute states for processId: %s from worksheet...", processid));
		}
		
		if (StringUtils.isNotBlank(processid))
		{
		    List<ExecuteState> esList = WorksheetUtil.getEventRowsByProcessId(processid);
		    
		    for (ExecuteState es : esList)
		    {
		    	if (Log.log.isDebugEnabled()) {
		    		Log.log.debug(String.format("Execute State: [%s]", es));
		    	}
		        
		        if (StringUtils.isNotBlank(es.getComponentId()))
		        {
		            componentId = es.getComponentId();
		            break;
		        }
		    }
		}
		
		if (StringUtils.isNotBlank(componentId) && 
		    !MainBase.main.configId.getGuid().equals(componentId))
		{
			if (Log.log.isDebugEnabled()) {
				Log.log.debug(String.format("Execute State componentId %s does not match current componentId %s" +
		                  	  				" forwarding %s to %s for MAction.executeProcess",
		                  	  				componentId, MainBase.main.configId.getGuid(), 
		                  	  				StringUtils.mapToString(params, "=", ", "), componentId));
			}
			
		    Main.esb.sendMessage(componentId, "MAction.executeProcess",params);
		    result = "";
		}
		
		return result;
	}

	private static void executeEventProcess(Map params, String problemid, boolean isProblemIdNew, String userid,
			String wiki, String processid, String reference, String event_eventid, String event_reference, String token,
			String rsviewIdGuid, boolean isEvent, ArrayList start, ActionProcessTaskRequest eventTask) {
	    final Map<String, Object> initProcessRequestMap = new HashMap<String, Object>();
	    initProcessRequestMap.put("wiki", wiki);
	    initProcessRequestMap.put("userid", userid);
	    initProcessRequestMap.put("problemid", problemid);
	    initProcessRequestMap.put("processid", processid);
	    initProcessRequestMap.put("isEvent", isEvent);
	    initProcessRequestMap.put("params", params);
	    initProcessRequestMap.put("start", start);
	    initProcessRequestMap.put("eventTask", eventTask);

	    final Map<String, Object> exeProcMap = new HashMap<String, Object>();
	    exeProcMap.put("params", params);
	    exeProcMap.put("problemid", problemid);
	    exeProcMap.put("processid", processid);
	    exeProcMap.put("isEvent", isEvent);
	    exeProcMap.put("start", start);
	    exeProcMap.put("eventTask", eventTask);
	    exeProcMap.put("wiki", wiki);
	    exeProcMap.put("userid", userid);
	    exeProcMap.put("reference", reference);
	    exeProcMap.put("event_reference", event_reference);
	    exeProcMap.put("event_eventid", event_eventid);
	    exeProcMap.put("token", token);
	    exeProcMap.put("rsview_id_guid", rsviewIdGuid);

        processid = ProcessRequest.initProcessRequest((String) initProcessRequestMap.get("wiki"), (String) initProcessRequestMap.get("userid"), (String) initProcessRequestMap.get("problemid"), (String) initProcessRequestMap.get("processid"), (Boolean) initProcessRequestMap.get("isEvent"), (Map) initProcessRequestMap.get("params"), (ArrayList) initProcessRequestMap.get("start"), (ActionProcessTaskRequest) initProcessRequestMap.get("eventTask"));

        exeProcMap.put("processid", processid);

        executeProcessInternal((Map) exeProcMap.get("params"), (String) exeProcMap.get("problemid"), 
        					   (String) exeProcMap.get("processid"), 
        					   ((Boolean) exeProcMap.get("isEvent")).booleanValue(), 
        					   (ArrayList) exeProcMap.get("start"), 
        					   (ActionProcessTaskRequest) exeProcMap.get("eventTask"), 
        					   (String) exeProcMap.get("wiki"), (String) exeProcMap.get("userid"), 
        					   (String) exeProcMap.get("reference"), 
        					   (String) exeProcMap.get("event_reference"),
        					   (String) exeProcMap.get("event_eventid"), 
        					   (String) exeProcMap.get("token"), 
        					   (String) exeProcMap.get("rsview_id_guid"),
        					   isProblemIdNew);
	}

	private static String validateOrg(Map params) {
		String orgName = (String) params.get(Constants.EXECUTE_ORG_NAME); // optional
        String orgId = (String) params.get(Constants.EXECUTE_ORG_ID); // optional
        
        if (OrgsVO.NONE_ORG_NAME.equalsIgnoreCase(orgName)
        		|| OrgsVO.STRING_DEFAULT.equalsIgnoreCase(orgId))
        {
            params.remove(Constants.EXECUTE_ORG_NAME);
            params.remove(Constants.EXECUTE_ORG_ID);
                        
        } else if (StringUtils.isNotBlank(orgName) && StringUtils.isBlank(orgId))
        {
            // Identify org id based on org name
            
            OrgsVO orgsVO = UserUtils.getOrg(null, orgName);
            
			if (orgsVO != null) {
				orgId = orgsVO.getSys_id();
				params.put(Constants.EXECUTE_ORG_ID, orgId);
			} else {
				String errMsg = String.format(RECEIVED_INVALID_ORG_INEXECUTE_REQUEST, "Org Name " + orgName,
						AT_SYNCHRONOUSLY, StringUtils.mapToString(params, "=", ", "));
				Log.log.info(errMsg);
				return ERR_MSG_PREFIX + errMsg;
			}

		} else if (StringUtils.isNotBlank(orgId)) {
			if (OrgsUtil.findOrgById(orgId) == null) {
				String errMsg = String.format(RECEIVED_INVALID_ORG_INEXECUTE_REQUEST,
						"Org Id " + (String) params.get(Constants.EXECUTE_ORG_ID), AT_SYNCHRONOUSLY,
						StringUtils.mapToString(params, "=", ", "));

				Log.log.info(errMsg);

				return ERR_MSG_PREFIX + errMsg;
			}
		}
        
        return null;
	}
    
    public static void logExecutionSummary(Map<String, String> rbcParams, String username, boolean isProblemIdNew)
    {
        
        ExecutionTimeUtil.log((String) rbcParams.get("RESOLVE.JOB_ID"), null, null, ExecutionStep.METHOD_EXECUTION, ExecutionStepPhase.START);
        
    	/*
    	 * Create new execution summary record only if no execution record exists for same worksheet.
    	 * Execution record with worksheet id is first checked in ES and then in archived worksheets.
    	 * This prevents creation of multiple execution records for same worksheet which 
    	 * happens when multiple executions use same worksheet.
    	 */
    	
    	boolean exists = false;
    	
    	if (rbcParams.containsKey(Constants.WORKSHEET_PROBLEMID) && 
    		StringUtils.isNotBlank(rbcParams.get(Constants.WORKSHEET_PROBLEMID))) {
    		QueryDTO queryDTO = new QueryDTO();
    		
    		String problemId = rbcParams.get(Constants.WORKSHEET_PROBLEMID);
    		
    		queryDTO.addFilterItem(new QueryFilter("worksheetId", QueryFilter.EQUALS, problemId));
    		
        	if (!isProblemIdNew) {        		
            	
        		long startTime = 0;
            	if (PerformanceDebug.debugRSControl()) {
            		startTime = System.currentTimeMillis();
            	}
                 
	    		try {
					ResponseDTO<ExecutionSummary> existingExecSummaryResp = 
														APIFactory.getExecutionSummarySearchAPI(ExecutionSummary.class)
														.searchByQuery(queryDTO, username);
					
					exists = existingExecSummaryResp != null && existingExecSummaryResp.getRecords() != null &&
							 !existingExecSummaryResp.getRecords().isEmpty();
				} catch (SearchException se) {
					Log.log.warn(String.format("Error %s occurred while searching for existing execution summary record " + 
											   "for worksheet id %s. Creating new execution summary record!!!", 
											   (StringUtils.isNotBlank(se.getMessage()) ? se.getMessage() : "unknown"),
											   rbcParams.get(Constants.WORKSHEET_PROBLEMID)), se);
				}
	    		
	    		if (PerformanceDebug.debugRSControl()) {
	        		Log.log.info(String.format("Check if execution summary exists in ES took total %d msec", 
	        								   (System.currentTimeMillis() - startTime)));
        	}
	    		
	    		if (!exists) {
	    			// Check in Archived Worksheets
	    			final List<Boolean> fBoolList = new ArrayList<Boolean>(1);
	    			fBoolList.add(Boolean.FALSE);
	    			
    	            try {
    	                HibernateUtil.actionNoCacheHibernateInitLocked(hibernateUtil -> {
    	                	ArchiveWorksheet archivedWorksheet = HibernateUtil.getDAOFactory().getArchiveWorksheetDAO()
    	                										 .findById(problemId);
    	                	
    	                	fBoolList.set(0, Boolean.valueOf(archivedWorksheet != null ? true : false));
    	                }, username);
    	            }
    	            catch (Throwable e) {
    	                Log.log.error(e.getMessage(), e);
    	            }
    	
    	            exists = fBoolList.get(0).booleanValue();
	    		}
        	}
    	}
    	
    	if (!exists) {
    		ServiceWorksheet.createNewExecutionSummary(rbcParams, username);
    	}
    	
    	ExecutionTimeUtil.log((String) rbcParams.get("RESOLVE.JOB_ID"), null, null, ExecutionStep.METHOD_EXECUTION, ExecutionStepPhase.END);
    }

    public static void updateExecuteCountForWikiDocument(String docFullName)
    {
        if (!StringUtils.isEmpty(docFullName))
        {
            try
            {
            	String tDocFullName = docFullName;
            	
            	if (StringUtils.isNotBlank(docFullName) && docFullName.startsWith(Constants.EXECUTE_ABORTWIKI)) {
            		tDocFullName = docFullName.substring(Constants.EXECUTE_ABORTWIKI.length());
            	}
            	
                WikiDocumentVO wikiDoc = ProcessRequest.findWikiDocumentByName(tDocFullName);

                if (wikiDoc != null)
                {
                    // increment the EXECUTE counter for this document
                    Map<String, Object> params = new HashMap<String, Object>();
                    params.put(ConstantValues.WIKI_DOCUMENT_SYS_ID, wikiDoc.getSys_id());
                    params.put(HibernateConstants.WIKIDOC_STATS_COUNTER_NAME_KEY, WikiDocStatsCounterName.EXECUTED);
                    WikiUtils.sendESBForUpdatingWikiRelations(params);
                }
                else
                {
                    Log.log.error(String.format("updateExecuteCountForWikiDocument : Can not find specified wiki " +
                    							"document [%s]", docFullName));
                }
            }
            catch (Exception e)
            {
                Log.log.error(e.getMessage(), e);
            }
        }
        else
        {
            Log.log.error("updateExecuteCountForWikiDocument : Specified empty wiki document");
        }
    }

    public static String executeProcessInternal(Map params, String problemid, String processid, boolean isEvent, ArrayList start, 
    											ActionProcessTaskRequest eventTask, String wiki, String userid, String reference, 
    											String event_reference, String event_eventid, String token, String rsviewIdGuid,
    											boolean isProblemIdNew)
    {
    	// gather necessary information to get RBC, that includes wiki params, Source (event originator), Queue, and Org Id
    	Map<String, String> rbcParams = (Map<String, String>)params.get("WIKI_PARAMS");
    	
        if (rbcParams == null)
        {
            rbcParams = new HashMap<String, String>();
        }
        
        if (params.containsKey(Constants.HTTP_REQUEST_SSO_TYPE) && StringUtils.isNotBlank((String)params.get(Constants.HTTP_REQUEST_SSO_TYPE)))
        {
            rbcParams.put(Constants.HTTP_REQUEST_SSO_TYPE, (String)params.get(Constants.HTTP_REQUEST_SSO_TYPE));
        }
        
        rbcParams.put(Constants.EXECUTE_REFERENCE, (String)params.get(Constants.EXECUTE_REFERENCE));
        
        String rbc = MainBase.main.getLicenseService().getRBCR63(rbcParams);
        
        rbcParams.put(Constants.WIKI, (String)params.get(Constants.WIKI));
        rbcParams.put(LicenseEnum.RBC.getKeygenName(), rbc);
        rbcParams.put(Constants.WORKSHEET_PROBLEMID, problemid);
        rbcParams.put(Constants.EXECUTE_ORG_ID, (String) params.get(Constants.EXECUTE_ORG_ID));
        
        logExecutionSummary(rbcParams, userid, isProblemIdNew);
        
        params.remove("WIKI_PARAMS");
        
        String result = null;

        // start parameters
        String processTimeoutStr = "10";
        if(params.get(Constants.EXECUTE_PROCESS_TIMEOUT) instanceof String) {
            processTimeoutStr = (String) params.get(Constants.EXECUTE_PROCESS_TIMEOUT); // optional
        }else if (params.get(Constants.EXECUTE_PROCESS_TIMEOUT) instanceof Integer) {
            processTimeoutStr = "" + params.get(Constants.EXECUTE_PROCESS_TIMEOUT);
        }
        String processLoop = (String) params.get(Constants.EXECUTE_PROCESS_LOOP); // optional
        String processDebug = (String) params.get(Constants.EXECUTE_PROCESS_DEBUG); // optional
        String processMock = (String) params.get(Constants.EXECUTE_PROCESS_MOCK); // optional
        String processCheckPreConditionNullVars = (String) params.get(Constants.EXECUTE_PROCESS_CHECK_PRECONDITION_NULL_VARS); // optional

        if (processid != null)
        {
            try
            {
                ProcessExecution.updateLastActivity(processid);
                Log.setProcessId(processid);
                params.put(Constants.EXECUTE_PROCESSID, processid);
                
                long processTimeout = handleProcessTimeout(wiki, processTimeoutStr);
                boolean hasLoop = "true".equalsIgnoreCase(processLoop);
                boolean isDebug = "true".equalsIgnoreCase(processDebug);
                boolean checkPreConditionNullVars = "true".equalsIgnoreCase(processCheckPreConditionNullVars);
                ScriptDebug debug = new ScriptDebug(isDebug, problemid);
                long processStartTime = Long.parseLong((String) params.get(Constants.EXECUTE_PROCESS_STARTTIME));

                try
                {
                    params.put(Constants.EXECUTE_GUID, Main.main.configId.guid);

                    if (Log.log.isDebugEnabled()) {
                        Log.log.debug("StartProcess - main initialization");
                    }
                    Log.log.info(String.format("start - total active runbooks: %d", RSControlMListenerUtil.getActiveRunbook()));
                    ProcessExecution.startProcess(problemid, processid, wiki, reference, event_eventid, event_reference, userid, processTimeout, hasLoop, debug, processMock, isEvent, processStartTime, params, ActionTaskProperties.getInstance(), start, eventTask, token, rsviewIdGuid, checkPreConditionNullVars);
                    result = processid;
                }
                catch (Throwable e)
                {
                    debug.println(e.getMessage(), e);
                }
                finally
                {
                    WorksheetUtils.writeDebugCAS(debug);
                }
            }
            finally
            {
                Log.clearProcessId();
            }
        }
        else
        {
            Log.log.warn(String.format("Unable to create process_id. Runbook not executed: %s", wiki));
        }

        ExecutionTimeUtil.log((String) rbcParams.get("RESOLVE.JOB_ID"), null, null, ExecutionStep.METHOD_EXECUTION, ExecutionStepPhase.END);
        return result;
    }

	private static long handleProcessTimeout(String wiki, String processTimeoutStr) {
		// init process timeout
		long processTimeout = 600;// secs - 10 mins
		if (StringUtils.isEmpty(processTimeoutStr))
		{
		    if (ProcessRequest.cacheProcessTimeout.containsKey(wiki))
		    {
		        processTimeout = ProcessRequest.cacheProcessTimeout.get(wiki);
		    }
		    else
		    {
		        Log.log.warn(String.format("Missing PROCESS_TIMEOUT - wiki: %s", wiki));
		    }
		}
		else
		{
		    processTimeout = Long.parseLong(processTimeoutStr) * 60;

		    // update wiki process timeout cache
		    ProcessRequest.cacheProcessTimeout.put(wiki, processTimeout);
		}

		if (processTimeout > 0)
		{
		    long newExpiration = (processTimeout * 1000L) + System.currentTimeMillis();
		    if (newExpiration > RSControlMListenerUtil.maxExpirationTime.get())
		    {
		        RSControlMListenerUtil.maxExpirationTime.set(newExpiration);
		    }
		}
		return processTimeout;
	}

    public static void executeProcessReplicated(Map params)
    {
        throw new RuntimeException("****** executeProcessReplicated: RSControl is not replicating any more !!! ******");
    } // executeProcessReplicated

    /*
     * Execute task from RSView
     */
    @SuppressWarnings("unchecked")
    public static String executeTask(Map params)
    {
        String result = "";

        // String userid = (String) params.get(Constants.EXECUTE_USERID); // required
        String userid = (String) (StringUtils.isNotEmpty((String) params.get(Constants.RESOLVE_INSTERNAL_EXECUTE_USERID)) ? params.get(Constants.RESOLVE_INSTERNAL_EXECUTE_USERID) : params.get(Constants.EXECUTE_USERID));
        String problemid = (String) params.get(Constants.EXECUTE_PROBLEMID); // required
        String actionname = (String) params.get(Constants.EXECUTE_ACTIONNAME); // required ACTIONANME or ACTIONID
        String actionnamespace = (String) params.get(Constants.EXECUTE_ACTIONNAMESPACE);
        String namespace = (String) params.get(Constants.EXECUTE_NAMESPACE); // optional
        String actionid = (String) params.get(Constants.EXECUTE_ACTIONID); // optional
        String executeid = (String) params.get(Constants.EXECUTE_EXECUTEID); // optional
        String processid = (String) params.get(Constants.EXECUTE_PROCESSID); // optional
        String wiki = (String) params.get(Constants.EXECUTE_WIKI); // optional
        String reference = (String) params.get(Constants.EXECUTE_REFERENCE); // optional
        String alertid = (String) params.get(Constants.EXECUTE_ALERTID); // optional
        String correlationid = (String) params.get(Constants.EXECUTE_CORRELATIONID); // optional
        String input = (String) params.get(Constants.EXECUTE_INPUT); // optional
        String delay = (String) params.get(Constants.EXECUTE_DELAY); // optional
        String processDebug = (String) params.get(Constants.EXECUTE_PROCESS_DEBUG); // optional
        String processMock = (String) params.get(Constants.EXECUTE_PROCESS_MOCK); // optional

        String token = null;

        // Remove token and rsview id guid from params since params gets bound to groovy scripts

        if (params.containsKey(Constants.HTTP_REQUEST_TOKEN))
        {
            token = (String) params.get(Constants.HTTP_REQUEST_TOKEN);
            params.remove(Constants.HTTP_REQUEST_TOKEN);
        }

        String rsviewIdGuid = null;

        if (params.containsKey(Constants.HTTP_ATTRIBUTE_RSVIEW_ID_GUID))
        {
            rsviewIdGuid = (String) params.get(Constants.HTTP_ATTRIBUTE_RSVIEW_ID_GUID);
            params.remove(Constants.HTTP_REQUEST_TOKEN);
        }
        
        String orgName = (String) params.get(Constants.EXECUTE_ORG_NAME); // optional
        
        if (StringUtils.isNotBlank(orgName))
        {
            // Identify org id based on org name
            
            OrgsVO orgsVO = UserUtils.getOrg(null, orgName);
            
            if (orgsVO != null)
            {
                params.put(Constants.EXECUTE_ORG_ID, orgsVO.getSys_id());
            }
        }
        
        try {
        	expiredV2LicenseAndEnvCheck(String.format(AT_ASYNCHRONOUSLY, 
        											  (StringUtils.isNotBlank(actionname) ? 
        											   "named " + actionname : "with id " + actionid), problemid, userid));
        } catch (Exception e) {
        	return ERR_MSG_PREFIX + e.getMessage();
        }
        
        int processTimeout = 0;

        // init script debug
        boolean isDebug = !StringUtils.isEmpty(processDebug) && processDebug.equalsIgnoreCase("true");

        try
        {
            // init thread processid
            Log.setProcessId(processid);

            // init problemid to active worksheet
            problemid = WorksheetUtils.initWorksheet(problemid, reference, alertid, correlationid, userid, null, false, params);

            // init script debug
            ScriptDebug debug = new ScriptDebug(isDebug, problemid);

            // init actiontask id
            if (StringUtils.isEmpty(actionid))
            {
                actionid = ProcessRequest.getActionTaskId(actionname, namespace);
                if (actionid == null)
                {
                    Log.log.warn("FAILED: Missing actiontask for name: " + actionname + " namespace: " + namespace);
                }
            }
            else if (StringUtils.isEmpty(actionname))
            {
                actionname = ProcessRequest.getActionTaskName(actionid);
                if (actionname == null)
                {
                    Log.log.warn("ERROR: Unable to find Action Name from action id " + actionid);
                }
            }

            if (StringUtils.isNotEmpty(actionname) && StringUtils.isNotEmpty(namespace))
            {
                int pos = actionname.indexOf("#");
                if (pos == -1)
                {
                    actionname = actionname + "#" + namespace;
                }
            }

            executeid = ProcessRequest.initTaskRequest(actionid, params, wiki, processid, executeid, problemid, userid, actionname);

            // execute task
            if (!StringUtils.isEmpty(executeid))
            {
                ProcessExecution.executeTask(executeid, processid, wiki, actionid, delay, problemid, reference, userid, input, params, debug, processMock, processTimeout, ActionTaskProperties.getInstance(), token, rsviewIdGuid, false);
                result = executeid;
            }
        }
        catch (Throwable e)
        {
            Log.log.warn(e.getMessage(), e);

            result = e.getMessage();
        }
        finally
        {
            Log.clearProcessId();
        }

        return result;
    } // executeTask

    /*
     * Execute AT synchronously - waiting for AT to complete
     */
    @SuppressWarnings("unchecked")
    public static String executeATSynchronously(Map params)
    {
        String result = "";

        String userid = (String) (StringUtils.isNotEmpty((String)params.get(Constants.RESOLVE_INSTERNAL_EXECUTE_USERID)) ? params.get(Constants.RESOLVE_INSTERNAL_EXECUTE_USERID) : params.get(Constants.EXECUTE_USERID));
        String problemid = (String) params.get(Constants.EXECUTE_PROBLEMID); // required
        String actionname = (String) params.get(Constants.EXECUTE_ACTIONNAME); // required ACTIONANME or ACTIONID
        String actionnamespace = (String) params.get(Constants.EXECUTE_ACTIONNAMESPACE);
        String namespace = (String) params.get(Constants.EXECUTE_NAMESPACE); // optional
        String actionid = (String) params.get(Constants.EXECUTE_ACTIONID); // optional
        String executeid = (String) params.get(Constants.EXECUTE_EXECUTEID); // optional
        String processid = (String) params.get(Constants.EXECUTE_PROCESSID); // optional
        String wiki = (String) params.get(Constants.EXECUTE_WIKI); // optional
        String reference = (String) params.get(Constants.EXECUTE_REFERENCE); // optional
        String alertid = (String) params.get(Constants.EXECUTE_ALERTID); // optional
        String correlationid = (String) params.get(Constants.EXECUTE_CORRELATIONID); // optional
        String input = (String) params.get(Constants.EXECUTE_INPUT); // optional
        String delay = (String) params.get(Constants.EXECUTE_DELAY); // optional
        String processDebug = (String) params.get(Constants.EXECUTE_PROCESS_DEBUG); // optional
        String processMock = (String) params.get(Constants.EXECUTE_PROCESS_MOCK); // optional
        String atSyncTimeoutRaw = (String) params.get(Constants.EXECUTE_AT_SYNCHRONOUSLY_TIMEOUT); // optional, timeout in milliseconds while waiting for AT completion
        long atSyncTimeout = 0; 
        try {
            atSyncTimeout = Long.parseLong(atSyncTimeoutRaw);
            if (atSyncTimeout < 0) 
            {
                Log.log.error("Error : EXECUTE_AT_SYNCHRONOUSLY_TIMEOUT value must be positive.");
                return "Error : EXECUTE_AT_SYNCHRONOUSLY_TIMEOUT value must be positive.";
            }
        }
        catch (NumberFormatException nfe)
        {
            Log.log.error("Error parsing EXECUTE_AT_SYNCHRONOUSLY_TIMEOUT value.", nfe);
            return "Error parsing EXECUTE_AT_SYNCHRONOUSLY_TIMEOUT value. Exception : " + nfe.getMessage();
        }

        String token = null;
        
        // Remove token and rsview id guid from params since params gets bound to groovy scripts
        
        if (params.containsKey(Constants.HTTP_REQUEST_TOKEN))
        {
            token = (String)params.get(Constants.HTTP_REQUEST_TOKEN);
            params.remove(Constants.HTTP_REQUEST_TOKEN);
        }
        
        String rsviewIdGuid = null;
        
        if (params.containsKey(Constants.HTTP_ATTRIBUTE_RSVIEW_ID_GUID))
        {
            rsviewIdGuid = (String)params.get(Constants.HTTP_ATTRIBUTE_RSVIEW_ID_GUID);
            params.remove(Constants.HTTP_REQUEST_TOKEN);
        }
        
        String orgName = (String) params.get(Constants.EXECUTE_ORG_NAME); // optional
        
        if (StringUtils.isNotBlank(orgName) && orgName.toLowerCase().trim().equals("none"))
        {
            orgName = null;
        }
        
        if (StringUtils.isNotBlank(orgName) && StringUtils.isBlank((String)params.get(Constants.EXECUTE_ORG_ID)))
        {
            // Identify org id based on org name
            
            OrgsVO orgsVO = UserUtils.getOrg(null, orgName);
            
            if (orgsVO != null)
            {
                params.put(Constants.EXECUTE_ORG_ID, orgsVO.getSys_id());
            }
        }
        
        if ((StringUtils.isNotBlank(orgName) && StringUtils.isBlank((String)params.get(Constants.EXECUTE_ORG_ID))) ||
             StringUtils.isNotBlank((String)params.get(Constants.EXECUTE_ORG_ID)))
        {
            // Validate Org Id either found from name or specified directly
            
            if (StringUtils.isBlank((String)params.get(Constants.EXECUTE_ORG_ID)) ||
                (OrgsUtil.findOrgById((String)params.get(Constants.EXECUTE_ORG_ID)) == null))
            {
            	String errMsg = String.format(RECEIVED_INVALID_ORG_INEXECUTE_REQUEST, 
            								  (StringUtils.isNotBlank(orgName) ? 
            								   "Org Name " + orgName : 
            								   "Org Id " + (String)params.get(Constants.EXECUTE_ORG_ID)),
            								  AT_SYNCHRONOUSLY,
            								  StringUtils.mapToString(params, "=", ", "));
            	
                Log.log.info(errMsg);
                
                return ERR_MSG_PREFIX + errMsg;
            }
        }
        
        try {
        	expiredV2LicenseAndEnvCheck(AT_SYNCHRONOUSLY);
        } catch (Exception e) {
        	return ERR_MSG_PREFIX + e.getMessage();
        }
        
        int processTimeout = 0;

        // init script debug
        boolean isDebug = !StringUtils.isEmpty(processDebug) && processDebug.equalsIgnoreCase("true");

        try
        {
            // init thread processid
            Log.setProcessId(processid);

            // init problemid to active worksheet
            problemid = WorksheetUtils.initWorksheet(problemid, reference, alertid, correlationid, userid, null, false, params);

            // init script debug
            ScriptDebug debug = new ScriptDebug(isDebug, problemid);

            // init actiontask id
            if (StringUtils.isEmpty(actionid))
            {
                actionid = ProcessRequest.getActionTaskId(actionname, namespace);
                if (actionid == null)
                {
                    Log.log.warn("FAILED: Missing actiontask for name: " + actionname + " namespace: " + namespace);
                }
            }
            else if (StringUtils.isEmpty(actionname))
            {
                actionname = ProcessRequest.getActionTaskName(actionid);
                if (actionname == null)
                {
                    Log.log.warn("ERROR: Unable to find Action Name from action id " + actionid);
                }
            }

            if (StringUtils.isNotEmpty(actionname) && StringUtils.isNotEmpty(namespace))
            {
                int pos = actionname.indexOf("#");
                if (pos == -1)
                {
                    actionname = actionname + "#" + namespace;
                }
            }

            executeid = ProcessRequest.initTaskRequest(actionid, params, wiki, processid, executeid, problemid, userid, actionname);

            // execute task
            if (!StringUtils.isEmpty(executeid))
            {
                ProcessExecution.executeTask(executeid, processid, wiki, actionid, delay, problemid, reference, userid, input, params, debug, processMock, processTimeout, ActionTaskProperties.getInstance(), token , rsviewIdGuid, false);
                long startEpoch = System.currentTimeMillis();
                TaskResult taskResult = WorksheetSearchAPI.findTaskResultByExecuteRequestId(executeid, "system");
                while(taskResult == null && (System.currentTimeMillis() - startEpoch) < atSyncTimeout)
                {
                    taskResult = WorksheetSearchAPI.findTaskResultByExecuteRequestId(executeid, "system");   
                }
                result = executeid;
            }
        }
        catch (Throwable e)
        {
            Log.log.warn(e.getMessage(), e);

            result = e.getMessage();
        }
        finally
        {
            Log.clearProcessId();
        }

        return result;
    }
    
    public static Map<String, Object> createSIRAndUpdateWSReferenceDetails(Map<String, Object> sirParams) throws Exception 
    {
        /*
    	Map<String, Object> response = new HashMap<String, Object>();
    	ResolveSecurityIncident resolveSIR = new ResolveSecurityIncident();
    	resolveSIR.setTitle((String) sirParams.get(Constants.EXECUTE_SIR_TITLE));
    	resolveSIR.setType((String) sirParams.get(Constants.EXECUTE_SIR_TYPE));
    	resolveSIR.setInvestigationType((String) sirParams.get(Constants.EXECUTE_SIR_INVESTIGATION_TYPE));
    	resolveSIR.setPlaybook((String) sirParams.get(Constants.EXECUTE_SIR_PLAYBOOK));
    	resolveSIR.setSeverity((String) sirParams.get(Constants.EXECUTE_SIR_SEVERITY));
    	resolveSIR.setOwner((String) sirParams.get(Constants.EXECUTE_SIR_OWNER));
    	
    	// Set external reference id as either REFERENCE, ALERTID or CORRELATIONID in the order
    	
    	String extRefId = null;
    	
    	if (sirParams.containsKey(Constants.EXECUTE_REFERENCE) && StringUtils.isNotBlank((String)sirParams.get(Constants.EXECUTE_REFERENCE)))
    	{
    	    extRefId = (String)sirParams.get(Constants.EXECUTE_REFERENCE);
    	}
    	
    	if (StringUtils.isBlank(extRefId) && sirParams.containsKey(Constants.EXECUTE_ALERTID) &&
    	    StringUtils.isNotBlank((String)sirParams.get(Constants.EXECUTE_ALERTID)))
    	{
    	    extRefId = (String)sirParams.get(Constants.EXECUTE_ALERTID);
    	}
    	
    	if (StringUtils.isBlank(extRefId) && sirParams.containsKey(Constants.EXECUTE_CORRELATIONID) &&
    	    StringUtils.isNotBlank((String)sirParams.get(Constants.EXECUTE_CORRELATIONID)))
    	{
    	    extRefId = (String)sirParams.get(Constants.EXECUTE_CORRELATIONID);
    	}
    	
    	if (StringUtils.isNotBlank(extRefId))
    	{
    	    resolveSIR.setExternalReferenceId(extRefId);
    	}
    	
    	resolveSIR.setSourceSystem((String) sirParams.get(Constants.EXECUTE_SIR_SOURCE_SYSTEM));
    	// check for problem ID being passed to use it (UI RR use case)
    	if (sirParams.containsKey(Constants.EXECUTE_SIR_PROBLEMID_ENFORSED_BY_UI_RR)) {
    		resolveSIR.setProblemId((String) sirParams.get(Constants.EXECUTE_SIR_PROBLEMID_ENFORSED_BY_UI_RR));
    	}
    	// RBA-14934 : Save All Data from a Gateway to an Investigation Record in SIR case
    	resolveSIR.setRequestData((String) sirParams.get(Constants.EXECUTE_SIR_INITIAL_REQUEST_DATA));
    	
    	if (sirParams.containsKey(Constants.EXECUTE_ORG_NAME) && 
    	    StringUtils.isNotBlank((String)sirParams.get(Constants.EXECUTE_ORG_NAME)))
    	{
    	    // Get Org Id for Org Name and set it as sysOrg in resolveSIR
    	    
    	    OrgsVO orgsVO = UserUtils.getOrg(null, (String)sirParams.get(Constants.EXECUTE_ORG_NAME));
            
            if (orgsVO != null)
            {
                resolveSIR.setSysOrg(orgsVO.getSys_id());
            }
    	}
    	
    	// upsert SIR
    	ResolveSecurityIncidentVO resolveSIRVO = PlaybookUtils.saveSecurityIncident(resolveSIR, resolveSIR.getOwner());
    	// returning back all necessary details
    	response.put(Constants.EXECUTE_PROBLEMID_ENFORCED_BY_SIR, resolveSIRVO.getProblemId());
    	response.put(Constants.EXECUTE_SIR_ID, resolveSIRVO.getSir());
    	response.put(Constants.EXECUTE_SIR_PLAYBOOK, resolveSIRVO.getPlaybook());
    	// Updating WS reference, alertid and correlationid
		Map<String, Object> referenceDetails = new HashMap<String, Object>();
		referenceDetails.put("u_reference", sirParams.get(Constants.EXECUTE_REFERENCE));
		referenceDetails.put("u_alert_id", sirParams.get(Constants.EXECUTE_ALERTID));
		referenceDetails.put("u_correlation_id", sirParams.get(Constants.EXECUTE_CORRELATIONID));
		ServiceWorksheet.updateWorksheet(resolveSIRVO.getProblemId(), referenceDetails, resolveSIR.getOwner());
		*/
        Map<String, Object> esbResponse = new HashMap<String, Object>();
        
        try
        {
            esbResponse = PlaybookUtils.createSIRAndUpdateWSReferenceDetails(sirParams);
        }
        catch (Throwable t)
        {
            Log.log.error("Failed to create SIR and update WS reference details due to " +
                          t.getLocalizedMessage() + " for SIR parameters [" + 
                          StringUtils.mapToString(sirParams, "=", ", ") + "]", t);
        }
        
    	return esbResponse;
    }
    
    public static Map<String, Object> searchWSByReferences(Map<String, Object> references) throws Exception
    {
        Map<String, Object> response = new HashMap<String, Object>();
        
        String reference = (String)references.get(Constants.EXECUTE_REFERENCE);
        String alertId = (String)references.get(Constants.EXECUTE_ALERTID);
        String correlationId = (String)references.get(Constants.EXECUTE_CORRELATIONID);
        
        if (StringUtils.isNotBlank(reference))
        {
            QueryDTO query = new QueryDTO();
            query.addFilterItem(new QueryFilter("auto", reference, "reference", QueryFilter.EQUALS, false));
            
            ResponseDTO<Worksheet> result = WorksheetSearchAPI.searchWorksheets(query, "system");
            
            if (result != null && !result.getRecords().isEmpty())
            {
                int i = 1;
                for (Worksheet ws : result.getRecords())
                {
                    if ((StringUtils.isNotBlank(alertId) && StringUtils.isNotBlank(ws.getAlertId()) &&
                         ws.getAlertId().toLowerCase().contains(alertId.toLowerCase())) ||
                        (StringUtils.isBlank(alertId) && StringUtils.isBlank(ws.getAlertId())))
                    {
                        if ((StringUtils.isNotBlank(correlationId) && StringUtils.isNotBlank(ws.getCorrelationId()) && 
                             ws.getCorrelationId().toLowerCase().contains(correlationId.toLowerCase())) ||
                            (StringUtils.isBlank(correlationId) && StringUtils.isBlank(ws.getCorrelationId())))
                        {
                            response.put(Constants.EXECUTE_SIR_PROBLEMID_ENFORSED_BY_UI_RR + (i++), ws.getSysId());
                        }
                    }
                }
            }
        }
        else if (StringUtils.isNotBlank(alertId))
        {
            QueryDTO query = new QueryDTO();
            query.addFilterItem(new QueryFilter("auto", alertId, "alertId", QueryFilter.EQUALS, false));
            
            ResponseDTO<Worksheet> result = WorksheetSearchAPI.searchWorksheets(query, "system");
            
            if (result != null && !result.getRecords().isEmpty())
            {
                int i = 1;
                for (Worksheet ws : result.getRecords())
                {
                    if ((StringUtils.isNotBlank(correlationId) && StringUtils.isNotBlank(ws.getCorrelationId()) && 
                         ws.getCorrelationId().toLowerCase().contains(correlationId.toLowerCase())) ||
                        (StringUtils.isBlank(correlationId) && StringUtils.isBlank(ws.getCorrelationId())))
                    {
                        response.put(Constants.EXECUTE_SIR_PROBLEMID_ENFORSED_BY_UI_RR + (i++), ws.getSysId());
                    }
                }
            }
        }
        else if (StringUtils.isNotBlank(correlationId))
        {
            QueryDTO query = new QueryDTO();
            query.addFilterItem(new QueryFilter("auto", correlationId, "correlationId", QueryFilter.EQUALS, false));
            
            ResponseDTO<Worksheet> result = WorksheetSearchAPI.searchWorksheets(query, "system");
            
            if (result != null && !result.getRecords().isEmpty())
            {
                int i = 1;
                for (Worksheet ws : result.getRecords())
                {
                    response.put(Constants.EXECUTE_SIR_PROBLEMID_ENFORSED_BY_UI_RR + (i++), ws.getSysId());
                }
            }
        }
        
        return response;
    }

    /*
     * Execute event from RSView
     */
    public static String executeEvent(Map params)
    {
        String result = "";

        // required parameters
        String event_eventid = (String) params.get(Constants.EVENT_EVENTID);
        String event_reference = (String) params.get(Constants.EVENT_REFERENCE);
        String processid = (String) params.get(Constants.EXECUTE_PROCESSID);

        try
        {
            if (StringUtils.isEmpty(event_eventid))
            {
                throw new Exception("FAILED: Missing EVENT_EVENTID: " + event_eventid);
            }

            if (StringUtils.isEmpty(processid) && StringUtils.isEmpty(event_reference))
            {
                throw new Exception("FAILED: Missing PROCESSID or EVENT_REFERENCE - processid: " + processid + " event_reference: " + event_reference);
            }

            expiredV2LicenseAndEnvCheck(String.format(EVENT_WITH_DETAILS, event_eventid, event_reference, processid));
            
            params.remove(Constants.EXECUTE_PROCESSID);
            result = executeProcess(params);
        }
        catch (Exception e)
        {
            result = e.getMessage();
            Log.log.error(e.getMessage(), e);
        }

        return result;
    } // executeEvent

    public static void removeProcessReplicated(Map params)
    {
        String processid = (String) params.get(Constants.EXECUTE_PROCESSID);
        String guid = (String) params.get(Constants.EXECUTE_GUID);
        String wiki = (String) params.get(Constants.EXECUTE_WIKI);
        String problemid = (String) params.get(Constants.EXECUTE_PROBLEMID);
        boolean isEvent = !Boolean.parseBoolean((String) params.get(Constants.EXECUTE_PROCESS_NOTEVENT));
        String event_reference = (String) params.get(Constants.EVENT_REFERENCE);
        boolean removeState = Boolean.parseBoolean((String) params.get(Constants.EXECUTE_REMOVESTATE));

        try
        {
        	//Only adjuct concurrent limit if execution has PROCESS_CONCURRENT_LIMIT set
        	String concurrentLimit = (String) params.get(Constants.PROCESS_CONCURRENT_LIMIT);
        	if (StringUtils.isNotBlank(concurrentLimit) && concurrentLimit.matches("\\d+") && Integer.valueOf(concurrentLimit) > 0) {
	            ProcessExecution.adjustProcessConcurrentLimit(params, guid);
        	}

            // if (!RSControlMListenerUtil.completedProcesses.containsKey(processid))
            // {
            // long t = System.currentTimeMillis();
            // RSControlMListenerUtil.completedProcesses.putIfAbsent(processid, t);
            // }

            // decrement active runbook count
            RSControlMListenerUtil.removeActiveRunbook(processid);
            Log.log.info("completed - total active runbooks: " + RSControlMListenerUtil.getActiveRunbook());
            
            // init thread processid
            Log.setProcessId(processid);
        }
        finally
        {
            Log.clearProcessId();
        }
    } // removeProcessReplicated

    public static void abortHibernatedProcess(String processId)
    {
        String status = Constants.ASSESS_STATUS_COMPLETED;
        com.resolve.search.model.ProcessRequest processRequest = ServiceWorksheet.findProcessRequestById(processId);
        if (processRequest != null)
        {
            String userId = "system";

            status = processRequest.getStatus();
            if (!Constants.ASSESS_STATUS_WAITING.equals(status))
            {
                return;
            }

            Map<String, Object> params = new HashMap<String, Object>();
            params.put(Constants.EXECUTE_PROCESSID, processId);
            params.put(Constants.EXECUTE_WIKI, processRequest.getWiki());
            params.put(Constants.EXECUTE_USERID, userId);
            params.put(Constants.EXECUTE_PROBLEMID, processRequest.getProblem());
            params.put(ProcessExecution.PROCESS_TIMEOUT_ROWKEY, processId);

            abortProcess(params);
        }
    }

    public static void abortProcess(Map params)
    {
        ExecutionTimeUtil.log((String) params.get("RESOLVE.JOB_ID"), null, null, ExecutionStep.METHOD_EXECUTION, ExecutionStepPhase.START);
        
        String processid = (String) params.get(Constants.EXECUTE_PROCESSID);
        String problemid = (String) params.get(Constants.EXECUTE_PROBLEMID);

        // if PROCESSID not defined and PROBLEMID defined, get list of PROCESSID
        if (StringUtils.isEmpty(processid) && StringUtils.isNotEmpty(problemid))
        {
            Set<String> processid_set = ServiceWorksheet.getWorksheetProcessids(problemid);
            for (String id : processid_set)
            {
                params.put(Constants.EXECUTE_PROCESSID, id);
                abortProcessPrivateRetry(params);
            }
        }
        else
        {
            abortProcessPrivateRetry(params);
        }
        ExecutionTimeUtil.log((String) params.get("RESOLVE.JOB_ID"), null, null, ExecutionStep.METHOD_EXECUTION, ExecutionStepPhase.END);
    }

    private static void abortProcessPrivateRetry(Map params)
    {
        String processid = (String) params.get(Constants.EXECUTE_PROCESSID);
        Log.log.info("abortProcess processid: " + processid);

        // flag to loop again to release the resources - ** TEMP FIX TILL WE FIND OUT THE ACTUAL SOLUTION OF WHAT TO DO IF LOCK FAILS
        // this will loop 20 times for now for 30 secs each cycle. So 10 mins to do the cleanup
        int noOfRetries = 1;
        for (noOfRetries = 1; noOfRetries <= 20; noOfRetries++)
        {
            if (Log.log.isDebugEnabled()) {
                Log.log.debug("Retrying count for AbortProcess:" + noOfRetries);
            }

            boolean tryAgain = abortProcessPrivate(params);

            // if the flag is false, then break out.
            if (!tryAgain)
            {
                break;
            }
        } // end of for loop

        if (Log.log.isDebugEnabled()) {
            Log.log.debug("Total # of retries for AbortProcess:" + noOfRetries);
        }
    } // abortProcessPrivateRetry

    private static boolean abortProcessPrivate(Map params)
    {
        String processid = (String) params.get(Constants.EXECUTE_PROCESSID);

        // flag to loop again to release the resources - ** TEMP FIX TILL WE FIND OUT THE ACTUAL SOLUTION OF WHAT TO DO IF LOCK FAILS
        boolean tryAgain = true;

        Log.setProcessId(processid);

        boolean cleanupLock = false;
        if (Log.log.isDebugEnabled()) {
            Log.log.debug("AbortProcess acquired lock");
        }
        tryAgain = false;

        Boolean isHibernationAbort = (params.get(ProcessExecution.PROCESS_TIMEOUT_ROWKEY) != null);

        // check if not already cleaned up
        if (isHibernationAbort || ProcessContext.existsProcessMap(processid))
        {
            // abort process and execute abort model
            ProcessExecution.abortProcessCAS(params);
        }
        else
        {
            // clean up lock
            cleanupLock = true;
        }
        Log.clearProcessId();

        return tryAgain;
    } // abortProcessPrivate

    public static void abortWorksheet(Map params)
    {
        String problemid = (String) params.get(Constants.EXECUTE_PROBLEMID);
        if (StringUtils.isNotEmpty(problemid))
        {
            Set<String> processid_set = ServiceWorksheet.getWorksheetProcessids(problemid);
            for (String id : processid_set)
            {
                params.put(Constants.EXECUTE_PROCESSID, id);
                abortProcessPrivateRetry(params);
            }
        }
        
        if (params.containsKey(Constants.EXECUTE_INCIDENT_ID)) {
            String rootResolutionId = (String) params.get(Constants.EXECUTE_ROOT_RUNBOOK_ID);
            String incidentId = (String) params.get(Constants.EXECUTE_INCIDENT_ID);
            EventMessage em = EventMessageFactory.getInstance().getIncidentResolutionAbortedMessage(incidentId, rootResolutionId);
            Main.esb.sendInternalMessage(Constants.ESB_NAME_EVENT_SOURCING_DCS, "", em.toJSON());
        }
        
        
    } // abortWorksheet

    public static String abortTask(Map params)
    {
        String result = "";

        String executeid = (String) params.get(Constants.EXECUTE_EXECUTEID);
        String processid = (String) params.get(Constants.EXECUTE_PROCESSID);
        String taskid = (String) params.get(Constants.EXECUTE_ACTIONID);

        try
        {
            // init thread processid
            Log.setProcessId(processid);

            // get targets
            Targets targets = ProcessExecution.initTargets(params);

            // send abort
            ProcessExecution.actionAbort(executeid, taskid, targets);
            
            if(params.containsKey(Constants.EXECUTE_INCIDENT_ID)) {
                String rootResolutionId = (String) params.get(Constants.EXECUTE_ROOT_RUNBOOK_ID);
                String taskId = (String) params.get(Constants.EXECUTE_ACTION_TASK_ID);
                String incidentId = (String) params.get(Constants.EXECUTE_INCIDENT_ID);
                
                EventMessage em = EventMessageFactory.getInstance().getIncidentResolutionTaskAbortedMessage(incidentId, taskId, rootResolutionId);
                Main.esb.sendInternalMessage(Constants.ESB_NAME_EVENT_SOURCING_DCS, "", em.toJSON());
            }

        }
        catch (Throwable e)
        {
            Log.log.warn(e.getMessage(), e);
        }
        finally
        {
            Log.clearProcessId();
        }
        
        return result;
    } // abortTask

    // NOTE: this method does not replicate, cacheFlush should be sent to the topic
    @SuppressWarnings("rawtypes")
	public static void cacheFlush(Map params)
    {
        String wiki = (String) params.get(Constants.EXECUTE_WIKI);
        List<String> tasks = (List<String>) Arrays.asList(params.get(Constants.EXECUTE_ACTIONNAME)).stream().map(it->
        {
        	if(it != null) {
	        	if(it instanceof String) {
	        		return (String)it;
	        	} 
	        	else {
	        		return it.toString();
	        	}
        	}
        	else {
        		return "";
        	}
        }).collect(Collectors.toList());
        List<String> ids = (List<String>) Arrays.asList(params.get(Constants.EXECUTE_ACTIONID)).stream().map(it->
        {
        	if(it != null) {
	        	if(it instanceof String) {
	        		return (String)it;
	        	} 
	        	else {
	        		return it.toString();
	        	}
        	}
        	else {
        		return "";
        	}
        }).collect(Collectors.toList());
        Boolean flushDependencies = Boolean.parseBoolean((String)params.get("FLUSHDEPENDENCIES"));
        Boolean flushAccessRights= Boolean.parseBoolean((String)params.get("FLUSHACCESSRIGHTS"));
        if (StringUtils.isNotBlank(wiki) && flushDependencies)
        {
            Log.log.info("cacheFlush ProcessDependency for runbook : " + wiki);
            ProcessRequest.cacheFlush(wiki);
        }
        if(StringUtils.isNotBlank(wiki) && flushAccessRights)
        {
        	Log.log.info("cacheFlush Access Rights for runbook : " + wiki);
        	com.resolve.services.util.UserUtils.cacheFlush(wiki);
        }
        if(ids != null && CollectionUtils.isNotEmpty(ids))
        {
        	Log.log.info("Cache flush for ATs: " + ids);
        	ids.forEach(id -> ActionTaskUtil.removeCacheById(id));
        	ids.forEach(id -> com.resolve.services.util.UserUtils.cacheTaskFlush(id));
        }
        if(tasks != null && CollectionUtils.isNotEmpty(tasks))
        {
        	Log.log.info("Cache flush for ATs: " + tasks);
        	tasks.forEach(task -> ActionTaskUtil.removeCacheByName(task));
        }

    } // cacheFlush
    
    public static ProcessState getProcessState(String processid)
    {
        return ProcessExecution.getProcessState(processid);
    } // getProcessState

    @Deprecated
    public static void archive(Map params) {
        Log.log.info("Archiving from RSControl is not supported from Resolve 6.4. Please use RSArchive.");
    } // archive

    public static void purge(Map params)
    {
        String cutoffDate = StringUtils.getString(Constants.PURGE_DATE, params);
        boolean isLobOnly = StringUtils.getBoolean(Constants.PURGE_LOB_ONLY, params);
        if (Log.log.isDebugEnabled()) {
            Log.log.debug("Incoming parameter");
            Log.log.debug("\t DATE=" + cutoffDate);
            Log.log.debug("\t LOB ONLY=" + isLobOnly);
        }
        PurgeTable.purge(cutoffDate, isLobOnly);
    } // purge

    public static void invalidateActionTask(Map params)
    {
        Object objId = params.get(Constants.ESB_PARAM_ACTIONTASKID);
        if(Log.log.isTraceEnabled()) {
            Log.log.trace("Invalidating ActionTask: " + objId);
        }
        if (objId instanceof String)
        {
            String task_sid = (String) objId;
            removeATFromCache(task_sid);
        }
        else if (objId instanceof List)
        {
            List<String> list = (List<String>) objId;
            for (String taskId : list)
            {
                removeATFromCache(taskId);
            }
        }

    } // invalidateActionTask

    private static void removeATFromCache(String taskId)
    {
        if (StringUtils.isNotEmpty(taskId))
        {
            ProcessRequest.parameterCache.remove(taskId);
            ProcessExecution.optionCache.remove(taskId);
        }
        
        ProcessExecution.actionTaskCache.remove(taskId);
    }

    public String stopConsumers(Map params)
    {
        String result;
        try
        {
            Log.log.info("MAction closing listeners");
            MainBase.getESB().getMServer().closeListeners(false);
            result = "All non-GUID queue listeners closed";
        }
        catch (Exception e)
        {
            result = "Failed to close queue listeners: " + e.getMessage();
            Log.log.error("Failed to close queue listeners", e);
        }
        return result;
    } // stopConsumers

    public String startConsumers(Map params)
    {
        StringBuilder result = new StringBuilder();
        try
        {
            com.resolve.esb.MServer mServer = MainBase.main.esb.getMServer();

            for (Object listener : mServer.getListeners())
            {
                MListener mListener = (MListener) listener;
                mListener.init(false);
                if (result.length() > 0)
                {
                    result.append("\n");
                }
                result.append("Restarted listener " + mListener.getReceiveQueueName());
                Log.log.info("Restarted listener for queue " + mListener.getReceiveQueueName());
            }
        }
        catch (Exception e)
        {
            if (result.length() > 0)
            {
                result.append("\n");
            }
            result.append("Failed to restart listeners: " + e.getMessage());
            Log.log.error("Failed to restart listeners", e);
        }
        return result.toString();
    } // startConsumers

    /**
     * 
     * Mandatory values are
     * 
     * BUTTON_NAME
     * FORM_VIEW_NAME
     * USERNAME
     * LIST OF NAME VALUE PAIRS THAT MAPS TO <colname>:<value> for the custom table mapped to this form
     * 
     * @param params
     */
    public static Map<String, Object> submitForm(Map params)
    {
        try
        {
            return ServiceHibernate.submitForm(params);
        }
        catch (Exception e)
        {
            Log.log.error("error in submitForm ", e);
            return new HashMap<String, Object>();
        }
    } // submitForm

    public static void wakeupExistingProcess(Map params)
    {
        String processid = (String) params.get(Constants.EXECUTE_PROCESSID);
        if (ProcessExecution.getProcessState(processid) != null)
        {
            Log.log.info("Received wakeup signal for processid: " + processid + ", will try to resume event from here.");
            executeProcess(params);
        }
        else
        {
            Log.log.info("Received wakeup signal for processid: " + processid + ", ignore signal from this rscontrol.");
        }
    }

    // Activates the Resolve cluster
    public Map<String, String> activateCluster(Map<String, String> params)
    {
        Log.log.info("Cluster activation message received. Params = " + params);
        Main main = (Main) MainBase.main;
        main.activateCluster(params);

        // index all wikis and actiontasks
        HashMap<String, Object> p = new HashMap<String, Object>();
        MainBase.esb.sendInternalMessage(Constants.ESB_NAME_RSVIEW, "MIndex.indexAllWikiDocuments", p);
        MainBase.esb.sendInternalMessage(Constants.ESB_NAME_RSVIEW, "MIndex.indexAllActionTasks", p);

        return getClusterStatus(params);
    }

    // Deactivates the Resolve cluster
    public Map<String, String> deactivateCluster(Map<String, String> params)
    {
        Log.log.info("Cluster deactivation message received. Params = " + params);
        Main main = (Main) MainBase.main;
        main.deactivateCluster(params);
        return getClusterStatus(params);
    }

    // Gets the status of the Resolve Cluster, whether it's been activated.
    // Returns a Map<String, String> with a key "STATUS" and a value of
    // either "ACTIVE" and "INACTIVE"

    public Map<String, String> getClusterStatus(Map<String, String> params)
    {
        Log.log.info("Get cluster status message received (RSCONTROL). Status clusterModeActive: " + MainBase.main.isClusterModeActive() + ", params = " + params);
        Map<String, String> status = new HashMap<String, String>();
        if (MainBase.main.isClusterModeActive())
        {
            status.put("STATUS", "ACTIVE");
        }
        else
        {
            status.put("STATUS", "INACTIVE");
        }

        Log.log.info("getClusterStatus return status: " + status);
        return status;
    }

    /**
     * This method is called by components which do not have DB connection
     * to get the cluster mode from RSControl. RSRemote & RSSync etc.
     * 
     * @param params
     * @return
     */
    public Map<String, String> getClusterProperties(Map<String, String> params)
    {
        Main main = (Main) MainBase.main;
        return main.getClusterProperties();
    }

    /**
     * Ping to check whether RsControl and ESB is alive.
     * 
     * @param params
     *            : Blank HashMap
     */
    public Map<String, String> esbPing(Map<String, String> params)
    {
        params.put("RESPONSE_FROM", MainBase.main.configId.getGuid());
        return params;
    }
    
    /*
     * Receiver of a message from RSREMOTE to log no Runbook event
     * to ES. An initiative of Volume Based License initiative, RBA-13621.  
     */
    public void logNoRBEvent(Map<String, String> params)
    {
        String queryString = params.get("queryString");
        if (queryString != null)
        {
            // this request came from RR where no mapping was found.
            String [] array = queryString.split("&");
            for (String string : array)
            {
                String[] keyValue = string.split("=");
                params.put(keyValue[0], keyValue[1]);
            }
            params.remove("queryString");
        }
        params.put(LicenseEnum.RBC_SOURCE.getKeygenName(), ExecutionSummary.RBC_UNBILLABLE);
        ServiceWorksheet.createNewExecutionSummary(params, "system");
    }
    
    public void updateGatewayFilter(Map<String, String> params) {

        GatewayFilterUtil.updateGatewayFilter(params);
    }
    
    public void undeployGatewayFilter(Map<String, String> params) {

        GatewayFilterUtil.undeployGatewayFilter(params);
    }

    public Map<String, Map<String, Object>> loadGatewayFilters(Map params) {
        
        if(params == null)
            return null;
    
        String gatewayName = (String)params.get("gatewayName");
        String gatewayType = (String)params.get("gatewayType");
        String queueName = (String)params.get("queueName");
        
        if(StringUtils.isBlank(gatewayName) || StringUtils.isBlank(queueName))
            return null;
        
        Map<String, Map<String, Object>> filterMap = new HashMap<String, Map<String, Object>>();
        
        try {
            if(!StringUtils.isBlank(gatewayType)) {
                if(gatewayType.equalsIgnoreCase("push"))
                    return PushGatewayFilterUtil.loadPushGatewayFilters(gatewayName, queueName, null);
        
                if(gatewayType.equalsIgnoreCase("pull"))
                    return PullGatewayFilterUtil.loadPullGatewayFilters(gatewayName, queueName, null);
                
                if(gatewayType.equalsIgnoreCase("msg"))
                    return MSGGatewayFilterUtil.loadMSGGatewayFilters(gatewayName, queueName, null);
            }

            List<GatewayVO> filters = GatewayUtil.getInstance().getAllFilters(gatewayName, queueName, "system");
            
            if(filters.size() != 0) {
                for(GatewayVO filter:filters) {
                    Map<String, Object> oneFilter = new HashMap<String, Object>();
                    
                    Class<?> filterClass = filter.getClass();
                    Method[] methods = filterClass.getMethods();
                    
                    for(Method method:methods) {
                        String methodName = method.getName();
                        if(methodName.startsWith("get")) {
                            if(methodName.equals("getBackEndJSON"))
                                continue;
                            Object value = method.invoke(filter, (Object[])null);
                            oneFilter.put(methodName.substring(3), value);
                        }
                    }
                    
                    filterMap.put(filter.getUniqueId(), oneFilter);
                }
            }
        } catch(Exception e) {
            Log.log.error(e.getMessage(), e);
        }
        
        return filterMap;
    }

    public Map<String, String> loadGatewayFilterAttributes(Map params) {
        
        if(params == null)
            return null;
        
        String sysId = (String)params.get("sysId");
        String gatewayType = (String)params.get("gatewayType");
        
        if(StringUtils.isBlank(sysId) || StringUtils.isBlank(gatewayType))
            return null;
        
        try {
            if(gatewayType.equalsIgnoreCase("push"))
                return PushGatewayFilterUtil.getPushGatewayFilterAttributes(sysId);
            
            if(gatewayType.equalsIgnoreCase("pull"))
                return PullGatewayFilterUtil.getPullGatewayFilterAttributes(sysId);
            
            if(gatewayType.equalsIgnoreCase("msg"))
                return MSGGatewayFilterUtil.getMSGGatewayFilterAttributes(sysId);
        } catch(Exception e) {
            Log.log.error(e.getMessage(), e);
        }
        
        return null;
    }
    
    private static final void expiredV2LicenseAndEnvCheck(String msgSubstitution) throws Exception {
    	if (MainBase.main.getLicenseService().getCurrentLicense().isV2AndExpiredAndNonProd() &&
    		!LicenseEnum.LICENSE_ENV_PRODUCTION.getKeygenName().equalsIgnoreCase(MainBase.main.configGeneral.getEnv())) {
    		String msg = ERR.E10025.getMessage();
    		
    		if (StringUtils.isNotBlank(msgSubstitution)) {
    			msg = msg.replaceFirst(SaveHelper.EXPIRED_V2LICENSE_MSG_OBJECT_IDENTIFIER, msgSubstitution);
    		}
    		
    		Log.alert(ERR.E10025.getCode(), msg, Constants.ALERT_TYPE_LICENSE);
			throw new Exception(msg);
    	}
    }
    
} // MAction
