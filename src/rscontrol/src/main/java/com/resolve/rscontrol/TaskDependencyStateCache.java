/******************************************************************************
* (C) Copyright 2014
*
* Resolve Systems, LLC
*
* All rights reserved
* This software is distributed under license and may not be used, copied,
* modified, or distributed without the express written permission of
* Resolve Systems, LLC.
*
******************************************************************************/

package com.resolve.rscontrol;

import java.util.HashSet;
import java.util.Map;
import java.util.Set;
import java.util.concurrent.ConcurrentHashMap;

public class TaskDependencyStateCache
{
    // ActionProcessTaskDependency SYS_ID => ActionProcessTaskDependency -- use cache lookup for state-change dependency updates
    private Map<String,ActionProcessTaskDependency> dependencyCache = new ConcurrentHashMap<String,ActionProcessTaskDependency>();
    
    // PROCESS_SID => Set ActionProcessTaskDependency SYS_ID -- used for removal of dependencyCache for entire process
    private Map<String,Set<String>> dependencyIdCache = new ConcurrentHashMap<String,Set<String>>();

    public void addTaskDependency(String processid, ActionProcessTaskDependency dependency)
    {
        // add to cache for state change update
        dependencyCache.put(dependency.getsys_id(), dependency);
        
        // add
        Set<String> sys_ids = dependencyIdCache.get(processid);
        if (sys_ids == null)
        {
            sys_ids = new HashSet<String>();
            dependencyIdCache.put(processid, sys_ids);
        }
        sys_ids.add(dependency.getsys_id());
    } // addTaskDependency
    
    public ActionProcessTaskDependency getTaskDependency(String sys_id)
    {
        return dependencyCache.get(sys_id);
    } // getTaskDependency
    
    public void removeTaskDependency(String processid)
    {
        Set<String> sys_ids = dependencyIdCache.get(processid);
        if (sys_ids != null)
        {
            for (String sys_id : sys_ids)
            {
                dependencyCache.remove(sys_id);
            }
        }
        dependencyIdCache.remove(processid);
    } // removeActionProcessTaskDependency
    
} // TaskDependencyStateCache
