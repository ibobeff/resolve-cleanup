/******************************************************************************
* (C) Copyright 2014
*
* Resolve Systems, LLC
*
* All rights reserved
* This software is distributed under license and may not be used, copied,
* modified, or distributed without the express written permission of
* Resolve Systems, LLC.
*
******************************************************************************/

package com.resolve.rscontrol;

import java.util.Map;
import java.util.HashMap;

public class TaskDependency
{
    ActionProcessTaskRequest task;  // ActionTask Execute Request definition for the EXECUTEID
    Map<String,ActionProcessTaskRequest> dependents;                // List of dependent ActionTasks for the EXECUTEID
    
    public ActionProcessTaskRequest getTask()
    {
        return task;
    } // getTask
    
    public void setTask(ActionProcessTaskRequest task)
    {
        this.task = task;
    } // setTask
    
    public String toString()
    {
        return task+" <= "+this.dependents;
    } // toString
    
    public ActionProcessTaskRequest getDependent(String executeid)
    {
        return (ActionProcessTaskRequest)dependents.get(executeid);
    } // getDependent
    
    public void putDependent(ActionProcessTaskRequest dependent)
    {
        if (this.dependents == null)
        {
            this.dependents = new HashMap<String,ActionProcessTaskRequest>();
        }
        
        this.dependents.put(dependent.getExecute(), dependent); 
    } // putDependent
    
    public Map<String,ActionProcessTaskRequest> getDependents()
    {
        return this.dependents;
    } // getDependents
    
} // TaskDependency
