/******************************************************************************
* (C) Copyright 2014
*
* Resolve Systems, LLC
*
* All rights reserved
* This software is distributed under license and may not be used, copied,
* modified, or distributed without the express written permission of
* Resolve Systems, LLC.
*
******************************************************************************/

package com.resolve.rscontrol;

import java.io.Serializable;
import java.util.Iterator;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

import com.resolve.util.Constants;
import com.resolve.util.StringUtils;

public class Targets extends ConcurrentHashMap<String, TargetInstance> implements Serializable
{
    private static final long serialVersionUID = -87858433452997528L;

	public void add(String name)
    {
        add(name, Constants.ESB_NAME_RSREMOTE, new ConcurrentHashMap<String, TargetInstance>());
    } // add
    
    public void add(String name, Map params)
    {
        add(name, Constants.ESB_NAME_RSREMOTE, params); 
    } // add
    
    public void add(String name, String esbAddr)
    {
        add(name, esbAddr, new ConcurrentHashMap<String, TargetInstance>());
    } // add
    
    public void add(String name, String esbAddr, Map params)
    {
        TargetInstance inst = new TargetInstance(name, esbAddr, params);
        this.put(name, inst);
    } // add
    
    public void add(TargetInstance inst)
    {
        this.put(inst.getAddress(), inst);
    } // add
    
    public void remove(String name)
    {
        super.remove(name);
    } // remove
    
    public TargetInstance get(String name)
    {
        return (TargetInstance)super.get(name);
    } // get
    
    public boolean has(String name)
    {
        return this.containsKey(name);
    } // has
    
    public String getGuid(String name)
    {
        String result = null;
        
        TargetInstance inst = this.get(name);
        if (inst != null)
        {
            result = inst.getGuid();
        }
        
        return result;
    } // getGuid
    
    public String getQueue(String name)
    {
        String result = null;
        
        TargetInstance inst = this.get(name);
        if (inst != null)
        {
            result = inst.getGuid();
        }
        
        return result;
    } // getQueue
    
    public void setQueue(String name, String queueName)
    {
        TargetInstance inst = this.get(name);
        if (inst != null)
        {
            inst.setGuid(queueName);
        }
    } // setQueue
    
    public void setAllQueue(String queueName)
    {
        for (TargetInstance inst : this.values())
        {
            inst.setGuid(queueName);
        }
    } // setAllQueue
    
    public Map getParams(String name)
    {
        Map result = null;
        
        TargetInstance inst = this.get(name);
        if (inst != null)
        {
            result = inst.getParams();
        }
        
        return result;
    } // getParams
    
    public String getParam(String name, String paramName)
    {
        String result = null;
        
        TargetInstance inst = this.get(name);
        if (inst != null)
        {
            result = inst.get(paramName);
        }
        
        return result;
    } // getParam

    public void setParam(String name, String paramName, String value)
    {
        TargetInstance inst = this.get(name);
        if (inst != null)
        {
            inst.put(paramName, value);
        }
    } // setParam
    
    public String toString()
    {
    	return StringUtils.collectionOfObjectsToString(this.values(), ", ");
    } // toString

} // Targets
