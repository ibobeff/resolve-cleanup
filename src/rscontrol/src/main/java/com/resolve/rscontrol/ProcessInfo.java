/******************************************************************************
* (C) Copyright 2014
*
* Resolve Systems, LLC
*
* All rights reserved
* This software is distributed under license and may not be used, copied,
* modified, or distributed without the express written permission of
* Resolve Systems, LLC.
*
******************************************************************************/

package com.resolve.rscontrol;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.Queue;
import java.util.Set;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ConcurrentLinkedQueue;
import java.util.concurrent.ConcurrentMap;
import java.util.concurrent.ConcurrentSkipListSet;
import java.util.concurrent.atomic.AtomicLong;

import com.resolve.util.Log;

public class ProcessInfo implements Serializable
{
    private static final long serialVersionUID = -2695662466389762004L;
	String processid;
    String wiki;
    Date startTime;

    static ConcurrentMap<String, ConcurrentMap<String, Object>> globalObjects = new ConcurrentHashMap<String, ConcurrentMap<String, Object>>();
    
    public ProcessInfo(String processid, String wiki)
    {
        this.processid = processid;
        this.wiki = wiki;
        
        if (processid != null)
        {
	        this.startTime = new Date();
        }
    } // ProcessInfo
    
    /*
     * Destroy / detach references to hazel objects
     */
    public void destroy()
    {
//    	finishedProcesses.add(processid);
    	
//        // remove list references
//        Log.log.debug("Calling ProcessInfo.destroy(): " + processid);
//        try
//        {
//            Log.log.trace("stringMap != null : " + (stringMap != null));
//	        if (stringMap != null)
//	        {
//	            Log.log.debug("DESTROY FOUND: " + processid+"/STRING");
//	            stringMap.destroy();
//	            stringMap = null;
//	        }
//            else
//            {
//                Log.log.trace("stringMap is NULL for " + processid);
//
//                stringMap = Hazelcast.getMap(processid+"/STRING");
//                    
//                // get variable value
//                if (stringMap != null)
//                {
//		            Log.log.debug("DESTROY GOT: " + processid+"/STRING");
//                    stringMap.destroy();
//                }
//            }
//        }
//        catch (Throwable e)
//        {
//            Log.log.error(e.getMessage(), e);
//        }
//        
//        // remove list references
//        try
//        {
//	        if (lists != null && lists.size() > 0)
//	        {
//	            for (String key : lists)
//	            {
//	                Log.log.debug("Hazelcast getList: "+key);
//	                IList ref = Hazelcast.getList(key);
//	                if (ref != null)
//	                {
//		                Log.log.debug("destroy: "+ref);
//	                    ref.destroy();
//	                }
//	            }
//	            lists = null;
//	        }
//        }
//        catch (Throwable e)
//        {
//            Log.log.error(e.getMessage(), e);
//        }
//        
//        // remove queue references
//        try
//        {
//	        if (queues != null && queues.size() > 0)
//	        {
//	            for (String key : queues)
//	            {
//	                Log.log.debug("Hazelcast getQueue: "+key);
//	                IQueue ref = Hazelcast.getQueue(key);
//	                if (ref != null)
//	                {
//		                Log.log.debug("destroy: "+ref);
//	                    ref.destroy();
//	                }
//	            }
//	            queues = null;
//	        }
//        }
//        catch (Throwable e)
//        {
//            Log.log.error(e.getMessage(), e);
//        }
//        
//        // remove map references
//        try
//        {
//	        if (maps != null && maps.size() > 0)
//	        {
//	            for (String key : maps)
//	            {
//	                Log.log.debug("Hazelcast getMap: "+key);
//	                IMap ref = Hazelcast.getMap(key);
//	                if (ref != null)
//	                {
//		                Log.log.debug("destroy: "+ref);
//	                    ref.destroy();
//	                }
//	            }
//	            maps = null;
//	        }
//        }
//        catch (Throwable e)
//        {
//            Log.log.error(e.getMessage(), e);
//        }
//        
//        // remove set references
//        try
//        {
//	        if (sets != null && sets.size() > 0)
//	        {
//	            for (String key : sets)
//	            {
//	                Log.log.debug("Hazelcast getSet: "+key);
//	                ISet ref = Hazelcast.getSet(key);
//	                if (ref != null)
//	                {
//		                Log.log.debug("destroy: "+ref);
//	                    ref.destroy();
//	                }
//	            }
//	            sets = null;
//	        }
//        }
//        catch (Throwable e)
//        {
//            Log.log.error(e.getMessage(), e);
//        }
//        
//        // remove atomic number
//        try
//        {
//            if (numbers != null)
//            {
//	            for (String name : numbers)
//	            {
//	                AtomicNumber number = Hazelcast.getAtomicNumber(name);
//	                if (number != null)
//	                {
//		                number.destroy();
//	                }
//	            }
//	            numbers.destroy();
//            }
//        }
//        catch (Throwable e)
//        {
//            Log.log.error(e.getMessage(), e);
//        }
        
    } // destroy
    
    ConcurrentMap<String,Object> getProcessMap(String pid)
    {
        ConcurrentMap<String,Object> map = globalObjects.get(pid);
        if (map==null)
        {
            globalObjects.putIfAbsent(pid, new ConcurrentHashMap<String, Object>());
            map = globalObjects.get(pid);
        }
        return map;
    }

    void setProcessMap(ConcurrentMap<String,Object> newMap, String pid)
    {
    	globalObjects.put(pid, newMap);
    }
    
    public String get(String name)
    {
        return getString(name);
    } // get
    
    public String getString(String name)
    {
        String result = "";
        String strName = processid + "_" +  name;
        
        if (processid != null)
        {
            result = (String)getProcessMap(processid).get(strName);
            if (result==null)
            {
                getProcessMap(processid).putIfAbsent(strName, "");
                result = (String)getProcessMap(processid).get(strName);
            }
        }
        else
        {
        	Log.log.warn("Referencing ProcessInfo (GLOBALS) outside of the process context. Process may have already completed.");
        }
        return result;
    } // getString
    
    public void set(String name, String value)
    {
        setString(name, value);
    } // set
    
    public void setString(String name, String value)
    {
    	if (processid != null)
    	{
            String strName = processid + "_" +  name;
            getProcessMap(processid).put(strName, value);
    	}
        else
        {
        	Log.log.warn("Referencing ProcessInfo (GLOBALS) outside of the process context. Process may have already completed.");
        }
    } // setString
    
    public AtomicLong getNumber(String name)
    {
        AtomicLong result = null;
        if (processid!=null)
        {    
            String numberName = processid+"/NUMBER/"+name;
            result = (AtomicLong)getProcessMap(processid).get(numberName);
            if (result==null)
            {
                getProcessMap(processid).putIfAbsent(numberName, new AtomicLong());
                result = (AtomicLong)getProcessMap(processid).get(numberName);
            }
        }
        else
        {
            Log.log.warn("Referencing ProcessInfo (GLOBALS) outside of the process context. Process may have already completed.");
        }
        return result;
    } // getNumber
    
    public List getList(String name)
    {
        List result = null;
        
        if (processid != null)
        {
	            // define key
	            String key = processid+"/LIST/"+name;
	            
	            // get hazel object for key
	            result = (List)getProcessMap(processid).get(key);
	            if(result==null)
	            {
	                getProcessMap(processid).putIfAbsent(key, Collections.synchronizedList(new ArrayList()));
	                result = (List)getProcessMap(processid).get(key);
	            }
        }
        else
        {
        	Log.log.warn("Referencing ProcessInfo (GLOBALS) outside of the process context. Process may have already completed.");
        }
        
        return result;
    } // getList
    
    public Queue getQueue(String name)
    {
        Queue result = null;
        
        if (processid != null)
        {
	            // define key
	            String key = processid+"/QUEUE/"+name;
	            
	            // get hazel object for key
                result = (Queue)getProcessMap(processid).get(key);
                if(result==null)
                {
                    getProcessMap(processid).putIfAbsent(key, new ConcurrentLinkedQueue());
                    result = (Queue)getProcessMap(processid).get(key);
                }
        }
        else
        {
        	Log.log.warn("Referencing ProcessInfo (GLOBALS) outside of the process context. Process may have already completed.");
        }
        
        return result;
    } // getQueue
    
    public Map getMap(String name)
    {
        Map result = null;
        
        if (processid != null)
        {
	            String key = processid+"/MAP/"+name;
	            
	            // get hazel object for key
                result = (Map)getProcessMap(processid).get(key);
                if(result==null)
                {
                    getProcessMap(processid).putIfAbsent(key, new ConcurrentHashMap());
                    result = (Map)getProcessMap(processid).get(key);
                }
        }
        else
        {
        	Log.log.warn("Referencing ProcessInfo (GLOBALS) outside of the process context. Process may have already completed.");
        }
        
        return result;
    } // getMap
    
    public Set getSet(String name)
    {
        Set result = null;
        
        if (processid != null)
        {
	            String key = processid+"/SET/"+name;
	            
	            // get hazel object for key
                result = (Set)getProcessMap(processid).get(key);
                if(result==null)
                {
                    getProcessMap(processid).putIfAbsent(key, new ConcurrentSkipListSet());
                    result = (Set)getProcessMap(processid).get(key);
                }
        }
        else
        {
        	Log.log.warn("Referencing ProcessInfo (GLOBALS) outside of the process context. Process may have already completed.");
        }
        
        return result;
    } // getSet

    
    static void cleanupProcess(String id)
    {
        globalObjects.remove(id);
    }
    
} // ProcessInfo
