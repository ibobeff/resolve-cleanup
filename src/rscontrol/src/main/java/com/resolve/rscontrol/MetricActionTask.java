/******************************************************************************
* (C) Copyright 2014
*
* Resolve Systems, LLC
*
* All rights reserved
* This software is distributed under license and may not be used, copied,
* modified, or distributed without the express written permission of
* Resolve Systems, LLC.
*
******************************************************************************/

package com.resolve.rscontrol;

import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

import com.resolve.util.Log;

public class MetricActionTask
{
    // metricgroup/id => metricUnit => MetricActionTaskUnit => (e.g. "cr/open" => "status" => MetricActionTaskUnit)
    public static Map<String,Map<String,MetricUnitActionTask>> metrics = new ConcurrentHashMap<String,Map<String,MetricUnitActionTask>>();
    
    public static Map<String,Map<String,MetricUnitActionTask>> getMetrics()
    {
        return metrics;
    } // getMetrics
    
    public static void addMetricUnitValue(String groupName, String id, String unitName, long value, int count)
    {
        // set key
        String key = groupName+"/"+id;
        
        // get units
        Map<String,MetricUnitActionTask> units  = metrics.get(key);
        if (units == null)
        {
            units = new ConcurrentHashMap<String,MetricUnitActionTask>();
            metrics.put(key, units);
        }
        
        // get unit
        MetricUnitActionTask unit = units.get(unitName);
        if (unit == null)
        {
            unit = new MetricUnitActionTask(unitName);
            units.put(unitName, unit);
        }
        
        unit.addValue(value, count);
    } // addMetricUnitValue
    
    public static void resetMetricUnitValue(String groupName, String id, String unitName)
    {
        // set key
        String key = groupName+"/"+id;
        Map<String,MetricUnitActionTask> units  = metrics.get(key);
        if (units == null)
        {
            Log.log.error("Missing metricGroup / id: "+key, new Exception());
        }
        
        MetricUnitActionTask unit = units.get(unitName);
        if (unit == null)
        {
            Log.log.error("Missing metricUnit: "+unitName, new Exception());
        }
        
        unit.reset();
    } // resetMetricUnitValue
    

} // MetricActionTask
