/******************************************************************************
* (C) Copyright 2014
*
* Resolve Systems, LLC
*
* All rights reserved
* This software is distributed under license and may not be used, copied,
* modified, or distributed without the express written permission of
* Resolve Systems, LLC.
*
******************************************************************************/

package com.resolve.rscontrol;

import java.io.Serializable;
import java.util.Collection;
import java.util.HashMap;
import java.util.Map;
import java.util.Set;

import com.resolve.util.Constants;

public class RATParams implements Map<Object, Object>, Cloneable, Serializable
{
    private static final long serialVersionUID = 8341911526968820596L;
    private Map<Object, Object> params;
    
    public RATParams(Map<Object, Object> params)
    {
        this.params = params;
    }
    
    @Override
    public int size()
    {
        return params.size();
    }

    @Override
    public boolean isEmpty()
    {
        return params.isEmpty();
    }

    @Override
    public boolean containsKey(Object key)
    {
        return params.containsKey(key);
    }

    @Override
    public boolean containsValue(Object value)
    {
        return params.containsValue(value);
    }

    @Override
    public Object get(Object key)
    {
        return params.get(key);
    }

    @Override
    public Object put(Object key, Object value)
    {
        if (key != null && key instanceof String && 
            (((String)key).equals(Constants.GROOVY_BINDING_RRATCONTEXTHELPER) ||
             ((String)key).equals(Constants.EXECUTE_ORG_NAME) ||
             ((String)key).equals(Constants.EXECUTE_ORG_ID)))
        {
            Object oldValue = params.get(key);
            
            if (oldValue == null)
            {
                params.put(key, value);
            }
            
            return oldValue;
        }
        else
        {
            return params.put(key, value);
        }
    }

    @Override
    public Object remove(Object key)
    {
        if (key != null && key instanceof String && 
            (((String)key).equals(Constants.GROOVY_BINDING_RRATCONTEXTHELPER) ||
             ((String)key).equals(Constants.EXECUTE_ORG_NAME) ||
             ((String)key).equals(Constants.EXECUTE_ORG_ID)))
        {
            return params.get(key);
        }
        else
        {
            return params.remove(key);
        }
    }

    @Override
    public void putAll(Map<?, ?> m)
    {
        Map<?, ?> tmp = new HashMap<Object, Object>(m);
        
        if (tmp != null && tmp.containsKey(Constants.GROOVY_BINDING_RRATCONTEXTHELPER) &&
            params != null && params.containsKey(Constants.GROOVY_BINDING_RRATCONTEXTHELPER))
        {
            tmp.remove(Constants.GROOVY_BINDING_RRATCONTEXTHELPER);
        }
        else if (tmp != null && tmp.containsKey(Constants.EXECUTE_ORG_NAME) &&
                 params != null && params.containsKey(Constants.EXECUTE_ORG_NAME))
        {
            tmp.remove(Constants.EXECUTE_ORG_NAME);
        }
        else if (tmp != null && tmp.containsKey(Constants.EXECUTE_ORG_ID) &&
                 params != null && params.containsKey(Constants.EXECUTE_ORG_ID))
        {
            tmp.remove(Constants.EXECUTE_ORG_ID);
        }
        
        params.putAll(tmp);
    }

    @Override
    public void clear()
    {
        Object oldRRCtxHlprValue = null;
        
        if (params != null && params.containsKey(Constants.GROOVY_BINDING_RRATCONTEXTHELPER))
        {
            oldRRCtxHlprValue = params.get(Constants.GROOVY_BINDING_RRATCONTEXTHELPER); 
        }
        
        Object oldOrgNameValue = null;
        
        if (params != null && params.containsKey(Constants.EXECUTE_ORG_NAME))
        {
            oldOrgNameValue = params.get(Constants.EXECUTE_ORG_NAME); 
        }
        
        Object oldOrgIdValue = null;
        
        if (params != null && params.containsKey(Constants.EXECUTE_ORG_ID))
        {
            oldOrgIdValue = params.get(Constants.EXECUTE_ORG_ID); 
        }
        
        params.clear();
        
        if (oldRRCtxHlprValue != null)
        {
            params.put(Constants.GROOVY_BINDING_RRATCONTEXTHELPER, oldRRCtxHlprValue);
        }
        
        if (oldOrgNameValue != null)
        {
            params.put(Constants.EXECUTE_ORG_NAME, oldOrgNameValue);
        }
        
        if (oldOrgIdValue != null)
        {
            params.put(Constants.EXECUTE_ORG_ID, oldOrgIdValue);
        }
    }

    @Override
    public Set<Object> keySet()
    {
        return params.keySet();
    }

    @Override
    public Collection<Object> values()
    {
        return params.values();
    }

    @Override
    public Set<java.util.Map.Entry<Object, Object>> entrySet()
    {
        return params.entrySet();
    }
}