/******************************************************************************
* (C) Copyright 2014
*
* Resolve Systems, LLC
*
* All rights reserved
* This software is distributed under license and may not be used, copied,
* modified, or distributed without the express written permission of
* Resolve Systems, LLC.
*
******************************************************************************/

package com.resolve.rscontrol;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.resolve.rsbase.MainBase;
import com.resolve.rsbase.BaseMetric;
import com.resolve.services.ServiceHibernate;
import com.resolve.services.hibernate.vo.MetricThresholdVO;
import com.resolve.services.vo.QueryDTO;
import com.resolve.util.MetricThresholdType;

/**
 * @author alokika.dash
 *
 */

/**
 * This class is used for receiving message in RSCONTROL to update thresholds used in metrics.
 */
public class MMetric
{

    public static void updateMetricThresholds(Map<String, MetricThresholdType> params)
    {
        // update thresholds
        BaseMetric.updateMetricThresholds(params);
    } //updateMetricThresholds
    
    /**
     * This method is called using a ESB message from rsremote.MMetric to populate the default 
     * thresholdProperties map with default metric threshold values in the database.
     * 
     * @param params
     */
    public static void populateDefaultMetricThresholds(Map<String, String> params)
    {
        String queue = params.get("RETURN_QUEUE");

        Map<String, MetricThresholdType> defaultThresholds = new HashMap<String, MetricThresholdType>();
        QueryDTO query = new QueryDTO();
        query.setModelName("MetricThreshold");
        query.setWhereClause("UGuid is null or UGuid = ''");

        List<MetricThresholdVO> data = ServiceHibernate.getMetricThreshold(query);
        for(MetricThresholdVO metric : data)
        {
            ServiceHibernate.processThresholdValuesInMap(metric, defaultThresholds);
            /*
            String metricname = metric.getUGroup() + "." + metric.getUName();
            MetricThresholdType metrictype = new MetricThresholdType();
            //TODO process the raw data from the database
            metrictype.setHigh(metric.getUHigh());
            metrictype.setLow(metric.getULow());
            metrictype.setActive(active);
            defaultThresholds.put(metricname, metrictype);
            */
        }

        MainBase.esb.sendMessage(queue, "MMetric.updateMetricThresholds", defaultThresholds);
        
    } //populateDefaultMetricThresholds
    
} //MMetric
