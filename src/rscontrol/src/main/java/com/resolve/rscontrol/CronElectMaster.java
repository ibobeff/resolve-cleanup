/******************************************************************************
* (C) Copyright 2014
import com.resolve.persistence.util.HibernateUtil
*
* Resolve Systems, LLC
*
* All rights reserved
* This software is distributed under license and may not be used, copied,
* modified, or distributed without the express written permission of
* Resolve Systems, LLC.
*
******************************************************************************/

package com.resolve.rscontrol;

import com.resolve.persistence.util.HibernateProxy;
import com.resolve.persistence.util.HibernateUtil;
import com.resolve.rsbase.MainBase;
import com.resolve.services.ServiceHibernate;
import com.resolve.util.Log;

public class CronElectMaster
{
    private static boolean cronActive;
    
    public static boolean isCronActive()
    {
        return cronActive;
    }
    
    public static void setCronActive(boolean b)
    {
        cronActive = b;
    }
    
    public void electMaster()
    {
        Log.log.info("cronActive = " + cronActive);
        if (cronActive)
        {
            int electionInterval = ((ConfigRegistration) Main.main.configRegistration).getElection();
            String guid = MainBase.main.configId.guid;
            
            try {
				((Main)MainBase.main).isMaster = (boolean) HibernateProxy.execute(() -> {
					return ServiceHibernate.electMaster(electionInterval, guid);
				});
			} catch (Exception e) {
                HibernateUtil.rethrowNestedTransaction(e);
				Log.log.error(e.getMessage(), e);
			}
            
            
            Log.log.info("After election, isMaster = " + ((Main)MainBase.main).isMaster);
        }
    } // electMaster

} // CronElectMaster
