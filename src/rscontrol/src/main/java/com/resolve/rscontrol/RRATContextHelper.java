/******************************************************************************
* (C) Copyright 2014
*
* Resolve Systems, LLC
*
* All rights reserved
* This software is distributed under license and may not be used, copied,
* modified, or distributed without the express written permission of
* Resolve Systems, LLC.
*
******************************************************************************/

package com.resolve.rscontrol;

import java.io.Serializable;
import java.util.HashMap;
import java.util.Map;

import com.resolve.rsbase.MainBase;
import com.resolve.util.Constants;
import com.resolve.util.CryptUtils;
import com.resolve.util.Log;

public class RRATContextHelper implements Serializable
{
    private static final long serialVersionUID = -543214522047559056L;
    private static final int DEFAULT_MIN_SECS_TO_WAIT = 5;
    private /*transient*/ String a;
    private /*transient*/ String b;
    
    public RRATContextHelper(String a, String b)
    {
        this.a = a;
        this.b = b;
    }
    
    @SuppressWarnings("unchecked")
    public String getUserCredentials(int secsToWait) throws Exception
    {
        String userCredentials = null;
        long mSecsToWait = secsToWait < DEFAULT_MIN_SECS_TO_WAIT ? DEFAULT_MIN_SECS_TO_WAIT * 1000: secsToWait * 1000;
        
        Map<String, String> params = new HashMap<String, String>();
        
        params.put(Constants.HTTP_REQUEST_TOKEN, CryptUtils.decrypt(a));
        
        Map<String, String> response = null;
        
        try
        {
            response = MainBase.getESB().call(CryptUtils.decrypt(b), "MSessionInfo.getSessionInfo", 
                                              params, mSecsToWait);
        }
        catch(Exception e)
        {
            Log.log.error("Error " + e.getMessage() + " in getting Session Info for token " + 
                          CryptUtils.decrypt(a) + " from rsview with id guid " + CryptUtils.decrypt(b), e);
            throw new Exception("Failed to get logged in user credentials");
        }
        
        if (response == null || response.isEmpty() || !response.containsKey(Constants.AUTH_USER_CREDENTIALS))
        {
            throw new Exception("Failed to get logged in user credentials");
        }
        
        userCredentials = response.get(Constants.AUTH_USER_CREDENTIALS);
        
        return userCredentials;
    }
}