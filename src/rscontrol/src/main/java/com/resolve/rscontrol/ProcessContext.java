/******************************************************************************
* (C) Copyright 2014
*
* Resolve Systems, LLC
*
* All rights reserved
* This software is distributed under license and may not be used, copied,
* modified, or distributed without the express written permission of
* Resolve Systems, LLC.
*
******************************************************************************/

package com.resolve.rscontrol;

import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.atomic.AtomicLong;

import com.resolve.util.Constants;
import com.resolve.util.Log;
import com.resolve.util.StringUtils;

public class ProcessContext {

	// processid => timeout
	static ConcurrentHashMap<String, Long> processTimeouts = new ConcurrentHashMap<String, Long>();

	static ConcurrentHashMap<String, AtomicLong> mergeTargetNumbers = new ConcurrentHashMap<String, AtomicLong>();

	// processid => map
	static ConcurrentHashMap<String, Map<String, Object>> processMaps = new ConcurrentHashMap<String, Map<String, Object>>();

	public static boolean existsProcessMap(String processid) {
		return processMaps.containsKey(processid);
	} // existsProcessMap

	@SuppressWarnings("rawtypes")
	public static Map createProcessMap(String processid, long processTimeout) {
		Map result = processMaps.get(processid);

		if (result != null) {
			return result;
		}

		// set processid timeout
		processTimeouts.put(processid, processTimeout);

		if (Log.log.isTraceEnabled()) {
			Log.log.trace(String.format("create a new process map for processid: %s", processid));
		}

		processMaps.putIfAbsent(processid, new ConcurrentHashMap<String, Object>());
		return processMaps.get(processid);
	} // createProcessMap

	public static Map<String, Object> getProcessMap(String processid) {
		Map<String, Object> result = null;

		// check if EXECUTETASK
		if (processid.equals("TASK")) {
			String taskMapId = Constants.PROCESS_MAP_PROCESS_POOL + "/TASK";
			processMaps.putIfAbsent(taskMapId, new ConcurrentHashMap<String, Object>());
			result = processMaps.get(taskMapId);
		} else {
			result = processMaps.get(processid);
			if (result == null) {
				if (Log.log.isDebugEnabled()) {
					Log.log.debug(String.format("Unable to retrieve processMap - processid: %s", processid));
				}
			}
		}
		return result;
	} // getProcessMap

	public static void removeProcessMap(String processid) {
		if (StringUtils.isNotEmpty(processid)) {
			// remove processid <=> map
			processMaps.remove(processid);

			// remove processid <=> timeout
			processTimeouts.remove(processid);
		}
	} // removeProcessMap

	public static long getProcessTimeout(String processid) {
		long result = 600;

		if (!processid.equals("TASK")) {
			result = processTimeouts.get(processid);
		}
		return result;
	} // getProcessTimeout

	@SuppressWarnings("rawtypes")
	public static Object get(String processid, String key) {
		Object result = null;

		Map processMap = getProcessMap(processid);
		if (processMap != null) {
			result = processMap.get(key);
		}
		return result;
	} // get

	@SuppressWarnings({ "rawtypes", "unchecked" })
	public static void put(String processid, String key, Object value) {
		Map procMap = getProcessMap(processid);

		if (procMap != null && value != null) {
			procMap.put(key, value);
		} else {
			if (Log.log.isTraceEnabled()) {
				if(procMap == null) {
					Log.log.trace(String.format("Failed to retrieve process map for processid: %s", processid));
				}
				if(value == null) {
					Log.log.trace(String.format("Failed to put null value into process map for processid: %s with key: %s", processid, key));
				}
			}
		}
	} // put

	@SuppressWarnings("rawtypes")
	public static void remove(String processid, String key) {
		Map processMap = getProcessMap(processid);
		if (processMap != null) {
			processMap.remove(key);
		}
	} // remove

	public static boolean processExists(String processid) {
		return processMaps.containsKey(processid);
	} // processExists

	public static AtomicLong getAtomicNumber(String name) {
		AtomicLong num = mergeTargetNumbers.get(name);
		if (num == null) {
			mergeTargetNumbers.putIfAbsent(name, new AtomicLong());
			num = mergeTargetNumbers.get(name);
		}
		return num;
	}

	public static void removeAtomicNumber(String name) {
		mergeTargetNumbers.remove(name);
	}

}
