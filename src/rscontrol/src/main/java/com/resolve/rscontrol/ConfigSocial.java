/******************************************************************************
* (C) Copyright 2014
*
* Resolve Systems, LLC
*
* All rights reserved
* This software is distributed under license and may not be used, copied,
* modified, or distributed without the express written permission of
* Resolve Systems, LLC.
*
******************************************************************************/

package com.resolve.rscontrol;

import com.resolve.util.ConfigMap;
import com.resolve.util.XDoc;

public class ConfigSocial extends ConfigMap
{
    private static final long serialVersionUID = -5956711993312495630L;
	static boolean active = true;
    static String archive = "0 0 3 * * ?";              // 3am daily
    static String dailyemail = "0 0 4 * * ?";           // 4am daily
    
    public ConfigSocial(XDoc config) throws Exception
    {
        super(config);
        
        define("active", BOOLEAN, "./SOCIAL/@ACTIVE");
        define("archive", STRING, "./SOCIAL/@ARCHIVE");
        define("dailyemail", STRING, "./SOCIAL/@DAILYEMAIL");
    } // ConfigSocial
    
    public void load()
    {
        loadAttributes();
    } // load

    public void save()
    {
        saveAttributes();
    } // save

    public static String getArchive()
    {
        return archive;
    }

    public static void setArchive(String archive)
    {
        ConfigSocial.archive = archive;
    }

    public static boolean isActive()
    {
        return active;
    }

    public static void setActive(boolean active)
    {
        ConfigSocial.active = active;
    }

    public static String getDailyemail()
    {
        return dailyemail;
    }

    public static void setDailyemail(String dailyemail)
    {
        ConfigSocial.dailyemail = dailyemail;
    }

} // ConfigSocial