/******************************************************************************
 * (C) Copyright 2014
 *
 * Resolve Systems, LLC
 *
 * All rights reserved
 * This software is distributed under license and may not be used, copied,
 * modified, or distributed without the express written permission of
 * Resolve Systems, LLC.
 *
 ******************************************************************************/

package com.resolve.rscontrol;

import com.resolve.util.CryptUtils;

public final class ConfigENC {

	private ConfigENC() {
	}

	public static void load() throws Exception {
		CryptUtils.setENCData(CryptUtils.getAESKey());
	}
}
