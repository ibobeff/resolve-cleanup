/******************************************************************************
* (C) Copyright 2014
*
* Resolve Systems, LLC
*
* All rights reserved
* This software is distributed under license and may not be used, copied,
* modified, or distributed without the express written permission of
* Resolve Systems, LLC.
*
******************************************************************************/

package com.resolve.rscontrol;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.resolve.rsbase.MainBase;
import com.resolve.persistence.model.DatabaseFilter;
import com.resolve.services.ServiceGateway;
import com.resolve.util.Log;
import com.resolve.util.StringUtils;

/**
 * @author alokika.dash
 *
 */
public class MDatabase extends MGateway
{
    private static final String MODEL_NAME = DatabaseFilter.class.getSimpleName();

    protected String getModelName()
    {
        return MODEL_NAME;
    }

    public void setPools(Map<String, String> pools)
    {
        ServiceGateway.setDatabasePools(pools);

    } // setPools

    private List<Map<String, String>> getPools(String queueName)
    {
        return ServiceGateway.getDatabasePools(queueName);
    } // getPools

    /**
     * This method is overridden because in addition to "filter" Database gateway also needs to
     * synchronize connection pools.
     */
    @Override
    public void synchronizeGateway(Map<String, String> params)
    {
        String gatewayName = params.get("GATEWAY_NAME");
        String queueName = params.get("QUEUE_NAME");
        String messageHandlerName = params.get("MESSAGE_HANDLER_NAME");
        String orgName = params.get("ORG_NAME");

        Log.log.debug("Gateway Synchronization message received with following parameters:");
        Log.log.debug("     GATEWAY_NAME: " + gatewayName);
        Log.log.debug("     QUEUE_NAME: " + queueName);
        Log.log.debug("     MESSAGE_HANDLER_NAME: " + messageHandlerName);
        if (StringUtils.isBlank(gatewayName) || StringUtils.isBlank(queueName) || StringUtils.isBlank(messageHandlerName))
        {
            Log.log.error("GATEWAY_NAME, QUEUE_NAME, MESSAGE_HANDLER_NAME must be passed along with the message for synchronizing gateways");
            throw new RuntimeException("GATEWAY_NAME, QUEUE_NAME, MESSAGE_HANDLER_NAME must be passed along with the message for synchronizing gateways");
        }
        else
        {
            Map<String, Object> messageParams = new HashMap<String, Object>();
            messageParams.put("POOLS", getPools(gatewayName));
            messageParams.put("FILTERS", getFilters(gatewayName));
            messageParams.put("NAMEPROPERTIES", getNameProperties(gatewayName));
            messageParams.put("ROUTINGSCHEMAS", getRoutingSchemas(gatewayName, orgName));

            //send the message back to the requester who sent this message.
            Log.log.trace("SENDING synchronization message to " + gatewayName + " gateway using queue " + queueName);
            if (MainBase.esb.sendInternalMessage(queueName, messageHandlerName + ".synchronizeGateway", messageParams) == false)
            {
                Log.log.warn("Failed to send synchronization message to " + gatewayName + " gateway.");
            }
        }
    }
} // MDatabase
