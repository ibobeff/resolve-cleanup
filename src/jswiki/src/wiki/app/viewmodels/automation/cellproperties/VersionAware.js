glu.defModel('RS.wiki.automation.VersionAware', {
	version: '',
	id: '',
	latest: false,
	latestStable: false,
	specificVersion: false,
	versionNumber: '',
	prevVersionNumber: '',
	specificVersionIsDisabled: true,
	latestStableIsDisabled: true,

	latestIsDisabled$: function() {
		return this.specificVersionIsDisabled && this.latestStableIsDisabled;
	},

	when_version_changed: {
		on: ['versionChanged'],
		action: function(newVal, oldVal) {
			if (!this.cell || this.loading || (Ext.isDefined(this.reqCount) && this.reqCount > 0))
				return;
			this.parentVM.fireEvent('cellversionchanged', this.cell, this.version);
		}
	},

	versionHistoryStore: null,

	changeLatestVersion: function(newVal) {
		if (this.specificVersion) {
			this.set('prevVersionNumber', this.versionNumber);
		}
		this.set('latest', newVal);
		if (newVal) {
			this.parentVM.fireEvent('cellversionchanged', this.cell, 'latest');
			//this.parentVM.fireEvent('updateSelectedTask', [this.cell]);
		}
	},

	changeLatestStableVersion: function(newVal) {
		this.set('latestStable', newVal);
		if (this.specificVersion) {
			this.set('prevVersionNumber', this.versionNumber);
		}
		if (newVal) {
			this.parentVM.fireEvent('cellversionchanged', this.cell, 'latestStable');
			//this.parentVM.fireEvent('updateSelectedTask', [this.cell]);
		}
	},

	changeSpecificVersion: function(newVal) {
		if ((this.latest || this.latestStable) && this.prevVersionNumber) {
			this.set('versionNumber', this.prevVersionNumber);
		}
		this.set('specificVersion', newVal);
		if (newVal) {
			if (this.versionNumber) {
				this.parentVM.fireEvent('cellversionchanged', this.cell, this.versionNumber);
			} else {
				var version = this.versionHistoryStore.getAt(0).get('version');
				this.set('versionNumber', version);
				this.parentVM.fireEvent('cellversionchanged', this.cell, version);
			}
			//this.parentVM.fireEvent('updateSelectedTask', [this.cell]);
		}
	},

	updateVersionNumber: function(versionNumber) {
		this.parentVM.fireEvent('cellversionchanged', this.cell, versionNumber);
		//this.parentVM.fireEvent('updateSelectedTask', [this.cell]);
	},

	populateVersion: function(cell, isNew) {
		this.resetVersion();
		this.set('loading', true);
		if (!isNew) {
			var version = cell.getAttribute('version');
			if (!version || version == "latest") {
				// null version means use latest
				this.set('latest', true);
			} else if (version == 'latestStable' || version == 'latestCommitted') {
				this.set('latestStable', true);
			} else {
				this.set('specificVersion', true);
				this.set('versionNumber', version);
			}
		}
		this.set('loading', false);
	},

	resetVersion: function() {
		this.set('loading', true);
		this.set('latest', false);
		this.set('latestStable', false);
		this.set('specificVersion', false);
		this.set('versionNumber', '');
		this.set('specificVersionIsDisabled', true);
		this.set('latestStableIsDisabled', true);
		if (!this.parentVM.versionChangedOnly) {
			this.set('prevVersionNumber', '');
		}
		this.set('loading', false);
	}
})