/******************************************************************************
* (C) Copyright 2014
*
* Resolve Systems, LLC
*
* All rights reserved
* This software is distributed under license and may not be used, copied,
* modified, or distributed without the express written permission of
* Resolve Systems, LLC.
*
******************************************************************************/

package com.resolve.sql;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.Timestamp;
import java.sql.Types;
import java.util.ArrayList;
import java.util.Date;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Random;
import java.util.TreeMap;

import com.resolve.util.GMTDate;
import com.resolve.util.Log;
import com.resolve.util.SysId;

public class SQLRecord
{
    Connection conn;
    String tablename;
    SQLMeta metadata;
    TreeMap data;
    String username;
    boolean hasHeader;
    ArrayList whereFields;
    ArrayList orderBySQL;
    ArrayList clauseSQL;
    
    // internal state variables for local query statement and resultset
    PreparedStatement resultStmt;
    ResultSet resultset;
    static int count=0; //counter used during bulk insert of wikidocs (BulkInsert.java). 
    
    public SQLRecord(Connection conn, String tablename) throws Exception
    {
        if (conn == null)
        {
            Log.log.error("NULL database connection: "+conn, new Exception());
	        throw new Exception("NULL database connection: "+conn);
        }
        
        this.metadata = SQLMeta.getTableMetaData(conn, tablename);
        if (this.metadata != null)
        {
            this.tablename = tablename;
            this.conn = conn;
            this.username = "system";
            this.hasHeader = true;
            this.data = new TreeMap();
            
            this.whereFields = null;
            this.orderBySQL = null;
            this.clauseSQL = null;
        }
    } // SQLRecord
    
    public SQLRecord(Connection conn, String tablename, TreeMap data) throws Exception
    {
        if (conn == null)
        {
            Log.log.error("NULL database connection: "+conn, new Exception());
            throw new Exception("NULL database connection: "+conn);
        }
        
        this.metadata = SQLMeta.getTableMetaData(conn, tablename);
        if (this.metadata != null)
        {
            this.tablename = tablename;
            this.conn = conn;
            this.username = "system";
            this.hasHeader = true;
            this.data = data;
            
            this.whereFields = null;
            this.orderBySQL = null;
            this.clauseSQL = null;
        }
    } // SQLRecord
    
    public void init()
    {
        this.metadata = SQLMeta.getTableMetaData(conn, tablename);
        if (this.metadata != null)
        {
            this.username = "system";
            this.hasHeader = true;
            this.data = new TreeMap();
            
            this.whereFields = null;
            this.orderBySQL = null;
            this.clauseSQL = null;
        }
    } // init
    
    // close prepared statement and resultset
    public void close()
    {
        try
        {
            if (resultStmt != null)
            {
                resultStmt.close();
            }
            if (resultset != null)
            {
                resultset.close();
            }
        }
        catch (Exception e)
        {
            Log.log.warn(e.getMessage(), e);
        }
    } // close
    
    public String getUsername()
    {
        return this.username;
    } // getUsername
    
    public void setUsername(String username)
    {
        this.username = username;
    } // setUsername
    
    public boolean hasHeader()
    {
        return hasHeader;
    } // hasHeader
    
    public void hasHeader(boolean hasHeader)
    {
        this.hasHeader = hasHeader;
    } // hasHeader
    
    public boolean isValid()
    {
        return this.tablename != null;
    } // isValid
    
    public Map getData()
    {
        Map result = new TreeMap();
        result.putAll(data);
        
        return result;
    } // getData
    
    public ResultSet getResultSet()
    {
        return resultset;
    } // getResultSet
    
    public Object get(String name)
    {
        return data.get(name);
    } // get
    
    public Object getObject(String name)
    {
        return data.get(name);
    } // getObject
    
    public String getString(String name)
    {
        return (String)data.get(name);
    } // getString
    
    public boolean getBoolean(String name)
    {
        boolean result = false;
        
        Object value = data.get(name);
        if (value != null)
        {
	        if (value instanceof Integer)
	        {
	            result = ((Integer)value).intValue() != 0;
	        }
	        else 
	        {
	            result = ((Boolean)value).booleanValue();
	        }
        }
        return result;
    } // getBoolean
    
    public int getInt(String name)
    {
        int result = 0;
        
        Object value = data.get(name);
        if (value != null)
        {
            if (value instanceof String)
            {
                result = Integer.parseInt((String)value);
            }
            else
            {
	            result = ((Integer)value).intValue();
            }
        }
        return result;
    } // getInt
    
    public long getLong(String name)
    {
        long result = 0;
        
        Object value = data.get(name);
        if (value != null)
        {
            if (value instanceof String)
            {
                result = Long.parseLong((String)value);
            }
            else
            {
	            result = ((Long)value).longValue();
            }
        }
        return result;
    } // getLong
    
    public long getTime(String name)
    {
        long result = 0;
        
        Object obj = data.get(name);
        if (obj instanceof java.util.Date)
        {
            result = ((java.util.Date)obj).getTime();
        }
        else if (obj instanceof java.sql.Timestamp)
        {
            result = ((java.sql.Timestamp)obj).getTime();
        }
        
        return result;
    } // getTimestamp
    
    public Date getDate(String name)
    {
        return (Date)data.get(name);
    } // getDate
    
    public Timestamp getTimestamp(String name)
    {
        return (Timestamp)data.get(name);
    } // getTimestamp
    
    public void put(String name, Object value)
    {
        if (metadata.containsKey(name) && value != null)
        {
	        data.put(name, value);
        }
    } // put
    
    public void put(String name, int value)
    {
        if (metadata.containsKey(name))
        {
	        data.put(name, new Integer(value));
        }
    } // put
    
    public void put(String name, long value)
    {
        if (metadata.containsKey(name))
        {
	        data.put(name, new Long(value));
        }
    } // put
    
    public void put(String name, boolean value)
    {
        if (metadata.containsKey(name))
        {
	        data.put(name, new Boolean(value));
        }
    } // put
    
    public void putAll(Map values)
    {
        for (Iterator i=values.entrySet().iterator(); i.hasNext(); )
        {
            Map.Entry entry = (Map.Entry)i.next();
            
            this.put((String)entry.getKey(), (String)entry.getValue());
        }
    } // putAll
    
    public void where(String name, String value) throws Exception
    {
        if (value == null)
        {
            whereIsNull(name);
        }
        else
        {
	        where(name, "=", value);
        }
    } // where
    
    public void where(String name, String comparison, Object value) throws Exception
    {
        if (whereFields == null)
        {
            this.whereFields = new ArrayList();
        }
            
        Field field = new Field(name, metadata.getType(name), value);
        field.setSQL(" ("+SQL.getEscapedIdentifier(name)+" "+comparison+" "+"?) ");
        whereFields.add(field);
    } // where
    
    public void whereIsNull(String name) throws Exception
    {
        if (whereFields == null)
        {
            this.whereFields = new ArrayList();
        }
            
        Field field = new Field(name, metadata.getType(name), null);
        field.setSQL(" ("+SQL.getEscapedIdentifier(name)+" IS NULL) ");
        whereFields.add(field);
    } // whereIsNull
    
    public void whereIsNotNull(String name) throws Exception
    {
        if (whereFields == null)
        {
            this.whereFields = new ArrayList();
        }
            
        Field field = new Field(name, metadata.getType(name), null);
        field.setSQL(" ("+SQL.getEscapedIdentifier(name)+" IS NOT NULL) ");
        whereFields.add(field);
    } // whereIsNotNull
    
    String getWhereSQL()
    {
        String result = "";
        
        // add where clauses
        if (whereFields != null && whereFields.size() > 0)
        {
            result += " WHERE ";
        
	        for (Iterator i=whereFields.iterator(); i.hasNext(); )
	        {
	            Field field = (Field)i.next();
	            result += field.getSQL();
	            
                if (i.hasNext())
                {
                    result += "AND ";
                }
                else
                {
                    result += " ";
                }
	        }
        }
        
        return result;
    } // getWhereSQL
    
    int getWhereValues(PreparedStatement stmt, int idx) throws Exception
    {
        // table fields
        if (whereFields != null)
        {
	        for (Iterator i=whereFields.iterator(); i.hasNext(); )
	        {
	            Field field = (Field)i.next();
	            
	            // NOTE: MAKE SURE all dates values are GMT!
				Log.log.trace("idx: "+idx+" name: "+field.getName()+" type: "+field.getType());
				if (field.getValue() != null)
				{
			        idx = setValue(stmt, idx, field.getType(), field.getValue());
				}
	        }
        }
        
        return idx;
    } // getWhereValues
    
    public void orderBy(String name) throws Exception
    {
        if (orderBySQL == null)
        {
            this.orderBySQL = new ArrayList();
        }
        orderBySQL.add(name);
    } // orderBy
    
    public void orderBy(String name, String ordering) throws Exception
    {
        if (orderBySQL == null)
        {
            this.orderBySQL = new ArrayList();
        }
        String value = name+" "+ordering;
        orderBySQL.add(value);
    } // orderBy
    
    String getOrderBy()
    {
        String result = "";
        
        // add where clauses
        if (orderBySQL != null && orderBySQL.size() > 0)
        {
            result += " ORDER BY ";
        
	        for (Iterator i=orderBySQL.iterator(); i.hasNext(); )
	        {
	            String clause = SQL.getEscapedIdentifier((String)i.next());
	            
	            result += clause;
	            if (i.hasNext())
	            {
	                result += ", ";
	            }
	            else
	            {
	                result += " ";
	            }
	        }
        }
        
        return result;
    } // getOrderBy
    
    
    public void addClause(String clause) throws Exception
    {
        if (clauseSQL == null)
        {
            this.clauseSQL = new ArrayList();
        }
        clauseSQL.add(" "+clause+" ");
    } // addClause
    
    String getClause()
    {
        String result = "";
        
        // add where clauses
        if (clauseSQL != null && clauseSQL.size() > 0)
        {
            result += " ";
        
	        for (Iterator i=clauseSQL.iterator(); i.hasNext(); )
	        {
	            String clause = (String)i.next();
	            
	            result += clause + " ";
	        }
        }
        
        return result;
    } // getClause
    
    public String insert() throws Exception
    {
        String result = null;
        
        /*
         * SQL Statement
         */
        String sql = "INSERT INTO "+tablename+" ";
        
        List fieldList = new ArrayList();
        
        // glidesoft header fields
        String sys_id = getString("SYS_ID");
        if (hasHeader)
        {
            fieldList.add("SYS_ID");
            fieldList.add("SYS_UPDATED_BY");
            fieldList.add("SYS_UPDATED_ON");
            fieldList.add("SYS_CREATED_BY");
            fieldList.add("SYS_CREATED_ON");
//            fieldList.add("SYS_MOD_COUNT");
            
            // remove duplicates
            data.remove("SYS_UPDATED_BY");
            data.remove("SYS_UPDATED_ON");
            data.remove("SYS_CREATED_BY");
            data.remove("SYS_CREATED_ON");
//            data.remove("SYS_MOD_COUNT");
        }
        else if (sys_id != null)
        {
            fieldList.add("SYS_ID");
        }
        
        // table fields
        for (Iterator i=data.entrySet().iterator(); i.hasNext(); )
        {
            Map.Entry entry = (Map.Entry)i.next();
            String key = (String)entry.getKey();
            
            if (!key.equalsIgnoreCase("SYS_ID"))
            {
	            fieldList.add(key);
            }
        }
        
        // generate SQL
        String fieldSQL = " (";
        String valueSQL = " VALUES (";
        for (Iterator i=fieldList.iterator(); i.hasNext(); )
        {
            String fieldName = SQL.dbi.getEscapedIdentifier((String)i.next());
            fieldSQL += fieldName;
            valueSQL += "?";
            
            if (i.hasNext())
            {
                fieldSQL += ",";
                valueSQL += ",";
            }
        }
        fieldSQL += ") ";
        valueSQL += ") ";
        
        // add field sql
        sql += fieldSQL;
        sql += valueSQL;
            
        // add other clauses
        sql += getClause();
        
        // debug
        Log.log.trace("sql:\n"+sql);
        
        /*
         * SQL Param values
         */
        int idx = 1;
        PreparedStatement stmt = conn.prepareStatement(sql);
        
        // glidesoft header fields
        if (hasHeader)
        {
	        if (sys_id == null)
	        {
	            sys_id = SysId.generate(data);
	        }
	        
	        Date date = GMTDate.getDate();
	        idx = setValue(stmt, idx, Types.VARCHAR, sys_id);
	        idx = setValue(stmt, idx, Types.VARCHAR, username);
	        idx = setValue(stmt, idx, Types.TIMESTAMP, date);
	        idx = setValue(stmt, idx, Types.VARCHAR, username);
	        idx = setValue(stmt, idx, Types.TIMESTAMP, date);
//	        idx = setValue(stmt, idx, Types.INTEGER, new Integer(0));
        }
        else if (sys_id != null)
        {
	        if (sys_id.equals(""))
	        {
	            sys_id = SysId.generate(data);
	        }
	        idx = setValue(stmt, idx, Types.VARCHAR, sys_id);
        }
        
        // table fields
        for (Iterator i=data.entrySet().iterator(); i.hasNext(); )
        {
            Map.Entry entry = (Map.Entry)i.next();
            String name = (String)entry.getKey();
            Object value = entry.getValue();
            
            if (value != null && !name.equalsIgnoreCase("SYS_ID"))
            {
                // NOTE: MAKE SURE all dates are GMT!
                Log.log.trace("name: "+name+" type: "+metadata.getType(name)+" value: "+value);                
		        idx = setValue(stmt, idx, metadata.getType(name), value);
            }
        }
        try
        {
            stmt.executeUpdate();
        }
        catch (Exception e)
        {
            Log.log.error("Error executing sql:" + sql, e);
            throw e;
        }
        finally
        {
            stmt.close();
        }
        
        // return sys_id
        result = sys_id;

        return result;
    } // insert
    
    /**
     * Inserts into the accessrights table 
     * @param accessrights_sys_id 
     * @param wiki_sys_id
     * @param wiki_resource_name  e.g. full name of the wiki document with the namespace
     * @return
     * @throws Exception
     */

    public String accessRightsInsert(String accessrights_sys_id, String wiki_sys_id, String wiki_resource_name) throws Exception
    {
        String result = null;
        
        /*
         * SQL Statement
         */
        String sql = "INSERT INTO "+tablename+" ";
        
        List fieldList = new ArrayList();
        
        // glidesoft header fields
        String sys_id = getString("SYS_ID");
        if (hasHeader)
        {
            fieldList.add("SYS_ID");
            fieldList.add("SYS_UPDATED_BY");
            fieldList.add("SYS_UPDATED_ON");
            fieldList.add("SYS_CREATED_BY");
            fieldList.add("SYS_CREATED_ON");
            fieldList.add("U_ADMIN_ACCESS");
            fieldList.add("U_EXECUTE_ACCESS");
            fieldList.add("U_READ_ACCESS");
            fieldList.add("U_WRITE_ACCESS");
            fieldList.add("U_RESOURCE_SYS_ID");
            fieldList.add("U_RESOURCE_NAME");
            fieldList.add("U_RESOURCE_TYPE");
//            fieldList.add("SYS_MOD_COUNT");
            
            // remove duplicates
            data.remove("SYS_UPDATED_BY");
            data.remove("SYS_UPDATED_ON");
            data.remove("SYS_CREATED_BY");
            data.remove("SYS_CREATED_ON");
            data.remove("U_ADMIN_ACCESS");
            data.remove("U_EXECUTE_ACCESS");
            data.remove("U_READ_ACCESS");
            data.remove("U_WRITE_ACCESS");
            data.remove("U_RESOURCE_SYS_ID");
            data.remove("U_RESOURCE_NAME");
            data.remove("U_RESOURCE_TYPE");
//            data.remove("SYS_MOD_COUNT");
        }
        else if (sys_id != null)
        {
            fieldList.add("SYS_ID");
        }
        
        // table fields
        for (Iterator i=data.entrySet().iterator(); i.hasNext(); )
        {
            Map.Entry entry = (Map.Entry)i.next();
            String key = (String)entry.getKey();
            
            if (!key.equalsIgnoreCase("SYS_ID"))
            {
                fieldList.add(key);
            }
        }
        
        // generate SQL
        String fieldSQL = " (";
        String valueSQL = " VALUES (";
        for (Iterator i=fieldList.iterator(); i.hasNext(); )
        {
            String fieldName = SQL.dbi.getEscapedIdentifier((String)i.next());
            fieldSQL += fieldName;
            valueSQL += "?";
            
            if (i.hasNext())
            {
                fieldSQL += ",";
                valueSQL += ",";
            }
        }
        fieldSQL += ") ";
        valueSQL += ") ";
        
        // add field sql
        sql += fieldSQL;
        sql += valueSQL;
            
        // add other clauses
        sql += getClause();
        
        // debug
        Log.log.trace("sql:\n"+sql);
        
        /*
         * SQL Param values
         */
        int idx = 1;
        PreparedStatement stmt = conn.prepareStatement(sql);
        
        String fullname= "";
        
        // glidesoft header fields
        if (hasHeader)
        {
            if (sys_id == null)
            {
                sys_id = accessrights_sys_id;
            }
            
            Date date = GMTDate.getDate();
            idx = setValue(stmt, idx, Types.VARCHAR, sys_id);
            idx = setValue(stmt, idx, Types.VARCHAR, username);
            idx = setValue(stmt, idx, Types.TIMESTAMP, date); //sys_updated_on 
            idx = setValue(stmt, idx, Types.VARCHAR, username);
            idx = setValue(stmt, idx, Types.TIMESTAMP, date); //sys_created_on
            String access = "admin,resolve_dev,resolve_process,resolve_user"; //default access rights
            idx = setValue(stmt, idx, Types.VARCHAR, access); // admin rights
            idx = setValue(stmt, idx, Types.VARCHAR, access); // edit rights
            idx = setValue(stmt, idx, Types.VARCHAR, access); // execute rights
            idx = setValue(stmt, idx, Types.VARCHAR, access); // read rights
            idx = setValue(stmt, idx, Types.VARCHAR, wiki_sys_id);
            idx = setValue(stmt, idx, Types.VARCHAR, wiki_resource_name);
            idx = setValue(stmt, idx, Types.VARCHAR, "wikidoc");
//          idx = setValue(stmt, idx, Types.INTEGER, new Integer(0));
        }
        else if (sys_id != null)
        {
            if (sys_id.equals(""))
            {
                sys_id = wiki_sys_id;
            }
            idx = setValue(stmt, idx, Types.VARCHAR, sys_id);
        }
        
        // table fields
        for (Iterator i=data.entrySet().iterator(); i.hasNext(); )
        {
            Map.Entry entry = (Map.Entry)i.next();
            String name = (String)entry.getKey();
            Object value = entry.getValue();
            
            if (value != null && !name.equalsIgnoreCase("SYS_ID"))
            {
                // NOTE: MAKE SURE all dates are GMT!
                Log.log.trace("name: "+name+" type: "+metadata.getType(name)+" value: "+value);                
                idx = setValue(stmt, idx, metadata.getType(name), value);
            }
        }
        stmt.executeUpdate();
        stmt.close();
        
        // return sys_id
        result = sys_id;

        return result;
        
    } //accessRightsInsert
    
    /**
     * Inserts into wikidoc table 
     * @param version  e.g. resolve version 3.4.1 or 3.2.5 or 3.3.1
     * @param wiki_sys_id 
     * @param accessrights_sys_id
     * @return
     * @throws Exception
     */
    public String wikiDocInsert(String version, String wiki_sys_id, String accessrights_sys_id) throws Exception
    {
        String result = null;
        
        /*
         * SQL Statement
         */
        String sql = "INSERT INTO "+tablename+" ";
        
        List fieldList = new ArrayList();
        
        // glidesoft header fields
        String sys_id = getString("SYS_ID");
        if (hasHeader)
        {
            fieldList.add("SYS_ID");
            fieldList.add("SYS_UPDATED_BY");
            fieldList.add("SYS_UPDATED_ON");
            fieldList.add("SYS_CREATED_BY");
            fieldList.add("SYS_CREATED_ON");
            fieldList.add("U_IS_DEFAULT_ROLE");
            fieldList.add("U_IS_ACTIVE");
            fieldList.add("U_IS_DELETED");
            fieldList.add("U_IS_HIDDEN");
            fieldList.add("U_IS_LOCKED");
            fieldList.add("U_IS_ROOT");
            fieldList.add("U_CONTENT");
            fieldList.add("U_NAMESPACE");
            fieldList.add("U_NAME");
            fieldList.add("U_FULLNAME");
            fieldList.add("U_VERSION");
            fieldList.add("U_ACCESSRIGHTS");
            if(version.equals("3.4.1"))
            {
                fieldList.add("U_LAST_REVIEWED_ON");
            }
//            fieldList.add("SYS_MOD_COUNT");
            
            // remove duplicates
            data.remove("SYS_UPDATED_BY");
            data.remove("SYS_UPDATED_ON");
            data.remove("SYS_CREATED_BY");
            data.remove("SYS_CREATED_ON");
            data.remove("U_IS_DEFAULT_ROLE");
            data.remove("U_IS_ACTIVE");
            data.remove("U_IS_DELETED");
            data.remove("U_IS_HIDDEN");
            data.remove("U_IS_LOCKED");
            data.remove("U_IS_ROOT");
            data.remove("U_CONTENT");
            data.remove("U_NAMESPACE");
            data.remove("U_NAME");
            data.remove("U_FULLNAME");
            data.remove("U_VERSION");
            data.remove("U_ACCESSRIGHTS");
            if(version.equals("3.4.1"))
            {
                data.remove("U_LAST_REVIEWED_ON");
            }
//            data.remove("SYS_MOD_COUNT");
        }
        else if (sys_id != null)
        {
            fieldList.add("SYS_ID");
        }
        
        // table fields
        for (Iterator i=data.entrySet().iterator(); i.hasNext(); )
        {
            Map.Entry entry = (Map.Entry)i.next();
            String key = (String)entry.getKey();
            
            if (!key.equalsIgnoreCase("SYS_ID"))
            {
                fieldList.add(key);
            }
        }
        
        // generate SQL
        String fieldSQL = " (";
        String valueSQL = " VALUES (";
        for (Iterator i=fieldList.iterator(); i.hasNext(); )
        {
            String fieldName = SQL.dbi.getEscapedIdentifier((String)i.next());
            fieldSQL += fieldName;
            valueSQL += "?";
            
            if (i.hasNext())
            {
                fieldSQL += ",";
                valueSQL += ",";
            }
        }
        fieldSQL += ") ";
        valueSQL += ") ";
        
        // add field sql
        sql += fieldSQL;
        sql += valueSQL;
            
        // add other clauses
        sql += getClause();
        
        // debug
        Log.log.trace("sql:\n"+sql);
        
        /*
         * SQL Param values
         */
        int idx = 1;
        PreparedStatement stmt = conn.prepareStatement(sql);
        
        String fullname= "";
        
        // glidesoft header fields
        if (hasHeader)
        {
            if (sys_id == null)
            {
                sys_id = wiki_sys_id;
            }
            
            Date date = GMTDate.getDate();
            idx = setValue(stmt, idx, Types.VARCHAR, sys_id);
            idx = setValue(stmt, idx, Types.VARCHAR, username);
            idx = setValue(stmt, idx, Types.TIMESTAMP, date); //sys_updated_on UTC_TIMESTAMP - INTERVAL '35' DAY
//            Date mydate = new Date();
//            mydate.setTime(date.getTime() + (-7 * 24 * 60 * 60 * 1000));
//            idx = setValue(stmt, idx, Types.TIMESTAMP, mydate);
            
            //randomly pick an author
            String author = pickAuthor();
            idx = setValue(stmt, idx, Types.VARCHAR, author);
            idx = setValue(stmt, idx, Types.TIMESTAMP, date); //sys_created_on
//            idx = setValue(stmt, idx, Types.TIMESTAMP, new Date((date.getTime() - 7*1000*60*60*24)));
            idx = setValue(stmt, idx, Types.CHAR, new String("Y"));
            idx = setValue(stmt, idx, Types.CHAR, new String("Y"));
            idx = setValue(stmt, idx, Types.CHAR, new String("N"));
            idx = setValue(stmt, idx, Types.CHAR, new String("N"));
            idx = setValue(stmt, idx, Types.CHAR, new String("N"));
            idx = setValue(stmt, idx, Types.CHAR, new String("N"));
            //randomly pick a namespace
            String content = "{section:type=SOURCE|title=main|id=rssection_main|height=300}" +
            		" Auto Generated Wiki Document from BulkInsert script. " +
                    "{section}";      
            String namespace = pickNamespace();
            idx = setValue(stmt, idx, Types.CLOB, content);
            idx = setValue(stmt, idx, Types.VARCHAR, namespace);
            //serially pick a wiki name
            String wikiname = pickWikiName();
            idx = setValue(stmt, idx, Types.VARCHAR, wikiname);
            // concatenate the two strings to generate the full name of wiki
            fullname = namespace+"."+wikiname;
            idx = setValue(stmt, idx, Types.VARCHAR, fullname);
            //U_Version
            idx = setValue(stmt, idx, Types.INTEGER, new Integer(2));
            //U_ACCESSRIGHTS
            idx = setValue(stmt, idx, Types.VARCHAR, accessrights_sys_id);
            
            if(version.equals("3.4.1"))
            {
                //set the last reviewed date 1 or 2 days ahead of time
                int days = 1;
                //int days = pickLastReviewedDate();
                idx = setValue(stmt, idx, Types.TIMESTAMP, new Date((date.getTime() + days*1000*60*60*24)));
            }
        }
        else if (sys_id != null)
        {
            if (sys_id.equals(""))
            {
                sys_id = wiki_sys_id;
            }
            idx = setValue(stmt, idx, Types.VARCHAR, sys_id);
        }
        
        // table fields
        for (Iterator i=data.entrySet().iterator(); i.hasNext(); )
        {
            Map.Entry entry = (Map.Entry)i.next();
            String name = (String)entry.getKey();
            Object value = entry.getValue();
            
            if (value != null && !name.equalsIgnoreCase("SYS_ID"))
            {
                // NOTE: MAKE SURE all dates are GMT!
                Log.log.trace("name: "+name+" type: "+metadata.getType(name)+" value: "+value);                
                idx = setValue(stmt, idx, metadata.getType(name), value);
            }
        }
        stmt.executeUpdate();
        stmt.close();
        
        // return sys_id
        result = sys_id;

        //return result;
        return fullname;
    } // wikiDocInsert

    private int pickLastReviewedDate()
    {
      //randomly pick an author
        int[] day_list = {1, 2, 3, 4, 5, 6, 7, 8, 9, 12, 23, 44, 57, 69, 30, 60, 90, 180, 80, 20, 15};
        Random rand = new Random();
        int choice = rand.nextInt(day_list.length);
        //System.out.println("Author = " + author_list[choice]);
        int day = day_list[choice];
        return day;
    } //pickLastReviewedDate

    /**
     * @return
     */
    private String pickAuthor()
    {
        //randomly pick an author
        String[] author_list = {"Kristina", "Paige", "Sherri", "Gretchen", "Karen", "Patrick", "Elsie", "Hazel", "Malcolm", "Dolores",
                                "Francis", "Sandy", "Marion", "Beth", "Julia", "Jerome", "Neal", "Jean", "Kristine", "Crystal",
                                "Alex", "Eric", "Wesley", "Franklin", "Claire", "Marian", "Marcia", "Dwight", "Wayne", "Stephanie",
                                "Neal", "Gretchen", "Tim", "Jerome", "Shelley", "Priscilla", "Elsie", "Beth", "Erica", "Douglas", 
                                "Donald", "Katherine", "Paul", "Patricia", "Lois", "Louis", "Christina", "Darlene", "Harvey", "William",
                                "Frederick", "Shirley", "Jason", "Judith", "Gretchen", "Don", "Glenda", "Scott", "Pat", "Michelle",
                                "Jessica", "Evan", "Melinda", "Calvin", "Eugene", "Vickie", "Luis", "Allan", "Melanie", "Marianne",
                                "Natalie", "Caroline", "Arlene", "Kyle", "Calvin", "Gary", "Samantha", "Sara", "Stacy", "Gladys",
                                "Mike", "Lynne", "Faye", "Diana", "Leon", "Ethel", "Steve", "Alison", "Sherri", "Patsy",
                                "Kelly", "Stacy", "Curtis", "Dana", "Jennifer", "Brett", "Brandon", "Keith", "Joann", "Posted", "Subscription"};
        Random rand = new Random();
        int choice = rand.nextInt(author_list.length);
        //System.out.println("Author = " + author_list[choice]);
        String author = author_list[choice];
        return author;
    } //pickAuthor
    
    /**
     * @return
     */
    private String pickNamespace()
    {
        //randomly pick a namespace
        String[] namespace_list = {"MediaResolve", "SpecialResolve", "Project1Resolve", "Project2Resolve", "Project3Resolve", "Project4Resolve", "Project5Resolve", "Project6Resolve", "Project7Resolve", "Project8Resolve",
                                   "MediaWikiResolve", "TemplateResolve", "HelpResolve", "DocResolve", "TestResolve", "UserResolve", "ManualResolve", "W3CResolve", "microsoftResolve", "apacheResolve",
                                   "XMLResolve", "WikipediaResolve", "GoogleResolve", "VehicleResolve", "DNSResolve", "JPMCResolve", "XQueryResolve", "SVGResolve", "EncryptionResolve", "HTMLResolve",
                                  };
        Random rand = new Random();
        int choice = rand.nextInt(namespace_list.length);
        //System.out.println("Namespace = " + namespace_list[choice]);
        String namespace = namespace_list[choice];
        return namespace;
    } // pickNamespace
    
    /** Randomly pick wiki names
     * @return
     */
    private String randomPickWikiName()
    {
        //randomly pick a wiki name
        Random rand = new Random();
        int start = 0;
        int end = 100000;
        int random = start + rand.nextInt(end - start + 1);
        String prefix = "resolve_";
        String wikiname = prefix + random;
        //System.out.println("Wiki name = " + wikiname);
        return wikiname;
    }
    
    /** Serially generating wiki names using count
     * @return
     */
    private String pickWikiName()
    {
        //randomly pick a wiki name
        String prefix = "resolve_";
        String wikiname = prefix + count;
        count++;
        //System.out.println("Wiki name = " + wikiname);
        return wikiname;
    } // pickWikiName
    
    public void delete(String name, String value) throws Exception
    {
        where(name, value);
        delete();
    } // delete
    
    public void delete() throws Exception
    {
        String sql = "DELETE FROM "+tablename+getWhereSQL();
        
        int idx = 1;
        PreparedStatement s = conn.prepareStatement(sql);
        idx = getWhereValues(s, idx);
        s.executeUpdate();
        s.close();
    } // delete
    
    public void update(String name, String value) throws Exception
    {
        where(name, value);
        update();
    } // update
    
    public int update() throws Exception
    {
        int result = 0;
        boolean hasPreviousField = false;
        String setFields = "";
        
        /*
         * SQL Statement
         */
        String sql = "UPDATE "+tablename+" SET ";
        
        // glidesoft header fields
        if (hasHeader)
        {
	        setFields += SQL.getEscapedIdentifier("SYS_UPDATED_BY")+"=?, ";
	        setFields += SQL.getEscapedIdentifier("SYS_UPDATED_ON")+"=?";
	        
	        // remove sys_update_by and sys_updated_on from data
	        data.remove("SYS_UPDATED_BY");
	        data.remove("SYS_UPDATED_ON");
            
            hasPreviousField = true;
        }
        
        // table fields
        for (Iterator i=data.entrySet().iterator(); i.hasNext(); )
        {
            
            Map.Entry entry = (Map.Entry)i.next();
            String key = (String)entry.getKey();
            
            if (!key.equalsIgnoreCase("SYS_ID"))
            {
                if (hasPreviousField)
                {
	                setFields += ", ";
                }
                
	            if (entry.getValue() == null)
	            {
	                setFields += SQL.getEscapedIdentifier(key)+"=NULL";
                    hasPreviousField = true;
	            }
	            else
	            {
	                setFields += SQL.getEscapedIdentifier(key)+"=?";
                    hasPreviousField = true;
	            }
            }
        }
        
        // make sure there are fields to set
        if (!setFields.equals(""))
        {
	        // add set fields
	        sql += setFields;
	        
	        // add where clauses
	        sql += getWhereSQL();
	        
	        // add other clauses
	        sql += getClause();
	        
	        Log.log.trace("Update SQL:\n"+sql);
	        
	        /*
	         * SQL Param values
	         */
	        int idx = 1;
	        PreparedStatement stmt = conn.prepareStatement(sql);
	        
	        // header fields
	        if (hasHeader)
	        {
		        Date date = GMTDate.getDate();
		        idx = setValue(stmt, idx, Types.VARCHAR, username);
		        idx = setValue(stmt, idx, Types.TIMESTAMP, date);
	        }
	        
	        // table fields
	        for (Iterator i=data.entrySet().iterator(); i.hasNext(); )
	        {
	            Map.Entry entry = (Map.Entry)i.next();
	            String name = (String)entry.getKey();
	            Object value = entry.getValue();
	            
	            if (value != null && !name.equalsIgnoreCase("SYS_ID"))
	            {
	                // NOTE: MAKE SURE all dates are GMT!
	                Log.log.trace("name: "+name+" type: "+metadata.getType(name)+" value: "+value);                
			        idx = setValue(stmt, idx, metadata.getType(name), value);
	            }
	        }
	        
	        // where fields
	        idx = getWhereValues(stmt, idx);
	        
	        result = stmt.executeUpdate();
	        stmt.close();
        }
        
        return result;
    } // update
    
    public void updateOrInsert(String name, String value) throws Exception
    {
        // update if filter already exists, otherwise insert 
        SQLRecord query = new SQLRecord(conn, this.tablename);
        query.query(name, value);
        if (query.next())
        {
            this.update(name, value);
        }
        else
        {
            this.insert();
        }
        query.close();
    } // updateOrInsert
    
    public void query(String name, String value) throws Exception
    {
        where(name, value);
        query();
    } // query
    
    public void query() throws Exception
    {
        String sql = "SELECT * FROM "+tablename;
        
        // add where clauses
        sql += getWhereSQL();
        
        // add orderby clauses
        sql += getOrderBy();
        
        // add other clauses
        sql += getClause();
        
        // debug sql
        Log.log.trace("Query SQL: "+sql);
        
        int idx = 1;
        resultStmt = conn.prepareStatement(sql);
        
        // where fields
        idx = getWhereValues(resultStmt, idx);
        
        resultset = resultStmt.executeQuery();
    } // query
    
    public boolean next() throws Exception
    {
        boolean result = false;
        
        // get next resultset
        result = resultset.next();
        if (result)
        {
	        for (Iterator i=metadata.entrySet().iterator(); i.hasNext(); )
	        {
	            Map.Entry entry = (Map.Entry)i.next();
	            String name = (String)entry.getKey();
	            
	            int type = metadata.getType(name);
	            switch (type)
	            {
	                case Types.BIT:
	                case Types.TINYINT:
	                case Types.BOOLEAN:
			            data.put(name, resultset.getBoolean(name));
			            break;
			            
	                case Types.CLOB:
			            data.put(name, resultset.getString(name));
			            break;
			            
			        case Types.INTEGER:
			        case Types.NUMERIC:
			        case Types.SMALLINT:
			            data.put(name, resultset.getInt(name));
			            break;
			            
			        case Types.TIMESTAMP:
			            Timestamp value = resultset.getTimestamp(name);
			            if (value != null)
			            {
				            data.put(name, new Date(value.getTime()));
			            }
			            break;
			            
		            default:
			            data.put(name, resultset.getObject(name));
		                break;
	            }
	        }
        }
        
        // close the statement for the resultset
        else
        {
            if (resultStmt != null)
            {
	            resultStmt.close();
	            resultStmt = null;
            }
        }
        
        return result;
    } // next
    
    int setValue(PreparedStatement stmt, int idx, int type, Object value) throws Exception
    {
        Log.log.trace("idx: "+idx+" type: "+type+" value: "+value);
        Object formattedValue = SQL.dbi.formatValue(type, value);
        
        // handle special types
        switch (type) 
        {
            case Types.CLOB:
            case Types.LONGVARCHAR:
		        stmt.setString(idx++, formattedValue.toString());
		        break;
		        
		    default:
		        stmt.setObject(idx++, formattedValue, type);
		        break;
        }
        
        return idx;
    } // setValue

    

} // SQLRecord

class Field
{
    String sql;
    String name;
    int type;
    Object value;
    
    public Field(String name, int type, Object value)
    {
        this.name = name;
        this.type = type;
        this.value = value;
    } // Field
    
    public String getSQL()
    {
        return sql;
    }
    public void setSQL(String sql)
    {
        this.sql = sql;
    }
    public String getName()
    {
        return name;
    }
    public int getType()
    {
        return type;
    }
    public Object getValue()
    {
        return value;
    }
    public void setValue(Object value)
    {
        this.value = value;
    }
    
} // Field

