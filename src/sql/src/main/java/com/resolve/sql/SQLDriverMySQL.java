/******************************************************************************
* (C) Copyright 2014
*
* Resolve Systems, LLC
*
* All rights reserved
* This software is distributed under license and may not be used, copied,
* modified, or distributed without the express written permission of
* Resolve Systems, LLC.
*
******************************************************************************/

package com.resolve.sql;

import com.resolve.util.Constants;
import com.resolve.util.StringUtils;

public class SQLDriverMySQL extends SQLDriverBase
{
    /*
     * SQL Connection Methods
     */
    public SQLDriverMySQL(String dbname, String host, String username, String password, String url)
    {
        super(dbname);
        
        this.host = host;
        this.username = username; 
        this.password = password;
        
        // init validation query
        validationQuery = "select count(*) from DUAL";
        
        // init uri
        if (!StringUtils.isEmpty(url))
        {
            connectURI = url;
        }
        else
        {
            if(!host.contains(":")) {
              host += ":3306";
            }
            
	        connectURI = "jdbc:mysql://"+this.host+"/"+dbname;
        }
        
        // set driver options
        properties.put("user", username);
        properties.put("password", password);
        properties.put("autoReconnect", "true");
        
        // init driver
        driverClassName = Constants.MYSQLDRIVER;
    } // SQLDriverMySQL
    
} // SQLDriverMySQL
