/******************************************************************************
* (C) Copyright 2014
*
* Resolve Systems, LLC
*
* All rights reserved
* This software is distributed under license and may not be used, copied,
* modified, or distributed without the express written permission of
* Resolve Systems, LLC.
*
******************************************************************************/

package com.resolve.sql;

import java.sql.Connection;

public interface SQLDriverInterface
{
    public void start() throws Exception;
    public void stop() throws Exception;
    public Connection getConnection() throws Exception;
    public String printDriverStats() throws Exception;
    public String getEscapedIdentifier(String identifier) throws Exception;
    public Object formatValue(int sqltype, Object value) throws Exception;
    
} // SQLDriverInterface
