/******************************************************************************
* (C) Copyright 2014
*
* Resolve Systems, LLC
*
* All rights reserved
* This software is distributed under license and may not be used, copied,
* modified, or distributed without the express written permission of
* Resolve Systems, LLC.
*
******************************************************************************/

package com.resolve.sql;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

import com.resolve.util.Constants;
import com.resolve.util.StringUtils;

public class SQLDriverDB2 extends SQLDriverBase
{
    /*
     * SQL Connection Methods
     */
    public SQLDriverDB2(String dbname, String host, String username, String password, String url)
    {
        super(dbname);
        
        this.host = host;
        this.username = username;
        this.password = password;

        // init validation query
        validationQuery = "select count(*) from sysibm.sysdummy1";
        
        // update host port if not defined
        if (host.indexOf(':') < 0)
        {
            this.host += ":50000";
        }
        
        // init uri
        if (!StringUtils.isEmpty(url))
        {
            connectURI = url;
        }
        else
        {
            connectURI = "jdbc:db2://"+this.host+"/"+dbname;
        }
        
        // set driver options
        properties.put("user", username);
        properties.put("password", password);
        
        // init driver
        driverClassName = Constants.DB2DRIVER;
    } // SQLDriverDB2
    
    public Connection getConnection() throws SQLException
    {
        Connection conn =  DriverManager.getConnection("jdbc:apache:commons:dbcp:"+dbname, properties);
        //Statement stmt = conn.createStatement();
        //stmt.execute("set schema='"+ dbname + "'");
        return conn;
    } // getConnection
    
    
} // SQLDriverDB2



