/******************************************************************************
* (C) Copyright 2014
*
* Resolve Systems, LLC
*
* All rights reserved
* This software is distributed under license and may not be used, copied,
* modified, or distributed without the express written permission of
* Resolve Systems, LLC.
*
******************************************************************************/

package com.resolve.sql;

import com.resolve.util.Constants;
import com.resolve.util.StringUtils;

public class SQLDriverOracle extends SQLDriverBase
{
    /*
     * SQL Connection Methods
     */
    public SQLDriverOracle(String dbname, String host, String username, String password, String url)
    {
        super(dbname);
        
        this.host = host;
        this.username = username;
        this.password = password;
        
        // init validation query
        validationQuery = "select count(*) from DUAL";
        
        // update host port if not defined
        if (host.indexOf(':') < 0)
        {
            this.host += ":1521";
        }
        
        // explicitly set the url if defined
        if (!StringUtils.isEmpty(url))
        {
	        connectURI = url;
        }
        else
        {
	        connectURI = "jdbc:oracle:thin:@"+this.host+":"+dbname;
        }
        //jdbc:oracle:thin:[USER/PASSWORD]@[HOST][:PORT]:SID
        //jdbc:oracle:thin:[USER/PASSWORD]@//[HOST][:PORT]/SERVICE 
        
        // set driver options
        properties.put("user", username);
        properties.put("password", password);
        properties.put("SetBigStringTryClob", "true");
        
        // set driver
        driverClassName = Constants.ORACLEDRIVER;
        
        // init quotestring
        identifierQuoteString = "\"";
    } // SQLDriverOracle
    
    /*
    public Object formatValue(int type, Object obj)
    {
        Object result = obj;
        
        if (type == Types.TIMESTAMP || type == Types.DATE || type == Types.TIME) 
        {
            //Date date = (Date)obj;
            //result = new Timestamp(date.getTime());
            
	        // get rid of the trailing .0
	        String value = date.
	        if (type == Types.DATE && value != null) 
	        {
	            int x = value.indexOf(".");
	            if (x > 0)
	            {
	                value = value.substring(0, x);
	            }
	        }
	        value = super.formatValue(type, value).toString();
 
            // figure out what encoding we're in
            if (value.length() == 12)
            {
                value = "to_date(" + value + ", 'YYYY-MM-DD')";
            }
            else if (value.length() == 10) 
            {
                // oracle will assume we mean this time to be *today's* time which
                // while a rational assumption, is the wrong assumption according to iso
                // the proper assumption is that a time only value
                // really means that time on year zero (also known as January 1, 1970
                String temp = "1970-01-01 " + value;
                temp = super.formatValue(type, temp).toString();
                value = "to_date(" + temp + ", 'YYYY-MM-DD HH24:MI:SS')";
            }
            else if (value.length() >= 10 )
            {
                value = "to_date(" + value + ", 'YYYY-MM-DD HH24:MI:SS')";
            }
            
            result = value;
        }

        return result;
    } // formatValue 
    */
    
} // SQLDriverOracle
