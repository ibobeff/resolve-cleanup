/******************************************************************************
* (C) Copyright 2014
*
* Resolve Systems, LLC
*
* All rights reserved
* This software is distributed under license and may not be used, copied,
* modified, or distributed without the express written permission of
* Resolve Systems, LLC.
*
******************************************************************************/

package com.resolve.sql;

import java.sql.Connection;
import java.sql.SQLException;

public class SQLDriverNull extends SQLDriverBase
{
    /*
     * SQL Connection Methods
     */
    public SQLDriverNull()
    {
        super(null);
    } // SQLDriverNull
    
    public void start() throws Exception
    {
    } // start
    
    public void stop() throws Exception
    {
    } // stop
    
    public Connection getConnection() throws SQLException
    {
        return null;
    } // getConnection
    
    public String printDriverStats() throws SQLException
    {
        return null;
    } // printDriverStats
    
} // SQLDriverNull
