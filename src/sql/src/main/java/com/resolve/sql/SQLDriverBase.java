/******************************************************************************
* (C) Copyright 2014
*
* Resolve Systems, LLC
*
* All rights reserved
* This software is distributed under license and may not be used, copied,
* modified, or distributed without the express written permission of
* Resolve Systems, LLC.
*
******************************************************************************/

package com.resolve.sql;

import java.util.Date;
import java.util.Properties;

import java.sql.DriverManager;
import java.sql.Connection;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.sql.Types;

import org.apache.commons.pool.ObjectPool;
import org.apache.commons.pool.impl.GenericObjectPool;
import org.apache.commons.dbcp.ConnectionFactory;
import org.apache.commons.dbcp.PoolingDriver;
import org.apache.commons.dbcp.PoolableConnectionFactory;
import org.apache.commons.dbcp.DriverManagerConnectionFactory;

import com.resolve.util.ERR;
import com.resolve.util.Log;

public abstract class SQLDriverBase implements SQLDriverInterface
{
    static String driverClassName;
    static String dbname;
    static Properties properties;
    static String connectURI;
    static String validationQuery = "select count(*) from DUAL";
    static boolean useQuotes = false;
    
    String host;
    String username;
    String password;
    
    static String identifierQuoteString = "";
    
    /*
     * SQL Connection Methods
     */
    public SQLDriverBase(String dbname)
    {

        SQLDriverBase.dbname   = dbname;
        SQLDriverBase.properties = new Properties();
        
        //Remove abandoned connections
        properties.setProperty("removeAbandoned", "true");
        properties.setProperty("removeAbandonedTimeout", "300");
        properties.setProperty("removeAbandonedTimeout", "true");
        
    } // SQLDriverBase
    
    public void start() throws Exception
    {
        // load JDBC driver
        loadDriver();
        
        // init driver
        setupDriver();
    } // start
    
    public void stop() throws Exception
    {
        shutdownDriver();
    } // stop
    
    public void setConnectURI(String uri)
    {
        connectURI = uri;
    } // setConnectURI
    
    public Connection getConnection() throws SQLException
    {
        return DriverManager.getConnection("jdbc:apache:commons:dbcp:"+dbname, properties);
    } // getConnection
    
    public static void loadDriver() throws Exception 
    {
        try 
        {
            Log.log.info("Setup SQL driver");
            Class.forName(driverClassName);
        } 
        catch (ClassNotFoundException e) 
        {
            Log.alert(ERR.E20003, "Missing SQL database driver", e);
        }
    } // loadDriver
        
    public static void setupDriver() throws Exception 
    {
        Log.log.info("Setup SQL Connection Pool");
        ConnectionFactory connectionFactory = new DriverManagerConnectionFactory(connectURI,properties);
        
        // 10 max connections, block for 30 minutes
        ObjectPool connectionPool = new GenericObjectPool(null, 10, GenericObjectPool.WHEN_EXHAUSTED_BLOCK, 1000*60*30, true, true);
        PoolableConnectionFactory poolableConnectionFactory = new PoolableConnectionFactory(connectionFactory,connectionPool, null, validationQuery, false,true);
        connectionPool.setFactory(poolableConnectionFactory);
        Class.forName("org.apache.commons.dbcp.PoolingDriver");
        PoolingDriver driver = (PoolingDriver) DriverManager.getDriver("jdbc:apache:commons:dbcp:");
        
        // register drivers
        driver.registerPool(dbname,connectionPool);
    } // setupDriver
    
    public static void shutdownDriver() throws SQLException 
    {
        PoolingDriver driver = (PoolingDriver) DriverManager.getDriver("jdbc:apache:commons:dbcp:");
        driver.closePool(dbname);
    } // shutdownDriver
    
    public String printDriverStats() throws SQLException
    {
        String result = "";
        
        PoolingDriver driver = (PoolingDriver) DriverManager.getDriver("jdbc:apache:commons:dbcp:");
        ObjectPool connectionPool = driver.getConnectionPool(dbname);
        
        result += "NumActive: " + connectionPool.getNumActive()+"\n";
        result += "NumIdle: " + connectionPool.getNumIdle()+"\n";
        
        return result;
    } // printDriverStats
    
    public String getIdentifierQuoteString() 
    {
        return identifierQuoteString;
    } // getIdentifierQuoteString

    public String getEscapedIdentifier(String identifier) 
    {
        String result = identifier;
        
        if (useQuotes)
        {
	        StringBuffer sb = new StringBuffer();
	        sb.append(getIdentifierQuoteString());
	        sb.append(identifier);
	        sb.append(getIdentifierQuoteString());
	        result = sb.toString();
        }
        
        return result;
    } // getEscapedIdentifier
    
    public Object formatValue(int type, Object obj)
    {
        Object result = obj;
        
        if (obj != null)
        {
	        // string obj but different storage type
	        if (obj instanceof String)
	        {
		        String value = obj.toString();
		        switch (type) 
		        {
		            /*
			        case Types.DATE:
			        case Types.TIMESTAMP:
			        case Types.TIME:
			            break;
		            */
			
			        case Types.TINYINT: // how mySQL answers when defined as BIT
			        case Types.BIT:
			            if (value.equals("false") || value.equals("0"))
			            {
			                result = new Boolean(false);
			            }
			            else
			            {
			                result = new Boolean(true);
			            }
			            break;
			
			        case Types.DECIMAL:
			        case Types.INTEGER:
			        case Types.NUMERIC:
			        case Types.SMALLINT:
			            if ("true".equals(value))
			            {
			                result = new Integer(1);
			            }
			            else if ("false".equals(value))
			            {
			                result = new Integer(0);
			            }
			            else
			            {
				            if (value != null)
				            {
				                value = value.replaceAll(",", "");
				            }
				            result = new Integer(value);
			            }
			            break;
		        }
	        }
        
	        // native formatting
	        else
	        {
		        switch (type) 
		        {
			        case Types.DATE:
			        case Types.TIMESTAMP:
			        case Types.TIME:
		                Date date = (Date)obj;
			            result = new Timestamp(date.getTime());
			            break;
		        }
	        }
        }
        
        return result;
    } // formatValue
    
    public String escape(String toEscape) 
    {
        if (toEscape == null) // nothing ventured, nothing gained
        {
            return "NULL";
        }

        StringBuffer buf = new StringBuffer((int) (toEscape.length() * 1.1));
        buf.append('\'');

        int stringLength = toEscape.length();        
        for (int i = 0; i < stringLength; ++i) 
        {
            char c = toEscape.charAt(i);

            switch (c) {
            case 0: /* how does this string get entered? */
                buf.append('\\');
                buf.append('0');
                break;

            case '\\':
                buf.append('\\');
                buf.append('\\');
                break;

            case '\'':
                buf.append('\'');
                buf.append('\'');
                break;

            case '"': /* Better safe than sorry */
                buf.append('\\');
                buf.append('"');
                break;

            case '\032': /* EOF - This gives problems on Win32 */
                buf.append('\\');
                buf.append('Z');
                break;

            default:
                buf.append(c);
            }
        }

        buf.append('\'');
        return buf.toString();
    } // escape

    public static boolean isUseQuotes()
    {
        return useQuotes;
    }

    public static void setUseQuotes(boolean useQuotes)
    {
        SQLDriverBase.useQuotes = useQuotes;
    }

} // SQLDriverBase
