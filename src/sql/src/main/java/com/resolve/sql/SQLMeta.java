/******************************************************************************
* (C) Copyright 2014
*
* Resolve Systems, LLC
*
* All rights reserved
* This software is distributed under license and may not be used, copied,
* modified, or distributed without the express written permission of
* Resolve Systems, LLC.
*
******************************************************************************/

package com.resolve.sql;

import java.util.TreeMap;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSetMetaData;

import com.resolve.util.Log;

public class SQLMeta extends TreeMap
{
    private static final long serialVersionUID = 5224484461000735349L;
	static TreeMap metatables = new TreeMap();
    
    public static SQLMeta getTableMetaData(Connection conn, String tablename)
    {
        SQLMeta result = null;
        
        tablename = tablename.toLowerCase();
        if (metatables.containsKey(tablename))
        {
	        // get meta data
            result = (SQLMeta)metatables.get(tablename);
        }
        else
        {
            // initialize meta data
            result = initTableMetaData(conn, tablename);
        }
        
        return result;
    } // getTableMetaData
    
    static SQLMeta initTableMetaData(Connection conn, String tablename)
    {
        SQLMeta result = null;

        try
        {
            PreparedStatement s = conn.prepareStatement("SELECT * FROM " + tablename + " where 1=2");
            s.executeQuery();
            result = new SQLMeta();

            ResultSetMetaData rsmd = s.getMetaData();
            for (int i = 1; i <= rsmd.getColumnCount(); i++)
            {
                String name = rsmd.getColumnName(i).toLowerCase();

                Log.log.trace("table: "+tablename+" field: "+name+" type: "+rsmd.getColumnType(i));
                SQLMetaData colMetaData = new SQLMetaData(name, rsmd.getColumnType(i));
                if (colMetaData != null)
                {
                    result.put(name.toLowerCase(), colMetaData);
                    result.put(name.toUpperCase(), colMetaData);
                }
            }

            // set static lookup
            metatables.put(tablename, result);

        }
        catch (Exception e)
        {
            Log.log.error("Failed to initialize table metadata: " + e.getMessage(), e);
        }

        return result;
    } // initTableMetaData
    
    public boolean hasField(String name)
    {
        return this.containsKey(name);
    } // hasField

    public int getType(String name)
    {
        int result = Integer.MAX_VALUE;
        
        SQLMetaData meta = (SQLMetaData)this.get(name);
        if (meta != null)
        {
            result = meta.getType();
        }
        
        return result;
	} // getType
    
} // SQLMeta
