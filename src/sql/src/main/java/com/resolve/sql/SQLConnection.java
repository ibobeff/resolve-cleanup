/******************************************************************************
* (C) Copyright 2014
*
* Resolve Systems, LLC
*
* All rights reserved
* This software is distributed under license and may not be used, copied,
* modified, or distributed without the express written permission of
* Resolve Systems, LLC.
*
******************************************************************************/

package com.resolve.sql;

import java.sql.Connection;
import java.sql.DatabaseMetaData;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.LinkedList;
import java.util.List;

import org.apache.commons.lang3.StringUtils;
import org.owasp.esapi.ESAPI;

import com.resolve.util.Log;
import com.resolve.util.ResolveDefaultValidator;
import com.resolve.util.SQLUtils;

public class SQLConnection
{
	private static final String CONNECTION_NOT_INITIALIZED_OR_CLOSED_ERR_MSG = "SQLConnection(%d) is not initialized or is " +
																			   "already closed";
	private static final String MISMATCHED_AUTO_COMMIT_ERR_MSG = 
														"Auto commit mode of this SQLConnection(%d) and actual connection's " +
														"auto commit mode do not match";
	
    private final Connection connection;
    private List<Statement> statements = null;
    private List<PreparedStatement> preparedStatements = null;
    private List<ResultSet> resultSets = null;
    private boolean originalAutoCommit;
    private boolean currentAutoCommit;
    
    public SQLConnection(Connection connection)
    {
        if(connection == null)
        {
            throw new RuntimeException("Connection must not be null");
        }
        else
        {
            this.connection = connection;
            
            if (connection != null) {
            	try {
            		originalAutoCommit = connection.getAutoCommit();
            		currentAutoCommit = originalAutoCommit;
				} catch (SQLException e) {
					String errMsg = String.format("Error %sin identifying connection's autocommit mode", 
												  (StringUtils.isNotBlank(e.getMessage()) ? "[" + e.getMessage() + "] " : ""));
					Log.log.error(errMsg, e);
					throw new RuntimeException(errMsg, e);
				}
            }
            
            statements = new LinkedList<Statement>();
            preparedStatements = new LinkedList<PreparedStatement>();
            resultSets = new LinkedList<ResultSet>();
        }
    }

    public Connection getConnection()
    {
        return connection;
    }
    
    public void commit() throws SQLException
    {
        if(connection != null && !connection.isClosed())
        {
            connection.commit();
        }
    }
    
    /*
     * This will swallow exceptions
     */
    public void close()
    {
        SQL.close(this);
    } // close
    
    void closeComplete() {
        try {
            for (ResultSet resultSet : resultSets) {
            	if (!resultSet.isClosed()) {
            		resultSet.close();
            	}
            }
            
            for (Statement statement : statements) {
            	if (!statement.isClosed()) {
            		statement.close();
            	}
            }
            
            for (Statement preparedStatement : preparedStatements) {
                preparedStatement.close();
            }
            
            if (connection != null) {
            	/*
            	 * Restore back connection's auto commit mode before closing 
            	 * since it could be wrapped connection from connection pool
            	 */
            	if (currentAutoCommit != originalAutoCommit) {
            		connection.setAutoCommit(originalAutoCommit);
            	}
            	
                connection.close();
            }
        } catch (SQLException e) {
        	String errMsg = String.format("Error %sin completeting closing of SQLConnection with id %d", 
					   					  (StringUtils.isNotBlank(e.getMessage()) ? "[" + e.getMessage() + "] " : ""),
					   					  hashCode());
            Log.log.warn(errMsg);
            throw new RuntimeException(errMsg, e);
        }
    }
    
    public Statement createStatement() throws SQLException
    {
        Statement statement = connection.createStatement();
        statements.add(statement);
        
        return statement;
    }
    
    public PreparedStatement prepareStatement(String query) throws SQLException
    {
        PreparedStatement preparedStatement = connection.prepareStatement(query);
        preparedStatements.add(preparedStatement);
        
        return preparedStatement;
    }
    
    public ResultSet executeQuery(String query) throws SQLException
    {
        ResultSet resultSet = null;
        
        //Always create new statement.
        Statement statement = createStatement();
        statements.add(statement); // add to the list
        try {
			resultSet = statement.executeQuery(ESAPI.validator().getValidInput(
																	"Execute Query", SQLUtils.getSafeSQL(query), 
																	ResolveDefaultValidator.HQL_SQL_SAFE_STRING_VALIDATOR,
																	ResolveDefaultValidator.MAX_HQL_SQL_SAFE_STRING_LENGTH,
																	false, false));
		} catch (Exception e) {
			throw new SQLException(e);
		}
        resultSets.add(resultSet);
        
        return resultSet;
    }

    public DatabaseMetaData getMetaData() throws Exception {
        
        return connection.getMetaData();
    }

    @Override
    public int hashCode()
    {
        final int prime = 31;
        int result = 1;
        result = prime * result + ((connection == null) ? 0 : connection.hashCode());
        return result;
    }

    @Override
    public boolean equals(Object obj)
    {
        if (this == obj)
        {
            return true;
        }
        if (obj == null)
        {
            return false;
        }
        if (!(obj instanceof SQLConnection))
        {
            return false;
        }
        SQLConnection other = (SQLConnection) obj;
        if (connection == null)
        {
            if (other.connection != null)
            {
                return false;
            }
        }
        else if (!connection.equals(other.connection))
        {
            return false;
        }
        return true;
    }
    
    
    public void setAutoCommit(boolean autoCommit) throws SQLException {
    	if (connection == null || connection.isClosed()) {
    		throw new SQLException(String.format(CONNECTION_NOT_INITIALIZED_OR_CLOSED_ERR_MSG, hashCode()));
    	}
    	
    	// Check SQLConnection's current auto commit with actual connection's auto commit before setting
		if (connection.getAutoCommit() != currentAutoCommit) {
			throw new SQLException(String.format(MISMATCHED_AUTO_COMMIT_ERR_MSG, hashCode()));
		}
		
		if (currentAutoCommit != autoCommit) {
			connection.setAutoCommit(autoCommit);	
			currentAutoCommit = autoCommit;
		}
    }
    
    public boolean getAutoCommit() throws SQLException {
    	if (connection == null && !connection.isClosed()) {
    		throw new SQLException(String.format(CONNECTION_NOT_INITIALIZED_OR_CLOSED_ERR_MSG, hashCode()));
    	}
    	
    	//Refresh SQLConnection's auto commit with actual connetions's auto commit 
    	if (connection.getAutoCommit() != currentAutoCommit) {
    		currentAutoCommit = connection.getAutoCommit();
    	}
    	
    	return currentAutoCommit;
    }
}
