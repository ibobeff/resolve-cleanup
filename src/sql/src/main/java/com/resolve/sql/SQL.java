/******************************************************************************
* (C) Copyright 2014
*
* Resolve Systems, LLC
*
* All rights reserved
* This software is distributed under license and may not be used, copied,
* modified, or distributed without the express written permission of
* Resolve Systems, LLC.
*
******************************************************************************/

package com.resolve.sql;

import java.sql.SQLException;
import java.util.concurrent.atomic.AtomicInteger;

import org.apache.commons.lang.StringUtils;

import com.resolve.util.Constants;
import com.resolve.util.Log;

public class SQL
{
	private static final String ILLEAGL_ARGUMENT_ERR_MSG = "Argument %s of %s type cannot be null";
	private static final String CONN_ARGUMENT_NAME = "conn";
	private static final String SQLCONNECTION_TYPE = SQLConnection.class.getSimpleName();
	
    public static boolean debug = false;
    static SQLDriverInterface dbi;
    static AtomicInteger connectionCount = new AtomicInteger(0);
    
    public static void init(SQLDriverInterface dbi)
    {
        // init dbi
        SQL.dbi = dbi;
    } // init
    
    public static void start()
    {
        try
        {
            if (dbi == null)
            {
                throw new Exception("Database Driver Interface not initialized");
            }
            dbi.start();
        }
        catch (Exception e)
        {
            Log.log.fatal("Failed to start database driver: "+e.getMessage() ,e);
        }
    } // start
    
    public static void stop()
    {
        try 
        {
            if (dbi != null)
            {
                dbi.stop();
            }
        } 
        catch (Exception e) 
        {
            Log.log.error("Failed to stop DB SQL service: "+e.getMessage(), e);
        }
    } // stop
    
    public static SQLDriverInterface getDriver(String dbtype, String dbname, String host, String username, String password, String url, boolean debug)
    {
        SQLDriverInterface result = null;
        
        // init driver
        SQL.debug = debug;
        
        if (dbtype.equalsIgnoreCase(Constants.DB_TYPE_MYSQL))
        {
	        result = new SQLDriverMySQL(dbname, host, username, password, url);
        }
        else if (dbtype.equalsIgnoreCase(Constants.DB_TYPE_ORACLE) || dbtype.equalsIgnoreCase(Constants.DB_TYPE_ORACLE_12C))
        {
	        result = new SQLDriverOracle(dbname, host, username, password, url);
        }
        else if (dbtype.equalsIgnoreCase(Constants.DB_TYPE_DB2))
        {
            result = new SQLDriverDB2(dbname, host, username, password, url);
        }
        else
        {
            Log.log.error("Database not supported: "+dbtype);
        }
        
        return result;
    } // getDriver
    
    public static SQLConnection getConnection() throws Exception
    {
        SQLConnection result = null;
        
        if (dbi == null)
        {
            throw new Exception("Database Driver Interface not initialized");
        }
        
        result = new SQLConnection(dbi.getConnection());
        if (result.getConnection() != null)
        {
            connectionCount.incrementAndGet();
            Log.log.info(String.format("Opening connection count: %d id: %d", connectionCount.get(), result.hashCode()));
        }
        
        return result;
    } // getConnection

/*    public static Connection getConnection() throws Exception
    {
        Connection result = null;
        
        if (dbi == null)
        {
            throw new Exception("Database Driver Interface not initialized");
        }
        
        result = dbi.getConnection();
        if (result != null)
        {
            connectionCount++;
            Log.log.debug("Opening connection count: "+connectionCount+" id: "+result.hashCode());
        }
        
        return result;
    } // getConnection
*/    
    public static void close(SQLConnection conn) {
        try {
	        if (conn != null) {
	            connectionCount.decrementAndGet();
	            Log.log.info(String.format("Closing connection count: %d id: %d", connectionCount.get(), conn.hashCode()));
	            conn.closeComplete();
	        }
        } catch (Throwable t) {
        	String errMsg = String.format("Error %sin closing passed SQLConnection(%d)", 
        								  (StringUtils.isNotBlank(t.getMessage()) ? "[" + t.getMessage() + "] " : ""),
        								  conn.hashCode());
            Log.log.warn(errMsg);
            throw new RuntimeException(errMsg, t);
        }
    } // close
    
    public static int getConnectionCount()
    {
        return connectionCount.get();
    } // getConnectionCount
    
    /*
     * SQL Helper Methods
     */
    
    public static String quote(String toQuote)
    {
        return "'" + toQuote + "'";
    } // quote
    
    public static void exceptionConnClose(SQLConnection conn, Exception e)
    {
        try
        {
            Log.log.warn(e.getMessage(), e);
            if (conn != null)
            {
                close(conn);
            }
        }
        catch (Exception e2) {} // ignore
    } // exceptionConnClose
    
    public static String printDriverStats()
    {
        String result = null;
        try
        {
	        result = dbi.printDriverStats();
        }
        catch (Exception e)
        {
            Log.log.warn("Failed to print SQL driver statistics: "+e.getMessage());
        }
        return result;
    } // pritnDriverStats
    
    public static String getEscapedIdentifier(String identifier)
    {
        String result = "";
            
        try
        {
            result = dbi.getEscapedIdentifier(identifier);
        }
        catch (Exception e)
        {
            Log.log.warn("Failed to escape DB identifiers: "+e.getMessage(), e);
        }
        
        return result;
    } // getEscapedIdentifier
    
    public static void setAutoCommit(SQLConnection conn, boolean autoCommit) throws SQLException, IllegalArgumentException {
    	if (conn == null) {
    		throw new IllegalArgumentException(String.format(ILLEAGL_ARGUMENT_ERR_MSG, CONN_ARGUMENT_NAME, 
    														 SQLCONNECTION_TYPE));
    	}
    	
    	conn.setAutoCommit(autoCommit);
    }
    
    public static boolean getAutoCommit(SQLConnection conn) throws SQLException, IllegalArgumentException {
    	if (conn == null) {
    		throw new IllegalArgumentException(String.format(ILLEAGL_ARGUMENT_ERR_MSG, CONN_ARGUMENT_NAME, 
    														 SQLCONNECTION_TYPE));
    	}
    	
    	return conn.getAutoCommit();
    }
    
    public static void commit(SQLConnection conn) throws SQLException, IllegalArgumentException {
    	if (conn == null) {
    		throw new IllegalArgumentException(String.format(ILLEAGL_ARGUMENT_ERR_MSG, CONN_ARGUMENT_NAME, 
    														 SQLCONNECTION_TYPE));
    	}
    	
        conn.commit();
    }
    
} // SQL
