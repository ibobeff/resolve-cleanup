/******************************************************************************
* (C) Copyright 2014
*
* Resolve Systems, LLC
*
* All rights reserved
* This software is distributed under license and may not be used, copied,
* modified, or distributed without the express written permission of
* Resolve Systems, LLC.
*
******************************************************************************/

package com.resolve.util;

import java.io.StringReader;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.sql.Clob;
import java.util.Arrays;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import javax.sql.rowset.serial.SerialClob;

import org.apache.commons.lang.reflect.FieldUtils;
import org.apache.commons.lang3.reflect.MethodUtils;
import org.owasp.esapi.codecs.Codec;
import org.owasp.esapi.codecs.DB2Codec;
import org.owasp.esapi.codecs.MySQLCodec;
import org.owasp.esapi.codecs.MySQLCodec.Mode;

import com.resolve.rsbase.ConfigSQL;
import com.resolve.rsbase.MainBase;

import org.owasp.esapi.codecs.OracleCodec;

import net.sf.jsqlparser.expression.AllComparisonExpression;
import net.sf.jsqlparser.expression.AnalyticExpression;
import net.sf.jsqlparser.expression.AnyComparisonExpression;
import net.sf.jsqlparser.expression.CaseExpression;
import net.sf.jsqlparser.expression.CastExpression;
import net.sf.jsqlparser.expression.DateTimeLiteralExpression;
import net.sf.jsqlparser.expression.DateValue;
import net.sf.jsqlparser.expression.DoubleValue;
import net.sf.jsqlparser.expression.ExpressionVisitor;
import net.sf.jsqlparser.expression.ExtractExpression;
import net.sf.jsqlparser.expression.Function;
import net.sf.jsqlparser.expression.HexValue;
import net.sf.jsqlparser.expression.IntervalExpression;
import net.sf.jsqlparser.expression.JdbcNamedParameter;
import net.sf.jsqlparser.expression.JdbcParameter;
import net.sf.jsqlparser.expression.JsonExpression;
import net.sf.jsqlparser.expression.KeepExpression;
import net.sf.jsqlparser.expression.LongValue;
import net.sf.jsqlparser.expression.MySQLGroupConcat;
import net.sf.jsqlparser.expression.NotExpression;
import net.sf.jsqlparser.expression.NullValue;
import net.sf.jsqlparser.expression.NumericBind;
import net.sf.jsqlparser.expression.OracleHierarchicalExpression;
import net.sf.jsqlparser.expression.OracleHint;
import net.sf.jsqlparser.expression.Parenthesis;
import net.sf.jsqlparser.expression.RowConstructor;
import net.sf.jsqlparser.expression.SignedExpression;
import net.sf.jsqlparser.expression.StringValue;
import net.sf.jsqlparser.expression.TimeKeyExpression;
import net.sf.jsqlparser.expression.TimeValue;
import net.sf.jsqlparser.expression.TimestampValue;
import net.sf.jsqlparser.expression.UserVariable;
import net.sf.jsqlparser.expression.ValueListExpression;
import net.sf.jsqlparser.expression.WhenClause;
import net.sf.jsqlparser.expression.operators.arithmetic.Addition;
import net.sf.jsqlparser.expression.operators.arithmetic.BitwiseAnd;
import net.sf.jsqlparser.expression.operators.arithmetic.BitwiseLeftShift;
import net.sf.jsqlparser.expression.operators.arithmetic.BitwiseOr;
import net.sf.jsqlparser.expression.operators.arithmetic.BitwiseRightShift;
import net.sf.jsqlparser.expression.operators.arithmetic.BitwiseXor;
import net.sf.jsqlparser.expression.operators.arithmetic.Concat;
import net.sf.jsqlparser.expression.operators.arithmetic.Division;
import net.sf.jsqlparser.expression.operators.arithmetic.Modulo;
import net.sf.jsqlparser.expression.operators.arithmetic.Multiplication;
import net.sf.jsqlparser.expression.operators.arithmetic.Subtraction;
import net.sf.jsqlparser.expression.operators.conditional.AndExpression;
import net.sf.jsqlparser.expression.operators.conditional.OrExpression;
import net.sf.jsqlparser.expression.operators.relational.Between;
import net.sf.jsqlparser.expression.operators.relational.EqualsTo;
import net.sf.jsqlparser.expression.operators.relational.ExistsExpression;
import net.sf.jsqlparser.expression.operators.relational.GreaterThan;
import net.sf.jsqlparser.expression.operators.relational.GreaterThanEquals;
import net.sf.jsqlparser.expression.operators.relational.InExpression;
import net.sf.jsqlparser.expression.operators.relational.IsNullExpression;
import net.sf.jsqlparser.expression.operators.relational.JsonOperator;
import net.sf.jsqlparser.expression.operators.relational.LikeExpression;
import net.sf.jsqlparser.expression.operators.relational.Matches;
import net.sf.jsqlparser.expression.operators.relational.MinorThan;
import net.sf.jsqlparser.expression.operators.relational.MinorThanEquals;
import net.sf.jsqlparser.expression.operators.relational.NotEqualsTo;
import net.sf.jsqlparser.expression.operators.relational.RegExpMatchOperator;
import net.sf.jsqlparser.expression.operators.relational.RegExpMySQLOperator;
import net.sf.jsqlparser.parser.CCJSqlParserManager;
import net.sf.jsqlparser.schema.Column;
import net.sf.jsqlparser.statement.Statement;
import net.sf.jsqlparser.statement.create.table.CreateTable;
import net.sf.jsqlparser.statement.delete.Delete;
import net.sf.jsqlparser.statement.drop.Drop;
import net.sf.jsqlparser.statement.insert.Insert;
import net.sf.jsqlparser.statement.replace.Replace;
import net.sf.jsqlparser.statement.select.PlainSelect;
import net.sf.jsqlparser.statement.select.Select;
import net.sf.jsqlparser.statement.select.SelectVisitor;
import net.sf.jsqlparser.statement.select.SetOperationList;
import net.sf.jsqlparser.statement.select.SubSelect;
import net.sf.jsqlparser.statement.select.WithItem;
import net.sf.jsqlparser.statement.truncate.Truncate;
import net.sf.jsqlparser.statement.update.Update;
import net.sf.jsqlparser.util.deparser.CreateTableDeParser;
import net.sf.jsqlparser.util.deparser.DeleteDeParser;
import net.sf.jsqlparser.util.deparser.InsertDeParser;
import net.sf.jsqlparser.util.deparser.ReplaceDeParser;
import net.sf.jsqlparser.util.deparser.UpdateDeParser;

/**
 * Utility class for SQL related functionalities.
 */
public class SQLUtils
{
    public static final String DBTYPE_MYSQL = "MYSQL";
    public static final String DBTYPE_ORACLE = "ORACLE";
    public static final String DBTYPE_DB2 = "DB2";
    private static final String LC_FROM = "from";
    private static final String LC_WHERE = "where";
    private static final String LC_IS = "is";
    private static final String LC_NOT = "not";
    private static final String SINGLE_SPACE = " ";
    private static final String EQUAL_SIGN = "=";
    private static final String NOT_SIGN = "!";
    private static final String LC_IS_HQL = SINGLE_SPACE + LC_IS + SINGLE_SPACE;
    private static final String LC_IS_NOT_HQL = SINGLE_SPACE + LC_IS + SINGLE_SPACE + LC_NOT + SINGLE_SPACE;
    private static final String LC_IS_SQL = SINGLE_SPACE + EQUAL_SIGN + SINGLE_SPACE;
    private static final String LC_IS_NOT_SQL = SINGLE_SPACE + NOT_SIGN + EQUAL_SIGN + SINGLE_SPACE;
    private static final String STAR = "*";
    private static final String DOT = ".";
    private static final String LC_ORDER = "order";
    private static final String LC_MYORDER = "myorder";
    private static final String SPACE_LC_ORDER_BY_SPACE = " order by ";
    private static final String DOT_LC_ORDER = DOT + LC_ORDER;
    private static final String DOT_LC_MYORDER = DOT + LC_MYORDER;
    private static final String MATCH_ONE_OR_MORE_SPACES = SINGLE_SPACE + "+";
    private static final String SELECT_SQL = "select ";
    private static final String SELECT_ALL_SQL = SELECT_SQL + STAR + " ";
    private static final String RESOLVE_PERSISTENCE_MODEL_PACKAGE_PREFIX = "com.resolve.persistence.model.";
    private static final String LC_GROUP = "group";
    private static final String LC_SPACE_GROUP_SPACE_BY_SPACE = " group by ";
    private static final String LC_MASK_SPACE_GROUP_SPACE_BY_SPACE = " pourg yb ";
    private static final String LC_MYGROUP = "mygroup";
    private static final String GET_CONFIG_SQL_METHOD_NAME = "getConfigSQL";
    private static final String CONFIG_SQL_FIELD_NAME = "configSQL";
    private static final String LC_AS = "as";
    
    /*
    public static final Pattern pattern1 = Pattern.compile("^(.*)FROM\\s(\\w+)\\sAS(.*)$");
    public static final Pattern pattern2 = Pattern.compile("^(.*)JOIN\\s(\\w+)\\sAS(.*)$");*/

    //stores Codecs for currently supported databases by Resolve
    private static Map<String, Codec> codecs = new HashMap<String, Codec>();

    private static String iDBType = DBTYPE_MYSQL;
    
    static
    {
        codecs.put(DBTYPE_MYSQL, new MySQLCodec(Mode.ANSI));
        codecs.put(DBTYPE_ORACLE, new OracleCodec());
        codecs.put(DBTYPE_DB2, new DB2Codec());
        
        Method getConfigSQLMethod = MethodUtils.getAccessibleMethod(MainBase.main.getClass(), GET_CONFIG_SQL_METHOD_NAME, 
																	(Class<?>[])null);
        
        ConfigSQL configSQL = null;
        
        if (getConfigSQLMethod != null) {
        	try {
				configSQL = (ConfigSQL) getConfigSQLMethod.invoke(MainBase.main, (Object[])null);
			} catch (IllegalAccessException | IllegalArgumentException | InvocationTargetException e) {
				Log.log.warn(String.format("Error %soccurred on invoking %s to get SQL configuration", 
										   (StringUtils.isNotBlank(e.getMessage()) ? "[" + e.getMessage() + "] " : ""), 
										   GET_CONFIG_SQL_METHOD_NAME));
			}
        } else {
        	try {
				configSQL = (ConfigSQL) FieldUtils.readField(MainBase.main, CONFIG_SQL_FIELD_NAME);
			} catch (Exception e) {
				Log.log.warn(String.format("Error %soccurred on getting field named %s containing SQL configuration.", 
										   (StringUtils.isNotBlank(e.getMessage()) ? "[" + e.getMessage() + "] " : ""), 
										   CONFIG_SQL_FIELD_NAME));
			}
        }
        
        if (configSQL != null) {
        	iDBType = configSQL.getDbtype().toUpperCase();
        	Log.log.info("Identified DB Type of component based on SQL RSBase Configuration is " + iDBType + ".");
        } else {
        	Log.log.warn("Unable to identify DB Type of component, as Main neither supports " + GET_CONFIG_SQL_METHOD_NAME + 
        				 "() nor has publicly accessible " + CONFIG_SQL_FIELD_NAME + 
        				 " field.\nIdentified DB Type of component is defaulted to " + DBTYPE_MYSQL);
        }
    }

    private static String transformHQLToSQL(String sql, String databseType) {
    	String updatedSQL = sql.toLowerCase();
    	
    	String effectiveDBType = StringUtils.isNotBlank(databseType) ? databseType : iDBType;
    	
    	// Convert HQL from ..... to SQL select .... from ....
    	if (StringUtils.isNotBlank(sql) && sql.toLowerCase().startsWith(LC_FROM)) {
    		if (sql.toLowerCase().contains(LC_WHERE)) {
    			String sqlBetweenFromAndWhere = StringUtils.substringBetween(sql.toLowerCase(), LC_FROM, LC_WHERE);
    			
    			if (sqlBetweenFromAndWhere != null) {
    				sqlBetweenFromAndWhere = sqlBetweenFromAndWhere.trim();
    			}
    			
    			if (StringUtils.isNotBlank(sqlBetweenFromAndWhere)) {
	    			String[] sqlBetweenFromWhereComps = sqlBetweenFromAndWhere.split(MATCH_ONE_OR_MORE_SPACES);
	    			
	    			// Oracle does not support table alias 
	    			
	    			updatedSQL = SELECT_SQL + 
	    						 (sqlBetweenFromWhereComps.length > 1 ?
	    						  sqlBetweenFromWhereComps[sqlBetweenFromWhereComps.length - 1] + DOT : "") + 
	    						 STAR + SINGLE_SPACE + 
	    						 (!DBTYPE_ORACLE.equals(effectiveDBType) && sqlBetweenFromWhereComps.length == 2 ? 
	    						  LC_FROM + SINGLE_SPACE + sqlBetweenFromWhereComps[0] + SINGLE_SPACE + LC_AS + 
	    						  SINGLE_SPACE + sqlBetweenFromWhereComps[1] + SINGLE_SPACE + LC_WHERE +
	    						  StringUtils.substringAfter(sql.toLowerCase(), LC_WHERE) : sql.toLowerCase());
    			}
    		} else {
    			updatedSQL = SELECT_ALL_SQL + sql.toLowerCase();
    		}
    		
    		Log.log.trace("Original : [" + sql + "], Updated : [" + updatedSQL + "]");
    	}
    	
    	// Remove Resolve model package prefix com.resolve.persistence.model
    	updatedSQL = updatedSQL.replace(RESOLVE_PERSISTENCE_MODEL_PACKAGE_PREFIX, "");
    	
    	// Convert HQL <model>.field where field is another model in HQL statement with modelfield as table in SQL
    	if (updatedSQL.contains(SINGLE_SPACE + LC_FROM + SINGLE_SPACE)) {
    		updatedSQL = StringUtils.substringBefore(updatedSQL, SINGLE_SPACE + LC_FROM + SINGLE_SPACE) + 
    					 SINGLE_SPACE + LC_FROM + SINGLE_SPACE +
    					 (updatedSQL.contains(SINGLE_SPACE + LC_WHERE + SINGLE_SPACE) ?
    					  StringUtils.substringBetween(updatedSQL, SINGLE_SPACE + LC_FROM + SINGLE_SPACE, 
    							  					   SINGLE_SPACE + LC_WHERE + SINGLE_SPACE).replace(DOT, "") +
    					  SINGLE_SPACE + LC_WHERE + SINGLE_SPACE +
    					  StringUtils.substringAfter(updatedSQL, SINGLE_SPACE + LC_WHERE + SINGLE_SPACE) : 
    					  StringUtils.substringAfter(updatedSQL, SINGLE_SPACE + LC_FROM + SINGLE_SPACE).replace(DOT, ""));
    	}
    	
    	// Convert is/is not in HQL to =/!= in SQL  	
    	if (updatedSQL.contains(SINGLE_SPACE + LC_WHERE + SINGLE_SPACE) &&
    		StringUtils.substringAfter(updatedSQL, SINGLE_SPACE + LC_WHERE + SINGLE_SPACE).
    		 contains(SINGLE_SPACE + LC_IS + SINGLE_SPACE)) {
    		updatedSQL = StringUtils.substringBefore(updatedSQL, SINGLE_SPACE + LC_WHERE + SINGLE_SPACE) + 
    					 SINGLE_SPACE + LC_WHERE + SINGLE_SPACE +
    					 StringUtils.substringAfter(updatedSQL, SINGLE_SPACE + LC_WHERE + SINGLE_SPACE).replace(LC_IS_NOT_HQL, LC_IS_NOT_SQL);
    		updatedSQL = StringUtils.substringBefore(updatedSQL, SINGLE_SPACE + LC_WHERE + SINGLE_SPACE) +
    				 	 SINGLE_SPACE + LC_WHERE + SINGLE_SPACE +
    					 StringUtils.substringAfter(updatedSQL, SINGLE_SPACE + LC_WHERE + SINGLE_SPACE).replace(LC_IS_HQL, LC_IS_SQL);
    	}
    	
    	// Convert <field>.order to <field>.myorder as order is part of SQL keyword "order by"
    	if (updatedSQL.contains(DOT_LC_ORDER)) {
    		updatedSQL = updatedSQL.replace(DOT_LC_ORDER, DOT_LC_MYORDER);
    	}
    	
    	// Convert order by order to order by myorder as order is part of SQL keyword "order by"
    	if (StringUtils.substringAfter(updatedSQL, SPACE_LC_ORDER_BY_SPACE).contains(LC_ORDER)) {
    		updatedSQL = StringUtils.substringBefore(updatedSQL, SPACE_LC_ORDER_BY_SPACE) +
    					SPACE_LC_ORDER_BY_SPACE + 
    					StringUtils.substringAfter(updatedSQL, SPACE_LC_ORDER_BY_SPACE).
    					replace(LC_ORDER, LC_MYORDER);
    	}
    	
    	// Replace fields/columns named "group" with "mygroup" as it conflicts with SQL group by clause
    	if (updatedSQL.contains(LC_GROUP)) {
    		// mask group by
    		updatedSQL = updatedSQL.replace(LC_SPACE_GROUP_SPACE_BY_SPACE, LC_MASK_SPACE_GROUP_SPACE_BY_SPACE);
    		updatedSQL = updatedSQL.replace(LC_GROUP, LC_MYGROUP);
    		// unmask group by
    		updatedSQL = updatedSQL.replace(LC_MASK_SPACE_GROUP_SPACE_BY_SPACE, LC_SPACE_GROUP_SPACE_BY_SPACE);
    	}
    	
    	// remove the binary keyword from query, which jsqlparser didn't like.
    	updatedSQL = updatedSQL.replace("binary", "");
    	
    	Log.log.trace("Updated : [" + updatedSQL + "]");
    	
    	return updatedSQL;
    }
    
    public static String getSafeSQL(String sql) throws Exception
    {
    	return getSafeSQL(sql, null);
    }
    
    /**
     * Encodes the SQL using encodes using OWASP ESAPI library to prevent SQL injection.
     *
     * @param sql
     * @param databaseType
     * @return
     * @throws Exception 
     */
    public static String getSafeSQL(String sql, String databaseType) throws Exception
    {
    	String safeSQL = null;
        
    	if (StringUtils.isBlank(sql)) {
    		return sql;
    	}
    	
    	String txedSQL = transformHQLToSQL(sql, databaseType);
    	
        CCJSqlParserManager jsqlParseMgr = new CCJSqlParserManager();
        
        try
        {
            String jsqlParserSafeSQL = null;
            Statement jsqlStmt = jsqlParseMgr.parse(new StringReader(txedSQL));
            
            StringBuilder sb = new StringBuilder();
            ResolveDeParserExpressionVisitor rdpev = new ResolveDeParserExpressionVisitor(sb);
            ResolveDeParserSelectVisitor rdpsv = new ResolveDeParserSelectVisitor(sb);
            
            if (jsqlStmt instanceof CreateTable)
            {
                Log.log.trace("Deparsing jsqlStmt[" + jsqlStmt + "] instanceof [" + CreateTable.class.getName() + "]");
                CreateTableDeParser deparser = new CreateTableDeParser(sb);
                deparser.deParse((CreateTable) jsqlStmt);
                jsqlParserSafeSQL = deparser.getBuffer().toString();
            }
            else if (jsqlStmt instanceof Delete)
            {
                Log.log.trace("Deparsing jsqlStmt[" + jsqlStmt + "] instanceof [" + Delete.class.getName() + "]");
                DeleteDeParser deparser = new DeleteDeParser(rdpev, sb);
                deparser.deParse((Delete) jsqlStmt);
                jsqlParserSafeSQL = deparser.getBuffer().toString();
            }
            else if (jsqlStmt instanceof Drop)
            {
                Log.log.trace("Deparsing jsqlStmt[" + jsqlStmt + "] instanceof [" + Drop.class.getName() + "]");
                jsqlParserSafeSQL = ((Drop)jsqlStmt).toString();
            }
            else if (jsqlStmt instanceof Insert)
            {
                Log.log.debug("Deparsing jsqlStmt[" + jsqlStmt + "] instanceof [" + Insert.class.getName() + "]");
                InsertDeParser deparser = new InsertDeParser(rdpev, rdpsv, sb);
                deparser.deParse((Insert) jsqlStmt);
                jsqlParserSafeSQL = deparser.getBuffer().toString();
            }
            else if (jsqlStmt instanceof Replace)
            {
                Log.log.trace("Deparsing jsqlStmt[" + jsqlStmt + "] instanceof [" + Replace.class.getName() + "]");
                ReplaceDeParser deparser = new ReplaceDeParser(rdpev, rdpsv, sb);
                deparser.deParse((Replace) jsqlStmt);
                jsqlParserSafeSQL = deparser.getBuffer().toString();
            }
            else if (jsqlStmt instanceof Select)
            {
                Log.log.trace("Deparsing jsqlStmt[" + jsqlStmt + "] instanceof [" + Select.class.getName() + "]");
                jsqlParserSafeSQL = ((Select)jsqlStmt).toString();
                Log.log.trace("Original[" + sql + "], Safe[" + jsqlParserSafeSQL + "]");     
            }
            else if (jsqlStmt instanceof Truncate)
            {
                Log.log.debug("Deparsing jsqlStmt[" + jsqlStmt + "] instanceof [" + Truncate.class.getName() + "]");
                jsqlParserSafeSQL = ((Truncate)jsqlStmt).toString();
            }
            else if (jsqlStmt instanceof Update)
            {
                Log.log.debug("Deparsing jsqlStmt[" + jsqlStmt + "] instanceof [" + Update.class.getName() + "]");
                UpdateDeParser deparser = new UpdateDeParser(rdpev, rdpsv, sb);
                deparser.deParse((Update) jsqlStmt);
                jsqlParserSafeSQL = deparser.getBuffer().toString();
            }
            
            if (StringUtils.isBlank(safeSQL) && StringUtils.isNotBlank(jsqlParserSafeSQL))
            {
                if (jsqlParserSafeSQL.startsWith("net.sf.jsqlparser.statement"))
                {
                    Log.log.warn("Failed to deparse [" + jsqlParserSafeSQL + "]");
                    throw new Exception("Failed to deparse [" + jsqlParserSafeSQL + "]");
                }
                
                safeSQL = jsqlParserSafeSQL;
            }
        }
        catch (Exception e)
        {
            String msg = "Foiled an attempt to execute unsafe SQL[" + sql + ", " + txedSQL + "]. " + e.getLocalizedMessage();
            Log.log.warn(msg);
            throw new /*Runtime*/Exception(msg, e);
        }
        
        return txedSQL.equals(sql) ? safeSQL : sql;
    }

    public static String prepareQueryString(List<String> strList)
    {
        return prepareQueryString(strList, null);
    }
    
    /**
     * THis is to convert a List to a comma sepated string which can be used in
     * the IN clause of a query eg. {A, B, C} ==> 'A','B','C'
     *
     * @param strList
     * @return
     */
    public static String prepareQueryString(List<String> strList, String dbType)
    {
        StringBuffer qry = new StringBuffer();

        if(StringUtils.isNotBlank(dbType) && !StringUtils.isAllUpperCase(dbType))
        {
            dbType = dbType.toUpperCase();
        }

        for (int x = 0; x < strList.size(); x++)
        {
            String sys_id = strList.get(x);
            if (x == (strList.size() - 1))
            {
                qry.append("'").append(/*getSafeSQL(*/sys_id.trim()/*, dbType)*/).append("'");// this is the
                                                                  // last one
            }
            else
            {
                qry.append("'").append(/*getSafeSQL(*/sys_id.trim()/*, dbType)*/).append("'").append(", ");
            }
        }
        return qry.toString();
    } // prepareQueryString
    
    /**
     * 
     * @param csv - 'str1','str2','str3'
     * @return
     */
    public static Set<String> convertQueryStringToSet(String csv)
    {
        Set<String> result = new HashSet<String>();
        
        if(StringUtils.isNotBlank(csv))
        {
            String[] arr = csv.split(",");
            for(String str : arr)
            {
                int indexOfQuote = str.indexOf("'");
                int lastIndexOfQuote = str.lastIndexOf("'");
                
                if (indexOfQuote >= 0 && lastIndexOfQuote >= 2 && (lastIndexOfQuote - indexOfQuote) > 0)
                {
                    String sysId = str.substring(indexOfQuote + 1, lastIndexOfQuote);
                    
                    if (sysId.trim().length() > 0)
                    {
                        result.add(sysId.trim());
                    }
                }
            }
        }
        
        return result;
        
    }

    /**
     * list of integer to string query used for the IN clause {1, 2, 3} ==>
     * 1,2,3
     *
     * @param strList
     * @return
     */
    public static String prepareQueryInteger(List<Integer> strList)
    {
        StringBuffer qry = new StringBuffer();

        for (int x = 0; x < strList.size(); x++)
        {
            Integer sys_id = strList.get(x);
            if (x == (strList.size() - 1))
            {
                qry.append(sys_id);// this is the last one
            }
            else
            {
                qry.append(sys_id).append(", ");
            }
        }
        return qry.toString();
    } // prepareQueryString

    public static String prepareQueryString(String[] strArray)
    {
        return prepareQueryString(Arrays.asList(strArray), null);
    }
    
    /**
     * THis is to convert a String Array to a comma sepated string which can be
     * used in the IN clause of a query eg. {A, B, C} ==> 'A','B','C'
     *
     * @param strArray
     * @return
     */
    public static String prepareQueryString(String[] strArray, String dbType)
    {
    	return prepareQueryString(Arrays.asList(strArray), dbType);
    } // prepareQueryString
    
    /**
     * THis is to convert a List to a comma sepated string which can be used in
     * the IN clause of a query eg. {A, B, C} ==> ?, ?, ?
     *
     * @param strList
     * @return
     */
    public static String prepareParametrerizedQueryString(List<String> strList)
    {
        StringBuffer qry = new StringBuffer();

        for (int x = 0; x < strList.size(); x++)
        {
            if (x == (strList.size() - 1))
            {
                qry.append("?");// this is the last one
            }
            else
            {
                qry.append("?, ");
            }
        }
        
        return qry.toString();
    }
    
    static public class ResolveDeParserExpressionVisitor implements ExpressionVisitor
    {
        private StringBuilder sb;
        
        public ResolveDeParserExpressionVisitor(StringBuilder sb)
        {
            this.sb = sb;
        }

        @Override
        public void visit(NullValue arg0)
        {
            sb.append(arg0);
        }

        @Override
        public void visit(Function arg0)
        {
            sb.append(arg0);
        }

        @Override
        public void visit(JdbcParameter arg0)
        {
            sb.append(arg0);
        }

        @Override
        public void visit(DoubleValue arg0)
        {
            sb.append(arg0);
        }

        @Override
        public void visit(LongValue arg0)
        {
            sb.append(arg0);
        }

        @Override
        public void visit(DateValue arg0)
        {
            sb.append(arg0);
        }

        @Override
        public void visit(TimeValue arg0)
        {
            sb.append(arg0);
        }

        @Override
        public void visit(TimestampValue arg0)
        {
            sb.append(arg0);
        }

        @Override
        public void visit(Parenthesis arg0)
        {
            sb.append(arg0);
        }

        @Override
        public void visit(StringValue arg0)
        {
            sb.append(arg0);
        }

        @Override
        public void visit(Addition arg0)
        {
            sb.append(arg0);
        }

        @Override
        public void visit(Division arg0)
        {
            sb.append(arg0);
        }

        @Override
        public void visit(Multiplication arg0)
        {
            sb.append(arg0);
        }

        @Override
        public void visit(Subtraction arg0)
        {
            sb.append(arg0);
        }

        @Override
        public void visit(AndExpression arg0)
        {
            sb.append(arg0);
        }

        @Override
        public void visit(OrExpression arg0)
        {
            sb.append(arg0);
        }

        @Override
        public void visit(Between arg0)
        {
            sb.append(arg0);
        }

        @Override
        public void visit(EqualsTo arg0)
        {
            sb.append(arg0);
        }

        @Override
        public void visit(GreaterThan arg0)
        {
            sb.append(arg0);
        }

        @Override
        public void visit(GreaterThanEquals arg0)
        {
            sb.append(arg0);
        }

        @Override
        public void visit(InExpression arg0)
        {
            sb.append(arg0);
        }

        @Override
        public void visit(IsNullExpression arg0)
        {
            sb.append(arg0);
        }

        @Override
        public void visit(LikeExpression arg0)
        {
            sb.append(arg0);
        }

        @Override
        public void visit(MinorThan arg0)
        {
            sb.append(arg0);
        }

        @Override
        public void visit(MinorThanEquals arg0)
        {
            sb.append(arg0);
        }

        @Override
        public void visit(NotEqualsTo arg0)
        {
            sb.append(arg0);
        }

        @Override
        public void visit(Column arg0)
        {
            sb.append(arg0);
        }

        @Override
        public void visit(SubSelect arg0)
        {
            sb.append(arg0);
        }

        @Override
        public void visit(CaseExpression arg0)
        {
            sb.append(arg0);
        }

        @Override
        public void visit(WhenClause arg0)
        {
            sb.append(arg0);
        }

        @Override
        public void visit(ExistsExpression arg0)
        {
            sb.append(arg0);
        }

        @Override
        public void visit(AllComparisonExpression arg0)
        {
            sb.append(arg0);
        }

        @Override
        public void visit(AnyComparisonExpression arg0)
        {
            sb.append(arg0);
        }

        @Override
        public void visit(Concat arg0)
        {
            sb.append(arg0);          
        }

        @Override
        public void visit(Matches arg0)
        {
            sb.append(arg0);
        }

        @Override
        public void visit(BitwiseAnd arg0)
        {
            sb.append(arg0);
        }

        @Override
        public void visit(BitwiseOr arg0)
        {
            sb.append(arg0);
        }

        @Override
        public void visit(BitwiseXor arg0)
        {
            sb.append(arg0);            
        }

        @Override
        public void visit(SignedExpression arg0)
        {
            sb.append(arg0);
        }

        @Override
        public void visit(JdbcNamedParameter arg0)
        {
            sb.append(arg0);
        }

        @Override
        public void visit(CastExpression arg0)
        {
            sb.append(arg0);
        }

        @Override
        public void visit(Modulo arg0)
        {
            sb.append(arg0);
        }

        @Override
        public void visit(AnalyticExpression arg0)
        {
            sb.append(arg0);
        }

        @Override
        public void visit(ExtractExpression arg0)
        {
            sb.append(arg0);
        }

        @Override
        public void visit(IntervalExpression arg0)
        {
            sb.append(arg0);
        }

        @Override
        public void visit(OracleHierarchicalExpression arg0)
        {
            sb.append(arg0);
        }

        @Override
        public void visit(RegExpMatchOperator arg0)
        {
            sb.append(arg0);
        }

		@Override
		public void visit(BitwiseRightShift arg0) {
			sb.append(arg0);
		}

		@Override
		public void visit(BitwiseLeftShift arg0) {
			sb.append(arg0);
		}

		@Override
		public void visit(HexValue arg0) {
			sb.append(arg0);
		}

		@Override
		public void visit(JsonExpression arg0) {
			sb.append(arg0);
		}

		@Override
		public void visit(JsonOperator arg0) {
			sb.append(arg0);
		}

		@Override
		public void visit(RegExpMySQLOperator arg0) {
			sb.append(arg0);
		}

		@Override
		public void visit(UserVariable arg0) {
			sb.append(arg0);
		}

		@Override
		public void visit(NumericBind arg0) {
			sb.append(arg0);
		}

		@Override
		public void visit(KeepExpression arg0) {
			sb.append(arg0);
		}

		@Override
		public void visit(MySQLGroupConcat arg0) {
			sb.append(arg0);
		}

		@Override
		public void visit(ValueListExpression arg0) {
			sb.append(arg0);
		}

		@Override
		public void visit(RowConstructor arg0) {
			sb.append(arg0);
		}

		@Override
		public void visit(OracleHint arg0) {
			sb.append(arg0);
		}

		@Override
		public void visit(TimeKeyExpression arg0) {
			sb.append(arg0);
		}

		@Override
		public void visit(DateTimeLiteralExpression arg0) {
			sb.append(arg0);
		}

		@Override
		public void visit(NotExpression arg0) {
			sb.append(arg0);
		}        
    }
    
    static public class ResolveDeParserSelectVisitor implements SelectVisitor
    {
        private StringBuilder sb;
        
        public ResolveDeParserSelectVisitor(StringBuilder sb)
        {
            this.sb = sb;
        }

        @Override
        public void visit(PlainSelect arg0)
        {
            sb.append(arg0);
        }

        @Override
        public void visit(SetOperationList arg0)
        {
            sb.append(arg0);
        }

        @Override
        public void visit(WithItem arg0)
        {
            sb.append(arg0);
        }
    }
    /*    
    public static void main(String[] args)
    {
        String csv = "'str1','str2','str3'";
        Set<String> test = convertQueryStringToSet(csv);   
        System.out.println(test);
    }*/
    
    public static Clob StringToClob(String str)
    {
        SerialClob clob = null;
        
        if (StringUtils.isNotBlank(str))
        {
            try
            {
                clob = new SerialClob(str.toCharArray());
            }
            catch (Exception e)
            {
                Log.log.error(e.getMessage(), e);
            }
        }
        
        return clob;
    }
    
    public static String ClobToString(Clob clob)
    {
        String str = null;
        
        if (clob != null)
        {
            try
            {
                if (clob.length() > 0)
                {
                    str = clob.getSubString(1L, (int)(clob.length() - 1));
                }
            }
            catch (Exception e)
            {
                Log.log.error(e.getMessage(), e);
            }
        }
        
        return str;
    }
} // SQLUtils