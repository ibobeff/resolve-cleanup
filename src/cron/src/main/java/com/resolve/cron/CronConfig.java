/******************************************************************************
* (C) Copyright 2014
*
* Resolve Systems, LLC
*
* All rights reserved
* This software is distributed under license and may not be used, copied,
* modified, or distributed without the express written permission of
* Resolve Systems, LLC.
*
******************************************************************************/

package com.resolve.cron;

public class CronConfig
{
    String name                = "CRON";             // cron instance name
    String filename            = "config/cron.ini";  // cronstore file
    int threadCount            = 5;                 // maximum number of threads allocated
    int threadPriority         = 5;      			 // execution thread priority
    int misfireThreshold       = 60;     			 // seconds late from expected execution time
    boolean notifyOn           = false;  			 // include notification listener
    
    public String getName()
    {
        return name;
    }
    
    public void setName(String name)
    {
        this.name = name;
    }

    public int getMisfireThreshold()
    {
        return misfireThreshold;
    }

    public void setMisfireThreshold(int misfireThreshold)
    {
        this.misfireThreshold = misfireThreshold;
    }
    
    public int getThreadCount()
    {
        return threadCount;
    }
    
    public void setThreadCount(int threadCount)
    {
        this.threadCount = threadCount;
    }
    
    public int getThreadPriority()
    {
        return threadPriority;
    }
    
    public void setThreadPriority(int threadPriority)
    {
        this.threadPriority = threadPriority;
    }

    public boolean isNotifyOn()
    {
        return notifyOn;
    }

    public void setNotifyOn(boolean notifyOn)
    {
        this.notifyOn = notifyOn;
    }

    public String getFilename()
    {
        return filename;
    }

    public void setFilename(String filename)
    {
        this.filename = filename;
    }
    
} // CronConfig
