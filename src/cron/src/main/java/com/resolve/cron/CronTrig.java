/******************************************************************************
* (C) Copyright 2014
*
* Resolve Systems, LLC
*
* All rights reserved
* This software is distributed under license and may not be used, copied,
* modified, or distributed without the express written permission of
* Resolve Systems, LLC.
*
******************************************************************************/

package com.resolve.cron;

import java.util.Date;

import org.quartz.Trigger;

import com.resolve.util.Constants;
import com.resolve.util.StringUtils;

public class CronTrig extends CronBase
{
    final static String REPEAT     = "REPEAT";
    final static String INTERVAL   = "INTERVAL";
    final static String START      = "START";
    final static String END        = "END";
    final static String EXPR       = "EXPR";
    
    String type;
    String name;
    String group;
    int repeat;
    long interval;
    Date start;
    Date end;
    String expression;
    Trigger trigger;
    
    public CronTrig()
    {
    } // CronTrig
    
    public CronTrig(String name)
    {
        this.name = name;
        this.group = "ALL";
        this.repeat = -1;        // -1 repeat indefinitely, 0 delayed single execution
        this.interval = 0;       // secs (repeat interval is reset to 1 by CronStore as it cannot be 0)
    } // CronTrig
    
    // create trigger from trigger definition string - definition: type:repeat=value&interval=value&...
    public CronTrig(String trigname, String definition)
    {
        this.name = trigname;
        this.group = "ALL";
        
        String[] typeParam = definition.split(":");
        if (typeParam.length == 2)
        {
            this.type = typeParam[0];
            
            String[] namevals = typeParam[1].split(StringUtils.FIELDSEPARATOR_REGEX);
            for (int i=0; i < namevals.length; i++)
            {
                String[] nameval = namevals[i].split(StringUtils.VALUESEPARATOR_REGEX);
                if (nameval.length == 2)
                {
                    String name = nameval[0];
                    String value = nameval[1];
                        
                    if (name.equals(REPEAT))
                    {
                        this.repeat = Integer.parseInt(value);
                    }
                    else if (name.equals(INTERVAL))
                    {
                        this.interval = Long.parseLong(value);
                    }
                    else if (name.equals(EXPR))
                    {
                        if (value.equals("null"))
                        {
                            value = null;
                        }
                        this.expression = value;
                    }
                }
            }
        }
        
    } // CronTrig
    
    public String toString()
    {
        String result = "";
        result += "CronTrig name: "+name+"\n";
        result += "  type: "+type+"\n";
        if (type != null && type.equals(Constants.CRON_TRIGGER_SIMPLE))
        {
            result += "  repeat: "+repeat+"\n";
            result += "  interval: "+(interval / 1000) +"\n";
            result += "  start: "+start+"\n";
            result += "  end: "+end+"\n";
        }
        else if (type != null && type.equals(Constants.CRON_TRIGGER_CRON))
        {
            result += "  expression: "+expression+"\n";
        }
        
        return result;
    } // toString
    
    // definition: type:repeat=value&interval=value&...
    public String getDefinition()
    {
        return this.type+":"+INTERVAL+StringUtils.VALUESEPARATOR+this.interval+StringUtils.FIELDSEPARATOR+REPEAT+StringUtils.VALUESEPARATOR+this.repeat+StringUtils.FIELDSEPARATOR+EXPR+StringUtils.VALUESEPARATOR+this.expression;
    } // getDefinition
    
    public boolean hasTrigger()
    {
        return trigger != null;
    } // hasTrigger
    
    public Date getEnd()
    {
        return end;
    }
    public void setEnd(Date end)
    {
        this.end = end;
    }
    public String getExpression()
    {
        String result = expression;
        if (type.equals(Constants.CRON_TRIGGER_SIMPLE))
        {
            result = "";
        }
        return result;
    }
    public void setExpression(String expression)
    {
        this.expression = expression;
    }
    public String getGroup()
    {
        return group;
    }
    public void setGroup(String group)
    {
        this.group = group;
    }
    public long getInterval()
    {
        long result = interval;
        if (type.equals(Constants.CRON_TRIGGER_CRON))
        {
            result = 0;
        }
        return result;
    }
    public void setInterval(long interval)
    {
        this.interval = interval * 1000;
    }
    public void setIntervalMSecs(long interval)
    {
        this.interval = interval;
    }
    public String getName()
    {
        return name.toUpperCase();
    }
    public void setName(String name)
    {
        this.name = name.toUpperCase();
    }
    public int getRepeat()
    {
        int result = repeat;
        if (type.equals(Constants.CRON_TRIGGER_CRON))
        {
            result = 0;
        }
        return result;
    }
    public void setRepeat(int repeat)
    {
        this.repeat = repeat;
    }
    public Date getStart()
    {
        return start;
    }
    public void setStart(Date start)
    {
        this.start = start;
    }
    public String getType()
    {
        return type;
    }
    public void setType(String type)
    {
        this.type = type;
    }
    public Trigger getTrigger()
    {
        return trigger;
    }
    public void setTrigger(Trigger trigger)
    {
        this.trigger = trigger;
    }

} // CronTrig
