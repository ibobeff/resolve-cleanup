/******************************************************************************
* (C) Copyright 2014
*
* Resolve Systems, LLC
*
* All rights reserved
* This software is distributed under license and may not be used, copied,
* modified, or distributed without the express written permission of
* Resolve Systems, LLC.
*
******************************************************************************/

package com.resolve.cron;

import com.resolve.util.ERR;
import com.resolve.util.Log;

public abstract class CronScheduler
{
    public CronStore cronstore;
        
    public CronScheduler(CronConfig config)
    {
        try
        {
            // initialize CronStore
            cronstore = new CronStore(config);
        }
        catch (Exception e)
        {
            Log.alert(ERR.E30001, e);
        }
    } // CronScheduler
        
    public void start()
    {
        try
        {
            // load stored tasks
            CronStore.load();
            
            // load default tasks
            defaultTasks();
                
            // run startup tasks
            startupTasks();
            
            // start CronStore
            CronStore.start();
        }
        catch (Exception e)
        {
            Log.alert(ERR.E30001, e);
        }
    } // start
            
    public void stop()
    {
        try
        {
            // stop CronStore
            CronStore.stop();
        }
        catch (Exception e)
        {
            Log.log.error("Failed to stop CronScheduler: "+e.getMessage(), e);
        }
    } // stop
    
    public void save()
    {
        CronStore.save();
    } // save
    
    protected void defaultTasks() throws Exception
    {
    } // defaultTasks
        
    protected void startupTasks() throws Exception
    {
    } // startupTasks
    
} // CronSchedule