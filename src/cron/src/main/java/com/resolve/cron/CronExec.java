/******************************************************************************
* (C) Copyright 2014
*
* Resolve Systems, LLC
*
* All rights reserved
* This software is distributed under license and may not be used, copied,
* modified, or distributed without the express written permission of
* Resolve Systems, LLC.
*
******************************************************************************/

package com.resolve.cron;

import org.quartz.JobDataMap;
import org.quartz.StatefulJob;
import org.quartz.JobExecutionContext;
import org.quartz.JobExecutionException;

import com.resolve.util.Log;

public class CronExec implements StatefulJob
{
    public JobDataMap data;
    
    public CronExec()
    {
    } // CronExec
    
    public void execute(JobExecutionContext context) throws JobExecutionException
    {
        String jobname = context.getJobDetail().getName();
       
        if (!jobname.equals("SPOOL"))
        {
	        Log.log.debug("Executing scheduled task: "+jobname);
        }
    } // execute
    
    public String getJobData(String name)
    {
        String result = (String)data.get(name.toUpperCase());
        if (result == null)
        {
            result = "";
        }
        return result;
    } // getJobData

} // CronExec