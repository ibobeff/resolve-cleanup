/******************************************************************************
* (C) Copyright 2014
*
* Resolve Systems, LLC
*
* All rights reserved
* This software is distributed under license and may not be used, copied,
* modified, or distributed without the express written permission of
* Resolve Systems, LLC.
*
******************************************************************************/

package com.resolve.cron;

import org.quartz.JobDataMap;
import org.quartz.JobExecutionContext;
import org.quartz.JobExecutionException;
import com.resolve.util.Log;

public class Echo extends CronExec 
{
    public Echo()
    {
    } // Echo
    
    public void execute(JobExecutionContext context) throws JobExecutionException
    {
        super.execute(context);
        
        JobDataMap data = context.getJobDetail().getJobDataMap();
        if (data != null)
        {
            String params = (String)data.get("PARAMS");
            Log.log.debug("Echo params: "+params);
        }
    } // execute

} // Echo

