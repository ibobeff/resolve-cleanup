/******************************************************************************
* (C) Copyright 2014
*
* Resolve Systems, LLC
*
* All rights reserved
* This software is distributed under license and may not be used, copied,
* modified, or distributed without the express written permission of
* Resolve Systems, LLC.
*
******************************************************************************/

package com.resolve.cron;

import org.quartz.JobDetail;
import org.quartz.JobDataMap;
import org.quartz.JobExecutionContext;
import org.quartz.JobExecutionException;

import com.resolve.util.Log;

public class Kill extends CronExec
{
    public Kill()
    {
    } // Kill
    
    public void execute(JobExecutionContext context) throws JobExecutionException
    {
        super.execute(context);
        
        /*
        JobDetail job = context.getJobDetail();
        if (job != null)
        {
            try
            {
    	        JobDataMap data     = job.getJobDataMap();
    	        
                String type         = (String)data.get("TYPE");
    	        String killname     = (String)data.get("KILLNAME");
    	        String jobname      = (String)data.get("JOBNAME");
    	        
	            // destroy process
                if (type.equals("PROCESS"))
                {
        	        Process process = (Process)data.get("OBJECT");
                    if (process != null)
                    {
                        Log.log.debug("Aborting process");
        	            process.destroy();
                    }
                }
                else if (type.equals("THREAD"))
                {
        	        Thread thread = (Thread)data.get("OBJECT");
                    if (thread != null)
                    {
                        Log.log.debug("Aborting thread id: "+thread.getId()+" name: "+thread.getName());
        	            thread.stop(new Exception());
                    }
                }
                    
                // remove task and job from scheduler
                if (jobname != null)
                {
                    CronStore.removeTask(jobname);
                    CronStore.removeJob(jobname);
                }
    	            
		        // remove kill job and trigger from scheduler
                if (killname != null)
                {
			        CronStore.removeJob(killname);
			        CronStore.removeTrigger(killname);
                }
                
                // indicate that the job was killed
                CronStore.setJobKilled(jobname);
            }
            catch (Exception e)
            {
                Log.log.error("Failed to execute kill task: "+e.getMessage(), e);
            }
        }
        */
    } // execute

} // Kill

