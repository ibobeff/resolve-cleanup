/******************************************************************************
* (C) Copyright 2014
*
* Resolve Systems, LLC
*
* All rights reserved
* This software is distributed under license and may not be used, copied,
* modified, or distributed without the express written permission of
* Resolve Systems, LLC.
*
******************************************************************************/

package com.resolve.cron;

import java.util.Date;
import java.util.HashMap;
import java.util.Map;

import com.resolve.rsbase.MainBase;
import com.resolve.util.Constants;
import com.resolve.util.Log;

/**
 * This is a convenience class to manage various operations for cron jobs in Resolve. This API could be called from any ActionTask's 
 * contents, assessor and so on. It can also be invoked from any groovy scripts in Resolve ( for example, script from any gateway filter).
 */
public class CronAPI
{
    /**
     * Schedules a runbook, Groovy script or JMS message to be triggered based on a
     * cron expression. This inserts the cron record into resolve_cron and
     * load into the scheduler.
     * 
     * <pre>
     * {@code
     * import com.resolve.cron.CronAPI;
     * //...
     * //...
     * String taskName = "MyTask"; 
     * String module = "RSQA"; 
     * String runbook = "RSQA.Random Number"; 
     * Map<String, String> cronParams = new HashMap<String, String>();
     * //put any required data in cronParams here.
     *         
     * String expression = "0 0 12 * * ?"; //fired at 12pm (noon) every day." 
     * Date startTime = new Date(2012, 1, 1); //start on 01/01/2012 
     * Date endTime = new Date(2012, 12, 31); //end on 12/31/2012
     * String userid = "resolve"; 
     * Boolean active = Boolean.TRUE;
     *         
     * boolean result = CronAPI.scheduleTask(taskName, module, runbook, cronParams, expression, startTime, endTime, userid, active);
     * }
     * </pre>
     * 
     * @param taskName
     *            - unique name for the scheduled cron task
     * @param module
     *            - optional namespace, useful for module export
     * @param runbook
     *            - name of the runbook wiki to be executed. Also supports
     *            &lt;ESBADDR&gt;#Class.Method to send JMS message and
     *            MService.&lt;path&gt;.groovy script to be executed
     * @param cronParams
     * @param expression
     *            like "0 0 12 * * ?" which is fired at 12pm (noon) every day.
     * @param startTime
     * @param endTime
     * @param userid
     * @param active
     */
    public static boolean scheduleTask(String taskName, String module, String runbook, Map<String, String> cronParams, String expression, Date startTime, Date endTime, String userid, Boolean active)
    {
        Map<String, Object> params = new HashMap<String, Object>();
        params.put(Constants.CRON_NAME, taskName);
        params.put(Constants.CRON_MODULE, module);
        params.put(Constants.CRON_RUNBOOK, runbook);
        params.put(Constants.CRON_PARAMS, cronParams);
        params.put(Constants.CRON_EXPRESSION, expression);
        params.put(Constants.CRON_START_TIME, startTime);
        params.put(Constants.CRON_END_TIME, endTime);
        params.put(Constants.CRON_USER_ID, userid);
        params.put(Constants.CRON_ACTIVE, active);
        
        //Submit to the ESB so it can be processed by RSControl
        boolean isSuccess = MainBase.esb.sendInternalMessage(Constants.ESB_NAME_RSCONTROL, "MCron.scheduleTask", params);
        if(isSuccess)
        {
            Log.log.debug("Task sent to the RSControl for scheduling.");
        }
        else
        {
            Log.log.warn("Task failed transmission to RSControl for scheduling.");
        }
        return isSuccess;
    }

    /**
     * Overloaded method which schedules a runbook, Groovy script or JMS message
     * to be triggered based on a cron expression.
     * 
     * <pre>
     * {@code
     * import com.resolve.cron.CronAPI;
     * //...
     * //...
     * String taskName = "MyTask"; 
     * String module = "RSQA"; 
     * String runbook = "RSQA.Random Number"; 
     * Map<String, String> cronParams = new HashMap<String, String>();
     * //put any required data in cronParams here.
     *   
     * String expression = "0 0 12 * * ?"; //fired at 12pm (noon) every day." 
     * String userid = "resolve"; 
     * Boolean active = Boolean.TRUE;
     *   
     * boolean result = CronAPI.scheduleTask(taskName, module, runbook, cronParams, expression, userid, active);
     * }
     * </pre>
     * 
     * @param taskName
     *            - unique name for the scheduled cron task
     * @param module
     *            - optional namespace, useful for module export
     * @param runbook
     *            - name of the runbook wiki to be executed. Also supports
     *            &lt;ESBADDR&gt;#Class.Method to send JMS message and
     *            MService.&lt;path&gt;.groovy script to be executed
     * @param cronParams
     * @param expression
     *            like "0 0 12 * * ?" which is fired at 12pm (noon) every day.
     * @param userid
     * @param active
     * @return
     */
    public static boolean scheduleTask(String taskName, String module, String runbook, Map<String, String> cronParams, String expression, String userid, Boolean active)
    {
        return scheduleTask(taskName, module, runbook, cronParams, expression, null, null, userid, active.booleanValue());
    }

    /**
     * Overloaded method which schedules a runbook, Groovy script or JMS message
     * to be triggered based on a cron expression.
     * 
     * <pre>
     * {@code
     * import com.resolve.cron.CronAPI;
     * //...
     * //...
     * String taskName = "MyTask"; 
     * String module = "RSQA"; 
     * String runbook = "RSQA.Random Number"; 
     * Map<String, String> cronParams = new HashMap<String, String>();
     * //put any required data in cronParams here.
     * 
     * String expression = "0 0 12 * * ?"; //fired at 12pm (noon) every day." 
     * String userid = "resolve"; 
     * Boolean active = Boolean.TRUE;
     * boolean result = CronAPI.scheduleTask(taskName, runbook, cronParams, expression, userid, active);
     * }
     * </pre>
     * 
     * @param taskName
     *            - unique name for the scheduled cron task
     * @param runbook
     *            - name of the runbook wiki to be executed. Also supports
     *            &lt;ESBADDR&gt;#Class.Method to send JMS message and
     *            MService.&lt;path&gt;.groovy script to be executed
     * @param cronParams
     * @param expression
     *            like "0 0 12 * * ?" which is fired at 12pm (noon) every day.
     * @param userid
     * @param active
     * @return
     */
    public static boolean scheduleTask(String taskName, String runbook, Map<String, String> cronParams, String expression, String userid, Boolean active)
    {
        return scheduleTask(taskName, null, runbook, cronParams, expression, null, null, userid, active.booleanValue());
    }

    /**
     * Overloaded method which schedules a runbook, Groovy script or JMS message
     * to be triggered based on a cron expression.
     * 
     * <pre>
     * {@code
     * import com.resolve.cron.CronAPI;
     * //...
     * //...
     * String taskName = "MyTask"; 
     * String module = "RSQA"; 
     * String runbook = "RSQA.Random Number"; 
     * String expression = "0 0 12 * * ?"; //fired at 12pm (noon) every day." 
     * String userid = "resolve"; 
     * Boolean active = Boolean.TRUE;
     * boolean result = CronAPI.scheduleTask(taskName, module, runbook, expression, userid, active);
     * }
     * </pre>
     * 
     * @param taskName
     *            - unique name for the scheduled cron task
     * @param module
     *            - optional namespace, useful for module export
     * @param runbook
     *            - name of the runbook wiki to be executed. Also supports
     *            &lt;ESBADDR&gt;#Class.Method to send JMS message and
     *            MService.&lt;path&gt;.groovy script to be executed
     * @param expression
     *            like "0 0 12 * * ?" which is fired at 12pm (noon) every day.
     * @param userid
     * @param active
     * @return
     */
    public static boolean scheduleTask(String taskName, String module, String runbook, String expression, String userid, Boolean active)
    {
        return scheduleTask(taskName, module, runbook, null, expression, null, null, userid, active.booleanValue());
    }

    /**
     * Overloaded method which schedules a runbook, Groovy script or JMS message
     * to be triggered based on a cron expression.
     * 
     * <pre>
     * {@code
     * import com.resolve.cron.CronAPI;
     * //...
     * //...
     * String taskName = "MyTask"; 
     * String runbook = "RSQA.Random Number"; 
     * String expression = "0 0 12 * * ?"; //fired at 12pm (noon) every day." 
     * String userid = "resolve"; 
     * Boolean active = Boolean.TRUE;
     * boolean result = CronAPI.scheduleTask(taskName, runbook, expression, userid, active);
     * }
     * </pre>
     * 
     * @param taskName
     *            - unique name for the scheduled cron task
     * @param runbook
     *            - name of the runbook wiki to be executed. Also supports
     *            &lt;ESBADDR&gt;#Class.Method to send JMS message and
     *            MService.&lt;path&gt;.groovy script to be executed
     * @param expression
     *            like "0 0 12 * * ?" which is fired at 12pm (noon) every day.
     * @param expression
     * @param userid
     * @param active
     * @return
     */
    public static boolean scheduleTask(String taskName, String runbook, String expression, String userid, Boolean active)
    {
        return scheduleTask(taskName, null, runbook, null, expression, null, null, userid, active.booleanValue());
    }

    /**
     * Unschedules an existing task. This method removes it from the
     * resolve_cron table.
     * 
     * <pre>
     * {@code
     * import com.resolve.cron.CronAPI;
     * //...
     * //...
     * String taskName = "MyTask"; 
     * String userid = "resolve"; 
     * 
     * boolean result = CronAPI.unscheduleTask(taskName, userid);
     * }
     * </pre>
     * 
     * @param taskName
     *            - task name to be unscheduled
     * @param userid
     * @return
     */
    public static boolean unscheduleTask(String taskName, String userid)
    {
        Map<String, Object> params = new HashMap<String, Object>();
        params.put(Constants.CRON_NAME, taskName);
        params.put(Constants.CRON_USER_ID, userid); //may be used in the future for audit log.
        
        //Submit to the ESB so it can be processed by RSControl
        boolean isSuccess = MainBase.esb.sendInternalMessage(Constants.ESB_NAME_RSCONTROL, "MCron.unscheduleTask", params);
        if(isSuccess)
        {
            Log.log.debug("Task sent to the RSControl for unscheduling.");
        }
        else
        {
            Log.log.warn("Task failed transmission to RSControl for unscheduling.");
        }
        
        return isSuccess;
    }

    /**
     * Activates a task and load in the scheduler.
     * 
     * <pre>
     * {@code
     * import com.resolve.cron.CronAPI;
     * //...
     * //...
     * String taskName = "MyTask"; 
     * String userid = "resolve"; 
     * 
     * boolean result = CronAPI.activateTask(taskName, userid);
     * }
     * </pre>
     * 
     * @param taskName
     *            - task name to be activated
     * @param userid
     * @return
     */
    public static boolean activateTask(String taskName, String userid)
    {
        Map<String, Object> params = new HashMap<String, Object>();
        params.put(Constants.CRON_NAME, taskName);
        params.put(Constants.CRON_USER_ID, userid);
        
        //Submit to the ESB so it can be processed by RSControl
        boolean isSuccess = MainBase.esb.sendInternalMessage(Constants.ESB_NAME_RSCONTROL, "MCron.activateTask", params);
        if(isSuccess)
        {
            Log.log.debug("Task sent to the RSControl for activation.");
        }
        else
        {
            Log.log.warn("Task failed transmission to RSControl for activation.");
        }
        return isSuccess;
    }

    /**
     * Deactivates a task and drops from the scheduler.
     * 
     * <pre>
     * {@code
     * import com.resolve.cron.CronAPI;
     * //...
     * //...
     * String taskName = "MyTask"; 
     * String userid = "resolve"; 
     * 
     * boolean result = CronAPI.deactivateTask(taskName, userid);
     * }
     * </pre>
     * 
     * @param taskName
     *            - task name to be deactivated
     * @param userid
     * @return
     */
    public static boolean deactivateTask(String taskName, String userid)
    {
        Map<String, Object> params = new HashMap<String, Object>();
        params.put(Constants.CRON_NAME, taskName);
        params.put(Constants.CRON_USER_ID, userid);
        
        //Submit to the ESB so it can be processed by RSControl
        boolean isSuccess = MainBase.esb.sendInternalMessage(Constants.ESB_NAME_RSCONTROL, "MCron.deactivateTask", params);
        if(isSuccess)
        {
            Log.log.debug("Task sent to the RSControl for deactivation.");
        }
        else
        {
            Log.log.warn("Task failed transmission to RSControl for deactivation.");
        }
        return isSuccess;
    }
}
