/******************************************************************************
* (C) Copyright 2014
*
* Resolve Systems, LLC
*
* All rights reserved
* This software is distributed under license and may not be used, copied,
* modified, or distributed without the express written permission of
* Resolve Systems, LLC.
*
******************************************************************************/

package com.resolve.cron;

import java.util.Iterator;
import java.util.Map;
import java.io.File;

import org.quartz.JobExecutionContext;
import org.quartz.JobExecutionException;

import org.apache.commons.io.FileUtils;

import com.resolve.util.Log;
import com.resolve.util.StringUtils;
import com.resolve.util.Constants;
import com.resolve.esb.MMsgHeader;
import com.resolve.esb.MMsgOptions;
import com.resolve.rsbase.MainBase;

public class Spool extends CronExec
{
    String          inputfile;
    String          tmpfile;
    boolean         hasInputFile;
    boolean         hasTmpFile;
    boolean         hasResultFile;

    public Spool()
    {
    } // Spool
    
    public void execute(JobExecutionContext context) throws JobExecutionException
    {
        super.execute(context);

        try
        {
	        data = context.getJobDetail().getJobDataMap();
	        if (data != null)
	        {
	            String spooldir = getJobData("SPOOLDIR");
	            if (spooldir != null)
                {
	                File spool = new File(spooldir);
                    if (spool.isDirectory())
                    {
                        File[] files = spool.listFiles();
                        if (files.length > 0)
                        {
							Log.log.debug("Spool files: "+files.length);
                        }
                        
                        for (int i=0; i < files.length; i++)
                        {
                            File file = files[i];
                            
                            // check not directory
                            if (file.isDirectory() == false)
                            {
                                // check not tempory file
                                String filename = file.getName();
                                if (!filename.endsWith(".swp") && !filename.endsWith(".tmp"))
                                {
									Log.log.debug("  file: "+filename);
                                    String content = FileUtils.readFileToString(file, "UTF-8");
                                    
                                    // process query header for record UPDATE
                                    String header = null;
                                    if (!StringUtils.isEmpty(content) && content.charAt(0) == '#')
                                    {
                                        // get header
                                        int endidx = content.indexOf("\n");
                                        
                                        header = content.substring(0, endidx);
                                        header = StringUtils.chomp(header);
	                                    content = content.substring(endidx, content.length());
                                        
										Log.log.debug("  header: "+header);
                                    }
                                        
                                    // get content
                                    content = StringUtils.chomp(content);
									Log.log.debug("  content: "+content);
                                        
									sendResult(header, content, filename);
                                        
                                    // remove file
                                    Log.log.debug("  file deleted");
					                file.delete();
                                }
                            }
                        }
                    }
                }
	        }
        }
        catch (Exception e)
        {
            Log.log.error("Failed to execute spool task: "+e.getMessage(), e);
        }
    } // execute

    void sendResult(String fileHeader, String content, String eventid)
    {
        try
        {
            // define result header
            MMsgHeader header = new MMsgHeader();
            MMsgOptions options = new MMsgOptions();
	        options.setAddress(MainBase.main.configId.ipaddress);
            options.setTarget(MainBase.main.configId.guid);
            options.setUserID("system");
            options.setParserID("GenericNameValue (=)");
            options.put("EVENTID", eventid);
//            header.put(Constants.ESB_PARAM_TARGET, MainBase.main.configId.guid);
//            header.put(Constants.ESB_PARAM_USERID, Constants.GLIDE_RESOLVE_SYSTEM_USER);
//            header.put(Constants.ESB_PARAM_PARSERID, "GenericNameValue (=)");
//            header.put("EVENTID", eventid);
            
            // update message header from fileHeader
            if (fileHeader  != null)
            {
	            Map fileHeaders = StringUtils.stringToMap(fileHeader.substring(1, fileHeader.length()), "=", ";");
           
	            // strip wrapping quotes
	            for (Iterator i=fileHeaders.entrySet().iterator(); i.hasNext(); )
	            {
	                Map.Entry entry = (Map.Entry)i.next();
	                String key = (String)entry.getKey();
	                String value = (String)entry.getValue();
	                
	                if (!StringUtils.isEmpty(value))
	                {
		                if (value.charAt(0) == '"' && value.charAt(value.length()-1) == '"')
		                {
		                    value = value.substring(1, value.length()-1);
		                }
		                options.put(key, value);
	                }
	            }
            }
            
            // set destination
            header.setRouteDest(Constants.ESB_NAME_RSSERVER, "MAction.eventResult");

            // normalize format - remove carriage return (\r)
            content = content.replaceAll("\r", "");
            
            header.setOptions(options);
            
            // send message
            MainBase.esb.sendMessage(header, content);
        }
        catch (Exception e)
        {
            Log.log.warn(e.getMessage(), e);
        }
    } // sendResult

} // Spool

