/******************************************************************************
* (C) Copyright 2014
*
* Resolve Systems, LLC
*
* All rights reserved
* This software is distributed under license and may not be used, copied,
* modified, or distributed without the express written permission of
* Resolve Systems, LLC.
*
******************************************************************************/

package com.resolve.cron;

import org.quartz.JobExecutionContext;
import org.quartz.JobExecutionException;

public class Execute extends CronExec
{
    public Execute()
    {
    } // Execute
    
    public void execute(JobExecutionContext context) throws JobExecutionException
    {
        super.execute(context);

        /*
        data = context.getJobDetail().getJobDataMap();
        if (data != null)
        {
            String jobname = getJobData("JOBNAME");
            String returnResult = getJobData("RETURNRESULT");
            
            ExecuteMain exec = new ExecuteMain(data);
            exec.execute(returnResult.equalsIgnoreCase("TRUE"));

            // remove job if triggername == NOW
            String triggername = getJobData("TRIGGERNAME");
            if (triggername != null && triggername.equals("NOW"))
            {
                CronStore.removeJob(jobname);
            }
        }
        */
    } // execute

} // Execute

