/******************************************************************************
* (C) Copyright 2014
*
* Resolve Systems, LLC
*
* All rights reserved
* This software is distributed under license and may not be used, copied,
* modified, or distributed without the express written permission of
* Resolve Systems, LLC.
*
******************************************************************************/

package com.resolve.cron;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.ConcurrentHashMap;

import org.quartz.CronTrigger;
import org.quartz.JobDetail;
import org.quartz.Scheduler;
import org.quartz.Trigger;

import com.resolve.services.ServiceHibernate;
import com.resolve.services.hibernate.vo.ResolveCronVO;
import com.resolve.util.Log;

public class CronStore
{
    final static String CRONFILE_TASK    = "TASK";
    
    static ConcurrentHashMap<String, CronTask> tasks = new ConcurrentHashMap<String, CronTask>();
    
    static CronConfig config;
    static Scheduler quartz;
    
    static int tcount = 0;    
    
    public CronStore(CronConfig config) throws Exception
    {
        // set configuration settings
        if (config == null)
        {
            config = new CronConfig();
        }
        CronStore.config = config;
        initProperties();
        
        // create quartz and listeners
        quartz = new org.quartz.impl.StdSchedulerFactory().getScheduler();
        
    } // CronStore
    
    public static void initProperties()
    {
        // main scheduler properties
        System.setProperty("org.quartz.scheduler.instanceName", config.getName());
        System.setProperty("org.quartz.scheduler.instanceId", "AUTO");
        
        // thread pool
        System.setProperty("org.quartz.threadPool.class", "org.quartz.simpl.SimpleThreadPool");
        System.setProperty("org.quartz.threadPool.threadCount", ""+config.getThreadCount());
        System.setProperty("org.quartz.threadPool.threadPriority", ""+config.getThreadPriority());
        
        // jobstore
        System.setProperty("org.quartz.jobStore.misfireThreshold", ""+config.getMisfireThreshold());
        System.setProperty("org.quartz.jobStore.class", "org.quartz.simpl.RAMJobStore");
        
        // shutdown
        System.setProperty("org.quartz.plugin.shutdownHook.class", "org.quartz.plugins.management.ShutdownHookPlugin");
        System.setProperty("org.quartz.plugin.shutdownHook.cleanShutdown","true");
    } // initProperties
    
    public static void load()
    {
        // load tasks cronid from db
        List<ResolveCronVO> cronTasks = ServiceHibernate.findAllActiveCronJobs(true);
        for (ResolveCronVO dbTask : cronTasks)
        {
            addTask(new CronTask(dbTask));
        }

    } // load
    
    public static void save()
    {
    } // save
    
    public static void start() throws Exception
    {
        // start scheduler
        quartz.start();
    } // start
    
    public static void stop() throws Exception
    {
        // shutdown scheduler
        quartz.shutdown(false);
    } // stop
    
    public static boolean addTask(CronTask task)
    {
        boolean result = false;
        
        try
        {
            // remove task
	        removeTask(task.getName());
	        
            // create job
            JobDetail job = new JobDetail(task.getName(), task.getName(), Class.forName(task.getClassname()));
            if (job != null)
            {
                if (task.getParams() != null && task.getParams().size() > 0)
                {
                    job.getJobDataMap().putAll(task.getParams());
                }
                    
                // set job durability so that it is not removed from scheduler if it has no trigger references
                job.setDurability(true);
                    
                // add job to scheduler
                //quartz.addJob(jobDetail, true);
                    
                Trigger trigger = null;
                if (task.getStartTime() != null || task.getEndTime() != null)
                {
	                trigger = new CronTrigger(task.getName(), task.getName(), task.getName(), task.getName(), task.getStartTime(), task.getEndTime(), task.getExpression());
                }
                else
                {
	                trigger = new CronTrigger(task.getName(), task.getName(), task.getExpression());
                }
                
                if (trigger != null)
                {
                    int triggerState = quartz.getTriggerState(trigger.getName(), trigger.getGroup());                
                    if (triggerState == Trigger.STATE_NONE  || triggerState == Trigger.STATE_COMPLETE)
                    {
		                Log.log.debug("Scheduled Cron Task: "+task.getName()+" wiki: "+task.getWiki()+" expression: "+task.getExpression()+" startTime: "+task.getStartTime()+" endTime: "+task.getEndTime());
                        quartz.scheduleJob(job, trigger);
                            
                        result = true;
                    }
                }
            }
                
            // if still scheduled add task to tasklist
	        tasks.put(task.getName(), task);
	        
            //Log.log.debug("Added Cron Task: "+task.getName());
            result = true;
        }
        catch (Exception e)
        {
            Log.log.error("Failed to addTask: "+e.getMessage(), e);
        }
        
        return result;
    } // addTask
    
    public static boolean removeTask(String taskname)
    {
        boolean result = false;
       
        try
        {
            if (taskname != null)
            {
                taskname = taskname.toUpperCase();
                
                Log.log.debug("Remove Cron Task: "+taskname);
                tasks.remove(taskname);
                quartz.unscheduleJob(taskname, taskname);
                quartz.deleteJob(taskname, taskname);
                    
                result = true;
            }
        }
        catch (Throwable e)
        {
            Log.log.error("Failed to removeTask: "+e.getMessage(), e);
        }
        
        return result;
    } // removeTask
    
    public static boolean isTaskScheduled(String taskname)
    {
        return tasks.containsKey(taskname);
    } // isTaskScheduled
    
    
    public static CronTask getTask(String taskname)
    {
        return (CronTask)tasks.get(taskname.toUpperCase());
    } // getTask
    
    public static List<CronTask> getTasks()
    {
        return new ArrayList<CronTask>(tasks.values());
    } // getTasks

} // CronStore

