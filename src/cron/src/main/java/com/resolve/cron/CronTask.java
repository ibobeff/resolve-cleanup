/******************************************************************************
* (C) Copyright 2014
*
* Resolve Systems, LLC
*
* All rights reserved
* This software is distributed under license and may not be used, copied,
* modified, or distributed without the express written permission of
* Resolve Systems, LLC.
*
******************************************************************************/

package com.resolve.cron;

import java.util.Date;
import java.util.HashMap;
import java.util.Map;

import com.resolve.services.hibernate.vo.ResolveCronVO;

public class CronTask extends CronBase
{
    String name;
    String classname;
    String wiki;
    String expression;
    Date startTime;
    Date endTime;
    Map params;
    
    public CronTask()
    {
    } // CronTask
    
    public CronTask(String name, String classname, String wiki, String expression, Map params)
    {
        this.name = name;
        this.classname = classname;
        this.wiki = wiki;
        this.expression = expression;
        this.params = new HashMap(params);
        this.startTime = null;
        this.endTime = null;
    } // CronTask
    
    public CronTask(String name, String classname, String wiki, String expression, Map params, Date startTime, Date endTime)
    {
        this.name = name;
        this.classname = classname;
        this.wiki = wiki;
        this.expression = expression;
        this.params = new HashMap(params);
        this.startTime = startTime;
        this.endTime = endTime;
    } // CronTask
    
    public CronTask(ResolveCronVO dbTask)
    {
        Map jobParams = new HashMap();
        jobParams.put("NAME", dbTask.getUName().toUpperCase());
        jobParams.put("WIKI", dbTask.getURunbook());
        jobParams.put("EXPRESSION", dbTask.getUExpression());
        jobParams.put("PARAMS", dbTask.getUParams());
        if (dbTask.getUStartTime() != null)
        {
	        jobParams.put("STARTTIME", dbTask.getUStartTime());
        }
        if (dbTask.getUEndTime() != null)
        {
	        jobParams.put("ENDTIME", dbTask.getUEndTime());
        }
        
        this.setName(dbTask.getUName().toUpperCase());
        this.setClassname("com.resolve.rscontrol.CronActionTrigger");
        this.setWiki(dbTask.getURunbook());
        this.setExpression(dbTask.getUExpression());
        this.setParams(jobParams);
        this.setStartTime(dbTask.getUStartTime());
        this.setEndTime(dbTask.getUEndTime());
    } // CronTask
    
    public String getName()
    {
        return name.toUpperCase();
    }
    public void setName(String name)
    {
        this.name = name.toUpperCase();
    }
    public String getClassname()
    {
        return classname;
    }
    public void setClassname(String classname)
    {
        this.classname = classname;
    }
    public String getWiki()
    {
        return wiki;
    }
    public void setWiki(String wiki)
    {
        this.wiki = wiki;
    }
    public String getExpression()
    {
        return expression;
    }
    public void setExpression(String expression)
    {
        this.expression = expression;
    }
    public Map getParams()
    {
        return params;
    }
    public void setParams(Map params)
    {
        this.params = params;
    }
    public Date getStartTime()
    {
        return startTime;
    }
    public void setStartTime(Date startTime)
    {
        this.startTime = startTime;
    }
    public Date getEndTime()
    {
        return endTime;
    }
    public void setEndTime(Date endTime)
    {
        this.endTime = endTime;
    }
    
} // CronTask
