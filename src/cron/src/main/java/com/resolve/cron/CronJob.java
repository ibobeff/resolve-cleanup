/******************************************************************************
* (C) Copyright 2014
*
* Resolve Systems, LLC
*
* All rights reserved
* This software is distributed under license and may not be used, copied,
* modified, or distributed without the express written permission of
* Resolve Systems, LLC.
*
******************************************************************************/

package com.resolve.cron;

import java.util.Iterator;
import java.util.Map;
import java.util.Hashtable;

import org.quartz.JobDetail;

import com.resolve.util.StringUtils;

public class CronJob extends CronBase
{
    String name;
    String group;
    String classname;
    CronJobData data;
    JobDetail jobDetail;
    
    public CronJob()
    {
    } // CronJob
    
    // create job from job definition string - definition: class:name1=value1&...
    public CronJob(String jobname, String definition)
    {
        this.name = jobname;
        this.group = "ALL";
        
        int separator = definition.indexOf(":");
        if (separator == -1)
        {
            this.classname = definition;
        }
        else
        {
            this.classname = definition.substring(0, separator);
            String jobdata = definition.substring(separator+1, definition.length());

            Hashtable data = new Hashtable();
            String[] namevals = jobdata.split(StringUtils.FIELDSEPARATOR_REGEX);
            for (int i=0; i < namevals.length; i++)
            {
                String[] nameval = namevals[i].split(StringUtils.VALUESEPARATOR_REGEX);
                if (nameval.length == 2)
                {
                    String name = nameval[0];
                    String value = nameval[1];
                        
                    data.put(name.toUpperCase(), value);
                }
            }
            this.setData(data);
        }
    } // CronJob
    
    public String toString()
    {
        String result = "";
        result += "CronJob name: "+name+"\n";
        result += "  classname: "+classname+"\n";
        result += "  data: "+data+"\n";
        
        return result;
    } // toString
    
    // definition: class:name1=value1&...
    public String getDefinition()
    {
        String result = "";
        
        result += classname;
        if (data != null && data.getData().size() > 0)
        {
            result += ":";
            for (Iterator i=data.getData().entrySet().iterator(); i.hasNext(); )
            {
                Map.Entry entry = (Map.Entry)i.next();
                String name = (String)entry.getKey();
                String value = (String)entry.getValue();
                
                value = value.replaceAll("\n", "");
                
                result += name+StringUtils.VALUESEPARATOR+value+StringUtils.FIELDSEPARATOR;
            }
        }
        
        return result;
    } // getDefinition
    
    public boolean hasData()
    {
        return data != null;
    } // hasData
    
    public boolean hasJobDetail()
    {
        return jobDetail != null;
    } // hasJobDetail
    
    public String getClassname()
    {
        return classname;
    }
    public void setClassname(String classname)
    {
        this.classname = classname;
    }
    public String getGroup()
    {
        return group;
    }
    public void setGroup(String group)
    {
        this.group = group.toUpperCase();
    }
    public String getName()
    {
        return name.toUpperCase();
    }
    public void setName(String name)
    {
        this.name = name.toUpperCase();
    }
    public Hashtable getData()
    {
        Hashtable result = null;
        
        if (data != null)
        {
            result = data.getData();
        }
        return result;
    }
    public void setData(Hashtable data)
    {
        this.data = new CronJobData();
        this.data.setData(data);
    }
    public JobDetail getJobDetail()
    {
        return jobDetail;
    }
    public void setJobDetail(JobDetail jobDetail)
    {
        this.jobDetail = jobDetail;
    }
} // CronJob
