/******************************************************************************
* (C) Copyright 2014
*
* Resolve Systems, LLC
*
* All rights reserved
* This software is distributed under license and may not be used, copied,
* modified, or distributed without the express written permission of
* Resolve Systems, LLC.
*
******************************************************************************/

package com.resolve.cron;

import java.util.Map;
import java.util.Hashtable;

// used to enumerate CronJob data in DBO instead of base collection types
public class CronJobData extends CronBase
{
    Hashtable data;
    
    public Hashtable getData()
    {
        return this.data;
    } // getData
    
    public void setData(Map data)
    {
        this.data = new Hashtable();
        this.data.putAll(data);
    } // setData
    
    public String toString()
    {
        return this.data.toString();
    } // toString

} // CronJobData
