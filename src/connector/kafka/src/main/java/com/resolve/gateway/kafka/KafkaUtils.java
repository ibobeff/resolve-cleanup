package com.resolve.gateway.kafka;

import java.io.IOException;
import java.util.Arrays;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Properties;

import org.apache.commons.lang.StringUtils;
import org.codehaus.jackson.JsonParseException;
import org.codehaus.jackson.map.JsonMappingException;
import org.codehaus.jackson.map.ObjectMapper;

import com.google.gson.Gson;

public class KafkaUtils {
	
	
	public static Properties jsonToProperties(String json) throws JsonParseException, JsonMappingException, IOException, Exception
	{
		Properties properties = new Properties();
		@SuppressWarnings("unchecked")
		HashMap<String,Object> propertiesHash = new ObjectMapper().readValue(json, HashMap.class);
		for(Entry<String, Object> curPropEntry: propertiesHash.entrySet())
		{
			properties.put(curPropEntry.getKey(), curPropEntry.getValue());
		}
		
		return properties;
	}
	
	public static List<String> parseBrokers(String brokerUrls) throws Exception
	{
		List<String> brokers = (List<String>) Arrays.asList(brokerUrls.trim().toLowerCase().split("\\s*,\\s*"));
		Collections.sort(brokers);
		return brokers;
	}
	
	public static String brokerListToString(List<String> brokers)
	{
		Collections.sort(brokers);
		return StringUtils.join(brokers, ",");
	}

	public static String propertiesToJson(Properties properties) 
	{
		Map<Object, Object> propertiesMap = new HashMap<>();
		
		for(Entry<Object, Object> curProp: properties.entrySet())
		{
			propertiesMap.put(curProp.getKey(), curProp.getValue());
		}
		
		return new Gson().toJson(propertiesMap);
	}
}
