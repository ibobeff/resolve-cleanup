package com.resolve.gateway.kafka;

import java.util.Properties;
import java.util.concurrent.ConcurrentHashMap;

import org.apache.kafka.clients.producer.KafkaProducer;
import org.apache.kafka.clients.producer.Producer;

public class ProducerStore 
{
	private ConcurrentHashMap<Properties, Producer<String, String>> store;
	public ProducerStore()
	{
		store = new ConcurrentHashMap<>();
	}
	
	public Producer<String, String> getProducer(Properties producerProperties) throws Exception
	{
		return store.get(producerProperties);
	}
	
	public Producer<String, String> createProducer(Properties producerProperties) throws Exception
	{
		Producer<String, String> producer = getProducer(producerProperties);
		if(producer == null)
		{
			producer = new KafkaProducer<>(producerProperties);
			store.put(producerProperties, producer);
		}
		
		return producer;
	}
	
	public Producer<String, String> createProducer(String brokerUrls, Properties producerProperties) throws Exception
	{
		if(producerProperties == null || producerProperties.size() == 0) 
		{
			String producerPropertiesJson = KafkaGateway.getInstance().getKafkaBlueprints().getDefaultProducerProperties();
			producerProperties = KafkaUtils.jsonToProperties(producerPropertiesJson);
		}
		producerProperties.put("bootstrap.servers", brokerUrls);
		return createProducer(producerProperties);
	}	
}
