package com.resolve.gateway.kafka;

import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Properties;

import org.apache.commons.lang.StringUtils;
import org.apache.kafka.clients.consumer.Consumer;
import org.apache.kafka.clients.consumer.ConsumerRecord;
import org.apache.kafka.clients.consumer.ConsumerRecords;
import org.apache.kafka.clients.consumer.KafkaConsumer;
import org.apache.kafka.clients.consumer.OffsetAndMetadata;
import org.apache.kafka.common.TopicPartition;
import org.apache.kafka.clients.consumer.ConsumerConfig;
import com.resolve.util.Log;

public class ResolveKafkaConsumer implements Runnable {
		
		private volatile boolean polling = true;
		Consumer<String, String> consumer;
		Properties consumerProperties;
		ArrayList<String> filterIds;
		LinkedList<ConsumerAction> actionQueue;
		int maxRecordPoll = 0;
		Long highestOffset = 0L;
		
		public ResolveKafkaConsumer(Properties consumerProperties)
		{
			this.consumerProperties = consumerProperties;
			filterIds = new ArrayList<String>();
			actionQueue = new LinkedList<ConsumerAction>();
			groomProperties();
			consumer = new KafkaConsumer<>(consumerProperties);
			maxRecordPoll = Integer.parseInt(consumerProperties.getProperty(ConsumerConfig.MAX_POLL_RECORDS_CONFIG));
		}
				
		public void run()
		{
			startPolling();
			return;
		}
		
		private void startPolling()
		{
			polling = true;
						
			Log.log.info(String.format("Starting Kafka Consumer. TopicData: %s", consumer.listTopics().toString()));
			while(polling)
			{	
				ConsumerRecords<String, String> records = consumer.poll(5000);
				Log.log.trace("Kafka Consumed: " + records.count() + " records for: " + consumer.listTopics().toString());
				
				for(ConsumerRecord<String, String> curRecord: records)
				{
					KafkaGateway.getInstance().processData(this, curRecord, null);
				}
				processQueuedActions();				
			}
			
			//Code only gets this far if the runnable has been instructed to stopPolling()
			Log.log.info(String.format("Stopping Kafka Consumer. TopicData: %s", consumer.listTopics().toString()));
			consumer.close();
			return;
		}
		
		private void processQueuedActions()
		{			
			while(!actionQueue.isEmpty())
			{
				ConsumerAction curAction = actionQueue.removeLast();
				try 
				{
					curAction.getActionMethod().invoke(consumer, curAction.getActionParameters());
				} 
				catch (IllegalAccessException | IllegalArgumentException | InvocationTargetException e) 
				{
					Log.log.error("Exception encountered invoking a consumer action", e);
				}
			}
		}
		
		public void stopPolling()
		{
			polling = false;
		}
		
		public Consumer<String, String> getConsumer()
		{
			return consumer;
		}
		
		public Properties getConsumerProperties()
		{
			return consumerProperties;
		}
		
		public void addFilterId(String id)
		{
			if(!filterIds.contains(id))
			{
				filterIds.add(id);
			}
		}
		
		public List<String> getFilterIds()
		{
			return filterIds;
		}
		
		public void seekToEnd(String topic, int partition)throws NoSuchMethodException, SecurityException
		{
			Method method = KafkaConsumer.class.getMethod("seekToEnd", Collection.class);
			actionQueue.push(new ConsumerAction(method, new Object[] {Collections.singletonList(new TopicPartition(topic,partition))}));
			Log.log.info(String.format("Seeking offset to end for filters: %s", StringUtils.join(filterIds, ", ")));
		}
		
		public void seekToBeginning(String topic, int partition) throws Exception
		{

			Log.log.info(String.format("Seeking offset to beginning for filters: %s", StringUtils.join(filterIds, ", ")));
			Method method = KafkaConsumer.class.getMethod("seekToBeginning", Collection.class);				
			actionQueue.push(new ConsumerAction(method, new Object[] {Collections.singletonList(new TopicPartition(topic,partition))}));

		}
		
		public void seekToOffset(String topic, int partition,Long offset) throws Exception
		{
			Log.log.info(String.format("Seeking offset to %s for filters: %s", offset.toString(), StringUtils.join(filterIds, ", ")));
			Method method = KafkaConsumer.class.getMethod("seek", TopicPartition.class, Long.class);				
			actionQueue.push(new ConsumerAction(method, new Object[] {Collections.singletonMap(new TopicPartition(topic,partition),new OffsetAndMetadata(offset))}));
		}
		
		public void commitOffset(String topic, int partition, Long offset, boolean force) throws Exception
		{
			if(force || offset > highestOffset)
			{
				highestOffset = offset;
				Log.log.info(String.format("Commiting at offset %s", offset.toString()));
				Method method = KafkaConsumer.class.getMethod("commitSync", Map.class);				
				actionQueue.push(new ConsumerAction(method, new Object[] {Collections.singletonMap(new TopicPartition(topic,partition),new OffsetAndMetadata(offset))}));
			}
			
		}
		public void groomProperties()
		{
			try
			{
				Integer.parseInt(consumerProperties.getProperty(ConsumerConfig.MAX_POLL_RECORDS_CONFIG));
			}
			catch(NumberFormatException e)
			{
				Log.log.warn(String.format("%s was missing from consumer properties. Setting this will help throttle consumption and prevent overflow from getting lost. Set in blueprints at 'rsremote.receive.kafka.defaultproducerproperties'. Defaulting to 50.", ConsumerConfig.MAX_POLL_RECORDS_CONFIG));
				consumerProperties.put(ConsumerConfig.MAX_POLL_RECORDS_CONFIG, 50);
			}
		}
		
		class ConsumerAction
		{
			Method actionMethod;
			Object[] actionParameters;
			
			public ConsumerAction(Method actionMethod, Object[] actionParameters)
			{
				this.actionMethod = actionMethod;
				this.actionParameters = actionParameters;
			}
			
			public Method getActionMethod()
			{
				return actionMethod;
			}
			
			public Object[] getActionParameters()
			{
				return actionParameters;
			}
		}
}
