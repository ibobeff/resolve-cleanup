package com.resolve.gateway.kafka;

import java.util.Properties;
import java.util.UUID;
import java.util.concurrent.ConcurrentHashMap;

import org.apache.kafka.clients.consumer.Consumer;
import org.apache.kafka.clients.producer.KafkaProducer;
import org.apache.kafka.clients.producer.ProducerRecord;
import org.apache.kafka.common.TopicPartition;

import com.resolve.gateway.AbstractGatewayAPI;

public class KafkaAPI extends AbstractGatewayAPI
{	
	/**
	 * 
	 * @param brokerUrls - Comma seperated list of broker entry points. All URLS provided should belong to a single Kafka Environment. Ex: node1.myKafkaCluster.intranet:9092,node2.myKafkaCluster.intranet:9092
	 * @param record - Must contain topic and message
	 * @param producerProperties - If left null, defaultproducerproperties from resolve blueprints will be used.
	 * @return Just a friendly message with no use.
	 * @throws Exception - When an exception is thrown you can assume the message was not sent.
	 */
	public static String sendMessage(String brokerUrls, ProducerRecord<String,String> record, Properties producerProperties) throws Exception
	{
		 KafkaGateway instance = KafkaGateway.getInstance();
		 KafkaProducer<String, String> producer = (KafkaProducer<String, String>) instance.getProducerStore().createProducer(brokerUrls, producerProperties);
		 producer.send(record);
		 return "Message Sent";
	}
	
	public static String sendMessage(String brokerUrls, ProducerRecord<String,String> record) throws Exception
	{
		 return sendMessage(brokerUrls, record, null);
	}
	
	public static String sendMessage(String brokerUrls, String topic, String message, Properties properties) throws Exception
	{
		String messageUUID = UUID.randomUUID().toString();
		ProducerRecord<String, String> record = new ProducerRecord<String, String>(topic, messageUUID, message.toString());
		return sendMessage(brokerUrls, record, properties);
	}
	
	public static String sendMessage(String brokerUrls, String topic, String message) throws Exception
	{
		return sendMessage(brokerUrls, topic, message, null);
	}
	
	public static String sendMessageExpectingAck(String brokerUrls, String topic, String message, Long ackTimeout, Properties producerProperties) throws Exception
	{
		if(ackTimeout == null) { ackTimeout = new Long(180); }
		KafkaGateway instance = KafkaGateway.getInstance();
		KafkaProducer<String, String> producer = (KafkaProducer<String, String>) instance.getProducerStore().createProducer(brokerUrls, producerProperties);
		String messageUUID = UUID.randomUUID().toString();
		ProducerRecord<String, String> record = new ProducerRecord<String, String>(topic, messageUUID, message.toString());
		
		Long startTimeMillis = System.currentTimeMillis();
		producer.send(record).get(ackTimeout, java.util.concurrent.TimeUnit.MILLISECONDS);
		Long elapsedTime = System.currentTimeMillis()-startTimeMillis;
		
		return "Message Sent and Acknowledged: " + elapsedTime + " milliseconds.";
		
	}
	
	public static String sendMessageExpectingAck(String brokerUrls, String topic, String message, Properties producerProperties) throws Exception
	{
		return sendMessageExpectingAck(brokerUrls, topic, message, null, producerProperties);
	}
	
	public static String sendMessageExpectingAck(String brokerUrls, String topic, String message, Long actTimeout) throws Exception
	{
		return sendMessageExpectingAck(brokerUrls, topic, message, actTimeout, null);
	}
	
	public static String sendMessageExpectingAck(String brokerUrls, String topic, String message) throws Exception
	{
		return sendMessageExpectingAck(brokerUrls, topic, message, null, null);
	}
	
	public static void seekToOffset(String filterName,String topic,int partition, Long offset) throws Exception
	{
		getConsumer(filterName).seekToOffset(topic,partition,offset);
	}
	
	public static void seekToEnd(String filterName,String topic,int partition) throws Exception
	{
		getConsumer(filterName).seekToEnd(topic,partition);
	}
	
	public static void seekToBeginning(String filterName,String topic,int partition) throws Exception
	{
		getConsumer(filterName).seekToBeginning(topic,partition);
	}
	
	public static void commitOffset(String filterName, String topic,int partition, Long offset) throws Exception 
	{
		getConsumer(filterName).commitOffset(topic, partition, offset, false);
	}
	public static void commitOffset(String filterName, String topic,int partition, Long offset, boolean force) throws Exception 
	{
		getConsumer(filterName).commitOffset(topic, partition, offset, force);
	}
	private static ResolveKafkaConsumer getConsumer(String filterId) throws Exception
	{
		KafkaGateway instance = KafkaGateway.getInstance();
		if(instance == null) throw new RuntimeException("KafkaGateway is not running for this rsremote");
		ConsumerStore store = instance.getConsumerStore();
		if(store == null) throw new RuntimeException ("No Consumer store was found on this rsremote. Make sure filters are deployed");
		ResolveKafkaConsumer consumer = store.getConsumer(filterId);
		if(consumer == null) throw new RuntimeException(String.format("No active consumer was found for %s", filterId));
		return consumer;
	}
}

