package com.resolve.gateway.kafka;

import java.net.InetAddress;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import java.util.Properties;
import java.util.List;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ThreadPoolExecutor;
import java.util.concurrent.ConcurrentHashMap.KeySetView;

import org.apache.kafka.clients.consumer.ConsumerRecord;
import org.apache.kafka.clients.consumer.ConsumerRecords;
import org.apache.kafka.clients.consumer.KafkaConsumer;
import org.apache.kafka.clients.producer.Producer;

import com.resolve.gateway.BaseClusteredGateway;
import com.resolve.gateway.BaseFilter;
import com.resolve.gateway.Filter;
import com.resolve.gateway.MKafka;
import com.resolve.util.Constants;
import com.resolve.util.Log;
import com.resolve.util.StringUtils;

import groovy.json.JsonParserType;
import groovy.json.JsonSlurper;
import groovy.util.Eval;

public class KafkaGateway extends BaseClusteredGateway
{
    // Singleton
    private static volatile KafkaGateway instance = null;
    private String queue = null;
    private ProducerStore producerStore;
    private ConsumerStore consumerStore;

    public static KafkaGateway getInstance(ConfigReceiveKafka config)
    {    	
        if (instance == null)
        {
            instance = new KafkaGateway(config);
        }
        return instance;
    }
    
    /**
     * This method is called once the initialization is done. If it gets called
     * before the initialization it will throw exception.
     *
     * @return
     */
    public static KafkaGateway getInstance()
    {
        if (instance == null)
        {
            throw new RuntimeException("Kafka Gateway is not initialized correctly..");
        }
        else
        {
            return instance;
        }
    }

    private KafkaGateway(ConfigReceiveKafka config)
    {
        // Important, call super here.    	
        super(config);
    }
    
	@Override
    public String getLicenseCode()
    {
        return "KAFKA";
    }

    @Override
    protected void addAdditionalValues(Map<String, Object> arg0, Map<String, String> arg1)
    {
        // TODO Auto-generated method stub
        
    }

    @Override
    protected String getGatewayEventType()
    {
        return "KAFKA";
    }

    @Override
    protected String getMessageHandlerName()
    {
        return MKafka.class.getSimpleName();
    }

    @Override
    protected Class<MKafka> getMessageHandlerClass()
    {
        return MKafka.class;
    }

    public String getQueueName()
    {
        return queue;
    }
    
    public ProducerStore getProducerStore()
    {
    	return producerStore;
    }
    
    public ConsumerStore getConsumerStore()
    {
    	return consumerStore;
    }
    
    @Override
    public void start()
    {
        Log.log.debug("Starting KAFKA Gateway");
        super.start();
    } // start

    @Override
    public void run()
    {
        //This call is must for all the ClusteredGateways.
        super.sendSyncRequest();
        if(isPrimary() && isActive())
        {
            //TODO maybe need to set all filters here if clearAndSetFilters does not get called during server startup.        	
        }
    }

	@Override
    protected void initialize()
    {
        ConfigReceiveKafka config = (ConfigReceiveKafka) configurations;
        
        queue = config.getQueue().toUpperCase();
        
        consumerStore = new ConsumerStore();
        producerStore = new ProducerStore();

        try
        {
            Log.log.info("Initializing Kafka Listener");
            this.gatewayConfigDir = "/config/kafka/";
            
        }
        catch (Exception e)
        {
            Log.log.error("Failed to config kafka Gateway: " + e.getMessage(), e);
        }
    }
	
	public ConfigReceiveKafka getKafkaBlueprints()
	{
		return (ConfigReceiveKafka) configurations;
	}
  
    /**
     * This method processes the message received from the Kafka system.
     *
     * @param message
     */
    public boolean processData(ResolveKafkaConsumer resolveKafkaConsumer, ConsumerRecord<String, String> record, Map<String, String> params)
    {
        
    	boolean result = true;
    	Object parseResults = null;
    	    	
    	try
    	{
    		JsonSlurper jsonSlurper = new JsonSlurper();
    		jsonSlurper.setType(JsonParserType.INDEX_OVERLAY); //This is also known as JsonFastParser
    		parseResults = jsonSlurper.parseText(record.value());
    	}
    	catch(Exception e)
    	{
    		Log.log.error("KafkaGateway encountered an error when parsing a JSON message to JsonSlurper. Topic: " + record.topic() + ", Message: " + record.value() + ", Exception: " + e.getMessage(),e);
    	}    	
    	
    	for(Filter filter: filters.values())
    	{    		
    		try
    		{
	    		KafkaFilter curFilter = (KafkaFilter) filter;
	    		
	    		String consumersBrokerUrls = resolveKafkaConsumer.getConsumerProperties().getProperty("bootstrap.servers");
	    		boolean filterContainsConsumersBroker = curFilter.containsBroker(consumersBrokerUrls);
	    		boolean filterMatchesTopic = curFilter.getTopic().equalsIgnoreCase(record.topic());
	    		if(curFilter.isActive() && filterMatchesTopic && filterContainsConsumersBroker)
	    		{   			
	    			boolean executeFilter = false;
	    			String filterCriteria = curFilter.getFilterCriteria();
	    			
	    			if(StringUtils.isEmpty(filterCriteria))
	    			{
	    				executeFilter = true;
	    			}
	    			else
	    			{   
	    				Object criteriaResults = Eval.me("JSON", parseResults, filterCriteria);
			    		executeFilter = (boolean) criteriaResults;
	    			}
	    			
	    			if(executeFilter)
	    			{
		    			Map<String, String> scriptParams = params;
		    			if(scriptParams == null)
		    			{
		    				scriptParams = new HashMap<>();
		    			}
	                    if(!scriptParams.containsKey(Constants.EVENT_EVENTID) || StringUtils.isBlank(scriptParams.get(Constants.EVENT_EVENTID)))
	                    {
	                    	scriptParams.put(Constants.EVENT_EVENTID, curFilter.getEventEventId());
	                    }
	                    
	                    scriptParams.put("WIKI", curFilter.getRunbook());
	                    scriptParams.put("KAFKAFILTER_NAME", curFilter.getId());
	                    scriptParams.put("FILTER_MESSAGE", record.value());
	                    scriptParams.put("FILTER_MESSAGE_KEY", record.key());
	                    scriptParams.put("FILTER_TOPIC", record.topic());
	                    scriptParams.put("FILTER_TOPIC_PARTITION", Integer.toString(record.partition()));
	                    scriptParams.put("FILTER_TOPIC_OFFSET", Long.toString(record.offset()));
	                    scriptParams.put("FILTER_BROKER_URLS", StringUtils.join(curFilter.getBrokerUrls(),","));
	                    scriptParams.put("FILTER_QUEUE_NAME", this.getQueueName());
	                    	                    
	                    int timeout = 60000;
	                    int safeAmountOfFreeQueueSlots = 10;
	                    long startTime = System.currentTimeMillis();
	                    int availableSpace = getExecuteMaxQueueSize() - getExecuterQueueCount();
	                    while(availableSpace < safeAmountOfFreeQueueSlots && System.currentTimeMillis() - startTime < timeout) 
	                    {
	                    	Thread.sleep(500);
	                    	if(System.currentTimeMillis() - startTime < timeout)
	                    	{
	                    		Log.log.warn("TimedOut waiting for slots to free up. Filter ID: " + curFilter.getId());
	                    	}
	                    }
	                    addToPrimaryDataQueue(curFilter, scriptParams);
	    			}
	    		}
    		}
            catch (Exception e)
            {
                Log.log.error("Error Processing Kafka Message. Topic: "+ record.topic() + ", Offset: " + record.offset() + ", Message: " + record.value() + ", Exception: " + e.getMessage(), e);
                try
                {
                    Thread.sleep(interval);
                }
                catch (InterruptedException ie)
                {
                    Log.log.warn("sleep interrupted: " + ie.getMessage(), ie);
                }
            }
    		
    	}
        return result;
    }    
    
    @Override
    public void clearAndSetFilters(List<Map<String, Object>> filterList)
    {
		Log.log.debug("Calling Gateway specific clearAndSetFilter" + (filterList != null ? " " + filterList.size() + ", [" + filterList + "]": ""));
        super.clearAndSetFilters(filterList);
        if(isPrimary() && isActive())
        {
            try
            {
            	consumerStore.stopConsumerRunnables();
                initKafkaConsumers(filterList);
            }
            catch (Exception e)
            {
                Log.log.error("Error ecountered when setting kafka filters: " + e.getMessage(), e);
            } 
        }
        
    } 
    
    public int getExecuterQueueCount()
    {    	
    	return ((ThreadPoolExecutor)executor).getQueue().size();
    }
    
    public int getExecuteMaxQueueSize()
    {
    	return getPrimaryDataQueueExecutorQueueSize();
    }
    
    @Override
    protected void cleanFilter(ConcurrentHashMap<String, Filter> undeployedFilters)
    {
    	consumerStore.stopConsumerRunnables();
    }    
    
	private void initKafkaConsumers(List<Map<String, Object>> filterList)
    {
		Log.log.debug("initKafkaConsumers()");
		try
		{
			for(Map<String, Object> curFilterAttributes: filterList)
			{
				if(curFilterAttributes.containsKey(KafkaFilter.ID))
				{
					KafkaFilter curFilter = (KafkaFilter) getFilter(curFilterAttributes);
					if(curFilter.isActive() && curFilter.isValid()) //check that the filter is assigned to this gateways queue
					{
						Properties properties = curFilter.getConsumerProperties();						
						properties.put("bootstrap.servers", KafkaUtils.brokerListToString(curFilter.getBrokerUrls()));						
						
						if(properties.get("group.id") == null)
						{
							String clusterName = getKafkaBlueprints().getClusterName(); //need to fix later
							properties.put("group.id", clusterName);
						}
						consumerStore.createConsumer(curFilter.getBrokerUrls(), curFilter.getTopic(), properties, curFilter.getId(),curFilter.getManualCommit());
					}
				}
			}
			consumerStore.startConsumerRunnables();			
		}
		catch(Exception e)
		{
			Log.log.error(e.getMessage(), e);
		}
    }
	
    @Override
    public void stop()
    {
        Log.log.warn("Stopping Kafka gateway");

    }
	
	@Override
    public Filter getFilter(Map<String, Object> params)
    {
        return new KafkaFilter((String) params.get(BaseFilter.ID), (String) params.get(BaseFilter.ACTIVE), (String) params.get(BaseFilter.ORDER), (String) params.get(BaseFilter.INTERVAL), 
                        (String) params.get(BaseFilter.EVENT_EVENTID), (String) params.get(BaseFilter.RUNBOOK), (String) params.get(BaseFilter.SCRIPT),
                        (String) params.get(KafkaFilter.BROKERURLS), (String) params.get(KafkaFilter.TOPIC), (String) params.get(KafkaFilter.FILTERCRITERIA),
                        (String) params.get(KafkaFilter.CONSUMERPROPERTIES),Boolean.valueOf((String)params.get(KafkaFilter.MANUALCOMMIT)));
    }	
	
}

