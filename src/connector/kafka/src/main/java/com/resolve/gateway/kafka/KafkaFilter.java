package com.resolve.gateway.kafka;

import java.io.IOException;
import java.util.List;
import java.util.Properties;

import org.apache.kafka.clients.consumer.KafkaConsumer;
import org.codehaus.jackson.JsonParseException;
import org.codehaus.jackson.map.JsonMappingException;

import com.resolve.gateway.BaseFilter;
import com.resolve.util.Log;
import com.resolve.util.StringUtils;

public class KafkaFilter extends BaseFilter
{
	public static final String TOPIC = "TOPIC";
	public static final String BROKERURLS = "BROKERURLS";
	public static final String FILTERCRITERIA = "FILTERCRITERIA";
	public static final String CONSUMERPROPERTIES = "CONSUMERPROPERTIES";
	public static final String MANUALCOMMIT = "MANUALCOMMIT";
	
	public List<String> brokerUrls;
	public String topic;
	public String filterCriteria;
	public Boolean manualCommit;
	public Properties consumerProperties;
	public ResolveKafkaConsumer consumer;
	
	public KafkaFilter(String id, String active, String order, String interval, String eventEventId, String runbook, 
			String script, String brokerUrls, String topic, String filterCriteria, String consumerProperties,Boolean manualCommit) 
	{
		super(id, active, order, interval, eventEventId, runbook, script);
		try
		{
			setBrokerUrls(brokerUrls);
			setTopic(topic);
			setFilterCriteria(filterCriteria);
			setConsumerProperties(consumerProperties);
			setManualCommit(manualCommit);
		}
		catch(Exception e)
		{
			Log.log.error("Error Building Kafka Filter: " + id, e);
		}
	}
    

	public boolean isValid() {
		//Put logic here that will check the filter for valid data
		return true;
	}
	
	public List<String> getBrokerUrls()
	{
		return brokerUrls;
	}
	
	public void setBrokerUrls(String brokerUrls) throws Exception
	{
		this.brokerUrls = KafkaUtils.parseBrokers(brokerUrls);
	}
	
	public String getTopic()
	{
		return topic;
	}
	
	public void setTopic(String topic)
	{
		this.topic = topic;
	}
	
	public String getFilterCriteria()
	{
		return filterCriteria;
	}
	
	public void setFilterCriteria(String filterCriteria)
	{
		this.filterCriteria = filterCriteria;
	}
	
	public Boolean getManualCommit() {
		return manualCommit;
	}


	public void setManualCommit(Boolean manualCommit) {
		this.manualCommit = manualCommit;
	}


	public Properties getConsumerProperties()
	{
		return consumerProperties;
	}
	
	public void setConsumerProperties(String consumerPropertiesJson) throws JsonParseException, JsonMappingException, IOException, Exception
	{
		if(StringUtils.isBlank(consumerPropertiesJson))
		{
			consumerPropertiesJson = KafkaGateway.getInstance().getKafkaBlueprints().getDefaultConsumerProperties();
		}
		
		//TODO Add logic to validate consumerPropertiesJson as JSON and print issue to log.
		this.consumerProperties = KafkaUtils.jsonToProperties(consumerPropertiesJson);
	}
	
	public void setKafkaConsumer(ResolveKafkaConsumer consumer)
	{
		this.consumer = consumer;
	}	
	
	public ResolveKafkaConsumer getKafkaConsumer()
	{
		return consumer;
	}
	
	public boolean containsBroker(String brokerUrls) throws Exception
	{
		List<String> brokers = KafkaUtils.parseBrokers(brokerUrls);
		for(String broker: brokers)
		{
			if(this.brokerUrls.contains(broker))
			{
				return true;
			}
		}		
		return false;
	}
	
}

