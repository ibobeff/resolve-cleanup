package com.resolve.gateway.kafka;

import java.io.File;
import java.util.HashMap;
import java.util.Map;

import org.apache.commons.io.FileUtils;

import com.resolve.gateway.Filter;
import com.resolve.rsbase.MainBase;
import com.resolve.rsremote.ConfigReceiveGateway;
import com.resolve.util.Log;
import com.resolve.util.ObjectProperties;
import com.resolve.util.StringUtils;
import com.resolve.util.XDoc;

public class ConfigReceiveKafka  extends ConfigReceiveGateway
{

    private static final long serialVersionUID = 1L;

    private static final String RECEIVE_KAFKA_NODE = "./RECEIVE/KAFKA/";
    private static final String RECEIVE_KAFKA_FILTER = RECEIVE_KAFKA_NODE + "FILTER";
    private static final String RECEIVE_DEFAULT_PRODUCER_PROPERTIES = RECEIVE_KAFKA_NODE + "@DEFAULTPRODUCERPROPERTIES";
    private static final String RECEIVE_DEFAULT_CONSUMER_PROPERTIES = RECEIVE_KAFKA_NODE + "@DEFAULTCONSUMERPROPERTIES";
    private static final String CLUSTERNAME = "./GENERAL/@CLUSTERNAME";

	private String queue = "KAFKA";
	private String defaultProducerProperties = "";
	private String defaultConsumerProperties = "";
	private String clusterName = "";
	
    public String getQueue()
    {
        return queue;
    }

    public void setQueue(String queue)
    {
        this.queue = queue;
    }
    
    public void setDefaultProducerProperties(String defaultProducerProperties)
    {
    	this.defaultProducerProperties = defaultProducerProperties;
    }
    
    public String getDefaultProducerProperties()
    {
    	return defaultProducerProperties;
    }
    
    public void setDefaultConsumerProperties(String defaultConsumerProperties)
    {
    	this.defaultConsumerProperties = defaultConsumerProperties;
    }
    
    public String getDefaultConsumerProperties()
    {
    	return defaultConsumerProperties;
    }
    
    public void setClusterName(String clusterName)
    {
    	this.clusterName = clusterName;
    }
    
    public String getClusterName()
    {
    	return clusterName;
    }
	
	public ConfigReceiveKafka(XDoc config) throws Exception
    {
        super(config);
        define("defaultProducerProperties", STRING, RECEIVE_DEFAULT_PRODUCER_PROPERTIES);
        define("defaultConsumerProperties", STRING, RECEIVE_DEFAULT_CONSUMER_PROPERTIES);
        define("clusterName", STRING, CLUSTERNAME);

    }
	
    @Override
    public String getRootNode()
    {
        return RECEIVE_KAFKA_NODE;
    }

    @Override
    public void load() throws Exception
    {
        try
        {
            loadAttributes();
            
            if (isActive()) // no need to unnecessarily load anything if the gateway
                            // is inactive.
            {
                KafkaGateway kafkaGateway = KafkaGateway.getInstance(this);

                // filters
                filters = xdoc.getListMapValue(RECEIVE_KAFKA_FILTER);
                
                

                // initialize filters
                if (filters.size() > 0)
                {
                    for (Map<String, Object> values : filters)
                    {
                        //it's important to make it lowercase because we store the files in lowercase
                        //without lowering it, linux systems have problem of not reading the script content.
                        String filterName = ((String)values.get(KafkaFilter.ID)).toLowerCase();
                        File scriptFile = getFile(MainBase.main.release.serviceName + "/config/kakfa/" + filterName + ".groovy");

                        try
                        {
                            if (scriptFile.exists())
                            {
                                String groovy = FileUtils.readFileToString(scriptFile, "UTF-8");
                                if (StringUtils.isNotBlank(groovy))
                                {
                                    values.put(KafkaFilter.SCRIPT, groovy);
                                    Log.log.debug("Loaded groovy script: " + scriptFile);
                                }
                            }

                            // init filter
                            kafkaGateway.setFilter(values);
                        }
                        catch (Exception e)
                        {
                            Log.log.warn(e.getMessage() + ". Skipping file: " + scriptFile.getAbsolutePath());
                        }
                    }
                }
            }
        }
        catch (Throwable e)
        {
            //deactivate it
            setActive(false);
            Log.log.error("Couldn not load configuration for Kafka gateway, check blueprint. " + e.getMessage(), e);
        }
        
    }
    
    @Override
    public void save() throws Exception
    {

        // create kafka directory
        File dir = getFile(MainBase.main.release.serviceName + "/config/kafka");
        if (!dir.exists())
        {
            FileUtils.forceMkdir(dir);
        }
        
        // clear list of filters to save
        if (filters != null && isActive())
        {
            filters.clear();

            // save sql files
            for (Filter filter : KafkaGateway.getInstance().getFilters().values())
            {
                KafkaFilter kafkaFilter = (KafkaFilter) filter;

                String groovy = kafkaFilter.getScript();
                
                String scriptFileName;
                File scriptFile = null;
                
                if (StringUtils.isNotBlank(groovy))
                {
                    scriptFileName = kafkaFilter.getId().toLowerCase() + ".groovy";
                    scriptFile = getFile(MainBase.main.release.serviceName + "/config/kafka/" + scriptFileName);
                }
                
                // add xdoc entry
                Map<String, Object> entry = new HashMap<String, Object>();

				putFilterDataIntoEntry(entry, kafkaFilter);
                
                filters.add(entry);

                if (scriptFile != null)
                {
                    try
                    {
                        // create groovy file
                        FileUtils.writeStringToFile(scriptFile, groovy, "UTF-8");
                    }
                    catch (Exception e)
                    {
                        Log.log.warn(e.getMessage() + ". Skipping file: " + scriptFile.getAbsolutePath());
                    }
                }
            }

            // filters
            xdoc.setListMapValue(RECEIVE_KAFKA_FILTER, filters);
        }

        // clear list of properties to save
        if (nameProperties != null && isActive())
        {
            // empty properties
            nameProperties.clear();

            // recreate properties
            for (String name : KafkaGateway.getInstance().getNameProperties().keySet())
            {
                // add xdoc entry
                Map<String, Object> entry = new HashMap<String, Object>();
                entry.put(NAME_KEY, name);
                nameProperties.add(entry);

                Map props = KafkaGateway.getInstance().getNameProperties().getNameProperties(name);
                ObjectProperties prop = new ObjectProperties(props);
                prop.save(props);
            }
        }
        saveAttributes();
    }

	private void putFilterDataIntoEntry(Map<String, Object> entry, KafkaFilter kafkaFilter)
    {
        entry.put(KafkaFilter.ID, kafkaFilter.getId());
        entry.put(KafkaFilter.ACTIVE, String.valueOf(kafkaFilter.isActive()));
        entry.put(KafkaFilter.ORDER, String.valueOf(kafkaFilter.getOrder()));
        entry.put(KafkaFilter.INTERVAL, String.valueOf(kafkaFilter.getInterval()));
        entry.put(KafkaFilter.EVENT_EVENTID, kafkaFilter.getEventEventId());
        entry.put(KafkaFilter.RUNBOOK, kafkaFilter.getRunbook());
        entry.put(KafkaFilter.SCRIPT, kafkaFilter.getScript());
        entry.put(KafkaFilter.BROKERURLS, KafkaUtils.brokerListToString(kafkaFilter.getBrokerUrls()));
        entry.put(KafkaFilter.TOPIC, kafkaFilter.getTopic());
        entry.put(KafkaFilter.FILTERCRITERIA, kafkaFilter.getFilterCriteria());
        entry.put(KafkaFilter.CONSUMERPROPERTIES, KafkaUtils.propertiesToJson(kafkaFilter.getConsumerProperties()));
    }
	

}

