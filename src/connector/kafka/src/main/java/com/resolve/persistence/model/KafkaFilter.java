package com.resolve.persistence.model;

import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Lob;
import javax.persistence.Table;
import javax.persistence.UniqueConstraint;

import org.apache.commons.lang3.StringUtils;

import com.resolve.services.interfaces.VO;
import com.resolve.services.hibernate.vo.KafkaFilterVO;

@Entity
@Table(name = "kafka_filter", uniqueConstraints = { @UniqueConstraint(columnNames = { "u_name", "u_queue" }) })
public class KafkaFilter extends GatewayFilter<KafkaFilterVO>
{
    private static final long serialVersionUID = 1L;
    private String UTopic;
    private String UBrokerUrls;
    private String UFilterCriteria;
    private String UConsumerProperties;
    private Boolean UManualCommit = false;
    
    // object referenced by
    public KafkaFilter()
    {
    }

    public KafkaFilter(KafkaFilterVO vo)
    {
        applyVOToModel(vo);
    }

    @Column(name = "u_topic")
    public String getUTopic()
    {
    	return this.UTopic;
    }    
    public void setUTopic(String uTopic)
    {
    	this.UTopic = uTopic;
    }
    
    @Column(name = "u_broker_urls")
    public String getUBrokerUrls()
    {
    	return this.UBrokerUrls;
    }
    public void setUBrokerUrls(String uBrokerUrls)
    {
    	this.UBrokerUrls = uBrokerUrls;
    }
    
    @Column(name = "u_filter_criteria", length = 4000)
    public String getUFilterCriteria()
    {
    	return this.UFilterCriteria;
    }
    public void setUFilterCriteria(String uFilterCriteria)
    {
    	this.UFilterCriteria = uFilterCriteria;
    }
    
    @Column(name="u_manual_commit")
    public Boolean getUManualCommit() {
		return UManualCommit;
	}

	public void setUManualCommit(Boolean uManualCommit) {
		UManualCommit = uManualCommit;
	}

	@Column(name = "u_consumer_properties", length = 4000)
    public String getUConsumerProperties()
    {
    	return this.UConsumerProperties;
    }
    public void setUConsumerProperties(String uConsumerProperties)
    {
    	this.UConsumerProperties = uConsumerProperties;
    }
    
    
    public KafkaFilterVO doGetVO()
    {
        KafkaFilterVO vo = new KafkaFilterVO();
        super.doGetBaseVO(vo);
        vo.setuTopic(getUTopic());
        vo.setuBrokerUrls(getUBrokerUrls());
        vo.setuFilterCriteria(getUFilterCriteria());
        vo.setuManualCommit(getUManualCommit());
        vo.setuConsumerProperties(getUConsumerProperties());
        return vo;
    }

    @Override
    public void applyVOToModel(KafkaFilterVO vo)
    {
        if (vo != null)
        {
        	super.applyVOToModel(vo);
        	if(StringUtils.isNotBlank(vo.getuTopic()) && vo.getuTopic().equals(VO.STRING_DEFAULT))
        	{
        		this.setUTopic(getUTopic());
        	}
        	else
        	{
        		this.setUTopic(vo.getuTopic());
        	}
        	
        	if(StringUtils.isNotBlank(vo.getuBrokerUrls()) && vo.getuBrokerUrls().equals(VO.STRING_DEFAULT))
        	{
        		this.setUBrokerUrls(getUBrokerUrls());
        	}
        	else
        	{
        		this.setUBrokerUrls(vo.getuBrokerUrls());
        	}
        	
        	if(StringUtils.isNotBlank(vo.getuFilterCriteria()) && vo.getuFilterCriteria().equals(VO.STRING_DEFAULT))
        	{
        		this.setUFilterCriteria(getUFilterCriteria());
        	}
        	else
        	{
        		this.setUFilterCriteria(vo.getuFilterCriteria());
        	}
        	
        	if(vo.getuManualCommit() !=  null)
        	{
        		this.setUManualCommit(vo.getuManualCommit());
        	}
        	else
        	{
        		this.setUManualCommit(new Boolean(false));
        	}
        	
        	if(StringUtils.isNotBlank(vo.getuConsumerProperties()) && vo.getuConsumerProperties().equals(VO.STRING_DEFAULT))
        	{
        		this.setUConsumerProperties(getUConsumerProperties());
        	}
        	else
        	{
        		this.setUConsumerProperties(vo.getuConsumerProperties());
        	}
        }
    }

	@Override
    public List<String> ugetCdataProperty()
    {
        List<String> list = super.ugetCdataProperty();
        return list;

    }
}


