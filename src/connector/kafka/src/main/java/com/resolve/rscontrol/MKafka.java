package com.resolve.rscontrol;

import com.resolve.persistence.model.KafkaFilter;

public class MKafka extends MGateway
{
    private static final String MODEL_NAME = KafkaFilter.class.getSimpleName();

    protected String getModelName()
    {
        return MODEL_NAME;
    }
}


