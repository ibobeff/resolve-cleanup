package com.resolve.gateway;

import java.util.Map;

import com.resolve.gateway.kafka.KafkaFilter;
import com.resolve.gateway.kafka.KafkaGateway;
import com.resolve.gateway.kafka.KafkaUtils;

public class MKafka extends MGateway
{
    private static final String RSCONTROL_SET_FILTERS = "MKafka.setFilters";

    private static final KafkaGateway instance = KafkaGateway.getInstance();

    public MKafka()
    {
        super.instance = instance;
    }
    
    protected String getSetFilterMethodName()
    {
        return RSCONTROL_SET_FILTERS;
    }

    /**
     * Populates the {@link Filter} data into the given filterMap {@link Map} instance
     * 
     * @param filterMap
     *      the {@link Map} instance to hold the {@link Filter} data
     * @param filter
     *      a {@link KafkaFilter} instance
     *            
     */
    
    public void populateGatewaySpecificFilterProperties(Map<String, String> filterMap, Filter filter)
    {
        if (instance.isActive())
        {
            KafkaFilter kafkaFilter = (KafkaFilter) filter;
            filterMap.put(KafkaFilter.BROKERURLS, KafkaUtils.brokerListToString(kafkaFilter.getBrokerUrls()));
            filterMap.put(KafkaFilter.TOPIC, kafkaFilter.getTopic());
            filterMap.put(KafkaFilter.FILTERCRITERIA, kafkaFilter.getFilterCriteria());
            filterMap.put(KafkaFilter.CONSUMERPROPERTIES, KafkaUtils.propertiesToJson(kafkaFilter.getConsumerProperties()));
            filterMap.put(KafkaFilter.MANUALCOMMIT, (kafkaFilter.getManualCommit()).toString());
        }
    } // populateGatewaySpecificProperties
}

