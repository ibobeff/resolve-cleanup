package com.resolve.gateway.kafka;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashSet;
import java.util.List;
import java.util.Properties;
import java.util.Set;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.TimeUnit;

import org.apache.kafka.clients.consumer.ConsumerConfig;
import org.apache.kafka.clients.consumer.KafkaConsumer;
import com.resolve.util.Log;

public class ConsumerStore {
	
	private ConcurrentHashMap<Properties, ResolveKafkaConsumer> store;
	
	public ConsumerStore()
	{
		store = new ConcurrentHashMap<>();
	}
	
	public ResolveKafkaConsumer getConsumer(Properties properties)
	{
		return store.get(properties);	
	}
	
	public ResolveKafkaConsumer createConsumer(List<String> brokerUrls, String topic, Properties properties, String filterId, Boolean manualCommit)
	{
		ArrayList<String> topics = new ArrayList<>();
		topics.add(topic);
		return createConsumer(brokerUrls, topics, properties, filterId,manualCommit);
	}
	
	public ResolveKafkaConsumer createConsumer(List<String> brokerUrls, Collection<String> topics, Properties properties, String filterId, Boolean manualCommit)
	{	
		Log.log.debug(String.format("createConsumer(). BrokerURLs: %s, Topics: %s, Properties: %s", brokerUrls.toString(), topics.toString(), properties.toString()));
		topics = new HashSet<String>(topics); //Convert to a set to remove duplicates		
		properties.put("bootstrap.servers", KafkaUtils.brokerListToString(brokerUrls));
		properties.put("enable.auto.commit", !manualCommit);
		ResolveKafkaConsumer resolveKafkaConsumer = getConsumer(properties);	
		
		if(resolveKafkaConsumer == null)
		{		
			resolveKafkaConsumer = new ResolveKafkaConsumer(properties);
		}
		else
		{
			topics.addAll(resolveKafkaConsumer.getConsumer().subscription());
		}
		
		resolveKafkaConsumer.getConsumer().subscribe(topics);
		resolveKafkaConsumer.addFilterId(filterId);
		store.put(properties, resolveKafkaConsumer);
		
		return resolveKafkaConsumer;
	}
	
	public void stopConsumerRunnables()
	{
		for(ResolveKafkaConsumer consumerRunnable: store.values())
		{
			consumerRunnable.stopPolling();
		}
		store = new ConcurrentHashMap<>();
	}
	
	public void startConsumerRunnables()
	{
		for(ResolveKafkaConsumer consumerRunnable: store.values())
		{
			new Thread(consumerRunnable).start();
		}
	}
	
	public ResolveKafkaConsumer getConsumer(String filterId)
	{
		for(ResolveKafkaConsumer consumer: store.values())
		{
			if(consumer.getFilterIds().contains(filterId))
			{
				return consumer;
			}
		}
		
		return null;
	}
	
	public int getConsumerCount()
	{
		return store.size();
	}
}
