package com.resolve.services.hibernate.vo;

import com.resolve.util.MappingAnnotation;

public class KafkaFilterVO extends GatewayFilterVO
{

    private static final long serialVersionUID = 1L;
    
    private String UTopic;
    private String UBrokerUrls;
    private String UFilterCriteria;
    private Boolean UManualCommit;
    private String UConsumerProperties;
    
    public KafkaFilterVO()
    {
    }
    
    @MappingAnnotation(columnName = "TOPIC")
    public String getuTopic() {
        return this.UTopic;
    }

    public void setuTopic(String uTopic) {
        this.UTopic = uTopic;
    }
    
    @MappingAnnotation(columnName = "BROKERURLS")
    public String getuBrokerUrls() {
        return this.UBrokerUrls;
    }

    public void setuBrokerUrls(String uBrokerUrls) {
        this.UBrokerUrls = uBrokerUrls;
    }
    
    @MappingAnnotation(columnName = "FILTERCRITERIA")
    public String getuFilterCriteria()
    {
    	return this.UFilterCriteria;
    }
    
    public void setuFilterCriteria(String uFilterCriteria)
    {
    	this.UFilterCriteria = uFilterCriteria;
    }
    
    @MappingAnnotation(columnName="MANUALCOMMIT")
    public Boolean getuManualCommit() {
		return UManualCommit;
	}

	public void setuManualCommit(Boolean uManualCommit) {
		this.UManualCommit = uManualCommit;
	}

	@MappingAnnotation(columnName = "CONSUMERPROPERTIES")
    public String getuConsumerProperties()
    {
    	return this.UConsumerProperties;
    }
    
    public void setuConsumerProperties(String uConsumerProperties)
    {
    	this.UConsumerProperties = uConsumerProperties;
    }
    
    @Override
    public boolean equals(Object obj)
    {
        if (this == obj) return true;
        if (!super.equals(obj)) return false;
        if (getClass() != obj.getClass()) return false;
        KafkaFilterVO other = (KafkaFilterVO) obj;
        
        return true;
    }
    
    @Override
    public int hashCode()
    {
        final int prime = 31;
        int result = super.hashCode();
        result = prime * result + ((UTopic == null) ? 0 : UTopic.hashCode());
        result = prime * result + ((UBrokerUrls == null) ? 0 : UBrokerUrls.hashCode());
        result = prime * result + ((UFilterCriteria == null) ? 0 : UFilterCriteria.hashCode());
        result = prime * result + ((UManualCommit == null) ? 0 : UManualCommit.hashCode());
        result = prime * result + ((UConsumerProperties == null) ? 0 : UConsumerProperties.hashCode());
        return result;
    }
}

