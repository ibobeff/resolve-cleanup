package com.resolve.gateway.splunkpull;

import java.util.Map;

import java.util.List;
import java.util.ArrayList;
import org.apache.log4j.MDC;

import com.resolve.rsbase.MainBase;
import com.resolve.gateway.BaseClusteredGateway;
import com.resolve.gateway.BaseFilter;
import com.resolve.gateway.Filter;
import com.resolve.gateway.MSplunkPull;
import com.resolve.gateway.splunkpull.SplunkPullAPI.TimeFrame;
import com.resolve.util.Log;
import com.resolve.util.StringUtils;
import com.splunk.Service;
import com.resolve.rsremote.ConfigReceiveGateway;

public class SplunkPullGateway extends BaseClusteredGateway {

    private static volatile SplunkPullGateway instance = null;

    private Service splunkService = null;

    private String queue = null;

    public static SplunkPullGateway getInstance(ConfigReceiveSplunkPull config) {
        if (instance == null) {
            instance = new SplunkPullGateway(config);
        }
        return instance;
    }

    /**
     * This method is called once the initialization is done. If it gets called
     * before the initialization it will throw exception.
     *
     * @return
     */
    public static SplunkPullGateway getInstance() {
        if (instance == null) {
            throw new RuntimeException("SplunkPullGateway: Splunk Gateway is not initialized correctly..");
        } else {
            return instance;
        }
    }

    private SplunkPullGateway(ConfigReceiveSplunkPull config) {
        super(config);
    }

    @Override
    public String getLicenseCode() {
        return "SPLUNKPULL";
    }

    @Override
    protected void addAdditionalValues(Map<String, Object> arg0, Map<String, String> arg1) {
    }

    @Override
    protected String getGatewayEventType() {
        return "SPLUNKPULL";
    }

    @Override
    protected String getMessageHandlerName() {
        return MSplunkPull.class.getSimpleName();
    }

    @Override
    protected Class<MSplunkPull> getMessageHandlerClass() {
        return MSplunkPull.class;
    }

    public String getQueueName() {
        return queue + getOrgSuffix();
    }
    
    public Service getSplunkService()
    {
        if(splunkService == null) {
            try {
            	Log.log.info("SplunkPullGateway: Authenticating SplunkPull Gateway");
                splunkService = authenticate((ConfigReceiveSplunkPull) configurations);
            } catch(Exception e) {
            	Log.log.error("SplunkPullGateway: Authentication failed to get Splunk service");
                Log.log.error(e.getMessage());
            }
        }
        
        return splunkService;
    }

    public void setSplunkService(Service splunkService)
    {
        this.splunkService = splunkService;
    }

    private static Service authenticate(ConfigReceiveSplunkPull config) throws Exception {

        return SplunkPullAPI.authenticate(config.getIp(), config.getPort(), config.getUsername(), config.getPassword(), config.getProtocol());
    }
    
    @Override
    public void start() {
    	Log.log.debug("SplunkPullGateway:******************* Starting SplunkPull Gateway **************************");
        super.start();
    }

    @Override
    public void run() {
        if (!((ConfigReceiveGateway) configurations).isMock()) super.sendSyncRequest();
        while (running) {
            try {
                long startTime = System.currentTimeMillis();
                
                /* if any remaining events are in primary data queue or 
                 * primary data executor has waiting messages still to be processed, 
                 * do not get messages for more new events.
                 */
                
                if (primaryDataQueue.isEmpty() && !primaryDataQueueExecutorBusy()) {
                    Log.log.trace("SplunkPullGateway: " +getQueueName() + " Primary Data Queue is empty and Primary Data Queue Executor is free.....");
                    for (Filter filter : orderedFilters) {
                        try {
                            MDC.put("filter", filter.getId());
                            if (shouldFilterExecute(filter, startTime)) {
                                Log.log.trace("SplunkPullGateway: Filter " + filter.getId() + " executing");
                                List<Map<String, String>> results = invokeService(filter);
                                for (Map<String, String> result : results) {
                                    addToPrimaryDataQueue(filter, result);
                                }
                            } else {
                                Log.log.trace("SplunkPullGateway: Filter -->" + filter.getId() + " is inactive or not ready to execute yet.....");

                            }
                        } finally {
                            MDC.put("filter", "");
                        }
                    }
                    if (orderedFilters == null || orderedFilters.size() == 0) {
                        Log.log.trace("SplunkPullGateway: There is not deployed filter for SplunkGateway");                    }
                }
                if (orderedFilters.size() == 0) {
                    Thread.sleep(interval);
                }
            } catch (Exception e) {
                Log.log.error("SplunkPullGateway: Warning: " + e.getMessage());
                Log.alert("SplunkPullGateway: Instance " + MainBase.main.configId.getGuid() + " " + getLicenseCode() + "-" + getQueueName() + "-" + getInstanceType() + " Polling Failed", "Instance " + MainBase.main.configId.getGuid() + " " + getLicenseCode() + "-" + getQueueName() + "-" + getInstanceType() + " Polling Failed " + "due to " + e.getMessage());
                try {
                    Thread.sleep(interval);
                } catch (InterruptedException ie) {
                    Log.log.warn("SplunkPullGateway: sleep interrupted: " + ie.getMessage(), ie);
                }
            }
        }
    }

    /**
     * Add customized code here to get the data as List<Map<String, String>> format for each filter 
     * @return List<Map<String, String>>
     */
    public List<Map<String, String>> invokeService(Filter filter) {

        //Add customized code here to get data from 3rd party system based the information in the filter;
        
        String query = ((SplunkPullFilter) filter).getQuery();
        String type = ((SplunkPullFilter) filter).getType();
        Log.log.debug("SplunkPullGateway: Pull filter invoked");
        if(StringUtils.isBlank(query) || StringUtils.isBlank(type)) {
            Log.log.error("SplunkPullGateway: No criteria is specified.");
            return null;
        }
        
        List<Map<String, String>> events = new ArrayList<Map<String, String>>();
        
        try {
            if(type.equals("Saved Search")) {
            	Log.log.debug("SplunkPullGateway: Saved Search API is called with a time frame of "+((SplunkPullFilter)filter).getTimeFrame()+ " and time modifier of " + ((SplunkPullFilter)filter).getTimeModifier());
            	  events = SplunkPullAPI.savedSearchWithTimeFrame(query, ((SplunkPullFilter) filter).getTimeout(), ((SplunkPullFilter) filter).getMaxNumOfResult(),getTimeFrame(((SplunkPullFilter) filter).getTimeFrame()),((SplunkPullFilter) filter).getTimeModifier());
            }
            
            else {
            	Log.log.debug("SplunkPullGateway: Normal Search API is called");
                events = SplunkPullAPI.normalSearch(query, ((SplunkPullFilter) filter).getTimeout());
            } 
        }
        catch (Exception e)
        {
        	Log.log.error("SplunkPullGateway: API call failed with errors");
            Log.log.error(e.getMessage(), e);
        }
        
        List<Map<String, String>> result = new ArrayList<Map<String, String>>();
        if(events != null) {
        	Log.log.debug("SplunkPullGateway: API call has retrieved --> " + events.size() + " results");
            result.addAll(events);
        }
        
        return result;
    }

    /**
     * Add customized code here to initilize the connection with 3rd party system
     * @return List<Map<String, String>>
     */
    @Override
    protected void initialize() {
        ConfigReceiveSplunkPull config = (ConfigReceiveSplunkPull) configurations;
        queue = config.getQueue().toUpperCase();
        this.gatewayConfigDir = "/config/splunk/";
        
        try {
            splunkService = authenticate(config);
        } catch(Exception e) {
            Log.log.error("SplunkPullGateway: ERROR --> " + e.getMessage() + ", Check your username and password");
        }
        Log.log.info("************** SplunkPullGateway Initialization complete *********************");    }
   
    /**
     * Add customized code here to do when stop the gateway
     */
    @Override
    public void stop() {
        Log.log.info("*********************** Stopping SplunkPull gateway ***************************");        //Add customized code here to stop the connection with 3rd party system;
        super.stop();
    }

    @Override
    public Filter getFilter(Map<String, Object> params) {
        return new SplunkPullFilter((String) params.get(BaseFilter.ID), (String) params.get(BaseFilter.ACTIVE), (String) params.get(BaseFilter.ORDER), (String) params.get(BaseFilter.INTERVAL), (String) params.get(BaseFilter.EVENT_EVENTID), (String) params.get(BaseFilter.RUNBOOK), (String) params.get(BaseFilter.SCRIPT), (String) params.get(SplunkPullFilter.MAXNUMOFRESULT), (String) params.get(SplunkPullFilter.QUERY), (String) params.get(SplunkPullFilter.TYPE), (String) params.get(SplunkPullFilter.TIMEOUT),(String)params.get(SplunkPullFilter.TIMEFRAME),(String)params.get(SplunkPullFilter.TIMEMODIFIER));
    }
    
    private SplunkPullAPI.TimeFrame getTimeFrame(String timeFrame){
      
      switch(timeFrame) {
        case "All time"         : return TimeFrame.AllTime;
        case "1 minute window"  : return TimeFrame.One_minute_window;
        case "5 minute window"  : return TimeFrame.Five_min_window;
        case "Last 15 minutes"  : return TimeFrame.Last_15_mins;
        case "Last 60 minutes"  : return TimeFrame.Last_60_mins;
        case "Last 4 hours"     : return TimeFrame.Last_4_hours;
        case "Last 24 hours"    : return TimeFrame.Last_24_hours;
        case "Last 7 days"      : return TimeFrame.Last_7_Days;
        case "Last 30 days"     : return TimeFrame.Last_30_days;
        case "Custom Relative"  : return TimeFrame.Custom_Relative;
        case "Custom Absolute"  : return TimeFrame.Custom_Absolute;
        default                 : return TimeFrame.AllTime;
    }
   }
}

