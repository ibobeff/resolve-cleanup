package com.resolve.gateway.splunkpull;

import java.io.InputStream;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.resolve.gateway.AbstractGatewayAPI;
import com.resolve.util.Log;
import com.resolve.util.StringUtils;
import com.splunk.Job;
import com.splunk.JobArgs;
import com.splunk.ResultsReaderXml;
import com.splunk.SSLSecurityProtocol;
import com.splunk.SavedSearch;
import com.splunk.SavedSearchDispatchArgs;
import com.splunk.Service;
import com.splunk.ServiceArgs;


public class SplunkPullAPI extends AbstractGatewayAPI
{    
    public enum TimeFrame {
      AllTime,One_minute_window,Five_min_window,Last_15_mins,Last_60_mins,Last_4_hours,Last_24_hours,Last_7_Days,Last_30_days,Custom_Relative,Custom_Absolute
    }
  
    /**
     * To establish connection with Splunk server with host ip, port and HTTP or HTTPS information and user credential and receive a Splunk service reference
     * @param host: the host ip 
     * @param port: the port number
     * @param username
     * @param password
     * @param protocol: the protocol for HTTPS or "" for HTTP
     * @return Service: the Service reference of Splunk connection
     * @throws Exception
     */
    public static Service authenticate(String host, int port, String username, String password, String protocol) throws Exception {
        
        ServiceArgs loginArgs = new ServiceArgs();
        
        loginArgs.setHost(host);
        loginArgs.setPort(port);
        loginArgs.setUsername(username);
        loginArgs.setPassword(password);
        
        if(StringUtils.isNotBlank(protocol))
            Service.setSslSecurityProtocol(SSLSecurityProtocol.valueOf(protocol));
        else
            Service.setSslSecurityProtocol(SSLSecurityProtocol.TLSv1);
        
        return Service.connect(loginArgs);
    }
    
    /**
     * To retrieve results from Splunk based on the saved search being defined using system configuration
     * @param query: the name of the saved search
     * @param timeout: how many seconds to wait before the connection is closed
     * @param maxNumberOfResults: the maximum number fo results to be retrieved from the saved Search
     * @throws Exception
     */
    public static List<Map<String, String>> savedSearch(String query, int timeout, int maxNumberOfResults) throws Exception {
        
        SplunkPullGateway gateway = SplunkPullGateway.getInstance();
        Service splunkService = gateway.getSplunkService();
        
        return savedSearch(splunkService, query, timeout, maxNumberOfResults, true);
    }
    
    /**
     * To retrieve results from Splunk based on the saved search being defined, given user credential and connection information
     * @param host: the host ip 
     * @param port: the port number
     * @param username
     * @param password
     * @param protocol: the protocol for HTTPS or "" for HTTP
     * @param query: the name of the saved search
     * @param timeout: how many seconds to wait before the connection is closed
     * @param maxNumberOfResults: the maximum number fo results to be retrieved from the saved Search
     * @return List<Map<String, String>>
     * @throws Exception
     */
    public static List<Map<String, String>> savedSearch(String host, int port, String username, String password, String protocol, String query, int timeout, int maxNumberOfResults) throws Exception {
    	Service splunkService = null;
    	try {
    		splunkService = authenticate(host, port, username, password, protocol);
    	}catch(Exception ex) {
    		Log.log.error("SplunkPullGateway: Authentication failure to Splunk when making "
    				+ "Saved search call --> " + ex.getMessage());
    		return null;
    	}
    	if(splunkService != null) {
    		return savedSearch(splunkService, query, timeout, maxNumberOfResults, false);
    	}else {
    		return null;
    	}
        
    }
    
    /**
     * To retrieve results from Splunk based on the saved search being defined, given the available service reference from Splunk after authentication
     * @param splunkService: the Service reference of Splunk connection after authentication that can be cached in the SplunkGateway for system user or the client side with non-system user
     * @param query: search criteria
     * @param timeout: how many seconds to wait before the connection is closed
     * @param maxNumberOfResults: the maximum number fo results to be retrieved from the saved Search
     * @param systemUser: true if the user is system user, false if not
     * @return List<Map<String, String>>
     * @throws Exception
     */
    public static List<Map<String, String>> savedSearch(Service splunkService, String query, int timeout, int maxNumberOfResults, boolean systemUser) throws Exception {
      return savedSearch(splunkService,query,timeout,maxNumberOfResults,systemUser,TimeFrame.AllTime,"");
    }
    
    /**
     * To retrieve results from Splunk based on the saved search being defined, given the available service reference from Splunk after authentication
     * @param splunkService: the Service reference of Splunk connection after authentication that can be cached in the SplunkGateway for system user or the client side with non-system user
     * @param query: search criteria
     * @param timeout: how many seconds to wait before the connection is closed
     * @param maxNumberOfResults: the maximum number fo results to be retrieved from the saved Search
     * @param systemUser: true if the user is system user, false if not
     * @return List<Map<String, String>>
     * @throws Exception
     */
    private static List<Map<String, String>> savedSearch(Service splunkService, String query, int timeout, int maxNumberOfResults, boolean systemUser,TimeFrame timeFrame,String timeModifier) throws Exception {
        
        if(splunkService == null)
            throw new Exception("Missing Service information.");
        
        List<Map<String, String>> events = new ArrayList<Map<String, String>>();
        Log.log.debug("SplunkPullGateway: calling saved search with query --> " + query);
        Job jobSavedSearch = null;
        ResultsReaderXml resultsReader = null;
        SavedSearch savedSearch = null;
        
        try {          
            try {
                savedSearch = splunkService.getSavedSearches().get(query);
                Log.log.debug("SplunkPullGateway: Saved search Call successful ");
            } catch(Exception ee) {
                if(systemUser) {
                	Log.log.error("SplunkPullGateway: Saved search failed getting gateway instance ");
                    SplunkPullGateway gateway = SplunkPullGateway.getInstance();
                    splunkService = gateway.getSplunkService();
                    savedSearch = splunkService.getSavedSearches().get(query);
                }
            }
            
            if(savedSearch == null) {
                Log.log.error("SplunkPullGateway: Saved Search query -->" + query + ", results not found.");                throw new Exception("Saved Search not found.");
            }
            Log.log.debug("SplunkPullGateway: Saved search return results --> " + savedSearch.toString());
            Log.log.info("SplunkPullGateway: Waiting for the job to finish...");

            SavedSearchDispatchArgs dispatchArgs = new SavedSearchDispatchArgs();
            dispatchArgs.setDispatchMaximumCount(maxNumberOfResults);
           
            //Logic to build time based args
            if(!timeFrame.equals(TimeFrame.AllTime)) {
                if(timeFrame.equals(TimeFrame.Custom_Relative)||timeFrame.equals(TimeFrame.Custom_Absolute)) {
                  if(StringUtils.isNotBlank(timeModifier)) {
                    String[] timeFrames = parseCustomTimeRanges(timeModifier);
                     if(timeFrames[0]!=null) {
                       if(timeFrame.equals(TimeFrame.Custom_Absolute))
                         dispatchArgs.setDispatchTimeFormat("%m/%d/%Y:%H:%M:%S");
                       dispatchArgs.setDispatchEarliestTime(timeFrames[0]);
                       if(StringUtils.isNotBlank(timeFrames[1])) dispatchArgs.setDispatchLatestTime(timeFrames[1]);
                     }else {
                       Log.log.warn("SplunkPullGateway: Time frame is set to Custom but no Time Modifier entered on the Filter. Defaulting to All Time");
                     }
                }else {
                  Log.log.warn("SplunkPullGateway: Time frame is set to Custom but no Time Modifier entered on the Filter. Defaulting to All Time");
                }
              }else {
                dispatchArgs.setDispatchEarliestTime(getFormattedTimeFrame(timeFrame));
              }
            }    
            
            jobSavedSearch = savedSearch.dispatch(dispatchArgs);

            long startTime = System.currentTimeMillis();
            
            while (!jobSavedSearch.isDone()) {
                long currentTime = System.currentTimeMillis();
                
                if(timeout != 0 && (startTime + timeout*1000) < currentTime)
                    throw new Exception("SplunkPullGateway: Search execution exceeded specified timeout.");
                else
                {
                    try {
                        Thread.sleep(500);
                    } catch (InterruptedException e) {
                        Log.log.error(e.getMessage());
                        e.printStackTrace();
                    }
                }
             }
             
            Log.log.debug("SplunkPullGateway: Number of result --> " + jobSavedSearch.getResultCount());
             
             InputStream results = jobSavedSearch.getResults();
             resultsReader = new ResultsReaderXml(results);
             Map<String, String> event = new HashMap<String, String>();
             
             while ((event = resultsReader.getNextEvent()) != null) {
                 Map<String, String> result = new HashMap<String, String>();
                 
                 for (String key: event.keySet()) {
                     result.put(key,  event.get(key));
                     Log.log.debug("SplunkPullGateway: savedSearch Key -> " + key + ":EventVal -> " + event.get(key));
                 }
                 
                 events.add(result);
             }
             
             resultsReader.close();
             
        } catch (Exception e) {
            e.printStackTrace();
            Log.log.error(e.getMessage());
        } finally {
            try {
                if(resultsReader != null)
                    resultsReader.close();
            } catch(Exception e1) {
                e1.getMessage();
            }
        }

        return events;
    }
    
    /**
     * Ensure time modifier has correct format. 
     * Some valid formats:  
     * 1. earliest=10/19/2017:0:0:0 latest=10/27/2017:0:0:0
     * 2. earliest=-2d
     * 3. earliest=-2d@d 
     * 
     * Some invalid formats
     * 1. latest=-2d
     * 2. ealrier=+2d
     * 3. 10/19/2017:0:0:0 to 10/27/2017:0:0:0
     * 
     * @param timeModifier
     * @return
     */
    private static String[] parseCustomTimeRanges(String timeModifier) {
      
      timeModifier = timeModifier.trim();
      
      String[] parsedTimeModifiers = new String[2];
      try {
        if(StringUtils.contains(timeModifier,"earliest=")) {
     
           if(timeModifier.split("=")[0].equalsIgnoreCase("earliest")) 
                 parsedTimeModifiers[0] = timeModifier.split("=")[1].split(",")[0];
           
           
           if((timeModifier.split(",")).length==2 && timeModifier.split(",")[1].split("=")[0].equalsIgnoreCase("latest"))
             parsedTimeModifiers[1] = timeModifier.split(",")[1].split("=")[1];
          } 
        }catch(IndexOutOfBoundsException exception) {
          Log.log.warn("SplunkPullGateway: Error parsing time modifier on custom query. The provided time modifier is "+ timeModifier);
          throw exception;
        }
      return parsedTimeModifiers;
     }

    /*
     * Accepts TimeFrame enum and returns a pre-defined splunk time modifier
     */
    private static String getFormattedTimeFrame(TimeFrame timeFrame) {
     
      switch(timeFrame) {
        case One_minute_window: return "-m";
        case Five_min_window:   return "-5m";
        case Last_15_mins:      return "-15m";
        case Last_60_mins:      return "-60m";
        case Last_4_hours:      return "-4h@m";
        case Last_24_hours:     return "-24h@h";
        case Last_7_Days:       return "-7d@h";
        case Last_30_days:      return "-30d@d";
        default:                return "";
      }
    }

    /**
     * Returns search results within a time frame as a List of String Maps
     * @param query The name of the saved search query
     * @param timeout The timeout for the search job
     * @param maxNumberOfResults The max results you want to retrieve in one interval
     * @param timeFrame Accepts all values defined by enum {@link TimeFrame}    
     * @param timeModifier Only required if sending {@link TimeFrame#Custom_Absolute} or {@link TimeFrame#Custom_Relative} in timeFrame
     *        For {@link TimeFrame#Custom_Absolute} use the format earilest=Month/Day/Year:Hours:Minutes:Seconds,latest=Month/Day/Year:Hours:Minutes:Seconds
     *        For {@link TimeFrame#Custom_Relativ} use the format earliest=-2d@d,latest=-1h
     *        When latest is not set current time will be used. When format is not correct all events will be fetched without a timeFrame
     * @return
     * @throws Exception
     */
    public static List<Map<String, String>> savedSearchWithTimeFrame(String query, int timeout, int maxNumberOfResults,TimeFrame timeFrame, String timeModifier) throws Exception{
      
      SplunkPullGateway gateway = SplunkPullGateway.getInstance();
      Service splunkService = gateway.getSplunkService();
      
      return savedSearch(splunkService, query, timeout, maxNumberOfResults,true,timeFrame,timeModifier);
    }
    
    /**
     * To retrieve results from Splunk based on the query provided using system configuration
     * @param query: search criteria
     * @param timeout: how many seconds to wait before the connection is closed
     * @return List<Map<String, String>>
     * @throws Exception
     */
    public static List<Map<String, String>> normalSearch(String query, int timeout) throws Exception {
    
        SplunkPullGateway gateway = SplunkPullGateway.getInstance();
        Service splunkService = gateway.getSplunkService();
        
        return normalSearch(splunkService, query, timeout, true);
    }
    
    /**
     * To retrieve results from Splunk based on the query provided, given user credential
     * @param host: the host ip 
     * @param port: the port number
     * @param username
     * @param password
     * @param protocol: the protocol for HTTPS or "" for HTTP
     * @param query: search criteria
     * @param timeout: how many seconds to wait before the connection is closed
     * @return List<Map<String, String>>
     * @throws Exception
     */
    public static List<Map<String, String>> normalSearch(String host, int port, String username, String password, String protocol, String query, int timeout) throws Exception {
    	Service splunkService = null;
    	try {
    		splunkService = authenticate(host, port, username, password, protocol);
    	}catch(Exception ex) {
    		Log.log.error("SplunkPullGateway: Authentication failure to Splunk when making "
    				+ "Normal search call --> " + ex.getMessage());
    		return null;
    	}
    	if(splunkService != null) {
    		return normalSearch(splunkService, query, timeout, false);
    	}else {
    		return null;
    	}
    }
    
    /**
     * To retrieve results from Splunk based on the query provided, given the available service reference from Splunk after authentication
     * @param splunkService: the Service reference of Splunk connection after authentication that can be cached in the SplunkGateway for system user or the client side with non-system user
     * @param query: search criteria
     * @param timeout: how many seconds to wait before the connection is closed
     * @param systemUser: true if the user is system user, false if not
     * @return List<Map<String, String>>
     * @throws Exception
     */
    public static List<Map<String, String>> normalSearch(Service splunkService, String query, int timeout, boolean systemUser) throws Exception {

        List<Map<String, String>> events = new ArrayList<Map<String, String>>();
        Map<String, String> event = new HashMap<String, String>();
        
        Job job = null;
        JobArgs jobargs = new JobArgs();
        jobargs.setExecutionMode(JobArgs.ExecutionMode.NORMAL);
//      jobargs.add("args.mysourcetype", "splunkd");
        Log.log.debug("SplunkPullGateway: Normal search with Query --> " + query);
        try {
            try {
                job = splunkService.getJobs().create(query, jobargs);
                Log.log.debug("SplunkPullGateway: Normal search create job successful ");
            } catch(Exception ee) {
                if(systemUser) {
                	Log.log.debug("SplunkPullGateway: Normal search create job failed getting gateway instance ");
                    SplunkPullGateway gateway = SplunkPullGateway.getInstance();
                    splunkService = gateway.getSplunkService();
                    Log.log.debug("SplunkPullGateway: Normal search Making alternate call after failure");
                    job = splunkService.getJobs().create(query, jobargs);
                    Log.log.debug("SplunkPullGateway: Normal search calling splunkService.getJobs().create "
                    		                    		+ " with query --> " + query);
                    
                }
            }
            
            if(job == null) {
                Log.log.error("SplunkPullGateway: Failed to connect to Normal Search from Splunk.");
                throw new Exception("Normal Search not found.");
            }

            long startTime = System.currentTimeMillis();
            
            while (!job.isDone()) {
                long currentTime = System.currentTimeMillis();
                
                if(timeout != 0 && (startTime + timeout*1000) < currentTime)
                    throw new Exception("SplunkPullGateway: Search execution exceeded specified timeout.");
                else
                {
                    try {
                        Thread.sleep(500);
                    } catch (InterruptedException e) {
                        Log.log.error(e.getMessage());
                    }
                }
            }
            
            // Get the search results and use the built-in XML parser to display them
            InputStream resultsNormalSearch =  job.getResults();
    
            ResultsReaderXml resultsReaderNormalSearch;

            resultsReaderNormalSearch = new ResultsReaderXml(resultsNormalSearch);
            
            while ((event = resultsReaderNormalSearch.getNextEvent()) != null) {
                Map<String, String> result = new HashMap<String, String>();
                
                for (String key: event.keySet()) {
                    result.put(key,  event.get(key));
                    Log.log.debug("SplunkPullGateway: normalSearch adding Key -> " + key + ": Value -> " + event.get(key));
                }
                
                events.add(result);
            }
        } catch (Exception e) {
            e.printStackTrace();
            Log.log.error(e.getMessage());
        }
        
        return events;
    }
    
} // SplunkPullAPI