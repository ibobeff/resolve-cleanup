package com.resolve.services.hibernate.vo;

import com.resolve.services.interfaces.VO;
import com.resolve.util.MappingAnnotation;
import com.resolve.util.StringUtils;

public class SplunkPullFilterVO extends GatewayFilterVO {

    private static final long serialVersionUID = 1L;

    public SplunkPullFilterVO() {
    }

    private Integer UMaxNumOfResult;

    private String UQuery;

    private String UType;

    private Integer UTimeout;
    
    private String UTimeFrame;
    
    private String UTimeModifier;

    @MappingAnnotation(columnName = "MAXNUMOFRESULT")
    public Integer getUMaxNumOfResult() {
        return this.UMaxNumOfResult;
    }

    public void setUMaxNumOfResult(Integer uMaxNumOfResult) {
        this.UMaxNumOfResult = uMaxNumOfResult;
    }

    @MappingAnnotation(columnName = "QUERY")
    public String getUQuery() {
        return this.UQuery;
    }

    public void setUQuery(String uQuery) {
        if (StringUtils.isNotBlank(uQuery) || UQuery.equals(VO.STRING_DEFAULT))
        {
            this.UQuery = uQuery != null ? uQuery.trim() : uQuery;
        }
    }

    @MappingAnnotation(columnName = "TYPE")
    public String getUType() {
        return this.UType;
    }

    public void setUType(String uType) {
        if (StringUtils.isNotBlank(uType) || UType.equals(VO.STRING_DEFAULT))
        {
            this.UType = uType != null ? uType.trim() : uType;
        }
    }

    @MappingAnnotation(columnName = "TIMEOUT")
    public Integer getUTimeout() {
        return this.UTimeout;
    }

    public void setUTimeout(Integer uTimeout) {
        this.UTimeout = uTimeout;
    }
    
    @MappingAnnotation(columnName="TIMEFRAME")
    public String getUTimeFrame() {
      return this.UTimeFrame;
    }
    
    public void setUTimeFrame(String uTimeFrame) {
      if (StringUtils.isNotBlank(uTimeFrame) || UTimeFrame.equals(VO.STRING_DEFAULT))
      {
          this.UTimeFrame = uTimeFrame != null ? uTimeFrame.trim() : uTimeFrame;
      }
    }
    
    @MappingAnnotation(columnName="TIMEMODIFIER")
    public String getUTimeModifier() {
      return this.UTimeModifier;
    }
    
    public void setUTimeModifier(String uTimeModifier) {
      if (StringUtils.isNotBlank(uTimeModifier) || UTimeModifier.equals(VO.STRING_DEFAULT))
      {
          this.UTimeModifier = uTimeModifier != null ? uTimeModifier.trim() : uTimeModifier;
      }
    }
    
    @Override
    public boolean equals(Object obj)
    {
        if (this == obj) return true;
        if (!super.equals(obj)) return false;
        if (getClass() != obj.getClass()) return false;
        SplunkPullFilterVO other = (SplunkPullFilterVO) obj;
        if (UQuery == null)
        {
            if (StringUtils.isNotBlank(other.UQuery)) return false;
        }
        else if (!UQuery.trim().equals(other.UQuery == null ? "" : other.UQuery.trim())) return false;
        if (UType == null)
        {
            if (StringUtils.isNotBlank(other.UType)) return false;
        }
        else if (!UType.trim().equals(other.UType == null ? "" : other.UType.trim())) return false;
        if (UMaxNumOfResult == null)
        {
            if (other.UMaxNumOfResult != null) return false;
        }
        else if (!UMaxNumOfResult.equals(other.UMaxNumOfResult)) return false;
        if (UTimeout == null)
        {
            if (other.UTimeout != null) return false;
        }
        else if (!UTimeout.equals(other.UTimeout)) return false;
        if (UTimeFrame == null)
        {
            if (StringUtils.isNotBlank(other.UTimeFrame)) return false;
        }
        else if (!UTimeFrame.trim().equals(other.UTimeFrame == null ? "" : other.UTimeFrame.trim())) return false;
        if (UTimeModifier == null)
        {
            if (StringUtils.isNotBlank(other.UTimeModifier)) return false;
        }
        else if (!UTimeModifier.trim().equals(other.UTimeModifier == null ? "" : other.UTimeModifier.trim())) return false;
        return true;
    }
    
    @Override
    public int hashCode()
    {
        final int prime = 31;
        int result = super.hashCode();
        result = prime * result + ((UMaxNumOfResult == null) ? 0 : UMaxNumOfResult.hashCode());
        result = prime * result + ((UQuery == null) ? 0 : UQuery.hashCode());
        result = prime * result + ((UType == null) ? 0 : UType.hashCode());
        result = prime * result + ((UTimeout == null) ? 0 : UTimeout.hashCode());
        result = prime * result + ((UTimeFrame == null) ? 0 : UTimeFrame.hashCode());
        result = prime * result + ((UTimeModifier == null) ? 0 : UTimeModifier.hashCode());
        return result;
    }
}

