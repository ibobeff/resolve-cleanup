package com.resolve.rscontrol;

import com.resolve.persistence.model.SplunkPullFilter;

public class MSplunkPull extends MGateway {

    private static final String MODEL_NAME = SplunkPullFilter.class.getSimpleName();

    protected String getModelName() {
        return MODEL_NAME;
    }
}

