package com.resolve.gateway.splunkpull;

import java.io.File;
import java.util.HashMap;
import java.util.Map;
import org.apache.commons.io.FileUtils;

import com.resolve.rsbase.MainBase;
import com.resolve.gateway.Filter;
import com.resolve.rsremote.ConfigReceiveGateway;
import com.resolve.util.Log;
import com.resolve.util.ObjectProperties;
import com.resolve.util.StringUtils;
import com.resolve.util.XDoc;
import com.splunk.SSLSecurityProtocol;

public class ConfigReceiveSplunkPull extends ConfigReceiveGateway {

    private String queue = "SPLUNK";
    private static final long serialVersionUID = 1L;

    private static final String RECEIVE_SPLUNK_NODE = "./RECEIVE/SPLUNKPULL/";
    private static final String RECEIVE_SPLUNK_FILTER = RECEIVE_SPLUNK_NODE + "FILTER";
    private static final String RECEIVE_SPLUNK_ATTR_P_ASSWORD = RECEIVE_SPLUNK_NODE + "@PASSWORD";
    private static final String RECEIVE_SPLUNK_ATTR_PORT = RECEIVE_SPLUNK_NODE + "@PORT";
    private static final String RECEIVE_SPLUNK_ATTR_IP = RECEIVE_SPLUNK_NODE + "@IP";
    private static final String RECEIVE_SPLUNK_ATTR_USERNAME = RECEIVE_SPLUNK_NODE + "@USERNAME";
    private static final String RECEIVE_SPLUNK_ATTR_PROTOCOL = RECEIVE_SPLUNK_NODE + "@PROTOCOL";

    private String username = "";
    private String password = "";
    private String ip = "";
    private int port = 0;
    private String protocol = SSLSecurityProtocol.TLSv1_2.name();
    private int timeout = 100;
    private int maxNumberOfResults = 10;

    public String getQueue() {
        return queue;
    }

    public void setQueue(String queue) {
        this.queue = queue;
    }

    public String getPassword() {
        return this.password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public int getPort() {
        return this.port;
    }

    public void setPort(int port) {
        this.port = port;
    }

    public String getIp() {
        return this.ip;
    }

    public void setIp(String ip) {
        this.ip = ip;
    }

    public String getUsername() {
        return this.username;
    }

    public void setUsername(String username) {
        this.username = username;
    }
    
    public String getProtocol()
    {
        return protocol;
    }

    public void setProtocol(String protocol)
    {
        this.protocol = protocol;
    }

    
    public int getTimeout()
    {
        return timeout;
    }

    public void setTimeout(int timeout)
    {
        this.timeout = timeout;
    }

    public int getMaxNumberOfResults()
    {
        return maxNumberOfResults;
    }

    public ConfigReceiveSplunkPull(XDoc config) throws Exception {
        super(config);
        define("password", SECURE, RECEIVE_SPLUNK_ATTR_P_ASSWORD);
        define("port", INT, RECEIVE_SPLUNK_ATTR_PORT);
        define("ip", STRING, RECEIVE_SPLUNK_ATTR_IP);
        define("username", STRING, RECEIVE_SPLUNK_ATTR_USERNAME);
        define("protocol", STRING, RECEIVE_SPLUNK_ATTR_PROTOCOL);
    }

    @Override
    public String getRootNode() {
        return RECEIVE_SPLUNK_NODE;
    }

    @SuppressWarnings("unchecked")
    @Override
    public void load() throws Exception {
        try {
            loadAttributes();
            if (isActive()) {
                SplunkPullGateway splunkPullGateway = SplunkPullGateway.getInstance(this);
                filters = xdoc.getListMapValue(RECEIVE_SPLUNK_FILTER);
                if (filters.size() > 0) {
                    for (Map<String, Object> values : filters) {
                        String filterName = ((String) values.get(SplunkPullFilter.ID)).toLowerCase();
                        File scriptFile = getFile(MainBase.main.release.serviceName + "/config/splunk/" + filterName + ".groovy");
                        try {
                            if (scriptFile.exists()) {
                                String groovy = FileUtils.readFileToString(scriptFile, "UTF-8");
                                if (StringUtils.isNotBlank(groovy)) {
                                    values.put(SplunkPullFilter.SCRIPT, groovy);
                                    Log.log.debug("SplunkPullGateway: Loaded groovy script --> " + scriptFile);                                }
                            }
                            splunkPullGateway.setFilter(values);
                        } catch (Exception e) {
                            Log.log.error("SplunkPullGateway: " + e.getMessage() + ". Skipping file --> " + scriptFile.getAbsolutePath());

                        }
                    }
                }
            }
        } catch (Throwable e) {
            setActive(false);
            Log.log.error("SplunkPullGateway: Couldn not load configuration for Splunk gateway, check blueprint. " + e.getMessage(), e);

        }
    }

    @SuppressWarnings({ "rawtypes", "unchecked" })
    @Override
    public void save() throws Exception {
        File dir = getFile(MainBase.main.release.serviceName + "/config/splunk");
        if (!dir.exists()) {
            FileUtils.forceMkdir(dir);
        }
        if (filters != null && isActive()) {
            filters.clear();
            for (Filter filter : SplunkPullGateway.getInstance().getFilters().values()) {
                SplunkPullFilter splunkFilter = (SplunkPullFilter) filter;
                String groovy = splunkFilter.getScript();
                String scriptFileName;
                File scriptFile = null;
                if (StringUtils.isNotBlank(groovy)) {
                    scriptFileName = splunkFilter.getId().toLowerCase() + ".groovy";
                    scriptFile = getFile(MainBase.main.release.serviceName + "/config/splunk/" + scriptFileName);
                }
                Map<String, Object> entry = new HashMap<String, Object>();
                putFilterDataIntoEntry(entry, splunkFilter);
                filters.add(entry);
                if (scriptFile != null) {
                    try {
                        FileUtils.writeStringToFile(scriptFile, groovy, "UTF-8");
                    } catch (Exception e) {
                        Log.log.error("SplunkPullGateway: " + e.getMessage() + ". Skipping file --> " + scriptFile.getAbsolutePath());                    }
                }
            }
            xdoc.setListMapValue(RECEIVE_SPLUNK_FILTER, filters);
        }
        if (nameProperties != null && isActive()) {
            nameProperties.clear();
            for (String name : SplunkPullGateway.getInstance().getNameProperties().keySet()) {
                Map<String, Object> entry = new HashMap<String, Object>();
                entry.put(NAME_KEY, name);
                nameProperties.add(entry);
                Map props = SplunkPullGateway.getInstance().getNameProperties().getNameProperties(name);
                ObjectProperties prop = new ObjectProperties(props);
                prop.save(props);
            }
        }
        saveAttributes();
    }

    private void putFilterDataIntoEntry(Map<String, Object> entry, SplunkPullFilter splunkFilter) {
        entry.put(SplunkPullFilter.ID, splunkFilter.getId());
        entry.put(SplunkPullFilter.ACTIVE, String.valueOf(splunkFilter.isActive()));
        entry.put(SplunkPullFilter.ORDER, String.valueOf(splunkFilter.getOrder()));
        entry.put(SplunkPullFilter.INTERVAL, String.valueOf(splunkFilter.getInterval()));
        entry.put(SplunkPullFilter.EVENT_EVENTID, splunkFilter.getEventEventId());
        entry.put(SplunkPullFilter.RUNBOOK, splunkFilter.getRunbook());
        entry.put(SplunkPullFilter.SCRIPT, splunkFilter.getScript());
        entry.put(SplunkPullFilter.MAXNUMOFRESULT, splunkFilter.getMaxNumOfResult());
        entry.put(SplunkPullFilter.QUERY, splunkFilter.getQuery());
        entry.put(SplunkPullFilter.TYPE, splunkFilter.getType());
        entry.put(SplunkPullFilter.TIMEOUT, splunkFilter.getTimeout());
        entry.put(SplunkPullFilter.TIMEFRAME, splunkFilter.getTimeFrame());
        entry.put(SplunkPullFilter.TIMEMODIFIER, splunkFilter.getTimeModifier());
    }
}

