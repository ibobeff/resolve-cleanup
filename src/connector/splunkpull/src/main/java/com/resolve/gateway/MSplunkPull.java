package com.resolve.gateway;

import java.util.Map;

import com.resolve.gateway.splunkpull.SplunkPullFilter;
import com.resolve.gateway.splunkpull.SplunkPullGateway;

public class MSplunkPull extends MGateway {

    private static final String RSCONTROL_SET_FILTERS = "MSplunkPull.setFilters";

    private static final SplunkPullGateway instance = SplunkPullGateway.getInstance();

    public MSplunkPull() {
        super.instance = instance;
    }

    protected String getSetFilterMethodName() {
        return RSCONTROL_SET_FILTERS;
    }

    /**
     * Populates the {@link Filter} data into the given filterMap {@link Map} instance
     * 
     * @param filterMap
     *      the {@link Map} instance to hold the {@link Filter} data
     * @param filter
     *      a {@link SplunkPullFilter} instance
     *            
     */
    public void populateGatewaySpecificFilterProperties(Map<String, String> filterMap, Filter filter) {
        if (instance.isActive()) {
            SplunkPullFilter splunkFilter = (SplunkPullFilter) filter;
            filterMap.put(SplunkPullFilter.MAXNUMOFRESULT, "" + splunkFilter.getMaxNumOfResult());
            filterMap.put(SplunkPullFilter.QUERY, splunkFilter.getQuery());
            filterMap.put(SplunkPullFilter.TYPE, splunkFilter.getType());
            filterMap.put(SplunkPullFilter.TIMEOUT, "" + splunkFilter.getTimeout());
            filterMap.put(SplunkPullFilter.TIMEFRAME, splunkFilter.getTimeFrame());
            filterMap.put(SplunkPullFilter.TIMEMODIFIER, splunkFilter.getTimeModifier());
        }
    }
}

