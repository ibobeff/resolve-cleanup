package com.resolve.gateway.splunkpull;

import com.resolve.gateway.BaseFilter;
import com.resolve.util.Log;

public class SplunkPullFilter extends BaseFilter {

    public static final String MAXNUMOFRESULT = "MAXNUMOFRESULT";

    public static final String QUERY = "QUERY";

    public static final String TYPE = "TYPE";

    public static final String TIMEOUT = "TIMEOUT";
    
    public static final String TIMEFRAME = "TIMEFRAME";
    
    public static final String TIMEMODIFIER = "TIMEMODIFIER";

    private Integer maxNumOfResult;

    private String query;

    private String type;

    private Integer timeout;
    
    private String timeFrame;
    
    private String timeModifier;

    public SplunkPullFilter(String id, String active, String order, String interval, String eventEventId, String runbook, String script, String maxNumOfResult, String query, String type, String timeout, String timeFrame, String timeModifier) {
        super(id, active, order, interval, eventEventId, runbook, script);
        try {
            this.maxNumOfResult = new Integer(maxNumOfResult);
        } catch (Exception e) {
            Log.log.error("SplunkPullGateway: maxNumOfResult --> " +maxNumOfResult + " should be in type Integer");
        }
        setQuery(query);
        setType(type);
        setTimeModifier(timeModifier);
        setTimeFrame(timeFrame);
        try {
            this.timeout = new Integer(timeout);
        } catch (Exception e) {
            Log.log.error("timeout -->" + timeout + " should be in type Integer");
        }
    }

    public Integer getMaxNumOfResult() {
        return this.maxNumOfResult;
    }

    public void setMaxNumOfResult(Integer maxNumOfResult) {
        this.maxNumOfResult = maxNumOfResult;
    }

    public String getQuery() {
        return this.query;
    }

    public void setQuery(String query) {
        this.query = query != null ? query.trim() : query;
    }

    public String getType() {
        return this.type;
    }

    public void setType(String type) {
        this.type = type != null ? type.trim() : type;
    }

    public Integer getTimeout() {
        return this.timeout;
    }

    public void setTimeout(Integer timeout) {
        this.timeout = timeout;
    }
    
    public void setTimeModifier(String timeModifier) {
      this.timeModifier = timeModifier;
    }
    
    public String getTimeModifier() {
      return this.timeModifier;
    }
    
    public void setTimeFrame(String timeFrame) {
      this.timeFrame = timeFrame;
    }
    
    public String getTimeFrame() {
      return this.timeFrame;
    }
}
