package com.resolve.persistence.model;

import java.util.List;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;
import javax.persistence.UniqueConstraint;
import com.resolve.services.interfaces.VO;
import com.resolve.services.hibernate.vo.SplunkPullFilterVO;

@Entity
@Table(name = "splunkpull_filter", uniqueConstraints = { @UniqueConstraint(columnNames = { "u_name", "u_queue" }) })
public class SplunkPullFilter extends GatewayFilter<SplunkPullFilterVO> {

    private static final long serialVersionUID = 1L;

    public SplunkPullFilter() {
    }

    public SplunkPullFilter(SplunkPullFilterVO vo) {
        applyVOToModel(vo);
    }

    private Integer UMaxNumOfResult;

    private String UQuery;

    private String UType;

    private Integer UTimeout;
    
    private String UTimeFrame;
    
    private String UTimeModifier;

    @Column(name = "u_maxnumofresult")
    public Integer getUMaxNumOfResult() {
        return this.UMaxNumOfResult;
    }

    public void setUMaxNumOfResult(Integer uMaxNumOfResult) {
        this.UMaxNumOfResult = uMaxNumOfResult;
    }

    @Column(name = "u_query", length = 400)
    public String getUQuery() {
        return this.UQuery;
    }

    public void setUQuery(String uQuery) {
        this.UQuery = uQuery;
    }

    @Column(name = "u_type", length = 400)
    public String getUType() {
        return this.UType;
    }

    public void setUType(String uType) {
        this.UType = uType;
    }

    @Column(name = "u_timeout")
    public Integer getUTimeout() {
        return this.UTimeout;
    }

    public void setUTimeout(Integer uTimeout) {
        this.UTimeout = uTimeout;
    }
    
    @Column(name ="u_timeframe")
    public String getUTimeFrame() {
      return this.UTimeFrame;
    }
    
    public void setUTimeFrame(String uTimeFrame) {
      this.UTimeFrame = uTimeFrame;
    }
    
    @Column(name="u_timemodifier",length=400)
    public String getUTimeModifier() {
      return this.UTimeModifier;
    }
    
    public void setUTimeModifier(String uTimeModifier) {
      this.UTimeModifier = uTimeModifier;
    }
    

    public SplunkPullFilterVO doGetVO() {
        SplunkPullFilterVO vo = new SplunkPullFilterVO();
        super.doGetBaseVO(vo);
        vo.setUMaxNumOfResult(getUMaxNumOfResult());
        vo.setUQuery(getUQuery());
        vo.setUType(getUType());
        vo.setUTimeout(getUTimeout());
        vo.setUTimeFrame(getUTimeFrame());
        vo.setUTimeModifier(getUTimeModifier());
        return vo;
    }

    @Override
    public void applyVOToModel(SplunkPullFilterVO vo) {
        if (vo == null) return;
        super.applyVOToModel(vo);
        if (!VO.INTEGER_DEFAULT.equals(vo.getUMaxNumOfResult())) this.setUMaxNumOfResult(vo.getUMaxNumOfResult()); else ;
        if (!VO.STRING_DEFAULT.equals(vo.getUQuery())) this.setUQuery(vo.getUQuery()); else ;
        if (!VO.STRING_DEFAULT.equals(vo.getUType())) this.setUType(vo.getUType()); else ;
        if (!VO.INTEGER_DEFAULT.equals(vo.getUTimeout())) this.setUTimeout(vo.getUTimeout()); else ;
        if (!VO.STRING_DEFAULT.equals(vo.getUTimeFrame())) this.setUTimeFrame(vo.getUTimeFrame()); else ;
        if (!VO.STRING_DEFAULT.equals(vo.getUTimeModifier())) this.setUTimeModifier(vo.getUTimeModifier()); else ;
    }

    @Override
    public List<String> ugetCdataProperty() {
        List<String> list = super.ugetCdataProperty();
        list.add("UQuery");
        list.add("UType");
        list.add("UTimeModifier");
        list.add("UTimeFrame");
        return list;
    }
}

