/******************************************************************************
* (C) Copyright 2014
*
* Resolve Systems, LLC
*
* All rights reserved
* This software is distributed under license and may not be used, copied,
* modified, or distributed without the express written permission of
* Resolve Systems, LLC.
*
******************************************************************************/

package com.resolve.gateway.qradarpull;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.net.CookieHandler;
import java.net.CookieManager;
import java.net.CookiePolicy;
import java.net.CookieStore;
import java.net.HttpCookie;
import java.net.HttpURLConnection;
import java.net.URL;
import java.net.URLConnection;
import java.net.URLEncoder;
import java.security.cert.X509Certificate;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;

import javax.net.ssl.HttpsURLConnection;
import javax.net.ssl.SSLContext;
import javax.net.ssl.TrustManager;
import javax.net.ssl.X509TrustManager;

import com.resolve.util.Log;
import com.resolve.util.StringUtils;

public class RestClient
{
    private static String baseUrl;
    private static String TLS = "TLSv1.2";
    private static final String lineSeparator = System.getProperty("line.separator");
    
    // Response status and headers 
    private int statusCode;
    private String reasonPhrase;
    private Map<String, String> responseHeaders = new HashMap<String, String>();

    private String user = null;
    private String pass = null;
    private boolean basicAuth = true;
    private boolean cookieEnabled = true;
    private boolean selfSigned = true;
    private boolean trustAll = false;
    
    /**
     * Constructor with basic information and user preferences
     * 
     * @param baseUrl: contains protocol (HTTP/HTTPS), host name or ip, port number
     * @param username: used for Basic Authentication or login to get a token, can be blank when token is provided or token is set in the cookie store
     * @param password: used for Basic Authentication or login to get a token, can be blank when token is provided or token is set in the cookie store
     * @param token: used for each REST call by explicitly embedded it with the key name of "token" in HTTPRequest when cookie store is not used.
     *        If the key name is not "token", embed the key and value in the request params. 
     *        Token can also be set in the request headers or request params or in the body payload by the caller based on the server needs.
     *        If the server supports cookies, no token needs to be embedded.
     * @param baseAuth: if true, username and password will be encode with Base64 and embedded into HTTP header for authentication
     * @param timeout: timeout for HTTP/HTTPS connection
     * @param selfSigned: HTTPSURLConnection will be eatablished when server provides a self-signed certificate, no hsst name will be verified
     * @param trustAll: HTTPSURLConnection will be esatblished with fake certificates and even without any certificate is provided
     * @throws Exception
     */
    public RestClient(String baseUrl, String user, String pass, boolean basicAuth, boolean cookieEnabled, boolean selfSigned, boolean trustAll) {
        
        this.baseUrl = baseUrl;
        this.user = user;
        this.pass = pass;
        this.basicAuth = basicAuth;
        this.cookieEnabled = cookieEnabled;
        this.selfSigned = selfSigned;
        this.trustAll = trustAll;
    }

    public String getBaseUrl()
    {
        return baseUrl;
    }

    /**
     * Call REST API using HTTP GET method.
     * If token value is not empty, token will be embedded as key of "token" for each REST call.
     * If basicAuth is true, Basic Authentication will be used for each REST call.
     * If the method call fails with token expired or unauthorized, caller needs to catch the exception and call login again to get a new token.
     * 
     * @param uri: REST interface/path defined by the server
     * @param params: the query key/value pairs to send to the server
     * @param headers: any header properties caller needs to add, such as "Cache-Control": "no-cache"
     * @param accept: tell the server what content type can be accepted, such as text/xml, application/xml, application/json
     * @return: the response string
     * @throws Exception
     */
    
    public String callGet(String uri, Map<String, String> params, Map<String, String>headers) throws Exception {
        
        HttpURLConnection conn = null;
        InputStream input = null;
        InputStream error = null;
        BufferedReader bufferedReader = null;
        StringBuilder path = new StringBuilder();
        StringBuilder response = new StringBuilder();

        if(StringUtils.isNotBlank(uri))
            path.append(uri);

        String url = "";
        
        if(params != null && params.size() != 0) {
            if(uri.indexOf("?") != -1)
                path.append("&");
            else
                path.append("?");
            
            for(Iterator<String> it=params.keySet().iterator(); it.hasNext();) {
                String key = it.next();
                String value = params.get(key);
                path.append(key).append("=").append(URLEncoder.encode(value, "UTF-8")).append("&");
            }
            
            url = path.toString();
            url = url.substring(0, url.length()-1);
        }
        
        else
            url = path.toString();
        
        try {
            conn = getConnection(url, headers);
            
            conn.setRequestMethod("GET");
            
            if(basicAuth)
                doBasicAuthentication(conn);

            conn.setDoInput(true);
            
            input = conn.getInputStream();
            bufferedReader = new BufferedReader(new InputStreamReader(input));
            
            String line = null;
            while ((line = bufferedReader.readLine()) != null)
                response.append(line);
    
            conn.getContent();
        } catch(Exception e) {
//            e.printStackTrace();
            int code = conn.getResponseCode();
            Log.log.debug("Response code is: " + code);
            error = conn.getErrorStream();
            bufferedReader = new BufferedReader(new InputStreamReader(error));
            
            String line = null;
            StringBuilder sb = new StringBuilder();
            while ((line = bufferedReader.readLine()) != null)
                sb.append(line);
            
            String message = e.getMessage();
            if(StringUtils.isNotEmpty(message))
                message = message + lineSeparator + sb.toString();
            else
                message = sb.toString();
            
            if(StringUtils.isEmpty(message))
                message = "";
            
            Exception ee = new Exception(message);
            ee.setStackTrace(e.getStackTrace());
            
            Log.log.error(ee.getMessage(), ee);
            throw ee;
        } finally {
            try {
                if(bufferedReader != null)
                    bufferedReader.close();
                if(conn != null)
                    conn.disconnect();
            } catch(Exception ee) {}
        }
        
        return response.toString();
    }
    
    /**
     * Call REST API using HTTP POST method.
     * If token value is not empty, token will be embedded as key of "token" for each REST call.
     * If basicAuth is true, Basic Authentication will be used for each REST call.
     * If the method call fails with token expired or unauthorized, caller needs to catch the exception and call login again to get a new token.
     * 
     * @param uri: REST interface/path defined by the server
     * @param params: the query key/value pairs to send to the server
     * @param body: data encoded in the format of the content type to send to the server
     * @param headers: any header properties caller needs to add
     * @param contentType: tell the server about the content type encoding for the HTTP request, such as text/xml, application/xml, application/json, etc.
     * @param accept: tell the server what content type can be accepted, such as text/xml, application/xml, application/json
     * @return: the response string
     * @throws Exception
     */
    
    public String callPost(String uri, Map<String, String> params, String body, Map<String, String>headers) throws Exception {

        HttpURLConnection conn = null;
        
        InputStream input = null;
        InputStream error = null;
        OutputStream output = null;
        
        BufferedReader bufferedReader = null;
        BufferedWriter bufferedWriter = null;
        
        StringBuilder path = new StringBuilder();
        StringBuilder response = new StringBuilder();
        
        try {
            path.append(uri);
            
            String url = "";

            if(params != null && params.size() != 0) {
                if(uri.indexOf("?") != -1)
                    path.append("&");
                else
                    path.append("?");
                
                for(Iterator<String> it=params.keySet().iterator(); it.hasNext();) {
                    String key = it.next();
                    String value = params.get(key);
                    path.append(key).append("=").append(URLEncoder.encode(value, "UTF-8")).append("&");
                    url = path.toString();
                    url = url.substring(0, url.length()-1);
                }
            }
            
            else
                url = path.toString();

            conn = getConnection(url, headers);
            
            conn.setRequestMethod("POST");

            conn.setDoInput(true);
            conn.setDoOutput(true);
            
            if(basicAuth)
                doBasicAuthentication(conn);
    
            if(body != null) {
                conn.addRequestProperty("Content-Length", Integer.toString(body.length()));
                output = conn.getOutputStream();
                BufferedWriter writer = new BufferedWriter(new OutputStreamWriter(output, "UTF8"));
                writer.write(body.trim());
                writer.flush();
            }
            
            input = conn.getInputStream();

            bufferedReader = new BufferedReader(new InputStreamReader(input));
            
            String line = null;
            while ((line = bufferedReader.readLine()) != null)
                response.append(line);

//            conn.getContent();
        } catch(Exception e) {
//            e.printStackTrace();
            int code = conn.getResponseCode();
            Log.log.debug("Response code is: " + code);
            error = conn.getErrorStream();
            bufferedReader = new BufferedReader(new InputStreamReader(error));
            
            String line = null;
            StringBuilder sb = new StringBuilder();
            while ((line = bufferedReader.readLine()) != null)
                sb.append(line);
            
            String message = e.getMessage();
            if(StringUtils.isNotEmpty(message))
                message = message + lineSeparator + sb.toString();
            else
                message = sb.toString();
            
            if(StringUtils.isEmpty(message))
                message = "";
            
            Exception ee = new Exception(message);
            ee.setStackTrace(e.getStackTrace());
            
            Log.log.error(ee.getMessage(), ee);
            throw ee;
        } finally {
            try {
                if(input != null)
                    input.close();
                if(error != null)
                    error.close();
                if(output != null)
                    output.close();
                if(bufferedReader != null)
                    bufferedReader.close();
                if(bufferedWriter != null)
                    bufferedWriter.close();
                if(conn != null)
                    conn.disconnect();
            } catch(Exception ee) {}
        }
        
        return response.toString();
    }
    
    private void doBasicAuthentication(URLConnection conn) throws Exception {
        
        if(StringUtils.isBlank(user) || StringUtils.isBlank(pass))
            throw new Exception("Failed to do Basic Authentication with username or password blank.");
        
        String auth = user + ":" + pass;
        String authEncoded = new String(org.apache.commons.codec.binary.Base64.encodeBase64(auth.getBytes()));
        conn.setRequestProperty("Authorization", String.format("Basic %s", authEncoded));
    }
    
    private HttpURLConnection getConnection(String uri, Map<String, String>headers) throws Exception {
        
        HttpURLConnection conn = null;
        
        CookieManager cookieManager = null;
        CookieStore cookieStore = null;
        
        if(cookieEnabled) {
            cookieManager = new CookieManager();
            cookieManager.setCookiePolicy(CookiePolicy.ACCEPT_ALL);
            cookieStore = cookieManager.getCookieStore();
            CookieHandler.setDefault(cookieManager);
        }
        
        URL url = new URL(baseUrl + uri);
//        Log.log.debug("url = " + url.toString());

        String protocol = url.getProtocol();
        if(StringUtils.isNotBlank(protocol) && protocol.equalsIgnoreCase("https")) {
            if(selfSigned) {
                HttpsURLConnection.setDefaultHostnameVerifier(new javax.net.ssl.HostnameVerifier() {
                    public boolean verify(String hostname, javax.net.ssl.SSLSession sslSession) {
                        return true;
                    }
                });
            }
            
            if(trustAll) {
                TrustManager[] trustAllCerts = trustAllCertificates();
                
                String tls = System.getProperty("https.protocols");
                
                if(StringUtils.isNotEmpty(tls)) {
                    if(tls.contains(",")) {
                        String tlss[] = tls.split(",");
                        tls = tlss[0];
                    }
                }

                else
                    tls = TLS;
                Log.log.debug("TLS version is: " + tls);
                
                SSLContext sc = SSLContext.getInstance(tls);
                sc.init(null, trustAllCerts, new java.security.SecureRandom());
                HttpsURLConnection.setDefaultSSLSocketFactory(sc.getSocketFactory());
                
                HttpsURLConnection.setDefaultHostnameVerifier(new javax.net.ssl.HostnameVerifier() {
                    public boolean verify(String hostname, javax.net.ssl.SSLSession sslSession) {
                        return true;
                    }
                });
            }

            conn = (HttpsURLConnection)url.openConnection();
        } // https
        
        else
            conn = (HttpURLConnection) url.openConnection();

        if(headers != null) {
            Set<String> keys = headers.keySet();
            for(Iterator it = keys.iterator(); it.hasNext();) {
                String key = (String)it.next();
                String value = headers.get(key);
                
                conn.addRequestProperty(key,  value);
            }
        }
        
        if(cookieEnabled) {
            List<HttpCookie> cookies = cookieStore.getCookies();
            
            if (cookies != null) {
                StringBuilder sb = new StringBuilder();
                
                for (HttpCookie cookie : cookies) {
                    String cookieName = cookie.getName();
                    String cookieValue = cookie.getValue();
                    
                    sb.append(cookieName).append("=").append(cookieValue).append(";");
                }
            
//              Log.log.info("cookies = " + sb.toString());
                conn.addRequestProperty("Cookie", sb.toString());
            }
        }
        
        return conn;
    }
    
    private static TrustManager[] trustAllCertificates() {
        // Create a trust manager that does not validate certificate chains
        TrustManager[] trustAllCerts = new TrustManager[] { new X509TrustManager() {
            public X509Certificate[] getAcceptedIssuers() {
                return new X509Certificate[0];
            }

            public void checkClientTrusted(X509Certificate[] certs, String authType) {
            }

            public void checkServerTrusted(X509Certificate[] certs, String authType) {
            }
        } };
        
        return trustAllCerts;
    }
}
