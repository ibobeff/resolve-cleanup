package com.resolve.gateway;

import java.util.Map;
import com.resolve.gateway.qradarpull.QRadarPullFilter;
import com.resolve.gateway.qradarpull.QRadarPullGateway;

public class MQRadarPull extends MGateway {

    private static final String RSCONTROL_SET_FILTERS = "MQRadarPull.setFilters";

    private static final QRadarPullGateway instance = QRadarPullGateway.getInstance();

    public MQRadarPull() {
        super.instance = instance;
    }

    protected String getSetFilterMethodName() {
        return RSCONTROL_SET_FILTERS;
    }

    /**
     * Populates the {@link Filter} data into the given filterMap {@link Map} instance
     * 
     * @param filterMap
     *      the {@link Map} instance to hold the {@link Filter} data
     * @param filter
     *      a {@link QRadarPullFilter} instance
     *            
     */
    public void populateGatewaySpecificFilterProperties(Map<String, String> filterMap, Filter filter) {
        if (instance.isActive()) {
            QRadarPullFilter qradarpullFilter = (QRadarPullFilter) filter;
            filterMap.put(QRadarPullFilter.QUERY, qradarpullFilter.getQuery());
            filterMap.put(QRadarPullFilter.SORT, qradarpullFilter.getSort());
        }
    }
}

