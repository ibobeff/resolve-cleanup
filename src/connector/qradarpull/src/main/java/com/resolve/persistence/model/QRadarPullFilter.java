package com.resolve.persistence.model;

import java.util.List;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;
import javax.persistence.UniqueConstraint;
import com.resolve.services.interfaces.VO;
import com.resolve.services.hibernate.vo.QRadarPullFilterVO;

@Entity
@Table(name = "qradarpull_filter", uniqueConstraints = { @UniqueConstraint(columnNames = { "u_name", "u_queue" }) })
public class QRadarPullFilter extends GatewayFilter<QRadarPullFilterVO> {

    private static final long serialVersionUID = 1L;

    public QRadarPullFilter() {
    }

    public QRadarPullFilter(QRadarPullFilterVO vo) {
        applyVOToModel(vo);
    }

    private String UQuery;
    private String USort;

    @Column(name = "u_query", length = 256)
    public String getUQuery() {
        return this.UQuery;
    }

    public void setUQuery(String uQuery) {
        this.UQuery = uQuery;
    }

    public QRadarPullFilterVO doGetVO() {
        QRadarPullFilterVO vo = new QRadarPullFilterVO();
        super.doGetBaseVO(vo);
        vo.setUQuery(getUQuery());
        vo.setUSort(getUSort());
        return vo;
    }
    
    @Column(name = "u_sort", length = 256)
    public String getUSort() {
    	return this.USort;
    }
    
    public void setUSort(String uSort) {
    	this.USort = uSort;
    }
    
    @Override
    public void applyVOToModel(QRadarPullFilterVO vo) {
        if (vo == null) return;
        super.applyVOToModel(vo);
        if (!VO.STRING_DEFAULT.equals(vo.getUQuery())) this.setUQuery(vo.getUQuery()); else ;
        if (!VO.STRING_DEFAULT.equals(vo.getUSort())) this.setUSort(vo.getUSort()); else ;
    }

    @Override
    public List<String> ugetCdataProperty() {
        List<String> list = super.ugetCdataProperty();
        list.add("UQuery");
        list.add("USort");
        return list;
    }
}

