package com.resolve.gateway.qradarpull;

import java.util.HashMap;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.Iterator;
import java.util.List;
import java.util.ArrayList;

import com.resolve.rsbase.MainBase;
import com.resolve.gateway.BaseClusteredGateway;
import com.resolve.gateway.BaseFilter;
import com.resolve.gateway.Filter;
import com.resolve.gateway.MQRadarPull;
import com.resolve.gateway.qradarpull.QRadarPullFilter;
import com.resolve.util.Constants;
import com.resolve.util.Log;
import com.resolve.util.StringUtils;
import com.resolve.rsremote.ConfigReceiveGateway;

public class QRadarPullGateway extends BaseClusteredGateway {

    private static volatile QRadarPullGateway instance = null;

    private String queue = null;

    public static QRadarPullGateway getInstance(ConfigReceiveQRadarPull config) {
        if (instance == null) {
            instance = new QRadarPullGateway(config);
        }
        return instance;
    }

    /**
     * This method is called once the initialization is done. If it gets called
     * before the initialization it will throw exception.
     *
     * @return
     */
    public static QRadarPullGateway getInstance() {
        if (instance == null) {
            throw new RuntimeException("QRadarPull Gateway is not initialized correctly..");
        } else {
            return instance;
        }
    }

    private QRadarPullGateway(ConfigReceiveQRadarPull config) {
        super(config);
    }

    @Override
    public String getLicenseCode() {
        return "QRADARPULL";
    }

    @Override
    protected void addAdditionalValues(Map<String, Object> arg0, Map<String, String> arg1) {
    }

    @Override
    protected String getGatewayEventType() {
        return "QRADARPULL";
    }

    @Override
    protected String getMessageHandlerName() {
        return MQRadarPull.class.getSimpleName();
    }

    @Override
    protected Class<MQRadarPull> getMessageHandlerClass() {
        return MQRadarPull.class;
    }

    public String getQueueName() {
        return queue + getOrgSuffix();
    }

    @Override
    public void start() {
        Log.log.debug("Starting QRadarPull Gateway");
        super.start();
    }

    @Override
    public void run() {
        if (!((ConfigReceiveGateway) configurations).isMock())
            super.sendSyncRequest();

        while (running) {
            try {
                long startTime = System.currentTimeMillis();

                if (primaryDataQueue.isEmpty()) {
                    Log.log.trace("Local Primary Event Queue for " + getGatewayEventType() + " is empty.....");

                    for (Filter filter : orderedFilters) {
                        if (shouldFilterExecute(filter, startTime)) {
                            Log.log.trace("Filter " + filter.getId() + " executing");

                            QRadarPullFilter currentFilter = (QRadarPullFilter)filter;
                            List<Map<String, String>> results = invokeService(currentFilter);

                            if(results.size() == 0)
                                continue;

                            for (Map<String, String> result : results) {
                                result.put(Constants.WORKSHEET_ALERTID, result.get("id"));
                                addToPrimaryDataQueue(filter, result);
                            }

                            Map<String, String> last = results.get(results.size() - 1);

                            String lastOffenseId = last.get(QRadarPullFilter.ID_FIELD);
                            String lastStartTime = last.get(QRadarPullFilter.LAST_UPDATED_TIME_FIELD);

                            Log.log.debug("The retrieved last offense id is " + lastOffenseId);
                            Log.log.debug("The retrieved last start time is " + lastStartTime);

                            if(StringUtils.isNotEmpty(lastOffenseId))
                                currentFilter.setLastOffenseId(lastOffenseId);

                            if(StringUtils.isNotEmpty(lastStartTime))
                                currentFilter.setLastStartTime(lastStartTime);

                        } else {
                            Log.log.trace("Filter:" + filter.getId() + " is inactive or not ready to execute yet.....");
                        }
                    }
                    if (orderedFilters == null || orderedFilters.size() == 0) {
                        Log.log.trace("There is not deployed filter for QRadarPullGateway");
                    }
                }
                if (orderedFilters.size() == 0) {
                    Thread.sleep(interval);
                }
            } catch (Exception e) {
                Log.log.warn("Warning: " + e.getMessage());
                Log.alert("Instance " + MainBase.main.configId.getGuid() + " " + getLicenseCode() + "-" + getQueueName() + "-" + getInstanceType() + " Polling Failed", "Instance " + MainBase.main.configId.getGuid() + " " + getLicenseCode() + "-" + getQueueName() + "-" + getInstanceType() + " Polling Failed " + "due to " + e.getMessage());
                try {
                    Thread.sleep(interval);
                } catch (InterruptedException ie) {
                    Log.log.warn("sleep interrupted: " + ie.getMessage(), ie);
                }
            }
        }
    }

    /**
     * Add customized code here to get the data as List<Map<String, String>> format for each filter
     * @return List<Map<String, String>>
     */
    public List<Map<String, String>> invokeService(QRadarPullFilter filter) {

        List<Map<String, String>> result = new ArrayList<Map<String, String>>();

        String query = filter.getQuery();
        String customQuery = query;
        String lastOffenseId = filter.getLastOffenseId();
        String lastStartTime = filter.getLastStartTime();

        if(StringUtils.isNotEmpty(customQuery)) {
            try {
                Matcher matcher = VAR_REGEX_GATEWAY_VARIABLE.matcher(customQuery);

                while(matcher.find()) {
                    String field = matcher.group(1);

                    if(QRadarPullFilter.LAST_OFFENSE_ID.equals(field)) {
                        if(lastOffenseId != null)
                            customQuery = customQuery.replaceAll("\\$\\{" + QRadarPullFilter.LAST_OFFENSE_ID + "\\}", filter.getLastOffenseId());
                        else {
                            query = "";
                            break;
                        }
                    }

                    if(QRadarPullFilter.LAST_START_TIME.equals(field)) {
                        if(lastStartTime != null)
                            customQuery = customQuery.replaceAll("\\$\\{" + QRadarPullFilter.LAST_START_TIME + "\\}", filter.getLastStartTime());
                        else {
                            query = "";
                            break;
                        }
                    }
                }
                Log.log.debug("custom query = " + customQuery);
            } catch(Exception e) {
//                e.printStackTrace();
                Log.log.error("Failed to translate the input query, use default query instead.", e);
                query = "";
            }
        }

        try {
            Long start = null;

            if(StringUtils.isBlank(query)) {
            	
                start = Long.parseLong(filter.getLastStartTime());
            }

            if(StringUtils.isBlank(query) || customQuery.equals(query))
                result = QRadarPullAPI.retrieveOffenses(start, null, "OPEN", null, null, null, query,filter.getSort());

            else
                result = QRadarPullAPI.getOffenses(null, null, null, customQuery,filter.getSort());

//          updateOffenseStatus(result, "HIDDEN");
        } catch(Exception e) {
            Log.log.error(e.getMessage(), e);
        }

        return result;
    }

    /**
     * Add customized code here to initilize the connection with 3rd party system
     * @return List<Map<String, String>>
     */
    @Override
    protected void initialize() {
        ConfigReceiveQRadarPull config = (ConfigReceiveQRadarPull) configurations;
        queue = config.getQueue().toUpperCase();
        this.gatewayConfigDir = "/config/qradarpull/";
        //Add customized code here to initilize the connection with 3rd party system;
    }

    /**
     * Add customized code here to do when stop the gateway
     */
    @Override
    public void stop() {
        Log.log.warn("Stopping QRadarPull gateway");
        //Add customized code here to stop the connection with 3rd party system;
        super.stop();
    }

    @Override
    public Filter getFilter(Map<String, Object> params) {

        QRadarPullFilter filter = new QRadarPullFilter((String) params.get(BaseFilter.ID), (String) params.get(BaseFilter.ACTIVE), (String) params.get(BaseFilter.ORDER),
                                                       (String) params.get(BaseFilter.INTERVAL), (String) params.get(BaseFilter.EVENT_EVENTID), (String) params.get(BaseFilter.RUNBOOK),
                                                       (String) params.get(BaseFilter.SCRIPT), (String) params.get(QRadarPullFilter.QUERY), (String) params.get(QRadarPullFilter.SORT));

        if ((params.containsKey(QRadarPullFilter.LAST_OFFENSE_ID)) && StringUtils.isNotBlank((String)params.get(QRadarPullFilter.LAST_OFFENSE_ID)))
            filter.setLastOffenseId((String)params.get(QRadarPullFilter.LAST_OFFENSE_ID));

        if ((params.containsKey(QRadarPullFilter.LAST_START_TIME)) && StringUtils.isNotBlank((String)params.get(QRadarPullFilter.LAST_START_TIME)))
            filter.setLastStartTime((String)params.get(QRadarPullFilter.LAST_START_TIME));

        return filter;
    }

    public Map<String, Object> loadSystemProperties() {

        Map<String, Object> properties = new HashMap<String, Object>();

        ConfigReceiveQRadarPull config = (ConfigReceiveQRadarPull) configurations;

        properties.put("HOST", config.getHost());
        properties.put("PORT", config.getPort());
        properties.put("SSL", config.getSsl());
        properties.put("SEC", config.getSec());

        return properties;
    }

    private void updateOffenseStatus(List<Map<String, String>> results, String status) {

        if(results == null)
            return;

        for(Map<String, String> result:results) {
            for(Iterator<String> it = result.keySet().iterator(); it.hasNext();) {
                String key = (String)it.next();
                if(key.equalsIgnoreCase("id")) {
                    try {
                        QRadarPullAPI.postOffense(result.get(key), status, null);
                    } catch(Exception e) {
                        Log.log.error(e.getMessage(), e);
                    }

                    return;
                }
            }
        }
    }

    private String translate(QRadarPullFilter filter, String query) throws Exception {

        if(query.contains("${" + QRadarPullFilter.LAST_OFFENSE_ID + "}"))
            query = query.replaceFirst("\\$\\{" + QRadarPullFilter.LAST_OFFENSE_ID + "\\}", filter.getLastOffenseId());

        if(query.contains("${" + QRadarPullFilter.LAST_START_TIME + "}"))
            query = query.replaceFirst("\\$\\{" + QRadarPullFilter.LAST_START_TIME + "\\}", filter.getLastStartTime());

        return query;
    }

} // QRadarPullGateway

