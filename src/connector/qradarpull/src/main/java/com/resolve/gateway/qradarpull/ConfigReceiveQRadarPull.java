package com.resolve.gateway.qradarpull;

import java.io.File;
import java.util.HashMap;
import java.util.Map;
import org.apache.commons.io.FileUtils;

import com.resolve.rsbase.MainBase;
import com.resolve.gateway.Filter;
import com.resolve.rsremote.ConfigReceiveGateway;
import com.resolve.util.Log;
import com.resolve.util.ObjectProperties;
import com.resolve.util.StringUtils;
import com.resolve.util.XDoc;

public class ConfigReceiveQRadarPull extends ConfigReceiveGateway {

    private static final long serialVersionUID = 1L;

    private static final String RECEIVE_QRADARPULL_NODE = "./RECEIVE/QRADARPULL/";

    private static final String RECEIVE_QRADARPULL_FILTER = RECEIVE_QRADARPULL_NODE + "FILTER";

    private String queue = "QRADARPULL";

    private static final String RECEIVE_QRADARPULL_ATTR_SEC = RECEIVE_QRADARPULL_NODE + "@SEC";

    private static final String RECEIVE_QRADARPULL_ATTR_PORT = RECEIVE_QRADARPULL_NODE + "@PORT";

    private static final String RECEIVE_QRADARPULL_ATTR_HOST = RECEIVE_QRADARPULL_NODE + "@HOST";

    private static final String RECEIVE_QRADARPULL_ATTR_SSL = RECEIVE_QRADARPULL_NODE + "@SSL";

    private String sec = "";

    private String port = "";

    private String host = "";

    private String ssl = "";

    public String getQueue() {
        return queue;
    }

    public void setQueue(String queue) {
        this.queue = queue;
    }

    public String getSec() {
        return this.sec;
    }

    public void setSec(String sec) {
        this.sec = sec;
    }

    public String getPort() {
        return this.port;
    }

    public void setPort(String port) {
        this.port = port;
    }

    public String getHost() {
        return this.host;
    }

    public void setHost(String host) {
        this.host = host;
    }

    public String getSsl() {
        return this.ssl;
    }

    public void setSsl(String ssl) {
        this.ssl = ssl;
    }

    public ConfigReceiveQRadarPull(XDoc config) throws Exception {
        super(config);
        define("sec", STRING, RECEIVE_QRADARPULL_ATTR_SEC);
        define("port", STRING, RECEIVE_QRADARPULL_ATTR_PORT);
        define("host", STRING, RECEIVE_QRADARPULL_ATTR_HOST);
        define("ssl", STRING, RECEIVE_QRADARPULL_ATTR_SSL);
    }

    @Override
    public String getRootNode() {
        return RECEIVE_QRADARPULL_NODE;
    }

    @Override
    public void load() throws Exception {
        try {
            loadAttributes();
            if (isActive()) {
                QRadarPullGateway qradarpullGateway = QRadarPullGateway.getInstance(this);
                filters = xdoc.getListMapValue(RECEIVE_QRADARPULL_FILTER);
                if (filters.size() > 0) {
                    for (Map<String, Object> values : filters) {
                        String filterName = ((String) values.get(QRadarPullFilter.ID)).toLowerCase();
                        File scriptFile = getFile(MainBase.main.release.serviceName + "/config/qradarpull/" + filterName + ".groovy");
                        try {
                            if (scriptFile.exists()) {
                                String groovy = FileUtils.readFileToString(scriptFile, "UTF-8");
                                if (StringUtils.isNotBlank(groovy)) {
                                    values.put(QRadarPullFilter.SCRIPT, groovy);
                                    Log.log.debug("Loaded groovy script: " + scriptFile);
                                }
                            }
                            qradarpullGateway.setFilter(values);
                        } catch (Exception e) {
                            Log.log.warn(e.getMessage() + ". Skipping file: " + scriptFile.getAbsolutePath());
                        }
                    }
                }
            }
        } catch (Throwable e) {
            setActive(false);
            Log.log.warn("Couldn not load configuration for QRadarPull gateway, check blueprint. " + e.getMessage(), e);
        }
    }

    @Override
    public void save() throws Exception {
        File dir = getFile(MainBase.main.release.serviceName + "/config/qradarpull");
        if (!dir.exists()) {
            FileUtils.forceMkdir(dir);
        }
        if (filters != null && isActive()) {
            filters.clear();
            for (Filter filter : QRadarPullGateway.getInstance().getFilters().values()) {
                QRadarPullFilter qradarpullFilter = (QRadarPullFilter) filter;
                
                String groovy = qradarpullFilter.getScript();
                String scriptFileName;
                File scriptFile = null;
                if (StringUtils.isNotBlank(groovy)) {
                    scriptFileName = qradarpullFilter.getId().toLowerCase() + ".groovy";
                    scriptFile = getFile(MainBase.main.release.serviceName + "/config/qradarpull/" + scriptFileName);
                }
                
                Map<String, Object> entry = new HashMap<String, Object>();
                
                putFilterDataIntoEntry(entry, qradarpullFilter);
                
                filters.add(entry);
                
                if (scriptFile != null) {
                    try {
                        FileUtils.writeStringToFile(scriptFile, groovy, "UTF-8");
                    } catch (Exception e) {
                        Log.log.warn(e.getMessage() + ". Skipping file: " + scriptFile.getAbsolutePath());
                    }
                }
            }
            xdoc.setListMapValue(RECEIVE_QRADARPULL_FILTER, filters);
        }
        if (nameProperties != null && isActive()) {
            nameProperties.clear();
            for (String name : QRadarPullGateway.getInstance().getNameProperties().keySet()) {
                Map<String, Object> entry = new HashMap<String, Object>();
                entry.put(NAME_KEY, name);
                nameProperties.add(entry);
                Map props = QRadarPullGateway.getInstance().getNameProperties().getNameProperties(name);
                ObjectProperties prop = new ObjectProperties(props);
                prop.save(props);
            }
        }
        saveAttributes();
    }

    private void putFilterDataIntoEntry(Map<String, Object> entry, QRadarPullFilter qradarpullFilter) {
        
        entry.put(QRadarPullFilter.ID, qradarpullFilter.getId());
        entry.put(QRadarPullFilter.ACTIVE, String.valueOf(qradarpullFilter.isActive()));
        entry.put(QRadarPullFilter.ORDER, String.valueOf(qradarpullFilter.getOrder()));
        entry.put(QRadarPullFilter.INTERVAL, String.valueOf(qradarpullFilter.getInterval()));
        entry.put(QRadarPullFilter.EVENT_EVENTID, qradarpullFilter.getEventEventId());
        entry.put(QRadarPullFilter.RUNBOOK, qradarpullFilter.getRunbook());
        entry.put(QRadarPullFilter.SCRIPT, qradarpullFilter.getScript());
        entry.put(QRadarPullFilter.QUERY, qradarpullFilter.getQuery());
        entry.put(QRadarPullFilter.LAST_OFFENSE_ID, qradarpullFilter.getLastOffenseId());
        entry.put(QRadarPullFilter.LAST_START_TIME, qradarpullFilter.getLastStartTime());
    }
}

