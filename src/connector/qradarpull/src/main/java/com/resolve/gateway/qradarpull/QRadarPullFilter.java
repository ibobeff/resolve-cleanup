package com.resolve.gateway.qradarpull;

import com.resolve.gateway.BaseFilter;
import com.resolve.gateway.RetainValue;

public class QRadarPullFilter extends BaseFilter {

    public static final String QUERY = "QUERY";
    
    public static final String SORT = "SORT";
    
    public static final String LAST_OFFENSE_ID = "LAST_OFFENSE_ID";
    
    public static final String LAST_START_TIME = "LAST_START_TIME";
    
    public static final String ID_FIELD = "id";
    
    public static final String LAST_UPDATED_TIME_FIELD = "start_time";

    private String query;

    private String lastOffenseId = null;

    private String lastStartTime = Long.toString(System.currentTimeMillis());
    
    private String sort;

    public QRadarPullFilter(String id, String active, String order, String interval, String eventEventId, String runbook, String script, String query, String sort) {
        super(id, active, order, interval, eventEventId, runbook, script);
        this.query = query;
        this.sort = sort;
    }

    public String getQuery() {
        return this.query;
    }

    public void setQuery(String query) {
        this.query = query;
    }
    
    public String getSort() {
		return sort;
	}

	public void setSort(String sort) {
		this.sort = sort;
	}

	@RetainValue
    public String getLastOffenseId()
    {
        return lastOffenseId;
    }

    public void setLastOffenseId(String lastOffenseId)
    {
        this.lastOffenseId = lastOffenseId;
    }

    @RetainValue
    public String getLastStartTime()
    {
        return lastStartTime;
    }

    public void setLastStartTime(String lastStartTime)
    {
        this.lastStartTime = lastStartTime;
    }
}

