package com.resolve.gateway.qradarpull;

import java.net.URLEncoder;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;
import com.resolve.gateway.AbstractGatewayAPI;
import com.resolve.util.Log;
import com.resolve.util.StringUtils;
import com.resolve.util.restclient.RestCaller;
import net.sf.json.JSONArray;
import net.sf.json.JSONObject;
import net.sf.json.JSONSerializer;

/**
 * A {@code QRadarPullAPI} is a client for QRadar RESTFul API endpoints. Use this class in an Action
 * Task or Groovy Script to request and post data to QRadar SIEM.
 * 
 * <p>
 * {@code QRadarPullAPI} is required to be initialized with QRadar console's hostname, port and an
 * authentication token. The class is initialized with values taken from corresponding QRadar Pull
 * Gateway blueprint properties when RSRemote is started. Resolve developers do not need to
 * initialize the class in the Action Task. The hostname and port are required fields used to build
 * the URL. The authentication token is required to be included in every API request as an HTTP
 * header for authentication.
 * </p>
 * Below is sample groovy code to demonstrate usage of methods of this class. It calls the
 * {@link #getOffenses(Integer, Integer, String, String)} method to retrieve offenses as a List
 * </p>
 * <p>
 * 
 * <pre>
 * import com.resolve.gateway.qradarpull.QRadarPullAPI;
 * 
 * def offenses_list = QRadarPullAPI.getOffenses(null,null,null,null);
 * </pre>
 * </p>
 * <p>
 * All params are optional, therefore null values are passed in this example. The above call
 * retrieves a list of open offenses on QRadar SIEM as a List of Map<String,String> objects and
 * copies it into the {@code offenses_list} variable
 * </p>
 * <p>
 * Large result sets might take a long time to retrieve. You can use the {@code start} and
 * {@code end} parameter to build the Range header parameter which can limit results and speed up
 * retrieval. For example you set {@code start} to 0 and {@code end} to 49 to fetch first 50
 * offenses. Then, you set start to 50 and end to 99 to fetch the next 50 offenses as demonstrated
 * below:
 * </p>
 * 
 * <pre>
 * import com.resolve.gateway.qradarpull.QRadarPullAPI;
 * 
 * def start  = 0;
 * def end    = 49;
 * 
 * do{
 *      def offenses_list = QRadarPullAPI.getOffenses(start,end,null,null);
 * 
 *      &#47;&#47;Do something with offenses_list
 *      ....
 *      &#47;&#47;Increment start and end pointers
 *      start+=50;
 *      end+=50;
 * 
 * }
 * while(offenses_list.size()>0);
 * 
 * </pre>
 * <p>
 * You can also use the {@code fields} parameter to specify which key/value pairs you'd like in the
 * offense Map.It can be used to weed out the keys not required in the response. In the below
 * example we demostrate this by including only id, description and event_count.
 * </p>
 * 
 * <pre>
 * import com.resolve.gateway.qradarpull.QRadarPullAPI;
 * 
 * def start  = 0;
 * def end    = 49;
 * def fields = "id,description,event_count";
 * 
 *     def offenses_list = QRadarPullAPI.getOffenses(start,end,fields,null);
 * </pre>
 * <p>
 * The above call retrieves a list of open offenses on QRadar SIEM as a List of Map<String,String>
 * objects.The Map will only contain the keys id, description and event_count
 * <p>
 * <p>
 * Use {@code filter} to restrict the elements in the offense list based on the contents of various
 * keys or {@code fields}.In this example we use it to include only those offenses whose event_count
 * is greater than 3
 * </p>
 * 
 * <pre>
 * {@code 
 * import com.resolve.gateway.qradarpull.QRadarPullAPI;
 * 
 * def start  = 0;
 * def end    = 49;
 * def fields = "id,description,event_count";
 * def filter = "event_count>3";
 * 
 *  def offenses_list = QRadarPullAPI.getOffenses(start,end,fields,filter);
 * }
 * </pre>
 * <p>
 * You can read more about {@code field} and {@code filter} params in QRadar RESTFul API
 * Documentation
 * </p>
 * 
 * @author anirudh.sethi
 *
 */

public class QRadarPullAPI extends AbstractGatewayAPI {
  private enum StatusCode {
    OPEN, HIDDEN, CLOSED;

    public static boolean contains(String s) {
      for (StatusCode code : values())
        if (code.name().equals(s))
          return true;
      return false;
    }
  }

  private static String HOSTNAME = "";
  private static String PORT = "";
  private static String SSL = "";
  private static String SEC = "";
  private static String SIEM_URI = "/api/siem";
  private static String ARIEL_URI = "/api/ariel/searches";

  private static QRadarPullGateway instance = QRadarPullGateway.getInstance();

  private static String baseUrl = null;
  private static String baseArielUrl = null;
  private static String baseSiemUrl = null;
  private static boolean selfSigned = true;

  static {
    try {
      Map<String, Object> properties = instance.loadSystemProperties();

      HOSTNAME = (String) properties.get("HOST");
      PORT = (String) properties.get("PORT");
      SSL = (String) properties.get("SSL");
      SEC = (String) properties.get("SEC");

      // QRadar only support HTTPS, either self-signed or CA-signed, default is
      // self-signed.
      selfSigned =
          (SSL == null || SSL.length() == 0 || SSL.equalsIgnoreCase("self-signed")) ? true : false;

      StringBuilder sb = new StringBuilder();

      sb.append("https://").append(HOSTNAME);
      if (StringUtils.isNotBlank(PORT))
        sb.append(":").append(PORT);

      baseUrl = sb.toString();
      baseArielUrl = baseUrl + ARIEL_URI;
      baseSiemUrl = baseUrl + SIEM_URI;

    } catch (Exception e) {
      Log.log.error(e.getMessage(), e);
    }
  }

  static List<Map<String, String>> retrieveOffenses(Long startTime, Long endTime, String status,
	      Integer start, Integer end, String fields, String query) throws Exception {
	  return retrieveOffenses(startTime, endTime, status, start, end, fields, query, null);
  }
  /**
   * Retrieve offenses in a given time range with specific status code via SIEM API.
   * 
   * @param startTime: Required - Long value of the start time in epoch format, included
   * @param endTime: Required - Long value of the end time in epoch format, excluded
   * @param status: Required - The status value of the offense to be pulled
   * @param start: Optional - Integer value of the start item in range
   * @param end: Optional - Integer value of the end item in range e.g., items=0-5
   * @param fields: Optional - Use this parameter to specify which fields you would like to get back
   *        in the response. Fields that are not named are excluded. Specify subfields in brackets
   *        and multiple fields in the same object are separated by commas. e.g.,
   *        field_one(field_two,field_three),field_four
   * @param query: Optional - A custom query to to used instead of the default query with the
   *        parameters provided e.g., field_one = "String" and field_two > 42 or not field_three in
   *        (1, 2, 3) start_time > 1483583844840 and start_time < 1487610374318
   * @return A list of offense details
   * @throws Exception
   */
  static List<Map<String, String>> retrieveOffenses(Long startTime, Long endTime, String status,
      Integer start, Integer end, String fields, String query, String sort) throws Exception {

    StringBuilder sb = new StringBuilder();

    String filter = query;

    if (StringUtils.isEmpty(query)) {
      if (startTime != null && startTime.longValue() > 0)
        sb.append("start_time >= " + startTime.toString());

      if (endTime != null && endTime.longValue() > 0)
        sb.append("start_time < " + endTime.toString());

      if (StringUtils.isNotEmpty(status))
        sb.append(" AND status = " + status);

      filter = sb.toString();
    }
    	
    Log.log.debug("filter = " + filter);

    return getOffenses(start, end, fields, filter,sort);
  }
  
  public static List<Map<String, String>> getOffenses(Integer start, Integer end, String fields,
	      String filter) throws Exception {
	  return getOffenses(start, end, fields, filter, null);
  }
  /**
   * <p>
   * Retrieve a list of offenses from QRadar System configured in blueprint. For more details click
   * <a href=
   * "https://www.ibm.com/support/knowledgecenter/en/SS42VS_7.3.1/com.ibm.qradar.doc/9.1--siem-offenses-GET.html">here</a>
   * </p>
   * 
   * @param start Optional - Large result sets might take a long time to retrieve.You can use the
   *        start parameter to build a range header parameter that limits the results and speeds up
   *        retrieval. For example, If you need the first 50 offenses you set the start parameter to
   *        0. Then, you set the parameter to 50 if you want to retrieve the next 50 offenses, and
   *        so on.
   * @param end: Optional - Use this parameter to restrict the number of elements that are returned
   *        in the list to a specified range.
   * @param fields: Optional - Use this parameter to specify which fields you would like to get back
   *        in the response. Fields that are not named are excluded. Specify subfields in brackets
   *        and multiple fields in the same object are separated by commas. e.g.,
   *        field_one(field_two,field_three),field_four
   * @param filter: Optional - This parameter is used to restrict the elements in a list base on the
   *        contents of various fields.
   * @return a list of offense details
   * @throws Exception
   */
  public static List<Map<String, String>> getOffenses(Integer start, Integer end, String fields,
      String filter, String sort) throws Exception {

    String resp = "";
    List<Map<String, String>> offenses = null;

    Map<String, String> headers = new HashMap<String, String>();
    headers.put("sec", SEC);

    if (start != null && end != null) {
      String range = "items=" + start.toString() + "-" + end.toString();
      headers.put("range", range);
    }

    Map<String, String> params = new HashMap<String, String>();

    if (fields != null && fields.length() != 0)
      params.put("fields", fields);

    if (filter != null && filter.length() != 0)
      params.put("filter", filter);
    
    if(StringUtils.isNotBlank(sort))
    	params.put("sort", sort);
    RestCaller client = new RestCaller(baseSiemUrl, "", "", false, true, selfSigned, false);

    try {
      resp = client.callGet("/offenses", params, headers);

      Log.log.debug(resp);

      if (StringUtils.isNotEmpty(resp))
        offenses = getJsonArrayMap(resp);
    } catch (Exception e) {
      Log.log.error(e.getMessage(), e);
      throw e;
    }

    return offenses;
  }

  private static List<Map<String, String>> getJsonArrayMap(String input) throws Exception {

    List<Map<String, String>> list = new ArrayList<Map<String, String>>();

    JSONArray jsonArray = (JSONArray) JSONSerializer.toJSON(input);
    for (int i = 0; i < jsonArray.size(); i++) {
      Map<String, Object> attr = (Map<String, Object>) jsonArray.get(i);
      Set<String> keySet = attr.keySet();

      Map<String, String> offense = new HashMap<String, String>();
      for (Iterator<String> it = keySet.iterator(); it.hasNext();) {
        String key = it.next();
        String value = attr.get(key).toString();

        offense.put(key, value);
      }

      list.add(offense);
    }

    return list;
  }

  static String parseOffenseId(String offense) {

    if (StringUtils.isBlank(offense))
      return "";

    String id = "";

    try {
      JSONObject json = JSONObject.fromObject(offense);
      id = json.getString("id");
      Log.log.debug("offense id = " + id);
    } catch (Exception e) {
      Log.log.error(e.getMessage(), e);
    }

    return id;
  }

  /**
   * Retrieve a list of notes for an offense.
   * 
   * @param offenseId: Required - The ID of the offense.
   * @param fields: Optional - Use this parameter to specify which fields you would like to get back
   *        in the response. Fields that are not named are excluded. Specify subfields in brackets
   *        and multiple fields in the same object are separated by commas. e.g.,
   *        field_one(field_two,field_three),field_four
   * @param filter: Optional - This parameter is used to restrict the elements in a list base on the
   *        contents of various fields. * @param start: Optional - Use this parameter to restrict
   *        the number of elements that are returned in the list to a specified range. The list is
   *        indexed starting at zero.
   * @param end: Optional - Use this parameter to restrict the number of elements that are returned
   *        in the list to a specified range.
   * @return a list of offense notes
   * @throws Exception
   */
  public static String getOffenseNotes(String offenseId, String fields, String filter,
      Integer start, Integer end) throws Exception {

    if (offenseId == null || offenseId.length() == 0)
      throw new Exception("No offense id is provided.");

    String resp = "";

    Map<String, String> headers = new HashMap<String, String>();
    headers.put("sec", SEC);

    String requestUrl = "/offenses/" + offenseId + "/notes";

    if (fields != null && fields.length() != 0) {
      if (!requestUrl.contains("?"))
        requestUrl = requestUrl + "?";
      else
        requestUrl = requestUrl + "&";
      requestUrl = requestUrl + "?" + URLEncoder.encode(fields, "UTF-8");
    }

    if (filter != null && filter.length() != 0) {
      if (!requestUrl.contains("?"))
        requestUrl = requestUrl + "?";
      else
        requestUrl = requestUrl + "&";
      requestUrl = requestUrl + "filter=" + URLEncoder.encode(filter, "UTF-8");
    }

    if (start != null && end != null) {
      try {
        String range = "items=" + start.toString() + "-" + end.toString();
        headers.put("range", range);
      } catch (Exception e) {
        Log.log.error(e.getMessage(), e);
      }
    }

    RestCaller client = new RestCaller(baseSiemUrl, "", "", false, true, selfSigned, false);

    try {
      resp = client.callGet(requestUrl, null, headers);
      Log.log.debug(resp);
    } catch (Exception e) {
      Log.log.error(e.getMessage(), e);
      throw e;
    }

    return resp;
  }

  /**
   * Update offense per offense id and parameters
   * 
   * @param offenseId: Required - The ID of the offense to update.
   * @param status: Optional - The new status for the offense. Set to one of: OPEN, HIDDEN, CLOSED.
   *        When the status of an offense is being set to CLOSED, a valid closing_reason_id must be
   *        provided. To hide an offense, use the HIDDEN status. To show a previously hidden
   *        offense, use the OPEN status.
   * @param closingReasonId: Optional - The ID of a closing reason. You must provide a valid
   *        closing_reason_id when you close an offense.
   * @return A JSON string of updated offense detail information
   * @throws Exception
   */
  public static String postOffense(String offenseId, String status, Integer closingReasonId)
      throws Exception {

    return postOffense(offenseId, null, null, status, closingReasonId, null, null);
  }

  /**
   * Update offense per offense id and parameters
   * 
   * @param offenseId: Required - The ID of the offense to update.
   * @param isProtected: Optional - Set to true to protect the offense.
   * @param followUp: Optional - Set to true to set the follow up flag on the offense.
   * @param status: Optional - The new status for the offense. Set to one of: OPEN, HIDDEN, CLOSED.
   *        When the status of an offense is being set to CLOSED, a valid closing_reason_id must be
   *        provided. To hide an offense, use the HIDDEN status. To show a previously hidden
   *        offense, use the OPEN status.
   * @param closingReasonId: Optional - The ID of a closing reason. You must provide a valid
   *        closing_reason_id when you close an offense.
   * @param assignedTo: Optional - A user to assign the offense to.
   * @param fields: Optional - Use this parameter to specify which fields you would like to get back
   *        in the response. Fields that are not named are excluded. Specify subfields in brackets
   *        and multiple fields in the same object are separated by commas.
   * @return A JSON string of updated offense detail information
   * @throws Exception
   */
  public static String postOffense(String offenseId, Boolean isProtected, Boolean followUp,
      String status, Integer closingReasonId, String assignedTo, String fields) throws Exception {

    if (offenseId == null || offenseId.length() == 0)
      throw new Exception("No offense id is provided.");

    if (StringUtils.isNotBlank(status) && (!status.equalsIgnoreCase(StatusCode.OPEN.toString()))
        && !status.equalsIgnoreCase(StatusCode.HIDDEN.toString())
        && !status.equalsIgnoreCase(StatusCode.CLOSED.toString()))
      throw new Exception("Status code is invalid.");

    if (StringUtils.isNotBlank(status) && status.equalsIgnoreCase(StatusCode.CLOSED.toString())
        && closingReasonId == null)
      throw new Exception("Closed reason id must be provided.");

    Map<String, String> headers = new HashMap<String, String>();
    headers.put("sec", SEC);

    String resp = "";
    Map<String, String> params = new HashMap<String, String>();

    if (StringUtils.isNotBlank(status))
      params.put("status", status);

    if (StringUtils.isNotBlank(assignedTo))
      params.put("assigned_to", assignedTo);

    if (StringUtils.isNotBlank(fields))
      params.put("fields", fields);

    if (isProtected != null)
      params.put("protected", isProtected.toString());

    if (followUp != null)
      params.put("follow_up", followUp.toString());

    if (closingReasonId != null)
      params.put("closing_reason_id", closingReasonId.toString());

    RestCaller client = new RestCaller(baseSiemUrl, "", "", false, true, selfSigned, false);

    try {
      resp = client.callPost("/offenses/" + offenseId, params, null, headers);

      Log.log.debug(resp);
    } catch (Exception e) {
      // e.printStackTrace();
      Log.log.error(e.getMessage(), e);
      throw e;
    }

    return resp;
  }

  /**
   * Create a note on an offense.
   * 
   * @param offenseId: Required - The offense ID to add the note to.
   * @param note: Required - The note text.
   * @param fields: Optional - Use this parameter to specify which fields you would like to get back
   *        in the response. Fields that are not named are excluded. Specify subfields in brackets
   *        and multiple fields in the same object are separated by commas.
   * @return The Note object that was created in JSON.
   * @throws Exception
   */
  public static String postOffenseNote(String offenseId, String note, String fields)
      throws Exception {

    if (offenseId == null || offenseId.length() == 0 || note == null || note.length() == 0)
      throw new Exception("No offense id or note is provided.");

    String resp = "";

    Map<String, String> headers = new HashMap<String, String>();
    headers.put("sec", SEC);

    String requestUrl =
        "/offenses/" + offenseId + "/notes?note_text=" + URLEncoder.encode(note, "UTF-8");

    if (fields != null && fields.length() != 0)
      requestUrl = requestUrl + "&fields=" + URLEncoder.encode(fields, "UTF-8");

    RestCaller client = new RestCaller(baseSiemUrl, "", "", false, true, selfSigned, false);

    try {
      resp = client.callPost(requestUrl, null, null, headers);

      Log.log.debug(resp);
    } catch (Exception e) {
      // e.printStackTrace();
      Log.log.error(e.getMessage(), e);
      throw e;
    }

    return resp;
  }

  /**
   * Creates a new Ariel search as specified by the Ariel Query Language (AQL) query expression.
   * Searches are executed synchronously.
   * 
   * @param query: Required - The AQL query to execute.
   * @param fields: Optional - Use this parameter to specify which fields you would like to get back
   *        in the response. Fields that are not named are excluded. Specify subfields in brackets
   *        and multiple fields in the same object are separated by commas.
   * @param retry: Required - The number of times to retry, every retry takes 1 second, before
   *        receiving status to "COMPLETED" or "ERROR" when the search is asynchronous
   * @param interval: Required - The number of seconds to wait for each retry.
   * @param start: Required - Integer value of the start item in range
   * @param end: Required - Integer value of the end item in range e.g., items=0-5, if the result
   *        set is dynamic, caller must use a loop to retrieve all the records
   * @return a JSON string with a list of event details
   * @throws Exception
   */
  @Deprecated
  public static String executeQuery(String query, String fields, int retry, Integer start,
      Integer end) throws Exception {

    return executeQuery(query, retry, 1, start, end);
  }

  /**
   * Creates a new Ariel search as specified by the Ariel Query Language (AQL) query expression.
   * Searches are executed synchronously.
   * 
   * @param query: Required - The AQL query to execute.
   * @param retry: Required - The number of times to retry, every retry takes 1 second, before
   *        receiving status to "COMPLETED" or "ERROR" when the search is asynchronous
   * @param interval: Required - The number of seconds to wait for each retry.
   * @param start: Required - Integer value of the start item in range
   * @param end: Required - Integer value of the end item in range e.g., items=0-5, if the result
   *        set is dynamic, caller must use a loop to retrieve all the records
   * @return a JSON string with a list of event details
   * @throws Exception
   */
  public static String executeQuery(String query, int retry, int interval, Integer start,
      Integer end) throws Exception {

    if (StringUtils.isBlank(query))
      throw new Exception("No query is provided.");

    if (start == null)
      throw new Exception("start number is not provided.");

    if (end == null)
      throw new Exception("end number id is not provided.");

    String resp = "";

    Map<String, String> headers = new HashMap<String, String>();
    headers.put("sec", SEC);

    Map<String, String> params = new HashMap<String, String>();

    params.put("query_expression", query);

    RestCaller client = new RestCaller(baseArielUrl, "", "", false, true, selfSigned, false);

    try {
      resp = client.callPost("", params, null, headers);
      Log.log.debug(resp);
      if (StringUtils.isNotEmpty(resp)) {
        if (resp.indexOf("search_id") != -1) {
          JSONObject json = JSONObject.fromObject(resp);
          String searchId = json.getString("search_id");
          Log.log.debug("searchId = " + searchId);
          if (StringUtils.isNotEmpty(searchId))
            return executeAsyncQuery(query, retry, interval, searchId, start, end);
        }

        else
          throw new Exception("Search id not found.");
      }
    } catch (Exception e) {
      Log.log.error(e.getMessage(), e);
      throw e;
    }

    return resp;
  }

  /**
   * Creates a new Ariel search as specified by the Ariel Query Language (AQL) query expression.
   * Searches are executed asynchronously.
   * 
   * @param query: Required - The AQL query to execute.
   * @param fields: Optional - Use this parameter to specify which fields you would like to get back
   *        in the response. Fields that are not named are excluded. Specify subfields in brackets
   *        and multiple fields in the same object are separated by commas.
   * @return a string of search id
   * @throws Exception
   * 
   * @deprecated use {@link #createSearch(String)}}
   */
  @Deprecated
  public static String createSearch(String query, String fields) throws Exception {

    return createSearch(query);
  }

  /**
   * Creates a new Ariel search
   * 
   * @param query the AQL query to execute
   * @return search id of the search created. Example - 03a3d605-800f-4e9a-b39c-9e282db3a855
   * @throws Exception - if the {@code query} string does not contain any text or is {@code null}
   */
  public static String createSearch(String query) throws Exception {

    if (StringUtils.isBlank(query))
      throw new Exception("No query is provided.");

    String searchId = "";

    Map<String, String> headers = new HashMap<String, String>();
    headers.put("sec", SEC);

    Map<String, String> params = new HashMap<String, String>();

    params.put("query_expression", query);

    RestCaller client = new RestCaller(baseArielUrl, "", "", false, true, selfSigned, false);

    try {
      String resp = client.callPost("", params, null, headers);
      Log.log.debug(resp);
      if (StringUtils.isNotEmpty(resp) && resp.indexOf("search_id") != -1) {
        JSONObject json = JSONObject.fromObject(resp);
        searchId = json.getString("search_id");
        Log.log.debug("searchId = " + searchId);
      }
    } catch (Exception e) {
      Log.log.error(e.getMessage(), e);
      throw e;
    }

    return searchId;
  }

  private static String executeAsyncQuery(String query, int retry, int interval, String searchId,
      Integer start, Integer end) throws Exception {

    if (StringUtils.isBlank(searchId))
      throw new Exception("No search id is provided.");

    String resp = "";
    int count = 0;
    boolean completed = false;

    for (int i = 0; i < retry + 1; i++) {
      resp = getSearchStatus(searchId);

      if (StringUtils.isNotEmpty(resp)) {
        JSONObject json = JSONObject.fromObject(resp);
        String status = json.getString("status");

        if (StringUtils.isBlank(status))
          throw new Exception("Search status is not available.");

        if (status.equalsIgnoreCase("ERROR"))
          throw new Exception("Search status is error.");

        if (status.equalsIgnoreCase("COMPLETED")) {
          completed = true;
          break;
        }
      }

      Log.log.debug("retry ++ = " + retry);
      Thread.sleep(interval * 1000);
    }

    while (completed) {
      Thread.sleep(interval * 1000);
      Log.log.debug("retry -- = " + retry);
      resp = getSearchResult(searchId, start, end);

      JSONObject json = null;
      JSONArray jsonArray = null;

      try {
        json = JSONObject.fromObject(resp);
        if (json == null) {
          Log.log.error("Result is not in JSON format.");
          break;
        }

        jsonArray = (JSONArray) json.get("events");
      } catch (Exception e) {
        Log.log.error(e.getMessage(), e);
        break;
      }

      if ((jsonArray == null || jsonArray.size() == 0)) {
        if (count != 0)
          break;

        if (retry > 1) {
          retry--;
          continue;
        }

        else
          throw new Exception("Search is timed out.");
      }

      else {
        if (end - start < 0)
          break;

        int size = jsonArray.size();
        count = count + size;

        if (count == end - start + 1)
          break;
      }
    }

    Log.log.debug(resp);
    return resp;
  }

  /**
   * Get the search status given search id. If the status is not "COMPLETE" or "ERROR", use a loop
   * to call this method and wait for the search to complete.
   * 
   * @param searchId: Required - The identifier for an Ariel search
   * @param fields: Optional - Use this parameter to specify which fields you would like to get back
   *        in the response. Fields that are not named are excluded. Specify subfields in brackets
   *        and multiple fields in the same object are separated by commas
   * @return Information about the specified search, including the search status
   * @throws Exception
   */
  @Deprecated
  public static String getSearchStatus(String searchId, String fields) throws Exception {

    return getSearchStatus(searchId);
  }

  /**
   * Get the search status given search id. If the status is not "COMPLETE" or "ERROR", use a loop
   * to call this method and wait for the search to complete.
   * 
   * @param searchId: Required - The identifier for an Ariel search Fields that are not named are
   *        excluded. Specify subfields in brackets and multiple fields in the same object are
   *        separated by commas
   * @return Information about the specified search, including the search status
   * @throws Exception
   */
  public static String getSearchStatus(String searchId) throws Exception {

    if (StringUtils.isBlank(searchId))
      throw new Exception("No search id is provided.");

    String status = "";

    Map<String, String> headers = new HashMap<String, String>();
    headers.put("sec", SEC);

    Map<String, String> params = new HashMap<String, String>();

    if (searchId != null && searchId.length() != 0)
      params.put("search_id", searchId);

    RestCaller client = new RestCaller(baseArielUrl, "", "", false, true, selfSigned, false);

    try {
      status = client.callGet("/" + searchId, params, headers);
      Log.log.debug(status);
    } catch (Exception e) {
      // e.printStackTrace();
      Log.log.error(e.getMessage(), e);
      throw e;
    }

    return status;
  }

  /**
   * The search results for the specified search_id. The result set is limited by the ranges of
   * start number and end number. If multiple pages are needed, use a loop to call this method with
   * the same search id.
   * 
   * @param searchId: Required - The ID of the search criteria for the returned results
   * @param start: Required - Integer value of the start item in range
   * @param end: Required - Integer value of the end item in range Use this parameter to restrict
   *        the number of elements that are returned in the list to a specified range. The list is
   *        indexed starting at zero. e.g., items=0-49
   * @return A string of event details in JSON format
   * @throws Exception
   */
  public static String getSearchResult(String searchId, Integer start, Integer end)
      throws Exception {

    if (StringUtils.isBlank(searchId))
      throw new Exception("No search id is provided.");

    if (start == null)
      throw new Exception("Start number is not provided.");

    if (end == null)
      throw new Exception("End number id is not provided.");

    String resp = "";

    Map<String, String> headers = new HashMap<String, String>();
    headers.put("sec", SEC);
    headers.put("Accept", "application/json");

    String range = "items=0-49";

    if (start != null && end != null) {
      try {
        range = "items=" + start.toString() + "-" + end.toString();
      } catch (Exception e) {
        Log.log.debug(e.getMessage(), e);
      }
    }

    headers.put("range", range);

    Map<String, String> params = new HashMap<String, String>();

    if (searchId != null && searchId.length() != 0)
      params.put("search_id", searchId);

    RestCaller client = new RestCaller(baseArielUrl, "", "", false, true, selfSigned, false);

    try {
      resp = client.callGet("/" + searchId + "/results", params, headers);

      Log.log.debug(resp);
    } catch (Exception e) {
      Log.log.error(e.getMessage(), e);
      throw e;
    }

    return resp;
  }

  /**
   * Retrieve a list of event details associated with the offense per offense id amd a ramge of
   * result set to be retrieved.
   * 
   * @param offenseId: Required - The ID of the offense to retrieve events for
   * @param fields: Optional - Use this parameter to specify which fields you would like to get back
   *        in the response. Fields that are not named are excluded. Specify subfields in brackets
   *        and multiple fields in the same object are separated by commas.
   * @param retry: Required - The query may not return the results immediately, then it will
   *        automatically retry, before the search status is not 'COMPLETED' or 'ERROR'.
   * @param interval: Required - The number of seconds to wait for each retry.
   * @param startTime: Optional - Timestamp for the first start time of the events in format
   *        'YYYY-MM-DD hh:mm:ss'
   * @param stopTime: Optional - Timestamp for the last start time of the events in format
   *        'YYYY-MM-DD hh:mm:ss'
   * @param start: Required - A number indicating the first item of the retrieved event set
   * @param stop: Required - A number indicating the last item of the retrieved event set e.g.,
   *        items=0-5, if the result set is dynamic, caller must use a loop to retrieve all the
   *        records
   * @return A list of event details
   * @throws Exception
   */
  @Deprecated
  public static String getEventsForOffense(String offenseId, String fields, int retry,
      String startTime, String stopTime, Integer start, Integer end) throws Exception {

    return getEventsForOffense(offenseId, retry, 1, startTime, stopTime, start, end);
  }

  /**
   * Retrieve a list of event details associated with the offense per offense id amd a ramge of
   * result set to be retrieved.
   * 
   * @param offenseId: Required - The ID of the offense to retrieve events for
   * @param retry: Required - The query may not return the results immediately, then it will
   *        automatically retry, before the search status is not 'COMPLETED' or 'ERROR'.
   * @param interval: Required - The number of seconds to wait for each retry.
   * @param startTime: Optional - Timestamp for the first start time of the events in format
   *        'YYYY-MM-DD hh:mm:ss'
   * @param stopTime: Optional - Timestamp for the last start time of the events in format
   *        'YYYY-MM-DD hh:mm:ss'
   * @param start: Required - A number indicating the first item of the retrieved event set
   * @param stop: Required - A number indicating the last item of the retrieved event set e.g.,
   *        items=0-5, if the result set is dynamic, caller must use a loop to retrieve all the
   *        records
   * @return A list of event details
   * @throws Exception
   */
  public static String getEventsForOffense(String offenseId, int retry, int interval,
      String startTime, String stopTime, Integer start, Integer end) throws Exception {

    if (StringUtils.isBlank(offenseId))
      throw new Exception("No offense id is provided.");

    if (start == null)
      throw new Exception("Start number is not provided.");

    if (end == null)
      throw new Exception("End number id is not provided.");

    String events = null;

    try {
      StringBuilder sb = new StringBuilder();
      sb.append("SELECT * FROM events WHERE InOffense(").append(offenseId).append(")");

      try {
        SimpleDateFormat format = new SimpleDateFormat("YYYY-MM-DD hh:mm:ss");

        if (StringUtils.isNotEmpty(startTime)) {
          format.parse(startTime);
          sb.append(" START '").append(startTime).append("'");
        }

        if (StringUtils.isNotEmpty(stopTime)) {
          format.parse(stopTime);
          sb.append(" STOP '").append(stopTime).append("'");
        }
      } catch (Exception ee) {
        Log.log.error(ee.getMessage(), ee);
        throw ee;
      }

      String query = sb.toString();
      Log.log.debug("query = " + query);
      events = executeQuery(query, retry, interval, start, end);
    } catch (Exception e) {
      // e.printStackTrace();
      Log.log.error(e.getMessage(), e);
      throw e;
    }

    return events;
  }

  /**
   * Creates a new Ariel search as specified based on the criteria and call executeQuery method to
   * get the result.
   * 
   * @param criteria A map of key/value pairs to compose the Ariel query
   * @param filter A valid Arial query string in addition to the criteria
   * @param fields Use this parameter to specify which fields you would like to get back in the
   *        response. Fields that are not named are excluded. Specify subfields in brackets and
   *        multiple fields in the same object are separated by commas.
   * @param retry The number of times to retry, before receiving status to "COMPLETED" or "ERROR"
   *        when the search is asynchronous
   * @param interval The number of seconds to wait for each retry.
   * @param start Integer value of the start item in range
   * @param end Integer value of the end item in range e.g., items=0-5, if the result set is
   *        dynamic, caller must use a loop to retrieve all the records
   * @return a JSON string with a list of event details
   * @throws Exception
   */
  @Deprecated
  public static String executeDynamicQuery(Map<String, String> criteria, String filter,
      String fields, int retry, Integer start, Integer end) throws Exception {

    return executeDynamicQuery(criteria, filter, retry, 1, start, end);
  }

  /**
   * Fetch all events from Ariel Events Database that match the provided {@code criteria}
   * <p>
   * This API will fetch all columns from the events database table. If you want to filter columns
   * in your search results {@see #executeQuery(String, int, int, Integer, Integer)}
   * </p>
   * 
   * @param criteria @{code Map<String,String>} of key/value pairs to compose the WHERE clause of
   *        the Ariel query
   * @param filter Add addit
   * @param retry The number of times to retry, before receiving status to "COMPLETED" or "ERROR"
   *        when the search is asynchronous
   * @param interval The number of seconds to wait for each retry.
   * @param start Integer value of the start item in range
   * @param end Integer value of the end item in range e.g., items=0-5, if the result set is
   *        dynamic, caller must use a loop to retrieve all the records
   * @return a JSON string with a list of event details
   * @throws Exception - if {@code criteria.start or end} is null or if {@code criteria} is an empty
   *         Map
   */
  public static String executeDynamicQuery(Map<String, String> criteria, String filter, int retry,
      int interval, Integer start, Integer end) throws Exception {

    if (criteria == null || criteria.size() == 0)
      throw new Exception("No criteria id is provided.");

    if (start == null)
      throw new Exception("Start number is not provided.");

    if (end == null)
      throw new Exception("End number id is not provided.");

    StringBuilder sb = new StringBuilder();
    sb.append("SELECT * FROM events WHERE ");

    boolean first = true;
    for (Iterator it = criteria.keySet().iterator(); it.hasNext();) {
      if (first)
        first = false;
      else if (it.hasNext())
        sb.append(" AND ");

      String key = (String) it.next();
      if (StringUtils.isBlank(key))
        continue;

      String value = criteria.get(key);
      sb.append(key).append("='").append(value).append("'");
    }

    if (StringUtils.isNotEmpty(filter))
      sb.append(" ").append(filter);

    String query = sb.toString();
    Log.log.debug("query = " + query);
    return executeQuery(query, retry, interval, start, end);
  }

  /**
   * Retrieve a list of offense source addresses currently in the QRadar system
   * 
   * @param fields Use this parameter to specify which fields you would like to get back in the
   *        response. Fields that are not named are excluded. Specify subfields in brackets and
   *        multiple fields in the same object are separated by commas. It is optional, send
   *        {@code null} if you don't want to specify any value
   * 
   * @param filter This parameter is used to restrict the elements in a list base on the contents of
   *        various fields. It is optional, set {@code null} if you don't want to specify any value
   * @param start,end Use start and end to restrict number of elements returned in the list to a
   *        specified range. The list is indexed starting 0. For example start=0 and end=49 restrict
   *        result to first 50 items in the list
   * @return
   * @throws Exception
   */
  public static String getSourceAddresses(String fields, String filter, Integer start, Integer end)
      throws Exception {
    
    Map<String, String> headers = new HashMap<String, String>();
    Map<String, String> params = new HashMap<String, String>();
    
    headers.put("sec", SEC);
    if(start!=null && end!=null) headers.put("Range","items="+start+"-"+end);
    
    params.put("fields", fields);
    params.put("filter", filter);

    RestCaller client   = new RestCaller(baseSiemUrl, "", "", false, true, selfSigned, false);
    String response = "";
    
    try {
      response = client.callGet("/source_addresses",params,headers);
      Log.log.debug(response);
    } catch (Exception e) {
      Log.log.error(e.getMessage(), e);
      throw e;
    }
    return response;
  }

  /**
   * Retrieve a list offense local destination addresses currently in the Qradar system
   * 
   * @param fields Use this parameter to specify which fields you would like to get back in the
   *        response. Fields that are not named are excluded. Specify subfields in brackets and
   *        multiple fields in the same object are separated by commas. It is optional, send
   *        {@code null} if you don't want to specify any value
   * 
   * @param filter This parameter is used to restrict the elements in a list base on the contents of
   *        various fields. It is optional, set {@code null} if you don't want to specify any value
   * @param start,end Use start and end to restrict number of elements returned in the list to a
   *        specified range. The list is indexed starting 0. For example start=0 and end=49 restrict
   *        result to first 50 items in the list
   * @return
   * @throws Exception
   */
  public static String getLocalDestinationAddresses(String fields, String filter, Integer start,
      Integer end) throws Exception {
    Map<String, String> headers = new HashMap<String, String>();
    Map<String, String> params = new HashMap<String, String>();

    headers.put("sec", SEC);
    if (start != null && end != null)
      headers.put("Range", "items=" + start + "-" + end);

    params.put("fields", fields);
    params.put("filter", filter);

    RestCaller client = new RestCaller(baseSiemUrl, "", "", false, true, selfSigned, false);
    String response = "";

    try {
      response = client.callGet("/local_destination_addresses", params, headers);
      Log.log.debug(response);
    } catch (Exception e) {
      Log.log.error(e.getMessage(), e);
      throw e;
    }
    return response;
  }
  
  /**
   * 
   * @param filter This parameter is used to restrict the elements in a list base on the contents of
   *        various fields. It is optional, set {@code null} if you don't want to specify any value
   * @param fields Use this parameter to specify which fields you would like to get back in the
   *        response. Fields that are not named are excluded. Specify subfields in brackets and
   *        multiple fields in the same object are separated by commas. It is optional, send
   *        {@code null} if you don't want to specify any value
   * @param start,end start,end Use start and end to restrict number of elements returned in the
   *        list to a specified range. The list is indexed starting 0. For example start=0 and
   *        end=49 restrict result to first 50 items in the list
   * @param sort This parameter is used to sort the elements in a list.Use + operator to sort field
   *        in ascending order. For example if you want to sort the response by name, send +name in
   *        the sort parameter. Use - operator to sort fields in descending order
   * @return
   * @throws Exception
   */
  public static String getOffenseTypes(String fields, String filter, Integer start, Integer end,
      String sort) throws Exception {
    
    Map<String, String> headers = new HashMap<String, String>();
    Map<String, String> params = new HashMap<String, String>();
    
    headers.put("sec", SEC);
    if(start!=null && end!=null) headers.put("Range","items="+start+"-"+end);
    
    params.put("fields", fields);
    params.put("filter", filter);
    params.put("sort", sort);

    RestCaller client   = new RestCaller(baseSiemUrl, "", "", false, true, selfSigned, false);
    String response = "";
    
    try {
      response = client.callGet("/offense_types",params,headers);
      Log.log.debug(response);
    } catch (Exception e) {
      Log.log.error(e.getMessage(), e);
      throw e;
    }
    return response;
  }

  public static String lookupOffenseType(int code) {

    String type = "";

    switch (code) {
      case -1:
        type = "NO_OP_MAPPER";
        break;
      case 0:
        type = "Y_ATTACKER";
        break;
      case 1:
        type = "BY_TARGET";
        break;
      case 2:
        type = "BY_QID";
        break;
      case 3:
        type = "BY_USERNAME";
        break;
      case 4:
        type = "BY_SRC_MAC";
        break;
      case 5:
        type = "BY_DST_MAC";
        break;
      case 6:
        type = "BY_DEVICE";
        break;
      case 7:
        type = "BY_HOSTNAME";
        break;
      case 8:
        type = "BY_SRC_PORT";
        break;
      case 9:
        type = "BY_DST_PORT";
        break;
      case 10:
        type = "BY_SRC_IPV6";
        break;
      case 11:
        type = "BY_DST_IPV6";
        break;
      case 12:
        type = "BY_SRC_ASN";
        break;
      case 13:
        type = "BY_DST_ASN";
        break;
      case 14:
        type = "BY_RULE";
        break;
      case 15:
        type = "BY_APP_ID";
        break;
      case 16:
        type = "BY_SRC_IDENTITY";
        break;
      case 17:
        type = "BY_DST_IDENTITY";
        break;
      case 18:
        type = "BY_SEARCH_RESULT";
        break;
      default:
        break;
    }

    return type;
  }

  // =============================================================================================

  private static void main1(String[] args) {

    SEC = "7785add3-5dde-4c3c-991a-d0b5a048c7a8"; // 69
    // SEC = "765e6483-4620-4c93-9f2e-92940cf77b94"; // 68
    // SEC = "7c62cba8-bda7-4e4e-99f1-42e052fc32ca"; // 66

    try {
      // System.out.println("1. Get offenses ...");
      // String query = "start_time > 1483583844840";
      // List<Map<String, String>> offenses = getOffenses(0, 2, null, query);

      System.out.println("2. Update offenses ...");
      // String resp2 = postOffense("32", "OPEN", 5);
      String resp2 = postOffense("111", null, null, null, null, null, null);
      System.out.println(resp2);
      /*
       * System.out.println("3. Excute query ..."); String resp3 =
       * executeQuery("SELECT * FROM events WHERE InOffense(27) AND starttime > 1483583844840" , 5,
       * 2, 0, 49); System.out.println(resp3);
       * 
       * System.out.println("4. Get events for offense ..."); String resp4 =
       * getEventsForOffense("36", 5, 2, null, null, 0, 49); System.out.println(resp4);
       * 
       * System.out.println("5. ExecuteDynamicQuery ..."); Map<String, String> conditions = new
       * HashMap<String, String>(); conditions.put("sourceip", "10.50.1.66"); //
       * conditions.put("destinationip", "127.0.0.1"); // conditions.put("magnitude ", "6"); //
       * conditions.put("username ", "admin");
       * 
       * String resp5 = executeDynamicQuery(conditions,
       * "START '2017-03-20 00:00' STOP '2017-03-21 00:00'", 10, 2, 0, 2);
       * System.out.println(resp5);
       */
    } catch (Exception e) {
      System.out.println("Message = " + e.getMessage());
      // e.printStackTrace();
    }
  } // main

} // QRadarPullAPI
