package com.resolve.services.hibernate.vo;

import com.resolve.util.MappingAnnotation;

public class QRadarPullFilterVO extends GatewayFilterVO {

    private static final long serialVersionUID = 1L;

    public QRadarPullFilterVO() {
    }

    private String UQuery;
    private String USort;

    @MappingAnnotation(columnName = "QUERY")
    public String getUQuery() {
        return this.UQuery;
    }

    public void setUQuery(String uQuery) {
        this.UQuery = uQuery;
    }
    
    @MappingAnnotation(columnName = "SORT")
    public String getUSort() {
    	return this.USort;
    }
    
    public void setUSort(String uSort) {
    	this.USort = uSort;
    }
    
    @Override
    public int hashCode()
    {
        final int prime = 31;
        int result = super.hashCode();
        result = prime * result + ((UQuery == null) ? 0 : UQuery.hashCode());
        result = prime * result + ((USort == null) ? 0 : USort.hashCode());
        return result;
    }
}

