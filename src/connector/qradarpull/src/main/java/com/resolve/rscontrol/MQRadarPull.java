package com.resolve.rscontrol;

import com.resolve.persistence.model.QRadarPullFilter;

public class MQRadarPull extends MGateway {

    private static final String MODEL_NAME = QRadarPullFilter.class.getSimpleName();

    protected String getModelName() {
        return MODEL_NAME;
    }
}

