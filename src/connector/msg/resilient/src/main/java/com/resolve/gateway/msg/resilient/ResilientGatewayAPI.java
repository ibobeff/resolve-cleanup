package com.resolve.gateway.msg.resilient;

import java.io.BufferedReader;
import java.io.File;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.FileInputStream;

import java.net.URL;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import com.co3.dto.comment.json.CommentDTO;
import com.co3.dto.task.json.TaskDTO;
import com.co3.simpleclient.SimpleClient;
import com.co3.dto.json.FullIncidentDataDTO;
import com.co3.dto.json.IncidentArtifactDTO;
import com.co3.dto.json.IncidentArtifactTypeDTO;
import com.fasterxml.jackson.core.type.TypeReference;

import com.resolve.gateway.AbstractGatewayAPI;
import com.resolve.rsremote.Main;
import com.resolve.util.Log;
import com.resolve.util.StringUtils;

import net.sf.json.JSONArray;
import net.sf.json.JSONObject;

public class ResilientGatewayAPI extends AbstractGatewayAPI
{
    private static String HOSTNAME = "";
    private static String PORT = "";
    private static String USERNAME = "";
    private static String P_ASSWORD = "";

    private static ResilientGateway gateway = ResilientGateway.getInstance();
    
    public static ResilientGatewayAPI instance = null;
    
    private static Map<String, Object> systemProperties = null;
    
    private static SimpleClient client = null;

    private ResilientGatewayAPI() {
        
        try {
            systemProperties = gateway.loadSystemProperties("Resilient");
            
            HOSTNAME = (String)systemProperties.get("HOST");
            PORT = (String)systemProperties.get("PORT");
            USERNAME = (String)systemProperties.get("USER");
            P_ASSWORD = (String)systemProperties.get("PASS");
            
            StringBuffer sb = new StringBuffer();
            sb.append("https://").append(HOSTNAME);
            if(StringUtils.isNotBlank(PORT))
                sb.append(":").append(PORT);

            URL url = new URL(sb.toString());
            String keystore = (String)System.getProperties().get("javax.net.ssl.trustStore");
            String keystorePass = (String)System.getProperties().get("javax.net.ssl.keyStorePassword");
            File keystoreFile = new File(keystore);
            
            String orgName = ((com.resolve.rsremote.ConfigGeneral)Main.main.configGeneral).getOrg();
            
            client = new SimpleClient(url, orgName, USERNAME, P_ASSWORD, keystoreFile, keystorePass);
            client.connect();
        } catch(Exception e) {
          Log.log.error(e.getMessage(), e);
        }
    }
    
    public static ResilientGatewayAPI getInstance() {
        
        if(instance == null)
            instance = new ResilientGatewayAPI();
        
        return instance;
    }

    /**
     * Establish connection to Resilient REST API. The connection is initalized when the RERemote is started. <br>
     * If any of the gateway API calls fails due to the connection, call this method to reconnect.
     * @throws Exception
     */
    public void getRESTConnection() throws Exception {
        
        client.connect();
    }
    
    /**
     * Update incident status given incident id. <br>
     * IncidentREST POST /orgs/{org_id}/incidents
     * @param incidentId: The incident ID of the incident being saved
     * @param status: The incident status, which is the "plan_status" in the super class PartialIncidentDTO
     * @param summary: Resolution summary
     * @return Echos back the incident object of FullIncidentDataDTO (JSON)
     * @throws Exception
     */
    public FullIncidentDataDTO updateIncidentStatus(Integer incidentId, String status, String summary) throws Exception {
        
        String incidentUrl = client.getOrgURL("/incidents/" + incidentId);
        Log.log.debug(incidentUrl);
        
        FullIncidentDataDTO incident = (FullIncidentDataDTO)client.get(incidentUrl, new TypeReference<FullIncidentDataDTO>() {});
        incident.setPlanStatus(status);
        if(summary != null)
            incident.setResolutionSummary(summary);
        
        return client.put(incidentUrl, incident, new TypeReference<FullIncidentDataDTO>() {});
    }
    
    /**
     * Update task status given task id. <br>
     * TaskREST PUT /orgs/{org_id}/tasks/{task_id}
     * @param taskId: The task ID.
     * @param status: The task's status. The possible task status values are available in the constDTO (task_statuses property). 
     * @return Echos back the task object of TaskDTO (JSON)
     * @throws Exception
     */
    public TaskDTO updateTaskStatus(Integer taskId, String status) throws Exception {
        
        String taskUrl = client.getOrgURL("/tasks/" + taskId);
        Log.log.debug(taskUrl);
        
        TaskDTO task = client.get(taskUrl, new TypeReference<TaskDTO>() {});
        task.setStatus(status);

        return client.put(taskUrl, task, new TypeReference<TaskDTO>() {});
    }
    
    /**
     * Creates a new incident comment. <br>
     * IncidentNoteREST POST /orgs/{org_id}/incidents/{inc_id}/comments
     * @param incidentId: The incident ID.
     * @param comment
     * @return Echos back the comment object in a map
     * @throws Exception
     */
    public Map<String, Object> createCommentForIncident(Integer incidentId, String comment) throws Exception {
        
        CommentDTO newIncidentComment = new CommentDTO();
        newIncidentComment.setText(comment);
        
        String incidentCommentUrl = client.getOrgURL("/incidents/" + incidentId + "/comments");
        Map<String, Object> incidentComments = client.post(incidentCommentUrl, newIncidentComment);
        
        return incidentComments;
    }
    
    /**
     * Creates a new task comment. <br>
     * TaskNoteREST GET /orgs/{org_id}/tasks/{task_id}/comments
     * @param taskId: The ID of the task
     * @param comment
     * @return Echos back the comment object in a map
     * @throws Exception
     */
    public Map<String, Object> createCommentForTask(Integer taskId, String comment) throws Exception {
        
        CommentDTO newTaskComment = new CommentDTO();
        newTaskComment.setText(comment);

        String taskCommentUrl = client.getOrgURL("/tasks/" + taskId + "/comments");
        Map<String, Object> taskComments = client.post(taskCommentUrl, newTaskComment);
        
        return taskComments;
    }
    
    /**
     * Gets an individual incident. <br>
     * GET /orgs/{org_id}/incidents/{inc_id}
     * @param incidentId: The incident ID
     * @return The requested incident in FullIncidentDataDTO type.
     * @throws Exception
     */
    public FullIncidentDataDTO getIncidentById(String incidentId) throws Exception {
        
        String relativeURL = client.getOrgURL(String.format("/incidents/%s", incidentId));
        
        Object response = client.get(relativeURL, new TypeReference<FullIncidentDataDTO>(){});
        
        if(response instanceof FullIncidentDataDTO)
            return (FullIncidentDataDTO)response;
        
        else
            throw new Exception("Response is not of type FullIncidentDataDTO");
    }
    
    /**
     * Gets an individual task. <br>
     * GET /orgs/{org_id}/tasks/{task_id}
     * @param taskId: The task ID
     * @return The task object in TaskDTO type.
     * @throws Exception
     */
    public TaskDTO getTaskById(String taskId) throws Exception {
        
        String relativeURL = client.getOrgURL(String.format("/tasks/%s", taskId));
        
        Object response = client.get(relativeURL, new TypeReference<TaskDTO>(){});
        
        if(response instanceof TaskDTO)
            return (TaskDTO)response;
        
        else
            throw new Exception("Response is not of type TaskDTO");
    }
    
    /**
     * Creates a new artifact on an incident. <br>
     * POST /orgs/{org_id}/incidents/{inc_id}/artifacts
     * @param incidentId: The incident ID
     * @param artifactType: The name of the artifact type
     * @param artifactValue: The value of the artifact
     * @return The artifact data. <br>
     * Note that the "value" property can contain multiple values separated by whitespace for artifact types that support multiple values 
     * (the constDTO.artifact_types will return a list of artifact types and each object has an is_multi_aware that indicates whether multiple values are supported). 
     * This capability is useful if you want to include a list of, say, IP addresses that are to be added to the incident.
     * @throws Exception
     */
    public List<Map<String, Object>> createArtifactForIncident(Integer incidentId, String artifactType, String artifactValue) throws Exception {
        
        int artifactTypeId = findArtifactTypeId(client, artifactType);
        
        IncidentArtifactDTO artifact = new IncidentArtifactDTO();
        
        artifact.setType(artifactTypeId);
        artifact.setValue(artifactValue);
       
        String artifactsURI = client.getOrgURL(String.format("/incidents/%d/artifacts", incidentId));
        
        return client.post(artifactsURI, artifact, new TypeReference<List<Map<String, Object>>>(){});
    }
    
    /**
     * Locates an artifact type ID given its name.
     * @param client: The client object to use to make the requests.
     * @param artifactTypeName: The name of the artifact type (e.g. "IP Address", "DNS Name", etc.).  A case insensitive
     * match is done.
     * @return The ID of the artifact type.
     * @throws IllegalArgumentException If the artifact type does not exist.
     */
    public int findArtifactTypeId(SimpleClient client, String artifactTypeName) throws Exception {
        for (IncidentArtifactTypeDTO artifactType : client.getConstData().getArtifactTypes()) {
            if (artifactType.getName().equalsIgnoreCase(artifactTypeName)) {
                return artifactType.getId();
            }
        }
        
        throw new IllegalArgumentException(String.format("Unsupported artifact type:  %s", artifactTypeName));
    }

    private static void main1(String[] args) {
//    public static void main(String[] args) {
        
        int incidentId = 0; // 2096;
        int taskId = 2251222;
        
        try {
            HOSTNAME = "resilient-01.resolvesys.com";
            PORT = null;
            USERNAME = "Jeffrey.Chen@resolvesys.com";
            P_ASSWORD = "Resolve1234";
            
            StringBuffer sb = new StringBuffer();
            sb.append("https://").append(HOSTNAME);
            if(StringUtils.isNotBlank(PORT))
                sb.append(":").append(PORT);

            URL url = new URL(sb.toString());
            String keystore = (String)System.getProperties().get("javax.net.ssl.trustStore");
            String keystorePass = (String)System.getProperties().get("javax.net.ssl.keyStorePassword");
            File keystoreFile = new File(keystore);
            
            client = new SimpleClient(url, "Resolve Systems LLC", USERNAME, P_ASSWORD, keystoreFile, keystorePass);
            client.connect();
        } catch(Exception e) {
          Log.log.error(e.getMessage(), e);
        }
        
        String incidentStr = getInstance().loadFile("C:\\work\\projects\\resilient\\data\\Incident.json");
        incidentId = getInstance().getId(incidentStr, "incident");
        System.out.println("Incident id = " + incidentId);
        
        String taskStr = getInstance().loadFile("C:\\work\\projects\\resilient\\data\\task.json");
        taskId = getInstance().getId(taskStr, "task");
        System.out.println("Task id = " + taskId);
        
        try {
            // 1. Update incident status
            FullIncidentDataDTO incident = ResilientGatewayAPI.getInstance().updateIncidentStatus(incidentId, "C", "asdfa");
            System.out.printf("1. Incident with id: %s is updated to %s.\n", incidentId, incident.getPlanStatus());
            
            // 2. Update task status
            TaskDTO task = ResilientGatewayAPI.getInstance().updateTaskStatus(taskId, "C");
            System.out.printf("2. Task with id: %s is updated to %s.\n", taskId, task.getStatus());
            
            // 3. Create comment for incident
            Map<String, Object> incidentComments = ResilientGatewayAPI.getInstance().createCommentForIncident(incidentId, "asdfasd");
            if(incidentComments.size() != 0) {
                String key = incidentComments.keySet().iterator().next();
                Integer id = (Integer)incidentComments.get(key);
                System.out.printf("3. First incident comment id is: %s.\n", id);
            }
            
            // 4. Create comment for task
            Map<String, Object> taskComments = ResilientGatewayAPI.getInstance().createCommentForTask(taskId, "adstgawet");
            if(taskComments.size() != 0) {
                String key = taskComments.keySet().iterator().next(); 
                Integer id = (Integer)taskComments.get(key);
                System.out.printf("4. First task comment id is: %s.\n", id);
            }
            
            // 5. Create artifact for incident
            List<Map<String, Object>> artifacts = ResilientGatewayAPI.getInstance().createArtifactForIncident(incidentId, "IP Address", "192.168.0.1 192.168.0.2");
            if(artifacts.size() != 0) {
                String key = artifacts.get(0).keySet().iterator().next();
                Integer id = (Integer)artifacts.get(0).get(key);
                System.out.printf("5. First incident artifact id is: %s.\n", id);
            }
        } catch(Exception e) {
            e.printStackTrace();
        }
    }
    
    private String loadFile(String fileName) {
        
        StringBuilder sb = new StringBuilder();
        InputStream input = null;
        BufferedReader reader = null;
        
        try {
            input = new FileInputStream(new File(fileName));
            reader = new BufferedReader(new InputStreamReader(input));
            
            String line = reader.readLine();
            while(line != null) {
                sb.append(line);
                line = reader.readLine();
            }
        } catch(Exception e) {
            e.printStackTrace();
        } finally {
            try {
            if(reader != null)
                reader.close();
            if(input != null)
                input.close();
            } catch(Exception ee) {
                ee.printStackTrace();
            }
        }
        
        return sb.toString();
    }
    
    public Integer getId(String payload, String key) {
        
        Integer id = null;
        
        try {
            Map<String, Object> json = JSONObject.fromObject(payload);
            Object inc = json.get(key);
            if(inc != null && inc instanceof JSONObject) {
                id = (Integer)((JSONObject)inc).get("id");
            }
        } catch(Exception e) {
            Log.log.error(e.getMessage(), e);
        }

        return id;
    }
    
    private List<Integer> getIncidentTypeIds(String payload) {
        
        List<Integer> ids = new ArrayList<Integer>();
        
        try {
            JSONObject json = JSONObject.fromObject(payload);
            JSONArray jsonArray = json.getJSONArray("incident_type_ids");
            
            if(jsonArray == null)
                return null;
            
            for(int i=0; i<jsonArray.size(); i++)
                ids.add((Integer)jsonArray.get(i));
        } catch(Exception e) {
            Log.log.error(e.getMessage(), e);
        }

        return ids;
    }
    
} // class ResilientGatewayAPI