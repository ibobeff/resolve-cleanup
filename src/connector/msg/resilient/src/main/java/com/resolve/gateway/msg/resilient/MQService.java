/******************************************************************************
* (C) Copyright 2014
*
* Resolve Systems, LLC
*
* All rights reserved
* This software is distributed under license and may not be used, copied,
* modified, or distributed without the express written permission of
* Resolve Systems, LLC.
*
******************************************************************************/

package com.resolve.gateway.msg.resilient;

import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import java.util.Set;
import java.util.concurrent.ConcurrentHashMap;

import javax.jms.Connection;
import javax.jms.Queue;
import javax.jms.QueueConnection;
import javax.jms.QueueSession;
import javax.jms.Session;
import javax.jms.TopicConnection;

import com.resolve.gateway.resolvegateway.msg.ConfigReceiveMSGGateway;
import com.resolve.gateway.resolvegateway.msg.MSGGateway;
import com.resolve.gateway.resolvegateway.msg.MSGGatewayFilter;
import com.resolve.util.Log;
import com.resolve.util.StringUtils;

public class MQService
{   
    public static final String AUTO_ACKNOWLEDGE = "AUTO_ACKNOWLEDGE";
    
    private static int retry = 5;
    private static int interval = 1000;
    
    protected static volatile Map<String, Queue> queues = new HashMap<String, Queue>();
    protected static volatile Map<String, QueueConnection> qcs = new ConcurrentHashMap<String, QueueConnection>();
    protected static volatile Map<String, TopicConnection> tcs = new ConcurrentHashMap<String, TopicConnection>();
    protected static volatile Map<String, MQSubscriber> subscribers = new ConcurrentHashMap<String, MQSubscriber>();
    protected static volatile Map<String, MQReceiver> receivers = new ConcurrentHashMap<String, MQReceiver>();

    public static final MQService instance = new MQService();

    private MQService() {}
    
    public static Map<String, MQSubscriber> getSubscribers()
    {
        return subscribers;
    }
    
    public void startQueueReceiver(MSGGatewayFilter filter) throws Exception {

        if(filter == null)
            throw new Exception("Filter is null.");
        
        String filterName = filter.getId();

        if(StringUtils.isBlank(filterName))
            throw new Exception("Filter name is not available.");
        
        String gatewayName = filter.getGatewayName();
        MSGGateway gateway = ConfigReceiveMSGGateway.getGateways().get(gatewayName);
        if(gateway == null) {
            Log.log.error("Failed to start queue receiver because gateway " + gatewayName + "not found");
            return;
        }
        
        MQReceiver receiver = receivers.get(filterName);

        /* This way cannot make the receiver to start listen again after a server down
        if(receiver != null) {
            // This is not working because the async session was still open with its open connection
            // The connection cannot be closed because it is still associated with the queue manager
            // even if the server was down
            receiver.startReceiving();
            // This way basic does not restart to listen after a server down
            receiver.getReceiver().setMessageListener(receiver);
            return;
        } */
        if(receiver != null) {
            Log.log.warn("Restart the receiver that was already started for filter " + filterName);
            receiver.stopReceiving(filter.getId());
        }

        try {
            QueueConnection connection = getQueueConnection(filter);
            
            if(connection == null)
                throw new Exception("MSG connection is null.");
            
            QueueSession session = getQueueSession(filter, connection);
            
            if(session == null)
                throw new Exception("MSG connection session is null.");
            
            String key = getConnectionKey(filter);
            String queueName = filter.getId();
            
            if(StringUtils.isBlank(queueName)) {
                Log.log.warn("Queue name is blank.");
                return;
            }
            
            receiver = new MQReceiver(connection, session, queueName, filterName);
            receiver.startReceiving();

            receivers.put(key, receiver);
            qcs.put(key, connection);
        } catch(Exception e) {
            e.printStackTrace();
            Log.log.error(e.getMessage(), e);
            throw e;
        }
    } // startQueueReceiver()
    
    public void stopQueueReceiver(MSGGatewayFilter filter) throws Exception {

        String queueName = filter.getId();
        String gatewayName = filter.getGatewayName();
        
        MSGGateway gateway = ConfigReceiveMSGGateway.getGateways().get(gatewayName);
        if(gateway == null) {
            Log.log.error("Failed to stop queue receiver because gateway " + gatewayName + "not found");
            return;
        }
        
        String connectionKey = getConnectionKey(filter);
        
        MQReceiver receiver = receivers.get(connectionKey);
        
        if(receiver != null) {
            receiver.stopReceiving(queueName);
            receivers.remove(connectionKey);
        }
    } //stopQueueReceiver()
    
    public void stopTopicSubscriber(MSGGatewayFilter filter) throws Exception {
        
        String topicName = filter.getId();
        String gatewayName = filter.getGatewayName();
        
        MSGGateway gateway = ConfigReceiveMSGGateway.getGateways().get(gatewayName);
        if(gateway == null) {
            Log.log.error("Failed to stop queue receiver because gateway " + gatewayName + "not found");
            return;
        }
        
        String connectionKey = getConnectionKey(filter);
        
        MQSubscriber subscriber = subscribers.get(connectionKey);
        
        if(subscriber == null)
            throw new Exception("Cannot find subscriber with name: " + connectionKey);
        
        subscriber.stopListening(topicName, false); // unsubscribe, read from filter later

        subscribers.remove(connectionKey);
    } //stopTopicSubscriber()
    
    public void clearConnections() {
        
        try {
            Set<String> subs = subscribers.keySet();
            
            for(Iterator it=subs.iterator(); it.hasNext();) {
                String key = (String)it.next();
                MQSubscriber subscriber = subscribers.get(key);
                subscriber.stopListening(key, false);
                subscribers.remove(key);
            }
        
            Set<String> topicConn = tcs.keySet();
            
            for(Iterator it=topicConn.iterator(); it.hasNext();) {
                String key = (String)it.next();
                Connection connection = tcs.get(key);
                if(connection != null) {
                    try {
                        connection.stop();
                        connection.close();
                        connection = null;
                    } catch(Exception e) {
                        System.err.println(e.getMessage());
                    }
                }
                
                tcs.remove(key);
            }
            
            Set<String> recs = receivers.keySet();
            
            for(Iterator it=recs.iterator(); it.hasNext();) {
                String key = (String)it.next();
                MQReceiver receiver = receivers.get(key);
                if(receiver != null) {
                    try {
                        receiver.stopReceiving(key);
                        receivers.remove(key);
                    } catch(Exception e) {
                        Log.log.error(e.getMessage(), e);
                    }
                }
            }
        
            Set<String> queueConn = qcs.keySet();
            
            for(Iterator it=queueConn.iterator(); it.hasNext();) {
                String key = (String)it.next();
                Connection connection = qcs.get(key);
                if(connection != null) {
                    try {
                        connection.stop();
                        connection.close();
                        connection = null;
                    } catch(Exception e) {
                        System.err.println(e.getMessage());
                    }
                }
                
                qcs.remove(key);
            }

        } catch(Exception e) {
            Log.log.error(e.getMessage(), e);
        }
    } // clearConnections()
    
    
    // This method must be implemented with the gateway specific ConnectionFactory library and logic
    // and return a QueueConnection
    protected QueueConnection getQueueConnection(MSGGatewayFilter filter) throws Exception {

        QueueConnection connection = null;
        
/*
 *      ********   SAMPLE CODE TO CREATE AND CONFIGURE A QUEUE CONNECTION  *****************
        JMSGatewayProperties properties = ConfigReceiveJMSGateway.getGatewayProperties(name);

        String user = filter.getUser();
        if(StringUtils.isBlank(user))
            user = properties.getJMSUser();

        // The password defined in the filter that is stored in the database has been encrypted
        // If it is blank, use the password defined in the blueprint.properties
        String passcode = filter.getPass();
        String pass = StringUtils.isBlank(passcode)?properties.getJMSPass():CryptUtils.decrypt(passcode);

        try {
          SampleConnectionFactory factory = new SampleConnectionFactory(getJMSServerUrl(properties, filter));

          factory.setUserName(user);
          factory.setPassword(pass);
          factory.setTrustStore(properties.getSslCertificate());
          factory.setTrustStorePassword(properties.getSslPassword());

          connection = factory.createQueueConnection();

          connection.setExceptionListener(this);
        } catch (Exception e) {
            Log.log.error(e.getMessage(), e);
            throw e;
        }
*/
        return connection;
    }

    // This method must be implemented with the gateway specific QueueSession library and logic
    // and return a QueueSession
    protected QueueSession getQueueSession(MSGGatewayFilter filter, QueueConnection connection) throws Exception {

        QueueSession session = null;

        connection.start();

        String ack = (String)filter.getAttributes().get("ack");
        if(StringUtils.isBlank(ack) || ack.equalsIgnoreCase("AUTO_ACKNOWLEDGE"))
            session = connection.createQueueSession(false, Session.AUTO_ACKNOWLEDGE);
        else
            session = connection.createQueueSession(false, Session.CLIENT_ACKNOWLEDGE);

        return session;
    }
    
  // This method must be implemented for each gateway specifically,
  // which returns a unique key for the JMS connection per gateway filter
   public String getConnectionKey(MSGGatewayFilter filter) {
       return filter.getId();
   }

} // class MQService