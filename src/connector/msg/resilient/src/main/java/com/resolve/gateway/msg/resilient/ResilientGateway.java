package com.resolve.gateway.msg.resilient;

import java.util.Iterator;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

import javax.jms.ExceptionListener;
import javax.jms.JMSException;
import javax.jms.QueueConnection;
import javax.jms.QueueSession;
import javax.jms.Session;

import com.co3.activemq.Co3ActiveMQSslConnectionFactory;
import com.resolve.gateway.Filter;
import com.resolve.gateway.resolvegateway.ServerHeartbeat;
import com.resolve.gateway.resolvegateway.msg.ConfigReceiveMSGGateway;
import com.resolve.gateway.resolvegateway.msg.MSGGateway;
import com.resolve.gateway.resolvegateway.msg.MSGGatewayFilter;
import com.resolve.gateway.resolvegateway.msg.MSGGatewayProperties;
import com.resolve.util.CryptUtils;
import com.resolve.util.Log;
import com.resolve.util.StringUtils;

public class ResilientGateway extends MSGGateway implements Runnable, ExceptionListener {

    public static final String TOPIC="topic";
    public static final String QUEUE="queue";
    
    private static volatile ResilientGateway instance = null;

    private static Map<String, String> serverUrl = new ConcurrentHashMap<String, String>();
    

    private ResilientGateway(ConfigReceiveMSGGateway config) {

        super(config);

        String gatewayName = this.getClass().getSimpleName();
        int index = gatewayName.indexOf("Gateway");
        if(index != -1)
            name = gatewayName.substring(0, index);

        Map<String, MSGGatewayFilter> jmsFilters = getMSGFilters();

        for(Iterator<String> iterator=jmsFilters.keySet().iterator(); iterator.hasNext();) {
            String filterName = iterator.next();
            MSGGatewayFilter filter = jmsFilters.get(filterName);

            if(name.indexOf(((MSGGatewayFilter)filter).getGatewayName()) != -1) {
                if(filters.get(filterName) == null) {
                    filters.put(filterName, jmsFilters.get(filterName));

                    if(filter.isDeployed()) {
                        orderedFilters.add(filter);
                        orderedFiltersMapById.put(filter.getId(), filter);
                    }
                }
            }
        }
        
        ConfigReceiveMSGGateway.getGateways().put(name, this);

//        properties = config.getGatewayProperties(name);
    }

    public static ResilientGateway getInstance(ConfigReceiveMSGGateway config) {

        if (instance == null) {
            instance = new ResilientGateway(config);
        }

        return instance;
    }

    public static ResilientGateway getInstance() {
        if (instance == null) {
            throw new RuntimeException("Resilient Gateway is not initialized correctly.");
        } else {
            return instance;
        }
    }

    @Override
    protected void initialize() {

        queue = config.getQueue().toUpperCase();

        try {
            Log.log.info("Initializing Resilient Listener ...");
            this.gatewayConfigDir = "/config/" + queue.toLowerCase() + "/";

            if(config.isActive() && config.isPrimary()) {

                // Monitor ActiveMQ server connection
                heartBeat = new ServerHeartbeat(this);
                heartBeat.start();
            }
        } catch (Exception e) {
            Log.log.error("Failed to config Resilient Gateway: " + e.getMessage(), e);
        }
    }
    
    @Override
    public void reinitialize() {
        
        super.reinitialize();
        
        //Add customized code here to initilize your server based on configuration and deployed filter data;

        if(isActive() && isPrimary()) {
            // Monitor ActiveMQ server connection
            heartBeat = new ServerHeartbeat(this);
            heartBeat.start();
        }
    }

    @Override
    public void start() {

        Log.log.info("Starting Resilient Gateway ...");

        super.start();
    }
    
    @Override
    public void stop() {

        Log.log.warn("Stopping Resilient gateway ...");

        super.stop();
        
        MQService.instance.clearConnections();
    }
    
    @Override
    protected void stopFilterListeners(Filter filter)
    {
        MSGGatewayFilter msgGatewayFilter = (MSGGatewayFilter)filter;
        String type = (String)msgGatewayFilter.getAttributes().get("type");
        
        try {
            if(type.equalsIgnoreCase(TOPIC))
                MQService.instance.stopTopicSubscriber(msgGatewayFilter);
            
            if(type.equalsIgnoreCase(QUEUE))
                MQService.instance.stopQueueReceiver(msgGatewayFilter);
        } catch (Exception e) {
            Log.log.error(e.getMessage(), e);
        }
    }

    @Override
    protected String getMessageHandlerName() {
        return this.getClass().getSimpleName();
    }
    
    @Override
    public void suspend() {
        
        for(Filter filter : orderedFilters) {
            MSGGatewayFilter msgGatewayFilter = (MSGGatewayFilter)filter;
            String type = (String)msgGatewayFilter.getAttributes().get("type");
            
            try {
                if(type.equalsIgnoreCase(TOPIC))
                    MQService.instance.stopTopicSubscriber(msgGatewayFilter);
                
                if(type.equalsIgnoreCase(QUEUE))
                    MQService.instance.stopQueueReceiver(msgGatewayFilter);
            } catch (Exception e) {
                Log.log.error(e.getMessage(), e);
            }
        }
    }


    protected QueueConnection getQueueConnection(MSGGatewayFilter filter) throws Exception {

        QueueConnection connection = null;

        MSGGatewayProperties properties = ConfigReceiveMSGGateway.getGatewayProperties(name);

        String user = filter.getUser();
        if(StringUtils.isBlank(user))
            user = properties.getMSGUser();

        // The password defined in the filter that is stored in the database has been encrypted
        // If it is blank, use the password defined in the blueprint.properties
        String passcode = filter.getPass();
        String pass = StringUtils.isBlank(passcode)?properties.getMSGPass():CryptUtils.decrypt(passcode);

        try {
          Co3ActiveMQSslConnectionFactory factory = new Co3ActiveMQSslConnectionFactory(getMSGServerUrl(properties, filter));

          factory.setUserName(user);
          factory.setPassword(pass);
          factory.setTrustStore(properties.getSslCertificate());
          factory.setTrustStorePassword(properties.getSslPassword());

          connection = factory.createQueueConnection();
          connection.start();
          connection.setExceptionListener(this);
        } catch (Exception e) {
            Log.log.error(e.getMessage(), e);
            throw e;
        }

        return connection;
    }

    protected QueueSession getQueueSession(MSGGatewayFilter filter, QueueConnection connection) throws Exception {

        QueueSession session = null;

        String ack = (String)filter.getAttributes().get("ack");
        if(StringUtils.isBlank(ack) || ack.equalsIgnoreCase("AUTO_ACKNOWLEDGE"))
            session = connection.createQueueSession(false, Session.AUTO_ACKNOWLEDGE);
        else
            session = connection.createQueueSession(false, Session.CLIENT_ACKNOWLEDGE);

        return session;
    }

    public String getConnectionKey(MSGGatewayFilter filter) {

        return filter.getId();
    }

    public void onException(JMSException e) {
        e.printStackTrace();
    }

    public boolean sendHeartbeat() {
        
        boolean isAlive = true;
        
        QueueConnection connection = null;

        MSGGatewayProperties properties = ConfigReceiveMSGGateway.getGatewayProperties(name);
        
        try {
            Co3ActiveMQSslConnectionFactory factory = new Co3ActiveMQSslConnectionFactory(getMSGServerUrl(properties, null));

            factory.setUserName(properties.getMSGUser());
            factory.setPassword(properties.getMSGPass());
            factory.setTrustStore(properties.getSslCertificate());
            factory.setTrustStorePassword(properties.getSslPassword());

            connection = factory.createQueueConnection();
/*            connection.start();
            connection.setExceptionListener(this);*/
        } catch(Exception e) {
            Log.log.error("Failed to establish connection with resilient ActiveMQ server.");
            Log.log.error(e.getMessage(), e);
            isAlive = false;
        }
        
        return isAlive;
    }
    

    public String getServerUrl(MSGGatewayFilter filter) {
        
        String filterName = filter.getId();
        String gatewayName = filter.getGatewayName();
        String url = serverUrl.get(filterName);
        
        if(url != null)
            return url;

        MSGGatewayProperties properties = ConfigReceiveMSGGateway.getGatewayProperties(gatewayName);
            
        StringBuilder sb = new StringBuilder();

        String host = properties.getHost();
        
        Integer portNum = properties.getPort();
        String port = portNum == null?"":portNum.toString();
        
        Boolean ssl = properties.isSsl();

        if(StringUtils.isBlank(host)) {
            Log.log.error("Host name is not available.");
            return null;
        }
        
        if(ssl)
            sb.append("https://");
        else
            sb.append("http://");
        
        sb.append(host);
        
        if(StringUtils.isNotBlank(port))
            sb.append(":").append(port);
            
        url = sb.toString();
        
        serverUrl.put(filterName, url);
        
        return url;
    }
    
    
    public void removeServerUrl(String filterName) {
        
        serverUrl.remove(filterName);
    }
    
    public String getMSGServerUrl(MSGGatewayProperties properties, MSGGatewayFilter filter) {
        
        String url = null;

        StringBuilder sb = new StringBuilder();
        
        String ssl = "";
        if(filter != null)
            ssl = filter.getSsl();
        if(StringUtils.isBlank(ssl)) {
            ssl = properties.getMSGSsl();
            if(ssl == null)
                ssl = "false";
        }
        
        
        String port = "";
        if(filter != null) {
            MSGGatewayFilter msgGatewayFilter = (MSGGatewayFilter)filter;
            port = (String)msgGatewayFilter.getAttributes().get("port");
        }
        
        if(StringUtils.isBlank(port) || port.equals("0")) {
            port = properties.getMSGPort().toString();
        }
        
        String host = (String)properties.getHost();

        if(StringUtils.isBlank(host) || StringUtils.isBlank(port)) {
            Log.log.error("Host name or port is not available.");
            return null;
        }
        
        if(ssl.equalsIgnoreCase("true"))
            sb.append("ssl://");
        else
            sb.append("jms://");
        
        sb.append(host);
        
        if(StringUtils.isNotBlank(port))
            sb.append(":").append(port);
            
        url = sb.toString();

        if(filter != null)
            serverUrl.put(filter.getId(), url);
        
        return url;
    }
    
} // class ResilientGateway