/******************************************************************************
* (C) Copyright 2014
*
* Resolve Systems, LLC
*
* All rights reserved
* This software is distributed under license and may not be used, copied,
* modified, or distributed without the express written permission of
* Resolve Systems, LLC.
*
******************************************************************************/

package com.resolve.gateway.msg.resilient;

import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.Callable;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.FutureTask;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.TimeoutException;

import javax.jms.BytesMessage;
import javax.jms.Message;
import javax.jms.MessageListener;
import javax.jms.TextMessage;
import javax.jms.Topic;
import javax.jms.TopicConnection;
import javax.jms.TopicSession;
import javax.jms.TopicSubscriber;

import com.resolve.gateway.resolvegateway.msg.ConfigReceiveMSGGateway;
import com.resolve.gateway.resolvegateway.msg.MSGGateway;
import com.resolve.gateway.resolvegateway.msg.MSGGatewayFilter;
import com.resolve.util.Base64;
import com.resolve.util.Log;
import com.resolve.util.StringUtils;
import com.resolve.util.SystemUtil;

import net.sf.json.JSONObject;

public class MQSubscriber implements MessageListener {

    private TopicConnection connection;
    private TopicSession session;
    private String topicName;
    private String filterName;
    private boolean durable;
    
    private TopicSubscriber subscriber;
    
    private static long waitTime = 2000;

    protected ExecutorService executor = Executors.newFixedThreadPool(SystemUtil.getCPUCount());
    
    public MQSubscriber() {}
    
    public MQSubscriber(TopicConnection connection, TopicSession session, String topicName, String filterName, boolean durable) {
        
        this.connection = connection;
        this.session = session;
        this.topicName = topicName;
        this.filterName = filterName;
        this.durable = durable;
    }
    
    public TopicConnection getConnection()
    {
        return connection;
    }

    public void setConnection(TopicConnection connection)
    {
        this.connection = connection;
    }

    public TopicSession getSession()
    {
        return session;
    }

    public void setSession(TopicSession session)
    {
        this.session = session;
    }

    public String getTopicName()
    {
        return topicName;
    }

    public void setTopicName(String topicName)
    {
        this.topicName = topicName;
    }
    
    public String getFilterName()
    {
        return filterName;
    }

    public void setFilterName(String filterName)
    {
        this.filterName = filterName;
    }

    public TopicSubscriber getSubscriber()
    {
        return subscriber;
    }

    public void setSubscriber(TopicSubscriber subscriber)
    {
        this.subscriber = subscriber;
    }

    public void startListening(String subscriptionName) throws Exception {
        
        try {
            Topic topic = (Topic)session.createTopic(topicName);
            
            if(StringUtils.isBlank(subscriptionName))
                subscriber = (TopicSubscriber)session.createSubscriber(topic);
            else
                subscriber = (TopicSubscriber)session.createDurableSubscriber(topic, subscriptionName);
            
            subscriber.setMessageListener(this);
            
//            System.out.println("Subscriber " + subscriptionName + " is started.");
            Log.log.info("Subscriber " + subscriptionName + " is started.");
        } catch(Exception e) {
            throw e;
        } catch(Throwable t) {
            t.printStackTrace();
            Log.log.error(t.getMessage());
            throw new Exception("Runtime exception: " + t.getMessage());
        }
    }
    
    public void onMessage(Message message) {
        
        String msgId = null;
        String msgType = null;
        String msgBody = null;
        String msgReceived = null;
        
        try {
//          msgType = message.getJMSType();
            msgId = message.getJMSMessageID();
            
            if(message instanceof TextMessage) {
                msgBody = ((TextMessage)message).getText();
                msgReceived = message.toString();
// System.out.println("Received message <" + msgBody + "> with ID <" + msgId + ">");
                Log.log.debug("Received message <" + msgBody + "> with ID <" + msgId + ">");
//                System.out.println(new MQHeaderList(message, true));
            }
            
            else if(message instanceof BytesMessage) {
                BytesMessage bytesMessage = (BytesMessage)message;
                byte[] bytes = new byte[(int)bytesMessage.getBodyLength()];
                bytesMessage.readBytes(bytes);
                
                msgBody = Base64.encodeBytes(bytes);
/*                
                DataInputStream in = new DataInputStream(new ByteArrayInputStream(bytes));
                MQHeaderIterator it = new MQHeaderIterator(in, bytesMessage.getStringProperty("JMS_IBM_Format"), bytesMessage.getIntProperty("JMS_IBM_Encoding"), 819);
                while (it.hasNext()) {
                    MQHeader item = (MQHeader) it.next();
                }

//                MQGetMessageOptions gmo = new MQGetMessageOptions();
//                queue.get(message, gmo);
                
                MQHeaderIterator it = new MQHeaderIterator((DataInput)message);
                while (it.hasNext()) {
                    MQHeader header = it.nextHeader();
                    //header.getValue(arg0);
                    System.out.println("Header type " + header.type() + ": " + header);
                } */
            }
            
            if(!processMessage(msgId, msgType, msgBody, msgReceived)) {
                Log.log.debug(msgReceived);
                Log.log.debug("Message type is: " + msgType);
                Log.log.error("Failed to process message.");
            }
            
//            String response = processSyncMessage(msgReceived);
        } catch (Exception e) {
            e.printStackTrace();
            Log.log.debug(message);
            Log.log.error(e.getMessage(), e);
        }
    }
    
    public void stopListening(String subscriptionName, boolean unsubscriber) throws Exception {

        try {
            if(subscriber != null)
                subscriber.close();
            
            if(session != null) {
                if(durable && unsubscriber)
                    session.unsubscribe(subscriptionName);
                session.close();
            }
            
            Log.log.info("Stop listening: " + subscriptionName);
            
            // The connection may have been used by other filters that shares that same connection,
            // it cannot be closed until the RSREMOTE shuts down, especially when there is a client id associated with it.
/*            if(connection != null) {
               connection.stop();
               connection.close();
            } */
        } catch(Exception e) {
            Log.log.error(e.getMessage(), e);
            throw e;
        }
        
//        System.out.println("Subscriber " + topicName + " stopped.");
        Log.log.info("Subscriber " + subscriptionName + " stopped.");
    }
    
    private boolean processMessage(String id , String type, String body, String msg) {

        MSGGatewayFilter filter = MSGGateway.getMSGFilters().get(filterName);
        String gatewayName = filter.getGatewayName();
        MSGGateway gateway = ConfigReceiveMSGGateway.getGateways().get(gatewayName);
        if(gateway == null) {
            Log.log.error("Failed to process sync message because gateway " + gatewayName + "not found");
            return false;
        }
        
        Map<String, String> params = new HashMap<String, String>();
        params.put("MQ_ID", id);
        params.put("MQ_TYPE", type);
        params.put("MQ_BODY", body);
        params.put("MQ_DATA", msg);
        
        if(StringUtils.isBlank(filterName)) {
            Log.log.warn("Filter name is not available.");
            return false;
        }
        
        return gateway.processData(filter, gateway, params);
    }
    
    private String processSyncMessage(String msg) {

        MSGGatewayFilter filter = MSGGateway.getMSGFilters().get(filterName);
        String gatewayName = filter.getGatewayName();
        MSGGateway gateway = ConfigReceiveMSGGateway.getGateways().get(gatewayName);
        if(gateway == null) {
            Log.log.error("Failed to process sync message because gateway " + gatewayName + "not found");
            return null;
        }
        
        Map<String, String> params = new HashMap<String, String>();
        params.put("MQ_DATA", msg);
        
//        waitTime = gateway.getTimeout();
        String result = "Response is: ";
        
        FilterCallable callable = new FilterCallable(gateway, filterName, params);
        FutureTask<String> futureTask = new FutureTask<String>(callable);
        
        try {
            executor.execute(futureTask);
            
            while (!futureTask.isDone()) {
                try {
                    Log.log.debug("Waiting for FutureTask to complete");
                    result = futureTask.get(waitTime, TimeUnit.SECONDS);
                } catch (InterruptedException | ExecutionException e) {
                    Log.log.error(e.toString());
                    JSONObject message = new JSONObject();
                    message.accumulate("Message", "Failed to process.");
                    result = message.toString();
                    break;
                } catch(TimeoutException e) {
                    futureTask.cancel(true);
                    Log.log.error(e.toString());
                    JSONObject message = new JSONObject();
                    message.accumulate("Message", "Task is timed out.");
                    result = message.toString();
                    break;
                }
            }
        } catch(Exception e) {
            Log.log.error(e.toString());
            JSONObject message = new JSONObject();
            message.accumulate("Message", "Failed to execute task.");
            result = message.toString();
        }
        
        return result;
    }
    
    class FilterCallable implements Callable<String> {

        private MSGGateway instance;
        private String filterName;
        private Map<String, String> params;

        
        public FilterCallable(final MSGGateway instance, final String filterName, final Map<String, String> params) {
            this.instance = instance;
            this.filterName = filterName;
            this.params = params;
        }
        
        @Override
        public String call() throws Exception {
            
            return instance.processBlockingData(filterName, params);
        }

    } // end of class FilterCallable

} // MQSubscriber