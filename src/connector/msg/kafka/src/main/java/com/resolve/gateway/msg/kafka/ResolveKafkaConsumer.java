package com.resolve.gateway.msg.kafka;

import java.time.Duration;

import org.apache.kafka.clients.consumer.Consumer;
import org.apache.kafka.clients.consumer.ConsumerRecords;
import com.resolve.util.Log;
import com.resolve.gateway.Filter;

/**
 * Provides resolve consumer implementation.
 */
public class ResolveKafkaConsumer
{
    private boolean isShutdown = false;

    private final Filter filter;

    public ResolveKafkaConsumer(Filter filter)
    {
        super();
        this.filter = filter;
    }

    /**
     * Starts a consumer.
     */
    public void start()
    {
        Consumer<Long, String> consumer = KafkaConsumerFactory.createConsumer(filter);

        while (!isShutdown)
        {
            // consumer will wait if no record is found at broker for the
            // provided duration.
            ConsumerRecords<Long, String> consumerRecords = consumer.poll(1000L);

            System.out.println(consumerRecords.count());
            if (consumerRecords.count() == 0)
            {
//                Log.log.debug("Kafka Consumer report - No message found");
                continue;
            }

            processRecords(consumerRecords);

            // commits the offset of record to broker.
            consumer.commitAsync();
        }
    }

    /**
     * Shuts down a consumer.
     */
    public void shutdown()
    {
        Log.log.debug(String.format("shutting down consumer with filter id [%s]", filter.getId()));

        this.isShutdown = true;
    }

    private void processRecords(final ConsumerRecords<Long, String> records)
    {
        records.forEach(record -> {
            System.out.println("Consumed message: Record key "+record.key()+", Record value "+record.value()+", " + "Record partition "+record.partition()+", Record offset "+ record.offset());
        });
    }
}
