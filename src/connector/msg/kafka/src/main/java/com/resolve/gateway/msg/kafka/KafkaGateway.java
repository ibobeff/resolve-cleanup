package com.resolve.gateway.msg.kafka;

import java.util.Iterator;
import java.util.Map;
import java.util.Map.Entry;

import com.resolve.gateway.Filter;
import com.resolve.gateway.resolvegateway.ServerHeartbeat;
import com.resolve.gateway.resolvegateway.msg.ConfigReceiveMSGGateway;
import com.resolve.gateway.resolvegateway.msg.MSGGateway;
import com.resolve.gateway.resolvegateway.msg.MSGGatewayFilter;
import com.resolve.util.Log;

public class KafkaGateway extends MSGGateway implements Runnable {

    private static volatile KafkaGateway instance = null;

    /**
     * ## Do not modify
     * Singleton constructor to take the blueprint configuration and initialize the deployed 
     * filters for this specific gateway. For any Messaging gateway this method is responsible
     * 1. Getting the gateway name
     * 2. Get MSG filter 
     * @param config
     */
    private KafkaGateway(ConfigReceiveMSGGateway config) {

        super(config);
        //
        String gatewayName = this.getClass().getSimpleName();
        int index = gatewayName.indexOf("Gateway");
        if(index != -1)
            name = gatewayName.substring(0, index);

        Map<String, MSGGatewayFilter> msgFilters = getMSGFilters();

        for(Iterator<String> iterator=msgFilters.keySet().iterator(); iterator.hasNext();) {
            String filterName = iterator.next();
            MSGGatewayFilter filter = msgFilters.get(filterName);

            if(name.indexOf(((MSGGatewayFilter)filter).getGatewayName()) != -1) {
                if(filters.get(filterName) == null) {
                    filters.put(filterName, msgFilters.get(filterName));

                    if(filter.isDeployed()) {
                        orderedFilters.add(filter);
                        orderedFiltersMapById.put(filter.getId(), filter);
                    }
                }
            }
        }

        ConfigReceiveMSGGateway.getGateways().put(name, this);
    }
    
    @Override
    protected void initMSGListener(Filter deployedFilter)
    {
        // TODO: Start subscription based on the filter configuration
    }

    @Override
    protected void stopFilterListeners(Filter filter)
    {
        // TODO: Stop deployed consumer/producer based on the filter.
    }
    
    // Public method to get an instance from this Singleton class
    // Do not modify
    public static KafkaGateway getInstance(ConfigReceiveMSGGateway config) {

        if (instance == null) {
            instance = new KafkaGateway(config);
        }

        return instance;
    }

    // This method is called after the instance is instantiated.
    // Do not modify
    public static KafkaGateway getInstance() {
        if (instance == null) {
            throw new RuntimeException("Kafka MSG Gateway is not initialized correctly.");
        } else {
            return instance;
        }
    }

    // Initialize the gateway with the default HTTP server instance and start to listen on the default port
    // Do not remove the existing code, but you can add additional logic for initialization if necessary
    @Override
    protected void initialize() {

        queue = config.getQueue().toUpperCase();

        try {
            Log.log.info("Initializing Kafka MSG Listener ...");
            this.gatewayConfigDir = "/config/" + queue.toLowerCase() + "/";

            if(config.isActive() && config.isPrimary()) {

                // Monitor MQ server connection
                heartBeat = new ServerHeartbeat(this);
                heartBeat.start();
            }
        } catch (Exception e) {
            Log.log.error("Failed to config Kafka MSG Gateway: " + e.getMessage(), e);
        }
    }

    @Override
    public void reinitialize() {

        super.reinitialize();

        //Add customized code here to initilize your server based on configuration and deployed filter data;

        if(isActive() && isPrimary()) {
            // Monitor MQ server connection
            heartBeat = new ServerHeartbeat(this);
            heartBeat.start();
        }
    }

    public boolean sendHeartbeat() {

        //TODO: To be done in future sprint
        boolean isAlive = true;
        return isAlive;
    }

    // The gateway will start to run by calling the super class common logic
    // and start the queue listeners or topic subscribers based on the MSG type defined in each filter
    @Override
    public void start() {

        Log.log.info("Starting KafkaGateway ...");

        super.start();
    }

    // This method will be called when the gateway is gracefully shutdown
    // The queue listeners or topic subscribers will be stopped for all the filters by the super class
    @Override
    public void stop() {

        Log.log.warn("Stopping KafkaGateway ...");
        //TODO: To be done in future sprint
        
        super.stop();
    }

    // This method will be called when a filter is deployed to validate the semantics of the field only
    // The syntax of the filter fields have been validated when the filter is defined and saved from UI
    // including whether the filter field is required or not per gateway definition in blueprint.properties
    @Override
    public int validateFilterFields(Map<String, Object> params) {

        int code = 0;

        // Add custom logic here

        return code;
    }

    // The method returns the error string that is associated with the error code defined
    // under the custom logic being implemented
    @Override
    public String getValidationError(int errorCode) {

		String error = null;

		// Add custom logic here

        return error;
    }
    // This method is used by the system
    // Do not modify
    @Override
    protected String getMessageHandlerName() {

        return this.getClass().getSimpleName();
    }

    
	// This method must be implemented for each gateway specifically,
	// which returns a unique key for the MSG connection per gateway filter
    public String getConnectionKey(MSGGatewayFilter filter) {

        return filter.getId();
    }

    // If the common logic in the super class does not support the use case for this specific gateway,
    // you may create override methods to replace the logic in the super class

}