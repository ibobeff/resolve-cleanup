package com.resolve.gateway.msg.kafka;

import java.util.concurrent.ExecutionException;

import org.apache.kafka.clients.producer.Producer;
import org.apache.kafka.clients.producer.ProducerRecord;

import com.resolve.gateway.Filter;

public class ResolveKafkaProducer
{
    private static final String RESOLVE_TOPIC = "test-topic";

    private final Filter filter;
    
    public ResolveKafkaProducer(Filter filter)
    {
        super();
        this.filter = filter;
    }

    public void produce(final String jsonContent)
    {
        Producer<Long, String> producer = KafkaProducerFactory.createProducer(filter);
        ProducerRecord<Long, String> record = new ProducerRecord<Long, String>(RESOLVE_TOPIC, jsonContent);

        try
        {
            producer.send(record).get();
        }
        catch (ExecutionException | InterruptedException e)
        {
            e.printStackTrace();
        }

        producer.close();
    }
}
