package com.resolve.gateway.test;

import java.util.UUID;
import java.util.concurrent.Executors;
import java.util.stream.IntStream;

import org.junit.Test;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.node.ObjectNode;
import com.resolve.gateway.Filter;
import com.resolve.gateway.msg.kafka.ConsumerService;
import com.resolve.gateway.msg.kafka.ResolveKafkaProducer;
import com.resolve.gateway.resolvegateway.msg.MSGGatewayFilter;

public class ConsumerProducerTest {
    
   @Test
   public void testConsumerProducer() {
       ConsumerService service = ConsumerService.getInstance();
       Filter gatewayFilter = new MSGGatewayFilter("resolveGateway", "true", "1", "eventEventId", "", "");

       service.startConsumer(gatewayFilter);

       Runnable runnable = () -> {
           ResolveKafkaProducer producer = new ResolveKafkaProducer(gatewayFilter);

           IntStream.range(0, 20).forEach(n -> {
               ObjectMapper mapper = new ObjectMapper();
               ObjectNode jsonObject = mapper.createObjectNode();
               jsonObject.put("index", n + 1);
               jsonObject.put("value", UUID.randomUUID().toString());
               
               producer.produce(jsonObject.toString());
               
               try {
                   // simulate heavy work
                   Thread.sleep(1000);
               } catch (InterruptedException e) {
                   e.printStackTrace();
               }
           });
       };
       runnable.run();
       Executors.newSingleThreadExecutor().execute(runnable);
       
       try {
           // consuming
           Thread.sleep(1000 * 30);
       } catch (InterruptedException e) {
           e.printStackTrace();
       }

       service.stopConsumer(gatewayFilter);
   }
}