package com.resolve.gateway.test;

import org.junit.BeforeClass;
import org.junit.runner.RunWith;
import org.junit.runners.Suite;
import org.junit.runners.Suite.SuiteClasses;

import com.resolve.gateway.msg.kafka.KafkaGateway;
import com.resolve.gateway.msg.kafka.KafkaGatewayAPI;


@RunWith(Suite.class)
@SuiteClasses({KafkaGateway.class, KafkaGatewayAPI.class })
public class EmulatorUnitTests
{
    
    @BeforeClass
    public static void setUpClass() {      
        System.out.println("TestCase1 setup");
    }

}

