package com.resolve.gateway.msg.kafka;

import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.Executors;

import com.resolve.gateway.Filter;

/**
 * Provides consumer related functionalities.
 */
public class ConsumerService
{
    private final Map<String, ResolveKafkaConsumer> consumerRegistry = new HashMap<>();
    private static volatile ConsumerService instance = null;
    
    private ConsumerService() {}
    
    public static ConsumerService getInstance() {

        if (instance == null) {
            instance = new ConsumerService();
        }

        return instance;
    }
    
    /**
     * Starts a consumer.
     * 
     * @param filter
     *            {@link Filter}
     */
    public void startConsumer(Filter filter)
    {
        final ResolveKafkaConsumer consumer = new ResolveKafkaConsumer(filter);
        consumerRegistry.put(filter.getId(), consumer);

        Runnable runnable = () -> {
            consumer.start();
        };

        Executors.newSingleThreadExecutor().execute(runnable);
    }

    /**
     * Stops the execution of a consumer.
     * 
     * @param filter
     *            {@link Filter}
     */
    public void stopConsumer(Filter filter)
    {
        final ResolveKafkaConsumer consumer = consumerRegistry.remove(filter.getId());
        if (consumer == null)
        {
            throw new IllegalStateException("Consumer is not running.");
        }

        consumer.shutdown();
    }
}
