package com.resolve.persistence.model;

import java.util.List;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Lob;
import javax.persistence.Table;
import javax.persistence.UniqueConstraint;
import com.resolve.services.interfaces.VO;
import com.resolve.services.hibernate.vo.JIRASERVICEDESKFilterVO;

@Entity
@Table(name = "jiraservicedesk_filter", uniqueConstraints = { @UniqueConstraint(columnNames = { "u_name", "u_queue" }) })
public class JIRASERVICEDESKFilter extends GatewayFilter<JIRASERVICEDESKFilterVO> {

    private static final long serialVersionUID = 1L;

    public JIRASERVICEDESKFilter() {
    }

    public JIRASERVICEDESKFilter(JIRASERVICEDESKFilterVO vo) {
        applyVOToModel(vo);
    }

    public JIRASERVICEDESKFilterVO doGetVO() {
        JIRASERVICEDESKFilterVO vo = new JIRASERVICEDESKFilterVO();
        super.doGetBaseVO(vo);
        return vo;
    }

    @Override
    public void applyVOToModel(JIRASERVICEDESKFilterVO vo) {
        if (vo == null) return;
        super.applyVOToModel(vo);
    }
}

