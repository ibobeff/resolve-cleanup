package com.resolve.gateway.jiraservicedesk;
import com.resolve.gateway.AbstractGatewayAPI;
import net.sf.json.JSONObject;



public class JIRASERVICEDESKAPI extends AbstractGatewayAPI
{
    static
    {
        instance = JIRASERVICEDESKGateway.getInstance();
    }

    /**
     * Adds a new comment to an issue.
     * @param key - Required. JIRA Key
     * @param comment - Required. Comment to be added
     * @param username- A valid user in JIRA. If passed null then the username configured in blueprint properties file is used.
     * @param password -Password for the user. If passed null then the password configured in blueprint properties file is used.
     * @return status of addComment request
     * @throws Exception
     */
    public static boolean addComment(String key, String comment, String username, String password) throws Exception
    {
        return ((JIRASERVICEDESKGateway) instance).addComment(key, comment, username, password);
    }

    
    /**
     * This API call is to create issue in JIRA.
     * Creates an issue from a JSON representation.
     * 
     * @param username -   A valid user in JIRA. If passed null then the username configured in blueprint properties file is used.
     * @param password -   Password for the user. If passed null then the password configured in blueprint properties file is used.
     * @param jsonParams - Required.Valid Fields and values that are required to create an issue as a JSON String
     * 
     *  e.g. {
    "fields": {
       "project":
       { 
          "key": "SD"
       },
       "summary": "Created Test Issue Through REST Call",
       "description": "Creating of an issue using project SD and issue type names using the REST API",
       "issuetype": {
          "name": "Incident"
       },
       "assignee":{"name": "user.name"}
      ,
       "priority":{"name":"Highest"}
      
   }
     * @return Key of the created JIRA
     * @throws Exception
     */
    public static String createIssue(String username, String password, JSONObject jsonParams) throws Exception
    {
       return ((JIRASERVICEDESKGateway) instance).createIssue(username, password, jsonParams);
    }
    
    /**
     * This API call is to create JIRA issue using fields projectKey,issueType,summary,description,assignee,priority
     * @param projectKey - Required. key of the project(Note: It's project key and key is different than id.)
     * @param issueType -Required. name of the issuetype
     * @param summary -Required. summary of the JIRA issue to be created.
     * @param description - Optional.description of the JIRA issue to be created.
     * @param assignee - Optional. name of assignee
     * @param priority - Optional. priority of JIRA issue.
    * @param username -  A valid user in JIRA. If passed null then the username configured in blueprint properties file is used.
     * @param password - Password for the user. If passed null then the password configured in blueprint properties file is used.
     * @return key of the created JIRA issue.
     * @throws Exception
     * 
    
}
     */
    public static String createIssue(String projectKey, String issueType, String summary, String description, String assignee, String priority, String username, String password) throws Exception
    {
        return ((JIRASERVICEDESKGateway) instance).createIssue(projectKey, issueType, summary, description, assignee, priority, username, password);
    }
    
/**
 * Edits the issue in JIRA of the specified jiraIdorKey.
 * If the issue cannot be edited in JIRA due to its workflow status (i.e. it is closed), then you will not be able to edit it with this method.
 * 
 * @param issueIdOrKey- Required. Id or Key of the issue. This issue will be updated in the API call.
 * @param projectKey- Key of the JIRA project.
 * @param issueType - Name of issuetype. e.g. If you are updating incident then value should be 'Incident'
 * @param summary - summary of the JIRA 
 * @param description - description of the JIRA
 * @param assignee - assignee of the JIRA
 * @param priority - priority of the JIRA
 * @param notifyUsers - If null then default value is true. If true then sends the email with notification that the issue was updated to users that watch it. Admin or project admin permissions are required to disable the notification.
 * @param overrideScreenSecurity - If null then default value is false. allows to update fields that are not on the screen. Only connect add-on users with admin scope permission are allowed to use this flag.
 * @param overrideEditableFlag - If null then default value is false.Updates the issue even if the issue is not editable due to being in a status with jira.issue.editable set to false or missing. Only connect add-on users with admin scope permission are allowed to use this flag.
 * @param username -  A valid user in JIRA. If passed null then the username configured in blueprint properties file is used.
 * @param password - Password for the user. If passed null then the password configured in blueprint properties file is used.
 * @return Status of edit issue.
 * @throws Exception
 */
    
    public static boolean editIssue(String issueIdOrKey, String projectKey, String issueType, String summary, String description, String assignee, String priority, Boolean notifyUsers,Boolean overrideScreenSecurity,Boolean overrideEditableFlag,String username, String password) throws Exception
    {
        return ((JIRASERVICEDESKGateway) instance).editIssue(issueIdOrKey, projectKey, issueType, summary, description, assignee, priority, notifyUsers, overrideScreenSecurity, overrideEditableFlag, username, password);
        
    }
    
    /**
     * Edits an issue from a JSON representation.
     * If the issue cannot be edited in JIRA due to its workflow status (i.e. it is closed), then you will not be able to edit it with this method.
     * 
     * @param issueIdOrKey- Required. Id or Key of the issue. This issue will be updated in the API call.
     * @param jsonParams - Required.JSON Representation of the Issue. Example is given below.
     * @param notifyUsers-If null then default value is true. If true then sends the email with notification that the issue was updated to users that watch it. Admin or project admin permissions are required to disable the notification.
     * @param overrideScreenSecurity- If null then default value is false. allows to update fields that are not on the screen. Only connect add-on users with admin scope permission are allowed to use this flag.
     * @param overrideEditableFlag -If null then default value is false.Updates the issue even if the issue is not editable due to being in a status with jira.issue.editable set to false or missing. Only connect add-on users with admin scope permission are allowed to use this flag.
     * @param username- A valid user in JIRA. If passed null then the username configured in blueprint properties file is used.
     * @param password- Password for the user. If passed null then the password configured in blueprint properties file is used.
     * @return - status of edit issue
     * @throws Exception
     *jsonParams:- e.g.{
 
         "fields":
     {    
      "summary":"Update the summary of JIRA",
       "assignee":{"name": "jeffrey.chen"},
       "priority":{"name":"High"},
        "project":
       { 
          "key": "SD"
       }, "issuetype": {
          "name": "Incident"
       },
        "description": "Creating of an issue using project SD and issue type names using the REST API"
 }
}
*/

    
    public static boolean editIssue( String issueIdOrKey, JSONObject jsonParams,Boolean notifyUsers,Boolean overrideScreenSecurity,Boolean overrideEditableFlag,String username, String password) throws Exception
    {
        return ((JIRASERVICEDESKGateway) instance).editIssue(issueIdOrKey, jsonParams,  notifyUsers, overrideScreenSecurity, overrideEditableFlag,username, password);
    }
    
    }
    



