package com.resolve.gateway.jiraservicedesk;

import com.resolve.gateway.BaseFilter;
import com.resolve.util.Log;

public class JIRASERVICEDESKFilter extends BaseFilter {

    public JIRASERVICEDESKFilter(String id, String active, String order, String interval, String eventEventId, String runbook, String script) {
        super(id, active, order, interval, eventEventId, runbook, script);
    }
}

