package com.resolve.gateway;

import java.util.Map;
import com.resolve.gateway.jiraservicedesk.JIRASERVICEDESKFilter;
import com.resolve.gateway.jiraservicedesk.JIRASERVICEDESKGateway;

public class MJIRASERVICEDESK extends MGateway {

    private static final String RSCONTROL_SET_FILTERS = "MJIRASERVICEDESK.setFilters";

    private static final JIRASERVICEDESKGateway instance = JIRASERVICEDESKGateway.getInstance();

    public MJIRASERVICEDESK() {
        super.instance = instance;
    }

    protected String getSetFilterMethodName() {
        return RSCONTROL_SET_FILTERS;
    }

    /**
     * Populates the {@link Filter} data into the given filterMap {@link Map} instance
     * 
     * @param filterMap
     *      the {@link Map} instance to hold the {@link Filter} data
     * @param filter
     *      a {@link JIRASERVICEDESKFilter} instance
     *            
     */
    public void populateGatewaySpecificFilterProperties(Map<String, String> filterMap, Filter filter) {
        if (instance.isActive()) {
            JIRASERVICEDESKFilter jiraservicedeskFilter = (JIRASERVICEDESKFilter) filter;
        }
    }
}

