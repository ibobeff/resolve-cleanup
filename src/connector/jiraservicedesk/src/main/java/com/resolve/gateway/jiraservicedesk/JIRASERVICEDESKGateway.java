package com.resolve.gateway.jiraservicedesk;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Map;

import com.resolve.gateway.BaseClusteredGateway;
import com.resolve.gateway.BaseFilter;
import com.resolve.gateway.Filter;
import com.resolve.gateway.MJIRASERVICEDESK;
import com.resolve.rsbase.MainBase;
import com.resolve.rsremote.ConfigReceiveGateway;
import com.resolve.util.Log;

import net.sf.json.JSONObject;

public class JIRASERVICEDESKGateway extends BaseClusteredGateway
{

    private static volatile JIRASERVICEDESKGateway instance = null;
    public static volatile ObjectService genericObjectService;
    private String queue = null;

    public static JIRASERVICEDESKGateway getInstance(ConfigReceiveJIRASERVICEDESK config)
    {
        if (instance == null)
        {
            instance = new JIRASERVICEDESKGateway(config);
        }
        return instance;
    }

    /**
     * This method is called once the initialization is done. If it gets called
     * before the initialization it will throw exception.
     *
     * @return
     */
    public static JIRASERVICEDESKGateway getInstance()
    {
        if (instance == null)
        {
            throw new RuntimeException("JIRASERVICEDESK Gateway is not initialized correctly..");
        }
        else
        {
            return instance;
        }
    }



    private JIRASERVICEDESKGateway(ConfigReceiveJIRASERVICEDESK config)
    {
        super(config);
    }

    @Override
    public String getLicenseCode()
    {
        return "JIRASERVICEDESK";
    }

    @Override
    protected void addAdditionalValues(Map<String, Object> arg0, Map<String, String> arg1)
    {
    }

    @Override
    protected String getGatewayEventType()
    {
        return "JIRASERVICEDESK";
    }

    @Override
    protected String getMessageHandlerName()
    {
        return MJIRASERVICEDESK.class.getSimpleName();
    }

    @Override
    protected Class<MJIRASERVICEDESK> getMessageHandlerClass()
    {
        return MJIRASERVICEDESK.class;
    }

    public String getQueueName()
    {
        return queue + getOrgSuffix();
    }

    @Override
    public void start()
    {
        Log.log.debug("Starting JIRASERVICEDESK Gateway");
        super.start();
    }

    @Override
    public void run()
    {

        if (!((ConfigReceiveGateway) configurations).isMock()) super.sendSyncRequest();
        while (running)
        {
            try
            {
              /* String jiraKey=createIssue("SD", "Incident", "Created Test Issue Through REST Call", "New JIRA to test API",
                                "jeffrey.chen", "Highest", "jeffrey.chen", "Resolve1234");

               System.out.println("JIRA Key created is: "+jiraKey);*/

               /*  boolean status = addComment("10103", "This is a comment Gateway API addded", null, null);*/
             /*   JSONArray a = new JSONArray();
                a.add("a");
                a.add("b");


                if(editIssue("10008", "SD", "Incident", "Edit summary", "Edit description", "jeffrey.chen", "Low", true,true,true,null, null))
                {
                    System.out.println("Edited issue");
                }*/

            }
            catch (Exception e1)
            {
                Log.log.error(e1.getMessage(), e1);
            }
            try
            {
                long startTime = System.currentTimeMillis();
                if (primaryDataQueue.isEmpty())
                {
                    Log.log.trace("Local Primary Event Queue for " + getGatewayEventType() + " is empty.....");
                    for (Filter filter : orderedFilters)
                    {
                        if (shouldFilterExecute(filter, startTime))
                        {
                            Log.log.trace("Filter " + filter.getId() + " executing");
                            List<Map<String, String>> results = invokeService(filter);
                            for (Map<String, String> result : results)
                            {
                                addToPrimaryDataQueue(filter, result);
                            }
                        }
                        else
                        {
                            Log.log.trace("Filter:" + filter.getId() + " is inactive or not ready to execute yet.....");
                        }
                    }
                    if (orderedFilters == null || orderedFilters.size() == 0)
                    {
                        Log.log.trace("There is not deployed filter for JIRASERVICEDESKGateway");
                    }
                }
                if (orderedFilters.size() == 0)
                {
                    Thread.sleep(interval);
                }
            }
            catch (Exception e)
            {
                Log.log.warn("Warning: " + e.getMessage());
                Log.alert("Instance " + MainBase.main.configId.getGuid() + " " + getLicenseCode() + "-" + getQueueName() + "-" + getInstanceType() + " Polling Failed", "Instance " + MainBase.main.configId.getGuid() + " " + getLicenseCode() + "-" + getQueueName() + "-" + getInstanceType() + " Polling Failed " + "due to " + e.getMessage());
                try
                {
                    Thread.sleep(interval);
                }
                catch (InterruptedException ie)
                {
                    Log.log.warn("sleep interrupted: " + ie.getMessage(), ie);
                }
            }
        }
    }

    /**
     * Add customized code here to get the data as List<Map<String, String>>
     * format for each filter
     *
     * @return List<Map<String, String>>
     */
    public List<Map<String, String>> invokeService(Filter filter)
    {
        List<Map<String, String>> result = new ArrayList<Map<String, String>>();
        // Add customized code here to get data from 3rd party system based the
        // information in the filter;
        return result;
    }

    /**
     * Add customized code here to initilize the connection with 3rd party
     * system
     *
     * @return List<Map<String, String>>
     */
    @Override
    protected void initialize()
    {
        ConfigReceiveJIRASERVICEDESK config = (ConfigReceiveJIRASERVICEDESK) configurations;
        queue = config.getQueue().toUpperCase();
        this.gatewayConfigDir = "/config/jiraservicedesk/";
        genericObjectService = new GenericObjectService(config);
        // Add customized code here to initilize the connection with 3rd party
        // system;
    }

    /**
     * Add customized code here to do when stop the gateway
     */
    @Override
    public void stop()
    {
        Log.log.warn("Stopping JIRASERVICEDESK gateway");
        // Add customized code here to stop the connection with 3rd party
        // system;
        super.stop();
    }

    @Override
    public Filter getFilter(Map<String, Object> params)
    {
        return new JIRASERVICEDESKFilter((String) params.get(BaseFilter.ID), (String) params.get(BaseFilter.ACTIVE), (String) params.get(BaseFilter.ORDER), (String) params.get(BaseFilter.INTERVAL), (String) params.get(BaseFilter.EVENT_EVENTID), (String) params.get(BaseFilter.RUNBOOK), (String) params.get(BaseFilter.SCRIPT));
    }

    public boolean checkGatewayConnection() throws Exception
    {
        Map<String, String> serverInfo = Collections.emptyMap();

        try
        {
            serverInfo = genericObjectService.getServerInfo(null, null);

            if (serverInfo == null || serverInfo.isEmpty())
            {
                throw new Exception("Failed to get server info from JIRA server. Connection to JIRA server possibly broken.");
            }
            else
            {
                Log.log.debug("Instance " + MainBase.main.configId.getGuid() + " : " + getLicenseCode() + "-" + getQueueName() + "-" + getInstanceType() + " connected to configured JIRA server");
            }
        }
        catch (Exception e)
        {
            Log.alert("Instance " + MainBase.main.configId.getGuid() + " breached " + getLicenseCode() + "-" + getQueueName() + "-" + getInstanceType() + "-ConnectionSelfCheck", "Instance " + MainBase.main.configId.getGuid() + " Breached " + getLicenseCode() + "-" + getQueueName() + "-" + getInstanceType() + "-ConnectionSelfCheck" + " due to " + e.getMessage());
        }

        return true;
    }

    public boolean addComment(String key, String comment, String username, String password) throws Exception
    {
        return genericObjectService.addComment(key, comment, username, password);
    }

    String createIssue(String username, String password, JSONObject jsonParams) throws Exception
    {
        return genericObjectService.createIssue(username, password, jsonParams);

    }

    public String createIssue(String projectKey, String issueType, String summary, String description, String assignee, String priority, String username, String password) throws Exception
    {
        return genericObjectService.createIssue(projectKey, issueType, summary, description, assignee, priority, username, password);
    }

    public boolean editIssue(String issueIdOrKey,String projectKey, String issueType, String summary, String description, String assignee, String priority,Boolean notifyUsers,Boolean overrideScreenSecurity,Boolean overrideEditableFlag, String username, String password) throws Exception
    {
        return genericObjectService.editIssue(issueIdOrKey,projectKey, issueType, summary, description, assignee, priority,notifyUsers, overrideScreenSecurity, overrideEditableFlag, username, password);
    }
    public boolean editIssue( String issueIdOrKey, JSONObject jsonParams,Boolean notifyUsers,Boolean overrideScreenSecurity,Boolean overrideEditableFlag,String username, String password) throws Exception
    {
        return genericObjectService.editIssue(issueIdOrKey, jsonParams, notifyUsers, overrideScreenSecurity, overrideEditableFlag, username, password);
    }

}
