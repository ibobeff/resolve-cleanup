/******************************************************************************
* (C) Copyright 2015
*
* Resolve Systems, LLC
*
* All rights reserved
* This software is distributed under license and may not be used, copied,
* modified, or distributed without the express written permission of
* Resolve Systems, LLC.
*
******************************************************************************/

package com.resolve.gateway.jiraservicedesk;


import java.util.Map;

import net.sf.json.JSON;
import net.sf.json.JSONObject;

public interface ObjectService
{
    Map<String, String> getServerInfo(String username, 
                                      String password) throws Exception;
    
    boolean addComment(String key,
                    String comment,
                    String username, 
                    String password) throws Exception;
 


    String createIssue(String username, String password, JSONObject jsonParams) throws Exception;
    
    String createIssue(String projectKey, String issueType,String summary, String description,String assignee,String priority,String username,String password) throws Exception;
    
    boolean editIssue(String issueIdOrKey, JSONObject jsonParams,Boolean notifyUsers,Boolean overrideScreenSecurity,Boolean overrideEditableFlag,String username, String password) throws Exception;
    boolean editIssue(String issueIdOrKey,String projectKey, String issueType,String summary, String description,String assignee,String priority,Boolean notifyUsers,Boolean overrideScreenSecurity,Boolean overrideEditableFlag,String username,String password) throws Exception;
    
 
    
   /* List<Map<String, String>> search(String urlQuery, 
                                     String username, 
                                     String password) throws Exception;
    
    List<Map<String, String>> search(String jql, Integer startAt,Integer maxResults,String fields,
                    String username, 
                    String password) throws Exception;*/
    
   
    
   /* List<Map<String, String>> getAllProjects()throws Exception;
    
    List<Map<String, String>> getAllIssueTypes()throws Exception;
    
    String getIssueFields(String projectKey,String issueType)throws Exception;
    
    String validateJqlQuery(String urlQuery,String createTimeStamp,String lastReadJiraID,boolean hasVariable) throws Exception;
    
    List<Map<String, String>> parseQueryResponse(String response) throws Exception;
    Map<String,String> getIssueFieldsIdNameMap(String username, String password) throws Exception;*/
    
}