package com.resolve.rscontrol;

import com.resolve.persistence.model.JIRASERVICEDESKFilter;

public class MJIRASERVICEDESK extends MGateway {

    private static final String MODEL_NAME = JIRASERVICEDESKFilter.class.getSimpleName();

    protected String getModelName() {
        return MODEL_NAME;
    }
}

