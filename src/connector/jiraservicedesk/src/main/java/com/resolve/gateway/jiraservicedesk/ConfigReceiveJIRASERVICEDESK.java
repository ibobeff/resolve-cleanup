package com.resolve.gateway.jiraservicedesk;

import java.io.File;
import java.util.HashMap;
import java.util.Map;
import org.apache.commons.io.FileUtils;

import com.resolve.rsbase.MainBase;
import com.resolve.gateway.Filter;
import com.resolve.rsremote.ConfigReceiveGateway;
import com.resolve.util.Log;
import com.resolve.util.ObjectProperties;
import com.resolve.util.StringUtils;
import com.resolve.util.XDoc;

public class ConfigReceiveJIRASERVICEDESK extends ConfigReceiveGateway {

    private static final long serialVersionUID = 1L;

    private static final String RECEIVE_JIRASERVICEDESK_NODE = "./RECEIVE/JIRASERVICEDESK/";

    private static final String RECEIVE_JIRASERVICEDESK_FILTER = RECEIVE_JIRASERVICEDESK_NODE + "FILTER";

    private String queue = "JIRASERVICEDESK";

    private static final String RECEIVE_JIRASERVICEDESK_ATTR_HTTPBASICAUTHUSERNAME = RECEIVE_JIRASERVICEDESK_NODE + "@HTTPBASICAUTHUSERNAME";

    private static final String RECEIVE_JIRASERVICEDESK_ATTR_HTTPBASICAUTHPASSWORD = RECEIVE_JIRASERVICEDESK_NODE + "@HTTPBASICAUTHPASSWORD";

    private static final String RECEIVE_JIRASERVICEDESK_ATTR_URL = RECEIVE_JIRASERVICEDESK_NODE + "@URL";
    
    private static final String RECEIVE_JIRASERVICEDESK_ATTR_SSL = RECEIVE_JIRASERVICEDESK_NODE + "@SSL";

    private String httpbasicauthusername = "";

    private String httpbasicauthpassword = "";

    private String url = "";
    
    private String ssl = "";
    
    public String getSsl() {
        return this.ssl;
    }

    public void setSsl(String ssl) {
        this.ssl = ssl;
    }

    public String getQueue() {
        return queue;
    }

    public void setQueue(String queue) {
        this.queue = queue;
    }

    public String getHttpbasicauthusername() {
        return this.httpbasicauthusername;
    }

    public void setHttpbasicauthusername(String httpbasicauthusername) {
        this.httpbasicauthusername = httpbasicauthusername;
    }

    public String getHttpbasicauthp_assword() {
        return this.httpbasicauthpassword;
    }

    public void setHttpbasicauthp_assword(String httpbasicauthpassword) {
        this.httpbasicauthpassword = httpbasicauthpassword;
    }

    public String getUrl() {
        return this.url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public ConfigReceiveJIRASERVICEDESK(XDoc config) throws Exception {
        super(config);
        define("httpbasicauthusername", STRING, RECEIVE_JIRASERVICEDESK_ATTR_HTTPBASICAUTHUSERNAME);
        define("httpbasicauthp_assword", SECURE, RECEIVE_JIRASERVICEDESK_ATTR_HTTPBASICAUTHPASSWORD);
        define("url", STRING, RECEIVE_JIRASERVICEDESK_ATTR_URL);
        define("ssl", STRING, RECEIVE_JIRASERVICEDESK_ATTR_SSL);
    }

    @Override
    public String getRootNode() {
        return RECEIVE_JIRASERVICEDESK_NODE;
    }

    @Override
    public void load() throws Exception {
        try {
            loadAttributes();
            if (isActive()) {
                JIRASERVICEDESKGateway jiraservicedeskGateway = JIRASERVICEDESKGateway.getInstance(this);
                filters = xdoc.getListMapValue(RECEIVE_JIRASERVICEDESK_FILTER);
                if (filters.size() > 0) {
                    for (Map<String, Object> values : filters) {
                        String filterName = ((String) values.get(JIRASERVICEDESKFilter.ID)).toLowerCase();
                        File scriptFile = getFile(MainBase.main.release.serviceName + "/config/jiraservicedesk/" + filterName + ".groovy");
                        try {
                            if (scriptFile.exists()) {
                                String groovy = FileUtils.readFileToString(scriptFile, "UTF-8");
                                if (StringUtils.isNotBlank(groovy)) {
                                    values.put(JIRASERVICEDESKFilter.SCRIPT, groovy);
                                    Log.log.debug("Loaded groovy script: " + scriptFile);
                                }
                            }
                            jiraservicedeskGateway.setFilter(values);
                        } catch (Exception e) {
                            Log.log.warn(e.getMessage() + ". Skipping file: " + scriptFile.getAbsolutePath());
                        }
                    }
                }
            }
        } catch (Throwable e) {
            setActive(false);
            Log.log.warn("Couldn not load configuration for JIRASERVICEDESK gateway, check blueprint. " + e.getMessage(), e);
        }
    }

    @Override
    public void save() throws Exception {
        File dir = getFile(MainBase.main.release.serviceName + "/config/jiraservicedesk");
        if (!dir.exists()) {
            FileUtils.forceMkdir(dir);
        }
        if (filters != null && isActive()) {
            filters.clear();
            for (Filter filter : JIRASERVICEDESKGateway.getInstance().getFilters().values()) {
                JIRASERVICEDESKFilter jiraservicedeskFilter = (JIRASERVICEDESKFilter) filter;
                String groovy = jiraservicedeskFilter.getScript();
                String scriptFileName;
                File scriptFile = null;
                if (StringUtils.isNotBlank(groovy)) {
                    scriptFileName = jiraservicedeskFilter.getId().toLowerCase() + ".groovy";
                    scriptFile = getFile(MainBase.main.release.serviceName + "/config/jiraservicedesk/" + scriptFileName);
                }
                Map<String, Object> entry = new HashMap<String, Object>();
                putFilterDataIntoEntry(entry, jiraservicedeskFilter);
                filters.add(entry);
                if (scriptFile != null) {
                    try {
                        FileUtils.writeStringToFile(scriptFile, groovy, "UTF-8");
                    } catch (Exception e) {
                        Log.log.warn(e.getMessage() + ". Skipping file: " + scriptFile.getAbsolutePath());
                    }
                }
            }
            xdoc.setListMapValue(RECEIVE_JIRASERVICEDESK_FILTER, filters);
        }
        if (nameProperties != null && isActive()) {
            nameProperties.clear();
            for (String name : JIRASERVICEDESKGateway.getInstance().getNameProperties().keySet()) {
                Map<String, Object> entry = new HashMap<String, Object>();
                entry.put(NAME_KEY, name);
                nameProperties.add(entry);
                Map props = JIRASERVICEDESKGateway.getInstance().getNameProperties().getNameProperties(name);
                ObjectProperties prop = new ObjectProperties(props);
                prop.save(props);
            }
        }
        saveAttributes();
    }

    private void putFilterDataIntoEntry(Map<String, Object> entry, JIRASERVICEDESKFilter jiraservicedeskFilter) {
        entry.put(JIRASERVICEDESKFilter.ID, jiraservicedeskFilter.getId());
        entry.put(JIRASERVICEDESKFilter.ACTIVE, String.valueOf(jiraservicedeskFilter.isActive()));
        entry.put(JIRASERVICEDESKFilter.ORDER, String.valueOf(jiraservicedeskFilter.getOrder()));
        entry.put(JIRASERVICEDESKFilter.INTERVAL, String.valueOf(jiraservicedeskFilter.getInterval()));
        entry.put(JIRASERVICEDESKFilter.EVENT_EVENTID, jiraservicedeskFilter.getEventEventId());
        entry.put(JIRASERVICEDESKFilter.RUNBOOK, jiraservicedeskFilter.getRunbook());
        entry.put(JIRASERVICEDESKFilter.SCRIPT, jiraservicedeskFilter.getScript());
    }
}

