package com.resolve.gateway.mcafee;

import java.io.File;
import java.util.HashMap;
import java.util.Map;

import org.apache.commons.io.FileUtils;

import com.resolve.gateway.Filter;
import com.resolve.rsbase.MainBase;
import com.resolve.rsremote.ConfigReceiveGateway;
import com.resolve.util.Log;
import com.resolve.util.ObjectProperties;
import com.resolve.util.StringUtils;
import com.resolve.util.XDoc;

public class ConfigReceiveMcAfee extends ConfigReceiveGateway {

    private static final long serialVersionUID = 1L;

    private static final String RECEIVE_MCAFEE_NODE = "./RECEIVE/MCAFEE/";

    private static final String RECEIVE_MCAFEE_FILTER = RECEIVE_MCAFEE_NODE + "FILTER";

    private String queue = "MCAFEE";

    private static final String RECEIVE_MCAFEE_ATTR_PASSWORD = RECEIVE_MCAFEE_NODE + "@PASSWORD";

    private static final String RECEIVE_MCAFEE_ATTR_PORT = RECEIVE_MCAFEE_NODE + "@PORT";

    private static final String RECEIVE_MCAFEE_ATTR_SSLTYPE = RECEIVE_MCAFEE_NODE + "@SSLTYPE";

    private static final String RECEIVE_MCAFEE_ATTR_HOST = RECEIVE_MCAFEE_NODE + "@HOST";

    private static final String RECEIVE_MCAFEE_ATTR_SSL = RECEIVE_MCAFEE_NODE + "@SSL";

    private static final String RECEIVE_MCAFEE_ATTR_USERNAME = RECEIVE_MCAFEE_NODE + "@USERNAME";

    private String password = "";

    private String port = "";

    private String ssltype = "";

    private String host = "";

    private String ssl = "";

    private String username = "";

    public String getQueue() {
        return queue;
    }

    public void setQueue(String queue) {
        this.queue = queue;
    }

    public String getPassword() {
        return this.password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getPort() {
        return this.port;
    }

    public void setPort(String port) {
        this.port = port;
    }

    public String getSsltype() {
        return this.ssltype;
    }

    public void setSsltype(String ssltype) {
        this.ssltype = ssltype;
    }

    public String getHost() {
        return this.host;
    }

    public void setHost(String host) {
        this.host = host;
    }

    public String getSsl() {
        return this.ssl;
    }

    public void setSsl(String ssl) {
        this.ssl = ssl;
    }

    public String getUsername() {
        return this.username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public ConfigReceiveMcAfee(XDoc config) throws Exception {
        super(config);
        define("password", SECURE, RECEIVE_MCAFEE_ATTR_PASSWORD);
        define("port", STRING, RECEIVE_MCAFEE_ATTR_PORT);
        define("ssltype", STRING, RECEIVE_MCAFEE_ATTR_SSLTYPE);
        define("host", STRING, RECEIVE_MCAFEE_ATTR_HOST);
        define("ssl", STRING, RECEIVE_MCAFEE_ATTR_SSL);
        define("username", STRING, RECEIVE_MCAFEE_ATTR_USERNAME);
    }

    @Override
    public String getRootNode() {
        return RECEIVE_MCAFEE_NODE;
    }

    @Override
    public void load() throws Exception {
        try {
            loadAttributes();
            
            if(StringUtils.isBlank(host))
                throw new Exception("rsremote.receive.mcafee.host is blank.");
            
            if(StringUtils.isBlank(ssl) || (!ssl.equalsIgnoreCase("true") && !!ssl.equalsIgnoreCase("false")))
                throw new Exception("rsremote.receive.mcafee.ssl is not valid.");
            
            if(StringUtils.isBlank(ssl) || (!ssl.equalsIgnoreCase("self-signed") && !!ssl.equalsIgnoreCase("ca-signed")))
                throw new Exception("rsremote.receive.mcafee.ssltype is not valid.");
            
            if (isActive()) {
                McAfeeGateway mcafeeGateway = McAfeeGateway.getInstance(this);
                filters = xdoc.getListMapValue(RECEIVE_MCAFEE_FILTER);
                if (filters.size() > 0) {
                    for (Map<String, Object> values : filters) {
                        String filterName = ((String) values.get(McAfeeFilter.ID)).toLowerCase();
                        File scriptFile = getFile(MainBase.main.release.serviceName + "/config/mcafee/" + filterName + ".groovy");
                        try {
                            if (scriptFile.exists()) {
                                String groovy = FileUtils.readFileToString(scriptFile, "UTF-8");
                                if (StringUtils.isNotBlank(groovy)) {
                                    values.put(McAfeeFilter.SCRIPT, groovy);
                                    Log.log.debug("Loaded groovy script: " + scriptFile);
                                }
                            }
                            mcafeeGateway.setFilter(values);
                        } catch (Exception e) {
                            Log.log.warn(e.getMessage() + ". Skipping file: " + scriptFile.getAbsolutePath());
                        }
                    }
                }
            }
        } catch (Throwable e) {
            setActive(false);
            Log.log.warn("Couldn not load configuration for McAfee gateway, check blueprint. " + e.getMessage(), e);
        }
    }

    @Override
    public void save() throws Exception {
        File dir = getFile(MainBase.main.release.serviceName + "/config/mcafee");
        if (!dir.exists()) {
            FileUtils.forceMkdir(dir);
        }
        if (filters != null && isActive()) {
            filters.clear();
            for (Filter filter : McAfeeGateway.getInstance().getFilters().values()) {
                McAfeeFilter mcafeeFilter = (McAfeeFilter) filter;
                String groovy = mcafeeFilter.getScript();
                String scriptFileName;
                File scriptFile = null;
                if (StringUtils.isNotBlank(groovy)) {
                    scriptFileName = mcafeeFilter.getId().toLowerCase() + ".groovy";
                    scriptFile = getFile(MainBase.main.release.serviceName + "/config/mcafee/" + scriptFileName);
                }
                Map<String, Object> entry = new HashMap<String, Object>();
                putFilterDataIntoEntry(entry, mcafeeFilter);
                filters.add(entry);
                if (scriptFile != null) {
                    try {
                        FileUtils.writeStringToFile(scriptFile, groovy, "UTF-8");
                    } catch (Exception e) {
                        Log.log.warn(e.getMessage() + ". Skipping file: " + scriptFile.getAbsolutePath());
                    }
                }
            }
            xdoc.setListMapValue(RECEIVE_MCAFEE_FILTER, filters);
        }
        if (nameProperties != null && isActive()) {
            nameProperties.clear();
            for (String name : McAfeeGateway.getInstance().getNameProperties().keySet()) {
                Map<String, Object> entry = new HashMap<String, Object>();
                entry.put(NAME_KEY, name);
                nameProperties.add(entry);
                Map props = McAfeeGateway.getInstance().getNameProperties().getNameProperties(name);
                ObjectProperties prop = new ObjectProperties(props);
                prop.save(props);
            }
        }
        saveAttributes();
    }

    private void putFilterDataIntoEntry(Map<String, Object> entry, McAfeeFilter mcafeeFilter) {
        entry.put(McAfeeFilter.ID, mcafeeFilter.getId());
        entry.put(McAfeeFilter.ACTIVE, String.valueOf(mcafeeFilter.isActive()));
        entry.put(McAfeeFilter.ORDER, String.valueOf(mcafeeFilter.getOrder()));
        entry.put(McAfeeFilter.INTERVAL, String.valueOf(mcafeeFilter.getInterval()));
        entry.put(McAfeeFilter.EVENT_EVENTID, mcafeeFilter.getEventEventId());
        entry.put(McAfeeFilter.RUNBOOK, mcafeeFilter.getRunbook());
        entry.put(McAfeeFilter.SCRIPT, mcafeeFilter.getScript());
        entry.put(McAfeeFilter.ATTR1, mcafeeFilter.getAttr1());
		entry.put(McAfeeFilter.ATTR2, mcafeeFilter.getAttr2());
        entry.put(McAfeeFilter.PAGESIZE, mcafeeFilter.getPagesize());
        entry.put(McAfeeFilter.TIMERANGE, mcafeeFilter.getTimerange());
		entry.put(McAfeeFilter.LAST_ALARM_ID, mcafeeFilter.getLastAlarmId());
    }

}

