package com.resolve.gateway.mcafee;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.resolve.gateway.AbstractGatewayAPI;
import com.resolve.util.Log;
import com.resolve.util.StringUtils;
import com.resolve.util.restclient.RestCaller;

import net.sf.json.JSONArray;
import net.sf.json.JSONObject;
import net.sf.json.JSONSerializer;

public class McAfeeAPI extends AbstractGatewayAPI
{    
    private enum TimeRange {
        CUSTOM,
        LAST_MINUTE,
        LAST_10_MINUTES,
        LAST_30_MINUTES,
        LAST_HOUR,
        CURRENT_DAY,
        PREVIOUS_DAY,
        LAST_24_HOURS,
        LAST_2_DAYS,
        LAST_3_DAYS,
        CURRENT_WEEK,
        PREVIOUS_WEEK,
        CURRENT_MONTH,
        PREVIOUS_MONTH,
        CURRENT_QUARTER,
        PREVIOUS_QUARTER,
        CURRENT_YEAR,
        PREVIOUS_YEAR;
        
        public static boolean contains(String s)
        {
            for(TimeRange timeRange:values())
                 if (timeRange.name().equals(s)) 
                    return true;
            return false;
        } 
    }
    
    private static String HOSTNAME = "10.50.2.24";
    private static String PORT = "";
    private static String SERVICE_URI = "/rs/esm/";
    private static String USERNAME = "NGCP";
    private static String P_ASSWORD = "Resolve1234!";
    private static String SSL = "true";
    private static String SSLTYPE = "self-signed";
    private static String RESP_TYPE = "application/json";
    private static String LOCALE = "en_US";
    private static String TOKEN = "Xsrf-Token";
    
    private static String baseUrl = "";
    private static Integer userId = null;

    private static boolean selfSigned = false;
    private static boolean trustAll = true;
    
    private static volatile String token = null;
    
    private static McAfeeGateway instance = McAfeeGateway.getInstance();
    
    static {
        Map<String, Object> properties = instance.loadSystemProperties();
        
        HOSTNAME = (String)properties.get("HOST");
        PORT = (String)properties.get("PORT");
        USERNAME = (String)properties.get("USERNAME");
        P_ASSWORD = (String)properties.get("PASSWORD");
        SSL = (String)properties.get("SSL");
        SSLTYPE = (String)properties.get("SSLTYPE");

        StringBuilder sb = new StringBuilder();
        
        if(StringUtils.isNotEmpty(SSL) && SSL.equalsIgnoreCase("true")) {
            if(SSLTYPE.equalsIgnoreCase("self-signed")) {
                trustAll = false;
                selfSigned = true;
            }
            
            else if(SSLTYPE.equalsIgnoreCase("trust-all")) {
                trustAll = true;
                selfSigned = false;
            }
            
            else {
                trustAll = false;
                selfSigned = false;
            }

            sb.append("https://");
        }
        
        else
            sb.append("http://");
        
        sb.append(HOSTNAME);
        
        if(StringUtils.isNotBlank(PORT))
            sb.append(":").append(PORT);
        
        sb.append(SERVICE_URI);
        
        baseUrl = sb.toString();

        try {
            login();
        } catch(Exception e) {
            Log.log.error(e.getMessage(), e);
        }
    }
    
    private static void login() throws Exception {
        
        if(StringUtils.isBlank(USERNAME))
            throw new Exception("Username is not available.");
        
        try {
//          RestCaller client = new RestCaller(baseUrl, USERNAME, P_ASSWORD, true, true, selfSigned, trustAll);
            RestCaller client = new RestCaller(baseUrl, "", "", false, true, selfSigned, trustAll);
                            
            Map<String, String> headers = new HashMap<String, String>();
            headers.put("Content-Type", RESP_TYPE);
            
            String encUser = new String(org.apache.commons.codec.binary.Base64.encodeBase64(USERNAME.getBytes()));
            String encPass = new String(org.apache.commons.codec.binary.Base64.encodeBase64(P_ASSWORD.getBytes()));
            
            JSONObject body = new JSONObject();
            body.accumulate("username", encUser);
            body.accumulate("password", encPass);
            body.accumulate("locale", LOCALE);

            String resp = client.callPost("login", null, body.toString(), headers);

            Map<String, String> respHeaders = client.getResponseHeaders();
            
            if(respHeaders != null)
                token = respHeaders.get(TOKEN);
            
            if(userId == null && StringUtils.isNotBlank(resp)) {
                JSONObject json = JSONObject.fromObject(resp);
                userId = (Integer)json.get("userId");
            }
            
            Log.log.debug("token = " + token);
        } catch(Exception e) {
            Log.log.error(e.getMessage(), e);
            throw e;
        }
    }
    
    /**
     * Retrieves a list of alarms that have been triggered and have not been acknowledged
     * @param timeRange
     * @param startTime
     * @param endTime
     * @param pageSize
     * @param pageNumber
     * @return
     * @throws Exception
     */
    static List<Map<String, String>> getUnacknowledgedAlarms(String triggeredTimeRange, String startTime, String endTime, Integer pageSize, Integer pageNumber) throws Exception {

        List<Map<String, String>> result = null;
        
        Map<String, String> headers = new HashMap<String, String>();
        
        headers.put("Content-Type", RESP_TYPE);
        headers.put("Accept", RESP_TYPE);
        headers.put("X-Xsrf-Token", token);
        
        Map<String, String> params = new HashMap<String, String>();

        if(triggeredTimeRange == null) {
            params.put("triggeredTimeRange", "CUSTOM");
//          params.put("customStart", "2017-03-15T23:50:42.000Z");
            params.put("customStart", startTime);
            params.put("customEnd", endTime);
//          params.put("customEnd", "2017-03-10T23:50:42.000Z");
//          params.put("customEnd", "2017-03-31T23:50:42.000Z");
        }
        
        else
            params.put("triggeredTimeRange", triggeredTimeRange);

        params.put("pageSize", pageSize.toString());
        params.put("pageNumber", pageNumber.toString());
        
        JSONObject body = new JSONObject();
        
        RestCaller client = new RestCaller(baseUrl, "", "", false, true, selfSigned, trustAll);
        
        try {
            JSONObject user = new JSONObject();
            user.put("username", USERNAME);
            
            JSONObject id = new JSONObject();
            id.put("value", userId);
            
            user.put("id", id);
            
            body.put("assignedUser", user);
            
            String resp = client.callPost("alarmGetUnacknowledgedTriggeredAlarms", params, body.toString(), headers);

            Log.log.debug(resp);
            
            result = parseResult(resp);
        } catch(Exception e) {
            try {
                login();

                headers.put("X-Xsrf-Token", token);
                
                String resp = client.callPost("alarmGetUnacknowledgedTriggeredAlarms", params, body.toString(), headers);
                
                result = parseResult(resp);

                Log.log.debug(resp);
            } catch(Exception ee) {
                Log.log.error(ee.getMessage(), ee);
                throw ee;
            }
        }
        
        return result;
    }
    
    static List<Map<String, String>> getTriggeredAlarmsPaged(String triggeredTimeRange, String startTime, String endTime, Integer pageSize, Integer pageNumber) throws Exception {

        List<Map<String, String>> result = null;
        
        Map<String, String> headers = new HashMap<String, String>();
        
        headers.put("Content-Type", RESP_TYPE);
        headers.put("Accept", RESP_TYPE);
        headers.put("X-Xsrf-Token", token);
        
        Map<String, String> params = new HashMap<String, String>();
        
        if(triggeredTimeRange == null) {
            params.put("triggeredTimeRange", "CUSTOM");
            params.put("customStart", "2017-03-15T23:50:42.000Z");
            params.put("customStart", startTime);
            params.put("customEnd", endTime);
        }
        
        else
            params.put("triggeredTimeRange", triggeredTimeRange);

        params.put("pageSize", pageSize.toString());
        params.put("pageNumber", pageNumber.toString());
        
        JSONObject body = new JSONObject();
        
        RestCaller client = new RestCaller(baseUrl, "", "", false, true, selfSigned, trustAll);
        
        try {
            JSONObject user = new JSONObject();
            user.put("username", USERNAME);
            
            JSONObject id = new JSONObject();
            id.put("value", userId);
            
            user.put("id", id);
            
            body.put("assignedUser", user);
            
            String resp = client.callPost("alarmGetTriggeredAlarmsPaged", params, body.toString(), headers);

            Log.log.debug(resp);
            
            result = parseResult(resp);
        } catch(Exception e) {
            try {
                login();

                headers.put("X-Xsrf-Token", token);
                
                String resp = client.callPost("alarmGetTriggeredAlarmsPaged", params, body.toString(), headers);
                
                result = parseResult(resp);

                Log.log.debug(resp);
            } catch(Exception ee) {
                Log.log.error(ee.getMessage(), ee);
                throw ee;
            }
        }
        
        return result;
    }
    
    /**
     * Gets the details for the triggered alarm
     * @param alarmId: Required - A single alarm id
     * @return JSON string of alarm details
     * @throws Exception
     */
    public static String getAlarmDetail(Integer alarmId) throws Exception {
        
        if(alarmId == null)
            throw new Exception("No alerm id is available for getting alarm detail.");
        
        String result = "";
        
        Map<String, String> headers = new HashMap<String, String>();
        
        headers.put("Content-Type", RESP_TYPE);
        headers.put("Accept", RESP_TYPE);
        headers.put("X-Xsrf-Token", token);
        
        JSONObject body = new JSONObject();
        
        RestCaller client = new RestCaller(baseUrl, "", "", false, true, selfSigned, trustAll);
        
        try {
            body.put("id", alarmId);
            
            result = client.callPost("notifyGetTriggeredNotificationDetail", null, body.toString(), headers);

            Log.log.debug(result);
        } catch(Exception e) {
            try {
                login();

                headers.put("X-Xsrf-Token", token);
                
                result = client.callPost("notifyGetTriggeredNotificationDetail", null, body.toString(), headers);

                Log.log.debug(result);
            } catch(Exception ee) {
                Log.log.error(ee.getMessage(), ee);
                throw ee;
            }
        }

        return result;
    }
    
    /**
     * Mark a triggered alarm as acknowledged
     * @param alarmId: Required - A single alarm id
     * @throws Exception
     */
    public static void alarmAcknowledgeTriggeredAlarm(Integer alarmId) throws Exception {
        
        if(alarmId == null)
            throw new Exception("No alerm id is available to acknowledge.");
        
        List<Integer> alarmIds = new ArrayList<Integer>();
        alarmIds.add(alarmId);
        
        alarmAcknowledgeTriggeredAlarms(alarmIds);
    }
    
    /**
     * Mark a triggered alarm as acknowledged
     * @param alarmIds: Required - A list of alarm ids
     * @throws Exception
     */
    public static void alarmAcknowledgeTriggeredAlarms(List<Integer> alarmIds) throws Exception {

        if(alarmIds == null || alarmIds.size() == 0)
            throw new Exception("No alerm id is available to acknowledge.");
        
        Map<String, String> headers = new HashMap<String, String>();
        
        headers.put("Content-Type", RESP_TYPE);
        headers.put("X-Xsrf-Token", token); 
        
        JSONObject body = new JSONObject();
        
        RestCaller client = new RestCaller(baseUrl, "", "", false, true, selfSigned, trustAll);
        
        try {
            JSONArray ids = new JSONArray();
            
            for(Integer alarmId:alarmIds) {
                ids.add(alarmId);
            }
            
            body.put("triggeredIds", ids);
            
            client.callPost("alarmAcknowledgeTriggeredAlarm", null, body.toString(), headers);
        } catch(Exception e) {
            try {
                login();

                headers.put("X-Xsrf-Token", token);

                String resp = client.callPost("alarmAcknowledgeTriggeredAlarm", null, body.toString(), headers);
                Log.log.debug(resp);
            } catch(Exception ee) {
                Log.log.error(ee.getMessage(), ee);
                throw ee;
            }
        }
    }
    
    private static List<Map<String, String>> parseResult(String resp) throws Exception {
        
        if(StringUtils.isBlank(resp))
            return null;
        
        List<Map<String, String>> result = new ArrayList<Map<String, String>>();
        
        if(StringUtils.isNotEmpty(resp)) {
            JSONArray list = (JSONArray) JSONSerializer.toJSON(resp);
            if(list != null) {
                for(int i=0; i<list.size(); i++) {
                    String alarm = list.getString(i);
                    if(StringUtils.isBlank(alarm))
                        continue;
                    JSONObject json = JSONObject.fromObject(alarm);

                    Map<String, String> map = new HashMap<String, String>();
                    map.put("id", json.getString("id"));
                    map.put("severity", json.getString("severity"));
                    map.put("assignee", json.getString("assignee"));
                    map.put("triggeredDate", json.getString("triggeredDate"));
                    map.put("acknowledgedDate", json.getString("acknowledgedDate"));
                    map.put("acknowledgedUsername", json.getString("acknowledgedUsername"));
                    map.put("alarmName", json.getString("alarmName"));
                    map.put("conditionType", json.getString("conditionType"));
                    map.put("summary", json.getString("summary"));
                    
                    Log.log.debug("id = " + json.getString("id"));
                    Log.log.debug("triggeredDate = " + json.getString("triggeredDate"));

                    result.add(map);
                }
            }
        }
        
        return result;
    }

    private static void main1(String[] args) {
        
        // https://10.50.2.24/rs/esm/alarmGetUnacknowledgedTriggeredAlarms?triggeredTimeRange=CUSTOM&customStart=2017-03-01T15:21:41.691Z&customEnd=2017-03-01T15:21:41.692Z&pageSize=0&pageNumber=0

        try {
            login();
            
            // 1. Get unacknowledged alarms
            List<Map<String, String>> result = getUnacknowledgedAlarms("CURRENT_QUARTER", null, null, 0, 0);
            System.out.println(result);
            
            // 2. Get triggered alarms paged
            result = getTriggeredAlarmsPaged("CURRENT_QUARTER", null, null, 0, 0);
            System.out.println(result);
            
            if(result != null && result.size() != 0) {
                // 3. Get alarm detail
                Map<String, String> alarm = result.get(0);
                String alarmId = alarm.get("id");
                System.out.println(alarmId);
                
                Integer id = (new Integer(alarmId));
                String alarmDetail = getAlarmDetail(id);
                System.out.println(alarmDetail);
                
                // 4. Send acknowledgement for alarm
                alarmAcknowledgeTriggeredAlarm(id);
/*            
                List<Long> alarmIds = new ArrayList<Long>();
                alarmIds.add((new Long(alarmId)).longValue());
                alarmAcknowledgeTriggeredAlarm(alarmIds);
*/            
            }
        } catch(Exception e) {
            e.printStackTrace();
        }
    }
}


