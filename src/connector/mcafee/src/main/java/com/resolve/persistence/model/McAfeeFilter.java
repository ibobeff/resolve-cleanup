package com.resolve.persistence.model;

import java.util.List;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Lob;
import javax.persistence.Table;
import javax.persistence.UniqueConstraint;
import com.resolve.services.interfaces.VO;
import com.resolve.services.hibernate.vo.McAfeeFilterVO;

@Entity
@Table(name = "mcafee_filter", uniqueConstraints = { @UniqueConstraint(columnNames = { "u_name", "u_queue" }) })
public class McAfeeFilter extends GatewayFilter<McAfeeFilterVO> {

    private static final long serialVersionUID = 1L;

    public McAfeeFilter() {
    }

    public McAfeeFilter(McAfeeFilterVO vo) {
        applyVOToModel(vo);
    }

    private String UAttr1;

    private Integer UPagesize;

    private String UTimerange;

    private String UAttr2;

    @Column(name = "u_attr1", length = 256)
    public String getUAttr1() {
        return this.UAttr1;
    }

    public void setUAttr1(String uAttr1) {
        this.UAttr1 = uAttr1;
    }

    @Column(name = "u_pagesize")
    public Integer getUPagesize() {
        return this.UPagesize;
    }

    public void setUPagesize(Integer uPagesize) {
        this.UPagesize = uPagesize;
    }

    @Column(name = "u_timerange", length = 100)
    public String getUTimerange() {
        return this.UTimerange;
    }

    public void setUTimerange(String uTimerange) {
        this.UTimerange = uTimerange;
    }

    @Column(name = "u_attr2", length = 256)
    public String getUAttr2() {
        return this.UAttr2;
    }

    public void setUAttr2(String uAttr2) {
        this.UAttr2 = uAttr2;
    }

    public McAfeeFilterVO doGetVO() {
        McAfeeFilterVO vo = new McAfeeFilterVO();
        super.doGetBaseVO(vo);
        vo.setUAttr1(getUAttr1());
        vo.setUPagesize(getUPagesize());
        vo.setUTimerange(getUTimerange());
        vo.setUAttr2(getUAttr2());
        return vo;
    }

    @Override
    public void applyVOToModel(McAfeeFilterVO vo) {
        if (vo == null) return;
        super.applyVOToModel(vo);
        if (!VO.STRING_DEFAULT.equals(vo.getUAttr1())) this.setUAttr1(vo.getUAttr1()); else ;
        if (!VO.INTEGER_DEFAULT.equals(vo.getUPagesize())) this.setUPagesize(vo.getUPagesize()); else ;
        if (!VO.STRING_DEFAULT.equals(vo.getUTimerange())) this.setUTimerange(vo.getUTimerange()); else ;
        if (!VO.STRING_DEFAULT.equals(vo.getUAttr2())) this.setUAttr2(vo.getUAttr2()); else ;
    }

    @Override
    public List<String> ugetCdataProperty() {
        List<String> list = super.ugetCdataProperty();
        list.add("UAttr1");
        list.add("UTimerange");
        list.add("UAttr2");
        return list;
    }
}

