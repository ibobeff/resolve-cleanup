package com.resolve.services.hibernate.vo;

import com.resolve.util.MappingAnnotation;

public class McAfeeFilterVO extends GatewayFilterVO {

    private static final long serialVersionUID = 1L;

    public McAfeeFilterVO() {
    }

    private String UAttr1;

    private Integer UPagesize;

    private String UTimerange;

    private String UAttr2;

    @MappingAnnotation(columnName = "ATTR1")
    public String getUAttr1() {
        return this.UAttr1;
    }

    public void setUAttr1(String uAttr1) {
        this.UAttr1 = uAttr1;
    }

    @MappingAnnotation(columnName = "PAGESIZE")
    public Integer getUPagesize() {
        return this.UPagesize;
    }

    public void setUPagesize(Integer uPagesize) {
        this.UPagesize = uPagesize;
    }

    @MappingAnnotation(columnName = "TIMERANGE")
    public String getUTimerange() {
        return this.UTimerange;
    }

    public void setUTimerange(String uTimerange) {
        this.UTimerange = uTimerange;
    }

    @MappingAnnotation(columnName = "ATTR2")
    public String getUAttr2() {
        return this.UAttr2;
    }

    public void setUAttr2(String uAttr2) {
        this.UAttr2 = uAttr2;
    }
    
    @Override
    public int hashCode()
    {
        final int prime = 31;
        int result = super.hashCode();
        result = prime * result + ((UAttr1 == null) ? 0 : UAttr1.hashCode());
        result = prime * result + ((UAttr2 == null) ? 0 : UAttr2.hashCode());
        result = prime * result + ((UPagesize == null) ? 0 : UPagesize.hashCode());
        result = prime * result + ((UTimerange == null) ? 0 : UTimerange.hashCode());
        return result;
    }
}

