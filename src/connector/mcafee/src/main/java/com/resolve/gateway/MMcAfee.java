package com.resolve.gateway;

import java.util.Map;
import com.resolve.gateway.mcafee.McAfeeFilter;
import com.resolve.gateway.mcafee.McAfeeGateway;

public class MMcAfee extends MGateway {

    private static final String RSCONTROL_SET_FILTERS = "MMcAfee.setFilters";

    private static final McAfeeGateway instance = McAfeeGateway.getInstance();

    public MMcAfee() {
        super.instance = instance;
    }

    protected String getSetFilterMethodName() {
        return RSCONTROL_SET_FILTERS;
    }

    /**
     * Populates the {@link Filter} data into the given filterMap {@link Map} instance
     * 
     * @param filterMap
     *      the {@link Map} instance to hold the {@link Filter} data
     * @param filter
     *      a {@link McAfeeFilter} instance
     *            
     */
    public void populateGatewaySpecificFilterProperties(Map<String, String> filterMap, Filter filter) {
        if (instance.isActive()) {
            McAfeeFilter mcafeeFilter = (McAfeeFilter) filter;
            filterMap.put(McAfeeFilter.ATTR1, mcafeeFilter.getAttr1());
            filterMap.put(McAfeeFilter.PAGESIZE, "" + mcafeeFilter.getPagesize());
            filterMap.put(McAfeeFilter.TIMERANGE, mcafeeFilter.getTimerange());
            filterMap.put(McAfeeFilter.ATTR2, mcafeeFilter.getAttr2());
        }
    }
}

