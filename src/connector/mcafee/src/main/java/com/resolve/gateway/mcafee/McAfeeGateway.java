package com.resolve.gateway.mcafee;

import java.util.HashMap;
import java.util.Map;
import java.util.List;
import java.util.ArrayList;

import com.resolve.rsbase.MainBase;
import com.resolve.gateway.BaseClusteredGateway;
import com.resolve.gateway.BaseFilter;
import com.resolve.gateway.Filter;
import com.resolve.gateway.MMcAfee;
import com.resolve.util.Log;
import com.resolve.util.StringUtils;
import com.resolve.rsremote.ConfigReceiveGateway;

public class McAfeeGateway extends BaseClusteredGateway {

    private static volatile McAfeeGateway instance = null;

    private String queue = null;

    public static McAfeeGateway getInstance(ConfigReceiveMcAfee config) {
        if (instance == null) {
            instance = new McAfeeGateway(config);
        }
        return instance;
    }

    /**
     * This method is called once the initialization is done. If it gets called
     * before the initialization it will throw exception.
     *
     * @return
     */
    public static McAfeeGateway getInstance() {
        if (instance == null) {
            throw new RuntimeException("McAfee Gateway is not initialized correctly..");
        } else {
            return instance;
        }
    }

    private McAfeeGateway(ConfigReceiveMcAfee config) {
        super(config);
    }

    @Override
    public String getLicenseCode() {
        return "MCAFEE";
    }

    @Override
    protected void addAdditionalValues(Map<String, Object> arg0, Map<String, String> arg1) {
    }

    @Override
    protected String getGatewayEventType() {
        return "MCAFEE";
    }

    @Override
    protected String getMessageHandlerName() {
        return MMcAfee.class.getSimpleName();
    }

    @Override
    protected Class<MMcAfee> getMessageHandlerClass() {
        return MMcAfee.class;
    }

    public String getQueueName() {
        return queue + getOrgSuffix();
    }

    @Override
    public void start() {
        Log.log.debug("Starting McAfee Gateway");
        super.start();
    }

    @Override
    public void run() {
        if (!((ConfigReceiveGateway) configurations).isMock())
			super.sendSyncRequest();
        while (running) {
            try {
                long startTime = System.currentTimeMillis();
                if (primaryDataQueue.isEmpty()) {
                    Log.log.trace("Local Primary Event Queue for " + getGatewayEventType() + " is empty.....");
                    for (Filter filter : orderedFilters) {
                        if (shouldFilterExecute(filter, startTime)) {
                            Log.log.trace("Filter " + filter.getId() + " executing");
                            McAfeeFilter currentFilter = (McAfeeFilter)filter;
                            List<Map<String, String>> results = invokeService(currentFilter);

                            if(results.size() == 0)
                                continue;

                            for (Map<String, String> result : results) {
                                addToPrimaryDataQueue(filter, result);
                            }
                            
                            Map<String, String> last = results.get(0);

                            String lastAlarmId = last.get(currentFilter.LAST_UPDATED_TIME_FIELD);
                            
                            Log.log.debug("The last retrieved alarm id is " + lastAlarmId);

                            if(StringUtils.isNotEmpty(lastAlarmId))
                                currentFilter.setLastAlarmId(lastAlarmId);
                        } else {
                            Log.log.trace("Filter:" + filter.getId() + " is inactive or not ready to execute yet.....");
                        }
                    }
                    if (orderedFilters == null || orderedFilters.size() == 0) {
                        Log.log.trace("There is not deployed filter for McAfeeGateway");
                    }
                }
                if (orderedFilters.size() == 0) {
                    Thread.sleep(interval);
                }
            } catch (Exception e) {
                Log.log.warn("Warning: " + e.getMessage());
                Log.alert("Instance " + MainBase.main.configId.getGuid() + " " + getLicenseCode() + "-" + getQueueName() + "-" + getInstanceType() + " Polling Failed", "Instance " + MainBase.main.configId.getGuid() + " " + getLicenseCode() + "-" + getQueueName() + "-" + getInstanceType() + " Polling Failed " + "due to " + e.getMessage());
                try {
                    Thread.sleep(interval);
                } catch (InterruptedException ie) {
                    Log.log.warn("sleep interrupted: " + ie.getMessage(), ie);
                }
            }
        }
    }

    /**
     * Add customized code here to get the data as List<Map<String, String>> format for each filter 
     * @return List<Map<String, String>>
     */
    public List<Map<String, String>> invokeService(McAfeeFilter filter) {
        List<Map<String, String>> result = new ArrayList<Map<String, String>>();
        String lastAlarmId = filter.getLastAlarmId();
        
        try {
/*            SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.000'Z'");
            sdf.setTimeZone(TimeZone.getTimeZone("GMT"));
//            sdf.setTimeZone(TimeZone.getTimeZone("UTC-TAI"));

            Date now = new Date();
            long previous = now.getTime() - filter.getInterval()*1000;
            
//            String start = "1970-01-01T00:00:00.000Z";
            String start = sdf.format(new Date(previous));
            String end = sdf.format(now);
            
            Log.log.debug("start time: " + start);
            Log.log.debug("end time: " + end);
 */           
            int last = 0;
            int pageSize = 0;
            int pageNumber = 1;
            
            String triggeredTimeRange = filter.getTimerange();
            Log.log.debug("triggeredTimeRange = " + triggeredTimeRange);
            
            Integer page = filter.getPagesize();
            if(page != null)
                pageSize = page.intValue();
            Log.log.debug("Page size = " + pageSize);

            if(lastAlarmId != null)
                last = (new Integer(lastAlarmId)).intValue();
            Log.log.debug("Last alarm Id = " + lastAlarmId);
            
            for(;;) {
                Log.log.debug("Page number = " + pageNumber);
                
                List<Map<String, String>> alarms = McAfeeAPI.getTriggeredAlarmsPaged(triggeredTimeRange, null, null, pageSize, pageNumber);
                
                if(alarms == null || alarms.size() == 0)
                    break;
                
                for(int i=0; i<alarms.size(); i++) {
                    Map<String, String> alarm =  alarms.get(i);
                    
                    String id = alarm.get("id");
                    lastAlarmId = id;
                    
                    if(id != null) {
                        try {
                            Integer alarmId = new Integer(id);
                            if(alarmId.intValue() <= last) {
                                alarms.remove(i);
                                i--;
                                
                                if(i < -1)
                                    break;
                                else
                                    continue;
                            }
                        } catch(Exception ee) {
                            Log.log.error(ee.getMessage(), ee);
                        }
                    }
                    
                    if(StringUtils.isNotEmpty(alarm.get("acknowledgedDate"))) {
                        alarms.remove(i);
                        i--;
                        if(i < -1)
                            break;
                        else
                            continue;
                    }
                    
                    alarm.put("ALERTID", id);
                }
                
                result.addAll(alarms);
                
                pageNumber ++;
            }
            
            filter.setLastAlarmId(lastAlarmId);
        } catch(Exception e) {
            Log.log.error(e.getMessage(), e);
        }
 
        return result;
    }

    /**
     * Add customized code here to initilize the connection with 3rd party system
     * @return List<Map<String, String>>
     */
    @Override
    protected void initialize() {
        ConfigReceiveMcAfee config = (ConfigReceiveMcAfee) configurations;
        queue = config.getQueue().toUpperCase();
        this.gatewayConfigDir = "/config/mcafee/";
        //Add customized code here to initilize the connection with 3rd party system;
    }

    /**
     * Add customized code here to do when stop the gateway
     */
    @Override
    public void stop() {
        Log.log.warn("Stopping McAfee gateway");
        //Add customized code here to stop the connection with 3rd party system;
        super.stop();
    }

    @Override
    public Filter getFilter(Map<String, Object> params) {
        McAfeeFilter filter = new McAfeeFilter((String) params.get(BaseFilter.ID), (String) params.get(BaseFilter.ACTIVE), 
								(String) params.get(BaseFilter.ORDER), (String) params.get(BaseFilter.INTERVAL), 
								(String) params.get(BaseFilter.EVENT_EVENTID), (String) params.get(BaseFilter.RUNBOOK), 
								(String) params.get(BaseFilter.SCRIPT), (String) params.get(McAfeeFilter.ATTR1), (String) params.get(McAfeeFilter.ATTR2),
								(String) params.get(McAfeeFilter.PAGESIZE), (String) params.get(McAfeeFilter.TIMERANGE));
        
        if ((params.containsKey(filter.LAST_ALARM_ID)) && StringUtils.isNotBlank((String)params.get(filter.LAST_ALARM_ID)))
            filter.setLastAlarmId((String)params.get(filter.LAST_ALARM_ID));
        
        return filter;
    }
    
    Map<String, Object> loadSystemProperties() {

        Map<String, Object> properties = new HashMap<String, Object>();

        ConfigReceiveMcAfee config = (ConfigReceiveMcAfee) configurations;

        properties.put("HOST", config.getHost());
        properties.put("PORT", config.getPort());
        properties.put("USERNAME", config.getUsername());
        properties.put("PASSWORD", config.getPassword());
        properties.put("SSL", config.getSsl());
        properties.put("SSLTYPE", config.getSsltype());

        return properties;
    }
 
}

