package com.resolve.rscontrol;

import com.resolve.persistence.model.McAfeeFilter;

public class MMcAfee extends MGateway {

    private static final String MODEL_NAME = McAfeeFilter.class.getSimpleName();

    protected String getModelName() {
        return MODEL_NAME;
    }
}

