package com.resolve.gateway.mcafee;

import com.resolve.gateway.BaseFilter;
import com.resolve.gateway.RetainValue;
import com.resolve.util.Log;

public class McAfeeFilter extends BaseFilter {
	public static final String LAST_ALARM_ID = "LAST_ALARM_ID";
    public static final String LAST_UPDATED_TIME_FIELD = "id";
	
	private String lastAlarmId = null;

    public static final String ATTR1 = "ATTR1";

    public static final String ATTR2 = "ATTR2";
	
    public static final String PAGESIZE = "PAGESIZE";

    public static final String TIMERANGE = "TIMERANGE";

    private String attr1;
    
    private String attr2;

    private Integer pagesize;

    private String timerange;

    public McAfeeFilter(String id, String active, String order, String interval, String eventEventId, String runbook, String script, String attr1, String attr2, String pagesize, String timerange) {
        super(id, active, order, interval, eventEventId, runbook, script);
        this.attr1 = attr1;
		this.attr2 = attr2;
		
        try {
            this.pagesize = new Integer(pagesize);
        } catch (Exception e) {
            Log.log.error("pagesize" + " should be in type " + "Integer");
        }
		
        this.timerange = timerange;
    }

    public String getAttr1() {
        return this.attr1;
    }

    public void setAttr1(String attr1) {
        this.attr1 = attr1;
    }

    public String getAttr2() {
        return this.attr2;
    }

    public void setAttr2(String attr2) {
        this.attr2 = attr2;
    }
    
    public Integer getPagesize() {
        return this.pagesize;
    }
    
    public void setPagesize(Integer pagesize) {
        this.pagesize = pagesize;
    }

    public String getTimerange() {
        return this.timerange;
    }

    public void setTimerange(String timerange) {
        this.timerange = timerange;
    }
    
	@RetainValue
    public String getLastAlarmId()
    {
        return lastAlarmId;
    }

    public void setLastAlarmId(String lastAlarmId)
    {
        this.lastAlarmId = lastAlarmId;
    }

}

