package com.resolve.gateway;

import java.util.Map;
import com.resolve.gateway.groovy.GroovyFilter;
import com.resolve.gateway.groovy.GroovyGateway;

public class MGroovy extends MGateway {

    private static final String RSCONTROL_SET_FILTERS = "MGroovy.setFilters";

    private static final GroovyGateway instance = GroovyGateway.getInstance();

    public MGroovy() {
        super.instance = instance;
    }

    protected String getSetFilterMethodName() {
        return RSCONTROL_SET_FILTERS;
    }

    /**
     * Populates the {@link Filter} data into the given filterMap {@link Map} instance
     * 
     * @param filterMap
     *      the {@link Map} instance to hold the {@link Filter} data
     * @param filter
     *      a {@link GroovyFilter} instance
     *            
     */
    public void populateGatewaySpecificFilterProperties(Map<String, String> filterMap, Filter filter) {
        if (instance.isActive()) {
            GroovyFilter groovyFilter = (GroovyFilter) filter;
            filterMap.put(GroovyFilter.GROOVYSCRIPT, groovyFilter.getGroovyScript());
        }
    }
}

