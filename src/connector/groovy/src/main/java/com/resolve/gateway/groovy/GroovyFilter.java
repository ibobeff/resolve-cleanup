package com.resolve.gateway.groovy;

import com.resolve.gateway.BaseFilter;
import com.resolve.util.Log;

public class GroovyFilter extends BaseFilter {

    public static final String GROOVYSCRIPT = "GROOVYSCRIPT";

    private String groovyScript;

    public GroovyFilter(String id, String active, String order, String interval, String eventEventId, String runbook, String script, String groovyScript) {
        super(id, active, order, interval, eventEventId, runbook, script);
        this.groovyScript = groovyScript;
    }

    public String getGroovyScript() {
        return this.groovyScript;
    }

    public void setGroovyScript(String groovyScript) {
        this.groovyScript = groovyScript;
    }
}

