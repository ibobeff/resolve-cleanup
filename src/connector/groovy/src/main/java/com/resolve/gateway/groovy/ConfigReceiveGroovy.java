package com.resolve.gateway.groovy;

import java.io.File;
import java.util.HashMap;
import java.util.Map;
import org.apache.commons.io.FileUtils;

import com.resolve.rsbase.MainBase;
import com.resolve.gateway.Filter;
import com.resolve.rsremote.ConfigReceiveGateway;
import com.resolve.util.Log;
import com.resolve.util.ObjectProperties;
import com.resolve.util.StringUtils;
import com.resolve.util.XDoc;

public class ConfigReceiveGroovy extends ConfigReceiveGateway {

    private static final long serialVersionUID = 1L;

    private static final String RECEIVE_GROOVY_NODE = "./RECEIVE/GROOVY/";

    private static final String RECEIVE_GROOVY_FILTER = RECEIVE_GROOVY_NODE + "FILTER";

    private String queue = "GROOVY";

    private static final String RECEIVE_GROOVY_ATTR_PORT = RECEIVE_GROOVY_NODE + "@PORT";

    private static final String RECEIVE_GROOVY_ATTR_USERNAME = RECEIVE_GROOVY_NODE + "@USERNAME";

    private String port = "";

    private String username = "";

    public String getQueue() {
        return queue;
    }

    public void setQueue(String queue) {
        this.queue = queue;
    }

    public String getPort() {
        return this.port;
    }

    public void setPort(String port) {
        this.port = port;
    }

    public String getUsername() {
        return this.username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public ConfigReceiveGroovy(XDoc config) throws Exception {
        super(config);
        define("port", STRING, RECEIVE_GROOVY_ATTR_PORT);
        define("username", STRING, RECEIVE_GROOVY_ATTR_USERNAME);
    }

    @Override
    public String getRootNode() {
        return RECEIVE_GROOVY_NODE;
    }

    @Override
    public void load() throws Exception {
        try {
            loadAttributes();
            if (isActive()) {
                GroovyGateway groovyGateway = GroovyGateway.getInstance(this);
                filters = xdoc.getListMapValue(RECEIVE_GROOVY_FILTER);
                if (filters.size() > 0) {
                    for (Map<String, Object> values : filters) {
                        String filterName = ((String) values.get(GroovyFilter.ID)).toLowerCase();
                        File scriptFile = getFile(MainBase.main.release.serviceName + "/config/groovy/" + filterName + ".groovy");
                        try {
                            if (scriptFile.exists()) {
                                String groovy = FileUtils.readFileToString(scriptFile, "UTF-8");
                                if (StringUtils.isNotBlank(groovy)) {
                                    values.put(GroovyFilter.SCRIPT, groovy);
                                    Log.log.debug("Loaded groovy script: " + scriptFile);
                                }
                            }
                            groovyGateway.setFilter(values);
                        } catch (Exception e) {
                            Log.log.warn(e.getMessage() + ". Skipping file: " + scriptFile.getAbsolutePath());
                        }
                    }
                }
            }
        } catch (Throwable e) {
            setActive(false);
            Log.log.warn("Couldn not load configuration for Groovy gateway, check blueprint. " + e.getMessage(), e);
        }
    }

    @Override
    public void save() throws Exception {
        File dir = getFile(MainBase.main.release.serviceName + "/config/groovy");
        if (!dir.exists()) {
            FileUtils.forceMkdir(dir);
        }
        if (filters != null && isActive()) {
            filters.clear();
            for (Filter filter : GroovyGateway.getInstance().getFilters().values()) {
                GroovyFilter groovyFilter = (GroovyFilter) filter;
                String groovy = groovyFilter.getScript();
                String scriptFileName;
                File scriptFile = null;
                if (StringUtils.isNotBlank(groovy)) {
                    scriptFileName = groovyFilter.getId().toLowerCase() + ".groovy";
                    scriptFile = getFile(MainBase.main.release.serviceName + "/config/groovy/" + scriptFileName);
                }
                Map<String, Object> entry = new HashMap<String, Object>();
                putFilterDataIntoEntry(entry, groovyFilter);
                filters.add(entry);
                if (scriptFile != null) {
                    try {
                        FileUtils.writeStringToFile(scriptFile, groovy, "UTF-8");
                    } catch (Exception e) {
                        Log.log.warn(e.getMessage() + ". Skipping file: " + scriptFile.getAbsolutePath());
                    }
                }
            }
            xdoc.setListMapValue(RECEIVE_GROOVY_FILTER, filters);
        }
        if (nameProperties != null && isActive()) {
            nameProperties.clear();
            for (String name : GroovyGateway.getInstance().getNameProperties().keySet()) {
                Map<String, Object> entry = new HashMap<String, Object>();
                entry.put(NAME_KEY, name);
                nameProperties.add(entry);
                Map props = GroovyGateway.getInstance().getNameProperties().getNameProperties(name);
                ObjectProperties prop = new ObjectProperties(props);
                prop.save(props);
            }
        }
        saveAttributes();
    }

    private void putFilterDataIntoEntry(Map<String, Object> entry, GroovyFilter groovyFilter) {
        entry.put(GroovyFilter.ID, groovyFilter.getId());
        entry.put(GroovyFilter.ACTIVE, String.valueOf(groovyFilter.isActive()));
        entry.put(GroovyFilter.ORDER, String.valueOf(groovyFilter.getOrder()));
        entry.put(GroovyFilter.INTERVAL, String.valueOf(groovyFilter.getInterval()));
        entry.put(GroovyFilter.EVENT_EVENTID, groovyFilter.getEventEventId());
        entry.put(GroovyFilter.RUNBOOK, groovyFilter.getRunbook());
        entry.put(GroovyFilter.SCRIPT, groovyFilter.getScript());
        entry.put(GroovyFilter.GROOVYSCRIPT, groovyFilter.getGroovyScript());
    }
}

