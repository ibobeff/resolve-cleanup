package com.resolve.gateway.groovy;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import com.resolve.gateway.BaseClusteredGateway;
import com.resolve.gateway.BaseFilter;
import com.resolve.gateway.Filter;
import com.resolve.gateway.MGroovy;
import com.resolve.groovy.script.GroovyScript;
import com.resolve.rsbase.MainBase;
import com.resolve.rsremote.ConfigReceiveGateway;
import com.resolve.util.Constants;
import com.resolve.util.Log;

import groovy.lang.Binding;

public class GroovyGateway extends BaseClusteredGateway {
    private int TASK_TIMEOUT = 60 * 1000;
    private static volatile GroovyGateway instance = null;

    private String queue = null;

    public static GroovyGateway getInstance(ConfigReceiveGroovy config) {
        if (instance == null) {
            instance = new GroovyGateway(config);
        }
        return instance;
    }

    /**
     * This method is called once the initialization is done. If it gets called
     * before the initialization it will throw exception.
     *
     * @return
     */
    public static GroovyGateway getInstance() {
        if (instance == null) {
            throw new RuntimeException("Groovy Gateway is not initialized correctly..");
        } else {
            return instance;
        }
    }

    private GroovyGateway(ConfigReceiveGroovy config) {
        super(config);
    }

    @Override
    public String getLicenseCode() {
        return "GROOVY";
    }

    @Override
    protected void addAdditionalValues(Map<String, Object> arg0, Map<String, String> arg1) {
    }

    @Override
    protected String getGatewayEventType() {
        return "GROOVY";
    }

    @Override
    protected String getMessageHandlerName() {
        return MGroovy.class.getSimpleName();
    }

    @Override
    protected Class<MGroovy> getMessageHandlerClass() {
        return MGroovy.class;
    }

    public String getQueueName() {
        return queue + getOrgSuffix();
    }

    @Override
    public void start() {
        Log.log.debug("Starting Groovy Gateway");
        super.start();
    }

    @Override
    public void run() {
        if (!((ConfigReceiveGateway) configurations).isMock()) super.sendSyncRequest();
        while (running) {
            try {
                long startTime = System.currentTimeMillis();
                if (primaryDataQueue.isEmpty()) {
                    Log.log.trace("Local Primary Event Queue for " + getGatewayEventType() + " is empty.....");
                    for (Filter filter : orderedFilters) {
                        if (shouldFilterExecute(filter, startTime)) {
                            Log.log.trace("Filter " + filter.getId() + " executing");
                            List<Map<String, String>> results = invokeService(filter);
                            for (Map<String, String> result : results) {
                                addToPrimaryDataQueue(filter, result);
                            }
                        } else {
                            Log.log.trace("Filter:" + filter.getId() + " is inactive or not ready to execute yet.....");
                        }
                    }
                    if (orderedFilters == null || orderedFilters.size() == 0) {
                        Log.log.trace("There is not deployed filter for GroovyGateway");
                    }
                }
                if (orderedFilters.size() == 0) {
                    Thread.sleep(interval);
                }
            } catch (Exception e) {
                Log.log.warn("Warning: " + e.getMessage());
                Log.alert("Instance " + MainBase.main.configId.getGuid() + " " + getLicenseCode() + "-" + getQueueName() + "-" + getInstanceType() + " Polling Failed", "Instance " + MainBase.main.configId.getGuid() + " " + getLicenseCode() + "-" + getQueueName() + "-" + getInstanceType() + " Polling Failed " + "due to " + e.getMessage());
                try {
                    Thread.sleep(interval);
                } catch (InterruptedException ie) {
                    Log.log.warn("sleep interrupted: " + ie.getMessage(), ie);
                }
            }
        }
    }

    /**
     * Add customized code here to get the data as List<Map<String, String>> format for each filter
     * @return List<Map<String, String>>
     */
    public List<Map<String, String>> invokeService(Filter filter)
    {
        List<Map<String, String>> result = new ArrayList<Map<String, String>>();
        // Add customized code here to get data from 3rd party system based the
        // information in the filter;
        GroovyFilter grrovyFilter = (GroovyFilter) filter;
        String script = grrovyFilter.getGroovyScript();

        String scriptName = filter.getId()+"_GroovyScriptGateway";

        Binding binding = new Binding();

        // execute script
        try
        {
            if((script.equals("")) || ( script == null)) {
                Log.log.error("Please provide groovy script on the Groovy Gateway Filter's Groovy Script Text Area");
                throw new Exception("No groovy script for execution");

            }

            binding.setVariable(Constants.GROOVY_BINDING_LOG, Log.log);
            binding.setVariable(Constants.GROOVY_BINDING_DEBUG, debug);
            Object groovyScriptResult = GroovyScript.executeAsync(script, scriptName, true, binding, TASK_TIMEOUT);

         //   List<Map<String, String>> grovvyResult = (List<Map<String, String>>)
            if (groovyScriptResult instanceof List)
            {
                result = (List<Map<String, String>>)groovyScriptResult;
            }
            else {
                Log.log.debug("Result returned by groovy script is: " + groovyScriptResult);
                Log.log.error("Groovy script is not returning result in the form of List<Map<String, String>>)");
                throw new Exception("Please make sure groovy script returns result in the form of List<Map<String, String>>)");

            }
            Log.log.debug("Groovy Script result: " + groovyScriptResult);
           // System.out.println("result is: " + groovyScriptResult);

        }
        catch (Exception ex)
        {
            sendAlert(ALERT_SEVERITY_CRITICAL, false, "Filter:" + filter.getId()+"_GroovyScriptGateway" + ":Groovy Script Error", ex.getMessage());
            Log.log.error("Failed to execute script for the Filter: " + filter.getId(), ex);

        }
        finally
        {
            // subclasses may need some cleanup (like Netcool wants to
            // return connection to the pool.
            groovyScriptBindingCleanup(binding);
        }

        return result;
    }

    /**
     * Add customized code here to initilize the connection with 3rd party system
     * @return List<Map<String, String>>
     */
    @Override
    protected void initialize() {
        ConfigReceiveGroovy config = (ConfigReceiveGroovy) configurations;
        queue = config.getQueue().toUpperCase();
        this.gatewayConfigDir = "/config/groovy/";
        //Add customized code here to initilize the connection with 3rd party system;
    }

    /**
     * Add customized code here to do when stop the gateway
     */
    @Override
    public void stop() {
        Log.log.warn("Stopping Groovy gateway");
        //Add customized code here to stop the connection with 3rd party system;
        super.stop();
    }

    @Override
    public Filter getFilter(Map<String, Object> params) {
        return new GroovyFilter((String) params.get(BaseFilter.ID), (String) params.get(BaseFilter.ACTIVE), (String) params.get(BaseFilter.ORDER), (String) params.get(BaseFilter.INTERVAL), (String) params.get(BaseFilter.EVENT_EVENTID), (String) params.get(BaseFilter.RUNBOOK), (String) params.get(BaseFilter.SCRIPT), (String) params.get(GroovyFilter.GROOVYSCRIPT));
    }
}

