package com.resolve.services.hibernate.vo;

import com.resolve.util.MappingAnnotation;

public class GroovyFilterVO extends GatewayFilterVO {

    private static final long serialVersionUID = 1L;

    public GroovyFilterVO() {
    }

    private String UGroovyScript;

    @MappingAnnotation(columnName = "GROOVYSCRIPT")
    public String getUGroovyScript() {
        return this.UGroovyScript;
    }

    public void setUGroovyScript(String uGroovyScript) {
        this.UGroovyScript = uGroovyScript;
    }
    
    @Override
    public int hashCode()
    {
        final int prime = 31;
        int result = super.hashCode();
        result = prime * result + ((UGroovyScript == null) ? 0 : UGroovyScript.hashCode());
        return result;
    }
}

