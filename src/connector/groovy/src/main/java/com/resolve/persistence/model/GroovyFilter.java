package com.resolve.persistence.model;

import java.util.List;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Lob;
import javax.persistence.Table;
import javax.persistence.UniqueConstraint;
import com.resolve.services.interfaces.VO;
import com.resolve.services.hibernate.vo.GroovyFilterVO;

@Entity
@Table(name = "groovy_filter", uniqueConstraints = { @UniqueConstraint(columnNames = { "u_name", "u_queue" }, name = "groovyf_u_name_u_queue_uk") })
public class GroovyFilter extends GatewayFilter<GroovyFilterVO> {

    private static final long serialVersionUID = 1L;

    public GroovyFilter() {
    }

    public GroovyFilter(GroovyFilterVO vo) {
        applyVOToModel(vo);
    }

    private String UGroovyScript;

    @Lob
    @Column(name = "u_groovyscript", length = 16777215)
    public String getUGroovyScript() {
        return this.UGroovyScript;
    }

    public void setUGroovyScript(String uGroovyScript) {
        this.UGroovyScript = uGroovyScript;
    }

    public GroovyFilterVO doGetVO() {
        GroovyFilterVO vo = new GroovyFilterVO();
        super.doGetBaseVO(vo);
        vo.setUGroovyScript(getUGroovyScript());
        return vo;
    }

    @Override
    public void applyVOToModel(GroovyFilterVO vo) {
        if (vo == null) return;
        super.applyVOToModel(vo);
        if (!VO.STRING_DEFAULT.equals(vo.getUGroovyScript())) this.setUGroovyScript(vo.getUGroovyScript()); else ;
    }

    @Override
    public List<String> ugetCdataProperty() {
        List<String> list = super.ugetCdataProperty();
        list.add("UGroovyScript");
        return list;
    }
}

