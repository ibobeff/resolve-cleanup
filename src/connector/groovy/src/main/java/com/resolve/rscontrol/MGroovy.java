package com.resolve.rscontrol;

import com.resolve.persistence.model.GroovyFilter;

public class MGroovy extends MGateway {

    private static final String MODEL_NAME = GroovyFilter.class.getSimpleName();

    protected String getModelName() {
        return MODEL_NAME;
    }
}

