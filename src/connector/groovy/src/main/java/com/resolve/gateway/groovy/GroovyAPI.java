package com.resolve.gateway.groovy;

import com.resolve.gateway.AbstractGatewayAPI;

public class GroovyAPI extends AbstractGatewayAPI
{
 /*    *//**
     * Returns {@link Set} of supported object types.
     * <p>
     * Default no object types are supported returning {@link Collections.EMPTY_SET}.
     * Derived external system specific gateway API must override this method to return
     * {@link Set<String>} of supported object types.
     * 
     * @return {@link Set} of supported object types
     *//*
    public static Set<String> getObjectTypes()
    {
        return Collections.emptySet();
    }
    

    *//**
     * Creates instance of specified object type in external system.
     * <p>
     * Default attribute name-value pair returned is  
     * {@value com.resolve.gateway.GatewayConstants#METHOD_NOT_SUPPORTED_KEY}-true
     * indicating external system specific gateway does not support creating instances of
     * specified object type.
     * <p>
     * If external system specific gateway does support creation of object then it must return
     * {@link Map} of attribute name-value pairs.
     * 
     * @param  objType   Type of object to create in external system
     * @param  objParams {@link Map} of object parameters
     * @param  userName  User name
     * @param  password  Password
     * @return           Attribute id-value {@link Map} of created object in external system
     * @throws Exception If any error occurs in creating object of specified
     *                   type in external system
     *//*
    public static Map<String, String> createObject(String objType, Map<String, String> objParams, String userName, String password) throws Exception
    {
        return null;
    }
    
    *//**
     * Reads attributes of specified object in external system.
     * <p>
     * Default attribute id-value pair returned is  
     * {@value com.resolve.gateway.GatewayConstants#METHOD_NOT_SUPPORTED_KEY}-true
     * indicating external system specific gateway does not support reading atributes of
     * object.
     * <p>
     * If external system specific gateway does support reading attributes of object then 
     * it must return {@link Map} of attribute name-value pairs. 
     *
     * @param  objType    Object type
     * @param  objId      Id of the object to read attributes of from external system
     * @param  attribs    {@link List} of attributes of object to read
     * @param  userName   User name
     * @param  password   Password
     * @return            {@link Map} of object attribute id-value pairs
     * @throws Exception  If any error occurs in reading attributes of the object in 
     *                    external system
     *//*
    public static Map<String, String> readObject(String objType, String objId, List<String> attribs, String userName, String password) throws Exception
    {
        return null;
    }
    
    *//**
     * Updates attributes of specified object in external system.
     * <p>
     * Default updated attribute name-value pair returned is  
     * {@value com.resolve.gateway.GatewayConstants#METHOD_NOT_SUPPORTED_KEY}-true
     * indicating external system specific gateway does not support updating attributes of
     * object.
     * <p>
     * If external system specific gateway does support updating attributes of object then it 
     * must return updated {@link Map} of attribute name-value pairs.
     * 
     * @param  objType   Object type
     * @param  objId     Id of the object to update attributes of in external system
     * @param  updParams Key-value {@link Map} of object attributes to update
     * @param  userName  User name
     * @param  password  Password
     * @return           Key-value {@link Map} of the updated object attributes
     * @throws Exception If any error occurs in updating attributes of the object in 
     *                   external system
     *//*
    public static Map<String, String> updateObject(String objType, String objId, Map<String, String> updParams, String userName, String password) throws Exception
    {
        return null;
    }
    
    *//**
     * Deletes specified object from external system.
     * <p>
     * Default deleted attribute name-value pair returned is  
     * {@value com.resolve.gateway.GatewayConstants#METHOD_NOT_SUPPORTED_KEY}-true
     * indicating external system specific gateway does not support object deletion.
     * <p>
     * If external system specific gateway does support object deletion then it 
     * must return {@link Map} of object deletion operation reult.
     * 
     * @param  objType   Object type
     * @param  objId     Id of the object to delete from external system
     * @param  userName  User name
     * @param  password  Password
     * @return           Key-value {@link Map} result of object delete operation
     * @throws Exception If any error occurs in deleting object from external system
     *//*
    public static Map<String, String> deleteObject(String objType, String objId, String userName, String password) throws Exception
    {
        return null;
    }
    
    *//**
     * Get list of objects from external system matching specified filter condition.
     * <p>
     * Default implementation returns {@link List} containing {@link Map} with 
     * {@value com.resolve.gateway.GatewayConstants#METHOD_NOT_SUPPORTED_KEY}-true indicating
     * external system specific gateway does not support getting objects matching filter condition.
     * <p>
     * If external system specific gateway does support getting objects matching filter condition then it 
     * must return {@link List} of {@link Map} of matching objects attribute id-value pairs.
     * 
     * @param  objType  Object type
     * @param  filter   External system gateway specific filer conditions
     * @param  userName User name
     * @param  password Password
     * @return          {@link List} of {@link Map}s of object attribute id-value pairs matching
     *                  filter condition
     * @throws Exception If any error occurs in getting object from external system
     *//*
    
    public static List<Map<String, String>> getObjects(String objType, BaseFilter filter, String userName, String password) throws Exception
    {
        List<Map<String, String>> objs = new ArrayList<Map<String, String>>();
        
        objs.add(null);
        
        return objs;
    }*/
}


