package com.resolve.gateway;

import java.util.Map;
import com.resolve.gateway.moogsoft.MoogSoftFilter;
import com.resolve.gateway.moogsoft.MoogSoftGateway;

public class MMoogSoft extends MGateway {

    private static final String RSCONTROL_SET_FILTERS = "MMoogSoft.setFilters";

    private static final MoogSoftGateway instance = MoogSoftGateway.getInstance();

    public MMoogSoft() {
        super.instance = instance;
    }

    protected String getSetFilterMethodName() {
        return RSCONTROL_SET_FILTERS;
    }

    /**
     * Populates the {@link Filter} data into the given filterMap {@link Map} instance
     * 
     * @param filterMap
     *      the {@link Map} instance to hold the {@link Filter} data
     * @param filter
     *      a {@link MoogSoftFilter} instance
     *            
     */
    public void populateGatewaySpecificFilterProperties(Map<String, String> filterMap, Filter filter) {
        if (instance.isActive()) {
            MoogSoftFilter moogsoftFilter = (MoogSoftFilter) filter;
            filterMap.put(MoogSoftFilter.PORT, "" + moogsoftFilter.getPort());
            filterMap.put(MoogSoftFilter.BLOCKING, moogsoftFilter.getBlocking());
            filterMap.put(MoogSoftFilter.URI, moogsoftFilter.getUri());
            filterMap.put(MoogSoftFilter.SSL, "" + moogsoftFilter.getSsl());
            filterMap.put(MoogSoftFilter.LOCALE , "" + moogsoftFilter.getLocale());
            filterMap.put(MoogSoftFilter.TIMEZONE , "" + moogsoftFilter.getTimeZone());
            filterMap.put(MoogSoftFilter.FORMAT, "" + moogsoftFilter.getFormat());
        }
    }
}

