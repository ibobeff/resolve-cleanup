package com.resolve.gateway.moogsoft;

import java.io.File;
import java.util.HashMap;
import java.util.Map;
import org.apache.commons.io.FileUtils;

import com.resolve.rsbase.MainBase;
import com.resolve.gateway.Filter;
import com.resolve.rsremote.ConfigReceiveGateway;
import com.resolve.util.Log;
import com.resolve.util.ObjectProperties;
import com.resolve.util.StringUtils;
import com.resolve.util.XDoc;

public class ConfigReceiveMoogSoft extends ConfigReceiveGateway {

    private static final long serialVersionUID = 1L;

    private static final String RECEIVE_MOOGSOFT_NODE = "./RECEIVE/MOOGSOFT/";

    private static final String RECEIVE_MOOGSOFT_FILTER = RECEIVE_MOOGSOFT_NODE + "FILTER";

    private static final String RECEIVE_MOOGSOFT_ATTR_HTTP_SSLPASSWORD = RECEIVE_MOOGSOFT_NODE + "@HTTP_SSLPASSWORD";

    private static final String RECEIVE_MOOGSOFT_ATTR_EXECUTE_TIMEOUT = RECEIVE_MOOGSOFT_NODE + "@EXECUTE_TIMEOUT";

    private static final String RECEIVE_MOOGSOFT_ATTR_HTTP_PORT = RECEIVE_MOOGSOFT_NODE + "@HTTP_PORT";

    private static final String RECEIVE_MOOGSOFT_ATTR_HTTP_SSL = RECEIVE_MOOGSOFT_NODE + "@HTTP_SSL";

    private static final String RECEIVE_MOOGSOFT_ATTR_HTTP_USERNAME = RECEIVE_MOOGSOFT_NODE + "@HTTP_USERNAME";

    private static final String RECEIVE_MOOGSOFT_ATTR_WAIT_TIMEOUT = RECEIVE_MOOGSOFT_NODE + "@WAIT_TIMEOUT";

    private static final String RECEIVE_MOOGSOFT_ATTR_PASSWORD = RECEIVE_MOOGSOFT_NODE + "@PASSWORD";

    private static final String RECEIVE_MOOGSOFT_ATTR_PORT = RECEIVE_MOOGSOFT_NODE + "@PORT";

    private static final String RECEIVE_MOOGSOFT_ATTR_SCRIPT_TIMEOUT = RECEIVE_MOOGSOFT_NODE + "@SCRIPT_TIMEOUT";

    private static final String RECEIVE_MOOGSOFT_ATTR_HTTP_SSLCERTIFICATE = RECEIVE_MOOGSOFT_NODE + "@HTTP_SSLCERTIFICATE";

    private static final String RECEIVE_MOOGSOFT_ATTR_HOST = RECEIVE_MOOGSOFT_NODE + "@HOST";

    private static final String RECEIVE_MOOGSOFT_ATTR_HTTP_PASSWORD = RECEIVE_MOOGSOFT_NODE + "@HTTP_PASSWORD";

    private static final String RECEIVE_MOOGSOFT_ATTR_USERNAME = RECEIVE_MOOGSOFT_NODE + "@USERNAME";
    
    static final Integer DEFAULT_HTTP_PORT = 7780;
    
    static final Integer DEFAULT_WAIT_TIMEOUT = 120;
    
    static final Integer DEFAULT_EXECUTE_TIMEOUT = 120;
    
    static final Boolean DEFAULT_HTTP_SSL = false;
    
    private String queue = "MOOGSOFT";

    private String http_sslpassword = "";

    private String execute_timeout = "";

    private String http_port = "";

    private String http_ssl = "";

    private String http_username = "";

    private String wait_timeout = "";

    private String password = "";

    private String port = "";

    private String script_timeout = "120";

    private String http_sslcertificate = "";

    private String host = "";

    private String http_password = "";

    private String username = "";

    public String getQueue()
    {
        return queue;
    }

    public void setQueue(String queue)
    {
        this.queue = queue;
    }

    public String getHttp_sslpassword()
    {
        return http_sslpassword;
    }

    public void setHttp_sslpassword(String http_sslpassword)
    {
        this.http_sslpassword = http_sslpassword;
    }

    public String getExecute_timeout()
    {
        return execute_timeout;
    }

    public void setExecute_timeout(String execute_timeout)
    {
        this.execute_timeout = execute_timeout;
    }

    public String getHttp_port()
    {
        return http_port;
    }
    
    public Integer getHttpPort()
    {
        Integer httpPort = DEFAULT_HTTP_PORT;
        
        try {
            httpPort = new Integer(http_port);
        } catch(Exception e) {
            Log.log.error(e.getMessage(),e);
        }
        
        return httpPort;
    }
    
    public void setHttp_port(String http_port)
    {
        this.http_port = http_port;
    }

    public String getHttp_ssl()
    {
        return http_ssl;
    }
    
    public Boolean getHttpSsl()
    {
        Boolean httpSSL = DEFAULT_HTTP_SSL;
        
        try {
            httpSSL = new Boolean(http_ssl);
        } catch(Exception e) {
            Log.log.error(e.getMessage(),e);
        }
        
        return httpSSL;
    }

    public void setHttp_ssl(String http_ssl)
    {
        this.http_ssl = http_ssl;
    }

    public String getHttp_username()
    {
        return http_username;
    }

    public void setHttp_username(String http_username)
    {
        this.http_username = http_username;
    }

    public String getWait_timeout()
    {
        return wait_timeout;
    }

    public void setWait_timeout(String wait_timeout)
    {
        this.wait_timeout = wait_timeout;
    }

    public String getPassword()
    {
        return password;
    }

    public void setPassword(String password)
    {
        this.password = password;
    }

    public String getPort()
    {
        return port;
    }

    public void setPort(String port)
    {
        this.port = port;
    }

    public String getScript_timeout()
    {
        return script_timeout;
    }

    public void setScript_timeout(String script_timeout)
    {
        this.script_timeout = script_timeout;
    }

    public String getHttp_sslcertificate()
    {
        return http_sslcertificate;
    }

    public void setHttp_sslcertificate(String http_sslcertificate)
    {
        this.http_sslcertificate = http_sslcertificate;
    }

    public String getHost()
    {
        return host;
    }

    public void setHost(String host)
    {
        this.host = host;
    }

    public String getHttp_password()
    {
        return http_password;
    }

    public void setHttp_password(String http_password)
    {
        this.http_password = http_password;
    }

    public String getUsername()
    {
        return username;
    }

    public void setUsername(String username)
    {
        this.username = username;
    }

    public ConfigReceiveMoogSoft(XDoc config) throws Exception {
        super(config);
        define("http_sslpassword", SECURE, RECEIVE_MOOGSOFT_ATTR_HTTP_SSLPASSWORD);
        define("execute_timeout", STRING, RECEIVE_MOOGSOFT_ATTR_EXECUTE_TIMEOUT);
        define("http_port", STRING, RECEIVE_MOOGSOFT_ATTR_HTTP_PORT);
        define("http_ssl", STRING, RECEIVE_MOOGSOFT_ATTR_HTTP_SSL);
        define("http_username", STRING, RECEIVE_MOOGSOFT_ATTR_HTTP_USERNAME);
        define("wait_timeout", STRING, RECEIVE_MOOGSOFT_ATTR_WAIT_TIMEOUT);
        define("password", SECURE, RECEIVE_MOOGSOFT_ATTR_PASSWORD);
        define("port", STRING, RECEIVE_MOOGSOFT_ATTR_PORT);
        define("script_timeout", STRING, RECEIVE_MOOGSOFT_ATTR_SCRIPT_TIMEOUT);
        define("http_sslcertificate", STRING, RECEIVE_MOOGSOFT_ATTR_HTTP_SSLCERTIFICATE);
        define("host", STRING, RECEIVE_MOOGSOFT_ATTR_HOST);
        define("http_password", SECURE, RECEIVE_MOOGSOFT_ATTR_HTTP_PASSWORD);
        define("username", STRING, RECEIVE_MOOGSOFT_ATTR_USERNAME);
    }

    @Override
    public String getRootNode() {
        return RECEIVE_MOOGSOFT_NODE;
    }

    @Override
    public void load() throws Exception {
        try {
            loadAttributes();
            if (isActive()) {
                MoogSoftGateway moogsoftGateway = MoogSoftGateway.getInstance(this);
                filters = xdoc.getListMapValue(RECEIVE_MOOGSOFT_FILTER);
                if (filters.size() > 0) {
                    for (Map<String, Object> values : filters) {
                        String filterName = ((String) values.get(MoogSoftFilter.ID)).toLowerCase();
                        File scriptFile = getFile(MainBase.main.release.serviceName + "/config/moogsoft/" + filterName + ".groovy");
                        try {
                            if (scriptFile.exists()) {
                                String groovy = FileUtils.readFileToString(scriptFile, "UTF-8");
                                if (StringUtils.isNotBlank(groovy)) {
                                    values.put(MoogSoftFilter.SCRIPT, groovy);
                                    Log.log.debug("Loaded groovy script: " + scriptFile);
                                }
                            }
                            moogsoftGateway.setFilter(values);
                        } catch (Exception e) {
                            Log.log.warn(e.getMessage() + ". Skipping file: " + scriptFile.getAbsolutePath());
                        }
                    }
                }
            }
        } catch (Throwable e) {
            setActive(false);
            Log.log.warn("Couldn not load configuration for MoogSoft gateway, check blueprint. " + e.getMessage(), e);
        }
    }

    @Override
    public void save() throws Exception {
        File dir = getFile(MainBase.main.release.serviceName + "/config/moogsoft");
        if (!dir.exists()) {
            FileUtils.forceMkdir(dir);
        }
        if (filters != null && isActive()) {
            filters.clear();
            for (Filter filter : MoogSoftGateway.getInstance().getFilters().values()) {
                MoogSoftFilter moogsoftFilter = (MoogSoftFilter) filter;
                String groovy = moogsoftFilter.getScript();
                String scriptFileName;
                File scriptFile = null;
                if (StringUtils.isNotBlank(groovy)) {
                    scriptFileName = moogsoftFilter.getId().toLowerCase() + ".groovy";
                    scriptFile = getFile(MainBase.main.release.serviceName + "/config/moogsoft/" + scriptFileName);
                }
                Map<String, Object> entry = new HashMap<String, Object>();
                putFilterDataIntoEntry(entry, moogsoftFilter);
                filters.add(entry);
                if (scriptFile != null) {
                    try {
                        FileUtils.writeStringToFile(scriptFile, groovy, "UTF-8");
                    } catch (Exception e) {
                        Log.log.warn(e.getMessage() + ". Skipping file: " + scriptFile.getAbsolutePath());
                    }
                }
            }
            xdoc.setListMapValue(RECEIVE_MOOGSOFT_FILTER, filters);
        }
        if (nameProperties != null && isActive()) {
            nameProperties.clear();
            for (String name : MoogSoftGateway.getInstance().getNameProperties().keySet()) {
                Map<String, Object> entry = new HashMap<String, Object>();
                entry.put(NAME_KEY, name);
                nameProperties.add(entry);
                Map props = MoogSoftGateway.getInstance().getNameProperties().getNameProperties(name);
                ObjectProperties prop = new ObjectProperties(props);
                prop.save(props);
            }
        }
        saveAttributes();
    }

    private void putFilterDataIntoEntry(Map<String, Object> entry, MoogSoftFilter moogsoftFilter) {
        entry.put(MoogSoftFilter.ID, moogsoftFilter.getId());
        entry.put(MoogSoftFilter.ACTIVE, String.valueOf(moogsoftFilter.isActive()));
        entry.put(MoogSoftFilter.ORDER, String.valueOf(moogsoftFilter.getOrder()));
        entry.put(MoogSoftFilter.INTERVAL, String.valueOf(moogsoftFilter.getInterval()));
        entry.put(MoogSoftFilter.EVENT_EVENTID, moogsoftFilter.getEventEventId());
        entry.put(MoogSoftFilter.RUNBOOK, moogsoftFilter.getRunbook());
        entry.put(MoogSoftFilter.SCRIPT, moogsoftFilter.getScript());
        entry.put(MoogSoftFilter.PORT, moogsoftFilter.getPort());
        entry.put(MoogSoftFilter.BLOCKING, moogsoftFilter.getBlocking());
        entry.put(MoogSoftFilter.URI, moogsoftFilter.getUri());
        entry.put(MoogSoftFilter.SSL, moogsoftFilter.getSsl());
    }
}

