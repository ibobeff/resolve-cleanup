package com.resolve.gateway.moogsoft;

import java.net.URLEncoder;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.TimeZone;

import org.apache.commons.collections.CollectionUtils;

import com.resolve.gateway.AbstractGatewayAPI;
import com.resolve.util.Log;
import com.resolve.util.StringUtils;
import com.resolve.util.restclient.RestCaller;

import net.sf.json.JSONArray;
import net.sf.json.JSONObject;
import net.sf.json.JSONSerializer;

/**
 * 
 * 
 * A {@code MoogSoftAPI} is a singleton client for Moogsoft Graze API endpoints. Use this class in
 * an Action Task or Groovy Script to request and post data to Moogsoft AI/Ops system configured in
 * the Blueprint <br>
 * <br>
 * {@code MoogSoftAPI} is required to be initialized with Moogsoft's hostname, port, username and
 * password. The class is initialized with values taken from corresponding Moogsoft Gateway
 * blueprint properties when RSRemote is started. <br>
 * <br>
 * Resolve developers do not need to initialize the class in the Action Task. The methods can be
 * statically accessed using the class name. <br>
 * <br>
 * The username and password configured in the blueprint is included in every HTTP request's header
 * for authentication <br>
 * <br>
 * The
 * {@code updateAlert(Integer, String, String, String), updateSituation(Integer, String, String, String)}methods
 * update custom info on Moogsoft Alert and Situation objects
 * <h3>Groovy Samples</h3>
 * <h4>1. Get Alert Details</h4>
 * <pre>
 * import com.resolve.gateway.moogsoft.MoogSoftAPI;
 * 
 * def alert_id = 4
 * def response = MoogSoftAPI.getAlertDetails(alert_id);  
 * </pre>
 * <h4>2. Update Alert Details</h4>
 * <pre>
 * import com.resolve.gateway.moogsoft.MoogSoftAPI;
 * 
 * def alert_id = 4
 * def status = "Resolved"
 * def summary = "Worksheet Summary"
 * def resolve_hostname = "test.resolvesys.com"
 * def resolve_port     = "8443"
 * def protocol         = "https"
 * def results = protocol + "://" + resolve_hostname +":"+resolve_port+"/"+ MoogSoftAPI.getResolveUri() + PARAMS["PROBLEMID"]
 * 
 * def response = MoogSoftAPI.updateAlert(alert_id,status,results,summary)
 * 
 * return response
 * </pre>
 * <h4>3. Update Alert using JSONObject</h4>
 * <pre>
 * import com.resolve.gateway.moogsoft.MoogSoftAPI;
 * import com.resolve.util.StringUtils;
 * 
 * def situation_id = 92
 * def paramJson = """{ "field1" : "value1" , "field2" : "value2" , "field3" : ["item1","item2","item3"]}"""
 * def inputJson = StringUtils.stringToJSONObject(paramJson);
 * 
 * def response = MoogSoftAPI.updateSituation(situation_id,inputJson)
 * 
 * return response
 * </pre>
 */

public class MoogSoftAPI extends AbstractGatewayAPI
{
    private static String HOSTNAME = "";
    private static String PORT = "";
    private static String URI = "/graze/v1/";
    private static String resolveURI = "resolve/jsp/rsclient.jsp?IFRAMEID=moogsoft#RS.worksheet.Worksheet/id=";
    
    private static String user = "";
    private static String pass = "";
    private static Locale LOCALE = Locale.US;
    private static String TIMEZONE = "GMT";
    private static String DATEFORMAT = "yyyy-MM-dd'T'HH:mm:ss'Z'";
    
    private static String baseUrl = null;
    private static RestCaller client;
    
    private static MoogSoftGateway instance = MoogSoftGateway.getInstance();

    static {
        try {
            Map<String, Object> properties = instance.loadSystemProperties();
            
            HOSTNAME = (String)properties.get("HOST");
            PORT = (String)properties.get("PORT");
            user = (String)properties.get("USERNAME");
            pass = (String)properties.get("PASSWORD");
            
            StringBuffer sb = new StringBuffer();
            sb.append("https://").append(HOSTNAME);
            if(StringUtils.isNotBlank(PORT))
                sb.append(":").append(PORT);
            sb.append(URI);
            
            baseUrl = sb.toString();
            client = new RestCaller(baseUrl, user, pass, true, true, false, true);
        } catch(Exception e) {
          Log.log.error(e.getMessage(), e);
        }
    }
    
    /**
     * Returns a constant Resolve worksheet URI used to update Moogsoft with Worksheet information
     * @return returns constant string <i>resolve/jsp/rsclient.jsp?IFRAMEID=moogsoft#RS.worksheet.Worksheet/id=</i>
     */
    public static String getResolveUri()
    {
        return resolveURI;
    }
    
    /**
     * A GET request that returns details (such as Description, Severity, etc.) of a specified alert.
     * @param alertId: The Alert ID
     * @return Successful requests return a JSON formatted String object which contains alert details
     * @throws Exception When alertId is null or if there are HTTP connection errors when connecting to Moogsoft
     */
    public static String getAlertDetails(Integer alertId) throws Exception {
        
        if(alertId == null)
            throw new Exception("alertId is not available.");
        
        String details = null;
        String uri = "getAlertDetails?alert_id=" + alertId;
        
        try {
            if (Log.log.isDebugEnabled()) {
                Log.log.debug(String.format("MoogSoftGateway:'%s'", uri));
            }
            details = client.callGet(uri, null, null);
            checkRespForError(details);
        } catch(Exception e) {
            Log.log.error(e.getMessage(), e);
            throw e;
        }
        
        return details;
    }
    
    /**
     * A GET request that returns details (such as Description, Status, etc.) of a specified Situation.
     * @param sitnId: The Situation ID
     * @return Successful requests return a JSON formatted String object which contains Situation details
     * @throws Exception When sitnId is null or if there are HTTP connection errors when connecting to Moogsoft 
     */
    public static String getSituationDetails(Integer sitnId) throws Exception {
        
        if(sitnId == null)
            throw new Exception("sitnId is not available.");
        
        String details = null;

        String uri = "getSituationDetails?sitn_id=" + sitnId;
        
        try {
            if (Log.log.isDebugEnabled()) {
                Log.log.debug(String.format("MoogSoftGateway:'%s'", uri));
            }
            details = client.callGet(uri, null, null);
            checkRespForError(details);
        } catch(Exception e) {
            Log.log.error(e.getMessage(), e);
            throw e;
        }
        
        return details;
    }
    
    /**
     * A GET request that returns the total number of Alerts, and a list of their Alert IDs for a specified Situation. 
     * This can be either all Alerts or just those Alerts unique to the Situation.
     * @param sitnId: required - The Situation ID
     * @param forUniqueAlerts: required - Indicates the Alerts to get from the Situation: 
     *                                    true = get only Alerts unique to the Situation, 
     *                                    false = get all Alerts in the Situation (default)
     * @return : A list of alert ids associated with a situation.
     * @throws Exception When sitnId is null or if there are HTTP connection errors when connecting to Moogsoft
     */
    public static List<String> getSituationAlertIds(Integer sitnId, Boolean forUniqueAlerts) throws Exception {
        
        if(sitnId == null)
            throw new Exception("sitnId is not available.");
        
        List<String> alerts = new ArrayList<String>();
        String alertList = null;
        
        try {
            StringBuilder sb = new StringBuilder();
            sb.append("getSituationAlertIds?sitn_id=").append(sitnId).append("&for_unique_alerts=").append(forUniqueAlerts);
            if (Log.log.isDebugEnabled()) {
                Log.log.debug(String.format("MoogSoftGateway:'%s'", sb.toString()));
            }
            alertList = client.callGet(sb.toString(), null, null);
            checkRespForError(alertList);
        } catch(Exception e) {
            Log.log.error(e.getMessage(), e);
            throw e;
        }

        if(StringUtils.isBlank(alertList))
            return null;
        try {
            JSONObject json = JSONObject.fromObject(alertList);
            Object alertIds = json.get("alert_ids");
            JSONArray alertIdArray = (JSONArray) JSONSerializer.toJSON(alertIds);
            
            for(int i=0; i<alertIdArray.size(); i++) {
                String alertId = alertIdArray.getString(i);
                Log.log.debug(alertId);
                alerts.add(alertId);
            }
        } catch(Exception e) {
            Log.log.error(e.getMessage(), e);
            throw e;
        }
        
        return alerts;
    }
    
  /**
   * A POST request that adds and merges custom information(actionStatus,actionResults,actionSummary
   * under custom info) for a specified Alert
   * 
   * @param alertId The Alert ID
   * @param status The actionStatus under custom info
   * @param results The actionResults under custom info 
   * @param summary: The actionSummary under custom info
   * @return : null if request is successful. A JSON formatted string, with status code and a message,
   *         if MoogSoft could not process the request
   * @throws Exception When alertId is null or if there are HTTP connection errors when connecting to Moogsoft
   */
    @SuppressWarnings("deprecation")
    public static String updateAlert(Integer alertId, String status, String results, String summary) throws Exception {

        if(alertId == null)
            throw new Exception("alertId is not available.");
        String result = null;
        JSONObject jsonBody = new JSONObject();
        JSONObject jsonResolve = new JSONObject();
        JSONObject json = new JSONObject();
        
        json.put("actionStatus", status);
        json.put("actionResults", results);
        json.put("actionSummary", summary);
        
        jsonResolve.put("Resolve", json);
        
        jsonBody.accumulate("custom_info", jsonResolve);
        jsonBody.accumulate("alert_id", alertId);
        
        try {
            if (Log.log.isDebugEnabled()) {
                Log.log.debug(String.format("MoogSoftGateway:addAlertCustomInfo:body '%s'", json.toString()));
            }
            String response = client.callPost("addAlertCustomInfo", null, jsonBody.toString(), null, "application/json", null);
            result = checkRespForError(response);
        } catch(Exception e) {
            Log.log.error(e.getMessage(), e);
            throw e;
        }
        return result;
    }
    
  /**
   * A POST request that creates a manual Situation
   * 
   * @param description The new Situation's description. If description is null, it is converted to an empty string
   * @return The new Situation's ID, if request is successful
   * @throws Exception When there are errors making an HTTP Request to Moogsoft or when HTTP
   *         response from Moogsoft does not contain the new situation ID, the response JSON from
   *         Moogsoft is included in the exception message
   */
    @SuppressWarnings("deprecation")
    public static Integer createSituation(String description) throws Exception {
        
        if(description == null)
            description = "";
        
        Integer sitnId = null;
        
        String result = "";
        
        JSONObject json = new JSONObject();

        json.put("description", description);
        
        try {
            result = client.callPost("createSituation", null, json.toString(), null, "application/json", null);
            Log.log.debug(result);
        } catch(Exception e) {
            Log.log.error(e.getMessage(), e);
            throw e;
        }
        
        try {
            if(StringUtils.isNotBlank(result)) {
                JSONObject jsonResult = JSONObject.fromObject(result);
                sitnId = (Integer)jsonResult.get("sitn_id");
                if(sitnId == null)
                    throw new Exception(jsonResult.toString());
            }
        } catch(Exception e) {
            Log.log.error(e.getMessage(), e);
            throw e;
        }
        
        return sitnId;
    }
    
    /**
     * A POST request that adds a specified Alert to the specified Situation.
     * 
     * @param alertId   The Alert ID
     * @param sitnId    The Situation ID
     * @return  null if successful. A JSON string, with status code and a message, will be returned if MoogSoft could not process the request for any reason.
     * @throws Exception When alertId or sitnId is null or if there are HTTP connection errors when connecting to Moogsoft
     */
    @SuppressWarnings("deprecation")
    public static String addAlertToSituation(Integer alertId, Integer sitnId) throws Exception {

        if(alertId == null)
            throw new Exception("alertId is not available.");
        
        if(sitnId == null)
            throw new Exception("sitnId is not available.");
        String result = null;
        JSONObject json = new JSONObject();

        json.put("alert_id", alertId);
        json.put("sitn_id", sitnId);
        
        try {
            if (Log.log.isDebugEnabled()) {
                Log.log.debug(String.format("MoogSoftGateway:addAlertToSituation:body '%s'", json.toString()));
            }
            String response = client.callPost("addAlertToSituation", null, json.toString(), null, "application/json", null);
            result = checkRespForError(response);
        } catch(Exception e) {
            Log.log.error(e.getMessage(), e);
            throw e;
        }
        return result;
    }
    
    /**
     * Two POST requests to create a new thread and add a thread entry simultaneously
     *  
     * @param sitnId        The Situation ID
     * @param threadName    The new thread's name 
     * @param threadEntry:  The first entry in the new thread
     * @return : null if successful. A JSON string, with status code and a message, will be returned if MoogSoft could not process the request for any reason.
     * @throws Exception When sitnId is null or threadName is blank or If there are HTTP connection errors when connecting to Moogsoft
     */
    public static String addSituationThread(Integer sitnId, String threadName, String threadEntry) throws Exception {
        
        if(sitnId == null)
            throw new Exception("sitnId is not available.");
        
        String response = createThread(sitnId, threadName);
        if (StringUtils.isNotBlank(response)) {
            return response;
        }
        
        return addThreadEntry(sitnId, threadName, threadEntry);
    }
    
  /**
   * A POST request that creates a new thread for a specified Situation.Threads are comments or
   * 'story activity' on Situations.
   * 
   * @param sitnId The Situation ID
   * @param threadName The name of the new Thread
   * @return null if successful. A JSON string, with status code and a message, will be returned if
   *         MoogSoft could not process the request for any reason. For example: If sitnId passed in
   *         was null
   * @throws Exception When threadName is blank or If there are HTTP connection errors when
   *         connecting to Moogsoft
   */
    @SuppressWarnings("deprecation")
    public static String createThread(Integer sitnId, String threadName) throws Exception {
        
        if(StringUtils.isBlank(threadName))
            throw new Exception("threadName is not available.");
        String result = null;
        JSONObject json = new JSONObject();
        //TODO: Add check for sitnId being null
        json.put("sitn_id", sitnId);
        json.put("thread_name", threadName);
        
        try {
            if (Log.log.isDebugEnabled()) {
                Log.log.debug(String.format("MoogSoftGateway:createThread:body '%s'", json.toString()));
            }
            String response = client.callPost("createThread", null, json.toString(), null, "application/json", null);
            result = checkRespForError(response);
        } catch(Exception e) {
            Log.log.error(e.getMessage(), e);
            throw e;
        }
        return result;
    }
    
  /**
   * A POST request that adds a new entry to an existing thread in a Situation. Threads are comments
   * or 'story activity' on Situations.
   * 
   * @param sitnId The Situation ID
   * @param threadName The name of the existing thread
   * @param threadEntry The entry to add
   * @returnnull if successful. A JSON string, with status code and a message, will be returned
   *         if MoogSoft could not process the request for any reason.
   * @throws Exception When threadName is blank or If there are HTTP connection errors when
   *         connecting to Moogsoft
   */
    @SuppressWarnings("deprecation")
    public static String addThreadEntry(Integer sitnId, String threadName, String threadEntry) throws Exception {
        
        if(StringUtils.isBlank(threadName))
            throw new Exception("threadName is not available.");
        String result = null;
        JSONObject json = new JSONObject();

        json.put("sitn_id", sitnId);
        json.put("thread_name", threadName);
        json.put("entry", threadEntry);

        try {
            if (Log.log.isDebugEnabled()) {
                Log.log.debug(String.format("MoogSoftGateway:addThreadEntry:body '%s'", json.toString()));
            }
            String response = client.callPost("addThreadEntry", null, json.toString(), null, "application/json", null);
            result = checkRespForError(response);
        } catch(Exception e) {
            Log.log.error(e.getMessage(), e);
            throw e;
        }
        return result;
    }
    
  /**
   * A POST request that adds and merges custom information for a specified Situation.
   * 
   * @param sitnId The Situation ID
   * @param status The actionStatus under custom info
   * @param results The actionResults under custom info
   * @param summary The actionSummary under custom info
   * @return null if successful. A JSON string, with status code and a message, will be returned if
   *         MoogSoft could not process the request for any reason.
   * @throws Exception When sitnId is null or If there are HTTP connection errors when connecting to
   *         Moogsoft
   */
    @SuppressWarnings("deprecation")
    public static String updateSituation(Integer sitnId, String status, String results, String summary) throws Exception {

        if(sitnId == null)
            throw new Exception("sitnId is not available.");
        String result = null;
        JSONObject jsonBody = new JSONObject();
        JSONObject jsonResolve = new JSONObject();
        JSONObject json = new JSONObject();

        json.put("actionStatus", status);
        json.put("actionResults", results);
        json.put("actionSummary", summary);
        
        jsonResolve.put("Resolve", json);
        
        jsonBody.accumulate("custom_info", jsonResolve);
        jsonBody.accumulate("sitn_id", new Integer(sitnId));

        try {
            if (Log.log.isDebugEnabled()) {
                Log.log.debug(String.format("MoogSoftGateway:addSituationCustomInfo:body '%s'", json.toString()));
            }
            String response = client.callPost("addSituationCustomInfo", null, jsonBody.toString(), null, "application/json", null);
            result = checkRespForError(response);
        } catch(Exception e) {
            Log.log.error(e.getMessage(), e);
            throw e;
        }
        return result;
    }
    
  /**
   * A POST request that adds and merges custom information for a specified Situation.
   * 
   * @param sitnId The Situation ID
   * @param paramJson net.sf.json.JSONObject representing custom info JSON
   * @return null if successful. A JSON formatted string, with status code and a message, will be
   *         returned if MoogSoft could not process the request for any reason.
   * @throws Exception When sitnId is null or If there are HTTP connection errors when connecting to
   *         Moogsoft or paramJson is an invalid net.sf.JSONObject
   */
    @SuppressWarnings("deprecation")
    public static String updateSituation(Integer sitnId, JSONObject paramJson) throws Exception
    {

        if (sitnId == null) 
            throw new Exception("sitnId is not available.");

        if (paramJson == null || !(paramJson instanceof JSONObject))
        {
            Log.log.error("Failed to update situation. Please pass valid JSON Object");
            throw new Exception("JSON Object passed to update situation is invalid.");

        }
        String result = null;
        JSONObject jsonBody = new JSONObject();
        jsonBody.accumulate("custom_info", paramJson);
        jsonBody.accumulate("sitn_id", new Integer(sitnId));

        try
        {
            if (jsonBody instanceof JSONObject)
            {
                if (Log.log.isDebugEnabled()) {
                    Log.log.debug(String.format("MoogSoftGateway:addSituationCustomInfo:body '%s'", jsonBody.toString()));
                }
                String response = client.callPost("addSituationCustomInfo", null, jsonBody.toString(), null, "application/json", null);
                result = checkRespForError(response);
            }
            else
            {
                Log.log.error("Error processing update situation Api call.");
                throw new Exception("Error processing update situation Api call.");
            }
        }
        catch (Exception e) {
            Log.log.error(e.getMessage(), e);
            throw e;
        }
        return result;
    }
    
    
  /**
   * Update all Situations that an Alert ID is associated with
   * 
   * @param alertId The Alert ID
   * @param status The actionStatus under custom info
   * @param results The actionResults under custom info
   * @param summary The actionSummary under custom info
   * @return : null if successful. A JSON string, with status code and a message, will be returned
   *         if MoogSoft could not process the request for any reason.
   * @throws Exception When alertId is null or if alertId does not exist in Moogsoft or when there
   *         are errors making an HTTP connection to Moogsoft
   */
    public static String updateAllSituations(Integer alertId, String status, String results, String summary) throws Exception {
        
        if(alertId == null)
            throw new Exception("alertId is not available.");
        
        String details = getAlertDetails(alertId);
        
        if(StringUtils.isBlank(details))
            throw new Exception("No alert details available.");
        String response = null;
        try {
            JSONObject jsonDetails = JSONObject.fromObject(details);
            
            JSONArray jsonArray = jsonDetails.getJSONArray("active_sitn_list");

            for(int i=0; i<jsonArray.size(); i++) {
                Integer sitnId = jsonArray.getInt(i);
                response = updateSituation(sitnId, status, results, summary);
            }
        } catch(Exception e) {
            Log.log.error(e.getMessage(), e);
            throw e;
        }
        return response;
    }
    
  /**
   * A POST request that closes a specified Situation that is currently open, and optionally closes
   * alerts in the Situation.
   * 
   * @param sitnId The Situation ID
   * @param resolution Determines what to do with the Situation's Alerts
   * <br><br>
   * <table border="1" style="margin-left: 40px"><tbody>
   *   <tr><th>&nbsp;value&nbsp;</th><th>effect&nbsp;</th>
   *   <tr><td><p>&nbsp;0&nbsp;</p></td><td><p>&nbsp;Close no alerts.</p></td></tr>
   *   <tr><td><p>&nbsp;1&nbsp;</p></td><td ><p>&nbsp;Close all alerts in this Situation.</p></td></tr>
   *   <tr><td ><p>&nbsp;2&nbsp;</p></td><td><p>&nbsp;Close only alerts unique to this Situation.</p></td></tr>
   *   </tbody></table>
   * @return null if successful. A JSON string, with status code and a message, will be returned
   *         if MoogSoft could not process the request for any reason.
   * @throws Exception When sitnId is null or when there is an error making an HTTP request to
   *         Moogsoft
   */
    @SuppressWarnings("deprecation")
    public static String closeSituation(Integer sitnId, Integer resolution) throws Exception {
        
        if(sitnId == null)
            throw new Exception("sitnId is not available.");
        
        if(resolution == null)
            resolution = 1;
        String result = null;
        JSONObject json = new JSONObject();

        json.put("sitn_id", sitnId);
        json.put("resolution", resolution);

        try {
            if (Log.log.isDebugEnabled()) {
                Log.log.debug(String.format("MoogSoftGateway:closeSituation:body '%s'", json.toString()));
            }
            String response = client.callPost("closeSituation", null, json.toString(), null, "application/json", null);
            result = checkRespForError(response);
        } catch(Exception e) {
            Log.log.error(e.getMessage(), e);
            throw e;
        }
        return result;
    }
    
  /**
   * This call sets the user provided timestamp format for the API calls of maintenance mode. This
   * method can be only be used when all the AT callers use the same set of timestamp across all the
   * RSRemote instance.
   * 
   * @param format: The custom format of the timestamp, e.g. 'yyyy-MM-dd'T'HH:mm:ss'Z''
   * @param locale: Use java.util.Locale class to provide a valid locale string, e.g.
   *        Locale.US.toString()
   * @param timeZone: The valid time zone string, e.g. 'PDT'
   * @return "Failed:Local not found" if local is not found, "success" if successful 
   */
    public static String setCustomTimestamp(String format, String locale, String timeZone) {
        
        String status = "Failed: Locale not found";
        
        if(StringUtils.isBlank(format) || StringUtils.isBlank(locale) || StringUtils.isBlank(timeZone))
            return "Failed: Parameters cannot be blank";
        
        DATEFORMAT = format;
        TIMEZONE = timeZone;
        
        Locale[] locales = Locale.getAvailableLocales();
        
        for(int i=0; i<locales.length; i++) {
            if(locales[i].toString().equals(locale)) {
                LOCALE = locales[i];
                status = "Success";
                break;
            }
        }
        
        return status;
    }
    
  /**
   * A POST request that creates a maintenance window. The maintenance window filters alerts caused
   * by a known period of maintenance. Use {@link #setCustomTimestamp(String, String, String)}
   * before calling this API to set format, locale and timezone of the datetime stamp passed in
   * startTime parameter. If you wanna use EPOCH time see
   * {@link #createMaintenanceWindow(String, String, String, long, long, boolean, Integer, Integer)}
   * 
   * @param name Name of the maintenance window.
   * @param description Description of the maintenance window.
   * @param filter JSON or SQL-like filter for alerts to match. The filter must be in JSON format,
   *        that is, the same format used in alert and Situation filters in the database.
   * @param startTime Date-Time stamp set by a previous call made to
   *        {@link #setCustomTimestamp(String, String, String)}
   * @param duration Duration of the maintenance window in seconds. The minimum duration is 1 second
   *        and the maximum is 157784630 seconds ( 5 years).
   * @param forwardAlerts Defines whether or not alerts should be forwarded to the next Moolet in
   *        the processing chain.
   * @param recurringPeriod Whether or not this is a recurring maintenance window. Set this to:<br>
   *        <ul>
   *        <li>1 for a recurring maintenance window.</li>
   *        <li>0 for a one-time maintenance window.</li>
   *        </ul>
   *        <br>
   *        If not specified, defaults to 0. If you set this property to 1, you must specify
   *        recurring_period_units.
   * @param recurringPeriodUnits Specifies the recurring period of the maintenance window, in days,
   *        weeks or months. Defaults to 0 if recurring_period is set to 0 . V alid values are:
   *        <ul>
   *        <li>2 = daily</li>
   *        <li>3 = weekly</li>
   *        <li>4 = monthly</li>
   *        </ul>
   * @return The new maintenance window ID.A JSON string, with status code and a message, will be
   *         returned if MoogSoft could not process the request for any reason.
   * @throws Exception When startTime or name is null or if there is error making an HTTP request to
   *         Moogsoft
   */
    
    public static String createMaintenanceWindow(String name, String description, String filter, String startTime, long duration, boolean forwardAlerts, Integer recurringPeriod, Integer recurringPeriodUnits) throws Exception {

        if(startTime == null)
            throw new Exception("startTime is not available.");
        String response = null;
        try {
            SimpleDateFormat sdf = new SimpleDateFormat(DATEFORMAT, LOCALE);
            sdf.setTimeZone(TimeZone.getTimeZone(TIMEZONE));           
            long time = sdf.parse(startTime).getTime();
            if(time >= 0) {
                time = time/1000;
            }
            response = createMaintenanceWindow(name, description, filter, time, duration, forwardAlerts, recurringPeriod, recurringPeriodUnits);
        } catch(Exception e) {
            Log.log.error(e.getMessage(), e);
            throw e;
        }
        return response;
    }
    
  /**
   * A POST request that creates a maintenance window. The maintenance window filters alerts caused
   * by a known period of maintenance.
   * 
   * @param name Name of the maintenance window.
   * @param description Description of the maintenance window.
   * @param filter JSON or SQL-like filter for alerts to match. The filter must be in JSON format,
   *        that is, the same format used in alert and Situation filters in the database.
   * @param startTime Start time of the maintenance window. This must be in epoch time and may be up
   *        to 5 years in the future.
   * @param duration Duration of the maintenance window in seconds. The minimum duration is 1 second
   *        and the maximum is 157784630 seconds ( 5 years).
   * @param forwardAlerts Defines whether or not alerts should be forwarded to the next Moolet in
   *        the processing chain.
   * @param recurringPeriod Whether or not this is a recurring maintenance window. Set this to:<br>
   *        <ul>
   *        <li>1 for a recurring maintenance window.</li>
   *        <li>0 for a one-time maintenance window.</li>
   *        </ul>
   *        <br>
   *        If not specified, defaults to 0. If you set this property to 1, you must specify
   *        recurring_period_units.
   * @param recurringPeriodUnits Specifies the recurring period of the maintenance window, in days,
   *        weeks or months. Defaults to 0 if recurring_period is set to 0 . V alid values are:
   *        <ul>
   *        <li>2 = daily</li>
   *        <li>3 = weekly</li>
   *        <li>4 = monthly</li>
   *        </ul>
   * @return The new maintenance window ID.A JSON string, with status code and a message, will be
   *         returned if MoogSoft could not process the request for any reason.
   * @throws Exception When startTime or name is null or if there is error making an HTTP request to
   *         Moogsoft
   */
    @SuppressWarnings("deprecation")
    public static String createMaintenanceWindow(String name, String description, String filter, long startTime, long duration, boolean forwardAlerts, Integer recurringPeriod, Integer recurringPeriodUnits) throws Exception {
        
        if(name == null)
            throw new Exception("name is not available.");
        
        if(description == null)
            description = "";
        
        String result = null;
        JSONObject json = new JSONObject();

        json.put("name", name);
        json.put("description", description);
        json.put("filter", filter);
        json.put("start_date_time", startTime);
        json.put("duration", duration);
        json.put("forward_alerts", forwardAlerts);
        
        if(recurringPeriod != null && recurringPeriodUnits != null) {
            json.put("recurring_period", recurringPeriod);
            json.put("recurring_period_units", recurringPeriodUnits);
        }

        try {
            if (Log.log.isDebugEnabled()) {
                Log.log.debug(String.format("MoogSoftGateway:createMaintenanceWindow:body '%s'", json.toString()));
            }
            String response = client.callPost("createMaintenanceWindow", null, json.toString(), null, "application/json", null);
            result = checkRespForError(response);
        } catch(Exception e) {
            Log.log.error(e.getMessage(), e);
            throw e;
        }
        return result;
    }
    
  /**
   * A GET request that returns maintenance windows based on the window ID and how many should be
   * fetched. Only returns active recurring windows and scheduled maintenance windows, not expired
   * or deleted maintenance windows.
   * 
   * @param startId The start point for where to fetch windows from (ie, 0 to start at the first, 10
   *        to start at the 11th)
   * @param limit The number of windows to fetch
   * @return A JSON string with a list of maintenance windows
   * @throws Exception When there are errors making an HTTP Request
   */
    public static String getMaintenanceWindows(int startId, int limit) throws Exception {

        String maintenaceWindows = null;
        
        try {
            StringBuilder sb = new StringBuilder();
            sb.append("getMaintenanceWindows?start=").append(startId).append("&limit=").append(limit);
            if (Log.log.isDebugEnabled()) {
                Log.log.debug(String.format("MoogSoftGateway:'%s'", sb.toString()));
            }
            maintenaceWindows = client.callGet(sb.toString(), null, null);
            checkRespForError(maintenaceWindows);
        } catch(Exception e) {
            Log.log.error(e.getMessage(), e);
            throw e;
        }

        if(StringUtils.isBlank(maintenaceWindows))
            return null;

        Log.log.debug(maintenaceWindows);
        
        return maintenaceWindows;
    }
    
  /**
   * A POST request that deletes a maintenance window.
   * 
   * @param id ID of the maintenance window to delete.
   * @return : null if successful. A JSON string, with status code and a message, will be returned
   *         if MoogSoft could not process the request for any reason.
   * @throws Exception When there is an error making an HTTP request to Moogsoft
   */
    @SuppressWarnings("deprecation")
    public static String deleteMaintenaceWindow(int id) throws Exception {
        String result = null;
        JSONObject json = new JSONObject();
        
        json.put("id", id);

        try {
            if (Log.log.isDebugEnabled()) {
                Log.log.debug(String.format("MoogSoftGateway:deleteMaintenanceWindow:body '%s'", json.toString()));
            }
            String response = client.callPost("deleteMaintenanceWindow", null, json.toString(), null, "application/json", null);
            result = checkRespForError(response);
        } catch(Exception e) {
            Log.log.error(e.getMessage(), e);
            throw e;
        }
        return result;
    }
    
    /*
     * If jsonResponse has statusCode return the json, else return null
     */
    private static String checkRespForError(String jsonResponse) {
        String result = null;
        if (StringUtils.isNotBlank(jsonResponse)) {
            result = jsonResponse;
        	try {
                JSONObject jsonObject = (JSONObject) JSONSerializer.toJSON(jsonResponse);
                if (jsonObject.has("statusCode")) {
                    // if the response has statusCode, that means something is not right... print the response.
                    Log.log.error(jsonResponse);
                    result = jsonResponse;
                }
            } catch (Exception e) {
                // looks like the returned response is not a json string. Print it as is in the log.
                Log.log.error(jsonResponse);
            }
        }
        return result;
    }
    
  /**
   * A POST request that assigns and acknowledges the moderator to the Alert for a specified alert
   * id and user id.
   * 
   * @param alertId The alert id
   * @param userId The user id
   * @return : null if successful. A JSON string, with status code and a message, will be returned
   *         if MoogSoft could not process the request for any reason.
   * @throws Exception
   */
    @SuppressWarnings("deprecation")
    public static String assignAndAcknowledgeAlert(Integer alertId, Integer userId) throws Exception {

        if (alertId == null || userId == null) {
            throw new Exception("AlertId or UserId cannot be null");
        }
        String result = null;
        JSONObject json = new JSONObject();

        json.put("alert_id", alertId);
        json.put("user_id", userId);
        
        try {
            if (Log.log.isDebugEnabled()) {
                Log.log.debug(String.format("MoogSoftGateway:assignAndAcknowledgeAlert:body '%s'", json.toString()));
            }
            String response = client.callPost("assignAndAcknowledgeAlert", null, json.toString(), null, "application/json", null);
            result = checkRespForError(response);
        } catch(Exception e) {
            Log.log.error(e.getMessage(), e);
            throw e;
        }
        return result;
    }
    
  /**
   * A POST request that assigns the moderator to the Alert for a specified alert id and user id.
   * 
   * @param alertId : Integer representing alert id.
   * @param userId : Integer representing user id.
   * @return : null if successful. A JSON string, with status code and a message, will be returned
   *         if MoogSoft could not process the request for any reason.
   * @throws Exception
   */
    @SuppressWarnings("deprecation")
    public static String assignAlert(Integer alertId, Integer userId) throws Exception {

        if (alertId == null || userId == null) {
            throw new Exception("AlertId or UserId cannot be null");
        }
        String result = null;
        JSONObject json = new JSONObject();

        json.put("alert_id", alertId);
        json.put("user_id", userId);
        
        try {
            if (Log.log.isDebugEnabled()) {
                Log.log.debug(String.format("MoogSoftGateway:assignAlert:body '%s'", json.toString()));
            }
            String response = client.callPost("assignAlert", null, json.toString(), null, "application/json", null);
            result = checkRespForError(response);
        } catch(Exception e) {
            Log.log.error(e.getMessage(), e);
            throw e;
        }
        return result;
    }
    
  /**
   * A POST request that closes a specified Alert.
   * 
   * @param alertId : Integer representing alert id.
   * @return : null if successful. A JSON string, with status code and a message, will be returned
   *         if MoogSoft could not process the request for any reason.
   * @throws Exception
   */
    @SuppressWarnings("deprecation")
    public static String closeAlert(Integer alertId) throws Exception {

        if (alertId == null) {
            throw new Exception("AlertId cannot be null");
        }
        String result = null;
        JSONObject json = new JSONObject();
        json.put("alert_id", alertId);
        
        try {
            if (Log.log.isDebugEnabled()) {
                Log.log.debug(String.format("MoogSoftGateway:closeAlert:body '%s'", json.toString()));
            }
            String response = client.callPost("closeAlert", null, json.toString(), null, "application/json", null);
            result = checkRespForError(response);
        } catch(Exception e) {
            Log.log.error(e.getMessage(), e);
            throw e;
        }
        return result;
    }
    
  /**
   * A POST request that de-assigns the current moderator from the Alert for a specified alert id.
   * 
   * @param alertId The Alert ID
   * @return : null if successful. A JSON string, with status code and a message, will be returned
   *         if MoogSoft could not process the request for any reason.
   * @throws Exception When Alert ID is null or if there are errors while making an HTTP request to
   *         Moogsoft
   */
    @SuppressWarnings("deprecation")
    public static String deassignAlert(Integer alertId) throws Exception {

        if (alertId == null) {
            throw new Exception("AlertId cannot be null");
        }
        String result = null;
        JSONObject json = new JSONObject();
        json.put("alert_id", alertId);
        
        try {
            if (Log.log.isDebugEnabled()) {
                Log.log.debug(String.format("MoogSoftGateway:deassignAlert:body '%s'", json.toString()));
            }
            String response = client.callPost("deassignAlert", null, json.toString(), null, "application/json", null);
            result = checkRespForError(response);
        } catch(Exception e) {
            Log.log.error(e.getMessage(), e);
            throw e;
        }
        return result;
    }
    
  /**
   * A GET request that returns the total number of Alerts, and a list of their Alert IDs for a
   * specified Alert filter and a limit
   * 
   * @param query A JSON or SQL like alert filter <br>
   *        <BLOCKQUOTE>You can now use SQL-like filter conditions similar to URL or Cookbook
   *        filters instead of JSON formatted filters:<br>
   *        Example request to get the first 20 alert_ids with query: <b>agent != SYSLOG AND
   *        description matches 'AUTH-SERVICE':</b></BLOCKQUOTE>
   * 
   * @param limit The maximum number of alert IDs to return
   * 
   * @return Successful requests return a JSON string which contains Alert details as follows:
   *         <BLOCKQUOTE> {<br>
   *         "total_alerts":20,<br>
   *         "alert_ids":[78,234,737,1253,1459,1733,2166,2653,2855,3133,3414,3538,3729,3905,3991,4110,4160,4536,4692,4701]<br>
   *         } </BLOCKQUOTE>
   * @throws Exception When query is blank or if there are errors making an HTTP request to Moogsoft
   */
    
    public static String getAlertIds(String query, Integer limit) throws Exception {
        
        if(StringUtils.isBlank(query))
            throw new Exception("Query filter 'query' cannot be blank.");
        if (limit == null)
            limit = 20;
        
        String jsonAlertIds = null;
        Map<String, String> params = new HashMap<>();
        params.put("query", query);
        params.put("limit", limit.toString());
        
        try {
            if (Log.log.isDebugEnabled()) {
                Log.log.debug(String.format("MoogSoftGateway:getAlertIds:params '%s'", params.toString()));
            }
            jsonAlertIds = client.callGet("getAlertIds", params, null);
            if (StringUtils.isNotBlank(jsonAlertIds) && jsonAlertIds.contains("User is not authenticated")) {
                Log.log.error(String.format("User %s is not authenticated", user));
            }
            checkRespForError(jsonAlertIds);
        } catch(Exception e) {
            Log.log.error(e.getMessage(), e);
            throw e;
        }
        
        return jsonAlertIds;
    }
    
  /**
   * A POST request that removes a specified alert from the specified Situation
   * 
   * @param alertId The Alert ID
   * @param situationId The Situation ID
   * @return : null if successful. A JSON string, with status code and a message, will be returned
   *         if MoogSoft could not process the request for any reason.
   * @throws Exception When alertId or situationId is null or when there are errors in making an
   *         HTTP request to Moogsoft
   */
    @SuppressWarnings("deprecation")
    public static String removeAlertFromSituation(Integer alertId, Integer situationId) throws Exception {

        if (alertId == null || situationId == null) {
            throw new Exception("AlertId or Situation Id cannot be null");
        }
        String result = null;
        JSONObject json = new JSONObject();

        json.put("alert_id", alertId);
        json.put("sitn_id", situationId);
        
        try {
            if (Log.log.isDebugEnabled()) {
                Log.log.debug(String.format("MoogSoftGateway:removeAlertFromSituation:body '%s'", json.toString()));
            }
            String response = client.callPost("removeAlertFromSituation", null, json.toString(), null, "application/json", null);
            result = checkRespForError(response);
        } catch(Exception e) {
            Log.log.error(e.getMessage(), e);
            throw e;
        }
        return result;
    }
    
  /**
   * A POST request that acknowledges or un-acknowledges the moderator to the Alert for a specified
   * alert id and acknowledge state.
   * 
   * @param alertId The Alert ID
   * @param acknowledgeState The acknowledge state. The possible values are 0 for un-acknowledged
   *        and 1 for acknowledged.
   * @return null if successful. A JSON string, with status code and a message, will be returned
   *         if MoogSoft could not process the request for any reason.
   * @throws Exception When alertId or acknowledgeState is null or if acknowledge state is not 0 or
   *         1 or if there are errors making HTTP request to Moogsoft
   */
    @SuppressWarnings("deprecation")
    public static String setAlertAcknowledgeState(Integer alertId, Integer acknowledgeState) throws Exception {

        if (alertId == null || acknowledgeState == null) {
            throw new Exception("AlertId and/or Acknowledge state, cannot be null");
        }
        
        if (acknowledgeState > 1 || acknowledgeState < 0) {
            throw new Exception("The acknowledge state cannot be other than either 0 for un-acknowledged and 1 for acknowledged");
        }
        String result = null;
        JSONObject json = new JSONObject();

        json.put("alert_id", alertId);
        json.put("acknowledged", acknowledgeState);
        
        try {
            if (Log.log.isDebugEnabled()) {
                Log.log.debug(String.format("MoogSoftGateway:setAlertAcknowledgeState:body '%s'", json.toString()));
            }
            String response = client.callPost("setAlertAcknowledgeState", null, json.toString(), null, "application/json", null);
            result = checkRespForError(response);
        } catch(Exception e) {
            Log.log.error(e.getMessage(), e);
            throw e;
        }
        return result;
    }
    
  /**
   * A POST request that sets the severity level for a specified Alert.
   * 
   * @param alertId The Alert ID
   * @param severity The alert's severity as an integer
   *        <BLOCKQUOTE>
   *        <table border="1" style="margin-left: 40px">
   *        <tr>
   *        <td>&nbsp;0&nbsp;</td>
   *        <td>&nbsp;Clear&nbsp;</td>
   *        </tr>
   *        <tr>
   *        <td>&nbsp;1&nbsp;</td>
   *        <td>&nbsp;Indeterminate&nbsp;</td>
   *        </tr>
   *        <tr>
   *        <td>&nbsp;2&nbsp;</td>
   *        <td>&nbsp;Warning&nbsp;</td>
   *        </tr>
   *        <tr>
   *        <td>&nbsp;3&nbsp;</td>
   *        <td>&nbsp;Minor&nbsp;</td>
   *        </tr>
   *        <tr>
   *        <td>&nbsp;4&nbsp;</td>
   *        <td>&nbsp;Major&nbsp;</td>
   *        </tr>
   *        <tr>
   *        <td>&nbsp;5&nbsp;</td>
   *        <td>&nbsp;Critical&nbsp;</td>
   *        </tr>
   *        </table>
   *        </BLOCKQUOTE>
   * 
   * @return null if successful. A JSON string, with status code and a message, will be returned if
   *         MoogSoft could not process the request for any reason.
   * @throws Exception When alertId or severity is null or if severity is greater than 5 or less
   *         than 0
   */
    @SuppressWarnings("deprecation")
    public static String setAlertSeverity(Integer alertId, Integer severity) throws Exception {

        if (alertId == null || severity == null) {
            throw new Exception("AlertId and/or severity cannot be null");
        }
        
        if (severity > 5 || severity < 0) {
            throw new Exception("The severity cannot be other than either 0 for clear, 1 for Indeterminate, 2 for Warning, 3 for Minor, 4 for Major or 5 for Critical");
        }
        String result = null;
        JSONObject json = new JSONObject();

        json.put("alert_id", alertId);
        json.put("severity", severity);
        
        try {
            if (Log.log.isDebugEnabled()) {
                Log.log.debug(String.format("MoogSoftGateway:setAlertSeverity:body '%s'", json.toString()));
            }
            String response = client.callPost("setAlertSeverity", null, json.toString(), null, "application/json", null);
            result = checkRespForError(response);
        } catch(Exception e) {
            Log.log.error(e.getMessage(), e);
            throw e;
        }
        return result;
    }
    
  /**
   * A POST request that adds a new process to the database.Processes are external business entities
   * related to business activities that are affected by the incidents that Moogsoft AIOps captures
   * in Situations.
   * 
   * @param name The process name
   * @param description The process description. Optional
   * @return null if successful. A JSON string, with status code and a message, will be returned
   *         if MoogSoft could not process the request for any reason.
   * @throws Exception When Process name is blank, When there are errors making an HTTP Request to
   *         Moogsoft
   */
    @SuppressWarnings("deprecation")
    public static String addProcess(String name, String description) throws Exception {

        if (StringUtils.isBlank(name)) {
            throw new Exception("Process name cannot be blank.");
        }
        String result = null;
        JSONObject json = new JSONObject();

        json.put("name", name);
        if (StringUtils.isNotBlank(description)) {
            json.put("description", description);
        }
        
        try {
            if (Log.log.isDebugEnabled()) {
                Log.log.debug(String.format("MoogSoftGateway:addProcess:body '%s'", json.toString()));
            }
            String response = client.callPost("addProcess", null, json.toString(), null, "application/json", null);
            result = checkRespForError(response);
        } catch(Exception e) {
            Log.log.error(e.getMessage(), e);
            throw e;
        }
        return result;
    }
    
  /**
   * A POST request that adds a new external service to the database.An external service is a
   * business entity monitored by Moogsoft AIOps via Event streams.
   * 
   * @param name Thes service name.
   * @param description The service description. Optional
   * @return null if successful. A JSON string, with status code and a message, will be returned
   *         if MoogSoft could not process the request for any reason.
   * @throws Exception When service name is blank or When there is an error making an HTTP request
   *         to Moogsoft
   */
    @SuppressWarnings("deprecation")
    public static String addService(String name, String description) throws Exception {

        if (StringUtils.isBlank(name)) {
            throw new Exception("Service name cannot be blank.");
        }
        String result = null;
        JSONObject json = new JSONObject();

        json.put("name", name);
        if (StringUtils.isNotBlank(description)) {
            json.put("description", description);
        }
        
        try {
            if (Log.log.isDebugEnabled()) {
                Log.log.debug(String.format("MoogSoftGateway:addService:body '%s'", json.toString()));
            }
            String response = client.callPost("addService", null, json.toString(), null, "application/json", null);
            result = checkRespForError(response);
        } catch(Exception e) {
            Log.log.error(e.getMessage(), e);
            throw e;
        }
        return result;
    }
    
    /**
     * A POST requests that deletes a maintenance window.
     * 
     * @param windowId : The Window ID
     * @return null if successful. A JSON string, with status code and a message, will be returned if MoogSoft could not process the request for any reason.
     * @throws Exception
     */
    @SuppressWarnings("deprecation")
    public static String deleteMaintenanceWindows(Integer windowId) throws Exception {
        if (windowId == null) {
            throw new Exception("Winodw id cannot be null.");
        }
        String result = null;
        JSONObject json = new JSONObject();

        json.put("id", windowId);
        
        try {
            if (Log.log.isDebugEnabled()) {
                Log.log.debug(String.format("MoogSoftGateway:deleteMaintenanceWindows:body '%s'", json.toString()));
            }
            String response = client.callPost("deleteMaintenanceWindows", null, json.toString(), null, "application/json", null);
            result = checkRespForError(response);
        } catch(Exception e) {
            Log.log.error(e.getMessage(), e);
            throw e;
        }
        
        return result;
    }
    
    /**
     * A GET request that exports the configuration and mapping needed for an integration in JSON format.
     *  The exported JSON file can be saved as a duplicate configuration of the original integration.
     *  For example, you can modify and save the returned object as webhook_lam_custom.conf. Run it with this command:<p>
     *  
     *  <pre> {@code $MOOGSOFT_HOME/bin/webhook_lam --config=webhook_lam_custom.conf}</pre>
     * @param integrationId
     * @return : JSON String representing integration configuration.
     * @throws Exception
     */
    public static String getIntegrationConfig(Integer integrationId) throws Exception {
        
        if(integrationId == null)
            throw new Exception("Integration Id cannot be null.");
        
        String details = null;
        String uri = "getIntegrationConfig?integration_id=" + integrationId;
        
        try {
            if (Log.log.isDebugEnabled()) {
                Log.log.debug(String.format("MoogSoftGateway:'%s'", uri));
            }
            details = client.callGet(uri, null, null);
            checkRespForError(details);
        } catch(Exception e) {
            Log.log.error(e.getMessage(), e);
            throw e;
        }
        
        return details;
    }
    
    /**
     * A GET request that returns maintenance windows that matches a filter.
     * 
     * @param filter : String representing SQL or JSON filter
     * @param limit : Integer representing the number of windows to be fetched. If null, defaults to 100.
     * @return : Json String representing the maintenance windows:<p>
     *      <pre>
     *      {@code
     *          {
                   "windows": [
                      {
                         "filter": {
                            "column": "type",
                            "op": 10,
                            "value": "KnownErrorType1234",
                            "type": "LEAF"
                         },
                         "duration": 3600,
                         "recurring_period": 1,
                         "del_flag": false,
                         "forward_alerts": false,
                         "last_updated": 1499425460,
                         "name": "My window 1",
                         "updated_by": 5,
                         "description": "My description 1",
                         "id": 1,
                         "recurring_period_units": 3,
                         "start_date_time": 1499425427
                      },
                      {
                         "filter": {
                            "column": "description",
                            "op": 10,
                            "value": "FireInServerRoom",
                            "type": "LEAF"
                         },
                         "duration": 3600,
                         "recurring_period": 0,
                         "del_flag": false,
                         "forward_alerts": false,
                         "last_updated": 1499425489,
                         "name": "My second window",
                         "updated_by": 5,
                         "description": "Technical details here",
                         "id": 2,
                         "recurring_period_units": 0,
                         "start_date_time": 1499425462
                      }
                   ]
                }
     *      }
     *      </pre>
     * @throws Exception
     */
    public static String findMaintenanceWindows(String filter, Integer limit) throws Exception {
        
        if(StringUtils.isBlank(filter)) {
            throw new Exception("SQL or JSON filter cannot be an empty string");
        }
        
        if (limit == null) {
            limit = 100;
        }
        
        String details = null;
        String uri = "findMaintenanceWindows?filter=" + URLEncoder.encode(filter,"UTF-8") +"&limit=" + limit;
        
        try {
            if (Log.log.isDebugEnabled()) {
                Log.log.debug(String.format("MoogSoftGateway:'%s'", uri));
            }
            details = client.callGet(uri, null, null);
            checkRespForError(details);
        } catch(Exception e) {
            Log.log.error(e.getMessage(), e);
            throw e;
        }
        
        return details;
    }
    
    /**
     * A GET request that returns the list of possible severities and severity IDs.
     * 
     * @return : Successful requests return a JSON String containing the following:<p>
     * <pre>
     * {@code [{
            "name": "Clear",
            "severity_id": 0
            }, {
                "name": "Indeterminate",
                "severity_id": 1
            }, {
                "name": "Warning",
                "severity_id": 2
            }, {
                "name": "Minor",
                "severity_id": 3
            }, {
                "name": "Major",
                "severity_id": 4
            }, {
                "name": "Critical",
                "severity_id": 5
            }]}
     * </pre>
     * @throws Exception
     */
    public static String getSeverities() throws Exception {
        
        String details = null;
        String uri = "getSeverities";
        
        try {
            if (Log.log.isDebugEnabled()) {
                Log.log.debug(String.format("MoogSoftGateway:'%s'", uri));
            }
            details = client.callGet(uri, null, null);
            checkRespForError(details);
        } catch(Exception e) {
            Log.log.error(e.getMessage(), e);
            throw e;
        }
        
        return details;
    }
    
    /**
     * A GET request that returns the list of processes.
     * 
     * @param limit : An Integer represeting the max number of results to be returned. If null, defaults to 1000.
     * @return : Successful requests return a JSON String containing the following:<p>
     * <pre>
     * {@code [{
            "process_id": 1,
            "name": "Example1",
            "description": "Example1"
            },
            {
                "process_id": 2,
                "name": "Example2",
                "description": "Example2"
            },
            {
                "process_id": 3,
                "name": "Example3",
                "description": "Example3"
            }]
       }
       </pre>
     * @throws Exception
     */
    public static String getProcesses(Integer limit) throws Exception {
        
        if (limit == null) {
            limit = 1000;
        }
        String details = null;
        String uri = "getProcesses?limit=" + limit;
        
        try {
            if (Log.log.isDebugEnabled()) {
                Log.log.debug(String.format("MoogSoftGateway:'%s'", uri));
            }
            details = client.callGet(uri, null, null);
            checkRespForError(details);
        } catch(Exception e) {
            Log.log.error(e.getMessage(), e);
            throw e;
        }
        
        return details;
    }
    
    /**
     * A GET request that returns the list of services.
     * 
     * @param limit : An Integer represeting the max number of results to be returned. If null, defaults to 1000.
     * @return : Successful requests return a JSON string containing the following:<p>
     * <pre>
     * {@code
     *      [{
                "service_id": 15,
                "service_name": "Service A"
            }, {
                "service_id": 4,
                "service_name": "Service B"
            }]
     * }
     * </pre>
     * @throws Exception
     */
    public static String getServices(Integer limit) throws Exception {
        
        if (limit == null) {
            limit = 1000;
        }
        String details = null;
        String uri = "getServices?limit=" + limit;
        
        try {
            if (Log.log.isDebugEnabled()) {
                Log.log.debug(String.format("MoogSoftGateway:'%s'", uri));
            }
            details = client.callGet(uri, null, null);
            checkRespForError(details);
        } catch(Exception e) {
            Log.log.error(e.getMessage(), e);
            throw e;
        }
        
        return details;
    }
    
    /**
     * A GET request that list all endpoints available with their description.
     * Auth token is not needed for this API.
     *  
     * @return : Successful requests return a JSON string containing an array of endpoints. e.g.:<p>
     * <pre>
     * {@code
     *      [{
                "endpoint": "getTeamSituationStats",
                "description": "returns the number of active situations assign to a team over time",
                "display_name": "Open Situations by Team",
                "parameters": {
                    "teams": {
                        "mapping": {
                            "display_value": "name",
                            "endpoint": "getTeams",
                            "value": "team_id"
                        },
                        "type": "mapped",
                        "required": false
                    },
                    "from": {
                        "description": "A timestamp from epoch in seconds",
                        "type": "Long",
                        "required": true
                    },
                    "to": {
                        "description": "A timestamp from epoch in seconds",
                        "type": "Long",
                        "required": true
                    }
                }
            }]
     * }
     * </pre>
     * @throws Exception
     */
    public static String getStats() throws Exception {
        
        String details = null;
        String uri = "getStats";
        
        try {
            if (Log.log.isDebugEnabled()) {
                Log.log.debug(String.format("MoogSoftGateway:'%s'", uri));
            }
            details = client.callGet(uri, null, null);
            checkRespForError(details);
        } catch(Exception e) {
            Log.log.error(e.getMessage(), e);
            throw e;
        }
        
        return details;
    }
    
    /**
     * A GET request that returns the list of active teams.
     * Auth token is not needed for this API.
     * 
     * @return : Successful requests return a JSON object containing the following:<p>
     * <pre>
     * {@code
     *      [{
                "status_id": 1,
                "name": "Opened"
            }, {
                "status_id": 2,
                "name": "Unassigned"
            }, {
                "status_id": 3,
                "name": "Assigned"
            }, {
                "status_id": 4,
                "name": "Acknowledged"
            }, {
                "status_id": 5,
                "name": "Unacknowledged"
            }, {
                "status_id": 6,
                "name": "Active"
            }, {
                "status_id": 7,
                "name": "Dormant"
            }, {
                "status_id": 8,
                "name": "Resolved"
            }, {
                "status_id": 9,
                "name": "Closed"
            }, {
                "status_id": 10,
                "name": "SLA Exceeded"
            }]
     * }
     * </pre>
     * @throws Exception
     */
    public static String getStatuses() throws Exception {
        
        String details = null;
        String uri = "getStatuses";
        
        try {
            if (Log.log.isDebugEnabled()) {
                Log.log.debug(String.format("MoogSoftGateway:'%s'", uri));
            }
            details = client.callGet(uri, null, null);
            checkRespForError(details);
        } catch(Exception e) {
            Log.log.error(e.getMessage(), e);
            throw e;
        }
        
        return details;
    }
    
    /**
     * A GET request that returns a summary of current alerts and Situations in Moogsoft AIOps.
     * 
     * @return : Successful requests return a JSON object system_summary, containing Moogsoft AIOps statistics in the following:<p>
     * <pre>
     * {@code
     *      {
               "system_summary": {
                  "total_events": 61676,
                  "open_sitns": 571,
                  "avg_events_per_sitn": 305,
                  "open_sitns_up": 565,
                  "open_sitns_down": 2,
                  "avg_alerts_per_sitn": 16,
                  "open_sigs_unassigned": 310,
                  "timestamp": 1499425056
               }
            }
     * }
     * </pre>
     * @throws Exception
     */
    public static String getSystemSummary() throws Exception {
        
        String details = null;
        String uri = "getSystemSummary";
        
        try {
            if (Log.log.isDebugEnabled()) {
                Log.log.debug(String.format("MoogSoftGateway:'%s'", uri));
            }
            details = client.callGet(uri, null, null);
            checkRespForError(details);
        } catch(Exception e) {
            Log.log.error(e.getMessage(), e);
            throw e;
        }
        return details;
    }
    
    /**
     * A GET request that returns current system status information for all processes.
     * @return : Successful requests return a JSON String containing the system status information.
     * @throws Exception
     */
    public static String getSystemStatus() throws Exception {
        String details = null;
        String uri = "getSystemStatus";
        
        try {
            if (Log.log.isDebugEnabled()) {
                Log.log.debug(String.format("MoogSoftGateway:'%s'", uri));
            }
            details = client.callGet(uri, null, null);
            checkRespForError(details);
        } catch(Exception e) {
            Log.log.error(e.getMessage(), e);
            throw e;
        }
        return details;
    }
    
    /**
     * A POST request that associates the external client with a specified Situation. This allows Moogsoft AIOps to filter events and send only those of interest to an external system.<p>
     * 
     * @param situationId : Integer representing Situation Id.
     * @param serviceName : The name of the external service (for example, ServiceNow).
     * @param resourceId : The ID of the external service entity to associate with this Situation
     * @return : null if successful. A JSON string, with status code and a message, will be returned if MoogSoft could not process the request for any reason.
     * @throws Exception
     */
    @SuppressWarnings("deprecation")
    public static String addSigCorrelationInfo(Integer situationId, String serviceName, String resourceId) throws Exception {

        if(situationId == null || StringUtils.isBlank(serviceName) || StringUtils.isBlank(resourceId))
            throw new Exception("Situation Id and/or Service Name and/or Resurce Id cannot be empty/null.");
        
        String result = null;
        JSONObject json = new JSONObject();

        json.put("sitn_id", situationId);
        json.put("service_name", serviceName);
        json.put("resource_id", resourceId);
        
        try {
            if (Log.log.isDebugEnabled()) {
                Log.log.debug(String.format("MoogSoftGateway:addSigCorrelationInfo:body '%s'", json.toString()));
            }
            String response = client.callPost("addSigCorrelationInfo", null, json.toString(), null, "application/json", null);
            result = checkRespForError(response);
        } catch(Exception e) {
            Log.log.error(e.getMessage(), e);
            throw e;
        }
        return result;
    }
    
    /**
     * A POST request that assigns and acknowledges the moderator to the Situation for a specified situation ID and user ID.<p>
     * 
     * @param situationId : Integer representing Situation Id.
     * @param userId : The User ID.
     * @param userName : A valid username.
     * @return : null if successful. A JSON string, with status code and a message, will be returned if MoogSoft could not process the request for any reason.
     * @throws Exception
     */
    @SuppressWarnings("deprecation")
    public static String assignAndAcknowledgeSituation(Integer situationId, String userId, String userName) throws Exception {

        if(situationId == null || StringUtils.isBlank(userId) && StringUtils.isBlank(userName))
            throw new Exception("Situation Id and either of User Id or User Name cannot be empty/null.");
        
        String result = null;
        JSONObject json = new JSONObject();

        json.put("sitn_id", situationId);
        json.put("user_id", userId);
        json.put("username", userName);
        
        try {
            if (Log.log.isDebugEnabled()) {
                Log.log.debug(String.format("MoogSoftGateway:assignAndAcknowledgeSituation:body '%s'", json.toString()));
            }
            String response = client.callPost("assignAndAcknowledgeSituation", null, json.toString(), null, "application/json", null);
            result = checkRespForError(response);
        } catch(Exception e) {
            Log.log.error(e.getMessage(), e);
            throw e;
        }
        return result;
    }
    
    /**
     * A POST request that assigns the moderator to the Situation for a specified Situation ID and user ID.<p>
     * 
     * @param situationId : Integer representing Situation Id.
     * @param userId : The User ID.
     * @param userName : A valid username.
     * @return : null if successful. A JSON string, with status code and a message, will be returned if MoogSoft could not process the request for any reason.
     * @throws Exception
     */
    @SuppressWarnings("deprecation")
    public static String assignSituation(Integer situationId, String userId, String userName) throws Exception {

        if(situationId == null || (StringUtils.isBlank(userId) && StringUtils.isBlank(userName)))
            throw new Exception("Situation Id and either of User Id or User Name cannot be empty/null.");
        
        String result = null;
        JSONObject json = new JSONObject();

        json.put("sitn_id", situationId);
        json.put("user_id", userId);
        json.put("username", userName);
        
        try {
            if (Log.log.isDebugEnabled()) {
                Log.log.debug(String.format("MoogSoftGateway:assignSituation:body '%s'", json.toString()));
            }
            String response = client.callPost("assignSituation", null, json.toString(), null, "application/json", null);
            result = checkRespForError(response);
        } catch(Exception e) {
            Log.log.error(e.getMessage(), e);
            throw e;
        }
        return result;
    }
    
    /**
     * A POST request that assigns one or more teams to a Situation. Once successfully run, Moogsoft AIOps marks the Situation as overridden and the Teams Manager Moolet can no longer modify its team assignment.<p>
     * The endpoint replaces any teams previously assigned to the Situation. You can also use it to unassign all teams from a Situation.<p>
     * 
     * @param situationId : Integer representing Situation Id.
     * @param teamIds : A list of Integer team IDs to assign to the Situation. Specify an empty list to unassign all teams from the Situation.
     * @param teamNames : A list of team names to assign to the Situation. Specify an empty list to unassign all teams from the Situation.
     * @return : null if successful. A JSON string, with status code and a message, will be returned if MoogSoft could not process the request for any reason.
     * @throws Exception
     * @throws NullPointerException if any of the parameters passed in are null
     */
    @SuppressWarnings("deprecation")
    public static String assignTeamsToSituation(Integer situationId, List<Integer> teamIds, List<String> teamNames) throws Exception {

        if(situationId == null || (teamIds == null && teamNames == null))
            throw new NullPointerException("Situation Id, teamIds or teamNames cannot be null");
        
        String result = null;
        JSONObject json = new JSONObject();

        json.put("sitn_id", situationId);
        json.put("team_ids", teamIds);
        json.put("team_names", teamNames);
        
        try {
            if (Log.log.isDebugEnabled()) {
                Log.log.debug(String.format("MoogSoftGateway:assignTeamsToSituation:body '%s'", json.toString()));
            }
            String response = client.callPost("assignTeamsToSituation", null, json.toString(), null, "application/json", null);
            result = checkRespForError(response);
        } catch(Exception e) {
            Log.log.error(e.getMessage(), e);
            throw e;
        }
        return result;
    }
    
    /**
     * A POST request that that de-assigns the current moderator from the Situation for a specified situation ID.
     * 
     * @param situationId : Integer representing Situation Id.
     * @return : null if successful. A JSON string, with status code and a message, will be returned if MoogSoft could not process the request for any reason.
     * @throws Exception
     */
    @SuppressWarnings("deprecation")
    public static String deassignSituation(Integer situationId) throws Exception {

        if(situationId == null)
            throw new Exception("Situation Id cannot be null.");
        
        String result = null;
        JSONObject json = new JSONObject();

        json.put("sitn_id", situationId);
        
        try {
            if (Log.log.isDebugEnabled()) {
                Log.log.debug(String.format("MoogSoftGateway:deassignSituation:body '%s'", json.toString()));
            }
            String response = client.callPost("deassignSituation", null, json.toString(), null, "application/json", null);
            result = checkRespForError(response);
        } catch(Exception e) {
            Log.log.error(e.getMessage(), e);
            throw e;
        }
        return result;
    }
    
    /**
     * A GET request that returns the total number of active Situations, and a list of their Situation IDs. Active Situations are those that are not Closed, Resolved or Dormant.
     * 
     * @return
     *      Successful requests return a JSON object containing the following:<p>
     *      <pre>
     *      {@code
     *          {
                   "total_situations":10,
                   "sitn_ids":[4, 5, 6, 12, 14, 15, 16, 17, 18, 19]
                }
     *      }
     *      </pre>
     * @throws Exception
     */
    public static String getActiveSituationIds() throws Exception {        
        String result = null;
        
        try {
            if (Log.log.isDebugEnabled()) {
                Log.log.debug("MoogSoftGateway:getActiveSituationIds:empty body ");
            }
            String response = client.callGet("getActiveSituationIds", null, null);
            result = checkRespForError(response);
        } catch(Exception e) {
            Log.log.error(e.getMessage(), e);
            throw e;
        }
        return result;
    }
    
    /**
     * A GET request that retrieves probable root cause (PRC) information for all alerts or specified alerts within a Situation.
     * 
     * @param situationId : Integer representing Situation Id.
     * @param alertIds : A list of Integer alert IDs (Optional)
     * @return
     *      Successful Return:<p>
     *      <pre>
     *      {@code
     *          {
                   "non_causal":
                     [2,3],
                   "unlabelled":
                     [4],
                   "causal":
                     [1]
                }
     *      }
     *      </pre>
     * @throws Exception
     */
    public static String getPrcLabels(Integer situationId, List<Integer> alertIds) throws Exception {
        
        if(situationId == null)
          throw new Exception("Situation Id cannot be null.");
        
        String prlLabels = null;
        Map<String, String> params = new HashMap<>();
        params.put("sitn_id", situationId.toString());
        if (CollectionUtils.isNotEmpty(alertIds)) {
            params.put("alert_ids", alertIds.toString());
        }
        
        try {
            if (Log.log.isDebugEnabled()) {
                Log.log.debug(String.format("MoogSoftGateway:getPrcLabels:params '%s'", params.toString()));
            }
            prlLabels = client.callGet("getPrcLabels", params, null);
            checkRespForError(prlLabels);
        } catch(Exception e) {
            Log.log.error(e.getMessage(), e);
            throw e;
        }
        
        return prlLabels;
    }
    
    /**
     * A GET request that retreives all correlation information related to a specified Situation.
     * 
     * @param situationId : Integer representing Situation Id.
     * @return
     *      Successful Return:<p>
     *      <pre>
     *      {@code
     *          [
                    {
                    "sig_id": 1,
                    "service_name": "Example1",
                    "external_id": "Example1",
                    "properties": "{"Example1":"Example1"}
                    },
                    {
                    "sig_id": 2,
                    "service_name": "Example2",
                    "external_id": "Example2",
                    "properties": "{"Example2":"Example2"}
                    }
                ]
     *      }
     *      </pre>
     * @throws Exception
     */
    public static String getSigCorrelationInfo(Integer situationId) throws Exception {
        
        if(situationId == null)
          throw new Exception("Situation Id cannot be null.");
        
        String prlLabels = null;
        Map<String, String> params = new HashMap<>();
        params.put("sitn_id", situationId.toString());
        
        try {
            if (Log.log.isDebugEnabled()) {
                Log.log.debug(String.format("MoogSoftGateway:getSigCorrelationInfo:params '%s'", params.toString()));
            }
            prlLabels = client.callGet("getSigCorrelationInfo", params, null);
            checkRespForError(prlLabels);
        } catch(Exception e) {
            Log.log.error(e.getMessage(), e);
            throw e;
        }
        
        return prlLabels;
    }
    
    /**
     * A GET request that returns the activies for Situations, ordered most recent last
     * 
     * @param situationIds : Integer representing Situation Id.
     * @param start : Integer represneting starting from which row should data be included in results.
     * @param limit : Integer representing limit for how many activities wanted in the output.
     * @param actions : List of Integers representing actions_code.
     * @return
     *      Successful requests return a JSON object containing an array of the following:<p>
     *      
     *      <pre>
     *      {@code
     *          "activities": [{
                    "uid": 2,
                    "action_code": 1,
                    "description": "Situation Created",
                    "details": {},
                    "type": "event",
                    "sig_id": 1,
                    "timed_at": 1507039842
                    }, {
                    "uid": 2,
                    "action_code": 14,
                    "description": "Added Alerts To Situation",
                    "details": {}
                    "alerts": [1, 2]
                }]
            }
            </pre>
     *                 
     * @throws Exception
     */
    public static String getSituationActions(List<Integer> situationIds, Integer start, Integer limit, List<Integer> actions) throws Exception {
        if(CollectionUtils.isEmpty(situationIds) || CollectionUtils.isEmpty(actions) || start == null || limit == null)
          throw new Exception("Situation Ids and/or actions and/or start and/or limit cannot be null/empty.");
        
        String prlLabels = null;
        Map<String, String> params = new HashMap<>();
        params.put("sitn_ids", situationIds.toString());
        params.put("actions", actions.toString());
        params.put("start", start.toString());
        params.put("limit", limit.toString());
        
        try {
            if (Log.log.isDebugEnabled()) {
                Log.log.debug(String.format("MoogSoftGateway:getSituationActions:params '%s'", params.toString()));
            }
            prlLabels = client.callGet("getSituationActions", params, null);
            checkRespForError(prlLabels);
        } catch(Exception e) {
            Log.log.error(e.getMessage(), e);
            throw e;
        }
        
        return prlLabels;
    }
    
    /**
     * A GET request that returns the description for a specified Situation.
     * 
     * @param situationId : Integer representing Situation Id.
     * @return:
     *      Successful requests return a JSON object containing the following:<p>
     *      
     *      <pre>
     *      {@code
     *          {
                   "sitn_id": "1",
                   "description": "SyslogLamCookbook source"
                }
     *      }
     *      </pre>
     * @throws Exception
     */
    public static String getSituationDescription(Integer situationId) throws Exception {
        
        if(situationId == null)
          throw new Exception("Situation Id cannot be null.");
        
        String prlLabels = null;
        Map<String, String> params = new HashMap<>();
        params.put("sitn_id", situationId.toString());
        
        try {
            if (Log.log.isDebugEnabled()) {
                Log.log.debug(String.format("MoogSoftGateway:getSituationDescription:params '%s'", params.toString()));
            }
            prlLabels = client.callGet("getSituationDescription", params, null);
            checkRespForError(prlLabels);
        } catch(Exception e) {
            Log.log.error(e.getMessage(), e);
            throw e;
        }
        
        return prlLabels;
    }
    
    /**
     * A GET request that returns a list of host names for a specified Situation, either for all the alerts in the Situation or just for the unique alerts.<p>
     * Hosts are the names (defined in the alerts.source field in the database) for the sources of Events.
     * 
     * @param situationId : Integer representing Situation Id.
     * @param forUniqueAlerts : Boolean indicates the alerts to use to get the host names:<p>
     *      true = use only alerts unique to the Situation<p>
     *      false = use all alerts in the Situation (default)
     * @return:
     *      Successful requests return a JSON object containing the following:<p>
     *      <pre>
     *      {@code
     *          {
                   "hosts": [
                      "172.27.127.247-888",
                      "90.223.60.20-SSS",
                      "bm1.wnwx.isp.acme.com-OOO",
                      "vm3.sshyw.isp.acme.com-OOO"
                   ]
                }
     *      }
     *      </pre>
     * @throws Exception
     */
    public static String getSituationHosts(Integer situationId, Boolean forUniqueAlerts) throws Exception {
        
        if(situationId == null)
          throw new Exception("Situation Id cannot be null.");
        
        if (forUniqueAlerts == null) {
            forUniqueAlerts = false;
        }
        
        String situationHosts = null;
        Map<String, String> params = new HashMap<>();
        params.put("sitn_id", situationId.toString());
        params.put("for_unique_alerts", forUniqueAlerts.toString());
        
        try {
            if (Log.log.isDebugEnabled()) {
                Log.log.debug(String.format("MoogSoftGateway:getSituationHosts:params '%s'", params.toString()));
            }
            situationHosts = client.callGet("getSituationHosts", params, null);
            checkRespForError(situationHosts);
        } catch(Exception e) {
            Log.log.error(e.getMessage(), e);
            throw e;
        }
        
        return situationHosts;
    }
    
    /**
     * A GET request that that returns the total number of Situations, and a list of their Situation IDs for a specified situation filter and a limit.
     * 
     * @param query : String representing a JSON or SQL like situation filter.
     * @param limit : The maximum number of situation IDs to return. If null, will be set to 20.
     * @return
     *      Successful requests return a JSON object containing the following:
     *      <pre>
     *      {@code
     *          {
                   "total_situations": 7,
                   "sitn_ids": [
                      87,
                      121,
                      128,
                      278,
                      523,
                      1003,
                      1519
                   ]
                }
     *      }
     *      </pre>
     * @throws Exception
     */
    public static String getSituationIds(String query, Integer limit) throws Exception {
        
        if(StringUtils.isBlank(query))
          throw new Exception("query, Situation filter, cannot be blank.");
        
        if (limit == null) {
            limit = 20;
        }
        
        String situationIds = null;
        Map<String, String> params = new HashMap<>();
        params.put("query", query);
        params.put("limit", limit.toString());
        
        try {
            if (Log.log.isDebugEnabled()) {
                Log.log.debug(String.format("MoogSoftGateway:getSituationIds:params '%s'", params.toString()));
            }
            situationIds = client.callGet("getSituationIds", params, null);
            checkRespForError(situationIds);
        } catch(Exception e) {
            Log.log.error(e.getMessage(), e);
            throw e;
        }
        
        return situationIds;
    }
    
    /**
     * A GET request that returns a list of process names for a specified Situation.
     * 
     * @param situationId : Integer representing Situation Id.
     * @return
     *      Successful requests return a JSON object containing the following:
     *      <pre>
     *      {@code
     *          {
                   "processes": [
                      "Knowledge Management",
                      "Online Transaction Processing",
                      "Web Content Management",
                      "40GbE",
                      "8-bit Unicode Transcoding Platform"
                   ]
                }
     *      }
     *      </pre>
     * @throws Exception
     */
    public static String getSituationProcesses(Integer situationId) throws Exception {
        
        if(situationId == null)
          throw new Exception("Situation Id cannot be null.");
        
        String situationProcessNames = null;
        Map<String, String> params = new HashMap<>();
        params.put("sitn_id", situationId.toString());
        
        try {
            if (Log.log.isDebugEnabled()) {
                Log.log.debug(String.format("MoogSoftGateway:getSituationProcesses:params '%s'", params.toString()));
            }
            situationProcessNames = client.callGet("getSituationProcesses", params, null);
            checkRespForError(situationProcessNames);
        } catch(Exception e) {
            Log.log.error(e.getMessage(), e);
            throw e;
        }
        
        return situationProcessNames;
    }
    
    /**
     * A GET request that returns a list of external service names for a specified Situation.
     * 
     * @param situationId : Integer representing Situation Id.
     * @return
     *      Successful request return, with a primary service name defined:
     *      <pre>
     *      {@code
     *          {
                   "services": [
                      "Cloud Management Platform",
                      "Geographic Information Systems",
                      "Knowledge Management",
                      "Online Transaction Processing",
                      "Storage Subsystem",
                      "Web Content Management",
                      "0-bit Emulation",
                      "40GbE",
                      "8-bit Unicode Transcoding Platform"
                   ]
                }
     *      }
     *      </pre>
     * @throws Exception
     */
    public static String getSituationServices(Integer situationId) throws Exception {
        
        if(situationId == null)
          throw new Exception("Situation Id cannot be null.");
        
        String situationServices = null;
        Map<String, String> params = new HashMap<>();
        params.put("sitn_id", situationId.toString());
        
        try {
            if (Log.log.isDebugEnabled()) {
                Log.log.debug(String.format("MoogSoftGateway:getSituationServices:params '%s'", params.toString()));
            }
            situationServices = client.callGet("getSituationServices", params, null);
            checkRespForError(situationServices);
        } catch(Exception e) {
            Log.log.error(e.getMessage(), e);
            throw e;
        }
        
        return situationServices;
    }
    
    /**
     * A GET request that returns thread entries for a specified Situation. Threads are comments or 'story activity' on Situations.
     * You can select specific thread entries to return using start and limit values. If not, their default values return the first 100 entries. The entries returned are ordered by most recent first.
     * 
     * @param situationId : Integer representing Situation Id.
     * @param threadName : String representing the name of the thread to get entries from.
     * @param start : The number of the first thread entry to return (default = 0)
     * @param limit : The maximum number of thread entries to return (default = 100)
     * @return
     *      Successful requests return a JSON String containing the following:<p>
     *      <pre>
     *      {@code
     *          {
                   "entries": [
                      {
                         "entry_text": "My second entry here",
                         "user_id": 5,
                         "time": 1499424718
                      },
                      {
                         "entry_text": "My entry here",
                         "user_id": 5,
                         "time": 1499424710
                      }
                   ],
                   "sitn_id": 358,
                   "thread_name": "Support"
                }
             }
             </pre>
     * @throws Exception
     */
    public static String getThreadEntries(Integer situationId, String threadName, Integer start, Integer limit) throws Exception {
        
        if(situationId == null || StringUtils.isBlank(threadName))
          throw new Exception("Situation Id and/or thread name cannot be null/empty.");
        
        if (start == null) {
            start = 0;
        }
        if (limit == null) {
            limit = 100;
        }
        String situationThreadEntries = null;
        Map<String, String> params = new HashMap<>();
        params.put("sitn_id", situationId.toString());
        params.put("thread_name", threadName);
        params.put("start", start.toString());
        params.put("limit", limit.toString());
        
        try {
            if (Log.log.isDebugEnabled()) {
                Log.log.debug(String.format("MoogSoftGateway:getThreadEntries:params '%s'", params.toString()));
            }
            situationThreadEntries = client.callGet("getThreadEntries", params, null);
            checkRespForError(situationThreadEntries);
        } catch(Exception e) {
            Log.log.error(e.getMessage(), e);
            throw e;
        }
        
        return situationThreadEntries;
    }
    
    /**
     * A DELETE request that removes all correlation information related to a specified Situation.
     * 
     * @param situationId : Integer representing Situation Id.
     * @param serviceName : String representing the service name (Optional)
     * @param externalId : String representing the external ID (Optional)
     * @return : null if successful. A JSON string, with status code and a message, will be returned if MoogSoft could not process the request for any reason.
     * @throws Exception
     */
    @SuppressWarnings("deprecation")
    public static String removeSigCorrelationInfo(Integer situationId, String serviceName, String externalId) throws Exception {

        if(situationId == null)
            throw new Exception("Situation Id cannot be null.");
        
        String result = null;
        JSONObject json = new JSONObject();
        json.put("sitn_id", situationId);
        
        if (StringUtils.isNotBlank(serviceName)) {
            json.put("serviceName", serviceName);
        }
        if (StringUtils.isNotBlank(externalId)) {
            json.put("externalId", externalId);
        }
        
        try {
            if (Log.log.isDebugEnabled()) {
                Log.log.debug(String.format("MoogSoftGateway:removeSigCorrelationInfo:body: %s", json.toString()));
            }
            String response = client.callPost("removeSigCorrelationInfo", null, json.toString(), null, "application/json", null);
            result = checkRespForError(response);
        } catch(Exception e) {
            Log.log.error(e.getMessage(), e);
            throw e;
        }
        return result;
    }
    
    /**
     * A POST request that resolves a specified Situation that is currently open.
     * 
     * @param situationId : Integer representing Situation Id.
     * @return : null if successful. A JSON string, with status code and a message, will be returned if MoogSoft could not process the request for any reason.
     * @throws Exception
     */
    @SuppressWarnings("deprecation")
    public static String resolveSituation(Integer situationId) throws Exception {

        if(situationId == null)
            throw new Exception("Situation Id cannot be null.");
        
        String result = null;
        JSONObject json = new JSONObject();
        json.put("sitn_id", situationId);
        
        try {
            if (Log.log.isDebugEnabled()) {
                Log.log.debug(String.format("MoogSoftGateway:resolveSituation:body: %s", json.toString()));
            }
            String response = client.callPost("resolveSituation", null, json.toString(), null, "application/json", null);
            result = checkRespForError(response);
        } catch(Exception e) {
            Log.log.error(e.getMessage(), e);
            throw e;
        }
        return result;
    }
    
    /**
     * A POST request that sets the probable root cause (PRC) labels for specified alerts within a Situation. You must specify at least one alert ID and a PRC level for the alert.
     * You can mark alerts as causal, non_causal or unlabelled within a Situation. An alert can have different PRC levels within different Situations.
     * 
     * @param situationId : Integer representing Situation Id.
     * @param causal : List of Integers representing casual PRC levels.
     * @param nonCausal : List of Integers representing non casual PRC levels.
     * @param unlabelled : List of Integers representing unlabelled PRC levels.
     * @return : null if successful. A JSON string, with status code and a message, will be returned if MoogSoft could not process the request for any reason.
     * @throws Exception
     */
    @SuppressWarnings("deprecation")
    public static String setPrcLabels(Integer situationId,
                    List<Integer> causal, List<Integer> nonCausal, List<Integer> unlabelled) throws Exception {

        if((situationId == null)) {
            throw new Exception("Situation Id cannot be null.");
        }
        
        if (CollectionUtils.isEmpty(causal) && CollectionUtils.isEmpty(nonCausal) && CollectionUtils.isEmpty(unlabelled)) {
            throw new Exception("At least one probable root cause (PRC) labels from casual, nonCasual or unlabelled should be specified.");
        }
        
        String result = null;
        JSONObject json = new JSONObject();
        json.put("sitn_id", situationId);
        if (CollectionUtils.isNotEmpty(causal)) {
            json.put("causal", causal.toArray());
        }
        if (CollectionUtils.isNotEmpty(nonCausal)) {
            json.put("non_causal", nonCausal.toArray());
        }
        if (CollectionUtils.isNotEmpty(unlabelled)) {
            json.put("unlabelled", unlabelled.toArray());
        }
        
        try {
            if (Log.log.isDebugEnabled()) {
                Log.log.debug(String.format("MoogSoftGateway:setPrcLabels:body: %s", json.toString()));
            }
            String response = client.callPost("setPrcLabels", null, json.toString(), null, "application/json", null);
            result = checkRespForError(response);
        } catch(Exception e) {
            Log.log.error(e.getMessage(), e);
            throw e;
        }
        return result;
    }
    
    /**
     * A POST request that acknowledges or unacknowledges the moderator to the Situation for a specified situation ID and acknowledge state.
     * 
     * @param situationId : Integer representing Situation Id.
     * @param acknowledged : The acknowledge state (0 for unacknowledged, 1 for acknowledged)
     * @return : null if successful. A JSON string, with status code and a message, will be returned if MoogSoft could not process the request for any reason.
     * @throws Exception
     */
    @SuppressWarnings("deprecation")
    public static String setSituationAcknowledgeState(Integer situationId, Integer acknowledged) throws Exception {
        if(situationId == null || acknowledged == null) {
            throw new Exception("Situation Id and/or the acknowledge state cannot be null.");
        }
        
        if (acknowledged < 0 || acknowledged > 1) {
            throw new Exception ("The acknowledge state can only have two states, 0 for unacknowledged and 1 for acknowledged");
        }
        String result = null;
        JSONObject json = new JSONObject();
        json.put("sitn_id", situationId);
        json.put("acknowledged", acknowledged);
        
        try {
            if (Log.log.isDebugEnabled()) {
                Log.log.debug(String.format("MoogSoftGateway:setSituationAcknowledgeState:body: %s", json.toString()));
            }
            String response = client.callPost("setSituationAcknowledgeState", null, json.toString(), null, "application/json", null);
            result = checkRespForError(response);
        } catch(Exception e) {
            Log.log.error(e.getMessage(), e);
            throw e;
        }
        return result;
    }
    
    /**
     * A POST request that sets the description for a specified Situation.
     * 
     * @param situationId : Integer representing Situation Id.
     * @param description : The description text to be applied to the Situation
     * @return : null if successful. A JSON string, with status code and a message, will be returned if MoogSoft could not process the request for any reason.
     * @throws Exception
     */
    @SuppressWarnings("deprecation")
    public static String setSituationDescription(Integer situationId, String description) throws Exception {
        if(situationId == null) {
            throw new Exception("Situation Id cannot be null.");
        }
        if (StringUtils.isBlank(description)) {
            description = "";
        }
        String result = null;
        JSONObject json = new JSONObject();
        json.put("sitn_id", situationId);
        json.put("description", description);
        
        try {
            if (Log.log.isDebugEnabled()) {
                Log.log.debug(String.format("MoogSoftGateway:setSituationDescription:body: %s", json.toString()));
            }
            String response = client.callPost("setSituationDescription", null, json.toString(), null, "application/json", null);
            result = checkRespForError(response);
        } catch(Exception e) {
            Log.log.error(e.getMessage(), e);
            throw e;
        }
        return result;
    }
    
    /**
     * A POST request that applies a list of processes to a specified Situation.
     * Any other processes already associated with the Situation are removed.
     * 
     * @param situationId : Integer representing Situation Id.
     * @param processNameList : A list of process names as text strings (for example, ["P1","P2"]). If any processes supplied do not exist in the database, they are created and assigned to the Situation.
     * @return : null if successful. A JSON string, with status code and a message, will be returned if MoogSoft could not process the request for any reason.
     * @throws Exception
     */
    @SuppressWarnings("deprecation")
    public static String setSituationProcesses(Integer situationId, List<String> processNameList) throws Exception {
        if(situationId == null || CollectionUtils.isEmpty(processNameList)) {
            throw new Exception("Situation Id and/or a list of process names cannot be null/empty");
        }

        String result = null;
        JSONObject json = new JSONObject();
        json.put("sitn_id", situationId);
        json.put("process_list", processNameList);
        
        try {
            if (Log.log.isDebugEnabled()) {
                Log.log.debug(String.format("MoogSoftGateway:setSituationProcesses:body: %s", json.toString()));
            }
            String response = client.callPost("setSituationProcesses", null, json.toString(), null, "application/json", null);
            result = checkRespForError(response);
        } catch(Exception e) {
            Log.log.error(e.getMessage(), e);
            throw e;
        }
        return result;
    }
    
    /**
     * A POST request that applies a list of external services to a specified Situation.
     * Any other services already associated with the Situation are removed.
     * 
     * @param situationId : Integer representing Situation Id.
     * @param serviceNameList : A list of service names as text strings (for example, ["S1","S2"]). If any services supplied do not exist in the database, they are created and assigned to the Situation.
     * @return : null if successful. A JSON string, with status code and a message, will be returned if MoogSoft could not process the request for any reason.
     * @throws Exception
     */
    @SuppressWarnings("deprecation")
    public static String setSituationServices(Integer situationId, List<String> serviceNameList) throws Exception {
        if(situationId == null || CollectionUtils.isEmpty(serviceNameList)) {
            throw new Exception("Situation Id and/or a list of service names cannot be null/empty");
        }

        String result = null;
        JSONObject json = new JSONObject();
        json.put("sitn_id", situationId);
        json.put("service_list", serviceNameList);
        
        try {
            if (Log.log.isDebugEnabled()) {
                Log.log.debug(String.format("MoogSoftGateway:setSituationServices:body: %s", json.toString()));
            }
            String response = client.callPost("setSituationServices", null, json.toString(), null, "application/json", null);
            result = checkRespForError(response);
        } catch(Exception e) {
            Log.log.error(e.getMessage(), e);
            throw e;
        }
        return result;
    }
} // class of MoogSoftAPI