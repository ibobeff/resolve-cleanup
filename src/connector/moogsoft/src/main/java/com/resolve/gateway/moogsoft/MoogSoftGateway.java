package com.resolve.gateway.moogsoft;

import java.lang.reflect.Method;
import java.util.HashMap;
import java.util.Map;
import java.util.List;
import java.util.concurrent.ConcurrentHashMap;
import com.resolve.gateway.BaseClusteredGateway;
import com.resolve.gateway.BaseFilter;
import com.resolve.gateway.Filter;
import com.resolve.gateway.MMoogSoft;
import com.resolve.rsremote.Main;
import com.resolve.util.Constants;
import com.resolve.util.Log;
import com.resolve.util.StringUtils;
// import com.resolve.util.metrics.ExecutionMetrics;
// import com.resolve.util.metrics.ExecutionMetricsException;

import net.sf.json.JSONArray;
import net.sf.json.JSONObject;

public class MoogSoftGateway extends BaseClusteredGateway {

    private static volatile MoogSoftGateway instance = null;
    public static final String GATEWAY_NAME = "GATEWAY_NAME";
    public static final String GATEWAY_TYPE = "GATEWAY_TYPE";
    private static int timeout = 60; //load it from blueprint.properties
    private static String serverUrl = null;

    private Map<Integer, MoogSoftHttpServer> moogsoftHttpServers = new HashMap<Integer, MoogSoftHttpServer>();
    private Map<String, MoogSoftHttpServer> deployedServlets = new HashMap<String, MoogSoftHttpServer>();

    private String queue = null;

    private MoogSoftHttpServer defaultHttpServer;

    public static MoogSoftGateway getInstance(ConfigReceiveMoogSoft config) {
        if (instance == null) {
            instance = new MoogSoftGateway(config);
        }
        return instance;
    }

    /**
     * <br> Before calling this method make sure the Moogsoft gateway is initialized </br>
     * <br> If it gets called before the initialization it will throw exception. </br>
     *
     * @return
     */
    public static MoogSoftGateway getInstance() {
        if (instance == null) {
            throw new RuntimeException("MoogSoft Gateway is not initialized correctly..");
        } else {
            return instance;
        }
    }

    private MoogSoftGateway(ConfigReceiveMoogSoft config) {
        super(config);
    }

    @Override
    public String getLicenseCode() {
        return "MOOGSOFT";
    }

    @Override
    protected void addAdditionalValues(Map<String, Object> arg0, Map<String, String> arg1) {
    }

    @Override
    protected String getGatewayEventType() {
        return "MOOGSOFT";
    }

    @Override
    protected String getMessageHandlerName() {
        return MMoogSoft.class.getSimpleName();
    }

    @Override
    protected Class<MMoogSoft> getMessageHandlerClass() {
        return MMoogSoft.class;
    }

    public String getQueueName() {
    	try {
    		Method method = super.getClass().getDeclaredMethod("getOrgSuffix", null);
	    	if(method != null) {
	    		return queue + getOrgSuffix();
	    	}
    	}catch(Exception ex) {
    		Log.log.debug("ERROR thrown when setting Queue name for Moogsoft");
    	}
    	return queue;
    }

    @Override
    public void start() {
        Log.log.debug("Starting MoogSoft Gateway");
        super.start();
    }

    @Override
    public void run() {
        
        super.sendSyncRequest();
        
        if(isPrimary() && isActive())
            initMoogSoftServers();
    }

    @Override
    protected void initialize() {
        
        ConfigReceiveMoogSoft config = (ConfigReceiveMoogSoft) configurations;
        queue = config.getQueue().toUpperCase();
        try {
            Log.log.info("Initializing MoogSoft Listener");
            this.gatewayConfigDir = "/config/moogsoft/";

            defaultHttpServer = new MoogSoftHttpServer(config);
        } catch (Exception e) {
            Log.log.error("Failed to config MoogSoft Gateway: " + e.getMessage(), e);
        }
    }

    @Override
    public void deactivate() {
        
        super.deactivate();
        
        for (MoogSoftHttpServer httpServer : moogsoftHttpServers.values()) {
            try {
                httpServer.stop();
            } catch (Exception e) {
                Log.log.error(e.getMessage(), e);
            }
        }

        try {
            defaultHttpServer.stop();
        } catch (Exception e)  {
            Log.log.error(e.getMessage(), e);
        }

        moogsoftHttpServers.clear();
    }

    /**
     * This method processes the message received from the MoogSoft system.
     *
     * @param message
     */
    public boolean processData(String filterName, Map<String, String> params) {
        boolean result = true;
        try {
            if (StringUtils.isNotBlank(filterName) && params != null) {
                MoogSoftFilter moogsoftFilter = (MoogSoftFilter) filters.get(filterName);
                if (moogsoftFilter != null && moogsoftFilter.isActive()) {
                    if (Log.log.isDebugEnabled()) {
                        Log.log.debug("Processing filter: " + moogsoftFilter.getId());
                        Log.log.debug("Data received through MoogSoft gateway: " + params);
                    }
                    Map<String, String> runbookParams = params;
                    if (!params.containsKey(Constants.EVENT_EVENTID) || StringUtils.isBlank(params.get(Constants.EVENT_EVENTID))) {
                        runbookParams.put(Constants.EVENT_EVENTID, moogsoftFilter.getEventEventId());
                    }
                    addToPrimaryDataQueue(moogsoftFilter, runbookParams);
                } else {
                    result = false;
                }
            }
        } catch (Exception e) {
            Log.log.warn("Error: " + e.getMessage(), e);
            try {
                Thread.sleep(interval);
            } catch (InterruptedException ie) {
                Log.log.warn("sleep interrupted: " + ie.getMessage(), ie);
            }
        }
        return result;
    }

    @Override
    public void clearAndSetFilters(List<Map<String, Object>> filterList) {
        Log.log.debug("Calling Gateway specific clearAndSetFilter" + (filterList != null ? " " + filterList.size() + ", [" + filterList + "]" : ""));
        super.clearAndSetFilters(filterList);

        if (isPrimary() && isActive()) {
            try {
                initMoogSoftServers();
            } catch (Exception e) {
                Log.log.error(e.getMessage(), e);
            }
        }
    }

    @Override
    protected void cleanFilter(ConcurrentHashMap<String, Filter> undeployedFilters) {
        //Add customized code here to modify your server when you clear out the deployed filters;

        for(Filter filter : undeployedFilters.values())
        {
            MoogSoftFilter moogsoftFilter = (MoogSoftFilter) filter;

            try
            {
                if(moogsoftFilter.getPort() > 0)
                {
                    MoogSoftHttpServer httpServer = moogsoftHttpServers.get(moogsoftFilter.getPort());
                    httpServer.removeServlet(moogsoftFilter.getId());
                    //if there is no filter/servlet remaining then stop the server on the port
                    if(httpServer.getServlets().isEmpty())
                    {
                        httpServer.stop();
                        moogsoftHttpServers.remove(moogsoftFilter.getPort());
                    }
                }
                else
                {
                    //undeploy the servlet from the default server.
                    defaultHttpServer.removeServlet(moogsoftFilter.getId());
                }
            }
            catch (Exception e)
            {
                Log.log.error(e.getMessage(), e);
            }
        }
    }

    public List<Filter> getOrderedFilters() {
        return orderedFilters;
    }

    private void initMoogSoftServers() {
        //Add customized code here to initilize your server based on configuration and deployed filter data;

        try {
            if (!defaultHttpServer.isStarted()) {
                defaultHttpServer.init();
                defaultHttpServer.start();

                if (defaultHttpServer.isStarted())
                {
                    defaultHttpServer.addDefaultResolveServlet();
                }
            }

            for (Filter filter : orderedFilters) {
                MoogSoftFilter moogsoftFilter = (MoogSoftFilter) filter;
                //add the URI based servlets to the default server
                if (moogsoftFilter.getPort() == null || moogsoftFilter.getPort() <= 0) {
                    if (defaultHttpServer.isStarted()) {
                        defaultHttpServer.addServlet(moogsoftFilter);
                        deployedServlets.put(filter.getId(), defaultHttpServer);
                    }
                } else {
                    MoogSoftHttpServer httpServer = moogsoftHttpServers.get(moogsoftFilter.getPort());

                    if (httpServer == null)
                    {
                        MoogSoftHttpServer httpServer1 = deployedServlets.get(filter.getId());

                        if (httpServer1 != null)
                        {
                            //port changed
                            httpServer1.removeServlet(filter.getId());
                            deployedServlets.remove(filter.getId());
                        }

                        httpServer = new MoogSoftHttpServer((ConfigReceiveMoogSoft) configurations, moogsoftFilter.getPort(), moogsoftFilter.getSsl());
                        httpServer.init();
                        httpServer.addServlet(moogsoftFilter);
                        httpServer.start();
                    }
                    else
                    {
                        httpServer.addServlet(moogsoftFilter);
                    }

                    moogsoftHttpServers.put(moogsoftFilter.getPort(), httpServer);
                    deployedServlets.put(filter.getId(), httpServer);
                }
            }
        }
        catch (Exception e)
        {
            Log.log.error(e.getMessage(), e);
        }
    }

    @Override
    public void stop() {
        Log.log.warn("Stopping MoogSoft gateway");

        for (MoogSoftHttpServer httpServer : moogsoftHttpServers.values()) {
            try {
                httpServer.stop();
            } catch (Exception e) {
                Log.log.error(e.getMessage(), e);
            }
        }

        try {
            defaultHttpServer.stop();
        } catch (Exception e)  {
            Log.log.error(e.getMessage(), e);
        }

        moogsoftHttpServers.clear();

        super.stop();
    }

    @Override
    public Filter getFilter(Map<String, Object> params) {
        return new MoogSoftFilter((String) params.get(BaseFilter.ID), (String) params.get(BaseFilter.ACTIVE),
        		(String) params.get(BaseFilter.ORDER), (String) params.get(BaseFilter.INTERVAL), 
        		(String) params.get(BaseFilter.EVENT_EVENTID), (String) params.get(BaseFilter.RUNBOOK), 
        		(String) params.get(BaseFilter.SCRIPT), (String) params.get(MoogSoftFilter.PORT),
        		(String) params.get(MoogSoftFilter.BLOCKING), (String) params.get(MoogSoftFilter.URI), 
        		(String) params.get(MoogSoftFilter.SSL),(String) params.get(MoogSoftFilter.LOCALE),
        		(String) params.get(MoogSoftFilter.FORMAT), (String) params.get(MoogSoftFilter.TIMEZONE));
    }

    public int getWaitTimeout() {
        
        int timeout = ConfigReceiveMoogSoft.DEFAULT_WAIT_TIMEOUT;
        
        try {
            ConfigReceiveMoogSoft configReceiveMoogSoft = (ConfigReceiveMoogSoft)configurations;
            String waitTimeout = configReceiveMoogSoft.getWait_timeout();
            timeout = (new Integer(waitTimeout)).intValue();
        } catch(Exception e) {
            Log.log.error(e.getMessage(),e);
        }
        
        return timeout;
    }

    public int getExecuteTimeout() {
        
        int timeout = ConfigReceiveMoogSoft.DEFAULT_EXECUTE_TIMEOUT;
        
        try {
            ConfigReceiveMoogSoft configReceiveMoogSoft = (ConfigReceiveMoogSoft)configurations;
            String executeTimeout = configReceiveMoogSoft.getExecute_timeout();
            timeout = (new Integer(executeTimeout)).intValue();
        } catch(Exception e) {
            Log.log.error(e.getMessage(),e);
        }
        
        return timeout;
    }

    public String getRunbookResult(String problemId, String processId, String wiki) {

        String str = "";
        long timeout = getWaitTimeout()*1000;

        Map<String, Object> params = new HashMap<String, Object>();
        params.put(Constants.EXECUTE_PROBLEMID, problemId);
        params.put(Constants.EXECUTE_PROCESSID, processId);

        try {
            Map<String, Object> result = Main.getESB().call(Constants.ESB_NAME_RSCONTROL, "MAction.getRunbookResult", params, timeout);

            if(result != null)
                str = parseRunbookResult(result, wiki);
        } catch(Exception e) {
            Log.log.error(e.getMessage(), e);
        }

        return str;
    }

    public String parseRunbookResult(Map<String, Object> result, String wiki)
    {
        String problemId = (String)result.get(Constants.EXECUTE_PROBLEMID);
        String processId = (String)result.get(Constants.EXECUTE_PROCESSID);
        String number = (String)result.get(Constants.WORKSHEET_NUMBER);
        String status = (String)result.get(Constants.STATUS);
        String condition = (String)result.get(Constants.EXECUTE_CONDITION);
        String severity = (String)result.get(Constants.EXECUTE_SEVERITY);
        List<Map<String, String>> taskStatus = (List<Map<String, String>>)result.get(Constants.TASK_STATUS);

        JSONObject json = new JSONObject();

        try
        {
            json.accumulate("Wiki Runbook", wiki);
            json.accumulate("Worksheet Id", problemId);
            json.accumulate("Worksheet Number", number);
            json.accumulate("Process Id", processId);
            json.accumulate("Condition", condition);
            json.accumulate("Severity", severity);
            json.accumulate("Status", status);

            if (StringUtils.isNotEmpty(status) && !status.equalsIgnoreCase(Constants.EXECUTE_STATUS_UNKNOWN))
            {
                JSONArray tasks = new JSONArray();

                for (Map<String, String> task:taskStatus)
                {
                    JSONObject taskItem = new JSONObject();
                    taskItem.accumulate("name", task.get(Constants.EXECUTE_TASKNAME));
                    taskItem.accumulate("id", task.get(Constants.EXECUTE_ACTIONID));
                    taskItem.accumulate("completion", task.get(Constants.EXECUTE_COMPLETION));
                    taskItem.accumulate("condition", task.get(Constants.EXECUTE_CONDITION));
                    taskItem.accumulate("severity", task.get(Constants.EXECUTE_SEVERITY));
                    taskItem.accumulate("summary", task.get(Constants.EXECUTE_SUMMARY));
                    taskItem.accumulate("detail", task.get(Constants.EXECUTE_DETAIL));

                    tasks.add(taskItem);
                }

                json.accumulate("Action Tasks", tasks);
            }
        }
        catch(Exception e)
        {
            Log.log.error(e.getMessage(), e);
        }

        return json.toString();

    } // parseRunbookResult()

    public String processBlockingData(String filterName, Map<String, String> params)
    {
        String result = "";

//        ExecutionMetrics.INSTANCE.registerEventStart("MoogSoft GW - execute runbook and get result or submit runbook for execution");

        try
        {
            if(StringUtils.isNotBlank(filterName) && params != null)
            {
                MoogSoftFilter httpFilter = (MoogSoftFilter) filters.get(filterName);
                if (httpFilter != null && httpFilter.isActive())
                {
                    if(Log.log.isDebugEnabled())
                    {
                        Log.log.debug("Processing filter: " + httpFilter.getId());
                        Log.log.debug("Data received through HTTP gateway: " + params);
                    }

                    Map<String, String> runbookParams = params;

                    if(!params.containsKey(Constants.EVENT_EVENTID) || StringUtils.isBlank(params.get(Constants.EVENT_EVENTID)))
                    {
                        runbookParams.put(Constants.EVENT_EVENTID, httpFilter.getEventEventId());
                    }

                    runbookParams.put("FILTER_ID", filterName);
//                    runbookParams.put(GATEWAY_NAME, "MOOGSOFT");
//                    runbookParams.put(GATEWAY_TYPE, "Push");
                    return this.receiveData(runbookParams);
                }
                else
                {
                    JSONObject message = new JSONObject();
                    message.accumulate("Message", "The filter is inactive.");
                    result = message.toString();
                }
            }
        }
        catch (Exception e)
        {
            Log.log.warn("Error: " + e.getMessage(), e);
            try
            {
                Thread.sleep(interval);
            }
            catch (InterruptedException ie)
            {
                Log.log.warn("sleep interrupted: " + ie.getMessage(), ie);
            }
        }
/*        finally
        {
            try
            {
                ExecutionMetrics.INSTANCE.registerEventEnd("MoogSoft GW - execute runbook and get result or submit runbook for execution", 1);
            }
            catch (ExecutionMetricsException eme)
            {
                Log.log.error(eme.getMessage(), eme);
            }
        }
*/
        return result;
    }
    
    public static String getServerUrl() {
        
        if(serverUrl != null)
            return serverUrl;
        
        Map<String, Object> properties = instance.loadSystemProperties();
        
        String HOSTNAME = (String)properties.get("HOST");
        String PORT = (String)properties.get("PORT");
        
        StringBuffer sb = new StringBuffer();
        sb.append("https://").append(HOSTNAME);
        if(StringUtils.isNotBlank(PORT))
            sb.append(":").append(PORT);
        
        serverUrl = sb.toString();
        
        return serverUrl;
    }

    public Map<String, Object> loadSystemProperties() {

        Map<String, Object> properties = new HashMap<String, Object>();

        ConfigReceiveMoogSoft config = (ConfigReceiveMoogSoft) configurations;

        properties.put("HOST", config.getHost());
        properties.put("PORT", config.getPort());
        properties.put("USERNAME", config.getUsername());
        properties.put("PASSWORD", config.getPassword());

        return properties;
    }

} // class MoogSoftGateway