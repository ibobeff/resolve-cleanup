package com.resolve.rscontrol;

import com.resolve.persistence.model.MoogSoftFilter;

public class MMoogSoft extends MGateway {

    private static final String MODEL_NAME = MoogSoftFilter.class.getSimpleName();

    protected String getModelName() {
        return MODEL_NAME;
    }
}

