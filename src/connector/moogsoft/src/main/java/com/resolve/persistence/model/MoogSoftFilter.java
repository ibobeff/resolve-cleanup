package com.resolve.persistence.model;

import java.util.List;

import javax.persistence.Table;
import javax.persistence.UniqueConstraint;

import org.apache.commons.lang3.StringUtils;
import javax.persistence.Column;
import javax.persistence.Entity;
import com.resolve.services.interfaces.VO;
import com.resolve.util.CryptUtils;
import com.resolve.util.Log;
import com.resolve.services.hibernate.vo.MoogSoftFilterVO;

@Entity
@Table(name = "moogsoft_filter", uniqueConstraints = { @UniqueConstraint(columnNames = { "u_name", "u_queue" }) })
public class MoogSoftFilter extends GatewayFilter<MoogSoftFilterVO> {

    private static final long serialVersionUID = 1L;

    public MoogSoftFilter() {
    }

    public MoogSoftFilter(MoogSoftFilterVO vo) {
        applyVOToModel(vo);
    }

    private String UPassword;

    private Integer UPort;

    private String UBlocking;

    private String UTimezone;

    private String UFormat;

    private String UUri;

    private Boolean USsl;

    private String UUsername;

    private String ULocale;

    @Column(name = "u_password", length = 256)
    public String getUPassword() {
        return this.UPassword;
    }

    public void setUPassword(String uPassword) {
        this.UPassword = uPassword;
    }

    @Column(name = "u_port")
    public Integer getUPort() {
        return this.UPort;
    }

    public void setUPort(Integer uPort) {
        this.UPort = uPort;
    }

    @Column(name = "u_blocking", length = 400)
    public String getUBlocking() {
        return this.UBlocking;
    }

    public void setUBlocking(String uBlocking) {
        this.UBlocking = uBlocking;
    }

    @Column(name = "u_timezone", length = 256)
    public String getUTimezone() {
        return this.UTimezone;
    }

    public void setUTimezone(String uTimezone) {
        this.UTimezone = uTimezone;
    }

    @Column(name = "u_format", length = 256)
    public String getUFormat() {
        return this.UFormat;
    }

    public void setUFormat(String uFormat) {
        this.UFormat = uFormat;
    }

    @Column(name = "u_uri", length = 256)
    public String getUUri() {
        return this.UUri;
    }

    public void setUUri(String uUri) {
        this.UUri = uUri;
    }

    @Column(name = "u_ssl", length = 1)
    public Boolean getUSsl() {
        return this.USsl;
    }

    public void setUSsl(Boolean uSsl) {
        this.USsl = uSsl;
    }

    @Column(name = "u_username", length = 256)
    public String getUUsername() {
        return this.UUsername;
    }

    public void setUUsername(String uUsername) {
        this.UUsername = uUsername;
    }

    @Column(name = "u_locale", length = 256)
    public String getULocale() {
        return this.ULocale;
    }

    public void setULocale(String uLocale) {
        this.ULocale = uLocale;
    }

    public MoogSoftFilterVO doGetVO() {
        MoogSoftFilterVO vo = new MoogSoftFilterVO();
        super.doGetBaseVO(vo);
        
        vo.setUPassword(getUPassword());
        vo.setUPort(getUPort());
        vo.setUBlocking(getUBlocking());
        vo.setUTimezone(getUTimezone());
        vo.setUFormat(getUFormat());
        vo.setUUri(getUUri());
        vo.setUSsl(getUSsl());
        vo.setUUsername(getUUsername());
        vo.setULocale(getULocale());
//        vo.setUTimezone();
        return vo;
    }

    @Override
    public void applyVOToModel(MoogSoftFilterVO vo) {
        if (vo == null) return;
        super.applyVOToModel(vo);
        
        String pass = vo.getUPassword();
        if (!VO.STRING_DEFAULT.equals(pass) && StringUtils.isNotBlank(pass)) {
            try {
                pass = CryptUtils.encrypt(pass);
            } catch(Exception e) {
                Log.log.error(e.getMessage(), e);
            }
        }

        this.setUPassword(pass);
        
        if (!VO.INTEGER_DEFAULT.equals(vo.getUPort())) this.setUPort(vo.getUPort()); else ;
        if (!VO.STRING_DEFAULT.equals(vo.getUBlocking())) this.setUBlocking(vo.getUBlocking()); else ;
        if (!VO.STRING_DEFAULT.equals(vo.getUTimezone())) this.setUTimezone(vo.getUTimezone()); else ;
        if (!VO.STRING_DEFAULT.equals(vo.getUFormat())) this.setUFormat(vo.getUFormat()); else ;
        if (!VO.STRING_DEFAULT.equals(vo.getUUri())) this.setUUri(vo.getUUri()); else ;
        this.setUSsl(vo.getUSsl());
        if (!VO.STRING_DEFAULT.equals(vo.getUUsername())) this.setUUsername(vo.getUUsername()); else ;
        if (!VO.STRING_DEFAULT.equals(vo.getULocale())) this.setULocale(vo.getULocale()); else ;
    }

    @Override
    public List<String> ugetCdataProperty() {
        List<String> list = super.ugetCdataProperty();
        list.add("UPassword");
        list.add("UBlocking");
        list.add("UTimezone");
        list.add("UFormat");
        list.add("UUri");
        list.add("UUsername");
        list.add("ULocale");
        return list;
    }
}

