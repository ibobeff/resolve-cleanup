package com.resolve.services.hibernate.vo;

import com.resolve.util.MappingAnnotation;
import com.resolve.util.StringUtils;

public class MoogSoftFilterVO extends GatewayFilterVO {

    private static final long serialVersionUID = 1L;

    public MoogSoftFilterVO() {
    }

    private String UPassword;

    private Integer UPort;

    private String UBlocking;

    private String UTimezone;

    private String UFormat;

    private String UUri;

    private Boolean USsl;

    private String UUsername;

    private String ULocale;

    @MappingAnnotation(columnName = "PASSWORD")
    public String getUPassword() {
        return this.UPassword;
    }

    public void setUPassword(String uPassword) {
        this.UPassword = uPassword;
    }

    @MappingAnnotation(columnName = "PORT")
    public Integer getUPort() {
        return this.UPort;
    }

    public void setUPort(Integer uPort) {
        this.UPort = uPort;
    }

    @MappingAnnotation(columnName = "BLOCKING")
    public String getUBlocking() {
        return this.UBlocking;
    }

    public void setUBlocking(String uBlocking) {
        this.UBlocking = uBlocking;
    }

    @MappingAnnotation(columnName = "TIMEZONE")
    public String getUTimezone() {
        return this.UTimezone;
    }

    public void setUTimezone(String uTimezone) {
        this.UTimezone = uTimezone;
    }

    @MappingAnnotation(columnName = "FORMAT")
    public String getUFormat() {
        return this.UFormat;
    }

    public void setUFormat(String uFormat) {
        this.UFormat = uFormat;
    }

    @MappingAnnotation(columnName = "URI")
    public String getUUri() {
        return this.UUri;
    }

    public void setUUri(String uUri) {
        this.UUri = uUri;
    }

    @MappingAnnotation(columnName = "SSL")
    public Boolean getUSsl() {
        return this.USsl;
    }

    public void setUSsl(Boolean uSsl) {
        this.USsl = uSsl;
    }

    @MappingAnnotation(columnName = "USERNAME")
    public String getUUsername() {
        return this.UUsername;
    }

    public void setUUsername(String uUsername) {
        this.UUsername = uUsername;
    }

    @MappingAnnotation(columnName = "LOCALE")
    public String getULocale() {
        return this.ULocale;
    }

    public void setULocale(String uLocale) {
        this.ULocale = uLocale;
    }
    
    @Override
    public int hashCode()
    {
        final int prime = 31;
        int result = super.hashCode();
//        result = prime * result + ((UPassword == null) ? 0 : UPassword.hashCode());
        result = prime * result + ((UPort == null) ? 0 : UPort.hashCode());
        result = prime * result + ((UBlocking == null) ? 0 : UBlocking.hashCode());
        result = prime * result + ((UTimezone == null) ? 0 : UTimezone.hashCode());
        result = prime * result + ((UFormat == null) ? 0 : UFormat.hashCode());
        result = prime * result + ((UUri == null) ? 0 : UUri.hashCode());
        result = prime * result + ((USsl == null) ? 0 : USsl.hashCode());
//        result = prime * result + ((UUsername == null) ? 0 : UUsername.hashCode());
        result = prime * result + ((ULocale == null) ? 0 : ULocale.hashCode());
        return result;
    }

    @Override
    public boolean equals(Object obj)
    {
        if (this == obj) return true;
        if (!super.equals(obj)) return false;
        if (getClass() != obj.getClass()) return false;
        MoogSoftFilterVO other = (MoogSoftFilterVO) obj;
//        if (UPassword == null)
//        {
//            if (StringUtils.isNotBlank(other.UPassword)) return false;
//        }
//        else if (!UPassword.trim().equals(other.UPassword == null ? "" : other.UPassword.trim())) return false;
        if (UPort == null)
        {
            if (other.UPort != null) return false;
        }
        else if (!UPort.equals(other.UPort == null ? "" : other.UPort)) return false;
        if (UBlocking == null)
        {
            if (StringUtils.isNotBlank(other.UBlocking)) return false;
        }

        else if (!convertUBlocking(UBlocking).trim().equals(other.UBlocking == null ? "" : other.UBlocking.trim())) return false;
        if (UTimezone == null)
        {
            if (StringUtils.isNotBlank(other.UTimezone)) return false;
        }
        else if (!UTimezone.trim().equals(other.UTimezone == null ? "" : other.UTimezone.trim())) return false;
        if (UFormat == null)
        {
            if (StringUtils.isNotBlank(other.UFormat)) return false;
        }
        else if (!UFormat.trim().equals(other.UFormat == null ? "" : other.UFormat.trim())) return false;
        if (UUri == null)
        {
            if (StringUtils.isNotBlank(other.UUri)) return false;
        }
        else if (!UUri.trim().equals(other.UUri == null ? "" : other.UUri.trim())) return false;
        if (USsl == null)
        {
            if (other.USsl != null) return false;
        }
        else if (!USsl.equals(other.USsl == null ? "" : other.USsl)) return false;
//        if (UUsername == null)
//        {
//            if (StringUtils.isNotBlank(other.UUsername)) return false;
//        }
//        else if (!UUsername.trim().equals(other.UUsername == null ? "" : other.UUsername.trim())) return false;
        if (ULocale == null)
        {
            if (StringUtils.isNotBlank(other.ULocale)) return false;
        }
        else if (!ULocale.trim().equals(other.ULocale == null ? "" : other.ULocale.trim())) return false;
        return true;
    }
    
    private String convertUBlocking(String blockCode) {
    	String retBlock = "";
        switch(blockCode) {
            case "1":
            	retBlock = "Default";
                break;
            case "2":
            	retBlock = "Gateway Script";
                break;
            case "3":
            	retBlock = "Worksheet ID";
                break;
            case "4":
            	retBlock = "Execution Complete";
                break;
            default:
                break;
        }
        return retBlock;
    }
}

