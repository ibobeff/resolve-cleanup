/******************************************************************************
* (C) Copyright 2014
*
* Resolve Systems, LLC
*
* All rights reserved
* This software is distributed under license and may not be used, copied,
* modified, or distributed without the express written permission of
* Resolve Systems, LLC.
*
******************************************************************************/

package com.resolve.gateway.hpomx;

import java.util.Collection;
import java.util.List;
import java.util.Map;

public interface ObjectService
{
    /**
     * Every object has its ID property, the subclass requires to implement this
     * method. Example: Service request could be "NUMBER".
     * 
     * @return
     */
    String getIdPropertyName();

    /**
     * Identity of the object. Implementing class provides this. Example:
     * Incident is "incident" or Problem is "problem"
     * 
     * @return
     */
    String getIdentity();

    /**
     * Creates an object by supplying username, password.
     * 
     * @param nodeIPAddress
     * @param params
     * @param username
     * @param password
     * @return the newly created incident's Id.
     * @throws Exception
     */
    String create(String nodeIPAddress, Map<String, String> params, String username, String password) throws Exception;

    /**
     * Updates an object by supplying username, password.
     * 
     * @param objectId
     *            is mandatory.
     * @param params
     * @param username
     * @param password
     * @return
     * @throws Exception
     */
    void update(String objectId, Map<String, String> params, String username, String password) throws Exception;

    /**
     * Closes an object by its id by supplying username, password.
     * 
     * @param objectId
     *            is mandatory.
     * @param username
     * @param password
     * @return
     * @throws Exception
     */
    void close(String objectId, String username, String password) throws Exception;

    /**
     * Retrieves an object's data by its id (for example, IncidentID).
     * 
     * @param eventId
     * @param username
     * @param password
     * @return
     */
    Map<String, String> selectByEventId(String eventId, String username, String password) throws Exception;

    /**
     * Read data from the object service by supplying username, password.
     * 
     * @param query
     * @param maxResult
     * @param username
     * @param password
     * @return
     * @throws Exception
     */
    List<Map<String, String>> search(String query, int maxResult, String username, String password) throws Exception;
    
    /**
     * Read data from the object service subscription.
     * 
     * @param query (optional)
     * @param maxResultPerPull
     * @param pullOpTimeOutInSecs
     * @param numOfPullOps
     * @param username (optional)
     * @param password (optional)
     * @return
     * @throws Exception
     */
    List<Map<String, String>> subscribe(String query, int maxResultsPerPull, long pullOpTimeOutInSecs, int numOfPullOps, String username, String password) throws Exception;
    
    /**
     * Renew an existing subscription on the object service opened previously.
     * 
     * @param subscriptionId        Id of subscription to renew
     * @param duration              Duration (in msec) to renew subscription for
     * @param username (optional)
     * @param password (optional)
     * @return true/false
     * @throws Exception
     */
    boolean renewSubscription(String subscriptionId, long duration, String username, String password) throws Exception;
    
    /**
     * Create new subscription on the object service.
     * 
     * @param query (optional)      
     * @param duration              Sunscription duration (in msec)
     * @param username (optional)
     * @param password (optional)
     * @return                      Subscription Id
     * @throws Exception
     */
    String createSubscription(String query, long duration, String username, String password) throws Exception;
    
    /**
     * Pull data from object service subscription.
     * 
     * @param subscriptionId
     * @param maxRecordsToPull
     * @param username (optional)
     * @param password (optional)
     * @return                      List of maps where each map represent a single pulled record
     * @throws Exception
     */
    List<Map<String, String>> pullSubscription(String subscriptionId, int maxRecordsToPull, String username, String password) throws Exception;
    
    /**
     * Pull data from object service subscription.
     * 
     * @param subscriptionId
     * @param maxRecordsToPull
     * @param username (optional)
     * @param password (optional)
     * @param filterIntervalInSecs
     * @return                      List of maps where each map represent a single pulled record
     * @throws Exception
     */
    List<Map<String, String>> pullSubscription(String subscriptionId, int maxRecordsToPull, String username, String password, int filterIntervalInSecs) throws Exception;
    
    /**
     * Unsubscribe subscription to object service.
     * 
     * @param subscriptionId
     * @param username (optional)
     * @param password (optional)
     * @return 
     * @throws Exception
     */
    void unSubscribe(String subscriptionId, String username, String password) throws Exception;
    
    /**
     * Object service owner own's one or more objects.
     * 
     * @param objectIds
     *              Collection of id's of objects to own
     * @param username (optional)
     * @param password (optional)
     * @return 
     * @throws Exception
     */
    void ownMany(Collection<String> objectIds, String username, String password) throws Exception;
    
    /**
     * Object service owner disowns one or more objects.
     * 
     * @param objectIds
     *              Collection of id's of objects to disown
     * @param username (optional)
     * @param password (optional)
     * @return 
     * @throws Exception
     */
    void disownMany(Collection<String> objectIds, String username, String password) throws Exception;
    
    /**
     * Add annotation to object in object service.
     * 
     * @param objectId
     *              Id of the object to add annotaion to
     * @param annotation
     *              annotation text
     * @param username (optional)
     * @param password (optional)
     * @return Annotation Id
     * @throws Exception
     */
    String addAnnotation(String objectId, String annotation, String username, String password) throws Exception;
    
    /**
     * Update annotation associated with object in object service.
     * 
     * @param objectId
     *              Id of the object to update annotaion for
     * @param annotationId
     *              Id of the annotation to update
     * @param annotationUpdate
     *              annotation text to update to
     * @param username (optional)
     * @param password (optional)
     * @return 
     * @throws Exception
     */
    void updateAnnotation(String objectId, String annotationId, String annotationUpdate, String username, String password) throws Exception;
    
    /**
     * Delete annotation associated with object in object service.
     * 
     * @param objectId
     *              Id of the object to delete annotaion from
     * @param annotationId
     *              Id of the annotation to delete
     * @param username (optional)
     * @param password (optional)
     * @return 
     * @throws Exception
     */
    void deleteAnnotation(String objectId, String annotationId, String username, String password) throws Exception;
    
    /**
     * Get annotation associated with object in object service.
     * 
     * @param objectId
     *              Id of the object to get annotaions for
     * @param username (optional)
     * @param password (optional)
     * @return Collection of annotations
     * @throws Exception
     */
    Collection<Map<String, String>> getAnnotations(String objectId, String username, String password) throws Exception;
}
