package com.resolve.gateway.hpomx;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.concurrent.ConcurrentHashMap;

import javax.xml.datatype.XMLGregorianCalendar;

import com.resolve.gateway.BaseClusteredGateway;
import com.resolve.gateway.BaseFilter;
import com.resolve.gateway.Filter;
import com.resolve.gateway.MHPOMX;
import com.resolve.query.ResolveQuery;
import com.resolve.rsbase.MainBase;
import com.resolve.rsremote.ConfigReceiveGateway;
import com.resolve.util.Constants;
import com.resolve.util.DateUtils;
import com.resolve.util.Log;
import com.resolve.util.StringUtils;
import com.sun.org.apache.xerces.internal.jaxp.datatype.XMLGregorianCalendarImpl;

public class HPOMXGateway extends BaseClusteredGateway {

    private static volatile HPOMXGateway instance = null;

    private String queue = null;
    
    private static volatile ObjectService eventService;
    private final ConfigReceiveHPOMX hpomxConfiguration;
    private final ResolveQuery resolveQuery;
    
    public static HPOMXGateway getInstance(ConfigReceiveHPOMX config) {
        if (instance == null) {
            instance = new HPOMXGateway(config);
        }
        return instance;
    }

    /**
     * This method is called once the initialization is done. If it gets called
     * before the initialization it will throw exception.
     *
     * @return
     */
    public static HPOMXGateway getInstance() {
        if (instance == null) {
            throw new RuntimeException("HPOMX Gateway is not initialized correctly..");
        } else {
            return instance;
        }
    }

    private HPOMXGateway(ConfigReceiveHPOMX config) {
        super(config);
        hpomxConfiguration = config;
        resolveQuery = new ResolveQuery(new HPOMXQueryTranslator());
    }

    @Override
    public String getLicenseCode() {
        return "HPOMX";
    }

    @Override
    protected void addAdditionalValues(Map<String, Object> arg0, Map<String, String> arg1) {
        return;
    }
    
    @Override
    protected void addAdditionalValues(Map<String, Object> event, Map<String, String> result, Filter filter) {
        
        List<String> objectIdKeyNames = new ArrayList<String>();
        
        switch (((HPOMXFilter)filter).getObject())
        {
            case "event":
                objectIdKeyNames.add(HPOMXConstants.FIELD_TITLE);
                objectIdKeyNames.add(HPOMXConstants.FIELD_EVENT_ID);
                break;
            
            default:
                break;
        }
        
        if (!objectIdKeyNames.isEmpty())
        {
            for (String objectIdKeyName : objectIdKeyNames)
            {
                if (result.containsKey(objectIdKeyName))
                {
                    if (objectIdKeyName.equals(HPOMXConstants.FIELD_TITLE))
                    {
                        event.put(Constants.EXECUTE_REFERENCE, result.get(objectIdKeyName));
                    }
                    else if (objectIdKeyName.equals(HPOMXConstants.FIELD_EVENT_ID))
                    {
                        event.put(Constants.EXECUTE_ALERTID, result.get(objectIdKeyName));
                    }
                }
                else if (result.containsKey(objectIdKeyName.toUpperCase()))
                {
                    if (objectIdKeyName.toUpperCase().equals(HPOMXConstants.FIELD_TITLE.toUpperCase()))
                    {
                        event.put(Constants.EXECUTE_REFERENCE, result.get(objectIdKeyName.toUpperCase()));
                    }
                    else if (objectIdKeyName.toUpperCase().equals(HPOMXConstants.FIELD_EVENT_ID.toUpperCase()))
                    {
                        event.put(Constants.EXECUTE_ALERTID, result.get(objectIdKeyName));
                    }
                }
            }
        }
    }

    @Override
    protected String getGatewayEventType() {
        return "HPOMX";
    }

    @Override
    protected String getMessageHandlerName() {
        return MHPOMX.class.getSimpleName();
    }

    @Override
    protected Class<MHPOMX> getMessageHandlerClass() {
        return MHPOMX.class;
    }

    public String getQueueName() {
        return queue + getOrgSuffix();
    }

    @Override
    public void start() {
        Log.log.debug("Starting HPOMX Gateway");
        super.start();
    }

    @Override
    protected void cleanFilter(ConcurrentHashMap<String, Filter> deletedFilters) {
        
        if (deletedFilters != null && !deletedFilters.isEmpty())
        {
            for (Filter deletedFilter : deletedFilters.values())
            {
                HPOMXFilter deletedHPOMXFilter = (HPOMXFilter)deletedFilter;
                
                if (HPOMXFilter.FILTERTYPE_SUBSCRIPTION.equalsIgnoreCase(deletedHPOMXFilter.getFilterType()) &&
                    StringUtils.isNotBlank(deletedHPOMXFilter.getSubscriptionId()))
                {
                    try
                    {
                        eventService.unSubscribe(deletedHPOMXFilter.getSubscriptionId(), 
                                                 null, null);
                    }
                    catch (Exception e)
                    {
                        Log.log.warn("Ignoring error " + e.getMessage() + " in unsubscribing HPOMXFilter " + 
                                     deletedHPOMXFilter.getId() + " of type " + 
                                     deletedHPOMXFilter.getFilterType() + " with subscription id " + 
                                     deletedHPOMXFilter.getSubscriptionId());
           
                    }
                }
            }
        }
    }

    @Override
    public void run() {
        
        if (!((ConfigReceiveGateway) configurations).isMock())
        {
            super.sendSyncRequest();
        }
        
        String resolveStatusValue = "" + hpomxConfiguration.getStatus_processed();
        String currentServerDateTime = DateUtils.getCurrentDateTimeAsXmlString();
        
        while (running)
        {
            try
            {
                long startTime = System.currentTimeMillis();
                
                if (primaryDataQueue.isEmpty() && !primaryDataQueueExecutorBusy())
                {
                    Log.log.trace(getQueueName() + " Primary Data Queue is empty and Primary Data Queue Executor is free.....");
                    
                    for (Filter filter : orderedFilters)
                    {
                        if (shouldFilterExecute(filter, startTime)) 
                        {
                            Log.log.trace("Filter " + filter.getId() + " executing");
                            
                            HPOMXFilter hpomxFilter = (HPOMXFilter)filter;
                            
                            if (HPOMXFilter.FILTERTYPE_ENUMERATION.equalsIgnoreCase(hpomxFilter.getFilterType()) &&
                                StringUtils.isBlank(hpomxFilter.getLastReceiveDate()))
                            {
                                hpomxFilter.setLastReceiveDate(currentServerDateTime);
                            }
                            
                            List<Map<String, String>> results = invokeService(filter);
                            
                            if (results != null && !results.isEmpty())
                            {
                                for (Map<String, String> result : results)
                                {
                                    boolean processRecord = true;
                                    
                                    if (HPOMXFilter.FILTERTYPE_ENUMERATION.equalsIgnoreCase(hpomxFilter.getFilterType()) &&
                                        hpomxConfiguration.getStatus_active())
                                    {
                                        // HPOM does not allow query based on custom fields
                                        if (result.containsKey(hpomxConfiguration.getStatus_fieldname()) &&
                                            result.get(hpomxConfiguration.getStatus_fieldname()).equalsIgnoreCase(resolveStatusValue))
                                        {
                                            processRecord = false;
                                            Log.log.debug("HPOM Record with title " + 
                                                          result.get(HPOMXConstants.FIELD_TITLE) + 
                                                          " and id " + 
                                                          result.get(HPOMXConstants.FIELD_EVENT_ID) + 
                                                          " is ignored as it is already processed." );
                                        }
                                    }
                                    
                                    if (processRecord)
                                    {
                                        addToPrimaryDataQueue(filter, result);
                                    }
                                }
                                
                                /* Update the lastReceivedDate property. Add a
                                 * second to it, milliseconds would have
                                 * been better but HPOM only deals with seconds.
                                 */
                                String lastReceivedTime = (String) results.get(results.size() - 1).get(HPOMXConstants.FIELD_RECEIVED_TIME);
                                XMLGregorianCalendar cal = XMLGregorianCalendarImpl.parse(lastReceivedTime);
                                cal.setSecond(cal.getSecond() + 1);
                                hpomxFilter.setLastReceiveDate(cal.toXMLFormat());
                            }
                        }
                        else
                        {
                            Log.log.trace("Filter:" + filter.getId() + " is inactive or not ready to execute yet.....");
                        }
                    }
                    
                    if (orderedFilters == null || orderedFilters.size() == 0)
                    {
                        Log.log.trace("There is not deployed filter for HPOMXGateway");
                    }
                }
                
                if (orderedFilters.size() == 0)
                {
                    Thread.sleep(interval);
                }
            } catch (Exception e) {
                Log.log.warn("Warning: " + e.getMessage());
                Log.alert("Instance " + MainBase.main.configId.getGuid() + " " + 
                          getLicenseCode() + "-" + getQueueName() + "-" + getInstanceType() + 
                          " Polling Failed", "Instance " + MainBase.main.configId.getGuid() + 
                          " " + getLicenseCode() + "-" + getQueueName() + "-" + 
                          getInstanceType() + " Polling Failed " + "due to " + e.getMessage());
                try
                {
                    Thread.sleep(interval);
                } catch (InterruptedException ie)
                {
                    Log.log.warn("sleep interrupted: " + ie.getMessage(), ie);
                }
            }
        }
    }

    /**
     * Add customized code here to get the data as List<Map<String, String>> format for each filter 
     * @return List<Map<String, String>>
     * @throws Exception 
     */
    public List<Map<String, String>> invokeService(Filter filter) throws Exception {
        List<Map<String, String>> result = Collections.emptyList();
        //Add customized code here to get data from 3rd party system based the information in the filter;
        HPOMXFilter hpomxFilter = (HPOMXFilter)filter;
        
        String nativeQuery = hpomxFilter.getQuery();
        
        if (StringUtils.isNotBlank(nativeQuery))
        {
            if (StringUtils.isBlank(hpomxFilter.getNativeQuery()))
            {
                try
                {
                    if (StringUtils.isNotBlank(nativeQuery))
                    {
                        nativeQuery = resolveQuery.translate(hpomxFilter.getQuery());
                    }
                }
                catch (Exception e)
                {
                    Log.log.debug("Translation of query \"" + nativeQuery + "\" to ResolveQuery resulted in error " + e.getMessage() + 
                                  ", and hence is treated as native query");
                }
                
                hpomxFilter.setNativeQuery(nativeQuery); // set it so we don't translate again
            }
        }

        if (HPOMXFilter.FILTERTYPE_ENUMERATION.equalsIgnoreCase(hpomxFilter.getFilterType()))
        {
            boolean hasVariable = VAR_REGEX_GATEWAY_VARIABLE.matcher(hpomxFilter.getNativeQuery()).find();
            
            if (hasVariable)
            {
                if (hpomxFilter.getNativeQuery().contains(HPOMXFilter.LAST_RECEIVE_DATE))
                {
                    nativeQuery = hpomxFilter.getNativeQuery().replaceFirst(Constants.VAR_REGEX_GATEWAY_VARIABLE, hpomxFilter.getLastReceiveDate());
                }
            }
            
            if (EventService.OBJECT_IDENTITY.equalsIgnoreCase(hpomxFilter.getObject()))
            {
                result = eventService.search(nativeQuery, 1000, null, null);
            }
        }
        else if (HPOMXFilter.FILTERTYPE_SUBSCRIPTION.equalsIgnoreCase(hpomxFilter.getFilterType()))
        {
            /* HPOMX gateway filters based on subscritpion which does not 
             * support querying of historical events. Subscription only supports
             * current events and events updated after creation of subscription.
             * As a result it makes no difference to events returned by subscritpion
             * even if LAST_RECEIVED_DATE is used in query set on subscription.
             */
            
            if (EventService.OBJECT_IDENTITY.equalsIgnoreCase(hpomxFilter.getObject()))
            {
                try
                {
                    // Creates new/renews existing subscription
                
                    getSubscription(hpomxFilter);
                
                    // Pull events from newly created/renewed subscription
                    
                    result = eventService.pullSubscription(hpomxFilter.getSubscriptionId(), 
                                                           hpomxConfiguration.getSubscription_max_records_to_pull(), 
                                                           null, null, hpomxFilter.getInterval());
                }
                catch (Exception e)
                {
                    Log.log.debug("Error " + e.getMessage() + " in invoking HPOMXFilter " + hpomxFilter.getId() + " of type " + 
                                  hpomxFilter.getFilterType() + " with subscription id " + hpomxFilter.getSubscriptionId() +
                                  ", resetting subscription attached to the filter.");
                    
                    hpomxFilter.setSubscriptionId(null);
                    hpomxFilter.setSubscriptionCreateTime(0l);
                    
                    throw e;
                }
            }
        }
        
        return result;
    }

    /**
     * Add customized code here to initialize connection with 3rd party system
     * @return List<Map<String, String>>
     */
    @Override
    protected void initialize() {
        ConfigReceiveHPOMX config = (ConfigReceiveHPOMX) configurations;
        queue = config.getQueue().toUpperCase();
        this.gatewayConfigDir = "/config/hpomx/";
        //Add customized code here to initilize the connection with 3rd party system;
        eventService = ObjectServiceFactory.getObjectService(config, EventService.OBJECT_IDENTITY);
    }

    /**
     * Add customized code here to do when stop the gateway
     */
    @Override
    public void stop() {
        Log.log.warn("Stopping HPOMX gateway");
        //Add customized code here to stop the connection with 3rd party system;
        super.stop();
    }

    @Override
    public Filter getFilter(Map<String, Object> params) {
        HPOMXFilter hpomxFilter = new HPOMXFilter((String) params.get(BaseFilter.ID), 
                                                  (String) params.get(BaseFilter.ACTIVE), 
                                                  (String) params.get(BaseFilter.ORDER), 
                                                  (String) params.get(BaseFilter.INTERVAL), 
                                                  (String) params.get(BaseFilter.EVENT_EVENTID), 
                                                  (String) params.get(BaseFilter.RUNBOOK), 
                                                  (String) params.get(BaseFilter.SCRIPT), 
                                                  (String) params.get(HPOMXFilter.QUERY), 
                                                  (String) params.get(HPOMXFilter.OBJECT), 
                                                  (String) params.get(HPOMXFilter.FILTERTYPE));
        
        String filterType = (String) params.get(HPOMXFilter.FILTERTYPE);
        
        if (HPOMXFilter.FILTERTYPE_ENUMERATION.equalsIgnoreCase(filterType))
        {
            if (params.containsKey(HPOMXFilter.LAST_RECEIVE_DATE) && StringUtils.isNotBlank((String)params.get(HPOMXFilter.LAST_RECEIVE_DATE)))
            {
                hpomxFilter.setLastReceiveDate((String)params.get(HPOMXFilter.LAST_RECEIVE_DATE));
            }
        }
        
        if (HPOMXFilter.FILTERTYPE_SUBSCRIPTION.equalsIgnoreCase(filterType))
        {
            if (params.containsKey(HPOMXFilter.SUBSCRIPTIONID) && StringUtils.isNotBlank((String)params.get(HPOMXFilter.SUBSCRIPTIONID)))
            {
                hpomxFilter.setSubscriptionId((String)params.get(HPOMXFilter.SUBSCRIPTIONID));
            }
            
            if (params.containsKey(HPOMXFilter.SUBSCRIPTIONCREATETIME) && StringUtils.isNotBlank((String)params.get(HPOMXFilter.SUBSCRIPTIONCREATETIME)))
            {
                hpomxFilter.setSubscriptionCreateTime((String)params.get(HPOMXFilter.SUBSCRIPTIONCREATETIME));
            }
        }
        
        return hpomxFilter;
    }
    
    private void getSubscription(HPOMXFilter hpomxFilter) throws Exception
    {
        boolean create = false;
        boolean renew = false;        
        long currentTimeInMillis = System.currentTimeMillis();
        
        if (StringUtils.isBlank(hpomxFilter.getSubscriptionId()))
        {
            create = true;
            renew = false;
            
            Log.log.debug("HPOMXFilter " + hpomxFilter.getId() + " of type " + 
                          hpomxFilter.getFilterType() + " has no subscription : create = " + 
                          create + ", renew = " + renew);
            
            hpomxFilter.setSubscriptionCreateTime(0l);
        }
        else
        {
            if ((currentTimeInMillis - hpomxFilter.getSubscriptionCreateTime()) >= 
                (hpomxConfiguration.getSubscription_expiration_duration_in_min() * 60 * 1000))
            {
                create = true;
                renew = false;
                
                Log.log.debug("HPOMXFilter " + hpomxFilter.getId() + " of type " + 
                              hpomxFilter.getFilterType() + " has subscription with id " + 
                              hpomxFilter.getSubscriptionId() + " and create timestamp (in msec) of " + 
                              hpomxFilter.getSubscriptionCreateTime() + 
                              " which has expired as current timestamp " + currentTimeInMillis + 
                              " (in msec) is same or past the subscription expiratin duration of " + 
                              hpomxConfiguration.getSubscription_expiration_duration_in_min() + 
                              " minute(s) : create = " + create + ", renew = " + renew);
            }
            else if ((currentTimeInMillis - hpomxFilter.getSubscriptionCreateTime()) >= 
                     (hpomxConfiguration.getSubscription_expiration_duration_in_min() * 50 * 1000))
            {
                create = false;
                renew = true;
                
                Log.log.debug("HPOMXFilter " + hpomxFilter.getId() + " of type " + 
                              hpomxFilter.getFilterType() + " has subscription with id " + 
                              hpomxFilter.getSubscriptionId() + " and create timestamp (in msec) of " + 
                              hpomxFilter.getSubscriptionCreateTime() + 
                              " which needs to be renewed as current timestamp " + currentTimeInMillis + 
                              " (in msec) is same or past the subscription renewal duration of " + 
                              (hpomxConfiguration.getSubscription_expiration_duration_in_min() * 50) + 
                              " seconds : create = " + create + ", renew = " + renew);
                
                hpomxFilter.setSubscriptionCreateTime(0l);
            }
            else
            {
                Log.log.debug("HPOMXFilter " + hpomxFilter.getId() + " of type " + 
                              hpomxFilter.getFilterType() + " has subscription with id " + 
                              hpomxFilter.getSubscriptionId() + " and create timestamp (in msec) of " + 
                              hpomxFilter.getSubscriptionCreateTime() + " with current timestamp " + 
                              currentTimeInMillis + 
                              " (in msec) has not expired and is still available for pull.");
                
                return;
            }
        }
           
        // handle subscripton renewal first so in case of failure it can be recreated 
        
        if (renew)
        {
            renew = eventService.renewSubscription(hpomxFilter.getSubscriptionId(), 
                                                   (hpomxConfiguration.getSubscription_expiration_duration_in_min() * 60 * 1000), 
                                                   null, null);
            
            Log.log.debug("HPOMXFilter " + hpomxFilter.getId() + " of type " + 
                          hpomxFilter.getFilterType() + " subscription with id " + 
                          hpomxFilter.getSubscriptionId() + (renew ? " renewed " : " failed to renew ") + "for another " +
                          hpomxConfiguration.getSubscription_expiration_duration_in_min() + " minute(s)");
            
            create = !renew;
        }
        
        if (create)
        {
            hpomxFilter.setSubscriptionId(null);
            hpomxFilter.setSubscriptionCreateTime(0l);
            
            String newSubscriptionId = eventService.createSubscription(hpomxFilter.getNativeQuery(),
                                                                       (hpomxConfiguration.getSubscription_expiration_duration_in_min() * 60 * 1000),
                                                                       null, null);
            
            Log.log.debug("HPOMXFilter " + hpomxFilter.getId() + " of type " + 
                          hpomxFilter.getFilterType() + " and native query [" + 
                          hpomxFilter.getNativeQuery() + "] subscription with expiry duration of " +
                          hpomxConfiguration.getSubscription_expiration_duration_in_min() + 
                          " minute(s) has been created successfully, New subscription id is " + 
                          newSubscriptionId);
          
            hpomxFilter.setSubscriptionId(newSubscriptionId);
        }
        
        // In either create/renew case set/update subscription create time
        
        hpomxFilter.setSubscriptionCreateTime(currentTimeInMillis);
    }
    
    @SuppressWarnings("rawtypes")
    @Override
    public void updateServerData(Filter filter, Map content)
    {
        if (content.containsKey(HPOMXConstants.FIELD_EVENT_ID))
        {
            String eventId = (String) content.get(HPOMXConstants.FIELD_EVENT_ID);
            Log.log.debug("HPOMX event ID is:" + eventId);
            Map<String, String> params = new HashMap<String, String>();

            // update custom status field
            if (hpomxConfiguration.getStatus_active())
            {
                String statusFieldValue = String.valueOf(hpomxConfiguration.getStatus_processed());
                
                if (content.containsKey(hpomxConfiguration.getStatus_fieldname()))
                {
                    statusFieldValue = (String) content.get(hpomxConfiguration.getStatus_fieldname());
                }
                
                params.put(hpomxConfiguration.getStatus_fieldname(), statusFieldValue);
            }
            
            if (hpomxConfiguration.getRunbookid_active())
            {
                String value = filter.getRunbook();

                if (content.containsKey(hpomxConfiguration.getRunbookid_fieldname()))
                {
                    value = (String) content.get(hpomxConfiguration.getRunbookid_fieldname());
                }
                
                params.put(hpomxConfiguration.getRunbookid_fieldname(), value);
            }
            
            if (params.size() > 0)
            {
                try
                {
                    updateEvent(eventId, params, null, null);
                }
                catch (Exception e)
                {
                    Log.log.error("Error updating the HPOM event: " + eventId, e);
                }
            }
        }
    }
    
    /*
     * Public API Implementation
     */
    
    public String createEvent(String nodeIPAddress, Map<String, String> params, String username, String password) throws Exception
    {
        return eventService.create(nodeIPAddress, params, username, password);
    }

    public void updateEvent(String sysId, Map<String, String> params, String username, String password) throws Exception
    {
        eventService.update(sysId, params, username, password);
    }

    public void closeEvent(String incidentId, String username, String password) throws Exception
    {
        eventService.close(incidentId, username, password);
    }

    public Map<String, String> selectEventById(String eventId, String username, String password) throws Exception
    {
        return eventService.selectByEventId(eventId, username, password);
    }

    public List<Map<String, String>> searchEvent(String query, int maxResult, String username, String password) throws Exception
    {
        if (StringUtils.isNotEmpty(query))
        {
            Log.log.debug("Incoming HPOMX Query: " + query);

            try
            {
                query = resolveQuery.translate(query);
                Log.log.debug("Translated HPOMX Query: " + query);
            }
            catch (Exception e)
            {
                Log.log.debug("Did not translate, the query \"" + query + "\" is treated as native.");
            }
        }
        return eventService.search(query, maxResult, username, password);
    }
    
    public List<Map<String, String>> subscribeEvents(String query, int maxResultsPerPull, long pullOpTimeOutInSecs, int numOfPullOps, String username, String password) throws Exception
    {
        if (StringUtils.isNotEmpty(query))
        {
            Log.log.debug("subscribeEvents: Incoming HPOMX Query: " + query);

            try
            {
                query = resolveQuery.translate(query);
                Log.log.debug("subscribeEvents: Translated HPOMX Query: " + query);
            }
            catch (Exception e)
            {
                Log.log.debug("Did not translate, the query \"" + query + "\" is treated as native.");
            }
        }
                
        return eventService.subscribe(query, maxResultsPerPull, pullOpTimeOutInSecs, numOfPullOps, username, password);
    }
    
    public Set<String> getObjectTypes()
    {
        Set<String> objectTypes = new HashSet<String>();
        objectTypes.add(EventService.OBJECT_IDENTITY);
        return objectTypes;
    }
    
    public HPOMXFilter getDeployedFilter(String filterId)
    {
        HPOMXFilter deployedFilter = null;
        
        for (Filter filter : orderedFilters)
        {
            if (filter.getId().equals(filterId))
            {
                HPOMXFilter foundFilter = (HPOMXFilter)filter;
                
                deployedFilter = new HPOMXFilter(foundFilter.getId(),
                                                 String.valueOf(foundFilter.isActive()),
                                                 String.valueOf(foundFilter.getOrder()),
                                                 String.valueOf(foundFilter.getInterval()),
                                                 foundFilter.getEventEventId(),
                                                 foundFilter.getRunbook(),
                                                 foundFilter.getScript(),
                                                 foundFilter.getQuery(),
                                                 foundFilter.getObject(),
                                                 foundFilter.getFilterType());
                
                if (deployedFilter.getNativeQuery() == null)
                {
                    try
                    {
                        String nativeQuery = resolveQuery.translate(deployedFilter.getQuery());
                        deployedFilter.setNativeQuery(nativeQuery); // set it so we don't translate again
                    }
                    catch (Exception e)
                    {
                        Log.log.debug("Wrong Resolve SQL syntaxt for HPOMX gateway, please check. Query provided \"" + deployedFilter.getQuery() + "\".");
                    }
                }
                
                break;
            }
        }
        
        return deployedFilter;
    }
    
    public void ownMany(Collection<String> incidentIds, String username, String password) throws Exception
    {
        eventService.ownMany(incidentIds, username, password);
    }
    
    public void disownMany(Collection<String> incidentIds, String username, String password) throws Exception
    {
        eventService.disownMany(incidentIds, username, password);
    }
    
    public String addAnnotation(String incidentId, String annotation, String username, String password) throws Exception
    {
        return eventService.addAnnotation(incidentId, annotation, username, password);
    }
    
    public void updateAnnotation(String objectId, String annotationId, String annotationUpdate, String username, String password) throws Exception
    {
        eventService.updateAnnotation(objectId, annotationId, annotationUpdate, username, password);
    }
    
    public void deleteAnnotation(String objectId, String annotationId, String username, String password) throws Exception
    {
        eventService.deleteAnnotation(objectId, annotationId, username, password);
    }
    
    public Collection<Map<String, String>> getAnnotations(String objectId, String username, String password) throws Exception
    {
        return eventService.getAnnotations(objectId, username, password);
    }
}

