package com.resolve.gateway.hpomx;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;

import com.resolve.gateway.AbstractGatewayAPI;
import com.resolve.gateway.BaseFilter;
import com.resolve.util.StringUtils;
import com.resolve.util.SystemUtil;

public class HPOMXAPI extends AbstractGatewayAPI
{
    static
    {
        instance = HPOMXGateway.getInstance();
    }
    
    /**
     * Returns {@link Set} of supported object types.
     * <p>
     * Default no object types are supported returning {@link Collections.EMPTY_SET}.
     * Derived external system specific gateway API must override this method to return
     * {@link Set<String>} of supported object types.
     * 
     * @return {@link Set} of supported object types
     */
    public static Set<String> getObjectTypes()
    {
        return ((HPOMXGateway)instance).getObjectTypes();
    }
    

    /**
     * Creates instance of specified object type in external system.
     * <p>
     * Default attribute name-value pair returned is  
     * {@value com.resolve.gateway.GatewayConstants#METHOD_NOT_SUPPORTED_KEY}-true
     * indicating external system specific gateway does not support creating instances of
     * specified object type.
     * <p>
     * If external system specific gateway does support creation of object then it must return
     * {@link Map} of attribute name-value pairs.
     * 
     * @param  objType   Type of object to create in external system
     * @param  objParams {@link Map} of object parameters
     * @param  userName  User name
     * @param  password  Password
     * @return           Attribute id-value {@link Map} of created object in external system
     * @throws Exception If any error occurs in creating object of specified
     *                   type in external system
     */
    public static Map<String, String> createObject(String objType, Map<String, String> objParams, String userName, String password) throws Exception
    {
        if (StringUtils.isBlank(objType) || objParams == null || objParams.isEmpty())
        {
            throw new Exception("Specified invalid parameters for createObject.");
        }
        
        Map<String, String> objectAttributes = new HashMap<String, String>();
        
        switch (objType)
        {
            case EventService.OBJECT_IDENTITY:
                String eventId = createEvent(SystemUtil.getHostIPAddress(), objParams, userName, password);
                objectAttributes = readObject(objType, eventId, null, userName, password);
                //objectAttributes.put(HPOMXConstants.FIELD_EVENT_ID, eventId);
                break;
                
            default:
                throw new Exception("Currently createObject is supported for objects of type event only");
        }
        
        return objectAttributes;
    }
    
    /**
     * Reads attributes of specified object in external system.
     * <p>
     * Default attribute id-value pair returned is  
     * {@value com.resolve.gateway.GatewayConstants#METHOD_NOT_SUPPORTED_KEY}-true
     * indicating external system specific gateway does not support reading atributes of
     * object.
     * <p>
     * If external system specific gateway does support reading attributes of object then 
     * it must return {@link Map} of attribute name-value pairs. 
     *
     * @param  objType    Object type
     * @param  objId      Id of the object to read attributes of from external system
     * @param  attribs    {@link List} of attributes of object to read, NOT USED
     * @param  userName   User name
     * @param  password   Password
     * @return            {@link Map} of object attribute id-value pairs
     * @throws Exception  If any error occurs in reading attributes of the object in 
     *                    external system
     */
    public static Map<String, String> readObject(String objType, String objId, List<String> attribs, String userName, String password) throws Exception
    {
        if (StringUtils.isBlank(objType) || StringUtils.isBlank(objId))
        {
            throw new Exception("Specified invalid parameters for readObject.");
        }
        
        Map<String, String> objectAttributes = new HashMap<String, String>();
        
        switch (objType)
        {
            case EventService.OBJECT_IDENTITY:
                objectAttributes = selectEventById(objId, userName, password);                
                break;
                
            default:
                throw new Exception("Currently readObject is supported for objects of type event only");
        }
        
        return objectAttributes;
    }
    
    /**
     * Updates attributes of specified object in external system.
     * <p>
     * Default updated attribute name-value pair returned is  
     * {@value com.resolve.gateway.GatewayConstants#METHOD_NOT_SUPPORTED_KEY}-true
     * indicating external system specific gateway does not support updating attributes of
     * object.
     * <p>
     * If external system specific gateway does support updating attributes of object then it 
     * must return updated {@link Map} of attribute name-value pairs.
     * 
     * @param  objType   Object type
     * @param  objId     Id of the object to update attributes of in external system
     * @param  updParams Key-value {@link Map} of object attributes to update
     * @param  userName  User name
     * @param  password  Password
     * @return           Key-value {@link Map} of the updated object attributes
     * @throws Exception If any error occurs in updating attributes of the object in 
     *                   external system
     */
    public static Map<String, String> updateObject(String objType, String objId, Map<String, String> updParams, String userName, String password) throws Exception
    {
        if (StringUtils.isBlank(objType) || updParams == null || updParams.isEmpty())
        {
            throw new Exception("Specified invalid parameters for updateObject.");
        }
        
        Map<String, String> updatedObjAttributes = new HashMap<String, String>();
        
        switch (objType)
        {
            case EventService.OBJECT_IDENTITY:
                updateEvent(objId, updParams, userName, password);
                updatedObjAttributes = readObject(objType, objId, null, userName, password);
                break;
                
            default:
                throw new Exception("Currently updateObject is supported for objects of type event only");
        }
        
        return updatedObjAttributes;
    }
    
    /**
     * Get list of objects from external system matching specified filter condition.
     * <p>
     * Default implementation returns {@link List} containing {@link Map} with 
     * {@value com.resolve.gateway.GatewayConstants#METHOD_NOT_SUPPORTED_KEY}-true indicating
     * external system specific gateway does not support getting objects matching filter condition.
     * <p>
     * If external system specific gateway does support getting objects matching filter condition then it 
     * must return {@link List} of {@link Map} of matching objects attribute id-value pairs.
     * 
     * @param  objType  Object type
     * @param  filter   External system gateway specific filer conditions
     * @param  userName User name
     * @param  password Password
     * @return          {@link List} of {@link Map}s of object attribute id-value pairs matching
     *                  filter condition
     * @throws Exception If any error occurs in getting object from external system
     */
    public static List<Map<String, String>> getObjects(String objType, BaseFilter filter, String userName, String password) throws Exception
    {
        if (StringUtils.isBlank(objType) || filter == null || StringUtils.isBlank(filter.getId()))
        {
            throw new Exception("Specified invalid parameters for getObjects.");
        }
        
        List<Map<String, String>> objs = new ArrayList<Map<String, String>>();
        
        switch (objType)
        {
            case EventService.OBJECT_IDENTITY:
                HPOMXFilter hpomxFilter = null;
                
                hpomxFilter = ((HPOMXGateway)instance).getDeployedFilter(filter.getId());
                                    
                if (hpomxFilter != null)
                {
                    if (HPOMXFilter.FILTERTYPE_ENUMERATION.equalsIgnoreCase(hpomxFilter.getFilterType()))
                    {
                        objs = searchEvent(hpomxFilter.getNativeQuery(), 
                                           ConfigReceiveHPOMX.DEFAULT_SUBSCRIPTION_RECORDS_TO_PULL, 
                                           userName, password);
                    }
                    else if (HPOMXFilter.FILTERTYPE_SUBSCRIPTION.equalsIgnoreCase(hpomxFilter.getFilterType()))
                    {
                        objs = subscribeEvents(hpomxFilter.getNativeQuery(), 
                                               ConfigReceiveHPOMX.DEFAULT_SUBSCRIPTION_RECORDS_TO_PULL,
                                               30l, 5, userName, password);
                    }
                }
                
                break;
                
            default:
                throw new Exception("Currently getObjects is supported for objects of type event only");
        }
                
        return objs;
    }
    
    /**
     * This method creates a new event in the HPOMX.
     *
     * <pre>
     * {@code
     *         import com.resolve.gateway.HPOMXAPI;
     *         import com.resolve.gateway.HPOMXConstants;
     *         ....
     *         ....
     *         try
     *         {
     *             String username = "hpomuser"; //if null then uses gateway configured value.
     *             String password = "hpompassword"; //if null then uses gateway configured value.
     *
     *             String nodeIPAddress = "123.123.123.123";
     *
     *             Map<String, String> params = new HashMap<String, String>();
     *
     *             params.put(HPOMXConstants.FIELD_TITLE, "My New Event.");
     *             params.put(HPOMXConstants.FIELD_DESCRIPTION, "My description.");
     *             //Could be one of Critical, Major, Minor, Normal, Warning and it's case sensitive.
     *             params.put(HPOMXConstants.FIELD_SEVERITY, "Major");
     *             //If there is any custom field then.
     *             params.put("Custom field 1", "Custom field 1 value");
     *             params.put("Custom field 2", "Custom field 2 value");
     *
     *             String eventId = HPOMXAPI.createEvent(nodeIPAddress, params, username, password);
     *             System.out.println("New event Id:" + eventId);
     *         }
     *         catch (Exception e)
     *         {
     *             e.printStackTrace();
     *             //Do something usefull with the execption.
     *         }
     * }
     * </pre>
     *
     * @param nodeIPAddress
     *            IP address or DNS host name to be used as the node reference
     *            in the event. If not provided uses the IP address of the
     *            server where RSRemote is running.
     * @param params
     *            is a {@link Map} with various keys (properties of the event)
     *            with their values. For the supported keys, refer to
     *            {@link HPOMXConstants}. Anything other than the listed ones
     *            will be treated as custom fields.
     *
     * @param username
     *            if not provided uses the gateway configuration.
     * @param password
     *            ties with the username. Gateway configuration is used if not
     *            provided.
     * @return the newly created event's Id.
     * @throws Exception
     */
    public static String createEvent(String nodeIPAddress, Map<String, String> params, String username, String password) throws Exception
    {
        return ((HPOMXGateway)instance).createEvent(nodeIPAddress, params, username, password);
    }
    
    /**
     * Queries the HPOMX events based on the provided query.
     *
     * <pre>
     * {@code
     *         import com.resolve.gateway.HPOMXAPI;
     *         import com.resolve.gateway.HPOMXConstants;
     *         ....
     *         ....
     *         try
     *         {
     *             String username = "hpomuser"; //if null then uses gateway configured value.
     *             String password = "hpompassword"; //if null then uses gateway configured value.
     *
     *             String query = "severity='Major' and title contains 'network'"; //HPOM is case-sensitive on severity
     *             int maxResult = 100;
     *
     *             List<Map<String, String>> result = HPOMXAPI.searchEvent(query, maxResult, username, password);
     *
     *             System.out.println("Total number of events returned: " + result.size());
     *             int i = 1;
     *             for(Map<String, String> map : result)
     *             {
     *                 System.out.printf("Event# %d\n", i++);
     *                 for(String key : map.keySet())
     *                 {
     *                     System.out.printf("%s = %s\n", key, map.get(key));
     *                 }
     *             }
     *         }
     *         catch (Exception e)
     *         {
     *             e.printStackTrace();
     *             //Do something usefull with the execption.
     *         }
     * }
     * </pre>
     *
     * @param query
     *            in the form severity = 'major' and title contains 'network'
     * @param maxResult
     *            to be retuened. -1 returns everything.
     * @param username
     *            if not provided uses the gateway configuration.
     * @param password
     *            ties with the username. Gateway configuration is used if not
     *            provided.
     * @return {@link List} of {@link Map} which contain properties for all
     *          events found.
     * @throws Exception
     */
    public static List<Map<String, String>> searchEvent(String query, int maxResult, String username, String password) throws Exception
    {
        return ((HPOMXGateway)instance).searchEvent(query, maxResult, username, password);
    }
    
    /**
     * Updates an existing event. HPOMX may place some restrictions on changing
     * the value for any arbitrary elements, for more information refer to the
     * HPOM documentation. However you can change Title and Severity using this
     * API. For severity the value must be one of these: - Normal - Warning -
     * nMinor - Major - Critical
     *
     * <pre>
     * {@code
     *         import com.resolve.gateway.HPOMXAPI;
     *         import com.resolve.gateway.HPOMXConstants;
     *         ....
     *         ....
     *         try
     *         {
     *             String username = "hpomuser"; //if null then uses gateway configured value.
     *             String password = "hpompassword"; //if null then uses gateway configured value.
     *
     *             String eventId = "2438be98-01c1-71e2-1fa2-0a1e0a130000";
     *
     *             Map<String, String> params = new HashMap<String, String>();
     *
     *             params.put(HPOMXConstants.FIELD_TITLE, "Changed title");
     *             params.put(HPOMXConstants.FIELD_SEVERITY, "Minor");
     *
     *             HPOMXAPI.updateEvent(eventId, params, username, password);
     *             System.out.println("Event updated sucessfully.");
     *         }
     *         catch (Exception e)
     *         {
     *             e.printStackTrace();
     *             //Do something usefull with the execption.
     *         }
     * }
     * </pre>
     *
     * @param eventId
     *            of the event to be updated.
     * @param params
     *            is a {@link Map} with various keys (properties of the event)
     *            with their values. For the supported keys, refer to
     *            {@link HPOMXConstants}. Anything other than the listed ones
     *            will be treated as custom fields.
     *
     * @param username
     *            if not provided uses the gateway configuration.
     * @param password
     *            ties with the username. Gateway configuration is used if not
     *            provided.
     * @throws Exception
     */
    public static void updateEvent(String eventId, Map<String, String> params, String username, String password) throws Exception
    {
        ((HPOMXGateway)instance).updateEvent(eventId, params, username, password);
    }
    
    /**
     * Subscribes HPOMX events based on the provided query and then pulls events from
     * subscription. 
     *
     * <pre>
     * {@code
     *         import com.resolve.gateway.HPOMXAPI;
     *         import com.resolve.gateway.HPOMXConstants;
     *         ....
     *         ....
     *         try
     *         {
     *             String username = "hpomuser"; //if null then uses gateway configured value.
     *             String password = "hpompassword"; //if null then uses gateway configured value.
     *
     *             String query = "severity='Major' and title contains 'network'"; //HPOM is case-sensitive on severity
     *             int maxResultsPerPull = 100;
     *             long pullOpTimeOutInSecs = 30l;
     *             int numOfPullOps = 5
     *
     *             List<Map<String, String>> result = 
     *                  HPOMXAPI.subscribeEvents(query, maxResultsPerPull, 
     *                                           pullOpTimeOutInSecs
     *                                           numOfPullOps, username, password);
     *
     *             System.out.println("Total number of events returned: " + result.size());
     *             int i = 1;
     *             for(Map<String, String> map : result)
     *             {
     *                 System.out.printf("Event# %d\n", i++);
     *                 for(String key : map.keySet())
     *                 {
     *                     System.out.printf("%s = %s\n", key, map.get(key));
     *                 }
     *             }
     *         }
     *         catch (Exception e)
     *         {
     *             e.printStackTrace();
     *             //Do something usefull with the execption.
     *         }
     * }
     * </pre>
     *
     * @param query
     *            in the form severity = 'major' and title contains 'network'
     * @param maxResult
     *            to be retuened. -1 returns everything.
     * @param username
     *            if not provided uses the gateway configuration.
     * @param password
     *            ties with the username. Gateway configuration is used if not
     *            provided.
     * @return {@link List} of {@link Map} which contain properties for all
     *          events found.
     * @throws Exception
     */
    public static List<Map<String, String>> subscribeEvents(String query, int maxResultsPerPull, long pullOpTimeOutInSecs, int numOfPullOps, String username, String password) throws Exception
    {
        return ((HPOMXGateway)instance).subscribeEvents(query, maxResultsPerPull, pullOpTimeOutInSecs, numOfPullOps, username, password);
    }
    
    /**
     * Gets the data for an existing event.
     *
     * <pre>
     * {@code
     *         import com.resolve.gateway.HPOMXAPI;
     *         import com.resolve.gateway.HPOMXConstants;
     *         ....
     *         ....
     *         try
     *         {
     *             String username = "hpomuser"; //if null then uses gateway configured value.
     *             String password = "hpompassword"; //if null then uses gateway configured value.
     *
     *             String eventId = "2438be98-01c1-71e2-1fa2-0a1e0a130000";
     *
     *             Map<String, String> result = HPOMXAPI.selectEventById(eventId, username, password);
     *
     *             System.out.println("Event details:");
     *             for(String key : result.keySet())
     *             {
     *                 System.out.printf("%s = %s\n", key, result.get(key));
     *             }
     *         }
     *         catch (Exception e)
     *         {
     *             e.printStackTrace();
     *             //Do something usefull with the execption.
     *         }
     * }
     * </pre>
     *
     * @param eventId
     *            to get data.
     * @param username
     *            if not provided uses the gateway configuration.
     * @param password
     *            ties with the username. Gateway configuration is used if not
     *            provided.
     * @throws Exception
     */
    public static Map<String, String> selectEventById(String eventId, String username, String password) throws Exception
    {
        return ((HPOMXGateway)instance).selectEventById(eventId, username, password);
    }
    
    /**
     * Deletes specified object from external system.
     * <p>
     * Default deleted attribute name-value pair returned is  
     * {@value com.resolve.gateway.GatewayConstants#METHOD_NOT_SUPPORTED_KEY}-true
     * indicating external system specific gateway does not support object deletion.
     * <p>
     * If external system specific gateway does support object deletion then it 
     * must return {@link Map} of object deletion operation reult.
     * 
     * @param  objType   Object type
     * @param  objId     Id of the object to delete from external system
     * @param  userName  User name
     * @param  password  Password
     * @return           Key-value {@link Map} result of object delete operation
     * @throws Exception If any error occurs in deleting object from external system
     */
    public static Map<String, String> deleteObject(String objType, String objId, String userName, String password) throws Exception
    {
        if (StringUtils.isBlank(objType) || StringUtils.isBlank(objId))
        {
            throw new Exception("Specified invalid parameters for deleteObject.");
        }
        
        Map<String, String> deleteObjectResuls = new HashMap<String, String>();
        deleteObjectResuls.put("Success", "true");
        
        switch (objType)
        {
            case EventService.OBJECT_IDENTITY:
                closeEvent(objId, userName, password);
                break;
                
            default:
                throw new Exception("Currently deleteObject is supported for objects of type event only");
        }
        
        return deleteObjectResuls;
    }
    
    /**
     * Closes an existing event.
     *
     * <pre>
     * {@code
     *         import com.resolve.gateway.HPOMXAPI;
     *         ....
     *         ....
     *         ....
     *         String eventId = "2438be98-01c1-71e2-1fa2-0a1e0a130000";
     *         ....
     *         try
     *         {
     *             String username = "hpomuser"; //if null then uses gateway configured value.
     *             String password = "hpompassword"; //if null then uses gateway configured value.
     *
     *             HPOMXAPI.closeEvent(eventId, username, password);
     *
     *             System.out.println("Closed Event with Id:" + eventId);
     *         }
     *         catch (Exception e)
     *         {
     *             System.out.println("Failed to closed Event with Id:" + eventId);
     *             e.printStackTrace();
     *             //Do something usefull with the execption.
     *         }
     * }
     * </pre>
     *
     * @param eventId
     *            Id of the event to close.
     * @param username
     *            if not provided uses the gateway configuration.
     * @param password
     *            ties with the username. Gateway configuration is used if not
     *            provided.
     * @throws Exception
     */
    public static void closeEvent(String eventId, String username, String password) throws Exception
    {
       ((HPOMXGateway)instance).closeEvent(eventId, username, password);
    }
    
    /**
     * Owns one or more event.
     *
     * <pre>
     * {@code
     *         import com.resolve.gateway.HPOMXAPI;
     *         ....
     *         ....
     *         ....
     *         String eventId1 = "2438be98-01c1-71e2-1fa2-0a1e0a130000";
     *         String eventId2 = "................";
     *         ....
     *         ....
     *         String eventIdn = "................";
     *         
     *         Collection<String> eventIds;
     *         
     *         eventIds.add(eventId1);
     *         eventIds.add(eventId2);
     *         ....
     *         ....
     *         eventIds.add(eventIdn);
     *         ....
     *         try
     *         {
     *             String username = "hpomuser"; //if null then uses gateway configured value.
     *             String password = "hpompassword"; //if null then uses gateway configured value.
     *
     *             HPOMXAPI.ownMany(eventIds, username, password);
     *
     *             System.out.println("Owned Events with event Ids:" + 
     *                                StringUtil.convertCollectionToString(eventIds.iterator(), ", "));
     *         }
     *         catch (Exception e)
     *         {
     *             System.out.println("Failed to take ownership of Events with events Ids:" + 
     *                                StringUtil.convertCollectionToString(eventIds.iterator(), ", "));
     *             e.printStackTrace();
     *             //Do something usefull with the execption.
     *         }
     * }
     * </pre>
     *
     * @param eventIds
     *            Collecetion of events Ids to take ownership of.
     * @param username
     *            if not provided uses the gateway configuration.
     * @param password
     *            ties with the username. Gateway configuration is used if not
     *            provided.
     * @throws Exception
     */
    public static void ownMany(Collection<String> eventIds, String username, String password) throws Exception
    {
        ((HPOMXGateway)instance).ownMany(eventIds, username, password);
    }
    
    /**
     * Disowns one or more event.
     *
     * <pre>
     * {@code
     *         import com.resolve.gateway.HPOMXAPI;
     *         ....
     *         ....
     *         ....
     *         String eventId1 = "2438be98-01c1-71e2-1fa2-0a1e0a130000";
     *         String eventId2 = "................";
     *         ....
     *         ....
     *         String eventIdn = "................";
     *         
     *         Collection<String> eventIds;
     *         
     *         eventIds.add(eventId1);
     *         eventIds.add(eventId2);
     *         ....
     *         ....
     *         eventIds.add(eventIdn);
     *         ....
     *         try
     *         {
     *             String username = "hpomuser"; //if null then uses gateway configured value.
     *             String password = "hpompassword"; //if null then uses gateway configured value.
     *
     *             HPOMXAPI.disownMany(eventIds, username, password);
     *
     *             System.out.println("Disowned Events with event Ids:" + 
     *                                StringUtil.convertCollectionToString(eventIds.iterator(), ", "));
     *         }
     *         catch (Exception e)
     *         {
     *             System.out.println("Failed to release ownership of Events with events Ids:" + 
     *                                StringUtil.convertCollectionToString(eventIds.iterator(), ", "));
     *             e.printStackTrace();
     *             //Do something usefull with the execption.
     *         }
     * }
     * </pre>
     *
     * @param eventId
     *            Collecetion of events Ids to release ownership of.
     * @param username
     *            if not provided uses the gateway configuration.
     * @param password
     *            ties with the username. Gateway configuration is used if not
     *            provided.
     * @throws Exception
     */
    public static void disownMany(Collection<String> eventIds, String username, String password) throws Exception
    {
        ((HPOMXGateway)instance).disownMany(eventIds, username, password);
    }
    
    /**
     * Add annotaion to an event.
     *
     * <pre>
     * {@code
     *         import com.resolve.gateway.HPOMXAPI;
     *         ....
     *         ....
     *         ....
     *         String eventId = "2438be98-01c1-71e2-1fa2-0a1e0a130000";
     *         String annotation = "This is test annotation";
     *         
     *         try
     *         {
     *             String username = "hpomuser"; //if null then uses gateway configured value.
     *             String password = "hpompassword"; //if null then uses gateway configured value.
     *
     *             String annotationId = HPOMXAPI.addAnnotation(eventId, annotation, username, password);
     *
     *             System.out.println("Annotation Id:" + annotationId);
     *         }
     *         catch (Exception e)
     *         {
     *             System.out.println("Failed to add annotation [" + annotation + 
     *                                "] to events with Id:" + eventId);
     *             e.printStackTrace();
     *             //Do something usefull with the execption.
     *         }
     * }
     * </pre>
     *
     * @param eventId
     *            Event Id of the event to add annotation to.
     * @param annotation
     *            Annotation text.
     * @param username
     *            if not provided uses the gateway configuration.
     * @param password
     *            ties with the username. Gateway configuration is used if not
     *            provided.
     * @return    Id of new annotation added to the event.
     * @throws Exception
     */
    public static String addAnnotation(String eventId, String annotation, String username, String password) throws Exception
    {
        return ((HPOMXGateway)instance).addAnnotation(eventId, annotation, username, password);
    }
    
    /**
     * Update annotaion of an event.
     *
     * <pre>
     * {@code
     *         import com.resolve.gateway.HPOMXAPI;
     *         ....
     *         ....
     *         ....
     *         String eventId = "2438be98-01c1-71e2-1fa2-0a1e0a130000";
     *         String annotationId = "89eb8342-1c10-2e17-2af1-000031a0e1a0";
     *         String annotationUpdate = "This is test annotation update";
     *         
     *         try
     *         {
     *             String username = "hpomuser"; //if null then uses gateway configured value.
     *             String password = "hpompassword"; //if null then uses gateway configured value.
     *
     *             HPOMXAPI.updateAnnotation(eventId, annotationId, annotationUpdate, username, password);
     *
     *             System.out.println("Successfully updated annotaion with id " + annotationId + " in event wih id " + eventId);
     *         }
     *         catch (Exception e)
     *         {
     *             System.out.println("Failed to update annotation id " + annotationId + 
     *                                " in event " + eventId + " to " + annotationUpdate);
     *             e.printStackTrace();
     *             //Do something usefull with the execption.
     *         }
     * }
     * </pre>
     *
     * @param eventId
     *            Event Id of the event to update annotation of.
     * @param annotationId
     *            Id of the annotation to update.
     * @param annotationUpdate
     *            Annotation text to update to.
     * @param username
     *            if not provided uses the gateway configuration.
     * @param password
     *            ties with the username. Gateway configuration is used if not
     *            provided.
     * @throws Exception
     */
    public static void updateAnnotation(String eventId, String annotationId, String annotationUpdate, String username, String password) throws Exception
    {
        ((HPOMXGateway)instance).updateAnnotation(eventId, annotationId, annotationUpdate, username, password);
    }
    
    /**
     * Delete annotaion of an event.
     *
     * <pre>
     * {@code
     *         import com.resolve.gateway.HPOMXAPI;
     *         ....
     *         ....
     *         ....
     *         String eventId = "2438be98-01c1-71e2-1fa2-0a1e0a130000";
     *         String annotationId = "89eb8342-1c10-2e17-2af1-000031a0e1a0";
     *         
     *         try
     *         {
     *             String username = "hpomuser"; //if null then uses gateway configured value.
     *             String password = "hpompassword"; //if null then uses gateway configured value.
     *
     *             HPOMXAPI.deleteAnnotation(eventId, annotationId, username, password);
     *
     *             System.out.println("Successfully deleted annotaion with id " + annotationId + " in event wih id " + eventId);
     *         }
     *         catch (Exception e)
     *         {
     *             System.out.println("Failed to delete annotation id " + annotationId + 
     *                                " in event " + eventId + " to " + annotationUpdate);
     *             e.printStackTrace();
     *             //Do something usefull with the execption.
     *         }
     * }
     * </pre>
     *
     * @param eventId
     *            Event Id of the event to delete annotation from.
     * @param annotationId
     *            Id of the annotation to delete.
     * @param username
     *            if not provided uses the gateway configuration.
     * @param password
     *            ties with the username. Gateway configuration is used if not
     *            provided.
     * @throws Exception
     */
    public static void deleteAnnotation(String eventId, String annotationId, String username, String password) throws Exception
    {
        ((HPOMXGateway)instance).deleteAnnotation(eventId, annotationId, username, password);
    }
    
    /**
     * Get annotaions of an event.
     *
     * <pre>
     * {@code
     *         import com.resolve.gateway.HPOMXAPI;
     *         import com.resolve.util.StringUtils;
     *         ....
     *         ....
     *         ....
     *         String eventId = "2438be98-01c1-71e2-1fa2-0a1e0a130000";
     *         
     *         try
     *         {
     *             String username = "hpomuser"; //if null then uses gateway configured value.
     *             String password = "hpompassword"; //if null then uses gateway configured value.
     *
     *             Collection<Map<String, Stirng>> annotations = 
     *                  HPOMXAPI.getAnnotations(eventId, username, password);
     *
     *             System.out.println("# of annotions in event wih id " + eventId + " is " + annotations.size());
     *             
     *             for(Map<String, String> annotation : annotations)
     *             {
     *                 System.out.println("Annotation [" + StringUtils.mapToLogString(annotation) + "]");
     *             }
     *         }
     *         catch (Exception e)
     *         {
     *             System.out.println("Failed to get annotations for event " + eventId);
     *             e.printStackTrace();
     *             //Do something usefull with the execption.
     *         }
     * }
     * </pre>
     *
     * @param eventId
     *            Event Id of the event to get annotations for.
     * @param username
     *            if not provided uses the gateway configuration.
     * @param password
     *            ties with the username. Gateway configuration is used if not
     *            provided.
     * @return    Collection of annotations.
     * @throws Exception
     */
    public static Collection<Map<String, String>> getAnnotations(String eventId, String username, String password) throws Exception
    {
        return ((HPOMXGateway)instance).getAnnotations(eventId, username, password);
    }
}


