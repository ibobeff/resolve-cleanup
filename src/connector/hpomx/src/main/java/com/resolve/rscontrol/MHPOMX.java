package com.resolve.rscontrol;

import com.resolve.persistence.model.HPOMXFilter;

public class MHPOMX extends MGateway {

    private static final String MODEL_NAME = HPOMXFilter.class.getSimpleName();

    protected String getModelName() {
        return MODEL_NAME;
    }
}

