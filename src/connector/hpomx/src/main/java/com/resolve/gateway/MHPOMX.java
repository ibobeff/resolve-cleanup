package com.resolve.gateway;

import java.util.Map;
import com.resolve.gateway.hpomx.HPOMXFilter;
import com.resolve.gateway.hpomx.HPOMXGateway;

public class MHPOMX extends MGateway {

    private static final String RSCONTROL_SET_FILTERS = "MHPOMX.setFilters";

    private static final HPOMXGateway instance = HPOMXGateway.getInstance();

    public MHPOMX() {
        super.instance = instance;
    }

    protected String getSetFilterMethodName() {
        return RSCONTROL_SET_FILTERS;
    }

    /**
     * Populates the {@link Filter} data into the given filterMap {@link Map} instance
     * 
     * @param filterMap
     *      the {@link Map} instance to hold the {@link Filter} data
     * @param filter
     *      a {@link HPOMXFilter} instance
     *            
     */
    public void populateGatewaySpecificFilterProperties(Map<String, String> filterMap, Filter filter) {
        if (instance.isActive()) {
            HPOMXFilter hpomxFilter = (HPOMXFilter) filter;
            filterMap.put(HPOMXFilter.QUERY, hpomxFilter.getQuery());
            filterMap.put(HPOMXFilter.OBJECT, hpomxFilter.getObject());
            filterMap.put(HPOMXFilter.FILTERTYPE, hpomxFilter.getFilterType());
        }
    }
}

