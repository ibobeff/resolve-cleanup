package com.resolve.persistence.model;

import java.util.List;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;
import javax.persistence.UniqueConstraint;
import com.resolve.services.interfaces.VO;
import com.resolve.services.hibernate.vo.HPOMXFilterVO;

@Entity
@Table(name = "hpomx_filter", uniqueConstraints = { @UniqueConstraint(columnNames = { "u_name", "u_queue" }) })
public class HPOMXFilter extends GatewayFilter<HPOMXFilterVO> {

    private static final long serialVersionUID = 1L;

    public HPOMXFilter() {
    }

    public HPOMXFilter(HPOMXFilterVO vo) {
        applyVOToModel(vo);
    }

    private String UQuery;

    private String UObject;

    private String UFilterType;

    @Column(name = "u_query", length = 256)
    public String getUQuery() {
        return this.UQuery;
    }

    public void setUQuery(String uQuery) {
        this.UQuery = uQuery;
    }

    @Column(name = "u_object", length = 256)
    public String getUObject() {
        return this.UObject;
    }

    public void setUObject(String uObject) {
        this.UObject = uObject;
    }

    @Column(name = "u_filtertype", length = 256)
    public String getUFilterType() {
        return this.UFilterType;
    }

    public void setUFilterType(String uFilterType) {
        this.UFilterType = uFilterType;
    }

    public HPOMXFilterVO doGetVO() {
        HPOMXFilterVO vo = new HPOMXFilterVO();
        super.doGetBaseVO(vo);
        vo.setUQuery(getUQuery());
        vo.setUObject(getUObject());
        vo.setUFilterType(getUFilterType());
        return vo;
    }

    @Override
    public void applyVOToModel(HPOMXFilterVO vo) {
        if (vo == null) return;
        super.applyVOToModel(vo);
        if (!VO.STRING_DEFAULT.equals(vo.getUQuery())) this.setUQuery(vo.getUQuery()); else ;
        if (!VO.STRING_DEFAULT.equals(vo.getUObject())) this.setUObject(vo.getUObject()); else ;
        if (!VO.STRING_DEFAULT.equals(vo.getUFilterType())) this.setUFilterType(vo.getUFilterType()); else ;
    }

    @Override
    public List<String> ugetCdataProperty() {
        List<String> list = super.ugetCdataProperty();
        list.add("UQuery");
        list.add("UObject");
        list.add("UFilterType");
        return list;
    }
}

