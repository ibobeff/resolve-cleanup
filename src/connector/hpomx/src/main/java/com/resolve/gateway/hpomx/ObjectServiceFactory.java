/******************************************************************************
* (C) Copyright 2014
*
* Resolve Systems, LLC
*
* All rights reserved
* This software is distributed under license and may not be used, copied,
* modified, or distributed without the express written permission of
* Resolve Systems, LLC.
*
******************************************************************************/

package com.resolve.gateway.hpomx;

public class ObjectServiceFactory
{
    public static ObjectService getObjectService(ConfigReceiveHPOMX configurations, String object)
    {
        if (object.equals(EventService.OBJECT_IDENTITY))
        {
            return new EventService(configurations);
        }
        else
        {
            throw new RuntimeException(object + " not implemented by HPOM Gateway yet.");
        }
    }
}
