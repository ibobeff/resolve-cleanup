package com.resolve.gateway.hpomx;

import com.resolve.gateway.BaseFilter;
import com.resolve.gateway.RetainValue;
import com.resolve.util.Log;
import com.resolve.util.StringUtils;

public class HPOMXFilter extends BaseFilter {

    public static final String QUERY = "QUERY";
    public static final String OBJECT = "OBJECT";
    public static final String FILTERTYPE = "FILTERTYPE";    
    public static final String FILTERTYPE_ENUMERATION = "enumeration";
    public static final String FILTERTYPE_SUBSCRIPTION = "subscription";    
    public static final String LAST_RECEIVE_DATE = "LASTRECEIVEDATE";
    public static final String SUBSCRIPTIONID = "SUBSCRIPTIONID";
    public static final String SUBSCRIPTIONCREATETIME = "SUBSCRIPTIONCREATETIME";
    
    private String query;
    private String object;
    private String filterType;
    private String lastReceiveDate;
    // Fields for subscription type filters
    private String subscriptionId;
    private long subscriptionCreateTime = 0l; // Create/Renewed timestamp in msec
    
    public HPOMXFilter(String id, String active, String order, String interval, String eventEventId, String runbook, String script, String query, String object, String filterType) {
        super(id, active, order, interval, eventEventId, runbook, script);
        this.query = query;
        this.object = object;
        this.filterType = filterType;
    }

    public String getQuery() {
        return this.query;
    }

    public void setQuery(String query) {
        this.query = query;
    }

    public String getObject() {
        return this.object;
    }

    public void setObject(String object) {
        this.object = object;
    }

    public String getFilterType() {
        return this.filterType;
    }

    public void setFilterType(String filterType) {
        this.filterType = filterType;
    }
    
    @RetainValue
    public String getLastReceiveDate()
    {
        return lastReceiveDate;
    }

    public void setLastReceiveDate(String lastReceiveDate)
    {
        this.lastReceiveDate = lastReceiveDate;
    }
    
    @RetainValue
    public String getSubscriptionId()
    {
        return subscriptionId;
    }
    
    public void setSubscriptionId(String subscriptionId)
    {
        this.subscriptionId = subscriptionId;
    }
    
    @RetainValue
    public long getSubscriptionCreateTime()
    {
        return subscriptionCreateTime;
    }
    
    public void setSubscriptionCreateTime(long subscriptionCreateTime)
    {
        this.subscriptionCreateTime = subscriptionCreateTime;
    }
    
    public void setSubscriptionCreateTime(String subscriptionCreateTimeTxt)
    {
        long subscriptionCreateTimeFromTxt = 0l;
        
        if (StringUtils.isNotBlank(subscriptionCreateTimeTxt))
        {               
            try
            {
                subscriptionCreateTimeFromTxt = Long.parseLong(subscriptionCreateTimeTxt);
                subscriptionCreateTime = subscriptionCreateTimeFromTxt;
            }
            catch (NumberFormatException nfe)
            {
                Log.log.warn("Parsing subscription create timestamp value of [" + 
                             subscriptionCreateTimeTxt + "] to long failed with error " + 
                             nfe.getMessage()); 
            }
        }
    }
}

