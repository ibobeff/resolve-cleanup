package com.resolve.services.hibernate.vo;

import com.resolve.util.Log;
import com.resolve.util.MappingAnnotation;
import com.resolve.util.StringUtils;

public class HPOMXFilterVO extends GatewayFilterVO
{
    private static final long serialVersionUID = 1L;
    
    private String UQuery;
    private String UObject;
    private String UFilterType;
    
    public HPOMXFilterVO()
    {
    }
    
    @Override
    public int hashCode()
    {
        final int prime = 31;
        int result = super.hashCode();
        result = prime * result + ((UObject == null) ? 0 : UObject.hashCode());
        result = prime * result + ((UQuery == null) ? 0 : UQuery.hashCode());
        result = prime * result + ((UFilterType == null) ? 0 : UFilterType.hashCode());
        return result;
    }
    
    @Override
    public boolean equals(Object obj)
    {
        if (this == obj) return true;
        if (!super.equals(obj)) return false;
        if (getClass() != obj.getClass()) return false;
        HPOMXFilterVO other = (HPOMXFilterVO) obj;
        
        Log.log.trace("[" + this + "] equals [" + other + "]");
        
        if (UObject == null)
        {
            if (StringUtils.isNotBlank(other.UObject)) return false;
        }
        else if (!UObject.trim().equals(other.UObject == null ? "" : other.UObject.trim())) return false;
        
        if (UQuery == null)
        {
            if (StringUtils.isNotBlank(other.UQuery)) return false;
        }
        else if (!UQuery.trim().equals(other.UQuery == null ? "" : other.UQuery.trim())) return false;
        
        if (UFilterType == null)
        {
            if (StringUtils.isNotBlank(other.UFilterType)) return false;
        }
        else if (!UFilterType.trim().equals(other.UFilterType == null ? "" : other.UFilterType.trim())) return false;
        
        return true;
    }
    
    @MappingAnnotation(columnName = "QUERY")
    public String getUQuery()
    {
        return this.UQuery;
    }

    public void setUQuery(String uQuery)
    {
        this.UQuery = uQuery;
    }

    @MappingAnnotation(columnName = "OBJECT")
    public String getUObject()
    {
        return this.UObject;
    }

    public void setUObject(String uObject)
    {
        this.UObject = uObject;
    }
    
    @MappingAnnotation(columnName = "FILTERTYPE")
    public String getUFilterType()
    {
        return this.UFilterType;
    }

    public void setUFilterType(String uFilterType)
    {
        this.UFilterType = uFilterType;
    }
    
    @Override
    public String toString()
    {
        StringBuilder sb = new StringBuilder();
        
        sb.append("UQuery = [" + (UQuery != null ? UQuery : "null") + "]\t");
        sb.append("UObject = [" + (UObject != null ? UObject : "null") + "]\t");
        sb.append("UFilterType = [" + (UFilterType != null ? UFilterType : "null") + "]\t");
        
        return sb.toString();
    }
}

