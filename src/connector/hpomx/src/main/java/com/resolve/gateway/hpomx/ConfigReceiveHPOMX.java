package com.resolve.gateway.hpomx;

import java.io.File;
import java.util.HashMap;
import java.util.Map;
import org.apache.commons.io.FileUtils;

import com.resolve.rsbase.MainBase;
import com.resolve.gateway.Filter;
import com.resolve.rsremote.ConfigReceiveGateway;
import com.resolve.util.Log;
import com.resolve.util.ObjectProperties;
import com.resolve.util.StringUtils;
import com.resolve.util.XDoc;

public class ConfigReceiveHPOMX extends ConfigReceiveGateway {
    
    private static final long serialVersionUID = 1L;

    private static final String RECEIVE_HPOMX_NODE = "./RECEIVE/HPOMX/";
    private static final String RECEIVE_HPOMX_FILTER = RECEIVE_HPOMX_NODE + "FILTER";
    private static final String RECEIVE_HPOMX_ATTR_SOCKET_TIMEOUT = RECEIVE_HPOMX_NODE + "@SOCKET_TIMEOUT";

    private static final String RECEIVE_HPOMX_ATTR_P_ASSWORD = RECEIVE_HPOMX_NODE + "@PASSWORD";
    private static final String RECEIVE_HPOMX_ATTR_STATUS_ACTIVE = RECEIVE_HPOMX_NODE + "@STATUS_ACTIVE";
    private static final String RECEIVE_HPOMX_ATTR_STATUS_PROCESSED = RECEIVE_HPOMX_NODE + "@STATUS_PROCESSED";
    private static final String RECEIVE_HPOMX_ATTR_STATUS_FIELDNAME = RECEIVE_HPOMX_NODE + "@STATUS_FIELDNAME";
    private static final String RECEIVE_HPOMX_ATTR_USERNAME = RECEIVE_HPOMX_NODE + "@USERNAME";
    private static final String RECEIVE_HPOMX_ATTR_RUNBOOKID_FIELDNAME = RECEIVE_HPOMX_NODE + "@RUNBOOKID_FIELDNAME";
    private static final String RECEIVE_HPOMX_ATTR_RUNBOOKID_ACTIVE = RECEIVE_HPOMX_NODE + "@RUNBOOKID_ACTIVE";
    private static final String RECEIVE_HPOMX_ATTR_URL = RECEIVE_HPOMX_NODE + "@URL";
	private static final String RECEIVE_HPOMX_ATTR_SUBSCRIPTION_EXPIRATION_DURATION_IN_MIN = RECEIVE_HPOMX_NODE + "@SUBSCRIPTION_EXPIRATION_DURATION_IN_MIN";
	private static final String RECEIVE_HPOMX_ATTR_SUBSCRIPTION_MAX_RECORDS_TO_PULL = RECEIVE_HPOMX_NODE + "@SUBSCRIPTION_MAX_RECORDS_TO_PULL";
    private static final int DEFAULT_SUBSCRIPTION_EXPIRATION_DURATION_IN_MIN = 60; // HPOM "SubscriptionExpiration" standard
    private static final int MAX_SUBSCRIPTION_EXPIRATION_DURATION_IN_MIN = 1440; // HPOM "SubscriptionExpirationMaximum" standard
	private static final int MIN_SUBSCRIPTION_RECORDS_TO_PULL = 1;
	public static final int DEFAULT_SUBSCRIPTION_RECORDS_TO_PULL = 100; // HPOM MaxItems Standard 
    private static final int MAX_SUBSCRIPTION_RECORDS_TO_PULL = 500; // HPOM MaxItemsMaximum Standard

    private String queue = "HPOMX";
    private int socket_timeout = 30;
    private String password = "";
    private boolean status_active = false;
    private int status_processed = 1;
    private String status_fieldname = "RESOLVE_STATUS";
    private String username = "";
    private String runbookid_fieldname = "RESOLVE_RUNBOOKID";
    private boolean runbookid_active = false;
    // http (default port 8080) or https (default port 8444) ://hpom_host:port/opr-webservice/
    private String url = "";
    private int subscription_expiration_duration_in_min = DEFAULT_SUBSCRIPTION_EXPIRATION_DURATION_IN_MIN;
	private int subscription_max_records_to_pull = DEFAULT_SUBSCRIPTION_RECORDS_TO_PULL;

    public String getQueue() {
        return queue;
    }

    public void setQueue(String queue) {
        this.queue = queue;
    }

    public int getSocket_timeout() {
        return this.socket_timeout;
    }

    public void setSocket_timeout(int socket_timeout) {
        this.socket_timeout = socket_timeout;
    }

    public String getPassword() {
        return this.password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public boolean getStatus_active() {
        return this.status_active;
    }

    public void setStatus_active(boolean status_active) {
        this.status_active = status_active;
    }

    public int getSubscription_expiration_duration_in_min() {
        return this.subscription_expiration_duration_in_min;
    }

    public void setSubscription_expiration_duration_in_min(int subscription_expiration_duration_in_min) {
        if (subscription_expiration_duration_in_min > 0 && subscription_expiration_duration_in_min <= MAX_SUBSCRIPTION_EXPIRATION_DURATION_IN_MIN)
        {
            this.subscription_expiration_duration_in_min = subscription_expiration_duration_in_min;
        }
    }

    public String getStatus_fieldname() {
        return this.status_fieldname;
    }

    public void setStatus_fieldname(String status_fieldname) {
        this.status_fieldname = status_fieldname;
    }

    public String getUsername() {
        return this.username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getRunbookid_fieldname() {
        return this.runbookid_fieldname;
    }

    public void setRunbookid_fieldname(String runbookid_fieldname) {
        this.runbookid_fieldname = runbookid_fieldname;
    }

    public boolean getRunbookid_active() {
        return this.runbookid_active;
    }

    public void setRunbookid_active(boolean runbookid_active) {
        this.runbookid_active = runbookid_active;
    }

    public String getUrl() {
        return this.url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public int getStatus_processed() {
        return this.status_processed;
    }

    public void setStatus_processed(int status_processed) {
        this.status_processed = status_processed;
    }
    
	public int getSubscription_max_records_to_pull() {
        return this.subscription_max_records_to_pull;
    }

    public void setSubscription_max_records_to_pull(int subscription_max_records_to_pull) {
		if (subscription_max_records_to_pull >= MIN_SUBSCRIPTION_RECORDS_TO_PULL &&
			subscription_max_records_to_pull <= MAX_SUBSCRIPTION_RECORDS_TO_PULL)
		{
        	this.subscription_max_records_to_pull = subscription_max_records_to_pull;
		}
    }

    public ConfigReceiveHPOMX(XDoc config) throws Exception {
        super(config);
        define("socket_timeout", INT, RECEIVE_HPOMX_ATTR_SOCKET_TIMEOUT);
        define("password", SECURE, RECEIVE_HPOMX_ATTR_P_ASSWORD);
        define("status_active", BOOLEAN, RECEIVE_HPOMX_ATTR_STATUS_ACTIVE);
        define("status_processed", INT, RECEIVE_HPOMX_ATTR_STATUS_PROCESSED);
        define("status_fieldname", STRING, RECEIVE_HPOMX_ATTR_STATUS_FIELDNAME);
        define("username", STRING, RECEIVE_HPOMX_ATTR_USERNAME);
        define("runbookid_fieldname", STRING, RECEIVE_HPOMX_ATTR_RUNBOOKID_FIELDNAME);
        define("runbookid_active", BOOLEAN, RECEIVE_HPOMX_ATTR_RUNBOOKID_ACTIVE);
        define("url", STRING, RECEIVE_HPOMX_ATTR_URL);
		define("subscription_expiration_duration_in_min", INT, 
			   RECEIVE_HPOMX_ATTR_SUBSCRIPTION_EXPIRATION_DURATION_IN_MIN);
		define("subscription_max_records_to_pull", INT, 
			   RECEIVE_HPOMX_ATTR_SUBSCRIPTION_MAX_RECORDS_TO_PULL);
    }

    @Override
    public String getRootNode() {
        return RECEIVE_HPOMX_NODE;
    }

    @SuppressWarnings("unchecked")
    @Override
    public void load() throws Exception {
        try {
            loadAttributes();
            if (isActive()) {
                HPOMXGateway hpomxGateway = HPOMXGateway.getInstance(this);
                filters = xdoc.getListMapValue(RECEIVE_HPOMX_FILTER);
                if (filters.size() > 0) {
                    for (Map<String, Object> values : filters) {
                        String filterName = ((String) values.get(HPOMXFilter.ID)).toLowerCase();
                        File scriptFile = getFile(MainBase.main.release.serviceName + "/config/hpomx/" + filterName + ".groovy");
                        try {
                            if (scriptFile.exists()) {
                                String groovy = FileUtils.readFileToString(scriptFile, "UTF-8");
                                if (StringUtils.isNotBlank(groovy)) {
                                    values.put(HPOMXFilter.SCRIPT, groovy);
                                    Log.log.debug("Loaded groovy script: " + scriptFile);
                                }
                            }
                            hpomxGateway.setFilter(values);
                        } catch (Exception e) {
                            Log.log.warn(e.getMessage() + ". Skipping file: " + scriptFile.getAbsolutePath());
                        }
                    }
                }
            }
        } catch (Throwable e) {
            setActive(false);
            Log.log.warn("Couldn not load configuration for HPOMX gateway, check blueprint. " + e.getMessage(), e);
        }
    }

    @SuppressWarnings({ "rawtypes", "unchecked" })
    @Override
    public void save() throws Exception {
        File dir = getFile(MainBase.main.release.serviceName + "/config/hpomx");
        if (!dir.exists()) {
            FileUtils.forceMkdir(dir);
        }
        if (filters != null && isActive()) {
            filters.clear();
            for (Filter filter : HPOMXGateway.getInstance().getFilters().values()) {
                HPOMXFilter hpomxFilter = (HPOMXFilter) filter;
                String groovy = hpomxFilter.getScript();
                String scriptFileName;
                File scriptFile = null;
                if (StringUtils.isNotBlank(groovy)) {
                    scriptFileName = hpomxFilter.getId().toLowerCase() + ".groovy";
                    scriptFile = getFile(MainBase.main.release.serviceName + "/config/hpomx/" + scriptFileName);
                }
                Map<String, Object> entry = new HashMap<String, Object>();
                putFilterDataIntoEntry(entry, hpomxFilter);
                filters.add(entry);
                if (scriptFile != null) {
                    try {
                        FileUtils.writeStringToFile(scriptFile, groovy, "UTF-8");
                    } catch (Exception e) {
                        Log.log.warn(e.getMessage() + ". Skipping file: " + scriptFile.getAbsolutePath());
                    }
                }
            }
            xdoc.setListMapValue(RECEIVE_HPOMX_FILTER, filters);
        }
        if (nameProperties != null && isActive()) {
            nameProperties.clear();
            for (String name : HPOMXGateway.getInstance().getNameProperties().keySet()) {
                Map<String, Object> entry = new HashMap<String, Object>();
                entry.put(NAME_KEY, name);
                nameProperties.add(entry);
                Map props = HPOMXGateway.getInstance().getNameProperties().getNameProperties(name);
                ObjectProperties prop = new ObjectProperties(props);
                prop.save(props);
            }
        }
        saveAttributes();
    }

    private void putFilterDataIntoEntry(Map<String, Object> entry, HPOMXFilter hpomxFilter) {
        entry.put(HPOMXFilter.ID, hpomxFilter.getId());
        entry.put(HPOMXFilter.ACTIVE, String.valueOf(hpomxFilter.isActive()));
        entry.put(HPOMXFilter.ORDER, String.valueOf(hpomxFilter.getOrder()));
        entry.put(HPOMXFilter.INTERVAL, String.valueOf(hpomxFilter.getInterval()));
        entry.put(HPOMXFilter.EVENT_EVENTID, hpomxFilter.getEventEventId());
        entry.put(HPOMXFilter.RUNBOOK, hpomxFilter.getRunbook());
        entry.put(HPOMXFilter.SCRIPT, hpomxFilter.getScript());
        entry.put(HPOMXFilter.QUERY, hpomxFilter.getQuery());
        entry.put(HPOMXFilter.OBJECT, hpomxFilter.getObject());
        entry.put(HPOMXFilter.FILTERTYPE, hpomxFilter.getFilterType());
        
        if (HPOMXFilter.FILTERTYPE_ENUMERATION.equalsIgnoreCase(hpomxFilter.getFilterType()))
        {
            if (StringUtils.isNotBlank(hpomxFilter.getLastReceiveDate()))
            {
                entry.put(HPOMXFilter.LAST_RECEIVE_DATE, hpomxFilter.getLastReceiveDate());
            }
        }
        
        if (HPOMXFilter.FILTERTYPE_SUBSCRIPTION.equalsIgnoreCase(hpomxFilter.getFilterType()))
        {
            if (StringUtils.isNotBlank(hpomxFilter.getSubscriptionId()))
            {
                entry.put(HPOMXFilter.SUBSCRIPTIONID, hpomxFilter.getSubscriptionId());
            }
            
            if (hpomxFilter.getSubscriptionCreateTime() != 0l)
            {
                entry.put(HPOMXFilter.SUBSCRIPTIONCREATETIME, String.valueOf(hpomxFilter.getSubscriptionCreateTime()));
            }
        }
    }
}

