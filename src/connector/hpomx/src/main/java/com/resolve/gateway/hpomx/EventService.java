/******************************************************************************
* (C) Copyright 2014
*
* Resolve Systems, LLC
*
* All rights reserved
* This software is distributed under license and may not be used, copied,
* modified, or distributed without the express written permission of
* Resolve Systems, LLC.
*
******************************************************************************/

package com.resolve.gateway.hpomx;

import java.math.BigInteger;
import java.net.InetAddress;
import java.rmi.RemoteException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.xml.bind.JAXBElement;
import javax.xml.datatype.DatatypeConfigurationException;
import javax.xml.datatype.DatatypeFactory;
import javax.xml.datatype.Duration;
import javax.xml.datatype.XMLGregorianCalendar;

import org.apache.axis2.AxisFault;
import org.apache.axis2.client.Options;
import org.apache.axis2.transport.http.HTTPConstants;
import org.apache.axis2.transport.http.HttpTransportProperties;
import org.apache.xerces.dom.ElementNSImpl;
import org.dmtf.schemas.wbem.wsman._1.wsman.AttributableDuration;
import org.dmtf.schemas.wbem.wsman._1.wsman.AttributableURI;
import org.dmtf.schemas.wbem.wsman._1.wsman.SelectorSetType;
import org.dmtf.schemas.wbem.wsman._1.wsman.SelectorType;
import org.xmlsoap.schemas.ws._2004._08.addressing.EndpointReferenceType;
import org.xmlsoap.schemas.ws._2004._08.eventing.DeliveryType;
import org.xmlsoap.schemas.ws._2004._08.eventing.Renew;
import org.xmlsoap.schemas.ws._2004._08.eventing.RenewResponse;
import org.xmlsoap.schemas.ws._2004._08.eventing.Subscribe;
import org.xmlsoap.schemas.ws._2004._08.eventing.SubscribeResponse;
import org.xmlsoap.schemas.ws._2004._08.eventing.Unsubscribe;
import org.xmlsoap.schemas.ws._2004._09.enumeration.Enumerate;
import org.xmlsoap.schemas.ws._2004._09.enumeration.EnumerateResponse;
import org.xmlsoap.schemas.ws._2004._09.enumeration.EnumerationContextType;
import org.xmlsoap.schemas.ws._2004._09.enumeration.ItemListType;
import org.xmlsoap.schemas.ws._2004._09.enumeration.Pull;
import org.xmlsoap.schemas.ws._2004._09.enumeration.PullResponse;

import com.hp.ov.opr.ws.client.axis.IncidentServiceStub;
import com.hp.schemas.ism.serviceoperation.incidentmanagement._1.incident.Incident;
import com.hp.schemas.ism.serviceoperation.incidentmanagement._1.incidentextensions.IncidentExtensionsType;
import com.hp.schemas.ism.servicetransition.configurationmanagement._1.configurationitem.ConfigurationItemPropertiesType;
import com.hp.schemas.ism.servicetransition.configurationmanagement._1.node.NodePropertiesType;
import com.hp.schemas.ism.servicetransition.configurationmanagement._1.node.NodeReferenceType;
import com.hp.schemas.opr.ws.serviceoperation.incidentmanagement._1.incident.Annotation;
import com.hp.schemas.opr.ws.serviceoperation.incidentmanagement._1.incident.Annotations;
import com.hp.schemas.opr.ws.serviceoperation.incidentmanagement._1.incident.IncidentIDs;
import com.hp.schemas.opr.ws.serviceoperation.incidentmanagement._1.incident.OperationsExtension;
import com.hp.schemas.opr.ws.serviceoperation.incidentmanagement._1.incident.UpdateAnnotation;
import com.hp.schemas.opr.ws.serviceoperation.incidentmanagement._1.incidentfilter.ConfigurationItemProperties;
import com.hp.schemas.opr.ws.serviceoperation.incidentmanagement._1.incidentfilter.CustomAttribute;
import com.hp.schemas.opr.ws.serviceoperation.incidentmanagement._1.incidentfilter.CustomAttributes;
import com.hp.schemas.opr.ws.serviceoperation.incidentmanagement._1.incidentfilter.DateOperators;
import com.hp.schemas.opr.ws.serviceoperation.incidentmanagement._1.incidentfilter.EmittingCI;
import com.hp.schemas.opr.ws.serviceoperation.incidentmanagement._1.incidentfilter.EmittingNode;
import com.hp.schemas.opr.ws.serviceoperation.incidentmanagement._1.incidentfilter.IncidentEnumerationFilter;
import com.hp.schemas.opr.ws.serviceoperation.incidentmanagement._1.incidentfilter.IncidentEventingFilter;
import com.hp.schemas.opr.ws.serviceoperation.incidentmanagement._1.incidentfilter.KeywordFilter;
import com.hp.schemas.opr.ws.serviceoperation.incidentmanagement._1.incidentfilter.KeywordOperators;
import com.hp.schemas.opr.ws.serviceoperation.incidentmanagement._1.incidentfilter.NodeProperties;
import com.hp.schemas.opr.ws.serviceoperation.incidentmanagement._1.incidentfilter.TimeFilter;
import com.resolve.util.Log;
import com.resolve.util.StringUtils;
import com.sun.org.apache.xerces.internal.jaxp.datatype.XMLGregorianCalendarImpl;

/**
 * This class acts as a service to manage event.
 */
public class EventService implements ObjectService
{
    // An incident can be uniquely identified via its ID
    private static final String INCIDENT_ID_SELECTOR_NAME = "IncidentID";
    private static final String FILTER_DIALECT_URI = "http://schemas.hp.com/opr/ws/ServiceOperation/IncidentManagement/1/IncidentFilter";
    private static final String INCIDENT_RESOURCE_URI = "http://schemas.hp.com/ism/ServiceOperation/IncidentManagement/1/Incident";
    private static final String PULL_DELIVERY_MODE = "http://schemas.dmtf.org/wbem/wsman/1/wsman/Pull";
    
    private static final String SUBSCRIPTION_ID_KEY = "SUBSCRIPTION_ID_KEY";
    private static final String ENUMERATION_CONTEXT_ID_KEY = "ENMERATION_CONTEXT_ID_KEY";
    private static final String PULL_READ_TIMEDOUT_EXCEPTION = "read timed out";
    
    static final String OBJECT_IDENTITY = "event";

    private static final String IDENTITY_PROPERTY = "IncidentID";
    
    private static final String HPOM_ANNOTATION_FIELD_AUTHOR = "Author";
    private static final String HPOM_ANNOTATION_FIELD_TEXT = "Text";
    private static final String HPOM_ANNOTATION_FIELD_DATE = "Date";
    private static final String HPOM_ANNOTATION_FIELD_ID = "ID";

    private final ConfigReceiveHPOMX configurations;

    // private final EventUtil eventUtil;

    public static final Map<String, String> defaultEventAttributes = new HashMap<String, String>();

    // the instance of the Service Stub that does the actual SOAP requests
    private IncidentServiceStub m_proxy = null;

    // the target URI of the WebService. For the OM Incident WS,
    // this is <hostname>:<port>/opr-webservice/Incident.svc
    private String m_targetURI = null;

    static
    {
        defaultEventAttributes.put(HPOMXConstants.FIELD_EVENT_ID.toLowerCase(), "string");
        defaultEventAttributes.put(HPOMXConstants.FIELD_AFFECTED_CI.toLowerCase(), "string");
        defaultEventAttributes.put(HPOMXConstants.FIELD_CATEGORY.toLowerCase(), "string");
        defaultEventAttributes.put(HPOMXConstants.FIELD_COLLABORATION_MODE.toLowerCase(), "string");
        defaultEventAttributes.put(HPOMXConstants.FIELD_DESCRIPTION.toLowerCase(), "string");
        defaultEventAttributes.put(HPOMXConstants.FIELD_LIFECYCLE_STATE.toLowerCase(), "string");
        defaultEventAttributes.put(HPOMXConstants.FIELD_PROBLEM_TYPE.toLowerCase(), "string");
        defaultEventAttributes.put(HPOMXConstants.FIELD_PRODUCT_TYPE.toLowerCase(), "string");
        defaultEventAttributes.put(HPOMXConstants.FIELD_SEVERITY.toLowerCase(), "string");
        defaultEventAttributes.put(HPOMXConstants.FIELD_SOLUTION.toLowerCase(), "string");
        defaultEventAttributes.put(HPOMXConstants.FIELD_SUB_CATEGORY.toLowerCase(), "string");
        defaultEventAttributes.put(HPOMXConstants.FIELD_TITLE.toLowerCase(), "string");
        defaultEventAttributes.put(HPOMXConstants.FIELD_TYPE.toLowerCase(), "string");
        defaultEventAttributes.put(HPOMXConstants.FIELD_ASSIGNED_OPERATOR.toLowerCase(), "string");
        defaultEventAttributes.put(HPOMXConstants.FIELD_CHANGE_TYPE.toLowerCase(), "string");
        defaultEventAttributes.put(HPOMXConstants.FIELD_ACKNOWLEDGE_CORRELATION_KEY.toLowerCase(), "string");
        defaultEventAttributes.put(HPOMXConstants.FIELD_APPLICATION.toLowerCase(), "string");
        defaultEventAttributes.put(HPOMXConstants.FIELD_CORRELATION_KEY.toLowerCase(), "string");
        defaultEventAttributes.put(HPOMXConstants.FIELD_OBJECT.toLowerCase(), "string");
        defaultEventAttributes.put(HPOMXConstants.FIELD_SOURCE.toLowerCase(), "string");
        defaultEventAttributes.put(HPOMXConstants.FIELD_ORIGINAL_EVENT.toLowerCase(), "string");
        defaultEventAttributes.put(HPOMXConstants.FIELD_RECEIVED_TIME.toLowerCase(), "xmlgregoriancalendar");
        defaultEventAttributes.put(HPOMXConstants.FIELD_STATE_CHANGE_TIME.toLowerCase(), "xmlgregoriancalendar");
        defaultEventAttributes.put(HPOMXConstants.FIELD_EMITTING_NODE_DNSNAME.toLowerCase(), "string");
        defaultEventAttributes.put(HPOMXConstants.FIELD_EMITTING_NODE_COREID.toLowerCase(), "string");
        defaultEventAttributes.put(HPOMXConstants.FIELD_CONDITION_MATCHED.toLowerCase(), "boolean");
        defaultEventAttributes.put(HPOMXConstants.FIELD_FORWARD_TO_TROUBLE_TICKET.toLowerCase(), "boolean");
        defaultEventAttributes.put(HPOMXConstants.FIELD_FORWARD_TO_NOTIFICATION.toLowerCase(), "boolean");
    }

    public EventService(ConfigReceiveHPOMX configurations)
    {
        this.configurations = configurations;
        try
        {
            m_targetURI = configurations.getUrl() + "/Incident.svc";
            Log.log.info("HPOM URI : [" + m_targetURI + "], username : [" + configurations.getUsername() + "], password : [" + configurations.getPassword() + "]");
            m_proxy = new IncidentServiceStub(m_targetURI, configurations.getUsername(), configurations.getPassword());
            m_proxy._getServiceClient().engageModule("addressing");
            m_proxy._getServiceClient().getOptions().setTimeOutInMilliSeconds(configurations.getSocket_timeout() * 1000);
        }
        catch (AxisFault e)
        {
            Log.log.error(e.getMessage(), e);
        }
    }

    public String getIdPropertyName()
    {
        return IDENTITY_PROPERTY;
    }

    public String getIdentity()
    {
        return OBJECT_IDENTITY;
    }

    /**
     * This class hold the tokens collectd from a query. This is required
     * because the HPOM doesn't have a formal query (like SQL) language. We
     * collect these tokens and build the an object that HPOM understands.
     */
    class QueryToken
    {
        // Supported operators for HPOM. Must be same with HPOMQueryTranslator
        final static String EQUAL = "|=|";
        final static String GREATER = "|>|";
        final static String GREATER_OR_EQUAL = "|>=|";
        final static String LESS_OR_EQUAL = "|<=|";
        final static String LESS = "|<|";
        final static String CONTAINS = "|CONTAINS|";
        final static String AND = "|&&|";

        private final String key;
        private final String operator;
        private final String value;

        QueryToken(String key, String operator, String value) throws Exception
        {
            if (key == null || operator == null || value == null)
            {
                throw new Exception("Key, Operator, and Value must not be null");
            }
            else
            {
                this.key = key.trim();
                this.operator = operator.trim();
                this.value = value.trim();
            }
        }

        public String getKey()
        {
            return key;
        }

        public String getOperator()
        {
            return operator;
        }

        public String getValue()
        {
            return value;
        }
    }

    // Splits the query item and build a QueryToken object.
    private QueryToken get(String queryItem) throws Exception
    {
        QueryToken result = null;
        if (queryItem.contains(QueryToken.EQUAL))
        {
            result = splitQueryItem(queryItem, QueryToken.EQUAL);
        }
        else if (queryItem.contains(QueryToken.GREATER))
        {
            result = splitQueryItem(queryItem, QueryToken.GREATER);
        }
        else if (queryItem.contains(QueryToken.GREATER_OR_EQUAL))
        {
            result = splitQueryItem(queryItem, QueryToken.GREATER_OR_EQUAL);
        }
        else if (queryItem.contains(QueryToken.LESS_OR_EQUAL))
        {
            result = splitQueryItem(queryItem, QueryToken.LESS_OR_EQUAL);
        }
        else if (queryItem.contains(QueryToken.LESS))
        {
            result = splitQueryItem(queryItem, QueryToken.LESS);
        }
        else if (queryItem.contains(QueryToken.CONTAINS))
        {
            result = splitQueryItem(queryItem, QueryToken.CONTAINS);
        }

        return result;
    }

    private QueryToken splitQueryItem(String queryItem, String delimeter) throws Exception
    {
        QueryToken result = null;
        String convertedDelem = java.util.regex.Pattern.quote(delimeter);

        String[] items = queryItem.split(convertedDelem);
        if (items.length == 2)
        {
            result = new QueryToken(items[0], delimeter, items[1]);
        }
        else
        {
            // This should never happen but just an extra protection.
            throw new Exception("Invalid query part: " + queryItem);
        }
        return result;
    }

    private IncidentEnumerationFilter prepareIncidentEnumerationFilter(String query) throws Exception
    {
        String andDelimeter = java.util.regex.Pattern.quote(QueryToken.AND);

        String[] queryItems = query.split(andDelimeter);
        List<EventService.QueryToken> queryTokens = new ArrayList<EventService.QueryToken>();
        for (String queryItem : queryItems)
        {
            queryTokens.add(get(queryItem));
        }

        IncidentEnumerationFilter enumFilter = new IncidentEnumerationFilter();
        CustomAttributes customAttributes = new CustomAttributes();

        for (EventService.QueryToken queryToken : queryTokens)
        {
            String originalKey = queryToken.getKey(); // May need for custom
                                                      // field
            String key = queryToken.getKey().toLowerCase();
            String operator = queryToken.getOperator();
            String value = queryToken.getValue();
            
            if (defaultEventAttributes.containsKey(key))
            {
                // Severity (added)
                if (key.equalsIgnoreCase(HPOMXConstants.FIELD_SEVERITY))
                {
                    enumFilter.getSeverity().add(value);
                }
                
                // EmittingNode
                if (key.equalsIgnoreCase(HPOMXConstants.FIELD_EMITTING_NODE_DNSNAME))
                {
                    NodeProperties nodeProperties = new NodeProperties();
                    nodeProperties.setDnsName(value);
                    EmittingNode emittingNode = new EmittingNode();
                    emittingNode.setNodeProperties(nodeProperties);
                    enumFilter.setEmittingNode(emittingNode);
                }
                
                // Category
                if (key.equalsIgnoreCase(HPOMXConstants.FIELD_CATEGORY))
                {
                    enumFilter.setCategory(value);
                }
                
                // Application
                if (key.equalsIgnoreCase(HPOMXConstants.FIELD_APPLICATION))
                {
                    enumFilter.setApplication(value);
                }
                
                // Object
                if (key.equalsIgnoreCase(HPOMXConstants.FIELD_OBJECT))
                {
                    enumFilter.setObject(value);
                }
                
                // EmittingCI (Emitting Core Id)           
                if (key.equalsIgnoreCase(HPOMXConstants.FIELD_EMITTING_NODE_COREID))
                {
                    ConfigurationItemProperties configItemProps = new ConfigurationItemProperties();
                    configItemProps.setID(value);
                    EmittingCI emittingCI = new EmittingCI();
                    emittingCI.setConfigurationItemProperties(configItemProps);
                    enumFilter.setEmittingCI(emittingCI);
                }
                
                // CorrelationKey
                if (key.equalsIgnoreCase(HPOMXConstants.FIELD_CORRELATION_KEY))
                {
                    enumFilter.setCorrelationKey(value);
                }
                
                // EscalationStatus
                if (key.equalsIgnoreCase(HPOMXConstants.FIELD_ESCALATION_STATUS))
                {
                    enumFilter.setEscalationStatus(value);
                }
                
                // ConditionMatched
                if (key.equalsIgnoreCase(HPOMXConstants.FIELD_CONDITION_MATCHED))
                {
                    enumFilter.setConditionMatched(Boolean.parseBoolean(value));
                }
                
                // ReceivedTime
                if (key.equalsIgnoreCase(HPOMXConstants.FIELD_RECEIVED_TIME))
                {
                    try
                    {
                        XMLGregorianCalendar cal = XMLGregorianCalendarImpl.parse(value);
                        TimeFilter timeFilter = new TimeFilter();
                        timeFilter.setValue(cal);
                        // Just the limitation on HPOM that both of this
                        // operators has to behave similar
                        if (operator.equals(QueryToken.LESS) || operator.equals(QueryToken.LESS_OR_EQUAL))
                        {
                            timeFilter.setComparator(DateOperators.TO);
                        }
                        else if (operator.equals(QueryToken.EQUAL))
                        {
                            timeFilter.setComparator(DateOperators.AT);
                        }
                        // Just the limitation on HPOM that both of this
                        // operators has to behave similar
                        else if (operator.equals(QueryToken.GREATER) || operator.equals(QueryToken.GREATER_OR_EQUAL))
                        {
                            timeFilter.setComparator(DateOperators.FROM);
                        }
                        enumFilter.setReceivedTime(timeFilter);
                    }
                    catch (Exception e)
                    {
                        Log.log.warn("Invalid value " + value + ", for the received time, ignoring it.", e);
                    }
                }
                
                // Title
                if (key.equalsIgnoreCase(HPOMXConstants.FIELD_TITLE))
                {
                    KeywordFilter keywordFilter = new KeywordFilter();
                    keywordFilter.setValue(value);
                    if (operator.equals(QueryToken.EQUAL))
                    {
                        keywordFilter.setComparator(KeywordOperators.EXACTLY);
                    }
                    else if (operator.equals(QueryToken.CONTAINS))
                    {
                        keywordFilter.setComparator(KeywordOperators.CONTAINS);
                    }
                    enumFilter.setTitle(keywordFilter);
                }
            }
            else
            // not in the fixed list, hence treat as custom.
            {
                CustomAttribute customAttribute = new CustomAttribute();
                customAttribute.setKey(originalKey);
                customAttribute.setText(value);
                // HPOM don't like null or empty string as field value, so we
                // cannot have a query
                // like severity='Warning' and ResolveStatus='null';
                /*
                 * if("null".equalsIgnoreCase(value)) {
                 * customAttribute.setText(""); } else {
                 * customAttribute.setText(value); }
                 */
                customAttributes.getCustomAttribute().add(customAttribute);
            }
        }

        if (customAttributes.getCustomAttribute().size() > 0)
        {
            enumFilter.setCustomAttributes(customAttributes);
        }

        return enumFilter;
    }
    
    private IncidentEventingFilter prepareIncidentEventingFilter(String query) throws Exception
    {
        String andDelimeter = java.util.regex.Pattern.quote(QueryToken.AND);

        String[] queryItems = query.split(andDelimeter);
        List<EventService.QueryToken> queryTokens = new ArrayList<EventService.QueryToken>();
        for (String queryItem : queryItems)
        {
            queryTokens.add(get(queryItem));
        }

        IncidentEventingFilter eventFilter = new IncidentEventingFilter();
        CustomAttributes customAttributes = new CustomAttributes();

        for (EventService.QueryToken queryToken : queryTokens)
        {
            String originalKey = queryToken.getKey(); // May need for custom
                                                      // field
            String key = queryToken.getKey().toLowerCase();
            String operator = queryToken.getOperator();
            String value = queryToken.getValue();
            
            if (defaultEventAttributes.containsKey(key))
            {
                // Severity (added)
                if (key.equalsIgnoreCase(HPOMXConstants.FIELD_SEVERITY))
                {
                    eventFilter.getSeverity().add(value);
                }
                
                // EmittingNode
                if (key.equalsIgnoreCase(HPOMXConstants.FIELD_EMITTING_NODE_DNSNAME))
                {
                    NodeProperties nodeProperties = new NodeProperties();
                    nodeProperties.setDnsName(value);
                    EmittingNode emittingNode = new EmittingNode();
                    emittingNode.setNodeProperties(nodeProperties);
                    eventFilter.setEmittingNode(emittingNode);
                }
                
                // Category
                if (key.equalsIgnoreCase(HPOMXConstants.FIELD_CATEGORY))
                {
                    eventFilter.setCategory(value);
                }
                
                // Application
                if (key.equalsIgnoreCase(HPOMXConstants.FIELD_APPLICATION))
                {
                    eventFilter.setApplication(value);
                }
                
                // Object
                if (key.equalsIgnoreCase(HPOMXConstants.FIELD_OBJECT))
                {
                    eventFilter.setObject(value);
                }
                
                // EmittingCI (Emitting Core Id)           
                if (key.equalsIgnoreCase(HPOMXConstants.FIELD_EMITTING_NODE_COREID))
                {
                    ConfigurationItemProperties configItemProps = new ConfigurationItemProperties();
                    configItemProps.setID(value);
                    EmittingCI emittingCI = new EmittingCI();
                    emittingCI.setConfigurationItemProperties(configItemProps);
                    eventFilter.setEmittingCI(emittingCI);
                }
                
                // CorrelationKey
                if (key.equalsIgnoreCase(HPOMXConstants.FIELD_CORRELATION_KEY))
                {
                    eventFilter.setCorrelationKey(value);
                }
                
                // Type
                if (key.equalsIgnoreCase(HPOMXConstants.FIELD_TYPE))
                {
                    eventFilter.setType(value);
                }
                
                // EscalationStatus
                if (key.equalsIgnoreCase(HPOMXConstants.FIELD_ESCALATION_STATUS))
                {
                    eventFilter.setEscalationStatus(value);
                }
                
                // ConditionMatched
                if (key.equalsIgnoreCase(HPOMXConstants.FIELD_CONDITION_MATCHED))
                {
                    eventFilter.setConditionMatched(Boolean.parseBoolean(value));
                }
                
                // ForwardToTroubleTicket
                if (key.equalsIgnoreCase(HPOMXConstants.FIELD_FORWARD_TO_TROUBLE_TICKET))
                {
                    eventFilter.setForwardToTroubleTicket(Boolean.parseBoolean(value));
                }
                
                // ForwardToNotificaion
                if (key.equalsIgnoreCase(HPOMXConstants.FIELD_FORWARD_TO_NOTIFICATION))
                {
                    eventFilter.setForwardToNotification(Boolean.parseBoolean(value));
                }
                
                // Title
                if (key.equalsIgnoreCase(HPOMXConstants.FIELD_TITLE))
                {
                    KeywordFilter keywordFilter = new KeywordFilter();
                    keywordFilter.setValue(value);
                    if (operator.equals(QueryToken.EQUAL))
                    {
                        keywordFilter.setComparator(KeywordOperators.EXACTLY);
                    }
                    else if (operator.equals(QueryToken.CONTAINS))
                    {
                        keywordFilter.setComparator(KeywordOperators.CONTAINS);
                    }
                    
                    eventFilter.setTitle(keywordFilter);
                }
                
                // ChangeType (add)
                if (key.equalsIgnoreCase(HPOMXConstants.FIELD_CHANGE_TYPE))
                {
                    eventFilter.getChangeType().add(value);
                }
            }
            else
            // not in the fixed list, hence treat as custom.
            {
                CustomAttribute customAttribute = new CustomAttribute();
                customAttribute.setKey(originalKey);
                customAttribute.setText(value);
                // HPOM don't like null or empty string as field value, so we
                // cannot have a query
                // like severity='Warning' and ResolveStatus='null';
                /*
                 * if("null".equalsIgnoreCase(value)) {
                 * customAttribute.setText(""); } else {
                 * customAttribute.setText(value); }
                 */
                customAttributes.getCustomAttribute().add(customAttribute);
            }
        }

        if (customAttributes.getCustomAttribute().size() > 0)
        {
            eventFilter.setCustomAttributes(customAttributes);
        }

        return eventFilter;
    }

    private static String extractIncidentIdFromEPR(EndpointReferenceType incidentEPR)
    {
        String incidentID = null;
        List<Object> anyList = incidentEPR.getReferenceParameters().getAny();
        for (Object obj : anyList)
        {
            if (obj instanceof ElementNSImpl)
            {
                ElementNSImpl objElem = (ElementNSImpl) obj;
                if (objElem.getLocalName().equals("SelectorSet"))
                {
                    incidentID = objElem.getFirstChild().getTextContent();
                }
            }
            else if (obj instanceof org.apache.xerces.dom.ElementNSImpl)
            {
                org.apache.xerces.dom.ElementNSImpl objElem = (org.apache.xerces.dom.ElementNSImpl) obj;
                if (objElem.getLocalName().equals("SelectorSet"))
                {
                    incidentID = objElem.getFirstChild().getTextContent();
                }
            }
        }
        return incidentID;
    }

    /**
     * create an OperationTimeout of 10 minutes (used by each operation method)
     * 
     * @return an OperationTimeout
     */
    private AttributableDuration setOperationTimeout()
    {
        AttributableDuration operationTimeout = new AttributableDuration();
        Duration timeoutDuration;
        try
        {
            timeoutDuration = DatatypeFactory.newInstance().newDurationDayTime(true, 0, 0, 10, 0);
            operationTimeout.setValue(timeoutDuration);
        }
        catch (DatatypeConfigurationException e)
        {
            Log.log.error(e.getMessage(), e);
        }
        return operationTimeout;
    }
    
    /**
     * create an OperationTimeout of specified milliseconds
     * 
     * @return an OperationTimeout
     */
    private AttributableDuration setOperationTimeout(long msecs)
    {
        long secs = msecs / 1000;
        
        int dSecs = (int)(secs % 60);
        int dMins = (int)((secs / 60) % 60);
        int dHours = (int) ((secs / 3600) % 24);
        int dDays = (int) ((secs / 86400) % 365);
        
        AttributableDuration operationTimeout = new AttributableDuration();
        Duration timeoutDuration;
        try
        {
            timeoutDuration = DatatypeFactory.newInstance().newDurationDayTime(true, dDays, dHours, dMins, dSecs);
            Log.log.debug("Setting WS Management Operation Timeout to " + timeoutDuration);
            operationTimeout.setValue(timeoutDuration);
        }
        catch (DatatypeConfigurationException e)
        {
            Log.log.error(e.getMessage(), e);
        }
        return operationTimeout;
    }
    
    /**
     * create a ResourceURI (used by each operation method)
     * 
     * @return a ResourceURI
     */
    private AttributableURI setResourceUri()
    {
        AttributableURI resourceUri = new AttributableURI();
        resourceUri.setValue(INCIDENT_RESOURCE_URI);
        return resourceUri;
    }

    /**
     * Method to call the get operation on a WebService server
     * 
     * @param incidentID
     *            ID of an incident, that should be retrieved
     * @return the Incident that was sent back from the server
     * @throws RemoteException
     */
    public Incident getEvent(String incidentId, IncidentServiceStub service) throws RemoteException
    {
        AttributableURI resourceUri = setResourceUri();
        AttributableDuration operationTimeout = setOperationTimeout();

        // SelectorSetType
        SelectorSetType selectorSet = new SelectorSetType();
        SelectorType incidentIdSelector = new SelectorType();
        incidentIdSelector.setName(INCIDENT_ID_SELECTOR_NAME);
        incidentIdSelector.getContent().add(incidentId);
        selectorSet.getSelector().add(incidentIdSelector);

        setBasicAuthentication(service);

        Incident incident = service.Get(resourceUri, selectorSet, operationTimeout);
        return incident;
    }

    @Override
    public String create(String nodeIPAddress, Map<String, String> params, String username, String password) throws Exception
    {
        IncidentServiceStub service = m_proxy;
        if (StringUtils.isNotEmpty(username) && StringUtils.isNotEmpty(password))
        {
            service = new IncidentServiceStub(m_targetURI, username, password);
            service._getServiceClient().engageModule("addressing");
            service._getServiceClient().getOptions().setTimeOutInMilliSeconds(configurations.getSocket_timeout() * 1000);
        }

        Incident incident = marshall(null, params);

        // The node reference is important otherwise HPOM won't create event.
        if (StringUtils.isEmpty(nodeIPAddress))
        {
            nodeIPAddress = InetAddress.getLocalHost().getHostAddress();
        }
        Log.log.debug("create NodeIPAddress : " + nodeIPAddress);
        NodeReferenceType nodeRef = new NodeReferenceType();
        NodePropertiesType nodeProperties = new NodePropertiesType();
        nodeProperties.setDnsName(nodeIPAddress);
        nodeRef.setNodeProperties(nodeProperties);
        incident.setEmittingNode(nodeRef);

        AttributableURI resourceUri = setResourceUri();
        AttributableDuration operationTimeout = setOperationTimeout();

        setBasicAuthentication(service);

        EndpointReferenceType epr = service.Create(incident, resourceUri, operationTimeout);
        String incidentID = extractIncidentIdFromEPR(epr);
        return incidentID;
    }

    private void setBasicAuthentication(IncidentServiceStub service)
    {
        // Setting basic auth
        Options options = service._getServiceClient().getOptions();
        HttpTransportProperties.Authenticator auth = new HttpTransportProperties.Authenticator();
        // auth.setPreemptiveAuthentication(true);
        auth.setUsername(configurations.getUsername());
        auth.setPassword(configurations.getPassword());
        // auth.setHost("10.30.10.19");
        // auth.setPort(8081);
        options.setProperty(HTTPConstants.AUTHENTICATE, auth);
        service._getServiceClient().setOptions(options);
    }

    @Override
    public void update(String incidentId, Map<String, String> params, String username, String password) throws Exception
    {
        try
        {
            IncidentServiceStub service = m_proxy;
            if (StringUtils.isNotEmpty(username) && StringUtils.isNotEmpty(password))
            {
                service = new IncidentServiceStub(m_targetURI, username, password);
                service._getServiceClient().engageModule("addressing");
	            service._getServiceClient().getOptions().setTimeOutInMilliSeconds(configurations.getSocket_timeout() * 1000);
            }

            Incident incident = getEvent(incidentId, service);
            if (incident == null)
            {
                throw new Exception("Invalid event id: " + incidentId);
            }
            else
            {
                incident = marshall(incident, params);
                AttributableURI resourceUri = setResourceUri();
                AttributableDuration operationTimeout = setOperationTimeout();

                // SelectorSetType
                SelectorSetType selectorSet = new SelectorSetType();
                SelectorType incidentIdSelector = new SelectorType();
                incidentIdSelector.setName(INCIDENT_ID_SELECTOR_NAME);
                incidentIdSelector.getContent().add(incident.getIncidentID());
                selectorSet.getSelector().add(incidentIdSelector);

                setBasicAuthentication(service);

                service.Put(incident, resourceUri, selectorSet, operationTimeout);

                com.hp.schemas.opr.ws.serviceoperation.incidentmanagement._1.incident.CustomAttributes attributes = marshallCustomAttribute(incident, params);
                if (attributes.getCustomAttribute() != null && attributes.getCustomAttribute().size() > 0)
                {
                    for (com.hp.schemas.opr.ws.serviceoperation.incidentmanagement._1.incident.CustomAttribute attribute : attributes.getCustomAttribute())
                    {
                        service.SetCustomAttribute(attribute, resourceUri, selectorSet, operationTimeout);
                        Log.log.debug("Custom attribute is updated(Key:" + attribute.getKey() + ", Value:" + attribute.getText());
                    }
                }
            }
        }
        catch (Exception e)
        {
            Log.log.error("Error updating the event with id: " + incidentId + ", actual error: " + e.getMessage(), e);
            throw e;
        }
    }

    @Override
    public void close(String incidentId, String username, String password) throws Exception
    {
        try
        {
            IncidentServiceStub service = m_proxy;
            if (StringUtils.isNotEmpty(username) && StringUtils.isNotEmpty(password))
            {
                service = new IncidentServiceStub(m_targetURI, username, password);
                service._getServiceClient().engageModule("addressing");
                service._getServiceClient().getOptions().setTimeOutInMilliSeconds(configurations.getSocket_timeout() * 1000);
            }

            AttributableURI resourceUri = setResourceUri();
            AttributableDuration operationTimeout = setOperationTimeout();

            // SelectorSetType
            SelectorSetType selectorSet = new SelectorSetType();
            SelectorType incidentIdSelector = new SelectorType();
            incidentIdSelector.setName(INCIDENT_ID_SELECTOR_NAME);
            incidentIdSelector.getContent().add(incidentId);
            selectorSet.getSelector().add(incidentIdSelector);

            setBasicAuthentication(service);

            service.Close(resourceUri, selectorSet, operationTimeout);
        }
        catch (Exception e)
        {
            Log.log.error("Error closing event with IncidentID: " + incidentId + ", actual error: " + e.getMessage(), e);
            throw e;
        }
    }

    @Override
    public Map<String, String> selectByEventId(String eventId, String username, String password) throws Exception
    {
        try
        {
            IncidentServiceStub service = m_proxy;
            if (StringUtils.isNotEmpty(username) && StringUtils.isNotEmpty(password))
            {
                service = new IncidentServiceStub(m_targetURI, username, password);
                service._getServiceClient().engageModule("addressing");
	            service._getServiceClient().getOptions().setTimeOutInMilliSeconds(configurations.getSocket_timeout() * 1000);
            }

            Incident incident = getEvent(eventId, service);
            return unmarshall(incident);
        }
        catch (Exception e)
        {
            Log.log.error("Error getting event data with event id: " + eventId + ", actual error: " + e.getMessage(), e);
            throw e;
        }
    }

    @Override
    public List<Map<String, String>> search(String query, int maxResult, String username, String password) throws Exception
    {
        try
        {
            IncidentServiceStub service = m_proxy;
            if (StringUtils.isNotEmpty(username) && StringUtils.isNotEmpty(password))
            {
                service = new IncidentServiceStub(m_targetURI, username, password);
                service._getServiceClient().engageModule("addressing");
	            service._getServiceClient().getOptions().setTimeOutInMilliSeconds(configurations.getSocket_timeout() * 1000);
            }

            Log.log.debug("HPOM Query: " + query);
            List<Map<String, String>> result = new ArrayList<Map<String, String>>();

            IncidentEnumerationFilter enumFilter = prepareIncidentEnumerationFilter(query);
            String enumerationContext = enumerateWithFilterOp(enumFilter, service);

            List<Incident> pulledIncidents = searchIncident(enumerationContext, BigInteger.valueOf(maxResult), service);
            for (Incident incident : pulledIncidents)
            {
                Map<String, String> incidentMap = unmarshall(incident);
                result.add(incidentMap);
            }

            return result;
        }
        catch (Exception e)
        {
            Log.log.error("Error searching event data with query: " + query + ", actual error: " + e.getMessage(), e);
            throw e;
        }
    }

    /**
     * Method to call the pull operation on a WebService server (used by
     * enumeration and eventing)
     * 
     * @param enumerationContext
     *            Context of the enumeration iterator
     * @param maxElements
     *            maximum number of elements that should be sent back
     * @return List of incidents
     * @throws RemoteException
     */
    private List<Incident> searchIncident(String enumerationContext, BigInteger maxElements, IncidentServiceStub service) throws RemoteException
    {
        List<Incident> incidents = new ArrayList<Incident>();

        // Pull
        Pull pull = new Pull();
        EnumerationContextType enumerationContextType = new EnumerationContextType();
        enumerationContextType.getContent().add(enumerationContext);

        Duration maxTimeDuration = null;
        try
        {
            maxTimeDuration = DatatypeFactory.newInstance().newDurationDayTime(true, 0, 0, 50, 0);
        }
        catch (DatatypeConfigurationException e)
        {
            e.printStackTrace();
        }

        pull.setEnumerationContext(enumerationContextType);
        pull.setMaxTime(maxTimeDuration);

        if (maxElements != null)
        {
            pull.setMaxElements(maxElements);
        }
        else
        {
            pull.setMaxElements(BigInteger.valueOf(100));
        }

        AttributableURI resourceUri = setResourceUri();
        AttributableDuration operationTimeout = setOperationTimeout();

        setBasicAuthentication(service);

        PullResponse pullResponse = service.PullOp(pull, resourceUri, operationTimeout);
        ItemListType itemListType = pullResponse.getItems();
        List<Object> itemList = itemListType.getAny();

        // after the pull, convert the returned items to Incidents
        for (Object itemObject : itemList)
        {
            try
            {
                javax.xml.bind.JAXBContext context = service.getContextMap().get(Incident.class);
                javax.xml.bind.Unmarshaller unmarshaller = context.createUnmarshaller();
                incidents.add(unmarshaller.unmarshal((ElementNSImpl) itemObject, Incident.class).getValue());
            }
            catch (javax.xml.bind.JAXBException bex)
            {
                throw org.apache.axis2.AxisFault.makeFault(bex);
            }
        }

        return incidents;
    }

    /**
     * Method to call the enumerate operation on a WebService server with a
     * filter
     * 
     * @param filter
     *            IncidentEnumerationFilter to filter for specific incidents
     * @return enumeration context ID, that can be used in subsequent pull
     *         operations
     * @throws RemoteException
     */
    private String enumerateWithFilterOp(IncidentEnumerationFilter filter, IncidentServiceStub service) throws RemoteException
    {
        if (service == null)
        {
            if (m_proxy == null || m_targetURI == null)
            {
                return null;
            }
            
            service = m_proxy;
        }
        
        AttributableURI resourceUri = setResourceUri();
        AttributableDuration operationTimeout = setOperationTimeout();
        
        setBasicAuthentication(service);

        // Enumerate
        Enumerate enumerate = new Enumerate();
        org.xmlsoap.schemas.ws._2004._09.enumeration.FilterType enumerationFilter = new org.xmlsoap.schemas.ws._2004._09.enumeration.FilterType();
        enumerationFilter.setDialect(FILTER_DIALECT_URI);
        // enumerationFilter.getContent().add(filter);
        enumerationFilter.getContent().add(new com.hp.schemas.opr.ws.serviceoperation.incidentmanagement._1.incidentfilter.ObjectFactory().createIncidentEnumerationFilter(filter));
        enumerate.setFilter(enumerationFilter);

        // SelectorSetType
        SelectorSetType selectorSet = null;

        EnumerateResponse enumerateResponse = m_proxy.EnumerateOp(enumerate, resourceUri, selectorSet, operationTimeout);
        String enumerationContext = (String) enumerateResponse.getEnumerationContext().getContent().get(0);
        return enumerationContext;
    }

    private Map<String, String> unmarshall(Incident inc)
    {
        Map<String, String> result = new HashMap<String, String>();

        result.put(HPOMXConstants.FIELD_EVENT_ID, inc.getIncidentID());
        if (inc.getAffectedCI() != null)
        {
            result.put("AffectedCI", inc.getAffectedCI().toString());
        }
        result.put(HPOMXConstants.FIELD_CATEGORY, inc.getCategory());
        result.put(HPOMXConstants.FIELD_COLLABORATION_MODE, inc.getCollaborationMode());
        result.put(HPOMXConstants.FIELD_DESCRIPTION, inc.getDescription());

        if (inc.getEmittingCI() != null)
        {
            ConfigurationItemPropertiesType configProperties = inc.getEmittingCI().getConfigurationItemProperties();

            // Map<String, String> configPropertiesMap = new HashMap<String,
            // String>();
            // configPropertiesMap.put("ID", configProperties.getID());
            // configPropertiesMap.put("Name", configProperties.getName());
            // result.put("EmittingCI_ID",
            // StringUtils.convertMapToString(configPropertiesMap));
            result.put(HPOMXConstants.FIELD_EMITTING_CI_ID, configProperties.getID());
            result.put(HPOMXConstants.FIELD_EMITTING_CI_NAME, configProperties.getName());
        }

        if (inc.getEmittingNode() != null)
        {
            NodePropertiesType nodeProperties = inc.getEmittingNode().getNodeProperties();

            /*
             * Map<String, String> nodePropertiesMap = new HashMap<String,
             * String>(); nodePropertiesMap.put("ShortHostname",
             * nodeProperties.getShortHostname());
             * nodePropertiesMap.put("PrimaryDnsDomainName",
             * nodeProperties.getPrimaryDnsDomainName());
             * nodePropertiesMap.put("NetworkAddress",
             * nodeProperties.getNetworkAddress());
             * nodePropertiesMap.put("DnsName", nodeProperties.getDnsName());
             * nodePropertiesMap.put("CoreId", nodeProperties.getCoreId());
             * 
             * result.put("EmittingNode",
             * StringUtils.convertMapToString(nodePropertiesMap));
             */
            result.put(HPOMXConstants.FIELD_EMITTING_NODE_SHORTHOSTNAME, nodeProperties.getShortHostname());
            result.put(HPOMXConstants.FIELD_EMITTING_NODE_PRIMARYDNSDOMAINNAME, nodeProperties.getPrimaryDnsDomainName());
            result.put(HPOMXConstants.FIELD_EMITTING_NODE_NETWORKADDRESS, nodeProperties.getNetworkAddress());
            result.put(HPOMXConstants.FIELD_EMITTING_NODE_DNSNAME, nodeProperties.getDnsName());
            result.put(HPOMXConstants.FIELD_EMITTING_NODE_COREID, nodeProperties.getCoreId());
        }

        result.put(HPOMXConstants.FIELD_LIFECYCLE_STATE, inc.getLifecycleState());
        result.put(HPOMXConstants.FIELD_PROBLEM_TYPE, inc.getProblemType());
        result.put(HPOMXConstants.FIELD_PRODUCT_TYPE, inc.getProductType());
        result.put(HPOMXConstants.FIELD_SEVERITY, inc.getSeverity());
        result.put(HPOMXConstants.FIELD_SOLUTION, inc.getSolution());
        result.put(HPOMXConstants.FIELD_SUB_CATEGORY, inc.getSubCategory());
        result.put(HPOMXConstants.FIELD_TITLE, inc.getTitle());
        result.put(HPOMXConstants.FIELD_TYPE, inc.getType());
        /*
         * if(inc.getAssignedOperator() != null) { AssignedOperator
         * assignedOperator = inc.getAssignedOperator(); Map<String, String>
         * assignedOperatorMap = new HashMap<String, String>();
         * assignedOperatorMap.put("",
         * assignedOperator.getEndpointReference().getServiceName().) }
         */
        /*
         * if(inc.getPrimaryAssignmentGroup() != null) { PrimaryAssignmentGroup
         * primaryAssignedGroup = inc.getPrimaryAssignmentGroup(); Map<String,
         * String> primaryAssignedGroupMap = new HashMap<String, String>(); }
         */

        if (inc.getExtensions() != null)
        {
            result.put(HPOMXConstants.FIELD_CHANGE_TYPE, inc.getExtensions().getChangeType());
            if (inc.getExtensions().getOperationsExtension() != null)
            {
                result.put(HPOMXConstants.FIELD_ACKNOWLEDGE_CORRELATION_KEY, inc.getExtensions().getOperationsExtension().getAcknowledgeCorrelationKey());
                result.put(HPOMXConstants.FIELD_APPLICATION, inc.getExtensions().getOperationsExtension().getApplication());
                result.put(HPOMXConstants.FIELD_CORRELATION_KEY, inc.getExtensions().getOperationsExtension().getCorrelationKey());
                result.put(HPOMXConstants.FIELD_OBJECT, inc.getExtensions().getOperationsExtension().getObject());
                result.put(HPOMXConstants.FIELD_ORIGINAL_EVENT, inc.getExtensions().getOperationsExtension().getOriginalEvent());
                result.put(HPOMXConstants.FIELD_SOURCE, inc.getExtensions().getOperationsExtension().getSource());
                if (inc.getExtensions().getOperationsExtension().getCreationTime() != null)
                {
                    result.put(HPOMXConstants.FIELD_CREATION_TYPE, inc.getExtensions().getOperationsExtension().getCreationTime().toXMLFormat());
                }
                if (inc.getExtensions().getOperationsExtension().getReceivedTime() != null)
                {
                    result.put(HPOMXConstants.FIELD_RECEIVED_TIME, inc.getExtensions().getOperationsExtension().getReceivedTime().toXMLFormat());
                }
                if (inc.getExtensions().getOperationsExtension().getStateChangeTime() != null)
                {
                    result.put(HPOMXConstants.FIELD_STATE_CHANGE_TIME, inc.getExtensions().getOperationsExtension().getStateChangeTime().toXMLFormat());
                }

                if (inc.getExtensions().getOperationsExtension().getCustomAttributes() != null)
                {
                    if (inc.getExtensions().getOperationsExtension().getCustomAttributes().getCustomAttribute() != null && inc.getExtensions().getOperationsExtension().getCustomAttributes().getCustomAttribute().size() > 0)
                    {
                        List<com.hp.schemas.opr.ws.serviceoperation.incidentmanagement._1.incident.CustomAttribute> attributes = inc.getExtensions().getOperationsExtension().getCustomAttributes().getCustomAttribute();
                        for (com.hp.schemas.opr.ws.serviceoperation.incidentmanagement._1.incident.CustomAttribute attribute : attributes)
                        {
                            result.put(attribute.getKey(), attribute.getText());
                        }
                    }
                }
            }
        }
        return result;
    }

    private Incident marshall(Incident incident, Map<String, String> incidentMap)
    {
        boolean isNew = false;
        Incident updatedIncident = null;
        
        if (incident == null)
        {
            incident = new Incident();
            isNew = true;
        }
        else
        {
            updatedIncident = new Incident();
        }
        
        // incident.setAffectedCI(value);
        // incident.setAssignedOperator(value);
        if (incidentMap.containsKey(HPOMXConstants.FIELD_EVENT_ID))
        {
            if (isNew)
            {
                incident.setIncidentID(incidentMap.get(HPOMXConstants.FIELD_EVENT_ID));
            }
        }
        
        if (!isNew)
        {
            updatedIncident.setIncidentID(incident.getIncidentID());
        }
        
        if (incidentMap.containsKey(HPOMXConstants.FIELD_CATEGORY) && StringUtils.isNotBlank(incidentMap.get(HPOMXConstants.FIELD_CATEGORY)))
        {
            if (isNew)
            {
                incident.setCategory(incidentMap.get(HPOMXConstants.FIELD_CATEGORY));
            }
            else
            {
                updatedIncident.setCategory(incidentMap.get(HPOMXConstants.FIELD_CATEGORY));
            }
        }
        else
        {
            if (!isNew)
            {
                updatedIncident.setCategory(incident.getCategory());
            }
        }
        
        if (incidentMap.containsKey(HPOMXConstants.FIELD_COLLABORATION_MODE) && StringUtils.isNotBlank(incidentMap.get(HPOMXConstants.FIELD_COLLABORATION_MODE)))
        {
            if (isNew)
            {
                incident.setCollaborationMode(incidentMap.get(HPOMXConstants.FIELD_COLLABORATION_MODE));
            }
            else
            {
                updatedIncident.setCollaborationMode(incidentMap.get(HPOMXConstants.FIELD_COLLABORATION_MODE));
            }
        }
        else
        {
            if (!isNew)
            {
                updatedIncident.setCollaborationMode(incident.getCollaborationMode());
            }
        }
        
        if (incidentMap.containsKey(HPOMXConstants.FIELD_DESCRIPTION) && StringUtils.isNotBlank(incidentMap.get(HPOMXConstants.FIELD_DESCRIPTION)))
        {
            if (isNew)
            {
                incident.setDescription(incidentMap.get(HPOMXConstants.FIELD_DESCRIPTION));
            }
            else
            {
                updatedIncident.setDescription(incidentMap.get(HPOMXConstants.FIELD_DESCRIPTION));
            }
        }
        else
        {
            if (!isNew)
            {
                updatedIncident.setDescription(incident.getDescription());
            }
        }

        if (incidentMap.containsKey(HPOMXConstants.FIELD_LIFECYCLE_STATE) && StringUtils.isNotBlank(incidentMap.get(HPOMXConstants.FIELD_LIFECYCLE_STATE)))
        {
            if (isNew)
            {
                incident.setLifecycleState(incidentMap.get(HPOMXConstants.FIELD_LIFECYCLE_STATE));
            }
            else
            {
                updatedIncident.setLifecycleState(incidentMap.get(HPOMXConstants.FIELD_LIFECYCLE_STATE));
            }
        }
        else
        {
            if (!isNew)
            {
                updatedIncident.setLifecycleState(incident.getLifecycleState());
            }
        }
        
        if (incidentMap.containsKey(HPOMXConstants.FIELD_PROBLEM_TYPE) && StringUtils.isNotBlank(incidentMap.get(HPOMXConstants.FIELD_PROBLEM_TYPE)))
        {
            if (isNew)
            {
                incident.setProblemType(incidentMap.get(HPOMXConstants.FIELD_PROBLEM_TYPE));
            }
            else
            {
                updatedIncident.setProblemType(incidentMap.get(HPOMXConstants.FIELD_PROBLEM_TYPE));
            }
        }
        else
        {
            if (!isNew)
            {
                updatedIncident.setProblemType(incident.getProblemType());
            }
        }
        
        if (incidentMap.containsKey(HPOMXConstants.FIELD_PRODUCT_TYPE) && StringUtils.isNotBlank(incidentMap.get(HPOMXConstants.FIELD_PRODUCT_TYPE)))
        {
            if (isNew)
            {
                incident.setProductType(incidentMap.get(HPOMXConstants.FIELD_PRODUCT_TYPE));
            }
            else
            {
                updatedIncident.setProductType(incidentMap.get(HPOMXConstants.FIELD_PRODUCT_TYPE));
            }
        }
        else
        {
            if (!isNew)
            {
                updatedIncident.setProductType(incident.getProductType());
            }
        }
        
        if (incidentMap.containsKey(HPOMXConstants.FIELD_SEVERITY) && StringUtils.isNotBlank(incidentMap.get(HPOMXConstants.FIELD_SEVERITY)))
        {
            if (isNew)
            {
                incident.setSeverity(incidentMap.get(HPOMXConstants.FIELD_SEVERITY));
            }
            else
            {
                updatedIncident.setSeverity(incidentMap.get(HPOMXConstants.FIELD_SEVERITY));
            }
        }
        else
        {
            if (!isNew)
            {
                updatedIncident.setSeverity(incident.getSeverity());
            }
        }
        
        if (incidentMap.containsKey(HPOMXConstants.FIELD_SOLUTION) && StringUtils.isNotBlank(incidentMap.get(HPOMXConstants.FIELD_SOLUTION)))
        {
            if (isNew)
            {
                incident.setSolution(incidentMap.get(HPOMXConstants.FIELD_SOLUTION));
            }
            else
            {
                updatedIncident.setSolution(incidentMap.get(HPOMXConstants.FIELD_SOLUTION));
            }
        }
        else
        {
            if (!isNew)
            {
                updatedIncident.setSolution(incident.getSolution());
            }
        }
        
        if (incidentMap.containsKey(HPOMXConstants.FIELD_SUB_CATEGORY) && StringUtils.isNotBlank(incidentMap.get(HPOMXConstants.FIELD_SOLUTION)))
        {
            if (isNew)
            {
                incident.setSubCategory(incidentMap.get(HPOMXConstants.FIELD_SUB_CATEGORY));
            }
            else
            {
                updatedIncident.setSubCategory(incidentMap.get(HPOMXConstants.FIELD_SUB_CATEGORY));
            }
        }
        else
        {
            if (!isNew)
            {
                updatedIncident.setSubCategory(incident.getSubCategory());
            }
        }
        
        if (incidentMap.containsKey(HPOMXConstants.FIELD_TITLE) && StringUtils.isNotBlank(incidentMap.get(HPOMXConstants.FIELD_TITLE)))
        {
            if (isNew)
            {
                incident.setTitle(incidentMap.get(HPOMXConstants.FIELD_TITLE));
            }
            else
            {
                updatedIncident.setTitle(incidentMap.get(HPOMXConstants.FIELD_TITLE));
            }
        }
        else
        {
            if (!isNew)
            {
                updatedIncident.setTitle(incident.getTitle());
            }
        }
        
        if (incidentMap.containsKey(HPOMXConstants.FIELD_TYPE) && StringUtils.isNotBlank(incidentMap.get(HPOMXConstants.FIELD_TYPE)))
        {
            if (isNew)
            {
                incident.setType(incidentMap.get(HPOMXConstants.FIELD_TYPE));
            }
            else
            {
                updatedIncident.setType(incidentMap.get(HPOMXConstants.FIELD_TYPE));
            }
        }
        else
        {
            if (!isNew)
            {
                updatedIncident.setType(incident.getType());
            }
        }
        
        IncidentExtensionsType updateExtensions = null;
        OperationsExtension updateExtension = null;
        
        IncidentExtensionsType extensions = null;
        OperationsExtension extension = null;
        
        if (isNew)
        {
            extensions = new IncidentExtensionsType();
            extension = new OperationsExtension();
        }
        else
        {
            updateExtensions = new IncidentExtensionsType();
            updateExtension = new OperationsExtension();
        }
        
        if (incidentMap.containsKey(HPOMXConstants.FIELD_ACKNOWLEDGE_CORRELATION_KEY) && StringUtils.isNotBlank(incidentMap.get(HPOMXConstants.FIELD_ACKNOWLEDGE_CORRELATION_KEY)))
        {
            if (isNew)
            {
                extension.setAcknowledgeCorrelationKey(incidentMap.get(HPOMXConstants.FIELD_ACKNOWLEDGE_CORRELATION_KEY));
            }
            else
            {
                updateExtension.setAcknowledgeCorrelationKey(incidentMap.get(HPOMXConstants.FIELD_ACKNOWLEDGE_CORRELATION_KEY));
            }
        }
        else
        {
            if (!isNew && incident.getExtensions() != null && incident.getExtensions().getOperationsExtension() != null)
            {
                updateExtension.setAcknowledgeCorrelationKey(incident.getExtensions().getOperationsExtension().getAcknowledgeCorrelationKey());
            }
        }
        
        if (incidentMap.containsKey(HPOMXConstants.FIELD_APPLICATION) && StringUtils.isNotBlank(incidentMap.get(HPOMXConstants.FIELD_APPLICATION)))
        {
            if (isNew)
            {
                extension.setApplication(incidentMap.get(HPOMXConstants.FIELD_APPLICATION));
            }
            else
            {
                updateExtension.setApplication(incidentMap.get(HPOMXConstants.FIELD_APPLICATION));
            }
        }
        else
        {
            if (!isNew && incident.getExtensions() != null && incident.getExtensions().getOperationsExtension() != null)
            {
                updateExtension.setApplication(incident.getExtensions().getOperationsExtension().getApplication());
            }
        }
        
        if (incidentMap.containsKey(HPOMXConstants.FIELD_CORRELATION_KEY) && StringUtils.isNotBlank(incidentMap.get(HPOMXConstants.FIELD_CORRELATION_KEY)))
        {
            if (isNew)
            {
                extension.setCorrelationKey(incidentMap.get(HPOMXConstants.FIELD_CORRELATION_KEY));
            }
            else
            {
                updateExtension.setCorrelationKey(incidentMap.get(HPOMXConstants.FIELD_CORRELATION_KEY));
            }
        }
        else
        {
            if (!isNew && incident.getExtensions() != null && incident.getExtensions().getOperationsExtension() != null)
            {
                updateExtension.setCorrelationKey(incident.getExtensions().getOperationsExtension().getCorrelationKey());
            }
        }
        
        if (incidentMap.containsKey(HPOMXConstants.FIELD_OBJECT) && StringUtils.isNotBlank(incidentMap.get(HPOMXConstants.FIELD_OBJECT)))
        {
            if (isNew)
            {
                extension.setObject(incidentMap.get(HPOMXConstants.FIELD_OBJECT));
            }
            else
            {
                updateExtension.setObject(incidentMap.get(HPOMXConstants.FIELD_OBJECT));
            }
        }
        else
        {
            if (!isNew && incident.getExtensions() != null && incident.getExtensions().getOperationsExtension() != null)
            {
                updateExtension.setObject(incident.getExtensions().getOperationsExtension().getObject());
            }
        }
        
        if (incidentMap.containsKey(HPOMXConstants.FIELD_SOURCE) && StringUtils.isNotBlank(incidentMap.get(HPOMXConstants.FIELD_SOURCE)))
        {
            if (isNew)
            {
                extension.setSource(incidentMap.get(HPOMXConstants.FIELD_SOURCE));
            }
            else
            {
                updateExtension.setSource(incidentMap.get(HPOMXConstants.FIELD_SOURCE));
            }
        }
        else
        {
            if (!isNew && incident.getExtensions() != null && incident.getExtensions().getOperationsExtension() != null)
            {
                updateExtension.setSource(incident.getExtensions().getOperationsExtension().getSource());
            }
        }
        
        if (incidentMap.containsKey(HPOMXConstants.FIELD_ORIGINAL_EVENT) && StringUtils.isNotBlank(incidentMap.get(HPOMXConstants.FIELD_ORIGINAL_EVENT)))
        {
            if (isNew)
            {
                extension.setOriginalEvent(incidentMap.get(HPOMXConstants.FIELD_ORIGINAL_EVENT));
            }
            else
            {
                updateExtension.setOriginalEvent(incidentMap.get(HPOMXConstants.FIELD_ORIGINAL_EVENT));
            }
        }
        else
        {
            if (!isNew && incident.getExtensions() != null && incident.getExtensions().getOperationsExtension() != null)
            {
                updateExtension.setOriginalEvent(incident.getExtensions().getOperationsExtension().getOriginalEvent());
            }
        }
        
        if (incidentMap.containsKey(HPOMXConstants.FIELD_RECEIVED_TIME) && StringUtils.isNotBlank(incidentMap.get(HPOMXConstants.FIELD_RECEIVED_TIME)))
        {
            try
            {
                if (isNew)
                {
                    extension.setReceivedTime(XMLGregorianCalendarImpl.parse(incidentMap.get(HPOMXConstants.FIELD_RECEIVED_TIME)));
                }
                else
                {
                    updateExtension.setReceivedTime(XMLGregorianCalendarImpl.parse(incidentMap.get(HPOMXConstants.FIELD_RECEIVED_TIME)));
                }
            }
            catch (Exception e)
            {
                Log.log.warn("Ignoring the invalid value for XMLGregorianCalendar : " + incidentMap.get(HPOMXConstants.FIELD_RECEIVED_TIME));
            }
        }
        else
        {
            if (!isNew && incident.getExtensions() != null && incident.getExtensions().getOperationsExtension() != null)
            {
                updateExtension.setReceivedTime(incident.getExtensions().getOperationsExtension().getReceivedTime());
            }
        }
        
        if (incidentMap.containsKey(HPOMXConstants.FIELD_STATE_CHANGE_TIME) && StringUtils.isNotBlank(incidentMap.get(HPOMXConstants.FIELD_STATE_CHANGE_TIME)))
        {
            try
            {
                if (isNew)
                {
                    extension.setStateChangeTime(XMLGregorianCalendarImpl.parse(incidentMap.get(HPOMXConstants.FIELD_STATE_CHANGE_TIME)));
                }
                else
                {
                    updateExtension.setStateChangeTime(XMLGregorianCalendarImpl.parse(incidentMap.get(HPOMXConstants.FIELD_STATE_CHANGE_TIME)));
                }
            }
            catch (Exception e)
            {
                Log.log.warn("Ignoring the invalid value for XMLGregorianCalendar : " + incidentMap.get(HPOMXConstants.FIELD_STATE_CHANGE_TIME));
            }
        }
        else
        {
            if (!isNew && incident.getExtensions() != null && incident.getExtensions().getOperationsExtension() != null)
            {
                updateExtension.setStateChangeTime(incident.getExtensions().getOperationsExtension().getStateChangeTime());
            }
        }

        if (incidentMap.containsKey(HPOMXConstants.FIELD_APPLICATION) && StringUtils.isNotBlank(incidentMap.get(HPOMXConstants.FIELD_APPLICATION)))
        {
            if (isNew)
            {
                extension.setAcknowledgeCorrelationKey(incidentMap.get(HPOMXConstants.FIELD_APPLICATION));
            }
            else
            {
                updateExtension.setAcknowledgeCorrelationKey(incidentMap.get(HPOMXConstants.FIELD_APPLICATION));
            }
        }
        else
        {
            if (!isNew && incident.getExtensions() != null && incident.getExtensions().getOperationsExtension() != null)
            {
                updateExtension.setAcknowledgeCorrelationKey(incident.getExtensions().getOperationsExtension().getAcknowledgeCorrelationKey());
            }
        }
        
        com.hp.schemas.opr.ws.serviceoperation.incidentmanagement._1.incident.CustomAttributes customAttributes = new com.hp.schemas.opr.ws.serviceoperation.incidentmanagement._1.incident.CustomAttributes();

        for (String key : incidentMap.keySet())
        {
            if (key != null && !defaultEventAttributes.containsKey(key.toLowerCase()))
            {
                com.hp.schemas.opr.ws.serviceoperation.incidentmanagement._1.incident.CustomAttribute myCA = new com.hp.schemas.opr.ws.serviceoperation.incidentmanagement._1.incident.CustomAttribute();
                myCA.setKey(key);
                myCA.setText(incidentMap.get(key));
                customAttributes.getCustomAttribute().add(myCA);
            }
            else
            {
                if (key != null)
                {
                    Log.log.warn("key in incidentMap is null");
                }
                else
                {
                    Log.log.debug(key  + " in incidentMap is default.");
                }
            }
        }
        
        if (isNew)
        {
            extension.setCustomAttributes(customAttributes);
            extensions.setOperationsExtension(extension);

            incident.setExtensions(extensions);
        }
        else
        {
            updateExtension.setCustomAttributes(customAttributes);
            updateExtensions.setOperationsExtension(updateExtension);
            
            updatedIncident.setExtensions(updateExtensions);
        }

        return isNew ? incident : updatedIncident;
    }

    private com.hp.schemas.opr.ws.serviceoperation.incidentmanagement._1.incident.CustomAttributes marshallCustomAttribute(Incident incident, Map<String, String> incidentMap)
    {
        com.hp.schemas.opr.ws.serviceoperation.incidentmanagement._1.incident.CustomAttributes customAttributes = new com.hp.schemas.opr.ws.serviceoperation.incidentmanagement._1.incident.CustomAttributes();

        for (String key : incidentMap.keySet())
        {
            if (!defaultEventAttributes.containsKey(key.toLowerCase()))
            {
                com.hp.schemas.opr.ws.serviceoperation.incidentmanagement._1.incident.CustomAttribute myCA = new com.hp.schemas.opr.ws.serviceoperation.incidentmanagement._1.incident.CustomAttribute();
                myCA.setKey(key);
                myCA.setText(incidentMap.get(key));
                customAttributes.getCustomAttribute().add(myCA);
            }
        }

        return customAttributes;
    }
    
    @Override
    public List<Map<String, String>> subscribe(String query, int maxResultsPerPull, long pullOpTimeOutInSecs, int numOfPullOps, String username, String password) throws Exception
    {
        List<Map<String, String>> result = new ArrayList<Map<String, String>>();
        
        String subscriptionId = null;
        IncidentServiceStub service = m_proxy;
        
        try
        {
            if (StringUtils.isNotEmpty(username) && StringUtils.isNotEmpty(password))
            {
                service = new IncidentServiceStub(m_targetURI, username, password);
                service._getServiceClient().engageModule("addressing");
                service._getServiceClient().getOptions().setTimeOutInMilliSeconds(configurations.getSocket_timeout() * 1000);
            }

            Log.log.debug("HPOMX Query: " + (StringUtils.isNotBlank(query) ? query : "No query provided"));
            
            IncidentEventingFilter eventFilter = null;
            
            if (StringUtils.isNotBlank(query))
            {
                eventFilter = prepareIncidentEventingFilter(query);
            }
            
            Map<String, String> subscriptionResult = subscribeWithFilterOp(eventFilter, service);
            
            Log.log.debug("subscribeWithFilterOp response : " + StringUtils.mapToString(subscriptionResult));

            if (!subscriptionResult.isEmpty() && subscriptionResult.containsKey(SUBSCRIPTION_ID_KEY) &&
                subscriptionResult.containsKey(ENUMERATION_CONTEXT_ID_KEY))
            {
                subscriptionId = subscriptionResult.get(SUBSCRIPTION_ID_KEY);
                
                for (int i = 0; i < numOfPullOps; i++)
                {
                    try
                    {
                        List<Incident> pulledIncidents = searchIncident(subscriptionResult.get(ENUMERATION_CONTEXT_ID_KEY), 
                                                                        BigInteger.valueOf(maxResultsPerPull), 
                                                                        (pullOpTimeOutInSecs > (configurations.getSocket_timeout() - 1) ? 
                                                                         (configurations.getSocket_timeout() - 1) : pullOpTimeOutInSecs), 
                                                                        service);
                        for (Incident incident : pulledIncidents)
                        {
                            Map<String, String> incidentMap = unmarshall(incident);
                            result.add(incidentMap);
                        }
                    }
                    catch(Exception rto)
                    {
                        if (rto.getMessage().toLowerCase().contains(PULL_READ_TIMEDOUT_EXCEPTION))
                        {
                            Log.log.debug("Operation Timed Out while pulling data from subscription with id " + 
                                          subscriptionId);
                        }
                    }
                }
            }
        }
        catch (Exception e)
        {
            Log.log.error("Error subscribing event data " + 
                          (StringUtils.isNotBlank(query) ? " with query: " + query : "") + 
                          ", actual error: " + e.getMessage(), e);
            throw e;
        }
        finally
        {
            if (StringUtils.isNotBlank(subscriptionId))
            {
                try
                {
                    unSubscribe(subscriptionId, service);
                }
                catch(Exception e)
                {
                    Log.log.warn("Ignoring error unsubscribing subscription with subscription id " + subscriptionId + 
                                 ", actual error: " + e.getMessage(), e);
                }
            }
        }
        
        return result;
    }
    
    /**
     * Method to call the pull operation on a WebService server (used by
     * enumeration and eventing)
     * 
     * @param enumerationContext
     *            Context of the enumeration iterator
     * @param maxElements
     *            maximum number of elements that should be sent back
     * @param opTimeOutInSec
     *            Time to wait in seconds for event from subscription
     * @return List of incidents
     * @throws RemoteException
     */
    private List<Incident> searchIncident(String enumerationContext, BigInteger maxElements, long opTimeOutInSec, IncidentServiceStub service) throws RemoteException
    {
        List<Incident> incidents = new ArrayList<Incident>();

        // Pull
        Pull pull = new Pull();
        EnumerationContextType enumerationContextType = new EnumerationContextType();
        enumerationContextType.getContent().add(enumerationContext);

        Duration maxTimeDuration = null;
        try
        {
            long opTimeOutInMilliSec = (opTimeOutInSec > 0) ? opTimeOutInSec * 1000 : 5000;
            maxTimeDuration = DatatypeFactory.newInstance().newDuration(opTimeOutInMilliSec);
        }
        catch (DatatypeConfigurationException e)
        {
            e.printStackTrace();
        }

        pull.setEnumerationContext(enumerationContextType);
        pull.setMaxTime(maxTimeDuration);

        if (maxElements != null)
        {
            pull.setMaxElements(maxElements);
        }
        else
        {
            pull.setMaxElements(BigInteger.valueOf(100));
        }

        AttributableURI resourceUri = setResourceUri();
        AttributableDuration operationTimeout = setOperationTimeout((opTimeOutInSec > 0) ? opTimeOutInSec * 1000 : 5000);

        setBasicAuthentication(service);

        PullResponse pullResponse = service.PullOp(pull, resourceUri, operationTimeout);
        ItemListType itemListType = pullResponse.getItems();
        List<Object> itemList = itemListType.getAny();

        // after the pull, convert the returned items to Incidents
        for (Object itemObject : itemList)
        {
            try
            {
                javax.xml.bind.JAXBContext context = service.getContextMap().get(Incident.class);
                javax.xml.bind.Unmarshaller unmarshaller = context.createUnmarshaller();
                incidents.add(unmarshaller.unmarshal((ElementNSImpl) itemObject, Incident.class).getValue());
            }
            catch (javax.xml.bind.JAXBException bex)
            {
                throw org.apache.axis2.AxisFault.makeFault(bex);
            }
        }

        return incidents;
    }
    
    /**
     * Method to call subscribe operation on a WebService server with a
     * filter
     * 
     * @param filter
     *            IncidentEventingFilter to filter for specific incidents
     * @return enumeration context ID, that can be used in subsequent pull
     *         operations and subscription id 
     * @throws RemoteException
     */
    @SuppressWarnings("unchecked")
    private Map<String, String> subscribeWithFilterOp(IncidentEventingFilter filter, IncidentServiceStub service) throws RemoteException
    {
        Map<String, String> result = new HashMap<String, String>();
        
        if (service == null)
        {
            if (m_proxy == null || m_targetURI == null)
            {
                return result;
            }
            
            service = m_proxy;
        }

        AttributableURI resourceUri = setResourceUri();
        AttributableDuration operationTimeout = setOperationTimeout();

        setBasicAuthentication(service);
        
        // Subscribe
        Subscribe subscribe = new Subscribe();
        
        DeliveryType pullDeliveryType = new DeliveryType();
        pullDeliveryType.setMode(PULL_DELIVERY_MODE);
        subscribe.setDelivery(pullDeliveryType);
        
        if (filter != null)
        {
            org.xmlsoap.schemas.ws._2004._08.eventing.FilterType subscriptionFilter = new org.xmlsoap.schemas.ws._2004._08.eventing.FilterType();
            subscriptionFilter.setDialect(FILTER_DIALECT_URI);
            // enumerationFilter.getContent().add(filter);
            subscriptionFilter.getContent().add(new com.hp.schemas.opr.ws.serviceoperation.incidentmanagement._1.incidentfilter.ObjectFactory().createIncidentEventingFilter(filter));
            subscribe.setFilter(subscriptionFilter);
        }

        SubscribeResponse subscriptionResponse = service.SubscribeOp(subscribe, resourceUri, operationTimeout);
        
        //JAXBElement<String> elm = (JAXBElement<String>) subscriptionResponse.getSubscriptionManager().getReferenceParameters().getAny().get(1);
        //Log.log.debug("Element Name: " + elm.getName() + ", declaredType: " + elm.getDeclaredType() + ", scope: " + elm.getScope() + ", value: " + elm.getValue() );
        String subscriptionId = ((JAXBElement<String>) subscriptionResponse.getSubscriptionManager().getReferenceParameters().getAny().get(1)).getValue();
        
        Log.log.debug("subscriptionId : " + subscriptionId);
        
        result.put(SUBSCRIPTION_ID_KEY, subscriptionId);
        
        Log.log.debug("Other Attributes : " + StringUtils.mapToString(subscriptionResponse.getOtherAttributes()));
        /*Log.log.debug("getAny(0) : " + subscriptionResponse.getAny().get(0));
        JAXBElement<String> elm = (JAXBElement<String>)subscriptionResponse.getAny().get(0);
        Log.log.debug("getAny elm(0) : " + elm.toString());
        Log.log.debug("Element Name: " + elm.getName() + ", declaredType: " + elm.getDeclaredType() + ", scope: " + elm.getScope() + ", value: " + elm.getValue() );
        String enumerationContextId = ((JAXBElement<String>)subscriptionResponse.getAny()).getValue();
        */
        result.put(ENUMERATION_CONTEXT_ID_KEY, subscriptionId);
        
        return result;
    }
    
    /**
     * Method to call unsubscribe operation on a WebService server
     * 
     * @param filter
     *            IncidentEnumerationFilter to filter for specific incidents
     * @return enumeration context ID, that can be used in subsequent pull
     *         operations and subscription id 
     * @throws RemoteException
     */
    private void unSubscribe(String subscriptionId, IncidentServiceStub service) throws RemoteException
    {
        if (service == null)
        {
            if (m_proxy == null || m_targetURI == null)
            {
                return;
            }
            
            service = m_proxy;
        }
        
        AttributableURI resourceUri = setResourceUri();
        AttributableDuration operationTimeout = setOperationTimeout();
        
        setBasicAuthentication(service);
        
        // Unsubscribe
        Unsubscribe unSubscribe = new Unsubscribe();
        
        service.UnsubscribeOp(unSubscribe, subscriptionId, resourceUri, operationTimeout);
    }

    @Override
    public boolean renewSubscription(String subscriptionId, long duration, String username, String password) throws Exception
    {
        boolean subscriptionRenewed = false;
        IncidentServiceStub service = m_proxy;
        
        try
        {
            if (StringUtils.isNotEmpty(username) && StringUtils.isNotEmpty(password))
            {
                service = new IncidentServiceStub(m_targetURI, username, password);
                service._getServiceClient().engageModule("addressing");
                service._getServiceClient().getOptions().setTimeOutInMilliSeconds(configurations.getSocket_timeout() * 1000);
            }
            
            AttributableURI resourceUri = setResourceUri();
            AttributableDuration operationTimeout = setOperationTimeout();
            
            setBasicAuthentication(service);
            
            // Renew
            Renew renew = new Renew();
            
            renew.setExpires(DatatypeFactory.newInstance().newDuration(duration).toString());
            
            RenewResponse renewResponse = service.RenewOp(renew, subscriptionId, resourceUri, operationTimeout);
            
            Log.log.debug("Subscription with subscriptionId " + subscriptionId + " renewed till " + renewResponse.getExpires());
            
            subscriptionRenewed = true;
        }
        catch (Exception e)
        {
            Log.log.error("Error renewing subscription with id " + subscriptionId + " for " + 
                          duration + " msec, actual error: " + e.getMessage(), e);
            //throw e;
            
            // unsubscribe if not renewable
            
            try
            {
                unSubscribe(subscriptionId, service);
            }
            catch(Exception e1)
            {
                Log.log.warn("Ignoring error unsubscribing subscription with subscription id " + subscriptionId + 
                             ", actual error: " + e1.getMessage(), e1);
            }
            
            if (e instanceof RemoteException)
            {
                throw e;
            }
        }
        
        return subscriptionRenewed;
    }

    @SuppressWarnings("unchecked")
    @Override
    public String createSubscription(String query, long duration, String username, String password) throws Exception
    {
        String newSubscriptionId = null;
        IncidentServiceStub service = m_proxy;
        
        try
        {
            if (StringUtils.isNotEmpty(username) && StringUtils.isNotEmpty(password))
            {
                service = new IncidentServiceStub(m_targetURI, username, password);
                service._getServiceClient().engageModule("addressing");
                service._getServiceClient().getOptions().setTimeOutInMilliSeconds(configurations.getSocket_timeout() * 1000);
            }

            Log.log.debug("HPOM Query: " + (StringUtils.isNotBlank(query) ? query : "No query provided"));
            
            IncidentEventingFilter eventFilter = null;
            
            if (StringUtils.isNotBlank(query))
            {
                eventFilter = prepareIncidentEventingFilter(query);
            }
            
            AttributableURI resourceUri = setResourceUri();
            AttributableDuration operationTimeout = setOperationTimeout();
            
            setBasicAuthentication(service);
            
            // Subscribe
            Subscribe subscribe = new Subscribe();
            
            DeliveryType pullDeliveryType = new DeliveryType();
            pullDeliveryType.setMode(PULL_DELIVERY_MODE);
            subscribe.setDelivery(pullDeliveryType);
            subscribe.setExpires(DatatypeFactory.newInstance().newDuration(duration).toString());
            
            if (eventFilter != null)
            {
                org.xmlsoap.schemas.ws._2004._08.eventing.FilterType subscriptionFilter = new org.xmlsoap.schemas.ws._2004._08.eventing.FilterType();
                subscriptionFilter.setDialect(FILTER_DIALECT_URI);
                // enumerationFilter.getContent().add(filter);
                subscriptionFilter.getContent().add(new com.hp.schemas.opr.ws.serviceoperation.incidentmanagement._1.incidentfilter.ObjectFactory().createIncidentEventingFilter(eventFilter));
                subscribe.setFilter(subscriptionFilter);
            }

            SubscribeResponse subscriptionResponse = service.SubscribeOp(subscribe, resourceUri, operationTimeout);
            
            //JAXBElement<String> elm = (JAXBElement<String>) subscriptionResponse.getSubscriptionManager().getReferenceParameters().getAny().get(1);
            //Log.log.debug("Element Name: " + elm.getName() + ", declaredType: " + elm.getDeclaredType() + ", scope: " + elm.getScope() + ", value: " + elm.getValue() );
            newSubscriptionId = ((JAXBElement<String>) subscriptionResponse.getSubscriptionManager().getReferenceParameters().getAny().get(1)).getValue();
            
            Log.log.debug("New subscriptionId : " + newSubscriptionId);
        }
        catch (Exception e)
        {
            Log.log.error("Error subscribing event data " + 
                          (StringUtils.isNotBlank(query) ? " with query: " + query : "") + 
                          ", actual error: " + e.getMessage(), e);
            throw e;
        }
        
        return newSubscriptionId;
    }

    @Override
    public List<Map<String, String>> pullSubscription(String subscriptionId, int maxRecordsToPull, String username, String password) throws Exception
    {
        List<Map<String, String>> result = new ArrayList<Map<String, String>>();
        
        IncidentServiceStub service = m_proxy;
        
        try
        {
            if (StringUtils.isNotEmpty(username) && StringUtils.isNotEmpty(password))
            {
                service = new IncidentServiceStub(m_targetURI, username, password);
                service._getServiceClient().engageModule("addressing");
                service._getServiceClient().getOptions().setTimeOutInMilliSeconds(configurations.getSocket_timeout() * 1000);
            }
            
            try
            {
                List<Incident> pulledIncidents = searchIncident(subscriptionId, 
                                                                BigInteger.valueOf(maxRecordsToPull), 
                                                                (configurations.getSocket_timeout() - 1), 
                                                                service);
                
                for (Incident incident : pulledIncidents)
                {
                    Map<String, String> incidentMap = unmarshall(incident);
                    result.add(incidentMap);
                }
            }
            catch(Exception rto)
            {
                if (rto.getMessage().toLowerCase().contains(PULL_READ_TIMEDOUT_EXCEPTION))
                {
                    Log.log.debug("Operation timed out while pulling data from subscription with id " + 
                                  subscriptionId);
                }
            }
        }
        catch (Exception e)
        {
            Log.log.error("Error pulling event data from subscription with id " + subscriptionId + 
                          ", actual error: " + e.getMessage(), e);
            
            // Unsubscribe
            
            try
            {
                unSubscribe(subscriptionId, service);
            }
            catch(Exception e1)
            {
                Log.log.warn("Ignoring error unsubscribing subscription with subscription id " + subscriptionId + 
                             ", actual error: " + e1.getMessage(), e1);
            }
            
            throw e;
        }
        
        return result;
    }
    
    @Override
    public List<Map<String, String>> pullSubscription(String subscriptionId, int maxRecordsToPull, String username, String password, int filterIntervalInSecs) throws Exception
    {
        List<Map<String, String>> result = new ArrayList<Map<String, String>>();
        
        IncidentServiceStub service = m_proxy;
        
        try
        {
            if (StringUtils.isNotEmpty(username) && StringUtils.isNotEmpty(password))
            {
                service = new IncidentServiceStub(m_targetURI, username, password);
                service._getServiceClient().engageModule("addressing");
                service._getServiceClient().getOptions().setTimeOutInMilliSeconds(filterIntervalInSecs * 1000);
            }
            
            try
            {
                List<Incident> pulledIncidents = searchIncident(subscriptionId, 
                                                                BigInteger.valueOf(maxRecordsToPull), 
                                                                filterIntervalInSecs, 
                                                                service);
                
                for (Incident incident : pulledIncidents)
                {
                    Map<String, String> incidentMap = unmarshall(incident);
                    result.add(incidentMap);
                }
            }
            catch(Exception rto)
            {
                if (rto.getMessage().toLowerCase().contains(PULL_READ_TIMEDOUT_EXCEPTION))
                {
                    Log.log.debug("Operation timed out while pulling data from subscription with id " + 
                                  subscriptionId);
                }
            }
        }
        catch (Exception e)
        {
            Log.log.error("Error pulling event data from subscription with id " + subscriptionId + 
                          ", actual error: " + e.getMessage(), e);
            
            // Unsubscribe
            
            try
            {
                unSubscribe(subscriptionId, service);
            }
            catch(Exception e1)
            {
                Log.log.warn("Ignoring error unsubscribing subscription with subscription id " + subscriptionId + 
                             ", actual error: " + e1.getMessage(), e1);
            }
            
            throw e;
        }
        
        return result;
    }
    
    @Override
    public void unSubscribe(String subscriptionId, String username, String password) throws Exception
    {
        IncidentServiceStub service = m_proxy;
        
        if (StringUtils.isNotEmpty(username) && StringUtils.isNotEmpty(password))
        {
            service = new IncidentServiceStub(m_targetURI, username, password);
            service._getServiceClient().engageModule("addressing");
            service._getServiceClient().getOptions().setTimeOutInMilliSeconds(configurations.getSocket_timeout() * 1000);
        }
        
        unSubscribe(subscriptionId, service);
    }

    @Override
    public void ownMany(Collection<String> objectIds, String username, String password) throws Exception
    {
        if (objectIds == null || objectIds.isEmpty())
        {
            return;
        }
        
        try
        {
            IncidentServiceStub service = m_proxy;
            
            if (StringUtils.isNotEmpty(username) && StringUtils.isNotEmpty(password))
            {
                service = new IncidentServiceStub(m_targetURI, username, password);
                service._getServiceClient().engageModule("addressing");
                service._getServiceClient().getOptions().setTimeOutInMilliSeconds(configurations.getSocket_timeout() * 1000);
            }
    
            Log.log.debug("HPOMX user " + 
                          ((StringUtils.isNotEmpty(username) && StringUtils.isNotEmpty(password)) ? 
                           username : configurations.getUsername() ) + 
                          " taking ownership of incident ids: " + 
                          StringUtils.convertCollectionToString(objectIds.iterator(), ", "));
            
            AttributableURI resourceUri = setResourceUri();
            AttributableDuration operationTimeout = setOperationTimeout();
            
            setBasicAuthentication(service);
            
            // IncidentIDs
            IncidentIDs incidentIDs = new IncidentIDs();
            incidentIDs.getId().addAll(objectIds);
            
            service.OwnMany(incidentIDs, resourceUri, operationTimeout);
        }
        catch (Exception e)
        {
            Log.log.error("Error in taking ownership of incident ids " + 
                          StringUtils.convertCollectionToString(objectIds.iterator(), ", ") + 
                          "by " + 
                          ((StringUtils.isNotEmpty(username) && StringUtils.isNotEmpty(password)) ? username : configurations.getUsername()) + 
                          " actual error: " + e.getMessage(), e);
              throw e;
        }
    }

    @Override
    public void disownMany(Collection<String> objectIds, String username, String password) throws Exception
    {
        if (objectIds == null || objectIds.isEmpty())
        {
            return;
        }
        
        try
        {
            IncidentServiceStub service = m_proxy;
            
            if (StringUtils.isNotEmpty(username) && StringUtils.isNotEmpty(password))
            {
                service = new IncidentServiceStub(m_targetURI, username, password);
                service._getServiceClient().engageModule("addressing");
                service._getServiceClient().getOptions().setTimeOutInMilliSeconds(configurations.getSocket_timeout() * 1000);
            }
    
            Log.log.debug("HPOMX user " + 
                          ((StringUtils.isNotEmpty(username) && StringUtils.isNotEmpty(password)) ? 
                           username : configurations.getUsername() ) + " releasing ownership of incident ids: " + 
                          StringUtils.convertCollectionToString(objectIds.iterator(), ", "));
            
            AttributableURI resourceUri = setResourceUri();
            AttributableDuration operationTimeout = setOperationTimeout();
            
            setBasicAuthentication(service);
            
            // IncidentIDs
            IncidentIDs incidentIDs = new IncidentIDs();
            incidentIDs.getId().addAll(objectIds);
            
            service.DisownMany(incidentIDs, resourceUri, operationTimeout);
        }
        catch (Exception e)
        {
            Log.log.error("Error in releasing ownership of incident ids " + 
                          StringUtils.convertCollectionToString(objectIds.iterator(), ", ") + 
                          "by " + 
                          ((StringUtils.isNotEmpty(username) && StringUtils.isNotEmpty(password)) ? 
                           username : configurations.getUsername()) + 
                          " actual error: " + e.getMessage(), e);
              throw e;
        }   
    }
    
    @Override
    public String addAnnotation(String objectId, String annotation, String username, String password) throws Exception
    {
        String annotationId = null;
        
        if (objectId == null || StringUtils.isBlank(annotation))
        {
            throw new Exception("Passed invalid arguments. Object id and/or annotation is null or empty.");
        }
        
        try
        {
            IncidentServiceStub service = m_proxy;
            
            if (StringUtils.isNotEmpty(username) && StringUtils.isNotEmpty(password))
            {
                service = new IncidentServiceStub(m_targetURI, username, password);
                service._getServiceClient().engageModule("addressing");
                service._getServiceClient().getOptions().setTimeOutInMilliSeconds(configurations.getSocket_timeout() * 1000);
            }
    
            Log.log.debug("Adding annotation [" + annotation + "] to Incident with id " + objectId);
            
            AttributableURI resourceUri = setResourceUri();
            AttributableDuration operationTimeout = setOperationTimeout();
            
            // SelectorSetType
            SelectorSetType selectorSet = new SelectorSetType();
            SelectorType incidentIdSelector = new SelectorType();
            incidentIdSelector.setName(INCIDENT_ID_SELECTOR_NAME);
            incidentIdSelector.getContent().add(objectId);
            selectorSet.getSelector().add(incidentIdSelector);
            
            setBasicAuthentication(service);
            
            /*
            JAXBElement<String> annotationElm = new com.hp.schemas.opr.ws.serviceoperation.incidentmanagement._1.incident.ObjectFactory().createAnnotationText(annotation);
            
            Log.log.debug("Annotation Element : [" + annotationElm.toString() + "]");
            
            
            Annotation annotationObj = new Annotation();
            annotationObj.setText(annotation);
            *
            
            IncidentExtensionsType incExt = new ObjectFactory().createIncidentExtensionsType();
            incExt.setAnnotationText(annotation);
            */
            
            annotationId = service.AddAnnotation(annotation, resourceUri, selectorSet, operationTimeout);
            
            Log.log.debug("Annotation Id of new annotation is " + annotationId);
        }
        catch (Exception e)
        {
            Log.log.error("Error in adding annotation [" + annotation + "] to Incident with id " + objectId +
                          " actual error: " + e.getMessage(), e);
            throw e;
        }
        
        return annotationId;
    }
    
    @Override
    public void updateAnnotation(String objectId, String annotationId, String annotationUpdate, String username, String password) throws Exception
    {
        if (objectId == null || annotationId == null || StringUtils.isBlank(annotationUpdate))
        {
            throw new Exception("Passed invalid arguments. Object id and/or annotation id and/or updated annotation is null or empty.");
        }
        
        try
        {
            IncidentServiceStub service = m_proxy;
            
            if (StringUtils.isNotEmpty(username) && StringUtils.isNotEmpty(password))
            {
                service = new IncidentServiceStub(m_targetURI, username, password);
                service._getServiceClient().engageModule("addressing");
                service._getServiceClient().getOptions().setTimeOutInMilliSeconds(configurations.getSocket_timeout() * 1000);
            }
    
            Log.log.debug("Updating annotation with id " + annotationId + 
                          " associated with Incident " + objectId + " to " + annotationUpdate);
            
            AttributableURI resourceUri = setResourceUri();
            AttributableDuration operationTimeout = setOperationTimeout();
            
            // SelectorSetType
            SelectorSetType selectorSet = new SelectorSetType();
            SelectorType incidentIdSelector = new SelectorType();
            incidentIdSelector.setName(INCIDENT_ID_SELECTOR_NAME);
            incidentIdSelector.getContent().add(objectId);
            selectorSet.getSelector().add(incidentIdSelector);
            
            UpdateAnnotation updateAnnotation = new UpdateAnnotation();
            updateAnnotation.setAnnotationId(annotationId);
            updateAnnotation.setAnnotationText(annotationUpdate);
            
            setBasicAuthentication(service);
                        
            service.UpdateAnnotation(updateAnnotation, resourceUri, selectorSet, operationTimeout);
            
            Log.log.debug("Successfully updated annotation");
        }
        catch (Exception e)
        {
            Log.log.error("Error in updating annotation with id " + annotationId + 
                          " associated with Incident " + objectId + " to [" + annotationUpdate +
                          "] actual error: " + e.getMessage(), e);
            throw e;
        }
    }
    
    @Override
    public void deleteAnnotation(String objectId, String annotationId, String username, String password) throws Exception
    {
        if (objectId == null || annotationId == null)
        {
            throw new Exception("Passed invalid arguments. Object id and/or annotation id is null or empty.");
        }
        
        try
        {
            IncidentServiceStub service = m_proxy;
            
            if (StringUtils.isNotEmpty(username) && StringUtils.isNotEmpty(password))
            {
                service = new IncidentServiceStub(m_targetURI, username, password);
                service._getServiceClient().engageModule("addressing");
                service._getServiceClient().getOptions().setTimeOutInMilliSeconds(configurations.getSocket_timeout() * 1000);
            }
    
            Log.log.debug("Deleting annotation with id " + annotationId + 
                          " associated with Incident id " + objectId);
            
            AttributableURI resourceUri = setResourceUri();
            AttributableDuration operationTimeout = setOperationTimeout();
            
            // SelectorSetType
            SelectorSetType selectorSet = new SelectorSetType();
            SelectorType incidentIdSelector = new SelectorType();
            incidentIdSelector.setName(INCIDENT_ID_SELECTOR_NAME);
            incidentIdSelector.getContent().add(objectId);
            selectorSet.getSelector().add(incidentIdSelector);
            
            setBasicAuthentication(service);
                        
            service.DeleteAnnotation(annotationId, resourceUri, selectorSet, operationTimeout);
            
            Log.log.debug("Successfully deleted annotation");
        }
        catch (Exception e)
        {
            Log.log.error("Error in deleting annotation with id " + annotationId + 
                          " associated with Incident id " + objectId + " actual error: " + 
                          e.getMessage(), e);
            throw e;
        }
    }

    @Override
    public Collection<Map<String, String>> getAnnotations(String objectId, String username, String password) throws Exception
    {
        Collection<Map<String, String>> annotations = new ArrayList<Map<String, String>>();
        
        if (objectId == null)
        {
            throw new Exception("Passed invalid arguments. Object id is null or empty.");
        }
        
        try
        {
            IncidentServiceStub service = m_proxy;
            
            if (StringUtils.isNotEmpty(username) && StringUtils.isNotEmpty(password))
            {
                service = new IncidentServiceStub(m_targetURI, username, password);
                service._getServiceClient().engageModule("addressing");
                service._getServiceClient().getOptions().setTimeOutInMilliSeconds(configurations.getSocket_timeout() * 1000);
            }
    
            Log.log.debug("Geting annotations for incident id " + objectId);
            
            AttributableURI resourceUri = setResourceUri();
            AttributableDuration operationTimeout = setOperationTimeout();
            
            // SelectorSetType
            SelectorSetType selectorSet = new SelectorSetType();
            SelectorType incidentIdSelector = new SelectorType();
            incidentIdSelector.setName(INCIDENT_ID_SELECTOR_NAME);
            incidentIdSelector.getContent().add(objectId);
            selectorSet.getSelector().add(incidentIdSelector);
            
            setBasicAuthentication(service);
                        
            Annotations hpomAnnotations = service.GetAnnotations(resourceUri, selectorSet, operationTimeout);
            
            Log.log.debug("# of annotations received " + hpomAnnotations != null ? hpomAnnotations.getAnnotation().size() : "none");
            
            if (hpomAnnotations != null && hpomAnnotations.getAnnotation() != null &&
                !hpomAnnotations.getAnnotation().isEmpty())
            {
                for (Annotation hpomAnnotation : hpomAnnotations.getAnnotation())
                {
                    Map<String, String> annotation = new HashMap<String, String>();
                    
                    annotation.put(HPOM_ANNOTATION_FIELD_AUTHOR, hpomAnnotation.getAuthor());
                    annotation.put(HPOM_ANNOTATION_FIELD_TEXT, hpomAnnotation.getText());
                    annotation.put(HPOM_ANNOTATION_FIELD_DATE, hpomAnnotation.getDate().toString());
                    annotation.put(HPOM_ANNOTATION_FIELD_ID, hpomAnnotation.getID());
                    
                    Log.log.debug("Annotation [" + StringUtils.mapToLogString(annotation)+ "]");
                    annotations.add(annotation);
                }
            }
        }
        catch (Exception e)
        {
            Log.log.error("Error in getting annotations for Incident id " + objectId + " actual error: " + 
                          e.getMessage(), e);
            throw e;
        }
        
        return annotations;
    }
}
