/******************************************************************************
* (C) Copyright 2014
*
* Resolve Systems, LLC
*
* All rights reserved
* This software is distributed under license and may not be used, copied,
* modified, or distributed without the express written permission of
* Resolve Systems, LLC.
*
******************************************************************************/

package com.resolve.gateway.weblogicjms;

import javax.jms.Message;
import javax.jms.MessageListener;
import javax.jms.Queue;
import javax.jms.QueueConnection;
import javax.jms.QueueReceiver;
import javax.jms.QueueSession;
import javax.jms.TextMessage;
import javax.naming.InitialContext;

import com.resolve.util.Log;

class WLReceiver extends MessageConsumer implements MessageListener {

    private QueueConnection connection;
    private QueueSession session;
    private String queueName;
    private String filterName;
    private QueueReceiver receiver;
    
    
    public WLReceiver() {}
    
    public WLReceiver(QueueConnection connection, QueueSession session, String queueName, String filterName) {
        
        this.connection = connection;
        this.session = session;
        this.queueName = queueName;
        this.filterName = filterName;
    }
    
    public void startReceiving(InitialContext ic) throws Exception {
        
        try {
            Queue queue = (Queue)ic.lookup(queueName);
            receiver = (QueueReceiver)session.createReceiver(queue);
            receiver.setMessageListener(this);
            
            Log.log.info("Receiver " + queueName + " is started.");
        } catch(Exception e) {
            throw e;
        } catch(Throwable t) {
            Log.log.error("Failed to start receiving data on queue: " + queueName);
            Log.log.error(t.getMessage(),t);
            throw new Exception("Runtime exception: " + t.getMessage());
        }
    }
    
    public void onMessage(Message message) {
        
        String msgId = null;
        String msgType = null;
        String msgBody = null;
        String msgReceived = null;
        
        try {
            msgType = message.getJMSType();
            msgId = message.getJMSMessageID();
            
            if(message instanceof TextMessage) {
//                msgType = "TEXT";
                msgBody = ((TextMessage)message).getText();
                msgReceived = message.toString();
                Log.log.debug("Received message <" + msgBody + "> with ID <" + msgId + ">");
            }
            
            if(!processMessage(filterName, msgId, msgType, msgBody, msgReceived)) {
                Log.log.debug(msgReceived);
                Log.log.debug("Message type is: " + msgType);
                Log.log.error("Failed to process message.");
            }
            
        } catch (Exception e) {
            e.printStackTrace();
            Log.log.debug(message);
            Log.log.error(e.getMessage(), e);
        }
    }
    
    public void stopReceiving() throws Exception {

        try {
            if(receiver != null)
                receiver.close();
            
            if(session != null)
                session.close();
            
            Log.log.info("Stop receiving: " + queueName);
            
            if(connection != null) {
               connection.stop();
               connection.close();
            }
        } catch(Exception e) {
            Log.log.error("Failed to stop the receiving process on queue: " + queueName );
            Log.log.error(e.getMessage(), e);
            throw e;
        }
        
        Log.log.info("Receiver " + this.queueName + " stopped.");
    }
}