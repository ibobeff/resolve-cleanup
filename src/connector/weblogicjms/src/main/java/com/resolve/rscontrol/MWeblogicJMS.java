package com.resolve.rscontrol;

import com.resolve.persistence.model.WeblogicJMSFilter;

public class MWeblogicJMS extends MGateway {

    private static final String MODEL_NAME = WeblogicJMSFilter.class.getSimpleName();

    protected String getModelName() {
        return MODEL_NAME;
    }
}

