/******************************************************************************
 * (C) Copyright 2014
 *
 * Resolve Systems, LLC
 *
 * All rights reserved This software is distributed under license and may not be
 * used, copied, modified, or distributed without the express written permission
 * of Resolve Systems, LLC.
 *
 ******************************************************************************/

package com.resolve.gateway.weblogicjms;

import com.resolve.gateway.AbstractGatewayAPI;

public class WeblogicJMSAPI extends AbstractGatewayAPI
{
    private WeblogicJMSFilter filter;

    /**
     * Constructor for using the API instance object to call functional APIs
     * with the parameters defined by filter
     * 
     * @param filter
     * @throws Exception
     */
    public WeblogicJMSAPI(WeblogicJMSFilter filter) throws Exception
    {

        if (filter == null) throw new Exception("Filter is not found.");

        this.filter = filter;

    }

    /**
     *  Constructor for using the API instance object to call functional APIs
     *  with the parameters defined by a filter with certain name.
     *  
     * @param filterName - defined filter name
     * @throws Exception - in case that of not existing filter with the provided name
     */
    public WeblogicJMSAPI(String filterName) throws Exception
    {

        WeblogicJMSFilter weblogicJmsFilter = WeblogicJMSGateway.getInstance().getFilterByName(filterName);
        if (weblogicJmsFilter == null) throw new Exception("Filter is not found.");

        this.filter = weblogicJmsFilter;

    }
    
    public WeblogicJMSAPI() throws Exception
    {
    }

    /**
     * Sends plain text messages to the queue/topic defined in by the filter.
     * @param message
     * @throws Exception
     */
    public void publish(String message) throws Exception
    {
        WeblogicService.instance.sendMessage(message, filter);
    }

    /**
     * Sends plain text messages by using a weblogic filter with specific name. 
     * @param msg
     * @param filterName - the name of a defined filter in Resolve
     * @throws Exception
     */
    public void publish(String message, String filterName) throws Exception
    {
        WeblogicService.instance.sendMessage(message, filterName);
    }
    
    
    /**
     * Custom way to publish a message.
     * 
     * @param type
     * @param url - example - t3://127.0.0.1:7001
     * @param initContextFactory - example - jms/TestResolveJMSConnectionFactory
     * @param user
     * @param pass
     * @param topicName - example - jms/TestResolveJMSQueue
     * @param message
     */
    public void publish(SubscriptionType type, String url, String initContextFactory, String jmsFactory, String user, String pass, String topicName, String message) {
        
        switch(type) {
            case TOPIC: {
                WeblogicService.instance.sendMessageToTopic(url, initContextFactory, jmsFactory, user, pass, topicName, message);
            }

           case QUEUE: {
               WeblogicService.instance.sendMessageToQueue(url, initContextFactory, jmsFactory, user, pass, topicName, message);
            }
        }
        
    }
    
    /**
     * Tests the connectivity to given WL server.
     * 
     * @param url
     * @param initContextFactory
     * @param jmsFactory
     * @param user
     * @param pass
     * @return
     */
    public boolean testConnectionToWeblogic(String url, String initContextFactory, String jmsFactory, String user, String pass) {
        return WeblogicService.instance.heartbeat(url, initContextFactory, user, pass, jmsFactory);
    }

    /**
     * Subscribe to a Topic with defined by filter(selected by its name) created on Resolve WeblogicJMSGateway.
     * 
     * @param filterName
     * @throws Exception
     */
    void subscribeToTopic(String filterName) throws Exception
    {
        WeblogicService.instance.startTopicSubscriber(filterName);
    }

    /**
     * Subscribe to a Topic with defined by filter created on Resolve WeblogicJMSGateway.
     * 
     * @throws Exception
     */
    void subscribeToTopic() throws Exception
    {
        WeblogicService.instance.startTopicSubscriber(filter);
    }

    /**
     * Listen to a Queue defined in a filter(selected by its name) created on Resolve WeblogicJMSGateway.
     * 
     * @param filterName
     * @throws Exception
     */
    void listenToQueue(String filterName) throws Exception
    {
        WeblogicService.instance.startQueueReceiver(filterName);
    }

    /**
     *  Listen to a Queue defined in a filter created on Resolve WeblogicJMSGateway.
     * @throws Exception
     */
    void listenToQueue() throws Exception
    {
        WeblogicService.instance.startQueueReceiver(filter);
    }

    void stopTopicSubscriber(String filterName) throws Exception
    {
        WeblogicService.instance.stopTopicSubscriber(filterName);
    }

    void stopQueueReceiver(String filterName) throws Exception
    {
        WeblogicService.instance.stopQueueReceiver(filterName);
    }

}
