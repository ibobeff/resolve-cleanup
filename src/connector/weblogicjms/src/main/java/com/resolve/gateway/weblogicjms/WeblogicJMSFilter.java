package com.resolve.gateway.weblogicjms;

import com.resolve.gateway.BaseFilter;

public class WeblogicJMSFilter extends BaseFilter
{

    public static final String TOPIC = "TOPIC";
    public static final String QUEUE = "QUEUE";
    public static final String ADDRESS = "ADDRESS";
    public static final String USERNAME = "USERNAME";
    public static final String PASSWORD = "PASSWORD";
    public static final String JMSFACTORY = "JMSFACTORY";
    public static final String VALUE = "VALUE";
    public static final String TYPE = "TYPE";
    public static final String CLIENT = "CLIENT";

    private String address;
    private String username;
    private String password;
    private String jmsfactory;
    private String type;
    private String value;
    private String client;

    public WeblogicJMSFilter(String id, String active, String order, String interval, String eventEventId,  
    						String runbook, String script, String address, String value, String type, 
    						String client, String username, String password, String jmsfactory)
    {
        super(id, active, order, interval, eventEventId, runbook, script);

        this.username = username;
        this.password = password;
        this.jmsfactory = jmsfactory;
        
        this.value = value;
        this.type = type;
        this.client = client;
        this.address = address;
    }

    public String getUsername()
    {
        return username;
    }
    
    public void setUsername(String username)
    {
        this.username = username;
    }
    
    public String getPassword()
    {
        return password;
    }
    
    public void setPassword(String password)
    {
        this.password = password;
    }
    
    public String getJmsfactory()
    {
        return jmsfactory;
    }
    
    public void setJmsfactory(String jmsfactory)
    {
        this.jmsfactory = jmsfactory;
    }
    
    public void setClient(String client)
    {
        this.client = client;
    }
    
    public String getClient()
    {
        return client;
    }
    
    public String getAddress()
    {
        return address;
    }
    
    public void setAddress(String address)
    {
        this.address = address;
    }
    
    public void setValue(String value)
    {
        this.value = value;
    }

    public String getValue()
    {
        return value;
    }

    public String getType()
    {
        return this.type;
    }

    public void setType(String type)
    {
        this.type = type;
    }
}
