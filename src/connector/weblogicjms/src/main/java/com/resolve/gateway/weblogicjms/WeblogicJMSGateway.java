package com.resolve.gateway.weblogicjms;

import java.util.List;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.CopyOnWriteArrayList;

import com.resolve.gateway.BaseClusteredGateway;
import com.resolve.gateway.BaseFilter;
import com.resolve.gateway.Filter;
import com.resolve.gateway.MWeblogicJMS;
import com.resolve.rsbase.MainBase;
import com.resolve.util.Constants;
import com.resolve.util.Log;
import com.resolve.util.StringUtils;

import net.sf.json.JSONObject;

public class WeblogicJMSGateway extends BaseClusteredGateway {
    
    private static volatile WeblogicJMSGateway instance = null;
    private static volatile WeblogicJMSHeartbeat heartBeat = null;
    private static volatile boolean wlServerAlive = false;
    
    private static volatile List<String> deployedFilters = new CopyOnWriteArrayList<String>();

    private String user;
    private String pass;
    private String jndiFactory;
    
    //Adding properties to handle security encryption
    private ConfigReceiveWeblogicJMS webJmsconfig;
    
    private int alive;
    

    private String queue = null;

    public static WeblogicJMSGateway getInstance(ConfigReceiveWeblogicJMS config) {
        
        if (instance == null) {
            instance = new WeblogicJMSGateway(config);
        }
        
        return instance;
    }
    
    

    /**
     * This method is called once the initialization is done. If it gets called
     * before the initialization it will throw exception.
     *
     * @return
     */
    public static WeblogicJMSGateway getInstance() {
        if (instance == null) {
            throw new RuntimeException("WeblogicJMS Gateway is not initialized correctly.");
        } else {
            return instance;
        }
    }

    private WeblogicJMSGateway(ConfigReceiveWeblogicJMS config) {
        super(config);
        this.webJmsconfig = config; 
    }

    
  public WeblogicJMSFilter getFilterByName(String filterName) {
        
        WeblogicJMSFilter filterFound = null;
        
        for(Filter filter: getInstance().getOrderedFilters()) {
            if(filter instanceof WeblogicJMSFilter) {
                if(filter.getId().equals(filterName))
                {
                    filterFound = (WeblogicJMSFilter)filter;
                    break;
                }
            }
        }
        
        return filterFound;
    }
  
  
    @Override
    public String getLicenseCode() {
        return "WEBLOGICJMS";
    }

    @Override
    protected void addAdditionalValues(Map<String, Object> arg0, Map<String, String> arg1) {
    }

    @Override
    protected String getGatewayEventType() {
        return "WEBLOGICJMS";
    }

    @Override
    protected String getMessageHandlerName() {
        return MWeblogicJMS.class.getSimpleName();
    }

    @Override
    protected Class<MWeblogicJMS> getMessageHandlerClass() {
        return MWeblogicJMS.class;
    }

    public String getQueueName() {
        return queue + getOrgSuffix();
    }

    @Override
    public void start() {
        Log.log.info("Starting WeblogicJMS Gateway ...");
        super.start();
    }

    @Override
    public void run() {
        
        // Reload all the deployed filters when RSRemote is stopped and started again.
        super.sendSyncRequest();
    }

    @Override
    protected void initialize() {
        ConfigReceiveWeblogicJMS config = (ConfigReceiveWeblogicJMS)configurations;
        
        try {
            user = config.getUsername();
            pass = config.getPassword();
            jndiFactory = config.getJndifactory();
            
            String aliveStr = config.getAlive();
            if(StringUtils.isBlank(aliveStr))
                alive = 60;
            
            else {
                try {
                    Integer aliveInt = new Integer(aliveStr);
                    alive = aliveInt.intValue();
                } catch(Exception ee) {
                    alive = 60;
                    ee.printStackTrace();
                    Log.log.error(ee.getMessage(), ee);
                }
            }
        } catch(Exception e) {
            e.printStackTrace();
            Log.log.error(e.getMessage(), e);
        } catch(Throwable t) {
            t.printStackTrace();
            Log.log.error(t.getMessage(), t);
        }
        
        queue = config.getQueue().toUpperCase();
        
        try {
            Log.log.info("Initializing WeblogicJMS Listener ...");
            this.gatewayConfigDir = "/config/weblogic/";
        } catch (Exception e) {
            Log.log.error("Failed to config WeblogicJMS Gateway: " + e.getMessage(), e);
        }

        if(config.isActive() && config.isPrimary()) {
            // Monitor MQ server connection if it's down
            wlServerAlive = WeblogicJMSHeartbeat.sendHeartbeat();

            heartBeat = new WeblogicJMSHeartbeat(this);
            heartBeat.start();
        }
    }

    /**
     * This method processes the message received from the WeblogicJMS system.
     *
     * @param message
     */
    public boolean processData(String filterName, Map<String, String> params) {
        
        boolean result = true;
        
        try {
            if (StringUtils.isNotBlank(filterName) && params != null) {
                WeblogicJMSFilter weblogicJMSFilter = (WeblogicJMSFilter) filters.get(filterName);
                if (weblogicJMSFilter != null && weblogicJMSFilter.isActive()) {
                    if (Log.log.isDebugEnabled()) {
                        Log.log.debug("Processing filter: " + weblogicJMSFilter.getId());
                        Log.log.debug("Data received through WeblogicJMS gateway: " + params);
                    }
                    Map<String, String> runbookParams = params;
                    
                    runbookParams.put(FILTER_ID_NAME, weblogicJMSFilter.getId());
                    
                    if (!params.containsKey(Constants.EVENT_EVENTID) || StringUtils.isBlank(params.get(Constants.EVENT_EVENTID)))
                        runbookParams.put(Constants.EVENT_EVENTID, weblogicJMSFilter.getEventEventId());

                    // addToPrimaryDataQueue(weblogicJMSFilter, runbookParams);
                    // send to the worker queue
                    if (MainBase.esb.sendInternalMessage(workerQueueName, getMessageHandlerName() + ".receiveData", runbookParams))
                    {
                        if (Log.log.isTraceEnabled())
                        {
                            Log.log.trace(">>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>");
                            Log.log.trace("Sent data to worker queue: " + workerQueueName);
                            Log.log.trace(">>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>");
                        }
                    }
                    else
                    {
                        result = false;
                        Log.log.warn("Could not send data to worker queue: " + workerQueueName);
                    }
                } else {
                    result = false;
                }
            }
        } catch (Exception e) {
            Log.log.warn("Error: " + e.getMessage(), e);
            try {
                Thread.sleep(interval);
            } catch (InterruptedException ie) {
                Log.log.warn("sleep interrupted: " + ie.getMessage(), ie);
            }
        }
        
        return result;
    }
    
    public String processBlockingData(String filterName, Map<String, String> params)
    {
        String result = "";
        
        try
        {
            if(StringUtils.isNotBlank(filterName) && params != null)
            {
                WeblogicJMSFilter weblogicJMSFilter = (WeblogicJMSFilter) filters.get(filterName);
                if (weblogicJMSFilter != null && weblogicJMSFilter.isActive()) {
                    if(Log.log.isDebugEnabled())
                    {
                        Log.log.debug("Processing filter: " + weblogicJMSFilter.getId());
                        Log.log.debug("Data received through WEBLOGICJMSJMS gateway: " + params);
                    }
                    
                    Map<String, String> runbookParams = params;
                    
                    if(!params.containsKey(Constants.EVENT_EVENTID) || StringUtils.isBlank(params.get(Constants.EVENT_EVENTID)))
                    {
                        runbookParams.put(Constants.EVENT_EVENTID, weblogicJMSFilter.getEventEventId());
                    }

                    runbookParams.put("FILTER_ID", filterName);
                    
                    result = instance.receiveData(runbookParams);
                }
                
                else {
                    JSONObject message = new JSONObject();
                    message.accumulate("Message", "The filter is inactive or runbook is not available.");
                    result = message.toString();
                }
            }
        } catch (Exception e) {
            Log.log.warn("Error: " + e.getMessage(), e);
            try {
                Thread.sleep(interval);
            } catch (InterruptedException ie) {
                Log.log.warn("sleep interrupted: " + ie.getMessage(), ie);
            }
        }
        
//        System.out.println("Response is: " + result);
        
        return result;
    }

    @Override
    public void clearAndSetFilters(List<Map<String, Object>> filterList) {
        Log.log.debug("Calling Gateway specific clearAndSetFilter" + (filterList != null ? " " + filterList.size() + ", [" + filterList + "]" : ""));
        
        try {
            super.clearAndSetFilters(filterList);
            
            if(((ConfigReceiveWeblogicJMS)configurations).isActive() && isPrimary())
                deployFilter(filterList);
        } catch (Exception e) {
            Log.log.error(e.getMessage(), e);
        }
    }

    @Override
    protected void cleanFilter(ConcurrentHashMap<String, Filter> undeployedFilters)
    {
        if(((ConfigReceiveWeblogicJMS)configurations).isActive() && isPrimary()) {
            for(Filter filter : undeployedFilters.values())
            {
                WeblogicJMSFilter weblogicFilter = (WeblogicJMSFilter)filter;
                try
                {
                    WeblogicService.instance.deactivateFilter(weblogicFilter);
                    
                    deployedFilters.remove(weblogicFilter.getId());
                }
                catch (Exception e)
                {
                    Log.log.error(e.getMessage(), e);
                }
            }
        }
    }

    public List<Filter> getOrderedFilters() {
        return orderedFilters;
    }
    
    public void reinitialize() {
        
        super.reinitialize();
        
        //Add customized code here to initilize your server based on configuration and deployed filter data;

        if(isActive() && isPrimary()) {
            wlServerAlive = WeblogicJMSHeartbeat.sendHeartbeat();
            
            heartBeat = new WeblogicJMSHeartbeat(this);
            heartBeat.start();
        }
    }
    
    public void redeploy() {
        
        for(Filter filter : orderedFilters) {
            WeblogicJMSFilter weblogicFilter = (WeblogicJMSFilter)filter;
            String type = (String)weblogicFilter.getType();
            String filterName = (String)weblogicFilter.getId();
            
            if(StringUtils.isEmpty(type))
                continue;
                
            if(type.trim().equalsIgnoreCase(WeblogicJMSFilter.TOPIC)) {
                try {
                    WeblogicJMSAPI api = new WeblogicJMSAPI(weblogicFilter);
                    api.subscribeToTopic();
                    if(!deployedFilters.contains(filterName)) {
                    	deployedFilters.add(filterName);
                	}
                } catch(Exception e) {
                    e.printStackTrace();
                    Log.log.error(e.getMessage(), e);
                }
            }
            
            if(type.trim().equalsIgnoreCase(WeblogicJMSFilter.QUEUE)) {
                try {
                    WeblogicJMSAPI api = new WeblogicJMSAPI(weblogicFilter);
                    api.listenToQueue();
                    if(!deployedFilters.contains(filterName)) {
                    	deployedFilters.add(filterName);
                    }
                } catch(Exception e) {
                    e.printStackTrace();
                    Log.log.error(e.getMessage(), e);
                }
            }
        }
        
        Log.log.info("WeblogicJMSFilter is re-deployed.");
    }
    
    private void deployFilter(List<Map<String, Object>> filterList) throws Exception {
        //Add customized code here to initilize your server based on configuration and deployed filter data;
       
        if(filterList != null && filterList.size() != 0) {
            for(int i=0; i<filterList.size(); i++) {
                Map<String, Object> params = filterList.get(i);
                String filterName = (String)params.get(WeblogicJMSFilter.ID);
                if(StringUtils.isBlank(filterName))
                    continue;
                
                if(deployedFilters.contains(filterName))
                    continue;
                
                String type = (String)params.get(WeblogicJMSFilter.TYPE);
                
                if(StringUtils.isEmpty(type))
                    continue;
                
                if(type.trim().equalsIgnoreCase(WeblogicJMSFilter.TOPIC)) {
                    boolean retry = false;
                    long initTime = System.currentTimeMillis();
                    
                    WeblogicJMSAPI api = new WeblogicJMSAPI(getFilterByName(filterName));
                    
                    do {
                        try {
                            api.subscribeToTopic();
                            deployedFilters.add(filterName);
                            retry = false;
                        } catch(Exception e) {
                            Log.log.error("Failed to deploy filter: " + (String)params.get(WeblogicJMSFilter.ID));
                            String errorMsg = e.getMessage();
                            Log.log.error(errorMsg, e);
                            
                            if(StringUtils.isNotEmpty(errorMsg) && errorMsg.contains("JMSWMQ0026"))
                                retry = true;
                            Log.log.debug("retry = " + retry);
                            
                            if(!retry)
                                throw e;
                            
                            try {
                                if(System.currentTimeMillis() - initTime > getFailoverInterval()*3)
                                    throw e;
                                Thread.sleep(getHeartbeatInterval());
                            } catch(Exception ee) {}
                        }
                    } while(retry);
                }
                
                else if(type.trim().equalsIgnoreCase(WeblogicJMSFilter.QUEUE)) {
                    try {
                        WeblogicJMSAPI api = new WeblogicJMSAPI(getFilterByName(filterName));
                        api.listenToQueue();
                        deployedFilters.add(filterName);
                    } catch(Exception e) {
                        e.printStackTrace();
                        Log.log.error("Failed to deploy filter: " + (String)params.get(WeblogicJMSFilter.ID));
                        Log.log.error(e.getMessage(), e);
                        throw e;
                    }
                }
            }
        }
    }
    
    @Override
    public void deactivate() {
        
        super.deactivate();
        
        for(Filter filter : orderedFilters)
        {
            WeblogicJMSFilter webLogicFilter = (WeblogicJMSFilter)filter;
            try
            {
                WeblogicService.instance.deactivateFilter(webLogicFilter);
                
                deployedFilters.remove(webLogicFilter.getId());
            }
            catch (Exception e)
            {
                Log.log.error(e.getMessage(), e);
            }
        }
        
        if(heartBeat != null)
            heartBeat.stopHeartBeat();
    }
    
    @Override
    public void stop() {
        Log.log.warn("Stopping WeblogicJMS gateway ...");
        //Add customized code here to stop the connection with 3rd party system;
        super.stop();

        WeblogicService.instance.clearConnections();
        
        if(heartBeat != null)
            heartBeat.stopHeartBeat();
    }

    @Override
    public Filter getFilter(Map<String, Object> params)
    {
        return new WeblogicJMSFilter((String) params.get(BaseFilter.ID), (String) params.get(BaseFilter.ACTIVE), (String) params.get(BaseFilter.ORDER), (String) params.get(BaseFilter.INTERVAL), (String) params.get(BaseFilter.EVENT_EVENTID), (String) params.get(BaseFilter.RUNBOOK), (String) params.get(BaseFilter.SCRIPT), (String) params.get(WeblogicJMSFilter.ADDRESS), (String) params.get(WeblogicJMSFilter.VALUE), (String) params.get(WeblogicJMSFilter.TYPE),
                        (String) params.get(WeblogicJMSFilter.CLIENT),
                        (String) params.get(WeblogicJMSFilter.USERNAME),
                        (String) params.get(WeblogicJMSFilter.PASSWORD),
                        (String) params.get(WeblogicJMSFilter.JMSFACTORY));
    }
    
    public ConfigReceiveWeblogicJMS getWebJmsconfig() {
		return webJmsconfig;
	}
	public void setWebJmsconfig(ConfigReceiveWeblogicJMS webJmsconfig) {
		this.webJmsconfig = webJmsconfig;
	}

	public String getQueue()
    {
        return queue;
    }
    public void setQueue(String queue)
    {
        this.queue = queue;
    }

    public String getUser()
    {
        return user;
    }
    public String getPass()
    {
        return pass;
    }


    public String getJndiFactory()
    {
        return jndiFactory;
    }

    public void setAlive(int alive)
    {
        this.alive = alive;
    }
    
    public int getAlive()
    {
        return alive;
    }

    public static boolean isWlServerAlive()
    {
        return wlServerAlive;
    }

    public static void setWlServerAlive(boolean wlServerAlive)
    {
        WeblogicJMSGateway.wlServerAlive = wlServerAlive;
    }
    
}

