package com.resolve.services.hibernate.vo;

import com.resolve.util.MappingAnnotation;
import com.resolve.util.StringUtils;

public class WeblogicJMSFilterVO extends GatewayFilterVO {

    private static final long serialVersionUID = 1L;
    
    private static final int ADDRESS_NOT_AVAILABLE = -1;
    private static final String ADDRESS_NOT_AVAILABLE_ERR = "Address is not available.";
    
    private static final int TYPE_NOT_AVAILABLE = -2;
    private static final String TYPE_NOT_AVAILABLE_ERR = "Type is not available.";

    private static final int VALUE_NOT_AVAILABLE = -3;
    private static final String NAME_NOT_AVAILABLE_ERR = "Queue/Topic name is not available.";
    
    private static final int CLIENTID_NOT_AVAILABLE = -4;
    private static final String CLIENTID_NOT_AVAILABLE_ERR = "Client id must have value.";
    

    public WeblogicJMSFilterVO() {
    }

    private String UAddress;
    private String UUsername;
    private String UPassword;
    private String UJmsfactory;
    private String UValue;
    private String UType;
    private String UClient;
    
    
    public void setUClient(String uClient)
    {
        UClient = uClient;
    }
    
    @MappingAnnotation(columnName = "CLIENT")
    public String getUClient()
    {
        return UClient;
    }
    
    @MappingAnnotation(columnName = "USERNAME")
    public String getUUsername() {
        return this.UUsername;
    }
    
    public void setUUsername(String uUsername)
    {
        UUsername = uUsername;
    }
    
    @MappingAnnotation(columnName = "PASSWORD")
    public String getUPassword() {
        return this.UPassword;
    }
    
    public void setUPassword(String uPassword)
    {
        UPassword = uPassword;
    }
    
    @MappingAnnotation(columnName = "JMSFACTORY")
    public String getUJmsfactory() {
        return this.UJmsfactory;
    }
    
    public void setUJmsfactory(String uJmsfactory)
    {
        UJmsfactory = uJmsfactory;
    }
    
    @MappingAnnotation(columnName = "ADDRESS")
    public String getUAddress() {
        return this.UAddress;
    }

    public void setUAddress(String UAddress) {
        this.UAddress = UAddress;
    }

    @MappingAnnotation(columnName = "VALUE")
    public String getUValue() {
        return this.UValue;
    }

    public void setUValue(String uValue) {
        this.UValue = uValue;
    }

    @MappingAnnotation(columnName = "TYPE")
    public String getUType() {
        return this.UType;
    }

    public void setUType(String uType) {
        this.UType = uType;
    }
    
    @Override
    public int isValid() {

        int valid = 0;
        
        if(StringUtils.isBlank(UAddress))
            valid = ADDRESS_NOT_AVAILABLE;
        if(StringUtils.isBlank(UType))
            valid = TYPE_NOT_AVAILABLE;
        if(StringUtils.isBlank(UValue))
            valid = VALUE_NOT_AVAILABLE;
        if(StringUtils.isBlank(UClient))
            valid = CLIENTID_NOT_AVAILABLE;
        
        return valid;
    }
    
    @Override
    public String getValidationError(int errorCode)
    {
        String error = null;
        
        switch(errorCode)
        {
            case ADDRESS_NOT_AVAILABLE:
                error = ADDRESS_NOT_AVAILABLE_ERR;
                break;
            case TYPE_NOT_AVAILABLE:
                error = TYPE_NOT_AVAILABLE_ERR;
                break;
            case VALUE_NOT_AVAILABLE:
                error = NAME_NOT_AVAILABLE_ERR;
            case CLIENTID_NOT_AVAILABLE:
                error = CLIENTID_NOT_AVAILABLE_ERR;
                break;
            default:
                break;
        }
        
        return error;
    }
    
    @Override
    public int hashCode()
    {
        final int prime = 31;
        int result = super.hashCode();
        result = prime * result + ((UAddress == null) ? 0 : UAddress.hashCode());
        result = prime * result + ((UUsername == null) ? 0 : UUsername.hashCode());
        result = prime * result + ((UType == null) ? 0 : UType.hashCode());
        result = prime * result + ((UPassword == null) ? 0 : UPassword.hashCode());
        result = prime * result + ((UJmsfactory == null) ? 0 : UJmsfactory.hashCode());
        result = prime * result + ((UValue == null) ? 0 : UValue.hashCode());
        result = prime * result + ((UClient == null) ? 0 : UClient.hashCode());
        return result;
    }
}

