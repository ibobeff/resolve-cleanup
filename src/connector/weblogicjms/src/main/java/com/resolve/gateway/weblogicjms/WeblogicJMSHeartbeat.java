/******************************************************************************
* (C) Copyright 2014
*
* Resolve Systems, LLC
*
* All rights reserved
* This software is distributed under license and may not be used, copied,
* modified, or distributed without the express written permission of
* Resolve Systems, LLC.
*
******************************************************************************/

package com.resolve.gateway.weblogicjms;

import java.util.Map;
import java.util.Iterator;

import com.resolve.gateway.Filter;
import com.resolve.util.Log;

class WeblogicJMSHeartbeat extends Thread
{
    private final WeblogicJMSGateway gateway;
    
    public WeblogicJMSHeartbeat(WeblogicJMSGateway gateway)
    {
        this.gateway = gateway;
    }

    @Override
    public void run()
    {
        Log.log.info("Heartbeat is started.");
        
        for(;;) {
            try {
                if(gateway.isActive()) {
                    try
                    {
                        // Detect if the WeblogicJMS server is up and running by connecting to any queue manager in QUEUE mode
                        boolean alive = sendHeartbeat();
                        
                        String status = (alive)?"":" not";
                        Log.log.debug("Heart beat: " + System.currentTimeMillis());
                        
                        // If the WeblogicJMS server was up then down
                        if(WeblogicJMSGateway.isWlServerAlive()) {
                            if(!alive) {
                                Log.log.warn("WeblogicJMS server is down.");
                                WeblogicJMSGateway.setWlServerAlive(false);
                             // The current implementation is tested when the queue manager is stopped.
                             // Cannot disconnect, otherwise, when reconnect and set client id will fail.
                             //   MQService.instance.clearConnections();
                             // If the complete MQ server is down, the connections might be closed.
                             // May need  new implemenation and re-test.
                            }
                        }
                        
                        // When the WeblogicJMS server was down and now up again, re-deploy the subscribers
                        else {
                            if(alive) {
                                Log.log.warn("WeblogicJMS server is back up.");
                                //No metter which filters are going to be redployed clear all the coonections
                                WeblogicService.instance.clearConnections();
                                gateway.redeploy();
                                WeblogicJMSGateway.setWlServerAlive(true);
                            }
                        }
                    } catch(Exception e) {
                        Log.log.error(e.getMessage(), e);
                    }
                }
                
                int interval = gateway.getAlive();
                sleep(interval*1000);
            } 
            catch(InterruptedException e)
            {
                Log.log.error(e.getMessage(), e);
                break;
            }
        }
    }

    public static boolean sendHeartbeat()
    {
        boolean isAlive = true;
        
        Map<String, Filter> filterMap = WeblogicJMSGateway.getInstance().getFilters();
        
        try {
            for(Iterator it = filterMap.values().iterator(); it.hasNext();) {
                WeblogicJMSFilter filter = (WeblogicJMSFilter)it.next();
                WeblogicService.instance.heartbeat(filter);
                break;
            } 

        } catch(Exception e) {
            Log.log.error("Failed to establish connection with WeblogicJMS server.");
            Log.log.error(e.getMessage(), e);
            isAlive = false;
        }
        
        return isAlive;
    }
    
    public void stopHeartBeat() {
        
        try {
            this.interrupt();
            Log.log.info("Heartbeat is stopped.");
        } catch(Exception e) {
            Log.log.error(e.getMessage(), e);
        }
    }
    
}
