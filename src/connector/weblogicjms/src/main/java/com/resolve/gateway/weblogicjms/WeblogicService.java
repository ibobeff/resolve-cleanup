package com.resolve.gateway.weblogicjms;

import java.util.Hashtable;
import java.util.Iterator;
import java.util.Map;
import java.util.Set;
import java.util.concurrent.ConcurrentHashMap;

import javax.jms.Connection;
import javax.jms.ConnectionFactory;
import javax.jms.DeliveryMode;
import javax.jms.JMSException;
import javax.jms.Queue;
import javax.jms.QueueConnection;
import javax.jms.QueueConnectionFactory;
import javax.jms.QueueSender;
import javax.jms.QueueSession;
import javax.jms.Session;
import javax.jms.TextMessage;
import javax.jms.Topic;
import javax.jms.TopicConnection;
import javax.jms.TopicConnectionFactory;
import javax.jms.TopicPublisher;
import javax.jms.TopicSession;
import javax.naming.Context;
import javax.naming.InitialContext;
import javax.naming.NamingException;

import com.resolve.util.Crypt;
import com.resolve.util.CryptUtils;
import com.resolve.util.Log;
import com.resolve.util.StringUtils;

import groovyjarjarcommonscli.MissingArgumentException;

class WeblogicService
{
    public static final WeblogicService instance = new WeblogicService();
    private static WeblogicJMSGateway gatewayInstance = WeblogicJMSGateway.getInstance();

    private static volatile Map<String, WLSubscriber> subscribers = new ConcurrentHashMap<String, WLSubscriber>();
    private static volatile Map<String, WLReceiver> receivers = new ConcurrentHashMap<String, WLReceiver>();

    private static volatile Map<String, TopicConnection> topicConnections = new ConcurrentHashMap<String, TopicConnection>();
    private static volatile Map<String, QueueConnection> queueConnections = new ConcurrentHashMap<String, QueueConnection>();

    private static int retry = 5;
    private static int interval = 1000;

    public void sendMessage(String message, String filterName) throws Exception
    {
        this.sendMessage(message, gatewayInstance.getFilterByName(filterName));

    }

    public void sendMessage(String message, WeblogicJMSFilter filter) throws Exception
    {
        if(filter == null) {
            throw new MissingArgumentException("Filter argument must be initicialized.");
        }
        
        switch (filter.getType().toUpperCase())
        {
            case "TOPIC":
            {
                sendMessageToTopic(message, filter);
                break;
            }
            case "QUEUE":
            {
                sendMessageToQueue(message, filter);
                break;
            }
        }

    }

    private void sendMessageToTopic(String message, WeblogicJMSFilter weblogicJMSFilter)
    {
        
        String username = weblogicJMSFilter.getUsername().isEmpty() ? gatewayInstance.getUser() : weblogicJMSFilter.getUsername();
        String password = weblogicJMSFilter.getPassword().isEmpty() ? gatewayInstance.getPass() : weblogicJMSFilter.getPassword();
        sendMessageToTopic(weblogicJMSFilter.getAddress(), username, password, weblogicJMSFilter.getJmsfactory(), weblogicJMSFilter.getValue(), message);
    }

    public void sendMessageToTopic(String address, String username, String password, String jmsFactory, String topicName, String message)
    {
    	String url = getJMSWeblogicURL(address);
    	
        this.sendMessageToTopic(url, gatewayInstance.getJndiFactory(), jmsFactory, username, password, topicName, message);
    }

    public void sendMessageToTopic(String url, String initContextFactory, String jmsFactory,
    				String user, String pass, String topicName, String message)
    {
        try
        {
            // Create and start connection
            InitialContext ic = createInitialContext(url, initContextFactory, user, pass);
            
            TopicConnectionFactory topicConnectionFactory = (TopicConnectionFactory) ic.lookup(jmsFactory);
            
            TopicConnection connection = topicConnectionFactory.createTopicConnection();
            connection.start();

            // 2) create topic session
            TopicSession ses = connection.createTopicSession(false, Session.AUTO_ACKNOWLEDGE);

            // 3) get the Topic object
            Topic topic = (Topic) ic.lookup(topicName);

            // 4)create topic publisher
            TopicPublisher topicPublisher = ses.createPublisher(topic);

            // This is the lowest-overhead delivery mode because it does not
            // require that the message be logged to stable storage.
            topicPublisher.setDeliveryMode(DeliveryMode.NON_PERSISTENT);

            // 5) create TextMessage object
            TextMessage msg = ses.createTextMessage();
            msg.setText(message);

            // 7) send message
            topicPublisher.publish(msg);

            System.out.println("Message successfully published: " + msg);

            // 8) connection close
            connection.close();
        }
        catch (Exception e)
        {
            e.printStackTrace();
            Log.log.error("Error while sending message: " + e.getMessage(), e);
        }
    }

    private void sendMessageToQueue(String message, WeblogicJMSFilter weblogicJMSFilter)
    {
        String username = weblogicJMSFilter.getUsername().isEmpty() ? gatewayInstance.getUser() : weblogicJMSFilter.getUsername();
        String password = weblogicJMSFilter.getPassword().isEmpty() ? gatewayInstance.getPass() : weblogicJMSFilter.getPassword();
        
        sendMessageToQueue(weblogicJMSFilter.getAddress(), weblogicJMSFilter.getJmsfactory(), username, password, weblogicJMSFilter.getValue(), message);
    }

    public void sendMessageToQueue(String address, String jmsfactory, String username, String password, String queueName, String message)
    {
    	String url = getJMSWeblogicURL(address);
        this.sendMessageToQueue(url, gatewayInstance.getJndiFactory(), jmsfactory, username, password, queueName, message);
    }

    public void sendMessageToQueue(String url, String initContextFactory, String jmsFactory, String user, String pass, String queueName, String message)
    {
        try
        {
            InitialContext ic = createInitialContext(url, initContextFactory, user, pass);
            QueueConnectionFactory qconFactory = (QueueConnectionFactory) ic.lookup(jmsFactory);
            QueueConnection qcon = qconFactory.createQueueConnection();

            QueueSession qsession = qcon.createQueueSession(false, Session.AUTO_ACKNOWLEDGE);
            Queue queue = (Queue) ic.lookup(queueName);
            QueueSender qsender = qsession.createSender(queue);
            qsender.setDeliveryMode(DeliveryMode.NON_PERSISTENT);

            TextMessage msg = qsession.createTextMessage();

            qcon.start();

            msg.setText(message);
            qsender.send(msg);

            qsender.close();
            qsession.close();
            qcon.close();

        }
        catch (Exception e)
        {
            e.printStackTrace();
            Log.log.error("Error while sending message: " + e.getMessage(), e);
        }
    }

    public void startTopicSubscriber(String filterName) throws Exception
    {
        startTopicSubscriber(gatewayInstance.getFilterByName(filterName));
    }

    public void startTopicSubscriber(WeblogicJMSFilter webLogicFilter) throws Exception
    {
        if(webLogicFilter == null) {
            throw new MissingArgumentException("Filter argument must be initicialized.");
        }
        
        TopicConnection connection = null;
        TopicSession session = null;

        String topicName = webLogicFilter.getValue();
        if (StringUtils.isBlank(topicName)) throw new Exception("Topic name is not available.");

        String subscriptionName = getSubscriptionName(webLogicFilter);

        WLSubscriber subscriber = subscribers.get(subscriptionName);

        if (subscriber != null)
        {
            return;
        }

        try
        {
            connection = getTopicConnection(webLogicFilter);
            session = connection.createTopicSession(false, Session.AUTO_ACKNOWLEDGE);

            InitialContext ic = createInitialContext(webLogicFilter);
            

            String filterName = webLogicFilter.getId();
            subscriber = new WLSubscriber(ic, connection, session, topicName, filterName, subscriptionName);
            subscriber.startListening();

            subscribers.put(subscriptionName, subscriber);
        }
        catch (Exception e)
        {
            e.printStackTrace();
            Log.log.error(e.getMessage(), e);
            throw e;
        }
    }

    public void startQueueReceiver(String filterName) throws Exception
    {
        startQueueReceiver(gatewayInstance.getFilterByName(filterName));
    }

    public void startQueueReceiver(WeblogicJMSFilter weblogicFilter) throws Exception
    {
        if(weblogicFilter == null) {
            throw new MissingArgumentException("Filter argument must be initicialized.");
        }
        
        QueueConnection connection = null;
        QueueSession session = null;

        String queueName = weblogicFilter.getValue();
        if (StringUtils.isBlank(queueName)) throw new Exception("Queue name is not available.");

        String subscriptionName = getSubscriptionName(weblogicFilter);

        WLReceiver receiver = receivers.get(subscriptionName);

        /*
         * This way cannot make the receiver to start listen again after a
         * server down if(receiver != null) { // This is not working because the
         * async session was still open with its open connection // The
         * connection cannot be closed because it is still associated with the
         * queue manager // even if the server was down
         * receiver.startReceiving(); // This way basic does not restart to
         * listen after a server down
         * receiver.getReceiver().setMessageListener(receiver); return; }
         */

        try
        {
            connection = getQueueConnection(weblogicFilter);
            queueConnections.put(getSubscriptionName(weblogicFilter), connection);

            session = (QueueSession) connection.createQueueSession(false, Session.AUTO_ACKNOWLEDGE);

            InitialContext ic = createInitialContext(weblogicFilter);
            

            String filterName = weblogicFilter.getId();
            receiver = new WLReceiver(connection, session, queueName, filterName);
            receiver.startReceiving(ic);

            receivers.put(subscriptionName, receiver);
        }
        catch (Exception e)
        {
            e.printStackTrace();
            Log.log.error(e.getMessage(), e);
            throw e;
        }

    }

    private QueueConnection getQueueConnection(WeblogicJMSFilter weblogicJMSFilter) throws Exception
    {

        QueueConnection connection = null;

        try
        {
            InitialContext ic = createInitialContext(weblogicJMSFilter);

            QueueConnectionFactory qConFactoryQueue = (QueueConnectionFactory) ic.lookup(weblogicJMSFilter.getJmsfactory());
            connection = qConFactoryQueue.createQueueConnection();
            connection.start();
        }
        catch (Throwable t)
        {
            Log.log.error("Failed to create new connection to queue: " + weblogicJMSFilter.getValue() + " with ClientID: " +  weblogicJMSFilter.getClient());
            Log.log.error(t.getMessage(), t);
            throw t;
        }

        return connection;
    }

    public void stopQueueReceiver(String filterName) throws Exception
    {
        stopQueueReceiver(gatewayInstance.getFilterByName(filterName));
    }

    public void stopQueueReceiver(WeblogicJMSFilter webLogicFilter) throws Exception
    {
        
        if(webLogicFilter == null) {
            throw new MissingArgumentException("Filter argument must be initicialized.");
        }
        
        String subscriptionName = getSubscriptionName(webLogicFilter);
        WLReceiver receiver = receivers.get(subscriptionName);

        if (receiver == null) {
        	Log.log.error("Cannot find receiver with name: " + subscriptionName);
        } else {
            receiver.stopReceiving();
        }
        
        receivers.remove(subscriptionName);
        queueConnections.remove(subscriptionName);
    }

    public void stopTopicSubscriber(String filterName) throws Exception
    {
        stopTopicSubscriber(gatewayInstance.getFilterByName(filterName));
    }

    private void stopTopicSubscriber(WeblogicJMSFilter webLogicFilter) throws Exception
    {
        
        if(webLogicFilter == null) {
            throw new MissingArgumentException("Filter argument must be initicialized.");
        }
        
        String subscriptionName = getSubscriptionName(webLogicFilter);
        WLSubscriber subscriber = subscribers.get(subscriptionName);

        if (subscriber == null) {
        	Log.log.error("Cannot find subscriber with name: " + subscriptionName);
        } else {
        	subscriber.stopListening();
        }
        
        subscribers.remove(subscriptionName);
        topicConnections.remove(subscriptionName);

    }

    public void deactivateFilter(WeblogicJMSFilter webLogicFilter) throws Exception
    {
        if (webLogicFilter.getType().equalsIgnoreCase(WeblogicJMSFilter.TOPIC))
        {
            stopTopicSubscriber(webLogicFilter);
        }

        if (webLogicFilter.getType().equalsIgnoreCase(WeblogicJMSFilter.QUEUE))
        {
            stopQueueReceiver(webLogicFilter);
        }

    }

    public TopicConnection getTopicConnection(WeblogicJMSFilter weblogicFilter) throws Exception
    {

        String key = getSubscriptionName(weblogicFilter);

        TopicConnection connection = topicConnections.get(key);
        if (connection == null) connection = getNewTopicConnection(weblogicFilter);

        int i = 0;

        while (i < retry)
        {
            try
            {
                connection.start();
                break;
            }
            catch (Exception e)
            {
            	if(i == (retry - 1)) {
            		Log.log.error("Cannot connect to Weblogic Server: it's probably down, tried " + (retry+1) + " times");
            		Log.log.error(e.getMessage(), e);
            	}else {
            		Log.log.info( "++++++++++++++++++++++++++++++++++++++++++++++++++++++++");
            		Log.log.info( (i + 1) + "th attempt for connecting to Weblogic failed");
            		Log.log.info( "++++++++++++++++++++++++++++++++++++++++++++++++++++++++");
            	}
                i++;
                Thread.sleep(interval);
                connection = getNewTopicConnection(weblogicFilter);
                continue;
            }
            catch (Throwable t)
            {
            	if(i == (retry - 1)) {
            		Log.log.error("Cannot connect to Weblogic Server: it's probably down, tried " + (retry+1) + " times");
            		t.printStackTrace();
                    Log.log.error(t.getMessage());
            	}else {
            		Log.log.info( "++++++++++++++++++++++++++++++++++++++++++++++++++++++++");
            		Log.log.info( (i + 1) + "th attempt for connecting to Weblogic failed");
            		Log.log.info( "++++++++++++++++++++++++++++++++++++++++++++++++++++++++");
            	}
                i++;
                Thread.sleep(interval);
                continue;
            }
        }

        if (i == retry)
        {
            topicConnections.remove(key);
            throw new Exception("Failed to create topic connection.");
        }

        return connection;
    }

    public TopicConnection getNewTopicConnection(WeblogicJMSFilter weblogicFilter) throws Exception
    {

        TopicConnection connection = null;

        try
        {
            InitialContext ic = createInitialContext(weblogicFilter);

            TopicConnectionFactory topicConnectionFactory = (TopicConnectionFactory) ic.lookup(weblogicFilter.getJmsfactory());
            connection = topicConnectionFactory.createTopicConnection();

            String clientId = weblogicFilter.getClient();
            connection.setClientID(clientId);
            topicConnections.put(getSubscriptionName(weblogicFilter), connection);
        }
        catch (Throwable t)
        {
            Log.log.error("Failed to create new connection to topic: " + weblogicFilter.getValue() + " with ClientID: " +  weblogicFilter.getClient());
            Log.log.error(t.getMessage(), t);
            throw t;
        }

        return connection;
    }

    private InitialContext createInitialContext(WeblogicJMSFilter weblogicFilter) throws Exception
    {
        String url = getJMSWeblogicURL(weblogicFilter.getAddress()) ;
        String username = weblogicFilter.getUsername();
        String password = weblogicFilter.getPassword();
        
        if(username.isEmpty()) {
            username = gatewayInstance.getUser();
        }
        
        if(password.isEmpty()) {
            password = gatewayInstance.getPass();
        }
        
        return createInitialContext(url, gatewayInstance.getJndiFactory(), username, password);
    }

    private InitialContext createInitialContext(String url, String initContextFactory, String user, String pass) throws Exception
    {
        try {
            Hashtable<String, String> env = new Hashtable<>();
            env.put(Context.INITIAL_CONTEXT_FACTORY, initContextFactory);
            env.put(Context.PROVIDER_URL, url);
            env.put(Context.SECURITY_PRINCIPAL, user);
            env.put(Context.SECURITY_CREDENTIALS, CryptUtils.decrypt(pass));
            
            InitialContext initialContext = new InitialContext(env);
            Log.log.info("Initial context successfully created for PROVIDER_URL: " + url + " SECURITY_PRINCIPAL: " + user + " INITIAL_CONTEXT_FACTORY: " + initContextFactory);
            
            return initialContext;
        } catch (Exception e) {
            Log.log.error("Initial context FAILED to be created for PROVIDER_URL: " + url + " SECURITY_PRINCIPAL: " + user + " INITIAL_CONTEXT_FACTORY: " + initContextFactory);
            Log.log.error(e.getMessage(), e);
            throw e;
        }
        
    }
    
    private String getSubscriptionName(WeblogicJMSFilter weblogicFilter)
    {
        String name = weblogicFilter.getValue();
        String filterName = weblogicFilter.getId();
        return name + "_" + filterName;
    }


    public void clearConnections()
    {

        try
        {
            Set<String> subs = subscribers.keySet();

            for (Iterator it = subs.iterator(); it.hasNext();)
            {
                String key = (String) it.next();
                WLSubscriber subscriber = subscribers.get(key);
                if (subscriber != null)
                {
                    try
                    {
                        subscriber.stopListening();
                    }
                    catch (Exception e)
                    {
                        Log.log.error(e.getMessage(), e);
                    }
                }

                subscribers.remove(key);
            }

            Set<String> topicConn = topicConnections.keySet();

            for (Iterator it = topicConn.iterator(); it.hasNext();)
            {
                String key = (String) it.next();
                Connection connection = topicConnections.get(key);
                if (connection != null)
                {
                    try
                    {
                        connection.stop();
                        connection.close();
                        connection = null;
                    }
                    catch (Exception e)
                    {
                        System.err.println(e.getMessage());
                    }
                }

                topicConnections.remove(key);
            }

            Set<String> recs = receivers.keySet();

            for (Iterator it = recs.iterator(); it.hasNext();)
            {
                String key = (String) it.next();
                WLReceiver receiver = receivers.get(key);
                if (receiver != null)
                {
                    try
                    {
                        receiver.stopReceiving();
                    }
                    catch (Exception e)
                    {
                        Log.log.error(e.getMessage(), e);
                    }
                }

                receivers.remove(key);
            }

            Set<String> queueConn = queueConnections.keySet();

            for (Iterator it = queueConn.iterator(); it.hasNext();)
            {
                String key = (String) it.next();
                Connection connection = queueConnections.get(key);
                if (connection != null)
                {
                    try
                    {
                        connection.stop();
                        connection.close();
                        connection = null;
                    }
                    catch (Exception e)
                    {
                        System.err.println(e.getMessage());
                    }
                }

                queueConnections.remove(key);
            }

        }
        catch (Exception e)
        {
            Log.log.error(e.getMessage(), e);
        }
    }

    /**
     * Utility function to construct the URL based on the blueprint settings 
     * @param address
     * @return
     */
    private String getJMSWeblogicURL(String address) {
    	
    	String url = "t3://" + address;;
    	if(gatewayInstance.getWebJmsconfig().getSecured() != null &&
    			gatewayInstance.getWebJmsconfig().getSecured().equalsIgnoreCase("true")) {
    		url = "t3s://" + address;
    		//Set the public certifcates if they are set in the blueprint
    		setPublicCertificates();
    	}
    	return url;
    }
    
    /**
     * Utility function to add the custom trust certificate key to the JVM
     * System path
     */
    private void setPublicCertificates() {
    	
    	if(gatewayInstance.getWebJmsconfig().getTrustkeystore() != null && 
    		gatewayInstance.getWebJmsconfig().getTrustkeystore().trim() != "" )
    		System.setProperty("weblogic.security.TrustKeyStore", 
    				gatewayInstance.getWebJmsconfig().getTrustkeystore());
    	
    	if(gatewayInstance.getWebJmsconfig().getCustomkeystorefile() != null &&
    		gatewayInstance.getWebJmsconfig().getCustomkeystorefile().trim() != "")
    		System.setProperty("weblogic.security.CustomTrustKeyStoreFileName", 
    				gatewayInstance.getWebJmsconfig().getCustomkeystorefile());
    	
    	if(gatewayInstance.getWebJmsconfig().getCustomkeystorepass() != null &&
    		gatewayInstance.getWebJmsconfig().getCustomkeystorepass().trim() != "")
    		System.setProperty("weblogic.security.CustomTrustKeyStorePassPhrase", 
    				gatewayInstance.getWebJmsconfig().getCustomkeystorepass());
    	
    	if(gatewayInstance.getWebJmsconfig().getCustomkeystortype() != null &&
    		gatewayInstance.getWebJmsconfig().getCustomkeystortype().trim() != "")
    		System.setProperty("weblogic.security.CustomTrustKeyStoreType", 
    				gatewayInstance.getWebJmsconfig().getCustomkeystortype());
    	
    }
    
    public void heartbeat(WeblogicJMSFilter filter) throws Exception
    {
        InitialContext ic = createInitialContext(filter);
        ConnectionFactory connectionFactory = (ConnectionFactory)ic.lookup(filter.getJmsfactory());
        Connection connection = connectionFactory.createConnection();
        connection.start();
        connection.stop();
        connection.close();
    }
    
    public boolean heartbeat(String url, String initContextFactory, String user, String pass, String jmsFactory) {
        try {
            InitialContext ic = createInitialContext(url, initContextFactory, user, pass);
            ConnectionFactory connectionFactory = (ConnectionFactory)ic.lookup(jmsFactory);
            Connection connection = connectionFactory.createConnection();
            connection.start();
            connection.stop();
            connection.close();
            
            return true;
        } catch (Exception e) {
            Log.log.error("Error while trying to get connection to WL for url: " + url + " USER: " + user + "JMS Factory: " + jmsFactory);
            Log.log.error(e.getMessage(), e);
            return false;
        }
    }
}
