package com.resolve.gateway;

import java.util.Map;

import com.resolve.gateway.weblogicjms.WeblogicJMSFilter;
import com.resolve.gateway.weblogicjms.WeblogicJMSGateway;

public class MWeblogicJMS extends MGateway {

    private static final String RSCONTROL_SET_FILTERS = "MWeblogicJMS.setFilters";

    private static final WeblogicJMSGateway instance = WeblogicJMSGateway.getInstance();

    public MWeblogicJMS() {
        super.instance = instance;
    }

    protected String getSetFilterMethodName() {
        return RSCONTROL_SET_FILTERS;
    }

    /**
     * Populates the {@link Filter} data into the given filterMap {@link Map} instance
     * 
     * @param filterMap
     *      the {@link Map} instance to hold the {@link Filter} data
     * @param filter
     *      a {@link WeblogicJMSFilter} instance
     *            
     */
    public void populateGatewaySpecificFilterProperties(Map<String, String> filterMap, Filter filter) {
        if (instance.isActive()) {
            WeblogicJMSFilter weblogicJMSFilter = (WeblogicJMSFilter) filter;
            filterMap.put(WeblogicJMSFilter.ADDRESS, weblogicJMSFilter.getAddress());
            filterMap.put(WeblogicJMSFilter.USERNAME, weblogicJMSFilter.getUsername());
            filterMap.put(WeblogicJMSFilter.PASSWORD, weblogicJMSFilter.getPassword());
            filterMap.put(WeblogicJMSFilter.JMSFACTORY, weblogicJMSFilter.getJmsfactory());
            filterMap.put(WeblogicJMSFilter.CLIENT, weblogicJMSFilter.getClient());
            filterMap.put(WeblogicJMSFilter.VALUE, weblogicJMSFilter.getValue());
            filterMap.put(WeblogicJMSFilter.TYPE, weblogicJMSFilter.getType());
        }
    }
}

