package com.resolve.gateway.weblogicjms;

public enum SubscriptionType
{
    TOPIC("TOPIC"), 
    QUEUE("QUEUE");

    private String name;
    
    private SubscriptionType(String name)
    {
        this.name = name;
    }


    public String getName()
    {
        return name;
    }
}
   


