/******************************************************************************
* (C) Copyright 2014
*
* Resolve Systems, LLC
*
* All rights reserved
* This software is distributed under license and may not be used, copied,
* modified, or distributed without the express written permission of
* Resolve Systems, LLC.
*
******************************************************************************/

package com.resolve.gateway.weblogicjms;

import javax.jms.Message;
import javax.jms.MessageListener;
import javax.jms.TextMessage;
import javax.jms.Topic;
import javax.jms.TopicConnection;
import javax.jms.TopicSession;
import javax.jms.TopicSubscriber;
import javax.naming.InitialContext;

import com.resolve.util.Log;
import com.resolve.util.StringUtils;

import weblogic.jms.extensions.WLSession;

class WLSubscriber extends MessageConsumer implements MessageListener {

    private InitialContext ic;
    private TopicConnection connection;
    private TopicSession session;
    private String topicName;
    private String filterName;
    private String subscriptionName;
    
    private TopicSubscriber subscriber;

    
    public WLSubscriber() {}
    
    public WLSubscriber(InitialContext ic, TopicConnection connection, TopicSession session, String topicName, String filterName, String subscriptionName) {
        super();
        this.connection = connection;
        this.session = session;
        this.topicName = topicName;
        this.filterName = filterName;
        this.subscriptionName = subscriptionName;
        this.ic = ic;
    }

    public void startListening() throws Exception {
        
        try {
        	Topic topic = (Topic)ic.lookup(topicName);
            
            if(StringUtils.isBlank(subscriptionName))
                subscriber = (TopicSubscriber)session.createSubscriber(topic);
            else
                subscriber = (TopicSubscriber)session.createDurableSubscriber(topic, subscriptionName);
            
            subscriber.setMessageListener(this);
            
            Log.log.info("Subscriber " + subscriptionName + " is started.");
        } catch(Exception e) {
            Log.log.error("Failed to start listening for data on TOPIC: " + topicName);
            e.printStackTrace();
            Log.log.error(e.getMessage(), e);
            throw e;
        }
    }
    
    public void onMessage(Message message) {
        
        String msgId = null;
        String msgType = null;
        String msgBody = null;
        String msgReceived = null;
        
        try {
            msgType = message.getJMSType();
            msgId = message.getJMSMessageID();
            
            if(message instanceof TextMessage) {
                msgBody = ((TextMessage)message).getText();
                msgReceived = message.toString();
                Log.log.debug("Received message <" + msgBody + "> with ID <" + msgId + ">");
            }
            
            if(!processMessage(filterName, msgId, msgType, msgBody, msgReceived)) {
                Log.log.debug(msgReceived);
                Log.log.debug("Message type is: " + msgType);
                Log.log.error("Failed to process message.");
            }
        } catch (Exception e) {
            e.printStackTrace();
            Log.log.debug(message);
            Log.log.error(e.getMessage(), e);
        }
    }
    
    public void stopListening() throws Exception {

        try {
            if(subscriber != null)
                subscriber.close();
            
            if(session != null) {
                Topic topic = (Topic) ic.lookup(topicName);
                ((WLSession)session).unsubscribe(topic, subscriptionName);
                session.close();
            }
            
            Log.log.info("Stop listening: " + subscriptionName);
            
            if(connection != null) {
               connection.stop();
               connection.close();
            } 
        } catch(Exception e) {
            Log.log.error("Failed to stop listening for data on TOPIC: " + topicName);
            Log.log.error(e.getMessage(), e);
            throw e;
        }
        
        Log.log.info("Subscriber " + subscriptionName + " stopped.");
    }
    
    
}