package com.resolve.gateway.weblogicjms;

import java.io.File;
import java.util.HashMap;
import java.util.Map;
import org.apache.commons.io.FileUtils;
import com.resolve.gateway.Filter;
import com.resolve.rsbase.MainBase;
import com.resolve.rsremote.ConfigReceiveGateway;
import com.resolve.util.Log;
import com.resolve.util.ObjectProperties;
import com.resolve.util.StringUtils;
import com.resolve.util.XDoc;

public class ConfigReceiveWeblogicJMS extends ConfigReceiveGateway {

    private static final long serialVersionUID = 1L;

    private static final String RECEIVE_WEBLOGICJMS_NODE = "./RECEIVE/WEBLOGICJMS/";

    private static final String RECEIVE_WEBLOGICJMS_FILTER = RECEIVE_WEBLOGICJMS_NODE + "FILTER";

    private String queue = "WEBLOGICJMS";

    private static final String RECEIVE_WEBLOGICJMS_ATTR_ALIVE = RECEIVE_WEBLOGICJMS_NODE + "@ALIVE";

    private static final String RECEIVE_WEBLOGICJMS_ATTR_USERNAME = RECEIVE_WEBLOGICJMS_NODE + "@USERNAME";

    private static final String RECEIVE_WEBLOGICJMS_ATTR_PASSWORD = RECEIVE_WEBLOGICJMS_NODE + "@PASSWORD";
    
    private static final String RECEIVE_WEBLOGICJMS_ATTR_JNDIFACTORY = RECEIVE_WEBLOGICJMS_NODE + "@JNDIFACTORY";

    private static final String RECEIVE_WEBLOGICJMS_ATTR_SECURED = RECEIVE_WEBLOGICJMS_NODE + "@SECURED";

    private static final String RECEIVE_WEBLOGICJMS_ATTR_TRUSTKEYSTORE = RECEIVE_WEBLOGICJMS_NODE + "@TRUSTKEYSTORE";
    private static final String RECEIVE_WEBLOGICJMS_ATTR_CUSTOMKEYSTOREFILE = RECEIVE_WEBLOGICJMS_NODE + "@CUSTOMKEYSTOREFILE";
    private static final String RECEIVE_WEBLOGICJMS_ATTR_CUSTOMKEYSTOREPASS = RECEIVE_WEBLOGICJMS_NODE + "@CUSTOMKEYSTOREPASS";
    private static final String RECEIVE_WEBLOGICJMS_ATTR_CUSTOMKEYSTORETYPE = RECEIVE_WEBLOGICJMS_NODE + "@CUSTOMKEYSTORETYPE";
    
    private String alive = "";

    private String username = "";

    private String p_assword = "";
    
    private String jndifactory = "";
    
    private String secured = "";
    private String trustkeystore = "";
    private String customkeystorefile = "";
    private String customkeystorepass = "";
    private String customkeystortype = "";

    public String getQueue() {
        return queue;
    }

    public void setQueue(String queue) {
        this.queue = queue;
    }

    public String getAlive() {
        return this.alive;
    }

    public void setAlive(String alive) {
        this.alive = alive;
    }

    public String getUsername() {
        return this.username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPassword() {
        return this.p_assword;
    }

    public void setPassword(String password) {
        this.p_assword = password;
    }

    public String getSecured() {
		return secured;
	}

	public void setSecured(String secured) {
		this.secured = secured;
	}

	public String getTrustkeystore() {
		return trustkeystore;
	}

	public void setTrustkeystore(String trustkeystore) {
		this.trustkeystore = trustkeystore;
	}

	public String getCustomkeystorefile() {
		return customkeystorefile;
	}

	public void setCustomkeystorefile(String customkeystorefile) {
		this.customkeystorefile = customkeystorefile;
	}

	public String getCustomkeystorepass() {
		return customkeystorepass;
	}

	public void setCustomkeystorepass(String customkeystorepass) {
		this.customkeystorepass = customkeystorepass;
	}

	public String getCustomkeystortype() {
		return customkeystortype;
	}

	public void setCustomkeystortype(String customkeystortype) {
		this.customkeystortype = customkeystortype;
	}

	public void setJndifactory(String jndifactory)
    {
        this.jndifactory = jndifactory;
    }
    public String getJndifactory()
    {
        return jndifactory;
    }
    public ConfigReceiveWeblogicJMS(XDoc config) throws Exception {
        
        super(config);
        
        define("alive", STRING, RECEIVE_WEBLOGICJMS_ATTR_ALIVE);
        define("username", STRING, RECEIVE_WEBLOGICJMS_ATTR_USERNAME);
        define("password", SECURE, RECEIVE_WEBLOGICJMS_ATTR_PASSWORD);
        define("jndifactory",STRING, RECEIVE_WEBLOGICJMS_ATTR_JNDIFACTORY);
        
        define("secured",STRING, RECEIVE_WEBLOGICJMS_ATTR_SECURED);
        define("trustkeystore",STRING, RECEIVE_WEBLOGICJMS_ATTR_TRUSTKEYSTORE);
        define("customkeystorefile",STRING, RECEIVE_WEBLOGICJMS_ATTR_CUSTOMKEYSTOREFILE);
        define("customkeystorepass",STRING, RECEIVE_WEBLOGICJMS_ATTR_CUSTOMKEYSTOREPASS);
        define("customkeystortype",STRING, RECEIVE_WEBLOGICJMS_ATTR_CUSTOMKEYSTORETYPE);
        
    }

    @Override
    public String getRootNode() {
        return RECEIVE_WEBLOGICJMS_NODE;
    }

    @Override
    public void load() throws Exception {
        try {
            loadAttributes();
            if (isActive()) {
                WeblogicJMSGateway weblogicJMSGateway = WeblogicJMSGateway.getInstance(this);
                filters = xdoc.getListMapValue(RECEIVE_WEBLOGICJMS_FILTER);
                if (filters.size() > 0) {
                    for (Map<String, Object> values : filters) {
                        String filterName = ((String) values.get(WeblogicJMSFilter.ID)).toLowerCase();
                        File scriptFile = getFile(MainBase.main.release.serviceName + "/config/weblogicjms/" + filterName + ".groovy");
                        try {
                            if (scriptFile.exists()) {
                                String groovy = FileUtils.readFileToString(scriptFile, "UTF-8");
                                if (StringUtils.isNotBlank(groovy)) {
                                    values.put(WeblogicJMSFilter.SCRIPT, groovy);
                                    Log.log.debug("Loaded groovy script: " + scriptFile);
                                }
                            }
                            weblogicJMSGateway.setFilter(values);
                        } catch (Exception e) {
                            Log.log.warn(e.getMessage() + ". Skipping file: " + scriptFile.getAbsolutePath());
                        }
                    }
                }
            }
        } catch (Throwable e) {
            setActive(false);
            Log.log.warn("Couldn not load configuration for WeblogicJMS gateway, check blueprint. " + e.getMessage(), e);
        }
    }

    @Override
    public void save() throws Exception {
        File dir = getFile(MainBase.main.release.serviceName + "/config/weblogicjms");
        if (!dir.exists()) {
            FileUtils.forceMkdir(dir);
        }
        if (filters != null && isActive()) {
            filters.clear();
            for (Filter filter : WeblogicJMSGateway.getInstance().getFilters().values()) {
                WeblogicJMSFilter weblogicJMSFilter = (WeblogicJMSFilter) filter;
                String groovy = weblogicJMSFilter.getScript();
                String scriptFileName;
                File scriptFile = null;
                if (StringUtils.isNotBlank(groovy)) {
                    scriptFileName = weblogicJMSFilter.getId().toLowerCase() + ".groovy";
                    scriptFile = getFile(MainBase.main.release.serviceName + "/config/weblogicjms/" + scriptFileName);
                }
                Map<String, Object> entry = new HashMap<String, Object>();
                putFilterDataIntoEntry(entry, weblogicJMSFilter);
                filters.add(entry);
                if (scriptFile != null) {
                    try {
                        FileUtils.writeStringToFile(scriptFile, groovy, "UTF-8");
                    } catch (Exception e) {
                        Log.log.warn(e.getMessage() + ". Skipping file: " + scriptFile.getAbsolutePath());
                    }
                }
            }
            xdoc.setListMapValue(RECEIVE_WEBLOGICJMS_FILTER, filters);
        }
        if (nameProperties != null && isActive()) {
            nameProperties.clear();
            for (String name : WeblogicJMSGateway.getInstance().getNameProperties().keySet()) {
                Map<String, Object> entry = new HashMap<String, Object>();
                entry.put(NAME_KEY, name);
                nameProperties.add(entry);
                Map props = WeblogicJMSGateway.getInstance().getNameProperties().getNameProperties(name);
                ObjectProperties prop = new ObjectProperties(props);
                prop.save(props);
            }
        }
        saveAttributes();
    }

    private void putFilterDataIntoEntry(Map<String, Object> entry, WeblogicJMSFilter weblogicJMSFilter) {
        entry.put(WeblogicJMSFilter.ID, weblogicJMSFilter.getId());
        entry.put(WeblogicJMSFilter.ACTIVE, String.valueOf(weblogicJMSFilter.isActive()));
        entry.put(WeblogicJMSFilter.ORDER, String.valueOf(weblogicJMSFilter.getOrder()));
        entry.put(WeblogicJMSFilter.INTERVAL, String.valueOf(weblogicJMSFilter.getInterval()));
        entry.put(WeblogicJMSFilter.EVENT_EVENTID, weblogicJMSFilter.getEventEventId());
        entry.put(WeblogicJMSFilter.RUNBOOK, weblogicJMSFilter.getRunbook());
        entry.put(WeblogicJMSFilter.SCRIPT, weblogicJMSFilter.getScript());
        
        entry.put(WeblogicJMSFilter.ADDRESS, weblogicJMSFilter.getAddress());
        entry.put(WeblogicJMSFilter.VALUE, weblogicJMSFilter.getValue());
        entry.put(WeblogicJMSFilter.TYPE, weblogicJMSFilter.getType());
        entry.put(WeblogicJMSFilter.CLIENT, weblogicJMSFilter.getClient());
        entry.put(WeblogicJMSFilter.USERNAME, weblogicJMSFilter.getUsername());
        entry.put(WeblogicJMSFilter.PASSWORD, weblogicJMSFilter.getPassword());
        entry.put(WeblogicJMSFilter.JMSFACTORY, weblogicJMSFilter.getJmsfactory());
    }
}

