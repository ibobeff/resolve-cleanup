package com.resolve.gateway.weblogicjms;

import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.Callable;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.FutureTask;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.TimeoutException;

import com.resolve.util.Log;
import com.resolve.util.SystemUtil;

import net.sf.json.JSONObject;


class MessageConsumer
{
    
    private static long waitTime = 2000;
    protected ExecutorService executor = Executors.newFixedThreadPool(SystemUtil.getCPUCount());
    
    protected boolean processMessage(String filterName, String id , String type, String body, String msg) {

        WeblogicJMSGateway gateway = WeblogicJMSGateway.getInstance();
        
        Map<String, String> params = new HashMap<String, String>();
        params.put("MQ_ID", id);
        params.put("MQ_TYPE", type);
        params.put("MQ_BODY", body);
        params.put("MQ_DATA", msg);
        
        return gateway.processData(filterName, params);
    }
    
    
    private String processSyncMessage(String filterName, String msg) {

        Map<String, String> params = new HashMap<String, String>();
        params.put("_DATA", msg);
        
        String result = "Response is: ";
        
        FilterCallable callable = new FilterCallable(filterName, params);
        FutureTask<String> futureTask = new FutureTask<String>(callable);
        
        try {
            executor.execute(futureTask);
            
            while (!futureTask.isDone()) {
                try {
                    Log.log.debug("Waiting for FutureTask to complete");
                    result = futureTask.get(waitTime, TimeUnit.SECONDS);
                } catch (InterruptedException | ExecutionException e) {
                    Log.log.error(e.toString());
                    JSONObject message = new JSONObject();
                    message.accumulate("Message", "Failed to process.");
                    result = message.toString();
                    break;
                } catch(TimeoutException e) {
                    futureTask.cancel(true);
                    Log.log.error(e.toString());
                    JSONObject message = new JSONObject();
                    message.accumulate("Message", "Task is timed out.");
                    result = message.toString();
                    break;
                }
            }
        } catch(Exception e) {
            Log.log.error(e.toString());
            JSONObject message = new JSONObject();
            message.accumulate("Message", "Failed to execute task.");
            result = message.toString();
        }
        
        return result;
    }
    
    class FilterCallable implements Callable<String> {

        private String filterName;
        private Map<String, String> params;

        
        public FilterCallable( final String filterName, final Map<String, String> params) {
            this.filterName = filterName;
            this.params = params;
        }
        
        @Override
        public String call() throws Exception {
            
            return WeblogicJMSGateway.getInstance().processBlockingData(filterName, params);
        }

    }
}
