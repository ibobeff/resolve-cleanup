package com.resolve.persistence.model;

import java.util.List;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;
import javax.persistence.UniqueConstraint;

import org.apache.commons.lang3.StringUtils;

import com.resolve.services.interfaces.VO;
import com.resolve.util.CryptUtils;
import com.resolve.util.Log;
import com.resolve.services.hibernate.vo.WeblogicJMSFilterVO;

@Entity
@Table(name = "weblogicjms_filter", uniqueConstraints = { @UniqueConstraint(columnNames = { "u_name", "u_queue" }) })
public class WeblogicJMSFilter extends GatewayFilter<WeblogicJMSFilterVO> {

    private static final long serialVersionUID = 1L;

    public WeblogicJMSFilter() {
    }

    public WeblogicJMSFilter(WeblogicJMSFilterVO vo) {
        applyVOToModel(vo);
    }

    private String UAddress;
    private String UUsername;
    private String UPassword;
    private String UJmsfactory;
    private String UValue;
    private String UType;
    private String UClient;
    
    @Column(name = "u_client", length = 100)
    public String getUClient()
    {
        return UClient;
    }
    
    public void setUClient(String uClient)
    {
        UClient = uClient;
    }

    @Column(name = "u_address", length = 100)
    public String getUAddress() {
        return this.UAddress;
    }

    public void setUAddress(String uAddress) {
        this.UAddress = uAddress;
    }
    
    @Column(name = "u_username", length = 100)
    public String getUUsername() {
        return this.UUsername;
    }
    
    public void setUUsername(String uUsername)
    {
        UUsername = uUsername;
    }
    
    @Column(name = "u_jmsfactory", length = 100)
    public String getUJmsfactory() {
        return this.UJmsfactory;
    }
    
    public void setUJmsfactory(String uJmsfactory)
    {
        UJmsfactory = uJmsfactory;
    }
    
    @Column(name = "u_password", length = 100)
    public String getUPassword() {
        return this.UPassword;
    }
    
    public void setUPassword(String uPassword)
    {
        UPassword = uPassword;
    }

    @Column(name = "u_value", length = 100)
    public String getUValue() {
        return this.UValue;
    }

    public void setUValue(String uValue) {
        this.UValue = uValue;
    }

    @Column(name = "u_type", length = 400)
    public String getUType() {
        return this.UType;
    }

    public void setUType(String uType) {
        this.UType = uType;
    }

    public WeblogicJMSFilterVO doGetVO() {
        WeblogicJMSFilterVO vo = new WeblogicJMSFilterVO();
        super.doGetBaseVO(vo);
        vo.setUAddress(getUAddress());
        vo.setUUsername(getUUsername());
        vo.setUPassword(getUPassword());
        vo.setUJmsfactory(getUJmsfactory());
        vo.setUName(getUName());
        vo.setUType(getUType());
        vo.setUValue(getUValue());
        vo.setUClient(getUClient());
        return vo;
    }

    @Override
    public void applyVOToModel(WeblogicJMSFilterVO vo) {
        if (vo == null) return;
        super.applyVOToModel(vo);
        
        String pass = vo.getUPassword();
        if (!VO.STRING_DEFAULT.equals(pass) && StringUtils.isNotBlank(pass)) {
            try {
                pass = CryptUtils.encrypt(pass);
            } catch(Exception e) {
                Log.log.error(e.getMessage(), e);
            }
        }
        this.setUPassword(pass);
        
        if (!VO.STRING_DEFAULT.equals(vo.getUAddress())) this.setUAddress(vo.getUAddress()); else ;
        if (!VO.STRING_DEFAULT.equals(vo.getUUsername())) this.setUUsername(vo.getUUsername()); else ;
        if (!VO.STRING_DEFAULT.equals(vo.getUJmsfactory())) this.setUJmsfactory(vo.getUJmsfactory()); else ;
        if (!VO.STRING_DEFAULT.equals(vo.getUName())) this.setUName(vo.getUName()); else ;
        if (!VO.STRING_DEFAULT.equals(vo.getUValue())) this.setUValue(vo.getUValue()); else ;
        if (!VO.STRING_DEFAULT.equals(vo.getUType())) this.setUType(vo.getUType()); else ;
        if (!VO.STRING_DEFAULT.equals(vo.getUClient())) this.setUClient(vo.getUClient()); else ;
    }

    @Override
    public List<String> ugetCdataProperty() {
        List<String> list = super.ugetCdataProperty();
        list.add("UValue");
        list.add("UType");
        list.add("UAddress");
        list.add("UUsername");
        list.add("UPassword");
        list.add("UJmsfactory");
        list.add("UClient");
        return list;
    }
}

