package com.resolve.gateway.weblogicjms.test;

import org.junit.runner.RunWith;
import org.junit.runners.Suite;
import org.junit.runners.Suite.SuiteClasses;

@RunWith(Suite.class)
@SuiteClasses({ /*Add all your API Junit test class here*/})
class UnitTestSuite
{

}


