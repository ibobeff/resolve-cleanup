package com.resolve.gateway;

import java.util.Map;

import com.resolve.gateway.splunk.SplunkFilter;
import com.resolve.gateway.splunk.SplunkGateway;

public class MSplunk extends MGateway {

    private static final String RSCONTROL_SET_FILTERS = "MSplunk.setFilters";

    private static final SplunkGateway instance = SplunkGateway.getInstance();

    public MSplunk() {
        super.instance = instance;
    }

    protected String getSetFilterMethodName() {
        return RSCONTROL_SET_FILTERS;
    }

    /**
     * Populates the {@link Filter} data into the given filterMap {@link Map} instance
     * 
     * @param filterMap
     *      the {@link Map} instance to hold the {@link Filter} data
     * @param filter
     *      a {@link SplunkFilter} instance
     *            
     */
    public void populateGatewaySpecificFilterProperties(Map<String, String> filterMap, Filter filter) {
        if (instance.isActive()) {
            SplunkFilter splunkFilter = (SplunkFilter) filter;
            filterMap.put(SplunkFilter.PORT, "" + splunkFilter.getPort());
            filterMap.put(SplunkFilter.URI, splunkFilter.getUri());
            filterMap.put(SplunkFilter.SSL, "" + splunkFilter.getSsl());
        }
    }
}

