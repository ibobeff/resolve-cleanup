package com.resolve.gateway.splunk;

import java.util.HashMap;
import java.util.Map;
import java.util.List;
import java.util.concurrent.ConcurrentHashMap;
import com.resolve.gateway.BaseClusteredGateway;
import com.resolve.gateway.BaseFilter;
import com.resolve.gateway.Filter;
import com.resolve.gateway.MSplunk;
import com.resolve.util.Constants;
import com.resolve.util.Log;
import com.resolve.util.StringUtils;
import com.splunk.Service;

public class SplunkGateway extends BaseClusteredGateway {

    private static volatile SplunkGateway instance = null;

    private Map<Integer, HttpServer> splunkServers = new HashMap<Integer, HttpServer>();
    private Map<String, HttpServer> deployedServlets = new HashMap<String, HttpServer>();
    
    private String queue = null;

    private HttpServer defaultSplunkServer;
    
    // Splunk SDK Service for use by Splunk pull APIs
    
    private Service splunkService = null;
    
    public static SplunkGateway getInstance(ConfigReceiveSplunk config) {
        if (instance == null) {
            instance = new SplunkGateway(config);
        }
        return instance;
    }

    /**
     * This method is called once the initialization is done. If it gets called
     * before the initialization it will throw exception.
     *
     * @return
     */
    public static SplunkGateway getInstance() {
        if (instance == null) {
            throw new RuntimeException("Splunk Gateway is not initialized correctly..");
        } else {
            return instance;
        }
    }

    private SplunkGateway(ConfigReceiveSplunk config) {
        super(config);
    }

    @Override
    public String getLicenseCode() {
        return "SPLUNK";
    }

    @Override
    protected void addAdditionalValues(Map<String, Object> arg0, Map<String, String> arg1) {
    }

    @Override
    protected String getGatewayEventType() {
        return "SPLUNK";
    }

    @Override
    protected String getMessageHandlerName() {
        return MSplunk.class.getSimpleName();
    }

    @Override
    protected Class<MSplunk> getMessageHandlerClass() {
        return MSplunk.class;
    }

    public String getQueueName() {
        return queue + getOrgSuffix();
    }

    @Override
    public void start() {
    	Log.log.debug("SplunkGateway:******************* Starting SplunkPush Gateway **************************");
        super.start();
    }

    @Override
    public void run() {
    	Log.log.debug("SplunkPushGateway: Sending Sync request");
        super.sendSyncRequest();
        
        if (isPrimary() && isActive()) {
            initSplunkServers();
        }
        Log.log.debug("SplunkGateway:************** SplunkPush Gateway Initialization complete ********************");
    }

    @Override
    protected void initialize() {
        ConfigReceiveSplunk config = (ConfigReceiveSplunk) configurations;
        queue = config.getQueue().toUpperCase();
        try {
            Log.log.info("SplunkPushGateway: Initializing Splunk Listener");            this.gatewayConfigDir = "/config/splunk/";
            defaultSplunkServer = new HttpServer(config);
        } catch (Exception e) {
            Log.log.error("SplunkPushGateway: Failed to config Splunk Gateway: " + e.getMessage(), e);        }
    }

    /**
     * This method processes the message received from the Splunk system.
     *
     * @param message
     */
    public boolean processData(String filterName, List<Map<String, String>> params) {
        boolean result = true;
        try {
            if (StringUtils.isNotBlank(filterName) && params != null && !params.isEmpty()) {
                SplunkFilter splunkFilter = (SplunkFilter) filters.get(filterName);
                if (splunkFilter != null && splunkFilter.isActive()) {
                    if (Log.log.isDebugEnabled()) {
                        Log.log.debug("SplunkPushGateway: Processing filter: " + splunkFilter.getId());
                        
                        if (Log.log.isDebugEnabled()) {
                            Log.log.debug("SplunkGateway: Data received from Splunk gateway -->"
                            		 + (params != null ? params.toString(): "No params"));
                        }
                    }
                    
                    for (Map<String, String> param : params) {
                        if (Log.log.isDebugEnabled()) {
                            Log.log.debug("SplunkPushGateway: Data received from Splunk gateway: " + StringUtils.mapToString(param));
                        }
                    
                        Map<String, String> runbookParams = param;
                        if (!param.containsKey(Constants.EVENT_EVENTID) || StringUtils.isBlank(param.get(Constants.EVENT_EVENTID))) {
                            runbookParams.put(Constants.EVENT_EVENTID, splunkFilter.getEventEventId());
                        }
                        Log.log.debug("SplunkPushGateway: Adding runbook params to queue: " + runbookParams != null
                        		? runbookParams.toString(): "No Runbook params sent");
                        addToPrimaryDataQueue(splunkFilter, runbookParams);
                    }
                } else {
                    result = false;
                }
            }
        } catch (Exception e) {
            Log.log.error("SplunkPushGateway: Error " + e.getMessage() + " processing received data" + (StringUtils.isNotBlank(filterName) ? " for filter " + filterName : ""), e);
            
            if (Log.log.isDebugEnabled() && params != null && !params.isEmpty()) {
                int i = 0;
                for (Map<String, String> param : params) {
                    Log.log.debug("SplunkPushGateway: Received data[" + i++ + "] : " + StringUtils.mapToString(param));
                }
            }
            
            try {
                Thread.sleep(interval);
            } catch (InterruptedException ie) {
                Log.log.warn("SplunkPushGateway: sleep interrupted: " + ie.getMessage(), ie);
            }
        }
        
        return result;
    }

    @Override
    public void clearAndSetFilters(List<Map<String, Object>> filterList) {
 Log.log.debug("SplunkPushGateway: Calling Gateway specific clearAndSetFilter" + (filterList != null ? " " + filterList.size() + ", [" + filterList + "]" : ""));
        super.clearAndSetFilters(filterList);
        
        if (isPrimary() && isActive()) {
            try {
                initSplunkServers();
            } catch (Exception e) {
                Log.log.error("SplunkGateway: " + e.getMessage(), e);
            }
        }
    }

    @Override
    protected void cleanFilter(ConcurrentHashMap<String, Filter> undeployedFilters) {
        //Add customized code here to modify your server when you clear out the deployed filters;
        
        for(Filter filter : undeployedFilters.values())
        {
            SplunkFilter splunkFilter = (SplunkFilter) filter;
            
            try
            {
                if(splunkFilter.getPort() > 0)
                {
                    HttpServer splunkServer = splunkServers.get(splunkFilter.getPort());
                    splunkServer.removeServlet(splunkFilter.getId());
                    Log.log.debug("SplunkPushGateway: Adding the splunk selvlet with port: " + splunkFilter.getPort());
                    //if there is no filter/servlet remaining then stop the server on the port
                    if(splunkServer.getServlets().isEmpty())
                    {
                    	Log.log.debug("SplunkPushGateway: No empty servlet hence stopping the http server");
                        splunkServer.stop();
                        splunkServers.remove(splunkFilter.getPort());
                    }
                }
                else
                {
                	Log.log.debug("SplunkPushGateway: No port set up in the filter for the servlet removing "
                			+ "	the servlet");
                    //undeploy the servlet from the default server.
                    defaultSplunkServer.removeServlet(splunkFilter.getId());
                }
            }
            catch (Exception e)
            {
                Log.log.error("SplunkGateway: " + e.getMessage(), e);
            }
        }
    }

    private void initSplunkServers() {
        //Add customized code here to initilize your server based on configuration and deployed filter data;
        
        try {
            if (!defaultSplunkServer.isStarted()) {
                defaultSplunkServer.init();
                defaultSplunkServer.start();
                
                if (defaultSplunkServer.isStarted())
                {
                    defaultSplunkServer.addDefaultResolveServlet();
                }
            }
            
            for (Filter filter : orderedFilters) {
                SplunkFilter splunkFilter = (SplunkFilter) filter;
                //add the URI based servlets to the default server
                if (splunkFilter.getPort() == null || splunkFilter.getPort() <= 0 || defaultSplunkServer.getPort() == splunkFilter.getPort()) {
                	Log.log.debug("SplunkPushGateway: No servlet defined in the filter creating the default servlet");
                    if (defaultSplunkServer.isStarted()) {
                        defaultSplunkServer.addServlet(splunkFilter);
                        deployedServlets.put(filter.getId(), defaultSplunkServer);
                    }
                } else {
                    HttpServer splunkServer = splunkServers.get(splunkFilter.getPort());
                    
                    if (splunkServer == null)
                    {
                        HttpServer splunkServer1 = deployedServlets.get(filter.getId());
                        
                        if (splunkServer1 != null)
                        {
                            //port changed
                        	Log.log.debug("SplunkPushGateway: Port changed so removing the serve for redeployment");
                            splunkServer1.removeServlet(filter.getId());
                            deployedServlets.remove(filter.getId());
                        }
                        
                        splunkServer = new HttpServer((ConfigReceiveSplunk) configurations, splunkFilter.getPort(), splunkFilter.getSsl());
                        splunkServer.init();
                        splunkServer.addServlet(splunkFilter);
                        splunkServer.start();
                        Log.log.debug("SplunkPushGateway: Server initialized and started");
                    }
                    else
                    {
                        splunkServer.addServlet(splunkFilter);
                    }
                    
                    splunkServers.put(splunkFilter.getPort(), splunkServer);
                    deployedServlets.put(filter.getId(), splunkServer);
                }
            }
        }
        catch (Exception e)
        {
        	Log.log.debug("SplunkPushGateway: Error when initializing Splunk push server");
            Log.log.error("SplunkGateway: " + e.getMessage(), e);
        }
    }

    @Override
    public void stop() {
    	Log.log.info("*****************************************************");
        Log.log.info("SplunkPushGateway: Stopping Splunk gateway");
        Log.log.info("*****************************************************");
        //Add customized code here to stop the connection with 3rd party system;
        
        for (HttpServer splunkServer : splunkServers.values()) {
            try {
                splunkServer.stop();
            } catch (Exception e) {
            	Log.log.error("SplunkPushGateway: Error stopping Splunk Push gateway");
                Log.log.error(e.getMessage(), e);
            }
        }
        
        try {
            defaultSplunkServer.stop();
        } catch (Exception e)  {
        	Log.log.error("SplunkGateway:Error Stopping Splunk gateway");
            Log.log.error("SplunkGateway: " + e.getMessage(), e);
        }
        
        splunkServers.clear();
        
        super.stop();
    }

    @Override
    public Filter getFilter(Map<String, Object> params) {
        return new SplunkFilter((String) params.get(BaseFilter.ID), (String) params.get(BaseFilter.ACTIVE), (String) params.get(BaseFilter.ORDER), (String) params.get(BaseFilter.INTERVAL), (String) params.get(BaseFilter.EVENT_EVENTID), (String) params.get(BaseFilter.RUNBOOK), (String) params.get(BaseFilter.SCRIPT), (String) params.get(SplunkFilter.PORT), (String) params.get(SplunkFilter.URI), (String) params.get(SplunkFilter.SSL));
    }
    
    private static Service authenticate(ConfigReceiveSplunk config) throws Exception {

        return SplunkAPI.authenticate(config.getSplunkHost(), config.getSplunkPort(), config.getSplunkUserName(), config.getSplunkP_assword(), config.getSplunkProtocolName());
    }
    
    public Service getSplunkService()
    {
        if(splunkService == null) {
            try {
            	Log.log.debug("SplunkPushGateway: Authenticating Splunk configuration");
                splunkService = authenticate((ConfigReceiveSplunk) configurations);
            } catch(Exception e) {
            	Log.log.debug("SplunkPushGateway: Authentication failed couldn't get splunk service");
                Log.log.error(e.getMessage());
            }
        }
        
        return splunkService;
    }
}

