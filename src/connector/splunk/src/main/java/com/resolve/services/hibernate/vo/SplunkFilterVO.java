package com.resolve.services.hibernate.vo;

import com.resolve.services.interfaces.VO;
import com.resolve.util.MappingAnnotation;
import com.resolve.util.StringUtils;

public class SplunkFilterVO extends GatewayFilterVO {

    private static final long serialVersionUID = 1L;

    public SplunkFilterVO() {
    }

    private Integer UPort;

    private String UUri;

    private Boolean USsl;

    @MappingAnnotation(columnName = "PORT")
    public Integer getUPort() {
        return this.UPort;
    }

    public void setUPort(Integer uPort) {
        this.UPort = uPort;
    }

    @MappingAnnotation(columnName = "URI")
    public String getUUri() {
        return this.UUri;
    }

    public void setUUri(String uUri) {
        if (StringUtils.isNotBlank(uUri) || UUri.equals(VO.STRING_DEFAULT))
        {
            this.UUri = uUri != null ? uUri.trim() : uUri;
        }
    }

    @MappingAnnotation(columnName = "SSL")
    public Boolean getUSsl() {
        return this.USsl;
    }

    public void setUSsl(Boolean uSsl) {
        this.USsl = uSsl;
    }
    
    @Override
    public boolean equals(Object obj)
    {
        if (this == obj) return true;
        if (!super.equals(obj)) return false;
        if (getClass() != obj.getClass()) return false;
        SplunkFilterVO other = (SplunkFilterVO) obj;
        if (UUri == null)
        {
            if (StringUtils.isNotBlank(other.UUri)) return false;
        }
        else if (!UUri.trim().equals(other.UUri == null ? "" : other.UUri.trim())) return false;
        if (UPort == null)
        {
            if (other.UPort != null) return false;
        }
        else if (!UPort.equals(other.UPort)) return false;
        if (USsl == null)
        {
            if (other.USsl != null) return false;
        }
        else if (!USsl.equals(other.USsl)) return false;
        return true;
    }
    
    @Override
    public int hashCode()
    {
        final int prime = 31;
        int result = super.hashCode();
        result = prime * result + ((UPort == null) ? 0 : UPort.hashCode());
        result = prime * result + ((UUri == null) ? 0 : UUri.hashCode());
        result = prime * result + ((USsl == null) ? 0 : USsl.hashCode());
        return result;
    }
}

