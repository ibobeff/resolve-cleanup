package com.resolve.gateway.splunk;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.eclipse.jetty.server.HttpConfiguration;
import org.eclipse.jetty.server.HttpConnectionFactory;
import org.eclipse.jetty.server.Server;
import org.eclipse.jetty.server.ServerConnector;
import org.eclipse.jetty.servlet.ServletContextHandler;
import org.eclipse.jetty.servlet.ServletHandler;
import org.eclipse.jetty.servlet.ServletHolder;
import org.eclipse.jetty.servlet.ServletMapping;
import org.eclipse.jetty.util.ssl.SslContextFactory;

import com.resolve.util.Log;
import com.resolve.util.ResolveDefaultHttpServlet;
import com.resolve.util.StringUtils;

public class HttpServer
{
    private final Server server;
    private final Integer port;
    private final Boolean isSsl;
    private final Map<String, SplunkFilter> servlets;

    public HttpServer(ConfigReceiveSplunk config)
    {
        this(config, config.getPort(), config.getSsl());
    }

    /**
     * This constructor is needed so that the port based filters could provide
     * their individual port and ssl setting.
     * 
     * @param configurations
     * @param port
     * @param isSsl
     */
    public HttpServer(ConfigReceiveSplunk configurations, Integer port, Boolean isSsl)
    {
        server = new Server();
        servlets = new HashMap<String, SplunkFilter>();
        HttpConfiguration http_config = new HttpConfiguration();
        http_config.setSecureScheme("https");
        http_config.setSendXPoweredBy(false);
        http_config.setSendServerVersion(false);
        
        int httpPort = configurations.getPort();
        
        if (port != null && port > 0)
        {
            httpPort = port;
        }
        
        this.port = httpPort;
        this.isSsl = isSsl;
        
        if (httpPort > 0)
        {
            if (isSsl)
            {
                SslContextFactory sslContextFactory = new SslContextFactory(configurations.getSslcertificate());
                sslContextFactory.setKeyStorePassword(configurations.getSslp_assword());
                sslContextFactory.setKeyManagerPassword(configurations.getSslp_assword());
                ServerConnector serverConnector = new ServerConnector(server, sslContextFactory,
                                                                      new HttpConnectionFactory(http_config));
                serverConnector.setPort(httpPort);
                server.addConnector(serverConnector);
            }
            else
            {
                ServerConnector connector = new ServerConnector(server, new HttpConnectionFactory(http_config));
                connector.setPort(httpPort);
                connector.setIdleTimeout(30000);
                server.addConnector(connector);
            }
            ServletContextHandler context = new ServletContextHandler(server, "/");
            server.setHandler(context);
        }
        else
        {
            Log.log.error("SplunkPushGateway: Invalid port provided to start server.");        }
    }

    public void init() throws Exception
    {
    
    }

    public void start() throws Exception
    {
        Log.log.info("SplunkPushGateway: Starting HTTP Server on port: " + port + " with ssl: " + isSsl);        try
        {
            server.start();
            Log.log.info("SplunkPushGateway: HTTP Server started on port: " + port + " with ssl: " + isSsl);        }
        catch (Throwable e)
        {
            Log.log.info("SplunkPushGateway: HTTP Server already started on port: " + port + " with ssl: " + isSsl);        }
        // server.join();
    }

    public void stop() throws Exception
    {
        Log.log.info("SplunkPushGateway: Stopping HTTP Server on port: " + port + " with ssl: " + isSsl);        server.stop();
    }

    public boolean isStarted()
    {
        return server.isStarted();
    }

    public boolean isStopped()
    {
        return server.isStopped();
    }

    public Map<String, SplunkFilter> getServlets()
    {
        return servlets;
    }

    public void addServlet(final SplunkFilter filter) throws Exception
    {
        String filterName = filter.getId();
        String uri = StringUtils.isBlank(filter.getUri()) ? filter.getId() : filter.getUri();
        boolean deploy = true;
        if(servlets.containsKey(filterName))
        {
            Log.log.debug("SplunkPushGateway: Servlet already exists: " + filterName);            SplunkFilter existingFilter = servlets.get(filterName);
            //if uri changed we need to adjust
            if(StringUtils.equals(existingFilter.getUri(), filter.getUri()))
            {
                deploy = false;
            }
            else
            {
                removeServlet(filterName);
            }
        }

        if(deploy)
        {
            String finalUri = uri;
            if (!uri.startsWith("/"))
            {
                finalUri = "/" + uri;
            }
            Log.log.debug("SplunkPushGateway: Starting filter at URI: " + finalUri);            ServletHandler context;
            if(server.getHandler() instanceof ServletContextHandler)
            {
                context = ((ServletContextHandler) server.getHandler()).getServletHandler();
            }
            else
            {
                context = (ServletHandler) server.getHandler();
            }
            ServletHolder servletHolder = new ServletHolder(filterName, new FilterServlet(filterName));
            context.addServletWithMapping(servletHolder, finalUri);
            servlets.put(filterName, filter);
        }
    }

    public void removeServlet(String filterName)
    {
        Log.log.debug("SplunkPushGateway: Stopping filter end point for : " + filterName);        try
        {
            ServletHandler handler = null;
            if(this.server.getHandler() instanceof ServletContextHandler)
            {
                handler = ((ServletContextHandler) server.getHandler()).getServletHandler();
            }
            else
            {
                handler = (ServletHandler) server.getHandler();
            }
            
            ServletHolder[] holders = handler.getServlets();
    
            List<ServletHolder> remainingServlets = new ArrayList<ServletHolder>();
            Set<String> names = new HashSet<String>();
            for (ServletHolder holder : holders)
            {
                if (!filterName.equals(holder.getName()))
                {
                    remainingServlets.add(holder);
                    names.add(holder.getName());
                }
            }
            
            List<ServletMapping> mappings = new ArrayList<ServletMapping>();
            for(ServletMapping mapping : handler.getServletMappings())
            {
               if(names.contains(mapping.getServletName()))
               {
                   mappings.add(mapping);
               }
            }
            
            /* Set the new configuration for the mappings and the servlets */
            handler.setServletMappings(mappings.toArray(new ServletMapping[0]));
            handler.setServlets(remainingServlets.toArray(new ServletHolder[0]));
            this.servlets.remove(filterName);
            this.server.setHandler(handler);
            Log.log.debug("SplunkPushGateway: Successfully stopped filter end point for : " + filterName);
        }
        catch(Exception e)
        {
            if(e.getMessage() != null && e.getMessage().contains("STARTED")) //when started is new stopped? :)
            {
                Log.log.debug("SplunkPushGateway: Successfully stopped filter end point for : " + filterName);            }
            else
            {
                Log.log.error("SplunkPushGateway: Could not stopped servlet for filter: " + filterName + ". " + e.getMessage());            }
        }
    }
    
    public void addDefaultResolveServlet() throws Exception
    {
        Log.log.debug("SplunkPushGateway: Adding Default Resolve Servlet at URI: " + "/");        ServletHandler context;
        if(server.getHandler() instanceof ServletContextHandler)
        {
            context = ((ServletContextHandler) server.getHandler()).getServletHandler();
        }
        else
        {
            context = (ServletHandler) server.getHandler();
        }
        ServletHolder servletHolder = new ServletHolder("/", new ResolveDefaultHttpServlet());
        context.addServletWithMapping(servletHolder, "/");
    }
    
    public int getPort()
    {
        return port;
    }
}
