package com.resolve.rscontrol;

import com.resolve.persistence.model.SplunkFilter;

public class MSplunk extends MGateway {

    private static final String MODEL_NAME = SplunkFilter.class.getSimpleName();

    protected String getModelName() {
        return MODEL_NAME;
    }
}

