package com.resolve.gateway.splunk;

import java.io.File;
import java.util.HashMap;
import java.util.Map;
import org.apache.commons.io.FileUtils;

import com.resolve.rsbase.MainBase;
import com.resolve.gateway.Filter;
import com.resolve.rsremote.ConfigReceiveGateway;
import com.resolve.util.Log;
import com.resolve.util.ObjectProperties;
import com.resolve.util.StringUtils;
import com.resolve.util.XDoc;
import com.splunk.SSLSecurityProtocol;

public class ConfigReceiveSplunk extends ConfigReceiveGateway {

    private static final long serialVersionUID = 1L;

    private static final String RECEIVE_SPLUNK_NODE = "./RECEIVE/SPLUNK/";

    private static final String RECEIVE_SPLUNK_FILTER = RECEIVE_SPLUNK_NODE + "FILTER";

    private String queue = "SPLUNK";

    private static final String RECEIVE_SPLUNK_ATTR_USERNAME = RECEIVE_SPLUNK_NODE + "@USERNAME";

    private static final String RECEIVE_SPLUNK_ATTR_PORT = RECEIVE_SPLUNK_NODE + "@PORT";

    private static final String RECEIVE_SPLUNK_ATTR_SSLCERTIFICATE = RECEIVE_SPLUNK_NODE + "@SSLCERTIFICATE";

    private static final String RECEIVE_SPLUNK_ATTR_SSLP_ASSWORD = RECEIVE_SPLUNK_NODE + "@SSLPASSWORD";

    private static final String RECEIVE_SPLUNK_ATTR_P_ASSWORD = RECEIVE_SPLUNK_NODE + "@PASSWORD";

    private static final String RECEIVE_SPLUNK_ATTR_SSL = RECEIVE_SPLUNK_NODE + "@SSL";

    // Splunk Pull API Configuration Constants
    
    private static final String RECEIVE_SPLUNK_ATTR_SPLUNK_HOST = RECEIVE_SPLUNK_NODE + "@SPLUNKHOST";
    private static final String RECEIVE_SPLUNK_ATTR_SPLUNK_PORT = RECEIVE_SPLUNK_NODE + "@SPLUNKPORT";
    private static final String RECEIVE_SPLUNK_ATTR_SPLUNK_USERNAME = RECEIVE_SPLUNK_NODE + "@SPLUNKUSERNAME";
    private static final String RECEIVE_SPLUNK_ATTR_SPLUNK_P_ASSWORD = RECEIVE_SPLUNK_NODE + "@SPLUNKPASSWORD";
    private static final String RECEIVE_SPLUNK_ATTR_SPLUNK_PROTOCOLNAME = RECEIVE_SPLUNK_NODE + "@SPLUNKPROTOCOLNAME";
    
    private String username = "";

    private int port = 0;

    private String sslcertificate = "";

    private String sslp_assword = "";

    private String p_assword = "";

    private boolean ssl = false;

    // Splunk Pull API Configuration
    
    private String splunkHost;
    private int splunkPort;
    private String splunkUserName;
    private String splunkPassword;
    private String splunkProtocolName = SSLSecurityProtocol.TLSv1.name();
    
    public String getQueue() {
        return queue;
    }

    public void setQueue(String queue) {
        this.queue = queue;
    }

    public String getUsername() {
        return this.username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public int getPort() {
        return this.port;
    }

    public void setPort(int port) {
        this.port = port;
    }

    public String getSslcertificate() {
        return this.sslcertificate;
    }

    public void setSslcertificate(String sslcertificate) {
        this.sslcertificate = sslcertificate;
    }

    public String getSslp_assword() {
        return this.sslp_assword;
    }

    public void setSslp_assword(String sslpassword) {
        this.sslp_assword = sslpassword;
    }

    public String getP_assword() {
        return this.p_assword;
    }

    public void setP_assword(String password) {
        this.p_assword = password;
    }

    public boolean getSsl() {
        return this.ssl;
    }

    public void setSsl(boolean ssl) {
        this.ssl = ssl;
    }

    // Splunk Pull API Configuration get/set methods
    
    public String getSplunkHost() {
        return this.splunkHost;
    }
    
    public void setSplunkHost(String splunkHost) {
        this.splunkHost = splunkHost;
    }
    
    public int getSplunkPort() {
        return this.splunkPort;
    }
    
    public void setSplunkPort(int splunkPort) {
        this.splunkPort = splunkPort;
    }
    
    public String getSplunkUserName() {
        return this.splunkUserName;
    }
    
    public void setSplunkUserName(String splunkUserName) {
        this.splunkUserName = splunkUserName;
    }
    
    public String getSplunkP_assword() {
        return this.splunkPassword;
    }
    
    public void setSplunkP_assword(String splunkPassword) {
        this.splunkPassword = splunkPassword;
    }
    
    public String getSplunkProtocolName() {
        return this.splunkProtocolName;
    }
    
    public void setSplunkProtocolName(String splunkProtocolName) {
        this.splunkProtocolName = splunkProtocolName;
    }
    
    public ConfigReceiveSplunk(XDoc config) throws Exception {
        super(config);
        define("username", STRING, RECEIVE_SPLUNK_ATTR_USERNAME);
        define("port", INTEGER, RECEIVE_SPLUNK_ATTR_PORT);
        define("sslcertificate", STRING, RECEIVE_SPLUNK_ATTR_SSLCERTIFICATE);
        define("sslp_assword", SECURE, RECEIVE_SPLUNK_ATTR_SSLP_ASSWORD);
        define("p_assword", SECURE, RECEIVE_SPLUNK_ATTR_P_ASSWORD);
        define("ssl", BOOLEAN, RECEIVE_SPLUNK_ATTR_SSL);
        
        define("splunkHost", STRING, RECEIVE_SPLUNK_ATTR_SPLUNK_HOST);
        define("splunkPort", INTEGER, RECEIVE_SPLUNK_ATTR_SPLUNK_PORT);
        define("splunkUserName", STRING, RECEIVE_SPLUNK_ATTR_SPLUNK_USERNAME);
        define("splunkP_assword", SECURE, RECEIVE_SPLUNK_ATTR_SPLUNK_P_ASSWORD);
        define("splunkProtocolName", STRING, RECEIVE_SPLUNK_ATTR_SPLUNK_PROTOCOLNAME);
    }

    @Override
    public String getRootNode() {
        return RECEIVE_SPLUNK_NODE;
    }

    @SuppressWarnings("unchecked")
    @Override
    public void load() throws Exception {
        try {
            loadAttributes();
            if (isActive()) {
                SplunkGateway splunkGateway = SplunkGateway.getInstance(this);
                filters = xdoc.getListMapValue(RECEIVE_SPLUNK_FILTER);
                if (filters.size() > 0) {
                    for (Map<String, Object> values : filters) {
                        String filterName = ((String) values.get(SplunkFilter.ID)).toLowerCase();
                        File scriptFile = getFile(MainBase.main.release.serviceName + "/config/splunk/" + filterName + ".groovy");
                        try {
                            if (scriptFile.exists()) {
                                String groovy = FileUtils.readFileToString(scriptFile, "UTF-8");
                                if (StringUtils.isNotBlank(groovy)) {
                                    values.put(SplunkFilter.SCRIPT, groovy);
                                    Log.log.debug("SplunkPushGateway: Loaded groovy script: " + scriptFile);                                }
                            }
                            splunkGateway.setFilter(values);
                        } catch (Exception e) {
                            Log.log.error("SplunkPushGateway:" + e.getMessage() + ". Skipping file: " + scriptFile.getAbsolutePath());                        }
                    }
                }
            }
        } catch (Throwable e) {
            setActive(false);
            Log.log.error("SplunkPushGateway: Couldn not load configuration for Splunk gateway, check blueprint. " + e.getMessage(), e);

        }
    }

    @SuppressWarnings({ "unchecked", "rawtypes" })
    @Override
    public void save() throws Exception {
        File dir = getFile(MainBase.main.release.serviceName + "/config/splunk");
        if (!dir.exists()) {
            FileUtils.forceMkdir(dir);
        }
        if (filters != null && isActive()) {
            filters.clear();
            for (Filter filter : SplunkGateway.getInstance().getFilters().values()) {
                SplunkFilter splunkFilter = (SplunkFilter) filter;
                String groovy = splunkFilter.getScript();
                String scriptFileName;
                File scriptFile = null;
                if (StringUtils.isNotBlank(groovy)) {
                    scriptFileName = splunkFilter.getId().toLowerCase() + ".groovy";
                    scriptFile = getFile(MainBase.main.release.serviceName + "/config/splunk/" + scriptFileName);
                }
                Map<String, Object> entry = new HashMap<String, Object>();
                putFilterDataIntoEntry(entry, splunkFilter);
                filters.add(entry);
                if (scriptFile != null) {
                    try {
                        FileUtils.writeStringToFile(scriptFile, groovy, "UTF-8");
                    } catch (Exception e) {
                        Log.log.error("SplunkPushGateway:" + e.getMessage() + ". Skipping file: " + scriptFile.getAbsolutePath());

                    }
                }
            }
            xdoc.setListMapValue(RECEIVE_SPLUNK_FILTER, filters);
        }
        if (nameProperties != null && isActive()) {
            nameProperties.clear();
            for (String name : SplunkGateway.getInstance().getNameProperties().keySet()) {
                Map<String, Object> entry = new HashMap<String, Object>();
                entry.put(NAME_KEY, name);
                nameProperties.add(entry);
                Map props = SplunkGateway.getInstance().getNameProperties().getNameProperties(name);
                ObjectProperties prop = new ObjectProperties(props);
                prop.save(props);
            }
        }
        saveAttributes();
    }

    private void putFilterDataIntoEntry(Map<String, Object> entry, SplunkFilter splunkFilter) {
        entry.put(SplunkFilter.ID, splunkFilter.getId());
        entry.put(SplunkFilter.ACTIVE, String.valueOf(splunkFilter.isActive()));
        entry.put(SplunkFilter.ORDER, String.valueOf(splunkFilter.getOrder()));
        entry.put(SplunkFilter.INTERVAL, String.valueOf(splunkFilter.getInterval()));
        entry.put(SplunkFilter.EVENT_EVENTID, splunkFilter.getEventEventId());
        entry.put(SplunkFilter.RUNBOOK, splunkFilter.getRunbook());
        entry.put(SplunkFilter.SCRIPT, splunkFilter.getScript());
        entry.put(SplunkFilter.PORT, splunkFilter.getPort());
        entry.put(SplunkFilter.URI, splunkFilter.getUri());
        entry.put(SplunkFilter.SSL, splunkFilter.getSsl());
    }
}

