package com.resolve.gateway.splunk;

import java.io.BufferedReader;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.commons.lang3.StringUtils;

import com.resolve.gateway.AbstractGatewayAPI;
import com.resolve.gateway.splunk.SplunkGateway;
import com.resolve.util.Log;
import com.splunk.Job;
import com.splunk.JobArgs;
import com.splunk.ResponseMessage;
import com.splunk.ResultsReaderXml;
import com.splunk.SSLSecurityProtocol;
import com.splunk.SavedSearch;
import com.splunk.SavedSearchDispatchArgs;
import com.splunk.Service;
import com.splunk.ServiceArgs;

public class SplunkAPI extends AbstractGatewayAPI
{
    static final String UPDATE_NOTABLE_EVENT = "/services/notable_update";
    static final String URGENCY = "urgency";
    static final String STATUS = "status";
    static final String OWNER = "newOwner";
    static final String COMMENT = "comment";
    static final String EVENT_ID = "ruleUIDs[]";
    static final String SEARCH_ID = "searchID";
    static HashMap<String, Integer> statusToInt = new HashMap<String, Integer>();
    
    static
    {
        statusToInt.put("new", 1);
        statusToInt.put("in progress", 2);
        statusToInt.put("pending", 3);
        statusToInt.put("resolved", 4);
        statusToInt.put("closed", 5);
    }

    /**
     * To establish connection with Splunk server with host ip, port and HTTP or HTTPS information and user credential and receive a Splunk service reference
     * @param host: the host ip 
     * @param port: the port number
     * @param username
     * @param password
     * @param protocol: the protocol for HTTPS or "" for HTTP
     * @return Service: the Service reference of Splunk connection
     * @throws Exception
     */
    public static Service authenticate(String host, int port, String username, String password, String protocol) throws Exception {
        
        ServiceArgs loginArgs = new ServiceArgs();
        
        loginArgs.setHost(host);
        loginArgs.setPort(port);
        loginArgs.setUsername(username);
        loginArgs.setPassword(password);
        
        if(StringUtils.isNotBlank(protocol))
            Service.setSslSecurityProtocol(SSLSecurityProtocol.valueOf(protocol));
        else
            Service.setSslSecurityProtocol(SSLSecurityProtocol.TLSv1);
        
        return Service.connect(loginArgs);
    }
    
    /**
     * To retrieve results from Splunk based on the saved search being defined using system configuration
     * @param query: the name of the saved search
     * @param timeout: how many seconds to wait before the connection is closed
     * @param maxNumberOfResults: the maximum number fo results to be retrieved from the saved Search
     * @throws Exception
     */
    public static List<Map<String, String>> savedSearch(String query, int timeout, int maxNumberOfResults) throws Exception {
        
        SplunkGateway gateway = SplunkGateway.getInstance();
        Service splunkService = gateway.getSplunkService();
        
        return savedSearch(splunkService, query, timeout, maxNumberOfResults, true);
    }
    
    /**
     * To retrieve results from Splunk based on the saved search being defined, given user credential and connection information
     * @param host: the host ip 
     * @param port: the port number
     * @param username
     * @param password
     * @param protocol: the protocol for HTTPS or "" for HTTP
     * @param query: the name of the saved search
     * @param timeout: how many seconds to wait before the connection is closed
     * @param maxNumberOfResults: the maximum number fo results to be retrieved from the saved Search
     * @return List<Map<String, String>>
     * @throws Exception
     */
    public static List<Map<String, String>> savedSearch(String host, int port, String username, String password, String protocol, String query, int timeout, int maxNumberOfResults) throws Exception {
    	Service splunkService = null;
    	try {
    			splunkService = authenticate(host, port, username, password, protocol);
    	}catch(Exception ex) {
    		Log.log.error("SplunkGateway: Authentication failure to Splunk when making "
    				+ "Saved search call --> " + ex.getMessage());
    		return null;
    	}
    	if(splunkService != null) {
    		return savedSearch(splunkService, query, timeout, maxNumberOfResults, false);
    	}else {
    		return null;
    	}
    }

    /**
     * To update the urgency, status, owner fields in notable event, and also add comment for the update
     * @param eventId: the event id 
     * @param urgency: urgency field of notable event, put in the value if you want to update it, otherwise leave it empty
     * @param status: status field of notable event, put in the value if you want to update it, otherwise leave it empty
     * @param owner: owner field of notable event, put in the value if you want to update it, otherwise leave it empty
     * @param comment: add some comment for update 
     * @return ResponseMessage
     * @throws Exception
     */
    public static String updateNotableEvent(String eventId, String urgency, String status, String owner, String comment) throws Exception {
        SplunkGateway gateway = SplunkGateway.getInstance();
        Service splunkService = gateway.getSplunkService();
        return updateNotableEvent(splunkService, eventId, urgency, status, owner, comment);
    }
    
    /**
     * To update the urgency, status, owner fields in notable event, and also add comment for the update
     * @param host: the host ip 
     * @param port: the port number
     * @param username
     * @param password
     * @param protocol: the protocol for HTTPS or "" for HTTP
     * @param eventId: the event id 
     * @param urgency: urgency field of notable event, put in the value if you want to update it, otherwise leave it empty
     * @param status: status field of notable event, put in the value if you want to update it, otherwise leave it empty
     * @param owner: owner field of notable event, put in the value if you want to update it, otherwise leave it empty
     * @param comment: add some comment for update 
     * @return ResponseMessage
     * @throws Exception
     */
    public static String updateNotableEvent(String host, int port, String username, String password, String protocol,String eventId, String urgency, String status, String owner, String comment) throws Exception {
    	Service splunkService = null;
    	try {
    		splunkService = authenticate(host, port, username, password, protocol);
    	}catch(Exception ex) {
    		Log.log.error("SplunkGateway: Authentication failure to Splunk when making "
    				+ "Update Notable Event call --> " + ex.getMessage());
    		return null;
    	}
    	if(splunkService != null) {	
    		return updateNotableEvent(splunkService, eventId, urgency, status, owner, comment);
    	}else {
    		return null;
    	}
    }
    
    /**
     * To update the urgency, status, owner fields in notable event, and also add comment for the update
     * @param splunkService: the Service reference of Splunk connection after authentication that can be cached in the SplunkGateway for system user or the client side with non-system user
     * @param eventId: the event id 
     * @param urgency: urgency field of notable event, put in the value if you want to update it, otherwise leave it empty
     * @param status: status field of notable event, put in the value if you want to update it, otherwise leave it empty
     * @param owner: owner field of notable event, put in the value if you want to update it, otherwise leave it empty
     * @param comment: add some comment for update 
     * @return ResponseMessage
     * @throws Exception
     */    
    public static String updateNotableEvent(Service splunkService, String eventId, String urgency, String status, String owner, String comment) throws Exception {
        Map<String, Object> params = new HashMap<String, Object>();
        if(urgency!=null && !urgency.isEmpty()) {
            params.put(URGENCY, urgency);
        }
        if(status!=null && !status.isEmpty()) {
            if(status.equals("1") || status.equals("2") || status.equals("3") || status.equals("4") || status.equals("5")) {
                params.put(STATUS, status);
            }
            else if(statusToInt.containsKey(status.toLowerCase())){
                params.put(STATUS, statusToInt.get(status.toLowerCase()));
            }
        }
        if(owner!=null && !owner.isEmpty()) {
            params.put(OWNER, owner);
        }
        if(comment!=null && !comment.isEmpty()) {
            params.put(COMMENT, comment);
        }
        //SU-50 :: Adding missing Event ID that needs to be passed for updating the record 
        params.put(EVENT_ID, eventId);
        Log.log.debug("SplunkPushGateway: Update notable event with Params "
        		       		+ " -->" + com.resolve.util.StringUtils.mapToLogString(params));
        Log.log.debug("SplunkPushGateway: Updated notable eventId --> " + eventId);
        ResponseMessage response;
        try {
            response = splunkService.post(UPDATE_NOTABLE_EVENT, params);
        }
        catch(com.splunk.HttpException e) {
        	Log.log.debug("SplunkGateway: Update notable event call failed --> " + e.getMessage());
            splunkService.login();
            response = splunkService.post(UPDATE_NOTABLE_EVENT, params);
        }
        
        BufferedReader reader = new BufferedReader(new InputStreamReader(response.getContent()));
        StringBuilder str = new StringBuilder();
        String line;
        while((line = reader.readLine()) != null) {
            str.append(line);
        }
        Log.log.debug("SplunkGateway: Update notable event call results--> " + str.toString());
        return str.toString();
    }
    
    /**
     * To retrieve results from Splunk based on the saved search being defined, given the available service reference from Splunk after authentication
     * @param splunkService: the Service reference of Splunk connection after authentication that can be cached in the SplunkGateway for system user or the client side with non-system user
     * @param query: search criteria
     * @param timeout: how many seconds to wait before the connection is closed
     * @param maxNumberOfResults: the maximum number fo results to be retrieved from the saved Search
     * @param systemUser: true if the user is system user, false if not
     * @return List<Map<String, String>>
     * @throws Exception
     */
    public static List<Map<String, String>> savedSearch(Service splunkService, String query, int timeout, int maxNumberOfResults, boolean systemUser) throws Exception {
        
        if(splunkService == null)
            throw new Exception("Missing Service information.");
        
        List<Map<String, String>> events = new ArrayList<Map<String, String>>();
        
        Job jobSavedSearch = null;
        ResultsReaderXml resultsReader = null;
        SavedSearch savedSearch = null;
        Log.log.debug("SplunkGateway: calling saved search with query --> " + query);
        try {          
            try {
                savedSearch = splunkService.getSavedSearches().get(query);
            } catch(Exception ee) {
            	Log.log.info("SplunkGateway: getSavedSearches call failed --> " + query);
                splunkService.login();
                savedSearch = splunkService.getSavedSearches().get(query);
            }
            
            if(savedSearch == null) {
                Log.log.error("SplunkPushGateway: Saved Search results for query -->" + query + " not found.");                throw new Exception("Saved Search not found.");
            }
            Log.log.debug("SplunkPushGateway: Saved search got result --> " + savedSearch.toString());
            Log.log.info("SplunkPushGateway: Waiting for the job to finish...");

            SavedSearchDispatchArgs dispatchArgs = new SavedSearchDispatchArgs();
            dispatchArgs.setDispatchMaximumCount(maxNumberOfResults);
            
            jobSavedSearch = savedSearch.dispatch(dispatchArgs);

            long startTime = System.currentTimeMillis();
            
            while (!jobSavedSearch.isDone()) {
                long currentTime = System.currentTimeMillis();
                
                if(timeout != 0 && (startTime + timeout*1000) < currentTime)
                    throw new Exception("SplunkPushGateway: Search execution exceeded specified timeout.");
                else
                {
                    try {
                        Thread.sleep(500);
                    } catch (InterruptedException e) {
                        Log.log.error(e.getMessage());
                        e.printStackTrace();
                    }
                }
             }
             
             Log.log.debug("SplunkPushGateway: Number of result --> " + jobSavedSearch.getResultCount());
             InputStream results = jobSavedSearch.getResults();
             resultsReader = new ResultsReaderXml(results);
             Map<String, String> event = new HashMap<String, String>();
             
             while ((event = resultsReader.getNextEvent()) != null) {
                 Map<String, String> result = new HashMap<String, String>();
                 
                 for (String key: event.keySet()) {
                     result.put(key,  event.get(key));
                     Log.log.debug("   " + key + ":  " + event.get(key));
                 }
                 
                 events.add(result);
             }
             
             resultsReader.close();
             
        } catch (Exception e) {
        	Log.log.error("SplunkGateway: Number of result" );
            e.printStackTrace();
            Log.log.error(e.getMessage());
        } finally {
            try {
                if(resultsReader != null)
                    resultsReader.close();
            } catch(Exception e1) {
                e1.getMessage();
            }
        }

        return events;
    }
    
    /**
     * To retrieve results from Splunk based on the query provided using system configuration
     * @param query: search criteria
     * @param timeout: how many seconds to wait before the connection is closed
     * @return List<Map<String, String>>
     * @throws Exception
     */
    public static List<Map<String, String>> normalSearch(String query, int timeout) throws Exception {
    
        SplunkGateway gateway = SplunkGateway.getInstance();
        Service splunkService = gateway.getSplunkService();
        
        return normalSearch(splunkService, query, timeout, true);
    }
    
    /**
     * To retrieve results from Splunk based on the query provided, given user credential
     * @param host: the host ip 
     * @param port: the port number
     * @param username
     * @param password
     * @param protocol: the protocol for HTTPS or "" for HTTP
     * @param query: search criteria
     * @param timeout: how many seconds to wait before the connection is closed
     * @return List<Map<String, String>>
     * @throws Exception
     */
    public static List<Map<String, String>> normalSearch(String host, int port, String username, String password, String protocol, String query, int timeout) throws Exception {
    	Service splunkService = null;
    	try {
    		splunkService = authenticate(host, port, username, password, protocol);
    	}catch(Exception ex) {
    		Log.log.error("SplunkGateway: Authentication failure to Splunk when making "
    				+ "Normal search call --> " + ex.getMessage());
    		return null;
    	}
    	if(splunkService != null) {
    		return normalSearch(splunkService, query, timeout, false);
    	}else {
    		return null;
    	}
    }
    
    /**
     * To retrieve results from Splunk based on the query provided, given the available service reference from Splunk after authentication
     * @param splunkService: the Service reference of Splunk connection after authentication that can be cached in the SplunkGateway for system user or the client side with non-system user
     * @param query: search criteria
     * @param timeout: how many seconds to wait before the connection is closed
     * @param systemUser: true if the user is system user, false if not
     * @return List<Map<String, String>>
     * @throws Exception
     */
    public static List<Map<String, String>> normalSearch(Service splunkService, String query, int timeout, boolean systemUser) throws Exception {

        List<Map<String, String>> events = new ArrayList<Map<String, String>>();
        Map<String, String> event = new HashMap<String, String>();
        
        Job job = null;
        JobArgs jobargs = new JobArgs();
        jobargs.setExecutionMode(JobArgs.ExecutionMode.NORMAL);
//      jobargs.add("args.mysourcetype", "splunkd");
        Log.log.debug("SplunkGateway: calling normal Search with query --> " + query);
        try {
            try {
                job = splunkService.getJobs().create(query, jobargs);
                Log.log.debug("SplunkGateway: Normal Search Job creation successful ");
            } catch(Exception ee) {
            	Log.log.error("SplunkPushGateway: Normal Search Job creation failed..logging in ");                splunkService.login();
                Log.log.debug("SplunkGateway: Normal Search creating job after login ");
                job = splunkService.getJobs().create(query, jobargs);
            }
            
            if(job == null) {
                Log.log.error("SplunkPushGateway: Failed to connect to Normal Search from Splunk.");                throw new Exception("Normal Search not found.");
            }

            long startTime = System.currentTimeMillis();
            
            while (!job.isDone()) {
                long currentTime = System.currentTimeMillis();
                
                if(timeout != 0 && (startTime + timeout*1000) < currentTime)
                    throw new Exception("SplunkPushGateway: Search execution exceeded specified timeout.");
                else
                {
                    try {
                        Thread.sleep(500);
                    } catch (InterruptedException e) {
                        Log.log.error(e.getMessage());
                    }
                }
            }
            
            // Get the search results and use the built-in XML parser to display them
            InputStream resultsNormalSearch =  job.getResults();
    
            ResultsReaderXml resultsReaderNormalSearch;

            resultsReaderNormalSearch = new ResultsReaderXml(resultsNormalSearch);
            
            while ((event = resultsReaderNormalSearch.getNextEvent()) != null) {
                Map<String, String> result = new HashMap<String, String>();
                
                for (String key: event.keySet()) {
                    result.put(key,  event.get(key));
                    Log.log.debug("SplunkGateway: results  Key -> " + key + ": EventVal -> " + event.get(key));
                }
                
                events.add(result);
            }
        } catch (Exception e) {
            e.printStackTrace();
            Log.log.error(e.getMessage());
        }
        
        return events;
    }
}
