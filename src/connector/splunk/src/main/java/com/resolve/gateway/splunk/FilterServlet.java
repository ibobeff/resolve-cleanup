package com.resolve.gateway.splunk;


import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.resolve.util.Log;
import com.resolve.util.ResolveHttpServlet;
import com.resolve.util.StringUtils;
import com.resolve.util.SystemUtil;

import net.sf.json.JSONObject;

public class FilterServlet extends ResolveHttpServlet
{
    private static final long serialVersionUID = -84276702043402876L;
    //private static final String FOBIDDEN_ENTITY = "Supports \"POST\" with Content-Type set to \"application/json\" and entity set to valid JSON only.";
    @SuppressWarnings("unused")
    private static final String METHOD_NOT_ALLOWED = "Method not supported currently.";
    private static final String UNSUPPORTED_MEDIA_TYPE = " media type not supported currently. Supports application/json media type only.";
    
    private final String filterName;

    protected ExecutorService executor = Executors.newFixedThreadPool(SystemUtil.getCPUCount());

    private SplunkGateway instance = SplunkGateway.getInstance();

    public FilterServlet(String filterName)
    {
        this.filterName = filterName;
    }

    private void setResponseStatusAndMsg(HttpServletResponse response, int statusCode, String respContentType, String respContent) throws IOException
    {
        response.setStatus(statusCode);
        
        if (StringUtils.isNotBlank(respContentType) && StringUtils.isNotBlank(respContent))
        {
            response.setContentType(respContentType);
            response.getWriter().println(respContent);
            response.getWriter().flush();
        }
    }
    
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException
    {
        String entity = "HTTP GET request submitted successfully.";
        
        if(Log.log.isDebugEnabled())
        {
        	 Log.log.debug("SplunkGateway: GET Request: URI=" + request.getRequestURI() + ", Client Address =" + request.getRemoteAddr() +
        		                          ", Client Host=" + request.getRemoteHost());
        }
        
        List<Map<String, String>> results = new ArrayList<Map<String, String>>();
            
        String queryString = request.getQueryString();
        Log.log.debug("SplunkGateway: GET Request query string --> " + queryString);
        if (StringUtils.isBlank(queryString))
        {
            String message = "SplunkPushGateway: Submitted Null query string.";
            
            if(Log.log.isInfoEnabled())
            {
                Log.log.info(message);
            }
            
            setResponseStatusAndMsg(response, HttpServletResponse.SC_FORBIDDEN, "text/plain", message);
            return;
        }
        
        Map<String, String> decodedQueryNameValues = StringUtils.urlToMap(queryString);
        
        if (decodedQueryNameValues.isEmpty())
        {
            String message = "SplunkPushGateway: Submitted query string " + StringUtils.urlToMap(queryString) + " contains no name=value&name1=value1&... pairs.";

            
            if(Log.log.isInfoEnabled())
            {
                Log.log.info(message);
            }
            
            setResponseStatusAndMsg(response, HttpServletResponse.SC_FORBIDDEN, "text/plain", message);
            return;
        }
        
        results.add(decodedQueryNameValues);
        
        executor.execute(new RequestProcessor(instance, filterName, /*request, response,*/  results));
        
        setResponseStatusAndMsg(response, HttpServletResponse.SC_OK, "text/plain", entity);
    }

    @SuppressWarnings("unchecked")
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException
    {
        if (!request.getContentType().equals("application/json") && !request.getContentType().equals("application/x-www-form-urlencoded"))
        {
            if (Log.log.isDebugEnabled())
            {
            	Log.log.debug("SplunkGateway: POST Request Content-Type of " + request.getContentType() + " is not supported." );
            }
            
            setResponseStatusAndMsg(response, HttpServletResponse.SC_UNSUPPORTED_MEDIA_TYPE, "text/plain", request.getContentType() + " " + UNSUPPORTED_MEDIA_TYPE);
            return;
        }
        
        String entity = "HTTP POST request submitted successfully.";

        if(Log.log.isDebugEnabled())
        {
        	 Log.log.debug("SplunkGateway: POST Request URI=" + request.getRequestURI() + ", Client IP=" + request.getRemoteAddr() +
                          ", Client Host=" + request.getRemoteHost());
        }
        
        List<Map<String, String>> results = new ArrayList<Map<String, String>>();
        
        if (request.getContentType().equals("application/x-www-form-urlencoded"))
        {
            String queryString = request.getQueryString();
            
            if (StringUtils.isBlank(queryString))
            {
                String message = "SplunkPushGateway: Submitted Null query string.";                
                if(Log.log.isInfoEnabled())
                {
                    Log.log.info(message);
                }
                
                setResponseStatusAndMsg(response, HttpServletResponse.SC_FORBIDDEN, "text/plain", message);
                return;
            }
            
            Map<String, String> decodedQueryNameValues = StringUtils.urlToMap(queryString);
            
            if (decodedQueryNameValues.isEmpty())
            {
                String message = "SplunkPushGateway: Submitted query string " + StringUtils.urlToMap(queryString) + " contains no name=value&name1=value1&... pairs.";                
                if(Log.log.isInfoEnabled())
                {
                    Log.log.info(message);
                }
                
                setResponseStatusAndMsg(response, HttpServletResponse.SC_FORBIDDEN, "text/plain", message);
                return;
            }
            
            results.add(decodedQueryNameValues);
        }
        else
        {
            String inputString = StringUtils.toString(request.getInputStream(), "utf-8");
            
            JSONObject jsonObj = null;
            
            try
            {
                if(Log.log.isDebugEnabled())
                {
                	Log.log.debug("SplunkGateway: Submitted JSON content " + inputString);
                }
                
                jsonObj = StringUtils.stringToJSONObject(inputString);
            }
            catch(Exception e)
            {
                String message = "SplunkPushGateway: Error " + e.getMessage() + " in converting submitted content " + inputString + " to JSON.";                
                if(Log.log.isInfoEnabled())
                {
                    Log.log.info(message);
                }
                
                setResponseStatusAndMsg(response, HttpServletResponse.SC_FORBIDDEN, "text/plain", message);
            }
    
            // JSONNull = {:}
            
            if (jsonObj != null && !jsonObj.isNullObject())
            {
                /*
                 * Will support only single hierarchical level i.e
                 *  
                 * JSON object or array of JSON objects
                 * 
                 * {key1:val1,....}
                 * 
                 * or
                 * 
                 * {key:[{key1:val1,key2:val2,...},{...},{...},....]}
                 * 
                 * Note: JSONObject will be considered as flat in returned Map<String, String>. All values of type JSONArray
                 * will be flattened out.
                 */
            	Log.log.debug("SplunkPushGateway: JSON response parsed --> " + jsonObj != null ? jsonObj.toString():
            		"JSON String is blank");
                Set<String> keySet = jsonObj.keySet();
                
                if (keySet != null && keySet.size() == 1 && jsonObj.getJSONObject(keySet.iterator().next()).isArray())
                {
                    // Handle JSONArray of flat JSONObjects
                    
                    try
                    {
                        results = StringUtils.jsonArrayToList(jsonObj.getJSONArray(keySet.iterator().next()));
                    }
                    catch(Exception e)
                    {
                        String message = "SplunkPushGateway: Error " + e.getMessage() + " in converting JSON array " + jsonObj.toString() + " to list of JSON object key value map.";                        
                        if(Log.log.isInfoEnabled())
                        {
                            Log.log.info(message);
                        }
                        
                        setResponseStatusAndMsg(response, HttpServletResponse.SC_FORBIDDEN, "text/plain", message);
                        return;
                    }
                }
                else
                {
                    // Handle flat JSONObject
                    
                    try
                    {
                        results.add(StringUtils.jsonObjectToMap(jsonObj));
                    }
                    catch(Exception e)
                    {
                        String message = "SplunkPushGateway: Error " + e.getMessage() + " in converting JSON object -->"
                        		+ jsonObj != null? jsonObj.toString(): " <Blank>" + " to key value map.";
                        
                        if(Log.log.isInfoEnabled())
                        {
                            Log.log.info(message);
                        }
                        
                        setResponseStatusAndMsg(response, HttpServletResponse.SC_FORBIDDEN, "text/plain", message);
                        return;
                    }
                }
                
                /*
                 * Replace key = result value = {key1:val1, key2:val2, ...} to
                 * key = key1 value = val1
                 * key = key2 value = val2
                 *             :
                 *             : 
                 *  
                 * Prefix keys which are not part of "result" JSON with header_ 
                 */
                
                if (!results.isEmpty())
                {
                    for (Map<String, String> result : results)
                    {
                        if (!result.isEmpty() && result.containsKey("result") && 
                            StringUtils.isNotBlank(result.get("result")))
                        {
                            JSONObject resultJSONObj = null;
                            
                            try
                            {
                                resultJSONObj = StringUtils.stringToJSONObject(result.get("result"));
                            }
                            catch (Exception e)
                            {
                                // Ignore result not an JSON error
                            }
                            
                            if (resultJSONObj != null && !resultJSONObj.isNullObject())
                            {
                                result.remove("result");
                                
                                // Prefix keys which are not part of "result" JSON with header_
                                
                                Map<String, String> nonResultKeyVal = new HashMap<String, String>();
                                
                                for (String nonResultKey : result.keySet())
                                {
                                    nonResultKeyVal.put("header_" + nonResultKey, result.get(nonResultKey));
                                }
                                
                                result.clear();
                                
                                result.putAll(nonResultKeyVal);
                                
                                Map<String, String> resultKeyVal = StringUtils.jsonObjectToMap(resultJSONObj);
                                
                                for (String resultKey : resultKeyVal.keySet())
                                {
                                    result.put(resultKey, resultKeyVal.get(resultKey));
                                }
                            }
                        }
                    }
                }
            }
            else
            {
            	String message = "SplunkPushGateway: Submitted Null JSON " + jsonObj != null ?
            			jsonObj.toString(): "Empty JSON object" + ".";
                
                if(Log.log.isInfoEnabled())
                {
                    Log.log.info(message);
                }
                
                setResponseStatusAndMsg(response, HttpServletResponse.SC_FORBIDDEN, "text/plain", message);
                return;
            }
        }
        
        executor.execute(new RequestProcessor(instance, filterName, /*request, response,*/  results));
        
        setResponseStatusAndMsg(response, HttpServletResponse.SC_OK, "text/plain", entity);
    }

    static class RequestProcessor implements Runnable
    {
        final SplunkGateway instance;
        final String filterName;
        //final HttpServletRequest request;
        //final HttpServletResponse response;
        final  List<Map<String, String>> params;
        
        public RequestProcessor(final SplunkGateway instance, final String filterName, /*final HttpServletRequest request, final HttpServletResponse response,*/ final  List<Map<String, String>> params)
        {
            this.instance = instance;
            this.filterName = filterName;
            //this.request = request;
            //this.response = response;
            this.params = params;
        }

        @Override
        public void run()
        {
            try
            {
                // process the data through a filter.
                boolean isProcessed = instance.processData(filterName, params);

                if (isProcessed)
                {
                    //response.setStatus(HttpServletResponse.SC_OK);
                    //response.setContentType("text/plain");
                    //response.getWriter().println("processed");
                    if(Log.log.isDebugEnabled())
                    {
                    	Log.log.debug("SplunkGateway: Gateway successfully processed " + params.size() + 
                                " data set" + (params.size() > 1 ? "s." : "."));
                    }
                }
                else
                {
                    // The requested resource is no longer available at the server
                    // this happens when a filter is no longer active, using http status code is 410
                    //response.setStatus(HttpServletResponse.SC_GONE);
                    
                    if(Log.log.isInfoEnabled())
                    {
                        Log.log.info("SplunkPushGateway: Gateway filter " + filterName + " not available.");                    }
                }
            }
            catch (Exception e)
            {
                //response.setStatus(HttpServletResponse.SC_BAD_REQUEST);
                Log.log.error(e.getMessage());
            }
        }
    }
}
