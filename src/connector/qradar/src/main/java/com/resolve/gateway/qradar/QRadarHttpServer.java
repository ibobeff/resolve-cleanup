package com.resolve.gateway.qradar;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.eclipse.jetty.server.HttpConfiguration;
import org.eclipse.jetty.server.HttpConnectionFactory;
import org.eclipse.jetty.server.Server;
import org.eclipse.jetty.server.ServerConnector;
import org.eclipse.jetty.servlet.ServletContextHandler;
import org.eclipse.jetty.servlet.ServletHandler;
import org.eclipse.jetty.servlet.ServletHolder;
import org.eclipse.jetty.servlet.ServletMapping;
import org.eclipse.jetty.util.ssl.SslContextFactory;

import com.resolve.util.Log;
import com.resolve.util.StringUtils;

public class QRadarHttpServer
{
    private final Server server;
    private final int port;
    private final boolean isSsl;
    private final Map<String, QRadarFilter> servlets;

    public QRadarHttpServer(ConfigReceiveQRadar config)
    {
        this(config, config.getHttpPort(), config.getHttpSsl());
    }

    /**
     * This constructor is needed so that the port based filters could provide
     * their individual port and ssl setting.
     * 
     * @param config
     * @param port
     * @param isSsl
     */
    public QRadarHttpServer(ConfigReceiveQRadar config, Integer port, Boolean isSsl)
    {
        server = new Server();
        servlets = new HashMap<String, QRadarFilter>();
        HttpConfiguration http_config = new HttpConfiguration();
        http_config.setSecureScheme("https");
        http_config.setSendXPoweredBy(false);
        http_config.setSendServerVersion(false);
        
        int httpPort = config.getHttpPort();
        if (port != null && port > 0)
        {
            httpPort = port;
        }
        this.port = httpPort;
        this.isSsl = isSsl;
        if (httpPort > 0)
        {
            if (isSsl)
            {
                SslContextFactory sslContextFactory = new SslContextFactory(config.getHttp_sslcertificate());
                sslContextFactory.setKeyStorePassword(config.getHttp_sslpassword());
                sslContextFactory.setKeyManagerPassword(config.getHttp_sslpassword());
                sslContextFactory.setIncludeProtocols("TLSv1.2");
                // sslContextFactory.setProtocol("TLSv1.2");
                // sslContextFactory.setNeedClientAuth(true);
                ServerConnector serverConnector = new ServerConnector(server, sslContextFactory,
                                                                      new HttpConnectionFactory(http_config));
                serverConnector.setPort(httpPort);
//                SslSocketConnector sslConnector = new SslSocketConnector(sslContextFactory);
//                sslConnector.setPort(httpPort);
//                server.addConnector(sslConnector);
                server.addConnector(serverConnector);
            }
            else
            {
//                Connector connector = new ServerConnector(server);
                ServerConnector connector = new ServerConnector(server, new HttpConnectionFactory(http_config));
                connector.setPort(httpPort);
                connector.setIdleTimeout(30000);
//                connector.setMaxIdleTime(30000);
                server.addConnector(connector);
            }
            //ServletContextHandler context = new ServletContextHandler(ServletContextHandler.SESSIONS);
            //context.setContextPath("/");
            ServletContextHandler context = new ServletContextHandler(server, "/");
            server.setHandler(context);
            server.setHandler(context);

            //context.addServlet(org.eclipse.jetty.servlet.DefaultServlet.class, "/");
        }
        else
        {
            Log.log.warn("Invalid port provided to start server.");
        }
    }

    public void init() throws Exception
    {
        // new servlet end point will be added to this context
        // ServletContextHandler context = new ServletContextHandler(server,
        // "/");
        // server.setHandler(context);
    }

    public void start() throws Exception
    {
        Log.log.info("Starting HTTP Server on port: " + port + " with ssl: " + isSsl);
        try
        {
            server.start();
            Log.log.info("HTTP Server started on port: " + port + " with ssl: " + isSsl);
        }
        catch (Throwable e)
        {
            Log.log.info("HTTP Server already started on port: " + port + " with ssl: " + isSsl);
        }
        // server.join();
    }

    public void stop() throws Exception
    {
        Log.log.info("Stopping HTTP Server on port: " + port + " with ssl: " + isSsl);
        server.stop();
    }

    public boolean isStarted()
    {
        return server.isStarted();
    }

    public boolean isStopped()
    {
        return server.isStopped();
    }

    public Map<String, QRadarFilter> getServlets()
    {
        return servlets;
    }

    public void addServlet(final QRadarFilter filter) throws Exception
    {
        String filterName = filter.getId();
        String uri = StringUtils.isBlank(filter.getUri()) ? filter.getId() : filter.getUri();
        boolean deploy = true;
        if(servlets.containsKey(filterName))
        {
            Log.log.debug("Servlet already exists: " + filterName);
            QRadarFilter existingFilter = servlets.get(filterName);
            //if uri changed we need to adjust
            if(StringUtils.equals(existingFilter.getUri(), filter.getUri()))
            {
                deploy = false;
            }
            else
            {
                removeServlet(filterName);
            }
        }

        if(deploy)
        {
            String finalUri = uri;
            if (!uri.startsWith("/"))
            {
                finalUri = "/" + uri;
            }
            Log.log.debug("Starting filter at URI: " + finalUri);
            ServletHandler context;
            if(server.getHandler() instanceof ServletContextHandler)
            {
                context = ((ServletContextHandler) server.getHandler()).getServletHandler();
            }
            else
            {
                context = (ServletHandler) server.getHandler();
            }
            ServletHolder servletHolder = new ServletHolder(filterName, new FilterServlet(filterName));
            context.addServletWithMapping(servletHolder, finalUri);
            servlets.put(filterName, filter);
        }
    }

    public void removeServlet(String filterName)
    {
        Log.log.debug("Stopping filter end point for : " + filterName);
        try
        {
            ServletHandler handler = null;
            if(this.server.getHandler() instanceof ServletContextHandler)
            {
                handler = ((ServletContextHandler) server.getHandler()).getServletHandler();
            }
            else
            {
                handler = (ServletHandler) server.getHandler();
            }
            ServletHolder[] holders = handler.getServlets();
    
            List<ServletHolder> remainingServlets = new ArrayList<ServletHolder>();
            Set<String> names = new HashSet<String>();
            for (ServletHolder holder : holders)
            {
                if (!filterName.equals(holder.getName()))
                {
                    remainingServlets.add(holder);
                    names.add(holder.getName());
                }
            }
            
            List<ServletMapping> mappings = new ArrayList<ServletMapping>();
            for(ServletMapping mapping : handler.getServletMappings())
            {
               if(names.contains(mapping.getServletName()))
               {
                   mappings.add(mapping);
               }
            }
            
            /* Set the new configuration for the mappings and the servlets */
            handler.setServletMappings(mappings.toArray(new ServletMapping[0]));
            handler.setServlets(remainingServlets.toArray(new ServletHolder[0]));
            this.servlets.remove(filterName);
            this.server.setHandler(handler);
            Log.log.debug("Successfully stopped filter end point for : " + filterName);
        }
        catch(Exception e)
        {
            if(e.getMessage() != null && e.getMessage().contains("STARTED")) //when started is new stopped? :)
            {
                Log.log.debug("Successfully stopped filter end point for : " + filterName);
            }
            else
            {
                Log.log.error("Could not stopped servlet for filter: " + filterName + ". " + e.getMessage());
            }
        }
    }
    
    public void addDefaultResolveServlet() throws Exception
    {
        Log.log.debug("Adding Default Resolve Servlet at URI: " + "/");
        ServletHandler context;
        if(server.getHandler() instanceof ServletContextHandler)
        {
            context = ((ServletContextHandler) server.getHandler()).getServletHandler();
        }
        else
        {
            context = (ServletHandler) server.getHandler();
        }
        ServletHolder servletHolder = new ServletHolder("/", new ResolveDefaultHttpServlet());
        context.addServletWithMapping(servletHolder, "/");
    }
}
