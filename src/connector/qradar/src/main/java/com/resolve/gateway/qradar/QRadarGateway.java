package com.resolve.gateway.qradar;

import java.util.Map;
import java.util.HashMap;
import java.util.List;
import java.util.concurrent.ConcurrentHashMap;
import com.resolve.gateway.BaseClusteredGateway;
import com.resolve.gateway.BaseFilter;
import com.resolve.gateway.Filter;
import com.resolve.gateway.MQRadar;
import com.resolve.rsremote.Main;
import com.resolve.util.Constants;
import com.resolve.util.Log;
import com.resolve.util.StringUtils;
import com.resolve.util.metrics.ExecutionMetrics;
import com.resolve.util.metrics.ExecutionMetricsException;

import net.sf.json.JSONArray;
import net.sf.json.JSONObject;

public class QRadarGateway extends BaseClusteredGateway {

    private static volatile QRadarGateway instance = null;
    
    private Map<Integer, QRadarHttpServer> qradarHttpServers = new HashMap<Integer, QRadarHttpServer>();
    private Map<String, QRadarHttpServer> deployedServlets = new HashMap<String, QRadarHttpServer>();

    private String queue = null;
    
    private QRadarHttpServer defaultHttpServer;

    public static QRadarGateway getInstance(ConfigReceiveQRadar config) {
        if (instance == null) {
            instance = new QRadarGateway(config);
        }
        return instance;
    }

    /**
     * This method is called once the initialization is done. If it gets called
     * before the initialization it will throw exception.
     *
     * @return
     */
    public static QRadarGateway getInstance() {
        if (instance == null) {
            throw new RuntimeException("QRadar Gateway is not initialized correctly..");
        } else {
            return instance;
        }
    }

    private QRadarGateway(ConfigReceiveQRadar config) {
        super(config);
    }

    @Override
    public String getLicenseCode() {
        return "QRADAR";
    }

    @Override
    protected void addAdditionalValues(Map<String, Object> arg0, Map<String, String> arg1) {
    }

    @Override
    protected String getGatewayEventType() {
        return "QRADAR";
    }

    @Override
    protected String getMessageHandlerName() {
        return MQRadar.class.getSimpleName();
    }

    @Override
    protected Class<MQRadar> getMessageHandlerClass() {
        return MQRadar.class;
    }

    public String getQueueName() {
        return queue + getOrgSuffix();
    }

    @Override
    public void start() {
        Log.log.debug("Starting QRadar Gateway");
        super.start();
    }

    @Override
    public void run() {
        super.sendSyncRequest();
        
        if (isPrimary() && isActive())
            initQRadarServers();
    }

    @Override
    protected void initialize() {
        ConfigReceiveQRadar config = (ConfigReceiveQRadar) configurations;
        queue = config.getQueue().toUpperCase();
        try {
            Log.log.info("Initializing QRadar Listener");
            this.gatewayConfigDir = "/config/qradar/";
            
            defaultHttpServer = new QRadarHttpServer(config);
        } catch (Exception e) {
            Log.log.error("Failed to config QRadar Gateway: " + e.getMessage(), e);
        }
    }

    /**
     * This method processes the message received from the QRadar system.
     *
     * @param message
     */
    public boolean processData(String filterName, Map<String, String> params) {
        boolean result = true;
        try {
            if (StringUtils.isNotBlank(filterName) && params != null) {
                QRadarFilter qradarFilter = (QRadarFilter) filters.get(filterName);
                if (qradarFilter != null && qradarFilter.isActive()) {
                    if (Log.log.isDebugEnabled()) {
                        Log.log.debug("Processing filter: " + qradarFilter.getId());
                        Log.log.debug("Data received through QRadar gateway: " + params);
                    }
                    Map<String, String> runbookParams = params;
                    if (!params.containsKey(Constants.EVENT_EVENTID) || StringUtils.isBlank(params.get(Constants.EVENT_EVENTID))) {
                        runbookParams.put(Constants.EVENT_EVENTID, qradarFilter.getEventEventId());
                    }
                    addToPrimaryDataQueue(qradarFilter, runbookParams);
                } else {
                    result = false;
                }
            }
        } catch (Exception e) {
            Log.log.warn("Error: " + e.getMessage(), e);
            try {
                Thread.sleep(interval);
            } catch (InterruptedException ie) {
                Log.log.warn("sleep interrupted: " + ie.getMessage(), ie);
            }
        }
        return result;
    }

    @Override
    public void clearAndSetFilters(List<Map<String, Object>> filterList) {
        Log.log.debug("Calling Gateway specific clearAndSetFilter" + (filterList != null ? " " + filterList.size() + ", [" + filterList + "]" : ""));
        super.clearAndSetFilters(filterList);
        
        if (isPrimary() && isActive()) {
            try {
                initQRadarServers();
            } catch (Exception e) {
                Log.log.error(e.getMessage(), e);
            }
        }
    }

    @Override
    protected void cleanFilter(ConcurrentHashMap<String, Filter> undeployedFilters) {
        //Add customized code here to modify your server when you clear out the deployed filters;
        
        for(Filter filter : undeployedFilters.values())
        {
            QRadarFilter qradarFilter = (QRadarFilter) filter;
            
            try
            {
                if(qradarFilter.getPort() > 0)
                {
                    QRadarHttpServer httpServer = qradarHttpServers.get(qradarFilter.getPort());
                    httpServer.removeServlet(qradarFilter.getId());
                    //if there is no filter/servlet remaining then stop the server on the port
                    if(httpServer.getServlets().isEmpty())
                    {
                        httpServer.stop();
                        qradarHttpServers.remove(qradarFilter.getPort());
                    }
                }
                else
                {
                    //undeploy the servlet from the default server.
                    defaultHttpServer.removeServlet(qradarFilter.getId());
                }
            }
            catch (Exception e)
            {
                Log.log.error(e.getMessage(), e);
            }
        }
    }

    private void initQRadarServers() {
        //Add customized code here to initilize your server based on configuration and deployed filter data;
        
        try {
            if (!defaultHttpServer.isStarted()) {
                defaultHttpServer.init();
                defaultHttpServer.start();
                
                if (defaultHttpServer.isStarted())
                {
                    defaultHttpServer.addDefaultResolveServlet();
                }
            }
            
            for (Filter filter : orderedFilters) {
                QRadarFilter qradarFilter = (QRadarFilter) filter;
                //add the URI based servlets to the default server
                if (qradarFilter.getPort() == null || qradarFilter.getPort() <= 0) {
                    if (defaultHttpServer.isStarted()) {
                        defaultHttpServer.addServlet(qradarFilter);
                        deployedServlets.put(filter.getId(), defaultHttpServer);
                    }
                } else {
                    QRadarHttpServer httpServer = qradarHttpServers.get(qradarFilter.getPort());
                    
                    if (httpServer == null)
                    {
                        QRadarHttpServer httpServer1 = deployedServlets.get(filter.getId());
                        
                        if (httpServer1 != null)
                        {
                            //port changed
                            httpServer1.removeServlet(filter.getId());
                            deployedServlets.remove(filter.getId());
                        }
                        
                        httpServer = new QRadarHttpServer((ConfigReceiveQRadar) configurations, qradarFilter.getPort(), qradarFilter.getSsl());
                        httpServer.init();
                        httpServer.addServlet(qradarFilter);
                        httpServer.start();
                    }
                    else
                    {
                        httpServer.addServlet(qradarFilter);
                    }
                    
                    qradarHttpServers.put(qradarFilter.getPort(), httpServer);
                    deployedServlets.put(filter.getId(), httpServer);
                }
            }
        }
        catch (Exception e)
        {
            Log.log.error(e.getMessage(), e);
        }
    }

    @Override
    public void stop() {
        Log.log.warn("Stopping QRadar gateway");
        //Add customized code here to stop the connection with 3rd party system;
        
        for (QRadarHttpServer httpServer : qradarHttpServers.values()) {
            try {
                httpServer.stop();
            } catch (Exception e) {
                Log.log.error(e.getMessage(), e);
            }
        }
        
        try {
            defaultHttpServer.stop();
        } catch (Exception e)  {
            Log.log.error(e.getMessage(), e);
        }
        
        qradarHttpServers.clear();
        
        super.stop();
    }

    @Override
    public Filter getFilter(Map<String, Object> params) {
        return new QRadarFilter((String) params.get(BaseFilter.ID), (String) params.get(BaseFilter.ACTIVE), (String) params.get(BaseFilter.ORDER), (String) params.get(BaseFilter.INTERVAL), (String) params.get(BaseFilter.EVENT_EVENTID), (String) params.get(BaseFilter.RUNBOOK), (String) params.get(BaseFilter.SCRIPT), (String) params.get(QRadarFilter.PORT), (String) params.get(QRadarFilter.BLOCKING), (String) params.get(QRadarFilter.URI), (String) params.get(QRadarFilter.SSL));
    }
    
    public int getWaitTimeout() {

        int timeout = 120;
        
        try {
            ConfigReceiveQRadar configReceiveQRadar = (ConfigReceiveQRadar)configurations;
            String waitTimeout = configReceiveQRadar.getWait_timeout();
            timeout = (new Integer(waitTimeout)).intValue();
        } catch(Exception e) {
            Log.log.error(e.getMessage(),e);
        }
        
        return timeout;
    }

    public int getExecuteTimeout() {

        int timeout = 120;
        
        try {
            ConfigReceiveQRadar configReceiveQRadar = (ConfigReceiveQRadar)configurations;
            String executeTimeout = configReceiveQRadar.getExecute_timeout();
            timeout = (new Integer(executeTimeout)).intValue();
        } catch(Exception e) {
            Log.log.error(e.getMessage(),e);
        }
        
        return timeout;
    }
    
    public String getRunbookResult(String problemId, String processId, String wiki) {
        
        String str = "";
        long timeout = getWaitTimeout()*1000;
        
        Map<String, Object> params = new HashMap<String, Object>();
        params.put(Constants.EXECUTE_PROBLEMID, problemId);
        params.put(Constants.EXECUTE_PROCESSID, processId);
        
        try {
            Map<String, Object> result = Main.getESB().call(Constants.ESB_NAME_RSCONTROL, "MAction.getRunbookResult", params, timeout);
            
            if(result != null)
                str = parseRunbookResult(result, wiki);
        } catch(Exception e) {
            Log.log.error(e.getMessage(), e);
        }
        
        return str;
    }
    
    public String parseRunbookResult(Map<String, Object> result, String wiki)
    {
        String problemId = (String)result.get(Constants.EXECUTE_PROBLEMID);
        String processId = (String)result.get(Constants.EXECUTE_PROCESSID);
        String number = (String)result.get(Constants.WORKSHEET_NUMBER);
        String status = (String)result.get(Constants.STATUS);
        String condition = (String)result.get(Constants.EXECUTE_CONDITION);
        String severity = (String)result.get(Constants.EXECUTE_SEVERITY);
        List<Map<String, String>> taskStatus = (List<Map<String, String>>)result.get(Constants.TASK_STATUS);

        JSONObject json = new JSONObject();
        
        try
        {
            json.accumulate("Wiki Runbook", wiki);
            json.accumulate("Worksheet Id", problemId);
            json.accumulate("Worksheet Number", number);
            json.accumulate("Process Id", processId);
            json.accumulate("Condition", condition);
            json.accumulate("Severity", severity);
            json.accumulate("Status", status);
            
            if (StringUtils.isNotEmpty(status) && !status.equalsIgnoreCase(Constants.EXECUTE_STATUS_UNKNOWN))
            {
                JSONArray tasks = new JSONArray();
                
                for (Map<String, String> task:taskStatus)
                {
                    JSONObject taskItem = new JSONObject();
                    taskItem.accumulate("name", task.get(Constants.EXECUTE_TASKNAME));
                    taskItem.accumulate("id", task.get(Constants.EXECUTE_ACTIONID));
                    taskItem.accumulate("completion", task.get(Constants.EXECUTE_COMPLETION));
                    taskItem.accumulate("condition", task.get(Constants.EXECUTE_CONDITION));
                    taskItem.accumulate("severity", task.get(Constants.EXECUTE_SEVERITY));
                    taskItem.accumulate("summary", task.get(Constants.EXECUTE_SUMMARY));
                    taskItem.accumulate("detail", task.get(Constants.EXECUTE_DETAIL));
                    
                    tasks.add(taskItem);
                }
            
                json.accumulate("Action Tasks", tasks);
            }
        }
        catch(Exception e)
        {
            Log.log.error(e.getMessage(), e);
        }
        
        return json.toString();
        
    } // parseRunbookResult()
    
    public String processBlockingData(String filterName, Map<String, String> params)
    {
        String result = "";
        
//        ExecutionMetrics.INSTANCE.registerEventStart("QRadar GW - execute runbook and get result or submit runbook for execution");
        
        try
        {
            if(StringUtils.isNotBlank(filterName) && params != null)
            {
                QRadarFilter httpFilter = (QRadarFilter) filters.get(filterName);
                if (httpFilter != null && httpFilter.isActive())
                {
                    if(Log.log.isDebugEnabled())
                    {
                        Log.log.debug("Processing filter: " + httpFilter.getId());
                        Log.log.debug("Data received through HTTP gateway: " + params);
                    }
                    
                    Map<String, String> runbookParams = params;
                    
                    if(!params.containsKey(Constants.EVENT_EVENTID) || StringUtils.isBlank(params.get(Constants.EVENT_EVENTID)))
                    {
                        runbookParams.put(Constants.EVENT_EVENTID, httpFilter.getEventEventId());
                    }

                    runbookParams.put("FILTER_ID", filterName);
                    
                    return instance.receiveData(runbookParams);
                }
                else
                {
                    JSONObject message = new JSONObject();
                    message.accumulate("Message", "The filter is inactive.");
                    result = message.toString();
                }
            }
        }
        catch (Exception e)
        {
            Log.log.warn("Error: " + e.getMessage(), e);
            try
            {
                Thread.sleep(interval);
            }
            catch (InterruptedException ie)
            {
                Log.log.warn("sleep interrupted: " + ie.getMessage(), ie);
            }
        }
/*        finally
        {
            try
            {
                ExecutionMetrics.INSTANCE.registerEventEnd("QRadar GW - execute runbook and get result or submit runbook for execution", 1);
            }
            catch (ExecutionMetricsException eme)
            {
                Log.log.error(eme.getMessage(), eme);
            }
        }*/
        
        return result;
    }
    
    public Map<String, Object> loadSystemProperties() {

        Map<String, Object> properties = new HashMap<String, Object>();

        ConfigReceiveQRadar config = (ConfigReceiveQRadar) configurations;

        properties.put("HOST", config.getHost());
        properties.put("PORT", config.getPort());
        properties.put("SSL", config.getSsl());
        properties.put("URI", config.getUri());
        properties.put("SEC", config.getSec());

        return properties;
    }
}