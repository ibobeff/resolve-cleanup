package com.resolve.gateway;

import java.util.Map;
import com.resolve.gateway.qradar.QRadarFilter;
import com.resolve.gateway.qradar.QRadarGateway;

public class MQRadar extends MGateway {

    private static final String RSCONTROL_SET_FILTERS = "MQRadar.setFilters";

    private static final QRadarGateway instance = QRadarGateway.getInstance();

    public MQRadar() {
        super.instance = instance;
    }

    protected String getSetFilterMethodName() {
        return RSCONTROL_SET_FILTERS;
    }

    /**
     * Populates the {@link Filter} data into the given filterMap {@link Map} instance
     * 
     * @param filterMap
     *      the {@link Map} instance to hold the {@link Filter} data
     * @param filter
     *      a {@link QRadarFilter} instance
     *            
     */
    public void populateGatewaySpecificFilterProperties(Map<String, String> filterMap, Filter filter) {
        if (instance.isActive()) {
            QRadarFilter qradarFilter = (QRadarFilter) filter;
            filterMap.put(QRadarFilter.PORT, "" + qradarFilter.getPort());
            filterMap.put(QRadarFilter.BLOCKING, qradarFilter.getBlocking());
            filterMap.put(QRadarFilter.URI, qradarFilter.getUri());
            filterMap.put(QRadarFilter.SSL, "" + qradarFilter.getSsl());
        }
    }
}

