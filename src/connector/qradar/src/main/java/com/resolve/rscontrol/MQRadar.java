package com.resolve.rscontrol;

import com.resolve.persistence.model.QRadarFilter;

public class MQRadar extends MGateway {

    private static final String MODEL_NAME = QRadarFilter.class.getSimpleName();

    protected String getModelName() {
        return MODEL_NAME;
    }
}

