/******************************************************************************
* (C) Copyright 2014
*
* Resolve Systems, LLC
*
* All rights reserved
* This software is distributed under license and may not be used, copied,
* modified, or distributed without the express written permission of
* Resolve Systems, LLC.
*
******************************************************************************/

package com.resolve.gateway.qradar;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.net.CookieHandler;
import java.net.CookieManager;
import java.net.CookiePolicy;
import java.net.CookieStore;
import java.net.HttpCookie;
import java.net.HttpURLConnection;
import java.net.URL;
import java.net.URLEncoder;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;

import javax.net.ssl.HttpsURLConnection;

import com.resolve.util.Log;
import com.resolve.util.StringUtils;

import net.sf.json.JSONArray;
import net.sf.json.JSONObject;
import net.sf.json.JSONSerializer;

public class QRadarAPI
{    
    private static String HOSTNAME = "";
    private static String PORT = "";
    private static String SSL = "Self-Signed";
    private static String URI = "/api/siem/";
    private static String SEC = "";
    
    private static QRadarGateway instance = QRadarGateway.getInstance();
    
    private static String baseUrl = null;
    private static boolean selfSigned = true;

    static {
        try {
            Map<String, Object> properties = instance.loadSystemProperties();
            
            HOSTNAME = (String)properties.get("HOST");
            PORT = (String)properties.get("PORT");
            SSL = (String)properties.get("SSL");
            URI = (String)properties.get("URI");
            SEC = (String)properties.get("SEC");
            
            // QRadar only support HTTPS, either self-signed or CA-signed, default is self-signed.
            selfSigned = (SSL == null || SSL.length() == 0 || SSL.equalsIgnoreCase("Self-Signed"))?true:false;
            
            StringBuilder sb = new StringBuilder();
            
            sb.append("https://").append(HOSTNAME);
            if(StringUtils.isNotBlank(PORT))
                sb.append(":").append(PORT);
            sb.append(URI);
            
            baseUrl = sb.toString();
        } catch(Exception e) {
          Log.log.error(e.getMessage(), e);
        }
    }
    
    /**
     * Retrieve the offense detail information per offense id and optional query fields included
     * @param offenseId: Required - The offense ID.
     * @param fields: Optional - Use this parameter to specify which fields you would like to get back in the response. 
     *                Fields that are not named are excluded. Specify subfields in brackets and multiple fields in the same object are separated by commas.
     * @return a JSON string of offense detail information
     * @throws Exception
     */
    public static String getOffense(String offenseId, String fields) throws Exception {
        
        if(offenseId == null || offenseId.length() == 0)
            throw new Exception("No offense id is provided.");

        Map<String, String> headers = new HashMap<String, String>();
        headers.put("sec", SEC);

        String requestUrl = baseUrl + "offenses/" + offenseId;
        
        if(fields != null && fields.length() != 0)
            requestUrl = requestUrl + "?fields=" + URLEncoder.encode(fields, "UTF-8");
        
        Log.log.debug(requestUrl);
        
        return getHTTPSURLResponse(requestUrl, "GET", headers, null, selfSigned);
    }
    
    /**
     * Retrieve a list of offenses that meet the query criteria
     * @param start: Optional - Use this parameter to restrict the number of elements that are returned in the list to a specified range. The list is indexed starting at zero.
     * @param end: Optional - Use this parameter to restrict the number of elements that are returned in the list to a specified range. 
     * @param fields: Optional - Use this parameter to specify which fields you would like to get back in the response. Fields that are not named are excluded. 
     *                           Specify subfields in brackets and multiple fields in the same object are separated by commas.
     * @param filter: Optional - This parameter is used to restrict the elements in a list base on the contents of various fields.
     * @return a JSON string a list of offense details
     * @throws Exception
     */
    public static String getOffenses(Integer start, Integer end, String fields, String filter) throws Exception {

        String requestUrl = baseUrl + "offenses";
        Map<String, String> headers = new HashMap<String, String>();
        headers.put("sec", SEC);
        
        if(start != null && end != null) {
            String range = "items=" + start.toString() + "-" + end.toString();
            headers.put("range", range);
        }
        
        if(fields != null && fields.length() != 0) {
            if(!requestUrl.contains("?"))
                requestUrl =  requestUrl + "?";
            else
                requestUrl =  requestUrl + "&";
            requestUrl = requestUrl + "fields=" + URLEncoder.encode(fields, "UTF-8");
        }
        
        if(filter != null && filter.length() != 0) {
            if(!requestUrl.contains("?"))
                requestUrl =  requestUrl + "?";
            else
                requestUrl =  requestUrl + "&";
            requestUrl = requestUrl + "filter=" + URLEncoder.encode(filter, "UTF-8");
        }
        
        Log.log.debug(requestUrl);  
        
        return getHTTPSURLResponse(requestUrl, "GET", headers, null, selfSigned);
    }

    /**
     * Update offense per offense id and parameters
     * @param offenseId: Required - The ID of the offense to update.
     * @param isProtected: Optional - Set to true to protect the offense.
     * @param followUp: Optional - Set to true to set the follow up flag on the offense.
     * @param status: Optional - The new status for the offense. Set to one of: OPEN, HIDDEN, CLOSED. 
     *                           When the status of an offense is being set to CLOSED, a valid closing_reason_id must be provided. 
     *                           To hide an offense, use the HIDDEN status. To show a previously hidden offense, use the OPEN status.
     * @param closingReasonId: Optional - The ID of a closing reason. You must provide a valid closing_reason_id when you close an offense.
     * @param assignedTo: Optional - A user to assign the offense to.
     * @param fields: Optional - Use this parameter to specify which fields you would like to get back in the response. Fields that are not named are excluded. Specify subfields in brackets and multiple fields in the same object are separated by commas.
     * @return a JSON string of updated offense detail information
     * @throws Exception
     */
    public static String postOffense(String offenseId, Boolean isProtected, Boolean followUp, String status, Integer closingReasonId, String assignedTo, String fields) throws Exception {

        if(offenseId == null || offenseId.length() == 0)
            throw new Exception("No offense id is provided.");

        Map<String, String> headers = new HashMap<String, String>();
        headers.put("sec", SEC);
        
        String requestUrl = baseUrl + "offenses/" + offenseId;
        
        if(status != null && status.length() != 0) {
            if(!requestUrl.contains("?"))
                requestUrl =  requestUrl + "?";
            else
                requestUrl =  requestUrl + "&";
            requestUrl = requestUrl + "status=" + status;
        }
        
        if(assignedTo != null && assignedTo.length() != 0) {
            if(!requestUrl.contains("?"))
                requestUrl =  requestUrl + "?";
            else
                requestUrl =  requestUrl + "&";
            requestUrl = requestUrl + "assignedTo=" + URLEncoder.encode(assignedTo, "UTF-8");
        }
        
        if(fields != null && fields.length() != 0) {
            if(!requestUrl.contains("?"))
                requestUrl =  requestUrl + "?";
            else
                requestUrl =  requestUrl + "&";
            requestUrl = requestUrl + "fields=" + URLEncoder.encode(fields, "UTF-8");
        }
        
        if(isProtected != null) {
            if(!requestUrl.contains("?"))
                requestUrl =  requestUrl + "?";
            else
                requestUrl =  requestUrl + "&";
            requestUrl = requestUrl + "protected=" + isProtected;
        }
        
        if(followUp != null) {
            if(!requestUrl.contains("?"))
                requestUrl =  requestUrl + "?";
            else
                requestUrl =  requestUrl + "&";
            requestUrl = requestUrl + "follow_up=" + followUp;
        }
        
        if(closingReasonId != null) {
            if(!requestUrl.contains("?"))
                requestUrl =  requestUrl + "?";
            else
                requestUrl =  requestUrl + "&";
            requestUrl = requestUrl + "closing_reason_id=" + closingReasonId;
        }
        
        Log.log.debug(requestUrl);   
        
        return getHTTPSURLResponse(requestUrl, "POST", headers, null, selfSigned);
    }
    
    /**
     * Retrieve the note object for the note ID. A note object contains id, create_time, username, note_text fields.
     * @param offenseId: Required - The offense ID to retrieve the note from.
     * @param noteId: Required - The note ID.
     * @param fields: Optional - Use this parameter to specify which fields you would like to get back in the response. 
                                 Fields that are not named are excluded. Specify subfields in brackets and multiple fields in the same object are separated by commas.
     * @return a JSON string of the offense note object
     * @throws Exception
     */
    public static String getOffenseNote(String offenseId, String noteId, String fields) throws Exception {
        
        if(offenseId == null || offenseId.length() == 0 || noteId == null || noteId.length() == 0)
            throw new Exception("No offense id or note id is provided.");
        
        try {
            new Integer(offenseId);
            new Integer(noteId);
        } catch(Exception e) {
            throw new Exception("Offense id or note id must be a number.");
        }
        
        Map<String, String> headers = new HashMap<String, String>();
        headers.put("sec", SEC);
        
        String requestUrl = baseUrl + "offenses/" + offenseId + "/notes/" + noteId;

        if(fields != null && fields.length() != 0)
            requestUrl = requestUrl + "?fields=" + URLEncoder.encode(fields, "UTF-8");
        
        Log.log.debug(requestUrl);  
        
        return getHTTPSURLResponse(requestUrl, "GET", headers, null, selfSigned);
    }
    
    /**
     * Retrieve an array of Note objects. A note object contains id, create_time, username, note_text fields.
     * @param offenseId: Required - The offense ID to retrieve the notes for.
     * @param fields: Optional - Use this parameter to specify which fields you would like to get back in the response. 
     *                           Fields that are not named are excluded. Specify subfields in brackets and multiple fields in the same object are separated by commas.
     * @param filter: Optional - This parameter is used to restrict the elements in a list base on the contents of various fields.
     * @param start
     * @param end
     * @return a JSON string a list of offense note objects
     * @throws Exception
     */
    public static String getOffenseNotes(String offenseId, String fields, String filter, Integer start, Integer end) throws Exception {
        
        if(offenseId == null || offenseId.length() == 0)
            throw new Exception("No offense id is provided.");
        
        Map<String, String> headers = new HashMap<String, String>();
        headers.put("sec", SEC);
        
        String requestUrl = baseUrl + "offenses/" + offenseId + "/notes";
        
        if(fields != null && fields.length() != 0) {
            if(!requestUrl.contains("?"))
                requestUrl =  requestUrl + "?";
            else
                requestUrl =  requestUrl + "&";
            requestUrl = requestUrl + "?" + URLEncoder.encode(fields, "UTF-8");
        }
        
        if(filter != null && filter.length() != 0) {
            if(!requestUrl.contains("?"))
                requestUrl =  requestUrl + "?";
            else
                requestUrl =  requestUrl + "&";
            requestUrl = requestUrl + "filter=" + URLEncoder.encode(filter, "UTF-8");
        }
        
        if(start != null && end != null) {
            try {
                String range = "items=" + start.toString() + "-" + end.toString();
                headers.put("range", range);
            } catch(Exception e) {
//                e.printStackTrace();
                Log.log.debug(e.getMessage(), e);
            }
        }
        
        Log.log.debug(requestUrl);
        
        return getHTTPSURLResponse(requestUrl, "GET", headers, null, selfSigned);
    }
    
    /**
     * Create a note object per offense id and note text.
     * @param offenseId: Required - The offense ID to add the note to.
     * @param note: Required - The note text.
     * @param fields: Optional - Use this parameter to specify which fields you would like to get back in the response. 
                                 Fields that are not named are excluded. Specify subfields in brackets and multiple fields in the same object are separated by commas.
     * @return a JSON string of the offense note object being created.
     * @throws Exception
     */
    public static String postOffenseNote(String offenseId, String note, String fields) throws Exception {

        if(offenseId == null || offenseId.length() == 0 || note == null || note.length() == 0)
            throw new Exception("No offense id or note is provided.");

        Map<String, String> headers = new HashMap<String, String>();
        headers.put("sec", SEC);
        
        String requestUrl = baseUrl + "offenses/" + offenseId + "/notes?note_text=" + URLEncoder.encode(note, "UTF-8");
        
        if(fields != null && fields.length() != 0)
            requestUrl = requestUrl + "&fields=" + URLEncoder.encode(fields, "UTF-8");
        
        Log.log.debug(requestUrl);
        
        return getHTTPSURLResponse(requestUrl, "POST", headers, null, selfSigned);
    }

    private static String parseException(String input) throws Exception {
        
        JSONObject attr = JSONObject.fromObject(input);
        for(Iterator<String> it = attr.keySet().iterator(); it.hasNext();) {
            String key = it.next();
            String value = (String)attr.get(key);
            
            if(key != null && key.equals("message"))
               return value;
        }

        return "";
    }
    
    private static List<Map<String, Object>> getJsonArrayMap(String input) throws Exception {
        
        List<Map<String, Object>> list = new ArrayList<Map<String, Object>>();

        JSONArray jsonArray = (JSONArray)JSONSerializer.toJSON(input);
        for(int i=0; i<jsonArray.size(); i++) {
            Map<String, Object> attr = (Map<String, Object>)jsonArray.get(i);
            Set<String> keySet = attr.keySet();
            
            Map<String, Object> offense = new HashMap<String, Object>();
            for(Iterator<String> it = keySet.iterator(); it.hasNext();) {
                String key = it.next();
                Object value = attr.get(key);

                offense.put(key, value);
            }
            
            list.add(offense);
        }

        return list;
    }
    
    private static String getHTTPURLResponse(String urlStr, String method, Map<String, String> httpHeader, String body) throws Exception {
        
        return getHTTPSURLResponse(urlStr, method, httpHeader, body, false);
    }
    
    private static String getHTTPSURLResponse(String urlStr, String method, Map<String, String> httpHeader, String body, boolean selfSigned) throws Exception {
        
        HttpURLConnection conn = null;
        OutputStream output = null;
        BufferedReader bufferedReader = null;
        StringBuilder response = new StringBuilder();
        
        CookieManager cookieManager = new CookieManager();
        cookieManager.setCookiePolicy(CookiePolicy.ACCEPT_ALL);
        CookieStore cookieStore = cookieManager.getCookieStore();
        CookieHandler.setDefault(cookieManager);
        
        try {       
            URL url = new URL(urlStr);
//            Log.log.info("url = " + urlStr);
            
            String protocol = url.getProtocol();
            if(protocol != null && protocol.length() != 0 && protocol.equalsIgnoreCase("https")) {
                if(selfSigned) {
                    HttpsURLConnection.setDefaultHostnameVerifier(new javax.net.ssl.HostnameVerifier() {
                        public boolean verify(String hostname, javax.net.ssl.SSLSession sslSession) {
                            return true;
                        }
                    });
                }
                
                conn = (HttpsURLConnection)url.openConnection();
//                conn.setRequestProperty("User-Agent", "Mozilla/5.0");
                conn.setDoInput(true);
            } // https
            
            else
                conn = (HttpURLConnection) url.openConnection();
            
            conn.setRequestMethod(method);
            
            if(httpHeader != null) {
                Set<String> keys = httpHeader.keySet();
                for(Iterator it = keys.iterator(); it.hasNext();) {
                    String key = (String)it.next();
                    String value = httpHeader.get(key);
                    
                    conn.addRequestProperty(key,  value);
                }
            }
            
            List<HttpCookie> cookies = cookieStore.getCookies();
            
            if (cookies != null) {
                StringBuilder sb = new StringBuilder();
                
                for (HttpCookie cookie : cookies) {
                    String cookieName = cookie.getName();
                    String cookieValue = cookie.getValue();
                    
                    sb.append(cookieName).append("=").append(cookieValue).append(";");
                }
                
//                Log.log.info("cookies = " + sb.toString());
                conn.addRequestProperty("Cookie", sb.toString());
            }
            
            if(body != null) {
                byte[] outputInBytes = body.getBytes("UTF-8");
                conn.setDoOutput(true);
                output = conn.getOutputStream();
                output.write(outputInBytes);
            }
            
            bufferedReader = new BufferedReader(new InputStreamReader(conn.getInputStream()));
            
            String line = null;
            while ((line = bufferedReader.readLine()) != null)
                response.append(line);

            conn.getContent();
        } catch(Exception e) {
//            e.printStackTrace();
            Log.log.error(e.getMessage(), e);
            throw e;
        } finally {
            try {
                if(output != null)
                    output.close();
                if(bufferedReader != null)
                    bufferedReader.close();
                if(conn != null)
                    conn.disconnect();
            } catch(Exception ee) {}
        }
        
        return response.toString();
    } // getResponse()
  
    private static void main1(String[] args) {
        
        test();
    }
  
    private static void test_ssl() {

        String host = "10.20.20.26";
        String port = "8443";
        
        String ssl = "true";
        String selfSigned = "true";
        
        String user = "admin";
        String pass = "resolve";
        
        String worksheetId = "";
        String token = "";
        
        String protocol = (ssl == null || ssl.length() == 0 || ssl.equals("false"))?"http://":"https://";
        String loginUrl = protocol + host + ":" + port + "/resolve/rest/login?username=" + user + "&password=" + pass;
System.out.println("loginUrl = " + loginUrl);
        Map<String, String> headers = new HashMap<String, String>();
//          headers.put("Accept", "application/json");
//          headers.put("Content-Type", "application/json");
        String login = null;
        try {
            if(selfSigned == null || selfSigned.length() == 0 || selfSigned.equals("false"))
                login = QRadarAPI.getHTTPURLResponse(loginUrl, "GET", headers, null);
            else
                login = QRadarAPI.getHTTPSURLResponse(loginUrl, "GET", headers, null, true);
System.out.println("login = " + login);
            if(login != null) {
                if(login.contains("Error") || login.contains("error"))
                    worksheetId = login;
                else {
                    int index = login.lastIndexOf(":");
                    if(index != -1)
                        token = login.substring(index+2, login.length()-2);
                }
            }
        } catch(Exception e) {
            e.printStackTrace();
        }
System.out.println("token = " + token);
    }
    
    private static void test() {
        
        String response = "";
        String offenseId = "2";
        String sec = "7c62cba8-bda7-4e4e-99f1-42e052fc32ca";
        
        List<Map<String, String>> offenseList = new ArrayList<Map<String, String>>();
        
        try {
/*
            // 1. Get offense with id
            response = api.getOffense(offenseId, null);
            System.out.println("1. Get offense with id ... "); 
            System.out.println(response);

            Map<String, Object> attr = QRadarAPI.getJsonMap(response);
            StringBuilder sb = new StringBuilder();
            for(Iterator<String> it=attr.keySet().iterator(); it.hasNext();) {
                String key = it.next();
                Object value = attr.get(key);
                sb.append("&").append(key).append("=").append(value.toString());
            }
            System.out.println(sb.toString());
            System.exit(0);

            // 2. Get offenses
            response = api.getOffenses(0, 2, null, null);
            System.out.println("2. Get offenses ...");
            System.out.println(response);
            getJsonArrayMap(response);

            // 3. Post offense status
            response = api.postOffense(offenseId, "OPEN", null, null, null, null, null);
            System.out.println("3. Post offense ... ");
            System.out.println(response);

            // 4. Get offense note with id
            response = api.getOffenseNote(offenseId, "101", null);
            System.out.println("4. Get offense note with id ... ");
            System.out.println(response);
*/
            // 5. Get offense notes
            response = getOffenseNotes(offenseId, null, null,  0, 2);
            System.out.println("5. Get offense notes ... ");
            System.out.println(response);
            
            // 6. Post offense note
            response = postOffenseNote(offenseId, "test note", null);
            System.out.println("6. Post offense note ... ");
            System.out.println(response);
        } catch(Exception e) {
            e.printStackTrace();
        }
        
    } // main()
    
} // QRadarAPI
