package com.resolve.gateway.qradar;

import java.io.File;
import java.util.HashMap;
import java.util.Map;
import org.apache.commons.io.FileUtils;

import com.resolve.rsbase.MainBase;
import com.resolve.gateway.Filter;
import com.resolve.rsremote.ConfigReceiveGateway;
import com.resolve.util.Log;
import com.resolve.util.ObjectProperties;
import com.resolve.util.StringUtils;
import com.resolve.util.XDoc;

public class ConfigReceiveQRadar extends ConfigReceiveGateway {

    private static final long serialVersionUID = 1L;

    private static final String RECEIVE_QRADAR_NODE = "./RECEIVE/QRADAR/";

    private static final String RECEIVE_QRADAR_FILTER = RECEIVE_QRADAR_NODE + "FILTER";

    private String queue = "QRADAR";

    private static final String RECEIVE_QRADAR_ATTR_HTTP_SSLPASSWORD = RECEIVE_QRADAR_NODE + "@HTTP_SSLPASSWORD";

    private static final String RECEIVE_QRADAR_ATTR_EXECUTE_TIMEOUT = RECEIVE_QRADAR_NODE + "@EXECUTE_TIMEOUT";

    private static final String RECEIVE_QRADAR_ATTR_HTTP_PORT = RECEIVE_QRADAR_NODE + "@HTTP_PORT";

    private static final String RECEIVE_QRADAR_ATTR_HTTP_SSL = RECEIVE_QRADAR_NODE + "@HTTP_SSL";

    private static final String RECEIVE_QRADAR_ATTR_HTTP_USERNAME = RECEIVE_QRADAR_NODE + "@HTTP_USERNAME";

    private static final String RECEIVE_QRADAR_ATTR_WAIT_TIMEOUT = RECEIVE_QRADAR_NODE + "@WAIT_TIMEOUT";

    private static final String RECEIVE_QRADAR_ATTR_SSL = RECEIVE_QRADAR_NODE + "@SSL";

    private static final String RECEIVE_QRADAR_ATTR_URI = RECEIVE_QRADAR_NODE + "@URI";

    private static final String RECEIVE_QRADAR_ATTR_SEC = RECEIVE_QRADAR_NODE + "@SEC";

    private static final String RECEIVE_QRADAR_ATTR_PORT = RECEIVE_QRADAR_NODE + "@PORT";

    private static final String RECEIVE_QRADAR_ATTR_SCRIPT_TIMEOUT = RECEIVE_QRADAR_NODE + "@SCRIPT_TIMEOUT";

    private static final String RECEIVE_QRADAR_ATTR_HTTP_SSLCERTIFICATE = RECEIVE_QRADAR_NODE + "@HTTP_SSLCERTIFICATE";

    private static final String RECEIVE_QRADAR_ATTR_HOST = RECEIVE_QRADAR_NODE + "@HOST";

    private static final String RECEIVE_QRADAR_ATTR_HTTP_PASSWORD = RECEIVE_QRADAR_NODE + "@HTTP_PASSWORD";
    
    static final Integer DEFAULT_HTTP_PORT = 7778;
    
    static final Boolean DEFAULT_HTTP_SSL = false;

    private String http_sslpassword = "";

    private String execute_timeout = "";

    private String http_port = "";

    private String http_ssl = "";

    private String http_username = "";

    private String wait_timeout = "";

    private String ssl = "";

    private String uri = "";

    private String sec = "";

    private String port = "";

    private String script_timeout = "";

    private String http_sslcertificate = "";

    private String host = "";

    private String http_password = "";

    public String getQueue() {
        return queue;
    }

    public void setQueue(String queue) {
        this.queue = queue;
    }

    public String getHttp_sslpassword() {
        return this.http_sslpassword;
    }

    public void setHttp_sslpassword(String http_sslpassword) {
        this.http_sslpassword = http_sslpassword;
    }

    public String getExecute_timeout() {
        return this.execute_timeout;
    }

    public void setExecute_timeout(String execute_timeout) {
        this.execute_timeout = execute_timeout;
    }

    public String getHttp_port() {
        return this.http_port;
    }
    
    public Integer getHttpPort()
    {
        Integer httpPort = DEFAULT_HTTP_PORT;
        
        try {
            httpPort = new Integer(http_port);
        } catch(Exception e) {
            Log.log.error(e.getMessage(),e);
        }
        
        return httpPort;
    }

    public void setHttp_port(String http_port) {
        this.http_port = http_port;
    }

    public String getHttp_ssl() {
        return this.http_ssl;
    }
    
    public Boolean getHttpSsl()
    {
        Boolean httpSSL = DEFAULT_HTTP_SSL;
        
        try {
            httpSSL = new Boolean(http_ssl);
        } catch(Exception e) {
            Log.log.error(e.getMessage(),e);
        }
        
        return httpSSL;
    }

    public void setHttp_ssl(String http_ssl) {
        this.http_ssl = http_ssl;
    }

    public String getHttp_username() {
        return this.http_username;
    }

    public void setHttp_username(String http_username) {
        this.http_username = http_username;
    }

    public String getWait_timeout() {
        return this.wait_timeout;
    }

    public void setWait_timeout(String wait_timeout) {
        this.wait_timeout = wait_timeout;
    }

    public String getSsl() {
        return this.ssl;
    }

    public void setSsl(String ssl) {
        this.ssl = ssl;
    }

    public String getUri() {
        return this.uri;
    }

    public void setUri(String uri) {
        this.uri = uri;
    }

    public String getSec() {
        return this.sec;
    }

    public void setSec(String sec) {
        this.sec = sec;
    }

    public String getPort() {
        return this.port;
    }

    public void setPort(String port) {
        this.port = port;
    }

    public String getScript_timeout() {
        return this.script_timeout;
    }

    public void setScript_timeout(String script_timeout) {
        this.script_timeout = script_timeout;
    }

    public String getHttp_sslcertificate() {
        return this.http_sslcertificate;
    }

    public void setHttp_sslcertificate(String http_sslcertificate) {
        this.http_sslcertificate = http_sslcertificate;
    }

    public String getHost() {
        return this.host;
    }

    public void setHost(String host) {
        this.host = host;
    }

    public String getHttp_password() {
        return this.http_password;
    }

    public void setHttp_password(String http_password) {
        this.http_password = http_password;
    }

    public ConfigReceiveQRadar(XDoc config) throws Exception {
        super(config);
        define("http_sslpassword", SECURE, RECEIVE_QRADAR_ATTR_HTTP_SSLPASSWORD);
        define("execute_timeout", STRING, RECEIVE_QRADAR_ATTR_EXECUTE_TIMEOUT);
        define("http_port", STRING, RECEIVE_QRADAR_ATTR_HTTP_PORT);
        define("http_ssl", STRING, RECEIVE_QRADAR_ATTR_HTTP_SSL);
        define("http_username", STRING, RECEIVE_QRADAR_ATTR_HTTP_USERNAME);
        define("wait_timeout", STRING, RECEIVE_QRADAR_ATTR_WAIT_TIMEOUT);
        define("ssl", STRING, RECEIVE_QRADAR_ATTR_SSL);
        define("uri", STRING, RECEIVE_QRADAR_ATTR_URI);
        define("sec", STRING, RECEIVE_QRADAR_ATTR_SEC);
        define("port", STRING, RECEIVE_QRADAR_ATTR_PORT);
        define("script_timeout", STRING, RECEIVE_QRADAR_ATTR_SCRIPT_TIMEOUT);
        define("http_sslcertificate", STRING, RECEIVE_QRADAR_ATTR_HTTP_SSLCERTIFICATE);
        define("host", STRING, RECEIVE_QRADAR_ATTR_HOST);
        define("http_password", SECURE, RECEIVE_QRADAR_ATTR_HTTP_PASSWORD);
    }

    @Override
    public String getRootNode() {
        return RECEIVE_QRADAR_NODE;
    }

    @Override
    public void load() throws Exception {
        try {
            loadAttributes();
            if (isActive()) {
                QRadarGateway qradarGateway = QRadarGateway.getInstance(this);
                filters = xdoc.getListMapValue(RECEIVE_QRADAR_FILTER);
                if (filters.size() > 0) {
                    for (Map<String, Object> values : filters) {
                        String filterName = ((String) values.get(QRadarFilter.ID)).toLowerCase();
                        File scriptFile = getFile(MainBase.main.release.serviceName + "/config/qradar/" + filterName + ".groovy");
                        try {
                            if (scriptFile.exists()) {
                                String groovy = FileUtils.readFileToString(scriptFile, "UTF-8");
                                if (StringUtils.isNotBlank(groovy)) {
                                    values.put(QRadarFilter.SCRIPT, groovy);
                                    Log.log.debug("Loaded groovy script: " + scriptFile);
                                }
                            }
                            qradarGateway.setFilter(values);
                        } catch (Exception e) {
                            Log.log.warn(e.getMessage() + ". Skipping file: " + scriptFile.getAbsolutePath());
                        }
                    }
                }
            }
        } catch (Throwable e) {
            setActive(false);
            Log.log.warn("Couldn not load configuration for QRadar gateway, check blueprint. " + e.getMessage(), e);
        }
    }

    @Override
    public void save() throws Exception {
        File dir = getFile(MainBase.main.release.serviceName + "/config/qradar");
        if (!dir.exists()) {
            FileUtils.forceMkdir(dir);
        }
        if (filters != null && isActive()) {
            filters.clear();
            for (Filter filter : QRadarGateway.getInstance().getFilters().values()) {
                QRadarFilter qradarFilter = (QRadarFilter) filter;
                String groovy = qradarFilter.getScript();
                String scriptFileName;
                File scriptFile = null;
                if (StringUtils.isNotBlank(groovy)) {
                    scriptFileName = qradarFilter.getId().toLowerCase() + ".groovy";
                    scriptFile = getFile(MainBase.main.release.serviceName + "/config/qradar/" + scriptFileName);
                }
                Map<String, Object> entry = new HashMap<String, Object>();
                putFilterDataIntoEntry(entry, qradarFilter);
                filters.add(entry);
                if (scriptFile != null) {
                    try {
                        FileUtils.writeStringToFile(scriptFile, groovy, "UTF-8");
                    } catch (Exception e) {
                        Log.log.warn(e.getMessage() + ". Skipping file: " + scriptFile.getAbsolutePath());
                    }
                }
            }
            xdoc.setListMapValue(RECEIVE_QRADAR_FILTER, filters);
        }
        if (nameProperties != null && isActive()) {
            nameProperties.clear();
            for (String name : QRadarGateway.getInstance().getNameProperties().keySet()) {
                Map<String, Object> entry = new HashMap<String, Object>();
                entry.put(NAME_KEY, name);
                nameProperties.add(entry);
                Map props = QRadarGateway.getInstance().getNameProperties().getNameProperties(name);
                ObjectProperties prop = new ObjectProperties(props);
                prop.save(props);
            }
        }
        saveAttributes();
    }

    private void putFilterDataIntoEntry(Map<String, Object> entry, QRadarFilter qradarFilter) {
        entry.put(QRadarFilter.ID, qradarFilter.getId());
        entry.put(QRadarFilter.ACTIVE, String.valueOf(qradarFilter.isActive()));
        entry.put(QRadarFilter.ORDER, String.valueOf(qradarFilter.getOrder()));
        entry.put(QRadarFilter.INTERVAL, String.valueOf(qradarFilter.getInterval()));
        entry.put(QRadarFilter.EVENT_EVENTID, qradarFilter.getEventEventId());
        entry.put(QRadarFilter.RUNBOOK, qradarFilter.getRunbook());
        entry.put(QRadarFilter.SCRIPT, qradarFilter.getScript());
        entry.put(QRadarFilter.PORT, qradarFilter.getPort());
        entry.put(QRadarFilter.BLOCKING, qradarFilter.getBlocking());
        entry.put(QRadarFilter.URI, qradarFilter.getUri());
        entry.put(QRadarFilter.SSL, qradarFilter.getSsl());
    }
}

