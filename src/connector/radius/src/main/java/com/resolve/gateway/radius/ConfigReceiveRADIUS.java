package com.resolve.gateway.radius;

import java.io.File;
import java.util.HashMap;
import java.util.Map;
import org.apache.commons.io.FileUtils;

import com.resolve.rsbase.MainBase;
import com.resolve.gateway.Filter;
import com.resolve.rsremote.ConfigReceiveGateway;
import com.resolve.util.Log;
import com.resolve.util.ObjectProperties;
import com.resolve.util.StringUtils;
import com.resolve.util.XDoc;

public class ConfigReceiveRADIUS extends ConfigReceiveGateway {

    private static final long serialVersionUID = 1L;

    private static final String RECEIVE_RADIUS_NODE = "./RECEIVE/RADIUS/";

    private static final String RECEIVE_RADIUS_FILTER = RECEIVE_RADIUS_NODE + "FILTER";

    private String queue = "RADIUS";

    public String getQueue() {
        return queue;
    }

    public void setQueue(String queue) {
        this.queue = queue;
    }

    public ConfigReceiveRADIUS(XDoc config) throws Exception {
        super(config);
    }

    @Override
    public String getRootNode() {
        return RECEIVE_RADIUS_NODE;
    }

    @SuppressWarnings("unchecked")
    @Override
    public void load() throws Exception {
        try {
            loadAttributes();
            if (isActive()) {
                RADIUSGateway radiusGateway = RADIUSGateway.getInstance(this);
                filters = xdoc.getListMapValue(RECEIVE_RADIUS_FILTER);
                if (filters.size() > 0) {
                    for (Map<String, Object> values : filters) {
                        String filterName = ((String) values.get(RADIUSFilter.ID)).toLowerCase();
                        File scriptFile = getFile(MainBase.main.release.serviceName + "/config/radius/" + filterName + ".groovy");
                        try {
                            if (scriptFile.exists()) {
                                String groovy = FileUtils.readFileToString(scriptFile, "UTF-8");
                                if (StringUtils.isNotBlank(groovy)) {
                                    values.put(RADIUSFilter.SCRIPT, groovy);
                                    Log.log.debug("Loaded groovy script: " + scriptFile);
                                }
                            }
                            radiusGateway.setFilter(values);
                        } catch (Exception e) {
                            Log.log.warn(e.getMessage() + ". Skipping file: " + scriptFile.getAbsolutePath());
                        }
                    }
                }
            }
        } catch (Throwable e) {
            setActive(false);
            Log.log.warn("Couldn not load configuration for RADIUS gateway, check blueprint. " + e.getMessage(), e);
        }
    }

    @SuppressWarnings({ "rawtypes", "unchecked" })
    @Override
    public void save() throws Exception {
        File dir = getFile(MainBase.main.release.serviceName + "/config/radius");
        if (!dir.exists()) {
            FileUtils.forceMkdir(dir);
        }
        if (filters != null && isActive()) {
            filters.clear();
            for (Filter filter : RADIUSGateway.getInstance().getFilters().values()) {
                RADIUSFilter radiusFilter = (RADIUSFilter) filter;
                String groovy = radiusFilter.getScript();
                String scriptFileName;
                File scriptFile = null;
                if (StringUtils.isNotBlank(groovy)) {
                    scriptFileName = radiusFilter.getId().toLowerCase() + ".groovy";
                    scriptFile = getFile(MainBase.main.release.serviceName + "/config/radius/" + scriptFileName);
                }
                Map<String, Object> entry = new HashMap<String, Object>();
                putFilterDataIntoEntry(entry, radiusFilter);
                filters.add(entry);
                if (scriptFile != null) {
                    try {
                        FileUtils.writeStringToFile(scriptFile, groovy, "UTF-8");
                    } catch (Exception e) {
                        Log.log.warn(e.getMessage() + ". Skipping file: " + scriptFile.getAbsolutePath());
                    }
                }
            }
            xdoc.setListMapValue(RECEIVE_RADIUS_FILTER, filters);
        }
        if (nameProperties != null && isActive()) {
            nameProperties.clear();
            for (String name : RADIUSGateway.getInstance().getNameProperties().keySet()) {
                Map<String, Object> entry = new HashMap<String, Object>();
                entry.put(NAME_KEY, name);
                nameProperties.add(entry);
                Map props = RADIUSGateway.getInstance().getNameProperties().getNameProperties(name);
                ObjectProperties prop = new ObjectProperties(props);
                prop.save(props);
            }
        }
        saveAttributes();
    }

    private void putFilterDataIntoEntry(Map<String, Object> entry, RADIUSFilter radiusFilter) {
        entry.put(RADIUSFilter.ID, radiusFilter.getId());
        entry.put(RADIUSFilter.ACTIVE, String.valueOf(radiusFilter.isActive()));
        entry.put(RADIUSFilter.ORDER, String.valueOf(radiusFilter.getOrder()));
        entry.put(RADIUSFilter.INTERVAL, String.valueOf(radiusFilter.getInterval()));
        entry.put(RADIUSFilter.EVENT_EVENTID, radiusFilter.getEventEventId());
        entry.put(RADIUSFilter.RUNBOOK, radiusFilter.getRunbook());
        entry.put(RADIUSFilter.SCRIPT, radiusFilter.getScript());
    }
}

