package com.resolve.gateway.radius.client;
import java.util.Iterator;
import net.sourceforge.jradiusclient.exception.*;
import net.sourceforge.jradiusclient.util.*;

import net.sourceforge.jradiusclient.RadiusClient;
import net.sourceforge.jradiusclient.RadiusPacket;
import net.sourceforge.jradiusclient.RadiusAttribute;
import net.sourceforge.jradiusclient.RadiusAttributeValues;

//https://github.com/detiber/jradius-client/blob/master/src/net/sourceforge/jradiusclient/TestRadiusClient.java
//import net.jradius.client.auth.RadiusAuthenticator;

public class TestClient{

    private static String userName = "TTest"; //dev / restricted / tim / ACSAdmin / TTest
    private static String userPass = "Resolve1234"; //R3solve / resolvepass / resolvetim / R3solve / Resolve1234
    private static String sharedSecret = "resolve"; //None
    private static String authMethod = "PAP"; //chap
    
    private static byte[] chapChallenge = null;
            
    public static void main(String [] args)
    {
        int authport = 1812;
        int acctport = 1813;
        String host = "10.50.1.156";
        
        RadiusClient rc = null;
        try{
            System.out.println("Instantiating JRadius client...");
            rc = new RadiusClient(host, authport, acctport, sharedSecret);
        }catch(RadiusException rex){
            System.out.println(rex.getMessage());
            System.exit(4);
        }catch(InvalidParameterException ivpex){
            System.out.println("Unable to create Radius Client due to invalid parameter!");
            System.out.println(ivpex.getMessage());
            System.exit(5);
        }
       
        basicAuthenticate(rc);
        
        //advAuthenticate(rc);
    }
    private static void basicAuthenticate(final RadiusClient rc){
        try{
                      
            RadiusPacket accessRequest = new RadiusPacket(RadiusPacket.ACCESS_REQUEST);
            RadiusAttribute userNameAttribute = new RadiusAttribute(RadiusAttributeValues.USER_NAME,userName.getBytes());
            accessRequest.setAttribute(userNameAttribute);
                
            if(authMethod.equalsIgnoreCase("chap")){
                //byte[] chapChallenge = chapUtil.getNextChapChallenge(16);
                accessRequest.setAttribute(new RadiusAttribute(RadiusAttributeValues.CHAP_PASSWORD,
                            chapEncrypt(userPass, chapChallenge)));

                accessRequest.setAttribute(new RadiusAttribute(RadiusAttributeValues.CHAP_CHALLENGE,
                            chapChallenge));   
            }else{
                accessRequest.setAttribute(new RadiusAttribute(RadiusAttributeValues.USER_PASSWORD,userPass.getBytes()));
            }
            
         
                
                //TESTING to see how to get attribute
                //String userGroup = "doesnt matter";
                //RadiusAttribute groupAttribute = new RadiusAttribute(RadiusAttributeValues.LOGIN_LAT_GROUP,userGroup.getBytes());
                //accessRequest.setAttribute(groupAttribute);

                
            //RadiusAttributeValues.LOGIN_LAT_GROUP   
            System.out.println("Performing basic authentication for ...");
            System.out.println("userName: " + userName);
            System.out.println("Password: " + userPass);
            RadiusPacket accessResponse = rc.authenticate(accessRequest);
                
                
            switch(accessResponse.getPacketType()){
                    case RadiusPacket.ACCESS_ACCEPT:
                        System.out.println("User " + userName + " authenticated");
                        printAttributes(accessResponse);

                        basicAccount(rc,userName);
                        break;
                    case RadiusPacket.ACCESS_REJECT:
                        System.out.println("User " + userName + " NOT authenticated");
                        printAttributes(accessResponse);
                        break;
                    case RadiusPacket.ACCESS_CHALLENGE:
                        String reply = new String(accessResponse.getAttribute(RadiusAttributeValues.REPLY_MESSAGE).getValue());
                        System.out.println("User " + userName + " Challenged with " + reply);
                        break;
                    default:
                        System.out.println("Whoa, what kind of RadiusPacket is this " + accessResponse.getPacketType());
                        break;
                
            }
        }catch(InvalidParameterException ivpex){
            System.out.println(ivpex.getMessage());
            ivpex.printStackTrace(System.out);
        }catch(RadiusException rex){
            System.out.println(rex.getMessage());
            rex.printStackTrace(System.out);
        }
    }
   
    
    
    private static void basicAccount(final RadiusClient rc, final String userName)
            throws InvalidParameterException, RadiusException{
        RadiusPacket accountRequest = new RadiusPacket(RadiusPacket.ACCOUNTING_REQUEST);
        accountRequest.setAttribute(new RadiusAttribute(RadiusAttributeValues.USER_NAME,userName.getBytes()));
        accountRequest.setAttribute(new RadiusAttribute(RadiusAttributeValues.ACCT_STATUS_TYPE,new byte[]{0, 0, 0, 1}));
        accountRequest.setAttribute(new RadiusAttribute(RadiusAttributeValues.ACCT_SESSION_ID,"12345678".getBytes())); //("bob").getBytes()
        accountRequest.setAttribute(new RadiusAttribute(RadiusAttributeValues.SERVICE_TYPE,new byte[]{0, 0, 0, 1}));
        
        RadiusPacket accountResponse = rc.account(accountRequest);
        
        switch(accountResponse.getPacketType()){
            case RadiusPacket.ACCOUNTING_MESSAGE:
                System.out.println("User " + userName + " got ACCOUNTING_MESSAGE response");
                break;
            case RadiusPacket.ACCOUNTING_RESPONSE:
                System.out.println("User " + userName + " got ACCOUNTING_RESPONSE response");
                break;
            case RadiusPacket.ACCOUNTING_STATUS:
                System.out.println("User " + userName + " got ACCOUNTING_STATUS response");
                break;
            default:
                System.out.println("User " + userName + " got invalid response " + accountResponse.getPacketType() );
                break;
        }
        printAttributes(accountResponse);
    }
    
    /*
    private static void advAuthenticate(final RadiusClient rc){
        try{
               
            RadiusPacket accessRequest = null;
        
            if(authMethod.equalsIgnoreCase("chap")){
                accessRequest = new ChapAccessRequest(userName, userPass);
            }else{
                accessRequest = new PapAccessRequest(userName,userPass);
            }
     
            System.out.println("Performing ADVANCED authentication for ...");
            System.out.println("userName: " + userName);
            System.out.println("Password: " + userPass);
            
            RadiusPacket accessResponse = rc.authenticate(accessRequest);
                switch(accessResponse.getPacketType()){
                    case RadiusPacket.ACCESS_ACCEPT:
                        System.out.println("User " + userName + " authenticated");
                        printAttributes(accessResponse);
                        advAccount(rc,userName);
                        break;
                    case RadiusPacket.ACCESS_REJECT:
                        System.out.println("User " + userName + " NOT authenticated");
                        printAttributes(accessResponse);
                        break;
                    case RadiusPacket.ACCESS_CHALLENGE:
                        String reply = new String(accessResponse.getAttribute(RadiusAttributeValues.REPLY_MESSAGE).getValue());
                        System.out.println("User " + userName + " Challenged with " + reply);
                        break;
                    default:
                        System.out.println("Whoa, what kind of RadiusPacket is this " + accessResponse.getPacketType());
                        break;
                }
        }catch(InvalidParameterException ivpex){
            System.out.println(ivpex.getMessage());
        }catch(RadiusException rex){
            System.out.println(rex.getMessage());
        }
    }
    private static void advAccount(final RadiusClient rc,
                                final String userName)
            throws InvalidParameterException, RadiusException{
        RadiusPacket accountRequest = new AccountingRequest(userName, new byte[]{0,0,0,1}, userName);
        RadiusPacket accountResponse = rc.account(accountRequest);
        switch(accountResponse.getPacketType()){
            case RadiusPacket.ACCOUNTING_MESSAGE:
                System.out.println("User " + userName + " got ACCOUNTING_MESSAGE response");
                break;
            case RadiusPacket.ACCOUNTING_RESPONSE:
                System.out.println("User " + userName + " got ACCOUNTING_RESPONSE response");
                break;
            case RadiusPacket.ACCOUNTING_STATUS:
                System.out.println("User " + userName + " got ACCOUNTING_STATUS response");
                break;
            default:
                System.out.println("User " + userName + " got invalid response " + accountResponse.getPacketType() );
                break;
        }
        printAttributes(accountResponse);
    }*/
    
    
    private static byte[] chapEncrypt(final String plainText, final byte[] chapChallenge){
        // see RFC 2865 section 2.2
        byte chapIdentifier = '0'; //chapUtil.getNextChapIdentifier();//tim not tested this
        byte[] chapPassword = new byte[17];
        chapPassword[0] = chapIdentifier;
        System.arraycopy(ChapUtil.chapEncrypt(chapIdentifier, plainText.getBytes(),chapChallenge), 0, chapPassword, 1, 16);
        return chapPassword;
    }

    
    @SuppressWarnings("rawtypes")
    private static void printAttributes(RadiusPacket rp){
        Iterator attributes = rp.getAttributes().iterator();
        RadiusAttribute tempRa;
        System.out.println("Response Packet Attributes");
        System.out.println("\tType\tValue");
        while(attributes.hasNext()){
            tempRa = (RadiusAttribute)attributes.next();
            System.out.println("\t" + tempRa.getType() + "\t" + new String(tempRa.getValue()));
        }
    }
}