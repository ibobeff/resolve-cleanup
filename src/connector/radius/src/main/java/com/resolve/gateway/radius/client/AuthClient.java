package com.resolve.gateway.radius.client;

import java.io.UnsupportedEncodingException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;

import org.apache.tools.ant.filters.TokenFilter.StringTokenizer;

import com.resolve.util.Constants;
import com.resolve.util.Log;
import com.resolve.util.StringUtils;

import net.sourceforge.jradiusclient.RadiusAttribute;
import net.sourceforge.jradiusclient.RadiusAttributeValues;
import net.sourceforge.jradiusclient.RadiusClient;
import net.sourceforge.jradiusclient.RadiusPacket;
import net.sourceforge.jradiusclient.exception.InvalidParameterException;
import net.sourceforge.jradiusclient.exception.RadiusException;
import net.sourceforge.jradiusclient.packets.ChapAccessRequest;
import net.sourceforge.jradiusclient.util.ChapUtil;

@SuppressWarnings("unused")
public class AuthClient
{
   // public RadiusClient radiusClient;
    public Map<String,String> authResponseAttribute = new HashMap<String, String>();
    public boolean isClientInitialised = false;
    private static byte[] chapChallenge = null;
    
    public RadiusClient initRadiusClient(String host, int authport, int acctport, String sharedSecret)
    {
        RadiusClient radiusClient = null;

        try
        {
            Log.log.info("Instantiating JRadius client to host: " + host);

            radiusClient = new RadiusClient(host, authport, acctport, sharedSecret);

            this.isClientInitialised = true;
            Log.log.info("Initialised Radius Client to host: " + host);

        }
        catch (RadiusException rex)
        {
            Log.log.error(rex.getMessage());

        }
        catch (InvalidParameterException ivpex)
        {
            Log.log.error("Unable to initialise Radius Client to host " + host);
            Log.log.error(ivpex.getMessage());

        }
        return radiusClient;
    }

    public RadiusPacket authenticateUser(RadiusClient radiusClient, String authMethod, String userName, String userPass, String sharedScecret)
    {
        // authMethod ="chap";
        // userName = "resolve";
        // userPass = "Resolv1234";
        RadiusPacket accessResponse = null;
        try
        {
            // ChapUtil chapUtil = new ChapUtil();

            RadiusPacket accessRequest = new RadiusPacket(RadiusPacket.ACCESS_REQUEST);
            RadiusAttribute userNameAttribute = new RadiusAttribute(RadiusAttributeValues.USER_NAME, userName.getBytes());
            accessRequest.setAttribute(userNameAttribute);

            /*
             * Below is for the CHAP protocol if
             * (authMethod.equalsIgnoreCase("chap")) { // byte[] chapChallenge =
             * chapUtil.getNextChapChallenge(16); // accessRequest = new
             * ChapAccessRequest(userName, userPass); byte[] chapChallenge =
             * chapUtil.getNextChapChallenge(16); accessRequest.setAttribute(new
             * RadiusAttribute(RadiusAttributeValues.CHAP_PASSWORD,
             * chapEncrypt(userPass, chapChallenge)));
             * 
             * accessRequest.setAttribute(new
             * RadiusAttribute(RadiusAttributeValues.CHAP_CHALLENGE,
             * chapChallenge)); } else
             */
            if (authMethod.equalsIgnoreCase("PAP"))
            {
                accessRequest.setAttribute(new RadiusAttribute(RadiusAttributeValues.USER_PASSWORD, userPass.getBytes()));
            }
            else
            {
                Log.log.error("Authetication method " + authMethod + " is not supported. Should be PAP");
                // authResponseAttribute.put("Status",authMethod+"
                // authentication protocol is not supported. Please specify
                // PAP/CHAP.");
                return null;
            }

            Log.log.info("Performing basic authentication...");

            accessResponse = radiusClient.authenticate(accessRequest);

        }
        catch (InvalidParameterException ivpex)
        {

            Log.log.error(ivpex.getMessage());
            ivpex.printStackTrace();
            // System.out.println(ivpex.getLocalizedMessage());
            // authResponseAttribute.put("Status", ivpex.getMessage());

        }
        catch (RadiusException rex)
        {

            Log.log.error(rex.getMessage());
            // authResponseAttribute.put("status", rex.getMessage());

        }
        return accessResponse;
    }

    private static byte[] chapEncrypt(final String plainText, final byte[] chapChallenge)
    {
        // see RFC 2865 section 2.2
        byte chapIdentifier = '0'; // chapUtil.getNextChapIdentifier();//tim not
                                   // tested this
        byte[] chapPassword = new byte[17];
        chapPassword[0] = chapIdentifier;
        System.arraycopy(ChapUtil.chapEncrypt(chapIdentifier, plainText.getBytes(), chapChallenge), 0, chapPassword, 1, 16);
        return chapPassword;
    }

    @SuppressWarnings("rawtypes")
    public Map<String, String> getAuthAttributes(RadiusPacket rp, String userName)
    {

        Map<String, String> responseAttributes = new HashMap<String, String>();

        try
        {
            switch (rp.getPacketType())
            {
                case RadiusPacket.ACCESS_ACCEPT:
                    responseAttributes.put("status", "Autheticated");
                    Log.log.info("User " + userName + " Authenticated");
                    break;
                case RadiusPacket.ACCESS_REJECT:
                    Log.log.warn("User " + userName + " NOT authenticated");
                    // responseAttributes.put("status", "NOT Autheticated");
                    break;
                case RadiusPacket.ACCESS_CHALLENGE:
                    String reply;

                    reply = new String(rp.getAttribute(RadiusAttributeValues.REPLY_MESSAGE).getValue());
                    responseAttributes.put("status", " Challenged with " + reply);

                    Log.log.warn("User " + userName + " Challenged with " + reply);
                    break;
                default:
                    responseAttributes.put("status", "Packet type received in response is invalid");
                    Log.log.warn("Packet type received in response is invalid" + rp.getPacketType());
                    break;

            }
            // System.out.println();

            // Collection<? extends Object> list = rp.getAttributes();

            Iterator attributes = rp.getAttributes().iterator();
            RadiusAttribute tempRa;
            String userGroup = new String();
            String userDomain = new String();
            while (attributes.hasNext())
            {
                tempRa = (RadiusAttribute) attributes.next();
                
                if (tempRa.getType() == 1) // username attribute is 1
                {
                    responseAttributes.put("username", new String(tempRa.getValue()));

                }
                if (tempRa.getType() == 26) // cisco av pair attribute is 26
                                            // that return group
                {
                    String group = new String(tempRa.getValue());

                    // below code removes nonprintable ascii characters from the
                    // byte array
                    char[] oldChars = new char[group.length()];
                    group.getChars(0, group.length(), oldChars, 0);
                    char[] newChars = new char[group.length()];
                    int newLen = 0;
                    for (int j = 0; j < group.length(); j++)
                    {
                        char ch = oldChars[j];
                        if (ch >= ' ')
                        {
                            newChars[newLen] = ch;
                            newLen++;
                        }
                    }
                    //group value after removing nonprintable ascii chars from the byte array
                    group = new String(newChars, 0, newLen);

                    String[] groupTokens = group.split(",");
                    for (String groupName : groupTokens)
                    {
                        if (groupTokens.length == 1) //internal users in ACS just returns the group name and no domain name. e.g Test
                        {
                          
                            responseAttributes.put(Constants.MEMBEROF, group);
                        }
                        else // AD users return group value as e.g 
                        {
                            if (groupName.contains("CN="))
                            {

                                String[] groupList = groupName.split("=");

                                if (!userGroup.isEmpty())
                                {
                                    userGroup = userGroup.concat("," + groupList[1]);

                                }
                                else
                                {
                                    userGroup = userGroup.concat(new String(groupList[1]));

                                }
                                responseAttributes.put(Constants.MEMBEROF, userGroup);

                            }

                            if (groupName.contains("DC="))
                            {

                                String[] domainControllerList = groupName.split("=");

                                if (!userDomain.isEmpty())
                                {
                                    if (!domainControllerList[1].equalsIgnoreCase("com"))
                                    {
                                        userDomain = userDomain.concat("," + domainControllerList[1]);

                                    }
                                    responseAttributes.put("domain", userDomain);
                                }
                                else
                                {
                                    if (!domainControllerList[1].equalsIgnoreCase("com"))
                                    {
                                        userDomain = userDomain.concat(new String(domainControllerList[1]));
                                        responseAttributes.put("domain", userDomain);

                                    }
                                }

                            }

                        }

                    }

                }
                // 25: (Accounting) Arbitrary value that the network access
                // server includes in all accounting packets for this user if
                // supplied by the RADIUS server.
            }
        }
        catch (InvalidParameterException e)
        {
            responseAttributes.put("status", e.getMessage());
            Log.log.error(e.getMessage(), e);
        }
        catch (RadiusException e)
        {
            responseAttributes.put("status", e.getMessage());
            Log.log.error(e.getMessage(), e);
        }

        return responseAttributes;

    }

}
