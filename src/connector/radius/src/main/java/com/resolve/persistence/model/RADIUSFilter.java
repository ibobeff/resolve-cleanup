package com.resolve.persistence.model;

import javax.persistence.Entity;
import javax.persistence.Table;
import javax.persistence.UniqueConstraint;
import com.resolve.services.hibernate.vo.RADIUSFilterVO;

@Entity
@Table(name = "radius_filter", uniqueConstraints = { @UniqueConstraint(columnNames = { "u_name", "u_queue" }) })
public class RADIUSFilter extends GatewayFilter<RADIUSFilterVO> {

    private static final long serialVersionUID = 1L;

    public RADIUSFilter() {
    }

    public RADIUSFilter(RADIUSFilterVO vo) {
        applyVOToModel(vo);
    }

    public RADIUSFilterVO doGetVO() {
        RADIUSFilterVO vo = new RADIUSFilterVO();
        super.doGetBaseVO(vo);
        return vo;
    }

    @Override
    public void applyVOToModel(RADIUSFilterVO vo) {
        if (vo == null) return;
        super.applyVOToModel(vo);
    }
}

