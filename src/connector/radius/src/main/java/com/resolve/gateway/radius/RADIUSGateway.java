package com.resolve.gateway.radius;

import java.util.Map;
import java.util.List;
import java.util.ArrayList;

import com.resolve.rsbase.MainBase;
import com.resolve.gateway.BaseClusteredGateway;
import com.resolve.gateway.BaseFilter;
import com.resolve.gateway.Filter;
import com.resolve.gateway.MRADIUS;
import com.resolve.gateway.radius.client.AuthClient;
import com.resolve.util.Log;
import com.resolve.util.StringUtils;

import net.sourceforge.jradiusclient.RadiusClient;
import net.sourceforge.jradiusclient.RadiusPacket;

import com.resolve.rsremote.ConfigReceiveGateway;

public class RADIUSGateway extends BaseClusteredGateway
{

    private static volatile RADIUSGateway instance = null;

    private String queue = null;

    public static RADIUSGateway getInstance(ConfigReceiveRADIUS config)
    {
        if (instance == null)
        {
            instance = new RADIUSGateway(config);
        }
        return instance;
    }

    /**
     * This method is called once the initialization is done. If it gets called
     * before the initialization it will throw exception.
     *
     * @return
     */
    public static RADIUSGateway getInstance()
    {
        if (instance == null)
        {
            throw new RuntimeException("RADIUS Gateway is not initialized correctly..");
        }
        else
        {
            return instance;
        }
    }

    private RADIUSGateway(ConfigReceiveRADIUS config)
    {
        super(config);
    }

    @Override
    public String getLicenseCode()
    {
        return "RADIUS";
    }

    @Override
    protected void addAdditionalValues(Map<String, Object> arg0, Map<String, String> arg1)
    {
    }

    @Override
    protected String getGatewayEventType()
    {
        return "RADIUS";
    }

    @Override
    protected String getMessageHandlerName()
    {
        return MRADIUS.class.getSimpleName();
    }

    @Override
    protected Class<MRADIUS> getMessageHandlerClass()
    {
        return MRADIUS.class;
    }

    public String getQueueName()
    {
        return queue + getOrgSuffix();
    }

    @Override
    public void start()
    {
        Log.log.debug("Starting RADIUS Gateway");
        super.start();
    }

    @Override
    public void run()
    {
        if (!((ConfigReceiveGateway) configurations).isMock()) super.sendSyncRequest();
        while (running)
        {
            try
            {
                long startTime = System.currentTimeMillis();
                if (primaryDataQueue.isEmpty())
                {
                    Log.log.trace("Local Primary Event Queue for " + getGatewayEventType() + " is empty.....");
                    for (Filter filter : orderedFilters)
                    {
                        if (shouldFilterExecute(filter, startTime))
                        {
                            Log.log.trace("Filter " + filter.getId() + " executing");
                            List<Map<String, String>> results = invokeService(filter);
                            for (Map<String, String> result : results)
                            {
                                addToPrimaryDataQueue(filter, result);
                            }
                        }
                        else
                        {
                            Log.log.trace("Filter:" + filter.getId() + " is inactive or not ready to execute yet.....");
                        }
                    }
                    if (orderedFilters == null || orderedFilters.size() == 0)
                    {
                        Log.log.trace("There is not deployed filter for RADIUSGateway");
                    }
                }
                if (orderedFilters.size() == 0)
                {
                    Thread.sleep(interval);
                }
            }
            catch (Exception e)
            {
                Log.log.warn("Warning: " + e.getMessage());
                Log.alert("Instance " + MainBase.main.configId.getGuid() + " " + getLicenseCode() + "-" + getQueueName() + "-" + getInstanceType() + " Polling Failed", "Instance " + MainBase.main.configId.getGuid() + " " + getLicenseCode() + "-" + getQueueName() + "-" + getInstanceType() + " Polling Failed " + "due to " + e.getMessage());
                try
                {
                    Thread.sleep(interval);
                }
                catch (InterruptedException ie)
                {
                    Log.log.warn("sleep interrupted: " + ie.getMessage(), ie);
                }
            }
        }
    }

    /**
     * Add customized code here to get the data as List<Map<String, String>>
     * format for each filter
     * 
     * @return List<Map<String, String>>
     */
    public List<Map<String, String>> invokeService(Filter filter)
    {
        List<Map<String, String>> result = new ArrayList<Map<String, String>>();
        // Add customized code here to get data from 3rd party system based the
        // information in the filter;
        return result;
    }

    /**
     * Add customized code here to initilize the connection with 3rd party
     * system
     * 
     * @return List<Map<String, String>>
     */
    @Override
    protected void initialize()
    {

        ConfigReceiveRADIUS config = (ConfigReceiveRADIUS) configurations;
        queue = config.getQueue().toUpperCase();
        this.gatewayConfigDir = "/config/radius/";
       
         /* Map<String,String> responseAttributes =
          authenticateUser("10.50.1.156",null,1812,1813, "hemantadmin",
          "Resolve1234", "PAP","resolve");
          if(responseAttributes.size() == 0) 
          {
              System.out.println("User not autheticated"); 
          } 
          for (Map.Entry<String,
          String> entry : responseAttributes.entrySet()) 
          {
              Log.log.info(entry.getKey()+": " + entry.getValue());
              System.out.println(entry.getKey()+": " + entry.getValue()); 
          }*/
       

        
        // Add customized code here to initilize the connection with 3rd party
        // system;

    }

    public Map<String, String> authenticateUser(String primaryHost, String secondaryHost, int authPort, int acctPort, String userName, String userPass, String authMethod, String sharedScecret)
    {

        AuthClient authClient = new AuthClient();
        RadiusClient radiusClient = null;
   

        Log.log.info("Initialising radius client for primary host...");
        if ((!(primaryHost == null)) || StringUtils.isNotEmpty(primaryHost))
        {
            radiusClient = authClient.initRadiusClient(primaryHost, authPort, acctPort, sharedScecret);
        }
        else
        {
            Log.log.error("Please provide primary host address.");
         //   authClient.authResponseAttribute.put("status", "Please provide primary host address.");
            return authClient.authResponseAttribute;

        }

        if (radiusClient == null && (!(secondaryHost == null)) || StringUtils.isNotEmpty(secondaryHost))
        {

            Log.log.error("Primary host failed. Initialising radius client for secondary host...");
            // init secondary client
            radiusClient = authClient.initRadiusClient(secondaryHost, authPort, acctPort, sharedScecret);
        }
        if (radiusClient != null)
        {

            // autheticate a user
            RadiusPacket authResponsePacket = authClient.authenticateUser(radiusClient, authMethod, userName, userPass, sharedScecret);
            // get authetication response attributes
            if (authResponsePacket != null)
            {
                authClient.authResponseAttribute = authClient.getAuthAttributes(authResponsePacket, userName);
            }
            else if (authClient.authResponseAttribute.size() == 0)
            {
                Log.log.error("Response packet is not received from authentication request..");
                // authClient.authResponseAttribute.put("status","Response
                // packet is not received from authentication request");
            }
        }

        else
        {

            Log.log.error("Failed to initialise radius client for primary and secondary hosts.");

            // authClient.authResponseAttribute.put("status","Failed to
            // initialise radius client for primary and secondary hosts.");
            return authClient.authResponseAttribute;
        }
        return authClient.authResponseAttribute;
    }

    /**
     * Add customized code here to do when stop the gateway
     */
    @Override
    public void stop()
    {
        Log.log.warn("Stopping RADIUS gateway");
        // Add customized code here to stop the connection with 3rd party
        // system;
        super.stop();
    }

    @Override
    public Filter getFilter(Map<String, Object> params)
    {
        return new RADIUSFilter((String) params.get(BaseFilter.ID), (String) params.get(BaseFilter.ACTIVE), (String) params.get(BaseFilter.ORDER), (String) params.get(BaseFilter.INTERVAL), (String) params.get(BaseFilter.EVENT_EVENTID), (String) params.get(BaseFilter.RUNBOOK), (String) params.get(BaseFilter.SCRIPT));
    }
}
