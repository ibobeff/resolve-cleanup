package com.resolve.rscontrol;

import com.resolve.persistence.model.ArcSightFilter;

public class MArcSight extends MGateway {

    private static final String MODEL_NAME = ArcSightFilter.class.getSimpleName();

    protected String getModelName() {
        return MODEL_NAME;
    }
}

