package com.resolve.gateway.arcsight;

import com.resolve.gateway.BaseFilter;
import com.resolve.util.Log;

public class ArcSightFilter extends BaseFilter {

    public static final String URI = "URI";

    public static final String START = "START";

    public static final String END = "END";

    public static final String REPORT = "REPORT";

    private String uri;

    private String start;

    private String end;

    private String report;

    public ArcSightFilter(String id, String active, String order, String interval, String eventEventId, String runbook, String script, String uri, String start, String end, String report) {
        super(id, active, order, interval, eventEventId, runbook, script);
        this.uri = uri;
        this.start = start;
        this.end = end;
        this.report = report;
    }

    public String getUri() {
        return this.uri;
    }

    public void setUri(String uri) {
        this.uri = uri;
    }

    public String getStart() {
        return this.start;
    }

    public void setStart(String start) {
        this.start = start;
    }

    public String getEnd() {
        return this.end;
    }

    public void setEnd(String end) {
        this.end = end;
    }

    public String getReport() {
        return this.report;
    }

    public void setReport(String report) {
        this.report = report;
    }
}

