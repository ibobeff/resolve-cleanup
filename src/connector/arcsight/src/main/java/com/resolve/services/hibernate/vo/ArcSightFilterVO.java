package com.resolve.services.hibernate.vo;

import com.resolve.util.MappingAnnotation;

public class ArcSightFilterVO extends GatewayFilterVO {

    private static final long serialVersionUID = 1L;

    public ArcSightFilterVO() {
    }

    private String UUri;

    private String UStart;

    private String UEnd;

    private String UReport;

    @MappingAnnotation(columnName = "URI")
    public String getUUri() {
        return this.UUri;
    }

    public void setUUri(String uUri) {
        this.UUri = uUri;
    }

    @MappingAnnotation(columnName = "START")
    public String getUStart() {
        return this.UStart;
    }

    public void setUStart(String uStart) {
        this.UStart = uStart;
    }

    @MappingAnnotation(columnName = "END")
    public String getUEnd() {
        return this.UEnd;
    }

    public void setUEnd(String uEnd) {
        this.UEnd = uEnd;
    }

    @MappingAnnotation(columnName = "REPORT")
    public String getUReport() {
        return this.UReport;
    }

    public void setUReport(String uReport) {
        this.UReport = uReport;
    }
    
    @Override
    public int hashCode()
    {
        final int prime = 31;
        int result = super.hashCode();
        result = prime * result + ((UEnd == null) ? 0 : UEnd.hashCode());
        result = prime * result + ((UStart == null) ? 0 : UStart.hashCode());
        result = prime * result + ((UReport == null) ? 0 : UReport.hashCode());
        result = prime * result + ((UUri == null) ? 0 : UUri.hashCode());
        
        return result;
    }
}

