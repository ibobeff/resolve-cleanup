package com.resolve.gateway.arcsight;

import java.io.BufferedReader;
import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.io.UnsupportedEncodingException;
import java.net.HttpURLConnection;
import java.net.URL;
import java.net.URLConnection;
import java.net.URLEncoder;
import java.util.HashMap;
import java.util.Map;
import java.util.logging.Logger;

import javax.net.ssl.HostnameVerifier;
import javax.net.ssl.HttpsURLConnection;
import javax.net.ssl.SSLContext;
import javax.net.ssl.SSLSession;
import javax.net.ssl.TrustManager;
import javax.net.ssl.X509TrustManager;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;

import org.w3c.dom.Document;
import org.w3c.dom.Node;

/**
 * 
 * @author Yanlin Wang (yanlin.wang@hp.com)
 * 
 */

public class ArcSightRestClient {

	/**
	 * generate ESM REST service URL based on parameters
	 * 
	 * @param host
	 *            - ESM host
	 * @param port
	 *            -
	 * @param moduleName
	 *            - ESM service module (eg. manager-service)
	 * @param serviceName
	 *            - ESM service name (eg. CaseService)
	 * @param methodName
	 *            - service method name (eg. getCaseEventIDs)
	 * @param params
	 *            - REST service parameters
	 * @return url string
	 */
	@SuppressWarnings("deprecation")
	public static String getRestUrl(String host, int port, String moduleName,
			String serviceName, String methodName,
			HashMap<String, String> params) {
		String url = String.format("https://%s:%d/www/%s/rest/%s/%s", host,
				port, moduleName, serviceName, methodName);
		boolean questionMark = false;
		for (Map.Entry<String, String> param : params.entrySet()) {
			if (!questionMark) {
				url = url + "?" + param.getKey() + "="
						+ new String(URLEncoder.encode(param.getValue()));
				questionMark = true;
			} else {
				url = url + "&" + param.getKey() + "="
						+ new String(URLEncoder.encode(param.getValue()));
			}
		}
		return url;
	}
	
	public static String getRestParams(HashMap<String, String> params) throws UnsupportedEncodingException {
		String str = null;
		for (Map.Entry<String, String> param : params.entrySet()) {
			String keystr = URLEncoder.encode(param.getKey(), "UTF-8");
			String paramstr = URLEncoder.encode(param.getValue(), "UTF-8");
//			keystr = param.getKey();
//			paramstr = param.getValue();
			if (str == null) {
				str = keystr + "=" + paramstr;
			} else {
				str = "&" + keystr + "=" + paramstr;
			}
		}
//		str = str.replace("+", "%20");
		return str;
	}

	/**
	 * Query a REST service and return the xml data
	 * 
	 * @param moduleName
	 *            - ESM service module (eg. manager-service)
	 * @param serviceName
	 *            - ESM service name (eg. CaseService)
	 * @param methodName
	 *            - service method name (eg. getCaseEventIDs)
	 * @param params
	 *            - REST service parameters
	 * @return REST response xml
	 * @throws Exception
	 */
	public static String getRestXml(String host, int port, String moduleName,
			String serviceName, String methodName,
			HashMap<String, String> params) throws Exception {
		String urlstr = getRestUrl(host, port, moduleName, serviceName,
				methodName, params);
		URL url = new URL(urlstr);
		URLConnection connection = url.openConnection();
		
		BufferedReader in = new BufferedReader(new InputStreamReader(
				connection.getInputStream()));
		StringBuffer sb = new StringBuffer();
		String line;
		while ((line = in.readLine()) != null) {
			sb.append(line);
		}
		in.close();
		return sb.toString();
	}
	
	public static String postRestXml(String host, int port, String moduleName,
			String serviceName, String methodName,
			HashMap<String, String> params) throws IOException {
		String urlstr = getRestUrl(host, port, moduleName, serviceName,
				methodName, new HashMap<String, String>());
		URL url = new URL(urlstr);
		HttpURLConnection connection = (HttpURLConnection)url.openConnection();
		connection.setDoOutput(true);
		connection.setDoInput(true);

		connection.setRequestProperty("Content-Type", "application/xml");
		
		OutputStream outstream = connection.getOutputStream();
		OutputStreamWriter outwriter = new OutputStreamWriter(connection.getOutputStream());
		String outparams = getRestParams(params);
		outwriter.write(outparams);
		outwriter.flush();
		outwriter.close();
		outstream.close();
		
		int status = connection.getResponseCode();
		
		BufferedReader inreader = new BufferedReader(new InputStreamReader(
				connection.getInputStream()));
		StringBuffer sb = new StringBuffer();
		String line;
		while ((line = inreader.readLine()) != null) {
			sb.append(line);
		}
		inreader.close();
		return sb.toString();
	}
	
	public static void trustAll() {
		// Create a trust manager that does not validate certificate chains
		TrustManager[] trustAllCerts = new TrustManager[] { new X509TrustManager() {
			public java.security.cert.X509Certificate[] getAcceptedIssuers() {
				return null;
			}

			public void checkClientTrusted(
					java.security.cert.X509Certificate[] certs, String authType) {
			}

			public void checkServerTrusted(
					java.security.cert.X509Certificate[] certs, String authType) {
			}
		} };

		// Install the all-trusting trust manager
		try {
			SSLContext sc = SSLContext.getInstance("SSL");
			sc.init(null, trustAllCerts, new java.security.SecureRandom());
			HttpsURLConnection
					.setDefaultSSLSocketFactory(sc.getSocketFactory());
			HttpsURLConnection
					.setDefaultHostnameVerifier(new HostnameVerifier() {
						public boolean verify(String string, SSLSession ssls) {
							return true;
						}
					});
		} catch (Exception ex) {
			//logger.debug("trustAll", ex);
		}
	}

	public static String login(String hostname, int port, String username,
			String password) throws Exception {
		trustAll();

		// Login through REST
		//logger.debug("Start REST login");
		HashMap<String, String> params = new HashMap<String, String>();
		params.put("login", username);
		params.put("password", password);
		String loginxml = getRestXml(hostname, port, "core-service",
				"LoginService", "login", params);
		//logger.debug("Received Login XML = " + loginxml);

		// Parse the XML for token
		DocumentBuilderFactory docfactory = DocumentBuilderFactory
				.newInstance();
		DocumentBuilder docbuilder = docfactory.newDocumentBuilder();
		Document doc = docbuilder.parse(new ByteArrayInputStream(loginxml
				.getBytes()));
		Node node = doc.getDocumentElement().getChildNodes().item(0);
		String token = node.getTextContent();
		if (token == null) {
			//logger.error("Failed to login through REST");
			return null;
		}
		//logger.debug("Successful login");
		return token;
	}

}
