package com.resolve.gateway.arcsight;

import java.io.ByteArrayInputStream;
import java.util.HashMap;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;

import org.w3c.dom.Document;
import org.w3c.dom.Node;

public class TestCaseRest {
	public static void main(String[] args) throws Exception {
		ArcSightRestClient.trustAll();
		
		// Login through REST
		HashMap<String, String> params = new HashMap<String, String>();
		params.put("login", "admin");
		params.put("password", "password");
		String xml = ArcSightRestClient.getRestXml("10.4.18.137", 8443,
				"core-service", "LoginService", "login", params);

		// Parse the XML for token
		DocumentBuilderFactory f = DocumentBuilderFactory.newInstance();
		DocumentBuilder b = f.newDocumentBuilder();
		Document d = b.parse(new ByteArrayInputStream(xml.getBytes()));
		Node node = d.getDocumentElement().getChildNodes().item(0);
		String token = node.getTextContent();
		if (token == null) {
			System.out.println("Failed to login");
			System.out.println(xml);
			return;
		}

		// Search case resource
		params = new HashMap<String, String>();
		params.put("authToken", token);
		params.put("caseID", "7B7wtYzEBABCAGyLqkEdj+g==");
		String resXml = ArcSightRestClient.getRestXml("10.4.18.137", 8443,
				"manager-service", "CaseService", "getCaseEventIDs", params);

		params = new HashMap<String, String>();
		params.put("authToken", token);
		params.put("resourceId", "7B7wtYzEBABCAGyLqkEdj+g==");
		String caseXml = ArcSightRestClient.getRestXml("10.4.18.137", 8443,
		//String caseXml = ArcSightRestClient.postRestXml("10.4.18.137", 8443,
				"manager-service", "CaseService", "getResourceById", params);
		
		int startIndex, endIndex;
		
		// extract xml namespace headers
		startIndex = caseXml.indexOf("xmlns");
		endIndex = caseXml.indexOf("><ns3:return>");
		String xmlNameSpace = caseXml.substring(startIndex, endIndex);
		
		// extract case body
		startIndex = caseXml.indexOf("<ns3:return>") + 12;
		endIndex = caseXml.indexOf("</ns3:return>");
		String caseBodyXml = caseXml.substring(startIndex, endIndex);
		// remove <referenceString>
		startIndex = caseBodyXml.indexOf("<referenceString>");
		endIndex = caseBodyXml.indexOf("</referenceString>") + 18;
		caseBodyXml = caseBodyXml.substring(0, startIndex) + caseBodyXml.substring(endIndex);
		
		// toggle stage of QUEUED and CLOSED
		String oldStage = "<stage>QUEUED</stage>";
		String newStage = "<stage>CLOSED</stage>";
		if (caseBodyXml.indexOf(oldStage) >= 0) {
			caseBodyXml = caseBodyXml.replace(oldStage, newStage);
		} else {
			caseBodyXml = caseBodyXml.replace(newStage, oldStage);
		}
		
		//caseBodyXml = "";
		String caseUpdateXml = "<?xml version=\"1.0\" encoding=\"UTF-8\" standalone=\"yes\"?>";
		caseUpdateXml += "<ns3:update " + xmlNameSpace + ">";
		caseUpdateXml += "<ns3:authToken>" + token + "</ns3:authToken>";
		caseUpdateXml += "<ns3:resource>" + caseBodyXml + "</ns3:resource>";
		caseUpdateXml += "</ns3:update>";
		
		params = new HashMap<String, String>();
//		params.put("authToken", token);
//		params.put("resource", caseXml);
		params.put("value", caseUpdateXml);
		String updateXml = ArcSightRestClient.postRestXml("10.4.18.137", 8443,
				"manager-service", "CaseService", "update", params);

		String updatedXml = caseXml.replace("UNCLASSIFIED", "");
	}
}
