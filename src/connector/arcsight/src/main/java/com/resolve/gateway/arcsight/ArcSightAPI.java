package com.resolve.gateway.arcsight;

import java.io.ByteArrayInputStream;
import java.io.StringWriter;
import java.nio.charset.StandardCharsets;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.transform.OutputKeys;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;

import org.apache.commons.codec.binary.Hex;
import org.w3c.dom.Document;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

import com.resolve.gateway.AbstractGatewayAPI;
import com.resolve.util.Log;
import com.resolve.util.StringUtils;
import com.resolve.util.restclient.RestCaller;

import net.sf.json.JSONArray;
import net.sf.json.JSONObject;

public class ArcSightAPI extends AbstractGatewayAPI
{
    private static final String lineSeparator = System.getProperty("line.separator");
    
    private static String NAMESPACE = "http://ws.v1.service.resource.manager.product.arcsight.com/caseService/";
    
    private static String HOSTNAME = "";
    private static String PORT = "";
    private static String CORE_SERVICE_URI = "/www/core-service";
    private static String MANAGER_SERVICE_URI = "/www/manager-service";
    private static String USERNAME = "";
    private static String P_ASSWORD = "";
    
    private static String SSL = "trust-all";
    private static String RESP_TYPE = "application/json";
    
    private static String baseUrl = "";
    private static String baseCoreUrl = "";
    private static String baseManagerUrl = "";
    
    private static String userId = "";
    private static boolean selfSigned = false;
    private static boolean trustAll = true;
    
    private static volatile String token = null;
    
    private static ArcSightGateway instance = ArcSightGateway.getInstance();
    
    static {
        Map<String, Object> properties = instance.loadSystemProperties();
        
        HOSTNAME = (String)properties.get("HOST");
        PORT = (String)properties.get("PORT");
        USERNAME = (String)properties.get("USERNAME");
        P_ASSWORD = (String)properties.get("PASSWORD");
        SSL = (String)properties.get("SSL");
        
        StringBuilder sb = new StringBuilder();

        if(StringUtils.isNotEmpty(SSL)) {
            if(SSL.equalsIgnoreCase("self-signed")) {
                trustAll = false;
                selfSigned = true;
            }
            
            else if(SSL.equalsIgnoreCase("trust-all")) {
                trustAll = true;
                selfSigned = false;
            }
            
            else {
                trustAll = false;
                selfSigned = false;
            }
        }
        
        sb.append("https://").append(HOSTNAME);
        
        if(StringUtils.isNotBlank(PORT))
            sb.append(":").append(PORT);
        
        baseUrl = sb.toString();
        baseCoreUrl = baseUrl + CORE_SERVICE_URI;
        baseManagerUrl = baseUrl + MANAGER_SERVICE_URI;
        
        try {
            login();
            Log.log.debug("token = " + token);
            userId = getUserId(USERNAME);
        } catch(Exception e) {
            Log.log.error(e.getMessage(), e);
        }
    }
    
    private static String login() throws Exception {
        
        if(StringUtils.isBlank(USERNAME))
            throw new Exception("Username is not available.");
        
        Map<String, String> params = new HashMap<String, String>();
        params.put("login", USERNAME);
        params.put("password", P_ASSWORD);
        params.put("alt", "json");
        
        try {
            RestCaller client = new RestCaller(baseCoreUrl, "", "", false, true, selfSigned, trustAll);
            String resp = client.callGet("/rest/LoginService/login", params, null, null);
            Log.log.debug(resp);
            if(StringUtils.isNotBlank(resp)) {
                JSONObject json = JSONObject.fromObject(resp);
                if(json != null) {
                    JSONObject response = json.getJSONObject("log.loginResponse");
                    if(response != null)
                        token = response.getString("log.return");
                }
                Log.log.debug("token = " + token);
            }
        } catch(Exception e) {
            Log.log.error(e.getMessage(), e);
            throw e;
        }
        
        return token;
    }
    
    private static void logout() throws Exception {
        
        if(StringUtils.isNotBlank(token)) {
            Map<String, String> params = new HashMap<String, String>();
            params.put("authToken", token);
            
            try {
                RestCaller client = new RestCaller(baseCoreUrl, "", "", false, true, selfSigned, trustAll);
                String resp = client.callGet("/rest/LoginService/logout", params, null, null);
                Log.log.debug(resp);
                if(resp != null) {
                    try {
                        int code = (new Integer(resp)).intValue();
                        if(code > 200 && code != 204)
                            throw new Exception("Failed to log out with response code: " + resp);
                    } catch(Exception ee) {}
                }
            } catch(Exception e) {
                Log.log.error(e.getMessage(), e);
                throw e;
            }
        }
        
        else
            throw new Exception("token is not available.");
    }
    
    /**
     * Look up report id given report name
     * @param reportName
     * @return report id
     * @throws Exception
     */
    private static String getReportId(String reportName) throws Exception {
        
        if(StringUtils.isBlank(reportName))
            throw new Exception("Report name is not available.");
        
        String reportId = "";
        String resp = "";
        
        Map<String, String> params = new HashMap<String, String>();
        params.put("name", reportName);
        params.put("authToken", token);
        params.put("alt", "json");
        
        RestCaller client = new RestCaller(baseManagerUrl, "", "", false, true, selfSigned, trustAll);
        
        try {          
            resp = client.callGet("/rest/ReportService/getResourceByName", params, null, null);
        } catch(Exception e) {
            try {
                login();
                params.put("authToken", token);
                resp = client.callGet("/rest/ReportService/getResourceByName", params, null, null);
            } catch(Exception ee) {
                Log.log.error(ee.getMessage(), ee);
                throw ee;
            }
        }
        Log.log.debug(resp);
        if(StringUtils.isNotEmpty(resp)) {
            JSONObject json = JSONObject.fromObject(resp);
            if(json != null) {
                JSONObject response = json.getJSONObject("rep.getResourceByNameResponse");
                if(response != null) {
                    JSONObject report = response.getJSONObject("rep.return");
                    if(report != null) {
                        reportId = report.getString("resourceid");
                    }
                }
            }
            Log.log.debug("reportId = " + reportId);
        }
        
        return reportId;
    }
    
    /**
     * Get a list of events from the report by report name
     * This method will run the report, download the report by the file id, parse the CSV file and extract a list of events.
     * @param name: Required - an existing report name configured on ArcSight Console
     * @return a list of events
     * @throws Exception
     */
    static List<String> retrieveEventsFromReport(String name) throws Exception {
        
        if(StringUtils.isBlank(name))
            throw new Exception("Report name is not available.");
        
        List<String> events = new ArrayList<String>();
        
        try {
            String reportId = getReportId(name);
            if(StringUtils.isBlank(reportId))
                throw new Exception("Report id not found for report: " + name);
            Log.log.debug("reportId = " + reportId);
            String report = getReport(reportId);
            if(StringUtils.isBlank(report))
                throw new Exception("Report not found for report id: " + reportId);
            Log.log.debug("report = " + report);
            String fileId = getFileId(report);
            if(StringUtils.isBlank(fileId))
                throw new Exception("fileId not found for file id: " + fileId);
            Log.log.debug("fileId = " + fileId);
            events = loadEvents(fileId);
            Log.log.debug("number of events = " + (events.size()-1));
        } catch(Exception e) {
            Log.log.error(e.getMessage(), e);
            throw e;
        }
        
        return events;
    }
    
    /**
     * Look up user id given a username.
     * @param username: Required - a valid username
     * @return a user id in string
     * @throws Exception
     */
    public static String getUserId(String username) throws Exception {
        
        if(StringUtils.isBlank(username))
            throw new Exception("User name is not available.");
        
        String userId = "";
        String resp = "";
        
        Map<String, String> params = new HashMap<String, String>();
        params.put("name", username);
        params.put("authToken", token);
        params.put("alt", "json");
        
        RestCaller client = new RestCaller(baseManagerUrl, "", "", false, true, selfSigned, trustAll);
        
        try {          
            resp = client.callGet("/rest/UserResourceService/getResourceByName", params, null, null);
        } catch(Exception e) {
            try {
                login();
                params.put("authToken", token);
                resp = client.callGet("/rest/UserResourceService/getResourceByName", params, null, null);
            } catch(Exception ee) {
                Log.log.error(ee.getMessage(), ee);
                throw ee;
            }
        }
        Log.log.debug(resp);
        if(StringUtils.isNotEmpty(resp)) {
            JSONObject json = JSONObject.fromObject(resp);
            if(json != null) {
                JSONObject response = json.getJSONObject("use.getResourceByNameResponse");
                if(response != null) {
                    JSONObject report = response.getJSONObject("use.return");
                    if(report != null) {
                        userId = report.getString("resourceid");
                    }
                }
            }
            Log.log.debug("userId = " + userId);
        }
        
        return userId;
    }
    
    private static String getCaseByName(String caseName) throws Exception {
        
        if(StringUtils.isBlank(caseName))
            throw new Exception("Case name is not available.");

        String resp = null;
        
        Map<String, String> params = new HashMap<String, String>();
        params.put("name", caseName);
        params.put("authToken", token);
        params.put("alt", "json");
        
        RestCaller client = new RestCaller(baseManagerUrl, "", "", false, true, selfSigned, trustAll);
        
        try {          
            resp = client.callGet("/rest/CaseService/getResourceByName", params, null, null);
        } catch(Exception e) {
            try {
                login();
                params.put("authToken", token);
                resp = client.callGet("/rest/CaseService/getResourceByName", params, null, null);
            } catch(Exception ee) {
                Log.log.error(ee.getMessage(), ee);
                throw ee;
            }
        }
        Log.log.debug(resp);
        return resp;
    }
    
    /**
     * This method is light-weighted that contains event ids only
     * @param caseId: Required - a valid case id
     * @return a list of event ids
     * @throws Exception
     */
    private static List<String> getCaseEventIDsByCaseId(String caseId) throws Exception {
    
        if(StringUtils.isBlank(caseId))
            throw new Exception("Case id is not available.");
        
        List<String> eventIDList = new ArrayList<String>();

        String resp = null;
        
        Map<String, String> params = new HashMap<String, String>();
        params.put("caseId", caseId);
        params.put("authToken", token);
        params.put("alt", "json");
        
        RestCaller client = new RestCaller(baseManagerUrl, "", "", false, true, selfSigned, trustAll);
        
        try {          
            resp = client.callGet("/rest/CaseService/getCaseEventIDs", params, null, null);
        } catch(Exception e) {
            try {
                login();
                params.put("authToken", token);
                resp = client.callGet("/rest/CaseService/getCaseEventIDs", params, null, null);
            } catch(Exception ee) {
                Log.log.error(ee.getMessage(), ee);
                throw ee;
            }
        }
        Log.log.debug(resp);
        if(resp != null ) {
            JSONObject json = JSONObject.fromObject(resp);
            if(json != null) {
                String result = json.getString("cas.getCaseEventIDsResponse");
                if(result != null) {
                    JSONObject resultJson = JSONObject.fromObject(result);
                    try {
                        JSONArray events = resultJson.getJSONArray("cas.return");
                        
                        for(int i=0; i<events.size(); i++) {
                            Integer eventId = (Integer)events.get(i);
                            eventIDList.add(eventId.toString());
                            Log.log.debug(eventId);
                        }
                    } catch(Exception e) {
                        Integer eventId = (Integer)resultJson.get("cas.return");
                        eventIDList.add(eventId.toString());
                        Log.log.debug(eventId);
                    }
                }
            }
        }
        
        return eventIDList;
    }
    
    private static String getCaseIdByCaseName(String name) throws Exception {
        
        if(StringUtils.isBlank(name))
            throw new Exception("Case name is not available.");
        
        String caseId = null;
        String resp = getCaseByName(name);
        
        if(StringUtils.isNotEmpty(resp)) {
            JSONObject json = JSONObject.fromObject(resp);
            if(json != null) {
                JSONObject response = json.getJSONObject("cas.getResourceByNameResponse");
                if(response != null) {
                    JSONObject report = response.getJSONObject("cas.return");
                    if(report != null)
                        caseId = report.getString("resourceid");
                }
            }
            Log.log.debug("caseId = " + caseId);
        }
        
        return caseId;
    }
    
    private static List<String> getCaseEventIDsByCaseName(String name) throws Exception {
        
        if(StringUtils.isBlank(name))
            throw new Exception("Case name is not available.");
        
        String events = null;
        List<String> eventIDList = new ArrayList<String>();
        String resp = getCaseByName(name);
        
        if(StringUtils.isNotEmpty(resp)) {
            JSONObject json = JSONObject.fromObject(resp);
            if(json != null) {
                JSONObject response = json.getJSONObject("cas.getResourceByNameResponse");
                if(response != null) {
                    JSONObject report = response.getJSONObject("cas.return");
                    if(report != null)
                        if(report != null)
                            events = report.getString("eventIDs");
                }
            }
            Log.log.debug("events = " + events);
        }
        
        if(events != null ) {
            String[] eventIDs = events.split(",");
            for(int i=0; i<eventIDs.length; i++)
                eventIDList.add(eventIDs[i]);
        }
        
        return eventIDList;
    }
    
    private static String getReport(String reportId) throws Exception {
        
        if(StringUtils.isBlank(reportId))
            throw new Exception("Report id is not available.");
        
        String resp = "";
        
        Map<String, String> params = new HashMap<String, String>();
        params.put("reportId", reportId);
        params.put("reportType", "Manual");
        params.put("authToken", token);
        params.put("alt", "json");
        
        RestCaller client = new RestCaller(baseManagerUrl, "", "", false, true, selfSigned, trustAll);
        
        try {          
            resp = client.callGet("/rest/ArchiveReportService/initDefaultArchiveReportDownloadById", params, null, null);
        } catch(Exception e) {
            try {
                login();
                params.put("authToken", token);
                resp = client.callGet("/rest/ArchiveReportService/initDefaultArchiveReportDownloadById", params, null, null);
            } catch(Exception ee) {
                Log.log.error(ee.getMessage(), ee);
                throw ee;
            }
        }
        Log.log.debug(resp);
        return resp;
    }
    
    /**
     * Get event summary from a list of Events returned in CSV file with the fields defined in the Query as keys and value in a map
     * @param events: Required - a list of string in CSV format
     * @return a list of event summaries
     * @throws Exception
     */
    static List<Map<String, String>> getEventSummary(List<String> events) throws Exception {
        
        if(events == null || events.size() < 2)
            throw new Exception("No event is available.");

        List<Map<String, String>> summary = new ArrayList<Map<String, String>>();
        
        String title = events.get(0);
        String[] keys = title.split(",");
        
        int length = keys.length;
        if(length == 0)
            return summary;

        for(int i=1; i<events.size(); i++) {
            String event = events.get(i);
            String[] fields = event.split(",");
            
            if(fields.length == 0)
                continue;
            
            if(fields.length != length)
                continue;
            
            Map<String, String> one = new HashMap<String, String>();
            
            for(int j=0; j<length; j++)
                one.put(keys[j], fields[j]);

            summary.add(one);
        }
        
        return summary;
    }
    
    private static List<String> getEventIds(List<String> events) throws Exception {
        
        if(events == null || events.size() < 2)
            throw new Exception("No event is available.");

        List<String> eventIds = new ArrayList<String>();
        
        int eventIdIndex = getFieldIndex(events.get(0), "Event ID");

        for(int i=1; i<events.size(); i++) {
            String event = events.get(i);
            String[] fields = event.split(",");
            
            if(eventIdIndex > fields.length-1)
                continue;
            
            String eventId = fields[eventIdIndex];
            if(eventId != null)
                eventIds.add(eventId);
        }
        
        return eventIds;
    }
    
    /**
     * Retrieve single event detail per event id. This method is called when getSecurityEvents() fails for multiple events. 
     * Use a loop to call this method for multiple events and pick the successful ones.
     * @param eventId: Required - a list of valid event ids
     * @param startMillis: Optional - system millisecond, default is "-1"
     * @param endMillis: Optional - system millisecond, default is "-1"
     * @param accept: Optional - default is "applicaton/json"
     * @return A JSON or XML string of an array of event details containing one event detail
     * @throws Exception
     */
    public static String getSecurityEvent(String eventId, String startMillis, String endMillis, String accept) throws Exception {
        
        List<String> eventIds = new ArrayList<String>();
        eventIds.add(eventId);
        
        return getSecurityEvents(eventIds, startMillis, endMillis, accept);
    }
    
    /**
     * Retrieve the event details given event ids.
     * @param eventIds: Required - a list of valid event ids
     * @param startMillis: Optional - system millisecond, default is "-1"
     * @param endMillis: Optional - system millisecond, default is "-1"
     * @param accept: Optional - default is "applicaton/json"
     * @return A JSON or XML string of an array of event details
     * @throws Exception
     */
    public static String getSecurityEvents(List<String> eventIds, String startMillis, String endMillis, String accept) throws Exception {
        
        if(eventIds == null || eventIds.size() == 0)
            throw new Exception("No event id is available.");
        
        if(StringUtils.isBlank(startMillis))
            startMillis = "-1";
        
        if(StringUtils.isBlank(accept))
            accept = RESP_TYPE;
        
        String resp = "";
        
        JSONObject query = new JSONObject();
        query.put("sev.authToken", token);
        
        JSONArray idArray = new JSONArray();
        for(String eventId:eventIds)
            idArray.add(eventId);
        
        query.put("sev.ids", idArray);
        
        query.put("sev.startMillis", startMillis);
        query.put("sev.endMillis", endMillis);
        
        RestCaller client = new RestCaller(baseManagerUrl, "", "", false, true, selfSigned, trustAll);
        try {
            JSONObject queryRequest = new JSONObject();
            queryRequest.put("sev.getSecurityEvents", query);
            
            String body = queryRequest.toString();
            Log.log.debug(body);
            resp = client.callPost("/rest/SecurityEventService/getSecurityEvents", null, body, null, RESP_TYPE, accept);
            Log.log.debug(resp);
        } catch(Exception e) {
            try {
                login();

                query.put("sev.authToken", token);
                JSONObject queryRequest = new JSONObject();
                queryRequest.put("sev.getSecurityEvents", query);
                
                String body = queryRequest.toString();
                Log.log.debug(body);
                resp = client.callPost("/rest/SecurityEventService/getSecurityEvents", null, body, null, RESP_TYPE, accept);
            } catch(Exception ee) {
                Log.log.error(ee.getMessage(), ee);
                throw ee;
            }
        }
        
        return resp;
    }

    /**
     * Extract a list of event name/event id map from the event details
     * @param securityEventResponse: Required - a string contains an array of event details
     * @return a list of event name/id map
     * @throws Exception
     */
    private static Map<String, String> getEventIds(String securityEventResponse) throws Exception {
        
        if(StringUtils.isBlank(securityEventResponse))
            throw new Exception("Security Events Response is not available.");
        
        Map<String, String> eventIds = new HashMap<String, String>();

        JSONObject json = JSONObject.fromObject(securityEventResponse);
        
        if(json != null) {
            JSONObject response = json.getJSONObject("sev.getSecurityEventsResponse");
            if(response != null) {
                JSONArray events  = response.getJSONArray("sev.return");
                for(int i=0; i<events.size(); i++) {
                    Map<String, Object> event = (Map<String, Object>)events.get(i);
                    String eventName = event.get("name").toString();
                    String eventId = event.get("eventId").toString();
                        if(StringUtils.isNotEmpty(eventName) && StringUtils.isNotEmpty(eventId))
                            eventIds.put(eventName, eventId);
                        Log.log.debug("eventName = " + eventName);
                        Log.log.debug("eventId = " + eventId);
                }
            }
        }

        return eventIds;
    }

    /**
     * Update the status of the event with a system-defined stageId
     * @param eventId: Required - a valid event id
     * @param stageId: Required - a valid system-defined stage id
     * @param comment: Optional - a string of user commnent
     * @return A JSON string of the current event payload
     * @throws Exception
     */
    public static String updateEventStage(String eventId, String stageId, String comment) throws Exception {
        
        if(StringUtils.isBlank(eventId))
            throw new Exception("Event id is not available.");
        
        if(StringUtils.isBlank(stageId))
            throw new Exception("Stage id is not available.");
        
        if(StringUtils.isBlank(userId))
            throw new Exception("User id is not available.");

        String resp = "";
        
        JSONObject query = new JSONObject();
        query.put("sev.authToken", token);
        query.put("sev.eventId", eventId);
        query.put("sev.stageId", stageId);
        query.put("sev.userId", userId);
        if(StringUtils.isNotEmpty(comment))
            query.put("sev.comment", comment);

        RestCaller client = new RestCaller(baseManagerUrl, "", "", false, true, selfSigned, trustAll);
        
        try {
            JSONObject queryRequest = new JSONObject();
            queryRequest.put("sev.setAnnotationStage", query);
            
            String body = queryRequest.toString();
            Log.log.debug(body);
            resp = client.callPost("/rest/SecurityEventService/setAnnotationStage", null, body, null, RESP_TYPE, RESP_TYPE);
            Log.log.debug(resp);
        } catch(Exception e) {
            try {
                login();
                
                query.remove("sev.authToken");
                query.put("sev.authToken", token);
                JSONObject queryRequest = new JSONObject();
                queryRequest.put("sev.getSecurityEvents", query);
                
                String body = queryRequest.toString();
                Log.log.debug(body);
                resp = client.callPost("/rest/SecurityEventService/setAnnotationStage", null, body, null, RESP_TYPE, RESP_TYPE);
            } catch(Exception ee) {
                Log.log.error(ee.getMessage(), ee);
                throw ee;
            }
        }
        
        return resp;
    }
    
    /**
     * Retrieve the entire case xml by caseId.
     * @param caseId: Required - a valid case id
     * @return a XML string of case payload
     * @throws Exception
     */
    private static String retrieveCaseById(String caseId) throws Exception {

        if(StringUtils.isBlank(caseId))
            throw new Exception("No case id is available.");
        
        String resp = "";
        
        HashMap<String, String> params = new HashMap<String, String>();
        params.put("authToken", token);
        params.put("resourceId", caseId);
        
        RestCaller client = new RestCaller(baseManagerUrl, "", "", false, true, selfSigned, trustAll);
        try {
            resp = client.callGet("/rest/CaseService/getResourceById", params, null, "text/xml");
            Log.log.debug(resp);
        } catch(Exception e) {
            try {
                login();
                params.put("authToken", token);
                resp = client.callGet("/rest/CaseService/getResourceById", params, null, "text/xml");
            } catch(Exception ee) {
                Log.log.error(ee.getMessage(), ee);
                throw ee;
            }
        }
        
        return resp;
    }

    /**
     * Fetches the entire case xml by caseId, then parse the xml and modifies the stage with updated value and other actions accordingly. 
     * @param caseId: Required - a valid case id
     * @param newStage: Required - stage names, such as QUEUED, INITIATED, FOLLOW-UP, FINAL, CLOSED
     * @param fields: Optional - field names must be any of the following:
     *                actionsTaken
     *                plannedActions
     *                recommendedActions
     *                followupContact
     *                finalReportAction
     * @return The response payload in JSON format
     * @throws Exception
     */
    public static String updateStageByCaseId(String caseId, String newStage, Map<String, String> fields) throws Exception {
        
        String caseXml = retrieveCaseById(caseId);
  
        if(StringUtils.isBlank(caseXml))
            throw new Exception("No case is available for case id:" + caseId);
        
        int startIndex, endIndex;
        String resp = "";
        
        // extract xml namespace headers
        startIndex = caseXml.indexOf("xmlns:ns4=");
        endIndex = caseXml.indexOf(" xmlns:ns5");
        if(endIndex == -1)
            endIndex = caseXml.indexOf("><ns4:return>");
        
        String xmlNameSpace = "";
        
        if(startIndex == -1 || endIndex == -1)
            xmlNameSpace = NAMESPACE;
        
        else
            xmlNameSpace = caseXml.substring(startIndex+11, endIndex-1);
        
        // Parse the XML
        DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
        DocumentBuilder builder = factory.newDocumentBuilder();
        Document document = builder.parse(new ByteArrayInputStream(caseXml.getBytes()));
        NodeList nodeList = document.getDocumentElement().getChildNodes();
        Node root = nodeList.item(0);
        NodeList list = root.getChildNodes();

        boolean stageChanged = false;
        
        for(int i=0; i<list.getLength(); i++) {
            Node node = list.item(i);
            String name = node.getNodeName();
            if(StringUtils.isBlank(name))
                continue;
            
            if(name.equals("stage")) {
                Node oldTextNode = node.getFirstChild();
                Node newTextNode = document.createTextNode(newStage);
                if(oldTextNode != null)
                    node.removeChild(oldTextNode);
                node.appendChild(newTextNode);
                stageChanged = true;
            }
            
            else if(fields.containsKey(name)) {
                String value = fields.get(name);
                Log.log.debug(name);
                Log.log.debug(value);
                if(StringUtils.isNotEmpty(value)) {
                    Node oldTextNode = node.getFirstChild();
                    Node newTextNode = document.createTextNode(value);
                    if(oldTextNode != null)
                        node.removeChild(oldTextNode);
                    node.appendChild(newTextNode);
                }
            }
        }
        
        if(!stageChanged)
            throw new Exception("Original stage value is not available.");
        
        factory = DocumentBuilderFactory.newInstance();
        factory.setNamespaceAware(true);
        builder = factory.newDocumentBuilder();
        Document newDocument = builder.newDocument();
        newDocument.setXmlStandalone(true);
        Node rootNode = newDocument.createElementNS(xmlNameSpace, "ns4:update");
        
        Node authToken = newDocument.createElement("ns4:authToken");
        Node tokenValue = newDocument.createTextNode(token);
        authToken.appendChild(tokenValue);
        
        rootNode.appendChild(authToken);
        
        Node resource = newDocument.createElement("ns4:resource");
        for(int i=0; i<list.getLength(); i++) {
            Node node = list.item(i);
            
            String name = node.getNodeName();
            if(StringUtils.isBlank(name))
                continue;

            Node importedNode = newDocument.importNode(node, true);
            resource.appendChild(importedNode);
        }
        
        rootNode.appendChild(resource);
        
        newDocument.appendChild(rootNode);
        
        DOMSource domSource = new DOMSource(newDocument);
        StringWriter writer = new StringWriter();
        StreamResult result = new StreamResult(writer);
        TransformerFactory tf = TransformerFactory.newInstance();
        Transformer transformer = tf.newTransformer();
        transformer.setOutputProperty(OutputKeys.STANDALONE, "yes");
        transformer.transform(domSource, result);
        
        String caseUpdateXml = writer.toString();
        Log.log.debug("caseUpdateXml = " + caseUpdateXml);
        try {
            RestCaller client = new RestCaller(baseManagerUrl, "", "", false, true, selfSigned, trustAll);
            resp = client.callPost("/rest/CaseService/update", null, caseUpdateXml, null, "text/xml", null);
            Log.log.debug("update resp = " + resp);
            if(StringUtils.isNotEmpty(resp) && resp.indexOf("stage: " + newStage) < 0)
                Log.log.warn("Warning: Update case stage to be: " + newStage + " may be failed.");
        } catch(Exception e) {
            Log.log.error(e.getMessage(), e);
            throw e;
        }
        
        return resp;
    }
    
// =============================================================================
    
    private static Map<String, String> getEventUserIds(List<String> events) throws Exception {
        
        if(events == null || events.size() <2)
            throw new Exception("No event is available.");

        Map<String, String> eventUserIds = new HashMap<String, String>();
        
        int eventIdIndex = getFieldIndex(events.get(0), "Event ID");
        int userIdIndex = getFieldIndex(events.get(0), "Source User ID");
        
        for(int i=1; i<events.size(); i++) {
            String event = events.get(i);
            String[] fields = event.split(",");
            
            if(eventIdIndex > fields.length-1 || fields.length <2)
                continue;
            
            String eventId = fields[eventIdIndex];
            String userId = fields[userIdIndex];
            if(eventId != null)
                eventUserIds.put(eventId, userId);
        }
        
        return eventUserIds;
    }
    
    public static Map<String, Map<String, String>> parseEventDetails(String securityEventResponse) throws Exception {
        
        if(StringUtils.isBlank(securityEventResponse))
            throw new Exception("Security Events Response is not available.");
        
        Map<String, Map<String, String>> eventDetails = new HashMap<String, Map<String, String>>();

        JSONObject json = JSONObject.fromObject(securityEventResponse);
        
        if(json != null) {
            JSONObject response = json.getJSONObject("sev.getSecurityEventsResponse");
            if(response != null) {
                Object eventObject = response.get("sev.return");
                if(eventObject instanceof JSONArray) {
                    JSONArray events  = response.getJSONArray("sev.return");
                    for(int i=0; i<events.size(); i++) {
                        JSONObject event = (JSONObject)events.get(i);
                        parseEventDetail(event, eventDetails);
                    }
                }
                
                else {
                    JSONObject event = (JSONObject)eventObject;
                    parseEventDetail(event, eventDetails);
                }
            }
        }

        return eventDetails;
    }
    
    private static void parseEventDetail(JSONObject event, Map<String, Map<String, String>> eventDetails) throws Exception {
        
        String eventId = event.get("eventId").toString();
        String name = (String)event.get("name");
        String type = (String)event.get("type");
        Integer severity = (Integer)event.get("severity");
        /*        
        JSONObject source = (JSONObject)event.get("source");
        if(source != null) {
            String userId = null;
            try {
                userId = (String)source.getString("userId");
            } catch(Exception e) {}
            
            if(StringUtils.isNotEmpty(eventId) && userId != null)
                eventEventDetails.put(eventId, userId);
            Log.log.debug("eventId = " + eventId);
            Log.log.debug("userId = " + userId);
        }
 */        
        Log.log.debug("eventId = " + eventId);
        Log.log.debug("name = " + name);
        Log.log.debug("type = " + type);
        Log.log.debug("severity = " + severity);
        
        Map<String, String> eventDetail = new HashMap<String, String>();
        
        eventDetail.put("name", name);
        eventDetail.put("type", type);
        eventDetail.put("severity", severity.toString());
        
        eventDetails.put(eventId, eventDetail);
    }

    private static String getFileId(String resp) throws Exception {

        String fileId = "";
        
        if(StringUtils.isNotEmpty(resp)) {
            JSONObject json = JSONObject.fromObject(resp);
            if(json != null) {
                JSONObject response = json.getJSONObject("arc.initDefaultArchiveReportDownloadByIdResponse");
                if(response != null)
                    fileId = response.getString("arc.return");
                else
                    throw new Exception("Failed to get report file.");
            }
            Log.log.debug("fileId = " + fileId);
        }
        
        return fileId;
    }

    private static List<String> loadEvents(String fileId) throws Exception {
        
        byte[] resp = null;
        
        List<String> events = new ArrayList<String>();
        List<String> keys = new ArrayList<String>();
        
        Map<String, String> params = new HashMap<String, String>();
        params.put("file.command", "download");
        params.put("file.id", fileId);
        params.put("authToken", token);
        
        RestCaller client = new RestCaller(baseManagerUrl, "", "", false, true, selfSigned, trustAll);
        
        try {          
            resp = client.callGetBinary("/fileservlet", params, null, null);
        } catch(Exception e) {
            try {
                login();
                params.put("authToken", token);
                resp = client.callGetBinary("/fileservlet", params, null, null);
            } catch(Exception ee) {
                Log.log.error(ee.getMessage(), ee);
                throw ee;
            }
        }
        Log.log.debug(new String(resp, StandardCharsets.UTF_8));
        if(resp.length != 0) {
            StringBuilder sb = new StringBuilder();
            byte[] buffer = new byte[1];

            for(int i=0; i<resp.length; i++) {
                buffer[0] = resp[i];
                String b = Arrays.toString(buffer);
                String in = new String(buffer, StandardCharsets.UTF_8);

                if(Hex.encodeHexString(buffer).equals("0a")) {
                    Log.log.debug(sb.toString());
                    events.add(sb.toString());
                    sb = new StringBuilder();
                }
                else
                    sb.append(in);
            }
        }
        
        return events;
    }
    
    private static int getFieldIndex(String event, String field) {
        
        if(StringUtils.isBlank(event) || StringUtils.isBlank(field))
            return -1;
        
        String[] fields = event.split(",");
        int length = fields.length;
        if(length == 0)
            return -1;
        
        for(int i=0; i<length; i++) {
            String fieldName = fields[i];
            if(field.equals(fieldName))
                return i;
        }
        
        return -1;
    }
    
    private static void main1(String[] args) {
     
        try {
            updateEventStage("13979897", "RJcLiNfoAABCAS8xbPIxG0g==", "zcasd");
     
            // 9. update stage for case
            Map<String, String> comments = new HashMap<String, String>();
            comments.put("actionsTaken", "asdf1111");
            comments.put("plannedActions", "asdf");
            comments.put("recommendedActions", "asdf");
            comments.put("followupContact", "asdf");
            comments.put("finalReportAction", "asdf");
            
            updateStageByCaseId("72xK2s1kBABCAHgp2RY1OkA==", "CLOSED", comments);
        } catch(Exception e) {
            e.printStackTrace();
        }
    }
    
    private static void test() {
        
        try {
            // 1. login
            login();

            // 2. logout
//            logout();
            
            // 3. get report id
            String reportId = getReportId("Test Resolve Report");
            
            // 4. run report and retrieve event ids
            List<String> eventList = retrieveEventsFromReport("Test Resolve Report");
            
            List<String> eventIds = getEventIds(eventList);
            
            // 5. get security events
/*            String startMillis = null;
            try {
                long start = System.currentTimeMillis();
                startMillis = (new Long(start)).toString();
            } catch(Exception ee) {
                ee.printStackTrace();
                throw ee;
            }*/
            
            String events = getSecurityEvents(eventIds, "-1", "-1", RESP_TYPE);
            
            // 6. update event stage of "Follow-Up"
            String eventId = eventIds.get(0);
/*            Map<String, String> eventUserIds = getEventUserIds(events);
            if(eventUserIds.size() == 0)
                eventUserIds = getEventUserIds(events);
            
            String userId = eventUserIds.get(eventId);
            if(userId != null)*/
//                updateEventStage(eventId, "RPsLiNfoAABCATMxbPIxG0g==", "zcasd");

            // 7. get case id by case name
            String caseId = getCaseIdByCaseName("CasewithEvent");
            
            // 8. get resource by case id
            String caseXml = retrieveCaseById(caseId);
            
            // 9. update stage for case
            Map<String, String> comments = new HashMap<String, String>();
            comments.put("actionsTaken", "asdf");
            comments.put("plannedActions", "asdf");
            comments.put("recommendedActions", "asdf");
            comments.put("followupContact", "asdf");
            comments.put("finalReportAction", "asdf");
            
            updateStageByCaseId(caseId, "CLOSED", comments);
            
            // 9. get event ids by case name
            List<String> eventIDList = getCaseEventIDsByCaseName("CasewithEvent");

            // 10. get event ids by case id
            eventIDList = getCaseEventIDsByCaseId(caseId);
        } catch(Exception e) {
            e.printStackTrace();
        }
    }
    
} // ArcSightAPI
