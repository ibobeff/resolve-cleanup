package com.resolve.gateway.arcsight;

import java.io.File;
import java.util.HashMap;
import java.util.Map;
import org.apache.commons.io.FileUtils;

import com.resolve.rsbase.MainBase;
import com.resolve.gateway.Filter;
import com.resolve.rsremote.ConfigReceiveGateway;
import com.resolve.util.Log;
import com.resolve.util.ObjectProperties;
import com.resolve.util.StringUtils;
import com.resolve.util.XDoc;

public class ConfigReceiveArcSight extends ConfigReceiveGateway {

    private static final long serialVersionUID = 1L;

    private static final String RECEIVE_ARCSIGHT_NODE = "./RECEIVE/ARCSIGHT/";

    private static final String RECEIVE_ARCSIGHT_FILTER = RECEIVE_ARCSIGHT_NODE + "FILTER";

    private String queue = "ARCSIGHT";

    private static final String RECEIVE_ARCSIGHT_ATTR_PASSWORD = RECEIVE_ARCSIGHT_NODE + "@PASSWORD";

    private static final String RECEIVE_ARCSIGHT_ATTR_PORT = RECEIVE_ARCSIGHT_NODE + "@PORT";

    private static final String RECEIVE_ARCSIGHT_ATTR_SSLPASSWORD = RECEIVE_ARCSIGHT_NODE + "@SSLPASSWORD";

    private static final String RECEIVE_ARCSIGHT_ATTR_SSLCERTIFICATE = RECEIVE_ARCSIGHT_NODE + "@SSLCERTIFICATE";

    private static final String RECEIVE_ARCSIGHT_ATTR_HOST = RECEIVE_ARCSIGHT_NODE + "@HOST";

    private static final String RECEIVE_ARCSIGHT_ATTR_SSL = RECEIVE_ARCSIGHT_NODE + "@SSL";

    private static final String RECEIVE_ARCSIGHT_ATTR_USERNAME = RECEIVE_ARCSIGHT_NODE + "@USERNAME";

    private String password = "";

    private String port = "";

    private String sslpassword = "";

    private String sslcertificate = "";

    private String host = "";

    private String ssl = "";

    private String username = "";

    public String getQueue() {
        return queue;
    }

    public void setQueue(String queue) {
        this.queue = queue;
    }

    public String getPassword() {
        return this.password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getPort() {
        return this.port;
    }

    public void setPort(String port) {
        this.port = port;
    }

    public String getSslpassword() {
        return this.sslpassword;
    }

    public void setSslpassword(String sslpassword) {
        this.sslpassword = sslpassword;
    }

    public String getSslcertificate() {
        return this.sslcertificate;
    }

    public void setSslcertificate(String sslcertificate) {
        this.sslcertificate = sslcertificate;
    }

    public String getHost() {
        return this.host;
    }

    public void setHost(String host) {
        this.host = host;
    }

    public String getSsl() {
        return this.ssl;
    }

    public void setSsl(String ssl) {
        this.ssl = ssl;
    }

    public String getUsername() {
        return this.username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public ConfigReceiveArcSight(XDoc config) throws Exception {
        super(config);
        define("password", SECURE, RECEIVE_ARCSIGHT_ATTR_PASSWORD);
        define("port", STRING, RECEIVE_ARCSIGHT_ATTR_PORT);
        define("sslpassword", SECURE, RECEIVE_ARCSIGHT_ATTR_SSLPASSWORD);
        define("sslcertificate", STRING, RECEIVE_ARCSIGHT_ATTR_SSLCERTIFICATE);
        define("host", STRING, RECEIVE_ARCSIGHT_ATTR_HOST);
        define("ssl", STRING, RECEIVE_ARCSIGHT_ATTR_SSL);
        define("username", STRING, RECEIVE_ARCSIGHT_ATTR_USERNAME);
    }

    @Override
    public String getRootNode() {
        return RECEIVE_ARCSIGHT_NODE;
    }

    @Override
    public void load() throws Exception {
        try {
            loadAttributes();
            if (isActive()) {
                ArcSightGateway arcsightGateway = ArcSightGateway.getInstance(this);
                filters = xdoc.getListMapValue(RECEIVE_ARCSIGHT_FILTER);
                if (filters.size() > 0) {
                    for (Map<String, Object> values : filters) {
                        String filterName = ((String) values.get(ArcSightFilter.ID)).toLowerCase();
                        File scriptFile = getFile(MainBase.main.release.serviceName + "/config/arcsight/" + filterName + ".groovy");
                        try {
                            if (scriptFile.exists()) {
                                String groovy = FileUtils.readFileToString(scriptFile, "UTF-8");
                                if (StringUtils.isNotBlank(groovy)) {
                                    values.put(ArcSightFilter.SCRIPT, groovy);
                                    Log.log.debug("Loaded groovy script: " + scriptFile);
                                }
                            }
                            arcsightGateway.setFilter(values);
                        } catch (Exception e) {
                            Log.log.warn(e.getMessage() + ". Skipping file: " + scriptFile.getAbsolutePath());
                        }
                    }
                }
            }
        } catch (Throwable e) {
            setActive(false);
            Log.log.warn("Couldn not load configuration for ArcSight gateway, check blueprint. " + e.getMessage(), e);
        }
    }

    @Override
    public void save() throws Exception {
        File dir = getFile(MainBase.main.release.serviceName + "/config/arcsight");
        if (!dir.exists()) {
            FileUtils.forceMkdir(dir);
        }
        if (filters != null && isActive()) {
            filters.clear();
            for (Filter filter : ArcSightGateway.getInstance().getFilters().values()) {
                ArcSightFilter arcsightFilter = (ArcSightFilter) filter;
                String groovy = arcsightFilter.getScript();
                String scriptFileName;
                File scriptFile = null;
                if (StringUtils.isNotBlank(groovy)) {
                    scriptFileName = arcsightFilter.getId().toLowerCase() + ".groovy";
                    scriptFile = getFile(MainBase.main.release.serviceName + "/config/arcsight/" + scriptFileName);
                }
                Map<String, Object> entry = new HashMap<String, Object>();
                putFilterDataIntoEntry(entry, arcsightFilter);
                filters.add(entry);
                if (scriptFile != null) {
                    try {
                        FileUtils.writeStringToFile(scriptFile, groovy, "UTF-8");
                    } catch (Exception e) {
                        Log.log.warn(e.getMessage() + ". Skipping file: " + scriptFile.getAbsolutePath());
                    }
                }
            }
            xdoc.setListMapValue(RECEIVE_ARCSIGHT_FILTER, filters);
        }
        if (nameProperties != null && isActive()) {
            nameProperties.clear();
            for (String name : ArcSightGateway.getInstance().getNameProperties().keySet()) {
                Map<String, Object> entry = new HashMap<String, Object>();
                entry.put(NAME_KEY, name);
                nameProperties.add(entry);
                Map props = ArcSightGateway.getInstance().getNameProperties().getNameProperties(name);
                ObjectProperties prop = new ObjectProperties(props);
                prop.save(props);
            }
        }
        saveAttributes();
    }

    private void putFilterDataIntoEntry(Map<String, Object> entry, ArcSightFilter arcsightFilter) {
        entry.put(ArcSightFilter.ID, arcsightFilter.getId());
        entry.put(ArcSightFilter.ACTIVE, String.valueOf(arcsightFilter.isActive()));
        entry.put(ArcSightFilter.ORDER, String.valueOf(arcsightFilter.getOrder()));
        entry.put(ArcSightFilter.INTERVAL, String.valueOf(arcsightFilter.getInterval()));
        entry.put(ArcSightFilter.EVENT_EVENTID, arcsightFilter.getEventEventId());
        entry.put(ArcSightFilter.RUNBOOK, arcsightFilter.getRunbook());
        entry.put(ArcSightFilter.SCRIPT, arcsightFilter.getScript());
        entry.put(ArcSightFilter.URI, arcsightFilter.getUri());
        entry.put(ArcSightFilter.START, arcsightFilter.getStart());
        entry.put(ArcSightFilter.END, arcsightFilter.getEnd());
        entry.put(ArcSightFilter.REPORT, arcsightFilter.getReport());
    }
}

