package com.resolve.gateway.arcsight;

import java.util.HashMap;
import java.util.Map;

import java.util.List;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.Date;

import com.resolve.rsbase.MainBase;
import com.resolve.gateway.BaseClusteredGateway;
import com.resolve.gateway.BaseFilter;
import com.resolve.gateway.Filter;
import com.resolve.gateway.MArcSight;
import com.resolve.util.Constants;
import com.resolve.util.Log;
import com.resolve.util.StringUtils;
import com.resolve.rsremote.ConfigReceiveGateway;

public class ArcSightGateway extends BaseClusteredGateway {

    private static volatile ArcSightGateway instance = null;

    private String queue = null;

    public static ArcSightGateway getInstance(ConfigReceiveArcSight config) {
        if (instance == null) {
            instance = new ArcSightGateway(config);
        }
        return instance;
    }

    /**
     * This method is called once the initialization is done. If it gets called
     * before the initialization it will throw exception.
     *
     * @return
     */
    public static ArcSightGateway getInstance() {
        if (instance == null) {
            throw new RuntimeException("ArcSight Gateway is not initialized correctly..");
        } else {
            return instance;
        }
    }

    private ArcSightGateway(ConfigReceiveArcSight config) {
        super(config);
    }

    @Override
    public String getLicenseCode() {
        return "ARCSIGHT";
    }

    @Override
    protected void addAdditionalValues(Map<String, Object> arg0, Map<String, String> arg1) {
    }

    @Override
    protected String getGatewayEventType() {
        return "ARCSIGHT";
    }

    @Override
    protected String getMessageHandlerName() {
        return MArcSight.class.getSimpleName();
    }

    @Override
    protected Class<MArcSight> getMessageHandlerClass() {
        return MArcSight.class;
    }

    public String getQueueName() {
        return queue + getOrgSuffix();
    }

    @Override
    public void start() {
        Log.log.debug("Starting ArcSight Gateway");
        super.start();
    }

    @Override
    public void run() {
        if (!((ConfigReceiveGateway) configurations).isMock()) super.sendSyncRequest();
        while (running) {
            try {
                long startTime = System.currentTimeMillis();
                if (primaryDataQueue.isEmpty()) {
                    Log.log.trace("Local Primary Event Queue for " + getGatewayEventType() + " is empty.....");
                    for (Filter filter : orderedFilters) {
                        if (shouldFilterExecute(filter, startTime)) {
                            Log.log.trace("Filter " + filter.getId() + " executing");
                            
                            if(!shouldInvokeService(filter))
                                continue;
                            
                            try {
                                Timestamp time = new Timestamp(startTime);
                                Log.log.trace(filter.getId() + " service is invoked at: " + time.toString());
                            } catch(Exception ee) {}
                            
                            List<Map<String, String>> results = invokeService((ArcSightFilter)filter);
                            
                            for (Map<String, String> result : results) {
                                result.put(Constants.WORKSHEET_ALERTID, result.get("Event ID"));
                                addToPrimaryDataQueue(filter, result);
                            }
                        } else {
                            Log.log.trace("Filter:" + filter.getId() + " is inactive or not ready to execute yet.....");
                        }
                    }
                    if (orderedFilters == null || orderedFilters.size() == 0) {
                        Log.log.trace("There is not deployed filter for ArcSightGateway");
                    }
                }
                if (orderedFilters.size() == 0) {
                    Thread.sleep(interval);
                }
            } catch (Exception e) {
                Log.log.warn("Warning: " + e.getMessage());
                Log.alert("Instance " + MainBase.main.configId.getGuid() + " " + getLicenseCode() + "-" + getQueueName() + "-" + getInstanceType() + " Polling Failed", "Instance " + MainBase.main.configId.getGuid() + " " + getLicenseCode() + "-" + getQueueName() + "-" + getInstanceType() + " Polling Failed " + "due to " + e.getMessage());
                try {
                    Thread.sleep(interval);
                } catch (InterruptedException ie) {
                    Log.log.warn("sleep interrupted: " + ie.getMessage(), ie);
                }
            }
        }
    }

    /**
     * Add customized code here to get the data as List<Map<String, String>> format for each filter 
     * @return List<Map<String, String>>
     */
    public List<Map<String, String>> invokeService(ArcSightFilter filter) {
        List<Map<String, String>> result = new ArrayList<Map<String, String>>();
        //Add customized code here to get data from 3rd party system based the information in the filter;
        
        try {
            String report = filter.getReport();
            if(StringUtils.isBlank(report))
                throw new Exception("Report name is not available.");
            
            List<String> eventList = ArcSightAPI.retrieveEventsFromReport(report);
            result = ArcSightAPI.getEventSummary(eventList);
        } catch(Exception e) {
            Log.log.error(e.getMessage(), e);
        }
        
        return result;
    }

    /**
     * Add customized code here to initilize the connection with 3rd party system
     * @return List<Map<String, String>>
     */
    @Override
    protected void initialize() {
        ConfigReceiveArcSight config = (ConfigReceiveArcSight) configurations;
        queue = config.getQueue().toUpperCase();
        this.gatewayConfigDir = "/config/arcsight/";
        //Add customized code here to initilize the connection with 3rd party system;
    }

    /**
     * Add customized code here to do when stop the gateway
     */
    @Override
    public void stop() {
        Log.log.warn("Stopping ArcSight gateway");
        //Add customized code here to stop the connection with 3rd party system;
        super.stop();
    }

    @Override
    public Filter getFilter(Map<String, Object> params) {
        return new ArcSightFilter((String) params.get(BaseFilter.ID), (String) params.get(BaseFilter.ACTIVE), (String) params.get(BaseFilter.ORDER), (String) params.get(BaseFilter.INTERVAL), (String) params.get(BaseFilter.EVENT_EVENTID), (String) params.get(BaseFilter.RUNBOOK), (String) params.get(BaseFilter.SCRIPT), (String) params.get(ArcSightFilter.URI), (String) params.get(ArcSightFilter.START), (String) params.get(ArcSightFilter.END), (String) params.get(ArcSightFilter.REPORT));
    }
    
    public Map<String, Object> loadSystemProperties() {

        Map<String, Object> properties = new HashMap<String, Object>();

        ConfigReceiveArcSight config = (ConfigReceiveArcSight) configurations;

        properties.put("HOST", config.getHost());
        properties.put("PORT", config.getPort());
        properties.put("USERNAME", config.getUsername());
        properties.put("PASSWORD", config.getPassword());
        properties.put("SSL", config.getSsl());

        return properties;
    }
    
    private boolean shouldInvokeService(Filter filter) {
        
        boolean invoke = false;
        long startTime = -1;
        long endTime = -1;
        
        String start = ((ArcSightFilter)filter).getStart();
        String end = ((ArcSightFilter)filter).getEnd();
        
        try {
            long currentTime = new Timestamp((new Date()).getTime()).getTime();
            
            if(!StringUtils.isBlank(start))
                startTime = Timestamp.valueOf(start).getTime();
            
            if(!StringUtils.isBlank(end))
                endTime = Timestamp.valueOf(end).getTime();
            
            if((startTime == -1 || currentTime >= startTime) && (endTime == -1 || currentTime < endTime))
                return true;
        } catch(Exception e) {
            Log.log.error(e.getMessage(), e);
            return true;
        }
        
        return invoke;
    }
}

