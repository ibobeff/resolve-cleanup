package com.resolve.persistence.model;

import java.util.List;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Lob;
import javax.persistence.Table;
import javax.persistence.UniqueConstraint;
import com.resolve.services.interfaces.VO;
import com.resolve.services.hibernate.vo.ArcSightFilterVO;

@Entity
@Table(name = "arcsight_filter", uniqueConstraints = { @UniqueConstraint(columnNames = { "u_name", "u_queue" }) })
public class ArcSightFilter extends GatewayFilter<ArcSightFilterVO> {

    private static final long serialVersionUID = 1L;

    public ArcSightFilter() {
    }

    public ArcSightFilter(ArcSightFilterVO vo) {
        applyVOToModel(vo);
    }

    private String UUri;

    private String UStart;

    private String UEnd;

    private String UReport;

    @Column(name = "u_uri", length = 256)
    public String getUUri() {
        return this.UUri;
    }

    public void setUUri(String uUri) {
        this.UUri = uUri;
    }

    @Column(name = "u_start", length = 256)
    public String getUStart() {
        return this.UStart;
    }

    public void setUStart(String uStart) {
        this.UStart = uStart;
    }

    @Column(name = "u_end", length = 256)
    public String getUEnd() {
        return this.UEnd;
    }

    public void setUEnd(String uEnd) {
        this.UEnd = uEnd;
    }

    @Column(name = "u_report", length = 256)
    public String getUReport() {
        return this.UReport;
    }

    public void setUReport(String uReport) {
        this.UReport = uReport;
    }

    public ArcSightFilterVO doGetVO() {
        ArcSightFilterVO vo = new ArcSightFilterVO();
        super.doGetBaseVO(vo);
        vo.setUUri(getUUri());
        vo.setUStart(getUStart());
        vo.setUEnd(getUEnd());
        vo.setUReport(getUReport());
        return vo;
    }

    @Override
    public void applyVOToModel(ArcSightFilterVO vo) {
        if (vo == null) return;
        super.applyVOToModel(vo);
        if (!VO.STRING_DEFAULT.equals(vo.getUUri())) this.setUUri(vo.getUUri()); else ;
        if (!VO.STRING_DEFAULT.equals(vo.getUStart())) this.setUStart(vo.getUStart()); else ;
        if (!VO.STRING_DEFAULT.equals(vo.getUEnd())) this.setUEnd(vo.getUEnd()); else ;
        if (!VO.STRING_DEFAULT.equals(vo.getUReport())) this.setUReport(vo.getUReport()); else ;
    }

    @Override
    public List<String> ugetCdataProperty() {
        List<String> list = super.ugetCdataProperty();
        list.add("UUri");
        list.add("UStart");
        list.add("UEnd");
        list.add("UReport");
        return list;
    }
}

