package com.resolve.gateway;

import java.util.Map;
import com.resolve.gateway.arcsight.ArcSightFilter;
import com.resolve.gateway.arcsight.ArcSightGateway;

public class MArcSight extends MGateway {

    private static final String RSCONTROL_SET_FILTERS = "MArcSight.setFilters";

    private static final ArcSightGateway instance = ArcSightGateway.getInstance();

    public MArcSight() {
        super.instance = instance;
    }

    protected String getSetFilterMethodName() {
        return RSCONTROL_SET_FILTERS;
    }

    /**
     * Populates the {@link Filter} data into the given filterMap {@link Map} instance
     * 
     * @param filterMap
     *      the {@link Map} instance to hold the {@link Filter} data
     * @param filter
     *      a {@link ArcSightFilter} instance
     *            
     */
    public void populateGatewaySpecificFilterProperties(Map<String, String> filterMap, Filter filter) {
        if (instance.isActive()) {
            ArcSightFilter arcsightFilter = (ArcSightFilter) filter;
            filterMap.put(ArcSightFilter.URI, arcsightFilter.getUri());
            filterMap.put(ArcSightFilter.START, arcsightFilter.getStart());
            filterMap.put(ArcSightFilter.END, arcsightFilter.getEnd());
            filterMap.put(ArcSightFilter.REPORT, arcsightFilter.getReport());
        }
    }
}

