package com.resolve.gateway.push.moogsoft;

import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

import com.resolve.gateway.Filter;
import com.resolve.gateway.resolvegateway.push.ConfigReceivePushGateway;
import com.resolve.gateway.resolvegateway.push.HttpServer;
import com.resolve.gateway.resolvegateway.push.PushGateway;
import com.resolve.gateway.resolvegateway.push.PushGatewayFilter;
import com.resolve.util.Log;

public class MoogsoftGateway extends PushGateway
{
    private static volatile MoogsoftGateway instance = null;

    private static volatile HttpServer defaultHttpServer;

    private Map<Integer, HttpServer> httpServers = new HashMap<Integer, HttpServer>();
    private Map<String, HttpServer> deployedServlets = new HashMap<String, HttpServer>();

    private MoogsoftGateway(ConfigReceivePushGateway config) {

        super(config);

        String gatewayName = this.getClass().getSimpleName();
        int index = gatewayName.indexOf("Gateway");
        if(index != -1)
            name = gatewayName.substring(0, index);

        Map<String, PushGatewayFilter> pushFilters = getPushFilters();

        for(Iterator<String> iterator=pushFilters.keySet().iterator(); iterator.hasNext();) {
            String filterName = iterator.next();
            PushGatewayFilter filter = pushFilters.get(filterName);

            if(name.indexOf(((PushGatewayFilter)filter).getGatewayName()) != -1) {
                if(filters.get(filterName) == null) {
                    filters.put(filterName, pushFilters.get(filterName));

                    if(filter.isDeployed()) {
                        orderedFilters.add(filter);
                        orderedFiltersMapById.put(filter.getId(), filter);
                    }
                }
            }
        }
        
        ConfigReceivePushGateway.getGateways().put(name, this);

//        properties = config.getGatewayProperties(name);
    }

    public static MoogsoftGateway getInstance(ConfigReceivePushGateway config) {

        if (instance == null) {
            instance = new MoogsoftGateway(config);
        }

        return instance;
    }

    public static MoogsoftGateway getInstance() {

        if (instance == null)
            Log.log.warn("Moogsoft Gateway is not initialized correctly..");

        return instance;
    }

    public HttpServer getDefaultHttpServer()
    {
        return defaultHttpServer;
    }

    public Map<Integer, HttpServer> getHttpServers()
    {
        return httpServers;
    }

    public Map<String, HttpServer> getDeployedServlets()
    {
        return deployedServlets;
    }

    @Override
    protected void initialize() {

        ConfigReceivePushGateway config = (ConfigReceivePushGateway)configurations;
        queue = config.getQueue().toUpperCase();

        try {
            Log.log.info("Initializing Listener to queue: " + queue);
            this.gatewayConfigDir = "/config/" + queue.toLowerCase() + "/";

            defaultHttpServer = new HttpServer(config, null, null, name);
            defaultHttpServer.init();
            defaultHttpServer.start();

            if (defaultHttpServer.isStarted())
                defaultHttpServer.addDefaultResolveServlet();
        } catch (Exception e) {
            Log.log.error("Failed to config MoogSoft Gateway: " + e.getMessage(), e);
        }
    }
    
    @Override
    public void reinitialize() {

        ConfigReceivePushGateway config = (ConfigReceivePushGateway)configurations;
        queue = config.getQueue().toUpperCase();

        try {
            Log.log.info("Initializing Listener to queue: " + queue);
            this.gatewayConfigDir = "/config/" + queue.toLowerCase() + "/";

            defaultHttpServer = new HttpServer(config, null, null, name);
            defaultHttpServer.init();
            defaultHttpServer.start();

            if (defaultHttpServer.isStarted())
                defaultHttpServer.addDefaultResolveServlet();

            ConfigReceivePushGateway.getGateways().put(name, this);
        } catch (Exception e) {
            Log.log.error("Failed to config MoogSoft Gateway: " + e.getMessage(), e);
        }
    }

    @Override
    public void deactivate() {
        
        super.deactivate();
        
        try {
            defaultHttpServer.stop();
        } catch (Exception e)  {
            Log.log.error(e.getMessage(), e);
        }
        
        Map<Integer, HttpServer> httpServers = getHttpServers();
        
        for (HttpServer httpServer : httpServers.values()) {
            try {
                httpServer.stop();
            } catch (Exception e) {
                Log.log.error(e.getMessage(), e);
            }
        }

        httpServers.clear();
    }
    
    @Override
    public void start() {

        Log.log.debug("Starting Moogsoft Gateway");
        super.start();
    }

    @Override
    public void stop() {

        Log.log.warn("Stopping Moogsoft gateway");

        try {
            getDefaultHttpServer().stop();
        } catch (Exception e)  {
            Log.log.error(e.getMessage(), e);
        }
        
        Map<Integer, HttpServer> httpServers = getHttpServers();
        
        for (HttpServer httpServer : httpServers.values()) {
            try {
                httpServer.stop();
            } catch (Exception e) {
                Log.log.error(e.getMessage(), e);
            }
        }

        httpServers.clear();

        super.stop();
    }

    @Override
    protected String getMessageHandlerName() {
        return this.getClass().getName();
    }

    @Override
    protected void cleanFilter(ConcurrentHashMap<String, Filter> originalFilters) {
        //Add customized code here to modify your server when you clear out the deployed filters;

        for(Filter filter : originalFilters.values())
        {
            PushGatewayFilter pushGatewayFilter = (PushGatewayFilter) filter;
            String id = pushGatewayFilter.getId();

            try
            {
                if(pushGatewayFilter.getPort() > 0)
                {
                    HttpServer httpServer = httpServers.get(pushGatewayFilter.getPort());
                    httpServer.removeServlet(pushGatewayFilter.getId());
                    //if there is no filter/servlet remaining then stop the server on the port
                    if(httpServer.getServlets().isEmpty())
                    {
                        httpServer.stop();
                        httpServers.remove(pushGatewayFilter.getPort());
                    }
                }
                else
                {
                    //undeploy the servlet from the default server.
                    defaultHttpServer.removeServlet(id);
                }

                MoogsoftGatewayAPI.tokens.remove(id);
            }
            catch (Exception e)
            {
                Log.log.error(e.getMessage(), e);
            }
        }
    }
    
    @Override
    public void initServers() {
        
        Map<Integer, HttpServer> httpServers = getHttpServers();
        Map<String, HttpServer> deployedServlets = getDeployedServlets();

        try {            
            for (Filter deployedFilter : orderedFilters) {
                PushGatewayFilter filter = (PushGatewayFilter)deployedFilter;
                
                if (filter.getPort() == null || filter.getPort() <= 0) {
                    addDefaultServlet(filter);
                } 
                
                else {
                    HttpServer httpServer = httpServers.get(filter.getPort());
    
                    if (httpServer == null)
                    {
                        HttpServer httpServer1 = deployedServlets.get(filter.getId());
    
                        if (httpServer1 != null)
                        {
                            //port changed
                            httpServer1.removeServlet(filter.getId());
                            getDeployedServlets().remove(filter.getId());
                        }
    
                        httpServer = new HttpServer((ConfigReceivePushGateway)configurations, filter.getPort(), filter.isSsl(), filter.getGatewayName());
                        httpServer.init();
                        httpServer.addServlet(filter);
                        httpServer.start();
                    }
                    else
                    {
                        httpServer.addServlet(filter);
                    }
    
                    httpServers.put(filter.getPort(), httpServer);
                    deployedServlets.put(filter.getId(), httpServer);
                }
            }
        }
        catch (Exception e)
        {
            Log.log.error(e.getMessage(), e);
        }
    }
    
    protected void removeFilters(List<Filter> undeployedFilters) {

        for(Filter undeployedFilter : undeployedFilters)
        {
            PushGatewayFilter filter = (PushGatewayFilter)undeployedFilter;
            String id = filter.getId();
            
            try
            {
                if(filter.getPort() > 0)
                {
                    HttpServer httpServer = httpServers.get(filter.getPort());
                    httpServer.removeServlet(filter.getId());
                    //if there is no filter/servlet remaining then stop the server on the port
                    if(httpServer.getServlets().isEmpty())
                    {
                        httpServer.stop();
                        httpServers.remove(filter.getPort());
                    }
                }
                else
                {
                    //undeploy the servlet from the default server.
                    defaultHttpServer.removeServlet(id);

                    filters.remove(id);
                }
            }
            catch (Exception e)
            {
                Log.log.error(e.getMessage(), e);
            }
        }
    }
    
} // class MoogsoftGateway
