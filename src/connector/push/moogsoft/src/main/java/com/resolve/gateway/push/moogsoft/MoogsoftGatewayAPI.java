package com.resolve.gateway.push.moogsoft;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.TimeZone;

import com.resolve.gateway.Filter;
import com.resolve.gateway.resolvegateway.GatewayProperties;
import com.resolve.gateway.resolvegateway.push.ConfigReceivePushGateway;
import com.resolve.gateway.resolvegateway.push.PushGatewayFilter;
import com.resolve.gateway.AbstractGatewayAPI;
import com.resolve.util.CryptUtils;
import com.resolve.util.Log;
import com.resolve.util.StringUtils;
import com.resolve.util.restclient.RestCaller;

import net.sf.json.JSONArray;
import net.sf.json.JSONObject;
import net.sf.json.JSONSerializer;

public class MoogsoftGatewayAPI extends AbstractGatewayAPI
{
    private static String HOSTNAME = "moog-01.resolvesys.com";
    private static String PORT = "";
    private static String URI = "/graze/v1/";
    private static String resolveURI = "resolve/jsp/rsclient.jsp?IFRAMEID=moogsoft#RS.worksheet.Worksheet/id=";

    private String user = "resolve_grazer";
    private String pass = "rgrazer";
    private Locale locale = Locale.US;
    private String timezone = "GMT";
    private String format = "yyyy-MM-dd'T'HH:mm:ss'Z'";
    
    private String filterId = null;
    
    private static String baseUrl = null;

    static volatile Map<String, String> tokens = new HashMap<String, String>();
    
    private static MoogsoftGateway gateway = MoogsoftGateway.getInstance();
    
    public static MoogsoftGatewayAPI instance = new MoogsoftGatewayAPI();
    
    private static GatewayProperties properties = null;

    public MoogsoftGatewayAPI() {
        
        try {
            properties = ConfigReceivePushGateway.getGatewayProperties("Moogsoft");

            HOSTNAME = (String)properties.getHost();
            PORT = (String)properties.getPort().toString();
            user = (String)properties.getUser();
            pass = (String)properties.getPass();
            
            StringBuffer sb = new StringBuffer();
            sb.append("https://").append(HOSTNAME);
            if(StringUtils.isNotBlank(PORT))
                sb.append(":").append(PORT);
            sb.append(URI);
            
            baseUrl = sb.toString();
        } catch(Exception e) {
            Log.log.error(e.getMessage(), e);
        }
    }

    public MoogsoftGatewayAPI(String filterId) throws Exception {
        
        Map<String, Filter> filters = gateway.getFilters();
        PushGatewayFilter filter = (PushGatewayFilter)filters.get(filterId);
        
        if(filter == null)
            throw new Exception("Filter not found or not deployed with filter id: " + filterId);
        
        this.filterId = filterId;
        
        try {
            Map<String, Object> attrs = filter.getAttributes();
            
            if(attrs != null) {
                for(Iterator<String> it=attrs.keySet().iterator(); it.hasNext();) {
                    String key = it.next();
                    switch(key) {
                        case "uusername":
                            String username = (String)attrs.get(key);
                            user = StringUtils.isBlank(username)?(String)properties.getUser():username;
                            break;
                        case "upassword":
                            String passcode = (String)attrs.get(key);
                            pass = StringUtils.isBlank(passcode)?(String)properties.getPass():CryptUtils.decrypt(passcode);
                            break;
                        case "utimezone":
                            String zone = (String)attrs.get(key);
                            timezone = StringUtils.isBlank(zone)?timezone:zone;
                            break;
                        case "uformat":
                            String timeFormat = (String)attrs.get(key);
                            format = StringUtils.isBlank(timeFormat)?format:timeFormat;
                            break;
                        default:
                            break;
                    }
                }
            }
            
            Log.log.debug("pass = " + pass);
            
            StringBuffer sb = new StringBuffer();
            sb.append("https://").append(HOSTNAME);
            if(StringUtils.isNotBlank(PORT))
                sb.append(":").append(PORT);
            sb.append(URI);
            
            baseUrl = sb.toString();
        } catch(Exception e) {
            Log.log.error(e.getMessage(), e);
        }
    }
    
    /**
     * Get the resolve URI to fetch the worksheet and attach the worksheet id after it
     * @return resulve URL
     */
    public static String getResolveUri()
    {
        return resolveURI;
    }
    
    /**
     * A GET request that returns details (such as Description, Severity, etc.) of a specified Alert.
     * @param alertId: value of alert id
     * @return a string of alert details in JSON format
     * @throws Exception
     */
    public String getAlertDetails(Integer alertId) throws Exception {
        
        if(alertId == null)
            throw new Exception("alertId is not available.");
        
        String details = null;
        String uri = "getAlertDetails?alert_id=" + alertId;
        
        RestCaller client = new RestCaller(baseUrl, user, pass, true, true, false, true);
        
        try {
            details = client.callGet(uri, null, null);
        } catch(Exception e) {
            Log.log.error(e.getMessage(), e);
            throw e;
        }
        
        return details;
    }
    
    /**
     * A GET request that returns details (such as Description, Status, etc.) of a specified Situation.
     * @param sitnId: value of situation id
     * @returna a string of situation details in JSON format
     * @throws Exception
     */
    public String getSituationDetails(Integer sitnId) throws Exception {
        
        if(sitnId == null)
            throw new Exception("sitnId is not available.");
        
        String details = null;

        String uri = "getSituationDetails?sitn_id=" + sitnId;
        RestCaller client = new RestCaller(baseUrl, user, pass, true, true, false, true);
        try {
            details = client.callGet(uri, null, null);
        } catch(Exception e) {
            Log.log.error(e.getMessage(), e);
            throw e;
        }
        
        return details;
    }
    
    /**
     * A GET request that returns the total number of Alerts, and a list of their Alert IDs for a specified Situation. 
     * This can be either all Alerts or just those Alerts unique to the Situation.
     * @param sitnId: required - The Situation ID
     * @param forUniqueAlerts: required - Indicates the Alerts to get from the Situation: 
     *                                    true = get only Alerts unique to the Situation, 
     *                                    false = get all Alerts in the Situation (default)
     * @return
     * @throws Exception
     */
    public List<String> getSituationAlertIds(Integer sitnId, Boolean forUniqueAlerts) throws Exception {
        
        if(sitnId == null)
            throw new Exception("sitnId is not available.");
        
        List<String> alerts = new ArrayList<String>();
        String alertList = null;
        
        RestCaller client = new RestCaller(baseUrl, user, pass, true, true, false, true);
        
        try {
            StringBuilder sb = new StringBuilder();
            sb.append("getSituationAlertIds?sitn_id=").append(sitnId).append("&for_unique_alerts=").append(forUniqueAlerts);
            
            alertList = client.callGet(sb.toString(), null, null);
        } catch(Exception e) {
            Log.log.error(e.getMessage(), e);
            throw e;
        }

        if(StringUtils.isBlank(alertList))
            return null;
        
        try {
            JSONObject json = JSONObject.fromObject(alertList);
            Object alertIds = json.get("alert_ids");
            JSONArray alertIdArray = (JSONArray) JSONSerializer.toJSON(alertIds);
            
            for(int i=0; i<alertIdArray.size(); i++) {
                String alertId = alertIdArray.getString(i);
                Log.log.debug(alertId);
                alerts.add(alertId);
            }
        } catch(Exception e) {
            Log.log.error(e.getMessage(), e);
            throw e;
        }
        
        return alerts;
    }
    
    /**
     * A POST request that adds and merges custom information for a specified Alert. Custom information is added and merged with any existing Alert custom information.
     * {"custom_info":{"Resolve":{"actionStatus":"Automation Completed","actionSummary":"adfdas"}},"alert_id":10,"auth_token":"a91b9d83fb1d4eeebbf27e676dc5a583"}
     * @param alertId: value of alert id
     * @param status: value to update Resolve.actionStatus field
     * @param summary: value to update Resolve.actionSummary field
     * @throws Exception
     */
    public void updateAlert(Integer alertId, String status, String results, String summary) throws Exception {

        if(alertId == null)
            throw new Exception("alertId is not available.");
        
        JSONObject jsonBody = new JSONObject();
        JSONObject jsonResolve = new JSONObject();
        JSONObject json = new JSONObject();
        
        json.put("actionStatus", status);
        json.put("actionResults", results);
        json.put("actionSummary", summary);
        
        jsonResolve.put("Resolve", json);
        
        jsonBody.accumulate("custom_info", jsonResolve);
        jsonBody.accumulate("alert_id", alertId);

        RestCaller client = new RestCaller(baseUrl, user, pass, true, true, false, true);
        
        try {
            client.callPost("addAlertCustomInfo", null, jsonBody.toString(), null, "application/json", null);
        } catch(Exception e) {
            Log.log.error(e.getMessage(), e);
            throw e;
        }
    }
    
    /**
     * A POST request that creates a manual Situation.
     * @return A new Situation ID
     * @throws Exception
     */
    public Integer createSituation(String description) throws Exception {
        
        if(description == null)
            description = "";
        
        Integer sitnId = null;
        
        String result = "";
        
        JSONObject json = new JSONObject();

        json.put("description", description);

        RestCaller client = new RestCaller(baseUrl, user, pass, true, true, false, true);
        
        try {
            result = client.callPost("createSituation", null, json.toString(), null, "application/json", null);
            Log.log.debug(result);
            
        } catch(Exception e) {
            Log.log.error(e.getMessage(), e);
            throw e;
        }
        
        try {
            if(StringUtils.isNotBlank(result)) {
                JSONObject jsonResult = JSONObject.fromObject(result);
                sitnId = (Integer)jsonResult.get("sitn_id");
                if(sitnId == null)
                    throw new Exception(jsonResult.toString());
            }
        } catch(Exception e) {
            Log.log.error(e.getMessage(), e);
            throw e;
        }
        
        return sitnId;
    }
    
    /**
     * A POST request that adds a specified Alert to the specified Situation.
     * @param alertId: value of alert id
     * @param sitnId: value of situation id
     * @throws Exception
     */
    public void addAlertToSituation(Integer alertId, Integer sitnId) throws Exception {

        if(alertId == null)
            throw new Exception("alertId is not available.");
        
        if(sitnId == null)
            throw new Exception("sitnId is not available.");
        
        JSONObject json = new JSONObject();

        json.put("alert_id", new Integer(alertId));
        json.put("sitn_id", new Integer(sitnId));
        
        RestCaller client = new RestCaller(baseUrl, user, pass, true, true, false, true);
        
        try {
            client.callPost("addAlertToSituation", null, json.toString(), null, "application/json", null);
        } catch(Exception e) {
            Log.log.error(e.getMessage(), e);
            throw e;
        }
    }
    
    /**
     * Create a new thread given the thread name and add an entry to the thread with the given thread name.
     * @param sitnId: value of the situation id
     * @param threadName: the thread name of the situation
     * @param threadEntry: the thread entry value of the situation
     * @throws Exception
     */
    public void addSituationThread(Integer sitnId, String threadName, String threadEntry) throws Exception {
        
        if(sitnId == null)
            throw new Exception("sitnId is not available.");
        
        createThread(sitnId, threadName);
        
        addThreadEntry(sitnId, threadName, threadEntry);
    }
    
    /**
     * A POST request that creates a new thread for a specified Situation.
     * @param sitnId: the value of the situation id
     * @param threadName: the thread name of the situation
     * @throws Exception
     */
    public void createThread(Integer sitnId, String threadName) throws Exception {
        
        if(StringUtils.isBlank(threadName))
            throw new Exception("threadName is not available.");
        
        JSONObject json = new JSONObject();

        json.put("sitn_id", new Integer(sitnId));
        json.put("thread_name", threadName);
        
        RestCaller client = new RestCaller(baseUrl, user, pass, true, true, false, true);
        
        try {
            client.callPost("createThread", null, json.toString(), null, "application/json", null);
        } catch(Exception e) {
            Log.log.error(e.getMessage(), e);
            throw e;
        }
    }
    
    /**
     * A POST request that adds a new entry to an existing thread in a Situation.
     * @param sitnId: the value of the situation id
     * @param threadName: the thread name of the situation
     * @param threadEntry: the thread entry value of the situation
     * @throws Exception
     */
    public void addThreadEntry(Integer sitnId, String threadName, String threadEntry) throws Exception {
        
        if(StringUtils.isBlank(threadName))
            throw new Exception("threadName is not available.");
        
        JSONObject json = new JSONObject();

        json.put("sitn_id", new Integer(sitnId));
        json.put("thread_name", threadName);
        json.put("entry", threadEntry);

        RestCaller client = new RestCaller(baseUrl, user, pass, true, true, false, true);
        
        try {
            client.callPost("addThreadEntry", null, json.toString(), null, "application/json", null);
        } catch(Exception e) {
            Log.log.error(e.getMessage(), e);
            throw e;
        }
    }
    
    /**
     * A POST request that adds and merges custom information for a specified Situation.
     * @param sitnId: value of situation id
     * @param status: value to update Resolve.actionStatus field
     * @param results: value to update Resolve.actionResults field
     * @param summary: value to update Resolve.actionSummary field
     * @throws Exception
     */
    public void updateSituation(Integer sitnId, String status, String results, String summary) throws Exception {

        if(sitnId == null)
            throw new Exception("sitnId is not available.");

        JSONObject jsonBody = new JSONObject();
        JSONObject jsonResolve = new JSONObject();
        JSONObject json = new JSONObject();

        json.put("actionStatus", status);
        json.put("actionResults", results);
        json.put("actionSummary", summary);
        
        jsonResolve.put("Resolve", json);
        
        jsonBody.accumulate("custom_info", jsonResolve);
        jsonBody.accumulate("sitn_id", new Integer(sitnId));

        RestCaller client = new RestCaller(baseUrl, user, pass, true, true, false, true);
        
        try {
            client.callPost("addSituationCustomInfo", null, jsonBody.toString(), null, "application/json", null);
        } catch(Exception e) {
            Log.log.error(e.getMessage(), e);
            throw e;
        }
    }
    
    /**
     *  A POST request that adds and merges custom information for a specified Situation.
     * <pre>
     * {@code
     * try{
     *       def results = "http://host addr:port/resolve/jsp/rsclient.jsp?IFRAMEID=moogsoft#RS.worksheet.Worksheet/id=" + PARAMS["PROBLEMID"]
     *       def status = "Status"
     *       def summary = "Summary"
     *       def sitnId = 5//situation id;
     *       JSONObject jsonResolve = new JSONObject();
     *       JSONObject json = new JSONObject();
     *       json.put("actionStatus", status);
     *       json.put("actionResults", results);
     *       json.put("actionSummary", summary);  
     *       jsonResolve.put("Resolve", json);     
     *      MoogSoftAPI.updateSituation(sitnId,jsonResolve); 
        }

     * catch (Exception ex)
     * {
     *      //Do something good with this error.
     *      ex.printStackTrace();
     * }
     * }
     * </pre>
     * @param sitnId value of situation id
     * @param paramJson A valid JSON Object that has custome fields to be updated and their values. If invalid JSON object is passed then throws exception.
     * @throws Exception
     */
    public void updateSituation(Integer sitnId,JSONObject paramJson) throws Exception
    {

        if (sitnId == null) throw new Exception("sitnId is not available.");

        if (paramJson == null || !(paramJson instanceof JSONObject))
        {
            Log.log.error("Failed to update situation. Please pass valid JSON Object");
            throw new Exception("JSON Object passed to update situation is invalid.");

        }

        JSONObject jsonBody = new JSONObject();
        jsonBody.accumulate("custom_info", paramJson);
        jsonBody.accumulate("sitn_id", new Integer(sitnId));

        RestCaller client = new RestCaller(baseUrl, user, pass, true, true, false, true);

        try
        {
            if (jsonBody instanceof JSONObject)
            {
                client.callPost("addSituationCustomInfo", null, jsonBody.toString(), null, "application/json", null);
            }
            else
            {
                Log.log.error("Error processing update situation Api call.");
                throw new Exception("Error processing update situation Api call.");
            }

        }
        catch (Exception e) {
            Log.log.error(e.getMessage(), e);
            throw e;
        }
    }
    
    
    /**
     * Update all the associated situation for a specific alert.
     * Call updateSitutaion for each active situation listed in the alert details.
     * @param alertId: the value of the alert id
     * @param status: value to update Resolve.actionStatus field
     * @param results: value to update Resolve.actionResults field
     * @param summary: value to update Resolve.actionSummary field
     * @throws Exception
     */
    public void updateAllSituations(Integer alertId, String status, String results, String summary) throws Exception {
        
        if(alertId == null)
            throw new Exception("alertId is not available.");
        
        String details = getAlertDetails(alertId);
        
        if(StringUtils.isBlank(details))
            throw new Exception("No alert details available.");
        
        try {
            JSONObject jsonDetails = JSONObject.fromObject(details);
            
            JSONArray jsonArray = jsonDetails.getJSONArray("active_sitn_list");

            for(int i=0; i<jsonArray.size(); i++) {
                Integer sitnId = jsonArray.getInt(i);
                updateSituation(sitnId, status, results, summary);
            }
        } catch(Exception e) {
            Log.log.error(e.getMessage(), e);
            throw e;
        }
    }
    
    /**
     * A POST request that closes a specified Situation that is currently open, and optionally closes Alerts in the Situation.
     * @param sitnId: the value of the situation id
     * @param resolution: Determines what to do  with the Situation's Alerts:
     *                    0: Close no Alerts
     *                    1: Close all Alerts in this Situation (TBD)
     *                    2: Close only Alerts unique to this Situation (TBD)
     * @throws Exception
     */
    public void closeSituation(Integer sitnId, Integer resolution) throws Exception {
        
        if(sitnId == null)
            throw new Exception("sitnId is not available.");
        
        if(resolution == null)
            resolution = 1;
        
        JSONObject json = new JSONObject();

        json.put("sitn_id", new Integer(sitnId));
        json.put("resolution", new Integer(resolution));

        RestCaller client = new RestCaller(baseUrl, user, pass, true, true, false, true);
        
        try {
            client.callPost("closeSituation", null, json.toString(), null, "application/json", null);
        } catch(Exception e) {
            Log.log.error(e.getMessage(), e);
            throw e;
        }
    }
    
    public Map<String, String> getCustomTimestamp() {
        
        Map<String, String> map = new HashMap<String, String>();
        
        map.put("locale", this.locale.toString());
        map.put("time_zone", this.timezone);
        map.put("date_format", this.format);
        
        return map;
    }
    
    /**
     * Set the user provided timestamp format for the API calls of maintenance mode.
     * This method can be only used when all the AT callers use the same set of timestamp across the RSRemote instance.
     * @param format: The custom format of the timestamp, e.g. 'yyyy-MM-dd'T'HH:mm:ss'Z''
     * @param locale: Use java.util.Locale class to provide a valid locale string, e.g. Locale.US.toString()
     * @param timeZone: The valid time zone string, e.g. 'PDT'
     * @return a string for status
     */
    public void setCustomTimestamp(String format, String locale, String timeZone) throws Exception {
        
        if(StringUtils.isBlank(format) || StringUtils.isBlank(locale) || StringUtils.isBlank(timeZone))
            throw new Exception("Failed: Parameters cannot be blank");
        
        this.format = format;
        this.timezone = timeZone;

        Locale validLocale = getValidLocale(locale);
        if(validLocale == null)
            throw new Exception("Failed to find valid locale: " + locale);
        
        this.locale = validLocale;
    }
    
    /**
     * Creates a maintenance window period that filters Alerts.
     * @param name: The name of the window
     * @param description: Description of the window
     * @param startTime: Time to start the window from (in Epoch seconds)
     * @param duration: Duration of the window in seconds
     * @param forwardAlerts: Do we forward the Alerts that are captured?
     * @param recurringPeriod: How many days/weeks/months to wait before this recurs
     * @param recurringPeriodUnits: Decides what the recurring period counts in 0 = minutes, 1 = hours, 2 = days, 3 = weeks, 4 = months
     * @return The id of the maintenance window being created
     * @throws Exception
     */
    
    public void createMaintenanceWindow(String name, String description, String filter, String startTime, long duration, boolean forwardAlerts, Integer recurringPeriod, Integer recurringPeriodUnits) throws Exception {

        if(startTime == null)
            throw new Exception("startTime is not available.");
        
        try {
            SimpleDateFormat sdf = new SimpleDateFormat(format, locale);
            sdf.setTimeZone(TimeZone.getTimeZone(timezone));           
            long time = sdf.parse(startTime).getTime();
            
            createMaintenanceWindow(name, description, filter, time, duration, forwardAlerts, recurringPeriod, recurringPeriodUnits);
        } catch(Exception e) {
            Log.log.error(e.getMessage(), e);
            throw e;
        }
    }
    
    public void createMaintenanceWindow(String name, String description, String filter, long startTime, long duration, boolean forwardAlerts, Integer recurringPeriod, Integer recurringPeriodUnits) throws Exception {
        
        if(name == null)
            throw new Exception("name is not available.");
        
        if(description == null)
            description = "";
        
        JSONObject json = new JSONObject();

        json.put("name", name);
        json.put("description", description);
        json.put("filter", filter);
        json.put("start_date_time", startTime);
        json.put("duration", duration);
        json.put("forward_alerts", forwardAlerts);
        
        if(recurringPeriod != null && recurringPeriodUnits != null) {
            json.put("recurring_period", recurringPeriod);
            json.put("recurring_period_units", recurringPeriodUnits);
        }

        RestCaller client = new RestCaller(baseUrl, user, pass, true, true, false, true);
        
        try {
            client.callPost("createMaintenanceWindow", null, json.toString(), null, "application/json", null); 
        } catch(Exception e) {
            Log.log.error(e.getMessage(), e);
            throw e;
        }
    }
    
    /**
     * Get maintenance windows based on window id  and how many to fetch.
     * @param start: The start point for where to fetch windows from (ie, 0 to start at the first, 10 to start at the 11th)
     * @param limit: The number of windows to fetch
     * @return A JSON string with a list of maintenance windows returned from Graze API
     * @throws Exception
     */
    public String getMaintenanceWindows(int startId, int limit) throws Exception {

        String maintenaceWindows = null;
        
        RestCaller client = new RestCaller(baseUrl, user, pass, true, true, false, true);
        
        try {
            StringBuilder sb = new StringBuilder();
            sb.append("getMaintenanceWindows?start=").append(startId).append("&limit=").append(limit);
            
            maintenaceWindows = client.callGet(sb.toString(), null, null);
        } catch(Exception e) {
            Log.log.error(e.getMessage(), e);
            throw e;
        }

        if(StringUtils.isBlank(maintenaceWindows))
            return null;

        Log.log.debug(maintenaceWindows);
        
        return maintenaceWindows;
    }
    
    /**
     * Delete Maintenance Window by id
     * @param id: The id of the window to delete
     * @throws Exception
     */
    public void deleteMaintenaceWindow(int id) throws Exception {

        JSONObject json = new JSONObject();

        json.put("id", id);

        RestCaller client = new RestCaller(baseUrl, user, pass, true, true, false, true);
        
        try {
            client.callPost("deleteMaintenanceWindow", null, json.toString(), null, "application/json", null); 
        } catch(Exception e) {
            Log.log.error(e.getMessage(), e);
            throw e;
        }
    }
    
    private Locale getValidLocale(String locale) {

        Locale validLocale = null;
        Locale[] locales = Locale.getAvailableLocales();
        
        for(int i=0; i<locales.length; i++) {
            if(locales[i].toString().equals(locale)) {
                validLocale = locales[i];
                break;
            }
        }
        
        return validLocale;
    }
    
    private static void main1(String[] args) {
//    public static void main(String[] args) {
        
        try {
            MoogsoftGatewayAPI api = new MoogsoftGatewayAPI();
/*            
            // 1. Authentication
            String token = api.authenticate();
            System.out.println("token = " + token);
*/
            // 2. Get alert details
            String alertDetails = api.getAlertDetails(10);
            System.out.println("alertDetails = " + alertDetails);

            // 3. Update alert
            api.updateAlert(10, "Automation Completed", null, "adfdas");
            
            // 4. Create situation
            Integer sitnId = api.createSituation("asdfa");
            System.out.println("sitnId = " + sitnId);

            // 5. Add alert to situation
            api.addAlertToSituation(10, sitnId);

            // 6. Get situation details
            String situationDetails = api.getSituationDetails(sitnId);
            System.out.println("situationDetails = " + situationDetails);
            
            // 7. Update single situation
            String resultURL = "https://dcao.cloud.resolvesys.com:8443/resolve/jsp/rsclient.jsp#RS.worksheet.Worksheet/id=77e5f3a0c1f74d2e9bffe146e64b9070&setActive=true";
            api.updateSituation(sitnId, "Automation Completed", resultURL, "adfdas");
            
            // 8. Update all situations for one alert
            api.updateAllSituations(10, "Automation Completed", resultURL, "all");
            
            // 9. Update situation thread
            api.addSituationThread(10, "thread 1", "afda");
            
            // 10. Close situation
            api.closeSituation(sitnId, 1);
            
            // 11. Create a maintenance window
            String filter = "{\"op\":0,\"column\":\"source\",\"type\":\"LEAF\",\"value\":\"SE102176\"}";
            api.createMaintenanceWindow("se102176-2", "Putting target server in maintenance mode", filter, 1496876400, 1814400, true, null, null);

            // 12. Get maintenance windows
            String maintenaceWindows = api.getMaintenanceWindows(1, 10);
            System.out.println(maintenaceWindows);
            
            // 13. Delete a maintenance window
            api.deleteMaintenaceWindow(80);
        } catch(Exception e) {
            e.printStackTrace();
        }
    }
    
} // class MoogsoftGatewayAPI