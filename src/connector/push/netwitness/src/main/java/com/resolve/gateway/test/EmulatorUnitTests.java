package com.resolve.gateway.test;

import org.junit.BeforeClass;
import org.junit.runner.RunWith;
import org.junit.runners.Suite;
import org.junit.runners.Suite.SuiteClasses;

import com.resolve.gateway.push.netwitness.NetwitnessGateway;
import com.resolve.gateway.push.netwitness.NetwitnessGatewayAPI;


@RunWith(Suite.class)
@SuiteClasses({NetwitnessGateway.class, NetwitnessGatewayAPI.class })
public class EmulatorUnitTests
{
    
    @BeforeClass
    public static void setUpClass() {      
        System.out.println("TestCase1 setup");
    }

}

