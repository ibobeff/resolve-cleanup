package com.resolve.gateway.push.netwitness;

import java.util.Map;
import java.util.Iterator;

import com.resolve.gateway.Filter;
import com.resolve.gateway.resolvegateway.push.PushGatewayFilter;
import com.resolve.gateway.AbstractGatewayAPI;
import com.resolve.util.CryptUtils;
import com.resolve.util.Log;
import com.resolve.util.StringUtils;

public class NetwitnessGatewayAPI extends AbstractGatewayAPI
{
    private static String HOSTNAME = "";
    private static String PORT = "";
    private static String URI = "";
    
    private String user = "";
    private String pass = "";
    
    private String filterId = null;
    
    private static String baseUrl = null;

    private static NetwitnessGateway gateway = NetwitnessGateway.getInstance();
    
    public static NetwitnessGatewayAPI instance = new NetwitnessGatewayAPI();
    
    private static Map<String, Object> systemProperties = null;

    // Sample code for composing the base URL for REST or SOAP web service call to connect to the third-party server
    private NetwitnessGatewayAPI() {
        
        try {
            systemProperties = gateway.loadSystemProperties("Sample");
            
            HOSTNAME = (String)systemProperties.get("HOST");
            PORT = (String)systemProperties.get("PORT");
            user = (String)systemProperties.get("USER");
            pass = (String)systemProperties.get("PASS");
            
            StringBuffer sb = new StringBuffer();
            sb.append("https://").append(HOSTNAME);
            if(StringUtils.isNotBlank(PORT))
                sb.append(":").append(PORT);
            sb.append(URI);
            
            baseUrl = sb.toString();
        } catch(Exception e) {
            Log.log.error(e.getMessage(), e);
        }
    }
    
    // Public method to get an instance from this Singleton class
    // Do not modify
    public static NetwitnessGatewayAPI getInstance() {
        
        if(instance == null)
            instance = new NetwitnessGatewayAPI();
        
        return instance;
    }
    
    /**
     * Load parameters and initialize the Sample gateway API based on the filter specific information.
     * @param filterId: the name of the filter specified in the UI. This filter must be deployed.
     * @throws Exception
     */
    public static NetwitnessGatewayAPI getInstance(String filterId) throws Exception {
        
        if(instance == null)
            instance = new NetwitnessGatewayAPI(filterId);
        
        return instance;
    }
    
    // Singleton constructor to take the filter name to load the filter fields 
    // if some of the filter attributes are not available, the sample code shows how to fall back to the 
    // default configuration on the gateway level in the blueprint.properties
    private NetwitnessGatewayAPI(String filterId) throws Exception {
        
        Map<String, Filter> filters = gateway.getFilters();
        PushGatewayFilter filter = (PushGatewayFilter)filters.get(filterId);
        
        if(filter == null)
            throw new Exception("Filter not found or not deployed with filter id: " + filterId);
        
        this.filterId = filterId;
        
        try {
            Map<String, Object> attrs = filter.getAttributes();
            
            if(attrs != null) {
                for(Iterator<String> it=attrs.keySet().iterator(); it.hasNext();) {
                    String key = it.next();
                    switch(key) {
                        case "uusername":
                            String username = (String)attrs.get(key);
                            user = StringUtils.isBlank(username)?(String)systemProperties.get("USERNAME"):username;
                            break;
                        case "upassword":
                            String passcode = (String)attrs.get(key);
                            pass = StringUtils.isBlank(passcode)?(String)systemProperties.get("PASSWORD"):CryptUtils.decrypt(passcode);
                            break;
                        default:
                            break;
                    }
                }
            }
            
            Log.log.debug("pass = " + pass);
            
            StringBuffer sb = new StringBuffer();
            sb.append("https://").append(HOSTNAME);
            if(StringUtils.isNotBlank(PORT))
                sb.append(":").append(PORT);
            sb.append(URI);
            
            baseUrl = sb.toString();
        } catch(Exception e) {
            Log.log.error(e.getMessage(), e);
        }
    }

    // Add more APIs for this specific gateway using object instance methods to be called by getInstance() or getInstance(filterId) methods
    // instead of publish static methods that will only work with the default constrcutor
    
} // class SamplePushGatewayAPI