package com.resolve.gateway.push.netwitness;

import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

import com.resolve.gateway.Filter;
import com.resolve.gateway.resolvegateway.push.ConfigReceivePushGateway;
import com.resolve.gateway.resolvegateway.push.HttpServer;
import com.resolve.gateway.resolvegateway.push.PushGateway;
import com.resolve.gateway.resolvegateway.push.PushGatewayFilter;
import com.resolve.util.Log;

public class NetwitnessGateway extends PushGateway
{
    private static volatile NetwitnessGateway instance = null;

    // This is the default Jetty server instance to be held in the specific gateway object
    // Replace the HttpServer class to any other server class if it's not HTTP Server.
    private static volatile HttpServer defaultHttpServer;

    // User can configure multiple Jetty server instances to listen to different ports and cached them in memory
    private Map<Integer, HttpServer> httpServers = new HashMap<Integer, HttpServer>();

    // Each Jetty server instance may have multiple servlets running, each of which is correspondent to a different filter name
    private Map<String, HttpServer> deployedServlets = new HashMap<String, HttpServer>();

    // Singleton constructor to take the blueprint configuration and initialize the deployed filters for this specific gateway
    // Do not modify
    private NetwitnessGateway(ConfigReceivePushGateway config) {

        super(config);

        String gatewayName = this.getClass().getSimpleName();

        int index = gatewayName.indexOf("Gateway");
        if(index != -1)
            name = gatewayName.substring(0, index);

        // Get all the push filters available from the super class
        Map<String, PushGatewayFilter> pushFilters = getPushFilters();

        // If any of the push filters is belonged to this specific gateway, cache it in the memory of this gateway object
        for(Iterator<String> iterator=pushFilters.keySet().iterator(); iterator.hasNext();) {
            String filterName = iterator.next();
            PushGatewayFilter filter = pushFilters.get(filterName);

            if(name.indexOf(((PushGatewayFilter)filter).getGatewayName()) != -1) {
                if(filters.get(filterName) == null) {
                    filters.put(filterName, pushFilters.get(filterName));

                    if(filter.isDeployed()) {
                        orderedFilters.add(filter);
                        orderedFiltersMapById.put(filter.getId(), filter);
                    }
                }
            }
        }

        ConfigReceivePushGateway.getGateways().put(name, this);
    }

    // Public method to get an instance from this Singleton class
    // Do not modify
    public static NetwitnessGateway getInstance(ConfigReceivePushGateway config) {

        if (instance == null) {
            instance = new NetwitnessGateway(config);
        }

        return instance;
    }

    // This method is called after the instance is instantiated.
    // Do not modify
    public static NetwitnessGateway getInstance() {

        if (instance == null)
            Log.log.warn("Netwitness Push Gateway is not initialized correctly..");

        return instance;
    }

    // Access methods for the cached Jetty server resource information
    // Do not modify
    public HttpServer getDefaultHttpServer()
    {
        return defaultHttpServer;
    }

    public Map<Integer, HttpServer> getHttpServers()
    {
        return httpServers;
    }

    public Map<String, HttpServer> getDeployedServlets()
    {
        return deployedServlets;
    }

    public String getInputKey()
    {
        return inputKey;
    }

	// If the unique key name is not "alert_id" from the result set, change the input key name here.
    public void setInputKey(String inputKey)
    {
        
        this.inputKey = inputKey;
    }

    // Initialize the gateway with the default HTTP server instance and start to listen on the default port
    // Do not remove the existing code, but you can add additional logic for initialization if necessary
    @Override
    protected void initialize() {

        ConfigReceivePushGateway config = (ConfigReceivePushGateway)configurations;
        queue = config.getQueue().toUpperCase();

        try {
            Log.log.info("Initializing Listener to queue: " + queue);
            this.gatewayConfigDir = "/config/" + queue.toLowerCase() + "/";

            defaultHttpServer = new HttpServer(config, null, null, name);
            defaultHttpServer.init();
            defaultHttpServer.start();

            if (defaultHttpServer.isStarted())
                defaultHttpServer.addDefaultResolveServlet();

            // Add additional custom logic here
        } catch (Exception e) {
            Log.log.error("Failed to config Netwitness Push Gateway: " + e.getMessage(), e);
        }
    }

@Override
    public void reinitialize() {

        ConfigReceivePushGateway config = (ConfigReceivePushGateway)configurations;
        queue = config.getQueue().toUpperCase();

        try {
            Log.log.info("Initializing Listener to queue: " + queue);
            this.gatewayConfigDir = "/config/" + queue.toLowerCase() + "/";

            defaultHttpServer = new HttpServer(config, null, null, name);
            defaultHttpServer.init();
            defaultHttpServer.start();

            if (defaultHttpServer.isStarted())
                defaultHttpServer.addDefaultResolveServlet();

            ConfigReceivePushGateway.getGateways().put(name, this);
        } catch (Exception e) {
            Log.log.error("Failed to config MoogSoft Gateway: " + e.getMessage(), e);
        }
    }
    @Override
    public void deactivate() {

        try {
            defaultHttpServer.stop();
        } catch (Exception e)  {
            Log.log.error(e.getMessage(), e);
        }

        Map<Integer, HttpServer> httpServers = getHttpServers();

        for (HttpServer httpServer : httpServers.values()) {
            try {
                httpServer.stop();
            } catch (Exception e) {
                Log.log.error(e.getMessage(), e);
            }
        }

        httpServers.clear();

        super.deactivate();
    }

    // The gateway will start to run by calling the super class common logic and start all the filter servlets to listen to the HTTP ports
    @Override
    public void start() {

        Log.log.debug("Starting Netwitness Push Gateway");

        super.start();

     	// Add additional custom logic here
    }

    // This method will be called when the gateway is gracefully shutdown
    @Override
    public void stop() {

        Log.log.warn("Stopping Netwitness Push gateway");

        try {
            getDefaultHttpServer().stop();
        } catch (Exception e)  {
            Log.log.error(e.getMessage(), e);
        }

        Map<Integer, HttpServer> httpServers = getHttpServers();

        for (HttpServer httpServer : httpServers.values()) {
            try {
                httpServer.stop();
            } catch (Exception e) {
                Log.log.error(e.getMessage(), e);
            }
        }

        httpServers.clear();

		// Add custom logic here

        super.stop();

    }

    // This method will be called when a filter is deployed to validate the semantics of the field only
    // The syntax of the filter fields have been validated when the filter is defined and saved from UI
    // including whether the filter field is required or not per gateway definition in blueprint.properties
    @Override
    public int validateFilterFields(Map<String, Object> params) {

        int code = 0;

        // Add custom logic here

        return code;
    }

    // The method returns the error string that is associated with the error code defined
    // under the custom logic being implemented
    @Override
    public String getValidationError(int errorCode) {

		String error = null;

		// Add custom logic here

        return error;
    }

    // This method is used by the system
    // Do not modify
    @Override
    protected String getMessageHandlerName() {

        return this.getClass().getName();
    }

    // This method is used by the system
    // Do not remove
    @Override
    protected void cleanFilter(ConcurrentHashMap<String, Filter> originalFilters) {
        //Add customized code here to modify your server when you clear out the deployed filters;

        for(Filter filter : originalFilters.values())
        {
            PushGatewayFilter pushGatewayFilter = (PushGatewayFilter) filter;
            String id = pushGatewayFilter.getId();

            try
            {
                if(pushGatewayFilter.getPort() > 0)
                {
                    HttpServer httpServer = httpServers.get(pushGatewayFilter.getPort());
                    httpServer.removeServlet(pushGatewayFilter.getId());
                    //if there is no filter/servlet remaining then stop the server on the port
                    if(httpServer.getServlets().isEmpty())
                    {
                        httpServer.stop();
                        httpServers.remove(pushGatewayFilter.getPort());
                    }
                }
                else
                {
                    //undeploy the servlet from the default server.
                    defaultHttpServer.removeServlet(id);
                }
            }
            catch (Exception e)
            {
                Log.log.error(e.getMessage(), e);
            }
        }
    }

    
    // If the common logic in the super class does not support the use case for this specific gateway,
    // you may create override methods to replace the logic in the super class

    @Override
    public void initServers() {

        Map<Integer, HttpServer> httpServers = getHttpServers();
        Map<String, HttpServer> deployedServlets = getDeployedServlets();

        try {
            for (Filter deployedFilter : orderedFilters) {
                PushGatewayFilter filter = (PushGatewayFilter)deployedFilter;
/*                String gatewayName = filter.getGatewayName();
                PushGateway gateway = ConfigReceivePushGateway.getGateways().get(gatewayName);*/

                if (filter.getPort() == null || filter.getPort() <= 0) {
                    addDefaultServlet(filter);
                }

                else {
                    HttpServer httpServer = httpServers.get(filter.getPort());

                    if (httpServer == null)
                    {
                        HttpServer httpServer1 = deployedServlets.get(filter.getId());

                        if (httpServer1 != null)
                        {
                            //port changed
                            httpServer1.removeServlet(filter.getId());
                            getDeployedServlets().remove(filter.getId());
                        }

                        httpServer = new HttpServer((ConfigReceivePushGateway)configurations, filter.getPort(), filter.isSsl(), filter.getGatewayName());
                        httpServer.init();
                        httpServer.addServlet(filter);
                        httpServer.start();
                    }
                    else
                    {
                        httpServer.addServlet(filter);
                    }

                    httpServers.put(filter.getPort(), httpServer);
                    deployedServlets.put(filter.getId(), httpServer);
                }
            }
        }
        catch (Exception e)
        {
            Log.log.error(e.getMessage(), e);
        }
    }

    protected void removeFilters(List<Filter> undeployedFilters) {

        for(Filter undeployedFilter : undeployedFilters)
        {
            PushGatewayFilter filter = (PushGatewayFilter)undeployedFilter;
            String id = filter.getId();

            try
            {
                if(filter.getPort() > 0)
                {
                    HttpServer httpServer = httpServers.get(filter.getPort());
                    httpServer.removeServlet(filter.getId());
                    //if there is no filter/servlet remaining then stop the server on the port
                    if(httpServer.getServlets().isEmpty())
                    {
                        httpServer.stop();
                        httpServers.remove(filter.getPort());
                    }
                }
                else
                {
                    //undeploy the servlet from the default server.
                    defaultHttpServer.removeServlet(id);

                    filters.remove(id);
                }
            }
            catch (Exception e)
            {
                Log.log.error(e.getMessage(), e);
            }
        }
    }
    
    // If the common logic in the super class does not support the use case for this specific gateway,
    // you may create override methods to replace the logic in the super class

} // class NetwitnessGateway
