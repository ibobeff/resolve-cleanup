package com.resolve.gateway;

import java.util.Map;
import com.resolve.gateway.ibmmq.IBMmqFilter;
import com.resolve.gateway.ibmmq.IBMmqGateway;

public class MIBMmq extends MGateway {

    private static final String RSCONTROL_SET_FILTERS = "MIBMmq.setFilters";

    private static final IBMmqGateway instance = IBMmqGateway.getInstance();

    public MIBMmq() {
        super.instance = instance;
    }

    protected String getSetFilterMethodName() {
        return RSCONTROL_SET_FILTERS;
    }

    /**
     * Populates the {@link Filter} data into the given filterMap {@link Map} instance
     * 
     * @param filterMap
     *      the {@link Map} instance to hold the {@link Filter} data
     * @param filter
     *      a {@link IBMmqFilter} instance
     *            
     */
    public void populateGatewaySpecificFilterProperties(Map<String, String> filterMap, Filter filter) {
        if (instance.isActive()) {
            IBMmqFilter ibmmqFilter = (IBMmqFilter) filter;
            filterMap.put(IBMmqFilter.TRANSPORT, ibmmqFilter.getTransport());
            filterMap.put(IBMmqFilter.PORT, "" + ibmmqFilter.getPort());
            filterMap.put(IBMmqFilter.MANAGER, ibmmqFilter.getManager());
            filterMap.put(IBMmqFilter.ACK, ibmmqFilter.getAck());
            filterMap.put(IBMmqFilter.NAME, ibmmqFilter.getName());
            filterMap.put(IBMmqFilter.TYPE, ibmmqFilter.getType());
            filterMap.put(IBMmqFilter.CHANNEL, ibmmqFilter.getChannel());
            filterMap.put(IBMmqFilter.DURABLE, "" + ibmmqFilter.getDurable());
        }
    }
}

