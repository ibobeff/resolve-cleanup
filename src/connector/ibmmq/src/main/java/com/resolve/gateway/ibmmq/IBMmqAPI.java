/******************************************************************************
* (C) Copyright 2014
*
* Resolve Systems, LLC
*
* All rights reserved
* This software is distributed under license and may not be used, copied,
* modified, or distributed without the express written permission of
* Resolve Systems, LLC.
*
******************************************************************************/

package com.resolve.gateway.ibmmq;

import com.resolve.util.Log;
import com.resolve.util.StringUtils;

import com.ibm.jms.JMSTextMessage;
import com.ibm.mq.jms.*;

import java.math.BigDecimal;
import java.util.concurrent.Callable;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.FutureTask;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.TimeoutException;

import javax.jms.*;

import com.resolve.gateway.AbstractGatewayAPI;
import com.resolve.gateway.Filter;

public class IBMmqAPI extends AbstractGatewayAPI
{
    private String ip;
    private String user;
    private String pass;
    
    private int port;
    private String queueManager;
    private String channel;
    private String type;
    private String name;
    private String ack;
    private String transport;
    private Boolean durable;
    private Boolean transacted;
    
    private static IBMmqGateway instance = IBMmqGateway.getInstance();
    
    /**
     * Constructor for using the API instance object to call functional APIs with the parameters defined by filter id
     * @param filterId
     * @throws Exception
     */
    public IBMmqAPI(String filterName) throws Exception {
        
        if(StringUtils.isBlank(filterName))
            throw new Exception("Filter name is not available.");
        
        IBMmqFilter filter = getFilter(filterName);
        if(filter == null)
            throw new Exception("Filter is not found.");
        
        this.ip = instance.getIp();
        this.user = instance.getUser();
        this.pass = instance.getPass();

        this.port = filter.getPort();
        this.queueManager = filter.getManager();
        this.channel = filter.getChannel();
        this.type = filter.getType();
        this.name = filter.getName();
        this.ack = filter.getAck();
        this.transport = filter.getTransport();
        this.durable = filter.getDurable();
        this.transacted = false;
    }
    
    /**
     * Constructor for using the API instance object to hold the system parameters to call functional APIs
     * 
     * @param ip
     * @param port
     * @param user
     * @param pass
     * @param queueManager
     * @param channel
     * @param type: Queue or Topic
     * @param name
     * @throws Exception
     */
    public IBMmqAPI(String ip, int port, String user, String pass, String queueManager, String channel, String type, String name) throws Exception {
        
        if(StringUtils.isBlank(ip))
            throw new Exception("Host is not available.");
        if(StringUtils.isBlank(queueManager))
            throw new Exception("Queue Manager is not available.");
        if(StringUtils.isBlank(channel))
            throw new Exception("Channel is not available.");
        if(StringUtils.isBlank(type))
            throw new Exception("Type is not available.");
        if(StringUtils.isBlank(name))
            throw new Exception("Queue/Topic name is not available.");
        
        if(StringUtils.isNotBlank(user) && StringUtils.isBlank(user))
            throw new Exception("User credential is not available.");
        
        if(port == 0)
            throw new Exception("Port is not available.");
        
        this.ip = ip;
        this.user = user;
        this.pass = pass;
        
        this.port = port;
        this.queueManager = queueManager;
        this.channel = channel;
        this.type = type;
        this.name = name;
        
        this.ack = MQService.AUTO_ACKNOWLEDGE;
        this.transport = MQService.TRANSPORT_CLIENT;
        this.durable = false;
        this.transacted = false;
    }
    
    /**
     * Constructor for using the API instance object to specify more system configuration parameters to call functional APIs
     * 
     * @param ip
     * @param user
     * @param pass
     * @param port
     * @param queueManager
     * @param channel
     * @param type: Queue or Topic
     * @param name
     * @param ack: MQService.AUTO_ACKNOWLEDGE or MQService.CLIENT_ACKNOWLEDGE
     * @param transport: TRANSPORT_CLIENT or TRANSPORT_BINDINGS
     * @param durable: durable subscription or non-durable subscription
     * @throws Exception
     */
    public IBMmqAPI(String ip, String user, String pass, int port, String queueManager, String channel, String type, String name, String ack, String transport, Boolean durable) throws Exception {
        
        if(StringUtils.isBlank(ip))
            throw new Exception("Host is not available.");
        if(StringUtils.isBlank(queueManager))
            throw new Exception("Queue Manager is not available.");
        if(StringUtils.isBlank(channel))
            throw new Exception("Channel is not available.");
        if(StringUtils.isBlank(type))
            throw new Exception("Type is not available.");
        if(StringUtils.isBlank(name))
            throw new Exception("Queue/Topic name is not available.");
        
        if(StringUtils.isNotBlank(user) && StringUtils.isBlank(user))
            throw new Exception("User credential is not available.");
        
        if(port == 0)
            throw new Exception("Port is not available.");
        
        this.ip = ip;
        this.user = user;
        this.pass = pass;
        
        this.port = port;
        this.queueManager = queueManager;
        this.channel = channel;
        this.type = type;
        this.name = name;
        this.ack = ack;
        this.transport = transport;
        this.durable = durable;
    }
    
    /**
     * Get filter object by filter name
     * @param name
     * @return filter instance
     */
    public IBMmqFilter getFilter(String name) {
        
        IBMmqFilter filterFound = null;
        
        for(Filter filter:instance.getOrderedFilters()) {
            if(filter instanceof IBMmqFilter) {
                if(filter.getId().equals(name))
                    filterFound = (IBMmqFilter)filter;
                    break;
            }
        }
        
        return filterFound;
    }
    
    /**
     * Send plain text messages
     * @param msg
     * @throws Exception
     */
    public void sendMessage(String msg) throws Exception {
        sendMessageWithEncoding(msg, "");
    }

    /**
     * Send plain text messages or send binary messages with specific encoding, e.g., UTF-8
     * 
     * @param msg
     * @param encoding
     * @throws Exception
     */
    public void sendMessageWithEncoding(String msg, String encoding) throws Exception {
        
        QueueConnection connection = null;
        MQQueueSession session = null;
        MessageProducer producer = null;
        Message message = null;
                        
        try {
//          JMSContext context = (JMSContext)mqqcf.createContext();
//          Queue queue = new MQQueue(queueName);
//          context.createProducer().send(queue, text);
            
            connection = MQService.instance.getQueueConnection(ip, user, pass, port, queueManager, channel, transport);
            
            if(StringUtils.isBlank(ack) || ack.equals(MQService.AUTO_ACKNOWLEDGE))
                session = (MQQueueSession)connection.createQueueSession(transacted, Session.AUTO_ACKNOWLEDGE);
            
            else if(ack.equals(MQService.CLIENT_ACKNOWLEDGE))
                session = (MQQueueSession)connection.createQueueSession(transacted, Session.CLIENT_ACKNOWLEDGE);
            
            if(StringUtils.isBlank(encoding))
                message = session.createTextMessage(msg);
            
            else {
                message = session.createBytesMessage();
                ((BytesMessage)message).writeBytes(msg.getBytes(encoding));
                if(encoding.equals("UTF-8"))
                    ((BytesMessage)message).writeUTF(msg);
            }
            
            Queue queue = MQService.instance.getQueue(name);
            producer = session.createProducer(queue);
            producer.send(message);
            
            Log.log.debug("Sending message: " + message.toString());
        } catch(Exception e) {
            Log.log.error(e.getMessage(), e);
            throw e;
        } finally {
            try {
                if(producer != null)
                    producer.close();
                if(session != null)
                    session.close();
                if(connection != null)
                    connection.close();
            } catch(Exception e) {
                Log.log.error(e.getMessage(), e);
            }
        }
    } // sendMessageWithEncoding
    
    /**
     * Send binary messages
     * 
     * @param bytes
     * @throws Exception
     */
    public void sendBinaryMessage(byte[] bytes) throws Exception {
        
        QueueConnection connection = null;
        MQQueueSession session = null;
        MessageProducer producer = null;
        
        try {
//          JMSContext context = (JMSContext)mqqcf.createContext();
//          Queue queue = new MQQueue(queueName);
//          context.createProducer().send(queue, text);
            
            connection = MQService.instance.getQueueConnection(ip, user, pass, port, queueManager, channel, transport);
            
            if(StringUtils.isBlank(ack) || ack.equals(MQService.AUTO_ACKNOWLEDGE))
                session = (MQQueueSession)connection.createQueueSession(transacted, Session.AUTO_ACKNOWLEDGE);
            
            else if(ack.equals(MQService.CLIENT_ACKNOWLEDGE))
                session = (MQQueueSession)connection.createQueueSession(transacted, Session.CLIENT_ACKNOWLEDGE);
            
            BytesMessage message = session.createBytesMessage();
            message.writeBytes(bytes);
//          message.setStringProperty("TEST_NAME", "runQueueSendRollback");
//          message.setIntProperty("TEST_PERSISTENCE", persistence);
//          message.setBooleanProperty("TEST_EXPLICIT", explicit);
            
            Queue queue = MQService.instance.getQueue(name);
            producer = session.createProducer(queue);
            producer.send(message);
            
            Log.log.debug("Sending binary message.");
        } catch(Exception e) {
            Log.log.error(e.getMessage(), e);
            throw e;
        } finally {
            try {
                if(producer != null)
                    producer.close();
                if(session != null)
                    session.close();
                if(connection != null)
                    connection.close();
            } catch(Exception e) {
              Log.log.error(e.getMessage(), e);
            }
        }
    } // sendBinaryMessage
    
    /**
     * Get bytes from a BytesMessage.
     * 
     * @param message: must be BytesMessage type of message
     * @return
     * @throws Exception
     */
    public byte[] getByteMessage(Message message) throws Exception {
        
        if(message == null)
            return null;
        
        byte[] bytes = null;

        if(message instanceof BytesMessage) {
            BytesMessage bm = (BytesMessage)message;
            long length = bm.getBodyLength();
            try {
                int len = (new BigDecimal(length)).intValue();
                bytes = new byte[len];
                ((BytesMessage)message).readBytes(bytes);
            } catch(Exception e) {
                // TBD: handle long length of byte[].
            }
        }
        
        else
            throw new Exception("Wrong message type expected.");
        
        return bytes;
    } // getByteMessage
    
    /**
     * Get plain text string from a Text Message
     * 
     * @param message: must be TextMessage type of message
     * @return
     * @throws Exception
     */
    public String getTextMessage(Message message) throws Exception {
        
        if(message == null)
            return null;
        
        String msgBody = null;

        if(message instanceof TextMessage) {
            msgBody = ((TextMessage)message).getText();
        }
        
        else
            throw new Exception("Wrong message type expected.");
        
        return msgBody;
    } // getTextMessage
    
    /**
     * Receive a message from the queue.
     * 
     * @param waitTime: no message will be received if the wait time is exceeded
     * @return
     * @throws Exception
     */
    public Message receiveMessage(int waitTime) throws Exception {
        
        ExecutorService executor = Executors.newFixedThreadPool(1);
        
        QueueConnection connection = null;
        MQQueueSession session = null;
        MessageConsumer consumer = null;
        
        Message message = null;
        
        try {
            connection = MQService.instance.getQueueConnection(ip, user, pass, port, queueManager, channel, transport);
            
            if(StringUtils.isBlank(ack) || ack.equals(MQService.AUTO_ACKNOWLEDGE))
                session = (MQQueueSession)connection.createQueueSession(transacted, Session.AUTO_ACKNOWLEDGE);
            
            else if(ack.equals(MQService.CLIENT_ACKNOWLEDGE))
                session = (MQQueueSession)connection.createQueueSession(transacted, Session.CLIENT_ACKNOWLEDGE);
            
            Queue queue = MQService.instance.getQueue(name);
            consumer = session.createConsumer(queue);
            
            ConsumerCallable callable = new ConsumerCallable(consumer);
            FutureTask<Message> futureTask = new FutureTask<Message>(callable);
            
            try {
                executor.execute(futureTask);
                
                while (!futureTask.isDone()) {
                    try {
                        Log.log.debug("Waiting for consumer to receive.");
                        message = futureTask.get(waitTime, TimeUnit.SECONDS);
                    } catch (InterruptedException | ExecutionException e) {
                        Log.log.error(e.toString());
                        break;
                    } catch(TimeoutException e) {
                        futureTask.cancel(true);
                        Log.log.error(e.toString());
                        throw new Exception("Receive() is timed out.");
                    }
                }
            } catch(Exception e) {
                Log.log.error(e.toString());
                throw e;
            }
            Log.log.debug("Message received: " + message.toString());
        } catch(Exception e) {
            Log.log.error(e.getMessage(), e);
            throw e;
        } finally {
            try {
                if(consumer != null)
                    consumer.close();
                if(session != null)
                    session.close();
                if(connection != null)
                    connection.close();
                if(executor != null)
                    executor.shutdown();
            } catch(Exception e) {
                Log.log.error(e.getMessage(), e);
            }
        }
     
        return message;
    } // receiveMessage
    
    /**
     * Publish a plain text message to a topic.
     * 
     * @param msg: must be TextMessage type of message
     * @throws Exception
     */
    public void publishMessage(String msg) throws Exception {
        
        MQTopicConnection connection = null;
        TopicSession session = null;
        MessageProducer producer = null;
        
        try {
            connection = MQService.instance.getNewTopicConnection(ip, user, pass, port, queueManager, channel, transport, false);
            
            if(StringUtils.isBlank(ack) || ack.equals(MQService.AUTO_ACKNOWLEDGE))
                session = (MQTopicSession)connection.createTopicSession(transacted, Session.AUTO_ACKNOWLEDGE);
            
            else if(ack.equals(MQService.CLIENT_ACKNOWLEDGE))
                session = (MQTopicSession)connection.createTopicSession(transacted, Session.CLIENT_ACKNOWLEDGE);
            
            MQTopic topic = (MQTopic)session.createTopic(name);
            producer = session.createProducer(topic);
            JMSTextMessage message = (JMSTextMessage)session.createTextMessage(msg);
            producer.send(message);
            
//            Log.log.debug("Message is published.");
//            Log.log.debug(msg);
        } catch(Exception e) {
            e.printStackTrace();
            Log.log.error(e.getMessage(), e);
            throw e;
        } finally {
            try {
                if(producer != null)
                    producer.close();
                if(session != null)
                    session.close();
                if(connection != null)
                    connection.close();
            } catch(Exception e) {
                e.printStackTrace();
                Log.log.error(e.getMessage(), e);
            }
        }
    } // publishMessage
    
    /**
     * Publish a binary message to a topic.
     * 
     * @param bytes
     * @throws Exception
     */
    public void publishBinaryMessage(byte[] bytes) throws Exception {
        
        MQTopicConnection connection = null;
        TopicSession session = null;
        MessageProducer producer = null;
        
        try {
            connection = MQService.instance.getNewTopicConnection(ip, user, pass, port, queueManager, channel, transport, false);
            
            if(StringUtils.isBlank(ack) || ack.equals(MQService.AUTO_ACKNOWLEDGE))
                session = (MQTopicSession)connection.createTopicSession(transacted, Session.AUTO_ACKNOWLEDGE);
            
            else if(ack.equals(MQService.CLIENT_ACKNOWLEDGE))
                session = (MQTopicSession)connection.createTopicSession(transacted, Session.CLIENT_ACKNOWLEDGE);
            
            BytesMessage message = session.createBytesMessage();
            message.writeBytes(bytes);
            
            MQTopic topic = (MQTopic)session.createTopic(name);
            producer = session.createProducer(topic);
            producer.send(message);
            
//            Log.log.debug("Message is published.");
//            Log.log.debug(message);
        } catch(Exception e) {
            e.printStackTrace();
            Log.log.error(e.getMessage(), e);
            throw e;
        } finally {
            try {
                if(producer != null)
                    producer.close();
                if(session != null)
                    session.close();
                if(connection != null)
                    connection.close();
            } catch(Exception e) {
                e.printStackTrace();
                Log.log.error(e.getMessage(), e);
            }
        }
    } // publishBinaryMessage
    
    /**
     * Subscribe to a Topic from IBM MQ server with a filter created on Resolve IBMMQGaetway.
     * 
     * @param filterName
     * @throws Exception
     */
    void subscribeToTopic(String filterName) throws Exception {

        MQService.instance.startTopicSubscriber(ip, user, pass, port, queueManager, channel, name, ack, transport, durable, filterName);

    } // subscribeToTopic
    
    /**
     * Listen to a Queue from IBM MQ server via a filter created on Resolve IBMMQGaetway.
     * @param filterName
     * @throws Exception
     */
    void listenToQueue(String filterName) throws Exception {

        MQService.instance.startQueueReceiver(ip, user, pass, port, queueManager, channel, name, ack, transport, filterName);

    } // listenToQueue
    
    class ConsumerCallable implements Callable<Message> {
        
        private MessageConsumer consumer = null;
        
        ConsumerCallable(MessageConsumer consumer) {
            this.consumer = consumer;
        }
        
        public Message call() throws Exception {
            return consumer.receive();
        }
    }

} // IBMmqAPI