/******************************************************************************
* (C) Copyright 2014
*
* Resolve Systems, LLC
*
* All rights reserved
* This software is distributed under license and may not be used, copied,
* modified, or distributed without the express written permission of
* Resolve Systems, LLC.
*
******************************************************************************/

package com.resolve.gateway.ibmmq;

import com.resolve.util.Log;
import com.resolve.util.StringUtils;

import com.ibm.msg.client.wmq.*;

import com.ibm.jms.JMSBytesMessage;
import com.ibm.jms.JMSTextMessage;
import com.ibm.mq.jms.*;

import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import java.util.Set;
import java.util.concurrent.Callable;
import java.util.concurrent.ConcurrentHashMap;

import javax.jms.*;

public class MQService
{
    public static final String MSG_TYPE_TEXT = "TEXT";
    public static final String MSG_TYPE_BINARY = "BYTE";
    public static final String TRANSPORT_CLIENT = "WMQConstants.WMQ_CM_CLIENT";
    public static final String TRANSPORT_BINDINGS = "WMQConstants.WMQ_CM_BINDINGS";
    public static final String CLIENT_ACKNOWLEDGE = "CLIENT_ACKNOWLEDGE";
    public static final String AUTO_ACKNOWLEDGE = "AUTO_ACKNOWLEDGE";
    
    private static int retry = 5;
    private static int interval = 1000;
    
    public static final String defaultUser = System.getProperty("user.name");
    
    private static volatile Map<String, MQQueue> queues = new HashMap<String, MQQueue>();
    private static volatile Map<String, MQQueueConnection> qcs = new ConcurrentHashMap<String, MQQueueConnection>();
    private static volatile Map<String, MQTopicConnection> tcs = new ConcurrentHashMap<String, MQTopicConnection>();
    private static volatile Map<String, MQSubscriber> subscribers = new ConcurrentHashMap<String, MQSubscriber>();
    private static volatile Map<String, MQReceiver> receivers = new ConcurrentHashMap<String, MQReceiver>();

    public static final MQService instance = new MQService();

    private MQService() {}

    public static Map<String, MQSubscriber> getSubscribers()
    {
        return subscribers;
    }
 
    public MQQueueConnection getQueueConnection(String ip, String user, String pass, int port, String queueManager, String channel, String transport) throws Exception {
        
        MQQueueConnection connection = null;
    
        try {
            MQQueueConnectionFactory mqqcf = new MQQueueConnectionFactory();
                
            if(StringUtils.isBlank(transport) || transport.equals(TRANSPORT_CLIENT))
                mqqcf.setTransportType(WMQConstants.WMQ_CM_CLIENT);
            
            else if(StringUtils.isNotBlank(transport) && transport.equals(TRANSPORT_BINDINGS))
                mqqcf.setTransportType(WMQConstants.WMQ_CM_BINDINGS);
            
            mqqcf.setHostName(ip);
            mqqcf.setPort(port);
            mqqcf.setQueueManager(queueManager);
            mqqcf.setChannel(channel);

            if(StringUtils.isBlank(user))
                connection = (MQQueueConnection)mqqcf.createConnection();
            else if(!StringUtils.isBlank(pass))
                connection = (MQQueueConnection)mqqcf.createConnection(user, pass);
            else
                throw new Exception("Failed to connect to MQ server or not authorized to access.");
            
            connection.start();
        } catch(Exception e) {
            Log.log.error(e.getMessage(), e);
            throw e;
        } catch(Throwable t) {
            t.printStackTrace();
            Log.log.error(t.getMessage());
            throw new Exception("Runtime exception: " + t.getMessage());
        }

        return connection;
    } // getQueueConnection()

    public MQQueue getQueue(String queueName) {

        if(StringUtils.isBlank(queueName))
            return null;

        MQQueue queue = queues.get(queueName);
        if(queue == null) {
            try {
                queue = new MQQueue(queueName);
                queues.put(queueName, queue);
            } catch(Exception e) {
                Log.log.error(e.getMessage(), e);
            }
        }

        return queue;
    }

    // JMS 2.0 ===============================================================================

    public MQTopicConnection getTopicConnection(String ip, String user, String pass, int port, String queueManager, String channel, String transport, Boolean durable) throws Exception {
        
        String key = getConnectionKey(ip, user, queueManager, transport);
        
        MQTopicConnection connection = tcs.get(key);
        if(connection == null)
            connection = getNewTopicConnection(ip, user, pass, port, queueManager, channel, transport, durable);
        
        int i = 0;
        
        while(i < retry) {
            try {
                connection.start();
                break;
            } catch(Exception e) {
                Log.log.error(e.getMessage(), e);
                i++;
                Thread.sleep(interval);
                connection = getNewTopicConnection(ip, user, pass, port, queueManager, channel, transport, durable);
                continue;
            } catch(Throwable t) {
                t.printStackTrace();
                Log.log.error(t.getMessage());
                i++;
                Thread.sleep(interval);
                continue;
            }
        } // while()
        
        if(i == retry) {
            tcs.remove(key);
            throw new Exception("Failed to create topic connection.");
        }
        
        return connection;
    } // getTopicConnection

    public MQTopicConnection getNewTopicConnection(String ip, String user, String pass, int port, String queueManager, String channel, String transport, Boolean durable) throws Exception {
        
        MQTopicConnection connection = null;
        
        try {
            MQTopicConnectionFactory mqtcf = new MQTopicConnectionFactory();
        
            if(StringUtils.isBlank(transport) || transport.equals(TRANSPORT_CLIENT))
                mqtcf.setTransportType(WMQConstants.WMQ_CM_CLIENT);
            
            else if(StringUtils.isNotBlank(transport) && transport.equals(TRANSPORT_BINDINGS))
                mqtcf.setTransportType(WMQConstants.WMQ_CM_BINDINGS);
            
            mqtcf.setHostName(ip);
            mqtcf.setPort(port);
            mqtcf.setQueueManager(queueManager);
            mqtcf.setChannel(channel);

            if(StringUtils.isBlank(user))
                connection = (MQTopicConnection)mqtcf.createConnection();
            else if(!StringUtils.isBlank(pass))
                connection = (MQTopicConnection)mqtcf.createConnection(user, pass);
            else
                throw new Exception("User credential is not available.");

            // String clientId = connection.getClientID();
            if(durable) {
                if(StringUtils.isBlank(user))
                    user = defaultUser;
                connection.setClientID(user+"_"+queueManager);

                // When there are multiple filters with durable subscription in the same queue manager, the second one will throw the following exception by setting client ID again
                // even if getClientID() returns null because the connection reference is different. Caching the topic connection or cache the client id equivalently fixes this error.
                // com.ibm.msg.client.jms.DetailedInvalidClientIDException: JMSCC0111: WebSphere MQ classes for JMS attempted to set a pre-existing client ID on a Connection or JMSContext. 
                // An application attempted to set the client ID property of a valid Connection or JMSContext to the value 'resolve' but this value was already in use.
                // Caching clientId does not work because if a new connection is created, getClientId() returns null, but if call setClientId() on the new connection fails on the same error above
                // because there is only one physical connection per queue manager/user.
                
                tcs.put(getConnectionKey(ip, user, queueManager, transport), connection);
            }
        } catch(JMSException e) {
          Log.log.error(e.getMessage(), e);
//            if(e.getMessage().contains("JMSWMQ0018"))
//                IBMmqGateway.setAlive(false);
            throw e;
        } catch(Exception e) {
            Log.log.error(e.getMessage(), e);
            throw e;
        } catch(Throwable t) {
            t.printStackTrace();
            Log.log.error(t.getMessage());
            throw new Exception("Runtime exception: " + t.getMessage());
        }
        
        return connection;
    } // getNewTopicConnection
    
    private String getConnectionKey(String ip, String user, String queueManager, String transport) {
        
        StringBuilder key = new StringBuilder();
        key.append(ip).append(queueManager).append(transport);
        if(StringUtils.isNotBlank(user))
            key.append(user);
        
        return key.toString();
    }
/*
    public MessageProducer getTopicProducer(String queueManager, String channel, String topicName) {

        MessageProducer producer = null;

        try {
            MQTopicConnection connection = getTopicConnection(queueManager, channel);
            MQTopicSession session = (MQTopicSession)connection.createTopicSession(false, Session.AUTO_ACKNOWLEDGE);
            MQTopic topic = (MQTopic)session.createTopic(topicName);
            producer = session.createProducer(topic);
        } catch(Exception e) {
            e.printStackTrace();
            Log.log.error(e.getMessage(), e);
        }

        return producer;
    }
*/
    public void startTopicSubscriber(String ip, String user, String pass, int port, String queueManager, String channel, String topicName, 
                                             String ack, String transport, Boolean durable, String filterName) throws Exception {
        
        MQTopicConnection connection = null;
        MQTopicSession session = null;
        
        if(StringUtils.isBlank(topicName))
            throw new Exception("Topic name is not available.");
        
        String name = queueManager + "_" + topicName + "_" + filterName;

        MQSubscriber subscriber = subscribers.get(name);

        // This way cannot restart subscription after a server down
/*        if(subscriber != null) {
            subscriber.getSubscriber().setMessageListener(subscriber);
            
            return;
*/
        try {
            connection = getTopicConnection(ip, user, pass, port, queueManager, channel, transport, durable);
            
            if(StringUtils.isBlank(ack) || ack.equals(AUTO_ACKNOWLEDGE))
                session = (MQTopicSession)connection.createTopicSession(false, Session.AUTO_ACKNOWLEDGE);
            
            else if(ack.equals(CLIENT_ACKNOWLEDGE))
                session = (MQTopicSession)connection.createTopicSession(false, Session.CLIENT_ACKNOWLEDGE);
            
            subscriber = new MQSubscriber(connection, session, topicName, filterName, durable);
            
            if(durable)
                subscriber.startListening(topicName + "_" + filterName);
            else 
                subscriber.startListening("");
            
            subscribers.put(name, subscriber);
        } catch(Exception e) {
            e.printStackTrace();
            Log.log.error(e.getMessage(), e);
            throw e;
        }
    } //startTopicSubscriber()
    
    public void stopTopicSubscriber(String queueManager, String topicName, String filterName) throws Exception {
        
        String name = queueManager + "_" + topicName + "_" + filterName;
        MQSubscriber subscriber = subscribers.get(name);
        
        if(subscriber == null)
            throw new Exception("Cannot find subscriber with name: " + name);
        
        subscriber.stopListening(queueManager, topicName + "_" + filterName, true); // unsubscribe

        subscribers.remove(name);
    } //stopTopicSubscriber()
    
    public void startQueueReceiver(String ip, String user, String pass, int port, String queueManager, String channel, String queueName, 
                                   String ack, String transport, String filterName) throws Exception {
        
        MQQueueConnection connection = null;
        MQQueueSession session = null;
                        
        if(StringUtils.isBlank(queueName))
            throw new Exception("Queue name is not available.");
        
        String name = queueManager + "_" + queueName + "_" + filterName;

        MQReceiver receiver = receivers.get(name);

        /* This way cannot make the receiver to start listen again after a server down
        if(receiver != null) {
            // This is not working because the async session was still open with its open connection
            // The connection cannot be closed because it is still associated with the queue manager
            // even if the server was down
            receiver.startReceiving();
            // This way basic does not restart to listen after a server down
            receiver.getReceiver().setMessageListener(receiver);
            return;
        } */

        try {
            connection = getQueueConnection(ip, user, pass, port, queueManager, channel, transport);
            qcs.put(getConnectionKey(ip, user, queueManager, transport), connection);
            
            if(StringUtils.isBlank(ack) || ack.equals(AUTO_ACKNOWLEDGE))
                session = (MQQueueSession)connection.createQueueSession(false, Session.AUTO_ACKNOWLEDGE);
            
            else if(ack.equals(CLIENT_ACKNOWLEDGE))
                session = (MQQueueSession)connection.createQueueSession(false, Session.CLIENT_ACKNOWLEDGE);
            
            receiver = new MQReceiver(connection, session, queueName, filterName);
            receiver.startReceiving();

            receivers.put(name, receiver);
        } catch(Exception e) {
            e.printStackTrace();
            Log.log.error(e.getMessage(), e);
            throw e;
        }
    } // startQueueReceiver()
    
    public void stopQueueReceiver(String queueManager, String queueName, String filterName) throws Exception {
        
        String name = queueManager + "_" + queueName + "_" + filterName;
        MQReceiver receiver = receivers.get(name);
        
        if(receiver == null)
            throw new Exception("Cannot find receiver with name: " + name);
        
        receiver.stopReceiving(queueManager, queueName);

        receivers.remove(name);
    } //stopQueueReceiver()
    
    public void clearConnections() {
        
        try {
            Set<String> subs = subscribers.keySet();
            
            for(Iterator it=subs.iterator(); it.hasNext();) {
                String key = (String)it.next();
                MQSubscriber subscriber = subscribers.get(key);
                if(subscriber != null) {
                    try {
                        int index = key.indexOf("_");
                        if(index > 0)
                            subscriber.stopListening(key.substring(0, index), key.substring(index+1), false);
                    } catch(Exception e) {
                        Log.log.error(e.getMessage(), e);
                    }
                }
                
                subscribers.remove(key);
            }
        
            Set<String> topicConn = tcs.keySet();
            
            for(Iterator it=topicConn.iterator(); it.hasNext();) {
                String key = (String)it.next();
                Connection connection = tcs.get(key);
                if(connection != null) {
                    try {
                        connection.stop();
                        connection.close();
                        connection = null;
                    } catch(Exception e) {
                        System.err.println(e.getMessage());
                    }
                }
                
                tcs.remove(key);
            }
            
            Set<String> recs = receivers.keySet();
            
            for(Iterator it=recs.iterator(); it.hasNext();) {
                String key = (String)it.next();
                MQReceiver receiver = receivers.get(key);
                if(receiver != null) {
                    try {
                        int index = key.indexOf("_");
                        if(index > 0)
                            receiver.stopReceiving(key.substring(0, index), key.substring(index+1));
                    } catch(Exception e) {
                        Log.log.error(e.getMessage(), e);
                    }
                }
                
                receivers.remove(key);
            }
        
            Set<String> queueConn = qcs.keySet();
            
            for(Iterator it=queueConn.iterator(); it.hasNext();) {
                String key = (String)it.next();
                Connection connection = qcs.get(key);
                if(connection != null) {
                    try {
                        connection.stop();
                        connection.close();
                        connection = null;
                    } catch(Exception e) {
                        System.err.println(e.getMessage());
                    }
                }
                
                qcs.remove(key);
            }

        } catch(Exception e) {
            Log.log.error(e.getMessage(), e);
        }
    } // clearConnections()

    // Testing ==============================================================================

    private static void main1(String[] args) {

        String ip = "10.50.1.199";
        int port = 1418;
        String queueManager = "TEST_MGRA";
        String channel = "SYSTEM.ADMIN.SVRCONN";
        String queueName = "TEST_QUEUE";
        String topicName = "fruit";
        
//        unitTest();
        try {
//          demoSendReceive("TEST1", "SYSTEM.ADMIN.SVRCONN", "TEST_QUEUE", "adfas");
//          demoPubSub("TEST_MGRA", "SYSTEM.ADMIN.SVRCONN", "NewsTopic", "agdasd");

//          demoSub();
            
//            MQService service = new MQService();
//            service.executeTestSub("TEST_MGRAConnectionFactory", "NewsTopic");

//            demoPub1();

//            IBMmqAPI.subscribe("TEST_MGRA", "SYSTEM.ADMIN.SVRCONN", "NewsTopic");

//            IBMmqAPI.publish("TEST_MGRA", "SYSTEM.ADMIN.SVRCONN", "NewsTopic", "agdasd");
            
            IBMmqAPI api = new IBMmqAPI(ip, port, "", "", queueManager, channel, "TOPIC", topicName);
            
            api.publishBinaryMessage((new String("afdsa")).getBytes());
            api.publishBinaryMessage((new String("11111")).getBytes());
        } catch(Exception e) {
            e.printStackTrace();
        }
    }

    private static void unitTest() {

        String ip = "10.50.1.199";
        int port = 1418;
        String queueManager = "TEST_MGRA";
        String channel = "SYSTEM.ADMIN.SVRCONN";
        String queueName = "TEST_QUEUE";
        String topicName = "NewsTopic";

        try {
            // 0. Simple send/receive
//            demoSendReceive("TEST1", "SYSTEM.ADMIN.SVRCONN", queueName, "adfas");
//            System.out.println("0. Simple send/receive.");

            // 1. Simple pub/sub
//            demoPubSub(queueManager, channel, topicName, "agdasd");
//            System.out.println("1. Simple pub/sub.");

            // 2. API Sender
            IBMmqAPI api = new IBMmqAPI(ip, port, "", "", queueManager, channel, "QUEUE", queueName);
//            api.sendMessage("adfas");
//            System.out.println("2. API sendMessage() sent.");
            byte[] bytes = new byte[1];
            bytes[0] = '1';
            api.sendBinaryMessage(bytes);
            System.out.println("2. API sendBinaryMessage() sent.");

            // 3. API text message Receiver
            Message message = api.receiveMessage(5);

            if(message instanceof TextMessage) {
                System.out.println(((TextMessage)message).getText());
                System.out.println("3. API text message Received.");
            }

            // 4. API binary message receiver
            if(message instanceof BytesMessage) {
                byte[] msg = api.getByteMessage((JMSBytesMessage)message);
                
                System.out.println("4. API binary message received.");
                System.out.println("Message length = " + msg.length);
                System.out.write(msg);
                System.out.println();
            }

            // 5. API publication
//            IBMmqAPI.pubMessage(topicName, "asdfasd");
            System.out.println("5. API message published.");

            // 6. API subcription
//            IBMmqAPI.subMessage(topicName);
            System.out.println("6. API message subscribed.");
        } catch(Exception e) {
            e.printStackTrace();
        }
    }

    public void executeTestSub(String connectionFactoryName, String topicName) {

        try {
//            executor.execute(new TestSub(connectionFactoryName, topicName));
        } catch(Exception e) {
            e.printStackTrace();
        }
/*
        String result = "Response is: ";

        TestSubCallable callable = new TestSubCallable(mqSubscriber);
        FutureTask<String> futureTask = new FutureTask<String>(callable);

        try {
            executor.execute(futureTask);

            while (!futureTask.isDone()) {
                try {
                    Log.log.debug("Waiting for FutureTask to complete");
                    result = futureTask.get(waitTime, TimeUnit.SECONDS);
                } catch (InterruptedException | ExecutionException e) {
                    Log.log.error(e.toString());
                    JSONObject message = new JSONObject();
                    message.accumulate("Message", "Failed to process.");
                    result = message.toString();
                    break;
                } catch(TimeoutException e) {
                    futureTask.cancel(true);
                    Log.log.error(e.toString());
                    JSONObject message = new JSONObject();
                    message.accumulate("Message", "Task is timed out.");
                    result = message.toString();
                    break;
                }
            }
        } catch(Exception e) {
            Log.log.error(e.toString());
            JSONObject message = new JSONObject();
            message.accumulate("Message", "Failed to execute task.");
            result = message.toString();
        }

        return result; */
    }

    class TestSub implements Runnable {

        String connectionFactoryName = null;
        String topicName = null;

        public TestSub(String connectionFactoryName, String topicName) {

            this.connectionFactoryName = connectionFactoryName;
            this.topicName = topicName;
        }

        @Override
        public void run() {

            MQSubscriber mqSubscriber = new MQSubscriber();

            try {
                mqSubscriber.connect(connectionFactoryName, topicName);
//                System.in.read();
//                JMSMessage receivedMessage = (JMSMessage)subscriber.receive();
//                System.out.println("\\nReceived message:\\n" + receivedMessage);
             } catch(Exception e) {
                 e.printStackTrace();
                 Log.log.error(e.getMessage(), e);
             }
        }
    }

    class TestSubCallable implements Callable<String> {

        private MQSubscriber mqSubscriber = null;

        public TestSubCallable(MQSubscriber mqSubscriber) {

            this.mqSubscriber = mqSubscriber;
        }

        @Override
        public String call() throws Exception {

            mqSubscriber.connect("TEST_MGRAConnectionFactory", "NewsTopic");

            return "success";
        }

    } // end of class FilterCallable


    public static void demoPub() throws Exception {

        try {
            TopicConnection connection;
            TopicSession session;
            JMSTopicUtils utils = new JMSTopicUtils();
            connection = (MQTopicConnection)utils.getConnection("TEST_MGRAConnectionFactory");
            session = (MQTopicSession)connection.createTopicSession(false, Session.AUTO_ACKNOWLEDGE);

            Topic topic = utils.getTopic("NewsTopic");

            long uniqueNumber = System.currentTimeMillis() % 1000;
            JMSTextMessage message = (JMSTextMessage) session.createTextMessage("SimplePubSub "+ uniqueNumber);

            MQTopicPublisher publisher = (MQTopicPublisher)session.createPublisher(topic);
            publisher.publish(message);
            System.out.println("Sent message:\\n" + message);

            publisher.close();
            session.close();
            connection.close();

//            MQTopicSubscriber subscriber = (MQTopicSubscriber) session.createSubscriber(topic);
//            JMSMessage receivedMessage = (JMSMessage) subscriber.receive(10000);
//            System.out.println("\\nReceived message:\\n" + receivedMessage);
        } catch(Exception e) {
            e.printStackTrace();
        }
    }

    public static void demoPub1() throws Exception {

        try {
            MQTopicConnectionFactory cf = new MQTopicConnectionFactory();

            // Config
            cf.setHostName("10.50.1.199");
            cf.setPort(1418);
            cf.setTransportType(JMSC.MQJMS_TP_CLIENT_MQ_TCPIP);
            cf.setQueueManager("TEST_MGRA");
            cf.setChannel("SYSTEM.ADMIN.SVRCONN");

            MQTopicConnection connection = (MQTopicConnection) cf.createTopicConnection("mp1", "Resolve1234");
            MQTopicSession session = (MQTopicSession) connection.createTopicSession(false, Session.AUTO_ACKNOWLEDGE);
            MQTopic topic = (MQTopic) session.createTopic("News");
            MQTopicPublisher publisher =  (MQTopicPublisher) session.createPublisher(topic);
            MQTopicSubscriber subscriber = (MQTopicSubscriber) session.createSubscriber(topic);

            long uniqueNumber = System.currentTimeMillis() % 1000;
            JMSTextMessage message = (JMSTextMessage) session.createTextMessage("SimplePubSub "+ uniqueNumber);

            // Start the connection
            connection.start();

            publisher.publish(message);
            System.out.println("Sent message:\\n" + message);

//            JMSMessage receivedMessage = (JMSMessage) subscriber.receive(10000);
//            System.out.println("\\nReceived message:\\n" + receivedMessage);

            publisher.close();
            subscriber.close();
            session.close();
            connection.close();

            System.out.println("\\nSUCCESS\\n");
        } catch (JMSException jmsex) {
            System.out.println(jmsex);
            System.out.println("\\nFAILURE\\n");
        } catch (Exception ex) {
            System.out.println(ex);
            System.out.println("\\nFAILURE\\n");
        }
    } // demoPub

    public static void demoSub() throws Exception {

        MQTopicPublisher publisher = null;
        MQTopicSubscriber subscriber = null;

        try {
            MQSubscriber mqSubscriber = new MQSubscriber();
            mqSubscriber.connect("TEST_MGRAConnectionFactory", "NewsTopic");
//            System.in.read();
//            JMSMessage receivedMessage = (JMSMessage)subscriber.receive();
//            System.out.println("\\nReceived message:\\n" + receivedMessage);
         } catch(Exception e) {
             throw e;
         } catch(Throwable t) {
             t.printStackTrace();
         } finally {
             try {
                 if(subscriber!= null)
                     subscriber.close();
                 if(publisher!= null)
                     publisher.close();
             } catch(Exception e) {
                 Log.log.error(e.getMessage(), e);
             }
         }
    } // demoSub


    public static void demoSendReceive(String queueManager, String channel, String queueName, String msg) throws Exception {

        if(StringUtils.isBlank(queueName) || StringUtils.isBlank(msg))
            throw new Exception("Queue name or message is blank.");

        QueueConnection qc = null;
        MQQueueSession qs = null;
        QueueSender sender = null;

        try {
            MQQueueConnectionFactory mqqcf = new MQQueueConnectionFactory();

//            mqqcf.setHostName("10.50.1.199");
//            mqqcf.setPort(1418);
//            mqqcf.setQueueManager("TEST_MGRA");
//            mqqcf.setChannel ("CHANNEL6");

            mqqcf.setHostName("10.50.1.199");
            mqqcf.setPort(1417);
            mqqcf.setQueueManager(queueManager);
            mqqcf.setChannel (channel);

            mqqcf.setTransportType(WMQConstants.WMQ_CM_CLIENT);

            qc = (MQQueueConnection)mqqcf.createQueueConnection("mq1", "Resolve1234");
            qc.start();
            qs = (MQQueueSession)qc.createQueueSession(false, Session.AUTO_ACKNOWLEDGE);
            Message tm = qs.createTextMessage(msg);
//            tm.setText(text);
            MQQueue q = new MQQueue(queueName);
            sender = qs.createSender(q);
            sender.send(tm);

            QueueReceiver qr = qs.createReceiver (q);
            Message m2 = qr.receive();

            System.out.println ("Received message: " + m2);
         } catch(Exception e) {
             throw e;
         } catch(Throwable t) {
             t.printStackTrace();
         } finally {
             try {
                 if(sender != null)
                    sender.close();
                 if(qs != null)
                     qs.close();
                 if(qc != null)
                     qc.close();
             } catch(Exception e) {
                 Log.log.error(e.getMessage(), e);
             }
         }
    } // demoSendReceive

    public static void demoPubSub(String queueManager, String channel, String topicName, String msg) throws Exception {

        if(StringUtils.isBlank(topicName) || StringUtils.isBlank(msg))
            throw new Exception("Topic name or message is blank.");

        MQConnectionFactory factory = null;
        Connection connection = null;
        Session session = null;
        Destination destination = null; // a destination can be a topic or a queue
        MessageProducer producer = null;
        MessageConsumer consumer = null;

        String icf = "com.sun.jndi.fscontext.RefFSContextFactory";
        String url = "file:/C:/JNDI-Directory";

        try {
            JNDIUtils jndiUtil= new JNDIUtils(icf, url);

            factory = (MQConnectionFactory)jndiUtil.getConnectionFactory("TEST_MGRAConnectionFactory");
            factory.setTransportType(WMQConstants.WMQ_CM_CLIENT);
            factory.setHostName("10.50.1.199");
            factory.setPort(1418);
            factory.setQueueManager(queueManager);
            factory.setChannel(channel);

            connection = factory.createConnection();
            connection.start();

            // Indicate a non-transactional session
            boolean transacted = false;
            session = connection.createSession( transacted, Session.AUTO_ACKNOWLEDGE);

            destination = jndiUtil.getDestination(topicName);

            producer = session.createProducer(destination);
            consumer = session.createConsumer(destination);

            TextMessage message = session.createTextMessage(msg);
            producer.send(message);
// System.out.println("NewsPublisher: Message Publication Completed");
//            TextMessage text = (TextMessage)consumer.receive(1000);
// System.out.println("NewsSubscriber: " + text);
         } catch(Exception e) {
             throw e;
         } catch(Throwable t) {
             t.printStackTrace();
         } finally {
             try {
                 if(producer!= null)
                    producer.close();
                 if(session!= null)
                    session.close();
                 if(connection!= null)
                    connection.close();
             } catch(Exception e) {
                 Log.log.error(e.getMessage(), e);
             }
         }
    } // demoPubSub

} // MQService
