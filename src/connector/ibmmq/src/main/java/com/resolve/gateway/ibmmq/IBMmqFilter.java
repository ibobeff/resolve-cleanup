package com.resolve.gateway.ibmmq;

import java.util.Hashtable;

import com.ibm.mq.MQException;
import com.ibm.mq.MQQueueManager;
import com.resolve.gateway.BaseFilter;
import com.resolve.util.Log;

public class IBMmqFilter extends BaseFilter {


    public static final String TOPIC = "TOPIC";
    public static final String QUEUE = "QUEUE";
    
    public static final String TRANSPORT = "TRANSPORT";

    public static final String PORT = "PORT";

    public static final String MANAGER = "MANAGER";

    public static final String ACK = "ACK";

    public static final String NAME = "NAME";

    public static final String TYPE = "TYPE";

    public static final String CHANNEL = "CHANNEL";

    public static final String DURABLE = "DURABLE";

    private String transport;

    private Integer port;

    private String manager;

    private String ack;

    private String name;

    private String type;

    private String channel;

    private Boolean durable;

    public IBMmqFilter(String id, String active, String order, String interval, String eventEventId, String runbook, String script, 
                       String transport, String port, String manager, String ack, String name, String type, String channel, String durable) {
         
        super(id, active, order, interval, eventEventId, runbook, script);
        this.transport = transport;
        try {
            this.port = new Integer(port);
        } catch (Exception e) {
            port = "0";
            Log.log.error("port" + " should be in type " + "Integer");
        }
        this.manager = manager;
        this.ack = ack;
        this.name = name;
        this.type = type;
        this.channel = channel;
        try {
            this.durable = new Boolean(durable);
        } catch (Exception e) {
            this.durable = false;
            Log.log.error("durable" + " should be in type " + "Boolean");
        }
    }

    public String getTransport() {
        return this.transport;
    }

    public void setTransport(String transport) {
        this.transport = transport;
    }

    public Integer getPort() {
        return this.port;
    }

    public void setPort(Integer port) {
        this.port = port;
    }

    public String getManager() {
        return this.manager;
    }

    public void setManager(String manager) {
        this.manager = manager;
    }

    public String getAck() {
        return this.ack;
    }

    public void setAck(String ack) {
        this.ack = ack;
    }

    public String getName() {
        return this.name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getType() {
        return this.type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getChannel() {
        return this.channel;
    }

    public void setChannel(String channel) {
        this.channel = channel;
    }

    public Boolean getDurable() {
        return this.durable;
    }

    public void setDurable(Boolean durable) {
        this.durable = durable;
    }

    /**
     * This method tells if the filter's queue is alive by testing if it's connected to the queue manager.
     * @param ip The ip address of IBM MQ server
     * @return true if the filter is alive; false otherwise.
     * @throws MQException
     */
    public boolean isConnected(String ip) {
        Hashtable<String, Object> properties = new Hashtable<String, Object> () {{
            put("hostname", ip);
            put("port", port.intValue());
            put("channel", channel);
            put("userID", IBMmqGateway.getUser());
            put("password", IBMmqGateway.getPass());
        }};
        boolean isConnected = false;
        try {
            MQQueueManager qManager = new MQQueueManager(manager, properties);
            isConnected = qManager.isConnected();
            qManager.disconnect();
        } catch (MQException e) {
            Log.log.warn(String.format("Error getting MQQueueManager %s", manager), e);
        }
        return isConnected;
        
    }
}

