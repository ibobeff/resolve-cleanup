package com.resolve.services.hibernate.vo;

import com.resolve.util.MappingAnnotation;
import com.resolve.util.StringUtils;

public class IBMmqFilterVO extends GatewayFilterVO {

    private static final long serialVersionUID = 1L;
    
    private static final int PORT_NOT_AVAILABLE = -1;
    private static final String PORT_NOT_AVAILABLE_ERR = "Port is not available.";
    
    private static final int QUEUE_MANAGER_NOT_AVAILABLE = -2;
    private static final String QUEUE_MANAGER_NOT_AVAILABLE_ERR = "Queue Manager is not available.";

    private static final int CHANNEL_NOT_AVAILABLE = -3;
    private static final String CHANNEL_NOT_AVAILABLE_ERR = "Channel is not available.";
    
    private static final int TYPE_NOT_AVAILABLE = -4;
    private static final String TYPE_NOT_AVAILABLE_ERR = "Type is not available.";

    private static final int VALUE_NOT_AVAILABLE = -5;
    private static final String VALUE_NOT_AVAILABLE_ERR = "Queue/Topic name is not available.";
    
    private static final int ACK_NOT_AVAILABLE = -6;
    private static final String ACK_NOT_AVAILABLE_ERR = "Acknowledgement mode is not available.";

    private static final int TRANSPORT_NOT_AVAILABLE = -7;
    private static final String TRANSPORT_NOT_AVAILABLE_ERR = "Transport mode is not available.";

    public IBMmqFilterVO() {
    }

    private String UTransport;

    private Integer UPort;

    private String UManager;

    private String UAck;

    private String UValue;

    private String UType;

    private String UChannel;

    private Boolean UDurable;

    @MappingAnnotation(columnName = "TRANSPORT")
    public String getUTransport() {
        return this.UTransport;
    }

    public void setUTransport(String uTransport) {
        this.UTransport = uTransport;
    }

    @MappingAnnotation(columnName = "PORT")
    public Integer getUPort() {
        return this.UPort;
    }

    public void setUPort(Integer uPort) {
        this.UPort = uPort;
    }

    @MappingAnnotation(columnName = "MANAGER")
    public String getUManager() {
        return this.UManager;
    }

    public void setUManager(String uManager) {
        this.UManager = uManager;
    }

    @MappingAnnotation(columnName = "ACK")
    public String getUAck() {
        return this.UAck;
    }

    public void setUAck(String uAck) {
        this.UAck = uAck;
    }

    @MappingAnnotation(columnName = "VALUE")
    public String getUValue() {
        return this.UValue;
    }

    public void setUValue(String uValue) {
        this.UValue = uValue;
    }

    @MappingAnnotation(columnName = "TYPE")
    public String getUType() {
        return this.UType;
    }

    public void setUType(String uType) {
        this.UType = uType;
    }

    @MappingAnnotation(columnName = "CHANNEL")
    public String getUChannel() {
        return this.UChannel;
    }

    public void setUChannel(String uChannel) {
        this.UChannel = uChannel;
    }

    @MappingAnnotation(columnName = "DURABLE")
    public Boolean getUDurable() {
        return this.UDurable;
    }

    public void setUDurable(Boolean uDurable) {
        this.UDurable = uDurable;
    }
    
    @Override
    public int isValid() {

        int valid = 0;
        
        if(UPort == null || UPort.intValue() == 0)
            valid = PORT_NOT_AVAILABLE;
        if(StringUtils.isBlank(UManager))
            valid = QUEUE_MANAGER_NOT_AVAILABLE;
        if(StringUtils.isBlank(UChannel))
            valid = CHANNEL_NOT_AVAILABLE;
        if(StringUtils.isBlank(UType))
            valid = TYPE_NOT_AVAILABLE;
        if(StringUtils.isBlank(UValue))
            valid = VALUE_NOT_AVAILABLE;
        if(StringUtils.isBlank(UAck))
            valid = ACK_NOT_AVAILABLE;
        if(StringUtils.isBlank(UTransport))
            valid = TRANSPORT_NOT_AVAILABLE;
        
        return valid;
    }
    
    @Override
    public String getValidationError(int errorCode)
    {
        String error = null;
        
        switch(errorCode)
        {
            case PORT_NOT_AVAILABLE:
                error = PORT_NOT_AVAILABLE_ERR;
                break;
            case QUEUE_MANAGER_NOT_AVAILABLE:
                error = QUEUE_MANAGER_NOT_AVAILABLE_ERR;
                break;
            case CHANNEL_NOT_AVAILABLE:
                error = CHANNEL_NOT_AVAILABLE_ERR;
                break;
            case TYPE_NOT_AVAILABLE:
                error = TYPE_NOT_AVAILABLE_ERR;
                break;
            case VALUE_NOT_AVAILABLE:
                error = VALUE_NOT_AVAILABLE_ERR;
                break;
            case ACK_NOT_AVAILABLE:
                error = ACK_NOT_AVAILABLE_ERR;
                break;
            case TRANSPORT_NOT_AVAILABLE:
                error = TRANSPORT_NOT_AVAILABLE_ERR;
                break;
            default:
                break;
        }
        
        return error;
    } // getValidationError()
    
    @Override
    public int hashCode()
    {
        final int prime = 31;
        int result = super.hashCode();
        result = prime * result + ((UTransport == null) ? 0 : UTransport.hashCode());
        result = prime * result + ((UPort == null) ? 0 : UPort.hashCode());
        result = prime * result + ((UManager == null) ? 0 : UManager.hashCode());
        result = prime * result + ((UAck == null) ? 0 : UAck.hashCode());
        result = prime * result + ((UValue == null) ? 0 : UValue.hashCode());
        result = prime * result + ((UType == null) ? 0 : UType.hashCode());
        result = prime * result + ((UChannel == null) ? 0 : UChannel.hashCode());
        result = prime * result + ((UDurable == null) ? 0 : UDurable.hashCode());
        return result;
    }
} //IBMmqFilterVO

