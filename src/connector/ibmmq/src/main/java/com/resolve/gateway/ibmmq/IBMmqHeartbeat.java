/******************************************************************************
* (C) Copyright 2014
*
* Resolve Systems, LLC
*
* All rights reserved
* This software is distributed under license and may not be used, copied,
* modified, or distributed without the express written permission of
* Resolve Systems, LLC.
*
******************************************************************************/

package com.resolve.gateway.ibmmq;

import java.util.Map;
import java.util.Iterator;

import com.ibm.mq.MQEnvironment;
import com.ibm.mq.MQQueueManager;

import com.resolve.gateway.Filter;
import com.resolve.util.Log;

public class IBMmqHeartbeat extends Thread
{
    private final IBMmqGateway gateway;
    
    public IBMmqHeartbeat(IBMmqGateway gateway)
    {
        this.gateway = gateway;
    }

    @Override
    public void run()
    {
        Log.log.info("Heartbeat is started.");
        
        for(;;) {
            try {
                if(gateway.isActive()) {
                    try
                    {
                        // Detect if the IMB MQ server is up and running by connecting to any queue manager in QUEUE mode
                        boolean alive = sendHeartbeat();
                        
                        String status = (alive)?"":" not";
                        Log.log.debug("Heart beat: " + System.currentTimeMillis());
                        Log.log.debug("Gateway: " + gateway.getIp() + " is" + status + " alive.");
                        
                        // If the IMB MQ server was up then down
                        if(IBMmqGateway.isMQServerAlive()) {
                            if(!alive) {
                                Log.log.warn("IBM MQ server is down.");
                                IBMmqGateway.setMQServerAlive(false);
                             // The current implementation is tested when the queue manager is stopped.
                             // Cannot disconnect, otherwise, when reconnect and set client id will fail.
                             //   MQService.instance.clearConnections();
                             // If the complete MQ server is down, the connections might be closed.
                             // May need  new implemenation and re-test.
                            }
                        }
                        
                        // When the IMB MQ server was down and now up again, re-deploy the subscribers
                        else {
                            if(alive) {
                                Log.log.warn("IBM MQ server is back up.");
                                gateway.redeploy();
                                IBMmqGateway.setMQServerAlive(true);
                            }
                        }
                    } catch(Exception e) {
                        Log.log.error(e.getMessage(), e);
                    }
                } // if()
                
                int interval = gateway.getAlive();
                sleep(interval*1000);
            } 
            catch(InterruptedException e)
            {
                Log.log.error(e.getMessage(), e);
                break;
            }
        } // for()
    } // run()

    public static boolean sendHeartbeat()
    {
        boolean isAlive = true;
        
        Map<String, Filter> filterMap = IBMmqGateway.getInstance().getFilters();
        
        try {
            for(Iterator it = filterMap.values().iterator(); it.hasNext();) {
                IBMmqFilter mqFilter = (IBMmqFilter)it.next();
                
                MQEnvironment.hostname = IBMmqGateway.getIp();
                MQEnvironment.port = mqFilter.getPort().intValue();
                MQEnvironment.channel = (String)mqFilter.getChannel();
                
                MQQueueManager manager = new MQQueueManager((String)mqFilter.getManager());
                isAlive = manager.isConnected();

                manager.disconnect();

                break;
            } // for()

            isAlive = false;
        } catch(Exception e) {
            Log.log.error("Failed to establish connection with IBM MQ server.");
            Log.log.error(e.getMessage(), e);
            isAlive = false;
        }
        
        return isAlive;
    } // sendHeartbeat()
    
    public void stopHeartBeat() {
        
        try {
            this.interrupt();
            Log.log.info("Heartbeat is stopped.");
        } catch(Exception e) {
            Log.log.error(e.getMessage(), e);
        }
    }
    
    private static void main1(String[] argc) {
        
        MQEnvironment.hostname = "10.50.1.199";
        MQEnvironment.port = 1418;
        MQEnvironment.channel = "SYSTEM.ADMIN.SVRCONN";
        
        try {
            MQQueueManager manager = new MQQueueManager("TEST_MGRA");
            boolean isAlive = manager.isConnected();
            System.out.println("isAlive = " + isAlive);
            manager.disconnect();
        } catch(Exception e) {
            e.printStackTrace();
        }
    }
}
