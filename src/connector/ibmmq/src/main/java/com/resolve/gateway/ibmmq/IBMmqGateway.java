package com.resolve.gateway.ibmmq;

import java.io.IOException;
import java.net.InetAddress;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.CopyOnWriteArrayList;

import org.apache.commons.lang3.tuple.Pair;

import com.ibm.mq.MQException;
import com.resolve.gateway.BaseClusteredGateway;
import com.resolve.gateway.BaseFilter;
import com.resolve.gateway.Filter;
import com.resolve.gateway.Gateway;
import com.resolve.gateway.MIBMmq;
import com.resolve.rsbase.MainBase;
import com.resolve.util.Constants;
import com.resolve.util.Log;
import com.resolve.util.StringUtils;

import net.sf.json.JSONObject;

public class IBMmqGateway extends BaseClusteredGateway {
    
    private static volatile IBMmqGateway instance = null;
    private static volatile IBMmqHeartbeat heartBeat = null; // This heartbeat is used to detect whether IBM MQ server is alive.
    private static volatile boolean mqServerAlive = false;
    
    private static volatile List<String> deployedFilters = new CopyOnWriteArrayList<String>();

    private static String ip; 
    private static String user;
    private static String pass;
    private static int alive;

    private String queue = null;

    public static IBMmqGateway getInstance(ConfigReceiveIBMmq config) {
        
        if (instance == null) {
            instance = new IBMmqGateway(config);
        }
        
        return instance;
    }
    
    public static boolean isMQServerAlive()
    {
        return mqServerAlive;
    }

    public static void setMQServerAlive(boolean alive)
    {
        IBMmqGateway.mqServerAlive = alive;
    }
    public static String getIp()
    {
        return ip;
    }

    public static String getUser()
    {
        return user;
    }

    public static String getPass()
    {
        return pass;
    }
    
    public static int getAlive()
    {
        return alive;
    }

    public static void setAlive(int alive)
    {
        IBMmqGateway.alive = alive;
    }
    
    /**
     * This method is called once the initialization is done. If it gets called
     * before the initialization it will throw exception.
     *
     * @return
     */
    public static IBMmqGateway getInstance() {
        if (instance == null) {
            throw new RuntimeException("IBMmq Gateway is not initialized correctly.");
        } else {
            return instance;
        }
    }

    private IBMmqGateway(ConfigReceiveIBMmq config) {
        super(config);
    }

    /**
     * 
     * This method gets the health status specific to IBMMQ gateway.
     * A healthy IBMMQ gateway must have valid license either of the following:
     * 1. If there are filters, all filters must be connected to the queue manager OR
     * 2. The IBMMQ server is reachable in case there a no filters deployed onto the gateway
     * Other than that, the gateway is Unhealthy. If there are filters deployed and some are
     * connected and some aren't then the staus is YELLOW.
     * 
     */
    @Override
    public Pair<HealthStatusCode, String> checkGatewayHealth(Map<String, Object> healthCheckOptions) {
        Log.log.trace("Health status check for IBMmqGateway.");
        String gatewayId = getQueueName() + " ";
        // Check license status
        Pair<HealthStatusCode, String> licenseStatus = super.checkGatewayHealth(healthCheckOptions);
        if (licenseStatus.getLeft() != Gateway.HealthStatusCode.GREEN) {
            return Pair.of(licenseStatus.getLeft(), gatewayId + licenseStatus.getRight());
        }
        boolean isConnected = true;
        int nAlives = 0;
        int nDeads = 0;
        String currentQueueMgr = "";
        try {
            if (filters.size() != 0) {
                // 1. If there are filters, all filters must be connected to the queue manager OR
                for(Iterator<Filter> it = filters.values().iterator(); it.hasNext();) {
                    IBMmqFilter filter = (IBMmqFilter)it.next();
                    currentQueueMgr = filter .getManager();
                    isConnected = filter.isConnected(ip);
                    if (!isConnected) {
                        nDeads++;
                        Log.log.warn(String.format("Filter %s is not connected to queue manager %s", filter.getName(), currentQueueMgr));
                    } else {
                        nAlives++;
                    }
                };
                // At this point are filters on the gateway.
                if (nAlives > 0 && nDeads > 0) {
                    // Some are good and some are bad the status is YELLOW.
                    return Pair.of(Gateway.HealthStatusCode.YELLOW, String.format("Some of the filters of %s are not connected", gatewayId));
                } else if (nAlives == 0 && nDeads > 0) {
                    // All is bad
                    return Pair.of(Gateway.HealthStatusCode.RED, String.format("All filters of %s are not connected", gatewayId));
                } else {
                    // All is good
                    return Pair.of(Gateway.HealthStatusCode.GREEN, gatewayId + HEALTHY_GATEWAY_MESSAGE);
                }
            } else {
                // 2. The IBMMQ server is reachable in case there a no filters deployed onto the gateway
                InetAddress inetAddress = InetAddress.getByName(ip);
                boolean serverReachable = inetAddress.isReachable(30000); // Try for 30 seconds
                if (serverReachable) {
                    Log.log.trace(String.format("IBM MQ Server %s is reachable.", ip));
                    return Pair.of(Gateway.HealthStatusCode.GREEN, "There are no filters deployed and IBM MQ Server is reachable.");
                } else {
                    Log.log.warn(String.format("IBM MQ Server %s is unreachable.", ip));
                    return Pair.of(Gateway.HealthStatusCode.RED, "There are no filters deployed and IBM MQ Server is unreachable.");
                }
            }
        } catch (IOException e) {
            Log.log.warn(String.format("Error trying to reach IBM MQ server %s.", ip), e);
            return Pair.of(Gateway.HealthStatusCode.RED, "There are no filters deployed and a network error occurs while trying to reach IBM MQ Server.");
        }
    }

    @Override
    public String getLicenseCode() {
        return "IBMMQ";
    }

    @Override
    protected void addAdditionalValues(Map<String, Object> arg0, Map<String, String> arg1) {
    }

    @Override
    protected String getGatewayEventType() {
        return "IBMMQ";
    }

    @Override
    protected String getMessageHandlerName() {
        return MIBMmq.class.getSimpleName();
    }

    @Override
    protected Class<MIBMmq> getMessageHandlerClass() {
        return MIBMmq.class;
    }

    public String getQueueName() {
        return queue + getOrgSuffix();
    }

    @Override
    public void start() {
        Log.log.info("Starting IBMmq Gateway ...");
        super.start();
    }

    @Override
    public void run() {
        
        // Reload all the deployed filters when RSRemote is stopped and started again.
        super.sendSyncRequest();
    }

    @Override
    protected void initialize() {
        ConfigReceiveIBMmq config = (ConfigReceiveIBMmq)configurations;
        
        try {
            ip = config.getIp();
            user = config.getUsername();
            pass = config.getPassword();
            
            String aliveStr = config.getAlive();
            if(StringUtils.isBlank(aliveStr))
                alive = 60;
            
            else {
                try {
                    Integer aliveInt = new Integer(aliveStr);
                    alive = aliveInt.intValue();
                } catch(Exception ee) {
                    alive = 60;
                    ee.printStackTrace();
                    Log.log.error(ee.getMessage(), ee);
                }
            }
        } catch(Exception e) {
            e.printStackTrace();
            Log.log.error(e.getMessage(), e);
        } catch(Throwable t) {
            t.printStackTrace();
            Log.log.error(t.getMessage(), t);
        }
        
        queue = config.getQueue().toUpperCase();
        
        try {
            Log.log.info("Initializing IBMmq Listener ...");
            this.gatewayConfigDir = "/config/ibmmq/";
        } catch (Exception e) {
            Log.log.error("Failed to config IBMmq Gateway: " + e.getMessage(), e);
        }

        if(config.isActive() && config.isPrimary()) {
            // Monitor MQ server connection if it's down
            mqServerAlive = IBMmqHeartbeat.sendHeartbeat();

            heartBeat = new IBMmqHeartbeat(this);
            heartBeat.start();
        }
    }

    /**
     * This method processes the message received from the IBMmq system.
     *
     * @param message
     */
    public boolean processData(String filterName, Map<String, String> params) {
        
        boolean result = true;
        
        try {
            if (StringUtils.isNotBlank(filterName) && params != null) {
                IBMmqFilter ibmmqFilter = (IBMmqFilter) filters.get(filterName);
                if (ibmmqFilter != null && ibmmqFilter.isActive()) {
                    if (Log.log.isDebugEnabled()) {
                        Log.log.debug("Processing filter: " + ibmmqFilter.getId());
                        Log.log.debug("Data received through IBMmq gateway: " + params);
                    }
                    Map<String, String> runbookParams = params;
                    
                    runbookParams.put(FILTER_ID_NAME, ibmmqFilter.getId());
                    
                    if (!params.containsKey(Constants.EVENT_EVENTID) || StringUtils.isBlank(params.get(Constants.EVENT_EVENTID)))
                        runbookParams.put(Constants.EVENT_EVENTID, ibmmqFilter.getEventEventId());

                    // addToPrimaryDataQueue(ibmmqFilter, runbookParams);
                    // send to the worker queue
                    if (MainBase.esb.sendInternalMessage(workerQueueName, getMessageHandlerName() + ".receiveData", runbookParams))
                    {
                        if (Log.log.isTraceEnabled())
                        {
                            Log.log.trace(">>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>");
                            Log.log.trace("Sent data to worker queue: " + workerQueueName);
                            Log.log.trace(">>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>");
                        }
                    }
                    else
                    {
                        result = false;
                        Log.log.warn("Could not send data to worker queue: " + workerQueueName);
                    }
                } else {
                    result = false;
                }
            }
        } catch (Exception e) {
            Log.log.warn("Error: " + e.getMessage(), e);
            try {
                Thread.sleep(interval);
            } catch (InterruptedException ie) {
                Log.log.warn("sleep interrupted: " + ie.getMessage(), ie);
            }
        }
        
        return result;
    }
    
    public String processBlockingData(String filterName, Map<String, String> params)
    {
        String result = "";
        
        try
        {
            if(StringUtils.isNotBlank(filterName) && params != null)
            {
                IBMmqFilter ibmmqFilter = (IBMmqFilter) filters.get(filterName);
                if (ibmmqFilter != null && ibmmqFilter.isActive()) {
                    if(Log.log.isDebugEnabled())
                    {
                        Log.log.debug("Processing filter: " + ibmmqFilter.getId());
                        Log.log.debug("Data received through IBMMQ gateway: " + params);
                    }
                    
                    Map<String, String> runbookParams = params;
                    
                    if(!params.containsKey(Constants.EVENT_EVENTID) || StringUtils.isBlank(params.get(Constants.EVENT_EVENTID)))
                    {
                        runbookParams.put(Constants.EVENT_EVENTID, ibmmqFilter.getEventEventId());
                    }

                    runbookParams.put("FILTER_ID", filterName);
                    
                    result = instance.receiveData(runbookParams);
                }
                
                else {
                    JSONObject message = new JSONObject();
                    message.accumulate("Message", "The filter is inactive or runbook is not available.");
                    result = message.toString();
                }
            }
        } catch (Exception e) {
            Log.log.warn("Error: " + e.getMessage(), e);
            try {
                Thread.sleep(interval);
            } catch (InterruptedException ie) {
                Log.log.warn("sleep interrupted: " + ie.getMessage(), ie);
            }
        }
        
//        System.out.println("Response is: " + result);
        
        return result;
    }

    @Override
    public void clearAndSetFilters(List<Map<String, Object>> filterList) {
        Log.log.debug("Calling Gateway specific clearAndSetFilter" + (filterList != null ? " " + filterList.size() + ", [" + filterList + "]" : ""));
        
        try {
            super.clearAndSetFilters(filterList);
            
            if(((ConfigReceiveIBMmq)configurations).isActive() && isPrimary())
                deployFilter(filterList);
        } catch (Exception e) {
            Log.log.error(e.getMessage(), e);
        }
    }

    @Override
    protected void cleanFilter(ConcurrentHashMap<String, Filter> undeployedFilters)
    {
        if(((ConfigReceiveIBMmq)configurations).isActive() && isPrimary()) {
            for(Filter filter : undeployedFilters.values())
            {
                IBMmqFilter mqFilter = (IBMmqFilter)filter;
                try
                {
                    if(mqFilter.getType().equalsIgnoreCase(IBMmqFilter.TOPIC))
                        MQService.instance.stopTopicSubscriber(mqFilter.getManager(), mqFilter.getName(), mqFilter.getId());
                    
                    if(mqFilter.getType().equalsIgnoreCase(IBMmqFilter.QUEUE))
                        MQService.instance.stopQueueReceiver(mqFilter.getManager(), mqFilter.getName(), mqFilter.getId());
                    
                    deployedFilters.remove(mqFilter.getId());
                }
                catch (Exception e)
                {
                    Log.log.error(e.getMessage(), e);
                }
            }
        }
    }

    public List<Filter> getOrderedFilters() {
        return orderedFilters;
    }
    
    public void reinitialize() {
        
        super.reinitialize();
        
        //Add customized code here to initilize your server based on configuration and deployed filter data;

        if(isActive() && isPrimary()) {
            // Monitor MQ server connection if it's down
            mqServerAlive = IBMmqHeartbeat.sendHeartbeat();
            
            heartBeat = new IBMmqHeartbeat(this);
            heartBeat.start();
        }
    }
    
    public void redeploy() {
        
        for(Filter filter : orderedFilters) {
            IBMmqFilter mqFilter = (IBMmqFilter)filter;
            String type = (String)mqFilter.getType();
            String filterName = (String)mqFilter.getId();
            
            if(StringUtils.isEmpty(type))
                continue;
                
            if(type.trim().equalsIgnoreCase(IBMmqFilter.TOPIC)) {
                try {
                    int port = mqFilter.getPort().intValue();
                    IBMmqAPI api = new IBMmqAPI(ip, user, pass, port, (String)mqFilter.getManager(), (String)mqFilter.getChannel(), mqFilter.getType(), (String)mqFilter.getName(), 
                                                mqFilter.getAck(), mqFilter.getTransport(), mqFilter.getDurable());
                    api.subscribeToTopic((String)mqFilter.getId());
                    deployedFilters.add(filterName);
                } catch(Exception e) {
                    e.printStackTrace();
                    Log.log.error(e.getMessage(), e);
                }
            }
            
            if(type.trim().equalsIgnoreCase(IBMmqFilter.QUEUE)) {
                try {
                    int port = mqFilter.getPort().intValue();
                    IBMmqAPI api = new IBMmqAPI(ip, user, pass, port, (String)mqFilter.getManager(), (String)mqFilter.getChannel(), mqFilter.getType(), (String)mqFilter.getName(), 
                                                mqFilter.getAck(), mqFilter.getTransport(), null);
                    api.listenToQueue(filterName);
                    deployedFilters.add(filterName);
                } catch(Exception e) {
                    e.printStackTrace();
                    Log.log.error(e.getMessage(), e);
                }
            }
        }
        
        Log.log.info("IBMmqFilter is re-deployed.");
    }
    
    private void deployFilter(List<Map<String, Object>> filterList) throws Exception {
        //Add customized code here to initilize your server based on configuration and deployed filter data;
       
        if(filterList != null && filterList.size() != 0) {
            for(int i=0; i<filterList.size(); i++) {
                Map<String, Object> params = filterList.get(i);
                String filterName = (String)params.get(IBMmqFilter.ID);
                if(StringUtils.isBlank(filterName))
                    continue;
                
                if(deployedFilters.contains(filterName))
                    continue;
                
                String type = (String)params.get(IBMmqFilter.TYPE);
                
                if(StringUtils.isEmpty(type))
                    continue;
                
                if(type.trim().equalsIgnoreCase(IBMmqFilter.TOPIC)) {
                    boolean retry = false;
                    long initTime = System.currentTimeMillis();
                    
                    String portStr = (String)params.get(IBMmqFilter.PORT);
                    int port = (new Integer(portStr)).intValue();
                    String durableStr = (String)params.get(IBMmqFilter.DURABLE);
                    Boolean durable = new Boolean(durableStr);
                    
                    IBMmqAPI api = new IBMmqAPI(ip, user, pass, port, (String)params.get(IBMmqFilter.MANAGER), (String)params.get(IBMmqFilter.CHANNEL), (String)params.get(IBMmqFilter.TYPE), 
                                               (String)params.get(IBMmqFilter.ID), (String)params.get(IBMmqFilter.ACK), (String)params.get(IBMmqFilter.TRANSPORT), durable);
                    
                    do {
                        try {
                            api.subscribeToTopic(filterName);
                            deployedFilters.add(filterName);
                            retry = false;
                        } catch(Exception e) {
                            Log.log.error("Failed to deploy filter: " + (String)params.get(IBMmqFilter.ID));
                            String errorMsg = e.getMessage();
                            Log.log.error(errorMsg, e);
                            
                            if(StringUtils.isNotEmpty(errorMsg) && errorMsg.contains("JMSWMQ0026"))
                                retry = true;
                            Log.log.debug("retry = " + retry);
                            
                            if(!retry)
                                throw e;
                            
                            try {
                                if(System.currentTimeMillis() - initTime > getFailoverInterval()*3)
                                    throw e;
                                Thread.sleep(getHeartbeatInterval());
                            } catch(Exception ee) {}
                        }
                    } while(retry);
                }
                
                else if(type.trim().equalsIgnoreCase(IBMmqFilter.QUEUE)) {
                    try {
                        String portStr = (String)params.get(IBMmqFilter.PORT);
                        int port = (new Integer(portStr)).intValue();
                        
                        IBMmqAPI api = new IBMmqAPI(ip, user, pass, port, (String)params.get(IBMmqFilter.MANAGER), (String)params.get(IBMmqFilter.CHANNEL), (String)params.get(IBMmqFilter.TYPE), 
                                                    (String)params.get(IBMmqFilter.ID), (String)params.get(IBMmqFilter.ACK), (String)params.get(IBMmqFilter.TRANSPORT), null);
                        api.listenToQueue((String)params.get(IBMmqFilter.ID));
                        deployedFilters.add(filterName);
                    } catch(Exception e) {
                        e.printStackTrace();
                        Log.log.error("Failed to deploy filter: " + (String)params.get(IBMmqFilter.ID));
                        Log.log.error(e.getMessage(), e);
                        throw e;
                    }
                }
            }
        }
    }
    
    @Override
    public void deactivate() {
        
        super.deactivate();
        
        for(Filter filter : orderedFilters)
        {
            IBMmqFilter mqFilter = (IBMmqFilter)filter;
            try
            {
                if(mqFilter.getType().equalsIgnoreCase(IBMmqFilter.TOPIC))
                    MQService.instance.stopTopicSubscriber(mqFilter.getManager(), mqFilter.getName(), mqFilter.getId());
                
                if(mqFilter.getType().equalsIgnoreCase(IBMmqFilter.QUEUE))
                    MQService.instance.stopQueueReceiver(mqFilter.getManager(), mqFilter.getName(), mqFilter.getId());
                
                deployedFilters.remove(mqFilter.getId());
            }
            catch (Exception e)
            {
                Log.log.error(e.getMessage(), e);
            }
        }
        
        if(heartBeat != null)
            heartBeat.stopHeartBeat();
    }
    
    @Override
    public void stop() {
        Log.log.warn("Stopping IBMmq gateway ...");
        //Add customized code here to stop the connection with 3rd party system;
        super.stop();
        
        MQService.instance.clearConnections();
        
        if(heartBeat != null)
            heartBeat.stopHeartBeat();
    }

    @Override
    public Filter getFilter(Map<String, Object> params) {
        return new IBMmqFilter((String) params.get(BaseFilter.ID), (String) params.get(BaseFilter.ACTIVE), (String) params.get(BaseFilter.ORDER), (String) params.get(BaseFilter.INTERVAL), 
                               (String) params.get(BaseFilter.EVENT_EVENTID), (String) params.get(BaseFilter.RUNBOOK), (String) params.get(BaseFilter.SCRIPT), 
                               (String) params.get(IBMmqFilter.TRANSPORT), (String) params.get(IBMmqFilter.PORT), (String) params.get(IBMmqFilter.MANAGER), (String) params.get(IBMmqFilter.ACK), 
                               (String) params.get(IBMmqFilter.NAME), (String) params.get(IBMmqFilter.TYPE), (String) params.get(IBMmqFilter.CHANNEL), (String) params.get(IBMmqFilter.DURABLE));
    }
}

