package com.resolve.gateway.ibmmq;

//JMS classes
import javax.jms.*;

import com.ibm.mq.jms.MQTopicConnection;
import com.ibm.mq.jms.MQTopicConnectionFactory;
import com.ibm.mq.jms.MQTopicSession;
import com.ibm.msg.client.wmq.WMQConstants;
import com.resolve.util.Log;

/**
*
* A wrapper class for JMS Topic calls
*
*/
public class JMSTopicUtils
{
    private static String icf = "com.sun.jndi.fscontext.RefFSContextFactory";
    private static String url = "file:/C:/JNDI-Directory";

    private JNDIUtils jndiUtils = null;

    public JMSTopicUtils() throws Exception {
        jndiUtils = new JNDIUtils(icf, url);
    }

    public MQTopicConnection getConnection(String connectionFactoryName) throws Exception {

        MQTopicConnection connection = null;

        try {
            MQTopicConnectionFactory factory = (MQTopicConnectionFactory)jndiUtils.getConnectionFactory("TEST_MGRAConnectionFactory");
            factory.setTransportType(WMQConstants.WMQ_CM_CLIENT);
            factory.setHostName("10.50.1.199");
            factory.setPort(1418);
            factory.setQueueManager("TEST_MGRA");
            factory.setChannel("SYSTEM.ADMIN.SVRCONN");

            connection = (MQTopicConnection)factory.createTopicConnection();
         } catch(Exception e) {
             throw e;
         } catch(Throwable t) {
             t.printStackTrace();
         }

        return connection;
    }

    public Topic getTopic(String topicName) throws Exception {

        return jndiUtils.getTopic("NewsTopic");
    }

} // JMSTopicUtils
