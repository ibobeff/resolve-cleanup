package com.resolve.gateway.ibmmq;

import java.io.File;
import java.util.HashMap;
import java.util.Map;
import org.apache.commons.io.FileUtils;

import com.resolve.rsbase.MainBase;
import com.resolve.gateway.Filter;
import com.resolve.rsremote.ConfigReceiveGateway;
import com.resolve.util.Log;
import com.resolve.util.ObjectProperties;
import com.resolve.util.StringUtils;
import com.resolve.util.XDoc;

public class ConfigReceiveIBMmq extends ConfigReceiveGateway {

    private static final long serialVersionUID = 1L;

    private static final String RECEIVE_IBMMQ_NODE = "./RECEIVE/IBMMQ/";

    private static final String RECEIVE_IBMMQ_FILTER = RECEIVE_IBMMQ_NODE + "FILTER";

    private String queue = "IBMMQ";

    private static final String RECEIVE_IBMMQ_ATTR_ALIVE = RECEIVE_IBMMQ_NODE + "@ALIVE";

    private static final String RECEIVE_IBMMQ_ATTR_USERNAME = RECEIVE_IBMMQ_NODE + "@USERNAME";

    private static final String RECEIVE_IBMMQ_ATTR_IP = RECEIVE_IBMMQ_NODE + "@IP";

    private static final String RECEIVE_IBMMQ_ATTR_PASSWORD = RECEIVE_IBMMQ_NODE + "@PASSWORD";

    private String alive = "";

    private String username = "";

    private String ip = "";

    private String p_assword = "";

    public String getQueue() {
        return queue;
    }

    public void setQueue(String queue) {
        this.queue = queue;
    }

    public String getAlive() {
        return this.alive;
    }

    public void setAlive(String alive) {
        this.alive = alive;
    }

    public String getUsername() {
        return this.username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getIp() {
        return this.ip;
    }

    public void setIp(String ip) {
        this.ip = ip;
    }

    public String getPassword() {
        return this.p_assword;
    }

    public void setPassword(String password) {
        this.p_assword = password;
    }

    public ConfigReceiveIBMmq(XDoc config) throws Exception {
        
        super(config);
        
        define("alive", STRING, RECEIVE_IBMMQ_ATTR_ALIVE);
        define("username", STRING, RECEIVE_IBMMQ_ATTR_USERNAME);
        define("ip", STRING, RECEIVE_IBMMQ_ATTR_IP);
        define("password", SECURE, RECEIVE_IBMMQ_ATTR_PASSWORD);
    }

    @Override
    public String getRootNode() {
        return RECEIVE_IBMMQ_NODE;
    }

    @Override
    public void load() throws Exception {
        try {
            loadAttributes();
            if (isActive()) {
                IBMmqGateway ibmmqGateway = IBMmqGateway.getInstance(this);
                filters = xdoc.getListMapValue(RECEIVE_IBMMQ_FILTER);
                if (filters.size() > 0) {
                    for (Map<String, Object> values : filters) {
                        String filterName = ((String) values.get(IBMmqFilter.ID)).toLowerCase();
                        File scriptFile = getFile(MainBase.main.release.serviceName + "/config/ibmmq/" + filterName + ".groovy");
                        try {
                            if (scriptFile.exists()) {
                                String groovy = FileUtils.readFileToString(scriptFile, "UTF-8");
                                if (StringUtils.isNotBlank(groovy)) {
                                    values.put(IBMmqFilter.SCRIPT, groovy);
                                    Log.log.debug("Loaded groovy script: " + scriptFile);
                                }
                            }
                            ibmmqGateway.setFilter(values);
                        } catch (Exception e) {
                            Log.log.warn(e.getMessage() + ". Skipping file: " + scriptFile.getAbsolutePath());
                        }
                    }
                }
            }
        } catch (Throwable e) {
            setActive(false);
            Log.log.warn("Couldn not load configuration for IBMmq gateway, check blueprint. " + e.getMessage(), e);
        }
    }

    @Override
    public void save() throws Exception {
        File dir = getFile(MainBase.main.release.serviceName + "/config/ibmmq");
        if (!dir.exists()) {
            FileUtils.forceMkdir(dir);
        }
        if (filters != null && isActive()) {
            filters.clear();
            for (Filter filter : IBMmqGateway.getInstance().getFilters().values()) {
                IBMmqFilter ibmmqFilter = (IBMmqFilter) filter;
                String groovy = ibmmqFilter.getScript();
                String scriptFileName;
                File scriptFile = null;
                if (StringUtils.isNotBlank(groovy)) {
                    scriptFileName = ibmmqFilter.getId().toLowerCase() + ".groovy";
                    scriptFile = getFile(MainBase.main.release.serviceName + "/config/ibmmq/" + scriptFileName);
                }
                Map<String, Object> entry = new HashMap<String, Object>();
                putFilterDataIntoEntry(entry, ibmmqFilter);
                filters.add(entry);
                if (scriptFile != null) {
                    try {
                        FileUtils.writeStringToFile(scriptFile, groovy, "UTF-8");
                    } catch (Exception e) {
                        Log.log.warn(e.getMessage() + ". Skipping file: " + scriptFile.getAbsolutePath());
                    }
                }
            }
            xdoc.setListMapValue(RECEIVE_IBMMQ_FILTER, filters);
        }
        if (nameProperties != null && isActive()) {
            nameProperties.clear();
            for (String name : IBMmqGateway.getInstance().getNameProperties().keySet()) {
                Map<String, Object> entry = new HashMap<String, Object>();
                entry.put(NAME_KEY, name);
                nameProperties.add(entry);
                Map props = IBMmqGateway.getInstance().getNameProperties().getNameProperties(name);
                ObjectProperties prop = new ObjectProperties(props);
                prop.save(props);
            }
        }
        saveAttributes();
    }

    private void putFilterDataIntoEntry(Map<String, Object> entry, IBMmqFilter ibmmqFilter) {
        entry.put(IBMmqFilter.ID, ibmmqFilter.getId());
        entry.put(IBMmqFilter.ACTIVE, String.valueOf(ibmmqFilter.isActive()));
        entry.put(IBMmqFilter.ORDER, String.valueOf(ibmmqFilter.getOrder()));
        entry.put(IBMmqFilter.INTERVAL, String.valueOf(ibmmqFilter.getInterval()));
        entry.put(IBMmqFilter.EVENT_EVENTID, ibmmqFilter.getEventEventId());
        entry.put(IBMmqFilter.RUNBOOK, ibmmqFilter.getRunbook());
        entry.put(IBMmqFilter.SCRIPT, ibmmqFilter.getScript());
        entry.put(IBMmqFilter.TRANSPORT, ibmmqFilter.getTransport());
        entry.put(IBMmqFilter.PORT, ibmmqFilter.getPort());
        entry.put(IBMmqFilter.MANAGER, ibmmqFilter.getManager());
        entry.put(IBMmqFilter.ACK, ibmmqFilter.getAck());
        entry.put(IBMmqFilter.NAME, ibmmqFilter.getName());
        entry.put(IBMmqFilter.TYPE, ibmmqFilter.getType());
        entry.put(IBMmqFilter.CHANNEL, ibmmqFilter.getChannel());
        entry.put(IBMmqFilter.DURABLE, ibmmqFilter.getDurable());
    }
}

