package com.resolve.persistence.model;

import java.util.List;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Lob;
import javax.persistence.Table;
import javax.persistence.UniqueConstraint;
import com.resolve.services.interfaces.VO;
import com.resolve.services.hibernate.vo.IBMmqFilterVO;

@Entity
@Table(name = "ibmmq_filter", uniqueConstraints = { @UniqueConstraint(columnNames = { "u_name", "u_queue" }) })
public class IBMmqFilter extends GatewayFilter<IBMmqFilterVO> {

    private static final long serialVersionUID = 1L;

    public IBMmqFilter() {
    }

    public IBMmqFilter(IBMmqFilterVO vo) {
        applyVOToModel(vo);
    }

    private String UTransport;

    private Integer UPort;

    private String UManager;

    private String UAck;

    private String UValue;

    private String UType;

    private String UChannel;

    private Boolean UDurable;

    @Column(name = "u_transport", length = 100)
    public String getUTransport() {
        return this.UTransport;
    }

    public void setUTransport(String uTransport) {
        this.UTransport = uTransport;
    }

    @Column(name = "u_port")
    public Integer getUPort() {
        return this.UPort;
    }

    public void setUPort(Integer uPort) {
        this.UPort = uPort;
    }

    @Column(name = "u_manager", length = 100)
    public String getUManager() {
        return this.UManager;
    }

    public void setUManager(String uManager) {
        this.UManager = uManager;
    }

    @Column(name = "u_ack", length = 100)
    public String getUAck() {
        return this.UAck;
    }

    public void setUAck(String uAck) {
        this.UAck = uAck;
    }

    @Column(name = "u_value", length = 100)
    public String getUValue() {
        return this.UValue;
    }

    public void setUValue(String uValue) {
        this.UValue = uValue;
    }

    @Column(name = "u_type", length = 400)
    public String getUType() {
        return this.UType;
    }

    public void setUType(String uType) {
        this.UType = uType;
    }

    @Column(name = "u_channel", length = 100)
    public String getUChannel() {
        return this.UChannel;
    }

    public void setUChannel(String uChannel) {
        this.UChannel = uChannel;
    }

    @Column(name = "u_durable", length = 1)
    public Boolean getUDurable() {
        return this.UDurable;
    }

    public void setUDurable(Boolean uDurable) {
        this.UDurable = uDurable;
    }

    public IBMmqFilterVO doGetVO() {
        IBMmqFilterVO vo = new IBMmqFilterVO();
        super.doGetBaseVO(vo);
        vo.setUTransport(getUTransport());
        vo.setUPort(getUPort());
        vo.setUManager(getUManager());
        vo.setUAck(getUAck());
        vo.setUValue(getUValue());
        vo.setUType(getUType());
        vo.setUChannel(getUChannel());
        vo.setUDurable(getUDurable());
        return vo;
    }

    @Override
    public void applyVOToModel(IBMmqFilterVO vo) {
        if (vo == null) return;
        super.applyVOToModel(vo);
        if (!VO.STRING_DEFAULT.equals(vo.getUTransport())) this.setUTransport(vo.getUTransport()); else ;
        if (!VO.INTEGER_DEFAULT.equals(vo.getUPort())) this.setUPort(vo.getUPort()); else ;
        if (!VO.STRING_DEFAULT.equals(vo.getUManager())) this.setUManager(vo.getUManager()); else ;
        if (!VO.STRING_DEFAULT.equals(vo.getUAck())) this.setUAck(vo.getUAck()); else ;
        if (!VO.STRING_DEFAULT.equals(vo.getUValue())) this.setUValue(vo.getUValue()); else ;
        if (!VO.STRING_DEFAULT.equals(vo.getUType())) this.setUType(vo.getUType()); else ;
        if (!VO.STRING_DEFAULT.equals(vo.getUChannel())) this.setUChannel(vo.getUChannel()); else ;
        this.setUDurable(vo.getUDurable());
    }

    @Override
    public List<String> ugetCdataProperty() {
        List<String> list = super.ugetCdataProperty();
        list.add("UTransport");
        list.add("UManager");
        list.add("UAck");
        list.add("UValue");
        list.add("UType");
        list.add("UChannel");
        return list;
    }
}

