package com.resolve.rscontrol;

import com.resolve.persistence.model.IBMmqFilter;

public class MIBMmq extends MGateway {

    private static final String MODEL_NAME = IBMmqFilter.class.getSimpleName();

    protected String getModelName() {
        return MODEL_NAME;
    }
}

