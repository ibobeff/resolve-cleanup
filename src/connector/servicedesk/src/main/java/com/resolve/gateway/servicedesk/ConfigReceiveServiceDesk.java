package com.resolve.gateway.servicedesk;

import java.io.File;
import java.util.HashMap;
import java.util.Map;

import org.apache.commons.io.FileUtils;

import com.resolve.rsbase.MainBase;
import com.resolve.gateway.Filter;
import com.resolve.rsremote.ConfigReceiveGateway;
import com.resolve.util.Log;
import com.resolve.util.ObjectProperties;
import com.resolve.util.StringUtils;
import com.resolve.util.XDoc;

public class ConfigReceiveServiceDesk extends ConfigReceiveGateway {

    private static final long serialVersionUID = 1L;

    private static final String RECEIVE_SERVICEDESK_NODE = "./RECEIVE/SERVICEDESK/";

    private static final String RECEIVE_SERVICEDESK_FILTER = RECEIVE_SERVICEDESK_NODE + "FILTER";

    private String queue = "SERVICEDESK";

    private static final String RECEIVE_SERVICEDESK_ATTR_USERNAME = RECEIVE_SERVICEDESK_NODE + "@USERNAME";

    private static final String RECEIVE_SERVICEDESK_ATTR_RESTPORT = RECEIVE_SERVICEDESK_NODE + "@RESTPORT";

    private static final String RECEIVE_SERVICEDESK_ATTR_TIMEDIFFINMILLISEC = RECEIVE_SERVICEDESK_NODE + "@TIMEDIFFINMILLISEC";

    private static final String RECEIVE_SERVICEDESK_ATTR_HOST = RECEIVE_SERVICEDESK_NODE + "@HOST";

    private static final String RECEIVE_SERVICEDESK_ATTR_PASSWORD = RECEIVE_SERVICEDESK_NODE + "@PASSWORD";

    private String username = "";

    private String restport = "";

    private String timediffinmillisec = "";

    private String host = "";

    private String password = "";

    public String getQueue() {
        return queue;
    }

    public void setQueue(String queue) {
        this.queue = queue;
    }

    public String getUsername() {
        return this.username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getRestport() {
        return this.restport;
    }

    public void setRestport(String restport) {
        this.restport = restport;
    }

    public String getTimediffinmillisec() {
        return this.timediffinmillisec;
    }

    public void setTimediffinmillisec(String timediffinmillisec) {
        this.timediffinmillisec = timediffinmillisec;
    }

    public String getHost() {
        return this.host;
    }

    public void setHost(String host) {
        this.host = host;
    }

    public String getPassword() {
        return this.password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public ConfigReceiveServiceDesk(XDoc config) throws Exception {
        super(config);
        define("username", STRING, RECEIVE_SERVICEDESK_ATTR_USERNAME);
        define("restport", STRING, RECEIVE_SERVICEDESK_ATTR_RESTPORT);
        define("timediffinmillisec", STRING, RECEIVE_SERVICEDESK_ATTR_TIMEDIFFINMILLISEC);
        define("host", STRING, RECEIVE_SERVICEDESK_ATTR_HOST);
        define("password", SECURE, RECEIVE_SERVICEDESK_ATTR_PASSWORD);
    }

    @Override
    public String getRootNode() {
        return RECEIVE_SERVICEDESK_NODE;
    }

    @Override
    public void load() throws Exception {
        try {
            loadAttributes();
            if (isActive()) {
                ServiceDeskGateway servicedeskGateway = ServiceDeskGateway.getInstance(this);
                filters = xdoc.getListMapValue(RECEIVE_SERVICEDESK_FILTER);
                if (filters.size() > 0) {
                    for (Map<String, Object> values : filters) {
                        String filterName = ((String) values.get(ServiceDeskFilter.ID)).toLowerCase();
                        File scriptFile = getFile(MainBase.main.release.serviceName + "/config/servicedesk/" + filterName + ".groovy");
                        try {
                            if (scriptFile.exists()) {
                                String groovy = FileUtils.readFileToString(scriptFile, "UTF-8");
                                if (StringUtils.isNotBlank(groovy)) {
                                    values.put(ServiceDeskFilter.SCRIPT, groovy);
                                    Log.log.debug("Loaded groovy script: " + scriptFile);
                                }
                            }
                            servicedeskGateway.setFilter(values);
                        } catch (Exception e) {
                            Log.log.warn(e.getMessage() + ". Skipping file: " + scriptFile.getAbsolutePath());
                        }
                    }
                }
            }
        } catch (Throwable e) {
            setActive(false);
            Log.log.warn("Couldn not load configuration for ServiceDesk gateway, check blueprint. " + e.getMessage(), e);
        }
    }

    @Override
    public void save() throws Exception {
        File dir = getFile(MainBase.main.release.serviceName + "/config/servicedesk");
        if (!dir.exists()) {
            FileUtils.forceMkdir(dir);
        }
        if (filters != null && isActive()) {
            filters.clear();
            for (Filter filter : ServiceDeskGateway.getInstance().getFilters().values()) {
                ServiceDeskFilter servicedeskFilter = (ServiceDeskFilter) filter;
                String groovy = servicedeskFilter.getScript();
                String scriptFileName;
                File scriptFile = null;
                if (StringUtils.isNotBlank(groovy)) {
                    scriptFileName = servicedeskFilter.getId().toLowerCase() + ".groovy";
                    scriptFile = getFile(MainBase.main.release.serviceName + "/config/servicedesk/" + scriptFileName);
                }
                Map<String, Object> entry = new HashMap<String, Object>();
                putFilterDataIntoEntry(entry, servicedeskFilter);
                if (StringUtils.isNotBlank(servicedeskFilter.getLastReadTime()))
                {
                   //Log.log.trace("Save to Config   @@@   " +servicedeskFilter.getId()+"==>"+ servicedeskFilter.getLastReadTime());
                    entry.put(servicedeskFilter.LAST_READ_TIME, servicedeskFilter.getLastReadTime());
                }
                if (StringUtils.isNotBlank(servicedeskFilter.getLastReadIncidentId()))
                {
                   //Log.log.trace("Save to Config   @@@   " +servicedeskFilter.getId()+"==>"+ servicedeskFilter.getLastReadTime());
                    entry.put(servicedeskFilter.LAST_READ_INCIDENT_ID, servicedeskFilter.getLastReadIncidentId());
                }
                filters.add(entry);
                if (scriptFile != null) {
                    try {
                        FileUtils.writeStringToFile(scriptFile, groovy, "UTF-8");
                    } catch (Exception e) {
                        Log.log.warn(e.getMessage() + ". Skipping file: " + scriptFile.getAbsolutePath());
                    }
                }
            }
            xdoc.setListMapValue(RECEIVE_SERVICEDESK_FILTER, filters);
        }
        if (nameProperties != null && isActive()) {
            nameProperties.clear();
            for (String name : ServiceDeskGateway.getInstance().getNameProperties().keySet()) {
                Map<String, Object> entry = new HashMap<String, Object>();
                entry.put(NAME_KEY, name);
                nameProperties.add(entry);
                Map props = ServiceDeskGateway.getInstance().getNameProperties().getNameProperties(name);
                ObjectProperties prop = new ObjectProperties(props);
                prop.save(props);
            }
        }
        saveAttributes();
    }

    private void putFilterDataIntoEntry(Map<String, Object> entry, ServiceDeskFilter servicedeskFilter) {
        entry.put(ServiceDeskFilter.ID, servicedeskFilter.getId());
        entry.put(ServiceDeskFilter.ACTIVE, String.valueOf(servicedeskFilter.isActive()));
        entry.put(ServiceDeskFilter.ORDER, String.valueOf(servicedeskFilter.getOrder()));
        entry.put(ServiceDeskFilter.INTERVAL, String.valueOf(servicedeskFilter.getInterval()));
        entry.put(ServiceDeskFilter.EVENT_EVENTID, servicedeskFilter.getEventEventId());
        entry.put(ServiceDeskFilter.RUNBOOK, servicedeskFilter.getRunbook());
        entry.put(ServiceDeskFilter.SCRIPT, servicedeskFilter.getScript());
        entry.put(ServiceDeskFilter.URLQUERY, servicedeskFilter.getUrlQuery());
    }
}

