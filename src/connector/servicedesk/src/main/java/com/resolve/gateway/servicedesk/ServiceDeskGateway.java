package com.resolve.gateway.servicedesk;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.TimeZone;

import org.apache.commons.httpclient.HttpStatus;
import org.apache.http.HttpHeaders;

import com.resolve.gateway.BaseClusteredGateway;
import com.resolve.gateway.BaseFilter;
import com.resolve.gateway.Filter;
import com.resolve.gateway.MServiceDesk;
import com.resolve.rsbase.MainBase;
import com.resolve.rsremote.ConfigReceiveGateway;
import com.resolve.util.Constants;
import com.resolve.util.Log;
import com.resolve.util.StringUtils;
import com.resolve.util.restclient.RestCallException;
import com.resolve.util.restclient.RestCaller;

import net.sf.json.JSONObject;

public class ServiceDeskGateway extends BaseClusteredGateway
{

    private static volatile ServiceDeskGateway instance = null;
    protected RestCaller restCaller = null;
    public static volatile String basicAuthToken;
    public static volatile long authTokenExpirTimeEpoch;

    private String queue = null;

    public static ServiceDeskGateway getInstance(ConfigReceiveServiceDesk config)
    {
        if (instance == null)
        {
            instance = new ServiceDeskGateway(config);
        }
        return instance;
    }

    /**
     * This method is called once the initialization is done. If it gets called
     * before the initialization it will throw exception.
     *
     * @return
     */
    public static ServiceDeskGateway getInstance()
    {
        if (instance == null)
        {
            throw new RuntimeException("ServiceDesk Gateway is not initialized correctly..");
        }
        else
        {
            return instance;
        }
    }

    private ServiceDeskGateway(ConfigReceiveServiceDesk config)
    {

        super(config);

    }

    @Override
    public String getLicenseCode()
    {
        return "SERVICEDESK";
    }

    @Override
    protected void addAdditionalValues(Map<String, Object> arg0, Map<String, String> arg1)
    {
    }

    @Override
    protected String getGatewayEventType()
    {
        return "SERVICEDESK";
    }

    @Override
    protected String getMessageHandlerName()
    {
        return MServiceDesk.class.getSimpleName();
    }

    @Override
    protected Class<MServiceDesk> getMessageHandlerClass()
    {
        return MServiceDesk.class;
    }

    public String getQueueName()
    {
        return queue + getOrgSuffix();
    }

    @Override
    public void start()
    {
        Log.log.debug("Starting ServiceDesk Gateway");
        super.start();
    }

    @Override
    public void run()
    {
        if (!((ConfigReceiveGateway) configurations).isMock()) super.sendSyncRequest();
        ConfigReceiveServiceDesk config = (ConfigReceiveServiceDesk) configurations;


        while (running)
        {

            try
            {
                long startTime = System.currentTimeMillis();
                if (primaryDataQueue.isEmpty())
                {
                    Log.log.trace("Local Primary Event Queue for " + getGatewayEventType() + " is empty.....");
                    for (Filter filter : orderedFilters)
                    {
                        ServiceDeskFilter serviceDeskFilter = (ServiceDeskFilter) filter;
                        if (shouldFilterExecute(filter, startTime))
                        {
                            // First time set it with the server's date time.
                            long currentSysTime = (long) Math.floor(System.currentTimeMillis() / 1000);
                            Long sreverTimeOffset = Long.parseLong(config.getTimediffinmillisec());
                            long timeDiff = (long) Math.floor(sreverTimeOffset / 1000);
                            if (StringUtils.isEmpty(serviceDeskFilter.getLastReadTime()))
                            {
                                String lastReadTime = String.valueOf(currentSysTime + timeDiff);
                               // lastReadTime = "1478650442";//"1487805455";//
                                serviceDeskFilter.setLastReadTime(lastReadTime);
                                // Log.log.trace("setting the last read time at
                                // (First Time Set)::
                                // "+serviceDeskFilter.getId()+"==>"+
                                // lastReadTime );
                            }

                            Log.log.trace("Filter " + filter.getId() + " executing");
                            List<Map<String, String>> results = invokeService(filter);
                            ArrayList<Long> incidentOpenDateList = new ArrayList<Long>();
                            ArrayList<Long> incidentIdList = new ArrayList<Long>();
                            if (results.size() > 0)
                            {
                                for (Map<String, String> result : results)
                                {

                                    // filter.setLastExecutionTime(arg0);
                                    if (result.containsKey("open_date"))
                                    {
                                        incidentOpenDateList.add(Long.parseLong(result.get("open_date")));

                                    }
                                    if (result.containsKey("id"))
                                    {
                                        incidentIdList.add(Long.parseLong(result.get("id")));

                                    }
                                    addToPrimaryDataQueue(filter, result);
                                }
                                Collections.sort(incidentOpenDateList);
                                Collections.sort(incidentIdList);
                                Log.log.debug("Read total: "+incidentIdList.size()+" Incidents from id: " + incidentIdList.get(0) + " To " + incidentIdList.get(incidentIdList.size() - 1));
                                Log.log.debug("Read Incidents from time: " + incidentOpenDateList.get(0) + " To " + incidentOpenDateList.get(incidentOpenDateList.size() - 1));
                               // System.out.println("Read total: "+incidentIdList.size()+" Incidents from id: " + incidentIdList.get(0) + " To " + incidentIdList.get(incidentIdList.size() - 1));
                               // System.out.println("Read Incidents from time: " + incidentOpenDateList.get(0) + " To " + incidentOpenDateList.get(incidentOpenDateList.size() - 1));

                                // Logic to set last read time...most recent
                                // open_date of the incident
                                if (incidentOpenDateList.size() > 0)
                                {
                                    long lastReadTimeToSet = incidentOpenDateList.get(incidentOpenDateList.size() - 1);
                                    serviceDeskFilter.setLastReadTime(String.valueOf(lastReadTimeToSet));
                                    Log.log.trace("Filter executed last time at: " + lastReadTimeToSet);
                                    Log.log.trace("Updating the last executed last time at(Set to filter)::  " + serviceDeskFilter.getId() + "==>" + lastReadTimeToSet);
                                }
                                else
                                {
                                    Log.log.trace("Last executed time::  " + serviceDeskFilter.getId() + "==>" + serviceDeskFilter.getLastReadTime());
                                }

                                if (incidentIdList.size() > 0)
                                {
                                    long lastIncidentIdToSet = incidentIdList.get(incidentIdList.size() - 1);
                                    serviceDeskFilter.setLastReadIncidentId(String.valueOf(lastIncidentIdToSet));
                                    Log.log.trace("Last read Incident Id : " + lastIncidentIdToSet);
                                    Log.log.trace("Updating the last Incident Id ::  " + serviceDeskFilter.getId() + "==>" + lastIncidentIdToSet);
                                }
                                else
                                {
                                    Log.log.trace("Last Incident Id read on filter::  " + serviceDeskFilter.getId() + "==>" + serviceDeskFilter.getLastReadTime());
                                }

                            }
                            else {
                               // System.out.println("Total Incidents received are: "+results.size());
                                Log.log.debug("Total Incidents received are: "+results.size());
                            }

                        }
                        else
                        {
                            Log.log.trace("Filter:" + filter.getId() + " is inactive or not ready to execute yet.....");
                        }
                    }
                    if (orderedFilters == null || orderedFilters.size() == 0)
                    {
                        Log.log.trace("There is not deployed filter for ServiceDeskGateway");
                    }
                }
                if (orderedFilters.size() == 0)
                {
                    Thread.sleep(interval);
                }
            }
            catch (Exception e)
            {
                Log.log.warn("Warning: " + e.getMessage());
                Log.alert("Instance " + MainBase.main.configId.getGuid() + " " + getLicenseCode() + "-" + getQueueName() + "-" + getInstanceType() + " Polling Failed", "Instance " + MainBase.main.configId.getGuid() + " " + getLicenseCode() + "-" + getQueueName() + "-" + getInstanceType() + " Polling Failed " + "due to " + e.getMessage());
                try
                {
                    Thread.sleep(interval);
                }
                catch (InterruptedException ie)
                {
                    Log.log.warn("sleep interrupted: " + ie.getMessage(), ie);
                }
            }
        }
    }



    /**
     * Add customized code here to get the data as List<Map<String, String>>
     * format for each filter
     *
     * @return List<Map<String, String>>
     */
    public List<Map<String, String>> invokeService(Filter filter)
    {
        List<Map<String, String>> result = new ArrayList<Map<String, String>>();

        // Add customized code here to get data from 3rd party system based the
        // information in the filter;

        try
        {
           String urlQuery = updateQuery(validateQuery(filter), filter);

           String getResponse = ServiceDeskAPI.getObject(urlQuery);
            result = ServiceDeskAPI.parseGetObjectResponse(getResponse);
            if (result.size() == 0)
            {
                Log.log.debug("No records found for query: " + urlQuery);

            }
        }
        catch (Exception e)
        {
            Log.log.error("Error occured while requesting incidents from ServiceDesk. " + e.getMessage());
        }

        return result;
    }

    /**
     * Add customized code here to initilize the connection with 3rd party
     * system
     *
     * @return List<Map<String, String>>
     */
    @Override
    protected void initialize()
    {
        ConfigReceiveServiceDesk config = (ConfigReceiveServiceDesk) configurations;
        queue = config.getQueue().toUpperCase();
        this.gatewayConfigDir = "/config/servicedesk/";

        // Add customized code here to initilize the connection with 3rd party
        // system;
        setupRestcaller();

        try
        {
            doBasicAuth();
        }
        catch (Exception e)
        {
            Log.log.error(e.getMessage(), e);
        }
    }

    /**
     * Add customized code here to do when stop the gateway
     */
    @Override
    public void stop()
    {
        Log.log.warn("Stopping ServiceDesk gateway");
        // Add customized code here to stop the connection with 3rd party
        // system;
        super.stop();
    }



    @Override
    public Filter getFilter(Map<String, Object> params)
    {
        ServiceDeskFilter serviceDeskFilter = new ServiceDeskFilter((String) params.get(BaseFilter.ID), (String) params.get(BaseFilter.ACTIVE), (String) params.get(BaseFilter.ORDER), (String) params.get(BaseFilter.INTERVAL), (String) params.get(BaseFilter.EVENT_EVENTID), (String) params.get(BaseFilter.RUNBOOK), (String) params.get(BaseFilter.SCRIPT), (String) params.get(ServiceDeskFilter.URLQUERY));
        if (params.containsKey(serviceDeskFilter.LAST_READ_TIME) && StringUtils.isNotBlank((String)params.get(serviceDeskFilter.LAST_READ_TIME)))
        {
            serviceDeskFilter.setLastReadTime((String)params.get(serviceDeskFilter.LAST_READ_TIME));
            serviceDeskFilter.setLastReadIncidentId((String)params.get(serviceDeskFilter.LAST_READ_INCIDENT_ID));
            //Log.log.trace("getFilter() GetFilter save to filter @@ ::  "+serviceDeskFilter.getId()+"==> "+ (String)params.get(serviceDeskFilter.LAST_READ_TIME));
        }

        return serviceDeskFilter;
    }

    private void setupRestcaller()
    {
        ConfigReceiveServiceDesk config = (ConfigReceiveServiceDesk) configurations;
        String baseUrl = "http://" + config.getHost() + ":" + config.getRestport() + "/caisd-rest/";
        restCaller = new RestCaller(baseUrl, null, null);
        restCaller.setHttpbasicauthusername(config.getUsername());
        restCaller.setHttpbasicauthpassword(config.getPassword());

    }



    private String validateQuery(Filter filter) throws Exception
    {

        ServiceDeskFilter sDeskFilter = (ServiceDeskFilter) filter;
        String urlQuery = sDeskFilter.getUrlQuery();
        String encodedUrlQuery =null;


        boolean hasVariable = VAR_REGEX_GATEWAY_VARIABLE.matcher(urlQuery).find();
        try
        {
            //lowercase where clause e.g wc= doesn't works in REST query of service desk
            if(urlQuery.contains("wc=")) {
                urlQuery=urlQuery.replaceFirst("wc=", "WC=");
            }
            //uppercase SIZE=  doesn't works in REST query of service desk
            if(urlQuery.contains("SIZE=")) {
                urlQuery=urlQuery.replaceFirst("SIZE=", "size=");
            }
            if(urlQuery.contains("Start=")) {
                urlQuery=urlQuery.replaceFirst("Start=", "start=");
            }
            if(urlQuery.contains("START=")) {
                urlQuery=urlQuery.replaceFirst("START=", "start=");
            }
       if (hasVariable)
            {

                if (urlQuery != null && urlQuery.contains(ServiceDeskFilter.OPEN_DATE))
                {
                    urlQuery = urlQuery.replaceFirst(Constants.VAR_REGEX_GATEWAY_VARIABLE, sDeskFilter.getLastReadTime());
                }

            }
       if (urlQuery.contains(">="))
       {

           urlQuery = urlQuery.replaceAll(">=", "%3E%3D");
       }
       if (urlQuery.contains("<="))
       {

           urlQuery = urlQuery.replaceAll("<=", "%3C%3D");
       }
            if (urlQuery.contains(">"))
            {
                urlQuery = urlQuery.replaceAll(">", "%3E");
            }
            if (urlQuery.contains("<"))
            {
                urlQuery = urlQuery.replaceAll("<", "%3C");
            }


            if (urlQuery.contains(" "))
            {
                urlQuery = urlQuery.replaceAll(" ", "%20");
            }


        }
        catch (Exception e)
        {
            Log.log.error("Error while processing the filter query");
            throw new Exception(e);
        }
         return urlQuery;
       // return encodedUrlQuery;

    }
  /**
   * If reading multiple records or reading multiple records with filter then an "Id > last read id in previous interval "condition  will be appended to the query.
   * and and "open_date >" will be replaced by "open_date>="
   * @param urlQuery
   * @return
   */
    private String updateQuery(String urlQuery, Filter filter)
    {
        ServiceDeskFilter sDeskFilter = (ServiceDeskFilter) filter;
        boolean isIdReplaced = false;
        boolean removeStartCondition=false;
        // Consider two cases
         //case 1: Read Multiple  Query: /in
         //case 2: Read Multiple with filter Query: urlQuery.contains("?WC=")
         // Both cases read all incidents who's open_date is >= last read incident open date and id> last read incidents id in previous interval.


      //Below if is for case 1: Read Multiple  Query: /in

        if(urlQuery.startsWith("in?size="))
        {

            if((sDeskFilter.getLastReadIncidentId() != null) && (sDeskFilter.getLastReadIncidentId() != ""))
            {
                String id = sDeskFilter.getLastReadIncidentId();
                urlQuery = "in"+"?WC=" + "id%3E" + id+"&size=500";
                isIdReplaced =true;
            }

        }
        else if(urlQuery.startsWith("in?start="))
        {

            if((sDeskFilter.getLastReadIncidentId() != null) && (sDeskFilter.getLastReadIncidentId() != ""))
            {
                String id = sDeskFilter.getLastReadIncidentId();
                urlQuery = "in"+"?WC=" + "id%3E" + id+"&size=500";
                isIdReplaced =true;
            }

        }

        else if((!urlQuery.contains("start=")) &&urlQuery.equalsIgnoreCase("in")) {

            if((sDeskFilter.getLastReadIncidentId() != null) && (sDeskFilter.getLastReadIncidentId() != ""))
            {
                String id = sDeskFilter.getLastReadIncidentId();
                if(!isIdReplaced)
                {
                    urlQuery = urlQuery.concat("?WC=" + "id%3E" + id+"&size=500");
                 // urlQuery = urlQuery.replace("?WC=", "WC=" + "id%3E" + id + "%20AND%20");
                    isIdReplaced=true;
                }
            }

            /*else if ((!urlQuery.contains("size=")))
            {
               urlQuery = urlQuery.concat("?size=500");
            }*/


        }
        //case 2: Read Multiple with filter. Query is like: urlQuery.contains("?WC=")
        else if (urlQuery.contains("?WC=") )
        {
           //LastReadIncidentId is not null that means filter is not running for the first time.Its not the first interval of that filter run.
            if((sDeskFilter.getLastReadIncidentId() != null) && (sDeskFilter.getLastReadIncidentId() != ""))
            {

                if (((!urlQuery.contains("open_date%3E%3D")) && !(urlQuery.contains("open_date>="))) && (urlQuery.contains("open_date%3E")))
                {
                    // Update query to change "open_date >= open_date" When filter is running for the first time and the condition is "opne_date >" then that will not be replaced
                    // with open_date>= If not for the first time then condition will be chnaged to "open_date >="
                    urlQuery = urlQuery.replaceFirst("open_date%3E", "open_date%3E%3D");

                }

                // update query to add condition id > last read incident id in
                // previous interval
                String id = sDeskFilter.getLastReadIncidentId();
                // urlQuery = urlQuery.concat("%20AND%20" + "id%3E" + id);
                if (!isIdReplaced)
                {
                    urlQuery = urlQuery.replace("?WC=", "?WC=" + "id%3E" + id + "%20AND%20");
                    if(urlQuery.contains("start="))
                    {
                       String[] parts = urlQuery.split("&start=");
                       //if query is like e.g start=10&size=500 then for the first interval of filter it is executed considering "start" value as a index in the resultset.
                       //After first interval next time onwards start is removed from the query and id is included.
                       if(parts.length == 2) {
                           String part0 = parts[0];
                           String part1 = parts[1];
                           part1 = part1.substring(part1.indexOf("&"), part1.length());
                           urlQuery = part0+part1;


                       }

                    }

                }

            }

            //Default max return size of CASD is 500. So resolve will always return max 500 at every interval
            if ((!urlQuery.contains("&size=")))
            {
                urlQuery = urlQuery.concat("&size=500");
            }

            Log.log.trace("Updated query to add Id: " + urlQuery);
        }
         //System.out.println("\nQuery Updated: "+urlQuery);
        Log.log.debug("\nQuery to be executed: "+urlQuery);
        return urlQuery;

    }

    synchronized public void doBasicAuth() throws Exception
    {

        if (!isValidAuthToken())
        {

            ConfigReceiveServiceDesk config = (ConfigReceiveServiceDesk) configurations;

            restCaller.setUrlSuffix("rest_access");
            // String url = "http://10.50.1.72:8050/caisd-rest/rest_access";

            String response = new String();

            JSONObject basicAuthRequestJSON = new JSONObject();
            try
            {


                basicAuthRequestJSON.put("rest_access", "null");
                String test = basicAuthRequestJSON.toString();

                Map<String, String> reqHeaders = new HashMap<String, String>();
                reqHeaders.put(HttpHeaders.CONTENT_TYPE, "application/json");
                reqHeaders.put("Accept", "application/json");


                Collection<Integer> expectedStatusCodes = new HashSet<Integer>();
                expectedStatusCodes.add(new Integer(HttpStatus.SC_OK));

                response = restCaller.postMethod(null, reqHeaders, basicAuthRequestJSON.toString(), expectedStatusCodes);

                parseBasicAuthResponse(response);
            }
            catch (RestCallException e)
            {
                Log.log.error("Failed to autheticate.");
                Log.log.error("Failed to execute rest request with status code [" + e.statusCode + " ] Reason Phrase: [" + e.reasonPhrase+" ]" + "\n Cause: " + e.fillInStackTrace());
                throw new Exception(e);
            }

            catch (Exception e)
            {
                Log.log.error("Failed to autheticate.");
                e.printStackTrace();
                throw new Exception(e);


            }
        }


    }


    private void parseBasicAuthResponse(String response) throws Exception
    {
        List<Map<String, Object>> resultList = new ArrayList<Map<String, Object>>();

        try
        {
            JSONObject input = JSONObject.fromObject(response);
            String accesskey = input.getJSONObject("rest_access").getString("access_key");
            String expiration_date = input.getJSONObject("rest_access").getString("expiration_date");
            long authTokenEpoch = Long.parseLong(expiration_date);
            basicAuthToken = accesskey;
            authTokenExpirTimeEpoch = authTokenEpoch;

        }
        catch (Exception e)
        {
            Log.log.error(e.getMessage());
            throw new Exception(e);
        }

    }

    public boolean isValidAuthToken()
    {
        // cretae two date objects and compare(current date and the tokendate)

        Boolean isTokenValid = false;

        // If Gateway is doing Authetication first time through initialise()
        if (authTokenExpirTimeEpoch == 0)
        {
            return false;
        }
        try {
            //get current sys time in epoch long
           long currentSysTimeEpoch = System.currentTimeMillis() / 1000;
           long diff_seconds =  authTokenExpirTimeEpoch - currentSysTimeEpoch;
           long diff_hours    = (long) Math.floor(diff_seconds/3600);
           long diff_days     = (long) Math.floor(diff_seconds/86400);
           if (diff_days <= 2)
           {

               return false;
           }
           else
           {
               isTokenValid = true;
               return true;
           }

        }
       /* try
        {
            Compare Time in Dates
            * //
            // authTokenExpirTimeEpoch = 1478814491;
            Date tokenExpirDate = epochToDate(authTokenExpirTimeEpoch);

            Date currentDate = formatDate(new Date());
            if (tokenExpirDate != null && currentDate != null)
            {
                long daysdiff = (tokenExpirDate.getTime() - currentDate.getTime()) / (24 * 60 * 60 * 1000) + 1;

                // renew token before it expires
                if (daysdiff <= 2)
                {

                    return false;
                }
                else
                {
                    isTokenValid = true;
                    return true;
                }

                 * if(tokenExpirDate.compareTo(currentDate) > 0) { return true;
                 * }

            }

        }*/
        catch (Exception e)
        {
            Log.log.error(e.getMessage(), e);
        }

        return isTokenValid;

    }

    public int getDiffBetweenDates(Date d1, Date d2)
    {
        int daysdiff = 0;
        long diff = d2.getTime() - d1.getTime();
        long diffDays = diff / (24 * 60 * 60 * 1000) + 1;
        daysdiff = (int) diffDays;
        return daysdiff;
    }

    private Date epochToDate(Long epochTime) throws Exception
    {
        Date epochToDate = null;

        epochToDate = new Date(epochTime * 1000);
        String format = "yyyy-MM-dd HH:mm:ss";
        SimpleDateFormat sdf = new SimpleDateFormat(format, Locale.getDefault());
        sdf.setTimeZone(TimeZone.getDefault());
        String tokenValidDate = sdf.format(epochToDate);

        epochToDate = sdf.parse(tokenValidDate);

        return epochToDate;

    }

    private Date formatDate(Date dt) throws Exception
    {

        DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss", Locale.getDefault());
        dateFormat.setTimeZone(TimeZone.getDefault());

        Date currentDate = dateFormat.parse(dateFormat.format(dt));

        return dt;
    }

    public Map<String, Object> loadSystemProperties()
    {

        Map<String, Object> properties = new HashMap<String, Object>();

        ConfigReceiveServiceDesk config = (ConfigReceiveServiceDesk) configurations;

        properties.put("HOST", config.getHost());
        properties.put("PORT", config.getRestport());
        properties.put("USERNAME", config.getUsername());
        properties.put("PASSWORD", config.getPassword());

        return properties;
    }

    public boolean checkGatewayConnection() throws Exception
    {
        try
        {

           String query = "bool";
           String response = ServiceDeskAPI.getObject(query);
           //System.out.println("bansdns");
            if (response == null || response.isEmpty())
            {
                throw new Exception("Connection to ServiceDesk server possibly broken.");
            }
            else
            {
                Log.log.debug("Instance " + MainBase.main.configId.getGuid() + " : " + getLicenseCode() + "-" + getQueueName() + "-" + getInstanceType() + " connected to configured ServiceDesk server");
            }
        }
        catch (Exception e)
        {
            Log.alert("Instance " + MainBase.main.configId.getGuid() + " breached " + getLicenseCode() + "-" + getQueueName() + "-" + getInstanceType() + "-ConnectionSelfCheck", "Instance " + MainBase.main.configId.getGuid() + "Breached " + getLicenseCode() + "-" + getQueueName() + "-" + getInstanceType() + "-ConnectionSelfCheck" + " due to " + e.getMessage());

        }

        return true;
    }
}
