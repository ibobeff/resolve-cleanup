package com.resolve.gateway.servicedesk;

import com.resolve.gateway.BaseFilter;
import com.resolve.gateway.RetainValue;

public class ServiceDeskFilter extends BaseFilter {

    public static final String URLQUERY = "URLQUERY";
    public static final String OPEN_DATE = "open_date";
    public final String LAST_READ_TIME = "LAST_READ_TIME";
    public final String LAST_READ_INCIDENT_ID = "LAST_READ_INCIDENT_ID";
    // NEXT_LINK variavle can be used in future if we need to retain next link value received in resultset
    //public final String NEXT_LINK = "NEXT_LINK";
    
    
   private  String lastReadTime;
   private  String lastReadIncidentId;
  // private  String nextLink;
   
  

    private String urlQuery;

    public ServiceDeskFilter(String id, String active, String order, String interval, String eventEventId, String runbook, String script, String urlQuery) {
        super(id, active, order, interval, eventEventId, runbook, script);
        this.urlQuery = urlQuery;
    }

    public String getUrlQuery() {
        return this.urlQuery;
    }

    public void setUrlQuery(String urlQuery) {
        this.urlQuery = urlQuery;
    }
    
  

    @RetainValue
    public  String getLastReadTime()
    {
        return this.lastReadTime;
    }
   
    public  void setLastReadTime(String lastReadTime)
    {
        this.lastReadTime = lastReadTime != null ? lastReadTime.trim() : lastReadTime;
      
        
    } 
    @RetainValue
    public String getLastReadIncidentId()
    {
        return lastReadIncidentId;
    }

    public void setLastReadIncidentId(String lastReadIncidentId)
    {
        this.lastReadIncidentId = lastReadIncidentId;
    }
    
  /* In future if we need to retain next link value received in resultset 
   * @RetainValue
    public String getNextLink()
    {
        return nextLink;
    }

    public void setNextLink(String nextLink)
    {
        this.nextLink = nextLink;
    }*/
}

