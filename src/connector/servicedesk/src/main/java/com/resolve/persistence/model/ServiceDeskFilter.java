package com.resolve.persistence.model;

import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;
import javax.persistence.UniqueConstraint;

import com.resolve.persistence.model.GatewayFilter;
import com.resolve.services.hibernate.vo.ServiceDeskFilterVO;
import com.resolve.services.interfaces.VO;

@Entity
@Table(name = "servicedesk_filter", uniqueConstraints = { @UniqueConstraint(columnNames = { "u_name", "u_queue" }) })
public class ServiceDeskFilter extends GatewayFilter<ServiceDeskFilterVO> {

    private static final long serialVersionUID = 1L;

    public ServiceDeskFilter() {
    }

    public ServiceDeskFilter(ServiceDeskFilterVO vo) {
        applyVOToModel(vo);
    }

    private String UUrlQuery;

    @Column(name = "u_urlquery", length = 400)
    public String getUUrlQuery() {
        return this.UUrlQuery;
    }

    public void setUUrlQuery(String uUrlQuery) {
        this.UUrlQuery = uUrlQuery;
    }

    public ServiceDeskFilterVO doGetVO() {
        ServiceDeskFilterVO vo = new ServiceDeskFilterVO();
        super.doGetBaseVO(vo);
        vo.setUUrlQuery(getUUrlQuery());
        return vo;
    }

    @Override
    public void applyVOToModel(ServiceDeskFilterVO vo) {
        if (vo == null) return;
        super.applyVOToModel(vo);
        if (!VO.STRING_DEFAULT.equals(vo.getUUrlQuery())) this.setUUrlQuery(vo.getUUrlQuery()); else ;
    }

    @Override
    public List<String> ugetCdataProperty() {
        List<String> list = super.ugetCdataProperty();
        list.add("UUrlQuery");
        return list;
    }
}

