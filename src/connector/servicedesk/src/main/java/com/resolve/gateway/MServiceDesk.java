package com.resolve.gateway;

import java.util.Map;
import com.resolve.gateway.servicedesk.ServiceDeskFilter;
import com.resolve.gateway.servicedesk.ServiceDeskGateway;

public class MServiceDesk extends MGateway {

    private static final String RSCONTROL_SET_FILTERS = "MServiceDesk.setFilters";

    private static final ServiceDeskGateway instance = ServiceDeskGateway.getInstance();

    public MServiceDesk() {
        super.instance = instance;
    }

    protected String getSetFilterMethodName() {
        return RSCONTROL_SET_FILTERS;
    }

    /**
     * Populates the {@link Filter} data into the given filterMap {@link Map} instance
     * 
     * @param filterMap
     *      the {@link Map} instance to hold the {@link Filter} data
     * @param filter
     *      a {@link ServiceDeskFilter} instance
     *            
     */
    public void populateGatewaySpecificFilterProperties(Map<String, String> filterMap, Filter filter) {
        if (instance.isActive()) {
            ServiceDeskFilter servicedeskFilter = (ServiceDeskFilter) filter;
            filterMap.put(ServiceDeskFilter.URLQUERY, servicedeskFilter.getUrlQuery());
        }
    }
}

