package com.resolve.gateway.servicedesk;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.apache.commons.httpclient.HttpStatus;
import org.apache.http.HttpHeaders;

import com.resolve.gateway.AbstractGatewayAPI;
import com.resolve.util.Log;
import com.resolve.util.StringUtils;
import com.resolve.util.restclient.RestCallException;
import com.resolve.util.restclient.RestCaller;

import net.sf.json.JSON;
import net.sf.json.JSONArray;
import net.sf.json.JSONObject;
import net.sf.json.JSONSerializer;

public class ServiceDeskAPI extends AbstractGatewayAPI
{
    
    private static String HOSTNAME;
    private static String PORT ;

    private static String USERNAME;
    private static String P_ASSWORD;
 
    private static String BASE_URL;
  
    
    private  static RestCaller restCaller ;
    
    private static ServiceDeskGateway instance = ServiceDeskGateway.getInstance();

    //Status
    public final static String OBJECT_STATUS="crs";
    public final static String UPDATE_STATUS_VALUE="incidentStatus";
    public final static String STATUS_OPEN="5200";
    public final static String STATUS_CLOSE="5201";
    public final static String STATUS_RESERACHING="5202";
    public final static String STATUS_FIP="5203"; //Fix in progress
    public final static String STATUS_CLUNRSLV="5204";
    public final static String STATUS_PO="5205"; //Problem-Open
    public final static String STATUS_PC="5206";//Problem-Closed
    public final static String STATUS_PF="5207";//Problem-Fixed
    public final static String STATUS_WIP="5208"; 
    public final static String STATUS_ACK="5209";
    public final static String STATUS_HOLD="5211"; //On hold, pending action that is outside the SLA scope
    public final static String STATUS_RE="5212";//Incident resolved
    public final static String STATUS_CLREQ="5213" ;//Close Requested
    public final static String STATUS_CNCL="5214";//Cancelled
    public final static String STATUS_AVOID="5215";
    public final static String STATUS_AEUR="5216";//Awaiting End User Response
    public final static String STATUS_AWTVNDR="5217";// //Awaiting Vendor
    public final static String STATUS_PNDCHG="5218";//"Pending Change
    public final static String STATUS_APP="5219";//Approval in Progress
    public final static String STATUS_FXD="5220";//"Fixed";
    public final static String STATUS_SARES="5221"; //"SA-Resolved"
    public final static String STATUS_SAABND="5222";// "SA-Abandon"
    public final static String STATUS_KE="40000";//Known Error
    public final static String STATUS_PRBANCOMP="40001";//Analysis Complete
    public final static String STATUS_PRBREJ="40002"; //Rejected
    

    /*//Priority
    public final static String OBJECT_PRIORITY="pri";
    public final static String UPDATE_PRIORITY="incidentPriority";
    public final static String PRIORITY_LOW="500";
    public final static String PRIORITY_MEDIUM_LOW="501";
    public final static String PRIORITY_MEDIUM="502";
    public final static String PRIORITY_MEDIUM_HIGH="503";
    public final static String PRIORITY_HIGH="504";
    public final static String PRIORITY_NONE="505";*/
    
    
    static {
        
        
        Map<String, Object> properties = instance.loadSystemProperties();
        
        HOSTNAME = (String)properties.get("HOST");
        PORT = (String)properties.get("PORT");
        USERNAME = (String)properties.get("USERNAME");
        P_ASSWORD = (String)properties.get("PASSWORD");
        BASE_URL = "http://" + ServiceDeskAPI.HOSTNAME + ":" + ServiceDeskAPI.PORT  + "/caisd-rest/";
        setupRestcaller();
    }
   
    /**
     *  This method is mainly to use from the Runbook that is being called from RR  
     * @param incidentId : This is the id of incident. This is different than the one that is seen on the servicedesk UI as a ref_num.
     * @return Map of incident attributes and their values
     * @throws Exception
     */
   public static Map<String, String> getIncident(String incidentId) throws Exception
    {
        String query = "in/" + incidentId;
        HashMap<String, String> incident = new HashMap<>();
        try
        {
            List<Map<String, String>> incidentList = ServiceDeskAPI.parseGetObjectResponse(getObject(query));
            if (incidentList.size() == 1)
            {

                incident = (HashMap<String, String>) incidentList.get(incidentList.size() - 1);
                Log.log.debug("Found incident with Id: " + incidentId);
            }
            else {
                Log.log.debug("Incident with given Id does not exist: " + incidentId);
            }

        }
        catch (Exception e)
        {
            Log.log.error("Error retrieving incident information from ServiceDesk.");
            throw new Exception(e);
        }
        
        return incident;
    }
   
   
    static String getObject(String query) throws Exception
    {
        
        if(query == null) {
            Log.log.error("Please enter valid query on Filter. "+query);
            throw new Exception("Query entered on filter is not valid: "+ query);
        }
        String response = new String();
        
         if(!instance.isValidAuthToken()) 
         { 
             instance.doBasicAuth();
         }
         
        try
        {

            Map<String, String> reqHeaders = new HashMap<String, String>();
            reqHeaders.put("X-AccessKey", instance.basicAuthToken);
            reqHeaders.put("Accept", "application/json");

            String inObjectAttributes = "*";

            reqHeaders.put("X-Obj-Attrs", inObjectAttributes);
            //System.out.println("Hello");
            
            String getQuery = BASE_URL + query;
            response = restCaller.getMethod(getQuery, reqHeaders);

            // parseGetIncidentsResponse(response);
        }
        catch (Exception e)
        {
            Log.log.error(e.getMessage(), e);
            e.printStackTrace();
            throw new Exception(e);
        }
        return response;
    }
    
    static  List<Map<String, String>> parseGetObjectResponse(String response) {
      
       List<Map<String, String>> result = new ArrayList<>();
       JSONArray incidentArray = null;
       JSONObject incidentObject = null;
       
       
       if(StringUtils.isEmpty(response)) {
           Log.log.error("No response from REST Get request is received");
           return result;
           
       }
       
       JSONObject input = JSONObject.fromObject(response);
       if (input.containsKey("@COUNT") || input.containsKey("collection_in"))
       {
            //In future if need to save and access the nex/previous link
             /*  JSON link = JSONSerializer.toJSON(JSONObject.fromObject(input.get("collection_in")).get("link"));
               if(link instanceof JSONArray) 
               {
                   List<Map<String, String>> linkMapList = processIncidentData(parseIncidentJsonArray((JSONArray) link, null));
                   for(Map<String,String> linkMap : linkMapList) 
                   {
                       Set<String> linkMapKeySet = linkMap.keySet();
                       for (Iterator<String> linkMapIterator = linkMapKeySet.iterator(); linkMapIterator.hasNext();)
                       {
                           String linkMapKey = linkMapIterator.next();
                           String linkMapValue = linkMap.get(linkMapKey);
                          if(linkMapKey.equals("rel") && linkMapValue.equals("next")) 
                          {
                              
                          }
                           // System.out.println(arrayKey + " = " + arrayValue);

                       }
                   }
                   
               }
               else {
                   
               }*/
               
           
           String toatlIncidents = input.getJSONObject("collection_in").getString("@COUNT");
           Log.log.debug("Total Incidents received are: " + toatlIncidents);
           if (Integer.parseInt(toatlIncidents) > 0)
           {
               JSON expected = JSONSerializer.toJSON(JSONObject.fromObject(input.get("collection_in")).get("in"));
               if (expected instanceof JSONArray)
               {

                   result = processIncidentData(parseIncidentJsonArray((JSONArray) expected, null));
               }
               else if (expected instanceof JSONObject)
               {
                   result = processIncidentData(parseIncidentJsonArray(null, (JSONObject) expected));
               }
               
           }

       }
      /* else if (input.containsKey("in"))
       {
           Set<String> jobjKeys = JSONObject.fromObject(input).keySet();           
           incidentObject = input.getJSONObject("in");
           result = processIncidentData(parseIncidentJsonArray(null, incidentObject));
       }
       else
       {

           Set<String> jobjKeys = JSONObject.fromObject(input).keySet();

           for (String jobjKey : jobjKeys)
           {

               result = processIncidentData(parseIncidentJsonArray(null, JSONObject.fromObject(JSONObject.fromObject(input).get(jobjKey))));
           }
       }*/
       else {
           
           Set<String> jobjKeys = JSONObject.fromObject(input).keySet();     
           for (String jobjKey : jobjKeys)
           {
               incidentObject = input.getJSONObject(jobjKey);
               result = processIncidentData(parseIncidentJsonArray(null, JSONObject.fromObject(JSONObject.fromObject(input).get(jobjKey))));
           }
       }

       return result;
   }
  
    private static List<Map<String, String>> parseIncidentJsonArray(JSONArray value,JSONObject incidentJSONObj)
    {

        JSONArray jsonArray;
        JSONObject valujson = new JSONObject();
        List<Map<String, String>> incidentMapList = new ArrayList<Map<String, String>>();

        if ((value != null) && (value instanceof JSONArray) && (incidentJSONObj == null))
        {
            String jsonArrayString = StringUtils.jsonArrayToString((JSONArray) value);
            jsonArray = (JSONArray) value;

            for (int i = 0; i < jsonArray.size(); i++)
            {
                Map<String, String> incident = new HashMap<String, String>();
                Map<String, String> jsonArrayMap = StringUtils.jsonObjectToMap(jsonArray.getJSONObject(i));

                Set<String> arrayKeySet = jsonArrayMap.keySet();
                for (Iterator<String> arrayIterator = arrayKeySet.iterator(); arrayIterator.hasNext();)
                {
                    String arrayKey = arrayIterator.next();
                    String arrayValue = jsonArrayMap.get(arrayKey);
                    incident.put(arrayKey, arrayValue);

                }

                incidentMapList.add(incident);

            }

        }
        else if ((value == null) && (incidentJSONObj != null) && (incidentJSONObj instanceof JSONObject))
        {
            Map<String, String> incident = new HashMap<String, String>();
            Map<String, String> jsonArrayMap = StringUtils.jsonObjectToMap(incidentJSONObj);
            Set<String> arrayKeySet = jsonArrayMap.keySet();
            for (Iterator<String> arrayIterator = arrayKeySet.iterator(); arrayIterator.hasNext();)
            {
                String arrayKey = arrayIterator.next();
                String arrayValue = jsonArrayMap.get(arrayKey);
                incident.put(arrayKey, arrayValue);
                // System.out.println(arrayKey + " = " + arrayValue);

            }

            incidentMapList.add(incident);
        }

        return incidentMapList;

    }
    
    private static  List<Map<String, String>> processIncidentData(List<Map<String, String>> incidentList)
    {
        JSONObject jsonObject = null;
        List<Map<String, String>> gatewayResultMapIncidentList= new ArrayList();
        int i =0;
        for (Map incidentMap : incidentList)
        {
            Map<String,String> incidentMapForGatewayResult = new HashMap<>();
           // System.out.println("Processing Incident=====> "+i);
            Set<String> keySet = incidentMap.keySet();
            for (Iterator<String> itr = keySet.iterator(); itr.hasNext();)
            {
                String gatewayResultMapKey="";
                String key = itr.next();
                String val = (String) incidentMap.get(key);
                
               
                try
                {
                    jsonObject = (JSONObject) JSONSerializer.toJSON(val);
                }
                catch (Exception e)
                {

                }
                if ((jsonObject != null) && (jsonObject instanceof JSONObject))
                {
                  
                    Map<String, String> incidentValueMap = StringUtils.jsonObjectToMap(jsonObject); // Check value of incident is JSON object
                    Set<String> keySet1 = incidentValueMap.keySet();
                    for (Iterator<String> itr1 = keySet1.iterator(); itr1.hasNext();)
                    {
                        String key1 = itr1.next();
                        String val1 = (String) incidentValueMap.get(key1);
                     //   jsonObjectToMap(val1,new HashMap());
                        if(key1.equals("link")) {
                            JSONObject linkJson = StringUtils.stringToJSONObject(val1);
                            Map<String, String> linkJsonMap = StringUtils.jsonObjectToMap(linkJson);
                            Set<String> linkObjMapKeySet = linkJsonMap.keySet();
                            for(Iterator<String> linkObjMapItr = linkObjMapKeySet.iterator(); linkObjMapItr.hasNext(); ) 
                            {
                                String linkObjMapKey = linkObjMapItr.next();
                                String linkObjMapVal = (String) linkJsonMap.get(linkObjMapKey);
                                gatewayResultMapKey = key+"."+key1+"."+linkObjMapKey;
                                gatewayResultMapKey= gatewayResultMapKey.replaceAll("@", "");
                                incidentMapForGatewayResult.put(gatewayResultMapKey, linkObjMapVal);
                                gatewayResultMapKey="";
                                
                            }
                            
                        }
                        else 
                        {
                            gatewayResultMapKey = key+"."+key1;
                            gatewayResultMapKey= gatewayResultMapKey.replaceAll("@", "");
                            incidentMapForGatewayResult.put(gatewayResultMapKey, val1);
                            gatewayResultMapKey="";
                        }
                       // System.out.println(key1 + "==> " + val1);
                    }
                    jsonObject=null;
                }
                else {
                    key= key.replaceAll("@", "");
                    incidentMapForGatewayResult.put(key, val);
                   // System.out.println(key + "else ==> " + val);
                }

            }
            i++;
          
            gatewayResultMapIncidentList.add(incidentMapForGatewayResult);
            //--
            
            int ii=0;
            Set<String> keySetTest = incidentMapForGatewayResult.keySet();
            for (Iterator<String> keySetTestit = keySetTest.iterator(); keySetTestit.hasNext();)
            {
                //System.out.println("Printing data for "+ii+1);
                String key1 = keySetTestit.next();
                String val1 = (String) incidentMapForGatewayResult.get(key1);
              //  System.out.println(key1 + "~~~~~~ " + val1);
                ii++;
            }
            
            //--
        }
        return gatewayResultMapIncidentList;
    }
       
    
 
    private static void setupRestcaller() {
         
        String baseUrl = "http://" + ServiceDeskAPI.HOSTNAME + ":" + ServiceDeskAPI.PORT  + "/caisd-rest/";
        ServiceDeskAPI.restCaller = new RestCaller(baseUrl, null, null);
        ServiceDeskAPI.restCaller.setHttpbasicauthusername(ServiceDeskAPI.USERNAME);
        ServiceDeskAPI.restCaller.setHttpbasicauthpassword(ServiceDeskAPI.P_ASSWORD);
        
     }
    
    


    
    /**
     * 
     * @param incidentId: Id of the incident
     * @param incidentStatus: Status to update
     *  @param incidentReferenceNum: Reference number of incident. Not required. Can be empty.
     * @return (true/false)status of the update.
     * @throws Exception
     */
    public static boolean updateStatus(String incidentId,String incidentReferenceNum,String incidentStatus) throws Exception
    {

        if (!instance.isValidAuthToken())
        {
            instance.doBasicAuth();
        }
        Boolean isUpdated = false;
        String response = new String();

        try
        {

            JSONObject updateJson = new JSONObject();
            JSONObject updateJson1 = new JSONObject();

            Map<String, String> reqHeaders = new HashMap<String, String>();
            reqHeaders.put("X-AccessKey", instance.basicAuthToken);
            Collection<Integer> expectedStatusCodes = new HashSet<Integer>();
            expectedStatusCodes.add(new Integer(HttpStatus.SC_OK));

            // To update Status

            if (incidentId != null && incidentStatus != null && (!StringUtils.isEmpty(incidentStatus)) && (!StringUtils.isEmpty(incidentId)))
            {
                updateJson.accumulate("@id", incidentStatus);
                updateJson1.accumulate("status", updateJson);

                updateJson.clear();
                updateJson.accumulate("in", updateJson1);

                restCaller.setUrlSuffix("in/");

                response = restCaller.putMethod(incidentId, null, reqHeaders, StringUtils.jsonObjectToMap(updateJson));
                if (incidentReferenceNum != null && StringUtils.isNotEmpty(incidentReferenceNum))
                {
                    Log.log.debug("Status updated successfully for incident: " + incidentReferenceNum + " Status: " + incidentStatus);
                }
                else
                {
                    Log.log.debug("Status updated successfully for incident: " + incidentId + " Status: " + incidentStatus);
                }

                isUpdated = true;
            }
            else
            {
                Log.log.error("Incident ID/Status are required to update incident status. Please provide all required information");
            }

        }

        catch (RestCallException e)
        {
            Log.log.error("Failed to update status of incident.");
            Log.log.error("Failed to execute rest request with status code [" + e.statusCode + " ] Reason Phrase: [" + e.reasonPhrase + " ]" + "\n Cause: " + e.fillInStackTrace());
            throw new Exception(e);
        }
        catch (Exception e)
        {
            Log.log.error("Failed to update status of incident");
            Log.log.error(e.getMessage(), e);
            throw new Exception(e);
        }

        return isUpdated;
    }
    


    /**
     * 
     * @param logComment : Comment to add to the activity log
     * @param incidentId: Id of the incident. 
     * @param incidentReferenceNum: Reference number of the incident. This is not the required field and can be empty.
     * @return status of the update(true/false).
     * @throws Exception
     */
    public static boolean updateAcitivityLog(String incidentId,String incidentReferenceNum,String logComment) throws Exception
    {
        incidentId = "cr:"+incidentId;
        Boolean isUpdated = false;
        String response = new String();
        if (!instance.isValidAuthToken())
        {
            instance.doBasicAuth();

        }
        try
        {
            Map<String, String> updateStatusMap = new HashMap<String,String>();
            JSONObject updateJson = new JSONObject();
            JSONObject updateJson1 = new JSONObject();
            
            Map<String, String> reqHeaders = new HashMap<String, String>();
            reqHeaders.put("X-AccessKey",instance.basicAuthToken);
            Collection<Integer> expectedStatusCodes = new HashSet<Integer>();
            expectedStatusCodes.add(new Integer(HttpStatus.SC_OK));
       
            if (logComment!=null && incidentId!= null && (!StringUtils.isEmpty(logComment)) && (!StringUtils.isEmpty(incidentId)))
            {
                /**
                 * Sample of the alg JSON to be set on POST body
                 * { "alg": { 
                 *  "type": { "-REL_ATTR": "LOG" }, 
                 *   "call_req_id": { "-REL_ATTR": "cr:400055" },
                  "description":"Activity Log created from REST API Java Samples code" } 
                  } */
                
                restCaller.setUrlSuffix("alg/");
                reqHeaders = new HashMap<String, String>();
                reqHeaders.put(HttpHeaders.CONTENT_TYPE, "application/json");
                reqHeaders.put("Accept", "application/json");
                reqHeaders.put("X-AccessKey",instance.basicAuthToken);
                
                //updateJson.accumulate("id", logId);
                updateJson.accumulate("description", logComment);  // "description": "Activity Log created from REST API Java Samples code"
             
                updateStatusMap.put("@REL_ATTR", "LOG");   // "type": { "-REL_ATTR": "LOG" },
                updateJson = updateJson.accumulate("type",StringUtils.mapToJson(updateStatusMap));
                
                updateStatusMap = new HashMap<String, String>();
                updateStatusMap.put("@REL_ATTR", incidentId);
                updateJson = updateJson.accumulate("call_req_id",StringUtils.mapToJson(updateStatusMap));  //"call_req_id": { "-REL_ATTR": "cr:400055" }
                
                updateJson1 = updateJson1.accumulate("alg",updateJson );
               
                response = restCaller.postMethod(null, reqHeaders, updateJson1.toString(), expectedStatusCodes);
                
                if(incidentReferenceNum != null && StringUtils.isNotEmpty(incidentReferenceNum))
                {
                    Log.log.debug("Comment added to the Activity log for incident: "+ incidentReferenceNum);    
                }
                else {
                    Log.log.debug("Comment added to the Activity log for incident: "+ incidentId);
                }
                     
                
                isUpdated=true;

            }
            else
            {

                Log.log.error("Incident ID/Log comment are required to update activity log. Please provide all required information");
            }

        }
        catch (RestCallException e)
        {
            Log.log.error("Failed to add comment to activity log of incident.");
            Log.log.error("Failed to execute rest request with status code [" + e.statusCode + " ] Reason Phrase: [" + e.reasonPhrase+" ]" + "\n Cause: " + e.fillInStackTrace());
            throw new Exception(e);
        }
        catch (Exception e)
        {
            Log.log.error("Failed to add comment to activity log of incident.");
            Log.log.error(e.getMessage(), e);
            throw new Exception(e);
        }

        return isUpdated;
        
    }
    
    /* Below is the method that updates the priority. For the future implementation and reference
     *  
     *  public static boolean updatePriority(String incidentId,String incidentPriority) throws Exception
        {
            Boolean isUpdated = false;
            String response = new String();
            if (!instance.isValidAuthToken())
            {
                instance.doBasicAuth();

            }
            try
            {

                JSONObject updateJson = new JSONObject();
                JSONObject updateJson1 = new JSONObject();

                Map<String, String> reqHeaders = new HashMap<String, String>();
                reqHeaders.put("X-AccessKey", instance.basicAuthToken);
                Collection<Integer> expectedStatusCodes = new HashSet<Integer>();
                expectedStatusCodes.add(new Integer(HttpStatus.SC_OK));

                // To update Status

                updateJson.accumulate("@id", incidentPriority);
                updateJson1.accumulate("priority", updateJson);

                updateJson.clear();
                updateJson.accumulate("in", updateJson1);

                restCaller.setUrlSuffix("in/");

                response = restCaller.putMethod(incidentId, null, reqHeaders, StringUtils.jsonObjectToMap(updateJson));
                isUpdated = true;

            }

            catch (RestCallException e)
            {
                Log.log.error("Failed to update priority of incident.");
                Log.log.error("Failed to execute rest request with status code [" + e.statusCode + " ] Reason Phrase: [" + e.reasonPhrase+" ]" + "\n Cause: " + e.fillInStackTrace());
                throw new Exception(e);
            }
            catch (Exception e)
            {

                Log.log.error("Failed to update priority of incident");
                Log.log.error(e.getMessage(), e);
                throw new Exception(e);
            }

            return isUpdated;
        }*/

    /*    *//**
             * Returns {@link Set} of supported object types.
             * <p>
             * Default no object types are supported returning
             * {@link Collections.EMPTY_SET}. Derived external system specific
             * gateway API must override this method to return {@link Set
             * <String>} of supported object types.
             * 
             * @return {@link Set} of supported object types
             */
    /*
     * public static Set<String> getObjectTypes() { return
     * Collections.emptySet(); }
     * 
     * 
     *//**
       * Creates instance of specified object type in external system.
       * <p>
       * Default attribute name-value pair returned is
       * {@value com.resolve.gateway.GatewayConstants#METHOD_NOT_SUPPORTED_KEY}
       * -true indicating external system specific gateway does not support
       * creating instances of specified object type.
       * <p>
       * If external system specific gateway does support creation of object
       * then it must return {@link Map} of attribute name-value pairs.
       * 
       * @param objType
       *            Type of object to create in external system
       * @param objParams
       *            {@link Map} of object parameters
       * @param userName
       *            User name
       * @param password
       *            Password
       * @return Attribute id-value {@link Map} of created object in external
       *         system
       * @throws Exception
       *             If any error occurs in creating object of specified type in
       *             external system
       */
    /*
     * public static Map<String, String> createObject(String objType,
     * Map<String, String> objParams, String userName, String password) throws
     * Exception { return null; }
     * 
     *//**
       * Reads attributes of specified object in external system.
       * <p>
       * Default attribute id-value pair returned is
       * {@value com.resolve.gateway.GatewayConstants#METHOD_NOT_SUPPORTED_KEY}
       * -true indicating external system specific gateway does not support
       * reading atributes of object.
       * <p>
       * If external system specific gateway does support reading attributes of
       * object then it must return {@link Map} of attribute name-value pairs.
       *
       * @param objType
       *            Object type
       * @param objId
       *            Id of the object to read attributes of from external system
       * @param attribs
       *            {@link List} of attributes of object to read
       * @param userName
       *            User name
       * @param password
       *            Password
       * @return {@link Map} of object attribute id-value pairs
       * @throws Exception
       *             If any error occurs in reading attributes of the object in
       *             external system
       */
    /*
     * public static Map<String, String> readObject(String objType, String
     * objId, List<String> attribs, String userName, String password) throws
     * Exception { return null; }
     * 
     *//**
       * Updates attributes of specified object in external system.
       * <p>
       * Default updated attribute name-value pair returned is
       * {@value com.resolve.gateway.GatewayConstants#METHOD_NOT_SUPPORTED_KEY}
       * -true indicating external system specific gateway does not support
       * updating attributes of object.
       * <p>
       * If external system specific gateway does support updating attributes of
       * object then it must return updated {@link Map} of attribute name-value
       * pairs.
       * 
       * @param objType
       *            Object type
       * @param objId
       *            Id of the object to update attributes of in external system
       * @param updParams
       *            Key-value {@link Map} of object attributes to update
       * @param userName
       *            User name
       * @param password
       *            Password
       * @return Key-value {@link Map} of the updated object attributes
       * @throws Exception
       *             If any error occurs in updating attributes of the object in
       *             external system
       */
    /*
     * public static Map<String, String> updateObject(String objType, String
     * objId, Map<String, String> updParams, String userName, String password)
     * throws Exception { return null; }
     * 
     *//**
       * Deletes specified object from external system.
       * <p>
       * Default deleted attribute name-value pair returned is
       * {@value com.resolve.gateway.GatewayConstants#METHOD_NOT_SUPPORTED_KEY}
       * -true indicating external system specific gateway does not support
       * object deletion.
       * <p>
       * If external system specific gateway does support object deletion then
       * it must return {@link Map} of object deletion operation reult.
       * 
       * @param objType
       *            Object type
       * @param objId
       *            Id of the object to delete from external system
       * @param userName
       *            User name
       * @param password
       *            Password
       * @return Key-value {@link Map} result of object delete operation
       * @throws Exception
       *             If any error occurs in deleting object from external system
       */
    /*
     * public static Map<String, String> deleteObject(String objType, String
     * objId, String userName, String password) throws Exception { return null;
     * }
     * 
     *//**
       * Get list of objects from external system matching specified filter
       * condition.
       * <p>
       * Default implementation returns {@link List} containing {@link Map} with
       * {@value com.resolve.gateway.GatewayConstants#METHOD_NOT_SUPPORTED_KEY}
       * -true indicating external system specific gateway does not support
       * getting objects matching filter condition.
       * <p>
       * If external system specific gateway does support getting objects
       * matching filter condition then it must return {@link List} of
       * {@link Map} of matching objects attribute id-value pairs.
       * 
       * @param objType
       *            Object type
       * @param filter
       *            External system gateway specific filer conditions
       * @param userName
       *            User name
       * @param password
       *            Password
       * @return {@link List} of {@link Map}s of object attribute id-value pairs
       *         matching filter condition
       * @throws Exception
       *             If any error occurs in getting object from external system
       *//*
         * 
         * public static List<Map<String, String>> getObjects(String objType,
         * BaseFilter filter, String userName, String password) throws Exception
         * { List<Map<String, String>> objs = new ArrayList<Map<String,
         * String>>();
         * 
         * objs.add(null);
         * 
         * return objs; }
         */
}

