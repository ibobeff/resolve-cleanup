package com.resolve.rscontrol;

import com.resolve.persistence.model.ServiceDeskFilter;

public class MServiceDesk extends MGateway {

    private static final String MODEL_NAME = ServiceDeskFilter.class.getSimpleName();

    protected String getModelName() {
        return MODEL_NAME;
    }
}

