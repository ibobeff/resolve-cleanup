package com.resolve.services.hibernate.vo;

import com.resolve.util.MappingAnnotation;

public class ServiceDeskFilterVO extends GatewayFilterVO {

    private static final long serialVersionUID = 1L;

    public ServiceDeskFilterVO() {
    }

    private String UUrlQuery;

    @MappingAnnotation(columnName = "URLQUERY")
    public String getUUrlQuery() {
        return this.UUrlQuery;
    }

    public void setUUrlQuery(String uUrlQuery) {
        this.UUrlQuery = uUrlQuery;
    }
    
    @Override
    public int hashCode()
    {
        final int prime = 31;
        int result = super.hashCode();
        result = prime * result + ((UUrlQuery == null) ? 0 : UUrlQuery.hashCode());
        return result;
    }
}

