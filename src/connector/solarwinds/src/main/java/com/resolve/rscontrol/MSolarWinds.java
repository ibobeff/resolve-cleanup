package com.resolve.rscontrol;

import com.resolve.persistence.model.SolarWindsFilter;

public class MSolarWinds extends MGateway {

    private static final String MODEL_NAME = SolarWindsFilter.class.getSimpleName();

    protected String getModelName() {
        return MODEL_NAME;
    }
}

