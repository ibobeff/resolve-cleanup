package com.resolve.gateway.solarwinds;

import java.util.HashMap;
import java.util.Map;
import java.util.List;
import java.util.concurrent.ConcurrentHashMap;
import com.resolve.gateway.BaseClusteredGateway;
import com.resolve.gateway.BaseFilter;
import com.resolve.gateway.Filter;
import com.resolve.gateway.MSolarWinds;
import com.resolve.rsremote.Main;
import com.resolve.util.Constants;
import com.resolve.util.Log;
import com.resolve.util.StringUtils;
import com.resolve.util.metrics.ExecutionMetrics;
import com.resolve.util.metrics.ExecutionMetricsException;

import net.sf.json.JSONArray;
import net.sf.json.JSONObject;

public class SolarWindsGateway extends BaseClusteredGateway {

    private static volatile SolarWindsGateway instance = null;

    private Map<Integer, SolarWindsHttpServer> solarwindsHttpServers = new HashMap<Integer, SolarWindsHttpServer>();
    private Map<String, SolarWindsHttpServer> deployedServlets = new HashMap<String, SolarWindsHttpServer>();

    private String queue = null;

    private SolarWindsHttpServer defaultHttpServer;

    public static SolarWindsGateway getInstance(ConfigReceiveSolarWinds config) {
        if (instance == null) {
            instance = new SolarWindsGateway(config);
        }
        return instance;
    }

    /**
     * This method is called once the initialization is done. If it gets called
     * before the initialization it will throw exception.
     *
     * @return
     */
    public static SolarWindsGateway getInstance() {
        if (instance == null) {
            throw new RuntimeException("SolarWinds Gateway is not initialized correctly..");
        } else {
            return instance;
        }
    }

    private SolarWindsGateway(ConfigReceiveSolarWinds config) {
        super(config);
    }

    @Override
    public String getLicenseCode() {
        return "SOLARWINDS";
    }

    @Override
    protected void addAdditionalValues(Map<String, Object> arg0, Map<String, String> arg1) {
    }

    @Override
    protected String getGatewayEventType() {
        return "SOLARWINDS";
    }

    @Override
    protected String getMessageHandlerName() {
        return MSolarWinds.class.getSimpleName();
    }

    @Override
    protected Class<MSolarWinds> getMessageHandlerClass() {
        return MSolarWinds.class;
    }

    public String getQueueName() {
        return queue + getOrgSuffix();
    }

    @Override
    public void start() {
        Log.log.debug("Starting SolarWinds Gateway");
        super.start();
    }

    @Override
    public void run() {
        super.sendSyncRequest();
        initSolarWindsServers();
    }

    @Override
    protected void initialize() {
        ConfigReceiveSolarWinds config = (ConfigReceiveSolarWinds) configurations;
        queue = config.getQueue().toUpperCase();
        try {
            Log.log.info("Initializing SolarWinds Listener");
            this.gatewayConfigDir = "/config/solarwinds/";

            defaultHttpServer = new SolarWindsHttpServer(config);
        } catch (Exception e) {
            Log.log.error("Failed to config SolarWinds Gateway: " + e.getMessage(), e);
        }
    }

    /**
     * This method processes the message received from the SolarWinds system.
     *
     * @param message
     */
    public boolean processData(String filterName, Map<String, String> params) {
        boolean result = true;
        try {
            if (StringUtils.isNotBlank(filterName) && params != null) {
                SolarWindsFilter solarwindsFilter = (SolarWindsFilter) filters.get(filterName);
                if (solarwindsFilter != null && solarwindsFilter.isActive()) {
                    if (Log.log.isDebugEnabled()) {
                        Log.log.debug("Processing filter: " + solarwindsFilter.getId());
                        Log.log.debug("Data received through SolarWinds gateway: " + params);
                    }
                    Map<String, String> runbookParams = params;
                    if (!params.containsKey(Constants.EVENT_EVENTID) || StringUtils.isBlank(params.get(Constants.EVENT_EVENTID))) {
                        runbookParams.put(Constants.EVENT_EVENTID, solarwindsFilter.getEventEventId());
                    }
                    addToPrimaryDataQueue(solarwindsFilter, runbookParams);
                } else {
                    result = false;
                }
            }
        } catch (Exception e) {
            Log.log.warn("Error: " + e.getMessage(), e);
            try {
                Thread.sleep(interval);
            } catch (InterruptedException ie) {
                Log.log.warn("sleep interrupted: " + ie.getMessage(), ie);
            }
        }
        return result;
    }

    @Override
    public void clearAndSetFilters(List<Map<String, Object>> filterList) {
        Log.log.debug("Calling Gateway specific clearAndSetFilter" + (filterList != null ? " " + filterList.size() + ", [" + filterList + "]" : ""));
        super.clearAndSetFilters(filterList);

        if (isPrimary() && isActive()) {
            try {
                initSolarWindsServers();
            } catch (Exception e) {
                Log.log.error(e.getMessage(), e);
            }
        }
    }

    @Override
    protected void cleanFilter(ConcurrentHashMap<String, Filter> undeployedFilters) {
        //Add customized code here to modify your server when you clear out the deployed filters;

        for(Filter filter : undeployedFilters.values())
        {
            SolarWindsFilter solarwindsFilter = (SolarWindsFilter) filter;

            try
            {
                if(solarwindsFilter.getPort() > 0)
                {
                    SolarWindsHttpServer httpServer = solarwindsHttpServers.get(solarwindsFilter.getPort());
                    httpServer.removeServlet(solarwindsFilter.getId());
                    //if there is no filter/servlet remaining then stop the server on the port
                    if(httpServer.getServlets().isEmpty())
                    {
                        httpServer.stop();
                        solarwindsHttpServers.remove(solarwindsFilter.getPort());
                    }
                }
                else
                {
                    //undeploy the servlet from the default server.
                    defaultHttpServer.removeServlet(solarwindsFilter.getId());
                }
            }
            catch (Exception e)
            {
                Log.log.error(e.getMessage(), e);
            }
        }
    }

    private void initSolarWindsServers() {
        //Add customized code here to initilize your server based on configuration and deployed filter data;

        try {
            if (!defaultHttpServer.isStarted()) {
                defaultHttpServer.init();
                defaultHttpServer.start();

                if (defaultHttpServer.isStarted())
                {
                    defaultHttpServer.addDefaultResolveServlet();
                }
            }

            for (Filter filter : orderedFilters) {
                SolarWindsFilter solarwindsFilter = (SolarWindsFilter) filter;
                //add the URI based servlets to the default server
                if (solarwindsFilter.getPort() == null || solarwindsFilter.getPort() <= 0) {
                    if (defaultHttpServer.isStarted()) {
                        defaultHttpServer.addServlet(solarwindsFilter);
                        deployedServlets.put(filter.getId(), defaultHttpServer);
                    }
                } else {
                    SolarWindsHttpServer httpServer = solarwindsHttpServers.get(solarwindsFilter.getPort());

                    if (httpServer == null)
                    {
                        SolarWindsHttpServer httpServer1 = deployedServlets.get(filter.getId());

                        if (httpServer1 != null)
                        {
                            //port changed
                            httpServer1.removeServlet(filter.getId());
                            deployedServlets.remove(filter.getId());
                        }

                        httpServer = new SolarWindsHttpServer((ConfigReceiveSolarWinds) configurations, solarwindsFilter.getPort(), solarwindsFilter.getSsl());
                        httpServer.init();
                        httpServer.addServlet(solarwindsFilter);
                        httpServer.start();
                    }
                    else
                    {
                        httpServer.addServlet(solarwindsFilter);
                    }

                    solarwindsHttpServers.put(solarwindsFilter.getPort(), httpServer);
                    deployedServlets.put(filter.getId(), httpServer);
                }
            }
        }
        catch (Exception e)
        {
            Log.log.error(e.getMessage(), e);
        }
    }

    @Override
    public void stop() {
        Log.log.warn("Stopping SolarWinds gateway");

        for (SolarWindsHttpServer httpServer : solarwindsHttpServers.values()) {
            try {
                httpServer.stop();
            } catch (Exception e) {
                Log.log.error(e.getMessage(), e);
            }
        }

        try {
            defaultHttpServer.stop();
        } catch (Exception e)  {
            Log.log.error(e.getMessage(), e);
        }

        solarwindsHttpServers.clear();

        super.stop();
    }

    @Override
    public Filter getFilter(Map<String, Object> params) {
        return new SolarWindsFilter((String) params.get(BaseFilter.ID), (String) params.get(BaseFilter.ACTIVE), (String) params.get(BaseFilter.ORDER), (String) params.get(BaseFilter.INTERVAL), (String) params.get(BaseFilter.EVENT_EVENTID), (String) params.get(BaseFilter.RUNBOOK), (String) params.get(BaseFilter.SCRIPT), (String) params.get(SolarWindsFilter.PORT), (String) params.get(SolarWindsFilter.BLOCKING), (String) params.get(SolarWindsFilter.URI), (String) params.get(SolarWindsFilter.SSL));
    }

    public int getWaitTimeout() {

        int timeout = 120;
        
        try {
            ConfigReceiveSolarWinds configReceiveSolarWinds = (ConfigReceiveSolarWinds)configurations;
            String waitTimeout = configReceiveSolarWinds.getWait_timeout();
            timeout = (new Integer(waitTimeout)).intValue();
        } catch(Exception e) {
            Log.log.error(e.getMessage(),e);
        }
        
        return timeout;
    }

    public int getExecuteTimeout() {

        int timeout = 120;
        
        try {
            ConfigReceiveSolarWinds configReceiveSolarWinds = (ConfigReceiveSolarWinds)configurations;
            String executeTimeout = configReceiveSolarWinds.getExecute_timeout();
            timeout = (new Integer(executeTimeout)).intValue();
        } catch(Exception e) {
            Log.log.error(e.getMessage(),e);
        }
        
        return timeout;
    }

    public String getRunbookResult(String problemId, String processId, String wiki) {

        String str = "";
        long timeout = getWaitTimeout()*1000;

        Map<String, Object> params = new HashMap<String, Object>();
        params.put(Constants.EXECUTE_PROBLEMID, problemId);
        params.put(Constants.EXECUTE_PROCESSID, processId);

        try {
            Map<String, Object> result = Main.getESB().call(Constants.ESB_NAME_RSCONTROL, "MAction.getRunbookResult", params, timeout);

            if(result != null)
                str = parseRunbookResult(result, wiki);
        } catch(Exception e) {
            Log.log.error(e.getMessage(), e);
        }

        return str;
    }

    public String parseRunbookResult(Map<String, Object> result, String wiki)
    {
        String problemId = (String)result.get(Constants.EXECUTE_PROBLEMID);
        String processId = (String)result.get(Constants.EXECUTE_PROCESSID);
        String number = (String)result.get(Constants.WORKSHEET_NUMBER);
        String status = (String)result.get(Constants.STATUS);
        String condition = (String)result.get(Constants.EXECUTE_CONDITION);
        String severity = (String)result.get(Constants.EXECUTE_SEVERITY);
        List<Map<String, String>> taskStatus = (List<Map<String, String>>)result.get(Constants.TASK_STATUS);

        JSONObject json = new JSONObject();

        try
        {
            json.accumulate("Wiki Runbook", wiki);
            json.accumulate("Worksheet Id", problemId);
            json.accumulate("Worksheet Number", number);
            json.accumulate("Process Id", processId);
            json.accumulate("Condition", condition);
            json.accumulate("Severity", severity);
            json.accumulate("Status", status);

            if (StringUtils.isNotEmpty(status) && !status.equalsIgnoreCase(Constants.EXECUTE_STATUS_UNKNOWN))
            {
                JSONArray tasks = new JSONArray();

                for (Map<String, String> task:taskStatus)
                {
                    JSONObject taskItem = new JSONObject();
                    taskItem.accumulate("name", task.get(Constants.EXECUTE_TASKNAME));
                    taskItem.accumulate("id", task.get(Constants.EXECUTE_ACTIONID));
                    taskItem.accumulate("completion", task.get(Constants.EXECUTE_COMPLETION));
                    taskItem.accumulate("condition", task.get(Constants.EXECUTE_CONDITION));
                    taskItem.accumulate("severity", task.get(Constants.EXECUTE_SEVERITY));
                    taskItem.accumulate("summary", task.get(Constants.EXECUTE_SUMMARY));
                    taskItem.accumulate("detail", task.get(Constants.EXECUTE_DETAIL));

                    tasks.add(taskItem);
                }

                json.accumulate("Action Tasks", tasks);
            }
        }
        catch(Exception e)
        {
            Log.log.error(e.getMessage(), e);
        }

        return json.toString();

    } // parseRunbookResult()

    public String processBlockingData(String filterName, Map<String, String> params)
    {
        String result = "";

        ExecutionMetrics.INSTANCE.registerEventStart("SolarWinds GW - execute runbook and get result or submit runbook for execution");

        try
        {
            if(StringUtils.isNotBlank(filterName) && params != null)
            {
                SolarWindsFilter httpFilter = (SolarWindsFilter) filters.get(filterName);
                if (httpFilter != null && httpFilter.isActive())
                {
                    if(Log.log.isDebugEnabled())
                    {
                        Log.log.debug("Processing filter: " + httpFilter.getId());
                        Log.log.debug("Data received through HTTP gateway: " + params);
                    }

                    Map<String, String> runbookParams = params;

                    if(!params.containsKey(Constants.EVENT_EVENTID) || StringUtils.isBlank(params.get(Constants.EVENT_EVENTID)))
                    {
                        runbookParams.put(Constants.EVENT_EVENTID, httpFilter.getEventEventId());
                    }

                    runbookParams.put("FILTER_ID", filterName);

                    return instance.receiveData(runbookParams);
                }
                else
                {
                    JSONObject message = new JSONObject();
                    message.accumulate("Message", "The filter is inactive.");
                    result = message.toString();
                }
            }
        }
        catch (Exception e)
        {
            Log.log.warn("Error: " + e.getMessage(), e);
            try
            {
                Thread.sleep(interval);
            }
            catch (InterruptedException ie)
            {
                Log.log.warn("sleep interrupted: " + ie.getMessage(), ie);
            }
        }
        finally
        {
            try
            {
                ExecutionMetrics.INSTANCE.registerEventEnd("SolarWinds GW - execute runbook and get result or submit runbook for execution", 1);
            }
            catch (ExecutionMetricsException eme)
            {
                Log.log.error(eme.getMessage(), eme);
            }
        }

        return result;
    }

    public Map<String, Object> loadSystemProperties() {

        Map<String, Object> properties = new HashMap<String, Object>();

        ConfigReceiveSolarWinds config = (ConfigReceiveSolarWinds)configurations;

        properties.put("HOST", config.getHost());
        properties.put("PORT", config.getPort());
        properties.put("USERNAME", config.getUsername());
        properties.put("PASSWORD", config.getPassword());
        properties.put("SSL", config.getSsl());
        properties.put("TIMEOUT", config.getTimeout());

        return properties;
    }
}