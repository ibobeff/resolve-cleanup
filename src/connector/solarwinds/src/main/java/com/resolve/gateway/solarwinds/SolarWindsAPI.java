package com.resolve.gateway.solarwinds;

import com.resolve.gateway.AbstractGatewayAPI;
import com.resolve.util.restclient.RestCaller;
import com.resolve.util.Log;
import com.resolve.util.StringUtils;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;

import net.sf.json.JSONArray;
import net.sf.json.JSONObject;

public class SolarWindsAPI extends AbstractGatewayAPI
{
    private static String HOSTNAME = "localhost";
    private static String PORT = "17778";
    private static String URI = "/SolarWinds/InformationService/v3/Json";
    private static String USERNAME = null;
    private static String P_ASSWORD = null;
    private static String SSL = "true";
    private static String TIMEOUT = "120";
  
    private static SolarWindsGateway instance = SolarWindsGateway.getInstance();
   
    static {
        try {
            Map<String, Object> properties = instance.loadSystemProperties();
            
            HOSTNAME = (String)properties.get("HOST");
            PORT = (String)properties.get("PORT");
            USERNAME = (String)properties.get("USERNAME");
            P_ASSWORD = (String)properties.get("PASSWORD");
            SSL = (String)properties.get("SSL");
            TIMEOUT = (String)properties.get("TIMEOUT");
        } catch(Exception e) {
            e.printStackTrace();
            Log.log.error(e.getMessage(), e);
        }
    }
  
    /**
     * Get alerts with a specific query.
     * @param query: Required - a valid SQL query
     * @return a list of alert details of the query results
     * @throws Exception
     */
    public static List<Map<String, Object>> getAlerts(String query) throws Exception {
        
        if(StringUtils.isBlank(query))
            throw new Exception("Query string must not be blank.");
        
        List<Map<String, Object>> alertList = new ArrayList<Map<String, Object>>();
        
        try {
            JSONObject queryRequest = new JSONObject();
            queryRequest.put("query", query);
            
            String protocol = (SSL == null || SSL.length() == 0 || SSL.equals("false"))?"http://":"https://";
            RestCaller client = new RestCaller(protocol + HOSTNAME + ":" + PORT, USERNAME, P_ASSWORD, true, false, false, true);
            String response = client.callPost(URI + "/Query", null, queryRequest.toString(), null, "application/json", "application/json");
            Log.log.info(response);
 System.out.println(response);
            if(StringUtils.isNotBlank(response))
                alertList = parseJson(response);
        } catch(Exception e) {
            e.printStackTrace();
            Log.log.error(e.getMessage(), e);
            throw new Exception(e);
        }
        
        return alertList;
    }
    
    /**
     * Get a specific alert by id, its object id and object type.
     * @param alertId: Required - value of alert id
     * @param objectId: Required - value of object id that the alert id is associated with
     * @param alertId: Required - value of object type that the alert id is associated with
     * @return a list of alert details that matches the criteria
     * @throws Exception
     */
    public static List<Map<String, Object>> getAlert(String alertId, String objectId, String objectType) throws Exception {
        
        if(StringUtils.isBlank(alertId) || StringUtils.isBlank(objectId) || StringUtils.isBlank(objectType))
            throw new Exception("alertId, objectId and objectType must be provided.");
        
        StringBuffer sb = new StringBuffer();
        sb.append("SELECT AlertDefID, Notes, AlertMessage, Acknowledged FROM Orion.AlertStatus WHERE AlertDefID='")
          .append(alertId).append("'").append(" AND ").append("ActiveObject='")
          .append(objectId).append("'").append(" AND ").append("ObjectType='").append(objectType).append("'");
        
        String query = sb.toString();
        Log.log.debug(query);
// System.out.println(query);
        List<Map<String, Object>> alertList = new ArrayList<Map<String, Object>>();
        
        try {
            JSONObject queryRequest = new JSONObject();
            queryRequest.put("query", query);
            
            String protocol = (SSL == null || SSL.length() == 0 || SSL.equals("false"))?"http://":"https://";
            RestCaller client = new RestCaller(protocol + HOSTNAME + ":" + PORT, USERNAME, P_ASSWORD, true, false, false, true);
            String response = client.callPost(URI + "/Query", null, queryRequest.toString(), null, "application/json", "application/json");        
//          String response = getResponse(URI + "/Query", queryRequest.toString());
            
            Log.log.info(response);
// System.out.println(response);
            if(StringUtils.isNotBlank(response))
                alertList = parseJson(response);
        } catch(Exception e) {
            e.printStackTrace();
            Log.log.error(e.getMessage(), e);
            throw new Exception(e);
        }
        
        return alertList;
    }
    
    /**
     * Acknowledge and close the status of the alert.
     * @param alertId: Required - value of alert id
     * @param objectId: Required - value of object id that the alert id is associated with
     * @param alertId: Required - value of object type that the alert id is associated with
     * @param alertId: Optional - value of notes
     * @throws Exception
     * @return true if the acknowledge is successful
     */
    public static boolean acknowledge(String alertId, String objectId, String objectType, String notes) throws Exception {

        if(StringUtils.isBlank(alertId) || StringUtils.isBlank(objectId) || StringUtils.isBlank(objectType))
            throw new Exception("alertId, objectId and objectType must be provided.");
        
        JSONArray array1 = new JSONArray();
        JSONArray array2 = new JSONArray();
        
        JSONObject item = new JSONObject();
        item.accumulate("DefinitionId", alertId);
        item.accumulate("ObjectId", objectId);
        item.accumulate("ObjectType", objectType);
        item.accumulate("Notes", notes);

        array2.add(item);
        array1.add(array2);
        
        Log.log.info(array1.toString());
// System.out.println(array1.toString());
        String protocol = (SSL == null || SSL.length() == 0 || SSL.equals("false"))?"http://":"https://";
        RestCaller client = new RestCaller(protocol + HOSTNAME + ":" + PORT, USERNAME, P_ASSWORD, true, false, false, true);
        String response = client.callPost(URI + "/Invoke/Orion.AlertStatus/Acknowledge", null, array1.toString(), null, "application/json", "application/json");
        
//        String response = getResponse(URI + "/Invoke/Orion.AlertStatus/Acknowledge", array1.toString());

        Log.log.info(response);
// System.out.println(response);
        if(response != null && response.equals("true"))
            return true;
        
        return false;
    }
    
    private static void main1(String[] args) {
        
        try {
            // 1. Get alerts with a specified query
            String query = "SELECT AlertDefID, AlertMessage FROM Orion.AlertStatus";
 System.out.println(query);
//            getAlerts(query);
            
            query = "SELECT AlertDefinitions.AlertDefID AS id, AlertDefinitions.Name AS name, AlertStatus.Notes AS notes, AlertStatus.AlertMessage AS message " +
                    "FROM Orion.AlertStatus " +
                    "INNER JOIN Orion.AlertDefinitions " +
                    "ON AlertStatus.AlertDefID = AlertDefinitions.AlertDefID " +
                    "WHERE Acknowledged = 1";
System.out.println(query);
//            getAlerts(query);
            
            // 2. Get alert given id
            getAlert("a9bac0a1-943e-40d1-aad3-565d97cea2f5", "sdfh", "sdgf");
            
            // 3. Acknowledge with alert id
            acknowledge("a9bac0a1-943e-40d1-aad3-565d97cea2f5", "", "", "Ack by REST");
        } catch(Exception e) {
            e.printStackTrace();
        }

//        MathOperation addition = (int a, int b) -> a + b;
    }
    
//    interface MathOperation {
//        int operation(int a, int b);
//    }
    
    /* deprecated method using SolarWinds sample code.
    // String query must be in json format
    
    private static String getResponse(String uri, String query) throws Exception {
        
        SSLContext ctx = trustAllCertificates();

        ClientConfig clientConfig = new DefaultClientConfig();
        
        clientConfig.getProperties().put(HTTPSProperties.PROPERTY_HTTPS_PROPERTIES,
                new HTTPSProperties(new HostnameVerifier() {
                    public boolean verify(String s, SSLSession sslSession) {
                        return true;
                    }
                }, ctx));

        Client client = Client.create(clientConfig);
        client.addFilter(new HTTPBasicAuthFilter(USERNAME, PASSWORD));

        WebResource webResource = client.resource("https://" + HOSTNAME + ":" + PORT + uri);

        String response = webResource.accept("application/json").type("application/json").post(String.class, query);

        return response;
    } 
    
    private static SSLContext trustAllCertificates() {
        // Create a trust manager that does not validate certificate chains
        TrustManager[] trustAllCerts = new TrustManager[] { new X509TrustManager() {
            public X509Certificate[] getAcceptedIssuers() {
                return new X509Certificate[0];
            }

            public void checkClientTrusted(X509Certificate[] certs, String authType) {
            }

            public void checkServerTrusted(X509Certificate[] certs, String authType) {
            }
        } };

        // Install the all-trusting trust manager
        try {
            SSLContext sc = SSLContext.getInstance("TLS");
            sc.init(null, trustAllCerts, new SecureRandom());
            HttpsURLConnection.setDefaultSSLSocketFactory(sc.getSocketFactory());
            return sc;
        } catch (Exception e) {
            return null;
        }
    } */
    
    private static List<Map<String, Object>> parseJson(String response) {

        List<Map<String, Object>> resultList = new ArrayList<Map<String, Object>>();
        
        try {
            JSONObject input = JSONObject.fromObject(response);
            
            JSONArray jsonArray = input.getJSONArray("results");
            
            for(int i=0; i<jsonArray.size(); i++) {
                Map<String, Object> attr = (Map<String, Object>)jsonArray.get(i);
                Set<String> keySet = attr.keySet();
                
                Map<String, Object> item = new HashMap<String, Object>();
                for(Iterator<String> it = keySet.iterator(); it.hasNext();) {
                    String key = it.next();
                    Object value = attr.get(key);
// System.out.println(key + " = " + value);
                    item.put(key, value);
                }
                
                resultList.add(item);
            }
        } catch(Exception e) {
            System.err.println(response);
            e.printStackTrace();
        }
        
        return resultList;
    }

}