package com.resolve.gateway;

import java.util.Map;
import com.resolve.gateway.solarwinds.SolarWindsFilter;
import com.resolve.gateway.solarwinds.SolarWindsGateway;

public class MSolarWinds extends MGateway {

    private static final String RSCONTROL_SET_FILTERS = "MSolarWinds.setFilters";

    private static final SolarWindsGateway instance = SolarWindsGateway.getInstance();

    public MSolarWinds() {
        super.instance = instance;
    }

    protected String getSetFilterMethodName() {
        return RSCONTROL_SET_FILTERS;
    }

    /**
     * Populates the {@link Filter} data into the given filterMap {@link Map} instance
     * 
     * @param filterMap
     *      the {@link Map} instance to hold the {@link Filter} data
     * @param filter
     *      a {@link SolarWindsFilter} instance
     *            
     */
    public void populateGatewaySpecificFilterProperties(Map<String, String> filterMap, Filter filter) {
        if (instance.isActive()) {
            SolarWindsFilter solarwindsFilter = (SolarWindsFilter) filter;
            filterMap.put(SolarWindsFilter.PORT, "" + solarwindsFilter.getPort());
            filterMap.put(SolarWindsFilter.BLOCKING, solarwindsFilter.getBlocking());
            filterMap.put(SolarWindsFilter.URI, solarwindsFilter.getUri());
            filterMap.put(SolarWindsFilter.SSL, "" + solarwindsFilter.getSsl());
        }
    }
}

