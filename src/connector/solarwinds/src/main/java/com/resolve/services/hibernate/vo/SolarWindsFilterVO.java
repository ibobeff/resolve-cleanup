package com.resolve.services.hibernate.vo;

import com.resolve.util.MappingAnnotation;

public class SolarWindsFilterVO extends GatewayFilterVO {

    private static final long serialVersionUID = 1L;

    public SolarWindsFilterVO() {
    }

    private Integer UPort;

    private String UBlocking;

    private String UUri;

    private Boolean USsl;

    @MappingAnnotation(columnName = "PORT")
    public Integer getUPort() {
        return this.UPort;
    }

    public void setUPort(Integer uPort) {
        this.UPort = uPort;
    }

    @MappingAnnotation(columnName = "BLOCKING")
    public String getUBlocking() {
        return this.UBlocking;
    }

    public void setUBlocking(String uBlocking) {
        this.UBlocking = uBlocking;
    }

    @MappingAnnotation(columnName = "URI")
    public String getUUri() {
        return this.UUri;
    }

    public void setUUri(String uUri) {
        this.UUri = uUri;
    }

    @MappingAnnotation(columnName = "SSL")
    public Boolean getUSsl() {
        return this.USsl;
    }

    public void setUSsl(Boolean uSsl) {
        this.USsl = uSsl;
    }
    
    @Override
    public int hashCode()
    {
        final int prime = 31;
        int result = super.hashCode();
        result = prime * result + ((UPort == null) ? 0 : UPort.hashCode());
        result = prime * result + ((UBlocking == null) ? 0 : UBlocking.hashCode());
        result = prime * result + ((UUri == null) ? 0 : UUri.hashCode());
        result = prime * result + ((USsl == null) ? 0 : USsl.hashCode());
        return result;
    }
}

