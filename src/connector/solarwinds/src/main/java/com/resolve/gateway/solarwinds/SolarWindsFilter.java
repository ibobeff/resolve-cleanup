package com.resolve.gateway.solarwinds;

import com.resolve.gateway.BaseFilter;
import com.resolve.util.Log;

public class SolarWindsFilter extends BaseFilter {

    public static final String PORT = "PORT";

    public static final String BLOCKING = "BLOCKING";

    public static final String URI = "URI";

    public static final String SSL = "SSL";

    private Integer port;

    private String blocking;

    private String uri;

    private Boolean ssl;

    public SolarWindsFilter(String id, String active, String order, String interval, String eventEventId, String runbook, String script, String port, String blocking, String uri, String ssl) {
        super(id, active, order, interval, eventEventId, runbook, script);

        try {
            this.port = new Integer(port);
        } catch (Exception e) {
            Log.log.error("port" + " should be in type " + "Integer");
        }

        this.blocking = "1";
        try {
            this.blocking = (new Integer(blocking)).toString();
        } catch (Exception e) {
            if(blocking != null) {
                switch(blocking) {
                    case "Default":
                        this.blocking = "1";
                        break;
                    case "Gateway Script":
                        this.blocking = "2";
                        break;
                    case "Worksheet ID":
                        this.blocking = "3";
                        break;
                    case "Execution Complete":
                        this.blocking = "4";
                        break;
                    default:
                        break;
                }
            }
        }

        this.uri = uri;

        try {
            this.ssl = new Boolean(ssl);
        } catch (Exception e) {
            Log.log.error("ssl" + " should be in type " + "Boolean");
        }
    }

    public Integer getPort() {
        return this.port;
    }

    public void setPort(Integer port) {
        this.port = port;
    }

    public String getBlocking() {
        return this.blocking;
    }

    public void setBlocking(String blocking) {
        this.blocking = blocking;
    }

    public String getUri() {
        return this.uri;
    }

    public void setUri(String uri) {
        this.uri = uri;
    }

    public Boolean getSsl() {
        return this.ssl;
    }

    public void setSsl(Boolean ssl) {
        this.ssl = ssl;
    }
    
    public int getBlockingCode()
    {
        int code = 1;
        
        try {
            code = (new Integer(getBlocking())).intValue();
        } catch(Exception e) {
            e.printStackTrace();
        }
        
        return code;
    }
}

