package com.resolve.gateway.solarwinds;

import java.io.IOException;
import java.util.Map;
import java.util.concurrent.Callable;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.FutureTask;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.TimeoutException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.resolve.gateway.Filter;
import com.resolve.util.Constants;
import com.resolve.util.Log;
import com.resolve.util.ResolveHttpServlet;
import com.resolve.util.StringUtils;
import com.resolve.util.SystemUtil;

import net.sf.json.JSONObject;

@SuppressWarnings("serial")
public class FilterServlet extends ResolveHttpServlet
{
    private static final long serialVersionUID = 7831538166676706839L;

	private final String filterName;
    private static long waitTime = 2000;

    protected ExecutorService executor = Executors.newFixedThreadPool(SystemUtil.getCPUCount());

    private SolarWindsGateway instance = SolarWindsGateway.getInstance();

    public FilterServlet(String filterName)
    {
        this.filterName = filterName;
        // Load waitTime from blueprint.properties
        waitTime = instance.getWaitTimeout();
    }

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException
    {
        Log.log.debug(request.getRequestURI());
//        response.setContentType("text/plain");
        response.setContentType("application/json");
        response.setStatus(HttpServletResponse.SC_OK);
        
        String inputString = StringUtils.toString(request.getInputStream(), "utf-8");
        //these are the post parameters through a form
        Map<String, String> params = StringUtils.urlToMap(inputString);

        //this loop will read from the query string parameters. e.g. ?name1=value&name2=value2
        Map<String, String[]> requestParameters = request.getParameterMap();
        for (String key : requestParameters.keySet())
        {
            if (requestParameters.get(key).length > 0)
            {
                params.put(key, StringUtils.arrayToString(requestParameters.get(key), ","));
            }
        }

        Map<String, Filter> filters = instance.getFilters();
        SolarWindsFilter solarwindsFilter = (SolarWindsFilter)filters.get(filterName);
        
        int blocking = 1;
        
        if(solarwindsFilter.getBlocking() != null)
            blocking = solarwindsFilter.getBlockingCode();
        
        if(blocking > 3) {
            String result = process(instance, filterName, request, response, params);
            response.getWriter().println(result);
        }
        
        else if(blocking > 1) {
            String result = processBlocking(instance, filterName, request, response, params);
            response.getWriter().println(result);
        }

        else {
            executor.execute(new RequestProcessor(instance, filterName, request, response, params));
            
            JSONObject message = new JSONObject();
            message.accumulate("Message", "Success.");
            response.getWriter().println(message.toString());
        }
    } // doGet()

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException
    {
        if(Log.log.isTraceEnabled())
        {
            Log.log.trace(request.getRequestURI());
            Log.log.trace("Client IP: " + request.getRemoteAddr());
            Log.log.trace("Client Host: " + request.getRemoteHost());
        }
        
        response.setContentType("application/json");
        response.setStatus(HttpServletResponse.SC_OK);

        String inputString = StringUtils.toString(request.getInputStream(), "utf-8");
        //these are the post parameters through a form
        Map<String, String> params = StringUtils.urlToMap(inputString);

        //this loop will read from the query string parameters. e.g. ?name1=value&name2=value2
        Map<String, String[]> requestParameters = request.getParameterMap();
        for (String key : requestParameters.keySet())
        {
            if (requestParameters.get(key).length > 0)
            {
                params.put(key, StringUtils.arrayToString(requestParameters.get(key), ","));
            }
        }
        
        Map<String, Filter> filters = instance.getFilters();
        SolarWindsFilter solarwindsFilter = (SolarWindsFilter)filters.get(filterName);
        
        int blocking = 1;
        
        if(solarwindsFilter.getBlocking() != null)
            blocking = solarwindsFilter.getBlockingCode();

        if(blocking > 3) {
            response.setContentType("application/json");
            String result = process(instance, filterName, request, response, params);
            response.getWriter().println(result);
        }
        
        else if(blocking > 1) {
            String result = processBlocking(instance, filterName, request, response, params);
            response.getWriter().println(result);
        }

        else {
            executor.execute(new RequestProcessor(instance, filterName, request, response, params));
            
            JSONObject message = new JSONObject();
            message.accumulate("Message", "HTTP request submitted successfully.");
            response.getWriter().println(message.toString());
        }
    } // doPost()
    
    private String process(final SolarWindsGateway instance, final String filterName, final HttpServletRequest request, final HttpServletResponse response, final  Map<String, String> params) {
        
        String resultStr = processBlocking(instance, filterName, request, response, params);
        Log.log.info("Synchronous HTTPGateway processing result: " + resultStr);
        
        JSONObject message = new JSONObject();

        if(StringUtils.isBlank(resultStr)) {
            message.accumulate("Message", "No information is available at this time.");
            return message.toString();
        }

        try {
            JSONObject obj = JSONObject.fromObject(resultStr);
            String result = obj.getString("Message");
            
            if(StringUtils.isBlank(result)) {
                message.accumulate("Message", "No information is available at this time.");
                return message.toString();
            }
            
            if(result.toLowerCase().indexOf("timed out") != -1 || result.indexOf("Failed to ") != -1)
                return resultStr;
  
            JSONObject json = JSONObject.fromObject(result);
            
            String problemId = json.getString("Worksheet Id");
            String processId = json.getString("Process Id");
            String wiki = json.getString("Wiki Runbook");
            
            Log.log.info("problemId = " + problemId + "!");
            Log.log.info("processId = " + processId + "!");
            
            if(StringUtils.isBlank(problemId) || StringUtils.isBlank(processId)) {
                message.accumulate("Message", "No information is available at this time!");
                return message.toString();
            }
        
            String status = instance.getRunbookResult(problemId, processId, wiki);
            
            if(StringUtils.isBlank(status))
                message.accumulate("Message", "Timed out.");
            
            else 
                message.accumulate("Message", status);

        } catch(Exception e) {
            Log.log.error(e.getMessage(), e);
            message.accumulate("Message", e.getMessage());
        }

        return message.toString();
    } // process()
    
    private String processBlocking(final SolarWindsGateway instance, final String filterName, final HttpServletRequest request, final HttpServletResponse response, final  Map<String, String> params) {
        
        String result = "Response is: ";
        
        FilterCallable callable = new FilterCallable(instance, filterName, params);
        FutureTask<String> futureTask = new FutureTask<String>(callable);
        
        try {
            executor.execute(futureTask);
            
            while (!futureTask.isDone()) {
                try {
                    Log.log.debug("Waiting for FutureTask to complete");
                    result = futureTask.get(waitTime, TimeUnit.SECONDS);
                } catch (InterruptedException | ExecutionException e) {
                    Log.log.error(e.toString());
                    JSONObject message = new JSONObject();
                    message.accumulate("Message", "Failed to process.");
                    result = message.toString();
                    break;
                } catch(TimeoutException e) {
                    futureTask.cancel(true);
                    Log.log.error(e.toString());
                    JSONObject message = new JSONObject();
                    message.accumulate("Message", "Task is timed out.");
                    result = message.toString();
                    break;
                }
            }
        } catch(Exception e) {
            Log.log.error(e.toString());
            JSONObject message = new JSONObject();
            message.accumulate("Message", "Failed to execute task.");
            result = message.toString();
        }
        
        return result;
    } // processBlocking()

    static class RequestProcessor implements Runnable
    {
        final SolarWindsGateway instance;
        final String filterName;
        final HttpServletRequest request;
        final HttpServletResponse response;
        final  Map<String, String> params;
        
        public RequestProcessor(final SolarWindsGateway instance, final String filterName, final HttpServletRequest request, final HttpServletResponse response, final  Map<String, String> params)
        {
            this.instance = instance;
            this.filterName = filterName;
            this.request = request;
            this.response = response;
            this.params = params;
        }

        @Override
        public void run()
        {
            try
            {
                // process the data through a filter.
                boolean isProcessed = instance.processData(filterName, params);

                if (isProcessed)
                {
                    response.setStatus(HttpServletResponse.SC_OK);
//                    response.setContentType("text/plain");
//                    response.getWriter().println("processed");
                }
                else
                {
                    // The requested resource is no longer available at the server
                    // this happens when a filter is no longer active, using http status code is 410
                    response.setStatus(HttpServletResponse.SC_GONE);
                    Log.log.info("Gateway filter " + filterName + " not available.");
                }
            }
            catch (Exception e)
            {
                response.setStatus(HttpServletResponse.SC_BAD_REQUEST);
                Log.log.error(e.getMessage());
            }
        }
        
    } // end of class RequestProcessor
    
    static class FilterCallable implements Callable<String> {
        
        private SolarWindsGateway instance;
        private String filterName;
        private Map<String, String> params;

        
        public FilterCallable(final SolarWindsGateway instance, final String filterName, final Map<String, String> params) {
            this.instance = instance;
            this.filterName = filterName;
            this.params = params;
        }
        
        @Override
        public String call() throws Exception {
            
            return instance.processBlockingData(filterName, params);
        }
        
    } // end of class FilterCallable
    
} // end of class FilterServlet
