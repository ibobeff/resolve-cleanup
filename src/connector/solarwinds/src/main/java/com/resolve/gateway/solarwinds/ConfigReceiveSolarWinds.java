package com.resolve.gateway.solarwinds;

import java.io.File;
import java.util.HashMap;
import java.util.Map;
import org.apache.commons.io.FileUtils;

import com.resolve.rsbase.MainBase;
import com.resolve.gateway.Filter;
import com.resolve.rsremote.ConfigReceiveGateway;
import com.resolve.util.Log;
import com.resolve.util.ObjectProperties;
import com.resolve.util.StringUtils;
import com.resolve.util.XDoc;

public class ConfigReceiveSolarWinds extends ConfigReceiveGateway {

    private static final long serialVersionUID = 1L;

    private static final String RECEIVE_SOLARWINDS_NODE = "./RECEIVE/SOLARWINDS/";

    private static final String RECEIVE_SOLARWINDS_FILTER = RECEIVE_SOLARWINDS_NODE + "FILTER";

    private static final String RECEIVE_SOLARWINDS_ATTR_HTTP_SSLPASSWORD = RECEIVE_SOLARWINDS_NODE + "@HTTP_SSLPASSWORD";

    private static final String RECEIVE_SOLARWINDS_ATTR_EXECUTE_TIMEOUT = RECEIVE_SOLARWINDS_NODE + "@EXECUTE_TIMEOUT";

    private static final String RECEIVE_SOLARWINDS_ATTR_HTTP_PORT = RECEIVE_SOLARWINDS_NODE + "@HTTP_PORT";

    private static final String RECEIVE_SOLARWINDS_ATTR_HTTP_SSL = RECEIVE_SOLARWINDS_NODE + "@HTTP_SSL";

    private static final String RECEIVE_SOLARWINDS_ATTR_HTTP_USERNAME = RECEIVE_SOLARWINDS_NODE + "@HTTP_USERNAME";

    private static final String RECEIVE_SOLARWINDS_ATTR_WAIT_TIMEOUT = RECEIVE_SOLARWINDS_NODE + "@WAIT_TIMEOUT";

    private static final String RECEIVE_SOLARWINDS_ATTR_SSL = RECEIVE_SOLARWINDS_NODE + "@SSL";

    private static final String RECEIVE_SOLARWINDS_ATTR_TIMEOUT = RECEIVE_SOLARWINDS_NODE + "@TIMEOUT";

    private static final String RECEIVE_SOLARWINDS_ATTR_PASSWORD = RECEIVE_SOLARWINDS_NODE + "@PASSWORD";

    private static final String RECEIVE_SOLARWINDS_ATTR_PORT = RECEIVE_SOLARWINDS_NODE + "@PORT";

    private static final String RECEIVE_SOLARWINDS_ATTR_SCRIPT_TIMEOUT = RECEIVE_SOLARWINDS_NODE + "@SCRIPT_TIMEOUT";

    private static final String RECEIVE_SOLARWINDS_ATTR_HTTP_SSLCERTIFICATE = RECEIVE_SOLARWINDS_NODE + "@HTTP_SSLCERTIFICATE";

    private static final String RECEIVE_SOLARWINDS_ATTR_HOST = RECEIVE_SOLARWINDS_NODE + "@HOST";

    private static final String RECEIVE_SOLARWINDS_ATTR_HTTP_PASSWORD = RECEIVE_SOLARWINDS_NODE + "@HTTP_PASSWORD";

    private static final String RECEIVE_SOLARWINDS_ATTR_USERNAME = RECEIVE_SOLARWINDS_NODE + "@USERNAME";
    
    static final Integer DEFAULT_HTTP_PORT = 7779;
    
    static final Integer DEFAULT_WAIT_TIMEOUT = 120;
    
    static final Integer DEFAULT_EXECUTE_TIMEOUT = 120;
    
    static final Boolean DEFAULT_HTTP_SSL = false;

    private String queue = "SOLARWINDS";

    private String http_sslpassword = "";

    private String execute_timeout = "";

    private String http_port = "";

    private String http_ssl = "";

    private String http_username = "";

    private String wait_timeout = "";

    private String ssl = "false";

    private String timeout = "120";

    private String password = "";

    private String port = "8787";

    private String script_timeout = "120";

    private String http_sslcertificate = "";

    private String host = "";

    private String http_password = "";

    private String username = "";

    public String getQueue()
    {
        return queue;
    }

    public void setQueue(String queue)
    {
        this.queue = queue;
    }

    public String getHttp_sslpassword()
    {
        return http_sslpassword;
    }

    public void setHttp_sslpassword(String http_sslpassword)
    {
        this.http_sslpassword = http_sslpassword;
    }

    public String getExecute_timeout()
    {
        return execute_timeout;
    }

    public void setExecute_timeout(String execute_timeout)
    {
        this.execute_timeout = execute_timeout;
    }

    public String getHttp_port()
    {
        return http_port;
    }
    
    public Integer getHttpPort()
    {
        Integer httpPort = DEFAULT_HTTP_PORT;
        
        try {
            httpPort = new Integer(http_port);
        } catch(Exception e) {
            Log.log.error(e.getMessage(),e);
        }
        
        return httpPort;
    }

    public void setHttp_port(String http_port)
    {
        this.http_port = http_port;
    }

    public String getHttp_ssl()
    {
        return http_ssl;
    }

    public Boolean getHttpSsl()
    {
        Boolean httpSSL = DEFAULT_HTTP_SSL;
        
        try {
            httpSSL = new Boolean(http_ssl);
        } catch(Exception e) {
            Log.log.error(e.getMessage(),e);
        }
        
        return httpSSL;
    }
    
    public void setHttp_ssl(String http_ssl)
    {
        this.http_ssl = http_ssl;
    }

    public String getHttp_username()
    {
        return http_username;
    }

    public void setHttp_username(String http_username)
    {
        this.http_username = http_username;
    }

    public String getWait_timeout()
    {
        return wait_timeout;
    }

    public void setWait_timeout(String wait_timeout)
    {
        this.wait_timeout = wait_timeout;
    }

    public String getSsl()
    {
        return ssl;
    }

    public void setSsl(String ssl)
    {
        this.ssl = ssl;
    }

    public String getTimeout()
    {
        return timeout;
    }

    public void setTimeout(String timeout)
    {
        this.timeout = timeout;
    }

    public String getPassword()
    {
        return password;
    }

    public void setPassword(String password)
    {
        this.password = password;
    }

    public String getPort()
    {
        return port;
    }

    public void setPort(String port)
    {
        this.port = port;
    }

    public String getScript_timeout()
    {
        return script_timeout;
    }

    public void setScript_timeout(String script_timeout)
    {
        this.script_timeout = script_timeout;
    }

    public String getHttp_sslcertificate()
    {
        return http_sslcertificate;
    }

    public void setHttp_sslcertificate(String http_sslcertificate)
    {
        this.http_sslcertificate = http_sslcertificate;
    }

    public String getHost()
    {
        return host;
    }

    public void setHost(String host)
    {
        this.host = host;
    }

    public String getHttp_password()
    {
        return http_password;
    }

    public void setHttp_password(String http_password)
    {
        this.http_password = http_password;
    }

    public String getUsername()
    {
        return username;
    }

    public void setUsername(String username)
    {
        this.username = username;
    }

    public ConfigReceiveSolarWinds(XDoc config) throws Exception {
        super(config);
        define("http_sslpassword", SECURE, RECEIVE_SOLARWINDS_ATTR_HTTP_SSLPASSWORD);
        define("execute_timeout", STRING, RECEIVE_SOLARWINDS_ATTR_EXECUTE_TIMEOUT);
        define("http_port", STRING, RECEIVE_SOLARWINDS_ATTR_HTTP_PORT);
        define("http_ssl", STRING, RECEIVE_SOLARWINDS_ATTR_HTTP_SSL);
        define("http_username", STRING, RECEIVE_SOLARWINDS_ATTR_HTTP_USERNAME);
        define("wait_timeout", STRING, RECEIVE_SOLARWINDS_ATTR_WAIT_TIMEOUT);
        define("ssl", STRING, RECEIVE_SOLARWINDS_ATTR_SSL);
        define("timeout", STRING, RECEIVE_SOLARWINDS_ATTR_TIMEOUT);
        define("password", SECURE, RECEIVE_SOLARWINDS_ATTR_PASSWORD);
        define("port", STRING, RECEIVE_SOLARWINDS_ATTR_PORT);
        define("script_timeout", STRING, RECEIVE_SOLARWINDS_ATTR_SCRIPT_TIMEOUT);
        define("http_sslcertificate", STRING, RECEIVE_SOLARWINDS_ATTR_HTTP_SSLCERTIFICATE);
        define("host", STRING, RECEIVE_SOLARWINDS_ATTR_HOST);
        define("http_password", SECURE, RECEIVE_SOLARWINDS_ATTR_HTTP_PASSWORD);
        define("username", STRING, RECEIVE_SOLARWINDS_ATTR_USERNAME);
    }

    @Override
    public String getRootNode() {
        return RECEIVE_SOLARWINDS_NODE;
    }

    @Override
    public void load() throws Exception {
        try {
            loadAttributes();
            if (isActive()) {
                SolarWindsGateway solarwindsGateway = SolarWindsGateway.getInstance(this);
                filters = xdoc.getListMapValue(RECEIVE_SOLARWINDS_FILTER);
                if (filters.size() > 0) {
                    for (Map<String, Object> values : filters) {
                        String filterName = ((String) values.get(SolarWindsFilter.ID)).toLowerCase();
                        File scriptFile = getFile(MainBase.main.release.serviceName + "/config/solarwinds/" + filterName + ".groovy");
                        try {
                            if (scriptFile.exists()) {
                                String groovy = FileUtils.readFileToString(scriptFile, "UTF-8");
                                if (StringUtils.isNotBlank(groovy)) {
                                    values.put(SolarWindsFilter.SCRIPT, groovy);
                                    Log.log.debug("Loaded groovy script: " + scriptFile);
                                }
                            }
                            solarwindsGateway.setFilter(values);
                        } catch (Exception e) {
                            Log.log.warn(e.getMessage() + ". Skipping file: " + scriptFile.getAbsolutePath());
                        }
                    }
                }
            }
        } catch (Throwable e) {
            setActive(false);
            Log.log.warn("Couldn not load configuration for SolarWinds gateway, check blueprint. " + e.getMessage(), e);
        }
    }

    @Override
    public void save() throws Exception {
        File dir = getFile(MainBase.main.release.serviceName + "/config/solarwinds");
        if (!dir.exists()) {
            FileUtils.forceMkdir(dir);
        }
        if (filters != null && isActive()) {
            filters.clear();
            for (Filter filter : SolarWindsGateway.getInstance().getFilters().values()) {
                SolarWindsFilter solarwindsFilter = (SolarWindsFilter) filter;
                String groovy = solarwindsFilter.getScript();
                String scriptFileName;
                File scriptFile = null;
                if (StringUtils.isNotBlank(groovy)) {
                    scriptFileName = solarwindsFilter.getId().toLowerCase() + ".groovy";
                    scriptFile = getFile(MainBase.main.release.serviceName + "/config/solarwinds/" + scriptFileName);
                }
                Map<String, Object> entry = new HashMap<String, Object>();
                putFilterDataIntoEntry(entry, solarwindsFilter);
                filters.add(entry);
                if (scriptFile != null) {
                    try {
                        FileUtils.writeStringToFile(scriptFile, groovy, "UTF-8");
                    } catch (Exception e) {
                        Log.log.warn(e.getMessage() + ". Skipping file: " + scriptFile.getAbsolutePath());
                    }
                }
            }
            xdoc.setListMapValue(RECEIVE_SOLARWINDS_FILTER, filters);
        }
        if (nameProperties != null && isActive()) {
            nameProperties.clear();
            for (String name : SolarWindsGateway.getInstance().getNameProperties().keySet()) {
                Map<String, Object> entry = new HashMap<String, Object>();
                entry.put(NAME_KEY, name);
                nameProperties.add(entry);
                Map props = SolarWindsGateway.getInstance().getNameProperties().getNameProperties(name);
                ObjectProperties prop = new ObjectProperties(props);
                prop.save(props);
            }
        }
        saveAttributes();
    }

    private void putFilterDataIntoEntry(Map<String, Object> entry, SolarWindsFilter solarwindsFilter) {
        entry.put(SolarWindsFilter.ID, solarwindsFilter.getId());
        entry.put(SolarWindsFilter.ACTIVE, String.valueOf(solarwindsFilter.isActive()));
        entry.put(SolarWindsFilter.ORDER, String.valueOf(solarwindsFilter.getOrder()));
        entry.put(SolarWindsFilter.INTERVAL, String.valueOf(solarwindsFilter.getInterval()));
        entry.put(SolarWindsFilter.EVENT_EVENTID, solarwindsFilter.getEventEventId());
        entry.put(SolarWindsFilter.RUNBOOK, solarwindsFilter.getRunbook());
        entry.put(SolarWindsFilter.SCRIPT, solarwindsFilter.getScript());
        entry.put(SolarWindsFilter.PORT, solarwindsFilter.getPort());
        entry.put(SolarWindsFilter.BLOCKING, solarwindsFilter.getBlocking());
        entry.put(SolarWindsFilter.URI, solarwindsFilter.getUri());
        entry.put(SolarWindsFilter.SSL, solarwindsFilter.getSsl());
    }
}

