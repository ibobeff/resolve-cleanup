package com.resolve.rscontrol;

import com.resolve.persistence.model.SCOMFilter;

public class MSCOM extends MGateway {

    private static final String MODEL_NAME = SCOMFilter.class.getSimpleName();

    protected String getModelName() {
        return MODEL_NAME;
    }
}

