package com.resolve.gateway;

import java.util.Map;
import com.resolve.gateway.scom.SCOMFilter;
import com.resolve.gateway.scom.SCOMGateway;

public class MSCOM extends MGateway {

    private static final String RSCONTROL_SET_FILTERS = "MSCOM.setFilters";

    private static final SCOMGateway instance = SCOMGateway.getInstance();

    public MSCOM() {
        super.instance = instance;
    }

    protected String getSetFilterMethodName() {
        return RSCONTROL_SET_FILTERS;
    }

    /**
     * Populates the {@link Filter} data into the given filterMap {@link Map} instance
     * 
     * @param filterMap
     *      the {@link Map} instance to hold the {@link Filter} data
     * @param filter
     *      a {@link SCOMFilter} instance
     *            
     */
    public void populateGatewaySpecificFilterProperties(Map<String, String> filterMap, Filter filter) {
        if (instance.isActive()) {
            SCOMFilter scomFilter = (SCOMFilter) filter;
            filterMap.put(SCOMFilter.ALERTRESOLUTIONSTATE, scomFilter.getAlertResolutionState());
        }
    }
}

