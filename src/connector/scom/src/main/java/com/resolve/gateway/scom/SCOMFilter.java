package com.resolve.gateway.scom;

import com.resolve.gateway.BaseFilter;
import com.resolve.util.Log;

public class SCOMFilter extends BaseFilter {

    public static final String ALERTRESOLUTIONSTATE = "ALERTRESOLUTIONSTATE";

    private String alertResolutionState;

    public SCOMFilter(String id, String active, String order, String interval, String eventEventId, String runbook, String script, String alertResolutionState) {
        super(id, active, order, interval, eventEventId, runbook, script);
        this.alertResolutionState = alertResolutionState;
    }

    public String getAlertResolutionState() {
        return this.alertResolutionState;
    }

    public void setAlertResolutionState(String alertResolutionState) {
        this.alertResolutionState = alertResolutionState;
    }
}

