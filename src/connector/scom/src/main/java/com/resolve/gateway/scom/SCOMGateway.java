package com.resolve.gateway.scom;

import java.util.Map;
import javax.xml.datatype.XMLGregorianCalendar;


import org.datacontract.schemas._2004._07.microsoft_enterprisemanagement_common.AlertGenerated;
import java.util.List;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.ArrayList;

import com.resolve.rsbase.MainBase;
import com.resolve.gateway.BaseClusteredGateway;
import com.resolve.gateway.BaseFilter;
import com.resolve.gateway.Filter;
import com.resolve.gateway.MSCOM;
import com.resolve.util.Log;


import com.resolve.rsremote.ConfigReceiveGateway;

public class SCOMGateway extends BaseClusteredGateway
{

    private static volatile SCOMGateway instance = null;



    private String queue = null;

    public static SCOMGateway getInstance(ConfigReceiveSCOM config)
    {
        if (instance == null)
        {
            instance = new SCOMGateway(config);
        }
        return instance;
    }

    public ConfigReceiveSCOM getConfig()
    {

        return (ConfigReceiveSCOM) configurations;
    }

    /**
     * This method is called once the initialization is done. If it gets called
     * before the initialization it will throw exception.
     *
     * @return
     */
    public static SCOMGateway getInstance()
    {
        if (instance == null)
        {
            throw new RuntimeException("SCOM Gateway is not initialized correctly..");
        }
        else
        {
            return instance;
        }
    }

    private SCOMGateway(ConfigReceiveSCOM config)
    {
        super(config);
    }

    @Override
    public String getLicenseCode()
    {
        return "SCOM";
    }

    @Override
    protected void addAdditionalValues(Map<String, Object> arg0, Map<String, String> arg1)
    {
    }

    @Override
    protected String getGatewayEventType()
    {
        return "SCOM";
    }

    @Override
    protected String getMessageHandlerName()
    {
        return MSCOM.class.getSimpleName();
    }

    @Override
    protected Class<MSCOM> getMessageHandlerClass()
    {
        return MSCOM.class;
    }

    /** Get the queue name for this gateway.
     * <p>
     * <em>NOTE: Hack Alert </em>
     * Because the design has changed for Queue naming with
     * the addition of ORG information, we have to create our 
     * Rabbit Queues with ORG names. That is not available in
     * old (pre version 6) Resolve. So we have to protect the
     * call. 
     * </p>
     * <p>
     * This is a HORRIBLE coding practice. 
     * </p>
     * @see com.resolve.gateway.Gateway#getQueueName()
     * @return String - the Queue name, with or without an Org name.
     */
    public String getQueueName()
    {
    	String queueOrgName = lookupOrgNameIfInVersion();
    	
        return queue + queueOrgName;
    }

	/** Get the org suffix, even if it does not exist.
	 * <p>
	 * This bit of hackery is here because we are following the dubious
	 * practice of having versions of Resolve, but not versions of gateways.
	 * Thus, the same code has to work in old and new versions of resolve.
	 * Even when new features are added to the base class that have not been
	 * back ported to old versions.
	 * </p>
	 * <p>
	 * This is an example of coupling by inheritance. We will always face this
	 * if classes in the core resolve that gateways inherit from can change
	 * underneath the gateway classes. 
	 * </p>
	 * @return String -- org name if available, otherwise the empty String ""
	 */
	private String lookupOrgNameIfInVersion() {
    	try {
    		Class<?> superClass = this.getClass().getSuperclass();
        	Method[] superMethods;
			if (null != superClass) {
				superMethods = superClass.getMethods();
				for (Method method: superMethods) {
					if (method.getName().equals("getOrgSuffix")) {
						return (String) method.invoke(this, (Object[])null);
					}
				}
			}
		} 
    	catch (SecurityException | 
    			IllegalAccessException | 
    			IllegalArgumentException | 
    			InvocationTargetException e) {
    		// SWALOW
			// Yes this is a bad practice, but (in order):
    		// We do not use a security manager.
    		// We know how that the getOrgSuffix method is accessible in a sub-class.
    		// We know that the getOrgSuffix method does not take any parameters.
    		// And finally, We know it does not throw any Exceptions (that I can find).
		} 
    	return "";
	}

    @Override
    public void start()
    {
        Log.log.debug("Starting SCOM Gateway");
        super.start();
    }

    @Override
    public void run()
    {
        if (!((ConfigReceiveGateway) configurations).isMock()) super.sendSyncRequest();
        while (running)
        {
            try
            {
                long startTime = System.currentTimeMillis();
                if (primaryDataQueue.isEmpty())
                {
                    Log.log.trace("Local Primary Event Queue for " + getGatewayEventType() + " is empty.....");
                    for (Filter filter : orderedFilters)
                    {
                        if (shouldFilterExecute(filter, startTime))
                        {
                            Log.log.trace("Filter " + filter.getId() + " executing");
                            List<Map<String, String>> results = invokeService(filter);
                            for (Map<String, String> result : results)
                            {
                                addToPrimaryDataQueue(filter, result);
                            }
                        }
                        else
                        {
                            Log.log.trace("Filter:" + filter.getId() + " is inactive or not ready to execute yet.....");
                        }
                    }
                    if (orderedFilters == null || orderedFilters.size() == 0)
                    {
                        Log.log.trace("There is not deployed filter for SCOMGateway");
                    }
                }
                if (orderedFilters.size() == 0)
                {
                    Thread.sleep(interval);
                }
            }
            catch (Exception e)
            {
                Log.log.warn("Warning: " + e.getMessage());
                Log.alert("Instance " + MainBase.main.configId.getGuid() + " " + getLicenseCode() + "-" + getQueueName() + "-" + getInstanceType() + " Polling Failed", "Instance " + MainBase.main.configId.getGuid() + " " + getLicenseCode() + "-" + getQueueName() + "-" + getInstanceType() + " Polling Failed " + "due to " + e.getMessage());
                try
                {
                    Thread.sleep(interval);
                }
                catch (InterruptedException ie)
                {
                    Log.log.warn("sleep interrupted: " + ie.getMessage(), ie);
                }
            }
        }
    }

    /**
     * Add customized code here to get the data as List<Map<String, String>>
     * format for each filter
     *
     * @return List<Map<String, String>>
     */
    public List<Map<String, String>> invokeService(Filter filter) throws Exception
    {

        // Add customized code here to get data from 3rd party system based the
        // information in the filter;
        List<AlertGenerated> alertGeneratedList = new ArrayList<>();
        List<Map<String, String>> result = new ArrayList<Map<String, String>>();
        AlertGenerated alert;
        ConfigReceiveSCOM config = (ConfigReceiveSCOM) configurations;
        String connectorState = "";
        Boolean isInitialsed = false;


            connectorState = SCOMAPI.getConnectorState();
            if (connectorState.equals((SCOMAPI.CONNECTOR_STATE_INITIALIZED)))
            {
                isInitialsed=true;
            }

            else if (connectorState.equals(SCOMAPI.CONNECTOR_STATE_UNINITIALIZED))
            {

                isInitialsed = SCOMAPI.createConnector(connectorState);
                if(!isInitialsed) {
                    Log.log.error("Failed to initialize connector with Id: "+config.getConnectorId());
                    throw new Exception("Failed to initialize connector with Id: "+config.getConnectorId());
                }


            }
            else if (connectorState.equals(SCOMAPI.CONNECTOR_STATE_NOTEXIST))
            {

                isInitialsed = SCOMAPI.createConnector(connectorState);
                if(!isInitialsed) {
                    Log.log.error("Failed to create new connector with Id: "+config.getConnectorId()+" Please make sure connector is created and subscribed to receive alerts, before you use.");
                    throw new Exception("Failed to create new connector with Id: "+config.getConnectorId());
                }
                else {
                    Log.log.debug("New Connector with Id: " + config.getConnectorId() + " is created in SCOM. Please create subscription for the connector to receive alerts from SCOM.");
                }

            }
         if (isInitialsed)
        {
            //After connector is nitialised it will check the value of resolution state. If null then will throw exception.
             //
             Short userRequestedState=null;
            SCOMFilter scomFilter = (SCOMFilter) filter;

             userRequestedState = validateRequestedState(scomFilter.getAlertResolutionState());


            if (userRequestedState != null)
            {
                alertGeneratedList = SCOMAPI.getMonitoringAlerts();
                Log.log.debug("Total number of alerts received on connector are(Note:before filtering on requested state): " + alertGeneratedList.size());

                for (int i = 0; i < alertGeneratedList.size(); i++)
                {
                    alert = alertGeneratedList.get(i);
                    if (userRequestedState.equals(alert.getResolutionState()))
                    {
                        result.add(SCOMAPI.getAlertAttributesMap(alert));
                    }
                    else {
                        Log.log.debug("Skipping alert."+"Requested State: "+userRequestedState+" Alert State: "+alert.getResolutionState());
                    }
                }
                if (result.size() > 0)
                {
                    Log.log.debug("Total number of alerts fetched after filtering on requesetd state: "+userRequestedState+" are: " + result.size());
                    acknowledgeAlert(alertGeneratedList, result.size());
                }
            }
            else
            {
                Log.log.error("Please specify correct resolution state in blueprint properties file. It could be(New,Acknowledged,Assigned To Engineering,Awaiting Evidence,Closed,Processed,Resolved,Scheduled,Send Alert) or any valid state that is present on SCOM");
                throw new Exception("Please set correct resolution state in blueprint properties.");
            }

        }
            else {
                Log.log.error("Connector: " + config.getConnectorId() + " is invalid. Connector should be in Initialized/Uninitialized state.Please make sure connector is created before you use.");
                throw new Exception("Failed to fetch alerts on connector with Id: "+config.getConnectorId());
            }


        return result;
    }


    private Short validateRequestedState(String requestedState) throws NumberFormatException{


        Short userRequestedState=null;
        try
        {
         userRequestedState = Short.parseShort(requestedState);
        }
        catch(NumberFormatException e) {
            userRequestedState = getStateCode(requestedState);

        }
        finally {
            return userRequestedState;
     }

    }



    private short getStateCode(String alertState)
    {

        Short alertStateCode = null;
        if (alertState.equalsIgnoreCase("New"))
        {
            return SCOMAPI.RESOLUTION_STATE_NEW;
        }
        else if (alertState.equalsIgnoreCase("Acknowledged"))
        {
            return SCOMAPI.RESOLUTION_STATE_ACKNOWLEDGED;
        }

        else if (alertState.equalsIgnoreCase("Assigned To Engineering"))
        {
            return SCOMAPI.RESOLUTION_STATE_ASIGNED_TO_ENGINEERING;
        }
        else if (alertState.equalsIgnoreCase("Awaiting Evidence"))
        {
            return SCOMAPI.RESOLUTION_STATE_AWAITING_EVIDENCE;
        }
        else if (alertState.equalsIgnoreCase("Closed"))
        {
            return SCOMAPI.RESOLUTION_STATE_CLOSED;
        }
        else if (alertState.equalsIgnoreCase("Processed"))
        {
            return SCOMAPI.RESOLUTION_STATE_PROCESSED;

        }
        else if (alertState.equalsIgnoreCase("Resolved"))
        {
            return SCOMAPI.RESOLUTION_STATE_RESOLVED;
        }
        else if (alertState.equalsIgnoreCase("Scheduled"))
        {
            return SCOMAPI.RESOLUTION_STATE_SCHEDULED;
        }
        else if (alertState.equalsIgnoreCase("Send Alert"))
        {
            return SCOMAPI.RESOLUTION_STATE_SENDALERT;
        }
        return alertStateCode;


    }
/**
 * Reads the most recent last modifified time of the alert in the alert list and use that to acknowledge alerts on the connector.
 *
 * The way we are approaching is:
 * Chnage the resolution state of alert through runbook
 * Set the subscription criteria on the connector to retrieve alerts depending on their state.
 *  * @param alertGeneratedList
 * @param resultMapSize
 */

    private void acknowledgeAlert(List<AlertGenerated> alertGeneratedList, int resultMapSize)
    {
        AlertGenerated alert;
        XMLGregorianCalendar prevAlertRaisedTime = null;
        XMLGregorianCalendar currentAlertRaisedTime = null;
        XMLGregorianCalendar ackTime = null;
        ConfigReceiveSCOM config = (ConfigReceiveSCOM) configurations;
        for (int i = 0; i < alertGeneratedList.size(); i++)
        {
            alert = alertGeneratedList.get(i);

            // Below logic to read the latest timestamp of the alert in the list
            // and use that to acknowledge alerts on the connector
            if (alertGeneratedList.size() == 1)
            {
                // ackTime = alert.getTimeRaised();
                ackTime = alert.getLastModified();
            }
            else
            {
                if (i > 0)
                {
                    // currentAlertRaisedTime = alert.getTimeRaised();
                    currentAlertRaisedTime = alert.getLastModified();

                    if ((prevAlertRaisedTime != null) && (currentAlertRaisedTime != null))
                    {

                        int timeComapreResult = prevAlertRaisedTime.compare(currentAlertRaisedTime);
                        if (timeComapreResult > 0)
                        { // If result is positive, then prevAlertRaisedTime is
                          // "later" than latestAlertRaisedTime
                            ackTime = prevAlertRaisedTime;
                        }
                        else if (timeComapreResult < 0)
                        {
                            ackTime = currentAlertRaisedTime;
                        }
                        else
                        { // both times are equal
                            ackTime = currentAlertRaisedTime;
                        }
                    }

                }
            }
            // prevAlertRaisedTime = alert.getTimeRaised();
            prevAlertRaisedTime = alert.getLastModified();

        }

        try
        {
            if (ackTime != null)
            {
                //ackTime.setMinute(ackTime.getMinute()+3);
                SCOMAPI.acknowledgeAlerts(ackTime);
                Log.log.debug("Acknowledging alerts up until the given bookmark.(UTC)" + ackTime.toString());
            }
            else
            {
                Log.log.error("Error acknowledging alerts. Time received to acknowledge alerts is not valid.");
            }

        }
        catch (Exception e)
        {
            Log.log.error("Failed to acknowledge alerts on connector id: " + config.getConnectorId());
            Log.log.error(e.getMessage(), e);
        }

    }


    /**
     * Add customized code here to initilize the connection with 3rd party
     * system
     *
     * @return List<Map<String, String>>
     */
    @Override
    protected void initialize()
    {
        // Add customized code here to initilize the connection with 3rd party

        ConfigReceiveSCOM config = (ConfigReceiveSCOM) configurations;
        queue = config.getQueue().toUpperCase();
        this.gatewayConfigDir = "/config/scom/";
    }

    /**
     * Add customized code here to do when stop the gateway
     */
    @Override
    public void stop()
    {
        Log.log.warn("Stopping SCOM gateway");
        // Add customized code here to stop the connection with 3rd party
        // system;
        super.stop();
    }

    @Override
    public Filter getFilter(Map<String, Object> params)
    {
        return new SCOMFilter((String) params.get(BaseFilter.ID), (String) params.get(BaseFilter.ACTIVE), (String) params.get(BaseFilter.ORDER), (String) params.get(BaseFilter.INTERVAL), (String) params.get(BaseFilter.EVENT_EVENTID), (String) params.get(BaseFilter.RUNBOOK), (String) params.get(BaseFilter.SCRIPT), (String) params.get(SCOMFilter.ALERTRESOLUTIONSTATE));
    }
}
