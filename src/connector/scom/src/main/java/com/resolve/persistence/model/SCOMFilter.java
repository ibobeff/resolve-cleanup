package com.resolve.persistence.model;

import java.util.List;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Lob;
import javax.persistence.Table;
import javax.persistence.UniqueConstraint;
import com.resolve.services.interfaces.VO;
import com.resolve.services.hibernate.vo.SCOMFilterVO;

@Entity
@Table(name = "scom_filter", uniqueConstraints = { @UniqueConstraint(columnNames = { "u_name", "u_queue" }) })
public class SCOMFilter extends GatewayFilter<SCOMFilterVO> {

    private static final long serialVersionUID = 1L;

    public SCOMFilter() {
    }

    public SCOMFilter(SCOMFilterVO vo) {
        applyVOToModel(vo);
    }

    private String UAlertResolutionState;

    @Column(name = "u_alertresolutionstate", length = 400)
    public String getUAlertResolutionState() {
        return this.UAlertResolutionState;
    }

    public void setUAlertResolutionState(String uAlertResolutionState) {
        this.UAlertResolutionState = uAlertResolutionState;
    }

    public SCOMFilterVO doGetVO() {
        SCOMFilterVO vo = new SCOMFilterVO();
        super.doGetBaseVO(vo);
        vo.setUAlertResolutionState(getUAlertResolutionState());
        return vo;
    }

    @Override
    public void applyVOToModel(SCOMFilterVO vo) {
        if (vo == null) return;
        super.applyVOToModel(vo);
        if (!VO.STRING_DEFAULT.equals(vo.getUAlertResolutionState())) this.setUAlertResolutionState(vo.getUAlertResolutionState()); else ;
    }

    @Override
    public List<String> ugetCdataProperty() {
        List<String> list = super.ugetCdataProperty();
        list.add("UAlertResolutionState");
        return list;
    }
}

