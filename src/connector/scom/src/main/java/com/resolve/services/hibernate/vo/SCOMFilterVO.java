package com.resolve.services.hibernate.vo;

import com.resolve.util.MappingAnnotation;

public class SCOMFilterVO extends GatewayFilterVO {

    private static final long serialVersionUID = 1L;

    public SCOMFilterVO() {
    }

    private String UAlertResolutionState;

    @MappingAnnotation(columnName = "ALERTRESOLUTIONSTATE")
    public String getUAlertResolutionState() {
        return this.UAlertResolutionState;
    }

    public void setUAlertResolutionState(String uAlertResolutionState) {
        this.UAlertResolutionState = uAlertResolutionState;
    }
    
    @Override
    public int hashCode()
    {
        final int prime = 31;
        int result = super.hashCode();
        result = prime * result + ((UAlertResolutionState == null) ? 0 : UAlertResolutionState.hashCode());
        return result;
    }
}

