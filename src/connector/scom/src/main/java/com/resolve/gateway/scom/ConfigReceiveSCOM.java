package com.resolve.gateway.scom;

import java.io.File;
import java.util.HashMap;
import java.util.Map;
import org.apache.commons.io.FileUtils;

import com.resolve.rsbase.MainBase;
import com.resolve.gateway.Filter;
import com.resolve.rsremote.ConfigReceiveGateway;
import com.resolve.util.Log;
import com.resolve.util.ObjectProperties;
import com.resolve.util.StringUtils;
import com.resolve.util.XDoc;

public class ConfigReceiveSCOM extends ConfigReceiveGateway {

    private static final long serialVersionUID = 1L;

    private static final String RECEIVE_SCOM_NODE = "./RECEIVE/SCOM/";

    private static final String RECEIVE_SCOM_FILTER = RECEIVE_SCOM_NODE + "FILTER";

    private String queue = "SCOM";

    private static final String RECEIVE_SCOM_ATTR_OMCFSERVICEURL = RECEIVE_SCOM_NODE + "@OMCFSERVICEURL";

    private static final String RECEIVE_SCOM_ATTR_PASSWORD = RECEIVE_SCOM_NODE + "@PASSWORD";

    private static final String RECEIVE_SCOM_ATTR_SCOMSERVERIPADDRESS = RECEIVE_SCOM_NODE + "@SCOMSERVERIPADDRESS";

    private static final String RECEIVE_SCOM_ATTR_TEMPDIRPATH = RECEIVE_SCOM_NODE + "@TEMPDIRPATH";

    private static final String RECEIVE_SCOM_ATTR_CONNECTORNAME = RECEIVE_SCOM_NODE + "@CONNECTORNAME";

    private static final String RECEIVE_SCOM_ATTR_CONNECTORDISPLAYNAME = RECEIVE_SCOM_NODE + "@CONNECTORDISPLAYNAME";

    private static final String RECEIVE_SCOM_ATTR_SCOMSERVERCOMPUTERNAME = RECEIVE_SCOM_NODE + "@SCOMSERVERCOMPUTERNAME";

    private static final String RECEIVE_SCOM_ATTR_CONNECTORID = RECEIVE_SCOM_NODE + "@CONNECTORID";

    private static final String RECEIVE_SCOM_ATTR_USERNAME = RECEIVE_SCOM_NODE + "@USERNAME";

    private static final String RECEIVE_SCOM_ATTR_CONNECTORDESCRIPTION = RECEIVE_SCOM_NODE + "@CONNECTORDESCRIPTION";

    private String omcfserviceurl = "";

    private String password = "";

    private String scomserveripaddress = "";

    private String tempdirpath = "";

    private String connectorName = "";

    private String connectorDisplayName = "";

    private String scomservercomputername = "";

    private String connectorId = "";

    private String username = "";

    private String connectorDescription = "";

    public String getQueue() {
        return queue;
    }

    public void setQueue(String queue) {
        this.queue = queue;
    }

    public String getOmcfserviceurl() {
        return this.omcfserviceurl;
    }

    public void setOmcfserviceurl(String omcfserviceurl) {
        this.omcfserviceurl = omcfserviceurl;
    }

    public String getPassword() {
        return this.password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getScomserveripaddress() {
        return this.scomserveripaddress;
    }

    public void setScomserveripaddress(String scomserveripaddress) {
        this.scomserveripaddress = scomserveripaddress;
    }

    public String getTempdirpath() {
        return this.tempdirpath;
    }

    public void setTempdirpath(String tempdirpath) {
        this.tempdirpath = tempdirpath;
    }

    public String getConnectorName() {
        return this.connectorName;
    }

    public void setConnectorName(String connectorName) {
        this.connectorName = connectorName;
    }

    public String getConnectorDisplayName() {
        return this.connectorDisplayName;
    }

    public void setConnectorDisplayName(String connectorDisplayName) {
        this.connectorDisplayName = connectorDisplayName;
    }

    public String getScomservercomputername() {
        return this.scomservercomputername;
    }

    public void setScomservercomputername(String scomservercomputername) {
        this.scomservercomputername = scomservercomputername;
    }

    public String getConnectorId() {
        return this.connectorId;
    }

    public void setConnectorId(String connectorId) {
        this.connectorId = connectorId;
    }

    public String getUsername() {
        return this.username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getConnectorDescription() {
        return this.connectorDescription;
    }

    public void setConnectorDescription(String connectorDescription) {
        this.connectorDescription = connectorDescription;
    }

    public ConfigReceiveSCOM(XDoc config) throws Exception {
        super(config);
        define("omcfserviceurl", STRING, RECEIVE_SCOM_ATTR_OMCFSERVICEURL);
        define("password", SECURE, RECEIVE_SCOM_ATTR_PASSWORD);
        define("scomserveripaddress", STRING, RECEIVE_SCOM_ATTR_SCOMSERVERIPADDRESS);
        define("tempdirpath", STRING, RECEIVE_SCOM_ATTR_TEMPDIRPATH);
        define("connectorName", STRING, RECEIVE_SCOM_ATTR_CONNECTORNAME);
        define("connectorDisplayName", STRING, RECEIVE_SCOM_ATTR_CONNECTORDISPLAYNAME);
        define("scomservercomputername", STRING, RECEIVE_SCOM_ATTR_SCOMSERVERCOMPUTERNAME);
        define("connectorId", STRING, RECEIVE_SCOM_ATTR_CONNECTORID);
        define("username", STRING, RECEIVE_SCOM_ATTR_USERNAME);
        define("connectorDescription", STRING, RECEIVE_SCOM_ATTR_CONNECTORDESCRIPTION);
    }

    @Override
    public String getRootNode() {
        return RECEIVE_SCOM_NODE;
    }

    @Override
    public void load() throws Exception {
        try {
            loadAttributes();
            if (isActive()) {
                SCOMGateway scomGateway = SCOMGateway.getInstance(this);
                filters = xdoc.getListMapValue(RECEIVE_SCOM_FILTER);
                if (filters.size() > 0) {
                    for (Map<String, Object> values : filters) {
                        String filterName = ((String) values.get(SCOMFilter.ID)).toLowerCase();
                        File scriptFile = getFile(MainBase.main.release.serviceName + "/config/scom/" + filterName + ".groovy");
                        try {
                            if (scriptFile.exists()) {
                                String groovy = FileUtils.readFileToString(scriptFile, "UTF-8");
                                if (StringUtils.isNotBlank(groovy)) {
                                    values.put(SCOMFilter.SCRIPT, groovy);
                                    Log.log.debug("Loaded groovy script: " + scriptFile);
                                }
                            }
                            scomGateway.setFilter(values);
                        } catch (Exception e) {
                            Log.log.warn(e.getMessage() + ". Skipping file: " + scriptFile.getAbsolutePath());
                        }
                    }
                }
            }
        } catch (Throwable e) {
            setActive(false);
            Log.log.warn("Couldn not load configuration for SCOM gateway, check blueprint. " + e.getMessage(), e);
        }
    }

    @Override
    public void save() throws Exception {
        File dir = getFile(MainBase.main.release.serviceName + "/config/scom");
        if (!dir.exists()) {
            FileUtils.forceMkdir(dir);
        }
        if (filters != null && isActive()) {
            filters.clear();
            for (Filter filter : SCOMGateway.getInstance().getFilters().values()) {
                SCOMFilter scomFilter = (SCOMFilter) filter;
                String groovy = scomFilter.getScript();
                String scriptFileName;
                File scriptFile = null;
                if (StringUtils.isNotBlank(groovy)) {
                    scriptFileName = scomFilter.getId().toLowerCase() + ".groovy";
                    scriptFile = getFile(MainBase.main.release.serviceName + "/config/scom/" + scriptFileName);
                }
                Map<String, Object> entry = new HashMap<String, Object>();
                putFilterDataIntoEntry(entry, scomFilter);
                filters.add(entry);
                if (scriptFile != null) {
                    try {
                        FileUtils.writeStringToFile(scriptFile, groovy, "UTF-8");
                    } catch (Exception e) {
                        Log.log.warn(e.getMessage() + ". Skipping file: " + scriptFile.getAbsolutePath());
                    }
                }
            }
            xdoc.setListMapValue(RECEIVE_SCOM_FILTER, filters);
        }
        if (nameProperties != null && isActive()) {
            nameProperties.clear();
            for (String name : SCOMGateway.getInstance().getNameProperties().keySet()) {
                Map<String, Object> entry = new HashMap<String, Object>();
                entry.put(NAME_KEY, name);
                nameProperties.add(entry);
                Map props = SCOMGateway.getInstance().getNameProperties().getNameProperties(name);
                ObjectProperties prop = new ObjectProperties(props);
                prop.save(props);
            }
        }
        saveAttributes();
    }

    private void putFilterDataIntoEntry(Map<String, Object> entry, SCOMFilter scomFilter) {
       entry.put(SCOMFilter.ID, scomFilter.getId());
        entry.put(SCOMFilter.ACTIVE, String.valueOf(scomFilter.isActive()));
        entry.put(SCOMFilter.ORDER, String.valueOf(scomFilter.getOrder()));
        entry.put(SCOMFilter.INTERVAL, String.valueOf(scomFilter.getInterval()));
        entry.put(SCOMFilter.EVENT_EVENTID, scomFilter.getEventEventId());
        entry.put(SCOMFilter.RUNBOOK, scomFilter.getRunbook());
        entry.put(SCOMFilter.SCRIPT, scomFilter.getScript());
        entry.put(SCOMFilter.ALERTRESOLUTIONSTATE, scomFilter.getAlertResolutionState());
        
    }
}

