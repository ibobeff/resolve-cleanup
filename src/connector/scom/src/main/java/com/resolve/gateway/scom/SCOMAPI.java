package com.resolve.gateway.scom;

import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.xml.bind.JAXBElement;
import javax.xml.datatype.XMLGregorianCalendar;
import javax.xml.namespace.QName;
import javax.xml.ws.BindingProvider;
import javax.xml.ws.Service;

import org.datacontract.schemas._2004._07.microsoft_enterprisemanagement.ArrayOfConnectorMonitoringAlert;
import org.datacontract.schemas._2004._07.microsoft_enterprisemanagement.ConnectorInfo;
import org.datacontract.schemas._2004._07.microsoft_enterprisemanagement.ConnectorMonitoringAlert;
import org.datacontract.schemas._2004._07.microsoft_enterprisemanagement.ConnectorState;
import org.datacontract.schemas._2004._07.microsoft_enterprisemanagement.HealthState;
import org.datacontract.schemas._2004._07.microsoft_enterprisemanagement.ManagementPackAlertSeverity;
import org.datacontract.schemas._2004._07.microsoft_enterprisemanagement.ManagementPackWorkflowPriority;
import org.datacontract.schemas._2004._07.microsoft_enterprisemanagement.MonitoringAlertConnectorStatus;
import org.datacontract.schemas._2004._07.microsoft_enterprisemanagement_common.AlertGenerated;
import org.tempuri.IConnectorFramework;
import org.tempuri.IConnectorFrameworkAcknowledgeMonitoringAlertsConnectorInvalidExceptionFaultFaultMessage;
import org.tempuri.IConnectorFrameworkAcknowledgeMonitoringAlertsInvalidDatabaseDataExceptionFaultFaultMessage;
import org.tempuri.IConnectorFrameworkAcknowledgeMonitoringAlertsServerDisconnectedExceptionFaultFaultMessage;
import org.tempuri.IConnectorFrameworkAcknowledgeMonitoringAlertsServiceNotRunningExceptionFaultFaultMessage;
import org.tempuri.IConnectorFrameworkAcknowledgeMonitoringAlertsTimeoutExceptionFaultFaultMessage;
import org.tempuri.IConnectorFrameworkAcknowledgeMonitoringAlertsUnauthorizedAccessEnterpriseManagementExceptionFaultFaultMessage;
import org.tempuri.IConnectorFrameworkAcknowledgeMonitoringAlertsUnknownAuthorizationStoreExceptionFaultFaultMessage;
import org.tempuri.IConnectorFrameworkAcknowledgeMonitoringAlertsUnknownChannelExceptionFaultFaultMessage;
import org.tempuri.IConnectorFrameworkAcknowledgeMonitoringAlertsUnknownDatabaseExceptionFaultFaultMessage;
import org.tempuri.IConnectorFrameworkAcknowledgeMonitoringAlertsUnknownServiceExceptionFaultFaultMessage;
import org.tempuri.IConnectorFrameworkCleanupConnectorInvalidExceptionFaultFaultMessage;
import org.tempuri.IConnectorFrameworkCleanupInvalidDatabaseDataExceptionFaultFaultMessage;
import org.tempuri.IConnectorFrameworkCleanupInvalidOperationExceptionFaultFaultMessage;
import org.tempuri.IConnectorFrameworkCleanupServerDisconnectedExceptionFaultFaultMessage;
import org.tempuri.IConnectorFrameworkCleanupServiceNotRunningExceptionFaultFaultMessage;
import org.tempuri.IConnectorFrameworkCleanupTimeoutExceptionFaultFaultMessage;
import org.tempuri.IConnectorFrameworkCleanupUnauthorizedAccessEnterpriseManagementExceptionFaultFaultMessage;
import org.tempuri.IConnectorFrameworkCleanupUnknownAuthorizationStoreExceptionFaultFaultMessage;
import org.tempuri.IConnectorFrameworkCleanupUnknownChannelExceptionFaultFaultMessage;
import org.tempuri.IConnectorFrameworkCleanupUnknownDatabaseExceptionFaultFaultMessage;
import org.tempuri.IConnectorFrameworkCleanupUnknownServiceExceptionFaultFaultMessage;
import org.tempuri.IConnectorFrameworkGetConnectorStateConnectorInvalidExceptionFaultFaultMessage;
import org.tempuri.IConnectorFrameworkGetConnectorStateInvalidDatabaseDataExceptionFaultFaultMessage;
import org.tempuri.IConnectorFrameworkGetConnectorStateServerDisconnectedExceptionFaultFaultMessage;
import org.tempuri.IConnectorFrameworkGetConnectorStateServiceNotRunningExceptionFaultFaultMessage;
import org.tempuri.IConnectorFrameworkGetConnectorStateTimeoutExceptionFaultFaultMessage;
import org.tempuri.IConnectorFrameworkGetConnectorStateUnauthorizedAccessEnterpriseManagementExceptionFaultFaultMessage;
import org.tempuri.IConnectorFrameworkGetConnectorStateUnknownAuthorizationStoreExceptionFaultFaultMessage;
import org.tempuri.IConnectorFrameworkGetConnectorStateUnknownChannelExceptionFaultFaultMessage;
import org.tempuri.IConnectorFrameworkGetConnectorStateUnknownDatabaseExceptionFaultFaultMessage;
import org.tempuri.IConnectorFrameworkGetConnectorStateUnknownServiceExceptionFaultFaultMessage;
import org.tempuri.IConnectorFrameworkGetMonitoringAlertsArgumentNullExceptionFaultFaultMessage;
import org.tempuri.IConnectorFrameworkGetMonitoringAlertsByIdsArgumentNullExceptionFaultFaultMessage;
import org.tempuri.IConnectorFrameworkGetMonitoringAlertsByIdsConnectorInvalidExceptionFaultFaultMessage;
import org.tempuri.IConnectorFrameworkGetMonitoringAlertsByIdsInvalidDatabaseDataExceptionFaultFaultMessage;
import org.tempuri.IConnectorFrameworkGetMonitoringAlertsByIdsInvalidOperationExceptionFaultFaultMessage;
import org.tempuri.IConnectorFrameworkGetMonitoringAlertsByIdsServerDisconnectedExceptionFaultFaultMessage;
import org.tempuri.IConnectorFrameworkGetMonitoringAlertsByIdsServiceNotRunningExceptionFaultFaultMessage;
import org.tempuri.IConnectorFrameworkGetMonitoringAlertsByIdsTimeoutExceptionFaultFaultMessage;
import org.tempuri.IConnectorFrameworkGetMonitoringAlertsByIdsUnauthorizedAccessEnterpriseManagementExceptionFaultFaultMessage;
import org.tempuri.IConnectorFrameworkGetMonitoringAlertsByIdsUnknownAuthorizationStoreExceptionFaultFaultMessage;
import org.tempuri.IConnectorFrameworkGetMonitoringAlertsByIdsUnknownChannelExceptionFaultFaultMessage;
import org.tempuri.IConnectorFrameworkGetMonitoringAlertsByIdsUnknownDatabaseExceptionFaultFaultMessage;
import org.tempuri.IConnectorFrameworkGetMonitoringAlertsByIdsUnknownServiceExceptionFaultFaultMessage;
import org.tempuri.IConnectorFrameworkGetMonitoringAlertsConnectorInvalidExceptionFaultFaultMessage;
import org.tempuri.IConnectorFrameworkGetMonitoringAlertsInvalidDatabaseDataExceptionFaultFaultMessage;
import org.tempuri.IConnectorFrameworkGetMonitoringAlertsObjectNotFoundExceptionFaultFaultMessage;
import org.tempuri.IConnectorFrameworkGetMonitoringAlertsServerDisconnectedExceptionFaultFaultMessage;
import org.tempuri.IConnectorFrameworkGetMonitoringAlertsServiceNotRunningExceptionFaultFaultMessage;
import org.tempuri.IConnectorFrameworkGetMonitoringAlertsTimeoutExceptionFaultFaultMessage;
import org.tempuri.IConnectorFrameworkGetMonitoringAlertsUnauthorizedAccessEnterpriseManagementExceptionFaultFaultMessage;
import org.tempuri.IConnectorFrameworkGetMonitoringAlertsUnknownAuthorizationStoreExceptionFaultFaultMessage;
import org.tempuri.IConnectorFrameworkGetMonitoringAlertsUnknownChannelExceptionFaultFaultMessage;
import org.tempuri.IConnectorFrameworkGetMonitoringAlertsUnknownDatabaseExceptionFaultFaultMessage;
import org.tempuri.IConnectorFrameworkGetMonitoringAlertsUnknownServiceExceptionFaultFaultMessage;
import org.tempuri.IConnectorFrameworkInitializeConnectorAlreadyInStateExceptionFaultFaultMessage;
import org.tempuri.IConnectorFrameworkInitializeConnectorInvalidExceptionFaultFaultMessage;
import org.tempuri.IConnectorFrameworkInitializeInvalidDatabaseDataExceptionFaultFaultMessage;
import org.tempuri.IConnectorFrameworkInitializeServerDisconnectedExceptionFaultFaultMessage;
import org.tempuri.IConnectorFrameworkInitializeServiceNotRunningExceptionFaultFaultMessage;
import org.tempuri.IConnectorFrameworkInitializeTimeoutExceptionFaultFaultMessage;
import org.tempuri.IConnectorFrameworkInitializeUnauthorizedAccessEnterpriseManagementExceptionFaultFaultMessage;
import org.tempuri.IConnectorFrameworkInitializeUnknownAuthorizationStoreExceptionFaultFaultMessage;
import org.tempuri.IConnectorFrameworkInitializeUnknownChannelExceptionFaultFaultMessage;
import org.tempuri.IConnectorFrameworkInitializeUnknownDatabaseExceptionFaultFaultMessage;
import org.tempuri.IConnectorFrameworkInitializeUnknownServiceExceptionFaultFaultMessage;
import org.tempuri.IConnectorFrameworkSetupWithConnectorIdArgumentNullExceptionFaultFaultMessage;
import org.tempuri.IConnectorFrameworkSetupWithConnectorIdArgumentOutOfRangeExceptionFaultFaultMessage;
import org.tempuri.IConnectorFrameworkSetupWithConnectorIdConnectorAlreadyExistsExceptionFaultFaultMessage;
import org.tempuri.IConnectorFrameworkSetupWithConnectorIdInvalidDatabaseDataExceptionFaultFaultMessage;
import org.tempuri.IConnectorFrameworkSetupWithConnectorIdServerDisconnectedExceptionFaultFaultMessage;
import org.tempuri.IConnectorFrameworkSetupWithConnectorIdServiceNotRunningExceptionFaultFaultMessage;
import org.tempuri.IConnectorFrameworkSetupWithConnectorIdTimeoutExceptionFaultFaultMessage;
import org.tempuri.IConnectorFrameworkSetupWithConnectorIdUnauthorizedAccessEnterpriseManagementExceptionFaultFaultMessage;
import org.tempuri.IConnectorFrameworkSetupWithConnectorIdUnknownAuthorizationStoreExceptionFaultFaultMessage;
import org.tempuri.IConnectorFrameworkSetupWithConnectorIdUnknownChannelExceptionFaultFaultMessage;
import org.tempuri.IConnectorFrameworkSetupWithConnectorIdUnknownDatabaseExceptionFaultFaultMessage;
import org.tempuri.IConnectorFrameworkSetupWithConnectorIdUnknownServiceExceptionFaultFaultMessage;
import org.tempuri.IConnectorFrameworkUninitializeConnectorAlreadyInStateExceptionFaultFaultMessage;
import org.tempuri.IConnectorFrameworkUninitializeConnectorInvalidExceptionFaultFaultMessage;
import org.tempuri.IConnectorFrameworkUninitializeInvalidDatabaseDataExceptionFaultFaultMessage;
import org.tempuri.IConnectorFrameworkUninitializeServerDisconnectedExceptionFaultFaultMessage;
import org.tempuri.IConnectorFrameworkUninitializeServiceNotRunningExceptionFaultFaultMessage;
import org.tempuri.IConnectorFrameworkUninitializeTimeoutExceptionFaultFaultMessage;
import org.tempuri.IConnectorFrameworkUninitializeUnauthorizedAccessEnterpriseManagementExceptionFaultFaultMessage;
import org.tempuri.IConnectorFrameworkUninitializeUnknownAuthorizationStoreExceptionFaultFaultMessage;
import org.tempuri.IConnectorFrameworkUninitializeUnknownChannelExceptionFaultFaultMessage;
import org.tempuri.IConnectorFrameworkUninitializeUnknownDatabaseExceptionFaultFaultMessage;
import org.tempuri.IConnectorFrameworkUninitializeUnknownServiceExceptionFaultFaultMessage;

import com.microsoft.schemas._2003._10.serialization.arrays.ArrayOfguid;
import com.resolve.gateway.AbstractGatewayAPI;
import com.resolve.util.Log;
import com.resolve.util.StringUtils;

public class SCOMAPI extends AbstractGatewayAPI
{

    private static final QName qname = new QName("http://tempuri.org/", "ConnectorFrameworkDataAccess");
    private final static String nameSpaceURI = "http://schemas.datacontract.org/2004/07/Microsoft.EnterpriseManagement.ConnectorFramework";
    private static SCOMGateway instance = SCOMGateway.getInstance();

     static final String CONNECTOR_STATE_INITIALIZED = "Initialized";
     static final String CONNECTOR_STATE_UNINITIALIZED = "Uninitialized";
     static final String CONNECTOR_STATE_NOTEXIST = "NotExist";

    public static final short RESOLUTION_STATE_ACKNOWLEDGED = 249;
    public static final short RESOLUTION_STATE_ASIGNED_TO_ENGINEERING = 248;
    public static final short RESOLUTION_STATE_AWAITING_EVIDENCE = 247;
    public static final short RESOLUTION_STATE_CLOSED = 255;
    public static final short RESOLUTION_STATE_NEW = 0;
    public static final short RESOLUTION_STATE_PROCESSED = 251;
    public static final short RESOLUTION_STATE_RESOLVED = 254;
    public static final short RESOLUTION_STATE_SCHEDULED = 250;
    public static final short RESOLUTION_STATE_SENDALERT = 252;

    public static final String ALERT_PARAMS = "alertParams";
    public static final String ALERT_CATEGORY = "category";
    public static final String ALERT_CONTEXT = "context";
    public static final String ALERT_CUSTOMFIELD1 = "customField1";
    public static final String ALERT_CUSTOMFIELD2 = "customField2";
    public static final String ALERT_CUSTOMFIELD3 = "customField3";
    public static final String ALERT_CUSTOMFIELD4 = "customField4";
    public static final String ALERT_CUSTOMFIELD5 = "customField5";
    public static final String ALERT_CUSTOMFIELD6 = "customField6";
    public static final String ALERT_CUSTOMFIELD7 = "customField7";
    public static final String ALERT_CUSTOMFIELD8 = "customField8";
    public static final String ALERT_CUSTOMFIELD9 = "customField9";
    public static final String ALERT_CUSTOMFIELD10 = "customField10";
    public static final String ALERT_DESCRIPTION = "description";
    public static final String ALERT_ID = "ScomAlertId";
    public static final String ALERT_LANGCODE = "langCode";
    public static final String ALERT_LASTMODIFIEDBY = "lastModifiedBy";
    public static final String ALERT_MONITORINGCLASSID = "monitoringClassId";
    public static final String ALERT_MONITORING_OBJECTDISPLAYNAME = "monitoringObjectDisplayName";
    public static final String ALERT_MONITORING_OBJECTFULLNAME = "monitoringObjectFullName";
    public static final String ALERT_MONITORING_OBJECTPATH = "monitoringObjectPath";
    public static final String ALERT_MONITORING_OBJECTNAME = "monitoringObjectName";
    public static final String ALERT_MONITORING_OBJECTID = "monitoringObjectId";
    public static final String ALERT_MONITORING_OBJECTHEALTHSTATE = "alertHelathState";
    public static final String ALERT_MONITORINGALERT_CONNECTORSTATUS = "alertConnectorStatus";
    public static final String ALERT_MONITORINGRULE_ID = "monitoringRuleID";
    public static final String ALERT_CONNECTORID = "connectorId";
    public static final String ALERT_TICKETID = "ticketId";
    public static final String ALERT_SITENAME = "sitename";
    public static final String ALERT_SEVERITY = "alertSeverity";
    public static final String ALERT_REPEATCOUNT = "repeatCount";
    public static final String ALERT_PROBLEMID = "problemId";
    public static final String ALERT_PRIORITY = "priority";
    public static final String ALERT_PRINCIPLE_NAME = "principaleName";
    public static final String ALERT_RESOLVEDBY = "resolvedBy";
    public static final String ALERT_RESOLUTIONSTATE = "resolutionState";
    public static final String ALERT_OWNER = "alertOwner";
    public static final String ALERT_NAME = "alertName";
    public static final String ALERT_NETBIOSDOMAINNAME = "netBiosDomainName";
    public static final String ALERT_NETBIOSCOMPUTERNAME = "netBiosComputerName";
    public static final String ALERT_MAINTENANCEMODE_LASTMODIFIED = "maintenanceModeLastModified";
    public static final String ALERT_TIMERESOLVED = "timeResolved";
    public static final String ALERT_TIME_RESOLUTIONSTATE_LASTMODIFIED = "timeResolutionStateLastModified";
    public static final String ALERT_TIME_RAISED = "timeRaised";
    public static final String ALERT_TIME_ADDED = "timeAdded";
    public static final String ALERT_LASTMODIFIED = "lastModified";
    public static final String ALERT_LASTMODIFIED_BYNONCONNECTOR = "lastModifiedByNonConnector";
    public static final String ALERT_STATE_LASTMODIFIED = "stateLastModified";
    public static final String SCOM_USERNAME = "scomUsername";
    public static final String SCOM_PASSWORD = "scomPassword";
    public static final String SCOM_TEMP_DIR = "tempDir";
    public static final String SCOM_IPADDR = "ipAddr";
    
    

    /*
     * * This method opens connection with the service and returns it. If there
     * is exception check system center data access service(OMCF web service) in
     * windows services and make sure it is on
     */

    private static IConnectorFramework initService() throws Exception
    {

        BindingProvider omcfBinding;
        IConnectorFramework omcfService = null;
        URL url;
        try
        {

            url = new URL(instance.getConfig().getOmcfserviceurl());

            Service service = Service.create(url, qname);
            omcfService = service.getPort(IConnectorFramework.class);
            omcfBinding = ((BindingProvider) omcfService);

            omcfBinding.getRequestContext().put(BindingProvider.ENDPOINT_ADDRESS_PROPERTY, instance.getConfig().getOmcfserviceurl());
            omcfBinding.getRequestContext().put(BindingProvider.USERNAME_PROPERTY, (instance.getConfig().getUsername()));
            omcfBinding.getRequestContext().put(BindingProvider.PASSWORD_PROPERTY, instance.getConfig().getPassword());

        }
        catch (MalformedURLException e)
        {
            Log.log.error(e.getMessage(), e);
            Log.log.error("Failed to connect to the SCOM OMCF(System Center Data Access) web service");
            throw new Exception(e);
        }
        catch (Exception e)
        {
            Log.log.error("Failed to connect to the SCOM OMCF(System Center Data Access) web service");
            Log.log.error(e.getMessage());
            throw new Exception(e);

        }
        return omcfService;

    }

    /**
     * 
     * @return
     * @throws Exception
     *             Connector can be in initialise and uninitialised state. Once
     *             connector is created it has to be initialsed before use.
     *             Before connector is removed it has to be in initialised
     *             connector state. First uninitialise and then remove
     *             connector.
     */
    static String getConnectorState() throws Exception
    {

        String connectorState = SCOMAPI.CONNECTOR_STATE_NOTEXIST;
        IConnectorFramework omcfService = SCOMAPI.initService();

        String connectorId = instance.getConfig().getConnectorId();
        ConnectorState state;

        try
        {
            state = omcfService.getConnectorState(connectorId);
            if (state.value().equals(SCOMAPI.CONNECTOR_STATE_INITIALIZED))
            {
                connectorState = SCOMAPI.CONNECTOR_STATE_INITIALIZED;
            }
            else if (state.value().equals(SCOMAPI.CONNECTOR_STATE_UNINITIALIZED))
            {
                connectorState = SCOMAPI.CONNECTOR_STATE_UNINITIALIZED;
            }
        }

        catch (IConnectorFrameworkGetConnectorStateConnectorInvalidExceptionFaultFaultMessage e)
        {
            Log.log.debug("Connector with Id: " +connectorId+"doesn't exist. New connector will be created.");
        }
        catch (IConnectorFrameworkGetConnectorStateInvalidDatabaseDataExceptionFaultFaultMessage e)
        {
            Log.log.error(e.getMessage(), e);
            throw new Exception(e.getMessage(), e);

        }
        catch (IConnectorFrameworkGetConnectorStateServerDisconnectedExceptionFaultFaultMessage e)
        {
            Log.log.error(e.getMessage(), e);
            throw new Exception(e.getMessage(), e);

        }
        catch (IConnectorFrameworkGetConnectorStateServiceNotRunningExceptionFaultFaultMessage e)
        {
            Log.log.error(e.getMessage(), e);
            throw new Exception(e.getMessage(), e);

        }
        catch (IConnectorFrameworkGetConnectorStateTimeoutExceptionFaultFaultMessage e)
        {
            Log.log.error(e.getMessage(), e);
            throw new Exception(e.getMessage(), e);

        }
        catch (IConnectorFrameworkGetConnectorStateUnauthorizedAccessEnterpriseManagementExceptionFaultFaultMessage e)
        {
            Log.log.error(e.getMessage(), e);
            throw new Exception(e.getMessage(), e);

        }
        catch (IConnectorFrameworkGetConnectorStateUnknownAuthorizationStoreExceptionFaultFaultMessage e)
        {
            Log.log.error(e.getMessage(), e);
            throw new Exception(e.getMessage(), e);

        }
        catch (IConnectorFrameworkGetConnectorStateUnknownChannelExceptionFaultFaultMessage e)
        {
            Log.log.error(e.getMessage(), e);
            throw new Exception(e.getMessage(), e);

        }
        catch (IConnectorFrameworkGetConnectorStateUnknownDatabaseExceptionFaultFaultMessage e)
        {
            Log.log.error(e.getMessage(), e);
            throw new Exception(e.getMessage(), e);

        }
        catch (IConnectorFrameworkGetConnectorStateUnknownServiceExceptionFaultFaultMessage e)
        {
            Log.log.error(e.getMessage(), e);
            throw new Exception(e.getMessage(), e);

        }
        catch (Exception e)
        {
            Log.log.error("Error retrieving the state of connector. Also please make sure connector ID is valid GUID number.");
            throw new Exception(e.getMessage(), e);
            
        }

        return connectorState;

    }

    /*
     * * ConnectorName - Name of the connector connDisplayName - Display Name
     * description - Description. If connector already exist then it will return
     * false. Check the details in log to know why it failed creating connector.
     * Create subscription for connector in SCOM before it's used.
     */

    static boolean createConnector(String ConnectorState) throws Exception
    {
        Boolean isCreated = false;
        try
        {

            // omcfService.
            // Currently connector name and display name is required fields
            // becasue after connector is created
            // because once connector is created then have to cretae
            // subscriptuon on that connector manually using SCOM application
            if (ConnectorState.equals(SCOMAPI.CONNECTOR_STATE_UNINITIALIZED))
            {
                IConnectorFramework omcfService = SCOMAPI.initService();
                String connectorId = instance.getConfig().getConnectorId();
                omcfService.initialize(connectorId);
                ConnectorState state = omcfService.getConnectorState(connectorId);
                if (state.value().equals(SCOMAPI.CONNECTOR_STATE_INITIALIZED))
                {
                    isCreated = true;
                }
            }
            if (ConnectorState.equals(SCOMAPI.CONNECTOR_STATE_NOTEXIST))
            {
                IConnectorFramework omcfService = SCOMAPI.initService();

                String connectorId = instance.getConfig().getConnectorId();
                String ConnectorName = instance.getConfig().getConnectorName();
                String connDisplayName = instance.getConfig().getConnectorDisplayName();
                String description = instance.getConfig().getConnectorDescription();
                if (!(ConnectorName == null) && !(connDisplayName == null) && StringUtils.isNotEmpty(ConnectorName) && StringUtils.isNotEmpty(connDisplayName) && description != null && StringUtils.isNotEmpty(description))
                {

                    ConnectorInfo info = new ConnectorInfo();
                    JAXBElement<String> nameValue = new JAXBElement<String>(new QName(nameSpaceURI, "Name"), String.class, ConnectorName);
                    JAXBElement<String> displyaNameValue = new JAXBElement<String>(new QName(nameSpaceURI, "DisplayName"), String.class, (connDisplayName));
                    JAXBElement<String> descriptionValue = new JAXBElement<String>(new QName(nameSpaceURI, "Description"), String.class, (description + " Id: " + instance.getConfig().getConnectorId()));
                        
                    info.setDescription(descriptionValue);
                    info.setName(nameValue);
                    info.setDisplayName(displyaNameValue);

                    omcfService.setupWithConnectorId(info, connectorId);
                    omcfService.initialize(connectorId);
                    
                    ConnectorState state = omcfService.getConnectorState(connectorId);
                    if (state.value().equals(SCOMAPI.CONNECTOR_STATE_INITIALIZED))
                    {
                        isCreated = true;
                    }
                }
                else
                {
                    Log.log.error("Please provide Connector Name,Display Name and Description to create new connector.");
                    throw new Exception("Please provide Connector Name,Display Name and Description to create new connector.");
                }
            }

        }
        catch (IConnectorFrameworkSetupWithConnectorIdArgumentNullExceptionFaultFaultMessage e)
        {
            Log.log.error(e.getMessage(), e);

        }
        catch (IConnectorFrameworkSetupWithConnectorIdArgumentOutOfRangeExceptionFaultFaultMessage e)
        {
            Log.log.error(e.getMessage(), e);

        }
        catch (IConnectorFrameworkSetupWithConnectorIdConnectorAlreadyExistsExceptionFaultFaultMessage e)
        {
            Log.log.error(e.getMessage(), e);

        }
        catch (IConnectorFrameworkSetupWithConnectorIdInvalidDatabaseDataExceptionFaultFaultMessage e)
        {
            Log.log.error(e.getMessage(), e);

        }
        catch (IConnectorFrameworkSetupWithConnectorIdServerDisconnectedExceptionFaultFaultMessage e)
        {
            Log.log.error(e.getMessage(), e);

        }
        catch (IConnectorFrameworkSetupWithConnectorIdServiceNotRunningExceptionFaultFaultMessage e)
        {
            Log.log.error(e.getMessage(), e);

        }
        catch (IConnectorFrameworkSetupWithConnectorIdTimeoutExceptionFaultFaultMessage e)
        {
            Log.log.error(e.getMessage(), e);

        }
        catch (IConnectorFrameworkSetupWithConnectorIdUnauthorizedAccessEnterpriseManagementExceptionFaultFaultMessage e)
        {
            Log.log.error(e.getMessage(), e);

        }
        catch (IConnectorFrameworkSetupWithConnectorIdUnknownAuthorizationStoreExceptionFaultFaultMessage e)
        {
            Log.log.error(e.getMessage(), e);

        }
        catch (IConnectorFrameworkSetupWithConnectorIdUnknownChannelExceptionFaultFaultMessage e)
        {
            Log.log.error(e.getMessage(), e);

        }
        catch (IConnectorFrameworkSetupWithConnectorIdUnknownDatabaseExceptionFaultFaultMessage e)
        {
            Log.log.error(e.getMessage(), e);

        }
        catch (IConnectorFrameworkSetupWithConnectorIdUnknownServiceExceptionFaultFaultMessage e)
        {
            Log.log.error(e.getMessage(), e);

        }
        catch (IConnectorFrameworkInitializeConnectorAlreadyInStateExceptionFaultFaultMessage e)
        {
            Log.log.error(e.getMessage(), e);

        }
        catch (IConnectorFrameworkInitializeConnectorInvalidExceptionFaultFaultMessage e)
        {
            Log.log.error(e.getMessage(), e);

        }
        catch (IConnectorFrameworkInitializeInvalidDatabaseDataExceptionFaultFaultMessage e)
        {
            Log.log.error(e.getMessage(), e);

        }
        catch (IConnectorFrameworkInitializeServerDisconnectedExceptionFaultFaultMessage e)
        {
            Log.log.error(e.getMessage(), e);

        }
        catch (IConnectorFrameworkInitializeServiceNotRunningExceptionFaultFaultMessage e)
        {
            Log.log.error(e.getMessage(), e);

        }
        catch (IConnectorFrameworkInitializeTimeoutExceptionFaultFaultMessage e)
        {
            Log.log.error(e.getMessage(), e);

        }
        catch (IConnectorFrameworkInitializeUnauthorizedAccessEnterpriseManagementExceptionFaultFaultMessage e)
        {
            Log.log.error(e.getMessage(), e);

        }
        catch (IConnectorFrameworkInitializeUnknownAuthorizationStoreExceptionFaultFaultMessage e)
        {
            Log.log.error(e.getMessage(), e);

        }
        catch (IConnectorFrameworkInitializeUnknownChannelExceptionFaultFaultMessage e)
        {
            Log.log.error(e.getMessage(), e);

        }
        catch (IConnectorFrameworkInitializeUnknownDatabaseExceptionFaultFaultMessage e)
        {
            Log.log.error(e.getMessage(), e);

        }
        catch (IConnectorFrameworkInitializeUnknownServiceExceptionFaultFaultMessage e)
        {
            Log.log.error(e.getMessage(), e);
        }
        catch (IConnectorFrameworkGetConnectorStateConnectorInvalidExceptionFaultFaultMessage e)
        {
            Log.log.error(e.getMessage(), e);
        }
        catch (IConnectorFrameworkGetConnectorStateInvalidDatabaseDataExceptionFaultFaultMessage e)
        {
            Log.log.error(e.getMessage(), e);
        }
        catch (IConnectorFrameworkGetConnectorStateServerDisconnectedExceptionFaultFaultMessage e)
        {
            Log.log.error(e.getMessage(), e);
        }
        catch (IConnectorFrameworkGetConnectorStateServiceNotRunningExceptionFaultFaultMessage e)
        {
            Log.log.error(e.getMessage(), e);
        }
        catch (IConnectorFrameworkGetConnectorStateTimeoutExceptionFaultFaultMessage e)
        {
            Log.log.error(e.getMessage(), e);
        }
        catch (IConnectorFrameworkGetConnectorStateUnauthorizedAccessEnterpriseManagementExceptionFaultFaultMessage e)
        {
            Log.log.error(e.getMessage(), e);
        }
        catch (IConnectorFrameworkGetConnectorStateUnknownAuthorizationStoreExceptionFaultFaultMessage e)
        {
            Log.log.error(e.getMessage(), e);
        }
        catch (IConnectorFrameworkGetConnectorStateUnknownChannelExceptionFaultFaultMessage e)
        {
            Log.log.error(e.getMessage(), e);
        }
        catch (IConnectorFrameworkGetConnectorStateUnknownDatabaseExceptionFaultFaultMessage e)
        {
            Log.log.error(e.getMessage(), e);
        }
        catch (IConnectorFrameworkGetConnectorStateUnknownServiceExceptionFaultFaultMessage e)
        {
            Log.log.error(e.getMessage(), e);
        }
        catch (Exception e)
        {
            Log.log.error(e.getMessage(), e);
        }
        return isCreated;

    }

    /**
     * Removes connector from SCOM.
     * ConnectorId should be specified in the blueprint properties file
     * @return Status of connector removal.
     */

    public static void removeConnector() throws Exception
    {

        ConnectorState state;
        
        try
        {
            IConnectorFramework omcfService = SCOMAPI.initService();
            String connectorId = instance.getConfig().getConnectorId();
            state = omcfService.getConnectorState(connectorId);
            Log.log.debug("Checking if connector is initialized. Current state: " + state.value());
            if (state.value().equals("Initialized"))
            {
                Log.log.debug("Uninitializing connector: " + connectorId);
                omcfService.uninitialize(connectorId);

                state = omcfService.getConnectorState(connectorId);
                if (state.value().equals("Uninitialized"))
                {
                    Log.log.debug("Current Connector state is: " + state.value() + ". Removing connector.");
                    omcfService.cleanup(connectorId);
                    
                }

            }
            else if (state.value().equals("Uninitialized"))
            {

                Log.log.debug("Connector is already in " + state.value() + " state.Can't remove uninitialized connector. ConnectorId: " + connectorId);
                throw new Exception("Connector is already in " + state.value() + " state.Can't remove uninitialized connector. ConnectorId: " + connectorId);
            }
        }
        catch (IConnectorFrameworkGetConnectorStateConnectorInvalidExceptionFaultFaultMessage e)
        {
            Log.log.error(e.getMessage(), e);
            throw e;
        }
        catch (IConnectorFrameworkGetConnectorStateInvalidDatabaseDataExceptionFaultFaultMessage e)
        {
            Log.log.error(e.getMessage(), e);
            throw e;
        }
        catch (IConnectorFrameworkGetConnectorStateServerDisconnectedExceptionFaultFaultMessage e)
        {
            Log.log.error(e.getMessage(), e);
            throw e;
        }
        catch (IConnectorFrameworkGetConnectorStateServiceNotRunningExceptionFaultFaultMessage e)
        {
            Log.log.error(e.getMessage(), e);
            throw e;
        }
        catch (IConnectorFrameworkGetConnectorStateTimeoutExceptionFaultFaultMessage e)
        {
            Log.log.error(e.getMessage(), e);
            throw e;
        }
        catch (IConnectorFrameworkGetConnectorStateUnauthorizedAccessEnterpriseManagementExceptionFaultFaultMessage e)
        {
            Log.log.error(e.getMessage(), e);
            throw e;
        }
        catch (IConnectorFrameworkGetConnectorStateUnknownAuthorizationStoreExceptionFaultFaultMessage e)
        {
            Log.log.error(e.getMessage(), e);
            throw e;
        }
        catch (IConnectorFrameworkGetConnectorStateUnknownChannelExceptionFaultFaultMessage e)
        {
            Log.log.error(e.getMessage(), e);
            throw e;
        }
        catch (IConnectorFrameworkGetConnectorStateUnknownDatabaseExceptionFaultFaultMessage e)
        {
            Log.log.error(e.getMessage(), e);

        }
        catch (IConnectorFrameworkGetConnectorStateUnknownServiceExceptionFaultFaultMessage e)
        {
            Log.log.error(e.getMessage(), e);
            throw e;
        }
        catch (IConnectorFrameworkUninitializeConnectorAlreadyInStateExceptionFaultFaultMessage e)
        {
            Log.log.error(e.getMessage(), e);
            throw e;
        }
        catch (IConnectorFrameworkUninitializeConnectorInvalidExceptionFaultFaultMessage e)
        {
            Log.log.error(e.getMessage(), e);
            throw e;
        }
        catch (IConnectorFrameworkUninitializeInvalidDatabaseDataExceptionFaultFaultMessage e)
        {
            Log.log.error(e.getMessage(), e);
            throw e;
        }
        catch (IConnectorFrameworkUninitializeServerDisconnectedExceptionFaultFaultMessage e)
        {
            Log.log.error(e.getMessage(), e);
            throw e;
        }
        catch (IConnectorFrameworkUninitializeServiceNotRunningExceptionFaultFaultMessage e)
        {
            Log.log.error(e.getMessage(), e);
            throw e;
        }
        catch (IConnectorFrameworkUninitializeTimeoutExceptionFaultFaultMessage e)
        {
            Log.log.error(e.getMessage(), e);
            throw e;
        }
        catch (IConnectorFrameworkUninitializeUnauthorizedAccessEnterpriseManagementExceptionFaultFaultMessage e)
        {
            Log.log.error(e.getMessage(), e);

        }
        catch (IConnectorFrameworkUninitializeUnknownAuthorizationStoreExceptionFaultFaultMessage e)
        {
            Log.log.error(e.getMessage(), e);
            throw e;
        }
        catch (IConnectorFrameworkUninitializeUnknownChannelExceptionFaultFaultMessage e)
        {
            Log.log.error(e.getMessage(), e);
            throw e;
        }
        catch (IConnectorFrameworkUninitializeUnknownDatabaseExceptionFaultFaultMessage e)
        {
            Log.log.error(e.getMessage(), e);
            throw e;
        }
        catch (IConnectorFrameworkUninitializeUnknownServiceExceptionFaultFaultMessage e)
        {
            Log.log.error(e.getMessage(), e);
            throw e;
        }
        catch (IConnectorFrameworkCleanupConnectorInvalidExceptionFaultFaultMessage e)
        {
            Log.log.error(e.getMessage(), e);
            throw e;
        }
        catch (IConnectorFrameworkCleanupInvalidDatabaseDataExceptionFaultFaultMessage e)
        {
            Log.log.error(e.getMessage(), e);
            throw e;
        }
        catch (IConnectorFrameworkCleanupInvalidOperationExceptionFaultFaultMessage e)
        {
            Log.log.error(e.getMessage(), e);
            throw e;
        }
        catch (IConnectorFrameworkCleanupServerDisconnectedExceptionFaultFaultMessage e)
        {
            Log.log.error(e.getMessage(), e);
            throw e;
        }
        catch (IConnectorFrameworkCleanupServiceNotRunningExceptionFaultFaultMessage e)
        {
            Log.log.error(e.getMessage(), e);
            throw e;
        }
        catch (IConnectorFrameworkCleanupTimeoutExceptionFaultFaultMessage e)
        {
            Log.log.error(e.getMessage(), e);
            throw e;
        }
        catch (IConnectorFrameworkCleanupUnauthorizedAccessEnterpriseManagementExceptionFaultFaultMessage e)
        {
            Log.log.error(e.getMessage(), e);
            throw e;
        }
        catch (IConnectorFrameworkCleanupUnknownAuthorizationStoreExceptionFaultFaultMessage e)
        {
            Log.log.error(e.getMessage(), e);
            throw e;
        }
        catch (IConnectorFrameworkCleanupUnknownChannelExceptionFaultFaultMessage e)
        {
            Log.log.error(e.getMessage(), e);
            throw e;
        }
        catch (IConnectorFrameworkCleanupUnknownDatabaseExceptionFaultFaultMessage e)
        {
            Log.log.error(e.getMessage(), e);
            throw e;
        }
        catch (IConnectorFrameworkCleanupUnknownServiceExceptionFaultFaultMessage e)
        {
            Log.log.error(e.getMessage(), e);
            throw e;
        }
        catch (Exception e)
        {
            Log.log.error(e.getMessage(), e);
            throw e;
        }
       

    }

    /*
     * * Create subscription for connectr before read alerts.
     * 
     */
    static List<AlertGenerated> getMonitoringAlerts() throws Exception
    {

        IConnectorFramework omcfService = SCOMAPI.initService();
        String connectorId = instance.getConfig().getConnectorId();
        List<ConnectorMonitoringAlert> alertList = new ArrayList<ConnectorMonitoringAlert>();
        List<AlertGenerated> alertGeneratedList = new ArrayList<AlertGenerated>();

        ArrayOfConnectorMonitoringAlert monitorinAlertList;
        try
        {
            // languageCodes
            // A comma-delimited list of language codes. The language code is an
            // ISO 639-2 three-letter code that corresponds
            // to the values used by the CultureInfo.ThreeLetterISOLanguageName
            // property.
            monitorinAlertList = omcfService.getMonitoringAlerts(connectorId, "eng,ang,ENU,enm");
            alertList = monitorinAlertList.getConnectorMonitoringAlert();

            Log.log.debug("Total alerts received before applying SCOM Filter-requested state criteria: " + alertList.size());

            for (int i = 0; i < alertList.size(); i++)
            {

                AlertGenerated alert = alertList.get(i).getAlertGenerated();
                alertGeneratedList.add(i, alert);

            }
        }
        catch (IConnectorFrameworkGetMonitoringAlertsArgumentNullExceptionFaultFaultMessage e)
        {
            Log.log.error(e.getMessage(), e);
            throw new Exception(e);
        }
        catch (IConnectorFrameworkGetMonitoringAlertsConnectorInvalidExceptionFaultFaultMessage e)
        {
            Log.log.error(e.getMessage(), e);
            throw new Exception(e);
        }
        catch (IConnectorFrameworkGetMonitoringAlertsInvalidDatabaseDataExceptionFaultFaultMessage e)
        {
            Log.log.error(e.getMessage(), e);
            throw new Exception(e);
        }
        catch (IConnectorFrameworkGetMonitoringAlertsObjectNotFoundExceptionFaultFaultMessage e)
        {
            Log.log.error(e.getMessage(), e);
            throw new Exception(e);
        }
        catch (IConnectorFrameworkGetMonitoringAlertsServerDisconnectedExceptionFaultFaultMessage e)
        {
            Log.log.error(e.getMessage(), e);
            throw new Exception(e);
        }
        catch (IConnectorFrameworkGetMonitoringAlertsServiceNotRunningExceptionFaultFaultMessage e)
        {
            Log.log.error(e.getMessage(), e);
            throw new Exception(e);
        }
        catch (IConnectorFrameworkGetMonitoringAlertsTimeoutExceptionFaultFaultMessage e)
        {
            Log.log.error(e.getMessage(), e);
            throw new Exception(e);
        }
        catch (IConnectorFrameworkGetMonitoringAlertsUnauthorizedAccessEnterpriseManagementExceptionFaultFaultMessage e)
        {
            Log.log.error(e.getMessage(), e);
            throw new Exception(e);
        }
        catch (IConnectorFrameworkGetMonitoringAlertsUnknownAuthorizationStoreExceptionFaultFaultMessage e)
        {
            Log.log.error(e.getMessage(), e);
            throw new Exception(e);
        }
        catch (IConnectorFrameworkGetMonitoringAlertsUnknownChannelExceptionFaultFaultMessage e)
        {
            Log.log.error(e.getMessage(), e);
            throw new Exception(e);
        }
        catch (IConnectorFrameworkGetMonitoringAlertsUnknownDatabaseExceptionFaultFaultMessage e)
        {
            Log.log.error(e.getMessage(), e);
            throw new Exception(e);
        }
        catch (IConnectorFrameworkGetMonitoringAlertsUnknownServiceExceptionFaultFaultMessage e)
        {
            Log.log.error(e.getMessage(), e);
            throw new Exception(e);
        }

        return alertGeneratedList;
    }

    /**
     * 
     * @param alertId
     *            - AlertId of the Alert object
     * @param languageCode
     *            - "eng" for english
     * @return Alert object.
     * @throws Exception
     */
    static AlertGenerated getMonitoringAlertById(String alertId, String languageCode) throws Exception
    {
        if (alertId == "" || alertId == null)
        {
            Log.log.error("Alert Id is required. Please pass valid Alert ID");
            return null;

        }
        String connectorId = instance.getConfig().getConnectorId();
        IConnectorFramework omcfService = SCOMAPI.initService();
        List<AlertGenerated> alertGeneratedList = new ArrayList<AlertGenerated>();
        ArrayOfguid arrayOfGuid = new ArrayOfguid();
        arrayOfGuid.getGuid().add(alertId);
        ArrayOfConnectorMonitoringAlert arrayCoonectorMonitoringAlert;
        try
        {
            arrayCoonectorMonitoringAlert = omcfService.getMonitoringAlertsByIds(connectorId, arrayOfGuid, languageCode);
            List<ConnectorMonitoringAlert> connectorMonitoringAlertList = arrayCoonectorMonitoringAlert.getConnectorMonitoringAlert();
            for (int ii = 0; ii < connectorMonitoringAlertList.size(); ii++)
            {
                alertGeneratedList.add(connectorMonitoringAlertList.get(ii).getAlertGenerated());
            }
        }
        catch (IConnectorFrameworkGetMonitoringAlertsByIdsArgumentNullExceptionFaultFaultMessage e)
        {
            Log.log.error(e.getMessage(), e);
            throw new Exception(e);
        }
        catch (IConnectorFrameworkGetMonitoringAlertsByIdsConnectorInvalidExceptionFaultFaultMessage e)
        {
            Log.log.error(e.getMessage(), e);
            throw new Exception(e);
        }
        catch (IConnectorFrameworkGetMonitoringAlertsByIdsInvalidDatabaseDataExceptionFaultFaultMessage e)
        {
            Log.log.error(e.getMessage(), e);
            throw new Exception(e);
        }
        catch (IConnectorFrameworkGetMonitoringAlertsByIdsInvalidOperationExceptionFaultFaultMessage e)
        {
            Log.log.error(e.getMessage(), e);
            throw new Exception(e);
        }
        catch (IConnectorFrameworkGetMonitoringAlertsByIdsServerDisconnectedExceptionFaultFaultMessage e)
        {
            Log.log.error(e.getMessage(), e);
            throw new Exception(e);
        }
        catch (IConnectorFrameworkGetMonitoringAlertsByIdsServiceNotRunningExceptionFaultFaultMessage e)
        {
            Log.log.error(e.getMessage(), e);
            throw new Exception(e);
        }
        catch (IConnectorFrameworkGetMonitoringAlertsByIdsTimeoutExceptionFaultFaultMessage e)
        {
            Log.log.error(e.getMessage(), e);
            throw new Exception(e);
        }
        catch (IConnectorFrameworkGetMonitoringAlertsByIdsUnauthorizedAccessEnterpriseManagementExceptionFaultFaultMessage e)
        {
            Log.log.error(e.getMessage(), e);
            throw new Exception(e);
        }
        catch (IConnectorFrameworkGetMonitoringAlertsByIdsUnknownAuthorizationStoreExceptionFaultFaultMessage e)
        {
            Log.log.error(e.getMessage(), e);
            throw new Exception(e);
        }
        catch (IConnectorFrameworkGetMonitoringAlertsByIdsUnknownChannelExceptionFaultFaultMessage e)
        {
            Log.log.error(e.getMessage(), e);
            throw new Exception(e);
        }
        catch (IConnectorFrameworkGetMonitoringAlertsByIdsUnknownDatabaseExceptionFaultFaultMessage e)
        {
            Log.log.error(e.getMessage(), e);
        }
        catch (IConnectorFrameworkGetMonitoringAlertsByIdsUnknownServiceExceptionFaultFaultMessage e)
        {
            Log.log.error(e.getMessage(), e);
            throw new Exception(e);
        }
        if (alertGeneratedList.size() == 1)
        {
            return alertGeneratedList.get(0);
        }
        else
        {

            Log.log.error("Alert not found with Id: " + alertId);

        }
        return null;

    }

    /**
     * Could be future enhacement. If want to read alert depending on the
     * timestamp. All alert after specified time are returned
     * 
     * @param year
     * @param month
     * @param date
     * @param hourOfDay
     * @param minute
     * @param second
     * @throws Exception
     */

    /*
     * public static List<AlertGenerated> getMonitoringAlertsByBookMark(int
     * year, int month, int date, int hourOfDay, int minute, int second) throws
     * Exception { IConnectorFramework omcfService = SCOMAPI.initService();
     * 
     * List<ConnectorMonitoringAlert> alertList = new
     * ArrayList<ConnectorMonitoringAlert>(); List<AlertGenerated>
     * alertGeneratedList = new ArrayList<AlertGenerated>(); try {
     * 
     * TimeZone utc = TimeZone.getTimeZone("UTC"); GregorianCalendar gc = new
     * GregorianCalendar(); gc.set(year, month , date); gc.set(year, month,
     * date, hourOfDay, minute, second); DateFormat df = new SimpleDateFormat(
     * "yyyy-MM-dd HH:mm:ssZ"); df.setTimeZone(utc); System.out.println(
     * " - Gregorian UTC [" + df.format(gc.getTime()) + "]");
     * 
     * XMLGregorianCalendar currServTime =
     * DatatypeFactory.newInstance().newXMLGregorianCalendar(gc);
     * 
     * Log.log.debug("Getting alerts generated after time:  " + currServTime);
     * 
     * String connectorId = instance.getConfig().getConnectorId();
     * Log.log.debug("Conn Id: " + connectorId); ArrayOfConnectorMonitoringAlert
     * monitorinAlertList =
     * omcfService.getMonitoringAlertsWithBookmark(connectorId, "eng",
     * currServTime);
     * 
     * Log.log.debug("Total alerts received: " + alertList.size());
     * 
     * for (int i = 0; i < alertList.size(); i++) { System.out.println("Alert "
     * + i + ": " + alertList.get(i).getDisplayString()); AlertGenerated alert =
     * alertList.get(i).getAlertGenerated(); alertGeneratedList.add(alert); //
     * 
     * 
     * }
     * 
     * } catch (DatatypeConfigurationException e) {
     * Log.log.error(e.getMessage(), e); throw new Exception(e); }
     * 
     * catch
     * (IConnectorFrameworkGetMonitoringAlertsWithBookmarkArgumentNullExceptionFaultFaultMessage
     * e) { Log.log.error(e.getMessage(), e); throw new Exception(e); } catch
     * (IConnectorFrameworkGetMonitoringAlertsWithBookmarkConnectorInvalidExceptionFaultFaultMessage
     * e) { Log.log.error(e.getMessage(), e); throw new Exception(e); } catch
     * (IConnectorFrameworkGetMonitoringAlertsWithBookmarkInvalidDatabaseDataExceptionFaultFaultMessage
     * e) { Log.log.error(e.getMessage(), e); throw new Exception(e); } catch
     * (IConnectorFrameworkGetMonitoringAlertsWithBookmarkObjectNotFoundExceptionFaultFaultMessage
     * e) { Log.log.error(e.getMessage(), e); throw new Exception(e); } catch
     * (IConnectorFrameworkGetMonitoringAlertsWithBookmarkServerDisconnectedExceptionFaultFaultMessage
     * e) { Log.log.error(e.getMessage(), e); throw new Exception(e); } catch
     * (IConnectorFrameworkGetMonitoringAlertsWithBookmarkServiceNotRunningExceptionFaultFaultMessage
     * e) { Log.log.error(e.getMessage(), e); throw new Exception(e); } catch
     * (IConnectorFrameworkGetMonitoringAlertsWithBookmarkTimeoutExceptionFaultFaultMessage
     * e) { Log.log.error(e.getMessage(), e); throw new Exception(e); } catch
     * (IConnectorFrameworkGetMonitoringAlertsWithBookmarkUnauthorizedAccessEnterpriseManagementExceptionFaultFaultMessage
     * e) { Log.log.error(e.getMessage(), e); throw new Exception(e); } catch
     * (IConnectorFrameworkGetMonitoringAlertsWithBookmarkUnknownAuthorizationStoreExceptionFaultFaultMessage
     * e) { Log.log.error(e.getMessage(), e); throw new Exception(e); } catch
     * (IConnectorFrameworkGetMonitoringAlertsWithBookmarkUnknownChannelExceptionFaultFaultMessage
     * e) { Log.log.error(e.getMessage(), e); throw new Exception(e); } catch
     * (IConnectorFrameworkGetMonitoringAlertsWithBookmarkUnknownDatabaseExceptionFaultFaultMessage
     * e) { Log.log.error(e.getMessage(), e); throw new Exception(e); } catch
     * (IConnectorFrameworkGetMonitoringAlertsWithBookmarkUnknownServiceExceptionFaultFaultMessage
     * e) { Log.log.error(e.getMessage(), e); throw new Exception(e); } return
     * alertGeneratedList; }
     */

    /**
     * All alerts with a timestamp prior to this will be acknowledged.
     * 
     * @param year
     * @param month
     * @param date
     * @param hourOfDay
     * @param minute
     * @param second
     * @throws Exception
     */

    static void acknowledgeAlerts(XMLGregorianCalendar xmlGcTime) throws Exception
    {

        /*
         * The monitoring connector acknowledges that it has successfully
         * received monitoring alerts from the
         * Microsoft.EnterpriseManagement.ConnectorFramework. The monitoring
         * connector must acknowledge that alerts have been received. Failure to
         * acknowledge an alert will result in subsequent calls to
         * MonitoringConnector returning previously returned alerts. After
         * acknowledging alerts, you get alerts that are updated after the
         * bookmark time that you acknowledged with.
         */

        IConnectorFramework omcfService = SCOMAPI.initService();

        try
        {
            Log.log.debug("Acknowledging alerts up until the given bookmark.(UTC):  " + xmlGcTime + " Connector Name: " + instance.getConfig().getConnectorName() + " Connector Id: " + instance.getConfig().getConnectorId());
            
          //  xmlGcTime.setMinute(xmlGcTime.getMinute()+3);
           // System.out.println("Acknowledging alerts up until the given bookmark.(UTC): " + xmlGcTime);
            omcfService.acknowledgeMonitoringAlerts(instance.getConfig().getConnectorId(), xmlGcTime);

        }
        catch (IConnectorFrameworkAcknowledgeMonitoringAlertsConnectorInvalidExceptionFaultFaultMessage e)
        {
            Log.log.error(e.getMessage(), e);
            throw new Exception(e);
        }
        catch (IConnectorFrameworkAcknowledgeMonitoringAlertsInvalidDatabaseDataExceptionFaultFaultMessage e)
        {
            Log.log.error(e.getMessage(), e);
            throw new Exception(e);
        }
        catch (IConnectorFrameworkAcknowledgeMonitoringAlertsServerDisconnectedExceptionFaultFaultMessage e)
        {
            Log.log.error(e.getMessage(), e);
            throw new Exception(e);
        }
        catch (IConnectorFrameworkAcknowledgeMonitoringAlertsServiceNotRunningExceptionFaultFaultMessage e)
        {
            Log.log.error(e.getMessage(), e);
            throw new Exception(e);
        }
        catch (IConnectorFrameworkAcknowledgeMonitoringAlertsTimeoutExceptionFaultFaultMessage e)
        {
            Log.log.error(e.getMessage(), e);
            throw new Exception(e);
        }
        catch (IConnectorFrameworkAcknowledgeMonitoringAlertsUnauthorizedAccessEnterpriseManagementExceptionFaultFaultMessage e)
        {
            Log.log.error(e.getMessage(), e);
            throw new Exception(e);
        }
        catch (IConnectorFrameworkAcknowledgeMonitoringAlertsUnknownAuthorizationStoreExceptionFaultFaultMessage e)
        {
            Log.log.error(e.getMessage(), e);
            throw new Exception(e);
        }
        catch (IConnectorFrameworkAcknowledgeMonitoringAlertsUnknownChannelExceptionFaultFaultMessage e)
        {
            Log.log.error(e.getMessage(), e);
            throw new Exception(e);
        }
        catch (IConnectorFrameworkAcknowledgeMonitoringAlertsUnknownDatabaseExceptionFaultFaultMessage e)
        {
            Log.log.error(e.getMessage(), e);
            throw new Exception(e);
        }
        catch (IConnectorFrameworkAcknowledgeMonitoringAlertsUnknownServiceExceptionFaultFaultMessage e)
        {
            Log.log.error(e.getMessage(), e);
            throw new Exception(e);
        }

    }

  

    /**
     * This method adds comment to the history of alert.
     * 
     * @param alertId : Id of the alert
     * @param comment : Comment to be added to the alert history
     * @throws Exception
     */
    public static void addHistoryComment(String alertId, String comment) throws Exception
    {
     
        try
        {

            AlertGenerated alert = getMonitoringAlertById(alertId, "eng");
            if (alert == null)
            {
                Log.log.error("Could not find alert object to update");
                throw new Exception("Could not find alert object to update");
                //return isUpdated;
            }
            IConnectorFramework omcfService = SCOMAPI.initService();
            ArrayOfConnectorMonitoringAlert alertsToUpdate;
            ArrayOfguid arrayOfGuid = new ArrayOfguid();
            arrayOfGuid.getGuid().add(alert.getId());
            String connectorId = instance.getConfig().getConnectorId();
            alertsToUpdate = omcfService.getMonitoringAlertsByIds(connectorId, arrayOfGuid, "eng");
            omcfService.updateMonitoringAlerts(connectorId, comment, alertsToUpdate);
          

        }
        catch (Exception e)
        {
            Log.log.error("Failed to update alert. ");
            Log.log.error(e.getMessage(), e);
            throw e;
        }

       
    }

    private static String timeToString(XMLGregorianCalendar cal)
    {
        if (cal != null)
        {
            GregorianCalendar gcal = cal.toGregorianCalendar();
            Date dt = gcal.getTime();
            if (dt != null)
            {
                return dt.toString();
            }
        }
        return "";
    }

    /**
     * Reads Alert attributes using getters of ALert object and build a Map of
     * alert attributes and its values.
     * 
     * @param alert:  Alert Object that is recieved from the SCOM API.
     * @return Map that has alert attributes as keys and its value
     */

	static Map<String, String> getAlertAttributesMap(AlertGenerated alert) {

		Map<String, String> alertAttributeMap = new HashMap<>();

		String alertParams = alert.getAlertParams();
		alertAttributeMap.put(ALERT_PARAMS, alertParams);

		String category = alert.getCategory();
		alertAttributeMap.put(ALERT_CATEGORY, category);

		String connectorId = alert.getConnectorId();
		alertAttributeMap.put(ALERT_CONNECTORID, connectorId);

		MonitoringAlertConnectorStatus status = alert.getConnectorStatus();
		if (null != status) {
			String alertConnectorStatus = status.value();
			alertAttributeMap.put(ALERT_MONITORINGALERT_CONNECTORSTATUS, alertConnectorStatus);
		}
		else {
			alertAttributeMap.put(ALERT_MONITORINGALERT_CONNECTORSTATUS, "");
		}
		String context = alert.getContext();
		alertAttributeMap.put(ALERT_CONTEXT, context);

		String customField1 = alert.getCustomField1();
		alertAttributeMap.put(ALERT_CUSTOMFIELD1, customField1);

		String customField2 = alert.getCustomField2();
		alertAttributeMap.put(ALERT_CUSTOMFIELD2, customField2);

		String customField3 = alert.getCustomField3();
		alertAttributeMap.put(ALERT_CUSTOMFIELD3, customField3);

		String customField4 = alert.getCustomField4();
		alertAttributeMap.put(ALERT_CUSTOMFIELD4, customField4);

		String customField5 = alert.getCustomField5();
		alertAttributeMap.put(ALERT_CUSTOMFIELD5, customField5);

		String customField6 = alert.getCustomField6();
		alertAttributeMap.put(ALERT_CUSTOMFIELD6, customField6);

		String customField7 = alert.getCustomField7();
		alertAttributeMap.put(ALERT_CUSTOMFIELD7, customField7);

		String customField8 = alert.getCustomField8();
		alertAttributeMap.put(ALERT_CUSTOMFIELD8, customField8);

		String customField9 = alert.getCustomField9();
		alertAttributeMap.put(ALERT_CUSTOMFIELD9, customField9);

		String customField10 = alert.getCustomField10();
		alertAttributeMap.put(ALERT_CUSTOMFIELD10, customField10);

		String description = alert.getDescription();
		alertAttributeMap.put(ALERT_DESCRIPTION, description);

		String id = alert.getId();
		alertAttributeMap.put(ALERT_ID, id);
		// System.out.println("Alert Id: "+id);
		Log.log.debug("Alert Id: " + id);

		String langCode = alert.getLanguageCode();
		alertAttributeMap.put(ALERT_LANGCODE, langCode);

		String lastModifiedBy = alert.getLastModifiedBy();
		alertAttributeMap.put(ALERT_LASTMODIFIEDBY, lastModifiedBy);

		String monitoringClassId = alert.getMonitoringClassId();
		alertAttributeMap.put(ALERT_MONITORINGCLASSID, monitoringClassId);

		String monitoringObjectDisplayName = alert.getMonitoringObjectDisplayName();
		alertAttributeMap.put(ALERT_MONITORING_OBJECTDISPLAYNAME, monitoringObjectDisplayName);

		String monitoringObjectFullName = alert.getMonitoringObjectFullName();
		alertAttributeMap.put(ALERT_MONITORING_OBJECTFULLNAME, monitoringObjectFullName);

		String ticketId = alert.getTicketId();
		alertAttributeMap.put(ALERT_TICKETID, ticketId);

		String siteName = alert.getSiteName();
		alertAttributeMap.put(ALERT_SITENAME, siteName);

		ManagementPackAlertSeverity severity = alert.getSeverity();
		String alertSeverity = severity.value();
		alertAttributeMap.put(ALERT_SEVERITY, alertSeverity);

		String resolvedBy = alert.getResolvedBy();
		alertAttributeMap.put(ALERT_RESOLVEDBY, resolvedBy);

		Short resolutionState = alert.getResolutionState();
		alertAttributeMap.put(ALERT_RESOLUTIONSTATE, resolutionState.toString());
		// System.out.println("resolutionState: " + resolutionState);

		int repeatCount = alert.getRepeatCount();
		alertAttributeMap.put(ALERT_REPEATCOUNT, Integer.toString(repeatCount));

		String problemId = alert.getProblemId();
		alertAttributeMap.put(ALERT_PROBLEMID, problemId);

		ManagementPackWorkflowPriority priority = alert.getPriority();
		if (null != priority) {
			String alertPriority = priority.value();
			alertAttributeMap.put(ALERT_PRIORITY, alertPriority);
		}
		else {
			alertAttributeMap.put(ALERT_PRIORITY, "");
		}
		String principaleName = alert.getPrincipalName();
		alertAttributeMap.put(ALERT_PRINCIPLE_NAME, principaleName);

		String owner = alert.getOwner();
		alertAttributeMap.put(ALERT_OWNER, owner);

		String netBiosDomainName = alert.getNetbiosDomainName();
		alertAttributeMap.put(ALERT_NETBIOSDOMAINNAME, netBiosDomainName);

		String netBiosComputerName = alert.getNetbiosComputerName();
		alertAttributeMap.put(ALERT_NETBIOSCOMPUTERNAME, netBiosComputerName);

		String name = alert.getName();
		alertAttributeMap.put(ALERT_NAME, name);

		String monitoringRuleID = alert.getMonitoringRuleId();
		alertAttributeMap.put(ALERT_MONITORINGRULE_ID, monitoringRuleID);

		String monitoringObjectPath = alert.getMonitoringObjectPath();
		alertAttributeMap.put(ALERT_MONITORING_OBJECTPATH, monitoringObjectPath);

		String monitoringObjectName = alert.getMonitoringObjectName();
		alertAttributeMap.put(ALERT_MONITORING_OBJECTNAME, monitoringObjectName);

		String monitoringObjectId = alert.getMonitoringObjectId();
		alertAttributeMap.put(ALERT_MONITORING_OBJECTID, monitoringObjectId);

		HealthState healthState = alert.getMonitoringObjectHealthState();
		String alertHelathState = healthState.value();
		alertAttributeMap.put(ALERT_MONITORING_OBJECTHEALTHSTATE, alertHelathState);

		// alert.getClass();
		String maintenanceModeLastModified = SCOMAPI.timeToString(alert.getMaintenanceModeLastModified());
		alertAttributeMap.put(ALERT_MAINTENANCEMODE_LASTMODIFIED, maintenanceModeLastModified);

		String timeResolve = SCOMAPI.timeToString(alert.getTimeResolved());
		alertAttributeMap.put(ALERT_TIMERESOLVED, timeResolve);

		String timeResolutionStatelastModified = SCOMAPI.timeToString(alert.getTimeResolutionStateLastModified());
		alertAttributeMap.put(ALERT_TIME_RESOLUTIONSTATE_LASTMODIFIED, timeResolutionStatelastModified);

		String timeRaised = SCOMAPI.timeToString(alert.getTimeRaised());
		alertAttributeMap.put(ALERT_TIME_RAISED, timeRaised);
		// System.out.println("timeRaised: "+alert.getTimeRaised());
		Log.log.debug("Alert TimeRaised: " + alert.getTimeRaised());

		String timeAdded = SCOMAPI.timeToString(alert.getTimeAdded());
		alertAttributeMap.put(ALERT_TIME_ADDED, timeAdded);
		// System.out.println("timeAdded: "+alert.getTimeAdded());
		Log.log.debug("Alert TimeAdded: " + alert.getTimeAdded());

		String lastModified = SCOMAPI.timeToString(alert.getLastModified());
		alertAttributeMap.put(ALERT_LASTMODIFIED, lastModified);
		// System.out.println("lastModified: "+alert.getLastModified());
		Log.log.debug("Alert LastModified Time: " + alert.getLastModified());

		String lastmodifiedbynonconn = SCOMAPI.timeToString(alert.getLastModifiedByNonConnector());
		alertAttributeMap.put(ALERT_LASTMODIFIED_BYNONCONNECTOR, lastmodifiedbynonconn);

		String stateLastModified = SCOMAPI.timeToString(alert.getStateLastModified());
		alertAttributeMap.put(ALERT_STATE_LASTMODIFIED, stateLastModified);

		alertAttributeMap.put(SCOM_IPADDR, instance.getConfig().getScomserveripaddress());

		return alertAttributeMap;

	}

    /*    *//**
             * Returns {@link Set} of supported object types.
             * <p>
             * Default no object types are supported returning
             * {@link Collections.EMPTY_SET}. Derived external system specific
             * gateway API must override this method to return {@link Set
             * <String>} of supported object types.
             * 
             * @return {@link Set} of supported object types
             */
    /*
     * public static Set<String> getObjectTypes() { return
     * Collections.emptySet(); }
     * 
     * 
     *//**
       * Creates instance of specified object type in external system.
       * <p>
       * Default attribute name-value pair returned is
       * {@value com.resolve.gateway.GatewayConstants#METHOD_NOT_SUPPORTED_KEY}
       * -true indicating external system specific gateway does not support
       * creating instances of specified object type.
       * <p>
       * If external system specific gateway does support creation of object
       * then it must return {@link Map} of attribute name-value pairs.
       * 
       * @param objType
       *            Type of object to create in external system
       * @param objParams
       *            {@link Map} of object parameters
       * @param userName
       *            User name
       * @param password
       *            Password
       * @return Attribute id-value {@link Map} of created object in external
       *         system
       * @throws Exception
       *             If any error occurs in creating object of specified type in
       *             external system
       */
    /*
     * public static Map<String, String> createObject(String objType,
     * Map<String, String> objParams, String userName, String password) throws
     * Exception { return null; }
     * 
     *//**
       * Reads attributes of specified object in external system.
       * <p>
       * Default attribute id-value pair returned is
       * {@value com.resolve.gateway.GatewayConstants#METHOD_NOT_SUPPORTED_KEY}
       * -true indicating external system specific gateway does not support
       * reading atributes of object.
       * <p>
       * If external system specific gateway does support reading attributes of
       * object then it must return {@link Map} of attribute name-value pairs.
       *
       * @param objType
       *            Object type
       * @param objId
       *            Id of the object to read attributes of from external system
       * @param attribs
       *            {@link List} of attributes of object to read
       * @param userName
       *            User name
       * @param password
       *            Password
       * @return {@link Map} of object attribute id-value pairs
       * @throws Exception
       *             If any error occurs in reading attributes of the object in
       *             external system
       */
    /*
     * public static Map<String, String> readObject(String objType, String
     * objId, List<String> attribs, String userName, String password) throws
     * Exception { return null; }
     * 
     *//**
       * Updates attributes of specified object in external system.
       * <p>
       * Default updated attribute name-value pair returned is
       * {@value com.resolve.gateway.GatewayConstants#METHOD_NOT_SUPPORTED_KEY}
       * -true indicating external system specific gateway does not support
       * updating attributes of object.
       * <p>
       * If external system specific gateway does support updating attributes of
       * object then it must return updated {@link Map} of attribute name-value
       * pairs.
       * 
       * @param objType
       *            Object type
       * @param objId
       *            Id of the object to update attributes of in external system
       * @param updParams
       *            Key-value {@link Map} of object attributes to update
       * @param userName
       *            User name
       * @param password
       *            Password
       * @return Key-value {@link Map} of the updated object attributes
       * @throws Exception
       *             If any error occurs in updating attributes of the object in
       *             external system
       */
    /*
     * public static Map<String, String> updateObject(String objType, String
     * objId, Map<String, String> updParams, String userName, String password)
     * throws Exception { return null; }
     * 
     *//**
       * Deletes specified object from external system.
       * <p>
       * Default deleted attribute name-value pair returned is
       * {@value com.resolve.gateway.GatewayConstants#METHOD_NOT_SUPPORTED_KEY}
       * -true indicating external system specific gateway does not support
       * object deletion.
       * <p>
       * If external system specific gateway does support object deletion then
       * it must return {@link Map} of object deletion operation reult.
       * 
       * @param objType
       *            Object type
       * @param objId
       *            Id of the object to delete from external system
       * @param userName
       *            User name
       * @param password
       *            Password
       * @return Key-value {@link Map} result of object delete operation
       * @throws Exception
       *             If any error occurs in deleting object from external system
       */
    /*
     * public static Map<String, String> deleteObject(String objType, String
     * objId, String userName, String password) throws Exception { return null;
     * }
     * 
     *//**
       * Get list of objects from external system matching specified filter
       * condition.
       * <p>
       * Default implementation returns {@link List} containing {@link Map} with
       * {@value com.resolve.gateway.GatewayConstants#METHOD_NOT_SUPPORTED_KEY}
       * -true indicating external system specific gateway does not support
       * getting objects matching filter condition.
       * <p>
       * If external system specific gateway does support getting objects
       * matching filter condition then it must return {@link List} of
       * {@link Map} of matching objects attribute id-value pairs.
       * 
       * @param objType
       *            Object type
       * @param filter
       *            External system gateway specific filer conditions
       * @param userName
       *            User name
       * @param password
       *            Password
       * @return {@link List} of {@link Map}s of object attribute id-value pairs
       *         matching filter condition
       * @throws Exception
       *             If any error occurs in getting object from external system
       *//*
         * 
         * public static List<Map<String, String>> getObjects(String objType,
         * BaseFilter filter, String userName, String password) throws Exception
         * { List<Map<String, String>> objs = new ArrayList<Map<String,
         * String>>();
         * 
         * objs.add(null);
         * 
         * return objs; }
         */

}
