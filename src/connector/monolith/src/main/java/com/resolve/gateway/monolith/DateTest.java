package com.resolve.gateway.monolith;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.TimeZone;

import org.joda.time.DateTimeZone;
import org.joda.time.LocalDateTime;

public class DateTest
{

    public static void main(String[] args)
    {
       
       /* LocalDateTime localDateTime = new LocalDateTime();
        System.out.println("Users localDateTime : "
                + localDateTime.toDateTime(DateTimeZone.getDefault()));
        System.out.println("Server's localDateTime : "
                + localDateTime.toDateTime(DateTimeZone.forID("CST6CDT"))); */ 
        
        
        Date date = new Date();
        DateFormat df = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");

        // Use Madrid's time zone to format the date in
        df.setTimeZone(TimeZone.getTimeZone("US/Michigan"));

        System.out.println("Date and time in CST6CDT: " + df.format(date));
        df.setTimeZone(TimeZone.getDefault());
        
        System.out.println("Date and time in local: " + df.format(date));
    }

}
