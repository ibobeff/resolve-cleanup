package com.resolve.gateway.monolith;
import java.io.UnsupportedEncodingException;
import java.net.URL;
import java.net.URLDecoder;
import java.net.URLEncoder;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;
import java.util.StringTokenizer;

import org.apache.commons.httpclient.HttpStatus;
import org.apache.http.HttpHeaders;
import org.apache.http.NameValuePair;
import org.apache.http.message.BasicNameValuePair;
import org.eclipse.jetty.util.StringUtil;

import com.resolve.gateway.DatabaseAPI;
import com.resolve.rsremote.ConfigReceiveGateway;
import com.resolve.util.Log;
import com.resolve.util.StringUtils;
import com.resolve.util.restclient.RestCallException;
import com.resolve.util.restclient.RestCaller;

import net.sf.json.JSON;
import net.sf.json.JSONArray;
import net.sf.json.JSONObject;
import net.sf.json.JSONSerializer;

/**
 * @author swati.kadam
 *
 */
public class GenericObjectService implements ObjectService
{
    protected final ConfigReceiveMonolith configurations;
    protected final RestCaller restCaller;
    private static final int FETCH_LIMIT = ConfigReceiveGateway.MIN_PRIMARY_DATA_QUEUE_EXECUTOR_QUEUE_SIZE;
    
   
    
    public GenericObjectService(ConfigReceiveMonolith configurations)
    {
       
        String baseUrl = configurations.getWebfqdn();
        if(!baseUrl.endsWith("/")) {
            baseUrl=baseUrl+"/";
            
        }
        restCaller = new RestCaller(baseUrl, null, null);
        restCaller.setHttpbasicauthusername(configurations.getUsername());
        restCaller.setHttpbasicauthpassword(configurations.getPassword());
        this.configurations = configurations;
        
/*        restCaller = new RestCaller(configurations.getWebfqdn(), 
                                    configurations.getUsername(), 
                                    configurations.getPassword()
                                   );
        restCaller.setUrlSuffix(configurations.getWebfqdn());
   // restCaller.setUrlSuffix("");
        this.configurations = configurations;*/
    }
    
    private void setupRestCaller(String objectType, String username, String p_assword)
    {
        if (StringUtils.isEmpty(username) || StringUtils.isEmpty(p_assword))
        {
            username = configurations.getUsername();
            p_assword = configurations.getPassword();
        }

        restCaller.setHttpbasicauthusername(username);
        restCaller.setHttpbasicauthpassword(p_assword);

        restCaller.setUrlSuffix(objectType);
        
    }
    
   
    @Override
    public  void ackEvent(String Events,String Shards) throws Exception
    {
       
        String queryAckEvents ="api/eventBase/tools/executeSQLTool/"+MonolithGateway.ackToolId+"?Events="+URLEncoder.encode(Events,"UTF-8")+"&Shards="+ URLEncoder.encode(Shards,"UTF-8");
        
       // queryGetToolName = queryGetToolName+"?filter="+URLEncoder.encode("[{\"property\": \"ToolName\", \"value\":\""+toolName+"\", \"type\": \"string\",\"operator\": \"eq\"}]","UTF-8");
        getObjects(queryAckEvents,null);
        
        
       // String query = 
        // TODO Auto-generated method stub
        
    }

    @Override
    public void unackEvent(String Events,String Shards) throws Exception
    {
        // TODO Auto-generated method stub
        String queryUnAckEvents ="api/eventBase/tools/executeSQLTool/"+MonolithGateway.unackToolId+"?Events="+URLEncoder.encode(Events,"UTF-8")+"&Shards="+ URLEncoder.encode(Shards,"UTF-8");
        
        // queryGetToolName = queryGetToolName+"?filter="+URLEncoder.encode("[{\"property\": \"ToolName\", \"value\":\""+toolName+"\", \"type\": \"string\",\"operator\": \"eq\"}]","UTF-8");
         getObjects(queryUnAckEvents,null);
    }

    @Override
    public void addJournalEntryUsingDBGateway(String poolName,String sql) throws Exception
    {
        // TODO Auto-generated method stub
        
        DatabaseAPI.update(poolName, sql);
        
    }

    @Override
    public List<Map<String, String>> getObjects(String query,Map<String, String> reqHeader) throws Exception
    {
        // TODO Auto-generated method stub
     //   String query = "https://assurenow.resolvesys.com/api/eventBase/events?FilterType=FilterID&FilterValue=1&DisplayID=3&DataType=&DateFrom=&DateTo=&FilterGroupID=1&page=1";
        
        try
        {
            query = restCaller.getBaseUrl()+query;
            String response = restCaller.getMethod(query, reqHeader);
            //--------------

            
            List<Map<String, String>> result = new ArrayList<>();
                 
            
            
            if(StringUtils.isEmpty(response)) {
                Log.log.error("No response from REST Get request is received");
                return result;
                
            }
            
            JSONObject input = JSONObject.fromObject(response);
            if ( (input.containsKey("success")&& input.getString("success").equalsIgnoreCase("true")) )//input.containsKey("data") &&
            {
                                    
                if(input.containsKey("total")) {
                String toatlEvents = input.getString("total");
                Log.log.debug("Total events in resultset are: " + toatlEvents);
                }
                
                if (input.containsKey("message"))
                {
                    
                    String loadedEntries = input.getString("message");
                    if(loadedEntries.contains("Loaded")) 
                    {
                        String loadedEntriesCount = "";
                        StringTokenizer token = new StringTokenizer(loadedEntries, " ");
                        int i = 0;
                        while (token.hasMoreTokens())
                        {
                            if (i == 1)
                            {
                                loadedEntriesCount = token.nextToken();
                                break;
                            }
                            token.nextToken();
                            i++;
                        }
                        Log.log.debug("Total " + loadedEntries);
                   
                        if (Integer.parseInt(loadedEntriesCount) > 0)
                        {
                            if(input.containsKey("data")) {
                                JSON objectsJSON = JSONSerializer.toJSON(input.get("data"));
                                if (objectsJSON instanceof JSONArray)
                                {
                                    result = parseObjectsDataJsonArray((JSONArray) objectsJSON);

                                }
                                if ((result.size() > 0) && StringUtils.isNotEmpty(loadedEntriesCount))
                                {
                                    Map<String, String> loadedEntries1 = new HashMap<String, String>();
                                    loadedEntries1.put("loadedNoOfEntries", loadedEntriesCount);
                                    result.add(loadedEntries1);
                                }  
                            }
                            

                        }
                    }
                    else if(loadedEntries.contains("SQL")) 
                    {
                        String rowsModifiedCount = "";
                        StringTokenizer token = new StringTokenizer(loadedEntries, " ");
                        int i = 0;
                        while (token.hasMoreTokens())
                        {
                            if (i == 2)
                            {
                                rowsModifiedCount = token.nextToken();
                                break;
                            }
                            token.nextToken();
                            i++;
                        }
                        
                       
                        if (Integer.parseInt(rowsModifiedCount) > 0)
                        {
                            
                            Map<String, String> modifiedEntries = new HashMap<String, String>();
                            modifiedEntries.put("SQL modified rows", rowsModifiedCount);
                            if(query.contains("api/eventBase/tools/executeSQLTool/"+MonolithGateway.ackToolId)) 
                            {
                              
                                Log.log.debug("No of events acknowledged: " + rowsModifiedCount);
                                System.out.println("No of events acknowledged: " + rowsModifiedCount);
                            }
                            if(query.contains("api/eventBase/tools/executeSQLTool/"+MonolithGateway.unackToolId)) 
                            {
                                Log.log.debug("No of events UnAcknowledged: " + rowsModifiedCount);
                                System.out.println("No of events UnAcknowledged: " + rowsModifiedCount);
                            }
                            result.add(modifiedEntries);
                        
                        }
                    }

                }
            }
            return result;
        }
        
        catch (Exception e)
        {
            Log.log.error(e.getMessage(), e);
            e.printStackTrace();
            throw e;
           
        }
       
    }

    public Map<String,String> getServerInfo(String username, String password)
    {

        String query = "/api/broker/Servers/read";
        query = restCaller.getBaseUrl() + query;
      
        String response = "";
        try
        {
            response = restCaller.getMethod(query, null);
            
            return StringUtils.jsonObjectToMap(StringUtils.stringToJSONObject(response));
        }
        catch (Exception e)
        {
            Log.log.debug("CHeck Gateway Connection Returned: "+response);
            Log.log.error(e.getMessage(), e);
        }

        return null;

    }
    private static List<Map<String, String>> parseObjectsDataJsonArray(JSONArray objectsData)
    {

        JSONArray jsonArray=objectsData;
        
        List<Map<String, String>> objectDataMapList = new ArrayList<Map<String, String>>();

          
            for (int i = 0; i < jsonArray.size(); i++)
            {
                Map<String, String> alarm = new HashMap<String, String>();
                Map<String, String> jsonArrayMap = StringUtils.jsonObjectToMap(jsonArray.getJSONObject(i));

                Set<String> arrayKeySet = jsonArrayMap.keySet();
                for (Iterator<String> arrayIterator = arrayKeySet.iterator(); arrayIterator.hasNext();)
                {
                    String arrayKey = arrayIterator.next();
                    String arrayValue = jsonArrayMap.get(arrayKey);
                    alarm.put(arrayKey, arrayValue);
                }

                objectDataMapList.add(alarm);

            }

       
            return objectDataMapList;

    }

    @Override
    public String getFilterId(String filterName) throws Exception
    {
        // TODO Auto-generated method stub
        String filterId = "";
        String filter = "";

        String queryGetFilterName = "api/eventBase/Filters/read";

        queryGetFilterName = queryGetFilterName + "?filter=" + URLEncoder.encode("[{\"property\": \"FilterName\", \"value\":\"" + filterName + "\", \"type\": \"string\",\"operator\": \"eq\"}]", "UTF-8");
        List<Map<String, String>> results = getObjects(queryGetFilterName, null);

        if (results.size() > 0)
        {

            Map<String, String> loadedEntries = results.get(results.size() - 1);
            if (loadedEntries.containsKey("loadedNoOfEntries"))
            {
                String loadedNoOfEntries = loadedEntries.get("loadedNoOfEntries");

                int noIfFiltersWithGivenName = Integer.parseInt(loadedNoOfEntries);

                if (noIfFiltersWithGivenName == 1)
                {
                    results.remove(results.size() - 1);
                    Map<String, String> filterData = results.get(0);
                    filterId = filterData.get("FilterID");

                }
                if (StringUtils.isNotEmpty(filterId))
                {
                    Log.log.debug("FilterName: " + filterName + "FilterId: " + filterId);
                    System.out.println("FilterName: " + filterName + "FilterId: " + filterId);
                }
                else
                {
                    Log.log.error("Failed to get FilterId for FilterName: " + filterName);
                }

            }

        }
        return filterId;
    }

    @Override
    public String getDisplayId(String displayName) throws Exception
    {
        // TODO Auto-generated method stub

        String displayId = "";

        String filter = "[{\"property\": \"DisplayName\", \"value\":\"" + displayName + "\", \"type\": \"string\",\"operator\": \"eq\"}]";

        String queryGetDisplayName = "api/eventBase/displays/read?filter=" + URLEncoder.encode("[{\"property\": \"DisplayName\", \"value\":\"" + displayName + "\", \"type\": \"datetime\",\"operator\": \"eq\"}]", "UTF-8");

        Map<String, String> reqHeader = new HashMap<>();

        reqHeader.put("filter", "[{\"property\": \"DisplayName\", \"value\":\"" + displayName + "\", \"type\": \"string\",\"operator\": \"eq\"}]");
        List<Map<String, String>> results = getObjects(queryGetDisplayName, null);

        if (results.size() > 0)
        {

            Map<String, String> loadedEntries = results.get(results.size() - 1);
            if (loadedEntries.containsKey("loadedNoOfEntries"))
            {
                String loadedNoOfEntries = loadedEntries.get("loadedNoOfEntries");

                int noOfDisplaysWithGivenName = Integer.parseInt(loadedNoOfEntries);

                if (noOfDisplaysWithGivenName == 1)
                {
                    results.remove(results.size() - 1);
                    Map<String, String> filterData = results.get(0);
                    displayId = filterData.get("DisplayID");
                    System.out.println("Display Name: " + displayName + "DisplayId: " + displayId);

                }
                if (StringUtils.isNotEmpty(displayId))
                {
                    Log.log.debug("DisplayName: " + displayName + " DisplayId: " + displayId);
                }
                else
                {
                    Log.log.error("Failed to get DisplayId for DisplayName: " + displayName);
                }

            }

        }
        return displayId;

    }

    @Override
    public String validateQuery(MonolithFilter monolithFilter, String filterId, String displayId,Boolean hasVariable)
    {

        String query = monolithFilter.getDefaultQuery();
        if(query.equalsIgnoreCase("Custom")) {
            query = monolithFilter.getQuery();
           
        }
        String queryFilter;
        String queryFilterOnFirstReportedTime="";
        String queryFilterOnAlarmId="";
        String queryFilterOnAckStatus="";
        String basicQuery="";
        try
        {
            if(!monolithFilter.getFilterName().equals("Acked Events")) 
            {
                 queryFilterOnAckStatus = "\"property\": \"Ack\", \"value\":\"0\", \"type\": \"int\",\"operator\": \"eq\"";
            }
            else if(monolithFilter.getFilterName().equals("Acked Events")) 
            {
                queryFilterOnAckStatus = "\"property\": \"Ack\", \"value\":\"1\", \"type\": \"int\",\"operator\": \"eq\"";
            }
                
            
           
            //If query has FIRST_REPORTED time i.e Fetching events based on timestamp
            if (hasVariable)
            {
                if (query.equalsIgnoreCase("api/eventBase/events?FirstReported>${FIRST_REPORTED}") || query.equalsIgnoreCase("/api/eventBase/events?FirstReported>${FIRST_REPORTED}") )
                {
                    queryFilterOnFirstReportedTime = "\"property\":\"FirstReported\",\"value\":\"" + monolithFilter.getFirstReportedTime() + "\",\"type\":\"datetime\",\"operator\":\"gt\"";
                }
                if (query.equalsIgnoreCase("api/eventBase/events?FirstReported>=${FIRST_REPORTED}") || query.equalsIgnoreCase("/api/eventBase/events?FirstReported>=${FIRST_REPORTED}") )
                {
                    queryFilterOnFirstReportedTime = "\"property\":\"FirstReported\",\"value\":\"" + monolithFilter.getFirstReportedTime() + "\",\"type\":\"datetime\",\"operator\":\"gte\"";
                }
            }

             if(query.equalsIgnoreCase("api/eventBase/events")|| query.equalsIgnoreCase("/api/eventBase/events")|| query.startsWith("api/eventBase/events?FirstReported") ||  query.startsWith("/api/eventBase/events?FirstReported")) 
            {
                if (StringUtils.isNotEmpty(monolithFilter.getLastReadAlarmId()))
                {
                    queryFilterOnAlarmId = "\"property\": \"AlarmId\", \"value\":\"" + monolithFilter.getLastReadAlarmId() + "\", \"type\": \"int\",\"operator\": \"gt\"";
                }
                
                String filterCriteria = "[{" + queryFilterOnAckStatus+"}";
                if(StringUtils.isNotEmpty(queryFilterOnFirstReportedTime)) 
                {
                    filterCriteria = filterCriteria +  ",{" +queryFilterOnFirstReportedTime+"}";
                }
                if(StringUtils.isNotEmpty(queryFilterOnAlarmId)) {
                    filterCriteria = filterCriteria +  ",{" +queryFilterOnAlarmId+"}";
                }
                filterCriteria =filterCriteria+"]";
                Log.log.debug("Filter criteria in REST call is: "+filterCriteria);
                
                queryFilter = URLEncoder.encode(filterCriteria, "UTF-8");
                
                query = "api/eventBase/events" + "?FilterType=FilterID" + "&FilterValue=" + filterId + "&DisplayID=" + displayId + "&filter=" + queryFilter;
             
              
               
            }
             //Validate custom query
             else 
             {
                 if (StringUtils.isNotEmpty(monolithFilter.getLastReadAlarmId()))
                 {
                     queryFilterOnAlarmId = "\"property\": \"AlarmId\", \"value\":\"" + monolithFilter.getLastReadAlarmId() + "\", \"type\": \"int\",\"operator\": \"gt\"";
                 }
                 String baseUrl = restCaller.getBaseUrl();
                 String tempQuery="";
                 if((!query.startsWith("https:")) ||(!query.startsWith("http:")) ) {
                   
                    if(!query.startsWith("/")) {
                        tempQuery = baseUrl+query;
                        
                    }
                    else if(query.startsWith("/") && baseUrl.endsWith("/"))
                    {
                       baseUrl =baseUrl.substring(0, baseUrl.length()-1);
                       tempQuery = baseUrl+query;
                    }
                   
                 }
                 
                 if(query.contains("&filter=") || query.contains("Filter=")) 
                 {
                     if(query.contains("Filter=")) {
                         query = query.replace("Filter=", "filter=");
                     }
                     Map<String, String> queryParamMap = splitQuery(tempQuery);
                    if (queryParamMap != null)
                    {
                        if (queryParamMap.containsKey("filter"))
                        {
                            String filterCriteria = queryParamMap.get("filter");
                            
                            if(StringUtil.isNotBlank(queryFilterOnAlarmId) && queryFilterOnAlarmId!=null) {
                                int index =filterCriteria.indexOf("}]");
                                filterCriteria = filterCriteria.substring(0, index)+"},{"+queryFilterOnAlarmId+"}]";
                            }
                            
                            filterCriteria = URLEncoder.encode(filterCriteria, "UTF-8");
                             basicQuery = query.substring(0, query.indexOf("?") + 1);
                            int i = 0;
                            for (Entry<String, String> entry : queryParamMap.entrySet())
                            {
                                String key = entry.getKey();
                                Object value = entry.getValue();
                                if (key.equals("filter"))
                                {
                                    value = filterCriteria;
                                }

                                if (i < queryParamMap.size() && (i != queryParamMap.size()-1))
                                {
                                    basicQuery = basicQuery + key + "=" + value + "&";
                                }
                                else
                                {
                                    basicQuery = basicQuery + key + "=" + value;
                                }

                                i++;
                            }

                        }
                    }
                     
                     
                 }
                 query=basicQuery;
                // query = query +"&start=" + "0"+ "&limit=" + FETCH_LIMIT;
             }
  
        }
        
        catch (UnsupportedEncodingException e)
        {
          
            Log.log.error(e.getMessage(), e);
        }
        Log.log.trace("REST Call: "+query);
        
        query = query +"&start=" + "0"+ "&limit=" + FETCH_LIMIT;//100;////"1000";
        String sortOnAlarmId = "[{\"property\":\"AlarmId\",\"direction\":\"ASC\"}]";
        query = query +"&sort="+URLEncoder.encode(sortOnAlarmId);
        
        return query;
        // TODO Auto-generated method stub

    }
    public static Map<String, String> splitQuery(String  url) throws UnsupportedEncodingException 
    {

        Map<String, String> paramKeyvalPairs = new LinkedHashMap<String, String>();
        try
        {
            URL urlQuery = new URL(url);
            String query = urlQuery.getQuery();
            String[] pairs = query.split("&");
            for (String pair : pairs)
            {
                int idx = pair.indexOf("=");
               // paramKeyvalPairs.put(URLDecoder.decode(pair.substring(0, idx), "UTF-8"), URLDecoder.decode(pair.substring(idx + 1), "UTF-8"));
                paramKeyvalPairs.put(pair.substring(0, idx), pair.substring(idx + 1));
            }
        }
        catch (Exception e)
        {
            Log.log.error("Error while parsing the custom Query provided on Gateway filter");
        }
        return paramKeyvalPairs;
    }

    @Override
    public String createSQLQueryTool(String toolTypeId, String toolSQL, String toolName, String journalStatus, String icon, String journalext) throws Exception
    {
        // TODO Auto-generated method stub
        String createToolResult=new String();
        List<NameValuePair> params = new ArrayList<NameValuePair>();
        params.add(new BasicNameValuePair("ToolTypeID", toolTypeId));
        params.add(new BasicNameValuePair("ToolSQL", toolSQL));
        params.add(new BasicNameValuePair("ToolName", toolName));
        params.add(new BasicNameValuePair("JournalStatus", journalStatus));
        params.add(new BasicNameValuePair("Icon", icon));
        params.add(new BasicNameValuePair("JournalText", journalext));
        
        Map<String, String> reqHeaders = new HashMap<String, String>();
       reqHeaders.put(HttpHeaders.CONTENT_TYPE, "application/x-www-form-urlencoded");
        reqHeaders.put("Accept", "application/json");
      
        
        try
        {
            restCaller.setUrlSuffix("api/eventBase/Tools/create");
            //{"success":true,"message":"Added record","data":{"id":"1005"}}
            //
          
            Collection<Integer> expectedStatusCodes = new HashSet<Integer>();
            expectedStatusCodes.add(new Integer(HttpStatus.SC_OK));
            
            String response = restCaller.callPost(null, reqHeaders, params,expectedStatusCodes);
            if(StringUtils.isEmpty(response)) {
                Log.log.error("No response from REST POST request is received");
                               
            }
            
            JSONObject input = JSONObject.fromObject(response);
            if ( (input.containsKey("success")&& input.getString("success").equalsIgnoreCase("true")) )//input.containsKey("data") &&
            {
                JSONObject id = JSONObject.fromObject(input.getString("data"));
                createToolResult = id.getString("id");
            }
            //{"success":false,"message":"One or more validations failed","data":[],"errors":{"Icon":"This field is required","ToolSQL":"This field is required"}}
          //{"success":false,"message":"One or more validations failed","data":[],"errors":{"ToolName":"This ToolName already exists"}}
            else if( (input.containsKey("success")&& input.getString("success").equalsIgnoreCase("false")) )
            {
                JSONObject errors = JSONObject.fromObject(input.getString("errors"));
                createToolResult = "Failed to create tool.\n"+StringUtils.jsonObjectToString(errors);
            }
        }
        catch (RestCallException e)
        {
           
            Log.log.error(e.getMessage(), e);
            throw e;
        }
        catch (Exception e)
        {
            Log.log.error(e.getMessage(), e);
            throw e;
        }
      return createToolResult;

    }

    @Override
    public String getToolId(String toolName) throws Exception
    {
        
        // TODO Auto-generated method stub
        String toolId="";
        String filter ="";
        
        String queryGetToolName ="api/eventBase/tools/read";
        
        queryGetToolName = queryGetToolName+"?filter="+URLEncoder.encode("[{\"property\": \"ToolName\", \"value\":\""+toolName+"\", \"type\": \"string\",\"operator\": \"eq\"}]","UTF-8");
         List<Map<String, String>> results =  getObjects(queryGetToolName,null);
         
         if(results.size() >0) {
             
             Map<String,String> loadedEntries = results.get(results.size()-1);
             if(loadedEntries.containsKey("loadedNoOfEntries")) 
             {
                 String loadedNoOfEntries = loadedEntries.get("loadedNoOfEntries");
                 
                 int noOfQueryToolWithGivenName = Integer.parseInt(loadedNoOfEntries);
                 
               
                if(noOfQueryToolWithGivenName==1) {
                    results.remove(results.size()-1);
                    Map<String,String> filterData = results.get(0);
                    toolId  = filterData.get("ToolID");
                    
                }
                if(StringUtils.isNotEmpty(toolId)) {
                 Log.log.debug("ToolName: "+ toolName+"ToolId: "+toolId);
                 System.out.println("ToolName: "+ toolName+"ToolId: "+toolId);
                }
                else 
                {
                    Log.log.debug("Tool does not exist in monolith: "+toolName);
                }
                 
             }
            
      
         }
        return toolId;
       
    }

    @Override
    public String getUserId(String userName) throws Exception
    {
        
        // TODO Auto-generated method stub
        String userId="";
              
        String queryGetUserId ="api/coreAAA/Users/read";
        
        queryGetUserId = queryGetUserId+"?filter="+URLEncoder.encode("[{\"property\": \"UserName\", \"value\":\""+userName+"\", \"type\": \"string\",\"operator\": \"eq\"}]","UTF-8");
         List<Map<String, String>> results =  getObjects(queryGetUserId,null);
         
         if(results.size() >0) {
             
             Map<String,String> loadedEntries = results.get(results.size()-1);
             if(loadedEntries.containsKey("loadedNoOfEntries")) 
             {
                 String loadedNoOfEntries = loadedEntries.get("loadedNoOfEntries");
                 
                 int noOfUsersWithGivenName = Integer.parseInt(loadedNoOfEntries);
                 
               
                if(noOfUsersWithGivenName==1) {
                    results.remove(results.size()-1);
                    Map<String,String> filterData = results.get(0);
                    userId  = filterData.get("UserID");
                    
                }
                if(StringUtils.isNotEmpty(userId)) {
                 Log.log.debug("UserName: "+ userName+" UserID: "+userId);
                 System.out.println("UserName: "+ userName+" UserID: "+userId);
                }
                else {
                    Log.log.debug("User does not exist in monolith: "+userName);
                }
                 
             }
            
      
         }
        return userId;
       
    }

    @Override
    public void addJournalEntry(String alarms, String journalComment) throws Exception
    {   
     // TODO Auto-generated method stub
       String addJournalEntryResult=new String();
        if(StringUtils.isEmpty(alarms) || (alarms == null) ) {
            Log.log.error("Please pass AlarmId to make journal entry");
            throw new Exception("AlarmId is required to make journal entry.");
        }
        
        if(StringUtils.isEmpty(journalComment) || (journalComment == null) ) {
            Log.log.error("Please pass Journal Comment to make journal entry");
            throw new Exception(" Journal Comment is required to make journal entry.");
        }
        if(alarms.endsWith(",")) {
            alarms = alarms.substring(0, alarms.length()-2);
        }
        
        List<NameValuePair> params = new ArrayList<NameValuePair>();
        
        params.add(new BasicNameValuePair("Alarms", alarms));
        params.add(new BasicNameValuePair("JournalEntry", journalComment));
        params.add(new BasicNameValuePair("DataType", "realtime"));

        
        Map<String, String> reqHeaders = new HashMap<String, String>();
       reqHeaders.put(HttpHeaders.CONTENT_TYPE, "application/x-www-form-urlencoded");
        reqHeaders.put("Accept", "application/json");
      
        
        try
        {
            restCaller.setUrlSuffix("api/eventBase/alarmJournals");
            //{"success":true,"message":"Added record","data":{"id":"1005"}}
          
            Collection<Integer> expectedStatusCodes = new HashSet<Integer>();
            expectedStatusCodes.add(new Integer(HttpStatus.SC_OK));
            
            String response = restCaller.callPost(null, reqHeaders, params,expectedStatusCodes);
            if(StringUtils.isEmpty(response)) {
                Log.log.error("No response from REST POST request is received");
                               
            }
            
            JSONObject input = JSONObject.fromObject(response);
            if ( (input.containsKey("success")&& input.getString("success").equalsIgnoreCase("true")) )//input.containsKey("data") &&
            {
                JSONObject id = JSONObject.fromObject(input.getString("data"));
                addJournalEntryResult = id.getString("id");
                Log.log.debug("Journal entry added successfully. Id: "+ addJournalEntryResult);
            }
            //{"success":false,"message":"One or more validations failed","data":[],"errors":{"Icon":"This field is required","ToolSQL":"This field is required"}}
          //{"success":false,"message":"One or more validations failed","data":[],"errors":{"ToolName":"This ToolName already exists"}}
            else if( (input.containsKey("success")&& input.getString("success").equalsIgnoreCase("false")) )
            {
                JSONObject errors = JSONObject.fromObject(input.getString("errors"));
                addJournalEntryResult = "Failed to add journal entry.\n"+StringUtils.jsonObjectToString(errors);
            }
        }
        catch (RestCallException e)
        {
           
            Log.log.error(e.getMessage(), e);
            throw e;
        }
        catch (Exception e)
        {
            Log.log.error(e.getMessage(), e);
            throw e;
        }
     // return addJournalEntryResult;

        
    }

    @Override
    public String updateEvent(String alarms, String summary) throws Exception
    {   
        String updateEventResult = "ERROR: Error occured when updating alarm id " + alarms;
        if(StringUtils.isEmpty(alarms) || (alarms == null) ) {
            Log.log.error("Please pass AlarmId For update");
            throw new Exception("AlarmId is required");
        }
        if(StringUtils.isEmpty(summary) || (summary == null) ) {
        	summary = "";
        }
       
        if(alarms.endsWith(",")) {
            alarms = alarms.substring(0, alarms.length()-2);
        }
        List<NameValuePair> params = new ArrayList<NameValuePair>();
        
        params.add(new BasicNameValuePair("id", alarms));
        params.add(new BasicNameValuePair("Summary", summary));
        
        Map<String, String> reqHeaders = new HashMap<String, String>();
        reqHeaders.put(HttpHeaders.CONTENT_TYPE, "application/x-www-form-urlencoded");
        reqHeaders.put("Accept", "application/json");
        
        try
        {
            restCaller.setUrlSuffix("api/eventBase/events/update");
            //{"success":true,"message":"Added record","data":{"id":"1005"}}
            Collection<Integer> expectedStatusCodes = new HashSet<Integer>();
            expectedStatusCodes.add(new Integer(HttpStatus.SC_OK));
            
            String response = restCaller.callPost(null, reqHeaders, params,expectedStatusCodes);
            if(StringUtils.isEmpty(response)) {
                Log.log.error("No response from REST POST request is received");
                updateEventResult = "ERROR:No response from REST POST request";
                return updateEventResult;
            }
            JSONObject input = JSONObject.fromObject(response);
            if ( (input.containsKey("success")&& input.getString("success").equalsIgnoreCase("true")) )//input.containsKey("data") &&
            {
                Log.log.debug("Event updated successfully ");
                updateEventResult = "SUCCESS: " +input.getString("message");
            }
            else if( (input.containsKey("success")&& input.getString("success").equalsIgnoreCase("false")) )
            {
//                JSONObject errors = JSONObject.fromObject(input.getString("message"));
                updateEventResult = "ERROR: "+input.getString("message");
            }
        }
        catch (RestCallException e)
        {
            Log.log.error(e.getMessage(), e);
            throw e;
        }
        catch (Exception e)
        {
            Log.log.error(e.getMessage(), e);
            throw e;
        }
        return updateEventResult;

    }
    
}
