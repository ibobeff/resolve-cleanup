package com.resolve.gateway.monolith;

import java.io.File;
import java.util.HashMap;
import java.util.Map;
import org.apache.commons.io.FileUtils;

import com.resolve.rsbase.MainBase;
import com.resolve.gateway.Filter;
import com.resolve.rsremote.ConfigReceiveGateway;
import com.resolve.util.Log;
import com.resolve.util.ObjectProperties;
import com.resolve.util.StringUtils;
import com.resolve.util.XDoc;

public class ConfigReceiveMonolith extends ConfigReceiveGateway {

    private static final long serialVersionUID = 1L;

    private static final String RECEIVE_MONOLITH_NODE = "./RECEIVE/MONOLITH/";

    private static final String RECEIVE_MONOLITH_FILTER = RECEIVE_MONOLITH_NODE + "FILTER";

    private String queue = "MONOLITH";

    private static final String RECEIVE_MONOLITH_ATTR_PASSWORD = RECEIVE_MONOLITH_NODE + "@PASSWORD";

    private static final String RECEIVE_MONOLITH_ATTR_DEFAULTTIMEZONE = RECEIVE_MONOLITH_NODE + "@DEFAULTTIMEZONE";

    private static final String RECEIVE_MONOLITH_ATTR_TIMEDIFFERENCEINSEC = RECEIVE_MONOLITH_NODE + "@TIMEDIFFERENCEINSEC";

    private static final String RECEIVE_MONOLITH_ATTR_DISPLAYNAME = RECEIVE_MONOLITH_NODE + "@DISPLAYNAME";

    private static final String RECEIVE_MONOLITH_ATTR_WEBFQDN = RECEIVE_MONOLITH_NODE + "@WEBFQDN";

    private static final String RECEIVE_MONOLITH_ATTR_USERNAME = RECEIVE_MONOLITH_NODE + "@USERNAME";

    private String password = "";

    private String defaulttimezone = "";

    private String TimeDifferenceInSec = "";

    private String DisplayName = "";

    private String webfqdn = "";

    private String username = "";

    public String getQueue() {
        return queue;
    }

    public void setQueue(String queue) {
        this.queue = queue;
    }

    public String getPassword() {
        return this.password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getDefaulttimezone() {
        return this.defaulttimezone;
    }

    public void setDefaulttimezone(String defaulttimezone) {
        this.defaulttimezone = defaulttimezone;
    }

    public String getTimeDifferenceInSec() {
        return this.TimeDifferenceInSec;
    }

    public void setTimeDifferenceInSec(String timeDifferenceInSec) {
        this.TimeDifferenceInSec = timeDifferenceInSec;
    }

    public String getDisplayName() {
        return this.DisplayName;
    }

    public void setDisplayName(String displayName) {
        this.DisplayName = displayName;
    }

    public String getWebfqdn() {
        return this.webfqdn;
    }

    public void setWebfqdn(String webfqdn) {
        this.webfqdn = webfqdn;
    }

    public String getUsername() {
        return this.username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public ConfigReceiveMonolith(XDoc config) throws Exception {
        super(config);
        define("password", SECURE, RECEIVE_MONOLITH_ATTR_PASSWORD);
        define("defaulttimezone", STRING, RECEIVE_MONOLITH_ATTR_DEFAULTTIMEZONE);
        define("TimeDifferenceInSec", STRING, RECEIVE_MONOLITH_ATTR_TIMEDIFFERENCEINSEC);
        define("DisplayName", STRING, RECEIVE_MONOLITH_ATTR_DISPLAYNAME);
        define("webfqdn", STRING, RECEIVE_MONOLITH_ATTR_WEBFQDN);
        define("username", STRING, RECEIVE_MONOLITH_ATTR_USERNAME);
    }

    @Override
    public String getRootNode() {
        return RECEIVE_MONOLITH_NODE;
    }

    @Override
    public void load() throws Exception {
        try {
            loadAttributes();
            if (isActive()) {
                MonolithGateway monolithGateway = MonolithGateway.getInstance(this);
                filters = xdoc.getListMapValue(RECEIVE_MONOLITH_FILTER);
                if (filters.size() > 0) {
                    for (Map<String, Object> values : filters) {
                        String filterName = ((String) values.get(MonolithFilter.ID)).toLowerCase();
                        File scriptFile = getFile(MainBase.main.release.serviceName + "/config/monolith/" + filterName + ".groovy");
                        try {
                            if (scriptFile.exists()) {
                                String groovy = FileUtils.readFileToString(scriptFile, "UTF-8");
                                if (StringUtils.isNotBlank(groovy)) {
                                    values.put(MonolithFilter.SCRIPT, groovy);
                                    Log.log.debug("Loaded groovy script: " + scriptFile);
                                }
                            }
                            monolithGateway.setFilter(values);
                        } catch (Exception e) {
                            Log.log.warn(e.getMessage() + ". Skipping file: " + scriptFile.getAbsolutePath());
                        }
                    }
                }
            }
        } catch (Throwable e) {
            setActive(false);
            Log.log.warn("Couldn not load configuration for Monolith gateway, check blueprint. " + e.getMessage(), e);
        }
    }

    @Override
    public void save() throws Exception {
        File dir = getFile(MainBase.main.release.serviceName + "/config/monolith");
        if (!dir.exists()) {
            FileUtils.forceMkdir(dir);
        }
        if (filters != null && isActive()) {
            filters.clear();
            for (Filter filter : MonolithGateway.getInstance().getFilters().values()) {
                MonolithFilter monolithFilter = (MonolithFilter) filter;
                String groovy = monolithFilter.getScript();
                String scriptFileName;
                File scriptFile = null;
                if (StringUtils.isNotBlank(groovy)) {
                    scriptFileName = monolithFilter.getId().toLowerCase() + ".groovy";
                    scriptFile = getFile(MainBase.main.release.serviceName + "/config/monolith/" + scriptFileName);
                }
                Map<String, Object> entry = new HashMap<String, Object>();
                putFilterDataIntoEntry(entry, monolithFilter);
                if (StringUtils.isNotBlank(monolithFilter.getFirstReportedTime()))
                {
                   Log.log.debug("Filter retained value of FirstOccurredTime: "+monolithFilter.getFirstReportedTime());
                    entry.put(MonolithFilter.FIRST_REPORTED_TIME, monolithFilter.getFirstReportedTime());
                }
                if (StringUtils.isNotBlank(monolithFilter.getLastReadAlarmId()))
                {
                    Log.log.debug("Filter retained value of Start Index: "+monolithFilter.getLastReadAlarmId());
                    entry.put(MonolithFilter.LAST_READ_ALARM_ID, monolithFilter.getLastReadAlarmId());
                }
                filters.add(entry);
                if (scriptFile != null) {
                    try {
                        FileUtils.writeStringToFile(scriptFile, groovy, "UTF-8");
                    } catch (Exception e) {
                        Log.log.warn(e.getMessage() + ". Skipping file: " + scriptFile.getAbsolutePath());
                    }
                }
            }
            xdoc.setListMapValue(RECEIVE_MONOLITH_FILTER, filters);
        }
        if (nameProperties != null && isActive()) {
            nameProperties.clear();
            for (String name : MonolithGateway.getInstance().getNameProperties().keySet()) {
                Map<String, Object> entry = new HashMap<String, Object>();
                entry.put(NAME_KEY, name);
                nameProperties.add(entry);
                Map props = MonolithGateway.getInstance().getNameProperties().getNameProperties(name);
                ObjectProperties prop = new ObjectProperties(props);
                prop.save(props);
            }
        }
        saveAttributes();
    }

    private void putFilterDataIntoEntry(Map<String, Object> entry, MonolithFilter monolithFilter) {
        entry.put(MonolithFilter.ID, monolithFilter.getId());
        entry.put(MonolithFilter.ACTIVE, String.valueOf(monolithFilter.isActive()));
        entry.put(MonolithFilter.ORDER, String.valueOf(monolithFilter.getOrder()));
        entry.put(MonolithFilter.INTERVAL, String.valueOf(monolithFilter.getInterval()));
        entry.put(MonolithFilter.EVENT_EVENTID, monolithFilter.getEventEventId());
        entry.put(MonolithFilter.RUNBOOK, monolithFilter.getRunbook());
        entry.put(MonolithFilter.SCRIPT, monolithFilter.getScript());
        entry.put(MonolithFilter.QUERY, monolithFilter.getQuery());
        entry.put(MonolithFilter.FILTERNAME, monolithFilter.getFilterName());
        entry.put(MonolithFilter.DEFAULTQUERY, monolithFilter.getDefaultQuery());
    }
}

