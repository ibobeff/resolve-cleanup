package com.resolve.gateway;

import java.util.Map;
import com.resolve.gateway.monolith.MonolithFilter;
import com.resolve.gateway.monolith.MonolithGateway;

public class MMonolith extends MGateway {

    private static final String RSCONTROL_SET_FILTERS = "MMonolith.setFilters";

    private static final MonolithGateway instance = MonolithGateway.getInstance();

    public MMonolith() {
        super.instance = instance;
    }

    protected String getSetFilterMethodName() {
        return RSCONTROL_SET_FILTERS;
    }

    /**
     * Populates the {@link Filter} data into the given filterMap {@link Map} instance
     * 
     * @param filterMap
     *      the {@link Map} instance to hold the {@link Filter} data
     * @param filter
     *      a {@link MonolithFilter} instance
     *            
     */
    public void populateGatewaySpecificFilterProperties(Map<String, String> filterMap, Filter filter) {
        if (instance.isActive()) {
            MonolithFilter monolithFilter = (MonolithFilter) filter;
            filterMap.put(MonolithFilter.QUERY, monolithFilter.getQuery());
            filterMap.put(MonolithFilter.FILTERNAME, monolithFilter.getFilterName());
          //  filterMap.put(MonolithFilter.LAST_READ_ALARM_ID, monolithFilter.getLastReadAlarmId());
            
           // filterMap.put(MonolithFilter.FIRST_REPORTED_TIME, monolithFilter.getFirstReportedTime());
            filterMap.put(MonolithFilter.DEFAULTQUERY, monolithFilter.getDefaultQuery());
        }
    }
}

