package com.resolve.gateway.monolith;

import java.util.List;
import java.util.Map;

public interface ObjectService
{
   
    public void ackEvent(String Events,String Shards) throws Exception;
    
    public String getFilterId(String filterName) throws Exception;
    public String getDisplayId(String displayName) throws Exception;
    public String getToolId(String toolName)  throws Exception;
    public String getUserId(String userName)  throws Exception;
    
    public void unackEvent(String Events,String Shards) throws Exception;
    public  void addJournalEntry(String alarms,String journalComment) throws Exception;
    public void addJournalEntryUsingDBGateway(String poolName,String sql) throws Exception;
    public List<Map<String, String>> getObjects(String url,Map<String, String> reqHeader) throws Exception;
    public String validateQuery(MonolithFilter monolithFilter,String filterId,String displayId,Boolean hasVariable);
    public String createSQLQueryTool(String toolTypeId,String toolSQL,String toolName,String journalStatus,String icon,String journalext) throws Exception;
    public Map<String, String> getServerInfo(String userName,String password);
    public String updateEvent(String alarmId, String summary) throws Exception;
        

}
