package com.resolve.gateway.monolith;

import java.util.Map;
import java.util.TimeZone;

import java.util.List;
import java.math.BigInteger;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.HashMap;

import com.resolve.rsbase.MainBase;
import com.resolve.gateway.BaseClusteredGateway;
import com.resolve.gateway.BaseFilter;
import com.resolve.gateway.Filter;
import com.resolve.gateway.MMonolith;
import com.resolve.util.Log;
import com.resolve.util.StringUtils;
import com.resolve.rsremote.ConfigReceiveGateway;

public class MonolithGateway extends BaseClusteredGateway
{

    private static volatile MonolithGateway instance = null;
    public static volatile ObjectService genericObjectService;
    public static volatile String displayId;
    public static volatile String ackToolId = "";
    public static volatile String unackToolId = "";
    public static volatile String userId = "";

    private final static String TOOLTYPEID_SQL = "2";
    private final static String TOOLTYPEID_VIEW = "1";
    private final static String DEFAULT_TOOL_ICON = "";
    private final static String DEFAULT_ACKNOWLEDGE_TOOL_ICON = "fugue/thumb-up.png";
    private final static String DEFAULT_UNACKNOWLEDGE_TOOL_ICON = "fugue/thumb.png";
    private final static String ACK_EVENT_SQL = "UPDATE Alarm SET Ack = 1, OwnerID = $USERID WHERE Ack = 0 AND AlarmId IN ($ALARMLIST);";
    private final static String UNACK_EVENT_SQL = "UPDATE Alarm SET Ack = 0, OwnerID = $USERID WHERE Ack = 1 AND AlarmId IN ($ALARMLIST);";
    private final static String ACK_TOOL_NAME = "Acknowledge ResolveGateway SQLTool";
    private final static String UNACK_TOOL_NAME = "UnAcknowledge ResolveGateway SQLTool";
    private final static String JOURNALSTATUS_ENABLE = "Enable";
    private final static String JOURNALSTATUS_DISABLE = "Disable";
    private final static String ACK_JOURNAL_TEXT = "Event Acknowledged by Resolve Systems";
    private final static String UNACK_JOURNAL_TEXT = "Event Unacknowledged by Resolve Systems";

    private String queue = null;

    public static MonolithGateway getInstance(ConfigReceiveMonolith config)
    {
        if (instance == null)
        {
            instance = new MonolithGateway(config);
        }
        return instance;
    }

    /**
     * This method is called once the initialization is done. If it gets called
     * before the initialization it will throw exception.
     *
     * @return
     */
    public static MonolithGateway getInstance()
    {
        if (instance == null)
        {
            throw new RuntimeException("Monolith Gateway is not initialized correctly..");
        }
        else
        {
            return instance;
        }
    }

    private MonolithGateway(ConfigReceiveMonolith config)
    {
        super(config);
    }

    @Override
    public String getLicenseCode()
    {
        return "MONOLITH";
    }

    @Override
    protected void addAdditionalValues(Map<String, Object> arg0, Map<String, String> arg1)
    {
    }

    @Override
    protected String getGatewayEventType()
    {
        return "MONOLITH";
    }

    @Override
    protected String getMessageHandlerName()
    {
        return MMonolith.class.getSimpleName();
    }

    @Override
    protected Class<MMonolith> getMessageHandlerClass()
    {
        return MMonolith.class;
    }

    public String getQueueName()
    {
        return queue + getOrgSuffix();
    }

    @Override
    public void start()
    {
        Log.log.debug("Starting Monolith Gateway");
        super.start();
    }

    @Override
    public void run()
    {
        if (!((ConfigReceiveGateway) configurations).isMock()) super.sendSyncRequest();
        while (running)
        {
            try
            {

                ConfigReceiveMonolith config = (ConfigReceiveMonolith) configurations;
                long startTime = System.currentTimeMillis();
                Date date1 = new Date(startTime);
                if (primaryDataQueue.isEmpty())
                {
                    Log.log.trace("Local Primary Event Queue for " + getGatewayEventType() + " is empty.....");
                    for (Filter filter : orderedFilters)
                    {
                        MonolithFilter monolithFilter = (MonolithFilter) filter;
                        if (shouldFilterExecute(filter, startTime))
                        {
                            Log.log.trace("Filter " + filter.getId() + " executing");
                            /////

                            DateFormat df = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");

                            // Use Configured time zone to format the date in
                            df.setTimeZone(TimeZone.getTimeZone(config.getDefaulttimezone()));

                            System.out.println("\n------------- Next Interval Starts(" + filter.getId() + ")---------------------\n");
                            // System.out.println("Date and time in CST6CDT: " +
                            // df.format(date));
                            if (StringUtils.isEmpty(monolithFilter.getFirstReportedTime()))
                            {
                                int timeDiffInsecond = 0;
                                String timeDiff = config.getTimeDifferenceInSec();
                                if (isInteger(timeDiff))
                                {
                                    timeDiffInsecond = Integer.parseInt(timeDiff);
                                }
                                else
                                {
                                    throw new Exception("Please specify valid number for TimeDifferenceInSec property value in blueprint");
                                }

                                Date date = new Date(startTime + (timeDiffInsecond * 1000));

                                monolithFilter.setFirstReportedTime(df.format(date));
                                // monolithFilter.setStartIndex("0");
                                Log.log.debug("Starting to fetch events from server time: " + df.format(date));
                                System.out.println("Starting to fetch events from server time: " + df.format(date));
                                df.setTimeZone(TimeZone.getDefault());
                                // System.out.println("Date and time in local: "
                                // + df.format(date));
                                date = new Date(startTime);
                                Log.log.debug("Starting to fetch events from local time: " + df.format(date));
                                System.out.println("Starting to fetch events from local time: " + df.format(date));

                            }
                            // monolithFilter.setFirstReportedTime("2017-03-22
                            // 17:46:25");

                            // System.out.println("TimeStamp:
                            // "+monolithFilter.getFirstReportedTime() );
                            // System.out.println("StartIndex:
                            // "+monolithFilter.getStartIndex());

                            ///
                            List<Map<String, String>> results = invokeService(filter);
                            ArrayList<BigInteger> alarmIdList = new ArrayList<BigInteger>();
                            if (results.size() > 0)
                            {

                                Map<String, String> loadedEntries = results.get(results.size() - 1);
                                /*
                                 * if(loadedEntries.containsKey(
                                 * "loadedNoOfEntries")) { // String
                                 * loadedNoOfEntries =
                                 * loadedEntries.get("loadedNoOfEntries"); //
                                 * int prevStartIndex =
                                 * Integer.parseInt(monolithFilter.getStartIndex
                                 * ()); // int nextStartIndex = prevStartIndex +
                                 * Integer.parseInt(loadedNoOfEntries);
                                 *
                                 *
                                 * //
                                 * monolithFilter.setStartIndex(nextStartIndex+
                                 * ""); // System.out.println(loadedEntries.get(
                                 * "loadedNoOfEntries"));
                                 * results.remove(results.size()-1); }
                                 */
                                /*
                                 * for (Map<String, String> result : results) {
                                 * if(result.containsKey("AlarmId")) {
                                 * alarmIdList.add(BigInteger.valueOf(Long.
                                 * parseLong(result.get("AlarmId")))); } }
                                 * Collections.sort(alarmIdList);
                                 */
                                String lastReadAlarmId = "";
                                // String firstReadAlarmId="";
                                if (results.get(results.size() - 1).containsKey("AlarmId"))
                                {
                                    lastReadAlarmId = results.get(results.size() - 1).get("AlarmId");
                                    /*
                                     * firstReadAlarmId =
                                     * results.get(0).get("AlarmId");
                                     */
                                    monolithFilter.setLastReadAlarmId(lastReadAlarmId);
                                    // System.out.println("Fetched events from:
                                    // "+ firstReadAlarmId+"-"+lastReadAlarmId);
                                    // BigInteger lastReadAlarmId =
                                    // //alarmIdList.get(alarmIdList.size() -
                                    // 1);
                                }

                                // System.out.println("Last read alarmId:
                                // "+monolithFilter.getLastReadAlarmId());
                                // Log.log.debug("Total Events Fetched: " +
                                // results.size());
                                // System.out.println("Total Events Fetched: " +
                                // results.size());

                            }
                            for (Map<String, String> result : results)
                            {
                                addToPrimaryDataQueue(filter, result);
                            }
                        }
                        else
                        {
                            Log.log.trace("Filter:" + filter.getId() + " is inactive or not ready to execute yet.....");
                        }
                    }
                    if (orderedFilters == null || orderedFilters.size() == 0)
                    {
                        Log.log.trace("There is not deployed filter for MonolithGateway");
                    }
                }
                if (orderedFilters.size() == 0)
                {
                    Thread.sleep(interval);
                }
            }
            catch (Exception e)
            {
                Log.log.warn("Warning: " + e.getMessage());
                Log.alert("Instance " + MainBase.main.configId.getGuid() + " " + getLicenseCode() + "-" + getQueueName() + "-" + getInstanceType() + " Polling Failed", "Instance " + MainBase.main.configId.getGuid() + " " + getLicenseCode() + "-" + getQueueName() + "-" + getInstanceType() + " Polling Failed " + "due to " + e.getMessage());
                try
                {
                    Thread.sleep(interval);
                }
                catch (InterruptedException ie)
                {
                    Log.log.warn("sleep interrupted: " + ie.getMessage(), ie);
                }
            }
        }
    }

    /**
     * Add customized code here to get the data as List<Map<String, String>>
     * format for each filter
     *
     * @return List<Map<String, String>>
     */
    public List<Map<String, String>> invokeService(Filter filter)
    {

        List<Map<String, String>> result = new ArrayList<Map<String, String>>();

        MonolithFilter monolithFilter = (MonolithFilter) filter;

        try
        {
            String query = monolithFilter.getDefaultQuery();
            if (query.equalsIgnoreCase("Custom"))
            {
                query = monolithFilter.getQuery();
                if (!query.contains("api/eventBase/events"))
                {
                    Log.log.error("Custom query can be used only to fetch events from Monolith.");
                    throw new Exception("Please enter query on gateway filter that is to fetch events from Monolith.");
                }

            }

            if ((query.equals("")) && (query == null) && StringUtils.isNotEmpty(query))
            {
                Log.log.error("Query on gateway filter is required");
                throw new Exception("Please enter valid query on gateway filter.");
            }

            if (query.contains("start=") || query.contains("limit=") || query.contains("sort="))
            {
                Log.log.error("Please remove start/limit/sort parameters in the query. Gateway add them to the query by default.");
                throw new Exception("Please enter valid query on gateway filter.Please remove start/limit/sort parameters in the query.");
            }

            if (StringUtils.isEmpty(MonolithGateway.ackToolId))
            {

                throw new Exception("QueryTool is Not available.QueryTool to acknowledge events is required and should be created in Monolith before start fetching events.");
            }
            if (StringUtils.isEmpty(MonolithGateway.unackToolId))
            {

                throw new Exception("QueryTool is Not available. QueryTool to UnAcknowledge events is required and should be created in Monolith before start fetching events.");
            }

            if (StringUtils.isEmpty(MonolithGateway.userId))
            {
                Log.log.error("Failed to get UserId for user");
                throw new Exception("Please makesure user with given UserName exists in monolith. Specify correct username in blueprint properties");
            }

            String filterId = getFilterId(monolithFilter.getFilterName());

            if (filterId.equals(""))
            {
                throw new Exception("FilterName is invalid and required to fetch events from Monolith.Please specify valid Monolith FilterName in blueprint properties.");
            }

            // Read query from gateway filter

            // Update query
            query = validateQuery(monolithFilter, filterId, displayId);
            // Read events
            result = getEvents(query);
            // Acknowledge events

            // ---
            if (result.size() > 0)
            {

                Map<String, String> loadedEntries = result.get(result.size() - 1);
                if (loadedEntries.containsKey("loadedNoOfEntries"))
                {

                    result.remove(result.size() - 1);
                }

                String lastReadAlarmId = "";
                String firstReadAlarmId = "";
                if (result.get(result.size() - 1).containsKey("AlarmId"))
                {
                    lastReadAlarmId = result.get(result.size() - 1).get("AlarmId");
                    firstReadAlarmId = result.get(0).get("AlarmId");

                    System.out.println("Fetched events from: " + firstReadAlarmId + "-" + lastReadAlarmId);
                    Log.log.debug("Fetched events from: " + firstReadAlarmId + "-" + lastReadAlarmId);
                }
                // ---
                ackEvent(result);
                // unAckEvent(result);

            }
            Log.log.debug("Total Events Fetched: " + result.size());
            System.out.println("Total Events Fetched: " + result.size());

        }
        catch (Exception e)
        {
            Log.log.error(e.getMessage(), e);
        }

        return result;
    }

    private static boolean isInteger(String s)
    {
        try
        {
            Integer.parseInt(s);
        }
        catch (NumberFormatException e)
        {
            return false;
        }
        catch (NullPointerException e)
        {
            return false;
        }

        return true;
    }
    public boolean checkGatewayConnection() throws Exception
    {

        try
        {
            Map<String, String> serverInfo = genericObjectService.getServerInfo(null, null);

            if (serverInfo == null )
            {
                throw new Exception ("Failed to get server info from Monolith AssureNow server. Connection to Monolith AssureNow server possibly broken.");
            }
            else
            {
                Log.log.debug("Instance " + MainBase.main.configId.getGuid() + " : " + getLicenseCode() + "-" +
                              getQueueName() + "-" + getInstanceType() + " connected to configured Monolith AssureNow server");
            }
        }
        catch(Exception e)
        {
            Log.alert("Instance " + MainBase.main.configId.getGuid() + " breached " + getLicenseCode() + "-" + getQueueName() + "-" + getInstanceType() + "-ConnectionSelfCheck",
                      "Instance " + MainBase.main.configId.getGuid() + " Breached " + getLicenseCode() + "-" + getQueueName() + "-" + getInstanceType() + "-ConnectionSelfCheck" +
                      " due to " + e.getMessage());
        }

        return true;
    }
    /**
     * Add customized code here to initilize the connection with 3rd party
     * system
     *
     * @return List<Map<String, String>>
     */
    @Override
    protected void initialize()
    {
        ConfigReceiveMonolith config = (ConfigReceiveMonolith) configurations;
        queue = config.getQueue().toUpperCase();
        this.gatewayConfigDir = "/config/monolith/";
        genericObjectService = new GenericObjectService(config);
        try
        {
            String displayId = getDisplayId(config.getDisplayName());
            if (displayId.equals(""))
            {
                Log.log.error("DisplayName  is invalid and is required to decide what events attributes to be fetched from Monolith.");

                throw new Exception("Please specify valid Monolith DisplayName in blueprint properties.");

            }
            else
                MonolithGateway.displayId = displayId;

            String ackToolId = getToolId(MonolithGateway.ACK_TOOL_NAME);
            String unackToolId = getToolId(MonolithGateway.UNACK_TOOL_NAME);
            String userId = getUserId(config.getUsername());

            if (ackToolId.equals(""))
            {
                Log.log.debug("Event Tool Acknowledge is not available. Creating new tool to acknowledge event");
                MonolithGateway.ackToolId = createSQLQueryTool(MonolithGateway.TOOLTYPEID_SQL, MonolithGateway.ACK_EVENT_SQL, MonolithGateway.ACK_TOOL_NAME, MonolithGateway.JOURNALSTATUS_ENABLE, MonolithGateway.DEFAULT_ACKNOWLEDGE_TOOL_ICON, MonolithGateway.ACK_JOURNAL_TEXT);

            }
            else
            {
                MonolithGateway.ackToolId = ackToolId;
            }
            if (unackToolId.equals(""))
            {
                Log.log.debug("Event Tool Acknowledge is not available. Creating new tool to Unacknowledge event");
                MonolithGateway.unackToolId = createSQLQueryTool(MonolithGateway.TOOLTYPEID_SQL, MonolithGateway.UNACK_EVENT_SQL, MonolithGateway.UNACK_TOOL_NAME, MonolithGateway.JOURNALSTATUS_ENABLE, MonolithGateway.DEFAULT_UNACKNOWLEDGE_TOOL_ICON, MonolithGateway.UNACK_JOURNAL_TEXT);

            }
            else
            {
                MonolithGateway.unackToolId = unackToolId;
            }
            if (userId.equals(""))
            {
                Log.log.error("Couldn't get userId for user: " + config.getUsername());
            }
            else
            {
                MonolithGateway.userId = userId;
            }

        }

        catch (Exception e)
        {
            if (StringUtils.isEmpty(MonolithGateway.displayId))
            {
                Log.log.error("Please specify valid display name in blueprint prpoerties. This display should be created in Monolith to decide what alarm fields gateway should read.");                Log.log.error(e.getMessage(), e);
            }
            else if (StringUtils.isEmpty(MonolithGateway.ackToolId))
            {
                Log.log.error("Resolve failed to create QueryTool to acknowledge events while initialising gateway. This tool is required and gateway will not proceed to fetch events.");
                Log.log.error(e.getMessage(), e);
            }
            else if (StringUtils.isEmpty(MonolithGateway.unackToolId))
            {
                Log.log.error("Resolve failed to create QueryTool to UnAcknowledge events while initialising gateway. This tool is required and gateway will not proceed to fetch events.");
                Log.log.error(e.getMessage(), e);
            }
            else
            {
                Log.log.error(e.getMessage(), e);
            }

        }
        // Add customized code here to initilize the connection with 3rd party
        // system;
    }

    /**
     * Add customized code here to do when stop the gateway
     */
    @Override
    public void stop()
    {
        Log.log.warn("Stopping Monolith gateway");
        // Add customized code here to stop the connection with 3rd party
        // system;
        super.stop();
    }

    @Override
    public Filter getFilter(Map<String, Object> params)
    {

        return new MonolithFilter((String) params.get(BaseFilter.ID), (String) params.get(BaseFilter.ACTIVE),
       (String) params.get(BaseFilter.ORDER), (String) params.get(BaseFilter.INTERVAL),
       (String) params.get(BaseFilter.EVENT_EVENTID), (String) params.get(BaseFilter.RUNBOOK),
       (String) params.get(BaseFilter.SCRIPT), (String) params.get(MonolithFilter.QUERY),
       (String) params.get(MonolithFilter.FILTERNAME), (String) params.get(MonolithFilter.DEFAULTQUERY),
       (String) params.get(MonolithFilter.FIRST_REPORTED_TIME),(String) params.get(MonolithFilter.LAST_READ_ALARM_ID));

    }

    private String getFilterId(String filterName) throws Exception
    {
        return genericObjectService.getFilterId(filterName);
    }

    private String getDisplayId(String displayName) throws Exception
    {
        return genericObjectService.getDisplayId(displayName);
    }

    private String validateQuery(MonolithFilter monolithFilter, String filterId, String displayId)
    {

        String query = monolithFilter.getDefaultQuery();
        if (query.equalsIgnoreCase("Custom"))
        {
            query = monolithFilter.getQuery();

        }
        boolean hasVariable = VAR_REGEX_GATEWAY_VARIABLE.matcher(query).find();// monolithFilter.FIRST_OCCURRED_TIME);

        return genericObjectService.validateQuery(monolithFilter, filterId, displayId, hasVariable);
    }

    private String getToolId(String toolName) throws Exception
    {
        String toolId = genericObjectService.getToolId(toolName);

        return toolId;

    }

    public String getUserId(String userName) throws Exception
    {
        if (StringUtils.isEmpty(userName))
        {
            throw new Exception("Please specify valid Monolith User Name in blueprint properties.");
        }
        String userId = genericObjectService.getUserId(userName);

        return userId;
    }

    private void ackEvent(List<Map<String, String>> eventMapList) throws Exception
    {
        String events = "";
        String shards = "";
        String alarmId = "";
        String shardId = "";
        int i = 0;

        for (Map eventDataMap : eventMapList)
        {
            alarmId = "";
            shardId = "";
            if (eventDataMap.containsKey("AlarmId") && eventDataMap.containsKey("ShardID"))
            {
                alarmId = eventDataMap.get("AlarmId").toString();
                shardId = eventDataMap.get("ShardID").toString();
                if (StringUtils.isEmpty("shardId"))
                {
                    // Default shardId in monolith is 1
                    shardId = "1";
                }
                if (StringUtils.isNotEmpty(alarmId) && StringUtils.isNotEmpty(shardId))
                {
                    if (StringUtils.isEmpty(events) && StringUtils.isEmpty(shards))
                    {
                        events = alarmId;
                        shards = shardId;
                    }
                    else
                    {
                        events = events + "," + alarmId;
                        shards = shards + "," + shardId;
                    }

                }
            }

            i++;
        }

        genericObjectService.ackEvent(events, shards);

    }

    private void unAckEvent(List<Map<String, String>> eventMapList) throws Exception
    {
        String events = "";
        String shards = "";
        String alarmId = "";
        String shardId = "";
        int i = 0;
        for (Map eventDataMap : eventMapList)
        {
            alarmId = "";
            shardId = "";
            if (eventDataMap.containsKey("AlarmId") && eventDataMap.containsKey("ShardID"))
            {
                alarmId = eventDataMap.get("AlarmId").toString();
                shardId = eventDataMap.get("ShardID").toString();
                // MonolithAPI.addJournalEntry("MAN", alarmId, "Comment using
                // Resolve DB Gateway..");
                if (StringUtils.isEmpty("shardId"))
                {
                    // Default shardId in monolith is 1
                    shardId = "1";
                }
                if (StringUtils.isNotEmpty(alarmId) && StringUtils.isNotEmpty(shardId))
                {
                    if (StringUtils.isEmpty(events) && StringUtils.isEmpty(shards))
                    {
                        events = alarmId;
                        shards = shardId;
                    }
                    else
                    {
                        events = events + "," + alarmId;
                        shards = shards + "," + shardId;
                    }

                }
            }

            i++;
        }
        genericObjectService.unackEvent(events, shards);

    }

    public Map<String, Object> loadSystemProperties()
    {
        Map<String, Object> properties = new HashMap<String, Object>();
        properties.put("USERID", MonolithGateway.userId);
        return properties;
    }

    void ackEvent(String events, String shards) throws Exception
    {

        genericObjectService.ackEvent(events, shards);
    }

    void unackEvent(String events, String shards) throws Exception
    {
        genericObjectService.unackEvent(events, shards);
    }
    
    void updateEvent(String alarmId, String summary) throws Exception
    {
        genericObjectService.updateEvent(alarmId, summary);
    }

    void addJournalEntryUsingDBGateway(String poolName, String AlarmId, String journalComment) throws Exception
    {
        long timeStamp;
        long timeDiffInsecond = 1;

        long startTime = System.currentTimeMillis();
        ConfigReceiveMonolith config = (ConfigReceiveMonolith) configurations;
        // timeDiffInsecond=config.
        if (poolName == null || StringUtils.isEmpty(poolName))
        {

            // poolName = config.poolName;
        }
        timeStamp = startTime + (timeDiffInsecond * 1000);

        String sql = "insert into Events.Journal (AlarmId,UserId,Entry,TimeStamp) " + "Values (" + AlarmId + "," + MonolithGateway.userId + ",'" + journalComment + "'," + startTime + ")";

        genericObjectService.addJournalEntryUsingDBGateway(poolName, sql);
    }

    void addJournalEntry(String alarms, String journalComment) throws Exception
    {

        genericObjectService.addJournalEntry(alarms, journalComment);
    }

    private List<Map<String, String>> getEvents(String url) throws Exception
    {
        return genericObjectService.getObjects(url, null);

    }

    private String createSQLQueryTool(String toolTypeId, String toolSQL, String toolName, String journalStatus, String icon, String journalext) throws Exception
    {
        return genericObjectService.createSQLQueryTool(toolTypeId, toolSQL, toolName, journalStatus, icon, journalext);

        // TODO Auto-generated method stub
    }

}
