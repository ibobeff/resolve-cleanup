package com.resolve.rscontrol;

import com.resolve.persistence.model.MonolithFilter;

public class MMonolith extends MGateway {

    private static final String MODEL_NAME = MonolithFilter.class.getSimpleName();

    protected String getModelName() {
        return MODEL_NAME;
    }
}

