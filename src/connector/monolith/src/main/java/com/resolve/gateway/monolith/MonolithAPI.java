//todo: add time diff in properties file
//create tool to ack and unack
//add filtergroupId is read events call
//dbpoolname in blueprint and update addjournalentry() in dbgateway
//dbpoolname in prop
//timedif in addjournalentry timestamp
//chnage timezone name to monolithusertimezone
//tool name as config
//checkconnection code

package com.resolve.gateway.monolith;
import java.util.Map;
import com.resolve.gateway.AbstractGatewayAPI;

public class MonolithAPI extends AbstractGatewayAPI
{
    private static String USERID;
    
    public final static String ALARM_ZONEID="ZoneID";
    public final static String ALARM_NODE="Node";
    public final static String ALARM_SUBMETHOD="SubMethod";
    public final static String ALARM_ALARMTYPE="AlarmType";
    public final static String ALARM_COUNT="Count";
    public final static String ALARM_ESCID="EscId";
    public final static String ALARM_DEPARTMENT="Department";
    public final static String ALARM_FORMAT="Format";
    public final static String ALARM_OWNERID="OwnerId";
    public final static String ALARM_SUBALARM_GROUP="SubAlarmGroup";
    public final static String ALARM_ALARMID="AlarmId";
    public final static String ALARM_ALARM_KEY="AlarmKey";
    public final static String ALARM_LAST_REPORTED="LastReported";
    public final static String ALARM_SRC_PORT="SrcPort";
    public final static String ALARM_ACK="Ack";
    public final static String ALARM_SUBDEVICE_TYPE="SubDeviceType";
    public final static String ALARM_SEVERITY="Severity";
    public final static String ALARM_METHOD="Method";
    public final static String ALARM_DESTPORT="DstPort";
    public final static String ALARM_FIRST_REPORTED="FirstReported";
    public final static String ALARM_SRCIP="SrcIP";
    public final static String ALARM_TICKET_FLAG="TicketFlag";
    public final static String ALARM_SHARDID="ShardID";
    public final static String ALARM_CUSTOM4="Custom4";
    public final static String ALARM_DEVICE_TYPE="DeviceType";
    public final static String ALARM_CUSTOM3="Custom3";
    public final static String ALARM_CUSTOM2="Custom2";
    public final static String ALARM_SCORE="Score";
    public final static String ALARM_TICKETID="TicketId";
    public final static String ALARM_CUSTOM1="Custom1";
    public final static String ALARM_ALARM_GROUP="AlarmGroup";
    public final static String ALARM_CUSTOM5="Custom5";
    public final static String ALARM_DSTIP="DstIP";
    public final static String ALARM_SUMMARY="Summary";
    public final static String ALARM_IPADDRESS="IPAddress";
    public final static String ALARM_ESCFLAG="EscFlag";
    public final static String ALARM_LOCATION="Location";
    
   
    private static MonolithGateway instance = MonolithGateway.getInstance();
    
    
    static
    {
        Map<String, Object> properties = instance.loadSystemProperties();
        USERID = (String) properties.get("USERID");

    }
    /**
     * This API call will update ACK status of Alarm as Acknowledged(1).
     * @param events - Single AlarmId or a comma seperated list of AlarmIds
     * @param shards - ShardID of Alarm (default: 1). This is also returned as one of Alarm attribute by Monolith Gateway and can be found in PARAM map.
     * @throws Exception
     */
    public static void ackEvent(String events,String shards) throws Exception{
        
        instance.ackEvent(events,shards);
    }
    
    /**
     * This API call will update ACK status of Alarm as UnAcknowledged(0).
     * @param events - Single AlarmId or a comma seperated list of AlarmIds
     * @param shards - ShardID of Alarm (default: 1). This is also returned as one of Alarm attribute by Monolith Gateway and can be found in PARAM map.
     * @throws Exception
     */
    public static void unackEvent(String events,String shards) throws Exception {
        instance.unackEvent(events,shards);
    }
    
   /* public static void addJournalEntryUsingDBGateway(String poolName,String alarmId,String journalComment) throws Exception
    {
       
      instance.addJournalEntryUsingDBGateway(poolName,alarmId,journalComment);
    }*/
   
    /**This API call is used to add Journal comment in Alarm Journal.
     * @param alarms: A single AlarmId or Comma separated list of AlarmId. 
     * Please Note: Monolith API makes entry in the Journal table even if AlarmId that doesn't exists is passed here. 
     * @param journalComment
     * @throws Exception
     */
    public static void addJournalEntry(String alarms,String journalComment) throws Exception
    {
      instance.addJournalEntry(alarms,journalComment);
    }
    
    /**
     * This API call is used to Update existing events
     * @param alarmId (Required)
     * @param summary (Optional)
     * @throws Exception
     */
    public static void updateEvent(String alarmId, String summary) throws Exception
    {
      instance.updateEvent(alarmId, summary); 
    }
    
 /*    *//**
     * Returns {@link Set} of supported object types.
     * <p>
     * Default no object types are supported returning {@link Collections.EMPTY_SET}.
     * Derived external system specific gateway API must override this method to return
     * {@link Set<String>} of supported object types.
     * 
     * @return {@link Set} of supported object types
     *//*
    public static Set<String> getObjectTypes()
    {
        return Collections.emptySet();
    }
    

    *//**
     * Creates instance of specified object type in external system.
     * <p>
     * Default attribute name-value pair returned is  
     * {@value com.resolve.gateway.GatewayConstants#METHOD_NOT_SUPPORTED_KEY}-true
     * indicating external system specific gateway does not support creating instances of
     * specified object type.
     * <p>
     * If external system specific gateway does support creation of object then it must return
     * {@link Map} of attribute name-value pairs.
     * 
     * @param  objType   Type of object to create in external system
     * @param  objParams {@link Map} of object parameters
     * @param  userName  User name
     * @param  password  Password
     * @return           Attribute id-value {@link Map} of created object in external system
     * @throws Exception If any error occurs in creating object of specified
     *                   type in external system
     *//*
    public static Map<String, String> createObject(String objType, Map<String, String> objParams, String userName, String password) throws Exception
    {
        return null;
    }
    
    *//**
     * Reads attributes of specified object in external system.
     * <p>
     * Default attribute id-value pair returned is  
     * {@value com.resolve.gateway.GatewayConstants#METHOD_NOT_SUPPORTED_KEY}-true
     * indicating external system specific gateway does not support reading atributes of
     * object.
     * <p>
     * If external system specific gateway does support reading attributes of object then 
     * it must return {@link Map} of attribute name-value pairs. 
     *
     * @param  objType    Object type
     * @param  objId      Id of the object to read attributes of from external system
     * @param  attribs    {@link List} of attributes of object to read
     * @param  userName   User name
     * @param  password   Password
     * @return            {@link Map} of object attribute id-value pairs
     * @throws Exception  If any error occurs in reading attributes of the object in 
     *                    external system
     *//*
    public static Map<String, String> readObject(String objType, String objId, List<String> attribs, String userName, String password) throws Exception
    {
        return null;
    }
    
    *//**
     * Updates attributes of specified object in external system.
     * <p>
     * Default updated attribute name-value pair returned is  
     * {@value com.resolve.gateway.GatewayConstants#METHOD_NOT_SUPPORTED_KEY}-true
     * indicating external system specific gateway does not support updating attributes of
     * object.
     * <p>
     * If external system specific gateway does support updating attributes of object then it 
     * must return updated {@link Map} of attribute name-value pairs.
     * 
     * @param  objType   Object type
     * @param  objId     Id of the object to update attributes of in external system
     * @param  updParams Key-value {@link Map} of object attributes to update
     * @param  userName  User name
     * @param  password  Password
     * @return           Key-value {@link Map} of the updated object attributes
     * @throws Exception If any error occurs in updating attributes of the object in 
     *                   external system
     *//*
    public static Map<String, String> updateObject(String objType, String objId, Map<String, String> updParams, String userName, String password) throws Exception
    {
        return null;
    }
    
    *//**
     * Deletes specified object from external system.
     * <p>
     * Default deleted attribute name-value pair returned is  
     * {@value com.resolve.gateway.GatewayConstants#METHOD_NOT_SUPPORTED_KEY}-true
     * indicating external system specific gateway does not support object deletion.
     * <p>
     * If external system specific gateway does support object deletion then it 
     * must return {@link Map} of object deletion operation reult.
     * 
     * @param  objType   Object type
     * @param  objId     Id of the object to delete from external system
     * @param  userName  User name
     * @param  password  Password
     * @return           Key-value {@link Map} result of object delete operation
     * @throws Exception If any error occurs in deleting object from external system
     *//*
    public static Map<String, String> deleteObject(String objType, String objId, String userName, String password) throws Exception
    {
        return null;
    }
    
    *//**
     * Get list of objects from external system matching specified filter condition.
     * <p>
     * Default implementation returns {@link List} containing {@link Map} with 
     * {@value com.resolve.gateway.GatewayConstants#METHOD_NOT_SUPPORTED_KEY}-true indicating
     * external system specific gateway does not support getting objects matching filter condition.
     * <p>
     * If external system specific gateway does support getting objects matching filter condition then it 
     * must return {@link List} of {@link Map} of matching objects attribute id-value pairs.
     * 
     * @param  objType  Object type
     * @param  filter   External system gateway specific filer conditions
     * @param  userName User name
     * @param  password Password
     * @return          {@link List} of {@link Map}s of object attribute id-value pairs matching
     *                  filter condition
     * @throws Exception If any error occurs in getting object from external system
     *//*
    
    public static List<Map<String, String>> getObjects(String objType, BaseFilter filter, String userName, String password) throws Exception
    {
        List<Map<String, String>> objs = new ArrayList<Map<String, String>>();
        
        objs.add(null);
        
        return objs;
    }*/
}


