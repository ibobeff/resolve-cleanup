package com.resolve.persistence.model;

import java.util.List;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Lob;
import javax.persistence.Table;
import javax.persistence.UniqueConstraint;
import com.resolve.services.interfaces.VO;
import com.resolve.services.hibernate.vo.MonolithFilterVO;

@Entity
@Table(name = "monolith_filter", uniqueConstraints = { @UniqueConstraint(columnNames = { "u_name", "u_queue" }) })
public class MonolithFilter extends GatewayFilter<MonolithFilterVO> {

    private static final long serialVersionUID = 1L;

    public MonolithFilter() {
    }

    public MonolithFilter(MonolithFilterVO vo) {
        applyVOToModel(vo);
    }

    private String UQuery;

    private String UFilterName;

    private String UDefaultQuery;

    @Column(name = "u_query", length = 400)
    public String getUQuery() {
        return this.UQuery;
    }

    public void setUQuery(String uQuery) {
        this.UQuery = uQuery;
    }

    @Column(name = "u_filtername", length = 400)
    public String getUFilterName() {
        return this.UFilterName;
    }

    public void setUFilterName(String uFilterName) {
        this.UFilterName = uFilterName;
    }

    @Column(name = "u_defaultquery", length = 400)
    public String getUDefaultQuery() {
        return this.UDefaultQuery;
    }

    public void setUDefaultQuery(String uDefaultQuery) {
        this.UDefaultQuery = uDefaultQuery;
    }

    public MonolithFilterVO doGetVO() {
        MonolithFilterVO vo = new MonolithFilterVO();
        super.doGetBaseVO(vo);
        vo.setUQuery(getUQuery());
        vo.setUFilterName(getUFilterName());
        vo.setUDefaultQuery(getUDefaultQuery());
        return vo;
    }

    @Override
    public void applyVOToModel(MonolithFilterVO vo) {
        if (vo == null) return;
        super.applyVOToModel(vo);
        if (!VO.STRING_DEFAULT.equals(vo.getUQuery())) this.setUQuery(vo.getUQuery()); else ;
        if (!VO.STRING_DEFAULT.equals(vo.getUFilterName())) this.setUFilterName(vo.getUFilterName()); else ;
        if (!VO.STRING_DEFAULT.equals(vo.getUDefaultQuery())) this.setUDefaultQuery(vo.getUDefaultQuery()); else ;
    }

    @Override
    public List<String> ugetCdataProperty() {
        List<String> list = super.ugetCdataProperty();
        list.add("UQuery");
        list.add("UFilterName");
        list.add("UDefaultQuery");
        return list;
    }
}

