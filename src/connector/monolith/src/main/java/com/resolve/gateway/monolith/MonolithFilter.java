package com.resolve.gateway.monolith;

import com.resolve.gateway.BaseFilter;
import com.resolve.gateway.RetainValue;

public class MonolithFilter extends BaseFilter {

    public static final String QUERY = "QUERY";

    public static final String FILTERNAME = "FILTERNAME";
    public static final String FIRST_REPORTED_TIME = "FIRST_REPORTED";
    public static final String LAST_READ_ALARM_ID = "LAST_READ_ALARM_ID";
    //public final String START_INDEX = "START_INDEX";
    public static final String DEFAULTQUERY = "DEFAULTQUERY";
    private String defaultQuery;
    private  String firstReportedTime;
 //   private String  startIndex;
    private String lastReadAlarmId;
    
    @RetainValue
    public String getLastReadAlarmId()
    {
        return lastReadAlarmId;
    }

    public void setLastReadAlarmId(String lastReadAlarmId)
    {
        this.lastReadAlarmId = lastReadAlarmId;
    }

    @RetainValue
    public String getFirstReportedTime()
    {
        return this.firstReportedTime;
    }

    public void setFirstReportedTime(String firstOccurredTime)
    {
        this.firstReportedTime = firstOccurredTime;
    }

   /* @RetainValue
    public String getStartIndex()
    {
        return this.startIndex;
    }

    public void setStartIndex(String startIndex)
    {
        this.startIndex = startIndex;
    }*/

    private String query;

    private String filterName;

    public MonolithFilter(String id, String active, String order, String interval, String eventEventId, String runbook, String script, String query, String filterName, String defaultQuery,String firstReportedtime,String lastReadAlarmId) {
        super(id, active, order, interval, eventEventId, runbook, script);
        this.query = query;
        this.filterName = filterName;
        this.defaultQuery = defaultQuery;
        setLastReadAlarmId(lastReadAlarmId);
        setFirstReportedTime(firstReportedtime);
    }

    public String getQuery() {
        return this.query;
    }

    public void setQuery(String query) {
        this.query = query;
    }

    public String getFilterName() {
        return this.filterName;
    }

    public void setFilterName(String filterName) {
        this.filterName = filterName;
    }

    public String getDefaultQuery() {
        return this.defaultQuery;
    }

    public void setDefaultQuery(String defaultQuery) {
        this.defaultQuery = defaultQuery;
    }
}


