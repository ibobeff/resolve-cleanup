package com.resolve.services.hibernate.vo;

import com.resolve.util.MappingAnnotation;

public class MonolithFilterVO extends GatewayFilterVO {

    private static final long serialVersionUID = 1L;

    public MonolithFilterVO() {
    }

    private String UQuery;

    private String UFilterName;

    private String UDefaultQuery;

    @MappingAnnotation(columnName = "QUERY")
    public String getUQuery() {
        return this.UQuery;
    }

    public void setUQuery(String uQuery) {
        this.UQuery = uQuery;
    }

    @MappingAnnotation(columnName = "FILTERNAME")
    public String getUFilterName() {
        return this.UFilterName;
    }

    public void setUFilterName(String uFilterName) {
        this.UFilterName = uFilterName;
    }

    @MappingAnnotation(columnName = "DEFAULTQUERY")
    public String getUDefaultQuery() {
        return this.UDefaultQuery;
    }

    public void setUDefaultQuery(String uDefaultQuery) {
        this.UDefaultQuery = uDefaultQuery;
    }
    
    @Override
    public int hashCode()
    {
        final int prime = 31;
        int result = super.hashCode();
        result = prime * result + ((UQuery == null) ? 0 : UQuery.hashCode());
        result = prime * result + ((UFilterName == null) ? 0 : UFilterName.hashCode());
        result = prime * result + ((UDefaultQuery == null) ? 0 : UDefaultQuery.hashCode());
        return result;
    }
}

