package com.resolve.gateway.pull.zenoss;

import java.math.BigDecimal;
import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.Map;

import org.apache.http.client.ClientProtocolException;

import com.resolve.gateway.AbstractGatewayAPI;
import com.resolve.gateway.resolvegateway.pull.PullGatewayFilter;
import com.resolve.util.Log;
import com.resolve.util.StringUtils;
import com.resolve.util.restclient.RestCallException;
import com.resolve.util.restclient.RestCaller;

import net.sf.json.JSONArray;
import net.sf.json.JSONObject;

public class ZenossGatewayAPI extends AbstractGatewayAPI
{
    private static String HOSTNAME = "";
    private static String PORT = "";
    private static String URI = "";
    private static String user = "";
    private static String pass = "";
    private static ZenossGateway gateway = ZenossGateway.getInstance();
    public static ZenossGatewayAPI instance = null;
    private static Map<String, Object> systemProperties = null;
    private static String baseZenossUrl = null;
    private String filterId = "";
    // Sample code for composing the base URL for REST or SOAP web service call to connect to the third-party server
    private ZenossGatewayAPI() {
        initialize();
    }
    
    //This block is required if the API calls are static
    static {
        try {
            //Load the default values as defined by the 
            systemProperties = gateway.loadSystemProperties("Zenoss");
            HOSTNAME = (String)systemProperties.get("HOST");
            PORT =((Integer)systemProperties.get("PORT")).toString();
            String sslType = (String)systemProperties.get("SSLTYPE");
            //Get username and password
            user = (String)systemProperties.get("USER");
            pass = (String)systemProperties.get("PASS");
            //Check for SSL configuration
            boolean isSsl = ((Boolean)systemProperties.get("SSL")).booleanValue();
            boolean selfSigned = ( sslType== null || sslType.length() == 0 || sslType.equalsIgnoreCase("self-signed")) ? true : false;
            if(isSsl) {
                baseZenossUrl="https://";
                 }
            else
            {    baseZenossUrl ="http://";}
            baseZenossUrl =baseZenossUrl +HOSTNAME+":"+PORT;
        } catch(Exception e) {
          Log.log.error(e.getMessage(), e);
        }
    }
    
    //Below is necessary if the API calls are instance methods
    private void initialize() {
        try {
            //Load the default values as defined by the 
            systemProperties = gateway.loadSystemProperties("Zenoss");
            HOSTNAME = (String)systemProperties.get("HOST");
            PORT =((Integer)systemProperties.get("PORT")).toString();
            String sslType = (String)systemProperties.get("SSLTYPE");
            //Get username and password
            user = (String)systemProperties.get("USER");
            pass = (String)systemProperties.get("PASS");
            //Check for SSL configuration
            boolean isSsl = ((Boolean)systemProperties.get("SSL")).booleanValue();
            boolean selfSigned = ( sslType== null || sslType.length() == 0 || sslType.equalsIgnoreCase("self-signed")) ? true : false;
            if(isSsl) {
                baseZenossUrl="https://";
                 }
            else
            {    baseZenossUrl ="http://";}
            baseZenossUrl =baseZenossUrl +HOSTNAME+":"+PORT;
        } catch(Exception e) {
          Log.log.error(e.getMessage(), e);
        }
    }
    // Public method to get an instance from this Singleton class
    // Do not modify
    public static ZenossGatewayAPI getInstance() {
        if(instance == null)
            instance = new ZenossGatewayAPI();
        return instance;
    }
    
    void updateDeployedFilters(PullGatewayFilter filter) {
    	gateway.updateOrderedFilter(filter);
    }
    
    /**
     * This method is used to query events based on eventState,firstSeen Time,lastSeentTime and devide Id. 
     * This API call queries all type of severity events in addition to given criteria.
     * MaxNoOfEvents
     * @param eventData
     * Should Include:::: eventState: Arraylist of eventStates.0-New,1-Acknowledged,2-Suppressed,3-Closed,4-Cleared,5-Aged
     *        firstTime: Timesstamp in Users timezone in yyyy-mm-dd hr:mm:ss format
     *        lastTime: Timesstamp in Users timezone in yyyy-mm-dd hr:mm:ss format
 * @param maxQueuseSizeResultsToFetch: Maximum events to be fetched. This is the Gateway Queue size which is 1000 by default
     * @return
     * @throws Exception
     */
    List<Map<String, String>> queryEvents(JSONObject eventData,
    					int maxQueuseSizeResultsToFetch) throws Exception
                                                                    
    {
    //Initialize the REST API caller
    RestCaller restCallerOld = new RestCaller(baseZenossUrl, user, pass); 
    restCallerOld.setUrlSuffix("zport/dmd/Events/evconsole_router");
    int start = 0,page = 0, limit=200;
    boolean hasMoreData  = true;
    String response = "";
    List<Map<String, String>> pulledEvents = new ArrayList<Map<String, String>>();
    Collection<Integer> statusCodeColl = new ArrayList<>();
    statusCodeColl.add(200);
    //Looping through the calls using the max page size same as the queue size
    while(hasMoreData)
    {
//		eventData.accumulate("start", start);
//		eventData.accumulate("page", page);
        try
        {
            Log.log.info("Making Post call to Zenoss API URL:: =====>" + restCallerOld.getBaseUrl());
            Log.log.info("Calling to Zenoss with Query data:: =====>" + StringUtils.jsonObjectToString(eventData));
            try {
            	//The replace string is a really crazy solution we had to make do with for now 
            	//Simply because Zenoss expects us to send \"<data\" for able to search components
                    response = restCallerOld.postMethod(null, null, StringUtils.jsonObjectToString(eventData), statusCodeColl);
            }catch(ClientProtocolException clex) {
                //usually when protocol is wrong(http/https) then this occurs
                Log.log.error("\n++++++++++++++++++++++++++++++++++++++++++++++++++++++++");
                Log.log.error("\nThe Client threw the protocol error for URL: -->" + restCallerOld.getBaseUrl());
                Log.log.error("\n+++++Please check the protocol for communication HTTP or HTTPS +++++++++++++++");
                throw clex;
            }
            catch(RestCallException ex) {
                //if username password is wrong then this occurs
                if(ex.statusCode > 300 && ex.statusCode < 390) {
                    Log.log.error("+++++++++++++++++++++++++++++++++++++++++++++++++++++++");
                    Log.log.error("There was a problem with the user access please check the user id and password for the"
                                    + " request: -->" + ex.getMessage() );
                    Log.log.error("Reason : " + ex.responseEntity);
                }
                else {
                    Log.log.error("Failed to Pull events from Zenoss. Reason: "+ex.reasonPhrase);
                }
                throw ex;
            }
            //If the status is not 200, thorw error 
                    if (restCallerOld.getStatusCode() != 200)
                {
                        Log.log.info("==============================================================");
                        Log.log.error("Query Returned no records.. Inputs Params are ===>" + StringUtils.jsonObjectToString(eventData));
                        Log.log.info("==============================================================");
                        Log.log.error(restCallerOld.getReasonPhrase());
                        throw new Exception("Error occured while querying events in Zenoss. Response Received: "+response);
                    }else {
                        JSONObject event = JSONObject.fromObject(response);
                        if (event.containsKey("result"))
                        {
                            event = JSONObject.fromObject(event.get("result"));
                            if (event.containsKey("success") && event.containsKey("totalCount"))
                            {

                                if ((Boolean) event.get("success") == true && (Integer) event.get("totalCount") > 0)
                                {
                                    if (event.containsKey("events"))
                                    {
                                        List<Map<String, String>> eventDataReturned =  parseResponse(JSONArray.fromObject(event.get("events")),maxQueuseSizeResultsToFetch,pulledEvents.size());
                                        if(eventDataReturned.size() !=0 )
                                        {
                                            pulledEvents.addAll(eventDataReturned);
                                            if(pulledEvents.size() >= maxQueuseSizeResultsToFetch || eventDataReturned.size() < 200) {
                                                break; // not more than quesize
                                            }
                                        }
                                        else {
                                            break;
                                        }
                                    }
                                }
                                else  if ((Boolean) event.get("success") == true && (Integer) event.get("totalCount") == 0)
                                {
                                    Log.log.debug("Filter query retrieved 0 events: "+response);
                                    break;
                                }else {
                                	Log.log.debug("Error while querying events in Zenoss. Response Received: " + response);
                                    break;
                                }
                            }else {
                            	Log.log.debug("Error while querying events in Zenoss. Response Received: " + response);
                                break;
                            }
                        }
                        else
                        {
                            Log.log.error("Error while querying events in Zenoss. Response Received: "+response);
                            break;
                        } 
                    }
        }
        catch (Exception e)
        {
            Log.log.error("Exception thrown  while querying events in Zenoss. For the request -->" + restCallerOld.getBaseUrl());
            Log.log.error("Error occured when sending the inputs params are ===>" + StringUtils.jsonObjectToString(eventData));
            Log.log.error("++++++++++++++++++++++ The error stack trace is printed below+++++++++++++++++");
            Log.log.error(e.getMessage()+"\n"+e.getStackTrace());
            break;
        }
        start += limit;
        page += 1;
    }
    Log.log.debug("Total number of events retrieved at this filter interval: "+pulledEvents.size());
    return pulledEvents;
}
    
     /**
      * 
      * @param event
      * @param limit
      * @param totalFetchedUntillNow
      * @return
      */
    private List<Map<String, String>> parseResponse(JSONArray event,int limit,int totalFetchedUntillNow){
        List<Map<String, String>> eventList = new ArrayList<>();
       // JSONObject eventData = new JSONObject();
        DecimalFormat format = new DecimalFormat();
       for(int i=0;i<event.size();i++ ) {
           JSONObject eventData =  JSONObject.fromObject(event.get(i)) ;
           Map<String, String> eventKeyValue = StringUtils.jsonObjectToMap(eventData);
           if(eventKeyValue.containsKey("firstTime")) {
               
        	   String firstTime = format.format(new BigDecimal(eventKeyValue.get("firstTime"))).replaceAll(",","");
               eventKeyValue.replace("firstTime",  firstTime);
           }
           if(eventKeyValue.containsKey("lastTime")) {
        	   
        	   String lastTime = format.format(new BigDecimal(eventKeyValue.get("lastTime"))).replaceAll(",","");
               eventKeyValue.replace("lastTime",  lastTime);
           }
           if(totalFetchedUntillNow == limit ) {
               break;
           }
           else {
           eventList.add(eventKeyValue);
           totalFetchedUntillNow+=1;
           }
           
        }
        
        
        return eventList;
        
    }
    /**
     * Acknowledge an event.
     * @param evid: evid of the event
     * @return status of acknowledgement(true/false)
     * @throws Exception
     */
    public static boolean acknowledge(String evid)throws Exception
    {
        if(StringUtils.isBlank(evid) ) {
           Log.log.debug("++++++++++++++++++++++++++++++++++++++++++");
           Log.log.debug("No Event Id was sent");
           Log.log.debug("++++++++++++++++++++++++++++++++++++++++++");
            throw new Exception("evid of an event is required.");
        }

        //Make the REST call 
        Log.log.debug("acknowldge called for Zenoss event/alert id: " + evid);
        RestCaller restCallerOld = new RestCaller(baseZenossUrl, user, pass); 
        restCallerOld.setUrlSuffix("zport/dmd/Events/evconsole_router");
        boolean isUpdated = false;
        String method = "acknowledge";
        String action = "EventsRouter";
        Collection<Integer> statusCode = new ArrayList<>();
        statusCode.add(200);
        JSONArray evids = new JSONArray();
        evids.add(evid);
        JSONObject eventData = new JSONObject();
        eventData.accumulate("evids", evids);
        evids.clear();
        evids.add(eventData);
        JSONObject jsonBody = new JSONObject();
        jsonBody.accumulate("action",action);
        jsonBody.accumulate("method",method);
        jsonBody.accumulate("data",evids);
        jsonBody.accumulate("tid",1);
        try
        {
            Log.log.debug("acknowldge Rest call being made for Zenoss event/alert id: " + evid);
            String response = restCallerOld.postMethod(null, null, StringUtils.jsonObjectToString(jsonBody), statusCode);
            if (restCallerOld.getStatusCode() != 200)
            {
                Log.log.error("Acknowledgement failed.. Inputs Params are===>" + StringUtils.jsonObjectToString(jsonBody));
            }
            JSONObject ackResponse = JSONObject.fromObject(response);
            if (ackResponse.containsKey("result"))
            {
                JSONObject data = JSONObject.fromObject(ackResponse).getJSONObject("result");
                if (data.containsKey("success"))
                {
                    Log.log.debug("acknowldgement successful for alert id:  " + evid);
                    isUpdated = data.getBoolean("success");
                }
            }
        }catch(Exception e) {
            Log.log.error(e.getMessage());
            throw e;
            
        }
       return isUpdated;
    }
    
    /**
     *  UnAcknowledge an event
     * @param evid: evid of the event
     * @return status of unAcknowledgement(true/false)
     * @throws Exception
     */
    public static boolean unacknowledge(String evid) throws Exception
    {
        
        if(StringUtils.isBlank(evid) ) {
            throw new Exception("evid of an event is required.");
        }
        RestCaller restCallerOld = new RestCaller(baseZenossUrl, user, pass);
        restCallerOld.setUrlSuffix("zport/dmd/Events/evconsole_router");
        boolean isUpdated = false;
        String method = "reopen";
        String action = "EventsRouter";
        
        Collection<Integer> statusCode = new ArrayList<>();
        statusCode.add(200);
        
        JSONArray evids = new JSONArray();
        evids.add(evid);
        
        JSONObject eventData = new JSONObject();
        eventData.accumulate("evids", evids);
        
        evids.clear();
        evids.add(eventData);
        
        JSONObject jsonBody = new JSONObject();
        jsonBody.accumulate("action",action);
        jsonBody.accumulate("method",method);
        jsonBody.accumulate("data",evids);
        jsonBody.accumulate("tid",1);
        
        try
        {

            String response = restCallerOld.postMethod(null, null, StringUtils.jsonObjectToString(jsonBody), statusCode);
            if (restCallerOld.getStatusCode() != 200)
            {
                Log.log.error("Failed to UnAcknowledge Event. Inputs Params are===>" + StringUtils.jsonObjectToString(jsonBody));
            }

            JSONObject unAckResponse = JSONObject.fromObject(response);
            if (unAckResponse.containsKey("result"))
            {
                JSONObject data = JSONObject.fromObject(unAckResponse).getJSONObject("result");
                if (data.containsKey("success"))
                {
                    isUpdated = data.getBoolean("success");

                }
            }
        }catch(Exception e) {
            Log.log.error(e.getMessage());
            throw e;
            
        }
       return isUpdated;
    }
    /**
     * <br> API call to add Resolve Worksheet ID and ServiceNow ID to the Event details </br>
     * <br> Call will be triggered when doing guided resolution from Zenoss, the resolution </br>
     * <br> would trigger an automation in Resolve. The Action task within the automation inside resolve </br>
     * <br> with groovy script will call this API to update Zenoss with the Worksheet ID and ServiceNowId </br>
     * @param evid
     * @param worksheetId
     * @param serviceNowId
     * @return
     * @throws Exception
     */
    public static boolean updateEventDetails(String evid, 
    					String worksheetId, String serviceNowId)throws Exception
    {
    	 boolean updated = false;
    	 if(StringUtils.isBlank(evid)) {
             throw new Exception("Update Event Details evid is required.");
         }
    	 //Create the rest caller instance to make a call
    	 RestCaller restCallerOld = new RestCaller(baseZenossUrl, user, pass);
         restCallerOld.setUrlSuffix("zport/dmd/Events/evconsole_router");
         String method = "updateDetails";
         String action = "EventsRouter";
         
         JSONObject eventData = new JSONObject();
         if(evid != null && !evid.isEmpty()) { 
			eventData.accumulate("evid", evid);
		 }
         if(worksheetId != null) {
        	 eventData.accumulate("WorksheetId", worksheetId);
		 }
         if(serviceNowId != null) {
        	 eventData.accumulate("ServiceNowId", serviceNowId);
		 }
         JSONArray evids = new JSONArray();
         evids.add(eventData);
         
         return callZenossAPI(restCallerOld, action, method, evids);
    }
    
    /**
     * <br> API call to add Resolve Worksheet ID and ServiceNow ID to the Event details </br>
     * <br> Call will be triggered when doing guided resolution from Zenoss, the resolution </br>
     * <br> would trigger an automation in Resolve. The Action task within the automation inside resolve </br>
     * <br> with groovy script will call this API to update Zenoss with the Worksheet ID and ServiceNowId </br>
     * <br> Make sure you pass "WorksheetId" and "ServiceNowId" as the Keys for the corresponding values </br>  
     * <br> ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ </br> 
     * <br> Using this method any key value pair can be added to a Zenoss event </br>
     * @param evid
     * @param worksheetId
     * @param serviceNowId
     * @return
     * @throws Exception
     */
    public static boolean updateEventDetails(String evid, 
    					Map<String, String> eventNameVals)throws Exception
    {
    	 boolean updated = false;
    	 if(StringUtils.isBlank(evid)) {
             throw new Exception("Update Event Details evid is required.");
         }
    	 //Create the rest caller instance to make a call
    	 RestCaller restCallerOld = new RestCaller(baseZenossUrl, user, pass);
         restCallerOld.setUrlSuffix("zport/dmd/Events/evconsole_router");
         String method = "updateDetails";
         String action = "EventsRouter";
         
         JSONObject eventData = new JSONObject();;
         eventData.accumulate("evid", evid);
         //By adding Map support the API will be able to add any field value to a Zenoss Event
         if(eventNameVals != null) {
        	 for(String names: eventNameVals.keySet()) {
        		 eventData.accumulate(names, eventNameVals.get(names));
        	 }
         }else {
        	 Log.log.debug("ERROR: The Event details are missing from the request");
        	 return false;
         }
         
         JSONArray evids = new JSONArray();
         if(eventData != null) {
        	 evids.add(eventData);
		 }
         else {
        	 Log.log.debug("ERROR: The Event details are missing from the request");
        	 return false;
         }
         return callZenossAPI(restCallerOld, action, method, evids);
    }
    
    /**
     * Common Function to make API calls to Zenoss
     * @param restCallerOld
     * @param action
     * @param method
     * @param evid
     * @return
     * @throws Exception
     */
    private static boolean callZenossAPI(RestCaller restCallerOld, String action,
    									String method, JSONArray evids)throws Exception {
    	boolean isUpdated = false;
    	 //Initial status code set to 200
        Collection<Integer> statusCode = new ArrayList<>();
        statusCode.add(200);
        //Create the json payload
        JSONObject jsonBody = new JSONObject();
        jsonBody.accumulate("action",action);
        jsonBody.accumulate("method",method);
        jsonBody.accumulate("data",evids);
        jsonBody.accumulate("tid",1);
        try
        {
            String response = restCallerOld.postMethod(null, null, StringUtils.jsonObjectToString(
            						jsonBody), statusCode);
            if (restCallerOld.getStatusCode() != 200)
            {
                Log.log.debug("Failed to Write Log to an Event. Input Params are===>" + 
                					StringUtils.jsonObjectToString(jsonBody));
                isUpdated = false;
            }else {

		        JSONObject unAckResponse = JSONObject.fromObject(response);
		        if (unAckResponse.containsKey("result"))
		        {
		            JSONObject data = JSONObject.fromObject(unAckResponse).getJSONObject("result");
		            if (data.containsKey("success"))
		            {
		                isUpdated = data.getBoolean("success");
		                if(isUpdated)
		                	Log.log.debug("sccessfully " + method);
		                else
		                	Log.log.debug("Update unsuccessful " + method);
		
		            }else {
		            	Log.log.debug("Update unsuccessful " + method);
		            	isUpdated = false;
		            }
		        }
            }
        }catch(Exception e) {
            Log.log.error(e.getMessage());
            throw e;
            
        }
        return isUpdated;
    }
    
    /**
     * Write Log comment to event1
     * @param evid: evid of the event
     * @param message: Log Message to be added to the event.
     * @return staus of writeLog(true/false)
     * @throws Exception
     */
    public static boolean writeLog(String evid,String message) throws Exception
    {
        
        if(StringUtils.isBlank(evid) || StringUtils.isBlank(message) ) {
            throw new Exception("evid/message is required.");
        }
        RestCaller restCallerOld = new RestCaller(baseZenossUrl, user, pass);
        restCallerOld.setUrlSuffix("zport/dmd/Events/evconsole_router");
        boolean isUpdated = false;
        String method = "write_log";
        String action = "EventsRouter";
        
        Collection<Integer> statusCode = new ArrayList<>();
        statusCode.add(200);
        
        
        JSONObject eventData = new JSONObject();
        eventData.accumulate("evid", evid);
        eventData.accumulate("message", message);
        
        JSONArray evids = new JSONArray();
        evids.add(eventData);
        
               
        JSONObject jsonBody = new JSONObject();
        jsonBody.accumulate("action",action);
        jsonBody.accumulate("method",method);
        jsonBody.accumulate("data",evids);
        jsonBody.accumulate("tid",1);
        
        try
        {

            String response = restCallerOld.postMethod(null, null, StringUtils.jsonObjectToString(jsonBody), statusCode);
            if (restCallerOld.getStatusCode() != 200)
            {
                Log.log.error("Failed to Write Log to an Event. Input Params are===>" + StringUtils.jsonObjectToString(jsonBody));
            }

            JSONObject unAckResponse = JSONObject.fromObject(response);
            if (unAckResponse.containsKey("result"))
            {
                JSONObject data = JSONObject.fromObject(unAckResponse).getJSONObject("result");
                if (data.containsKey("success"))
                {
                    isUpdated = data.getBoolean("success");
                    Log.log.debug("Added log sccessfully");

                }
            }
        }catch(Exception e) {
            Log.log.error(e.getMessage());
            throw e;
            
        }
       return isUpdated;
    }

    /**
     * Close Event.
     * @param evid: evid of the event
     * @return status of close event(true/false)
     * @throws Exception
     */
    public static boolean closeEvent(String evid) throws Exception
    {
        
        if(StringUtils.isBlank(evid) ) {
            throw new Exception("evid of an event is required.");
        }
        RestCaller restCallerOld = new RestCaller(baseZenossUrl, user, pass);
        restCallerOld.setUrlSuffix("zport/dmd/Events/evconsole_router");
        boolean isUpdated = false;
        String method = "close";
        String action = "EventsRouter";
        
        Collection<Integer> statusCode = new ArrayList<>();
        statusCode.add(200);
        
        JSONArray evids = new JSONArray();
        evids.add(evid);
        
        JSONObject eventData = new JSONObject();
        eventData.accumulate("evids", evids);
        
        evids.clear();
        evids.add(eventData);
        
        JSONObject jsonBody = new JSONObject();
        jsonBody.accumulate("action",action);
        jsonBody.accumulate("method",method);
        jsonBody.accumulate("data",evids);
        jsonBody.accumulate("tid",1);
        
        try
        {

            String response = restCallerOld.postMethod(null, null, StringUtils.jsonObjectToString(jsonBody), statusCode);
            if (restCallerOld.getStatusCode() != 200)
            {
                Log.log.error("Failed to close an Event.. Inputs Params are===>" + StringUtils.jsonObjectToString(jsonBody));
            }

            JSONObject ackResponse = JSONObject.fromObject(response);
            if (ackResponse.containsKey("result"))
            {
                JSONObject data = JSONObject.fromObject(ackResponse).getJSONObject("result");
                if (data.containsKey("success"))
                {
                    isUpdated = data.getBoolean("success");

                }
            }
        }catch(Exception e) {
            Log.log.error(e.getMessage());
            throw e;
            
        }
       return isUpdated;
    }
    
    static String getDevicePriorities() throws Exception
    {
     
        RestCaller restCallerOld = new RestCaller(baseZenossUrl, user, pass);
        
        restCallerOld.setUrlSuffix("zport/dmd/Events/device_router");
        String method = "getPriorities";
        String action = "DeviceRouter";
        
        Collection<Integer> statusCode = new ArrayList<>();
        statusCode.add(200);
        
        JSONObject jsonBody = new JSONObject();
        jsonBody.accumulate("action",action);
        jsonBody.accumulate("method",method);
        jsonBody.accumulate("tid",1);
        
        
            String response = restCallerOld.postMethod(null, null, StringUtils.jsonObjectToString(jsonBody), statusCode);
            if (restCallerOld.getStatusCode() != 200)
            {
                throw new Exception();
            }

            JSONObject ackResponse = JSONObject.fromObject(response);
            if (ackResponse.containsKey("result"))
            {
                JSONObject data = JSONObject.fromObject(ackResponse).getJSONObject("result");
                if (!data.containsKey("success"))
                {
                    throw new Exception();
                }
            }
            return response;
    }
    
    
    /**
     * Load parameters and initialize the Sample gateway API based on the filter specific information.
     * @param filterId: the name of the filter specified in the UI. This filter must be deployed.
     * @throws Exception
     */
    // Optional
    public static ZenossGatewayAPI getInstance(String filterId) throws Exception {

        if(instance == null)
            instance = new ZenossGatewayAPI(filterId);

        return instance;
    }

    // Singleton constructor to take the filter name to load the filter fields
    // if some of the filter attributes are not available, the sample code shows how to fall back to the
    // default configuration on the gateway level in the blueprint.properties
    // Optional
    private ZenossGatewayAPI(String filterId) throws Exception {

        initialize();
    }

    // Add more APIs for this specific gateway using object instance methods to be called by getInstance() or getInstance(filterId) methods
    // instead of publish static methods that will only work with the default constrcutor


} // class ZenossGatewayAPI