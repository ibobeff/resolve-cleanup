package com.resolve.gateway.pull.zenoss;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Iterator;
import java.util.Map;

import com.resolve.rsbase.MainBase;
import com.resolve.gateway.resolvegateway.pull.ConfigReceivePullGateway;
import com.resolve.gateway.resolvegateway.pull.PullGateway;
import com.resolve.gateway.resolvegateway.pull.PullGatewayFilter;
import com.resolve.gateway.resolvegateway.pull.PullGatewayProperties;
import com.resolve.util.Log;
import com.resolve.util.StringUtils;

public class ZenossGateway extends PullGateway
{
    
    public static final int FIRST_SEEN_DATE_FORMAT_WRONG = -1;
    public static final int LAST_SEEN_DATE_FORMAT_WRONG = -2;
    public static final int FIRST_SEEN_DATE_MACRO_WRONG = -3;
    public static final int LAST_SEEN_DATE_MACRO_WRONG = -4;
    
    public static final String FIRST_SEEN_DATE_FORMAT_WRONG_MESSAGE = "First Seen Date is formatted incorrectly - format should be yyyy-mm-dd HH:mm:ss";
    public static final String LAST_SEEN_DATE_FORMAT_WRONG_MESSAGE = "Last Seen Date is formatted incorrectly - format should be yyyy-mm-dd HH:mm:ss";
    public static final String LAST_SEEN_DATE_MACRO_WRONG_MESSAGE = "Last Seen Date macro supported is ${LAST_SEEN}";
    public static final String FIRST_SEEN_DATE_MACRO_WRONG_MESSAGE = "First Seen Date macro supported is ${FIRST_SEEN}";
    
    
    private static volatile ZenossGateway instance = null;
    // Time out for ESB messages
    static final long SEND_TIMEOUT = 10 * 60 * 1000; // milliseconds
    // Specific condition to meet for querying/retriving events from third-party servers, i.e. Sample server, such as Moogsoft server
    // This query is used to pull back events/alerts/incidents from the remote server
    public static final String QUERY = "QUERY";
    // Optional last value fields that will be persisted into the database for each polling interval
    public static final String LAST_VALUE = "LAST_VALUE";
    public static final String LAST_TIMESTAMP = "LAST_TIMESTAMP";
    // Optional flexible attributes for the specific gateway that to be persisted into the database
    public static final String LAST_VALUE_FIELD = "LAST_VALUE_FIELD";
    // Singleton constructor to take the blueprint configuration and initialize the deployed filters for this specific gateway
    // Do not modify
    private ZenossGateway(ConfigReceivePullGateway config) {
        super(config);
        String gatewayName = this.getClass().getSimpleName();
        int index = gatewayName.indexOf("Gateway");
        if(index != -1) {
            name = gatewayName.substring(0, index);
            if(StringUtils.isBlank(name)) {
                Log.log.error("Not appropriate gateway name: " + gatewayName);
                return;
            }
            try {
                StringBuilder sb = new StringBuilder();
                PullGatewayProperties properties = ConfigReceivePullGateway.getGatewayProperties(name);
                sb.append(properties.getPackageName()).append(".").append(gatewayName).append("Impl");
                Class<?> implClass = Class.forName(sb.toString());
                impl = (ZenossGatewayImpl)implClass.newInstance();
            } catch(Exception e) {
                Log.log.error(e.getMessage(), e);
            }
        }
        else {
            Log.log.error("Not appropriate gateway name: " + gatewayName);
            return;
        }
        Map<String, PullGatewayFilter> pullFilters = getPullFilters();
        for(Iterator<String> iterator=pullFilters.keySet().iterator(); iterator.hasNext();) {
            String filterName = iterator.next();
            PullGatewayFilter filter = pullFilters.get(filterName);
            if(name.indexOf(((PullGatewayFilter)filter).getGatewayName()) != -1) {
                if(filters.get(filterName) == null) {
                    filters.put(filterName, pullFilters.get(filterName));
                    if(filter.isDeployed()) {
                        orderedFilters.add(filter);
                        orderedFiltersMapById.put(filter.getId(), filter);
                    }
                }
            }
        }
    }
    // Public method to get an instance from this Singleton class
    // Do not modify
    public static ZenossGateway getInstance(ConfigReceivePullGateway config) {
        if (instance == null) {
            instance = new ZenossGateway(config);
        }
        return instance;
    }
    // This method is called after the instance is instantiated.
    // Do not modify
    public static ZenossGateway getInstance() {
        if (instance == null) {
            throw new RuntimeException("Zenoss Pull Gateway is not initialized correctly..");
        } else {
            return instance;
        }
    }
    // Initialize the gateway with the default HTTP server instance and start to listen on the default port
    // Do not remove the existing code, but you can add additional logic for initialization if necessary
    @Override
    protected void initialize() {
        queue = config.getQueue().toUpperCase();
        try {
            Log.log.info("Initializing Zenoss Listener ...");
            this.gatewayConfigDir = "/config/" + queue.toLowerCase() + "/";
            PullGatewayProperties properties = ConfigReceivePullGateway.getGatewayProperties(name);
            ConfigReceivePullGateway.getGateways().put(name, this);
            Log.log.debug("+++++++++++++++++++++++++++++++++++++++++++++++++++++");
            Log.log.debug("+++++++ZENOSS Gateway is up and running now ++++++++");
            Log.log.debug("+++++++++++++++++++++++++++++++++++++++++++++++++++++");
           Map<String,Object> addProps = properties.getAdditionalProperties();
           
           ZenossGateway gateway = ZenossGateway.getInstance();
           Map<String, Object> systemProperties = gateway.loadSystemProperties("Zenoss");
           
           String HOSTNAME = systemProperties.get("HOST") == null? "" :(String)systemProperties.get("HOST");
           String PORT = systemProperties.get("PORT") == null? "": ((Integer)systemProperties.get("PORT")).toString();
           String USER = systemProperties.get("USER") == null? "" :(String)systemProperties.get("USER");
           String PASS = systemProperties.get("PASS") == null? "" :(String)systemProperties.get("PASS");
           
           if(StringUtils.isBlank((String)addProps.get("USERTIMEZONE"))) {
               Log.alert("Instance " + MainBase.main.configId.getGuid() + " breached " + getLicenseCode() + "-" + getQueueName() + "-" + getInstanceType() + "-BlueprintProperties Required Fields Value Missing","Please specify UserTimeZone in blueprint properties.");
           }
           if(StringUtils.isBlank(HOSTNAME)){
               Log.alert("Instance " + MainBase.main.configId.getGuid() + " breached " + getLicenseCode() + "-" + getQueueName() + "-" + getInstanceType() + "-BlueprintProperties Required Fields Value Missing","Please specify Host Address in blueprint properties.");
           }

           if(StringUtils.isBlank(PORT)){
               Log.alert("Instance " + MainBase.main.configId.getGuid() + " breached " + getLicenseCode() + "-" + getQueueName() + "-" + getInstanceType() + "-BlueprintProperties Required Fields Value Missing","Please specify Http/Https Zenoss App Port in blueprint properties.");
           }
           
           if(StringUtils.isBlank(USER)){
               Log.alert("Instance " + MainBase.main.configId.getGuid() + " breached " + getLicenseCode() + "-" + getQueueName() + "-" + getInstanceType() + "-BlueprintProperties Required Fields Value Missing","Please specify Username in blueprint properties.");
           }
           if(StringUtils.isBlank(PASS)){
               Log.alert("Instance " + MainBase.main.configId.getGuid() + " breached " + getLicenseCode() + "-" + getQueueName() + "-" + getInstanceType() + "-BlueprintProperties Required Fields Value Missing","Please specify Password in blueprint properties.");
           }
           
            // Add additional custom logic here, e.g., start the Sample server etc.
        } catch (Exception e) {
            Log.log.error("Failed to initialize Zenoss Gateway: " + e.getMessage(), e);
        }
    }
    
    @Override
    public void reinitialize() {
    	super.reinitialize();
        initialize();
    }
    // The gateway will start to run by calling the super class common logic to start the polling for each deployed filter
    @Override
    public void start() {
        Log.log.debug("Starting Zenoss Pull Gateway");
        super.start();
    }
 // This method will be called when the gateway is gracefully shutdown
    @Override
    public void stop() {
        Log.log.warn("Stopping Zenoss Pull gateway");
        // Add code for shutdown the Sample Server
        super.stop();
    }
    // This method will be called when primary server is up and secondary server must be deactivated
    // All the resouces being started must be stopped.
    @Override
    public void deactivate() {
        super.deactivate();
    }
    // This method is used by the system; Method commented because the pullgateway class has this method with the right
    //messagehandler name
    // Do not modify
//    @Override
//    protected String getMessageHandlerName() {
//        return this.getClass().getName();
//    }
    // If the common logic in the super class does not support the use case for this specific gateway,
    // you may create override methods to replace the logic in the super class
    
    // This method will be called when a filter is deployed to validate the semantics of the field only
    // The syntax of the filter fields have been validated when the filter is defined and saved from UI
    // including whether the filter field is required or not per gateway definition in blueprint.properties
    @Override
    public int validateFilterFields(Map<String, Object> params) {

        int code = 0;
        if(params.get("ufirstSeenDateTime") != null && ((String)params.get("ufirstSeenDateTime")).contains("$") && (!((String)params.get("ufirstSeenDateTime")).equals("${FIRST_SEEN}"))) {
            
            code = FIRST_SEEN_DATE_MACRO_WRONG;
        }
        else if(params.get("ulastSeenDateTime") != null && ((String)params.get("ulastSeenDateTime")).contains("$") && (!((String)params.get("ulastSeenDateTime")).equals("${LAST_SEEN}"))) {
            
            code = LAST_SEEN_DATE_MACRO_WRONG;
        }
        else if(StringUtils.isNotBlank(params.get("ufirstSeenDateTime") == null ? "":(String)params.get("ufirstSeenDateTime")) 
        		&& (!(params.get("ufirstSeenDateTime") == null ? "":(String)params.get("ufirstSeenDateTime")).equals("${FIRST_SEEN}"))) 
        {
            String formatString = "yyyy-MM-dd hh:mm:ss";
           
                try {
                    Date date = new SimpleDateFormat(formatString).parse((String)params.get("ufirstSeenDateTime")); //ufirstSeenDateTime
                                      
                } catch (ParseException e) {
                    code = FIRST_SEEN_DATE_FORMAT_WRONG;
                }
            
            
        }
        
        else if(StringUtils.isNotBlank(params.get("ulastSeenDateTime") == null ? "":(String)params.get("ulastSeenDateTime")) && 
        		(!(params.get("ulastSeenDateTime") == null ? "":(String)params.get("ulastSeenDateTime")).equals("${LAST_SEEN}")))  //ulastSeenDateTime
        {
            String formatString = "yyyy-MM-dd hh:mm:ss";
           
                try {
                    Date date = new SimpleDateFormat(formatString).parse(((String)params.get("ulastSeenDateTime")));
                                      
                } catch (ParseException e) {
                    code = LAST_SEEN_DATE_FORMAT_WRONG;
                }
            
            
        }
     
        // Add custom logic here

        return code;
    }
    @Override
    public String getLicenseCode()
    {
        return "ZENOSS";
    }
    /**
     * Required for resolution routing to work. The queue name is sent to the rscontrol by calling this mathod
     */
    @Override
    public String getQueueName()
    {
        return queue + getOrgSuffix();
    }
    // The method returns the error string that is associated with the error code defined
    // under the custom logic being implemented
    @Override
    public String getValidationError(int errorCode) {

        String error = null;
        switch(errorCode)
        {
                        
            case FIRST_SEEN_DATE_FORMAT_WRONG:
                error =  FIRST_SEEN_DATE_FORMAT_WRONG_MESSAGE;
                break;
                
            case LAST_SEEN_DATE_FORMAT_WRONG:
                error =  LAST_SEEN_DATE_FORMAT_WRONG_MESSAGE;
                break;
                
            case LAST_SEEN_DATE_MACRO_WRONG:
                error =  LAST_SEEN_DATE_MACRO_WRONG_MESSAGE;
                break;
                
            case FIRST_SEEN_DATE_MACRO_WRONG:
                error =  FIRST_SEEN_DATE_MACRO_WRONG_MESSAGE;
                break;
                
            default:
                error =  "Error code not found.";
                break;
        }
        // Add custom logic here

        return error;
    }
    
    public boolean checkGatewayConnection() throws Exception
    {
        
        try
        {
            String response = getDevicePriorities(); 
            
            if (response == null || StringUtils.isBlank(response))
            {
                throw new Exception ("Failed to get response from Zenoss. Connection to Zenoss server possibly broken.");
            }
            else
            {
                Log.log.debug("Instance " + MainBase.main.configId.getGuid() + " : " + getLicenseCode() + "-" + 
                              getQueueName() + "-" + getInstanceType() + " connected to configured Zenoss server.");
            }
        }
        catch(Exception e)
        {
            Log.alert("Instance " + MainBase.main.configId.getGuid() + " breached " + getLicenseCode() + "-" + getQueueName() + "-" + getInstanceType() + "-ConnectionSelfCheck", 
                      "Instance " + MainBase.main.configId.getGuid() + "Breached " + getLicenseCode() + "-" + getQueueName() + "-" + getInstanceType() + "-ConnectionSelfCheck" + 
                      " due to " + e.getMessage());
        }
        
        return true;
    }
    
    private String getDevicePriorities() throws Exception 
    {
        // TODO Auto-generated method stub}
           return ZenossGatewayAPI.getDevicePriorities();
       
    }

} // class ZenossGateway
