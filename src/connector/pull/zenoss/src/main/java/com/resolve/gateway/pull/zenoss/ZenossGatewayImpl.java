package com.resolve.gateway.pull.zenoss;

import java.math.BigDecimal;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.StringTokenizer;
import java.util.TimeZone;
import java.util.regex.Pattern;

import com.resolve.gateway.resolvegateway.ResolveGatewayUtil;
import com.resolve.gateway.resolvegateway.pull.ConfigReceivePullGateway;
import com.resolve.gateway.resolvegateway.pull.PullGatewayFilter;
import com.resolve.gateway.resolvegateway.pull.PullGatewayInterface;
import com.resolve.gateway.resolvegateway.pull.PullGatewayProperties;
import com.resolve.util.Constants;
import com.resolve.util.Log;
import com.resolve.util.StringUtils;

import net.sf.json.JSONArray;
import net.sf.json.JSONObject;

public class ZenossGatewayImpl implements PullGatewayInterface
{
    // Define a pattern to match for building custom query
    // Optional
    private Pattern pattern;
    private ZenossGatewayAPI zenossApi= null;
    private String currentUtcTime;
    
    public ZenossGatewayImpl() {
        //Initialize the date values
        currentUtcTime = getCurrentDateTime("");
    }
    /**
     * <br> Get the Current Date and Time based on User provided Time Zone</br>
     * <br> The Timezone could be "UTC", "PST" etc </br>
     * @param userTimeZone
     * @return
     */
    private String getCurrentDateTime(String userTimeZone) {
        Date curentUtcTime = Date.from(java.time.ZonedDateTime.now().toInstant());
        SimpleDateFormat sdf = new SimpleDateFormat( "yyyy-MM-dd HH:mm:ss.SSS" );
        if(StringUtils.isEmpty(userTimeZone)){
            sdf.setTimeZone( TimeZone.getTimeZone("UTC"));
        }else {
            sdf.setTimeZone(TimeZone.getTimeZone(userTimeZone));
        }
        return sdf.format(curentUtcTime);
    }
    /**
     * <br> This method adds the amount of Milli Second to the time provided in </br>
     * <br> the first param provided in the call </br>
     * @param userTime
     * @param timeMillsec
     * @return
     */
    private String addTimeToCurrentTime(String userTime, int timeMillsec) {
    	SimpleDateFormat sdf = new SimpleDateFormat( "yyyy-MM-dd HH:mm:ss.SSS" );
    	Date dateTime = new Date();
    	try {
            dateTime = sdf.parse(userTime) ;
        }catch(Exception ex) {
            Log.log.debug("Couldn't convert the filtered entered time to real time in Method getDateTimeInProvidedZone");
            Log.log.debug("In class ZenossGatewayImpl. Please refere the stack trace below");
            ex.printStackTrace();
        }
    	Calendar cal = Calendar.getInstance();
    	cal.setTime(dateTime);
    	cal.add(Calendar.MILLISECOND, timeMillsec);
//        return sdf.format(cal.getTime());
    	return Long.valueOf(cal.getTime().getTime()).toString();
    }
    /**
     * 
     * @param userTimeZone
     * @param userProvidedTime
     * @return
     */
    private String getDateTimeInProvidedZone(String userTimeZone,  String userProvidedTime) {
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss.SSS");
        if(userProvidedTime.indexOf(".") == -1) {
        	userProvidedTime = userProvidedTime + ".000";
        }
        Date dateTime = new Date();
        try {
            dateTime = sdf.parse(userProvidedTime) ;
        }catch(Exception ex) {
            Log.log.debug("Couldn't convert the filtered entered time to real time in Method getDateTimeInProvidedZone");
            Log.log.debug("In class ZenossGatewayImpl. Please refere the stack trace below");
            ex.printStackTrace();
        }
        //Get the user entered time zone
        TimeZone zenossTzone = TimeZone.getTimeZone(userTimeZone);
        TimeZone utcTzone = TimeZone.getTimeZone("UTC");
      //This condition assumes that user has a different 
        if(!StringUtils.isEmpty(userTimeZone)){
            long fromOffset = zenossTzone.getOffset(dateTime.getTime());//get offset
            long toOffset = utcTzone.getOffset(dateTime.getTime());
            long convertedTime = dateTime.getTime() - (fromOffset - toOffset);
            Date convDate = new Date(convertedTime);
            return sdf.format(convDate);
        }
            //If the blue print doesn't have a entry assume user entered UTC date format
            return sdf.format(dateTime);
    }
    
  
    // There are two typical ways to pull events from the third-party server:
    // by custom query or by time bucket defined in the filter depending on the methods
    // provided by the third-party server
    // Included is the template code for implementation using custom query and maintain
    // the last values, e.g., last id, last timestamp, or any last field values
    // The custom query may contain macros in the regex pattern of "\\$\\{[_A-Z]*}", it
    // can be built on the fly by calling buildCustomQuery() method in the super gateway class.
    // This method must be implemented, but the logic for query and update last value is optional.
   public List<Map<String, String>> retrieveData(PullGatewayFilter filter) throws Exception {
        List<Map<String, String>> result = new ArrayList<Map<String, String>>();

        try
        {
            // Get the Filter Attributes
            Map<String, Object> filterAttrs = filter.getAttributes();
            
            ZenossGateway zenossInstance=  ZenossGateway.getInstance();
            // Add the filter attributes to the Array
            List<String> stateList = statusCodeArray(filterAttrs.get("ueventStatus") == null ? "" : (String) filterAttrs.get("ueventStatus"));
            // get first seen and last seen from the filter attributes
            PullGatewayProperties props = ConfigReceivePullGateway.getGatewayProperties(filter.getGatewayName());
            // Get the API instance
            zenossApi = ZenossGatewayAPI.getInstance();
            Map<String, Object> addProps = props.getAdditionalProperties();

            
            ZenossGateway gateway = ZenossGateway.getInstance();
            Map<String, Object> systemProperties = gateway.loadSystemProperties("Zenoss");
            
            String HOSTNAME = systemProperties.get("HOST") == null ? "" : (String)systemProperties.get("HOST");
            String PORT =(systemProperties.get("PORT") == null ? "0" : (Integer)systemProperties.get("PORT")).toString();
            String USER = systemProperties.get("USER") == null ? "" : (String)systemProperties.get("USER");
            String PASS = systemProperties.get("PASS") == null ? "" :(String)systemProperties.get("PASS");
            
            if(StringUtils.isBlank((String)addProps.get("USERTIMEZONE"))) {
                Log.alert("BlueprintProperties Required Fields Value Missing","Please specify UserTimeZone in blueprint properties.");
                Log.log.error("Please specify UserTimeZone in blueprint properties.");
            }
            if(StringUtils.isBlank(HOSTNAME)){
                Log.alert("BlueprintProperties Required Fields Value Missing","Please specify Host Address in blueprint properties.");
                Log.log.error("BlueprintProperties Required Fields Value Missing, Please specify Host Address in blueprint properties.");
            }

            if(StringUtils.isBlank(PORT)){
                Log.alert("BlueprintProperties Required Fields Value Missing","Please specify Http/Https Zenoss App Port in blueprint properties.");
                Log.log.error("BlueprintProperties Required Fields Value Missing, Please specify Http/Https Zenoss App Port in blueprint properties.");
            }
            
            if(StringUtils.isBlank(USER)){
                Log.alert("BlueprintProperties Required Fields Value Missing","Please specify Username in blueprint properties.");
                Log.log.error("BlueprintProperties Required Fields Value Missing, Please specify Username in blueprint properties.");
            }
            if(StringUtils.isBlank(PASS)){
                Log.alert("BlueprintProperties Required Fields Value Missing","Please specify Password in blueprint properties.");
                Log.log.error("BlueprintProperties Required Fields Value Missing, Please specify Password in blueprint properties.");
            }
                       
            
 
            result = zenossApi.queryEvents(initFilterAttribs(filter,addProps),zenossInstance.getPrimaryDataQueueExecutorQueueSize()); 

            
        } catch(Exception e) {
            Log.log.error(e.getMessage(), e);
        }
        return result;
    }
    
   private JSONObject initFilterAttribs(PullGatewayFilter filter, Map<String, Object> addProps) {
	   
	   JSONObject filterAttrData = new JSONObject();
	   Map<String, Object> filterAttrs = filter.getAttributes();
	   String userFirstSeenTime = "";
       String userLastSeenTime = "";
       String firstSeenPersistedDateonFilter = "";
       String firstSeenDateonFilterToUTC = "";
       String devices = "";
       String severity = "";
       JSONArray severityArray = new JSONArray();
       /******************** Filter attribute logic starts here *****************/
       List<String> stateList = new ArrayList<String>();
       Map<String, Object> additional = new HashMap<String, Object>();
       firstSeenPersistedDateonFilter = filter.getTimeRange();
       String userTimeZone = addProps.get("USERTIMEZONE") == null? "" :(String) addProps.get("USERTIMEZONE");
       //Loop through the sdk attributes set in the blueprint
       for(Iterator<String> it=filterAttrs.keySet().iterator(); it.hasNext();) {
    	   String key = it.next();
           String value = filterAttrs.get(key) == null ? "" : (String)filterAttrs.get(key);
           String attribute = key.substring(1);
           switch(attribute) {
           		case "firstSeenDateTime":
           			userFirstSeenTime = value;
           			if (firstSeenPersistedDateonFilter == null || StringUtils.isBlank(firstSeenPersistedDateonFilter))
	           	       {
	           	           if (value.trim().equalsIgnoreCase("${FIRST_SEEN}"))
	           	           {
//	           	        	   String userctz = getDateTimeInProvidedZone(userTimeZone, currentUtcTime);
	           	               firstSeenDateonFilterToUTC = convertDataToLongFormat(getDateTimeLong(currentUtcTime));
	           	           }
	           	           else
	           	           {
	           	               if (StringUtils.isNotBlank(value) && StringUtils.isNotEmpty(value))
	           	               {
	           	            	   String userZoneTime = getDateTimeInProvidedZone(userTimeZone, value);
	           	                   firstSeenDateonFilterToUTC = getDateTimeLong(userZoneTime);
	           	               }
	           	               else if (StringUtils.isBlank(value))
	           	               {
	           	                   Log.log.debug("No First Seen Time is provided on the filter.");
	           	               }
	           	           }
	           	       }
	       	       else {
	       	    	    
	       	           // use the UTC date of the last event fetched in the previous filter interval. As we want to fetch events created after that time. This is the first seen time of the event
//	       	    	firstSeenDateonFilterToUTC = firstSeenPersistedDateonFilter;
	       	      //The Last saved time needs to be changed and increment by 1 millisecond to avoid unack events being picked up again
	       	    	   if(firstSeenPersistedDateonFilter.indexOf(".") != -1) {
		       	           long number1 = Long.parseLong(firstSeenPersistedDateonFilter.substring(0, firstSeenPersistedDateonFilter.indexOf(".")));
		       	           long number2 = Long.parseLong(firstSeenPersistedDateonFilter.substring(firstSeenPersistedDateonFilter.indexOf(".") + 1));
		       	           number2 = number2 + 1;
		       	           firstSeenDateonFilterToUTC = Long.valueOf(number1).toString()  + Long.valueOf(number2).toString();
	       	    	   }else {
		       	    		firstSeenDateonFilterToUTC = firstSeenPersistedDateonFilter;
	       	    	   }
	       	       }
           		   break;
           		case "lastSeenDateTime":
           			if (value.trim().equalsIgnoreCase("${LAST_SEEN}"))
	           	       {
           				value = currentUtcTime;
	           	       }
	           	       if (StringUtils.isNotBlank(value) && StringUtils.isNotEmpty(value))
	           	       {
	           	    	value = getDateTimeInProvidedZone(userTimeZone, value);
	           	       }
	           	       break;
           		case "deviceIp":
           			devices = value;
           			break;
           		case "eventSeverity":
           			severity = value;
           			int severityCode = severityToCode(severity);
           			//Passing the code to get the Array
           	        severityArray = initializeSeverity(new Integer(severityCode));
           	        break;
           		case "eventStatus":
           			stateList = statusCodeArray(value);
           			break;
//           		case "component":
//           			if(StringUtils.isNotEmpty(value)) {
//           				value = "\\\"" + value + "\\\"";
//           				additional.put(attribute, value);
//           			}
//           			break;
           		default:
           			if(StringUtils.isNotEmpty(value))
           				additional.put(attribute, value);
                    break;           			
           }
       }

    // Add the filter attributes to the Array     
       filterAttrData = populateEventData(200,firstSeenDateonFilterToUTC, userLastSeenTime,
    		   devices, severityArray,getEventArr(stateList), additional);
	   return filterAttrData;
   }
   /**
    * 
    * @param userProvidedTime
    * @return
    */
   private String getDateTimeLong(String userProvidedTime) {
       SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss.SSS");
       sdf.setTimeZone(TimeZone.getTimeZone("UTC"));
       Date dateTime = new Date();
       long conDate = 0;
       try {
           dateTime = sdf.parse(userProvidedTime) ;
       }catch(Exception ex) {
           Log.log.debug("Bad date time format expecting date in yyyy-MM-dd HH:mm:ss.SSS format");
           ex.printStackTrace();
       }
       return Long.toString(dateTime.getTime());
   }
   /**
    * 
    * @param dateToCon
    * @return
    */
   private String convertDataToLongFormat(String dateToCon) {
	     String dateInLongFormat = "";
	     if(dateToCon.indexOf(".") != -1) {
	    	 long number1 = Long.parseLong(dateToCon.substring(0, dateToCon.indexOf(".")));
	    	 long number2 = Long.parseLong(dateToCon.substring(dateToCon.indexOf(".") + 1));
	    	 number2 = number2 + 1;
	         long number3 = number1*1000 + number2;
	         dateInLongFormat = Long.valueOf(number3).toString();
	     }
	     else {
	    	 long number4 = Long.parseLong(dateToCon)/1000;
	    	 long number5 = Long.parseLong(dateToCon)%1000;
	    	 number5 = number5 + 1;
	    	 dateInLongFormat = Long.valueOf(number4).toString() + Long.valueOf(number5).toString();
//	    	 dateInLongFormat = dateToCon;
	     }
         return dateInLongFormat;
   }
   
   /**
    * Method to create the event json
    * @param limit
    * @param start
    * @param page
    * @param firstTime
    * @param lastTime
    * @param device
    * @param severityArray
    * @param eventStateArr
    * @return
    */
   private JSONObject populateEventData(int limit, String firstTime,
            String lastTime, String device, JSONArray severityArray
            ,JSONArray eventStateArr, Map<String, Object> addData) {

		JSONObject eventData = new JSONObject();
		int start = 0,page = 0;
		String method = "query", action = "EventsRouter",sortOn = "firstTime" ;
		eventData.accumulate("limit", limit);
		eventData.accumulate("sort", sortOn);
		eventData.accumulate("dir", "ASC");
		eventData.accumulate("detailFormat", true);
		eventData.accumulate("start", start);
		eventData.accumulate("page", page);
		JSONObject params = initializeParams(firstTime, lastTime, device, severityArray, eventStateArr);
		for(Iterator<String> it=addData.keySet().iterator(); it.hasNext();) {
			String key = it.next();
			params.accumulate(key, addData.get(key));
		}
		eventData.accumulate("params", params);
		JSONArray data = new JSONArray();
		data.add(eventData);
		eventData.clear();
		eventData.accumulate("method", method);
		eventData.accumulate("action", action);
		eventData.accumulate("data", data);
		eventData.accumulate("tid", 1);
		
		return eventData;
   }
   
   /**
    * 
    * @param firstTime
    * @param lastTime
    * @param device
    * @param severityArray
    * @param eventStateArr
    * @return
    */
     private JSONObject initializeParams(String firstTime, String lastTime, String device,
                     JSONArray severityArray,  JSONArray eventStateArr) {
         
         JSONObject params = new JSONObject();
         params.accumulate("eventState", eventStateArr);
         params.accumulate("severity", severityArray);
         params.accumulate("firstTime", firstTime);
         params.accumulate("lastTime", lastTime);
         if(StringUtils.isNotEmpty(device)) {
             params.accumulate("device", device + "*");
         }
         
         return params;
     }
     /**
      * 
      * @param eventState
      * @return
      */
     private JSONArray getEventArr(List<String> eventState) {
	     JSONArray eventStateArr = new JSONArray();
	     int statusCode = 0;
	     for (String state : eventState)
	     {
	         statusCode = stateToStatusCodeMap(state);
	         if(statusCode > 0) {
	             eventStateArr.add(statusCode);
	         }else {
	             eventStateArr.add(0);
	                 //Log error and move on
	         }
	     }
	     return eventStateArr;
     }
     /**
      * Convert the Severity code to the Severity array, The array is used to make Zenoss 
      * API call
      * @param severity
      * @return
      */
     private int severityToCode(String severity) {
         if(severity==null) {
             return -1;
         }
         switch (severity)
         {
             case "All":
                 return -1;
             case "Critical":
                 return 5;
             case "Error":
                 return 4;
             case "Warning":
                 return 3;
             case "Info":
                 return 2;
             case "Debug":
                 return 1;
             case "Clear":
                 return 0;
             default:
                 return -1;
         }
     }
     /**
      *  
      * @param state
      * @return
      */
      private int stateToStatusCodeMap(String state) {
          if(state==null) {
              return 0;
          }
          switch (state)
          {
              case "New":
                  return 0;
              case "Acknowledged":
                  return 1;
              case "Suppressed":
                  return 2;
              case "Closed":
                  return 3;
              case "Cleared":
                  return 4;
              case "Aged":
                  return 5;
              default:
                  return 0;
          }
      }
     /**
      * <p> Initializes the severity array </p>
      * @return
      */
     private JSONArray initializeSeverity(int sevCount) {
         JSONArray severityArray = new JSONArray();
         if(sevCount == -1) {
             sevCount = 5;
             while(sevCount >= 0) {
                 severityArray.add(sevCount);
                 sevCount-- ;
             }
         }else {
             severityArray.add(sevCount);
         }
         return severityArray;
     }
    //Utility function to add comma separated values to the Array
    private List <String> statusCodeArray(String commaSepStatus){
        List<String> statusLst = new ArrayList<String>();
        if(commaSepStatus != null && !commaSepStatus.isEmpty()) {
            StringTokenizer strToken = new StringTokenizer(commaSepStatus, ",");
            while(strToken.hasMoreTokens()) {
                statusLst.add(strToken.nextToken());
            }
        }
        return statusLst;
    }
    // Zenoss code for persist the last value in database
    // Optional
    public void updateLastValues(PullGatewayFilter filter, List<Map<String, String>> results) throws Exception
    {
        int size = results.size();
        String firstSeenTimeToPersist = "";
        PullGatewayProperties props = ConfigReceivePullGateway.getGatewayProperties(filter.getGatewayName());

        Map<String, Object> addProps = props.getAdditionalProperties();
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss.SSS");
        if (size == 0) return;

        // Response sends back epoch time. Which should be converted to UTC to
        // persist in the database and converted in the usertimezone to print in the debug log
        Map<String, String> last = results.get(size - 1);
        //The time needs to be carefully persisted because Zenoss can retuen .830 or .83 or just .8 it should always
        //be saved as 3 digit after decimal
        firstSeenTimeToPersist = last.get("firstTime");
        if(firstSeenTimeToPersist.indexOf(".") != -1) {
	           long number1 = Long.parseLong(firstSeenTimeToPersist.substring(0, firstSeenTimeToPersist.indexOf(".")));
	           long number2 = Long.parseLong(firstSeenTimeToPersist.substring(firstSeenTimeToPersist.indexOf(".") + 1));
//	           number2 = number2 + 1;
	           int offsetLen = Long.valueOf(number2).toString().length();
	           long number3 = number1*1000 + (number2 * (long)(Math.pow(10, 3 - offsetLen)) + 1);
	           firstSeenTimeToPersist = Long.valueOf(number3).toString();
        }else {
        	long firstSeenLong = Long.parseLong(firstSeenTimeToPersist)*1000;
        	firstSeenTimeToPersist = Long.valueOf(firstSeenLong).toString();
        }
        //persist the epoch time. This is for the debug reference only and is not used in any of the pulling logic
       // filter.setLastId(firstSeenTimeToPersist);
        filter.setTimeRange(firstSeenTimeToPersist);
        BigDecimal decimal = new BigDecimal(firstSeenTimeToPersist);
//        long value = (long) (decimal.doubleValue() * 1000);
//        sdf.setTimeZone(TimeZone.getTimeZone("UTC"));
        sdf.setTimeZone(TimeZone.getTimeZone(addProps.get("USERTIMEZONE") == null?"UTC":(String) addProps.get("USERTIMEZONE")));
        // convert epoch to UTC timezone and update the Last Value column of the  filter table. This persisted value is used in next filter interval pull time
        String epochToUTCDate = sdf.format(new Date(Long.parseLong(firstSeenTimeToPersist.replace(".", ""))));
        filter.setLastValue(epochToUTCDate);

        // convert epoch to usertimezone and update the LastTimeStamp column of
        // the filter table.  This is for the debug reference only and is not used in any of the pulling logic
        
//        String epochToUserTimeZone = sdf.format(new Date(value));
        filter.setLastTimestamp(epochToUTCDate);
        ResolveGatewayUtil.updatePullGatewayFilter(filter,false);
        zenossApi.updateDeployedFilters(filter);
        //Update the main filter too
//        filter.setQueue(Constants.DEFAULT_GATEWAY_QUEUE_NAME);
//        ResolveGatewayUtil.updatePullGatewayFilter(filter);
        Log.log.debug(" +++++++++++++++++++++++++++++ Filter Interval Statistics==> +++++++++++++++++++++++++++++ ");
        Log.log.debug("Total Fetched Events: " + results.size());
//        Log.log.debug("First Seent Time of the Last event fetched at this filter interval(in User's TimeZone) is: " + epochToUTCDate);
        Log.log.debug("TimeStamp Of the Last Fetched Event During this Filter Interval===>   " + " Epoch Time: " + last.get("firstTime"));
        Log.log.debug(" +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ ");
    }
    // If the gateway needs to send acknowledgement back to the third-party server
    // implement this method
    // Optional
    public void updateAck(List<Map<String, String>> results) throws Exception{
        //Run through each of the records and acknowledge
            for(Map<String, String> eventsMap : results) {
                zenossApi.acknowledge(eventsMap.get("evid"));
            }
    }
    // Optional
    public void setRegexPattern(String pattern) {
        this.pattern = Pattern.compile(pattern);
    }

} // class ZenossGatewayImpl
