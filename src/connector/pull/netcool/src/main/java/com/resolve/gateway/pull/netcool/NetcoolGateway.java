package com.resolve.gateway.pull.netcool;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.Hashtable;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Properties;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ConcurrentLinkedQueue;
import java.util.concurrent.TimeUnit;

import org.apache.commons.pool.BasePoolableObjectFactory;
import org.apache.commons.pool.impl.GenericObjectPool;

import com.resolve.rsbase.MainBase;
import com.resolve.gateway.Filter;
import com.resolve.gateway.resolvegateway.pull.ConfigReceivePullGateway;
import com.resolve.gateway.resolvegateway.pull.PullGateway;
import com.resolve.gateway.resolvegateway.pull.PullGatewayFilter;
import com.resolve.gateway.resolvegateway.pull.PullGatewayProperties;
import com.resolve.util.ERR;
import com.resolve.util.Log;
import com.resolve.util.MessageDispatcher;
import com.resolve.util.SQLUtils;
import com.resolve.util.StringUtils;
import com.resolve.util.ThreadUtils;
import com.resolve.util.metrics.ExecutionMetrics;
import com.resolve.thread.ScheduledExecutor;
import com.resolve.util.queue.QueueListener;
import com.resolve.util.queue.QueueManager;

public class NetcoolGateway extends PullGateway
{
    private static volatile NetcoolGateway instance = null;
    
    private final static int STATE_PASSIVE_CONNECT = 0;
    private final static int STATE_ACTIVE_PRIMARY = 1;
    private final static int STATE_ACTIVE_BACKUP = 2;
    
    private static final String NETCOOL_SERVER_SERIAL = "NETCOOL_SERVER_SERIAL";
    private static final String NETCOOL_SERIAL = "NETCOOL_SERIAL";
    private static final String NETCOOL_SERVER_NAME = "NETCOOL_SERVER_NAME";
    
    private int state = STATE_PASSIVE_CONNECT;
    private int retryDelay = 5000;
    private int reconnectDelay = 60000;
    private boolean hasBackupOS = false;
    private int maxConnection = 10;
    private String version;
    private int UID = 0;
    private ConcurrentLinkedQueue<String> writeQueue = null;
    private String url;
    private String url2;
    private String lastUrl;
    private String username;
    private String password;
    private long lastSerial = 0;
    private long lastServerSerial = 0;
    private boolean reinitLastValues;
    private long initTS;
    private long diffTS;
    private boolean updateResolveStatus = false;
    private boolean updateRunbookId = false;
    private boolean connChanged = true;
    private String statusFieldname = "ResolveStatus";
    private String statusProcess = "1";
    private String statusType = "NUMBER";
    private String runbookIdFieldname = "RunbookId";
    private boolean checkNetcoolConnectionIsValid = false;
    private long minEvictableIdleTimeMillis = 10 * 60 * 1000;
    private long timeBetweenEvictionRunsMillis = 5 * 60 * 1000;
    
    Properties connProps = new Properties();

    private Connection conn;
    private long eventCounter;

    // Time out for ESB messages
    static final long SEND_TIMEOUT = 10 * 60 * 1000; // milliseconds

    // Specific condition to meet for querying/retriving events from third-party servers, i.e. Sample server, such as Moogsoft server
    // This query is used to pull back events/alerts/incidents from the remote server
    public static final String QUERY = "QUERY";
    
    private final static int MAX_RETRY = 3;
    private String PRIMARY_POOL = "PrimaryObjectServer";
    private String SECONDARY_POOL = "SecondaryObjectServer";
    
    @SuppressWarnings("rawtypes")
    private ConcurrentHashMap<String, GenericObjectPool> connectionPool = new ConcurrentHashMap<String, GenericObjectPool>();

    // Optional last value fields that will be persisted into the database for each polling interval
    public static final String LAST_VALUE = "LAST_VALUE";
    public static final String LAST_TIMESTAMP = "LAST_TIMESTAMP";

    // Optional flexible attributes for the specific gateway that to be persisted into the database
    public static final String LAST_VALUE_FIELD = "LAST_VALUE_FIELD";

    // Cached instance of the third-party server
    // Optional
    // static SampleServer server = new SampleServer();

    // Singleton constructor to take the blueprint configuration and initialize the deployed filters for this specific gateway
    // Do not modify
    private NetcoolGateway(ConfigReceivePullGateway config) {

        super(config);

        String gatewayName = this.getClass().getSimpleName();
        int index = gatewayName.indexOf("Gateway");
        if(index != -1) {
            name = gatewayName.substring(0, index);

            if(StringUtils.isBlank(name)) {
                Log.log.error("NetcoolGateway: Not appropriate gateway name: " + gatewayName);
                return;
            }

            try {
                StringBuilder sb = new StringBuilder();
                PullGatewayProperties properties = (PullGatewayProperties)ConfigReceivePullGateway.getGatewayProperties(name);
                sb.append(properties.getPackageName()).append(".").append(gatewayName).append("Impl");
                Class<?> implClass = Class.forName(sb.toString());
                impl = (NetcoolGatewayImpl)implClass.newInstance();
            } catch(Exception e) {
                Log.log.error(e.getMessage(), e);
            }
        }

        else {
            Log.log.error("NetcoolGateway: Not appropriate gateway name: " + gatewayName);
            return;
        }

        Map<String, PullGatewayFilter> pullFilters = getPullFilters();

        for(Iterator<String> iterator=pullFilters.keySet().iterator(); iterator.hasNext();) {
            String filterName = iterator.next();
            PullGatewayFilter filter = pullFilters.get(filterName);

            if(name.indexOf(((PullGatewayFilter)filter).getGatewayName()) != -1) {
                if(filters.get(filterName) == null) {
                    filters.put(filterName, pullFilters.get(filterName));

                    if(filter.isDeployed()) {
                        orderedFilters.add(filter);
                        orderedFiltersMapById.put(filter.getId(), filter);
                    }
                }
            }
        }

        ConfigReceivePullGateway.getGateways().put(name, this);
    }

    // Public method to get an instance from this Singleton class
    // Do not modify
    public static NetcoolGateway getInstance(ConfigReceivePullGateway config) {

        if (instance == null) {
            instance = new NetcoolGateway(config);
        }

        return instance;
    }

    // This method is called after the instance is instantiated.
    // Do not modify
    public static NetcoolGateway getInstance() {

        if (instance == null) {
            throw new RuntimeException("Netcool Gateway is not initialized correctly..");
        } else {
            return instance;
        }
    }

    // Initialize the gateway with the default HTTP server instance and start to listen on the default port
    // Do not remove the existing code, but you can add additional logic for initialization if necessary
    @Override
    protected void initialize() {

        queue = config.getQueue().toUpperCase();

        try {
            Log.log.info("NetcoolGateway: Initializing Netcool Listener ...");
            this.gatewayConfigDir = "/config/" + queue.toLowerCase() + "/";

            PullGatewayProperties properties = (PullGatewayProperties)ConfigReceivePullGateway.getGatewayProperties(name);

            // Add additional custom logic here, e.g., start the Sample server etc.
            
            Map<String, Object> additional = properties.getAdditionalProperties();

            version = (String)additional.get("version");
            statusProcess = (String)additional.get("status.process");
            statusType = (String)additional.get("status.type");
            statusFieldname = (String)additional.get("status.fieldname");
            runbookIdFieldname = (String)additional.get("runbookid.fieldname");
            
            String max = (String)additional.get("poolsize");
            String isValid = (String)additional.get("activeconnectionvalidation");
            String retryDelayStr = (String)additional.get("retrydelay");
            String reconnectDelayStr = (String)additional.get("reconnectdelay");
            String runbookActive = (String)additional.get("runbookid.active");
            String statusActive = (String)additional.get("status.active");
            String minEvictableIdleTime = (String)additional.get("minevictableidletime");
            String timeBetweenEvictionRuns = (String)additional.get("timebetweenevictionruns");
            
            if (StringUtils.isBlank(statusType) || (!"NUMBER".equalsIgnoreCase(statusType) && !"STRING".equalsIgnoreCase(statusType))) {
                Log.log.warn("NetcoolGateway: Invalid Status Type " + statusType + ": Value must be NUMBER or STRING");
                statusType = "NUMBER";
            }
            
            else
                statusType = statusType.toUpperCase();
            
            if(StringUtils.isNotEmpty(isValid)) {
                try {
                    checkNetcoolConnectionIsValid = Boolean.parseBoolean(isValid);
                    updateResolveStatus = Boolean.parseBoolean(statusActive);
                    updateRunbookId = Boolean.parseBoolean(runbookActive);
                } catch(Exception ee) {
                    Log.log.error(ee.getMessage(), ee);
                }
            }
            
            if(StringUtils.isNotEmpty(max)) {
                try {
                    maxConnection = Integer.parseInt(max);
                    retryDelay = Integer.parseInt(retryDelayStr)*1000;
                    reconnectDelay = Integer.parseInt(reconnectDelayStr)*1000;
                    minEvictableIdleTimeMillis = Integer.parseInt(minEvictableIdleTime)*1000;
                    timeBetweenEvictionRunsMillis = Integer.parseInt(timeBetweenEvictionRuns)*1000;
                } catch(Exception ee) {
                    Log.log.error(ee.getMessage(), ee);
                }
            }
            
            // init ObjectServer connection
            String driver = (String)additional.get("url");
            if (StringUtils.isEmpty(driver))
                driver = "com.sybase.jdbc3.jdbc.SybDriver";
            Class.forName(driver);
            
            // init primary objectserver url
            url = (String)additional.get("url");
            String host = properties.getHost();
            if(StringUtils.isBlank(url) && StringUtils.isNotBlank(host))
                url = "jdbc:sybase:Tds:" + host + ":" + properties.getPort() + "?LITERAL_PARAMS=true";
            
            // init backup objectserver url if defined
            String url2Str = (String)additional.get("url2");
            if(!StringUtils.isNotEmpty(url2Str)) {
                url2 = url2Str;
                hasBackupOS = true;
            }

            connProps.put("user", properties.getUser());
            connProps.put("password", properties.getPass());
            
            String appName = (String)additional.get("applicationname");
            if(StringUtils.isNotBlank(appName))
                connProps.put("APPLICATIONNAME", appName);
            
            // Add listener;
            QueueListener<String> emailListener = new UpdateSqlListener();
            writeQueue = QueueManager.getInstance().getResolveConcurrentLinkedQueue(emailListener);

            checkDeadlock();
            ScheduledExecutor.getInstance().executeRepeat(getLicenseCode() + "-" + getQueueName() + "-" + getInstanceType() + "-CheckDeadLock", this, "checkDeadlock", 30, TimeUnit.SECONDS);
        } catch (Exception e) {
            Log.log.error("NetcoolGateway: Failed to initialize RemedyX Server: " + e.getMessage(), e);
        }
    }

    // The gateway will start to run by calling the super class common logic to start the polling for each deployed filter
    @Override
    public void start() {

        Log.log.debug("Starting Netcool Gateway");
        
        super.start();
        
        state = STATE_PASSIVE_CONNECT;

        if (StringUtils.isBlank(url) && StringUtils.isBlank(url2))
            Log.log.info("No netcool URL configured, this gateway will act as filter processor from other event mgmt system via interfaces like Webservices.");

        else
            connect();
    }

    // This method will be called when the gateway is gracefully shutdown
    @Override
    public void stop() {

        Log.log.warn("Stopping Netcool Pull gateway");

        // Add code for shutdown the Sample Server
        try {
            disconnect();
        } catch (Exception e) {
            Log.log.warn("Unable to close connection. " + e.getMessage());
        }

        super.stop();
    }

    public void run() {
        if (conn == null)
            connect();

        // This call is must for all the ClusteredGateways.
//        super.sendSyncRequest();

        long lastPrimaryRetry = 0;
        String serverSerialKeyName = null;

        if (StringUtils.isBlank(url) && StringUtils.isBlank(url2))
            Log.log.info("No netcool URL configured, this gateway will act as filter processor from other event mgmt system via interfaces like Webservices.");
        else {
            while (running) {
                Log.log.trace("state: " + getStateString() + " active: " + isActive());
                
                try {
                    // connection established
                    if (conn != null && !conn.isClosed()) {
                        long startTime = System.currentTimeMillis();
    
                        /* if any remaining events are in primary data queue or 
                         * primary data executor has waiting messages still to be processed, 
                         * do not get messages for more new events.
                         */
                        
                        if (primaryDataQueue.isEmpty() && !primaryDataQueueExecutorBusy()) {
                            // check for new alerts
                            for (Filter filter : orderedFilters) {
                                if (shouldFilterExecute(filter, startTime)) {
                                    PullGatewayFilter netcoolFilter = (PullGatewayFilter)filter;
                                    Map<String, Object> attrs = netcoolFilter.getAttributes();

                                    if(Log.log.isTraceEnabled())
                                        Log.log.trace("Before poll: event counter = " + eventCounter + ", Message counter=" + MessageDispatcher.messageCounter);
                                    
                                    List<Map<String, String>> netcoolEvents = invokeObjectServer(netcoolFilter);
                                    
                                    Log.log.debug("NetCoolFilter " + netcoolFilter.getId() + " with SQL [" + netcoolFilter.getQuery() + "] returned " + (netcoolEvents != null ? netcoolEvents.size() : "0") + " records.");
                                    
                                    if(netcoolEvents.size() > 0) {
                                        int netCoolEventsCount = netcoolEvents.size() < getPrimaryDataQueueExecutorQueueSize() ? netcoolEvents.size() : getPrimaryDataQueueExecutorQueueSize();
                                        // structure for the single SQL
                                        // query update
                                        List<String> serverSerialList = new ArrayList<String>(netCoolEventsCount);

                                        ExecutionMetrics.INSTANCE.registerEventStart("NetCool primary GW - pull from DB and put into the worker queue");
                                        
                                        for (Map<String, String> netcoolEvent : netcoolEvents) {
                                            // Initialize serverSerialKeyName
                                            if (StringUtils.isBlank(serverSerialKeyName)) {
                                                for (String netcoolEventKey : netcoolEvent.keySet()) {
                                                    if (netcoolEventKey.toUpperCase().equals("SERVERSERIAL")) {
                                                        serverSerialKeyName = netcoolEventKey;
                                                        Log.log.debug("SERVERSERIAL Key Name in Netcool event is " + serverSerialKeyName);
                                                        break;
                                                    }
                                               }
                                                
                                                if (StringUtils.isBlank(serverSerialKeyName))
                                                    throw new Exception("Unable to find Server Serial key name in received Netcool event " + StringUtils.mapToString(netcoolEvent));
                                            }
                                            
                                            addToPrimaryDataQueue(netcoolFilter, netcoolEvent);

                                            if (netcoolEvent.containsKey(serverSerialKeyName)) {
                                                Log.log.debug(String.format("Current Serial  : %s", netcoolEvent.get(serverSerialKeyName)));
                                                long currentNetcoolServerSerial = Long.parseLong(netcoolEvent.get(serverSerialKeyName));
                                                
                                                long netCoolServerSerial = 0L;
                                                String lastServerSerialStr = ((Long)attrs.get("lastServerSerial")).toString();
                                                Log.log.debug(String.format("SERVERSERIAL B4 : %s", lastServerSerialStr));
                                                
                                                if(StringUtils.isNotBlank(lastServerSerialStr)) {
                                                    try {
                                                        netCoolServerSerial = Long.parseLong(lastServerSerialStr);
                                                    } catch(Exception ee) {
                                                        Log.log.warn(ee.getMessage(), ee);
                                                    }
                                                }
                                                
                                                if (currentNetcoolServerSerial > netCoolServerSerial)
                                                    attrs.put("lastServerSerial", currentNetcoolServerSerial);
                                            }
                                            // adding the ServerSerial to the list for the single SQL query update
                                            serverSerialList.add(netcoolEvent.get(serverSerialKeyName));
                                        }
                                        
                                        attrs.put("lastSuccessfulQuery", System.currentTimeMillis());
                                        
                                        ExecutionMetrics.INSTANCE.registerEventStart("NetCool primary GW - acknowledgement (bulk update)");
                                        bulkUpdateServerDataSingleSQLQuery(netcoolFilter, serverSerialList);
                                        ExecutionMetrics.INSTANCE.registerEventEnd("NetCool primary GW - acknowledgement (bulk update)", netCoolEventsCount);
                                        
                                        ExecutionMetrics.INSTANCE.registerEventEnd("NetCool primary GW - pull from DB and put into the worker queue", netCoolEventsCount);
                                    }
                                }
                                else
                                    Log.log.trace("Filter:" + filter.getId() + " is inactive or not ready to execute yet.....");
                            }
                        }
                        
                        Thread.sleep(interval);
    
                        // attempt to reconnect to primary if currently using backup
                        if (state == STATE_ACTIVE_BACKUP) {
                            if (System.currentTimeMillis() - lastPrimaryRetry > reconnectDelay) {
                                connect();
                                lastPrimaryRetry = System.currentTimeMillis();
                            }
                        }
                    }
                    
                    // reconnect
                    else {
                        Thread.sleep(reconnectDelay);
                        Log.log.info("Reconnecting");
                        state = STATE_PASSIVE_CONNECT;
                        connect();
                    }
                } catch (Exception e) {
                    Log.log.warn("Error: " + e.getMessage(), e);
                    Log.alert("Instance " + MainBase.main.configId.getGuid() + " " + getLicenseCode() + "-" + getQueueName() + "-" + getInstanceType() + " Polling Failed", 
                              "Instance " + MainBase.main.configId.getGuid() + " " + getLicenseCode() + "-" + getQueueName() + "-" + getInstanceType() + " Polling Failed " + 
                              "due to " + e.getMessage());
                    
                    try {
                        Thread.sleep(interval);
                    } catch (InterruptedException ie) {
                        Log.log.warn("sleep interrupted: " + ie.getMessage(), ie);
                    }
                }
            }
        }
    }
    
    private List<Map<String, String>> invokeObjectServer(PullGatewayFilter filter) {
        
        List<Map<String, String>> result = new ArrayList<Map<String, String>>();

        ResultSet rs = null;
        Statement stmt = null;
        
        Map<String, Object> attrs = filter.getAttributes();

        try {
            // init lastSuccessfulQUery
            if (reinitLastValues) {
                attrs.put("lastSuccessfulQuery", initTS);
                attrs.put("lastSerial", lastSerial);
                attrs.put("lastServerSerial", lastServerSerial);

                reinitLastValues = false;
            }
            
            Long lastSuccessfulQuery = (Long)attrs.get("lastSuccessfulQuery");
            try {
                if(lastSuccessfulQuery != null && lastSuccessfulQuery == 0L)
                    attrs.put("lastSuccessfulQuery", initTS);
            } catch(Exception ee) {
                Log.log.warn(ee.getMessage(), ee);
            }

            Long lastSerialValue = (Long)attrs.get("lastSerial");
            try {
                if(lastSerialValue != null && lastSerialValue == 0L)
                    attrs.put("lastSerial", lastSerial);
            } catch(Exception ee) {
                Log.log.warn(ee.getMessage(), ee);
            }
            
            Long lastServerSerialValue = (Long)attrs.get("lastServerSerial");
            try {
                if(lastServerSerialValue != null && lastServerSerialValue == 0L)
                    attrs.put("lastServerSerial", lastServerSerial);
            } catch(Exception ee) {
                Log.log.warn(ee.getMessage(), ee);
            }

            // init query
            String query = filter.getQuery();
            String runbook = filter.getRunbook();

            // replace LASTSERIAL with value
            query = query.replaceFirst("LASTSERIAL", "" + attrs.get("lastSerial"));

            // replace LASTSERVERSERIAL with value
            query = query.replaceFirst("LASTSERVERSERIAL", "" + attrs.get("lastServerSerialStr"));

            // replace LASTQUERY with value
            query = query.replaceFirst("LASTQUERY", "" + attrs.get("lastSuccessfulQuery"));

            Log.log.debug("Executing filter: " + filter.getId());
            Log.log.trace("executing query for runbook: " + runbook + "\n");
            Log.log.trace("  sql: " + query);
            
            lastSuccessfulQuery = (Long)attrs.get("lastSuccessfulQuery");
            try {
                long lastQuery = 0L;
                
                if(lastSuccessfulQuery != null)
                    lastQuery = lastSuccessfulQuery;

                Log.log.trace("  lastSuccessfulQuery: " + new java.util.Date(lastQuery * 1000));
            } catch(Exception ee) {
                Log.log.warn(ee.getMessage(), ee);
            }

            stmt = conn.createStatement();
            try {
                rs = stmt.executeQuery(query);
            } catch (SQLException ex) {
                Log.log.error("Failed to Execute Filter: " + filter.getId(), ex);
                sendAlert("CRITICAL", true, "Filter:" + filter.getId() + ":Update SQL Error", ex.getMessage());
            }

            int eventCounter = 0;
            while (rs.next()) {
                Map<String, String> runbookParams = new Hashtable<String, String>();

                // update lastSerial
                String serverName = rs.getString("ServerName").replace('\0', ' ').trim();
                long newSerial = rs.getLong("Serial");
                long newServerSerial = rs.getLong("ServerSerial");
                
                lastSerialValue = (Long)attrs.get("lastSerial");
                try {
                    long lastSerial = lastSerialValue;
                    if(lastSerialValue == null)
                        lastSerial = 0L;
                        if(newServerSerial > lastSerial)
                            attrs.put("lastSerial", newSerial);
                } catch(Exception ee) {
                    Log.log.warn(ee.getMessage(), ee);
                }
                
                lastServerSerialValue = (Long)attrs.get("lastServerSerial");
                try {
                    long lastServerSerial = lastServerSerialValue;
                    if(lastServerSerialValue == null)
                        lastServerSerial = 0L;
                        if(newServerSerial > lastServerSerial)
                            attrs.put("lastServerSerial", newServerSerial);
                } catch(Exception ee) {
                    Log.log.warn(ee.getMessage(), ee);
                }

                // define values from SELECT
                ResultSetMetaData rsmd = rs.getMetaData();
                for (int i = 1; i <= rsmd.getColumnCount(); i++) {
                    String name = rsmd.getColumnName(i);
                    String value = rs.getString(i);

                    // remove null from value
                    value = value.replace('\0', ' ').trim();
                    if (name.equalsIgnoreCase("PROCESSID"))
                        name = "NETCOOL_" + name;

                    if (name.equalsIgnoreCase("GUID"))
                        name = "NETCOOL_" + name;

                    if (name.equalsIgnoreCase("QUEUE"))
                        name = "NETCOOL_" + name;

                    if (isUppercase())
                        runbookParams.put(name.toUpperCase(), value);

                    else
                        runbookParams.put(name, value);
                }

                runbookParams.put(NETCOOL_SERVER_NAME, serverName);
                runbookParams.put(NETCOOL_SERIAL, "" + newSerial);
                runbookParams.put(NETCOOL_SERVER_SERIAL, "" + newServerSerial);
                result.add(runbookParams);
                eventCounter++;
            }
            
            rs.close();

            if (eventCounter > 0) {
                // update lastQuery timestamp for successful queries
                rs = stmt.executeQuery("SELECT max(getdate) FROM alerts.colors");

                if (rs.next()) {
                    long ts = rs.getLong(1); // remote timestamp on

                    attrs.put("lastSuccessfulQuery", ts);
                    Log.log.trace("Updating lastSuccessfulQuery: " + ts);
                }
                
                rs.close();
            }

            stmt.close();
        } catch (Exception e) {
            String msg = e.getMessage();
            if (msg != null && msg.contains("ObjectServer is currently busy"))
                Log.log.warn("ObjectServer is currently busy. Waiting for database locks to be released");

            else if (msg != null && msg.indexOf("ObjectServer") > -1 && msg.indexOf("resynchronized") > -1 && msg.indexOf("resync lock to be released") > -1) {
                // Exception: ObjectServer is currently being resynchronized.
                // Waiting for resync lock to be released
                Log.log.warn("Waiting for ObjectServer resynchronization lock to be released: " + e.getMessage(), e);
                connChanged = true;
                boolean reTry = true;
                // Keep retrying until initTimestamp() is successful.
                while (reTry) {
                    try  {
                        Thread.sleep(60000);
                        initTimestamp();
                        reTry = false;
                    } catch (Exception ex) {
                        Log.log.error("Failed to reinitialize timestamp after ObjectServer was locked by resynchronization", ex);
                    }
                }
            }
            
            else
                Log.log.error("Failed to poll for events: " + e.getMessage(), e);
        } finally {
            if (rs != null) {
                try {
                    rs.close();
                } catch (Exception sqlEx) {
                } // ignore

                rs = null;
            }

            if (stmt != null) {
                try {
                    stmt.close();
                } catch (Exception sqlEx) {
                } // ignore

                stmt = null;
            }
        }

        return result;
    }

    // This method will be called when a filter is deployed to validate the semantics of the field only
    // The syntax of the filter fields have been validated when the filter is defined and saved from UI
    // including whether the filter field is required or not per gateway definition in blueprint.properties
    @Override
    public int validateFilterFields(Map<String, Object> params) {

        int code = 0;

        // Add custom logic here

        return code;
    }

    // The method returns the error string that is associated with the error code defined
    // under the custom logic being implemented
    @Override
    public String getValidationError(int errorCode) {

		String error = null;

		// Add custom logic here

        return error;
    }

    // This method is used by the system
    // Do not modify
    @Override
    protected String getMessageHandlerName() {

        return this.getClass().getName();
    }

    // If the common logic in the super class does not support the use case for this specific gateway,
    // you may create override methods to replace the logic in the super class
    
    private boolean connect() {
        
        boolean result = false;

        Log.log.info("Current state: " + getStateString());
        
        if ((running && isActive()) || (isWorker() && !isPrimary())) {
            // try connecting to primary
            Log.log.info("Attempting to connect with PRIMARY Netcool ObjectServer");
            
            if (connectObjectServer(url)) {
                state = STATE_ACTIVE_PRIMARY;
                Log.log.info("New state: " + getStateString());
                initPrimaryPool();
                result = true;
            }
            
            else {
                if (hasBackupOS) {
                    // already connected (just attempting primary above)
                    if (state == STATE_ACTIVE_BACKUP)
                        result = true;
                    
                    // attempt to connect to backup
                    else {
                        Log.log.info("Attempting to connect with BACKUP Netcool ObjectServer");
                        if (connectObjectServer(url2)) {
                            state = STATE_ACTIVE_BACKUP;
                            Log.log.info("New state: " + getStateString());
                            initSecondaryPool();
                            result = true;
                        }
                        else {
                            state = STATE_PASSIVE_CONNECT;
                            Log.log.info("New state: " + getStateString());
                            Log.alert(ERR.E50001);

                            result = false;
                        }
                    }
                }
                else
                    result = false;
            }
        }

        return result;
    }
    
    private void disconnect() {
        
        try {
            if (conn != null)
                conn.close();
        } catch (Exception e) {
            Log.log.warn("Unable to close connection. " + e.getMessage());
        }
    }
    
    private void closeConnection(Connection con) {
        
        if (con != null) {
            try {
                con.close();
            } catch (Throwable ex) {
            }
        }
    }
    
    private String getStateString() {
        
        String result = "";

        switch (state) {
            case STATE_PASSIVE_CONNECT:
                result = "PASSIVE_CONNECT";
                break;
            case STATE_ACTIVE_PRIMARY:
                result = "ACTIVE_PRIMARY";
                break;
            case STATE_ACTIVE_BACKUP:
                result = "ACTIVE_BACKUP";
                break;
        }

        return result;
    }
    
    private boolean connectObjectServer(String url) {
        
        boolean result = false;
        int retry = MAX_RETRY;

        Log.log.info("Connecting url: " + url);
        
        if (state == STATE_PASSIVE_CONNECT) {
            closeConnection(conn);
            conn = null;
        }

        if (state == STATE_ACTIVE_BACKUP)
            retry = 1;

        while (!result && retry > 0) {
            try {
                // get connection
                //Driver Manager change JIRA #RS-25998
                //Connection newconn = DriverManager.getConnection(url, username, password);
                Connection newconn = DriverManager.getConnection(url, connProps);
                closeConnection(conn);
                
                Log.log.info("Connected to Netcool at: " + url);
                
                this.conn = newconn;

                // set lastUrl and connChanged
                if (StringUtils.isEmpty(lastUrl) || !lastUrl.equals(url))
                {
                    this.connChanged = true;
                }
                this.lastUrl = url;

                // get starting serial number
                initTimestamp();

                result = true;
            } catch (Exception e) {
                try {
                    if (state != STATE_ACTIVE_BACKUP) {
                        Log.log.warn("Unable to connect to Netcool: " + e.getMessage());
                        Thread.sleep(retryDelay);
                    }
                } catch (Exception e2) {
                }
                
                retry--;
            }
        }

        if (retry == 0 && state != STATE_ACTIVE_BACKUP)
            Log.log.error("ABORTING: Failed to connect to Netcool at: " + url);

        return result;
    }
    
    private void initPrimaryPool() {
        // initConnectionPool(this.url, maxConnection, PRIMARY_POOL);
        initCustomizedConnectionPool(PRIMARY_POOL);
    }
    
    private void initSecondaryPool()
    {
        // initConnectionPool(this.url2, maxConnection, SECONDARY_POOL);
        initCustomizedConnectionPool(SECONDARY_POOL);
    }
    
    @SuppressWarnings({ "unchecked", "rawtypes" })
    private void initCustomizedConnectionPool(String poolName) {
        
        if (connectionPool.get(poolName) != null)
            return;

        GenericObjectPool pool = new GenericObjectPool(new NetcoolConnFactory());

        // If maximum connection number
        pool.setMaxActive(maxConnection);
        if (checkNetcoolConnectionIsValid)
            pool.setTestOnBorrow(true);

        pool.setMinEvictableIdleTimeMillis(minEvictableIdleTimeMillis);
        pool.setTimeBetweenEvictionRunsMillis(timeBetweenEvictionRunsMillis);
        connectionPool.put(poolName, pool);
    }
    
    @SuppressWarnings("rawtypes")
    private class NetcoolConnFactory extends BasePoolableObjectFactory {
        
        String sqlCheck = "SELECT TOP 1 Severity From alerts.colors;";
        
        @Override
        public Object makeObject() throws Exception {
            
            Connection newconn = null;
            
            if (state == STATE_ACTIVE_PRIMARY) {
               // Driver Manager change JIRA #RS-25998
               // newconn = DriverManager.getConnection(url, username, password);
                 newconn = DriverManager.getConnection(url, connProps);
            }
            
            else if (state == STATE_ACTIVE_BACKUP) {
              //Driver Manager manager  JIRA #RS-25998
              //  newconn = DriverManager.getConnection(url2, username, password);
                 newconn = DriverManager.getConnection(url2, connProps);
            }
            
            return newconn;
        }

        @Override
        public void destroyObject(Object obj) throws Exception {
            
            Log.log.trace("Destroying connection object");
            
            if (obj != null && !((Connection)obj).isClosed()) {
                try {
                    ((Connection) obj).close();
                } catch (SQLException sqle) {
                    Log.log.trace("Error Destroying Connection", sqle);
                }
            }
        }

        @Override
        public boolean validateObject(Object obj) {
            
            Log.log.trace("Validating Connection Object");
            
            boolean b = false;
            Connection conn = (Connection) obj;
            Statement s = null;
            
            try {
                s = conn.createStatement();
                s.setQueryTimeout(10);
                s.execute(sqlCheck);
                b = true;
            } catch (Throwable ex) {
                b = false;
                Log.log.error("Connection Validation Failed", ex);
            } finally {
                try {
                    if (s != null)
                        s.close();
                }
                catch (SQLException sqle) {
                    // do nothing
                }
            }
            
            return b;
        }
    }
    
    private void initTimestamp() throws Exception {
        
        Statement stmt = null;
        ResultSet rs = null;

        try {
            if (conn == null) {
                boolean success = connect();
                
                if (!success)
                    throw new Exception("Failed to initialize serial and timestamp. Unable to connect.");
            }
            
            // initialize Serial
            Log.log.debug("getting start ServerSerial");

            // This is inefficient
            // String sql = "SELECT Serial, getdate FROM alerts.status ORDER BY Serial DESC;";
            String sql = "SELECT max(Serial), max(ServerSerial), max(getdate) FROM alerts.status";

            stmt = conn.createStatement();
            rs = stmt.executeQuery(sql);
            
            if (rs.next()) {
                // set lastSerial if new or changed connection
                if (connChanged) {
                    lastSerial = rs.getLong(1);
                    lastServerSerial = rs.getLong(2);
                    Log.log.debug("lastSerial: " + lastSerial);
                }

                reinitLastValues = true;
                initTS = rs.getLong(3);
                Log.log.debug("initTS: " + initTS);

                diffTS = System.currentTimeMillis() / 1000 - initTS;
                Log.log.debug("diffTS: " + diffTS);
            }
            
            rs.close();
            stmt.close();

            // initialize UID
            Log.log.debug("getting UID for netcool user");
            sql = "SELECT UID FROM master.names WHERE Name='" + username + "';";

            stmt = conn.createStatement();
            rs = stmt.executeQuery(sql);
            if (rs.next()) {
                UID = rs.getInt("UID");
                Log.log.debug("Name: " + username + " UID: " + UID);
            }
            
            rs.close();
            stmt.close();
        } catch (Exception e) {
            Log.log.error("Failed to initialize serial and timestamp: " + e.getMessage(), e);
            throw e;
        } finally {
            try {
                if (rs != null)
                    rs.close();
                if (stmt != null)
                    stmt.close();
            } catch (Exception sqlEx) {
            }
        }
    }
    
    private void checkDeadlock() {
        
        ThreadUtils.detectDeadlocks();
        //SystemExecutor.executeDelayed(this, "checkDeadlock", 30, TimeUnit.SECONDS);
    }
    
    public String update(String sql) throws Exception {
        
        String resultStr = null;
        int result = -1;
        Statement stmt = null;
        boolean attempted = true;
        Connection con = getCustomizedConnection();
        
        try {
            stmt = con.createStatement();
            while (attempted) {
                Log.log.trace("Synchrnous netcool update call, sql: " + sql);
                try {
                    result = stmt.executeUpdate(sql);
                    Log.log.debug("(" + result + ")row affected");
                    attempted = false;
                    resultStr = "" + result;
                } catch (Exception e) {
                    // dont remove if ObjectServer is busy
                    if (e.getMessage().contains("ObjectServer is currently busy")) {
                        Log.log.info("Object Server is busy, will retry update. Exception is " + e.getMessage());
                        Thread.sleep(5000);
                    }
                    else {
                        attempted = false;
                        resultStr = e.getMessage();
                        Log.log.warn("Netcool Update Exception: " + e.getMessage(), e);
                    }
                }
            }
        } catch (Exception e) {
            Log.log.warn(e.getMessage());
            resultStr = e.getMessage();
            throw e;
        } finally {
            if (stmt != null) {
                try {
                    stmt.close();
                } catch (Throwable sqlEx) {
                } // ignore

                stmt = null;
            }

            if (con != null) {
                try {
                    returnCustomizedConnection(con);
                } catch (Throwable sqlEx) {
                } // ignore
            }
        }
        
        return resultStr;
    }
    
    private Connection returnCustomizedConnection(Connection conn) {
        
        if (state == STATE_ACTIVE_PRIMARY)
            return returnCustomizedConnection(PRIMARY_POOL, conn);

        else if (state == STATE_ACTIVE_BACKUP)
            return returnCustomizedConnection(SECONDARY_POOL, conn);

        else
            throw new RuntimeException("Failed to return connection. Netcool gateway is not active.");
    }
    
    @SuppressWarnings({ "unchecked", "rawtypes" })
    private Connection returnCustomizedConnection(String pool, Connection jconn) {
        
        GenericObjectPool obj = connectionPool.get(pool);
        Connection conn = null;
        
        if (obj != null) {
            try {
                obj.returnObject(jconn);
            } catch (Exception ex) {
                throw new RuntimeException(ex);
            }
        }
        
        return conn;
    }
    
    private Connection getCustomizedConnection() {
        
        if (state == STATE_ACTIVE_PRIMARY)
            return getCustomizedConnection(PRIMARY_POOL);
        
        else if (state == STATE_ACTIVE_BACKUP)
            return getCustomizedConnection(SECONDARY_POOL);

        else
            throw new RuntimeException("Failed to get valid connection. Netcool gateway is not active.");
    }
    
    @SuppressWarnings({ "rawtypes", "unchecked" })
    private Connection getCustomizedConnection(String pool) {
        
        Log.log.debug("Inside getCustomizedConnection");
        
        GenericObjectPool obj = connectionPool.get(pool);
        
        if (Log.log.isTraceEnabled())
            Log.log.trace("Connection pool: " + pool + ", active=" + obj.getNumActive() + ", idle=" + obj.getNumIdle());

        Connection conn = null;
        if (obj != null) {
            try {
                while (true) {
                    conn = (Connection) obj.borrowObject();
                    
                    if (conn.isClosed()) // Bad connection, remove from pool.
                        obj.invalidateObject(conn);
                    else
                        break;
                }
            } catch (Exception ex) {
                throw new RuntimeException(ex);
            }
        }
        
        return conn;
    }
    
    public void bulkUpdateServerDataSingleSQLQuery(Filter filter, List<String> serverSerialList) {
        
        String updateFields = null;
        
        if (updateRunbookId) {
            String value = filter.getRunbook();
            updateFields = runbookIdFieldname + "=" + "'" + value + "'";
        }

        StringBuilder whereClause = null;

        if (updateResolveStatus) {
            if (updateFields != null)
                updateFields += ", ";

            else
                updateFields = "";

            if ("STRING".equalsIgnoreCase(statusType))
                updateFields += statusFieldname + "='" + statusProcess + "'";

            else
                updateFields += statusFieldname + "=" + statusProcess;

            if (serverSerialList.size() > 0) {
                whereClause = new StringBuilder(" where ServerSerial in (");
                int index = 1;
                
                for (String string : serverSerialList) {
                    whereClause.append(string);
                    if (index < serverSerialList.size()) {
                        whereClause.append(",");
                        index++;
                    }
                    else
                        whereClause.append(")");
                }
            }
        }

        if (updateFields != null) {
            try {
                StringBuilder updateSQLBuilder = new StringBuilder();
                updateSQLBuilder.append("update alerts.status set ");
                updateSQLBuilder.append(updateFields);
                
                if (whereClause != null)
                    updateSQLBuilder.append(whereClause.toString());

                if (Log.log.isTraceEnabled())
                    Log.log.trace("Update sql: " + updateSQLBuilder.toString());
                
                PreparedStatement stmt;
				try {
					stmt = conn.prepareStatement(SQLUtils.getSafeSQL(updateSQLBuilder.toString()));
				} catch (Exception e) {
					throw new SQLException(e);
				}
                int rowsUpdated = stmt.executeUpdate();
                if (Log.log.isTraceEnabled())
                    Log.log.trace("(" + rowsUpdated + ")row(s) affected");
            } catch (SQLException e) {
                Log.log.error(e.getMessage(), e);
            }
        }
    }
    
} // class NetcoolGateway
