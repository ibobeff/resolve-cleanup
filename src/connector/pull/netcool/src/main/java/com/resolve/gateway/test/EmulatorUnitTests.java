package com.resolve.gateway.test;

import org.junit.BeforeClass;
import org.junit.runner.RunWith;
import org.junit.runners.Suite;
import org.junit.runners.Suite.SuiteClasses;

import com.resolve.gateway.netcool.NetcoolGateway;
import com.resolve.gateway.pull.netcool.NetcoolGatewayAPI;


@RunWith(Suite.class)
@SuiteClasses({NetcoolGateway.class, NetcoolGatewayAPI.class })
public class EmulatorUnitTests
{
    
    @BeforeClass
    public static void setUpClass() {      
        System.out.println("TestCase1 setup");
    }

}

