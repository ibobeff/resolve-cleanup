package com.resolve.gateway.pull.netcool;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.regex.Pattern;

import com.resolve.gateway.resolvegateway.ResolveGatewayUtil;
import com.resolve.gateway.resolvegateway.pull.PullGateway;
import com.resolve.gateway.resolvegateway.pull.PullGatewayFilter;
import com.resolve.gateway.resolvegateway.pull.PullGatewayInterface;
import com.resolve.util.Log;
import com.resolve.util.StringUtils;

public class NetcoolGatewayImpl implements PullGatewayInterface
{
    // Define a pattern to match for building custom query
    // Optional
    private Pattern pattern;

    public NetcoolGatewayImpl() {

    }

	// There are two typical ways to pull events from the third-party server:
	// by custom query or by time bucket defined in the filter depending on the methods
	// provided by the third-party server
    // Included is the template code for implementation using custom query and maintain
    // the last values, e.g., last id, last timestamp, or any last field values
    // The custom query may contain macros in the regex pattern of "\\$\\{[_A-Z]*}", it
    // can be built on the fly by calling buildCustomQuery() method in the super gateway class.
    // This method must be implemented, but the logic for query and update last value is optional.
    public List<Map<String, String>> retrieveData(PullGatewayFilter filter) throws Exception {

        List<Map<String, String>> result = new ArrayList<Map<String, String>>();

        try {
            String query = filter.getQuery();

            Map<String, String> fieldValuePairs = new HashMap<String, String>();
            String lastValue = filter.getLastValue();
            if(StringUtils.isBlank(lastValue))
                lastValue = "0";
            fieldValuePairs.put(PullGatewayFilter.LAST_VALUE, lastValue);

            Map<String, Object> filterAttrs =  filter.getAttributes();
            fieldValuePairs.put(NetcoolGateway.LAST_VALUE_FIELD, (String)filterAttrs.get("ulastEntryId"));

            String customQuery = PullGateway.getInstance().buildCustomQuery(pattern, query, fieldValuePairs);

            Log.log.debug("Netcool Pull Gateway Query to execute:::" + customQuery);

            // Retrieve the events/alerts/incidents data from the third-party server
            // result = NetcoolGatewayAPI.getInstance().getResult(customQuery);

    		// If the gateway needs to send acknowledgement back to the third-party server
    		// NetcoolGatewayAPI.getInstance().updateAck(result);
        } catch(Exception e) {
            Log.log.error(e.getMessage(), e);
        }

        return result;
    }

    // Netcool code for persist the last value in database
    // Optional
    public void updateLastValues(PullGatewayFilter filter, List<Map<String, String>> results) throws Exception {

        int size = results.size();

        if(size == 0)
            return;

        Map<String, String> last = results.get(size - 1);

        Map<String, Object> filterAttrs = filter.getAttributes();
        String lastValue = last.get(filterAttrs.get("ulastValueField"));

        Log.log.debug("The retrieved last value is " + lastValue);

        if(StringUtils.isNotEmpty(lastValue)) {
            filter.setLastValue(lastValue);
            ResolveGatewayUtil.updatePullGatewayFilter(filter, false);
        }
    }

    // If the gateway needs to send acknowledgement back to the third-party server
    // implement this method
    // Optional
    public void updateAck(List<Map<String, String>> results) throws Exception {

    }

    // Optional
    public void setRegexPattern(String pattern) {

        this.pattern = Pattern.compile(pattern);
    }

} // class NetcoolGatewayImpl
