/******************************************************************************
* (C) Copyright 2014
*
* Resolve Systems, LLC
*
* All rights reserved
* This software is distributed under license and may not be used, copied,
* modified, or distributed without the express written permission of
* Resolve Systems, LLC.
*
******************************************************************************/

package com.resolve.gateway.pull.netcool;

import com.resolve.util.Log;
import com.resolve.util.queue.AbstractQueueListener;

public class UpdateSqlListener extends AbstractQueueListener<String>
{
    public boolean process(String updateStatement)
    {
        boolean result = true;

        try
        {
            if (Log.log.isTraceEnabled())
            {
                Log.log.trace("Received update SQL statement to execute");
                Log.log.trace("Statement: " + updateStatement);
            }

            NetcoolGateway.getInstance().update(updateStatement);
        }
        catch (Exception e)
        {
            Log.log.warn("Failed to execute update statement: " + updateStatement, e);
            result = false;
        }

        return result;
    }
}
