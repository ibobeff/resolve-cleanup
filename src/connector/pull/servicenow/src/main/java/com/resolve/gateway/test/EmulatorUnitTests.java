package com.resolve.gateway.test;

import org.junit.BeforeClass;
import org.junit.runner.RunWith;
import org.junit.runners.Suite;
import org.junit.runners.Suite.SuiteClasses;

import com.resolve.gateway.pull.servicenow.SERVICENOWGateway;
import com.resolve.gateway.pull.servicenow.SERVICENOWGatewayAPI;


@RunWith(Suite.class)
@SuiteClasses({SERVICENOWGateway.class, SERVICENOWGatewayAPI.class })
public class EmulatorUnitTests
{
    
    @BeforeClass
    public static void setUpClass() {      
        System.out.println("TestCase1 setup");
    }

}

