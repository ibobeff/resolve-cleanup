package com.resolve.gateway.pull.servicenow;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.regex.Pattern;
import com.resolve.util.Constants;

import com.resolve.gateway.resolvegateway.ResolveGatewayUtil;
import com.resolve.gateway.resolvegateway.pull.ConfigReceivePullGateway;
import com.resolve.gateway.resolvegateway.pull.PullGateway;
import com.resolve.gateway.resolvegateway.pull.PullGatewayFilter;
import com.resolve.gateway.resolvegateway.pull.PullGatewayInterface;
import com.resolve.gateway.resolvegateway.pull.PullGatewayProperties;
import com.resolve.util.Log;
import com.resolve.util.StringUtils;

public class SERVICENOWGatewayImpl implements PullGatewayInterface
{
    // Define a pattern to match for building custom query
    // Optional
    private Pattern pattern;
    public static  GenericObjectService genericObjectService;
    private static final String NUMBER = "number";
    public static final String SYS_ID = "sys_id";
    public static String CHANGE_DATE = "sys_updated_on";
    static String lastChangeDateMacroInitVal;
    static String lastIncidentNumberMacroInitVal;

  
    public SERVICENOWGatewayImpl() {
       
    }

	// There are two typical ways to pull events from the third-party server:
	// by custom query or by time bucket defined in the filter depending on the methods
	// provided by the third-party server
    // Included is the template code for implementation using custom query and maintain
    // the last values, e.g., last id, last timestamp, or any last field values
    // The custom query may contain macros in the regex pattern of "\\$\\{[_A-Z]*}", it
    // can be built on the fly by calling buildCustomQuery() method in the super gateway class.
    // This method must be implemented, but the logic for query and update last value is optional.
    public List<Map<String, String>> retrieveData(PullGatewayFilter filter) throws Exception {
        List<Map<String, String>> result = new ArrayList<Map<String, String>>();

        try
        {
            String query = filter.getQuery();

            String orderBy = CHANGE_DATE;
            Map<String, Object> filterAttrs = filter.getAttributes();

            this.pattern = Pattern.compile(Constants.VAR_REGEX_CONNECTOR_VARIABLE);
            SERVICENOWGateway gateway = SERVICENOWGateway.getInstance();
            Map<String, Object> systemProperties = gateway.loadSystemProperties("SERVICENOW");
            PullGatewayProperties properties = ConfigReceivePullGateway.getGatewayProperties("SERVICENOW");
            Map<String, Object> configMap = properties.getAdditionalProperties();

        
            Map<String, String> fieldValuePairs = new HashMap<String, String>();

            // String lastValue = filter.getLastValue();
            String lastIncidentNumber = filter.getLastValue();
            String lastChangeDate = filter.getLastTimestamp();

            if (StringUtils.isBlank(lastIncidentNumber))
            {
                lastIncidentNumber = lastIncidentNumberMacroInitVal;

            }
            if (StringUtils.isBlank(lastChangeDate))
            {
                lastChangeDate = lastChangeDateMacroInitVal;

            }

            fieldValuePairs.put("LAST_NUMBER", lastIncidentNumber);
            fieldValuePairs.put("LAST_CHANGE_DATE", lastChangeDate);
          

            String customQuery = PullGateway.getInstance().buildCustomQuery(pattern, query, fieldValuePairs);

            Log.log.debug("SERVICENOW Pull Gateway Query to execute:::" + customQuery);
            result = SERVICENOWGateway.genericObjectService.search((String) filterAttrs.get("uobject"), customQuery, orderBy, true, null, null);
           /* Map<String, String> params = new HashMap<String, String>();
            params.put("description","This is created via GatewayAPI call");
            params.put("urgency","1" );
            params.put("state", "2");
            Map<String, String>  response = SERVICENOWGatewayAPI.createIncident(params,"", "");
            Map<String, String>  response1 = SERVICENOWGatewayAPI.createObject("incident", params, "", "");*/
            
            
            
            
            

        } catch(Exception e) {
            Log.log.error(e.getMessage(), e);
        }

        return result;
    }

    // SERVICENOW code for persist the last value in database
    // Optional
    public void updateLastValues(PullGatewayFilter filter, List<Map<String, String>> results) throws Exception {

        int size = results.size();

        if(size == 0)
            return;

        Map<String, String> last = results.get(size - 1);

        Map<String, Object> filterAttrs = filter.getAttributes();
        String lastValue = last.get(filterAttrs.get("ulastValueField"));
        
        String lastChangeDate = (String) last.get(CHANGE_DATE);
        String lastNumber = (String) last.get(NUMBER);

        Log.log.debug(CHANGE_DATE+" value of last retrieved incident is:  " + lastChangeDate);
        Log.log.debug(NUMBER+" of last retrieved incident is: " + lastNumber);

        if(StringUtils.isNotEmpty(lastChangeDate)) {
            filter.setLastTimestamp(lastChangeDate);
          
        }
        if(StringUtils.isNotEmpty(lastNumber)) {
            filter.setLastValue(lastNumber);
          
        }
        ResolveGatewayUtil.updatePullGatewayFilter(filter, false);
    }

    // If the gateway needs to send acknowledgement back to the third-party server
    // implement this method
    // Optional
    public void updateAck(List<Map<String, String>> results) throws Exception {

    }

    // Optional
    public void setRegexPattern(String pattern) {

        this.pattern = Pattern.compile(pattern);
    }

} // class SERVICENOWGatewayImpl
