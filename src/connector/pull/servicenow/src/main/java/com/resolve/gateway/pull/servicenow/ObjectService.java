/******************************************************************************
* (C) Copyright 2014
*
* Resolve Systems, LLC
*
* All rights reserved
* This software is distributed under license and may not be used, copied,
* modified, or distributed without the express written permission of
* Resolve Systems, LLC.
*
******************************************************************************/

package com.resolve.gateway.pull.servicenow;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

public interface ObjectService
{
    /**
     * Every object has its ID property, the subclass requires to implement this
     * method. Example: Service request could be "NUMBER".
     * 
     * @return
     */
    //String getIdPropertyName();

    /**
     * Identity of the object. Implementing class provides this. Example:
     * Incident is "incident" or Problem is "problem"
     * 
     * @return
     */
    //String getIdentity();

    /**
     * Creates an object by supplying username, password.
     *
     * @param object type
     * @param params
     * @param username
     * @param p_assword
     * @return a Map with all the properties of the new object.
     * @throws Exception
     */
    Map<String, String> create(String objectType, Map<String, String> params, String username, String p_assword) throws Exception;

    /**
     * Creates an object by supplying username, password.
     *
     * @param object type
     * @param parentObjectId
     *            such as SysId for an Incident. It's mandatory.
     * @param note
     *            to be added.
     * @param username
     * @param p_assword
     * @return
     * @throws Exception
     */
    boolean createWorknote(String objectType, String parentObjectId, String note, String username, String p_assword) throws Exception;

    /**
     * Updates an object by supplying username, password.
     *
     * @param object type
     * @param objectId
     *            is mandatory.
     * @param params
     * @param username
     * @param p_assword
     * @return
     * @throws Exception
     */
    void update(String objectType, String objectId, Map<String, String> params, String username, String p_assword) throws Exception;

    /**
     * Deletes an object by its id by supplying username, password.
     *
     * @param object type
     * @param objectId
     *            is mandatory.
     * @param username
     * @param p_assword
     * @return
     * @throws Exception
     */
    void delete(String objectType, String objectId, String username, String p_assword) throws Exception;

    /**
     * Retrieves an object's data by its default id (sys_id).
     *
     * @param object type
     * @param objectId
     *            is mandatory
     * @param username
     * @param p_assword
     * @return
     * @throws Exception
     */
    Map<String, String> selectByID(String objectType, String objectId, String username, String p_assword) throws Exception;

    /**
     * Retrieves an object's data by its alternate id (for example, Incident
     * Number).
     * 
     * @param object type
     * @param alternate id (property) name
     * @param alternate id (property) value
     * @param username
     * @param p_assword
     * @return
     */
    Map<String, String> selectByAlternateID(String objectType, String altIDName, String altIDValue, String username, String p_assword) throws Exception;

    /**
     * Read data from the object service by supplying username, password.
     * 
     * @param object type
     * @param query
     * @param orderBy
     * @param isAscending
     *            if true the order by ascending.
     * @param username
     * @param p_assword
     * @return
     * @throws Exception
     */
    List<Map<String, String>> search(String objectType, String query, String orderBy, boolean isAscending, String username, String p_assword) throws Exception;

    /**
     * Read data from the object service with page limit.
     * 
     * @param object type
     * @param query
     * @param orderBy
     * @param isAscending
     *            if true the order by ascending.
     * @param pageLmit
     * @param username
     * @param p_assword
     * @return
     * @throws Exception
     */
    List<Map<String, String>> searchWithPageLimit(String objectType, String query, String orderBy, boolean isAscending, int pageLimit, String username, String p_assword) throws Exception;
    
    
    String getLatestIncidentNumber() throws Exception;
    
    String getServiceNowServerDateTime();
    
    
    /**
     * This method inserts/updates incoming data into a specified staging table and triggers transformation based on predefined transform maps in the import set table
     * @param stagingTableName Name of the import set table that is created in ServiceNow
     * @param params - Key Value pair of parameter names and their values.
     * @param username 
     * @param p_assword
     * @throws Exception
     */
    String importSetApi(String stagingTableName, HashMap<String, String> params, String username, String p_assword) throws Exception;

    Map<String, String> createIncident(Map<String, String> params, String username, String p_assword) throws Exception;

    boolean createIncidentWorknote(String parentSysId, String note, String username, String p_assword) throws Exception;

    void updateIncident(String sysId, Map<String, String> params, String username, String password) throws Exception;

    void updateObject(String objectType, String sysId, Map<String, String> params, String username, String p_assword) throws Exception;

    void deleteObject(String objectType, String sysId, String username, String p_assword)throws Exception;

    List<Map<String, String>> searchObjectWithPageLimit(String objectType, String query, String orderBy, boolean isAscending, int pageLimit, String username, String p_assword) throws Exception;
    
    
    public Map<String, String> createObject(String objectType, Map<String, String> params, String username, String password) throws Exception;
    
    public List<Map<String, String>> searchObject(String objectType, String query, String orderBy, boolean isAscending, String username, String p_assword) throws Exception;
    
    public Map<String, String> selectObjectByAlternateID(String objectType, String altIDName, String altIDValue, String username, String p_assword) throws Exception;
    
    public Map<String, String> selectObjectBySysId(String objectType, String sysId, String username, String p_assword) throws Exception;
    
    public List<Map<String, String>> searchIncident(String query, String username, String password) throws Exception;
    
    public void updateIncidentStatus(String sysId, String status, String username, String password) throws Exception;
    
    public void deleteIncident(String sysId, String username, String password) throws Exception;
    
    public Map<String, String> selectIncidentBySysId(String sysId, String username, String password) throws Exception;
    
    public Map<String, String> selectIncidentByNumber(String number, String username, String password) throws Exception;
    
    
    
 
}
