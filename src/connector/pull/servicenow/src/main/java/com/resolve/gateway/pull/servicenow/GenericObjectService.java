/******************************************************************************
* (C) Copyright 2014
*
* Resolve Systems, LLC
*
* All rights reserved
* This software is distributed under license and may not be used, copied,
* modified, or distributed without the express written permission of
* Resolve Systems, LLC.
*
******************************************************************************/

package com.resolve.gateway.pull.servicenow;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.GregorianCalendar;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.TimeZone;

import org.apache.commons.httpclient.HttpStatus;

import com.resolve.util.DateUtils;
import com.resolve.util.Log;
import com.resolve.util.StringUtils;
import com.resolve.util.restclient.RestCallException;
import com.resolve.gateway.util.rest.RestCaller;

import net.sf.json.JSONArray;
import net.sf.json.JSONObject;
import net.sf.json.JSONSerializer;


/**
 * Generic ServiceNow object service provider class.
 * Uses REST API
 * 
 * @author hemant.phanasgaonkar
 */
public class GenericObjectService implements ObjectService
{
    protected  Map<String,Object> configMap=null;
    
   // protected final RestCaller restCaller;

    //protected static final String OBJECT_CLASS = "class";
  
    RestCaller getRestCallerInstance()
    {
        RestCaller restCaller = new RestCaller((String) configMap.get("URL"));
        return restCaller;
    }
    public GenericObjectService(Map<String,Object> configMap)
    {
        this.configMap = configMap;
      
        boolean isMutualTlsEnabled = Boolean.parseBoolean((String) configMap.get("MUTUALTLSENABLED"));
        boolean isProxyEnabled = Boolean.parseBoolean((String) configMap.get("PROXYENABLED"));
        boolean isHeaderRedireEnabled = Boolean.parseBoolean((String) configMap.get("HEADERREDIRENABLE"));
       

        if (isProxyEnabled)
        {
            int port = -1;
            String proxyPort = (String) configMap.get("PROXYPORT");
            if (StringUtils.isNotBlank(proxyPort))
            {
                port = Integer.parseInt(proxyPort);
            }
            RestCaller.configProxyOverride((String) configMap.get("PROXYHOST"), port, (String) configMap.get("PROXYUSER"), (String) configMap.get("PROXYPASS"));
        }
        if (isMutualTlsEnabled)
        {
            RestCaller.configTLS(Boolean.parseBoolean((String) configMap.get("MUTUALTLSENABLED")), (String) configMap.get("MUTUALTLSKEYSTORE"), (String) configMap.get("MUTUALTLSKEYSTOREPASS"), (String) configMap.get("MUTUALTLSTRUSTSTORE"), (String) configMap.get("MUTUALTLSTRUSTSTOREPASS"));
        }
        if (isHeaderRedireEnabled)
        {
            RestCaller.configHeaderRedirect(Boolean.parseBoolean((String) configMap.get("HEADERREDIRENABLE")), (String) configMap.get("HEADERREDIRURL"), (String) configMap.get("HEADERREDIRHEADER"), Boolean.parseBoolean((String) configMap.get("HEADERREDIRENCAUTH")));
        }
      
    }
    
    protected void setupRestCaller(RestCaller restCaller,String objectType, String username, String p_assword)
    {
        
        if (StringUtils.isEmpty(username) || StringUtils.isEmpty(p_assword))
        {
            username = (String) configMap.get("HTTPBASICAUTHUSERNAME");
            p_assword =(String) configMap.get("HTTPBASICAUTHPASSWORD");
        }

        restCaller.setHttpbasicauthusername(username);
        restCaller.setHttpbasicauthpassword(p_assword);

        restCaller.setUrlSuffix(objectType);
    }
    
    @Override
    public final Map<String, String> create(String objectType, Map<String, String> objectProperties, String username, String p_assword) throws Exception
    {
        
        RestCaller restCaller = getRestCallerInstance();
        setupRestCaller(restCaller,objectType, username, p_assword);
        
        String response = null;
        
        try
        {
            response = restCaller.postMethod(null, null, objectProperties);
        }
        catch (RestCallException rce)
        {
            Log.log.debug("GenericObjectService.create failed due to : "  + rce.getMessage());
            
            if ((rce.statusCode == HttpStatus.SC_OK || (rce.statusCode == HttpStatus.SC_NO_CONTENT)) &&
                StringUtils.isNotBlank(rce.responseEntity) && rce.responseEntity.contains("sys_id"))
            {
                response = rce.responseEntity;
            }
            else
            {
                throw rce;
            }
        }

        return StringUtils.jsonObjectStringToMap("result", response);
    }

    @Override
    public boolean createWorknote(String objectType, String parentObjectId, String note, String username, String p_assword) throws Exception
    {
        boolean result = true;

        Map<String, String> objectProperties = new HashMap<String, String>();
        objectProperties.put("work_notes", note);
        update(objectType, parentObjectId, objectProperties, username, p_assword);

        return result;
    }

    @Override
    public void update(String objectType, String objectId, Map<String, String> objectProperties, String username, String p_assword) throws Exception
    {
        RestCaller restCaller = getRestCallerInstance();
        setupRestCaller(restCaller,objectType, username, p_assword);

        restCaller.setUrlSuffix(objectType);
        
        Map<String, String> reqHeaders = new HashMap<String, String>();
        reqHeaders.put("X-no-response-body", "true");
        
        restCaller.putMethod(objectId, null, reqHeaders, objectProperties);
    }

    @Override
    public void delete(String objectType, String objectId, String username, String p_assword) throws Exception
    {
        RestCaller restCaller = getRestCallerInstance();
        setupRestCaller(restCaller,objectType, username, p_assword);
        
        restCaller.deleteMethod(objectId);
    }

    @Override
    public Map<String, String> selectByID(String objectType, String objectId, String username, String p_assword) throws Exception
    {
        RestCaller restCaller = getRestCallerInstance();
        setupRestCaller(restCaller,objectType, username, p_assword);

        String response = restCaller.getMethod(objectId, new HashMap<String, String>(), null);

        Map<String, String> selectResult = StringUtils.jsonObjectStringToMap("result", response);
        
        if(selectResult == null || selectResult.isEmpty())
        {
            throw new Exception("Select " + objectType + " by " + "ServiceNowGateway.SYS_ID" + " = " + objectId + " did not return any results.");
        }
        
        return selectResult;
    }

    @Override
    public Map<String, String> selectByAlternateID(String objectType, String altIDName, String altIDValue, String username, String p_assword) throws Exception
    {
        RestCaller restCaller = getRestCallerInstance();
        setupRestCaller(restCaller,objectType, username, p_assword);

        Map<String, String> reqParams = new HashMap<String, String>();
        reqParams.put(altIDName, altIDValue);

        String response = restCaller.getMethod(null, reqParams, null);

        List< Map<String, String>> searchResults = StringUtils.jsonArrayStringToList("result",response);
        
        if(searchResults == null || searchResults.isEmpty() || searchResults.size() > 1)
        {
            throw new Exception("Search " + objectType + " by alternate id " + altIDName + " = " + altIDValue + " did not return any results or returned multiple results.");
        }
        
        return searchResults.get(0);
    }

    @Override
    public List<Map<String, String>> search(String objectType, String query, String orderBy, boolean isAscending, String username, String p_assword) throws Exception
    {
        int pageLimit =  Integer.parseInt((String) configMap.get("PAGELIMIT"));
        return searchWithPageLimit(objectType, query, orderBy, isAscending, Integer.parseInt((String) configMap.get("PAGELIMIT")), username, p_assword);
    }
    
    @Override
    public List<Map<String, String>> searchWithPageLimit(String objectType, String query, String orderBy, boolean isAscending, int pageLimit, String username, String p_assword) throws Exception
    {
        RestCaller restCaller = getRestCallerInstance();
        setupRestCaller(restCaller,objectType, username, p_assword);
            Map<String, String> reqParams = new HashMap<String, String>();
        StringBuilder qryBldr = new StringBuilder(query != null ? query : "");
        
        if (StringUtils.isNotEmpty(orderBy))
        {
            if(StringUtils.isNotEmpty(qryBldr.toString()))
            {
                qryBldr.append("^");
            }
                            
            if (isAscending)
            {
                qryBldr.append("ORDERBY" + orderBy);
            }
            else
            {
                qryBldr.append("ORDERBYDESC" + orderBy);
            }
        }
        
        if(StringUtils.isNotEmpty(qryBldr.toString()))
        {
            reqParams.put("sysparm_query", qryBldr.toString());
        }
        
        int offset = 0;
        boolean isAvailable = true;
        
        reqParams.put("sysparm_limit", Integer.toString(pageLimit > Integer.parseInt((String) configMap.get("PAGELIMIT")) ?  Integer.parseInt((String)configMap.get("PAGELIMIT")) : pageLimit));
        
        List<Map<String, String>> searchResults = new ArrayList<Map<String, String>>();
        
        while(isAvailable)
        {
            if(offset > 0)
            {
                reqParams.put("sysparm_offset", Integer.toString(offset));
            }
            
            String response = null;
            
            try
            {
                response = restCaller.getMethod(null, reqParams, null);
            }
            catch (RestCallException rce)
            {
               Log.log.debug("GenericObjectService.search failed due to : "  + rce.getMessage());
             
                if (rce.statusCode != HttpStatus.SC_OK && rce.statusCode != HttpStatus.SC_NOT_FOUND && 
                    StringUtils.isNotBlank(rce.responseEntity) && (rce.responseEntity.contains("sys_id") || rce.responseEntity.contains("error")))
                {
                    response = rce.responseEntity;
                }
                else
                {
                    throw rce;
                }
            }
            
            isAvailable = false;
            
            if(response != null && !response.isEmpty())
            {
                searchResults.addAll(StringUtils.jsonArrayStringToList("result",response));
                
                if(restCaller.getResponseHeaders().containsKey("Link") &&
                   restCaller.getResponseHeaders().get("Link").indexOf("rel=\"next\"") > 0 )
                {
                    isAvailable = true;
                    offset += Integer.parseInt((String)configMap.get("PAGELIMIT"));
                }
            }
        }

        return searchResults;
    }
    
    public String getLatestIncidentNumber() throws Exception
    {
        String response = "";
        String latestIncidentNumber = "";
        List<Map<String, String>> incidentMap=null;
        RestCaller restcaller = new RestCaller((String)configMap.get("URL")); 
        
        RestCaller restCaller = getRestCallerInstance();
        setupRestCaller(restCaller,"incident", "", "");
        
        Map<String, String> reqParams = new HashMap<String, String>();
        reqParams.put("sysparm_query", "ORDERBYDESC sys_created_on");
        reqParams.put("sysparm_limit", "1");
        reqParams.put("sysparm_offset", "0");

        try
        {
            response = restCaller.getMethod(null, reqParams, null);
            JSONObject jsonObj = (JSONObject) JSONSerializer.toJSON(response);
            if (jsonObj.containsKey("result"))
            {
                JSONArray incidentArray = (JSONArray) jsonObj.get("result");
                if (incidentArray.size() == 1)
                {
                    jsonObj = (JSONObject) incidentArray.get(0);
                    if (jsonObj.containsKey("number"))
                    {
                        latestIncidentNumber = jsonObj.getString("number");
                    }
                    else
                    {
                        throw new Exception("Error getting latest Incident Number from ServiceNow");
                    }
                }
                else
                {
                    throw new Exception("Error getting latest Incident Number from ServiceNow");
                }
            }
            else
            {
                throw new Exception("Error getting latest Incident Number from ServiceNow");
            }
        }
        catch (RestCallException rce)
        {
            Log.log.debug("GenericObjectService.search failed due to : "  + rce.getMessage());
            
            if (rce.statusCode != HttpStatus.SC_OK && rce.statusCode != HttpStatus.SC_NOT_FOUND && 
                StringUtils.isNotBlank(rce.responseEntity) && rce.responseEntity.contains("sys_id"))
            {
                response = rce.responseEntity;
            }
            else
            {
                Log.log.error("Error getting latest Incident Number from ServiceNow");
                throw rce;
            }
        }
       
        return latestIncidentNumber;

    }
    
    /**
     * This method attempts to acces the ServiceNow server for its current date
     * and time in GMT. There is a "Scripted Web Service" we need to create in
     * ServiceNow server for this. If we do not create the web service then we
     * will use whatever current Date and Time in the Resolve server which may
     * be out of sync with ServiceNow server.
     */
    public String getServiceNowServerDateTime()
    {
        String result = null;

      
        
        try
        {
            RestCaller restCaller = new RestCaller((String) configMap.get("DATETIMERESTSERVICEURL"));
        
            restCaller.setUrlSuffix("");
        
            String response = restCaller.getMethod((String) configMap.get("DATETIMERESTSERVICEURL"), (Map<String, String>)null);
            Map<String, String> serverDateTime = StringUtils.jsonObjectStringToMap("result", response);
            
            if (serverDateTime != null && !serverDateTime.isEmpty())
            {
                Calendar cal = new GregorianCalendar(TimeZone.getTimeZone("Etc/GMT+0"));
                cal.setTimeInMillis(Long.parseLong(serverDateTime.get("serverdatetime")));
                DateFormat df = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
                df.setCalendar(cal);
                result = df.format(cal.getTime());
                Log.log.info("serverdatetime : " + serverDateTime.get("serverdatetime") + 
                              ", ServiceNow Server Date Time : " + result);
            }
        }
        catch(Exception e)
        {
            Log.log.warn("Failed to get ServiceNow server date time using url " + 
                            (String) configMap.get("DATETIMEWEBSERVICENAME") + 
                         " with user id " + (String) configMap.get("HTTPBASICAUTHUSERNAME")+ 
                         ", Using this rsremote's local time as ServiceNow server date time!!!");
        }
        
        if (StringUtils.isEmpty(result))
        {
            Calendar now = Calendar.getInstance();
            TimeZone tz = now.getTimeZone();
            String tzStr = tz.getID();
            result = DateUtils.convertDateToGMTString(now.toString(), tzStr);
            Log.log.warn("Local Server Date Time set as ServiceNow server date time : " + result);
        }

        return result;
    }


    @Override
    public String importSetApi(String stagingTableName, HashMap<String, String> params, String username, String p_assword) throws Exception
    {
        // TODO Auto-generated method stub
                
      if(StringUtils.isEmpty(stagingTableName)) {
          throw new Exception("Staging Table Name is required.");
          
      }
      if((params == null) || (params.size()== 0)) {
          throw new Exception("Please provide the fields name and values in the params map.");
          
      }
        RestCaller restCaller = new RestCaller((String) configMap.get("IMPORTSETAPIURL"), username, p_assword);
       
        if (StringUtils.isEmpty(username) || StringUtils.isEmpty(p_assword))
        {
            username =(String)configMap.get("HTTPBASICAUTHUSERNAME");
            p_assword = (String)configMap.get("HTTPBASICAUTHPASSWORD");
        }

        restCaller.setHttpbasicauthusername(username);
        restCaller.setHttpbasicauthpassword(p_assword);
        restCaller.setUrlSuffix(stagingTableName);
        
        String response = null;
        
        try
        {
            response = restCaller.postMethod(null, null, params);
            
        }
        catch (RestCallException rce)
        {
            Log.log.debug("importSetApi call failed due to : "  + rce.getMessage());
            
            if (rce.statusCode == HttpStatus.SC_CREATED  &&
                StringUtils.isNotBlank(rce.responseEntity) && rce.responseEntity.contains("sys_id"))
            {
                response = rce.responseEntity;
               
            }
            else
            {
                throw rce;
            }
        }
        String formattedResponse = StringUtils.jsonObjectToString(StringUtils.stringToJSONObject(response));
        return formattedResponse;
        
    }
    
    
   //Generic ServiceNow Object CRUD methods
    
    public Map<String, String> createObject(String objectType, Map<String, String> params, String username, String password) throws Exception
    {
        return create(objectType, params, username, password);
    }
    
    public Map<String, String> selectObjectBySysId(String objectType, String sysId, String username, String password) throws Exception
    {
        return selectByID(objectType, sysId, username, password);
    }
    
    public Map<String, String> selectObjectByAlternateID(String objectType, String altIDName, String altIDValue, String username, String password) throws Exception
    {
        return selectByAlternateID(objectType, altIDName, altIDValue, username, password);
    }
    
    public List<Map<String, String>> searchObject(String objectType, String query, String orderBy, boolean isAscending, String username, String password) throws Exception
    {
        return search(objectType, query, orderBy, isAscending, username, password);
    }
    
    public void updateObject(String objectType, String sysId, Map<String, String> params, String username, String password) throws Exception
    {
        update(objectType, sysId, params, username, password);
    }
    
    public void deleteObject(String objectType, String sysId, String username, String password) throws Exception
    {
        delete(objectType, sysId, username, password);
    }
    
    public List<Map<String, String>> searchObjectWithPageLimit(String objectType, String query, String orderBy, boolean isAscending, int pageLimit, String username, String password) throws Exception
    {
        return searchWithPageLimit(objectType, query, orderBy, isAscending, pageLimit, username, password);
    }
    
  
    
    /*
     * Public API method implementation goes here
     */
    @Deprecated
    public Map<String, String> createIncident(Map<String, String> params, String username, String p_assword) throws Exception
    {
        return create("incident", params, username, p_assword);
    }

    @Deprecated
    public boolean createIncidentWorknote(String parentSysId, String note, String username, String p_assword) throws Exception
    {
        return createWorknote("incident", parentSysId, note, username, p_assword);
    }

    @Deprecated
    public void updateIncident(String sysId, Map<String, String> params, String username, String password) throws Exception
    {
        update("incident", sysId, params, username, password);
    }

    @Deprecated
    public void updateIncidentStatus(String sysId, String status, String username, String password) throws Exception
    {
        Map<String, String> params = new HashMap<String, String>();
        params.put("incident_state", status);
        update("incident", sysId, params, username, password);
    }

    @Deprecated
    public void deleteIncident(String sysId, String username, String password) throws Exception
    {
        delete("incident", sysId, username, password);
    }

    @Deprecated
    public Map<String, String> selectIncidentBySysId(String sysId, String username, String password) throws Exception
    {
        return selectByID("incident", sysId, username, password);
    }

    @Deprecated
    public Map<String, String> selectIncidentByNumber(String number, String username, String password) throws Exception
    {
        return selectByAlternateID("incident", "number", number, username, password);
    }

    @Deprecated
    public List<Map<String, String>> searchIncident(String query, String username, String password) throws Exception
    {
        return search("incident", query, null, false, username, password);
    }
    
    
}
