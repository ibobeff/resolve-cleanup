package com.resolve.gateway.pull.servicenow;

import java.util.Iterator;
import java.util.Map;

import com.resolve.gateway.resolvegateway.pull.ConfigReceivePullGateway;
import com.resolve.gateway.resolvegateway.pull.PullGateway;
import com.resolve.gateway.resolvegateway.pull.PullGatewayFilter;
import com.resolve.gateway.resolvegateway.pull.PullGatewayProperties;
import com.resolve.util.Log;
import com.resolve.util.StringUtils;

public class SERVICENOWGateway extends PullGateway
{
    private static volatile SERVICENOWGateway instance = null;
    public static volatile ObjectService genericObjectService;
    // Time out for ESB messages
    static final long SEND_TIMEOUT = 10 * 60 * 1000; // milliseconds

    // Specific condition to meet for querying/retriving events from third-party servers, i.e. Sample server, such as Moogsoft server
    // This query is used to pull back events/alerts/incidents from the remote server
    public static final String QUERY = "QUERY";

    // Optional last value fields that will be persisted into the database for each polling interval
    public static final String LAST_VALUE = "LAST_VALUE";
    public static final String LAST_TIMESTAMP = "LAST_TIMESTAMP";
    public final static String VAR_REGEX_CONNECTOR_VARIABLE = "\\$\\{[_A-Z]*}";
    // Optional flexible attributes for the specific gateway that to be persisted into the database
    public static final String LAST_VALUE_FIELD = "LAST_VALUE_FIELD";

    // Cached instance of the third-party server
    // Optional
    // static SampleServer server = new SampleServer();

    // Singleton constructor to take the blueprint configuration and initialize the deployed filters for this specific gateway
    // Do not modify
    private SERVICENOWGateway(ConfigReceivePullGateway config) {

        super(config);

        String gatewayName = this.getClass().getSimpleName();
        int index = gatewayName.indexOf("Gateway");
        if(index != -1) {
            name = gatewayName.substring(0, index);

            if(StringUtils.isBlank(name)) {
                Log.log.error("Not appropriate gateway name: " + gatewayName);
                return;
            }

            try {
                StringBuilder sb = new StringBuilder();
                PullGatewayProperties properties = ConfigReceivePullGateway.getGatewayProperties(name);
                sb.append(properties.getPackageName()).append(".").append(gatewayName).append("Impl");
                Class<?> implClass = Class.forName(sb.toString());
                impl = (SERVICENOWGatewayImpl)implClass.newInstance();
            } catch(Exception e) {
                Log.log.error(e.getMessage(), e);
            }
        }

        else {
            Log.log.error("Not appropriate gateway name: " + gatewayName);
            return;
        }

        Map<String, PullGatewayFilter> pullFilters = getPullFilters();

        for(Iterator<String> iterator=pullFilters.keySet().iterator(); iterator.hasNext();) {
            String filterName = iterator.next();
            PullGatewayFilter filter = pullFilters.get(filterName);

            if(name.indexOf(((PullGatewayFilter)filter).getGatewayName()) != -1) {
                if(filters.get(filterName) == null) {
                    filters.put(filterName, pullFilters.get(filterName));

                    if(filter.isDeployed()) {
                        orderedFilters.add(filter);
                        orderedFiltersMapById.put(filter.getId(), filter);
                    }
                }
            }
        }

        ConfigReceivePullGateway.getGateways().put(name, this);
    }

    // Public method to get an instance from this Singleton class
    // Do not modify
    public static SERVICENOWGateway getInstance(ConfigReceivePullGateway config) {

        if (instance == null) {
            instance = new SERVICENOWGateway(config);
        }

        return instance;
    }

    // This method is called after the instance is instantiated.
    // Do not modify
    public static SERVICENOWGateway getInstance() {

        if (instance == null) {
            throw new RuntimeException("SERVICENOW Pull Gateway is not initialized correctly..");
        } else {
            return instance;
        }
    }

    // Initialize the gateway with the default HTTP server instance and start to listen on the default port
    // Do not remove the existing code, but you can add additional logic for initialization if necessary
    @Override
    protected void initialize() {

        queue = config.getQueue().toUpperCase();
        

        try {
            Log.log.info("Initializing SERVICENOW Listener ...");
            this.gatewayConfigDir = "/config/" + queue.toLowerCase() + "/";

            PullGatewayProperties properties = ConfigReceivePullGateway.getGatewayProperties(name);
           Map<String,Object> configMap = properties.getAdditionalProperties();
            genericObjectService = new GenericObjectService(configMap) ;
            
           //SERVICENOWGatewayImpl.genericObjectService=(GenericObjectService) genericObjectService;
           SERVICENOWGatewayImpl.lastIncidentNumberMacroInitVal=genericObjectService.getLatestIncidentNumber();
           SERVICENOWGatewayImpl.lastChangeDateMacroInitVal=genericObjectService.getServiceNowServerDateTime();
           
            // Add additional custom logic here, e.g., start the Sample server etc.

        } catch (Exception e) {
            Log.log.error("Failed to initialize ServiceNow Server: " + e.getMessage(), e);
        }
    }

    // The gateway will start to run by calling the super class common logic to start the polling for each deployed filter
    @Override
    public void start() {

        Log.log.debug("Starting SERVICENOW Pull Gateway");

        super.start();
    }

 // This method will be called when the gateway is gracefully shutdown
    @Override
    public void stop() {

        Log.log.warn("Stopping SERVICENOW Pull gateway");

        // Add code for shutdown the Sample Server

        super.stop();
    }

    // This method will be called when a filter is deployed to validate the semantics of the field only
    // The syntax of the filter fields have been validated when the filter is defined and saved from UI
    // including whether the filter field is required or not per gateway definition in blueprint.properties
    @Override
    public int validateFilterFields(Map<String, Object> params) {

        int code = 0;

        // Add custom logic here

        return code;
    }

    // The method returns the error string that is associated with the error code defined
    // under the custom logic being implemented
    @Override
    public String getValidationError(int errorCode) {

		String error = null;

		// Add custom logic here

        return error;
    }

    // This method is used by the system
    // Do not modify
    @Override
    protected String getMessageHandlerName() {

        return this.getClass().getName();
    }

    // If the common logic in the super class does not support the use case for this specific gateway,
    // you may create override methods to replace the logic in the super class

} // class SERVICENOWGateway
