/**
 * Resolve LLC @ copyright 2017
 */
package com.resolve.gateway.pull.ansible;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.regex.Pattern;

import com.resolve.gateway.resolvegateway.ResolveGatewayUtil;
import com.resolve.gateway.resolvegateway.pull.PullGateway;
import com.resolve.gateway.resolvegateway.pull.PullGatewayFilter;
import com.resolve.gateway.resolvegateway.pull.PullGatewayInterface;
import com.resolve.util.Log;
import com.resolve.util.StringUtils;
// TODO: Auto-generated Javadoc

/**
 * The Class AnsibleGatewayImpl.
 *
 * <p>
 * <br>Class responsible for retrieving or pulling data from the external source (Product's DB, API  etc) </br>
 * The class provides the general template methods to achieve the following objective 
 * <ul>
 *      <li> Retrieving data from external sources:   method name -> retrieveData(PullGatewayFilter) </li>
 *      <li> Acknowledge message after processing data:   method name -> updateAck(List<Map<String, String>> results) </li>
 *      <li> Update the key of the last record update:   method name -> updateLastValues(PullGatewayFilter, List<Map<String, String>>) </li>
 *  </ul>
 *  </P>
 *  <br> The above methods should be called in the exact sequence to complete data pull and processing  for the gateway</br>
 *  <br> refer the method documentation for the right way to processing records   </br>
 */
public class AnsibleGatewayImpl implements PullGatewayInterface
{
  
    /** Optional ::   Define a pattern to match for building custom query. */
    private Pattern pattern;

    /**
     * The gateway class intatiates this class by calling this constructor, can only be called from the package.
     */
    AnsibleGatewayImpl() {

    }

	//
    /**
	 *  This is step I as mentioned in the class description
	 *  <p>
	 *  <br> The method is responsible for retrieving (pulling) data from an external source based on criterias set </br>
	 *   <br> in the filter (check the filter values in resolve filter created for this pull gateway)  </br>
	 *  <br> There are two typical ways to pull events from the third-party server:  </br>
	 *  <br> Usually is provided in the Query field of the filter. The field value is available through the <PullGatewayFilter> </br>
	 * <br> passed to this method. The query could be a regex pattern which easily be implemented using PATTERN </br>
	 *  <br>Custom query or by time bucket defined in the filter depending on the methods provided by the third-party product</br>
	 *  </p>
	 *  <p>
	 *  <br>Included is the template code for implementation using custom query and maintain</br>
	 *  <br>the last values, e.g., last id, last timestamp, or any last field values </br>
	 *  <br>The custom query may contain macros in the regex pattern of "\\$\\{[_A-Z]*}", it </br>
	 *  <br>can be built on the fly by calling buildCustomQuery() method in the super gateway class.</br>
	 *  <br>This method must be implemented, but the logic for query and update last value is optional. </br>
	 *  </p>
	 *  
	 *  <br>METHOD BODY :: </br>
	 *  
	 *       <br>       // This is the Query field entered in the filter field (Gateway filter)   </br>
     *    <br> String query = filter.getQuery(); </br>
            
     *   <br>    // Build the Map for creating the custom query later  </br>
     *    <br>   Map<String, String> fieldValuePairs = new HashMap<String, String>(); </br>
           <br> //  Every acknowledged or processed event is persisted in the DB so that in case anything fails </br>
           <br>  //    the filter doesn't refetch the already processed events avoiding duplication. The logic below  </br>
           <br>  //   retrieves the last saved unique id   </br>
     *    <br>   String lastValue = filter.getLastValue(); </br>
     *     <br>  if(StringUtils.isBlank(lastValue)) </br>
     *     <br>      lastValue = "0"; </br>
     *    <br>   fieldValuePairs.put(PullGatewayFilter.LAST_VALUE, lastValue); </br>

     *     <br> Map<String, Object> filterAttrs =  filter.getAttributes(); </br>
     *     <br>  fieldValuePairs.put(AnsibleGateway.LAST_VALUE_FIELD, (String)filterAttrs.get("ulastEntryId"));</br>

          <br>  //  Custom query for filtering the fetched records </br>
      *   <br>   String customQuery = buildCustomQuery(pattern, query, fieldValuePairs); </br>


          <br>  // TODO: Retrieve the events/alerts/incidents data from the third-party server   </br>
           <br> // result = AnsibleGatewayAPI.getInstance().getResult(customQuery); </br>
	 * @param filter the filter
	 * @return the list
	 * @throws Exception the exception
	 */
    public List<Map<String, String>> retrieveData(PullGatewayFilter filter) throws Exception {

        /** The output will be a List of Maps. If the Json response has multiple child objects create
        subsequent objects to hold that data and pass it on  */
        List<Map<String, String>> result = new ArrayList<Map<String, String>>();

        try {
            /** This is the Query field entered in the filter field (Gateway filter)  */
            String query = filter.getQuery();
            
            /** Build the Map for creating the custom query later  */
            Map<String, String> fieldValuePairs = new HashMap<String, String>();
            /**  Every acknowledged or processed event is persisted in the DB so that in case anything fails
             *    the filter doesn't refetch the already processed events avoiding duplication. The logic below 
             *    retrieves the last saved unique id  */ 
            String lastValue = filter.getLastValue();
            if(StringUtils.isBlank(lastValue))
                lastValue = "0";
            fieldValuePairs.put(PullGatewayFilter.LAST_VALUE, lastValue);

            Map<String, Object> filterAttrs =  filter.getAttributes();
            fieldValuePairs.put(AnsibleGateway.LAST_VALUE_FIELD, (String)filterAttrs.get("ulastEntryId"));

            /**  Custom query for filtering the fetched records*/
            String customQuery = buildCustomQuery(pattern, query, fieldValuePairs);

            Log.log.debug("Ansible Pull Gateway Query to execute:::" + customQuery);

            /** TODO: Retrieve the events/alerts/incidents data from the third-party server  */
            // result = AnsibleGatewayAPI.getInstance().getResult(customQuery);

        } catch(Exception e) {
            Log.log.error(e.getMessage(), e);
        }

        return result;
    }

    /**
     *  <br>This is step II as mentioned in the class description, this could be optional  </br>
     * <br> Usually products or external event, incident sources provide apis to acknowledge </br> 
     *   <br>   that the message has been received and processed
     *
     * @param results the results
     * @throws Exception the exception
     */
    public void updateAck(List<Map<String, String>> results) throws Exception {

    }

    /**
     *  <p>
     * This is step III as mentioned in the class description
     *  <br>Method responsible for updating the last value that was processed using the unique key to identify the message </br>
     * The value is persisted in the database. The default code persists the id field of the record
     * </p>
     *  <br>The method includes sample code to update the last value using PullGatewayutil method  </br>
     * 
     * <br> METHOD BODY:  </br>
     * 
     *  <br> // Sample code  for updating the last value to the DB </br>
     * <br> int size = results.size(); </br>
 
        <br> if(size == 0)  </br>
           <br>  return;  </br>
         <br>//Get the last record retrieved in the response  </br>
         <br>Map<String, String> last = results.get(size - 1); </br>
         <br>//Get the time stamp of the record  </br>
        Map<String, Object> filterAttrs = filter.getAttributes();

        <br> if(StringUtils.isNotEmpty(lastValue)) { </br>
          <br>   filter.setLastValue(lastValue); </br>
            <br> //Persists the time stamp as the last record's id in the DB </br>
            <br> ResolveGatewayUtil.updatePullGatewayFilter(filter);  </br>
       <br>  }  </br>
     * 
     * @param filter  The filter data displayed on the actual filter and created using blueprint proerties
     * @param results the results
     * @throws Exception the exception
     */
    public void updateLastValues(PullGatewayFilter filter, List<Map<String, String>> results) throws Exception {

        int size = results.size();

        if(size == 0)
            return;
        // Sample code  for updating the last value to the DB
        Map<String, String> last = results.get(size - 1);

        Map<String, Object> filterAttrs = filter.getAttributes();
        String lastValue = last.get(filterAttrs.get("ulastValueField"));

        Log.log.debug("The retrieved last value is " + lastValue);

        if(StringUtils.isNotEmpty(lastValue)) {
            filter.setLastValue(lastValue);
            ResolveGatewayUtil.updatePullGatewayFilter(filter,false);
        }
    }

   

    /**
     *  <br>This method usually gets the regex pattern that needs to be appllied to any filter fields.  </br>
     *  <br>The patterns can be read from the blueprint property file in the gateway class and set on this impl method </br>
     *  <br> and later used for filtering retrieved records
     * 
     * METHOD BODY:
     *   
     * this.pattern = Pattern.compile(pattern);
     *  Optional
     * @see com.resolve.gateway.resolvegateway.pull.PullGatewayInterface#setRegexPattern(java.lang.String)
     */
    
    public void setRegexPattern(String pattern) {

        this.pattern = Pattern.compile(pattern);
    }
    
   /**
    *  <br> Usually this is used if the pull filter has a Query filter filed. Additionally the regex defined in the previous </br>
    *  <br> Method can be used along side the query filed to filter out pulled records  </br>
    *
    * @param pattern the pattern
    * @param query the query
    * @param fieldValuePairs the field value pairs
    * @return the string
    */
    private String buildCustomQuery(Pattern pattern, String query, Map<String, String> fieldValuePairs){
    	return null;
    }
    
    /** <br> Add more methods for this specific gateway if you want to change the retrieval, acknowledgement or  </br>
     *  <br>update last value logic. Please refere the gateway manual and understand how the pull filter works  </br>
     *  <br>before adding any custom logic   */
    
} // class AnsibleGatewayImpl
