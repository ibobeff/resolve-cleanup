/**
 * Resolve LLC @ copyright 2017
 */
package com.resolve.gateway.pull.ansible;

import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import com.resolve.gateway.resolvegateway.pull.ConfigReceivePullGateway;
import com.resolve.gateway.resolvegateway.pull.PullGateway;
import com.resolve.gateway.resolvegateway.pull.PullGatewayFilter;
import com.resolve.gateway.resolvegateway.pull.PullGatewayInterface;
import com.resolve.gateway.resolvegateway.pull.PullGatewayProperties;
import com.resolve.rsremote.Main;
import com.resolve.util.Constants;
import com.resolve.util.Log;
import com.resolve.util.StringUtils;

/**
 * The Class AnsibleGateway.
 * <p>
     * <br> This class is responsible for creating/initializing the gateway on resolve. If the gateway's </br>
     * <br> properties are properly configured in the resolve's blueprint.properties file, then this class</br>
     *  <br> gets called from the main class of rsremote when rsremote starts up </br>
 *  </p>
 *  <p>
 *      <br> The class also has the standard start stop and heart beat methods to   </br>
 */
public class AnsibleGateway extends PullGateway
{
    
    /** The singleton instance. */
    private static volatile AnsibleGateway instance = null;

    /** Time out for ESB/MQ messages in milliseconds
     The Constant SEND_TIMEOUT. */
    static final long SEND_TIMEOUT = 10 * 60 * 1000; 

    /** Specific condition to meet for querying/retriving events from third-party servers, 
     * i.e. Sample server, such as Moogsoft server.
     * This query is used to pull back events/alerts/incidents from the remote server
     The Constant QUERY. */
    public static final String QUERY = "QUERY";

    /** Optional last value fields that will be persisted into the database for each polling interval
     The Constant LAST_VALUE. */
    public static final String LAST_VALUE = "LAST_VALUE";
    
    /** The Constant LAST_TIMESTAMP. */
    public static final String LAST_TIMESTAMP = "LAST_TIMESTAMP";

    /** Optional flexible attributes for the specific gateway that to be persisted into the database
     The Constant LAST_VALUE_FIELD. */
    public static final String LAST_VALUE_FIELD = "LAST_VALUE_FIELD";

    /** Cached instance of the third-party server
     Optional */
    // static SampleServer server = new SampleServer();

    /**
     * <p>Private constructor to take the blueprint configuration and initialize the deployed filters for this specific gateway
     * P.S. Do not modify the method modifier type
     * </p>
     * 
     *<br> SAMPLE METHOD BODY CODE:  </br>
     * 
     *<br> // Super class loads the deafult properties </br> 
     * <br>  super(config); </br>
     * <br>  // get the gateway name from this class </br>  
     *  <br> String gatewayName = this.getClass().getSimpleName(); </br>
     *   <br>int index = gatewayName.indexOf("Gateway"); </br>
     *  <br> // If the gateway is configured in blueprint.properties file   </br>
     *  <br> if(index != -1) { </br>
     *  <br>     name = gatewayName.substring(0, index); </br>
     *      
//      <br>      if(StringUtils.isBlank(name)) {  </br>
//      <br>          Log.log.error("Not appropriate gateway name: " + gatewayName);  </br>
//     <br>           return;  </br>
//     <br>       }  </br>
//            
//    <br>        try {             </br>
//    <br>            StringBuilder sb = new StringBuilder();           </br>
//     <br>           // Get all the configured gateway properties  </br>
//     <br>         PullGatewayProperties properties = (PullGatewayProperties)ConfigReceivePullGateway.  </br>
//                                         <br>                   getGatewayProperties(name);           </br>
//    <br>         sb.append(properties.getPackageName()).append(".").append(gatewayName).append("Impl");  </br>
//     <br>           //Dynamically instatitate the "impl" class for the gateway based on blueprint.property entries    </br>
//    <br>            Class<?> implClass = Class.forName(sb.toString());   </br>
//    <br>          impl = (AnsibleGatewayImpl)implClass.newInstance();  </br>
//     <br>       } catch(Exception e) {   </br>
//    <br>            Log.log.error(e.getMessage(), e);   </br>
//    <br>        }   </br>
//    <br>    }        </br>
//
//    <br>    else {   </br>
//    <br>        Log.log.error("Not appropriate gateway name: " + gatewayName);    </br>
//     <br>       return;             </br>
//    <br>    }  </br>
//
//     <br>   Map<String, PullGatewayFilter> pullFilters = getPullFilters();   </br>
//     <br>   //Standard logic to loop through the filter fields and populate the values to the map that can be used later </br>
//    <br>    for(Iterator<String> iterator=pullFilters.keySet().iterator(); iterator.hasNext();) {  </br>
//     <br>       String filterName = iterator.next();   </br>
//     <br>       PullGatewayFilter filter = pullFilters.get(filterName);   </br>
//
//   <br>         if(name.indexOf(((PullGatewayFilter)filter).getGatewayName()) != -1) {  </br>
//     <br>    // adding the filter properties to a map as key value pair   </br>
//    <br>            if(filters.get(filterName) == null) {   </br>
//     <br>               filters.put(filterName, pullFilters.get(filterName));  </br>
//      <br>              // Check if the filter is in the deployed state    </br>
//     <br>               if(filter.isDeployed()) {   </br>
//      <br>                  orderedFilters.add(filter);   </br>
//      <br>                  orderedFiltersMapById.put(filter.getId(), filter);  </br>
//     <br>               }    </br>
//     <br>           }    </br>
//      <br>      }        </br>
//      <br>  }       </br>
//
//        ConfigReceivePullGateway.getGateways().put(name, this);
     * 
     * @param config the config
     */
    private AnsibleGateway(ConfigReceivePullGateway config) {
        // Super class loads the deafult properties 
        super(config);
        // get the gateway name from this class  
        String gatewayName = this.getClass().getSimpleName();
        int index = gatewayName.indexOf("Gateway");
        // If the gateway is configured in blueprint.properties file  
        if(index != -1) {
            name = gatewayName.substring(0, index);
            
            if(StringUtils.isBlank(name)) {
                Log.log.error("Not appropriate gateway name: " + gatewayName);
                return;
            }
            
            try {
                StringBuilder sb = new StringBuilder();
                // Get all the configured gateway properties  
                PullGatewayProperties properties = (PullGatewayProperties)ConfigReceivePullGateway.
                                                            getGatewayProperties(name);
                sb.append(properties.getPackageName()).append(".").append(gatewayName).append("Impl");
                //Dynamically instatitate the "impl" class for the gateway based on blueprint.property entries  
                Class<?> implClass = Class.forName(sb.toString());
                impl = (AnsibleGatewayImpl)implClass.newInstance();
            } catch(Exception e) {
                Log.log.error(e.getMessage(), e);
            }
        }

        else {
            Log.log.error("Not appropriate gateway name: " + gatewayName);
            return;
        }

        Map<String, PullGatewayFilter> pullFilters = getPullFilters();
        //Standard logic to loop through the filter fields and populate the values to the map that can be used later 
        for(Iterator<String> iterator=pullFilters.keySet().iterator(); iterator.hasNext();) {
            String filterName = iterator.next();
            PullGatewayFilter filter = pullFilters.get(filterName);

            if(name.indexOf(((PullGatewayFilter)filter).getGatewayName()) != -1) {
         // adding the filter properties to a map as key value pair 
                if(filters.get(filterName) == null) {
                    filters.put(filterName, pullFilters.get(filterName));
                    // Check if the filter is in the deployed state  
                    if(filter.isDeployed()) {
                        orderedFilters.add(filter);
                        orderedFiltersMapById.put(filter.getId(), filter);
                    }
                }
            }
        }

        ConfigReceivePullGateway.getGateways().put(name, this);
    }

    /**
     * Public method to get an instance from this Singleton class
     * Do not modify
     * @param config the config
     * @return single instance of AnsibleGateway
     */
    public static AnsibleGateway getInstance(ConfigReceivePullGateway config) {

        if (instance == null) {
            instance = new AnsibleGateway(config);
        }
        return instance;
    }

    /**
     * <br> Gets the single instance of AnsibleGateway. Usually rsremote keeps a single instance </br>
     * <br> this gateway class and always use this method to get the classes instance </br>
     *        Do not modify
     * @return single instance of AnsibleGateway
     */
    public static AnsibleGateway getInstance() {

        if (instance == null) {
            throw new RuntimeException("Ansible Gateway is not initialized correctly..");
        } else {
            return instance;
        }
    }

    
    /**
     *  <br>Initialize the gateway with the blueprint property entries, mostly the the queue the gateway will </br>
     *  <br> be using to communicate with Resolve. </br>
     *  
     *  SAMPLE METHOD BODY CODE:
     *  
     *   <br>       //Get the queue name from the blueprint.properties file </br>
     *    <br>      queue = config.getQueue().toUpperCase();  </br>

       <br> try {  </br>
      <br>      Log.log.info("Initializing Ansible Listener ...");   </br>
       <br>     this.gatewayConfigDir = "/config/" + queue.toLowerCase() + "/";  </br>
        <br>    // Load the propertiees from the config folder config.xml file   </br>
        <br>    PullGatewayProperties properties = (PullGatewayProperties)ConfigReceivePullGateway.getGatewayProperties(name); </br>
                                                                    
          <br>  // Add additional custom logic here, e.g., start the Sample server etc. </br>

        <br>} catch (Exception e) {  </br>
         <br>   Log.log.error("Failed to initialize RemedyX Server: " + e.getMessage(), e); </br>
        }
     *  @see rsremote.receive.zenoss.queue property on blueprint.properties
     * @see com.resolve.gateway.resolvegateway.pull.PullGateway#initialize()
     * IMP Do not remove the existing code, but you can add additional logic for initialization if necessary
     */
    @Override
    protected void initialize() {

        queue = config.getQueue().toUpperCase();

        try {
            Log.log.info("Initializing Ansible Listener ...");
            this.gatewayConfigDir = "/config/" + queue.toLowerCase() + "/";
            // Load the propertiees from the config folder config.xml file 
            PullGatewayProperties properties = (PullGatewayProperties)ConfigReceivePullGateway.
                                                                    getGatewayProperties(name);

            // Add additional custom logic here, e.g., start the Sample server etc. 

        } catch (Exception e) {
            Log.log.error("Failed to initialize RemedyX Server: " + e.getMessage(), e);
        }
    }

    /**
     *  <br>The gateway will start to run by calling the super class common logic to start the polling for each deployed filter</br>
     * @see com.resolve.gateway.BaseClusteredGateway#start()
     */
    @Override
    public void start() {

        Log.log.debug("Starting Ansible Gateway");

        super.start();
    }

	/**
	 * This method will be called when the gateway is gracefully shutdown
	 * @see com.resolve.gateway.resolvegateway.pull.PullGateway#stop()
	 */
    @Override
    public void stop() {

        Log.log.warn("Stopping Ansible gateway");

        /** Add code for shutdown the Sample Server  */

        super.stop();
    }

    /**
     *<br> This method will be called when a filter is deployed to validate the semantics of the field only</br>
     *<br>The syntax of the filter fields have been validated when the filter is defined and saved from UI </br>
     *
     *<br> SAMPLE METHOD BODY CODE:  </br>
     *
        <br> int code = 0; </br>

        <br> // Add custom logic here for validating a specific filter field  </br>  

        <br> return code;  </br>
     * @see com.resolve.gateway.resolvegateway.ResolveGateway#validateFilterFields(java.util.Map)
     * including whether the filter field is required or not per gateway definition in blueprint.properties
     */
    // 
    @Override
    public int validateFilterFields(Map<String, Object> params) {

        int code = 0;

        // Add custom logic here for validating a specific filter field  

        return code;
    }

    /**
     * <br>The method returns the error string that is associated with the error code defined. Is responsible</br>
     * <br> for doing the custom validation for filter fileds </br>
     * 
     * <br> METHOD BODY:  </br>
     *     <br> String error = null; </br>

       <br> // Add logic to map and throw user definer error messages here  </br>

        <br> return error; </br>
     * @see com.resolve.gateway.resolvegateway.ResolveGateway#getValidationError(int)
     */
    @Override
    public String getValidationError(int errorCode) {

		String error = null;

		// Add logic to map and throw user definer error messages here 

        return error;
    }
    /**
     * <br> This Method is absolutely required by the Gateway license service </br>
     * <br> Usually the Queue name is the same as the license code </br>
     * @return Gateway License code name
     */
    @Override
    public String getLicenseCode()
    {
        return "TEMPLATE";
    }
    

    /** If the common logic in the super class does not support the use case for this specific gateway,
      you may create additional  methods to replace or override existing method logic from the super class  */

} // class AnsibleGateway
