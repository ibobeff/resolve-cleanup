/**
 * Resolve LLC @ copyright 2017
 */
package com.resolve.gateway.pull.builder;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import com.resolve.gateway.resolvegateway.ResolveGatewayUtil;
import com.resolve.gateway.resolvegateway.pull.PullGatewayFilter;
import com.resolve.gateway.resolvegateway.pull.PullGatewayInterface;
import com.resolve.groovy.script.GroovyScript;
import com.resolve.rsbase.MainBase;
import com.resolve.util.Constants;
import com.resolve.util.CryptUtils;
import com.resolve.util.Log;

import groovy.lang.Binding;
import net.sf.json.JSONObject;

/**
 * Not implemented
 *
 * @author Resolve LLC
 *
 */
public class BuilderGatewayImpl implements PullGatewayInterface {

	private int TASK_TIMEOUT = 60 * 1000; // 60 seconds
  /**
   * The gateway class intatiates this class by calling this constructor, can only be called from
   * the package.
   */
  BuilderGatewayImpl() {}

/**
 * <br> Method responsible for retrieving external data provided in the groovy gateway </br>
 * <br> script field (gscript) field. 
 */
  @Override
  public List<Map<String, String>> retrieveData(PullGatewayFilter filter) throws Exception {
	  List<Map<String, String>> resultList = new ArrayList<Map<String, String>>();
	  Log.log.debug("GatewayBuilder: Initializing the groovy bindings");
	  //Create the Groovy binding
	  Binding binding = new Binding();
      binding.setVariable(Constants.GROOVY_BINDING_LOG, Log.log);
      binding.setVariable(Constants.GROOVY_BINDING_ESB, MainBase.esb);
      //Get the name values defined by tge params on the screen
      Map<String, Object> nameValAttrs = filter.getAttributes();

//      Log.log.debug("GroovyGateway: Received the user defined fields --> " + nameValAttrs.toString());
      Map<String, String> usrNamValMap = extractJsonData(nameValAttrs);
      usrNamValMap.put("RS_LAST_ID", filter.getLastId());
      usrNamValMap.put("RS_LAST_VALUE", filter.getLastValue());
      usrNamValMap.put("RS_LAST_TIMESTAMP", filter.getLastTimestamp());
      //Add the attributes defined on the front end to the script data
      binding.setVariable(Constants.GROOVY_BINDING_INPUTS, usrNamValMap);
      //The Script 
      String script = (String)nameValAttrs.get("ugscript");
      try
      {               
    	  Log.log.debug("GatewayBuilder: Executing Groovy script --> " + script);
          Object obj = GroovyScript.thread(script, filter.getUsername(), true, binding, TASK_TIMEOUT);
          //Make sure the return type is List<Map<String, String>> only
          if (obj instanceof List<?>) {
        	  Log.log.debug("GatewayBuilder: Groovy execution fetch results");
        	  resultList = (List<Map<String, String>>) obj;
          } else {
        	  resultList = null;
        	  Log.log.error("GatewayBuilder: The Return value of the groovy script should be in "
        	  		+ "List<Map<String, String>> format");
          }
      }
      catch (Exception ex)
      {
         //Set the resultList to NULL and send the response back
    	  resultList = null;
    	  Log.log.error("GatewayBuilder: Error Retirieving data", ex);    
      }
    return resultList;
  }

  
  /**
   * <br> The method responsible for updating the last value to the DB </br>
   * <br> Gateway users can provide any last value field they wish to be saved </br>
   * <br> The Map should contain
   */
  @Override
  public void updateLastValues(PullGatewayFilter filter, List<Map<String, String>> results)
      throws Exception {
	  Map<String, String> lastValMap;
	  
	  try {
		  if(results != null && results.size() > 0) {
			  int size = results.size();
			  lastValMap = results.get(size - 1);
			  Log.log.debug("GatewayBuilder: Updating last value field after retrieving data");
			  if(lastValMap != null && !lastValMap.isEmpty() ) {
				  String lastId = lastValMap.get("RS_LAST_ID");
				  if(lastId != null) {
					  Log.log.debug("GatewayBuilder: Last id found in the received record, Storing "
					  + "it in the DB with the filter as last id --> " + lastId);
					  filter.setLastId(lastId);
				  }
				  String lastVal = lastValMap.get("RS_LAST_VALUE");
				  if(lastVal != null) {
					  Log.log.debug("GatewayBuilder: Last Value found in the received record, Storing "
					 + "it in the DB with the filter as last value --> " + lastVal);
					  filter.setLastValue(lastVal);
				  }
				  String lastTimeStmp = lastValMap.get("RS_LAST_TIMESTAMP");
				  if(lastTimeStmp != null) {
					  Log.log.debug("GatewayBuilder: Last Timestamp found in the received record, Storing "
					+ "it in the DB with the filter as last Timestamp --> " + lastTimeStmp);
					  filter.setLastTimestamp(lastTimeStmp);
				  }
				  Log.log.debug("GatewayBuilder: Persisiting the last values to the DB");
				  //Update the last value in the DB
				  ResolveGatewayUtil.updatePullGatewayFilter(filter,false);
			  }else {
				  Log.log.debug("GatewayBuilder: Last record doesn't have data");
			  }
		    }
	  }catch(Exception ex) {
		  Log.log.error("GatewayBuilder: ERROR Updating last value in DB", ex);
	  }
	  
  }

  /**
   * Not implemented
   */
  @Override
  public void setRegexPattern(String pattern) {}

  /**
   * Not implemented
   */
  @Override
  public void updateAck(List<Map<String, String>> results) throws Exception {}
  /*
   * Utility method to convert JSON to MAP and extract the user provided values
   */
  private Map<String,String> extractJsonData(Map<String, Object> jsonData){
	  JSONObject jsonObj = null;
	  Map<String, String> paramMap = new HashMap<String, String>();
	  try {
		  if(jsonData != null) {
			jsonObj = JSONObject.fromObject(jsonData);
		  	Iterator<String> keys = jsonObj.keys();
		  	String mapKey = null;
		  	String mapVal = null;
		  	while(keys.hasNext()) {
		  		String key = keys.next();
		  		String val = jsonObj.getString(key);
		  		if(val != null && !val.equalsIgnoreCase("null") && !key.equalsIgnoreCase("ugscript")) {
			  		JSONObject objVals = JSONObject.fromObject(jsonObj.getString(key));
			  		Iterator<String> keyz = objVals.keys();
			  		while(keyz.hasNext()) {
			  			String keyn = keyz.next();
				  		if(keyn.equalsIgnoreCase("label")) {
				  			mapKey = objVals.getString(keyn).trim();
				  		}else if(keyn.equalsIgnoreCase("value")) {
				  			mapVal = objVals.getString(keyn).trim();
				  		}else if(keyn.equalsIgnoreCase("encrypt") && 
				  				objVals.getString(keyn).equalsIgnoreCase("true")) {
				  			mapVal = CryptUtils.decrypt(mapVal);
				  		}
				  		if(mapKey != null && mapVal != null) {
				  			paramMap.put(mapKey, mapVal);
				  		}
			  		}
		  		}
		  	}
		  }
	  }catch(Exception ex) {
		  Log.log.error("GatewayBuilder: Error when parsing results");
	  }
	  return paramMap;
  }

}
