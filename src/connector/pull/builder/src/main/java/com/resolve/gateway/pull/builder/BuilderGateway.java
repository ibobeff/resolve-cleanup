/**
 * Resolve LLC @ copyright 2017
 */
package com.resolve.gateway.pull.builder;

import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

import com.resolve.gateway.Filter;
import com.resolve.gateway.resolvegateway.pull.ConfigReceivePullGateway;
import com.resolve.gateway.resolvegateway.pull.PullGateway;
import com.resolve.gateway.resolvegateway.pull.PullGatewayFilter;
import com.resolve.gateway.resolvegateway.pull.PullGatewayProperties;
import com.resolve.util.Log;
import com.resolve.util.StringUtils;


/**
 *
 * @author Resolve Systems LLC
 *
 */
public class BuilderGateway extends PullGateway {

  /* The name of the gateway */
  private String gatewayName = gatewayClassName.substring(0, gatewayClassName.length() - 7);

  private static volatile BuilderGateway instance = null;
  
  /** Time out for ESB/MQ messages in milliseconds The Constant SEND_TIMEOUT. */
  static final long SEND_TIMEOUT = 10 * 60 * 1000;

  private ConfigReceivePullGateway pullConfig;
  private BuilderGateway(ConfigReceivePullGateway config) {
	  
    super(config);
    this.pullConfig = config;
    // Get all the configured gateway properties from blueprint
    PullGatewayProperties properties = ConfigReceivePullGateway.getGatewayProperties(gatewayName);
    //Get the Name of the gateway
    String gatewayName = this.getClass().getSimpleName();
    int index = gatewayName.indexOf("Gateway");
    if(index != -1) {
        name = gatewayName.substring(0, index);
        if(StringUtils.isBlank(name)) {
            Log.log.error("Not appropriate gateway name: " + gatewayName);
            return;
        }
    }
    if (properties == null) {
      Log.log.error(
          super.gatewayClassName + ":There was an error reading gateway properties from blueprint");
    }

    // initialize parent class impl reference
    super.impl = new BuilderGatewayImpl();

    // set up BaseClusteredGateway with deployed and undeployed filters retreived from database
    this.setUpFilters(super.getPullFilters());

    // register this gateway instance with ConfigReceivePullGateway gateways map
    ConfigReceivePullGateway.getGateways().put(gatewayName, this);
    
    //Get the existing deployed filters and make sure you add them the list of deployed filters
    Map<String, PullGatewayFilter> pullFilters = getPullFilters();
    for(Iterator<String> iterator=pullFilters.keySet().iterator(); iterator.hasNext();) {
        String filterName = iterator.next();
        PullGatewayFilter filter = pullFilters.get(filterName);
        if(name.indexOf(((PullGatewayFilter)filter).getGatewayName()) != -1) {
            if(filters.get(filterName) == null) {
                filters.put(filterName, pullFilters.get(filterName));
                if(filter.isDeployed()) {
                    orderedFilters.add(filter);
                    orderedFiltersMapById.put(filter.getId(), filter);
                }
            }
        }
    }
  }

  /*
   * Set up filters map in BaseClusteredGateway class with filters retreived from the database Also
   * add filters marked as deployed in DB to orderedFilters list and orderedFiltersMapById in
   * BaseClusteredGateway
   */
  private void setUpFilters(Map<String, PullGatewayFilter> pullFilters) {

    for (String filterName : pullFilters.keySet()) {

      PullGatewayFilter filter = pullFilters.get(filterName);

      if (gatewayClassName.equals(filter.getGatewayName())) {

        Log.log.debug(gatewayClassName + ":Retrieving filter " + filterName + "from configuration");
        super.filters.put(filterName, pullFilters.get(filterName));

        if (filter.isDeployed()) {
          super.orderedFilters.add(filter);
          super.orderedFiltersMapById.put(filter.getId(), filter);
        }
        Log.log.debug(gatewayClassName + ":" + filterName + "retrieved successfully");
      }
    }
  }

  /**
   * Public method to get an instance from this Singleton class Do not modify
   *
   * @param config the config
   * @return single instance of GroovyGateway
   */
  public static BuilderGateway getInstance(ConfigReceivePullGateway config) {

    if (instance == null) {
      instance = new BuilderGateway(config);
    }
    return instance;
  }

  /**
   * <br>
   * Gets the single instance of GroovyGateway. Usually rsremote keeps a single instance </br>
   * <br>
   * this gateway class and always use this method to get the classes instance </br>
   * Do not modify
   *
   * @return single instance of GroovyGateway
   */
  public static BuilderGateway getInstance() {

    if (instance == null) {
      throw new RuntimeException(
          "GroovyGateway:Groovy Gateway was not instantiated.Check blueprint and config.xml");
    } else {
      return instance;
    }
  }


  /*
   * Initialize the gateway with the blueprint property entries, including the queue the gateway
   * will be using to communicate with Resolve.
   */
  @Override
  protected void initialize() {
	 Log.log.debug("*************GatewayBuilder***************");
	 Log.log.debug("BuilderGateway: Initialing groovy gateway");
	 Log.log.debug("*************GatewayBuilder***************");
	 //Don't remove this is required 
	 ConfigReceivePullGateway.getGateways().put(gatewayName, this);
    super.queue = super.config.getQueue().toUpperCase();
    queue = config.getQueue().toUpperCase();
    if (queue == null) {
      Log.log.error(
          gatewayClassName + ":Queue name was not set correctly.Check blueprint and/or config.xml");
    }
  }

  /**
   * <br>
   * The gateway will start to run by calling the super class common logic to start the polling for
   * each deployed filter</br>
   *
   * @see com.resolve.gateway.BaseClusteredGateway#start()
   */
  @Override
  public void start() {

    Log.log.debug(gatewayClassName + ": Starting Builder Gateway");

    super.start();

    Log.log.debug(gatewayClassName + ":Started");
  }

  /**
   * This method will be called when the gateway is gracefully shutdown
   *
   * @see com.resolve.gateway.resolvegateway.pull.PullGateway#stop()
   */
  @Override
  public void stop() {

    Log.log.warn(gatewayClassName + ":Stopping Builder Gateway");

    super.stop();

    Log.log.info(gatewayClassName + ":Stopped Builder Gateway");

  }
  @Override
  public void reinitialize() {
  	super.reinitialize();
  	Log.log.info(gatewayName + " --> reinitialization started, Queue name --> " + queue);
  	pullConfig.loadPullGatewayFilters(gatewayName, queue);
  	//this.getPullFilters().clear();
  	 Map<String, PullGatewayFilter> pullFilters = getPullFilters();
  	 orderedFilters.clear();
     for(Iterator<String> iterator=pullFilters.keySet().iterator(); iterator.hasNext();) {
         String filterName = iterator.next();
         PullGatewayFilter filter = pullFilters.get(filterName);
         Log.log.info(" Filter name " + filterName);
         orderedFilters.add(filter);
         orderedFiltersMapById.put(filter.getId(), filter);
     }

      initialize();
  }
  @Override
  public String getLicenseCode()
  {
      return "BUILDER";
  }

  /**
   * Required for resolution routing to work. The queue name is sent to  
   * the rscontrol by calling this mathod
   */
  @Override
  public String getQueueName()
  {
      return queue + getOrgSuffix();
  }
} // class GroovyGateway
