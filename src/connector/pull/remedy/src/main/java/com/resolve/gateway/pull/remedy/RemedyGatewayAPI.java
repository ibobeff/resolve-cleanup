/**
 * Resolve LLC @ copyright 2017
 */
package com.resolve.gateway.pull.remedy;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;

import com.bmc.arsys.api.ARServerUser;
import com.bmc.arsys.api.Entry;
import com.bmc.arsys.api.Field;
import com.bmc.arsys.api.OutputInteger;
import com.bmc.arsys.api.QualifierInfo;
import com.bmc.arsys.api.SortInfo;
import com.bmc.arsys.api.Timestamp;
import com.bmc.arsys.api.Value;

import com.resolve.gateway.AbstractGatewayAPI;
import com.resolve.gateway.Filter;
import com.resolve.gateway.resolvegateway.pull.PullGatewayFilter;
import com.resolve.util.Log;
import com.resolve.util.StringUtils;

import net.sf.json.JSONArray;
import net.sf.json.JSONObject;

public class RemedyGatewayAPI extends AbstractGatewayAPI
{
    private static String HOSTNAME = "";
    private static String PORT = "";
    private static String USERNAME = "";
    private static String P_ASSWORD = "";

    private static RemedyGateway gateway = RemedyGateway.getInstance();
    
    public static RemedyGatewayAPI instance = null;
    
    private static Map<String, Object> systemProperties = null;
    
    private String formName = null;
    private String fieldList = null;
    
    private PullGatewayFilter filter = null;

    public RemedyGatewayAPI() {
        
        try {
            systemProperties = gateway.loadSystemProperties("Remedy");
            
            HOSTNAME = (String)systemProperties.get("HOST");
            PORT = (String)systemProperties.get("PORT");
            USERNAME = (String)systemProperties.get("USER");
            P_ASSWORD = (String)systemProperties.get("PASS");
        } catch(Exception e) {
          Log.log.error("RemedyGateway: " + e.getMessage(), e);
        }
    }
    
    public RemedyGatewayAPI(String filterId) throws Exception {
        
        Map<String, Filter> filters = gateway.getFilters();
        PullGatewayFilter filter = (PullGatewayFilter)filters.get(filterId);
        
        if(filter == null)
            throw new Exception("RemedyGateway: Filter not found or not deployed with filter id: " + filterId);
        
        this.filter = filter;

        Map<String, Object> attrs = filter.getAttributes();
        
        if(attrs != null) {
            for(Iterator<String> it=attrs.keySet().iterator(); it.hasNext();) {
                String key = it.next();
                
                switch(key) {
                    case "uformName":
                        formName = (String)attrs.get(key);
                        break;
                    case "ufieldList":
                        fieldList = (String)attrs.get(key);
                        break;
                    default:
                        break;
                }
            }
        }
        
        Log.log.debug("RemedyGateway:  uformName = " + formName);
        Log.log.debug("RemedyGateway: ufieldlist = " + fieldList);
    }
    
    // Connect the current user to the server.
    private ARServerUser connect(String username, String password) throws Exception
    {
        Log.log.debug("RemedyGateway: Connecting to AR Server...");
        
        ARServerUser server = gateway.server;

        try
        {
            if (StringUtils.isNotEmpty(username))
            {
                server.setUser(username);
                server.setPassword(password);
            }

            //at this point verify if the server is still available
            //the getServerVersion call is simply to see if a connection is
            //available to the server.
            
            String serverVersion = server.getServerVersion();
            
            if(StringUtils.isBlank(serverVersion))
                throw new Exception("RemedyGateway: Cannot establish a network connection to the AR System server; Connection refused: " + server.getServer() + ":" + server.getPort());
            else
                Log.log.debug("RemedyGateway: AR System server version: " + serverVersion);
            
            //now that the server is available verify the user
            server.verifyUser();
        }
        catch (Exception e)
        {
            // This exception is triggered by a bad server, password or,
            // if guest access is turned off, by an unknown username.
            Log.log.error(e.getMessage(), e);
            throw new Exception(e.getMessage(), e);
        }
        
        Log.log.debug("RemedyGateway: Connected to AR Server " + server.getServer());
        
        return server;
    }

    private void disconnect(ARServerUser server)
    {
        if (server != null)
        {
            // Logout the user from the server. This releases there source
            // allocated on the server for the user.
            server.logout();
            Log.log.debug("RemedyGateway: User logged out.");
        }
    }
    
    public String create(String form, String fieldValueStr, String username, String password) throws Exception
    {
        Map<String, String> fieldValues = StringUtils.stringToMap(fieldValueStr, "=", "&");
        return create(form, fieldValues, username, password);
    } // create

    public String create(String formName, Map<String, String> data, String username, String password) throws Exception
    {
        String entryId = "";

        ARServerUser server = null;

        try
        {
            server = connect(username, password);
            Entry newEntry = new Entry();
            for (String key : data.keySet())
            {
                int intKey = Integer.valueOf(key);
                newEntry.put(intKey, new Value(data.get(key)));
            }

            entryId = server.createEntry(formName, newEntry);
            Log.log.debug("RemedyGateway: Entry create in the form " + formName + " with EntryId " + entryId);
        }
        catch (Exception e)
        {
            Log.log.error("RemedyGateway: Could not create the form " + formName);
            throw e;
        }
        finally
        {
            disconnect(server);
        }

        return entryId;
    }

    public void update(String formName, String entryId, String fieldValueStr, String username, String password) throws Exception
    {
        Map<String, String> fieldValues = StringUtils.stringToMap(fieldValueStr, "=", "&");
        update(formName, entryId, fieldValues, username, password);
    } // update

    public void update(String formName, String entryId, Map<String, String> data, String username, String password) throws Exception
    {
        ARServerUser server = null;

        try
        {
            server = connect(username, password);
            
            Entry entry = new Entry();
            for (String key : data.keySet())
            {
                int intKey = Integer.valueOf(key);
                entry.put(intKey, new Value(data.get(key)));
            }

            server.setEntry(formName, entryId, entry, null, 0);
            Log.log.debug("RemedyGateway: Entry updated successfully");
        }
        catch (Exception e)
        {
            Log.log.error("RemedyGateway: Entry could not be updated with for the form " + formName + " with EntryId " + entryId);
            throw e;
        }
        finally
        {
            disconnect(server);
        }
    }

    public void updateStatus(String formName, String entryId, String status, String username, String password) throws Exception
    {
        Map<String, String> data = new HashMap<String, String>();
        data.put("" + com.bmc.arsys.api.Constants.AR_CORE_STATUS, status);
        update(formName, entryId, data, username, password);
    }

    public void delete(String formName, String entryId, String username, String password) throws Exception
    {
        ARServerUser server = null;

        try
        {
            server = connect(username, password);

            server.deleteEntry(formName, entryId, 0);
        }
        catch (Exception e)
        {
            Log.log.error("RemedyGateway: Could not delete entry for the form " + formName + " with EntryId " + entryId);
            throw e;
        }
        finally
        {
            disconnect(server);
        }
    }

    public Map<String, String> selectByEntryID(String formName, String entryId, String username, String password) throws Exception
    {
        Map<String, String> result = new HashMap<String, String>();

        ARServerUser server = null;

        try
        {
            server = connect(username, password);

            Entry entry = server.getEntry(formName, entryId, null);
            if (entry == null)
            {
                Log.log.debug("RemedyGateway: No data found for ID#" + entryId);
                return result;
            }
            else
            {
                // Retrieve all properties of fields in the entry.
                Set<Integer> fieldIds = entry.keySet();
                for (Integer fieldId : fieldIds)
                {
                    // Field field = server.getField(formName,
                    // fieldId.intValue());
                    Value value = entry.get(fieldId);
                    // RemedyFormField remedyFormField = new
                    // RemedyFormField(fieldId, field.getName().toString(),
                    // field.getFieldType(), field.getDataType(),
                    // value.toString());
                    // result.add(remedyFormField);
                    result.put("" + fieldId, value.toString());
                }
            }
        }
        catch (Exception e)
        {
            Log.log.error("RemedyGateway: Problem while querying for " + formName + " by entry ID " + entryId + "\n" + e.getMessage(), e);
            throw e;
        }
        catch (Throwable e)
        {
            Log.log.error("RemedyGateway: " + e.getMessage(), e);
        }
        finally
        {
            disconnect(server);
        }
        return result;
    }

    public List<Map<String, String>> search(String formName, String criteria, int[] fieldIds, int maxResults, Integer sortFieldId, Boolean isAscending, ARServerUser server) throws Exception
    {
        List<Map<String, String>> result = new ArrayList<Map<String, String>>();

        try
        {
            if(StringUtils.isBlank(formName)) {
                if(filter != null)
                    formName = (String)filter.getAttributes().get("uformName");
            }
            
            List<Field> fields = server.getListFieldObjects(formName);

            // Create the search qualifier.
            QualifierInfo qual = server.parseQualification(criteria, fields, null, com.bmc.arsys.api.Constants.AR_QUALCONTEXT_DEFAULT);

            OutputInteger nMatches = new OutputInteger();
            List<SortInfo> sortOrder = new ArrayList<SortInfo>();
            if(sortFieldId != null) {
                if(isAscending != null && isAscending)
                    sortOrder.add(new SortInfo(sortFieldId, com.bmc.arsys.api.Constants.AR_SORT_ASCENDING));

                else
                    sortOrder.add(new SortInfo(sortFieldId, com.bmc.arsys.api.Constants.AR_SORT_DESCENDING));
            }

            if (fieldIds == null || fieldIds.length == 0) {
                String formList = (String)filter.getAttributes().get("uformList");
                fieldIds = RemedyGateway.getFormFields(formList);
            }

            // Retrieve entries from the form using the given qualification.
            if(maxResults <= 0)
            {
                Log.log.debug("RemedyGateway: Consumer asking to get all the records, maxResult is passed: " + maxResults);
                maxResults = com.bmc.arsys.api.Constants.AR_NO_MAX_LIST_RETRIEVE;
            }
            else
            {
                Log.log.debug("RemedyGateway: Consumer asking to get " + maxResults + " records");
            }
            //List<Entry> entryList = server.getListEntryObjects(formName, qual, 0, com.bmc.arsys.api.Constants.AR_NO_MAX_LIST_RETRIEVE, sortOrder, fieldIds, true, nMatches);
            List<Entry> entryList = server.getListEntryObjects(formName, qual, 0, maxResults, sortOrder, fieldIds, true, nMatches);
            Log.log.debug("RemedyGateway: Query returned " + nMatches + " matches.");
            if (nMatches.intValue() > 0)
            {
                Log.log.debug("RemedyGateway: Returned list size: " + entryList.size());
                for (Entry entry : entryList)
                {
                    Map<String, String> data = new HashMap<String, String>();
                    Set<Integer> keys = entry.keySet();
                    for (Integer key : keys)
                    {
                        // Log.log.debug("KEY: %d, VALUE: %s\n", key,
                        // entry.get(key).toString());
                        Value value = entry.get(key);
                        if (value.getDataType().equals(com.bmc.arsys.api.Constants.AR_DATA_TYPE_TIME))
                        {
                            Timestamp val = (Timestamp) value.getValue();
                            data.put(String.valueOf(key), "" + val.getValue());
                        }
                        else
                        {
                            data.put(String.valueOf(key), entry.get(key).toString());
                        }
                    }
                    result.add(data);
                }
            }
        }
        catch (Exception e)
        {
            Log.log.error("RemedyGateway: Problem while querying form: " + formName + " using query: " + criteria + "\n" + e.getMessage(), e);
        }
        catch (Throwable e)
        {
            Log.log.error("RemedyGateway: " + e.getMessage(), e);
        }

        return result;
    }

    public List<Map<String, String>> search(String formName, String criteria, String fieldIds, int maxResults, Integer sortFieldId, Boolean isAscending, String username, String password) throws Exception
    {
        int[] fields = {};
        
        if(StringUtils.isBlank(formName)) {
            if(filter != null)
                formName = (String)filter.getAttributes().get("uformName");
        }

        if (StringUtils.isNotEmpty(fieldIds)) {
            String formList = (String)filter.getAttributes().get("uformList");
            fields = RemedyGateway.getFormFields(formList);
        }
        
        return search(formName, criteria, fields, maxResults, sortFieldId, isAscending, username, password);
    }

    public List<Map<String, String>> search(String formName, String criteria, int[] fieldIds, int maxResults, Integer sortFieldId, Boolean isAscending, String username, String password) throws Exception
    {
        List<Map<String, String>> result = new ArrayList<Map<String, String>>();

        ARServerUser server = null;
        try
        {
            // Retrieve the detail info of all fields from theform.
            server = connect(username, password);
            result = search(formName, criteria, fieldIds, maxResults, sortFieldId, isAscending, server);
        }
        catch (Exception e)
        {
            Log.log.error("RemedyGateway: Problem while querying form: " + formName + " using query: " + criteria + "\n" + e.getMessage(), e);
            throw e;
        }
        catch (Throwable e)
        {
            Log.log.error("RemedyGateway: " + e.getMessage(), e);
            throw e;
        }
        finally
        {
            disconnect(server);
        }

        return result;
    }

    public List<String> find(String formName, String query, String username, String password) throws Exception
    {
        List<String> result = new ArrayList<String>();

        ARServerUser server = null;
        try
        {
            // Retrieve the detail info of all fields from theform.
            server = connect(username, password);

            List<Field> fields = server.getListFieldObjects(formName);

            // Create the search qualifier.
            QualifierInfo qual = server.parseQualification(query, fields, null, com.bmc.arsys.api.Constants.AR_QUALCONTEXT_DEFAULT);

            OutputInteger nMatches = new OutputInteger();
            List<SortInfo> sortOrder = new ArrayList<SortInfo>();
            sortOrder.add(new SortInfo(com.bmc.arsys.api.Constants.AR_CORE_LAST_MODIFIED_BY, com.bmc.arsys.api.Constants.AR_SORT_ASCENDING));

            int[] fieldIds = { com.bmc.arsys.api.Constants.AR_CORE_ENTRY_ID };

            // Retrieve entries from the form using the given qualification.
            List<Entry> entryList = server.getListEntryObjects(formName, qual, 0, com.bmc.arsys.api.Constants.AR_NO_MAX_LIST_RETRIEVE, sortOrder, fieldIds, true, nMatches);
            Log.log.debug("Query returned " + nMatches + " matches.");
            if (nMatches.intValue() > 0)
            {
                for (Entry entry : entryList)
                {
                    result.add(entry.getEntryId());
                }
            }
        }
        catch (Exception e)
        {
            Log.log.error("RemedyGateway: Problem while querying form: " + formName + " using query: " + query + "\n" + e.getMessage(), e);
        }
        catch (Throwable e)
        {
            Log.log.error("RemedyGateway: " + e.getMessage(), e);
        }
        finally
        {
            disconnect(server);
        }

        return result;
    }

    private static void main1(String[] args) {
        
        int incidentId = 0; // 2096;
        int taskId = 2251222;
        
        try {
            HOSTNAME = "resilient-01.resolvesys.com";
            PORT = null;
            USERNAME = "Jeffrey.Chen@resolvesys.com";
            P_ASSWORD = "Resolve1234";
        } catch(Exception e) {
          Log.log.error(e.getMessage(), e);
        }

        try {
            // 1. Update incident status

        } catch(Exception e) {
        	Log.log.error("RemedyGateway: " + e.getMessage(), e);
            e.printStackTrace();
        }
    }
    
    public Integer getId(String payload, String key) {
        
        Integer id = null;
        
        try {
            Map<String, Object> json = JSONObject.fromObject(payload);
            Object inc = json.get(key);
            if(inc != null && inc instanceof JSONObject) {
                id = (Integer)((JSONObject)inc).get("id");
            }
        } catch(Exception e) {
            Log.log.error("RemedyGateway: " + e.getMessage(), e);
        }

        return id;
    }
    
    private List<Integer> getIncidentTypeIds(String payload) {
        
        List<Integer> ids = new ArrayList<Integer>();
        
        try {
            JSONObject json = JSONObject.fromObject(payload);
            JSONArray jsonArray = json.getJSONArray("incident_type_ids");
            
            if(jsonArray == null)
                return null;
            
            for(int i=0; i<jsonArray.size(); i++)
                ids.add((Integer)jsonArray.get(i));
        } catch(Exception e) {
            Log.log.error("RemedyGateway: " + e.getMessage(), e);
        }

        return ids;
    }
    
} // class RemedyGatewayAPI