package com.resolve.gateway.jira;

import java.io.File;
import java.util.HashMap;
import java.util.Map;
import org.apache.commons.io.FileUtils;

import com.resolve.rsbase.MainBase;
import com.resolve.gateway.Filter;
import com.resolve.rsremote.ConfigReceiveGateway;
import com.resolve.util.Log;
import com.resolve.util.ObjectProperties;
import com.resolve.util.StringUtils;
import com.resolve.util.XDoc;

public class ConfigReceiveJIRA extends ConfigReceiveGateway {

    private static final long serialVersionUID = 1L;

    private static final String RECEIVE_JIRA_NODE = "./RECEIVE/JIRA/";

    private static final String RECEIVE_JIRA_FILTER = RECEIVE_JIRA_NODE + "FILTER";

    private String queue = "JIRA";

    private static final String RECEIVE_JIRA_ATTR_HTTPBASICAUTHUSERNAME = RECEIVE_JIRA_NODE + "@HTTPBASICAUTHUSERNAME";

    private static final String RECEIVE_JIRA_ATTR_HTTPBASICAUTHPASSWORD = RECEIVE_JIRA_NODE + "@HTTPBASICAUTHPASSWORD";

    private static final String RECEIVE_JIRA_ATTR_URL = RECEIVE_JIRA_NODE + "@URL";

    private String httpbasicauthusername = "";

    private String httpbasicauthpassword = "";

    private String url = "";

    public String getQueue() {
        return queue;
    }

    public void setQueue(String queue) {
        this.queue = queue;
    }

    public String getHttpbasicauthusername() {
        return this.httpbasicauthusername;
    }

    public void setHttpbasicauthusername(String httpbasicauthusername) {
        this.httpbasicauthusername = httpbasicauthusername;
    }

    public String getHttpbasicauthp_assword() {
        return this.httpbasicauthpassword;
    }

    public void setHttpbasicauthp_assword(String httpbasicauthpassword) {
        this.httpbasicauthpassword = httpbasicauthpassword;
        
    }

    public String getUrl() {
        return this.url;
    }

    public void setUrl(String url) {
        this.url = url;
        }

    public ConfigReceiveJIRA(XDoc config) throws Exception {
        super(config);
        define("httpbasicauthusername", STRING, RECEIVE_JIRA_ATTR_HTTPBASICAUTHUSERNAME);
        define("httpbasicauthp_assword", SECURE, RECEIVE_JIRA_ATTR_HTTPBASICAUTHPASSWORD);
        define("url", STRING, RECEIVE_JIRA_ATTR_URL);
    }

    @Override
    public String getRootNode() {
        return RECEIVE_JIRA_NODE;
    }

    @Override
    public void load() throws Exception {
        try {
            loadAttributes();
            if (isActive()) {
                JIRAGateway jira1Gateway = JIRAGateway.getInstance(this);
                filters = xdoc.getListMapValue(RECEIVE_JIRA_FILTER);
                if (filters.size() > 0) {
                    for (Map<String, Object> values : filters) {
                        String filterName = ((String) values.get(JIRAFilter.ID)).toLowerCase();
                        File scriptFile = getFile(MainBase.main.release.serviceName + "/config/jira/" + filterName + ".groovy");
                        try {
                            if (scriptFile.exists()) {
                                String groovy = FileUtils.readFileToString(scriptFile, "UTF-8");
                                if (StringUtils.isNotBlank(groovy)) {
                                    values.put(JIRAFilter.SCRIPT, groovy);
                                    Log.log.debug("Loaded groovy script: " + scriptFile);
                                }
                            }
                            jira1Gateway.setFilter(values);
                        } catch (Exception e) {
                            Log.log.warn(e.getMessage() + ". Skipping file: " + scriptFile.getAbsolutePath());
                        }
                    }
                }
            }
        } catch (Throwable e) {
            setActive(false);
            Log.log.warn("Could not load configuration for JIRA gateway, check blueprint. " + e.getMessage(), e);
        }
    }

    @Override
    public void save() throws Exception {
        File dir = getFile(MainBase.main.release.serviceName + "/config/jira");
        if (!dir.exists()) {
            FileUtils.forceMkdir(dir);
        }
        if (filters != null && isActive()) {
            filters.clear();
            for (Filter filter : JIRAGateway.getInstance().getFilters().values()) {
                 JIRAFilter jira1Filter = (JIRAFilter) filter;
                String groovy = jira1Filter.getScript();
                String scriptFileName;
                File scriptFile = null;
                if (StringUtils.isNotBlank(groovy)) {
                    scriptFileName = jira1Filter.getId().toLowerCase() + ".groovy";
                    scriptFile = getFile(MainBase.main.release.serviceName + "/config/jira/" + scriptFileName);
                }
                Map<String, Object> entry = new HashMap<String, Object>();
                putFilterDataIntoEntry(entry, jira1Filter);
                if (StringUtils.isNotBlank(jira1Filter.getLastIssueProcessedCreateTimeStamp()))
                {
                   Log.log.debug("Filter retained value of FirstOccurredTime: "+jira1Filter.getLastIssueProcessedCreateTimeStamp());
                    entry.put(JIRAFilter.CREATETIMESTAMP, jira1Filter.getLastIssueProcessedCreateTimeStamp());
                }
                if (StringUtils.isNotBlank(jira1Filter.getLastReadJiraId()))
                {
                    Log.log.debug("Filter retained value of Start Index: "+jira1Filter.getLastReadJiraId());
                    entry.put(JIRAFilter.LAST_READ_JIRA_ID, jira1Filter.getLastReadJiraId());
                }
                filters.add(entry);
                if (scriptFile != null) {
                    try {
                        FileUtils.writeStringToFile(scriptFile, groovy, "UTF-8");
                    } catch (Exception e) {
                        Log.log.warn(e.getMessage() + ". Skipping file: " + scriptFile.getAbsolutePath());
                    }
                }
            }
            xdoc.setListMapValue(RECEIVE_JIRA_FILTER, filters);
        }
        if (nameProperties != null && isActive()) {
            nameProperties.clear();
            for (String name : JIRAGateway.getInstance().getNameProperties().keySet()) {
                Map<String, Object> entry = new HashMap<String, Object>();
                entry.put(NAME_KEY, name);
                nameProperties.add(entry);
                Map props = JIRAGateway.getInstance().getNameProperties().getNameProperties(name);
                ObjectProperties prop = new ObjectProperties(props);
                prop.save(props);
            }
        }
        saveAttributes();
    }

    private void putFilterDataIntoEntry(Map<String, Object> entry, JIRAFilter jira1Filter) {
        entry.put(JIRAFilter.ID, jira1Filter.getId());
        entry.put(JIRAFilter.ACTIVE, String.valueOf(jira1Filter.isActive()));
        entry.put(JIRAFilter.ORDER, String.valueOf(jira1Filter.getOrder()));
        entry.put(JIRAFilter.INTERVAL, String.valueOf(jira1Filter.getInterval()));
        entry.put(JIRAFilter.EVENT_EVENTID, jira1Filter.getEventEventId());
        entry.put(JIRAFilter.RUNBOOK, jira1Filter.getRunbook());
        entry.put(JIRAFilter.SCRIPT, jira1Filter.getScript());
        entry.put(JIRAFilter.URLQUERY, jira1Filter.getUrlQuery());
    }
}

