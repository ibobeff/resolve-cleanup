package com.resolve.rscontrol;

import com.resolve.persistence.model.JIRAFilter;

public class MJIRA extends MGateway {

    private static final String MODEL_NAME = JIRAFilter.class.getSimpleName();

    protected String getModelName() {
        return MODEL_NAME;
    }
}

