package com.resolve.gateway.jira;
import java.util.Map;
import org.joda.time.DateTime;
import java.util.List;
import java.util.Collections;
import java.util.Date;

import com.resolve.rsbase.MainBase;
import com.resolve.gateway.BaseClusteredGateway;
import com.resolve.gateway.BaseFilter;
import com.resolve.gateway.Filter;
import com.resolve.gateway.MJIRA;
import com.resolve.util.DateUtils;
import com.resolve.util.Log;
import com.resolve.util.StringUtils;

import net.sf.json.JSON;

import com.resolve.rsremote.ConfigReceiveGateway;

public class JIRAGateway extends BaseClusteredGateway {

    private static volatile JIRAGateway instance = null;

    private String queue = null;
    public static volatile ObjectService genericObjectService;
    private static final int FETCH_LIMIT = ConfigReceiveGateway.MIN_PRIMARY_DATA_QUEUE_EXECUTOR_QUEUE_SIZE;

    public static JIRAGateway getInstance(ConfigReceiveJIRA config) {
        if (instance == null) {
            instance = new JIRAGateway(config);
        }
        return instance;
    }

    /**
     * This method is called once the initialization is done. If it gets called
     * before the initialization it will throw exception.
     *
     * @return
     */
    public static JIRAGateway getInstance() {
        if (instance == null) {
            throw new RuntimeException("JIRA Gateway is not initialized correctly..");
        } else {
            return instance;
        }
    }

    private JIRAGateway(ConfigReceiveJIRA config) {
        super(config);
    }

    @Override
    public String getLicenseCode() {
        return "JIRA";
    }

    @Override
    protected void addAdditionalValues(Map<String, Object> arg0, Map<String, String> arg1) {
    }

    @Override
    protected String getGatewayEventType() {
        return "JIRA";
    }

    @Override
    protected String getMessageHandlerName() {
        return MJIRA.class.getSimpleName();
    }

    @Override
    protected Class<MJIRA> getMessageHandlerClass() {
        return MJIRA.class;
    }

    public String getQueueName() {
        return queue + getOrgSuffix();
    }

    @Override
    public void start() {
        Log.log.debug("Starting JIRA Gateway");
        super.start();
    }

    @Override
    public void run()
    {
        if (!((ConfigReceiveGateway) configurations).isMock()) super.sendSyncRequest();
        while (running)
        {
            try
            {
                long startTime = System.currentTimeMillis();
                String sdkJIRAServerTime = getSDKJIRAServerTimeStamp();
                if (primaryDataQueue.isEmpty())
                {
                    Log.log.trace("Local Primary Event Queue for " + getGatewayEventType() + " is empty.....");
                    for (Filter filter : orderedFilters)
                    {
                        if (shouldFilterExecute(filter, startTime))
                        {
                            JIRAFilter jiraFilter = (JIRAFilter) filter;
                          //  String lastReadJiraId = jiraFilter.getLastReadJiraId();
                          //  String createdTime = jiraFilter.getLastIssueProcessedCreateTimeStamp();
                            /*
                             * On Creation of new filter Last Issue Processed
                             * Create Time will be blank or null, then set it to
                             * current JIRA Server Time in 'yyyy-MM-dd HH:mm'
                             * format
                             */
                            if (StringUtils.isBlank(jiraFilter.getLastIssueProcessedCreateTimeStamp()))
                            {
                                jiraFilter.setLastIssueProcessedCreateTimeStamp(sdkJIRAServerTime);
                            }

                            Log.log.trace("Filter " + filter.getId() + " executing");
                            List<Map<String, String>> results = invokeService(filter);

                            if (results.size() > 0)
                            {
                                String firstReadJIRAID = "";
                                String lastReadJiraID = "";
                                String firstReadJIRAKey = "";
                                String lastReadJiraKey = "";

                                if (results.get(0).containsKey("id"))
                                {
                                    firstReadJIRAID = results.get(0).get("id");
                                }
                                if (results.get(results.size() - 1).containsKey("id"))
                                {
                                    lastReadJiraID = results.get(results.size() - 1).get("id");
                                    jiraFilter.setLastReadJiraId(lastReadJiraID);
                                }
                                if (results.get(0).containsKey("id"))
                                {
                                    firstReadJIRAKey = results.get(0).get("key");
                                }
                                if (results.get(results.size() - 1).containsKey("key"))
                                {
                                    lastReadJiraKey = results.get(results.size() - 1).get("key");

                                }
                                System.out.println("Total JIRA's fetched: " + results.size() + " from: " + firstReadJIRAID + "("+firstReadJIRAKey+") To: " + lastReadJiraID+"("+lastReadJiraKey+")");
                                Log.log.debug("Total JIRA's fetched: " + results.size() + " from: " + firstReadJIRAID + "("+firstReadJIRAKey+") To: " + lastReadJiraID+"("+lastReadJiraKey+")");

                                // Enqueue runbook excution for each object
                                for (Map<String, String> sdkJIRAObject : results)
                                {
                                    addToPrimaryDataQueue(jiraFilter, sdkJIRAObject);

                                }

                            }
                            else
                            {
                                System.out.println("Total: " + results.size());
                                Log.log.debug("Total JIRA's fetched: " + results.size());
                            }

                        }
                        else
                        {
                            Log.log.trace("Filter:" + filter.getId() + " is inactive or not ready to execute yet.....");
                        }
                        if (orderedFilters == null || orderedFilters.size() == 0)
                        {
                            Log.log.trace("There is not deployed filter for JIRAGateway");
                        }
                    }
                    if (orderedFilters == null || orderedFilters.size() == 0)
                    {
                        Log.log.trace("There is not deployed filter for JIRAGateway");
                    }
                }
                if (orderedFilters.size() == 0)
                {
                    Thread.sleep(interval);
                }
            }
            catch (Exception e)
            {
                Log.log.warn("Warning: " + e.getMessage());
                Log.alert("Instance " + MainBase.main.configId.getGuid() + " " + getLicenseCode() + "-" + getQueueName() + "-" + getInstanceType() + " Polling Failed", "Instance " + MainBase.main.configId.getGuid() + " " + getLicenseCode() + "-" + getQueueName() + "-" + getInstanceType() + " Polling Failed " + "due to " + e.getMessage());
                try
                {
                    Thread.sleep(interval);
                }
                catch (InterruptedException ie)
                {
                    Log.log.warn("sleep interrupted: " + ie.getMessage(), ie);
                }
            }
        }
    }

    /**
     * Add customized code here to get the data as List<Map<String, String>> format for each filter
     * @return List<Map<String, String>>
     */
    public List<Map<String, String>> invokeService(Filter filter)
    {
        ConfigReceiveJIRA config = (ConfigReceiveJIRA) configurations;
        List<Map<String, String>> result = Collections.emptyList();

        JIRAFilter jiraFilter = (JIRAFilter) filter;
        boolean hasVariable = false;
        Log.log.trace("JIRAFilter " + filter);
        String body = "{\"fields\": {\"project\":{\"key\":\"QA\"},\"summary\": \"Created Test Issue Through REST Call\",\"description\":\"Creating of an issue using project QA and issue type names using the REST API\",\"issuetype\": {" + "\"name\": \"Test Run\"},\"customfield_10120\": {\"i\": \"10114\" }}}";

        // genericObjectService.getIssueFields("JIR", "");

        String urlQuery = jiraFilter.getUrlQuery();
        if (StringUtils.isNotBlank(urlQuery))
        {
            hasVariable = VAR_REGEX_GATEWAY_VARIABLE.matcher(urlQuery).find();
        }
        String createTimeStamp = jiraFilter.getLastIssueProcessedCreateTimeStamp();
        String lastReadJiraID = jiraFilter.getLastReadJiraId();

        if (StringUtils.isNotBlank(urlQuery))
        {
            try
            {
                Log.log.debug("--- "+jiraFilter.getId()+" interval started"+" ---");
                Log.log.debug("Filter query: "+urlQuery);
                urlQuery = genericObjectService.validateJqlQuery(urlQuery, createTimeStamp, lastReadJiraID, hasVariable);
                Log.log.debug("Updated Filter query: "+urlQuery);

               // result = genericObjectService.search(urlQuery, config.getHttpbasicauthusername(), config.getHttpbasicauthp_assword());
                result = genericObjectService.search(urlQuery, null, null);

                // result = genericObjectService.search(urlQuery,null,null);
            }
            catch (Exception e)
            {
                Log.log.error(e.getMessage(), e);
            }
        }
        return result;
    }

    /**
     * Add customized code here to initilize the connection with 3rd party system
     * @return List<Map<String, String>>
     */
    @Override
    protected void initialize() {
        ConfigReceiveJIRA config = (ConfigReceiveJIRA) configurations;
        queue = config.getQueue().toUpperCase();
        this.gatewayConfigDir = "/config/jira/";
        genericObjectService = new GenericObjectService(config);
        //Add customized code here to initilize the connection with 3rd party system;
    }

    /**
     * Add customized code here to do when stop the gateway
     */
    @Override
    public void stop() {
        Log.log.warn("Stopping JIRA gateway");
        //Add customized code here to stop the connection with 3rd party system;
        super.stop();
    }

    @Override
    public Filter getFilter(Map<String, Object> params) {
        return new JIRAFilter((String) params.get(BaseFilter.ID), (String) params.get(BaseFilter.ACTIVE), (String) params.get(BaseFilter.ORDER), (String) params.get(BaseFilter.INTERVAL), (String) params.get(BaseFilter.EVENT_EVENTID), (String) params.get(BaseFilter.RUNBOOK), (String) params.get(BaseFilter.SCRIPT), (String) params.get(JIRAFilter.URLQUERY),(String) params.get(JIRAFilter.LAST_READ_JIRA_ID),(String) params.get(JIRAFilter.CREATETIMESTAMP));
    }
    //This method acceses the JIRA server for its Server time stamp.
    private String getSDKJIRAServerTimeStamp()
    {
        String result = null;

        Map<String, String> serverInfo = Collections.emptyMap();

        try
        {
            serverInfo = genericObjectService.getServerInfo(null, null);

            if (serverInfo != null && !serverInfo.isEmpty())
            {
                String serverTimeStamp = serverInfo.get("serverTime"); //2017-05-23T11:45:49.513-0700


                if (StringUtils.isNotEmpty(serverTimeStamp))
                {
                    java.util.Date date = new DateTime( serverTimeStamp).toDate(); //Tue May 23 11:26:46 PDT 2017 //2017-05-23 11:26
                    result=DateUtils.convertDateToString(date,"yyyy-MM-dd HH:mm");

                }


            }
        }
        catch(Exception e)
        {
            Log.log.warn("Failed to get JIRA ServerInfo, falling back to local time as JIRA server time.", e);
        }

        if (StringUtils.isBlank(result))
        {
            result = DateUtils.convertDateToString(new Date(), "yyyy-MM-dd HH:mm");
        }

        Log.log.debug("JIRA Server Time Stamp = " + result);

        return result;
    }

    public boolean checkGatewayConnection() throws Exception
    {
        Map<String, String> serverInfo = Collections.emptyMap();

        try
        {
            serverInfo = genericObjectService.getServerInfo(null, null);

            if (serverInfo == null || serverInfo.isEmpty())
            {
                throw new Exception ("Failed to get server info from JIRA server. Connection to JIRA server possibly broken.");
            }
            else
            {
                Log.log.debug("Instance " + MainBase.main.configId.getGuid() + " : " + getLicenseCode() + "-" +
                              getQueueName() + "-" + getInstanceType() + " connected to configured JIRA server");
            }
        }
        catch(Exception e)
        {
            Log.alert("Instance " + MainBase.main.configId.getGuid() + " breached " + getLicenseCode() + "-" + getQueueName() + "-" + getInstanceType() + "-ConnectionSelfCheck",
                      "Instance " + MainBase.main.configId.getGuid() + " Breached " + getLicenseCode() + "-" + getQueueName() + "-" + getInstanceType() + "-ConnectionSelfCheck" +
                      " due to " + e.getMessage());
        }

        return true;
    }

    public boolean addComment(String key,
                              String comment,
                              String username,
                              String password) throws Exception
    {
        return genericObjectService.addComment(key, comment, username, password);
    }

    String createIssue(String username, String password, JSON jsonParams) throws Exception{
        return genericObjectService.createIssue(username, password, jsonParams);

    }
    List<Map<String, String>> getAllProjects()throws Exception
    {
        return genericObjectService.getAllProjects();
    }

    String getIssueFields(String projectKey,String issueType) throws Exception
    {
        return genericObjectService.getIssueFields(projectKey,issueType);
    }

    public  List<Map<String, String>> getAllIssueTypes()throws Exception{
        return genericObjectService.getAllIssueTypes();
    }


    public Map<String, String> getServerInfo(String username,
                                             String password) throws Exception
    {
        return genericObjectService.getServerInfo(username, password);
    }

    public List<Map<String, String>> search(String urlQuery,
                                            String username,
                                            String password) throws Exception
    {
        return genericObjectService.search(urlQuery, username, password);
    }

    public List<Map<String, String>> search(String jql, Integer startAt,Integer maxResults,String fields,
                    String username,
                    String password) throws Exception{
        return genericObjectService.search(jql, startAt,maxResults,fields, username, password);
    }

}
