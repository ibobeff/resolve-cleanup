/******************************************************************************
* (C) Copyright 2015
*
* Resolve Systems, LLC
*
* All rights reserved
* This software is distributed under license and may not be used, copied,
* modified, or distributed without the express written permission of
* Resolve Systems, LLC.
*
******************************************************************************/

package com.resolve.gateway.jira;

import java.net.URLEncoder;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.StringTokenizer;

import org.apache.commons.httpclient.HttpStatus;

import com.resolve.rsremote.ConfigReceiveGateway;
import com.resolve.rsremote.Main;

import com.resolve.util.Log;
import com.resolve.util.StringUtils;
import com.resolve.util.restclient.RestCallException;
import com.resolve.util.restclient.RestCaller;

import net.sf.json.JSON;
import net.sf.json.JSONArray;
import net.sf.json.JSONException;
import net.sf.json.JSONObject;
import net.sf.json.JSONSerializer;

/**
 * Generic SDKJIRA object service provider class.
 * Uses REST API
 * 
 * @author Hemant Phanasgaonkar
 */
public class GenericObjectService implements ObjectService
{
    protected final ConfigReceiveJIRA configurations;
    protected final RestCaller restCaller;
    private static final int FETCH_LIMIT = ConfigReceiveGateway.MIN_PRIMARY_DATA_QUEUE_EXECUTOR_QUEUE_SIZE;
    static  Map<String,String> jiraFieldsNameIdMap=new HashMap();
    
    public GenericObjectService(ConfigReceiveJIRA configurations)
    {
        restCaller = new RestCaller(configurations.getUrl(), null, null, Main.main.configProxy);
        this.configurations = configurations;
        jiraFieldsNameIdMap = getIssueFieldsIdNameMap(null,null);
    }
        
    @Override
    public Map<String, String> getServerInfo(String username, String password) throws Exception
    {
        setupRestCaller("serverInfo", username, password);
        String response = restCaller.getMethod(null, (String)null, null);
       

        if (Log.log.isDebugEnabled())
        {
            Log.log.debug("Get serverInfo returnede [" + response + "]");
        }
        
        return StringUtils.jsonObjectToMap(StringUtils.stringToJSONObject(response));
    }
    private ArrayList<String> parseQueryToken(String token){
        
        int index =token.indexOf("=");
        String jqlQuery = token.substring(token.indexOf("=")+1,token.length()); 
        ArrayList<String> tokenList = new ArrayList<>();
        tokenList.add(jqlQuery);
        return tokenList;
        
    }
    @SuppressWarnings("unchecked")
    @Override
    public List<Map<String, String>> search(String urlQuery, String username, String password) throws Exception
    {
        
        List<Map<String, String>> issues = Collections.emptyList();
        JSONObject jsonObj = new JSONObject();
        JSONArray fieldsArray = new JSONArray();
        setupRestCaller("search", username, password);
        //Map<String,String> jsonQueryMap=new HashMap<String,String>();
        String response = null;
       // String jsonQuery= "{\"jql\":\"";
      //  urlQuery = "jql=project = 'RBA' and  id > 143680 order by id asc&startAt=0&maxResults=1000&validateQuery=true&fields=summary,comment&expand=id";
        if (StringUtils.isNoneBlank(urlQuery))
        {
            Map<String, List<String>> reqParams = new HashMap<String, List<String>>();
            if (urlQuery.contains("&"))
            {
                StringTokenizer token = new StringTokenizer(urlQuery, "&");
                while (token.hasMoreTokens())
                {
                    String queryParam = token.nextToken();
                    if (queryParam.startsWith("jql="))
                    {
                        String[]jqlParamValue = queryParam.split("jql=");
                        String paramNameValue=jqlParamValue[1].trim();
                        //String value= paramNameValue.nextToken();
                        jsonObj.accumulate("jql", paramNameValue);
                       // jsonQueryMap.put("jql", paramNameValue);
                       // jsonQuery+=paramNameValue+"\",";
                    }
                    if (queryParam.replace(" ", "").startsWith("fields="))
                    {
                        String[]jqlParamValue = queryParam.split("fields=");
                        String paramNameValue=jqlParamValue[1].trim();
                        //String value= paramNameValue.nextToken();
                       // jsonQueryMap.put("fields", paramNameValue);
                        if(paramNameValue.contains(",")) 
                        {
                            StringTokenizer fieldsTokens = new StringTokenizer(paramNameValue, ",");
                            while(fieldsTokens.hasMoreTokens()) {
                                fieldsArray.add(fieldsTokens.nextToken().toString());
                            }
                            jsonObj.accumulate("fields", fieldsArray);
                           // jsonQuery+=fieldsArray.toString()+",";
                        }
                        else 
                        {
                            fieldsArray.add(paramNameValue);
                            jsonObj.accumulate("fields", fieldsArray);
                          //  jsonQuery+="\"fields\":"+fieldsArray.toString()+",";
                        }
                        
                    } 
                    
                }
            }
            else{
                String[]jqlParamValue = urlQuery.split("jql=");
                String paramNameValue=jqlParamValue[1];
                jsonObj.accumulate("jql", paramNameValue);
                //String value= paramNameValue.nextToken();
               // jsonQueryMap.put("jql", paramNameValue);
            }
           // jsonQueryMap.put("maxResults",FETCH_LIMIT+"" );
            jsonObj.accumulate("maxResults", FETCH_LIMIT);
          //  jsonQuery+="\"maxResults\":"+FETCH_LIMIT+"}";
            urlQuery= StringUtils.jsonObjectToString(jsonObj);
            System.out.println(urlQuery);
            Log.log.debug("JSON body for POST search request is: "+urlQuery);
           // urlQuery = jsonObj.toString();
                         
       }
       
         
                Collection<Integer> expectedStatusCodes = new HashSet<Integer>();
                expectedStatusCodes.add(new Integer(HttpStatus.SC_OK));
               // String a = new String(jsonObj.toString());
              //  System.out.println(StringUtils.mapToJson(jsonQueryMap).toString());
                
                response = restCaller.postMethod(null, null,jsonObj.toString(), expectedStatusCodes);
         
       
        
        if (Log.log.isDebugEnabled())
        {
            Log.log.debug("search query: " + urlQuery + " returned [" + response + "]");
        }
        issues = parseQueryResponse(response);

        
        return issues;
    }
    
    @Override
    public boolean addComment(String key,
                              String comment,
                              String username, 
                              String password) throws Exception
    {
        setupRestCaller("issue/" + key + "/comment", username, password);
        
        Map<String, String> jsonCommentMap = new HashMap<String, String>();
        
        jsonCommentMap.put("body", comment);
        
        String response = null;
        
        try
        {
            response = restCaller.postMethod(null, null, StringUtils.mapToJson(jsonCommentMap), null);
        }
        catch (RestCallException rce)
        {
            Log.log.debug("GenericObjectService.adComment failed due to : "  + rce.getMessage());
            throw rce;
        }

        if (Log.log.isDebugEnabled())
        {
            Log.log.debug("add comment returned [" + response + "]");
        }
        
        Map<String, String> responseMap = StringUtils.jsonObjectToMap(StringUtils.stringToJSONObject(response));
         
        return responseMap.containsKey("body") && responseMap.get("body").equals(comment);        
    }
    
    private void setupRestCaller(String objectType, String username, String password)
    {
        if (StringUtils.isEmpty(username) || StringUtils.isEmpty(password))
        {
            username = configurations.getHttpbasicauthusername();
            password = configurations.getHttpbasicauthp_assword();
        }

        restCaller.setHttpbasicauthusername(username);
        restCaller.setHttpbasicauthpassword(password);
        
        restCaller.setUrlSuffix(objectType);
    }

    @Override
    public String createIssue(String username, String password,JSON jsonBody) throws Exception
    {
       // TODO Auto-generated method stub
        List<Map<String, String>> issues = Collections.emptyList();

        setupRestCaller("issue", username, password);
        String jiraKey="Failed to create a JIRA.";
        String response = null;

        if (StringUtils.isNoneBlank(jsonBody.toString()))
        {
            response = restCaller.postMethod(null, null, jsonBody.toString(), null);
        }

        Map<String, Object> responseJSON = new HashMap<String, Object>();

        StringUtils.jsonObjectStringToMap(response, "responseJSON", responseJSON);

        if (responseJSON.containsKey("key"))
        {
            Log.log.debug("Created issue successfully in JIRA. Key: "+responseJSON.get("key"));
            jiraKey=(String) responseJSON.get("key");
            System.out.println("Created issue successfully in JIRA. Key: "+responseJSON.get("key"));
            return jiraKey;
        }
        else {
            Log.log.error("Failed to create issue in JIRA.");
            if(responseJSON.containsKey("errors")) {
                Log.log.error(responseJSON.get("errors"));
            }
            throw new Exception("Failed to create issue. Response received: "+responseJSON);
        }

    }

    @Override
    /**
     * Returns all projects which are visible for the user. To create an issue
     * in JIRA you need to specify a Project. This call is used to get
     * Project information before creating an issue.
     * @return Returns a list of projects for which the user has the BROWSE, ADMINISTER or PROJECT_ADMIN project permission.
     * @throws Exception
     */
    public List<Map<String, String>> getAllProjects() throws Exception
    {
        String response = "";
        List<Map<String, String>> allProjectsData = new ArrayList();
        try
        {
            setupRestCaller("project", null, null);

            // TODO Auto-generated method stub
      
            response = restCaller.getMethod(null,(String)null, (Map<String, String>)null);
            JSON jsonArray = JSONSerializer.toJSON(response);
            if (jsonArray instanceof JSONArray)
            {
                allProjectsData = StringUtils.jsonArrayToList((JSONArray) jsonArray);

            }
           
            return allProjectsData;
        }
        catch (Exception e)
        {
            Log.log.error("Failed to get projects information from JIRA.");

            throw new Exception("Failed to retrieve Projects from JIRA: " + response);
        }

    }

    @Override
    
    public  List<Map<String, String>> getAllIssueTypes() throws Exception
    {
        // TODO Auto-generated method stub
        String response = "";
        List<Map<String, String>> issuTypeData = new ArrayList();
        try
        {
            setupRestCaller("issuetype", null, null);
            // TODO Auto-generated method stub

            response = restCaller.getMethod(null, (String) null, (Map<String, String>) null);
            JSON jsonArray = JSONSerializer.toJSON(response);
            if (jsonArray instanceof JSONArray)
            {
                issuTypeData = StringUtils.jsonArrayToList((JSONArray) jsonArray);

            }
            return issuTypeData;
        }
        catch (Exception e)
        {
            Log.log.error("Failed to get Issue Types from JIRA.");

            throw new Exception("Failed to retrieve Issue Types from JIRA: " + response);
        }

    }
    
    

    @Override

    public String getIssueFields(String projectKey,String issueType) throws Exception
    {
        // TODO Auto-generated method stub
        String response = "";
        try
        {
            if (projectKey == "" || projectKey == null || projectKey == " ")
            {
                Log.log.error("Project key is required field to retrive issue fields");
                response = "Project key is required field to retrive issue fields.";
                throw new Exception("Project key is required field to retrive issue fields.");
            }
            setupRestCaller("issue/createmeta", null, null);
            // TODO Auto-generated method stub
            // Map<String, String> reqHeaders = new HashMap<String, String>();
            String urlParam = new String();
            if (issueType != null && issueType != "" && issueType != " ")
            {
                urlParam = "projectKeys=" + projectKey + "&issuetypeNames=" + issueType + "&expand=projects.issuetypes.fields";
            }
            else
            {
                urlParam = "projectKeys="  + projectKey + "&expand=projects.issuetypes.fields";
            }

            response = restCaller.getMethod(null, urlParam, (Map<String, String>) null);
           // Map<String,Map<String,String>>  issuFields=parseIssuFiedsData(response);
            //System.out.println( projectKey + " project issuefields are: " + response);
            if(StringUtils.stringToJSONObject(response) instanceof JSONObject) {
                //System.out.println(StringUtils.jsonObjectToString(StringUtils.stringToJSONObject(response)));
                return StringUtils.jsonObjectToString(StringUtils.stringToJSONObject(response));
            }
            else
                return response;
        }
        catch (Exception e)
        {
            Log.log.error("Failed to get Issue Types from JIRA.");

            throw e;
        }

    }
    
/*    Map<String,Map<String,String>> parseIssuFiedsData(String response){

        Map<String,Map<String,String>>  issuFields = new HashMap();
        JSON fieldsJSON = JSONSerializer.toJSON(response);
        if(fieldsJSON instanceof JSONObject) 
        {
            if(((JSONObject) fieldsJSON).containsKey("projects")) {
                
               JSON projects = JSONSerializer.toJSON(((JSONObject) fieldsJSON).get("projects"));
               if(projects instanceof JSONArray) {
                   JSONArray project = (JSONArray)projects;
                   JSONObject projectJSON  = (JSONObject) project.get(0);
                   if(projectJSON.containsKey("issuetypes")) 
                   {
                     JSONArray issueTypeandFieldsOfProj = projectJSON.getJSONArray("issuetypes"); 
                     for(JSONObject obj:issueTypeandFieldsOfProj ) {
                         
                     }
                   }
                   
               }
               
            }

            if(((JSONObject) fieldsJSON).containsKey("issuetypes")) {
               JSON issuTypeJson= (JSONObject) ((JSONObject) fieldsJSON).get("issuetypes");
               
              // JSON issuTypeJson = JSONSerializer
           }
        }
        
        
        return issuFields;
    }*/

    @Override
    public String validateJqlQuery(String urlQuery,String createTimeStamp,String lastReadJiraID,boolean hasVariable)
    {

        String jql = "";
        String fields = "";
        StringBuffer numbers = new StringBuffer();
        String idValue = "";
        
        try
        {
            if(urlQuery.contains("& fields")) {
                urlQuery =urlQuery.replace("& fields", "&fields");
        }
            if (urlQuery.contains(" > "))
            {
                urlQuery = urlQuery.replace(" > ", ">");
            }
            if (urlQuery.contains("> "))
            {
                urlQuery = urlQuery.replace("> ", ">");
            }
            if (urlQuery.contains(" >"))
            {
                urlQuery = urlQuery.replace(" >", ">");
            }
            
            if (urlQuery.contains("id > "))
            {
                urlQuery = urlQuery.replace("id > ", "id>");
            }
            if (urlQuery.contains("id >"))
            {
                urlQuery = urlQuery.replace("id >", "id>");
            }
            if (urlQuery.contains("id> "))
            {
                urlQuery = urlQuery.replace("id> ", "id>");
            }
           
            if (urlQuery.replaceAll(" ","").contains("&fields="))
            {
                /*String temp =urlQuery.replaceAll(" ","");
                StringTokenizer token = new StringTokenizer("&fields=", temp);
                jql = token.nextToken();
                fields = token.nextToken();
                urlQuery = jql + "order by id asc "+fields;*/
                String[]temp = urlQuery.split("&");
                jql = temp[0];
                fields =temp[1];
                urlQuery = jql + " order by id asc &"+fields;

            }
            else
            {

                urlQuery = urlQuery + " order by id asc";
            }
            String temp = "";
            if (hasVariable)
            {
                urlQuery = urlQuery.replaceAll("\\$\\{" + JIRAFilter.CREATETIMESTAMP + "\\}", "'" + createTimeStamp + "'");
            }
            Log.log.debug("Transformed URL Query [" + urlQuery + "]");
            temp = urlQuery.replaceAll(" ", "");

            if (StringUtils.isNotBlank(lastReadJiraID))
            {

               
               String[] split=null;
               
                if (temp.contains("id>") || temp.contains("id>="))
                {
                    if(temp.contains("id>=")) {
                        split = urlQuery.split("id>=");
                    }
                   
                    else if(temp.contains("id>")) {
                        split = urlQuery.split("id>");
                    }
                    
                    if (split.length == 2)
                    {
                      
                        int index = split[1].indexOf("order");
                        
                            idValue = split[1].substring(0, index);
                     }

                       /* for (char c : idValue.toCharArray())
                        {
                            if (Character.isDigit(c))
                                numbers.append(c);
                            else
                                break;
                        }*/

                        System.out.println(idValue.toString());
                        split[1] = split[1].replaceFirst(idValue.toString(), lastReadJiraID+" ");
                         if(temp.contains("id>=")) {
                            urlQuery = split[0] + " " + "id >" + split[1] ;
                            //urlQuery = split[0] + " " + "id >=" + split[1] ; This way it fetches the same record twice at each interval
                        }
                         else if(temp.contains("id>")) {
                            urlQuery = split[0] + " " + "id >" + split[1] ;
                        }
                        
                       if(urlQuery.contains("id>=")) {
                           urlQuery = urlQuery.replace("id>=", "id>");
                       }

                    }
                else if ((!temp.contains("key=")) && (!temp.contains("id=")))
                {
                    if (temp.replaceAll(" ","").contains("&fields="))
                    {
                        /*String temp =urlQuery.replaceAll(" ","");
                        StringTokenizer token = new StringTokenizer("&fields=", temp);
                        jql = token.nextToken();
                        fields = token.nextToken();
                        urlQuery = jql + "order by id asc "+fields;*/
                        String[]tempStr = urlQuery.split("&");
                        jql = tempStr[0];
                        fields =tempStr[1];
                        if(temp.contains("orderbyidasc")) 
                        {
                            int startIndex = urlQuery.indexOf("order");
                            String subStr = urlQuery.substring(0, startIndex)+" and id > " + lastReadJiraID+ " order by id asc";
                            urlQuery=subStr + "&"+fields;
                                                   
                            
                        }
                        //urlQuery = jql +" and id > " + lastReadJiraID+ "order by id asc &"+fields;

                    }
                    else {
                        if(temp.contains("orderbyidasc")) 
                        {
                            String subStr="";
                            int startIndex = urlQuery.indexOf("order");
                            if(fields !="") {
                                subStr = urlQuery.substring(0, startIndex)+" and id > " + lastReadJiraID+ " order by id asc &"+fields; 
                            }
                            else
                                subStr = urlQuery.substring(0, startIndex)+" and id > " + lastReadJiraID+ " order by id asc";
                            
                            
                            urlQuery=subStr;
                           
                            
                            
                        }
                    }
                
                   
                }

                }
/*                else if ((!temp.contains("key=")) && (!temp.contains("id=")))
                {
                    if (temp.replaceAll(" ","").contains("&fields="))
                    {
                        String temp =urlQuery.replaceAll(" ","");
                        StringTokenizer token = new StringTokenizer("&fields=", temp);
                        jql = token.nextToken();
                        fields = token.nextToken();
                        urlQuery = jql + "order by id asc "+fields;
                        String[]tempStr = urlQuery.split("&");
                        jql = tempStr[0];
                        fields =tempStr[1];
                        if(temp.contains("orderbyidasc")) 
                        {
                            int startIndex = urlQuery.indexOf("order");
                            String subStr = urlQuery.substring(0, startIndex)+" and id > " + lastReadJiraID+ " order by id asc &"+fields;
                            url
                            Query=subStr;
                            int aaa=11;
                            
                            
                        }
                        //urlQuery = jql +" and id > " + lastReadJiraID+ "order by id asc &"+fields;

                    }
                   
                }*/

            System.out.println("Query: " + urlQuery);
        }
        catch(Exception e) {
            Log.log.error(e.getMessage());
        }
       
        return urlQuery;
    }

    @Override
    public List<Map<String, String>> parseQueryResponse(String response) throws Exception
    {
        // TODO Auto-generated method stub
      
        JSONObject input = JSONObject.fromObject(response);
        JSON issuesJSON=null;
        JSONArray issuesJSONArray=null;
        List<Map<String, String>> issueMapList = new ArrayList<Map<String, String>>();
        if(input.containsKey("issues")) {
             issuesJSON = JSONSerializer.toJSON( input.get("issues"));
             issuesJSONArray = (JSONArray)issuesJSON;
        }
       //Parse the JIRAs(Called as issues in the REST response)
        if (issuesJSONArray!=null && issuesJSONArray instanceof JSONArray)
        {
            // Process all JIRAs received
            for (int i = 0; i < issuesJSONArray.size(); i++)
            {
                Map<String, String> jira = new HashMap<String, String>();
                Map<String, String> jsonArrayMap = StringUtils.jsonObjectToMap(issuesJSONArray.getJSONObject(i));

                Set<String> arrayKeySet = jsonArrayMap.keySet(); // JIRA keys
                                                                 // all
                if (jsonArrayMap.containsKey("fields"))
                {
                    String jiraKey = "";
                    if (jsonArrayMap.containsKey("key"))
                    {
                        jiraKey = jsonArrayMap.get("key").toString();
                        jira = parseIssueFields(jsonArrayMap.get("fields"), jiraKey);
                    }

                }
                for (Iterator<String> arrayIterator = arrayKeySet.iterator(); arrayIterator.hasNext();)
                {
                    String arrayKey = arrayIterator.next();
                    String arrayValue = jsonArrayMap.get(arrayKey);
                    // System.out.println(arrayKey+" : "+arrayValue);
                    if (arrayKey.equals("fields"))
                    {
                        continue;
                    }

                    else
                    {
                        jira.put(arrayKey, arrayValue);
                    }
                     //System.out.println("Key: "+arrayKey+" value: "+
                     //arrayValue);
                }

                issueMapList.add(i, jira);
                 //System.out.println("Jira total fields: "+jira.size());

            }
        }
        return issueMapList;
    }
    
    public Map<String, String> parseIssueFields(String jiraFields,String jiraKey )
    {
        JSONObject jiraFieldsJson = JSONObject.fromObject(jiraFields);
        Map<String, String> jiraFieldMap = StringUtils.jsonObjectToMap(jiraFieldsJson);
        Map<String, String> jiraFieldValuesMap = new HashMap<String, String>();
      
        Set<String> jiraFieldNames = jiraFieldMap.keySet();
        String fieldValue = "";
        JSON jiraFieldVal = null;
        String jiraFieldNameMapped="";
        for (Iterator<String> arrayIterator = jiraFieldNames.iterator(); arrayIterator.hasNext();)
        {
            String jiraFieldName = arrayIterator.next();
            if(GenericObjectService.jiraFieldsNameIdMap.size()==0) {
                GenericObjectService.jiraFieldsNameIdMap= getIssueFieldsIdNameMap(configurations.getHttpbasicauthusername(),configurations.getHttpbasicauthp_assword());
            }
            if(jiraFieldsNameIdMap.size()>0) {
                if(jiraFieldsNameIdMap.containsKey(jiraFieldName)) {
                    
                    jiraFieldNameMapped =jiraFieldsNameIdMap.get(jiraFieldName);
                }
            }
            String jiraFieldValue = jiraFieldMap.get(jiraFieldName);
            jiraFieldName=jiraFieldNameMapped;
             //System.out.println(jiraFieldName + " : " + jiraFieldValue);

            try
            {
                if (jiraFieldValue.startsWith("{\"") || jiraFieldValue.startsWith("["))
                {
                    jiraFieldVal = JSONSerializer.toJSON(jiraFieldValue);

                }
                else
                {
                    jiraFieldValuesMap.put(jiraFieldName, jiraFieldValue + "");
                   //  System.out.println(jiraFieldName+" : "+jiraFieldValue);
                    continue;
                }
                if (jiraFieldVal instanceof JSONObject)
                {
                    if (((JSONObject) jiraFieldVal).containsKey("name"))
                    {
                        jiraFieldValuesMap.put(jiraFieldName, ((JSONObject) jiraFieldVal).get("name").toString());
                       // System.out.println(jiraFieldName+" : "+((JSONObject)
                        // jiraFieldVal).get("name").toString());
                    }
                    else if (((JSONObject) jiraFieldVal).containsKey("value"))
                    {
                        jiraFieldValuesMap.put(jiraFieldName, ((JSONObject) jiraFieldVal).get("value").toString());
                        // System.out.println(jiraFieldName+" : "+((JSONObject)
                        // jiraFieldVal).get("value").toString());
                    }
                    else
                    {
                        jiraFieldValuesMap.put(jiraFieldName, jiraFieldVal.toString());
                        // System.out.println(jiraFieldName+" : "+jiraFieldVal.toString());
                    }
                }
                else if (jiraFieldVal instanceof JSONArray)
                {

                    fieldValue = parseJiraFieldValueArray((JSONArray) jiraFieldVal);
                    jiraFieldValuesMap.put(jiraFieldName, fieldValue);
                    // System.out.println(jiraFieldName+" : "+fieldValue);

                }
                else
                {
                    jiraFieldValuesMap.put(jiraFieldName, fieldValue);
                   //  System.out.println("************ "+jiraFieldName+" : "+fieldValue);
                }
            }
            catch (JSONException e)
            {
                jiraFieldValuesMap.put(jiraFieldName, jiraFieldValue + "");

            }
            catch (Exception e)
            {
                Log.log.error("Exception occurred while processing fields of Jira: " + jiraKey);

            }

        }
        // System.out.println("Jira total only fields:
        // "+jiraFieldValuesMap.size());
        return jiraFieldValuesMap;

    }
 
    private  String parseJiraFieldValueArray(JSONArray objectsData)
    {
        /*JSON Array can be set of string,jsonobject or jsonarray
        Here I am assuming that JSONArray either contains all strings or all jsonobj or all jsonarray as its 
        contents and thats why checking the type of firsr array elemnt and 
        depending on that type calls tht stringutil functions to flatten out the JASONArray to Map or to list*/

        JSONArray jsonArray = objectsData;
        String type = "";
        String jiraFieldValue = "";
        if (jsonArray.size() == 0)
        {
            return "";
        }

        Object test = jsonArray.get(0);
        if (test instanceof String)
        {
            type = "string";
        }
        else if (test instanceof JSONArray)
        {
            type = "array";
        }
        else if (test instanceof JSONObject)
        {
            type = "jsonobj";
        }

        if (type.equals("string"))
        {
            String nameValue = "";
            List<String> arrayFieldsList = StringUtils.jsonArrayToList(jsonArray.toString());
            for (int j = 0; j < arrayFieldsList.size(); j++)
            {
                String fieldValue = arrayFieldsList.get(j);
                if (fieldValue.contains("name="))
                {

                    String[] split = fieldValue.split("name=");
                    if (split.length == 2)
                    {
                        if (nameValue.equals(""))
                        {
                            nameValue = split[1].substring(0, split[1].indexOf(","));
                        }
                        else
                        {
                            nameValue += "," + split[1].substring(0, split[1].indexOf(","));
                        }

                    }
                }
                else {
                    if (nameValue.equals(""))
                    {
                        nameValue = fieldValue;
                    }
                    else
                    {
                        nameValue += "," + fieldValue;
                    }
                }
            }
            jiraFieldValue = nameValue;
        }

        else if (type.equals("jsonobj"))
        {
            jiraFieldValue = "";
            List<Map<String, String>> jsonArrayFieldsToMapList = StringUtils.jsonArrayToList(jsonArray);
            for (int i = 0; i < jsonArrayFieldsToMapList.size(); i++)
            {
                Map<String, String> jsonArrayMap = jsonArrayFieldsToMapList.get(i);
                Set<String> arrayKeySet = jsonArrayMap.keySet();
                for (Iterator<String> arrayIterator = arrayKeySet.iterator(); arrayIterator.hasNext();)
                {
                    String arrayKey = arrayIterator.next();
                    if (arrayKey.equals("name"))
                    {
                        if (jiraFieldValue.equals(""))
                        {
                            jiraFieldValue = jsonArrayMap.get(arrayKey);
                        }
                        else
                        {
                            jiraFieldValue += ","+jsonArrayMap.get(arrayKey);
                        }

                    }

                }
            }

        }

        return jiraFieldValue;

    }

    @Override
    public Map<String, String> getIssueFieldsIdNameMap(String userName,String password) 
    {
        // TODO Auto-generated method stub
        List<Map<String, String>> issueFieldsList = new ArrayList();///
        Map<String, String> issueFieldsIdNameMap = new HashMap<String, String>();
        String id="";
        String name="";
        String response = "";
        try
        {
            setupRestCaller("field", null, null);

            // TODO Auto-generated method stub

            response = restCaller.getMethod(null, (String) null, (Map<String, String>) null);
          
            if (StringUtils.isNotBlank(response))
            {
                JSON fieldsMetaArray = JSONSerializer.toJSON(response);
                if (fieldsMetaArray instanceof JSONArray)
                {
                    issueFieldsList = StringUtils.jsonArrayToList((JSONArray) fieldsMetaArray);
                    for(int i=0;i<issueFieldsList.size();i++) 
                    {
                        id="";
                        name="";
                        Map<String, String> fieldInfo = issueFieldsList.get(i);
                        if(fieldInfo.containsKey("id")) {
                            id=fieldInfo.get("id");
                        }
                        if(fieldInfo.containsKey("name")) {
                            name=fieldInfo.get("name");
                        }
                        if(id !="" && name!="") {
                            issueFieldsIdNameMap.put(id,name);  
                        }
                    }
                }
                
            }
        }
        catch (Exception e)
        {
            Log.log.error("Failed to get JIRA fields information from JIRA.");

            
        }
        return issueFieldsIdNameMap;
    }

    @Override
    public List<Map<String, String>> search(String jql, Integer startAt, Integer maxResults, String fields, String username, String password) throws Exception
    {
        // TODO Auto-generated method stub
        JSONArray fieldsArray = new JSONArray();
        JSONObject postJsonBody = new JSONObject();
        int total = 0;
        String response = "";
        List<Map<String, String>> issues = new ArrayList<>();
        Collection<Integer> expectedStatusCodes = new HashSet<Integer>();
        expectedStatusCodes.add(new Integer(HttpStatus.SC_OK));
        setupRestCaller("search", username, password);
        if ((jql == null) || jql.equals("") || StringUtils.isBlank(jql))
        {
            Log.log.error("Error occurred in JIRAPI search. JQL is required to search.");
            throw new Exception("jql is required to search JIRA");
        }
        if (maxResults == null || maxResults.equals(""))
        {
            maxResults = FETCH_LIMIT;

        }
        else if(maxResults !=null && maxResults<=0) {
            Log.log.error("Error occurred in JIRAPI search. maxResults should be >=0");
            throw new Exception("maxResults should be >=0");
        }
        if((startAt!=null) && (startAt.intValue() < 0)) {
            Log.log.error("Error occurred in JIRAPI search. startAt should be >=0");
            throw new Exception("startAt should be >=0");
        }
       
        if ((fields != null) && (StringUtils.isNotEmpty(fields.trim())) && (fields.contains(",")))
        {

            StringTokenizer fieldsTokens = new StringTokenizer(fields, ",");
            while (fieldsTokens.hasMoreTokens())
            {
                fieldsArray.add(fieldsTokens.nextToken().toString());

            }
        }
        else if ((StringUtils.isNotEmpty(fields.trim())))
        {
            fieldsArray.add(fields);
        }

       
        if (startAt != null && (startAt >= 0))
        {

            postJsonBody.accumulate("startAt", startAt);
        }
        else
        {
            startAt=0;
            postJsonBody.accumulate("startAt", 0);
        }
        postJsonBody.accumulate("maxResults", maxResults);
        if (fieldsArray.size() > 0)
        {
            postJsonBody.accumulate("fields", fieldsArray);
        }
        postJsonBody.accumulate("jql", jql);
        Log.log.debug("JSON request : "+postJsonBody.toString());
        System.out.println("JSON request : "+postJsonBody.toString());

        response = restCaller.postMethod(null, null, postJsonBody.toString(), expectedStatusCodes);

        issues = parseQueryResponse(response);

        JSON jsonResponse = JSONSerializer.toJSON(response);
        if (jsonResponse instanceof JSONObject)
        {
            if (((JSONObject) jsonResponse).containsKey("total"))
            {
                total = (int) ((JSONObject) jsonResponse).get("total");
                Log.log.debug("Total JIRA's retrieved are: "+total);
                System.out.println("Total JIRA's retrieved are: "+total);
            }
        }
        int startIndex = startAt + maxResults;
        if (total > maxResults)
        {

            while (startIndex < total)
            {
                postJsonBody.remove("startAt");
                postJsonBody.accumulate("startAt", startIndex);
                Log.log.debug("JSON request : "+postJsonBody.toString());
                System.out.println("JSON request : "+postJsonBody.toString());

                response = restCaller.postMethod(null, null, postJsonBody.toString(), expectedStatusCodes);

                issues.addAll(parseQueryResponse(response));
                startIndex = startIndex + maxResults;
            }

        }
        if(issues.size()>0) {
            String firstReadJIRAID = "";
            String lastReadJiraID = "";
           
            if (issues.get(0).containsKey("key"))
            {
                firstReadJIRAID = issues.get(0).get("key");
            }
            if (issues.get(issues.size() - 1).containsKey("key"))
            {
               lastReadJiraID = issues.get(issues.size() - 1).get("key");

            }
            Log.log.debug("Total JIRA's fetched are: " + issues.size());
            System.out.println("Total: " + issues.size() + " from: " + firstReadJIRAID + " To: " + lastReadJiraID);
        }
     

        return issues;

    }

 
}
