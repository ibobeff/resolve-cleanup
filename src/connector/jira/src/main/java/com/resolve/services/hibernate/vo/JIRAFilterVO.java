package com.resolve.services.hibernate.vo;

import com.resolve.util.MappingAnnotation;
import com.resolve.util.StringUtils;

public class JIRAFilterVO extends GatewayFilterVO {

    private static final long serialVersionUID = 6061631016677683205L;
    public static final int MISSING_URL_QUERY_IN_FILTER = -1;
    public static final int MISSING_JQL_IN_FILTER = -2;
    public static final int START_AT_NOT_ALLOWED = -3;
    public static final int MAXRESULTS_NOT_ALLOWED = -4;
    public static final int ORDERBY_NOT_ALLOWED = -5;
    public static final int ONLY_ANDFIELDS_ALLOWED = -6;
      
   
    public static final String MISSING_URL_QUERY_IN_FILTER_MESSAGE = "Specify URL query for filter. Filter must have URL query.";
    public static final String QUERY_SHOULD_START_WITH_JQL_MESSAGE = "Filter URL query must start with jql=. Please enter valid JQL in filter URL query.";
    public static final String START_AT_NOT_ALLOWED_IN_QUERY_MESSAGE = "JIRA Gateway substitues value of startAt in the query to paginate the results returned by query.Please do no specify startAt parameter in URL query.";
    public static final String MAXRESULTS_NOT_ALLOWED_IN_QUERY_MESSAGE = "Please do not specify maxResults parameter in URL query. JIRA Gateway returns maximum 1000 JIRA's by default.";
    public static final String ORDERBY_NOT_ALLOWED_IN_JQL_MESSAGE = "Please do not specify order by clause in JQL query. JIRA Gateway uses order by id asc by default.";
    public static final String ONLY_FIELDS_ALLOWED ="Only &fields= is allowed in JIRA URL query.";
  
    

    public JIRAFilterVO() {
    }

    private String UUrlQuery;

    @MappingAnnotation(columnName = "URLQUERY")
    public String getUUrlQuery() {
        return this.UUrlQuery;
    }

    public void setUUrlQuery(String uUrlQuery) {
        this.UUrlQuery = uUrlQuery;
    }
    @Override
    public int hashCode()
    {
        final int prime = 31;
        int result = super.hashCode();
        result = prime * result + ((UUrlQuery == null) ? 0 : UUrlQuery.hashCode());
       
        return result;
    }
    

    @Override
    public boolean equals(Object obj)
    {
        if (this == obj)
            return true;
        
        if (!super.equals(obj))
            return false;
        
        if (getClass() != obj.getClass())
            return false;
        
        JIRAFilterVO other = (JIRAFilterVO) obj;

        if (UUrlQuery == null)
        {
            if (StringUtils.isNotBlank(other.UUrlQuery))
                return false;
        }
        else
        {
            if (!UUrlQuery.trim().equals(other.UUrlQuery == null ? "" : other.UUrlQuery.trim()))
                return false;
        }
        
      
        
        return true;
    }
    
    @Override
    public int isValid()
    {

        String query = UUrlQuery.toLowerCase();
       
        if (StringUtils.isBlank(query))
        {
            return MISSING_URL_QUERY_IN_FILTER;
        }
        
        if(query.contains("& ") )
        {
            int index = query.indexOf("& ");
            while (index >= 0) {
               
                
                String substr = query.substring(index, index+8);
                if((!substr.replace(" ","").equals("&fields")) && (!substr.equals("& fields"))) {
                    return ONLY_ANDFIELDS_ALLOWED;
                } 
                index = query.indexOf("& ", index + 1);
            }
        }
        else if(query.contains("&"))
        {
            int index = query.indexOf("&");
            while (index >= 0) {
               
               
                String substr = query.substring(index, index+7);
                if((!substr.replace(" ","").equals("&fields"))) {
                    return ONLY_ANDFIELDS_ALLOWED;
                } 
                index = query.indexOf("&", index + 1);
            }
        }
       
   /*     if(query.contains("& ")) {
            int start = query.indexOf("&");
            String substr = query.substring(start, start+8);
            if((!substr.equals("&fields")) && (!substr.equals("& fields"))) {
                return ONLY_ANDFIELDS_ALLOWED;
            } 
        }
        else if(query.contains("&")) {
            int start = query.indexOf("&");
            String substr = query.substring(start, start+7);
            if((!substr.equals("&fields")) && (!substr.equals("& fields"))) {
                return ONLY_ANDFIELDS_ALLOWED;
            } 
        }*/
       
        
        if(!query.startsWith("jql=")) {
            
            return MISSING_JQL_IN_FILTER;
        }
        if (query.contains("startat"))
        {
            return START_AT_NOT_ALLOWED;//Log.log.error("JIRA Gateway uses id to keep track of records that are read. Removing startAt clause in the JSONPost query.");
            
        }
        if (query.contains("maxresults"))
        {
            return MAXRESULTS_NOT_ALLOWED;//Log.log.error("JIRA Gateway uses id to keep track of records that are read. Removing startAt clause in the JSONPost query.");
            
        }
        if (query.contains("order by") || query.replace(" ","").contains("orderby"))
        {
            return ORDERBY_NOT_ALLOWED;//Log.log.error("JIRA Gateway uses id to keep track of records that are read. Removing startAt clause in the JSONPost query.");
            
        }
        
        return 0;
    }
    @Override
    public String getValidationError(int errorCode)
    {
        switch(errorCode)
        {
                        
            case MISSING_URL_QUERY_IN_FILTER:
                return MISSING_URL_QUERY_IN_FILTER_MESSAGE;
                
            case MISSING_JQL_IN_FILTER:
                return QUERY_SHOULD_START_WITH_JQL_MESSAGE;
                
            case START_AT_NOT_ALLOWED:
                return START_AT_NOT_ALLOWED_IN_QUERY_MESSAGE;
                
            case MAXRESULTS_NOT_ALLOWED:
                return MAXRESULTS_NOT_ALLOWED_IN_QUERY_MESSAGE;
                
            case ORDERBY_NOT_ALLOWED:
                return ORDERBY_NOT_ALLOWED_IN_JQL_MESSAGE;
                
            case ONLY_ANDFIELDS_ALLOWED:
                return ONLY_FIELDS_ALLOWED;
                
                
            default:
                return "Error code not found.";
        }
    }
    
}

