package com.resolve.gateway.jira;

import com.resolve.gateway.BaseFilter;
import com.resolve.gateway.RetainValue;
import com.resolve.util.StringUtils;

public class JIRAFilter extends BaseFilter {

    public static final String URLQUERY = "URLQUERY";
    public static final String CREATETIMESTAMP = "CREATETIMESTAMP";
    public static final String ISSUE_CREATE_TIME_STAMP_ATTRIBUTE = "created";
    public static final String LAST_READ_JIRA_ID = "LAST_READ_JIRA_ID";
    private String lastReadJiraId;
    private String createTimeStamp; // Last Issue Processed Create Time Stamp in 'yyyy-MM-dd HH:mm' format
    private String urlQuery;

    public JIRAFilter(String id, String active, String order, String interval, String eventEventId, String runbook, String script, String urlQuery,String lastReadId,String createTime) {
        super(id, active, order, interval, eventEventId, runbook, script);
        this.urlQuery = urlQuery;
        setLastReadJiraId(lastReadId);
        setLastIssueProcessedCreateTimeStamp(createTime);
    }

    @RetainValue 
    public String getLastReadJiraId()
    {
        return lastReadJiraId;
    }

    public void setLastReadJiraId(String lastReadJiraId)
    {
        this.lastReadJiraId = lastReadJiraId;
    }
 
    @RetainValue
    public String getLastIssueProcessedCreateTimeStamp()
    {
        return createTimeStamp;
    }

    public void setLastIssueProcessedCreateTimeStamp(String lastIssueProcessedCreateTimeStamp)
    {
        this.createTimeStamp = lastIssueProcessedCreateTimeStamp != null ? lastIssueProcessedCreateTimeStamp.trim() : lastIssueProcessedCreateTimeStamp;
    }
    @Override
    public String toString()
    {
        return super.toString() +  (StringUtils.isNotBlank(urlQuery) ? ", urlQuery=" + urlQuery : "") + 
                  (StringUtils.isNotBlank(createTimeStamp) ? ", createTimeStamp =" + createTimeStamp : "");
    }
    
    public String getUrlQuery() {
        return this.urlQuery;
    }

    public void setUrlQuery(String urlQuery) {
        this.urlQuery = urlQuery;
    }
}

