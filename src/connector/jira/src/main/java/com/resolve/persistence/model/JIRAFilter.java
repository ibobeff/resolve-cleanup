package com.resolve.persistence.model;

import java.util.List;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Lob;
import javax.persistence.Table;
import javax.persistence.UniqueConstraint;
import com.resolve.services.interfaces.VO;
import com.resolve.services.hibernate.vo.JIRAFilterVO;

@Entity
@Table(name = "jira_filter", uniqueConstraints = { @UniqueConstraint(columnNames = { "u_name", "u_queue" }) })
public class JIRAFilter extends GatewayFilter<JIRAFilterVO> {

    private static final long serialVersionUID = 1L;

    public JIRAFilter() {
    }

    public JIRAFilter(JIRAFilterVO vo) {
        applyVOToModel(vo);
    }

    private String UUrlQuery;

    @Lob
    @Column(name = "u_urlquery", length = 16777215)
    public String getUUrlQuery() {
        return this.UUrlQuery;
    }

    public void setUUrlQuery(String uUrlQuery) {
        this.UUrlQuery = uUrlQuery;
    }

    public JIRAFilterVO doGetVO() {
        JIRAFilterVO vo = new JIRAFilterVO();
        super.doGetBaseVO(vo);
        vo.setUUrlQuery(getUUrlQuery());
        return vo;
    }

    @Override
    public void applyVOToModel(JIRAFilterVO vo) {
        if (vo == null) return;
        super.applyVOToModel(vo);
        if (!VO.STRING_DEFAULT.equals(vo.getUUrlQuery())) this.setUUrlQuery(vo.getUUrlQuery()); else ;
    }

    @Override
    public List<String> ugetCdataProperty() {
        List<String> list = super.ugetCdataProperty();
        list.add("UUrlQuery");
        return list;
    }
}

