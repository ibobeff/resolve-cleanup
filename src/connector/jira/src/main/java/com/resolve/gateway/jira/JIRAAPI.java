package com.resolve.gateway.jira;

import java.util.List;
import java.util.Map;

import com.resolve.gateway.AbstractGatewayAPI;

import net.sf.json.JSON;

public class JIRAAPI extends AbstractGatewayAPI
{
    
    static
    {
        instance = JIRAGateway.getInstance();
    }

    // SDKJIRA Specific Methods
/**
 * Returns general information about the current JIRA server.
 * @param username If passed null then the username configured in blueprint properties file is used.
 * @param password If passed null then the password configured in blueprint properties file is used.
 * @return Returns a Map representation of the server info.
 * Returned map has information about - baseUrl,version,versionNumbers,deploymentType,buildNumber,buildDate,serverTime,scmInfo,serverTitle
 * @throws Exception
 */
    public static Map<String, String> getServerInfo(String username, String password) throws Exception
    {
        return ((JIRAGateway) instance).getServerInfo(username, password);
    }

    
    /**
     * Search JIRA based on jql. Return all JIRA's that are returned by the query.
     * @param jql - Required.a JQL query string.
     * @param startAt - null or the index of the first issue to return (0-based)
     * @param maxResults -null or the maximum number of issues to return (defaults to Gateway queue size. Gateway queue size is 1000 by default). 
     * @param fields - Comma seperated list of fields to return for each issue. By default, all navigable fields are returned
     * @param username - A valid user in JIRA. If passed null then the username configured in blueprint properties file is used.
     * @param password -password Password for the user. If passed null then the password configured in blueprint properties file is used.
     * @return List of JIRA's fetched. Fields of each JIRA are in Map<String,String>
     * @throws Exception
     */
   public List<Map<String, String>> search(String jql, Integer startAt,Integer maxResults,String fields,
                    String username, 
                    String password) throws Exception{
        return ((JIRAGateway) instance).search(jql, startAt,maxResults,fields, username, password);
    }

    /**
     * Adds a new comment to an issue.
     * @param key JIRA Key
     * @param comment Comment to be added
     * @param username A valid user in JIRA. If passed null then the username configured in blueprint properties file is used.
     * @param password Password for the user. If passed null then the password configured in blueprint properties file is used.
     * @return status of addComment request
     * @throws Exception
     */
    public static boolean addComment(String key, String comment, String username, String password) throws Exception
    {
        return ((JIRAGateway) instance).addComment(key, comment, username, password);
    }

    
    /**
     * 
     * @param username -A valid user in JIRA. If passed null then the username configured in blueprint properties file is used.
     * @param password -Password for the user. If passed null then the password configured in blueprint properties file is used.
     * @param jsonParams - Valid Fields and values that are required to create an issue as a JSON String
     * 
     * e.g {
    "fields": {
       "project":
       { 
          "key": "JIR"
       },
       "summary": "Created Test Issue Through REST Call",
       "description": "Creating of an issue using project JIR and issue type names using the REST API",
       "issuetype": {
          "name": "Test Run"
       },
       "customfield_10120": {
        "id": "10114",
         "children": [
                    {
                      
                      "id": "10433"
                    }]
       }
      
   }
}
     * @return Key of the created JIRA
     * @throws Exception
     */
    public static String createIssue(String username, String password, JSON jsonParams) throws Exception
    {
       return ((JIRAGateway) instance).createIssue(username, password, jsonParams);
    }
    
    /**
     * Returns all projects which are visible for the user. To create an issue
     * in JIRA you need to specify a Project. This call is used to get
     * Project information before creating an issue.
     * @return Returns a list of projects for which the user has the BROWSE, ADMINISTER or PROJECT_ADMIN project permission.
     * @throws Exception
     */
    public static List<Map<String, String>> getAllProjects() throws Exception
    {
        return ((JIRAGateway) instance).getAllProjects();
    }
    
    /**
     * Returns a list of all issue types visible to the user. To create an issue
     * in JIRA you need to specify a Issue Type. This call is used to get
     * issueType information before creating an issue. 
     *  @return Returns a list of all issue types visible to the user.
     * 
     * @throws Exception
     */
    public static List<Map<String, String>>  getAllIssueTypes() throws Exception
    {

        return ((JIRAGateway) instance).getAllIssueTypes();
    }
    
    /**  Retrieve the information for the issue fields based on given projectKey and/or issuetype
     * You need project and\or issue type,to retrieve the information for the issue fields.
     * @param projectKey required (Get it using getAllProjects() Gateway API call )
     * @param issueType optional(Get it using getAllIssueTypes() Gateway API call)
     * @return what fields are available for an issue,what the default values are and what fields are mandatory.
     * @throws Exception
     */
    public static String getIssueFields(String projectKey,String issueType) throws Exception
    {
        return ((JIRAGateway) instance).getIssueFields(projectKey, issueType);
    }
    


    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
 /*    *//**
     * Returns {@link Set} of supported object types.
     * <p>
     * Default no object types are supported returning {@link Collections.EMPTY_SET}.
     * Derived external system specific gateway API must override this method to return
     * {@link Set<String>} of supported object types.
     * 
     * @return {@link Set} of supported object types
     *//*
    public static Set<String> getObjectTypes()
    {
        return Collections.emptySet();
    }
    

    *//**
     * Creates instance of specified object type in external system.
     * <p>
     * Default attribute name-value pair returned is  
     * {@value com.resolve.gateway.GatewayConstants#METHOD_NOT_SUPPORTED_KEY}-true
     * indicating external system specific gateway does not support creating instances of
     * specified object type.
     * <p>
     * If external system specific gateway does support creation of object then it must return
     * {@link Map} of attribute name-value pairs.
     * 
     * @param  objType   Type of object to create in external system
     * @param  objParams {@link Map} of object parameters
     * @param  userName  User name
     * @param  password  Password
     * @return           Attribute id-value {@link Map} of created object in external system
     * @throws Exception If any error occurs in creating object of specified
     *                   type in external system
     *//*
    public static Map<String, String> createObject(String objType, Map<String, String> objParams, String userName, String password) throws Exception
    {
        return null;
    }
    
    *//**
     * Reads attributes of specified object in external system.
     * <p>
     * Default attribute id-value pair returned is  
     * {@value com.resolve.gateway.GatewayConstants#METHOD_NOT_SUPPORTED_KEY}-true
     * indicating external system specific gateway does not support reading atributes of
     * object.
     * <p>
     * If external system specific gateway does support reading attributes of object then 
     * it must return {@link Map} of attribute name-value pairs. 
     *
     * @param  objType    Object type
     * @param  objId      Id of the object to read attributes of from external system
     * @param  attribs    {@link List} of attributes of object to read
     * @param  userName   User name
     * @param  password   Password
     * @return            {@link Map} of object attribute id-value pairs
     * @throws Exception  If any error occurs in reading attributes of the object in 
     *                    external system
     *//*
    public static Map<String, String> readObject(String objType, String objId, List<String> attribs, String userName, String password) throws Exception
    {
        return null;
    }
    
    *//**
     * Updates attributes of specified object in external system.
     * <p>
     * Default updated attribute name-value pair returned is  
     * {@value com.resolve.gateway.GatewayConstants#METHOD_NOT_SUPPORTED_KEY}-true
     * indicating external system specific gateway does not support updating attributes of
     * object.
     * <p>
     * If external system specific gateway does support updating attributes of object then it 
     * must return updated {@link Map} of attribute name-value pairs.
     * 
     * @param  objType   Object type
     * @param  objId     Id of the object to update attributes of in external system
     * @param  updParams Key-value {@link Map} of object attributes to update
     * @param  userName  User name
     * @param  password  Password
     * @return           Key-value {@link Map} of the updated object attributes
     * @throws Exception If any error occurs in updating attributes of the object in 
     *                   external system
     *//*
    public static Map<String, String> updateObject(String objType, String objId, Map<String, String> updParams, String userName, String password) throws Exception
    {
        return null;
    }
    
    *//**
     * Deletes specified object from external system.
     * <p>
     * Default deleted attribute name-value pair returned is  
     * {@value com.resolve.gateway.GatewayConstants#METHOD_NOT_SUPPORTED_KEY}-true
     * indicating external system specific gateway does not support object deletion.
     * <p>
     * If external system specific gateway does support object deletion then it 
     * must return {@link Map} of object deletion operation reult.
     * 
     * @param  objType   Object type
     * @param  objId     Id of the object to delete from external system
     * @param  userName  User name
     * @param  password  Password
     * @return           Key-value {@link Map} result of object delete operation
     * @throws Exception If any error occurs in deleting object from external system
     *//*
    public static Map<String, String> deleteObject(String objType, String objId, String userName, String password) throws Exception
    {
        return null;
    }
    
    *//**
     * Get list of objects from external system matching specified filter condition.
     * <p>
     * Default implementation returns {@link List} containing {@link Map} with 
     * {@value com.resolve.gateway.GatewayConstants#METHOD_NOT_SUPPORTED_KEY}-true indicating
     * external system specific gateway does not support getting objects matching filter condition.
     * <p>
     * If external system specific gateway does support getting objects matching filter condition then it 
     * must return {@link List} of {@link Map} of matching objects attribute id-value pairs.
     * 
     * @param  objType  Object type
     * @param  filter   External system gateway specific filer conditions
     * @param  userName User name
     * @param  password Password
     * @return          {@link List} of {@link Map}s of object attribute id-value pairs matching
     *                  filter condition
     * @throws Exception If any error occurs in getting object from external system
     *//*
    
    public static List<Map<String, String>> getObjects(String objType, BaseFilter filter, String userName, String password) throws Exception
    {
        List<Map<String, String>> objs = new ArrayList<Map<String, String>>();
        
        objs.add(null);
        
        return objs;
    }*/
}


