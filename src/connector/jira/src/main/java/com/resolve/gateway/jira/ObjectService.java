/******************************************************************************
* (C) Copyright 2015
*
* Resolve Systems, LLC
*
* All rights reserved
* This software is distributed under license and may not be used, copied,
* modified, or distributed without the express written permission of
* Resolve Systems, LLC.
*
******************************************************************************/

package com.resolve.gateway.jira;

import java.util.List;
import java.util.Map;

import net.sf.json.JSON;

public interface ObjectService
{
    Map<String, String> getServerInfo(String username, 
                                      String password) throws Exception;
    
    List<Map<String, String>> search(String urlQuery, 
                                     String username, 
                                     String password) throws Exception;
    
    List<Map<String, String>> search(String jql, Integer startAt,Integer maxResults,String fields,
                    String username, 
                    String password) throws Exception;
    
    boolean addComment(String key,
                       String comment,
                       String username, 
                       String password) throws Exception;
    
 

    String createIssue(String username, String password, JSON jsonParams) throws Exception;
    
    List<Map<String, String>> getAllProjects()throws Exception;
    
    List<Map<String, String>> getAllIssueTypes()throws Exception;
    
    String getIssueFields(String projectKey,String issueType)throws Exception;
    
    String validateJqlQuery(String urlQuery,String createTimeStamp,String lastReadJiraID,boolean hasVariable) throws Exception;
    
    List<Map<String, String>> parseQueryResponse(String response) throws Exception;
    Map<String,String> getIssueFieldsIdNameMap(String username, String password) throws Exception;
    
}