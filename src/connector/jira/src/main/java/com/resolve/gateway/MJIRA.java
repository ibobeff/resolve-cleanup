package com.resolve.gateway;

import java.util.Map;
import com.resolve.gateway.jira.JIRAFilter;
import com.resolve.gateway.jira.JIRAGateway;

public class MJIRA extends MGateway {

    private static final String RSCONTROL_SET_FILTERS = "MJIRA.setFilters";

    private static final JIRAGateway instance = JIRAGateway.getInstance();

    public MJIRA() {
        super.instance = instance;
    }

    protected String getSetFilterMethodName() {
        return RSCONTROL_SET_FILTERS;
    }

    /**
     * Populates the {@link Filter} data into the given filterMap {@link Map} instance
     * 
     * @param filterMap
     *      the {@link Map} instance to hold the {@link Filter} data
     * @param filter
     *      a {@link JIRAFilter} instance
     *            
     */
    public void populateGatewaySpecificFilterProperties(Map<String, String> filterMap, Filter filter) {
        if (instance.isActive()) {
            JIRAFilter jira1Filter = (JIRAFilter) filter;
            filterMap.put(JIRAFilter.URLQUERY, jira1Filter.getUrlQuery());
        }
    }
}

