package com.resolve.rscontrol;

import com.resolve.persistence.model.ZENOSSFilter;

public class MZENOSS extends MGateway {

    private static final String MODEL_NAME = ZENOSSFilter.class.getSimpleName();

    protected String getModelName() {
        return MODEL_NAME;
    }
}

