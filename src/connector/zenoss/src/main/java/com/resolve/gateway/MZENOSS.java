package com.resolve.gateway;

import java.util.Map;
import com.resolve.gateway.zenoss.ZENOSSFilter;
import com.resolve.gateway.zenoss.ZENOSSGateway;

public class MZENOSS extends MGateway {

    private static final String RSCONTROL_SET_FILTERS = "MZENOSS.setFilters";

    private static final ZENOSSGateway instance = ZENOSSGateway.getInstance();

    public MZENOSS() {
        super.instance = instance;
    }

    protected String getSetFilterMethodName() {
        return RSCONTROL_SET_FILTERS;
    }

    /**
     * Populates the {@link Filter} data into the given filterMap {@link Map} instance
     * 
     * @param filterMap
     *      the {@link Map} instance to hold the {@link Filter} data
     * @param filter
     *      a {@link ZENOSSFilter} instance
     *            
     */
    public void populateGatewaySpecificFilterProperties(Map<String, String> filterMap, Filter filter) {
        if (instance.isActive()) {
            ZENOSSFilter zenossFilter = (ZENOSSFilter) filter;
            filterMap.put(ZENOSSFilter.DEVICEIP, zenossFilter.getDeviceIp());
            filterMap.put(ZENOSSFilter.EVENTSTAUS, zenossFilter.getEventStaus());
            filterMap.put(ZENOSSFilter.FIRSTSEENDATETIME, zenossFilter.getFirstSeenDateTime());
            filterMap.put(ZENOSSFilter.LASTSEENDATETIME, zenossFilter.getLastSeenDateTime());
        }
    }
}

