package com.resolve.services.hibernate.vo;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;


import com.resolve.util.MappingAnnotation;
import com.resolve.util.StringUtils;

public class ZENOSSFilterVO extends GatewayFilterVO {

    private static final long serialVersionUID = 1L;
    public static final int FIRST_SEEN_DATE_FORMAT_WRONG = -1;
    public static final int LAST_SEEN_DATE_FORMAT_WRONG = -2;
    public static final String FIRST_SEEN_DATE_FORMAT_WRONG_MESSAGE = "First Seen Date is formatted incorrectly - format should be yyyy-mm-dd HH:mm:ss";
    public static final String LAST_SEEN_DATE_FORMAT_WRONG_MESSAGE = "Last Seen Date is formatted incorrectly - format should be yyyy-mm-dd HH:mm:ss";
    

    public ZENOSSFilterVO() {
    }

    private String UDeviceIp;

    private String UEventStaus;

    private String UFirstSeenDateTime;

    private String ULastSeenDateTime;

    @MappingAnnotation(columnName = "DEVICEIP")
    public String getUDeviceIp() {
        return this.UDeviceIp;
    }

    public void setUDeviceIp(String uDeviceIp) {
        this.UDeviceIp = uDeviceIp;
    }

    @MappingAnnotation(columnName = "EVENTSTAUS")
    public String getUEventStaus() {
        return this.UEventStaus;
    }

    public void setUEventStaus(String uEventStaus) {
        this.UEventStaus = uEventStaus;
    }

    @MappingAnnotation(columnName = "FIRSTSEENDATETIME")
    public String getUFirstSeenDateTime() {
        return this.UFirstSeenDateTime;
    }

    public void setUFirstSeenDateTime(String uFirstSeenDateTime) {
        this.UFirstSeenDateTime = uFirstSeenDateTime;
    }

    @MappingAnnotation(columnName = "LASTSEENDATETIME")
    public String getULastSeenDateTime() {
        return this.ULastSeenDateTime;
    }

    public void setULastSeenDateTime(String uLastSeenDateTime) {
        this.ULastSeenDateTime = uLastSeenDateTime;
    }
    
    @Override
    public int isValid()
    {
        if(StringUtils.isNotBlank(UFirstSeenDateTime) && (!UFirstSeenDateTime.equals("${FIRST_SEEN}"))) 
        {
            String formatString = "yyyy-MM-dd hh:mm:ss";
           
                try {
                    Date date = new SimpleDateFormat(formatString).parse(UFirstSeenDateTime);
                                      
                } catch (ParseException e) {
                    return FIRST_SEEN_DATE_FORMAT_WRONG;
                }
            
            
        }
        
        if(StringUtils.isNotBlank(ULastSeenDateTime) && (!ULastSeenDateTime.equals("${LAST_SEEN}"))) 
        {
            String formatString = "yyyy-MM-dd hh:mm:ss";
           
                try {
                    Date date = new SimpleDateFormat(formatString).parse(ULastSeenDateTime);
                                      
                } catch (ParseException e) {
                    return LAST_SEEN_DATE_FORMAT_WRONG;
                }
            
            
        }
        return 0;
        
    }
    
    @Override
    public String getValidationError(int errorCode)
    {
        switch(errorCode)
        {
                        
            case FIRST_SEEN_DATE_FORMAT_WRONG:
                return FIRST_SEEN_DATE_FORMAT_WRONG_MESSAGE;
                
            case LAST_SEEN_DATE_FORMAT_WRONG:
                return LAST_SEEN_DATE_FORMAT_WRONG_MESSAGE;
                
            default:
                return "Error code not found.";
        }
    }
}

