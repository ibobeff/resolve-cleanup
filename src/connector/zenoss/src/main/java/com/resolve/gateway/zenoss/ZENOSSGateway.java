package com.resolve.gateway.zenoss;

import java.util.Map;
import java.util.TimeZone;

import java.util.List;
import java.math.BigDecimal;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;

import com.resolve.rsbase.MainBase;
import com.resolve.gateway.BaseClusteredGateway;
import com.resolve.gateway.BaseFilter;
import com.resolve.gateway.Filter;
import com.resolve.gateway.MZENOSS;
import com.resolve.util.Log;
import com.resolve.util.StringUtils;
import com.resolve.rsremote.ConfigReceiveGateway;


public class ZENOSSGateway extends BaseClusteredGateway
{

    private static volatile ZENOSSGateway instance = null;
    public static volatile ObjectService genericObjectService;

    private String queue = null;

    public static ZENOSSGateway getInstance(ConfigReceiveZENOSS config)
    {
        if (instance == null)
        {
            instance = new ZENOSSGateway(config);
        }
        return instance;
    }

    /**
     * This method is called once the initialization is done. If it gets called
     * before the initialization it will throw exception.
     *
     * @return
     */
    public static ZENOSSGateway getInstance()
    {
        if (instance == null)
        {
            throw new RuntimeException("ZENOSS Gateway is not initialized correctly..");
        }
        else
        {
            return instance;
        }
    }

    private ZENOSSGateway(ConfigReceiveZENOSS config)
    {
        super(config);
    }

    @Override
    public String getLicenseCode()
    {
        return "ZENOSS";
    }

    @Override
    protected void addAdditionalValues(Map<String, Object> arg0, Map<String, String> arg1)
    {
    }

    @Override
    protected String getGatewayEventType()
    {
        return "ZENOSS";
    }

    @Override
    protected String getMessageHandlerName()
    {
        return MZENOSS.class.getSimpleName();
    }

    @Override
    protected Class<MZENOSS> getMessageHandlerClass()
    {
        return MZENOSS.class;
    }

    public String getQueueName()
    {
        return queue;
    }

    @Override
    public void start()
    {
        Log.log.debug("Starting ZENOSS Gateway");
        super.start();
    }

    @Override
    public void run()
    {
        if (!((ConfigReceiveGateway) configurations).isMock()) super.sendSyncRequest();
        
        Date curentUtcTime = Date.from(java.time.ZonedDateTime.now().toInstant());
        SimpleDateFormat sdf = new SimpleDateFormat( "yyyy-MM-dd HH:mm:ss" );
        sdf.setTimeZone( TimeZone.getTimeZone( "UTC" ) );
        String currentUtcTime = sdf.format(curentUtcTime);
        ConfigReceiveZENOSS config = (ConfigReceiveZENOSS) configurations;
        while (running)
        {
            try
            {
                long startTime = System.currentTimeMillis();
                if (primaryDataQueue.isEmpty())
                {
                    Log.log.trace("Local Primary Event Queue for " + getGatewayEventType() + " is empty.....");
                    for (Filter filter : orderedFilters)
                    {
                        /*genericObjectService.acknowledge("0242ac11-0014-8bd9-11e7-c0e857a424b2");
                        genericObjectService.unacknowledge("0242ac11-0014-8bd9-11e7-c0e857a424b2");
                        genericObjectService.writeLog("0242ac11-0014-8bd9-11e7-c0e857a424b2", "Gateway added log comment");
                        genericObjectService.closeEvent("0242ac11-0014-8bd9-11e7-c0e857a424b2");*/
                        
                        if (shouldFilterExecute(filter, startTime))
                        {
                            ZENOSSFilter zenossFilter = (ZENOSSFilter) filter;
                            String firstSeenPersistedDateonFilter = zenossFilter.getPersistedFirstSeenTime();
                            String firstSeenDateonFilter = zenossFilter.getFirstSeenDateTime().trim();
                            String userTimeZone = config.getUserTimeZone();     
                           
                           /* if (lastSeenDateonFilter.equals("${LAST_SEEN}"))
                            {
                                zenossFilter.setLastSeenDateTime(currentUtcTime);
                            }*/
                            
                            if (StringUtils.isBlank(firstSeenPersistedDateonFilter))
                            {
                               
                                if (firstSeenDateonFilter.equals("${FIRST_SEEN}"))
                                {
                                    zenossFilter.setPersistedFirstSeenTime(currentUtcTime);
                                }
                                else if(StringUtils.isBlank(userTimeZone)){
                                    throw new Exception("Please specify the Zenos User TimeZone in the blueprint property");
                                    
                                }
                                else if(StringUtils.isNotBlank(firstSeenDateonFilter)){
                                    
                                    sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
                                    sdf.setTimeZone(TimeZone.getTimeZone(userTimeZone));
                                  //  userTimeZone="";
                                    System.out.println("Default timezone is: "+TimeZone.getTimeZone(userTimeZone).getID());
                                    Date firstSeenDateOnFilterInUserTimeZone =  sdf.parse(firstSeenDateonFilter);
                                    sdf.setTimeZone( TimeZone.getTimeZone("UTC"));
                                    String firstSeenDateonFilterToUTC=sdf.format(firstSeenDateOnFilterInUserTimeZone);
                                    zenossFilter.setPersistedFirstSeenTime(firstSeenDateonFilterToUTC);
                                }
                            }
                            Log.log.trace("Filter " + filter.getId() + " executing");
                            List<Map<String, String>> results = invokeService(filter);
                            if (results.size() > 0)
                            {
                                Map<String, String> firstReadEvent = results.get(0);
                                /*if(firstReadEvent.containsKey("evid")) {
                                    String firstReadEvidOfcurrentFilterInterval = firstReadEvent.get("evid");
                                    String lastReadEvidOfPrviousFilterInterval = zenossFilter.getPersistedLastReadEvenEvid();
                                    System.out.println("Current First: "+firstReadEvidOfcurrentFilterInterval+"  Prev Last: "+lastReadEvidOfPrviousFilterInterval);
                                    
                                    if(firstReadEvidOfcurrentFilterInterval.equals(lastReadEvidOfPrviousFilterInterval)) {
                                        results.remove(0);
                                    }
                                }*/
                                String persistFirstSeenTime = "";
                                if (results.get(results.size() - 1).containsKey("firstTime"))
                                {
                                    persistFirstSeenTime = results.get(results.size() - 1).get("firstTime");
                                    persistFirstSeenTime=persistFirstSeenTime.replaceAll(",","");
                                   // String dedupId = results.get(results.size() - 1).get("dedupid");
                                    BigDecimal decimal = new BigDecimal(persistFirstSeenTime);
                                    long value = (long)(decimal.doubleValue()*1000);
                                    sdf.setTimeZone(TimeZone.getTimeZone("UTC"));
                                    String epochToUTCDate = sdf.format(new Date(value));
                                    sdf.setTimeZone(TimeZone.getTimeZone( config.getUserTimeZone() ));
                                    String epochToUserTimeZone = sdf.format(new Date(value));
                                    Log.log.debug("First Seent Time of the Last event fetched at this filter interval is: "+epochToUserTimeZone);
                                    System.out.println("Total: "+results.size()+"   "+" Epoch Time: "+decimal+" User Timezone time: "+epochToUserTimeZone+"  UTC Time: "+epochToUTCDate);
                                    Log.log.debug("Total Fetched Events: "+results.size());
                                    Log.log.debug("TimeStamp Of the Last Fetched Event During this Filter Interval===>   "+" Epoch Time: "+decimal+" User Timezone time: "+epochToUserTimeZone+"  UTC Time: "+epochToUTCDate);
                                    zenossFilter.setPersistedFirstSeenTime(epochToUTCDate);
                                  
                                }
                                /*if (results.get(results.size() - 1).containsKey("evid"))
                                {
                                    zenossFilter.setPersistedLastReadEvenEvid(results.get(results.size() - 1).get("evid"));    
                                }*/
                                
                                
                                
                            }
                            for (Map<String, String> result : results)
                            {
                                addToPrimaryDataQueue(filter, result);
                            }
                        }
                        else
                        {

                            Log.log.trace("Filter:" + filter.getId() + " is inactive or not ready to execute yet.....");
                        }
                    }
                    if (orderedFilters == null || orderedFilters.size() == 0)
                    {
                        Log.log.trace("There is not deployed filter for ZENOSSGateway");
                    }
                }
                if (orderedFilters.size() == 0)
                {
                    Thread.sleep(interval);
                }
            }
            catch (Exception e)
            {
                Log.log.warn("Warning: " + e.getMessage());
                Log.alert("Instance " + MainBase.main.configId.getGuid() + " " + getLicenseCode() + "-" + getQueueName() + "-" + getInstanceType() + " Polling Failed", "Instance " + MainBase.main.configId.getGuid() + " " + getLicenseCode() + "-" + getQueueName() + "-" + getInstanceType() + " Polling Failed " + "due to " + e.getMessage());
                try
                {

                    Thread.sleep(interval);
                }
                catch (InterruptedException ie)
                {
                    Log.log.warn("sleep interrupted: " + ie.getMessage(), ie);
                }
            }
        }
    }

    /**
     * Add customized code here to get the data as List<Map<String, String>>
     * format for each filter
     * 
     * @return List<Map<String, String>>
     */
    public List<Map<String, String>> invokeService(Filter filter)
    {
        List<Map<String, String>> result = new ArrayList<Map<String, String>>();
        ConfigReceiveZENOSS config = (ConfigReceiveZENOSS) configurations;
        ZENOSSFilter zenossFilter = (ZENOSSFilter) filter;
        ArrayList<String> state = new ArrayList<String>();
        state.add(zenossFilter.getEventStaus());
       
        try
        {
                     
            result = genericObjectService.queryEvents(state,  zenossFilter.getPersistedFirstSeenTime(), zenossFilter.getLastSeenDateTime(), zenossFilter.getDeviceIp(),config.getPrimaryDataQueueExecutorQueueSize());
            if (result.size() > 0)
            {
                acknowledgeEvents(result);
            }

        }

        catch (Exception e)
        {
            Log.log.error(e.getMessage(), e);
        }

        // Add customized code here to get data from 3rd party system based the
        // information in the filter;
        return result;
    }

    private void acknowledgeEvents(List<Map<String, String>> events)
    {
        for (Map<String, String> event : events)
        {
            if (event.containsKey("evid"))
            {
                try
                {
                    if (!genericObjectService.acknowledge(event.get("evid")))
                    {
                        Log.log.error("Gateway Failed to Acknowledge Event: " + event.get("evid"));
                    }
                }
                catch (Exception e)
                {
                    Log.log.error(e.getMessage(), e);
                    Log.log.error("Gateway Failed to Acknowledge Event: " + event.get("evid"));
                }
            }

        }

    }

    
    public boolean acknowledge(String evid) throws Exception
    {
        return genericObjectService.acknowledge(evid);
    }
    
    public boolean unAcknowledge(String evid) throws Exception
    {
        return genericObjectService.unacknowledge(evid);
    }
    
    public boolean writeLog(String evid,String message) throws Exception
    {
        return genericObjectService.writeLog(evid, message);
    }
    
    public boolean closeEvent(String evid) throws Exception
    {
        return genericObjectService.closeEvent(evid);
    }
    
    private String getDevicePriorities() throws Exception 
    {
        // TODO Auto-generated method stub}
       return genericObjectService.getDevicePriorities();
       
    }
    public List<Map<String, String>> queryEvents(ArrayList<String> eventState, String firstTime,String lastTime,String device,int maxResultsToFetch) throws Exception
    {
        return genericObjectService.queryEvents(eventState, firstTime, lastTime, device, maxResultsToFetch);
    }
    
    public boolean checkGatewayConnection() throws Exception
    {
        
        
        try
        {
            String response = getDevicePriorities(); 
            
            if (response == null || StringUtils.isBlank(response))
            {
                throw new Exception ("Failed to get response from Zenoss. Connection to Zenoss server possibly broken.");
            }
            else
            {
                Log.log.debug("Instance " + MainBase.main.configId.getGuid() + " : " + getLicenseCode() + "-" + 
                              getQueueName() + "-" + getInstanceType() + " connected to configured Zenoss server.");
            }
        }
        catch(Exception e)
        {
            Log.alert("Instance " + MainBase.main.configId.getGuid() + " breached " + getLicenseCode() + "-" + getQueueName() + "-" + getInstanceType() + "-ConnectionSelfCheck", 
                      "Instance " + MainBase.main.configId.getGuid() + "Breached " + getLicenseCode() + "-" + getQueueName() + "-" + getInstanceType() + "-ConnectionSelfCheck" + 
                      " due to " + e.getMessage());
        }
        
        return true;
    }
    
    /**
     * Add customized code here to initilize the connection with 3rd party
     * system
     * 
     * @return List<Map<String, String>>
     */
    @Override
    protected void initialize()
    {
        ConfigReceiveZENOSS config = (ConfigReceiveZENOSS) configurations;
        queue = config.getQueue().toUpperCase();
        this.gatewayConfigDir = "/config/zenoss/";
        genericObjectService = new GenericObjectService(config);
        if(StringUtils.isBlank(config.getUserTimeZone())) {
            Log.alert("Instance " + MainBase.main.configId.getGuid() + " breached " + getLicenseCode() + "-" + getQueueName() + "-" + getInstanceType() + "-BlueprintProperties Required Fields Value Missing","Please specify UserTimeZone in bleprint properties.");
        }
        if(StringUtils.isBlank(config.getZenoss_host())) {
            Log.alert("Instance " + MainBase.main.configId.getGuid() + " breached " + getLicenseCode() + "-" + getQueueName() + "-" + getInstanceType() + "-BlueprintProperties Required Fields Value Missing","Please specify Host Address in bleprint properties.");
        }

        if(StringUtils.isBlank(config.getZenoss_port())) {
            Log.alert("Instance " + MainBase.main.configId.getGuid() + " breached " + getLicenseCode() + "-" + getQueueName() + "-" + getInstanceType() + "-BlueprintProperties Required Fields Value Missing","Please specify Http/Https Zenoss App Port in bleprint properties.");
        }
        

        // Add customized code here to initilize the connection with 3rd party
        
        // system;
    }

    /**
     * Add customized code here to do when stop the gateway
     */
    @Override
    public void stop()
    {
        Log.log.warn("Stopping ZENOSS gateway");
        // Add customized code here to stop the connection with 3rd party
        // system;
        super.stop();
    }

    @Override
    public Filter getFilter(Map<String, Object> params)
    {
        return new ZENOSSFilter((String) params.get(BaseFilter.ID), (String) params.get(BaseFilter.ACTIVE), (String) params.get(BaseFilter.ORDER), (String) params.get(BaseFilter.INTERVAL), (String) params.get(BaseFilter.EVENT_EVENTID), (String) params.get(BaseFilter.RUNBOOK), (String) params.get(BaseFilter.SCRIPT), (String) params.get(ZENOSSFilter.DEVICEIP), (String) params.get(ZENOSSFilter.EVENTSTAUS), (String) params.get(ZENOSSFilter.FIRSTSEENDATETIME),
                        (String) params.get(ZENOSSFilter.LASTSEENDATETIME), (String) params.get(ZENOSSFilter.FIRST_SEEN_TIME));//,(String) params.get(ZENOSSFilter.LAST_READ_EVID));
    }
}
