package com.resolve.gateway.zenoss;

import java.math.BigDecimal;
import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.resolve.util.Log;
import com.resolve.util.StringUtils;
import com.resolve.util.restclient.RestCaller;

import net.sf.json.JSONArray;
import net.sf.json.JSONObject;

public class GenericObjectService implements ObjectService
{
    protected final ConfigReceiveZENOSS configurations;
    private static boolean selfSigned = false;
    private static boolean isSsl = false;
    private static String baseZenossUrl = null;

    private static String CONTENT_TYPE = "application/json";
    private static String ACCEPT_TYPE = "application/json";
    
    public GenericObjectService(ConfigReceiveZENOSS configurations)
    {
        String host = configurations.getZenoss_host();
        String port=configurations.getZenoss_port();
        String sslType = configurations.getSsltype();

        isSsl = java.lang.Boolean.parseBoolean(configurations.getSsl());
        selfSigned = ( sslType== null || sslType.length() == 0 || sslType.equalsIgnoreCase("self-signed")) ? true : false;
        if(isSsl) {
            baseZenossUrl="https://";
             }
        else
        {    baseZenossUrl ="http://";}
        baseZenossUrl =baseZenossUrl +host+":"+port;
        
        this.configurations = configurations;
    }
   

    @Override
    public List<Map<String, String>> queryEvents(ArrayList<String> eventState, String firstTime,String lastTime,String device,int maxResultsToFetch) throws Exception
    {
        // TODO Auto-generated method stub
        RestCaller restCallerOld = new RestCaller(baseZenossUrl, configurations.getHttpbasicauthusername(), configurations.getHttpbasicauthpassword());
        restCallerOld.setUrlSuffix("zport/dmd/Events/evconsole_router");
        String method = "query";
        String action = "EventsRouter";
        String sortOn = "firstTime";
        int stateCode = 0;
        ;

        Collection<Integer> statusCode = new ArrayList<>();
        statusCode.add(200);

        int limit = 200;
        int start = 0;
        int page = 0;
        List<Map<String, String>> pulledEvents = new ArrayList<Map<String, String>>();
      
        JSONArray severityArray = new JSONArray();
        severityArray.add(5);
        severityArray.add(4);
        severityArray.add(3);
        severityArray.add(2);
        severityArray.add(1);
        severityArray.add(0);
        
        JSONArray eventStateArr = new JSONArray();

        for (String state : eventState)
        {
            switch (state)
            {
                case "New":
                    stateCode = 0;
                    break;
                case "Acknowledged":
                    stateCode = 1;
                    break;
                case "Suppressed":
                    stateCode = 2;
                    break;
                case "Closed":
                    stateCode = 3;
                    break;
                case "Cleared":
                    stateCode = 4;
                    break;
                case "Aged":
                    stateCode = 5;
                    break;
                default:
                    throw new Exception("Please provide valid state");
            }
            eventStateArr.add(stateCode);
        }

        for (;;)
        {
            JSONObject eventData = new JSONObject();
            JSONObject params = new JSONObject();

            eventData.accumulate("limit", limit);
            eventData.accumulate("start", start);
            eventData.accumulate("page", page);
            eventData.accumulate("sort", sortOn);
            eventData.accumulate("dir", "ASC");
            eventData.accumulate("detailFormat", true);
            params.accumulate("eventState", eventStateArr);
            params.accumulate("severity", severityArray);
            params.accumulate("firstTime", firstTime);
            params.accumulate("lastTime", lastTime);
            if(device!="" && StringUtils.isNotEmpty(device)) {
                params.accumulate("device", device + "*");
            }
            
            eventData.accumulate("params", params);

            JSONArray data = new JSONArray();
            data.add(eventData);
            eventData.clear();

            eventData.accumulate("method", method);
            eventData.accumulate("action", action);
            eventData.accumulate("data", data);
            eventData.accumulate("tid", 1);

            try
            {
                Log.log.error("Query Events Criteria is===>" + StringUtils.jsonObjectToString(eventData));
                String response = restCallerOld.postMethod(null, null, StringUtils.jsonObjectToString(eventData), statusCode);
               
                if (restCallerOld.getStatusCode() != 200)
                {
                    Log.log.error("Query Returned no records.. Inputs Params are===>" + StringUtils.jsonObjectToString(eventData));
                    throw new Exception("Error occured while querying events in Zenoss. Response Received: "+response);
                }
                
                JSONObject event = JSONObject.fromObject(response);
                if (event.containsKey("result"))
                {
                    event = JSONObject.fromObject(event.get("result"));
                    if (event.containsKey("success") && event.containsKey("totalCount"))
                    {

                        if ((Boolean) event.get("success") == true && (Integer) event.get("totalCount") > 0)
                        {
                            if (event.containsKey("events"))
                            {
                                List<Map<String, String>> eventDataReturned =  parseResponse(JSONArray.fromObject(event.get("events")),maxResultsToFetch,pulledEvents.size());
                                if(eventDataReturned.size() !=0 )
                                {
                                    pulledEvents.addAll(eventDataReturned);
                                    if(pulledEvents.size()==maxResultsToFetch) {
                                        break; // not more than quesize
                                    }
                                }
                                else {
                                    break;
                                }
                            }
                        }
                        else
                        {
                            Log.log.error("Error while querying events in Zenoss. Response Received: "+response);
                            break;
                        }
                    }

                }
                else
                {
                    Log.log.error("Error while querying events in Zenoss. Response Received: "+response);
                    break;
                }

            }
            catch (Exception e)
            {
                Log.log.error("Error while querying events in Zenoss.");
                break;
            }

            start += limit;
            page += 1;

        }

        return pulledEvents;
    }
    
    
   
    List<Map<String, String>> parseResponse(JSONArray event,int limit,int totalFetchedUntillNow){
        List<Map<String, String>> eventList = new ArrayList<>();
       // JSONObject eventData = new JSONObject();
        DecimalFormat format = new DecimalFormat();
       for(int i=0;i<event.size();i++ ) {
           JSONObject eventData =  JSONObject.fromObject(event.get(i)) ;
           Map<String, String> eventKeyValue = StringUtils.jsonObjectToMap(eventData);
           if(eventKeyValue.containsKey("firstTime")) {
               
               eventKeyValue.replace("firstTime",  format.format(new BigDecimal(eventKeyValue.get("firstTime"))));
           }
           if(eventKeyValue.containsKey("lastTime")) {
               
               eventKeyValue.replace("lastTime",  format.format(new BigDecimal(eventKeyValue.get("lastTime"))));
           }
           if(totalFetchedUntillNow == limit ) {
               break;
           }
           else {
           eventList.add(eventKeyValue);
           totalFetchedUntillNow+=1;
           }
           
        }
        
        
        return eventList;
        
    }


    @Override
    public boolean acknowledge(String evid) throws Exception
    {
        // TODO Auto-generated method stub}
       
        if(StringUtils.isBlank(evid) ) {
            throw new Exception("evid of an event is required.");
        }
        RestCaller restCallerOld = new RestCaller(baseZenossUrl, configurations.getHttpbasicauthusername(), configurations.getHttpbasicauthpassword());
        restCallerOld.setUrlSuffix("zport/dmd/Events/evconsole_router");
        boolean isUpdated = false;
        String method = "acknowledge";
        String action = "EventsRouter";
        
        Collection<Integer> statusCode = new ArrayList<>();
        statusCode.add(200);
        
        JSONArray evids = new JSONArray();
        evids.add(evid);
        
        JSONObject eventData = new JSONObject();
        eventData.accumulate("evids", evids);
        
        evids.clear();
        evids.add(eventData);
        
        JSONObject jsonBody = new JSONObject();
        jsonBody.accumulate("action",action);
        jsonBody.accumulate("method",method);
        jsonBody.accumulate("data",evids);
        jsonBody.accumulate("tid",1);
        
        try
        {

            String response = restCallerOld.postMethod(null, null, StringUtils.jsonObjectToString(jsonBody), statusCode);
            if (restCallerOld.getStatusCode() != 200)
            {
                Log.log.error("Acknowledgement failed.. Inputs Params are===>" + StringUtils.jsonObjectToString(jsonBody));
            }

            JSONObject ackResponse = JSONObject.fromObject(response);
            if (ackResponse.containsKey("result"))
            {
                JSONObject data = JSONObject.fromObject(ackResponse).getJSONObject("result");
                if (data.containsKey("success"))
                {
                    isUpdated = data.getBoolean("success");

                }
            }
        }catch(Exception e) {
            Log.log.error(e.getMessage());
            throw e;
            
        }
       return isUpdated;
    }


    @Override
    public boolean unacknowledge(String evid) throws Exception
    {
        // TODO Auto-generated method stub}
        
        if(StringUtils.isBlank(evid) ) {
            throw new Exception("evid of an event is required.");
        }
        RestCaller restCallerOld = new RestCaller(baseZenossUrl, configurations.getHttpbasicauthusername(), configurations.getHttpbasicauthpassword());
        restCallerOld.setUrlSuffix("zport/dmd/Events/evconsole_router");
        boolean isUpdated = false;
        String method = "reopen";
        String action = "EventsRouter";
        
        Collection<Integer> statusCode = new ArrayList<>();
        statusCode.add(200);
        
        JSONArray evids = new JSONArray();
        evids.add(evid);
        
        JSONObject eventData = new JSONObject();
        eventData.accumulate("evids", evids);
        
        evids.clear();
        evids.add(eventData);
        
        JSONObject jsonBody = new JSONObject();
        jsonBody.accumulate("action",action);
        jsonBody.accumulate("method",method);
        jsonBody.accumulate("data",evids);
        jsonBody.accumulate("tid",1);
        
        try
        {

            String response = restCallerOld.postMethod(null, null, StringUtils.jsonObjectToString(jsonBody), statusCode);
            if (restCallerOld.getStatusCode() != 200)
            {
                Log.log.error("Failed to UnAcknowledge Event. Inputs Params are===>" + StringUtils.jsonObjectToString(jsonBody));
            }

            JSONObject unAckResponse = JSONObject.fromObject(response);
            if (unAckResponse.containsKey("result"))
            {
                JSONObject data = JSONObject.fromObject(unAckResponse).getJSONObject("result");
                if (data.containsKey("success"))
                {
                    isUpdated = data.getBoolean("success");

                }
            }
        }catch(Exception e) {
            Log.log.error(e.getMessage());
            throw e;
            
        }
       return isUpdated;
    }
    
    @Override
    public boolean writeLog(String evid,String message) throws Exception
    {
        // TODO Auto-generated method stub}
        
        if(StringUtils.isBlank(evid) || StringUtils.isBlank(message) ) {
            throw new Exception("evid/message is required.");
        }
        RestCaller restCallerOld = new RestCaller(baseZenossUrl, configurations.getHttpbasicauthusername(), configurations.getHttpbasicauthpassword());
        restCallerOld.setUrlSuffix("zport/dmd/Events/evconsole_router");
        boolean isUpdated = false;
        String method = "write_log";
        String action = "EventsRouter";
        
        Collection<Integer> statusCode = new ArrayList<>();
        statusCode.add(200);
        
        
        JSONObject eventData = new JSONObject();
        eventData.accumulate("evid", evid);
        eventData.accumulate("message", message);
        
        JSONArray evids = new JSONArray();
        evids.add(eventData);
        
               
        JSONObject jsonBody = new JSONObject();
        jsonBody.accumulate("action",action);
        jsonBody.accumulate("method",method);
        jsonBody.accumulate("data",evids);
        jsonBody.accumulate("tid",1);
        
        try
        {

            String response = restCallerOld.postMethod(null, null, StringUtils.jsonObjectToString(jsonBody), statusCode);
            if (restCallerOld.getStatusCode() != 200)
            {
                Log.log.error("Failed to Write Log to an Event. Input Params are===>" + StringUtils.jsonObjectToString(jsonBody));
            }

            JSONObject unAckResponse = JSONObject.fromObject(response);
            if (unAckResponse.containsKey("result"))
            {
                JSONObject data = JSONObject.fromObject(unAckResponse).getJSONObject("result");
                if (data.containsKey("success"))
                {
                    isUpdated = data.getBoolean("success");
                    Log.log.debug("Added log sccessfully");

                }
            }
        }catch(Exception e) {
            Log.log.error(e.getMessage());
            throw e;
            
        }
       return isUpdated;
    }


    @Override
    public boolean closeEvent(String evid) throws Exception
    {
     // TODO Auto-generated method stub}
        
        if(StringUtils.isBlank(evid) ) {
            throw new Exception("evid of an event is required.");
        }
        RestCaller restCallerOld = new RestCaller(baseZenossUrl, configurations.getHttpbasicauthusername(), configurations.getHttpbasicauthpassword());
        restCallerOld.setUrlSuffix("zport/dmd/Events/evconsole_router");
        boolean isUpdated = false;
        String method = "close";
        String action = "EventsRouter";
        
        Collection<Integer> statusCode = new ArrayList<>();
        statusCode.add(200);
        
        JSONArray evids = new JSONArray();
        evids.add(evid);
        
        JSONObject eventData = new JSONObject();
        eventData.accumulate("evids", evids);
        
        evids.clear();
        evids.add(eventData);
        
        JSONObject jsonBody = new JSONObject();
        jsonBody.accumulate("action",action);
        jsonBody.accumulate("method",method);
        jsonBody.accumulate("data",evids);
        jsonBody.accumulate("tid",1);
        
        try
        {

            String response = restCallerOld.postMethod(null, null, StringUtils.jsonObjectToString(jsonBody), statusCode);
            if (restCallerOld.getStatusCode() != 200)
            {
                Log.log.error("Failed to close an Event.. Inputs Params are===>" + StringUtils.jsonObjectToString(jsonBody));
            }

            JSONObject ackResponse = JSONObject.fromObject(response);
            if (ackResponse.containsKey("result"))
            {
                JSONObject data = JSONObject.fromObject(ackResponse).getJSONObject("result");
                if (data.containsKey("success"))
                {
                    isUpdated = data.getBoolean("success");

                }
            }
        }catch(Exception e) {
            Log.log.error(e.getMessage());
            throw e;
            
        }
       return isUpdated;
    }
    
    public String getDevicePriorities() throws Exception
    {
        RestCaller restCallerOld = new RestCaller(baseZenossUrl, configurations.getHttpbasicauthusername(), configurations.getHttpbasicauthpassword());
        restCallerOld.setUrlSuffix("zport/dmd/Events/device_router");
        String method = "getPriorities";
        String action = "DeviceRouter";
        
        Collection<Integer> statusCode = new ArrayList<>();
        statusCode.add(200);
        
        JSONObject jsonBody = new JSONObject();
        jsonBody.accumulate("action",action);
        jsonBody.accumulate("method",method);
        jsonBody.accumulate("tid",1);
        
        
            String response = restCallerOld.postMethod(null, null, StringUtils.jsonObjectToString(jsonBody), statusCode);
            if (restCallerOld.getStatusCode() != 200)
            {
                throw new Exception();
            }

            JSONObject ackResponse = JSONObject.fromObject(response);
            if (ackResponse.containsKey("result"))
            {
                JSONObject data = JSONObject.fromObject(ackResponse).getJSONObject("result");
                if (!data.containsKey("success"))
                {
                    throw new Exception();
                }
            }
            return response;
        }
     
    
}
