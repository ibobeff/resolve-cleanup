package com.resolve.persistence.model;

import java.util.List;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Lob;
import javax.persistence.Table;
import javax.persistence.UniqueConstraint;
import com.resolve.services.interfaces.VO;
import com.resolve.services.hibernate.vo.ZENOSSFilterVO;

@Entity
@Table(name = "zenoss_filter", uniqueConstraints = { @UniqueConstraint(columnNames = { "u_name", "u_queue" }) })
public class ZENOSSFilter extends GatewayFilter<ZENOSSFilterVO> {

    private static final long serialVersionUID = 1L;

    public ZENOSSFilter() {
    }

    public ZENOSSFilter(ZENOSSFilterVO vo) {
        applyVOToModel(vo);
    }

    private String UDeviceIp;

    private String UEventStaus;

    private String UFirstSeenDateTime;

    private String ULastSeenDateTime;

    @Column(name = "u_deviceip", length = 400)
    public String getUDeviceIp() {
        return this.UDeviceIp;
    }

    public void setUDeviceIp(String uDeviceIp) {
        this.UDeviceIp = uDeviceIp;
    }

    @Column(name = "u_eventstaus", length = 400)
    public String getUEventStaus() {
        return this.UEventStaus;
    }

    public void setUEventStaus(String uEventStaus) {
        this.UEventStaus = uEventStaus;
    }

    @Column(name = "u_firstseendatetime", length = 400)
    public String getUFirstSeenDateTime() {
        return this.UFirstSeenDateTime;
    }

    public void setUFirstSeenDateTime(String uFirstSeenDateTime) {
        this.UFirstSeenDateTime = uFirstSeenDateTime;
    }

    @Column(name = "u_lastseendatetime", length = 400)
    public String getULastSeenDateTime() {
        return this.ULastSeenDateTime;
    }

    public void setULastSeenDateTime(String uLastSeenDateTime) {
        this.ULastSeenDateTime = uLastSeenDateTime;
    }

    public ZENOSSFilterVO doGetVO() {
        ZENOSSFilterVO vo = new ZENOSSFilterVO();
        super.doGetBaseVO(vo);
        vo.setUDeviceIp(getUDeviceIp());
        vo.setUEventStaus(getUEventStaus());
        vo.setUFirstSeenDateTime(getUFirstSeenDateTime());
        vo.setULastSeenDateTime(getULastSeenDateTime());
        return vo;
    }

    @Override
    public void applyVOToModel(ZENOSSFilterVO vo) {
        if (vo == null) return;
        super.applyVOToModel(vo);
        if (!VO.STRING_DEFAULT.equals(vo.getUDeviceIp())) this.setUDeviceIp(vo.getUDeviceIp()); else ;
        if (!VO.STRING_DEFAULT.equals(vo.getUEventStaus())) this.setUEventStaus(vo.getUEventStaus()); else ;
        if (!VO.STRING_DEFAULT.equals(vo.getUFirstSeenDateTime())) this.setUFirstSeenDateTime(vo.getUFirstSeenDateTime()); else ;
        if (!VO.STRING_DEFAULT.equals(vo.getULastSeenDateTime())) this.setULastSeenDateTime(vo.getULastSeenDateTime()); else ;
    }

    @Override
    public List<String> ugetCdataProperty() {
        List<String> list = super.ugetCdataProperty();
        list.add("UDeviceIp");
        list.add("UEventStaus");
        list.add("UFirstSeenDateTime");
        list.add("ULastSeenDateTime");
        return list;
    }
}

