package com.resolve.gateway.zenoss;

import com.resolve.gateway.BaseFilter;
import com.resolve.util.Log;
import com.resolve.gateway.RetainValue;
public class ZENOSSFilter extends BaseFilter {

    public static final String DEVICEIP = "DEVICEIP";

    public static final String EVENTSTAUS = "EVENTSTAUS";

    public static final String FIRSTSEENDATETIME = "FIRSTSEENDATETIME";

    public static final String LASTSEENDATETIME = "LASTSEENDATETIME";

    public static final String FIRST_SEEN_TIME="FIRSTSEENTIME";
    
   // public static final String LAST_READ_EVID="LASTREADEVID";
    
    public static final String FIRST_SEEN_GATEWAY_MACRO="FIRST_SEEN";
    
    
    private String deviceIp;

    private String eventStaus;

    private String firstSeenDateTime;

    private String lastSeenDateTime;

    
    private String persistedFirstSeenTime;
    
   // private String persistedLastReadEvenEvid;
    
    
/*    @RetainValue
    public String getPersistedLastReadEvenEvid()
    {
        return persistedLastReadEvenEvid;
    }


    public void setPersistedLastReadEvenEvid(String persistedLastReadEvenEvid)
    {
        this.persistedLastReadEvenEvid = persistedLastReadEvenEvid;
    }*/


    @RetainValue 
    public String getPersistedFirstSeenTime()
    {
        return persistedFirstSeenTime;
    }


    public void setPersistedFirstSeenTime(String persistedFirstSeenTime)
    {
        this.persistedFirstSeenTime = persistedFirstSeenTime;
    }
    
    public ZENOSSFilter(String id, String active, String order, String interval, String eventEventId, String runbook, String script, String deviceIp, String eventStaus, String firstSeenDateTime, String lastSeenDateTime,String persistedFirstSeenTime) {//,String lastReadEvid) {
        super(id, active, order, interval, eventEventId, runbook, script);
        this.deviceIp = deviceIp;
        this.eventStaus = eventStaus;
        this.firstSeenDateTime = firstSeenDateTime;
        this.lastSeenDateTime = lastSeenDateTime;
        setPersistedFirstSeenTime(persistedFirstSeenTime);
       // setPersistedLastReadEvenEvid(lastReadEvid);
    }

    public String getDeviceIp() {
        return this.deviceIp;
    }

    public void setDeviceIp(String deviceIp) {
        this.deviceIp = deviceIp;
    }

    public String getEventStaus() {
        return this.eventStaus;
    }

    public void setEventStaus(String eventStaus) {
        this.eventStaus = eventStaus;
    }

    public String getFirstSeenDateTime() {
        return this.firstSeenDateTime;
    }

    public void setFirstSeenDateTime(String firstSeenDateTime) {
        this.firstSeenDateTime = firstSeenDateTime;
    }

    public String getLastSeenDateTime() {
        return this.lastSeenDateTime;
    }

    public void setLastSeenDateTime(String lastSeenDateTime) {
        this.lastSeenDateTime = lastSeenDateTime;
    }
}

