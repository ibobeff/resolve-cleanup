package com.resolve.gateway.zenoss;

import java.io.File;
import java.util.HashMap;
import java.util.Map;
import org.apache.commons.io.FileUtils;

import com.resolve.rsbase.MainBase;
import com.resolve.gateway.Filter;
import com.resolve.rsremote.ConfigReceiveGateway;
import com.resolve.util.Log;
import com.resolve.util.ObjectProperties;
import com.resolve.util.StringUtils;
import com.resolve.util.XDoc;

public class ConfigReceiveZENOSS extends ConfigReceiveGateway {

    private static final long serialVersionUID = 1L;

    private static final String RECEIVE_ZENOSS_NODE = "./RECEIVE/ZENOSS/";

    private static final String RECEIVE_ZENOSS_FILTER = RECEIVE_ZENOSS_NODE + "FILTER";

    private String queue = "ZENOSS";

    private static final String RECEIVE_ZENOSS_ATTR_HTTPBASICAUTHUSERNAME = RECEIVE_ZENOSS_NODE + "@HTTPBASICAUTHUSERNAME";

    private static final String RECEIVE_ZENOSS_ATTR_SSLTYPE = RECEIVE_ZENOSS_NODE + "@SSLTYPE";

    private static final String RECEIVE_ZENOSS_ATTR_ZENOSS_PORT = RECEIVE_ZENOSS_NODE + "@ZENOSS_PORT";

    private static final String RECEIVE_ZENOSS_ATTR_SSL = RECEIVE_ZENOSS_NODE + "@SSL";

    private static final String RECEIVE_ZENOSS_ATTR_HTTPBASICAUTHPASSWORD = RECEIVE_ZENOSS_NODE + "@HTTPBASICAUTHPASSWORD";

    private static final String RECEIVE_ZENOSS_ATTR_ZENOSS_HOST = RECEIVE_ZENOSS_NODE + "@ZENOSS_HOST";
    
    private static final String RECEIVE_ZENOSS_ATTR_USERTIMEZONE = RECEIVE_ZENOSS_NODE + "@USERTIMEZONE";

    private String httpbasicauthusername = "";

    private String ssltype = "";
    
    private String userTimeZone = "";
    
    private String zenoss_port = "";

    private String ssl = "";

    private String httpbasicauthpassword = "";

    private String zenoss_host = "";

    public String getQueue() {
        return queue;
    }

    public void setQueue(String queue) {
        this.queue = queue;
    }

    public String getHttpbasicauthusername() {
        return this.httpbasicauthusername;
    }

    public void setHttpbasicauthusername(String httpbasicauthusername) {
        this.httpbasicauthusername = httpbasicauthusername;
    }

    public String getSsltype() {
        return this.ssltype;
    }

    public void setSsltype(String ssltype) {
        this.ssltype = ssltype;
    }

    public String getZenoss_port() {
        return this.zenoss_port;
    }

    public void setZenoss_port(String zenoss_port) {
        this.zenoss_port = zenoss_port;
    }

    public String getSsl() {
        return this.ssl;
    }

    public void setSsl(String ssl) {
        this.ssl = ssl;
    }

    public String getHttpbasicauthpassword() {
        return this.httpbasicauthpassword;
    }

    public void setHttpbasicauthpassword(String httpbasicauthpassword) {
        this.httpbasicauthpassword = httpbasicauthpassword;
    }

    public String getZenoss_host() {
        return this.zenoss_host;
    }

    public void setZenoss_host(String zenoss_host) {
        this.zenoss_host = zenoss_host;
    }
    
    public String getUserTimeZone() {
        return this.userTimeZone;
    }

    public void setUserTimeZone(String userTimeZone) {
        this.userTimeZone = userTimeZone;
    }

    public ConfigReceiveZENOSS(XDoc config) throws Exception {
        super(config);
        define("httpbasicauthusername", STRING, RECEIVE_ZENOSS_ATTR_HTTPBASICAUTHUSERNAME);
        define("ssltype", STRING, RECEIVE_ZENOSS_ATTR_SSLTYPE);
        define("zenoss_port", STRING, RECEIVE_ZENOSS_ATTR_ZENOSS_PORT);
        define("ssl", STRING, RECEIVE_ZENOSS_ATTR_SSL);
        define("httpbasicauthpassword", SECURE, RECEIVE_ZENOSS_ATTR_HTTPBASICAUTHPASSWORD);
        define("zenoss_host", STRING, RECEIVE_ZENOSS_ATTR_ZENOSS_HOST);
        define("userTimeZone", STRING, RECEIVE_ZENOSS_ATTR_USERTIMEZONE);
    }

    @Override
    public String getRootNode() {
        return RECEIVE_ZENOSS_NODE;
    }

    @Override
    public void load() throws Exception {
        try {
            loadAttributes();
            if (isActive()) {
                ZENOSSGateway zenossGateway = ZENOSSGateway.getInstance(this);
                filters = xdoc.getListMapValue(RECEIVE_ZENOSS_FILTER);
                if (filters.size() > 0) {
                    for (Map<String, Object> values : filters) {
                        String filterName = ((String) values.get(ZENOSSFilter.ID)).toLowerCase();
                        File scriptFile = getFile(MainBase.main.release.serviceName + "/config/zenoss/" + filterName + ".groovy");
                        try {
                            if (scriptFile.exists()) {
                                String groovy = FileUtils.readFileToString(scriptFile, "UTF-8");
                                if (StringUtils.isNotBlank(groovy)) {
                                    values.put(ZENOSSFilter.SCRIPT, groovy);
                                    Log.log.debug("Loaded groovy script: " + scriptFile);
                                }
                            }
                            zenossGateway.setFilter(values);
                        } catch (Exception e) {
                            Log.log.warn(e.getMessage() + ". Skipping file: " + scriptFile.getAbsolutePath());
                        }
                    }
                }
            }
        } catch (Throwable e) {
            setActive(false);
            Log.log.warn("Couldn not load configuration for ZENOSS gateway, check blueprint. " + e.getMessage(), e);
        }
    }

    @Override
    public void save() throws Exception {
        File dir = getFile(MainBase.main.release.serviceName + "/config/zenoss");
        if (!dir.exists()) {
            FileUtils.forceMkdir(dir);
        }
        if (filters != null && isActive()) {
            filters.clear();
            for (Filter filter : ZENOSSGateway.getInstance().getFilters().values()) {
                ZENOSSFilter zenossFilter = (ZENOSSFilter) filter;
                String groovy = zenossFilter.getScript();
                String scriptFileName;
                File scriptFile = null;
                if (StringUtils.isNotBlank(groovy)) {
                    scriptFileName = zenossFilter.getId().toLowerCase() + ".groovy";
                    scriptFile = getFile(MainBase.main.release.serviceName + "/config/zenoss/" + scriptFileName);
                }
                Map<String, Object> entry = new HashMap<String, Object>();
                putFilterDataIntoEntry(entry, zenossFilter);
                if(StringUtils.isNoneBlank(zenossFilter.getPersistedFirstSeenTime())) {
                    entry.put(ZENOSSFilter.FIRST_SEEN_TIME,zenossFilter.getPersistedFirstSeenTime());
                }
               /* if(StringUtils.isNoneBlank(zenossFilter.getPersistedLastReadEvenEvid())) {
                    entry.put(ZENOSSFilter.LAST_READ_EVID,zenossFilter.getPersistedLastReadEvenEvid());
                }*/
                filters.add(entry);
                if (scriptFile != null) {
                    try {
                        FileUtils.writeStringToFile(scriptFile, groovy, "UTF-8");
                    } catch (Exception e) {
                        Log.log.warn(e.getMessage() + ". Skipping file: " + scriptFile.getAbsolutePath());
                    }
                }
            }
            xdoc.setListMapValue(RECEIVE_ZENOSS_FILTER, filters);
        }
        if (nameProperties != null && isActive()) {
            nameProperties.clear();
            for (String name : ZENOSSGateway.getInstance().getNameProperties().keySet()) {
                Map<String, Object> entry = new HashMap<String, Object>();
                entry.put(NAME_KEY, name);
                nameProperties.add(entry);
                Map props = ZENOSSGateway.getInstance().getNameProperties().getNameProperties(name);
                ObjectProperties prop = new ObjectProperties(props);
                prop.save(props);
            }
        }
        saveAttributes();
    }

    private void putFilterDataIntoEntry(Map<String, Object> entry, ZENOSSFilter zenossFilter) {
        entry.put(ZENOSSFilter.ID, zenossFilter.getId());
        entry.put(ZENOSSFilter.ACTIVE, String.valueOf(zenossFilter.isActive()));
        entry.put(ZENOSSFilter.ORDER, String.valueOf(zenossFilter.getOrder()));
        entry.put(ZENOSSFilter.INTERVAL, String.valueOf(zenossFilter.getInterval()));
        entry.put(ZENOSSFilter.EVENT_EVENTID, zenossFilter.getEventEventId());
        entry.put(ZENOSSFilter.RUNBOOK, zenossFilter.getRunbook());
        entry.put(ZENOSSFilter.SCRIPT, zenossFilter.getScript());
        entry.put(ZENOSSFilter.DEVICEIP, zenossFilter.getDeviceIp());
        entry.put(ZENOSSFilter.EVENTSTAUS, zenossFilter.getEventStaus());
        entry.put(ZENOSSFilter.FIRSTSEENDATETIME, zenossFilter.getFirstSeenDateTime());
        entry.put(ZENOSSFilter.LASTSEENDATETIME, zenossFilter.getLastSeenDateTime());
    }
}

