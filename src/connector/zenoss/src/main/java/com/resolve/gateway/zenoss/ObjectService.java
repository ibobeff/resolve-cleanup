package com.resolve.gateway.zenoss;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

public interface ObjectService
{
    List<Map<String, String>> queryEvents(ArrayList<String> eventState, String firstTime,String lastTime,String device,int limit) throws Exception;
    boolean acknowledge(String evid) throws Exception;
    boolean unacknowledge(String evid) throws Exception;
    boolean writeLog(String evid,String messege) throws Exception;
    boolean closeEvent(String evid) throws Exception;
    String getDevicePriorities() throws Exception;
 }
