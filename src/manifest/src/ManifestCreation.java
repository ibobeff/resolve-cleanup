/******************************************************************************
* (C) Copyright 2014
*
* Resolve Systems, LLC
*
* All rights reserved
* This software is distributed under license and may not be used, copied,
* modified, or distributed without the express written permission of
* Resolve Systems, LLC.
*
******************************************************************************/

import java.io.*;
import java.util.*;

public class ManifestCreation
{
    /**
     * @param args
     */
    private static final String DEFAULT_HOME = "C:/project/resolve";
    private static final String DEFAULT_MANIFEST = "manifest.txt";
    private static final String DEFAULT_log = "log.txt";
    private static final String DEFAULT_UPDATE = "C:/project/resolve/update/update.txt";
    
    public static void generateManifestFile(File[] files, String home, BufferedWriter out)
    {
        //Close the output stream
        for (File file : files)
        {
            if (file.isDirectory()) 
            {
                generateManifestFile(file.listFiles(), home, out); //recurse into sub directory
            } 
            else 
            {
                try 
                {
                    String fixedSlashes = file.getAbsolutePath();
                    fixedSlashes = fixedSlashes.replace('\\', '/');
                    fixedSlashes = fixedSlashes.replaceAll(home, "");
                    out.write(fixedSlashes + "?" + CryptMD5.encrypt(file) + "\n");
                }
                catch (Exception e)
                {
                    //Catch exception if any
                    System.err.println("Error: " + e.getMessage());
                }
            }
        }
    }

    public static void parseExcludeUpdateFile (String exclude_update_file, ArrayList<String> exclusions, String dist_dir, String home_dir)
    {
        try
        {
            FileReader exclusionReader = new FileReader(exclude_update_file);
            Scanner exclusionScanner = new Scanner(exclusionReader);
            String next;
            Compress compress = new Compress();
            while(exclusionScanner.hasNext())
            {
                next = exclusionScanner.next();
                if(next.contains("/war/") && next.contains(".zip")) // then this is relative to home/tomcat/webapps/resolve
                {
//                    System.out.println("Zip Location:" + home_dir + "/src/" + next);
                    File zipFile = new File(home_dir + "/src/" + next);
                    int warIdx = next.indexOf("/war/");
                    next = next.substring(warIdx + 5, (next.length()-4));
                    exclusions.add((dist_dir+"/tomcat/webapps/resolve/"+next));
                    if (zipFile.exists())
                    {
//                        System.out.println("Zip Exists");
                        String[] zipFiles = compress.zipContent(zipFile);
                        for (String filePath : zipFiles)
                        {
//                            System.out.println("File Path: " + filePath);
                            if (!filePath.contains(next + "/"))
                            {
                                exclusions.add(dist_dir+"/tomcat/webapps/resolve/" + filePath);
                            }
                        }
                    }
                }
                else if(next.contains("/src/") && next.contains(".zip")) // then this is relative to home/tomcat/webapps/resolve
                {
                    File zipFile = new File(home_dir + "/src/" + next);
//                    System.out.println("Zip Location:" + home_dir + "/src/" + next);
                    int src = next.indexOf("/src/");
                    next = next.substring(src + 5, (next.length()-4));
                    exclusions.add((dist_dir+"/tomcat/webapps/resolve/"+next));
                    if (zipFile.exists())
                    {
//                        System.out.println("Zip Exists");
                        String[] zipFiles = compress.zipContent(zipFile);
                        for (String filePath : zipFiles)
                        {
//                            System.out.println("File Path: " + filePath);
                            if (!filePath.contains(next + "/"))
                            {
                                exclusions.add(dist_dir+"/tomcat/webapps/resolve/" + filePath);
                            }
                        }
                    }
                }
                else if(next.contains("/build/") && next.contains(".zip")) // then this is relative to home/tomcat/webapps/resolve
                {
                    File zipFile = new File(home_dir + "/src/" + next);
//                    System.out.println("Zip Location:" + home_dir + "/build/" + next);
                    int buildIdx = next.indexOf("/build/");
                    next = next.substring(buildIdx + 7, (next.length()-4));
                    exclusions.add((dist_dir+"/tomcat/webapps/resolve/"+next));
                    if (zipFile.exists())
                    {
//                        System.out.println("Zip Exists");
                        String[] zipFiles = compress.zipContent(zipFile);
                        for (String filePath : zipFiles)
                        {
//                            System.out.println("File Path: " + filePath);
                            if (!filePath.contains(next + "/"))
                            {
                                exclusions.add(dist_dir+"/tomcat/webapps/resolve/" + filePath);
                            }
                        }
                    }
                }
                else //this is relative to the home directory
                {
                    exclusions.add((dist_dir+ "/" + next));
                }
            }
        }
        catch (FileNotFoundException e)
        {
            if(e.getMessage().contains("(The system cannot find the file specified)"))
            {
                System.out.println("System could not find the exclusion file, please double check that this file exists and that the path is typed correctly in you config file.");
                System.err.println(e.getMessage());
            }
            else
            {
                e.printStackTrace();                    
            }
        }

    }
    
    public static Tree parseManifestFile(Scanner manifestFile, String root, BufferedWriter out)
    {
        Tree manifestDirectory = new Tree();
        Node manifestRoot = new Node(root,null);
        manifestDirectory.setRoot(manifestRoot);
        
        Scanner manifestLine;
        boolean homeDirFound = false;
        String piecedHome = "";
        ArrayList<String> pathWithoutHome = new ArrayList<String> ();
        String fileName = "";
        String checkSum = "";
        String next = "";
  
        while(manifestFile.hasNext())
        {
            homeDirFound = false;
            piecedHome = "";
            manifestLine = new Scanner(manifestFile.nextLine());
            manifestLine.useDelimiter("/");
            while(manifestLine.hasNext())
            {
                next = manifestLine.next();
//                System.out.print("NEXT: " + next);
                if(!homeDirFound)
                {
                    piecedHome +=next;
                    if(piecedHome.equals(root))
                    {
                        homeDirFound = true;
                        pathWithoutHome.add(piecedHome);
//                        System.out.println(" Found Home");
                    }
                    else
                    {
                        piecedHome += "/";
//                        System.out.println(" Not Home");
                    }
                }
                else
                {
                    if(manifestLine.hasNext())
                    {
                        pathWithoutHome.add(next);
//                        System.out.println(" Next");
                    }
                    else
                    {
                        manifestLine = new Scanner(next);
                        manifestLine.useDelimiter("\\?");
                        
                        fileName = manifestLine.next();
                        checkSum = manifestLine.next();
//                        System.out.println(" File");
                    }
                }
            }
            String[] dummy = new String[]{"dummy"};
            int size = pathWithoutHome.size();
            StringBuilder pathArrayStr = new StringBuilder();
            for (String path : pathWithoutHome)
            {
                pathArrayStr.append("/" + path);
            }
            
            String [] pathArray = pathWithoutHome.toArray(dummy);
            pathArrayStr = new StringBuilder();
//            for (String path : pathArray)
//            {
//                pathArrayStr.append("/" + path);
//            }
//            System.out.println("addFileWithPath(" + pathArrayStr.toString() + ", " + size + ", " + fileName + ", " + checkSum + ")");
            manifestDirectory.addFileWithPath(pathArray, size, fileName, checkSum);
           
            pathWithoutHome.clear();            

        }
        return manifestDirectory;
    }
    
    public static void compareCurrentAgainstManifest(File[] currentFiles, ArrayList<String> exclusions, BufferedWriter out, BufferedWriter updateWriter, Tree parsedManifest, String root, String home_dir)
    {        
        //parse each file according to the directory, 
        //  '->If you find it, check the check sum and check it off
        //      '->If the checksums are different then it has been changed
        //  '-> if you don't find it then print that it is new
        //  '-> if you find it remove the node from the tree
        Scanner fileDirectory;
        ArrayList<String> pathWithoutHome = new ArrayList<String> ();
        String next = "";
        boolean homeDirFound = false;
        String piecedHome = "";
        String fileName = "";
        String currentCheckSum = "";
        String originalCheckSum = "";
        //Go through all of the nodes remaining and 
        for (File file : currentFiles)
        {
            String fileStr = file.toString().replace('\\', '/');
            fileStr.replaceAll(home_dir, "");
            fileDirectory = new Scanner(fileStr);
            fileDirectory.useDelimiter("/");
            homeDirFound = false;
            piecedHome = "";
            if (file.isDirectory()) 
            {
                if(!exclusions.contains(file.toString().replace('\\', '/')))
                {
                    compareCurrentAgainstManifest(file.listFiles(), exclusions, out, updateWriter, parsedManifest, root, home_dir); //recurse into sub directory
                }
            } 
            if (!file.isDirectory()) //going to assume for the time being that we don't care if they have extra empty directories or are missing empty directories
            {
                if(!exclusions.contains(file.toString().replace('\\', '/')))
                {
                    while(fileDirectory.hasNext())
                    {
                        next = fileDirectory.next();
                        
                        if(!homeDirFound)
                        {
                            //System.out.println("!homedir found: next = "+next+" |piecedHome = "+piecedHome+" |home_dir = "+home_dir);
                            piecedHome = next;
                            if(piecedHome.equals(root))
                            {
                                homeDirFound = true;
                                pathWithoutHome.add(piecedHome);
                            }
                            else
                            {
//                                piecedHome += "/";
                            }
                        }
                        else
                        {
                            //System.out.println("else");
                            if(fileDirectory.hasNext())
                            {
                                pathWithoutHome.add(next);
                            }
                            else
                            {
                                fileDirectory = new Scanner(next);
                                fileDirectory.useDelimiter("\\?");                           
                                fileName = fileDirectory.next();
                            }
                        }
                    }
                    //fileName = file.getAbsolutePath().replace('\\', '/');
                    if(!fileName.equals("manifest.txt") && !fileName.equals("manifest.txt.swp"))
                    {
                        currentCheckSum = CryptMD5.encrypt(file);
                    
                        String[] dummy = new String[]{"dummy"};
                        int size = pathWithoutHome.size();
                        String [] pathArray = pathWithoutHome.toArray(dummy);
                        originalCheckSum = parsedManifest.findFileWithPath(pathArray, size, fileName); 
                        
//                        StringBuilder pathArrayStr = new StringBuilder();
//                        for (String path : pathArray)
//                        {
//                            pathArrayStr.append("/" + path);
//                        }
//                        System.out.println("findFileWithPath(" + pathArrayStr.toString() + ", " + size + ", " + fileName + ")=" + originalCheckSum);
                        //System.out.println("FileName = "+fileName+", originalCheckSum = "+originalCheckSum+", currentCheckSum = "+currentCheckSum);
                        if(originalCheckSum == null || originalCheckSum == "")
                        {
                            //that file isn't in the tree
                            try
                            {
                                String filePath = file.toString();
                                out.write("NEW: "+filePath+ "\n");
                                System.out.println("NEW: "+filePath);
                                if (updateWriter != null)
                                {
                                    filePath = filePath.replace('\\', '/');
                                    filePath = filePath.replaceAll(home_dir + "/dist/", "");
                                    updateWriter.write(filePath + "\n");
                                }
                            }
                            catch (IOException e)
                            {
                                e.printStackTrace();
                            }
                            
                        }
                        else if(originalCheckSum.equals(currentCheckSum))
                        {
                            //remove the node to denote that it was found                
                            parsedManifest.deleteFileWithPath(pathArray, size, fileName);
                        }
                        else
                        {
                            //you have a change in the file
                            try
                            {
                                String filePath = file.toString();
                                out.write("CHANGED: "+filePath+ "\n");
                                System.out.println("CHANGED: "+filePath);
                                if (updateWriter != null)
                                {
                                    filePath = filePath.replace('\\', '/');
                                    filePath = filePath.replaceAll(home_dir + "/dist/", "");
                                    updateWriter.write(filePath + "\n");
                                }
                            }
                            catch (IOException e)
                            {
                                e.printStackTrace();
                            }
                            
                            //remove the node to denote that it was found
                            parsedManifest.deleteFileWithPath(pathArray, size, fileName);
                        }
                    }
                    pathWithoutHome.clear();
                }
            }
        }
    }
    
    public static void usage()
    {
        System.out.println("USAGE:");
        System.out.println("-gen: this is used to generate a new manifest file");
        System.out.println("-config: following this argument should be a file path and name for the config file.  Contained in the config file:");
        System.out.println("\t'home=' to set a different home directory, use this followed by the path name");
        System.out.println("\t'manifest=' to set a different manifest name or location use this followed by the path name");
        System.out.println("\t'log='to set a different log name or location use this followed by the path name");
        System.out.println("\t'exclude='to set an exclude file name or location use this followed by the path name");
        System.out.println("\t'exclude_update='to set a different log name or location use this followed by the path name");
        System.out.println("\tThe above should be seperated by line breaks");
        System.out.println("\nDefualts:");
        System.out.println("-default for home is 'C:\\project\\resolve3\\dist'");
        System.out.println("-default for manifest is 'manifest.txt' which will show up in the directory where the .jar file resides.");
        System.out.println("-default for log is 'log.txt' which will show up in the directory where the .jar file resides.");
        System.out.println("-default for exclude_update is 'update/update.txt");
        System.out.println("-default is no exclusions file except for update/update.txt");
        System.out.println("\n*for all defaults, if you don't need to change the default then no new file name should be provided*");
        System.out.println("\nExample command line:\n\tjava manifest.jar -gen -config C:/project/manifest.cfg"); 
    }
    
    public static void main(String[] args)
    {
        boolean manifestGenerationMode = false;
        ArrayList<String> exclusions = new ArrayList<String>();
        String home_dir = DEFAULT_HOME;
        String dist_dir = DEFAULT_HOME + "/dist";
        String root = "dist";
        boolean run = true;
        String manifest_file = DEFAULT_MANIFEST;
        String log_file = DEFAULT_log;
        String update_file = DEFAULT_UPDATE;
        String exclusion_file = "";
        boolean enhanceUpdate = false;
        //parse command line arguments
        if(args.length == 0)
        {
            run = false;
            usage();
        }
        if(args.length > 0)
        {
            for (int i=0; i<args.length; i++)
            {
                String arg = args[i];
                if (arg.equals("-gen"))
                {
                    manifestGenerationMode = true;
                }
                else if (arg.equals("-config"))
                {
                    i++;
                    arg = args[i];
                    FileReader configFile;
                    try
                    {
                        configFile = new FileReader(arg);
                        Scanner configScanner = new Scanner(configFile);
                        //it is expected that the config file will have:
                        //      "home=" before the home directory
                        //      "manifest=" before the manifest directory
                        //      "log=" before the manifest directory.
                        //Scanner line = new Scanner(configScanner.nextLine());
                        Scanner configLineScanner;

                        while(configScanner.hasNext())
                        {
                            configLineScanner = new Scanner (configScanner.nextLine());
                            configLineScanner.useDelimiter("=");
                            String next = configLineScanner.next();
                            if(next.equals("home"))
                            {
                                if(configLineScanner.hasNext())
                                {
                                    dist_dir = configLineScanner.next().replace('\\', '/');
                                    dist_dir = dist_dir.trim();
                                }
                            }
                            else if(next.equals("manifest"))
                            {
                                if(configLineScanner.hasNext())
                                {
                                    manifest_file = configLineScanner.next().replace('\\', '/');
                                    manifest_file = manifest_file.trim();
                                }
                            }
                            else if(next.equals("log"))
                            {
                                if(configLineScanner.hasNext())
                                {
                                    log_file = configLineScanner.next().replace('\\', '/');
                                    log_file = log_file.trim();
                                }
                            }
                            else if(next.equals("exclude"))
                            {
                                if(configLineScanner.hasNext())
                                {
                                    exclusion_file = configLineScanner.next().replace('\\', '/');
                                    exclusion_file = exclusion_file.trim();
                                }
                            }
                            else if(next.equals("exclude_update"))
                            {
                                if(configLineScanner.hasNext())
                                {
                                    update_file = configLineScanner.next().replace('\\', '/');
                                    update_file = update_file.trim();
                                }
                            }
                        }
                    }
                    catch (FileNotFoundException e)
                    {
                        e.printStackTrace();
                    } 
                }
                else if (arg.equals("-home"))
                {
                    if (args.length > ++i)
                    {
                        dist_dir = args[i].replace('\\', '/');
                        dist_dir = dist_dir.trim();
                    }
                }
                else if (arg.equals("-u"))
                {
                    enhanceUpdate = true;
                }
                else if(arg.equals("-h") || arg.equals ("-help") || arg.equals("--help") || arg.equals("-?"))
                {
                    run = false;
                    usage();
                }
            }
        }
        
        File dist = new File(dist_dir);
        if (dist.exists())
        {
            dist = dist.getAbsoluteFile();
            dist_dir = dist.getAbsolutePath().replace('\\', '/');
            home_dir = dist.getParentFile().getAbsolutePath().replace('\\', '/');
            root = dist_dir.replaceAll(home_dir, "");
            if (root.startsWith("/"))
            {
                root = root.substring(1);
            }
        }
        
        if(run && !manifestGenerationMode)
        {
            parseExcludeUpdateFile(update_file, exclusions, dist_dir, home_dir);
            if(exclusion_file != "") //parse the exclusion file
            {   
                try
                {
                    FileReader exclusionReader = new FileReader(exclusion_file);
                    Scanner exclusionScanner = new Scanner(exclusionReader);
                    
                    while(exclusionScanner.hasNext())
                    {
                        String next = exclusionScanner.next();
                        if(next.endsWith("/")) //fix home directory if it has a trailing forward slash (no trailing backslash possilbe due to previous replace)
                        {
                            int trimTo = next.length() -1;
                            next = next.substring(0, trimTo);
                        }
                        if( !next.startsWith("/") && !next.startsWith("\\") && !next.startsWith("C:")) //fix home directory if its relative
                        {
                            String currentDir = new File(".").getAbsoluteFile().getParentFile().getAbsolutePath();
//                            currentDir = currentDir.replace(".", "");
                            next = currentDir + "/" + next;
                        }
                        exclusions.add(next.replace('\\', '/'));                       
                    }
                }
                catch (FileNotFoundException e)
                {
                    if(e.getMessage().contains("(The system cannot find the file specified)"))
                    {
                        System.out.println("System could not find the exclusion file, please double check that this file exists and that the path is typed correctly in you config file.");
                        System.err.println(e.getMessage());
                    }
                    else
                    {
                        e.printStackTrace();                    
                    }
                }
            }
             
            for(int j=0; j<exclusions.size(); j++)
            {
                if(exclusions.get(j).equals(dist_dir))
                {
                    run = false;
                    System.out.println("excluding home directory; not perfoming a check");
                }
            }
        }
        
        if (run)
        {
            if (dist_dir.endsWith("/")) //fix home directory if it has a trailing forward slash (no trailing backslash possilbe due to previous replace)
            {
                int trimTo = dist_dir.length() -1;
                dist_dir = dist_dir.substring(0, trimTo);
            }
            
            if(!manifest_file.equals(DEFAULT_MANIFEST))
            {
                exclusions.add(manifest_file);
            }
            
            if (manifestGenerationMode)
            {
                try
                {
                    FileWriter fileStream = new FileWriter(manifest_file);
                    BufferedWriter out = new BufferedWriter(fileStream);
                    File[] files = new File(dist_dir).listFiles();
                    generateManifestFile(files, home_dir, out);
                    out.close();
                }
                catch (Exception e)
                {
                    //Catch exception if any
                    e.printStackTrace();
                    System.err.println("Error: " + e.getMessage());
                }
                System.out.println("MANIFEST FILE COMPLETE");
            }
            else
            {
                BufferedWriter out = null;
                BufferedWriter updateWriter = null;
                try
                {
                    FileWriter fileStream = new FileWriter(log_file);
                    out = new BufferedWriter(fileStream);
                    if (enhanceUpdate)
                    {
                        File updateFile = new File(update_file);
                        if (updateFile.exists())
                        {
                            updateWriter = new BufferedWriter(new FileWriter(update_file, true));
                        }
                    }
                    
                    FileReader manifestFile = new FileReader(manifest_file);
                    Scanner manifestScanner = new Scanner(manifestFile);
                    manifestScanner.useDelimiter(" ");
                    
                    File[] files = new File(dist_dir).listFiles();
                    System.out.println("parseing manifest file");
                    Tree parsedManifest = parseManifestFile(manifestScanner, root, out);
//                    System.out.println("Parsed Manifest: " + parsedManifest.getRoot().toString(new ArrayList()));
                    System.out.println("comparing manifest data against current file system");
//                    for (String exclusion : exclusions)
//                    {
//                        System.out.println("Exclusion:" + exclusion);
//                    }
                    compareCurrentAgainstManifest(files, exclusions, out, updateWriter, parsedManifest, root, home_dir);                                    
                    
                    String missingFiles = parsedManifest.retrieveMissingFiles(parsedManifest.getRoot(), exclusions, home_dir);
                    if(!missingFiles.equals(""))
                    {
                        System.out.print("Missing Files:");
                        out.write("Missing Files:");
                        System.out.println(missingFiles);
                        out.write(missingFiles+"\n");
                    }

                    System.out.println("CHECK COMPLETE");
                }
                catch (Exception e)
                {
                    e.printStackTrace();
                    if(e.getMessage() != null && e.getMessage().contains("(The system cannot find the file specified)"))
                    {
                        System.out.println("System could not find the manifest file, please generate the manifest file you are specifying with -gen before trying to compare against it.");
                        System.err.println(e.getMessage());
                    }
                    else
                    {
                        if(e.getMessage() == null)
                            System.err.println("Error: "+e.toString());
                        else
                            System.err.println("Error: " + e.getMessage());
                    }
                }
                finally
                {
                    try
                    {
                        if (out != null)
                        {
                            out.close();
                        }
                    }
                    catch (IOException ioe)
                    {
                        System.err.println("Failed to Close Log Writer: " + ioe.getMessage());
                    }
                    try
                    {
                        if (updateWriter != null)
                        {
                            updateWriter.close();
                        }
                    }
                    catch (IOException ioe)
                    {
                        System.err.println("Failed to Close Update Writer: " + ioe.getMessage());
                    }
                }
            }
        }
    }

}
