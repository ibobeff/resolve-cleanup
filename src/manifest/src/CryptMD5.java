/******************************************************************************
* (C) Copyright 2014
*
* Resolve Systems, LLC
*
* All rights reserved
* This software is distributed under license and may not be used, copied,
* modified, or distributed without the express written permission of
* Resolve Systems, LLC.
*
******************************************************************************/

import java.io.File;
import java.io.FileInputStream;
import java.io.InputStream;
import java.math.BigInteger;
import java.security.MessageDigest;

public class CryptMD5 
{
    public final static int MAX_READ_SIZE=5242880;
    
    public static String encrypt(String str)
    {
        String result = str;
        
        try
        {
            MessageDigest algorithm = MessageDigest.getInstance("MD5");
            algorithm.reset();
            algorithm.update(str.getBytes());
            byte md5sum[] = algorithm.digest();
                    
            StringBuffer hexString = new StringBuffer();
            for (int i=0; i < md5sum.length;i++) 
            {
                hexString.append(Integer.toHexString(0xFF & md5sum[i]));
            }
            result = hexString.toString();
        }
        catch (Throwable e)
        {
            result = null;
            System.err.println("Failed to Generate Checksum for file "+str+": " + e.getMessage());
        }
        
        return result;
    } // encrypt
    
    public static String encrypt(byte[] input)
    {
        String result = null;
        try
        {
            MessageDigest md5 = MessageDigest.getInstance("MD5");
            md5.reset();
            byte[] md5sum = md5.digest(input);
            
            StringBuilder output = new StringBuilder(16);
            for (int i=0; i < md5sum.length; i++)
            {
                output.append(Integer.toHexString(md5sum[i] & 0xFF ));
            }

            System.out.println("Checksum = " + output.toString());
            result = output.toString();
        }
        catch (Exception e)
        {
            result = null;
            System.err.println("Failed to Generate Checksum for file "+input+": " + e.getMessage());
        }
        
        return result;
    } // encrypt
    
    public static String encrypt(File file)
    {
        String result = null;
        
        try
        {
            MessageDigest md5 = MessageDigest.getInstance("MD5");
            md5.reset();
            InputStream is = new FileInputStream(file);
            
            byte[] input = new byte[MAX_READ_SIZE];
            
            int numRead = MAX_READ_SIZE;
            while (numRead == MAX_READ_SIZE)
            {
                numRead = is.read(input);
                if (numRead != -1)
                {
                    md5.update(input, 0, numRead);
                }
            }
            is.close();
            
            
            byte[] md5sum = md5.digest();
            
            BigInteger output = new BigInteger(1, md5sum);
            //String fileName = file.getName();

            //System.out.println("(" + fileName + ") = " + output.toString(16));
            result = output.toString(16);
            while (result.length() < 32)
            {
                result = "0" + result;
            }
        }
        catch (Exception e)
        {
            result = null;
            System.err.println("Failed to Generate Checksum "+file.getAbsoluteFile()+": " + e.getMessage());
        }
        
        
        return result;
    } // encrypt
    
} // CryptMD5
