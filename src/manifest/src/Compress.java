/******************************************************************************
* (C) Copyright 2014
*
* Resolve Systems, LLC
*
* All rights reserved
* This software is distributed under license and may not be used, copied,
* modified, or distributed without the express written permission of
* Resolve Systems, LLC.
*
******************************************************************************/

import java.io.File;
import java.util.ArrayList;
import java.util.Enumeration;
import java.util.zip.ZipEntry;
import java.util.zip.ZipFile;

public class Compress
{
    public static final String ZIP_ZIPPING = "zipping";
    public static final String ZIP_UNZIPPING = "unzipping";
    static final int BUFFER = 2048;
    
    public String[] zipContent(File zipFile)
    {
        return zipContent(zipFile, false);
    }

    /**
     * @param zipFile
     * @return
     */
    public String[] zipContent(File zipFile, boolean includeDirectory)
    {
        ArrayList<String> content = null;
        ZipFile zip = null;
        try
        {
            zip = new ZipFile(zipFile);
            content = new ArrayList<String>();
            Enumeration<ZipEntry> entries = (Enumeration<ZipEntry>) zip.entries();
            int i = 0;
            while (entries.hasMoreElements())
            {
                ZipEntry zipEntry = entries.nextElement();
                if (includeDirectory || !zipEntry.isDirectory())
                {
                    content.add(zipEntry.getName());
                }
            }
        }
        catch (Exception e)
        {
            System.out.println("Failed to Read Zip Content. " + e.getMessage());
            e.printStackTrace();
        }
        finally
        {
            try
            {
                if (zip != null)
                {
                    zip.close();
                }
            }
            catch (Exception e)
            {
                System.out.println("Failed to Close Zip File. " + e.getMessage());
                e.printStackTrace();
            }
        }
        return content != null ? content.toArray(new String[0]) : null;
    }// zipContent(File zipFile)

} // Compress