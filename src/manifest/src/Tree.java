/******************************************************************************
* (C) Copyright 2014
*
* Resolve Systems, LLC
*
* All rights reserved
* This software is distributed under license and may not be used, copied,
* modified, or distributed without the express written permission of
* Resolve Systems, LLC.
*
******************************************************************************/

import java.util.ArrayList;



public class Tree
{
    private Node root;
    boolean missingFiles = false;
    public Tree()
    {
       root = null; 
    }
    
    public void setRoot(Node root)
    {
        this.root = root;
    }
    
    public Node getRoot()
    {
        return root;
    }

    private Node findInChildren(Node toBeSearched, String directory)
    {
        Node toBeReturned = null;
        boolean found = false;
        ArrayList<Node> children = toBeSearched.getChildren();
        Node current;
        for (int i=0; i<children.size() && !found; i++)
        {
            current = children.get(i);
            if(current.getDirectoryName().equals(directory))
            {
                toBeReturned = current;
                found = true;
            }
        }
        return toBeReturned;
    }
    
    public String findFileWithPath(String[] path, int pathSize, String fileName) 
    {
        Node currentNode = root;
        boolean done = false;
        String toBeReturned = null;
        for(int i=0; i<pathSize && !done; i++)
        {
            if(i==0)
            {
                if(!root.getDirectoryName().equals(path[i]))
                {
                    done = true;
                    System.out.println ("this file is not in the home directory");
                }
            }
            else
            {
                currentNode = findInChildren (currentNode, path[i]);              
            }
            if(currentNode == null)
            {
                done = true;
            }
        }
        if (!done)
        {
            toBeReturned = currentNode.getChecksumOfFile(fileName);
        }
        return toBeReturned;
    }
    
    public void deleteFileWithPath(String[] path, int pathSize, String fileName)
    {
        Node currentNode = root;
        boolean done = false;
        for(int i=0; i<pathSize && !done; i++)
        {
            if(i==0)
            {
                if(!root.getDirectoryName().equals(path[i]))
                {
                    done = true;
                    System.out.println ("this file is not in the home directory");
                }
            }
            else
            {
                currentNode = findInChildren (currentNode, path[i]);              
            }
            if(currentNode == null)
            {
                done = true;
            }
        }
        if (!done)
        {
            String returnValue = currentNode.deleteFile(fileName);
            if(returnValue.equals("deleteMe"))
            {
                deleteNode(currentNode);
            }
        }
        else
        {
            System.err.println("Error: you attempted to delete a node that did not exist");
        }
    }

    public void addFileWithPath(String[] path, int arraySize, String fileName, String checkSum)
    {
        Node currentNode = root;
        boolean foundEnd = false;
        Node previous = root;
        for(int i=0; i<arraySize; i++)
        {
            if(!foundEnd)
            {
                if(i==0)
                {
                    if(!root.getDirectoryName().equals(path[i]))
                    {
                        foundEnd = true;
                        System.out.println ("this file is not in the home directory");
                    }
                }
                else
                {
                    previous = currentNode;
                    currentNode = findInChildren (currentNode, path[i]);              
                }
                if(currentNode == null)
                {
                    foundEnd = true;
                    currentNode = previous;
                }
            }
            if(foundEnd)
            {
                Node newNode = new Node(path[i], currentNode);
                currentNode.addChild(newNode);
                currentNode = newNode;
            }
        }
        if(currentNode.getChecksumOfFile(fileName) == "")
        {
            currentNode.addFile(fileName, checkSum);
        }
    }
    
    public void insertDirectoryUnder(String directoryName, Node parent)
    {
        Node newNode = new Node(directoryName, parent);
        parent.addChild(newNode);
    }
   
    public void insertFileUnder(String fileName, String checksum, Node parent)
    {
        parent.addFile(fileName, checksum);
    }
    
    public void deleteNode(Node toBeDeleted) //This is used only for cases in which a whole portion of the directory structure is excluded
    {
        if(!toBeDeleted.getDirectoryName().equals(root.getDirectoryName()))
        {          
            Node directory = toBeDeleted.getParent();
            String returnValue = directory.deleteChild(toBeDeleted);
            toBeDeleted.newParent(null);
            if(returnValue.equals("deleteMe"))
            {
                deleteNode(directory);
            }
        }
    }

    public String retrieveMissingFiles(Node start, ArrayList<String> exclusions, String home_dir)
    {
        String toReturn = recurseRemainingDirectories(start, exclusions, home_dir);
        return toReturn;
    }
    private String recurseRemainingDirectories(Node start, ArrayList<String> exclusions, String home_dir)
    {
        String toFill = "";
        String path = home_dir + "/" + start.getWholePath();
        if(!start.toString().equals(""))
        {
            if(!exclusions.contains(path))
            {
//                System.out.println("Non Excluded Missing Path:" + path);
                toFill += start.toString(exclusions, home_dir);
//                System.out.println("toFill+=:" + start.toString(exclusions, home_dir));
            }
        }

        if(!exclusions.contains(path))
        {
            ArrayList<Node> children = start.getChildren();
            for(int i=0; i<children.size(); i++)
            {
                toFill += recurseRemainingDirectories(children.get(i), exclusions, home_dir);
            }
        }
        return toFill;
    }
}



class Node
{
    String directoryName;
    ArrayList<fileChecksumPair> files;
    Node parent;
    ArrayList<Node> children;
    
    public Node(String directoryName, Node parent)
    {
        this.directoryName = directoryName;
        this.parent = parent;
        children = new ArrayList<Node>();
        files = new ArrayList<fileChecksumPair>();
    }
    
    public void newParent(Node parent)
    {
        this.parent = parent;
    }
    public void addChild(Node childNode)
    {
        children.add(childNode);
    }
    public void addFile(String fileName, String checkSum)
    {
        fileChecksumPair newFile = new fileChecksumPair (fileName, checkSum);
        files.add(newFile);
    }
    
    public String deleteFile (String FileToBeDeleted)
    {
        String toReturn = "";
        for(int i=0; i<files.size(); i++)
        {
            if(files.get(i).fileName.equals(FileToBeDeleted))
            {
                deletePair(files.get(i));
            }
        }
        
        if(files.isEmpty() && children.isEmpty())
        {
            toReturn = "deleteMe";
        }
        else
        {
            toReturn = "wereGood";
        }
        return toReturn;
    }
    
    private String deletePair(fileChecksumPair toBeDeleted)
    {
        String toReturn = "";
        files.remove(toBeDeleted);
        if(files.isEmpty() && children.isEmpty())
        {
            toReturn = "deleteMe";
        }
        else
        {
            toReturn = "wereGood";
        }
        return toReturn;
    }

    public String deleteChild(Node toBeDeleted)
    {
        String toReturn = "";
        children.remove(toBeDeleted);
        if(files.isEmpty() && children.isEmpty())
        {
            toReturn = "deleteMe";
        }
        else
        {
            toReturn = "wereGood";
        }
        return toReturn;
    }
    public String getDirectoryName()
    {
        return directoryName;
    }
    public Node getParent()
    {
        return parent;
    }
    public ArrayList<Node> getChildren()
    {
        return children;
    }
    public ArrayList<fileChecksumPair> getFiles()
    {
        return files;
    }
    public String getChecksumOfFile(String fileName)
    {
        String toBeReturned = "";
        for(int i=0; i<files.size();i++)
        {
            if(files.get(i).fileName.equals(fileName))
            {
                toBeReturned = files.get(i).checkSum;
            }
        }
        return toBeReturned;
    }
    
    public String getWholePath()
    {
        Node current = parent;
        ArrayList<String> directories = new ArrayList<String>();
        String toReturn = "";
        while(current != null)
        {
            directories.add(current.getDirectoryName());
            current = current.getParent();
        }
        
        for(int i=(directories.size()); i>0; i--)
        {
            toReturn += directories.get(i-1)+"/";
        }
        toReturn += directoryName;
        
        return toReturn;
    }
    public String toString(ArrayList<String> exclusions, String home_dir)
    {
        String toReturn = "";
        String nextFile = "";
        if(!files.isEmpty())
        {
            String path = getWholePath();
            for (int i=0; i<files.size();i++)
            {
                if(!files.get(i).toString().equals("manifest.txt") && !files.get(i).toString().equals("manifest.txt.swp"))
                {
                    nextFile = home_dir + "/" + path + "/" + files.get(i).toString();
                    if(!exclusions.contains(nextFile))
                    {
                        toReturn += "\n\t" + nextFile;
                    }
                }
               
            }
        }
        return toReturn;        
    }
}
class fileChecksumPair
{
    String fileName;
    String checkSum;
    
    public fileChecksumPair (String fileName, String checkSum)
    {
        this.fileName = fileName;
        this.checkSum = checkSum;
    }
    public String toString()
    {
        return fileName;
    }
}