package com.resolve.test.controller.sample;

import static com.resolve.test.controller.util.ResolveCookie.getResolveCookie;
import static io.restassured.RestAssured.given;
import org.junit.Test;

import com.resolve.test.controller.util.Logger;
import com.resolve.test.controller.util.ResponseSpecGenerator;

import io.restassured.response.Response;

//Example using custom matcher and json file reader

public class SampleScript2 {

	@Test
	public void testGetAllTagsWithResponseJson() throws Exception {

		Response response =
				given().
					filter(getResolveCookie()).//log().all().
				when().
					get("tag/listTags").
				then().
				assertThat().
					statusCode(200).
				and().
					spec(new ResponseSpecGenerator("sample/SampleScript2.json").matchFile()).
				extract().response();

			Logger.debug(response.asString());
	}

}
