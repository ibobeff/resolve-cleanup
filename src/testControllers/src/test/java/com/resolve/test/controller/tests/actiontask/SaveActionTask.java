package com.resolve.test.controller.tests.actiontask;


import org.junit.Test;
import net.serenitybdd.junit.runners.SerenityRunner;
import net.thucydides.core.annotations.Steps;
import org.junit.runner.RunWith;

import com.resolve.test.controller.steps.ActionTaskSteps;

@RunWith(SerenityRunner.class)

public class SaveActionTask {

	@Steps
	ActionTaskSteps actiontaskSteps;
	
	@Test
	public void testSaveActionTask() throws Exception {
		actiontaskSteps.saveActionTask("testControllers", "saveAT_BaseCase");
		actiontaskSteps.testForStatusCode(200);
		//actiontaskSteps.checkJsonSchema("ImpexLogList.json");
		actiontaskSteps.checkSuccessful();
		actiontaskSteps.checkFullName("saveAT_BaseCase#testControllers");
	}
}
