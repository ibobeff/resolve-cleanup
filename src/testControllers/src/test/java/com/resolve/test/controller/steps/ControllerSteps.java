package com.resolve.test.controller.steps;

import static io.restassured.RestAssured.given;
import static io.restassured.module.jsv.JsonSchemaValidator.matchesJsonSchemaInClasspath;
import static org.hamcrest.Matchers.equalTo;
import static org.hamcrest.Matchers.is;
import static org.hamcrest.Matchers.nullValue;

import com.resolve.test.controller.util.GeneralTest;
import com.resolve.test.controller.util.ResponseSpecGenerator;

import io.restassured.response.Response;
import net.thucydides.core.annotations.Step;

public class ControllerSteps extends GeneralTest {

	protected Response response;
	
	@Step
	public void get(String path) throws Exception {
		response =
				given().
					filter(getResolveCookie()).
				when().
					get(path);

			//Logger.debug(response.asString());
	}
	
	@Step
	public void testForStatusCode(int statusCode) throws Exception {
		response.then().assertThat().statusCode(statusCode);
	}
	
	@Step
	public void matchJsonFile(String jsonFile) throws Exception {
		response.then().spec(new ResponseSpecGenerator(jsonFile).matchFile());
	}
	
	@Step
    public void checkSuccessful() throws Exception {
        response.then().assertThat().body("success", equalTo(true));
    }
	
	@Step
    public void checkJsonSchema(String schema) throws Exception {
        response.then().assertThat().body(matchesJsonSchemaInClasspath("Schemas/" + schema));
    }
	
	@Step
    public void checkBodyPropertyEquals(String propId, String expectedValue) throws Exception {
        response.then().assertThat().body(propId, equalTo(expectedValue));
    }
	
	@Step
    public void checkBodyPropertyEquals(String propId, Boolean expectedValue) throws Exception {
        response.then().assertThat().body(propId, equalTo(expectedValue));
    }
	
	@Step
    public void checkBodyPropertyIsNull(String propId) throws Exception {
        response.then().assertThat().body(propId, is(nullValue()));
    }
}
