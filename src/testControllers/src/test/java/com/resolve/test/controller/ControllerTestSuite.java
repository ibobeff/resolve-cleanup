package com.resolve.test.controller;

import org.junit.BeforeClass;
import org.junit.runner.RunWith;
import org.junit.runners.Suite;

import com.resolve.test.controller.tests.*;
import com.resolve.test.controller.tests.tag.DeleteTags;
import com.resolve.test.controller.tests.tag.ListTags;

import static com.resolve.test.controller.util.ResolveCookie.getResolveCookie;

@RunWith(Suite.class)

@Suite.SuiteClasses({
	//ListTags.class,
	//DeleteTags.class
//	/actiontask/get 
//	/actiontask/save 
//	/actiontask/delete 
//	/actiontask/actiontaskmanipulation 
//	/wiki/get 
//	/wiki/save 
//	/wiki/names 
//	/wiki/resultmacro 
//	/wiki/listPlaybookTemplates 
//	/wiki/getSIRAttachments
})

public class ControllerTestSuite {
	
	@BeforeClass
	public static void setUp() throws Exception{
		getResolveCookie();
	}
	
}
