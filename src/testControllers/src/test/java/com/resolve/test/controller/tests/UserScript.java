package com.resolve.test.controller.tests;

import org.junit.Test;

public class UserScript {

	@Test
	public void testListUsers() throws Exception {
		ControllerTestSteps.testForSuccessStatusCodeOnGet("user/listUsers");
	}
	
	@Test
	public void testListRoles() throws Exception {
		ControllerTestSteps.testForSuccessStatusCodeOnGet("user/listRoles");
	}

	@Test
	public void testListGroups() throws Exception {
		ControllerTestSteps.testForSuccessStatusCodeOnGet("user/listGroups");
	}
	
	@Test
	public void testListOrgs() throws Exception {
		ControllerTestSteps.testForSuccessStatusCodeOnGet("user/listOrgs");
	}
	
}
