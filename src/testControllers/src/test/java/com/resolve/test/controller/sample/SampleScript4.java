package com.resolve.test.controller.sample;

import static com.resolve.test.controller.util.ResolveCookie.getResolveCookie;
import static io.restassured.RestAssured.given;
import static org.hamcrest.Matchers.*;

import org.junit.Test;

import com.resolve.test.controller.util.Logger;
import io.restassured.builder.ResponseSpecBuilder;
import io.restassured.http.ContentType;
import io.restassured.response.Response;
import io.restassured.specification.ResponseSpecification;

//Example using ResponseSpecBuilder

public class SampleScript4 {

	ResponseSpecification checkStatusCodeAndSuccess = 
		    new ResponseSpecBuilder().
		        expectStatusCode(200).
		        expectContentType(ContentType.JSON).
		        expectBody("success", equalTo(true)).
		        expectBody("records", is(not(empty()))). //or contains...
		        build();
	
	@Test
	public void testGetTagsWithResponseSpec() throws Exception {

		Response response =
			given().
				filter(getResolveCookie()).//log().all().
			when().
				get("tag/listTags").
			then().
				assertThat().
		        spec(checkStatusCodeAndSuccess).
			extract().response();

		Logger.debug(response.asString());
	}

}
