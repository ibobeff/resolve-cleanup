package com.resolve.test.controller.tests.actiontask;


import org.junit.Test;
import net.serenitybdd.junit.runners.SerenityRunner;
import net.thucydides.core.annotations.Steps;
import org.junit.runner.RunWith;

import com.resolve.test.controller.steps.ActionTaskSteps;

@RunWith(SerenityRunner.class)

public class DeleteActionTask {

	@Steps
	ActionTaskSteps actiontaskSteps;
	
	@Test
	public void testDeleteActionTask() throws Exception {
		String id = actiontaskSteps.saveActionTask("testControllers", "deleteAT_BaseCase");
		//TODO - Need to create sql script for data to be deleted
		actiontaskSteps.deleteActionTask(id);
		actiontaskSteps.testForStatusCode(200);
		actiontaskSteps.checkSuccessful();
		//TODO - Come up with a better way to verify without using another controller. The server returns true whether or not the id exists
	}
}
