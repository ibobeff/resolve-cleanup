package com.resolve.test.controller.tests.importexportlog;

import org.junit.Test;
import org.junit.runner.RunWith;

import com.resolve.test.controller.steps.ImpexLogSteps;
import com.resolve.test.controller.util.GeneralTest;

import net.serenitybdd.junit.runners.SerenityRunner;
import net.thucydides.core.annotations.Steps;

@RunWith(SerenityRunner.class)

public class GetImpexLogRec extends GeneralTest {

    @Steps
    ImpexLogSteps impexLogSteps;
	
	@Test
    public void testImportExportLogRec() throws Exception {
        impexLogSteps.getImpexLogRec("8a80cb816103b172016103b9470e00e1");
        impexLogSteps.testForStatusCode(200);
        impexLogSteps.checkJsonSchema("ImpexLogRec.json");
        //TODO - as this request could be checked 1:1, we could verify it against a json template
        //impexLogSteps.matchJsonFile("ExpectedData/getImpexLogRec.json");
        impexLogSteps.checkSuccessful();
        impexLogSteps.checkBodyPropertyEquals("data.id", "8a80cb816103b172016103b9470e00e1");
        impexLogSteps.checkBodyPropertyEquals("data.utype", "IMPORT");
        impexLogSteps.checkBodyPropertyEquals("data.sysIsDeleted", false);
    }
	
	// Negative Test - request an invalid id
	@Test
    public void testImportExportLogRecInvalid() throws Exception {
        impexLogSteps.getImpexLogRec("INVALID_ID");
        impexLogSteps.testForStatusCode(200);
        impexLogSteps.checkSuccessful();
        impexLogSteps.checkBodyPropertyIsNull("data");
    }
}
