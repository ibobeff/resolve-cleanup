package com.resolve.test.controller.tests;

import org.junit.Test;

public class SocialScript {

	@Test
	public void testListAllStreams() throws Exception {
		ControllerTestSteps.testForSuccessStatusCodeOnGet("social/listAllStreams");
	}
	
	@Test
	public void testListUserStreams() throws Exception {
		ControllerTestSteps.testForSuccessStatusCodeOnGet("social/listUserStreams");
	}
	
	@Test
	public void testListDocumentUserStreams() throws Exception {
		ControllerTestSteps.testForSuccessStatusCodeOnGet("social/listDocumentUserStreams");
	}

	@Test
	public void testListActionTaskUserStreams() throws Exception {
		ControllerTestSteps.testForSuccessStatusCodeOnGet("social/listActionTaskUserStreams");
	}
	
	@Test
	public void testGetUserStreams() throws Exception {
		ControllerTestSteps.testForSuccessStatusCodeOnGet("social/getUserStreams");
	}
	
	@Test
	public void testListComponentTypes() throws Exception {
		ControllerTestSteps.testForSuccessStatusCodeOnGet("social/listComponentTypes");
	}
	
	@Test
	public void testAttachmentList() throws Exception {
		ControllerTestSteps.testForSuccessStatusCodeOnPost("social/attachmentList");
	}

}
