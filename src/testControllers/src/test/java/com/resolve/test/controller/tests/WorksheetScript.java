package com.resolve.test.controller.tests;

import org.junit.Test;
import org.junit.Ignore;

public class WorksheetScript {

	@Test
	public void testListWorksheets() throws Exception {
		ControllerTestSteps.testForSuccessStatusCodeOnGet("worksheet/list");
	}
	
	@Test
	public void testGetActive() throws Exception {
		ControllerTestSteps.testForSuccessStatusCodeOnGet("worksheet/getActive");
	}
	
	@Test
	public void testGetNextNumber() throws Exception {
		ControllerTestSteps.testForSuccessStatusCodeOnGet("worksheet/getNextNumber");
	}
	
	@Test
	public void testListProcessRequests() throws Exception {
		ControllerTestSteps.testForSuccessStatusCodeOnGet("processrequest/list");
	}
	
	@Ignore @Test
	public void testListTaskResults() throws Exception {
		ControllerTestSteps.testForSuccessStatusCodeOnGet("taskresult/list");
	}
}
