package com.resolve.test.controller.steps;

import net.thucydides.core.annotations.Step;

import static io.restassured.RestAssured.given;
import static org.hamcrest.Matchers.*;

import com.resolve.test.controller.util.EndPoints;

public class TagSteps extends ControllerSteps{
	
	@Step
	public void getAllTags() throws Exception {
		get(EndPoints.TAG_LIST);
	}
	
	@Step
	public void returnedTagsMatchJSON(String jsonFile) throws Exception {
		matchJsonFile("TagJson/ListTags.json");
	}
	
	@Step
	public void returnedTagsContainExpected() throws Exception {
		response.then().body("records", hasItem(allOf(hasEntry("name", "Test_Tag2"),hasEntry("description", "Tag 2 Description"))));
		response.then().body("records", hasItem(allOf(hasEntry("name", "Test_Tag1"),hasEntry("description", "Tag 1 Description"))));
		
	}
	
	@Step
    public void deleteTag(String id) throws Exception {
	    response = 
	        given().
            filter(getResolveCookie()).
            header("Content-Type", "application/x-www-form-urlencoded; charset=UTF-8").
            body("ids=" + id).
        when().
            post(EndPoints.TAG_DELETE);
    }

	@Step
	public String saveTag(String name) throws Exception {
		 response = 
			        given().
		            filter(getResolveCookie()).
		            header("Content-Type", "application/json").
		            body("{\"id\":\"\",\"name\":\"" + name + "\",\"description\":\"\"}").
		        when().
		            post(EndPoints.TAG_SAVE);
		 
		 return response.path("data.id");
	}
	
	@Step
    public void checkSysId(String sysId) throws Exception {
        response.then().assertThat().body("data.id", is(sysId));
    }
	
	@Step
    public void checkName(String name) throws Exception {
        response.then().assertThat().body("data.name", is(name));
    }
}
