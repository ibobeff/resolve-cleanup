package com.resolve.test.controller.steps;

import static io.restassured.RestAssured.given;
import static org.hamcrest.Matchers.is;

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.resolve.test.controller.util.EndPoints;

import net.thucydides.core.annotations.Step;

public class ActionTaskSteps extends ControllerSteps{

	@Step
	public void getActionTaskById(String id) throws Exception {
		
		response =
				given().
					filter(getResolveCookie()).
					header("Content-Type", "application/x-www-form-urlencoded; charset=UTF-8").
					body("id=" + id + "&name=").
				when().
					post(EndPoints.ACTIONTASK_GET);
	}
	
	@Step
	public void getActionTaskByName(String name) throws Exception {
		
		response =
				given().
					filter(getResolveCookie()).
					header("Content-Type", "application/x-www-form-urlencoded; charset=UTF-8").
					body("id=&name=" + name).
				when().
					post(EndPoints.ACTIONTASK_GET);
	}

	@Step
	public void checkSysId(String sysId) {
		response.then().assertThat().body("data.id", is(sysId));
	}

	@Step
	public void checkFullName(String fullName) {
		response.then().assertThat().body("data.ufullName", is(fullName));
	}
	
	@Step
	public String saveActionTask(String namespace, String name) throws Exception {
		
		String taskJson = "{\"id\":\"\",\"unamespace\":\"" + namespace + "\",\"uname\":\"" + name + "\",\"ufullName\":\"#\",\"umenuPath\":\"/" + namespace + "\",\"usummary\":\"\",\"udescription\":\"\",\"uactive\":true,\"ulogresult\":true,\"uisHidden\":false,\"wizardType\":\"\",\"resolveActionInvoc\":{\"utype\":\"ASSESS\",\"utimeout\":300,\"resolveActionInvocOptions\":[{\"id\":\"\",\"uname\":\"INPUTFILE\",\"udescription\":\"\",\"uvalue\":\"\"}],\"resolveActionParameters\":[],\"preprocess\":{\"id\":\"\",\"uname\":\"\",\"uscript\":\"\"},\"ucommand\":\"\",\"parser\":{\"parserType\":\"text\",\"sampleOutput\":\"\",\"parserAutoGenEnabled\":true,\"parserConfig\":null},\"assess\":{\"id\":\"\",\"uname\":\"\",\"uscript\":\"\",\"uonlyCompleted\":true,\"summaryRule\":\"[]\",\"detailRule\":\"[]\"},\"outputMappings\":[],\"expressions\":[],\"resolveActionTaskMockData\":[]},\"assignedTo\":null,\"uisDefaultRole\":true,\"accessRights\":{\"ureadAccess\":\"\",\"uwriteAccess\":\"\",\"uexecuteAccess\":\"\",\"uadminAccess\":\"\"},\"referencedIn\":[],\"tags\":[]}";
		//TODO - Need to parameterize the json for creating an actiontask. This should be a map, not a string. Or create a POJO model of an actiontask?
		response =
				given().
					filter(getResolveCookie()).
					header("Content-Type", "application/json").
					body(taskJson).
				when().
					post(EndPoints.ACTIONTASK_SAVE);
		
		return response.path("data.id");
	}
	
	@Step
	public void deleteActionTask(String id) throws Exception {
		 
		response =
				given().
					filter(getResolveCookie()).
					header("Content-Type", "application/x-www-form-urlencoded").
					body("ids=" + id + "&all=false&validate=true").
				when().
					post(EndPoints.ACTIONTASK_DELETE);
	}

	@Step
	public void moveActionTask(String id, String module, String name, Boolean overwrite) throws Exception {

		response =
				given().
					filter(getResolveCookie()).
					header("Content-Type", "application/x-www-form-urlencoded").
					body("ids=" + id +"&action=rename&module=" + module + "&name=" + name + "&overwrite=" + overwrite).
				when().
					post(EndPoints.ACTIONTASK_MOVE_RENAME);

	}
	
	@Step
	public void moveActionTaskOverwrite(String id, String module, String name) throws Exception {

		moveActionTask(id, module, name, true);

	}
}
