package com.resolve.test.controller.tests;

import org.junit.Ignore;
import org.junit.Test;

public class GatewayScript {

	@Ignore @Test
	public void testListFilters() throws Exception {
		// String modelName
		ControllerTestSteps.testForSuccessStatusCodeOnGet("gateway/listFilters");
	}
	
	@Ignore @Test
	public void testListGateways() throws Exception {
		// @Param String gatewayName
		ControllerTestSteps.testForSuccessStatusCodeOnGet("gateway/listGateways");
	}

	@Test
	public void testListQueues() throws Exception {
		ControllerTestSteps.testForSuccessStatusCodeOnGet("gateway/listQueues");
	}

	@Ignore @Test
	public void testListDeployedFilters() throws Exception {
		// @Param String modelName
		ControllerTestSteps.testForSuccessStatusCodeOnGet("gateway/listDeployedFilters");
	}
	
	@Test
	public void testGetDatabaseDrivers() throws Exception {
		ControllerTestSteps.testForSuccessStatusCodeOnGet("gateway/getDatabaseDrivers");
	}
	
	
}
