package com.resolve.test.controller.sample;

import static com.resolve.test.controller.util.ResolveCookie.getResolveCookie;
import static io.restassured.RestAssured.given;
import static org.hamcrest.Matchers.*;

import org.junit.Test;

import com.resolve.test.controller.util.Logger;
import io.restassured.response.Response;

//This is the base example to test login and cookie handling

public class SampleScript1 {

	@Test
	public void testGetAllTags() throws Exception {

		Response response =
			given().
				filter(getResolveCookie()).log().all().
			when().
				get("tag/listTags").
			then().
			assertThat().
				statusCode(200).
			and().
				body("success", equalTo(true)).
			and().
				body("records", is(not(empty()))).
			and().
			    body("records.sysUpdatedBy", everyItem(is("admin"))).
            and().
                body("records.sysCreatedBy", hasSize(1)).
            and().
                body("records.any { it.containsKey('sysCreatedBy') }", is(true)).
			extract().response();

		Logger.debug(response.asString());
	}

}
