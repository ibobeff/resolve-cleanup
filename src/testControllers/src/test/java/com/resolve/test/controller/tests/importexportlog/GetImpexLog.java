package com.resolve.test.controller.tests.importexportlog;

import org.junit.Test;
import org.junit.runner.RunWith;

import com.resolve.test.controller.steps.ImpexLogSteps;
import com.resolve.test.controller.util.GeneralTest;

import net.serenitybdd.junit.runners.SerenityRunner;
import net.thucydides.core.annotations.Steps;

@RunWith(SerenityRunner.class)

public class GetImpexLog extends GeneralTest {

    @Steps
    ImpexLogSteps impexLogSteps;

	@Test
	public void testImportExportLogList() throws Exception {
		impexLogSteps.getAllImpexLogs();
		impexLogSteps.testForStatusCode(200);
		impexLogSteps.checkJsonSchema("ImpexLogList.json");
		impexLogSteps.checkSuccessful();
		impexLogSteps.checkTotal();
		impexLogSteps.checkRecordsSize();
		impexLogSteps.checkRecordExists("8a80cb816103b172016103b9470e00e1");
		impexLogSteps.checkSysId("8a80cb816103b172016103b9470e00e1");
	}

}
