package com.resolve.test.controller.util;

import java.util.Arrays;
import java.util.Collection;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;
import org.junit.runners.Parameterized.Parameters;

import static org.junit.Assert.assertEquals;

import com.resolve.test.controller.util.Logger.LogLevels;

@RunWith(Parameterized.class)
public class LoggerTest {
	
	private String logLevelString;
	private LogLevels logLevelEnum;
	
	public LoggerTest(String logLevelString,LogLevels logLevelEnum) {
		this.logLevelString = logLevelString;
		this.logLevelEnum = logLevelEnum;
	}
	
	@Parameters
	public static Collection<Object[]> data(){
		return Arrays.asList(new Object[][]{{"DEBUG",LogLevels.DEBUG},{"TRACE",LogLevels.TRACE},{"INFO",LogLevels.INFO},{"ERROR",LogLevels.ERROR}});
	}
	
	@Test
	public void getLevelFromStringtest() {
		assertEquals(LogLevels.getLevelFromString(logLevelString),logLevelEnum);
	}
	
	@Test
	public void changeLogLeveltest(){
		//Set the value
		Logger.changeLogLevel(logLevelEnum);
		
		//check default value
		assertEquals(Logger.getLogLevel(),logLevelEnum);
		
	}

}
