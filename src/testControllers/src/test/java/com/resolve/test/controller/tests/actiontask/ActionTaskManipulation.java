package com.resolve.test.controller.tests.actiontask;


import org.junit.Test;
import net.serenitybdd.junit.runners.SerenityRunner;
import net.thucydides.core.annotations.Steps;
import org.junit.runner.RunWith;

import com.resolve.test.controller.steps.ActionTaskSteps;

@RunWith(SerenityRunner.class)

public class ActionTaskManipulation {

	@Steps
	ActionTaskSteps actiontaskSteps;
	
	@Test
	public void testMoveActionTask() throws Exception {
		String id = actiontaskSteps.saveActionTask("testControllers", "moveAT_BaseCase");
		//TODO - Need to create sql script for data to be deleted and remove above call
		actiontaskSteps.moveActionTaskOverwrite(id, "testControllers", "moveAT_BaseCase2");
		actiontaskSteps.testForStatusCode(200);
		actiontaskSteps.checkSuccessful();
		//TODO - Come up with a better way to verify. Controller doesn't return any useful data.
	}
}
