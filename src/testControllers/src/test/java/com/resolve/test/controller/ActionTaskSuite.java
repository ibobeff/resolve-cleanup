package com.resolve.test.controller;

import org.junit.BeforeClass;
import org.junit.runner.RunWith;
import org.junit.runners.Suite;

import com.resolve.test.controller.tests.*;
import com.resolve.test.controller.tests.actiontask.ActionTaskManipulation;
import com.resolve.test.controller.tests.actiontask.DeleteActionTask;
import com.resolve.test.controller.tests.actiontask.GetActionTask;
import com.resolve.test.controller.tests.actiontask.SaveActionTask;
import com.resolve.test.controller.tests.importexportlog.DeleteImpexLogRec;
import com.resolve.test.controller.tests.importexportlog.GetImpexLog;
import com.resolve.test.controller.tests.importexportlog.GetImpexLogRec;
import com.resolve.test.controller.tests.tag.DeleteTags;
import com.resolve.test.controller.tests.tag.ListTags;

import static com.resolve.test.controller.util.ResolveCookie.getResolveCookie;

@RunWith(Suite.class)

@Suite.SuiteClasses({
	GetActionTask.class,
	SaveActionTask.class,
	DeleteActionTask.class,
	ActionTaskManipulation.class
})

public class ActionTaskSuite {
	
	@BeforeClass
	public static void setUp() throws Exception{
		getResolveCookie();
	}
	
}
