package com.resolve.test.controller.util;

import org.junit.runner.RunWith;
import org.junit.runners.Suite;

@RunWith(Suite.class)
@Suite.SuiteClasses({
	LoggerTest.class,
	LoginPropertiesTest.class
})
public class UtilTests {

}
