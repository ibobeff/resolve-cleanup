package com.resolve.test.controller;

import org.junit.BeforeClass;
import org.junit.runner.RunWith;
import org.junit.runners.Suite;

import com.resolve.test.controller.tests.*;
import com.resolve.test.controller.tests.tag.DeleteTags;
import com.resolve.test.controller.tests.tag.ListTags;
import com.resolve.test.controller.tests.tag.SaveTag;

import static com.resolve.test.controller.util.ResolveCookie.getResolveCookie;

@RunWith(Suite.class)

@Suite.SuiteClasses({
	ListTags.class,
	SaveTag.class,
	DeleteTags.class
})

public class TagSuite {
	
	@BeforeClass
	public static void setUp() throws Exception{
		getResolveCookie();
	}
	
}
