package com.resolve.test.controller.sample;

import static com.resolve.test.controller.util.ResolveCookie.getResolveCookie;
import static io.restassured.RestAssured.given;
import static org.hamcrest.Matchers.*;

import java.util.HashMap;
import java.util.Map;

import org.junit.Test;
import org.junit.runner.RunWith;

import com.resolve.test.controller.util.Logger;
import com.tngtech.java.junit.dataprovider.*;

import io.restassured.response.Response;

//Example using data driver
//Uses junit-dataprovider to make it more like TestNG
//This may make it easier for us to create data driven tests
//We could also use this as a model to make our own version that 
//specifies a json file as the input and loads that before the test

//Not a great example because obviously the same name is expected. Need to pick a better controller to flesh this out

@RunWith( DataProviderRunner.class)
public class SampleScript3 {
	
	@DataProvider
	public static Object[][] provideTagNames() {
	    return new Object[][] {
	        { "Test_Tag1", "Tag 1 Description" },
	        { "Test_Tag2", "Tag 2 Description" }
	    };
	}

	@Test
	@UseDataProvider("provideTagNames")
	public void testSaveTagWithDataDriver(String inputTag, String inputDescription) throws Exception {

		Map<String, Object>  jsonAsMap = new HashMap<>();
		jsonAsMap.put("id", "");
		jsonAsMap.put("name", inputTag);
		jsonAsMap.put("description", inputDescription);
		
		Response response =
			given().
				filter(getResolveCookie()).//log().all().
				header("Content-Type","application/json").
				body(jsonAsMap).
			when().
				post("tag/saveTag").
			then().
			assertThat().
					statusCode(200).
				and().
					body("success", equalTo(true)).
				and().
					body("data.name", equalTo(inputTag)).
				and().
					body("data.description", equalTo(inputDescription)).
			extract().response();

		Logger.debug(response.asString());
	}

}
