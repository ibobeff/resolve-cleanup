package com.resolve.test.controller.tests;

import org.junit.Ignore;
import org.junit.Test;

public class CatalogScript {

	@Test
	public void testList() throws Exception {
		ControllerTestSteps.testForSuccessStatusCodeOnGet("catalog/list");
	}
	
	@Test
	public void testNames() throws Exception {
		ControllerTestSteps.testForSuccessStatusCodeOnGet("catalog/names");
	}
	
	@Ignore @Test
	public void testRefNames() throws Exception {
		// @Param String catalogId
		ControllerTestSteps.testForSuccessStatusCodeOnGet("catalog/refnames");
	}
	
	@Test
	public void testImageList() throws Exception {
		ControllerTestSteps.testForSuccessStatusCodeOnGet("catalog/imageList");
	}
	
	@Test
	public void testTags() throws Exception {
		ControllerTestSteps.testForSuccessStatusCodeOnGet("catalog/tags");
	}
}
