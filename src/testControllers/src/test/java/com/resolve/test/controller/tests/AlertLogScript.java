package com.resolve.test.controller.tests;

import org.junit.Ignore;
import org.junit.Test;

public class AlertLogScript {

	@Ignore @Test
	public void testGetAlertLog() throws Exception {
		ControllerTestSteps.testForSuccessStatusCodeOnGet("alertlog/getAlertLog");
	}
	
	@Test
	public void testGetLicenseLog() throws Exception {
		ControllerTestSteps.testForSuccessStatusCodeOnGet("alertlog/getLicenseLog");
	}
	
}
