package com.resolve.test.controller.tests;

import org.junit.Test;

public class JobSchedulerScript {

	@Test
	public void testJobSchedulerList() throws Exception {
		ControllerTestSteps.testForSuccessStatusCodeOnGet("jobscheduler/list");
	}
}
