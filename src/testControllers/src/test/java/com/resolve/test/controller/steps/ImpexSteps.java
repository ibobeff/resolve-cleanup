package com.resolve.test.controller.steps;

import static io.restassured.RestAssured.given;

import java.io.File;

import com.resolve.test.controller.util.EndPoints;

import net.thucydides.core.annotations.Step;

public class ImpexSteps extends ControllerSteps{
    
	@Step
	public void uploadFile(String filename) throws Exception {
		given().
            filter(getResolveCookie()).
            header("Content-Type", "multipart/form-data").
            multiPart(new File(getClass().getClassLoader().getResource("Files/" + filename).getFile())).
        when().
            post(EndPoints.IMPEX_UPLOAD_FILE).
        then().//log().all().
        assertThat().statusCode(200);
	}
	
	@Step
    public void importFile(String filename) throws Exception {
	    given().
            filter(getResolveCookie()).
            header("Content-Type", "application/x-www-form-urlencoded; charset=UTF-8").
            body("moduleName=" + filename + "&excludeIds=").
        when().
            post(EndPoints.IMPEX_IMPORT).
        then().//log().all().
        assertThat().statusCode(200);
    }
}
