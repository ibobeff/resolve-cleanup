package com.resolve.test.controller.util;

import static com.resolve.test.controller.util.LoginProperties.get;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.net.URISyntaxException;
import java.nio.charset.Charset;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.Scanner;

import org.junit.Test;

import com.resolve.test.controller.util.Logger.LogLevels;


public class LoginPropertiesTest {
	
	@Test
	public void loginPropertiesTest() throws Exception {
		
		assertNull(LoginProperties.getProperties());
		LoginProperties.loadProperties();
		assertNotNull(LoginProperties.getProperties());
		
		assertEquals(getKeyFromLoginProperties("userName"), get("userName"));
		assertEquals(getKeyFromLoginProperties("baseUrl"), get("baseUrl"));
		assertEquals(getKeyFromLoginProperties("password"), get("password"));
		assertEquals(getKeyFromLoginProperties("logLevel"), get("logLevel"));
	}
	
	@Test
	public void checkIfLoggerLoadsTest() throws Exception{
		
		changeKeyInLoginProperties("logLevel", "TRACE");
		LoginProperties.loadProperties();
		assertEquals(Logger.getLogLevel(), LogLevels.TRACE);
		
		changeKeyInLoginProperties("logLevel", "DEBUG");
		LoginProperties.loadProperties();
		assertEquals(Logger.getLogLevel(),LogLevels.DEBUG);
		
		changeKeyInLoginProperties("logLevel", "ERROR");
		LoginProperties.loadProperties();
		assertEquals(Logger.getLogLevel(),LogLevels.ERROR);
		
		changeKeyInLoginProperties("logLevel", "INFO");
		LoginProperties.loadProperties();
		assertEquals(Logger.getLogLevel(),LogLevels.INFO);
		
		
		
	}
	
	private String getKeyFromLoginProperties(String key) throws FileNotFoundException{
		File file = new File(LoginProperties.class.getClassLoader().getResource("login.properties").getPath());
		final Scanner scanner = new Scanner(file);
		while (scanner.hasNextLine()) {
			 final String lineFromFile = scanner.nextLine();
			 if(lineFromFile.contains(key)) { 
			     scanner.close();  
				 return lineFromFile.substring(key.length()+1);
			   }
		}
		scanner.close();
		return null;
	}
	
	private void changeKeyInLoginProperties(String key,String value) throws IOException, URISyntaxException{
		Path loginPropertiesPath = Paths.get(ClassLoader.getSystemResource("login.properties").toURI());
		Charset charset = StandardCharsets.UTF_8;
		String content = new String(Files.readAllBytes(loginPropertiesPath), charset);
		content = content.replaceAll(key+".+", key+"="+value);
		Files.write(loginPropertiesPath, content.getBytes(charset));
	}
	
}
