package com.resolve.test.controller.tests;

import org.junit.Ignore;
import org.junit.Test;

public class WikiScript {

	@Test
	public void testNamespaceList() throws Exception {
		ControllerTestSteps.testForSuccessStatusCodeOnGet("wiki/namespace/list");
	}
	
	@Test
	public void testListPlaybookTemplates() throws Exception {
		ControllerTestSteps.testForSuccessStatusCodeOnGet("wiki/listPlaybookTemplates");
	}
	
	@Ignore @Test
	public void testNames() throws Exception {
		ControllerTestSteps.testForSuccessStatusCodeOnGet("wiki/names");
	}

}
