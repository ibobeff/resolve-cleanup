package com.resolve.test.controller.tests;

import org.junit.Ignore;
import org.junit.Test;

public class WikiAdminScript {

	@Ignore @Test
	public void testListWikis() throws Exception {
		ControllerTestSteps.testForSuccessStatusCodeOnGet("wikiadmin/list");
	}
	
	@Ignore @Test
	public void testListRunbooks() throws Exception {
		ControllerTestSteps.testForSuccessStatusCodeOnGet("wikiadmin/listRunbooks");
	}
	
	@Ignore @Test
	public void testListWikidocs() throws Exception {
		ControllerTestSteps.testForSuccessStatusCodeOnGet("wiki/list");
	}
	
	@Test
	public void testListPlaybookTemplates() throws Exception {
		ControllerTestSteps.testForSuccessStatusCodeOnGet("wikiadmin/listPlaybookTemplates");
	}
}
