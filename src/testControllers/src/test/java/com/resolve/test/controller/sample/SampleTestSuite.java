package com.resolve.test.controller.sample;

import org.junit.BeforeClass;
import org.junit.runner.RunWith;
import org.junit.runners.Suite;
import static com.resolve.test.controller.util.ResolveCookie.getResolveCookie;


@RunWith(Suite.class)
@Suite.SuiteClasses({
	SampleScript1.class,
	SampleScript2.class,
	SampleScript3.class,
	SampleScript4.class
})

public class SampleTestSuite {
	
	@BeforeClass
	public static void setUp() throws Exception{
		getResolveCookie();
	}
	
	
}
