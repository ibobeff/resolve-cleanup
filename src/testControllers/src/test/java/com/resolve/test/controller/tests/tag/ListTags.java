package com.resolve.test.controller.tests.tag;


import org.junit.Test;
import net.serenitybdd.junit.runners.SerenityRunner;
import net.thucydides.core.annotations.Steps;
import org.junit.runner.RunWith;

import com.resolve.test.controller.steps.TagSteps;
import com.resolve.test.controller.util.GeneralTest;

@RunWith(SerenityRunner.class)

public class ListTags extends GeneralTest {

	@Steps
	TagSteps tagSteps;
	
	@Test
	public void testListTagsBaseCase() throws Exception {
		tagSteps.getAllTags();
		tagSteps.testForStatusCode(200);
		tagSteps.checkSuccessful();
		tagSteps.returnedTagsContainExpected();
	}
	
	//TODO - Create sql inserts for list tags
}
