package com.resolve.test.controller.steps;

import static io.restassured.RestAssured.given;
import static io.restassured.module.jsv.JsonSchemaValidator.matchesJsonSchemaInClasspath;
import static org.hamcrest.Matchers.greaterThan;
import static org.hamcrest.Matchers.hasSize;
import static org.hamcrest.Matchers.is;
import static org.hamcrest.Matchers.notNullValue;

import java.util.HashMap;
import java.util.List;

import com.resolve.test.controller.util.EndPoints;
import com.resolve.test.controller.util.Logger;

import io.restassured.response.Response;
import net.thucydides.core.annotations.Step;

public class ImpexLogSteps extends ControllerSteps{
	
	@Step
	public void getAllImpexLogs() throws Exception {
		get(EndPoints.IMPEX_LOG_LIST);
	}
	
	@Step
	public void checkTotal() throws Exception {
	    response.then().assertThat().body("total", greaterThan(0));
	}
	
	@Step
    public void checkRecordsSize() throws Exception {
        response.then().assertThat().body("records", hasSize(greaterThan(0)));
    }
	
	@Step
    public void checkRecordExists(String recordId) throws Exception {
        response.then().assertThat().body("records.find { it.id == '" + recordId + "' }", is(notNullValue()));
    }
	
	@Step
    public void checkSysId(String sysId) throws Exception {
        response.then().assertThat().body("records.find { it.id == '" + sysId + "' }.sys_id", is(sysId));
    }
	
	@Step
    public void getImpexLogRec(String id) throws Exception {
	    response = 
	        given().
            filter(getResolveCookie()).
            header("Content-Type", "application/x-www-form-urlencoded; charset=UTF-8").
            body("id=" + id).
        when().
            post(EndPoints.IMPEX_LOG_REC);
    }

   @Step
    public void deleteLog(String id) throws Exception {
        response =
            given().
                filter(getResolveCookie()).
                header("Content-Type", "application/x-www-form-urlencoded; charset=UTF-8").
                body("ids=" + id).
            when().
                post(EndPoints.IMPEX_LOG_REC_DEL).
            then().//log().all().
            assertThat().
                    statusCode(200).
                    and().body(matchesJsonSchemaInClasspath("Schemas/ImpexLogDelete.json")).
            extract().response();
        
        Logger.debug(response.asString());
    }

	@Step
    public String getImpexLogRecIdFromImportModuleName(String importModuleName) throws Exception {
	    Response responseListCreated = 
            given().filter(getResolveCookie()).
            when().get(EndPoints.IMPEX_LOG_LIST).
            then().//log().all().
            assertThat().
                statusCode(200).
            extract().response();
	    
        List<HashMap<String, String>> records = responseListCreated.path("records");
        String impexLogId = "";
        for (int i = 0; i < records.size(); i++) {
            if (records.get(i).get("ulogSummary").contains("Module Name: " + importModuleName)) {
                impexLogId = records.get(i).get("id");
                break;
            }
        }
        return impexLogId;
    }
}
