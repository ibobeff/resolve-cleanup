package com.resolve.test.controller.tests.importexportlog;

import org.junit.Test;
import org.junit.runner.RunWith;

import com.resolve.test.controller.steps.ImpexLogSteps;
import com.resolve.test.controller.util.GeneralTest;

import net.serenitybdd.junit.runners.SerenityRunner;
import net.thucydides.core.annotations.Steps;

@RunWith(SerenityRunner.class)

public class DeleteImpexLogRec extends GeneralTest {

    @Steps
    ImpexLogSteps impexLogSteps;
	
	@Test
    public void testImportExportLogDelete() throws Exception {
        // TODO - move these to impex controller tests
	    //impexSteps.uploadFile("rx-home.zip");
        //impexSteps.importFile("rx-home");
	    //db.executeSqlCmd("impexDelete.sql");
	    String impexLogId = "2c9597a16123184f0161238b0f64007a";
	    impexLogSteps.deleteLog(impexLogId);
        // Get the list and check the log event is deleted
        impexLogSteps.getAllImpexLogs();
        impexLogSteps.testForStatusCode(200);
        impexLogSteps.checkBodyPropertyIsNull("records.find { it.id == '" + impexLogId + "' }");
    }
	
	// Negative Test - request to delete an invalid id
    @Test
    public void testImportExportLogDeleteInvalid() throws Exception {
        impexLogSteps.deleteLog("INVALID_ID");
        impexLogSteps.checkBodyPropertyIsNull("data");
    }

}
