package com.resolve.test.controller.tests.actiontask;


import org.junit.Test;
import net.serenitybdd.junit.runners.SerenityRunner;
import net.thucydides.core.annotations.Steps;
import org.junit.runner.RunWith;

import com.resolve.test.controller.steps.ActionTaskSteps;

@RunWith(SerenityRunner.class)

public class GetActionTask {

	@Steps
	ActionTaskSteps actiontaskSteps;
	
	@Test
	public void testGetActionTaskById() throws Exception {
		actiontaskSteps.getActionTaskById("e4e07300c6112272018f2a8daf9fbc46");
		actiontaskSteps.testForStatusCode(200);
		//actiontaskSteps.checkJsonSchema("ImpexLogList.json");
		actiontaskSteps.checkSuccessful();
		actiontaskSteps.checkSysId("e4e07300c6112272018f2a8daf9fbc46");
		actiontaskSteps.checkFullName("comment#resolve");
	}
	
	@Test
	public void testGetActionTaskByName() throws Exception {
		actiontaskSteps.getActionTaskByName("comment#resolve");
		actiontaskSteps.testForStatusCode(200);
		//actiontaskSteps.checkJsonSchema("ImpexLogList.json");
		actiontaskSteps.checkSuccessful();
		actiontaskSteps.checkSysId("e4e07300c6112272018f2a8daf9fbc46");
		actiontaskSteps.checkFullName("comment#resolve");
	}
}
