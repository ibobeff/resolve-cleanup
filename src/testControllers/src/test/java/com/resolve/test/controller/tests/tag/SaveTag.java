package com.resolve.test.controller.tests.tag;


import org.junit.Test;
import net.serenitybdd.junit.runners.SerenityRunner;
import net.thucydides.core.annotations.Steps;
import org.junit.runner.RunWith;

import com.resolve.test.controller.steps.TagSteps;
import com.resolve.test.controller.util.GeneralTest;

@RunWith(SerenityRunner.class)

public class SaveTag extends GeneralTest {

	@Steps
	TagSteps tagSteps;
	
	@Test
	public void testDeleteTagBaseCase() throws Exception {
		String id = tagSteps.saveTag("saveTag");
		//TODO - Create sql inserts for tag to be deleted and remove above step
		tagSteps.testForStatusCode(200);
		tagSteps.checkSuccessful();
		tagSteps.checkSysId(id);
		tagSteps.checkName("saveTag");
	}
}
