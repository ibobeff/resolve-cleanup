package com.resolve.test.controller.tests;

import org.junit.Test;

public class LicenseScript {

	@Test
	public void testLicenses() throws Exception {
		ControllerTestSteps.testForSuccessStatusCodeOnGet("license/licenses");
	}
	
	@Test
	public void testSummary() throws Exception {
		ControllerTestSteps.testForSuccessStatusCodeOnGet("license/summary");
	}
}
