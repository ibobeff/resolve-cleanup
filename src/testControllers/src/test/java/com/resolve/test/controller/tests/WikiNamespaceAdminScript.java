package com.resolve.test.controller.tests;

import org.junit.Test;

public class WikiNamespaceAdminScript {

	@Test
	public void testListNamespaces() throws Exception {
		ControllerTestSteps.testForSuccessStatusCodeOnGet("nsadmin/list");
	}
}
