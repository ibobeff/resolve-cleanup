package com.resolve.test.controller.util;

import static org.hamcrest.Matchers.*;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;

import io.restassured.builder.ResponseSpecBuilder;
import io.restassured.http.ContentType;
import io.restassured.specification.ResponseSpecification;

//This class has methods to generate and return a response spec based on what is found in the provided json file
//It should be expanded as needed as we create the controller tests

//The nice thing here is since it generates a spec instead of implementing a matcher, all the restassured/hamcrest
//matchers can be used and they'll format the output appropriately. And if we need a custom matcher, we can create that
//separately and apply the logic for when to use it here!

//Standard Resolve json response includes the following top level fields:
//success
//message
//data
//records

public class ResponseSpecGenerator{

	JsonNode expectedJSON;
	private ResponseSpecification spec; //may not want this here if the test script may try to grab it more than once
	
	public ResponseSpecGenerator(String filePath) throws IOException {
		
		ClassLoader classLoader = ClassLoader.getSystemClassLoader();
		File file = new File(classLoader.getResource(filePath).getFile());
		 
		//Read File Content
		String fileContent = new String(Files.readAllBytes(file.toPath()));
		//System.out.println(expectedJSON);
		
		ObjectMapper mapper = new ObjectMapper();
		expectedJSON = mapper.readTree(fileContent);
		
		//Initialize spec, we can add to it later
		spec = new ResponseSpecBuilder().
		        expectContentType(ContentType.JSON).
		        //expectBody("records", is(not(empty()))). //or contains...
		        build();
	}

	public ResponseSpecification matchFile() {
		addSuccessMatcher();
		addRecordsMatcherContains();
		//maybe iterate the top level fields in the expectedJSON and create matchers based on that
		return spec;
	}
	
	public ResponseSpecification matchSuccess() {
		addSuccessMatcher();
		return spec;
	}
	
	public ResponseSpecification matchRecords() {
		addRecordsMatcherContains();
		return spec;
	}
	
	private void addSuccessMatcher() {
		JsonNode successNode = expectedJSON.get("success");
		if(successNode != null)
		{
			Boolean success = successNode.asBoolean();
			Logger.debug("Found success value in json file; adding matcher to spec: body(\"success\", equalTo(" + success + "))");
			spec.body("success", equalTo(success));
		}
		else
		{
			Logger.debug("Success value not found in target json file. Matcher not added.");
		}
	}
	
	private void addRecordsMatcherContains() {
		//This is what we're looking at in the results file
		//Should iterate items and create contains matchers for their path
		
		//		"records":[  
		//		           {  
		//		              "name":"Test_Tag2",
		//		              "description":"Tag 2 Description"
		//		           },
		//		           {  
		//		              "name":"Test_Tag1",
		//		              "description":"Tag 1 Description"
		//		           }
		//		        ],
		
		//JsonNode records = expectedJSON.get("records");
		
		//TODO - Write method to generate matchers for records list
		
		//Iterate the list and generate the matchers
		//spec.body("records", hasItems(records));
		
		
		//May need to write my own "hasEnties" based on IsMapContaining and AllOf
		//Otherwise, create an all of that contains has entries for each item in list, then add a matcher for each map
		//Actually, the latter would create better reports
		
		//Is there a better way to write these?
		//spec.body("records", hasItem(allOf(hasEntry("name", "Test_Tag2"),hasEntry("description", "Tag 2 Description"))));
		//spec.body("records", hasItem(allOf(hasEntry("name", "Test_Tag1"),hasEntry("description", "Tag 1 Description"))));
		
	}
}
