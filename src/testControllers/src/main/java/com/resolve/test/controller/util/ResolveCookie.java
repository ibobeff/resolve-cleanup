package com.resolve.test.controller.util;

public class ResolveCookie {
	
	public static ResolveFilter resolveCookie;
	
	public static ResolveFilter getResolveCookie() throws Exception {
		if (resolveCookie != null) {
			return resolveCookie;
		}
		else {
			LoginUtil.loginUser();
			return resolveCookie;
		}
	}
	
}
