package com.resolve.test.controller.util;

import java.io.File;

public class DatabaseUtils {
    
    public static String dbUrl;
    public static String dbUserName;
    public static String dbPassword;
    public static String dbName;

    private static volatile DatabaseUtils INSTANCE = new DatabaseUtils();

    private DatabaseUtils () {
        try {
            // TODO - read the db properties from the command line
            if (System.getProperty("databaseUrl") != null) {
                dbUrl = System.getProperty("databaseUrl");
            } else {
                DatabaseProperties.loadProperties();
                dbUrl = DatabaseProperties.get("location");
                dbName = DatabaseProperties.get("dbName");
                dbUserName = DatabaseProperties.get("username");
                dbPassword = DatabaseProperties.get("password");
            }

            // TODO - Setup jdbc connection to the mysql instance

            // Call the reset DB script
            //executeSqlCmd("resolveDB.sql");
        }
        catch (Exception e) {
            Logger.debug(e.toString());
        }
    }
    
    public static DatabaseUtils getInstance() {
        return INSTANCE;
    }
    
    public void executeSqlCmd (String sqlFilename) {
        File file = new File(getClass().getClassLoader().getResource("SQL/" + sqlFilename).getFile());
        String absolutePath = file.getAbsolutePath();
        
        String[] executeCmd = new String[]{
            "mysqlsh",
            "--host=" + dbUrl,
            "--user=" + dbUserName,
            "--password=" + dbPassword,
            dbName,
            "-e",
            " source " + absolutePath
        };

        Process runtimeProcess;
        try {
            runtimeProcess = Runtime.getRuntime().exec(executeCmd);
            int processComplete = runtimeProcess.waitFor();
            if (processComplete == 0) {
                System.out.println("SQL executed successfully");
            } else {
                 System.out.println("Could not execute SQL");
            }
        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }
}
