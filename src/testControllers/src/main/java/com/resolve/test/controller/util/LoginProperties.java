package com.resolve.test.controller.util;

import java.io.InputStream;
import java.util.Properties;

import com.resolve.test.controller.util.Logger.LogLevels;

public class LoginProperties {
	
	private static Properties prop;
	private final static String PROPERTIES_FILE = "login.properties";

	/**
	 * Loads ~/login.properties file
	 * @throws Exception 
	 */
	public static void loadProperties() throws Exception {
		
	    prop = new Properties();
		InputStream property_input_stream = null;

		try {
			property_input_stream = LoginProperties.class.getClassLoader().getResourceAsStream(PROPERTIES_FILE);
			if (property_input_stream == null) {
				System.out.println("Cannot read properties file " + PROPERTIES_FILE);
				return;
			}

			prop.load(property_input_stream);
			LogLevels userLogLevel = LogLevels.getLevelFromString(prop.getProperty("logLevel"));
			if (userLogLevel != null)
				Logger.changeLogLevel(userLogLevel);
		} catch (Exception e) {
			e.printStackTrace();
			throw e;
		}
	}
	
	public static Properties getProperties() throws Exception {
		return prop != null ? (Properties)prop.clone() : null;
	}

	public static String get(String key) throws Exception {
		if(prop == null)
			loadProperties();
		return prop.getProperty(key);
	}

}
