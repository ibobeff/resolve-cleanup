package com.resolve.test.controller.util;

public class Logger {
	
	private static LogLevels levels = LogLevels.ERROR;
	
	private Logger(){
		
	}
	
	public enum LogLevels{
		TRACE(0),
		DEBUG(1),
		INFO(2),
		ERROR(3);
		
		private final int levelValue;
		
		LogLevels(final int levelValue){
			this.levelValue = levelValue;
		}
		
		int getLevelValue(){
			return levelValue;
		}
		
	    boolean isLessThanOrEqualTo(LogLevels levelValue){
	    	return this.getLevelValue() <= levelValue.getLevelValue();
	    }

		public static LogLevels getLevelFromString(String property){
			if(property.equals("DEBUG"))
				return LogLevels.DEBUG;
			else if(property.equals("INFO"))
				return LogLevels.INFO;
			else if(property.equals("ERROR"))
				return LogLevels.ERROR;
			else if(property.equals("TRACE"))
				return LogLevels.TRACE;
			return null;
		}
		
	}
	
	//Hide constructor
	private Logger(LogLevels logLevel){
		
	}
	
	public static void info(String message){
		if(levels.isLessThanOrEqualTo(LogLevels.INFO))
			System.out.println("INFO:" + message);
	}
	
	public static void debug(String message){
		if(levels.isLessThanOrEqualTo(LogLevels.DEBUG))
			System.out.println("DEBUG:" + message);
	}
	
	public static void trace(String message){
		if(levels.isLessThanOrEqualTo(LogLevels.TRACE))
			System.out.println("TRACE:" + message);
	}
	
	public static void error(String message){
		if(levels.isLessThanOrEqualTo(LogLevels.ERROR))
			System.out.println("ERROR:"  +message);
	}
	
	public static void changeLogLevel(LogLevels levels){
		Logger.levels = levels; 
	}
	
	public static LogLevels getLogLevel(){
		return levels;
	}
}
