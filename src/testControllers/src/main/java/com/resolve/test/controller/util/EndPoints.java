package com.resolve.test.controller.util;

public interface EndPoints {
    String IMPEX_LOG_LIST     = "/impexlog/getImpexLog";
    String IMPEX_LOG_REC      = "/impexlog/getImpexLogRec";
    String IMPEX_LOG_REC_DEL  = "/impexlog/deleteImpexLogRecs";
    String IMPEX_UPLOAD_FILE  = "/impex/uploadFile";
    String IMPEX_IMPORT       = "/impex/import";

    String TAG_LIST	= "/tag/listTags";
    String TAG_DELETE	= "/tag/deleteTags";
    String TAG_SAVE	 = "/tag/saveTag";
    
    String ACTIONTASK_GET = "/actiontask/get";
    String ACTIONTASK_SAVE = "/actiontask/save";
    String ACTIONTASK_DELETE = "/actiontask/delete";
    String ACTIONTASK_MOVE_RENAME = "/actiontask/actiontaskmanipulation";
}
