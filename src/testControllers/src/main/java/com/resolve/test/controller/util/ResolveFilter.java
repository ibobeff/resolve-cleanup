package com.resolve.test.controller.util;

import io.restassured.filter.Filter;
import io.restassured.filter.FilterContext;
import io.restassured.response.Response;
import io.restassured.specification.FilterableRequestSpecification;
import io.restassured.specification.FilterableResponseSpecification;
import java.util.HashMap;
import java.util.Map;

public class ResolveFilter implements Filter {

	private Map<String, String> cookies = new HashMap<String, String>();
	private String csrf = null;

    public Response filter(FilterableRequestSpecification requestSpec, FilterableResponseSpecification responseSpec, FilterContext ctx) {

        // add all previously stored cookies to subsequent requests
        // but only if they're not already in the request spec
        for (Map.Entry<String, String> cookie : cookies.entrySet()) {
            if (!requestSpec.getCookies().hasCookieWithName(cookie.getKey())) {
                requestSpec.cookie(cookie.getKey(), cookie.getValue());
            }
        }
        
        //All requests require a Referer but it doesn't matter what
        //the value is as long as it's from the same server
        if (!requestSpec.getHeaders().hasHeaderWithName("Referer")) {
        		requestSpec.header("Referer",requestSpec.getURI());
        }
        
        //Add csrf header if exists and isn't already added
        if (csrf != null && !requestSpec.getHeaders().hasHeaderWithName("OWASP_CSRFTOKEN")) {
        		requestSpec.header("OWASP_CSRFTOKEN",csrf);
        }
        
        //Add X-Requested-With header if it isn't already there
        if (!requestSpec.getHeaders().hasHeaderWithName("X-Requested-With")) {
    		requestSpec.header("X-Requested-With","XMLHttpRequest, XMLHttpRequest");
        }
        
//        //Add Content-Type header if it isn't already there
//        //Needed for POST, shouldn't bother GET
//        if (!requestSpec.getHeaders().hasHeaderWithName("Content-Type")) {
//    		requestSpec.header("Content-Type","application/json");
//        }
        //Above code doesn't work because it seems content type is set to a default
        //Looks like we'll just have to specify it in the post request unless we can figure
        //out a way to check here what kind of request we're making
        
        final Response response = ctx.next(requestSpec, responseSpec);

        cookies.putAll(response.getCookies());

        return response;
    }
    
    public void setCSRFHeader(String csrfToken)
    {
    		csrf = csrfToken;
    }

}
