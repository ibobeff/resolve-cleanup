package com.resolve.test.controller.util;

import static io.restassured.RestAssured.given;

import io.restassured.RestAssured;
import io.restassured.response.Response;

public abstract class GeneralTest {
    
    public static String baseUrl;
    public static ResolveFilter resolveCookie;
    public static DatabaseUtils db = DatabaseUtils.getInstance();

    public GeneralTest () {
        try
        {
            if (System.getProperty("resolveBaseUrl") != null) {
                baseUrl = System.getProperty("resolveBaseUrl");
            } else {
                baseUrl = LoginProperties.get("baseUrl");
            }
            RestAssured.baseURI = baseUrl + LoginProperties.get("basePath");
            RestAssured.enableLoggingOfRequestAndResponseIfValidationFails();
        }
        catch (Exception e)
        {
            Logger.debug(e.toString());
        }
    }
        
    public static ResolveFilter getResolveCookie() throws Exception {
        if (resolveCookie != null) {
            return resolveCookie;
        }
        else {
            loginUser();
            return resolveCookie;
        }
    }
    
    public static void loginUser() throws Exception {
        //Get initial csrf token
        RestAssured.baseURI = baseUrl;
        Response response1 = 
                given().
                    header("FETCH-CSRF-TOKEN", "1").
                    header("Content-Type","text/plain").
                when().post("resolve/JavaScriptServlet");
        Logger.debug(response1.asString());
        String csrf = response1.asString();
        csrf = csrf.substring(csrf.indexOf("OWASP_CSRFTOKEN") + "OWASP_CSRFTOKEN".length() + 1, csrf.length());
        Logger.debug("Initial csrf token: " + csrf);
        
        String jsessionid = response1.getCookie("JSESSIONID");
        Logger.debug("Initial session id: " + jsessionid);
        
        //Got the initial token needed for login
        //Now try to login with that token and the jsessionid
        
        RestAssured.baseURI = baseUrl + LoginProperties.get("basePath");
        resolveCookie = new ResolveFilter();
        Response response = 
            given().
            filter(resolveCookie).
            //contentType(ContentType.ANY).
            header("OWASP_CSRFTOKEN", csrf).
            header("Referer", RestAssured.baseURI + "client/login").
            cookie("JSESSIONID",jsessionid).
                formParam("username", LoginProperties.get("userName")).
                formParam("password", LoginProperties.get("password")).
                //formParam("hashState", "").
                //formParam("url", "/jsp/rsclient.jsp").
            when().post("client/login");
        
        //Logger.changeLogLevel(LogLevels.getLevelFromString(LoginProperties.get("logLevel")));
        Logger.debug(response.asString());
        Logger.debug(String.valueOf(response.getStatusCode()));
        Logger.debug("Cookies: " + response.getCookies().toString());

        //grab the new csrf token and add it to filter
        String csrfToken = response.body().jsonPath().get("data[1]");
        resolveCookie.setCSRFHeader(csrfToken);
    }
}
