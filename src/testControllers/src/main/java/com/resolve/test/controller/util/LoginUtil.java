package com.resolve.test.controller.util;

import static com.resolve.test.controller.util.ResolveCookie.resolveCookie;
import static io.restassured.RestAssured.given;

import io.restassured.RestAssured;
import io.restassured.response.Response;

public class LoginUtil {

	public static void loginUser() throws Exception {

		//Get initial csrf token
		
		RestAssured.baseURI = LoginProperties.get("baseUrl");
		Response response1 = 
				given().
					header("FETCH-CSRF-TOKEN", "1").
					header("Content-Type","text/plain").
				when().post("resolve/JavaScriptServlet");
		Logger.debug(response1.asString());
		String csrf = response1.asString();
		csrf = csrf.substring(csrf.indexOf("OWASP_CSRFTOKEN") + "OWASP_CSRFTOKEN".length() + 1, csrf.length());
		Logger.debug("Initial csrf token: " + csrf);
		
		String jsessionid = response1.getCookie("JSESSIONID");
		Logger.debug("Initial session id: " + jsessionid);
		
		//Got the initial token needed for login
		//Now try to login with that token and the jsessionid
		
		RestAssured.baseURI = LoginProperties.get("baseUrl") + LoginProperties.get("basePath");
		resolveCookie = new ResolveFilter();
		Response response = 
			given().
			filter(resolveCookie).
			//contentType(ContentType.ANY).
			header("OWASP_CSRFTOKEN", csrf).
			header("Referer", RestAssured.baseURI + "client/login").
			cookie("JSESSIONID",jsessionid).
				formParam("username", LoginProperties.get("userName")).
				formParam("password", LoginProperties.get("password")).
				//formParam("hashState", "").
				//formParam("url", "/jsp/rsclient.jsp").
			when().post("client/login");
		
		//Logger.changeLogLevel(LogLevels.getLevelFromString(LoginProperties.get("logLevel")));
		Logger.debug(response.asString());
		Logger.debug(String.valueOf(response.getStatusCode()));
		Logger.debug("Cookies: " + response.getCookies().toString());
		
		
		//grab the new csrf token and add it to filter
		String csrfToken = response.body().jsonPath().get("data[1]");
		resolveCookie.setCSRFHeader(csrfToken);
		
		//*********************//
		
		//Everything below here is the old login without csrf
		
//		RestAssured.baseURI = LoginProperties.get("baseUrl") + LoginProperties.get("basePath");
//		resolveCookie = new CookieFilter();
//		Response response = 
//			given().filter(resolveCookie).
//				formParam("username", LoginProperties.get("userName")).
//				formParam("password", LoginProperties.get("password")).
//				formParam("hashState", "").
//				formParam("url", "/jsp/rsclient.jsp").
//			when().post("login");
//		
//		//Logger.changeLogLevel(LogLevels.getLevelFromString(LoginProperties.get("logLevel")));
//		Logger.debug(response.asString());
//		Logger.debug(String.valueOf(response.getStatusCode()));
	}

}
