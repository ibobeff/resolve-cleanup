package com.resolve.test.controller.util;

import static com.resolve.test.controller.util.ResolveCookie.getResolveCookie;
import static io.restassured.RestAssured.given;

import com.resolve.test.controller.util.Logger;

import io.restassured.response.Response;

public class ControllerTestSteps {
	
	public static void testForStatusCode(String path, int statusCode) throws Exception {
		Response response =
			given().filter(getResolveCookie()).
			when().get(path).
			then().assertThat().statusCode(statusCode).extract().response();

		Logger.debug(response.asString());
	}
	
	public static void testForSuccessStatusCodeOnGet(String path) throws Exception {
		Response response =
			given().filter(getResolveCookie()).
			when().get(path).
			then().assertThat().statusCode(200).extract().response();

		Logger.debug(response.asString());
	}
	
	public static void testForSuccessStatusCodeOnPost(String path) throws Exception {
		Response response =
			given().filter(getResolveCookie()).
			when().post(path).
			then().assertThat().statusCode(200).extract().response();

		Logger.debug(response.asString());
	}
}
