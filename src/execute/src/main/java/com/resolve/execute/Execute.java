/******************************************************************************
* (C) Copyright 2014
*
* Resolve Systems, LLC
*
* All rights reserved
* This software is distributed under license and may not be used, copied,
* modified, or distributed without the express written permission of
* Resolve Systems, LLC.
*
******************************************************************************/

package com.resolve.execute;

import java.io.BufferedReader;
import java.io.File;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.concurrent.TimeUnit;

import com.resolve.rsbase.MainBase;
import com.resolve.thread.ScheduledExecutor;
import com.resolve.util.Log;
import com.resolve.util.StringUtils;

public class Execute
{
    String execdir = null;
    String[] execenv = null;
    boolean isAborted = false;
    
    public Execute(String execdir)
    {
        this.execdir = execdir;
    } // Execute

    public Execute(String execdir, String[] execenv)
    {
        this.execdir = execdir;
        this.execenv = execenv;
    } // Execute
    
    public void setEnv(String[] execenv)
    {
        this.execenv = execenv;
    } // setEnv
    
    @SuppressWarnings("unchecked")
    public ExecuteResult execute(String command, String input, boolean wait, int timeout, String jobname)
    {
        Log.log.debug("Inside Execute.execute(String command, String input, boolean wait, int timeout, String jobname)");
        
        ExecuteResult result = new ExecuteResult();
        
        Process process = null;
        BufferedReader br = null;
        PrintWriter in = null;
        
        try
        {
	        // check if execution directory is specified
	        if (StringUtils.isEmpty(command))
	        {
	            throw new ExecuteException("Execution command not defined");
	        }
	        
	        if (StringUtils.isEmpty(execdir))
	        {
	            throw new ExecuteException("Execution directory not defined");
	        }
            
            // start time
            long startTime = System.currentTimeMillis();
            
            // execute command
            ProcessBuilder pb = new ProcessBuilder(prepareCommand(command)); 
            
            pb.redirectErrorStream(true);
            pb.directory(new File(execdir));
            result.setCommand(command);
            
            // init enviroments
            if (execenv != null)
            {
	            Map env = pb.environment();
	            int i=0;
	            while (i < execenv.length)
	            {
	                env.put(execenv[i], execenv[i+1]);
	                i += 2;
	            }
            }
            
            // start process
            process = pb.start();
            if (StringUtils.isEmpty(jobname))
            {
                jobname = ""+process.hashCode();
            }
            jobname = jobname.toUpperCase();
            
            // set result process object
            result.setProcess(process);
            
            // set job process so that scheduled kill can abort the process
            ExecuteMain.setJobProcess(jobname, process);
            
            // schedule timeout kill task
            if (timeout > 0)
            {
            	Map params = new HashMap();
            	params.put("TYPE", "PROCESS");
            	params.put("JOBNAME", jobname);
            	params.put("EXECUTE", this);
            	
            	ScheduledExecutor.getInstance().executeDelayed(jobname, ExecuteMain.class, "kill", params, timeout, TimeUnit.SECONDS);
            }
            
            // wait for completion of spawned process
            if (wait) 
            {
                // initialize stream capture threads
                if (StringUtils.isEmpty(input))
                {
                    process.getOutputStream().close();
                }
                
                br = new BufferedReader(new InputStreamReader(process.getInputStream()));
                boolean blankLine = false;
                String line; 
                while ((line = br.readLine()) != null) 
                { 
                    if (line.length() > 0)
                    {
                        result.append(line+"\n");
                        blankLine = false;
                    }
                    else
                    {
                        if (blankLine)
                        {
                            result.append("\n");
                            blankLine = false;
                        }
                        else
                        {
                            blankLine = true;
                        }
                    }
                }  
                
                // send input
                if (!StringUtils.isEmpty(input))
                {
                    in = new PrintWriter(process.getOutputStream(), true);
                    in.print(input);
                    in.flush();
                    in.close();
                }
                
                // returncode
                result.setReturncode(process.waitFor());
            }
            else
            {
                result.setReturncode(0);
            }
            
            // stop time
            result.setDuration(System.currentTimeMillis() - startTime);
            if(Log.log.isTraceEnabled())
            {
                Log.log.trace("  Completed execute duration: "+result.getDuration());
            }
            
            // check if process was terminated by kill job
	        if (isAborted)
            {
	            if(Log.log.isTraceEnabled())
	            {
	                Log.log.trace("ActionTask was aborted jobname: "+jobname);
	            }
                result.setCompletion(false);
                result.setAborted(true);
            }
	        else
	        {
	            // Check return code. Some commands (like pinging a bad IP) should set completion to false.
	            if(result.getReturncode() == 0) {
	                result.setCompletion(true);
	            } else {
	                result.setCompletion(false);
	            }
	            //result.setCompletion(true);
	        }
        }
        catch (Throwable e)
        {
            Log.log.warn(e.getMessage(), e);
            
            result.setCompletion(false);
            result.setResult(e.getMessage());
        }
        finally
        {
            try
            {
	            if (process != null)
	            {
	                process.getInputStream().close();
	                process.getOutputStream().close();
	                process.getErrorStream().close();
	                process.destroy();
	            }
	            if (br != null)
	            {
	                br.close();
	            }
	            if (in != null)
	            {
	                in.close();
	            }
	                            
            }
            catch (Exception e)
            {
                Log.log.warn(e.getMessage());
            }
            
            ExecuteMain.removeJobProcess(jobname);
        }
        
        return result;
    } // execute
    
    private List prepareCommand(String command)
    {
		ArrayList result = new ArrayList();
		
        if(Log.log.isTraceEnabled())
        {
            Log.log.trace("Execute command: "+command);
        }
		
		StringBuffer currPart = new StringBuffer();
		boolean isEscaped = false;
		boolean insideQuote = false;
		char quoteChar = '\0';
		
		for (int x = 0; x < command.length(); x++) 
		{ 
		    char ch = command.charAt(x);
		    char ch2 = '\0';
		    if (x+1 < command.length())
		    {
		        ch2 = command.charAt(x+1);
		    }
		
		    // handle escaped \ - for the following characters " '
			if ((ch == '\\') && ((ch2 == '"')  || ch2 == '\''))
			{
			    isEscaped = true;
			}
			else if (isEscaped)
			{
			    isEscaped = false;
			    currPart.append(ch); 
			}
			
		    // match first quote
			else if (insideQuote == false && (ch == '\'' || ch == '"'))
			{ 
				quoteChar = ch;
	            insideQuote = true;
			} 
			
			// match end quote
			else if (insideQuote == true && ch == quoteChar)
			{
				quoteChar = '\0';
				insideQuote = false;
				
				// only add if not empty "" or " "
				if (!currPart.toString().trim().equals(""))
				{
				    result.add(currPart.toString()); 
				}
			    currPart = new StringBuffer(); 
			}
			else if (insideQuote == false && Character.isWhitespace(ch)) 
			{ 
				// only add if not empty "" or " "
				if (!currPart.toString().trim().equals(""))
				{
				    result.add(currPart.toString()); 
				}
			    currPart = new StringBuffer(); 
		    } 
			else 
			{ 
			    isEscaped = false;
			    currPart.append(ch); 
		    }
		
		    // get the last part of the command 
			if (x == command.length() - 1) 
			{
				// only add if not empty "" or " "
                if (!currPart.toString().trim().equals(""))
                {
			        result.add(currPart.toString()); 
                }
		    } 
		}

        Log.log.trace("preparedCommand:");
        int idx = 0;
        for (Iterator i=result.iterator(); i.hasNext(); )
        {
            Log.log.trace(idx+": "+i.next());
            idx++;
        }
		
		return result; 
	} // prepareCommand  
    
    public static String exec(String cmdline)
    {
        return exec(cmdline, 0);
    } // execute
    
    public static String exec(String cmdline, int timeout)
    {
        String result = "";
        
        Execute execute = new Execute(MainBase.main.configGeneral.getHome());
        ExecuteResult rc = execute.execute(cmdline, null, true, timeout, null);
        if (rc != null)
        {
	        result = rc.getResultString();
        }
        
        return result;
    } // execute
    
    public static ExecuteResult run(String cmdline)
    {
        return run(cmdline, 0);
    } // run
    
    public static ExecuteResult run(String cmdline, int timeout)
    {
        Execute execute = new Execute(MainBase.main.configGeneral.getHome());
        return execute.execute(cmdline, null, true, timeout, null);
    } // run

    public boolean isAborted()
    {
        return isAborted;
    }

    public void setAborted(boolean isAborted)
    {
        this.isAborted = isAborted;
    }
    
} // Execute
