/******************************************************************************
* (C) Copyright 2014
*
* Resolve Systems, LLC
*
* All rights reserved
* This software is distributed under license and may not be used, copied,
* modified, or distributed without the express written permission of
* Resolve Systems, LLC.
*
******************************************************************************/

package com.resolve.execute;

import groovy.lang.Binding;

import java.util.List;
import java.util.Map;

import com.resolve.rsbase.MainBase;
import com.resolve.groovy.ThreadGroovy;
import com.resolve.util.Constants;
import com.resolve.util.EncryptionTransportObject;
import com.resolve.util.Log;
import com.resolve.util.StringUtils;

public class ExecuteInternal
{
    public ExecuteInternal()
    {
    } // ExecuteInternal

    public ExecuteResult execute(String script, String args, Map inputs, Map params, Map flows, int timeout, String jobname)
    {
        ExecuteResult result = new ExecuteResult();

        long starttime = System.currentTimeMillis();

        // Decrypt map to allow user to use it natively
        List preInputs = EncryptionTransportObject.decryptMap(inputs);
        List preParams = EncryptionTransportObject.decryptMap(params);
        List preFlows = EncryptionTransportObject.decryptMap(flows);

        try
        {
            if (!StringUtils.isEmpty(script))
            {
                // binding
                Binding binding = new Binding();
                binding.setVariable(Constants.GROOVY_BINDING_ARGS, args);
                binding.setVariable(Constants.GROOVY_BINDING_LOG, Log.log);
                binding.setVariable(Constants.GROOVY_BINDING_RESULT, result);
                binding.setVariable(Constants.GROOVY_BINDING_MAIN, MainBase.main);

                binding.setVariable(Constants.GROOVY_BINDING_INPUTS, inputs);
                binding.setVariable(Constants.GROOVY_BINDING_PARAMS, params);
                binding.setVariable(Constants.GROOVY_BINDING_FLOWS, flows);

                // args
                Object[] runArgs = {};

                // thread groovy script
                ThreadGroovy groovyScript = new ThreadGroovy(script, jobname, true, binding, runArgs);
                Thread thread = new Thread(groovyScript);

                /*
                 * ExecuteMain.setJobThread(jobname, thread); if (timeout > 0) {
                 * Map killParams = new HashMap(); killParams.put("TYPE",
                 * "THREAD"); killParams.put("JOBNAME", jobname.toUpperCase());
                 * Executor.executeDelayed(ExecuteMain.class, "kill",
                 * killParams, timeout, TimeUnit.SECONDS); }
                 */

                // start
                thread.start();

                // wait for thread to complete
                thread.join(timeout * 1000);

                Thread.State state = thread.getState();
                if (state != Thread.State.TERMINATED)
                {
                    throw new InterruptedException("ERROR: Script execution timed out");
                }

                Object rc = groovyScript.getResult();

                // set results
                result.setReturncode(0);
                result.setCompletion(true);
                if (rc != null)
                {
                    result.setResult(rc);
                }

                if (rc instanceof Map)
                {
                    String status = (String) ((Map) rc).get("STATUS");
                    if (status != null && status.equalsIgnoreCase("FALSE"))
                    {
                        result.setReturncode(1);
                    }
                }
            }
            else
            {
                Log.log.warn("FAILED: Missing content for execution");
                result.setResult("FAILED: Missing content for execution");
                result.setCompletion(false);
                result.setReturncode(-1);
            }
        }
        catch (Exception e)
        {
            Log.log.warn(e.getMessage(), e);

            result.setResult(e.getMessage());
            result.setCompletion(false);
            result.setReturncode(-1);
        }
        finally
        {
            // Restore Encryption objects
            EncryptionTransportObject.restoreMap(inputs, preInputs);
            EncryptionTransportObject.restoreMap(params, preParams);
            EncryptionTransportObject.restoreMap(flows, preFlows);
        }

        // set duration
        result.setDuration(System.currentTimeMillis() - starttime);

        return result;
    } // execute

} // ExecuteInternal
