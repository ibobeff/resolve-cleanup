/******************************************************************************
* (C) Copyright 2014
*
* Resolve Systems, LLC
*
* All rights reserved
* This software is distributed under license and may not be used, copied,
* modified, or distributed without the express written permission of
* Resolve Systems, LLC.
*
******************************************************************************/

package com.resolve.execute;

public class ExecuteJava extends Execute
{
    String javahome;
    String classpath;
    String javaoptions;
    
    public ExecuteJava(String execdir)
    {
        super(execdir);
        
        this.javahome       = null;
        this.classpath      = null;
        this.javaoptions    = "";
    } // Execute
    
    public ExecuteJava(String execdir, String javahome, String classpath)
    {
        super(execdir);
        
        this.javahome       = javahome;
        this.classpath      = classpath;
        this.javaoptions    = "";
    } // Execute
    
    public ExecuteJava(String dir, String home, String classpath, String javaoptions)
    {
        super(dir);
        
        this.javahome       = home;
        this.classpath      = classpath;
        this.javaoptions    = javaoptions;
    } // Execute
    
    public ExecuteResult start(String cmdline, String input)
    {
        return execute(cmdline, input, true, 0, null);
    } // start
    
    public ExecuteResult execute(String className, String args, String input, boolean wait, int timeout, String jobname)
    {
        if (javahome == null)
        {
            javahome = System.getProperty("java.home");
        }
        if (classpath == null)
        {
            classpath = System.getProperty("java.class.path")+";";
        }
        if (javaoptions == null)
        {
            javaoptions = "";
        }
        
        // init cmdline
        String cmdline = "\""+javahome+"/bin/java"+"\" "+"-cp "+"\""+this.classpath+"\""+" "+javaoptions+" "+"\""+className+"\" "+args;
        
        // execute command and enviornment
        return super.execute(cmdline, input, wait, timeout, jobname);
    } // execute 

} // ExecuteJava
