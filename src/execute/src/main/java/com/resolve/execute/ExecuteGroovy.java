/******************************************************************************
* (C) Copyright 2014
*
* Resolve Systems, LLC
*
* All rights reserved
* This software is distributed under license and may not be used, copied,
* modified, or distributed without the express written permission of
* Resolve Systems, LLC.
*
******************************************************************************/

package com.resolve.execute;

public class ExecuteGroovy extends ExecuteJava
{
    public ExecuteGroovy(String execdir, String javahome, String classpath)
    {
        super(execdir, javahome, classpath);
    } // ExecuteGroovy
    
    public ExecuteGroovy(String execdir, String javahome, String classpath, String javaoptions)
    {
        super(execdir, javahome, classpath, javaoptions);
    } // ExecuteGroovy
    
    public ExecuteResult execute(String filename, String args, String input, boolean wait, int timeout, String jobname)
    {
        //String groovycmd = "groovy.lang.GroovyShell \""+filename+"\" "+args;
        args = "\""+filename+"\" "+args;
        
        return super.execute("groovy.lang.GroovyShell", args, input, wait, timeout, jobname);
    } // execute 

} // ExecuteGroovy
