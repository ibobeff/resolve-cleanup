/******************************************************************************
* (C) Copyright 2014
*
* Resolve Systems, LLC
*
* All rights reserved
* This software is distributed under license and may not be used, copied,
* modified, or distributed without the express written permission of
* Resolve Systems, LLC.
*
******************************************************************************/

package com.resolve.execute;

import java.io.InputStream;
import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.io.IOException;
import java.lang.StringBuilder;

import com.resolve.util.Log;

public class ExecuteStreamCapture extends java.lang.Thread
{
    InputStream instream;
    
    boolean isClosed = false;
    StringBuilder output = new StringBuilder();
    BufferedReader br;
    
    public ExecuteStreamCapture(InputStream instream)
    {
        this.instream = instream;
        this.br = new BufferedReader(new InputStreamReader(instream));
        
        this.setDaemon(true);
    } // StreamGobbler
    
    public void run()
    {
        try
        {
            boolean blankLine = false;
            String line = null;
            while ( ((line = br.readLine()) != null) && (isClosed == false))
            {
                if (line.length() > 0)
                {
	                output.append(line+"\n");
                    blankLine = false;
                }
                else
                {
                    if (blankLine)
                    {
		                output.append("\n");
                        blankLine = false;
                    }
                    else
                    {
	                    blankLine = true;
                    }
                }
            }
        }
        catch (IOException e)
        {
            Log.log.error("Failed to capture output stream: "+e.getMessage() ,e);
        };
    } // run
    
    public void close()
    {
        isClosed = true;
        
        boolean blankLine = false;
        
        try
        {
	        String line = br.readLine();
	        if (line != null)
	        {
		        if (line.length() > 0)
		        {
		            output.append(line+"\n");
		            blankLine = false;
		        }
		        else
		        {
		            if (blankLine)
		            {
		                output.append("\n");
		                blankLine = false;
		            }
		            else
		            {
		                blankLine = true;
		            }
		        }
	        }
        }
        catch (IOException e)
        {
            Log.log.error("Failed to close capture output stream: "+e.getMessage() ,e);
        };
    } // close

    public boolean isClosed()
    {
        return isClosed;
    }

    public StringBuilder getOutput()
    {
        return output;
    }

} // ExecuteStreamCapture

