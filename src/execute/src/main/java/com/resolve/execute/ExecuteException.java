/******************************************************************************
* (C) Copyright 2014
*
* Resolve Systems, LLC
*
* All rights reserved
* This software is distributed under license and may not be used, copied,
* modified, or distributed without the express written permission of
* Resolve Systems, LLC.
*
******************************************************************************/

package com.resolve.execute;

public class ExecuteException extends Exception
{
    private static final long serialVersionUID = 4384899770138184908L;

	public ExecuteException(String msg)
    {
        super(msg);
    } // ExecuteException
    
    public ExecuteException(Exception e)
    {
        super(e);
    } // ExecuteException
    
} // ExecuteException
