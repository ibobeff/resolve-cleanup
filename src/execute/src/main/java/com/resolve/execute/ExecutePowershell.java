/******************************************************************************
* (C) Copyright 2014
*
* Resolve Systems, LLC
*
* All rights reserved
* This software is distributed under license and may not be used, copied,
* modified, or distributed without the express written permission of
* Resolve Systems, LLC.
*
******************************************************************************/

package com.resolve.execute;

import java.util.regex.Pattern;
import java.util.regex.Matcher;

public class ExecutePowershell extends Execute
{
    static Pattern infilePattern = Pattern.compile("\\\\infile_Thread_\\d+\\.in\\(");
    String options;
    
    public ExecutePowershell(String execdir)
    {
        super(execdir);
        
        this.options    = "";
    } // ExecutePowershell
    
    public ExecutePowershell(String execdir, String options)
    {
        super(execdir);
        
        this.options = options;
    } // ExecutePowershell
    
    public ExecuteResult start(String cmdline, String input)
    {
        return execute(cmdline, input, true, 0, null);
    } // start
    
    public ExecuteResult execute(String cmdline, String input, boolean wait, int timeout, String jobname)
    {
        ExecuteResult result = null;
        
        cmdline = "powershell"+" -NoLogo -ExecutionPolicy Unrestricted -File "+options+" "+cmdline;
        
        // execute command and environment
        result = super.execute(cmdline, input, wait, timeout, jobname);
        
        // check for infile error msg
        Matcher matcher = infilePattern.matcher(result.toString());
        if (matcher.find())
        {
            result.setReturncode(1);
        }
        
        // check for vbscript runtime error
        else if (result.toString().indexOf("Powershell runtime error") > 0)
        {
            result.setReturncode(1);
        }
             
        return result;
    } // execute 

} // ExecutePowershell
