/******************************************************************************
* (C) Copyright 2014
*
* Resolve Systems, LLC
*
* All rights reserved
* This software is distributed under license and may not be used, copied,
* modified, or distributed without the express written permission of
* Resolve Systems, LLC.
*
******************************************************************************/

package com.resolve.execute;

import java.lang.Process;
import java.lang.StringBuilder;

public class ExecuteResult
{
    String command;
    boolean completion;
    int returncode;
    long duration;
    Process process;
    StringBuffer output;
    StringBuffer debug;
    
    // option object result
    Object resultObj = null;
    boolean isObject = false;
    boolean isAborted = false;
    
    public ExecuteResult()
    {
        command = "";
        completion = false;
        returncode = -1;
        output = new StringBuffer();
        debug = null;
    } // ExecuteResult
    
    public ExecuteResult(boolean completion)
    {
        this.command = "";
        this.completion = completion;
        if (completion == true)
        {
            returncode = 0;
        }
        else
        {
            returncode = -1;
        }
        output = new StringBuffer();
        debug = null;
    } // ExecuteResult
    
    public ExecuteResult(boolean completion, String resultStr)
    {
        this(completion);
        this.setResult(resultStr);
    } // ExecuteResult
    
    public String toString()
    {
        String result = null;
        
        if (isObject)
        {
            result = resultObj.toString();
        }
        else
        {
            result = output.toString();
        }
        return result;
    } // toString
    
    public boolean isObject()
    {
        return isObject;
    } // isObject
    
    public Object getResult()
    {
        Object result = null;
        
        if (isObject)
        {
            result = resultObj;
        }
        else
        {
            result = output.toString();
        }
        return result;
    } // getResult
    
    public String getResultString()
    {
        return output.toString();
    } // getResultString
    
    public Object getResultObject()
    {
        return resultObj;
    } // getResultObject
    
    public void setResult(Object obj)
    {
        if (obj != null)
        {
            isObject = true;
	        resultObj = obj;
        }
    } // setResult
    
    public void setResult(String str)
    {
        if (str == null)
        {
            str = "";
        }
        output = new StringBuffer(str);
    } // setResult
    
    public void append(Object obj)
    {
        if (obj != null)
        {
	        output.append(obj);
        }
    } // append
    
    public void print(String str)
    {
        output.append(str);
    } // print
    
    public void println(String str)
    {
        output.append(str+"\n");
    } // println
    
    public String getCommand()
    {
        return command;
    }
    public void setCommand(String command)
    {
        this.command = command;
    }
    public Process getProcess()
    {
        return process;
    }
    public void setProcess(Process process)
    {
        this.process = process;
    }
    public int getReturncode()
    {
        return returncode;
    }
    public void setReturncode(int returncode)
    {
        this.returncode = returncode;
    }
    public long getDuration()
    {
        return duration;
    }
    public void setDuration(long duration)
    {
        this.duration = duration;
    }
    public boolean isCompletion()
    {
        return completion;
    }
    public void setCompletion(boolean completion)
    {
        this.completion = completion;
    }
    public StringBuffer getDebug()
    {
        return debug;
    }
    public void setDebug(StringBuffer debug)
    {
        this.debug = debug;
    }
    public boolean isAborted()
    {
        return isAborted;
    }
    public void setAborted(boolean isAborted)
    {
        this.isAborted = isAborted;
    }

} // ExecuteResult
