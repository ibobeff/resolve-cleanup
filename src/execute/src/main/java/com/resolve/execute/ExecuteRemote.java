/******************************************************************************
* (C) Copyright 2014
*
* Resolve Systems, LLC
*
* All rights reserved
* This software is distributed under license and may not be used, copied,
* modified, or distributed without the express written permission of
* Resolve Systems, LLC.
*
******************************************************************************/

package com.resolve.execute;

import groovy.lang.Binding;
import groovy.lang.GString;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.resolve.rsbase.MainBase;
import com.resolve.groovy.ThreadGroovy;
import com.resolve.rsbase.SessionBinding;
import com.resolve.rsbase.SessionMap;
import com.resolve.rsbase.SessionPool;
import com.resolve.util.Constants;
import com.resolve.util.EncryptionTransportObject;
import com.resolve.util.Log;
import com.resolve.util.ScriptDebug;
import com.resolve.util.StringUtils;

public class ExecuteRemote
{
    public static SessionPool sessionPool = new SessionPool();

    public ExecuteRemote()
    {
    } // ExecuteRemote

    public ExecuteResult execute(String processid, String script, String scriptName, boolean contentCacheable, String args, Map inputs, Map params, Map flows, ScriptDebug debug, String reference, int timeout, int processTimeout, String jobname, String problemid, String wiki, boolean destroy)
    {
        ExecuteResult result = new ExecuteResult();
        long starttime = System.currentTimeMillis();
        Thread thread = null;
        // Decrypt map to allow user to use it natively
        List preInputs = EncryptionTransportObject.decryptMap(inputs);
        List preParams = EncryptionTransportObject.decryptMap(params);
        List preFlows = EncryptionTransportObject.decryptMap(flows);
        try
        {
            if (!StringUtils.isEmpty(script))
            {
                SessionMap sessionMap = null;

                // init scriptdebug
                debug.setComponent("RSRemote:" + jobname);
                debug.setMap(inputs, new HashMap(), params, flows);

                // binding
                Binding binding = new Binding();
                binding.setVariable(Constants.GROOVY_BINDING_ARGS, args);
                binding.setVariable(Constants.GROOVY_BINDING_RESULT, result);
                binding.setVariable(Constants.GROOVY_BINDING_SYSLOG, Log.syslog);
                binding.setVariable(Constants.GROOVY_BINDING_MAIN, MainBase.main);
                binding.setVariable(Constants.GROOVY_BINDING_LOG, Log.log);
                binding.setVariable(Constants.GROOVY_BINDING_REFERENCE, reference);
                binding.setVariable(Constants.GROOVY_BINDING_PARAMS, params);
                binding.setVariable(Constants.GROOVY_BINDING_FLOWS, flows);
                binding.setVariable(Constants.GROOVY_BINDING_INPUTS, inputs);
                binding.setVariable(Constants.GROOVY_BINDING_PROCESSID, processid);
                binding.setVariable(Constants.GROOVY_BINDING_PROBLEMID, problemid);
                binding.setVariable(Constants.GROOVY_BINDING_WIKI, wiki);
                binding.setVariable(Constants.GROOVY_BINDING_DEBUG, debug);
                binding.setVariable(Constants.GROOVY_BINDING_ACTIONNAME, scriptName);
                binding.setVariable(Constants.GROOVY_BINDING_ESB, MainBase.esb);
                
                String userId = params.get(Constants.EXECUTE_USERID) != null ? params.get(Constants.EXECUTE_USERID).toString() : "";
                binding.setVariable(Constants.GROOVY_BINDING_USERID, userId);

                // add OLD_PARAMS if exists
                if (params != null && params.containsKey(Constants.GROOVY_BINDING_OLD_PARAMS))
                {
                    binding.setVariable(Constants.GROOVY_BINDING_OLD_PARAMS, params.get(Constants.GROOVY_BINDING_OLD_PARAMS));
                }

                // init session map for the processid
                if (!StringUtils.isEmpty(processid))
                {
                    sessionMap = sessionPool.getSessionMap(processid, processTimeout);

                    // init SessionBinding with current flow
                    SessionBinding sessions = new SessionBinding(sessionMap, flows);

                    // set bindings
                    binding.setVariable(Constants.GROOVY_BINDING_SESSIONS, sessions);
                    Log.log.trace("SessionPool SessionMaps: " + sessionPool);
                    Log.log.trace("  SessionMap sessions: " + sessionMap);
                }

                // args
                Object[] runArgs = {};

                // execute script
                ThreadGroovy groovyScript = new ThreadGroovy(script, scriptName, contentCacheable, binding, runArgs);
                groovyScript.setSessionMap(sessionMap);

                thread = new Thread(groovyScript);

                // start thread
                thread.start();

                // set job thread so that abort request can interrupt the thread
                ExecuteMain.setJobThread(jobname, thread);

                // wait for thread to complete
                thread.join(timeout * 1000);

                Thread.State state = thread.getState();
                if (state != Thread.State.TERMINATED)
                {
                    throw new InterruptedException("ERROR: Script execution timed out");
                }

                Object rc = groovyScript.getResult();
                if (rc != null)
                {
                    if (rc instanceof String)
                    {
                        result.setResult(rc.toString());
                    }
                    else if (rc instanceof GString)
                    {
                        result.setResult(rc.toString());
                    }
                    else
                    {
                        result.setResult(rc);
                    }
                }
                else
                {
                    result.setResult("");
                }

                // set results
                result.setCompletion(true);
                if (groovyScript.isException())
                {
                    result.setReturncode(Constants.ASSESS_RETURNCODE_EXECUTE_EXCEPTION);
                    inputs.put(Constants.INPUTS_RSEXCEPTION, result.getResult());
                }
                else
                {
                    result.setReturncode(Constants.ASSESS_RETURNCODE_EXECUTE_SUCCESS);
                }

                Log.log.trace("After SessionMap sessions: " + sessionMap);
            }
            else
            {
                Log.log.warn("FAILED: Missing content for execution");
                debug.println("ExecuteRemote", "FAILED: Missing content for execution");

                result.setResult("FAILED: Missing content for execution");
                result.setCompletion(false);
                result.setReturncode(Constants.ASSESS_RETURNCODE_EXECUTE_FAIL);
            }
        }
        catch (InterruptedException ie)
        {
            String msg = "Task timed out: processid=" + processid + ", jobname=" + jobname;
            Log.log.warn(msg);
            debug.println("ExecuteRemote", msg);

            if (thread != null)
            {
                try
                {
                    if (destroy)
                    {
                        thread.stop();
                    }
                    else
                    {
                        thread.interrupt();
                    }
                }
                catch (Throwable t)
                {
                    Log.log.warn(t.getMessage());
                }
            }

            // NOTE completion=false as the task was aborted prematurely
            result.setResult(msg);
            result.setCompletion(false);
            result.setAborted(true);
            result.setReturncode(Constants.ASSESS_RETURNCODE_EXECUTE_TIMEOUT);
        }
        catch (Exception e)
        {
            Log.log.warn(e.getMessage());
            Log.log.debug(e.getMessage(), e);
            debug.println(e.getMessage(), e);

            result.setResult(e.getMessage());
            result.setCompletion(false);
            result.setReturncode(Constants.ASSESS_RETURNCODE_EXECUTE_FAIL);
        }
        finally
        {
            ExecuteMain.removeJobThread(jobname);
            // Restore Encryption objects
            EncryptionTransportObject.restoreMap(inputs, preInputs);
            EncryptionTransportObject.restoreMap(params, preParams);
            EncryptionTransportObject.restoreMap(flows, preFlows);
        }

        // set duration
        result.setDuration(System.currentTimeMillis() - starttime);

        return result;
    } // execute

    public static void removeSessionMap(String processid)
    {
        sessionPool.removeSessionMap(processid);
    } // removeSessionMap

} // ExecuteRemote
