/******************************************************************************
* (C) Copyright 2014
*
* Resolve Systems, LLC
*
* All rights reserved
* This software is distributed under license and may not be used, copied,
* modified, or distributed without the express written permission of
* Resolve Systems, LLC.
*
******************************************************************************/

package com.resolve.execute;

import java.util.Iterator;
import java.util.Vector;

import com.resolve.util.StringUtils;

public class ExecuteAnt extends ExecuteJava
{
    public ExecuteAnt(String execdir, String javahome, String classpath)
    {
        super(execdir, javahome, classpath);
    } // ExecuteAnt
    
    public ExecuteAnt(String execdir, String javahome, String classpath, String javaoptions)
    {
        super(execdir, javahome, classpath, javaoptions);
    } // ExecuteAnt
     
    public ExecuteResult execute(String filename, String target)
    {
        return this.execute(filename, target, null, null, true, 0, null);
    } // execute
    
    public ExecuteResult execute(String filename, String target, String env, String input, boolean wait, int timeout, String jobname)
    {
        // init java options
        javaoptions += getJavaOptions();
        
        // init java command
        String antfile    = getAntFilename(filename);
        String antenv     = getAntEnv(env);
        String antoptions = getAntOptions(antfile, antenv);
        String anttarget  = getAntTarget(target);
        
        // execute ant
        String args = antoptions + anttarget;
        return super.execute("org.apache.tools.ant.launch.Launcher", args, input, wait, timeout, jobname);
    } // execute 
    
    String getAntFilename(String filename)
    {
        String result;
        
        if ((filename.length() > 2) && (filename.charAt(0) == '/' 
            || filename.charAt(0) == '\\'
            || filename.charAt(1) == ':'))
        {
            result = "\""+filename+"\"";
        }
        else
        {
            result = "\""+execdir+"/"+filename+"\"";
        }
        
        return result;
    } // getAntFilename
    
    String getAntTarget(String target)
    {
        String result;
        
        // check for null target
        if (StringUtils.isEmpty(target))
        {
            result = "MAIN";
        }
        else
        {
            result = target;
        }
        
        return result;
    } // getAntTarget
    
    String getAntEnv(String env)
    {
        // home directory
        String result = " -Dresolve.home="+"\""+execdir+"\"";
        
        // check for null env
        if (!StringUtils.isEmpty(env))
        {
            result += " " + env + " ";
        }
        
        return result;
    } // getAntEnv
    
        
    String getAntOptions(String antfile, String antenv)
    {
        return "-f " + antfile + " " + antenv + " ";
    } // getAntOptions
    
    String getJavaOptions()
    {
        return " " + "-Dant.home=" +"\""+execdir+"\"";
    } // getJavaOptions
    
    static String getAntLibClasspath(String libpath, Vector libs)
    {
        String result = "";
        
        // java and ant libraries
        if (libs != null)
        {
            for (Iterator i=libs.iterator(); i.hasNext(); )
            {
                result += libpath + "/" + (String)i.next() + ";";
            }
        }
        
        return result;
    } // getAntLibClasspath

    static String getAntLibClasspath(String libpath, String[] libs)
    {
        String result = "";
        
        if (libs != null)
        {
            for (int i=0; i < libs.length; i++)
            {
                String lib = libs[i];
                result += libpath + "/" + lib + ";";
            }
        }
        
        return result;
    } // getAntClasspath
    
} // ExecuteAnt
