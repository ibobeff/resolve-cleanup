/******************************************************************************
* (C) Copyright 2014
*
* Resolve Systems, LLC
*
* All rights reserved
* This software is distributed under license and may not be used, copied,
* modified, or distributed without the express written permission of
* Resolve Systems, LLC.
*
******************************************************************************/

package com.resolve.execute;

import java.io.File;
import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.apache.commons.io.FileUtils;

import com.resolve.esb.MMsgContent;
import com.resolve.esb.MMsgHeader;
import com.resolve.esb.MMsgOptions;
import com.resolve.rsbase.MainBase;
import com.resolve.thread.ScheduledExecutor;
import com.resolve.util.Constants;
import com.resolve.util.Log;
import com.resolve.util.ScriptDebug;
import com.resolve.util.StringUtils;

/**
 * This is the shared ExecuteMain. Use the ExecuteMain in the RSRemote for external executor extensions.
 * 
 * @author duke.tantiprasut
 *
 */

public class ExecuteMain
{
    final static Pattern VAR_REGEX_CONFIG = Pattern.compile(Constants.VAR_REGEX_CONFIG);
    final static Pattern VAR_REGEX_TARGET = Pattern.compile(Constants.VAR_REGEX_TARGET);
    
    public static ConcurrentHashMap jobKilled = new ConcurrentHashMap();
    public static ConcurrentHashMap jobProcess = new ConcurrentHashMap();
    public static ConcurrentHashMap jobThread = new ConcurrentHashMap();
    
    Thread          thread;
    Map             data;
    String          inputfile;
    String          tmpfile;
    boolean         hasInputFile;
    boolean         hasTmpFile;
    boolean         hasResultFile;
    long            startTime;
    
	// called from Executor
    public ExecuteMain()
    {
    } // ExecuteMain
    
    public ExecuteMain(Map params)
    {
        this.data = params;
    } // ExecuteMain
    
    public void setThread(Thread thread)
    {
        this.thread = thread;
    } // setThread
    
    public Thread getThread()
    {
        return this.thread;
    } // getThread
    
    public void execute()
    {
        execute(true);
    } // execute

    public void execute(Map params)
    {
    	this.data = params;
    	
    	this.execute(true);
    } // execute
    
    public void execute(boolean returnResult)
    {
        if (data != null)
        {
            ExecuteResult result = null;
            hasInputFile = false;
            hasTmpFile = false;

            String type         = getParam("TYPE");
            String jobname      = getParam("JOBNAME");
            String filename     = getParam("FILENAME");
            String processid    = getParam(Constants.EXECUTE_PROCESSID);
                
            // init script debug
            boolean isDebug = false;
            String debugStr = getParam(Constants.EXECUTE_DEBUG);
            if (StringUtils.isNotEmpty(debugStr) && debugStr.equalsIgnoreCase("true"))
            {
                isDebug = true;
            }
            ScriptDebug debug = new ScriptDebug(isDebug, null);
            debug.setComponent("ExecuteMain:"+jobname);
            
            try
            {
		        // init thread processid
		        Log.setProcessId(processid);
		        
                Log.log.debug("ExecuteMain");
	            Log.log.debug("  type: "+type+" jobname: "+jobname);                
	            //Log.log.debug("  data: "+data);
                
                // init startTime
                //startTime = System.currentTimeMillis();
                
                // ExecuteOS, ExecuteCMD
                if (type.equalsIgnoreCase(Constants.ACTION_INVOCATION_TYPE_OS) 
                    || type.equalsIgnoreCase(Constants.ACTION_INVOCATION_TYPE_BASH)
                    || type.equalsIgnoreCase(Constants.ACTION_INVOCATION_TYPE_CMD))
                {
                    result = executeOS();
                }

                // ExecuteGroovy
                else if (type.equalsIgnoreCase(Constants.ACTION_INVOCATION_TYPE_GROOVY))
                {
                    result = executeGroovy();
                }

                // ExecuteRemote
                else if (type.equalsIgnoreCase(Constants.ACTION_INVOCATION_TYPE_INTERNAL)
                    || type.equalsIgnoreCase(Constants.ACTION_INVOCATION_TYPE_REMOTE))
                {
                    result = executeInternal(debug);
                }

                else
                {
                    throw new Exception("Invalid ActionInvocation type: "+type);
                }
                
                if (result != null)
                {
                    Log.log.debug("Results:\n"+result);
                    debug.println("Results:\n"+result);
                }

                // return result to RSSERVER
                if (result != null && returnResult)
                {
                    Object resultValue;
                    if (result.isObject())
                    {
                        resultValue = result.getResultObject();
                    }
                    else
                    {
                        resultValue = result.getResultString();
                    }
                    
                    /*
                     * ResultValue Post-process
                     */ 
                    
                    // check if process was terminated by kill job
			        if (result.isAborted())
                    {
                        Log.log.debug("ActionTask was aborted jobname: "+jobname);
                        debug.println("ActionTask was aborted jobname: "+jobname);
                        resultValue = "ActionTask aborted by user or timeout. Available results:\n\n"+resultValue;
                    }
                    
                    // set debug
			        if (debug.isDebug())
			        {
				        result.setDebug(debug.getBuffer());
			        }
        
                    
                    Log.log.info("Sending results");
                    Log.log.debug("  jobname: "+jobname);
                    Log.log.debug("  command: "+result.getCommand());
                    Log.log.debug("  completion: "+result.isCompletion());
                    Log.log.debug("  returncode: "+result.getReturncode());
                    Log.log.debug("  duration: "+result.getDuration());
                    
                    debug.println("Sending results");
                    debug.println("  jobname: "+jobname);
                    debug.println("  command: "+result.getCommand());
                    debug.println("  completion: "+result.isCompletion());
                    debug.println("  returncode: "+result.getReturncode());
                    debug.println("  duration: "+result.getDuration());
                    
                    sendResult(result, resultValue);
                }
            }
            catch (Throwable e)
            {
                Log.log.error("Failed to execute actiontask: "+e.getMessage(), e);
                debug.println("ExecuteMain", "Failed to execute actiontask: "+e.getMessage(), e);
                
                // init result
                if (result == null)
                {
                    result = new ExecuteResult();
                    result.setCompletion(false);
                    result.setReturncode(Constants.ASSESS_RETURNCODE_EXECUTE_EXCEPTION);
                    result.setDuration(0);
                }
                
                // set debug
		        if (debug.isDebug())
		        {
			        result.setDebug(debug.getBuffer());
		        }
        
                sendResult(result, "Failed to execute actiontask: "+e.getMessage()+StringUtils.getStackTrace(e));
            }
            finally
            {
	            Log.clearProcessId();
            }
        }
        
    } // execute
    
    public void sendResult(ExecuteResult result, Object content)
    {
        try
        {
            Log.log.debug("Send Result: Execute Main");
            
            // stop time
            //long duration = System.currentTimeMillis() - startTime;
            
            // send results to rsserver
            String destaddr = getParam("RESULTADDR");
            if (StringUtils.isEmpty(destaddr))
            {
            	destaddr = Constants.ESB_NAME_RSSERVER;
            }
            
            String address = getParam(Constants.EXECUTE_ADDRESS);

            // update target if not defined
            String target = getParam(Constants.EXECUTE_TARGET);
            if (StringUtils.isEmpty(target))
            {
                target = MainBase.main.configId.guid;
            }

            // normalize format - remove carriage return (\r)
            if (result.isObject() == false)
            {
	            content = ((String)content).replaceAll("\r", "");
            }
            
            // set destination method
            String classMethod = getParam(Constants.ESB_PARAM_CLASSMETHOD);
            if (StringUtils.isEmpty(classMethod))
            {
                classMethod = "MResult.actionResult";
            }
            
            // create header
            MMsgHeader header = new MMsgHeader();
            if (StringUtils.isEmpty(destaddr))
            {
	            header.setRouteDest(Constants.ESB_NAME_RSSERVER, classMethod);
            }
            else
            {
	            header.setRouteDest(destaddr, classMethod);
            }
            
            // options
            MMsgOptions options = new MMsgOptions();
            options.setAddress(address);
            options.setTarget(target);
            options.setAffinity(getParam(Constants.EXECUTE_AFFINITY));
            options.setActionName(getParam(Constants.EXECUTE_ACTIONNAME));
            options.setActionNamespace(getParam(Constants.EXECUTE_ACTIONNAMESPACE));
            options.setActionSummary(getParam(Constants.EXECUTE_ACTIONSUMMARY));
            options.setActionHidden(getParam(Constants.EXECUTE_ACTIONHIDDEN));
            options.setActionTags(getParam(Constants.EXECUTE_ACTIONTAGS));
            options.setActionRoles(getParam(Constants.EXECUTE_ACTIONROLES));
            options.setActionID(getParam(Constants.EXECUTE_ACTIONID));
            options.setProcessID(getParam(Constants.EXECUTE_PROCESSID));
            options.setWiki(getParam(Constants.EXECUTE_WIKI));
            options.setExecuteID(getParam(Constants.EXECUTE_EXECUTEID));
            options.setParserID(getParam(Constants.EXECUTE_PARSERID));
            options.setAssessID(getParam(Constants.EXECUTE_ASSESSID));
            options.setDuration("" + result.getDuration());
            options.setLogResult(getParam(Constants.EXECUTE_LOGRESULT));
            options.setUserID(getParam(Constants.EXECUTE_USERID));
            options.setProblemID(getParam(Constants.EXECUTE_PROBLEMID));
            options.setReference(getParam(Constants.EXECUTE_REFERENCE));
            options.setReturnCode("" + result.getReturncode());
            options.setCompletion("" + result.isCompletion());
            options.setCommand(result.getCommand());
            options.setTimeout("" + getParam(Constants.EXECUTE_TIMEOUT));
            options.setProcessTimeout("" + getParam(Constants.EXECUTE_PROCESS_TIMEOUT));
            options.setMetricId(getParam(Constants.EXECUTE_METRICID));
            header.setOptions(options);
            
            // content
            MMsgContent msg = new MMsgContent();
            msg.setParams("" + getParam(Constants.EXECUTE_PARAMS));
            msg.setFlows("" + getParam(Constants.EXECUTE_FLOWS));
            msg.setInputs("" + getParam(Constants.EXECUTE_INPUTS));
            msg.setRaw(content);
                        
            // set debug
            StringBuffer debug = result.getDebug();
            if (debug != null)
            {
                msg.setDebug(debug.toString());
            }
            else
            {
                msg.setDebug("");
            }


            // send message
            MainBase.esb.sendMessage(header, msg);
        }
        catch (Exception e)
        {
            Log.log.warn(e.getMessage(), e);
        }
    } // sendResult

    String readInputFile(String filename)
    {
        String result = "";
        String filenameProxy = "";
        
        if (!filename.equals(inputfile) && !filename.startsWith("/"))
        {
            if (!filename.endsWith(".groovy"))
            {
                filename += ".groovy";
            }
            
            filenameProxy = MainBase.main.getGSEHome()+"/proxy/"+filename;

            filename = MainBase.main.getGSEHome()+"/"+filename;
            
        }
    
        // read content from file
        Log.log.trace("Filename: "+filenameProxy);
        File file = new File(filenameProxy);
        if (!file.exists())
        {
	        Log.log.trace("Filename: "+filename);
            file = new File(filename);
        }
        if (file.exists())
        {
            try
            {
                result = FileUtils.readFileToString(file, "UTF-8");
            }
            catch (Exception e)
            {
                Log.log.error("Failed to execute REMOTE actiontask: "+e.getMessage(), e);
            }
        }
        
        return result;
    } // readInputFile
    
    ExecuteResult executeOS()
    {
        ExecuteResult result = null;

        if (data != null)
        {
            String jobname = getParam("JOBNAME");
            String execpath = getParam("EXECPATH");
            String cmdline = getParam("CMDLINE");
            String input = getParam("INPUT");
            int timeout = StringUtils.stringToInt(getParam("TIMEOUT"));

            // replace variables
            cmdline = replaceVars(cmdline);

            ExecuteOS os = new ExecuteOS(execpath);
            result = os.execute(cmdline, input, true, timeout, jobname);
        }
        return result;
    } // executeOS

    ExecuteResult executeGroovy()
    {
        ExecuteResult result = null;

        if (data != null)
        {
            String jobname = getParam("JOBNAME");
            String execpath = getParam("EXECPATH");
            String javahome = getParam("JREPATH");
            String filename = getParam("FILENAME");
            String args = getParam("ARGS");
            String input = getParam("INPUT");
            String classpath = getParam(Constants.ACTION_INVOCATION_OPTIONS_CLASSPATH);
            String javaoptions = getParam(Constants.ACTION_INVOCATION_OPTIONS_JAVAOPTIONS);
            int timeout = StringUtils.stringToInt(getParam("TIMEOUT"));

            // replace variables
            filename = replaceVars(filename);
            args = replaceVars(args);

            // replace classpath
            classpath = replaceVars(classpath);
            
            ExecuteGroovy groovy = new ExecuteGroovy(execpath, javahome, classpath, javaoptions);
            result = groovy.execute(filename, args, input, true, timeout, jobname);
        }
        return result;
    } // executeGroovy

    ExecuteResult executeInternal(ScriptDebug debug)
    {
        ExecuteResult result = null;
        
        if (data != null)
        {
            String content = null;
            String actionName = getParam(Constants.EXECUTE_ACTIONNAME);
            String jobname = getParam(Constants.EXECUTE_JOBNAME);
            String filename = getParam(Constants.EXECUTE_FILENAME);
            String args = getParam(Constants.EXECUTE_ARGS);
            String targetAddr = getParam(Constants.EXECUTE_TARGET);
            String paramsStr = getParam(Constants.EXECUTE_PARAMS);
            String flowsStr = getParam(Constants.EXECUTE_FLOWS);
            String inputsStr = getParam(Constants.EXECUTE_INPUTS);
            String reference = getParam(Constants.EXECUTE_REFERENCE);
            String processid = getParam(Constants.EXECUTE_PROCESSID);
            String problemid = getParam(Constants.EXECUTE_PROBLEMID);
            String wiki = getParam(Constants.EXECUTE_WIKI);
            boolean destroy = getParamBoolean(Constants.ACTION_INVOCATION_OPTIONS_TIMEOUT_DESTROY_THREAD);
            
            int timeout = StringUtils.stringToInt(getParam(Constants.EXECUTE_TIMEOUT));
            int processTimeout = StringUtils.stringToInt(getParam(Constants.EXECUTE_PROCESS_TIMEOUT));
            
            // get INPUTFILE content
            if (StringUtils.isEmpty(filename) || filename.equals(Constants.OPTIONS_INPUTFILE))
            {
                content = getParam(Constants.ACTION_INVOCATION_OPTIONS_INPUTFILE);
            }
            else
            {
                content = readInputFile(replaceVars(filename));
            }
                
        	// replace variables
        	content = replaceVars(content);
        	args = replaceVars(args);
	        	
            Map inputs = (Map)StringUtils.stringToObj(inputsStr);
            if (inputs == null)
            {
                inputs = new HashMap();
            }
            Map params = (Map)StringUtils.stringToObj(paramsStr);
            if (params == null)
            {
                params = new HashMap();
            }
            Map flows = (Map)StringUtils.stringToObj(flowsStr);
            if (flows == null)
            {
                flows = new HashMap();
            }
            
            Log.log.debug("ExecuteInternal name: "+actionName);
            Log.log.debug("  inputs: "+inputs);
            Log.log.debug("  params: "+params);
            Log.log.debug("  flows: "+flows);
            
            debug.println("ExecuteInternal name: "+actionName);
            debug.println("  inputs: "+inputs);
            debug.println("  params: "+params);
            debug.println("  flows: "+flows);
            
            // execute
            ExecuteRemote executeRemote = new ExecuteRemote();
            result = executeRemote.execute(processid, content, actionName, true, args, inputs, params, flows, debug, reference, timeout, processTimeout, jobname, problemid, wiki, destroy);
            
            // update params and flows
            setParam(Constants.EXECUTE_PARAMS, StringUtils.objToString(params));
            setParam(Constants.EXECUTE_FLOWS, StringUtils.objToString(flows));
            setParam(Constants.EXECUTE_INPUTS, StringUtils.objToString(inputs));
        }

        return result;
    } // executeInternal

    String replaceVars(String line)
    {
        String result = line;

        if (!StringUtils.isEmpty(line))
        {
	        // ${TMPFILE}
	        if (line.contains(Constants.OPTIONS_TMPFILE))
	        {
		        // initialize
		        String prefix = getParam(Constants.ACTION_INVOCATION_OPTIONS_TMPFILE_PREFIX);
		        if (StringUtils.isEmpty(prefix))
		        {
		            prefix = "tmpfile_";
		        }
		        String postfix = getParam(Constants.ACTION_INVOCATION_OPTIONS_TMPFILE_POSTFIX);
		        if (StringUtils.isEmpty(postfix))
		        {
		            postfix = ".tmp";
		        }
	
	            tmpfile = prefix + Thread.currentThread().getName()+postfix;
	            tmpfile = inputfile.replace('-', '_');
	            result = result.replace(Constants.OPTIONS_TMPFILE, tmpfile);
	        }
	        
	        // ${INPUTFILE}
	        if (line.contains(Constants.OPTIONS_INPUTFILE) && inputfile != null)
	        {
	            result = result.replace(Constants.OPTIONS_INPUTFILE, inputfile);
	        }
	        
	        // ${INSTALLDIR}
	        if (line.contains(Constants.OPTIONS_INSTALLDIR))
	        {
	            result = result.replace(Constants.OPTIONS_INSTALLDIR, MainBase.main.configGeneral.home);
	        }
	        
	        // $CONFIG{<property_name>}
	        String matchContent = result;
	        result = "";
	        
	        Matcher matcher = VAR_REGEX_CONFIG.matcher(matchContent);
	        int last = 0;
	        while (matcher.find())
	        {
	            String name = matcher.group(1);
	            String value = MainBase.main.configProperty.getProperty(name);
	                
	            result += matchContent.substring(last, matcher.start()) + value;
	            last = matcher.end();
	        }
	        result += matchContent.substring(last, matchContent.length());
        }
        
        return result;
    } // replaceVars

    String getParam(String name)
    {
        String result = "";
        
        Object value = data.get(name.toUpperCase());
        if (value != null)
        {
	        if (value instanceof String)
	        {
	        	result = (String)value;
	        	
	        	if (result.equalsIgnoreCase("null"))
	        	{
	        		result = "";
	        	}
	        }
	        else
	        {
	        	result = "" + value;
	        }
        }
        
        return result;
    } // getParam
    
    void setParam(String name, String value)
    {
        data.put(name.toUpperCase(), value);
    } // setParam
    
    Object getParamObject(String name)
    {
        return data.get(name.toUpperCase());
    } // getParamObject
    
    boolean getParamBoolean(String name)
    {
        boolean result = false;
        
        Object value = data.get(name.toUpperCase());
        if (value != null)
        {
	        if (value instanceof String)
	        {
                if (((String)value).equalsIgnoreCase("true"))
                {
                    result = true;
                }
	        }
	        else if (value instanceof Boolean)
	        {
	            result = ((Boolean)value).booleanValue();
	        }
        }
        
        return result;
    } // getParamBoolean
    
    public void kill(Map params)
    {
        String type    = null;
        String jobname = null;
        Execute execute = null;
        
        try
        {
	        type    = (String)params.get("TYPE");
	        jobname = (String)params.get("JOBNAME");
	        execute = (Execute)params.get("EXECUTE");
    	        
            if (type.equals("PROCESS"))
            {
    	        Process process = getJobProcess(jobname);
    	        if (process != null)
    	        {
	                Log.log.info("Aborting ExecuteMain system process");
					process.destroy();
    	            process.getInputStream().close();
    	            process.getOutputStream().close();
    	            process.getErrorStream().close();
    	        }
	            ExecuteMain.removeJobProcess(jobname);
            }
            else if (type.equals("THREAD"))
            {
    	        Thread thread = getJobThread(jobname);
    	        if (thread != null)
    	        {
	                Log.log.info("Aborting ExecuteMain thread id: "+thread.getId()+" name: "+thread.getName());
		            thread.interrupt();
    	        }
    	        ExecuteMain.removeJobThread(jobname);
            }
                    
            // indicate that the job was killed
            if (execute != null)
            {
	            execute.setAborted(true);
            }
        }
        catch (Exception e)
        {
            Log.log.error("Failed to kill actiontask: "+e.getMessage(), e);
        }
    } // kill
    
    public static Process getJobProcess(String jobname)
    {
    	return (Process)jobProcess.get(jobname.toUpperCase());
    } // getJobProcess
    
    public static void setJobProcess(String jobname, Process process)
    {
    	if (jobname != null && process != null)
    	{
	    	jobProcess.put(jobname.toUpperCase(), process);
    	}
    } // setJobProcess
    
    public static void removeJobProcess(String jobname)
    {
    	if (jobname != null)
    	{
	    	jobProcess.remove(jobname.toUpperCase());
    	}
    } // removeJobProcess

    public static Thread getJobThread(String jobname)
    {
    	return (Thread)jobThread.get(jobname.toUpperCase());
    } // getJobThread
    
    public static void setJobThread(String jobname, Thread thread)
    {
    	if (jobname != null && thread != null)
    	{
	    	jobThread.put(jobname.toUpperCase(), thread);
    	}
    } // setJobThread
    
    public static void removeJobThread(String jobname)
    {
        if (jobname != null)
        {
	    	jobThread.remove(jobname.toUpperCase());
        }
    } // removeJobThread

} // ExecuteMain
