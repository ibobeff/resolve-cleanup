/******************************************************************************
 * (C) Copyright 2014
 *
 * Resolve Systems, LLC
 *
 * All rights reserved
 * This software is distributed under license and may not be
 * used, copied, modified, or distributed without the express written permission
 * of Resolve Systems, LLC.
 *
 ******************************************************************************/

package com.resolve.execute;

import java.net.InetAddress;
import java.net.UnknownHostException;

import com.resolve.rsbase.MainBase;
import com.resolve.util.Log;

public class ExecuteWinExec extends Execute
{
    String options;
    int timeout;
    String ipaddress;
    String username;
    String password;

    public ExecuteWinExec(String execdir, int timeout, String ipaddress, String username, String password)
    {
        super(execdir);

        this.options = "";
        this.timeout = timeout;
        this.ipaddress = ipaddress;
        this.username = username;
        this.password = password;
    } // ExecuteWinExec

    public ExecuteResult start(String cmdline, String input)
    {
        return execute(cmdline, input, true, 0, null);
    } // start

    public ExecuteResult execute(String cmdline, String input, boolean wait, int timeout, String jobname)
    {
        ExecuteResult result = null;

        if (ipaddress != null && username != null && password != null)
        {
            try
            {
                // CWE-350: Reliance on Reverse DNS Resolution for a Security-Critical Action
                // Dako: introducing ipAddressAfterReverseDNSLookup to be used instead of string ipaddress
                // 2016-04-15: BEGIN
                String ipAddressAfterReverseDNSLookup = InetAddress.getByName(ipaddress).getCanonicalHostName();

                String localHostName = InetAddress.getLocalHost().getHostName();
                String localHostAddress = InetAddress.getLocalHost().getHostAddress();
                System.out.printf("WinExec is comparing %s == %s\n", ipAddressAfterReverseDNSLookup, localHostName);
                System.out.printf("WinExec comparing %s == %s\n", ipAddressAfterReverseDNSLookup, localHostAddress);
                if (ipAddressAfterReverseDNSLookup != null && 
                                !ipAddressAfterReverseDNSLookup.equals(".") && 
                                !ipAddressAfterReverseDNSLookup.equalsIgnoreCase("localhost") && 
                                !ipAddressAfterReverseDNSLookup.equalsIgnoreCase(localHostName) && 
                                !ipAddressAfterReverseDNSLookup.equalsIgnoreCase(localHostAddress))
                {
                    cmdline = "\"" + MainBase.main.getProductHome() + "/bin/remcom.exe\" " + "\\\\" + ipAddressAfterReverseDNSLookup + " /user:\"" + username + "\" /pwd:\"" + password + "\" " + cmdline;
                }
                // 2016-04-15: END

            }
            catch (UnknownHostException e)
            {
                Log.log.error("Failed to execute WINEXEC actiontask: " + e.getMessage(), e);
            }
        }

        // execute command and environment
        result = super.execute(cmdline, input, wait, timeout, jobname);

        // raw result
        String raw = result.getResultString();

        // remove header and get remote command
        String commandStr = "Remote Command:";
        int commandPos = raw.indexOf(commandStr);

        String resultStr = "Initiating Connection to Remote Service";
        int resultPos = raw.indexOf(resultStr);

        if (commandPos >= 0 && resultPos >= 0)
        {
            String command = raw.substring(commandPos, resultPos - 1);
            result.setCommand(command.trim());
            result.setResult(raw.substring(resultPos));
        }
        else
        {
            result.setResult(raw);
        }

        return result;
    } // execute

} // ExecuteWinExec
