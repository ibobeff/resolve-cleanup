/******************************************************************************
* (C) Copyright 2014
*
* Resolve Systems, LLC
*
* All rights reserved
* This software is distributed under license and may not be used, copied,
* modified, or distributed without the express written permission of
* Resolve Systems, LLC.
*
******************************************************************************/

package com.resolve.execute;

public class ExecuteWinTask extends Execute
{
    String options;
    
    public ExecuteWinTask(String execdir)
    {
        super(execdir);
        
        this.options    = "";
    } // ExecuteWinTask
    
    public ExecuteWinTask(String execdir, String options)
    {
        super(execdir);
        
        this.options = options;
    } // ExecuteWinTask
    
    public ExecuteResult start(String cmdline)
    {
        return execute(cmdline, true, 0, null);
    } // start
    
    /*
    public ExecuteResult execute(String cmdline, String[] args)
    {
        return this.execute(cmdline+StringUtils.join(args," "), true, 0, null);
    } // execute
    
    public ExecuteResult execute(String cmdline, String[] args, int timeout, String jobname)
    {
        return this.execute(cmdline+StringUtils.join(args," "), true, timeout, jobname);
    } // execute
    
    public ExecuteResult execute(String cmdline, String[] args, boolean wait)
    {
        return this.execute(cmdline+StringUtils.join(args," "), wait, 0, null);
    } // execute
    
    public ExecuteResult execute(String cmdline, String[] args, boolean wait, int timeout, String jobname)
    {
        return this.execute(cmdline+StringUtils.join(args," "), wait, timeout, jobname);
    } // execute
    */
    
    public ExecuteResult execute(String cmdline, boolean wait, int timeout, String jobname)
    {
        cmdline = "scm/rsi/script/wintask/bin/taskexec"+" "+options+" "+cmdline;
        
        // execute command and enviornment
        return super.execute(cmdline, null, wait, timeout, jobname);
    } // execute 

} // ExecuteWinTask
