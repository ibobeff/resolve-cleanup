/******************************************************************************
* (C) Copyright 2014
*
* Resolve Systems, LLC
*
* All rights reserved
* This software is distributed under license and may not be used, copied,
* modified, or distributed without the express written permission of
* Resolve Systems, LLC.
*
******************************************************************************/

package com.resolve.notification;

import java.io.File;
import java.io.IOException;
import java.util.HashMap;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import com.resolve.rsbase.MainBase;
import com.resolve.services.hibernate.util.GenericUserUtils;
import com.resolve.services.hibernate.util.ResolveBlueprintUtil;
import com.resolve.services.hibernate.vo.ResolveBlueprintVO;
import com.resolve.services.hibernate.vo.UsersVO;
import com.resolve.util.Constants;
import com.resolve.util.FileUtils;
import com.resolve.util.Log;
import com.resolve.util.Properties;
import com.resolve.util.StringUtils;

/**
 * This class is for sending notifications (e.g., Email, MS Exchange etc.) to user.
 */
public class NotificationAPI
{

    public static final String RSREMOTE_RECEIVE_EXCHANGE_QUEUE = "rsremote.receive.exchange.queue";
    public static final String RSREMOTE_RECEIVE_EMAIL_QUEUE = "rsremote.receive.email.queue";
    public static final String RSREMOTE_RECEIVE_EWS_QUEUE = "rsremote.receive.ews.queue";
    public static final String RSREMOTE_RECEIVE_XMPP_QUEUE = "rsremote.receive.xmpp.queue";

    public static final String RSREMOTE_RECEIVE_EXCHANGE_ACTIVE = "rsremote.receive.exchange.active";
    public static final String RSREMOTE_RECEIVE_EMAIL_ACTIVE = "rsremote.receive.email.active";
    public static final String RSREMOTE_RECEIVE_EWS_ACTIVE = "rsremote.receive.ews.active";
    public static final String RSREMOTE_RECEIVE_XMPP_ACTIVE = "rsremote.receive.xmpp.active";

    public static final String RSREMOTE_RECEIVE_EXCHANGE_SOCIAL_POST = "rsremote.receive.exchange.socialpost";
    public static final String RSREMOTE_RECEIVE_EMAIL_SOCIAL_POST = "rsremote.receive.email.socialpost";
    public static final String RSREMOTE_RECEIVE_EWS_SOCIAL_POST = "rsremote.receive.ews.socialpost";
    public static final String RSREMOTE_RECEIVE_XMPP_SOCIAL_POST = "rsremote.receive.xmpp.socialpost";

    private static final String DEFAULT_SUBJECT = "Resolve Notification";
    private static final String TRUE = "true";

    public static Set<NotificationTarget> targets = new LinkedHashSet<NotificationTarget>();

    /**
     * This initializer is called by components having access to Hibernate transaction.
     */
    public static void init()
    {
        //clean it up so we can get new targets from DB.
        targets.clear();

        init(null);
    }

    public static void init(List<NotificationTarget> initialTargets)
    {
        if(initialTargets != null && initialTargets.size() > 0) //RSMGMT component sends the target directly.
        {
            targets.addAll(initialTargets);
        }
        else
        {
            List<ResolveBlueprintVO> blueprints = ResolveBlueprintUtil.findAllResolveBlueprint();
            for(ResolveBlueprintVO blueprint : blueprints)
            {
                Properties prop = new Properties();
                try
                {
                    prop.load(StringUtils.toInputStream(blueprint.getUBlueprint()));
                    if(prop.containsKey(RSREMOTE_RECEIVE_EXCHANGE_ACTIVE) && prop.get(RSREMOTE_RECEIVE_EXCHANGE_ACTIVE).equalsIgnoreCase(TRUE))
                    {
                        if(prop.containsKey(RSREMOTE_RECEIVE_EXCHANGE_SOCIAL_POST) && prop.get(RSREMOTE_RECEIVE_EXCHANGE_SOCIAL_POST).equalsIgnoreCase(TRUE))
                        {
                            targets.add(new NotificationTarget(Constants.NOTIFICATION_CARRIER_TYPE_EMAIL, prop.get(RSREMOTE_RECEIVE_EMAIL_QUEUE)));
                        }
                    }
                    if(prop.containsKey(RSREMOTE_RECEIVE_EMAIL_ACTIVE) && prop.get(RSREMOTE_RECEIVE_EMAIL_ACTIVE).equalsIgnoreCase(TRUE))
                    {
                        if(prop.containsKey(RSREMOTE_RECEIVE_EMAIL_SOCIAL_POST) && prop.get(RSREMOTE_RECEIVE_EMAIL_SOCIAL_POST).equalsIgnoreCase(TRUE))
                        {
                            targets.add(new NotificationTarget(Constants.NOTIFICATION_CARRIER_TYPE_EMAIL, prop.get(RSREMOTE_RECEIVE_EMAIL_QUEUE)));
                        }
                    }
                    if(prop.containsKey(RSREMOTE_RECEIVE_EWS_ACTIVE) && prop.get(RSREMOTE_RECEIVE_EWS_ACTIVE).equalsIgnoreCase(TRUE))
                    {
                        if(prop.containsKey(RSREMOTE_RECEIVE_EWS_SOCIAL_POST) && prop.get(RSREMOTE_RECEIVE_EWS_SOCIAL_POST).equalsIgnoreCase(TRUE))
                        {
                            targets.add(new NotificationTarget(Constants.NOTIFICATION_CARRIER_TYPE_EWS, prop.get(RSREMOTE_RECEIVE_EWS_QUEUE)));
                        }
                    }
                    if(prop.containsKey(RSREMOTE_RECEIVE_XMPP_ACTIVE) && prop.get(RSREMOTE_RECEIVE_XMPP_ACTIVE).equalsIgnoreCase(TRUE))
                    {
                        if(prop.containsKey(RSREMOTE_RECEIVE_XMPP_SOCIAL_POST) && prop.get(RSREMOTE_RECEIVE_XMPP_SOCIAL_POST).equalsIgnoreCase(TRUE))
                        {
                            targets.add(new NotificationTarget(Constants.NOTIFICATION_CARRIER_TYPE_XMPP, prop.get(RSREMOTE_RECEIVE_XMPP_QUEUE)));
                        }
                    }
                }
                catch (IOException e)
                {
                    Log.log.warn("Could not load blueprint properties: " + e.getMessage());
                }
            }
            if(targets.size() == 0)
            {
                Log.log.warn("NotificationAPI init: did not find any active notification targets (e.g., Email gateway)");
            }
            else
            {
                if(Log.log.isDebugEnabled())
                {
                    for(NotificationTarget target : targets)
                    {
                        Log.log.debug("Social Notification target found: " + target.toString());
                    }
                }
            }
        }
    }

    private static Map<String, byte[]> getAttachments(String attachments) throws IOException
    {
        Map<String, byte[]> result = new HashMap<String, byte[]>();
        List<String> attachedFiles = StringUtils.stringToList(attachments, ",");
        for (String attachment : attachedFiles)
        {
            if(StringUtils.isNotEmpty(attachment))
            {
                File file = new File(attachment);
                if(file.isFile())
                {
                    result.put(attachment, FileUtils.readFileToByteArray(file));
                }
            }
        }
        return result;
    }

    private static String getEmailAddresses(String userNames)
    {
        StringBuilder result = new StringBuilder();
        if(StringUtils.isNotEmpty(userNames))
        {
            for(String userName : StringUtils.convertStringToList(userNames, ","))
            {
                UsersVO user = GenericUserUtils.getUser(userName);
                if(user != null && StringUtils.isNotEmpty(user.getUEmail()))
                {
                    result.append(user.getUEmail());
                    result.append(",");
                }
            }
            result.setLength(result.length() - 1);
        }

        return result.toString();
    }

    /**
     * This method informs the caller whether there is at least one email gateway active.
     *
     * @return true if any email gateway is active.
     */
    public static boolean isEmailActive()
    {
        return targets.size() > 0;
    }

    /**
     * Submits a notification message to the ESB that will get picked up by a
     * gateway deployed in certain RSRemote instance. It could be Email or
     * Exchange gateway that will be used to sent email. If this method is
     * called from the "content script" of an Action Task then the message will
     * be routed through RSControl for final delivery.
     *
     * <pre>
     * {@code
     * import com.resolve.notification.NotificationAPI;
     * ....
     *         try
     *         {
     *             String to = "toemail@mailserver.com";
     *             String cc = "ccemail1@mailserver.com, ccemail2@mailserver";
     *             String bcc = "bccemail1@mailserver.com, bccemail2@mailserver";
     *             String from = "myemail@mailserver.com";
     *             String subject = "Weather news.";
     *             String content = "The weather is nice with no chance of rain";
     *             //String attachments = "c:/temp/myattachment1.doc,c:/temp/myattachment2.pdf"; //for Windows files.
     *             String attachments = "/usr/local/myattachment1.doc,/usr/local/myattachment2.pdf";
     *
     *             NotificationAPI.send(to, cc, bcc, from, subject, content, attachments);
     *         }
     *         catch (Exception e)
     *         {
     *             //Do something good with the exception.
     *         }
     * }
     * </pre>
     *
     * @param to
     *            address to send the notification.
     * @param cc
     *            comma separated list of usernames to be used in email address
     *            as CC receipient.
     * @param bcc
     *            comma separated list of usernames to be used in email address
     *            as BCC receipient.
     * @param from
     *            email address, if null default will be used.
     * @param subject
     *            of the notification, if null default will be used.
     * @param content
     *            to be delivered.
     * @param attachments
     *            , comma separated list of files to be attached. Must be the
     *            absolute path to the files.
     * @throws Exception
     */
    public static void send(String to, String cc, String bcc, String from, String subject, String content, String attachments) throws Exception
    {
        send(to, cc, bcc, from, null, subject, content, attachments, from);
    }

    /**
     * Submits a notification message to the ESB that will get picked up by a
     * gateway deployed in certain RSRemote instance. It could be Email or
     * Exchange gateway that will be used to sent email. If this method is
     * called from the "content script" of an Action Task then the message will
     * be routed through RSControl for final delivery.
     *
     * <pre>
     * {@code
     * import com.resolve.notification.NotificationAPI;
     * ....
     *         try
     *         {
     *             String to = "toemail@mailserver.com";
     *             String cc = "ccemail1@mailserver.com, ccemail2@mailserver";
     *             String bcc = "bccemail1@mailserver.com, bccemail2@mailserver";
     *             String from = "myemail@mailserver.com";
     *             String replyTo = "maillist1@mailserver.com";
     *             String subject = "Weather news.";
     *             String content = "The weather is nice with no chance of rain";
     *             //String attachments = "c:/temp/myattachment1.doc,c:/temp/myattachment2.pdf"; //for Windows files.
     *             String attachments = "/usr/local/myattachment1.doc,/usr/local/myattachment2.pdf";
     *
     *             NotificationAPI.send(to, cc, bcc, from, replyTo, subject, content, attachments);
     *         }
     *         catch (Exception e)
     *         {
     *             //Do something good with the exception.
     *         }
     * }
     * </pre>
     *
     * @param to
     *            address to send the notification.
     * @param cc
     *            comma separated list of usernames to be used in email address
     *            as CC receipient.
     * @param bcc
     *            comma separated list of usernames to be used in email address
     *            as BCC receipient.
     * @param from
     *            email address, if null default will be used.
     * @param replyTo
     *            the addres that will be replied to by the receiver, in general used for mailing list.
     * @param subject
     *            of the notification, if null default will be used.
     * @param content
     *            to be delivered.
     * @param attachments
     *            , comma separated list of files to be attached. Must be the
     *            absolute path to the files.
     * @param fromDisplayName
     *            email from display name
     * @throws Exception
     */
    public static void send(String to, String cc, String bcc, String from, String replyTo, String subject, String content, String attachments, String fromDisplayName) throws Exception
    {
        //Users user = UserUtils.getUser(userName);
        Log.log.trace("send() : Sending notification to :" + to + ", content:" + content);
        try
        {
            if(StringUtils.isNotEmpty(to))
            {
                Map<String, Object> params = new HashMap<String, Object>();

                params.put(Constants.NOTIFICATION_CARRIER_TYPE, from);

                params.put(Constants.NOTIFICATION_MESSAGE_FROM, from);
                params.put(Constants.NOTIFICATION_MESSAGE_REPLY_TO, replyTo);
                params.put(Constants.NOTIFICATION_MESSAGE_TO, to);
                params.put(Constants.NOTIFICATION_MESSAGE_CC, cc); //Originally we were applying getEmailAddresses to cc and bcc, but the docs say to pass a list of emails
                params.put(Constants.NOTIFICATION_MESSAGE_BCC, bcc); //(not user names). We may need to come back to this if there is in fact a case where we need to pass users
                params.put(Constants.NOTIFICATION_MESSAGE_SUBJECT, subject);
                params.put(Constants.NOTIFICATION_MESSAGE_CONTENT, content);
                params.put(Constants.NOTIFICATION_MESSAGE_FROM_DISPLAY_NAME, fromDisplayName);

                params.put(Constants.NOTIFICATION_MESSAGE_ATTACHMENTS, getAttachments(attachments));

                Log.log.trace("send() : params: " + params);

                String classMethod = null;
                if(targets == null || targets.size() == 0)
                {
                    //For example, RSRemote type components, it'll just send to MNotification in RSControl

                    classMethod = "MNotification.sendMessage";

                    //Submit to the ESB so it can be processed by RSControl
                    boolean isSuccess = MainBase.esb.sendInternalMessage(Constants.ESB_NAME_RSCONTROL, classMethod, params);
                    if(isSuccess)
                    {
                        Log.log.debug("Notification sent to the RSControl for processing.");
                    }
                    else
                    {
                        Log.log.warn("Notification failed during transmission to RSControl for processing.");
                    }
                }
                else
                {
                    Log.log.trace("send() : targets size: " + targets.size());
                    for(NotificationTarget target : targets)
                    {
                        if(target.carrierType.equals(Constants.NOTIFICATION_CARRIER_TYPE_EMAIL))
                        {
                            classMethod = "MEmail.sendMessage";
                        }
                        else if(target.carrierType.equals(Constants.NOTIFICATION_CARRIER_TYPE_EXCHANGE))
                        {
                            classMethod = "MExchange.sendMessage";
                        }
                        /*else if(target.carrierType.equals(Constants.NOTIFICATION_CARRIER_TYPE_XMPP))
                        {
                            classMethod = "MXMPP.sendMessage";
                        }*/
                        else if(target.carrierType.equals(Constants.NOTIFICATION_CARRIER_TYPE_EWS))
                        {
                            classMethod = "MEWS.sendMessage";
                        }

                        //Submit to the ESB
                        boolean isSuccess = MainBase.esb.sendMessage(target.gateway + "_TOPIC", classMethod, params);
                        if(isSuccess)
                        {
                            Log.log.trace("send() : ESB message sent to : " + target.gateway);
                            //TODO as soon as the first gateway is used, get out of here. However, in the future there will be
                            //more logic
                            break;
                        }
                        else
                        {
                            Log.log.warn("Email Gateway: " + target.gateway + " is invalid, may be bad data in resolve_blueprint. Ignoring.");
                        }
                    }
                }
            }
        }
        catch (Exception e)
        {
            Log.log.error(e.getMessage(), e);
            throw e;
        }
    }

    public static void sendIMToUser(String userName, String from, String content) throws Exception
    {
        Log.log.trace("Sending IM notification to user:" + userName + ", content:" + content);
        try
        {
            UsersVO user = GenericUserUtils.getUser(userName);
            if(user != null && StringUtils.isNotEmpty(user.getUIM()) && targets.size() > 0)
            {
                sendIM(user.getUIM(), content, from, null);
            }
        }
        catch (Exception e)
        {
            Log.log.error(e.getMessage() , e);
            throw e;
        }
        catch (NoClassDefFoundError e)
        {
            Log.log.warn("This API  (NotificationAPI.sendIMToUser) is not supported in this Resolve component. Please refer to the NotificationAPI document for more information.");
            Log.log.error("Detail error: " + e.getMessage() , e);
            throw new Exception("This API is not supported in this Resolve component. Please refer to the NotificationAPI document for more information.");
        }
    }

    public static void sendIM(String to, String content, String username, String password) throws Exception
    {
        //Users user = UserUtils.getUser(userName);
        try
        {
            if(StringUtils.isNotEmpty(to))
            {
                Map<String, Object> params = new HashMap<String, Object>();

                params.put(Constants.NOTIFICATION_MESSAGE_TO, to);
                params.put(Constants.NOTIFICATION_MESSAGE_CONTENT, content);
                params.put(Constants.NOTIFICATION_CARRIER_TYPE, Constants.NOTIFICATION_CARRIER_TYPE_XMPP);
                params.put(Constants.NOTIFICATION_CARRIER_USERNAME, username);
                params.put(Constants.NOTIFICATION_CARRIER_PASSWORD, password);

                String classMethod = null;
                if(targets == null || targets.size() == 0)
                {
                    //For example, RSRemote type components, it'll just send to MNotification in RSControl

                    classMethod = "MNotification.sendMessage";

                    //Submit to the ESB so it can be processed by RSControl
                    boolean isSuccess = MainBase.esb.sendInternalMessage(Constants.ESB_NAME_RSCONTROL, classMethod, params);
                    if(isSuccess)
                    {
                        Log.log.debug("Notification sent to the RSControl for processing.");
                    }
                    else
                    {
                        Log.log.warn("Notification failed during transmission to RSControl for processing.");
                    }
                }
                else
                {
                    for(NotificationTarget target : targets)
                    {
                        if(target.carrierType.equals(Constants.NOTIFICATION_CARRIER_TYPE_XMPP))
                        {
                            classMethod = "MXMPP.sendMessage";

                            //Submit to the ESB
                            boolean isSuccess = MainBase.esb.sendMessage(target.gateway, classMethod, params);
                            if(isSuccess)
                            {
                                //TODO as soon as the first gateway is used, get out of here. However, in the future there will be
                                //more logic
                                break;
                            }
                            else
                            {
                                Log.log.warn("XMPP Gateway: " + target.gateway + " is invalid, may be bad data in resolve_blueprint. Ignoring.");
                            }
                        }
                    }
                }
            }
        }
        catch (Exception e)
        {
            Log.log.error(e.getMessage(), e);
            throw e;
        }
    }

    /**
     * Submits a notification message to the ESB that will get picked up by a
     * gateway deployed in certain RSRemote instance.
     *
     * IMPORTANT NOTE: This method can only be used from the "assessor" of an
     * Action Task. Or server side code in RSControl and RSView.
     *
     * <pre>
     * {@code
     * import com.resolve.notification.NotificationAPI;
     * ....
     *         try
     *         {
     *             String username = "joe";
     *             String cc = "ccemail1@mailserver.com, ccemail2@mailserver";
     *             String bcc = "bccemail1@mailserver.com, bccemail2@mailserver";
     *             String from = "myemail@mailserver.com";
     *             String subject = "Weather news.";
     *             String content = "The weather is nice with no chance of rain";
     *             //String attachments = "c:/temp/myattachment1.doc,c:/temp/myattachment2.pdf";
     *             String attachments = "/usr/local/myattachment1.doc,/usr/local/myattachment2.pdf";
     *
     *             NotificationAPI.sendToUser(username, cc, bcc, from, subject, content, attachments);
     *         }
     *         catch (Exception e)
     *         {
     *             //Do something good with the exception.
     *         }
     * }
     * </pre>
     *
     * @param userName
     *            to send the notification.
     * @param cc
     *            comma separated list of usernames to be used in email address
     *            as CC receipient.
     * @param bcc
     *            comma separated list of usernames to be used in email address
     *            as BCC receipient.
     * @param from
     *            email address, if null default will be used.
     * @param subject
     *            of the notification, if null default will be used.
     * @param content
     *            to be delivered.
     * @param attachments
     *            , comma separated list of files to be attached. Must be the
     *            absolute path to the files.
     * @throws Exception
     */
    public static void sendToUser(String userName, String cc, String bcc, String from, String subject, String content, String attachments) throws Exception
    {
        Log.log.trace("Sending notification to user:" + userName + ", content:" + content);
        try
        {
            UsersVO user = GenericUserUtils.getUser(userName);
            if(user != null && StringUtils.isNotEmpty(user.getUEmail()) && targets.size() > 0)
            {
                send(user.getUEmail(), cc, bcc, from, subject, content, attachments);
            }
        }
        catch (Exception e)
        {
            Log.log.error(e.getMessage() , e);
            throw e;
        }
        catch (NoClassDefFoundError e)
        {
            Log.log.warn("This API  (NotificationAPI.sendToUser) is not supported in this Resolve component. Please refer to the NotificationAPI document for more information.");
            Log.log.error("Detail error: " + e.getMessage() , e);
            throw new Exception("This API is not supported in this Resolve component. Please refer to the NotificationAPI document for more information.");
        }
    }

    /**
     * Sends notification to a user by her username with the default subject.
     *
     * IMPORTANT NOTE: This method can only be used from the "assessor" of an
     * Action Task. Or server side code in RSControl and RSView.
     *
     * <pre>
     * {@code
     * import com.resolve.notification.NotificationAPI;
     * ....
     *         try
     *         {
     *             String username = "joe";
     *             String content = "The weather is nice with no chance of rain";
     *             //String attachments = "c:/temp/myattachment1.doc,c:/temp/myattachment2.pdf";
     *             String attachments = "/usr/local/myattachment1.doc,/usr/local/myattachment2.pdf";
     *
     *             NotificationAPI.sendToUser(username, content, attachments);
     *         }
     *         catch (Exception e)
     *         {
     *             //Do something good with the exception.
     *         }
     * }
     * </pre>
     *
     * @param userName
     *            to send the notification.
     * @param content
     *            to be delivered.
     * @param attachments
     *            , comma separated list of files to be attached. Must be the
     *            absolute path to the files.
     * @throws Exception
     */
    public static void sendToUser(String userName, String content, String attachments) throws Exception
    {
        sendToUser(userName, DEFAULT_SUBJECT, content, attachments);
    }

    /**
     * Sends notification to a user by her Resolve username.
     *
     * IMPORTANT NOTE: This method can only be used from the "assessor" of an
     * Action Task. Or server side code in RSControl and RSView.
     *
     * <pre>
     * {@code
     * import com.resolve.notification.NotificationAPI;
     * ....
     *         try
     *         {
     *             String username = "joe";
     *             String subject = "Weather news.";
     *             String content = "The weather is nice with no chance of rain";
     *             //String attachments = "c:/temp/myattachment1.doc,c:/temp/myattachment2.pdf";
     *             String attachments = "/usr/local/myattachment1.doc,/usr/local/myattachment2.pdf";
     *
     *             NotificationAPI.sendToUser(username, subject, content, attachments);
     *         }
     *         catch (Exception e)
     *         {
     *             //Do something good with the exception.
     *         }
     * }
     * </pre>
     *
     * @param userName
     *            to send the notification.
     * @param subject
     *            of the email.
     * @param content
     *            to be delivered.
     * @param attachments
     *            , comma separated list of files to be attached. Must be the
     *            absolute path to the files.
     * @throws Exception
     */
    public static void sendToUser(String userName, String subject, String content, String attachments) throws Exception
    {
        sendToUser(userName, null, null, null, subject, content, attachments);
    }

    /**
     * Sends notification to a group of users with default subject.
     *
     * IMPORTANT NOTE: This method can only be used from the "assessor" of an
     * Action Task. Or server side code in RSControl and RSView.
     *
     * <pre>
     * {@code
     * import com.resolve.notification.NotificationAPI;
     * ....
     *         try
     *         {
     *             String groupName = "mygroup";
     *             String content = "The weather is nice with no chance of rain";
     *             //String attachments = "c:/temp/myattachment1.doc,c:/temp/myattachment2.pdf";
     *             String attachments = "/usr/local/myattachment1.doc,/usr/local/myattachment2.pdf";
     *
     *             NotificationAPI.sendToGroup(groupName, content, attachments);
     *         }
     *         catch (Exception e)
     *         {
     *             //Do something good with the exception.
     *         }
     * }
     * </pre>
     *
     * @param groupName
     *            to send the notification to its member users.
     * @param content
     *            to be delivered.
     * @param attachments
     *            , comma separated list of files to be attached. Must be the
     *            absolute path to the files.
     * @throws Exception
     */
    public static void sendToGroup(String groupName, String content, String attachments) throws Exception
    {
        sendToGroup(groupName, DEFAULT_SUBJECT, content, attachments);
    }

    /**
     * Sends notification to a group of users.
     *
     * IMPORTANT NOTE: This method can only be used from the "assessor" of an Action Task. Or
     * server side code in RSControl and RSView.
     *
     * <pre>
     * {@code
     * import com.resolve.notification.NotificationAPI;
     * ....
     *         try
     *         {
     *             String groupName = "mygroup";
     *             String subject = "Weather news.";
     *             String content = "The weather is nice with no chance of rain";
     *             //String attachments = "c:/temp/myattachment1.doc,c:/temp/myattachment2.pdf";
     *             String attachments = "/usr/local/myattachment1.doc,/usr/local/myattachment2.pdf";
     *
     *             NotificationAPI.sendToGroup(groupName, subject, content, attachments);
     *         }
     *         catch (Exception e)
     *         {
     *             //Do something good with the exception.
     *         }
     * }
     * </pre>
     *
     * @param groupName to send the notification to its member users.
     * @param subject of the email.
     * @param content to be delivered.
     * @param attachments a comma separated list of files to be attached. Must be the absolute path to the files.
     * @throws Exception
     */
    public static void sendToGroup(String groupName, String subject, String content, String attachments) throws Exception
    {
        try
        {
            Set<String> userNames = GenericUserUtils.getUsersForGroup(groupName);
            for(String userName : userNames)
            {
                sendToUser(userName, null, null, null, subject, content, attachments);
            }
        }
        catch (Exception e)
        {
            Log.log.warn("Make sure you are calling this API from a supported Resolve component. Please refer to the NotificationAPI document for more information.", e);
            Log.log.error("Detail error: " + e.getMessage() , e);
            throw e;
        }
        catch (NoClassDefFoundError e)
        {
            Log.log.warn("This API (NotificationAPI.sendToGroup) is not supported in this Resolve component. Please refer to the NotificationAPI document for more information.");
            Log.log.error("Detail error: " + e.getMessage() , e);
            throw new Exception("This API is not supported in this Resolve component. Please refer to the NotificationAPI document for more information.");
        }
    }

    public static void sendToSocialUser(String socialUsername, String content, String senderUsername, boolean sendEmail) throws Exception
    {
        sendToSocialUser(socialUsername, content, content, senderUsername, sendEmail);
    } // sendToSocialUser

    public static void sendToSocialUser(String socialUsername, String subject, String content, String senderUsername, boolean sendEmail) throws Exception
    {
        Map<String, String> params = new HashMap<String, String>();
        params.put("USERNAME", senderUsername);
        params.put("SUBJECT", subject);
        params.put("POSTCONTENT", content);
        params.put("TARGET", socialUsername);

        //Submit to the ESB
        boolean isSuccess = MainBase.esb.sendMessage(Constants.ESB_NAME_RSVIEW, "MSocial.postToUser", params);
        if(isSuccess)
        {
            if(Log.log.isTraceEnabled())
            {
                Log.log.trace("Submitted social notification to user:" + socialUsername);
            }
        }
        else
        {
            Log.log.warn("Could not submit social notification.");
        }

        if(sendEmail)
        {
            sendToUser(socialUsername, subject, content, null);
        }
    }

    public static void sendToSocialTeam(String socialTeamname, String content, String senderUsername, boolean sendEmail) throws Exception
    {
        sendToSocialTeam(socialTeamname, "", content, senderUsername, sendEmail);
    }
    
    public static void sendToSocialTeam(String socialTeamname, String subject, String content, String senderUsername, boolean sendEmail) throws Exception
    {
        Map<String, String> params = new HashMap<String, String>();
        params.put("USERNAME", senderUsername);
        params.put("SUBJECT", subject);
        params.put("POSTCONTENT", content);
        params.put("TARGET", socialTeamname);
        params.put("SENDEMAIL", Boolean.toString(sendEmail));

        //Submit to the ESB
        boolean isSuccess = MainBase.esb.sendMessage(Constants.ESB_NAME_RSVIEW, "MSocial.postToTeam", params);
        if(isSuccess)
        {
            if(Log.log.isTraceEnabled())
            {
                Log.log.trace("Submitted social notification to team:" + socialTeamname);
            }
        }
        else
        {
            Log.log.warn("Could not submit social notification to team:" + socialTeamname);
        }
    }

    /**
     * This method will send notification (Email, XMPP etc.) based on the incoming
     * data in the Map.
     *
     * @param params - shall have key's like Refer to RSComponent.java for the corresponding constants.
     *      sys_id - Id of the social component (Team, Forum, Process etc.)
     *      type - Type of the component ("Team", "Forum", "Process" etc.)
     *      subject - Optional, Subject for email
     *      content - Content to be send.
     */
    @Deprecated
    public static void sendToSocialComponent(Map<String, Object> params)
    {
        //find the target
        if(params == null)
        {
            Log.log.debug("Nothing to set, the param was null, don't make empty call!!!");
        }
        else
        {
            Log.log.trace("############# Received Social Post ###############");
            Log.log.trace(params);
            Log.log.trace("############# End of Social Post ###############");
            //Submit to the ESB
            boolean isSuccess = MainBase.esb.sendMessage(Constants.ESB_NAME_RSVIEW, "MSocial.postToSocialComponent", params);
            if(isSuccess)
            {
                if(Log.log.isTraceEnabled())
                {
                    Log.log.trace("Submitted notification message to social component.");
                }
            }
            else
            {
                Log.log.warn("Could not submit notification message to social component.");
            }
        }
    }

    /**
     * This method will send notification (Email, XMPP etc.) based on the incoming
     * data in the Map.
     *
     * @param params - shall have key's like Refer to RSComponent.java for the corresponding constants.
     *      sys_id - Id of the social component (Team, Forum, Process etc.)
     *      type - Type of the component ("Team", "Forum", "Process" etc.)
     *      subject - Optional, Subject for email
     *      content - Content to be send.
     */
    @Deprecated
    public static void forwardToSocialComponent(Map<String, Object> params)
    {
        //find the target
        if(params == null)
        {
            Log.log.debug("Nothing to set, the param was null, don't make empty call!!!");
        }
        else
        {
            Log.log.trace("############# Received Social Post to forward ###############");
            Log.log.trace(params);
            Log.log.trace("############# End of Social Post to forward ###############");
            //Submit to the ESB
            boolean isSuccess = MainBase.esb.sendMessage(Constants.ESB_NAME_RSVIEW, "MSocial.forwardToSocialComponent", params);
            if(isSuccess)
            {
                if(Log.log.isTraceEnabled())
                {
                    Log.log.trace("Submitted notification message to social component.");
                }
            }
            else
            {
                Log.log.warn("Could not submit notification message to social component.");
            }
        }
    }
}
