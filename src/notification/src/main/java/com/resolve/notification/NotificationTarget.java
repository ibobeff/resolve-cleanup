/******************************************************************************
* (C) Copyright 2014
*
* Resolve Systems, LLC
*
* All rights reserved
* This software is distributed under license and may not be used, copied,
* modified, or distributed without the express written permission of
* Resolve Systems, LLC.
*
******************************************************************************/

package com.resolve.notification;

public class NotificationTarget
{
    public String carrierType; //EMAIL, EXCHANGE, EWS, XMPP, SMS etc.
    public String gateway; //it's the Queue name a certain RSRemote listens.
    
    public NotificationTarget(String carrierType, String gateway)
    {
        this.carrierType = carrierType;
        this.gateway = gateway;
    }
    
    @Override
    public int hashCode()
    {
        final int prime = 31;
        int result = 1;
        result = prime * result + ((carrierType == null) ? 0 : carrierType.hashCode());
        result = prime * result + ((gateway == null) ? 0 : gateway.hashCode());
        return result;
    }

    @Override
    public boolean equals(Object obj)
    {
        if (this == obj) return true;
        if (obj == null) return false;
        if (getClass() != obj.getClass()) return false;
        NotificationTarget other = (NotificationTarget) obj;
        if (carrierType == null)
        {
            if (other.carrierType != null) return false;
        }
        else if (!carrierType.equals(other.carrierType)) return false;
        if (gateway == null)
        {
            if (other.gateway != null) return false;
        }
        else if (!gateway.equals(other.gateway)) return false;
        return true;
    }
    
    @Override
    public String toString()
    {
        return "NotificationTarget [carrierType=" + carrierType + ", gateway=" + gateway + "]";
    }
}
