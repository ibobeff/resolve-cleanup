/******************************************************************************
* (C) Copyright 2014
*
* Resolve Systems, LLC
*
* All rights reserved
* This software is distributed under license and may not be used, copied,
* modified, or distributed without the express written permission of
* Resolve Systems, LLC.
*
******************************************************************************/

package com.resolve.connect;

import it.sauronsoftware.ftp4j.FTPAbortedException;
import it.sauronsoftware.ftp4j.FTPClient;
import it.sauronsoftware.ftp4j.FTPDataTransferException;
import it.sauronsoftware.ftp4j.FTPException;
import it.sauronsoftware.ftp4j.FTPIllegalReplyException;

import java.io.IOException;

import javax.net.ssl.SSLSocketFactory;

import com.resolve.rsbase.MainBase;
import com.resolve.rsbase.SessionObjectInterface;
import com.resolve.util.Log;
import com.resolve.util.SSLUtils;
import com.resolve.util.StringUtils;

public class FTPConnect implements SessionObjectInterface
{

    private final FTPClient client;
    //FTP over implicit TLS/SSL
    private boolean isFTPS = false;
    //FTP over explicit TLS/SSL
    private boolean isFTPES = false;
    private static final String RESOLVE_TEMP_DIR = MainBase.main.configGeneral.home + "/tmp/";

    /**
     * Constructor with anonymous connection and no SSL.
     *
     * @param ftpServer IP or the DNS name.
     * @param port if port is other than the standard 21 (or 990 if FTPS).
     */
    public FTPConnect(String ftpServer, Integer port) throws ConnectException
    {
        this(ftpServer, port, null, null, false, false);
    }

    /**
     * Constructor with no SSL.
     *
     * @param ftpServer IP or the DNS name.
     * @param port if port is other than the standard 21 (or 990 if FTPS).
     * @param username - no need if calling anonymous.
     * @param password - no need if calling anonymous.
     */
    public FTPConnect(String ftpServer, Integer port, String username, String password) throws ConnectException
    {
        this(ftpServer, port, username, password, false, false);
    }

    /**
     * Constructor with SSL configuration.
     *
     * @param ftpServer IP or the DNS name.
     * @param port if port is other than the standard 21 (or 990 if FTPS/FTPES).
     * @param username - no need if calling anonymous.
     * @param password - no need if calling anonymous.
     * @param isFTPS - True if the FTP connection is over implicit TLS/SSL.
     * @param isFTPES - True if the FTP connection is over explicit TLS/SSL. isFTPS takes priority over isFTPES if both are true.
     */
    public FTPConnect(String ftpServer, Integer port, String username, String password, boolean isFTPS, boolean isFTPES) throws ConnectException
    {
        this.isFTPS = isFTPS;
        this.isFTPES = isFTPES;
        SSLSocketFactory sslSocketFactory = null;

        client = new FTPClient();
        
        // if port is other than the standard 21 (or 990 if FTPS):
        try
        {
            //must be before the client.connect call.
            if(isFTPS)
            {
                sslSocketFactory = SSLUtils.getSSLSocketFactory();
                client.setSSLSocketFactory(sslSocketFactory);
                client.setSecurity(FTPClient.SECURITY_FTPS); // enables FTPS
            }
            else
            {
                if(isFTPES)
                {
                    sslSocketFactory = SSLUtils.getSSLSocketFactory();
                    client.setSSLSocketFactory(sslSocketFactory);
                    client.setSecurity(FTPClient.SECURITY_FTPES); // enables FTPES
                }
            }

            if(port != null && port > 0)
            {
                client.connect(ftpServer, port);
            }
            else
            {
                client.connect(ftpServer);
            }

            //If the caller passes blank/null username caller knows that
            //the FTP server allows anonymous access.
            if(StringUtils.isBlank(username))
            {
                client.login("anonymous", "resolve"); //annonymous, so anything will work.
            }
            else
            {
                client.login(username, password);
            }
        }
        catch (IllegalStateException e)
        {
            Log.log.error(e.getMessage(), e);
            throw new ConnectException(e.getMessage(), e);
        }
        catch (IOException e)
        {
            Log.log.error(e.getMessage(), e);
            throw new ConnectException(e.getMessage(), e);
        }
        catch (FTPIllegalReplyException e)
        {
            Log.log.error(e.getMessage(), e);
            throw new ConnectException(e.getMessage(), e);
        }
        catch (FTPException e)
        {
            Log.log.error(e.getMessage(), e);
            throw new ConnectException(e.getMessage(), e);
        }
        catch (Exception e)
        {
            Log.log.error(e.getMessage(), e);
            throw new ConnectException(e.getMessage(), e);
        }
    }

    public boolean isFTPS()
    {
        return isFTPS;
    }

    public void setFTPS(boolean isFTPS)
    {
        this.isFTPS = isFTPS;
    }

    public boolean isFTPES()
    {
        return isFTPES;
    }

    public void setFTPES(boolean isFTPES)
    {
        this.isFTPES = isFTPES;
    }

    /**
     * Downloads a file from ftp server.
     *
     * <pre>
     * {@code
     *     import com.resolve.connect.FTPConnect;
     *     ...
     *     try
     *
     *     {
     *         String ftpServer = "ftp server DNS or IP";
     *         Integer port = 21;
     *         String username = "ftpusername"; //null or  empty if you are accessing anonymously
     *         String password = "ftpuserpwd";  //null or empty if you are accessing anonymously
     *         FTPConnect connect = new FTPConnect(ftpServer, port, username, password);
     *         connect.download("/Products/resolve/logs", "server.log.jar", "/opt/tmp", false);
     *     }
     *     catch (ConnectException e)
     *     {
     *         //do something with the exception
     *     }
     * }
     * </pre>
     *
     * @param directory in the remote server from where the file will be downloaded.
     * @param fileToDownload is the name of the file (e.g., server.log) from the remote directory.
     * @param localDirectory is the name of the local directory where it'll save the downloaded file. If it's
     * not provided then resolve tmp directory will be used (e.g., /opt/resolve/tmp)
     * @param isBinary true if the ftp type should be binary, null will select AUTO mode.
     * @return
     * @throws ConnectException
     */
    public String download(String directory, String fileToDownload, String localDirectory, Boolean isBinary) throws ConnectException
    {
        String result = null;
        try
        {
            client.changeDirectory(directory);
            if(isBinary == null)
            {
                client.setType(FTPClient.TYPE_AUTO);
            }
            else
            {
                client.setType(isBinary ? FTPClient.TYPE_BINARY : FTPClient.TYPE_TEXTUAL);
            }
            if(StringUtils.isBlank(localDirectory))
            {
                localDirectory = RESOLVE_TEMP_DIR;
            }
            else
            {
                if(!localDirectory.endsWith("/"))
                {
                    localDirectory = localDirectory.trim() + "/";
                }
            }
            client.download(fileToDownload, new java.io.File(localDirectory + fileToDownload));

            //send the absolute location of the downloaded file to the caller.
            result = localDirectory + fileToDownload;
        }
        catch (IllegalStateException e)
        {
            Log.log.error(e.getMessage(), e);
            throw new ConnectException(e.getMessage(), e);
        }
        catch (IOException e)
        {
            Log.log.error(e.getMessage(), e);
            throw new ConnectException(e.getMessage(), e);
        }
        catch (FTPIllegalReplyException e)
        {
            Log.log.error(e.getMessage(), e);
            throw new ConnectException(e.getMessage(), e);
        }
        catch (FTPException e)
        {
            Log.log.error(e.getMessage(), e);
            throw new ConnectException(e.getMessage(), e);
        }
        catch (FTPDataTransferException e)
        {
            Log.log.error(e.getMessage(), e);
            throw new ConnectException(e.getMessage(), e);
        }
        catch (FTPAbortedException e)
        {
            Log.log.error(e.getMessage(), e);
            throw new ConnectException(e.getMessage(), e);
        }
        return result;
    }

    /**
     * Uploads a file to the ftp server.
     *
     * <pre>
     * {@code
     *     import com.resolve.connect.FTPConnect;
     *     ...
     *     try
     *
     *     {
     *         String ftpServer = "ftp server DNS or IP";
     *         Integer port = 21;
     *         String username = "ftpusername"; //null or  empty if you are accessing anonymously
     *         String password = "ftpuserpwd";  //null or empty if you are accessing anonymously
     *         FTPConnect connect = new FTPConnect(ftpServer, port, username, password);
     *         connect.upload("/Products/resolve/binary", "c:/temp/resolve-remote.jar", true);
     *     }
     *     catch (ConnectException e)
     *     {
     *         //do something with the exception
     *     }
     * }
     * </pre>
     *
     * @param directory in the remote server to upload this file.
     * @param fileToUpload is the absolute path to the file (e.g., /opt/temp/myfile.txt).
     * @param isBinary true if the ftp type should be binary, null will select AUTO mode.
     * @return
     * @throws ConnectException
     */
    public boolean upload(String directory, String fileToUpload, Boolean isBinary) throws ConnectException
    {
        boolean result = false;

        try
        {
            client.changeDirectory(directory);
            if(isBinary == null)
            {
                client.setType(FTPClient.TYPE_AUTO);
            }
            else
            {
                client.setType(isBinary ? FTPClient.TYPE_BINARY : FTPClient.TYPE_TEXTUAL);
            }
            client.upload(new java.io.File(fileToUpload));
            result = true;
        }
        catch (IllegalStateException e)
        {
            Log.log.error(e.getMessage(), e);
            throw new ConnectException(e.getMessage(), e);
        }
        catch (IOException e)
        {
            Log.log.error(e.getMessage(), e);
            throw new ConnectException(e.getMessage(), e);
        }
        catch (FTPIllegalReplyException e)
        {
            Log.log.error(e.getMessage(), e);
            throw new ConnectException(e.getMessage(), e);
        }
        catch (FTPException e)
        {
            Log.log.error(e.getMessage(), e);
            throw new ConnectException(e.getMessage(), e);
        }
        catch (FTPDataTransferException e)
        {
            Log.log.error(e.getMessage(), e);
            throw new ConnectException(e.getMessage(), e);
        }
        catch (FTPAbortedException e)
        {
            Log.log.error(e.getMessage(), e);
            throw new ConnectException(e.getMessage(), e);
        }
        return result;
    }

    @Override
    public void close()
    {
        try
        {
            client.disconnect(true);
        }
        catch (IllegalStateException e)
        {
            Log.log.error(e.getMessage(), e);
        }
        catch (IOException e)
        {
            Log.log.error(e.getMessage(), e);
        }
        catch (FTPIllegalReplyException e)
        {
            Log.log.error(e.getMessage(), e);
        }
        catch (FTPException e)
        {
            Log.log.error(e.getMessage(), e);
        }
    }

    @Override
    public void finalize() throws Throwable
    {
        this.close();
    } // finalize

}
