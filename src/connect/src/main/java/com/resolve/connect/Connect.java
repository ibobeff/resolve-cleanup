/******************************************************************************
* (C) Copyright 2014
*
* Resolve Systems, LLC
*
* All rights reserved
* This software is distributed under license and may not be used, copied,
* modified, or distributed without the express written permission of
* Resolve Systems, LLC.
*
******************************************************************************/

package com.resolve.connect;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Iterator;
import java.util.List;
import java.util.UUID;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import com.google.common.primitives.Bytes;
import com.resolve.rsbase.SessionObjectInterface;
import com.resolve.util.Log;

public abstract class Connect implements SessionObjectInterface, TimerEventListener
{
    final static int DEFAULT_EXPECT_TIMEOUT = 10;
    final static int DEFAULT_PROCESS_TIMEOUT = -1;
    
    protected String id;

    int options = 0;
    
    String content = "";
    String matchContent = "";
    String matchPattern = null;
    String postMatch = null;

    //List<Byte> bytes = new ArrayList<Byte>();
    //List<Byte> matchBytes = new ArrayList<Byte>();
    byte[] matchBytes = new byte[0];
    byte[] postBytes = new byte[0];

    
    // default values
    long expectTimeout = DEFAULT_EXPECT_TIMEOUT;
    protected boolean isClosed = true;
    volatile boolean continueReading = true;
    
    protected ConnectReader reader; // output consumer
    
    public void connect() throws Exception
    {
        isClosed = false;
        id = UUID.randomUUID().toString();
    } // connect
    
    public void close()
    {
        isClosed = true;
    } // close
    
    public void finalize() throws Throwable
    {
        this.close();
    } // finalize
    
    public void send() throws Exception
    {
        if (isClosed || reader == null) 
        {
            throw new Exception("WARN: Connection is not established");
        }
        
        this.clear();
    } // send
    
    public String read() throws Exception
    {
        if (isClosed || reader == null) 
        {
            throw new Exception("WARN: Connection is not established");
        }
        
        return reader.read();
    } // read

    public byte[] readByte() throws Exception
    {
        if (isClosed || reader == null) 
        {
            throw new Exception("WARN: Connection is not established");
        }
        
        return reader.readByte();
    } // read

    public void clear() throws Exception
    {
        reader.clear();
        matchContent = "";
        matchBytes = new byte[0];
    } // clear
    
    public String expect() throws Exception
    {
        return read();
    } // expect

    public String expect(String pattern) throws Exception
    {
        return expect(pattern, expectTimeout);
    } // expect

    public String expect(String pattern, long timeout) throws Exception
    {
        ArrayList patterns = new ArrayList();
        patterns.add(pattern);
        
        return expect(patterns, timeout);
    } // expect 
    
    public String expect(List patterns) throws Exception
    {
        return expect(patterns, expectTimeout);
    } // expect
    
    public String expect(List patterns, long timeout) throws Exception
    {
        String result = "";
        
        if (isClosed || reader == null) 
        {
            String msg = "WARN: Connection is not established. ";
            if (reader != null)
            {
                msg += reader.read();
            }
            throw new Exception(msg);
        }
        
        try
        {
            boolean found = false;
            
            // check that spawned process is running
            // set timer
            Timer tm = null;
            if (timeout != -1)
            {
                tm = new Timer(timeout, this);
                tm.startTimer();
                continueReading = true;
            }
                
            for (Iterator i=patterns.iterator(); !found && i.hasNext(); )
            {
	            // init pattern
                String regex = (String)i.next();
                Pattern pattern = Pattern.compile(regex, options);
                Matcher matcher = pattern.matcher(matchContent);
                matchPattern = regex;
    	            
	            // try to match remainder of matchContent
                if (matcher.find())
                {
                    found = true;
                    matchContent = matchContent.substring(matcher.end(), matchContent.length());
                }
            }
                
            while (continueReading && !found)
            {
                    
                // Sleeping if bytes are not available rather than blocking
                if (!reader.hasContent())
                {
                    Thread.sleep(500);
                }
                else
                {
                    String readContent = reader.read();
	                matchContent += readContent;
                    content += readContent;
                    Log.log.trace("matchContent: "+matchContent);
                    
                    // update result to matching content
                    result = matchContent;
                    
		            for (Iterator i=patterns.iterator(); i.hasNext() && !found; )
		            {
			            // init pattern
		                String regex = (String)i.next();
		                Pattern pattern = Pattern.compile(regex, options);
		                Matcher matcher = pattern.matcher(matchContent);
	                    matchPattern = regex;
	                    
	                    if (matcher.find())
		                {
		                    found = true;
		                    matchContent = matchContent.substring(matcher.end(), matchContent.length());
                            postMatch = matchContent;
                            
                            Log.log.trace("pattern: "+pattern);
                            Log.log.trace("postContent: "+postMatch);
		                }
                    }
                }
            }
                
            if (tm != null)
            {
                tm.setStatus(Timer.TIMER_COMPLETED);
            }
        }
        catch (Exception e)
        {
            Log.log.error("Connector expect exception: "+e.getMessage());
        }
        
        return result;
    } // expect

    public String expectByte() throws Exception
    {
        return read();
    } // expect

    public byte[] expect(byte[] pattern) throws Exception
    {
        return expect(pattern, expectTimeout);
    } // expect

    public byte[] expect(byte[] pattern, long timeout) throws Exception
    {
        byte[] result = null;
        
        if (isClosed || reader == null) 
        {
            String msg = "WARN: Connection is not established. ";
            throw new Exception(msg);
        }
        
        try
        {
            boolean found = false;
            
            // check that spawned process is running
            // set timer
            Timer tm = null;
            if (timeout != -1)
            {
                tm = new Timer(timeout, this);
                tm.startTimer();
                continueReading = true;
            }
            
            while (continueReading && !found)
            {
                // Sleeping if bytes are not available rather than blocking
                if (!reader.hasContent())
                {
                    Thread.sleep(500);
                }
                else
                {
                    //get the bytes from reader.
                    byte[] readContent = reader.readByte();
                    matchBytes = Bytes.concat(matchBytes, readContent);
                    
                    // update result to matching content
                    result = matchBytes;
                    
                    int location = Bytes.indexOf(matchBytes, pattern);
                    int patternLen = pattern.length;
                    
                    if(location >= 0)
                    {
                        found = true;
                        
                        matchBytes = Arrays.copyOfRange(matchBytes, location + patternLen + 1, matchBytes.length - 1);
                        postBytes = Arrays.copyOf(matchBytes, matchBytes.length);
                        Log.log.trace("Total bytes found : "+matchBytes.length);
                        Log.log.trace("postBytes length: "+postBytes.length);
                    }
                }
            }
                
            if (tm != null)
            {
                tm.setStatus(Timer.TIMER_COMPLETED);
            }
        }
        catch (Exception e)
        {
            Log.log.error("Connector expect exception: "+e.getMessage());
        }
        
        return result;
    } // expect

    /**
     * Timer callback method This method is invoked when the timeout occurs
     */
    public synchronized void timerTimedOut()
    {
        continueReading = false;
    } // timerTimedOut

    /**
     * Timer callback method This method is invoked by the Timer, when the timer
     * thread receives an interrupted exception
     */
    public void timerInterrupted(InterruptedException ioe)
    {
        continueReading = false;
    } // timerInterrupted

    /**
     * This method returns true if the last expect() or expectErr() method
     * returned because of a timeout rather then a match against the output of
     * the process.
     */
    public boolean isLastExpectTimeout()
    {
        return (!continueReading);
    } // isLastExpectTimeout

    public String getContent()
    {
        return content;
    } // getContent

    public String getPostMatch()
    {
        return postMatch;
    } // getPostMatch

    public String getMatchPattern()
    {
        return matchPattern;
    } // getMatchPattern
    
    public int getOptions()
    {
        return options;
    }
    
    public void setOptions(int options)
    {
        this.options = options;
    }

    public void sleep(long secs)
    {
        try
        {
            Thread.sleep(secs * 1000);
        }
        catch (Exception e) {};
    } // sleep
    
    public void setDefaultTimeout(long timeout)
    {
    	this.expectTimeout = timeout;
    } // setDefaultTimeout

    public boolean isClosed()
    {
        return isClosed;
    }

    public void setClosed(boolean isClosed)
    {
        this.isClosed = isClosed;
    }

    public String getId()
    {
        if(id==null)
        {
            //it's possible that a subclass forgot to call super() 
            //from its constructor.
            id = UUID.randomUUID().toString();
        }
        return id;
    }

    public void setId(String id)
    {
        this.id = id;
    }

    @Override
    public int hashCode()
    {
        final int prime = 31;
        int result = 1;
        result = prime * result + ((id == null) ? 0 : id.hashCode());
        return result;
    }

    @Override
    public boolean equals(Object obj)
    {
        if (this == obj)
        {
            return true;
        }
        if (obj == null)
        {
            return false;
        }
        if (!(obj instanceof Connect))
        {
            return false;
        }
        Connect other = (Connect) obj;
        if (id == null)
        {
            if (other.id != null)
            {
                return false;
            }
        }
        else if (!id.equals(other.id))
        {
            return false;
        }
        return true;
    }
} // Connect
