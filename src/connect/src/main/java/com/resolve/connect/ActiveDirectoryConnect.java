/******************************************************************************
* (C) Copyright 2014
*
* Resolve Systems, LLC
*
* All rights reserved
* This software is distributed under license and may not be used, copied,
* modified, or distributed without the express written permission of
* Resolve Systems, LLC.
*
******************************************************************************/

package com.resolve.connect;

import java.util.ArrayList;
import java.util.Enumeration;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import com.novell.ldap.LDAPAttribute;
import com.novell.ldap.LDAPAttributeSet;
import com.novell.ldap.LDAPConnection;
import com.novell.ldap.LDAPEntry;
import com.novell.ldap.LDAPException;
import com.novell.ldap.LDAPJSSESecureSocketFactory;
import com.novell.ldap.LDAPMessage;
import com.novell.ldap.LDAPMessageQueue;
import com.novell.ldap.LDAPSearchResults;
import com.novell.ldap.LDAPUnbindRequest;
import com.resolve.rsbase.SessionObjectInterface;
import com.resolve.util.Log;
import com.resolve.util.StringUtils;

/**
 * This class provides a wrapper API to communicate with an Active Directory (AD) server.
 *
 */
public class ActiveDirectoryConnect implements SessionObjectInterface
{
    //only supports version 3 at this point.
    private int version = LDAPConnection.LDAP_V3;

    private final String host;
    private final int port;
    private final boolean isSSL;
    private boolean isConnected;
    private LDAPConnection conn = null;

    /**
     * This constructor prepares an Unsecured LDAP connection using the default
     * LDAP server port 389.
     *
     * @param host LDAP server host name (DNS) or its IP address.
     * @throws ConnectException
     */
    public ActiveDirectoryConnect(final String host) throws ConnectException
    {
        this(host, 389);
    }

    /**
     * This constructor prepares an  Unsecured LDAP connection and connects to the server.
     *
     * @param host LDAP server host name (DNS) or its IP address.
     * @param port LDAP server port.
     */
    public ActiveDirectoryConnect(final String host, final int port) throws ConnectException
    {
        this(host, port, false);
    }

    /**
     * This constructor prepares a AD connection and connects to the server.
     *
     * @param host LDAP server host name (DNS) or its IP address.
     * @param port LDAP server port.
     * @param isSSL true if the server requires secured (LDAPS) connection.
     */
    public ActiveDirectoryConnect(final String host, final int port, final boolean isSSL) throws ConnectException
    {
        this.host = host;
        this.port = port;
        this.isSSL = isSSL;
        try
        {
            if (this.isSSL)
            {
                Log.log.debug("Creating Secure AD Connection Object");
                LDAPJSSESecureSocketFactory socketFactory = new LDAPJSSESecureSocketFactory();
                this.conn = new LDAPConnection(socketFactory);
            }
            else
            {
                conn = new LDAPConnection();
            }
        }
        catch (Exception e)
        {
            Log.log.error(e.getMessage(), e);
            throw new ConnectException(e.getMessage(), e);
        }
    }

    private boolean bind(String bindDN, String bindPassword, LDAPConnection lc, int ldapVersion)
    {
        boolean bound = false;

        if (bindDN != null && bindDN.length() > 0 && bindPassword != null)
        {
            try
            {
                Log.log.debug("ActiveDirectory Bind user: " + bindDN);
                lc.bind(ldapVersion, bindDN, bindPassword.getBytes("UTF8"));
                bound = true;
                Log.log.debug("ActiveDirectory Bind successful");
            }
            catch (Exception e)
            {
                Log.log.warn("ActiveDirectory Bind failed: " + e.getMessage());
            }
        }
        else
        {
            Log.log.debug("ActiveDirectory Bind does not have necessary info");
        }
        return bound;
    } // bind

    @Override
    public void close()
    {
        try
        {
            if (conn != null && conn.isConnected())
            {
                // explicitly send unbind message request - disconnect should automatically unbind,
                // but novell eDirectory need an explicit unbind on auth fails
                try
                {
                    if (!conn.isBound())
                    {
                        Log.log.debug("Explicitly sending LDAPUnbindRequest");
                        LDAPMessage unbindMsg = new LDAPUnbindRequest(null);
                        int msgId = unbindMsg.getMessageID();
                        LDAPMessageQueue responseQueue = conn.sendRequest(unbindMsg, null);
                        responseQueue.getResponse(msgId);
                    }
                }
                catch (LDAPException e)
                {
                    Log.log.error("Failed to unbind: " + e.getMessage(), e);
                }
                // disconnect
                Log.log.debug("Disconnecting LDAP connection");
                conn.disconnect();
            }
        }
        catch (LDAPException e)
        {
            Log.log.error("Failed to finilize connection: " + e.getMessage(), e);
        }
        finally
        {
            conn = null;
        }
    }

    @Override
    public void finalize()
    {
        this.close();
    }

    /**
     * This method can be called to connect to the LDAP server.
     *
     * @throws ConnectException
     */
    public void connect() throws ConnectException
    {
        try
        {
            // connect to ldap server
            Log.log.debug("Connecting to LDAP host " + host + " and port " + port);
            conn.connect(this.host, this.port);
            isConnected = true;
            Log.log.debug("LDAP Connected successfully to host " + host + " and port " + port);
        }
        catch (LDAPException e)
        {
            Log.log.error(e.getMessage(), e);
            throw new ConnectException(e.getMessage(), e);
        }
    }

    /**
     * This method verifies that if LDAP server is connected.
     *
     * <pre>
     * {@code
     *     import com.resolve.connect.ActiveDirectoryConnect;
     *     ...
     *     ...
     *     //ActiveDirectoryConnect connect = new ActiveDirectoryConnect("192.168.1.100");
     *     ActiveDirectoryConnect connect = new ActiveDirectoryConnect("192.168.1.100", 389);
     *     if(connect.isConnected())
     *     {
     *         System.out.println("Connected");
     *     }
     *     else
     *     {
     *         System.out.println("Not connected");
     *     }
     * }
     * </pre>
     *
     * @return
     * @throws ConnectException
     */
    public boolean isConnected() throws ConnectException
    {
        if (!isConnected)
        {
            connect();
        }
        return isConnected;
    }

    /**
     * This method searches the Active Directory (AD) server and returns the result in a {@link List} of {@link Map}. Use of
     * this method requires fairly good understanding of the AD server and its various configurations.
     *
     * <pre>
     * {@code
     *  import com.resolve.connect.ActiveDirectoryConnect;
     *  ...
     *  ...
     *  ActiveDirectoryConnect connect = null;
     *  try
     *  {
     *      connect = new ActiveDirectoryConnect("192.168.224.128", 389);
     *      List<Map<String, Object>> response = connect.search("ou=orgunit,dc=resolvetest-3,dc=com", 2, "(objectClass=*)", null, "resolvetest-3", "resolve", "xxxxx");
     *      //if successful response will have data retrieved from the search
     *      System.out.println(response.size());
     *  }
     *  catch (ConnectException e)
     *  {
     *      //do something about this exception
     *  }
     *  finally
     *  {
     *      if (connect != null)
     *      {
     *         connect.close();
     *      }
     *  }
     * }
     * </pre>
     *
     * @param baseDN The base distinguished name to search from
     * @param scope The scope of the entries to search. The following are the valid options:
     *  0 - searches only the base DN
     *  1 - searches only entries under the base DN
     *  2 - searches the base DN and all entries within its subtree
     * @param filter Search filter specifying the search criteria.
     * @param attributeNames Names of attributes to retrieve, leave it null for all attributes
     * @param domain name of the active directory domain.
     * @param username if necessary to perform this search.
     * @param password if necessary to perform this search.
     * @return
     * @throws ConnectException
     */
    public List<Map<String, Object>> search(final String baseDN, final int scope, final String filter, final String[] attributeNames, final String domain,  final String username, final String password) throws ConnectException
    {
        List<Map<String, Object>> result = new ArrayList<Map<String, Object>>();
        try
        {
            //first validate the parameters
            if (StringUtils.isBlank(baseDN))
            {
                throw new ConnectException("Invalid baseDN value, must not be empty or null.");
            }
            if (scope < 0 || scope > 2)
            {
                throw new ConnectException("Invalid scope value, must be 0, 1, or 2.");
            }
            if (isConnected())
            {
                /*LDAPSearchConstraints constraints = conn.getSearchConstraints();
                constraints.setReferralFollowing(true);
                constraints.setReferralHandler(sbh);
                conn.setConstraints(constraints);
                */
                boolean bind = bind(username + "@" + domain, password, conn, version);

                if (bind)
                {
                    LDAPSearchResults searchResults = conn.search(baseDN, scope, filter, attributeNames, false);
                    while (searchResults.hasMore())
                    {
                        Map<String, Object> map = new HashMap<String, Object>();

                        LDAPEntry entry = searchResults.next();

                        LDAPAttributeSet attributeSet = entry.getAttributeSet();

                        if (attributeSet != null)
                        {
                            Iterator<LDAPAttribute> allAttributes = attributeSet.iterator();

                            while (allAttributes.hasNext())
                            {
                                LDAPAttribute attribute = allAttributes.next();
                                String attributeName = attribute.getName();
                                Enumeration allValues = attribute.getStringValues();

                                StringBuilder attrValue = new StringBuilder();
                                if (allValues != null)
                                {
                                    while (allValues.hasMoreElements())
                                    {
                                        Object value = allValues.nextElement();
                                        if (value != null)
                                        {
                                            attrValue.append((String) value);
                                            attrValue.append(",");
                                        }
                                    }
                                    attrValue.setLength(attrValue.length() - 1);
                                }
                                map.put(attributeName, attrValue.toString());
                            }
                            result.add(map);
                        }
                    }
                }
            }
            else
            {
                throw new ConnectException("AD server cannot be connected, search failed.");
            }
        }
        catch (LDAPException e)
        {
            Log.log.error(e.getMessage(), e);
            throw new ConnectException(e.getMessage(), e);
        }
        return result;
    }
} // ActiveDirectoryConnect
