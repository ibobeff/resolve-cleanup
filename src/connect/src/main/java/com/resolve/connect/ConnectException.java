/******************************************************************************
* (C) Copyright 2014
*
* Resolve Systems, LLC
*
* All rights reserved
* This software is distributed under license and may not be used, copied,
* modified, or distributed without the express written permission of
* Resolve Systems, LLC.
*
******************************************************************************/

package com.resolve.connect;

@SuppressWarnings("serial")
public class ConnectException extends Exception
{
    private static final long serialVersionUID = -3177345833786337119L;

	public ConnectException(String message)
    {
        super(message);
    }

    public ConnectException(String message, Throwable t)
    {
        super(message, t);
    }
}
