/******************************************************************************
* (C) Copyright 2014
*
* Resolve Systems, LLC
*
* All rights reserved
* This software is distributed under license and may not be used, copied,
* modified, or distributed without the express written permission of
* Resolve Systems, LLC.
*
******************************************************************************/

package com.resolve.connect;

import wslite.soap.SOAPClient;
import wslite.soap.SOAPResponse;

import com.resolve.util.Log;

public class WSLiteConnect
{
    private final String uri; 

    private SOAPClient client;
    
    /**
     * Cunstructor with the URI of the web service.
     * 
     * @param uri like http://www.w3schools.com/webservices/tempconvert.asmx?WSDL
     */
    public WSLiteConnect(final String uri)
    {
        this.uri = uri;
    }

    /**
     * Initiates a connection to the web service.
     * 
     * @throws Exception
     */
    public void connect() throws Exception
    {
        /* Create a connection instance */
        try
        {
            client = new SOAPClient(uri);
        }
        catch (Exception e2)
        {
            Log.log.error("Cannot connect to an existing web service" + e2.getMessage(), e2);
            throw e2;
        }
    } // connect

    /**
     * Send SOAP envelope to the web service and receives the response.
     * 
     * <pre>
     * {@code
     *      import com.resolve.connect.WSLiteConnect;
     *      ......
     *      String resolveUrl = "http://localhost:8080/resolve/webservice/WebserviceListener?wsdl";
     *      String soap1 = "<soapenv:Envelope xmlns:soapenv=" + "\"http://schemas.xmlsoap.org/soap/envelope/\" "
     *          + "xmlns:xsd=\"http://www.resolve-systems.com/resolve/WebServiceListener/xsd\">\n";
     *      soap1 += "<soapenv:Header/>\n";
     *      soap1 += "<soapenv:Body><xsd:startRunbookWithLoginByString>\n";
     *      soap1 += "<!--Optional:-->\n";
     *      soap1 += "<xsd:username>CHANGE_IT</xsd:username>\n";
     *      soap1 += "<!--Optional:-->\n";
     *      soap1 += "<xsd:password>CHANGE_IT</xsd:password>\n";
     *      soap1 += "<xsd:params>RUNBOOK=test.runbook1</xsd:params>\n";
     *      soap1 += "<xsd:params>\"PROBLEMID\"=\"NEW\"</xsd:params>\n";
     *      soap1 += "</xsd:startRunbookWithLoginByString>\n";
     *      soap1 += "</soapenv:Body>\n";
     *      soap1 += "</soapenv:Envelope>";
     *              
     *      WSLiteConnect wsconnectTest1 = new WSLiteConnect(resolveUrl);
     *      wsconnectTest1.connect();
     *      String response1 = wsconnectTest1.send(soap1);
     *      System.out.println(response1);
     * }
     * </pre>
     * 
     * @param soapEnvelope a fully constructaed SOAP envelope as follows
     * <pre>
     * {@code 
     *  <soap:Envelope xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema" xmlns:soap="http://schemas.xmlsoap.org/soap/envelope/">
            <soap:Body>
                <CelsiusToFahrenheit xmlns="http://tempuri.org/">
                    <Celsius>200</Celsius>
                </CelsiusToFahrenheit>
            </soap:Body>
        </soap:Envelope>
     * }
     * </pre>
     * @return the response from the web service as follows:
     * <pre>
     * {@code 
        <soap:Envelope xmlns:soap="http://schemas.xmlsoap.org/soap/envelope/" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema">
            <soap:Body>
                <CelsiusToFahrenheitResponse xmlns="http://tempuri.org/">
                    <CelsiusToFahrenheitResult>392</CelsiusToFahrenheitResult>
                </CelsiusToFahrenheitResponse>
            </soap:Body>
       </soap:Envelope>
     * }
     * </pre>
     * @throws Exception
     */
    public String send(final String soapEnvelope) throws Exception
    {
        if(client == null)
        {
            client = new SOAPClient(uri);
        }
        
        // WebService Response
        String result = null;

        try
        {
            SOAPResponse response = client.send(soapEnvelope);
            result = response.getText();
            Log.log.trace(response);
            //System.out.println(response);
        }
        catch (Exception e)
        {
            Log.log.error("WSLiteConnect.java Caught Exception: " + e.getMessage(), e);
            throw e;
        }
        
        return result;
        
    } //send

     /***********************************
     * Description: Self Test
     ************************************/
    /*
    public static void main(String[] args) throws Exception
    {
        
        //init logging
        Log.init();

        String resolveUrl = "http://localhost:8888/resolve/webservice/WebserviceListener?wsdl";

        //Test Case 1:  XML to startRunbookWithLoginByString from SOAP UI + HTTP + WSDL pointing to resolve Web services
        String soap1 = "<soapenv:Envelope xmlns:soapenv=" + "\"http://schemas.xmlsoap.org/soap/envelope/\" "
                        + "xmlns:xsd=\"http://www.resolve-systems.com/resolve/WebServiceListener/xsd\">\n";
        soap1 += "<soapenv:Header/>\n";
        soap1 += "<soapenv:Body><xsd:startRunbookWithLoginByString>\n";
        soap1 += "<!--Optional:-->\n";
        soap1 += "<xsd:username>admin</xsd:username>\n";
        soap1 += "<!--Optional:-->\n";
        soap1 += "<xsd:password>resolve</xsd:password>\n";
        soap1 += "<xsd:params>RUNBOOK=bipul.test</xsd:params>\n";
        soap1 += "<xsd:params>\"PROBLEMID\"=\"NEW\"</xsd:params>\n";
        soap1 += "</xsd:startRunbookWithLoginByString>\n";
        soap1 += "</soapenv:Body>\n";
        soap1 += "</soapenv:Envelope>";
        
        
        // NOTE : for every new message that you want to send create a new url connection ELSE you get the "Cannot write output after reading input" error
        WSLiteConnect wsconnectTest1 = new WSLiteConnect(resolveUrl);
        wsconnectTest1.connect();
        String response1 = wsconnectTest1.send(soap1);
        System.out.println(response1);

        //Test Case 2: XML to executeRunbookWithLoginByString from SOAP UI + HTTP + WSDL pointing to resolve Web services
        String soap2 = "<soapenv:Envelope xmlns:soapenv=" + "\"http://schemas.xmlsoap.org/soap/envelope/\" "
                           + "xmlns:xsd=\"http://www.resolve-systems.com/resolve/WebServiceListener/xsd\">\n";
        soap2 += "<soapenv:Header/>\n";
        soap2 += "<soapenv:Body>\n";
        soap2 +=    "<xsd:executeRunbookWithLoginByString>\n";
        soap2 +=       "<!--Optional:-->\n";
        soap2 +=       "<xsd:username>" + "admin" + "</xsd:username>\n";
        soap2 +=       "<!--Optional:-->";
        soap2 +=      "<xsd:password>" + "resolve" + "</xsd:password>\n";
        soap2 +=       "<!--Optional:-->\n";
        soap2 +=       "<xsd:params>RUNBOOK=bipul.test</xsd:params>\n";
        soap2 +=    "</xsd:executeRunbookWithLoginByString>\n";
        soap2 += "</soapenv:Body>\n";
        soap2 += "</soapenv:Envelope>";
        
        String response2 = wsconnectTest1.send(soap2);
        System.out.println(response2);

        //Test Case 3: XML to create a simple Web Service that converts the temperature from Celsius to Fahrenheit and vice versa + WSDL pointing to www.w3schools.com
        String soap3 = "<soap:Envelope xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\"" +
        		" xmlns:xsd=\"http://www.w3.org/2001/XMLSchema\" xmlns:soap=\"http://schemas.xmlsoap.org/soap/envelope/\">\n";
        soap3 += "<soap:Body>\n";
        soap3 +=    "<CelsiusToFahrenheit xmlns=\"http://tempuri.org/\">\n";
        soap3 +=    "<Celsius>200</Celsius>\n";
        soap3 +=    "</CelsiusToFahrenheit>\n";
        soap3 +=  "</soap:Body>\n";
        soap3 += "</soap:Envelope>";
        
        String externalUrl = "http://www.w3schools.com/webservices/tempconvert.asmx?WSDL";
        
        // NOTE : for every new message that you want to send create a new url connection ELSE you get the "Cannot write output after reading input" error
        WSLiteConnect wsconnectTest3 = new WSLiteConnect(externalUrl);
        wsconnectTest3.connect();
        String response3 = wsconnectTest1.send(soap3);
        System.out.println(response3);
        
    } // Test Main
    */
}
