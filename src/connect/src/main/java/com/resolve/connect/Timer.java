/******************************************************************************
* (C) Copyright 2014
*
* Resolve Systems, LLC
*
* All rights reserved
* This software is distributed under license and may not be used, copied,
* modified, or distributed without the express written permission of
* Resolve Systems, LLC.
*
******************************************************************************/

package com.resolve.connect;

/**
 * This class acts like a timer and invokes the listener on time-out.
 */
public class Timer implements Runnable
{
    // The time interval in milliseconds up to which the process
    // should be allowed to run.
    private long timeOut = 0;

    // Timer event Listener
    private TimerEventListener listener = null;

    // Thread Object
    private Thread thread = null;

    // Timer Status
    public static final int TIMER_NOT_STARTED = 0;
    public static final int TIMER_STARTED = 1;
    public static final int TIMER_TIMEDOUT = 2;
    public static final int TIMER_INTERRUPTED = 3;
    public static final int TIMER_COMPLETED = 4;

    // Stores the current status of Timer
    private int currentStatus = TIMER_NOT_STARTED;

    /**
     * Constructor
     * 
     * @param timeOut
     *            Time interval after which the listener will be invoked
     * @param listener
     *            Object implementing the TimerEventListener interface
     */
    public Timer(long timeOut, TimerEventListener listener)
    {
        if (timeOut < 1)
        {
            throw new IllegalArgumentException("Time-Out value cannot be < 1");
        }
        if (listener == null)
        {
            throw new IllegalArgumentException("Listener cannot be null");
        }
        this.timeOut = timeOut * 1000;
        this.listener = listener;
    } // Timer

    /**
     * Starts the timer
     */
    public void startTimer()
    {

        thread = new Thread(this);
        currentStatus = TIMER_STARTED;
        
        thread.setDaemon(true);
        thread.start();
    } // startTimer

    /**
     * This method returns the status of the timer
     * 
     * NOT_STARTED = 0; STARTED = 1; TIMEDOUT = 2; INTERRUPTED = 3; COMPLETED = 4;
     * 
     */
    public int getStatus()
    {
        return currentStatus;
    } // getStatus

    public void setStatus(int status)
    {
        this.currentStatus = status;
    } // setStatus

    // Thread method
    public void run()
    {
        try
        {
            Thread.sleep(timeOut);
            
            if (currentStatus == TIMER_STARTED)
            {
	            currentStatus = TIMER_TIMEDOUT;
	            listener.timerTimedOut();
            }
        }
        catch (InterruptedException iexp)
        {
            currentStatus = TIMER_INTERRUPTED;
            listener.timerInterrupted(iexp);
        }
    } // run
    
} // Timer
