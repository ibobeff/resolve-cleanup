/******************************************************************************
* (C) Copyright 2014
*
* Resolve Systems, LLC
*
* All rights reserved
* This software is distributed under license and may not be used, copied,
* modified, or distributed without the express written permission of
* Resolve Systems, LLC.
*
******************************************************************************/

package com.resolve.connect;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;

import java.net.HttpURLConnection;
import java.net.URI;
import java.net.URL;
import java.net.URLConnection;
import java.net.URLEncoder;
import java.nio.charset.StandardCharsets;

import javax.net.ssl.HttpsURLConnection;

import java.util.Hashtable;
import java.util.Map;

import org.apache.commons.lang3.StringUtils;

import com.resolve.util.Log;

/**
 * HTTPConnect is a low level API to send messages to a URL, and retrieve the response
 * @author justin.geiser
 *
 */
public class HTTPConnect extends Connect
{
    final static int DEFAULT_HTTP_CONNECTION_TIMEOUT = 10;
    final static int DEFAULT_HTTP_REQUEST_TIMEOUT = 100;
    final static String DEFAULT_CONTENT_TYPE = "text/xml; charset=utf-8";
    
    String uri;
    String message;
    Map<String, String> properties;
    
    String protocol;
    String host;
    String port;
    int timeout;

    public HTTPConnect() throws Exception
    {
        this.properties = new Hashtable<String, String>();
        addProperty("Content-Type", "text/xml; charset=utf-8");
        this.uri = "http://localhost";
        this.message = "";
        
        reader = new WebReader();
        super.connect();
    }//base constructor, default type is text/xml
    
    public HTTPConnect(Map<String, String> properties) throws Exception
    {
        this.properties = properties;
        if(properties.get("Content-Type") ==  null)
        {
            addProperty("Content-Type", "text/xml; charset=utf-8");
        }
        this.uri = "http://localhost:";
        this.message = "";
        
        reader = new WebReader();
        super.connect();
    }//constructor with default properties
    
    public HTTPConnect(String protocol, String host, String port, Integer timeout) throws Exception {
        
        if(StringUtils.isBlank(host))
            throw new Exception("Invalid host.");
        
        this.protocol = protocol;
        
        this.host = host;
      
        this.port = port;
       
        if(timeout == null || timeout.intValue() == 0)
            timeout = DEFAULT_HTTP_CONNECTION_TIMEOUT;
        
        this.timeout = timeout*1000;

//        String url = getValidUrl(host);
//        if(!url.contains("https"))
//            HttpPing();

        super.connect();
    }
    
    /**
     * Creates a connection to a URL, gets response
     * @param uri URL to connect to 
     */
    public void send(String uri)
    {
        send(uri, null, null);
    }//used to get a response from a uri with no message
    
    /**
     * Send a message to a URL
     * @param uri Address to send message to
     * @param message Message to send
     */
    public void send(String uri, String message)
    {
        send(uri, message, null);
    }//send a message to a url
    
    /**
     * Send a message and request properties to a URL. Sets values then calls send().
     * @param uri Address to send message to
     * @param message Message to send
     * @param properties Connection request properties
     */
    public void send(String uri, String message, Map<String, String> properties)
    {
        try{
            //set uri if it is formatted correctly
            if (uri.charAt(0) == '/')
            {
                setUri(uri);
            }
            else if (uri.startsWith("www"))
            {
                setUri("http://"+uri);
            }
            else if (uri.startsWith("http"))
            {
                setUri(uri);
            }
            if(message == null)
            {
                message = "";
            }
            message = message.trim();
            setMessage(message);
            //add any additional properties passed
            if(properties != null)
            {
                for(String key : properties.keySet())
                {
                    addProperty(key, properties.get(key));
                }
            }
            send();
        }
        catch(Exception e)
        {
            Log.log.error("HTTPConnect send fail: " + e.getMessage(), e);
        }
    }//set uri, message, and properties, then send message
    
    /**
     * Send the set message to the set uri with the set request properties
     */
    public void send()
    {
        try
        {
	        URL url = new URL(uri);
	        Log.log.debug("Opening Connection: " + uri);
	        URLConnection conn = url.openConnection();
	        conn.setDoOutput(true);
	        if (properties != null)
	        {
	            for (String key : properties.keySet())
	            {
	                String value = properties.get(key);
	                Log.log.trace("Adding Request Property - " + key + ":" + value);
	                conn.addRequestProperty(key, value);
	            }
	        }
	        if(Log.log.isTraceEnabled())
	        {
	            Log.log.trace("Adding Request Property - Content-Length:" + message.length());
	        }
	        conn.addRequestProperty("Content-Length", Integer.toString(message.length()));
	        //only send a message if there is a message to send
	        if (message != null && !message.equals(""))
	        {
	            Log.log.debug("Sending Message: " + message);
	            BufferedWriter writer = new BufferedWriter(new OutputStreamWriter(conn.getOutputStream(), "UTF8"));
	            writer.write(message);
	            writer.flush();
	            writer.close();
	        }
	
	        Log.log.debug("Getting response");
	        StringBuilder response = new StringBuilder("HTTP Response \n");
	        BufferedReader br = new BufferedReader(new InputStreamReader(conn.getInputStream()));
	        
	        String line = null;
	        while ((line = br.readLine()) != null) {
	            response.append(line);
	        }
	        br.close();
	        
	        Log.log.debug("HTTPConnect Response: " + response.toString());

	        ((WebReader)reader).set(response.toString());
        }
        catch(Exception e)
        {
            Log.log.error("HTTPConnect send fail: "+e.getMessage(), e); 
        }
    }//send a message to a uri, get a response

    /**
     * Get the uri
     * @return
     */
    public String getUri()
    {
        return uri;
    }

    /**
     * Set the uri
     * @param uri
     */
    public void setUri(String uri)
    {
        this.uri = uri;
    }

    /**
     * Get the message to send
     * @return
     */
    public String getMessage()
    {
        return message;
    }

    /**
     * Set the message to send
     * @param message
     */
    public void setMessage(String message)
    {
        this.message = message;
    }

    /**
     * Get the request properties
     * @return
     */
    public Map<String, String> getProperties()
    {
        return properties;
    }

    /**
     * Set the request properties
     * @param properties
     */
    public void setProperties(Map<String, String> properties)
    {
        this.properties = properties;
    }
    
    /**
     * Add a property to the request properties
     * @param key name of the property to add
     * @param value value of the property to add
     */
    public void addProperty(String key, String value)
    {
        properties.put(key, value);
    }
    
    /**
     * Get the value of a request property
     * @param key name of the property to get
     * @return
     */
    public String getProperty(String key)
    {
        return properties.get(key);
    }
    
    public  void HttpPing() throws Exception {
        
        HttpURLConnection conn = null;
        
        String urlStr = getValidUrl("");
        
        Log.log.trace("urlStr = " + urlStr);
        
        try {
            URL url = new URL(urlStr);
            
            if(url.toString().contains("https"))
                conn = (HttpsURLConnection) url.openConnection();
            else
                conn = (HttpURLConnection) url.openConnection();
            
            conn.setConnectTimeout(timeout);
            conn.setReadTimeout(timeout);
            conn.setRequestMethod("HEAD");
            conn.setUseCaches(false);
            
            Log.log.trace("timeout = " + timeout);
            
            int code = conn.getResponseCode();
            
            Log.log.debug("HTTP HEAD request to " + urlStr + " returned code = " + code);
            
            // Any response back from server should be treated as valid ping response
            //if(200 < code && code <= 399)
            //    throw new Exception("HTTP connection failed for code: " + code);
        } catch (Exception e) {
            Log.log.error("HTTP Connection failed for connecting to: " + urlStr);
            Log.log.error(e.getMessage());
            throw new Exception("HTTP Connection failed for connecting to: " + urlStr);
        } finally {
            if (conn != null) {
                BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(conn.getInputStream()));
                
                String line = null;
                while ((line = bufferedReader.readLine()) != null)
                {
                    Log.log.debug("HTTP HEAD response line: " + line);
                }

                bufferedReader.close();
                
                conn.disconnect();
            }
        }
    }
    
    public String sendGet(String commandUrl, String headers, Integer timeout) throws Exception {
        
        if(StringUtils.isBlank(commandUrl))
            throw new Exception("URL is missing.");
        
        HttpURLConnection conn = null;
        BufferedReader bufferedReader = null;
        StringBuilder response = new StringBuilder();
        
        try {
            if(!commandUrl.startsWith("http"))
                commandUrl = getValidUrl(commandUrl);
            
            Log.log.debug("commandUrl = " + commandUrl);
            
            URL url = new URL(commandUrl);
            
            URI uri = new URI(url.getProtocol(), url.getUserInfo(), url.getHost(), url.getPort(), url.getPath(), url.getQuery(), url.getRef());
            
            String urlStr = uri.toASCIIString();
            
            url = new URL(urlStr);

            if(url.toString().contains("https"))
                conn = (HttpsURLConnection) url.openConnection();
            else
                conn = (HttpURLConnection) url.openConnection();
            conn.setRequestMethod("GET");
            
            if(timeout != null)
                conn.setConnectTimeout(timeout.intValue() * 1000);
            else
                conn.setConnectTimeout(DEFAULT_HTTP_REQUEST_TIMEOUT * 1000);
            
            if(StringUtils.isNotBlank(headers)) {
                String[] headerList = headers.split("\\|");
                for(int i=0; i<headerList.length; i++) {
                    String header = headerList[i];
                    if (!header.contains(":")) {
                        Log.log.warn("Discarding invalid HTTP Header: '" + header + "'. It should have key-value seperated by a colon, ':'");
                        continue;
                    }
                    String[] keyValue = header.split(":");
                    conn.addRequestProperty(keyValue[0],  keyValue[1]);
                }
            }
            
            bufferedReader = new BufferedReader(new InputStreamReader(conn.getInputStream()));
            
            String line = null;
            while ((line = bufferedReader.readLine()) != null)
                response.append(line);

            bufferedReader.close();
            
            Log.log.debug("HTTPConnect Response: " + response);
//            ((WebReader)reader).set(response);
        } catch(Exception e) {
            Log.log.error(e.getMessage());
            throw e;
        } finally {
            if(bufferedReader != null)
                bufferedReader.close();;
            if(conn != null)
                conn.disconnect();
        }
        
        return response.toString();
    }
   
    public String sendPost(String commandUrl, String headers, String body, Integer timeout) throws Exception {
        
        String contentType = DEFAULT_CONTENT_TYPE;
        HttpURLConnection conn = null;
        
        BufferedReader bufferedReader = null;
        BufferedWriter writer = null;

        StringBuilder response = new StringBuilder();
        
        try {
            if(!commandUrl.startsWith("http"))
                commandUrl = getValidUrl(commandUrl);
            
            Log.log.debug("commandUrl = " + commandUrl);
            
            URL url = new URL(commandUrl);
            
            URI uri = new URI(url.getProtocol(), url.getUserInfo(), url.getHost(), url.getPort(), url.getPath(), url.getQuery(), url.getRef());
            
            String urlStr = uri.toASCIIString();
            
            url = new URL(urlStr);
            
            if(url.toString().contains("https"))
                conn = (HttpsURLConnection) url.openConnection();
            else
                conn = (HttpURLConnection) url.openConnection();
            
            conn.setRequestMethod("POST");
            conn.setDoOutput(true);
            
            if(timeout != null)
                conn.setConnectTimeout(timeout.intValue() * 1000);
            else
                conn.setConnectTimeout(DEFAULT_HTTP_REQUEST_TIMEOUT * 1000);
            
            Log.log.debug("post: commandUrl = " + commandUrl);
            
            if(StringUtils.isNotBlank(headers)) {
                String[] headerList = headers.split("\\|");
                for(int i=0; i<headerList.length; i++) {
                    String header = headerList[i];
                    String[] keyValue = header.split(":");
                    String key = keyValue[0];
                    String value = keyValue[1];
                    if(StringUtils.isNotBlank(key)) {
                        conn.addRequestProperty(key,  value);
                        if(key.equalsIgnoreCase("Content-Type"))
                            contentType = value;
                    }
                }
            }

            if(StringUtils.isNoneBlank(body)) {
                writer = new BufferedWriter(new OutputStreamWriter(conn.getOutputStream(), StandardCharsets.UTF_8.name()));
                writer.write(body);
                writer.flush();
            }
            
            bufferedReader = new BufferedReader(new InputStreamReader(conn.getInputStream()));
            
            String line = null;
            while ((line = bufferedReader.readLine()) != null)
                response.append(line);

            bufferedReader.close();

            Log.log.debug("HTTPConnect Response: " + response);
        } catch(Exception e) {
            Log.log.error(e.getMessage());
            throw e;
        } finally {
            if(writer != null)
                writer.close();
            if(bufferedReader != null)
                bufferedReader.close();
            if(conn != null)
                conn.disconnect();
        }
        
        return response.toString();
    }
    
    private String getValidUrl(String uri) {

        StringBuilder sb = new StringBuilder();
        
        if(!uri.startsWith("http")) {
            if(!host.startsWith("http"))
                sb.append(this.protocol).append("://");
            sb.append(this.host);
            
            if(StringUtils.isNotBlank(port))
                sb.append(":").append(port);
        }
        
        if(!host.endsWith("/") && !uri.startsWith("/"))
            sb.append("/");
  
        sb.append(uri);
            
        return sb.toString();
    }
    
    private String encodeUrl(String commandUrl) {
        
        String encodedUrl = commandUrl;
        
        int index = commandUrl.indexOf("?");
        if(index == -1)
            return commandUrl;
        
        String query = commandUrl.substring(index+1);
        String uri = commandUrl.substring(0, index+1);
        StringBuilder sb = new StringBuilder(uri);
        
        if(query.length() == 0)
            return commandUrl;
        
        String[] keyValues = query.split("&");
        
        for(int i=0; i<keyValues.length; i++) {
            String[] keyValue = keyValues[i].split("=");
            if(keyValue.length < 2)
                continue;
            
            String key = keyValue[0];
            String value = keyValue[1];
            if(key.length() == 0 || value.length() ==0)
                continue;
            
            try {
                sb.append(key).append("=").append(URLEncoder.encode(value, "UTF-8").replaceAll("\\+", "%20")).append("&");
            } catch(Exception e) {
                Log.log.error(e.getMessage(), e);
            }
        }
        
        encodedUrl = sb.toString();
        encodedUrl = encodedUrl.substring(0, encodedUrl.length()-1);
        
        return encodedUrl;
    }
    
} // HTTPConnection
