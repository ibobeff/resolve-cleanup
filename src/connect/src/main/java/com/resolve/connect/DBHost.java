/******************************************************************************
* (C) Copyright 2014
*
* Resolve Systems, LLC
*
* All rights reserved
* This software is distributed under license and may not be used, copied,
* modified, or distributed without the express written permission of
* Resolve Systems, LLC.
*
******************************************************************************/

package com.resolve.connect;

/**
 * This class is used during parsing host name passed by a *DBConnect consumer.
 */
public class DBHost
{
    public String host;
    public int port;
    
    public DBHost(String hostName, int defaultPort)
    {
        host=hostName;
        port=defaultPort;
    }
}
