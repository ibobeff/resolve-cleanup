/******************************************************************************
* (C) Copyright 2014
*
* Resolve Systems, LLC
*
* All rights reserved
* This software is distributed under license and may not be used, copied,
* modified, or distributed without the express written permission of
* Resolve Systems, LLC.
*
******************************************************************************/

package com.resolve.connect;

import com.resolve.util.Constants;

/**
 * Helper class used to generate Oracle JDBC URLs and the name of the Driver class used by Resolve.
 * <br>URLs are in the form of jdbc:oracle:thin{@literal @}<i>hostname</i>:<i>port</i>:<i>dbname</i>
 * @author justin.geiser
 *
 */
public class OracleDBConnect
{
    /**
     * Uses the hostname and dbname to generate the JDBC URL.  The port used will be 1521 unless the hostname if formated like <i>hostname</i>:<i>port</i>
     * @param hostname
     * @param instance
     * @return
     */
    public static String getConnectURL(String hostname, String instance)
    {
        DBHost dbHost = DBConnectHelper.parseHost(hostname, 1521);
        return getConnectURL(dbHost.host, dbHost.port, instance);
    } // getConnectURL
    
    /**
     * Uses the hostname, port, and dbname to generate the JDBC URL.
     * @param hostname
     * @param port
     * @param instance
     * @return
     */
    public static String getConnectURL(String hostname, int port, String instance)
    {
        return "jdbc:oracle:thin:@"+hostname+":"+port+":"+instance;
    } // getConnectURL
    
    /**
     * Uses the hostname, port, and dbname to generate the JDBC URL.
     * @param hostname
     * @param port
     * @param instance
     * @return
     */
    public static String getConnectURL(String hostname, String port, String instance)
    {
        return "jdbc:oracle:thin:@"+hostname+":"+port+":"+instance;
    } // getConnectURL
    
    /**
     * Returns the name of the Oracle driver shipped with Resolve
     * @return
     */
    public static String getDriver()
    {
        return Constants.ORACLEDRIVER;
    } // getDriver
    
} // OracleDBConnect
