package com.resolve.connect;

import java.io.Closeable;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.security.PublicKey;
import java.security.Security;
import java.util.Collections;
import java.util.List;

import org.apache.commons.io.IOUtils;
import org.bouncycastle.jce.provider.BouncyCastleProvider;

import com.resolve.util.Log;
import com.resolve.util.StringUtils;

import net.schmizz.keepalive.KeepAliveProvider;
import net.schmizz.sshj.DefaultConfig;
import net.schmizz.sshj.SSHClient;
import net.schmizz.sshj.connection.channel.direct.PTYMode;
import net.schmizz.sshj.connection.channel.direct.Session.Shell;
import net.schmizz.sshj.transport.TransportException;
import net.schmizz.sshj.transport.verification.HostKeyVerifier;
import net.schmizz.sshj.userauth.UserAuth;
import net.schmizz.sshj.userauth.keyprovider.KeyProvider;
import net.schmizz.sshj.userauth.method.AuthKeyboardInteractive;
import net.schmizz.sshj.userauth.method.AuthNone;
import net.schmizz.sshj.userauth.method.ChallengeResponseProvider;
import net.schmizz.sshj.userauth.password.Resource;
import net.schmizz.sshj.xfer.FileSystemFile;
import net.schmizz.sshj.xfer.InMemorySourceFile;
import net.schmizz.sshj.xfer.scp.SCPFileTransfer;
 
 
public class SSHConnectSSHJ extends Connect implements Closeable
{

    
    static {
        
        Security.addProvider(new BouncyCastleProvider());
        
    }
    public final static String P_ASSWORD = "PASSWORD";
    public final static String PUBLICKEY = "PUBLICKEY";
    public final static String INTERACTIVE = "INTERACTIVE";
    public final static String NONE = "NONE";
    public String authMethod = P_ASSWORD;  // password, publickey, interactive

    // auth fields
    protected String username;
    protected String password;
    protected File privateKeyFile;
    protected String privateKeyPassphrase;
    protected List responses;
   
    protected String hostname;    // destination host
    protected int port =22;           // destination port
    protected int timeout;        // connect timeout
    protected int keepAliveInterval; // interval at which keep alives are sent
 

    protected String prompt;      // Prompt
    private static DefaultConfig customConfig;
    private SSHClient client;
    private net.schmizz.sshj.connection.channel.direct.Session session;
    private Shell shell; 
    public SSHConnectSSHJ()
    {
        super();
    }
    /*
     * USERNAME/PASSWORD
     */
    public SSHConnectSSHJ(String username, String password, String hostname) throws Exception
    {
        this(username, password, hostname, 22, 0);
    } // SSHConnectSSHJ
    
    public SSHConnectSSHJ(String username, String password, String hostname, String prompt, boolean userNamePasswdLogin) throws Exception
    {
        this(username, password, hostname, 22, 0, prompt);
    } // SSHConnectSSHJ

    public SSHConnectSSHJ(String username, String password, String hostname, int port) throws Exception
    {
        this(username, password, hostname, port, 0);
    } // SSHConnectSSHJ
    
    public SSHConnectSSHJ(String username, String password, String hostname, int port, String prompt) throws Exception
    {
        this(username, password, hostname, port, 0, prompt);
    } // SSHConnectSSHJ

    public SSHConnectSSHJ(String username, String password, String hostname, int port, int timeout) throws Exception
    {
        this(username, password, hostname, port, timeout, true);
    } // SSHConnectSSHJ

    public SSHConnectSSHJ(String username, String password, String hostname, int port, int timeout, int keepAliveInterval) throws Exception
    {
        this(username, password, hostname, port, timeout, keepAliveInterval, true);
    } // SSHConnectSSHJ
    
    public SSHConnectSSHJ(String username, String password, String hostname, int port, int timeout, String prompt) throws Exception
    {
        this(username, password, hostname, port, timeout, -1, true, prompt);
    } // SSHConnectSSHJ
    
    public SSHConnectSSHJ(String username, String password, String hostname, int port, int timeout, int keepAliveInterval, String prompt) throws Exception
    {
        this(username, password, hostname, port, timeout, keepAliveInterval, true, prompt);
    } // SSHConnectSSHJ
    
    public SSHConnectSSHJ(String username, String password, String hostname, int port, int timeout, boolean isConnect) throws Exception
    {
        this(username, password, hostname, port, timeout, -1, isConnect, null);
    } // SSHConnectSSHJ
    
    public SSHConnectSSHJ(String username, String password, String hostname, int port, int timeout, int keepAliveInterval, boolean isConnect) throws Exception
    {
        this(username, password, hostname, port, timeout, keepAliveInterval, isConnect, null);
    } // SSHConnectSSHJ
    
    public SSHConnectSSHJ(String username, String password, String hostname, int port, int timeout, int keepAliveInterval,boolean isConnect, String prompt) throws Exception
    {
        super();

        authMethod = P_ASSWORD;

        this.username = username;
        this.password = password;
        this.hostname = hostname;
        this.port = port;
        this.timeout = timeout * 1000;
        this.prompt = prompt;
        this.keepAliveInterval = keepAliveInterval;

        if(isConnect)
        {
            connect();
        }
    } // SSHConnectSSHJ

    /*
     * PUBLIC KEY
     */
    public SSHConnectSSHJ(String username, String filename, String passphrase, String hostname) throws Exception
    {
        this(username, filename, passphrase, hostname, 22, 0);
    } // SSHConnectSSHJ
    
    public SSHConnectSSHJ(String username, String filename, String passphrase, String hostname, String prompt) throws Exception
    {
        this(username, filename, passphrase, hostname, 22, 0, prompt);
    } // SSHConnectSSHJ

    public SSHConnectSSHJ(String username, String filename, String passphrase, String hostname, int port) throws Exception
    {
        this(username, filename, passphrase, hostname, port, 0);
    } // SSHConnectSSHJ
    
    public SSHConnectSSHJ(String username, String filename, String passphrase, String hostname, int port, String prompt) throws Exception
    {
        this(username, filename, passphrase, hostname, port, 0, prompt);
    } // SSHConnectSSHJ

    public SSHConnectSSHJ(String username, String filename, String passphrase, String hostname, int port, int timeout) throws Exception
    {
        this(username, filename, passphrase, hostname, port, timeout, -1,true, null);
    } // SSHConnectSSHJ
    
    public SSHConnectSSHJ(String username, String filename, String passphrase, String hostname, int port, int timeout, int keepAliveInterval) throws Exception
    {
        this(username, filename, passphrase, hostname, port, timeout, keepAliveInterval, true, null);
    } // SSHConnectSSHJ
    
    public SSHConnectSSHJ(String username, String filename, String passphrase, String hostname, int port, int timeout, String prompt) throws Exception
    {
        this(username, filename, passphrase, hostname, port, timeout, -1, true, prompt);
    } // SSHConnectSSHJ
    
    public SSHConnectSSHJ(String username, String filename, String passphrase, String hostname, int port, int timeout, int keepAliveInterval, String prompt) throws Exception
    {
        this(username, filename, passphrase, hostname, port, timeout, keepAliveInterval, true, prompt);
    } // SSHConnectSSHJ
    
    public SSHConnectSSHJ(String username, String filename, String passphrase, String hostname, int port, int timeout, boolean isConnect) throws Exception
    {
        this(username, filename, passphrase, hostname, port, timeout, -1, isConnect, null);
    } // SSHConnectSSHJ

    public SSHConnectSSHJ(String username, String filename, String passphrase, String hostname, int port, int timeout, int keepAliveInterval, boolean isConnect) throws Exception
    {
        this(username, filename, passphrase, hostname, port, timeout, keepAliveInterval, isConnect, null);
    } // SSHConnectSSHJ
    
    public SSHConnectSSHJ(String username, String filename, String passphrase, String hostname, int port, int timeout, int keepAliveInterval,boolean isConnect, String prompt) throws Exception
    {
        authMethod = PUBLICKEY;

        File file = new File(filename);
        if (!file.exists())
        {
            throw new Exception("Unable to find private key file: "+file.getAbsolutePath());
        }

        this.username = username;
        this.privateKeyFile = file;
        this.privateKeyPassphrase = passphrase;
        this.hostname = hostname;
        this.port = port;
        this.timeout = timeout * 1000;
        this.prompt = prompt;
        this.keepAliveInterval = keepAliveInterval;

        if(isConnect)
        {
            connect();
        }
    } // SSHConnectSSHJ

    /*
     * INTERACTIVE
     */
    public SSHConnectSSHJ(String username, List responses, String hostname) throws Exception
    {
        this(username, responses, hostname, 22);
    } // SSHConnectSSHJ
    
    public SSHConnectSSHJ(String username, List responses, String hostname, String prompt) throws Exception
    {
        this(username, responses, hostname, 22, prompt);
    } // SSHConnectSSHJ

    public SSHConnectSSHJ(String username, List responses, String hostname, int port) throws Exception
    {
        this(username, responses, hostname, port, 0);
    } // SSHConnectSSHJ
    
    public SSHConnectSSHJ(String username, List responses, String hostname, int port, String prompt) throws Exception
    {
        this(username, responses, hostname, port, 0, prompt);
    } // SSHConnectSSHJ

    public SSHConnectSSHJ(String username, List responses, String hostname, int port, int timeout) throws Exception
    {
        this(username, responses, hostname, port, timeout, true);
    } // SSHConnectSSHJ
    
    public SSHConnectSSHJ(String username, List responses, String hostname, int port, int timeout, int keepAliveInterval) throws Exception
    {
        this(username, responses, hostname, port, timeout, keepAliveInterval, true);
    } // SSHConnectSSHJ
    
    public SSHConnectSSHJ(String username, List responses, String hostname, int port, int timeout, String prompt) throws Exception
    {
        this(username, responses, hostname, port, timeout, -1, true, prompt);
    } // SSHConnectSSHJ
    
    public SSHConnectSSHJ(String username, List responses, String hostname, int port, int timeout, int keepAliveInterval, String prompt) throws Exception
    {
        this(username, responses, hostname, port, timeout, keepAliveInterval, true, prompt);
    } // SSHConnectSSHJ
    
    public SSHConnectSSHJ(String username, List responses, String hostname, int port, int timeout, boolean isConnect) throws Exception
    {
        this(username, responses, hostname, port, timeout, -1, true, null);
    } // SSHConnectSSHJ
    
    public SSHConnectSSHJ(String username, List responses, String hostname, int port, int timeout, int keepAliveInterval, boolean isConnect) throws Exception
    {
        this(username, responses, hostname, port, timeout, keepAliveInterval, true, null);
    } // SSHConnectSSHJ

    public SSHConnectSSHJ(String username, List responses, String hostname, int port, int timeout, int keepAliveInterval, boolean isConnect, String prompt) throws Exception
    {
        authMethod = INTERACTIVE;

        this.username = username;
        this.responses = responses;
        this.hostname = hostname;
        this.port = port;
        this.timeout = timeout * 1000;
        this.prompt = prompt;
        this.keepAliveInterval = keepAliveInterval;

        if(isConnect)
        {
            connect();
        }
    } // SSHConnectSSHJ

    /*
     * NONE - no SSH authentication
     */
    public SSHConnectSSHJ(String username, String hostname) throws Exception
    {
        this(username, hostname, 22, 0);
    } // SSHConnectSSHJ

    public SSHConnectSSHJ(String username, String hostname, int port) throws Exception
    {
        this(username, hostname, port, 0);
    } // SSHConnectSSHJ

    public SSHConnectSSHJ(String username, String hostname, int port, int timeout) throws Exception
    {
        this(username, hostname, port, timeout, true);
    } // SSHConnectSSHJ

    public SSHConnectSSHJ(String username, String hostname, int port, int timeout, int keepAliveInterval) throws Exception
    {
        this(username, hostname, port, timeout, true);
    } // SSHConnectSSHJ
    
    public SSHConnectSSHJ(String username, String hostname, int port, int timeout, boolean isConnect) throws Exception
    {
        authMethod = NONE;

        this.username = username;
        this.password = "";
        this.hostname = hostname;
        this.port = port;
        this.timeout = timeout * 1000;
        this.keepAliveInterval = keepAliveInterval;

        if(isConnect)
        {
            connect();
        }
    } // SSHConnectSSHJ
    
    public static String getAuthMethods(String username, String hostname) throws Exception
    {
        return getAuthMethods(username, hostname, 22, 10);
    } //getAuthMethods

    public static String getAuthMethods(String username, String hostname, int port) throws Exception
    {
        return getAuthMethods(username, hostname, port, 10);
    } //getAuthMethods

    public static String getAuthMethods(String username, String hostname, int port, int timeout) throws Exception
    {
        
        Log.log.debug("SSHConnect to "+hostname+":"+port);

        SSHClient sc = new SSHClient();
        sc.addHostKeyVerifier(new HostKeyVerifier()
        {
            
            @Override
            public boolean verify(String arg0, int arg1, PublicKey arg2)
            {
               
                return true;
            }
        });
        sc.setTimeout(timeout);
        sc.setConnectTimeout(timeout);
        try {
            Log.log.trace("  connecting");

            sc.connect(hostname, port);
            
            UserAuth u = sc.getUserAuth();

            try{
                  sc.auth(username, new AuthNone());
            }catch(Exception e)
            {
                //ignore.  Unsuccesful authentication attempt needs to be made first.  
            }
            String out =  org.apache.commons.lang.StringUtils.join(u.getAllowedMethods().iterator(), ",");
            if(Log.log.isTraceEnabled())
            {
                Log.log.trace("Available Authentication Methods: "+out);
            }
        
            return out;
        }finally {
            
        
            sc.close();
        }   
        
       
    } //getAuthMethods

    public static boolean isInteractive(String authMethods)
    {
        boolean result = false;
        if (authMethods != null)
        {
            result = org.apache.commons.lang.StringUtils.containsIgnoreCase(authMethods, "keyboard-interactive");
        }
        return result;
    } //isInteractive
    
    
    public static boolean isPassword(String authMethods)
    {
        boolean result = false;
        if (authMethods != null)
        {
            result = org.apache.commons.lang.StringUtils.containsIgnoreCase(authMethods, "password");
        }
        return result;
    } //isPassword

    public static boolean isPublicKey(String authMethods)
    {

        boolean result = false;
        if (authMethods != null)
        {
            result = org.apache.commons.lang.StringUtils.containsIgnoreCase(authMethods, "publickey");
        }
        return result;
    } //isPublicKey
    
   
    
    
    public void connect() throws Exception
    {
        try
        {
            /* Create a connection instance */
            Log.log.debug("SSHConnect to "+hostname+":"+port);
            if (hostname == null)
            {
                throw new Exception("Missing hostname");
            }
            if(keepAliveInterval > 0 && customConfig != null)
            {
            	client = new SSHClient(customConfig);
            }else
            {
            	client = new SSHClient();
            }
            client.getConnection().getKeepAlive().setKeepAliveInterval(keepAliveInterval);
            client.addHostKeyVerifier(new HostKeyVerifier()
            {
                
                @Override
                public boolean verify(String arg0, int arg1, PublicKey arg2)
                {
                   
                    return true;
                }
            });
            client.setConnectTimeout(timeout);
            
            Log.log.trace("  connecting");
            client.connect(hostname, port);

           
            /* Authenticate */
            Log.log.trace("  authenticating mode: "+authMethod);
     
            
            
            authenticate(client);
            
            //String banner = client.getUserAuth().getBanner();
            
            
            session = client.startSession();
        
            session.allocatePTY("dumb", 80, 24, 0, 0, Collections.<PTYMode, Integer>emptyMap());
            
            if(prompt != null)
                reader =   new SSHJReader(session.getInputStream(), prompt, session.getRemoteCharset());
            else
                reader =   new SSHJReader(session.getInputStream(), session.getRemoteCharset());
               
            
            
            ((SSHJReader) reader).start();
            shell = session.startShell();
           
            shell.setAutoExpand(true);
            
          
            
            
            
            
            
            isClosed = false;
           
            super.connect();
        }
        catch (Exception e)
        {
            
            Log.log.error("SSHConnect connect exception: "+e.getMessage(), e);
            reader = new SSHReader(e.getMessage());
            throw new Exception("SSHConnect connect exception: "+e.getMessage(), e);
        }
    } // connect
    
    protected static class SSHInteractiveCallback implements ChallengeResponseProvider
    {   
        private StringBuffer message = new StringBuffer();
        private static final char[] EMPTY_RESPONSE = new char[0];
        int order = 0;
        List<String>  responses;
        private String name, instruction;
        int timeout;
        long startTime;
        
        public SSHInteractiveCallback(List<String> responses, int timeout)
        {
            this.responses = responses;
            this.timeout = timeout;
            this.startTime = System.currentTimeMillis();
        }

        @Override
        public List<String> getSubmethods()
        {
             return Collections.emptyList();
        }

        @Override
        public void init(Resource paramResource, String name, String instruction)
        {
            order = 0;
            Log.log.debug(name + " " + instruction);
            this.name = name;
            this.message.delete(0, this.message.length());
            this.instruction = instruction;
            this.message.append(name+"\n");
            this.message.append(instruction+"\n");
            this.message.append("Prompts: \n");
        }

        @Override
        public char[] getResponse(String prompt, boolean echo) 
        {
            char[] response;
    
            this.message.append(StringUtils.join(prompt, "\n"));
            
            Log.log.trace("Interactive prompts:\n"+message);
 

            if (timeout > 0 && (System.currentTimeMillis() - startTime) > timeout)
            {
             //   throw new Error("Interactive Timeout Expired");
            }
            if (responses == null)
            {
               
                Log.log.trace("Sending empty responses");
                response = EMPTY_RESPONSE;
            }
            else
            {
               String msg = responses.get(order++);
               Log.log.trace("Sending responses:\n"+msg);
               response = msg.toCharArray();
            }
    
            return response;
          
          
        }

        @Override
        public boolean shouldRetry()
        {
            
            return false;
        }
        
        
        
    }
    
    public void put(String fromFile) throws FileNotFoundException, IOException
    {
        
        put(fromFile, ".", "0664");
    }
    
    
    public void put(String fromFile, String toDirectory) throws FileNotFoundException, IOException
    {
        
        put(fromFile, toDirectory, "0664");
    }
    /**
     * SCP a file from the local system to a directory on the remote system using mode 0644
     * @param fromFile File to SCP
     * @param toDirectory Directory to copy to
     * @throws FileNotFoundException If local file can not be found
     * @throws IOException
     */
    public void put(String fromFile, String toDirectory, final String mode) throws FileNotFoundException, IOException
    {
       
        if (!mode.matches("[0-7]{0,4}"))
        {
            Log.log.warn("Invalid Mode: " + mode);
            throw new NumberFormatException("Invalid Mode - " + mode);
        }
        try
        {
            File file = new File(fromFile);
            if (!file.isFile())
            {
                Log.log.warn(fromFile + " (" + file.getAbsolutePath() + ") could not be found");
                throw new FileNotFoundException(fromFile + " (" + file.getAbsolutePath() + ") could not be found");
            }
            else
            {   
                
                client.useCompression();
                
                SCPFileTransfer ft = client.newSCPFileTransfer();
               
                ft.upload(new FileSystemFile(fromFile) {

                    @Override
                    public int getPermissions() throws IOException
                    {
                        if(mode.isEmpty())
                            return 420;
                        return  Integer.parseInt(mode, 8);
                       
                    }
                    
                    
                }, toDirectory);
                
            }
        } 
        catch (IOException ioe)
        {
            Log.log.warn("Failed to SCP File " + fromFile + " to " + toDirectory + ": " + ioe.getMessage(), ioe);
            throw new IOException("SSHConnect IO exception: "+ ioe.getMessage(), ioe);
        }
    } // put
    /**
     * SCP a file from the local system to a directory on the remote system using mode 0644
     * @param fromFile File to SCP
     * @param toDirectory Directory to copy to
     * @throws FileNotFoundException If local file can not be found
     * @throws IOException
     */
    public void putFileContent(final String fileName, final String fileContent, String toDirectory, final String mode) throws FileNotFoundException, IOException
    {
      
        if (!mode.matches("[0-7]{0,4}"))
        {
            Log.log.warn("Invalid Mode: " + mode);
            throw new NumberFormatException("Invalid Mode - " + mode);
        }
        try
        { 
                        
            client.useCompression();
            
            SCPFileTransfer ft = client.newSCPFileTransfer();
            
            ft.upload(new InMemorySourceFile() {

                @Override
                public int getPermissions() throws IOException
                {
                   if(mode.isEmpty())
                       return 0;
                   return  Integer.parseInt(mode);
                   
                }

                @Override
                public String getName()
                {
                     
                    return fileName;
                }

                @Override
                public long getLength()
                {
                   return fileContent.length();
                }

                @Override
                public InputStream getInputStream() throws IOException
                {
                    
                    return IOUtils.toInputStream(fileContent);
                }
                
                
            }, toDirectory);
               
             
        } 
        catch (IOException ioe)
        {
            Log.log.warn("Failed to SCP File " + fileName + " to " + toDirectory + ": " + ioe.getMessage(), ioe);
            throw new IOException("Failed to SCP File " + fileName + " to " + toDirectory + ": " + ioe.getMessage(), ioe);
        }
    }
    
    
    /**
     * SCP a remote file to the local system
     * @param remoteFile Remote file to copy
     * @param localDirectory Local directory to copy file to
     * @return true if local file exists after transfer, false otherwise
     * @throws FileNotFoundException If local directory can not be found or is not a directory
     * @throws IOException
     */
    public boolean get(String remoteFile, String localDirectory) throws FileNotFoundException, IOException
    {
        boolean result = false;

        try
        {
            File localDir = new File(localDirectory);
            if (!localDir.isDirectory())
            {
                Log.log.warn(localDirectory + " could not be found");
                throw new FileNotFoundException(localDirectory + " could not be found");
            }
            else
            {
                SCPFileTransfer ft = client.newSCPFileTransfer();
                
                ft.download(remoteFile, localDirectory);
                 
                File tmpRemoteFile = new File(remoteFile);
                File localFile = new File(localDirectory ,  tmpRemoteFile.getName());
                if (localFile.exists())
                {
                    result = true;
                }
                
            }
        }
        catch (IOException ioe)
        {
            Log.log.warn("Failed to Get File " + remoteFile + " to " + localDirectory + ": " + ioe.getMessage(), ioe);
            throw new IOException("Failed to Get File " + remoteFile + " to " + localDirectory + ": " + ioe.getMessage(), ioe);
        }

        return result;
    } // get
    
    
    protected  void authenticate(SSHClient c) throws TransportException, Exception
    {
        boolean isAuthenticated;
   
        switch(authMethod.toUpperCase())
        {
            case P_ASSWORD:
            
                client.authPassword(username, password);

                break;
                
            case PUBLICKEY:
                
                KeyProvider kp = c.loadKeys(privateKeyFile.toString(),privateKeyPassphrase );
                
                client.authPublickey(username, kp);
                break;
                
                
            case INTERACTIVE:
            
                
                c.auth(username, new AuthKeyboardInteractive(new SSHInteractiveCallback(responses, timeout)));
  
                break;
            
            
            case NONE: 
                
                c.auth(username, new AuthNone());
                break;
                
    
        } 
             
        
    }
    
    
    protected String authenticateNone(SSHClient c)    throws TransportException, Exception
    {
        AuthNone auth =  new AuthNone();
        c.auth(username,auth);
        return c.getUserAuth().getBanner();
        
    }
    @Override
    public void close() 
    { 
            closeQuietly();
    } // close

    

    /**
     * Sends a command to the remote system connected to.
     * A newline character will automatically be added to the line to simulate hitting the
     * 'Enter' key, thereby executing the command
     * @param line Command to send
     * @throws Exception
     */
    public void send(String line) throws Exception
    {
        send(line, true);
    } // send

    /**
     * Sends a command to the remote system connected to.
     * The sendNewline option specifies whether to add a newline character to the sent
     * command, simulating the 'Enter' key, thereby executing the command
     * @param line Command to send
     * @param sendNewline Whether or not to send a newline character with the command
     * @throws Exception
     */
    public void send(String line, boolean sendNewline) throws Exception
    {
        try
        {
            
            super.send();

            // include line termination
            if (sendNewline)
            {
                line += "\n";
            }
            if(Log.log.isTraceEnabled())
            {
                Log.log.trace("sending: "+line);
            }
            
            
            OutputStream os = shell.getOutputStream();
          
            
            os.write(line.getBytes(shell.getRemoteCharset()));
            os.flush();
            
           
            
           
            
        }
        catch (Exception e)
        {
            Log.log.warn(e.getMessage());
            throw new Exception("Could not send command to server: " + e.getMessage(), e);
          
        }
    } // send
    public void closeQuietly()
    {
        if(shell != null)
            IOUtils.closeQuietly(shell);
        if(client != null)
            IOUtils.closeQuietly(client);

        isClosed = true;
    }
    
    @Override
    public boolean isClosed()
    {
    	if(!client.isConnected())
    	{
    		closeQuietly();
    	}
    	return isClosed;
    }
    
    public static void initCustomConfig()
    {
    	customConfig = new DefaultConfig();
    }
    
    public static void initKeepAlive()
    {
    	if(customConfig == null)
    	{
    		initCustomConfig();
    	}
    	customConfig.setKeepAliveProvider(KeepAliveProvider.KEEP_ALIVE);
    }

	public String getHostname()
    {
        return hostname;
    }

    public void setHostname(String hostname)
    {
        this.hostname = hostname;
    }

    public int getPort()
    {
        return port;
    }

    public void setPort(int port)
    {
        this.port = port;
    }

    public int getTimeout()
    {
        return timeout;
    }

    public void setTimeout(int timeout)
    {
        this.timeout = timeout;
    }

    public String getUsername()
    {
        return username;
    }

    public void setUsername(String username)
    {
        this.username = username;
    }

    public String getPassword()
    {
        return password;
    }

    public void setPassword(String password)
    {
        this.password = password;
    }

    public String getPrivateKeyFileName()
    {
        return privateKeyFile.getName();
    }

    public String getPassphrase()
    {
        return privateKeyPassphrase;
    }

    public void setPassphrase(String privateKeyPassphrase)
    {
        this.privateKeyPassphrase = privateKeyPassphrase;
    }

    public List getResponses()
    {
        return responses;
    }

    public void setResponses(List responses)
    {
        this.responses = responses;
    }
    
	public static DefaultConfig getCustomConfig() {
		if(customConfig == null)
		{
			customConfig = new DefaultConfig();
		}
		return customConfig;
	}
    

}
