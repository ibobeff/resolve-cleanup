/******************************************************************************
* (C) Copyright 2014
*
* Resolve Systems, LLC
*
* All rights reserved
* This software is distributed under license and may not be used, copied,
* modified, or distributed without the express written permission of
* Resolve Systems, LLC.
*
******************************************************************************/

package com.resolve.connect;

import java.io.InputStream;
import java.util.ArrayList;
import java.util.List;

//import com.google.common.primitives.Bytes;

/**
 * This thread consumes output from the remote server and displays it in the
 * terminal window.
 * 
 */
class SocketReader extends AbstractConnectReader
{
    List<Byte> bytes = new ArrayList<Byte>();
    InputStream in;
    
    public SocketReader(InputStream in)
    {
        this.in = in;
        // set daemon thread
        setDaemon(true);
    } // SocketReader

    public void run()
    {
        byte[] buff = new byte[32768];

        try
        {
            while (true)
            {
                int len = in.read(buff);
                if (len == -1) 
                {
                    return;
                }
                synchronized (this)
                {
                    //bytes.addAll(Bytes.asList(buff));
                    for(byte byt : buff)
                    {
                        bytes.add(byt);  
                    }
                }
                //Log.log.debug(new String(buff, 0, len));
            }
        }
        catch (Exception e) { }
    } // run

    public synchronized String read()
    {
        return new String(readByte());
    } // read
    
    @Override
    public synchronized byte[] readByte()
    {
        if (bytes.size() > 0)
        {
            //byte[] result = Bytes.toArray(bytes);
            byte[] result = new byte[bytes.size()];
            int i=0;
            for(byte byt : bytes)
            {
                result[i++] = byt;
            }
            bytes.clear(); //re-initialize
            return result; 
        }
        return new byte[0];
    } // readByte

    public synchronized void clear()
    {
        bytes.clear();
    } // clear
    
    public boolean hasContent()
    {
        return bytes.size() > 0;
    } // hasContent
    
} // SocketReader
