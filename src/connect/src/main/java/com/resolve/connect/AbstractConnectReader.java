/******************************************************************************
* (C) Copyright 2014
*
* Resolve Systems, LLC
*
* All rights reserved
* This software is distributed under license and may not be used, copied,
* modified, or distributed without the express written permission of
* Resolve Systems, LLC.
*
******************************************************************************/

package com.resolve.connect;

/**
 * This is an abstract class used for Connector readers {@link ConnectReader}. 
 */
public abstract class AbstractConnectReader extends Thread implements ConnectReader
{
    public byte[] readByte()
    {
        //if necessary each subclass should implement this method.
        throw new RuntimeException("Subclass must implement this.");
    }
}
