/******************************************************************************
* (C) Copyright 2014
*
* Resolve Systems, LLC
*
* All rights reserved
* This software is distributed under license and may not be used, copied,
* modified, or distributed without the express written permission of
* Resolve Systems, LLC.
*
******************************************************************************/

package com.resolve.connect;

import com.resolve.util.Constants;
import com.resolve.util.StringUtils;

/**
 * Helper class used to generate PostgreSQL JDBC URLs and the name of the Driver class used by Resolve.
 * <br>URLs are in the form of jdbc:postgresql://<i>hostname</i>:<i>port</i>/<i>dbname</i>
 * @author justin.geiser
 *
 */
public class PostgreSQLDBConnect
{
    /**
     * Uses the hostname and dbname to generate the JDBC URL.  The port used will be 5432 unless the hostname if formated like <i>hostname</i>:<i>port</i>
     * @param hostname
     * @param dbname
     * @return
     */
    public static String getConnectURL(String hostname, String dbname)
    {
        DBHost dbHost = DBConnectHelper.parseHost(hostname, 5432);
        return getConnectURL(dbHost.host, dbHost.port, dbname);
    } // getConnectURL
    
    /**
     * Uses the hostname, port, and dbname to generate the JDBC URL.
     * @param hostname
     * @param port
     * @param dbname
     * @return
     */
    public static String getConnectURL(String hostname, int port, String dbname)
    {
        return String.format("jdbc:postgresql://%s:%d/%s", hostname, port, dbname);
        
    } // getConnectURL

    /**
     * Uses the hostname, port, and dbname to generate the JDBC URL.
     * @param hostname
     * @param port
     * @param dbname
     * @return
     */
    public static String getConnectURL(String hostname, String port, String dbname)
    {
        if(StringUtils.isNumeric(port))
        {
            return getConnectURL(hostname, Integer.valueOf(port), dbname);
        }
        throw new RuntimeException("Port must be a number like 5432.");
    } // getConnectURL


    /**
     * Returns the name of the PostgreSQL driver shipped with Resolve
     * @return
     */
    public static String getDriver()
    {
        return Constants.POSTGRESQLDRIVER;
    } // getDriver
    
} // PostgresDBConnect
