/******************************************************************************
* (C) Copyright 2014
*
* Resolve Systems, LLC
*
* All rights reserved
* This software is distributed under license and may not be used, copied,
* modified, or distributed without the express written permission of
* Resolve Systems, LLC.
*
******************************************************************************/

package com.resolve.connect;

import java.util.concurrent.CountDownLatch;

import com.intelliden.common.IntellidenException;
import com.intelliden.icos.api.ApiSession;
import com.intelliden.icos.api.workflow.WorkflowManager;
import com.intelliden.icos.idc.Work;
import com.intelliden.icos.idc.WorkState;
import com.resolve.util.Log;

public class ITNCMUoWPoller implements Runnable
{
    private String uowId;
    private ApiSession session;
    private CountDownLatch latch;

    /**
     * Constructor
     * 
     * @param session
     *            A connected ApiSession instance
     * @param uowId
     *            The UOW ID of the UOW to poll for completion
     * @param latch
     *            A CountDownLatch that can be use to make the calling routine
     *            wait for the UOW to complete. Pass in a null CountDownLatch if
     *            you don't need or want to use this feature.
     */
    public ITNCMUoWPoller(ApiSession session, String uowId, CountDownLatch latch)
    {
        this.uowId = uowId;
        this.session = session;
        this.latch = latch;
    } // ITNCMUoWPoller

    /*
     * (non-Javadoc)
     * 
     * @see java.lang.Runnable#run()
     */
    public void run()
    {
        boolean finished = false;
        WorkflowManager wfManager = session.workflowManager();

        while (!finished)
        {
            try
            {
                Work work = wfManager.getWork(uowId);
                WorkState state = work.getState();
                if (state.equals(WorkState.FINISHED) || state.equals(WorkState.CANCELLED) || state.equals(WorkState.EXPIRED))
                {
                    finished = true;
                    // Potentially do something with the UOW data at this point
                    String execStatus = work.getExecutionStatus();
                    byte[] logBytes = wfManager.getWorkLog(uowId);
                    if(Log.log.isTraceEnabled())
                    {
                        Log.log.trace("UOW " + uowId + " execution status: " + execStatus);
                        Log.log.trace("UOW " + uowId + " log: " + new String(logBytes));
                    }
                }
            }
            catch (IntellidenException ie)
            {
                Log.log.debug("An error occurred while fetching or processing UOW " + uowId + ie.getNestedExceptionStackTrace(), ie);
                
                // You may want to retry a few times, but we will just exit in this example.
                finished = true;
            }
            catch (Throwable t)
            {
                Log.log.debug("An error occurred while fetching or processing UOW " + uowId, t);
                finished = true;
            }

        }
        if (latch != null)
        {
            latch.countDown();
        }
    }
} // ITNCMUoWPoller
