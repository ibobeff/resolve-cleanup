/******************************************************************************
* (C) Copyright 2014
*
* Resolve Systems, LLC
*
* All rights reserved
* This software is distributed under license and may not be used, copied,
* modified, or distributed without the express written permission of
* Resolve Systems, LLC.
*
******************************************************************************/

package com.resolve.connect;

import java.io.IOException;
import java.net.InetAddress;
import java.net.UnknownHostException;

import org.xbill.DNS.DClass;
import org.xbill.DNS.ExtendedResolver;
import org.xbill.DNS.Message;
import org.xbill.DNS.Name;
import org.xbill.DNS.Record;
import org.xbill.DNS.Resolver;
import org.xbill.DNS.ReverseMap;
import org.xbill.DNS.Section;
import org.xbill.DNS.Type;

import com.google.common.net.InetAddresses;
import com.resolve.util.Log;
import com.resolve.util.StringUtils;

/**
 * This adapter provides various network related functionalities.
 */
public class NetworkConnect
{
    /**
     * Test whether the hostIP address is reachable. Best effort is made to try
     * to reach the host, but firewalls and server configuration may block
     * requests resulting in a unreachable status while some specific ports may
     * be accessible. Typically uses ICMP ECHO REQUESTs if the privilege can be
     * obtained, otherwise it tries to establish a TCP connection on port 7
     * (Echo) of the destination host.
     *
     * The timeout value, in seconds, indicates the maximum amount of time the
     * try should take. If the operation times out before getting an answer, the
     * host is deemed unreachable.
     *
     * <pre>
     * {@code
     *      import com.resolve.connect.NetworkConnect;
     *      ...
     *
     *      try
     *      {
     *          String hostIP = "10.20.2.113";
     *          int timeout = 5; //seconds
     *          boolean result = pingByIP(hostIP, timeout);
     *          System.out.printf("Is PING to %s succeeded? %b\n", hostIP, result);
     *      }
     *      catch (ConnectException e)
     *      {
     *          //do something about this exception
     *      }
     * }
     * </pre>
     *
     * @param hostIP
     *            is a valid IP address of the host (e.g., 192.168.2.100)
     * @param timeout
     *            in seconds, must be greater than 0.
     * @return
     * @throws ConnectException
     */
    public static boolean pingByIP(String hostIP, int timeout) throws ConnectException
    {
        boolean result = false;

        try
        {
            if (StringUtils.isBlank(hostIP))
            {
                throw new ConnectException("Host IP must be provided");
            }
            if (timeout <= 0)
            {
                throw new ConnectException("TIMEOUT must be a positive integer. Provided: " + timeout);
            }

            // InetAddress inet = InetAddress.getByAddress(hostIp, new byte[]
            // {10, 20, 2, (byte)128});
            if (InetAddresses.isInetAddress(hostIP))
            {
                InetAddress inet = InetAddresses.forString(hostIP);
                Log.log.trace("Sending Ping Request to " + inet);
                result = inet.isReachable(timeout * 1000); // making it
                                                           // milliseconds
            }
        }
        catch (UnknownHostException e)
        {
            Log.log.error(e.getMessage(), e);
            throw new ConnectException(e.getMessage(), e);
        }
        catch (IOException e)
        {
            Log.log.error(e.getMessage(), e);
            throw new ConnectException(e.getMessage(), e);
        }

        return result;
    }

    /**
     * Test whether the hostName is reachable. Best effort is made to try to
     * reach the host, but firewalls and server configuration may block requests
     * resulting in a unreachable status while some specific ports may be
     * accessible. Typically uses ICMP ECHO REQUESTs if the privilege can be
     * obtained, otherwise it tries to establish a TCP connection on port 7
     * (Echo) of the destination host.
     *
     * The timeout value, in seconds, indicates the maximum amount of time the
     * try should take. If the operation times out before getting an answer, the
     * host is deemed unreachable.
     *
     * <pre>
     * {@code
     *      import com.resolve.connect.NetworkConnect;
     *      ...
     *
     *      try
     *      {
     *          String hostName = "MyServer.acme.com";
     *          int timeout = 5; //seconds
     *          boolean result = pingByName(hostName, timeout);
     *          System.out.printf("Is PING to %s succeeded? %b\n", hostName, result);
     *      }
     *      catch (ConnectException e)
     *      {
     *          //do something about this exception
     *      }
     * }
     * </pre>
     *
     * @param hostName
     *            is a DNS name of the host (e.g., MyServer1.acme.com)
     * @param timeout
     *            in seconds, must be greater than 0.
     * @return
     * @throws ConnectException
     */
    public static boolean pingByName(String hostName, int timeout) throws ConnectException
    {
        boolean result = false;

        try
        {
            if (StringUtils.isBlank(hostName))
            {
                throw new ConnectException("Host Name must be provided");
            }
            if (timeout <= 0)
            {
                throw new ConnectException("TIMEOUT must be a positive integer. Provided: " + timeout);
            }

            InetAddress inet = InetAddress.getByName(hostName);
            Log.log.trace("Sending Ping Request to " + inet);
            result = inet.isReachable(timeout * 1000);
        }
        catch (UnknownHostException e)
        {
            Log.log.error(e.getMessage(), e);
            throw new ConnectException(e.getMessage(), e);
        }
        catch (IOException e)
        {
            Log.log.error(e.getMessage(), e);
            throw new ConnectException(e.getMessage(), e);
        }

        return result;
    }

    /**
     * Resolves the reverse DNS name for the given IP address. This method is
     * mostly useful in an inernal network where the corporate DNS server holds
     * the information of any machines in the network.
     *
     * <pre>
     * {@code
     *      import com.resolve.connect.NetworkConnect;
     *      ...
     *
     *      try
     *      {
     *          String hostIP = "192.168.1.100";
     *          String result = reverseDNS(hostIP, timeout);
     *          System.out.printf("DNS name for the IP %s is %s\n", hostIP, result);
     *      }
     *      catch (ConnectException e)
     *      {
     *          //do something about this exception
     *      }
     * }
     * </pre>
     *
     * @param hostIP
     *            is a valid IP address of the host (e.g., 192.168.1.100).
     * @return DNS name if the lookup succeeded otherwise the incoming hostIP.
     * @throws ConnectException
     */
    public static String reverseDNS(String hostIP) throws ConnectException
    {
        String result = null;

        Resolver resolver;
        try
        {
            if (StringUtils.isBlank(hostIP))
            {
                throw new ConnectException("Host IP must be provided");
            }

            resolver = new ExtendedResolver();
            Name name = ReverseMap.fromAddress(hostIP);
            int type = Type.PTR;
            int dclass = DClass.IN;
            Record rec = Record.newRecord(name, type, dclass);
            Message query = Message.newQuery(rec);
            Message response = resolver.send(query);

            Record[] answers = response.getSectionArray(Section.ANSWER);
            if (answers.length == 0)
            {
                result = hostIP;
            }
            else
            {
                result = answers[0].rdataToString();
            }
        }
        catch (UnknownHostException e)
        {
            Log.log.error(e.getMessage(), e);
            throw new ConnectException(e.getMessage(), e);
        }
        catch (IOException e)
        {
            Log.log.error(e.getMessage(), e);
            throw new ConnectException(e.getMessage(), e);
        }

        return result;
    }
}
