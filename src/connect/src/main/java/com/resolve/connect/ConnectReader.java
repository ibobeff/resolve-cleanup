/******************************************************************************
* (C) Copyright 2014
*
* Resolve Systems, LLC
*
* All rights reserved
* This software is distributed under license and may not be used, copied,
* modified, or distributed without the express written permission of
* Resolve Systems, LLC.
*
******************************************************************************/

package com.resolve.connect;

public interface ConnectReader
{
    // required by Connect.expect() methods
    public String read();
    public byte[] readByte();
    public void clear();
    public boolean hasContent();
    
} // ConnectReader
