/******************************************************************************
* (C) Copyright 2014
*
* Resolve Systems, LLC
*
* All rights reserved
* This software is distributed under license and may not be used, copied,
* modified, or distributed without the express written permission of
* Resolve Systems, LLC.
*
******************************************************************************/

package com.resolve.connect;


public class DBConnectHelper
{
    /**
     * This method validates and parses the hostName string and
     * determine the host name/IP or the port provided by the
     * invoker.
     * 
     * @param hostName in the form mydbserver or mydbserver:1234
     * @param defaultPort is used if hostName doesn't have port defined.
     * @return
     */
    public static DBHost parseHost(String hostName, int defaultPort)
    {
        DBHost dbHost = new DBHost(hostName, defaultPort);
        String[] strings = hostName.split(":");
        if(strings.length > 2)
        {
            throw new RuntimeException("Invalid host value: " + hostName);
        }
        else if(strings.length == 2)
        {
            try
            {
                dbHost.host = strings[0];
                dbHost.port = Integer.parseInt(strings[1]);
            }
            catch (NumberFormatException e)
            {
                throw new RuntimeException("Invalid port number : " + strings[1]);
            }
        }
        
        return dbHost;
    }
    /**
     * @param args
     */
    /*
    public static void main(String[] args)
    {
        DBHost result = parseHost("svr1", 1234);
        System.out.printf("Host: %s, Port %d\n", result.host, result.port);
        result = parseHost("server2:890", 1234);
        System.out.printf("Host: %s, Port %d\n", result.host, result.port);
    }
    */
}
