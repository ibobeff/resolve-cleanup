/******************************************************************************
* (C) Copyright 2014
*
* Resolve Systems, LLC
*
* All rights reserved
* This software is distributed under license and may not be used, copied,
* modified, or distributed without the express written permission of
* Resolve Systems, LLC.
*
******************************************************************************/

package com.resolve.connect;

import java.security.GeneralSecurityException;

import com.gargoylesoftware.htmlunit.BrowserVersion;
import com.gargoylesoftware.htmlunit.Page;
import com.gargoylesoftware.htmlunit.WebClient;
import com.gargoylesoftware.htmlunit.WebWindow;
import com.resolve.rsbase.SessionObjectInterface;
import com.resolve.util.Log;

public class HtmlConnect extends WebClient implements SessionObjectInterface
{
    private static final long serialVersionUID = 4618849169847484457L;

	public HtmlConnect()
    {
        super();
    } // HtmlConnect

    public HtmlConnect(BrowserVersion browserVersion)
    {
        super(browserVersion);
    } // HtmlConnect

    public HtmlConnect(BrowserVersion browserVersion, String proxyHost, int proxyPort)
    {
        super(browserVersion, proxyHost, proxyPort);
    } // HtmlConnect

//    public void setUseInsecureSSLv3Only() throws GeneralSecurityException
//    {
//        super.setUseInsecureSSLv3Only();
//    } // setUseInsecureSSLv3Only

    public void close()
    {
        try
        {
            super.close();
        }
        catch (Exception e)
        {
            Log.log.warn("Error closing connection: " + e.getMessage());
        }
    } // close

    public void finalize()
    {
        try
        {
            super.close();
        }
        catch (Exception e)
        {
            Log.log.warn("Error closing connection: " + e.getMessage());
        }
    } // finalize

    public Page getCurrentWindowPage()
    {
        Page page = null;
        try
        {
            WebWindow window = this.getCurrentWindow();
            page = window.getEnclosedPage();
        }
        catch (Exception e)
        {
            Log.log.error("Unable to Retrieve Page: " + e.getMessage(), e);
        }

        return page;
    }

} // HtmlConnect
