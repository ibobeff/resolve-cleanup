/******************************************************************************
* (C) Copyright 2014
*
* Resolve Systems, LLC
*
* All rights reserved
* This software is distributed under license and may not be used, copied,
* modified, or distributed without the express written permission of
* Resolve Systems, LLC.
*
******************************************************************************/

package com.resolve.connect;

import java.io.File;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Map;

import javax.mail.internet.InternetAddress;

import org.apache.commons.mail.Email;
import org.apache.commons.mail.EmailAttachment;
import org.apache.commons.mail.EmailException;
import org.apache.commons.mail.HtmlEmail;
import org.apache.commons.mail.SimpleEmail;

import com.resolve.rsbase.SessionObjectInterface;
import com.resolve.util.Log;
import com.resolve.util.StringUtils;

public class EmailConnect2 implements SessionObjectInterface
{
    private final Email message;

    /**
     * This constructor. This prepares a HTML message by default.
     *
     * @param host
     *            - name or the IP address of the SMTP server (e.g.,
     *            hotmail-@gt;smtp.live.com and yahoo-@gt;smtp.mail.yahoo.com)
     * @param port
     *            - SMTP server port (e.g., hotmail-@gt;587, yahoo-@gt;465)
     * @param username
     *            - username to login to the SMTP server, null/empty if the
     *            server do not need username.
     * @param password
     *            - password of the user, null/empty if the server do not need
     *            user password.
     * @param isSSL
     *            set to true if your email server supports Secure Socket Layer
     *            (SSL). Hotmail doesn't need this but for yahoo and gmail set it
     *            to true.
     * @param hasAttachment
     *            set to true if you are adding attachment to the message.
     * @param properties
     *            add any properties your email server may need. For example,
     *            hotmail.com needs a property "mail.smtp.starttls.enable" with
     *            the value "true";
     * @throws Exception
     */
    public EmailConnect2(String host, int port, String username, String password, boolean isSSL, boolean hasAttachment, Map<String, String> properties) throws Exception
    {
        this(host, port, username, password, isSSL, true, hasAttachment, properties);
    } // EmailConnect2

    /**
     * This constructor.
     *
     * @param host
     *            - name or the IP address of the SMTP server (e.g.,
     *            hotmail-@gt;smtp.live.com and yahoo-@gt;smtp.mail.yahoo.com)
     * @param port
     *            - SMTP server port (e.g., hotmail-@gt;587, yahoo-@gt;465)
     * @param username
     *            - username to login to the SMTP server, null/empty if the
     *            server do not need username.
     * @param password
     *            - password of the user, null/empty if the server do not need
     *            user password.
     * @param isSSL
     *            set to true if your email server supports Secure Socket Layer
     *            (SSL). Hotmail doesn't need this but for ahoo and gmail set it
     *            to true.
     * @param isHtml
     *            set to true if you want to send HTML content
     * @param hasAttachment
     *            set to true if you are adding attachment to the message.
     * @param properties
     *            add any properties your email server may need. For example,
     *            hotmail.com needs a property "mail.smtp.starttls.enable" with
     *            the value "true";
     * @throws Exception
     */
    public EmailConnect2(String host, int port, String username, String password, boolean isSSL, boolean isHtml, boolean hasAttachment, Map<String, String> properties) throws Exception
    {
        if (hasAttachment || isHtml)
        {
            message = new HtmlEmail();
        }
        else
        {
            message = new SimpleEmail();
        }
        message.setSSL(isSSL);
        message.setHostName(host);
        if(isSSL){
        	message.setSslSmtpPort(Integer.toString(port));
        } else {
        	message.setSmtpPort(port);
        }
        if (StringUtils.isNotBlank(username) && StringUtils.isNotBlank(password))
        {
            message.setAuthentication(username, password);
        }

        if (properties != null && properties.size() > 0)
        {
            message.getMailSession().getProperties().putAll(properties);
            
            Log.log.trace("Mail session properties:");
            for (String key : properties.keySet())
            {
                Log.log.trace(key+": "+message.getMailSession().getProperties().get(key));
            }
        } 
    } // EmailConnect2

    public void close()
    {
    } // close

    public void finalize()
    {
    } // finalize

    /**
     * Send the message.
     *
     * <pre>
     * {@code
     *  try
     *         {
     *             Map<String, String> props = new HashMap<String, String>();
     *             props.put("mail.smtp.starttls.enable", "true");
     *             //for hotmail (live) account set isSSL parameter to false and true for gmail and yahoo.
     *             EmailConnect2 conn = new EmailConnect2("smtp.live.com", 587, "my_address@hotmail.com", "mypassword", false, true, props);
     *             conn.addTo("some_address@gmail.com");
     *             conn.setFrom("my_address@gmail.com");
     *             conn.setSubject("Hello from resolve EmailConnect2!");
     *             conn.setHtml("<html>This is HTML</html>");
     *             conn.setText("Tish is text");
     *
     *             File newFile = new File("PATH_TO_SOME_FILE/SMyFile.pdf");
     *             conn.addAttachment(newFile);
     *
     *             conn.send();
     *             conn.close();
     *         }
     *         catch (Exception e)
     *         {
     *             // Do something good with this exception.
     *         }
     * }
     * </pre>
     *
     * @return
     * @throws Exception
     */
    public String send() throws Exception
    {
        String result = "";
        try
        {
            Log.log.trace("Mail Properties");
            Map properties = message.getMailSession().getProperties();
            for (Object key : properties.keySet())
            {
                Log.log.trace("key: "+key+" value: "+properties.get(key));
            }
            Log.log.trace("From: "+message.getFromAddress());
            Log.log.trace("To: "+ message.getToAddresses());
            Log.log.trace("Subject: "+message.getSubject());
             
            
            result = message.send();
        }
        catch (Exception e)
        {
            result = "Unable to send message : " + e.getMessage();
            Log.log.error("Unable to send message: " + e.getMessage(), e);
        }
        return result;
    } // send
    
    /**
     * Set the from address, if the STMP server needs it then it must be a valid
     * email address.
     *
     * @param from
     * @return
     */
    public String setFrom(String from)
    {
    	return setFrom(from, null);
    }

    /**
     * Set the from address, if the STMP server needs it then it must be a valid
     * email address.
     *
     * @param from
     * @param from display name (optional)
     * @return
     */
    public String setFrom(String from, String fromDisplayName)
    {
        String result = "";

        try
        {
            if(StringUtils.isNotBlank(fromDisplayName))
            {
                message.setFrom(from, fromDisplayName);
            }
            else
            {
                message.setFrom(from);
            }
        }
        catch (Exception e)
        {
            result = "Unable to add From : " + e.getMessage();
            Log.log.error("Unable to add From : " + e.getMessage());
        }

        return result;
    } // setFrom

    /**
     * Adds reply to to the message.
     * @param replyTo must be in a comma separated email addresses.
     */
    public String setReplyTo(String replyTo)
    {
        String result = "";

        if(StringUtils.isNotBlank(replyTo))
        {
            try
            {
                Collection<String> addresses = StringUtils.stringToList(replyTo, ",");
                
                Collection<InternetAddress> iAddresses = new ArrayList<InternetAddress>();
                
                for(String address : addresses)
                {
                    iAddresses.add(new InternetAddress(address));
                }
                message.setReplyTo(iAddresses);
            }
            catch (Exception e)
            {
                result = "Unable to add ReplyTo : " + e.getMessage();
                Log.log.error(e.getMessage(), e);
            }
        }
        return result;
    }

    /**
     * Add email address of the receipient, must be a valid email address. To
     * add multiple address cal this method multiple times.
     *
     * @param addr
     * @return
     */
    public String addTo(String addr)
    {
        String result = "";

        try
        {
            message.addTo(addr);
        }
        catch (Exception e)
        {
            result = "Unable to add recipient: " + addr + " due to " + e.getMessage();
            Log.log.error("Unable to add recipient: "  + addr + " due to " + e.getMessage());
        }

        return result;
    } // addTo
    
    /**
     * Add email address of the receipient, must be a valid email address. To
     * add multiple address cal this method multiple times.
     *
     * @param addr
     * @return
     */
    public String addTo(String... addr)
    {
        String result = "";

        try
        {
            message.addTo(addr);
        }
        catch (Exception e)
        {
            result = "Unable to add recipient(s): "  + addr + " due to " + e.getMessage();
            Log.log.error("Unable to add recipient(s): "  + addr + " due to " + e.getMessage());
        }

        return result;
    } // addTo

    /**
     * Add CC address, must be a valid email address. To add multiple address
     * cal this method multiple times.
     *
     * @param addr
     * @return
     */
    public String addCC(String addr)
    {
        String result = "";

        try
        {
            message.addCc(addr);
        }
        catch (Exception e)
        {
            result = "Unable to add CC receipient: " + e.getMessage();
            Log.log.error("Unable to add CC receipient: " + e.getMessage());
        }
        return result;
    } // addCC

    /**
     * Add BCC address, must be a valid email address. To add multiple address
     * cal this method multiple times.
     *
     * @param addr
     * @return
     */
    public String addBCC(String addr)
    {
        String result = "";

        try
        {
            message.addBcc(addr);
        }
        catch (Exception e)
        {
            result = "Unable to add BCC receipient: " + e.getMessage();
            Log.log.error("Unable to BCC receipient: " + e.getMessage());
        }
        return result;
    } // addBCC

    /**
     * Set the message subject.
     *
     * @param subject
     * @return
     */
    public String setSubject(String subject)
    {
        String result = "";

        try
        {
            message.setSubject(subject);
        }
        catch (Exception e)
        {
            result = "Unable to set message subject: " + e.getMessage();
            Log.log.error("Unable to set message subject: " + e.getMessage());
        }
        return result;
    } // setSubject

    /**
     * Set the HTML content.
     *
     * @param html
     * @return
     */
    public String setHtml(String html)
    {
        String result = "";

        if (message instanceof HtmlEmail)
        {
            try
            {
                ((HtmlEmail) message).setHtmlMsg(html);
            }
            catch (EmailException e)
            {
                result = "ERROR: could not set html: " + e.getMessage();
                Log.log.error(result, e);
            }
        }

        return result;
    }

    /**
     * Set the normal text content.
     *
     * @param text
     * @return
     */
    public String setText(String text)
    {
        String result = "";

        try
        {
            message.setMsg(text);
        }
        catch (EmailException e)
        {
            result = "ERROR: could not set text: " + e.getMessage();
            Log.log.error(result, e);
        }

        return result;
    } // setText

    /**
     * Add attachment,
     *
     * @param file
     *            - must be available in the local file system where this method
     *            is being invoked.
     * @return
     */
    public String addAttachment(File file)
    {
        String result = "";

        if (file != null)
        {
            try
            {
                if (file.exists())
                {
                    EmailAttachment attachment = new EmailAttachment();
                    attachment.setPath(file.getAbsolutePath());
                    attachment.setDisposition(EmailAttachment.ATTACHMENT);
                    attachment.setName(file.getName());
                    if (message instanceof HtmlEmail)
                    {
                        ((HtmlEmail) message).attach(attachment);
                    }
                    else
                    {
                        throw new Exception("Doesn't support attachment. Instantiate a different EmailConnect2 object. Check EmailConnect2 Javadoc for more information.");
                    }
                }
                else
                {
                    throw new Exception("File does not exist: " + file.getAbsolutePath() + "/" + file.getName());
                }
            }
            catch (Exception e)
            {
                result = "Unable to add message attachment: " + e.getMessage();
                Log.log.error("Unable to add message attachment: " + e.getMessage());
            }
        }
        return result;
    } // addAttachment

} // EmailConnect
