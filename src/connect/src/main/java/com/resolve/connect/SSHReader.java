/******************************************************************************
* (C) Copyright 2014
*
* Resolve Systems, LLC
*
* All rights reserved
* This software is distributed under license and may not be used, copied,
* modified, or distributed without the express written permission of
* Resolve Systems, LLC.
*
******************************************************************************/

package com.resolve.connect;

import java.io.InputStream;
import java.util.Arrays;

import org.apache.commons.lang3.StringUtils;

import com.resolve.util.Log;

/**
 * This thread consumes output from the remote server and displays it in the
 * terminal window.
 * 
 */
class SSHReader extends AbstractConnectReader
{
    StringBuffer buffer = new StringBuffer();
    InputStream in;
    
    public SSHReader(InputStream in)
    {
        this(in, null);
    }
    
    public SSHReader(InputStream in, String prompt)
    {
        if (StringUtils.isNotBlank(prompt))
        {          
            byte[] buff = new byte[32768];
            int len = -1;
            
            // Clear the stream before using it
            boolean foundPrompt = false;
            
            try
            {
                /*
                 *  Delay reading of output stream to get prompt after authorization for 1 sec to get both last 
                 *  successful login attempt and prompt in single read.
                 *  
                 *  If no delay then occassionaly only last successful login is received first and prompt is received 
                 *  as part of subsequent command output. Command output is searched for prompt to indicate compeltion
                 *  of command. Prompt from authorization in command output results in it getting truncated and 
                 *  written to elastic search.  
                 */
                
                Thread.sleep(1000);
                
                while(in.available() > 0 && !foundPrompt)
                {
                    try
                    {
                        len = in.read(buff, 0, in.available() < buff.length ? in.available() : buff.length);
                        
                        if (len > 0)
                        {
                            String streamData = new String(buff, 0, len);
                            Log.log.debug("Passed \"in\" stream is not clear. Found [" + streamData + "]");
                            
                            if (streamData.contains(prompt))
                            {
                                Log.log.debug("Found the prompt " + prompt + " in stream data " + streamData);
                            }
                            else
                            {
                                Log.log.debug("Prompt " + prompt + " not found. Sleeping for 100 ms...");
                                Thread.sleep(100);
                            }
                        }
                    }
                    catch (Exception e)
                    {
                        Log.log.error(e.getMessage(), e);
                    }
                }
            }
            catch (Exception e)
            {
                Log.log.error(e.getMessage(), e);
            }
        }
        
        this.in = in;
        
        // set daemon thread
        setDaemon(true);
    } // SSHReader

    public SSHReader(String result)
    {
        // no inputstream
        this.in = null;
        
        // init result
        if (result == null)
        {
            result = "";
        }
        buffer = new StringBuffer(result);
    } // SSHReader

    public void run()
    {
        byte[] buff = new byte[32768];
        Arrays.fill(buff, (byte)0);

        try
        {
            while (in != null)
            {
                int len = in.read(buff);
                if (len == -1) 
                {
                    return;
                }
                
                //Log.log.debug(new String(buff, 0, len));
                synchronized (this)
                {
                    String result = new String(buff, 0, len);
                    result = result.replaceAll("\r\n", System.getProperty("line.separator"));
                    buffer.append(result);
                }
            }
        }
        catch (Exception e) {
            e.printStackTrace();
        }
    } // run
    
    public synchronized String read()
    {
        String result = "";
        
        if (buffer.length() > 0)
        {
            result = buffer.toString();
            Log.log.trace("read() result=[" + result + "]");
            buffer = new StringBuffer();
        }
        
        return result;
    } // read
    
    public synchronized void clear()
    {
        buffer = new StringBuffer();
    } // clear
    
    public boolean hasContent()
    {
        return buffer.length() > 0;
    } // hasContent
    
} // SSHReader
