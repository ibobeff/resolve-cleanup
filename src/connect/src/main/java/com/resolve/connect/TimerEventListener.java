/******************************************************************************
* (C) Copyright 2014
*
* Resolve Systems, LLC
*
* All rights reserved
* This software is distributed under license and may not be used, copied,
* modified, or distributed without the express written permission of
* Resolve Systems, LLC.
*
******************************************************************************/

package com.resolve.connect;

/**
 * This interface represents the events triggered by Timer class
 */
public interface TimerEventListener
{

    /**
     * This method represents the time-out event triggered by Timer.
     */
    public void timerTimedOut();

    /**
     * This method is invoked by the Timer, when the timer thread receives an
     * interrupted exception
     */
    public void timerInterrupted(InterruptedException ioe);

} // TimerEventListener
