/******************************************************************************
* (C) Copyright 2014
*
* Resolve Systems, LLC
*
* All rights reserved
* This software is distributed under license and may not be used, copied,
* modified, or distributed without the express written permission of
* Resolve Systems, LLC.
*
******************************************************************************/

package com.resolve.connect;

import java.net.URL;
import java.util.HashMap;
import java.util.Iterator;
import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.io.UnsupportedEncodingException;
import java.net.MalformedURLException;
import java.net.URLConnection;

import com.resolve.util.Log;

/**
 * @author alokika.dash
 * 
 */

public class WSConnect
{
    private String uri; 
    private HashMap<String, String> xmlProperties;
    private URLConnection conn = null;

    /**
     * @param uri
     */
    public WSConnect(String uri)
    {
        this.uri = uri;
        xmlProperties = new HashMap<String, String>();
        createDefaultXMLProperties();
    } // WSConnect

    /**
     * @param uri
     * @param xmlProperties
     */
    public WSConnect(String uri, HashMap<String, String> xmlProperties)
    {
        this.uri = uri;
        this.xmlProperties = xmlProperties;
        createDefaultXMLProperties();
    } // WSConnect


    public void createDefaultXMLProperties()
    { // save the default XML properties 
        xmlProperties.put("Content-Type", "text/xml; charset=utf-8");
    } //createDefaultXMLProperties
    
    public HashMap<String, String> getProperties()
    {
        return xmlProperties;
    } //getProperties


    public void setProperties(HashMap<String, String> xmlProperties)
    {
        this.xmlProperties = xmlProperties;
    } // setProperties

    public String getProperty(String name)
    {
        return xmlProperties.get(name);
    } // getProperty
    
    public void setProperty(String name, String value) 
    {
        xmlProperties.put(name, value);
    } // setProperty

    public void connect() throws Exception
    {
        URL url;
               
        /* Create a connection instance */
        try
        {
            url = new URL(uri);
            conn = url.openConnection();
            conn.setDoOutput(true);
            conn.getAllowUserInteraction();
            // Set Default Connection properties
            conn.addRequestProperty("Content-Type", "text/xml; charset=utf-8"); 
        }
        catch (MalformedURLException e1)
        {
            Log.log.error("Invalid URL " + uri + ":  " + e1.getMessage(), e1);
            throw e1;
        }
        catch (Exception e2)
        {
            Log.log.error("Cannot connect to an existing web service" + e2.getMessage(), e2);
            throw e2;
        }
    } // connect

    void initConnectionProperties(String xml) 
    {
        Iterator<String> propertyEntries = xmlProperties.keySet().iterator();
        while (propertyEntries.hasNext())
        {
            String propertyname = propertyEntries.next();
            if (!propertyname.equalsIgnoreCase("Content-Type"))
            {
                conn.addRequestProperty("Content-Type", "text/xml; charset=utf-8");
                xmlProperties.put("Content-Type", "text/xml; charset=utf-8");
            }
            if (!propertyname.equalsIgnoreCase("Content-Length"))
            {
                conn.addRequestProperty("Content-Length", Integer.toString(xml.length()));
                xmlProperties.put("Content-Length", Integer.toString(xml.length()));
            }
        }       
    } // initConnectionProperties
    
    public String send(String xml) throws Exception
    {
        // WebService Response
        String response = null;
        
        // connection properties
        initConnectionProperties(xml);
        
        // Open Writer and Reader to send xml messages 
        BufferedWriter writer = null;
        BufferedReader reader = null;
        try
        {
            writer = new BufferedWriter(new OutputStreamWriter(conn.getOutputStream(), "UTF8"));
            writer.write(xml);
            writer.flush();
            writer.close();
            
            reader = new BufferedReader(new InputStreamReader(conn.getInputStream()));
            response = "\nWebService Response:\n";
            String line = "";
            while ((line = reader.readLine()) != null)
            {
                response += line + "\n";
            }
            reader.close();
            Log.log.trace(response);
            //System.out.println(response);
        }
        catch (UnsupportedEncodingException e1)
        {
            Log.log.error("Unfamiliar message content" + e1.getMessage(), e1);
            throw e1;
        }
        catch (IOException e2)
        {
            Log.log.error("WSConnect.java Caught IOException: " + e2.getMessage(), e2);
            throw e2;
        }
        
        return response;
        
    } //send

     /*
     * Description: Self Test
    public static void main(String[] args) throws Exception
    {
        
        //init logging
        Log.init();
        
        //Test Case 1:  XML to startRunbookWithLoginByString from SOAP UI + HTTP + WSDL pointing to resolve Web services
        String xml1 = "<soapenv:Envelope xmlns:soapenv=" + "\"http://schemas.xmlsoap.org/soap/envelope/\" "
                        + "xmlns:xsd=\"http://www.resolve-systems.com/resolve/WebServiceListener/xsd\">\n";
        xml1 += "<soapenv:Header/>\n";
        xml1 += "<soapenv:Body><xsd:startRunbookWithLoginByString>\n";
        xml1 += "<!--Optional:-->\n";
        xml1 += "<xsd:username>admin</xsd:username>\n";
        xml1 += "<!--Optional:-->\n";
        xml1 += "<xsd:password>resolve</xsd:password>\n";
        xml1 += "<xsd:params>RUNBOOK=mytest.sample</xsd:params>\n";
        xml1 += "<xsd:params>\"PROBLEMID\"=\"NEW\"</xsd:params>\n";
        xml1 += "</xsd:startRunbookWithLoginByString>\n";
        xml1 += "</soapenv:Body>\n";
        xml1 += "</soapenv:Envelope>";
        
        String myURL1 = "http://localhost:9080/resolve/webservice/WebserviceListener?wsdl";
        
        //Test Case 2: XML to executeRunbookWithLoginByString from SOAP UI + HTTP + WSDL pointing to resolve Web services
        String xml2 = "<soapenv:Envelope xmlns:soapenv=" + "\"http://schemas.xmlsoap.org/soap/envelope/\" "
                           + "xmlns:xsd=\"http://www.resolve-systems.com/resolve/WebServiceListener/xsd\">\n";
        xml2 += "<soapenv:Header/>\n";
        xml2 += "<soapenv:Body>\n";
        xml2 +=    "<xsd:executeRunbookWithLoginByString>\n";
        xml2 +=       "<!--Optional:-->\n";
        xml2 +=       "<xsd:username>" + "admin" + "</xsd:username>\n";
        xml2 +=       "<!--Optional:-->";
        xml2 +=      "<xsd:password>" + "resolve" + "</xsd:password>\n";
        xml2 +=       "<!--Optional:-->\n";
        xml2 +=       "<xsd:params>RUNBOOK=mytest.sample</xsd:params>\n";
        xml2 +=    "</xsd:executeRunbookWithLoginByString>\n";
        xml2 += "</soapenv:Body>\n";
        xml2 += "</soapenv:Envelope>";
        
        String myURL2 = "http://localhost:9080/resolve/webservice/WebserviceListener?wsdl";
        
        //Test Case 3: XML to create a simple Web Service that converts the temperature from Celsius to Fahrenheit and vice versa + WSDL pointing to www.w3schools.com
        String xml3 = "<soap:Envelope xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\"" +
        		" xmlns:xsd=\"http://www.w3.org/2001/XMLSchema\" xmlns:soap=\"http://schemas.xmlsoap.org/soap/envelope/\">\n";
        xml3 += "<soap:Body>\n";
        xml3 +=    "<CelsiusToFahrenheit xmlns=\"http://tempuri.org/\">\n";
        xml3 +=    "<Celsius>200</Celsius>\n";
        xml3 +=    "</CelsiusToFahrenheit>\n";
        xml3 +=  "</soap:Body>\n";
        xml3 += "</soap:Envelope>";
        
        String myURL3 = "http://www.w3schools.com/webservices/tempconvert.asmx?WSDL";
        
        // NOTE : for every new message that you want to send create a new url connection ELSE you get the "Cannot write output after reading input" error
        WSConnect wsconnectTest = new WSConnect(myURL2);
        wsconnectTest.connect();
        wsconnectTest.send(xml2);
       
        wsconnectTest.connect();
        wsconnectTest.send(xml1);
    } // Test Main
    */
}
