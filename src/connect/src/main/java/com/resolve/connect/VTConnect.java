/******************************************************************************
* (C) Copyright 2014
*
* Resolve Systems, LLC
*
* All rights reserved
* This software is distributed under license and may not be used, copied,
* modified, or distributed without the express written permission of
* Resolve Systems, LLC.
*
******************************************************************************/

package com.resolve.connect;

import com.jagacy.Key;
import com.jagacy.SessionVt;
import com.jagacy.util.JagacyException;
import com.resolve.rsbase.SessionObjectInterface;
import com.resolve.util.Log;

public class VTConnect extends SessionVt implements SessionObjectInterface
{

    public VTConnect(String sessionName, String host, int port) throws JagacyException
    {
        super(sessionName, host, port);

        this.open();
    } // VTConnect

    public VTConnect(String sessionName, String host, int port, String terminalType) throws JagacyException
    {
        super(sessionName, host, port, terminalType);

        this.open();
    } // VTConnect

    public VTConnect(String sessionName, String host, int port, String terminalType, String sslKeyFile, String sslKeyPassword) throws JagacyException
    {
        super(sessionName, host, port, terminalType);

        this.open(sslKeyFile, sslKeyPassword);
    } // VTConnect

    public void close()
    {
        try
        {
            super.close();
        }
        catch (Exception e)
        {
            Log.log.warn("Error closing connection: " + e.getMessage());
        }
    } // close

    public void finalize()
    {
        try
        {
            super.close();
        }
        catch (Exception e)
        {
            Log.log.warn("Error closing connection: " + e.getMessage());
        }

        super.finalize();
    } // finalize

    // key mapping
    public static Key ATTN = Key.ATTN;
    public static int ATTN_ID = Key.ATTN_ID;
    public static String ATTN_NAME = Key.ATTN_NAME;
    public static Key ATTN_WAIT = Key.ATTN_WAIT;
    public static Key BACK_TAB = Key.BACK_TAB;
    public static int BACK_TAB_ID = Key.BACK_TAB_ID;
    public static String BACK_TAB_NAME = Key.BACK_TAB_NAME;
    public static Key BACKSPACE = Key.BACKSPACE;
    public static int BACKSPACE_ID = Key.BACKSPACE_ID;
    public static String BACKSPACE_NAME = Key.BACKSPACE_NAME;
    public static Key CLEAR = Key.CLEAR;
    public static int CLEAR_ID = Key.CLEAR_ID;
    public static String CLEAR_NAME = Key.CLEAR_NAME;
    public static Key CLEAR_WAIT = Key.CLEAR_WAIT;
    public static Key CURSOR_SELECT = Key.CURSOR_SELECT;
    public static int CURSOR_SELECT_ID = Key.CURSOR_SELECT_ID;
    public static String CURSOR_SELECT_NAME = Key.CURSOR_SELECT_NAME;
    public static Key CURSOR_SELECT_WAIT = Key.CURSOR_SELECT_WAIT;
    public static Key DELETE = Key.DELETE;
    public static int DELETE_ID = Key.DELETE_ID;
    public static String DELETE_NAME = Key.DELETE_NAME;
    public static Key DOWN_ARROW = Key.DOWN_ARROW;
    public static int DOWN_ARROW_ID = Key.DOWN_ARROW_ID;
    public static String DOWN_ARROW_NAME = Key.DOWN_ARROW_NAME;
    public static Key DUPLICATE = Key.DUPLICATE;
    public static int DUPLICATE_ID = Key.DUPLICATE_ID;
    public static String DUPLICATE_NAME = Key.DUPLICATE_NAME;
    public static Key ENTER = Key.ENTER;
    public static int ENTER_ID = Key.ENTER_ID;
    public static String ENTER_NAME = Key.ENTER_NAME;
    public static Key ENTER_WAIT = Key.ENTER_WAIT;
    public static Key ERASE_EOF = Key.ERASE_EOF;
    public static int ERASE_EOF_ID = Key.ERASE_EOF_ID;
    public static String ERASE_EOF_NAME = Key.ERASE_EOF_NAME;
    public static Key ERASE_INPUT = Key.ERASE_INPUT;
    public static int ERASE_INPUT_ID = Key.ERASE_INPUT_ID;
    public static String ERASE_INPUT_NAME = Key.ERASE_INPUT_NAME;
    public static Key HOME = Key.HOME;
    public static int HOME_ID = Key.HOME_ID;
    public static String HOME_NAME = Key.HOME_NAME;
    public static Key INSERT = Key.INSERT;
    public static int INSERT_ID = Key.INSERT_ID;
    public static String INSERT_NAME = Key.INSERT_NAME;
    public static Key KEYPAD_0 = Key.KEYPAD_0;
    public static int KEYPAD_0_ID = Key.KEYPAD_0_ID;
    public static String KEYPAD_0_NAME = Key.KEYPAD_0_NAME;
    public static Key KEYPAD_1 = Key.KEYPAD_1;
    public static int KEYPAD_1_ID = Key.KEYPAD_1_ID;
    public static String KEYPAD_1_NAME = Key.KEYPAD_1_NAME;
    public static Key KEYPAD_2 = Key.KEYPAD_2;
    public static int KEYPAD_2_ID = Key.KEYPAD_2_ID;
    public static String KEYPAD_2_NAME = Key.KEYPAD_2_NAME;
    public static Key KEYPAD_3 = Key.KEYPAD_3;
    public static int KEYPAD_3_ID = Key.KEYPAD_3_ID;
    public static String KEYPAD_3_NAME = Key.KEYPAD_3_NAME;
    public static Key KEYPAD_4 = Key.KEYPAD_4;
    public static int KEYPAD_4_ID = Key.KEYPAD_4_ID;
    public static String KEYPAD_4_NAME = Key.KEYPAD_4_NAME;
    public static Key KEYPAD_5 = Key.KEYPAD_5;
    public static int KEYPAD_5_ID = Key.KEYPAD_5_ID;
    public static String KEYPAD_5_NAME = Key.KEYPAD_5_NAME;
    public static Key KEYPAD_6 = Key.KEYPAD_6;
    public static int KEYPAD_6_ID = Key.KEYPAD_6_ID;
    public static String KEYPAD_6_NAME = Key.KEYPAD_6_NAME;
    public static Key KEYPAD_7 = Key.KEYPAD_7;
    public static int KEYPAD_7_ID = Key.KEYPAD_7_ID;
    public static String KEYPAD_7_NAME = Key.KEYPAD_7_NAME;
    public static Key KEYPAD_8 = Key.KEYPAD_8;
    public static int KEYPAD_8_ID = Key.KEYPAD_8_ID;
    public static String KEYPAD_8_NAME = Key.KEYPAD_8_NAME;
    public static Key KEYPAD_9 = Key.KEYPAD_9;
    public static int KEYPAD_9_ID = Key.KEYPAD_9_ID;
    public static String KEYPAD_9_NAME = Key.KEYPAD_9_NAME;
    public static Key KEYPAD_COMMA = Key.KEYPAD_COMMA;
    public static int KEYPAD_COMMA_ID = Key.KEYPAD_COMMA_ID;
    public static String KEYPAD_COMMA_NAME = Key.KEYPAD_COMMA_NAME;
    public static Key KEYPAD_HYPHEN = Key.KEYPAD_HYPHEN;
    public static int KEYPAD_HYPHEN_ID = Key.KEYPAD_HYPHEN_ID;
    public static String KEYPAD_HYPHEN_NAME = Key.KEYPAD_HYPHEN_NAME;
    public static Key KEYPAD_PERIOD = Key.KEYPAD_PERIOD;
    public static int KEYPAD_PERIOD_ID = Key.KEYPAD_PERIOD_ID;
    public static String KEYPAD_PERIOD_NAME = Key.KEYPAD_PERIOD_NAME;
    public static Key KEYPAD_RETURN = Key.KEYPAD_RETURN;
    public static int KEYPAD_RETURN_ID = Key.KEYPAD_RETURN_ID;
    public static String KEYPAD_RETURN_NAME = Key.KEYPAD_RETURN_NAME;
    public static Key LEFT_ARROW = Key.LEFT_ARROW;
    public static int LEFT_ARROW_ID = Key.LEFT_ARROW_ID;
    public static String LEFT_ARROW_NAME = Key.LEFT_ARROW_NAME;
    public static int MAX_ID = Key.MAX_ID;
    public static Key NEWLINE = Key.NEWLINE;
    public static int NEWLINE_ID = Key.NEWLINE_ID;
    public static String NEWLINE_NAME = Key.NEWLINE_NAME;
    public static Key PA1 = Key.PA1;
    public static int PA1_ID = Key.PA1_ID;
    public static String PA1_NAME = Key.PA1_NAME;
    public static Key PA1_WAIT = Key.PA1_WAIT;
    public static Key PA2 = Key.PA2;
    public static int PA2_ID = Key.PA2_ID;
    public static String PA2_NAME = Key.PA2_NAME;
    public static Key PA2_WAIT = Key.PA2_WAIT;
    public static Key PA3 = Key.PA3;
    public static int PA3_ID = Key.PA3_ID;
    public static String PA3_NAME = Key.PA3_NAME;
    public static Key PA3_WAIT = Key.PA3_WAIT;
    public static Key PF1 = Key.PF1;
    public static int PF1_ID = Key.PF1_ID;
    public static String PF1_NAME = Key.PF1_NAME;
    public static Key PF1_WAIT = Key.PF1_WAIT;
    public static Key PF10 = Key.PF10;
    public static int PF10_ID = Key.PF10_ID;
    public static String PF10_NAME = Key.PF10_NAME;
    public static Key PF10_WAIT = Key.PF10_WAIT;
    public static Key PF11 = Key.PF11;
    public static int PF11_ID = Key.PF11_ID;
    public static String PF11_NAME = Key.PF11_NAME;
    public static Key PF11_WAIT = Key.PF11_WAIT;
    public static Key PF12 = Key.PF12;
    public static int PF12_ID = Key.PF12_ID;
    public static String PF12_NAME = Key.PF12_NAME;
    public static Key PF12_WAIT = Key.PF12_WAIT;
    public static Key PF13 = Key.PF13;
    public static int PF13_ID = Key.PF13_ID;
    public static String PF13_NAME = Key.PF13_NAME;
    public static Key PF13_WAIT = Key.PF13_WAIT;
    public static Key PF14 = Key.PF14;
    public static int PF14_ID = Key.PF14_ID;
    public static String PF14_NAME = Key.PF14_NAME;
    public static Key PF14_WAIT = Key.PF14_WAIT;
    public static Key PF15 = Key.PF15;
    public static int PF15_ID = Key.PF15_ID;
    public static String PF15_NAME = Key.PF15_NAME;
    public static Key PF15_WAIT = Key.PF15_WAIT;
    public static Key PF16 = Key.PF16;
    public static int PF16_ID = Key.PF16_ID;
    public static String PF16_NAME = Key.PF16_NAME;
    public static Key PF16_WAIT = Key.PF16_WAIT;
    public static Key PF17 = Key.PF17;
    public static int PF17_ID = Key.PF17_ID;
    public static String PF17_NAME = Key.PF17_NAME;
    public static Key PF17_WAIT = Key.PF17_WAIT;
    public static Key PF18 = Key.PF18;
    public static int PF18_ID = Key.PF18_ID;
    public static String PF18_NAME = Key.PF18_NAME;
    public static Key PF18_WAIT = Key.PF18_WAIT;
    public static Key PF19 = Key.PF19;
    public static int PF19_ID = Key.PF19_ID;
    public static String PF19_NAME = Key.PF19_NAME;
    public static Key PF19_WAIT = Key.PF19_WAIT;
    public static Key PF2 = Key.PF2;
    public static int PF2_ID = Key.PF2_ID;
    public static String PF2_NAME = Key.PF2_NAME;
    public static Key PF2_WAIT = Key.PF2_WAIT;
    public static Key PF20 = Key.PF20;
    public static int PF20_ID = Key.PF20_ID;
    public static String PF20_NAME = Key.PF20_NAME;
    public static Key PF20_WAIT = Key.PF20_WAIT;
    public static Key PF21 = Key.PF21;
    public static int PF21_ID = Key.PF21_ID;
    public static String PF21_NAME = Key.PF21_NAME;
    public static Key PF21_WAIT = Key.PF21_WAIT;
    public static Key PF22 = Key.PF22;
    public static int PF22_ID = Key.PF22_ID;
    public static String PF22_NAME = Key.PF22_NAME;
    public static Key PF22_WAIT = Key.PF22_WAIT;
    public static Key PF23 = Key.PF23;
    public static int PF23_ID = Key.PF23_ID;
    public static String PF23_NAME = Key.PF23_NAME;
    public static Key PF23_WAIT = Key.PF23_WAIT;
    public static Key PF24 = Key.PF24;
    public static int PF24_ID = Key.PF24_ID;
    public static String PF24_NAME = Key.PF24_NAME;
    public static Key PF24_WAIT = Key.PF24_WAIT;
    public static Key PF3 = Key.PF3;
    public static int PF3_ID = Key.PF3_ID;
    public static String PF3_NAME = Key.PF3_NAME;
    public static Key PF3_WAIT = Key.PF3_WAIT;
    public static Key PF4 = Key.PF4;
    public static int PF4_ID = Key.PF4_ID;
    public static String PF4_NAME = Key.PF4_NAME;
    public static Key PF4_WAIT = Key.PF4_WAIT;
    public static Key PF5 = Key.PF5;
    public static int PF5_ID = Key.PF5_ID;
    public static String PF5_NAME = Key.PF5_NAME;
    public static Key PF5_WAIT = Key.PF5_WAIT;
    public static Key PF6 = Key.PF6;
    public static int PF6_ID = Key.PF6_ID;
    public static String PF6_NAME = Key.PF6_NAME;
    public static Key PF6_WAIT = Key.PF6_WAIT;
    public static Key PF7 = Key.PF7;
    public static int PF7_ID = Key.PF7_ID;
    public static String PF7_NAME = Key.PF7_NAME;
    public static Key PF7_WAIT = Key.PF7_WAIT;
    public static Key PF8 = Key.PF8;
    public static int PF8_ID = Key.PF8_ID;
    public static String PF8_NAME = Key.PF8_NAME;
    public static Key PF8_WAIT = Key.PF8_WAIT;
    public static Key PF9 = Key.PF9;
    public static int PF9_ID = Key.PF9_ID;
    public static String PF9_NAME = Key.PF9_NAME;
    public static Key PF9_WAIT = Key.PF9_WAIT;
    public static Key RESET = Key.RESET;
    public static int RESET_ID = Key.RESET_ID;
    public static String RESET_NAME = Key.RESET_NAME;
    public static Key RIGHT_ARROW = Key.RIGHT_ARROW;
    public static int RIGHT_ARROW_ID = Key.RIGHT_ARROW_ID;
    public static String RIGHT_ARROW_NAME = Key.RIGHT_ARROW_NAME;
    public static Key SYSREQ = Key.SYSREQ;
    public static int SYSREQ_ID = Key.SYSREQ_ID;
    public static String SYSREQ_NAME = Key.SYSREQ_NAME;
    public static Key SYSREQ_WAIT = Key.SYSREQ_WAIT;
    public static Key TAB = Key.TAB;
    public static int TAB_ID = Key.TAB_ID;
    public static String TAB_NAME = Key.TAB_NAME;
    public static Key UP_ARROW = Key.UP_ARROW;
    public static int UP_ARROW_ID = Key.UP_ARROW_ID;
    public static String UP_ARROW_NAME = Key.UP_ARROW_NAME;

} // VTConnect
