/******************************************************************************
* (C) Copyright 2014
*
* Resolve Systems, LLC
*
* All rights reserved
* This software is distributed under license and may not be used, copied,
* modified, or distributed without the express written permission of
* Resolve Systems, LLC.
*
******************************************************************************/

package com.resolve.connect;

import com.resolve.util.Constants;

/**
 * Helper class used to generate MSSQL JDBC URLs and the name of the Driver class used by Resolve.
 * <br>URLs are in the form of jdbc:sqlserver://<i>hostname</i>:<i>port</i>;databaseName=<i>dbname</i>
 * @author justin.geiser
 *
 */
public class MSSQLDBConnect
{
    /**
     * Uses the hostname and dbname to generate the JDBC URL.  The port used will be 1433 unless the hostname if formated like <i>hostname</i>:<i>port</i>
     * @param hostname
     * @param dbname
     * @return
     */
    public static String getConnectURL(String hostname, String dbname)
    {
        DBHost dbHost = DBConnectHelper.parseHost(hostname, 1433);
        return getConnectURL(dbHost.host, dbHost.port, dbname);
    } // getConnectURL
    
    /**
     * Uses the hostname, port, and dbname to generate the JDBC URL.
     * @param hostname
     * @param port
     * @param dbname
     * @return
     */
    public static String getConnectURL(String hostname, int port, String dbname)
    {
        String url = "jdbc:sqlserver://" + hostname + ":" + port;
        if (dbname != null)
        {
	        url += ";databaseName=" + dbname;
        }
        return url;
    } // getConnectURL
    
    /**
     * Returns the name of the MSSQL driver shipped with Resolve
     * @return
     */
    public static String getDriver()
    {
        return Constants.MSSQLDRIVER;
    } // getDriver
    
} // MSSQLDBConnect
