/******************************************************************************
* (C) Copyright 2014
*
* Resolve Systems, LLC
*
* All rights reserved
* This software is distributed under license and may not be used, copied,
* modified, or distributed without the express written permission of
* Resolve Systems, LLC.
*
******************************************************************************/

package com.resolve.connect;

import com.resolve.util.Constants;

/**
 * Helper class used to generate DB2 JDBC URLs and the name of the Driver class used by Resolve.
 * <br>URLs are in the form of jdbc:db2://<i>hostname</i>:<i>port</i>/<i>dbname</i>
 * @author justin.geiser
 *
 */
public class DB2DBConnect
{
    /**
     * Uses the hostname and dbname to generate the JDBC URL.  The port used will be 50000 unless the hostname if formated like <i>hostname</i>:<i>port</i>
     * @param hostname
     * @param dbname
     * @return
     */
    public static String getConnectURL(String hostname, String dbname)
    {
        DBHost dbHost = DBConnectHelper.parseHost(hostname, 50000);
        return getConnectURL(dbHost.host, dbHost.port, dbname);
    } // getConnectURL
    
    /**
     * Uses the hostname, port, and dbname to generate the JDBC URL.
     * @param hostname
     * @param port
     * @param dbname
     * @return
     */
    public static String getConnectURL(String hostname, int port, String dbname)
    {
        return "jdbc:db2://"+hostname+":"+port+"/"+dbname;
    } // getConnectURL
    
    /**
     * Returns the name of the DB2 driver shipped with Resolve
     * @return
     */
    public static String getDriver()
    {
        return Constants.DB2DRIVER;
    } // getDriver
    
} // DB2DBConnect
