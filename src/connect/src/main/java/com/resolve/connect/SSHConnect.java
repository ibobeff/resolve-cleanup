/******************************************************************************
* (C) Copyright 2014
*
* Resolve Systems, LLC
*
* All rights reserved
* This software is distributed under license and may not be used, copied,
* modified, or distributed without the express written permission of
* Resolve Systems, LLC.
*
******************************************************************************/

package com.resolve.connect;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.List;

import com.resolve.util.FileUtils;
import com.resolve.util.Log;
import com.resolve.util.StringUtils;

import ch.ethz.ssh2.Connection;
import ch.ethz.ssh2.InteractiveCallback;
import ch.ethz.ssh2.SCPClient;
import ch.ethz.ssh2.Session;

public class SSHConnect extends Connect
{
    public final static String P_ASSWORD = "PASSWORD";
    public final static String PUBLICKEY = "PUBLICKEY";
    public final static String INTERACTIVE = "INTERACTIVE";
    public final static String NONE = "NONE";
    public String authMethod = P_ASSWORD;  // password, publickey, interactive

    // auth fields
    protected String username;
    protected String password;
    protected File privateKeyFile;
    protected String privateKeyPassphrase;
    protected List responses;

    protected Connection conn;    // ssh connection
    protected Session sess;       // ssh session
    protected SCPClient scp;      // scp client
    protected String hostname;    // destination host
    protected int port;           // destination port
    protected int timeout;        // connect timeout
    protected InputStream in;     // stdout from ssh session
    protected InputStream err;    // stderr from ssh session
    protected OutputStream out;   // stdin to ssh session

    protected int x;              // terminal x size
    protected int y;              // terminal y size

    protected String prompt;      // Prompt
    
    public SSHConnect() throws Exception
    {
        super();
    }

    /*
     * USERNAME/PASSWORD
     */
    public SSHConnect(String username, String password, String hostname) throws Exception
    {
        this(username, password, hostname, 22, 0);
    } // SSHConnect
    
    public SSHConnect(String username, String password, String hostname, String prompt, boolean userNamePasswdLogin) throws Exception
    {
        this(username, password, hostname, 22, 0, prompt);
    } // SSHConnect

    public SSHConnect(String username, String password, String hostname, int port) throws Exception
    {
        this(username, password, hostname, port, 0);
    } // SSHConnect
    
    public SSHConnect(String username, String password, String hostname, int port, String prompt) throws Exception
    {
        this(username, password, hostname, port, 0, prompt);
    } // SSHConnect

    public SSHConnect(String username, String password, String hostname, int port, int timeout) throws Exception
    {
        this(username, password, hostname, port, timeout, true);
    } // SSHConnect

    public SSHConnect(String username, String password, String hostname, int port, int timeout, String prompt) throws Exception
    {
        this(username, password, hostname, port, timeout, true, prompt);
    } // SSHConnect
    
    public SSHConnect(String username, String password, String hostname, int port, int timeout, boolean isConnect) throws Exception
    {
        this(username, password, hostname, port, timeout, isConnect, null);
    } // SSHConnect
    
    public SSHConnect(String username, String password, String hostname, int port, int timeout, boolean isConnect, String prompt) throws Exception
    {
        super();

        authMethod = P_ASSWORD;

        this.username = username;
        this.password = password;
        this.hostname = hostname;
        this.port = port;
        this.timeout = timeout * 1000;
        this.prompt = prompt;

        if(isConnect)
        {
            connect();
        }
    } // SSHConnect

    /*
     * PUBLIC KEY
     */
    public SSHConnect(String username, String filename, String passphrase, String hostname) throws Exception
    {
        this(username, filename, passphrase, hostname, 22, 0);
    } // SSHConnect
    
    public SSHConnect(String username, String filename, String passphrase, String hostname, String prompt) throws Exception
    {
        this(username, filename, passphrase, hostname, 22, 0, prompt);
    } // SSHConnect

    public SSHConnect(String username, String filename, String passphrase, String hostname, int port) throws Exception
    {
        this(username, filename, passphrase, hostname, port, 0);
    } // SSHConnect
    
    public SSHConnect(String username, String filename, String passphrase, String hostname, int port, String prompt) throws Exception
    {
        this(username, filename, passphrase, hostname, port, 0, prompt);
    } // SSHConnect

    public SSHConnect(String username, String filename, String passphrase, String hostname, int port, int timeout) throws Exception
    {
        this(username, filename, passphrase, hostname, port, timeout, true, null);
    } // SSHConnect
    
    public SSHConnect(String username, String filename, String passphrase, String hostname, int port, int timeout, String prompt) throws Exception
    {
        this(username, filename, passphrase, hostname, port, timeout, true, prompt);
    } // SSHConnect
    
    public SSHConnect(String username, String filename, String passphrase, String hostname, int port, int timeout, boolean isConnect) throws Exception
    {
        this(username, filename, passphrase, hostname, port, timeout, isConnect, null);
    }

    public SSHConnect(String username, String filename, String passphrase, String hostname, int port, int timeout, boolean isConnect, String prompt) throws Exception
    {
        authMethod = PUBLICKEY;

        File file = new File(filename);
        if (!file.exists())
        {
            throw new Exception("Unable to find private key file: "+file.getAbsolutePath());
        }

        this.username = username;
        this.privateKeyFile = file;
        this.privateKeyPassphrase = passphrase;
        this.hostname = hostname;
        this.port = port;
        this.timeout = timeout * 1000;
        this.prompt = prompt;

        if(isConnect)
        {
            connect();
        }
    } // SSHConnect

    /*
     * INTERACTIVE
     */
    public SSHConnect(String username, List responses, String hostname) throws Exception
    {
        this(username, responses, hostname, 22);
    } // SSHConnect
    
    public SSHConnect(String username, List responses, String hostname, String prompt) throws Exception
    {
        this(username, responses, hostname, 22, prompt);
    } // SSHConnect

    public SSHConnect(String username, List responses, String hostname, int port) throws Exception
    {
        this(username, responses, hostname, port, 0);
    } // SSHConnect
    
    public SSHConnect(String username, List responses, String hostname, int port, String prompt) throws Exception
    {
        this(username, responses, hostname, port, 0, prompt);
    } // SSHConnect

    public SSHConnect(String username, List responses, String hostname, int port, int timeout) throws Exception
    {
        this(username, responses, hostname, port, timeout, true);
    } // SSHConnect
    
    public SSHConnect(String username, List responses, String hostname, int port, int timeout, String prompt) throws Exception
    {
        this(username, responses, hostname, port, timeout, true, prompt);
    } // SSHConnect
    
    public SSHConnect(String username, List responses, String hostname, int port, int timeout, boolean isConnect) throws Exception
    {
        this(username, responses, hostname, port, timeout, true, null);
    } // SSHConnect

    public SSHConnect(String username, List responses, String hostname, int port, int timeout, boolean isConnect, String prompt) throws Exception
    {
        authMethod = INTERACTIVE;

        this.username = username;
        this.responses = responses;
        this.hostname = hostname;
        this.port = port;
        this.timeout = timeout * 1000;
        this.prompt = prompt;

        if(isConnect)
        {
            connect();
        }
    } // SSHConnect

    /*
     * NONE - no SSH authentication
     */
    public SSHConnect(String username, String hostname) throws Exception
    {
        this(username, hostname, 22, 0);
    } // SSHConnect

    public SSHConnect(String username, String hostname, int port) throws Exception
    {
        this(username, hostname, port, 0);
    } // SSHConnect

    public SSHConnect(String username, String hostname, int port, int timeout) throws Exception
    {
        this(username, hostname, port, timeout, true);
    } // SSHConnect

    public SSHConnect(String username, String hostname, int port, int timeout, boolean isConnect) throws Exception
    {
        authMethod = NONE;

        this.username = username;
        this.password = "";
        this.hostname = hostname;
        this.port = port;
        this.timeout = timeout * 1000;

        if(isConnect)
        {
            connect();
        }
    } // SSHConnect


    /*
     * QUERY for authentication methods
     */

    public static String getAuthMethods(String username, String hostname) throws Exception
    {
        return getAuthMethods(username, hostname, 22, 10);
    } //getAuthMethods

    public static String getAuthMethods(String username, String hostname, int port) throws Exception
    {
        return getAuthMethods(username, hostname, port, 10);
    } //getAuthMethods

    public static String getAuthMethods(String username, String hostname, int port, int timeout) throws Exception
    {
        String result = "";

        Log.log.debug("SSHConnect to "+hostname+":"+port);
        Connection conn = new Connection(hostname, port);

        int connTimeout = timeout * 1000;
        Log.log.trace("  connecting");
        conn.connect(null, connTimeout, connTimeout);

        String[] queryMethods = conn.getRemainingAuthMethods(username);
        if (queryMethods != null)
        {
            result = StringUtils.join(queryMethods, ",");
        }
        if(Log.log.isTraceEnabled())
        {
            Log.log.trace("Available Authentication Methods: "+result);
        }
        conn.close();

        return result;
    } //getAuthMethods


    public static boolean isInteractive(String authMethods)
    {
        boolean result = false;
        if (authMethods != null)
        {
            result = authMethods.toUpperCase().contains("KEYBOARD-INTERACTIVE");
        }
        return result;
    } //isInteractive

    public static boolean isPassword(String authMethods)
    {
        boolean result = false;
        if (authMethods != null)
        {
            result = authMethods.toUpperCase().contains("PASSWORD");
        }
        return result;
    } //isPassword

    public static boolean isPublicKey(String authMethods)
    {
        boolean result = false;
        if (authMethods != null)
        {
            result = authMethods.toUpperCase().contains("PUBLICKEY");
        }
        return result;
    } //isPublicKey
    
    public void connect() throws Exception
    {
        try
        {
            /* Create a connection instance */
            Log.log.debug("SSHConnect to "+hostname+":"+port);
            if (hostname == null)
            {
                throw new Exception("Missing hostname");
            }
            conn = new Connection(hostname, port);

            /* Now connect */
            Log.log.trace("  connecting");
            conn.connect(null, timeout, timeout);

            /* Authenticate */
            Log.log.trace("  authenticating mode: "+authMethod);
            boolean isAuthenticated = false;
            if (authMethod.equalsIgnoreCase(P_ASSWORD))
            {
                isAuthenticated = conn.authenticateWithPassword(username, password);
            }
            else if (authMethod.equalsIgnoreCase(PUBLICKEY))
            {
                /*
                NOTE PUTTY USERS: Event though your key file may start with "-----BEGIN..." it is not in the expected format. You have to convert it to the OpenSSH key format by using the "puttygen" tool (can be downloaded from the Putty website). Simply load your key and then use the "Conversions/Export OpenSSH key" functionality to get a proper PEM file.

                Parameters:
                    user - A String holding the username.
                    pemFile - A File object pointing to a file containing a DSA or RSA private key of the user in OpenSSH key format (PEM, you can't miss the "-----BEGIN DSA PRIVATE KEY-----" or "-----BEGIN RSA PRIVATE KEY-----" tag).
                    password - If the PEM file is encrypted then you must specify the password. Otherwise, this argument will be ignored and can be set to null.
                */
                isAuthenticated = conn.authenticateWithPublicKey(username, privateKeyFile, privateKeyPassphrase);
            }
            else if (authMethod.equalsIgnoreCase(INTERACTIVE))
            {
                SSHConnectInteractiveCallback callback = new SSHConnectInteractiveCallback(responses, timeout);
                isAuthenticated = conn.authenticateWithKeyboardInteractive(username, callback);

                if (responses == null)
                {
                    throw new Exception(callback.getMessage());
                }
            }
            else if (authMethod.equalsIgnoreCase(NONE))
            {
                isAuthenticated = conn.authenticateWithNone(username);
            }

            if (isAuthenticated == false)
            {
                throw new Exception("Authentication failed.");
            }

            /* Create a session */
            Log.log.trace("  open session");
            Session sess = conn.openSession();

            // terminal size
            x = 80;
            y = 24;

            sess.requestPTY("dumb", x, y, 0, 0, null);
            sess.startShell();

            // set streams
            in = sess.getStdout();
            err = sess.getStderr();
            out = sess.getStdin();

            Log.log.trace("  read results");
            // start session consumer
            
            if (StringUtils.isBlank(prompt))
            {
                reader = new SSHReader(in);
            }
            else
            {
                reader = new SSHReader(in, prompt);
            }
            
            ((SSHReader)reader).start();

            super.connect();
        }
        catch (Exception e)
        {
            Log.log.error("SSHConnect connect exception: "+e.getMessage(), e);
            reader = new SSHReader(e.getMessage());
            throw e;
        }
    } // connect

    /**
     * SCP a file from the local system to a directory on the remote system using mode 0644
     * @param fromFile File to SCP
     * @param toDirectory Directory to copy to
     * @throws FileNotFoundException If local file can not be found
     * @throws IOException
     */
    public void put(String fromFile, String toDirectory) throws FileNotFoundException, IOException
    {
        try
        {
            File file = new File(fromFile);
            if (!file.isFile())
            {
                Log.log.warn(fromFile + " (" + file.getAbsolutePath() + ") could not be found");
                throw new FileNotFoundException(fromFile + " (" + file.getAbsolutePath() + ") could not be found");
            }
            else
            {
                if (scp == null)
                {
                    scp = new SCPClient(conn);
                }
                scp.put(fromFile, toDirectory, "0644");
            }
        }
        catch (FileNotFoundException fnfe)
        {
            throw fnfe;
        }
        catch (IOException ioe)
        {
            Log.log.warn("Failed to SCP File " + fromFile + " to " + toDirectory + ": " + ioe.getMessage(), ioe);
            throw ioe;
        }
    } // put

    /**
     * SCP a file from the local system to a directory on the remote system using the specified mode
     * @param fromFile File to SCP
     * @param toDirectory Directory to copy to
     * @param mode a four digit string (e.g. 0644, see "man chmod", "man open")
     * @throws FileNotFoundException If local file can not be found
     * @throws NumberFormatException If mode is an invalid number
     * @throws IOException
     */
    public void put(String fromFile, String toDirectory, String mode) throws FileNotFoundException, NumberFormatException, IOException
    {
        try
        {
            File file = new File(fromFile);
            while (mode.length() < 4)
            {
                mode = "0" + mode;
            }
            if (!mode.matches("[0-7]{4}"))
            {
                Log.log.warn("Invalid Mode: " + mode);
                throw new NumberFormatException("Invalid Mode - " + mode);
            }
            else if (!file.isFile())
            {
                Log.log.warn(fromFile + " (" + file.getAbsolutePath() + ") could not be found");
                throw new FileNotFoundException(fromFile + " (" + file.getAbsolutePath() + ") could not be found");
            }
            else
            {
                if (scp == null)
                {
                    scp = new SCPClient(conn);
                }
                scp.put(fromFile, toDirectory, mode);
            }
        }
        catch (IOException ioe)
        {
            Log.log.warn("Failed to SCP File " + fromFile + " to " + toDirectory + ": " + ioe.getMessage(), ioe);
            throw ioe;
        }
    } // put

    /**
     * SCP a file from the local system to a directory on the remote system using mode 0644
     * @param fromFile File to SCP
     * @param toDirectory Directory to copy to
     * @throws FileNotFoundException If local file can not be found
     * @throws IOException
     */
    public void put(File fromFile, String toDirectory) throws FileNotFoundException, IOException
    {
        try
        {
            if (!fromFile.isFile())
            {
                Log.log.warn(fromFile.getAbsolutePath() + " could not be found");
                throw new FileNotFoundException(fromFile.getAbsolutePath() + " could not be found");
            }
            else
            {
                if (scp == null)
                {
                    scp = new SCPClient(conn);
                }
                scp.put(fromFile.getAbsolutePath(), toDirectory, "0644");
            }
        }
        catch (IOException ioe)
        {
            Log.log.warn("Failed to SCP File " + fromFile.getAbsolutePath() + " to " + toDirectory + ": " + ioe.getMessage(), ioe);
            throw ioe;
        }
    } // put

    /**
     * SCP a file from the local directory to a directory on the remote system using the specified mode
     * @param fromFile File to SCP
     * @param toDirectory Directory to copy to
     * @param mode a four digit string (e.g. 0644, see "man chmod", "man open")
     * @throws FileNotFoundException If local file can not be found
     * @throws NumberFormatException If mode is an invalid number
     * @throws IOException
     */
    public void put(File fromFile, String toDirectory, String mode) throws FileNotFoundException, NumberFormatException, IOException
    {
        try
        {
            while (mode.length() < 4)
            {
                mode = "0" + mode;
            }
            if (!mode.matches("[0-7]{4}"))
            {
                Log.log.warn("Invalid Mode: " + mode);
                throw new NumberFormatException("Invalid Mode - " + mode);
            }
            if (!fromFile.isFile())
            {
                Log.log.warn(fromFile.getAbsolutePath() + " could not be found");
                throw new FileNotFoundException(fromFile.getAbsolutePath() + " could not be found");
            }
            else
            {
                if (scp == null)
                {
                    scp = new SCPClient(conn);
                }
                scp.put(fromFile.getAbsolutePath(), toDirectory, mode);
            }
        }
        catch (IOException ioe)
        {
            Log.log.warn("Failed to SCP File " + fromFile.getAbsolutePath() + " to " + toDirectory + ": " + ioe.getMessage(), ioe);
            throw ioe;
        }
    } // put

    /**
     * SCP a file to the home directory on a remote system using mode 0644
     * @param fromFile File to SCP
     * @throws FileNotFoundException If local file can not be found
     * @throws IOException
     */
    public void put(String fromFile) throws FileNotFoundException, IOException
    {
        try
        {
            File file = new File(fromFile);
            if (!file.isFile())
            {
                Log.log.warn(fromFile + " (" + file.getAbsolutePath() + ") could not be found");
                throw new FileNotFoundException(fromFile + " (" + file.getAbsolutePath() + ") could not be found");
            }
            else
            {
                if (scp == null)
                {
                    scp = new SCPClient(conn);
                }
                scp.put(fromFile, ".", "0644");
            }
        }
        catch (IOException ioe)
        {
            Log.log.warn("Failed to SCP File " + fromFile + ": " + ioe.getMessage(), ioe);
            throw ioe;
        }
    } // put

    /**
     * SCP a file to the home directory on a remote system using mode 0644
     * @param fromFile File to SCP
     * @throws FileNotFoundException If local file can not be found
     * @throws IOException
     */
    public void put(File fromFile) throws FileNotFoundException, IOException
    {
        try
        {
            if (!fromFile.isFile())
            {
                Log.log.warn(fromFile.getAbsolutePath() + " could not be found");
                throw new FileNotFoundException(fromFile.getAbsolutePath() + " could not be found");
            }
            else
            {
                if (scp == null)
                {
                    scp = new SCPClient(conn);
                }
                scp.put(fromFile.getAbsolutePath(), ".", "0644");
            }
        }
        catch (IOException ioe)
        {
            Log.log.warn("Failed to SCP File " + fromFile.getAbsolutePath() + ": " + ioe.getMessage(), ioe);
            throw ioe;
        }
    } // put

    /**
     * SCP a file from the local system to a directory on the remote system using the specified mode
     *
     * @param fileName name of the file to be stored.
     * @param fileContent content of a file to SCP. The file content is stored as a random file in the resolve "tmp" folder
     * and then ship out to the remote server.
     * @param toDirectory Directory to copy to
     * @param mode a four digit string (e.g. 0644, see "man chmod", "man open")
     * @throws ConnectException
     */
    public void putFileContent(String fileName, String fileContent, String toDirectory, String mode) throws ConnectException
    {
        File file = null;

        try
        {
            file = FileUtils.saveRandomTextFile(fileName, fileContent, null);

            while (mode.length() < 4)
            {
                mode = "0" + mode;
            }
            if (!mode.matches("[0-7]{4}"))
            {
                Log.log.warn("Invalid Mode: " + mode);
                throw new NumberFormatException("Invalid Mode - " + mode);
            }
            else if (!file.isFile())
            {
                Log.log.warn(file + " (" + file.getAbsolutePath() + ") could not be found");
                throw new FileNotFoundException(file + " (" + file.getAbsolutePath() + ") could not be found");
            }
            else
            {
                if (scp == null)
                {
                    scp = new SCPClient(conn);
                }
                scp.put(file.getAbsolutePath(), toDirectory, mode);
            }
        }
        catch (IOException ioe)
        {
            Log.log.warn("Failed to SCP to " + toDirectory + ": " + ioe.getMessage(), ioe);
            throw new ConnectException(ioe.getMessage(), ioe);
        }
        catch (Exception e)
        {
            Log.log.warn(e.getMessage(), e);
            throw new ConnectException(e.getMessage(), e);
        }
        finally
        {
            //delete the randomly created file.
            if(file!=null)
            {
                FileUtils.deleteQuietly(file);
            }
        }
    } // put

    /**
     * SCP a remote file to the local system
     * @param remoteFile Remote file to copy
     * @param localDirectory Local directory to copy file to
     * @return true if local file exists after transfer, false otherwise
     * @throws FileNotFoundException If local directory can not be found or is not a directory
     * @throws IOException
     */
    public boolean get(String remoteFile, String localDirectory) throws FileNotFoundException, IOException
    {
        boolean result = false;

        try
        {
            File localDir = new File(localDirectory);
            if (!localDir.isDirectory())
            {
                Log.log.warn(localDirectory + " could not be found");
                throw new FileNotFoundException(localDirectory + " could not be found");
            }
            else
            {
                if (scp == null)
                {
                    scp = new SCPClient(conn);
                }
                scp.get(remoteFile, localDirectory);

                File tmpRemoteFile = new File(remoteFile);
                File localFile = new File(localDirectory + "/" + tmpRemoteFile.getName());
                if (localFile.exists())
                {
	                result = true;
                }
            }
        }
        catch (IOException ioe)
        {
            Log.log.warn("Failed to Get File " + remoteFile + " to " + localDirectory + ": " + ioe.getMessage(), ioe);
            throw ioe;
        }

        return result;
    } // get

    @Override
    public void close()
    {
        /* Close this session */
        if (sess != null)
        {
            sess.close();
        }

        /* Close the connection */
        if (conn != null)
        {
            conn.close();
        }

        isClosed = true;
    } // close

    /**
     * Sends a command to the remote system connected to.
     * A newline character will automatically be added to the line to simulate hitting the
     * 'Enter' key, thereby executing the command
     * @param line Command to send
     * @throws Exception
     */
    public void send(String line) throws Exception
    {
        send(line, true);
    } // send

    /**
     * Sends a command to the remote system connected to.
     * The sendNewline option specifies whether to add a newline character to the sent
     * command, simulating the 'Enter' key, thereby executing the command
     * @param line Command to send
     * @param sendNewline Whether or not to send a newline character with the command
     * @throws Exception
     */
    public void send(String line, boolean sendNewline) throws Exception
    {
        try
        {
            super.send();

            // include line termination
            if (sendNewline)
            {
                line += "\n";
            }
            if(Log.log.isTraceEnabled())
            {
                Log.log.trace("sending: "+line);
            }

            out.write(line.getBytes());
            out.flush();
        }
        catch (Exception e)
        {
            Log.log.warn(e.getMessage());
        }
    } // send

    public String getHostname()
    {
        return hostname;
    }

    public void setHostname(String hostname)
    {
        this.hostname = hostname;
    }

    public int getPort()
    {
        return port;
    }

    public void setPort(int port)
    {
        this.port = port;
    }

    public int getTimeout()
    {
        return timeout;
    }

    public void setTimeout(int timeout)
    {
        this.timeout = timeout;
    }

    public String getUsername()
    {
        return username;
    }

    public void setUsername(String username)
    {
        this.username = username;
    }

    public String getPassword()
    {
        return password;
    }

    public void setPassword(String password)
    {
        this.password = password;
    }

    public String getPrivateKeyFileName()
    {
        return privateKeyFile.getName();
    }

    public String getPassphrase()
    {
        return privateKeyPassphrase;
    }

    public void setPassphrase(String privateKeyPassphrase)
    {
        this.privateKeyPassphrase = privateKeyPassphrase;
    }

    public List getResponses()
    {
        return responses;
    }

    public void setResponses(List responses)
    {
        this.responses = responses;
    }

    @Override
    public int hashCode()
    {
        final int prime = 31;
        int result = super.hashCode();
        result = prime * result + ((hostname == null) ? 0 : hostname.hashCode());
        result = prime * result + ((password == null) ? 0 : password.hashCode());
        result = prime * result + port;
        result = prime * result + ((privateKeyFile == null) ? 0 : privateKeyFile.hashCode());
        result = prime * result + ((privateKeyPassphrase == null) ? 0 : privateKeyPassphrase.hashCode());
        result = prime * result + ((username == null) ? 0 : username.hashCode());
        return result;
    }

    @Override
    public boolean equals(Object obj)
    {
        if (this == obj)
        {
            return true;
        }
        if (!super.equals(obj))
        {
            return false;
        }
        if (!(obj instanceof SSHConnect))
        {
            return false;
        }
        SSHConnect other = (SSHConnect) obj;
        if (hostname == null)
        {
            if (other.hostname != null)
            {
                return false;
            }
        }
        else if (!hostname.equals(other.hostname))
        {
            return false;
        }
        if (password == null)
        {
            if (other.password != null)
            {
                return false;
            }
        }
        else if (!password.equals(other.password))
        {
            return false;
        }
        if (port != other.port)
        {
            return false;
        }
        if (privateKeyFile == null)
        {
            if (other.privateKeyFile != null)
            {
                return false;
            }
        }
        else if (!privateKeyFile.equals(other.privateKeyFile))
        {
            return false;
        }
        if (privateKeyPassphrase == null)
        {
            if (other.privateKeyPassphrase != null)
            {
                return false;
            }
        }
        else if (!privateKeyPassphrase.equals(other.privateKeyPassphrase))
        {
            return false;
        }
        if (username == null)
        {
            if (other.username != null)
            {
                return false;
            }
        }
        else if (!username.equals(other.username))
        {
            return false;
        }
        return true;
    }

    @Override
    public String toString()
    {
        return "SSHConnect [username=" + username + ", hostname=" + hostname + ", port=" + port + "]";
    }

} // SSHConnect

class SSHConnectInteractiveCallback implements InteractiveCallback
{
    String message;
    String[] responses;
    int timeout;
    long startTime;

    public SSHConnectInteractiveCallback(List responseList, int timeout)
    {
        this.message = "";
        this.responses = null;
        if (responseList != null)
        {
            this.responses = new String[responseList.size()];

            int i=0;
            for (Object value : responseList)
            {
                this.responses[i++] = (String)value;
            }
        }
        this.timeout = timeout;
        this.startTime = System.currentTimeMillis();
    } // SSHConnectInteractiveCallback

    public String[] replyToChallenge(String name, String instruction, int numPrompts, String[] prompt, boolean[] echo) throws Exception
    {
        String[] result;

        // capture prompts
        this.message += name+"\n";
        this.message += instruction+"\n";
        this.message += "Prompts: \n";
        this.message += StringUtils.join(prompt, "\n");
        Log.log.trace("Interactive prompts:\n"+message);

        if (timeout > 0 && (System.currentTimeMillis() - startTime) > timeout)
        {
            throw new Exception("Interactive Timeout Expired");
        }
        if (responses == null)
        {
            // init empty responses
            result = new String[numPrompts];
            for (int i=0; i < numPrompts; i++)
            {
                result[i] = "";
            }
            Log.log.trace("Sending empty responses");
        }
        else
        {
            result = new String[numPrompts];
            for (int i=0; i < numPrompts; i++)
            {
                result[i] = ((String) responses[i]).trim();
            }

            String msg = "";

            for (int i=0; i < responses.length; i++)
            {
                msg += responses[i]+"\n";
            }
            Log.log.trace("Sending responses:\n"+msg);
        }

        return result;
    } // replyToChallenge

    public String getMessage()
    {
        return this.message;
    } // getMessage
} // SSHConnectInteractiveCallback
