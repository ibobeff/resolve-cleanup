/******************************************************************************
* (C) Copyright 2014
*
* Resolve Systems, LLC
*
* All rights reserved
* This software is distributed under license and may not be used, copied,
* modified, or distributed without the express written permission of
* Resolve Systems, LLC.
*
******************************************************************************/

package com.resolve.connect;

/**
 * This thread consumes output from the remote server and displays it in the
 * terminal window.
 * 
 */
class WebReader extends AbstractConnectReader
{
    StringBuffer buffer;
    
    public WebReader()
    {
        buffer = new StringBuffer();
    } // WebReader
    
    public synchronized String read()
    {
        String result = "";
        
        if (buffer.length() > 0)
        {
            result = buffer.toString();
            buffer = new StringBuffer();
        }
        
        return result; 
    } // read
    
    public synchronized void clear()
    {
        buffer = new StringBuffer();
    } // clear
    
    public boolean hasContent()
    {
        return buffer.length() > 0;
    } // hasContent
    
    public void set(String contents)
    {
        buffer = new StringBuffer(contents);
    } // set
    
} // SSHReader
