/******************************************************************************
* (C) Copyright 2014
*
* Resolve Systems, LLC
*
* All rights reserved
* This software is distributed under license and may not be used, copied,
* modified, or distributed without the express written permission of
* Resolve Systems, LLC.
*
******************************************************************************/

package com.resolve.connect;

import com.resolve.rsbase.SessionObjectInterface;

public class TN3270oConnect extends OhioConnect implements SessionObjectInterface
{
    public TN3270oConnect(String host, int port) throws Exception
    {
        super("TN3270", host, port);

        this.connect();
    } // TN3270oConnect

} // TN3270oConnect
