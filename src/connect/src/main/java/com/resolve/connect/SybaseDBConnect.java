/******************************************************************************
* (C) Copyright 2014
*
* Resolve Systems, LLC
*
* All rights reserved
* This software is distributed under license and may not be used, copied,
* modified, or distributed without the express written permission of
* Resolve Systems, LLC.
*
******************************************************************************/

package com.resolve.connect;

/**
 * Helper class used to generate Sybase JDBC URLs and the name of the Driver class used by Resolve.
 * <br>URLs are in the form of jdbc:sybase:Tds:<i>hostname</i>:<i>port</i>?ServiceName=<i>dbname</i>
 * @author justin.geiser
 *
 */
public class SybaseDBConnect
{
    /**
     * Uses the hostname to generate the JDBC URL.
     * The port used will be 7100 unless the hostname if formated like <i>hostname</i>:<i>port</i>
     * @param hostname
     * @return
     */
    public static String getConnectURL(String hostname)
    {
        DBHost dbHost = DBConnectHelper.parseHost(hostname, 7100);
        return getConnectURL(dbHost.host, dbHost.port);
    } // getConnectURL
    
    
    /**
     * Uses the hostname and port to generate the JDBC URL.
     * @param hostname
     * @param port
     * @return
     */
    public static String getConnectURL(String hostname, int port)
    {
        return "jdbc:sybase:Tds:" + hostname + ":" + port;
    } // getConnectURL
    
    /**
     * Uses the hostname and dbname to generate the JDBC URL.
     * The port used will be 7100 unless the hostname if formated like <i>hostname</i>:<i>port</i>
     * @param hostname
     * @param dbname
     * @return
     */
    public static String getConnectURL(String hostname, String dbname)
    {
        DBHost dbHost = DBConnectHelper.parseHost(hostname, 7100);
        return getConnectURL(dbHost.host, dbHost.port, dbname);
    } // getConnectURL
    
    
    /**
     * Uses the hostname, port, and dbname to generate the JDBC URL.
     * @param hostname
     * @param port
     * @param dbname
     * @return
     */
    public static String getConnectURL(String hostname, int port, String dbname)
    {
        return "jdbc:sybase:Tds:" + hostname + ":" + port + "?ServiceName=" + dbname;
    } // getConnectURL
    
    /**
     * Returns the name of the Sybase driver shipped with Resolve
     * @return
     */
    public static String getDriver()
    {
        return "com.sybase.jdbc3.jdbc.SybDriver";
    } // getDriver
    
} // SybaseDBConnect
