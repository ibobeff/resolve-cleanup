/******************************************************************************
* (C) Copyright 2014
*
* Resolve Systems, LLC
*
* All rights reserved
* This software is distributed under license and may not be used, copied,
* modified, or distributed without the express written permission of
* Resolve Systems, LLC.
*
******************************************************************************/

package com.resolve.connect;

import java.io.InputStream;
import java.io.OutputStream;

import org.apache.commons.net.telnet.TelnetClient;
import org.apache.commons.net.telnet.SuppressGAOptionHandler;
import org.apache.commons.net.telnet.EchoOptionHandler;

import com.resolve.util.Log;

public class TelnetConnect extends Connect
{
    protected String hostname = null; // destination hostname
    protected int port;           // destination port
    protected int timeout;        // connect timeout
    protected TelnetClient conn; // telnet connection
    protected InputStream in; // stdout from telnet session
    protected InputStream err; // stderr from telnet session
    protected OutputStream out; // stdin to ssh session
    protected String charset = null; // option character set (e.g. US-ASCII, UTF-8)
    // option (if true) returns every character it reads. If false (default) returns only the ASCII characters.
    //useful for some devices where unicode characters are returned and the consumer needs them.
    private boolean printSpecial = false;  
    
    protected boolean sendCarriageReturn = false;

    public TelnetConnect() throws Exception
    {
        super();
    }
    
    public TelnetConnect(String hostname) throws Exception
    {
        this(hostname, 23, 0, null, true, true);
    } // TelnetConnect

    public TelnetConnect(String hostname, int port) throws Exception
    {
        this(hostname, port, 0, null, true, true);
    } // TelnetConnect

    public TelnetConnect(String hostname, int port, int timeout) throws Exception
    {
        this(hostname, port, timeout, null, true, true);
    } // TelnetConnect

    public TelnetConnect(String hostname, int port, int timeout, String terminalType) throws Exception
    {
        this(hostname, port, timeout, terminalType, true, true);
    }

    public TelnetConnect(String hostname, int port, int timeout, String terminalType, boolean suppressGAOption, boolean echoOption) throws Exception
    {
        this(hostname, port, timeout, terminalType, true, true, true);
    } // TelnetConnect

    public TelnetConnect(String hostname, int port, int timeout, String terminalType, boolean suppressGAOption, boolean echoOption, boolean isConnect) throws Exception
    {
        try
        {
            /* Create a connection instance */
            if (terminalType == null)
            {
                conn = new TelnetClient();
            }
            else
            {
                conn = new TelnetClient(terminalType);
            }

            // suppress go-ahead-option
            if (suppressGAOption)
            {
                SuppressGAOptionHandler gaOpt = new SuppressGAOptionHandler(true, true, true, true);
                conn.addOptionHandler(gaOpt);
            }

            // echo handler
            if (echoOption)
            {
                EchoOptionHandler echoOpt = new EchoOptionHandler(true, true, true, true);
                conn.addOptionHandler(echoOpt);
            }

            // set timeout
            if (timeout > 0)
            {
                conn.setConnectTimeout(timeout);
            }

            this.hostname=hostname;
            this.port=port;
            this.timeout=timeout;

            if(isConnect)
            {
                // connect
                conn.connect(hostname, port);
    
                // set stream
                in = conn.getInputStream();
                out = conn.getOutputStream();
    
                // start session consumer
                reader = new TelnetReader(in, printSpecial);
                ((TelnetReader) reader).start();
            
                super.connect();
            }
        }
        catch (Exception e)
        {
            Log.log.error("Unable to connect to: " + hostname + " port: " + port + ". " + e.getMessage());
            throw e;
        }
    } // TelnetConnect

    public boolean isPrintSpecial()
    {
        return printSpecial;
    }

    /**
     * If the client wants all the characters (useful in telnetting to some devices that returns special characters). 
     * @param printSpecial - true returns everything.
     */
    public void setPrintSpecial(boolean printSpecial)
    {
        ((TelnetReader) reader).setPrintSpecial(printSpecial);
        this.printSpecial = printSpecial;
    }
    
    public String getHostname()
    {
        return hostname;
    }

    public void setHostname(String hostname)
    {
        this.hostname = hostname;
    }

    public int getPort()
    {
        return port;
    }

    public void setPort(int port)
    {
        this.port = port;
    }

    public int getTimeout()
    {
        return timeout;
    }

    public void close()
    {
        try
        {
            /* Close this session */
            if (conn != null)
            {
                conn.disconnect();
            }
        }
        catch (Exception e)
        {
            Log.log.error("Failed to close TelnetConnect: " + e.getMessage());
        }

        isClosed = true;
    } // close

    public boolean isConnected()
    {
        boolean result = false;

        if (conn != null)
        {
            result = conn.isConnected();
        }

        return result;
    } // isConnected

    public void flush() throws Exception
    {
        try
        {
            if (out != null)
            {
                Log.log.trace("flushing");
                out.flush();
            }
        }
        catch (Exception e)
        {
            Log.log.warn(e.getMessage(), e);
        }
    } // flush
    
    /**
     * Sends a command to the remote system connected to.
     * A newline character will automatically be added to the line to simulate hitting the
     * 'Enter' key, thereby executing the command
     * @param line Command to send
     * @throws Exception
     */
    public void send(String line) throws Exception
    {
        send(line, true);
    } // send
    
    /**
     * Sends a command to the remote system connected to.
     * The sendNewline option specifies whether to add a newline character to the sent
     * command, simulating the 'Enter' key, thereby executing the command
     * @param line Command to send
     * @param sendNewline Whether or not to send a newline character with the command
     * @throws Exception
     */
    public void send(String line, boolean sendNewline) throws Exception
    {
        try
        {
            super.send();

            // include line termination
            Log.log.debug("sending: " + line);

            if (charset == null)
            {
                Log.log.trace("sending " + line.getBytes().length + " bytes");
                if (sendNewline)
                {
                    if (sendCarriageReturn)
                    {
                        line += "\r";
                    }
                    line += "\n";
                }
                out.write(line.getBytes());
                Log.log.trace("Done Sending Line");
            }
            else
            {
                /*
                 * Log.log.debug("using charset: "+charset); //out.write(line.getBytes(charset)); Log.log.debug("sending "+line.getBytes(charset).length+" bytes"); for (int i=0; i < line.getBytes(charset).length; i++) { Log.log.debug("sending i: "+i+" byte: "+line.getBytes(charset)[i]); out.write(line.getBytes(charset)[i]); out.flush(); } out.write("\r\n".getBytes(charset));
                 */

                Log.log.trace("sending " + line.getBytes().length + " bytes");
                out.write(line.getBytes(charset));
                if (sendNewline)
                {
                    if (sendCarriageReturn)
                    {
                        out.write("\r\n".getBytes(charset));
                    }
                    else
                    {
                        out.write("\n".getBytes(charset));
                    }
                }
                Log.log.trace("Done Sending Charset Line");
            }
            out.flush();
            Log.log.trace("Output Stream Flush Done");
        }
        catch (Exception e)
        {
            Log.log.warn(e.getMessage());
            throw e;
        }
    } // send

    public void setCharset(String charset)
    {
        this.charset = charset;
    } // setChartset
    
    public void sendCarriageReturn(boolean sendCarriageReturn)
    {
        this.sendCarriageReturn = sendCarriageReturn;
    }

    public TelnetClient getTelnetClient()
    {
        return this.conn;
    } // getTelnetClient

} // TelnetConnect
