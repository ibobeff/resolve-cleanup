/******************************************************************************
* (C) Copyright 2014
*
* Resolve Systems, LLC
*
* All rights reserved
* This software is distributed under license and may not be used, copied,
* modified, or distributed without the express written permission of
* Resolve Systems, LLC.
*
******************************************************************************/

package com.resolve.connect;

import java.util.Hashtable;
import java.util.Iterator;
import java.util.Map;

import org.apache.commons.httpclient.HttpClient;
import org.apache.commons.httpclient.HttpStatus;
import org.apache.commons.httpclient.NameValuePair;
import org.apache.commons.httpclient.DefaultHttpMethodRetryHandler;
import org.apache.commons.httpclient.methods.GetMethod;
import org.apache.commons.httpclient.methods.PostMethod;
import org.apache.commons.httpclient.params.HttpMethodParams;
import org.apache.commons.httpclient.util.URIUtil;

import com.resolve.util.Log;
import com.resolve.util.StringUtils;

public class WebConnect extends Connect
{
    boolean debug = false;
    
    HttpClient client;
    String host;
    String uri;
    Map params;
    String cookie;
    
    public WebConnect() throws Exception
    {
        this(false);
    } // WebConnect();
    
    public WebConnect(boolean debug) throws Exception
    {
        this.debug = debug;
	    this.params = new Hashtable();
        this.cookie = null;
        this.host = "http://localhost";
        this.uri = "/";
        
        // init log if not called from RSRemote
        if (Log.log == null)
        {
            Log.init("com.resolve.connect.WebConnect");
        }
        
        // set log level
        initLogLevel();
        
        // init clients
        client = new HttpClient();
        reader = new WebReader();
        
        super.connect();
    } // WebClient
    
    void initLogLevel()
    {
        System.setProperty("org.apache.commons.logging.Log", "org.apache.commons.logging.impl.SimpleLog");
        System.setProperty("org.apache.commons.logging.simplelog.showdatetime", "true");
        
        // debugging
        if (debug)
        {
	        //System.setProperty("org.apache.commons.logging.simplelog.log.httpclient.wire", "debug");
	        System.setProperty("org.apache.commons.logging.simplelog.log.httpclient.wire.header", "debug");
	        System.setProperty("org.apache.commons.logging.simplelog.log.org.apache.commons.httpclient", "debug"); 
        }
        else
        {
	        System.setProperty("org.apache.commons.logging.simplelog.log.org.apache.commons.httpclient", "error"); 
        }
    } // initLogLevel
    
    public void send(String path)
    {
        get(path);
    } // send

    public void get(String path)
    {
        try
        {
            if (path.charAt(0) == '/')
            {
                setURI(path);
            }
            else if (path.startsWith("www"))
            {
                setURL("http://"+path);
            }
            else if (path.startsWith("http"))
            {
                setURL(path);
            }
            get();
        }
        catch (Exception e)
        {
            Log.log.error("Failed WebConnect GET request: "+e.getMessage());
            
            System.out.println("GET path: "+path+" error: "+e.getMessage());
            e.printStackTrace();
        }
    } // get
    
    public void get() throws Exception
    {
        // create url
        String url = host+URIUtil.encodePathQuery(uri);
		Log.log.debug("GET URL: "+url);
        
        // Create a method instance.
        GetMethod method = new GetMethod(url);

        // Provide custom retry handler is necessary
        method.getParams().setParameter(HttpMethodParams.RETRY_HANDLER, new DefaultHttpMethodRetryHandler(3, false));

        // manually set cookie if defined
        if (!com.resolve.util.StringUtils.isEmpty(cookie))
        {
			method.setRequestHeader("Cookie", cookie);
        }

        try
        {
            // Execute the method.
            int statusCode = client.executeMethod(method);

            if (statusCode != HttpStatus.SC_OK)
            {
                Log.log.debug(method.getStatusLine().toString());
            }

            // Read the response body.
            byte[] responseBody = method.getResponseBody();

            // Deal with the response. Use caution: ensure correct character encoding and is not binary data
            ((WebReader)reader).set(new String(responseBody));
        }
        catch (Exception e)
        {
            System.out.println("GET url: "+url+" error: "+e.getMessage());
            e.printStackTrace();
            
            Log.log.error("Failed WebConnect GET request: "+e.getMessage());
        }
        finally
        {
            // Release the connection.
            method.releaseConnection();
        }
    } // get
    
    public void post(String path)
    {
        try
        {
            if (path.charAt(0) == '/')
            {
                setURI(path);
            }
            else if (path.startsWith("www"))
            {
                setURL("http://"+path);
            }
            else if (path.startsWith("http"))
            {
                setURL(path);
            }
            post();
        }
        catch (Exception e)
        {
            System.out.println("POST path: "+path+" error: "+e.getMessage());
            e.printStackTrace();
            
            Log.log.error("Failed WebConnect POST request: "+e.getMessage());
        }
    } // post
    
    public void post() throws Exception
    {
        // create url
        String url = host+URIUtil.encodePathQuery(uri);
		Log.log.debug("POST URL: "+url);
        
        // Create a method instance.
        PostMethod method = new PostMethod(url);
        
        // set form parameters
        NameValuePair[] data = new NameValuePair[params.size()];
        int idx = 0;
        for (Iterator i=params.entrySet().iterator(); i.hasNext(); )
        {
            Map.Entry entry = (Map.Entry)i.next();
            String name = (String)entry.getKey();
            String value = (String)entry.getValue();
            
            data[idx] = new NameValuePair(name, value);
            idx++;
        }
		method.setRequestBody(data);

        // Provide custom retry handler is necessary
        method.getParams().setParameter(HttpMethodParams.RETRY_HANDLER, new DefaultHttpMethodRetryHandler(3, false));
        method.getParams().setParameter("http.protocol.single-cookie-header", new Boolean(true));
        
        // manually set cookie if defined
        if (!StringUtils.isEmpty(cookie))
        {
			method.setRequestHeader("Cookie", cookie);
        }

        try
        {
            // Execute the method.
            int statusCode = client.executeMethod(method);

            if (statusCode != HttpStatus.SC_OK)
            {
                Log.log.debug(method.getStatusLine().toString());
            }

            // Read the response body.
            byte[] responseBody = method.getResponseBody();

            // Deal with the response. Use caution: ensure correct character encoding and is not binary data
            ((WebReader)reader).set(new String(responseBody));
        }
        catch (Exception e)
        {
            System.out.println("POST url: "+url+" error: "+e.getMessage());
            e.printStackTrace();
            
            Log.log.error("Failed WebConnect POST request: "+e.getMessage());
        }
        finally
        {
            // Release the connection.
            method.releaseConnection();
        }
    } // postMethod
    
    public void setURL(String url) throws Exception
    {
        if (!url.startsWith("http"))
        {
            url = "http://"+url;
        }
        
        // get url host
        int pos = url.indexOf('/', "http://".length());
        if (pos != -1)
        {
            String host = url.substring(0, pos);
            String uri = url.substring(pos, url.length());
            setHost(host);
            setURI(uri);
        }
        else
        {
            setHost(url);
        }
    } // setURL
    
    public void setHost(String host)
    {
        if (host.startsWith("http"))
        {
	        this.host = host;
        }
        else
        {
            this.host = "http://"+host;
        }
    } // setHost
    
    public void setURI(String uri) throws Exception
    {
        if (uri.charAt(0) != '/')
        {
            uri = "/"+uri;
        }
        
        this.uri = uri;
    } // setURI
    
    public Map getParams()
    {
        return this.params;
    } // getParams
    
    public void setParams(Map params)
    {
        this.params = params;
    } // setParams
    
    public String getCookie()
    {
        return this.cookie;
    } // getCookie
    
    public void setCookie(String cookie)
    {
        this.cookie = cookie;
    } // setCookie
    
} // WebConnect
