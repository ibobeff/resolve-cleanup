/******************************************************************************
* (C) Copyright 2014
*
* Resolve Systems, LLC
*
* All rights reserved
* This software is distributed under license and may not be used, copied,
* modified, or distributed without the express written permission of
* Resolve Systems, LLC.
*
******************************************************************************/

package com.resolve.connect;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.StringReader;

import org.apache.pdfbox.pdmodel.PDDocument;
import org.apache.pdfbox.util.PDFTextStripper;

import com.lowagie.text.Document;
import com.lowagie.text.DocumentException;
import com.lowagie.text.Paragraph;
import com.lowagie.text.html.simpleparser.HTMLWorker;
import com.lowagie.text.pdf.PdfWriter;
import com.resolve.rsbase.MainBase;
import com.resolve.util.FileUtils;
import com.resolve.util.Log;
import com.resolve.util.StringUtils;

/**
 * This class is the adaptor to create and read PDF file.
 */
public class PDFAdaptor
{
    // where files could be stored
    private static final String RESOLVE_TEMP_DIR = MainBase.main.configGeneral.home + "/tmp/";

    private String tempDir = RESOLVE_TEMP_DIR;
    //private String tempDir = "c:/temp/";

    /**
     * Sets the temporary directory where the caller wants to store the PDF files.
     *
     * NOTE: Make sure that the user which started the RSRemote has full access to this directory.
     * If you do not set this then it will be the resolve's temp folder by default.
     *
     * @param tempDir
     */
    public void setTempDir(String tempDir)
    {
        this.tempDir = tempDir;
    }

    private void addContent(Document document, String content) throws DocumentException
    {
        document.add(new Paragraph(content));
    }

    /**
     * Reads the PDF file and returns the content as String.
     *
     * @param file
     *            - the absolute file path of the file to read
     * @return the content as {@link String}
     * @throws ConnectException
     */
    public String readPDF(String file) throws ConnectException
    {
        String result = null;

        if (StringUtils.isNotEmpty(file))
        {
            try
            {
                File inputFile = new File(file);
                if (inputFile.isFile())
                {
                    InputStream inputStream = FileUtils.openInputStream(inputFile);
                    result = readPDF(inputStream);
                }
            }
            catch (IOException e)
            {
                Log.log.error(e.getMessage(), e);
                throw new ConnectException(e.getMessage(), e);
            }
        }
        return result;
    }

    /**
     * Reads the PDF content from a InputStream.
     *
     * @param inputStream to read PDF content from.
     * @return the content as {@link String}
     * @throws ConnectException
     */
    public String readPDF(InputStream inputStream) throws ConnectException
    {
        String result = null;

        PDDocument document = null;
        try
        {
            document = PDDocument.load(inputStream);
            PDFTextStripper s = new PDFTextStripper();
            result = s.getText(document);
        }
        catch (IOException e)
        {
            Log.log.error(e.getMessage(), e);
            throw new ConnectException(e.getMessage(), e);
        }
        finally
        {
            if (document != null)
            {
                try
                {
                    document.close();
                }
                catch (IOException e)
                {
                    Log.log.warn(e.getMessage());
                    // just warn and ignore, no need to escalate
                }
            }
        }

        return result;
    }

    /**
     * Creates a PDF file from the incoming content.
     *
     * @param fileName to be stored the PDF.
     * @param content to be written to the PDF.
     * @param contentType could be "plain" or "html" (default is html).
     *
     * @return absolute path to the file.
     * @throws ConnectException
     */
    public String createPDF(String fileName, String content, String contentType) throws ConnectException
    {
        String result = null;

        Document document = null;

        try
        {
            String absoluteFilePath = tempDir + fileName;

            document = new Document();
            PdfWriter.getInstance(document, new FileOutputStream(absoluteFilePath));
            document.open();
            document.addCreationDate();
            if(StringUtils.isBlank(contentType))
            {
                contentType = "html";
            }

            if("html".equalsIgnoreCase(contentType))
            {
                HTMLWorker htmlWorker = new HTMLWorker(document);
                htmlWorker.parse(new StringReader(content));
            }
            else
            {
                addContent(document, content);
            }
            result = absoluteFilePath;
        }
        catch (IOException e)
        {
            Log.log.error(e.getMessage(), e);
            throw new ConnectException(e.getMessage(), e);
        }
        catch (DocumentException e)
        {
            Log.log.error(e.getMessage(), e);
            throw new ConnectException(e.getMessage(), e);
        }
        finally
        {
            if (document != null)
            {
                document.close();
            }
        }
        return result;
    }

    /**
     * This method helps deleting the generated file from an action task.
     *
     * @param file - the absolute path to the PDF file.
     * @return
     * @throws ConnectException
     */
    public boolean deletePDF(String file) throws ConnectException
    {
        boolean result = true;

        if (StringUtils.isNotEmpty(file))
        {
            try
            {
                File inputFile = new File(file);
                if (inputFile.isFile())
                {
                    FileUtils.deleteQuietly(inputFile);
                }
            }
            catch (Exception e)
            {
                Log.log.error(e.getMessage(), e);
                throw new ConnectException(e.getMessage(), e);
            }
        }
        else
        {
            throw new ConnectException("\"file\" parameter must not be empty or null");
        }
        return result;
    }
}
