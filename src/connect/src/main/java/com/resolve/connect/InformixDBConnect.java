/******************************************************************************
* (C) Copyright 2014
*
* Resolve Systems, LLC
*
* All rights reserved
* This software is distributed under license and may not be used, copied,
* modified, or distributed without the express written permission of
* Resolve Systems, LLC.
*
******************************************************************************/

package com.resolve.connect;

/**
 * Helper class used to generate Informix JDBC URLs and the name of the Driver class used by Resolve.
 * <br><b>Note:</b> The driver used by Resolve does not ship with the standard Resolve install, but must be added separately
 * <br>URL generated without username and password jdbc:informix-sqli://<i>hostname</i>:<i>port</i>/<i>dbname</i>:INFORMIXSERVER=<i>servername</i>
 * <br>URL generated with username and password jdbc:informix-sqli://<i>hostname</i>:<i>port</i>/<i>dbname</i>:INFORMIXSERVER=<i>servername</i>;user=<i>username</i>;password=<i>password</i>
 * @author justin.geiser
 *
 */
public class InformixDBConnect
{
    /**
     * Uses the hostname and dbname to generate the JDBC URL.
     * The port used will be 1526 unless the hostname if formated like <i>hostname</i>:<i>port</i>
     * @param hostname
     * @param dbname
     * @param servername
     * @return
     */ 
    public static String getConnectURL(String hostname, String dbname, String servername)
    {
        DBHost dbHost = DBConnectHelper.parseHost(hostname, 1526);
        return getConnectURL(dbHost.host, dbHost.port, dbname, servername);
    } // getConnectURL
    
    /**
     * Uses the hostname, port, and dbname to generate the JDBC URL.
     * @param hostname
     * @param port
     * @param dbname
     * @param servername
     * @return
     */
    public static String getConnectURL(String hostname, int port, String dbname, String servername)
    {
        return "jdbc:informix-sqli://"+hostname+":"+port+"/"+dbname+":INFORMIXSERVER="+servername;
    } // getConnectURL
    
    /**
     * Uses the hosname, dbname, username, and password to generate the JDBC URL.
     * The port used will be 1526 unless the hostname if formated like <i>hostname</i>:<i>port</i>.
     * @param hostname
     * @param dbname
     * @param servername
     * @param username
     * @param password
     * @return
     */
    public static String getConnectURL(String hostname, String dbname, String servername, String username, String password)
    {
        DBHost dbHost = DBConnectHelper.parseHost(hostname, 1526);
        return getConnectURL(dbHost.host, dbHost.port, dbname, servername, username, password);
    } // getConnectURL
    
    /**
     * Uses the hosname, port, dbname, username, and password to generate the JDBC URL.
     * @param hostname
     * @param port
     * @param dbname
     * @param servername
     * @param username
     * @param password
     * @return
     */
    public static String getConnectURL(String hostname, int port, String dbname, String servername, String username, String password)
    {
        return "jdbc:informix-sqli://"+hostname+":"+port+"/"+dbname+":INFORMIXSERVER="+servername+";user="+username+";password="+password;
    } // getConnectURL

    /**
     * Returns the name of the Informix driver most commonly used by Resolve
     * @return
     */
    public static String getDriver()
    {
        return "com.informix.jdbc.IfxDriver";
    } // getDriver
    
} // InformixDBConnect
