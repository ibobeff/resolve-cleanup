/******************************************************************************
* (C) Copyright 2014
*
* Resolve Systems, LLC
*
* All rights reserved
* This software is distributed under license and may not be used, copied,
* modified, or distributed without the express written permission of
* Resolve Systems, LLC.
*
******************************************************************************/

package com.resolve.connect;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Properties;
import java.util.concurrent.CountDownLatch;

import com.intelliden.common.IntellidenException;
import com.intelliden.icos.api.ApiFactory;
import com.intelliden.icos.api.ApiSession;
import com.intelliden.icos.api.resources.ResourceManager;
import com.intelliden.icos.api.workflow.WorkflowManager;
import com.intelliden.icos.idc.Command;
import com.intelliden.icos.idc.CommandWork;
import com.intelliden.icos.idc.DataAccessRight;
import com.intelliden.icos.idc.NativeCommandSet;
import com.intelliden.icos.idc.NativeCommandSetCommand;
import com.intelliden.icos.idc.NetworkResourceKey;
import com.intelliden.icos.idc.Resource;
import com.intelliden.icos.idc.ResourceContainerDescriptor;
import com.intelliden.icos.idc.ResourceContentDescriptor;
import com.intelliden.icos.idc.VtmosCriterion;
import com.intelliden.icos.idc.Work;
import com.intelliden.icos.idc.WorkKey;
import com.resolve.util.Log;

/**
 * HTTPConnect is a low level API to send messages to a URL, and retrieve the
 * response
 * 
 * MAIN APIS: submitAndWaitNCS, getWork, execCmdSet, listCmdSet
 * 
 * @author duke.tantiprasut
 * 
 */
public class ITNCMConnect extends Connect
{
    private static final String CNAME = ITNCMConnect.class.getName();

    final int execWindow = 4 * 60 * 60 * 1000; // UOW execution window in
                                               // milliseconds used as a default
                                               // value

    String user;
    String password;
    String server;
    int port;
    ApiSession session = null;

    public ITNCMConnect(String user, String password, String server, int port) throws Exception
    {
        this.user = user;
        this.password = password;
        this.server = server;
        this.port = port;

        this.session = this.getSession();
    } // ITNCM

    public void connect() throws Exception
    {
        this.session = this.getSession();
    } // connect

    public ApiSession getSession() throws Exception
    {
        this.session = ApiFactory.getSession(user, password, server, port);
        return this.session;
    } // getSession

    public void close()
    {
        try
        {
            this.session.disconnect();
        }
        catch (Throwable e)
        {
            Log.log.error(e.getMessage(), e);
        }
    } // close

    /*
     * SimpleSubmitNCS
     */

    /**
     * submitAndWaitNCS wrapper
     * 
     * @return - Unit of Work ID (UOW ID)
     * 
     */
    public String submitAndWaitNCS(String deviceName, String ncsName, String ncsRealm, boolean searchSubRealms, Map params, String description) throws Exception
    {
        String uowId = null;

        uowId = submitNCS(deviceName, ncsName, ncsRealm, searchSubRealms, params, description);
        waitForUOW(uowId);

        return uowId;
    } // submitAndWaitNCS

    public String submitNCS(String deviceName, String ncsName, String ncsRealm, boolean searchSubRealms, Map params, String description) throws Exception
    {
        String result;

        NativeCommandSet ncs = (NativeCommandSet) ITNCMConnect.getResource(session, ncsName, ncsRealm, searchSubRealms, Resource.RESOURCE_NATIVE_COMMAND_SET, null);
        ResourceContentDescriptor nrDesc = ITNCMConnect.getNetworkResourceDescriptor(deviceName, session);
        result = this.submitNCS(session, (NetworkResourceKey) nrDesc.getResourceKey(), ncs, params, description);

        //Log.log.debug("Submitted UOW ID " + result);
        return result;
    } // submitNCS
    
    public Work pollWork(String uowId)
    {
        waitForUOW(uowId);
        return getWork(uowId);
    } // pollWork

    public void waitForUOW(String uowId)
    {
        CountDownLatch latch = new CountDownLatch(1);
        Thread poller = new Thread(new ITNCMUoWPoller(session, uowId, latch));
        poller.start();
        try
        {
            latch.await();
        }
        catch (InterruptedException ignore)
        {
        }
        //Log.log.debug("Finished Polling for completion of UOW " + uowId);
    } // waitForUOW

    /**
     * A simple example routine to execute a NativeCommandSet against a
     * NetworkResource. The code can be easily modifed to call a routine to
     * immediately execute a NativeCommandSetCommand, or to call a routine to
     * create and submit a UOW containing the NativeCommandSetCommand.
     * 
     * @param session
     *            A connected ApiSession instance.
     * @param deviceName
     *            The name of the NetworkResource in ITNC to apply the
     *            NativeCommandSet against.
     * @param ncsName
     *            The name of the NativeCommandSet to use.
     * @param ncsRealm
     *            The name of the Realm to start searching for the
     *            NativeCommandSet from, in "/" delimiet format, such as
     *            ITNCM/realm1/realm2. If a null String is supplied, the search
     *            will start from the root realm of the ITNCM installation.
     * @param searchSubrealms
     *            boolean, indicates whether or not to search subrealms.
     * @param paramProps
     *            A Properties object containing data to be used to populate the
     *            NativeCommandSet paramters. The parameter names are the
     *            property keys, and the parameter values are the property
     *            values.
     * @param uowDescription
     *            A String that will show up in the GUI as the UOW description.
     * @return The UOW ID of the submitted UOW.
     * @throws IntellidenException
     * @throws SimpleExampleException
     */
    public String submitNCS(ApiSession session, String deviceName, String ncsName, String ncsRealm, boolean searchSubrealms, Map params, String uowDescription) throws Exception
    {

        // Need the NetworkResourceKey to indicate the device to submit the
        // NativeCommandSet against.
        ResourceContentDescriptor nrDesc = ITNCMConnect.getNetworkResourceDescriptor(deviceName, session);
        if (nrDesc == null)
        {
            throw new Exception("Network Resource was not found: " + deviceName);
        }
        NetworkResourceKey nrKey = (NetworkResourceKey) nrDesc.getResourceKey();

        // Need a local copy of the NativeCommandSet
        String resourceType = Resource.RESOURCE_NATIVE_COMMAND_SET;
        VtmosCriterion vtmos = null; // wild-card VTMOS in this example
        NativeCommandSet ncs = (NativeCommandSet) ITNCMConnect.getResource(session, ncsName, ncsRealm, searchSubrealms, resourceType, vtmos);
        if (ncs == null)
        {
            throw new Exception("Native Command Set was not found: " + ncsName);
        }
        
        Properties paramProps = new Properties();
        paramProps.putAll(params);

        // Populate the NativeCommandSet paramters
        ncs.applyParameters(paramProps);

        // Create an empty Command to use to execute the NativeCommandSet, and
        // set up the Command for execution. Use the correct Command subclass.
        NativeCommandSetCommand command = session.resourceManager().dataFactory().newNativeCommandSetCommand();
        ArrayList<NetworkResourceKey> nrKeys = new ArrayList<NetworkResourceKey>();
        nrKeys.add(nrKey);
        command.setKeys(nrKeys);
        command.addCommandSet(ncs);
        // Comment needs to be populated in case the Command is directly
        // executed instead of being wrapping in UOW explicitly by the client.
        command.setComment(uowDescription);

        // see more detailed examples for other Commmand options that can be
        // set,
        // such as rollback details

        // Send the command to the server for immediate execution, avoids clock
        // skew problems.
        // Alternatively use the submitUow() routine in this class if a specific
        // execution window is important, or if you need to set things like the
        // userLabels on UOWs
        return executeCommand(session, command);
    } // submitNCS

    /**
     * A simple example routine to execute a NativeCommandSet against a
     * NetworkResource. The code can be easily modifed to call a routine to
     * immediately execute a NativeCommandSetCommand, or to call a routine to
     * create and submit a UOW containing the NativeCommandSetCommand.
     * 
     * @param session
     *            A connected ApiSession instance.
     * @param nrKey
     *            The NetworkResourceKey of the NetworkResource in ITNC to apply
     *            the NativeCommandSet against.
     * @param ncs
     *            The NativeCommandSet to use.
     * @param paramProps
     *            A Properties object containing data to be used to populate the
     *            NativeCommandSet paramters. The parameter names are the
     *            property keys, and the parameter values are the property
     *            values. If you have already populated the paramters in this
     *            NativeCommandSet, pass in a null Properties object object and
     *            this routine will not apply the parameter values.
     * @param uowDescription
     *            A String that will show up in the GUI as the UOW description.
     * @return The UOW ID of the submitted UOW.
     * @throws IntellidenException
     * @throws SimpleExampleException
     */
    public String submitNCS(ApiSession session, NetworkResourceKey nrKey, NativeCommandSet ncs, Map params, String uowDescription) throws Exception
    {

        // Populate the NativeCommandSet paramters
        if (params != null)
        {
            Properties paramProps = new Properties();
            paramProps.putAll(params);
            ncs.applyParameters(paramProps);
        }

        // Create an empty Command to use to execute the NativeCommandSet, and
        // set up the Command for execution. Use the correct Command subclass.
        NativeCommandSetCommand command = session.resourceManager().dataFactory().newNativeCommandSetCommand();
        ArrayList<NetworkResourceKey> nrKeys = new ArrayList<NetworkResourceKey>();
        nrKeys.add(nrKey);
        command.setKeys(nrKeys);
        command.addCommandSet(ncs);
        // Comment needs to be populated in case the Command is directly
        // executed instead of being wrapping in UOW explicitly by the client.
        command.setComment(uowDescription);

        // see more detailed examples for other Commmand options that can be
        // set,
        // such as rollback details

        // Send the command to the server for immediate execution, avoids clock
        // skew problems.
        // Alternatively use the submitUow() routine in this class if a specific
        // execution window is important, or if you need to set things like the
        // userLabels on UOWs
        return executeCommand(session, command);
    } // submitNCS

    /**
     * A simple example routine to execute a NativeCommandSet against a
     * NetworkResource. The code can be easily modifed to call a routine to
     * immediately execute a NativeCommandSetCommand, or to call a routine to
     * create and submit a UOW containing the NativeCommandSetCommand.
     * 
     * @param session
     *            A connected ApiSession instance.
     * @param deviceName
     *            The name of the NetworkResource in ITNC to apply the
     *            NativeCommandSet against.
     * @param ncs
     *            The NativeCommandSet to use.
     * @param paramProps
     *            A Properties object containing data to be used to populate the
     *            NativeCommandSet paramters. The parameter names are the
     *            property keys, and the parameter values are the property
     *            values. If you have already populated the paramters in this
     *            NativeCommandSet, pass in a null Properties object object and
     *            this routine will not apply the parameter values.
     * @param uowDescription
     *            A String that will show up in the GUI as the UOW description.
     * @return The UOW ID of the submitted UOW.
     * @throws IntellidenException
     * @throws SimpleExampleException
     */
    public String submitNCS(ApiSession session, String deviceName, NativeCommandSet ncs, Map params, String uowDescription) throws Exception
    {

        // Need the NetworkResourceKey to indicate the device to submit the
        // NativeCommandSet against.
        ResourceContentDescriptor nrDesc = ITNCMConnect.getNetworkResourceDescriptor(deviceName, session);
        if (nrDesc == null)
        {
            throw new Exception("Network Resource was not found: " + deviceName);
        }
        NetworkResourceKey nrKey = (NetworkResourceKey) nrDesc.getResourceKey();

        // Populate the NativeCommandSet paramters
        if (params != null)
        {
            Properties paramProps = new Properties();
            paramProps.putAll(params);
            
            ncs.applyParameters(paramProps);
        }

        // Create an empty Command to use to execute the NativeCommandSet, and
        // set up the Command for execution. Use the correct Command subclass.
        NativeCommandSetCommand command = session.resourceManager().dataFactory().newNativeCommandSetCommand();
        ArrayList<NetworkResourceKey> nrKeys = new ArrayList<NetworkResourceKey>();
        nrKeys.add(nrKey);
        command.setKeys(nrKeys);
        command.addCommandSet(ncs);
        // Comment needs to be populated in case the Command is directly
        // executed instead of being wrapping in UOW explicitly by the client.
        command.setComment(uowDescription);

        // see more detailed examples for other Commmand options that can be
        // set,
        // such as rollback details

        // Send the command to the server for immediate execution, avoids clock
        // skew problems.
        // Alternatively use the submitUow() routine in this class if a specific
        // execution window is important, or if you need to set things like the
        // userLabels on UOWs
        return executeCommand(session, command);
    }

    /**
     * A simple example routine to create a UOW containing a Commmand on the
     * client, and submit the UOW with an execution window based on the client
     * clock.
     * 
     * @param session
     *            A connected ApiSession instance
     * @param command
     *            The Command to be executed, with all required fields
     *            populated.
     * @param uowDescription
     *            The commment / description field for the UOW.
     * @return The UOW ID, as displayed in the GUI for this UOW
     * @throws IntellidenException
     */
    protected String submitUow(ApiSession session, Command command, String uowDescription) throws Exception
    {

        WorkflowManager wfManager = session.workflowManager();

        // Create an empty CommandWork, which is the API equivalent of a UOW.
        // Alway use a CommandWork object, and not any of the "legacy" Work
        // subclasses such as CommandSetWork. Most of the other Work subclasses
        // are only supported for backwards compatibility with legacy API code.
        CommandWork work = wfManager.dataFactory().newCommandWork();

        // The CommandWork just needs a Command, a comment, and some scheduling
        // information. There is no way to schedule a CommmandWork for immediate
        // execution, and the scheduling information is base on the client's
        // clock. Since Dates are used, time zone information is not used
        // either. Clock skew or time zone differences can result in unexpected
        // UOW exection scheduling issues, which are often interpreted as
        // performance issues involving slow UOW performance
        work.setCommand(command);
        work.setComment(uowDescription);
        work.setExecutionWindowStartTime(new Date());
        work.setExecutionWindowEndTime(new Date(System.currentTimeMillis() + execWindow));

        // At this point, if you passed in additional data, you could also
        // set things like the UOW userLabels.
        // work.setUserLabel1(label1)

        boolean overrideUowConflicts = true; // Ignored since version 5.x
        WorkKey key = wfManager.submit(work, overrideUowConflicts);

        // The friendlyName is the UOW ID as seen in the GUI.
        // the distingquishedName is the unique key for the UOW in the database
        return key.getFriendlyName();
    }

    /**
     * A simple example routine to submit a Command for immediate execution on
     * the ITNCM server. The UOW is not created by the client, but one is
     * implicitly created on the server.
     * 
     * @param session
     *            A connected ApiSession instance
     * @param command
     *            The Command to be executed, with all required fields
     *            populated, including the comment fied.
     * @return The UOW ID, as displayed in the GUI for this UOW
     * @throws IntellidenException
     */
    public String executeCommand(ApiSession session, Command command) throws IntellidenException
    {

        // In this example, we will execute the Command directly to avoid client
        // to server clock skew issues. With this approach, the ITNCM server
        // implicitly creates a UOW scheduled for immediate exectution. The
        // returned
        // Command has data that can be used to calculate the UOW WorkKey or ID
        //
        // This approach has a couple trade-offs. While it avoids clock
        // synchronization issues for immediate execution, it does not allow
        // setting an execution window, and does not have access to the UOW to
        // set things such as the user label fiedls.
        boolean overrideUowConflicts = true; // Ignored since version 5.x

        // You could call executeCommandBlocking, but you would not know how
        // long to wait to be completely sure the UOW had completed, so there
        // would still be cases where you needed to extract the UOW ID or key
        // and externally track the UOW to completion. The same type of Command
        // will be returned as was submitted, so if you know the Command type
        // you can cast the returned Command to that type to access type
        // specific
        // data.
        Command submittedCommand = session.workflowManager().executeCommandNonBlocking(command, overrideUowConflicts);
        // Since the Command was sumbitted for non-blocking execution, not much
        // will be
        // populated except the key and ID data.
        long uowId = submittedCommand.getExecutionId();

        return "" + uowId;
    } // executeCommand

    /*
     * SimpleApiUtils
     */
    /**
     * Convenience Method to get an Intelliden Resource. This can be a Resource
     * on any type, such as a CommandSet or a NetworkResource. The Resource
     * types are defined as constant fields in the Resource interface, and
     * should be used instead of literal Strings.
     * 
     * @param apisession
     *            A connected ApiSession.
     * @param resourceName
     *            The name the Resource as known by in the R-Series server.
     * @param realm
     *            The "/" delimited name of the realm to start searching from.
     *            If null, will start from the root realm.
     * @param searchSubrealms
     *            Indicates whether to search the sub-realms of the realm
     *            specified by the "realm" parameter
     * @param resourceType
     *            The type of Resource to search for, as defined as constants on
     *            the Resource interface.
     * @param vtmos
     *            The VtmosCriterion to use when searching for the device. A
     *            null VtmosCriterion acts a a wild card for VTMOS.
     * @return A Resource of the type specified by the resourceType parameter
     *         You will need to cast the returned Resource to its actual type to
     *         use it as that type. If nothing is found, null is returned.
     * @throws IntellidenException
     */
    public static Resource getResource(ApiSession apisession, String resourceName, String realm, boolean searchSubrealms, String resourceType, VtmosCriterion vtmos) throws IntellidenException
    {

        final String MNAME = CNAME + ".getResource(): ";

        Resource resource = null;
        ResourceManager resourceManager = apisession.resourceManager();

        Collection<ResourceContentDescriptor> descriptors = getResourceDescriptors(apisession, resourceName, realm, searchSubrealms, resourceType, vtmos);

        // Did we get any results?
        if (null != descriptors && descriptors.size() > 0)
        {
            // should only be one object in the collection, warn if there are
            // more
            if (descriptors.size() > 1)
            {
                Log.log.error(MNAME + "More than one resource was returned in the  search for " + resourceName);
            }
            // Return the first object in the collection; there should be only
            // one
            ResourceContentDescriptor descriptor = (ResourceContentDescriptor) descriptors.iterator().next();
            resource = resourceManager.getResource(descriptor);

        }
        return resource;
    }

    /**
     * Convenience Method to get the collection of descriptors for a specified
     * resource. Note that is some cases, more than one matching resource may be
     * found.
     * 
     * @param apisession
     *            A connected ApiSession.
     * @param resourceName
     *            The name of the Resource in the R-Series, null for wildcard
     * @param realm
     *            The name of the realm to start searching from, as a "/"
     *            delimited String. If null, will start from the root realm.
     * @param searchSubrealms
     *            Whether or not to search in the child realms of the realm
     *            specified by the "realm" parameter.
     * @param resourceType
     *            The type of Resource to search for, as defined as constants on
     *            the Resource interface. Null for wildcard
     * @param vtmos
     *            VTMOS criterion for the resource(s), null for wildcard
     * @return A Collection of ResoureContentDescriptors for Resources matching
     *         the search criteria.
     * @throws IntellidenException
     */
    public static Collection<ResourceContentDescriptor> getResourceDescriptors(ApiSession apisession, String resourceName, String realm, boolean searchSubrealms, String resourceType, VtmosCriterion vtmos) throws IntellidenException
    {

        final String MNAME = CNAME + ".getResourceDescriptors(): ";

        ResourceManager resourceManager = apisession.resourceManager();

        // Get the ResourcecontainerDescriptor for the realm
        ResourceContainerDescriptor realmDescriptor = null;

        if (null == realm)
        {
            try
            {
                realmDescriptor = resourceManager.getRootContainer();
            }
            catch (IntellidenException ie)
            {
                Log.log.error(MNAME + "Exception getting the ResourceContainerDescriptor" + " for the root realm");
                throw ie;
            }
        }
        else
        {
            try
            {
                realmDescriptor = resourceManager.getContainer(realm);
            }
            catch (IntellidenException ie)
            {
                Log.log.error(MNAME + "Exception getting the ResourceContainerDescriptor" + " for the realm " + realm);
                throw ie;
            }
        }
        if (null == realmDescriptor)
        {
            String message = "A null ResourceContainerDescriptor  was returned" + " for the realm " + realm;
            Log.log.error(MNAME + message);
            throw new IntellidenException(message);
        }

        // We need an access rights mask for the version of search we are
        // doing. We only need view rights.
        int accessMask = DataAccessRight.VIEW.getId();

        Collection<ResourceContentDescriptor> descriptors = resourceManager.search(realmDescriptor.getKey(), searchSubrealms, resourceType, resourceName, vtmos, accessMask, false, false, false, null);

        return descriptors;
    }

    /**
     * Convenience method to fetch the ResourceContentDescriptor for a
     * NetworkResource based on the name of the NetworkResource.
     * 
     * @param resName
     *            The name of the NetworkResource in the R-Series.
     * @param session
     *            A connected ApiSession
     * @return The ResourceContentDescriptor for the specified NetworkResource,
     *         or null if the resource is not found.
     * @throws IntellidenException
     */
    public static ResourceContentDescriptor getNetworkResourceDescriptor(String resName, ApiSession session) throws IntellidenException
    {
        final String MNAME = CNAME + ".getNetworkResourceDescriptor(): ";

        ResourceContentDescriptor desc = null;
        Collection<ResourceContentDescriptor> descriptors = getResourceDescriptors(session, resName, null, true, Resource.RESOURCE_NETWORKERSOURCE, null);

        if (null != descriptors && descriptors.size() > 0)
        {
            if (descriptors.size() > 1)
            {
                Log.log.error(MNAME + "The search for the NetworkResource " + resName + " returned more than one result.  Will " + " use the first value in the returned Collection");
            }
            Iterator<ResourceContentDescriptor> iter = descriptors.iterator();
            if (iter.hasNext())
            {
                desc = (ResourceContentDescriptor) iter.next();
            }
        }

        return desc;
    }

    /*
     * PollUoW
     */
    public Work getWork(String uowId)
    {
        Work result = null;
        
        try
        {
            WorkflowManager wfManager = session.workflowManager();
            Work work = null;
            try
            {
                result = wfManager.getWork(uowId);
                if(Log.log.isTraceEnabled())
                {
                    Log.log.trace("Work State: " + work.getState()  + " Execute Status: " + work.getExecutionStatus());
                }
            }
            catch (IntellidenException ie)
            {
                Log.log.error("Error fetching UOW " + uowId + " from the ITNCM server");
                throw ie;
            }
        }
        catch (Exception e)
        {
            Log.log.error(e.getMessage(), e);
        }

        return result;
    } // getWork
    
    public String getWorkLog(String uowId)
    {
        String result = null;
        
        try
        {
            WorkflowManager wfManager = session.workflowManager();
	        result = new String(wfManager.getWorkLog(uowId));
        }
        catch (Exception e)
        {
            Log.log.error(e.getMessage(), e);
        }

        return result;
    } // getWorkLog

    /*
     * ExecCmdSet
     */

    public String execCmdSet(String device, String cmdSet, String cmdSetRealm, Map params, String comment)
    {
        String uowId = null;
        
        try
        {
            uowId = this.submitNCS(session, device, cmdSet, cmdSetRealm, false, params, comment);
        }
        catch (Exception e)
        {
            Log.log.error(e.getMessage(), e);
        }

        return uowId;
    } // ExecCmdSet

    /*
     * listCmdSet
     */
    public Map<String, List<String>> listCmdSet(String startRealm)
    {
        Map<String, List<String>> result = new HashMap<String, List<String>>();

        try
        {
            Collection<NativeCommandSet> cmdSets = this.collectCommandSets(session, startRealm, false);

            for (NativeCommandSet cmdSet : cmdSets)
            {
                String name = cmdSet.getName();
                //result += "CommandSet: " + name;

                List<String> paramNames = ITNCMConnect.extractParams(cmdSet);
                /*
                for (String paramName : paramNames)
                {
                    result += "ParamName: " + paramName;
                }
                */
                
                result.put(name, paramNames);
            }
        }
        catch (Exception e)
        {
            Log.log.error(e.getMessage(), e);
        }
        
        return result;
    } // listCmdSet

    /*
     * NativeCmdSetCollector
     */

    /**
     * Convenience method to fetch NativeCommandSets from a realm tree.
     * 
     * @param session
     *            A connected ApiSession instance
     * @param startingRealm
     *            The name on the realm to start searching for NativeCommandSets
     *            from, in "/" delimited format, such as ITNCM/realm1/realm2.
     *            For this method, passing in null String means to start the
     *            search from the root realm.
     * @param cmdSetName
     *            The name of the NativeCommandSet to search for. Passing in a
     *            null String means to wildcard the name.
     * @param vtmos
     *            The VtmosCriterion of the NativeCommandSets search for.
     *            Passing in null means to wildcard the VTMOS. Passing in a
     *            VTMOS criterion with trailing null values means to wildcard
     *            the null positions. Note that in the case of VtmosCriterion,
     *            "trailing" references Vendor, Type, Model, and OS, with OS
     *            being them most trailing element.
     * @param searchSubrealms
     *            boolean, whether or not to search sub-realms of the specified
     *            starting realm.
     * @return A Collection of NativeCommandsSets matching the search criteria.
     * @throws IntellidenException
     */
    public Collection<NativeCommandSet> collectCommandSets(ApiSession session, String startingRealm, String cmdSetName, VtmosCriterion vtmos, boolean searchSubrealms) throws IntellidenException
    {

        Collection<NativeCommandSet> cmdSets = new ArrayList<NativeCommandSet>();
        String type = Resource.RESOURCE_NATIVE_COMMAND_SET;

        Collection<ResourceContentDescriptor> ncsDescs = ITNCMConnect.getResourceDescriptors(session, cmdSetName, startingRealm, searchSubrealms, type, vtmos);

        if (ncsDescs != null && ncsDescs.size() > 0)
        {
            ResourceManager rm = session.resourceManager();
            for (ResourceContentDescriptor desc : ncsDescs)
            {
                NativeCommandSet ncs = (NativeCommandSet) rm.getResource(desc);
                cmdSets.add(ncs);
            }
        }

        return cmdSets;
    }

    /**
     * Convenience method to fetch NativeCommandSets from a realm tree.
     * 
     * @param session
     *            A connected ApiSession instance
     * @param startingRealm
     *            The name on the realm to start searching for NativeCommandSets
     *            from, in "/" delimited format, such as ITNCM/realm1/realm2.
     *            For this method, passing in null String means to start the
     *            search from the root realm.
     * @param vtmos
     *            The VtmosCriterion of the NativeCommandSets search for.
     *            Passing in null means to wildcard the VTMOS. Passing in a
     *            VTMOS criterion with trailing null values means to wildcard
     *            the null positions. Note that in the case of VtmosCriterion,
     *            "trailing" references Vendor, Type, Model, and OS, with OS
     *            being them most trailing element.
     * @param searchSubrealms
     *            boolean, whether or not to search sub-realms of the specified
     *            starting realm.
     * @return A Collection of NativeCommandsSets matching the search criteria.
     * @throws IntellidenException
     */
    public Collection<NativeCommandSet> collectCommandSets(ApiSession session, String startingRealm, VtmosCriterion vtmos, boolean searchSubrealms) throws IntellidenException
    {

        String cmdSetName = null;
        return collectCommandSets(session, startingRealm, cmdSetName, vtmos, searchSubrealms);

    }

    /**
     * Convenience method to fetch NativeCommandSets from a realm tree.
     * 
     * @param session
     *            A connected ApiSession instance
     * @param startingRealm
     *            The name on the realm to start searching for NativeCommandSets
     *            from, in "/" delimited format, such as ITNCM/realm1/realm2.
     *            For this method, passing in null String means to start the
     *            search from the root realm. * @param cmdSetName The name of
     *            the NativeCommandSet to search for. Passing in a null String
     *            means to wildcard the name.
     * @param searchSubrealms
     *            boolean, whether or not to search sub-realms of the specified
     *            starting realm.
     * @return A Collection of NativeCommandsSets matching the search criteria.
     * @throws IntellidenException
     */
    public Collection<NativeCommandSet> collectCommandSets(ApiSession session, String startingRealm, boolean searchSubrealms) throws IntellidenException
    {

        String cmdSetName = null;
        VtmosCriterion vtmos = null;
        return collectCommandSets(session, startingRealm, cmdSetName, vtmos, searchSubrealms);

    }

    /**
     * Convenience method to fetch NativeCommandSets from a realm tree.
     * 
     * @param session
     *            A connected ApiSession instance
     * @param startingRealm
     *            The name on the realm to start searching for NativeCommandSets
     *            from, in "/" delimited format, such as ITNCM/realm1/realm2.
     *            For this method, passing in null String means to start the
     *            search from the root realm.
     * @param searchSubrealms
     *            boolean, whether or not to search sub-realms of the specified
     *            starting realm.
     * @return A Collection of NativeCommandsSets matching the search criteria.
     * @throws IntellidenException
     */
    public Collection<NativeCommandSet> collectCommandSets(ApiSession session, String startingRealm, String cmdSetName, boolean searchSubrealms) throws IntellidenException
    {

        VtmosCriterion vtmos = null;
        return collectCommandSets(session, startingRealm, cmdSetName, vtmos, searchSubrealms);

    }

    /**
     * Convenience method to fetch NativeCommandSets from a realm tree.
     * 
     * @param session
     *            A connected ApiSession instance
     * @param startingRealm
     *            The name on the realm to start searching for NativeCommandSets
     *            from, in "/" delimited format, such as ITNCM/realm1/realm2.
     *            For this method, passing in null String means to start the
     *            search from the root realm.
     * @return A Collection of NativeCommandsSets matching the search criteria.
     * @throws IntellidenException
     */
    public Collection<NativeCommandSet> collectCommandSets(ApiSession session, String startingRealm) throws IntellidenException
    {

        String cmdSetName = null;
        VtmosCriterion vtmos = null;
        boolean searchSubrealms = true;
        return collectCommandSets(session, startingRealm, cmdSetName, vtmos, searchSubrealms);
    }

    /*
     * NativeCmdSetParamExtractor
     */
    /**
     * An example routine to list the parameter names in a NativeCommandSet.
     * Uses the same underlying code as the 6.2 ITNCM GUI client.
     * 
     * @param ncs
     *            The NativeCommandSet to get the paramter names from.
     * @return The List of parameter names found in the NativeCommandSets
     * @throws SimpleExampleException
     */
    public static List<String> extractParams(NativeCommandSet ncs) throws Exception
    {
        ArrayList<String> paramNames = new ArrayList<String>();

        String ncsCli = ncs.getCommands();
        ArrayList<String> paramValues = new ArrayList<String>();

        getParameters(ncsCli, paramNames, paramValues);

        return paramNames;

    }

    public static void getParameters(String string, ArrayList<String> keys, ArrayList<String> values) throws Exception
    {
        if (string != null)
        {
            // make sure this is not a "\$" which means literally a "$"
            int firstToken = string.indexOf("$");
            boolean valid = false;
            while ((!valid) && (firstToken > -1))
            {
                if (firstToken > 0)
                {
                    if (string.charAt(firstToken - 1) == '\\')
                    {
                        // skip this one.
                        firstToken = string.indexOf('$', firstToken + 1);
                    }
                    else
                    {
                        valid = true;
                    }
                }
                else
                {
                    valid = true;
                }
            }
            if (firstToken == -1)
            {
                // no more parameters
                return;
            }

            int secondToken = string.indexOf("$", firstToken + 1);
            valid = false;
            while ((!valid) && (secondToken > -1))
            {
                if (string.charAt(secondToken - 1) == '\\')
                {
                    // skip this one.
                    secondToken = string.indexOf('$', secondToken + 1);
                }
                else
                {
                    valid = true;
                }
            }
            if (secondToken == -1)
            {
                throw new Exception("$ mismatch in this native command set: " + string);
            }

            addParameter(string.substring(firstToken + 1, secondToken), keys, values);

            getParameters(string.substring(secondToken + 1), keys, values);
        }
    }

    public static void addParameter(String propString, ArrayList<String> keys, ArrayList<String> values)
    {
        String property = null;
        String value = null;

        if (propString != null)
        {
            int equals = propString.indexOf("=");
            if (equals != -1)
            {
                property = propString.substring(0, equals);
                // trim the property name but not the value
                property = property.trim();
                value = propString.substring(equals + 1);
            }
            else
            {
                property = propString.trim();
            }
            keys.add(property);
            values.add(value);
        }
    }

} // ITNCMConnect
