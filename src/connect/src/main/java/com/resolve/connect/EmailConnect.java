/******************************************************************************
* (C) Copyright 2014
*
* Resolve Systems, LLC
*
* All rights reserved
* This software is distributed under license and may not be used, copied,
* modified, or distributed without the express written permission of
* Resolve Systems, LLC.
*
******************************************************************************/

package com.resolve.connect;

import java.io.File;
import java.util.Properties;

import javax.activation.DataHandler;
import javax.activation.DataSource;
import javax.activation.FileDataSource;
import javax.mail.Address;
import javax.mail.BodyPart;
import javax.mail.Message;
import javax.mail.Multipart;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeBodyPart;
import javax.mail.internet.MimeMessage;
import javax.mail.internet.MimeMultipart;

import com.resolve.rsbase.SessionObjectInterface;
import com.resolve.util.Log;
import com.resolve.util.StringUtils;

public class EmailConnect implements SessionObjectInterface
{
    private static final String DEFAULT_CONTENTTYPE = "text/html";
    
    boolean isMultipart;
    Multipart multipart;
    boolean secure;
    Session session;
    MimeMessage message;
    String host;
    int port;
    String username;
    String password;
    
    public EmailConnect(String host, int port)  throws Exception
    {
        this(host, port, false);
    } // EmailConnect
    
    public EmailConnect(String host, int port, boolean hasAttachment)  throws Exception
    {
        this.isMultipart = hasAttachment;
        this.secure = false;
        this.host = host;
        this.port = port;
        
        Properties props = System.getProperties();
        props.put("mail.smtp.host", host);
        props.put("mail.smtp.port", Integer.toString(port));
        props.put("mail.smtp.connectiontimeout", 60000);
            
        session = Session.getDefaultInstance(props, null);
            
        message = new MimeMessage(session);
        if (isMultipart)
        {
            multipart = new MimeMultipart();
            message.setContent(multipart);
        }
    } // EmailConnect
    
    public EmailConnect(String host, int port, String username, String password) throws Exception
    {
        this(host,port,username,password,false);
    } // EmailConnect
    
    public EmailConnect(String host, int port, String username, String password, boolean hasAttachment) throws Exception
    {
        this.isMultipart = hasAttachment;
        this.secure = true;
        this.host = host;
        this.port = port;
        this.username = username;
        this.password = password;
            
        Properties props = System.getProperties();
        props.put("mail.smtps.host", host);
        props.put("mail.smtps.port", Integer.toString(port));
        props.put("mail.smtps.connectiontimeout", 60000);
        props.put("mail.smtps.auth", "true");
            
        session = Session.getDefaultInstance(props, null);
            
        message = new MimeMessage(session);
        if (isMultipart)
        {
            multipart = new MimeMultipart();
            message.setContent(multipart);
        }
    } // EmailConnect
    
    public void close()
    {
    } // close
    
    public void finalize()
    {
    } // finalize
    
    public String send() throws Exception
    {
        String result = "";
        Transport transport = null;
        try
        {
            if (!secure)
            {
                Transport.send(message);
            }
            else
            {
                transport = session.getTransport("smtps");
                transport.connect(host, port, username, password);
                transport.sendMessage(message, message.getAllRecipients());
            }
        }
        catch (Exception e)
        {
            result = "Unable to send message : "+e.getMessage();
            Log.log.error("Unable to send message: "+e.getMessage(), e);
        }
        finally 
        {
            if (transport != null)
            {
                transport.close();
            }
        }
        return result;
    } // send
    
    public String setFrom(String from)
    {
        String result = "";
        
        try
        {
            message.setFrom(new InternetAddress(from));
        }
        catch (Exception e)
        {
            result =  "Unable to add recipient: "+e.getMessage();
            Log.log.error("Unable to add recipient: "+e.getMessage());
        }
        
        return result;
    } // setFrom

    public String setReplyTo(String replyTo)
    {
        String result = "";
        if(StringUtils.isNotBlank(replyTo))
        {
            try
            {
                Address replyToAddress = new InternetAddress(replyTo);
                message.setReplyTo(new Address[] {replyToAddress});
            }
            catch (Exception e)
            {
                result =  "Unable to add recipient: "+e.getMessage();
                Log.log.error("Unable to add recipient: "+e.getMessage());
            }
        }
        return result;
    } // setFrom

    public String addTo(String addr)
    {
        String result = "";
        
        try
        {
            message.addRecipient(Message.RecipientType.TO, new InternetAddress(addr));
        }
        catch (Exception e)
        {
            result =  "Unable to add recipient: "+e.getMessage();
            Log.log.error("Unable to add recipient: "+e.getMessage());
        }
        
        return result;
    } // addTo

    public String addCC(String addr)
    {
        String result = "";
        
        try
        {
            message.addRecipient(Message.RecipientType.CC, new InternetAddress(addr));
        }
        catch (Exception e)
        {
            result = "Unable to add recipient: "+e.getMessage();
            Log.log.error("Unable to add recipient: "+e.getMessage());
        }
        return result;
    } // addCC

    public String addBCC(String addr)
    {
        String result = "";
        
        try
        {
            message.addRecipient(Message.RecipientType.BCC, new InternetAddress(addr));
        }
        catch (Exception e)
        {
            result = "Unable to add recipient: "+e.getMessage();
            Log.log.error("Unable to add recipient: "+e.getMessage());
        }
        return result;
    } // addBCC
    
    public String setSubject(String subject)
    {
        String result = "";
        
        try
        {
            message.setSubject(subject);
        }
        catch (Exception e)
        {
            result = "Unable to set message subject: "+e.getMessage();
            Log.log.error("Unable to set message subject: "+e.getMessage());
        }
        return result;
    } // setSubject

    public String setText(String text)
    {
        return setText(text, DEFAULT_CONTENTTYPE);
    } // setText

    public String setText(String text, String contentType)
    {
        String result = "";
        
        try
        {
            if (isMultipart)
            {
                BodyPart messageBodyPart = new MimeBodyPart();
                messageBodyPart.setText(text);
                multipart.addBodyPart(messageBodyPart);
            }
            else
            {
                //message.setText(text);
                message.setContent(text, contentType);
            }
        }
        catch (Exception e)
        {
            result = "Unable to set message text: "+e.getMessage();
            Log.log.error("Unable to set message text: "+e.getMessage(), e);
        }
        return result;
    } // setText

    public String addAttachment(File file)
    {
        String result = "";
        
        try
        {
            if (file.exists())
            {
                BodyPart messageBodyPart = new MimeBodyPart();
                DataSource src = new FileDataSource(file);
                messageBodyPart.setDataHandler(new DataHandler(src));
                messageBodyPart.setFileName(file.getName());
                multipart.addBodyPart(messageBodyPart);
            }
            else
            {
                throw new Exception("File does not exist: "+file.getAbsolutePath()+"/"+file.getName());
            }
        }
        catch (Exception e)
        {
            result = "Unable to add message attachment: "+e.getMessage();
            Log.log.error("Unable to add message attachment: "+e.getMessage());
        }
        return result;
    } // addAttachment
    
    public Session getSession()
    {
        return session;
    }

    public void setSession(Session session)
    {
        this.session = session;
    }

    public MimeMessage getMessage()
    {
        return message;
    }

    public void setMessage(MimeMessage message)
    {
        this.message = message;
    }

} // EmailConnect
