/******************************************************************************
* (C) Copyright 2014
*
* Resolve Systems, LLC
*
* All rights reserved
* This software is distributed under license and may not be used, copied,
* modified, or distributed without the express written permission of
* Resolve Systems, LLC.
*
******************************************************************************/

package com.resolve.connect;

import java.io.InputStream;

import com.resolve.util.Log;

/**
 * This thread consumes output from the remote server and displays it in the
 * terminal window.
 * 
 */
public class TelnetReader extends AbstractConnectReader
{
    StringBuffer buffer = new StringBuffer();
    InputStream in;
    private boolean printSpecial = false;
    
    public TelnetReader(InputStream in, boolean printSpecial)
    {
        this.in = in;
        // set daemon thread
        setDaemon(true);
        this.printSpecial = printSpecial;
    } // TelnetReader

    public boolean isPrintSpecial()
    {
        return printSpecial;
    }

    public void setPrintSpecial(boolean printSpecial)
    {
        this.printSpecial = printSpecial;
    }

    public void run()
    {
        try
        {
            if(in != null)
            {
                int ch;
                while( ( ch = in.read()) != -1) {
                    if(printSpecial)
                    {
                        // this will enter everything (incl. unicode) into the buffer
                        buffer.append((char) ch);
                    }
                    else
                    {
                        // this will enter only the ASCII characters.
                        if(ch < 128)
                        {
                            buffer.append((char) ch);
                        }
                    }
                }
            }
        }
        catch (Exception e) { }
    } // run
    
    public synchronized String read()
    {
        String result = "";
        
        if (buffer.length() > 0)
        {
            result = buffer.toString();
            buffer = new StringBuffer();
        }
        Log.log.debug(result);
        System.out.println(result);

        return result; 
    } // read
    
    public synchronized void clear()
    {
        buffer = new StringBuffer();
    } // clear
    
    public boolean hasContent()
    {
        return buffer.length() > 0;
    } // hasContent
} // TelnetReader
