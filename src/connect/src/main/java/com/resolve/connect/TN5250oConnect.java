/******************************************************************************
* (C) Copyright 2014
*
* Resolve Systems, LLC
*
* All rights reserved
* This software is distributed under license and may not be used, copied,
* modified, or distributed without the express written permission of
* Resolve Systems, LLC.
*
******************************************************************************/

package com.resolve.connect;

import com.resolve.rsbase.SessionObjectInterface;

public class TN5250oConnect extends OhioConnect implements SessionObjectInterface
{
    public TN5250oConnect(String host, int port) throws Exception
    {
        super("TN5250", host, port);

        this.connect();
    } // TN5250oConnect

} // TN5250oConnect
