/******************************************************************************
* (C) Copyright 2014
*
* Resolve Systems, LLC
*
* All rights reserved
* This software is distributed under license and may not be used, copied,
* modified, or distributed without the express written permission of
* Resolve Systems, LLC.
*
******************************************************************************/

package com.resolve.connect;

import java.io.InputStream;
import java.io.OutputStream;
import java.net.InetSocketAddress;
import java.net.Socket;
import java.net.SocketAddress;

import com.resolve.util.Log;
import com.resolve.util.StringUtils;

/**
 * This is a socket connector that leverages {@link Socket} object and send and receives data from it.
 */
public class SocketConnect extends Connect
{
    Socket conn; // socket connection
    InputStream in; // stdout from the session
    InputStream err; // stderr from session
    OutputStream out; // = new PrintWriter(socket.getOutputStream(), true);

    String charset = null; // option character set (e.g. US-ASCII, UTF-8)

    public SocketConnect(String hostname, int port) throws Exception
    {
        this(hostname, port, 0);
    } // SocketConnect

    public SocketConnect(String hostname, int port, int timeout) throws Exception
    {
        try
        {
            conn = new Socket(hostname, port);

            SocketAddress address = new InetSocketAddress(hostname, port);

            // set timeout
            if (timeout > 0)
            {
                conn.setSoTimeout(timeout);
            }
            if(!conn.isConnected())
            {
                conn.connect(address);
            }
            // set stream
            in = conn.getInputStream();
            out = conn.getOutputStream();

            // start session consumer
            reader = new SocketReader(in);
            ((SocketReader)reader).start();
            
            super.connect();
        }
        catch (Exception e)
        {
            Log.log.warn("Unable to connect to: " + hostname + " port: " + port + ". " + e.getMessage());
        }
    } // TelnetConnect

    public void close()
    {
        try
        {
            /* Close this session */
            if (conn != null)
            {
                conn.close();
            }
        }
        catch (Exception e)
        {
            Log.log.error("Failed to close TelnetConnect: " + e.getMessage());
        }

        isClosed = true;
    } // close

    public boolean isConnected()
    {
        boolean result = false;

        if (conn != null)
        {
            result = conn.isConnected();
        }

        return result;
    } // isConnected

    public void flush() throws Exception
    {
        if (out != null)
        {
            Log.log.trace("flushing");
            out.flush();
        }
    } // flush

    public void send(byte[] data) throws Exception
    {
        try
        {
            super.send();
            Log.log.trace("sending " + data.length + " bytes");
            out.write(data);
            out.flush();
        }
        catch (Exception e)
        {
            Log.log.warn(e.getMessage());
        }
    } // send

    public void send(String line) throws Exception
    {
        if(StringUtils.isNotEmpty(line))
        {
            Log.log.trace("sending " + line);
            try
            {
                super.send();

                // include line termination
                if (charset == null)
                {
                    send(line.getBytes());
                }
                else
                {
                    Log.log.trace("sending "+line.getBytes().length+" bytes");
                    send(line.getBytes(charset));
                }
            }
            catch (Exception e)
            {
                Log.log.warn(e.getMessage());
            }
        }
    } // send

    public void setCharset(String charset)
    {
        this.charset = charset;
    } // setChartset
    
    /*
    public static void main(String... args) throws Exception
    {
        Log.init();
        
        //This is a simulator written in perl.
        String host = "localhost";
        int port = 9000;
        
        SocketConnect connect = new SocketConnect(host, port);
        
        try
        {
            //SocketConnect connect = new SocketConnect(host, port, 10000, "---    END");
            
            String username = "resolve";
            String password = "resolve";
            String waitPattern = "---    END";
            connect.expect("");
            
            String[] commands = new String[] {"LGI:OP=\"" + username + "\",PWD=\"" + password + "\";"
            ,"REG NE:NAME=\"" + "WI07_Seidengasse L01" + "\";"
            ,"RST BTSBRD: TYPE=hugo, IDTYPE=hugo, BTSID=4711,HDBRDTYPE=hugo,CN=7, SRN=9, SN=99;"
            ,"UNREG NE:NAME=\"WI07_Seidengasse L01\";"
            ,"SHK HAND:;"
            ,"UNREG NE:NAME=\"WI07_Seidengasse L01\";"
            ,"LGO:OP=\"" + username + "\";"
            };
            
            for(String command : commands)
            {
                System.out.println("---------------START------------------");
                System.out.println("Command::: " + command);
//                Send String and get back String 
                System.out.println("--------------- Sending as text data ------------------\n");
                connect.send(command);
                String result = connect.expect(waitPattern);
                System.out.println("Response::: " + result);
                
//                Send binary data and get back binary
                System.out.println("--------------- Sending as binary data ------------------\n");
                connect.send(command.getBytes());
                byte[] resultBytes = connect.expect(waitPattern.getBytes(), -1);
                String result1 = new String(resultBytes);
                System.out.println("Response::: " + result1);
                System.out.println("---------------END------------------\n\n");
            }
            //System.out.println("Response::: " + connect.read());
        }
        catch (Exception e)
        {
            e.printStackTrace();
        } 
        finally
        {
            connect.close();
        }
        
    }
    */

} // SocketConnect
