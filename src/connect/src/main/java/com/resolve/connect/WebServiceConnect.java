/******************************************************************************
* (C) Copyright 2014
*
* Resolve Systems, LLC
*
* All rights reserved
* This software is distributed under license and may not be used, copied,
* modified, or distributed without the express written permission of
* Resolve Systems, LLC.
*
******************************************************************************/

package com.resolve.connect;

import java.net.URL;

import org.apache.cxf.endpoint.Client;
import org.apache.cxf.endpoint.dynamic.DynamicClientFactory;
import org.apache.cxf.transport.http.HTTPConduit;
import org.apache.cxf.transports.http.configuration.HTTPClientPolicy;

import com.resolve.util.Log;

import groovyx.net.ws.WSClient;

public class WebServiceConnect extends WSClient
{
    static final String HTTPS = "https";

    boolean simpleBindingEnabled = true;

    public WebServiceConnect(String wsdlUrl)
    {
        super(wsdlUrl, Thread.currentThread().getContextClassLoader());

        this.initialize();
    } // WebServiceConnect

    public WebServiceConnect(String wsdlUrl, Boolean simpleBindingEnabled)
    {
        super(wsdlUrl, Thread.currentThread().getContextClassLoader());
        this.simpleBindingEnabled = simpleBindingEnabled;

        this.initialize();
    } // WebServiceConnect

    public boolean isSimpleBindingEnabled()
    {
        return simpleBindingEnabled;
    } // isSimpleBindingEnabled

    public void setSimpleBindingEnabled(boolean simpleBindingEnabled)
    {
        this.simpleBindingEnabled = simpleBindingEnabled;
    } // setSimpleBindingEnabled

    /**
     * Initializes the default configurations for ssl, http basic authentication
     * and a proxy. If no properties were set previously, the standard
     * properties of the respective configurations will be used.
     */
    public void initialize()
    {
        HTTPConduit conduit;
        URL url;

        url = this.url;

        this.proxyHelper.initialize();
        this.basicAuthHelper.initialize();

        final boolean isSSLProtocol = HTTPS.equals(this.url.getProtocol());
        if (isSSLProtocol)
        {
            this.sslHelper.initialize();
            url = this.sslHelper.getLocalWsdlUrl(this.url);
        }

        this.client = createClient(url);

        this.soapHelper.enable(this.client);
        this.proxyHelper.enable(this.client);
        this.basicAuthHelper.enable(this.client);

        if (isSSLProtocol)
        {
            this.sslHelper.enable(this.client);
        }

        this.mtomHelper.enable(this.client);

        conduit = (HTTPConduit) this.client.getConduit();
        configureHttpClientPolicy(conduit);
    } // initialize

    /**
     * Configures the configuration for the transport of the client connection.
     * 
     * @param conduit
     *            The conduit that need to be modified
     */
    private void configureHttpClientPolicy(HTTPConduit conduit)
    {
        HTTPClientPolicy httpClientPolicy = conduit.getClient();
        httpClientPolicy.setAllowChunking(false);

        conduit.setClient(httpClientPolicy);
    } // configureHttpClientPolicy

    /**
     * The cxf-implementation of IWSClient.
     *   see groovyx.net.ws.IWSClient#createClient(java.lang.Object[])
     * 
     * @param url
     *            : url (The url of the wsdl-file), cl (The classloader)
     */
    public Client createClient(URL url)
    {
        Client result = null;

        DynamicClientFactory clientFactory;

        clientFactory = DynamicClientFactory.newInstance();
        clientFactory.setSimpleBindingEnabled(simpleBindingEnabled);

        result = clientFactory.createClient(url.toExternalForm(), this.classloader);
        return result;
    } // createClient

    public static WSClient createWSClient(String wsdlUrl)
    {
        WSClient client = new WSClient(wsdlUrl, Thread.currentThread().getContextClassLoader());
        client.initialize();
        return client;
    } // createWSClient

    public static Object createWSObject(WSClient client, String typeName) 
    {
        try
        {
            return client.create(typeName);
        }
        catch (IllegalAccessException e)
        {
            Log.log.error(e.getMessage(), e);
        }
        return null;
    } // createWSObject

} // WebServiceConnect
