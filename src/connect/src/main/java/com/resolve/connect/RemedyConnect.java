/******************************************************************************
* (C) Copyright 2014
*
* Resolve Systems, LLC
*
* All rights reserved
* This software is distributed under license and may not be used, copied,
* modified, or distributed without the express written permission of
* Resolve Systems, LLC.
*
******************************************************************************/

package com.resolve.connect;

import java.util.Map;
import java.util.Hashtable;
import java.util.Iterator;
import java.util.List;
import java.util.ArrayList;
import java.util.regex.Pattern;
import java.util.regex.Matcher;

import com.remedy.arsys.api.*;

import com.resolve.util.StringUtils;
import com.resolve.util.Log;


public class RemedyConnect
{
    ARServerUser session = null;
    static boolean debug = false;
    
    public RemedyConnect(String host, String username, String password)
    {
        this.connect(host, 0, username, password);
    } // RemedyConnect
    
    public RemedyConnect(String host, int port, String username, String password)
    {
        this.connect(host, port, username, password);
    } // RemedyConnect
    
    public static void setDebug(boolean debug)
    {
        RemedyConnect.debug = debug;
    } // setDebug
    
    public void connect(String host, int port, String username, String password)
    {
        if (host != null && username != null && password != null)
        {
            Log.log.debug("Connecting to AR Server at: "+host); 
            
            session = new ARServerUser();
            session.setServer(host);
            if (port != 0)
            {
                session.setPort(port);
            } 
            session.setUser(new AccessNameID(username));
            session.setPassword(new AccessNameID(password));
            
            try
            {
                session.verifyUser(new VerifyUserCriteria());
            }
            catch (ARException e)
            {
                Log.log.warn("Error verifying user: " + e);
                session.clear();
                session = null;
            }
            
            Log.log.debug("Connected to AR Server.");
        }
    } // connect
    
    public void close()
    {
        // Clear memory held by our entry object
        // Similar cleanup should be done for all objects created by a factory
        // EntryFactory.getFactory().releaseInstance(entry);
        
        // Clear memory used by our user context object
        if (session != null)
        {
            session.clear();
        }
        
        Log.log.info("AR System objects closed");
    } // close
    
    public String create(String form, String fieldValueStr) throws Exception
    {
        Map fieldValues = StringUtils.stringToMap(fieldValueStr, "=", "&");
        return create(form, fieldValues);
    } // create
    
    public String create(String form, Map fieldValues) throws Exception
    {
        String result = "";
        
        try
        {
            Entry entry = (Entry) EntryFactory.getFactory().newInstance();
            entry.setContext(session);
            entry.setSchemaID(new NameID(form));
            
            // set values
            setEntryItemValues(entry, fieldValues);
            
            // create record
            entry.create();
            result = entry.getEntryID().toString();
            
            // clean up
            EntryFactory.getFactory().releaseInstance(entry);
            
            Log.log.trace("Created entry id: " + result);
        }
        catch (ARException e)
        {
            ARExceptionHandler(e, "Problem while creating entry: ");
        }
        
        return result;
    } // create

    private void setEntryItemValues(Entry entry, Map fieldValues)
    {
        ArrayList entries = new ArrayList();
        for (Iterator i=fieldValues.entrySet().iterator(); i.hasNext(); )
        {
            Map.Entry rec = (Map.Entry)i.next();
            
            String key = (String)rec.getKey();
            String value = (String)rec.getValue();
            
            // only process field ids
            if (StringUtils.isNumeric(key))
            {
                entries.add(new EntryItem(new FieldID(Long.parseLong(key)), new Value(value)));
            }
        }
        
        // allocate EntryItems array
        EntryItem[] entryItems = new EntryItem[entries.size()];
        entries.toArray(entryItems);
        entry.setEntryItems(entryItems);
    } // setEntryItemValues
    
    public String update(String form, String entryId, String fieldValueStr) throws Exception
    {
        Map fieldValues = StringUtils.stringToMap(fieldValueStr, "=", "&");
        return update(form, entryId, fieldValues);
    } // update
    
    public String update(String form, String entryId, Map fieldValues) throws Exception
    {
        String result = null;
        
        try
        {
            // find record
            EntryKey entryKey = new EntryKey(new NameID(form), new EntryID(entryId));
            Entry entry = EntryFactory.findByKey(session, entryKey, null);
            
            // set values
            setEntryItemValues(entry, fieldValues);
            
            // update record
            entry.store();
            
            // clean up
            EntryFactory.getFactory().releaseInstance(entry);
            result = entryId;
            
            Log.log.debug("Record: "+entryId+" successfully updated");
        }
        catch(ARException e)
        {
            ARExceptionHandler(e,"Problem while modifying record: ");
        }
        
        return result;
    } // update
    
    public List find(String form, String criteria) throws Exception
    {
        List result = new ArrayList();
        
        //Log.log.debug("Finding records records with criteria: " + criteria);
        
        try
        {
            QualifierInfo qualifier = prepareQualifierInfo(form, criteria);
            if (qualifier != null)
            {
                // Set the EntryListCriteria
                EntryListCriteria listCriteria = new EntryListCriteria();
                listCriteria.setSchemaID(new NameID(form));
                listCriteria.setQualifier(qualifier);
                
                // Make the call to retreive the query results list
                Integer nMatches = new Integer(0);
                EntryListInfo[] entryInfo = EntryFactory.findEntryListInfos(session, listCriteria, null, false, nMatches);
                
                if(Log.log.isTraceEnabled())
                {
                    Log.log.trace("Query returned " + nMatches + " matches.");
                }
                if (nMatches.intValue() > 0)
                {
                    // Print out the matches
                    //Log.log.trace("EntryID:Description");
                    
                    for (int i = 0; i < entryInfo.length; i++)
                    {
                        result.add(entryInfo[i].getEntryID().toString());
                        if(Log.log.isTraceEnabled())
                        {
                            Log.log.trace(entryInfo[i].getEntryID().toString() + ":" + new String(entryInfo[i].getDescription()));
                        }
                    }
                }
                EntryFactory.getFactory().releaseInstance(entryInfo);
            }
        }
        catch (ARException e)
        {
            ARExceptionHandler(e, "Problem while querying by qualifier: ");
        }
        
        return result;
    } // find

    QualifierInfo prepareQualifierInfo(String form, String criteria) throws ARException
    {
        QualifierInfo result = null;
        
        // Set the field criteria to retrieve all field info
        FieldCriteria fCrit = new FieldCriteria();
        fCrit.setRetrieveAll(true);
        
        // Retrieve all types of fields
        FieldListCriteria fListCrit = new FieldListCriteria(new NameID(form), new Timestamp(0), FieldType.AR_ALL_FIELD);
        
        // Load the field array with all fields in the test form
        Field[] formFields = FieldFactory.findObjects(session, fListCrit, fCrit);
        
        // Create the search qualifier.
        result = Util.ARGetQualifier(session, criteria, formFields, null, Constants.AR_QUALCONTEXT_DEFAULT);
        
        // cleanup
        FieldFactory.getFactory().releaseInstance(formFields);
        
        return result;
    } // prepareQualifierInfo

    public Map retrieve(String form, String entryId) throws Exception
    {
        return retrieve(form, entryId, null);
    } // retrieve
    
    public Map retrieve(String form, String entryId, String fieldList) throws Exception
    {
        Map result = new Hashtable();
        
        Log.log.debug("Retrieving record with entry ID:" + entryId);
        
        try
        {
            // get list of fields to retrieve
            EntryCriteria entryCriteria = null;
            if (fieldList != null)
            {
                boolean hasFields = false;
                
                String[] fieldIds = fieldList.split("&");
                
                EntryListFieldInfo[] entryListFieldList = new EntryListFieldInfo[fieldIds.length];
                for (int i=0; i < fieldIds.length; i++)
                {
                    String id = fieldIds[i];
                    
                    if (!StringUtils.isEmpty(id))
                    {
                        Long fieldId = Long.parseLong(id.trim());
                        entryListFieldList[i] = new EntryListFieldInfo(new FieldID(fieldId));
                        
                        hasFields = true;
                    }
                }
                
                // Set the entry criteria
                if (hasFields)
                {
                    entryCriteria = new EntryCriteria();
                    entryCriteria.setEntryListFieldInfo(entryListFieldList);
                }
            }
            
            // retrieve entry with field criterias
            EntryKey entryKey = new EntryKey(new NameID(form), new EntryID(entryId));
            Entry entry = EntryFactory.findByKey(session, entryKey, entryCriteria);
            
            EntryItem[] entryItemList = entry.getEntryItems();
            if (entryItemList == null)
            {
                Log.log.debug("No data found for entry ID:" + entryId);
            }
            else
            {
                FieldCriteria fCrit = new FieldCriteria();
                fCrit.setRetrieveAll(true);
                
                Log.log.trace("Number of fields: " + entryItemList.length);
                for (int i = 0; i < entryItemList.length; i++)
                {
                    // Get the field's details
                    FieldID id = entryItemList[i].getFieldID();
                    Field field = FieldFactory.findByKey(session, new FieldKey(new NameID(form), id), fCrit);
                    
                    // Output field's name, value, id, and type
                    String value =  entryItemList[i].getValue().toString();
                    if (value == null)
                    {
                        value = "";
                    }
                    else if (!StringUtils.isAsciiPrintable(value))
                    {
                        value = "value not printable";
                    }
                    result.put(field.getName().toString(), value);
                    
                    Log.log.trace(field.getName().toString());
                    Log.log.trace(": " + entryItemList[i].getValue());
                    Log.log.trace(" ID: " + id);
                    Log.log.trace(" Field type: " + field.getDataType().toInt());
                    Log.log.trace("");
/*
                    // Handle if Date..
                    if (field.getDataType().toInt() == 7)
                    {
                        System.out.print(" Date value: ");
                        Timestamp callDateTimeTS = (Timestamp) entryItemList[i].getValue().getValue();
                        // First getValue returns a Value object,
                        // second returns a generic Object that
                        // can be cast as a Timestamp
                        if (callDateTimeTS != null) System.out.print(callDateTimeTS.toDate());
                    }
*/
                    // Release memory held by our field object
                    //field.clear();
                    FieldFactory.getFactory().releaseInstance(field);
                } 
            }
            EntryFactory.getFactory().releaseInstance(entry);
        }
        catch (ARException e)
        {
            ARExceptionHandler(e, "Problem while querying by entry id: ");
        }
        
        return result;
    } // retrieve

    public String print(String form, String entryId) throws Exception
    {
        return print(form, entryId, null);
    } // print
    
    public String print(String form, String entryId, String fieldList) throws Exception
    {
        String result = "";
        
        Map entry = retrieve(form, entryId, fieldList);
        
        for (Iterator i=entry.entrySet().iterator(); i.hasNext(); )
        {
            Map.Entry field = (Map.Entry)i.next();
            String key = (String)field.getKey();
            String value = (String)field.getValue();
            
            result += key+"="+value+"\n";
        }
        result += "\n\n";
        
        return result;
    } // print
    
    public String delete(String form, String entryId) throws Exception
    {
        String result = null;
        
        try
        {
            // retrieve entry with field criterias
            EntryKey entryKey = new EntryKey(new NameID(form), new EntryID(entryId));
            Entry entry = EntryFactory.findByKey(session, entryKey, null);
            
            // remove
            entry.remove();
            EntryFactory.getFactory().releaseInstance(entry);
            
            // set to entryId removed
            result = entryId;
            
            Log.log.trace("Record: "+entryId+" successfully deleted");
        }
        catch (ARException e)
        {
            ARExceptionHandler(e, "Problem while querying by entry id: ");
        }
        
        return result;
    } // delete

    public void printStatusList(StatusInfo[] statusList)
    {
        if (statusList == null || statusList.length == 0)
        {
            Log.log.trace("Status List is empty.");
            return;
        }
            
        Log.log.debug("Message type: ");
        switch (statusList[0].getMessageType())
        {
            case Constants.AR_RETURN_OK:
                Log.log.trace("Note");
                break;
            case Constants.AR_RETURN_WARNING:
                Log.log.trace("Warning");
                break;
            case Constants.AR_RETURN_ERROR:
                Log.log.trace("Error");
                break;
            case Constants.AR_RETURN_FATAL:
                Log.log.trace("Fatal Error");
                break;
            default:
                Log.log.trace("Unknown (" + statusList[0].getMessageType() + ")");
                break;
        }
            
        Log.log.trace("Status List:");
        for (int i = 0; i < statusList.length; i++)
        {
            Log.log.trace(statusList[i].getMessageText());
            Log.log.trace(statusList[i].getAppendedText());
        }
    } // printStatusList
    
    public Map createFieldValues(String log)
    {
        Map result = new Hashtable();
        
        Pattern pattern = Pattern.compile(".*\\((\\d+)\\) = (.+)");
        String[] lines = log.split("\n");
        
        for (int i=0; i < lines.length; i++)
        {
            String line = lines[i];
            
            Matcher matcher = pattern.matcher(line);
            if (matcher.matches())
            {
                String key = matcher.group(1);
                String value = matcher.group(2);
                
                if (!StringUtils.isEmpty(key))
                {
                    if (value.equals("\"\""))
                    {
                        value = "";
                    }
                    result.put(key, value);
                }
            }
        }
        Log.log.trace("fieldValues: "+result);
        
        return result;
    } // createFieldValues
    
    public void ARExceptionHandler(ARException e, String errMessage) throws Exception
    {
        /*
        System.out.println(errMessage);
        if (session != null)
        {
            printStatusList(session.getLastStatus());
        }
        System.out.print("Stack Trace:");
        e.printStackTrace();
        */
        Log.log.error("ARExceptionHandler: "+errMessage, e);
        throw e;
    } // ARExceptionHandler
    
//    public static void main(String[] args) throws Exception
//    {
//        // init connection
//        RemedyConnect remedy = new RemedyConnect("172.16.5.100", "Demo", "dalsup");
//        //remedy.setDebug(true);
//        
//        // add new record
//        Map fieldValues = new Hashtable();
//        fieldValues.put("10007000", "new record name: "+System.currentTimeMillis());
//        fieldValues.put("8", "test");
//        fieldValues.put("1000000001", "generationE");
//        String entryId = remedy.create("TMS:TaskTemplate", fieldValues);
//        
//        // update record
//        fieldValues.put("8", "new summary text here");
//        remedy.update("TMS:TaskTemplate", entryId, fieldValues);
//       
//        // find record eid - NOTE: must use single quotes around fieldid (or fieldname) and double quotes around string values
//        //List list = remedy.find("TMS:TaskTemplate", "( \'Create Date\' > \"1/1/2004 12:00:00 AM\" )");
//        //List list = remedy.find("TMS:TaskTemplate", "\'1000000001\' = \"generationE\"");
//        List list = remedy.find("TMS:TaskTemplate", "\'8\' = \"new summary text here\"");
//        Log.log.debug("list: "+list);
//        for (Iterator i=list.iterator(); i.hasNext(); )
//        {
//            String eid = (String)i.next();
//            
//            //Map result  = remedy.retrieve("TMS:TaskTemplate", eid);
//            Map result  = remedy.retrieve("TMS:TaskTemplate", eid, "8");
//            Log.log.debug("Result: "+result);
//            remedy.delete("TMS:TaskTemplate", eid);
//        }
//        
//        list = remedy.find("TMS:TaskTemplate", "\'8\' = \"new summary text here\"");
//        Log.log.debug("list: "+list);
//        for (Iterator i=list.iterator(); i.hasNext(); )
//        {
//            String eid = (String)i.next();
//                    
//            //Map result  = remedy.retrieve("TMS:TaskTemplate", eid);
//            Map result  = remedy.retrieve("TMS:TaskTemplate", eid, "8");
//            if(Log.log.isTraceEnabled())
//            {
//                Log.log.trace("Result: "+result);
//            }
//        }
//        
//        // delete record
//        remedy.delete("TMS:TaskTemplate", entryId);
//        
//        // close connection
//        remedy.close();
//        
//    } // main
    
} // RemedyConnect
