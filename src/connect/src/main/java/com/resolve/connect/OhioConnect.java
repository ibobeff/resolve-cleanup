/******************************************************************************
* (C) Copyright 2014
*
* Resolve Systems, LLC
*
* All rights reserved
* This software is distributed under license and may not be used, copied,
* modified, or distributed without the express written permission of
* Resolve Systems, LLC.
*
******************************************************************************/

package com.resolve.connect;

import org.ohio.iOhio;
import org.ohio.iOhioOIA;
import org.ohio.iOhioOIAListener;
import org.ohio.iOhioPosition;
import org.ohio.iOhioScreen;
import org.ohio.iOhioScreenListener;
import org.ohio.iOhioSession;
import org.ohio.iOhioSessionListener;

import com.icominfo.Ohio.OhioConst;
import com.icominfo.Ohio.OhioManager;
import com.icominfo.Ohio.OhioPosition;
import com.icominfo.Ohio.OhioScreen;
import com.icominfo.Ali.AliData;

import com.resolve.rsbase.SessionObjectInterface;
import com.resolve.util.Log;

public class OhioConnect implements iOhioOIAListener, iOhioScreenListener, iOhioSessionListener, SessionObjectInterface
{
    // only one manager is necessary and mandatory for several connections
    static OhioManager manager = new OhioManager();

    int emulation;
    String host;
    int port;
    String sessionName;
    iOhioSession session;

    // internal data for the session management
    int m_InputInhibited = -1;
    int m_Owner = -1;
    int m_screen = -1;

    public OhioConnect(String emul, String host, int port) throws Exception
    {
        int emulation;

        if (emul.equalsIgnoreCase("TN3270"))
        {
            emulation = AliData.ALI_EMUL_3270;
        }
        else if (emul.equalsIgnoreCase("TN5250"))
        {
            emulation = AliData.ALI_EMUL_5250;
        }
        else
        {
            throw new Exception("Invalid emulation type: " + emul);
        }

        this.emulation = emulation;
        this.host = host;
        this.port = port;
        this.sessionName = ""+System.currentTimeMillis();
    } // OhioConnect

    /**
     * this method allows connection to a host, with the following parameters:
     * emul: emulation type that can be found in the class
     * com.icominfo.Ali.AliData.ALI_EMUL_... 3270 emulation type number is
     * com.icominfo.Ali.AliData.ALI_EMUL_3270 = 134
     * com.icominfo.Ali.AliData.ALI_EMUL_5250 = 138 Host: Host TCP_IP address =
     * clemson.clemson.edu port: connection port, default is 23
     */
    public void connect()
    {
        // manager = new OhioManager();

        if (session == null || session.isConnected() == false)
        {

            /**
             * this is the independent controller used for the connection
             */
            manager.setControllerClassName("com.icominfo.Controller.Oem.Oem");

            /**
             * giving at least 3 parameters: the emul type, the TCP_IP address
             * and the port for the emul type, the value can be found in the
             * class com.icominfo.Ali.AliData.ALI_EMUL_... 3270 emulation type
             * number is 134
             */
            String connectionString = "Emul=" + emulation + "&Param=[Com1]Service.Adresse TCP_IP=" + host + ";Service.Port=" + port + ";";

            /**
             * giving the connection string and a session name the session name
             * identifies the session to the OhioManager
             */
            session = manager.openSession(connectionString, sessionName);

            /**
             * subscribing to the listener in order to receive some events, like
             * connection, disconnection screenUpdate...
             */
            session.getScreen().addScreenListener(this);
            session.getScreen().getOIA().addOIAListener(this);
            session.addSessionListener(this);

            /**
             * trying to connect
             */
            session.connect();
        }
    } // connect

    /**
     * method for disconnecting and closing the session if the session is not
     * closed, you can do a connect, and the session will be connected again the
     * resources will be released with the closesession method
     */
    public void disconnect()
    {
        if (session != null)
        {
            session.disconnect();
        }
        if (manager != null)
        {
            manager.closeSession(session);
        }
    } // disconnect

    public void close()
    {
        try
        {
            this.disconnect();
        }
        catch (Exception e)
        {
            Log.log.warn("Error closing connection: " + e.getMessage());
        }
    } // close

    public void finalize()
    {
        try
        {
            disconnect();
        }
        catch (Exception e)
        {
            Log.log.warn("Error closing connection: " + e.getMessage());
        }
    } // finalize

    /**
     * SessionListener called when an event occurs to the session: connect or
     * disconnect
     */
    public void onSessionChanged(int inUpdate)
    {
        if (inUpdate == iOhioSession.OHIO_STATE_CONNECTED)
        {
            Log.log.debug("Session connected");
        }

        if (inUpdate == iOhioSession.OHIO_STATE_DISCONNECTED)
        {
            Log.log.debug("Session disconnected");
        }
    } // onSessionChanged

    /**
     * ScreenListener called when an event occurs on the screen: host or client
     * update
     */
    public void onScreenChanged(int inUpdate, iOhioPosition inStart, iOhioPosition inEnd)
    {
        if (inUpdate == iOhioScreen.OHIO_UPDATE_CLIENT)
        {
            Log.log.trace("Screen updated by client");
        }
        if (inUpdate == iOhioScreen.OHIO_UPDATE_HOST)
        {
            Log.log.trace("Screen updated by host");
            m_screen = inUpdate;
        }
    } // onScreenChange

    /**
     * OIA listener called when an event occurs on oia
     */
    public void onOIAChanged()
    {
        iOhioOIA oia = session.getScreen().getOIA();

        m_InputInhibited = oia.getInputInhibited();
        switch (m_InputInhibited)
        {

            case iOhioOIA.OHIO_INPUTINHIBITED_COMMCHECK:
                Log.log.trace("InputInhibited=COMMCHECK");
                break;
            case iOhioOIA.OHIO_INPUTINHIBITED_MACHINECHECK:
                Log.log.trace("InputInhibited=MACHINECHECK");
                break;
            case iOhioOIA.OHIO_INPUTINHIBITED_NOTINHIBITED:
                Log.log.trace("InputInhibited=NOTINHIBITED");
                break;
            case iOhioOIA.OHIO_INPUTINHIBITED_OTHER:
                Log.log.trace("InputInhibited=OTHER");
                break;
            case iOhioOIA.OHIO_INPUTINHIBITED_PROGCHECK:
                Log.log.trace("InputInhibited=PROGCHECK");
                break;
            case iOhioOIA.OHIO_INPUTINHIBITED_SYSTEM_WAIT:
                Log.log.trace("InputInhibited=SYSTEM_WAIT");
                break;
        }

        m_Owner = oia.getOwner();
        switch (m_Owner)
        {
            case iOhioOIA.OHIO_OWNER_APP:
                Log.log.trace("Owner=APP");
                break;
            case iOhioOIA.OHIO_OWNER_NVT:
                Log.log.trace("Owner=NVT");
                break;
            case iOhioOIA.OHIO_OWNER_SSCP:
                Log.log.trace("Owner=SSCP");
                break;
            case iOhioOIA.OHIO_OWNER_UNKNOWN:
                Log.log.trace("Owner=UNKNOWN");
                break;
            case iOhioOIA.OHIO_OWNER_UNOWNED:
                Log.log.trace("Owner=UNOWNED");
                break;
        }
    } // onOIAChanged

    /**
     * get and set cursor position
     */
    public iOhioPosition getCursor()
    {
        return session.getScreen().getCursor();
    } // getCursor

    public void setCursor(iOhioPosition position)
    {
        session.getScreen().setCursor(position);
    } // setCursor

    public void setCursor(int row, int col)
    {
        session.getScreen().setCursor(new OhioPosition(row, col));
    } // setCursor

    /**
     * get ohio screen object
     */
    public OhioScreen getScreen()
    {
        return (OhioScreen) session.getScreen();
    } // getScreen

    /**
     * get data
     */

    public String getData(iOhioPosition position, int len)
    {
        return new String(((OhioScreen) session.getScreen()).getData(position, len, iOhio.OHIO_PLANE_TEXT));
    } // getData

    public String getData(int row, int col, int len)
    {
        return new String(((OhioScreen) session.getScreen()).getData(new OhioPosition(row, col), len, iOhio.OHIO_PLANE_TEXT));
    } // getData

    public String getData(iOhioPosition startPos, iOhioPosition endPos)
    {
        return new String(session.getScreen().getData(startPos, endPos, iOhio.OHIO_PLANE_TEXT));
    } // getData

    public String getData(int startRow, int startCol, int endRow, int endCol)
    {
        return new String(((OhioScreen) session.getScreen()).getData(new OhioPosition(startRow, startCol), new OhioPosition(endRow, endCol),
                        iOhio.OHIO_PLANE_TEXT));
    } // getData

    /**
     * send strings
     */

    public void send(String value)
    {
        session.getScreen().setString(value, null);
    } // send

    public void send(String value, iOhioPosition position)
    {
        session.getScreen().setString(value, position);
    } // send

    public void send(String value, int row, int col)
    {
        session.getScreen().setString(value, new OhioPosition(row, col));
    } // send

    /**
     * this method sends the enter key to the host
     */
    public void sendEnter()
    {
        session.getScreen().sendKeys("[enter]", null);
    } // sendEnter

    public void sendKey(String value)
    {
        session.getScreen().sendKeys(value, null);
    } // sendKey

    public void sendKey(String value, iOhioPosition position)
    {
        session.getScreen().sendKeys(value, position);
    } // sendKey

    public void sendKey(String value, int row, int col)
    {
        session.getScreen().sendKeys(value, new OhioPosition(row, col));
    } // sendKey

    public void sendKey(int value)
    {
        ((OhioScreen) session.getScreen()).sendFunctionKey(value, null);
    } // sendKey

    public void sendKey(int value, OhioPosition position)
    {
        ((OhioScreen) session.getScreen()).sendFunctionKey(value, position);
    } // sendKey

    public void sendKey(int value, int row, int col)
    {
        ((OhioScreen) session.getScreen()).sendFunctionKey(value, new OhioPosition(row, col));
    } // sendKey

    public void sendFunctionKey(int value)
    {
        ((OhioScreen) session.getScreen()).sendFunctionKey(value, null);
    } // sendFunctionKey

    public void sendAidKey(int value)
    {
        session.getScreen().sendAid(value);
    } // sendAidKey

    /**
     * this method waits for a new screen here, waiting for
     * INPUTINHIBITED=NOTINHIBITED and for OHIO_UPDATE_HOST
     * 
     * WARNING: please, the synchro for a new screen is specific for each
     * host!!!
     */

    /**
     * wait for screen to refresh
     * 
     * @param timeout - timeout in seconds
     */
    public boolean waitForScreen()
    {
        // wait for default 10 secs
        return waitForScreen(10);
    } // waitForScreen
    
    
    public boolean waitForScreen(int timeout)
    {
        boolean result = true;
        
        int retry = timeout;
        try
        {
            while (retry > 0 && (m_screen != org.ohio.iOhio.OHIO_UPDATE_HOST || m_InputInhibited != org.ohio.iOhio.OHIO_INPUTINHIBITED_NOTINHIBITED))
            {
                Thread.sleep(1000);
                retry--;
            }
        }
        catch (Exception e)
        {
            result = false;
        }
        
        // timeout
        if (retry == 0)
        {
            result = false;
        }
        
        m_screen = -1;
        
        return result;
    } // waitForScreen

    /**
     * retrieving the screen and printing it
     */
    public String printScreen()
    {
        return session.getScreen().getString();
    } // printScreen

    /**
     * Main example
     */
//    public static void main(String args[]) throws Exception
//    {
//        Log.init("");
//
//        OhioConnect conn = new OhioConnect("TN5250", "96.47.200.189", 23);
//        // testOhio.connect(com.icominfo.Ali.AliData.ALI_EMUL_3270,
//        // "clemson.clemson.edu", 23, "clemson");
//        conn.connect();
//        if (conn.waitForScreen(20) == false)
//        {
//            System.out.println("waitForScreen timed-out");
//        }
//        System.out.println(conn.printScreen());
//        System.out.println("Press enter for next screen");
//        
//        OhioConnect conn2 = new OhioConnect("TN5250", "96.47.200.189", 23);
//        // testOhio.connect(com.icominfo.Ali.AliData.ALI_EMUL_3270,
//        // "clemson.clemson.edu", 23, "clemson");
//        conn2.connect();
//        if (conn2.waitForScreen(20) == false)
//        {
//            System.out.println("waitForScreen timed-out");
//        }
//        System.out.println(conn2.printScreen());
//        System.out.println("Press enter for next screen");
//        
//        OhioConnect conn3 = new OhioConnect("TN5250", "96.47.200.189", 23);
//        // testOhio.connect(com.icominfo.Ali.AliData.ALI_EMUL_3270,
//        // "clemson.clemson.edu", 23, "clemson");
//        conn3.connect();
//        if (conn3.waitForScreen(20) == false)
//        {
//            System.out.println("waitForScreen timed-out");
//        }
//        System.out.println(conn3.printScreen());
//        System.out.println("Press enter for next screen");
//        
//        /*
//        conn.pause();
//
//        // logging in
//        conn.setCursor(18, 65);
//        conn.send("GEISERJ");
//        conn.setCursor(19, 65);
//        conn.send("resolve");
//        conn.sendEnter();
//
//        if (conn.waitForScreen(20) == false)
//        {
//            System.out.println("waitForScreen timed-out");
//        }
//        System.out.println(conn.printScreen());
//        System.out.println("Press enter for next screen");
//        conn.pause();
//
//        // logout
//        conn.send("90");
//        conn.sendEnter();
//
//        conn.waitForScreen();
//        System.out.println(conn.printScreen());
//        System.out.println("Press enter for next screen");
//        conn.pause();
//        */
//        /*
//         * testOhio.sendEnter(); testOhio.waitForScreen();
//         * testOhio.printScreen();
//         * System.out.println("Press enter to disconnect"); // TestOhio.pause();
//         */
//
//        System.out.println("Press enter to quit the example");
//        conn.pause();
//        conn.disconnect();
//        conn2.disconnect();
//        conn3.disconnect();
//    } // main

    // used by main example
    public static void pause()
    {
        try
        {
            while (System.in.read() != 13)
            {
            }
        }
        catch (Exception e)
        {
        }
    } // pause

    /**
     * Constants
     */

	public static java.lang.String     OHIO_KEY_ATTN = OhioConst.OHIO_KEY_ATTN; // Attention
	public static java.lang.String     OHIO_KEY_BACKSPACE = OhioConst.OHIO_KEY_BACKSPACE; // Destructive Backspace - move cursor one position left, delete character, shift remainder of field one position left.
	public static java.lang.String     OHIO_KEY_BACKTAB = OhioConst.OHIO_KEY_BACKTAB ; // Back Tab - tab to previous unprotected field
	public static java.lang.String     OHIO_KEY_CLEAR = OhioConst.OHIO_KEY_CLEAR; // Clear
	public static java.lang.String     OHIO_KEY_CURSORSELECT = OhioConst.OHIO_KEY_CURSORSELECT; // Cursor Select
	public static java.lang.String     OHIO_KEY_DELETE = OhioConst.OHIO_KEY_DELETE; // Delete at cursor, shift remainder of field one position left
	public static java.lang.String     OHIO_KEY_DOWN = OhioConst.OHIO_KEY_DOWN; // Cursor down
	public static java.lang.String     OHIO_KEY_DUP = OhioConst.OHIO_KEY_DUP; // Duplicate
	public static java.lang.String     OHIO_KEY_ENTER = OhioConst.OHIO_KEY_ENTER; // Enter
	public static java.lang.String     OHIO_KEY_ERASEEOF = OhioConst.OHIO_KEY_ERASEEOF; // Erase from cursor to end of field, inclusive
	public static java.lang.String     OHIO_KEY_ERASEINPUT = OhioConst.OHIO_KEY_ERASEINPUT; // Erase input - clear all unprotected fields
	public static java.lang.String     OHIO_KEY_FASTDOWN = OhioConst.OHIO_KEY_FASTDOWN; // Cursor down two rows
	public static java.lang.String     OHIO_KEY_FASTLEFT = OhioConst.OHIO_KEY_FASTLEFT; // Cursor left two positions
	public static java.lang.String     OHIO_KEY_FASTRIGHT = OhioConst.OHIO_KEY_FASTRIGHT; // Cusor right two positions
	public static java.lang.String     OHIO_KEY_FASTUP = OhioConst.OHIO_KEY_FASTUP; // Cursor up two rows
	public static java.lang.String     OHIO_KEY_FIELDMARK = OhioConst.OHIO_KEY_FIELDMARK; // Field Mark
	public static java.lang.String     OHIO_KEY_HOME = OhioConst.OHIO_KEY_HOME; // Home
	public static java.lang.String     OHIO_KEY_INSERT = OhioConst.OHIO_KEY_INSERT; // Insert mode - turns on insert mode for all subsequent keystrokes until [reset]
	public static java.lang.String     OHIO_KEY_LEFT = OhioConst.OHIO_KEY_LEFT; // Cursor left
	public static java.lang.String     OHIO_KEY_NEWLINE = OhioConst.OHIO_KEY_NEWLINE; // New line - move to first unprotected field on next or subsequent line
	public static java.lang.String     OHIO_KEY_PA1 = OhioConst.OHIO_KEY_PA1; // Program Attention 1
	public static java.lang.String     OHIO_KEY_PA2 = OhioConst.OHIO_KEY_PA2; // Program Attention 2
	public static java.lang.String     OHIO_KEY_PA3 = OhioConst.OHIO_KEY_PA3; // Program Attention 3
	public static java.lang.String     OHIO_KEY_PF1 = OhioConst.OHIO_KEY_PF1; // Program Function 1
	public static java.lang.String     OHIO_KEY_PF10 = OhioConst.OHIO_KEY_PF10; // Program Function 10
	public static java.lang.String     OHIO_KEY_PF11 = OhioConst.OHIO_KEY_PF11; // Program Function 11
	public static java.lang.String     OHIO_KEY_PF12 = OhioConst.OHIO_KEY_PF12; // Program Function 12
	public static java.lang.String     OHIO_KEY_PF13 = OhioConst.OHIO_KEY_PF13; // Program Function 13
	public static java.lang.String     OHIO_KEY_PF14 = OhioConst.OHIO_KEY_PF14; // Program Function 14
	public static java.lang.String     OHIO_KEY_PF15 = OhioConst.OHIO_KEY_PF15; // Program Function 15
	public static java.lang.String     OHIO_KEY_PF16 = OhioConst.OHIO_KEY_PF16; // Program Function 16
	public static java.lang.String     OHIO_KEY_PF17 = OhioConst.OHIO_KEY_PF17; // Program Function 17
	public static java.lang.String     OHIO_KEY_PF18 = OhioConst.OHIO_KEY_PF18; // Program Function 18
	public static java.lang.String     OHIO_KEY_PF19 = OhioConst.OHIO_KEY_PF19; // Program Function 19
	public static java.lang.String     OHIO_KEY_PF2 = OhioConst.OHIO_KEY_PF2; // Program Function 2
	public static java.lang.String     OHIO_KEY_PF20 = OhioConst.OHIO_KEY_PF20; // Program Function 20
	public static java.lang.String     OHIO_KEY_PF21 = OhioConst.OHIO_KEY_PF21; // Program Function 21
	public static java.lang.String     OHIO_KEY_PF22 = OhioConst.OHIO_KEY_PF22; // Program Function 22
	public static java.lang.String     OHIO_KEY_PF23 = OhioConst.OHIO_KEY_PF23; // Program Function 23
	public static java.lang.String     OHIO_KEY_PF24 = OhioConst.OHIO_KEY_PF24; // Program Function 24
	public static java.lang.String     OHIO_KEY_PF3 = OhioConst.OHIO_KEY_PF3; // Program Function 3
	public static java.lang.String     OHIO_KEY_PF4 = OhioConst.OHIO_KEY_PF4; // Program Function 4
	public static java.lang.String     OHIO_KEY_PF5 = OhioConst.OHIO_KEY_PF5; // Program Function 5
	public static java.lang.String     OHIO_KEY_PF6 = OhioConst.OHIO_KEY_PF6; // Program Function 6
	public static java.lang.String     OHIO_KEY_PF7 = OhioConst.OHIO_KEY_PF7; // Program Function 7
	public static java.lang.String     OHIO_KEY_PF8 = OhioConst.OHIO_KEY_PF8; // Program Function 8
	public static java.lang.String     OHIO_KEY_PF9 = OhioConst.OHIO_KEY_PF9; // Program Function 9
	public static java.lang.String     OHIO_KEY_RESET = OhioConst.OHIO_KEY_RESET; // Reset - clear keyboard lock and clear insert mode
	public static java.lang.String     OHIO_KEY_RIGHT = OhioConst.OHIO_KEY_RIGHT; // Cursor right
	public static java.lang.String     OHIO_KEY_SYSREQ = OhioConst.OHIO_KEY_SYSREQ; // System Request
	public static java.lang.String     OHIO_KEY_TAB = OhioConst.OHIO_KEY_TAB; // Tab forward to next unprotected field
	public static java.lang.String     OHIO_KEY_UP = OhioConst.OHIO_KEY_UP; // Cursor up
} // OhioConnect
