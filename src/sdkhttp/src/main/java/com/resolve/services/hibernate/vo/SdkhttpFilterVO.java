package com.resolve.services.hibernate.vo;

import com.resolve.util.MappingAnnotation;

public class SdkhttpFilterVO extends GatewayFilterVO {

    private static final long serialVersionUID = 1L;

    public SdkhttpFilterVO() {
    }

    private Integer UPort;

    private String UUri;

    private Boolean USsl;

    @MappingAnnotation(columnName = "PORT")
    public Integer getUPort() {
        return this.UPort;
    }

    public void setUPort(Integer uPort) {
        this.UPort = uPort;
    }

    @MappingAnnotation(columnName = "URI")
    public String getUUri() {
        return this.UUri;
    }

    public void setUUri(String uUri) {
        this.UUri = uUri;
    }

    @MappingAnnotation(columnName = "SSL")
    public Boolean getUSsl() {
        return this.USsl;
    }

    public void setUSsl(Boolean uSsl) {
        this.USsl = uSsl;
    }
}

