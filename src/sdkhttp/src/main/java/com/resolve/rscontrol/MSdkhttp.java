package com.resolve.rscontrol;

import com.resolve.persistence.model.SdkhttpFilter;

public class MSdkhttp extends MGateway {

    private static final String MODEL_NAME = SdkhttpFilter.class.getSimpleName();

    protected String getModelName() {
        return MODEL_NAME;
    }
}

