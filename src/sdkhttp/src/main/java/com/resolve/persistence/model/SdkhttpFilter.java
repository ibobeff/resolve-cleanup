package com.resolve.persistence.model;

import java.util.List;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Lob;
import javax.persistence.Table;
import javax.persistence.UniqueConstraint;
import com.resolve.services.interfaces.VO;
import com.resolve.services.hibernate.vo.SdkhttpFilterVO;

@Entity
@Table(name = "sdkhttp_filter", uniqueConstraints = { @UniqueConstraint(columnNames = { "u_name", "u_queue" }) })
public class SdkhttpFilter extends GatewayFilter<SdkhttpFilterVO> {

    private static final long serialVersionUID = 1L;

    public SdkhttpFilter() {
    }

    public SdkhttpFilter(SdkhttpFilterVO vo) {
        applyVOToModel(vo);
    }

    private Integer UPort;

    private String UUri;

    private Boolean USsl;

    @Column(name = "u_port")
    public Integer getUPort() {
        return this.UPort;
    }

    public void setUPort(Integer uPort) {
        this.UPort = uPort;
    }

    @Column(name = "u_uri", length = 256)
    public String getUUri() {
        return this.UUri;
    }

    public void setUUri(String uUri) {
        this.UUri = uUri;
    }

    @Column(name = "u_ssl", length = 1)
    public Boolean getUSsl() {
        return this.USsl;
    }

    public void setUSsl(Boolean uSsl) {
        this.USsl = uSsl;
    }

    public SdkhttpFilterVO doGetVO() {
        SdkhttpFilterVO vo = new SdkhttpFilterVO();
        super.doGetBaseVO(vo);
        vo.setUPort(getUPort());
        vo.setUUri(getUUri());
        vo.setUSsl(getUSsl());
        return vo;
    }

    @Override
    public void applyVOToModel(SdkhttpFilterVO vo) {
        if (vo == null) return;
        super.applyVOToModel(vo);
        if (!VO.INTEGER_DEFAULT.equals(vo.getUPort())) this.setUPort(vo.getUPort()); else ;
        if (!VO.STRING_DEFAULT.equals(vo.getUUri())) this.setUUri(vo.getUUri()); else ;
        this.setUSsl(vo.getUSsl());
    }
}

