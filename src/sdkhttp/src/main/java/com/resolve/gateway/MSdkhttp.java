package com.resolve.gateway;

import java.util.Map;
import com.sdkhttp.gateway.SdkhttpFilter;
import com.sdkhttp.gateway.SdkhttpGateway;

public class MSdkhttp extends MGateway {

    private static final String RSCONTROL_SET_FILTERS = "MSdkhttp.setFilters";

    private static final SdkhttpGateway instance = SdkhttpGateway.getInstance();

    public MSdkhttp() {
        super.instance = instance;
    }

    protected String getSetFilterMethodName() {
        return RSCONTROL_SET_FILTERS;
    }

    /**
     * Populates the {@link Filter} data into the given filterMap {@link Map} instance
     * 
     * @param filterMap
     *      the {@link Map} instance to hold the {@link Filter} data
     * @param filter
     *      a {@link SdkhttpFilter} instance
     *            
     */
    public void populateGatewaySpecificFilterProperties(Map<String, String> filterMap, Filter filter) {
        if (instance.isActive()) {
            SdkhttpFilter sdkhttpFilter = (SdkhttpFilter) filter;
            filterMap.put(SdkhttpFilter.PORT, "" + sdkhttpFilter.getPort());
            filterMap.put(SdkhttpFilter.URI, sdkhttpFilter.getUri());
            filterMap.put(SdkhttpFilter.SSL, "" + sdkhttpFilter.getSsl());
        }
    }
}

