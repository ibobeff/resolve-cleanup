package com.sdkhttp.gateway;


import java.io.IOException;
import java.util.Map;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.resolve.util.Log;
import com.resolve.util.StringUtils;
import com.resolve.util.SystemUtil;

@SuppressWarnings("serial")
public class FilterServlet extends HttpServlet
{
    private final String filterName;

    protected ExecutorService executor = Executors.newFixedThreadPool(SystemUtil.getCPUCount());

    private SdkhttpGateway instance = SdkhttpGateway.getInstance();

    public FilterServlet(String filterName)
    {
        this.filterName = filterName;
    }

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException
    {
        Log.log.debug(request.getRequestURI());
        response.setContentType("text/plain");
        response.setStatus(HttpServletResponse.SC_OK);
        
        String inputString = StringUtils.toString(request.getInputStream(), "utf-8");
        //these are the post parameters through a form
        Map<String, String> params = StringUtils.urlToMap(inputString);

        //this loop will read from the query string parameters. e.g. ?name1=value&name2=value2
        Map<String, String[]> requestParameters = request.getParameterMap();
        for (String key : requestParameters.keySet())
        {
            if (requestParameters.get(key).length > 0)
            {
                params.put(key, StringUtils.arrayToString(requestParameters.get(key), ","));
            }
        }

        executor.execute(new RequestProcessor(instance, filterName, request, response,  params));
        
        response.getWriter().println("Success");
    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException
    {
        String result = "HTTP request submitted successfully.";

        if(Log.log.isTraceEnabled())
        {
            Log.log.trace(request.getRequestURI());
            Log.log.trace("Client IP: " + request.getRemoteAddr());
            Log.log.trace("Client Host: " + request.getRemoteHost());
        }

        String inputString = StringUtils.toString(request.getInputStream(), "utf-8");
        //these are the post parameters through a form
        Map<String, String> params = StringUtils.urlToMap(inputString);

        //this loop will read from the query string parameters. e.g. ?name1=value&name2=value2
        Map<String, String[]> requestParameters = request.getParameterMap();
        for (String key : requestParameters.keySet())
        {
            if (requestParameters.get(key).length > 0)
            {
                params.put(key, StringUtils.arrayToString(requestParameters.get(key), ","));
            }
        }

        executor.execute(new RequestProcessor(instance, filterName, request, response,  params));
        response.getWriter().println(result);
    }

    static class RequestProcessor implements Runnable
    {
        final SdkhttpGateway instance;
        final String filterName;
        final HttpServletRequest request;
        final HttpServletResponse response;
        final  Map<String, String> params;
        
        public RequestProcessor(final SdkhttpGateway instance, final String filterName, final HttpServletRequest request, final HttpServletResponse response, final  Map<String, String> params)
        {
            this.instance = instance;
            this.filterName = filterName;
            this.request = request;
            this.response = response;
            this.params = params;
        }

        @Override
        public void run()
        {
            try
            {
                // process the data through a filter.
                boolean isProcessed = instance.processData(filterName, params);

                if (isProcessed)
                {
                    response.setStatus(HttpServletResponse.SC_OK);
                    response.setContentType("text/plain");
                    response.getWriter().println("processed");
                }
                else
                {
                    // The requested resource is no longer available at the server
                    // this happens when a filter is no longer active, using http status code is 410
                    response.setStatus(HttpServletResponse.SC_GONE);
                    Log.log.info("Gateway filter " + filterName + " not available.");
                }
            }
            catch (Exception e)
            {
                response.setStatus(HttpServletResponse.SC_BAD_REQUEST);
                Log.log.error(e.getMessage());
            }
        }
    }
}
