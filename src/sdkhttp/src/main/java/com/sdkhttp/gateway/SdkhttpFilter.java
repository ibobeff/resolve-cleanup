package com.sdkhttp.gateway;

import com.resolve.gateway.BaseFilter;
import com.resolve.util.Log;

public class SdkhttpFilter extends BaseFilter {

    public static final String PORT = "PORT";

    public static final String URI = "URI";

    public static final String SSL = "SSL";

    private Integer port;

    private String uri;

    private Boolean ssl;

    public SdkhttpFilter(String id, String active, String order, String interval, String eventEventId, String runbook, String script, String port, String uri, String ssl) {
        super(id, active, order, interval, eventEventId, runbook, script);
        try {
            this.port = new Integer(port);
        } catch (Exception e) {
            Log.log.error("port" + " should be in type " + "Integer");
        }
        this.uri = uri;
        try {
            this.ssl = new Boolean(ssl);
        } catch (Exception e) {
            Log.log.error("ssl" + " should be in type " + "Boolean");
        }
    }

    public Integer getPort() {
        return this.port;
    }

    public void setPort(Integer port) {
        this.port = port;
    }

    public String getUri() {
        return this.uri;
    }

    public void setUri(String uri) {
        this.uri = uri;
    }

    public Boolean getSsl() {
        return this.ssl;
    }

    public void setSsl(Boolean ssl) {
        this.ssl = ssl;
    }
}

