package com.sdkhttp.gateway;

import java.io.File;
import java.util.HashMap;
import java.util.Map;
import org.apache.commons.io.FileUtils;

import com.resolve.rsbase.MainBase;
import com.resolve.gateway.Filter;
import com.resolve.rsremote.ConfigReceiveGateway;
import com.resolve.util.Log;
import com.resolve.util.ObjectProperties;
import com.resolve.util.StringUtils;
import com.resolve.util.XDoc;

public class ConfigReceiveSdkhttp extends ConfigReceiveGateway {

    private static final long serialVersionUID = 1L;

    private static final String RECEIVE_SDKHTTP_NODE = "./RECEIVE/SDKHTTP/";

    private static final String RECEIVE_SDKHTTP_FILTER = RECEIVE_SDKHTTP_NODE + "FILTER";

    private String queue = "SDKHTTP";

    private static final String RECEIVE_SDKHTTP_ATTR_USERNAME = RECEIVE_SDKHTTP_NODE + "@USERNAME";

    private static final String RECEIVE_SDKHTTP_ATTR_PORT = RECEIVE_SDKHTTP_NODE + "@PORT";

    private static final String RECEIVE_SDKHTTP_ATTR_SSLCERTIFICATE = RECEIVE_SDKHTTP_NODE + "@SSLCERTIFICATE";

    private static final String RECEIVE_SDKHTTP_ATTR_SSLPASSWORD = RECEIVE_SDKHTTP_NODE + "@SSLPASSWORD";

    private static final String RECEIVE_SDKHTTP_ATTR_PASSWORD = RECEIVE_SDKHTTP_NODE + "@PASSWORD";

    private static final String RECEIVE_SDKHTTP_ATTR_SSL = RECEIVE_SDKHTTP_NODE + "@SSL";

    private String username = "";

    private Integer port = 0;

    private String sslcertificate = "";

    private String sslpassword = "";

    private String password = "";

    private Boolean ssl = false;

    public String getQueue() {
        return queue;
    }

    public void setQueue(String queue) {
        this.queue = queue;
    }

    public String getUsername() {
        return this.username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public Integer getPort() {
        return this.port;
    }

    public void setPort(Integer port) {
        this.port = port;
    }

    public String getSslcertificate() {
        return this.sslcertificate;
    }

    public void setSslcertificate(String sslcertificate) {
        this.sslcertificate = sslcertificate;
    }

    public String getSslpassword() {
        return this.sslpassword;
    }

    public void setSslpassword(String sslpassword) {
        this.sslpassword = sslpassword;
    }

    public String getPassword() {
        return this.password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public Boolean getSsl() {
        return this.ssl;
    }

    public void setSsl(Boolean ssl) {
        this.ssl = ssl;
    }

    public ConfigReceiveSdkhttp(XDoc config) throws Exception {
        super(config);
        define("username", STRING, RECEIVE_SDKHTTP_ATTR_USERNAME);
        define("port", INTEGER, RECEIVE_SDKHTTP_ATTR_PORT);
        define("sslcertificate", STRING, RECEIVE_SDKHTTP_ATTR_SSLCERTIFICATE);
        define("sslpassword", SECURE, RECEIVE_SDKHTTP_ATTR_SSLPASSWORD);
        define("password", SECURE, RECEIVE_SDKHTTP_ATTR_PASSWORD);
        define("ssl", BOOLEAN, RECEIVE_SDKHTTP_ATTR_SSL);
    }

    @Override
    public String getRootNode() {
        return RECEIVE_SDKHTTP_NODE;
    }

    @Override
    public void load() throws Exception {
        try {
            loadAttributes();
            if (isActive()) {
                SdkhttpGateway sdkhttpGateway = SdkhttpGateway.getInstance(this);
                filters = xdoc.getListMapValue(RECEIVE_SDKHTTP_FILTER);
                if (filters.size() > 0) {
                    for (Map<String, Object> values : filters) {
                        String filterName = ((String) values.get(SdkhttpFilter.ID)).toLowerCase();
                        File scriptFile = getFile(MainBase.main.release.serviceName + "/config/sdkhttp/" + filterName + ".groovy");
                        try {
                            if (scriptFile.exists()) {
                                String groovy = FileUtils.readFileToString(scriptFile, "UTF-8");
                                if (StringUtils.isNotBlank(groovy)) {
                                    values.put(SdkhttpFilter.SCRIPT, groovy);
                                    Log.log.debug("Loaded groovy script: " + scriptFile);
                                }
                            }
                            sdkhttpGateway.setFilter(values);
                        } catch (Exception e) {
                            Log.log.warn(e.getMessage() + ". Skipping file: " + scriptFile.getAbsolutePath());
                        }
                    }
                }
            }
        } catch (Throwable e) {
            setActive(false);
            Log.log.warn("Couldn not load configuration for Sdkhttp gateway, check blueprint. " + e.getMessage(), e);
        }
    }

    @Override
    public void save() throws Exception {
        File dir = getFile(MainBase.main.release.serviceName + "/config/sdkhttp");
        if (!dir.exists()) {
            FileUtils.forceMkdir(dir);
        }
        if (filters != null && isActive()) {
            filters.clear();
            for (Filter filter : SdkhttpGateway.getInstance().getFilters().values()) {
                SdkhttpFilter sdkhttpFilter = (SdkhttpFilter) filter;
                String groovy = sdkhttpFilter.getScript();
                String scriptFileName;
                File scriptFile = null;
                if (StringUtils.isNotBlank(groovy)) {
                    scriptFileName = sdkhttpFilter.getId().toLowerCase() + ".groovy";
                    scriptFile = getFile(MainBase.main.release.serviceName + "/config/sdkhttp/" + scriptFileName);
                }
                Map<String, Object> entry = new HashMap<String, Object>();
                putFilterDataIntoEntry(entry, sdkhttpFilter);
                filters.add(entry);
                if (scriptFile != null) {
                    try {
                        FileUtils.writeStringToFile(scriptFile, groovy, "UTF-8");
                    } catch (Exception e) {
                        Log.log.warn(e.getMessage() + ". Skipping file: " + scriptFile.getAbsolutePath());
                    }
                }
            }
            xdoc.setListMapValue(RECEIVE_SDKHTTP_FILTER, filters);
        }
        if (nameProperties != null && isActive()) {
            nameProperties.clear();
            for (String name : SdkhttpGateway.getInstance().getNameProperties().keySet()) {
                Map<String, Object> entry = new HashMap<String, Object>();
                entry.put(NAME_KEY, name);
                nameProperties.add(entry);
                Map props = SdkhttpGateway.getInstance().getNameProperties().getNameProperties(name);
                ObjectProperties prop = new ObjectProperties(props);
                prop.save(props);
            }
        }
        saveAttributes();
    }

    private void putFilterDataIntoEntry(Map<String, Object> entry, SdkhttpFilter sdkhttpFilter) {
        entry.put(SdkhttpFilter.ID, sdkhttpFilter.getId());
        entry.put(SdkhttpFilter.ACTIVE, String.valueOf(sdkhttpFilter.isActive()));
        entry.put(SdkhttpFilter.ORDER, String.valueOf(sdkhttpFilter.getOrder()));
        entry.put(SdkhttpFilter.INTERVAL, String.valueOf(sdkhttpFilter.getInterval()));
        entry.put(SdkhttpFilter.EVENT_EVENTID, sdkhttpFilter.getEventEventId());
        entry.put(SdkhttpFilter.RUNBOOK, sdkhttpFilter.getRunbook());
        entry.put(SdkhttpFilter.SCRIPT, sdkhttpFilter.getScript());
        entry.put(SdkhttpFilter.PORT, sdkhttpFilter.getPort());
        entry.put(SdkhttpFilter.URI, sdkhttpFilter.getUri());
        entry.put(SdkhttpFilter.SSL, sdkhttpFilter.getSsl());
    }
}

