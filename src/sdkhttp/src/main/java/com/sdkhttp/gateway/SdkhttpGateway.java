package com.sdkhttp.gateway;

import java.util.HashMap;
import java.util.Map;
import java.util.List;
import java.util.concurrent.ConcurrentHashMap;
import com.resolve.gateway.BaseClusteredGateway;
import com.resolve.gateway.BaseFilter;
import com.resolve.gateway.Filter;
import com.resolve.gateway.MSdkhttp;
import com.resolve.util.Constants;
import com.resolve.util.Log;
import com.resolve.util.StringUtils;

public class SdkhttpGateway extends BaseClusteredGateway {

    // Singleton
    private static volatile SdkhttpGateway instance = null;

    private Map<Integer, HttpServer> httpServers = new HashMap<Integer, HttpServer>();
    private Map<String, HttpServer> deployedServlets = new HashMap<String, HttpServer>();
    
    // This will be set in initialize() with the proper queue name from the
    // configuration.
    private String queue = "HTTP";

    private HttpServer defaultHttpServer;

    public static SdkhttpGateway getInstance(ConfigReceiveSdkhttp config) {
        if (instance == null) {
            instance = new SdkhttpGateway(config);
        }
        return instance;
    }

    /**
     * This method is called once the initialization is done. If it gets called
     * before the initialization it will throw exception.
     *
     * @return
     */
    public static SdkhttpGateway getInstance() {
        if (instance == null) {
            throw new RuntimeException("Sdkhttp Gateway is not initialized correctly..");
        } else {
            return instance;
        }
    }

    private SdkhttpGateway(ConfigReceiveSdkhttp config) {
        super(config);
    }

    @Override
    public String getLicenseCode() {
        return "SDKHTTP";
    }

    @Override
    protected void addAdditionalValues(Map<String, Object> arg0, Map<String, String> arg1) {
    }

    @Override
    protected String getGatewayEventType() {
        return "SDKHTTP";
    }

    @Override
    protected String getMessageHandlerName() {
        return MSdkhttp.class.getSimpleName();
    }

    @Override
    protected Class<MSdkhttp> getMessageHandlerClass() {
        return MSdkhttp.class;
    }

    public String getQueueName() {
        return queue;
    }

    @Override
    public void start() {
        Log.log.debug("Starting Sdkhttp Gateway");
        super.start();
    }

    @Override
    public void run() {
        super.sendSyncRequest();
        if (isPrimary() && isActive()) {
            initSdkhttpServers();
        }
    }

    /**
     * Add customized code here to initilize the connection with 3rd party system
     * @return List<Map<String, String>>
     */
    @Override
    protected void initialize() {
        ConfigReceiveSdkhttp config = (ConfigReceiveSdkhttp) configurations;
        queue = config.getQueue().toUpperCase();
        try {
            Log.log.info("Initializing Sdkhttp Listener");
            this.gatewayConfigDir = "/config/sdkhttp/";
            defaultHttpServer = new HttpServer(config);
        } catch (Exception e) {
            Log.log.error("Failed to config Sdkhttp Gateway: " + e.getMessage(), e);
        }
    }

    /**
     * This method processes the message received from the Sdkhttp system.
     *
     * @param message
     */
    public boolean processData(String filterName, Map<String, String> params) {
        boolean result = true;
        try {
            if (StringUtils.isNotBlank(filterName) && params != null) {
                SdkhttpFilter sdkhttpFilter = (SdkhttpFilter) filters.get(filterName);
                if (sdkhttpFilter != null && sdkhttpFilter.isActive()) {
                    if (Log.log.isDebugEnabled()) {
                        Log.log.debug("Processing filter: " + sdkhttpFilter.getId());
                        Log.log.debug("Data received through Sdkhttp gateway: " + params);
                    }
                    Map<String, String> runbookParams = params;
                    if (!params.containsKey(Constants.EVENT_EVENTID) || StringUtils.isBlank(params.get(Constants.EVENT_EVENTID))) {
                        runbookParams.put(Constants.EVENT_EVENTID, sdkhttpFilter.getEventEventId());
                    }
                    addToPrimaryDataQueue(sdkhttpFilter, runbookParams);
                } else {
                    result = false;
                }
            }
        } catch (Exception e) {
            Log.log.warn("Error: " + e.getMessage(), e);
            try {
                Thread.sleep(interval);
            } catch (InterruptedException ie) {
                Log.log.warn("sleep interrupted: " + ie.getMessage(), ie);
            }
        }
        return result;
    }

    private void initSdkhttpServers() {
        try
        {
            if(!defaultHttpServer.isStarted())
            {
                defaultHttpServer.init();
                defaultHttpServer.start();
            }
            
            for(Filter filter : orderedFilters)
            {
                SdkhttpFilter httpFilter = (SdkhttpFilter) filter;
                //add the URI based servlets to the default server
                if(httpFilter.getPort() == null || httpFilter.getPort() <= 0)
                {
                    if(defaultHttpServer.isStarted())
                    {
                        defaultHttpServer.addServlet(httpFilter);
                        deployedServlets.put(filter.getId(), defaultHttpServer);
                    }
                }
                else
                {
                    HttpServer httpServer = httpServers.get(httpFilter.getPort());
                    if(httpServer == null)
                    {
                        HttpServer httpServer1 = deployedServlets.get(filter.getId());
                        if(httpServer1 != null)
                        {
                            //port changed
                            httpServer1.removeServlet(filter.getId());
                            deployedServlets.remove(filter.getId());
                        }
                        
                        httpServer = new HttpServer((ConfigReceiveSdkhttp) configurations, httpFilter.getPort(), httpFilter.getSsl());
                        httpServer.init();
                        httpServer.addServlet(httpFilter);
                        httpServer.start();
                    }
                    else
                    {
                        httpServer.addServlet(httpFilter);
                    }
                    httpServers.put(httpFilter.getPort(), httpServer);
                    deployedServlets.put(filter.getId(), httpServer);
                }
            }
        }
        catch (Exception e)
        {
            Log.log.error(e.getMessage(), e);
        }
    }
    @Override
    protected void cleanFilter(ConcurrentHashMap<String, Filter> undeployedFilters) {
        for(Filter filter : undeployedFilters.values())
        {
            SdkhttpFilter httpFilter = (SdkhttpFilter) filter;
            try
            {
                if(httpFilter.getPort() > 0)
                {
                    HttpServer httpServer = httpServers.get(httpFilter.getPort());
                    httpServer.removeServlet(httpFilter.getId());
                    //if there is no filter/servlet remaining then stop the server on the port
                    if(httpServer.getServlets().isEmpty())
                    {
                        httpServer.stop();
                        httpServers.remove(httpFilter.getPort());
                    }
                }
                else
                {
                    //undeploy the servlet from the default server.
                    defaultHttpServer.removeServlet(httpFilter.getId());
                }
            }
            catch (Exception e)
            {
                Log.log.error(e.getMessage(), e);
            }
        }
    }

    @Override
    public void clearAndSetFilters(List<Map<String, Object>> filterList) {
        Log.log.debug("Calling Gateway specific clearAndSetFilter" + (filterList != null ? " " + filterList.size() + ", [" + filterList + "]" : ""));
        super.clearAndSetFilters(filterList);
        if (isPrimary() && isActive()) {
            try {
                initSdkhttpServers();
            } catch (Exception e) {
                Log.log.error(e.getMessage(), e);
            }
        }
    }

    @Override
    public void stop() {
        Log.log.warn("Stopping Sdkhttp gateway");
        super.stop();
        for(HttpServer httpServer : httpServers.values())
        {
            try
            {
                httpServer.stop();
            }
            catch (Exception e)
            {
                Log.log.error(e.getMessage(), e);
            }
        }
        try
        {
            defaultHttpServer.stop();
        }
        catch (Exception e)
        {
            Log.log.error(e.getMessage(), e);
        }
        httpServers.clear();
    }

    @Override
    public Filter getFilter(Map<String, Object> params) {
        return new SdkhttpFilter((String) params.get(BaseFilter.ID), (String) params.get(BaseFilter.ACTIVE), (String) params.get(BaseFilter.ORDER), (String) params.get(BaseFilter.INTERVAL), (String) params.get(BaseFilter.EVENT_EVENTID), (String) params.get(BaseFilter.RUNBOOK), (String) params.get(BaseFilter.SCRIPT), (String) params.get(SdkhttpFilter.PORT), (String) params.get(SdkhttpFilter.URI), (String) params.get(SdkhttpFilter.SSL));
    }
}

