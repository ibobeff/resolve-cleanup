/******************************************************************************
 * (C) Copyright 2014
 * 
 * Resolve Systems, LLC
 * 
 * All rights reserved This software is distributed under license and may not be
 * used, copied, modified, or distributed without the express written permission
 * of Resolve Systems, LLC.
 * 
 ******************************************************************************/

package com.resolve.rssync;

import java.io.Serializable;
import java.net.InetAddress;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.List;
import java.util.Map;

import org.elasticsearch.action.DocWriteResponse;
//import org.elasticsearch.action.WriteConsistencyLevel;
import org.elasticsearch.action.delete.DeleteRequestBuilder;
import org.elasticsearch.action.delete.DeleteResponse;
import org.elasticsearch.action.index.IndexAction;
//import org.elasticsearch.action.deletebyquery.DeleteByQueryResponse;
import org.elasticsearch.action.index.IndexRequestBuilder;
import org.elasticsearch.action.index.IndexResponse;
import org.elasticsearch.action.search.SearchRequestBuilder;
import org.elasticsearch.action.search.SearchResponse;
import org.elasticsearch.action.support.ActiveShardCount;
import org.elasticsearch.action.support.WriteRequest;
//import org.elasticsearch.action.support.replication.ReplicationType;
import org.elasticsearch.client.Client;
import org.elasticsearch.client.transport.TransportClient;
//import org.elasticsearch.common.settings.ImmutableSettings;
import org.elasticsearch.common.settings.Settings;
import org.elasticsearch.common.transport.InetSocketTransportAddress;
import org.elasticsearch.common.transport.TransportAddress;
import org.elasticsearch.common.unit.TimeValue;
import org.elasticsearch.index.query.QueryBuilder;
import org.elasticsearch.rest.RestStatus;
import org.elasticsearch.search.SearchHit;
import org.elasticsearch.transport.client.PreBuiltTransportClient;

import com.resolve.search.ConfigSearch;
import com.resolve.search.SearchAdminAPI;
import com.resolve.search.SearchConstants;
import com.resolve.search.SearchException;
import com.resolve.search.SearchService;
import com.resolve.search.SearchUtils;
import com.resolve.search.SyncData;
import com.resolve.search.SyncService;
import com.resolve.search.model.SyncLog;
import com.resolve.services.vo.QueryDTO;
import com.resolve.util.DateUtils;
import com.resolve.util.Log;
import com.resolve.util.StringUtils;

/**
 * This class is the offline synchronizer. When Other Resolve components like RSControl, RSView cannot
 * synchronize data with the remote cluster then they add the record into one of the synclog indexes.
 * RSSync periodically picks them up and update them in the remote cluster. 
 */
public final class Synchronizer
{
//    private static final WriteConsistencyLevel WRITE_CONSISTENCY_LEVEL_ONE = WriteConsistencyLevel.ONE;
    
    private static volatile Synchronizer instance = null;

    private final ConfigSearch config;
    private Client remoteClient;
    private SearchService localSearchService = null;

    public static Synchronizer getInstance()
    {
        if (instance == null)
        {
            throw new RuntimeException("SyncService is not initialized correctly..");
        }
        else
        {
            return instance;
        }
    }

    public static Synchronizer getInstance(ConfigSearch config)
    {
        if (instance == null)
        {
            instance = new Synchronizer(config);
        }
        return instance;
    }

    private Synchronizer(ConfigSearch config)
    {
        this.config = config;
    }

    public void init()
    {
        Log.log.debug("Initializing sync listener.");
        this.remoteClient = createRemoteClient();
        Log.log.info("Remote client: " + remoteClient);
        localSearchService = SearchAdminAPI.getSearchService();
        Log.log.debug("sync listener initialized.");
    }
    
    public Client getRemoteClient()
    {
        return remoteClient;
    }

    /**
     * Creates a client. ES client is already thread safe and automatically
     * reconnects / discovers the cluster.
     * 
     * @return
     */
    private Client createRemoteClient()
    {
        Log.log.info("Creating Remote RSSearch client.");
        Log.log.info("Remote cluster name: " + config.getSyncCluster());
        // client will try 60 seconds before giving up
        Settings settings = Settings.builder().put("cluster.name", config.getSyncCluster()).put("client.transport.ping_timeout", TimeValue.timeValueSeconds(60)).build();

        // a Map with value as serverip:port
        Map<Integer, String> syncServers = config.getSyncServers();
        String syncServerPort = ((Integer) config.getSyncServerport()).toString();
        TransportAddress[] transportAddresses = new InetSocketTransportAddress[syncServers.size()];
        int i = 0;
        try 
        {
	        for (Integer key : syncServers.keySet())
	        {
	            String syncServer = syncServers.get(key);
	            String[] serverInfo = new String[] { syncServer, syncServerPort };
	            String syncServerIp = serverInfo[0];
	            Integer syncPort = Integer.valueOf(serverInfo[1]);
	            TransportAddress transportAddress = new InetSocketTransportAddress(InetAddress.getByName(syncServerIp), syncPort);
	            Log.log.info("Sync Server: " + syncServerIp + ":" + syncPort);
	            transportAddresses[i++] = transportAddress;
	        }
        }
        catch (Exception ex)
        {
        	Log.log.error(ex,ex);
        	throw new RuntimeException(ex); 
        }
        
        return new PreBuiltTransportClient(settings).addTransportAddresses(transportAddresses);
//        return new TransportClient(settings).addTransportAddresses(transportAddresses);
    }

    public void synchronize(String syncLogIndex, List<SyncLog> syncLogs) throws SearchException
    {
        for(SyncLog syncLog : syncLogs)
        {
            int count = 0;
            long t = 1000;
            int retry = 8; // number of times to retry ES operation before giving up
            while (count < retry)
            {
                try
                {
                    doSyncOperation(syncLogIndex, syncLog);
                    break;
                }
                catch (org.elasticsearch.common.util.concurrent.EsRejectedExecutionException ex)
                {
                    if (count>retry)
                    {
                        Log.log.error("Failed to complete a sync operation in number of retry: " + retry + ", syncLog: " + syncLog, ex);
                        throw ex; 
                    }
                    
                    Log.log.info("Sync operation failed and will retry. syncLog: " + syncLog, ex);
                    Log.log.info("Retry sync operation count = " + count + ", interval = " + t);
                    
                    try
                    {
                        Thread.sleep(t);
                        t*= 2;   // exponential backoff
                    }
                    catch (InterruptedException e)
                    {
                        throw new RuntimeException(e);
                    }
                    ++count;
                }
            }
        }
    }
    
    
    private void doSyncOperation(String syncLogIndex, SyncLog syncLog) throws SearchException
    {
        //one of INDEX, UPDATE_BY_SCRIPT, DELETE, DELETE_BY_QUERY
        if("INDEX".equalsIgnoreCase(syncLog.getTransactionType()) || "UPDATE_BY_SCRIPT".equalsIgnoreCase(syncLog.getTransactionType()))
        {
            indexDocument(syncLogIndex, syncLog);
        }
        else if("DELETE".equalsIgnoreCase(syncLog.getTransactionType()) || "DELETE_BY_QUERY".equalsIgnoreCase(syncLog.getTransactionType()))
        {
            deleteDocument(syncLogIndex, syncLog);
        }
    }
    
    
    /**
     * Used by RSSync to index a document.
     * 
     * @param syncLog
     * @throws SearchException
     */
    private void indexDocument(String syncLogIndex, SyncLog syncLog) throws SearchException
    {
        IndexRequestBuilder indexRequestBuilder = new IndexRequestBuilder(remoteClient, IndexAction.INSTANCE, syncLog.getIndexName());
        indexRequestBuilder.setType(syncLog.getDocumentType());
        indexRequestBuilder.setWaitForActiveShards(ActiveShardCount.ONE); //.setConsistencyLevel(WRITE_CONSISTENCY_LEVEL_ONE);
        indexRequestBuilder.setId(syncLog.getSysId());
        // get the latest object from the local ES cluster
        Map<String, Object> source = localSearchService.getById(syncLog.getIndexName(), syncLog.getDocumentType(), syncLog.getSysId(), syncLog.getParentId(), syncLog.getRoutingKey());
        boolean removeLog = false;
        if (source != null)
        {
            try
            {
                indexRequestBuilder.setSource(source);
                IndexResponse response = indexRequestBuilder.setWaitForActiveShards(ActiveShardCount.ONE).execute().actionGet();

                Log.log.debug("Index document index name:" + syncLog.getIndexName() + ", response id: " + response.getId() + ", IndexResponse response: " + response);
                if (response != null && response.getId() != null)
                {
                    removeLog = true;
                }
            }
            catch(Exception e)
            {
                Log.log.warn(e.getMessage(), e);
                throw e;
            }
        }
        else
        {
            removeLog = true;
            if(Log.log.isTraceEnabled())
            {
                Log.log.trace("Actual document not found in Local ES, might have been deleted already: " + syncLog.toString());
            }
        }

        if (removeLog)
        {
            // remove the record from synclog index
            localSearchService.deleteById(syncLogIndex, SearchConstants.DOCUMENT_TYPE_SYNC_LOG, syncLog.getSysId());
            if(Log.log.isTraceEnabled())
            {
                Log.log.trace("Successfully deleted synclog data : " + syncLog.toString());
            }
        }
    }

    /**
     * Deletes data by its sysId.
     * 
     * @param indexName
     * @param documentType
     * @param id
     * @throws SearchException
     */
    public void deleteById(String indexName, String documentType, String id) throws SearchException
    {
        deleteById(indexName, documentType, id, null);
    }

    /**
     * Deletes data by its sysId and routing key.
     * 
     * @param indexName
     * @param documentType
     * @param id
     * @param routingKey
     * @throws SearchException
     */
    void deleteById(String indexName, String documentType, String id, String routingKey) throws SearchException
    {
        Client client = getRemoteClient();
        DeleteRequestBuilder deleteRequestBuilder = client.prepareDelete(indexName, documentType, id);
        if (StringUtils.isNotBlank(routingKey))
        {
            deleteRequestBuilder.setRouting(routingKey);
        }

        // set the consistency level to QUORUM, unlike some other system quorum
        // in ES means replicas/2 + 1
        DeleteResponse response = deleteRequestBuilder.setRefreshPolicy(WriteRequest.RefreshPolicy.IMMEDIATE).setWaitForActiveShards(ActiveShardCount.ALL).execute().actionGet();
        if (response.getResult().equals(DocWriteResponse.Result.DELETED))
        {
            Log.log.trace("Index: " + indexName + ", Document: " + documentType + ", Delete successful: " + id);
            // send to synchronization service
            String syncLogIndexName = SearchUtils.getSyncLogIndexName(response.getIndex(), response.getType());
            if (config.isSync() && !config.getSyncExcludeSet().contains(response.getIndex()) && !"synclog".equals(response.getType()) && StringUtils.isNotEmpty(syncLogIndexName))
            {
                SyncData<Serializable> syncData = new SyncData<Serializable>(syncLogIndexName, response.getIndex(), response.getType(), SearchUtils.SYNC_OPERATION.DELETE, null, routingKey, response.getId(), DateUtils.getCurrentTimeInMillis(), null, "system");
                SyncService.getInstance().enqueue(syncData);
            }
        }
        else
        {
            Log.log.trace("Index: " + indexName + ", Document: " + documentType + ", Id not found for deletion: " + id);
        }
    }
    
    
    void deleteByQueryBuilder(String indexName, String documentType, QueryBuilder query) throws SearchException
    {
    	Collection<String> ids = searchIDsByQuery(new String[] {indexName}, new String[] {documentType}, null, query);
    	
    	for (String id : ids)
    	{
    		deleteById(indexName, documentType, id);
    	}

    }
    
    
    public Collection<String> searchIDsByQuery(final String[] indexNames, final String[] documentTypes, final QueryDTO query, final QueryBuilder queryBuilder) throws SearchException
    {
        List<String> result = new ArrayList<String>();

        Client client = getRemoteClient();

        QueryDTO tmpQuery = query;

        if (tmpQuery == null)
        {
            tmpQuery = new QueryDTO();
        }

        int limit = tmpQuery.getLimit();
        int start = tmpQuery.getStart() != -1 ? tmpQuery.getStart() : 0;

        SearchRequestBuilder searchRequestBuilder = client.prepareSearch();
        searchRequestBuilder.setIndices(indexNames).setTypes(documentTypes);

        // comma separated list of fields
        String selectFields = StringUtils.isBlank(query.getSelectColumns()) ? "*" : query.getSelectColumns();
        String excludeFields = StringUtils.isBlank(query.getExcludeColumns()) ? "" : query.getExcludeColumns();
        if (StringUtils.isNotBlank(query.getExcludeColumns())  || StringUtils.isNotBlank(query.getSelectColumns()))
        {
            String[] selects = Arrays.asList(selectFields.split("\\s*,\\s*")).toArray(new String [] {"1"});
            String[] excludes = Arrays.asList(excludeFields.split("\\s*,\\s*")).toArray(new String [] {"1"});
            searchRequestBuilder.setFetchSource(selects, excludes);
        }

        // add the sorting information
//        addSortOrder(tmpQuery, searchRequestBuilder);

        searchRequestBuilder.setFrom(start);
        // this adds some overhead that's why it's in trace
        if (Log.log.isTraceEnabled())
        {
            searchRequestBuilder.setExplain(true);
        }

        if (queryBuilder != null)
        {
            searchRequestBuilder.setQuery(queryBuilder);
        }
        
        if (Log.log.isTraceEnabled())
        {
            Log.log.trace(searchRequestBuilder.toString());
        }

        SearchResponse resp = searchRequestBuilder.execute().actionGet();
        if (Log.log.isTraceEnabled())
        {
            Log.log.trace(resp.toString());
        }

        long total = resp.getHits().getTotalHits();
        int count = 0;
        while (true)
        {
            for (SearchHit hit : resp.getHits())
            {
                try
                {
                    String src = hit.getSourceAsString();
                    String docId = hit.getId();
                    result.add(docId);
                }
                catch (Exception e)
                {
                    Log.log.error(e.getMessage(), e);
                    throw new SearchException(e.getMessage(), e);
                }
                    count++;
            }
                
            //assumes default limit of 10, if ES changes this this function will need to be updated
            if (count < 10 || result.size() >= total)
            {
                break;
            }
            else
            {
                count = 0;
                start = start + 10;
                searchRequestBuilder.setFrom(start);
                resp = searchRequestBuilder.execute().actionGet();
            }
        }
        
        return result;
    }
    
    
    /**
     * This will delete data in the remote cluster, generally used by RSSync
     * during log replay.
     * 
     * @param syncLog
     */
    private void deleteDocument(String syncLogIndex, SyncLog syncLog) throws SearchException
    {
        boolean removeLog = false;
        
        if(StringUtils.isNotBlank(syncLog.getQuery()))
        {
            QueryBuilder queryBuilder = SearchUtils.fromSource(syncLog.getQuery());
            try 
            {
            	deleteByQueryBuilder(syncLog.getIndexName(), syncLog.getDocumentType(), queryBuilder);
            }
            catch(Exception e)
            {
                Log.log.warn(e.getMessage(), e);
            }
            Log.log.debug("Delete index name:" + syncLog.getIndexName() + ", delete query: " + queryBuilder);
//            try
//            {
//                DeleteByQueryResponse response = remoteClient.prepareDeleteByQuery(syncLog.getIndexName()).setTypes(syncLog.getDocumentType()).setReplicationType(ReplicationType.ASYNC).setQuery(queryBuilder).execute().actionGet();
//                Log.log.debug("Delete index name:" + syncLog.getIndexName() + ", DeleteByQueryResponse status: " + response.status());
//                if (response != null && response.status().equals(RestStatus.OK))
//                {
//                    removeLog = true;
//                }
//            }
//            catch(Exception e)
//            {
//                Log.log.warn(e.getMessage(), e);
//            }
        }
        else
        {
            DeleteRequestBuilder deleteRequestBuilder = remoteClient.prepareDelete(syncLog.getIndexName(), syncLog.getDocumentType(), syncLog.getSysId());
            if (StringUtils.isNotBlank(syncLog.getRoutingKey()))
            {
                deleteRequestBuilder.setRouting(syncLog.getRoutingKey());
            }
            
            try
            {
                DeleteResponse response = deleteRequestBuilder.setWaitForActiveShards(ActiveShardCount.ONE).execute().actionGet();
                if (response != null && response.getResult().equals(DocWriteResponse.Result.DELETED)) //.isFound())
                {
                    removeLog = true;
                }
            }
            catch(Exception e)
            {
                Log.log.warn(e.getMessage(), e);
            }
        }
        
        if (removeLog)
        {
            // remove the record from synclog index
            localSearchService.deleteById(syncLogIndex, SearchConstants.DOCUMENT_TYPE_SYNC_LOG, syncLog.getSysId());
            Log.log.debug("Successfully deleted synclog data : " + syncLog.toString());
        }
    }
}
