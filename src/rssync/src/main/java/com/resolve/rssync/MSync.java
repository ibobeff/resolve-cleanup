/******************************************************************************
* (C) Copyright 2014
*
* Resolve Systems, LLC
*
* All rights reserved
* This software is distributed under license and may not be used, copied,
* modified, or distributed without the express written permission of
* Resolve Systems, LLC.
*
******************************************************************************/

package com.resolve.rssync;

import java.util.HashMap;
import java.util.Map;

import com.resolve.search.SearchAdminAPI;
import com.resolve.util.Log;

public class MSync
{
    /**
     * This method retuns the number of items yet to be synchronized with the remote ES cluster.
     * 
     * @param params
     */
    public static Map<String, String> logCount(Map<String, Object> params)
    {
        Map<String, String> result = new HashMap<String, String>();

        //following synclog indexes are available at the moment
        //synclog_misc,synclog_socialpost,synclog_systempost,synclog_tag,synclog_worksheet,synclog_processrequest,synclog_taskresult
        result.put("worksheet", ""+SearchAdminAPI.getTotalCount("synclog_worksheet", "synclog"));
        result.put("processrequest", ""+SearchAdminAPI.getTotalCount("synclog_processrequest", "synclog"));
        result.put("taskresult", ""+SearchAdminAPI.getTotalCount("synclog_taskresult", "synclog"));
        result.put("tag", ""+SearchAdminAPI.getTotalCount("synclog_tag", "synclog"));
        result.put("socialpost+comments", ""+SearchAdminAPI.getTotalCount("synclog_socialpost", "synclog"));
        result.put("systempost+comments", ""+SearchAdminAPI.getTotalCount("synclog_systempost", "synclog"));
        result.put("misc", ""+SearchAdminAPI.getTotalCount("synclog_misc", "synclog"));
        // SIR data
        result.put("pbnotes", ""+SearchAdminAPI.getTotalCount("synclog_pbnotes", "synclog"));
        result.put("pbauditlog", ""+SearchAdminAPI.getTotalCount("synclog_pbauditlog", "synclog"));
        result.put("pbartifacts", ""+SearchAdminAPI.getTotalCount("synclog_pbartifacts", "synclog"));
        result.put("pbattachment", ""+SearchAdminAPI.getTotalCount("synclog_pbattachment", "synclog"));
        result.put("pbactivity", ""+SearchAdminAPI.getTotalCount("synclog_pbactivity", "synclog"));
        
        Log.log.debug("Items yet to be synchronized with remote ES cluster...");
        for(String key : result.keySet())
        {
            Log.log.debug("\t index : " + key + " : " + result.get(key));
        }
        return result;
    }
}
