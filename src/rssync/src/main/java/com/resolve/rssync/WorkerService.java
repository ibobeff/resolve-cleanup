/******************************************************************************
 * (C) Copyright 2014
 * 
 * Resolve Systems, LLC
 * 
 * All rights reserved This software is distributed under license and may not be
 * used, copied, modified, or distributed without the express written permission
 * of Resolve Systems, LLC.
 * 
 ******************************************************************************/
package com.resolve.rssync;

import java.util.LinkedList;
import java.util.List;
import java.util.Set;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

import com.resolve.search.ConfigSearch;
import com.resolve.util.Log;
import com.resolve.util.StringUtils;

public class WorkerService
{
    private final ConfigGeneral configGeneral;
    private final ConfigSearch configSearch;
    private Set<String> syncLogs;
    
    ExecutorService executor = null;
    private List<WorkerThread> workers = new LinkedList<WorkerThread>();
    
    WorkerService(final ConfigGeneral configGeneral, final ConfigSearch configSearch)
    {
        this.configGeneral = configGeneral;
        this.configSearch = configSearch;
        this.syncLogs = StringUtils.stringToSet(configGeneral.getSynclogs(), ",");
    }
    
    void start()
    {
        executor = Executors.newFixedThreadPool(syncLogs.size());
        for(String syncLog : syncLogs)
        {
            WorkerThread worker = new WorkerThread(configGeneral, configSearch, syncLog);
            executor.execute(worker);
            workers.add(worker);
        }
    }
    
    void stop()
    {
        if(executor != null)
        {
            for(WorkerThread worker : workers)
            {
                Log.log.debug("Stopping worker for " + worker.toString());
                worker.stop();
            }
            executor.shutdown();
            while (!executor.isTerminated())
            {
            }
            Log.log.info("Finished all worker threads.");
        }
    }
}
