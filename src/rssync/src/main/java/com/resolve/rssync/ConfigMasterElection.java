/******************************************************************************
* (C) Copyright 2014
*
* Resolve Systems, LLC
*
* All rights reserved
* This software is distributed under license and may not be used, copied,
* modified, or distributed without the express written permission of
* Resolve Systems, LLC.
*
******************************************************************************/

package com.resolve.rssync;

import com.resolve.util.ConfigMap;
import com.resolve.util.XDoc;


public class ConfigMasterElection extends ConfigMap
{
    private static final long serialVersionUID = 1188572764580854352L;
	int interval = 30;
    
    public ConfigMasterElection(XDoc config) throws Exception
    {
        super(config);
        
        define("interval", INTEGER, "./MASTERELECTION/@INTERVAL");
    } // ConfigRegistration
    
    public void load()
    {
        loadAttributes();
    } // load

    public void save()
    {
        saveAttributes();
    } // save

    public int getInterval()
    {
        return interval;
    }

    public void setElection(int t)
    {
        this.interval = t;
    }

    
} // ConfigMasterElection
