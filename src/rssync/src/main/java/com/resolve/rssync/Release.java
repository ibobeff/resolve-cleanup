
/******************************************************************************
* (C) Copyright 2016
*
* Resolve Systems, LLC
*
* All rights reserved
* This software is distributed under license and may not be used, copied,
* modified, or distributed without the express written permission of
* Resolve Systems, LLC.
*
******************************************************************************/

package com.resolve.rssync;

public class Release extends com.resolve.rsbase.Release
{
    public Release(String service)
    {
        super();

        name = "RSSync";
        type = "RSSYNC";
        if (service == null || service.equals(""))
        {
            service = "rssync";
        }
        serviceName = service;
        version = "Corona";
        year = "2016";
	copyright = "(C) Copyright 2016 Resolve Systems, LLC";

    } // Release

} // Release
        