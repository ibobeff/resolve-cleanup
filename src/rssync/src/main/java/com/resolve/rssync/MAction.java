/******************************************************************************
* (C) Copyright 2014
*
* Resolve Systems, LLC
*
* All rights reserved
* This software is distributed under license and may not be used, copied,
* modified, or distributed without the express written permission of
* Resolve Systems, LLC.
*
******************************************************************************/

package com.resolve.rssync;

import java.util.Map;

import com.resolve.rsbase.MainBase;
import com.resolve.util.Log;

public class MAction
{
    public void activateCluster(Map<String, String> params)
    {
        Log.log.info("***** Cluster activation message received *****");
        Main main = (Main) MainBase.main;
        main.activateCluster(params);
    }

    public void deactivateCluster(Map<String, String> params)
    {
        Log.log.info("***** Cluster deactivation message received *****");
        Main main = (Main) MainBase.main;
        main.deactivateCluster(params);
    }
} // MAction
