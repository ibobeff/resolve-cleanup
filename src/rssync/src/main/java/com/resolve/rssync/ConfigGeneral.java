/******************************************************************************
* (C) Copyright 2014
*
* Resolve Systems, LLC
*
* All rights reserved
* This software is distributed under license and may not be used, copied,
* modified, or distributed without the express written permission of
* Resolve Systems, LLC.
*
******************************************************************************/

package com.resolve.rssync;

import com.resolve.util.XDoc;

public class ConfigGeneral extends com.resolve.rsbase.ConfigGeneral
{
    private static final long serialVersionUID = 1782253565622045816L;
	String synclogs = "synclog_misc,synclog_socialpost,synclog_systempost,synclog_tag,synclog_worksheet,synclog_processrequest,synclog_taskresult,"
			+ "synclog_pbnotes,synclog_pbauditlog,synclog_pbartifacts,synclog_pbattachment,synclog_pbactivity";
    long syncInterval = 10000L; //pause between sync
    int syncSize = 1000; //record size to retrieve from ES and push to remote
    public int scheduledPool;
    
    public ConfigGeneral(XDoc config) throws Exception
    {
        super(config);
        define("synclogs", STRING, "./GENERAL/@SYNCLOGS");
        define("syncInterval", LONG, "./GENERAL/@SYNCINTERVAL");
        define("syncSize", INTEGER, "./GENERAL/@SYNCSIZE");
        define("scheduledPool", INTEGER, "./GENERAL/@SCHEDULEDPOOL");
    } // ConfigGeneral

    public void load()
    {
        super.load();
    } // load

    public void save()
    {
        super.save();
    } // save

    public String getSynclogs()
    {
        return synclogs;
    }

    public void setSynclogs(String synclogs)
    {
        this.synclogs = synclogs;
    }

    public long getSyncInterval()
    {
        return syncInterval;
    }

    public void setSyncInterval(long syncInterval)
    {
        this.syncInterval = syncInterval;
    }

    public int getSyncSize()
    {
        return syncSize;
    }

    public void setSyncSize(int syncSize)
    {
        this.syncSize = syncSize;
    }
    
    public int getScheduledPool() {
        return scheduledPool;
    }

    public void setScheduledPool(int scheduledPool) {
        this.scheduledPool = scheduledPool;
    }

} // ConfigGeneral
