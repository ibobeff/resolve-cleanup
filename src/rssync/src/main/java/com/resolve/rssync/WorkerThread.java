/******************************************************************************
 * (C) Copyright 2014
 * 
 * Resolve Systems, LLC
 * 
 * All rights reserved This software is distributed under license and may not be
 * used, copied, modified, or distributed without the express written permission
 * of Resolve Systems, LLC.
 * 
 ******************************************************************************/
package com.resolve.rssync;

import java.util.List;

import com.resolve.search.APIFactory;
import com.resolve.search.ConfigSearch;
import com.resolve.search.SearchAPI;
import com.resolve.search.SearchException;
import com.resolve.search.model.SyncLog;
import com.resolve.services.vo.QueryDTO;
import com.resolve.services.vo.QuerySort;
import com.resolve.util.Log;

public class WorkerThread implements Runnable
{
    private String syncLog;
    private long syncInterval;
    private int syncSize;
    private SearchAPI<SyncLog> syncLogSearchAPI;
    private QueryDTO queryDTO;
    
    private final Synchronizer syncService;

    private boolean running = false;
    
    public WorkerThread(final ConfigGeneral configGeneral, final ConfigSearch configSearch, final String syncLog)
    {
        this.syncLog = syncLog;
        this.syncInterval = configGeneral.getSyncInterval();
        this.syncSize = configGeneral.getSyncSize();
    
        queryDTO = new QueryDTO(0, this.syncSize);
        queryDTO.addSortItem(new QuerySort("timestamp", QuerySort.SortOrder.ASC));
        queryDTO.setModelName(syncLog);

        syncLogSearchAPI = APIFactory.getSyncLogAPI(syncLog);
        syncService = Synchronizer.getInstance(configSearch);
    }

    @Override
    public void run()
    {
        syncService.init();
        Log.log.info("Worker started for " + syncLog);
        running = true;
        process();
    }

    public void stop() 
    {
        running = false;
    }
    
    private void process()
    {
        try
        {
            long waitInterval = syncInterval;
            while(running)
            {
                if (ElectMaster.isMaster())
                {
                    try
                    {
                        List<SyncLog> syncLogs = syncLogSearchAPI.searchByQuery(queryDTO, "system").getRecords();
                        if(Log.log.isDebugEnabled())
                        {
                            Log.log.debug("queryDTO: " + queryDTO);                    
                            Log.log.debug("Total " + syncLogs.size() + " records retrieved from " + syncLog);
                        }
                        syncService.synchronize(syncLog, syncLogs);
                        waitInterval = syncInterval;
                    }
                    catch(org.elasticsearch.client.transport.NoNodeAvailableException nnaExp)
                    {
                        waitInterval*=2;
                        if (waitInterval>60000)  
                        {
                            waitInterval = 60000; // cap at 1 min
                        }
                        Log.log.warn("Error: NoNodeAvailableException. Change sync interval to " + waitInterval);
                    }
                    catch (SearchException e)
                    {
                        Log.log.warn(e.getMessage(), e);
                    }
                }
                
                Thread.sleep(waitInterval);
            }
            Log.log.debug("Worker for " + syncLog + " stopped.");
        }
        catch (InterruptedException e)
        {
            Log.log.warn(e.getMessage(), e);
        }
        catch (Throwable th)
        {
            Log.log.error("Unexpected exception causes sync thread to die!", th);
        }
    }

    @Override
    public String toString()
    {
        return this.syncLog;
    }
}
