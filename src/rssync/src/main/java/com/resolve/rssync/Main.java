/******************************************************************************
* (C) Copyright 2014
*
* Resolve Systems, LLC
*
* All rights reserved
* This software is distributed under license and may not be used, copied,
* modified, or distributed without the express written permission of
* Resolve Systems, LLC.
*
******************************************************************************/

package com.resolve.rssync;

import java.io.File;
import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.TimeUnit;

import com.resolve.esb.MListener;
import com.resolve.rsbase.ConfigId;
import com.resolve.rsbase.MainBase;
import com.resolve.rsbase.ConfigSQL;
import com.resolve.search.ConfigSearch;
import com.resolve.search.SearchAdminAPI;
import com.resolve.services.ServiceHibernate;
import com.resolve.sql.SQL;
import com.resolve.sql.SQLDriverInterface;
import com.resolve.util.Constants;
import com.resolve.util.Log;
import com.resolve.util.XDoc;
import com.resolve.thread.ScheduledExecutor;

public class Main extends MainBase
{
    private ConfigSearch configSearch;
    public ConfigSQL configSQL;
    public ConfigGeneral configGeneral;
    public ConfigESB configESB;
    ConfigMasterElection configMasterElection;
    
    WorkerService workerService = null;
    
    public static void main(final String[] args)
    {
        String serviceName = null;

        try
        {
            if (args.length > 0)
            {
                serviceName = args[0];
            }

            main = new Main();
            main.init(serviceName);
        }
        catch (Throwable t)
        {
            t.printStackTrace();
            Log.log.error("Failed to start RSSYNC", t);
            System.exit(0);
        }
    }

    @Override
    public void init(String serviceName) throws Exception
    {
        // init release information
        initRelease(new Release(serviceName));

        // init logging
        initLog();
        
        // init enc
        initENC();

        // init signal handlers
        initShutdownHandler();

        // init configuration
        initConfig();

        // init executor
        initThreadPools();
        
        // init sql persistence
        initSQL();

        // init save config
        initSaveConfig();

        // init message bus
        initESB();

        // start message bus only after initialization is finished.
        startESB();
        
        // init ElasticSearch to local cluster
        initElasticSearch();
        
        // start the worker service
        workerService = new WorkerService((ConfigGeneral) configGeneral, configSearch);
        workerService.start();
        
        // start
        initComplete();
        startDefaultTasks();

        //get cluster based properties from RSControl
        initClusterProperties();

        Log.log.info("RSSYNC started...");
    }

    @Override
    protected void loadConfig(XDoc configDoc) throws Exception
    {
//    	super.initConfigFile();       
    	super.loadConfig(configDoc);

        if (configGeneral == null)
        {
            configGeneral = new ConfigGeneral(configDoc);
            configGeneral.load();
            setClusterModeProperty(configGeneral.getClusterName() + "." + Constants.CLUSTER_MODE);
        }
        if (configId == null)
        {
            configId = new ConfigId(configDoc);
            configId.load();
        }

        if(configESB == null)
        {
            configESB = new ConfigESB(configDoc);
            configESB.load();
        }

        if(configSearch == null)
        {
            configSearch = new ConfigSearch(configDoc);
            configSearch.load();
        }
        
        configSQL = new ConfigSQL(configDoc);
        configSQL.load();
        
        if(configMasterElection==null)
        {
            configMasterElection = new ConfigMasterElection(configDoc);
            configMasterElection.load();
        }
        
    } // loadConfig
    
    @Override
    protected void startESB() throws Exception
    {
        Log.log.info("Starting Message Bus");
        mServer.setDefaultClassPackage(defaultClassPackage);
        mServer.start();

        // add default publications
        mServer.initDefaultPublications();
        
        // add default subscription
        mServer.subscribePublication(Constants.ESB_NAME_RSSYNCS);

        Log.log.info("  Initializing ESB Listener for the queue: " + Constants.ESB_NAME_RSSYNC);
        MListener mListener = mServer.createListener(Constants.ESB_NAME_RSSYNC, "com.resolve.rssync");
        mListener.init(false);

        Log.log.info("  Initializing ESB Listener for the queue: " + MainBase.main.configId.getGuid());
        MListener mListener1 = mServer.createListener(MainBase.main.configId.getGuid(), "com.resolve.rssync");
        mListener1.init(false);
    }

    /**
     * Initialize search (ElasticSearch) service
     */
    private void initElasticSearch()
    {
        //the search is always on in RSCONTROL
        Log.log.info("Initializing Search.");
        //add system properties that the RSSearch needs, this removes the Hibernate 
        //dependency from RSRemote when we eventually start accessing RSSearch from RSRemote.
        Map<String, String> properties = new HashMap<String, String>();
        //properties.put("search.exclude.namespace", PropertiesUtil.getPropertyString("search.exclude.namespace"));
        //properties.put("search.exclude.document.pattern", PropertiesUtil.getPropertyString("search.exclude.document.pattern"));
        //properties.put("search.attachment.limit", PropertiesUtil.getPropertyString("search.attachment.limit"));
        //properties.put("dashboard.namespace.exclude", PropertiesUtil.getPropertyString("dashboard.namespace.exclude"));

        configSearch.setProperties(properties);
        int maxRetry = 30; // up to 10 minuts
        int retry = 0;
        while(retry<maxRetry)
        {
            try
            {
                SearchAdminAPI.init(configSearch);
                break;
            }
            catch(Throwable ex)
            {
                Log.log.error(ex,ex);
                retry++;
                try
                {
                    Thread.sleep(20000); // 20 seconds
                }
                catch (Exception e)
                {
                    throw new RuntimeException(e);
                }
            }
        }
        Log.log.info("Search Initialized Successfully.");
    }
    
    protected void initENC() throws Exception
    {
        // load configuration state and initialization overrides
        Log.log.info("Loading configuration settings for enc");
        super.initENC();
        loadENCConfig();
    } // initENC

    protected void loadENCConfig() throws Exception
    {
        ConfigENC.load();
    } // loadENCConfig

    @Override
    protected void exit(String[] argv)
    {
        System.exit(0);
    }
    
    @Override
    protected void terminate()
    {
        // shutdown
        isShutdown = true;

        Log.log.warn("Terminating " + release.name.toUpperCase());
        workerService.stop();
        
        super.exitSaveConfig();
        Log.log.warn("Terminated " + release.name.toUpperCase());
    } // terminate
    
    public void activateCluster(Map<String, String> params)
    {
        //prevent reactivating if it's already active
        if(!isClusterModeActive())
        {
            activateCluster();
            try
            {
                //workerService.start();
            }
            catch (Exception e)
            {
                Log.log.error(e.getMessage(), e);
            }
        }
    }

    public void deactivateCluster(Map<String, String> params)
    {
        //prevent deactivating if it's already inactive
        if(isClusterModeActive())
        {
            deactivateCluster();
            try
            {
                //workerService.stop();
            }
            catch (Exception e)
            {
                Log.log.error(e.getMessage(), e);
            }
        }
    }

    void startDefaultTasks()
    {
        // electMaster
        ScheduledExecutor.getInstance().executeRepeat("ELECTMASTER", ElectMaster.class, "electMaster", ((ConfigMasterElection) ((com.resolve.rssync.Main)Main.main).configMasterElection).getInterval(), TimeUnit.SECONDS);
    } // startDefaultTasks
    
    protected void saveConfig() throws Exception
    {
        configMasterElection.save();
        configSQL.save();
        super.saveConfig();
    } // saveConfig

    protected void initSQL()
    {
        Log.log.info("Starting SQL");

        // get db driver
        SQLDriverInterface driver = SQL.getDriver(configSQL.getDbtype(), configSQL.getDbname(), configSQL.getHost(), configSQL.getUsername(), configSQL.getP_assword(), configSQL.getUrl(), configSQL.isDebug());

        if (driver != null)
        {
            SQL.init(driver);
            SQL.start();
        }
    } // initSQL
    
}
