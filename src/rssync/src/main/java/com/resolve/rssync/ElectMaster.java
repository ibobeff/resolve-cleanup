/******************************************************************************
* (C) Copyright 2014
*
* Resolve Systems, LLC
*
* All rights reserved
* This software is distributed under license and may not be used, copied,
* modified, or distributed without the express written permission of
* Resolve Systems, LLC.
*
******************************************************************************/

package com.resolve.rssync;

import com.resolve.rsbase.MainBase;
import com.resolve.services.ServiceHibernate;
import com.resolve.util.Log;

public class ElectMaster
{
    private static boolean isMaster=false;
    
    public static boolean isMaster()
    {
        return isMaster;
    }
    
    public static void setMaster(boolean b)
    {
        isMaster = b;
    }
    
    public void electMaster()
    {
        int electionInterval = ((ConfigMasterElection) ((com.resolve.rssync.Main)Main.main).configMasterElection).getInterval();
        String guid = MainBase.main.configId.guid;
        
        isMaster = ServiceHibernate.genericElectMaster(electionInterval, guid, "RSSYNC");
        Log.log.info("RSSync: after election, isMaster = " + isMaster);
    } // electMaster

} // ElectMaster

