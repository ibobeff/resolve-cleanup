/**
 * Resolve LLC @ copyright 2017
 */
package com.resolve.gateway.push.push_template;

import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import java.util.List;
import java.util.concurrent.ConcurrentHashMap;

import com.resolve.gateway.Filter;
import com.resolve.gateway.pull.template.*;
import com.resolve.gateway.resolvegateway.push.ConfigReceivePushGateway;
import com.resolve.gateway.resolvegateway.push.HttpServer;
import com.resolve.gateway.resolvegateway.push.PushGateway;
import com.resolve.gateway.resolvegateway.push.PushGatewayFilter;
import com.resolve.util.Log;

/**
 * The Class TemplateGateway.
 * <p>
 *   <br> This class is responsible for creating/initializing the gateway on resolve. If the gateway's </br>
  *   <br> properties are properly configured in the Resolve's blueprint.properties file, then this class</br>
     *  <br> gets called from the main class of rsremote when rsremote starts up. Primary job of this  </br>
     *  <br> Class is to make the gateway functional and enable communication between Resolve and </br>
     *  <br> rsremote through rsmq. The class also has the standard start stop methods to enable HA</br>
     *  <br> between primary rsremote and secondary instance</br>
 *  </p>
 *  <p>
 *      <br> Being the push gateway filter this class is also responsible for adding and exposing http ports </br>
 *      <br> and exposing them for external HTTP push requests </br>
 *   </p>
 */
public class TemplateGateway extends PushGateway
{
    private static volatile TemplateGateway instance = null;

   /** This is the default Jetty server instance to be held in the specific gateway object
    Replace the HttpServer class to any other server class if it�s not HTTP Server. */
    private static volatile HttpServer defaultHttpServer;

    /**User can configure multiple Jetty server instances to listen to different ports and cached them in memory
     */
    private Map<Integer, HttpServer> httpServers = new HashMap<Integer, HttpServer>();

    /** Each Jetty server instance may have multiple servlets running, each of which is correspondent to a 
     * different filter name
     */
    private Map<String, HttpServer> deployedServlets = new HashMap<String, HttpServer>();

    /** Singleton constructor to take the blueprint configuration and initialize the deployed filters for this
    *specific gateway
     Do not modify the method's access type*/
    private TemplateGateway(ConfigReceivePushGateway config) {

        super(config);

        String gatewayName = this.getClass().getSimpleName();
        /** Get the */
        int index = gatewayName.indexOf("Gateway");
        if(index != -1)
            name = gatewayName.substring(0, index);

        /** Get all the push filters available from the super class */
        Map<String, PushGatewayFilter> pushFilters = getPushFilters();

        // If any of the push filters is belonged to this specific gateway, cache it in the memory of this gateway object
        for(Iterator<String> iterator=pushFilters.keySet().iterator(); iterator.hasNext();) {
            String filterName = iterator.next();
            PushGatewayFilter filter = pushFilters.get(filterName);

            if(name.indexOf(((PushGatewayFilter)filter).getGatewayName()) != -1) {
                if(filters.get(filterName) == null) {
                    filters.put(filterName, pushFilters.get(filterName));

                    if(filter.isDeployed()) {
                        orderedFilters.add(filter);
                        orderedFiltersMapById.put(filter.getId(), filter);
                    }
                }
            }
        }

        ConfigReceivePushGateway.getGateways().put(name, this);
    }

    /**
     * Public method to get an instance from this Singleton class
     * Do not modify
     * @param config the config
     * @return single instance of TemplateGateway
     */
    public static TemplateGateway getInstance(ConfigReceivePushGateway config) {

        if (instance == null) {
            instance = new TemplateGateway(config);
        }

        return instance;
    }

    /**
     * <br> Gets the single instance of TemplateGateway. Usually rsremote keeps a single instance </br>
     * <br> this gateway class and always use this method to get the classes instance </br>
     *        Do not modify
     * @return single instance of TemplateGateway
     */
    public static TemplateGateway getInstance() {

        if (instance == null)
            Log.log.warn("Template Push Gateway is not initialized correctly..");

        return instance;
    }

    // Access methods for the cached Jetty server resource information
    // Do not modify
    public HttpServer getDefaultHttpServer()
    {
        return defaultHttpServer;
    }

    public Map<Integer, HttpServer> getHttpServers()
    {
        return httpServers;
    }

    public Map<String, HttpServer> getDeployedServlets()
    {
        return deployedServlets;
    }

    public String getInputKey()
    {
        return inputKey;
    }

	// If the unique key name is not "alert_id" from the result set, change the input key name here.
    public void setInputKey(String inputKey)
    {
        this.inputKey = inputKey;
    }

    /**
     *  Initialize the gateway with the blueprint property entries, Initialize the gateway with the default HTTP server
     *   instance and start to listen on the default port
     * @see com.resolve.gateway.resolvegateway.pull.PullGateway#initialize()
     * IMP Do not remove the existing code, but you can add additional logic for initialization if necessary
     */
    @Override
    protected void initialize() {

        ConfigReceivePushGateway config = (ConfigReceivePushGateway)configurations;
        queue = config.getQueue().toUpperCase();

        try {
            Log.log.info("Initializing Listener to queue: " + queue);
            this.gatewayConfigDir = "/config/" + queue.toLowerCase() + "/";

            defaultHttpServer = new HttpServer(config, null, null, name);
            defaultHttpServer.init();
            defaultHttpServer.start();

            if (defaultHttpServer.isStarted())
                defaultHttpServer.addDefaultResolveServlet();

            // Add additional custom logic here
        } catch (Exception e) {
            Log.log.error("Failed to config Template Push Gateway: " + e.getMessage(), e);
        }
    }

    @Override
    public void reinitialize() {

        ConfigReceivePushGateway config = (ConfigReceivePushGateway)configurations;
        queue = config.getQueue().toUpperCase();

        try {
            Log.log.info("Initializing Listener to queue: " + queue);
            this.gatewayConfigDir = "/config/" + queue.toLowerCase() + "/";

            defaultHttpServer = new HttpServer(config, null, null, name);
            defaultHttpServer.init();
            defaultHttpServer.start();

            if (defaultHttpServer.isStarted())
                defaultHttpServer.addDefaultResolveServlet();

            ConfigReceivePushGateway.getGateways().put(name, this);
        } catch (Exception e) {
            Log.log.error("Failed to config MoogSoft Gateway: " + e.getMessage(), e);
        }
    }
    @Override
    public void deactivate() {

        try {
            defaultHttpServer.stop();
        } catch (Exception e)  {
            Log.log.error(e.getMessage(), e);
        }

        Map<Integer, HttpServer> httpServers = getHttpServers();

        for (HttpServer httpServer : httpServers.values()) {
            try {
                httpServer.stop();
            } catch (Exception e) {
                Log.log.error(e.getMessage(), e);
            }
        }

        httpServers.clear();

        super.deactivate();
    }

    // The gateway will start to run by calling the super class common logic and start all the filter servlets to listen to the HTTP ports
    @Override
    public void start() {

        Log.log.debug("Starting Template Push Gateway");

        super.start();

     	// Add additional custom logic here
    }

    // This method will be called when the gateway is gracefully shutdown
    @Override
    public void stop() {

        Log.log.warn("Stopping Template Push gateway");

        try {
            getDefaultHttpServer().stop();
        } catch (Exception e)  {
            Log.log.error(e.getMessage(), e);
        }

        Map<Integer, HttpServer> httpServers = getHttpServers();

        for (HttpServer httpServer : httpServers.values()) {
            try {
                httpServer.stop();
            } catch (Exception e) {
                Log.log.error(e.getMessage(), e);
            }
        }

        httpServers.clear();

		// Add custom logic here

        super.stop();

    }

    /**
     * <br> This Method is absolutely required by the Gateway license service </br>
     * <br> Usually the Queue name is the same as the license code </br>
     * @return Gateway License code name
     */
    @Override
    public String getLicenseCode()
    {
        return "TEMPLATE";
    }
    // This method will be called when a filter is deployed to validate the semantics of the field only
    // The syntax of the filter fields have been validated when the filter is defined and saved from UI
    // including whether the filter field is required or not per gateway definition in blueprint.properties
    @Override
    public int validateFilterFields(Map<String, Object> params) {

        int code = 0;

        // Add custom logic here

        return code;
    }

    // The method returns the error string that is associated with the error code defined
    // under the custom logic being implemented
    @Override
    public String getValidationError(int errorCode) {

		String error = null;

		// Add custom logic here

        return error;
    }

    // This method is used by the system
    // Do not remove
    @Override
    protected void cleanFilter(ConcurrentHashMap<String, Filter> originalFilters) {
        //Add customized code here to modify your server when you clear out the deployed filters;

        for(Filter filter : originalFilters.values())
        {
            PushGatewayFilter pushGatewayFilter = (PushGatewayFilter) filter;
            String id = pushGatewayFilter.getId();

            try
            {
                if(pushGatewayFilter.getPort() > 0)
                {
                    HttpServer httpServer = httpServers.get(pushGatewayFilter.getPort());
                    httpServer.removeServlet(pushGatewayFilter.getId());
                    //if there is no filter/servlet remaining then stop the server on the port
                    if(httpServer.getServlets().isEmpty())
                    {
                        httpServer.stop();
                        httpServers.remove(pushGatewayFilter.getPort());
                    }
                }
                else
                {
                    //undeploy the servlet from the default server.
                    defaultHttpServer.removeServlet(id);
                }
            }
            catch (Exception e)
            {
                Log.log.error(e.getMessage(), e);
            }
        }
    }

    @Override
    public void initServers() {

        Map<Integer, HttpServer> httpServers = getHttpServers();
        Map<String, HttpServer> deployedServlets = getDeployedServlets();

        try {
            for (Filter deployedFilter : orderedFilters) {
                PushGatewayFilter filter = (PushGatewayFilter)deployedFilter;
/*                String gatewayName = filter.getGatewayName();
                PushGateway gateway = ConfigReceivePushGateway.getGateways().get(gatewayName);*/

                if (filter.getPort() == null || filter.getPort() <= 0) {
                    addDefaultServlet(filter);
                }

                else {
                    HttpServer httpServer = httpServers.get(filter.getPort());

                    if (httpServer == null)
                    {
                        HttpServer httpServer1 = deployedServlets.get(filter.getId());

                        if (httpServer1 != null)
                        {
                            //port changed
                            httpServer1.removeServlet(filter.getId());
                            getDeployedServlets().remove(filter.getId());
                        }

                        httpServer = new HttpServer((ConfigReceivePushGateway)configurations, filter.getPort(), filter.isSsl(), filter.getGatewayName());
                        httpServer.init();
                        httpServer.addServlet(filter);
                        httpServer.start();
                    }
                    else
                    {
                        httpServer.addServlet(filter);
                    }

                    httpServers.put(filter.getPort(), httpServer);
                    deployedServlets.put(filter.getId(), httpServer);
                }
            }
        }
        catch (Exception e)
        {
            Log.log.error(e.getMessage(), e);
        }
    }

	@Override
    protected void removeFilters(List<Filter> undeployedFilters) {

        for(Filter undeployedFilter : undeployedFilters)
        {
            PushGatewayFilter filter = (PushGatewayFilter)undeployedFilter;
            String id = filter.getId();

            try
            {
                if(filter.getPort() > 0)
                {
                    HttpServer httpServer = httpServers.get(filter.getPort());
                    httpServer.removeServlet(filter.getId());
                    //if there is no filter/servlet remaining then stop the server on the port
                    if(httpServer.getServlets().isEmpty())
                    {
                        httpServer.stop();
                        httpServers.remove(filter.getPort());
                    }
                }
                else
                {
                    //undeploy the servlet from the default server.
                    defaultHttpServer.removeServlet(id);

                    filters.remove(id);
                }
            }
            catch (Exception e)
            {
                Log.log.error(e.getMessage(), e);
            }
        }
    }

    // If the common logic in the super class does not support the use case for this specific gateway,
    // you may create override methods to replace the logic in the super class

} // class TemplateGateway
