package com.resolve.gateway.msg.msg_template;

import java.net.URL;
import java.util.Iterator;
import java.util.Map;

import com.resolve.gateway.AbstractGatewayAPI;
import com.resolve.gateway.Filter;
import com.resolve.gateway.resolvegateway.GatewayProperties;
import com.resolve.gateway.resolvegateway.msg.ConfigReceiveMSGGateway;
import com.resolve.gateway.resolvegateway.msg.MSGGatewayFilter;
import com.resolve.util.CryptUtils;
import com.resolve.util.Log;
import com.resolve.util.StringUtils;

public class TemplateGatewayAPI extends AbstractGatewayAPI {

    private static String HOSTNAME = "";
    private static String PORT = "";

    private String user = "";
    private String pass = "";

    private String filterId = null;

    private URL url = null;

    private static TemplateGateway gateway = TemplateGateway.getInstance();

    public static TemplateGatewayAPI instance = null;

    private static GatewayProperties properties = null;

	private TemplateGatewayAPI() {

        try {
            properties = ConfigReceiveMSGGateway.getGatewayProperties("Template");

            HOSTNAME = (String)properties.getHost();
            PORT = (String)properties.getPort().toString();
            user = (String)properties.getPass();
            pass = (String)properties.getPass();

			url = buildGatewayURL();

            // Add custom logic for third-party connection
        } catch(Exception e) {
          Log.log.error(e.getMessage(), e);
        }
    }

        /**
	     * Load parameters and initialize the Sample gateway API based on the filter specific information.
	     * @param filterId: the name of the filter specified in the UI. This filter must be deployed.
	     * @throws Exception
	     */
	    public TemplateGatewayAPI(String filterId) throws Exception {

	        Map<String, Filter> filters = gateway.getFilters();
	        MSGGatewayFilter filter = (MSGGatewayFilter)filters.get(filterId);

	        if(filter == null)
	            throw new Exception("Filter not found or not deployed with filter id: " + filterId);

	        this.filterId = filterId;

	        try {
	            Map<String, Object> attrs = filter.getAttributes();

	            if(attrs != null) {
	                for(Iterator<String> it=attrs.keySet().iterator(); it.hasNext();) {
	                    String key = it.next();
	                    switch(key) {
	                        case "uusername":
	                            String username = (String)attrs.get(key);
	                            user = StringUtils.isBlank(username)?(String)properties.getUser():username;
	                            break;
	                        case "upassword":
	                            String passcode = (String)attrs.get(key);
	                            pass = StringUtils.isBlank(passcode)?(String)properties.getPass():CryptUtils.decrypt(passcode);
	                            break;
	                        default:
	                            break;
	                    }
	                }
	            }

	            Log.log.debug("pass = " + pass);

				url = buildGatewayURL();

				// Add custom logic for third-party connection
	        } catch(Exception e) {
	            Log.log.error(e.getMessage(), e);
	        }
    }

    // Template code for composing the base URL for REST or SOAP web service call to connect to the third-party server
    public URL buildGatewayURL() throws Exception {

        StringBuffer sb = new StringBuffer();
        sb.append("https://").append(HOSTNAME);
        if(StringUtils.isNotBlank(PORT))
            sb.append(":").append(PORT);

        return new URL(sb.toString());
    }

    // Add more APIs for this specific gateway using object instance methods to be called by getInstance() or getInstance(filterId) methods
    // instead of publish static methods that will only work with the default constrcutor

} // class TemplateGatewayAPI