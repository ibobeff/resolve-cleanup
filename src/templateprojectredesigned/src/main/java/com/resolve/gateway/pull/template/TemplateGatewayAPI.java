/**
 * Resolve LLC @ copyright 2017
 */
package com.resolve.gateway.pull.pull_template;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import com.resolve.gateway.AbstractGatewayAPI;
import com.resolve.gateway.resolvegateway.GatewayProperties;
import com.resolve.util.Log;
import com.resolve.util.restclient.RestCaller;

/**
 * 
 * @author Resolve Systems LLC
 * 
 *  <P>   This class is responsible for making external API calls for the Pull gateway . The SDK supports  
 *   <br> REST and SOAP calls out of the box.  The external calls are usually made to perform the following </br> 
 *              <ul>
 *                  <li> 1.  Retrieve data from the external source </li>
 *                  <li> 2.  Acknowledge the message has been processed </li>
 *                  <li> 3.  UnAcknowledge an event already acknowledged </li>
 *                  <li> 4.  Write log messages for the external events </li>
 *                  <li> 5.  Close external events </li>
 *              </ul>
 *              
 *              This generated class has the template methods that can be implemented with the custom logic
 *              
 *   </p>
 */
 
public class TemplateGatewayAPI extends AbstractGatewayAPI {
    
    /** External API of the product's Host name as populated in the blueprint.properties */
    private static String HOSTNAME = "";
    /** External API of the product's port name as populated in the blueprint.properties file*/
    private static String PORT = "";
    /** External API of the product's URI as populated in the blueprint.properties file*/
    private static String URI = "";
    /** Base URL for connecting to the external products's API*/
    private static String baseUrl = null;
    /** User name for connecting to the external products's API*/
    private String user = "";
    /** Encrypted password for connecting to the external product's API*/
    private String pass = "";
    /** Place hoder for retrieving the above values from blueprint.properties file */
    private static Map<String, Object> systemProperties = null;
    
    /** Filter id created for this gateway*/
    private String filterId = null;
    private static TemplateGateway gateway = TemplateGateway.getInstance();
    public static TemplateGatewayAPI instance = null;
    private static GatewayProperties properties = null;
    
    /** Utility class available for making Rest calls  to external product API*/
    private RestCaller restCaller;
    
    private TemplateGatewayAPI() {
        initialize();
    }
    /**
     *  <br> The Basic constructor that iniatialzes the host and port data as configured in the blueprint.properties </br>
     *   
     * <br>  SAMPLE METHOD BODY: <br>
     *   
     *   <br>        try {  </br>
     <br>       // Load the system properties configured in the blueprint using the utility gateway class provided with sdk  </br>
      <br>      systemProperties = gateway.loadSystemProperties("Template"); </br>
      <br>      HOSTNAME = (String)systemProperties.get("HOST");  </br>
      <br>      PORT =((Integer)systemProperties.get("PORT")).toString();  </br>
      <br>      String sslType = (String)systemProperties.get("SSLTYPE");  </br>
      <br>      //Get username and password   </br>
     <br>       user = (String)systemProperties.get("USER");  </br>
      <br>      pass = (String)systemProperties.get("PASS");  </br>
       <br>     // Check for SSL configuration in the blueprint   </br>
       <br>     boolean isSsl = ((Boolean)systemProperties.get("SSL")).booleanValue(); </br>
      <br>      boolean selfSigned = ( sslType== null || sslType.length() == 0 ||
           sslType.equalsIgnoreCase("self-signed")) ? true : false; </br>
      <br>      // Append ssl to the URL call if SSL is enabled   </br>
      <br>      if(isSsl) {  </br>
      <br>          baseUrl="https://";  </br>
       <br>          }   </br>
      <br>      else  </br>
      <br>      {    baseUrl ="http://";}   </br>
      <br>      // Final URL used for making external REST or SOAP calls    </br>
      <br>      baseUrl = baseUrl +HOSTNAME+":"+PORT;  </br>
      <br>  } catch(Exception e) {  </br>
       <br>   Log.log.error(e.getMessage(), e);  </br>
      <br>  }   </br>
     */
    private void initialize() {

        try {
            // Load the system properties configured in the blueprint using the utility gateway class provided with sdk 
            systemProperties = gateway.loadSystemProperties("Template");
            HOSTNAME = (String)systemProperties.get("HOST");
            PORT =((Integer)systemProperties.get("PORT")).toString();
            String sslType = (String)systemProperties.get("SSLTYPE");
            //Get username and password 
            user = (String)systemProperties.get("USER");
            pass = (String)systemProperties.get("PASS");
            // Check for SSL configuration in the blueprint  
            boolean isSsl = ((Boolean)systemProperties.get("SSL")).booleanValue();
            boolean selfSigned = ( sslType== null || sslType.length() == 0 || sslType.equalsIgnoreCase("self-signed")) ? true : false;
            // Append ssl to the URL call if SSL is enabled 
            if(isSsl) {
                baseUrl="https://";
                 }
            else
            {    baseUrl ="http://";}
            // Final URL used for making external REST or SOAP calls  
            baseUrl = baseUrl +HOSTNAME+":"+PORT;
        } catch(Exception e) {
          Log.log.error(e.getMessage(), e);
        }
    }
    
    /**
     *  Allow only a static instance of this API class. This is a public method exposed to action tasks
     *  from Resolve so that the API can be invoked from action taks to acknowledge etc
     * @return 
     */
    public static TemplateGatewayAPI getInstance() {
        if(instance == null)
            instance = new TemplateGatewayAPI();
        return instance;
    }

    /**
     * <br>Core method that's called by the pull gateways to retrieve external messages/events/alerts </br>
     * <br>Developers can use the Rest caller provided by the sdk or use their own rest caller to perform the </br>
     * <br>API call for retrieving the data.   </br>
     * <br> It's important to populate the response in the format needed by this method for the gateway to </br>
     * <br> direct the messages to the right action tasks in resolve </br>
     * 
     * <br> METHOD BODY:  </br>
     * 
     * <br>        List<Map<String, String>> pulledEvents = new ArrayList<Map<String, String>>();  </br>
      // <br> TODO add your implementation here     </br>
     <br>   RestCaller restCaller = new RestCaller(baseUrl, user, pass);   </br>
     <br> // Sample code for composing the base URL for REST or SOAP web service call to connect </br>
     <br> //  to the third-party server </br>
        
      <br>  // Make the required Restcalls using the restCaller  </br>
        
      <br>  // Build trhe response and return it back  </br>
      <br>  return pulledEvents;  </br>
     * 
     * @param eventState
     * @param params
     * @return
     * @throws Exception
     */
     List<Map<String, String>> queryEvents(List<String> eventState, String... params) throws Exception
    {
        List<Map<String, String>> pulledEvents = new ArrayList<Map<String, String>>();
      // TODO add your implementation here   
        RestCaller restCaller = new RestCaller(baseUrl, user, pass); 
     // Sample code for composing the base URL for REST or SOAP web service call to connect
     //  to the third-party server
        
        // Make the required Restcalls using the restCaller 
        
        // Build trhe response and return it back 
        return pulledEvents;
    }
     /**
      * <br> This call is for acknowldging the message pulled from the externl API. acknowldging a message </br>
      * <br> changes the message status in the called product, and can be retrieved again by Querying for  </br>
      * <br> acknowledged messages. . Usually products expose APIs for this call. This method should be  </br>
      * <br> able to make the call based on the event id of the message passed.
      * 
      * <br> METHOD BODY: </br>
      * 
      *   <br>      // @@ TODO add your implementation here    </br>
        <br> return false; </br>
      * @param evid
      * @return
      * @throws Exception
      */
    public boolean acknowledge(String evid)throws Exception
    {
        // @@ TODO add your implementation here  
        return false;
    }
    
    /**
      * <br>This call can be used to unacknowledge a message that has been acknowledged as processed. </br>
      * <br> Unacknowldging a message makes the message available for processing again. Usually products </br>
      * <br> expose APIs for this call. This method should be able to make the call based on the event id </br>
      * <br> of the message passed, and using the rest Caller or SOAP caller </br>
      * 
      * <br>  METHOD BODY:  </br>
      *   <br>    // @@ TODO add your implementation here    </br>
       <br> return false; </br>
      * 
     * @param evid
     * @return
     * @throws Exception
     */
    public boolean unacknowledge(String evid) throws Exception
    {
      // @@ TODO add your implementation here  
        return false;
    }
    
    /**
     * <br>This call is made to write log messages of a message that has been retrieved using the pull request. </br>
     * <br>Usually the event id uniquely identify's the message and can be used to send a message for comments </br>
     * <br>on the external product. This can be done only if the external product exposes APIs to write log messages </br>
     * 
     * <br> METHOD BODY: </br>
     * 
     *   <br>     // @@ TODO add your implementation here   </br>
       <br> return false; </br>
     * @param evid
     * @param message
     * @return
     * @throws Exception
     */
    public boolean writeLog(String evid,String message) throws Exception
    {
        // @@ TODO add your implementation here  
        return false;
    }
    
    /**
     * <br> This call will close an event/alert/message based on the event id/unique id received from </br>
     * <br> the queried events from the filter. </br>
     * 
     * <br> METHOD BODY: </br>
     *    <br>    TODO add your implementation here   </br>
     * <br>  return false; </br>
     * @param evid
     * @return
     * @throws Exception
     */
    public boolean closeEvent(String evid) throws Exception
    {
      /** @@ TODO add your implementation here  */
        return false;
    }
    /** <br> Add more API calls for this specific gateway if product exposes additional APIs that can be used </br>
     *<br>  to process the retrieved records or addional actions are required to send the right data to Resolve </br>
     * <br> before or after every filter execution interval. Please refere the gateway manual and understand </br>
     * <br> how the pull filter works before adding any custom logic   </br> */


} // class TemplateGatewayAPI