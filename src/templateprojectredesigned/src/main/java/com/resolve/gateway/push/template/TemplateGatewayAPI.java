package com.resolve.gateway.push.push_template;

import java.util.Map;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import com.resolve.gateway.Filter;
import com.resolve.gateway.pull.template.TemplateGateway;
//import com.resolve.gateway.pull.template.TemplateGatewayAPI;
import com.resolve.gateway.resolvegateway.GatewayProperties;
import com.resolve.gateway.resolvegateway.push.PushGatewayFilter;
//import com.resolve.gateway.resolvegateway.pull.ConfigReceivePushGateway;
import com.resolve.gateway.AbstractGatewayAPI;
import com.resolve.util.CryptUtils;
import com.resolve.util.Log;
import com.resolve.util.StringUtils;
import com.resolve.util.restclient.RestCaller;
/**
 * 
 * @author Resolve Systems LLC
 * 
 *  <P>   This class is responsible for making external API calls for the Pull gateway . The SDK supports  
 *   <br> REST and SOAP calls out of the box.  The external calls are usually made to perform the following </br> 
 *   <br> This generated class has the template methods that can be implemented with the custom logic </br>
 *   </p>
 */
 
public class TemplateGatewayAPI extends AbstractGatewayAPI {
    private static String HOSTNAME = "";
    private static String PORT = "";
    private static String URI = "";

    private String user = "";
    private String pass = "";
    private static String baseUrl = null;
    
    private String filterId = null;
    private static TemplateGateway gateway = TemplateGateway.getInstance();
    public static TemplateGatewayAPI instance = null;
    private static GatewayProperties properties = null;
    private static Map<String, Object> systemProperties = null;
    
    //Utility class available for making Rest calls
    private RestCaller restCaller;
    
    private TemplateGatewayAPI() {
        initialize();
    }
    /**
     *   The Basic constructor that iniatialzes the host and port data as configured in the blueprint.properties
     */
    private void initialize() {

        try {
            //Load the system properties configured in the blueprint using the utility gateway class provided with sdk
            systemProperties = gateway.loadSystemProperties("Template");
            HOSTNAME = (String)systemProperties.get("HOST");
            PORT =((Integer)systemProperties.get("PORT")).toString();
            String sslType = (String)systemProperties.get("SSLTYPE");
            //Get username and password
            user = (String)systemProperties.get("USER");
            pass = (String)systemProperties.get("PASS");
            //Check for SSL configuration in the blueprint
            boolean isSsl = ((Boolean)systemProperties.get("SSL")).booleanValue();
            boolean selfSigned = ( sslType== null || sslType.length() == 0 || sslType.equalsIgnoreCase("self-signed")) ? true : false;
            //Append ssl to the URL call if SSL is enabled
            if(isSsl) {
                baseUrl="https://";
                 }
            else
            {    baseUrl ="http://";}
            //Final URL used for making external REST or SOAP calls
            baseUrl = baseUrl +HOSTNAME+":"+PORT;
        } catch(Exception e) {
          Log.log.error(e.getMessage(), e);
        }
    }
    
    /**
     *  Allow only a static instance of the API to be generated of this class
     * @return
     */
    public static TemplateGatewayAPI getInstance() {
        if(instance == null)
            instance = new TemplateGatewayAPI();
        return instance;
    }

    /**
     * Called for querying 
     * @param eventState
     * @param params
     * @return
     * @throws Exception
     */
     List<Map<String, String>> queryEvents(List<String> eventState, String... params) throws Exception
    {
        List<Map<String, String>> pulledEvents = new ArrayList<Map<String, String>>();
      //@@ TODO add your implementation here
        RestCaller restCaller = new RestCaller(baseUrl, user, pass); 
     // Sample code for composing the base URL for REST or SOAP web service call to connect to the third-party server
        
        //Make the required Restcalls using the restCaller
        
        //Build trhe response and return it back
        return pulledEvents;
    }

     
     /**
      * Use this function if you want to write anything to the log for an external event
      * @param evid
      * @param message
      * @return
      * @throws Exception
      */
     public boolean writeLog(String evid,String message) throws Exception
     {
         //@@ TODO add your implementation here
         return false;
     }
     
     /**
      * Use this function if you want to close an event in the remote product
      * @param evid
      * @return
      * @throws Exception
      */
     public boolean closeEvent(String evid) throws Exception
     {
       //@@ TODO add your implementation here
         return false;
     }

     /**
      * 
      * @param uri
      * @param params
      */
    public void makeRestCall(String uri, String...params) {
        RestCaller restCaller = new RestCaller(uri, params[0], params[1]);
    }


} // class SamplePushGatewayAPI