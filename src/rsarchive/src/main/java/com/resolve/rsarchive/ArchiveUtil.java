/******************************************************************************
* (C) Copyright 2016
*
* Resolve Systems, LLC
*
* All rights reserved
* This software is distributed under license and may not be used, copied,
* modified, or distributed without the express written permission of
* Resolve Systems, LLC.
*
******************************************************************************/ 
package com.resolve.rsarchive;

import java.util.Map;

import com.resolve.services.ServiceArchive;
import com.resolve.util.Constants;
import com.resolve.util.Log;
import com.resolve.util.StringUtils;

public class ArchiveUtil {
    @SuppressWarnings("rawtypes")
	public static void archive(Map params) {
        boolean force = StringUtils.getBoolean(Constants.ESB_PARAM_FORCE, params) ;

        Log.log.debug(String.format("Incoming parameter\t FORCE = %b", force));

        String dbType = ((Main) Main.main).configSQL.getDbtype().toLowerCase();
        ServiceArchive.archive(dbType, force);
    } // archive
}
    
    
