package com.resolve.rsarchive;

import org.quartz.JobExecutionContext;
import org.quartz.JobExecutionException;

import com.resolve.cron.CronExec;
import com.resolve.services.archive.ArchiveSIRData;
import com.resolve.util.Log;

public class SirBackupAction extends CronExec
{
    public SirBackupAction()
    {
    } // SirArchiveAction
    
    public void execute(JobExecutionContext context) throws JobExecutionException
    {
        super.execute(context);
        try
        {
        	// Skip if previous run of SIR backup is still in progress
        	if (!ArchiveSIRData.archiveSIRDataInProgress) {
        		ArchiveSIRData.startArchive();
        	}
        }
        catch (Exception e)
        {
            Log.log.error("Failed to execute SIR archive trigger.", e);
        }
    } // execute

} // Action


