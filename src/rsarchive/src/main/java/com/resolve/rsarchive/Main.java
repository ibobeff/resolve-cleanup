
/******************************************************************************
* (C) Copyright 2016
*
* Resolve Systems, LLC
*
* All rights reserved
* This software is distributed under license and may not be used, copied,
* modified, or distributed without the express written permission of
* Resolve Systems, LLC.
*
******************************************************************************/

package com.resolve.rsarchive;

import com.resolve.rsbase.ConfigSQL;
import com.resolve.search.ConfigSearch;
import com.resolve.search.SearchAdminAPI;

import java.io.File;
import java.text.ParseException;
import java.util.HashMap;
import java.util.Map;

import org.apache.commons.lang3.tuple.Pair;
import org.quartz.CronExpression;

import com.resolve.rsbase.ConfigGeneral;
import com.resolve.rsbase.ConfigId;
import com.resolve.rsbase.MainBase;
import com.resolve.cron.CronScheduler;
import com.resolve.cron.CronStore;
import com.resolve.cron.CronTask;
import com.resolve.persistence.util.HibernateUtil;
import com.resolve.rsarchive.Release;
import com.resolve.services.ServiceHibernate;
import com.resolve.services.archive.ArchiveSIRData;
import com.resolve.services.archive.ConfigArchive;
import com.resolve.sql.SQL;
import com.resolve.sql.SQLConnection;
import com.resolve.sql.SQLDriverInterface;
import com.resolve.util.Log;
import com.resolve.util.StringUtils;
import com.resolve.util.XDoc;

public class Main extends MainBase
{
    public ConfigArchive configArchive;
    public ConfigSQL configSQL;
//    public ConfigGeneral configGeneral;
    private ConfigSearch configSearch;
    public ConfigCron configCron;
    CronScheduler cron;
    
    public static void main(final String[] args) throws Exception
    {
        String serviceName = null;

        try
        {
            if (args.length > 0)
            {
                serviceName = args[0];
            }

            main = new Main();
            main.init(serviceName);
        }
        catch (Throwable t)
        {
            t.printStackTrace();
            Log.log.error("Failed to start RSArchive", t);
            System.exit(0);
        }
    }
    
    private Pair<Boolean, CronExpression> validateSchedule(String schedule) {
    	Pair<Boolean, CronExpression> result = Pair.of(Boolean.FALSE, null) ;
    	
    	if (StringUtils.isNotBlank(schedule)) {
    		try {
    			CronExpression ce = new CronExpression(schedule);
    			result = Pair.of(Boolean.TRUE, ce);
    		} catch (ParseException pe) {
    			Log.log.error(String.format("Error %sin parsing specified schedule [%s] to %s", 
    						  			    (StringUtils.isNotBlank(pe.getMessage()) ? "[" + pe.getMessage() + "] " : ""),
    						  			    schedule, CronExpression.class.getName()));
    		}
    	}
    	
    	return result;
    }
    
    @Override
    public void init(String serviceName) throws Exception {
        // init release information
        initRelease(new Release(serviceName));

        // init logging
        initLog();

        initENC();
        
        // init signal handlers
        initShutdownHandler();

        // init configuration
        initConfig();
        
        Pair<Boolean, CronExpression> scheduleValidAndCronExpr = validateSchedule(configArchive.getSchedule());
        Pair<Boolean, CronExpression> sirScheduleValidAndCronExpr = validateSchedule(configArchive.getSirArchiveSchedule());
        
        if (!scheduleValidAndCronExpr.getLeft() || !sirScheduleValidAndCronExpr.getLeft() ||
        	scheduleValidAndCronExpr.getRight() == null || sirScheduleValidAndCronExpr.getRight() == null) {
        	String errMsg = String.format("Failed to validate configured SIR [%s] and/or Non-SIR [%s] schedules", 
        								  configArchive.getSirArchiveSchedule(), configArchive.getSchedule());
        	Log.log.error(errMsg);
        	throw new Exception(errMsg);
        }
        
        // init timezone
        initTimezone();
        
        if (configArchive.isActive()) {
        	initSQL();
        
        	initPersistence();
                
        	initElasticSearch();
        
        	startCron(new CronScheduler(configCron.config){ });
        
        	// init scheduler
            scheduleArchiveJob(scheduleValidAndCronExpr.getRight());
        
	        try {
	            if (configArchive.isSirBackup() && configArchive.isSirRestore()) {
	                throw new Exception("Both flags, sirArchive and sirRestore, cannot be true at the same time.");
	            }
	            
	            if (configArchive.isSirBackup()) {
	                //archive/backup the SIR data to SQL database
	                scheduleSIRArchiveJob(sirScheduleValidAndCronExpr.getRight());
	            }
	            
	            if (configArchive.isSirRestore()) {
	                /*
	                 * Restore backed up SIR data from SQL databse to ES.
	                 * As restore is a one time operation, we need not run a scheduler.
	                 */
	                ArchiveSIRData.startRestore();
	            }
	        } catch(Exception ex) {
	            /*
	             * We throw and catch the exception right here so that process should not stop
	             * and user still should be notified about configuration error.
	             */
	            Log.log.error("Error while starting SIR archive/restore job.", ex);
	        }
        }
        
        // save configuration
        initSaveConfig();

        if (configArchive.isActive()) {
        	Log.log.info("RSArchive started...");
        } else {
        	Log.log.info("RSArchive is Not Active., Shutting Down...");
        }
        
    }

    protected void initENC() throws Exception
    {
        // load configuration state and initialization overrides
        Log.log.info("Loading configuration settings for enc");
        loadENCConfig();
        super.initENC();
    } // initENC

    protected void loadENCConfig() throws Exception
    {
        ConfigENC.load();
    }

    
    private void scheduleArchiveJob(CronExpression cronExpr)
    {
        CronTask t = new CronTask();
        t.setName("RSArchive");
        Log.log.info(String.format("Non-SIR Archive Schedule From Configuation: [%s][%s]", configArchive.getSchedule(),
        						   cronExpr));
        t.setExpression(configArchive.getSchedule());
        t.setClassname("com.resolve.rsarchive.CronAction");
        CronStore.addTask(t);
    }
    
    private void scheduleSIRArchiveJob(CronExpression cronExpr)
    {
        CronTask t = new CronTask();
        t.setName("RSSIRArchive");
        Log.log.info(String.format("SIR Archive Schedule From Configuation: [%s][%s]", configArchive.getSirArchiveSchedule(),
        						   cronExpr));
        t.setExpression(configArchive.getSirArchiveSchedule());
        t.setClassname("com.resolve.rsarchive.SirBackupAction");
        CronStore.addTask(t);
    }

    protected void stopCron() {
    	if (configArchive.isActive()) {
	        try {
	            cron.stop();
	            Log.log.info("CRON stopped");
	        } catch (Exception e) {
	            Log.log.error(e, e);
	            throw new RuntimeException(e);
	        }
    	}
    }
    
    protected void startCron(CronScheduler sch)
    {
        Log.log.info("Starting CRON");
        cron = sch;
        try
        {
            // start CronStore
            CronStore.start();
        }
        catch (Exception e)
        {
            Log.log.error(e, e);
            throw new RuntimeException(e);
        }
        
        Log.log.info("CRON started");
    } // startCron

    @Override
    protected void exit(String[] argv)
    {
        System.exit(0);
    }

    @Override
    protected void terminate() {
        // shutdown
        isShutdown = true;

        Log.log.warn("Terminating " + release.name.toUpperCase());

        try {
            stopCron();
        } catch (Exception e) {
            Log.log.error("Failed to terminate RSArchive: " + e.getMessage(), e);
        }
        
        exitSaveConfig();
        Log.log.warn("Terminated " + release.name.toUpperCase());
    } // terminate

    @Override
    protected void loadConfig(XDoc configDoc) throws Exception
    {
        if (configGeneral == null)
        {
            configGeneral = new ConfigGeneral(configDoc);
            configGeneral.load();
        }
        if (configId == null)
        {
            configId = new ConfigId(configDoc);
            configId.load();
        }
        
        configSQL = new ConfigSQL(configDoc);
        configSQL.load();

        configCron = new ConfigCron(configDoc);
        configCron.load();
        
        configArchive = new ConfigArchive(configDoc);
        configArchive.load();

        configSearch = new ConfigSearch(configDoc);
        configSearch.load();
        
    } // loadConfig

    protected void saveConfig() throws Exception
    {
        configSQL.save();
        configCron.save();
        configArchive.save();
        configSearch.save();
        
        super.saveConfig();
    } // saveConfig
    
    
    protected void initSQL()
    {
        try
        {
            Log.log.info("Starting SQL");

            // get db driver
            SQLDriverInterface driver = SQL.getDriver(configSQL.getDbtype(), configSQL.getDbname(), configSQL.getHost(), configSQL.getUsername(), configSQL.getP_assword(), configSQL.getUrl(), configSQL.isDebug());

            if (driver != null)
            {
                SQL.init(driver);
                SQL.start();
                SQLConnection conn = SQL.getConnection();
                SQL.close(conn);
            }
        }
        catch (Exception e)
        {
            Log.log.error("Failed to initialize SQL database connection", e);
            throw new RuntimeException(e);
        }
    } // initSQL

    /**
     * Initialize search (ElasticSearch) service
     */
    protected void initElasticSearch()
    {
        //the search is always on in RSCONTROL
        Log.log.info("Initializing Search.");
        Map<String, String> properties = new HashMap<String, String>();
        configSearch.setProperties(properties);
        SearchAdminAPI.init(configSearch);
        Log.log.info("Search Initialized Successfully.");
    }

    protected void initPersistence()
    {
        String customMappingFileLocation = configGeneral.getHome() + File.separator + "rsarchive" + File.separator + "config";
        String hibernateCfgLocation = configGeneral.getHome() + File.separator + "rsarchive" + File.separator + "config" + File.separator + "hibernate.cfg.xml";

        /*
         * Uncomment only if CI - 353 Streamline Hibernate initialization is approved for 6.4
         * to not initialize custom tables and avoid schema update.
         */
//        ServiceHibernate.initPersistence(configSQL, null, null, hibernateCfgLocation, false);
        ServiceHibernate.initPersistence(configSQL, null, customMappingFileLocation, hibernateCfgLocation);
        
        HibernateUtil.initMasterLockObject();

    }
}