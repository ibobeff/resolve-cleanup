package com.resolve.rsarchive;

import java.util.HashMap;

import org.quartz.JobExecutionContext;
import org.quartz.JobExecutionException;

import com.resolve.cron.CronExec;
import com.resolve.util.Log;

public class CronAction extends CronExec
{
    public CronAction()
    {
    } // ActionTrigger
    
    @SuppressWarnings({ "rawtypes", "static-access" })
    public void execute(JobExecutionContext context) throws JobExecutionException
    {
        super.execute(context);
        try
        {
            ArchiveUtil.archive(new HashMap());
            
//            data = context.getJobDetail().getJobDataMap();
//            if (data != null)
//            {
//                //data from ResolveCron model
//                String name = getJobData("NAME"); // name of the job scheduler like 'DOCUMENT ARCHIVE CHECK'
//                String wiki = getJobData("WIKI"); // will be command like 'RSVIEWS#MSocial.dailyEmail' or a wiki document like 'EventCatalog.UpdateEventHistory'
//                String paramsStr = getJobData("PARAMS");
//                Map params = StringUtils.urlToMap(paramsStr, false);
//            }
        }
        catch (Exception e)
        {
            Log.log.error("Failed to execute CronActionTrigger: "+e.getMessage(), e);
        }
    } // execute

} // Action


