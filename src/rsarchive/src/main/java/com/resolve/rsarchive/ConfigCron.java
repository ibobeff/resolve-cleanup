/******************************************************************************
* (C) Copyright 2016
*
* Resolve Systems, LLC
*
* All rights reserved
* This software is distributed under license and may not be used, copied,
* modified, or distributed without the express written permission of
* Resolve Systems, LLC.
*
******************************************************************************/

package com.resolve.rsarchive;

import com.resolve.rsbase.MainBase;
import com.resolve.cron.CronConfig;
import com.resolve.util.ConfigMap;
import com.resolve.util.XDoc;

public class ConfigCron extends ConfigMap
{
    private static final long serialVersionUID = 2237476482900301089L;
    public CronConfig config;
    
    public ConfigCron(XDoc config) throws Exception
    {
        super(config);
    } // ConfigESB
    
    public void load() throws Exception
    {
        this.config = new CronConfig();
        
        config.setFilename(xdoc.getStringValue("./CRON/FILENAME", MainBase.main.getProductHome()+"/"+config.getFilename()));
        config.setThreadCount(xdoc.getIntValue("./CRON/@THREADCOUNT", config.getThreadCount()));
        config.setThreadPriority(xdoc.getIntValue("./CRON/@THREADPRIORITY", config.getThreadPriority()));
        config.setMisfireThreshold(xdoc.getIntValue("./CRON/@MISFIRETHRESHOLD", config.getMisfireThreshold()));
        config.setNotifyOn(xdoc.getBooleanValue("./CRON/@NOTIFYON", config.isNotifyOn()));
    } // load

    public void save() throws Exception
    {
        xdoc.setStringValue("./CRON/@FILENAME", config.getFilename());
        xdoc.setIntValue("./CRON/@THREADCOUNT", config.getThreadCount());
        xdoc.setIntValue("./CRON/@THREADPRIORITY", config.getThreadPriority());
        xdoc.setIntValue("./CRON/@MISFIRETHRESHOLD", config.getMisfireThreshold());
        xdoc.setBooleanValue("./CRON/@NOTIFYON", config.isNotifyOn());
    } // save
    
} // ConfigCron

