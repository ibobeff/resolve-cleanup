/******************************************************************************
* (C) Copyright 2014
*
* Resolve Systems, LLC
*
* All rights reserved
* This software is distributed under license and may not be used, copied,
* modified, or distributed without the express written permission of
* Resolve Systems, LLC.
*
******************************************************************************/

package com.resolve.rsremote;

import java.lang.annotation.Annotation;
import java.lang.reflect.Constructor;
import java.lang.reflect.Field;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.lang.reflect.Modifier;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.TimeUnit;

import javax.persistence.Column;
import javax.persistence.Lob;

import com.resolve.thread.TaskExecutor;
import com.resolve.thread.ScheduledExecutor;
import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.collections.MapUtils;
import org.apache.commons.lang3.reflect.MethodUtils;
import org.apache.commons.lang3.tuple.Pair;
import org.dom4j.Element;
import org.dom4j.Node;

import com.resolve.esb.MListener;
import com.resolve.rsbase.MainBase;
import com.resolve.gateway.BaseClusteredGateway;
import com.resolve.gateway.BaseGateway;
import com.resolve.gateway.Filter;
import com.resolve.gateway.FilterCompanionModel;
import com.resolve.gateway.Gateway;
import com.resolve.gateway.MGateway;
import com.resolve.gateway.amqp.AmqpGateway;
import com.resolve.gateway.caspectrum.CASpectrumGateway;
import com.resolve.gateway.database.DBGateway;
import com.resolve.gateway.database.DBGatewayFactory;
import com.resolve.gateway.email.EmailGateway;
import com.resolve.gateway.ews.EWSGateway;
import com.resolve.gateway.exchange.ExchangeGateway;
import com.resolve.gateway.hpom.HPOMGateway;
import com.resolve.gateway.hpsm.HPSMGateway;
import com.resolve.gateway.http.HttpGateway;
import com.resolve.gateway.itm.ITMGateway;
import com.resolve.gateway.netcool.NetcoolGateway;
import com.resolve.gateway.remedy.RemedyGateway;
import com.resolve.gateway.remedyx.RemedyxGateway;
import com.resolve.gateway.resolvegateway.msg.ConfigReceiveMSGGateway;
import com.resolve.gateway.resolvegateway.msg.MSGGateway;
import com.resolve.gateway.resolvegateway.pull.ConfigReceivePullGateway;
import com.resolve.gateway.resolvegateway.pull.PullGateway;
import com.resolve.gateway.resolvegateway.push.ConfigReceivePushGateway;
import com.resolve.gateway.resolvegateway.push.PushGateway;
import com.resolve.gateway.salesforce.SalesforceGateway;
import com.resolve.gateway.servicenow.ServiceNowGateway;
import com.resolve.gateway.snmp.SNMPGateway;
import com.resolve.gateway.ssh.SSHGateway;
import com.resolve.gateway.tcp.TCPGateway;
import com.resolve.gateway.telnet.TelnetGateway;
import com.resolve.gateway.tibcobespoke.TIBCOBespokeGateway;
import com.resolve.gateway.tsrm.TSRMGateway;
import com.resolve.gateway.xmpp.XMPPGateway;
import com.resolve.persistence.model.GatewayFilter;
import com.resolve.services.hibernate.vo.GatewayFilterVO;
import com.resolve.util.Constants;
import com.resolve.util.ERR;
import com.resolve.util.Log;
import com.resolve.util.MappingAnnotation;
import com.resolve.util.MessageDispatcher;
import com.resolve.util.StringUtils;
import com.resolve.util.XDoc;

public class Main extends com.resolve.rsbase.MainBase
{
    final static int CHECKOUT_INTERVAL = 1;
    final static int UPDATE_INTERVAL = 6; 

    ConfigRegistration configRegistration;
    ConfigReceiveSNMP configReceiveSNMP;
    ConfigReceiveNetcool configReceiveNetcool;
    ConfigReceiveExchange configReceiveExchange;
    ConfigReceiveRemedy configReceiveRemedy;
    ConfigReceiveRemedyx configReceiveRemedyx;
    ConfigReceiveDB configReceiveDB;
    ConfigReceiveEmail configReceiveEmail;
    ConfigReceiveTSRM configReceiveTSRM;
    ConfigReceiveITM configReceiveITM;
    ConfigReceiveXMPP configReceiveXMPP;
    ConfigReceiveServiceNow configReceiveServiceNow;
    ConfigReceiveSalesforce configReceiveSalesforce;
    ConfigReceiveSSH configReceiveSSH;
    ConfigReceiveHPOM configReceiveHPOM;
    ConfigReceiveTelnet configReceiveTelnet;
    ConfigReceiveEWS configReceiveEWS;
    ConfigReceiveLDAP configReceiveLDAP;
    ConfigReceiveAD configReceiveAD;
    ConfigReceiveHPSM configReceiveHPSM;
    ConfigReceiveAmqp configReceiveAmqp;
    ConfigReceiveTCP configReceiveTCP;
    ConfigReceiveHTTP configReceiveHTTP;
    ConfigReceiveTIBCOBespoke configReceiveTIBCOBespoke;
    ConfigReceiveCASpectrum configReceiveCASpectrum;
    ConfigSelfCheck configSelfCheck;
    
    ArrayList<MListener> namedQueueListeners = new ArrayList<MListener>();

    private boolean isSocialGatewayShutdown = true;

    private Map<String, String> sdkDvlpdImplPrefxToPkg = new HashMap<String, String>();
    private Map<String, ConfigReceiveGateway> sdkdDvlpdCfgRcvs = new HashMap<String, ConfigReceiveGateway>();
    private static Map<String, Object> gwInstances = new HashMap<String, Object>();
    private final static Set<String> reservedUFilterFields = new HashSet<String>(Arrays.asList("UQueue",
                                                                                               "UOrder",
                                                                                               "UEventEventId",
                                                                                               "URunbook",
                                                                                               "UActive",
                                                                                               "UName",
                                                                                               "UInterval",
                                                                                               "UScript"));
    // Map of Filter Companion Model name to its Details
    private static Map<String, Pair<String, Map<String, String>>> 
    														fcmName2Details = 
    															new HashMap<String, Pair<String, Map<String, String>>>();
    
    /*
     * Main execution method for RSRemote
     */
    public static Map<String, Object> getGatewayInstances() {
        return gwInstances;
    }

    public static void saveGatewayInstance(Object gtwInstanceObj) {
        Gateway gateway = (Gateway) gtwInstanceObj;
        gwInstances.put(gateway.getQueueName().toUpperCase(), gateway);
    }

    public static void main(String[] args)
    {
        String serviceName = null;

        try
        {
            if (args.length > 0)
            {
                serviceName = args[0];
            }

            main = new Main();
            main.init(serviceName);
        }
        catch (Throwable t)
        {
            t.printStackTrace();
            Log.alert(ERR.E10005, t);

            System.exit(0);
        }

    } // main

    public void init(String serviceName) throws Exception
    {
        // init release information
        initRelease(new Release(serviceName));

        // init logging
        initLog();

        // init enc
        initENC();

        // init configuration
        initConfig();

        // init signal handlers
        initShutdownHandler();

        // init startup
        initStartup();

        // init executor
        initThreadPools();

        // init timezone
        initTimezone();

        // init message bus
        initESB();

        // start config
        startConfig();

        // start message bus only after initialization is finished.
        startESB();

        // save configuration
        initSaveConfig();

        // display started message and set MListener to active
        initComplete();

        // init license service
        initLicenseService();

        // init license
        checkLicense();

        //get cluster based properties from RSControl
        initClusterProperties();

        // start receivers, make sure this is after initLicenseService() call
        if(isClusterModeActive())
        {
            startReceivers();
        }

        // initMetric
        // Note: metric instantiation depends on MServer.setActive to be true in order to use the
        // ESB_NAME_RSREMOTES queue, hence initMetric() is placed after initComplete() during initialization
        initMetric();

        // store blueprint into databasae
        // first time ever when the RSRemote comes up, send a message to RSCONTROL
        // to store the blueprint in database, after that it will be done through a scheduled task.
        MRegister.updateBlueprint();

        // startup tasks
        startDefaultTasks();
        
        startSelfCheckTasks();

    } // init

    private String getBlueprint() {
        return "";
    }

    protected void checkLicense()
    {
        getLicenseService().checkLicense();

        //check for maximum heap size, do not need memory limit on RSREMOTE at this point
        //LicenseUtil.checkMaximumMemory(license.getMaxMemory().get(LicenseUtil.COMPONENT_RSREMOTE));

        /* this is irrelavent now since gateways will check for itself and
         * if the license is not available they will shutdown automatically.
        if (Constants.LICENSE_TYPE_COM.equalsIgnoreCase(license.getType()))
        {
            isSocialGatewayShutdown = false;
            stopReceivers();
        }*/

        //since this component is ok to run, send its presence.
        getLicenseService().sendLicenseRegistration();

        // schedule next license check
        ScheduledExecutor.getInstance().executeDelayed(this, "checkLicense", 30, TimeUnit.MINUTES);
    }

    protected void initENC() throws Exception
    {
        // load configuration state and initialization overrides
        Log.log.info("Loading configuration settings for enc");
        loadENCConfig();
        super.initENC();
    } // initENC

    protected void loadENCConfig() throws Exception
    {
        ConfigENC.load();
    }

    /*
     * NOTE: String args[] is required to catch NT service shutdown message
     */
    protected void exit(String[] argv)
    {
        System.exit(0);
    } // exit

    protected void terminate()
    {
        // shutdown
        isShutdown = true;

        Log.log.warn("Terminating " + release.name.toUpperCase());

        try
        {
            // social gateway (email, xmpp) shutdown
            isSocialGatewayShutdown = true;

            // close queue consumers except GUID queue
            Log.log.info("Closing listeners");
            MainBase.main.esb.getMServer().closeListeners(false);
        }
        catch (Exception e)
        {
            Log.log.error("Failed to terminate RSRemote: " + e.getMessage(), e);
        }

        try
        {
            // wait for completion or timeout
            ExecuteMain.waitForActiveCompletion();
        }
        catch (Exception e)
        {
            Log.log.error("Failed to terminate RSRemote: " + e.getMessage(), e);
        }

        try
        {
            // stop gateways
            Log.log.info("Stopping receivers");
            stopReceivers();
        }
        catch (Exception e)
        {
            Log.log.error("Failed to terminate RSRemote: " + e.getMessage(), e);
        }
        unregisterConfig();
        try
        {
            // save config
            exitSaveConfig();

            // terminate
            super.terminate();
        }
        catch (Exception e)
        {
            Log.log.error("Failed to terminate RSRemote: " + e.getMessage(), e);
        }
        Log.log.warn("Terminated " + release.name.toUpperCase());
    } // terminate
    
    private void unregisterConfig() {
        try
        {
            String guid = configId.getGuid();
            Map unregisterConfigParams = new HashMap();
            unregisterConfigParams.put("GUID", guid);
            Log.log.info("unregisering rsremote config: " + guid);
            MainBase.main.esb.sendMessage("RSCONTROLS", "MRegister.unregisterRSRemoteConfig", unregisterConfigParams);
        }
        catch (Exception e)
        {
            Log.log.error("Failed to terminate RSRemote: " + e.getMessage(), e);
        }
    }

    protected void verifyLicense()
    {
        Log.log.info("verifyLicense initiated");

        if (configReceiveExchange.isActive() || configReceiveEmail.isActive())
        {
            Log.log.info("Email gateway available cannot shutdown completely.");
            isSocialGatewayShutdown = false;
            stopReceivers();
        }
        else
        {
            exit(null);
        }
    } // exit

    @SuppressWarnings("unchecked")
    private Map<String, String> getSDKDevelopedGateways(XDoc configDoc) throws Exception
    {
        Map<String, String> sdkDevelopedGateways = new HashMap<String, String>();
        
        List<Element> sdkGtwNodes = configDoc.getNodes("/CONFIGURATION/RECEIVE/*[@IMPLPREFIX]");
        
        if (sdkGtwNodes != null && !sdkGtwNodes.isEmpty())
        {
            for (Element sdkGtwNode : sdkGtwNodes)
            {
                String implprefix = sdkGtwNode.attributeValue("IMPLPREFIX"); //configDoc.getStringValue(sdkGtwNode, "@IMPLPREFIX", "");
                String sdkgtwpackage = sdkGtwNode.attributeValue("PACKAGE"); //configDoc.getStringValue(sdkGtwNode, "@PACKAGE", "");
                
                if (StringUtils.isNotBlank(implprefix) && StringUtils.isNotBlank(sdkgtwpackage))
                {
                    sdkDevelopedGateways.put(implprefix, sdkgtwpackage);
                }
            }
        }
        
        return sdkDevelopedGateways;
    }
    
    @SuppressWarnings("unchecked")
    private void validateConfigReceiveImpl(String cfgReceiveGtwClassName) throws Exception
    {
        Class<? extends ConfigReceiveGateway> cfgReceiveGtwClass = null;
        
        // Load <package>.ConfigReceive<implprefix> CLass
        
        try
        {
            cfgReceiveGtwClass = 
                (Class<? extends ConfigReceiveGateway>) Class.forName(cfgReceiveGtwClassName, false, 
                                                                      this.getClass().getClassLoader());
        }
        catch(Exception e)
        {
            Log.log.warn("Failed to load " + cfgReceiveGtwClassName + ".", e);
            throw e;
        }
        
        // Constructor with XDoc as parameter
        
        try
        {
            cfgReceiveGtwClass.getDeclaredConstructor(XDoc.class);
        }
        catch (Exception e)
        {
            Log.log.warn("Expected constructor " + cfgReceiveGtwClassName + "(" + XDoc.class.getName() + ") not found", e);
            throw e;
        }
        
        // Methods getRootNode(), load() and save()
        
        try
        {
           cfgReceiveGtwClass.getDeclaredMethod("getRootNode", (Class<?>[])null);
        }
        catch (Exception e)
        {
            Log.log.warn("Expected method getRootNode() returning String not found in " + cfgReceiveGtwClassName + ".", e);
            throw e;
        }
        
        try
        {
            cfgReceiveGtwClass.getDeclaredMethod("load", (Class<?>[])null);
        }
        catch (Exception e)
        {
            Log.log.warn("Expected method load() not found in " + cfgReceiveGtwClassName + ".", e);
            throw e;
        }
        
        try
        {
            cfgReceiveGtwClass.getMethod("save", (Class<?>[])null);
        }
        catch (Exception e)
        {
            Log.log.warn("Expected method save() not found in " + cfgReceiveGtwClassName + ".", e);
            throw e;
        }
    }
    
    @SuppressWarnings("unchecked")
    private Map<String, Class<?>> validateFilterVOImpl(String gtwFilterVOClassName) throws Exception
    {
        Class<? extends GatewayFilterVO> gtwFilterVOClass = null;
        Map<String, Class<?>> getterToRtnType = new HashMap<String, Class<?>>();
        
        // Load com.resolve.services.hibernate.vo.<implprefix>FilterVO CLass
        
        try
        {
            gtwFilterVOClass = (Class<? extends GatewayFilterVO>) Class.forName(gtwFilterVOClassName, 
                                                                                false, 
                                                                                this.getClass().getClassLoader());
        }
        catch(Exception e)
        {
            Log.log.warn("Failed to load " + gtwFilterVOClassName + ".", e);
            throw e;
        }
        
        // Identify and validate U<xxxx> Filter Fields and getU<xxxx> public methods with @MappingAnnotation
        
        Field[] userFields = null;
        
        try
        {
            userFields = gtwFilterVOClass.getDeclaredFields();
            
            for (int i = 0; i < userFields.length; i++)
            {
                Field field = userFields[i];
                
                int fldModifiers = field.getModifiers();
                
                // field is not Static and not Final
                
                if (!Modifier.isStatic(fldModifiers) && !Modifier.isFinal(fldModifiers))
                {
                    if (!field.getName().startsWith("U"))
                    {
                        Log.log.warn("User declared field " + field.getName() + " in " + gtwFilterVOClassName + " does not start with U.");
                        throw new Exception ("User declared field " + field.getName() + " in " + gtwFilterVOClassName + " does not start with U.");
                    }
                    
                    if (reservedUFilterFields.contains(field.getName()))
                    {
                        Log.log.warn("User declared field name " + field.getName() + " is reserved and should not be used in " + gtwFilterVOClassName + ".");
                        throw new Exception ("User declared field name " + field.getName() + " is reserved and should not be used in " + gtwFilterVOClassName + ".");
                    }
                    
                    // Check if method getU<xxxx> is annotaed with @MappingAnnotation
                    
                    Method getMethod = null;
                    
                    try
                    {
                        getMethod = gtwFilterVOClass.getDeclaredMethod("get" + field.getName(), (Class<?>[])null);
                    }
                    catch(Exception mnfe)
                    {
                        Log.log.info("User declared field " + field.getName() + " in " + gtwFilterVOClassName + " does not have corresponding getter method.");
                    }
                    
                    if (getMethod != null && Modifier.isPublic(getMethod.getModifiers()) && 
                        getMethod.getAnnotation(MappingAnnotation.class) != null)
                    {
                        getterToRtnType.put("get" + field.getName(), getMethod.getReturnType());
                    }
                }
            }
        }
        catch(Exception gdfe)
        {
            Log.log.warn("Failed to get declared fields in " + gtwFilterVOClassName + ".", gdfe);
            throw gdfe;
        }
        
        if (!getterToRtnType.isEmpty())
        {
            Log.log.info(gtwFilterVOClassName + " has " + getterToRtnType.size() + " user defined private, non Static, non Final Fields." );
        }
        else
        {
            Log.log.warn(gtwFilterVOClassName + " does not have any user defined non Static, non Final Fields.");
        }
            		
        return getterToRtnType;
    }
    
    @SuppressWarnings("unchecked")
    private void validateFilterModelImpl(String gtwFilterModelClassName, String gtwFilterVOClassName,
                                         Map<String, Class<?>> getterToRtnType) throws Exception
    {
        Class<? extends GatewayFilter<? extends GatewayFilterVO>> gtwFilterModelClass = null;
        
        // Load com.resolve.persistence.model.<implprefix>Filter CLass
        
        try
        {
            gtwFilterModelClass = 
                (Class<? extends GatewayFilter<? extends GatewayFilterVO>>)Class.forName(gtwFilterModelClassName, 
                                                                                         false, this.getClass().getClassLoader());
        }
        catch(Exception e)
        {
            Log.log.warn("Failed to load " + gtwFilterModelClassName + ".", e);
            throw e;
        }
        
        // Methods doGetVO(), applyVOToModel(? extends GatewayFilterVO)
        
        try
        {
            gtwFilterModelClass.getDeclaredMethod("doGetVO", (Class<?>[])null);
        }
        catch (Exception e)
        {
            Log.log.warn("Expected method doGetVO() returning " + gtwFilterVOClassName + " not found in " + gtwFilterModelClassName + ".", e);
            throw e;
        }
        
        try
        {
            gtwFilterModelClass.getDeclaredMethod("applyVOToModel", Class.forName(gtwFilterVOClassName));
        }
        catch (Exception e)
        {
            Log.log.warn("Expected method applyVOToModel(" + gtwFilterVOClassName + ") not found in " + gtwFilterModelClassName + ".", e);
            throw e;
        }
        
        boolean hasLob = false;
        
        for (String getterMthdName : getterToRtnType.keySet())
        {
            Method getMethod = null;
            
            try
            {
                getMethod = gtwFilterModelClass.getDeclaredMethod(getterMthdName, (Class<?>[])null);
            }
            catch(Exception mnfe)
            {
                Log.log.warn("Expected method " + getterMthdName + " returning " + getterToRtnType.get(getterMthdName).getName() + " not found in " + gtwFilterModelClassName + ".", mnfe);
                
                try
                {
                    throw mnfe;
                }
                catch (Exception e1)
                {
                    ;
                }
            }
            
            if (getMethod != null)
            {
                if (!getMethod.getReturnType().getName().equals(getterToRtnType.get(getterMthdName).getName()))
                {
                    Log.log.warn("Return type " + getMethod.getReturnType().getName() + " of " + getterMthdName + 
                                 " in " + gtwFilterModelClassName + " does not match return type " + 
                                 getterToRtnType.get(getterMthdName).getName() + " of " + getterMthdName + 
                                 " in " + gtwFilterVOClassName + ".");
                    throw new Exception("Return type " + getMethod.getReturnType().getName() + " of " + getterMthdName + 
                                        " in " + gtwFilterModelClassName + " does not match return type " + 
                                        getterToRtnType.get(getterMthdName).getName() + " of " + getterMthdName + 
                                        " in " + gtwFilterVOClassName + ".");
                }
                
                Annotation clmnAntn = null;
                
                clmnAntn = getMethod.getAnnotation(Column.class);
                
                if (clmnAntn == null)
                {
                    Log.log.warn(getterMthdName + " is missing mandatory @Column annotation.");
                    throw new Exception(getterMthdName + " is missing mandatory @Column annotation.");
                }
                
                if (getMethod.getReturnType().getSimpleName().equals("String") && getMethod.getAnnotation(Lob.class) != null)
                {
                    int clmnLength = (int) clmnAntn.annotationType().getMethod("length", (Class<?>[])null).invoke(clmnAntn, (Object[])null);
                    
                    if (clmnLength != 16777215)
                    {
                        Log.log.warn("@Lob annotated method " + getterMthdName + " @Column annotation length attribute value " + 
                                     clmnLength + " does not match expected 16777215");
                        throw new Exception("@Lob annotated method " + getterMthdName + " @Column annotation length attribute value " + 
                                            clmnLength + " does not match expected 16777215");
                    }
                }
                
                if (!hasLob)
                {
                    if (getMethod.getAnnotation(Lob.class) != null)
                    {
                        hasLob = true;
                    }
                }
            }
        }
        
        if (hasLob)
        {
            try
            {
                gtwFilterModelClass.getDeclaredMethod("ugetCdataProperty", (Class<?>[])null);
            }
            catch (Exception e)
            {
                Log.log.warn("Expected method ugetCdataProperty() returning List<String> not found in " + gtwFilterModelClassName + ".", e);
                throw e;
            }
        }
    }
    
    @SuppressWarnings("unchecked")
    private void validateGtwMsgHandlerImpl(String gtwMsgHandlerClassName) throws Exception
    {
        Class<? extends MGateway> gtwMsgHandlerClass = null;
        
        // Load com.resolve.gateway.M<implprefix> CLass
        
        try
        {
            gtwMsgHandlerClass = 
                (Class<? extends MGateway>) Class.forName(gtwMsgHandlerClassName, false, 
                                                          this.getClass().getClassLoader());
        }
        catch(Exception e)
        {
            Log.log.warn("Failed to load " + gtwMsgHandlerClassName + ".", e);
            throw e;
        }
        
        // Methods getSetFilterMethodName(), populateGatewaySpecificFilterProperties(Map<String, String>, Filter)
        
        try
        {
            gtwMsgHandlerClass.getDeclaredMethod("getSetFilterMethodName", (Class<?>[])null);
        }
        catch (Exception e)
        {
            Log.log.warn("Expected method getSetFilterMethodName() returning String not found in " + gtwMsgHandlerClassName + ".", e);
            throw e;
        }
        
        try
        {
            gtwMsgHandlerClass.getDeclaredMethod("populateGatewaySpecificFilterProperties", Map.class, Filter.class);
        }
        catch (Exception e)
        {
            Log.log.warn("Expected method populateGatewaySpecificFilterProperties(Map<String, String>, Filter) not found in " + gtwMsgHandlerClassName + ".", e);
            throw e;
        }
    }
    
    @SuppressWarnings("unchecked")
    private void validateGtwRSCtrlMsgHandlerImpl(String gtwRSCtrlMsgHandlerClassName) throws Exception
    {
        Class<? extends com.resolve.rscontrol.MGateway> gtwRSCtrlMsgHandlerClass = null;
        
        // Load com.resolve.rscontrol.M<implprefix> CLass
        
        try
        {
            gtwRSCtrlMsgHandlerClass = 
                (Class<? extends com.resolve.rscontrol.MGateway>) Class.forName(gtwRSCtrlMsgHandlerClassName, false, 
                                                                                this.getClass().getClassLoader());
        }
        catch(Exception e)
        {
            Log.log.warn("Failed to load " + gtwRSCtrlMsgHandlerClassName + ".", e);
            throw e;
        }
        
        // Methods getModelName()
        
        try
        {
            gtwRSCtrlMsgHandlerClass.getDeclaredMethod("getModelName", (Class<?>[])null);
        }
        catch (Exception e)
        {
            Log.log.warn("Expected method getModelName() returning String not found in " + gtwRSCtrlMsgHandlerClassName + ".", e);
            throw e;
        }
    }
    
    @SuppressWarnings("unchecked")
    private void validateGtwImpl(String gtwClassName, String implPrefix, String implPackage) throws Exception
    {
        Class<? extends BaseClusteredGateway> gtwClass = null;
        
        // Load <package>.<implprefix>Gateway CLass
        
        try
        {
            gtwClass = 
                (Class<? extends BaseClusteredGateway>) Class.forName(gtwClassName, false, 
                                                                      this.getClass().getClassLoader());
        }
        catch(Exception e)
        {
            Log.log.warn("Failed to load " + gtwClassName + ".", e);
            throw e;
        }
        
        // Method Validation
        
        // getInstance(<implPackage>.ConfigReceive<implPrefix>)
        
        try
        {
            gtwClass.getDeclaredMethod("getInstance", Class.forName(implPackage + ".ConfigReceive" + implPrefix));
        }
        catch (Exception e)
        {
            Log.log.warn("Expected method getInstance(" + (implPackage + ".ConfigReceive" + implPrefix) + 
                         ") returning " + gtwClassName + " not found in " + gtwClassName + ".", e);
            throw e;
        }
        
        // getInstance()
        
        try
        {
            gtwClass.getDeclaredMethod("getInstance", (Class<?>[])null);
        }
        catch (Exception e)
        {
            Log.log.warn("Expected method getInstance() returning " + gtwClassName + " not found in " + gtwClassName + ".", e);
            throw e;
        }
        
        // getLicenseCode()
        
        try
        {
            gtwClass.getDeclaredMethod("getLicenseCode", (Class<?>[])null);
        }
        catch (Exception e)
        {
            Log.log.warn("Expected method getLicenseCode() returning String not found in " + gtwClassName + ".", e);
            throw e;
        }
        
        // getGatewayEventType()
        
        try
        {
            gtwClass.getDeclaredMethod("getGatewayEventType", (Class<?>[])null);
        }
        catch (Exception e)
        {
            Log.log.warn("Expected method getGatewayEventType() returning String not found in " + gtwClassName + ".", e);
            throw e;
        }
        
        // getMessageHandlerName()
        
        try
        {
            gtwClass.getDeclaredMethod("getMessageHandlerName", (Class<?>[])null);
        }
        catch (Exception e)
        {
            Log.log.warn("Expected method getMessageHandlerName() returning String not found in " + gtwClassName + ".", e);
            throw e;
        }
        
        // getMessageHandlerClass()
        
        try
        {
            gtwClass.getDeclaredMethod("getMessageHandlerClass", (Class<?>[])null);
        }
        catch (Exception e)
        {
            Log.log.warn("Expected method getMessageHandlerClass() returning M" + implPrefix + " not found in " + gtwClassName + ".", e);
            throw e;
        }
        
        // getQueueName()
        
        try
        {
            gtwClass.getDeclaredMethod("getQueueName", (Class<?>[])null);
        }
        catch (Exception e)
        {
            Log.log.warn("Expected method getQueueName() returning String not found in " + gtwClassName + ".", e);
            throw e;
        }
        
        // initialize()
        
        try
        {
            gtwClass.getDeclaredMethod("initialize", (Class<?>[])null);
        }
        catch (Exception e)
        {
            Log.log.warn("Expected method initialize not found in " + gtwClassName + ".", e);
            throw e;
        }
        
        // getFilter()
        
        try
        {
            gtwClass.getDeclaredMethod("getFilter", Map.class);
        }
        catch (Exception e)
        {
            Log.log.warn("Expected method getFilter(Map<String, Object>) not found in " + gtwClassName + ".", e);
            throw e;
        }
        
        // start()
        
        try
        {
            gtwClass.getDeclaredMethod("start", (Class<?>[])null);
        }
        catch (Exception e)
        {
            Log.log.warn("Expected method start() not found in " + gtwClassName + ".", e);
            throw e;
        }
        
        // run()
        
        try
        {
            gtwClass.getDeclaredMethod("run", (Class<?>[])null);
        }
        catch (Exception e)
        {
            Log.log.warn("Expected method run() not found in " + gtwClassName + ".", e);
            throw e;
        }
        
        // cleanFilter(ConcurrentHashMap<String, Filter>) - Optional
        
        try
        {
            gtwClass.getDeclaredMethod("cleanFilter", ConcurrentHashMap.class);
            Log.log.info("Optional method cleanFilter() found in " + gtwClassName + ".");
        }
        catch (Exception e)
        {
            ;
        }
        
        // addAdditionalValues(Map<String, Object>, Map<String, String>)
        
        try
        {
            gtwClass.getDeclaredMethod("addAdditionalValues", Map.class, Map.class);
        }
        catch (Exception e)
        {
            Log.log.warn("Expected method addAdditionalValues(Map<String, Object>, Map<String, String>) not found in " + gtwClassName + ".", e);
            throw e;
        }
        
        // addAdditionalValues(Map<String, Object>, Map<String, String>, Filter) - Optional
        
        try
        {
            gtwClass.getDeclaredMethod("addAdditionalValues", Map.class, Map.class, Filter.class);
            Log.log.info("Optional method addAdditionalValues(Map<String, Object>, Map<String, String>, Filter) found in " + gtwClassName + ".");
        }
        catch (Exception e)
        {
            ;
        }
        
        // requireLicense()
        
        try
        {
            gtwClass.getDeclaredMethod("requireLicense", (Class<?>[])null);
            Log.log.info("Optional method requireLicense() found in " + gtwClassName + ".");
        }
        catch (Exception e)
        {
            ;
        }
    }
    
    private void validateGtwAPIImpl(String gtwAPIImplClassName) throws Exception
    {
        try
        {
            Class.forName(gtwAPIImplClassName, false, this.getClass().getClassLoader());
        }
        catch(Exception e)
        {
            Log.log.warn("Failed to load " + gtwAPIImplClassName + ".", e);
            throw e;
        }
    }
    
    private void validatedateSDKDvlpdGtwImpl(String implPrefix, String implPackage) throws Exception
    {
        validateConfigReceiveImpl(implPackage + ".ConfigReceive" + implPrefix);
        Map<String, Class<?>> getterToRtnType = validateFilterVOImpl("com.resolve.services.hibernate.vo." + implPrefix + "FilterVO");
        validateFilterModelImpl("com.resolve.persistence.model." + implPrefix + "Filter", 
                                "com.resolve.services.hibernate.vo." + implPrefix + "FilterVO",
                                getterToRtnType);
        validateGtwMsgHandlerImpl("com.resolve.gateway." + "M" + implPrefix);
        validateGtwRSCtrlMsgHandlerImpl("com.resolve.rscontrol." + "M" + implPrefix);
        validateGtwImpl(implPackage + "." + implPrefix + "Gateway", implPrefix, implPackage);
        validateGtwAPIImpl(implPackage + "." + implPrefix + "API");
    }
    
    @SuppressWarnings("unchecked")
    private ConfigReceiveGateway loadSDKDevelopedGateway(String gtwClassName, XDoc configDoc) throws Exception
    {
        Class<? extends ConfigReceiveGateway> cfgReceiveGtwClass = 
            (Class<? extends ConfigReceiveGateway>) Class.forName(gtwClassName, false, this.getClass().getClassLoader());
        
        
        Constructor<? extends ConfigReceiveGateway> cfgReceiveGtwConstructor = cfgReceiveGtwClass.getDeclaredConstructor(configDoc.getClass());
        
        ConfigReceiveGateway cfgReceiveGtwInst = cfgReceiveGtwConstructor.newInstance(configDoc);
        cfgReceiveGtwInst.load();
        
        return cfgReceiveGtwInst;
    }
    
    private void loadSDKDevelopedGateways(XDoc configDoc) throws Exception
    {
        sdkDvlpdImplPrefxToPkg = getSDKDevelopedGateways(configDoc);
        
        for (String implPrefix : sdkDvlpdImplPrefxToPkg.keySet())
        {
            String packageName = sdkDvlpdImplPrefxToPkg.get(implPrefix);
            
            if(packageName != null) {
                Node node = configDoc.getNode("/CONFIGURATION/RECEIVE/" + implPrefix.toUpperCase());
                if(packageName.indexOf(".push.") != -1) {
                    ConfigReceivePushGateway config = new ConfigReceivePushGateway(configDoc, implPrefix);
                    config.load();
                
                    config.setRootNode("./RECEIVE/" + implPrefix.toUpperCase());
    
                    config.loadPushGatewayProperties(implPrefix, node.asXML());
                    
                    PushGateway.getInstance(config);
                    
                    sdkdDvlpdCfgRcvs.put(implPrefix, config);
                }
                
                else if(packageName.indexOf(".pull.") != -1) {
                    ConfigReceivePullGateway config = new ConfigReceivePullGateway(configDoc, implPrefix);
                    config.load();
                
                    config.setRootNode("./RECEIVE/" + implPrefix.toUpperCase());
    
                    config.loadPullGatewayProperties(implPrefix, node.asXML());
                    
                    PullGateway.getInstance(config);
                    
                    sdkdDvlpdCfgRcvs.put(implPrefix, config);
                }
                
                else if(packageName.indexOf(".msg.") != -1) {
                    ConfigReceiveMSGGateway config = new ConfigReceiveMSGGateway(configDoc, implPrefix);
                    config.load();
                
                    config.setRootNode("./RECEIVE/" + implPrefix.toUpperCase());
    
                    config.loadMSGGatewayProperties(implPrefix, node.asXML());
                    
                    MSGGateway.getInstance(config);
                    
                    sdkdDvlpdCfgRcvs.put(implPrefix, config);
                }
            
                else {
                    try
                    {
                        validatedateSDKDvlpdGtwImpl(implPrefix, sdkDvlpdImplPrefxToPkg.get(implPrefix));
                        
                        try
                        {
                            ConfigReceiveGateway cfgReceiveGtwInst = loadSDKDevelopedGateway(sdkDvlpdImplPrefxToPkg.get(implPrefix) + ".ConfigReceive" + implPrefix, configDoc);
                            sdkdDvlpdCfgRcvs.put(implPrefix, cfgReceiveGtwInst);
                        }
                        catch (Exception e)
                        {
                            Log.log.warn("Error loading " + sdkDvlpdImplPrefxToPkg.get(implPrefix) + ".ConfigReceive" + implPrefix, e);
                        }
                    }
                    catch (Exception ve)
                    {
                        Log.log.error(implPrefix+"Gateway: Failed to Validate SDK Developed " + sdkDvlpdImplPrefxToPkg.get(implPrefix) + "." + implPrefix + " Gateway Implementation." , ve);
                    }
                }
            }
        }
    }
    
    protected void loadConfig(XDoc configDoc) throws Exception
    {
        // override and additional configX.load
        configGeneral = new ConfigGeneral(configDoc);
        configGeneral.load();
        setClusterModeProperty(configGeneral.getClusterName() + "." + Constants.CLUSTER_MODE);

        configRegistration = new ConfigRegistration(configDoc);
        configRegistration.load();

        // load SNMP listener
        configReceiveSNMP = new ConfigReceiveSNMP(configDoc);
        configReceiveSNMP.load();

        // load Netcool listener
        configReceiveNetcool = new ConfigReceiveNetcool(configDoc);
        configReceiveNetcool.load();

        // load Exchange listener
        configReceiveExchange = new ConfigReceiveExchange(configDoc);
        configReceiveExchange.load();

        // load Remedy listener
        configReceiveRemedy = new ConfigReceiveRemedy(configDoc);
        configReceiveRemedy.load();

        // load Remedy listener
        configReceiveDB = new ConfigReceiveDB(configDoc);
        configReceiveDB.load();

        // load Email listener
        configReceiveEmail = new ConfigReceiveEmail(configDoc);
        configReceiveEmail.load();

        configReceiveTSRM = new ConfigReceiveTSRM(configDoc);
        configReceiveTSRM.load();

        configReceiveITM = new ConfigReceiveITM(configDoc);
        configReceiveITM.load();

        configReceiveXMPP = new ConfigReceiveXMPP(configDoc);
        configReceiveXMPP.load();

        configReceiveServiceNow = new ConfigReceiveServiceNow(configDoc);
        configReceiveServiceNow.load();

        configReceiveSalesforce = new ConfigReceiveSalesforce(configDoc);
        configReceiveSalesforce.load();

        configReceiveSSH = new ConfigReceiveSSH(configDoc);
        configReceiveSSH.load();

        configReceiveRemedyx = new ConfigReceiveRemedyx(configDoc);
        configReceiveRemedyx.load();

        configReceiveHPOM = new ConfigReceiveHPOM(configDoc);
        configReceiveHPOM.load();

        configReceiveTelnet = new ConfigReceiveTelnet(configDoc);
        configReceiveTelnet.load();

        configReceiveEWS = new ConfigReceiveEWS(configDoc);
        configReceiveEWS.load();

        configReceiveLDAP = new ConfigReceiveLDAP(configDoc);
        configReceiveLDAP.load();

        configReceiveAD = new ConfigReceiveAD(configDoc);
        configReceiveAD.load();

        configReceiveHPSM = new ConfigReceiveHPSM(configDoc);
        configReceiveHPSM.load();

        configReceiveAmqp = new ConfigReceiveAmqp(configDoc);
        configReceiveAmqp.load();

        configReceiveTCP = new ConfigReceiveTCP(configDoc);
        configReceiveTCP.load();

        configReceiveHTTP = new ConfigReceiveHTTP(configDoc);
        configReceiveHTTP.load();

        configReceiveTIBCOBespoke = new ConfigReceiveTIBCOBespoke(configDoc);
        configReceiveTIBCOBespoke.load();
        
        configReceiveCASpectrum = new ConfigReceiveCASpectrum(configDoc);
        configReceiveCASpectrum.load();
        
        configSelfCheck = new ConfigSelfCheck(configDoc);
        configSelfCheck.load();
        
        // Load sdk developed gateways
        
        loadSDKDevelopedGateways(configDoc);
        
        super.loadConfig(configDoc);
    } // loadConfig

    private void saveSDKDevelopedGateways() throws Exception
    {
        for (ConfigReceiveGateway sdkdDvlpdCfgRcv : sdkdDvlpdCfgRcvs.values())
        {
            sdkdDvlpdCfgRcv.save();
        }
    }
    
    @Override
    protected void saveConfig() throws Exception
    {
        // save config
        configReceiveSNMP.save();
        configReceiveNetcool.save();
        configReceiveExchange.save();
        configReceiveRemedy.save();
        configReceiveDB.save();
        configReceiveEmail.save();
        configESB.save();
        configRegistration.save();
        configReceiveTSRM.save();
        configReceiveITM.save();
        configReceiveXMPP.save();
        configReceiveServiceNow.save();
        configReceiveSalesforce.save();
        configReceiveSSH.save();
        configReceiveRemedyx.save();
        configReceiveHPOM.save();
        configReceiveTelnet.save();
        configReceiveEWS.save();
        configReceiveLDAP.save();
        configReceiveAD.save();
        configReceiveHPSM.save();
        configReceiveAmqp.save();
        configReceiveTCP.save();
        configReceiveHTTP.save();
        configReceiveTIBCOBespoke.save();
        configReceiveCASpectrum.save();
        
        configSelfCheck.save();

        // Save SDK developed gateway configurations
        
        saveSDKDevelopedGateways();
        
        super.saveConfig();
    } // saveConfig

    protected void loadESB(XDoc configDoc) throws Exception
    {
        // Using rsremote's own ConfigESB
        configESB = new ConfigESB(configDoc);
        configESB.load();
    }

    @Override
    protected void initESB() throws Exception
    {
        super.initESB();
    } // initESB

    @Override
    protected void startESB() throws Exception
    {
        super.startESB();
        
		if (Log.isDcsLoggingEnabled()) {
			try {
				
				boolean success = registerDCSClient(Log.getDcsClientUsername(), Log.getDcsClientPassword());
				Log.log.info((success ? "SUCCESSFUL" : "UNSUCCESSFUL") + " Client Registration send to RabbitMq.");
			} catch (Exception ex) {
				Log.log.info("Error while registering DCS client. " + ex.getMessage());
			}

        }
        
        // add default subscription
        mServer.subscribePublication(Constants.ESB_NAME_BROADCAST);
        mServer.subscribePublication(Constants.ESB_NAME_RSREMOTES);

        initNamedQueues();

        //creates the queue if it does not exisis
        mServer.createQueue(Constants.ESB_NAME_EXECUTEQUEUE);
        mServer.createDurableQueue(Constants.ESB_NAME_DURABLE_EXECUTEQUEUE);

        Log.log.info("  Initializing ESB Listener for the queue: " + MainBase.main.configId.getGuid());
        MListener mListener = mServer.createListener(MainBase.main.configId.getGuid(), "com.resolve.rsremote");
        mListener.init(false);
    }

    protected void initNamedQueues() throws Exception
    {
        com.resolve.rsremote.ConfigESB rmtESB = (com.resolve.rsremote.ConfigESB) configESB;
        String orgName = ((com.resolve.rsremote.ConfigGeneral)configGeneral).getOrgSuffix();
        
        if (rmtESB.namedQueues.size() > 0)
        {
            for (ConfigESB.NamedQueue queue : rmtESB.namedQueues)
            {
                if (!StringUtils.isEmpty(queue.name))
                {
                    String queueNameWithOrgSuffix = queue.name.equals(Constants.ESB_NAME_RSREMOTE) ? queue.name + orgName : queue.name;
                    
                    MListener mListener = mServer.createListener(queueNameWithOrgSuffix, "com.resolve.rsremote");
                    mListener.init(false);
                    namedQueueListeners.add(mListener);
                    Log.log.info("Added a new listener for queue " + queueNameWithOrgSuffix);
                }
            }
        }
    } // initNamedQueues

    @Override
    public void initMetric() throws Exception
    {
        metric = new Metric();
        
        super.initMetric();
        //BaseMetric.requestThresholdUpdate(300);
    } // initMetric

    private String retrieveGatewayData(Object gtwInstanceObj, String methodName) {
    	String gtwRetrievedData = null;
    	
    	try {
			Method gtwRetrieveDataMethod = gtwInstanceObj.getClass().getMethod(methodName, (Class<?>[])null);
			
			if (gtwRetrieveDataMethod != null) {
				try {
					gtwRetrievedData = (String) gtwRetrieveDataMethod.invoke(gtwInstanceObj, (Object[])null);
				} catch (IllegalAccessException | IllegalArgumentException | InvocationTargetException e) {
					Log.log.warn(String.format("Error %sin retrieving data using method %s from %s", 
							   				   (StringUtils.isNotBlank(e.getMessage()) ? e.getMessage() : ""), 
							   				   gtwRetrieveDataMethod.getName(), gtwInstanceObj.getClass().getSimpleName()));
				}
			}
		} catch (NoSuchMethodException | SecurityException e) {
			Log.log.warn(String.format("Error %sin getting method %s to retrieve data from %s", 
									   (StringUtils.isNotBlank(e.getMessage()) ? e.getMessage() : ""), methodName,
									   gtwInstanceObj.getClass().getSimpleName()));
		}
    	
    	return gtwRetrievedData;
    }
    
    @SuppressWarnings("unchecked")
    private Object startStopSDKDvlpdGtwRcvr(String implPrefix, String gtwClassName, boolean start, 
    										Map<String, String> gtwQName2MsgHandlerNames) throws Exception
    {
        Class<? extends BaseClusteredGateway> sdkDvlpdGtwClass = 
                        (Class<? extends BaseClusteredGateway>) Class.forName(gtwClassName, false, this.getClass().getClassLoader());
        
        
        Method gtwGetInstanceMethod = sdkDvlpdGtwClass.getDeclaredMethod("getInstance", sdkdDvlpdCfgRcvs.get(implPrefix).getClass());
                    
        Object gtwInstanceObj = gtwGetInstanceMethod.invoke(null, sdkdDvlpdCfgRcvs.get(implPrefix));
        
        Method gtwStartStopMethod = gtwInstanceObj.getClass().getMethod((start ? "start" : "stop"), (Class<?>[])null);
        
        gtwStartStopMethod.invoke(gtwInstanceObj, (Object[])null);
        
        // Retrieve queue name & message handler names if start
        
        if (start && gtwQName2MsgHandlerNames != null) {
        	String qName = retrieveGatewayData(gtwInstanceObj, Gateway.GET_QUEUE_NAME_METHOD_NAME);
        	String msgHandlerName = retrieveGatewayData(gtwInstanceObj, BaseGateway.GET_MESSAGE_HANDLER_METHOD_NAME);
        	saveGatewayInstance(gtwInstanceObj);
        	if (StringUtils.isNotBlank(qName) && StringUtils.isNotBlank(msgHandlerName)) {
        		gtwQName2MsgHandlerNames.put(qName.toUpperCase(), msgHandlerName);
        	}
        }
        
        return gtwInstanceObj;
    }
    
    private void startStopSDKDvlpdGtwRcvrs(boolean start, Map<String, String> gtwQName2MsgHandlerNames)
    {
        for (String implPrefix : sdkDvlpdImplPrefxToPkg.keySet())
        {
            // Start/Stop SDK Developed Gateway only if it is Active
            if (!sdkdDvlpdCfgRcvs.containsKey(implPrefix))
                continue;
            
            ConfigReceiveGateway config = (ConfigReceiveGateway)sdkdDvlpdCfgRcvs.get(implPrefix);
            String org = ((com.resolve.rsremote.ConfigGeneral)Main.main.configGeneral).getOrgSuffix();
            
            if (config != null && config.isActive())
            {
                try
                {
                    if(config.getClass().getName().indexOf(".push.") != -1)
                        ((ConfigReceivePushGateway)config).loadPushGatewayFilters(implPrefix, config.getQueue() + org);
                    else if(config.getClass().getName().indexOf(".pull.") != -1)
                        ((ConfigReceivePullGateway)config).loadPullGatewayFilters(implPrefix, config.getQueue() + org);
                    else if(config.getClass().getName().indexOf(".msg.") != -1)
                        ((ConfigReceiveMSGGateway)config).loadMSGGatewayFilters(implPrefix, config.getQueue() + org);
                    
                    Object gateway = startStopSDKDvlpdGtwRcvr(implPrefix, 
                    										  String.format("%s.%sGateway", 
                    												  		sdkDvlpdImplPrefxToPkg.get(implPrefix), 
                    												  		implPrefix), start, 
                    										  				gtwQName2MsgHandlerNames);
                    
                    Log.log.warn((start ? "Started " : "Stopped ") + sdkDvlpdImplPrefxToPkg.get(implPrefix) + "." + 
                                 implPrefix + "Gateway" );
                    
                    if(config.getClass().getName().indexOf(".push.") != -1)
                        ConfigReceivePushGateway.getGateways().put(implPrefix, (PushGateway)gateway);
                    else if(config.getClass().getName().indexOf(".pull.") != -1)
                        ConfigReceivePullGateway.getGateways().put(implPrefix, (PullGateway)gateway);
                    else if(config.getClass().getName().indexOf(".msg.") != -1)
                        ConfigReceiveMSGGateway.getGateways().put(implPrefix, (MSGGateway)gateway);
                }
                catch (Throwable t)
                {
                    Log.log.warn("Error " + (start ? "starting " : "stopping ") + sdkDvlpdImplPrefxToPkg.get(implPrefix) + "." + 
                                 implPrefix + "Gateway" + (start ? ", check blueprint configuration" : ""), t);
                    
                    if (start)
                    {
                        
                        sdkdDvlpdCfgRcvs.get(implPrefix).setActive(false);
                    }
                }
            }
        }
    }
    
	private void saveFilterCompanionModel(BaseGateway gateway) {
    	Class<?> msgHandlerClass = gateway.getMessageHandlerClassPublic();
    	List<Method> fcmAnnotatedMethods = 
    					MethodUtils.getMethodsListWithAnnotation(msgHandlerClass, 
    															 (Class<? extends Annotation>) FilterCompanionModel.class, 
    															 false, false);
    	
    	if (CollectionUtils.isNotEmpty(fcmAnnotatedMethods)) {
    		fcmAnnotatedMethods.stream().forEach(fcmAnnotatedMethod -> {
    			Annotation annotation = fcmAnnotatedMethod.getAnnotation(FilterCompanionModel.class);
    			
    			if (annotation != null) {
    				FilterCompanionModel fcm = (FilterCompanionModel)annotation;
    				
    				Pair<String, Map<String, String>> fcmDetails = fcmName2Details.get(fcm.modelName()); 
    				
    				if (fcmDetails == null) {
    					fcmDetails = Pair.of(msgHandlerClass.getSimpleName(), new HashMap<String, String>());
    					fcmName2Details.put(fcm.modelName(), fcmDetails);
    				}
    				
    				if (fcm.isClearAndSetMethod()) {
    					fcmDetails.getRight().put(Constants.FILTER_OPERATION_CLEARANDSET, fcmAnnotatedMethod.getName());
    				}
    				
    				if (fcm.isdeployMethod()) {
    					fcmDetails.getRight().put(Constants.FILTER_OPERATION_DEPLOY, fcmAnnotatedMethod.getName());
    				}
    				
    				if (fcm.isUndeployMethod()) {
    					fcmDetails.getRight().put(Constants.FILTER_OPERATION_UNDEPLOY, fcmAnnotatedMethod.getName());
    				}
    			}
    		});
    	}
    }
    
    protected void setMessageHandlerName(Map<String, String> gtwQName2MsgHandlerNames, 
    									 Gateway gateway) {
    	if (gtwQName2MsgHandlerNames != null && gateway != null && 
    		gateway instanceof BaseGateway &&
    		StringUtils.isNotBlank(gateway.getQueueName()) && 
            StringUtils.isNotBlank(((BaseGateway)gateway).getMessageHandlerNamePublic())) {
    		gtwQName2MsgHandlerNames.put(gateway.getQueueName(), ((BaseGateway)gateway).getMessageHandlerNamePublic());
    		saveGatewayInstance(gateway);
    		saveFilterCompanionModel((BaseGateway) gateway);
    	}
    }
    
    protected void startReceivers() throws Exception
    {
    	Map<String, String> gtwQName2MsgHandlerNames = new HashMap<String, String>();
    	
        // Initialize message dispatcher before starting gateways
        
        MessageDispatcher.init();
        
        // SNMP Trap Listener
        if (configReceiveSNMP.isActive())
        {
            //            SNMPTrapListener listener = new SNMPTrapListener(configReceiveSNMP.getPort());
            //            listener.start();

            SNMPGateway snmp = SNMPGateway.getInstance(configReceiveSNMP);
            setMessageHandlerName(gtwQName2MsgHandlerNames, snmp);
            snmp.start();
        }

        // Netcool Listener
        if (configReceiveNetcool.isActive())
        {
            try
            {
                NetcoolGateway netcool = NetcoolGateway.getInstance(configReceiveNetcool);
                setMessageHandlerName(gtwQName2MsgHandlerNames, netcool);
                netcool.start();
            }
            catch (Throwable e)
            {
                configReceiveNetcool.setActive(false);
                Log.log.warn("Could not start netcool gateway, check the blueprint configuration.", e);
            }
        }

        // Exchange Listener
        if (configReceiveExchange.isActive())
        {
            try
            {
                ExchangeGateway exchange = ExchangeGateway.getInstance(configReceiveExchange);
                setMessageHandlerName(gtwQName2MsgHandlerNames, exchange);
                // start gateway
                exchange.start();
            }
            catch (Throwable e)
            {
                configReceiveExchange.setActive(false);
                Log.log.warn("Could not start exchange gateway, check the blueprint configuration.", e);
            }
        }

        // Email Listener
        if (configReceiveEmail.isActive())
        {
            try
            {
                EmailGateway email = EmailGateway.getInstance(configReceiveEmail);
                setMessageHandlerName(gtwQName2MsgHandlerNames, email);
                email.start();
            }
            catch (Throwable e)
            {
                configReceiveEmail.setActive(false);
                Log.log.warn("Could not start Email gateway, check the blueprint configuration.", e);
            }
        }

        if (configReceiveRemedy.isActive())
        {
            try
            {
                RemedyGateway.init(configReceiveRemedy);
            }
            catch (Throwable e)
            {
                configReceiveRemedy.setActive(false);
                Log.log.warn("Could not start Remedy gateway, check the blueprint configuration.", e);
            }
        }

        if (configReceiveRemedyx.isActive())
        {
            try
            {
                com.resolve.gateway.remedyx.RemedyxGateway remedyGateway = 
                		com.resolve.gateway.remedyx.RemedyxGateway.getInstance(configReceiveRemedyx);
                setMessageHandlerName(gtwQName2MsgHandlerNames, remedyGateway);
                remedyGateway.start();
            }
            catch (Throwable e)
            {
                configReceiveRemedyx.setActive(false);
                Log.log.warn("Could not start Remedyx gateway, check the blueprint configuration.", e);
            }
        }

        // Database gateway Listener
        if (configReceiveDB.isActive())
        {
            try
            {
                DBGateway db = DBGatewayFactory.getInstance(configReceiveDB);
                setMessageHandlerName(gtwQName2MsgHandlerNames, db);
                db.start();
            }
            catch (Throwable e)
            {
                configReceiveDB.setActive(false);
                Log.log.warn("Could not start Database(DB) gateway, check the blueprint configuration.", e);
            }
        }

        if (configReceiveTSRM.isActive())
        {
            try
            {
                TSRMGateway tsrm = TSRMGateway.getInstance(configReceiveTSRM);
                setMessageHandlerName(gtwQName2MsgHandlerNames, tsrm);
                tsrm.start();
            }
            catch (Throwable e)
            {
                configReceiveTSRM.setActive(false);
                Log.log.warn("Could not start TSRM gateway, check the blueprint configuration.", e);
            }
        }

        if (configReceiveITM.isActive())
        {
            try
            {
                ITMGateway.init(configReceiveITM);
            }
            catch (Throwable e)
            {
                configReceiveITM.setActive(false);
                Log.log.warn("Could not start ITM gateway, check the blueprint configuration.", e);
            }
        }

        if (configReceiveXMPP.isActive())
        {
            try
            {
                XMPPGateway xmpp = XMPPGateway.getInstance(configReceiveXMPP);
                setMessageHandlerName(gtwQName2MsgHandlerNames, xmpp);
                xmpp.start();
            }
            catch (Throwable e)
            {
                configReceiveXMPP.setActive(false);
                Log.log.warn("Could not start XMPP gateway, check the blueprint configuration.", e);
            }
        }

        if (configReceiveServiceNow.isActive())
        {
            try
            {
                ServiceNowGateway serviceNowGateway = ServiceNowGateway.getInstance(configReceiveServiceNow);
                setMessageHandlerName(gtwQName2MsgHandlerNames, serviceNowGateway);
                serviceNowGateway.start();
            }
            catch (Throwable e)
            {
                configReceiveServiceNow.setActive(false);
                Log.log.warn("Could not start ServiceNow gateway, check the blueprint configuration.", e);
            }
        }

        if (configReceiveSalesforce.isActive())
        {
            try
            {
                SalesforceGateway salesforceGateway = SalesforceGateway.getInstance(configReceiveSalesforce);
                setMessageHandlerName(gtwQName2MsgHandlerNames, salesforceGateway);
                salesforceGateway.start();
            }
            catch (Throwable e)
            {
                configReceiveSalesforce.setActive(false);
                Log.log.warn("Could not start Salesforce gateway, check the blueprint configuration.", e);
            }
        }

        if (configReceiveSSH.isActive())
        {
            try
            {
                SSHGateway sshGateway = SSHGateway.getInstance(configReceiveSSH);
                setMessageHandlerName(gtwQName2MsgHandlerNames, sshGateway);
                sshGateway.start();
            }
            catch (Throwable e)
            {
                configReceiveSSH.setActive(false);
                Log.log.warn("Could not start SSH Pool gateway, check the blueprint configuration.", e);
            }
        }

        if (configReceiveHPOM.isActive())
        {
            try
            {
                HPOMGateway hpomGateway = HPOMGateway.getInstance(configReceiveHPOM);
                setMessageHandlerName(gtwQName2MsgHandlerNames, hpomGateway);
                hpomGateway.start();
            }
            catch (Throwable e)
            {
                configReceiveHPOM.setActive(false);
                Log.log.warn("Could not start HPOM gateway, check the blueprint configuration.", e);
            }
        }

        if (configReceiveTelnet.isActive())
        {
            try
            {
                TelnetGateway telnetGateway = TelnetGateway.getInstance(configReceiveTelnet);
                setMessageHandlerName(gtwQName2MsgHandlerNames, telnetGateway);
                telnetGateway.start();
            }
            catch (Throwable e)
            {
                configReceiveTelnet.setActive(false);
                Log.log.warn("Could not start Telnet Pool gateway, check the blueprint configuration.", e);
            }
        }

        if (configReceiveEWS.isActive())
        {
            try
            {
                EWSGateway ewsGateway = EWSGateway.getInstance(configReceiveEWS);
                setMessageHandlerName(gtwQName2MsgHandlerNames, ewsGateway);
                ewsGateway.start();
            }
            catch (Throwable e)
            {
                configReceiveEWS.setActive(false);
                Log.log.warn("Could not start EWS gateway, check the blueprint configuration.", e);
            }
        }

        //for now simply start the Queue listener
        if (configReceiveLDAP.isActive())
        {
            try
            {
                MListener mListenerLDAP = mServer.createListener(configReceiveLDAP.getQueue(), "com.resolve.rsremote");
                mListenerLDAP.init(false);
                //runningGateways.add(ldapGateway);
            }
            catch (Throwable e)
            {
                configReceiveLDAP.setActive(false);
                Log.log.warn("Could not start LDAP gateway, check the blueprint configuration.", e);
            }
        }

        //for now simply start the Queue listener
        if (configReceiveAD.isActive())
        {
            try
            {
                MListener mListenerAD = mServer.createListener(configReceiveAD.getQueue(), "com.resolve.rsremote");
                mListenerAD.init(false);
                //runningGateways.add(adGateway);
            }
            catch (Throwable e)
            {
                configReceiveAD.setActive(false);
                Log.log.warn("Could not start ActiveDirectory(AD) gateway, check the blueprint configuration.", e);
            }
        }

        if (configReceiveHPSM.isActive())
        {
            try
            {
                HPSMGateway hpsmGateway = HPSMGateway.getInstance(configReceiveHPSM);
                setMessageHandlerName(gtwQName2MsgHandlerNames, hpsmGateway);
                hpsmGateway.start();
            }
            catch (Throwable e)
            {
                configReceiveHPSM.setActive(false);
                Log.log.warn("Could not start HPSM gateway, check the blueprint configuration.", e);
            }
        }

        if (configReceiveAmqp.isActive())
        {
            try
            {
                AmqpGateway amqpGateway = AmqpGateway.getInstance(configReceiveAmqp);
                setMessageHandlerName(gtwQName2MsgHandlerNames, amqpGateway);
                amqpGateway.start();
            }
            catch (Throwable e)
            {
                configReceiveAmqp.setActive(false);
                Log.log.warn("Could not start AMQP gateway, check the blueprint configuration.", e);
            }
        }

        if (configReceiveTCP.isActive())
        {
            try
            {
                TCPGateway tcpGateway = TCPGateway.getInstance(configReceiveTCP);
                setMessageHandlerName(gtwQName2MsgHandlerNames, tcpGateway);
                tcpGateway.start();
            }
            catch (Throwable e)
            {
                configReceiveTCP.setActive(false);
                Log.log.warn("Could not start TCP gateway, check the blueprint configuration.", e);
            }
        }

        if (configReceiveHTTP.isActive())
        {
            try
            {
                HttpGateway httpGateway = HttpGateway.getInstance(configReceiveHTTP);
                setMessageHandlerName(gtwQName2MsgHandlerNames, httpGateway);
                httpGateway.start();
            }
            catch (Throwable e)
            {
                configReceiveHTTP.setActive(false);
                Log.log.warn("Could not start HTTP gateway, check the blueprint configuration.", e);
            }
        }

        if (configReceiveTIBCOBespoke.isActive())
        {
            try
            {
                TIBCOBespokeGateway tibcoGateway = TIBCOBespokeGateway.getInstance(configReceiveTIBCOBespoke);
                setMessageHandlerName(gtwQName2MsgHandlerNames, tibcoGateway);
                tibcoGateway.start();
            }
            catch (Throwable e)
            {
                configReceiveTIBCOBespoke.setActive(false);
                Log.log.warn("Could not start TIBCO gateway, check the blueprint configuration.", e);
            }
        }

        // CA Spectrum
        if (configReceiveCASpectrum != null && configReceiveCASpectrum.isActive())
        {
            try
            {
                CASpectrumGateway caSpectrumGateway = CASpectrumGateway.getInstance(configReceiveCASpectrum);
                setMessageHandlerName(gtwQName2MsgHandlerNames, caSpectrumGateway);
                caSpectrumGateway.start();
            }
            catch (Throwable e)
            {
                configReceiveCASpectrum.setActive(false);
                Log.log.warn("Could not start CASpectrum gateway, check the blueprint configuration.", e);
            }
        }
        
        // Start sdk developed gateway receivers
        
        startStopSDKDvlpdGtwRcvrs(true, gtwQName2MsgHandlerNames);
        
        // Set gateway queue name to message handler names map in MRegister 
        if (MapUtils.isNotEmpty(gtwQName2MsgHandlerNames)) {
        	MRegister.gtwQName2MsgHandlerNames.putAll(gtwQName2MsgHandlerNames);
        }
    } // startReceivers

    protected void stopReceivers()
    {
        if (configReceiveSNMP.isActive())
        {
            //            SNMPTrapListener listener = new SNMPTrapListener();
            //            try
            //            {
            //                listener.stop();
            //            }
            //            catch (Exception e)
            //            {
            //                Log.log.warn("Could not stop SNMPTrapListener gracefully. " + e.getMessage());
            //            }

            try
            {
                SNMPGateway.getInstance().stop();
            }
            catch (Exception e)
            {
                Log.log.warn("Could not stop SNMP Gateway Listener gracefully. " + e.getMessage());
            }
        }

        if (configReceiveNetcool.isActive())
        {
            NetcoolGateway netcool = NetcoolGateway.getInstance();
            try
            {
                netcool.stop();
            }
            catch (Exception e)
            {
                Log.log.warn("Could not stop Netcool Gateway Listener gracefully. " + e.getMessage());
            }
        }

        if (this.isSocialGatewayShutdown && configReceiveExchange.isActive())
        {
            ExchangeGateway exchange = ExchangeGateway.getInstance(configReceiveExchange);
            try
            {
                exchange.stop();
            }
            catch (Exception e)
            {
                Log.log.warn("Could not stop Exchange Gateway Listener gracefully. " + e.getMessage());
            }
        }

        if (this.isSocialGatewayShutdown && configReceiveEmail.isActive())
        {
            try
            {
                EmailGateway.getInstance().stop();
            }
            catch (Exception e)
            {
                Log.log.warn("Could not stop Email Gateway Listener gracefully. " + e.getMessage());
            }
        }

        if (configReceiveRemedy.isActive())
        {
            try
            {
                RemedyGateway.stopPolling();
            }
            catch (Exception e)
            {
                Log.log.warn("Could not stop Remedy Gateway Listener gracefully. " + e.getMessage());
            }
        }

        if (configReceiveRemedyx.isActive())
        {
            try
            {
                RemedyxGateway.getInstance().stop();
            }
            catch (Exception e)
            {
                Log.log.warn("Could not stop RemedyX Gateway Listener gracefully. " + e.getMessage());
            }
        }

        if (configReceiveDB.isActive())
        {
            try
            {
                DBGatewayFactory.getInstance().stop();
            }
            catch (Exception e)
            {
                Log.log.warn("Could not stop DB Gateway Listener gracefully. " + e.getMessage());
            }
        }

        if (configReceiveTSRM.isActive())
        {
            try
            {
                TSRMGateway.getInstance().stop();
            }
            catch (Exception e)
            {
                Log.log.warn("Could not stop TSRM Gateway Listener gracefully. " + e.getMessage());
            }
        }

        if (configReceiveITM.isActive())
        {
            try
            {
                configReceiveITM.setActive(false);
            }
            catch (Exception e)
            {
                Log.log.warn("Could not stop ITM Gateway Listener gracefully. " + e.getMessage());
            }
        }

        if (this.isSocialGatewayShutdown && configReceiveXMPP.isActive())
        {
            try
            {
                XMPPGateway.getInstance().stop();
            }
            catch (Exception e)
            {
                Log.log.warn("Could not stop XMPP Gateway Listener gracefully. " + e.getMessage());
            }
        }

        if (configReceiveServiceNow.isActive())
        {
            try
            {
                ServiceNowGateway.getInstance().stop();
            }
            catch (Exception e)
            {
                Log.log.warn("Could not stop ServiceNow Gateway Listener gracefully. " + e.getMessage());
            }
        }

        if (configReceiveSSH.isActive())
        {
            try
            {
                SSHGateway.getInstance().stop();
            }
            catch (Exception e)
            {
                Log.log.warn("Could not stop SSH Gateway Listener gracefully. " + e.getMessage());
            }
            
        }

        if (configReceiveHPOM.isActive())
        {
            try
            {
                HPOMGateway.getInstance().stop();
            }
            catch (Exception e)
            {
                Log.log.warn("Could not stop HPOM Gateway Listener gracefully. " + e.getMessage());
            }
        }

        if (configReceiveTelnet.isActive())
        {
            try
            {
                TelnetGateway.getInstance().stop();
            }
            catch (Exception e)
            {
                Log.log.warn("Could not stop Telnet Gateway Listener gracefully. " + e.getMessage());
            }
        }

        if (this.isSocialGatewayShutdown && configReceiveEWS.isActive())
        {
            try
            {
                EWSGateway.getInstance().stop();
            }
            catch (Exception e)
            {
                Log.log.warn("Could not stop EWS Gateway Listener gracefully. " + e.getMessage());
            }
        }
        
        if (configReceiveAmqp.isActive())
        {
            try
            {
                AmqpGateway.getInstance().stop();
            }
            catch (Exception e)
            {
                Log.log.warn("Could not stop AMQP Gateway Listener gracefully. " + e.getMessage());
            }
        }
        
        if (configReceiveTCP.isActive())
        {
            try
            {
                TCPGateway.getInstance().stop();
            }
            catch (Exception e)
            {
                Log.log.warn("Could not stop TCP Gateway Listener gracefully. " + e.getMessage());
            }
        }
        
        if (configReceiveHTTP.isActive())
        {
            try
            {
                HttpGateway.getInstance().stop();
            }
            catch (Exception e)
            {
                Log.log.warn("Could not stop HTTP Gateway Listener gracefully. " + e.getMessage());
            }
        }
        
        if (configReceiveTIBCOBespoke.isActive())
        {
            try
            {
                TIBCOBespokeGateway.getInstance().stop();
            }
            catch (Exception e)
            {
                Log.log.warn("Could not stop TIBCO Bespoke Gateway Listener gracefully. " + e.getMessage());
            }
        }
        
        if (configReceiveCASpectrum != null && configReceiveCASpectrum.isActive())
        {
            try
            {
                CASpectrumGateway.getInstance().stop();
            }
            catch (Exception e)
            {
                Log.log.warn("Could not stop CA Spectrum Gateway Listener gracefully. " + e.getMessage());
            }
        }
        
        // Stop sdk developed gateway receivers
        
        startStopSDKDvlpdGtwRcvrs(false, null);
        
    } // stopReceivers

    protected void startDefaultTasks()
    {
        Log.log.debug("Scheduling default jobs");

        // register
        ScheduledExecutor.getInstance().executeRepeat("REGISTER", MRegister.class, "register", 0, Main.main.configRegistration.getInterval() * 60, TimeUnit.SECONDS);
        if (Main.main.configRegistration.isLogHeartbeat())
        {
            ScheduledExecutor.getInstance().executeRepeat("REGISTER", MRegister.class, "logHeartbeat", 60, Main.main.configRegistration.getInterval() * 60, TimeUnit.SECONDS);
        }

        ScheduledExecutor.getInstance().executeRepeat("BLUEPRINT", MRegister.class, "updateBlueprint", 60, Main.main.configRegistration.getInterval() * 60, TimeUnit.SECONDS);
        
        // config properties
        Map<String, String> params = new HashMap<String, String>();
        params.put("JOBNAME", "CONFIG-PROPERTIES");
        params.put("TYPE", Constants.ACTION_INVOCATION_TYPE_REMOTE);
        params.put("FILENAME", "config/GetProperties.groovy");
        params.put("CLASSMETHOD", "MResult.configResult");
        ScheduledExecutor.getInstance().executeDelayed(com.resolve.rsremote.ExecuteMain.class, "execute", params, 3, TimeUnit.MINUTES);

        // send metrics
        ScheduledExecutor.getInstance().executeRepeat(metric, "sendMetrics", 5, 5, TimeUnit.MINUTES);

        // SystemExecutor.executeRepeat(Metric.class, "sendMetrics", 1, 1,
        // TimeUnit.MINUTES);
    } // startDefaultTasks

    public ConfigReceiveDB getConfigReceiveDB()
    {
        return configReceiveDB;
    }

    public ConfigReceiveSNMP getConfigReceiveSNMP()
    {
        return configReceiveSNMP;
    }

    public ConfigReceiveNetcool getConfigReceiveNetcool()
    {
        return configReceiveNetcool;
    }

    public ConfigReceiveExchange getConfigReceiveExchange()
    {
        return configReceiveExchange;
    }

    public ConfigReceiveRemedy getConfigReceiveRemedy()
    {
        return configReceiveRemedy;
    }

    public ConfigReceiveRemedyx getConfigReceiveRemedyx()
    {
        return configReceiveRemedyx;
    }

    public ConfigReceiveEmail getConfigReceiveEmail()
    {
        return configReceiveEmail;
    }

    public ConfigReceiveTSRM getConfigReceiveTSRM()
    {
        return configReceiveTSRM;
    }

    public ConfigReceiveITM getConfigReceiveITM()
    {
        return configReceiveITM;
    }

    public ConfigReceiveXMPP getConfigReceiveXMPP()
    {
        return configReceiveXMPP;
    }

    public ConfigReceiveServiceNow getConfigReceiveServiceNow()
    {
        return configReceiveServiceNow;
    }

    public ConfigReceiveSalesforce getConfigReceiveSalesforce()
    {
        return configReceiveSalesforce;
    }

    public ConfigReceiveSSH getConfigReceiveSSH()
    {
        return configReceiveSSH;
    }

    public ConfigReceiveHPOM getConfigReceiveHPOM()
    {
        return configReceiveHPOM;
    }

    public ConfigReceiveTelnet getConfigReceiveTelnet()
    {
        return configReceiveTelnet;
    }

    public ConfigReceiveEWS getConfigReceiveEWS()
    {
        return configReceiveEWS;
    }

    public ConfigReceiveLDAP getConfigReceiveLDAP()
    {
        return configReceiveLDAP;
    }

    public ConfigReceiveAD getConfigReceiveAD()
    {
        return configReceiveAD;
    }

    public ConfigReceiveHPSM getConfigReceiveHPSM()
    {
        return configReceiveHPSM;
    }
    
    public ConfigReceiveAmqp getConfigReceiveAmqp()
    {
        return configReceiveAmqp;
    }

    public ConfigReceiveTCP getConfigReceiveTCP()
    {
        return configReceiveTCP;
    }
    
    public ConfigReceiveHTTP getConfigReceiveHTTP()
    {
        return configReceiveHTTP;
    }

    public ConfigReceiveTIBCOBespoke getConfigReceiveCenturyLinkTIBCO()
    {
        return configReceiveTIBCOBespoke;
    }
    
    public void activateCluster(Map<String, String> params)
    {
        //prevent reactivating if it's already active
        if(!isClusterModeActive())
        {
            activateCluster();
            try
            {
                startReceivers();
            }
            catch (Exception e)
            {
                Log.log.error(e.getMessage(), e);
            }
        }
    }

    public void deactivateCluster(Map<String, String> params)
    {
        if(isClusterModeActive())
        {
            deactivateCluster();
            try
            {
                stopReceivers();
            }
            catch (Exception e)
            {
                Log.log.error(e.getMessage(), e);
            }
        }
    }
    
    void startSelfCheckTasks()
    {
        if (configSelfCheck.isPingActive())
        {
        	RSRemoteSelfCheck.timeout = configSelfCheck.getPingTimeout();
            ScheduledExecutor.getInstance().executeRepeat(RSRemoteSelfCheck.class, "pingRSControl", 10, configSelfCheck.getPingInterval(), TimeUnit.SECONDS);
        }
    }
    
    public static Map<String, Pair<String, Map<String, String>>> getFilterCompanionModelDetails() {
        return fcmName2Details;
    }
    
} // Main
