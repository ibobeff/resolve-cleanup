/******************************************************************************
* (C) Copyright 2014
*
* Resolve Systems, LLC
*
* All rights reserved
* This software is distributed under license and may not be used, copied,
* modified, or distributed without the express written permission of
* Resolve Systems, LLC.
*
******************************************************************************/

package com.resolve.rsremote;

import java.io.File;
import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import com.resolve.esb.MListener;
import com.resolve.esb.MMsgContent;
import com.resolve.esb.MMsgHeader;
import com.resolve.esb.MMsgOptions;
import com.resolve.rsbase.MainBase;
import com.resolve.thread.TaskExecutor;
import com.resolve.util.Constants;
import com.resolve.util.Log;
import com.resolve.util.StringUtils;

public class MAction
{
    public void executeRequest(MMsgHeader header, Map msg) throws Exception
    {
        // init options
        MMsgOptions options = header.getOptions();
        MMsgContent content = new MMsgContent(msg);

        // return if shutting down
        if (MainBase.main.isShutdown == true && StringUtils.isBlank(options.getAffinity()))
        {
            Log.log.error("RSRemote is Shutting Down, skipping execution of " + options.getActionName());
            return;
        }
        
        
        // pass-through options
        String actionname = options.getActionName();
        String actionnamespace = options.getActionNamespace();
        String actionsummary = options.getActionSummary();
        String actionhidden = options.getActionHidden();
        String actiontags = options.getActionTags();
        String actionid = options.getActionID();
        String processid = options.getProcessID();
        String wiki = options.getWiki();
        String executeid = options.getExecuteID();
        String parserid = options.getParserID();
        String assessid = options.getAssessID();
        String logResult = options.getLogResult();
        String userid = options.getUserID();
        String problemid = options.getProblemID();
        String reference = options.getReference();
        String address = options.getAddress();
        String target = options.getTarget();
        String affinity = options.getAffinity();
        String resultAddr = options.getResultAddr();

        // cron job params
        String taskname = options.getTaskname();
        String type = options.getType();
        String command = options.getCommand();
        String args = options.getArgs();
        String input = options.getInput();
        String delay = options.getDelay();
        String timeout = options.getTimeout();
        String processTimeout = options.getProcessTimeout();
        String metricId = options.getMetricId();

        // content
        String properties = content.getProperties();
        String debug = content.getDebug();
        String params = content.getParams();
        String flows = content.getFlows();
        String inputs = content.getInputs();

        try
        {
            // init thread processid
            Log.setProcessId(processid);

            Log.log.debug("executeRequest processid: " + processid + " executeid: " + executeid);
            // Log.log.debug("  options: "+properties);

            // get properties parameters
            Map props = (Map) StringUtils.stringToObj(properties);
            type = type.toUpperCase();

            // init execute main
            Map execParams = new HashMap();
            execParams.put("JOBNAME", taskname.toUpperCase());
            execParams.put("TYPE", type);

            if (type.equalsIgnoreCase(Constants.ACTION_INVOCATION_TYPE_OS))
            {
                String cmdline = getCmdline(command, args);
                String execpath = (String) props.get(Constants.EXECUTE_EXECPATH);
                if (execpath == null)
                {
                    execpath = Main.main.configGeneral.getHome();
                }

                execParams.put(Constants.EXECUTE_CMDLINE, cmdline);
                execParams.put(Constants.EXECUTE_INPUT, input);
                execParams.put(Constants.EXECUTE_EXECPATH, execpath);
            }
            else if (type.equalsIgnoreCase(Constants.ACTION_INVOCATION_TYPE_JAVA))
            {
                String classpath = (String) props.get(Constants.EXECUTE_CLASSPATH);
                if (classpath == null)
                {
                    classpath = "";
                }

                String javaoptions = (String) props.get(Constants.EXECUTE_JAVAOPTIONS);
                if (javaoptions == null)
                {
                    javaoptions = "";
                }

                String execpath = (String) props.get(Constants.EXECUTE_EXECPATH);
                if (execpath == null)
                {
                    execpath = Main.main.configGeneral.getHome();
                }

                execParams.put(Constants.EXECUTE_FILENAME, command);
                execParams.put(Constants.EXECUTE_ARGS, args);
                execParams.put(Constants.EXECUTE_INPUT, input);
                execParams.put(Constants.EXECUTE_EXECPATH, execpath);
                execParams.put(Constants.EXECUTE_JREPATH, Main.main.configGeneral.getHome() + "/jdk");
                execParams.put(Constants.EXECUTE_CLASSPATH, classpath);
                execParams.put(Constants.EXECUTE_JAVAOPTIONS, javaoptions);
            }
            else if (type.equalsIgnoreCase(Constants.ACTION_INVOCATION_TYPE_REMOTE))
            {
                execParams.put(Constants.EXECUTE_FILENAME, command);
                execParams.put(Constants.EXECUTE_ARGS, args);
            }
            else if (type.equalsIgnoreCase(Constants.ACTION_INVOCATION_TYPE_BASH))
            {
                String cmdline = StringUtils.escapeQuotesForBash(getCmdline(command, args));
                String execpath = (String) props.get(Constants.EXECUTE_EXECPATH);
                if (execpath == null)
                {
                    execpath = Main.main.configGeneral.getHome();
                }

                execParams.put(Constants.EXECUTE_CMDLINE, "/bin/bash -c \"" + cmdline + "\"");
                execParams.put(Constants.EXECUTE_INPUT, input);
                execParams.put(Constants.EXECUTE_EXECPATH, execpath);
            }
            else if (type.equalsIgnoreCase(Constants.ACTION_INVOCATION_TYPE_CMD))
            {
                String cmdline = getCmdline(command, args);
                String execpath = (String) props.get(Constants.EXECUTE_EXECPATH);
                if (execpath == null)
                {
                    execpath = Main.main.configGeneral.getHome();
                }

                execParams.put(Constants.EXECUTE_CMDLINE, "cmd /c " + cmdline);
                execParams.put(Constants.EXECUTE_INPUT, input);
                execParams.put(Constants.EXECUTE_EXECPATH, execpath);
            }
            else if (type.equalsIgnoreCase(Constants.ACTION_INVOCATION_TYPE_CSCRIPT) || type.equalsIgnoreCase(Constants.ACTION_INVOCATION_TYPE_POWERSHELL) || type.equalsIgnoreCase(Constants.ACTION_INVOCATION_TYPE_WINEXEC) || type.equalsIgnoreCase(Constants.ACTION_INVOCATION_TYPE_WINTASK))
            {
                String cmdline = getCmdline(command, args);
                String execpath = (String) props.get(Constants.EXECUTE_EXECPATH);
                if (execpath == null)
                {
                    execpath = Main.main.configGeneral.getHome();
                }

                execParams.put(Constants.EXECUTE_CMDLINE, cmdline);
                execParams.put(Constants.EXECUTE_INPUT, input);
                execParams.put(Constants.EXECUTE_EXECPATH, execpath);
            }
            else if (type.equalsIgnoreCase(Constants.ACTION_INVOCATION_TYPE_EXTERNAL))
            {
                String cmdline = getCmdline(command, args);
                execParams.put(Constants.EXECUTE_CMDLINE, cmdline);
                execParams.put(Constants.EXECUTE_INPUT, input);
                execParams.put(Constants.EXECUTE_EXECPATH, (String) props.get(Constants.EXECUTE_EXECPATH));
            }

            // initialize parameters
            execParams.put(Constants.EXECUTE_ACTIONNAME, actionname);
            execParams.put(Constants.EXECUTE_ACTIONNAMESPACE, actionnamespace);
            execParams.put(Constants.EXECUTE_ACTIONSUMMARY, actionsummary);
            execParams.put(Constants.EXECUTE_ACTIONHIDDEN, actionhidden);
            execParams.put(Constants.EXECUTE_ACTIONTAGS, actiontags);
            execParams.put(Constants.EXECUTE_ACTIONID, actionid);
            execParams.put(Constants.EXECUTE_PROCESSID, processid);
            execParams.put(Constants.EXECUTE_WIKI, wiki);
            execParams.put(Constants.EXECUTE_EXECUTEID, executeid);
            execParams.put(Constants.EXECUTE_PARSERID, parserid);
            execParams.put(Constants.EXECUTE_ASSESSID, assessid);
            execParams.put(Constants.EXECUTE_LOGRESULT, logResult);
            execParams.put(Constants.EXECUTE_USERID, userid);
            execParams.put(Constants.EXECUTE_PROBLEMID, problemid);
            execParams.put(Constants.EXECUTE_REFERENCE, reference);
            execParams.put(Constants.EXECUTE_DELAY, delay);
            execParams.put(Constants.EXECUTE_ADDRESS, address);
            execParams.put(Constants.EXECUTE_TARGET, target);
            
            
            Map tmpParams = (Map) StringUtils.stringToObj(params);
            tmpParams.put(Constants.EXECUTE_ACTIONRESULT_GUID, MainBase.main.configId.getGuid());
            params = StringUtils.objToString(tmpParams);
            
            execParams.put(Constants.EXECUTE_ACTIONRESULT_GUID, MainBase.main.configId.getGuid());
            execParams.put(Constants.EXECUTE_AFFINITY, affinity);
            execParams.put(Constants.EXECUTE_PARAMS, params);
            execParams.put(Constants.EXECUTE_FLOWS, flows);
            execParams.put(Constants.EXECUTE_INPUTS, inputs);
            execParams.put(Constants.EXECUTE_TIMEOUT, timeout);
            execParams.put(Constants.EXECUTE_PROCESS_TIMEOUT, processTimeout);
            execParams.put(Constants.EXECUTE_METRICID, metricId);
            execParams.put(Constants.EXECUTE_JOBNAME, taskname);
            execParams.put(Constants.EXECUTE_DEBUG, debug);
            execParams.put(Constants.EXECUTE_RESULTADDR, resultAddr);

            // set properties
            setProperties(execParams, props);

            // immediate actiontask - delayed is performed at the msg delivery (MListener)
            Log.log.info("Executing ActionTask name: " + taskname);
            TaskExecutor.execute(com.resolve.rsremote.ExecuteMain.class, "execute", execParams);

        }
        finally
        {
            Log.clearProcessId();
        }

    } // executeRequest
    
    public String stopConsumers(Map params)
    {
        String result;
        try
        {
            Log.log.info("MAction closing listeners");
            MainBase.main.esb.getMServer().closeListeners(false);
            result = "All non-GUID queue listeners closed";
        }
        catch (Exception e)
        {
            result = "Failed to close queue listeners: " + e.getMessage();
            Log.log.error("Failed to close queue listeners", e);
        }
        return result;
    }
    
    public String startConsumers(Map params)
    {
        StringBuilder result = new StringBuilder();
        try
        {
            com.resolve.esb.MServer mServer = MainBase.main.esb.getMServer();
    
            for (Object listener: mServer.getListeners())
            {
                MListener mListener = (MListener) listener;
                mListener.init(false);
                if (result.length() > 0)
                {
                    result.append("\n");
                }
                result.append("Restarted listener " + mListener.getReceiveQueueName());
                Log.log.info("Restarted listener for queue " + mListener.getReceiveQueueName()); 
            }
        }
        catch (Exception e)
        {
            if (result.length() > 0)
            {
                result.append("\n");
            }
            result.append("Failed to restart listeners: " + e.getMessage());
            Log.log.error("Failed to restart listeners", e);
        }
        return result.toString();
    }

    public void abortRequest(Map params) throws Exception
    {
        String taskname = (String) params.get(Constants.EXECUTE_TASKNAME);

        // kill process job
        Process process = ExecuteMain.getJobProcess(taskname);
        if (process != null)
        {
            Map killParams = new HashMap();
            killParams.put(Constants.EXECUTE_TYPE, "PROCESS");
            killParams.put(Constants.EXECUTE_JOBNAME, taskname.toUpperCase());
            TaskExecutor.execute(ExecuteMain.class, "kill", killParams);
        }

        // kill thread job
        else
        {
            Thread thread = ExecuteMain.getJobThread(taskname);
            if (thread != null)
            {
                Map killParams = new HashMap();
                killParams.put(Constants.EXECUTE_TYPE, "THREAD");
                killParams.put(Constants.EXECUTE_JOBNAME, taskname.toUpperCase());
                TaskExecutor.execute(ExecuteMain.class, "kill", killParams);
            }
        }
    } // abortRequest

    void setProperties(Map cronParams, Map props)
    {
        // only insert properties if not defined in cronParams
        for (Iterator i = props.entrySet().iterator(); i.hasNext();)
        {
            Map.Entry entry = (Map.Entry) i.next();
            String key = (String) entry.getKey();
            String value = (String) entry.getValue();

            if (!cronParams.containsKey(key))
            {
                cronParams.put(key, value);
            }
        }
    } // setProperties

    String getCmdline(String command, String args)
    {
        String result;

        if (StringUtils.isEmpty(args))
        {
            result = command;
        }
        else
        {
            result = command + " " + args;
        }

        return result;
    } // getCmdline

    public static void updateLicense(Map params)
    {
        Log.log.debug("Received license");
        if (!Main.main.release.getType().equalsIgnoreCase("RSVIEW"))
        {

        }
    } // updateLicense

    
    // Activates the Resolve cluster
    public Map<String, String> activateCluster(Map<String, String> params)
    {
        Log.log.info("***** Cluster activation message received *****");
        Main main = (Main) MainBase.main;
        main.activateCluster(params);
        
        // index all wikis and actiontasks
        HashMap<String, Object> p = new HashMap<String, Object>();
        MainBase.esb.sendInternalMessage(Constants.ESB_NAME_RSVIEW, "MIndex.indexAllWikiDocuments", p);
        MainBase.esb.sendInternalMessage(Constants.ESB_NAME_RSVIEW, "MIndex.indexAllActionTasks", p);
        
        return getClusterStatus(params);
    }

    // Deactivates the Resolve cluster
    public Map<String, String> deactivateCluster(Map<String, String> params)
    {
        Log.log.info("***** Cluster deactivation message received *****");
        Main main = (Main) MainBase.main;
        main.deactivateCluster(params);
        return getClusterStatus(params);
    }
    
    // Gets the status of the Resolve Cluster, whether it's been activated. 
    // Returns a Map<String, String> with a key "STATUS" and a value of 
    // either "ACTIVE" and "INACTIVE"
    
    public Map<String, String> getClusterStatus(Map<String, String> params)
    {
        Log.log.info("***** Cluster status message received (RSREMOTE). Status: " + MainBase.main.isClusterModeActive() + " *****");
        Map<String, String> status = new HashMap<String, String>();
        if(MainBase.main.isClusterModeActive()) {
            status.put("STATUS", "ACTIVE");
        } else {
            status.put("STATUS", "INACTIVE");
        }
        
        return status;
    }
    
    public Map<String, String> esbPing(Map<String, String> params)
    {
        params.put("RESPONSE-FROM", MainBase.main.configId.getGuid());
        return params;
        //MainBase.esb.sendInternalMessage(componet, "MAction.esbPingResponse", params);
    }
    
} // MAction
