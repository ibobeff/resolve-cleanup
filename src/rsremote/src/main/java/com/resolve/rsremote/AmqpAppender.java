/******************************************************************************
* (C) Copyright 2019
*
* Resolve Systems, LLC
*
* All rights reserved
* This software is distributed under license and may not be used, copied,
* modified, or distributed without the express written permission of
* Resolve Systems, LLC.
*
******************************************************************************/
package com.resolve.rsremote;

import org.apache.log4j.AppenderSkeleton;
import org.apache.log4j.helpers.LogLog;
import org.apache.log4j.spi.LocationInfo;
import org.apache.log4j.spi.LoggingEvent;

import com.resolve.esb.ESB;
import com.resolve.rsbase.MainBase;
import com.resolve.util.Constants;


public class AmqpAppender extends AppenderSkeleton {
	private static final String RESOLVE_ESB_PACKAGE;
	
	static {
		RESOLVE_ESB_PACKAGE = ESB.class.getPackage().getName();
	}
	
    /* (non-Javadoc)
     * @see org.apache.log4j.Appender#close()
     */
    @Override
    public void close() {
    }

    /* (non-Javadoc)
     * @see org.apache.log4j.Appender#requiresLayout()
     */
    @Override
    public boolean requiresLayout() {
        return true;
    }

    private boolean isLogEventFromResolveESB(LoggingEvent event) {
    	boolean resolveESBLogEvent = false;
    	
    	LocationInfo locInfo = event.getLocationInformation();
    	
    	if (!locInfo.equals(LocationInfo.NA_LOCATION_INFO) &&
    		locInfo.getClassName().startsWith(RESOLVE_ESB_PACKAGE)) {
    		LogLog.warn(String.format("Logging Event %s:%s:%s:%s:[%s] ignored by %s appender", 
    								   locInfo.getFileName(), locInfo.getLineNumber(), locInfo.getClassName(),
    								   locInfo.getMethodName(), layout.format(event), getName()));
    		resolveESBLogEvent = true;
    	}
    	
    	return resolveESBLogEvent;
    }
    
    /* (non-Javadoc)
     * @see org.apache.log4j.AppenderSkeleton#append(org.apache.log4j.spi.LoggingEvent)
     */
    @Override
    protected void append(LoggingEvent event) {
        if (!isLogEventFromResolveESB(event) && MainBase.esb != null)
        {
            String logEntry = this.layout.format(event);
            try
            {
                MainBase.esb.getMServer().getDefaultMSender().sendMessage(Constants.ESB_NAME_RSLOG, logEntry);
            }
            catch (Exception e) {
                LogLog.warn("Failed sending message [" + logEntry + "] to remote server: " + e.getMessage());
            }
        }
    }
}
