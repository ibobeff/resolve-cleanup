/******************************************************************************
* (C) Copyright 2014
*
* Resolve Systems, LLC
*
* All rights reserved
* This software is distributed under license and may not be used, copied,
* modified, or distributed without the express written permission of
* Resolve Systems, LLC.
*
******************************************************************************/

package com.resolve.rsremote;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Properties;

import com.resolve.auth.ActiveDirectory;
import com.resolve.util.Constants;
import com.resolve.util.Log;
import com.resolve.util.StringUtils;

public class AuthActiveDirectory
{
    static Map cache = new HashMap();
    
    public static Map authenticate(Map params)
    {
        Map result = new HashMap();
        
        String domain = StringUtils.getString(Constants.AUTH_DOMAIN, params);
        if (StringUtils.isNotEmpty(domain))
        {
            ActiveDirectory ad = (ActiveDirectory)cache.get(domain);
            if (ad == null)
            {
                // initialize LDAP
                String mode = StringUtils.getString(Constants.AUTH_AD_MODE, params);
                String host = StringUtils.getString(Constants.AUTH_AD_HOST, params);
                int port = StringUtils.getInt(Constants.AUTH_AD_PORT, params);
                String bindDN = StringUtils.getString(Constants.AUTH_AD_BINDDN, params); 
                String bindPassword = StringUtils.getString(Constants.AUTH_AD_BINDP_ASSWORD, params);
                List<String> baseDNList = StringUtils.getList(Constants.AUTH_AD_BASEDNLIST, params);
                boolean ssl = StringUtils.getBoolean(Constants.AUTH_AD_SSL, params); 
                Properties syncProp = StringUtils.getProperties(Constants.AUTH_AD_PROPERTIES, params);
                
                int version = StringUtils.getInt(Constants.AUTH_AD_VERSION, params);
                String defaultDomain = StringUtils.getString(Constants.AUTH_AD_DEFAULT_DOMAIN, params);
                String uidAttribute = StringUtils.getString(Constants.AUTH_AD_UID_ATTRIBUTE, params);
                
                try
                {
                    ad = new ActiveDirectory(host, port, baseDNList, ssl, mode, bindDN, bindPassword, syncProp);
                    ad.setVersion(version);
                    ad.setDefaultDomain(defaultDomain);
                    ad.setUIDAttribute(uidAttribute);
                    
                    cache.put(domain, ad);
                }
                catch (Exception e)
                {
                    Log.log.error(e.getMessage(), e);
                }
            }
            
            if (ad != null)
            {
                String username = StringUtils.getString(Constants.AUTH_USERNAME, params);
                String p_assword = StringUtils.getString(Constants.AUTH_P_ASSWORD, params);
                String mode = StringUtils.getString(Constants.AUTH_AD_MODE, params);
                
                Map attributes = ad.authenticate(username, p_assword, mode);
                if (attributes != null)
                {
                    Log.log.info("Authentication successful for username: "+username);
                    result = attributes;
                }
            }
        }
        
        return result;
    } // authenticate
    
    public static void invalidate(String domain)
    {
        cache.remove(domain);
    } // invalidate

} // AuthActiveDirectory
