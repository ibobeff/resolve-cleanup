/******************************************************************************
* (C) Copyright 2014
*
* Resolve Systems, LLC
*
* All rights reserved
* This software is distributed under license and may not be used, copied,
* modified, or distributed without the express written permission of
* Resolve Systems, LLC.
*
******************************************************************************/

package com.resolve.rsremote;

import java.util.HashMap;
import java.util.Map;

import com.resolve.rsbase.MainBase;
import com.resolve.util.Constants;
import com.resolve.util.Log;
import com.resolve.util.MetricThresholdInit;
import com.resolve.util.MetricThresholdType;

public class MetricThresholdInitImpl implements MetricThresholdInit
{
    @Override
    public Map<String, MetricThresholdType> init()
    {
        Map<String, MetricThresholdType> result = new HashMap<String, MetricThresholdType>();
        
        //send JMS to rsmgmt with the GUID to refresh threshold values
        Map<String, String> params = new HashMap<String, String>();
        params.put("GUID", MainBase.main.configId.getGuid());
        MainBase.esb.sendDelayedMessage(Constants.ESB_NAME_METRIC, "MMetric.updateMetricThresholdProperties", params, 300);
        Log.log.debug("Sending request to update metric threshold properties: ");
        
        return result;
    } 
} // MetricThresholdInitImpl 
