/******************************************************************************
* (C) Copyright 2014
*
* Resolve Systems, LLC
*
* All rights reserved
* This software is distributed under license and may not be used, copied,
* modified, or distributed without the express written permission of
* Resolve Systems, LLC.
*
******************************************************************************/

package com.resolve.rsremote;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Properties;

import com.resolve.auth.LDAP;
import com.resolve.util.Constants;
import com.resolve.util.Log;
import com.resolve.util.StringUtils;

public class AuthLDAP
{
    static Map cache = new HashMap();
    
    public static Map authenticate(Map params)
    {
        Map result = new HashMap();
        
        String domain = StringUtils.getString(Constants.AUTH_DOMAIN, params);
        if (StringUtils.isNotEmpty(domain))
        {
            LDAP ldap = (LDAP)cache.get(domain);
            if (ldap == null)
            {
                // initialize LDAP
                String mode = StringUtils.getString(Constants.AUTH_LDAP_MODE, params);
                String host = StringUtils.getString(Constants.AUTH_LDAP_HOST, params);
                int port = StringUtils.getInt(Constants.AUTH_LDAP_PORT, params);
                String bindDN = StringUtils.getString(Constants.AUTH_LDAP_BINDDN, params); 
                String bindPassword = StringUtils.getString(Constants.AUTH_LDAP_BINDP_ASSWORD, params);
                List<String> baseDNList = StringUtils.getList(Constants.AUTH_LDAP_BASEDNLIST, params);
                boolean ssl = StringUtils.getBoolean(Constants.AUTH_LDAP_SSL, params); 
                String cryptType = StringUtils.getString(Constants.AUTH_LDAP_CRYPTTYPE, params);
                String cryptPrefix = StringUtils.getString(Constants.AUTH_LDAP_CRYPTPREFIX, params);
                Properties syncProp = StringUtils.getProperties(Constants.AUTH_LDAP_PROPERTIES, params);
                
                int version = StringUtils.getInt(Constants.AUTH_LDAP_VERSION, params);
                String defaultDomain = StringUtils.getString(Constants.AUTH_LDAP_DEFAULT_DOMAIN, params);
                String uidAttribute = StringUtils.getString(Constants.AUTH_LDAP_UID_ATTRIBUTE, params);
                String p_asswordAttribute = StringUtils.getString(Constants.AUTH_LDAP_P_ASSWORD_ATTRIBUTE, params);
                
                try
                {
                    ldap = new LDAP(mode, host, port, bindDN, bindPassword, baseDNList, ssl, cryptType, cryptPrefix, syncProp);
                    ldap.setVersion(version);
                    ldap.setDefaultDomain(defaultDomain);
                    ldap.setUIDAttribute(uidAttribute);
                    ldap.setPasswordAttribute(p_asswordAttribute);
                    
                    cache.put(domain, ldap);
                }
                catch (Exception e)
                {
                    Log.log.error(e.getMessage(), e);
                }
            }
            
            if (ldap != null)
            {
                String username = StringUtils.getString(Constants.AUTH_USERNAME, params);
                String p_assword = StringUtils.getString(Constants.AUTH_P_ASSWORD, params);
                Map attributes = ldap.authenticate(username, p_assword);
                if (attributes != null)
                {
                    Log.log.info("Authentication successful for username: "+username);
                    result = attributes;
                }
            }
        }
        
        return result;
    } // authenticate
    
    public static void invalidate(String domain)
    {
        cache.remove(domain);
    } // invalidate

} // AuthLDAP
