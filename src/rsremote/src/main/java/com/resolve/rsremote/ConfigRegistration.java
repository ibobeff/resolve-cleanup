/******************************************************************************
* (C) Copyright 2014
*
* Resolve Systems, LLC
*
* All rights reserved
* This software is distributed under license and may not be used, copied,
* modified, or distributed without the express written permission of
* Resolve Systems, LLC.
*
******************************************************************************/

package com.resolve.rsremote;

import com.resolve.util.ConfigMap;
import com.resolve.util.XDoc;

public class ConfigRegistration extends ConfigMap
{
    private static final long serialVersionUID = 7099102868114466206L;
	int interval = 5; // 5 mins
    String parentGuid = "RSCONTROL";
    boolean logHeartbeat = true;

    public ConfigRegistration(XDoc config) throws Exception
    {
        super(config);

        define("interval", INTEGER, "./REGISTRATION/@INTERVAL");
        define("parentGuid", STRING, "./REGISTRATION/@PARENTGUID");
        define("logHeartbeat", BOOLEAN, "./REGISTRATION/@LOGHEARTBEAT");
    } // ConfigRegistration

    public void load()
    {
        loadAttributes();
    } // load

    public void save()
    {
        saveAttributes();
    } // save

    public int getInterval()
    {
        return interval;
    }

    public void setInterval(int interval)
    {
        this.interval = interval;
    }

    public String getParentGuid()
    {
        return parentGuid;
    }

    public void setParentGuid(String parentGuid)
    {
        this.parentGuid = parentGuid;
    }

    public boolean isLogHeartbeat()
    {
        return logHeartbeat;
    }

    public void setLogHeartbeat(boolean logHeartbeat)
    {
        this.logHeartbeat = logHeartbeat;
    }

} // ConfigRegistration
