/******************************************************************************
* (C) Copyright 2014
*
* Resolve Systems, LLC
*
* All rights reserved
* This software is distributed under license and may not be used, copied,
* modified, or distributed without the express written permission of
* Resolve Systems, LLC.
*
******************************************************************************/

package com.resolve.rsremote.executor;

import java.util.HashMap;
import java.util.Map;

import com.resolve.gateway.itm.ITMGateway;
import com.resolve.util.Constants;
import com.resolve.util.EncryptionTransportObject;
import com.resolve.util.StringUtils;

/**
 * This class implements the External command executor interface that is
 * specific to the ITM.
 * 
 */
public class ITMExecutor implements ExternalExecutor
{
    public static final String EXECUTOR_TYPE = "ITM";

    private static final String ITM_HOST_USERNAME = "USERNAME";
    private static final String ITM_HOST_P_ASSWORD = "PASSWORD";
    private static final String REMOTE_AGENT_USERNAME = "REMOTE_AGENT_USERNAME";
    private static final String AGENT_NAME = "AGENT_NAME";

    private final Map<String, Object> allowedParams = new HashMap<String, Object>();

    private final Map<String, Object> data;

    public ITMExecutor(Map<String, Object> data)
    {
        this.data = data;

        allowedParams.put(Constants.EXECUTE_CMDLINE, null);
        allowedParams.put(Constants.EXECUTE_INPUT, null);
        allowedParams.put(Constants.EXECUTE_FILENAME, null);
        allowedParams.put(Constants.EXECUTE_EXECPATH, null);
        allowedParams.put(Constants.ACTION_INVOCATION_OPTIONS_INPUTFILE_POSTFIX, null);
    }

    private boolean verifyInput(Map params) throws Exception
    {
        String agentName = (String) params.get(AGENT_NAME);
        if (StringUtils.isEmpty(agentName))
        {
            throw new Exception("Agent Name must be provided. Define a INPUT variable name AGENT_NAME and set its value");
        }
        return true;
    }

    @Override
    public String execute() throws Exception
    {
        if (data != null)
        {
            // String paramsStr = getParam(Constants.EXECUTE_PARAMS);
            // String flowsStr = getParam(Constants.EXECUTE_FLOWS);
            String inputsStr = getParam(Constants.EXECUTE_INPUTS);

            Map params = (Map) StringUtils.stringToObj(inputsStr);
            if (verifyInput(params))
            {
                // String jobname = getParam(Constants.EXECUTE_JOBNAME);
                String remoteDirectory = getParam(Constants.EXECUTE_EXECPATH);
                String scriptFileExt = getParam(Constants.ACTION_INVOCATION_OPTIONS_INPUTFILE_POSTFIX);
                String commandLine = getParam(Constants.EXECUTE_CMDLINE);
                String scriptContent = getParam(Constants.ACTION_INVOCATION_OPTIONS_INPUTFILE);

                String username = (String) params.get(ITM_HOST_USERNAME);
                String password = null;
                Object obj = params.get(ITM_HOST_P_ASSWORD);
                if (obj instanceof EncryptionTransportObject)
                {
                    password = ((EncryptionTransportObject) obj).getPlainText();
                }
                else
                {
                    password = (String) params.get(ITM_HOST_P_ASSWORD);
                }
                String remoteUsername = (String) params.get(REMOTE_AGENT_USERNAME);
                String agentName = (String) params.get(AGENT_NAME);

                ITMGateway itmGateway = new ITMGateway(username, password, remoteUsername, agentName, commandLine, scriptContent, scriptFileExt, remoteDirectory);

                itmGateway.setCommandLine(commandLine);
                itmGateway.setScriptContent(scriptContent);
                itmGateway.setScriptFileExt(scriptFileExt);
                itmGateway.setRemoteDirectory(remoteDirectory);

                itmGateway.setUsername(username);
                itmGateway.setPassword(password);
                itmGateway.setAgentName(agentName);
                itmGateway.setRemoteUsername(remoteUsername);

                return itmGateway.execute();
            }
        }
        return null;
    }

    String getParam(String name)
    {
        String result = "";

        Object value = data.get(name.toUpperCase());
        if (value != null)
        {
            if (value instanceof String)
            {
                result = (String) value;

                if (result.equalsIgnoreCase("null"))
                {
                    result = "";
                }
            }
            else
            {
                result = "" + value;
            }
        }

        return result;
    } // getParam
}
