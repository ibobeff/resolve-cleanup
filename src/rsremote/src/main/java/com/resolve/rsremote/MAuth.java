/******************************************************************************
* (C) Copyright 2014
*
* Resolve Systems, LLC
*
* All rights reserved
* This software is distributed under license and may not be used, copied,
* modified, or distributed without the express written permission of
* Resolve Systems, LLC.
*
******************************************************************************/

package com.resolve.rsremote;

import java.io.File;
import java.util.Map;

import com.resolve.rsbase.MainBase;
import com.resolve.util.Constants;
import com.resolve.util.Log;
import com.resolve.util.SSLUtils;
import com.resolve.util.StringUtils;

public class MAuth
{
    public void invalidateDomain(Map params)
    {
        String domain = StringUtils.getString(Constants.AUTH_DOMAIN, params);
        AuthLDAP.invalidate(domain);
        AuthActiveDirectory.invalidate(domain);
    } // invalidateDomain
    
    public Map authenticateLDAP(Map params)
    {
        return AuthLDAP.authenticate(params);
    } // authenticateLDAP
    
    public Map authenticateActiveDirectory(Map params)
    {
        return AuthActiveDirectory.authenticate(params);
    } // authenticateActiveDirectory

    /**
     * This method imports SSL client certificate from an LDAP server.
     * RSView sends the message (refer ConfigLDAPUtil.sendMessageToImportSSLCertificate. 
     * 
     * @param params
     */
    public void importSSLCertificate(Map<String, String> params)
    {
        if (params != null)
        {
            String host = params.get(Constants.AUTH_LDAP_HOST);
            String port = params.get(Constants.AUTH_LDAP_PORT);
            try
            {
                File certStore = new File(MainBase.main.getResolveHome() + "/jdk/lib/security/cacerts");
                SSLUtils.importCertificate(certStore, "changeit", host, Integer.parseInt(port));
            }
            catch (Exception e)
            {
                Log.log.error(e.getMessage(), e);
            }
        }
    }

} // MAuth
