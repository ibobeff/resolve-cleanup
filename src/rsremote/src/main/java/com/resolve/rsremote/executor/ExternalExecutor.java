/******************************************************************************
* (C) Copyright 2014
*
* Resolve Systems, LLC
*
* All rights reserved
* This software is distributed under license and may not be used, copied,
* modified, or distributed without the express written permission of
* Resolve Systems, LLC.
*
******************************************************************************/

package com.resolve.rsremote.executor;

/**
 * 
 */
public interface ExternalExecutor
{
    /**
     * Verifies the incoming data and executes command/script.
     * 
     * @return
     */
    String execute() throws Exception;
}
