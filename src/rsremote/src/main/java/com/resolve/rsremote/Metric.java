/******************************************************************************
* (C) Copyright 2014
*
* Resolve Systems, LLC
*
* All rights reserved
* This software is distributed under license and may not be used, copied,
* modified, or distributed without the express written permission of
* Resolve Systems, LLC.
*
******************************************************************************/

package com.resolve.rsremote;

import java.lang.management.ManagementFactory;
import java.lang.management.ThreadMXBean;
import java.util.Map;

import org.apache.commons.collections.MapUtils;
import org.apache.commons.lang3.tuple.Pair;

import com.resolve.execute.ExecuteOS;
import com.resolve.execute.ExecuteResult;
import com.resolve.rsbase.BaseMetric;
import com.resolve.util.MetricMsg;

public class Metric extends BaseMetric
{
    //read  and load the threshold values at component startup
    //public static MetricThreshold mthreshold = new MetricThreshold(Constants.ESB_NAME_RSREMOTE);
    
    public Metric()
    {
        super(new MetricThresholdInitImpl());
    }
    
    public void sendMetrics()
    {
        if(mthreshold.thresholdProperties.keySet().isEmpty())
        {
            mthreshold.setThresholdProperties();
        }
        
        // send MetricJVM
        sendMetricJVM();

        // send cpu (only unix)
        sendMetricCPU();
    } // sendMetrics
    
    void sendMetricJVM()
    {
        MetricMsg result = new MetricMsg("JVM", Main.main.release.getHostServiceName() + "/" + Main.main.configId.getGuid(), "");

        // MemoryPercentFree
        // int value = (int) (Runtime.getRuntime().freeMemory() * 100 /
        // Runtime.getRuntime().totalMemory());
        long free = (Runtime.getRuntime().maxMemory() - Runtime.getRuntime().totalMemory()) + Runtime.getRuntime().freeMemory();
        int value = (int) (free * 100 / Runtime.getRuntime().maxMemory());
        result.addMetric("mem_free", value);
        mthreshold.checkThresholds("jvm", value, "mem_free");
//        String metricname = "JVM".toLowerCase() + ".mem_free";
//        // threshold memory free value
//        if (value < mthreshold.thresholdProperties.get(metricname).getLow())
//        {
//            Log.alert(ERR.E70001, "Low available memory: "+value);
//        }

        // PeakThread
        ThreadMXBean thread = ManagementFactory.getThreadMXBean();
        result.addMetric("thread_count", thread.getThreadCount());
//        int thread_count_percentage = (int) (thread.getThreadCount() * 100) / (Main.main.configGeneral.getMaxThread() * 2); //approx max for the jvm thread = double the thread poll max
//        mthreshold.checkThresholds("jvm", thread_count_percentage, "thread_count");
//        mthreshold.checkThresholds("jvm", thread.getThreadCount(), "thread_count");
        Main.sendMetric(result);
    } // sendMetricJVM

    void sendMetricCPU()
    {
        String osname = System.getProperty("os.name").toLowerCase();
        if (osname.equals("linux") || osname.equals("sunos") || osname.equals("aix"))
        {
            // execute and parse uptime
            ExecuteOS os = new ExecuteOS(Main.main.getProductHome());
            ExecuteResult result = os.execute("uptime", null, true, 10, "sendMetricCPU");
            String out = result.getResultString();
            if (out != null)
            {
                Map<String, Pair<Long, Integer>> cpuMetrics = MetricMsg.parseLinuxCPUMetricOutput(out);
            	
            	if (MapUtils.isNotEmpty(cpuMetrics)) {
            		MetricMsg msg = new MetricMsg("Server", 
                							  	  Main.main.release.getHostServiceName() + "/" + 
                							  	  Main.main.configId.getGuid(), "");

            		if (cpuMetrics.containsKey(MetricMsg.CPU_METRIC_LOAD_1)) {
            			msg.addMetric(MetricMsg.CPU_METRIC_LOAD_1, 
            						  cpuMetrics.get(MetricMsg.CPU_METRIC_LOAD_1).getLeft().longValue());
            			mthreshold.checkThresholds("server", 
            									   cpuMetrics.get(MetricMsg.CPU_METRIC_LOAD_1).getRight().intValue() / 100, 
            									   MetricMsg.CPU_METRIC_LOAD_1);
            		}
            		
            		if (cpuMetrics.containsKey(MetricMsg.CPU_METRIC_LOAD_5)) {
            			msg.addMetric(MetricMsg.CPU_METRIC_LOAD_5, 
            						  cpuMetrics.get(MetricMsg.CPU_METRIC_LOAD_5).getLeft().longValue());
            			mthreshold.checkThresholds("server", 
            									   cpuMetrics.get(MetricMsg.CPU_METRIC_LOAD_5).getRight().intValue() / 100, 
            									   MetricMsg.CPU_METRIC_LOAD_5);
            		}
            		
            		if (cpuMetrics.containsKey(MetricMsg.CPU_METRIC_LOAD_15)) {
            			msg.addMetric(MetricMsg.CPU_METRIC_LOAD_15, 
            						  cpuMetrics.get(MetricMsg.CPU_METRIC_LOAD_15).getLeft().longValue());
            			mthreshold.checkThresholds("server", 
            									   cpuMetrics.get(MetricMsg.CPU_METRIC_LOAD_15).getRight().intValue() / 100, 
            									   MetricMsg.CPU_METRIC_LOAD_15);
            		}
            		
            		Main.sendMetric(msg);
            	}
            }
        }
    } // sendMetricCPU

} // Metric
