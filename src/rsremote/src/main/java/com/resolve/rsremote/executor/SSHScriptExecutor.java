/******************************************************************************
* (C) Copyright 2014
*
* Resolve Systems, LLC
*
* All rights reserved
* This software is distributed under license and may not be used, copied,
* modified, or distributed without the express written permission of
* Resolve Systems, LLC.
*
******************************************************************************/

package com.resolve.rsremote.executor;

import java.io.File;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.regex.Pattern;

import com.resolve.connect.SSHConnect;
import com.resolve.rsremote.Main;
import com.resolve.util.Constants;
import com.resolve.util.FileUtils;
import com.resolve.util.StringUtils;
import com.resolve.util.EncryptionTransportObject;

/**
 * This class implements the External command executor interface that is
 * specific to running various scripts in a Linux/Unix server over SSH. This
 * uses {@link SSHConnect} to communicate with the server.
 */
public class SSHScriptExecutor implements ExternalExecutor
{
    public static final String EXECUTOR_TYPE = "SSH";

    private static final String HOSTNAME = "HOSTNAME";
    private static final String PORT = "PORT";
    private static final String USERNAME = "USERNAME";
    private static final String P_ASSWORD = "PASSWORD";
    private static final String TIMEOUT = "TIMEOUT";
    private static final String PROMPT = "PROMPT";

    private final Map<String, Object> allowedParams = new HashMap<String, Object>();

    private final Map<String, Object> data;

    public SSHScriptExecutor(Map<String, Object> data)
    {
        this.data = data;

        allowedParams.put(Constants.EXECUTE_CMDLINE, null);
        allowedParams.put(Constants.EXECUTE_INPUT, null);
        allowedParams.put(Constants.EXECUTE_FILENAME, null);
        allowedParams.put(Constants.EXECUTE_EXECPATH, null);
        allowedParams.put(Constants.ACTION_INVOCATION_OPTIONS_INPUTFILE_POSTFIX, null);
    }

    private boolean verifyInput(Map params) throws Exception
    {
        String hostIp = (String) params.get(HOSTNAME);
        String hostUsername = (String) params.get(USERNAME);
        String hostPassword = (String) params.get(P_ASSWORD);
        String scriptContent = getParam(Constants.ACTION_INVOCATION_OPTIONS_INPUTFILE);

        if (StringUtils.isEmpty(hostIp))
        {
            throw new Exception("Host Name or IP address must be provided. Define a INPUT variable name HOSTNAME and set its value");
        }
        if (StringUtils.isEmpty(hostUsername))
        {
            throw new Exception("Username must be provided. Define a INPUT variable name USERNAME and set its value");
        }
        if (StringUtils.isEmpty(hostPassword))
        {
            throw new Exception("Password must be provided. Define a INPUT variable name PASSWORD and set its value");
        }
        if (StringUtils.isEmpty(scriptContent))
        {
            throw new Exception("Content must be provided. Define it in the content tab of the Action Task.");
        }

        return true;
    }

    @Override
    public String execute() throws Exception
    {
        String result = "";

        if (data != null)
        {
            // String paramsStr = getParam(Constants.EXECUTE_PARAMS);
            // String flowsStr = getParam(Constants.EXECUTE_FLOWS);
            String inputsStr = getParam(Constants.EXECUTE_INPUTS);

            Map inputs = (Map) StringUtils.stringToObj(inputsStr);
            List preInputs = EncryptionTransportObject.decryptMap(inputs);
            try
            {
                if (verifyInput(inputs))
                {
                    String remoteDirectory = getParam(Constants.EXECUTE_EXECPATH);
                    remoteDirectory = (StringUtils.isEmpty(remoteDirectory) ? "." : remoteDirectory);
    
                    String scriptFileExt = getParam(Constants.ACTION_INVOCATION_OPTIONS_INPUTFILE_POSTFIX);
    
                    String commandLine = getParam(Constants.EXECUTE_CMDLINE);
                    if (StringUtils.isEmpty(commandLine))
                    {
                        commandLine = "sh";
                    }
    
                    String scriptContent = getParam(Constants.ACTION_INVOCATION_OPTIONS_INPUTFILE);
    
                    String hostName = (String) inputs.get(HOSTNAME);
    
                    int port = 22;
                    if (inputs.containsKey(PORT) && inputs.get(PORT) != null)
                    {
                        port = Integer.valueOf((String) inputs.get(PORT));
                    }
    
                    int timeout = 60 * 1000;
                    if (inputs.containsKey(TIMEOUT) && inputs.get(TIMEOUT) != null)
                    {
                        timeout = Integer.valueOf((String) inputs.get(TIMEOUT)) * 1000;
                    }
    
                    String username = (String) inputs.get(USERNAME);
                    String password = (String) inputs.get(P_ASSWORD);
                    String prompt = (String) inputs.get(PROMPT);
                    if (StringUtils.isEmpty(prompt))
                    {
                        prompt = "]$";
                    }
                    String promptRegEx = Pattern.quote(prompt);
    
                    String resolveTempDir = Main.main.configGeneral.home + "/tmp";
                    String scriptFileName = FileUtils.createRandomFileName(scriptFileExt, "in");
    
                    File localScriptFile = new File(resolveTempDir + "/" + scriptFileName);
                    FileUtils.writeStringToFile(localScriptFile, scriptContent.replaceAll("\r", ""));
    
                    SSHConnect conn = new SSHConnect(username, password, hostName, port, timeout);
    
                    conn.put(localScriptFile, remoteDirectory, "0755"); // make it
                                                                        // executable
    
                    // change the working firectory in the remote server.
                    conn.send("cd " + remoteDirectory);
    
                    // execute the script in the remote server.
                    conn.send(commandLine + " " + scriptFileName);
    
                    result += conn.expect(promptRegEx);
    
                    // delete the remote file. We're on the
                    conn.send("rm " + scriptFileName);
                    // delete the local file.
                    localScriptFile.delete();
    
                    // close the connection.
                    conn.close();
                }
            }
            finally
            {
                EncryptionTransportObject.restoreMap(inputs, preInputs);
            }
        }
        return result;
    }

    private String getParam(String name)
    {
        String result = "";

        Object value = data.get(name.toUpperCase());
        if (value != null)
        {
            if (value instanceof String)
            {
                result = (String) value;

                if (result.equalsIgnoreCase("null"))
                {
                    result = "";
                }
            }
            else
            {
                result = "" + value;
            }
        }

        return result;
    } // getParam
}
