/******************************************************************************
* (C) Copyright 2014
*
* Resolve Systems, LLC
*
* All rights reserved
* This software is distributed under license and may not be used, copied,
* modified, or distributed without the express written permission of
* Resolve Systems, LLC.
*
******************************************************************************/

package com.resolve.rsremote.executor;

import java.util.Map;

public class ExternalExecutorFactory
{

    private ExternalExecutorFactory()
    {

    }

    public static ExternalExecutor getExecutor(String type, Map<String, Object> data)
    {
        if (type.equals(ITMExecutor.EXECUTOR_TYPE))
        {
            return new ITMExecutor(data);
        }
        if (type.equals(SSHScriptExecutor.EXECUTOR_TYPE))
        {
            return new SSHScriptExecutor(data);
        }
        else
        {
            throw new RuntimeException("Unsupported Executor type " + type);
        }
    }
}
