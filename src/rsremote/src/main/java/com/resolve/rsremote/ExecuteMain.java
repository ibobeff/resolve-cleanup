/******************************************************************************
* (C) Copyright 2014
*
* Resolve Systems, LLC
*
* All rights reserved
* This software is distributed under license and may not be used, copied,
* modified, or distributed without the express written permission of
* Resolve Systems, LLC.
*
******************************************************************************/

package com.resolve.rsremote;

import java.io.File;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.concurrent.atomic.AtomicLong;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.apache.commons.io.FileUtils;
import org.apache.commons.lang3.SerializationException;

import com.resolve.esb.MMsgContent;
import com.resolve.esb.MMsgHeader;
import com.resolve.esb.MMsgOptions;
import com.resolve.rsbase.MainBase;
import com.resolve.execute.Execute;
import com.resolve.execute.ExecuteAnt;
import com.resolve.execute.ExecuteCScript;
import com.resolve.execute.ExecuteGroovy;
import com.resolve.execute.ExecuteJava;
import com.resolve.execute.ExecuteOS;
import com.resolve.execute.ExecutePowershell;
import com.resolve.execute.ExecuteRemote;
import com.resolve.execute.ExecuteResult;
import com.resolve.execute.ExecuteWinExec;
import com.resolve.execute.ExecuteWinTask;
import com.resolve.rsremote.executor.ExternalExecutor;
import com.resolve.rsremote.executor.ExternalExecutorFactory;
import com.resolve.thread.TaskExecutor;
import com.resolve.util.Constants;
import com.resolve.util.Log;
import com.resolve.util.ObjectProperties;
import com.resolve.util.ScriptDebug;
import com.resolve.util.StringUtils;
import com.resolve.util.ThreadUtils;

public class ExecuteMain
{
    final static Pattern VAR_REGEX_CONFIG = Pattern.compile(Constants.VAR_REGEX_CONFIG);
    final static Pattern VAR_REGEX_TARGET = Pattern.compile(Constants.VAR_REGEX_TARGET);
    
    public static AtomicInteger activeTasks = new AtomicInteger(0);
    public static AtomicLong maxExpirationTime = new AtomicLong(0);

    public static ConcurrentHashMap jobKilled = new ConcurrentHashMap();
    public static ConcurrentHashMap jobProcess = new ConcurrentHashMap();
    public static ConcurrentHashMap jobThread = new ConcurrentHashMap();

    Thread thread;
    Map data;
    String inputfile;
    String tmpfile;
    boolean hasInputFile;
    boolean hasTmpFile;
    boolean hasResultFile;
    long startTime;

    // called from Executor
    public ExecuteMain()
    {
    } // ExecuteMain

    public ExecuteMain(Map params)
    {
        this.data = params;
    } // ExecuteMain

    public void setThread(Thread thread)
    {
        this.thread = thread;
    } // setThread

    public Thread getThread()
    {
        return this.thread;
    } // getThread

    public void execute()
    {
        execute(true);
    } // execute

    public void execute(Map params)
    {
        this.data = params;

        this.execute(true);
    } // execute

    public void execute(boolean returnResult)
    {
        if (data != null)
        {
            ExecuteResult result = null;
            hasInputFile = false;
            hasTmpFile = false;

            String type = getParam("TYPE");
            String jobname = getParam("JOBNAME");
            String filename = getParam("FILENAME");
            String processid = getParam(Constants.EXECUTE_PROCESSID);
			String fileEncoding = getParam(Constants.ACTION_INVOCATION_OPTIONS_INPUTFILE_ENCODING);
            // init script debug
            boolean isDebug = false;
            String debugStr = getParam(Constants.EXECUTE_DEBUG);
            if (StringUtils.isNotEmpty(debugStr) && debugStr.equalsIgnoreCase("true"))
            {
                isDebug = true;
            }
            ScriptDebug debug = new ScriptDebug(isDebug, null);
            debug.setComponent("ExecuteMain:" + jobname);

            try
            {
                // init thread processid
                Log.setProcessId(processid);
                
                Log.log.debug("ExecuteMain");
                Log.log.debug("  type: " + type + " jobname: " + jobname);
                // Log.log.debug("  data: "+data);
                
                // increment active count
                int count = activeTasks.incrementAndGet();
                Log.log.debug("active count: "+count);
                
                // init startTime
                // startTime = System.currentTimeMillis();
                
                // set shutdown expirationTimeout
                long timeout = StringUtils.stringToLong(getParam(Constants.EXECUTE_TIMEOUT));
                if (timeout > 0)
                {
                    long newExpiration = (timeout*1000) + System.currentTimeMillis();
                    if (newExpiration > ExecuteMain.maxExpirationTime.get())
                    {
                        ExecuteMain.maxExpirationTime.set(newExpiration);
                    }
                }

                // createInputFile if type is not REMOTE, INTERNAL or EXTERNAL
                String contents = getParam(Constants.ACTION_INVOCATION_OPTIONS_INPUTFILE);
                if (!StringUtils.isEmpty(contents))
                {
                    if (!filename.equals("${INPUTFILE}") && !type.equalsIgnoreCase(Constants.ACTION_INVOCATION_TYPE_REMOTE) && !type.equalsIgnoreCase(Constants.ACTION_INVOCATION_TYPE_EXTERNAL))
                    {
                        if (type.equalsIgnoreCase(Constants.ACTION_INVOCATION_TYPE_BASH))
                        {
                            contents = contents.replaceAll("[\r\015\032]", "");
                        }
                        String extension = getParam(Constants.ACTION_INVOCATION_OPTIONS_INPUTFILE_POSTFIX);
                        if (type.equalsIgnoreCase(Constants.ACTION_INVOCATION_TYPE_POWERSHELL))
                        {
                            extension = ".ps1";
                        } else if (type.equalsIgnoreCase(Constants.ACTION_INVOCATION_TYPE_CSCRIPT))
                        {
                        	extension = ".vbs";
                        }
                        if(fileEncoding == null || fileEncoding.trim().isEmpty()) {
							fileEncoding = "UTF-8";
						}
						Log.log.debug("create input file with file encoding: " + fileEncoding);
                        createInputFile(contents, extension, fileEncoding);
                    }
                }

                // ExecuteOS, ExecuteCMD
                if (type.equalsIgnoreCase(Constants.ACTION_INVOCATION_TYPE_OS) || type.equalsIgnoreCase(Constants.ACTION_INVOCATION_TYPE_BASH) || type.equalsIgnoreCase(Constants.ACTION_INVOCATION_TYPE_CMD))
                {
                    result = executeOS();
                }

                // ExecuteJava
                else if (type.equalsIgnoreCase(Constants.ACTION_INVOCATION_TYPE_JAVA))
                {
                    result = executeJava();
                }

                // ExecuteGroovy
                else if (type.equalsIgnoreCase(Constants.ACTION_INVOCATION_TYPE_GROOVY))
                {
                    result = executeGroovy();
                }

                // ExecuteAnt
                else if (type.equalsIgnoreCase(Constants.ACTION_INVOCATION_TYPE_ANT))
                {
                    result = executeAnt();
                }

                // ExecuteRemote
                else if (type.equalsIgnoreCase(Constants.ACTION_INVOCATION_TYPE_REMOTE))
                {
                    // RBA-14752 OS AT followed by Remote AT gives missing content in Remote AT
//                    if (data != null && !data.isEmpty() && data.containsKey(Constants.EXECUTE_FILENAME) &&
//                        StringUtils.isNotBlank((String)data.get(Constants.EXECUTE_FILENAME)) &&
//                        data.containsKey(Constants.ACTION_INVOCATION_OPTIONS_INPUTFILE))
//                    {
//                        // Reset the FILENAME value to ""
//                        data.put(Constants.EXECUTE_FILENAME, "");
//                    }
                    
                    result = executeRemote(debug);
                }

                // ExecuteCScript
                else if (type.equalsIgnoreCase(Constants.ACTION_INVOCATION_TYPE_CSCRIPT))
                {
                    result = executeCScript();
                }

                // ExecutePowershell
                else if (type.equalsIgnoreCase(Constants.ACTION_INVOCATION_TYPE_POWERSHELL))
                {
                    result = executePowershell();
                }

                // ExecuteWinExec
                else if (type.equalsIgnoreCase(Constants.ACTION_INVOCATION_TYPE_WINEXEC))
                {
                    result = executeWinExec();
                }

                // ExecuteWinTask
                else if (type.equalsIgnoreCase(Constants.ACTION_INVOCATION_TYPE_WINTASK))
                {
                    result = executeWinTask();
                }

                // ExecuteExternal
                else if (type.equalsIgnoreCase(Constants.ACTION_INVOCATION_TYPE_EXTERNAL))
                {
                    // result = executeExternal(debug);
                    result = executeExternal();
                }

                // no operations
                else if (type.equalsIgnoreCase(Constants.ACTION_INVOCATION_TYPE_NONE))
                {
                    // no operations
                    result = new ExecuteResult(true);
                }
                else
                {
                    throw new Exception("Invalid ActionInvocation type: " + type);
                }

                // check if resulttype == FILE
                String resulttype = getParam(Constants.ACTION_INVOCATION_OPTIONS_RESULTTYPE);
                if (resulttype != null && resulttype.equalsIgnoreCase("FILE"))
                {
                    getFileResult(result);
                }

                // delete inputfile and tmpfile
                deleteFiles();

                if (result != null)
                {
                    Log.log.debug("Results:\n" + result);
                    debug.println("Results:\n" + result);
                }

                // return result to RSSERVER
                if (result != null && returnResult)
                {
                    Object resultValue;
                    if (result.isObject())
                    {
                        resultValue = result.getResultObject();
                    }
                    else
                    {
                        resultValue = result.getResultString();
                    }

                    /*
                     * ResultValue Post-process
                     */
                    String excludeCmdline = getParam(Constants.ACTION_INVOCATION_OPTIONS_CMDLINE_EXCLUDE);
                    if (excludeCmdline != null && excludeCmdline.equalsIgnoreCase("TRUE"))
                    {
                        result.setCommand("");
                    }

                    String blankValues = getParam(Constants.ACTION_INVOCATION_OPTIONS_BLANKVALUES);
                    if (StringUtils.isNotEmpty(blankValues))
                    {
                        // blank private variables / values
                        if (result.isObject() == false)
                        {
                            resultValue = blankVals((String) resultValue, blankValues);
                        }
                        String command = result.getCommand();
                        if (StringUtils.isNotEmpty(command))
                        {
                            command = blankVals(command, blankValues);
                            result.setCommand(command);
                        }
                    }

                    // check if process was terminated by kill job
                    if (result.isAborted())
                    {
                        Log.log.debug("ActionTask was aborted jobname: " + jobname);
                        debug.println("ActionTask was aborted jobname: " + jobname);
                        resultValue = "ActionTask aborted by user or timeout. Available results:\n\n" + resultValue;
                    }

                    // set debug
                    if (debug.isDebug())
                    {
                        result.setDebug(debug.getBuffer());
                    }

                    Log.log.info("Sending results");
                    Log.log.debug("  jobname: " + jobname);
                    Log.log.debug("  command: " + result.getCommand());
                    Log.log.debug("  completion: " + result.isCompletion());
                    Log.log.debug("  returncode: " + result.getReturncode());
                    Log.log.debug("  duration: " + result.getDuration());

                    debug.println("Sending results");
                    debug.println("  jobname: " + jobname);
                    debug.println("  command: " + result.getCommand());
                    debug.println("  completion: " + result.isCompletion());
                    debug.println("  returncode: " + result.getReturncode());
                    debug.println("  duration: " + result.getDuration());

                    sendResult(result, resultValue);
                }
            }
            catch (Throwable e)
            {
                Log.log.error("Failed to execute actiontask: " + e.getMessage(), e);
                debug.println("ExecuteMain", "Failed to execute actiontask: " + e.getMessage(), e);

                // init result
                if (result == null)
                {
                    result = new ExecuteResult();
                    result.setCompletion(false);
                    result.setReturncode(Constants.ASSESS_RETURNCODE_EXECUTE_EXCEPTION);
                    result.setDuration(0);
                } else {
                    result.setCompletion(false);
                    result.setReturncode(Constants.ASSESS_RETURNCODE_EXECUTE_EXCEPTION);
                }

                // set debug
                if (debug.isDebug())
                {
                    result.setDebug(debug.getBuffer());
                }

                try {
                    sendResult(result, "Failed to execute actiontask: " + e.getMessage() + StringUtils.getStackTrace(e));
                } catch(Exception ee) {
                    Log.log.warn(ee.getMessage() + StringUtils.getStackTrace(ee), ee);
                }
            }
            finally
            {
                Log.clearProcessId();
                
                // decrement active count
                activeTasks.decrementAndGet();
            }
        }

    } // execute

    public void sendResult(ExecuteResult result, Object content) throws Exception
    {
        try
        {
            Log.log.debug("Send Result: Execute Main");

            // stop time
            // long duration = System.currentTimeMillis() - startTime;

            // send results to rsserver
            String destaddr = getParam(Constants.EXECUTE_RESULTADDR);
            if (StringUtils.isEmpty(destaddr))
            {
                destaddr = Constants.ESB_NAME_RSSERVER;
            }

            String address = getParam(Constants.EXECUTE_ADDRESS);

            // update target if not defined
            String target = getParam(Constants.EXECUTE_TARGET);
            if (StringUtils.isEmpty(target))
            {
                target = MainBase.main.configId.guid;
            }

            // normalize format - remove carriage return (\r)
            if (result.isObject() == false)
            {
                content = ((String) content).replaceAll("\r", "");
            }

            // set destination method
            String classMethod = getParam(Constants.ESB_PARAM_CLASSMETHOD);
            if (StringUtils.isEmpty(classMethod))
            {
                classMethod = "MResult.actionResult";
            }

            // create header
            MMsgHeader header = new MMsgHeader();
            if (StringUtils.isEmpty(destaddr))
            {
                header.setRouteDest(Constants.ESB_NAME_RSSERVER, classMethod);
            }
            else
            {
                header.setRouteDest(destaddr, classMethod);
            }

            // options
            MMsgOptions options = new MMsgOptions();
            options.setAddress(address);
            options.setTarget(target);
            options.setAffinity(getParam(Constants.EXECUTE_AFFINITY));
            options.setActionName(getParam(Constants.EXECUTE_ACTIONNAME));
            options.setActionNamespace(getParam(Constants.EXECUTE_ACTIONNAMESPACE));
            options.setActionSummary(getParam(Constants.EXECUTE_ACTIONSUMMARY));
            options.setActionHidden(getParam(Constants.EXECUTE_ACTIONHIDDEN));
            options.setActionTags(getParam(Constants.EXECUTE_ACTIONTAGS));
            options.setActionRoles(getParam(Constants.EXECUTE_ACTIONROLES));
            options.setActionID(getParam(Constants.EXECUTE_ACTIONID));
            options.setProcessID(getParam(Constants.EXECUTE_PROCESSID));
            options.setWiki(getParam(Constants.EXECUTE_WIKI));
            options.setExecuteID(getParam(Constants.EXECUTE_EXECUTEID));
            options.setParserID(getParam(Constants.EXECUTE_PARSERID));
            options.setAssessID(getParam(Constants.EXECUTE_ASSESSID));
            options.setDuration("" + result.getDuration());
            options.setLogResult(getParam(Constants.EXECUTE_LOGRESULT));
            options.setUserID(getParam(Constants.EXECUTE_USERID));
            options.setProblemID(getParam(Constants.EXECUTE_PROBLEMID));
            options.setReference(getParam(Constants.EXECUTE_REFERENCE));
            options.setReturnCode("" + result.getReturncode());
            options.setCompletion("" + result.isCompletion());
            options.setCommand(result.getCommand());
            options.setTimeout("" + getParam(Constants.EXECUTE_TIMEOUT));
            options.setProcessTimeout("" + getParam(Constants.EXECUTE_PROCESS_TIMEOUT));
            options.setMetricId(getParam(Constants.EXECUTE_METRICID));
            options.setBlankValues(getParam(Constants.ACTION_INVOCATION_OPTIONS_BLANKVALUES));
            header.setOptions(options);

            // content
            MMsgContent msg = new MMsgContent();
            msg.setParams("" + getParam(Constants.EXECUTE_PARAMS));
            msg.setFlows("" + getParam(Constants.EXECUTE_FLOWS));
            msg.setInputs("" + getParam(Constants.EXECUTE_INPUTS));
            msg.setRaw(content);

            // set debug
            StringBuffer debug = result.getDebug();
            if (debug != null)
            {
                msg.setDebug(debug.toString());
            }
            else
            {
                msg.setDebug("");
            }
            
            // Test for serialization from the PARAMS, FLOWS, and INPUTS. If a serialization error occurs, the try/catch will throw a 
            // SerializationException to properly fail the actiontasks execution
            ObjectProperties.serialize(msg);

            // send message
            MainBase.esb.sendMessage(header, msg);
        } 
        
        catch (Exception e)
        {
            Log.log.warn(e.getMessage(), e);
            throw e;
        }
    } // sendResult
    
    public static void waitForActiveCompletion()
    {
        Log.log.info("Wait activity completion");
        while ((activeTasks.get() > 0 || ExecuteRemote.sessionPool.sessionMapSize() > 0) && System.currentTimeMillis() < ExecuteMain.maxExpirationTime.get())
        {
            Log.log.warn("Pending ActionTasks - count: "+activeTasks.get()+" open sessions:  " + ExecuteRemote.sessionPool.size() + 
                            " timeout: " + new Date(ExecuteMain.maxExpirationTime.get()));
            ThreadUtils.sleep(5000);
        }
        
        if (activeTasks.get() > 0 || ExecuteRemote.sessionPool.size() > 0)
        {
            Log.log.warn("SHUTDOWN TIMEOUT - Terminating with pending ActionTasks - count: "+activeTasks.get() +
                            ", open sessions: " + ExecuteRemote.sessionPool.size());
        }
        
    } // waitForActiveCompletion
	
	void createInputFile(String contents, String extension) throws Exception {
		this.createInputFile(contents, extension, "UTF-8");
	}

    void createInputFile(String contents, String extension, String fileEncoding) throws Exception
    {
        hasInputFile = true;

        // init extension
        if (StringUtils.isEmpty(extension))
        {
            extension = ".in";
        }

        // set input filename
        inputfile = "infile_" + Thread.currentThread().getName() + extension;
        inputfile = inputfile.replace('-', '_');

        // replace content variables
        contents = replaceVars(contents);
        Log.log.trace("INPUTFILE: " + contents);

        // create inputfile
        File file = new File(inputfile);
        FileUtils.writeStringToFile(file, contents, fileEncoding);
    } // createInputFile

    String readInputFile(String filename)
    {
        String result = "";
        String filenameProxy = "";

        if (!filename.equals(inputfile) && !filename.startsWith("/"))
        {
            if (!filename.endsWith(".groovy"))
            {
                filename += ".groovy";
            }

            filenameProxy = MainBase.main.getGSEHome() + "/proxy/" + filename;

            filename = MainBase.main.getGSEHome() + "/" + filename;

        }

        // read content from file
        Log.log.trace("Filename: " + filenameProxy);
        File file = new File(filenameProxy);
        if (!file.exists())
        {
            Log.log.trace("Filename: " + filename);
            file = new File(filename);
        }
        if (file.exists())
        {
            try
            {
                result = FileUtils.readFileToString(file, "UTF-8");
            }
            catch (Exception e)
            {
                Log.log.error("Failed to execute REMOTE actiontask: " + e.getMessage(), e);
            }
        }

        return result;
    } // readInputFile

    void getFileResult(ExecuteResult result)
    {
        try
        {
            String filename = getParam(Constants.ACTION_INVOCATION_OPTIONS_RESULTFILE);
            if (filename != null)
            {
                hasResultFile = true;

                // check if ${TMPFILE}
                if (filename.equals(Constants.OPTIONS_TMPFILE))
                {
                    filename = tmpfile;
                    hasTmpFile = true;
                }
                else
                {
                    // substitute \ for /
                    filename = filename.replace('\\', '/');

                    // remove quotes if filename surrounded by quotes
                    if (filename.charAt(0) == '"')
                    {
                        filename = filename.substring(1, filename.length() - 1);
                    }
                }

                // check if need to wait
                int wait = StringUtils.stringToInt(getParam(Constants.ACTION_INVOCATION_OPTIONS_RESULTFILE_WAIT));
                if (wait != 0)
                {
                    Log.log.trace("RESULTFILE wait: " + wait);
                    Thread.sleep(wait * 1000); // secs
                }

                File file = new File(filename);
                if (file.exists())
                {

                    // check if tail
                    String taillength = getParam(Constants.ACTION_INVOCATION_OPTIONS_TAILLENGTH);
                    if (StringUtils.isEmpty(taillength))
                    {
                        String lines = FileUtils.readFileToString(file, "UTF-8");
                        result.setResult(lines);
                    }
                    // tail file
                    else
                    {
                        List lines = FileUtils.readLines(file, "UTF-8");
                        int len = Integer.parseInt(taillength);
                        if (lines.size() < len)
                        {
                            len = lines.size();
                        }

                        for (int i = lines.size() - len; i < lines.size(); i++)
                        {
                            result.append(lines.get(i) + "\n");
                        }
                    }
                }
                // set status to false
                else
                {
                    result.setCompletion(false);
                    result.setResult("FAILED: Unable to retrieve file: " + filename);
                }
            }
        }
        catch (Exception e)
        {
            result.setResult(e.getMessage());
        }
    } // getFileResult

    void deleteFiles()
    {
        // remove inputfile
        if (hasInputFile)
        {
            File file = new File(inputfile);
            if (file.exists())
            {
                file.delete();
            }
        }

        // remove tmpfile
        if (hasTmpFile)
        {
            String doRemove = getParam(Constants.ACTION_INVOCATION_OPTIONS_TMPFILE_REMOVE);
            if (StringUtils.isEmpty(doRemove) || doRemove.equalsIgnoreCase("TRUE") || doRemove.equals("1"))
            {
                File file = new File(tmpfile);
                if (file != null && file.exists())
                {
                    file.delete();
                }
            }
        }
        else if (hasResultFile)
        {
            String doRemove = getParam(Constants.ACTION_INVOCATION_OPTIONS_RESULTFILE_REMOVE);
            if (!StringUtils.isEmpty(doRemove) && (doRemove.equalsIgnoreCase("TRUE") || doRemove.equals("1")))
            {
                File file = new File(getParam(Constants.ACTION_INVOCATION_OPTIONS_RESULTFILE));
                if (file != null && file.exists())
                {
                    file.delete();
                }
            }
        }
    } // deleteFiles

    ExecuteResult executeOS()
    {
        ExecuteResult result = null;

        if (data != null)
        {
            String jobname = getParam("JOBNAME");
            String execpath = getParam("EXECPATH");
            String cmdline = getParam("CMDLINE");
            String input = getParam("INPUT");
            int timeout = StringUtils.stringToInt(getParam("TIMEOUT"));

            // replace variables
            cmdline = replaceVars(cmdline);

            ExecuteOS os = new ExecuteOS(execpath);
            result = os.execute(cmdline, input, true, timeout, jobname);
        }
        return result;
    } // executeOS

    ExecuteResult executeJava()
    {
        ExecuteResult result = null;

        if (data != null)
        {
            String jobname = getParam("JOBNAME");
            String execpath = getParam("EXECPATH");
            String javahome = getParam("JREPATH");
            String filename = getParam("FILENAME");
            String args = getParam("ARGS");
            String input = getParam("INPUT");
            String classpath = getParam(Constants.ACTION_INVOCATION_OPTIONS_CLASSPATH);
            String javaoptions = getParam(Constants.ACTION_INVOCATION_OPTIONS_JAVAOPTIONS);
            int timeout = StringUtils.stringToInt(getParam("TIMEOUT"));

            // replace variables
            args = replaceVars(args);

            // replace classpath
            classpath = replaceVars(classpath);

            ExecuteJava java = new ExecuteJava(execpath, javahome, classpath, javaoptions);
            result = java.execute(filename, args, input, true, timeout, jobname);
        }
        return result;
    } // executeJava

    ExecuteResult executeGroovy()
    {
        ExecuteResult result = null;

        if (data != null)
        {
            String jobname = getParam("JOBNAME");
            String execpath = getParam("EXECPATH");
            String javahome = getParam("JREPATH");
            String filename = getParam("FILENAME");
            String args = getParam("ARGS");
            String input = getParam("INPUT");
            String classpath = getParam(Constants.ACTION_INVOCATION_OPTIONS_CLASSPATH);
            String javaoptions = getParam(Constants.ACTION_INVOCATION_OPTIONS_JAVAOPTIONS);
            int timeout = StringUtils.stringToInt(getParam("TIMEOUT"));

            // replace variables
            filename = replaceVars(filename);
            args = replaceVars(args);

            // replace classpath
            classpath = replaceVars(classpath);

            ExecuteGroovy groovy = new ExecuteGroovy(execpath, javahome, classpath, javaoptions);
            result = groovy.execute(filename, args, input, true, timeout, jobname);
        }
        return result;
    } // executeGroovy

    ExecuteResult executeAnt()
    {
        ExecuteResult result = null;

        if (data != null)
        {
            String jobname = getParam("JOBNAME");
            String execdir = getParam("EXECPATH");
            String javahome = getParam("JREPATH");
            String filename = getParam("FILENAME");
            String args = getParam("ARGS");
            String input = getParam("INPUT");
            String classpath = getParam(Constants.ACTION_INVOCATION_OPTIONS_CLASSPATH);
            String javaoptions = getParam(Constants.ACTION_INVOCATION_OPTIONS_JAVAOPTIONS);
            int timeout = StringUtils.stringToInt(getParam("TIMEOUT"));

            // replace variables
            filename = replaceVars(filename);
            args = replaceVars(args);

            // replace classpath
            classpath = replaceVars(classpath);

            ExecuteAnt ant = new ExecuteAnt(execdir, javahome, classpath, javaoptions);
            result = ant.execute(filename, args, "", input, true, timeout, jobname);
        }
        return result;
    } // executeAnt

    ExecuteResult executeRemote(ScriptDebug debug)
    {
        ExecuteResult result = null;

        if (data != null)
        {
            String content = null;
            String actionName = getParam(Constants.EXECUTE_ACTIONNAME);
            String jobname = getParam(Constants.EXECUTE_JOBNAME);
            String filename = getParam(Constants.EXECUTE_FILENAME);
            String args = getParam(Constants.EXECUTE_ARGS);
            String targetAddr = getParam(Constants.EXECUTE_TARGET);
            String paramsStr = getParam(Constants.EXECUTE_PARAMS);
            String flowsStr = getParam(Constants.EXECUTE_FLOWS);
            String inputsStr = getParam(Constants.EXECUTE_INPUTS);
            String reference = getParam(Constants.EXECUTE_REFERENCE);
            String processid = getParam(Constants.EXECUTE_PROCESSID);
            String problemid = getParam(Constants.EXECUTE_PROBLEMID);
            String wiki = getParam(Constants.EXECUTE_WIKI);
            boolean contentCacheable = getParamBoolean(Constants.EXECUTE_CACHEABLE);
            boolean destroy = getParamBoolean(Constants.ACTION_INVOCATION_OPTIONS_TIMEOUT_DESTROY_THREAD);

            int timeout = StringUtils.stringToInt(getParam(Constants.EXECUTE_TIMEOUT));
            int processTimeout = StringUtils.stringToInt(getParam(Constants.EXECUTE_PROCESS_TIMEOUT));

            // get INPUTFILE content
            if (StringUtils.isEmpty(filename) || filename.equals(Constants.OPTIONS_INPUTFILE))
            {
                content = getParam(Constants.ACTION_INVOCATION_OPTIONS_INPUTFILE);
            }
            else
            {
                content = readInputFile(replaceVars(filename));
                
                // RBA-14752 OS AT followed by Remote AT gives missing content in Remote AT
                if (StringUtils.isBlank(content))
                {
                    content = getParam(Constants.ACTION_INVOCATION_OPTIONS_INPUTFILE);
                }
            }

            // replace variables
            content = replaceVars(content);
            args = replaceVars(args);

            Map inputs = (Map) StringUtils.stringToObj(inputsStr);
            if (inputs == null)
            {
                inputs = new HashMap();
            }
            Map params = (Map) StringUtils.stringToObj(paramsStr);
            if (params == null)
            {
                params = new HashMap();
            }
            Map flows = (Map) StringUtils.stringToObj(flowsStr);
            if (flows == null)
            {
                flows = new HashMap();
            }

            Log.log.debug("ExecuteRemote name: " + actionName);
            Log.log.debug("  inputs: " + StringUtils.mapToLogString(inputs));
            Log.log.debug("  params: " + StringUtils.mapToLogString(params));
            Log.log.debug("  flows: " + StringUtils.mapToLogString(flows));

            debug.println("ExecuteRemote name: " + actionName);
            debug.println("  inputs: " + StringUtils.mapToLogString(inputs));
            debug.println("  params: " + StringUtils.mapToLogString(params));
            debug.println("  flows: " + StringUtils.mapToLogString(flows));

            // execute
            ExecuteRemote executeRemote = new ExecuteRemote();
            result = executeRemote.execute(processid, content, actionName, contentCacheable, args, inputs, params, flows, debug, reference, timeout, processTimeout, jobname, problemid, wiki, destroy);

            // update params and flows
            setParam(Constants.EXECUTE_PARAMS, StringUtils.objToString(params));
            setParam(Constants.EXECUTE_FLOWS, StringUtils.objToString(flows));
            setParam(Constants.EXECUTE_INPUTS, StringUtils.objToString(inputs));
        }

        return result;
    } // executeRemote

    ExecuteResult executeExternal()
    {
        ExecuteResult result = new ExecuteResult();

        if (data != null)
        {
            try
            {
                String externalType = getParam(Constants.ACTION_INVOCATION_OPTIONS_EXTERNAL_TYPE);
                ExternalExecutor executor = ExternalExecutorFactory.getExecutor(externalType, data);
                String output = executor.execute();
                result.setResult(output);
                result.setCompletion(true);
                result.setReturncode(Constants.ASSESS_RETURNCODE_EXECUTE_SUCCESS);
            }
            catch (Exception ex)
            {
                Log.log.error(ex.getMessage(), ex);
                result.setCompletion(false);
                result.setResult(ex.getMessage());
                result.setReturncode(Constants.ASSESS_RETURNCODE_EXECUTE_EXCEPTION);
            }
        }

        return result;
    } // executeExternal

    ExecuteResult executeCScript()
    {
        ExecuteResult result = null;

        if (data != null)
        {
            String jobname = getParam("JOBNAME");
            String execpath = getParam("EXECPATH");
            String cmdline = getParam("CMDLINE");
            String input = getParam("INPUT");
            int timeout = StringUtils.stringToInt(getParam("TIMEOUT"));

            // replace variables
            cmdline = replaceVars(cmdline);

            ExecuteCScript cscript = new ExecuteCScript(execpath);
            result = cscript.execute(this.inputfile, input, true, timeout, jobname);
        }
        return result;
    } // executeCScript

    ExecuteResult executePowershell()
    {
        ExecuteResult result = null;

        if (data != null)
        {
            String jobname = getParam("JOBNAME");
            String execpath = getParam("EXECPATH");
            String cmdline = getParam("CMDLINE");
            String input = getParam("INPUT");
            int timeout = StringUtils.stringToInt(getParam("TIMEOUT"));

            // replace variables
            cmdline = replaceVars(cmdline);

            ExecutePowershell cscript = new ExecutePowershell(execpath);
            result = cscript.execute(this.inputfile, input, true, timeout, jobname);
        }
        return result;
    } // executePowershell

    ExecuteResult executeWinExec()
    {
        ExecuteResult result = null;

        if (data != null)
        {
            String jobname = getParam(Constants.EXECUTE_JOBNAME);
            String execpath = getParam(Constants.EXECUTE_EXECPATH);
            String cmdline = getParam(Constants.EXECUTE_CMDLINE);
            String input = getParam(Constants.EXECUTE_INPUT);
            int timeout = StringUtils.stringToInt(getParam(Constants.EXECUTE_TIMEOUT));
            String paramStr = getParam(Constants.EXECUTE_PARAMS);

            // get params
            Map params = (Map) StringUtils.stringToObj(paramStr);
            // Map params = StringUtils.stringToMap(paramStr);
            String ipaddress = (String) params.get("IPADDRESS");
            String username = (String) params.get("USERNAME");
            String password = (String) params.get("PASSWORD");

            // replace variables
            cmdline = replaceVars(cmdline);

            ExecuteWinExec winexec = new ExecuteWinExec(execpath, timeout, ipaddress, username, password);
            result = winexec.execute(cmdline, input, true, timeout, jobname);
        }
        return result;
    } // executeWinExec

    ExecuteResult executeWinTask()
    {
        ExecuteResult result = null;

        if (data != null)
        {
            String jobname = getParam("JOBNAME");
            String execpath = getParam("EXECPATH");
            String cmdline = getParam("CMDLINE");
            int timeout = StringUtils.stringToInt(getParam("TIMEOUT"));

            // replace variables
            cmdline = replaceVars(cmdline);

            ExecuteWinTask wintask = new ExecuteWinTask(execpath);
            result = wintask.execute(cmdline, true, timeout, jobname);
        }
        return result;
    } // executeWinTask

    String replaceVars(String line)
    {
        String result = line;

        if (!StringUtils.isEmpty(line))
        {
            // ${TMPFILE}
            if (line.contains(Constants.OPTIONS_TMPFILE))
            {
                // initialize
                String prefix = getParam(Constants.ACTION_INVOCATION_OPTIONS_TMPFILE_PREFIX);
                if (StringUtils.isEmpty(prefix))
                {
                    prefix = "tmpfile_";
                }
                String postfix = getParam(Constants.ACTION_INVOCATION_OPTIONS_TMPFILE_POSTFIX);
                if (StringUtils.isEmpty(postfix))
                {
                    postfix = ".tmp";
                }

                tmpfile = prefix + Thread.currentThread().getName() + postfix;
                tmpfile = tmpfile.replace('-', '_');
                result = result.replace(Constants.OPTIONS_TMPFILE, tmpfile);
            }

            // ${INPUTFILE}
            if (line.contains(Constants.OPTIONS_INPUTFILE) && inputfile != null)
            {
                result = result.replace(Constants.OPTIONS_INPUTFILE, inputfile);
            }

            // ${INSTALLDIR}
            if (line.contains(Constants.OPTIONS_INSTALLDIR))
            {
                result = result.replace(Constants.OPTIONS_INSTALLDIR, MainBase.main.configGeneral.home);
            }

            // $CONFIG{<property_name>}
            String matchContent = result;
            result = "";

            Matcher matcher = VAR_REGEX_CONFIG.matcher(matchContent);
            int last = 0;
            while (matcher.find())
            {
                String name = matcher.group(1);
                String value = MainBase.main.configProperty.getProperty(name);

                result += matchContent.substring(last, matcher.start()) + value;
                last = matcher.end();
            }
            result += matchContent.substring(last, matchContent.length());
        }

        return result;
    } // replaceVars

    String blankVals(String line, String blankValues)
    {
        final String stars = "*****";

        String result = line;
        if (result != null && blankValues != null)
        {
            String[] values = blankValues.split(",");
            for (int i = 0; i < values.length; i++)
            {
                String value = values[i].trim();
                if (!StringUtils.isEmpty(value))
                {
                    // match config variable value
                    if (value.startsWith("$CONFIG"))
                    {
                        // $CONFIG{<property_name>}
                        String matchContent = value;

                        Matcher matcher = VAR_REGEX_CONFIG.matcher(matchContent);
                        while (matcher.find())
                        {
                            String name = matcher.group(1);
                            String matchValue = MainBase.main.configProperty.getProperty(name);
                            if (matchValue != null)
                            {
                                result = result.replace(matchValue, stars);
                            }
                        }
                    }

                    // match value literal
                    else
                    {
                        result = result.replace(value, stars);
                    }
                }
            }
        }

        return result;
    } // blankVals

    String getParam(String name)
    {
        String result = "";

        Object value = data.get(name.toUpperCase());
        if (value != null)
        {
            if (value instanceof String)
            {
                result = (String) value;

                if (result.equalsIgnoreCase("null"))
                {
                    result = "";
                }
            }
            else
            {
                result = "" + value;
            }
        }

        return result;
    } // getParam

    void setParam(String name, String value)
    {
        data.put(name.toUpperCase(), value);
    } // setParam

    Object getParamObject(String name)
    {
        return data.get(name.toUpperCase());
    } // getParamObject
    
    boolean getParamBoolean(String name)
    {
        boolean result = false;
        
        Object value = data.get(name.toUpperCase());
        if (value != null)
        {
            if (value instanceof String)
            {
                if (((String)value).equalsIgnoreCase("true"))
                {
                    result = true;
                }
            }
            else if (value instanceof Boolean)
            {
                result = ((Boolean)value).booleanValue();
            }
        }
        
        return result;
    } // getParamBoolean
    
    public void kill(Map params)
    {
        String type = null;
        String jobname = null;
        Execute execute = null;

        try
        {
            type = (String) params.get("TYPE");
            jobname = (String) params.get("JOBNAME");
            execute = (Execute) params.get("EXECUTE");

            if (type.equals("PROCESS"))
            {
                Process process = getJobProcess(jobname);
                if (process != null)
                {
                    Log.log.info("Aborting ExecuteMain system process");
                    process.destroy();
                    process.getInputStream().close();
                    process.getOutputStream().close();
                    process.getErrorStream().close();
                }
                ExecuteMain.removeJobProcess(jobname);
            }
            else if (type.equals("THREAD"))
            {
                Thread thread = getJobThread(jobname);
                if (thread != null)
                {
                    Log.log.info("Aborting ExecuteMain thread id: " + thread.getId() + " name: " + thread.getName());
                    thread.interrupt();
                }
                ExecuteMain.removeJobThread(jobname);
            }

            // indicate that the job was killed
            if (execute != null)
            {
                execute.setAborted(true);
            }
        }
        catch (Exception e)
        {
            Log.log.error("Failed to kill actiontask: " + e.getMessage(), e);
        }
    } // kill

    public static Process getJobProcess(String jobname)
    {
        return (Process) jobProcess.get(jobname.toUpperCase());
    } // getJobProcess

    public static void setJobProcess(String jobname, Process process)
    {
        if (jobname != null && process != null)
        {
            jobProcess.put(jobname.toUpperCase(), process);
        }
    } // setJobProcess

    public static void removeJobProcess(String jobname)
    {
        if (jobname != null)
        {
            jobProcess.remove(jobname.toUpperCase());
        }
    } // removeJobProcess

    /*
     * public static Boolean getJobKilled(String jobname) { return
     * (Boolean)jobKilled.get(jobname.toUpperCase()); } // getJobKilled
     * 
     * public static void setJobKilled(String jobname) { if (jobname != null) {
     * jobKilled.put(jobname.toUpperCase(), true); } } // setJobKilled
     * 
     * public static void removeJobKilled(String jobname) { if (jobname != null)
     * { jobKilled.remove(jobname.toUpperCase()); } } // removeJobKilled
     */

    public static Thread getJobThread(String jobname)
    {
        return (Thread) jobThread.get(jobname.toUpperCase());
    } // getJobThread

    public static void setJobThread(String jobname, Thread thread)
    {
        if (jobname != null && thread != null)
        {
            jobThread.put(jobname.toUpperCase(), thread);
        }
    } // setJobThread

    public static void removeJobThread(String jobname)
    {
        if (jobname != null)
        {
            jobThread.remove(jobname.toUpperCase());
        }
    } // removeJobThread

} // ExecuteMain
