/******************************************************************************
* (C) Copyright 2014
*
* Resolve Systems, LLC
*
* All rights reserved
* This software is distributed under license and may not be used, copied,
* modified, or distributed without the express written permission of
* Resolve Systems, LLC.
*
******************************************************************************/

package com.resolve.rsremote;

import java.util.HashMap;
import java.util.Map;

import com.resolve.rsbase.MainBase;
import com.resolve.util.Log;
import com.resolve.util.StringUtils;

/**
 * Called by Rest Webservice to get status of a component.
 *
 */
public class MMCP
{
    /**
     *
     * @param params
     *            RETURN_QUEUE: is where status will be submitted, typically RSMGMT GUID.
     * @return
     */
    public static void getComponentStatus(Map<String, String> params)
    {
        if(params == null || params.size() == 0)
        {
            throw new RuntimeException("params is Null or Empty, can't do anything, check the calling code.");
        }
        else
        {
            String returnQueue = params.get("RETURN_QUEUE");
            if(StringUtils.isEmpty(returnQueue))
            {
                Log.log.warn("params did not have a key RETURN_QUEUE with value in it.");
            }
            else
            {
                Map<String, String> reply = new HashMap<String, String>();
                reply.put(MainBase.main.configId.getGuid(), "RUNNING");
                //this will go to the RSMGMT that asked for the status.
                MainBase.esb.sendInternalMessage(returnQueue, "MMCP.receiveStatus", reply);
            }
        }
    }
    
    
}
