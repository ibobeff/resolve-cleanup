package com.resolve.rsremote;

import java.util.HashMap;
import java.util.Map;

import com.resolve.util.ERR;
import com.resolve.util.Log;

public class RSRemoteSelfCheck
{
	public static int timeout;
	
	@SuppressWarnings("unchecked")
	public void pingRSControl()
	{
		try
		{
			Log.log.trace("Sending ESB ping to RSCONTROL");
			// send a message to any running RSControl and wait for response.
			Map<String, String> response = Main.esb.call("RSCONTROL", "MAction.esbPing", new HashMap<String, String>(), timeout*1000);
			
			if (response == null)
			{
				Log.log.error("ESB Ping has not been received from RSCONTROL since the last ping for " + timeout + " seconds. MQ could be too busy or component may not be running.");
				Log.alert(ERR.E20010);
			}
			else
			{
				Log.log.trace("Received ESB ping from RSCONTROL");
			}
		}
		catch (Exception e)
		{
			Log.log.error(e.getMessage(), e);
		}
	}
}
