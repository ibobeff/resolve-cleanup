/******************************************************************************
* (C) Copyright 2014
*
* Resolve Systems, LLC
*
* All rights reserved
* This software is distributed under license and may not be used, copied,
* modified, or distributed without the express written permission of
* Resolve Systems, LLC.
*
******************************************************************************/

package com.resolve.rsremote;

import java.io.File;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.Hashtable;
import java.util.List;
import java.util.Map;

import org.apache.commons.collections.MapUtils;
import org.apache.commons.lang3.tuple.Pair;
import org.dom4j.Element;

import com.resolve.gateway.BaseClusteredGateway;
import com.resolve.rsbase.MainBase;
import com.resolve.util.Constants;
import com.resolve.util.FileUtils;
import com.resolve.util.GMTDate;
import com.resolve.util.Log;
import com.resolve.util.StringUtils;
import com.resolve.util.XDoc;

public class MRegister
{
    private static SimpleDateFormat sdf = new SimpleDateFormat(Constants.HEARTBEAT_FORMAT);
    private static File blueprintFile = null;
    private static Date blueprintLastUpdated;
    public static Map<String, String> gtwQName2MsgHandlerNames = new HashMap<String, String>();
    public static volatile boolean isBlueprintAcknowledged = false;

    public String register(Map params)
    {
        register();

        return "SUCCESS: Registration completed";
    } // register

    @SuppressWarnings("unchecked")
    public static void register()
    {
        try
        {
            Log.log.debug("Registration");

            File file = new File(Main.main.getProductHome() + "/config/config.xml");
            XDoc doc = new XDoc(file);
            if (file.exists())
            {
                ArrayList content = new ArrayList();

                // set GUID and NAME
                Hashtable reg = new Hashtable();
                reg.put("GUID", Main.main.configId.guid);
                reg.put("NAME", Main.main.configId.name);
                reg.put("TYPE", Main.main.release.type);
                reg.put("IPADDRESS", Main.main.configId.ipaddress);
                reg.put("VERSION", Main.main.release.version);
                reg.put("DESCRIPTION", Main.main.configId.description);
                reg.put("LOCATION", Main.main.configId.location);
                reg.put("GROUP", Main.main.configId.group);
                reg.put("CREATETIME", GMTDate.getDate());
                reg.put("UPDATETIME", GMTDate.getDate());
                reg.put("OS", System.getProperty("os.name"));
                Map<String, Object> gwInstances = Main.getGatewayInstances();
                List<Object> receives = doc.getNodes("./RECEIVE/*");
                for (Object o: receives) {
                    Element e = (Element) o;
                    String gwXpath = e.getUniquePath();
                    String queueName = doc.getStringValue(gwXpath + "/@QUEUE", "").toUpperCase();
                    Object gwObject = gwInstances.get(queueName);
                    if (gwObject != null && (gwObject instanceof BaseClusteredGateway)) {
                        BaseClusteredGateway gw = (BaseClusteredGateway) gwObject;
                        // non-clustered gateways are stand-alone. Only clustered gateways can have PRIMARY role.
                        if (queueName.equals(gw.getQueueName())) {
                            e.addAttribute("PRIMARY", Boolean.toString(gw.isPrimary()));
                            e.addAttribute("ORIGINALPRIMARY", Boolean.toString(gw.isOriginalPrimary()));
                        }
                    }
                }
                reg.put("CONFIG", doc.toString());
                if (StringUtils.isNotBlank(((com.resolve.rsremote.ConfigGeneral)Main.main.configGeneral).getOrg()))
                {
                    reg.put("ORG", ((com.resolve.rsremote.ConfigGeneral)Main.main.configGeneral).getOrg());
                }
                
                content.add(reg);

                // get parent GUID and NAME
                if (Main.esb.sendInternalMessage(Constants.ESB_NAME_SYSTEM_RSCONTROL, "MRegister.register", content) == false)
                {
                    Log.log.warn("Failed to send registration message");
                }
            }
            else
            {
                Log.log.error("Missing config.xml file: " + file.getPath());
            }
        }
        catch (Exception e)
        {
            Log.log.error("Failed to register RSRemote: " + e.getMessage(), e);
        }
    } // register

    public static Hashtable terminate(Map params)
    {
        String msg = (String) params.get("MSG");
        String save = (String) params.get("SAVE");

        if (StringUtils.isNotBlank(msg))
        {
            Log.log.info(msg);
        }

        if (save != null && save.equalsIgnoreCase("FALSE"))
        {
            MainBase.overrideSaveConfigOnExit = true;
        }

        Main main = (Main) Main.main;
        main.verifyLicense();

        return null;
    } // terminate

    public String logHeartbeat(Map params)
    {
        logHeartbeat();

        return "SUCCESS: Heartbeat completed";
    } // logHeartbeat

    public static void logHeartbeat()
    {
        Date time = new Date();
        Log.log.info("HEARTBEAT: " + sdf.format(time));
        System.out.println("HEARTBEAT:" + sdf.format(time));
    } // logHeartbeat
    
    public static String getBlueprintData() {
        
        String blueprintData = "Error: No blueprint data found.";
        
        if(blueprintFile == null)
        {
            blueprintFile = FileUtils.getFile(Main.main.getResolveHome() + "/rsmgmt/config/blueprint.properties");
        }
        
        try
        {
            blueprintData = FileUtils.readFileToString(blueprintFile, "utf8");
        }
        catch (IOException e)
        {
            Log.log.error(e.getMessage(), e);
        }
        
        return blueprintData;
    }

    public static void updateBlueprint()
    {
        if(blueprintFile == null)
        {
            blueprintFile = FileUtils.getFile(Main.main.getResolveHome() + "/rsmgmt/config/blueprint.properties");
        }

        Log.log.debug("Checking changes to blueprint.properties");
        if(blueprintLastUpdated == null || FileUtils.isFileNewer(blueprintFile, blueprintLastUpdated))
        {
            try
            {
                //we will be sending the information to RSCONTROL to store it.
                //Log.log.error("DB Connection Missing, unable to Store Blueprint in DB");
                Log.log.info("Sending blueprint.properties to RSCONTROL to Store in DB");
                String blueprintString = FileUtils.readFileToString(blueprintFile);

                Map<String, String> params = new HashMap<String, String>();
                params.put("GUID", MainBase.main.configId.getGuid());
                params.put("BLUEPRINT", blueprintString);
                
                Map<String, String> response = MainBase.esb.call(Constants.ESB_NAME_RSCONTROL, "MConfig.storeBlueprint", params, 30000L);
                blueprintLastUpdated = new Date();
                
                if(Log.log.isDebugEnabled())
                {
                    if(response != null)
                    {
                        Log.log.debug("Update blueprint response from RSCONTROL");
                        for(String key : response.keySet())
                        {
                            Log.log.debug(response.get(key));
                        }
                    }
                }
            }
            catch (Exception e)
            {
                Log.log.error(e.getMessage(), e);
            }
        }
    }

    /**
     * Called by RSControl after receiving the update blueprint message.
     */
    public static void requestState(Map<String, Object> params)
    {
        String requester = (String) params.get("GUID");
        Map<String, String> params1 = new HashMap<String, String>();
        params1.put("GUID", Main.main.configId.getGuid());
        MainBase.esb.sendInternalMessage(requester, "MConfig.receiveState", params1);
        Log.log.debug("Sent alive state message to RSCONTROL:" + requester);
    }
    
    /**
     * Get gateway event type to message handler names.
     * 
     * @return			Map of gateway event type to message handler names
     */
    public Map<String, String> getQueueName2MsgHandlerNames(Map<String, String> params) {
    	if (Log.log.isDebugEnabled()) {
    		Log.log.debug(String.format("Received request to get gateway queue name to messages handler names, returning " +
    									"[%s]", StringUtils.mapToString(gtwQName2MsgHandlerNames, "=", ", ")));
    	}
    	
    	return gtwQName2MsgHandlerNames;
    }
    
    /**
     * 
     */
    public Map<String, Pair<String, Map<String, String>>> getFilterCompanionModelDetails(Map<String, String> params)  {
    	
    	Map<String, Pair<String, Map<String, String>>> fcmName2Details = Main.getFilterCompanionModelDetails();
    	
    	if (MapUtils.isNotEmpty(fcmName2Details) && Log.log.isDebugEnabled()) {
    		Log.log.debug(String.format("Received request to get gateway filter companion model details, returning " +
    									"details for models [%s]", StringUtils.setToString(fcmName2Details.keySet(), ", ")));
    	}
    	
    	return fcmName2Details;
    }
} // MRegister
