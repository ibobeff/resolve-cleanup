package com.resolve.rsremote;

import com.resolve.util.XDoc;

public class ConfigSelfCheck extends ConfigGeneral
{
    private static final long serialVersionUID = 512340348243761843L;

    // Ping RSCONTROL
    boolean pingActive;
    int pingTimeout;
    int pingInterval;

    public ConfigSelfCheck(XDoc config) throws Exception
    {
        super(config);

        define("pingActive", BOOLEAN, "./SELFCHECK/PING/@ACTIVE");
        define("pingInterval", INTEGER, "./SELFCHECK/PING/@INTERVAL");
        define("pingTimeout", INTEGER, "./SELFCHECK/PING/@TIMEOUT");
    }

    public boolean isPingActive()
    {
        return pingActive;
    }

    public void setPingActive(boolean pingActive)
    {
        this.pingActive = pingActive;
    }

    public int getPingTimeout()
    {
        return pingTimeout;
    }

    public void setPingTimeout(int pingTimeout)
    {
        this.pingTimeout = pingTimeout;
    }

    public int getPingInterval()
    {
        return pingInterval;
    }

    public void setPingInterval(int pingInterval)
    {
        this.pingInterval = pingInterval;
    }

}
