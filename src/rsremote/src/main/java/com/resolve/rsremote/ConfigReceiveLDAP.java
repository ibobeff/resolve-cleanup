/******************************************************************************
* (C) Copyright 2014
*
* Resolve Systems, LLC
*
* All rights reserved
* This software is distributed under license and may not be used, copied,
* modified, or distributed without the express written permission of
* Resolve Systems, LLC.
*
******************************************************************************/

package com.resolve.rsremote;

import java.io.File;

import org.apache.commons.io.FileUtils;

import com.resolve.rsbase.MainBase;
import com.resolve.util.Log;
import com.resolve.util.XDoc;

public class ConfigReceiveLDAP extends ConfigReceiveGateway
{
    private static final long serialVersionUID = 1L;

    private static final String RECEIVE_LDAP_NODE = "./RECEIVE/LDAP/";

    boolean active = false;
    String queue = "LDAP";

    File propertiesFile = null;

    public ConfigReceiveLDAP(XDoc config) throws Exception
    {
        super(config);
    } // ConfigReceiveLDAP

    public String getRootNode()
    {
        return RECEIVE_LDAP_NODE;
    }

    public void load() throws Exception
    {
        try
        {
            loadAttributes();
        }
        catch (Throwable e)
        {
            //deactivate it
            setActive(false);
            Log.log.warn("Couldn not load configuration for ITM gateway, check blueprint. " + e.getMessage(), e);
        }
    } // load

    public void save() throws Exception
    {
        // create email directory
        File dir = new File(MainBase.main.release.serviceName + "/config/ldap");
        if (!dir.exists())
        {
            FileUtils.forceMkdir(dir);
        }

        saveAttributes();
    } // save

    public boolean isActive()
    {
        return active;
    }

    public void setActive(boolean active)
    {
        this.active = active;
    }

    public File getPropertiesFile()
    {
        return propertiesFile;
    }

    public void setPropertiesFile(File propertiesFile)
    {
        this.propertiesFile = propertiesFile;
    }

    public String getQueue()
    {
        return queue;
    } // getQueue

    public void setQueue(String queue)
    {
        this.queue = queue;
    } // setQueue
} // ConfigReceiveLDAP
