package com.resolve.rslog.model;

import java.sql.ResultSet;
import java.sql.SQLException;

import org.springframework.jdbc.core.RowMapper;

public class PropertiesRowMapper implements RowMapper<Properties>
{
    @Override
    public Properties mapRow(ResultSet row, int rowNum) throws SQLException
    {
        Properties p = new Properties();
        p.setUValue(row.getString("u_value"));
        
        return p;
    }
}
