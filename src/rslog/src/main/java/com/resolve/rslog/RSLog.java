package com.resolve.rslog;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
// @EnableEurekaClient
public class RSLog
{
    private static final Logger LOG = LogManager.getLogger(RSLog.class.getName());
    
    public static void main(String[] args) {
        try {
            System.setProperty("spring.config.name", "web-server");
            SpringApplication.run(RSLog.class, args);
            LOG.info("RSLog Started");
        } catch (Throwable t) {
            LOG.error("Error:", t);
        }
    }
}