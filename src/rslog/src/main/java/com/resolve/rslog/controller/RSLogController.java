package com.resolve.rslog.controller;

import java.io.IOException;
import java.util.List;
import java.util.Map;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.lang3.StringUtils;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.resolve.rslog.service.LogReaderService;

@Controller
class RSLogController {
    private static final Logger LOG = LogManager.getLogger(RSLogController.class.getName());
    private final String PING_RESPONSE = "Hello from RSLog";
    @Autowired
    LogReaderService logReaderService;

    /**
     * Controller API endpoint to handle get collect the log files, as per the criteria, and zip it up in a file, log.zip, and send it to the caller.
     * 
     * @param compGUID
     *            : String representing GUID of the component whoes log is needed.
     * @param component
     *            : Optional String representing the component name. If not provided, all the files from given GUID will be considered.
     * @param from
     *            : Optional Long representing start time from where log files are requested. If not provided, it will consider files from the start of the time.
     * @param to
     *            : Optional Long representing end time upto which the log files are requested. If not provided, all the files upto current time are considered. If both, from and to, are not provided,
     *            all the files will be considered.
     * @param request
     * @param response
     */
    @RequestMapping(value = "/getLog", method = { RequestMethod.POST, RequestMethod.GET }, headers = "Accept=application/json")
    void getLog(@RequestParam String compGUID, 
                        @RequestParam(required = false, defaultValue = "") List<String> components,
                        @RequestParam(required = false) Long from,
                        @RequestParam(required = false) Long to,
                        HttpServletRequest request, HttpServletResponse response) {

        try {
            if (LOG.isDebugEnabled()) {
                String debugMsg = String.format("Received a request from IP: %s with params compGUID: %s, components: %s, from: %s, to: %s", request.getRemoteAddr(), StringUtils.isBlank(compGUID) ? "" : compGUID, components, from == null ? "null" : from.toString(), to == null ? "null" : to.toString());
                LOG.debug(debugMsg);
            }

            logReaderService.readLog(compGUID, components, from, to, request.getSession(true).getId(), response);
        }
        catch (Exception e) {
            LOG.error("Error while getting the log zip file.", e);
            try {
                response.sendError(500, "Error while getting the log zip.");
            }
            catch (IOException e1) {
                // Do nothing here
            }
        }
    }
    
    /**
     * API to be invoked by RSMgmt as part of a self check. Only RSMgmt installed on a primary host should be able to call it.
     * 
     * @param request: HttpServletRequest
     * @param response: HttpServletResponse
     * @return: An arbritrary String indicating the caller that RSLog is listening on the given host and port.
     */
    @RequestMapping(value = "/rslog/ping", method = { RequestMethod.GET })
    @ResponseBody
    public String ping(HttpServletRequest request, HttpServletResponse response) {
        if (LOG.isDebugEnabled()) {
            LOG.debug(String.format("Received a ping request from IP: %s", request.getRemoteAddr()));
        }
        return PING_RESPONSE;
    }
    
    /**
     * API to get the list of log files for specified list of RSRemote GUIDs and an optional gateway type.
     * 
     * @param guids: List of Strings representing RSRemote GUIDs
     * @param components: Optional List of strings representing a gateway type. E.g. netcool,http,servicenow etc. If all is mentioned, all gateway logs will be returned.
     * @param from: Long representing the date from which the logs are needed.
     *        If only from date is provided, logs from the selected date upto current date will be selected.
     * @param to: Long representing a date upto which the logs are needed.
     *        If only to date is provided, logs from the start of the start of the universe upto the specified date will be selected.
     * @param needRemoteLogs: Optional Boolean flag specifying whether the RSRemote logs are needed or not.
     * @param request: HttpServletRequest
     * @param response: HttpServletResponse
     * @return: A Map of String and an Object, where key is a GUID and value is a list of log file names.<br>
     * <pre>{@code
     *      Sample output:
     *      {
     *          "303FB65F733E2753796A0287366945B4": [
     *              "303FB65F733E2753796A0287366945B4_2019-03-27_HttpGateway.log",
     *              "303FB65F733E2753796A0287366945B4_2019-03-26_HttpGateway.log",
     *              "303FB65F733E2753796A0287366945B4_2019-03-26.log",
     *              "303FB65F733E2753796A0287366945B4_2019-03-25_HttpGateway.log",
     *              "303FB65F733E2753796A0287366945B4_2019-03-25.log"
     *          ]
     *      }
     *
     *</pre>
     * 
     */
    
    @RequestMapping(value = "/listLogFiles", method = { RequestMethod.GET, RequestMethod.POST }, headers = "Accept=application/json")
    @ResponseBody
    public Map<String, Object> listLogFiles(@RequestParam List<String> guids,
                    @RequestParam(required = false ) List<String> components,
                    @RequestParam (required = false) Long from,
                    @RequestParam (required = false) Long to,
                    @RequestParam (required = false, defaultValue = "false") Boolean needRemoteLogs,
                    HttpServletRequest request, HttpServletResponse response) {
        Map<String, Object> logFileList = null;
        try {
            if (LOG.isDebugEnabled()) {
                String debugMsg = String.format("Received a request to list log files from IP: %s with params guids: %s and components: %s", request.getRemoteAddr(), guids, components);
                LOG.debug(debugMsg);
            }

            logFileList = logReaderService.listLogFiles(guids, components, from, to, needRemoteLogs);
        }
        catch (Exception e) {
            LOG.error("Error while getting list of log files.", e);
            try {
                response.sendError(500, "Error while getting list of log files.");
            }
            catch (IOException e1) {
                // Do nothing here
            }
        }
        return logFileList;
    }
    
    /**
     * API to download specified log file(s).
     * 
     * @param files: List of log file names needed to be downloaded. 
     * @param request: HttpServletRequest
     * @param response: HttpServletResponse
     * @return: Zip file with the name 'log.zip' will be writte to the response output stream.
     * @throws ServletException
     */
    @RequestMapping(value = "/downloadLogFiles", method = { RequestMethod.POST, RequestMethod.GET }, headers = "Accept=application/json")
    void downloadLogFiles(@RequestParam List<String> files,
                        HttpServletRequest request, HttpServletResponse response) {
        try {
            if (LOG.isDebugEnabled()) {
                String debugMsg = String.format("Received a downloadLogFiles request from IP: %s with params fileNames: %s", request.getRemoteAddr(), files);
                LOG.debug(debugMsg);
            }

            logReaderService.downloadLogFiles(files, request.getSession(true).getId(), response);
        }
        catch (Exception e) {
            LOG.error("Error while getting the log zip file.", e);
            try {
                response.sendError(500, "Error while getting the log zip.");
            }
            catch (IOException e1) {
                // Do nothing here
            }
        }
    }
}
