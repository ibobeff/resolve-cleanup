package com.resolve.rslog.service;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.lang.management.ManagementFactory;
import java.net.HttpURLConnection;
import java.net.InetAddress;
import java.net.URL;
import java.util.Date;
import java.util.UUID;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.scheduling.annotation.EnableScheduling;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

import com.resolve.rslog.config.Config;
import com.resolve.rslog.config.RSLogConfig;
import com.resolve.util.Log;
import com.resolve.util.StringUtils;

@EnableScheduling
@Component
public class CaptureJvmMetricService {
    private static final Logger LOG = LogManager.getLogger(CaptureJvmMetricService.class.getName());
    RSLogConfig rsLogConfig = Config.getRsLogConfig();
    private String esEndpoint = "/metricjvm/metricjvm/";
    private String jvmIndexDocTemplate = "{\"src\": \"%s\"," +           // component source
                                         "\"ts\": %d," +                 // document insertion time
                                         "\"threadCount\": %d," +        // current thread count
                                         "\"freeMem\": %d}";             // current free memory
    
    @Scheduled (fixedRate = 300000) // Run every 5 minutes.
    public void reportCurrentTime() {
        if (LOG.isDebugEnabled()) {
            LOG.debug(String.format("RSLog JVM metric indexer started."));
        }
        
        URL url = null;
        HttpURLConnection conn = null;
        
        try {
            if (StringUtils.isNotBlank(rsLogConfig.getEsHost()) && StringUtils.isNotBlank(rsLogConfig.getEsPort())) {
                long freeMem = (Runtime.getRuntime().maxMemory() - Runtime.getRuntime().totalMemory()) + Runtime.getRuntime().freeMemory();
                long threadCount = ManagementFactory.getThreadMXBean().getThreadCount();
                String src = InetAddress.getLocalHost().getHostName().toUpperCase() + "/RSLOG";
                
                StringBuilder urlBuilder = new StringBuilder("http://");
                urlBuilder.append( rsLogConfig.getEsHost().trim())
                    .append(":")
                    .append(rsLogConfig.getEsPort().trim())
                    .append(esEndpoint)
                    .append(UUID.randomUUID().toString().replace("-", ""));
                String jvmIndexDoc = String.format(jvmIndexDocTemplate, src, new Date().getTime(), threadCount, freeMem);
                if (LOG.isDebugEnabled()) {
                    LOG.debug(String.format("RSLog JVM Metric Payload: %s", jvmIndexDoc));
                }
                byte[] postData = jvmIndexDoc.toString().getBytes();
                
                url = new URL(urlBuilder.toString());
                conn = (HttpURLConnection) url.openConnection();
                conn.setReadTimeout(15000);
                conn.setConnectTimeout(15000);
                conn.setRequestMethod("PUT");
                conn.setDoOutput(true);
                
                conn.setRequestProperty( "Accept", "application/json");
                conn.setRequestProperty( "Content-Length", Integer.toString( postData.length ));
                conn.getOutputStream().write(postData);
                conn.getOutputStream().flush();
                //conn.getOutputStream().close();

                int rc = conn.getResponseCode();
                if (rc != 201) {
                    LOG.error(String.format("A response code of %d is returned while indexing RSLog JVM metric in ES.", rc));
                    BufferedReader in = new BufferedReader(new InputStreamReader(conn.getErrorStream()));
                    String line;
                    while ((line = in.readLine()) != null) {
                        LOG.error(line);
                    }
                    
                }
            } else {
                Log.log.error("For RSLog JVM metric indexing, elasticsearch port and host are required.");
            }
        } catch (Exception e) {
            LOG.error("Error while indexing RSLog JVM Metrics", e);
        } finally {
            if (conn != null) {
                conn.disconnect();
            }
        }
    }
}
