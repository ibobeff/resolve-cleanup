package com.resolve.rslog.dao;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import com.resolve.rslog.model.Properties;
import com.resolve.rslog.model.PropertiesRowMapper;

@Transactional
@Repository
public class PropertiesDAO
{
    @Autowired
    private JdbcTemplate jdbcTemplate;
    
    public Properties getPropertyByName (String name) {
        String sql = "SELECT u_value FROM properties where u_name = ?";
        RowMapper<Properties> rowMapper = new PropertiesRowMapper();
        return this.jdbcTemplate.queryForObject(sql, rowMapper, name);
    }
}
