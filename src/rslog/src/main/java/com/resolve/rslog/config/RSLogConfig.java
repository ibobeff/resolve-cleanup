package com.resolve.rslog.config;

import com.resolve.util.ConfigMap;
import com.resolve.util.XDoc;

public class RSLogConfig extends ConfigMap {
    
    private static final long serialVersionUID = -4372130140921654818L;
    
    private String dbtype = "mysql";
    private String dbname = "resolve";
    private String host = "localhost";
    private String username = "resolve";
    private String p_assword = "resolve";
    private String url;
    private String location;
    private String esPort = "9200";
    private String esHost = "localhost";

    public String getDbtype() {
        return dbtype;
    }

    public void setDbtype(String dbtype) {
        this.dbtype = dbtype;
    }

    public String getDbname() {
        return dbname;
    }

    public void setDbname(String dbname) {
        this.dbname = dbname;
    }

    public String getHost() {
        return host;
    }

    public void setHost(String host) {
        this.host = host;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getP_assword() {
        return p_assword;
    }

    public void setP_assword(String p_assword) {
        this.p_assword = p_assword;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public String getLocation() {
        return location;
    }

    public String getEsPort() {
        return esPort;
    }

    public void setEsPort(String esPort) {
        this.esPort = esPort;
    }

    public String getEsHost() {
        return esHost;
    }

    public void setEsHost(String esHost) {
        this.esHost = esHost;
    }

    public void setLocation(String location) {
        this.location = location;
    }

    public RSLogConfig(XDoc config) throws Exception {
        super(config);
        
        define("dbtype", STRING, "./SQL/@DBTYPE");
        define("dbname", STRING, "./SQL/@DBNAME");
        define("host", STRING, "./SQL/@HOST");
        define("username", STRING, "./SQL/@USERNAME");
        define("p_assword", SECURE, "./SQL/@PASSWORD");
        define("url", STRING, "./SQL/@URL");
        define("location", STRING, "./LOG/@LOCATION");
        define("esPort", STRING, "./ES/@PORT");
        define("esHost", STRING, "./ES/@HOST");
    }
    
    @Override
    public void load() throws Exception {
        loadAttributes();
    }

    @Override
    public void save() throws Exception {
        saveAttributes();
    }
    
}
