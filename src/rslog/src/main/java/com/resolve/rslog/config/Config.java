package com.resolve.rslog.config;

import java.io.File;

import javax.sql.DataSource;

import org.apache.commons.lang.StringUtils;
import org.springframework.boot.jdbc.DataSourceBuilder;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import com.resolve.util.CryptUtils;
import com.resolve.util.XDoc;

@Configuration
public class Config {
    
    private String configFilename = "config/config.xml";
    private XDoc configDoc;
    private static RSLogConfig rsLogConfig;
    
    public Config() throws Exception {
        
        init("rslog");
    }
    
    @SuppressWarnings("rawtypes")
    @Bean
    public DataSource getDataSource() {
        rsLogConfig.getHost();
        DataSourceBuilder dataSourceBuilder = DataSourceBuilder.create();
        populateDateSource(dataSourceBuilder, rsLogConfig);
        dataSourceBuilder.username(rsLogConfig.getUsername());
        dataSourceBuilder.password(rsLogConfig.getP_assword());
        return dataSourceBuilder.build();
    }

    public void init(String serviceName) throws Exception {
        
        initConfig(serviceName);
        CryptUtils.setENCData(CryptUtils.getAESKey());
        loadConfig();
        saveConfig();
    }

    protected void exit(String[] argv) {
        
    }
    
    public void initConfig(String serviceName) throws Exception {
        File configFile = new File(serviceName+"/"+configFilename);
        if (configFile.exists())
            configDoc = new XDoc(configFile);
    }
    
    public void loadConfig() throws Exception {
        rsLogConfig = new RSLogConfig(configDoc);
        rsLogConfig.load();
    }
    
    public void saveConfig() throws Exception {
        rsLogConfig.save();
    }

    public static RSLogConfig getRsLogConfig() {
        return rsLogConfig;
    }
    
    @SuppressWarnings("rawtypes")
    private void populateDateSource(DataSourceBuilder dataSourceBuilder, RSLogConfig rsLogConfig) {
        String dbType = rsLogConfig.getDbtype();
        String dbname = rsLogConfig.getDbname();
        if (StringUtils.isNotBlank(dbType)) {
            String url = rsLogConfig.getUrl();
            if (dbType.equalsIgnoreCase("mysql")) {
                if (StringUtils.isBlank(url)) {
                    url = "jdbc:mysql://"+rsLogConfig.getHost()+"/"+dbname;
                }
                dataSourceBuilder.driverClassName("org.mariadb.jdbc.Driver");
            } else if (dbType.toLowerCase().contains("oracle")) {
                if (StringUtils.isBlank(url)) {
                    String host = rsLogConfig.getHost();
                    if (host.indexOf(':') < 0) {
                        host += ":1521";
                    }
                    url = "jdbc:oracle:thin:@"+host+":"+dbname;
                }
                dataSourceBuilder.driverClassName("oracle.jdbc.OracleDriver");
            }
            dataSourceBuilder.url(url);
        }
    }
}
