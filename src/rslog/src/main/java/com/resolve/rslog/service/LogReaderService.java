package com.resolve.rslog.service;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.OutputStream;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletResponse;

import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.compress.archivers.ArchiveOutputStream;
import org.apache.commons.compress.archivers.ArchiveStreamFactory;
import org.apache.commons.compress.archivers.zip.ZipArchiveEntry;
import org.apache.commons.compress.utils.IOUtils;
import org.apache.commons.io.FileUtils;
import org.apache.commons.io.FilenameUtils;
import org.apache.commons.io.IOCase;
import org.apache.commons.io.filefilter.FileFilterUtils;
import org.apache.commons.io.filefilter.IOFileFilter;
import org.apache.commons.io.filefilter.NotFileFilter;
import org.apache.commons.io.filefilter.OrFileFilter;
import org.apache.commons.io.filefilter.RegexFileFilter;
import org.apache.commons.io.filefilter.TrueFileFilter;
import org.apache.commons.io.filefilter.WildcardFileFilter;
import org.apache.commons.lang3.StringUtils;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.stereotype.Service;

import com.resolve.rslog.config.Config;

@Service
public class LogReaderService
{
    private static final Logger LOG = LogManager.getLogger(LogReaderService.class.getName());
    private static String ZIP_FILE_TEMPATE = "rslog/log/log-SESSIONID.zip";
    private String location = Config.getRsLogConfig().getLocation();
    
    /**
     * API to collect the log file(s), zip it up and send it to the caller.
     *  
     * @param compGUID : String repsenting GUID of the component.
     * @param level : String representing log leve. It could either be TRACE or INFO or DEBUG or ERROR or WARN
     * @param component : String represeting a component name. It could be actiontask or wiki or in case of RSRemote log, it's the gateway name like SNOW or NETCOOL.
     * @param from : Long representing start time (in milliseconds) of the requested log.
     * @param to : Long representing the end time (in milliseconds) upto wihch the log is needed.
     * @throws Exception 
     */
    public void readLog(String compGUID, List<String> components, Long from, Long to, String sessionId, HttpServletResponse response) throws Exception {
        if (StringUtils.isBlank(compGUID))
            compGUID = "";
        
        Collection<File> filesToBeZipped = collectLogFiles(compGUID, components, from, to, true);
        
        String zipFileName = ZIP_FILE_TEMPATE.replace("SESSIONID", sessionId);
        if (CollectionUtils.isNotEmpty(filesToBeZipped)) {
            zipIt3(filesToBeZipped, zipFileName);
        }
        writeFileToResponse(zipFileName, response);
    }
    
    private Collection<File> collectFilesToBeZipped(List<File> fileCollection, Long from, Long to, boolean needRemoteLogs) {
        List<File> filesToBeZipped = new ArrayList<>();
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
        
        for (File file : fileCollection) {
            String name = FilenameUtils.removeExtension(file.getName());
            String dateString = name.split("_")[1];
            Date date = null;
            // TODO check if the date format is correct first.
            try {
                date = sdf.parse(dateString);
            } catch (ParseException e) {
                LOG.error("Error while parting date on file: " + file.getName(), e);
            }
           
            if (date != null) {
               long fileDate = date.getTime();
               /*
                * The files are sorted in ascending order by date.
                */
               if (to < fileDate) {
                   break;
               }
               // make sure the file date falls between 'from' and 'to'
               if (fileDate >= from && fileDate <= to) {
                   filesToBeZipped.add(file);
               }
            }
        }
        
        return filesToBeZipped;
    }
    
    private void zipIt3 (Collection<File> filesToZip, String zipFileName) throws Exception {
        if (LOG.isDebugEnabled()) {
            LOG.debug(String.format("Files collected to be zipped: %s",filesToZip));
        }
        
        File logFile = new File(zipFileName);
        if (logFile.exists()) {
            logFile.delete();
        }
        
        if (CollectionUtils.isEmpty(filesToZip)) {
            return;
        }
        
        OutputStream zip_output = new FileOutputStream(logFile);
        /* Create Archive Output Stream that attaches File Output Stream / and specifies type of compression */
        ArchiveOutputStream logical_zip = new ArchiveStreamFactory().createArchiveOutputStream(ArchiveStreamFactory.ZIP, zip_output);
        
        filesToZip.stream().forEach(file -> {
            try {
                String canonicalPath = file.getCanonicalPath();
                String relativePath = canonicalPath.substring(canonicalPath.indexOf(location) + location.length() + 1, canonicalPath.length());
                logical_zip.putArchiveEntry(new ZipArchiveEntry(relativePath));
                /* Copy input file */
                IOUtils.copy(new FileInputStream(file), logical_zip);
                /* Close Archieve entry, write trailer information */
                logical_zip.closeArchiveEntry();
            } catch (Exception e) {
                LOG.error("Error while zipping log files.", e);
            }
        });
        
        /* Finish addition of entries to the file */
        logical_zip.finish();
        /* Close output stream, our files are zipped */
        zip_output.close();
    }
    
    /*
     * Collect all files found on a system filtered with GUID and an optional component.
     */
    private Collection<File> collectLogFiles(String GUID, List<String> components, Long from, Long to, boolean needRemoteLogs) {
        final List<String> nameFilterList = new ArrayList<>();
        components.stream().forEach(component -> {
            if (component.equalsIgnoreCase("all")) {
                nameFilterList.add(String.format("%s*", GUID.toUpperCase()));
            } else {
                if (component.equalsIgnoreCase("database")) {
                    nameFilterList.add(String.format("%s*db*", GUID.toUpperCase()));
                    nameFilterList.add(String.format("%s*database*", GUID.toUpperCase()));
                } else {
                    nameFilterList.add(String.format("%s*%s*", GUID.toUpperCase(), component));
                }
            }
        });
        
        if (CollectionUtils.isEmpty(nameFilterList)) {
            nameFilterList.add(String.format("%s*", GUID.toUpperCase()));
        }
        
        WildcardFileFilter wff = new WildcardFileFilter(nameFilterList, IOCase.INSENSITIVE);
        File logFolder = new File (location);
        IOFileFilter  filter = null;
        if (needRemoteLogs) {
            OrFileFilter off = new OrFileFilter(Arrays.asList(new RegexFileFilter(String.format("^%s_[0-9-]{10}.log", GUID))));
            filter = FileFilterUtils.or(wff, off);
        } else { // no remote logs files are needed, so filter them out.
            NotFileFilter nff = new NotFileFilter(new RegexFileFilter("^[A-Z0-9]{32}_[0-9-]{10}.log"));
            filter = FileFilterUtils.and(wff, nff);
        }
        Collection<File> logFileCollection = FileUtils.listFiles(logFolder, filter, TrueFileFilter.INSTANCE);
        if (from == null && to == null) {
            // if from and to, both are null, collect all the log files.
            return logFileCollection;
        } else {
            // Sort the files in ascending order by name.
            List<File> sodtedList = new ArrayList<File>(logFileCollection);
            sodtedList.sort(Comparator.comparing(File::getName));
            // if 'from' (start) is null, start from the start of the time.
            if (from == null)
                from = 0L;
            // if 'to' (the end) is null, consider current time.
            if (to == null)
                to = new Date().getTime();
            logFileCollection = collectFilesToBeZipped(sodtedList, from, to, needRemoteLogs);
        }
        return logFileCollection;
    }
    
    public Map<String, Object> listLogFiles(List<String> guids, List<String> components, Long from, Long to, boolean needRemoteLogs) {
        Map<String, Object> compFileListMap = new HashMap<>();
        if (CollectionUtils.isNotEmpty(guids)) {
            guids.stream().forEach(guid -> {
                if (StringUtils.isNotBlank(guid)) {
                    Collection<File> fileCollection = collectLogFiles(guid, components, from, to, needRemoteLogs);
                    List<String> fileNameList = new ArrayList<>();
                    if (CollectionUtils.isNotEmpty(fileCollection)) {
                        // sort the files with it's name in descending order so that latest file will be at the top.
                        Collections.sort((List<File>)fileCollection, (File o1, File o2) -> o2.compareTo(o1));
                        fileCollection.stream().forEach(file -> {
                            fileNameList.add(file.getName());
                        });
                    }
                    compFileListMap.put(guid, fileNameList);
                }
            });
        }
        return compFileListMap;
    }
    
    public void downloadLogFiles(List<String> fileNames, String sessionId, HttpServletResponse response) throws Exception {
        WildcardFileFilter wff = new WildcardFileFilter(fileNames, IOCase.INSENSITIVE);
        File logFolder = new File (location);
        Collection<File> logFileCollection = FileUtils.listFiles(logFolder, wff, TrueFileFilter.INSTANCE);
        
        String zipFileName = ZIP_FILE_TEMPATE.replace("SESSIONID", sessionId);
        if (CollectionUtils.isNotEmpty(logFileCollection)) {
            zipIt3(logFileCollection, zipFileName);
        }
        writeFileToResponse(zipFileName, response);
    }
    
    private void writeFileToResponse(String zipFileName, HttpServletResponse response) throws Exception {
        File logFile = new File(zipFileName);
        if (logFile.exists()) {
            byte[] fileContent = new byte[1024*8];
            FileInputStream fis = new FileInputStream(logFile);
            while (true) {
                int len = fis.read(fileContent);
                if (len <= 0) {
                    break;
                }
                response.getOutputStream().write(fileContent, 0, len);
            }
            fis.close();
            // delete the log file, once it's been sent.
            logFile.delete();
        } else {
            response.sendError(404, "No log file matched the search criteria.");
        }
    }
}
