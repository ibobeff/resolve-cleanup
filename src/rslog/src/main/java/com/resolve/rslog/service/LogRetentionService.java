package com.resolve.rslog.service;

import java.io.File;
import java.util.Collection;

import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.io.FileUtils;
import org.apache.commons.io.filefilter.AgeFileFilter;
import org.apache.commons.io.filefilter.TrueFileFilter;
import org.apache.commons.lang3.StringUtils;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.EnableScheduling;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

import com.resolve.rslog.config.Config;
import com.resolve.rslog.config.RSLogConfig;
import com.resolve.rslog.dao.PropertiesDAO;
import com.resolve.rslog.model.Properties;

@EnableScheduling
@Component
public class LogRetentionService {
    private static final Logger LOG = LogManager.getLogger(LogRetentionService.class.getName());
    private String RETENTION_PROPERTY = "rslog.log.retention.days";
    RSLogConfig rsLogConfig = Config.getRsLogConfig();
    
    @Autowired
    private PropertiesDAO propertiesDAO;
    
    @Scheduled (cron = "0 0 0 * * *") // Run at 12:00 AM every day.
    public void reportCurrentTime() {
        int days = getRetentionDays();
        
        if (LOG.isDebugEnabled()) {
            LOG.debug(String.format("Retention days: %d", days));
        }
        
        long cutoff = System.currentTimeMillis() - (days * 24 * 60 * 60 * 1000);
        String logLocation = rsLogConfig.getLocation();
        
        File logFolder = new File (logLocation);
        Collection<File> files = FileUtils.listFiles(logFolder, new AgeFileFilter(cutoff, true), TrueFileFilter.INSTANCE);
        
        if (CollectionUtils.isNotEmpty(files)) {
            files.stream().forEach(file -> {
                file.delete();
            });
        }
    }
    
    private int getRetentionDays() {
        int retentionDays = 10;
        Properties property = propertiesDAO.getPropertyByName(RETENTION_PROPERTY);
        if (property != null) {
            String days = property.getUValue();
            if (StringUtils.isNotBlank(days)) {
                try {
                    retentionDays = Integer.parseInt(days);
                } catch (Exception e) {
                    LOG.error(String.format("Error while parsing renention days, '%s', received from system property", days), e);
                }
            }
        }
        return retentionDays;
    }
}
