/******************************************************************************
* (C) Copyright 2014
*
* Resolve Systems, LLC
*
* All rights reserved
* This software is distributed under license and may not be used, copied,
* modified, or distributed without the express written permission of
* Resolve Systems, LLC.
*
******************************************************************************/

package com.resolve.rsconsole;

import java.util.Hashtable;

public class CMDSet extends CMDBase
{
    public CMDSet()
    {
        super("SET", "Set variable properties value");
        
        usage = "set <VARIABLE> <VALUE>";
        description = "Set variable property value";
        this.setHelpParams("VARIABLE", "Variable name");
        this.setHelpParams("VALUE", "Variable value. Use double quotes if value includes spaces.");
    } // CMDSet
    
    //public Hashtable execute(String[] params) throws CMDException
    public Hashtable execute(String[] params) throws Exception
    {
        Hashtable result = null;
        
        if (params.length == 3)
        {
            this.setVariable(params[1], params[2]);
        }
        return result;
    } // execute

} // CMDSet
