/******************************************************************************
* (C) Copyright 2014
*
* Resolve Systems, LLC
*
* All rights reserved
* This software is distributed under license and may not be used, copied,
* modified, or distributed without the express written permission of
* Resolve Systems, LLC.
*
******************************************************************************/

package com.resolve.rsconsole;

import java.util.Hashtable;

public class CMDPwd extends CMDBase
{
    public CMDPwd()
    {
        super("PWD", "Displays the current script execution path");
        
        description = "Displays the current script execution path";
    } // CMDPwd
    
    public Hashtable execute(String[] params) throws CMDException
    {
        Hashtable result = null;
        
        Main.println(getVariable(Main.VAR_CONS_LOCATION));
        Main.println();
        
        return result;
    } // execute

} // CMDPwd
