/******************************************************************************
* (C) Copyright 2014
*
* Resolve Systems, LLC
*
* All rights reserved
* This software is distributed under license and may not be used, copied,
* modified, or distributed without the express written permission of
* Resolve Systems, LLC.
*
******************************************************************************/

package com.resolve.rsconsole;


import java.util.List;
import java.util.Map;

import com.resolve.util.ConfigMap;
import com.resolve.util.CryptUtils;
import com.resolve.util.Log;
import com.resolve.util.XDoc;

public class ConfigUser extends ConfigMap
{
    private static final long serialVersionUID = 3887229115389817727L;
	List<Map> users;
    
    public ConfigUser(XDoc config) throws Exception
    {
        super(config);
    } // ConfigReceiveNetcool
    
    public void load() throws Exception
    {
        // users
        users = xdoc.getListMapValue("./USERS/USER");
        
        // check if users are defined
        if (users == null || users.size() == 0)
        {
            Main.println("Missing user accounts in the config.xml file");
            Main.println();
            System.exit(1);
        }
        
        // decrypt passwords
        decryptPasswords();
        
    } // load

    public void save() throws Exception
    {
        // encrypt passwords
        encryptPasswords();
        
        // users
        xdoc.setListMapValue("./USERS/USER", users);
        
    } // save
    
    void decryptPasswords()
    {
        for (Map user : users)
        {
            String username = (String)user.get("USERNAME");
            String password = (String)user.get("PASSWORD");
            try
            {
                password = CryptUtils.decrypt(password);
	            user.put("PASSWORD", password);
            }
            catch (Exception e)
            {
                Log.log.warn("Unable to decrypt password for user: "+username+". "+e.getMessage());
            }
        }
    } // decryptPasswords
    
    void encryptPasswords()
    {
        for (Map user : users)
        {
            String username = (String)user.get("USERNAME");
            String password = (String)user.get("PASSWORD");
            try
            {
                password = CryptUtils.encrypt(password);
	            user.put("PASSWORD", password);
            }
            catch (Exception e)
            {
                Log.log.warn("Unable to encrypt password for user: "+username+". "+e.getMessage(), e);
            }
        }
    } // encryptPasswords
    
    public boolean authenticate(String authUser, String authPass)
    {
        boolean result = false;
        
        for (Map user : users)
        {
            String username = (String)user.get("USERNAME");
            String password = (String)user.get("PASSWORD");
            
            if (username.equalsIgnoreCase(authUser) && password.equals(authPass))
            {
                result = true;
            }
        }
        return result;
    } // authenticate
    
} // ConfigUser
