/******************************************************************************
* (C) Copyright 2014
*
* Resolve Systems, LLC
*
* All rights reserved
* This software is distributed under license and may not be used, copied,
* modified, or distributed without the express written permission of
* Resolve Systems, LLC.
*
******************************************************************************/

package com.resolve.rsconsole;

import java.util.Hashtable;

public class CMDUnset extends CMDBase
{
    public CMDUnset()
    {
        super("UNSET", "Remove variable");
        
        usage = "unset <VARIABLE>";
        description = "Remove variable property";
        this.setHelpParams("VARIABLE", "Variable name");
    } // CMDQuit
    
    public Hashtable execute(String[] params) throws CMDException
    {
        Hashtable result = null;
        
        if (params.length == 2)
        {
            this.removeVariable(params[1]);
        }
        
        return result;
    } // execute

} // CMDUnset
