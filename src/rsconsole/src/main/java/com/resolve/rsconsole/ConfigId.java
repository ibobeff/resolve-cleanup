/******************************************************************************
* (C) Copyright 2014
*
* Resolve Systems, LLC
*
* All rights reserved
* This software is distributed under license and may not be used, copied,
* modified, or distributed without the express written permission of
* Resolve Systems, LLC.
*
******************************************************************************/

package com.resolve.rsconsole;

import java.net.InetAddress;

import com.resolve.util.ConfigMap;
import com.resolve.util.Guid;
import com.resolve.util.Log;
import com.resolve.util.StringUtils;
import com.resolve.util.XDoc;

public class ConfigId extends ConfigMap
{
    private static final long serialVersionUID = 2217537990294007930L;
	String name = InetAddress.getLocalHost().getHostName();
    String description = Main.main.release.type;
    String guid = null;
    
    public ConfigId(XDoc config) throws Exception
    {
        super(config);
        
        define("name", STRING, "./ID/@NAME");
        define("description", STRING, "./ID/@DESCRIPTION");
        define("guid", STRING, "./ID/@GUID");
    } // ConfigId
    
    public void load() throws Exception
    {
        loadAttributes();
        
        if (StringUtils.isEmpty(guid))
        {
            guid = new Guid().toString();
            Log.log.info("Generating new GUID: "+guid);
        }
                
    } // load

    public void save()
    {
        saveAttributes();
    } // save

    public String getName()
    {
        return name;
    }

    public void setName(String name)
    {
        this.name = name;
    }

    public String getDescription()
    {
        return description;
    }

    public void setDescription(String description)
    {
        this.description = description;
    }

    public String getGuid()
    {
        return guid;
    }

    public void setGuid(String guid)
    {
        this.guid = guid;
    }
    
} // ConfigId
