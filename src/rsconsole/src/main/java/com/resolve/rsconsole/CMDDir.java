/******************************************************************************
* (C) Copyright 2014
*
* Resolve Systems, LLC
*
* All rights reserved
* This software is distributed under license and may not be used, copied,
* modified, or distributed without the express written permission of
* Resolve Systems, LLC.
*
******************************************************************************/

package com.resolve.rsconsole;

import java.io.File;
import java.util.Hashtable;

import com.resolve.util.FileUtils;

public class CMDDir extends CMDBase
{
    public CMDDir()
    {
        super("DIR", "Displays the available commands for the directory");
        
        description = "Displays the available commands for the directory.";
    } // CMDirC
    
    public Hashtable execute(String[] params) throws Exception
    {
        String location = getVariable(Main.VAR_CONS_LOCATION);
        Hashtable result = runDir(params, location);
        return result;
    } // execute
    
    static Hashtable runDir(String[] params, String location)
    {
        Hashtable result = null;
        
        File dir = FileUtils.getFile(Main.main.getGSEHome()+"/"+location);
        if (dir.exists())
        {
            File[] filenames = dir.listFiles();
            for (int i=0; i < filenames.length; i++)
            {
                String filename = filenames[i].getName();
                if (filenames[i].isDirectory())
                {
                    Main.println(filename+"/");
                }
                else if (filename.endsWith(".groovy") || (filename.endsWith(".cons")))
                {
                    // skip hidden files starting with "_"
                    if (!filename.startsWith("_"))
                    {
                        Main.println(filename);
                    }
                }
            }
        }
        else
        {
            Main.println("Invalid location: "+location);
        }
        Main.println();
            
        return result;
    }

} // CMDDir
