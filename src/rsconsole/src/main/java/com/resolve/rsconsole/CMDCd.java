/******************************************************************************
* (C) Copyright 2014
*
* Resolve Systems, LLC
*
* All rights reserved
* This software is distributed under license and may not be used, copied,
* modified, or distributed without the express written permission of
* Resolve Systems, LLC.
*
******************************************************************************/

package com.resolve.rsconsole;

import java.util.Hashtable;

import com.resolve.rsbase.MainBase;
import com.resolve.util.StringUtils;

public class CMDCd extends CMDBase
{
    public CMDCd()
    {
        super("CD", "Change script execution directory");
        
        usage = "CD <location>";
        description = "Change script execution directory to the location specified. Location can be an aboslute or relative path.";
        this.setHelpParams("LOCATION", "Directory name (e.g. /, .., /etc ");
    } // CMDCd
    
    public Hashtable execute(String[] params) throws Exception
    {
        Hashtable result = null;
        Main main = (Main)MainBase.main;
        
        if (params.length == 2)
        {
            String dir = params[1];
            
            // absolute
            if (dir.startsWith("/"))
            {
                if (main.setLocation(dir) == false)
                {
                    Main.println("Invalid directory: "+dir);
                }
            }
            
            // relative
            else
            {
                String[] newPaths = dir.split("/");
                
                if (newPaths.length > 0)
                {
                    // change directory
                    main.setLocation(newPaths[0]);
                    
                    // recurse if more paths
                    if (newPaths.length > 1)
                    {
                        String[] newParams = new String[2];
                        newParams[0] = "cd";
                        newParams[1] = StringUtils.join(newPaths, "/", 1, newPaths.length -1);
                        result = this.execute(newParams);
                    }
                }
            }
        }
        
        return result;
    } // execute

} // CMDCd
