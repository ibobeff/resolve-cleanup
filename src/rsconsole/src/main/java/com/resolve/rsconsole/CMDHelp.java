/******************************************************************************
* (C) Copyright 2014
*
* Resolve Systems, LLC
*
* All rights reserved
* This software is distributed under license and may not be used, copied,
* modified, or distributed without the express written permission of
* Resolve Systems, LLC.
*
******************************************************************************/

package com.resolve.rsconsole;

import java.util.Iterator;
import java.util.Hashtable;
import java.util.Map;

import com.resolve.rsbase.MainBase;

public class CMDHelp extends CMDBase
{
    public CMDHelp()
    {
        super("HELP", "Display help message");
        
        usage = "help <command name>";
        description = "For more information on specific commands, type help <command name>.";
    } // CMDQuit
    
    public Hashtable execute(String[] params) throws CMDException
    {
        Hashtable result = null;
        
        Main main = (Main)MainBase.main;
        for (Iterator i=main.commands.values().iterator(); i.hasNext(); )
        {
            CMDBase command = (CMDBase)i.next();
            setHelpParams(command.getName(), command.getSummary());
        }
        Main.println(getHelp());
        
        return result;
    } // execute

} // CMDHelp
