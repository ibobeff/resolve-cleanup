/******************************************************************************
* (C) Copyright 2014
*
* Resolve Systems, LLC
*
* All rights reserved
* This software is distributed under license and may not be used, copied,
* modified, or distributed without the express written permission of
* Resolve Systems, LLC.
*
******************************************************************************/

package com.resolve.rsconsole;

import com.resolve.util.XDoc;

public class ConfigGeneral extends com.resolve.rsbase.ConfigGeneral
{
    private static final long serialVersionUID = 8492654080709253750L;
	int screenWidth = 80;
    public int scheduledPool;


    
    public ConfigGeneral(XDoc config) throws Exception
    {
        super(config);
        
        define("screenWidth", INTEGER, "./GENERAL/@SCREENWIDTH");
        define("scheduledPool", INTEGER, "./GENERAL/@SCHEDULEDPOOL");
    } // ConfigGeneral
    
    public void load()
    {
        super.load();
    } // load

    public void save()
    {
        super.save();
    } // save

    public int getScreenWidth()
    {
        return screenWidth;
    }

    public void setScreenWidth(int screenWidth)
    {
        this.screenWidth = screenWidth;
    }
    
    public int getScheduledPool() {
        return scheduledPool;
    }

    public void setScheduledPool(int scheduledPool) {
        this.scheduledPool = scheduledPool;
    }

} // ConfigGeneral