/******************************************************************************
 * (C) Copyright 2014
 *
 * Resolve Systems, LLC
 *
 * All rights reserved
 * This software is distributed under license and may not be used, copied,
 * modified, or distributed without the express written permission of
 * Resolve Systems, LLC.
 *
 ******************************************************************************/

package com.resolve.rsconsole;

import java.io.BufferedInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.util.Properties;

import org.apache.log4j.Logger;
import org.apache.log4j.PropertyConfigurator;
import org.owasp.esapi.reference.Log4JLogger;

import com.resolve.util.FileUtils;

public class History
{
    public static Logger log;

    static String username = "rsconsole";

    public static void init(String configFile) throws Exception
    {
        try
        {
            File file = FileUtils.getFile(configFile);
            if (file.exists() == false)
            {
                throw new Exception("Unable to find Log configuration file: " + file.getAbsolutePath());
            }

            // RBA-13770 : log4j:WARN No such property [enabled] in org.apache.log4j.net.SyslogAppender
            Properties currentLog4JProperties = new Properties();
            BufferedInputStream bis = new BufferedInputStream(new FileInputStream(configFile));
            if (bis != null)
            {
                currentLog4JProperties.load(bis);
            }
            Properties elementsToRemove = new Properties();
            String ntEventLogEnabled = currentLog4JProperties.containsKey("log4j.appender.NTEventLog.enabled") ? currentLog4JProperties.getProperty("log4j.appender.NTEventLog.enabled") : "false";
            String syslogEnabled = currentLog4JProperties.containsKey("log4j.appender.SYSLOG.enabled") ? currentLog4JProperties.getProperty("log4j.appender.SYSLOG.enabled") : "false";
            elementsToRemove.put("log4j.appender.NTEventLog.enabled", ntEventLogEnabled);
            elementsToRemove.put("log4j.appender.SYSLOG.enabled", syslogEnabled);
            for (Object keyToRemove : elementsToRemove.keySet())
            {
                if (currentLog4JProperties.containsKey(keyToRemove))
                {
                    elementsToRemove.put(keyToRemove, currentLog4JProperties.get(keyToRemove));
                    currentLog4JProperties.remove(keyToRemove);
                }
            }

            // configure logger
            // PropertyConfigurator.configure(configFile);
            PropertyConfigurator.configure(currentLog4JProperties);

            // init logger
            log = Log4JLogger.getLogger("com.resolve.history");
            // log = Logger.getLogger("com.resolve.history");
        }
        catch (Exception e)
        {
            Main.println(e.getMessage());
        }
    } // init

    public static void setUsername(String username)
    {
        History.username = username;
    } // setUsername

    public static void log(String msg)
    {
        log.info("[" + username + "] - " + msg);
    } // log

    public static void log(String msg, String username)
    {
        log.info("[" + username + "] - " + msg);
    } // log

} // History
