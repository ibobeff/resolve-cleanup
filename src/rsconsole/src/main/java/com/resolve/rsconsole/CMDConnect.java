/******************************************************************************
* (C) Copyright 2014
*
* Resolve Systems, LLC
*
* All rights reserved
* This software is distributed under license and may not be used, copied,
* modified, or distributed without the express written permission of
* Resolve Systems, LLC.
*
******************************************************************************/

package com.resolve.rsconsole;

import java.util.Iterator;
import java.util.Map;

import com.resolve.rsbase.MainBase;

import java.util.Hashtable;

public class CMDConnect extends CMDBase
{
    public CMDConnect()
    {
        super("CONNECT", "Manage connection information for console sessions");
        
        usage =  "CONNECT <session name>\n";
        usage += "        add <session name> <guid>\n";
        usage += "        add <session name>\n";
        usage += "        print <session name>\n";
        usage += "        remove <session name>\n";
        usage += "        list\n";
        description = "Manage connection information for console session. Add defines a new session information. Print displays an existing session information. Remove deletes an existing session. List displays a list of known sessions. CONNECT <session>[:<target>] sets the current session to <session> and target proxy group to <target>. ";
        
        this.setHelpParams("SESSION", "Session name assigned to the connection information");
        this.setHelpParams("GUID", "Destination address GUID");
        this.setHelpParams("NAME", "Destination address NAME");
    } // CMDConnect
    
    public Hashtable execute(String[] params) throws Exception
    {
        Hashtable result = null;
        Main main = (Main)MainBase.main;
        
        // print usage
        if (params.length == 1)
        {
            this.help();
        }
        
        // list or connect
        else if (params.length == 2)
        {
            String session = params[1].toUpperCase();
            
            // list
            if (session.equalsIgnoreCase("LIST"))
            {
                for (Iterator i=main.sessions.entrySet().iterator(); i.hasNext(); )
                {
                    Map.Entry entry = (Map.Entry)i.next();
                    String name = (String)entry.getKey();
                    String guidName = (String)entry.getValue();
                    Main.println("session: "+name+" address: "+guidName);
                }
            }
            
            // connect
            else
            {
                // init ESB
                ((Main)Main.main).initESB();
                
                // get target from session
                String target = "";
                String[] sessionTarget = session.split(":");
                session = sessionTarget[0];
                if (sessionTarget.length == 2)
                {
                    target = sessionTarget[1];
                }
                
                // set target and connect
                if (!main.setSession(session))
                {
                    Main.println("Adding new session: "+session);
                    session = session.toUpperCase();
	                main.sessions.put(session, session);
	                
	                String currentSession = main.variables.get(Main.VAR_CONS_SESSION);
	                if (!currentSession.equalsIgnoreCase(session))
	                {
	                    // estabish connection by getting version
	                    main.setSession(session);
	                }
                }
                // auto-clear target
                this.setVariable(Main.VAR_CONS_TARGET, target);
                    
                // estabish connection by getting version
                main.setMethod("MInfo.version", "MDefault.ignore");
                result = new Hashtable();
                Main.println("Connect session: "+session);
            }
        }
        
        // print or remove
        else if (params.length == 3)
        {
            String command = params[1];
            String session = params[2].toUpperCase();
            if (command.equalsIgnoreCase("PRINT"))
            {
                String guidName = (String)main.sessions.get(session);
                Main.println("session: "+session+" address: "+guidName);
            }
            else if (command.equalsIgnoreCase("REMOVE"))
            {
                main.sessions.remove(session);
                Main.println("Removed session: "+session);
            }
            if (command.equalsIgnoreCase("ADD"))
            {
                main.sessions.put(session, session);
                Main.println("Added session: "+session);
                
//                String currentSession = main.variables.get(Main.VAR_CONS_SESSION);
//                if (!currentSession.equalsIgnoreCase(session))
//                {
//                    // estabish connection by getting version
//                    main.setSession(session);
//                }
            }
            else
            {
                this.help();
            }
        }
        
        // add
        else if (params.length == 4)
        {
            String command = params[1];
            if (command.equalsIgnoreCase("ADD"))
            {
                String session = params[2].toUpperCase();
                String address = params[3];
                main.sessions.put(session, address);
                Main.println("Added session: "+session);
                
//                String currentSession = main.variables.get(Main.VAR_CONS_SESSION);
//                if (!currentSession.equalsIgnoreCase(session))
//                {
//                    // estabish connection by getting version
//                    main.setSession(session);
//                }
            }
            else
            {
                this.help();
            }
        }
        else
        {
            this.help();
        }
        System.out.println();
        
        return result;
    } // execute

} // CMDConnect
