/******************************************************************************
* (C) Copyright 2014
*
* Resolve Systems, LLC
*
* All rights reserved
* This software is distributed under license and may not be used, copied,
* modified, or distributed without the express written permission of
* Resolve Systems, LLC.
*
******************************************************************************/

package com.resolve.rsconsole;

import java.util.Hashtable;

import com.resolve.rsbase.MainBase;
import com.resolve.util.Constants;
import com.resolve.util.Log;

public class CMDDBDisconnect extends CMDBase
{

    public CMDDBDisconnect()
    {
        super("DBDISCONNECT", "Disconnect from Database");
        
        description = "Disconnect from Database";
    } // CMDDBDisconnect
    
    public Hashtable execute(String[] params) throws CMDException
    {
        Hashtable result = null;
        
        Main main = (Main)MainBase.main;
        if (main.dbConnected)
        {
            try {
                Main.println("Closing Database Connection");
                main.dbConnection.close();
            }
            catch (Exception e)
            {
                Main.println("Failed to Close Existing DB Connection");
                Log.log.warn("Failed to Close Existing DB Connection", e);
            }
            main.dbConnected = false;
            main.dbConnection = null;
            main.binding.setVariable(Constants.RSCONSOLE_BINDING_DB, null);
        }
        else
        {
            Main.println("Not currently connected to a database");
        }
        return result;
    } // execute
    
} // CMDDBDisconnect