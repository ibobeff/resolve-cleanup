/******************************************************************************
* (C) Copyright 2014
*
* Resolve Systems, LLC
*
* All rights reserved
* This software is distributed under license and may not be used, copied,
* modified, or distributed without the express written permission of
* Resolve Systems, LLC.
*
******************************************************************************/

package com.resolve.rsconsole;

import java.io.File;
import java.util.Hashtable;

import com.resolve.rsbase.MainBase;
import com.resolve.util.Log;

import ml.options.Options;
import ml.options.Options.Multiplicity;
import ml.options.Options.Prefix;
import ml.options.Options.Separator;

public class CMDJMSConnect extends CMDBase
{
    public CMDJMSConnect()
    {
        super("JMSCONNECT", "Connect to a Specified JMS Server");
        
        usage =  "JMSCONNECT [-u <username>] [-p <password>] [-ba <broker address2>] [-bp <broker port2>] <broker address> [<broker port>]\n";
        usage += "Options:\n";
        usage += "    -u username           Username to use when connecting to JMS Broker\n";
        usage += "    -p password           Password to use when connecting to JMS Broker\n";
        usage += "    -ba <broker address2> Backup JMS Address\n";
        usage += "    -bp <broker port2>    Backup JMS Port\n";
        description = "Setup Connect Configuration to a Specified JMS Server.  Connection will occur with CONNECT command";
        
        this.setHelpParams("BROKER ADDRESS", "Address of JMS Broker to Connect To");
        this.setHelpParams("BROKER PORT", "Port to use when connecting to JMS Broker - Default is 4004");        this.setHelpParams("BROKER ADDRESS2", "Address of Backup JMS Broker to Connect To");
        this.setHelpParams("BROKER PORT2", "Port to use when connecting to Backup JMS Broker - Default is 4004");        this.setHelpParams("USERNAME", "Username to use when connecting to JMS Broker - Default is ESB USERNAME in config.xml");
        this.setHelpParams("PASSWORD", "Password to use when connecting to JMS Broker - Default is ESB PASSWORD in config.xml");
    } // CMDJMSConnect

    public Hashtable execute(String[] params) throws Exception
    {
        Hashtable result = null;
        
        String brokerPort = "4004";
        String username = null;
        String password = null;
        String brokerAddr = null;
        String brokerPort2 = "4004";
        String brokerAddr2 = null;
        
        String[] inputs = new String[params.length - 1];
        for (int i=1; i<params.length; i++)
        {
            inputs[i-1] = params[i];
        }
        
        Options opt = new Options(inputs, Prefix.DASH, 1, 2);
        opt.getSet().addOption("p", Separator.BLANK, Multiplicity.ZERO_OR_ONE);
        opt.getSet().addOption("u", Separator.BLANK, Multiplicity.ZERO_OR_ONE);
        opt.getSet().addOption("ba", Separator.BLANK, Multiplicity.ZERO_OR_ONE);
        opt.getSet().addOption("bp", Separator.BLANK, Multiplicity.ZERO_OR_ONE);
        if (!opt.check())
        {
            this.help();
        }
        else
        {
            Main main = (Main) MainBase.main;
	        if (opt.getSet().isSet("u"))
	        {
	            username = opt.getSet().getOption("u").getResultValue(0);
	        }
	        if (opt.getSet().isSet("p"))
	        {
	            password = opt.getSet().getOption("p").getResultValue(0);
	        }
	        if (opt.getSet().isSet("ba"))
	        {
	            brokerAddr2 = opt.getSet().getOption("ba").getResultValue(0);
	            
		        if (opt.getSet().isSet("bp"))
		        {
		            brokerPort2 = opt.getSet().getOption("bp").getResultValue(0);
	                if (!brokerPort2.matches("\\d+"))
	                {
	                    Main.println("Invalid Backup Port, Using Default (4004)");
		                brokerPort2 = "4004";
	                }
		        }
	        }
	        
	        if (username != null && password == null)
	        {
	            password = main.configESB.config.getP_assword();
	        }
	        if (password != null && username == null)
	        {
	            username = main.configESB.config.getUsername();
	        }
	        
	        for (String input : opt.getSet().getData())
	        {
	            if (brokerAddr == null)
	            {
	                brokerAddr = input;
	            }
	            else
	            {
	                if (input.matches("\\d+"))
	                {
		                brokerPort = input;
	                }
	                else
	                {
	                    Main.println("Invalid Port, Using Default (4004)");
	                }
	            }
	        }
	        
	        Log.log.debug("BrokerAddr: " + brokerAddr);
	        Log.log.debug("Port: " + brokerPort);
	        Log.log.debug("Username: " + username);
	        Log.log.debug("Password: " + password);
	        Log.log.debug("BrokerAddr2: " + brokerAddr2);
	        Log.log.debug("Port2: " + brokerPort2);
	        
	        if (brokerAddr2 != null && brokerPort2 != null)
	        {
	            main.initRemoteESB(brokerAddr, brokerPort, username, password, brokerAddr2, brokerPort2);
	        }
	        else
	        {
		        main.initRemoteESB(brokerAddr, brokerPort, username, password);
	        }
        }
        return result;
    } // execute

}
