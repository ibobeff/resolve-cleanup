/******************************************************************************
* (C) Copyright 2014
*
* Resolve Systems, LLC
*
* All rights reserved
* This software is distributed under license and may not be used, copied,
* modified, or distributed without the express written permission of
* Resolve Systems, LLC.
*
******************************************************************************/

package com.resolve.rsconsole;

import com.resolve.rsconsole.Main;
import com.resolve.util.ConfigMap;
import com.resolve.util.XDoc;

public class ConfigGSE extends ConfigMap
{
    private static final long serialVersionUID = 3333560402949124054L;
	String path = Main.main.configGeneral.home+"/rsconsole/service";
    
    public ConfigGSE(XDoc config) throws Exception
    {
        super(config);
        
        define("path", STRING, "./GSE/@PATH");
    } // ConfigGSE
    
    public void load() throws Exception
    {
        loadAttributes();
    } // load

    public void save()
    {
        saveAttributes();
    } // save

    public String getPath()
    {
        return path;
    }

    public void setPath(String path)
    {
        this.path = path;
    }
    
} // ConfigGSE
