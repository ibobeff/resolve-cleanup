/******************************************************************************
* (C) Copyright 2014
*
* Resolve Systems, LLC
*
* All rights reserved
* This software is distributed under license and may not be used, copied,
* modified, or distributed without the express written permission of
* Resolve Systems, LLC.
*
******************************************************************************/

package com.resolve.rsconsole;

import java.sql.DriverManager;
import java.sql.SQLException;
import java.util.Hashtable;

import com.resolve.rsbase.MainBase;
import com.resolve.rsconsole.CMDBase;
import com.resolve.util.Constants;
import com.resolve.util.Log;


public class CMDDBConnect extends CMDBase
{
    public CMDDBConnect()
    {
       super("DBConnect", "Connect the RSConsole to a Database");
       
       usage =  "DBCONNECT <connection name>\n";
       usage += "       DBCONNECT <database type> <username> [<password>] <connection url> \n";
       
       description = "Connect the RSConsole to a Database";
       
       this.setHelpParams("CONNECTION NAME", "Read from connections.cfg inside of RSControl's config folder");
       this.setHelpParams("DATABASE TYPE", "Type of Database to Connect to, only Oracle and MySQL are supported");
       this.setHelpParams("CONNECTION URL", "Connection URL of the Database to connect to");
       this.setHelpParams("USERNAME", "Username to use when connecting to a Database");
       this.setHelpParams("PASSWORD", "Password to use when connecting to a Database");
    } // CMDDBConnect

    public Hashtable execute(String[] params) throws Exception
    {
        Hashtable result = null;
        Main main = (Main) MainBase.main;
        if (main.dbConnected)
        {
            try {
                Main.println("Closing Existing Connection");
                main.dbConnection.close();
            }
            catch (Exception e)
            {
                Log.log.warn("Failed to Close Existing DB Connection", e);
            }
            main.dbConnected = false;
            main.dbConnection = null;
        }
        if (params.length == 2)
        {
            String connectionName = params[1];
            if (main.dbConnectionProperties != null)
            {
	            String dbType = main.dbConnectionProperties.get(connectionName + ".type");
	            String url = main.dbConnectionProperties.get(connectionName + ".url");
	            String username = main.dbConnectionProperties.get(connectionName + ".username");
	            String password = main.dbConnectionProperties.get(connectionName + ".password");
	            if (dbType == null || url == null || username == null || password == null)
	            {
	                Main.println("Properties for " + connectionName + " have not been set up");
	            }
	            else
	            {
	                connect(dbType, username, password, url);
	            }
            }
            else
            {
                Main.println("No Connection Properties Have been Set Up");
            }
        }
        else if (params.length == 5)
        {
            connect(params[1], params[2], params[3], params[4]);
        }
        else if (params.length == 4)
        {
            connect(params[1], params[2], "", params[3]);
        }
        else
        {
            help();
        }
        return result;
    } // execute
    
    private void connect(String dbType, String username, String password, String url) throws Exception
    {
        Main main = (Main) MainBase.main;
        
        String driverClass = null;
        if (dbType.equalsIgnoreCase("oracle"))
        {
            driverClass = Constants.ORACLEDRIVER;
        }
        else if (dbType.equalsIgnoreCase("mysql"))
        {
            driverClass = Constants.MYSQLDRIVER;
        }
        else if (dbType.equalsIgnoreCase("db2"))
        {
            driverClass = Constants.DB2DRIVER;
        }
        if (driverClass != null)
        {
            try
            {
                Class.forName(driverClass);
                main.dbConnection = DriverManager.getConnection(url, username, password);
                main.dbConnected = true;
                main.binding.setVariable(Constants.RSCONSOLE_BINDING_DB, main.dbConnection);
                Main.println("DB Connection Successful");
            }
            catch (SQLException sqle)
            {
                Log.log.error("Failed to Connect to " + dbType + " DB at " + url, sqle);
                String message = sqle.getMessage();
                if (message.contains("No suitable driver"))
                {
                    Main.println("Connection details are incorrectly formatted");
                }
                else if (message.contains("Access denied for user") || message.contains("invalid username/password"))
                {
                    Main.println("Username or Password is Incorrect");
                }
                else
                {
                    Main.println("Unable to connect to Database");
                }
            }
        }
        else
        {
            Main.println("DB Type " + dbType + " Not Supported");
        }
    } // connect
} //CMDDBConnect
