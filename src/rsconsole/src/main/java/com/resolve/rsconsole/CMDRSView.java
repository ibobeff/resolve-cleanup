/******************************************************************************
* (C) Copyright 2014
*
* Resolve Systems, LLC
*
* All rights reserved
* This software is distributed under license and may not be used, copied,
* modified, or distributed without the express written permission of
* Resolve Systems, LLC.
*
******************************************************************************/

package com.resolve.rsconsole;

import java.io.File;
import java.io.FileInputStream;
import java.util.Hashtable;

import com.resolve.rsbase.MainBase;
import com.resolve.util.Log;

public class CMDRSView extends CMDBase
{
    int readSize = 4096;
    
    public CMDRSView()
    {
        super("RS-View", "Display Contents of RSConsole Script");
        
        usage = "RS-View <script name>";
        description = "Outputs Content of Specified Script";
        
        this.setHelpParams("SCRIPT NAME", "Name of the Script to Display");
    } // CMDRSView
    
    public Hashtable execute(String[] params)
    {
        Hashtable result = null;
        
        Main main = (Main) MainBase.main;
        
        if (params.length <= 1)
        {
            Main.println("Must Specify A Script To Output");
        }
        else
        {
	        try
	        {
		        File scriptFile = main.findScriptFile(params[1]);
		        if (scriptFile.isFile())
		        {
		            Log.log.debug("Read Script: " + scriptFile);
		            FileInputStream fis = new FileInputStream(scriptFile);
		            
					long fileSize = scriptFile.length();
					byte[] fileContent;
	                Main.println(scriptFile.getName() + " Content:");
	                
	                while (fileSize > 0)
	                {
	                    if (fileSize > readSize)
	                    {
				            fileContent = new byte[readSize];
	                    }
	                    else
	                    {
	                        fileContent = new byte[(int) fileSize];
	                    }
	                    int read = fis.read(fileContent);
	                    if (read > fileSize)
	                    {
	                        read = (int) fileSize;
	                    }
	                    Main.print(new String(fileContent));
	                    fileSize -= read;
	                }
	                Main.println("\n");
	                
	                if (fis != null)
	                {
	                    fis.close();
	                }
		        }
		        else
		        {
		            if (scriptFile.exists())
		            {
			            Main.println("Specified file " + scriptFile + " is not a directory");
		            }
		            else
		            {
			            Main.println("Specified file " + scriptFile + " does not exist");
		            }
		        }
	        }
	        catch (Exception e)
	        {
	            Log.log.warn("Unexpected Exception Reading File: " + e.getMessage(), e);
	            Main.println("Unexpected Exception Reading File: " + e.getMessage());
	        }
        }
        
        return result;
    } // execute

} // CMDRSView
