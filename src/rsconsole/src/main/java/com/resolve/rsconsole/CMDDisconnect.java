/******************************************************************************
* (C) Copyright 2014
*
* Resolve Systems, LLC
*
* All rights reserved
* This software is distributed under license and may not be used, copied,
* modified, or distributed without the express written permission of
* Resolve Systems, LLC.
*
******************************************************************************/

package com.resolve.rsconsole;

import java.util.Hashtable;

import com.resolve.rsbase.MainBase;
import com.resolve.util.Log;

public class CMDDisconnect extends CMDBase
{

    public CMDDisconnect()
    {
        super("DISCONNECT", "Disconnect from Message Server");
        
        description = "Disconnect from Message Server";
    } // CMDDisconnect
    
    public Hashtable execute(String[] params) throws CMDException
    {
        Hashtable result = null;
        
        Main main = (Main)MainBase.main;
        if (main.connected)
        {
	        try
	        {
	            main.closeESB();
	        }
	        catch (Exception e)
	        {
	            Log.log.warn("Unable to Close Connection to Network: " + e.getMessage(), e);
	        }
        }
        else
        {
            Main.println("Not currently connected to a network");
        }
        return result;
    } // execute
    
} // CMDDisconnect