/******************************************************************************
* (C) Copyright 2014
*
* Resolve Systems, LLC
*
* All rights reserved
* This software is distributed under license and may not be used, copied,
* modified, or distributed without the express written permission of
* Resolve Systems, LLC.
*
******************************************************************************/

package com.resolve.rsconsole;

import java.util.Iterator;
import java.util.Map;
import java.util.Hashtable;
import java.util.Vector;
import java.util.List;

import com.resolve.rsbase.MainBase;
import com.resolve.util.ConfigMap;
import com.resolve.util.XDoc;

public class ConfigSession extends ConfigMap
{
    private static final long serialVersionUID = 4872675311024259666L;
	List sessions;
    
    public ConfigSession(XDoc config) throws Exception
    {
        super(config);
    } // ConfigId
    
    public void load() throws Exception
    {
        sessions = xdoc.getListMapValue("./CONNECTION/SESSION", null);
        if (sessions == null)
        {
            sessions = new Vector();
        }
        
        Main main = (Main)MainBase.main;
        if (main.sessions == null)
        {
            main.sessions = new Hashtable();
        }
        for (Iterator i=sessions.iterator(); i.hasNext(); )
        {
            Hashtable def = (Hashtable)i.next();
            String name = (String)def.get("NAME");
            String address = (String)def.get("ADDRESS");
            main.sessions.put(name.toUpperCase(), address);
        }
    } // load

    public void save()
    {
        sessions.clear();
        
        Main main = (Main)MainBase.main;
        for (Iterator i=main.sessions.entrySet().iterator(); i.hasNext(); )
        {
            Map.Entry entry = (Map.Entry)i.next();
            String name = (String)entry.getKey();
            String address = (String)entry.getValue();
            Hashtable def = new Hashtable();
            def.put("NAME", name);
            def.put("ADDRESS", address);
            sessions.add(def);
        }
        
        xdoc.setListMapValue("./CONNECTION/SESSION", sessions);
    } // save
    
    public String toString()
    {
        String result = "";
        
        result += "ConfigSession:\n";
        for (Iterator i=sessions.iterator(); i.hasNext(); )
        {
            Hashtable def = (Hashtable)i.next();
            result += "  name: "+def.get("NAME")+" address: "+def.get("ADDRESS")+"\n";
        }
        result += "\n";
        return result;
    } // toString
    
} // ConfigSession
