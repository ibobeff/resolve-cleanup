/******************************************************************************
* (C) Copyright 2014
*
* Resolve Systems, LLC
*
* All rights reserved
* This software is distributed under license and may not be used, copied,
* modified, or distributed without the express written permission of
* Resolve Systems, LLC.
*
******************************************************************************/

package com.resolve.rsconsole;

import java.util.Iterator;
import java.util.Map;
import java.util.List;
import java.util.Hashtable;
import java.util.TreeMap;

import com.resolve.esb.MMsgHeader;
import com.resolve.esb.MUtils;

public class MDefault extends com.resolve.rsbase.MDefault
{
    public Hashtable unknown(MMsgHeader header, String params) throws Exception
    {
        Main.println("\n\n["+header.getSourceString()+"]");
        Main.println(params);
        
        return null;
    } // unknown
    
    public Hashtable unknown(MMsgHeader header, Map params) throws Exception
    {
        Main.println("\n\n["+header.getSourceString()+"]");
        Main.println(MUtils.dumpMessage(params));
        
        return null;
    } // unknown
    
    public Hashtable unknown(MMsgHeader header, Object params) throws Exception
    {
        Main.println("\n\n["+header.getSourceString()+"]");
        Main.println(params.toString());
        
        return null;
    } // unknown
    
    public Hashtable print(MMsgHeader header, String params) throws Exception
    {
        Main.println("\n\n["+header.getSourceString()+"]");
        Main.println(params);
        return null;
    } // print
    
    public Hashtable print(MMsgHeader header, Map params) throws Exception
    {
        Main.println("\n\n["+header.getSourceString()+"]");
        
        for (Iterator i=params.entrySet().iterator(); i.hasNext(); )
        {
            Map.Entry entry = (Map.Entry)i.next();
            Main.println(entry.getKey() +": "+entry.getValue());
        }
        
        return null;
    } // print
    
    public Hashtable print(MMsgHeader header, Object params) throws Exception
    {
        Main.println("\n\n["+header.getSourceString()+"]");
        Main.println(params.toString());
        return null;
    } // print
    
    public Hashtable printList(MMsgHeader header, Object params) throws Exception
    {
        Main.println("\n\n["+header.getSourceString()+"]");
        
        List list = (List)params;
        for (Iterator i=list.iterator(); i.hasNext(); )
        {
            Main.println(i.next().toString());
        }
        
        return null;
    } // printList
    
    public Hashtable printMap(MMsgHeader header, Object params) throws Exception
    {
        Main.println("\n\n["+header.getSourceString()+"]");
        
        Map map = (Map)params;
        for (Iterator i=map.entrySet().iterator(); i.hasNext(); )
        {
            Map.Entry entry = (Map.Entry)i.next();
            Main.println(entry.getKey() +": "+entry.getValue());
        }
        
        return null;
    } // printMap
    
    public Hashtable printMessage(MMsgHeader header, Map params) throws Exception
    {
        Main.println("\n\n["+header.getSourceString()+"]");
        Main.println((String)params.get("MESSAGE"));
        return null;
    } // printMessage
    
    public Hashtable printDetail(MMsgHeader header, Map params) throws Exception
    {
        Main.println("\n\n["+header.getSourceString()+"]");
        
        boolean isDetail = params.containsKey("DETAIL");
        params.remove("DETAIL");
        
        TreeMap map = new TreeMap(params);
        for (Iterator i=map.entrySet().iterator(); i.hasNext(); )
        {
            Map.Entry entry = (Map.Entry)i.next();
            if (isDetail)
            {
                System.out.println(entry.getValue());
            }
            else
            {
                System.out.println(entry.getKey());
            }
        }
        return null;
    } // printDetail
    
    public Hashtable printString(String params) throws Exception
    {
        Main.println();
        Main.println(params);
        return null;
    } // printString
    
    public Hashtable ignore(Map params) throws Exception
    {
        return null;
    } // ignore
    
} // MDefault
