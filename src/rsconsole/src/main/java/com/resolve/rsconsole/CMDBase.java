/******************************************************************************
* (C) Copyright 2014
*
* Resolve Systems, LLC
*
* All rights reserved
* This software is distributed under license and may not be used, copied,
* modified, or distributed without the express written permission of
* Resolve Systems, LLC.
*
******************************************************************************/

package com.resolve.rsconsole;

import java.util.Iterator;
import java.util.Map;
import java.util.TreeMap;

import com.resolve.rsbase.MainBase;
import com.resolve.util.StringUtils;

public abstract class CMDBase implements CMDInterface
{
    protected String name;              // command name
    protected String summary;           // command list summary description
    protected String usage;             // command usage syntax
    protected String description;       // command detailed description
    protected TreeMap helpParams;       // param name => description
    
    public CMDBase(String name, String summary)
    {
        this.name = name;
        this.summary = summary;
        this.usage = name;
        this.description = "";
        this.helpParams = new TreeMap();
    } // CMDBase
    
    public String getName()
    {
        return name;
    } // getName

    public String getSummary()
    {
        return summary;
    } // getSummary
    
    public String getHelp()
    {
        String result = "";
        
        ConfigGeneral configGeneral = (ConfigGeneral)MainBase.main.configGeneral;
        int screenWidth = configGeneral.screenWidth;
        
        // print header
        result += "\n";
        result += "Usage: "+usage+"\n";
        result += "\n";
        result += justify(description, 0, screenWidth, false) + "\n";
        result += "\n";
        
        // get param name column width
        int maxNameLen = 12;
        for (Iterator i=helpParams.entrySet().iterator(); i.hasNext(); )
        {
            Map.Entry entry = (Map.Entry)i.next();
            String name = (String)entry.getKey();
            if (name.length() >= maxNameLen)
            {
                maxNameLen = name.length() + 1;
            }
        }
        
        // print param name
        for (Iterator i=helpParams.entrySet().iterator(); i.hasNext(); )
        {
            Map.Entry entry = (Map.Entry)i.next();
            String name = (String)entry.getKey();
            String value = (String)entry.getValue();
                
            int indentLen = maxNameLen + 2;
            result += "  " + StringUtils.rightPad(name, maxNameLen) + justify(value, indentLen, screenWidth, false);
        }
        
        return result;
    } // getHelp
    
    public void help()
    {
        Main.println(getHelp());
    } // help
    
    protected void setHelpParams(String name, String description)
    {
        if (!name.toUpperCase().startsWith("RS-"))
        {
	        helpParams.put(name.toUpperCase(), description);
        }
    } // setHelpParams
    
    protected String getVariable(String name)
    {
        Main main = (Main)MainBase.main;
        return main.variables.get(name.toUpperCase());
    } // getVariable
    
    protected void setVariable(String name, String value)
    {
        Main main = (Main)MainBase.main;
        main.variables.set(name.toUpperCase(), value);
    } // setVariable
    
    protected void removeVariable(String name)
    {
        Main main = (Main)MainBase.main;
        main.variables.remove(name.toUpperCase());
    } // removeVariable

    protected String justify(String text, int indentLen, int screenwidth, boolean indentFirstLine)
    {
        String result = "";
        String indent = "";
        
        if (indentLen >= screenwidth)
        {
            return null;
        }
        
        // create indentation string
        for (int i=0; i < indentLen; i++)
        {
            indent += " ";
        }
        
        boolean isFirstLine = true;
        
        // no need to justify text
        if (indentLen + text.length() < screenwidth)
        {
            if (indentFirstLine)
            {
    	        result += indent+text+"\n";
            }
            else
            {
    	        result += text+"\n";
            }
            isFirstLine = false;
        }
        
        // need to justify text
        else
        {
            String line;
            if (indentFirstLine)
            {
                line = indent;
            }
            else
            {
                line = "";
            }
            
            int width = screenwidth;
            if (isFirstLine)
            {
                width = width - indentLen;
            }
            
            String[] tokens = text.split(" ");
            for (int i = 0; i < tokens.length; i++)
            {
                // append line to result
                if (tokens[i].length() + line.length() >= width - 1)
                {
                    result += line+"\n";
                    line = indent + tokens[i]+" ";
                    
                    width = screenwidth;
                    isFirstLine = false;
                }
                else
                {
	                line = line + tokens[i]+ " ";
                }
            }
            result += line+"\n";
        }
        
        return result;
    } // justify
    
} // CMDBase
