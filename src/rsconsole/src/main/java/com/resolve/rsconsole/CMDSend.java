/******************************************************************************
* (C) Copyright 2014
*
* Resolve Systems, LLC
*
* All rights reserved
* This software is distributed under license and may not be used, copied,
* modified, or distributed without the express written permission of
* Resolve Systems, LLC.
*
******************************************************************************/

package com.resolve.rsconsole;

import java.util.Hashtable;

public class CMDSend extends CMDBase
{
    public CMDSend()
    {
        super("SEND", "Send raw message");
        
        usage = "send <PARAMETERS>";
        description = "Send a message using the header and variable properties defined";
        this.setHelpParams("<PARAMETERS>", "List of name-values (e.g. name1=\"value1\" name2=\"value2\")");
    } // CMDSet
    
    public Hashtable execute(String[] params) throws CMDException
    {
        Hashtable result = new Hashtable();
        
        for (int i=1; i < params.length; i++)
        {
            String[] nameval = params[i].split("=");
            if (nameval.length == 1)
            {
                result.put(nameval[0].toUpperCase(), "");
            }
            else if (nameval.length == 2)
            {
                result.put(nameval[0].toUpperCase(), nameval[1]);
            }
        }
        
        return result;
    } // execute

} // CMDSet
