/******************************************************************************
* (C) Copyright 2014
*
* Resolve Systems, LLC
*
* All rights reserved
* This software is distributed under license and may not be used, copied,
* modified, or distributed without the express written permission of
* Resolve Systems, LLC.
*
******************************************************************************/

package com.resolve.rsconsole;

import java.util.Hashtable;

import com.resolve.rsbase.MainBase;

public class CMDExit extends CMDBase
{

    public CMDExit()
    {
        super("EXIT", "Exit the RSConsole");
        
        description = "Exit the RSConsole";
    } // CMDQuit
    
    public Hashtable execute(String[] params) throws CMDException
    {
        Hashtable result = null;
        
        Main main = (Main)MainBase.main;
        main.terminate = true;
        
        return result;
    } // execute
    
} // CMDExit
