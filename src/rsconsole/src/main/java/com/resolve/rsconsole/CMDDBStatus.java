/******************************************************************************
* (C) Copyright 2014
*
* Resolve Systems, LLC
*
* All rights reserved
* This software is distributed under license and may not be used, copied,
* modified, or distributed without the express written permission of
* Resolve Systems, LLC.
*
******************************************************************************/

package com.resolve.rsconsole;

import java.sql.Connection;
import java.sql.DatabaseMetaData;
import java.sql.ResultSet;
import java.sql.Statement;
import java.text.SimpleDateFormat;
import java.util.Hashtable;

import com.resolve.rsbase.MainBase;
import com.resolve.util.Log;

public class CMDDBStatus extends CMDBase
{
    public CMDDBStatus()
    {
        super("DBStatus", "Retrieve Status of DB Connection");
        
        usage =  "DBSTATUS <connection name>\n";
        
        description = "Retrieve Status of DB Connection";
    } // CMDDBStatus

    public Hashtable execute(String[] params) throws Exception
    {
        Hashtable result = null;
        Main main = (Main) MainBase.main;        
        if (!main.dbConnected)
        {
            Main.println("Not Currently Connected to a Database");
        }
        else
        {
            Connection conn = main.dbConnection;
            if (conn.isClosed())
            {
                Main.println("Connection to Database has been Closed");
                main.dbConnected = false;
            }
            else
            {
                try {
                    Statement statement = conn.createStatement();
                    ResultSet rs = statement.executeQuery("SELECT CURRENT_TIMESTAMP FROM DUAL");
                    DatabaseMetaData data = conn.getMetaData();
                    Main.println("Database Product: " + data.getDatabaseProductName());
                    Main.println("Database Version: " + data.getDatabaseProductVersion());
                    Main.println("Connection URL: " + data.getURL());
                    Main.println("Connected as: " + data.getUserName());
                    Main.print("Database Current Timestamp: ");
                    if (rs.next())
                    {
                        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-dd-MM hh:mm:ss");
                        Main.println(sdf.format(rs.getTimestamp(1)));
                    }
                    else
                    {
                        Main.println("Failed to Obtain Time from Database");
                    }
                }
                catch (Exception e)
                {
                    Log.log.error("Failed to obtain status of database: " + e.getMessage(), e);
                    Main.println("Database Connection is No Longer Valid, Closing Connection");
                    try
                    {
                        conn.close();
                    }
                    catch (Exception e1)
                    {
                        Log.log.error("Failed to Close Connection: " + e1.getMessage());
                    }
                    main.dbConnected = false;
                    main.dbConnection = null;
                }
            }
        }
        
        return result;
    } // execute
} // CMDDBStatus