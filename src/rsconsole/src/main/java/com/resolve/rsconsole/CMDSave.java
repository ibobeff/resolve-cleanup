/******************************************************************************
* (C) Copyright 2014
*
* Resolve Systems, LLC
*
* All rights reserved
* This software is distributed under license and may not be used, copied,
* modified, or distributed without the express written permission of
* Resolve Systems, LLC.
*
******************************************************************************/

package com.resolve.rsconsole;

import java.util.Hashtable;

import com.resolve.rsbase.MainBase;

public class CMDSave extends CMDBase
{
    public CMDSave()
    {
        super("SAVE", "Save configuration and variable property values");
        
        description = "Save configuration and variable property values";
    } // CMDSet
    
    public Hashtable execute(String[] params) throws Exception
    {
        Hashtable result = null;
        
        Main main = (Main)MainBase.main;
        main.save();
        
        return result;
    } // execute

} // CMDSet
