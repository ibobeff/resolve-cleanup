/******************************************************************************
* (C) Copyright 2014
*
* Resolve Systems, LLC
*
* All rights reserved
* This software is distributed under license and may not be used, copied,
* modified, or distributed without the express written permission of
* Resolve Systems, LLC.
*
******************************************************************************/

package com.resolve.rsconsole;

import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.Hashtable;

import com.resolve.util.CryptUtils;
import com.resolve.util.Log;

import org.apache.commons.codec.binary.Base64;

public class CMDRSEncrypt extends CMDBase
{
    private static String HASH_FUNCTION = "SHA-1";
    
    public CMDRSEncrypt()
    {
		super("RS-Encrypt", "Encrypt Specified String");
        
        usage = "RS-Encrypt [<Hash Function>] <String to Encrypt>";
        description = "Encrypts the Specified String and Displays the Result";
        
        this.setHelpParams("HASH FUNCTION", "Hash Function to Encrypt With");
        this.setHelpParams("UNENCTRYPTED STRING", "String to Encrypt with Hash Function");
    }

    @Override
    public Hashtable execute(String[] params) throws Exception
    {
        Hashtable result = null;
        
        if (params.length <= 1)
        {
            Main.println("Must Provide a String to Encrypt");
        }
        else 
        {
            try
            {
		        String encryptedString = "";
		        if (params.length == 2)
		        {
		            String plainText = params[1];
		            encryptedString = CryptUtils.encrypt(plainText);
		        }
		        else
		        {
			        HASH_FUNCTION = params[1];
		            String plainText = params[2];
		            encryptedString = encrypt(plainText);
		            encryptedString = HASH_FUNCTION.toUpperCase() + ":" + encryptedString;
		        }
	            Main.println(encryptedString);
            }
            catch (NoSuchAlgorithmException nsae)
            {
                Log.log.warn("No Such Algorithm: " + nsae.getMessage(), nsae);
                Main.println("No Such Encryption Algorithm: " + HASH_FUNCTION);
            }
            catch (Exception e)
            {
                Log.log.warn("Failed to Entrypt: " + e.getMessage(), e);
                Main.println(e.getMessage());
            }
        }
        
        return result;
    }
    
    private static String encrypt(String x) throws NoSuchAlgorithmException
    {
        String answer = x;
        if (x != null)
        {
            MessageDigest d = MessageDigest.getInstance(HASH_FUNCTION);
            d.reset();
            d.update(x.getBytes());
            byte[] bytes = d.digest();
            answer = new String(Base64.encodeBase64(bytes)); //converted to apache encoding
        }
        return answer;
    } // encrypt
}
