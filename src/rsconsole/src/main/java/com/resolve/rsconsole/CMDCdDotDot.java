/******************************************************************************
* (C) Copyright 2014
*
* Resolve Systems, LLC
*
* All rights reserved
* This software is distributed under license and may not be used, copied,
* modified, or distributed without the express written permission of
* Resolve Systems, LLC.
*
******************************************************************************/

package com.resolve.rsconsole;

import java.util.Hashtable;

import com.resolve.rsbase.MainBase;

public class CMDCdDotDot extends CMDBase
{
    public CMDCdDotDot()
    {
        super("CD..", "Change script execution directory");
        
        usage = "CD..";
        description = "Change script execution directory to the parent directory.";
    } // CMDCd
    
    public Hashtable execute(String[] params) throws Exception
    {
        Hashtable result = null;
        Main main = (Main)MainBase.main;
        
        // change directory
        main.setLocation("..");
                    
        return result;
    } // execute

} // CMDCd
