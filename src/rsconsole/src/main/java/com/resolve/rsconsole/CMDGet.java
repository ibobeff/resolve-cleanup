/******************************************************************************
* (C) Copyright 2014
*
* Resolve Systems, LLC
*
* All rights reserved
* This software is distributed under license and may not be used, copied,
* modified, or distributed without the express written permission of
* Resolve Systems, LLC.
*
******************************************************************************/

package com.resolve.rsconsole;

import java.util.Iterator;
import java.util.Map;

import com.resolve.rsbase.MainBase;

import java.util.Hashtable;

public class CMDGet extends CMDBase
{
    public CMDGet()
    {
        super("GET", "Get variable properties value");
        
        usage = "get <VARIABLE>";
        description = "Get variable property value";
        this.setHelpParams("VARIABLE", "Variable name");
    } // CMDGet
    
    public Hashtable execute(String[] params) throws CMDException
    {
        Hashtable result = null;
        
        Main main = (Main)MainBase.main;
        if (params.length == 1)
        {
            for (Iterator i=main.variables.entrySet().iterator(); i.hasNext(); )
            {
                Map.Entry entry = (Map.Entry)i.next();
                String name = (String)entry.getKey();
                String value = (String)entry.getValue();
                Main.println(name.toUpperCase()+": "+value);
            }
            System.out.println();
        }
        else if (params.length == 2)
        {
            String name = params[1];
            
            String value = this.getVariable(name);
            if (value == null)
            {
                Main.println(name.toUpperCase()+" is not defined");
            }
            else
            {
                Main.println(name.toUpperCase()+": "+value);
            }
            Main.println();
        }
        
        return result;
    } // execute

} // CMDGet
