/******************************************************************************
* (C) Copyright 2014
*
* Resolve Systems, LLC
*
* All rights reserved
* This software is distributed under license and may not be used, copied,
* modified, or distributed without the express written permission of
* Resolve Systems, LLC.
*
******************************************************************************/

package com.resolve.rsconsole;

import java.util.Hashtable;

public class CMDLs extends CMDBase
{
    public CMDLs()
    {
        super("LS", "Displays the available commands for the directory");
        
        description = "Displays the available commands for the directory.";
    } // CMDirC
    
    public Hashtable execute(String[] params) throws Exception
    {
        String location = getVariable(Main.VAR_CONS_LOCATION);
        Hashtable result = CMDDir.runDir(params, location);
        return result;
    } // execute

} // CMDLs