/******************************************************************************
* (C) Copyright 2014
*
* Resolve Systems, LLC
*
* All rights reserved
* This software is distributed under license and may not be used, copied,
* modified, or distributed without the express written permission of
* Resolve Systems, LLC.
*
******************************************************************************/

package com.resolve.rsconsole;

import java.util.Hashtable;

import com.resolve.rsbase.MainBase;

public class CMDMethod extends CMDBase
{
    public CMDMethod()
    {
        super("METHOD", "Set the destination method for the session");
        
        usage = "method <CLASSMETHOD>";
        description = "Set the destination method for the session.";
        this.setHelpParams("CLASSMETHOD", "Destination Class and method name");
    } // CMDMethod
    
    public Hashtable execute(String[] params) throws Exception
    {
        Hashtable result = null;
        
        if (params.length == 2)
        {
            Main main = (Main)MainBase.main;
            main.setMethod(params[1]);
        }
        
        return result;
    } // execute

} // CMDMethod
