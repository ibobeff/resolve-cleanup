/******************************************************************************
* (C) Copyright 2014
*
* Resolve Systems, LLC
*
* All rights reserved
* This software is distributed under license and may not be used, copied,
* modified, or distributed without the express written permission of
* Resolve Systems, LLC.
*
******************************************************************************/

package com.resolve.rsconsole;

import java.util.Hashtable;

import com.resolve.rsbase.MainBase;

public class CMDDest extends CMDBase
{
    public CMDDest()
    {
        super("DEST", "Set the destination address for the session");
        
        usage = "dest <GUID/NAME>";
        description = "Set the destination address for the session. NOTE: include the / between GUID and NAME.";
        this.setHelpParams("GUID", "Destination address GUID");
        this.setHelpParams("NAME", "Destination address name");
    } // CMDDest
    
    public Hashtable execute(String[] params) throws Exception
    {
        Hashtable result = null;
        
        if (params.length == 2)
        {
            Main main = (Main)MainBase.main;
            main.setSessionDest(params[1]);
        }
        
        return result;
    } // execute

} // CMDDest
