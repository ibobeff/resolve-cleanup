/******************************************************************************
* (C) Copyright 2014
*
* Resolve Systems, LLC
*
* All rights reserved
* This software is distributed under license and may not be used, copied,
* modified, or distributed without the express written permission of
* Resolve Systems, LLC.
*
******************************************************************************/

package com.resolve.rsconsole;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.sql.Connection;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import com.resolve.esb.MListener;
import com.resolve.esb.MMsgDest;
import com.resolve.esb.MMsgHeader;
import com.resolve.esb.MMsgRoute;
import com.resolve.rsbase.MainBase;
import com.resolve.groovy.script.GroovyScript;
import com.resolve.thread.TaskExecutor;
import com.resolve.thread.ScheduledExecutor;
import com.resolve.util.Constants;
import com.resolve.util.CopyFile;
import com.resolve.util.FileUtils;
import com.resolve.util.Log;
import com.resolve.util.PasswordField;
import com.resolve.util.Properties;
import com.resolve.util.StringUtils;
import com.resolve.util.Tokenizer;
import com.resolve.util.XDoc;

import groovy.lang.Binding;
import ml.options.Options;
import ml.options.Options.Multiplicity;
import ml.options.Options.Prefix;
import ml.options.Options.Separator;

public class Main extends MainBase
{
    public final static String VAR_MSG_TYPE = "HEADER_TYPE";
    
    public final static String VAR_MSG_ROUTE = "HEADER_ROUTE";
    public final static String VAR_MSG_CALLBACK = "HEADER_CALLBACK";

    public final static String VAR_CONS_LOCATION = "CONS_LOCATION";
    public final static String VAR_CONS_SESSION = "CONS_SESSION";
    public final static String VAR_CONS_TARGET = "CONS_TARGET";
    public final static String VAR_CONS_TARGETOVERRIDE = "CONS_TARGETOVERRIDE";
    public final static String VAR_CONS_USERNAME = "CONS_USERNAME";

    public final static String VAL_DEFAULT_MSGTYPE = "DEFAULT";
    public final static String VAL_DEFAULT_MSGROUTE = "UNKNOWN";
    public final static String VAL_DEFAULT_MSGCALLBACK = "MDefault.unknown";

    public ConfigSession configSession;
    public ConfigUser configUser;

    public Properties systems;
    public Properties variables;
    public Map commands;
    public Map sessions;

    public Connection dbConnection;
    public boolean dbConnected;
    public String dbConnectionsFile = "config/connections.cfg";
    public Properties dbConnectionProperties;

    public boolean terminate;
    public boolean connected;

    Binding binding;
    String username;
    String password;
    String scriptFilename;
    File scriptFile;
    String[] scriptArgs;
    int scriptExitWait = 5;

    public static void main(String[] args)
    {
        try
        {
            // start
            main = new Main(args);
            main.start();
        }
        catch (Throwable e)
        {
            e.printStackTrace();
            Log.log.error("Failed to start RSConsole: " + e.getMessage(), e);

            System.exit(0);
        }

    } // main

    public Main(String[] args) throws Exception
    {
        Main.main = this;

        // init release information
        initRelease(new Release("rsconsole"));

        // process command line options
        initOptions(args);

        // init logging
        initLog();

        // init enc
        initENC();

        // init signal handlers
        initShutdownHandler();

        // print console header
        printTitle();

        // init configuration
        initConfig();

        // prompt username / password
        authenticate();

        // init timezone
        initTimezone();

        // init thread pools
        initThreadPools();

        // start config
        startConfig();

        // save configuration
        initSaveConfig();

        // init db connection properties
        initDBConnections();

        // start
        initComplete();

    } // Main

    public void init(String serviceName) throws Exception
    {
    } // init

    void initOptions(String[] args)
    {
        Options opt = new Options(args, Prefix.DASH);
        opt.getSet().addOption("h");
        opt.getSet().addOption("-help");
        opt.getSet().addOption("u", Separator.BLANK, Multiplicity.ZERO_OR_ONE);
        opt.getSet().addOption("p", Separator.BLANK, Multiplicity.ZERO_OR_ONE);
        opt.getSet().addOption("f", Separator.BLANK, Multiplicity.ZERO_OR_ONE);
        opt.getSet().addOption("F", Separator.NONE, Multiplicity.ZERO_OR_MORE);
        opt.getSet().addOption("w", Separator.BLANK, Multiplicity.ZERO_OR_ONE);
        opt.check();

        Options optSlash = new Options(args, Prefix.SLASH);
        optSlash.getSet().addOption("\\?");
        optSlash.check();

        if (opt.getSet().isSet("h") || opt.getSet().isSet("-help") || optSlash.getSet().isSet("\\?"))
        {
            // Print usage
            printTitle();
            System.out.println("-u <username>        username to log into rsconsole");
            System.out.println("-p <password>        password to log into rsconsole");
            System.out.println("-f <filename>        script file in service(script.cons or script.groovy)");
            System.out.println("-FD<arguments>;...   arguments to provide to script file specified by -f)");
            System.out.println("-w <secs>            wait for specified secs before exiting script");
            System.out.println();
            System.exit(1);
        }
        if (opt.getSet().isSet("u"))
        {
            username = opt.getSet().getOption("u").getResultValue(0);
        }
        if (opt.getSet().isSet("p"))
        {
            password = opt.getSet().getOption("p").getResultValue(0);
        }
        if (opt.getSet().isSet("f"))
        {
            String filename = opt.getSet().getOption("f").getResultValue(0);

            // prefix with rsconsole/service
            if (filename != null && filename.charAt(0) != '/')
            {
                filename = "rsconsole/service/" + filename;
            }

            // check if file exists
            File file = FileUtils.getFile(filename);
            if (file == null || !file.exists())
            {
                // try .cons extension
                file = FileUtils.getFile(filename + ".cons");
                if (!file.exists())
                {
                    // try .groovy extension
                    file = FileUtils.getFile(filename + ".groovy");
                }
            }

            scriptFilename = filename;
            scriptFile = file;

            // add script arguments
            scriptArgs = null;
        }
        if (opt.getSet().isSet("F"))
        {
            if (scriptFile != null)
            {
                String strArgs = opt.getSet().getOption("F").getResultValue(0);
                strArgs = scriptFile.getName() + ";" + strArgs;
                scriptArgs = StringUtils.stringToList(strArgs, ";").toArray(new String[0]);
            }
        }
        if (opt.getSet().isSet("w"))
        {
            scriptExitWait = Integer.parseInt(opt.getSet().getOption("w").getResultValue(0));
        }
    } // initOptions

    public void start()
    {
        try
        {
            terminate = false;

            // init sessions
            initSessions();

            // init variables
            initVariables();

            // init system config
            initSystemConfig();

            // init commands
            initCommands();

            // init binding
            initBinding();

            // execute script
            if (scriptFile != null)
            {
                if (scriptFile.exists())
                {
                    executeScript(scriptFile, scriptArgs);
                    try
                    {
                        Thread.sleep(scriptExitWait * 1000);
                    }
                    catch (Exception e)
                    {
                    }
                }
                else
                {
                    System.out.println("Unable to find script: " + scriptFilename + ", " + scriptFilename + ".cons" + ", or " + scriptFilename + ".groovy");
                }
            }

            // interactive
            else
            {
                BufferedReader stdin = new BufferedReader(new InputStreamReader(System.in));
                while (!terminate)
                {
                    try
                    {
                        // print prompt
                        printPrompt();

                        // execute command
                        executeCommand(stdin.readLine());
                    }
                    catch (Exception e)
                    {
                        Main.println("ERROR: " + e.getMessage());
                        Log.log.error(e.getMessage(), e);
                    }
                }
            }

            // terminate rsconsole
            System.out.println();
            System.exit(0);
        }
        catch (Throwable t)
        {
            Main.println("Failed to start RSConsole: " + t.getMessage());
            Log.log.error("Failed to start RSConsole: " + t.getMessage(), t);
            System.exit(1);
        }
    } // start

    void initBinding()
    {
        binding = new Binding();
        binding.setVariable(Constants.GROOVY_BINDING_MAIN, this);
        binding.setVariable(Constants.GROOVY_BINDING_LOG, Log.log);
        binding.setVariable(Constants.RSCONSOLE_BINDING_VARS, this.variables);
        binding.setVariable(Constants.RSCONSOLE_BINDING_SYS, this.systems);
        binding.setVariable(Constants.RSCONSOLE_BINDING_ENV, this.configProperty.getProperties());

        // binding.setVariable(Constants.RSCONSOLE_BINDING_DB, dbConnection); - initialized when connected
        // binding.setVariable(Constants.GROOVY_BINDING_ESB, MainBase.esb); - initialized when connected
    } // initBinding

    void initDBConnections()
    {
        Log.log.debug("Loading DB Connection Properties from connections.properties");
        File connections = FileUtils.getFile(release.serviceName + "/" + dbConnectionsFile);
        if (connections.exists())
        {
            FileInputStream reader = null;
            try
            {
                reader = new FileInputStream(connections);
                dbConnectionProperties = new Properties();
                dbConnectionProperties.load(reader);
                reader.close();
            }
            catch (Exception e)
            {
                Log.log.warn("Unable to Load DB Connection properties from " + dbConnectionsFile);
            }
        }
        
        //new GenericJDBCHandler().refreshTableMap(null);
    }

    void executeCommand(String command) throws Exception
    {
        if (!StringUtils.isEmpty(command))
        {
            Log.log.debug("command: " + command);
            History.log(command);

            // parse input
            Object msg = parse(command);
            Log.log.debug("  content: " + msg);
            if (msg != null)
            {
                sendMessage(msg);
            }
        }
    } // executeCommand

    protected void initENC() throws Exception
    {
        // load configuration state and initialization overrides
        Log.log.info("Loading configuration settings for enc");
        loadENCConfig();
        super.initENC();
    } // initENC

    protected void loadENCConfig() throws Exception
    {
        ConfigENC.load();
    }

    protected void exit(String[] argv)
    {
        Log.log.info("Terminate initiated");
        System.exit(0);
    } // exit

    protected void terminate()
    {
        try
        {
            Log.log.info("Terminating " + release.name.toUpperCase());

            // remove username from configId.guid
            //int pos = configId.guid.indexOf("-");
            //if (pos > 0)
            //{
            //    configId.guid = configId.guid.substring(0, pos);
            //}
            
            // remove configId.guid to allow for simultaneous sessions
            configId.guid = "";
            
            if (dbConnected)
            {
                Log.log.info("Disconnecting from Database");
                dbConnection.close();
            }

            exitSaveConfig();

            super.terminate();
        }
        catch (Exception e)
        {
            Log.log.error("Failed to terminate RSConsole: " + e.getMessage(), e);
        }
    } // terminate

    protected void loadConfig(XDoc configDoc) throws Exception
    {
        // override and additional configX.load
        configGeneral = new ConfigGeneral(configDoc);
        configGeneral.load();

        configSession = new ConfigSession(configDoc);
        configSession.load();

        configUser = new ConfigUser(configDoc);
        configUser.load();

        super.loadConfig(configDoc);
    } // loadConfig

    protected void saveConfig() throws Exception
    {
        // additional configX.save
        configSession.save();

        // users
        configUser.save();

        if (configGeneral != null)
        {
            configGeneral.save();
        }
        //don't save configId inorder to allow multiple simultaneous sessions
        //if (configId != null)
        //{
            //configId.save();
        //}
        if (configProperty != null)
        {
            configProperty.save();
        }
        if (configRegistration != null)
        {
            configRegistration.save();
        }
        
        if (configESB != null)
        {
            configESB.save();
        }

    } // saveConfig

    public void save()
    {
        super.exitSaveConfig();
    } // save

    public void exitSaveConfig()
    {
        // saving configuration // saving configuration
        if (!overrideSaveConfigOnExit && configGeneral.saveConfigOnExit == true)
        {
            try
            {
                Log.log.info("Saving configuration");

                // save config
                saveConfig();

                // copy config.xml to config.bak
                Log.log.info("Backup configuration");
                CopyFile.setRevisions(configGeneral.configRevisions);
                CopyFile.backup(configFile.getAbsolutePath());

                // save configuration to file
                configDoc.toPrettyFile(configFile);
                Log.log.info("Saved configuration");
            }
            catch (Exception e)
            {
                Log.log.error("Unable to save configuration file: " + e.getMessage(), e);
            }

        }
    } // exitSaveConfig

    protected void initLog() throws Exception
    {
        super.initLog();

        // init logging
        History.init(release.serviceName + "/" + logConfigFilename);
        History.log("*** STARTING " + release.name.toUpperCase() + " ***");
    } // initLog

    protected void initESB() throws Exception
    {
        if (!connected)
        {
            // initialize ESB
            Main.println();
            Main.println("Connecting with secure network...");
            Main.println();
            super.initESB();

            // start message bus
            startESB();

            // set binding
            binding.setVariable(Constants.GROOVY_BINDING_ESB, MainBase.esb);
            connected = true;
        }
    } // initESB

    @Override
    protected void startESB() throws Exception
    {
        super.startESB();

        MListener mListener = mServer.createListener(MainBase.main.configId.getGuid(), "com.resolve.rsconsole");
        mListener.init(false);
        
        // HP: TODO Need to figure out better solution than hard coding durable queues/topics here
        mServer.createDurableQueue(Constants.ESB_NAME_DURABLE_EXECUTEQUEUE);
        
        if(mServer != null)
        {
            mServer.setActive(true);
        }
    } // startESB

    protected void closeESB() throws Exception
    {
        if (connected)
        {
            Main.println();
            Main.println("Disconnecting with secure network...");
            Main.println();

            setSession(VAL_DEFAULT_MSGROUTE);

            // stop message bus
            super.closeESB();
            mServer = null;

            // set binding
            binding.setVariable(Constants.GROOVY_BINDING_ESB, null);

            connected = false;
        }
    } // disconnectESB

    protected void initRemoteESB(String brokerAddr, String brokerPort, String username, String password, String brokerAddr2, String brokerPort2)
                    throws Exception
    {
        if (connected)
        {
            closeESB();
        }
        configESB.setAddress(brokerAddr, brokerPort);
        if (username != null && password != null)
        {
            configESB.setUserPass(configESB.config.getUsername(), configESB.config.getP_assword(), username, password);
        }
        configESB.setAddress2(brokerAddr2, brokerPort2);
    } // initRemoteESB

    protected void initRemoteESB(String brokerAddr, String brokerPort, String username, String password) throws Exception
    {
        initRemoteESB(brokerAddr, brokerPort, username, password, null, null);
    } // initRemoteESB

    void authenticate()
    {
        // prompt for username / password if not provided via command line
        // arguments
        if (username == null || password == null)
        {
            try
            {
                BufferedReader br = new BufferedReader(new InputStreamReader(System.in));

                print("Username: ");
                username = br.readLine();
                password = PasswordField.getPassword("Password:  ");
            }
            catch (IOException e)
            {
                Log.log.error("Failed to intialize username and password: " + e.getMessage());
            }
        }

        // check ESB
        if (!configUser.authenticate(username, password))
        {
            println();
            println("Invalid username or password.");
            println();
            System.exit(1);
        }

        // append guid with username
//        configId.guid = configId.guid + "-" + username;

        // set user
        setUsername(username);

        println();

    } // authenticate

    void initSessions() throws Exception
    {
        // queues
        sessions.put(Constants.ESB_NAME_RSCONTROL, Constants.ESB_NAME_RSCONTROL);
        sessions.put(Constants.ESB_NAME_RSCONFIG, Constants.ESB_NAME_RSCONFIG);
        sessions.put(Constants.ESB_NAME_RSSERVER, Constants.ESB_NAME_RSSERVER);
        sessions.put(Constants.ESB_NAME_RSREMOTE, Constants.ESB_NAME_RSREMOTE);
        sessions.put(Constants.ESB_NAME_RSMGMT, Constants.ESB_NAME_RSMGMT);
        sessions.put(Constants.ESB_NAME_RSVIEW, Constants.ESB_NAME_RSVIEW);
        sessions.put(Constants.ESB_NAME_RSWIKI, Constants.ESB_NAME_RSWIKI);

        // topics
        sessions.put(Constants.ESB_NAME_BROADCAST, Constants.ESB_NAME_BROADCAST);
        sessions.put(Constants.ESB_NAME_RSPROCESS, Constants.ESB_NAME_RSPROCESS);
        sessions.put(Constants.ESB_NAME_RSSERVERS, Constants.ESB_NAME_RSSERVERS);
        sessions.put(Constants.ESB_NAME_RSCONTROLS, Constants.ESB_NAME_RSCONTROLS);
        sessions.put(Constants.ESB_NAME_RSACTIONS, Constants.ESB_NAME_RSACTIONS);
        sessions.put(Constants.ESB_NAME_RSREMOTES, Constants.ESB_NAME_RSREMOTES);
        sessions.put(Constants.ESB_NAME_RSMGMTS, Constants.ESB_NAME_RSMGMTS);
        sessions.put(Constants.ESB_NAME_RSVIEWS, Constants.ESB_NAME_RSVIEWS);
        sessions.put(Constants.ESB_NAME_RSWIKIS, Constants.ESB_NAME_RSWIKIS);

    } // initSessions

    void initVariables() throws Exception
    {
        variables = new Properties();

        // Message Handler (for introspection use DEFAULT)
        variables.setDefault(VAR_MSG_TYPE, VAL_DEFAULT_MSGTYPE);

        // message route (use
        // GUID1/NAME1#Class1.Method1&GUID2/NAME2#Class2.Method2&...)
        variables.setDefault(VAR_MSG_ROUTE, VAL_DEFAULT_MSGROUTE);

        // callback Class.method() invoked on returned messages
        variables.setDefault(VAR_MSG_CALLBACK, VAL_DEFAULT_MSGCALLBACK);

        // location for the current console command
        variables.setDefault(VAR_CONS_LOCATION, "/");

        // RSCONSOLE target and targetOverride
        variables.setDefault(VAR_CONS_TARGET, "");
        variables.setDefault(VAR_CONS_TARGETOVERRIDE, "");

        // authenticate username and password with RSCONTROL
        variables.setDefault(VAR_CONS_USERNAME, "TESTUSER");

    } // initVariables

    void initSystemConfig()
    {
        String systemFile = "rsconsole/config/system.cfg";

        systems = new Properties(systemFile);
    } // initSystemConfig

    void initCommands()
    {
        commands = new HashMap();

        setCommand(new CMDQuit());
        setCommand(new CMDExit());
        setCommand(new CMDHelp());
        setCommand(new CMDSet());
        setCommand(new CMDUnset());
        setCommand(new CMDGet());
        setCommand(new CMDSave());
//        setCommand(new CMDSend());
        setCommand(new CMDDest());
        setCommand(new CMDMethod());
        setCommand(new CMDPwd());
        setCommand(new CMDCd());
        setCommand(new CMDCdDotDot());
        setCommand(new CMDDir());
        setCommand(new CMDLs());
        setCommand(new CMDConnect());
        setCommand(new CMDDisconnect());
        setCommand(new CMDJMSConnect());
        setCommand(new CMDDBConnect());
        setCommand(new CMDDBDisconnect());
        setCommand(new CMDDBStatus());
        setCommand(new CMDRSEncrypt());
        setCommand(new CMDRSView());
//        setCommand(new CMDTarget());
//        setCommand(new CMDAction());
    } // initCommands


    void setCommand(CMDBase cmd)
    {
        commands.put(cmd.getName().toUpperCase(), cmd);
    } // setCommand

    CMDBase getCommand(String name)
    {
        return (CMDBase) commands.get(name.toUpperCase());
    } // getCommand

    void printTitle()
    {
        System.out.println();
        System.out.println("RSConsole [Version " + release.version + "]");
        System.out.println(release.copyright);
        System.out.println();
    } // printTitle

    void printPrompt() throws Exception
    {
        // init message header
        MMsgHeader header = new MMsgHeader();
        header.setRoute(variables.get(VAR_MSG_ROUTE));
        MMsgDest dest = header.getDest();

        if (dest == null || dest.getAddress().equalsIgnoreCase("NULL") || !dest.isDefined())
        {
            String session = "";
            variables.set(VAR_CONS_SESSION, session);
        }

        String session = variables.get(VAR_CONS_SESSION);
        String target = variables.get(VAR_CONS_TARGET);
        if (!StringUtils.isEmpty(target))
        {
            session = session + ":" + target;
        }
        String location = variables.get(VAR_CONS_LOCATION);
        Main.print(session + "#" + location + "> ");
    } // printPrompt

    Object parse(String line) throws Exception
    {
        Object result = null;
        boolean externalHelp = false;

        Tokenizer tokenizer = new Tokenizer(line, ' ', '"');
        String[] tokens = tokenizer.getAllTokens();
        if (tokens.length > 0)
        {
            String cmdName = tokens[0];

            // check if help for command
            CMDBase command = getCommand(cmdName);
            if (command != null)
            {
                // HELP <command name>
                if (cmdName.equalsIgnoreCase("HELP") && tokens.length > 1)
                {
                    CMDBase helpCommand = getCommand(tokens[1]);
                    if (helpCommand != null)
                    {
                        Main.println(helpCommand.getHelp());
                    }
                    else
                    {
                        externalHelp = true;
                    }
                }

                // COMMAND (including help)
                else
                {
                    result = command.execute(tokens);
                }
            }

            // script.cons or script.groovy command
            if (command == null || externalHelp == true)
            {
                if (externalHelp)
                {
                    cmdName = tokens[1];
                }

                // execute script
                File scriptFile = findScriptFile(cmdName);
                if (scriptFile.exists())
                {
                    Log.log.debug("Execute Script: " + scriptFile);
                    result = executeScript(scriptFile, tokens);
                    Log.log.debug("Result: " + result);
                }

                // unknown command
                else
                {
                    Main.println("Unknown command or script");
                    Main.println();
                }
            }
        }

        return result;
    } // parse

    File findScriptFile(String command) throws IOException
    {
        String location = variables.get(VAR_CONS_LOCATION);

        // check absolute command filename
        String pathname = getGSEHome() + "/" + location + "/";
        if (command.startsWith("/"))
        {
            pathname = getGSEHome()+"/";
        }

        // update pathname and set command
        String fullpathname = pathname + command;
        File dir = FileUtils.getFile(fullpathname);
        int pos = dir.getCanonicalPath().lastIndexOf(dir.getName());
        if (pos != -1)
        {
            pathname = dir.getCanonicalPath().replace('\\', '/').substring(0, pos);
        }
        String filename = dir.getName();

        // test different filename extension
        // String filename = cmdName;
        File scriptFile = FileUtils.getFile(pathname + filename);
        if (!scriptFile.exists())
        {
            // try .cons extension
            scriptFile = FileUtils.getFile(pathname + filename + ".cons");
            if (!scriptFile.exists())
            {
                // try .groovy extension
                scriptFile = FileUtils.getFile(pathname + filename + ".groovy");
            }
        }

        // search current directory for filename.groovy with ignorecase
        if (!scriptFile.exists())
        {
            boolean match = false;

            dir = FileUtils.getFile(pathname);
            File[] filenames = dir.listFiles();
            for (int i = 0; !match && i < filenames.length; i++)
            {
                String entryName = filenames[i].getName();
                String matchName = filename + ".groovy";
                if (!filenames[i].isDirectory() && matchName.equalsIgnoreCase(entryName))
                {
                    match = true;
                    scriptFile = filenames[i];
                }
            }
        }
        return scriptFile;
    }

    Object executeScript(File scriptFile, String[] argsArray) throws Exception
    {
        boolean isHelp = false;
        Object result = null;
        if (argsArray != null && argsArray.length > 0)
        {
            if (argsArray[0].equalsIgnoreCase("HELP"))
            {
                isHelp = true;
            }
        }
        else
        {
            argsArray = new String[] { scriptFile.getName() };
        }

        // set args binding
        binding.setVariable(Constants.RSCONSOLE_BINDING_HELP, isHelp);
        binding.setVariable(Constants.GROOVY_BINDING_ARGS.toLowerCase(), argsArray); // args
        binding.setVariable(Constants.GROOVY_BINDING_ARGS, argsArray); // ARGS

        // console command script
        if (scriptFile.getName().endsWith(".cons"))
        {
            executeConsoleScript(scriptFile);
        }

        // groovy script
        else if (scriptFile.getName().endsWith(".groovy"))
        {

            String execFilename = scriptFile.getCanonicalPath();
            execFilename = execFilename.replace('\\', '/');

            File file = FileUtils.getFile(execFilename);
            String script = FileUtils.readFileToString(file, "UTF-8");

            String scriptName = scriptFile.getName().substring(0, scriptFile.getName().indexOf('.'));

            // execute
            if (isHelp)
            {
                println();
            }
            result = GroovyScript.execute(script, scriptName, true, binding);
        }

        return result;
    } // executeScript

    void executeConsoleScript(File file) throws Exception
    {
        List commands = FileUtils.readLines(file, "UTF-8");
        for (Iterator i = commands.iterator(); i.hasNext();)
        {
            String command = (String) i.next();
            executeCommand(command);
        }
    } // executeConsoleScript

    boolean setLocation(String dir) throws Exception
    {
        boolean result = false;

        String location = variables.get(VAR_CONS_LOCATION);

        // process ..
        if (dir.equals(".."))
        {
            String[] currPaths = location.split("/");
            location = StringUtils.join(currPaths, "/", 0, currPaths.length - 2);
            if (location == null)
            {
                location = "/";
            }
            dir = "";
        }

        // absolute dir name
        else if (dir.startsWith("/"))
        {
            location = dir;
        }

        // relative dir name
        else
        {
            if (!location.endsWith("/"))
            {
                dir = "/" + dir;
            }
            location = location + dir;
        }

        // check if valid directory location
        String pathname = getGSEHome()+"/"+location;
        File file = FileUtils.getFile(pathname);
        if (file.exists() && file.isDirectory())
        {
            variables.set(VAR_CONS_LOCATION, location);
            result = true;
        }
        else
        {
            Main.println("Invalid directory path");
            Main.println();
            result = false;
        }
        return result;
    } // setLocation

    public static void print(String out)
    {
        System.out.print(out);
    } // print

    public static void println()
    {
        System.out.println("");
    } // println

    public static void println(String out)
    {
        System.out.println(out);
    } // println

    public boolean sendMessage(String guid, String classMethod, Object content) throws Exception
    {
        return sendMessage(guid, classMethod, content, "MDefault.ignore");
    } // sendMEssage

    public boolean sendMessage(String guid, String classMethod, Object content, String callback) throws Exception
    {
        // initESB
        if (!connected)
        {
            initESB();
        }

        String msgtype = variables.get(VAR_MSG_TYPE);
        String route = guid + "#" + classMethod;

        // init callaback
        if (callback == null)
        {
            callback = "MDefault.ignore";
        }

        // init message header
        MMsgHeader header = new MMsgHeader();
        header.setType(msgtype);
        header.setRoute(route);
        header.setCaller(release.getHostServiceName());

        // init content
        if (content == null)
        {
            content = new HashMap();
        }

        // send message
        boolean result = false;
        try
        {
            result = mServer.sendMessage(header, content, callback);
        }
        catch (Exception jmse)
        {
            if (jmse.getMessage().contains("Invalid destination"))
            {
                String session = variables.get(VAR_CONS_SESSION);
                sessions.remove(session);
                setSession(VAL_DEFAULT_MSGROUTE);
            }
            throw jmse;
        }
        return result;
        // return mServer.sendMessage(header, content, callback);
    } // sendMessage

    public boolean sendMessage(Object content) throws Exception
    {
        String msgtype = variables.get(VAR_MSG_TYPE);
        String route = variables.get(VAR_MSG_ROUTE);
        String callback = variables.get(VAR_MSG_CALLBACK);

        Log.log.debug("ROUTE: " + route);
        setMethod(null, null);

        if (route.equalsIgnoreCase(VAL_DEFAULT_MSGROUTE))
        {
            throw new Exception("Invalid message route: " + route);
        }
        else if (!connected)
        {
            initESB();
        }

        // init message header
        MMsgHeader header = new MMsgHeader();
        header.setType(msgtype);
        header.setRoute(route);
        header.setCaller(release.getHostServiceName());

        // get current destination
        MMsgDest dest = header.getRoute().getDest();

        // send message
        boolean result = false;
        try
        {
            result = mServer.sendMessage(header, content, callback);
        }
        catch (Exception jmse)
        {
            if (jmse.getMessage().contains("Invalid destination"))
            {
                String session = variables.get(VAR_CONS_SESSION);
                sessions.remove(session);
                setSession(VAL_DEFAULT_MSGROUTE);
            }
            throw jmse;
        }
        return result;
        // return mServer.sendMessage(header, content, callback);
    } // sendMessage

    public boolean setSession(String sessionName) throws Exception
    {
        boolean result = false;

        String guidName = (String) sessions.get(sessionName);
        if (!StringUtils.isEmpty(guidName))
        {
            setSessionDest(guidName);
            variables.set(VAR_CONS_SESSION, sessionName.toUpperCase());
            result = true;
        }
        else if (sessionName.equals(VAL_DEFAULT_MSGROUTE))
        {
            setSessionDest(sessionName);
            variables.set(VAR_CONS_SESSION, "");
        }
        return result;
    } // setSession

    public void setSessionDest(String address) throws Exception
    {
        if (address.equals(VAL_DEFAULT_MSGROUTE))
        {
            variables.set(VAR_MSG_ROUTE, VAL_DEFAULT_MSGROUTE);
        }
        else
        {
            String msgRoute = variables.get(VAR_MSG_ROUTE);
            MMsgRoute route = new MMsgRoute(msgRoute);
            MMsgDest dest = route.getDest();
            if (dest == null)
            {
                dest = new MMsgDest();
                route.add(dest);
            }
            dest.setAddress(address);
            variables.set(VAR_MSG_ROUTE, route.toString());
        }
    } // setSessionDest

    public void setMethod(String classMethod, String callback) throws Exception
    {
        setMethod(classMethod);
        setCallback(callback);
    } // setMethod

    public void setMethod(String classMethod) throws Exception
    {
        if (!variables.get(VAR_MSG_ROUTE).equalsIgnoreCase(VAL_DEFAULT_MSGROUTE))
        {
            if (classMethod == null)
            {
                classMethod = "MDefault.ignore";
            }
            MMsgDest dest;

            String msgRoute = variables.get(VAR_MSG_ROUTE);
            MMsgRoute route = new MMsgRoute(msgRoute);
            dest = route.getDest();
            if (dest == null)
            {
                dest = new MMsgDest();
                route.add(dest);
            }
            dest.setClassMethod(classMethod);
            variables.set(VAR_MSG_ROUTE, route.toString());
        }
    } // setMethod

    public void setCallback(String callback) throws Exception
    {
        if (callback == null)
        {
            callback = VAL_DEFAULT_MSGCALLBACK;
        }
        variables.set(VAR_MSG_CALLBACK, callback);
    } // setCallback

    public String getUsername()
    {
        return this.username;
    } // getUsername

    void setUsername(String username)
    {
        this.username = username;
        History.setUsername(username);
    } // setUsername

    public String getDist()
    {
        return configGeneral.getHome();
    } // setDist

    public boolean isDBConnected()
    {
        return dbConnected;
    } // isDBConnected

} // Main
