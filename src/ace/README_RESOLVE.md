Building
----------

To build for resolve you must have node installed.  Run the following command from the ace folder after doing a git pull to get the freshest information:

node ./Makefile.dryice.js -s --m

This will shrinkwrap all the files into ace-min.js and compress it with the --m

Then copy the build/ace-min.js file to the rsview/war/js folder