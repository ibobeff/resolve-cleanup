ALTER TABLE sys_user MODIFY COLUMN user_name VARCHAR(255);
ALTER TABLE audit_log MODIFY COLUMN u_username VARCHAR(255);
ALTER TABLE meta_filter MODIFY COLUMN u_userid VARCHAR(255);
ALTER TABLE meta_table_view MODIFY COLUMN u_user VARCHAR(255);
ALTER TABLE resolve_session MODIFY COLUMN u_active_type VARCHAR(255);

DELIMITER $$
CREATE PROCEDURE UPDATE_USER_NAME()
BEGIN
    DECLARE done INT DEFAULT 0;
    DECLARE tableName VARCHAR(64);

    DECLARE tableCur CURSOR FOR SELECT DISTINCT(table_name) FROM information_schema.columns WHERE table_schema = SCHEMA() AND character_maximum_length < 255 AND (column_name='SYS_CREATED_BY' OR column_name='SYS_UPDATED_BY');
    DECLARE CONTINUE HANDLER FOR NOT FOUND SET done = 1;
    OPEN tableCur;

    alterLoop: LOOP
        FETCH tableCur INTO tableName;
        IF done = 1 THEN
            LEAVE alterLoop;
        END IF;
        
        
        IF tableName IS NOT NULL THEN
            SET @SQL := CONCAT_WS(' ', 'ALTER TABLE ',tableName,' MODIFY COLUMN sys_created_by VARCHAR(255), MODIFY COLUMN sys_updated_by VARCHAR(255)');
            PREPARE alterStmt FROM @SQL;
            EXECUTE alterStmt;
            DEALLOCATE PREPARE alterStmt;
        END IF;
    END LOOP;

    CLOSE tableCur;
END$$
DELIMITER ;

CALL UPDATE_USER_NAME();
DROP PROCEDURE UPDATE_USER_NAME;

ALTER TABLE social_process MODIFY COLUMN u_owner VARCHAR(255);
ALTER TABLE social_team MODIFY COLUMN u_owner VARCHAR(255);
ALTER TABLE social_forum MODIFY u_owner VARCHAR(255);
ALTER TABLE rss_subscription MODIFY u_rss_owner VARCHAR(255);
ALTER TABLE content_request MODIFY u_assignee VARCHAR(255), MODIFY u_requester VARCHAR(255), MODIFY u_manager varchar(255);
ALTER TABLE service_request MODIFY u_assignee VARCHAR(255), MODIFY u_requester VARCHAR(255), MODIFY u_manager varchar(255);
ALTER TABLE resolve_impex_wiki MODIFY u_value VARCHAR(333);

ALTER TABLE meta_form_view DROP COLUMN u_meta_view_prop_sys_id;
ALTER TABLE meta_form_tab_field DROP COLUMN u_meta_style_sys_id;
ALTER TABLE meta_field_properties DROP COLUMN u_meta_style_sys_id;
ALTER TABLE meta_table_view_field DROP COLUMN u_meta_style_sys_id;

ALTER TABLE TMETRIC_LOOKUP ADD U_DT_ROOT_DOC VARCHAR(255);

DELETE FROM properties WHERE u_name='index.on.startup';
DELETE FROM properties WHERE u_name='indexing.enabled.wikiuiflag';
DELETE FROM properties WHERE u_name='index.resync';
DELETE FROM properties WHERE u_name='remove.expired.posts';

ALTER TABLE emailserver_filter MODIFY COLUMN u_query VARCHAR(4000);
ALTER TABLE ews_filter MODIFY COLUMN u_query VARCHAR(4000);
ALTER TABLE hpom_filter MODIFY COLUMN u_query VARCHAR(4000);
ALTER TABLE hpsm_filter MODIFY COLUMN u_query VARCHAR(4000);
ALTER TABLE remedyx_filter MODIFY COLUMN u_query VARCHAR(4000);
ALTER TABLE salesforce_filter MODIFY COLUMN u_query VARCHAR(4000);
ALTER TABLE servicenow_filter MODIFY COLUMN u_query VARCHAR(4000);
ALTER TABLE tsrm_filter MODIFY COLUMN u_query VARCHAR(4000);

ALTER TABLE catalog_attachment MODIFY COLUMN filename VARCHAR(256);
ALTER TABLE dt_wikidoc_wikidoc_rel MODIFY COLUMN u_wikidoc_fullname VARCHAR(256);
ALTER TABLE social_post_attachment MODIFY COLUMN u_filename VARCHAR(256);
ALTER TABLE wikiattachment MODIFY COLUMN u_filename VARCHAR(256);

ALTER TABLE archive_worksheet_debug DROP COLUMN u_worksheet;
ALTER TABLE metric_threshold MODIFY COLUMN u_ipaddress VARCHAR(256);
ALTER TABLE metric_threshold MODIFY COLUMN u_componenttype VARCHAR(200);
UPDATE sys_app_module SET resolve_group = null WHERE resolve_group = '' OR resolve_group = '&nbsp'

DROP INDEX u_name ON metric_threshold;

ALTER TABLE wikiarchive MODIFY COLUMN u_table_sys_id VARCHAR(32);
ALTER TABLE wikiarchive MODIFY COLUMN u_table_name VARCHAR(64);
ALTER TABLE wikiarchive MODIFY COLUMN u_table_column VARCHAR(64);

ALTER TABLE resolve_security_incident DROP INDEX rsi_sequence_idx;
ALTER TABLE resolve_security_incident ADD UNIQUE (u_sequence);

ALTER TABLE meta_form_view DROP INDEX mfv_u_form_name;
ALTER TABLE properties DROP INDEX prop_u_name_idx;
ALTER TABLE resolve_cns_name DROP INDEX rcn_u_name_idx;
ALTER TABLE resolve_mcp_group DROP INDEX rmc_u_groupname_idx;
ALTER TABLE resolve_preprocess DROP INDEX prep_u_name_idx;
ALTER TABLE resolve_registration DROP INDEX rr_uguid_idx;
ALTER TABLE resolve_tag DROP INDEX rt_u_name_idx;
ALTER TABLE sys_user DROP INDEX user_name_idx;
ALTER TABLE wikidoc DROP INDEX wd_name_namespace_idx;
ALTER TABLE wikidoc DROP INDEX wd_u_fullname_idx;
