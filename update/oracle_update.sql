ALTER TABLE sys_user MODIFY user_name VARCHAR(255);
ALTER TABLE audit_log MODIFY u_username VARCHAR(255);
ALTER TABLE meta_filter MODIFY u_userid VARCHAR(255);
ALTER TABLE meta_table_view MODIFY u_user VARCHAR(255);
ALTER TABLE resolve_session MODIFY u_active_type VARCHAR(255);

DELIMITER $$
BEGIN
FOR t IN (SELECT DISTINCT(table_name) FROM all_tab_cols WHERE owner = USER and char_length < 255 and (column_name='SYS_CREATED_BY' or column_name='SYS_UPDATED_BY')) LOOP
    EXECUTE IMMEDIATE ('ALTER TABLE ' || t.table_name || ' MODIFY (SYS_UPDATED_BY VARCHAR2(255), SYS_CREATED_BY VARCHAR2(255))');
END LOOP;
END$$
DELIMITER ;

ALTER TABLE social_process MODIFY u_owner VARCHAR(255);
ALTER TABLE social_team MODIFY u_owner VARCHAR(255);
ALTER TABLE social_forum MODIFY u_owner VARCHAR(255);
ALTER TABLE rss_subscription MODIFY u_rss_owner VARCHAR(255);
ALTER TABLE content_request MODIFY (u_assignee VARCHAR(255), u_requester VARCHAR(255), u_manager varchar(255));
ALTER TABLE service_request MODIFY (u_assignee VARCHAR(255), u_requester VARCHAR(255), u_manager varchar(255));
ALTER TABLE resolve_impex_wiki MODIFY u_value VARCHAR2(333);


ALTER TABLE meta_form_view DROP COLUMN u_meta_view_prop_sys_id;
ALTER TABLE meta_form_tab_field DROP COLUMN u_meta_style_sys_id;
ALTER TABLE meta_field_properties DROP COLUMN u_meta_style_sys_id;
ALTER TABLE meta_table_view_field DROP COLUMN u_meta_style_sys_id;

ALTER TABLE TMETRIC_LOOKUP ADD U_DT_ROOT_DOC VARCHAR2(255);

DELETE FROM properties WHERE u_name='index.on.startup';
DELETE FROM properties WHERE u_name='indexing.enabled.wikiuiflag';
DELETE FROM properties WHERE u_name='index.resync';
DELETE FROM properties WHERE u_name='remove.expired.posts';

ALTER TABLE emailserver_filter MODIFY u_query VARCHAR2(4000);
ALTER TABLE ews_filter MODIFY u_query VARCHAR2(4000);
ALTER TABLE hpom_filter MODIFY u_query VARCHAR2(4000);
ALTER TABLE hpsm_filter MODIFY u_query VARCHAR2(4000);
ALTER TABLE remedyx_filter MODIFY u_query VARCHAR2(4000);
ALTER TABLE salesforce_filter MODIFY u_query VARCHAR2(4000);
ALTER TABLE servicenow_filter MODIFY u_query VARCHAR2(4000);
ALTER TABLE tsrm_filter MODIFY u_query VARCHAR2(4000);

ALTER TABLE catalog_attachment MODIFY filename VARCHAR2(256);
ALTER TABLE dt_wikidoc_wikidoc_rel MODIFY u_wikidoc_fullname VARCHAR2(256);
ALTER TABLE social_post_attachment MODIFY u_filename VARCHAR2(256);
ALTER TABLE wikiattachment MODIFY u_filename VARCHAR2(256);

ALTER TABLE archive_worksheet_debug DROP COLUMN u_worksheet;
ALTER TABLE metric_threshold MODIFY u_ipaddress VARCHAR2(256);
ALTER TABLE metric_threshold MODIFY u_componenttype VARCHAR2(200);
UPDATE sys_app_module SET resolve_group = null WHERE resolve_group = '' OR resolve_group = '&nbsp'ALTER TABLE wikiattachment MODIFY u_filename VARCHAR2(256);

DROP INDEX SYS_C0019699;

ALTER TABLE wikiarchive MODIFY u_table_sys_id VARCHAR(32);
ALTER TABLE wikiarchive MODIFY u_table_name VARCHAR(64);
ALTER TABLE wikiarchive MODIFY u_table_column VARCHAR(64);

DROP INDEX rsi_sequence_idx;
ALTER TABLE resolve_security_incident ADD UNIQUE (u_sequence);

