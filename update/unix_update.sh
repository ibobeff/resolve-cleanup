#!/bin/bash
hash zip 2>/dev/null || { echo >&2 "Zip Command Is Required For Update. Please Install Before Re-Trying the Update Process"; exit 1; }

JDK="${DIST}"/jdk
MAJORVERSIONNEW=`head "${DIST}"/file/tmp/jdk_version.txt 2>&1 | awk 'NR==1{ gsub(/"/,""); split($4,a,/\./); print a[2] }'`
LESSERVERSIONNEW=`head "${DIST}"/file/tmp/jdk_version.txt 2>&1 | awk 'NR==1{ gsub(/"/,""); split($4,a,/_/); print a[2] }' | awk 'NR==1{split($1,b,/-/); print b[1]}'`
if [ -d "${JDK}" ]; then
    MAJORVERSION=`"${JDK}"/bin/java -fullversion 2>&1 | awk 'NR==1{ gsub(/"/,""); split($4,a,/\./); print a[2] }'`
    LESSERVERSION=`"${JDK}"/bin/java -fullversion 2>&1 | awk 'NR==1{ gsub(/"/,""); split($4,a,/_/); print a[2] }' | awk 'NR==1{split($1,b,/-/); print b[1]}'`
else
    MAJORVERSION=0
    LESSERVERSION=0
fi
if [ "${MAJORVERSION}" \< "${MAJORVERSIONNEW}" ]; then
    echo updating jdk to 1.${MAJORVERSIONNEW}.0.${LESSERVERSIONNEW}
    rm jdk/jre 2>/dev/null
    rm jdk/jdk 2>/dev/null
    zip -rq ${DIST}/jdk-1.${MAJORVERSION}.0.${LESSERVERSION}.zip ${JDK} >/dev/null 2>&1
    if [ $? -eq 0 ]; then
        rm -rf jdk
        unzip -oq "${DIST}"/file/linux64_jdk.zip -d "${DIST}"
        chmod 555 jdk/bin/*
    else
        echo "Unable to Update JDK Version From 1.${MAJORVERSION}.0.${LESSERVERSION} to 1.${MAJORVERSIONNEW}.0.${LESSERVERSIONNEW}"
        exit 1
    fi
elif [ "${LESSERVERSION}" \< "${LESSERVERSIONNEW}" ]; then
    echo updating jdk to 1.${MAJORVERSIONNEW}.0.${LESSERVERSIONNEW}
    rm jdk/jre 2>/dev/null
    zip -rq ${DIST}/jdk-1.${MAJORVERSION}.0.${LESSERVERSION}.zip ${JDK} >/dev/null 2>&1
    if [ $? -eq 0 ]; then
        rm -rf jdk
        unzip -oq "${DIST}"/file/linux64_jdk.zip -d "${DIST}"
        chmod 555 jdk/bin/*
    else
        echo "Unable to Update JDK Version From 1.${MAJORVERSION}.0.${LESSERVERSION} to 1.${MAJORVERSIONNEW}.0.${LESSERVERSIONNEW}"
        exit 1
    fi
fi

#Validate System Limits are Set Correctly
if [ "${ANSWER}" != "AUTO" ]; then
    RSSEARCH=`grep "^resolve.rssearch=" $BLUEPRINT | awk 'BEGIN{FS="="}{print $NF}' | tr [A-Z] [a-z]`
    if [ "${RSSEARCH}" == "true" ]; then
        XMS=`grep "^rssearch.run.Xms=" $BLUEPRINT | awk 'BEGIN{FS="="}{print $NF}'`
        XMX=`grep "^rssearch.run.Xmx=" $BLUEPRINT | awk 'BEGIN{FS="="}{print $NF}'`
        if [ "${XMS}" != "${XMX}" ];then
            echo "RSSearch Xms and Xmx values must be identical for memory locking."
            echo "Please edit the rssearch.run.Xms and Xmx values in the blueprint and make sure they are identical"
            echo "Do you Want to Continue The Update Anyway (Y/N)?"
            CONTINUE=
            while [ "${CONTINUE}x" == "x" ]
            do
                read inputline
                CONTINUE=`echo $inputline | tr [A-Z] [a-z]`
                if [ "${CONTINUE}" == "y" ] || [ "${CONTINUE}" == "yes" ]; then
                    CONTINUE="Y"
                elif [ "${CONTINUE}" == "n" ] || [ "${CONTINUE}" == "no" ]; then
                    CONTINUE="N"
                else
                    CONTINUE=
                    printf "Please Enter Y or N "
                fi
            done

            if [ "${CONTINUE}" == "N" ]; then
                echo "Cancelling Update"
                exit 1
            fi
        fi
        type sysctl >/dev/null 2>&1
        if [ $? -ne 0 ] ; then
            echo "Cannot find the sysctl command to validate that vm.max_map_count is set correctly."
            echo "If this value is not set to at least 262144 then Elasticsearch may not start.  Do you Want to Continue the Update Anyway (Y/N)?"
            CONTINUE=
            while [ "${CONTINUE}x" == "x" ]
            do
                read inputline
                CONTINUE=`echo $inputline | tr [A-Z] [a-z]`
                if [ "${CONTINUE}" == "y" ] || [ "${CONTINUE}" == "yes" ]; then
                    CONTINUE="Y"
                elif [ "${CONTINUE}" == "n" ] || [ "${CONTINUE}" == "no" ]; then
                    CONTINUE="N"
                else
                    CONTINUE=
                    printf "Please Enter Y or N "
                fi
            done

            if [ "${CONTINUE}" == "N" ]; then
                echo "Cancelling Update"
                exit 1
            fi
        else
            MAPCOUNT=`sysctl vm.max_map_count | awk 'BEGIN{FS="="}{print $2}'`
            if [ ${MAPCOUNT} -lt 262144 ]; then
                echo "Current vm.max_map_count (${MAPCOUNT}) is too low for the update to run successfully."
                echo "Please set it to at least 262144 before continuing with the update. This can be done temporarily by running sysctl -w vm.max_map_count=262144 as root, or permanently by configuring vm.max_map_count in /etc/sysctl.conf or by running the new script setup_sysctl.sh"
                echo "Do you Want to Continue The Update Anyway (Y/N)?"
                CONTINUE=
                while [ "${CONTINUE}x" == "x" ]
                do
                    read inputline
                    CONTINUE=`echo $inputline | tr [A-Z] [a-z]`
                    if [ "${CONTINUE}" == "y" ] || [ "${CONTINUE}" == "yes" ]; then
                        CONTINUE="Y"
                    elif [ "${CONTINUE}" == "n" ] || [ "${CONTINUE}" == "no" ]; then
                        CONTINUE="N"
                    else
                        CONTINUE=
                        printf "Please Enter Y or N "
                    fi
                done

                if [ "${CONTINUE}" == "N" ]; then
                    echo "Cancelling Update"
                    exit 1
                fi
            fi
        fi
        type ulimit >/dev/null 2>&1
        if [ $? -ne 0 ] ; then
            echo "Cannot find the ulimit command to validate that system limits are set correctly."
            echo "If these values are not set correctly then Elasticsearch may not start.  Do you Want to Continue the Update Anyway (Y/N)?"
            CONTINUE=
            while [ "${CONTINUE}x" == "x" ]
            do
                read inputline
                CONTINUE=`echo $inputline | tr [A-Z] [a-z]`
                if [ "${CONTINUE}" == "y" ] || [ "${CONTINUE}" == "yes" ]; then
                    CONTINUE="Y"
                elif [ "${CONTINUE}" == "n" ] || [ "${CONTINUE}" == "no" ]; then
                    CONTINUE="N"
                else
                    CONTINUE=
                    printf "Please Enter Y or N "
                fi
            done

            if [ "${CONTINUE}" == "N" ]; then
                echo "Cancelling Update"
                exit 1
            fi
        else
            MEMLOCK=`ulimit -a | grep "max locked memory" | awk '{print $NF}'`
            if [ "${MEMLOCK}" != "unlimited" ]; then
                echo "Current max lock memory ("${MEMLOCK}") should be unlimited to allow for RSSerach memory locking."
                echo "Please set it to unlimited before continuing with the installation. This can be done temporarily by running ulimit -l unlimited as root, or permanently by configuring nofile in /etc/security/limits.conf or by running the script setup_limits.sh. Note that using the later methods will require restrating the session to take effect."
                echo "Do you Want to Continue The Update Anyway (Y/N)?"
                CONTINUE=
                while [ "${CONTINUE}x" == "x" ]
                do
                    read inputline
                    CONTINUE=`echo $inputline | tr [A-Z] [a-z]`
                    if [ "${CONTINUE}" == "y" ] || [ "${CONTINUE}" == "yes" ]; then
                        CONTINUE="Y"
                    elif [ "${CONTINUE}" == "n" ] || [ "${CONTINUE}" == "no" ]; then
                        CONTINUE="N"
                    else
                        CONTINUE=
                        printf "Please Enter Y or N "
                    fi
                done

                if [ "${CONTINUE}" == "N" ]; then
                    echo "Cancelling Update"
                    exit 1
                fi
            fi
            OPENFILES=`ulimit -a | grep "open files" | awk '{print $NF}'`
            if [ ${OPENFILES} -lt 2048 ]; then
                echo "Current max file descriptors (${OPENFILES}) is too low for the install to run successfully."
                echo "Please set it to at least 2048 (recommended is 131072) before continuing with the installation. This can be done temporarily by running ulimit -n 131072 as root, or permanently by configuring nofile in /etc/security/limits.conf or by running the script setup_limits.sh. Note that using the later methods will require restrating the session to take effect."
                echo "Do you Want to Continue The Update Anyway (Y/N)?"
                CONTINUE=
                while [ "${CONTINUE}x" == "x" ]
                do
                    read inputline
                    CONTINUE=`echo $inputline | tr [A-Z] [a-z]`
                    if [ "${CONTINUE}" == "y" ] || [ "${CONTINUE}" == "yes" ]; then
                        CONTINUE="Y"
                    elif [ "${CONTINUE}" == "n" ] || [ "${CONTINUE}" == "no" ]; then
                        CONTINUE="N"
                    else
                        CONTINUE=
                        printf "Please Enter Y or N "
                    fi
                done

                if [ "${CONTINUE}" == "N" ]; then
                    echo "Cancelling Update"
                    exit 1
                fi
            fi
            MAXPROCESS=`ulimit -a | grep "max user processes" | awk '{print $NF}'`
            if [ ${MAXPROCESS} -lt 2048 ]; then
                echo "Current max user processes (${MAXPROCESS}) is too low for the install to run successfully."
                echo "Please set it to at least 2048 (recommended is 10240) before continuing with the installation. This can be done temporarily by running ulimit -u 10240 as root, or permanently by configuring nproc in /etc/security/limits.conf or by running the script setup_limits.sh. Note that using the later methods will require restrating the session to take effect."
                echo "Do you Want to Continue The Update Anyway (Y/N)?"
                CONTINUE=
                while [ "${CONTINUE}x" == "x" ]
                do
                    read inputline
                    CONTINUE=`echo $inputline | tr [A-Z] [a-z]`
                    if [ "${CONTINUE}" == "y" ] || [ "${CONTINUE}" == "yes" ]; then
                        CONTINUE="Y"
                    elif [ "${CONTINUE}" == "n" ] || [ "${CONTINUE}" == "no" ]; then
                        CONTINUE="N"
                    else
                        CONTINUE=
                        printf "Please Enter Y or N "
                    fi
                done

                if [ "${CONTINUE}" == "N" ]; then
                    echo "Cancelling Update"
                    exit 1
                fi
            fi
        fi
    fi
fi

rsmgmt/bin/run.sh -f install/resolve-update-cleanup -u 1>/dev/null
unzip -oq "${FILENAME}"
