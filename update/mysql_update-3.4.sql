ALTER TABLE sys_user MODIFY COLUMN user_password varchar(100);
ALTER TABLE archive_action_result DROP PRIMARY KEY, ADD PRIMARY KEY(sys_id,sys_created_on);
ALTER TABLE archive_action_result_lob DROP PRIMARY KEY, ADD PRIMARY KEY(sys_id,sys_created_on);
ALTER TABLE archive_execute_dependency DROP PRIMARY KEY, ADD PRIMARY KEY(sys_id,sys_created_on);
ALTER TABLE archive_execute_request DROP PRIMARY KEY, ADD PRIMARY KEY(sys_id,sys_created_on);
ALTER TABLE archive_execute_result DROP PRIMARY KEY, ADD PRIMARY KEY(sys_id,sys_created_on);
ALTER TABLE archive_execute_result_lob DROP PRIMARY KEY, ADD PRIMARY KEY(sys_id,sys_created_on);
ALTER TABLE archive_process_request DROP PRIMARY KEY, ADD PRIMARY KEY(sys_id,sys_created_on);
ALTER TABLE archive_worksheet DROP PRIMARY KEY, ADD PRIMARY KEY(sys_id,sys_created_on);
ALTER TABLE archive_worksheet_debug DROP PRIMARY KEY, ADD PRIMARY KEY(sys_id,sys_created_on);
ALTER TABLE meta_field_properties MODIFY COLUMN u_list_values VARCHAR(1000);
ALTER TABLE emailserver_filter add column u_query varchar(2000);
UPDATE emailserver_filter set u_query = concat(u_field, ' contains ''', u_regex, '''') where u_field is not null;

ALTER TABLE sys_user MODIFY COLUMN user_name VARCHAR(255);
ALTER TABLE audit_log MODIFY COLUMN u_username VARCHAR(255);
ALTER TABLE meta_filter MODIFY COLUMN u_userid VARCHAR(255);
ALTER TABLE meta_table_view MODIFY COLUMN u_user VARCHAR(255);
ALTER TABLE resolve_session MODIFY COLUMN u_active_type VARCHAR(255);

DELIMITER $$
CREATE PROCEDURE UPDATE_USER_NAME()
BEGIN
    DECLARE done INT DEFAULT 0;
    DECLARE tableName VARCHAR(64);

    DECLARE tableCur CURSOR FOR SELECT DISTINCT(table_name) FROM information_schema.columns WHERE table_schema = SCHEMA() AND character_maximum_length < 255 AND (column_name='SYS_CREATED_BY' OR column_name='SYS_UPDATED_BY');
    DECLARE CONTINUE HANDLER FOR NOT FOUND SET done = 1;
    OPEN tableCur;

    alterLoop: LOOP
        FETCH tableCur INTO tableName;
        IF done = 1 THEN
            LEAVE alterLoop;
        END IF;
        
        
        IF tableName IS NOT NULL THEN
            SET @SQL := CONCAT_WS(' ', 'ALTER TABLE ',tableName,' MODIFY COLUMN sys_created_by VARCHAR(255), MODIFY COLUMN sys_updated_by VARCHAR(255)');
            PREPARE alterStmt FROM @SQL;
            EXECUTE alterStmt;
            DEALLOCATE PREPARE alterStmt;
        END IF;
    END LOOP;

    CLOSE tableCur;
END$$
DELIMITER ;

CALL UPDATE_USER_NAME();
DROP PROCEDURE UPDATE_USER_NAME;

ALTER TABLE social_process MODIFY COLUMN u_owner VARCHAR(255);
ALTER TABLE social_team MODIFY COLUMN u_owner VARCHAR(255);
ALTER TABLE social_forum MODIFY u_owner VARCHAR(255);
ALTER TABLE rss_subscription MODIFY u_rss_owner VARCHAR(255);
ALTER TABLE content_request MODIFY u_assignee VARCHAR(255), MODIFY u_requester VARCHAR(255), MODIFY u_manager varchar(255);
ALTER TABLE service_request MODIFY u_assignee VARCHAR(255), MODIFY u_requester VARCHAR(255), MODIFY u_manager varchar(255);
ALTER TABLE resolve_impex_wiki MODIFY u_value VARCHAR(333);

ALTER TABLE meta_form_view DROP COLUMN u_meta_view_prop_sys_id;
ALTER TABLE meta_form_tab_field DROP COLUMN u_meta_style_sys_id;
ALTER TABLE meta_field_properties DROP COLUMN u_meta_style_sys_id;
ALTER TABLE meta_table_view_field DROP COLUMN u_meta_style_sys_id;
