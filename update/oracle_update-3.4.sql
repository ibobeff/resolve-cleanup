ALTER TABLE sys_user MODIFY user_password VARCHAR2(100);
ALTER TABLE meta_field_properties MODIFY u_list_values VARCHAR(1000);
ALTER TABLE emailserver_filter add u_query varchar(2000);
UPDATE emailserver_filter set u_query = u_field || ' contains ''' || u_regex || '''' where u_field is not null;
ALTER TABLE sys_user MODIFY user_name VARCHAR(255);
ALTER TABLE audit_log MODIFY u_username VARCHAR(255);
ALTER TABLE meta_filter MODIFY u_userid VARCHAR(255);
ALTER TABLE meta_table_view MODIFY u_user VARCHAR(255);
ALTER TABLE resolve_session MODIFY u_active_type VARCHAR(255);

DELIMITER $$
BEGIN
FOR t IN (SELECT DISTINCT(table_name) FROM all_tab_cols WHERE owner = USER and char_length < 255 and (column_name='SYS_CREATED_BY' or column_name='SYS_UPDATED_BY')) LOOP
    EXECUTE IMMEDIATE ('ALTER TABLE ' || t.table_name || ' MODIFY (SYS_UPDATED_BY VARCHAR2(255), SYS_CREATED_BY VARCHAR2(255))');
END LOOP;
END$$
DELIMITER ;

ALTER TABLE social_process MODIFY u_owner VARCHAR(255);
ALTER TABLE social_team MODIFY u_owner VARCHAR(255);
ALTER TABLE social_forum MODIFY u_owner VARCHAR(255);
ALTER TABLE rss_subscription MODIFY u_rss_owner VARCHAR(255);
ALTER TABLE content_request MODIFY (u_assignee VARCHAR(255), u_requester VARCHAR(255), u_manager varchar(255));
ALTER TABLE service_request MODIFY (u_assignee VARCHAR(255), u_requester VARCHAR(255), u_manager varchar(255));
ALTER TABLE resolve_impex_wiki MODIFY u_value VARCHAR2(333);

ALTER TABLE meta_form_view DROP COLUMN u_meta_view_prop_sys_id;
ALTER TABLE meta_form_tab_field DROP COLUMN u_meta_style_sys_id;
ALTER TABLE meta_field_properties DROP COLUMN u_meta_style_sys_id;
ALTER TABLE meta_table_view_field DROP COLUMN u_meta_style_sys_id;
