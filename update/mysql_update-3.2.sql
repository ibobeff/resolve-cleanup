UPDATE wikidoc SET u_has_active_model = 'Y' WHERE u_model_process IS NOT NULL;
UPDATE wikidoc SET u_has_active_model = 'N' WHERE u_model_process IS NULL;
ALTER TABLE tsrm_filter MODIFY COLUMN u_object VARCHAR(40); 
ALTER TABLE resolve_action_task MODIFY COLUMN u_summary VARCHAR(300);
ALTER TABLE wikidoc ADD COLUMN u_has_active_model CHAR(1);
ALTER TABLE resolve_impex_module DROP COLUMN u_log_history;
