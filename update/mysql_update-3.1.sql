ALTER TABLE resolve_action_result CHANGE u_summary u_summary LONGTEXT NULL;
ALTER TABLE worksheet CHANGE u_description u_description LONGTEXT NULL, CHANGE u_summary u_summary LONGTEXT NULL;
ALTER TABLE resolve_assess CHANGE u_name u_name VARCHAR(300) NOT NULL;
ALTER TABLE resolve_parser CHANGE u_name u_name VARCHAR(300) NOT NULL;
ALTER TABLE resolve_preprocess CHANGE u_name u_name VARCHAR(300) NOT NULL;
ALTER TABLE resolve_wiki_lookup CHANGE u_regex u_regex VARCHAR(333) NULL , CHANGE u_wiki u_wiki VARCHAR(300) NULL;
ALTER TABLE resolve_impex_module CHANGE u_name u_name VARCHAR(333) NULL;
ALTER TABLE resolve_action_pkg CHANGE u_name u_name VARCHAR(333) NULL;
DROP TABLE metric_jvm;
DROP TABLE metric_runbook;
DROP TABLE metric_day_jvm;
DROP TABLE metric_day_runbook;
DROP TABLE metric_day_server;
DROP TABLE metric_day_transaction;
DROP TABLE metric_day_users;
DROP TABLE metric_hr_jvm;
DROP TABLE metric_hr_runbook;
DROP TABLE metric_hr_server;
DROP TABLE metric_hr_transaction;
DROP TABLE metric_hr_users;
DROP TABLE metric_min_jvm;
DROP TABLE metric_min_runbook;
DROP TABLE metric_min_server;
DROP TABLE metric_min_transaction;
DROP TABLE metric_min_users;
