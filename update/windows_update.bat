@echo off
Setlocal EnableDelayedExpansion
pushd %DIST%

set MAJORVERSION=0
set LESSERVERSION=0
IF EXIST "jdk" (
	for /f "tokens=2-5 delims=.-_" %%j in ('"jdk\bin\java" -fullversion 2^>^&1') do (
		set MAJORVERSION=%%j
		set LESSERVERSION=%%l
	)
) ELSE IF EXIST "jre" (
	for /f "tokens=2-5 delims=.-_" %%j in ('"jre\bin\java" -fullversion 2^>^&1') do (
		set MAJORVERSION=%%j
		set LESSERVERSION=%%l
	)
) ELSE IF EXIST "jre7" (
	for /f "tokens=2-5 delims=.-_" %%j in ('"jre7\bin\java" -fullversion 2^>^&1') do (
		set MAJORVERSION=%%j
		set LESSERVERSION=%%l
	)
)

for /f "tokens=2-5 delims=.-_" %%j in ('type "%DIST%\file\tmp\jdk_version.txt"') do (
    set MAJORVERSIONNEW=%%j
    set LESSERVERSIONNEW=%%l
)

IF %MAJORVERSION% LSS %MAJORVERSIONNEW% (
	IF EXIST "jre" ( 
		zip -rq "%DIST%\jdk-1.%MAJORVERSION%.0.%LESSERVERSION%.zip" "jre" >nul 2>&1
	) ELSE If EXIST "jre7" (
		zip -rq "%DIST%\jdk-1.%MAJORVERSION%.0.%LESSERVERSION%.zip" "jre7" >nul 2>&1
	) ELSE IF EXIST "jdk" (
		zip -rq "%DIST%\jdk-1.%MAJORVERSION%.0.%LESSERVERSION%.zip" "jdk" >nul 2>&1
	)
	
    IF ERRORLEVEL 1 GOTO :ERROR
	If EXIST "jdk" rmdir /S /Q "jdk"
    "%DIST%\unzip" -oq "%DIST%/file/windows_jdk.zip" -d "%DIST%"
    GOTO :END
) ELSE IF %LESSERVERSION% LSS %LESSERVERSIONNEW% (
	IF EXIST "jre" ( 
		zip -rq "%DIST%\jdk-1.%MAJORVERSION%.0.%LESSERVERSION%.zip" "jre" >nul 2>&1
	) ELSE If EXIST "jre7" (
		zip -rq "%DIST%\jdk-1.%MAJORVERSION%.0.%LESSERVERSION%.zip" "jre7" >nul 2>&1
	) ELSE IF EXIST "jdk" (
		zip -rq "%DIST%\jdk-1.%MAJORVERSION%.0.%LESSERVERSION%.zip" "jdk" >nul 2>&1
	)
    IF ERRORLEVEL 1 GOTO :ERROR
	If EXIST "jdk" rmdir /S /Q "jdk"
    "%DIST%\unzip" -oq "%DIST%/file/windows_jdk.zip" -d "%DIST%"
    GOTO :END
)

:ERROR
    echo Unable to Update JDK Version From 1.%MAJORVERSION%.0.%LESSERVERSION% to 1.%MAJORVERSIONNEW%.0.%LESSERVERSIONNEW%
    exit /B 1

:END
call rsmgmt\bin\run.bat -f install/resolve-update-cleanup -u >nul 2>&1
"%DIST%\unzip" -oq "%DIST%\%ZIPFILENAME%" -d "%DIST%"
set MAJORVERSION=
set LESSERVERSION=
set MAJORVERSION=
set LESSERVERSION=
popd
