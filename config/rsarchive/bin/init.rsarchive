#!/bin/bash
#
# init.rsarchive
#
# Copyright (c) 2008 Resolve Systems
# All rights reserved.
#
### RedHat Service Information follows:
#
# By default, install service for runlevels 3 and 5, with
# start priority of 95 and stop priority of 05.
#
# chkconfig: 35 95 05
# description: Resolve RSArchive
#

R_USER=resolve
DIST=INSTALLDIR
LOCKFILE=$DIST/rsarchive/bin/lock
TERMINATETIMEOUT=600
LD_LIBRARY_PATH=$DIST/lib
RETVAL=0
export LD_LIBRARY_PATH

success()
{
    printf "OK"
}

failure()
{
    printf "FAILED"
}

status()
{
    if [ -f ${LOCKFILE} ] ; then
        pid=`cat ${LOCKFILE} | tr '\n' ' '`
        ps -p ${pid} > /dev/null 2>&1
        if [ $? -ne 0 ] ; then
           if [ -z "$pid" ]; then
               printf "DOWN"
           else
               printf "DOWN PID: "$pid""
           fi
            rm -f ${LOCKFILE}
        else
            printf "UP PID: "$pid""
        fi
    else
        pid=""
        printf "DOWN"
    fi

    linux=false
    sunos=false
    case "`uname`" in
    Linux*) linux=true;;
    SunOS*) sunos=true;;
    esac

    if $sunos; then
        PROCESS=`/usr/ucb/ps -augxwww | grep "${DIST}/rsarchive/lib/resolve-archive.jar" | nawk '/resolve-archive.jar/ && !/awk/ && !/grep/ {print $2}' | tr '\n' ' '`
    else
        PROCESS=`ps -ef | grep "${DIST}/rsarchive/lib/resolve-archive.jar" | awk '/resolve-archive.jar/ && !/awk/ && !/grep/ {print $2}' | tr '\n' ' '`
    fi

    if [ "${PROCESS}x" != "${pid}x" ]; then
        printf "\nHANGING $PROCESS"
    fi
}

util_inc ()
{
    cntvar=$1
    val="`eval echo \\$${cntvar}`"
    eval ${cntvar}=`expr ${val} + 1`
}

case "$1" in
start_msg)
    echo "Starting RSArchive"
    ;;

stop_msg)
    echo "Stopping RSArchive"
    ;;

status)
    printf "RSARCHIVE: "
    status
    printf "\n"
    ;;

start)
    ID=`id | sed -e "s/^uid=[0-9]*(\(.*\)) gid.*/\1/"`
    cd $DIST/rsarchive/bin
    printf "Starting RSArchive "
    if [ "${ID}x" = "${R_USER}x" ]; then
        "./run.sh" 1 > "$DIST/rsarchive/log/stdout" 2>&1
    else
        /bin/su $R_USER "./run.sh" 1 > "$DIST/rsarchive/log/stdout" 2>&1
    fi
    if [ $? -eq 0 ]; then
        success
    else
        failure
    fi
    printf "\n"
    if [ $? -ne 0 ] ; then
        exit 1
    fi
    ;;

start_safe)
    ID=`id | sed -e "s/^uid=[0-9]*(\(.*\)) gid.*/\1/"`
    cd $DIST/rsarchive/bin
    echo "Starting RSArchive "
    if [ "${ID}x" = "${R_USER}x" ]; then
        "./run.sh"
    else
        /bin/su $R_USER "./run.sh"
    fi
    if [ $? -ne 0 ] ; then
        exit 1
    fi
    ;;

stop)
    if [ -f ${LOCKFILE} ] ; then
        printf "Stopping RSArchive: "
        RETVAL=1

        if [[ "$2" =~ ^[0-9]+$ ]]; then
            TERMINATETIMEOUT=$2
        fi 

        pid=`cat ${LOCKFILE}`
        [ "$pid" != "" ] && kill -TERM ${pid}

        if [ $? -eq 0 ] ; then
            cnt=0
            while [ ${cnt} -lt ${TERMINATETIMEOUT} ] ; do
                printCnt=`expr ${cnt} % 5`
                if [ ${printCnt} -eq 0 ]; then
                    printf "."
                fi
                sleep 1
                ps -p ${pid} > /dev/null 2>&1
                if [ $? -ne 0 ] ; then
                    break
                fi
                util_inc cnt
            done
        else
            rm -f "${LOCKFILE}"
        fi

        ps -p ${pid} > /dev/null 2>&1
        if [ $? -eq 0 ] ; then
            printf "Timeout\n"
            printf "Graceful shutdown of RSArchive failed - killing it..."
            # Signal USR1 (3) will dump debug information to the agent log
            kill -3 ${pid}
            sleep 1
            ps -p ${pid} > /dev/null 2>&1
            if [ $? -eq 0 ] ; then
                kill -9 ${pid}
                sleep 2
            fi
            ps -p ${pid} > /dev/null 2>&1
            if [ $? -eq 0 ] ; then
                printf "Cannot kill RSArchive - giving up.\n"
                RETVAL = 0
            fi
        fi
        rm -f ${LOCKFILE}

        if [ $RETVAL -eq 1 ] ; then
            success
            RETVAL=0
        else
            failure
            RETVAL=1
        fi
        printf "\n"
    fi
    ;;

restart)
    $0 stop
    $0 start
    RETVAL=$?
    ;;

kill)
    $0 stop 1
    ;;

*)
    echo "Usage: $0 {start|start_safe|stop|kill|restart|status}"
    exit 1
    ;;
esac
exit $RETVAL
