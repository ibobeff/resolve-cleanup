/* create connection test table */
CONNECT resolve;
CREATE TABLE resolve_event 
(
   u_value VARCHAR(1024) DEFAULT NULL,
   sys_id VARCHAR(32) NOT NULL,
   sys_updated_by VARCHAR(255) DEFAULT NULL,
   sys_updated_on DATETIME DEFAULT NULL,
   sys_created_by VARCHAR(255) DEFAULT NULL,
   sys_created_on DATETIME DEFAULT NULL,
   sys_mod_count INT(11) DEFAULT NULL,
   PRIMARY KEY (sys_id)
) ENGINE=MYISAM DEFAULT CHARSET=utf8;

CREATE TABLE resolve_sequence_generator
(
   sys_id varchar(32) NOT NULL,
   u_name varchar(100) DEFAULT NULL,
   u_number int(11) DEFAULT NULL,
   PRIMARY KEY (sys_id)
 ) ENGINE=InnoDB DEFAULT CHARSET=utf8;


CREATE TABLE resolve_shared_object 
(
  sys_id varchar(32) NOT NULL,
  u_content longblob,
  u_name varchar(400) DEFAULT NULL,
  u_number int(11) DEFAULT NULL,
  PRIMARY KEY (sys_id)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

/*--------------------------------------------------------------------------------------*/
/* Following are the partitioned archive tables. 										*/
/*																						*/
/* In MySQL 5.1 although there is no TRUNCATE command here is a work around:			*/
/*																						*/
/* ALTER TABLE <TABLE_NAME> DROP PARTITON <PARTITION_NAME>								*/
/* ALTER TABLE <TABLE_NAME> ADD PARTITON (PARTITION <PARTITION_NAME> VALUES IN (<VALUE>)*/	 
/*																						*/
/* Example: 																			*/
/*																						*/
/* ALTER TABLE archive_action_result DROP PARTITON P_JAN								*/
/* ALTER TABLE archive_action_result ADD PARTITON (PARTITION P_JAN VALUES IN (1)		*/
CREATE TABLE archive_action_result (
	sys_id varchar(32) NOT NULL,
	u_address varchar(100),
	u_completion varchar(40),
	u_condition varchar(40),
	u_duration int(10),
	u_esbaddr varchar(100),
	u_severity varchar(40),
	u_timestamp bigint(19),
	sys_created_by varchar(255),
	sys_created_on datetime,
	sys_mod_count int(10),
	sys_updated_by varchar(255),
	sys_updated_on datetime(3),
	u_action_result_lob varchar(32),
	u_execute_request varchar(32),
	u_execute_result varchar(32),
	u_problem varchar(32),
	u_process varchar(32),
	u_target varchar(100),
	u_target_guid varchar(32),
	u_actiontask varchar(32),
    PRIMARY KEY (sys_id, sys_created_on)
) ENGINE=MyISAM 
  PARTITION BY LIST(MONTH(sys_created_on) ) (
    PARTITION P_JAN VALUES IN(1),
    PARTITION P_FEB VALUES IN(2),
    PARTITION P_MAR VALUES IN(3),
    PARTITION P_APR VALUES IN(4),
    PARTITION P_MAY VALUES IN(5),
    PARTITION P_JUN VALUES IN(6),
    PARTITION P_JUL VALUES IN(7),
    PARTITION P_AUG VALUES IN(8),
    PARTITION P_SEP VALUES IN(9),
    PARTITION P_OCT VALUES IN(10),
    PARTITION P_NOV VALUES IN(11),
    PARTITION P_DEC VALUES IN(12)
    );
/*try to create the table again incase they have a mysql version less than 5.6*/
CREATE TABLE archive_action_result (
	sys_id varchar(32) NOT NULL,
	u_address varchar(100),
	u_completion varchar(40),
	u_condition varchar(40),
	u_duration int(10),
	u_esbaddr varchar(100),
	u_severity varchar(40),
	u_timestamp bigint(19),
	sys_created_by varchar(255),
	sys_created_on datetime,
	sys_mod_count int(10),
	sys_updated_by varchar(255),
	sys_updated_on datetime,
	u_action_result_lob varchar(32),
	u_execute_request varchar(32),
	u_execute_result varchar(32),
	u_problem varchar(32),
	u_process varchar(32),
	u_target varchar(100),
	u_target_guid varchar(32),
	u_actiontask varchar(32),
    PRIMARY KEY (sys_id, sys_created_on)
) ENGINE=MyISAM 
  PARTITION BY LIST(MONTH(sys_created_on) ) (
    PARTITION P_JAN VALUES IN(1),
    PARTITION P_FEB VALUES IN(2),
    PARTITION P_MAR VALUES IN(3),
    PARTITION P_APR VALUES IN(4),
    PARTITION P_MAY VALUES IN(5),
    PARTITION P_JUN VALUES IN(6),
    PARTITION P_JUL VALUES IN(7),
    PARTITION P_AUG VALUES IN(8),
    PARTITION P_SEP VALUES IN(9),
    PARTITION P_OCT VALUES IN(10),
    PARTITION P_NOV VALUES IN(11),
    PARTITION P_DEC VALUES IN(12)
    );

CREATE TABLE archive_action_result_lob (
	sys_id varchar(32) NOT NULL,
	u_detail longtext,
	u_summary longtext,
	sys_created_by varchar(255),
	sys_created_on datetime,
	sys_mod_count int(10),
	sys_updated_by varchar(255),
	sys_updated_on datetime,
    PRIMARY KEY (sys_id, sys_created_on)
) ENGINE=MyISAM
  PARTITION BY LIST(MONTH(sys_created_on) ) (
    PARTITION P_JAN VALUES IN(1),
    PARTITION P_FEB VALUES IN(2),
    PARTITION P_MAR VALUES IN(3),
    PARTITION P_APR VALUES IN(4),
    PARTITION P_MAY VALUES IN(5),
    PARTITION P_JUN VALUES IN(6),
    PARTITION P_JUL VALUES IN(7),
    PARTITION P_AUG VALUES IN(8),
    PARTITION P_SEP VALUES IN(9),
    PARTITION P_OCT VALUES IN(10),
    PARTITION P_NOV VALUES IN(11),
    PARTITION P_DEC VALUES IN(12)
    );

CREATE TABLE archive_execute_dependency (
	sys_id varchar(32) NOT NULL,
	u_completion bit(1),
	u_condition varchar(40),
	u_execute varchar(32),
	u_expression varchar(4000),
	u_merge varchar(40),
	u_severity varchar(40),
	u_type varchar(40),
	sys_created_by varchar(255),
	sys_created_on datetime,
	sys_mod_count int(10),
	sys_updated_by varchar(255),
	sys_updated_on datetime,
	u_execute_request varchar(32),
    PRIMARY KEY (sys_id, sys_created_on)
) ENGINE=MyISAM
  PARTITION BY LIST(MONTH(sys_created_on) ) (
    PARTITION P_JAN VALUES IN(1),
    PARTITION P_FEB VALUES IN(2),
    PARTITION P_MAR VALUES IN(3),
    PARTITION P_APR VALUES IN(4),
    PARTITION P_MAY VALUES IN(5),
    PARTITION P_JUN VALUES IN(6),
    PARTITION P_JUL VALUES IN(7),
    PARTITION P_AUG VALUES IN(8),
    PARTITION P_SEP VALUES IN(9),
    PARTITION P_OCT VALUES IN(10),
    PARTITION P_NOV VALUES IN(11),
    PARTITION P_DEC VALUES IN(12)
    );

CREATE TABLE archive_execute_request (
	sys_id varchar(32) NOT NULL,
	u_duration int(10),
	u_event_id varchar(255),
	u_execute_status varchar(40),
	u_node_id varchar(255),
	u_node_label varchar(255),
	u_number varchar(40),
	u_trigger varchar(100),
	u_wiki varchar(255),
	sys_created_by varchar(255),
	sys_created_on datetime,
	sys_mod_count int(10),
	sys_updated_by varchar(255),
	sys_updated_on datetime,
	u_problem varchar(32),
	u_process varchar(32),
	u_task varchar(32),
    PRIMARY KEY (sys_id, sys_created_on)
) ENGINE=MyISAM
  PARTITION BY LIST(MONTH(sys_created_on) ) (
    PARTITION P_JAN VALUES IN(1),
    PARTITION P_FEB VALUES IN(2),
    PARTITION P_MAR VALUES IN(3),
    PARTITION P_APR VALUES IN(4),
    PARTITION P_MAY VALUES IN(5),
    PARTITION P_JUN VALUES IN(6),
    PARTITION P_JUL VALUES IN(7),
    PARTITION P_AUG VALUES IN(8),
    PARTITION P_SEP VALUES IN(9),
    PARTITION P_OCT VALUES IN(10),
    PARTITION P_NOV VALUES IN(11),
    PARTITION P_DEC VALUES IN(12)
    );

CREATE TABLE archive_execute_result (
	sys_id varchar(32) NOT NULL,
	u_address varchar(100),
	u_duration int(10),
	u_returncode int(10),
	u_target varchar(32),
	sys_created_by varchar(255),
	sys_created_on datetime,
	sys_mod_count int(10),
	sys_updated_by varchar(255),
	sys_updated_on datetime,
	u_request varchar(32),
	u_execute_result_lob varchar(32),
    PRIMARY KEY (sys_id, sys_created_on)
) ENGINE=MyISAM
  PARTITION BY LIST(MONTH(sys_created_on) ) (
    PARTITION P_JAN VALUES IN(1),
    PARTITION P_FEB VALUES IN(2),
    PARTITION P_MAR VALUES IN(3),
    PARTITION P_APR VALUES IN(4),
    PARTITION P_MAY VALUES IN(5),
    PARTITION P_JUN VALUES IN(6),
    PARTITION P_JUL VALUES IN(7),
    PARTITION P_AUG VALUES IN(8),
    PARTITION P_SEP VALUES IN(9),
    PARTITION P_OCT VALUES IN(10),
    PARTITION P_NOV VALUES IN(11),
    PARTITION P_DEC VALUES IN(12)
    );

CREATE TABLE archive_execute_result_lob (
	sys_id varchar(32) NOT NULL,
	u_command longtext,
	u_raw longtext,
	sys_created_by varchar(255),
	sys_created_on datetime,
	sys_mod_count int(10),
	sys_updated_by varchar(255),
	sys_updated_on datetime,
    PRIMARY KEY (sys_id, sys_created_on)
) ENGINE=MyISAM
  PARTITION BY LIST(MONTH(sys_created_on) ) (
    PARTITION P_JAN VALUES IN(1),
    PARTITION P_FEB VALUES IN(2),
    PARTITION P_MAR VALUES IN(3),
    PARTITION P_APR VALUES IN(4),
    PARTITION P_MAY VALUES IN(5),
    PARTITION P_JUN VALUES IN(6),
    PARTITION P_JUL VALUES IN(7),
    PARTITION P_AUG VALUES IN(8),
    PARTITION P_SEP VALUES IN(9),
    PARTITION P_OCT VALUES IN(10),
    PARTITION P_NOV VALUES IN(11),
    PARTITION P_DEC VALUES IN(12)
    );

CREATE TABLE archive_process_request (
	sys_id varchar(32) NOT NULL,
	u_duration int(10),
	u_number varchar(40),
	u_status varchar(40),
	u_timeout varchar(40),
	u_wiki varchar(255),
	sys_created_by varchar(255),
	sys_created_on datetime,
	sys_mod_count int(10),
	sys_updated_by varchar(255),
	sys_updated_on datetime,
	u_problem varchar(32),
    PRIMARY KEY (sys_id, sys_created_on)
) ENGINE=MyISAM
  PARTITION BY LIST(MONTH(sys_created_on) ) (
    PARTITION P_JAN VALUES IN(1),
    PARTITION P_FEB VALUES IN(2),
    PARTITION P_MAR VALUES IN(3),
    PARTITION P_APR VALUES IN(4),
    PARTITION P_MAY VALUES IN(5),
    PARTITION P_JUN VALUES IN(6),
    PARTITION P_JUL VALUES IN(7),
    PARTITION P_AUG VALUES IN(8),
    PARTITION P_SEP VALUES IN(9),
    PARTITION P_OCT VALUES IN(10),
    PARTITION P_NOV VALUES IN(11),
    PARTITION P_DEC VALUES IN(12)
    );

CREATE TABLE archive_worksheet (
	sys_id varchar(32) NOT NULL,
	u_alert_id varchar(333),
	u_condition varchar(40),
	u_correlation_id varchar(333),
	u_debug longtext,
	u_description longtext,
	u_number varchar(40),
	u_reference varchar(333),
	u_severity varchar(40),
	u_summary longtext,
	u_work_notes longtext,
	u_worksheet longtext,
	sys_created_by varchar(255),
	sys_created_on datetime,
	sys_mod_count int(10),
	sys_updated_by varchar(255),
	sys_updated_on datetime,
	u_assigned_to varchar(32),
    PRIMARY KEY (sys_id, sys_created_on)
) ENGINE=MyISAM
  PARTITION BY LIST(MONTH(sys_created_on) ) (
    PARTITION P_JAN VALUES IN(1),
    PARTITION P_FEB VALUES IN(2),
    PARTITION P_MAR VALUES IN(3),
    PARTITION P_APR VALUES IN(4),
    PARTITION P_MAY VALUES IN(5),
    PARTITION P_JUN VALUES IN(6),
    PARTITION P_JUL VALUES IN(7),
    PARTITION P_AUG VALUES IN(8),
    PARTITION P_SEP VALUES IN(9),
    PARTITION P_OCT VALUES IN(10),
    PARTITION P_NOV VALUES IN(11),
    PARTITION P_DEC VALUES IN(12)
    );

CREATE TABLE archive_worksheet_debug (
	sys_id varchar(32) NOT NULL,
	u_debug longtext,
	u_timestamp bigint(19),
	sys_created_by varchar(40),
	sys_created_on datetime,
	sys_mod_count int(10),
	sys_updated_by varchar(40),
	sys_updated_on datetime,
    PRIMARY KEY (sys_id, sys_created_on)
) ENGINE=MyISAM
  PARTITION BY LIST(MONTH(sys_created_on) ) (
    PARTITION P_JAN VALUES IN(1),
    PARTITION P_FEB VALUES IN(2),
    PARTITION P_MAR VALUES IN(3),
    PARTITION P_APR VALUES IN(4),
    PARTITION P_MAY VALUES IN(5),
    PARTITION P_JUN VALUES IN(6),
    PARTITION P_JUL VALUES IN(7),
    PARTITION P_AUG VALUES IN(8),
    PARTITION P_SEP VALUES IN(9),
    PARTITION P_OCT VALUES IN(10),
    PARTITION P_NOV VALUES IN(11),
    PARTITION P_DEC VALUES IN(12)
    );
