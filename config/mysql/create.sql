DROP DATABASE IF EXISTS resolve;

CREATE DATABASE resolve;

GRANT ALL PRIVILEGES ON *.* TO 'resolve'@'localhost' IDENTIFIED BY 'resolve';

GRANT ALL PRIVILEGES ON *.* TO 'resolve'@'127.0.0.1' IDENTIFIED BY 'resolve';

GRANT ALL PRIVILEGES ON *.* TO 'resolve'@'%' IDENTIFIED BY 'resolve';

/* GRANT ALL PRIVILEGES ON *.* TO 'resolve'@'HOSTNAME.DOMAINNAME.com' IDENTIFIED BY 'resolve'; */

FLUSH PRIVILEGES;

CONNECT mysql;
DELETE FROM mysql.user WHERE user='';
DELETE FROM mysql.user WHERE user='root' AND host='production.mysql.com';
