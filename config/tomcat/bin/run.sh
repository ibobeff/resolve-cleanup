#!/bin/bash
# -----------------------------------------------------------------------------
# Start Script for the CATALINA Server
#
# $Id: startup.sh 385888 2006-03-14 21:04:40Z keith $
# -----------------------------------------------------------------------------

# resolve java path
BIN=`echo $0 | awk '{ print substr( $0, 0, length($0)-6 ) }'`

cd ${BIN}../..

DIST="`pwd`"
. ${DIST}/tomcat/bin/rsview-env.sh
CATALINA_PID=${DIST}/tomcat/bin/lock

if [ -f ${CATALINA_PID} ]; then
    if [ $# -eq 0 ] || [ "$1" != "1" ]; then
        echo "Lock File Detected"
        echo "Cancelling RSView Startup"
        exit 1
    fi
fi

JAVA_HOME="${RSVIEWJDK}"

JAVA_RESOLVEMAJOR=`${DIST}/jdk/bin/java -version 2>&1 | awk 'NR==1{split($0,a,"."); print a[2] }'`
JAVA_RESOLVELESSER=`${DIST}/jdk/bin/java -version 2>&1 | awk  'NR==1{gsub(/"/,"");split($0,a,"_"); print a[2] }'`
JAVA_BLUEPRINTMAJOR=`${JAVA_HOME}/bin/java -version 2>&1 | awk 'NR==1{split($0,a,"."); print a[2] }'`
JAVA_BLUEPRINTLESSER=`${JAVA_HOME}/bin/java -version 2>&1 | awk  'NR==1{gsub(/"/,"");split($0,a,"_"); print a[2] }'`
if [ -d "${JAVA_HOME}" ];
then
	if [ ${JAVA_RESOLVEMAJOR} -ne ${JAVA_BLUEPRINTMAJOR} ]; then
			echo "supplied java version is different than the required version, using default resolve java"
			JAVA_HOME=${DIST}/jdk
	fi

	if [ ${JAVA_RESOLVELESSER} -gt ${JAVA_BLUEPRINTLESSER} ]; then
			echo "supplied java version is less than required, using default resolve java"
			JAVA_HOME=${DIST}/jdk
	fi
else
	echo given java_home folder does not exist using default resolve java
	set JAVA_HOME="$DIST/jdk"
fi
BASE="$DIST/tomcat/bin"

# Better OS/400 detection: see Bugzilla 31132
os400=false
darwin=false
linux=false
sunos=false
case "`uname`" in
CYGWIN*) cygwin=true;;
OS400*) os400=true;;
Darwin*) darwin=true;;
Linux*) linux=true;;
SunOS*) sunos=true;;
esac

# JCONSOLE - make sure "hostname -i" resolves to an ipaddress not 127.0.0.1
#JCONSOLE="-Dcom.sun.management.jmxremote.port=5550 -Dcom.sun.management.jmxremote.authenticate=false -Dcom.sun.management.jmxremote.ssl=false"

# Eclipse Debugging
#DEBUGGING="-Xdebug -Xrunjdwp:transport=dt_socket,address=8003,server=y,suspend=n"

# GC Logging
#GCLOGGING="-XX:+PrintGCDetails -verbose:gc -Xloggc:${DIST}/tomcat/logs/gc.log"

JAVA_OPTS=

# Randomness entropy
JAVA_OPTS="${JAVA_OPTS} -Djava.security.egd=file:///dev/urandom"

# OWASP ESAPI
JAVA_OPTS="${JAVA_OPTS} -Dorg.owasp.esapi.resources=${DIST}/tomcat/webapps/resolve/WEB-INF"

#Disable JDBC 4.1 auto-commit spec compliance property for latest Oracle JDBC Driver
JAVA_OPTS="${JAVA_OPTS} -Doracle.jdbc.autoCommitSpecCompliant=false"

if $sunos; then
  JAVA_OPTS="${JAVA_OPTS} -d64 -Xms256M -Xmx1024M -Xss256K -XX:MaxPermSize=512M -XX:+UseConcMarkSweepGC -XX:+CMSClassUnloadingEnabled -XX:+HeapDumpOnOutOfMemoryError"
else
  JAVA_OPTS="${JAVA_OPTS} -Xms256M -Xmx1024M -Xss256K -XX:MaxPermSize=512M -XX:+UseConcMarkSweepGC -XX:+CMSClassUnloadingEnabled -XX:+HeapDumpOnOutOfMemoryError"
fi

JAVA_OPTS="${JAVA_OPTS} ${JCONSOLE} ${DEBUGGING} ${GCLOGGING}"

if [ "$1" == "NEO_CLUSTER" ]; then
    JAVA_OPTS="${JAVA_OPTS} -Dneo_cluster_mode=true"
fi

export JAVA_HOME
export JAVA_OPTS
export CATALINA_PID

unset DISPLAY

# Remove atomikos tm files
rm ${DIST}/tomcat/logs/*.epoch 2>/dev/null
rm ${DIST}/tomcat/logs/tmlog.lck 2>/dev/null
rm ${DIST}/tomcat/logs/tmlog*.log 2>/dev/null
rm ${DIST}/tomcat/*.epoch 2>/dev/null
rm ${DIST}/tomcat/tmlog.lck 2>/dev/null
rm ${DIST}/tomcat/tmlog*.log 2>/dev/null

EXECUTABLE=catalina.sh

# Check that target executable exists
if $os400; then
  # -x will Only work on the os400 if the files are:
  # 1. owned by the user
  # 2. owned by the PRIMARY group of the user
  # this will not work if the user belongs in secondary groups
  eval
else
  if [ ! -x "$BASE"/"$EXECUTABLE" ]; then
    echo "Cannot find $BASE/$EXECUTABLE"
    echo "This file is needed to run this program"
    exit 1
  fi
fi

"$BASE"/"$EXECUTABLE" start "$@"

PROCESS=`cat ${CATALINA_PID}`

echo "Started RSView pid: ${PROCESS}"
