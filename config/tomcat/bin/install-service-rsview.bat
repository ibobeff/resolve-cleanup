@echo off

pushd %~dp0\..
set CATALINA_HOME=%cd%
set CATALINA_BASE=%CATALINA_HOME%
set EXECUTABLE=%CATALINA_HOME%\bin\tomcat8.exe

rem Set default Service name
set SERVICE_NAME=RSView
set PR_DISPLAYNAME=RSView
set DIST=%CATALINA_HOME%\..
call "%DIST%\tomcat\bin\rsview-env.bat"
set JRE=%RSVIEWJDK%


if exist "%JRE%" (
	goto jre_exists
) else (
	echo given java_home folder does not exist using default resolve java
	set JRE=%DIST%\jdk
	goto finish_run
)

:jre_exists
	for /f "tokens=2-5 delims=.-_" %%j in ('"%JRE%\bin\java" -fullversion 2^>^&1') do (
			set MAJORVERSIONNEW=%%j 
			set LESSERVERSIONNEW=%%l
		)
	for /f "tokens=2-5 delims=.-_" %%j in ('"%DIST%\jdk\bin\java" -fullversion 2^>^&1') do (
			set MAJORVERSION=%%j 
			set LESSERVERSION=%%l
		)
	if 0%MAJORVERSIONNEW% NEQ 0%MAJORVERSION% (
		echo given java version is not java version 8 using default resolve java
		set JRE=%DIST%\jdk
	)
	if 0%LESSERVERSIONNEW% LSS 0%LESSERVERSION% (
		echo given java version is less than resolve's installed java version using default resolve java
		set JRE=%DIST%\jdk
	)
	goto finish_run
:finish_run

set JAVA_OPTS=
IF DEFINED JAVA_OPTS set JAVA_OPTS=%JAVA_OPTS%#
set JAVA_OPTS=%JAVA_OPTS%-Dorg.owasp.esapi.resources=%DIST%\tomcat\webapps\resolve\WEB-INF
rem Disable JDBC 4.1 auto-commit spec compliance property to for latest Oracle JDBC Driver
set JAVA_OPTS=%JAVA_OPTS%-Doracle.jdbc.autoCommitSpecCompliant=false

rem Use the environment variables as an example - each command line option is prefixed with PR_
set PR_DESCRIPTION=RSView Tomcat Service - http://www.resolve-systems.com
set PR_INSTALL=%EXECUTABLE%
set PR_LOGPATH=%CATALINA_HOME%\logs

set PR_CLASSPATH=%JRE%\lib\tools.jar;%CATALINA_HOME%\bin\bootstrap.jar;%CATALINA_HOME%\..\gatewaylibs\*;%CATALINA_HOME%\lib\*;%CATALINA_HOME%\webapps\resolve\WEB-INF\lib\*;%CATALINA_HOME%\..\gatewaylibs\*;%CATALINA_HOME%\bin\tomcat-juli.jar
set PR_JVM=%JRE%\bin\server\jvm.dll

rem Install the service
echo Installing the service  %SERVICE_NAME%
echo Using CATALINA_HOME:    %CATALINA_HOME%
echo Using JAVA_HOME:        %JRE%

rem Setting service parameters needs to be done in separate steps
"%EXECUTABLE%" //IS//%SERVICE_NAME% --StartClass org.apache.catalina.startup.Bootstrap --StopClass org.apache.catalina.startup.Bootstrap --StartParams start --StopParams stop --JvmMs 256 --JvmMx 1024 --JvmSs 256

rem clear environment variables
set PR_DISPLAYNAME=
set PR_DESCRIPTION=
set PR_INSTALL=
set PR_LOGPATH=
set PR_CLASSPATH=
set PR_JVM=

rem set service parameters
set PR_STDOUTPUT=%CATALINA_HOME%\logs\stdout.log
set PR_STDERROR=%CATALINA_HOME%\logs\stderr.log
set PR_STARTUP=auto
set PR_JVMOPTIONS=
set PR_KEYSTORE=
rem set PR_KEYSTORE=-Djavax.net.ssl.trustStore=%CATALINA_HOME%\conf\.keystore;

"%EXECUTABLE%" //US//%SERVICE_NAME% --JvmOptions "%JAVA_OPTS%#-Xms256M#-Xmx1024M#-Xss256K#-XX:MaxPermSize=512M#-XX:+CMSClassUnloadingEnabled#-XX:+UseConcMarkSweepGC#-XX:+HeapDumpOnOutOfMemoryError#%PR_KEYSTORE%-Dcatalina.base=%CATALINA_BASE%#-Dcatalina.home=%CATALINA_HOME%#-Djava.endorsed.dirs=%CATALINA_HOME%\endorsed#-Dfile.encoding=iso-8859-1#-Djava.io.tmpdir=%CATALINA_BASE%\temp" --StartMode jvm --StopMode jvm

echo The service '%SERVICE_NAME%' has been installed.
popd

set JAVA_OPTS=
set SERVICE_NAME=
set EXECUTABLE=
set CATALINA_BASE=
set CATALINA_HOME=
set PR_STDOUTPUT=
set PR_STDERROR=
set PR_STARTUP=
set PR_JVMOPTIONS=
set PR_KEYSTORE=
