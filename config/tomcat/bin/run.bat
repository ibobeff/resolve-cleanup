@echo off
Setlocal enabledelayedexpansion
rem resolve java path
set DIST=%~dp0\..\..
call "%DIST%\tomcat\bin\rsview-env.bat"
set JRE=%RSVIEWJDK%
set JAVA_HOME=%RSVIEWJDK%

if exist "%JRE%" (
	goto jre_exists
) else (
	echo given java_home folder does not exist using default resolve java
	set JRE=%DIST%\jdk
	set JAVA_HOME=%DIST%\jdk
	goto finish_run
)

:jre_exists
	for /f "tokens=2-5 delims=.-_" %%j in ('"%JRE%\bin\java" -fullversion 2^>^&1') do (
			set MAJORVERSIONNEW=%%j 
			set LESSERVERSIONNEW=%%l
		)
	for /f "tokens=2-5 delims=.-_" %%j in ('"%DIST%\jdk\bin\java" -fullversion 2^>^&1') do (
			set MAJORVERSION=%%j 
			set LESSERVERSION=%%l
		)
	if 0%MAJORVERSIONNEW% NEQ 0%MAJORVERSION% (
		echo given java version is not java version 8 using default resolve java
		set JRE=%DIST%\jdk
		set JAVA_HOME=%DIST%\jdk
	)
	if 0%LESSERVERSIONNEW% LSS 0%LESSERVERSION% (
		echo given java version is less than resolve's installed java version using default resolve java
		set JRE=%DIST%\jdk
		set JAVA_HOME=%DIST%\jdk
	)
	goto finish_run
:finish_run

set BASE="%DIST%/tomcat/bin"

rem JCONSOLE - make sure "hostname -i" resolves to an ipaddress not 127.0.0.1
rem set JCONSOLE=-Dcom.sun.management.jmxremote.port=5550 -Dcom.sun.management.jmxremote.authenticate=false -Dcom.sun.management.jmxremote.ssl=false

rem Eclipse Debugging
rem set DEBUGGING=-Xdebug -Xrunjdwp:transport=dt_socket,address=8003,server=y,suspend=n

rem GC Logging
rem set GCLOGGING=-XX:+PrintGCDetails -verbose:gc -Xloggc:"%DIST%/tomcat/logs/gc.log"

rem keystore for LDAPS
set KEYSTORE=
rem KEYSTORE=-Djavax.net.ssl.trustStore="%DIST%/tomcat/bin/.keystore"

set JAVA_OPTS=
set JAVA_OPTS=%JAVA_OPTS% -Xms256M -Xmx1024M -Xss256K -XX:MaxPermSize=512M -XX:+UseConcMarkSweepGC -XX:+CMSClassUnloadingEnabled -XX:+HeapDumpOnOutOfMemoryError

rem OWASP ESAPI
set JAVA_OPTS=%JAVA_OPTS% -Dorg.owasp.esapi.resources="%DIST%/tomcat/webapps/resolve/WEB-INF"

rem Disable JDBC 4.1 auto-commit spec compliance property for latest Oracle JDBC Driver
set JAVA_OPTS=%JAVA_OPTS% -Doracle.jdbc.autoCommitSpecCompliant=false

set JAVA_OPTS=%JAVA_OPTS% %KEYSTORE% %JCONSOLE% %DEBUGGING% %GCLOGGING%

IF NOT "%1" == "NEO_CLUSTER" GOTO :START
    set JAVA_OPTS=%JAVA_OPTS% -Dneo_cluster_mode=true

:START
pushd %DIST%\tomcat\bin
call startup
popd

set DIST=
set JRE=
set JAVA_HOME=
set BASE=
set JCONSOLE=
set DEBUGGING=
set GCLOGGING=
set KEYSTORE=
set JAVA_OPTS=
