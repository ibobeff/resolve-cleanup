#!/bin/bash
# -----------------------------------------------------------------------------
# Stop script for the CATALINA Server
#
# $Id: shutdown.sh 385888 2006-03-14 21:04:40Z keith $
# -----------------------------------------------------------------------------

# resolve java path
BIN=`echo $0 | awk '{ print substr( $0, 0, length($0)-7 ) }'`

cd ${BIN}../..

DIST="`pwd`"
CATALINA_PID=${DIST}/tomcat/bin/lock
JAVA_HOME="$DIST/jdk"
BASE="$DIST/tomcat/bin"
export JAVA_HOME

EXECUTABLE=catalina.sh

# Check that target executable exists
if [ ! -x "$BASE"/"$EXECUTABLE" ]; then
  echo "Cannot find $BASE/$EXECUTABLE"
  echo "This file is needed to run this program"
  exit 1
fi

"$BASE"/"$EXECUTABLE" stop "$@"

if [ ! -f ${CATALINA_PID} ]; then
    echo "Lock File Not Found..."
    exit 1
fi

LOCK=`cat ${CATALINA_PID}`

echo "Stopped RSView PID: ${LOCK}"

rm ${CATALINA_PID}

for i in 1 2 3 4 5
do
  PROCESS=`ps -ef | awk '/'${LOCK}'/ && !/awk/ {print $2}'`

  if [ "$PROCESS" = "" ]; then
    exit 0
  else
    sleep 1
  fi
done
kill ${PROCESS} 

for i in 1 2 3 4 5
do
  PROCESS=`ps -ef | awk '/'${LOCK}'/ && !/awk/ {print $2}'`

  if [ "$PROCESS" = "" ]; then
    exit 0
  else
    sleep 1
  fi
done
kill -2 ${PROCESS} > /dev/null 2>&1

for i in 1 2 3 4 5
do
  PROCESS=`ps -ef | awk '/'${LOCK}'/ && !/awk/ {print $2}'`

  if [ "$PROCESS" = "" ]; then
    exit 0
  else
    sleep 1
  fi
done
kill -9 ${PROCESS} > /dev/null 2>&1
