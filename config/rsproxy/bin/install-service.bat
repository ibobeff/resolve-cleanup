@echo off

pushd %~dp0..\..
set DIST=%CD%
call "%DIST%\rsproxy\bin\rsproxy-env.bat"
popd
set JRE=%RSPROXYJDK%\jre
set ENVPATH=%JRE%\bin

if EXIST "%JRE%" (
	goto jre_exists
) else (
	echo given java_home folder does not exist using default resolve java
	set JRE=%DIST%/jdk
	goto finish_run
)
:jre_exists
	for /f "tokens=2-5 delims=.-_" %%j in ('"%JRE%\bin\java" -fullversion 2^>^&1') do (
			set MAJORVERSIONNEW=%%j 
			set LESSERVERSIONNEW=%%l
		)
	for /f "tokens=2-5 delims=.-_" %%j in ('"%DIST%\jdk\bin\java" -fullversion 2^>^&1') do (
			set MAJORVERSION=%%j 
			set LESSERVERSION=%%l
		)
	if 0%MAJORVERSIONNEW% NEQ 0%MAJORVERSION% (
		echo given java version is java version 8 using default resolve java
		set JRE=%DIST%/jdk
	)
	if 0%LESSERVERSIONNEW% LSS 0%LESSERVERSION% (
		echo given java version is less than resolve's installed java version using default resolve java
		set JRE=%DIST%/jdk
	)
	goto finish_run
:finish_run

set JAVA_LIB_PATH=%DIST%\lib

rem init JAVA_OPTS
set JAVA_OPTS=
IF DEFINED JAVA_OPTS set JAVA_OPTS=%JAVA_OPTS%;
set JAVA_OPTS=%JAVA_OPTS%-Dresolve.component.type=rsproxy

rem OWASP ESAPI
set JAVA_OPTS=%JAVA_OPTS%;-Dorg.owasp.esapi.resources=%DIST%\rsproxy\config

rem default classes
set CP=%DIST%/rsproxy/service/proxy
set CP=%CP%;%DIST%/rsproxy/lib/resolve-proxy.jar
set CP=%CP%;%DIST%/rsproxy/lib/commons-io.jar
set CP=%CP%;%DIST%/rsproxy/lib/commons-lang.jar
set CP=%CP%;%DIST%/rsproxy/lib/commons-lang3.jar
set CP=%CP%;%DIST%/rsproxy/lib/commons-logging.jar
set CP=%CP%;%DIST%/rsproxy/lib/log4j.jar
set CP=%CP%;%DIST%/rsproxy/lib/dom4j.jar
set CP=%CP%;%DIST%/rsproxy/lib/jaxen.jar
set CP=%CP%;%DIST%/rsproxy/lib/esapi.jar
set CP=%CP%;%DIST%/rsproxy/lib/joda-time.jar
set CP=%CP%;%DIST%/rsproxy/lib/org.apache.commons.fileupload.jar
set CP=%CP%;%DIST%/rsproxy/lib/servlet-api.jar

rem final JAVA_OPTS
set JAVA_OPTS=%JAVA_OPTS%;-Djava.library.path='%JAVA_LIB_PATH%'

set EXECUTABLE=%DIST%\rsproxy\bin\rsproxy.exe
set SERVICE_NAME=RSProxy
set PR_DISPLAYNAME=RSProxy
set PR_DESCRIPTION=RSProxy
set PR_INSTALL=%EXECUTABLE%
set PR_LOGPATH=%DIST%\rsproxy\log
set PR_LOGPREFIX=service
set PR_CLASSPATH=%CP%
set PR_JVM=%JRE%\bin\server\jvm.dll
set PR_STARTUP=auto
set PR_STARTCLASS=com.resolve.rsproxy.Main
set PR_STARTMETHOD=main
set PR_STARTPATH=%DIST%
set PR_STARTMODE=jvm
set PR_STOPCLASS=java.lang.System
set PR_STOPMETHOD=exit
set PR_STOPPATH=%DIST%
set PR_STOPMODE=jvm
set PR_STOPTIMEOUT=30
set PR_STARTPARAMS=

"%EXECUTABLE%" //IS//%SERVICE_NAME% ++StartParams=rsproxy


set PR_STDOUTPUT=%DIST%\rsproxy\log\stdout
set PR_STDERROR=%DIST%\rsproxy\log\stderr
set PR_JVMMS=64
set PR_JVMMX=512
set PR_JVMSS=256
"%EXECUTABLE%" //US//%SERVICE_NAME% --JvmOptions="%JAVA_OPTS%;-XX:+HeapDumpOnOutOfMemoryError"

rem cleanup
set SERVICE_NAME=
set PR_DISPLAYNAME=
set PR_DESCRIPTION=
set PR_INSTALL=
set PR_LOGPATH=
set PR_LOGPREFIX=
set PR_CLASSPATH=
set PR_JVM=
set PR_STARTUP=
set PR_STARTCLASS=
set PR_STARTMETHOD=
set PR_STARTPATH=
set PR_STARTMODE=
set PR_STOPCLASS=
set PR_STOPMETHOD=
set PR_STOPPATH=
set PR_STOPMODE=
set PR_STOPTIMEOUT=
set PR_STARTPARAMS=
set PR_STDOUTPUT=
set PR_STDERROR=
set PR_JVMMS=
set PR_JVMMX=
set PR_JVMSS=
set JAVA_OPTS=

echo The 'rsproxy' service has been installed.

