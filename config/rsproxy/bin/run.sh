#!/bin/bash

# properties
INSTANCE_NAME=rsproxy
BIN=`echo $0 | awk '{ print substr( $0, 0, length($0)-6 ) }'`

cd ${BIN}../..
DIST=`pwd`
. ${DIST}/rsproxy/bin/rsproxy-env.sh
JDK=${RSPROXYJDK}
if [ -d "${JDK}" ];
then
	JAVA_RESOLVEMAJOR=`${DIST}/jdk/bin/java -version 2>&1 | awk 'NR==1{split($0,a,"."); print a[2] }'`
	JAVA_RESOLVELESSER=`${DIST}/jdk/bin/java -version 2>&1 | awk  'NR==1{gsub(/"/,"");split($0,a,"_"); print a[2] }'`
	JAVA_BLUEPRINTMAJOR=`${JDK}/bin/java -version 2>&1 | awk 'NR==1{split($0,a,"."); print a[2] }'`
	JAVA_BLUEPRINTLESSER=`${JDK}/bin/java -version 2>&1 | awk  'NR==1{gsub(/"/,"");split($0,a,"_"); print a[2] }'`
	if [ ${JAVA_RESOLVEMAJOR} -ne ${JAVA_BLUEPRINTMAJOR} ]; then
			echo "supplied java version is different than the required version, using default resolve java"
			JDK=${DIST}/jdk
	fi

	if [ ${JAVA_RESOLVELESSER} -gt ${JAVA_BLUEPRINTLESSER} ]; then
			echo "supplied java version is less than required, using default resolve java"
			JDK=${DIST}/jdk
	fi
else
	echo given java_home folder does not exist using default resolve java
	JDK=${DIST}/jdk
fi
LOCKFILE=${DIST}/${INSTANCE_NAME}/bin/lock
ENVPATH=${JDK}/bin
JAVA_LIB_PATH=${DIST}/lib

if [ $# -le 1 ]; then
    if [ -f ${LOCKFILE} ]; then
        if [ $# -eq 0 ] || [ "$1" != "1" ]; then
            echo "Lock File Detected"
            echo "Cancelling ${INSTANCE_NAME} Startup"
            exit 1
        fi
    fi
fi

# set os type
linux=false
sunos=false
case "`uname`" in
Linux*) linux=true;;
SunOS*) sunos=true;;
esac

JAVA_OPTS=

# OWASP ESAPI
JAVA_OPTS="${JAVA_OPTS} -Dorg.owasp.esapi.resources=${DIST}/${INSTANCE_NAME}/config"

# execute
if $linux; then
    JAVA_EXEC=${JDK}/bin/java
else
    JAVA_EXEC=${JDK}/bin/sparcv9/java
    JAVA_OPTS="${JAVA_OPTS} -d64"
fi

# default classes
CP="${DIST}/${INSTANCE_NAME}/service/proxy"
CP="${CP}:${DIST}/${INSTANCE_NAME}/lib/resolve-proxy.jar"
# CP="${CP}:${DIST}/${INSTANCE_NAME}/lib/commons-io.jar"
# CP="${CP}:${DIST}/${INSTANCE_NAME}/lib/commons-lang.jar"
# CP="${CP}:${DIST}/${INSTANCE_NAME}/lib/commons-lang3.jar"
# CP="${CP}:${DIST}/${INSTANCE_NAME}/lib/commons-logging.jar"
# CP="${CP}:${DIST}/${INSTANCE_NAME}/lib/log4j.jar"
# CP="${CP}:${DIST}/${INSTANCE_NAME}/lib/dom4j.jar"
# CP="${CP}:${DIST}/${INSTANCE_NAME}/lib/jaxen.jar"
# CP="${CP}:${DIST}/${INSTANCE_NAME}/lib/esapi.jar"
# CP="${CP}:${DIST}/${INSTANCE_NAME}/lib/joda-time.jar"
# CP="${CP}:${DIST}/${INSTANCE_NAME}/lib/org.apache.commons.fileupload.jar"
# CP="${CP}:${DIST}/${INSTANCE_NAME}/lib/servlet-api.jar"

# JCONSOLE - make sure "hostname -i" resolves to an ipaddress not 127.0.0.1
#JCONSOLE="-Dcom.sun.management.jmxremote.port=5553 -Dcom.sun.management.jmxremote.authenticate=false -Dcom.sun.management.jmxremote.ssl=false"

# Eclipse Debugging
#DEBUGGING="-Xdebug -Xrunjdwp:transport=dt_socket,address=8004,server=y,suspend=n"

# GC Logging
#GCLOGGING="-XX:+PrintGCDetails -verbose:gc -Xloggc:${DIST}/rsproxy/log/gc.log"


PATH=${JDK}/bin:${PATH} export PATH

if [ $# -le 1 ]; then
    exec "${JAVA_EXEC}" -Djava.io.tmpdir=${DIST}/tmp ${JAVA_OPTS} ${JCONSOLE} ${DEBUGGING} ${GCLOGGING} -Djava.library.path="${JAVA_LIB_PATH}" -Dorg.apache.cxf.Logger=org.apache.cxf.common.logging.Log4jLogger -server -Xms64M -Xmx512M -Xss256K -XX:+UseConcMarkSweepGC -XX:+HeapDumpOnOutOfMemoryError -cp "${CP}" com.resolve.rsproxy.Main ${INSTANCE_NAME} >> ${DIST}/${INSTANCE_NAME}/log/stdout 2>&1 &

    PID=$!
    echo "${PID}" > ${LOCKFILE}
    echo "Started ${INSTANCE_NAME} pid: ${PID}"
else
    "${JAVA_EXEC}" -Djava.io.tmpdir=${DIST}/tmp ${JAVA_OPTS} ${JCONSOLE} ${DEBUGGING} ${GCLOGGING} -Djava.library.path="${JAVA_LIB_PATH}" -Dorg.apache.cxf.Logger=org.apache.cxf.common.logging.Log4jLogger -cp "${CP}" com.resolve.rsproxy.Main ${INSTANCE_NAME} $@
fi
