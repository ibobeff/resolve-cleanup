@echo off

pushd %~dp0\..\..
set DIST=%CD%
call "%DIST%\rsproxy\bin\rsproxy-env.bat"
set JRE=%RSPROXY%\jre
if EXIST "%JRE%" (
	goto jre_exists
) else (
	echo given java_home folder does not exist using default resolve java
	set JRE=%DIST%\jdk
	goto finish_run
)
:jre_exists
	for /f "tokens=2-5 delims=.-_" %%j in ('"%JRE%\bin\java" -fullversion 2^>^&1') do (
			set MAJORVERSIONNEW=%%j 
			set LESSERVERSIONNEW=%%l
		)
	for /f "tokens=2-5 delims=.-_" %%j in ('"%DIST%\jdk\bin\java" -fullversion 2^>^&1') do (
			set MAJORVERSION=%%j 
			set LESSERVERSION=%%l
		)
	if 0%MAJORVERSIONNEW% NEQ 0%MAJORVERSION% (
		echo given java version is java version 8 using default resolve java
		set JRE=%DIST%\jdk
	)
	if 0%LESSERVERSIONNEW% LSS 0%LESSERVERSION% (
		echo given java version is less than resolve's installed java version using default resolve java
		set JRE=%DIST%\jdk
	)
	goto finish_run
:finish_run
set ENVPATH=%JRE%/bin
set JAVA_LIB_PATH=%DIST%/lib

rem execute
set JAVA_OPTS=
set JAVA_OPTS=%JAVA_OPTS% -Djava.library.path="%JAVA_LIB_PATH%"
set JAVA_OPTS=%JAVA_OPTS% -Dorg.apache.cxf.Logger=org.apache.cxf.common.logging.Log4jLogger

rem OWASP ESAPI
set JAVA_OPTS=%JAVA_OPTS% -Dorg.owasp.esapi.resources="%DIST%/rsproxy/config"

rem default classes
set CP=%DIST%/rsproxy/service/proxy
set CP=%CP%;%DIST%/rsproxy/lib/resolve-proxy.jar
rem set CP=%CP%;%DIST%/rsproxy/lib/commons-io.jar
rem set CP=%CP%;%DIST%/rsproxy/lib/commons-lang.jar
rem set CP=%CP%;%DIST%/rsproxy/lib/commons-lang3.jar
rem set CP=%CP%;%DIST%/rsproxy/lib/commons-logging.jar
rem set CP=%CP%;%DIST%/rsproxy/lib/log4j.jar
rem set CP=%CP%;%DIST%/rsproxy/lib/dom4j.jar
rem set CP=%CP%;%DIST%/rsproxy/lib/jaxen.jar
rem set CP=%CP%;%DIST%/rsproxy/lib/esapi.jar
rem set CP=%CP%;%DIST%/rsproxy/lib/joda-time.jar
rem set CP=%CP%;%DIST%/rsproxy/lib/org.apache.commons.fileupload.jar
rem set CP=%CP%;%DIST%/rsproxy/lib/servlet-api.jar

rem JCONSOLE - make sure "hostname -i" resolves to an ipaddress not 127.0.0.1
rem set JCONSOLE=-Dcom.sun.management.jmxremote.port=5551 -Dcom.sun.management.jmxremote.authenticate=false -Dcom.sun.management.jmxremote.ssl=false

rem Eclipse Debugging
rem set DEBUGGING=-Xdebug -Xrunjdwp:transport=dt_socket,address=8004,server=y,suspend=n

rem GC Logging
rem set GCLOGGING=-XX:+PrintGCDetails -verbose:gc -Xloggc:${DIST}/rsproxy/log/gc.log

rem execute
cd %DIST%

set PATH=%PATH%;%ENVPATH%
"%JRE%\bin\java" %JAVA_OPTS% %JCONSOLE% %DEBUGGING% %GCLOGGING% -server -Xms64M -Xmx512M -Xss256K -XX:+UseConcMarkSweepGC -XX:+HeapDumpOnOutOfMemoryError -cp "%CP%" com.resolve.rsproxy.Main rsproxy %1 %2 %3 %4 %5 %6 %7 %8 %9

popd

set DIST=
set JRE=
set ENVPATH=
set JAVA_LIB_PATH=
set JAVA_OPTS=
set JCONSOLE=
set DEBUGGING=
set GCLOGGING=
