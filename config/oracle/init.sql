CREATE TABLE resolve_sequence_generator
(
   sys_id varchar2(32),
   u_name varchar2(100) DEFAULT NULL,
   u_number number(11) DEFAULT NULL,
   PRIMARY KEY (sys_id)
 );
 
CREATE TABLE resolve_event
(
   u_value VARCHAR2(1024) DEFAULT NULL,
   sys_id varchar2(32),
   sys_updated_by varchar2(255) DEFAULT NULL,
   sys_updated_on timestamp DEFAULT NULL,
   sys_created_by varchar2(255) DEFAULT NULL,
   sys_created_on timestamp DEFAULT NULL,
   sys_mod_count number(11) DEFAULT NULL,
   PRIMARY KEY (sys_id) 
);

/*----------------------------------------------------------------------------------------------*/
/* Following are the partitioned archive tables for effecient management.						*/
/* It uses a virtual column to partition the data which is a Oracle 11g feature.                */
/*                                                                                              */ 
/* In Oracle 11g there is a TRUNCATE PARTITION command as follows:								*/
/*                                                                                              */ 
/* ALTER TABLE <TABLE_NAME> TRUNCATE PARTITON <PARTITION_NAME>									*/
/*                                                                                              */ 
/* Example:																						*/
/*                                                                                              */ 
/* ALTER TABLE archive_action_result TRUNCATE PARTITON P_JAN									*/
/*                                                                                              */ 
/* IMPORTANT NOTE: This is only for Oracle 11g Database, any version older than that (e.g., 10g)*/
/* will not work. Although it'll error out on older databases these tables will be created from	*/
/* another section of the system but there won't be any partition which is ok.					*/
/*----------------------------------------------------------------------------------------------*/

create table archive_action_result 
(
	sys_id varchar2(32) not null,
	u_address varchar2(100),
	u_completion varchar2(40),
	u_condition varchar2(40),
	u_duration number(10,0),
	u_esbaddr varchar2(100),
	u_severity varchar2(40),
	u_timestamp number(19,0),
	sys_created_by varchar2(255),
	sys_created_on timestamp,
	sys_mod_count number(10,0),
	sys_updated_by varchar2(255),
	sys_updated_on timestamp,
	v_sys_created_month as (to_char(sys_created_on, 'MON')),
	u_action_result_lob varchar2(32),
	u_execute_request varchar2(32),
	u_execute_result varchar2(32),
	u_problem varchar2(32),
	u_process varchar2(32),
	u_target varchar2(100),
	u_target_guid varchar2(32),
	u_actiontask varchar2(32),
	primary key (sys_id)
)
PARTITION BY LIST (v_sys_created_month)
(
	PARTITION P_JAN VALUES ('JAN'),
	PARTITION P_FEB VALUES ('FEB'),
	PARTITION P_MAR VALUES ('MAR'),
	PARTITION P_APR VALUES ('APR'),
	PARTITION P_MAY VALUES ('MAY'),
	PARTITION P_JUN VALUES ('JUN'),
	PARTITION P_JUL VALUES ('JUL'),
	PARTITION P_AUG VALUES ('AUG'),
	PARTITION P_SEP VALUES ('SEP'),
	PARTITION P_OCT VALUES ('OCT'),
	PARTITION P_NOV VALUES ('NOV'),
	PARTITION P_DEC VALUES ('DEC')
);

create table archive_action_result_lob (
	sys_id varchar2(32 char) not null,
	u_detail clob,
	u_summary clob,
	sys_created_by varchar2(255 char),
	sys_created_on timestamp,
	sys_mod_count number(10,0),
	sys_updated_by varchar2(255 char),
	sys_updated_on timestamp,
	v_sys_created_month as (to_char(sys_created_on, 'MON')),
	primary key (sys_id)
)
PARTITION BY LIST (v_sys_created_month)
(
	PARTITION P_JAN VALUES ('JAN'),
	PARTITION P_FEB VALUES ('FEB'),
	PARTITION P_MAR VALUES ('MAR'),
	PARTITION P_APR VALUES ('APR'),
	PARTITION P_MAY VALUES ('MAY'),
	PARTITION P_JUN VALUES ('JUN'),
	PARTITION P_JUL VALUES ('JUL'),
	PARTITION P_AUG VALUES ('AUG'),
	PARTITION P_SEP VALUES ('SEP'),
	PARTITION P_OCT VALUES ('OCT'),
	PARTITION P_NOV VALUES ('NOV'),
	PARTITION P_DEC VALUES ('DEC')
);

create table archive_execute_dependency (
	sys_id varchar2(32 char) not null,
	u_completion number(1,0),
	u_condition varchar2(40 char),
	u_execute varchar2(32 char),
	u_expression varchar2(4000 char),
	u_merge varchar2(40 char),
	u_severity varchar2(40 char),
	u_type varchar2(40 char),
	sys_created_by varchar2(255 char),
	sys_created_on timestamp,
	sys_mod_count number(10,0),
	sys_updated_by varchar2(255 char),
	sys_updated_on timestamp,
	v_sys_created_month as (to_char(sys_created_on, 'MON')),
	u_execute_request varchar2(32 char),
	primary key (sys_id)
)
PARTITION BY LIST (v_sys_created_month)
(
	PARTITION P_JAN VALUES ('JAN'),
	PARTITION P_FEB VALUES ('FEB'),
	PARTITION P_MAR VALUES ('MAR'),
	PARTITION P_APR VALUES ('APR'),
	PARTITION P_MAY VALUES ('MAY'),
	PARTITION P_JUN VALUES ('JUN'),
	PARTITION P_JUL VALUES ('JUL'),
	PARTITION P_AUG VALUES ('AUG'),
	PARTITION P_SEP VALUES ('SEP'),
	PARTITION P_OCT VALUES ('OCT'),
	PARTITION P_NOV VALUES ('NOV'),
	PARTITION P_DEC VALUES ('DEC')
);

create table archive_execute_request (
	sys_id varchar2(32 char) not null,
	u_duration number(10,0),
	u_event_id varchar2(255 char),
	u_execute_status varchar2(40 char),
	u_node_id varchar2(255 char),
	u_node_label varchar2(255 char),
	u_number varchar2(40 char),
	u_trigger varchar2(100 char),
	u_wiki varchar2(255 char),
	sys_created_by varchar2(255 char),
	sys_created_on timestamp,
	sys_mod_count number(10,0),
	sys_updated_by varchar2(255 char),
	sys_updated_on timestamp,
	v_sys_created_month as (to_char(sys_created_on, 'MON')),
	u_problem varchar2(32 char),
	u_process varchar2(32 char),
	u_task varchar2(32 char),
	primary key (sys_id)
)
PARTITION BY LIST (v_sys_created_month)
(
	PARTITION P_JAN VALUES ('JAN'),
	PARTITION P_FEB VALUES ('FEB'),
	PARTITION P_MAR VALUES ('MAR'),
	PARTITION P_APR VALUES ('APR'),
	PARTITION P_MAY VALUES ('MAY'),
	PARTITION P_JUN VALUES ('JUN'),
	PARTITION P_JUL VALUES ('JUL'),
	PARTITION P_AUG VALUES ('AUG'),
	PARTITION P_SEP VALUES ('SEP'),
	PARTITION P_OCT VALUES ('OCT'),
	PARTITION P_NOV VALUES ('NOV'),
	PARTITION P_DEC VALUES ('DEC')
);

create table archive_execute_result (
	sys_id varchar2(32 char) not null,
	u_address varchar2(100 char),
	u_duration number(10,0),
	u_returncode number(10,0),
	u_target varchar2(32 char),
	sys_created_by varchar2(255 char),
	sys_created_on timestamp,
	sys_mod_count number(10,0),
	sys_updated_by varchar2(255 char),
	sys_updated_on timestamp,
	v_sys_created_month as (to_char(sys_created_on, 'MON')),
	u_request varchar2(32 char),
	u_execute_result_lob varchar2(32 char),
	primary key (sys_id)
)
PARTITION BY LIST (v_sys_created_month)
(
	PARTITION P_JAN VALUES ('JAN'),
	PARTITION P_FEB VALUES ('FEB'),
	PARTITION P_MAR VALUES ('MAR'),
	PARTITION P_APR VALUES ('APR'),
	PARTITION P_MAY VALUES ('MAY'),
	PARTITION P_JUN VALUES ('JUN'),
	PARTITION P_JUL VALUES ('JUL'),
	PARTITION P_AUG VALUES ('AUG'),
	PARTITION P_SEP VALUES ('SEP'),
	PARTITION P_OCT VALUES ('OCT'),
	PARTITION P_NOV VALUES ('NOV'),
	PARTITION P_DEC VALUES ('DEC')
);

create table archive_execute_result_lob (
	sys_id varchar2(32 char) not null,
	u_command clob,
	u_raw clob,
	sys_created_by varchar2(255 char),
	sys_created_on timestamp,
	sys_mod_count number(10,0),
	sys_updated_by varchar2(255 char),
	sys_updated_on timestamp,
	v_sys_created_month as (to_char(sys_created_on, 'MON')),
	primary key (sys_id)
)
PARTITION BY LIST (v_sys_created_month)
(
	PARTITION P_JAN VALUES ('JAN'),
	PARTITION P_FEB VALUES ('FEB'),
	PARTITION P_MAR VALUES ('MAR'),
	PARTITION P_APR VALUES ('APR'),
	PARTITION P_MAY VALUES ('MAY'),
	PARTITION P_JUN VALUES ('JUN'),
	PARTITION P_JUL VALUES ('JUL'),
	PARTITION P_AUG VALUES ('AUG'),
	PARTITION P_SEP VALUES ('SEP'),
	PARTITION P_OCT VALUES ('OCT'),
	PARTITION P_NOV VALUES ('NOV'),
	PARTITION P_DEC VALUES ('DEC')
);

create table archive_process_request (
	sys_id varchar2(32 char) not null,
	u_duration number(10,0),
	u_number varchar2(40 char),
	u_status varchar2(40 char),
	u_timeout varchar2(40 char),
	u_wiki varchar2(255 char),
	sys_created_by varchar2(255 char),
	sys_created_on timestamp,
	sys_mod_count number(10,0),
	sys_updated_by varchar2(255 char),
	sys_updated_on timestamp,
	v_sys_created_month as (to_char(sys_created_on, 'MON')),
	u_problem varchar2(32 char),
	primary key (sys_id)
)
PARTITION BY LIST (v_sys_created_month)
(
	PARTITION P_JAN VALUES ('JAN'),
	PARTITION P_FEB VALUES ('FEB'),
	PARTITION P_MAR VALUES ('MAR'),
	PARTITION P_APR VALUES ('APR'),
	PARTITION P_MAY VALUES ('MAY'),
	PARTITION P_JUN VALUES ('JUN'),
	PARTITION P_JUL VALUES ('JUL'),
	PARTITION P_AUG VALUES ('AUG'),
	PARTITION P_SEP VALUES ('SEP'),
	PARTITION P_OCT VALUES ('OCT'),
	PARTITION P_NOV VALUES ('NOV'),
	PARTITION P_DEC VALUES ('DEC')
);

create table archive_worksheet (
	sys_id varchar2(32 char) not null,
	u_alert_id varchar2(333 char),
	u_condition varchar2(40 char),
	u_correlation_id varchar2(333 char),
	u_debug clob,
	u_description clob,
	u_number varchar2(40 char),
	u_reference varchar2(333 char),
	u_severity varchar2(40 char),
	u_summary clob,
	u_work_notes clob,
	u_worksheet clob,
	sys_created_by varchar2(255 char),
	sys_created_on timestamp,
	sys_mod_count number(10,0),
	sys_updated_by varchar2(255 char),
	sys_updated_on timestamp,
	v_sys_created_month as (to_char(sys_created_on, 'MON')),
	u_assigned_to varchar2(255 char),
	primary key (sys_id)
)
PARTITION BY LIST (v_sys_created_month)
(
	PARTITION P_JAN VALUES ('JAN'),
	PARTITION P_FEB VALUES ('FEB'),
	PARTITION P_MAR VALUES ('MAR'),
	PARTITION P_APR VALUES ('APR'),
	PARTITION P_MAY VALUES ('MAY'),
	PARTITION P_JUN VALUES ('JUN'),
	PARTITION P_JUL VALUES ('JUL'),
	PARTITION P_AUG VALUES ('AUG'),
	PARTITION P_SEP VALUES ('SEP'),
	PARTITION P_OCT VALUES ('OCT'),
	PARTITION P_NOV VALUES ('NOV'),
	PARTITION P_DEC VALUES ('DEC')
);

create table archive_worksheet_debug (
	sys_id varchar2(32 char) not null,
	u_debug clob,
	u_timestamp number(19,0),
	sys_created_by varchar2(40 char),
	sys_created_on timestamp,
	sys_mod_count number(10,0),
	sys_updated_by varchar2(40 char),
	sys_updated_on timestamp,
	v_sys_created_month as (to_char(sys_created_on, 'MON')),
	primary key (sys_id)
)
PARTITION BY LIST (v_sys_created_month)
(
	PARTITION P_JAN VALUES ('JAN'),
	PARTITION P_FEB VALUES ('FEB'),
	PARTITION P_MAR VALUES ('MAR'),
	PARTITION P_APR VALUES ('APR'),
	PARTITION P_MAY VALUES ('MAY'),
	PARTITION P_JUN VALUES ('JUN'),
	PARTITION P_JUL VALUES ('JUL'),
	PARTITION P_AUG VALUES ('AUG'),
	PARTITION P_SEP VALUES ('SEP'),
	PARTITION P_OCT VALUES ('OCT'),
	PARTITION P_NOV VALUES ('NOV'),
	PARTITION P_DEC VALUES ('DEC')
);
