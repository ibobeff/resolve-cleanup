#!/bin/bash
DIST=`echo $0 | awk '{ print substr( $0, 0, length($0)-7 ) }'`

cd ${DIST}

if [ ! -f lock ]; then
    echo "Lock File Not Found...Exiting"
    exit 1
fi

LOCK=`cat lock`
rm lock

kill ${LOCK}
echo "Stopped RSMgmt PID: ${LOCK}"

for i in 1 2 3 4 5
do
  PROCESS=`ps -ef | awk '/'${LOCK}'/ && !/awk/ {print $2}'`

  if [ "$PROCESS" = "" ]; then
    exit 0
  else
    sleep 1
  fi
done
kill -2 ${LOCK} > /dev/null 2>&1

for i in 1 2 3 4 5
do
  PROCESS=`ps -ef | awk '/'${LOCK}'/ && !/awk/ {print $2}'`

  if [ "$PROCESS" = "" ]; then
    exit 0
  else
    sleep 1
  fi
done
kill -9 ${LOCK} > /dev/null 2>&1
