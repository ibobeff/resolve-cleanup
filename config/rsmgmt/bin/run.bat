@echo off
Setlocal EnableDelayedExpansion
pushd %~dp0\..\..
set DIST=%CD%
call "%DIST%\rsmgmt\bin\rsmgmt-env.bat"
set JRE=%RSMGMTJDK%

if EXIST "%JRE%" (
	goto jre_exists
) else (
	echo given java_home folder does not exist using default resolve java
	set JRE=%DIST%\jdk
	goto finish_run
)
:jre_exists
	for /f "tokens=2-5 delims=.-_" %%j in ('"%JRE%\bin\java" -fullversion 2^>^&1') do (
			set MAJORVERSIONNEW=%%j 
			set LESSERVERSIONNEW=%%l
		)
	for /f "tokens=2-5 delims=.-_" %%j in ('"%DIST%\jdk\bin\java" -fullversion 2^>^&1') do (
			set MAJORVERSION=%%j 
			set LESSERVERSION=%%l
		)
	if 0%MAJORVERSIONNEW% NEQ 0%MAJORVERSION% (
		echo given java version is not java version 8 using default resolve java
		set JRE=%DIST%\jdk
	)
	if 0%LESSERVERSIONNEW% LSS 0%LESSERVERSION% (
		echo given java version is less than resolve's installed java version using default resolve java
		set JRE=%DIST%\jdk
	)
	goto finish_run
:finish_run
set ENVPATH=%JRE%/bin
set JAVA_LIB_PATH=%DIST%/lib

rem execute
set JAVA_OPTS=
set JAVA_OPTS=%JAVA_OPTS% -Djava.library.path="%JAVA_LIB_PATH%"
set JAVA_OPTS=%JAVA_OPTS% -Dorg.apache.cxf.Logger=org.apache.cxf.common.logging.Log4jLogger

rem OWASP ESAPI
set JAVA_OPTS=%JAVA_OPTS% -Dorg.owasp.esapi.resources="%DIST%/rsmgmt/config"

rem default classes
set CP=%DIST%/rsmgmt/service/proxy
set CP=%CP%;%DIST%/rsmgmt/lib/*

set CP=%CP%;%DIST%/rsmgmt/lib/*
rem set CP=%CP%;%DIST%/lib/commons-io.jar
rem set CP=%CP%;%DIST%/lib/commons-lang.jar
rem set CP=%CP%;%DIST%/lib/commons-lang3.jar
rem set CP=%CP%;%DIST%/lib/commons-logging.jar
rem set CP=%CP%;%DIST%/lib/commons-collections.jar
rem set CP=%CP%;%DIST%/lib/commons-dbcp.jar
rem set CP=%CP%;%DIST%/lib/commons-pool.jar
rem set CP=%CP%;%DIST%/lib/commons-beanutils.jar
rem set CP=%CP%;%DIST%/lib/commons-codec.jar
rem set CP=%CP%;%DIST%/lib/log4j.jar
rem set CP=%CP%;%DIST%/lib/dom4j.jar
rem set CP=%CP%;%DIST%/lib/jaxen.jar
rem set CP=%CP%;%DIST%/lib/jms.jar
rem set CP=%CP%;%DIST%/lib/groovy.jar
rem set CP=%CP%;%DIST%/lib/concurrent.jar
rem set CP=%CP%;%DIST%/lib/options.jar
rem set CP=%CP%;%DIST%/lib/snmp4j.jar
rem set CP=%CP%;%DIST%/lib/ezmorph.jar
rem set CP=%CP%;%DIST%/lib/json-lib.jar
rem set CP=%CP%;%DIST%/lib/zookeeper.jar
rem set CP=%CP%;%DIST%/lib/opencsv.jar
rem set CP=%CP%;%DIST%/lib/poi.jar
rem set CP=%CP%;%DIST%/lib/poi-ooxml.jar
rem set CP=%CP%;%DIST%/lib/esapi.jar
rem set CP=%CP%;%DIST%/lib/joda-time.jar
rem set CP=%CP%;%DIST%/lib/jackson-core-asl.jar
rem set CP=%CP%;%DIST%/lib/jackson-core.jar
rem set CP=%CP%;%DIST%/lib/jackson-datatype-json-org.jar
rem set CP=%CP%;%DIST%/lib/jackson-mapper-asl.jar
rem set CP=%CP%;%DIST%/lib/guava.jar

rem rem RabbitMQ
rem set CP=%CP%;%DIST%/lib/rabbitmq-client.jar

rem rem ElasticSearch
rem set CP=%CP%;%DIST%/lib/antlr-runtime.jar
rem set CP=%CP%;%DIST%/lib/apache-log4j-extras.jar
rem set CP=%CP%;%DIST%/lib/asm.jar
rem set CP=%CP%;%DIST%/lib/asm-commons.jar
rem set CP=%CP%;%DIST%/lib/elasticsearch.jar
rem rem set CP=%CP%;%DIST%/lib/groovy-all.jar
rem set CP=%CP%;%DIST%/lib/jna.jar
rem set CP=%CP%;%DIST%/lib/jts.jar
rem set CP=%CP%;%DIST%/lib/lucene-analyzers-common.jar
rem rem set CP=%CP%;%DIST%/lib/lucene-codecs.jar
rem set CP=%CP%;%DIST%/lib/lucene-core.jar
rem rem set CP=%CP%;%DIST%/lib/lucene-expressions.jar
rem set CP=%CP%;%DIST%/lib/lucene-grouping.jar
rem set CP=%CP%;%DIST%/lib/lucene-highlighter.jar
rem set CP=%CP%;%DIST%/lib/lucene-join.jar
rem set CP=%CP%;%DIST%/lib/lucene-memory.jar
rem set CP=%CP%;%DIST%/lib/lucene-misc.jar
rem set CP=%CP%;%DIST%/lib/lucene-queries.jar
rem set CP=%CP%;%DIST%/lib/lucene-queryparser.jar
rem set CP=%CP%;%DIST%/lib/lucene-sandbox.jar
rem set CP=%CP%;%DIST%/lib/lucene-spatial.jar
rem set CP=%CP%;%DIST%/lib/lucene-suggest.jar
rem set CP=%CP%;%DIST%/lib/spatial4j.jar
rem set CP=%CP%;%DIST%/lib/tika.jar
rem set CP=%CP%;%DIST%/lib/jsoup.jar

rem set CP=%CP%;%DIST%/lib/netty.jar
rem set CP=%CP%;%DIST%/lib/transport.jar
rem set CP=%CP%;%DIST%/lib/jackson-dataformat-cbor.jar
rem set CP=%CP%;%DIST%/lib/jackson-dataformat-yaml.jar
rem set CP=%CP%;%DIST%/lib/jackson-dataformat-smile.jar
rem set CP=%CP%;%DIST%/lib/reindex-client.jar
rem set CP=%CP%;%DIST%/lib/lang-mustache-client.jar
rem set CP=%CP%;%DIST%/lib/percolator-client.jar
rem set CP=%CP%;%DIST%/lib/transport-netty3-client.jar
rem set CP=%CP%;%DIST%/lib/transport-netty4-client.jar
rem set CP=%CP%;%DIST%/lib/HdrHistogram.jar
rem set CP=%CP%;%DIST%/lib/hppc.jar
rem set CP=%CP%;%DIST%/lib/lucene-backward-codecs.jar
rem set CP=%CP%;%DIST%/lib/lucene-spatial3d.jar
rem set CP=%CP%;%DIST%/lib/lucene-spatial-extras.jar
rem set CP=%CP%;%DIST%/lib/netty-buffer.jar
rem set CP=%CP%;%DIST%/lib/netty-codec.jar
rem set CP=%CP%;%DIST%/lib/netty-codec-http.jar
rem set CP=%CP%;%DIST%/lib/netty-common.jar
rem set CP=%CP%;%DIST%/lib/netty-handler.jar
rem set CP=%CP%;%DIST%/lib/netty-resolver.jar
rem set CP=%CP%;%DIST%/lib/netty-transport.jar
rem set CP=%CP%;%DIST%/lib/t-digest.jar
rem set CP=%CP%;%DIST%/lib/log4j-api.jar
rem set CP=%CP%;%DIST%/lib/log4j-core.jar
rem rem set CP=%CP%;%DIST%/lib/log4j-to-slf4j.jar
rem set CP=%CP%;%DIST%/lib/slf4j-api.jar

rem rem connectors
rem set CP=%CP%;%DIST%/lib/ganymed-ssh2.jar
rem set CP=%CP%;%DIST%/lib/commons-httpclient.jar
rem set CP=%CP%;%DIST%/lib/httpcore.jar
rem set CP=%CP%;%DIST%/lib/httpclient.jar
rem set CP=%CP%;%DIST%/lib/httpmime.jar
rem set CP=%CP%;%DIST%/lib/httpclient-win.jar
rem set CP=%CP%;%DIST%/lib/fluent-hc.jar
rem set CP=%CP%;%DIST%/lib/jna.jar
rem set CP=%CP%;%DIST%/lib/jna-platform.jar
rem set CP=%CP%;%DIST%/lib/httpclient-cache.jar
rem set CP=%CP%;%DIST%/lib/commons-codec.jar
rem set CP=%CP%;%DIST%/lib/commons-net.jar
rem set CP=%CP%;%DIST%/lib/mariadb-java-client.jar
rem set CP=%CP%;%DIST%/lib/ojdbc8.jar
rem set CP=%CP%;%DIST%/lib/xdb6.jar
rem set CP=%CP%;%DIST%/lib/mail.jar
rem set CP=%CP%;%DIST%/lib/activation.jar
rem rem set CP=%CP%;%DIST%/lib/groovyws.jar

rem rem REST web service (RESTLet 2.1.2) with embedded Jetty 7 (for SSL support)
rem set CP=%CP%;%DIST%/lib/org.restlet.ext.fileupload.jar
rem set CP=%CP%;%DIST%/lib/org.restlet.ext.jetty.jar
rem set CP=%CP%;%DIST%/lib/org.restlet.ext.ssl.jar
rem set CP=%CP%;%DIST%/lib/org.restlet.jar
rem set CP=%CP%;%DIST%/lib/org.jsslutils.jar
rem set CP=%CP%;%DIST%/lib/jetty-ajp.jar
rem set CP=%CP%;%DIST%/lib/jetty-continuation.jar
rem set CP=%CP%;%DIST%/lib/jetty-http.jar
rem set CP=%CP%;%DIST%/lib/jetty-io.jar
rem set CP=%CP%;%DIST%/lib/jetty-security.jar
rem set CP=%CP%;%DIST%/lib/jetty-server.jar
rem set CP=%CP%;%DIST%/lib/jetty-servlet.jar
rem set CP=%CP%;%DIST%/lib/jetty-servlets.jar
rem set CP=%CP%;%DIST%/lib/jetty-util.jar
rem set CP=%CP%;%DIST%/lib/jetty-webapp.jar
rem set CP=%CP%;%DIST%/lib/org.apache.commons.fileupload.jar
rem set CP=%CP%;%DIST%/lib/servlet-api.jar

rem rem JSQL Parser
rem set CP=%CP%;%DIST%/lib/jsqlparser.jar

rem RSProperties2Yaml
set CP=%CP%;%DIST%/lib/RSProperties2Yaml-1.0.0-RELEASE-jar-with-dependencies.jar

rem JCONSOLE - make sure "hostname -i" resolves to an ipaddress not 127.0.0.1
rem set JCONSOLE=-Dcom.sun.management.jmxremote.port=5551 -Dcom.sun.management.jmxremote.authenticate=false -Dcom.sun.management.jmxremote.ssl=false

rem Eclipse Debugging
rem set DEBUGGING=-Xdebug -Xrunjdwp:transport=dt_socket,address=8000,server=y,suspend=n

rem GC Logging
rem set GCLOGGING=-XX:+PrintGCDetails -verbose:gc -Xloggc:${DIST}/rscontrol/log/gc.log

rem execute
cd %DIST%

set PATH=%PATH%;%ENVPATH%
"%JRE%\bin\java" %JAVA_OPTS% %JCONSOLE% %DEBUGGING% %GCLOGGING% -server -Xms64M -Xmx512M -Xss256K -XX:+UseConcMarkSweepGC -XX:+HeapDumpOnOutOfMemoryError -cp "%CP%" com.resolve.rsmgmt.Main rsmgmt %1 %2 %3 %4 %5 %6 %7 %8 %9

popd

set DIST=
set JRE=
set ENVPATH=
set JAVA_LIB_PATH=
set JAVA_OPTS=
set JCONSOLE=
set DEBUGGING=
set GCLOGGING=
IF %ERRORLEVEL% EQU -1 (
	echo error>%TEMP%\error.txt
) ELSE (
	echo pass>%TEMP%\error.txt
)
