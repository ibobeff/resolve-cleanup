#!/bin/bash

# properties
INSTANCE_NAME=rsmgmt
BIN=`echo $0 | awk '{ print substr( $0, 0, length($0)-6 ) }'`

cd ${BIN}../..
DIST=`pwd`
. ${DIST}/rsmgmt/bin/rsmgmt-env.sh
JDK=${RSMGMTJDK}
if [ -d "${JDK}" ];
then
	JAVA_RESOLVEMAJOR=`${DIST}/jdk/bin/java -version 2>&1 | awk 'NR==1{split($0,a,"."); print a[2] }'`
	JAVA_RESOLVELESSER=`${DIST}/jdk/bin/java -version 2>&1 | awk  'NR==1{gsub(/"/,"");split($0,a,"_"); print a[2] }'`
	JAVA_BLUEPRINTMAJOR=`${JDK}/bin/java -version 2>&1 | awk 'NR==1{split($0,a,"."); print a[2] }'`
	JAVA_BLUEPRINTLESSER=`${JDK}/bin/java -version 2>&1 | awk  'NR==1{gsub(/"/,"");split($0,a,"_"); print a[2] }'`
	if [ ${JAVA_RESOLVEMAJOR} -ne ${JAVA_BLUEPRINTMAJOR} ]; then
			echo "supplied java version is different than the required version, using default resolve java"
			JDK=${DIST}/jdk
	fi

	if [ ${JAVA_RESOLVELESSER} -gt ${JAVA_BLUEPRINTLESSER} ]; then
			echo "supplied java version is less than required using, default resolve java"
			JDK=${DIST}/jdk
	fi
else
	echo given java_home folder does not exist using default resolve java
	JDK=${DIST}/jdk
fi
LOCKFILE=${DIST}/${INSTANCE_NAME}/bin/lock
ENVPATH=${JDK}/bin
JAVA_LIB_PATH=${DIST}/lib

if [ $# -le 1 ]; then
    if [ -f ${LOCKFILE} ]; then
        if [ $# -eq 0 ] || [ "$1" != "1" ]; then
            echo "Lock File Detected"
            echo "Cancelling ${INSTANCE_NAME} Startup"
            exit 1
        fi
    fi
fi

# set os type
linux=false
sunos=false
case "`uname`" in
Linux*) linux=true;;
SunOS*) sunos=true;;
esac

JAVA_OPTS=

# OWASP ESAPI
JAVA_OPTS="${JAVA_OPTS} -Dorg.owasp.esapi.resources=${DIST}/${INSTANCE_NAME}/config"

# execute
if $linux; then
    JAVA_EXEC=${JDK}/bin/java
else
    JAVA_EXEC=${JDK}/bin/sparcv9/java
    JAVA_OPTS="${JAVA_OPTS} -d64"
fi

# default classes
CP="${DIST}/${INSTANCE_NAME}/service/proxy"
CP="${CP}:${DIST}/${INSTANCE_NAME}/lib/resolve-mgmt.jar"
CP="${CP}:${DIST}/${INSTANCE_NAME}/lib/*"
#CP="${CP}:${DIST}/lib/*"
CP="${CP}:${DIST}/${INSTANCE_NAME}/lib/*"
# CP="${CP}:${DIST}/lib/commons-io.jar"
# CP="${CP}:${DIST}/lib/commons-lang.jar"
# CP="${CP}:${DIST}/lib/commons-lang3.jar"
# CP="${CP}:${DIST}/lib/commons-logging.jar"
# CP="${CP}:${DIST}/lib/commons-collections.jar"
# CP="${CP}:${DIST}/lib/commons-dbcp.jar"
# CP="${CP}:${DIST}/lib/commons-pool.jar"
# CP="${CP}:${DIST}/lib/commons-beanutils.jar"
# CP="${CP}:${DIST}/lib/commons-codec.jar"
# CP="${CP}:${DIST}/lib/log4j.jar"
# CP="${CP}:${DIST}/lib/dom4j.jar"
# CP="${CP}:${DIST}/lib/jaxen.jar"
# CP="${CP}:${DIST}/lib/jms.jar"
# CP="${CP}:${DIST}/lib/groovy.jar"
# CP="${CP}:${DIST}/lib/concurrent.jar"
# CP="${CP}:${DIST}/lib/options.jar"
# CP="${CP}:${DIST}/lib/snmp4j.jar"
# CP="${CP}:${DIST}/lib/ezmorph.jar"
# CP="${CP}:${DIST}/lib/json-lib.jar"
# CP="${CP}:${DIST}/lib/zookeeper.jar"
# CP="${CP}:${DIST}/lib/opencsv.jar"
# CP="${CP}:${DIST}/lib/poi.jar"
# CP="${CP}:${DIST}/lib/poi-ooxml.jar"
# CP="${CP}:${DIST}/lib/esapi.jar"
# CP="${CP}:${DIST}/lib/joda-time.jar"
# CP="${CP}:${DIST}/lib/jackson-core-asl.jar"
# CP="${CP}:${DIST}/lib/jackson-core.jar"
# CP="${CP}:${DIST}/lib/jackson-datatype-json-org.jar"
# CP="${CP}:${DIST}/lib/jackson-mapper-asl.jar"
# CP="${CP}:${DIST}/lib/guava.jar"

# # RabbitMQ
# CP="${CP}:${DIST}/lib/rabbitmq-client.jar"

# # ElasticSearch
# CP="${CP}:${DIST}/lib/antlr-runtime.jar"
# CP="${CP}:${DIST}/lib/apache-log4j-extras.jar"
# CP="${CP}:${DIST}/lib/asm.jar"
# CP="${CP}:${DIST}/lib/asm-commons.jar"
# CP="${CP}:${DIST}/lib/elasticsearch.jar"
# # CP="${CP}:${DIST}/lib/groovy-all.jar"
# CP="${CP}:${DIST}/lib/jna.jar"
# CP="${CP}:${DIST}/lib/jts.jar"
# CP="${CP}:${DIST}/lib/lucene-analyzers-common.jar"
# # CP="${CP}:${DIST}/lib/lucene-codecs.jar"
# CP="${CP}:${DIST}/lib/lucene-core.jar"
# # CP="${CP}:${DIST}/lib/lucene-expressions.jar"
# CP="${CP}:${DIST}/lib/lucene-grouping.jar"
# CP="${CP}:${DIST}/lib/lucene-highlighter.jar"
# CP="${CP}:${DIST}/lib/lucene-join.jar"
# CP="${CP}:${DIST}/lib/lucene-memory.jar"
# CP="${CP}:${DIST}/lib/lucene-misc.jar"
# CP="${CP}:${DIST}/lib/lucene-queries.jar"
# CP="${CP}:${DIST}/lib/lucene-queryparser.jar"
# CP="${CP}:${DIST}/lib/lucene-sandbox.jar"
# CP="${CP}:${DIST}/lib/lucene-spatial.jar"
# CP="${CP}:${DIST}/lib/lucene-suggest.jar"
# CP="${CP}:${DIST}/lib/spatial4j.jar"
# CP="${CP}:${DIST}/lib/tika.jar"
# CP="${CP}:${DIST}/lib/jsoup.jar"

# CP="${CP}:${DIST}/lib/netty.jar"
# CP="${CP}:${DIST}/lib/transport.jar"
# CP="${CP}:${DIST}/lib/jackson-dataformat-cbor.jar"
# CP="${CP}:${DIST}/lib/jackson-dataformat-yaml.jar"
# CP="${CP}:${DIST}/lib/jackson-dataformat-smile.jar"
# CP="${CP}:${DIST}/lib/reindex-client.jar"
# CP="${CP}:${DIST}/lib/lang-mustache-client.jar"
# CP="${CP}:${DIST}/lib/percolator-client.jar"
# CP="${CP}:${DIST}/lib/transport-netty3-client.jar"
# CP="${CP}:${DIST}/lib/transport-netty4-client.jar"
# CP="${CP}:${DIST}/lib/HdrHistogram.jar"
# CP="${CP}:${DIST}/lib/hppc.jar"
# CP="${CP}:${DIST}/lib/lucene-backward-codecs.jar"
# CP="${CP}:${DIST}/lib/lucene-spatial3d.jar"
# CP="${CP}:${DIST}/lib/lucene-spatial-extras.jar"
# CP="${CP}:${DIST}/lib/netty-buffer.jar"
# CP="${CP}:${DIST}/lib/netty-codec.jar"
# CP="${CP}:${DIST}/lib/netty-codec-http.jar"
# CP="${CP}:${DIST}/lib/netty-common.jar"
# CP="${CP}:${DIST}/lib/netty-handler.jar"
# CP="${CP}:${DIST}/lib/netty-resolver.jar"
# CP="${CP}:${DIST}/lib/netty-transport.jar"
# CP="${CP}:${DIST}/lib/t-digest.jar"
# CP="${CP}:${DIST}/lib/log4j-api.jar"
# CP="${CP}:${DIST}/lib/log4j-core.jar"
# #CP="${CP}:${DIST}/lib/log4j-to-slf4j.jar"
# CP="${CP}:${DIST}/lib/slf4j-api.jar"

# # connectors
# CP="${CP}:${DIST}/lib/ganymed-ssh2.jar"
# CP="${CP}:${DIST}/lib/commons-httpclient.jar"
# CP="${CP}:${DIST}/lib/httpcore.jar"
# CP="${CP}:${DIST}/lib/httpclient.jar"
# CP="${CP}:${DIST}/lib/httpmime.jar"
# CP="${CP}:${DIST}/lib/httpclient-win.jar"
# CP="${CP}:${DIST}/lib/fluent-hc.jar"
# CP="${CP}:${DIST}/lib/jna.jar"
# CP="${CP}:${DIST}/lib/jna-platform.jar"
# CP="${CP}:${DIST}/lib/httpclient-cache.jar"
# CP="${CP}:${DIST}/lib/commons-codec.jar"
# CP="${CP}:${DIST}/lib/commons-net.jar"
# CP="${CP}:${DIST}/lib/mariadb-java-client.jar"
# CP="${CP}:${DIST}/lib/ojdbc8.jar"
# CP="${CP}:${DIST}/lib/xdb6.jar"
# CP="${CP}:${DIST}/lib/mail.jar"
# CP="${CP}:${DIST}/lib/activation.jar"
# #CP="${CP}:${DIST}/lib/groovyws.jar"

# # REST web service (RESTLet 2.1.2) with embedded Jetty 7 (for SSL support)
# CP="${CP}:${DIST}/lib/org.restlet.ext.fileupload.jar"
# CP="${CP}:${DIST}/lib/org.restlet.ext.jetty.jar"
# CP="${CP}:${DIST}/lib/org.restlet.ext.ssl.jar"
# CP="${CP}:${DIST}/lib/org.restlet.jar"
# CP="${CP}:${DIST}/lib/org.jsslutils.jar"
# CP="${CP}:${DIST}/lib/jetty-ajp.jar"
# CP="${CP}:${DIST}/lib/jetty-continuation.jar"
# CP="${CP}:${DIST}/lib/jetty-http.jar"
# CP="${CP}:${DIST}/lib/jetty-io.jar"
# CP="${CP}:${DIST}/lib/jetty-security.jar"
# CP="${CP}:${DIST}/lib/jetty-server.jar"
# CP="${CP}:${DIST}/lib/jetty-servlet.jar"
# CP="${CP}:${DIST}/lib/jetty-servlets.jar"
# CP="${CP}:${DIST}/lib/jetty-util.jar"
# CP="${CP}:${DIST}/lib/jetty-webapp.jar"
# CP="${CP}:${DIST}/lib/org.apache.commons.fileupload.jar"
# CP="${CP}:${DIST}/lib/servlet-api.jar"
# CP="${CP}:${DIST}/lib/search-guard-5-5.4.1-14.jar"
# CP="${CP}:${DIST}/lib/search-guard-ssl-5.4.1-22.jar"

# # JSQL Parser
# CP="${CP}:${DIST}/lib/jsqlparser.jar"

# RSProperties2Yaml
CP="${CP}:${DIST}/lib/RSProperties2Yaml-1.0.0-RELEASE-jar-with-dependencies.jar"

# JCONSOLE - make sure "hostname -i" resolves to an ipaddress not 127.0.0.1
#JCONSOLE="-Dcom.sun.management.jmxremote.port=5553 -Dcom.sun.management.jmxremote.authenticate=false -Dcom.sun.management.jmxremote.ssl=false"

# Eclipse Debugging
#DEBUGGING="-Xdebug -Xrunjdwp:transport=dt_socket,address=8000,server=y,suspend=n"

# GC Logging
#GCLOGGING="-XX:+PrintGCDetails -verbose:gc -Xloggc:${DIST}/rscontrol/log/gc.log"


PATH=${JDK}/bin:${PATH} export PATH

if [ $# -le 1 ]; then
    exec "${JAVA_EXEC}" -Djava.io.tmpdir=${DIST}/tmp ${JAVA_OPTS} ${JCONSOLE} ${DEBUGGING} ${GCLOGGING} -Djava.security.egd=file:///dev/urandom -Djava.library.path="${JAVA_LIB_PATH}" -Dorg.apache.cxf.Logger=org.apache.cxf.common.logging.Log4jLogger -server -Xms64M -Xmx512M -Xss256K -XX:+UseConcMarkSweepGC -XX:+HeapDumpOnOutOfMemoryError -cp "${CP}" com.resolve.rsmgmt.Main ${INSTANCE_NAME} >> ${DIST}/${INSTANCE_NAME}/log/stdout 2>&1 &

    PID=$!
    echo "${PID}" > ${LOCKFILE}
    echo "Started ${INSTANCE_NAME} pid: ${PID}"
else
    "${JAVA_EXEC}" -Djava.io.tmpdir=${DIST}/tmp ${JAVA_OPTS} ${JCONSOLE} ${DEBUGGING} ${GCLOGGING} -Djava.security.egd=file:///dev/urandom -Djava.library.path="${JAVA_LIB_PATH}" -Dorg.apache.cxf.Logger=org.apache.cxf.common.logging.Log4jLogger -cp "${CP}" com.resolve.rsmgmt.Main ${INSTANCE_NAME} $@
fi
