@echo off

pushd %~dp0..\..
set DIST=%CD%
call "%DIST%\rsmgmt\bin\rsmgmt-env.bat"
popd
set JRE=%RSMGMTJDK%

if EXIST "%JRE%" (
	goto jre_exists
) else (
	echo given java_home folder does not exist using default resolve java
	set JRE=%DIST%/jdk
	goto finish_run
)
:jre_exists
	for /f "tokens=2-5 delims=.-_" %%j in ('"%JRE%\bin\java" -fullversion 2^>^&1') do (
			set MAJORVERSIONNEW=%%j 
			set LESSERVERSIONNEW=%%l
		)
	for /f "tokens=2-5 delims=.-_" %%j in ('"%DIST%\jdk\bin\java" -fullversion 2^>^&1') do (
			set MAJORVERSION=%%j 
			set LESSERVERSION=%%l
		)
	if 0%MAJORVERSIONNEW% NEQ 0%MAJORVERSION% (
		echo given java version is not java version 8 using default resolve java
		set JRE=%DIST%/jdk
	)
	if 0%LESSERVERSIONNEW% LSS 0%LESSERVERSION% (
		echo given java version is less than resolve's installed java version using default resolve java
		set JRE=%DIST%/jdk
	)
	goto finish_run
:finish_run

set ENVPATH=%JRE%\bin
set JAVA_LIB_PATH=%DIST%\lib

rem init JAVA_OPTS
set JAVA_OPTS=
IF DEFINED JAVA_OPTS set JAVA_OPTS=%JAVA_OPTS%;
set JAVA_OPTS=%JAVA_OPTS%-Dresolve.component.type=rsmgmt

rem OWASP ESAPI
set JAVA_OPTS=%JAVA_OPTS%;-Dorg.owasp.esapi.resources=%DIST%\rsmgmt\config

rem default classes
set CP=%DIST%/rsmgmt/service/proxy
set CP=%CP%;%DIST%/rsmgmt/lib/resolve-mgmt.jar
set CP=%CP%;%DIST%/lib/commons-io.jar
set CP=%CP%;%DIST%/lib/commons-lang.jar
set CP=%CP%;%DIST%/lib/commons-lang3.jar
set CP=%CP%;%DIST%/lib/commons-logging.jar
set CP=%CP%;%DIST%/lib/commons-collections.jar
set CP=%CP%;%DIST%/lib/commons-dbcp.jar
set CP=%CP%;%DIST%/lib/commons-pool.jar
set CP=%CP%;%DIST%/lib/commons-beanutils.jar
set CP=%CP%;%DIST%/lib/commons-codec.jar
set CP=%CP%;%DIST%/lib/log4j.jar
set CP=%CP%;%DIST%/lib/dom4j.jar
set CP=%CP%;%DIST%/lib/jaxen.jar
set CP=%CP%;%DIST%/lib/jms.jar
set CP=%CP%;%DIST%/lib/groovy.jar
set CP=%CP%;%DIST%/lib/concurrent.jar
set CP=%CP%;%DIST%/lib/options.jar
set CP=%CP%;%DIST%/lib/snmp4j.jar
set CP=%CP%;%DIST%/lib/ezmorph.jar
set CP=%CP%;%DIST%/lib/json-lib.jar
set CP=%CP%;%DIST%/lib/zookeeper.jar
set CP=%CP%;%DIST%/lib/opencsv.jar
set CP=%CP%;%DIST%/lib/poi.jar
set CP=%CP%;%DIST%/lib/poi-ooxml.jar
set CP=%CP%;%DIST%/lib/esapi.jar
set CP=%CP%;%DIST%/lib/joda-time.jar
set CP=%CP%;%DIST%/lib/jackson-core-asl.jar
set CP=%CP%;%DIST%/lib/jackson-core.jar
set CP=%CP%;%DIST%/lib/jackson-datatype-json-org.jar
set CP=%CP%;%DIST%/lib/jackson-mapper-asl.jar
set CP=%CP%;%DIST%/lib/guava.jar

rem RabbitMQ
set CP=%CP%;%DIST%/lib/rabbitmq-client.jar

rem ElasticSearch
set CP=%CP%;%DIST%/lib/antlr-runtime.jar
set CP=%CP%;%DIST%/lib/apache-log4j-extras.jar
set CP=%CP%;%DIST%/lib/asm.jar
set CP=%CP%;%DIST%/lib/asm-commons.jar
set CP=%CP%;%DIST%/lib/elasticsearch.jar
rem set CP=%CP%;%DIST%/lib/groovy-all.jar
set CP=%CP%;%DIST%/lib/jna.jar
set CP=%CP%;%DIST%/lib/jts.jar
set CP=%CP%;%DIST%/lib/lucene-analyzers-common.jar
rem set CP=%CP%;%DIST%/lib/lucene-codecs.jar
set CP=%CP%;%DIST%/lib/lucene-core.jar
rem set CP=%CP%;%DIST%/lib/lucene-expressions.jar
set CP=%CP%;%DIST%/lib/lucene-grouping.jar
set CP=%CP%;%DIST%/lib/lucene-highlighter.jar
set CP=%CP%;%DIST%/lib/lucene-join.jar
set CP=%CP%;%DIST%/lib/lucene-memory.jar
set CP=%CP%;%DIST%/lib/lucene-misc.jar
set CP=%CP%;%DIST%/lib/lucene-queries.jar
set CP=%CP%;%DIST%/lib/lucene-queryparser.jar
set CP=%CP%;%DIST%/lib/lucene-sandbox.jar
set CP=%CP%;%DIST%/lib/lucene-spatial.jar
set CP=%CP%;%DIST%/lib/lucene-suggest.jar
set CP=%CP%;%DIST%/lib/spatial4j.jar
set CP=%CP%;%DIST%/lib/tika.jar
set CP=%CP%;%DIST%/lib/jsoup.jar

set CP=%CP%;%DIST%/lib/netty.jar
set CP=%CP%;%DIST%/lib/transport.jar
set CP=%CP%;%DIST%/lib/jackson-dataformat-cbor.jar
set CP=%CP%;%DIST%/lib/jackson-dataformat-yaml.jar
set CP=%CP%;%DIST%/lib/jackson-dataformat-smile.jar
set CP=%CP%;%DIST%/lib/reindex-client.jar
set CP=%CP%;%DIST%/lib/lang-mustache-client.jar
set CP=%CP%;%DIST%/lib/percolator-client.jar
set CP=%CP%;%DIST%/lib/transport-netty3-client.jar
set CP=%CP%;%DIST%/lib/transport-netty4-client.jar
set CP=%CP%;%DIST%/lib/HdrHistogram.jar
set CP=%CP%;%DIST%/lib/hppc.jar
set CP=%CP%;%DIST%/lib/lucene-backward-codecs.jar
set CP=%CP%;%DIST%/lib/lucene-spatial3d.jar
set CP=%CP%;%DIST%/lib/lucene-spatial-extras.jar
set CP=%CP%;%DIST%/lib/netty-buffer.jar
set CP=%CP%;%DIST%/lib/netty-codec.jar
set CP=%CP%;%DIST%/lib/netty-codec-http.jar
set CP=%CP%;%DIST%/lib/netty-common.jar
set CP=%CP%;%DIST%/lib/netty-handler.jar
set CP=%CP%;%DIST%/lib/netty-resolver.jar
set CP=%CP%;%DIST%/lib/netty-transport.jar
set CP=%CP%;%DIST%/lib/t-digest.jar
set CP=%CP%;%DIST%/lib/log4j-api.jar
set CP=%CP%;%DIST%/lib/log4j-core.jar
rem set CP=%CP%;%DIST%/lib/log4j-to-slf4j.jar
set CP=%CP%;%DIST%/lib/slf4j-api.jar

rem connectors
set CP=%CP%;%DIST%/lib/ganymed-ssh2.jar
set CP=%CP%;%DIST%/lib/commons-httpclient.jar
set CP=%CP%;%DIST%/lib/httpcore.jar
set CP=%CP%;%DIST%/lib/httpclient.jar
set CP=%CP%;%DIST%/lib/httpmime.jar
set CP=%CP%;%DIST%/lib/httpclient-win.jar
set CP=%CP%;%DIST%/lib/fluent-hc.jar
set CP=%CP%;%DIST%/lib/jna.jar
set CP=%CP%;%DIST%/lib/jna-platform.jar
set CP=%CP%;%DIST%/lib/httpclient-cache.jar
set CP=%CP%;%DIST%/lib/commons-codec.jar
set CP=%CP%;%DIST%/lib/commons-net.jar
set CP=%CP%;%DIST%/lib/mariadb-java-client.jar
set CP=%CP%;%DIST%/lib/ojdbc8.jar
set CP=%CP%;%DIST%/lib/xdb6.jar
set CP=%CP%;%DIST%/lib/mail.jar
set CP=%CP%;%DIST%/lib/activation.jar
rem set CP=%CP%;%DIST%/lib/groovyws.jar

rem REST web service (RESTLet 2.1.2) with embedded Jetty 7 (for SSL support)
set CP=%CP%;%DIST%/lib/org.restlet.ext.fileupload.jar
set CP=%CP%;%DIST%/lib/org.restlet.ext.jetty.jar
set CP=%CP%;%DIST%/lib/org.restlet.ext.ssl.jar
set CP=%CP%;%DIST%/lib/org.restlet.jar
set CP=%CP%;%DIST%/lib/org.jsslutils.jar
set CP=%CP%;%DIST%/lib/jetty-ajp.jar
set CP=%CP%;%DIST%/lib/jetty-continuation.jar
set CP=%CP%;%DIST%/lib/jetty-http.jar
set CP=%CP%;%DIST%/lib/jetty-io.jar
set CP=%CP%;%DIST%/lib/jetty-security.jar
set CP=%CP%;%DIST%/lib/jetty-server.jar
set CP=%CP%;%DIST%/lib/jetty-servlet.jar
set CP=%CP%;%DIST%/lib/jetty-servlets.jar
set CP=%CP%;%DIST%/lib/jetty-util.jar
set CP=%CP%;%DIST%/lib/jetty-webapp.jar
set CP=%CP%;%DIST%/lib/org.apache.commons.fileupload.jar
set CP=%CP%;%DIST%/lib/servlet-api.jar

rem JSQL Parser
set CP=%CP%;%DIST%/lib/jsqlparser.jar




rem final JAVA_OPTS
set JAVA_OPTS=%JAVA_OPTS%;-Djava.library.path='%JAVA_LIB_PATH%';-Dorg.apache.cxf.Logger=org.apache.cxf.common.logging.Log4jLogger

set EXECUTABLE=%DIST%\rsmgmt\bin\rsmgmt.exe
set SERVICE_NAME=RSMgmt
set PR_DISPLAYNAME=RSMgmt
set PR_DESCRIPTION=RSMgmt
set PR_INSTALL=%EXECUTABLE%
set PR_LOGPATH=%DIST%\rsmgmt\log
set PR_LOGPREFIX=service
set PR_CLASSPATH=%CP%
set PR_JVM=%JRE%\bin\server\jvm.dll
set PR_STARTUP=auto
set PR_STARTCLASS=com.resolve.rsmgmt.Main
set PR_STARTMETHOD=main
set PR_STARTPATH=%DIST%
set PR_STARTMODE=jvm
set PR_STOPCLASS=java.lang.System
set PR_STOPMETHOD=exit
set PR_STOPPATH=%DIST%
set PR_STOPMODE=jvm
set PR_STOPTIMEOUT=30
set PR_STARTPARAMS=

"%EXECUTABLE%" //IS//%SERVICE_NAME% ++StartParams=rsmgmt


set PR_STDOUTPUT=%DIST%\rsmgmt\log\stdout
set PR_STDERROR=%DIST%\rsmgmt\log\stderr
set PR_JVMMS=64
set PR_JVMMX=512
set PR_JVMSS=256
"%EXECUTABLE%" //US//%SERVICE_NAME% --JvmOptions="%JAVA_OPTS%;-XX:+HeapDumpOnOutOfMemoryError"

rem cleanup
set SERVICE_NAME=
set PR_DISPLAYNAME=
set PR_DESCRIPTION=
set PR_INSTALL=
set PR_LOGPATH=
set PR_LOGPREFIX=
set PR_CLASSPATH=
set PR_JVM=
set PR_STARTUP=
set PR_STARTCLASS=
set PR_STARTMETHOD=
set PR_STARTPATH=
set PR_STARTMODE=
set PR_STOPCLASS=
set PR_STOPMETHOD=
set PR_STOPPATH=
set PR_STOPMODE=
set PR_STOPTIMEOUT=
set PR_STARTPARAMS=
set PR_STDOUTPUT=
set PR_STDERROR=
set PR_JVMMS=
set PR_JVMMX=
set PR_JVMSS=
set JAVA_OPTS=

echo The 'rsmgmt' service has been installed.

