@echo off
setlocal EnableDelayedExpansion

pushd %~dp0\..\..
set DIST=%CD%

rem default classes
set CP=

rem Custom Jars

rem echo %CD%
set CP=%DIST%\rsgatewayemulator\lib\resolve-gatewayemulator.jar
set CP=%CP%;%DIST%\rsgatewayemulator\lib\commons-io.jar
set CP=%CP%;%DIST%\rsgatewayemulator\lib\commons-lang3.jar
set CP=%CP%;%DIST%\rsgatewayemulator\lib\commons-logging.jar
set CP=%CP%;%DIST%\rsgatewayemulator\lib\dom4j.jar
set CP=%CP%;%DIST%\rsgatewayemulator\lib\jaxen.jar
set CP=%CP%;%DIST%\rsgatewayemulator\lib\log4j.jar
set CP=%CP%;%DIST%\rsgatewayemulator\lib\esapi.jar
set CP=%CP%;%DIST%\rsgatewayemulator\lib\org.apache.commons.fileupload.jar
set CP=%CP%;%DIST%\rsgatewayemulator\lib\servlet-api.jar
set CP=%CP%;%DIST%\rsgatewayemulator\lib\commons-codec.jar
set CP=%CP%;%DIST%\rsgatewayemulator\lib\*


set PATH=%PATH%;%ENVPATH%
"java" -cp "%CP%" -Dorg.owasp.esapi.resources="%DIST%/rsgatewayemulator/config" com.resolve.rsgatewayemulator.BlueprintConfig

popd
