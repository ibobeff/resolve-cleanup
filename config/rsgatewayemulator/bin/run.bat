@echo off
setlocal EnableDelayedExpansion

pushd %~dp0\..\..
set DIST=%CD%
REM set JRE=%DIST%\jdk
REM set ENVPATH=%JRE%\bin
REM set JAVA_LIB_PATH=%DIST%\lib
REM for /f "tokens=2-5 delims=.-_" %%j in ('"%JRE%\bin\java" -fullversion 2^>^&1') do (
REM		set MAJORVERSIONNEW=%%j 
REM		set LESSERVERSIONNEW=%%l
REM	)
REM for /f "tokens=2-5 delims=.-_" %%j in ('"%DIST%\jdk\bin\java" -fullversion 2^>^&1') do (
REM		set MAJORVERSION=%%j 
REM		set LESSERVERSION=%%l
REM	)
REM if %MAJORVERSIONNEW% NEQ %MAJORVERSION% (
REM	echo given java version is java version 8 using default resolve java
REM	set JRE=%DIST%/jdk
REM )
REM if %LESSERVERSIONNEW% LSS %LESSERVERSION% (
REM	echo given java version is less than resolve's installed java version using default resolve java
REM	set JRE=%DIST%/jdk
REM )

rem execute
set JAVA_OPTS=
set JAVA_OPTS=%JAVA_OPTS% -Djava.library.path="%JAVA_LIB_PATH%"
set JAVA_OPTS=%JAVA_OPTS% -Dorg.apache.cxf.Logger=org.apache.cxf.common.logging.Log4jLogger

rem OWASP ESAPI
set JAVA_OPTS=%JAVA_OPTS% -Dorg.owasp.esapi.resources="%DIST%\rsgatewayemulator\config"

rem default classes
set CP=%DIST%\rsgatewayemulator\lib\*
set CP=%CP%;%DIST%\rsgatewayemulator\lib\gateway\*

rem set CP=%CP%;%DIST%/lib/*
set CP=%CP%;%DIST%/rsgatewayemulator/lib/*

rem external classpath
set EXTERNALCLASSPATHS=
set CP=%CP%;%EXTERNALCLASSPATHS%

rem JCONSOLE - make sure "hostname -i" resolves to an ipaddress not 127.0.0.1
rem set JCONSOLE=-Dcom.sun.management.jmxremote.port=5551 -Dcom.sun.management.jmxremote.authenticate=false -Dcom.sun.management.jmxremote.ssl=false

rem Eclipse Debugging
rem set DEBUGGING=-Xdebug -Xrunjdwp:transport=dt_socket,address=8001,server=y,suspend=n

rem GC Logging
rem set GCLOGGING=-XX:+PrintGCDetails -verbose:gc -Xloggc:$../rscontrol/log/gc.log

rem execute

set PATH=%PATH%;%ENVPATH%

"java" %JAVA_OPTS% %JCONSOLE% %DEBUGGING% %GCLOGGING% -server -Xms256M -Xmx512M -Xss256K -XX:+UseConcMarkSweepGC -XX:+CMSClassUnloadingEnabled -XX:+HeapDumpOnOutOfMemoryError -cp "%CP%" com.resolve.rsgatewayemulator.Main rsgatewayemulator %1 %2 %3

popd
