#!/bin/bash

if [ "$1" == "--help" ]; then
    echo ""
    echo "This script will stop the local RSGatewayEmulator"
    echo "gracefully.  This may take up to 10 minutes."
    echo ""
    echo "Options:"
    echo "    -k       Skip the Graceful shutdown.  This may cause"
    echo "             automations to be interrupted and eventually aborted"
    exit 0
fi

DIST=`echo $0 | awk '{ print substr( $0, 0, length($0)-7 ) }'`

cd ${DIST}

if [ ! -f lock ]; then
    echo "Lock File Not Found...Exiting"
    exit 1
fi

LOCK=`cat lock`
rm lock

kill ${LOCK}
echo "Stopping RSGatewayEmulator PID: ${LOCK}"

WAIT_LOOP=60

while [ $# -gt 0 ]
do
    if [ $1 == "-k" ]; then
        WAIT_LOOP=1
    fi
    shift
done

COUNT=0;
while [ $COUNT -lt $WAIT_LOOP ]
do
    for i in 1 2 3 4 5
    do
      PROCESS=`ps -ef | awk '/'${LOCK}'/ && !/awk/ {print $2}'`

      if [ "$PROCESS" = "" ]; then
        exit 0
      else
        sleep 1
      fi
    done
    printf "."
    COUNT=$[$COUNT+1]
done
kill -2 ${LOCK} > /dev/null 2>&1

COUNT=0;
while [ $COUNT -lt $WAIT_LOOP ]
do
    for i in 1 2 3 4 5
    do
      PROCESS=`ps -ef | awk '/'${LOCK}'/ && !/awk/ {print $2}'`

      if [ "$PROCESS" = "" ]; then
        exit 0
      else
        sleep 1
      fi
    done
    printf "."
    COUNT=$[$COUNT+1]
done
kill -9 ${LOCK} > /dev/null 2>&1
