#!/bin/bash

INSTANCE_NAME=rsgatewayemulator
script="$0"
HOME="$(dirname $script)"
cd ${HOME}/../..

HOME=`pwd`

CP="${HOME}/${INSTANCE_NAME}/lib/resolve-gatewayemulator.jar"
CP="${CP}:${HOME}/${INSTANCE_NAME}/lib/commons-io.jar"
CP="${CP}:${HOME}/${INSTANCE_NAME}/lib/commons-lang3.jar"
CP="${CP}:${HOME}/${INSTANCE_NAME}/lib/commons-logging.jar"
CP="${CP}:${HOME}/${INSTANCE_NAME}/lib/dom4j.jar"
CP="${CP}:${HOME}/${INSTANCE_NAME}/lib/jaxen.jar"
CP="${CP}:${HOME}/${INSTANCE_NAME}/lib/log4j.jar"
CP="${CP}:${HOME}/${INSTANCE_NAME}/lib/esapi.jar"
CP="${CP}:${HOME}/${INSTANCE_NAME}/lib/org.apache.commons.fileupload.jar"
CP="${CP}:${HOME}/${INSTANCE_NAME}/lib/servlet-api.jar"
CP="${CP}:${HOME}/${INSTANCE_NAME}/lib/commons-codec.jar"
CP="${CP}:${HOME}/${INSTANCE_NAME}/lib/osapi.jar"
CP="${CP}:${HOME}/${INSTANCE_NAME}/lib/*"

JAVA_EXEC=java

exec "${JAVA_EXEC}" -cp "${CP}" -Dorg.owasp.esapi.resources="${HOME}/${INSTANCE_NAME}/config" com.resolve.rsgatewayemulator.BlueprintConfig

