@echo off
setlocal EnableDelayedExpansion

pushd %~dp0\..\..
set DIST=%CD%

rem default classes
set CP=

rem Custom Jars

rem echo %CD%
set CP=%DIST%\rsgatewayemulator\lib\resolve-gatewayemulator.jar
set CP=%CP%;%DIST%\rsgatewayemulator\lib\commons-io.jar
set CP=%CP%;%DIST%\rsgatewayemulator\lib\commons-lang3.jar
set CP=%CP%;%DIST%\rsgatewayemulator\lib\dom4j.jar
set CP=%CP%;%DIST%\rsgatewayemulator\lib\jaxen.jar
set CP=%CP%;%DIST%\rsgatewayemulator\lib\ejb3-persistence.jar

FOR /R "%DIST%\rsgatewayemulator\lib\gateway" %%c in ("*.jar") DO (
    set CP=!CP!;%%c
)

set PATH=%PATH%;%ENVPATH%
"java" -cp "%CP%" com.resolve.rsgatewayemulator.ExportFilter %1

popd
