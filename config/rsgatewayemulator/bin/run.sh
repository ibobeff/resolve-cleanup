#!/bin/bash

# properties
INSTANCE_NAME=rsgatewayemulator
BIN=`echo $0 | awk '{ print substr( $0, 0, length($0)-6 ) }'`

cd ${BIN}../..
DIST=`pwd`

JDK=`$JAVA_HOME`
LOCKFILE=${DIST}/${INSTANCE_NAME}/bin/lock
# ENVPATH=${JDK}/bin
# JAVA_LIB_PATH=${DIST}/lib:${LD_LIBRARY_PATH}

if [ -f ${LOCKFILE} ]; then
    if [ $# -eq 0 ] || [ "$1" != "1" ]; then
        echo "Lock File Detected"
        echo "Cancelling ${INSTANCE_NAME} Startup"
        exit 1
    fi
fi


# set os type
linux=false
sunos=false
case "`uname`" in
Linux*) linux=true;;
SunOS*) sunos=true;;
esac

# execute
JAVA_OPTS=

# OWASP ESAPI
JAVA_OPTS="${JAVA_OPTS} -Dorg.owasp.esapi.resources=${DIST}/${INSTANCE_NAME}/config"

if $linux; then
    JAVA_EXEC=java
else
    JAVA_EXEC=${JDK}/bin/sparcv9/java
    JAVA_OPTS="${JAVA_OPTS} -d64"
fi

# default classes
CP="${DIST}/${INSTANCE_NAME}/lib/*"
CP="${DIST}/${INSTANCE_NAME}/lib/gateway/*"

#CP="${CP}:${DIST}/lib/*"
CP="${CP}:${DIST}/${INSTANCE_NAME}/lib/*"
# external classpath
EXTERNALCLASSPATHS=
CP="${CP}:${EXTERNALCLASSPATHS}"

# JCONSOLE - make sure "hostname -i" resolves to an ipaddress not 127.0.0.1
#JCONSOLE="-Dcom.sun.management.jmxremote.port=5552 -Dcom.sun.management.jmxremote.authenticate=false -Dcom.sun.management.jmxremote.ssl=false"

# Eclipse Debugging
#DEBUGGING="-Xdebug -Xrunjdwp:transport=dt_socket,address=8001,server=y,suspend=n"

# GC Logging
#GCLOGGING="-XX:+PrintGCDetails -verbose:gc -Xloggc:${DIST}/rsremote/log/gc.log"


PATH=${JDK}/bin:${PATH} export PATH

if [ $# -le 0 ]; then
    echo "Usage: [-GC] Generate Code based on provided blueprint file"
    echo "       [-UT] Execute the API Unit Test, if any"
	echo "       [-SE] Simulator Execution of gateway, also go through filter test, if any"
else
    exec "${JAVA_EXEC}" ${JAVA_OPTS} ${JCONSOLE} ${DEBUGGING} ${GCLOGGING} -Djava.library.path="${JAVA_LIB_PATH}" -Dorg.apache.cxf.Logger=org.apache.cxf.common.logging.Log4jLogger -server -Xms256M -Xmx512M -Xss256K -XX:+UseConcMarkSweepGC -XX:+CMSClassUnloadingEnabled -XX:+HeapDumpOnOutOfMemoryError -cp "${CP}" com.resolve.rsgatewayemulator.Main ${INSTANCE_NAME} $1 >> ${DIST}/${INSTANCE_NAME}/log/stdout 2>&1 
fi

#PID=$!
#echo "${PID}" > ${LOCKFILE}
#echo "Started ${INSTANCE_NAME} pid: ${PID}"

rm ${DIST}/${INSTANCE_NAME}/bin/lock

