#!/bin/bash

INSTANCE_NAME=rsgatewayemulator

script="$0"
HOME="$(dirname $script)"

cd ${HOME}/../..

HOME=`pwd`

CP="${HOME}/${INSTANCE_NAME}/lib/resolve-gatewayemulator.jar"
CP="${CP}:${HOME}/${INSTANCE_NAME}/lib/commons-io.jar"
CP="${CP}:${HOME}/${INSTANCE_NAME}/lib/commons-lang3.jar"
CP="${CP}:${HOME}/${INSTANCE_NAME}/lib/dom4j.jar"
CP="${CP}:${HOME}/${INSTANCE_NAME}/lib/jaxen.jar"
CP="${CP}:${HOME}/${INSTANCE_NAME}/lib/ejb3-persistence.jar"

# rsgatewayemulator lib classes
for FILE in `ls ${HOME}/${INSTANCE_NAME}/lib/gateway`
do
    case $FILE in
    *.jar)
        CP="${CP}:${HOME}/${INSTANCE_NAME}/lib/gateway/$FILE"
    ;;
    esac
done

JAVA_EXEC=java

exec "${JAVA_EXEC}" -cp "${CP}" com.resolve.rsgatewayemulator.ExportFilter $1
