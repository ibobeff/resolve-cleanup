#!/bin/bash

INSTANCE_NAME=rsconsole

# set os type
linux=false
sunos=false
case "`uname`" in
Linux*) linux=true;;
SunOS*) sunos=true;;
esac

# properties
BIN=`echo $0 | awk '{ print substr( $0, 0, length($0)-6 ) }'`

cd ${BIN}../..
DIST=`pwd`
. ${DIST}/rsconsole/bin/rsconsole-env.sh
JDK=${RSCONSOLEJDK}
if [ -d "${JDK}" ];
then
	JAVA_RESOLVEMAJOR=`${DIST}/jdk/bin/java -version 2>&1 | awk 'NR==1{split($0,a,"."); print a[2] }'`
	JAVA_RESOLVELESSER=`${DIST}/jdk/bin/java -version 2>&1 | awk  'NR==1{gsub(/"/,"");split($0,a,"_"); print a[2] }'`
	JAVA_BLUEPRINTMAJOR=`${JDK}/bin/java -version 2>&1 | awk 'NR==1{split($0,a,"."); print a[2] }'`
	JAVA_BLUEPRINTLESSER=`${JDK}/bin/java -version 2>&1 | awk  'NR==1{gsub(/"/,"");split($0,a,"_"); print a[2] }'`
	if [ ${JAVA_RESOLVEMAJOR} -ne ${JAVA_BLUEPRINTMAJOR} ]; then
			echo "supplied java version is different than the required version, using default resolve java"
			JDK=${DIST}/jdk
	fi

	if [ ${JAVA_RESOLVELESSER} -gt ${JAVA_BLUEPRINTLESSER} ]; then
			echo "supplied java version is less than required, using default resolve java"
			JDK=${DIST}/jdk
	fi
else
	echo given java_home folder does not exist using default resolve java
	JDK=${DIST}/jdk
fi
CP="${DIST}/rsconsole/lib/resolve-console.jar"
#CP="${CP}:${DIST}/lib/*"
CP="${CP}:${DIST}/${INSTANCE_NAME}/lib/*"
# CP="${CP}:${DIST}/lib/commons-io.jar"
# CP="${CP}:${DIST}/lib/commons-lang3.jar"
# CP="${CP}:${DIST}/lib/commons-logging.jar"
# CP="${CP}:${DIST}/lib/commons-collections.jar"
# CP="${CP}:${DIST}/lib/commons-codec.jar"
# CP="${CP}:${DIST}/lib/commons-beanutils.jar"
# CP="${CP}:${DIST}/lib/log4j.jar"
# CP="${CP}:${DIST}/lib/dom4j.jar"
# CP="${CP}:${DIST}/lib/jaxen.jar"
# CP="${CP}:${DIST}/lib/quartz.jar"
# CP="${CP}:${DIST}/lib/jms.jar"
# CP="${CP}:${DIST}/lib/oro.jar"
# CP="${CP}:${DIST}/lib/groovy.jar"
# CP="${CP}:${DIST}/lib/concurrent.jar"
# CP="${CP}:${DIST}/lib/options.jar"
# CP="${CP}:${DIST}/lib/persistence.jar"
# CP="${CP}:${DIST}/lib/ezmorph.jar"
# CP="${CP}:${DIST}/lib/json-lib.jar"
# CP="${CP}:${DIST}/lib/poi.jar"
# CP="${CP}:${DIST}/lib/poi-ooxml.jar"
# CP="${CP}:${DIST}/lib/org.apache.commons.fileupload.jar"
# CP="${CP}:${DIST}/lib/javax.servlet.jar"
# CP="${CP}:${DIST}/lib/esapi.jar"
# CP="${CP}:${DIST}/lib/jsoup.jar"
# CP="${CP}:${DIST}/lib/docx4j.jar"
# CP="${CP}:${DIST}/lib/jaxb-xmldsig-core.jar"
# CP="${CP}:${DIST}/lib/xmlgraphics-commons.jar"
# CP="${CP}:${DIST}/lib/xalan.jar"
# CP="${CP}:${DIST}/lib/slf4j-api.jar"
# CP="${CP}:${DIST}/lib/slf4j-simple.jar"
# CP="${CP}:${DIST}/lib/guava.jar"
# CP="${CP}:${DIST}/lib/xercesImpl.jar"
# CP="${CP}:${DIST}/lib/httpclient.jar"
# CP="${CP}:${DIST}/lib/httpcore.jar"
# CP="${CP}:${DIST}/lib/commons-beanutils.jar"
# CP="${CP}:${DIST}/lib/jackson-core-asl.jar"
# CP="${CP}:${DIST}/lib/jackson-mapper-asl.jar"

# RabbitMQ
CP="${CP}:${DIST}/lib/rabbitmq-client.jar"

# DB Connect
CP="${CP}:${DIST}/lib/mariadb-java-client.jar"
CP="${CP}:${DIST}/lib/ojdbc8.jar"
CP="${CP}:${DIST}/lib/xdb6.jar"

# JSQL Parser
CP="${CP}:${DIST}/lib/jsqlparser.jar"

JAVA_OPTS=

# OWASP ESAPI
JAVA_OPTS="${JAVA_OPTS} -Dorg.owasp.esapi.resources=${DIST}/rsconsole/config"


# execute
if $linux; then
    JAVA_EXEC=${JDK}/bin/java
else
    JAVA_EXEC=${JDK}/bin/sparcv9/java
    JAVA_OPTS="${JAVA_OPTS} -d64"
fi

"${JAVA_EXEC}" -Djava.io.tmpdir=${DIST}/tmp ${JAVA_OPTS} -Djava.library.path="${DIST}/lib" -cp "${CP}" com.resolve.rsconsole.Main $@
