@echo off

pushd %~dp0\..\..

set DIST=%CD%
call "%DIST%\rsconsole\bin\rsconsole-env.bat"
set JRE=%RSCONSOLEJDK%
if EXIST "%JRE%" (
	goto jre_exists
) else (
	echo given java_home folder does not exist using default resolve java
	set JRE=%DIST%/jdk
	goto finish_run
)
:jre_exists
	for /f "tokens=2-5 delims=.-_" %%j in ('"%JRE%\bin\java" -fullversion 2^>^&1') do (
			set MAJORVERSIONNEW=%%j 
			set LESSERVERSIONNEW=%%l
		)
	for /f "tokens=2-5 delims=.-_" %%j in ('"%DIST%\jdk\bin\java" -fullversion 2^>^&1') do (
			set MAJORVERSION=%%j 
			set LESSERVERSION=%%l
		)
	if 0%MAJORVERSIONNEW% NEQ 0%MAJORVERSION% (
		echo given java version is java version 8 using default resolve java
		set JRE=%DIST%/jdk
	)
	if 0%LESSERVERSIONNEW% LSS 0%LESSERVERSION% (
		echo given java version is less than resolve's installed java version using default resolve java
		set JRE=%DIST%/jdk
	)
	goto finish_run
:finish_run
set CP=%DIST%/rsconsole/lib/resolve-console.jar

rem set CP=%CP%;%DIST%/lib/*
set CP=%CP%;%DIST%/rsconsole/lib/*
rem set CP=%CP%;%DIST%/lib/commons-io.jar
rem set CP=%CP%;%DIST%/lib/commons-lang.jar
rem set CP=%CP%;%DIST%/lib/commons-lang3.jar
rem set CP=%CP%;%DIST%/lib/commons-logging.jar
rem set CP=%CP%;%DIST%/lib/commons-collections.jar
rem set CP=%CP%;%DIST%/lib/commons-codec.jar
rem set CP=%CP%;%DIST%/lib/commons-beanutils.jar
rem set CP=%CP%;%DIST%/lib/log4j.jar
rem set CP=%CP%;%DIST%/lib/dom4j.jar
rem set CP=%CP%;%DIST%/lib/jaxen.jar
rem set CP=%CP%;%DIST%/lib/quartz.jar
rem set CP=%CP%;%DIST%/lib/oro.jar
rem set CP=%CP%;%DIST%/lib/jms.jar
rem set CP=%CP%;%DIST%/lib/groovy.jar
rem set CP=%CP%;%DIST%/lib/concurrent.jar
rem set CP=%CP%;%DIST%/lib/options.jar
rem set CP=%CP%;%DIST%/lib/persistence.jar
rem set CP=%CP%;%DIST%/lib/ezmorph.jar
rem set CP=%CP%;%DIST%/lib/json-lib.jar
rem set CP=%CP%;%DIST%/lib/poi.jar
rem set CP=%CP%;%DIST%/lib/poi-ooxml.jar
rem set CP=%CP%;%DIST%/lib/org.apache.commons.fileupload.jar
rem set CP=%CP%;%DIST%/lib/javax.servlet.jar
rem set CP=%CP%;%DIST%/lib/esapi.jar
rem set CP=%CP%;%DIST%/lib/jsoup.jar
rem set CP=%CP%;%DIST%/lib/docx4j.jar
rem set CP=%CP%;%DIST%/lib/jaxb-xmldsig-core.jar
rem set CP=%CP%;%DIST%/lib/xmlgraphics-commons.jar
rem set CP=%CP%;%DIST%/lib/xalan.jar
rem set CP=%CP%;%DIST%/lib/slf4j-api.jar
rem set CP=%CP%;%DIST%/lib/slf4j-simple.jar
rem set CP=%CP%;%DIST%/lib/guava.jar
rem set CP=%CP%;%DIST%/lib/xercesImpl.jar
rem set CP=%CP%;%DIST%/lib/httpcore.jar
rem set CP=%CP%;%DIST%/lib/httpclient.jar
rem set CP=%CP%;%DIST%/lib/commons-beanutils.jar
rem set CP=%CP%;%DIST%/lib/jackson-core-asl.jar
rem set CP=%CP%;%DIST%/lib/jackson-mapper-asl.jar


rem RABBITMQ
set CP=%CP%;%DIST%/lib/rabbitmq-client.jar

rem DB Connect
set CP=%CP%;%DIST%/lib/mariadb-java-client.jar
set CP=%CP%;%DIST%/lib/ojdbc8.jar
set CP=%CP%;%DIST%/lib/xdb6.jar

rem JSQL Parser
rem set CP=%CP%;%DIST%/lib/jsqlparser.jar

set JAVA_OPTS=

rem OWASP ESAPI
set JAVA_OPTS=%JAVA_OPTS% -Dorg.owasp.esapi.resources="%DIST%/rsconsole/config"

"%JRE%\bin\java" %JAVA_OPTS% -Djava.library.path="%DIST%/lib" -cp "%CP%" com.resolve.rsconsole.Main %1 %2 %3 %4 %5 %6 %7 %8 %9

popd

set DIST=
set JRE=
set CP=
set JAVA_OPTS=
