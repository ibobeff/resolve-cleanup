input {
  jdbc {
    jdbc_driver_library => "../lib/mariadb-java-client.jar"
    jdbc_driver_class => "org.mariadb.jdbc.Driver"
    jdbc_connection_string => "jdbc:mariadb://localhost:3306/resolve"
	jdbc_default_timezone => "America/Los_Angeles"
    jdbc_user => "resolve"
    jdbc_password => "${RESOLVE_DB_PWD}"
    clean_run => "false"
    last_run_metadata_path => ".logstash_jdbc_last_run"
    schedule => "*/5 * * * *"
    statement => "select rsi.sys_is_deleted, rsi.u_problem_id, rsi.sys_id, rsi.u_problem_id, rsi.sys_updated_on, rsi.sys_org, rsi.u_sir, rsi.u_title, w.u_content description, rsi.u_investigation_type,
	rsi.u_playbook, rsi.u_severity, rsi.u_due_by, rsi.u_owner, rsi.u_members, rsi.u_source_system, rsi.u_status, rsi.u_sla, rsi.sys_created_on,
	rsi.u_closed_on, rsi.u_closed_by, rsi.u_data_compromised, rsi.u_priority, rsi.u_classification
	from resolve_security_incident rsi, wikidoc w where w.u_fullname = rsi.u_playbook
	and rsi.sys_updated_on > :sql_last_value"
  }
}

filter {
	mutate {            
        gsub => [
           "description", "<.*?>|{(.*?)}", ""
        ]
        strip => ["description"]
    }

	elasticsearch {
		hosts => ["localhost:9200"]
        index => "pbartifacts"
		query => "sir:%{u_sir}"
		sort => "sysCreatedOn:desc"
		fields => { 
			"sysId" => "pbartifact_sys_id"
			"sysCreatedBy" => "pbartifact_sys_created_by"
			"sysCreatedOn" => "pbartifact_sys_created_on"
			"sysUpdatedBy" => "pbartifact_sys_updated_by"
			"sysUpdatedOn" => "pbartifact_sys_updated_on"
			"name" => "pbartifact_name"
			"value" => "pbartifact_value"
			"source" => "pbartifact_source"
			"sourceValue" => "pbartifact_source_value"
		}
		result_size => 999
	}

	elasticsearch {
		hosts => ["localhost:9200"]
        index => "pbnotes"
		query => "incidentId:%{sys_id}"
		sort => "sysCreatedOn:desc"
		fields => { 
			"sysId" => "pbnotes_sys_id"
			"note" => "pbnotes_note"
			"sysCreatedBy" => "pbnotes_sys_created_by"
			"sysCreatedOn" => "pbnotes_sys_created_on"
			"activityName" => "pbnotes_activity_name"
			"source" => "pbnotes_source"
			"sourceValue" => "pbnotes_source_value"
		}
		result_size => 999
	}

	elasticsearch {
		hosts => ["localhost:9200"]
        index => "taskresultalias"
		query => "problemId:%{u_problem_id}"
		sort => "sysCreatedOn:desc"
		fields => { 
			"sysId" => "automation_result_sys_id"
			"sysCreatedBy" => "automation_result_sys_created_by"
			"sysCreatedOn" => "automation_result_sys_created_on"
			"summary" => "automation_result_summary"
			"detail" => "automation_result_detail"
			"targetGUID" => "automation_result_targetGUID"
			"esbaddr" => "automation_result_esbaddr"
			"completion" => "automation_result_completion"
			"condition" => "automation_result_condition"
			"severity" => "automation_result_severity"
		}
		result_size => 999
	}

	elasticsearch {
		hosts => ["localhost:9200"]
        index => "pbattachment"
		query => "incidentId:%{sys_id}"
		sort => "sysCreatedOn:desc"
		fields => { 
			"sysId" => "pbattachment_sys_id"
			"sysCreatedBy" => "pbattachment_sys_created_by"
			"sysCreatedOn" => "pbattachment_sys_created_on"
			"name" => "pbattachment_name"
			"description" => "pbattachment_description"
			"malicious" => "pbattachment_malicious"
			"source" => "pbattachment_source"
			"sourceValue" => "pbattachment_source_value"
		}
		result_size => 999
	}

	aggregate {
		task_id => "%{sys_id}"
		code => "
			if (event.get('u_sir') != nil)
				map['sys_id'] = event.get('sys_id')
				map['sys_updated_by'] = event.get('sys_updated_by')
				map['sys_updated_on'] = event.get('sys_updated_on')
				map['sys_org'] = event.get('sys_org')
				map['sir'] = event.get('u_sir')
				map['title'] = event.get('u_title')
				map['description'] = event.get('description')
				map['playbook'] = event.get('u_playbook')
				map['severity'] = event.get('u_severity')
				map['due_by'] = event.get('u_due_by')
				map['owner'] = event.get('u_owner')
				map['members'] = event.get('u_members')
				map['source_system'] = event.get('u_source_system')
				map['status'] = event.get('u_status')
				map['sla'] = event.get('u_sla')
				map['sys_created_on'] = event.get('sys_created_on')
				map['closed_on'] = event.get('u_closed_on')
				map['closed_by'] = event.get('u_closed_by')
				map['data_compromised'] = event.get('u_data_compromised')
				map['sir_type'] = event.get('u_investigation_type')
				map['priority'] = event.get('u_priority')
				map['classification'] = event.get('u_classification')
				map['sys_is_deleted'] = event.get('sys_is_deleted')
				map['problem_id'] = event.get('u_problem_id')

				map['pbartifacts'] ||= []
				if (event.get('pbartifact_sys_id') != nil)
					map['pbartifacts'] << {
						'sys_id' => event.get('pbartifact_sys_id'),
						'sys_created_by' => event.get('pbartifact_sys_created_by'),
						'sys_created_on' => event.get('pbartifact_sys_created_on'),
						'sys_updated_by' => event.get('pbartifact_sys_updated_by'),
						'sys_updated_on' => event.get('pbartifact_sys_updated_on'),
						'name' => event.get('pbartifact_name'),
						'value' => event.get('pbartifact_value'),
						'source' => event.get('pbartifact_source'),
						'source_value' => event.get('pbartifact_source_value'),
					}

					if (map['pbartifacts'].first['sys_id'].kind_of?(Array))
						map['pbartifacts'] = map['pbartifacts'].first.inject([]){|a, (k,vs)| 
	 						vs.each_with_index{|v,i| (a[i] ||= {})[k] = v} 
							a
						}
					end
				end

				map['pbnotes'] ||= []
				if (event.get('pbnotes_sys_id') != nil)
					map['pbnotes'] << {
						'sys_id' => event.get('pbnotes_sys_id'),
						'note' => event.get('pbnotes_note'),
						'sys_created_by' => event.get('pbnotes_sys_created_by'),
						'sys_created_on' => event.get('pbnotes_sys_created_on'),
						'activity_name' => event.get('pbnotes_activity_name'),
						'source' => event.get('pbnotes_source'),
						'source_value' => event.get('pbnotes_source_value'),
					}

					if (map['pbnotes'].first['sys_id'].kind_of?(Array))
						map['pbnotes'] = map['pbnotes'].first.inject([]){|a, (k,vs)| 
	 						vs.each_with_index{|v,i| (a[i] ||= {})[k] = v} 
							a
						}
					end
				end

				map['automation_results'] ||= []
				if (event.get('automation_result_sys_id') != nil)
					map['automation_results'] << {
						'sys_id' => event.get('automation_result_sys_id'),
						'sys_created_by' => event.get('automation_result_sys_created_by'),
						'sys_created_on' => event.get('automation_result_sys_created_on'),
						'summary' => event.get('automation_result_summary'),
						'detail' => event.get('automation_result_detail'),
						'target' => event.get('automation_result_targetGUID'),
						'esbaddr' => event.get('automation_result_esbaddr'),
						'completion' => event.get('automation_result_completion'),
						'severity' => event.get('automation_result_severity'),
						'condition' => event.get('automation_result_condition'),
					}

					if (map['automation_results'].first['sys_id'].kind_of?(Array))
						map['automation_results'] = map['automation_results'].first.inject([]){|a, (k,vs)| 
	 						vs.each_with_index{|v,i| (a[i] ||= {})[k] = v} 
							a
						}
					end
				end

				map['pbattachments'] ||= []
				if (event.get('pbattachment_sys_id') != nil)
					map['pbattachments'] << {
						'sys_id' => event.get('pbattachment_sys_id'),
						'sys_created_by' => event.get('pbattachment_sys_created_by'),
						'sys_created_on' => event.get('pbattachment_sys_created_on'),
						'name' => event.get('pbattachment_name'),
						'malicious' => event.get('pbattachment_malicious'),
						'source' => event.get('pbattachment_source'),
						'source_value' => event.get('pbattachment_source_value'),
						'description' => event.get('pbattachment_description'),
					}

					if (map['pbattachments'].first['sys_id'].kind_of?(Array))
						map['pbattachments'] = map['pbattachments'].first.inject([]){|a, (k,vs)| 
	 						vs.each_with_index{|v,i| (a[i] ||= {})[k] = v} 
							a
						}
					end
				end
			end
			event.cancel()
		"
		push_previous_map_as_event => true
		timeout => 30
   }
}

output {
  elasticsearch { 
  	hosts => ["localhost:9200"]
  	index => "security-incident"
  	document_id => "%{sys_id}"
  	codec => "json"
  }
}
