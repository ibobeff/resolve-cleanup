#!/bin/bash
LOGSTASH_BIN="$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )"
BIN=`echo $0 | awk '{ print substr( $0, 0, length($0)-20 ) }'`

cd ${BIN}/../..
DIST=`pwd`
. ${DIST}/logstash/bin/logstash-env.sh
export JAVA_HOME=${LOGSTASHJDK}

if [ -d ${JAVA_HOME} ];
	then 
	JAVA_RESOLVEMAJOR=`${DIST}/jdk/bin/java -version 2>&1 | awk 'NR==1{split($0,a,"."); print a[2] }'`
	JAVA_RESOLVELESSER=`${DIST}/jdk/bin/java -version 2>&1 | awk  'NR==1{gsub(/"/,"");split($0,a,"_"); print a[2] }'`
	JAVA_BLUEPRINTMAJOR=`${JAVA_HOME}/bin/java -version 2>&1 | awk 'NR==1{split($0,a,"."); print a[2] }'`
	JAVA_BLUEPRINTLESSER=`${JAVA_HOME}/bin/java -version 2>&1 | awk  'NR==1{gsub(/"/,"");split($0,a,"_"); print a[2] }'`
	if [ ${JAVA_RESOLVEMAJOR} -ne ${JAVA_BLUEPRINTMAJOR} ]; then
			echo "supplied java version is different than the required version, using default resolve java"
			export JAVA_HOME=${DIST}/jdk
	fi

	if [ ${JAVA_RESOLVELESSER} -gt ${JAVA_BLUEPRINTLESSER} ]; then
			echo "supplied java version is less than required, using  default resolve java"
			export JAVA_HOME=${DIST}/jdk
	fi
else
	echo given java_home folder does not exist using default resolve java
	export JAVA_HOME=${DIST}/jdk
fi
$LOGSTASH_BIN/logstash-keystore $@
