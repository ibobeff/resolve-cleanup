#!/bin/bash
if [ $# -lt 2 ]; then
  echo ""
  echo "USAGE: <KEYSTORE-KEY> <KEYSTORE-VALUE>"
  echo "KEYSTORE-KEY - The name of the logstash keystore key"
  echo "KEYSTORE-VALUE - raw text value of that associate with keystore key"
  echo "This script will change value of logstash keystore of a given key to the given keystore value"
  exit 1
fi
LOGSTASH_BIN="$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )"
echo "logstash-keystore.sh remove $1"
$LOGSTASH_BIN/logstash-keystore.sh remove $1
echo "logstash-keystore.sh add $1"
$LOGSTASH_BIN/logstash-keystore.sh add $1 <<!
$2
!
