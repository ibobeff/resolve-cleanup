@echo off

pushd %~dp0\..\..
set DIST=%CD%
call "%DIST%\rssync\bin\rssync-env.bat"
set JRE=%RSSYNCJDK%

if EXIST "%JRE%" (
	goto jre_exists
) else (
	echo given java_home folder does not exist using default resolve java
	set JRE=%DIST%/jdk
	goto finish_run
)
:jre_exists
	for /f "tokens=2-5 delims=.-_" %%j in ('"%JRE%\bin\java" -fullversion 2^>^&1') do (
			set MAJORVERSIONNEW=%%j 
			set LESSERVERSIONNEW=%%l
		)
	for /f "tokens=2-5 delims=.-_" %%j in ('"%DIST%\jdk\bin\java" -fullversion 2^>^&1') do (
			set MAJORVERSION=%%j 
			set LESSERVERSION=%%l
		)
	if 0%MAJORVERSIONNEW% NEQ 0%MAJORVERSION% (
		echo given java version is java version 8 using default resolve java
		set JRE=%DIST%/jdk
	)
	if 0%LESSERVERSIONNEW% LSS 0%LESSERVERSION% (
		echo given java version is less than resolve's installed java version using default resolve java
		set JRE=%DIST%/jdk
	)
	goto finish_run
:finish_run
set ENVPATH=%JRE%/bin
set JAVA_LIB_PATH=%DIST%/lib

rem execute
set JAVA_OPTS=
set JAVA_OPTS=%JAVA_OPTS% -Djava.library.path="%JAVA_LIB_PATH%"
set JAVA_OPTS=%JAVA_OPTS% -Dorg.apache.cxf.Logger=org.apache.cxf.common.logging.Log4jLogger

rem OWASP ESAPI
set JAVA_OPTS=%JAVA_OPTS% -Dorg.owasp.esapi.resources="%DIST%/rssync/config"

rem default classes
set CP=%DIST%/rssync/service/sync
set CP=%CP%;%DIST%/rssync/lib/resolve-sync.jar

rem set CP=%CP%;%DIST%/lib/*
set CP=%CP%;%DIST%/rssync/lib/*
rem set CP=%CP%;%DIST%/lib/commons-io.jar
rem set CP=%CP%;%DIST%/lib/commons-lang.jar
rem set CP=%CP%;%DIST%/lib/commons-lang3.jar
rem set CP=%CP%;%DIST%/lib/commons-logging.jar
rem set CP=%CP%;%DIST%/lib/commons-codec.jar
rem set CP=%CP%;%DIST%/lib/log4j.jar
rem set CP=%CP%;%DIST%/lib/dom4j.jar
rem set CP=%CP%;%DIST%/lib/jaxen.jar
rem set CP=%CP%;%DIST%/lib/esapi.jar
rem set CP=%CP%;%DIST%/lib/joda-time.jar
rem set CP=%CP%;%DIST%/lib/org.apache.commons.fileupload.jar
rem set CP=%CP%;%DIST%/lib/servlet-api.jar

rem rem RabbitMQ
rem set CP=%CP%;%DIST%/lib/rabbitmq-client.jar

rem rem ElasticSearch
rem set CP=%CP%;%DIST%/lib/antlr-runtime.jar
rem set CP=%CP%;%DIST%/lib/asm.jar
rem set CP=%CP%;%DIST%/lib/asm-commons.jar
rem set CP=%CP%;%DIST%/lib/elasticsearch.jar
rem rem set CP=%CP%;%DIST%/lib/groovy-all.jar
rem set CP=%CP%;%DIST%/lib/jna.jar
rem set CP=%CP%;%DIST%/lib/jts.jar
rem set CP=%CP%;%DIST%/lib/lucene-analyzers-common.jar
rem set CP=%CP%;%DIST%/lib/lucene-core.jar
rem set CP=%CP%;%DIST%/lib/lucene-expressions.jar
rem set CP=%CP%;%DIST%/lib/lucene-grouping.jar
rem set CP=%CP%;%DIST%/lib/lucene-highlighter.jar
rem set CP=%CP%;%DIST%/lib/lucene-join.jar
rem set CP=%CP%;%DIST%/lib/lucene-memory.jar
rem set CP=%CP%;%DIST%/lib/lucene-misc.jar
rem set CP=%CP%;%DIST%/lib/lucene-queries.jar
rem set CP=%CP%;%DIST%/lib/lucene-queryparser.jar
rem set CP=%CP%;%DIST%/lib/lucene-sandbox.jar
rem set CP=%CP%;%DIST%/lib/lucene-spatial.jar
rem set CP=%CP%;%DIST%/lib/lucene-suggest.jar
rem set CP=%CP%;%DIST%/lib/spatial4j.jar
rem set CP=%CP%;%DIST%/lib/tika.jar
rem set CP=%CP%;%DIST%/lib/jsoup.jar
rem set CP=%CP%;%DIST%/lib/mariadb-java-client.jar
rem set CP=%CP%;%DIST%/lib/ojdbc8.jar
rem set CP=%CP%;%DIST%/lib/xdb6.jar
rem set CP=%CP%;%DIST%/lib/commons-dbcp.jar
rem set CP=%CP%;%DIST%/lib/commons-pool.jar

rem set CP=%CP%;%DIST%/lib/jackson-core-asl.jar
rem set CP=%CP%;%DIST%/lib/jackson-core.jar
rem set CP=%CP%;%DIST%/lib/jackson-datatype-json-org.jar
rem set CP=%CP%;%DIST%/lib/jackson-mapper-asl.jar
rem set CP=%CP%;%DIST%/lib/guava.jar
rem set CP=%CP%;%DIST%/lib/json-lib.jar

rem JCONSOLE - make sure "hostname -i" resolves to an ipaddress not 127.0.0.1
rem set JCONSOLE=-Dcom.sun.management.jmxremote.port=5551 -Dcom.sun.management.jmxremote.authenticate=false -Dcom.sun.management.jmxremote.ssl=false

rem Eclipse Debugging
rem set DEBUGGING=-Xdebug -Xrunjdwp:transport=dt_socket,address=8005,server=y,suspend=n

rem GC Logging
rem set GCLOGGING=-XX:+PrintGCDetails -verbose:gc -Xloggc:${DIST}/rssync/log/gc.log

rem execute
cd %DIST%

set PATH=%PATH%;%ENVPATH%
"%JRE%\bin\java" %JAVA_OPTS% %JCONSOLE% %DEBUGGING% %GCLOGGING% -server -Xms64M -Xmx512M -Xss256K -XX:+UseConcMarkSweepGC -XX:+HeapDumpOnOutOfMemoryError -cp "%CP%" com.resolve.rssync.Main rssync %1 %2 %3 %4 %5 %6 %7 %8 %9

popd

set DIST=
set JRE=
set ENVPATH=
set JAVA_LIB_PATH=
set JAVA_OPTS=
set JCONSOLE=
set DEBUGGING=
set GCLOGGING=
