@echo off

pushd %~dp0..\..
set DIST=%CD%
call "%DIST%\rssync\bin\rssync-env.bat"
popd
set JRE=%RSSYNCJDK%

if EXIST "%JRE%" (
	goto jre_exists
) else (
	echo given java_home folder does not exist using default resolve java
	set JRE=%DIST%\jdk
	goto finish_run
)
:jre_exists
	for /f "tokens=2-5 delims=.-_" %%j in ('"%JRE%\bin\java" -fullversion 2^>^&1') do (
			set MAJORVERSIONNEW=%%j 
			set LESSERVERSIONNEW=%%l
		)
	for /f "tokens=2-5 delims=.-_" %%j in ('"%DIST%\jdk\bin\java" -fullversion 2^>^&1') do (
			set MAJORVERSION=%%j 
			set LESSERVERSION=%%l
		)
	if 0%MAJORVERSIONNEW% NEQ 0%MAJORVERSION% (
		echo given java version is java version 8 using default resolve java
		set JRE=%DIST%\jdk
	)
	if 0%LESSERVERSIONNEW% LSS 0%LESSERVERSION% (
		echo given java version is less than resolve's installed java version using default resolve java
		set JRE=%DIST%\jdk
	)
	goto finish_run
:finish_run
set ENVPATH=%JRE%\bin
set JAVA_LIB_PATH=%DIST%\lib

rem init JAVA_OPTS
set JAVA_OPTS=
IF DEFINED JAVA_OPTS set JAVA_OPTS=%JAVA_OPTS%;
set JAVA_OPTS=%JAVA_OPTS%-Dresolve.component.type=rssync

rem OWASP ESAPI
set JAVA_OPTS=%JAVA_OPTS%;-Dorg.owasp.esapi.resources=%DIST%\rssync\config

rem default classes
set CP=%DIST%/rssync/service/sync
set CP=%CP%;%DIST%/rssync/lib/resolve-sync.jar
set CP=%CP%;%DIST%/lib/commons-io.jar
set CP=%CP%;%DIST%/lib/commons-lang.jar
set CP=%CP%;%DIST%/lib/commons-lang3.jar
set CP=%CP%;%DIST%/lib/commons-logging.jar
set CP=%CP%;%DIST%/lib/commons-codec.jar
set CP=%CP%;%DIST%/lib/log4j.jar
set CP=%CP%;%DIST%/lib/dom4j.jar
set CP=%CP%;%DIST%/lib/jaxen.jar
set CP=%CP%;%DIST%/lib/esapi.jar
set CP=%CP%;%DIST%/lib/joda-time.jar
set CP=%CP%;%DIST%/lib/org.apache.commons.fileupload.jar
set CP=%CP%;%DIST%/lib/servlet-api.jar

rem ElasticSearch
set CP=%CP%;%DIST%/lib/antlr-runtime.jar
set CP=%CP%;%DIST%/lib/asm.jar
set CP=%CP%;%DIST%/lib/asm-commons.jar
set CP=%CP%;%DIST%/lib/elasticsearch.jar
rem set CP=%CP%;%DIST%/lib/groovy-all.jar
set CP=%CP%;%DIST%/lib/jna.jar
set CP=%CP%;%DIST%/lib/jts.jar
set CP=%CP%;%DIST%/lib/lucene-analyzers-common.jar
set CP=%CP%;%DIST%/lib/lucene-core.jar
set CP=%CP%;%DIST%/lib/lucene-expressions.jar
set CP=%CP%;%DIST%/lib/lucene-grouping.jar
set CP=%CP%;%DIST%/lib/lucene-highlighter.jar
set CP=%CP%;%DIST%/lib/lucene-join.jar
set CP=%CP%;%DIST%/lib/lucene-memory.jar
set CP=%CP%;%DIST%/lib/lucene-misc.jar
set CP=%CP%;%DIST%/lib/lucene-queries.jar
set CP=%CP%;%DIST%/lib/lucene-queryparser.jar
set CP=%CP%;%DIST%/lib/lucene-sandbox.jar
set CP=%CP%;%DIST%/lib/lucene-spatial.jar
set CP=%CP%;%DIST%/lib/lucene-suggest.jar
set CP=%CP%;%DIST%/lib/spatial4j.jar
set CP=%CP%;%DIST%/lib/tika.jar
set CP=%CP%;%DIST%/lib/jsoup.jar

set CP=%CP%;%DIST%/lib/jackson-core-asl.jar
set CP=%CP%;%DIST%/lib/jackson-core.jar
set CP=%CP%;%DIST%/lib/jackson-datatype-json-org.jar
set CP=%CP%;%DIST%/lib/jackson-mapper-asl.jar
set CP=%CP%;%DIST%/lib/guava.jar
set CP=%CP%;%DIST%/lib/json-lib.jar

rem final JAVA_OPTS
set JAVA_OPTS=%JAVA_OPTS%;-Djava.library.path='%JAVA_LIB_PATH%'

set EXECUTABLE=%DIST%\rssync\bin\rssync.exe
set SERVICE_NAME=RSSync
set PR_DISPLAYNAME=RSSync
set PR_DESCRIPTION=RSSync
set PR_INSTALL=%EXECUTABLE%
set PR_LOGPATH=%DIST%\rssync\log
set PR_LOGPREFIX=service
set PR_CLASSPATH=%CP%
set PR_JVM=%JRE%\bin\server\jvm.dll
set PR_STARTUP=auto
set PR_STARTCLASS=com.resolve.rssync.Main
set PR_STARTMETHOD=main
set PR_STARTPATH=%DIST%
set PR_STARTMODE=jvm
set PR_STOPCLASS=java.lang.System
set PR_STOPMETHOD=exit
set PR_STOPPATH=%DIST%
set PR_STOPMODE=jvm
set PR_STOPTIMEOUT=30
set PR_STARTPARAMS=

"%EXECUTABLE%" //IS//%SERVICE_NAME% ++StartParams=rssync


set PR_STDOUTPUT=%DIST%\rssync\log\stdout
set PR_STDERROR=%DIST%\rssync\log\stderr
set PR_JVMMS=64
set PR_JVMMX=1024
set PR_JVMSS=1024
"%EXECUTABLE%" //US//%SERVICE_NAME% --JvmOptions="%JAVA_OPTS%;-XX:+HeapDumpOnOutOfMemoryError"

rem cleanup
set SERVICE_NAME=
set PR_DISPLAYNAME=
set PR_DESCRIPTION=
set PR_INSTALL=
set PR_LOGPATH=
set PR_LOGPREFIX=
set PR_CLASSPATH=
set PR_JVM=
set PR_STARTUP=
set PR_STARTCLASS=
set PR_STARTMETHOD=
set PR_STARTPATH=
set PR_STARTMODE=
set PR_STOPCLASS=
set PR_STOPMETHOD=
set PR_STOPPATH=
set PR_STOPMODE=
set PR_STOPTIMEOUT=
set PR_STARTPARAMS=
set PR_STDOUTPUT=
set PR_STDERROR=
set PR_JVMMS=
set PR_JVMMX=
set PR_JVMSS=
set JAVA_OPTS=

echo The 'rssync' service has been installed.

