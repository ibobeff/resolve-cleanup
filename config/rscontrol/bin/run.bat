@echo off
setlocal EnableDelayedExpansion

pushd %~dp0\..\..
set DIST=%CD%
call "%DIST%\rscontrol\bin\rscontrol-env.bat"
set JDK=%RSCONTROLJDK%
if EXIST "%JDK%" (
	goto jre_exists
) else (
	echo given java_home folder does not exist using default resolve java
	set JDK=%DIST%/jdk
	goto finish_run
)
:jre_exists
	for /f "tokens=2-5 delims=.-_" %%j in ('"%JDK%\bin\java" -fullversion 2^>^&1') do (
			set MAJORVERSIONNEW=%%j 
			set LESSERVERSIONNEW=%%l
		)
	for /f "tokens=2-5 delims=.-_" %%j in ('"%DIST%\jdk\bin\java" -fullversion 2^>^&1') do (
			set MAJORVERSION=%%j 
			set LESSERVERSION=%%l
		)
	if 0%MAJORVERSIONNEW% NEQ 0%MAJORVERSION% (
		echo given java version is java version 8 using default resolve java
		set JDK=%DIST%/jdk
	)
	if 0%LESSERVERSIONNEW% LSS 0%LESSERVERSION% (
		echo given java version is less than resolve's installed java version using default resolve java
		set JDK=%DIST%/jdk
	)
	goto finish_run
:finish_run
set CP=%DIST%/rscontrol/config
set CP=%CP%;%DIST%/rscontrol/lib/resolve-control.jar

rem set CP=%CP%;%DIST%/lib/*
set CP=%CP%;%DIST%/rscontrol/lib/*
rem set CP=%CP%;%DIST%/lib
rem set CP=%CP%;%DIST%/lib/classmate.jar
rem set CP=%CP%;%DIST%/lib/commons-io.jar
rem set CP=%CP%;%DIST%/lib/commons-lang.jar
rem set CP=%CP%;%DIST%/lib/commons-lang3.jar
rem set CP=%CP%;%DIST%/lib/commons-logging.jar
rem set CP=%CP%;%DIST%/lib/commons-collections.jar
rem set CP=%CP%;%DIST%/lib/commons-collections4.jar
rem set CP=%CP%;%DIST%/lib/commons-dbcp.jar
rem set CP=%CP%;%DIST%/lib/commons-pool.jar
rem set CP=%CP%;%DIST%/lib/commons-beanutils.jar
rem set CP=%CP%;%DIST%/lib/commons-codec.jar
rem set CP=%CP%;%DIST%/lib/httpcore.jar
rem set CP=%CP%;%DIST%/lib/httpclient.jar
rem set CP=%CP%;%DIST%/lib/httpmime.jar
rem set CP=%CP%;%DIST%/lib/log4j.jar
rem set CP=%CP%;%DIST%/lib/dom4j.jar
rem set CP=%CP%;%DIST%/lib/jaxen.jar
rem set CP=%CP%;%DIST%/lib/quartz.jar
rem set CP=%CP%;%DIST%/lib/jms.jar
rem set CP=%CP%;%DIST%/lib/groovy.jar
rem set CP=%CP%;%DIST%/lib/concurrent.jar
rem set CP=%CP%;%DIST%/lib/persistence.jar
rem set CP=%CP%;%DIST%/lib/antlr.jar
rem set CP=%CP%;%DIST%/lib/javassist.jar
rem set CP=%CP%;%DIST%/lib/jta.jar
rem set CP=%CP%;%DIST%/lib/slf4j-api.jar
rem set CP=%CP%;%DIST%/lib/slf4j-log4j.jar
rem set CP=%CP%;%DIST%/lib/hibernate-core.jar
rem set CP=%CP%;%DIST%/lib/hibernate-commons-annotations.jar
rem set CP=%CP%;%DIST%/lib/hibernate-ehcache.jar
rem set CP=%CP%;%DIST%/lib/hibernate-jpa-2.1.jar
rem set CP=%CP%;%DIST%/lib/naming-factory.jar
rem set CP=%CP%;%DIST%/lib/naming-resources.jar
rem set CP=%CP%;%DIST%/lib/transactions-hibernate4.jar
rem set CP=%CP%;%DIST%/lib/transactions-osgi.jar
rem set CP=%CP%;%DIST%/lib/ezmorph.jar
rem set CP=%CP%;%DIST%/lib/jandex.jar
rem set CP=%CP%;%DIST%/lib/json-lib.jar
rem set CP=%CP%;%DIST%/lib/ehcache.jar
rem set CP=%CP%;%DIST%/lib/jackson-core-asl.jar
rem set CP=%CP%;%DIST%/lib/jackson-core.jar
rem set CP=%CP%;%DIST%/lib/jackson-datatype-json-org.jar
rem set CP=%CP%;%DIST%/lib/jackson-mapper-asl.jar
rem set CP=%CP%;%DIST%/lib/jboss-logging.jar
rem set CP=%CP%;%DIST%/lib/jboss-transaction-api_1.2_spec.jar
rem set CP=%CP%;%DIST%/lib/org.apache.commons.fileupload.jar
rem set CP=%CP%;%DIST%/lib/javax.servlet.jar
rem set CP=%CP%;%DIST%/lib/esapi.jar
rem set CP=%CP%;%DIST%/lib/docx4j.jar
rem set CP=%CP%;%DIST%/lib/jaxb-xmldsig-core.jar
rem set CP=%CP%;%DIST%/lib/xmlgraphics-commons.jar
rem set CP=%CP%;%DIST%/lib/xalan.jar
rem set CP=%CP%;%DIST%/lib/slf4j-api.jar

rem rem Database
rem set CP=%CP%;%DIST%/lib/mariadb-java-client.jar
rem set CP=%CP%;%DIST%/lib/ojdbc8.jar
rem set CP=%CP%;%DIST%/lib/xdb6.jar

rem rem RabbitMQ
rem set CP=%CP%;%DIST%/lib/rabbitmq-client.jar

rem rem ElasticSearch
rem set CP=%CP%;%DIST%/lib/antlr-runtime.jar
rem set CP=%CP%;%DIST%/lib/apache-log4j-extras.jar
rem set CP=%CP%;%DIST%/lib/asm.jar
rem set CP=%CP%;%DIST%/lib/asm-commons.jar
rem set CP=%CP%;%DIST%/lib/elasticsearch.jar
rem rem set CP=%CP%;%DIST%/lib/groovy-all.jar
rem set CP=%CP%;%DIST%/lib/jna.jar
rem set CP=%CP%;%DIST%/lib/jts.jar
rem set CP=%CP%;%DIST%/lib/lucene-analyzers-common.jar
rem set CP=%CP%;%DIST%/lib/lucene-core.jar
rem rem set CP=%CP%;%DIST%/lib/lucene-expressions.jar
rem set CP=%CP%;%DIST%/lib/lucene-grouping.jar
rem set CP=%CP%;%DIST%/lib/lucene-highlighter.jar
rem set CP=%CP%;%DIST%/lib/lucene-join.jar
rem set CP=%CP%;%DIST%/lib/lucene-memory.jar
rem set CP=%CP%;%DIST%/lib/lucene-misc.jar
rem set CP=%CP%;%DIST%/lib/lucene-queries.jar
rem set CP=%CP%;%DIST%/lib/lucene-queryparser.jar
rem set CP=%CP%;%DIST%/lib/lucene-sandbox.jar
rem set CP=%CP%;%DIST%/lib/lucene-spatial.jar
rem set CP=%CP%;%DIST%/lib/lucene-suggest.jar
rem set CP=%CP%;%DIST%/lib/spatial4j.jar
rem set CP=%CP%;%DIST%/lib/tika.jar
rem set CP=%CP%;%DIST%/lib/jsoup.jar

rem set CP=%CP%;%DIST%/lib/jettison.jar
rem set CP=%CP%;%DIST%/lib/guava.jar
rem set CP=%CP%;%DIST%/lib/joda-time.jar

rem set CP=%CP%;%DIST%/lib/netty.jar
rem set CP=%CP%;%DIST%/lib/transport.jar
rem set CP=%CP%;%DIST%/lib/jackson-dataformat-cbor.jar
rem set CP=%CP%;%DIST%/lib/jackson-dataformat-yaml.jar
rem set CP=%CP%;%DIST%/lib/jackson-dataformat-smile.jar
rem set CP=%CP%;%DIST%/lib/reindex-client.jar
rem set CP=%CP%;%DIST%/lib/lang-mustache-client.jar
rem set CP=%CP%;%DIST%/lib/percolator-client.jar
rem set CP=%CP%;%DIST%/lib/transport-netty3-client.jar
rem set CP=%CP%;%DIST%/lib/transport-netty4-client.jar
rem set CP=%CP%;%DIST%/lib/HdrHistogram.jar
rem set CP=%CP%;%DIST%/lib/hppc.jar
rem set CP=%CP%;%DIST%/lib/lucene-backward-codecs.jar
rem set CP=%CP%;%DIST%/lib/lucene-spatial3d.jar
rem set CP=%CP%;%DIST%/lib/lucene-spatial-extras.jar
rem set CP=%CP%;%DIST%/lib/netty-buffer.jar
rem set CP=%CP%;%DIST%/lib/netty-codec.jar
rem set CP=%CP%;%DIST%/lib/netty-codec-http.jar
rem set CP=%CP%;%DIST%/lib/netty-common.jar
rem set CP=%CP%;%DIST%/lib/netty-handler.jar
rem set CP=%CP%;%DIST%/lib/netty-resolver.jar
rem set CP=%CP%;%DIST%/lib/netty-transport.jar
rem set CP=%CP%;%DIST%/lib/t-digest.jar
rem set CP=%CP%;%DIST%/lib/log4j-api.jar
rem set CP=%CP%;%DIST%/lib/log4j-core.jar
rem set CP=%CP%;%DIST%/lib/log4j-to-slf4j.jar
rem set CP=%CP%;%DIST%/lib/search-guard-5-5.4.1-14.jar
rem set CP=%CP%;%DIST%/lib/search-guard-ssl-5.4.1-22.jar

rem rem JSQL Parser
rem set CP=%CP%;%DIST%/lib/jsqlparser.jar

rem rem Spring Security BCrypt
rem set CP=%CP%;%DIST%/lib/spring-security-core.jar

rem Custom Jars
rem FOR %%c in ("%DIST%\rscontrol\lib\*.jar") DO (
rem     IF NOT %%c==%DIST%\rscontrol\lib\resolve-control.jar (
rem         set CP=!CP!;%DIST%/rscontrol/lib/%%~nc%%~xc
rem     )
rem )

rem SDK Developed Gateway Jars
set CP=%CP%;%DIST%/gatewaylibs/*

rem JCONSOLE - make sure "hostname -i" resolves to an ipaddress not 127.0.0.1
rem set JCONSOLE=-Dcom.sun.management.jmxremote.port=5551 -Dcom.sun.management.jmxremote.authenticate=false -Dcom.sun.management.jmxremote.ssl=false

rem Eclipse Debugging
rem set DEBUGGING=-Xdebug -Xrunjdwp:transport=dt_socket,address=8002,server=y,suspend=n

rem GC Logging
rem set GCLOGGING=-XX:+PrintGCDetails -verbose:gc -Xloggc:${DIST}/rscontrol/log/gc.log

set JAVA_OPTS=

rem OWASP ESAPI
set JAVA_OPTS=%JAVA_OPTS% -Dorg.owasp.esapi.resources="%DIST%/rscontrol/config"

rem Disable JDBC 4.1 auto-commit spec compliance property to false for latest Oracle JDBC Driver
set JAVA_OPTS=%JAVA_OPTS% -Doracle.jdbc.autoCommitSpecCompliant=false

cd %DIST%

"%JDK%\bin\java" -server %JAVA_OPTS% -Djava.library.path="%DIST%/lib" -Xms512M -Xmx1024M -Xss256K -XX:+UseConcMarkSweepGC -XX:+CMSClassUnloadingEnabled -XX:+HeapDumpOnOutOfMemoryError %JCONSOLE% %DEBUGGING% %GCLOGGING% -cp "%CP%" com.resolve.rscontrol.Main 

popd

set DIST=
set JDK=
set CP=
set JAVA_OPTS=
set JCONSOLE=
set DEBUGGING=
set GCLOGGING=
