@echo off
setlocal EnableDelayedExpansion

pushd %~dp0..\..
set DIST=%CD%
call "%DIST%\rscontrol\bin\rscontrol-env.bat"
popd
set JDK=%RSCONTROLJDK%

if EXIST "%JDK%" (
	goto jre_exists
) else (
	echo given java_home folder does not exist using default resolve java
	set JDK=%DIST%\jdk
	goto finish_run
)
:jre_exists
	for /f "tokens=2-5 delims=.-_" %%j in ('"%JDK%\bin\java" -fullversion 2^>^&1') do (
			set MAJORVERSIONNEW=%%j 
			set LESSERVERSIONNEW=%%l
		)
	for /f "tokens=2-5 delims=.-_" %%j in ('"%DIST%\jdk\bin\java" -fullversion 2^>^&1') do (
			set MAJORVERSION=%%j 
			set LESSERVERSION=%%l
		)
	if 0%MAJORVERSIONNEW% NEQ 0%MAJORVERSION% (
		echo given java version is java version 8 using default resolve java
		set JDK=%DIST%\jdk
	)
	if 0%LESSERVERSIONNEW% LSS 0%LESSERVERSION% (
		echo given java version is less than resolve's installed java version using default resolve java
		set JDK=%DIST%\jdk
	)
	goto finish_run
:finish_run

set ENVPATH=%JDK%\bin
set JAVA_LIB_PATH=%DIST%\lib

rem init JAVA_OPTS
set JAVA_OPTS=
IF DEFINED JAVA_OPTS set JAVA_OPTS=%JAVA_OPTS%;
set JAVA_OPTS=%JAVA_OPTS%-Dresolve.component.type=rscontrol

rem OWASP ESAPI
set JAVA_OPTS=%JAVA_OPTS%;-Dorg.owasp.esapi.resources=%DIST%\rscontrol\config

rem Disable JDBC 4.1 auto-commit spec compliance property to for latest Oracle JDBC Driver
set JAVA_OPTS=%JAVA_OPTS%;-Doracle.jdbc.autoCommitSpecCompliant=false

set CP=%DIST%/rscontrol/config
set CP=%CP%;%DIST%/rscontrol/lib/resolve-control.jar
set CP=%CP%;%DIST%/lib
set CP=%CP%;%DIST%/lib/commons-io.jar
set CP=%CP%;%DIST%/lib/commons-lang.jar
set CP=%CP%;%DIST%/lib/commons-lang3.jar
set CP=%CP%;%DIST%/lib/commons-logging.jar
set CP=%CP%;%DIST%/lib/commons-collections.jar
set CP=%CP%;%DIST%/lib/commons-dbcp.jar
set CP=%CP%;%DIST%/lib/commons-pool.jar
set CP=%CP%;%DIST%/lib/commons-beanutils.jar
set CP=%CP%;%DIST%/lib/commons-codec.jar
set CP=%CP%;%DIST%/lib/httpcore.jar
set CP=%CP%;%DIST%/lib/httpclient.jar
set CP=%CP%;%DIST%/lib/httpmime.jar
set CP=%CP%;%DIST%/lib/log4j.jar
set CP=%CP%;%DIST%/lib/dom4j.jar
set CP=%CP%;%DIST%/lib/jaxen.jar
set CP=%CP%;%DIST%/lib/quartz.jar
set CP=%CP%;%DIST%/lib/jms.jar
set CP=%CP%;%DIST%/lib/groovy.jar
set CP=%CP%;%DIST%/lib/concurrent.jar
set CP=%CP%;%DIST%/lib/hazelcast.jar
set CP=%CP%;%DIST%/lib/persistence.jar
set CP=%CP%;%DIST%/lib/antlr.jar
set CP=%CP%;%DIST%/lib/javassist.jar
set CP=%CP%;%DIST%/lib/jta.jar
set CP=%CP%;%DIST%/lib/slf4j-api.jar
set CP=%CP%;%DIST%/lib/slf4j-log4j.jar
set CP=%CP%;%DIST%/lib/hibernate3.jar
set CP=%CP%;%DIST%/lib/hibernate-annotations.jar
set CP=%CP%;%DIST%/lib/hibernate-commons-annotations.jar
set CP=%CP%;%DIST%/lib/ejb3-persistence.jar
set CP=%CP%;%DIST%/lib/naming-factory.jar
set CP=%CP%;%DIST%/lib/naming-resources.jar
set CP=%CP%;%DIST%/lib/transactions-hibernate3.jar
set CP=%CP%;%DIST%/lib/transactions-osgi.jar
set CP=%CP%;%DIST%/lib/ezmorph.jar
set CP=%CP%;%DIST%/lib/json-lib.jar
set CP=%CP%;%DIST%/lib/ehcache.jar
set CP=%CP%;%DIST%/lib/jackson-core-asl.jar
set CP=%CP%;%DIST%/lib/jackson-core.jar
set CP=%CP%;%DIST%/lib/jackson-datatype-json-org.jar
set CP=%CP%;%DIST%/lib/jackson-mapper-asl.jar
set CP=%CP%;%DIST%/lib/org.apache.commons.fileupload.jar
set CP=%CP%;%DIST%/lib/javax.servlet.jar
set CP=%CP%;%DIST%/lib/esapi.jar
set CP=%CP%;%DIST%/lib/docx4j.jar
set CP=%CP%;%DIST%/lib/jaxb-xmldsig-core.jar
set CP=%CP%;%DIST%/lib/xmlgraphics-commons.jar

rem Database
set CP=%CP%;%DIST%/lib/mariadb-java-client.jar
set CP=%CP%;%DIST%/lib/ojdbc8.jar
set CP=%CP%;%DIST%/lib/xdb6.jar

rem RabbitMQ
set CP=%CP%;%DIST%/lib/rabbitmq-client.jar

rem ElasticSearch
set CP=%CP%;%DIST%/lib/antlr-runtime.jar
set CP=%CP%;%DIST%/lib/apache-log4j-extras.jar
set CP=%CP%;%DIST%/lib/asm.jar
set CP=%CP%;%DIST%/lib/asm-commons.jar
set CP=%CP%;%DIST%/lib/elasticsearch.jar
rem set CP=%CP%;%DIST%/lib/groovy-all.jar
set CP=%CP%;%DIST%/lib/jna.jar
set CP=%CP%;%DIST%/lib/jts.jar
set CP=%CP%;%DIST%/lib/lucene-analyzers-common.jar
set CP=%CP%;%DIST%/lib/lucene-core.jar
set CP=%CP%;%DIST%/lib/lucene-expressions.jar
set CP=%CP%;%DIST%/lib/lucene-grouping.jar
set CP=%CP%;%DIST%/lib/lucene-highlighter.jar
set CP=%CP%;%DIST%/lib/lucene-join.jar
set CP=%CP%;%DIST%/lib/lucene-memory.jar
set CP=%CP%;%DIST%/lib/lucene-misc.jar
set CP=%CP%;%DIST%/lib/lucene-queries.jar
set CP=%CP%;%DIST%/lib/lucene-queryparser.jar
set CP=%CP%;%DIST%/lib/lucene-sandbox.jar
set CP=%CP%;%DIST%/lib/lucene-spatial.jar
set CP=%CP%;%DIST%/lib/lucene-suggest.jar
set CP=%CP%;%DIST%/lib/spatial4j.jar
set CP=%CP%;%DIST%/lib/tika.jar
set CP=%CP%;%DIST%/lib/jsoup.jar

set CP=%CP%;%DIST%/lib/jettison.jar
set CP=%CP%;%DIST%/lib/joda-time.jar
set CP=%CP%;%DIST%/lib/guava.jar

rem JSQL Parser
set CP=%CP%;%DIST%/lib/jsqlparser.jar

rem Spring Security BCrypt
set CP=%CP%;%DIST%/lib/spring-security-core-3.1.0.RELEASE.jar

rem Custom Jars
FOR %%c in ("%DIST%\rscontrol\lib\*.jar") DO (
    IF %%c==%DIST%\rscontrol\lib\resolve-control.jar GOTO :LOOP
        set CP=!CP!;%DIST%/rscontrol/lib/%%~nc%%~xc
    :LOOP
    rem END LOOP
)

rem SDK Developed Gateway Jars
set CP=%CP%;%DIST%/gatewaylibs/*

rem final JAVA_OPTS
set JAVA_OPTS=%JAVA_OPTS%;-Djava.library.path='%JAVA_LIB_PATH%';

set EXECUTABLE=%DIST%\rscontrol\bin\rscontrol.exe
set SERVICE_NAME=RSControl
set PR_DISPLAYNAME=RSControl
set PR_DESCRIPTION=RSControl
set PR_INSTALL=%EXECUTABLE%
set PR_LOGPATH=%DIST%\rscontrol\log
set PR_LOGPREFIX=service
set PR_CLASSPATH=%CP%
set PR_JVM=%JDK%\bin\server\jvm.dll
set PR_STARTUP=auto
set PR_STARTCLASS=com.resolve.rscontrol.Main
set PR_STARTMETHOD=main
set PR_STARTPATH=%DIST%
set PR_STARTMODE=jvm
set PR_STOPCLASS=java.lang.System
set PR_STOPMETHOD=exit
set PR_STOPPATH=%DIST%
set PR_STOPMODE=jvm
set PR_STOPTIMEOUT=30
set PR_STARTPARAMS=

"%EXECUTABLE%" //IS//%SERVICE_NAME% ++StartParams=rscontrol


set PR_STDOUTPUT=%DIST%\rscontrol\log\stdout
set PR_STDERROR=%DIST%\rscontrol\log\stderr
set PR_JVMMS=512
set PR_JVMMX=1024
set PR_JVMSS=256
"%EXECUTABLE%" //US//%SERVICE_NAME% --JvmOptions="%JAVA_OPTS%;-XX:+CMSClassUnloadingEnabled;-XX:+UseConcMarkSweepGC;-XX:+HeapDumpOnOutOfMemoryError"

rem cleanup
set SERVICE_NAME=
set PR_DISPLAYNAME=
set PR_DESCRIPTION=
set PR_INSTALL=
set PR_LOGPATH=
set PR_LOGPREFIX=
set PR_CLASSPATH=
set PR_JVM=
set PR_STARTUP=
set PR_STARTCLASS=
set PR_STARTMETHOD=
set PR_STARTPATH=
set PR_STARTMODE=
set PR_STOPCLASS=
set PR_STOPMETHOD=
set PR_STOPPATH=
set PR_STOPMODE=
set PR_STOPTIMEOUT=
set PR_STARTPARAMS=
set PR_STDOUTPUT=
set PR_STDERROR=
set PR_JVMMS=
set PR_JVMMX=
set PR_JVMSS=
set JAVA_OPTS=

echo The 'rscontrol' service has been installed.

