#!/bin/bash

INSTANCE_NAME=rscontrol

# set os type
linux=false
sunos=false
case "`uname`" in
Linux*) linux=true;;
SunOS*) sunos=true;;
esac

# properties
BIN=`echo $0 | awk '{ print substr( $0, 0, length($0)-6 ) }'`

cd ${BIN}../..
DIST=`pwd`
. ${DIST}/rscontrol/bin/rscontrol-env.sh
JDK=${RSCONTROLJDK}
if [ -d "${JDK}" ];
then
	JAVA_RESOLVEMAJOR=`${DIST}/jdk/bin/java -version 2>&1 | awk 'NR==1{split($0,a,"."); print a[2] }'`
	JAVA_RESOLVELESSER=`${DIST}/jdk/bin/java -version 2>&1 | awk  'NR==1{gsub(/"/,"");split($0,a,"_"); print a[2] }'`
	JAVA_BLUEPRINTMAJOR=`${JDK}/bin/java -version 2>&1 | awk 'NR==1{split($0,a,"."); print a[2] }'`
	JAVA_BLUEPRINTLESSER=`${JDK}/bin/java -version 2>&1 | awk  'NR==1{gsub(/"/,"");split($0,a,"_"); print a[2] }'`
	if [ ${JAVA_RESOLVEMAJOR} -ne ${JAVA_BLUEPRINTMAJOR} ]; then
			echo "supplied java version is different than the required version, using default resolve java"
			JDK=${DIST}/jdk
	fi

	if [ ${JAVA_RESOLVELESSER} -gt ${JAVA_BLUEPRINTLESSER} ]; then
			echo "supplied java version is less than required, using default resolve java"
			JDK=${DIST}/jdk
	fi
else
	echo given java_home folder does not exist using default resolve java
	set JDK=${DIST}/jdk
fi
LOCKFILE=${DIST}/rscontrol/bin/lock

if [ -f ${LOCKFILE} ]; then
    if [ $# -eq 0 ] || [ "$1" != "1" ]; then
        echo "Lock File Detected"
        echo "Cancelling RSControl Startup"
        exit 1
    fi
fi

CP="${DIST}/rscontrol/config"
CP="${CP}:${DIST}/rscontrol/lib/resolve-control.jar"
#CP="${CP}:${DIST}/lib/*"
CP="${CP}:${DIST}/${INSTANCE_NAME}/lib/*"
# CP="${CP}:${DIST}/lib/classmate.jar"
# CP="${CP}:${DIST}/lib/commons-io.jar"
# CP="${CP}:${DIST}/lib/commons-lang.jar"
# CP="${CP}:${DIST}/lib/commons-lang3.jar"
# CP="${CP}:${DIST}/lib/commons-logging.jar"
# CP="${CP}:${DIST}/lib/commons-collections.jar"
# CP="${CP}:${DIST}/lib/commons-collections4.jar"
# CP="${CP}:${DIST}/lib/commons-dbcp.jar"
# CP="${CP}:${DIST}/lib/commons-pool.jar"
# CP="${CP}:${DIST}/lib/commons-beanutils.jar"
# CP="${CP}:${DIST}/lib/commons-codec.jar"
# CP="${CP}:${DIST}/lib/httpcore.jar"
# CP="${CP}:${DIST}/lib/httpclient.jar"
# CP="${CP}:${DIST}/lib/httpmime.jar"
# CP="${CP}:${DIST}/lib/log4j.jar"
# CP="${CP}:${DIST}/lib/dom4j.jar"
# CP="${CP}:${DIST}/lib/jaxen.jar"
# CP="${CP}:${DIST}/lib/quartz.jar"
# CP="${CP}:${DIST}/lib/oro.jar"
# CP="${CP}:${DIST}/lib/jms.jar"
# CP="${CP}:${DIST}/lib/groovy.jar"
# CP="${CP}:${DIST}/lib/concurrent.jar"
# CP="${CP}:${DIST}/lib/persistence.jar"
# CP="${CP}:${DIST}/lib/antlr.jar"
# CP="${CP}:${DIST}/lib/javassist.jar"
# CP="${CP}:${DIST}/lib/jta.jar"
# CP="${CP}:${DIST}/lib/slf4j-api.jar"
# CP="${CP}:${DIST}/lib/slf4j-log4j.jar"
# CP="${CP}:${DIST}/lib/hibernate-core.jar"
# CP="${CP}:${DIST}/lib/hibernate-commons-annotations.jar"
# CP="${CP}:${DIST}/lib/hibernate-ehcache.jar"
# CP="${CP}:${DIST}/lib/hibernate-jpa-2.1.jar"
# CP="${CP}:${DIST}/lib/naming-factory.jar"
# CP="${CP}:${DIST}/lib/naming-resources.jar"
# CP="${CP}:${DIST}/lib/transactions-hibernate4.jar"
# CP="${CP}:${DIST}/lib/transactions-osgi.jar"
# CP="${CP}:${DIST}/lib/ezmorph.jar"
# CP="${CP}:${DIST}/lib/jandex.jar"
# CP="${CP}:${DIST}/lib/json-lib.jar"
# CP="${CP}:${DIST}/lib/ehcache.jar"
# CP="${CP}:${DIST}/lib/jackson-core-asl.jar"
# CP="${CP}:${DIST}/lib/jackson-core.jar"
# CP="${CP}:${DIST}/lib/jackson-datatype-json-org.jar"
# CP="${CP}:${DIST}/lib/jackson-mapper-asl.jar"
# CP="${CP}:${DIST}/lib/jboss-logging.jar"
# CP="${CP}:${DIST}/lib/jboss-transaction-api_1.2_spec.jar"
# CP="${CP}:${DIST}/lib/org.apache.commons.fileupload.jar"
# CP="${CP}:${DIST}/lib/javax.servlet.jar"
# CP="${CP}:${DIST}/lib/esapi.jar"
# CP="${CP}:${DIST}/lib/docx4j.jar"
# CP="${CP}:${DIST}/lib/jaxb-xmldsig-core.jar"
# CP="${CP}:${DIST}/lib/xmlgraphics-commons.jar"
# CP="${CP}:${DIST}/lib/xalan.jar"
# CP="${CP}:${DIST}/lib/lib/slf4j-api.jar"

# # Database
# CP="${CP}:${DIST}/lib/mariadb-java-client.jar"
# CP="${CP}:${DIST}/lib/ojdbc8.jar"
# CP="${CP}:${DIST}/lib/xdb6.jar"

# # RabbitMQ
# CP="${CP}:${DIST}/lib/rabbitmq-client.jar"

# CP="${CP}:${DIST}/lib/jettison.jar"
# CP="${CP}:${DIST}/lib/joda-time.jar"
# CP="${CP}:${DIST}/lib/guava.jar"

# # ElasticSearch
# CP="${CP}:${DIST}/lib/antlr-runtime.jar"
# CP="${CP}:${DIST}/lib/apache-log4j-extras.jar"
# CP="${CP}:${DIST}/lib/asm.jar"
# CP="${CP}:${DIST}/lib/asm-commons.jar"
# CP="${CP}:${DIST}/lib/elasticsearch.jar"
# # CP="${CP}:${DIST}/lib/groovy-all.jar"
# CP="${CP}:${DIST}/lib/jna.jar"
# CP="${CP}:${DIST}/lib/jts.jar"
# CP="${CP}:${DIST}/lib/lucene-analyzers-common.jar"
# # CP="${CP}:${DIST}/lib/lucene-codecs.jar"
# CP="${CP}:${DIST}/lib/lucene-core.jar"
# #CP="${CP}:${DIST}/lib/lucene-expressions.jar"
# CP="${CP}:${DIST}/lib/lucene-grouping.jar"
# CP="${CP}:${DIST}/lib/lucene-highlighter.jar"
# CP="${CP}:${DIST}/lib/lucene-join.jar"
# CP="${CP}:${DIST}/lib/lucene-memory.jar"
# CP="${CP}:${DIST}/lib/lucene-misc.jar"
# CP="${CP}:${DIST}/lib/lucene-queries.jar"
# CP="${CP}:${DIST}/lib/lucene-queryparser.jar"
# CP="${CP}:${DIST}/lib/lucene-sandbox.jar"
# CP="${CP}:${DIST}/lib/lucene-spatial.jar"
# CP="${CP}:${DIST}/lib/lucene-suggest.jar"
# CP="${CP}:${DIST}/lib/spatial4j.jar"
# CP="${CP}:${DIST}/lib/tika.jar"
# CP="${CP}:${DIST}/lib/jsoup.jar"

# CP="${CP}:${DIST}/lib/netty.jar"
# CP="${CP}:${DIST}/lib/transport.jar"
# CP="${CP}:${DIST}/lib/jackson-dataformat-cbor.jar"
# CP="${CP}:${DIST}/lib/jackson-dataformat-yaml.jar"
# CP="${CP}:${DIST}/lib/jackson-dataformat-smile.jar"
# CP="${CP}:${DIST}/lib/reindex-client.jar"
# CP="${CP}:${DIST}/lib/lang-mustache-client.jar"
# CP="${CP}:${DIST}/lib/percolator-client.jar"
# CP="${CP}:${DIST}/lib/transport-netty3-client.jar"
# CP="${CP}:${DIST}/lib/transport-netty4-client.jar"
# CP="${CP}:${DIST}/lib/HdrHistogram.jar"
# CP="${CP}:${DIST}/lib/hppc.jar"
# CP="${CP}:${DIST}/lib/lucene-backward-codecs.jar"
# CP="${CP}:${DIST}/lib/lucene-spatial3d.jar"
# CP="${CP}:${DIST}/lib/lucene-spatial-extras.jar"
# CP="${CP}:${DIST}/lib/netty-buffer.jar"
# CP="${CP}:${DIST}/lib/netty-codec.jar"
# CP="${CP}:${DIST}/lib/netty-codec-http.jar"
# CP="${CP}:${DIST}/lib/netty-common.jar"
# CP="${CP}:${DIST}/lib/netty-handler.jar"
# CP="${CP}:${DIST}/lib/netty-resolver.jar"
# CP="${CP}:${DIST}/lib/netty-transport.jar"
# CP="${CP}:${DIST}/lib/t-digest.jar"
# CP="${CP}:${DIST}/lib/log4j-api.jar"
# CP="${CP}:${DIST}/lib/log4j-core.jar"
# CP="${CP}:${DIST}/lib/log4j-to-slf4j.jar"
# CP="${CP}:${DIST}/lib/search-guard-5-5.4.1-14.jar"
# CP="${CP}:${DIST}/lib/search-guard-ssl-5.4.1-22.jar"

# # JSQL Parser
# CP="${CP}:${DIST}/lib/jsqlparser.jar"

# # Spring Security BCrypt
# CP="${CP}:${DIST}/lib/spring-security-core.jar"

# # rscontrol lib classes
# for FILE in `ls $DIST/rscontrol/lib/`
# do
#     case $FILE in
#     *.jar)
#         if [ $FILE != "resolve-control.jar" ]; then
#             CP="${CP}:${DIST}/rscontrol/lib/$FILE"
#         fi
#     ;;
#     esac
# done

# SDK Developed Gateway Jars
CP="${CP}:${DIST}/gatewaylibs/*"

# Remove atomikos tm files
rm ${DIST}/rscontrol/log/*.epoch 2>/dev/null
rm ${DIST}/rscontrol/log/tmlog.lck 2>/dev/null
rm ${DIST}/rscontrol/log/tmlog*.log 2>/dev/null

# JCONSOLE - make sure "hostname -i" resolves to an ipaddress not 127.0.0.1
#JCONSOLE="-Dcom.sun.management.jmxremote.port=5551 -Dcom.sun.management.jmxremote.authenticate=false -Dcom.sun.management.jmxremote.ssl=false"

# Eclipse Debugging
#DEBUGGING="-Xdebug -Xrunjdwp:transport=dt_socket,address=8002,server=y,suspend=n"

# GC Logging
#GCLOGGING="-XX:+PrintGCDetails -verbose:gc -Xloggc:${DIST}/rscontrol/log/gc.log"


JAVA_OPTS=

# OWASP ESAPI
JAVA_OPTS="${JAVA_OPTS} -Dorg.owasp.esapi.resources=${DIST}/rscontrol/config"

#Disable JDBC 4.1 auto-commit spec compliance property to false for latest Oracle JDBC Driver
JAVA_OPTS="${JAVA_OPTS} -Doracle.jdbc.autoCommitSpecCompliant=false"

# execute
if $linux; then
    JAVA_EXEC=${JDK}/bin/java
else
    JAVA_EXEC=${JDK}/bin/sparcv9/java
    JAVA_OPTS="${JAVA_OPTS} -d64"
fi

exec "${JAVA_EXEC}" -Djava.io.tmpdir=${DIST}/tmp ${JAVA_OPTS} -Djava.library.path="${DIST}/lib" -server -Xms512M -Xmx1024M -Xss256K -XX:+UseConcMarkSweepGC -XX:+CMSClassUnloadingEnabled -XX:+HeapDumpOnOutOfMemoryError ${JCONSOLE} ${DEBUGGING} ${GCLOGGING} -cp "${CP}" com.resolve.rscontrol.Main  >> ${DIST}/rscontrol/log/stdout 2>&1 &

PID=$!
echo "${PID}" > ${LOCKFILE}
echo "Started RSControl pid: ${PID}"
