CREATE TABLE resolve_event  (
    sys_id VARCHAR(32) NOT NULL , 
    u_value VARCHAR(1024) WITH DEFAULT NULL , 
    sys_updated_by VARCHAR(40) WITH DEFAULT NULL , 
    sys_updated_on TIMESTAMP WITH DEFAULT NULL , 
    sys_created_by VARCHAR(40) WITH DEFAULT NULL , 
    sys_created_on TIMESTAMP WITH DEFAULT NULL , 
    sys_mod_count INTEGER WITH DEFAULT 0,
    PRIMARY KEY (sys_id)
); 

CREATE TABLE resolve_sequence_generator (
    sys_id VARCHAR(32) NOT NULL,
    u_name VARCHAR(100) WITH DEFAULT NULL,
    u_number INTEGER WITH DEFAULT 0,
    PRIMARY KEY (sys_id)
);
