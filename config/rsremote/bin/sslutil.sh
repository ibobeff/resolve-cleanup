#!/bin/bash 

# properties
DIST=../../
JRE=$DIST/jdk
JAVA_LIB_PATH=$DIST/lib

"$JRE/bin/java" -cp "../lib/resolve-remote.jar:$JAVA_LIB_PATH/json-lib.jar:$JAVA_LIB_PATH/commons-lang.jar:$JAVA_LIB_PATH/log4j.jar" com.resolve.util.SSLUtils "$@";

#if ["$1" == ""] || ["$2" == ""] || ["$3" == ""] || ["$4" == ""]; then
#	echo "Usage: ./sslutil.sh keystore keystorepassword host ssl-port" 
#	echo "Example: ./sslutil.sh "/opt/resolve/jre/lib/security/cacerts" changeit tsrm-server 443 
#else
#      #"$JRE/bin/java" -cp "../lib/resolve-remote.jar:$JAVA_LIB_PATH/json-lib.jar:$JAVA_LIB_PATH/commons-lang.jar:$JAVA_LIB_PATH/log4j.jar" com.resolve.util.SSLUtils "$@";
#      "$JRE/bin/java" -cp "../lib/resolve-remote.jar:$JAVA_LIB_PATH/json-lib.jar:$JAVA_LIB_PATH/commons-lang.jar:$JAVA_LIB_PATH/log4j.jar" com.resolve.util.SSLUtils;
#fi
