@echo off
if "%1"=="" GOTO END
if "%2"=="" GOTO END
if "%3"=="" GOTO END
if "%4"=="" GOTO END
"%JAVA_HOME%/bin/java" -cp "..\..\lib\log4j.jar;..\lib\resolve-remote.jar;" com.resolve.util.SSLUtils %1 %2 %3 %4
exit /B 0
:END 
echo Usage: sslutil.bat keystore keystorepassword host ssl-port 
echo Example: sslutil.bat "c:\Program Files\Resolve\jdk\lib\security\cacerts" changeit tsrm-server 443 
exit /B 1
