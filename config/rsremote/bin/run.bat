@echo off
setlocal EnableDelayedExpansion

pushd %~dp0\..\..
set DIST=%CD%
call "%DIST%\rsremote\bin\rsremote-env.bat"
set JRE=%RSREMOTEJDK%
if EXIST "%JRE%" (
	goto jre_exists
) ELSE (
	echo given java_home folder does not exist using default resolve java
	set JRE=%DIST%/jdk
	goto finish_run
)
set ENVPATH=%JRE%\bin
set JAVA_LIB_PATH=%DIST%\lib
:jre_exists
	for /f "tokens=2-5 delims=.-_" %%j in ('"%JRE%\bin\java" -fullversion 2^>^&1') do (
			set MAJORVERSIONNEW=%%j 
			set LESSERVERSIONNEW=%%l
		)
	for /f "tokens=2-5 delims=.-_" %%j in ('"%DIST%\jdk\bin\java" -fullversion 2^>^&1') do (
			set MAJORVERSION=%%j 
			set LESSERVERSION=%%l
		)
	if 0%MAJORVERSIONNEW% NEQ 0%MAJORVERSION% (
		echo given java version is java version 8 using default resolve java
		set JRE=%DIST%\jdk
	)
	if 0%LESSERVERSIONNEW% LSS 0%LESSERVERSION% (
		echo given java version is less than resolve's installed java version using default resolve java
		set JRE=%DIST%\jdk\
	)
	goto finish_run
:finish_run
rem execute
set JAVA_OPTS=
set JAVA_OPTS=%JAVA_OPTS% -Djava.library.path="%JAVA_LIB_PATH%"
set JAVA_OPTS=%JAVA_OPTS% -Dorg.apache.cxf.Logger=org.apache.cxf.common.logging.Log4jLogger

rem OWASP ESAPI
set JAVA_OPTS=%JAVA_OPTS% -Dorg.owasp.esapi.resources="%DIST%\rsremote\config"

rem *** NETCOOL GATEWAY ***
set NETCOOL=FALSE

rem *** REMEDY GATEWAY ***
rem NOTE: copy all remedy jars and dll files to %DIST%/lib/remedy
set REMEDY=FALSE

rem *** REMEDYX GATEWAY ***
rem NOTE: copy all the platform specific remedy libraries (jar/dll/so) to %DIST%/lib/remedyx
set REMEDYX=FALSE

rem *** MICROSOFT ADAPTER ***
set MSPOI=FALSE

rem *** DB GATEWAY ***
set DBGW=FALSE

rem *** MySQL ADAPTER ***
set MYSQL=FALSE

rem *** Oracle ADAPTER ***
set ORACLE=FALSE

rem *** DB2 ADAPTER ***
set DB2=FALSE

rem *** Microsoft SQL ADAPTER ***
set MSSQL=FALSE

rem *** PostgreSQL ADAPTER ***
set POSTGRESQL=FALSE

rem *** Informix ADAPTER ***
set INFORMIX=FALSE

rem *** Sybase ADAPTER ***
set SYBASE=FALSE

rem *** TN3270 ADAPTER ***
set TN3270=FALSE

rem *** TN5250 ADAPTER ***
set TN5250=FALSE

rem *** VT ADAPTER ***
set VT=FALSE

rem *** HTML ADAPTER ***
set HTMLCONNECT=FALSE

rem *** PDF ADAPTER ***
set PDF=FALSE

rem *** EXCHANGE GATEWAY ***
set EXCHANGE=FALSE

rem *** ITNCM ADAPTER ***
rem NOTE: copy all ITNCM jars and dll files to %DIST%/lib/itncm
set ITNCM=FALSE

rem *** TSRM GATEWAY ***
set TSRM=FALSE

rem *** XMPP GATEWAY ***
set XMPP=FALSE

rem *** SNMP GATEWAY ***
set SNMP=FALSE

rem *** ServiceNow GATEWAY ***
set SERVICENOW=FALSE

rem *** HP Operations Manager GATEWAY ***
set HPOM=FALSE

rem *** EmailConnect2 ***
set EMAILCONNECT2=FALSE

rem *** EWS GATEWAY ***
set EWS=FALSE

rem *** WSLITE Adapter ***
set WSLITECONNECT=FALSE

rem *** EMAIL GATEWAY ***
set EMAIL=FALSE

rem *** FTP Adapter
set FTPCONNECT=FALSE

rem *** Network ADAPTER ***
set NETWORKCONNECT=FALSE

rem *** HP Service Manager GATEWAY ***
set HPSM=FALSE

rem *** HTTP GATEWAY ***
set HTTP=FALSE

rem default classes
set CP=%DIST%/rsremote/service/proxy
set CP=%CP%;%DIST%/rsremote/lib/resolve-remote.jar
set CP=%CP%;%DIST%/rsremote/lib/*

rem set CP=%CP%;%DIST%/lib/*
rem set CP=%CP%;%DIST%/lib/commons-io.jar

rem set CP=%CP%;%DIST%/lib/commons-lang.jar
rem set CP=%CP%;%DIST%/lib/commons-lang3.jar
rem set CP=%CP%;%DIST%/lib/commons-logging.jar
rem set CP=%CP%;%DIST%/lib/commons-beanutils.jar
rem set CP=%CP%;%DIST%/lib/commons-collections.jar
rem set CP=%CP%;%DIST%/lib/commons-collections4.jar
rem set CP=%CP%;%DIST%/lib/commons-codec.jar
rem set CP=%CP%;%DIST%/lib/log4j.jar
rem set CP=%CP%;%DIST%/lib/dom4j.jar
rem set CP=%CP%;%DIST%/lib/jaxen.jar
rem set CP=%CP%;%DIST%/lib/jms.jar
rem set CP=%CP%;%DIST%/lib/groovy.jar
rem set CP=%CP%;%DIST%/lib/concurrent.jar
rem set CP=%CP%;%DIST%/lib/rabbitmq-client.jar
rem set CP=%CP%;%DIST%/lib/ezmorph.jar
rem set CP=%CP%;%DIST%/lib/json-lib.jar
rem set CP=%CP%;%DIST%/lib/org.apache.commons.fileupload.jar
rem rem set CP=%CP%;%DIST%/lib/javax.servlet.jar
rem set CP=%CP%;%DIST%/lib/servlet-api.jar
rem set CP=%CP%;%DIST%/lib/esapi.jar
rem set CP=%CP%;%DIST%/lib/joda-time.jar
rem set CP=%CP%;%DIST%/lib/docx4j.jar
rem set CP=%CP%;%DIST%/lib/jaxb-xmldsig-core.jar
rem set CP=%CP%;%DIST%/lib/xmlgraphics-commons.jar
rem set CP=%CP%;%DIST%/lib/xalan.jar
rem set CP=%CP%;%DIST%/lib/slf4j-api.jar
rem set CP=%CP%;%DIST%/lib/jackson-core.jar
rem set CP=%CP%;%DIST%/lib/jackson-core-asl.jar
rem set CP=%CP%;%DIST%/lib/jackson-datatype-json-org.jar
rem set CP=%CP%;%DIST%/lib/jackson-mapper-asl.jar
rem set CP=%CP%;%DIST%/lib/hibernate-jpa-2.1.jar

rem rem ElasticSearch
rem set CP=%CP%;%DIST%/lib/antlr-runtime.jar
rem set CP=%CP%;%DIST%/lib/apache-log4j-extras.jar
rem set CP=%CP%;%DIST%/lib/asm.jar
rem set CP=%CP%;%DIST%/lib/asm-commons.jar
rem set CP=%CP%;%DIST%/lib/elasticsearch.jar
rem rem set CP=%CP%;%DIST%/lib/groovy-all.jar
rem set CP=%CP%;%DIST%/lib/jna.jar
rem set CP=%CP%;%DIST%/lib/jts.jar
rem set CP=%CP%;%DIST%/lib/lucene-analyzers-common.jar
rem set CP=%CP%;%DIST%/lib/lucene-core.jar
rem set CP=%CP%;%DIST%/lib/lucene-expressions.jar
rem set CP=%CP%;%DIST%/lib/lucene-grouping.jar
rem set CP=%CP%;%DIST%/lib/lucene-highlighter.jar
rem set CP=%CP%;%DIST%/lib/lucene-join.jar
rem set CP=%CP%;%DIST%/lib/lucene-memory.jar
rem set CP=%CP%;%DIST%/lib/lucene-misc.jar
rem set CP=%CP%;%DIST%/lib/lucene-queries.jar
rem set CP=%CP%;%DIST%/lib/lucene-queryparser.jar
rem set CP=%CP%;%DIST%/lib/lucene-sandbox.jar
rem set CP=%CP%;%DIST%/lib/lucene-spatial.jar
rem set CP=%CP%;%DIST%/lib/lucene-suggest.jar
rem set CP=%CP%;%DIST%/lib/spatial4j.jar
rem rem set CP=%CP%;%DIST%/lib/tika.jar
rem set CP=%CP%;%DIST%/lib/jsoup.jar

rem rem connectors
rem set CP=%CP%;%DIST%/lib/ganymed-ssh2.jar
rem set CP=%CP%;%DIST%/lib/sshj-0.21.1.jar
rem set CP=%CP%;%DIST%/lib/bcpkix-jdk15on-157.jar
rem set CP=%CP%;%DIST%/lib/bcprov-ext-jdk15on-157.jar
rem set CP=%CP%;%DIST%/lib/bcprov-jdk15on-157.jar
rem set CP=%CP%;%DIST%/lib/eddsa-0.1.0.jar
rem set CP=%CP%;%DIST%/lib/jzlib-1.1.3.jar
rem set CP=%CP%;%DIST%/lib/slf4j-log4j.jar
rem set CP=%CP%;%DIST%/lib/commons-codec.jar
rem set CP=%CP%;%DIST%/lib/commons-net.jar
rem set CP=%CP%;%DIST%/lib/httpcore.jar
rem set CP=%CP%;%DIST%/lib/httpclient.jar
rem set CP=%CP%;%DIST%/lib/httpmime.jar
rem set CP=%CP%;%DIST%/lib/httpclient-win.jar
rem set CP=%CP%;%DIST%/lib/fluent-hc.jar
rem set CP=%CP%;%DIST%/lib/jna.jar
rem set CP=%CP%;%DIST%/lib/jna-platform.jar
rem set CP=%CP%;%DIST%/lib/httpclient-cache.jar
rem set CP=%CP%;%DIST%/lib/mail.jar
rem set CP=%CP%;%DIST%/lib/activation.jar
rem rem set CP=%CP%;%DIST%/lib/groovyws.jar
rem set CP=%CP%;%DIST%/lib/http-builder.jar

rem rem default gateway libraries
rem set CP=%CP%;%DIST%/lib/antlr.jar
rem set CP=%CP%;%DIST%/lib/antlrworks.jar
rem set CP=%CP%;%DIST%/lib/ldap.jar

rem rem MCP
rem set CP=%CP%;%DIST%/lib/guava.jar
rem set CP=%CP%;%DIST%/lib/org.xbill.dns.jar

rem rem receivers
rem set CP=%CP%;%DIST%/lib/snmp.jar

rem rem JSQL Parser
rem set CP=%CP%;%DIST%/lib/jsqlparser.jar

rem rem Gateway metrics logging
rem set CP=%CP%;%DIST%/lib/gson.jar

rem NetcoolGateway
rem IF %NETCOOL%==FALSE GOTO :END_NETCOOL
rem     set CP=%CP%;%DIST%/lib/jconn3.jar
rem     set CP=%CP%;%DIST%/lib/commons-pool.jar
rem :END_NETCOOL

rem rem RemedyGateway
rem IF %REMEDY%==FALSE GOTO :END_REMEDY
rem     set CP=%CP%;%DIST%/lib/remedy/arapi70.jar
rem     set CP=%CP%;%DIST%/lib/remedy/arutil70.jar
rem     set CP=%CP%;%DIST%/lib/commons-pool.jar

rem     set JAVA_LIB_PATH=%JAVA_LIB_PATH%;%DIST%/lib/remedy
rem     set ENVPATH=%ENVPATH%;%DIST%/lib/remedy
rem :END_REMEDY

rem rem RemedyxGateway
rem IF %REMEDYX%==FALSE GOTO :END_REMEDYX
rem     set CP=%CP%;%DIST%/lib/remedyx/arapi7604.jar

rem     set JAVA_LIB_PATH=%JAVA_LIB_PATH%;%DIST%/lib/remedyx
rem     set ENVPATH=%ENVPATH%;%DIST%/lib/remedyx
rem :END_REMEDYX

rem rem Microsoft Adapter
rem IF %MSPOI%==FALSE GOTO :END_MSPOI
rem     set CP=%CP%;%DIST%/lib/poi.jar
rem     set CP=%CP%;%DIST%/lib/poi-ooxml.jar
rem     set CP=%CP%;%DIST%/lib/geronimo-stax-api.jar
rem     set CP=%CP%;%DIST%/lib/xmlbeans.jar
rem     set CP=%CP%;%DIST%/lib/ooxml-schemas.jar

rem set JAVA_OPTS=%JAVA_OPTS% -Djava.awt.headless=true

rem :END_MSPOI

rem rem DB Gateway
rem IF %DBGW%==FALSE GOTO :END_DBGW
rem     set CP=%CP%;%DIST%/lib/commons-pool.jar
rem     set CP=%CP%;%DIST%/lib/commons-dbcp.jar
rem :END_DBGW

rem rem MySQL Adapter
rem IF %MYSQL%==FALSE GOTO :END_MYSQL
rem     set CP=%CP%;%DIST%/lib/mariadb-java-client.jar
rem :END_MYSQL

rem rem Oracle Adapter
rem IF %ORACLE%==FALSE GOTO :END_ORACLE
rem     set CP=%CP%;%DIST%/lib/ojdbc8.jar
rem     set CP=%CP%;%DIST%/lib/xdb6.jar
rem :END_ORACLE

rem rem DB2 Adapter
rem IF %DB2%==FALSE GOTO :END_DB2
rem     set CP=%CP%;%DIST%/lib/db2jcc.jar
rem :END_DB2

rem rem Microsoft SQL Adapter
rem IF %MSSQL%==FALSE GOTO :END_MSSQL
rem     set CP=%CP%;%DIST%/lib/sqljdbc4.jar
rem :END_MSSQL

rem rem PostgreSQL SQL Adapter
rem IF %POSTGRESQL%==FALSE GOTO :END_POSTGRESQL
rem     set CP=%CP%;%DIST%/lib/postgresql.jar
rem :END_POSTGRESQL

rem rem Informix SQL Adapter
rem IF %INFORMIX%==FALSE GOTO :END_INFORMIX
rem     set CP=%CP%;%DIST%/lib/ifxjdbc.jar
rem :END_INFORMIX

rem rem Sybase SQL Adapter
rem IF %SYBASE%==FALSE GOTO :END_SYBASE
rem     set CP=%CP%;%DIST%/lib/jconn3.jar
rem :END_SYBASE

rem rem TN3270 Adapter
rem IF %TN3270%==FALSE GOTO :END_TN3270
rem     set CP=%CP%;%DIST%/lib/jagacy/jagacy3270.jar
rem :END_TN3270

rem rem TN5250 Adapter
rem IF %TN5250%==FALSE GOTO :END_TN5250
rem     set CP=%CP%;%DIST%/lib/jagacy/IcomWJC.jar
rem :END_TN5250

rem rem VT Adapter
rem IF %VT%==FALSE GOTO :END_VT
rem     set CP=%CP%;%DIST%/lib/jagacy/jagacyVT.jar
rem :END_VT

rem rem HtmlConnect Adapter
rem IF %HTMLCONNECT%==FALSE GOTO :END_HTMLCONNECT
rem     set CP=%CP%;%DIST%/lib/cssparser.jar
rem     set CP=%CP%;%DIST%/lib/nekohtml.jar
rem     set CP=%CP%;%DIST%/lib/sac.jar
rem     set CP=%CP%;%DIST%/lib/httpcore.jar
rem     set CP=%CP%;%DIST%/lib/httpmime.jar
rem     set CP=%CP%;%DIST%/lib/htmlunit-core-js.jar
rem     set CP=%CP%;%DIST%/lib/htmlunit.jar
rem     set CP=%CP%;%DIST%/lib/serializer.jar
rem     set CP=%CP%;%DIST%/lib/xalan.jar
rem     set CP=%CP%;%DIST%/lib/xercesImpl.jar
rem     set CP=%CP%;%DIST%/lib/xml-apis.jar
rem 	set CP=%CP%;%DIST%/lib/websocket-api.jar
rem 	set CP=%CP%;%DIST%/lib/websocket-client.jar
rem 	set CP=%CP%;%DIST%/lib/websocket-common.jar
rem :END_HTMLCONNECT

rem rem PDF Adapter
rem IF %PDF%==FALSE GOTO :END_PDF
rem     set CP=%CP%;%DIST%/lib/itext.jar
rem     set CP=%CP%;%DIST%/lib/pdfbox.jar
rem     set CP=%CP%;%DIST%/lib/fontbox.jar
rem     set CP=%CP%;%DIST%/lib/jempbox.jar
rem :END_PDF

rem rem Exchange Gateway
rem IF %EXCHANGE%==FALSE GOTO :END_EXCHANGE
rem     set CP=%CP%;%DIST%/lib/moonrug/moonrug.jar
rem     set CP=%CP%;%DIST%/lib/moonrug/apache-mime4j.jar
rem     set CP=%CP%;%DIST%/lib/moonrug/xpp3.jar
rem     set JAVA_OPTS=%JAVA_OPTS% -Dmoonrug.license="%DIST%/lib/moonrug/moonrug.xml"
rem :END_EXCHANGE

rem rem ITNCM Gateway
rem IF %ITNCM%==FALSE GOTO :END_ITNCM
rem     set CP=%CP%;%DIST%/lib/itncm/appframework.jar
rem     set CP=%CP%;%DIST%/lib/itncm/presentation.jar
rem     set CP=%CP%;%DIST%/lib/itncm/common.jar
rem     set CP=%CP%;%DIST%/lib/itncm/driverinterface.jar
rem     set CP=%CP%;%DIST%/lib/itncm/intelliden-soap-apis.jar
rem     set CP=%CP%;%DIST%/lib/itncm/ibm_tivoli-events.jar
rem     set CP=%CP%;%DIST%/lib/itncm/xstream-1.2.2.jar
rem rem set CP=%CP%;%DIST%/lib/itncm/httpclient-4.0.1.jar
rem rem set CP=%CP%;%DIST%/lib/itncm/httpcore-4.0.1.jar
rem     set CP=%CP%;%DIST%/lib/httpcore.jar
rem     set CP=%CP%;%DIST%/lib/itncm/jdom-1.0beta8.jar
rem     set CP=%CP%;%DIST%/lib/itncm/mibble-commercial-parser-2.9.2.jar
rem rem set CP=%CP%;%DIST%/lib/itncm/ojdbc14.jar
rem rem set CP=%CP%;%DIST%/lib/itncm/commons-codec-1.4.jar
rem rem set CP=%CP%;%DIST%/lib/itncm/commons-logging-1.1.jar
rem :END_ITNCM

rem rem TSRM Gateway
rem IF %TSRM%==FALSE GOTO :END_TSRM
rem     set CP=%CP%;%DIST%/lib/antlr.jar
rem     set CP=%CP%;%DIST%/lib/antlrworks.jar
rem :END_TSRM

rem rem XMPP Gateway
rem IF %XMPP%==FALSE GOTO :END_XMPP
rem     set CP=%CP%;%DIST%/lib/smack.jar
rem     set CP=%CP%;%DIST%/lib/smackx.jar
rem     set CP=%CP%;%DIST%/lib/smackx-debug.jar
rem     set CP=%CP%;%DIST%/lib/smackx-jingle.jar
rem :END_XMPP

rem rem SNMP Gateway
rem IF %SNMP%==FALSE GOTO :END_SNMP
rem     set CP=%CP%;%DIST%/lib/snmp4j.jar
rem :END_SNMP

rem rem ServiceNow Gateway
rem rem IF %SERVICENOW%==FALSE GOTO :END_SERVICENOW
rem rem :END_SERVICENOW

rem rem HPOM Gateway
rem IF %HPOM%==FALSE GOTO :END_HPOM
rem 	set CP=%CP%;%DIST%/lib/hpom-client.jar
rem 	set CP=%CP%;%DIST%/lib/addressing.jar
rem 	set CP=%CP%;%DIST%/lib/commons-codec.jar
rem 	set CP=%CP%;%DIST%/lib/commons-httpclient.jar
rem 	set CP=%CP%;%DIST%/lib/commons-logging.jar
rem 	set CP=%CP%;%DIST%/lib/httpcore.jar
rem 	set CP=%CP%;%DIST%/lib/xercesImpl.jar
rem 	set CP=%CP%;%DIST%/lib/mail.jar
rem 	set CP=%CP%;%DIST%/lib/axiom-api.jar
rem 	set CP=%CP%;%DIST%/lib/axiom-impl.jar
rem 	set CP=%CP%;%DIST%/lib/axis2-adb.jar
rem 	set CP=%CP%;%DIST%/lib/axis2-kernel.jar
rem 	set CP=%CP%;%DIST%/lib/axis2-transport-http.jar
rem 	set CP=%CP%;%DIST%/lib/axis2-transport-local.jar
rem 	set CP=%CP%;%DIST%/lib/neethi.jar
rem 	set CP=%CP%;%DIST%/lib/woden-api.jar
rem 	set CP=%CP%;%DIST%/lib/wsdl4j.jar
rem 	set CP=%CP%;%DIST%/lib/xalan.jar
rem 	set CP=%CP%;%DIST%/lib/XmlSchema.jar
rem :END_HPOM

rem rem Email Adapter 2
rem IF %EMAILCONNECT2%==FALSE GOTO :END_EMAILCONNECT2
rem     set CP=%CP%;%DIST%/lib/commons-email.jar
rem :END_EMAILCONNECT2

rem rem EWS Gateway
rem IF %EWS%==FALSE GOTO :END_EWS
rem     set CP=%CP%;%DIST%/lib/EWSAPI.jar
rem     set CP=%CP%;%DIST%/lib/jcifs.jar
rem     set CP=%CP%;%DIST%/lib/commons-httpclient.jar
rem :END_EWS

rem rem WSLITE CONNECT Adapter
rem IF %WSLITECONNECT%==FALSE GOTO :END_WSLITECONNECT
rem     set CP=%CP%;%DIST%/lib/groovy-wslite.jar
rem :END_WSLITECONNECT

rem rem Email Gateway
rem IF %EMAIL%==FALSE GOTO :END_EMAIL
rem     set CP=%CP%;%DIST%/lib/commons-email.jar
rem :END_EMAIL

rem rem FTP Adapter
rem IF %FTPCONNECT%==FALSE GOTO :END_FTPCONNECT
rem     set CP=%CP%;%DIST%/lib/ftp4j.jar
rem :END_FTPCONNECT

rem rem HPSM Gateway
rem IF %HPSM%==FALSE GOTO :END_HPSM
rem     set CP=%CP%;%DIST%/lib/hpsm-client.jar
rem :END_HPSM

rem rem HTTP Gateway
rem IF %HTTP%==FALSE GOTO :END_HTTP
rem     set CP=%CP%;%DIST%/lib/jetty-continuation.jar
rem     set CP=%CP%;%DIST%/lib/jetty-http.jar
rem     set CP=%CP%;%DIST%/lib/jetty-io.jar
rem     set CP=%CP%;%DIST%/lib/jetty-security.jar
rem     set CP=%CP%;%DIST%/lib/jetty-server.jar
rem     set CP=%CP%;%DIST%/lib/jetty-servlet.jar
rem     set CP=%CP%;%DIST%/lib/jetty-servlets.jar
rem     set CP=%CP%;%DIST%/lib/jetty-util.jar
rem     set CP=%CP%;%DIST%/lib/jetty-webapp.jar
rem     set CP=%CP%;%DIST%/lib/websocket-api.jar
rem     set CP=%CP%;%DIST%/lib/websocket-client.jar
rem     set CP=%CP%;%DIST%/lib/websocket-common.jar
rem :END_HTTP

rem rem Custom Jars
rem FOR %%c in ("%DIST%\rsremote\lib\*.jar") DO (
rem     IF %%c==%DIST%\rsremote\lib\resolve-remote.jar GOTO :LOOP
rem         set CP=!CP!;%DIST%/rsremote/lib/%%~nc%%~xc
rem     :LOOP
rem     rem END LOOP
rem )

set CP=%CP%;%DIST%/lib/*
rem SDK Developed Gateway Jars
set CP=%CP%;%DIST%/gatewaylibs/*

rem external classpath
set EXTERNALCLASSPATHS=
set CP=%CP%;%EXTERNALCLASSPATHS%

rem JCONSOLE - make sure "hostname -i" resolves to an ipaddress not 127.0.0.1
rem set JCONSOLE=-Dcom.sun.management.jmxremote.port=5551 -Dcom.sun.management.jmxremote.authenticate=false -Dcom.sun.management.jmxremote.ssl=false

rem Eclipse Debugging
rem set DEBUGGING=-Xdebug -Xrunjdwp:transport=dt_socket,address=8001,server=y,suspend=n

rem GC Logging
rem set GCLOGGING=-XX:+PrintGCDetails -verbose:gc -Xloggc:${DIST}/rscontrol/log/gc.log

rem execute
cd %DIST%

set PATH=%PATH%;%ENVPATH%
"%JRE%\bin\java" %JAVA_OPTS% %JCONSOLE% %DEBUGGING% %GCLOGGING% -server -Xms256M -Xmx512M -Xss256K -XX:+UseConcMarkSweepGC -XX:+CMSClassUnloadingEnabled -XX:+HeapDumpOnOutOfMemoryError -cp "%CP%" com.resolve.rsremote.Main rsremote

popd

set DIST=
set JRE=
set ENVPATH=
set JAVA_LIB_PATH=
set JAVA_OPTS=
set JCONSOLE=
set DEBUGGING=
set GCLOGGING=
set NETCOOL=
set REMEDY=
set REMEDYX=
set MSPOI=
set DBGW=
set MYSQL=
set ORACLE=
set DB2=
set MSSQL=
set POSTGRESQL=
set INFORMIX=
set SYBASE=
set TN3270=
set TN5250=
set VT=
set HTMLCONNECT=
set PDF=
set EXCHANGE=
set ITNCM=
set TSRM=
set XMPP=
set SNMP=
set SERVICENOW=
set HPOM=
set EMAILCONNECT2=
set EWS=
set WSLITECONNECT=
set EMAIL=
set FTPCONNECT=
set NETWORKCONNECT=
set HPSM=
