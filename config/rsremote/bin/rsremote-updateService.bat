rem *** NETCOOL GATEWAY ***
set NETCOOL=FALSE

rem *** REMEDY GATEWAY ***
rem NOTE: copy all remedy 6.3 jars (not 7.0) and dll files to %DIST%/lib/remedy
set REMEDY=FALSE

rem *** REMEDYX GATEWAY ***
rem NOTE: copy all the platform specific remedy libraries (jar/dll/so) to %DIST%/lib/remedyx
set REMEDYX=FALSE

rem *** MICROSOFT ADAPTER ***
set MSPOI=FALSE

rem *** DB GATEWAY ***
set DBGW=FALSE

rem *** DB2 ADAPTER ***
set DB2=FALSE

rem *** Microsoft SQL ADAPTER ***
set MSSQL=FALSE

rem *** PostgreSQL ***
set POSTGRESQL=FALSE

rem *** Informix ADAPTER ***
set INFORMIX=FALSE

rem *** Sybase ADAPTER ***
set SYBASE=FALSE

rem *** TN3270 ADAPTER ***
set TN3270=FALSE

rem *** TN5250 ADAPTER ***
set TN5250=FALSE

rem *** VT ADAPTER ***
set VT=FALSE

rem *** HTML ADAPTER ***
set HTMLCONNECT=FALSE

rem *** PDF ADAPTER ***
set PDF=FALSE

rem *** EXCHANGE GATEWAY ***
set EXCHANGE=FALSE

rem *** ITNCM ADAPTER ***
set ITNCM=FALSE

rem *** TSRM GATEWAY ***
set TSRM=FALSE

rem *** XMPP GATEWAY ***
set XMPP=FALSE

rem *** SNMP GATEWAY ***
set SNMP=FALSE

rem *** ServiceNow GATEWAY ***
set SERVICENOW=FALSE

rem *** HP Operations Manager GATEWAY ***
set HPOM=FALSE

rem *** EmailConnect2 ***
set EMAILCONNECT2=FALSE

rem *** EWS GATEWAY ***
set EWS=FALSE

rem *** WSLITE Adapter ***
set WSLITECONNECT=FALSE

rem *** EMAIL GATEWAY ***
set EMAIL=FALSE

rem *** FTP ADAPTER ***
set FTPCONNECT=FALSE

rem *** Network ADAPTER ***
set NETWORKCONNECT=FALSE

rem *** HP Service Manager GATEWAY ***
set HPSM=FALSE

rem *** HTTP GATEWAY ***
set HTTP=FALSE

rem default classes
set CP=%DIST%/rsremote/service/proxy
set CP=%CP%;%DIST%/rsremote/lib/resolve-remote.jar
set CP=%CP%;%DIST%/lib/commons-io.jar
set CP=%CP%;%DIST%/lib/commons-lang.jar
set CP=%CP%;%DIST%/lib/commons-lang3.jar
set CP=%CP%;%DIST%/lib/commons-logging.jar
set CP=%CP%;%DIST%/lib/commons-beanutils.jar
set CP=%CP%;%DIST%/lib/commons-collections.jar
set CP=%CP%;%DIST%/lib/commons-collections4.jar
set CP=%CP%;%DIST%/lib/commons-codec.jar
set CP=%CP%;%DIST%/lib/log4j.jar
set CP=%CP%;%DIST%/lib/dom4j.jar
set CP=%CP%;%DIST%/lib/jaxen.jar
set CP=%CP%;%DIST%/lib/jms.jar
set CP=%CP%;%DIST%/lib/groovy.jar
set CP=%CP%;%DIST%/lib/concurrent.jar
set CP=%CP%;%DIST%/lib/rabbitmq-client.jar
set CP=%CP%;%DIST%/lib/ezmorph.jar
set CP=%CP%;%DIST%/lib/json-lib.jar
set CP=%CP%;%DIST%/lib/org.apache.commons.fileupload.jar
rem set CP=%CP%;%DIST%/lib/javax.servlet.jar
set CP=%CP%;%DIST%/lib/servlet-api.jar
set CP=%CP%;%DIST%/lib/esapi.jar
set CP=%CP%;%DIST%/lib/joda-time.jar
set CP=%CP%;%DIST%/lib/slf4j-api.jar
set CP=%CP%;%DIST%/lib/jackson-core.jar
set CP=%CP%;%DIST%/lib/jackson-core-asl.jar
set CP=%CP%;%DIST%/lib/jackson-datatype-json-org.jar
set CP=%CP%;%DIST%/lib/jackson-mapper-asl.jar
set CP=%CP%;%DIST%/lib/hibernate-jpa-2.1.jar

rem ElasticSearch
set CP=%CP%;%DIST%/lib/antlr-runtime.jar
set CP=%CP%;%DIST%/lib/apache-log4j-extras.jar
set CP=%CP%;%DIST%/lib/asm.jar
set CP=%CP%;%DIST%/lib/asm-commons.jar
set CP=%CP%;%DIST%/lib/elasticsearch.jar
rem set CP=%CP%;%DIST%/lib/groovy-all.jar
set CP=%CP%;%DIST%/lib/jna.jar
set CP=%CP%;%DIST%/lib/jts.jar
set CP=%CP%;%DIST%/lib/lucene-analyzers-common.jar
set CP=%CP%;%DIST%/lib/lucene-core.jar
set CP=%CP%;%DIST%/lib/lucene-expressions.jar
set CP=%CP%;%DIST%/lib/lucene-grouping.jar
set CP=%CP%;%DIST%/lib/lucene-highlighter.jar
set CP=%CP%;%DIST%/lib/lucene-join.jar
set CP=%CP%;%DIST%/lib/lucene-memory.jar
set CP=%CP%;%DIST%/lib/lucene-misc.jar
set CP=%CP%;%DIST%/lib/lucene-queries.jar
set CP=%CP%;%DIST%/lib/lucene-queryparser.jar
set CP=%CP%;%DIST%/lib/lucene-sandbox.jar
set CP=%CP%;%DIST%/lib/lucene-spatial.jar
set CP=%CP%;%DIST%/lib/lucene-suggest.jar
set CP=%CP%;%DIST%/lib/spatial4j.jar
rem set CP=%CP%;%DIST%/lib/tika.jar
set CP=%CP%;%DIST%/lib/jsoup.jar

rem connectors
set CP=%CP%;%DIST%/lib/ganymed-ssh2.jar
set CP=%CP%;%DIST%/lib/sshj-0.21.1.jar
set CP=%CP%;%DIST%/lib/bcpkix-jdk15on-157.jar
set CP=%CP%;%DIST%/lib/bcprov-ext-jdk15on-157.jar
set CP=%CP%;%DIST%/lib/bcprov-jdk15on-157.jar
set CP=%CP%;%DIST%/lib/eddsa-0.1.0.jar
set CP=%CP%;%DIST%/lib/jzlib-1.1.3.jar
set CP=%CP%;%DIST%/lib/commons-codec.jar
set CP=%CP%;%DIST%/lib/commons-net.jar
set CP=%CP%;%DIST%/lib/httpcore.jar
set CP=%CP%;%DIST%/lib/httpclient.jar
set CP=%CP%;%DIST%/lib/httpmime.jar
set CP=%CP%;%DIST%/lib/httpclient-win.jar
set CP=%CP%;%DIST%/lib/fluent-hc.jar
set CP=%CP%;%DIST%/lib/jna.jar
set CP=%CP%;%DIST%/lib/jna-platform.jar
set CP=%CP%;%DIST%/lib/httpclient-cache.jar
set CP=%CP%;%DIST%/lib/mariadb-java-client.jar
set CP=%CP%;%DIST%/lib/ojdbc8.jar
set CP=%CP%;%DIST%/lib/xdb6.jar
set CP=%CP%;%DIST%/lib/mail.jar
set CP=%CP%;%DIST%/lib/activation.jar
rem set CP=%CP%;%DIST%/lib/groovyws.jar
set CP=%CP%;%DIST%/lib/http-builder.jar

rem default gateway libraries
set CP=%CP%;%DIST%/lib/antlr.jar
set CP=%CP%;%DIST%/lib/antlrworks.jar
set CP=%CP%;%DIST%/lib/ldap.jar

rem MCP
set CP=%CP%;%DIST%/lib/guava.jar
set CP=%CP%;%DIST%/lib/org.xbill.dns.jar

rem receivers
set CP=%CP%;%DIST%/lib/snmp.jar

rem JSQL Parser
set CP=%CP%;%DIST%/lib/jsqlparser.jar

rem Gateway metrics logging
set CP=%CP%;%DIST%/lib/gson.jar

rem NetcoolGateway
IF %NETCOOL%==FALSE GOTO :END_NETCOOL
    set CP=%CP%;%DIST%/lib/jconn3.jar
    set CP=%CP%;%DIST%/lib/commons-pool.jar
:END_NETCOOL

rem RemedyGateway
IF %REMEDY%==FALSE GOTO :END_REMEDY
    set CP=%CP%;%DIST%/lib/remedy/arapi70.jar
    set CP=%CP%;%DIST%/lib/remedy/arutil70.jar
    set CP=%CP%;%DIST%/lib/commons-pool.jar
    set CP=%CP%;%DIST%/lib/commons-collections.jar

    set JAVA_LIB_PATH=%JAVA_LIB_PATH%;%DIST%/lib/remedy
    set ENVPATH=%ENVPATH%;%DIST%/lib/remedy
:END_REMEDY

rem RemedyxGateway
IF %REMEDYX%==FALSE GOTO :END_REMEDYX
    set CP=%CP%;%DIST%/lib/remedyx/arapi7604.jar
    set CP=%CP%;%DIST%/lib/antlr.jar
    set CP=%CP%;%DIST%/lib/antlrworks.jar

    set JAVA_LIB_PATH=%JAVA_LIB_PATH%;%DIST%/lib/remedyx
    set ENVPATH=%ENVPATH%;%DIST%/lib/remedyx
:END_REMEDYX

rem Microsoft Adapter
IF %MSPOI%==FALSE GOTO :END_MSPOI
    set CP=%CP%;%DIST%/lib/poi.jar
    set CP=%CP%;%DIST%/lib/poi-ooxml.jar
    set CP=%CP%;%DIST%/lib/geronimo-stax-api.jar
    set CP=%CP%;%DIST%/lib/xmlbeans.jar
    set CP=%CP%;%DIST%/lib/ooxml-schemas.jar
	set JAVA_OPTS=%JAVA_OPTS%;-Djava.awt.headless=true
:END_MSPOI

rem DB Gateway
IF %DBGW%==FALSE GOTO :END_DBGW
    set CP=%CP%;%DIST%/lib/commons-pool.jar
    set CP=%CP%;%DIST%/lib/commons-dbcp.jar
:END_DBGW

rem DB2 Adapter
IF %DB2%==FALSE GOTO :END_DB2
    set CP=%CP%;%DIST%/lib/db2jcc.jar
:END_DB2

rem Microsoft SQL Adapter
IF %MSSQL%==FALSE GOTO :END_MSSQL
    set CP=%CP%;%DIST%/lib/sqljdbc4.jar
:END_MSSQL

rem PostgreSQL SQL Adapter
IF %POSTGRESQL%==FALSE GOTO :END_POSTGRESQL
    set CP=%CP%;%DIST%/lib/postgresql.jar
:END_POSTGRESQL

rem Informix SQL Adapter
IF %INFORMIX%==FALSE GOTO :END_INFORMIX
    set CP=%CP%;%DIST%/lib/ifxjdbc.jar
:END_INFORMIX

rem Sybase SQL Adapter
IF %SYBASE%==FALSE GOTO :END_SYBASE
    set CP=%CP%;%DIST%/lib/jconn3.jar
:END_SYBASE

rem TN3270 Adapter
IF %TN3270%==FALSE GOTO :END_TN3270
    set CP=%CP%;%DIST%/lib/jagacy3270.jar
:END_TN3270

rem TN5250 Adapter
IF %TN5250%==FALSE GOTO :END_TN5250
    set CP=%CP%;%DIST%/lib/IcomWJC.jar
:END_TN5250

rem VT Adapter
IF %VT%==FALSE GOTO :END_VT
    set CP=%CP%;%DIST%/lib/jagacyVT.jar
:END_VT

rem HtmlConnect Adapter
IF %HTMLCONNECT%==FALSE GOTO :END_HTMLCONNECT
    set CP=%CP%;%DIST%/lib/cssparser.jar
    set CP=%CP%;%DIST%/lib/nekohtml.jar
    set CP=%CP%;%DIST%/lib/sac.jar
    set CP=%CP%;%DIST%/lib/httpmime.jar
    set CP=%CP%;%DIST%/lib/htmlunit-core-js.jar
    set CP=%CP%;%DIST%/lib/htmlunit.jar
    set CP=%CP%;%DIST%/lib/serializer.jar
    set CP=%CP%;%DIST%/lib/xalan.jar
    set CP=%CP%;%DIST%/lib/xercesImpl.jar
    set CP=%CP%;%DIST%/lib/xml-apis.jar
:END_HTMLCONNECT

rem PDF Adapter
IF %PDF%==FALSE GOTO :END_PDF
    set CP=%CP%;%DIST%/lib/itext.jar
    set CP=%CP%;%DIST%/lib/pdfbox.jar
    set CP=%CP%;%DIST%/lib/fontbox.jar
    set CP=%CP%;%DIST%/lib/jempbox.jar
:END_PDF

rem Exchange Gateway
IF %EXCHANGE%==FALSE GOTO :END_EXCHANGE
    set CP=%CP%;%DIST%/lib/moonrug.jar
    set CP=%CP%;%DIST%/lib/apache-mime4j.jar
    set CP=%CP%;%DIST%/lib/httpmime.jar
    set CP=%CP%;%DIST%/lib/xpp3.jar
    set JAVA_OPTS=%JAVA_OPTS%;-Dmoonrug.license=%DIST%/license/moonrug.xml
:END_EXCHANGE

rem ITNCM Gateway
IF %ITNCM%==FALSE GOTO :END_ITNCM
    set CP=%CP%;%DIST%/lib/itncm/appframework.jar
    set CP=%CP%;%DIST%/lib/itncm/presentation.jar
    set CP=%CP%;%DIST%/lib/itncm/common.jar
    set CP=%CP%;%DIST%/lib/itncm/driverinterface.jar
    set CP=%CP%;%DIST%/lib/itncm/intelliden-soap-apis.jar
    set CP=%CP%;%DIST%/lib/itncm/ibm_tivoli-events.jar
    set CP=%CP%;%DIST%/lib/itncm/xstream-1.2.2.jar
rem set CP=%CP%;%DIST%/lib/itncm/httpclient-4.0.1.jar
rem set CP=%CP%;%DIST%/lib/itncm/httpcore-4.0.1.jar
    set CP=%CP%;%DIST%/lib/itncm/jdom-1.0beta8.jar
    set CP=%CP%;%DIST%/lib/itncm/mibble-commercial-parser-2.9.2.jar
rem set CP=%CP%;%DIST%/lib/itncm/ojdbc14.jar
rem set CP=%CP%;%DIST%/lib/itncm/commons-codec-1.4.jar
rem set CP=%CP%;%DIST%/lib/itncm/commons-logging-1.1.jar
:END_ITNCM

rem TSRM Gateway
IF %TSRM%==FALSE GOTO :END_TSRM
    set CP=%CP%;%DIST%/lib/antlr.jar
    set CP=%CP%;%DIST%/lib/antlrworks.jar
:END_TSRM

rem XMPP Gateway
IF %XMPP%==FALSE GOTO :END_XMPP
    set CP=%CP%;%DIST%/lib/smack.jar
    set CP=%CP%;%DIST%/lib/smackx.jar
    set CP=%CP%;%DIST%/lib/smackx-debug.jar
    set CP=%CP%;%DIST%/lib/smackx-jingle.jar
    set CP=%CP%;%DIST%/lib/antlr.jar
    set CP=%CP%;%DIST%/lib/antlrworks.jar
:END_XMPP

rem SNMP Gateway
IF %SNMP%==FALSE GOTO :END_SNMP
    set CP=%CP%;%DIST%/lib/snmp4j.jar
:END_SNMP

rem ServiceNow Gateway
IF %SERVICENOW%==FALSE GOTO :END_SERVICENOW
    set CP=%CP%;%DIST%/lib/antlr.jar
    set CP=%CP%;%DIST%/lib/antlrworks.jar
:END_SERVICENOW

rem HPOM Gateway
IF %HPOM%==FALSE GOTO :END_HPOM
    set CP=%CP%;%DIST%/lib/antlr.jar
    set CP=%CP%;%DIST%/lib/antlrworks.jar
	set CP=%CP%;%DIST%/lib/hpom-client.jar
	set CP=%CP%;%DIST%/lib/addressing.jar
	set CP=%CP%;%DIST%/lib/commons-codec.jar
	set CP=%CP%;%DIST%/lib/commons-httpclient.jar
	set CP=%CP%;%DIST%/lib/commons-logging.jar
	set CP=%CP%;%DIST%/lib/httpcore.jar
	set CP=%CP%;%DIST%/lib/xercesImpl.jar
	set CP=%CP%;%DIST%/lib/mail.jar
	set CP=%CP%;%DIST%/lib/axiom-api.jar
	set CP=%CP%;%DIST%/lib/axiom-impl.jar
	set CP=%CP%;%DIST%/lib/axis2-adb.jar
	set CP=%CP%;%DIST%/lib/axis2-kernel.jar
	set CP=%CP%;%DIST%/lib/axis2-transport-http.jar
	set CP=%CP%;%DIST%/lib/axis2-transport-local.jar
	set CP=%CP%;%DIST%/lib/neethi.jar
	set CP=%CP%;%DIST%/lib/woden-api.jar
	set CP=%CP%;%DIST%/lib/wsdl4j.jar
	set CP=%CP%;%DIST%/lib/xalan.jar
	set CP=%CP%;%DIST%/lib/XmlSchema.jar
:END_HPOM

rem Email Adapter 2
IF %EMAILCONNECT2%==FALSE GOTO :END_EMAILCONNECT2
    set CP=%CP%;%DIST%/lib/commons-email.jar
:END_EMAILCONNECT2

rem EWS Gateway
IF %EWS%==FALSE GOTO :END_EWS
    set CP=%CP%;%DIST%/lib/EWSAPI.jar
    set CP=%CP%;%DIST%/lib/jcifs.jar
    set CP=%CP%;%DIST%/lib/antlr.jar
    set CP=%CP%;%DIST%/lib/antlrworks.jar
    set CP=%CP%;%DIST%/lib/commons-httpclient.jar
:END_EWS

rem WSLITE CONNECT Adapter
IF %WSLITECONNECT%==FALSE GOTO :END_WSLITECONNECT
    set CP=%CP%;%DIST%/lib/groovy-wslite.jar
:END_WSLITECONNECT

rem Email Gateway
IF %EMAIL%==FALSE GOTO :END_EMAIL
    set CP=%CP%;%DIST%/lib/commons-email.jar
:END_EMAIL

rem FTP Adapter
IF %FTPCONNECT%==FALSE GOTO :END_FTPCONNECT
    set CP=%CP%;%DIST%/lib/ftp4j.jar
:END_FTPCONNECT

rem HPSM Gateway
IF %HPSM%==FALSE GOTO :END_HPSM
    set CP=%CP%;%DIST%/lib/hpsm-client.jar
:END_HPSM

rem HTTP Gateway
IF %HTTP%==FALSE GOTO :END_HTTP
    set CP=%CP%;%DIST%/lib/jetty-continuation.jar
    set CP=%CP%;%DIST%/lib/jetty-http.jar
    set CP=%CP%;%DIST%/lib/jetty-io.jar
    set CP=%CP%;%DIST%/lib/jetty-security.jar
    set CP=%CP%;%DIST%/lib/jetty-server.jar
    set CP=%CP%;%DIST%/lib/jetty-servlet.jar
    set CP=%CP%;%DIST%/lib/jetty-servlets.jar
    set CP=%CP%;%DIST%/lib/jetty-util.jar
    set CP=%CP%;%DIST%/lib/jetty-webapp.jar
    set CP=%CP%;%DIST%/lib/websocket-api.jar
    set CP=%CP%;%DIST%/lib/websocket-client.jar
    set CP=%CP%;%DIST%/lib/websocket-common.jar
:END_HTTP

rem Custom Jars
set CP=%CP%;%DIST%/rsremote/lib/*

rem SDK Developed Gateway Jars
set CP=%CP%;%DIST%/gatewaylibs/*

rem external classpath
set EXTERNALCLASSPATHS=
set CP=%CP%;%EXTERNALCLASSPATHS%

rem final JAVA_OPTS
set JAVA_OPTS=%JAVA_OPTS%;-Djava.library.path='%JAVA_LIB_PATH%';-Dorg.apache.cxf.Logger=org.apache.cxf.common.logging.Log4jLogger

set EXECUTABLE=%DIST%\rsremote\bin\rsremote.exe
set SERVICE_NAME=RSRemote
set PR_DISPLAYNAME=RSRemote
set PR_DESCRIPTION=RSRemote
set PR_INSTALL=%EXECUTABLE%
set PR_LOGPATH=%DIST%\rsremote\log
set PR_LOGPREFIX=service
set PR_CLASSPATH=%CP%
set PR_JVM=%JRE%\bin\server\jvm.dll
set PR_STARTUP=auto
set PR_STARTCLASS=com.resolve.rsremote.Main
set PR_STARTMETHOD=main
set PR_STARTPATH=%DIST%
set PR_STARTMODE=jvm
set PR_STOPCLASS=java.lang.System
set PR_STOPMETHOD=exit
set PR_STOPPATH=%DIST%
set PR_STOPMODE=jvm
set PR_STOPTIMEOUT=30
set PR_STARTPARAMS=
set PR_ENVIRONMENT=PATH='%PATH%;%ENVPATH%'
set PR_STDOUTPUT=%DIST%\rsremote\log\stdout
set PR_STDERROR=%DIST%\rsremote\log\stderr
set PR_JVMMS=256
set PR_JVMMX=512
set PR_JVMSS=256
"%EXECUTABLE%" //US//%SERVICE_NAME% --JvmOptions="%JAVA_OPTS%;-XX:+CMSClassUnloadingEnabled;-XX:+UseConcMarkSweepGC;-XX:+HeapDumpOnOutOfMemoryError"

rem cleanup
set SERVICE_NAME=
set PR_DISPLAYNAME=
set PR_DESCRIPTION=
set PR_INSTALL=
set PR_LOGPATH=
set PR_LOGPREFIX=
set PR_CLASSPATH=
set PR_JVM=
set PR_STARTUP=
set PR_STARTCLASS=
set PR_STARTMETHOD=
set PR_STARTPATH=
set PR_STARTMODE=
set PR_STOPCLASS=
set PR_STOPMETHOD=
set PR_STOPPATH=
set PR_STOPMODE=
set PR_STOPTIMEOUT=
set PR_STARTPARAMS=
set PR_STDOUTPUT=
set PR_STDERROR=
set PR_ENVIRONMENT=
set PR_JVMMS=
set PR_JVMMX=
set PR_JVMSS=
set JAVA_OPTS=