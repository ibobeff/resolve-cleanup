#!/bin/bash

# properties
INSTANCE_NAME=rsremote
BIN=`echo $0 | awk '{ print substr( $0, 0, length($0)-6 ) }'`
cd ${BIN}../..
DIST=`pwd`
. ${DIST}/rsremote/bin/rsremote-env.sh
JDK=${RSREMOTEJDK}
if [ -d "${JDK}" ];
then
	JAVA_RESOLVEMAJOR=`${DIST}/jdk/bin/java -version 2>&1 | awk 'NR==1{split($0,a,"."); print a[2] }'`
	JAVA_RESOLVELESSER=`${DIST}/jdk/bin/java -version 2>&1 | awk  'NR==1{gsub(/"/,"");split($0,a,"_"); print a[2] }'`
	JAVA_BLUEPRINTMAJOR=`${JDK}/bin/java -version 2>&1 | awk 'NR==1{split($0,a,"."); print a[2] }'`
	JAVA_BLUEPRINTLESSER=`${JDK}/bin/java -version 2>&1 | awk  'NR==1{gsub(/"/,"");split($0,a,"_"); print a[2] }'`
	if [ ${JAVA_RESOLVEMAJOR} -ne ${JAVA_BLUEPRINTMAJOR} ]; then
			echo "supplied java version is different than the required version, using default resolve java"
			JDK=${DIST}/jdk
	fi

	if [ ${JAVA_RESOLVELESSER} -gt ${JAVA_BLUEPRINTLESSER} ]; then
			echo "supplied java version is less than required, using default resolve java"
			JDK=${DIST}/jdk
	fi
else
	echo given java_home folder does not exist using default resolve java
	JDK=${DIST}/jdk
fi
LOCKFILE=${DIST}/${INSTANCE_NAME}/bin/lock
ENVPATH=${JDK}/bin
JAVA_LIB_PATH=${DIST}/lib:${LD_LIBRARY_PATH}

if [ -f ${LOCKFILE} ]; then
    if [ $# -eq 0 ] || [ "$1" != "1" ]; then
        echo "Lock File Detected"
        echo "Cancelling ${INSTANCE_NAME} Startup"
        exit 1
    fi
fi

# *** NETCOOL GATEWAY ***
NETCOOL=TRUE

# *** REMEDYX GATEWAY ***
REMEDYX=FALSE

# *** MICROSOFT ADAPTER ***
MSPOI=FALSE

# *** DB GATEWAY ***
DBGW=FALSE

# *** MySQL ADAPTER ***
MYSQL=FALSE

# *** Oracle ADAPTER ***
ORACLE=FALSE

# *** DB2 ADAPTER ***
DB2=FALSE

# *** Microsoft SQL ADAPTER ***
MSSQL=FALSE

# *** PostgreSQL ADAPTER ***
POSTGRESQL=FALSE

# *** Informix ADAPTER ***
INFORMIX=FALSE

# *** Sybase ADAPTER ***
SYBASE=FALSE

# *** TN3270 ADAPTER ***
TN3270=FALSE

# *** TN5250 ADAPTER ***
TN5250=FALSE

# *** VT ADAPTER ***
VT=FALSE

# *** HTML ADAPTER ***
HTMLCONNECT=FALSE

# *** PDF ADAPTER ***
PDF=FALSE

# *** EXCHANGE GATEWAY ***
EXCHANGE=FALSE

# *** ITNCM ADAPTER ***
ITNCM=FALSE

# *** TSRM GATEWAY ***
TSRM=FALSE

# *** XMPP GATEWAY ***
XMPP=FALSE

# *** SNMP GATEWAY ***
SNMP=FALSE

# *** ServiceNow GATEWAY ***
SERVICENOW=FALSE

# *** HP Operations Manager GATEWAY ***
HPOM=FALSE

# *** EmailConnect2 ***
EMAILCONNECT2=FALSE

# *** EWS GATEWAY ***
EWS=FALSE

# *** WSLITE ADAPTER ***
WSLITECONNECT=FALSE

# *** EMAIL GATEWAY ***
EMAIL=FALSE

# *** FTP ADAPTER ***
FTPCONNECT=FALSE

# *** NETWORK ADAPTER ***
NETWORKCONNECT=FALSE

# *** HP Service Manager GATEWAY ***
HPSM=FALSE

# *** HTTP GATEWAY ***
HTTP=FALSE

# set os type
linux=false
sunos=false
case "`uname`" in
Linux*) linux=true;;
SunOS*) sunos=true;;
esac

# execute
JAVA_OPTS=

# OWASP ESAPI
JAVA_OPTS="${JAVA_OPTS} -Dorg.owasp.esapi.resources=${DIST}/${INSTANCE_NAME}/config"

if $linux; then
    JAVA_EXEC=${JDK}/bin/java
else
    JAVA_EXEC=${JDK}/bin/sparcv9/java
    JAVA_OPTS="${JAVA_OPTS} -d64"
fi

# default classes
CP="${DIST}/${INSTANCE_NAME}/service/proxy"
CP="${CP}:${DIST}/${INSTANCE_NAME}/lib/resolve-remote.jar"

CP="${CP}:${DIST}/${INSTANCE_NAME}/lib/*"
#CP="${CP}:${DIST}/lib/*"# CP="${CP}:${DIST}/lib/commons-io.jar"
# CP="${CP}:${DIST}/lib/commons-lang.jar"
# CP="${CP}:${DIST}/lib/commons-lang3.jar"
# CP="${CP}:${DIST}/lib/commons-logging.jar"
# CP="${CP}:${DIST}/lib/commons-beanutils.jar"
# CP="${CP}:${DIST}/lib/commons-collections.jar"
# CP="${CP}:${DIST}/lib/commons-collections4.jar"
# CP="${CP}:${DIST}/lib/commons-codec.jar"
# CP="${CP}:${DIST}/lib/log4j.jar"
# CP="${CP}:${DIST}/lib/dom4j.jar"
# CP="${CP}:${DIST}/lib/jaxen.jar"
# CP="${CP}:${DIST}/lib/jms.jar"
# CP="${CP}:${DIST}/lib/groovy.jar"
# CP="${CP}:${DIST}/lib/concurrent.jar"
# CP="${CP}:${DIST}/lib/ezmorph.jar"
# CP="${CP}:${DIST}/lib/json-lib.jar"
# CP="${CP}:${DIST}/lib/org.apache.commons.fileupload.jar"
# #CP="${CP}:${DIST}/lib/javax.servlet.jar"
# CP="${CP}:${DIST}/lib/servlet-api.jar"
# CP="${CP}:${DIST}/lib/esapi.jar"
# CP="${CP}:${DIST}/lib/joda-time.jar"
# CP="${CP}:${DIST}/lib/docx4j.jar"
# CP="${CP}:${DIST}/lib/jaxb-xmldsig-core.jar"
# CP="${CP}:${DIST}/lib/xmlgraphics-commons.jar"
# CP="${CP}:${DIST}/lib/xalan.jar"
# CP="${CP}:${DIST}/lib/slf4j-api.jar"
# CP="${CP}:${DIST}/lib/jackson-core.jar"
# CP="${CP}:${DIST}/lib/jackson-core-asl.jar"
# CP="${CP}:${DIST}/lib/jackson-datatype-json-org.jar"
# CP="${CP}:${DIST}/lib/jackson-mapper-asl.jar"
# CP="${CP}:${DIST}/lib/hibernate-jpa-2.1.jar"

# # RabbitMQ
# CP="${CP}:${DIST}/lib/rabbitmq-client.jar"

# # ElasticSearch
# CP="${CP}:${DIST}/lib/antlr-runtime.jar"
# CP="${CP}:${DIST}/lib/apache-log4j-extras.jar"
# CP="${CP}:${DIST}/lib/asm.jar"
# CP="${CP}:${DIST}/lib/asm-commons.jar"
# CP="${CP}:${DIST}/lib/elasticsearch.jar"
# # CP="${CP}:${DIST}/lib/groovy-all.jar"
# CP="${CP}:${DIST}/lib/jna.jar"
# CP="${CP}:${DIST}/lib/jts.jar"
# CP="${CP}:${DIST}/lib/lucene-analyzers-common.jar"
# # CP="${CP}:${DIST}/lib/lucene-codecs.jar"
# CP="${CP}:${DIST}/lib/lucene-core.jar"
# CP="${CP}:${DIST}/lib/lucene-expressions.jar"
# CP="${CP}:${DIST}/lib/lucene-grouping.jar"
# CP="${CP}:${DIST}/lib/lucene-highlighter.jar"
# CP="${CP}:${DIST}/lib/lucene-join.jar"
# CP="${CP}:${DIST}/lib/lucene-memory.jar"
# CP="${CP}:${DIST}/lib/lucene-misc.jar"
# CP="${CP}:${DIST}/lib/lucene-queries.jar"
# CP="${CP}:${DIST}/lib/lucene-queryparser.jar"
# CP="${CP}:${DIST}/lib/lucene-sandbox.jar"
# CP="${CP}:${DIST}/lib/lucene-spatial.jar"
# CP="${CP}:${DIST}/lib/lucene-suggest.jar"
# CP="${CP}:${DIST}/lib/spatial4j.jar"
# # CP="${CP}:${DIST}/lib/tika.jar"
# CP="${CP}:${DIST}/lib/jsoup.jar"

# # connectors
# CP="${CP}:${DIST}/lib/ganymed-ssh2.jar"
# CP="${CP}:${DIST}/lib/sshj-0.21.1.jar"
# CP="${CP}:${DIST}/lib/bcpkix-jdk15on-157.jar"
# CP="${CP}:${DIST}/lib/bcprov-ext-jdk15on-157.jar"
# CP="${CP}:${DIST}/lib/bcprov-jdk15on-157.jar"
# CP="${CP}:${DIST}/lib/eddsa-0.1.0.jar"
# CP="${CP}:${DIST}/lib/jzlib-1.1.3.jar"
# CP="${CP}:${DIST}/lib/slf4j-log4j.jar"
# CP="${CP}:${DIST}/lib/commons-codec.jar"
# CP="${CP}:${DIST}/lib/commons-net.jar"
# CP="${CP}:${DIST}/lib/httpcore.jar"
# CP="${CP}:${DIST}/lib/httpclient.jar"
# CP="${CP}:${DIST}/lib/httpmime.jar"
# CP="${CP}:${DIST}/lib/httpclient-win.jar"
# CP="${CP}:${DIST}/lib/fluent-hc.jar"
# CP="${CP}:${DIST}/lib/jna.jar"
# CP="${CP}:${DIST}/lib/jna-platform.jar"
# CP="${CP}:${DIST}/lib/httpclient-cache.jar"
# CP="${CP}:${DIST}/lib/mail.jar"
# CP="${CP}:${DIST}/lib/activation.jar"
# # CP="${CP}:${DIST}/lib/groovyws.jar"
# CP="${CP}:${DIST}/lib/snmp.jar"
# CP="${CP}:${DIST}/lib/http-builder.jar"

# # default gateway libraries
# CP="${CP}:${DIST}/lib/antlr.jar"
# CP="${CP}:${DIST}/lib/antlrworks.jar"
# CP="${CP}:${DIST}/lib/ldap.jar"

# # mcp
# CP="${CP}:${DIST}/lib/guava.jar"
# CP="${CP}:${DIST}/lib/org.xbill.dns.jar"

# # JSQL Parser
# CP="${CP}:${DIST}/lib/jsqlparser.jar"

# # Gateway metrics logging
# CP="${CP}:${DIST}/lib/gson.jar"

# # Netcool Gateway
# if [ "${NETCOOL}" = "TRUE" ]
# then
#     CP="${CP}:${DIST}/lib/jconn3.jar"
#     CP="${CP}:${DIST}/lib/commons-pool.jar"
# fi

# # Remedyx Gateway
# if [ "${REMEDYX}" = "TRUE" ]
# then
#     CP="${CP}:${DIST}/lib/remedyx/arapi7604.jar"
# fi

# # Microsoft Adapter
# if [ "${MSPOI}" = "TRUE" ]
# then
#     CP="${CP}:${DIST}/lib/poi.jar"
#     CP="${CP}:${DIST}/lib/poi-ooxml.jar"
#     CP="${CP}:${DIST}/lib/geronimo-stax-api.jar"
#     CP="${CP}:${DIST}/lib/xmlbeans.jar"
#     CP="${CP}:${DIST}/lib/ooxml-schemas.jar"
	
# 	JAVA_OPTS="${JAVA_OPTS} -Djava.awt.headless=true"
# fi

# # DB Gateway
# if [ "${DBGW}" = "TRUE" ]
# then
#     CP="${CP}:${DIST}/lib/commons-dbcp.jar"
#     CP="${CP}:${DIST}/lib/commons-pool.jar"
# fi

# # MySQL Adapter
# if [ "${MYSQL}" = "TRUE" ]
# then
#     CP="${CP}:${DIST}/lib/mariadb-java-client.jar"
# fi

# # Oracle Adapter
# if [ "${ORACLE}" = "TRUE" ]
# then
#     CP="${CP}:${DIST}/lib/ojdbc8.jar"
#     CP="${CP}:${DIST}/lib/xdb6.jar"
# fi

# # DB2 Adapter
# if [ "${DB2}" = "TRUE" ]
# then
#     CP="${CP}:${DIST}/lib/db2jcc.jar"
# fi

# # Microsoft SQL Adapter
# if [ "${MSSQL}" = "TRUE" ]
# then
#     CP="${CP}:${DIST}/lib/sqljdbc4.jar"
# fi

# # PostgreSQL Adapter
# if [ "${POSTGRESQL}" = "TRUE" ]
# then
#     CP="${CP}:${DIST}/lib/postgresql.jar"
# fi

# # Informix Adapter
# if [ "${INFORMIX}" = "TRUE" ]
# then
#     CP="${CP}:${DIST}/lib/ifxjdbc.jar"
# fi

# # Sybase Adapter
# if [ "${SYBASE}" = "TRUE" ]
# then
#     CP="${CP}:${DIST}/lib/jconn3.jar"
# fi

# # TN3270 Adapter
# if [ "${TN3270}" = "TRUE" ]
# then
#     CP="${CP}:${DIST}/lib/jagacy/jagacy3270.jar"
# fi

# # TN5250 Adapter
# if [ "${TN5250}" = "TRUE" ]
# then
#     CP="${CP}:${DIST}/lib/jagacy/IcomWJC.jar"
# fi

# # VT Adapter
# if [ "${VT}" = "TRUE" ]
# then
#     CP="${CP}:${DIST}/lib/jagacy/jagacyVT.jar"
# fi

# # HTML Adapter
# if [ "${HTMLCONNECT}" = "TRUE" ]
# then
#     CP="${CP}:${DIST}/lib/cssparser.jar"
#     CP="${CP}:${DIST}/lib/nekohtml.jar"
#     CP="${CP}:${DIST}/lib/sac.jar"
#     CP="${CP}:${DIST}/lib/httpmime.jar"
#     CP="${CP}:${DIST}/lib/htmlunit-core-js.jar"
#     CP="${CP}:${DIST}/lib/htmlunit.jar"
#     CP="${CP}:${DIST}/lib/serializer.jar"
#     CP="${CP}:${DIST}/lib/xalan.jar"
#     CP="${CP}:${DIST}/lib/xercesImpl.jar"
#     CP="${CP}:${DIST}/lib/xml-apis.jar"
# 	CP="${CP}:${DIST}/lib/websocket-api.jar"
#     CP="${CP}:${DIST}/lib/websocket-client.jar"
#     CP="${CP}:${DIST}/lib/websocket-common.jar"
# fi

# # PDF Adapter
# if [ "${PDF}" = "TRUE" ]
# then
#     CP="${CP}:${DIST}/lib/itext.jar"
#     CP="${CP}:${DIST}/lib/pdfbox.jar"
#     CP="${CP}:${DIST}/lib/fontbox.jar"
#     CP="${CP}:${DIST}/lib/jempbox.jar"
# fi

# # Exchange Gateway
# if [ "${EXCHANGE}" = "TRUE" ]
# then
#     CP="${CP}:${DIST}/lib/moonrug/moonrug.jar"
#     CP="${CP}:${DIST}/lib/moonrug/apache-mime4j.jar"
#     CP="${CP}:${DIST}/lib/moonrug/xpp3.jar"

#     JAVA_OPTS="${JAVA_OPTS} -Dmoonrug.license=${DIST}/lib/moonrug/moonrug.xml"
# fi

# # ITNCM Adatper
# if [ "${ITNCM}" = "TRUE" ]
# then
#     CP="${CP}:${DIST}/lib/itncm/appframework.jar"
#     CP="${CP}:${DIST}/lib/itncm/presentation.jar"
#     CP="${CP}:${DIST}/lib/itncm/common.jar"
#     CP="${CP}:${DIST}/lib/itncm/driverinterface.jar"
#     CP="${CP}:${DIST}/lib/itncm/intelliden-soap-apis.jar"
#     CP="${CP}:${DIST}/lib/itncm/ibm_tivoli-events.jar"
#     CP="${CP}:${DIST}/lib/itncm/xstream-1.2.2.jar"
#     #CP="${CP}:${DIST}/lib/itncm/httpclient-4.0.1.jar"
#     #CP="${CP}:${DIST}/lib/itncm/httpcore-4.0.1.jar"
#     CP="${CP}:${DIST}/lib/itncm/jdom-1.0beta8.jar"
#     CP="${CP}:${DIST}/lib/itncm/mibble-commercial-parser-2.9.2.jar"
#     #CP="${CP}:${DIST}/lib/itncm/ojdbc14.jar"
#     #CP="${CP}:${DIST}/lib/itncm/commons-codec-1.4.jar"
#     #CP="${CP}:${DIST}/lib/itncm/commons-logging-1.1.jar"
# fi

# # TSRM Gateway
# if [ "${TSRM}" = "TRUE" ]
# then
# 	CP="${CP}:${DIST}/lib/antlr.jar"
#     CP="${CP}:${DIST}/lib/antlrworks.jar"
# fi

# # XMPP Gateway
# if [ "${XMPP}" = "TRUE" ]
# then
#     CP="${CP}:${DIST}/lib/smack.jar"
#     CP="${CP}:${DIST}/lib/smackx.jar"
#     CP="${CP}:${DIST}/lib/smackx-debug.jar"
#     CP="${CP}:${DIST}/lib/smackx-jingle.jar"
# fi

# # SNMP Gateway
# if [ "${SNMP}" = "TRUE" ]
# then
#     CP="${CP}:${DIST}/lib/snmp4j.jar"
# fi

# # ServiceNow Gateway
# #if [ "${SERVICENOW}" = "TRUE" ]
# #then
# #fi

# # HPOM Gateway
# if [ "${HPOM}" = "TRUE" ]
# then
#     CP="${CP}:${DIST}/lib/hpom-client.jar"
#     CP="${CP}:${DIST}/lib/addressing.jar"
#     CP="${CP}:${DIST}/lib/commons-codec.jar"
#     CP="${CP}:${DIST}/lib/commons-httpclient.jar"
#     CP="${CP}:${DIST}/lib/commons-logging.jar"
#     CP="${CP}:${DIST}/lib/httpcore.jar"
#     CP="${CP}:${DIST}/lib/xercesImpl.jar"
#     CP="${CP}:${DIST}/lib/mail.jar"
#     CP="${CP}:${DIST}/lib/axiom-api.jar"
#     CP="${CP}:${DIST}/lib/axiom-impl.jar"
#     CP="${CP}:${DIST}/lib/axis2-adb.jar"
#     CP="${CP}:${DIST}/lib/axis2-kernel.jar"
#     CP="${CP}:${DIST}/lib/axis2-transport-http.jar"
#     CP="${CP}:${DIST}/lib/axis2-transport-local.jar"
#     CP="${CP}:${DIST}/lib/neethi.jar"
#     CP="${CP}:${DIST}/lib/woden-api.jar"
#     CP="${CP}:${DIST}/lib/wsdl4j.jar"
#     CP="${CP}:${DIST}/lib/xalan.jar"
#     CP="${CP}:${DIST}/lib/XmlSchema.jar"
# fi

# # Email Adapter 2
# if [ "${EMAILCONNECT2}" = "TRUE" ]
# then
#     CP="${CP}:${DIST}/lib/commons-email.jar"
# fi

# # EWS Gateway
# if [ "${EWS}" = "TRUE" ]
# then
#     CP="${CP}:${DIST}/lib/EWSAPI.jar"
#     CP="${CP}:${DIST}/lib/jcifs.jar"
#     CP="${CP}:${DIST}/lib/commons-httpclient.jar"
# fi

# # WSLITE Adapter
# if [ "${WSLITECONNECT}" = "TRUE" ]
# then
#     CP="${CP}:${DIST}/lib/groovy-wslite.jar"
# fi

# # Email Gateway
# if [ "${EMAIL}" = "TRUE" ]
# then
#     CP="${CP}:${DIST}/lib/commons-email.jar"
# fi

# # FTP Adapter
# if [ "${FTPCONNECT}" = "TRUE" ]
# then
#     CP="${CP}:${DIST}/lib/ftp4j.jar"
# fi

# # HPSM Gateway
# if [ "${HPSM}" = "TRUE" ]
# then
#     CP="${CP}:${DIST}/lib/hpsm-client.jar"
# fi

# # HTTP Gateway
# if [ "${HTTP}" = "TRUE" ]
# then
#     CP="${CP}:${DIST}/lib/jetty-continuation.jar"
#     CP="${CP}:${DIST}/lib/jetty-http.jar"
#     CP="${CP}:${DIST}/lib/jetty-io.jar"
#     CP="${CP}:${DIST}/lib/jetty-security.jar"
#     CP="${CP}:${DIST}/lib/jetty-server.jar"
#     CP="${CP}:${DIST}/lib/jetty-servlet.jar"
#     CP="${CP}:${DIST}/lib/jetty-servlets.jar"
#     CP="${CP}:${DIST}/lib/jetty-util.jar"
#     CP="${CP}:${DIST}/lib/jetty-webapp.jar"
#     CP="${CP}:${DIST}/lib/websocket-api.jar"
#     CP="${CP}:${DIST}/lib/websocket-client.jar"
#     CP="${CP}:${DIST}/lib/websocket-common.jar"
# fi

# # rsremote lib classes
# for FILE in `ls $DIST/${INSTANCE_NAME}/lib/`
# do
#     case $FILE in
#     *.jar)
#         if [ $FILE != "resolve-remote.jar" ]; then
#             CP="${CP}:${DIST}/${INSTANCE_NAME}/lib/$FILE"
#         fi
#     ;;
#     esac
# done

# SDK Developed Gateway Jars
CP="${CP}:${DIST}/gatewaylibs/*"

# external classpath
EXTERNALCLASSPATHS=
CP="${CP}:${EXTERNALCLASSPATHS}"

# JCONSOLE - make sure "hostname -i" resolves to an ipaddress not 127.0.0.1
#JCONSOLE="-Dcom.sun.management.jmxremote.port=5552 -Dcom.sun.management.jmxremote.authenticate=false -Dcom.sun.management.jmxremote.ssl=false"

# Eclipse Debugging
#DEBUGGING="-Xdebug -Xrunjdwp:transport=dt_socket,address=8001,server=y,suspend=n"

# GC Logging
#GCLOGGING="-XX:+PrintGCDetails -verbose:gc -Xloggc:${DIST}/rsremote/log/gc.log"


PATH=${JDK}/bin:${PATH} export PATH

exec "${JAVA_EXEC}" -Djava.io.tmpdir=${DIST}/tmp ${JAVA_OPTS} ${JCONSOLE} ${DEBUGGING} ${GCLOGGING} -Djava.library.path="${JAVA_LIB_PATH}" -Dorg.apache.cxf.Logger=org.apache.cxf.common.logging.Log4jLogger -server -Xms256M -Xmx512M -Xss256K -XX:+UseConcMarkSweepGC -XX:+CMSClassUnloadingEnabled -XX:+HeapDumpOnOutOfMemoryError -cp "${CP}" com.resolve.rsremote.Main ${INSTANCE_NAME} >> ${DIST}/${INSTANCE_NAME}/log/stdout 2>&1 &

PID=$!
echo "${PID}" > ${LOCKFILE}
echo "Started ${INSTANCE_NAME} pid: ${PID}"
