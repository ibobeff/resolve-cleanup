#!/bin/bash

if [ "$#" -lt 3 ]; then
    echo "Illegal number of parameters"
    echo Usage: index_name host1 port1  start size
    echo Example: ./getindexids.sh worksheet_20170808  10.50.2.249 9700  0 80
    exit 1
fi

set -e
INDEX_NAME="$1"
HOST1="$2"
PORT1="$3"
#HOST2="$5"
#PORT2="$6"
START="$4"
SIZE="$5"

if [ -z $4 ]
  then
    START=0
fi

if [ -z $5 ]
  then
    SIZE=9999
fi

echo
echo Start from $START
echo Size is $SIZE 


echo
echo document data from $HOST1:$PORT1 
curl  -XPOST "$HOST1:$PORT1/$INDEX_NAME/_search?pretty" -d '{"query": {"match_all": {}},   "_source": ["sysId"], "from":'$START', "size": '$SIZE'}'  
echo






