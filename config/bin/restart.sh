#!/bin/bash

if [ $# -lt 1 ]; then
  echo ""
  echo "Usage: <Resolve Component...>"
  echo "This script will restart the specified Resolve Components"
  echo "ALL can be used to restart all components (except RSCONSOLE) installed on this system"
  exit 1
fi

# properties
DIST=`echo $0 | awk '{ print substr( $0, 0, length($0)-10 ) }'`/..

echo "Stopping $@"
$DIST/bin/stop.sh $@
sleep 10
echo "Starting $@"
$DIST/bin/run.sh $@
