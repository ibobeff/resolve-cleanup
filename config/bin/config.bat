@echo off

set DIST=%~dp0..

if not "%1" == "--help" GOTO :CONFIGURE
   echo This script will configure Resolve components in the current directory.
   echo The rsmgmt/configure/blueprint.properties file will be used to configure the
   echo Resolve installation
   echo.
   GOTO :END
:CONFIGURE

:LOOP
if "%1" == "" GOTO :END_LOOP
    set ARG=%1
    if not "%ARG:~0,1%" == "-" GOTO :OPTION
        if not "%FLAGS%" == "" GOTO :ADDFLAG
            set FLAGS=%1
            GOTO :SHIFT
        :ADDFLAG
            set FLAGS=%FLAGS% %1
            GOTO :SHIFT
    :OPTION
    if not "%OPTIONS%" == "" GOTO :ADDOPTION
        set OPTIONS=%1
        GOTO :SHIFT
    :ADDOPTION
        set OPTIONS=%OPTIONS%;%1
        GOTO :SHIFT
    :SHIFT
    shift
GOTO :LOOP
:END_LOOP

call "%DIST%\rsmgmt\bin\run.bat" %FLAGS% -f install/resolve-configure -s "-FD%OPTIONS%"

set /p ERROR=<%TEMP%\error.txt
set DIST=%~dp0..

set DIST=
set RESETGUID=
set OPTIONS=
set FLAGS=
set ARG=
IF "%ERROR%" EQU "error" (
  echo "WARNING!!! Insufficient diskspace."
  exit -1
) ELSE (
  echo "config successfull"
)