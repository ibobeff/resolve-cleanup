#!/bin/bash

if [ $# -lt 1 ] || [ "$1" == "--help" ]; then
    echo ""
    echo "Usage: <VM MAP COUNT>"
    echo "This setup script will add a Virtual Memory Max Map Count (default 262144) for"
    echo "the Resolve installation user on this system (/etc/sysctl.conf).  This script is"
    echo "only for initial setup on Linux systems."
    echo ""
    echo "VM MAP COUNT  - Virtual memory Max map Count for the Elasticsearch server"
    echo "                Can use --default to setup with default file limit (262144)"
    echo ""
    echo "Examples:"
    echo "    ./setup_sysctl.sh  --default"
    echo "                Setup limits.conf with all default values"
    echo "    ./setup_sysctl.sh  300000"
    echo "                Setup 300000 as the max map count"
    exit 0
fi

linux=false
sunos=false
case "`uname`" in
Linux*) linux=true;;
SunOS*) sunos=true;;
esac

if $sunos; then
    echo "SunOS server detected, skipping limit setup"
    exit 0
fi

ID=`id | sed -e "s/^uid=[0-9]*(\(.*\)) gid.*/\1/"`

if [ "${ID}x" != "rootx" ]; then
    echo "The Service Setup Must be run as the Root User"
    exit 0
fi

REGEX="^[0-9]+$"

#VM MMAP COUNT
if [ "$1" == "--default" ]; then
    VM_MMAP_COUNT=262144
elif [[ $1 =~ $REGEX ]]; then
    VM_MMAP_COUNT=$1
else
    echo "Invalid Argument, VM_MMAP_COUNT must be either --default or a number"
    exit 0
fi

echo "Backing Up Existing sysctl.conf"
if [ -f /etc/sysctl.conf.resolvebackup ]; then
    echo "Backup file /etc/sysctl.conf.resolvebackup already exists"
    printf "Do You Wish to Continue and Overwrite the Backup? (Y/N) "
    ANSWER=

    while [ "${ANSWER}x" == "x" ]
    do
        read inputline
        ANSWER=`echo $inputline | tr [A-Z] [a-z]`
        if [ "${ANSWER}" == "y" ] || [ "${ANSWER}" == "yes" ]; then
            ANSWER="Y"
        elif [ "${ANSWER}" == "n" ] || [ "${ANSWER}" == "no" ]; then
            ANSWER="N"
        else
            ANSWER=
            printf "Please Enter Y or N "
        fi
    done

    if [ "${ANSWER}" == "N" ]; then
        echo "Cancelling Sysctl Setup"
        exit 1
    fi
fi

cp -p /etc/sysctl.conf /etc/sysctl.conf.resolvebackup

if [ $VM_MMAP_COUNT -gt 0 ]; then
    echo "vm.max_map_count= $VM_MMAP_COUNT" | tee -a /etc/sysctl.conf
fi

sysctl -p

echo "Sysctl configuration has been set up"

