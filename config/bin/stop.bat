@echo off

if not "%1" == "" GOTO :START
    echo.
    echo "Usage: <Resolve Component...>"
    echo "This script will stop the specified Resolve Components"
    echo "ALL can be used to stop all components (except RSCONSOLE) installed on this system"
    GOTO :END

:START

:LOOP
if "%1" == "" GOTO :END_LOOP
    if /i "%1" == "ALL" GOTO :ALL
        sc stop %1
    GOTO :SHIFT
        :ALL
            sc stop rsmgmt
            sc stop rsremote
            sc stop rscontrol
            sc stop rsview
            sc stop rssearch
            sc stop rsmq
    :SHIFT
    shift
GOTO :LOOP
:END_LOOP

:END
