#!/bin/bash

if [ $# -lt 1 ]; then
  echo ""
  echo "Usage: <Resolve Component...>"
  echo "This script will start up the specified Resolve Components"
  echo "ALL can be used to start up all components (except RSCONSOLE) installed on this system"
  exit 1
fi

# properties
DIST=`echo $0 | awk '{ print substr( $0, 0, length($0)-6 ) }'`/..
BLUEPRINT=${DIST}/bin/blueprint.properties
FORCE=false
eval LASTARG=\${$#}

if [ $LASTARG = "1" ]; then
    FORCE=true
fi

while [ $# -gt 0 ]
do
    COMPONENT=`echo $1 | tr [A-Z] [a-z]`
    if [ $COMPONENT = "all" ]; then
        if [ -f $DIST/bin/init.rsmq ]; then
            if $FORCE; then
                $DIST/bin/init.rsmq start
            else
                $DIST/bin/init.rsmq start_safe
            fi
        fi
        if [ -f $DIST/bin/init.rsmgmt ]; then
            if $FORCE; then
                $DIST/bin/init.rsmgmt start
            else
                $DIST/bin/init.rsmgmt start_safe
            fi
        fi
          if [ -f $DIST/bin/init.rssearch ]; then
            if $FORCE; then
                $DIST/bin/init.rssearch start
            else
                $DIST/bin/init.rssearch start_safe
            fi
	    printf "Waiting for Elastic Search Startup Completion"
	    ES_PORT=`grep "rssearch.yml.http.port=" $BLUEPRINT | awk 'BEGIN{FS="="}{print $NF}' | tr [A-Z] [a-z]`
	    ES_URI=`grep "LOCALHOST=" $BLUEPRINT | awk 'BEGIN{FS="="}{print $NF}' | tr [A-Z] [a-z]`

	    function Elastic_Search_Status(){
	      echo http://$1:$2/_cluster/health?wait_for_status=yellow&timeout=40s
	      elastic_rp=$(curl -silent http://$1:$2/_cluster/health?wait_for_status=yellow&timeout=40s)
	      status=`echo "$elastic_rp"  | grep -o 'green\|yellow'`
	      echo $status
	    }
 
	    esready=$(Elastic_Search_Status  $ES_URI $ES_PORT $ES_URI)
	    COUNTER=1
	    #check the reuslt 
	    while [ $COUNTER -le 35 -a -z "$esready" ]
	    do 
	      printf "."
	      sleep 5
	      esready=$(Elastic_Search_Status  $ES_SEARCH_URI)
	      ((COUNTER=COUNTER+1))
	    done  
        fi
        if [ -f $DIST/bin/init.rsview ]; then
            if $FORCE; then
                $DIST/bin/init.rsview start
            else
                $DIST/bin/init.rsview start_safe
            fi
        fi
        if [ -f $DIST/bin/init.rscontrol ]; then
            if $FORCE; then
                $DIST/bin/init.rscontrol start
            else
                $DIST/bin/init.rscontrol start_safe
            fi
        fi

		# also starts Zookeeper, Kafka, Greenplum and Vault
        for FILE in `ls $DIST/bin`
        do
            case $FILE in
            init.*)
                if [ $FILE != "init.rsmq" ] && [ $FILE != "init.rsmgmt" ] && [ $FILE != "init.rssearch" ] && [ $FILE != "init.rsview" ] && [ $FILE != "init.rscontrol" ] && [ -f $DIST/bin/$FILE ]; then
                    if $FORCE; then
                        $DIST/bin/$FILE start
                    else
                        $DIST/bin/$FILE start_safe
                    fi
                fi
            ;;
            esac
        done
    elif [ $COMPONENT != "1" ]; then
        if [ $COMPONENT == "tomcat" ]; then
            COMPONENT=rsview
        elif [ $COMPONENT == "elasticsearch" ]; then
            COMPONENT=rssearch
        elif [ $COMPONENT == "rabbitmq" ]; then
            COMPONENT=rsmq
        fi
        INITFILE="${DIST}/bin/init.${COMPONENT}"
        if [ -f $INITFILE ]; then
            if $FORCE; then
                $INITFILE start
            else
                $INITFILE start_safe
            fi
        else
            echo "Invalid Component: ${COMPONENT}"
        fi
    fi
    shift
done
