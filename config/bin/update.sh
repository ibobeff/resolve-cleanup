#!/bin/bash

if [ $# -lt 2 ]; then
    echo "Usage: -f <filename> [OPTIONS]"
    echo ""
    echo "This script will update Resolve components."
    echo "The update zip file should be placed in the install directory."
    echo "The Name of the update zip is provided with -f."
    echo ""
    echo "Options:"
    echo "    -u            RSConsole username, used to import any modules"
    echo "                  included in the update. Default will be used"
    echo "                  if this option is not used"
    echo "    -p            RSConsole password, used to import any modules"
    echo "                  included in the update. Default will be used"
    echo "                  if this option or -pw option is not used"
    echo "    -pw           Prompt user for RSConsole password. Alternative"
    echo "                  to providing it with -p option"
    echo "    --continue    Continue failed update"
    echo "    --force       Continue update even if failures are detected"
    echo "    --no-config   Do not run configuration off of blueprint"
    echo "                  after updating"
    echo "    --no-stop     Do not stop all components before applying"
    echo "                  update"
    echo "    --no-start    Do not start all components after update is"
    echo "                  complete"
    echo "    --no-restart  Do not stop all components before applying"
    echo "                  update and do not start all components after"
    echo "                  update is complete"
    echo "    --no-import   Do not import any Updated Modules"
    echo "    --skip-sql    Do not apply any SQL updates included in the"
    echo "                  update. This includes ALTERS, Inserts, Updates"
    echo "                  and Deletes"
    echo "    --no-migrate  Do not run any Elasticsearch Data Migration"
    echo "    --no-timeout  Remove all Timeouts from RSView, Elasticsearch,"
    echo "                  and Migration tasks"
    echo "    --for-dr      Specify Update is for a DR Environment."
    echo "                  Components will not start and DB ALTERS,"
    echo "                  Inserts, Updates, and Deletes will not run"
    echo ""
    exit 1
fi

DIST=`echo $0 | awk '{ print substr( $0, 0, length($0)-9 ) }'`/..
OPTIONS=""
PROMPT_PASSWORD=false
BLUEPRINT=${DIST}/bin/blueprint.properties
ANSWER=
SUCCESS=FALSE

# set os type
linux=false
solaris=false
case "`uname`" in
Linux*) linux=true;;
SunOS*) solaris=true;;
esac

while [ $# -gt 0 ]
do
    if [ $1 == "-u" ]; then
        shift
        if [ $# -gt 0 ]; then
            USERNAME=$1
        else
            USERNAME=admin
        fi
    elif [ $1 == "-pw" ]; then
        PROMPT_PASSWORD=true
    elif [ $1 == "-p" ]; then
        shift
        if [ $# -gt 0 ]; then
            PASSWORD=$1
        else
            PASSWORD=resolve
        fi
    elif [ $1 == "-f" ]; then
        shift
        if [ $# -gt 0 ]; then
            FILENAME=$1
        fi
    else
        if [ $1 == "--auto-accept" ]; then
            ANSWER="AUTO"
        fi
        if [ "${OPTIONS}x" == "x" ]; then
            OPTIONS=$1
        else
            OPTIONS="$OPTIONS;$1"
        fi
    fi
    shift
done

if $PROMPT_PASSWORD; then
    printf "Password: "
    read -s PASSWORD
    echo ""
elif [ "${PASSWORD}x" == "x" ]; then
    PASSWORD=resolve
fi

if [ "${USERNAME}x" == "x" ]; then
    USERNAME=admin
fi

if [ "${FILENAME}x" == "x" ]; then
    echo "Missing update file name."
    echo "Canceling update."
    exit 1
elif [ ! -f $FILENAME ]; then
    echo "Update file $FILENAME does not exist."
    echo "Canceling update."
    exit 1
else
    cp $FILENAME $DIST 2>/dev/null
    FILENAME=`echo $FILENAME | awk 'BEGIN{FS="/"}{print $NF}'`
    unzip -oq $DIST/$FILENAME -d $DIST

    cd ${DIST}
    INSTALLDIR=`pwd`
fi

CP=
for FILE in `ls $INSTALLDIR/file/tmp/`
do
    case $FILE in
    *.jar)
        if [ "${CP}x" == "x" ]; then
            CP="${INSTALLDIR}/file/tmp/$FILE"
        else
            CP="${CP}:${INSTALLDIR}/file/tmp/$FILE"
        fi
    ;;
    esac
done
unzip -oq "${FILENAME}"
chmod 555 ${DIST}/file/tmp/unix_update.sh
source "${DIST}"/file/tmp/unix_update.sh
"${INSTALLDIR}/jdk/bin/java" -cp "$CP" -Djava.io.tmpdir=${DIST}/tmp -Dorg.owasp.esapi.resources="${INSTALLDIR}/file/tmp" com.resolve.update.Main "-FD$OPTIONS" -d "${INSTALLDIR}" -f "${INSTALLDIR}/file/tmp/update.xml" -u $USERNAME -p $PASSWORD -z $FILENAME
if [ $? -eq 0 ] ; then
    SUCCESS=TRUE
    rm $FILENAME 2>/dev/null
fi

if [ "${SUCCESS}" = "TRUE" ]; then
    ######################MCP Monitored Statement
    echo "Resolve update finished in $SECONDS seconds."
    ######################MCP Monitored Statement
    rsmgmt/bin/run.sh -f mgmt/Log "-FDResolve;Update;Finished;in;$SECONDS;seconds" -u 1>/dev/null
else
    ######################MCP Monitored Statement
    echo "Resolve update failed in $SECONDS seconds."
    ######################MCP Monitored Statement
    rsmgmt/bin/run.sh -f mgmt/Log "-FDResolve;Update;Failed;in;$SECONDS;seconds" -u 1>/dev/null
fi
