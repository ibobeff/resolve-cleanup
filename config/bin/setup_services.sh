#!/bin/bash

if [ $# -lt 1 ] || [ "$1" == "--help" ]; then
    echo ""
    echo "Usage: [Component]"
    echo "This setup script will configure Resolve to start automatically on server startup or restart"
    echo "The script will create copies in /etc/systemd/system then run systemctl enable for the specified resolve component(s)"
    echo ""
    echo "Using 'all' for the component name will run the setup for all components installed on this server"
    exit 0
fi

ID=`id | sed -e "s/^uid=[0-9]*(\(.*\)) gid.*/\1/"`

if [ "${ID}x" != "rootx" ]; then
    echo "The Service Setup Must be run as the Root User"
    exit 0
fi

DIST=`echo $0 | awk '{ print substr( $0, 0, length($0)-17 ) }'`/..

BIN=$DIST/bin

RC3=false
RC5=false

if [ -d /etc/rc3.d ]; then
    RC3=true
fi
if [ -d /etc/rc5.d ]; then
    RC5=true
fi

COMPONENTS=("$@")
ARGS=${#COMPONENTS[@]}
for (( i=0;i<$ARGS;i++));
do
    COMPONENT=${COMPONENTS[${i}]}
    if [ $COMPONENT = "tomcat" ]; then
        COMPONENT="rsview"
    fi
    if [ $COMPONENT = "elasticsearch" ]; then
        COMPONENT="rssearch"
    fi
    if [ $COMPONENT = "rabbitmq" ]; then
        COMPONENT="rsmq"
    fi
    if [ $COMPONENT = "all" ]; then
        for FILE in `ls $BIN`
        do
            case $FILE in
            init.*)
                if [ -f $BIN/$FILE ]; then
                    COMPONENT=`echo $FILE | awk '{ print substr( $0, 6 ) }'`
                    echo "Setup $COMPONENT in /etc/systemd/system"
                    if [ $COMPONENT = "rsmq" ]; then
                        cp -f $DIST/rabbitmq/linux64/rabbitmq/sbin/$COMPONENT.service /etc/systemd/system/$COMPONENT.service
                    elif [ $COMPONENT = "rssearch" ]; then
                        cp -f $DIST/elasticsearch/bin/$COMPONENT.service /etc/systemd/system/$COMPONENT.service
                    elif [ $COMPONENT = "rsview" ]; then
                        cp -f $DIST/tomcat/bin/$COMPONENT.service /etc/systemd/system/$COMPONENT.service
                    else
                        cp -f $DIST/$COMPONENT/bin/$COMPONENT.service /etc/systemd/system/$COMPONENT.service
                    fi

                    systemctl daemon-reexec
                    systemctl enable $COMPONENT
                fi
            ;;
            esac
        done

        echo "Setting up container service"
        cp -f $BIN/resolve.service /etc/systemd/system/resolve.service
        systemctl daemon-reexec
        systemctl enable resolve
    else
        FILE="${BIN}/init.${COMPONENT}"
        if [ -f $FILE ]; then
            echo "Setup $COMPONENT in /etc/systemd/system"
            if [ $COMPONENT = "rsmq" ]; then
                cp -f $DIST/rabbitmq/linux64/rabbitmq/sbin/$COMPONENT.service /etc/systemd/system/$COMPONENT.service
            elif [ $COMPONENT = "rssearch" ]; then
                cp -f $DIST/elasticsearch/bin/$COMPONENT.service /etc/systemd/system/$COMPONENT.service
            elif [ $COMPONENT = "rsview" ]; then
                cp -f $DIST/tomcat/bin/$COMPONENT.service /etc/systemd/system/$COMPONENT.service
            else
                cp -f $DIST/$COMPONENT/bin/$COMPONENT.service /etc/systemd/system/$COMPONENT.service
            fi

            systemctl daemon-reexec
            systemctl enable $COMPONENT
        else
            echo "Invalid Component: ${COMPONENT}"
        fi
    fi

    echo "Reloading systemctl daemons"
    systemctl daemon-reload
done
