@echo off

if not "%1" == "/?" GOTO :COMMAND
    echo.
    echo "Usage: <Resolve Component...>"
    echo "This script will get the status of the specified Resolve Components"
    GOTO :END

:COMMAND
if not "%1" == "" GOTO :START
    call %0 all
    goto :END


:START

:LOOP
if "%1" == "" GOTO :END_LOOP
    if /i "%1" == "TOMCAT" GOTO :RSVIEW
    if /i "%1" == "ALL" GOTO :ALL
        sc query %1
    GOTO :SHIFT
        :ALL
            sc query rsremote
            sc query rscontrol
            sc query rsmgmt
            sc query rsmq
            sc query rssearch
        :RSVIEW
            sc query rsview
    :SHIFT
    shift
GOTO :LOOP
:END_LOOP

:END
