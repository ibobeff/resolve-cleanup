#!/bin/bash
# properties
DIST=`echo $0 | awk '{ print substr( $0, 0, length($0)-10 ) }'`/..
JDK=$DIST/jdk
JAVA_LIB_PATH=$DIST/lib
# OWASP ESAPI
# RSMGMT shall be on every server running resolve
JAVA_OPTS="${JAVA_OPTS} -Dorg.owasp.esapi.resources=${DIST}/rsmgmt/config"

"$JDK/bin/java" ${JAVA_OPTS} -cp "$JAVA_LIB_PATH/rsexec.jar:$JAVA_LIB_PATH/json-lib.jar:$JAVA_LIB_PATH/commons-lang3.jar:$JAVA_LIB_PATH/log4j.jar:$JAVA_LIB_PATH/esapi.jar" com.resolve.util.client.RSClientExecute "$@"
