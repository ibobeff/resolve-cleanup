#!/bin/bash

if [ "$#" -ne 6 ]; then
    echo "Illegal number of parameters"
    echo Usage: index_alias sysid host1 port1 host2 port2
    exit 1
fi

set -e
INDEX_NAME="$1"
SYSID="$2"
HOST1="$3"
PORT1="$4"
HOST2="$5"
PORT2="$6"

echo
echo document data from $HOST1:$PORT1 
ft1='{"filter":{ "term":{"_id": '"$SYSID"'}}}'
ft1='{"query": { "bool": {"must": [{ "term": {"id": "'"$SYSID"'"}}]}}}'
echo $ft1
curl  -XPOST "$HOST1:$PORT1/$INDEX_NAME/_search?pretty" -d '{"query": { "bool": {"must": [{ "term": {"id": "'"$SYSID"'"}}]}}}'  
echo
echo ---------------------------------------
echo
echo document data from $HOST2:$PORT2 
curl -XGET "$HOST2:$PORT2/$INDEX_NAME/_search?pretty"  -d '{"query": { "bool": {"must": [{ "term": {"id": "'"$SYSID"'"}}]}}}'






