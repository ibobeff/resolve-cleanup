#!/bin/bash

if [ $# -lt 2 ]; then
   echo ""
   echo "Usage: -i <Version>"
   echo ""
   echo "This script will roll back Resolve components."
   echo ""
   echo "The ID of the update to roll back will be provided by -i. To"
   echo "determine which updates are on a system check the \"file\" folder"
   echo "for checksum files like update-<Version>.md5 All Resolve"
   echo "components must be stopped before executing the rollback."
   echo ""
   exit 1
fi

DIST=`echo $0 | awk '{ print substr( $0, 0, length($0)-11 ) }'`/..
OPTIONS=""
ANSWER=

# set os type
linux=false
solaris=false
case "`uname`" in
Linux*) linux=true;;
SunOS*) solaris=true;;
esac

if $solaris; then
    RUNNING=`/usr/ucb/ps -augxwww | grep resolve-[a-zA-Z]*\.jar | grep -v grep`
else
    RUNNING=`ps -ef | grep -e resolve-[a-zA-Z]*\.jar | grep -v grep`
fi
if [ "${RUNNING}x" != "x" ]; then
    echo "WARNING!!!  Resolve Components Have Been Detected Running on the System"
    echo "All Resolve Components Must be Stopped before Rolling Back"
    echo "Cancelling Rollback"
    exit 1
fi

if $solaris; then
    RUNNING=`/usr/ucb/ps -augxwww | grep Bootstrap | grep sree | grep hazelcast | grep -v grep`
else
    RUNNING=`ps -ef | grep Bootstrap | grep sree | grep hazelcast | grep -v grep`
fi
if [ "${RUNNING}x" != "x" ]; then
    echo "WARNING!!!  RSView Has Been Detected Running on the System"
    echo "All Resolve Components Must be Stopped before Rolling Back"
    echo "Cancelling Rollback"
    exit 1
fi

if $solaris; then
    RUNNING=`/usr/ucb/ps -augxwww | grep coord.cfg | grep -v grep`
else
    RUNNING=`ps -ef | grep coord.cfg | grep -v grep`
fi
if [ "${RUNNING}x" != "x" ]; then
    echo "WARNING!!!  RSZK Has Been Detected Running on the System"
    echo "All Resolve Components Must be Stopped before Rolling Back"
    echo "Cancelling Rollback"
    exit 1
fi

if $solaris; then
    RUNNING=`/usr/ucb/ps -augxwww | grep routerconfig.xml | grep -v grep`
else
    RUNNING=`ps -ef | grep routerconfig.xml | grep -v grep`
fi
if [ "${RUNNING}x" != "x" ]; then
    echo "WARNING!!!  RSMQ Has Been Detected Running on the System"
    echo "All Resolve Components Must be Stopped before Rolling Back"
    echo "Cancelling Rollback"
    exit 1
fi

while [ $# -gt 0 ]
do
    if [ $1 == "-i" ]; then
        shift
        if [ $# -gt 0 ]; then
            UPDATE=$1
        fi
    else
        if [ $1 == "--auto-accept" ]; then
            ANSWER=AUTO
        elif [ "${OPTIONS}x" == "x" ]; then
            OPTIONS=$1
        else
            OPTIONS="$OPTIONS;$1"
        fi
    fi
    shift
done

if [ "${UPDATE}x" == "x" ]; then
    echo "Missing Update ID to Roll Back"
    echo "Cancelling Rollback"
    exit 1
fi

echo "You are about to roll back the changed applied by the ${UPDATE} update on this server."
echo "This action cannot be undone, are you sure you want to continue?"

if [ "$ANSWER" != "AUTO" ]; then
    printf "Execute Rollback? (Y/N) "
    ANSWER=

    while [ "${ANSWER}x" == "x" ]
    do
        read inputline
        ANSWER=`echo $inputline | tr [A-Z] [a-z]`
        if [ "${ANSWER}" == "y" ] || [ "${ANSWER}" == "yes" ]; then
            ANSWER="Y"
        elif [ "${ANSWER}" == "n" ] || [ "${ANSWER}" == "no" ]; then
            ANSWER="N"
        else
            ANSWER=
            printf "Please Enter Y or N "
        fi
    done

    if [ "${ANSWER}" == "N" ]; then
        echo "Cancelling Rollback"
        exit 1
    fi
fi

UPDATE=`echo $UPDATE | tr . _`

${DIST}/rsmgmt/bin/run.sh -f install/resolve-rollback "-FD$UPDATE;$OPTIONS"

chmod 755 ${DIST}/rabbitmq/linux64/rabbitmq/sbin/*
chmod 755 ${DIST}/rabbitmq/linux64/erlang/bin/*
chmod 755 ${DIST}/rabbitmq/linux64/erlang/erts-7.3/bin/*
chmod 755 ${DIST}/rsconsole/bin/*.sh
chmod 755 ${DIST}/rsmgmt/bin/*.sh
chmod 755 ${DIST}/rsmgmt/bin/init.*
chmod 755 ${DIST}/rscontrol/bin/*.sh
chmod 755 ${DIST}/rscontrol/bin/init.*
chmod 755 ${DIST}/rsremote/bin/*.sh
chmod 755 ${DIST}/rsremote/bin/init.*
chmod 754 ${DIST}/tomcat/bin/*.sh
chmod 754 ${DIST}/tomcat/bin/init.*
chmod 755 ${DIST}/rssync/bin/*.sh
chmod 755 ${DIST}/rssync/bin/init.*
chmod 755 ${DIST}/rsarchive/bin/*.sh
chmod 755 ${DIST}/rsarchive/bin/init.*
chmod 755 ${DIST}/elasticsearch/bin/*.sh
chmod 755 ${DIST}/elasticsearch/bin/init.*
chmod 755 ${DIST}/elasticsearch/bin/elasticsearch
chmod 755 ${DIST}/bin/*.sh
