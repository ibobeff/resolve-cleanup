#!/bin/bash

# properties
BIN=`echo $0 | awk '{ print substr( $0, 0, length($0)-6 ) }'`

cd ${BIN}
DIST=`pwd`
PRIMARY=TRUE

export ZOOKEEPER_PID_FILE=$DIST/lock

if [ -f ${ZOOKEEPER_PID_FILE} ]; then
    if [ $# -eq 0 ] || [ "$1" != "1" ]; then
        echo "Lock File Detected"
        echo "Cancelling Zookeeper Startup"
        exit 1
    fi
fi

echo "Starting Zookeeper"
bin/zkServer.sh start
sleep 20s
`ps -ef | grep "/dcs/zookeeper-3.4.12" | awk '/zookeeper-3.4.12/ && !/awk/ && !/grep/ {print $2}' | tr '\n' ' ' > $ZOOKEEPER_PID_FILE`

PID=`cat $ZOOKEEPER_PID_FILE`
echo "Started Zookeeper pid: ${PID}"
