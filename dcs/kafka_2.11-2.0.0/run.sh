#!/bin/bash

# properties
BIN=`echo $0 | awk '{ print substr( $0, 0, length($0)-6 ) }'`

cd ${BIN}
DIST=`pwd`
PRIMARY=TRUE

export KAFKA_PID_FILE=$DIST/lock

if [ -f ${KAFKA_PID_FILE} ]; then
    if [ $# -eq 0 ] || [ "$1" != "1" ]; then
        echo "Lock file for Kafka detected"
        echo "Canceling Kafka Startup"
        exit 1
    fi
fi

echo "Starting Kafka"
bin/kafka-server-start.sh config/server.properties &
sleep 30s
`ps -ef | grep "/dcs/kafka_2.11-2.0.0" | awk '/kafka_2.11-2.0.0/ && !/awk/ && !/grep/ {print $2}' | tr '\n' ' ' > $KAFKA_PID_FILE`

PID=`cat $KAFKA_PID_FILE`
echo "Started Kafka pid: ${PID}"
